<?php

class CAddOnLeaseAssociations extends CLeaseAssociations {

	public static function fetchAddOnLeaseAssociationByIdByLeaseIdByPropertyIdByCid( $intAddOnleaseAssociationId, $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intAddOnleaseAssociationId ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.id AS add_on_group_id,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.is_active,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						( ce.name_last || \' \' || ce.name_first ) as leasing_agent_name,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						scr.charge_amount AS rent_amount,
						scr.ar_code_id AS rent_ar_code_id,
						scd.charge_amount AS deposit_amount,
						scd.ar_code_id AS deposit_ar_code_id,
						scrf.charge_amount AS other_amount,
						scrf.ar_code_id AS other_ar_code_id,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						LEFT JOIN scheduled_charges scr ON ( scr.cid = la.cid AND scr.property_id = la.property_id AND scr.lease_association_id = la.id AND scr.lease_id = la.lease_id AND scr.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scr.ar_trigger_id = ' . CArTrigger::MONTHLY . '  AND scr.deleted_on IS NULL)
						LEFT JOIN scheduled_charges scd ON ( scd.cid = la.cid AND scd.property_id = la.property_id AND scd.lease_association_id = la.id AND scd.lease_id = la.lease_id AND scd.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scd.ar_trigger_id = ' . CArTrigger::MOVE_IN . ' AND scd.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scrf ON ( scrf.cid = la.cid AND scrf.property_id = la.property_id AND scrf.lease_association_id = la.id AND scrf.lease_id = la.lease_id AND scrf.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ( scrf.ar_trigger_id = ' . CArTrigger::APPLICATION_COMPLETED . ' OR scrf.ar_trigger_id = ' . CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT . ' ) AND scrf.deleted_on IS NULL )
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.property_id = la.property_id AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.property_id = ao.property_id AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.id = ' . ( int ) $intAddOnleaseAssociationId . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.deleted_on IS NULL';

		return self::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public static function fetchNewAddOnLeaseAssociationByIdByLeaseIdByPropertyIdByCid( $intAddOnleaseAssociationId, $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intAddOnleaseAssociationId ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.id AS add_on_group_id,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.is_active,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						( ce.name_last || \' \' || ce.name_first ) as leasing_agent_name,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.property_id = la.property_id AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.property_id = ao.property_id AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.id = ' . ( int ) $intAddOnleaseAssociationId . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.deleted_on IS NULL';

		$objAddOnleaseAssociation = self::fetchLeaseAssociation( $strSql, $objDatabase );

		$strSql = 'SELECT
						sc.*
						FROM
							scheduled_charges sc
						WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.property_id =' . ( int ) $intPropertyId . '
						AND sc.lease_association_id  = ' . ( int ) $intAddOnleaseAssociationId . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.ar_trigger_id IN (' . implode( ',', CArTrigger::$c_arrintAddOnsArTriggers ) . ' )
						AND sc.deleted_on IS NULL
						ORDER BY
						sc.ar_trigger_id DESC';

		$arrobjAddOnleaseAssociationScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledCharges( $strSql, $objDatabase );

		if( false == valArr( $arrobjAddOnleaseAssociationScheduledCharges ) ) {
			return $objAddOnleaseAssociation;
		} else {
			$arrobjTempScheduledCharges = [];
			foreach( $arrobjAddOnleaseAssociationScheduledCharges  as $objAddOnleaseAssociationScheduledCharge ) {
				$arrobjTempScheduledCharges[$objAddOnleaseAssociationScheduledCharge->getId()] = $objAddOnleaseAssociationScheduledCharge;
			}
			if( true == valObj( $objAddOnleaseAssociation, 'CAddOnLeaseAssociation' ) )
			$objAddOnleaseAssociation->setScheduledCharges( $arrobjTempScheduledCharges );
		}

		return $objAddOnleaseAssociation;
	}

	public static function fetchCustomAddOnLeaseAssociationsForBulkMoveInByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase, $boolIsCheckDeleted = true, $arrintLeaseStatusTypeIds = NULL ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strDeleteConditions = ( true == $boolIsCheckDeleted ) ? 'AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';
		$strLeaseStatusTypes = ( false == empty( $arrintLeaseStatusTypeIds ) ) ? ' AND la.lease_status_type_id IN (' . implode( ',', $arrintLeaseStatusTypeIds ) . ') ' : '';

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.is_active,
						aog.add_on_type_id,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						sc.charge_amount as rent_amount,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						CASE WHEN sc.installment_id > 0 THEN 1 ELSE 0 END as is_included_in_installment,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aoc.id AS add_on_category_id,
						aog.id AS add_on_group_id,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN properties p ON ( la.cid = p.cid AND la.property_id = p.id )
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
						LEFT JOIN scheduled_charges sc ON ( sc.cid = la.cid AND sc.lease_association_id = la.id AND la.lease_id = sc.lease_id AND sc.ar_trigger_id IN ( ' . ( int ) CArTrigger::MONTHLY . ' ) )
						LEFT JOIN property_preferences pp_student ON ( la.cid = pp_student.cid AND la.property_id = pp_student.property_id AND pp_student.key = \'ENABLE_SEMESTER_SELECTION\' )
						LEFT JOIN property_preferences pp_checklist ON ( la.cid = pp_checklist.cid AND la.id = pp_checklist.property_id AND pp_checklist.key = \'ENABLE_MOVE_IN_CHECKLIST\' )
					WHERE
						la.cid = ' . ( int ) $intCid . $strLeaseStatusTypes . '
						AND CASE WHEN ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp_student.id IS NOT NULL AND pp_checklist.id IS NOT NULL THEN
									aog.add_on_type_id = ' . CAddOnType::RENTABLE_ITEMS . '
									AND ao.unit_space_id IS NULL
							ELSE CASE WHEN pp_checklist.id IS NULL THEN
										aog.add_on_type_id IN ( ' . implode( ',', CAddOnType::$c_arrintScheduledChargeAddOnTypeIds ) . ' )
										ELSE true
								 END
							END
						AND la.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ' . $strDeleteConditions;

		$strSql .= ' ORDER BY la.lease_status_type_id, la.start_date';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByIdsByCid( $arrintAddOnLeaseAssociationIds, $intCid,  $objDatabase ) {
		if( false == valArr( $arrintAddOnLeaseAssociationIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.name AS add_on_group_name,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name
					FROM
						lease_associations as la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.id IN( ' . implode( ',', $arrintAddOnLeaseAssociationIds ) . ' )';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationMaxIdByAddOnIdByCid( $intAddOnId, $intCid, $objDatabase ) {

		if( true == is_null( $intAddOnId ) || false == is_numeric( $intAddOnId ) ) 	return NULL;

		$strSql = 'SELECT MAX(id) as id FROM lease_associations WHERE ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ar_origin_reference_id = ' . ( int ) $intAddOnId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						lease_associations
					WHERE
						ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );

	}

	public static function fetchAddOnLeaseAssociationByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						lease_associations
					WHERE
						ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );

	}

	// ByLeaseId

	public static function fetchAddOnLeaseAssociationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aog.name AS add_on_group_name,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.add_on_type_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByLeaseIdByLeaseStatusTypeIdsByCid( $intLeaseId, $arrintLeaseStatusTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aog.name AS add_on_group_name,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.add_on_type_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.lease_status_type_id IN (' . implode( ',', $arrintLeaseStatusTypeIds ) . ')
						AND la.cid = ' . ( int ) $intCid . '
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByPropertyIdByLeaseIdsByCid( $intPropertyId, $arrintLeaseIds, $intCid, $objDatabase, $boolIsCheckDeleted = true, $arrintLeaseStatusTypeIds = NULL, $arrintLeaseIntervalIds = NULL ) {
		if( false == valArr( array_filter( $arrintLeaseIds ) ) ) return NULL;

		$strDeleteConditions = ( true == $boolIsCheckDeleted ) ?  'AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';

		$strLeaseIntevalConditions = ( true == valArr( $arrintLeaseIntervalIds ) ) ? ' AND la.lease_interval_id IN (' . implode( ',', $arrintLeaseIntervalIds ) . ') ' : '';

		$strLeaseStatusTypes = ( false == empty( $arrintLeaseStatusTypeIds ) ) ? ' AND la.lease_status_type_id IN (' . implode( ',', $arrintLeaseStatusTypeIds ) . ') ' : '';

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						aog.add_on_type_id,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						ao.unit_space_id as unit_space_id,
						sc.charge_amount as current_rent_amount,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aoc.id as add_on_category_id,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
						LEFT JOIN scheduled_charges sc ON ( sc.cid = la.cid AND sc.property_id = la.property_id AND sc.ar_origin_reference_id = la.ar_origin_reference_id AND sc.deleted_on IS NULL )
					WHERE
						la.cid = ' . ( int ) $intCid . $strLeaseStatusTypes . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.lease_id IN (' . implode( ',', $arrintLeaseIds ) . ') ' . $strDeleteConditions . $strLeaseIntevalConditions;

		$strSql .= ' ORDER BY la.lease_status_type_id, la.start_date';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchCustomAddOnLeaseAssociationsByLeaseIdsByAddOnTypeIdsByCid( $arrintLeaseIds, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsCheckDeleted = true, $arrintLeaseStatusTypeIds = NULL, $boolExcludeAddOnAttachedToUnit = false ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strDeleteConditions = ( true == $boolIsCheckDeleted ) ? 'AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';
		$strLeaseStatusTypes = ( false == empty( $arrintLeaseStatusTypeIds ) ) ? ' AND la.lease_status_type_id IN (' . implode( ',', $arrintLeaseStatusTypeIds ) . ') ' : '';

		$strAddOnAttachedToUnit = ( true == $boolExcludeAddOnAttachedToUnit ) ? ' AND ao.unit_space_id IS NULL' :'';

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.is_active,
						aog.add_on_type_id,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						scr.charge_amount as rent_amount,
						scr.ar_code_id as rent_ar_code_id,
						scd.charge_amount AS deposit_amount,
						scd.ar_code_id as deposit_ar_code_id,
						scrf.charge_amount AS other_amount,
						scrf.ar_code_id as other_ar_code_id,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						CASE WHEN scr.installment_id > 0 THEN 1 ELSE 0 END as is_included_in_installment,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aoc.id AS add_on_category_id,
						aog.id AS add_on_group_id,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
						LEFT JOIN scheduled_charges scr ON ( scr.cid = la.cid AND scr.lease_id = la.lease_id AND scr.ar_origin_reference_id = la.ar_origin_reference_id AND scr.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scr.ar_trigger_id = ' . CArTrigger::MONTHLY . ' AND scr.lease_association_id = la.id AND scr.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scd ON ( scd.cid = la.cid AND scd.lease_id = la.lease_id AND scd.ar_origin_reference_id = la.ar_origin_reference_id AND scd.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scd.ar_trigger_id = ' . CArTrigger::MOVE_IN . ' AND scd.lease_association_id = la.id AND scd.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scrf ON ( scrf.cid = la.cid AND scrf.lease_id = la.lease_id AND scrf.ar_origin_reference_id = la.ar_origin_reference_id AND scrf.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ( scrf.ar_trigger_id = ' . CArTrigger::APPLICATION_COMPLETED . ' OR scrf.ar_trigger_id = ' . CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT . ' ) AND scrf.lease_association_id = la.id AND scrf.deleted_on IS NULL )
					WHERE
						la.cid = ' . ( int ) $intCid . $strLeaseStatusTypes . '
						AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' )
						AND la.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ' . $strDeleteConditions . $strAddOnAttachedToUnit;

		$strSql .= ' ORDER BY la.lease_status_type_id, la.start_date';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchNewCustomAddOnLeaseAssociationsByLeaseIdsByAddOnTypeIdsByPropertyIdsByCid( $arrintLeaseIds, $arrintAddOnTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIsCheckDeleted = true, $arrintLeaseStatusTypeIds = NULL, $boolExcludeAddOnAttachedToUnit = false ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strDeleteConditions = ( true == $boolIsCheckDeleted ) ? 'AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';
		$strLeaseStatusTypes = ( false == empty( $arrintLeaseStatusTypeIds ) ) ? ' AND la.lease_status_type_id IN (' . implode( ',', $arrintLeaseStatusTypeIds ) . ') ' : '';

		$strAddOnAttachedToUnit = ( true == $boolExcludeAddOnAttachedToUnit ) ? ' AND ao.unit_space_id IS NULL' :'';

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.is_active,
						aog.add_on_type_id,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aoc.id AS add_on_category_id,
						aog.id AS add_on_group_id,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
						
					WHERE
						la.cid = ' . ( int ) $intCid . $strLeaseStatusTypes . '
						AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' )
						AND la.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) 
						AND ( la.quote_id IS NULL OR ( la.quote_id IS NOT NULL AND la.is_selected_quote IS TRUE ) ) ' . $strDeleteConditions . $strAddOnAttachedToUnit;

		$strSql .= ' ORDER BY la.lease_status_type_id, la.start_date';

		$arrobjAddOnLeaseAssociations = self::fetchLeaseAssociations( $strSql, $objDatabase );

		if( true == valArr( $arrobjAddOnLeaseAssociations ) ) {
			$strSql = 'SELECT
						sc.*
						FROM
							scheduled_charges sc
						WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND sc.lease_association_id IN (' . implode( ',', array_keys( $arrobjAddOnLeaseAssociations ) ) . ' )
						AND sc.ar_trigger_id IN (' . implode( ',', CArTrigger::$c_arrintAddOnsArTriggers ) . ' )
						AND sc.deleted_on IS NULL
						ORDER BY
						sc.ar_trigger_id DESC';

			$arrobjAddOnScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledCharges( $strSql, $objDatabase );

			if( false == valArr( $arrobjAddOnScheduledCharges ) ) {
				return $arrobjAddOnLeaseAssociations;
			} else {
				foreach( $arrobjAddOnLeaseAssociations  as $objAddOnLeaseAssociation ) {
					$arrobjTempScheduledCharges = [];
					foreach( $arrobjAddOnScheduledCharges as $objAddOnScheduledCharge ) {
						if( $objAddOnLeaseAssociation->getId() == $objAddOnScheduledCharge->getLeaseAssociationId() ) {
							$arrobjTempScheduledCharges[$objAddOnScheduledCharge->getId()] = $objAddOnScheduledCharge;
						}
					}
					$objAddOnLeaseAssociation->setScheduledCharges( $arrobjTempScheduledCharges );
				}
			}
		}
		return $arrobjAddOnLeaseAssociations;
	}

	public static function fetchCustomAddOnLeaseAssociationsByLeaseIdsByAddOnTypeIdsByCidByLeaseStatusTypeIds( $arrintLeaseIds, $arrintAddOnTypeIds, $intCid, $objDatabase, $arrintLeaseStatusTypeIds = NULL ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.is_active,
						aog.add_on_type_id,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						ao.unit_space_id as unit_space_id,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aoc.id AS add_on_category_id,
						aog.id AS add_on_group_id,
						CASE WHEN ao.unit_space_id IS NULL
							THEN false
							ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )						
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND ( la.lease_status_type_id IN (' . implode( ',', $arrintLeaseStatusTypeIds ) . ') OR ( la.lease_status_type_id = ' . CLeaseStatusType::NOTICE . ' AND ao.unit_space_id IS NOT NULL ) ) 
						AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' )
						AND la.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ' .
		          ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL ';

		$strSql .= ' ORDER BY la.lease_status_type_id, la.start_date';

		$arrobjAddOnLeaseAssociations = self::fetchLeaseAssociations( $strSql, $objDatabase );

		if( true == valArr( $arrobjAddOnLeaseAssociations ) ) {
			$strSql = 'SELECT
						sc.*,
						CASE WHEN sc.installment_id > 0 THEN 1 ELSE 0 END as is_included_in_installment
						FROM
							scheduled_charges sc
						WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_association_id IN (' . implode( ',', array_keys( $arrobjAddOnLeaseAssociations ) ) . ' )
						AND sc.ar_trigger_id IN (' . implode( ',', CArTrigger::$c_arrintAddOnsArTriggers ) . ' )
						AND sc.deleted_on IS NULL
						ORDER BY
						sc.ar_trigger_id DESC';

			$arrobjAddOnScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledCharges( $strSql, $objDatabase );

			if( false == valArr( $arrobjAddOnScheduledCharges ) ) {
				return $arrobjAddOnLeaseAssociations;
			} else {
				foreach( $arrobjAddOnLeaseAssociations  as $objAddOnLeaseAssociation ) {
					$arrobjTempScheduledCharges = [];
					foreach( $arrobjAddOnScheduledCharges as $objAddOnScheduledCharge ) {
						if( $objAddOnLeaseAssociation->getId() == $objAddOnScheduledCharge->getLeaseAssociationId() ) {
							$arrobjTempScheduledCharges[$objAddOnScheduledCharge->getId()] = $objAddOnScheduledCharge;
						}
					}
					$objAddOnLeaseAssociation->setScheduledCharges( $arrobjTempScheduledCharges );
				}
			}
		}
		return $arrobjAddOnLeaseAssociations;
	}

	public static function fetchPaginatedReadyAddOnsByCidByAddOnTypeIds( $objDashBoardLibrary, $arrintAddOnTypeIds, $boolMoveIn, $intOffset = NULL, $intLimit = NULL, $intCid, $objDatabase ) {

		if( false == $objDashBoardLibrary->getSelectedPropertyIds() ) return NULL;

		$strSql = 'SELECT
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.name AS add_on_group_name,
						CASE
							WHEN c.name_first IS NOT NULL AND c.name_last IS NOT NULL
							THEN ( c.name_last || \', \' || c.name_first )
							WHEN c.name_first IS NOT NULL AND c.name_last IS NULL
							THEN c.name_first
							ELSE c.name_last
						END AS customer_name_full,
					c.email_address AS customer_email_address,
					c.phone_number AS phone_number,
					p.property_name AS property_name,
					l.unit_number_cache,
					la.id,
					la.start_date,
					la.end_date,
					la.lease_id,
					la.customer_id,
					la.property_id,
					ao.available_on AS available_on,
					ao.unit_space_status_type_id AS unit_space_status_type_id
				FROM
					lease_associations la
					JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
					JOIN add_on_groups aog ON ( ao.cid = aog.cid AND ao.add_on_group_id = aog.id )
					JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					JOIN customers c ON ( c.cid = la.cid AND c.id = la.customer_id )
					JOIN cached_leases l ON ( l.cid = la.cid AND l.id = la.lease_id )
					JOIN properties p ON ( p.cid = la.cid AND p.id = la.property_id )
					JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.customer_type_id = 1 )
				WHERE
					la.cid = ' . ( int ) $intCid . '
					AND la.deleted_on IS NULL
			  		AND la.property_id IN ( ' . implode( ',', $objDashBoardLibrary->getSelectedPropertyIds() ) . ' ) ';

		$strSql .= ' AND riot.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' )';

		if( true == $boolMoveIn ) {
			$strSql .= ' AND la.lease_status_type_id = ' . CLeaseStatusType::FUTURE . '
						 AND lc.lease_status_type_id != ' . CLeaseStatusType::FUTURE . '
						 AND la.start_date <= NOW() ';
		} else {
			$strSql .= ' AND la.lease_status_type_id IN ( ' . CLeaseStatusType::NOTICE . ' )
						 AND la.end_date < NOW() ';
		}

		if( true == valStr( $objDashBoardLibrary->getSortBy() ) && true == valStr( $objDashBoardLibrary->getSortDirection() ) ) {

			$strSql .= ' ORDER BY ( ' . $objDashBoardLibrary->getSortBy() . ' ) ' . $objDashBoardLibrary->getSortDirection();

			if( 'customer_name_full' != $objDashBoardLibrary->getSortBy() ) {
				$strSql .= ' , customer_name_full ASC';
			}

		} else {
			$strSql .= 'ORDER BY start_date ASC, customer_name_full ASC';
		}

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByAddOnIdByLeaseStatuTypeIdsByCid( $intAddOnId, $arrintLeaseStatusTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						ao.available_on,
						cu.username 	
					FROM
						lease_associations la
						JOIN add_ons ao ON ( la.cid = ao.cid AND ao.id = la.ar_origin_reference_id AND ao.is_active IS TRUE )
						JOIN lease_customers lc ON ( la.lease_id = lc.lease_id AND la.customer_id = lc.customer_id AND la.cid = lc.cid )
						LEFT JOIN company_users cu ON ( cu.cid = ao.cid AND cu.id = ao.updated_by )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.ar_origin_reference_id = ' . ( int ) $intAddOnId . '
						AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND la.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL
					ORDER BY start_date, id';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociaitonsByStartDateByLeaseIdByCid( $strStartDate, $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						la.*,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.name AS add_on_group_name,
						ao.unit_space_status_type_id as unit_space_status_type_id,
						ao.unit_space_id,
						ao.available_on as available_on
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( ao.cid = aog.cid AND ao.add_on_group_id = aog.id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.start_date < \'' . $strStartDate . '\'
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL';

		$arrobjAddOnLeaseAssociations = self::fetchLeaseAssociations( $strSql, $objDatabase );

		if( true == valArr( $arrobjAddOnLeaseAssociations ) ) {
			$strSql = 'SELECT
						sc.*
						FROM
							scheduled_charges sc
						WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_association_id IN (' . implode( ',', array_keys( $arrobjAddOnLeaseAssociations ) ) . ' )
						AND sc.ar_trigger_id = ' . CArTrigger::MONTHLY . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.deleted_on IS NULL
						ORDER BY
						sc.ar_trigger_id DESC';

			$arrobjAddOnScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledCharges( $strSql, $objDatabase );

			if( false == valArr( $arrobjAddOnScheduledCharges ) ) {
				return $arrobjAddOnLeaseAssociations;
			} else {
				foreach( $arrobjAddOnLeaseAssociations  as $objAddOnLeaseAssociation ) {
					$arrobjTempScheduledCharges = [];
					foreach( $arrobjAddOnScheduledCharges as $objAddOnScheduledCharge ) {
						if( $objAddOnLeaseAssociation->getId() == $objAddOnScheduledCharge->getLeaseAssociationId() ) {
							$arrobjTempScheduledCharges[$objAddOnScheduledCharge->getId()] = $objAddOnScheduledCharge;
						}
					}
					$objAddOnLeaseAssociation->setScheduledCharges( $arrobjTempScheduledCharges );
				}
			}
		}
		return $arrobjAddOnLeaseAssociations;
	}

	// ByLeaseIntervalId

	public static function fetchAddOnLeaseAssociationsByLeaseIntervalIdsByCid( $arrintLeaseIntervalIds, $intCid, $objDatabase, $arrintExcludeLeaseStatusTypeIds = [] ) {
		if( false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSqlConditions = '';
		if( true == valArr( $arrintExcludeLeaseStatusTypeIds ) ) {
			$strSqlConditions = ' AND la.lease_status_type_id NOT IN ( ' . implode( ',', $arrintExcludeLeaseStatusTypeIds ) . ' ) ';
		}

		$strSql = 'SELECT
                        la.*,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.name AS add_on_group_name,
						ao.unit_space_status_type_id as unit_space_status_type_id,
						ao.unit_space_id,
						ao.available_on as available_on
 				    FROM 
 				        lease_associations la
 				        JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( ao.cid = aog.cid AND ao.add_on_group_id = aog.id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
 				    WHERE 
 				        la.cid = ' . ( int ) $intCid . ' 
 				        AND la.lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ') 
 				        AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . $strSqlConditions;

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdsByAddOnTypeIdsByCid( $intPropertyId, $arrintLeaseIntervalIds, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsCheckDeleted = false, $boolExcludeAddOnsAttachedToUnit = false, $arrintLeaseStatusTypeIds = NULL, $boolIncludeIsSelectedQuote = true ) {
		if( false == valArr( $arrintAddOnTypeIds ) || false == valArr( $arrintLeaseIntervalIds ) ) {
			return NULL;
		}

		$strSqlConditions = '';

		if( true == valArr( $arrintLeaseStatusTypeIds ) ) {
			$strSqlConditions .= ' AND la.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ') ';
		}

		$strSqlConditions .= ( true == $boolIsCheckDeleted ) ? ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';

		$strSqlConditions .= ( true == $boolExcludeAddOnsAttachedToUnit ) ? ' AND ao.unit_space_id IS NULL' :'';

		$strSqlConditions .= ( true == $boolIncludeIsSelectedQuote ) ? ' AND la.is_selected_quote IS TRUE' :'';

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aog.name AS add_on_group_name,
						aot.id AS add_on_type_id,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						lst.id AS lease_status_type_id,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aoc.id AS add_on_category_id,
						scr.charge_amount AS rent_amount,
						scr.ar_code_id AS rent_ar_code_id,
						scd.charge_amount AS deposit_amount,
						scd.ar_code_id AS deposit_ar_code_id,
						scrf.charge_amount AS other_amount,
						scrf.ar_code_id AS other_ar_code_id,
						scd.lease_term_months AS lease_term_month,
						util_get_system_translated( \'name\', aot.name, aot.details ) AS add_on_type_name,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN lease_intervals li ON ( la.cid = li.cid AND la.property_id = li.property_id AND la.lease_id = li.lease_id AND la.lease_interval_id = li.id )
						LEFT JOIN scheduled_charges scr ON ( scr.cid = la.cid AND scr.lease_id = la.lease_id AND scr.ar_origin_reference_id = la.ar_origin_reference_id AND scr.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scr.ar_trigger_id = ' . CArTrigger::MONTHLY . ' AND scr.lease_association_id = la.id AND scr.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scd ON ( scd.cid = la.cid AND scd.lease_id = la.lease_id AND scd.ar_origin_reference_id = la.ar_origin_reference_id AND scd.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scd.ar_trigger_id = ' . CArTrigger::MOVE_IN . ' AND scd.lease_association_id = la.id AND scd.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scrf ON ( scrf.cid = la.cid AND scrf.lease_id = la.lease_id AND scrf.ar_origin_reference_id = la.ar_origin_reference_id AND scrf.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ( scrf.ar_trigger_id = ' . CArTrigger::APPLICATION_COMPLETED . ' OR scrf.ar_trigger_id = ' . CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT . ' ) AND scrf.lease_association_id = la.id AND scrf.deleted_on IS NULL )
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_types aot ON ( aog.add_on_type_id = aot.id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id =' . ( int ) $intPropertyId . '
						AND la.lease_interval_id IN (' . implode( ',', $arrintLeaseIntervalIds ) . ')' . $strSqlConditions . '
						AND aog.add_on_type_id IN (' . implode( ',', $arrintAddOnTypeIds ) . ')
						AND la.deleted_on IS NULL';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public function fetchNewAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdsByAddOnTypeIdsByCid( $intPropertyId, $arrintLeaseIntervalIds, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsCheckDeleted = false, $boolExcludeAddOnsAttachedToUnit = false, $arrintLeaseStatusTypeIds = NULL, $boolIncludeIsSelectedQuote = true ) {
		if( false == valArr( $arrintAddOnTypeIds ) || false == valArr( $arrintLeaseIntervalIds ) ) {
			return NULL;
		}

		$strSqlConditions = '';

		if( true == valArr( $arrintLeaseStatusTypeIds ) ) {
			$strSqlConditions .= ' AND la.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ') ';
		}

		$strSqlConditions .= ( true == $boolIsCheckDeleted ) ? ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';

		$strSqlConditions .= ( true == $boolExcludeAddOnsAttachedToUnit ) ? ' AND ao.unit_space_id IS NULL' :'';

		$strSqlConditions .= ( true == $boolIncludeIsSelectedQuote ) ? ' AND la.is_selected_quote IS TRUE' :'';

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aog.name AS add_on_group_name,
						aot.id AS add_on_type_id,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS full_add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						lst.id AS lease_status_type_id,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aoc.id AS add_on_category_id,
						util_get_system_translated( \'name\', aot.name, aot.details ) AS add_on_type_name,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN lease_intervals li ON ( la.cid = li.cid AND la.property_id = li.property_id AND la.lease_id = li.lease_id AND la.lease_interval_id = li.id )
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_types aot ON ( aog.add_on_type_id = aot.id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id =' . ( int ) $intPropertyId . '
						AND la.lease_interval_id IN (' . implode( ',', $arrintLeaseIntervalIds ) . ')' . $strSqlConditions . '
						AND aog.add_on_type_id IN (' . implode( ',', $arrintAddOnTypeIds ) . ')
						AND la.deleted_on IS NULL';

		$arrobjAddOnLeaseAssociations   = parent::fetchLeaseAssociations( $strSql, $objDatabase );

		if( true == valArr( $arrobjAddOnLeaseAssociations ) ) {
			$strSql = 'SELECT
						sc.*,
						sc.lease_term_months AS lease_term_month
						FROM
							scheduled_charges sc
						WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.property_id =' . ( int ) $intPropertyId . '
						AND sc.lease_interval_id IN (' . implode( ',', $arrintLeaseIntervalIds ) . ' )
						AND sc.lease_association_id IN (' . implode( ',', array_keys( $arrobjAddOnLeaseAssociations ) ) . ' )
						AND sc.is_unselected_quote = FALSE
						AND sc.ar_trigger_id IN (' . implode( ',', CArTrigger::$c_arrintAddOnsArTriggers ) . ' )
						AND sc.deleted_on IS NULL
						ORDER BY
						sc.ar_trigger_id DESC';

			$arrobjAddOnScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledCharges( $strSql, $objDatabase );

			if( false == valArr( $arrobjAddOnScheduledCharges ) ) {
				return $arrobjAddOnLeaseAssociations;
			} else {
				foreach( $arrobjAddOnLeaseAssociations  as $objAddOnLeaseAssociation ) {
					$arrobjTempScheduledCharges = [];
					foreach( $arrobjAddOnScheduledCharges as $objAddOnScheduledCharge ) {
						if( $objAddOnLeaseAssociation->getId() == $objAddOnScheduledCharge->getLeaseAssociationId() ) {
							$arrobjTempScheduledCharges[$objAddOnScheduledCharge->getId()] = $objAddOnScheduledCharge;
						}
					}
					$objAddOnLeaseAssociation->setScheduledCharges( $arrobjTempScheduledCharges );
				}
			}
		}

		return $arrobjAddOnLeaseAssociations;
	}

	public static function fetchAddOnLeaseAssociationsScheduledChargesByQuoteIdsByAddOnTypeIdsByCid( $arrintQuoteIds, $arrintAddOnTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintQuoteIds ) ) return NULL;
		if( false == valArr( $arrintAddOnTypeIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )
						AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' )
						AND la.deleted_on IS NULL';

		$arrobjAddOnLeaseAssociations = self::fetchLeaseAssociations( $strSql, $objDatabase );

		if( true == valArr( $arrobjAddOnLeaseAssociations ) ) {
			$strSql = 'SELECT
						sc.*
						FROM
							scheduled_charges sc
						WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND sc.lease_association_id IN (' . implode( ',', array_keys( $arrobjAddOnLeaseAssociations ) ) . ' )
						AND sc.ar_trigger_id IN (' . implode( ',', CArTrigger::$c_arrintAddOnsArTriggers ) . ' )
						AND sc.deleted_on IS NULL
						ORDER BY
						sc.ar_trigger_id DESC';

			$arrobjAddOnScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledCharges( $strSql, $objDatabase );

			if( false == valArr( $arrobjAddOnScheduledCharges ) ) {
				return $arrobjAddOnLeaseAssociations;
			} else {
				foreach( $arrobjAddOnLeaseAssociations  as $objAddOnLeaseAssociation ) {
					$arrobjTempScheduledCharges = [];
					foreach( $arrobjAddOnScheduledCharges as $objAddOnScheduledCharge ) {
						if( $objAddOnLeaseAssociation->getId() == $objAddOnScheduledCharge->getLeaseAssociationId() ) {
							$arrobjTempScheduledCharges[$objAddOnScheduledCharge->getId()] = $objAddOnScheduledCharge;
						}
					}
					$objAddOnLeaseAssociation->setScheduledCharges( $arrobjTempScheduledCharges );
				}
			}
		}

		return $arrobjAddOnLeaseAssociations;
	}

	public static function fetchCustomAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdsByAddOnTypeIdsByCid( $intPropertyId, $arrintLeaseIntervalIds, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsCheckDeleted = false, $boolExcludeAddOnsAttachedToUnit = false, $arrintLeaseStatusTypeIds = NULL, $boolCheckIsSelectedQuote = true ) {
		if( false == valArr( $arrintAddOnTypeIds ) || false == valArr( $arrintLeaseIntervalIds ) ) {
			return NULL;
		}

		$strSqlConditions = '';

		if( true == valArr( $arrintLeaseStatusTypeIds ) ) {
			$strSqlConditions .= ' AND la.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ') ';
		}

		$strSqlConditions .= ( true == $boolIsCheckDeleted ) ? ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';

		$strSqlConditions .= ( true == $boolExcludeAddOnsAttachedToUnit ) ? ' AND ao.unit_space_id IS NULL' :'';

		$strSqlConditions .= ( true == $boolCheckIsSelectedQuote ) ? ' AND la.is_selected_quote IS TRUE' : '';

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aog.name AS add_on_group_name,
						aot.id AS add_on_type_id,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						ao.unit_space_id as unit_space_id,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						scr.charge_amount AS rent_amount,
						scr.ar_code_id AS rent_ar_code_id,
						scd.charge_amount AS deposit_amount,
						scd.ar_code_id AS deposit_ar_code_id,
						scrf.charge_amount AS other_amount,
						scrf.ar_code_id AS other_ar_code_id,
						scd.lease_term_months AS lease_term_month,
						util_get_system_translated( \'name\', aot.name, aot.details ) AS add_on_type_name
					FROM
						lease_associations la
						JOIN lease_intervals li ON ( la.cid = li.cid AND la.property_id = li.property_id AND la.lease_id = li.lease_id AND la.lease_interval_id = li.id )
						LEFT JOIN scheduled_charges scr ON ( scr.cid = la.cid AND scr.lease_id = la.lease_id AND scr.ar_origin_reference_id = la.ar_origin_reference_id AND scr.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scr.ar_trigger_id = ' . CArTrigger::MONTHLY . ' AND scr.lease_association_id = la.id AND scr.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scd ON ( scd.cid = la.cid AND scd.lease_id = la.lease_id AND scd.ar_origin_reference_id = la.ar_origin_reference_id AND scd.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scd.ar_trigger_id = ' . CArTrigger::MOVE_IN . ' AND scd.lease_association_id = la.id AND scd.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scrf ON ( scrf.cid = la.cid AND scrf.lease_id = la.lease_id AND scrf.ar_origin_reference_id = la.ar_origin_reference_id AND scrf.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ( scrf.ar_trigger_id = ' . CArTrigger::APPLICATION_COMPLETED . ' OR scrf.ar_trigger_id = ' . CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT . ' ) AND scrf.lease_association_id = la.id AND scrf.deleted_on IS NULL )
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_types aot ON ( aog.add_on_type_id = aot.id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id =' . ( int ) $intPropertyId . '
						AND la.lease_interval_id IN (' . implode( ',', $arrintLeaseIntervalIds ) . ')' . $strSqlConditions . '
						AND aog.add_on_type_id IN (' . implode( ',', $arrintAddOnTypeIds ) . ')';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchRenewalAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdsByAddOnTypeIdsByCid( $intPropertyId, $arrintLeaseIntervalIds, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsFetchDeleted = false, $boolIncludeGroupLeaseTerms = true, $boolCheckIsSelectedQuote = true ) {

		if( false == valId( $intPropertyId )
			|| false == valId( $intCid )
			|| false == valArr( $arrintLeaseIntervalIds )
			|| false == valArr( $arrintAddOnTypeIds ) ) return NULL;
		$strSqlConditions = '';
		$strSqlConditions .= ( true == $boolIsFetchDeleted ) ? '' : ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL';
		$strSqlConditions .= ( true == $boolCheckIsSelectedQuote ) ? ' AND la.is_selected_quote IS TRUE' : '';
		$strGroupLeaseTermCondition = ( true == $boolIncludeGroupLeaseTerms ) ? '' : ' AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )';

		$strSql = 'WITH
						ao
					AS(
							SELECT
								la.*,
								aog.id AS add_on_group_id,
								aog.name AS add_on_group_name,
								aog.name AS add_on_type_id,
								aog.remote_primary_key AS add_on_group_remote_primary_key,
								util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
								ao.remote_primary_key AS add_on_remote_primary_key,
								ao.available_on AS available_on,
								ao.unit_space_status_type_id,
								ao.unit_space_id AS unit_space_id,
								aog.add_on_type_id,
								util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
								util_get_system_translated( \'name\', aot.name, aot.details ) AS add_on_type_name
							FROM
								lease_associations la
								JOIN add_ons ao ON (
									ao.cid = la.cid AND ao.id = la.ar_origin_reference_id
									AND la.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
								)
								JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
								JOIN add_on_types aot ON ( aog.add_on_type_id = aot.id )
								JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
								JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
							WHERE
								la.cid = ' . ( int ) $intCid . '
								AND la.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CURRENT . ' )
								AND la.property_id = ' . ( int ) $intPropertyId . '
								AND la.lease_interval_id IN ( ' . sqlIntImplode( $arrintLeaseIntervalIds ) . ' )
								AND aog.add_on_type_id IN ( ' . sqlIntImplode( $arrintAddOnTypeIds ) . ' )
								' . $strSqlConditions . '
						),
					pcs AS (
							SELECT
								pcs.*
							FROM
								property_charge_settings pcs
							WHERE
								pcs.cid = ' . ( int ) $intCid . '
								AND pcs.property_id = ' . ( int ) $intPropertyId . '
							),
					lt AS (
							SELECT
								lt.*
							FROM
								pcs
								JOIN lease_terms lt ON (
									pcs.cid = lt.cid
									AND pcs.lease_term_structure_id = lt.lease_term_structure_id
									AND lt.is_renewal IS TRUE
									' . $strGroupLeaseTermCondition . '
								)
							WHERE
								lt.cid = ' . ( int ) $intCid . '
							),
					sc AS (
							SELECT
								sc.*,
								ac.round_type_id
							FROM
								scheduled_charges sc
								JOIN ao ON (
									sc.cid = ao.cid
									AND sc.lease_id = ao.lease_id
									AND sc.ar_origin_reference_id = ao.ar_origin_reference_id
									AND sc.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
									AND sc.ar_trigger_id IN ( ' . ( int ) CArTrigger::MONTHLY . ', ' . ( int ) CArTrigger::MOVE_IN . ', ' . ( int ) CArTrigger::APPLICATION_COMPLETED . ', ' . ( int ) CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT . ' )
									AND sc.lease_association_id = ao.id
								)
								JOIN ar_codes ac ON( ac.cid = sc.cid AND ac.id = sc.ar_code_id )
							WHERE
								sc.cid = ' . ( int ) $intCid . '
								AND sc.deleted_by IS NULL
							)
					SELECT
						la.*,
						rounder( scr.charge_amount, COALESCE( scr.round_type_id, pcs1.round_type_id ) ) AS rent_amount,
						scr.ar_code_id AS rent_ar_code_id,
						rounder( scd.charge_amount, COALESCE( scd.round_type_id, pcs1.round_type_id ) ) AS deposit_amount,
						scd.ar_code_id AS deposit_ar_code_id,
						rounder( scrf.charge_amount, COALESCE( scrf.round_type_id, pcs1.round_type_id ) ) AS other_amount,
						scrf.ar_code_id AS other_ar_code_id,
						ltr.term_month AS lease_term_month,
						ltr.name AS lease_term_name,
						ltr.id as lease_term_id,
						scr.id AS rent_scheduled_charge_id,
						scd.id AS deposit_scheduled_charge_id,
						scrf.id AS other_scheduled_charge_id,
						scr.rate_log_id AS rent_rate_log_id,
						scd.rate_log_id AS deposit_rate_log_id,
						scrf.rate_log_id AS other_rate_log_id
					FROM
						ao AS la
						JOIN pcs AS pcs1 ON( pcs1.cid = la.cid AND pcs1.property_id = la.property_id AND pcs1.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN sc As scr ON (
							scr.cid = la.cid
							AND scr.ar_code_type_id = ' . ( int ) CArCodeType::RENT . '
							AND scr.lease_association_id = la.id
						)
						LEFT JOIN lt AS ltr ON (
							scr.cid = ltr.cid
							AND ltr.term_month = scr.lease_term_months
						)
						LEFT JOIN sc AS scd ON (
							scd.cid = la.cid
							AND scd.ar_code_type_id = ' . ( int ) CArCodeType::DEPOSIT . '
							AND scd.lease_association_id = la.id
							AND ltr.term_month = scd.lease_term_months
						)
						LEFT JOIN sc AS scrf ON (
							scrf.cid = la.cid
							AND scrf.ar_code_type_id = ' . ( int ) CArCodeType::OTHER_INCOME . '
							AND scrf.lease_association_id = la.id
							AND ltr.term_month = scrf.lease_term_months
						)
					ORDER BY la.id ASC, lease_term_month ASC';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchTransferAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdsByAddOnTypeIdsByCid( $intPropertyId, $arrintLeaseIntervalIds, $arrintAddOnTypeIds, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId )
		 || false == valId( $intCid )
		 || false == valArr( $arrintLeaseIntervalIds )
		 || false == valArr( $arrintAddOnTypeIds ) ) return NULL;

		$strSql = 'WITH
						ao
					AS(
							SELECT
								la.*,
								aog.id AS add_on_group_id,
								aog.name AS add_on_group_name,
								aog.name AS add_on_type_id,
								aog.remote_primary_key AS add_on_group_remote_primary_key,
								util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
								ao.remote_primary_key AS add_on_remote_primary_key,
								ao.available_on AS available_on,
								ao.unit_space_status_type_id,
								ao.unit_space_id AS unit_space_id,
								aog.add_on_type_id,
								util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
								util_get_system_translated( \'name\', aot.name, aot.details ) AS add_on_type_name
							FROM
								lease_associations la
								JOIN add_ons ao ON (
									ao.cid = la.cid AND ao.id = la.ar_origin_reference_id
									AND la.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
								)
								JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
								JOIN add_on_types aot ON ( aog.add_on_type_id = aot.id )
								JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
								JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
							WHERE
								la.cid = ' . ( int ) $intCid . '
								AND la.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CURRENT . ' )
								AND la.deleted_on IS NULL
								AND la.property_id = ' . ( int ) $intPropertyId . '
								AND la.lease_interval_id IN ( ' . sqlIntImplode( $arrintLeaseIntervalIds ) . ' )
								AND aog.add_on_type_id IN ( ' . sqlIntImplode( $arrintAddOnTypeIds ) . ' )
						),
					pcs AS (
							SELECT
								pcs.*
							FROM
								property_charge_settings pcs
							WHERE
								pcs.cid = ' . ( int ) $intCid . '
								AND pcs.property_id = ' . ( int ) $intPropertyId . '
							),
					lt AS (
							SELECT
								lt.*
							FROM
								pcs
								JOIN lease_terms lt ON (
									pcs.cid = lt.cid
									AND pcs.lease_term_structure_id = lt.lease_term_structure_id
									AND lt.is_renewal IS TRUE
								)
							WHERE
								lt.cid = ' . ( int ) $intCid . '
							),
					sc AS (
							SELECT
								sc.*
							FROM
								scheduled_charges sc
								JOIN ao ON (
									sc.cid = ao.cid
									AND sc.lease_id = ao.lease_id
									AND sc.ar_origin_reference_id = ao.ar_origin_reference_id
									AND sc.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
									AND sc.ar_trigger_id IN ( ' . ( int ) CArTrigger::MONTHLY . ', ' . ( int ) CArTrigger::MOVE_IN . ', ' . ( int ) CArTrigger::APPLICATION_COMPLETED . ', ' . ( int ) CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT . ' )
									AND sc.lease_association_id = ao.id
								)
							WHERE
								sc.cid = ' . ( int ) $intCid . '
								AND sc.deleted_by IS NULL
							)
					SELECT
						la.*,
						scr.charge_amount AS rent_amount,
						scr.ar_code_id AS rent_ar_code_id,
						scd.charge_amount AS deposit_amount,
						scd.ar_code_id AS deposit_ar_code_id,
						scrf.charge_amount AS other_amount,
						scrf.ar_code_id AS other_ar_code_id,
						ltr.term_month AS lease_term_month,
						ltr.name AS lease_term_name,
						ltr.id as lease_term_id,
						scr.id AS rent_scheduled_charge_id,
						scd.id AS deposit_scheduled_charge_id,
						scrf.id AS other_scheduled_charge_id,
						scr.rate_log_id AS rent_rate_log_id,
						scd.rate_log_id AS deposit_rate_log_id,
						scrf.rate_log_id AS other_rate_log_id
					FROM
						ao AS la
						LEFT JOIN sc As scr ON (
							scr.cid = la.cid
							AND scr.ar_code_type_id = ' . ( int ) CArCodeType::RENT . '
							AND scr.lease_association_id = la.id
						)
						LEFT JOIN lt AS ltr ON (
							scr.cid = ltr.cid
							AND ltr.term_month = scr.lease_term_months
						)
						LEFT JOIN sc AS scd ON (
							scd.cid = la.cid
							AND scd.ar_code_type_id = ' . ( int ) CArCodeType::DEPOSIT . '
							AND scd.lease_association_id = la.id
							AND ltr.term_month = scd.lease_term_months
						)
						LEFT JOIN sc AS scrf ON (
							scrf.cid = la.cid
							AND scrf.ar_code_type_id = ' . ( int ) CArCodeType::OTHER_INCOME . '
							AND scrf.lease_association_id = la.id
							AND ltr.term_month = scrf.lease_term_months
						)
					ORDER BY la.id ASC, lease_term_month ASC';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdByLeaseStatusTypeIdsByCid( $intPropertyId, $intLeaseIntervalId, $intCid, $objDatabase, $arrintLeaseStatusTypeIds ) {

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						ao.unit_space_id as unit_space_id,
						aog.add_on_type_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.property_id=' . ( int ) $intPropertyId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id =' . ( int ) $intLeaseIntervalId . ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL';

		if( true == valArr( $arrintLeaseStatusTypeIds ) ) {
			$strSql .= ' AND la.lease_status_type_id IN (' . implode( ',', $arrintLeaseStatusTypeIds ) . ')';
		}

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchActiveAddOnLeaseAssociationsByLeaseIntervalIdByAddOnTypeIdsByCid( $intLeaseIntervalId, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolExcludeAddOnsAttachedToUnit = false ) {

		$strExcludeAddOnsAttachedToUnit = '';
		if( true == $boolExcludeAddOnsAttachedToUnit ) {
			$strExcludeAddOnsAttachedToUnit = ' AND ao.unit_space_id is NULL';
		}

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.deleted_on IS NULL' . $strExcludeAddOnsAttachedToUnit . '
						AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' ) ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByPropertyIdByQuoteIdByAddOnTypeIdsByCid( $intPropertyId, $intQuoteId, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsCheckDeleted = false, $boolExcludeAddOnAttachedToUnit = false ) {

		if( false == valArr( $arrintAddOnTypeIds ) ) {
			return NULL;
		}

		$strDeleteConditions = ( true == $boolIsCheckDeleted ) ? ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';

		$strAddOnAttachedToUnit = ( true == $boolExcludeAddOnAttachedToUnit ) ? ' AND ao.unit_space_id IS NULL' :'';

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						ao.unit_space_id as unit_space_id,
						aog.add_on_type_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id =' . ( int ) $intPropertyId . '
						AND la.quote_id =' . ( int ) $intQuoteId . '
						AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' ) '
									. $strDeleteConditions . $strAddOnAttachedToUnit;

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByQuoteIdsByAddOnTypeIdsByCid( $arrintQuoteIds, $arrintAddOnTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintQuoteIds ) ) return NULL;
		if( false == valArr( $arrintAddOnTypeIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						scr.charge_amount AS rent_amount,
						scr.ar_code_id AS rent_ar_code_id,
						scd.charge_amount AS deposit_amount,
						scd.ar_code_id AS deposit_ar_code_id,
						scrf.charge_amount AS other_amount,
						scrf.ar_code_id AS other_ar_code_id,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
						LEFT JOIN scheduled_charges scr ON ( scr.cid = la.cid AND scr.lease_association_id = la.id AND scr.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scr.ar_trigger_id = ' . CArTrigger::MONTHLY . ' )
						LEFT JOIN scheduled_charges scd ON ( scd.cid = la.cid AND scd.lease_association_id = la.id AND scd.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scd.ar_trigger_id = ' . CArTrigger::MOVE_IN . ' )
						LEFT JOIN scheduled_charges scrf ON ( scrf.cid = la.cid AND scrf.lease_association_id = la.id AND scrf.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ( scrf.ar_trigger_id = ' . CArTrigger::APPLICATION_COMPLETED . ' OR scrf.ar_trigger_id = ' . CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT . ' ) )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )
						AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' )
						AND la.deleted_on IS NULL';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchNewAddOnLeaseAssociationsByQuoteIdsByAddOnTypeIdsByCid( $arrintQuoteIds, $arrintAddOnTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintQuoteIds ) ) return NULL;
		if( false == valArr( $arrintAddOnTypeIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.quote_id IN ( ' . implode( ',', $arrintQuoteIds ) . ' )
						AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' )
						AND la.deleted_on IS NULL';

		$arrobjAddOnLeaseAssociations = parent::fetchLeaseAssociations( $strSql, $objDatabase );

		if( true == valArr( $arrobjAddOnLeaseAssociations ) ) {
			$strSql = 'SELECT
						sc.*,
						sc.lease_term_months AS lease_term_month
						FROM
							scheduled_charges sc
						WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_association_id IN (' . implode( ',', array_keys( $arrobjAddOnLeaseAssociations ) ) . ' )
						AND sc.ar_trigger_id IN (' . implode( ',', CArTrigger::$c_arrintAddOnsArTriggers ) . ' )
						AND sc.deleted_on IS NULL
						ORDER BY
						sc.ar_trigger_id DESC';

			$arrobjAddOnScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledCharges( $strSql, $objDatabase );

			if( false == valArr( $arrobjAddOnScheduledCharges ) ) {
				return $arrobjAddOnLeaseAssociations;
			} else {
				foreach( $arrobjAddOnLeaseAssociations as $objAddOnLeaseAssociation ) {
					$arrobjTempScheduledCharges = [];
					foreach( $arrobjAddOnScheduledCharges as $objAddOnScheduledCharge ) {
						if( $objAddOnLeaseAssociation->getId() == $objAddOnScheduledCharge->getLeaseAssociationId() ) {
							$arrobjTempScheduledCharges[$objAddOnScheduledCharge->getId()] = $objAddOnScheduledCharge;
						}
					}
					$objAddOnLeaseAssociation->setScheduledCharges( $arrobjTempScheduledCharges );
				}
			}
		}

		return $arrobjAddOnLeaseAssociations;
	}

	public static function fetchAddOnLeaseAssociationsByQuoteIdByCid( $intQuoteId, $intCid, $objDatabase ) {

		$strSql = '	SELECT	la.*,
							aog.name AS add_on_group_name,
							util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
							util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
							aog.add_on_type_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
				   WHERE la.cid = ' . ( int ) $intCid . '
				   		 AND la.quote_id = ' . ( int ) $intQuoteId;

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	// Count functions

	public static function fetchPendingAddOnLeaseAssociationsCountByAddOnTypeIdsByCid( $arrintSelectedPropertyIds, $boolMoveIn, $intCid, $arrintAddOnTypeIds, $objDatabase ) {

		$strSql = 'SELECT
					count( DISTINCT la.id )
				FROM
					lease_associations la
					JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
					JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
					JOIN lease_intervals li ON ( li.cid = la.cid AND li.id = la.lease_interval_id )
				WHERE
					la.cid = ' . ( int ) $intCid . '
					AND la.deleted_on IS NULL';

		if( true == valArr( $arrintSelectedPropertyIds ) ) {
			$strSql .= ' AND la.property_id IN (' . implode( ',', $arrintSelectedPropertyIds ) . ' ) ';
		}

		if( true == $boolMoveIn ) {
			$strSql .= ' AND la.lease_status_type_id = ' . CLeaseStatusType::FUTURE . '
						 AND li.lease_status_type_id != ' . CLeaseStatusType::FUTURE . '
						 AND la.start_date <= NOW() ';
		} else {
			$strSql .= ' AND la.lease_status_type_id IN ( ' . CLeaseStatusType::NOTICE . ' )
						 AND la.end_date < NOW() ';
		}

		$strSql .= ' AND aog.add_on_type_id IN (' . implode( ',', $arrintAddOnTypeIds ) . ')';

		$arrintAddOns = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintAddOns[0]['count'] ) ) return $arrintAddOns[0]['count'];

		return 0;
	}

	public static function fetchAddOnLeaseAssociationsCountByLeaseIdByAddOnTypeIdByCid( $intLeaseId, $intAddOnTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT( * ) AS total_assignable_items
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
					WHERE
						aog.add_on_type_id = ' . ( int ) $intAddOnTypeId . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.cid =  ' . ( int ) $intCid . '
						AND la.lease_status_type_id <> ' . CLeaseStatusType::PAST . '
						AND la.deleted_on IS NULL';

		return self::fetchColumn( $strSql, 'total_assignable_items', $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsCountByLeaseIdsByAddOnTypeIdByCid( $arrintLeaseIds, $intAddOnTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT( la.* ) AS total_assignable_items,
						l.id as lease_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
						LEFT JOIN leases l ON ( l.cid = la.cid AND l.id = la.lease_id )
					WHERE
						aog.add_on_type_id = ' . ( int ) $intAddOnTypeId . '
						AND la.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND la.cid = ' . ( int ) $intCid . '
						AND la.lease_status_type_id <> ' . CLeaseStatusType::PAST . '
						AND la.deleted_on IS NULL
					GROUP BY l.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsCountByLeaseIdByMoveOutDateByCid( $intLeaseId, $strMoveOutDate, $intCid, $objDatabase ) {

		$strWhere = 'WHERE lease_id = ' . ( int ) $intLeaseId . '
						AND ( move_out_date IS NULL OR move_out_date > \'' . $strMoveOutDate . '\')
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL';

		return self::fetchLeaseAssociationCount( $strWhere, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsCountByAddOnIdByCid( $intArOriginReferenceId, $intCid, $objDatabase, $boolIsCheckDeleted = false ) {
		$strDeleteConditions = ( true == $boolIsCheckDeleted ) ? ' AND deleted_on IS NULL AND deleted_by IS NULL' : '';

		$strWhere = 'WHERE lease_status_type_id IN (' . implode( ',', CLeaseStatusType::$c_arrintActiveAddOnReservationStatusTypes ) . ' )
					 AND ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
					 AND cid = ' . ( int ) $intCid . '
					 AND ((quote_id IS NOT NULL AND is_selected_quote = TRUE) OR (quote_id IS NULL)) ' . $strDeleteConditions;

		return self::fetchLeaseAssociationCount( $strWhere, $objDatabase );
	}

	// This function is for new dashboard

	public static function fetchAddOnsMoveInCountByDashboardFilterByAddOnTypeIdsByCid( $objDashboardFilter, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valArr( $arrintAddOnTypeIds ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
						COUNT( id ) AS ' .
							( false == $boolIsGroupByProperties ?
						' count, MAX'
									:
						' residents_rentable_items,
						property_id,
						property_name,
						' ) . '
						(priority) AS priority
					FROM (
						SELECT *
						FROM (
							SELECT
								la.id,
								la.cid,
								' . ( true == $boolIsGroupByProperties ? '
								la.property_id,
								p.property_name, '
									: '' ) . '
								CASE
									WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' )::int ) >= la.start_date THEN
										3
									WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'within\'
										AND (
												( la.start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) )
												OR ( \'past\' = dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' AND ( la.start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										3
									WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' )::int ) >= la.start_date THEN
										2
									WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'within\'
										AND (
												( la.start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) )
												OR ( \'past\' = dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' AND ( la.start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										2
									ELSE
										1
								END AS priority
							FROM
								lease_associations la
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = la.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
								' . ( true == $boolIsGroupByProperties ? ' JOIN properties p ON ( p.cid = la.cid AND p.id = la.property_id )' : '' ) . '
								JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ao.unit_space_id IS NULL )
								JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
								JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
								JOIN cached_leases cl ON ( la.cid = cl.cid AND cl.id = la.lease_id )
								LEFT JOIN dashboard_priorities dp ON dp.cid = la.cid
							WHERE
								la.cid = ' . ( int ) $intCid . '
								AND la.deleted_on IS NULL
								AND la.lease_status_type_id = ' . CLeaseStatusType::FUTURE . '
								AND cl.lease_status_type_id != ' . CLeaseStatusType::FUTURE . '
								AND la.start_date <= NOW ( )
								AND ( aoc.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' ) OR ( ' . CDefaultAddOnCategory::OTHER . ' = aoc.default_add_on_category_id AND aoc.add_on_type_id IS NULL AND aog.add_on_type_id = ' . CAddOnType::RENTABLE_ITEMS . ' ) )
						) AS lease_association
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere .
							( false == $boolIsGroupByProperties ? '
						ORDER BY
							priority DESC
						LIMIT 100 '
							: '' ) . '
					) AS lease_count ' .
						( true == $boolIsGroupByProperties ? '
					GROUP BY
						property_id,
						property_name,
						priority'
						  : '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAddOnsMoveOutCountByDashboardFilterByAddOnTypeIdsByCid( $objDashboardFilter, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0, $intLookAheadDays = 0, $boolIncludeMonthToMonthLeases = true ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valArr( $arrintAddOnTypeIds ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		if( false == $boolIncludeMonthToMonthLeases ) {
			$strMonthToMonthCondition = ' AND cl.is_month_to_month <> 1';
		}

		$strSql .= 'SELECT
						COUNT( id ) AS ' .
		           ( false == $boolIsGroupByProperties ? '
						count, MAX'
			           : '
						residents_rentable_items,
						property_id,
						property_name,
							' ) . '
						(priority) AS priority
					FROM (
						SELECT *
						FROM (
							SELECT
								la.id,
								la.cid,
									' . ( true == $boolIsGroupByProperties ? '
								la.property_id,
								p.property_name, '
				: '' ) . '
								CASE
									WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' )::int ) >= la.end_date THEN
										3
									WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'within\'
										AND (
												( la.end_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) )
												OR ( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( la.end_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										3
									WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' )::int ) >= la.end_date THEN
										2
									WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'within\'
										AND (
												( la.end_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) )
												OR ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( la.end_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										2
									ELSE
										1
								END AS priority
							FROM
								lease_associations la
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = la.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
								' . ( true == $boolIsGroupByProperties ? ' JOIN properties p ON ( p.cid = la.cid AND p.id = la.property_id )' : '' ) . '
								JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ao.unit_space_id IS NULL )
								JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
								JOIN cached_leases cl ON ( la.cid = cl.cid AND cl.id = la.lease_id )
								LEFT JOIN dashboard_priorities dp ON dp.cid = la.cid
							WHERE
								la.cid = ' . ( int ) $intCid . '
								AND la.deleted_on IS NULL
								 AND la.lease_status_type_id IN ( ' . CLeaseStatusType::NOTICE . ', ' . CLeaseStatusType::CURRENT . ' )
								 AND ( la.end_date <= NOW( ) + INTERVAL \'' . ( int ) $intLookAheadDays . ' DAYS\' ) ' . $strMonthToMonthCondition . '
								AND ao.unit_space_id IS NULL
								AND aog.add_on_type_id IN (' . implode( ',', $arrintAddOnTypeIds ) . ')
						) AS lease_association
						WHERE
							cid = ' . ( int ) $intCid .
		           $strPriorityWhere .
		           ( false == $boolIsGroupByProperties ? '
						ORDER BY
							priority DESC
						LIMIT
							100 '
			           : '' ) . '
					) AS lease_association_count ' .
		           ( true == $boolIsGroupByProperties ? '
					GROUP BY
						property_id,
						property_name,
						priority'
			           : '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedAddOnsMoveInByDashboardFilterByAddOnTypeIdsCid( $objDashboardFilter, $arrintAddOnTypeIds, $intCid, $intOffset, $intPageSize, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valArr( $arrintAddOnTypeIds ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strPrioritySql = ', CASE WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' )::int ) >= la.start_date THEN
								3
							WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'within\'
								AND (
										( la.start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) )
										OR ( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( la.start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
									) THEN
								3
							WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' )::int ) >= la.start_date THEN
								2
							WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'within\'
								AND (
										( la.start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) )
										OR ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( la.start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
									) THEN
								2
							ELSE
								1
							END AS priority';

		$strSql = 'SELECT *
					FROM (
						SELECT
							la.cid,
							la.id,
							la.lease_id,
							la.property_id,
							la.customer_id,
							la.quote_id,
							la.is_selected_quote,
							func_format_customer_name ( cl.name_first, cl.name_last ) AS customer_full_name,
							cl.primary_phone_number as phone_number,
					 		COALESCE ( aog.name || \'( \' || ao.name || \' )\', aog.name, ao.name, NULL ) AS item_name,
					 		CASE WHEN la.start_date = DATE_TRUNC(\'day\', NOW() )
					   			THEN \'Today\'
					 			ELSE to_char( la.start_date, \'MM/DD/YY\' )
					 		END as start_date,
							p.property_name
							' . $strPrioritySql . '
						 FROM
							 lease_associations la
							 JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = la.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							 JOIN properties p ON ( la.cid = p.cid AND la.property_id = p.id )
							 JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ao.unit_space_id IS NULL )
							 JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
							 JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
							 JOIN cached_leases cl ON ( la.cid = cl.cid AND cl.id = la.lease_id )
							 LEFT JOIN dashboard_priorities dp ON dp.cid = la.cid
						 WHERE
							 la.cid = ' . ( int ) $intCid . '
							 AND cl.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )
							 AND la.deleted_on IS NULL
							 AND ( aoc.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' ) OR ( ' . CDefaultAddOnCategory::OTHER . ' = aoc.default_add_on_category_id AND aoc.add_on_type_id IS NULL AND aog.add_on_type_id = ' . CAddOnType::RENTABLE_ITEMS . ' ) )
							 AND la.lease_status_type_id = ' . CLeaseStatusType::FUTURE . '
							 AND cl.lease_status_type_id != ' . CLeaseStatusType::FUTURE . '
							 AND la.start_date <= NOW ( )
					) AS lease_association
					WHERE
						cid = ' . ( int ) $intCid . '
						 AND ( ( quote_id IS NOT NULL AND is_selected_quote = TRUE) OR ( quote_id IS NULL ) )
						' . $strPriorityWhere . '
					ORDER BY
						' . $strSortBy . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedAddOnsMoveOutByDashboardFilterByAddOnTypeIdsByCid( $objDashboardFilter, $arrintAddOnTypeIds, $intCid, $intOffset, $intPageSize, $strSortBy, $objDatabase, $intLookAheadDays, $boolIncludeMonthToMonthLeases ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valArr( $arrintAddOnTypeIds ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strPrioritySql = ', CASE WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' )::int ) >= la.end_date THEN
								3
							WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'within\'
								AND (
										( la.end_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) )
										OR ( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( la.end_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
									) THEN
								3
							WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' )::int ) >= la.end_date THEN
								2
							WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'within\'
								AND (
										( la.end_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) )
										OR ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( la.end_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
									) THEN
								2
							ELSE
								1
							END AS priority';

		if( false == $boolIncludeMonthToMonthLeases ) {
			$strMonthToMonthCondition = ' AND cl.is_month_to_month <> 1';
		}

		$strSql = 'SELECT *
						FROM (
							SELECT
								la.id,
								la.cid,
								la.lease_id,
								la.property_id,
								la.customer_id,
								la.lease_status_type_id,
								ao.id AS add_on_id,
								aog.id AS add_on_group_id,
								func_format_customer_name ( cl.name_first, cl.name_last ) AS customer_full_name,
								cl.primary_phone_number as phone_number,
						 		COALESCE ( aog.name || \'( \' || ao.name || \' )\', aog.name, ao.name, NULL ) AS item_name,
						 		CASE WHEN la.end_date = DATE_TRUNC(\'day\', NOW() )
						   			THEN \'Today\'
						 			ELSE to_char( la.end_date, \'MM/DD/YY\' )
						 		END as end_date,
								p.property_name
								' . $strPrioritySql . '
							 FROM
								 lease_associations la
								 JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = la.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
								 JOIN properties p ON ( la.cid = p.cid AND la.property_id = p.id )
								 JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ao.unit_space_id IS NULL )
								 JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
								 JOIN cached_leases cl ON ( la.cid = cl.cid AND cl.id = la.lease_id )
								 LEFT JOIN dashboard_priorities dp ON dp.cid = la.cid
								 LEFT JOIN lease_intervals li ON (la.cid = li.cid AND la.id = li.lease_id AND li.lease_interval_type_id NOT IN( ' . CLeaseIntervalType::LEASE_MODIFICATION . ' ) AND li.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' ) )
							 WHERE
								 la.cid = ' . ( int ) $intCid . '
								 AND cl.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )
								 ' . $strMonthToMonthCondition . '
								 AND la.deleted_on IS NULL
								 AND aog.add_on_type_id IN ( ' . implode( ',', $arrintAddOnTypeIds ) . ' )
								 AND la.lease_status_type_id IN ( ' . CLeaseStatusType::NOTICE . ', ' . CLeaseStatusType::CURRENT . ' )
								 AND ( la.end_date <= NOW( ) + INTERVAL \'' . ( int ) $intLookAheadDays . ' DAYS\' )
								 AND ao.unit_space_id IS NULL
						) AS lease_association
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere . '
						ORDER BY
							' . $strSortBy . '
						OFFSET
							' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objDatabase );
	}

	// ByPropertyId

	public static function fetchIntegratedAdOnLeaseAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM lease_associations WHERE remote_primary_key IS NOT NULL AND ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND property_id= ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	// misc functions

	public static function fetchConflictingAddOnLeaseAssociationByAddOnIdByLeaseAssociationStartDateByCid( $intArOriginReferenceId, $strStartDate, $intCid, $objDatabase, $intAddOnLeaseAssociationId = NULL ) {

		$strSql = 'SELECT
					   	*
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ao.is_active = true )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.start_date <= \'' . $strStartDate . '\'
						AND ( la.end_date >= \'' . $strStartDate . '\' OR la.end_date IS NULL )
						AND la.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
					   	AND la.lease_id IS NOT NULL
						AND ( la.quote_id IS NULL OR ( la.quote_id IS NOT NULL AND la.is_selected_quote = TRUE ) )
					   	AND la.lease_status_type_id IN (' . implode( ',', CLeaseStatusType::$c_arrintActiveAddOnReservationStatusTypes ) . ')
					   	AND la.deleted_on IS NULL';

		if( false == is_null( $intAddOnLeaseAssociationId ) ) {
			$strSql .= ' AND la.id !=' . ( int ) $intAddOnLeaseAssociationId;
		}

		$strSql .= ' ORDER BY la.id DESC LIMIT 1';

		return self::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public static function fetchConflictingAddOnLeaseAssociationByAddOnIdByLeaseAssociationStartDateByEndDateByCid( $intAddOnId, $strStartDate, $strEndDate, $intCid, $objDatabase, $intAddOnLeaseAssociationId = NULL ) {

		$strEndDateSql = ( true == valStr( $strEndDate ) ) ? ' AND la.start_date <= \'' . $strEndDate . '\'' : '';

		$strSql = 'SELECT
					   	*
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.start_date > \'' . $strStartDate . '\'
						' . $strEndDateSql . '
						AND la.ar_origin_reference_id = ' . ( int ) $intAddOnId . '
						AND la.lease_status_type_id IN (' . implode( ',', CLeaseStatusType::$c_arrintActiveAddOnReservationStatusTypes ) . ')
						AND la.deleted_on IS NULL
						AND ao.is_active = true
						AND la.lease_id IS NOT NULL';

		if( false == is_null( $intAddOnLeaseAssociationId ) ) {
			$strSql .= ' AND la.id !=' . ( int ) $intAddOnLeaseAssociationId;
		}

		$strSql .= ' ORDER BY la.id ASC LIMIT 1';

		return self::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public static function fetchUnreturnedAddOnLeaseAssociationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.add_on_type_id,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						util_get_translated( \'description\', ao.description, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS description
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND aog.add_on_type_id = ' . CAddOnType::ASSIGNABLE_ITEMS . '
						AND la.lease_status_type_id != ' . CLeaseStatusType::PAST . '
						AND la.deleted_on IS Null';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchUnreturnedAddOnLeaseAssociationsByLeaseIdByLeaseStatusTypeIdByCid( $intLeaseId, $intLeaseStatusTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						la.*,
						aog.name AS add_on_group_name,
						aog.add_on_type_id,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ao.description AS description
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN scheduled_charges sc ON ( sc.lease_association_id = la.id AND sc.cid = la.cid )
					WHERE
						la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND aog.add_on_type_id = ' . CAddOnType::ASSIGNABLE_ITEMS . '
						AND la.lease_status_type_id = ' . ( int ) $intLeaseStatusTypeId . '
						AND la.deleted_on IS Null
						AND sc.deleted_on IS Null';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByUnitSpaceIdByLeaseIdByPropertyIdByCid( $intUnitSpaceId, $intLeaseId, $intPropertyId, $intCid, $arrintLeaseStatusTypeIds, $objDatabase, $intAddOnId = NULL ) {

		if( false == valArr( $arrintLeaseStatusTypeIds ) ) return NULL;

		$strInnerSql 	= ( NULL != $intLeaseId ) ? ' AND la.lease_id = ' . ( int ) $intLeaseId . ' ' : '';
		$strInnerSql 	.= ( NULL != $intAddOnId ) ? ' AND ao.id=' . ( int ) $intAddOnId : '';

		$strSql  = 'SELECT
						la.*,
						aog.name as add_on_group_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.id = la.ar_origin_reference_id AND ao.cid = la.cid )
						JOIN add_on_groups aog ON ( aog.id = ao.add_on_group_id AND aog.cid = ao.cid )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id=' . ( int ) $intPropertyId . '
						AND ao.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND la.lease_status_type_id IN (' . implode( ',', $arrintLeaseStatusTypeIds ) . ')
						AND la.deleted_on IS NULL ' . $strInnerSql;

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchActiveAddonLeaseAssociationsByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseIntervalId ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						aog.name as add_on_group_name
				  FROM add_ons ao
						JOIN add_on_groups aog ON ( ao.add_on_group_id = aog.id AND ao.cid = aog.cid )
						JOIN lease_associations la ON (la.cid = ao.cid AND la.ar_origin_reference_id = ao.id)
				  WHERE ao.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
						AND la.deleted_by IS NULL
						AND la.deleted_on IS NULL';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchCustomAddOnLeaseAssociationsByAddOnIdsByCid( $arrintAddOnIds, $intCid,  $objDatabase ) {

		if( false == valArr( $arrintAddOnIds ) ) return NULL;

		$strSql = 'SELECT la.*
					FROM
						lease_associations as la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.ar_origin_reference_id IN( ' . implode( ',', $arrintAddOnIds ) . ' )';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByAddOnIdsByExcludingLeaseIdByPropertyIdByCid( $arrintAddOnIds, $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql  = 'SELECT
						la.*
					FROM lease_associations la
						 LEFT JOIN cached_leases cl ON ( la.lease_id = cl.id AND la.cid = cl.cid )
						 JOIN add_ons ao ON ( ao.id = la.ar_origin_reference_id AND ao.cid = la.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.lease_status_type_id IN (' . implode( ',', CLeaseStatusType::$c_arrintActiveAddOnReservationStatusTypes ) . ')
						AND la.lease_id <> ' . ( int ) $intLeaseId . '
						AND la.ar_origin_reference_id IN ( ' . implode( ',', $arrintAddOnIds ) . ' )
						AND la.deleted_on IS NULL
						AND ao.is_active = true
						AND ao.unit_space_id IS NULL';
		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql  = 'SELECT
						la.*,
						aoc.id AS add_on_category_id,
						aog.id AS add_on_group_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.id = la.ar_origin_reference_id AND ao.cid = la.cid AND ao.unit_space_id IS NULL )
						JOIN add_on_groups aog ON ( aog.id = ao.add_on_group_id AND aog.cid = ao.cid )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.lease_id = ' . ( int ) $intLeaseId . '
						AND	la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.property_id=' . ( int ) $intPropertyId . '
						AND la.deleted_on IS NULL';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByPropertyIdsByLeaseIntervalIdsByAddOnTypeIdsByCid( $arrintPropertyIds, $arrintLeaseIntervalIds, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsCheckDeleted = false, $boolExcludeAddOnsAttachedToUnit = false, $arrintLeaseStatusTypeIds = NULL, $boolCheckIsSelectedQuote = true ) {
		if( false == valArr( $arrintAddOnTypeIds ) || false == valArr( $arrintLeaseIntervalIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSqlConditions = '';

		if( true == valArr( $arrintLeaseStatusTypeIds ) ) {
			$strSqlConditions .= ' AND la.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ') ';
		}

		$strSqlConditions .= ( true == $boolIsCheckDeleted ) ? ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';

		$strSqlConditions .= ( true == $boolExcludeAddOnsAttachedToUnit ) ? ' AND ao.unit_space_id IS NULL' :'';

		$strSqlConditions .= ( true == $boolCheckIsSelectedQuote ) ? ' AND la.is_selected_quote IS TRUE' : '';

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aog.name AS add_on_group_name,
						aot.id AS add_on_type_id,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ce.name_first || \' \' || ce.name_last AS leasing_agent_name,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						aog.add_on_type_id,
						util_get_system_translated( \'name\', lst.name, lst.details ) AS lease_status_type_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						rounder( scr.charge_amount, COALESCE( acr.round_type_id, pcs.round_type_id ) ) AS rent_amount,
						scr.ar_code_id AS rent_ar_code_id,
						rounder( scd.charge_amount, COALESCE( acd.round_type_id, pcs.round_type_id ) ) AS deposit_amount,
						scd.ar_code_id AS deposit_ar_code_id,
						rounder( scrf.charge_amount, COALESCE( acrf.round_type_id, pcs.round_type_id ) ) AS other_amount,
						scrf.ar_code_id AS other_ar_code_id,
						scd.lease_term_months AS lease_term_month,
						util_get_system_translated( \'name\', aot.name, aot.details ) AS add_on_type_name,
						util_get_translated( \'name\', lt.name, lt.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS lease_term_name,
						CASE WHEN ao.unit_space_id IS NULL
							 THEN false
							 ELSE true
						END as is_attached_to_unit
					FROM
						lease_associations la
						JOIN property_charge_settings pcs ON( pcs.cid = la.cid AND pcs.property_id = la.property_id AND pcs.cid = ' . ( int ) $intCid . ' )
						JOIN lease_intervals li ON ( la.cid = li.cid AND la.property_id = li.property_id AND la.lease_id = li.lease_id AND la.lease_interval_id = li.id )
						JOIN lease_terms lt ON ( li.cid = lt.cid AND li.lease_term_id = lt.id AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL ) )
						LEFT JOIN scheduled_charges scr ON ( scr.cid = la.cid AND scr.lease_id = la.lease_id AND scr.ar_origin_reference_id = la.ar_origin_reference_id AND scr.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scr.ar_code_type_id = ' . CArCodeType::RENT . ' AND scr.lease_association_id = la.id )
						LEFT JOIN scheduled_charges scd ON ( scd.cid = la.cid AND scd.lease_id = la.lease_id AND scd.ar_origin_reference_id = la.ar_origin_reference_id AND scd.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scd.ar_code_type_id = ' . CArCodeType::DEPOSIT . ' AND scd.lease_association_id = la.id )
						LEFT JOIN scheduled_charges scrf ON ( scrf.cid = la.cid AND scrf.lease_id = la.lease_id AND scrf.ar_origin_reference_id = la.ar_origin_reference_id AND scrf.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scrf.ar_code_type_id = ' . CArCodeType::OTHER_INCOME . ' AND scrf.lease_association_id = la.id )
						LEFT JOIN ar_codes acr ON ( acr.cid = pcs.cid AND acr.id = scr.ar_code_id )
						LEFT JOIN ar_codes acd ON ( acd.cid = pcs.cid AND acd.id = scd.ar_code_id )
						LEFT JOIN ar_codes acrf ON ( acrf.cid = pcs.cid AND acrf.id = scrf.ar_code_id )
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_types aot ON ( aog.add_on_type_id = aot.id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ')
						AND la.lease_interval_id IN (' . sqlIntImplode( $arrintLeaseIntervalIds ) . ')' . $strSqlConditions . '
						AND aog.add_on_type_id IN (' . sqlIntImplode( $arrintAddOnTypeIds ) . ')';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByLeaseIntervalIdExcludingQuoteIdByCid( $intLeaseIntervalId, $intCid, $objDatabase, $boolIsExcludeAttachedToUnit = true, $arrintLeaseStatusTypes = [] ) {

		$strJoinSql = '';
		if( true == $boolIsExcludeAttachedToUnit ) {
			$strJoinSql = ' AND ao.unit_space_id IS NULL';
		}

		if( true == valArr( $arrintLeaseStatusTypes ) ) {
			$strJoinSql .= ' AND la.lease_status_type_id IN (' . sqlIntImplode( $arrintLeaseStatusTypes ) . ')';
		}

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aog.name AS add_on_group_name,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.add_on_type_id,
						ao.available_on,
						ao.is_active,
						aoc.id As add_on_category_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . $strJoinSql . '  )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id AND aog.add_on_type_id IN ( ' . CAddOnType::RENTABLE_ITEMS . ', ' . CAddOnType::ASSIGNABLE_ITEMS . ', ' . CAddOnType::SERVICES . ' ) )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND la.is_selected_quote IS TRUE
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchIntegratedLeaseAssociationsByPropertyId( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = ' SELECT
						la.*
					FROM
    				    lease_associations la
    			    WHERE
    				    la.cid = ' . ( int ) $intCid . '
    				    AND la.property_id = ' . ( int ) $intPropertyId . '
    				    AND la.deleted_on IS NULL
	                    AND la.remote_primary_key IS NOT NULL
    				    AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' ';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchIntegratedLeaseAssociationsByLeaseId( $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = ' SELECT
						la.*
					FROM
    				    lease_associations la
    			    WHERE
    				    la.cid = ' . ( int ) $intCid . '
    				    AND la.lease_id = ' . ( int ) $intLeaseId . '
    				    AND la.deleted_on IS NULL
	                    AND la.remote_primary_key IS NOT NULL
    				    AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' ';

		return parent::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationByRemotePrimaryKeyByLeaseIdByCid( $strRemotePrimayKey, $intLeaseId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						la.*
					FROM
						lease_associations la
					WHERE 
						remote_primary_key = \'' . $strRemotePrimayKey . '\'
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND la.deleted_on IS NULL
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' LIMIT 1';
		return self::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public static function fetchConflictedAddOnLeaseAssociationsByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						la.id,
						la.ar_origin_reference_id,
						la.ar_origin_id, 
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						aog.name AS add_on_group_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.id = la.ar_origin_reference_id AND ao.cid = la.cid )
						JOIN add_on_groups aog ON ( aog.id = ao.add_on_group_id AND aog.cid = ao.cid  )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN cached_leases cl ON ( cl.id = la.lease_id AND la.cid = cl.cid AND la.lease_interval_id = cl.active_lease_interval_id )
						LEFT JOIN lease_associations la1 ON( la.cid = la1.cid AND la.ar_origin_id = la1.ar_origin_id AND la.ar_origin_reference_id = la1.ar_origin_reference_id AND la.lease_id <> la1.lease_id AND ( la1.lease_status_type_id = 4 OR la1.lease_status_type_id = 3 ) )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.deleted_on IS NULL
						AND aog.add_on_type_id = 2 
						AND aog.has_inventory = TRUE
						AND ( la1.id IS NOT NULL
                              OR CASE
                                   WHEN cl.unit_space_id IS NOT NULL
                              AND ao.unit_space_id IS NOT NULL THEN cl.unit_space_id <> ao.unit_space_id
                                 END
                              OR CASE
                                   WHEN cl.lease_start_date < ao.available_on THEN cl.lease_start_date < ao.available_on                                   
                                 END )
					GROUP BY
						la.ar_origin_reference_id,
						la.ar_origin_id,
						la.id,
						ao.name,
						aog.name,
						aoc.name
						,ao.details-->\'' . CLocaleContainer::createService()->getLocaleCode() . '-->name\'
						,aoc.details-->\'' . CLocaleContainer::createService()->getLocaleCode() . '-->name\'';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchRentableItemsApprovalCountByDashboardFilterByAddOnTypeIdsByCid( $objDashboardFilter, $intAddOnTypeId, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
						COUNT( id ) AS ' .
		 ( false == $boolIsGroupByProperties ? '
						count, MAX'
		  : '
						residents_rentable_items,
						property_id,
						property_name,
							' ) . '
						(priority) AS priority
					FROM (
						SELECT *
						FROM (
							SELECT
								la.id,
								la.cid,
									' . ( true == $boolIsGroupByProperties ? '
								la.property_id,
								p.property_name, '
		  : '' ) . '
								CASE
									WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' )::int ) >= la.end_date THEN
										3
									WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'within\'
										AND (
												( la.end_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) )
												OR ( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( la.end_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										3
									WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' )::int ) >= la.end_date THEN
										2
									WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'within\'
										AND (
												( la.end_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) )
												OR ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( la.end_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										2
									ELSE
										1
								END AS priority
							FROM
							 lease_associations la
							 JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = la.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							 JOIN properties p ON ( la.cid = p.cid AND la.property_id = p.id )
							 JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . 'AND ao.unit_space_id IS NULL )
							 JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
							 JOIN cached_leases cl ON ( la.cid = cl.cid AND cl.id = la.lease_id )
							 JOIN lease_intervals li ON ( li.cid = la.cid AND li.property_id = la.property_id AND li.lease_id = la.lease_id AND la.lease_interval_id = li.id )
							 LEFT JOIN dashboard_priorities dp ON dp.cid = la.cid
							 JOIN property_preferences pp ON ( la.cid = pp.cid AND la.property_id = pp.property_id AND pp.key = \'TURN_ON_RENTABLE_ITEMS\' )
						 WHERE
							 la.cid = ' . ( int ) $intCid . '
							 AND la.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
							 AND la.deleted_on IS NULL
							 AND aog.add_on_type_id = ' . ( int ) $intAddOnTypeId . '
							 AND cl.occupancy_type_id NOT IN ( ' . implode( ',', COccupancyType::$c_arrintExcludedOccupancyTypes ) . ' )
							 AND li.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType:: $c_arrintActiveAddOnReservationStatusTypes ) . ' )
							 AND la.quote_id IS NULL
						) AS lease_association
						WHERE
							cid = ' . ( int ) $intCid .
		 $strPriorityWhere .
		 ( false == $boolIsGroupByProperties ? '
						ORDER BY
							priority DESC
						LIMIT
							100 '
		  : '' ) . '
					) AS lease_association_count ' .
		 ( true == $boolIsGroupByProperties ? '
					GROUP BY
						property_id,
						property_name,
						priority'
		  : '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedAddOnsApprovalByDashboardFilterByAddOnTypeIdByCid( $objDashboardFilter, $intAddOnTypeId, $intCid, $intOffset, $intPageSize, $strSortBy, $objDatabase ) {
		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strPrioritySql = ', CASE WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' )::int ) >= la.start_date THEN
								3
							WHEN TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'within\'
								AND (
										( la.start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) )
										OR ( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( la.start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
									) THEN
								3
							WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'past\' AND ( CURRENT_DATE - ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' )::int ) >= la.start_date THEN
								2
							WHEN TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' ) <> \'\' AND TRIM( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) <> \'\' AND dp.residents_rentable_items->>\'important_move_in_move_out_date_type\' = \'within\'
								AND (
										( la.start_date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) )
										OR ( dp.residents_rentable_items->>\'urgent_move_in_move_out_date_type\' = \'past\' AND ( la.start_date BETWEEN ( CURRENT_DATE - TRIM ( dp.residents_rentable_items->>\'important_move_in_move_out_date_within\' ) ::int ) AND CURRENT_DATE ) )
									) THEN
								2
							ELSE
								1
							END AS priority';

		$strSql = '	SELECT *
					  FROM (
						SELECT
							la.cid,
							la.id,
							la.lease_id,
							la.property_id,
							la.customer_id,
							ao.id as add_on_id,
							cl.unit_number_cache,
							func_format_customer_name ( cl.name_first, cl.name_last ) AS customer_full_name,
					 		COALESCE ( aog.name || \'( \' || ao.name || \' )\', aog.name, ao.name, NULL ) AS item_name,
					 		CASE WHEN la.start_date = DATE_TRUNC(\'day\', NOW() )
					   			THEN \'Today\'
					 			ELSE to_char( la.start_date, \'MM/DD/YY\' )
					 		END as start_date,
							p.property_name
							' . $strPrioritySql . '
						 FROM
							 lease_associations la
							 JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = la.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							 JOIN properties p ON ( la.cid = p.cid AND la.property_id = p.id )
							 JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ao.unit_space_id IS NULL )
							 JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
							 JOIN cached_leases cl ON ( la.cid = cl.cid AND cl.id = la.lease_id )
							 JOIN lease_intervals li ON ( li.cid = la.cid AND li.property_id = la.property_id AND li.lease_id = la.lease_id AND la.lease_interval_id = li.id )
							 LEFT JOIN dashboard_priorities dp ON dp.cid = la.cid
							 JOIN property_preferences pp ON ( la.cid = pp.cid AND la.property_id = pp.property_id AND pp.key = \'TURN_ON_RENTABLE_ITEMS\' )
						 WHERE
							 la.cid = ' . ( int ) $intCid . '
							 AND la.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
							 AND la.deleted_on IS NULL
							 AND aog.add_on_type_id = ' . ( int ) $intAddOnTypeId . '
							 AND li.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType:: $c_arrintActiveAddOnReservationStatusTypes ) . ' )
							 AND la.quote_id IS NULL
							) AS lease_association
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY
						' . $strSortBy . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAddOnsLeaseAssociationsApprovalListByIdByAddOnIdsLeaseIdsByByAddOnTypeIdsByPropertyIdsByCid( $arrintIds, $arrintAddOnIds, $arrintLeaseIds, $arrintAddOnTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) || false == valArr( $arrintAddOnIds ) || false == valArr( $arrintLeaseIds ) || false == valArr( $arrintAddOnTypeIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = '	SELECT
						la.cid,
						la.id,
						la.lease_id,
						la.property_id,
						ao.id as add_on_id,
				        COALESCE ( aog.name || \'( \' || ao.name || \' )\', aog.name, ao.name, NULL ) AS item_name,
						p.property_name
					 FROM
						 lease_associations la
						 JOIN properties p ON ( la.cid = p.cid AND la.property_id = p.id )
						 JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						 JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
					 WHERE
						 la.cid = ' . ( int ) $intCid . '
						 AND la.id IN ( ' . sqlIntImplode( $arrintIds ) . ')
						 AND la.deleted_on IS NULL
						 AND la.lease_status_type_id = ' . CLeaseStatusType::APPLICANT . '
						 AND la.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ')
						 AND la.lease_id IN ( ' . sqlIntImplode( $arrintLeaseIds ) . ')
						 AND la.ar_origin_reference_id IN ( ' . sqlIntImplode( $arrintAddOnIds ) . ')
						 AND aog.add_on_type_id IN ( ' . sqlIntImplode( $arrintAddOnTypeIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	// we have excluded same lease association

	public static function fetchAddOnLeaseAssociationByAddOnIdByAddOnAvailableOnDateByLeaseAssociationStartDate( $intAddOnId, $strAvailableOnDate, $intCid, $objDatabase, $intExcludedAddOnLeaseAssociationId ) {

		$strSql = 'SELECT
					   	*
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
					WHERE
						la.id !=' . ( int ) $intExcludedAddOnLeaseAssociationId . '
						AND la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.start_date < \'' . $strAvailableOnDate . '\'
						AND la.ar_origin_reference_id = ' . ( int ) $intAddOnId . '
						AND la.lease_status_type_id =' . CLeaseStatusType::FUTURE . '
						AND la.deleted_on IS NULL
						AND ao.is_active = true';

		$strSql .= ' ORDER BY la.id ASC LIMIT 1';

		return self::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationByAddOnIdByleaseStatusTypeIdByCid( $intAddOnId, $arrintLeaseStatusTypeIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
						JOIN add_ons ao ON ( la.cid = ao.cid AND ao.id = la.ar_origin_reference_id )
						LEFT JOIN company_users cu ON ( cu.cid = ao.cid AND cu.id = ao.updated_by )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.ar_origin_reference_id = ' . ( int ) $intAddOnId . '
						AND la.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL
					ORDER BY start_date, id';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByAddOnIdsByLeaseStatusTypeIdsByCid( $arrintAddOnIds, $arrintLeaseStatusTypeIds, $arrintLeaseAssociationLeaseStatusTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAddOnIds ) || false == valArr( $arrintLeaseStatusTypeIds ) || false == valArr( $arrintLeaseAssociationLeaseStatusTypeIds ) ) return NULL;

		$strSql = 'SELECT
						la.*,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						ao.available_on,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.name AS add_on_group_name,
						aog.id AS add_on_group_id,
						aog.add_on_type_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( la.cid = ao.cid AND ao.id = la.ar_origin_reference_id AND ao.is_active IS TRUE )
						JOIN lease_customers lc ON ( la.lease_id = lc.lease_id AND la.customer_id = lc.customer_id AND la.cid = lc.cid )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.property_id = ao.property_id AND aog.id = ao.add_on_group_id)
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.ar_origin_reference_id IN ( ' . implode( ',', $arrintAddOnIds ) . ' )
						AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND la.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseAssociationLeaseStatusTypeIds ) . ' )
						AND la.deleted_on IS NULL
						AND la.deleted_by IS NULL
					ORDER BY start_date, id';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByLeaseIdsByLeaseStatusTypeIdByPropertyIdByCid( $arrintLeaseIds, $intLeaseStatusTypeId, $strRewindDate, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					*
				FROM
					lease_associations
				WHERE
					cid = ' . ( int ) $intCid . '
					AND property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
					AND lease_id IN (' . implode( ',', $arrintLeaseIds ) . ')
					AND lease_status_type_id = ' . ( int ) $intLeaseStatusTypeId . '
					AND end_date > DATE_TRUNC( \'MONTH\', \'' . $strRewindDate . '\'::date )
					AND deleted_on IS NULL
					AND deleted_by IS NULL ';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsCountByAddOnIdByLeaseStatusTypeIdByPropertyIdByCid( $intArOriginReferenceId, $intLeaseStatusTypeId, $arrintPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT( * ) AS total_applicant_lease_association
					FROM
						lease_associations la
						JOIN lease_intervals li ON ( la.cid = li.cid AND la.property_id = li.property_id AND la.lease_id = li.lease_id AND la.lease_interval_id = li.id AND li.lease_status_type_id = la.lease_status_type_id )
						LEFT JOIN quotes q ON ( la.cid = q.cid AND la.quote_id = q.id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $arrintPropertyId . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
						AND la.lease_status_type_id = ' . ( int ) $intLeaseStatusTypeId . '
						AND la.deleted_by IS NULL
						AND la.deleted_on IS NULL
						AND CASE
                                WHEN q.expires_on < NOW() THEN q.expires_on > NOW()
                                WHEN la.quote_id IS NOT NULL THEN la.quote_id = q.id
                                ELSE la.quote_id IS NULL
                            END';

		return self::fetchColumn( $strSql, 'total_applicant_lease_association', $objDatabase );
	}

	public static function fetchCustomAddOnLeaseAssociationsByAddOnIdsByAddOnGroupIdsByCid( $arrintAddOnIds, $arrintAddOnGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAddOnIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						la.*, 
						ao.add_on_group_id
					FROM
						lease_associations la
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id AND aog.add_on_type_id IN ( ' . CAddOnType::RENTABLE_ITEMS . ' ) )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.ar_origin_reference_id IN( ' . implode( ',', $arrintAddOnIds ) . ' )
						AND aog.id IN (' . implode( ',', $arrintAddOnGroupIds ) . ' )';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByIdsByAddOnIdsByLeaseStatusTypeIdByCid( $arrintAddOnIds, $arrintLeaseIds, $intLeaseStatusTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAddOnIds ) || false == valArr( $arrintLeaseIds ) || false == valId( $intLeaseStatusTypeId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						la.*
					FROM
						lease_associations la
					WHERE
					la.cid = ' . ( int ) $intCid . ' AND
					la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND
					la.ar_origin_reference_id IN ( ' . implode( ',', $arrintAddOnIds ) . ' ) AND
					la.lease_status_type_id = ' . ( int ) $intLeaseStatusTypeId . ' AND
					la.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) AND
					la.deleted_on IS NULL AND
					la.deleted_by IS NULL';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );

	}

	public static function fetchAddonLeaseAssociationsByIdsByAddOnIdsByCid( $arrintLeaseAssociationIds, $arrintAddOnIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAddOnIds ) || false == valArr( $arrintLeaseAssociationIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						func_format_customer_name( cl.name_first, cl.name_last ) AS customer_full_name,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS name,
						COALESCE( aog.name || \'( \' || ao.name || \' )\', aog.name, ao.name, NULL ) AS item_name,
						ao.unit_space_status_type_id
					FROM
						lease_associations la
						JOIN cached_leases cl ON ( la.cid = cl.cid AND cl.id = la.lease_id )
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = 4 )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id )
						JOIN add_on_categories aoc ON ( aog.add_on_category_id = aoc.id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						la.cid = ' . ( int ) $intCid . ' AND
						la.id IN ( ' . implode( ',', $arrintLeaseAssociationIds ) . ' ) AND
						la.ar_origin_reference_id IN ( ' . implode( ',', $arrintAddOnIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsMaxIdByAddOnIdByCid( $arrintAddOnIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAddOnIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT MAX(id) as id FROM lease_associations WHERE ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ar_origin_reference_id IN ( ' . implode( ',', $arrintAddOnIds ) . ' ) AND cid = ' . ( int ) $intCid . ' GROUP BY ar_origin_reference_id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveAddOnLeaseAssociationsByAddOnIdsByCid( $arrintAddOnIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAddOnIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT la.*
					FROM
						lease_associations as la
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
						AND la.ar_origin_reference_id IN( ' . implode( ',', $arrintAddOnIds ) . ' )
						AND la.deleted_on IS NULL';

		return self::fetchLeaseAssociations( $strSql, $objDatabase );
	}

	public static function fetchLeaseAssociationByRemotePrimaryKeyByLeaseIdByArOriginReferenceIdByPropertyIdByCid( $strRemotePrimaryKey, $intLeaseId, $intArOriginReferenceId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						la.*
					FROM
						lease_associations la
					WHERE 
						remote_primary_key = \'' . $strRemotePrimaryKey . '\'
						AND la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' 
						AND la.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						LIMIT 1';

		return self::fetchLeaseAssociation( $strSql, $objDatabase );
	}

	public static function fetchAddOnLeaseAssociationsByPropertyIdByLeaseIntervalIdByAddOnTypeIdsByCid( $intPropertyId, $arrintLeaseIntervalId, $arrintAddOnTypeIds, $intCid, $objDatabase, $boolIsCheckDeleted = false ) {
		if( false == valArr( $arrintAddOnTypeIds ) || false == valId( $arrintLeaseIntervalId ) ) {
			return NULL;
		}

		$strSqlConditions = '';

		$strSqlConditions .= ( true == $boolIsCheckDeleted ) ? ' AND la.deleted_on IS NULL AND la.deleted_by IS NULL' : '';

		$strSql = 'SELECT
						la.*,
						aog.id AS add_on_group_id,
						aot.id AS add_on_type_id,
						aog.remote_primary_key AS add_on_group_remote_primary_key,
						aog.lookup_code AS add_on_group_lookup_code,
						ao.remote_primary_key AS add_on_remote_primary_key,
						ao.available_on as available_on,
						ao.unit_space_status_type_id,
						ao.unit_space_id as unit_space_id,
						ao.lookup_code AS add_on_lookup_code,
						aog.add_on_type_id
					FROM
						lease_associations la
						JOIN lease_intervals li ON ( la.cid = li.cid AND la.property_id = li.property_id AND la.lease_id = li.lease_id AND la.lease_interval_id = li.id )
						LEFT JOIN scheduled_charges scr ON ( scr.cid = la.cid AND scr.lease_id = la.lease_id AND scr.ar_origin_reference_id = la.ar_origin_reference_id AND scr.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scr.ar_trigger_id = ' . CArTrigger::MONTHLY . ' AND scr.lease_association_id = la.id AND scr.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scd ON ( scd.cid = la.cid AND scd.lease_id = la.lease_id AND scd.ar_origin_reference_id = la.ar_origin_reference_id AND scd.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND scd.ar_trigger_id = ' . CArTrigger::MOVE_IN . ' AND scd.lease_association_id = la.id AND scd.deleted_on IS NULL )
						LEFT JOIN scheduled_charges scrf ON ( scrf.cid = la.cid AND scrf.lease_id = la.lease_id AND scrf.ar_origin_reference_id = la.ar_origin_reference_id AND scrf.ar_origin_id = ' . CArOrigin::ADD_ONS . ' AND ( scrf.ar_trigger_id = ' . CArTrigger::APPLICATION_COMPLETED . ' OR scrf.ar_trigger_id = ' . CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT . ' ) AND scrf.lease_association_id = la.id AND scrf.deleted_on IS NULL )
						JOIN add_ons ao ON ( ao.cid = la.cid AND ao.id = la.ar_origin_reference_id AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . ' )
						JOIN add_on_groups aog ON ( aog.cid = ao.cid AND aog.id = ao.add_on_group_id)
						JOIN add_on_types aot ON ( aog.add_on_type_id = aot.id )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						JOIN lease_status_types lst ON ( la.lease_status_type_id = lst.id )
						LEFT JOIN company_employees ce ON ( la.company_employee_id = ce.id AND la.cid = ce.cid )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id =' . ( int ) $intPropertyId . '
						AND la.lease_interval_id = ' . $arrintLeaseIntervalId . $strSqlConditions . '
						AND aog.add_on_type_id IN (' . implode( ',', $arrintAddOnTypeIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

}
?>