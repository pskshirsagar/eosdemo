<?php

class CSpecialRateAssociations extends CRateAssociations {

	public static function fetchIntegratedSpecialRateAssociationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		// MEGARATES_COMMENTS: replacement function for CPropertySpecials::fetchPropertySpecialsByPropertyIdsByCid()
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id AND sp.cid = ' . ( int ) $intCid . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ra.remote_primary_key IS NOT NULL';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchWebsiteSpecialRateAssociationsByPropertyIdsByArCascadeIdsByCid( $arrintPropertyIds, $arrintArCascadeIds, $intCid, $objDatabase, $strMoveInDate = NULL, $intSelectedLeaseStartWindowId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == valArr( $arrintArCascadeIds ) ) return NULL;

		$strSqlJoin        = '';
		$strSelectFields   = '';

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		if( true == valId( $intSelectedLeaseStartWindowId ) ) {
			$strSqlJoin .= ' LEFT JOIN LATERAL
								(
									SELECT
									  sum ( CASE WHEN sub.show_on_website IS TRUE THEN 1 ELSE 0 END ) AS web_visible_rates_count,
									  count ( sub.id ) AS all_rates_count,
									  array_to_string ( array_agg ( sub.space_configuration_id ORDER BY sub.id ), \',\' ) AS space_configuration_ids,
									  array_to_string ( array_agg ( sub.show_on_website ORDER BY sub.id ), \',\' ) AS web_visible_rates
									FROM
									  (
									    SELECT
											r.id,
											r.show_on_website,
											r.ar_cascade_id,
											r.ar_cascade_reference_id,
											r.ar_origin_id,
											r.ar_origin_reference_id,
											CASE WHEN 0 < r.space_configuration_id THEN r.space_configuration_id ELSE NULL END AS space_configuration_id
											FROM
											    rate_logs r
											    LEFT JOIN property_ar_origin_rules paor ON ( r.cid = paor.cid AND r.property_id = paor.property_id AND r.ar_origin_id = paor.ar_origin_id AND paor.use_lease_terms_for_rent IS TRUE )
											WHERE
											    r.cid = sp.cid
											    AND r.ar_code_type_id = ' . CArCodeType::RENT . '
											    AND r.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . '
											    AND r.ar_origin_reference_id = sp.id
											    AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id
											    AND r.ar_cascade_id = ra.ar_cascade_id
											    AND r.is_allowed = TRUE
											    AND r.rate_amount <> 0
											    AND r.deactivation_date > CURRENT_DATE
											    AND CURRENT_DATE BETWEEN r.effective_date AND r.effective_through_date
											    AND r.effective_through_date >= r.effective_date
											    AND r.is_effective_datetime_ignored = FALSE
											    AND CASE WHEN paor.id IS NOT NULL THEN r.lease_start_window_id = ' . ( int ) $intSelectedLeaseStartWindowId . ' ELSE TRUE END
									  ) AS sub
								) r ON TRUE';

			$strSelectFields   .= ' ,r.space_configuration_ids, r.all_rates_count, r.web_visible_rates_count, r.web_visible_rates';
		}

		$strSql = 'SELECT
						sp.*,
						ra.id as rate_association_id,
						ra.hide_description,
						ra.property_id,
						ra.property_floorplan_id,
						ra.unit_type_id,
						ra.unit_space_id,
						ra.ar_cascade_id,
						ra.ar_cascade_reference_id,
						ra.ar_origin_id,
						ra.ar_origin_reference_id,
						ra.hide_rates' . $strSelectFields . '
					FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id AND sp.cid = ' . ( int ) $intCid . ' AND sp.is_active::BOOLEAN = true AND sp.deleted_on IS NULL AND sp.show_on_website::BOOLEAN = true AND sp.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . ' )'
						. $strSqlJoin .
				   ' WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.ar_cascade_id IN ( ' . implode( ',', $arrintArCascadeIds ) . ' )
						AND ra.is_published::BOOLEAN = true
						AND sp.show_on_website::BOOLEAN = true
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ( ( ' . $strMoveInDate . ' ::Date >= sp.start_date::DATE AND ' . $strMoveInDate . ' ::Date <= sp.end_date::DATE ) OR
									( ' . $strMoveInDate . ' ::Date >= sp.start_date::DATE AND sp.end_date IS NULL ) OR
									( sp.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= sp.end_date::DATE ) OR
									( sp.start_date IS NULL AND sp.end_date IS NULL )
									)
						AND sp.special_type_id <> ' . CSpecialType::FIXED_BAH_ADJUSTMENT . '
						AND sp.special_type_id <> ' . CSpecialType::INSTALLMENT_PLAN;

		$strSql .= ' ORDER BY ra.ar_cascade_id';
		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		$arrobjSpecials = [];

		foreach( $arrstrData as $strRow ) {
			$objSpecialRateAssociation = new CSpecialRateAssociation();
			$objSpecialRateAssociation->setValues( $strRow );

			$objSpecial = new CSpecial();
			$objSpecial->setValues( $strRow );
			$objSpecial->setSpecialRateAssociations( [ $objSpecialRateAssociation ] );

			$arrobjSpecials[] = $objSpecial;
		}

		return $arrobjSpecials;
	}

	public static function fetchWebsiteSpecialRateAssociationsWithLeaseTermIdByPropertyIdsByArCascadeIdsByCid( $arrintPropertyIds, $arrintArCascadeIds, $intCid, $objDatabase, $strMoveInDate = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == valArr( $arrintArCascadeIds ) ) return NULL;

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setMaxArCascadeId( 4 );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSqlJoin = '';

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		$strSql = 'SELECT
						sp.*,
						ra.id as rate_association_id,
						ra.hide_description,
						ra.property_id,
						ra.property_floorplan_id,
						ra.unit_type_id,
						ra.unit_space_id,
						ra.ar_cascade_id,
						ra.ar_cascade_reference_id,
						ra.ar_origin_id,
						ra.ar_origin_reference_id,
						ra.hide_rates,
						rates_subquery.lease_term_ids
					FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id AND sp.cid = ' . ( int ) $intCid . ' AND sp.is_active::BOOLEAN = true AND sp.deleted_on IS NULL AND sp.show_on_website::BOOLEAN = true AND sp.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . ' )
						LEFT JOIN (
							SELECT cid, property_id, ar_cascade_reference_id, ar_origin_reference_id, 
                            	array_to_string( array_agg( distinct r.lease_term_id ), \',\') AS lease_term_ids
                            FROM 
                            	prospect_rates r 
                            WHERE 
                            	r.ar_origin_id = ' . CArOrigin::SPECIAL . '
                            GROUP BY 
                            	cid, property_id, ar_cascade_reference_id, ar_origin_reference_id
                        ) rates_subquery 
                        	ON ( ra.cid = rates_subquery.cid AND ra.property_id = rates_subquery.property_id AND ra.ar_cascade_reference_id = rates_subquery.ar_cascade_reference_id AND ra.ar_origin_reference_id = rates_subquery.ar_origin_reference_id ) '
		          . $strSqlJoin .
		          ' WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.ar_cascade_id IN ( ' . implode( ',', $arrintArCascadeIds ) . ' )
						AND ra.is_published::BOOLEAN = true
						AND sp.show_on_website::BOOLEAN = true
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ( ( ' . $strMoveInDate . ' ::Date >= sp.start_date AND ' . $strMoveInDate . ' ::Date <= sp.end_date ) OR
									( ' . $strMoveInDate . ' ::Date >= sp.start_date AND sp.end_date IS NULL ) OR
									( sp.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= sp.end_date ) OR
									( sp.start_date IS NULL AND sp.end_date IS NULL )
									)
						AND sp.special_type_id <> ' . CSpecialType::FIXED_BAH_ADJUSTMENT . '
						AND sp.special_type_id <> ' . CSpecialType::INSTALLMENT_PLAN;

		$strSql .= ' ORDER BY ra.ar_cascade_id';
		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		$arrobjSpecials = [];

		foreach( $arrstrData as $strRow ) {
			$objSpecialRateAssociation = new CSpecialRateAssociation();
			$objSpecialRateAssociation->setValues( $strRow );

			$objSpecial = new CSpecial();
			$objSpecial->setValues( $strRow );
			$objSpecial->setSpecialRateAssociations( [ $objSpecialRateAssociation ] );

			$arrobjSpecials[] = $objSpecial;
		}

		return $arrobjSpecials;
	}

	public static function fetchWebsiteSpecialsByPropertyIdByUnitTypeIdByFloorplanIdByUnitSpaceIdByCid( $intPropertyId, $intUnitTypeId, $intPropertyFloorplanId, $intUnitSpaceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sp.*,
						ra.id as rate_association_id,
						ra.hide_description,
						property_id,
						property_floorplan_id,
						unit_type_id,
						unit_space_id,
						ar_cascade_id,
						ar_cascade_reference_id,
						ar_origin_id,
						ar_origin_reference_id,
						hide_rates
					FROM
						rate_associations ra
					JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id AND sp.cid = ' . ( int ) $intCid . ' AND sp.is_active::BOOLEAN = true AND sp.deleted_on IS NULL AND sp.show_on_website::BOOLEAN = true AND sp.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id =  ' . ( int ) $intPropertyId . '
						AND (
							( ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND ar_cascade_reference_id = ' . ( int ) $intPropertyId . ' ) OR
							( ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' AND ar_cascade_reference_id = ' . ( int ) $intPropertyFloorplanId . ' ) OR
							( ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND ar_cascade_reference_id = ' . ( int ) $intUnitTypeId . ' ) OR
							( ra.ar_cascade_id = ' . CArCascade::SPACE . ' AND ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . ' )
						)
						AND ra.is_published::BOOLEAN = true
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ( ( NOW() >= sp.start_date AND NOW() <= sp.end_date ) OR
								( NOW() >= sp.start_date AND sp.end_date IS NULL ) OR
								( sp.start_date IS NULL AND NOW() <= sp.end_date ) OR
								( sp.start_date IS NULL AND sp.end_date IS NULL )
							)';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		$arrobjSpecials = [];

		foreach( $arrstrData as $strRow ) {
			$objSpecialRateAssociation = new CSpecialRateAssociation();
			$objSpecialRateAssociation->setValues( $strRow );

			$objSpecial = new CSpecial();
			$objSpecial->setValues( $strRow );
			$objSpecial->setSpecialRateAssociations( [ $objSpecialRateAssociation ] );

			$arrobjSpecials[] = $objSpecial;
		}

		return $arrobjSpecials;
	}

	public static function fetchSpecialRateAssociationByArOriginReferenceIdByPropertyIdByCid( $intArOriginReferenceId, $intPropertyId, $intCid, $objDatabase ) {
		// MEGARATES_COMMENTS: replacement function for CPropertySpecials::fetchPropertySpecialByCompanySpecialIdByPropertyIdByCid()

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id AND sp.cid = ' . ( int ) $intCid . ' AND sp.id = ' . ( int ) $intArOriginReferenceId . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' LIMIT 1';

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchSpecialRateAssociationsByArOriginReferenceIdByCid( $intArOriginReferenceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						LEFT JOIN ' . CProperties::VIEW_COMPANY_USERS_PROPERTIES . ' vcup ON ( vcup.id = ps.property_id AND vcup.cid = ' . ( int ) $intCid . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedProspectPortalSpecialRateAssociationsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $intArCascadeId = NULL, $arrintArCascadeReferenceIds = NULL ) {
		//	MEGARATES : Replacement function for CPropertySpecials::fetchCurrentPublishedProspectPropertySpecialsCountByPropertyIdsByCid() and CPropertySpecials::fetchCurrentPublishedPropertySpecialsCountByPropertyIdsByCid()
		//	and CPropertySpecials::fetchPublishedPropertySpecialsByPropertyIdsByUnitSpaceIdsByCid()

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCondition = '';

		if( false == is_null( $intArCascadeId ) && true == in_array( $intArCascadeId, [ CArCascade::PROPERTY, CArCascade::FLOOR_PLAN, CArCascade::UNIT_TYPE, CArCascade::SPACE ] ) ) {
			$strCondition = ' AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId;
			$strCondition .= ( true == valArr( $arrintArCascadeReferenceIds ) ) ? 'AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintArCascadeReferenceIds ) . ' ) ' : '';
		}

		$strSql = 'SELECT
						ra.property_id, count( ra.property_id )
					FROM
						rate_associations ra
						JOIN specials sp ON ( ra.cid = sp.cid AND sp.id = ra.ar_origin_reference_id AND sp.cid = ' . ( int ) $intCid . ' AND sp.special_recipient_id = ' . ( int ) CSpecialRecipient::PROSPECTS . ' AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strCondition . '
						AND ra.is_published::BOOLEAN = true
						AND sp.is_active::BOOLEAN = true
						AND sp.show_on_website::BOOLEAN = true
						AND sp.deleted_on IS NULL
						AND NOW() >= COALESCE( sp.start_date, NOW() )
						AND NOW() <= COALESCE( sp.end_date, NOW() )
					GROUP BY ra.property_id';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponse ) ) {
			$arrintSpecialRateAssociationCount = [];

			foreach( $arrmixResponse as $arrintResponse ) {
				$arrintSpecialRateAssociationCount[$arrintResponse['property_id']] = $arrintResponse['count'];
			}

			return $arrintSpecialRateAssociationCount;
		}

		return [ 0 ];
	}

	public static function fetchCurrentPublishedSpecialRateAssocaitionsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id AND sp.cid = ' . ( int ) $intCid . ' AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . ' AND sp.special_recipient_id = ' . ( int ) CSpecialRecipient::PROSPECTS . ' )
					WHERE
						ra.cid =' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.is_published::BOOLEAN = true
						AND sp.is_active::BOOLEAN = true
						AND sp.deleted_on IS NULL
						AND NOW() >= COALESCE( sp.start_date, NOW() )
						AND NOW() <= COALESCE( sp.end_date, NOW() )';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchIntegratedRateAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		// MEGARATES : Replacement function for CPropertySpecials::fetchIntegratedPropertySpecialsByPropertyIdByCid()

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id )
					WHERE
						ra.cid =' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . '
						AND ra.remote_primary_key IS NOT NULL';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchSpecialRateAssociationsByPropertyIdsByArOriginReferenceIdsByCid( $arrintPropertyIds, $arrintArOriginReferenceIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintArOriginReferenceIds ) ) return NULL;

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ra.ar_origin_reference_id IN ( ' . implode( ',', $arrintArOriginReferenceIds ) . ' )';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchSpecialRateAssociationsByPropertyIdByArCascadeIdByArCascadeReferenceIdByCid( $intPropertyId, $intArCascadeId, $intArCascadeReferenceId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						rate_associations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId . '
						AND ar_origin_id = ' . ( int ) CArOrigin::SPECIAL;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchSpecialRateAssociationByArCascadeIdByArCascadeReferenceIdByArOriginReferenceIdByCid( $intArCascadeId, $intArCascadeReferenceId, $intArOriginReferenceId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						rate_associations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId . '
						AND ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . '
						AND ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . ' LIMIT 1 ';

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchSpecialRateAssociationByPropertyIdByArCascadeIdArCascadeReferenceIdsByCid( $intPropertyId, $arrintArCascadeIdArCascadeReferenceIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintArCascadeIdArCascadeReferenceIds ) || \Psi\Libraries\UtilFunctions\count( $arrintArCascadeIdArCascadeReferenceIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintArCascadeIdArCascadeReferenceIds, COUNT_RECURSIVE ) ) return NULL;

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', sp.name, sp.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as special_name,
						sp.description as special_description,
						sp.start_date,
						sp.end_date,
						sp.quantity_remaining,
						sp.is_active,
						sp.show_on_website
				   FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . '
						AND ( ra.ar_cascade_id, ra.ar_cascade_reference_id ) IN ( ' . sqlIntMultiImplode( $arrintArCascadeIdArCascadeReferenceIds ) . ' )
						AND sp.deleted_on IS NULL';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedWebsiteSpecialRateAssociationByCidByPropertyIdByArCascadeIdArCascadeReferenceIdsByFloorplanAvailabilityFilter( $intCid, $intPropertyId, $arrintArCascadeIdArCascadeReferenceIds, $objFloorplanAvailabilityFilter, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valArr( $arrintArCascadeIdArCascadeReferenceIds ) || \Psi\Libraries\UtilFunctions\count( $arrintArCascadeIdArCascadeReferenceIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintArCascadeIdArCascadeReferenceIds, COUNT_RECURSIVE ) ) return NULL;

		$strMoveInStartDate		= '\'' . standardizeMoveInDate( $objFloorplanAvailabilityFilter->getMoveInStartDate() ) . '\'';
		$strMoveInEndDate		= '\'' . standardizeMoveInDate( $objFloorplanAvailabilityFilter->getMoveInEndDate() ) . '\'';

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', sp.name, sp.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as special_name,
						sp.description AS special_description,
						sp.start_date,
						sp.end_date,
						sp.quantity_remaining,
						sp.is_active,
						sp.show_on_website
				   FROM
						rate_associations ra
						JOIN specials sp ON ( sp.cid = ra.cid AND sp.id = ra.ar_origin_reference_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ( ra.ar_cascade_id, ra.ar_cascade_reference_id ) IN ( ' . sqlIntMultiImplode( $arrintArCascadeIdArCascadeReferenceIds ) . ' )
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . '
						AND ra.is_published::BOOLEAN = true
						AND sp.is_active::BOOLEAN = true
						AND sp.show_on_website = true
						AND sp.deleted_on IS NULL
						AND ( ( ' . $strMoveInStartDate . ' >= sp.start_date AND ' . $strMoveInEndDate . ' <= sp.end_date )
							OR ( sp.start_date BETWEEN ' . $strMoveInStartDate . ' AND ' . $strMoveInEndDate . ')
							OR ( sp.end_date BETWEEN ' . $strMoveInStartDate . ' AND ' . $strMoveInEndDate . ')
							OR ( ' . $strMoveInStartDate . ' >= sp.start_date AND sp.end_date IS NULL )
							OR ( ' . $strMoveInEndDate . ' <= sp.end_date AND sp.start_date IS NULL )
							OR ( sp.start_date IS NULL AND ' . $strMoveInEndDate . ' <= sp.end_date ) 
							OR ( sp.start_date IS NULL AND sp.end_date IS NULL ) )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSpecialRateAssociationsByArCascadeIdByArCascadeReferenceIdsByArOriginReferenceIdByCid( $intArCascadeId, $arrintArCascadeReferenceIds, $intArOriginReferenceId, $intCid, $objDatabase ) {

		if( false == valId( $intArCascadeId ) || false == valId( $intArOriginReferenceId ) || false == valArr( $arrintArCascadeReferenceIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					  ra.*
					FROM
					  rate_associations ra
					  JOIN specials s ON ( s.cid = ra.cid AND s.id = ra.ar_origin_reference_id )
					WHERE
					  ra.cid = ' . ( int ) $intCid . ' AND
					  ra.ar_cascade_id = ' . ( int ) $intArCascadeId . ' AND
					  ra.ar_cascade_reference_id IN ( ' . implode( ',',  $arrintArCascadeReferenceIds ) . ' ) AND
					  ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND
					  ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . ' AND
					  s.is_active IS TRUE AND
					  s.deleted_on IS NULL';

		return self::fetchRateAssociations( $strSql, $objDatabase );

	}

}
?>