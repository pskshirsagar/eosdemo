<?php

class CLeaseCustomerScheduledTasks extends \Psi\Eos\Entrata\CScheduledTasks {

	public static function fetchLeaseCustomerScheduledTasksByScheduledTaskEmailSearchFilter( $objScheduledTaskSearchFilter, $objDatabase ) {
		$arrstrWhere = [];

		if( true == valStr( $objScheduledTaskSearchFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'st.property_id IN ( ' . $objScheduledTaskSearchFilter->getPropertyIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventTypeIds() ) ) {
			$arrstrWhere[] = 'st.event_type_id IN ( ' . $objScheduledTaskSearchFilter->getEventTypeIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventSubTypeIds() ) ) {
			$arrstrWhere[] = 'st.event_sub_type_id IN ( ' . $objScheduledTaskSearchFilter->getEventSubTypeIds() . ' )';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getLeaseIntervalTypeIds() ) ) {
			$arrstrWhere[] = 'st.task_details ->> \'lease_interval_type_id\' IN ( \'' . $objScheduledTaskSearchFilter->getLeaseIntervalTypeIds() . '\' )';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventTriggerTypes() ) ) {
			$arrstrWhere[] = 'st.schedule_details ->> \'event_trigger_type\' IN ( \'' . $objScheduledTaskSearchFilter->getEventTriggerTypes() . '\' )';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getSubject() ) ) {
			$arrstrWhere[] = 'ste.subject LIKE \'%' . addslashes( trim( $objScheduledTaskSearchFilter->getSubject() ) ) . '%\'';
		}

		$strSql = 'SELECT
						st.id,
						st.cid,
						st.property_id,
						st.task_details ->> \'compose_email_id\' as compose_email_id,
						st.schedule_details ->> \'event_days\' as num_days,
						st.schedule_details ->> \'event_trigger_type\' as event_trigger_type,
						ste.subject as email_subject,
						st.event_type_id as event_id,
						st.task_details ->> \'lease_interval_type_id\' as lease_interval_type_id,
						st.event_sub_type_id,
						p.property_name
					FROM
						scheduled_tasks st
						JOIN scheduled_task_emails ste ON ( st.cid = ste.cid AND ( st. task_details ->> \'compose_email_id\' )::INTEGER = ste.id )
						JOIN properties p ON ( st.cid = p.cid AND st.property_id = p.id )
					WHERE
						st.cid = ' . ( int ) $objScheduledTaskSearchFilter->getCid() . '
						AND st.deleted_by IS NULL 
						AND st.deleted_on IS NULL
						AND ( st.task_details ? \'compose_email_id\'
						AND ( st.task_details ->> \'compose_email_id\' ) IS NOT NULL )
						AND st.task_details ->> \'compose_email_id\' != \'\'';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		if( true == is_numeric( $objScheduledTaskSearchFilter->getOffset() ) && true == is_numeric( $objScheduledTaskSearchFilter->getPageSize() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objScheduledTaskSearchFilter->getOffset() . ' LIMIT ' . ( int ) $objScheduledTaskSearchFilter->getPageSize();
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountLeaseCustomerScheduledTasksByScheduledTaskEmailSearchFilter( $objScheduledTaskSearchFilter, $objDatabase ) {
		$arrstrWhere = [];

		if( true == valStr( $objScheduledTaskSearchFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'st.property_id IN ( ' . $objScheduledTaskSearchFilter->getPropertyIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventTypeIds() ) ) {
			$arrstrWhere[] = 'st.event_type_id IN ( ' . $objScheduledTaskSearchFilter->getEventTypeIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventSubTypeIds() ) ) {
			$arrstrWhere[] = 'st.event_sub_type_id IN ( ' . $objScheduledTaskSearchFilter->getEventSubTypeIds() . ')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getLeaseIntervalTypeIds() ) ) {
			$arrstrWhere[] = 'st.task_details ->> \'lease_interval_type_id\' IN ( \'' . $objScheduledTaskSearchFilter->getLeaseIntervalTypeIds() . '\' )';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getEventTriggerTypes() ) ) {
			$arrstrWhere[] = 'st.schedule_details ->> \'event_trigger_type\' IN ( \'' . $objScheduledTaskSearchFilter->getEventTriggerTypes() . '\')';
		}

		if( true == valStr( $objScheduledTaskSearchFilter->getSubject() ) ) {
			$arrstrWhere[] = 'ste.subject LIKE \'%' . addslashes( trim( $objScheduledTaskSearchFilter->getSubject() ) ) . '%\'';
		}

		$strSql = 'SELECT
						COUNT( st.id )
					FROM
						scheduled_tasks st
						JOIN scheduled_task_emails ste ON ( st.cid = ste.cid AND ( st. task_details ->> \'compose_email_id\' )::INTEGER = ste.id )
						JOIN event_types et ON ( st.event_type_id = et.id )
						JOIN properties p ON ( st.cid = p.cid AND st.property_id = p.id )
					WHERE
						st.cid = ' . ( int ) $objScheduledTaskSearchFilter->getCid() . '
						AND st.deleted_by IS NULL 
						AND st.deleted_on IS NULL
						AND ( st.task_details ? \'compose_email_id\'
						AND ( st.task_details ->> \'compose_email_id\' ) IS NOT NULL )
						AND st.task_details ->> \'compose_email_id\' != \'\'';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return $arrstrData[0]['count'];
		}

		return 0;
	}

}

?>
