<?php

class CAmenityRateAssociation extends CRateAssociation {

	protected $m_strAmenityId;
	protected $m_strAmenityExternalDescription;
	protected $m_strUnitSpaceName;
	protected $m_strFloorplanName;
	protected $m_strBuildingName;
	protected $m_intPropertyUnitId;
	protected $m_intAmenityTypeId;
	protected $m_strDefaultAmenityName;
	protected $m_intDefaultAmenityId;

	protected $m_arrobjRates;

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['amenity_id'] ) ) {
			$this->setAmenityId( $arrmixValues['amenity_id'] );
		}
		if( true == isset( $arrmixValues['amenity_external_description'] ) ) {
			$this->setAmenityExternalDescription( $arrmixValues['amenity_external_description'] );
		}
		if( true == isset( $arrmixValues['unit_space_name'] ) ) {
			$this->setUnitSpaceName( $arrmixValues['unit_space_name'] );
		}
		if( true == isset( $arrmixValues['floorplan_name'] ) ) {
			$this->setFloorplanName( $arrmixValues['floorplan_name'] );
		}
		if( true == isset( $arrmixValues['property_unit_id'] ) ) {
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		}
		if( true == isset( $arrmixValues['amenity_type_id'] ) ) {
			$this->setAmenityTypeId( $arrmixValues['amenity_type_id'] );
		}
		if( true == isset( $arrmixValues['default_amenity_id'] ) ) {
			$this->setDefaultAmenityId( $arrmixValues['default_amenity_id'] );
		}
		if( true == isset( $arrmixValues['default_amenity_name'] ) ) {
			$this->setDefaultAmenityName( $arrmixValues['default_amenity_name'] );
		}
	}

	/**
	 * Get Functions
	 *
	 */

	public function getAmenityId() {
		return $this->m_strAmenityId;
	}

	public function getAmenityExternalDescription() {
		return $this->m_strAmenityExternalDescription;
	}

	public function getAmenityTypeId() {
		return $this->m_intAmenityTypeId;
	}

	public function getDefaultAmenityId() {
		return $this->m_intDefaultAmenityId;
	}

	public function getDefaultAmenityName() {
		return $this->m_strDefaultAmenityName;
	}

	public function getUnitSpaceName() {
		return $this->m_strUnitSpaceName;
	}

	public function getFloorplanName() {
		return $this->m_strFloorplanName;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getRawApartmentAmenity() {
		$arrstrRawApartmentAmenity = [
			'id'            => $this->getId(),
			'amenityName'   => $this->getAmenityName()
		];
		return $arrstrRawApartmentAmenity;
	}

	public function getRawCommunityAmenity() {
		$arrstrRawCommunityAmenity = [
			'id'            => $this->getId(),
			'amenityName'   => $this->getAmenityName()
		];
		return $arrstrRawCommunityAmenity;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setAmenityId( $strAmenityId ) {
		$this->m_strAmenityId = $strAmenityId;
	}

	public function setAmenityExternalDescription( $strAmenityExternalDescription ) {
		return $this->m_strAmenityExternalDescription = $strAmenityExternalDescription;
	}

	public function setAmenityTypeId( $intAmenityTypeId ) {
		$this->m_intAmenityTypeId = $intAmenityTypeId;
	}

	public function setDefaultAmenityId( $intDefaultAmenityId ) {
		$this->m_intDefaultAmenityId = $intDefaultAmenityId;
	}

	public function setDefaultAmenityName( $strDefaultAmenityName ) {
		$this->m_strDefaultAmenityName = $strDefaultAmenityName;
	}

	public function setUnitSpaceName( $strUnitSpaceName ) {
		$this->m_strUnitSpaceName = $strUnitSpaceName;
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setFloorplanName( $strFloorplanName ) {
		$this->m_strFloorplanName = $strFloorplanName;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
		$this->m_intArCascadeReferenceId = $intArCascadeReferenceId;
	}

	public function setInternalDescription( $strDescription ) {
		$this->m_strDescription = $strDescription;
	}

	/**
	 * Validate functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 0 > $this->m_intCid ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArOriginId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intArOriginId ) || 0 > $this->m_intArOriginId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_origin_id', __( 'ArOrigin id id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArCascadeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intArCascadeId ) || 0 > $this->m_intArCascadeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_cascade_id', __( 'ArCascade id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || 0 > $this->m_intPropertyId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'property id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArCascadeReferenceId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intArCascadeReferenceId ) || 0 > $this->m_intArCascadeReferenceId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_cascade_reference_id', __( 'cascade reference id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArOriginReferenceId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intArOriginReferenceId ) || 0 > $this->m_intArOriginReferenceId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_origin_reference_id', __( 'origin reference id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valExternalVideoUrl() {
		$boolIsValid = true;

		if( true == valStr( $this->m_strExternalVideoUrl ) && false == CValidation::checkUrl( $this->m_strExternalVideoUrl, false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_video_url', CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsFromBulkAmenities = false ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valArCascadeReferenceId();
				$boolIsValid &= $this->valArOriginReferenceId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= true;
				if( false == $boolIsFromBulkAmenities ) {
					$boolIsValid &= $this->valExternalVideoUrl();
				}
				break;

			case 'validate_without_ar_origin_ref_id':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valArOriginId();
				$boolIsValid &= $this->valArCascadeId();
				$boolIsValid &= $this->valArCascadeReferenceId();
				break;

			default:
				parent::validate( $strAction );
				break;
		}

		return $boolIsValid;
	}

	public function fetchRates( $objDatabase ) {

		// $this->m_objRatesCriteria = CRatesCriteria::forAmenities( $this->m_intCid, $this->m_intPropertyId );
		// $this->m_objRatesCriteria->setArOriginReferenceId( $this->getArOriginReferenceId() );

		// $this->m_objRatesLibrary = new CRatesLibrary( $this->m_objRatesCriteria );
		// $this->m_objRatesLibrary->initialize( $this->m_objDatabase );

		// $this->m_arrobjRates = $this->m_objRatesLibrary->fetchRates( $this->m_objDatabase );

		$this->m_arrobjRates = \Psi\Eos\Entrata\CRates::createService()->fetchRatesByArOriginReferenceIdByArOriginIdByCid( $this->getId(), CArOrigin::AMENITY, $this->getCid(), $objDatabase );

		return $this->m_arrobjRates;
	}

}
?>