<?php

class CSpecialRateAssociation extends CRateAssociation {

	/**
	 * Get Functions
	 */

	public function getSpecialId() {
		if( $this->valArOriginId() ) {
			return $this->getArOriginReferenceId();
		}
	}

	/**
	 * Validate functions
	 */

	public function valArOriginId() {
		if( true == is_null( $this->getArOriginId() ) || CArOrigin::SPECIAL != $this->getArOriginId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Special rate association is required.' ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;
		$boolIsValid &= parent::validate( $strAction );

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valArOriginId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Other functions
	 */

	// @FIXME: function name has to be checkDeleteSpecial() - [KAL]

	public function getIsDeleteSpecial( $objDatabase ) {
		$strSql = 'SELECT (
    						SELECT COUNT(sc.id)
    						FROM scheduled_charges sc
    							JOIN rates ra ON ( sc.cid = ra.cid AND sc.rate_id = ra.id AND ra.cid = ' . ( int ) $intCid . ' AND ra.ar_origin_id = ' . ( int ) CArOrigin::SPECIAL . ' )
    						WHERE sc.cid = ' . ( int ) $this->getCid() . ' AND ra.ar_origin_reference_id = ' . ( int ) $this->getArOriginReferenceId() . '
    					  ) + (
							SELECT COUNT(id) FROM lease_associations WHERE cid = ' . ( int ) $this->getCid() . ' AND ( quote_id IS NOT NULL OR renewal_offer_id IS NOT NULL ) AND ar_origin_id = . ' . ( int ) CArOrigin::SPECIAL . ' AND ar_origin_reference_id = ' . ( int ) $this->getArOriginReferenceId() . '
    					  ) + (
							SELECT COUNT(id) FROM renewal_template_offer_specials WHERE cid = ' . ( int ) $this->getCid() . ' AND special_id = ' . ( int ) $this->getArOriginReferenceId() . '
						  ) as count';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( 0 == ( int ) $arrstrData[0]['count'] ) {
			return true;
		}

		return false;
	}

	public function fetchRates( $objDatabase, $arrmixRateFilter = [] ) {
		// use library

		$this->m_arrobjRates = \Psi\Eos\Entrata\CRates::createService()->fetchRatesByArOriginReferenceIdByArOriginIdByCid( $this->getId(), CArOrigin::SPECIAL, $this->getCid(), $objDatabase );
		return $this->m_arrobjRates;
	}

}
?>