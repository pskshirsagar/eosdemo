<?php

class CPetRateAssociation extends CRateAssociation {

	protected $m_intDefaultPetTypeId;
	protected $m_intPropertyUnitId;
	protected $m_intQuoteId;
	protected $m_intMaxPets;

	protected $m_strPetTypeName;
	protected $m_strPetTypeDescription;
	protected $m_strDefaultPetTypeName;
	protected $m_boolIsCountAgainstMaxPetAmount;

	protected $m_arrobjPetRates;
	protected $m_arrobjRates;

	const PETS_ALLOWED_AGAINST_COUNT = 999;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCountAgainstMaxPetAmount = 1;
	}

	/**
	 * Get Functions
	 */

	public function getDefaultPetTypeName() {
		return $this->m_strDefaultPetTypeName;
	}

	public function getPetTypeName() {
		return $this->m_strPetTypeName;
	}

	public function getPetTypeDescription() {
		return $this->m_strPetTypeDescription;
	}

	public function getDefaultPetTypeId() {
		return $this->m_intDefaultPetTypeId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getPetTypeId() {
		return $this->m_intArOriginReferenceId;
	}

	public function getIsCountAgainstMaxPetAmount() {
		return $this->m_boolIsCountAgainstMaxPetAmount;
	}

	public function getPetRates() {
		return $this->m_arrobjPetRates;
	}

	public function getQuoteId() {
		return $this->m_intQuoteId;
	}

	public function getMaxPets() {
		return $this->m_intMaxPets;
	}

	/**
	 * Set functions
	 */

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['default_pet_type_name'] ) ) {
			$this->setDefaultPetTypeName( $arrmixValues['default_pet_type_name'] );
		}
		if( true == isset( $arrmixValues['pet_type_name'] ) ) {
			$this->setPetTypeName( $arrmixValues['pet_type_name'] );
		}
		if( true == isset( $arrmixValues['pet_type_description'] ) ) {
			$this->setPetTypeDescription( $arrmixValues['pet_type_description'] );
		}
		if( true == isset( $arrmixValues['default_pet_type_id'] ) ) {
			$this->setDefaultPetTypeId( $arrmixValues['default_pet_type_id'] );
		}
		if( true == isset( $arrmixValues['property_unit_id'] ) ) {
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		}

		if( true == isset( $arrmixValues['pet_max_allowed'] ) ) {
			( self::PETS_ALLOWED_AGAINST_COUNT == $arrmixValues['pet_max_allowed'] ) ? $this->setIsCountAgainstMaxPetAmount( 0 ) : $this->setIsCountAgainstMaxPetAmount( 1 );
		}

		if( true == isset( $arrmixValues['quote_id'] ) ) {
			$this->setQuoteId( $arrmixValues['quote_id'] );
		}

		if( true == isset( $arrmixValues['max_pets'] ) ) {
			$this->setMaxPets( $arrmixValues['max_pets'] );
		}
	}

	public function setDefaultPetTypeName( $strDefaultPetTypeName ) {
		$this->m_strDefaultPetTypeName = $strDefaultPetTypeName;
	}

	public function setPetTypeName( $strPetTypeName ) {
		$this->m_strPetTypeName = $strPetTypeName;
	}

	public function setPetTypeDescription( $strPetTypeDescription ) {
		$this->m_strPetTypeDescription = $strPetTypeDescription;
	}

	public function setDefaultPetTypeId( $intDefaultPetTypeId ) {
		$this->m_intDefaultPetTypeId = $intDefaultPetTypeId;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setIsCountAgainstMaxPetAmount( $boolIsCountAgainstMaxPetAmount ) {
		$this->m_boolIsCountAgainstMaxPetAmount = $boolIsCountAgainstMaxPetAmount;
	}

	public function setQuoteId( $intQuoteId ) {
		$this->m_intQuoteId = $intQuoteId;
	}

	public function setMaxPets( $intMaxPets ) {
		$this->m_intMaxPets = $intMaxPets;
	}

	/**
	 * Create functions
	 */

	public function createPetRate() {
		// It has to be createRate(). KAL
		$objRate = new CRate();
		$objRate->setCid( $this->m_intCid );
		$objRate->setArOriginId( CArOrigin::PET );
		$objRate->setArOriginReferenceId( $this->getId() );

		return $objRate;
	}

	/**
	 * Validate functions
	 */

	public function valPetMaxAllowed() {
		if( false == $this->getIsCountAgainstMaxPetAmount() ) {
			return true;
		}

		$boolIsValid = true;

		if( true == is_null( $this->getPetMaxAllowed() ) || 0 > $this->getPetMaxAllowed() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_max_allowed', __( 'Please enter proper Max count.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Please enter the proper description.' ) ) );
		}

		return $boolIsValid;
	}

	public function valArOriginReferenceId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getArOriginReferenceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_origin_reference_id', __( 'Please select pet type.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeletePropertyPet( $objDatabase ) {
		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load database.' ) ) );
			return false;
		}

		$arrobjUnitSpaceAssociations = \Psi\Eos\Entrata\Custom\CPetRateAssociations::createService()->fetchPetRateAssociationsByArCascadeIdByArOriginReferenceIdByPetTypeIdByPropertyId( $this->getArOriginReferenceId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
		if( true == valArr( $arrobjUnitSpaceAssociations ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'You cannot delete a pet type that is associated to a unit.' ) ) );
			return false;
		}
		return true;
	}

	public function valDeleteUnitPet( $objDatabase ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load database.' ) ) );
			return false;
		}

		$arrobjLeaseAssociations = \Psi\Eos\Entrata\Custom\CPetLeaseAssociations::createService()->fetchPetLeaseAssociationsByArCascadeIdbyArCascadeReferenceIdByArOriginIdByArOriginReferenceIdByPropertyIdByCid( $this->getArCascadeId(), $this->getArCascadeReferenceId(), CArOrigin::PET, $this->getArOriginReferenceId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjLeaseAssociations ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Pet type is associated with lease, Unable to delete.' ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valArOriginReferenceId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valArOriginReferenceId();
				$boolIsValid &= $this->valPetMaxAllowed();
				break;

			case VALIDATE_DELETE:
				break;

			case 'delete_property_pet':
				$boolIsValid &= $this->valDeletePropertyPet( $objDatabase );
				break;

			case 'delete_unit_pet':
				$boolIsValid &= $this->valDeleteUnitPet( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch functions
	 */

	public function fetchPetRates( $objDatabase ) {
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $this->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $this->getPropertyId() ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::PET ] );
		$objRateLogsFilter->setArOriginReferenceIds( [ $this->getId() ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
		$this->m_arrobjRates = \Psi\Eos\Entrata\CRateLogs::createService()->fetchTempProspectRates( $objDatabase );

		return $this->m_arrobjRates;
	}

	/**
	 * Other functions
	 */

	public function addPetRate( $objPetRate ) {
		$this->m_arrobjPetRates[$objPetRate->getId()] = $objPetRate;
	}

}
?>