<?php

class CAmenityLeaseAssociation extends CLeaseAssociation {

	protected $m_strAmenityName;

	/**
	 * Get Functions
	 */

	public function getAmenityName() {
		return $this->m_strAmenityName;
	}

	/**
	 * Set functions
	 */

	public function setAmenityName( $strAmenityName ) {
		$this->m_strAmenityName = $strAmenityName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrmixValues['amenity_name'] ) && true == $boolDirectSet ) {
			$this->m_strAmenityName = trim( $arrmixValues['amenity_name'] );
		} elseif( true == isset( $arrmixValues['amenity_name'] ) ) {
			$this->setAmenityName( $arrmixValues['amenity_name'] );
		}

	}

	/**
	 * Create functions Validate functions Fetch functions Other functions
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == $boolReturnSqlOnly ) {
			$strSql = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			return $strSql;
		} else {
			$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase );
			$boolIsValid &= $this->logAmenityLeaseAssociationChange( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $strApplicationAction = VALIDATE_INSERT );

			return $boolIsValid;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == $boolReturnSqlOnly ) {
			$strSql = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			return $strSql;
		} else {
			$boolIsValid = parent::delete( $intCurrentUserId, $objDatabase );
			$boolIsValid &= $this->logAmenityLeaseAssociationChange( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $strApplicationAction = VALIDATE_DELETE );

			return $boolIsValid;
		}
	}

	public function logAmenityLeaseAssociationChange( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $strApplicationAction ) {
		$objApplicationLogLibrary = new CApplicationLogLibrary();
		$boolIsValid = $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, $strApplicationAction );

		return $boolIsValid;
	}

	public function logAmenityFileAssociationChange( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $strApplicationAction ) {
		$objApplicationLogLibrary = new CApplicationLogLibrary();
		$boolIsValid = $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, $strApplicationAction );

		return $boolIsValid;
	}
}
?>