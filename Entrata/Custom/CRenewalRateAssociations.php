<?php

class CRenewalRateAssociations extends CRateAssociations {

	public static function fetchBaseRentForLeaseTermsByLease( $objLease, $objDatabase ) {
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $objLease->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $objLease->getPropertyId() ] );
		$objRateLogsFilter->setUnitTypeIds( [ $objLease->getUnitTypeId() ] );
		$objRateLogsFilter->setTriggerIds( [ CArTrigger::MONTHLY ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE ] );
		$objRateLogsFilter->setLeaseIntervalIds( [ $objLease->getActiveLeaseIntervalId() ] );
		$objRateLogsFilter->setPropertyRatesOnly( true );
		$objRateLogsFilter->setIsRenewal( true );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
		$strSql = 'SELECT * FROM renewal_rates';
		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

}