<?php

class CAddOnLeaseAssociation extends CLeaseAssociation {

	protected $m_strAddOnName;
	protected $m_strAgentName;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strAvailableOn;
	protected $m_strAddOnGroupName;
	protected $m_strLeasingAgentName;
	protected $m_strCustomerNameFull;
	protected $m_strAddOnCategoryName;
	protected $m_strLeaseStatusTypeName;
	protected $m_strHoldUntilDate;
	protected $m_strPhoneNumber;
	protected $m_strEmailAddress;
	protected $m_strPropertyName;
	protected $m_strUnitNumber;
	protected $m_strDescription;
	protected $m_strLeaseTermName;
	protected $m_strAddOnGroupRemotePrimaryKey;
	protected $m_strAddOnRemotePrimaryKey;
	protected $m_strAddOnTypeName;
	protected $m_strUserName;
	protected $m_strFullAddOnName;

	protected $m_intRentArCodeId;
	protected $m_intOtherArCodeId;
	protected $m_intDepositArCodeId;
	protected $m_intRentRateId;
	protected $m_intOtherRateId;
	protected $m_intDepositRateId;
	protected $m_intCurrentRentAmount;
	protected $m_intCompanyEmployeeId;
	protected $m_intScheduledChargeId;
	protected $m_intRentScheduledChargeId;
	protected $m_intDepositScheduledChargeId;
	protected $m_intOtherScheduledChargeId;
	protected $m_intScheduledChargeDeletedBy;
	protected $m_intAddOnCategoryId;
	protected $m_intRentRateLogId;
	protected $m_intRateLogId;
	protected $m_intDepositRateLogId;
	protected $m_intOtherRateLogId;
	protected $m_intLeaseTermMonth;
	protected $m_intApplicantId;
	protected $m_intDefaultAddOnCategoryId;

	protected $m_fltDepositAmount;
	protected $m_fltOtherAmount;
	protected $m_fltChargeAmount;

	protected $m_boolHasRecurringChargePosted;
	protected $m_boolIsActive;
	protected $m_boolIsIncludedInInstallment;
	protected $m_boolIsAttachedToUnit;
	protected $m_boolIsStudentSemesterSelectionEnabled;

	protected $m_objLease;
	protected $m_objProperty;
	protected $m_objClient;

	protected $m_arrintRecurringScheduledChargeIds;
	protected $m_arrobjArTransactions;
	protected $m_arrobjRates;
	protected $m_boolHasInventoryFlagForItem;

	const MOVE_IN		 = 1;
	const UPDATE		 = 2;
	const NOTICE		 = 3;
	const MOVE_OUT		 = 4;
	const REVERSAL		 = 5;
	const LOST_OR_RETURN = 6;

	/**
	 * Get Functions
	 */

	public function getAddOnCategoryName() {
		return $this->m_strAddOnCategoryName;
	}

	public function getAgentName() {
		return $this->m_strAgentName;
	}

	public function getCustomerNameFull() {
		return $this->m_strCustomerNameFull;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function getAddOnTypeId() {
		return $this->m_intAddOnTypeId;
	}

	public function getAddOnTypeName() {
		return $this->m_strAddOnTypeName;
	}

	public function getAddOnCategoryId() {
		return $this->m_intAddOnCategoryId;
	}

	public function getDefaultAddOnCategoryId() {
		return $this->m_intDefaultAddOnCategoryId;
	}

	public function getAddOnGroupId() {
		return $this->m_intAddOnGroupId;
	}

	public function getAddOnGroupName() {
		return $this->m_strAddOnGroupName;
	}

	public function getAddOnName() {
		return $this->m_strAddOnName;
	}

	public function getFullAddOnName() {
		return $this->m_strFullAddOnName;
	}

	public function getLeasingAgentName() {
		return $this->m_strLeasingAgentName;
	}

	public function getRentAmount() {
		return $this->m_fltRentAmount;
	}

	public function getDepositAmount() {
		return $this->m_fltDepositAmount;
	}

	public function getOtherAmount() {
		return $this->m_fltOtherAmount;
	}

	public function getCurrentRentAmount() {
		return $this->m_intCurrentRentAmount;
	}

	public function getRentArCodeId() {
		return $this->m_intRentArCodeId;
	}

	public function getDepositArCodeId() {
		return $this->m_intDepositArCodeId;
	}

	public function getOtherArCodeId() {
		return $this->m_intOtherArCodeId;
	}

	public function getRentRateId() {
		return $this->m_intRentRateId;
	}

	public function getDepositRateId() {
		return $this->m_intDepositRateId;
	}

	public function getOtherRateId() {
		return $this->m_intOtherRateId;
	}

	public function getLease() {
		return $this->m_objLease;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getArTransactions() {
		return $this->m_arrobjArTransactions;
	}

	public function getScheduledCharges() {
		return $this->m_arrobjScheduledCharges;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function getAddOnGroupRemotePrimaryKey() {
		return $this->m_strAddOnGroupRemotePrimaryKey;
	}

	public function getAddOnRemotePrimaryKey() {
		return $this->m_strAddOnRemotePrimaryKey;
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function getAvailableOn() {
		return $this->m_strAvailableOn;
	}

	public function getHoldUntilDate() {
		return $this->m_strHoldUntilDate;
	}

	public function getLeaseStatusTypeName() {
		return $this->m_strLeaseStatusTypeName;
	}

	public function getHasRecurringChargePosted() {
		return $this->m_boolHasRecurringChargePosted;
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function getIsIncludedInInstallment() {
		return $this->m_boolIsIncludedInInstallment;
	}

	public function getIsAttachedToUnit() {
		return $this->m_boolIsAttachedToUnit;
	}

	public function getScheduledChargeId() {
		return $this->m_intScheduledChargeId;
	}

	public function getRentScheduledChargeId() {
		return $this->m_intRentScheduledChargeId;
	}

	public function getDepositScheduledChargeId() {
		return $this->m_intDepositScheduledChargeId;
	}

	public function getOtherScheduledChargeId() {
		return $this->m_intOtherScheduledChargeId;
	}

	public function getScheduledChargeDeletedBy() {
		return $this->m_intScheduledChargeDeletedBy;
	}

	public function getChargeAmount() {
		return $this->m_fltChargeAmount;
	}

	public function getRateLogId() {
		return $this->m_intRateLogId;
	}

	public function getRentRateLogId() {
		return $this->m_intRentRateLogId;
	}

	public function getDepositRateLogId() {
		return $this->m_intDepositRateLogId;
	}

	public function getOtherRateLogId() {
		return $this->m_intOtherRateLogId;
	}

	public function getLeaseTermMonth() {
		return $this->m_intLeaseTermMonth;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getRates() {
		return $this->m_arrobjRates;
	}

	/**
	 * Set functions
	 */

	public function setAddOnCategoryName( $strAddOnCategoryName ) {
		$this->m_strAddOnCategoryName = CStrings::strTrimDef( $strAddOnCategoryName, 50, NULL, true );
	}

	public function setAgentName( $strAgentName ) {
		$this->m_strAgentName = CStrings::strTrimDef( $strAgentName, 50, NULL, true );
	}

	public function setCustomerNameFull( $strCustomerNameFull ) {
		$this->m_strCustomerNameFull = CStrings::strTrimDef( $strCustomerNameFull, 50, NULL, true );
	}

	public function setAddOnCategoryId( $intAddOnCategoryId ) {
		$this->m_intAddOnCategoryId = CStrings::strToIntDef( $intAddOnCategoryId );
	}

	public function setDefaultAddOnCategoryId( $intDefaultAddOnCategoryId ) {
		$this->m_intDefaultAddOnCategoryId = CStrings::strToIntDef( $intDefaultAddOnCategoryId );
	}

	public function setAddOnTypeId( $intAddOnTypeId ) {
		$this->m_intAddOnTypeId = CStrings::strToIntDef( $intAddOnTypeId );
	}

	public function setAddOnTypeName( $strAddOnTypeName ) {
		$this->m_strAddOnTypeName = CStrings::strTrimDef( $strAddOnTypeName, 50, NULL, true );
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = $intUnitSpaceId;
	}

	public function setAddOnGroupId( $intAddOnGroupId ) {
		$this->m_intAddOnGroupId = $intAddOnGroupId;
	}

	public function setAddOnGroupName( $strAddOnGroupName ) {
		$this->m_strAddOnGroupName = CStrings::strTrimDef( $strAddOnGroupName, 50, NULL, true );
	}

	public function setAddOnName( $strAddOnName ) {
		$this->m_strAddOnName = CStrings::strTrimDef( $strAddOnName, 50, NULL, true );
	}

	public function setFullAddOnName( $strFullAddOnName ) {
		$this->m_strFullAddOnName = $strFullAddOnName;
	}

	public function setLeasingAgentName( $strLeasingAgentName ) {
		$this->m_strLeasingAgentName = CStrings::strTrimDef( $strLeasingAgentName, 50, NULL, true );
	}

	public function setRentAmount( $fltRentAmount ) {
		$this->m_fltRentAmount = CStrings::strToFloatDef( $fltRentAmount, NULL, false, 2 );
	}

	public function setCurrentRentAmount( $intCurrentRentAmount ) {
		$this->m_intCurrentRentAmount = ( int ) $intCurrentRentAmount;
	}

	public function setDepositAmount( $fltDepositAmount ) {
		$this->m_fltDepositAmount = CStrings::strToFloatDef( $fltDepositAmount, NULL, false, 2 );
	}

	public function setOtherAmount( $fltOtherAmount ) {
		$this->m_fltOtherAmount = CStrings::strToFloatDef( $fltOtherAmount, NULL, false, 2 );
	}

	public function setRentArCodeId( $intRentArCodeId ) {
		$this->m_intRentArCodeId = CStrings::strToIntDef( $intRentArCodeId, NULL, false );
	}

	public function setDepositArCodeId( $intDepositArCodeId ) {
		$this->m_intDepositArCodeId = CStrings::strToIntDef( $intDepositArCodeId, NULL, false );
	}

	public function setOtherArCodeId( $intOtherArCodeId ) {
		$this->m_intOtherArCodeId = CStrings::strToIntDef( $intOtherArCodeId, NULL, false );
	}

	public function setRentRateId( $intRentRateId ) {
		$this->m_intRentRateId = CStrings::strToIntDef( $intRentRateId, NULL, false );
	}

	public function setDepositRateId( $intDepositRateId ) {
		$this->m_intDepositRateId = CStrings::strToIntDef( $intDepositRateId, NULL, false );
	}

	public function setOtherRateId( $intOtherRateId ) {
		$this->m_intOtherRateId = CStrings::strToIntDef( $intOtherRateId, NULL, false );
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = CStrings::strTrimDef( $strPhoneNumber, 50, NULL, true );
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = CStrings::strTrimDef( $strEmailAddress, 50, NULL, true );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = CStrings::strTrimDef( $strUnitNumber, 50, NULL, true );
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = CStrings::strTrimDef( $strDescription, 50, NULL, true );
	}

	public function setAddOnGroupRemotePrimaryKey( $strAddOnGroupRemotePrimaryKey ) {
		$this->m_strAddOnGroupRemotePrimaryKey = CStrings::strTrimDef( $strAddOnGroupRemotePrimaryKey, 50, NULL, true );
	}

	public function setAddOnRemotePrimaryKey( $strAddOnRemotePrimaryKey ) {
		$this->m_strAddOnRemotePrimaryKey = CStrings::strTrimDef( $strAddOnRemotePrimaryKey, 50, NULL, true );
	}

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	public function setAvailableOn( $strAvailableOn ) {
		$this->m_strAvailableOn = CStrings::strTrimDef( $strAvailableOn, 50, NULL, true );
	}

	public function setHoldUntilDate( $strHoldUntilDate ) {
		$this->m_strHoldUntilDate = CStrings::strTrimDef( $strHoldUntilDate, 50, NULL, true );
	}

	public function setLeaseStatusTypeName( $strLeaseStatusTypeName ) {
		$this->m_strLeaseStatusTypeName = $strLeaseStatusTypeName;
	}

	public function setHasRecurringChargePosted( $boolHasRecurringChargePosted ) {
		$this->m_boolHasRecurringChargePosted = $boolHasRecurringChargePosted;
	}

	public function setIsActive( $boolIsActive ) {
		$this->m_boolIsActive = CStrings::strToBool( $boolIsActive );
	}

	public function setIsIncludedInInstallment( $boolIsIncludedInInstallment ) {
		$this->m_boolIsIncludedInInstallment = $boolIsIncludedInInstallment;
	}

	public function setIsAttachedToUnit( $boolIsAttachedToUnit ) {
		$this->m_boolIsAttachedToUnit = CStrings::strToBool( $boolIsAttachedToUnit );
	}

	public function setScheduledChargeId( $intScheduledChargeId ) {
		$this->m_intScheduledChargeId = ( int ) $intScheduledChargeId;
	}

	public function setRentScheduledChargeId( $intRentScheduledChargeId ) {
		$this->m_intRentScheduledChargeId = CStrings::strToIntDef( $intRentScheduledChargeId );
	}

	public function setDepositScheduledChargeId( $intDepositScheduledChargeId ) {
		$this->m_intDepositScheduledChargeId = CStrings::strToIntDef( $intDepositScheduledChargeId );
	}

	public function setOtherScheduledChargeId( $intOtherScheduledChargeId ) {
		$this->m_intOtherScheduledChargeId = CStrings::strToIntDef( $intOtherScheduledChargeId );
	}

	public function setScheduledChargeDeletedBy( $intScheduledChargeDeletedBy ) {
		$this->m_intScheduledChargeDeletedBy = ( int ) $intScheduledChargeDeletedBy;
	}

	public function setChargeAmount( $fltChargeAmount ) {
		$this->m_fltChargeAmount = CStrings::strToFloatDef( $fltChargeAmount, NULL, false, 2 );
	}

	public function setRentRateLogId( $intRentRateLogId ) {
		$this->m_intRentRateLogId = CStrings::strToIntDef( $intRentRateLogId );
	}

	public function setRateLogId( $intRateLogId ) {
		$this->m_intRateLogId = CStrings::strToIntDef( $intRateLogId );
	}

	public function setDepositRateLogId( $intDepositRateLogId ) {
		$this->m_intDepositRateLogId = CStrings::strToIntDef( $intDepositRateLogId );
	}

	public function setOtherRateLogId( $intOtherRateLogId ) {
		$this->m_intOtherRateLogId = CStrings::strToIntDef( $intOtherRateLogId );
	}

	public function setLeaseTermMonth( $intLeaseTermMonth ) {
		$this->m_intLeaseTermMonth = CStrings::strToIntDef( $intLeaseTermMonth );
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setRates( $arrobjRates ) {
		$this->m_arrobjRates = $arrobjRates;
	}

	public function setScheduledCharges( $arrobjScheduledCharges ) {
		$this->m_arrobjScheduledCharges = $arrobjScheduledCharges;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrmixValues['add_on_category_name'] ) && true == $boolDirectSet ) {
			$this->m_strAddOnCategoryName = trim( $arrmixValues['add_on_category_name'] );
		} elseif( true == isset( $arrmixValues['add_on_category_name'] ) ) {
			$this->setAddOnCategoryName( $arrmixValues['add_on_category_name'] );
		}

		if( true == isset( $arrmixValues['unit_space_id'] ) && true == $boolDirectSet ) {
			$this->m_intUnitSpaceId = trim( $arrmixValues['unit_space_id'] );
		} elseif( true == isset( $arrmixValues['unit_space_id'] ) ) {
			$this->setUnitSpaceId( $arrmixValues['unit_space_id'] );
		}

		if( true == isset( $arrmixValues['add_on_type_id'] ) && true == $boolDirectSet ) {
			$this->m_intAddOnTypeId = trim( $arrmixValues['add_on_type_id'] );
		} elseif( true == isset( $arrmixValues['add_on_type_id'] ) ) {
			$this->setAddOnTypeId( $arrmixValues['add_on_type_id'] );
		}

		if( true == isset( $arrmixValues['add_on_category_id'] ) && true == $boolDirectSet ) {
			$this->m_intAddOnCategoryId = trim( $arrmixValues['add_on_category_id'] );
		} elseif( true == isset( $arrmixValues['add_on_category_id'] ) ) {
			$this->setAddOnCategoryId( $arrmixValues['add_on_category_id'] );
		}

		if( true == isset( $arrmixValues['default_add_on_category_id'] ) && true == $boolDirectSet ) {
			$this->m_intDefaultAddOnCategoryId = trim( $arrmixValues['default_add_on_category_id'] );
		} elseif( true == isset( $arrmixValues['default_add_on_category_id'] ) ) {
			$this->setDefaultAddOnCategoryId( $arrmixValues['default_add_on_category_id'] );
		}

		if( true == isset( $arrmixValues['add_on_group_name'] ) && true == $boolDirectSet ) {
			$this->m_strAddOnGroupName = trim( $arrmixValues['add_on_group_name'] );
		} elseif( true == isset( $arrmixValues['add_on_group_name'] ) ) {
			$this->setAddOnGroupName( $arrmixValues['add_on_group_name'] );
		}

		if( true == isset( $arrmixValues['add_on_group_id'] ) && true == $boolDirectSet ) {
			$this->m_intAddOnGroupId = trim( $arrmixValues['add_on_group_id'] );
		} elseif( true == isset( $arrmixValues['add_on_group_id'] ) ) {
			$this->setAddOnGroupId( $arrmixValues['add_on_group_id'] );
		}

		if( true == isset( $arrmixValues['add_on_name'] ) && true == $boolDirectSet ) {
			$this->m_strAddOnName = trim( $arrmixValues['add_on_name'] );
		} elseif( true == isset( $arrmixValues['add_on_name'] ) ) {
			$this->setAddOnName( $arrmixValues['add_on_name'] );
		}

		if( true == isset( $arrmixValues['leasing_agent_name'] ) && true == $boolDirectSet ) {
			$this->m_strAgentName = trim( $arrmixValues['leasing_agent_name'] );
		} elseif( isset( $arrmixValues['leasing_agent_name'] ) ) {
			$this->setAgentName( $arrmixValues['leasing_agent_name'] );
		}

		if( true == isset( $arrmixValues['customer_name_full'] ) && true == $boolDirectSet ) {
			$this->m_strCustomerNameFull = trim( $arrmixValues['customer_name_full'] );
		} elseif( isset( $arrmixValues['customer_name_full'] ) ) {
			$this->setCustomerNameFull( $arrmixValues['customer_name_full'] );
		}

		if( true == isset( $arrmixValues['leasing_agent_name'] ) && true == $boolDirectSet ) {
			$this->m_strLeasingAgentName = trim( $arrmixValues['leasing_agent_name'] );
		} elseif( true == isset( $arrmixValues['leasing_agent_name'] ) ) {
			$this->setLeasingAgentName( $arrmixValues['leasing_agent_name'] );
		}

		if( true == isset( $arrmixValues['available_on'] ) && true == $boolDirectSet ) {
			$this->m_strAvailableOn = trim( $arrmixValues['available_on'] );
		} elseif( true == isset( $arrmixValues['available_on'] ) ) {
			$this->setAvailableOn( $arrmixValues['available_on'] );
		}

		if( true == isset( $arrmixValues['lease_status_type_name'] ) && true == $boolDirectSet ) {
			$this->m_strLeaseStatusTypeName = trim( $arrmixValues['lease_status_type_name'] );
		} elseif( true == isset( $arrmixValues['lease_status_type_name'] ) ) {
			$this->setLeaseStatusTypeName( $arrmixValues['lease_status_type_name'] );
		}

		if( true == isset( $arrmixValues['rent_amount'] ) && true == $boolDirectSet ) {
			$this->m_fltRentAmount = trim( $arrmixValues['rent_amount'] );
		} elseif( true == isset( $arrmixValues['rent_amount'] ) ) {
			$this->setRentAmount( $arrmixValues['rent_amount'] );
		}

		if( true == isset( $arrmixValues['deposit_amount'] ) && true == $boolDirectSet ) {
			$this->m_fltDepositAmount = trim( $arrmixValues['deposit_amount'] );
		} elseif( true == isset( $arrmixValues['deposit_amount'] ) ) {
			$this->setDepositAmount( $arrmixValues['deposit_amount'] );
		}

		if( true == isset( $arrmixValues['other_amount'] ) && true == $boolDirectSet ) {
			$this->m_fltOtherAmount = trim( $arrmixValues['other_amount'] );
		} elseif( true == isset( $arrmixValues['other_amount'] ) ) {
			$this->setOtherAmount( $arrmixValues['other_amount'] );
		}

		if( true == isset( $arrmixValues['rent_ar_code_id'] ) && true == $boolDirectSet ) {
			$this->m_intRentArCodeId = trim( $arrmixValues['rent_ar_code_id'] );
		} elseif( isset( $arrmixValues['rent_ar_code_id'] ) ) {
			$this->setRentArCodeId( $arrmixValues['rent_ar_code_id'] );
		}

		if( true == isset( $arrmixValues['deposit_ar_code_id'] ) && true == $boolDirectSet ) {
			$this->m_intDepositArCodeId = trim( $arrmixValues['deposit_ar_code_id'] );
		} elseif( isset( $arrmixValues['deposit_ar_code_id'] ) ) {
			$this->setDepositArCodeId( $arrmixValues['deposit_ar_code_id'] );
		}
		if( true == isset( $arrmixValues['other_ar_code_id'] ) && true == $boolDirectSet ) {
			$this->m_intOtherArCodeId = trim( $arrmixValues['other_ar_code_id'] );
		} elseif( isset( $arrmixValues['other_ar_code_id'] ) ) {
			$this->setOtherArCodeId( $arrmixValues['other_ar_code_id'] );
		}

		if( true == isset( $arrmixValues['rent_rate_id'] ) && true == $boolDirectSet ) {
			$this->m_intRentRateId = trim( $arrmixValues['rent_rate_id'] );
		} elseif( isset( $arrmixValues['rent_rate_id'] ) ) {
			$this->setRentRateId( $arrmixValues['rent_rate_id'] );
		}

		if( true == isset( $arrmixValues['deposit_rate_id'] ) && true == $boolDirectSet ) {
			$this->m_intDepositRateId = trim( $arrmixValues['deposit_rate_id'] );
		} elseif( isset( $arrmixValues['deposit_rate_id'] ) ) {
			$this->setDepositRateId( $arrmixValues['deposit_rate_id'] );
		}
		if( true == isset( $arrmixValues['other_rate_id'] ) && true == $boolDirectSet ) {
			$this->m_intOtherRateId = trim( $arrmixValues['other_rate_id'] );
		} elseif( isset( $arrmixValues['other_rate_id'] ) ) {
			$this->setOtherRateId( $arrmixValues['other_rate_id'] );
		}

		if( true == isset( $arrmixValues['phone_number'] ) && true == $boolDirectSet ) {
			$this->m_strPhoneNumber = trim( $arrmixValues['phone_number'] );
		} elseif( true == isset( $arrmixValues['phone_number'] ) ) {
			$this->setPhoneNumber( $arrmixValues['phone_number'] );
		}

		if( true == isset( $arrmixValues['customer_email_address'] ) && true == $boolDirectSet ) {
			$this->m_strEmailAddress = trim( $arrmixValues['customer_email_address'] );
		} elseif( true == isset( $arrmixValues['customer_email_address'] ) ) {
			$this->setEmailAddress( $arrmixValues['customer_email_address'] );
		}

		if( true == isset( $arrmixValues['property_name'] ) && true == $boolDirectSet ) {
			$this->m_strPropertyName = trim( $arrmixValues['property_name'] );
		} elseif( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}

		if( true == isset( $arrmixValues['unit_number_cache'] ) && true == $boolDirectSet ) {
			$this->m_strUnitNumber = trim( $arrmixValues['unit_number_cache'] );
		} elseif( true == isset( $arrmixValues['unit_number_cache'] ) ) {
			$this->setUnitNumber( $arrmixValues['unit_number_cache'] );
		}

		if( true == isset( $arrmixValues['description'] ) && true == $boolDirectSet ) {
			$this->m_strDescription = trim( $arrmixValues['description'] );
		} elseif( true == isset( $arrmixValues['description'] ) ) {
			$this->setDescription( $arrmixValues['description'] );
		}

		if( true == isset( $arrmixValues['add_on_group_remote_primary_key'] ) && true == $boolDirectSet ) {
			$this->m_strAddOnGroupRemotePrimaryKey = trim( $arrmixValues['add_on_group_remote_primary_key'] );
		} elseif( isset( $arrmixValues['add_on_group_remote_primary_key'] ) ) {
			$this->setAddOnGroupRemotePrimaryKey( $arrmixValues['add_on_group_remote_primary_key'] );
		}

		if( true == isset( $arrmixValues['add_on_remote_primary_key'] ) && true == $boolDirectSet ) {
			$this->m_strAddOnRemotePrimaryKey = trim( $arrmixValues['add_on_remote_primary_key'] );
		} elseif( isset( $arrmixValues['add_on_remote_primary_key'] ) ) {
			$this->setAddOnRemotePrimaryKey( $arrmixValues['add_on_remote_primary_key'] );
		}

		if( true == isset( $arrmixValues['scheduled_charge_id'] ) && true == $boolDirectSet ) {
			$this->m_intScheduledChargeId = trim( $arrmixValues['scheduled_charge_id'] );
		} elseif( isset( $arrmixValues['scheduled_charge_id'] ) ) {
			$this->setScheduledChargeId( $arrmixValues['scheduled_charge_id'] );
		}

		if( true == isset( $arrmixValues['scheduled_charge_deleted_by'] ) && true == $boolDirectSet ) {
			$this->m_intScheduledChargeDeletedBy = trim( $arrmixValues['scheduled_charge_deleted_by'] );
		} elseif( isset( $arrmixValues['scheduled_charge_deleted_by'] ) ) {
			$this->setScheduledChargeDeletedBy( $arrmixValues['scheduled_charge_deleted_by'] );
		}

		if( true == isset( $arrmixValues['is_included_in_installment'] ) && true == $boolDirectSet ) {
			$this->m_boolIsIncludedInInstallment = trim( $arrmixValues['is_included_in_installment'] );
		} elseif( isset( $arrmixValues['is_included_in_installment'] ) ) {
			$this->setIsIncludedInInstallment( $arrmixValues['is_included_in_installment'] );
		}

		if( true == isset( $arrmixValues['is_attached_to_unit'] ) && true == $boolDirectSet ) {
			$this->m_boolIsAttachedToUnit = trim( $arrmixValues['is_attached_to_unit'] );
		} elseif( isset( $arrmixValues['is_attached_to_unit'] ) ) {
			$this->setIsAttachedToUnit( $arrmixValues['is_attached_to_unit'] );
		}

		if( true == isset( $arrmixValues['is_active'] ) && true == $boolDirectSet ) {
			$this->m_boolIsActive = trim( $arrmixValues['is_active'] );
		} elseif( isset( $arrmixValues['is_active'] ) ) {
			$this->setIsActive( $arrmixValues['is_active'] );
		}

		if( true == isset( $arrmixValues['current_rent_amount'] ) && true == $boolDirectSet ) {
			$this->m_fltChargeAmount = trim( $arrmixValues['current_rent_amount'] );
		} elseif( true == isset( $arrmixValues['current_rent_amount'] ) ) {
			$this->setChargeAmount( $arrmixValues['current_rent_amount'] );
		}

		if( true == isset( $arrmixValues['rent_rate_log_id'] ) ) {
			$this->setRentRateLogId( $arrmixValues['rent_rate_log_id'] );
		}
		if( true == isset( $arrmixValues['rate_log_id'] ) ) {
			$this->setRateLogId( $arrmixValues['rate_log_id'] );
		}
		if( true == isset( $arrmixValues['deposit_rate_log_id'] ) ) {
			$this->setDepositRateLogId( $arrmixValues['deposit_rate_log_id'] );
		}
		if( true == isset( $arrmixValues['other_rate_log_id'] ) ) {
			$this->setOtherRateLogId( $arrmixValues['other_rate_log_id'] );
		}
		if( true == isset( $arrmixValues['lease_term_month'] ) ) {
			$this->setLeaseTermMonth( $arrmixValues['lease_term_month'] );
		}
		if( true == isset( $arrmixValues['lease_term_name'] ) ) {
			$this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		}
		if( true == isset( $arrmixValues['rent_scheduled_charge_id'] ) ) {
			$this->setRentScheduledChargeId( $arrmixValues['rent_scheduled_charge_id'] );
		}
		if( true == isset( $arrmixValues['deposit_scheduled_charge_id'] ) ) {
			$this->setDepositScheduledChargeId( $arrmixValues['deposit_scheduled_charge_id'] );
		}
		if( true == isset( $arrmixValues['other_scheduled_charge_id'] ) ) {
			$this->setOtherScheduledChargeId( $arrmixValues['other_scheduled_charge_id'] );
		}
		if( true == isset( $arrmixValues['add_on_type_name'] ) ) {
			$this->setAddOnTypeName( $arrmixValues['add_on_type_name'] );
		}
		if( true == isset( $arrmixValues['username'] ) ) {
			$this->setUserName( $arrmixValues['username'] );
		}

		if( true == isset( $arrmixValues['full_add_on_name'] ) ) {
			$this->setFullAddOnName( $arrmixValues['full_add_on_name'] );
		}

	}

	public function addRate( $objRate ) {
		$this->m_arrobjRates[$objRate->getId()] = $objRate;
	}

	public function setLeaseAssociationLocks( $strLeaseStartDate, $strLeaseEndDate ) {
		if( strtotime( $this->getStartDate() ) <> strtotime( $strLeaseStartDate ) ) {
			$this->setUseLeaseStartDate( false );
		} else {
			$this->setUseLeaseStartDate( true );
		}

		if( strtotime( $this->getEndDate() ) <> strtotime( $strLeaseEndDate ) ) {
			$this->setUseLeaseMoveOutDate( false );
		} else {
			$this->setUseLeaseMoveOutDate( true );
		}

		if( false == $this->getUseLeaseStartDate() || false == $this->getUseLeaseMoveOutDate() ) {
			$this->setLockToLeaseDates( false );
		} else {
			$this->setLockToLeaseDates( true );
		}
	}

	/**
	 * Create functions
	 */

	public function createOldScheduledCharge( $boolIsDeposit = false, $boolIsReservationFee = false, $objDatabase ) {

		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setLeaseId( $this->getLeaseId() );
		$objScheduledCharge->setArCascadeId( CArCascade::PROPERTY );
		$objScheduledCharge->setArCascadeReferenceId( $this->getPropertyId() );
		$objScheduledCharge->setArOriginId( CArOrigin::ADD_ONS );
		$objScheduledCharge->setArOriginReferenceId( $this->getArOriginReferenceId() );
		$objScheduledCharge->setArFormulaId( CArFormula::FIXED_AMOUNT );
		$objScheduledCharge->setScheduledChargeTypeId( CScheduledChargeType::STANDARD );
		$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::MANUAL_CHANGE );
		$objScheduledCharge->setLeaseAssociationId( $this->getId() );

		if( false == $boolIsDeposit && false == $boolIsReservationFee ) {
			$objScheduledCharge->setArTriggerId( CArTrigger::MONTHLY );
			$objScheduledCharge->setMonthToMonthAmount( $this->getRentAmount() );
			$objScheduledCharge->setArCodeId( $this->getRentArCodeId() );
			$objScheduledCharge->setRateId( $this->getRentRateId() );
			$objScheduledCharge->setChargeAmount( $this->getRentAmount() );
			$strBillingEndDate = NULL;

			if( true == $this->m_boolIsStudentSemesterSelectionEnabled && true == valId( $this->m_objLease->getLeaseStartWindowId() ) ) {
				$strBillingEndDate = CLeaseStartWindows::fetchBillingEndDateByLeaseTermIdByPropertyIdByCid( $this->m_objLease->getLeaseStartWindowId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
			}

			if( true == valStr( $strBillingEndDate ) ) {
				$strBillingEndDate = ( strtotime( $this->getStartDate() ) >= strtotime( $strBillingEndDate ) ) ? $this->getEndDate() : $strBillingEndDate;
				$objScheduledCharge->setChargeEndDate( $strBillingEndDate );
				$objScheduledCharge->setEndsWithMoveOut( false );
			}
		} elseif( true == $boolIsDeposit ) {
			$objScheduledCharge->setArTriggerId( CArTrigger::MOVE_IN );
			$objScheduledCharge->setChargeEndDate( NULL );
			$objScheduledCharge->setArCodeId( $this->getDepositArCodeId() );
			$objScheduledCharge->setRateId( $this->getDepositRateId() );
			$objScheduledCharge->setChargeAmount( $this->getDepositAmount() );
			$objScheduledCharge->setEndsWithMoveOut( true );
		} elseif( true == $boolIsReservationFee ) {

			if( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
				$objScheduledCharge->setArTriggerId( CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT );
			} else {
				$objScheduledCharge->setArTriggerId( CArTrigger::APPLICATION_COMPLETED );
			}
			$objScheduledCharge->setChargeEndDate( NULL );
			$objScheduledCharge->setEndsWithMoveOut( true );
			$objScheduledCharge->setArCodeId( $this->getOtherArCodeId() );
			$objScheduledCharge->setRateId( $this->getOtherRateId() );
			$objScheduledCharge->setChargeAmount( $this->getOtherAmount() );
		}

		$objScheduledCharge->setChargeStartDate( $this->getStartDate() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );
		$objScheduledCharge->setCustomerId( $this->getCustomerId() );

		if( false == $this->m_boolIsStudentSemesterSelectionEnabled && CArTriggerType::RECURRING_LEASE_CHARGES == $objScheduledCharge->getArTriggerTypeId() ) {
		    $objScheduledCharge->setEndsWithMoveOut( true );
		}

		return $objScheduledCharge;
	}

	public function createScheduledCharge( $boolIsDeposit = false, $boolIsReservationFee = false, $objDatabase, $intRentArTriggerId = CArTrigger::MONTHLY ) {

		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setLeaseId( $this->getLeaseId() );
		$objScheduledCharge->setArCascadeId( CArCascade::PROPERTY );
		$objScheduledCharge->setArCascadeReferenceId( $this->getPropertyId() );
		$objScheduledCharge->setArOriginId( CArOrigin::ADD_ONS );
		$objScheduledCharge->setArOriginReferenceId( $this->getArOriginReferenceId() );
		$objScheduledCharge->setArFormulaId( CArFormula::FIXED_AMOUNT );
		if( true === valObj( $this->m_objLease, 'CLease' ) ) {
			if( COccupancyType::COMMERCIAL === $this->m_objLease->getOccupancyTypeId() ) {
				$objScheduledCharge->setScheduledChargeTypeId( CScheduledChargeType::LEASE_ABSTRACT );
			} else {
				$objScheduledCharge->setScheduledChargeTypeId( CScheduledChargeType::STANDARD );
			}
		}
		$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::MANUAL_CHANGE );
		$objScheduledCharge->setLeaseAssociationId( $this->getId() );

		if( false == $boolIsDeposit && false == $boolIsReservationFee ) {
			$objScheduledCharge->setArTriggerId( $intRentArTriggerId );
			$strBillingEndDate = NULL;

			if( true == $this->m_boolIsStudentSemesterSelectionEnabled && true == valId( $this->m_objLease->getLeaseStartWindowId() ) ) {
				$objLeaseInterval  = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByIdByCid( $this->getLeaseIntervalId(), $this->m_objLease->getCid(), $objDatabase );
				$strBillingEndDate = CLeaseStartWindows::fetchBillingEndDateByLeaseTermIdByPropertyIdByCid( $objLeaseInterval->getLeaseStartWindowId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
			}

			if( true == valStr( $strBillingEndDate ) ) {
				$strBillingEndDate = ( strtotime( $this->getStartDate() ) >= strtotime( $strBillingEndDate ) ) ? $this->getEndDate() : $strBillingEndDate;
				$objScheduledCharge->setChargeEndDate( $strBillingEndDate );
				$objScheduledCharge->setEndsWithMoveOut( false );
			}
		} elseif( true == $boolIsDeposit ) {
			$objScheduledCharge->setArTriggerId( CArTrigger::MOVE_IN );
			$objScheduledCharge->setChargeEndDate( NULL );
			$objScheduledCharge->setEndsWithMoveOut( true );
		} elseif( true == $boolIsReservationFee ) {

			if( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
				$objScheduledCharge->setArTriggerId( CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT );
			} else {
				if( CAddOnType::SERVICES == $this->getAddOnTypeId() ) {
					$objScheduledCharge->setArTriggerId( $intRentArTriggerId );
				} else {
					$objScheduledCharge->setArTriggerId( CArTrigger::APPLICATION_COMPLETED );
				}
			}
			$objScheduledCharge->setChargeEndDate( NULL );
			$objScheduledCharge->setEndsWithMoveOut( true );
		}

		$objScheduledCharge->setChargeStartDate( $this->getStartDate() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );
		$objScheduledCharge->setCustomerId( $this->getCustomerId() );

		if( false == $this->m_boolIsStudentSemesterSelectionEnabled && CArTriggerType::RECURRING_LEASE_CHARGES == $objScheduledCharge->getArTriggerTypeId() ) {
			$objScheduledCharge->setEndsWithMoveOut( true );
		}

		return $objScheduledCharge;
	}

	/**
	 * Validate functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is required.' ) ) );
		}

		if( true == $boolIsValid ) {

			$mixValidatedStartDate = CValidation::checkISODateFormat( $this->m_strStartDate, $boolFormat = true );
			if( false == $mixValidatedStartDate ) {
				$this->m_strStartDate = NULL;
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date must be in correct date format.' ) ) );
			} else {
				$this->setStartDate( $mixValidatedStartDate );
			}
		}

		return $boolIsValid;
	}

	public function valEndDate() {

		$boolIsValid = true;

		$mixValidatedEndDate = CValidation::checkISODateFormat( $this->m_strEndDate, $boolFormat = true );

		if( true == isset( $this->m_strEndDate ) && 0 < strlen( $this->m_strEndDate ) && false == $mixValidatedEndDate ) {
			$this->m_strEndDate = NULL;
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date must be in correct date format.' ) ) );
		} elseif( true == isset( $mixValidatedEndDate ) ) {
			$this->setEndDate( $mixValidatedEndDate );
		}

		if( true == $boolIsValid && true == valStr( $this->m_strStartDate ) && true == valStr( $this->m_strEndDate ) && strtotime( $this->m_strStartDate ) > strtotime( $this->m_strEndDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date must be greater than or equal to start date.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $intAllowFlexibledates = NULL, $strLeaseEndDate = NULL, $strAvailableOn = NULL, $strHoldUntilDate = NULL, $boolIsLeasingAgentReq = false, $boolIsInsert = false ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_lease_add_on_lease_association':
				$boolIsValid &= $this->valReservationDates( $objDatabase, $intAllowFlexibledates );
				$boolIsValid &= $this->valChargeCodes();
				$boolIsValid &= $this->valCompanyEmployee( $boolIsLeasingAgentReq );
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valMiscellaneousData( $strLeaseEndDate, $strAvailableOn, $strHoldUntilDate );
				}

				if( true == $boolIsValid && true == $boolIsInsert ) {
					$boolIsValid &= $this->valExistingAddOnLeaseAssociation( $objDatabase, $intAllowFlexibledates );
				}
				break;

				// MEGA RATE:: NEED TO CHANGE CODE
			case 'validate_lease_assignable_item_reservation':
				$boolIsValid &= $this->valCompanyEmployee( $boolIsLeasingAgentReq );
				break;

			case 'update_add_on_lease_association':
				$boolIsValid &= $this->valReservationDates( $objDatabase, $intAllowFlexibledates );
				$boolIsValid &= $this->valCompanyEmployee( $boolIsLeasingAgentReq );
				$boolIsValid &= $this->valChargeCodes();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valMiscellaneousData( $strLeaseEndDate, $strAvailableOn, $strHoldUntilDate );
				}
				break;

			case 'validate_lead_add_on_lease_association':
				$boolIsValid &= $this->valReservationDates( $objDatabase, $intAllowFlexibledates );
				$boolIsValid &= $this->valChargeCodes();
				$boolIsValid &= $this->valCompanyEmployee( $boolIsLeasingAgentReq );
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valMiscellaneousData( $strLeaseEndDate, $strAvailableOn, $strHoldUntilDate );
				}
				break;

			case 'validate_renewal_add_on_lease_association':
				$boolIsValid &= $this->valReservationDates( $objDatabase, $intAllowFlexibledates );
				$boolIsValid &= $this->valRenewalAmount();
				break;

				// MEGA RATE:: NEED TO CHANGE CODE
			case 'validate_renewal_assignable_item_reservation':
				$boolIsValid &= $this->valRenewalAmount();
				break;

			case 'validate_current_or_notice_add_on_lease_association':
				$boolIsValid &= $this->valRenewalAmount();
				break;

			case 'validate_add_on_available_on':
				$boolIsValid &= $this->validateAddOnAvailableOn( $objDatabase );
				break;

			case 'validate_add_on_lease_association_notice':
				$boolIsValid &= $this->valCancelOrPlaceOnNoticeReservation( $strAvailableOn, true, $strLeaseEndDate, $objDatabase );
				break;

			case 'validate_add_on_lease_association_move_in':
				$boolIsValid &= $this->valReservationDates( $objDatabase, $intAllowFlexibledates, $boolIsOnlyStartDate = true );
				$boolIsValid &= $this->valConflictingAddOnLeaseAssociation( $objDatabase );
				break;

			default:
				// Added default case
				break;
		}

		return $boolIsValid;
	}

	public function valReservationDates( $objDatabase, $intAllowFlexibledates, $boolIsOnlyStartDate = false ) {

		$boolIsValid = true;

		if( false == $boolIsOnlyStartDate && true == is_null( $this->m_strStartDate ) && true == is_null( $this->m_strEndDate ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select reservation start date and end date.' ), NULL ) );
			$boolIsValid &= false;
		} else {

			if( true == $boolIsValid && true == is_null( $this->m_strStartDate ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select reservation start date. ' ), NULL ) );
				$boolIsValid = false;
			}

			if( true == $boolIsValid ) {
				$boolIsValid &= $this->valStartDate();
			}

			if( false == $boolIsOnlyStartDate ) {
				if( true == is_null( $this->m_strEndDate ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select reservation end date. ' ), NULL ) );
					$boolIsValid = false;
				}

				$boolIsValid &= $this->valEndDate();
			}
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->valAddOnAvailability( $objDatabase, $intAllowFlexibledates );
		}

		return $boolIsValid;
	}

	public function valChargeCodes() {

		$boolIsValid = true;

		if( 0 >= $this->m_fltRentAmount && false == is_null( $this->m_intRentArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Rent amount is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( false == is_null( $this->m_fltRentAmount ) && 0 < $this->m_fltRentAmount && true == is_null( $this->m_intRentArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Rent charge code is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( false == is_null( $this->m_fltDepositAmount ) && 0 < $this->m_fltDepositAmount && true == is_null( $this->m_intDepositArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Deposit charge code is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( ( ( true == is_null( $this->m_fltDepositAmount ) || 0 == $this->m_fltDepositAmount ) ) && false == is_null( $this->m_intDepositArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Deposit amount is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( false == is_null( $this->m_fltOtherAmount ) && 0 < $this->m_fltOtherAmount && true == is_null( $this->m_intOtherArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Reservation charge code is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( ( ( true == is_null( $this->m_fltOtherAmount ) || 0 == $this->m_fltOtherAmount ) ) && false == is_null( $this->m_intOtherArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Reservation amount is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDepositAmount() {

		$boolIsValid = true;

		if( $this->m_fltDepositAmount <= 0 ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Deposit amount cannot be 0. ' ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valOtherAmount() {

		$boolIsValid = true;

		if( $this->m_fltOtherAmount <= 0 ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Reservation fee cannot be 0. ' ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCompanyEmployee( $boolIsLeasingAgentReq = false ) {

		$boolIsValid = true;

		if( true == $boolIsLeasingAgentReq && true == is_null( $this->getCompanyEmployeeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No leasing agent assigned/associated.' ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMiscellaneousData( $strLeaseEndDate = NULL, $strAvailableOn = NULL, $strHoldUntilDate = NULL ) {

		$boolIsValid = true;

		if( true == valStr( $this->getStartDate() ) && true == valStr( $strLeaseEndDate ) && strtotime( $this->getStartDate() ) > strtotime( $strLeaseEndDate ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date cannot be greater than lease end date.' ) ) );
			$boolIsValid &= false;
		}

		if( true == valStr( $this->getEndDate() ) && true == valStr( $strLeaseEndDate ) && strtotime( $this->getEndDate() ) > strtotime( $strLeaseEndDate ) && true == $this->getUseLeaseMoveOutDate() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date cannot be greater than lease end date.' ) ) );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && true == valStr( $strHoldUntilDate ) && strtotime( $this->getStartDate() ) <= strtotime( $strHoldUntilDate ) && true == is_null( $this->getId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hold_until_date', __( 'This item will not be available until {%t,0,DATE_NUMERIC_STANDARD}.', [ strtotime( ' + 1 days ', strtotime( $strHoldUntilDate ) ) ] ) ) );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && true == valStr( $strAvailableOn ) && strtotime( $this->getStartDate() ) < strtotime( $strAvailableOn ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on_date', 'This item will not be available until {%t,0,DATE_NUMERIC_STANDARD}.', [ strtotime( $strAvailableOn ) ] ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valRenewalAmount() {
		$boolIsValid = true;

		if( ( 0 == $this->m_fltRentAmount || 0 >= $this->m_fltRentAmount ) && false == is_null( $this->m_intRentArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Rent amount is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( false == is_null( $this->m_fltRentAmount ) && 0 < $this->m_fltRentAmount && true == is_null( $this->m_intRentArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Rent charge code is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( false == is_null( $this->m_fltDepositAmount ) && 0 < $this->m_fltDepositAmount && true == is_null( $this->m_intDepositArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Deposit charge code is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( ( ( true == is_null( $this->m_fltDepositAmount ) || 0 == $this->m_fltDepositAmount ) ) && false == is_null( $this->m_intDepositArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Deposit amount is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( false == is_null( $this->m_fltOtherAmount ) && 0 < $this->m_fltOtherAmount && true == is_null( $this->m_intOtherArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Reservation charge code is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		if( ( ( true == is_null( $this->m_fltOtherAmount ) || 0 == $this->m_fltOtherAmount ) ) && false == is_null( $this->m_intOtherArCodeId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Reservation amount is required. ' ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validateAddOnAvailableOn( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_numeric( $this->getArOriginReferenceId() ) && 0 < $this->getArOriginReferenceId() ) {
			$objAddOn = \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnByIdByCid( $this->getArOriginReferenceId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objAddOn, \CAddOn::class ) && false == is_null( $objAddOn->getAvailableOn() ) && strtotime( $this->getStartDate() ) < strtotime( $objAddOn->getAvailableOn() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Rentable Item {%s,0} is not available until {%t,1,DATE_NUMERIC_STANDARD}', [ $objAddOn->getName(), $objAddOn->getAvailableOn() ] ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valCancelOrPlaceOnNoticeReservation( $strAvailableOn, $boolIsAvailableOnRequired = false, $strLeaseEndDate = NULL, $objDatabase ) {

		$boolIsValid = true;

		if( ( CLeaseStatusType::NOTICE == $this->getLeaseStatusTypeId() || CLeaseStatusType::PAST == $this->getLeaseStatusTypeId() ) && ( CAddOnType::ASSIGNABLE_ITEMS != $this->getAddOnTypeId() ) && true == $this->getIsActive() ) {
			if( true == $boolIsAvailableOnRequired && false == valStr( $strAvailableOn ) && 0 < $this->getArOriginReferenceId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on', __( 'Rentable Item available on date is required.' ) ) );
				$boolIsValid &= false;
			}

			if( true == is_null( $this->getEndDate() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'Reservation end date is required.' ) ) );
				$boolIsValid &= false;
			}

			if( true == $boolIsValid && true == valStr( $strAvailableOn ) && strtotime( $strAvailableOn ) <= strtotime( $this->getEndDate() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on', __( 'Rentable Item available on date cannot be less than and equal to reservation end date.' ) ) );
				$boolIsValid &= false;
			}

			if( true == is_null( $this->getEndDate() ) && true == $boolIsValid && true == valStr( $strAvailableOn ) && strtotime( $strAvailableOn ) <= strtotime( $this->getStartDate() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on', __( 'Rentable Item available on date cannot be less than and equal to reservation start date.' ) ) );
				$boolIsValid &= false;
			}

			if( true == $boolIsValid && CLeaseStatusType::NOTICE == $this->getLeaseStatusTypeId() && true == valStr( $this->getArOriginReferenceId() ) ) {

				$objConflictingAddOnLeaseAssociation = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchConflictingAddOnLeaseAssociationByAddOnIdByLeaseAssociationStartDateByEndDateByCid( $this->getArOriginReferenceId(), $this->getStartDate(), $this->getEndDate(), $this->getCid(), $objDatabase, $this->getId() );

				if( true == valObj( $objConflictingAddOnLeaseAssociation, \CAddOnLeaseAssociation::class ) ) {
					$boolIsValid = false;
					$strErrorMsg = __( 'This item is currently reserved starting <b style="font-weight: bold;">{%t,0,DATE_NUMERIC_STANDARD}</b>.Please choose an earlier end date.', [ $objConflictingAddOnLeaseAssociation->getStartDate() ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', $strErrorMsg ) );
				}
			}

			if( true == $boolIsValid && false == $this->getIsAttachedToUnit() && ( CLeaseStatusType::PAST == $this->getLeaseStatusTypeId() || CLeaseStatusType::NOTICE == $this->getLeaseStatusTypeId() ) && true == valStr( $this->getArOriginReferenceId() ) ) {

				$objAddOnAvailableOn = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationByAddOnIdByAddOnAvailableOnDateByLeaseAssociationStartDate( $this->getArOriginReferenceId(), $this->getAvailableOn(), $this->getCid(), $objDatabase, $this->getId() );

				if( true == valObj( $objAddOnAvailableOn, \CAddOnLeaseAssociation::class ) ) {
					$boolIsValid = false;
					$strErrorMsg = __( 'This item is currently reserved starting <b style="font-weight: bold;">{%t,0,DATE_NUMERIC_STANDARD}</b>.Please choose an earlier available on date.', [ $objAddOnAvailableOn->getStartDate() ] );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on_date', $strErrorMsg ) );
				}
			}
		}

		if( true == $boolIsValid && CLeaseStatusType::PAST == $this->getLeaseStatusTypeId() && true == valStr( $this->getEndDate() ) && strtotime( $this->getEndDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'Reservation end date cannot be greater than today date.' ) ) );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && true == valStr( $this->getEndDate() ) && strtotime( $this->getEndDate() ) > strtotime( $strLeaseEndDate ) && true == $this->getUseLeaseMoveOutDate() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'Reservation end date cannot be greater than lease move-out / end date.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valConflictingAddOnLeaseAssociation( $objDatabase ) {

		$boolIsValid = true;

		if( true == valStr( $this->getArOriginReferenceId() ) ) {
			$arrobjAddOnLeaseAssociations = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationsByAddOnIdByLeaseStatuTypeIdsByCid( $this->getArOriginReferenceId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ], $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjAddOnLeaseAssociations ) ) {
				$objCurrentAddOnLeaseAssociation  = current( $arrobjAddOnLeaseAssociations );

				if( true == valObj( $objCurrentAddOnLeaseAssociation, \CAddOnLeaseAssociation::class ) && false == is_null( $objCurrentAddOnLeaseAssociation->getEndDate() ) && strtotime( $objCurrentAddOnLeaseAssociation->getEndDate() ) < strtotime( date( 'm/d/Y' ) ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'This item is currently reserved please move-out current Rentable Item reservation first in order to perform move-in' ) ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valAddOnAvailability( $objDatabase, $boolIsAllowFlexibledates = false ) {

		$boolIsValid = true;

		if( false == $this->validateAddOnAvailableOn( $objDatabase ) ) {
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && true == valStr( $this->getArOriginReferenceId() ) ) {

			$objConflictingAddOnLeaseAssociation = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchConflictingAddOnLeaseAssociationByAddOnIdByLeaseAssociationStartDateByCid( $this->getArOriginReferenceId(), $this->getStartDate(), $this->getCid(), $objDatabase, $this->getId() );

			if( true == valObj( $objConflictingAddOnLeaseAssociation, \CAddOnLeaseAssociation::class ) ) {
				$boolIsValid = false;
				$strErrorMsg = '';
				if( false == is_null( $objConflictingAddOnLeaseAssociation->getEndDate() ) && true == $boolIsAllowFlexibledates ) {
					$strErrorMsg = __( 'This item is currently reserved until <b style="font-weight: bold;">{%t,0,DATE_NUMERIC_STANDARD}</b>. Please choose a later start date.', [ $objConflictingAddOnLeaseAssociation->getEndDate() ] );
				} elseif( true == is_null( $objConflictingAddOnLeaseAssociation->getEndDate() ) && true == $boolIsAllowFlexibledates ) {
					$strErrorMsg = __( 'Please choose a later start date.' );
				} elseif( false == is_null( $objConflictingAddOnLeaseAssociation->getEndDate() ) && false == $boolIsAllowFlexibledates ) {
					$strErrorMsg = __( 'This item is currently reserved until <b style="font-weight: bold;">{%t,0,DATE_NUMERIC_STANDARD}</b>.', [ $objConflictingAddOnLeaseAssociation->getEndDate() ] );
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', $strErrorMsg ) );
			}
		}

		if( true == $boolIsValid && true == valStr( $this->getArOriginReferenceId() ) ) {

			$objConflictingAddOnLeaseAssociation = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchConflictingAddOnLeaseAssociationByAddOnIdByLeaseAssociationStartDateByEndDateByCid( $this->getArOriginReferenceId(), $this->getStartDate(), $this->getEndDate(), $this->getCid(), $objDatabase, $this->getId() );

			if( true == valObj( $objConflictingAddOnLeaseAssociation, \CAddOnLeaseAssociation::class ) ) {
				$boolIsValid = false;
				$strErrorMsg = __( 'This item is currently reserved starting <b style="font-weight: bold;">{%t,0,DATE_NUMERIC_STANDARD}</b>.', [ $objConflictingAddOnLeaseAssociation->getStartDate() ] );
				$strErrorMsg .= ( true == $boolIsAllowFlexibledates ) ? __( 'Please choose an earlier end date.' ) : '';
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', $strErrorMsg ) );
			}
		}

		return $boolIsValid;
	}

	/**
	 * Fetch functions
	 */

	public function updateAddOnStatus( $intCurrentUserId, $objDatabase ) {

		$boolIsValid = true;
		if( true == is_numeric( $this->getArOriginReferenceId() ) && 0 < $this->getArOriginReferenceId() ) {
			$objAddOn = \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnByIdByCid( $this->getArOriginReferenceId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objAddOn, \CAddOn::class ) ) {

				if( false == is_null( $this->getAvailableOn() ) ) {
					$objAddOn->setAvailableOn( $this->getAvailableOn() );
				} else {
					$objAddOn->setAvailableOn( NULL );
				}

				if( CLeaseStatusType::CANCELLED == $this->getLeaseStatusTypeId() ) {
					$objAddOn->setHoldUntilDate( NULL );
				}

				if( false == $objAddOn->update( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsg( $objAddOn->getErrorMsgs() );
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function setReservationCustomerId( $objDatabase ) {
		if( true == is_null( $this->getCustomerId() ) ) {
			$arrintResponse = fetchData( 'SELECT customer_id FROM lease_associations WHERE cid = ' . ( int ) $this->getCid() . ' AND id = ' . $this->getId(), $objDatabase );

			if( true == valArr( $arrintResponse ) ) {
				$this->setCustomerId( $arrintResponse[0]['customer_id'] );
			}
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolIsFromQuote = false ) {

		if( false == parent::insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		$objAddOn = \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnByIdByCid( $this->getArOriginReferenceId(), $this->getCid(), $objDatabase );

		if( true == $objAddOn->getIsActive() && CAddOnType::SERVICES != $this->getAddOnTypeId() && false == $this->updateAddOnStatus( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		if( false == is_null( $this->getLeaseId() ) && false == $boolIsFromQuote ) {
			$objEventLibrary = $this->logActivityForAddOnLeaseAssociation( $objDatabase, CEventSubType::ADD_ON_ADDED, $intCurrentUserId );

			if( true == valObj( $objEventLibrary, \CEventLibrary::class ) && false == $objEventLibrary->insertEvent( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolUpdateRecurringCharge = true ) {
		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolUpdateRecurringCharge );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolUpdateRecurringCharge = true, $boolIsFromQuote = false, $boolUpdateChargeAmount = true, $boolIsFromShuffle = false, $boolIsFromBulkPlaceOnNotice = false, $boolIsSkipEventLog = false ) {

		$this->setReservationCustomerId( $objDatabase );

		$objOldAddOnLeaseAssociation = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchNewAddOnLeaseAssociationByIdByLeaseIdByPropertyIdByCid( $this->getId(), $this->getLeaseId(), $this->getPropertyId(), $this->getCid(), $objDatabase, true );
		$objAddOn = \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnByIdByCid( $this->getArOriginReferenceId(), $this->getCid(), $objDatabase );

		if( false == parent::update( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		if( CAddOnType::SERVICES == $this->getAddOnTypeId() && CFrequency::ONCE != $objAddOn->getFrequencyId() && strtotime( $objOldAddOnLeaseAssociation->getEndDate() ) != strtotime( $this->getEndDate() ) ) {

			$objCalendarEventType   = \Psi\Eos\Entrata\CCalendarEventTypes::createService()->fetchCalendarEventTypesByDefaultCalendarEventTypeIdByCalendarEventCategoryIdByCid( CDefaultCalendarEventType::SERVICES_RESERVATION, CCalendarEventCategory::RESIDENT, $this->getCid(), $this->m_objDatabase );

			$strCalendarStartDate = $this->getEndDate();
			$strCalendarEndDate = $objOldAddOnLeaseAssociation->getEndDate();

			if( strtotime( $objOldAddOnLeaseAssociation->getEndDate() ) < strtotime( $this->getEndDate() ) ) {
				$strCalendarStartDate = $objOldAddOnLeaseAssociation->getEndDate();
				$strCalendarEndDate  = $this->getEndDate();
			}

			$objCalendarEventFilter = new CCalendarEventsFilter();
			$objCalendarEventFilter->setCid( $this->getCid() );
			$objCalendarEventFilter->setCalendarEventCategoryId( \CCalendarEventCategory::RESIDENT );
			$objCalendarEventFilter->setCalendarEventTypeId( current( $objCalendarEventType )->getId() );
			$objCalendarEventFilter->setReferenceId( $this->getId() );
			$objCalendarEventFilter->setIsIncludeDeleted( true );
			$objCalendarEventFilter->setStartDate( $strCalendarStartDate );
			$objCalendarEventFilter->setEndDate( $strCalendarEndDate );
			$objCalendarEventFilter->setOrderBy( ', ce.start_datetime' );
			$arrobjCalendarEvents = ( array ) \Psi\Eos\Entrata\CCalendarEvents::createService()->fetchSimpleCalendarEventsByCalendarEventFilter( $objCalendarEventFilter, $this->m_objDatabase, true );
			foreach( $arrobjCalendarEvents as $objCalendarEvent ) {
				$strStartDate = $objCalendarEvent->getStartDatetime();
				$strEndDate   = $objCalendarEvent->getEndDatetime();

				if( strtotime( $strStartDate ) != strtotime( $strEndDate ) ) {
					if( strtotime( $strStartDate ) < strtotime( $this->getEndDate() ) && strtotime( $strEndDate ) <= strtotime( $this->getEndDate() ) ) {
						$objCalendarEvent->setDeletedBy( NULL );
						$objCalendarEvent->setDeletedOn( NULL );
						$arrobjUpdateCalendarEvents[] = $objCalendarEvent;
					}

					if( strtotime( $strStartDate ) > strtotime( $this->getEndDate() ) && strtotime( $strEndDate ) > strtotime( $this->getEndDate() ) ) {
						$objCalendarEvent->setDeletedBy( $intCurrentUserId );
   					    $objCalendarEvent->setDeletedOn( 'NOW()' );
						$arrobjUpdateCalendarEvents[] = $objCalendarEvent;
					}

					if( strtotime( $strStartDate ) <= strtotime( $this->getEndDate() ) && strtotime( $strEndDate ) > strtotime( $this->getEndDate() ) ) {
						$objInsertCalendarEvent = clone $objCalendarEvent;
						$objInsertCalendarEvent->setId( NULL );
						$objInsertCalendarEvent->setUpdatedBy( $intCurrentUserId );
						$objInsertCalendarEvent->setUpdatedOn( 'NOW()' );
						$objInsertCalendarEvent->setCreatedBy( $intCurrentUserId );
						$objInsertCalendarEvent->setCreatedOn( 'NOW()' );
						$objInsertCalendarEvent->setDeletedBy( $intCurrentUserId );
						$objInsertCalendarEvent->setDeletedOn( 'NOW()' );
						$objInsertCalendarEvent->setStartDatetime( date( 'm/d/Y', strtotime( $this->getEndDate() . '+1 days' ) ) );
						$arrobjInsertCalendarEvents[] = $objInsertCalendarEvent;

						$objCalendarEvent->setUpdatedBy( $intCurrentUserId );
						$objCalendarEvent->setUpdatedOn( 'NOW()' );
						$objCalendarEvent->setDeletedBy( NULL );
						$objCalendarEvent->setDeletedOn( NULL );
						$objCalendarEvent->setEndDatetime( date( 'm/d/Y', strtotime( $this->getEndDate() ) ) );
						$arrobjUpdateCalendarEvents[] = $objCalendarEvent;
					}
				} else {
					if( strtotime( $strStartDate ) <= strtotime( $this->getEndDate() ) ) {
						$objCalendarEvent->setDeletedBy( NULL );
						$objCalendarEvent->setDeletedOn( NULL );
						$arrobjUpdateCalendarEvents[] = $objCalendarEvent;
					}

					if( strtotime( $strStartDate ) > strtotime( $this->getEndDate() ) ) {
                        $objCalendarEvent->setDeletedBy( $intCurrentUserId );
   					    $objCalendarEvent->setDeletedOn( 'NOW()' );
 						$arrobjUpdateCalendarEvents[] = $objCalendarEvent;

					}
				}
			}

			if( true === valArr( $arrobjUpdateCalendarEvents ) ) {
				foreach( $arrobjUpdateCalendarEvents as $objUpdateCalendarEvent ) {
					if( false == $objUpdateCalendarEvent->validate( 'validate_event_details' ) || false === $objUpdateCalendarEvent->update( $intCurrentUserId, $this->m_objDatabase ) ) {
						$this->addErrorMsgs( $objUpdateCalendarEvent->getErrorMsgs() );
						return false;
					}
				}
			}

			if( true === valArr( $arrobjInsertCalendarEvents ) ) {
				foreach( $arrobjInsertCalendarEvents as $objInsertCalendarEvent ) {
					if( false == $objInsertCalendarEvent->validate( 'validate_event_details' ) || false === $objInsertCalendarEvent->insert( $intCurrentUserId, $this->m_objDatabase ) ) {
						$this->addErrorMsgs( $objInsertCalendarEvent->getErrorMsgs() );
						return false;
					}
				}
			}
		}

		if( true == $boolUpdateRecurringCharge && false == $this->updateRecurringCharge( $intCurrentUserId, $objDatabase, $boolUpdateChargeAmount, $boolIsFromShuffle ) ) {
				return false;
		}

		if( true == $objAddOn->getIsActive() && CAddOnType::SERVICES != $this->getAddOnTypeId() && false == $this->updateAddOnStatus( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		// Log reservation activity
		if( ( false == is_null( $this->getLeaseId() ) || false == is_null( $this->getLeaseIntervalId() ) ) && false == $boolIsSkipEventLog ) {
			$arrmixData = $this->getActivityChanges( $objOldAddOnLeaseAssociation, $objDatabase, $boolIsFromBulkPlaceOnNotice );
			if( true == $arrmixData['insert_event'] ) {
				$intEventSubTypeId = ( false == $boolIsFromQuote ) ? CEventSubType::ADD_ON_UPDATED : CEventSubType::ADD_ON_ADDED;
				$objEventLibrary = $this->logActivityForAddOnLeaseAssociation( $objDatabase, $intEventSubTypeId, $intCurrentUserId, $arrmixData );
				if( true == valObj( $objEventLibrary, \CEventLibrary::class ) && false == $objEventLibrary->insertEvent( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	private function getAddOnFullName() {
		$strAddOnName = ( false == is_null( $this->getAddOnName() ) ) ? ' - ' . $this->getAddOnName() : '';
		return trim( $this->getAddOnGroupName() . $strAddOnName, ', ' );
	}

	public function delete( $intCompanyUserId, $objDatabase, $boolIsSoftDelete = false, $boolIsDeleteSchedueledCharges = false ) {

		if( true == $boolIsSoftDelete && CLeaseStatusType::PAST == $this->getLeaseStatusTypeId() ) {
			return true;
		}

		if( true == $boolIsDeleteSchedueledCharges && false == $this->deleteScheduledCharges( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		if( false == $boolIsSoftDelete ) {
			$objEventLibrary = $this->logActivityForAddOnLeaseAssociation( $objDatabase, CEventSubType::ADD_ON_DELETED, $intCompanyUserId );
			if( true == valObj( $objEventLibrary, \CEventLibrary::class ) && false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
				return false;
			}

			return parent::delete( $intCompanyUserId, $objDatabase );
		}

		$this->setLeaseStatusTypeId( CLeaseStatusType::CANCELLED );
		$this->setDeletedBy( $intCompanyUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCompanyUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( $this->update( $intCompanyUserId, $objDatabase ) ) {
			return true;
		}
	}

	public function deleteScheduledCharges( $intCompanyUserId, $objDatabase ) {
		$boolIsValid    = true;
		$arrobjScheudledCharges     = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByPropertyIdByLeaseIdByLeaseAssociationIdByCid( $this->getPropertyId(), $this->getLeaseId(), $this->getId(), $this->getCid(), $objDatabase, $boolCheckPosted = false );
		$objPropertyChargeSetting   = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

		foreach( $arrobjScheudledCharges as $objScheudledCharge ) {
			$arrintArCodeIds[] = $objScheudledCharge->getArCodeId();
		}

		if( true == valArr( $arrintArCodeIds ) ) {
			$arrobjArCodes = ( array ) \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByIdsByCid( array_filter( $arrintArCodeIds ), $this->getCid(), $objDatabase );
		}

		foreach( $arrobjScheudledCharges as $objScheduledCharge ) {
			$boolIsDetached = ( true == valObj( $objPropertyChargeSetting, \CPropertyChargeSetting::class ) && false == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $arrobjArCodes[$objScheduledCharge->getArCodeId()]->getProrateCharges() ) ) ? 1 : 0;

			if( true == valStr( $this->getEndDate() ) && strtotime( $objScheduledCharge->getChargeStartDate() ) > strtotime( $this->getEndDate() ) ) {
				$strEndDate = $objScheduledCharge->getChargeStartDate();
			} else {
				$strEndDate = $this->getEndDate();
			}

			$strEndDate = ( true == $boolIsDetached && true == valStr( $strEndDate ) ) ? date( 'm/t/Y', strtotime( $strEndDate ) ) : $strEndDate;
			$objScheduledCharge->setChargeEndDate( $strEndDate );

			$boolIsValid &= $objScheduledCharge->delete( $intCompanyUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function updateRecurringCharge( $intCurrentUserId, $objDatabase, $boolUpdateChargeAmount = true, $boolIsFromShuffle = false ) {
		$arrobjRecurringScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchRecurringScheduledChargesByAddOnLeaseAssociationIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase, NULL, $boolIsFromShuffle );
		$this->m_objProperty            = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		$this->m_objLease               = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
		$arrobjPropertyPreferences      = rekeyObjects( 'key', $this->m_objProperty->fetchPropertyPreferencesByKeys( [ 'ENABLE_SEMESTER_SELECTION' ], $objDatabase ) );

		$arrobjAddOnLeaseAssociationCharges = ( array ) $this->getScheduledCharges();

		$this->isStudentProperty( $this->m_objProperty, $arrobjPropertyPreferences, $objDatabase );

		foreach( $arrobjRecurringScheduledCharges as $objRecurringScheduledCharge ) {

			if( true == valObj( $objRecurringScheduledCharge, \CScheduledCharge::class ) ) {

				$objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $objRecurringScheduledCharge->getArCodeId(), $this->getCid(), $objDatabase );

				// This is the hack we appllied for the scheduled charge validation (if scheduled charge associated with the cancel lease interval.)
				if( CLeaseStatusType::CANCELLED == $objRecurringScheduledCharge->getLeaseStatusTypeId() ) {
					continue;
				}

				if( true == valId( $objRecurringScheduledCharge->getInstallmentId() ) ) {
					continue;
				} // Temporarly we are not updating scheduled charges if they are associated with installment

				if( true == valArr( $arrobjAddOnLeaseAssociationCharges ) && true == in_array( $this->getLeaseStatusTypeId(), [ CLeaseStatusType::FUTURE, CLeaseStatusType::APPLICANT ] ) && true == $boolUpdateChargeAmount ) {
					$objRecurringScheduledCharge->setChargeAmount( $arrobjAddOnLeaseAssociationCharges[$objRecurringScheduledCharge->getId()]->getChargeAmount() );
				}

				$objPropertyChargeSetting = ( true == valObj( $this->m_objProperty, \CProperty::class ) ) ? $this->m_objProperty->fetchPropertyChargeSetting( $objDatabase ) : \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $objRecurringScheduledCharge->getPropertyId(), $objRecurringScheduledCharge->getCid(), $objDatabase );
				$boolIsImmediateMoveIn    = ( true == valObj( $this->m_objLease, \CLease::class ) && true == $this->m_objLease->getIsImmediateMoveIn() ? true : false );
				$boolIsDetached           = ( true == valObj( $objPropertyChargeSetting, \CPropertyChargeSetting::class ) && false == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $objArCode->getProrateCharges() ) ) ? 1 : 0;
				$strChargeStartDate       = ( false == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $objArCode->getProrateCharges(), $boolIsImmediateMoveIn ) ) ? date( 'm/1/Y', strtotime( $this->getStartDate() ) ) : date( 'm/d/Y', strtotime( $this->getStartDate() ) );
				$strChargeEndDate         = ( true == $boolIsDetached ) ? date( 'm/t/Y', strtotime( $this->getEndDate() ) ) : $this->getEndDate();

				// if SC is not already posted then update its start date
				if( true == is_null( $objRecurringScheduledCharge->getLastPostedOn() ) && CLeaseStatusType::FUTURE == $this->getLeaseStatusTypeId() ) {
					$objRecurringScheduledCharge->setChargeStartDate( $strChargeStartDate );
				}

				$arrintIncludedLeaseStatuses = [ CLeaseStatusType::NOTICE, CLeaseStatusType::PAST ];

				if( true == $boolIsFromShuffle ) {
					$arrintIncludedLeaseStatuses = [ CLeaseStatusType::NOTICE, CLeaseStatusType::PAST, CLeaseStatusType::CURRENT ];
				}

				if( true == $this->m_boolIsStudentSemesterSelectionEnabled && true == valId( $objRecurringScheduledCharge->getLeaseStartWindowId() ) && false == in_array( $this->getLeaseStatusTypeId(), $arrintIncludedLeaseStatuses ) ) {
					$strBillingEndDate = CLeaseStartWindows::fetchBillingEndDateByLeaseTermIdByPropertyIdByCid( $objRecurringScheduledCharge->getLeaseStartWindowId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

					if( true == valStr( $strBillingEndDate ) ) {
						$strBillingEndDate = ( true == $boolIsDetached ) ? date( 'm/t/Y', strtotime( $strBillingEndDate ) ) : $strBillingEndDate;
						$strBillingEndDate = ( strtotime( $strChargeStartDate ) >= strtotime( $strBillingEndDate ) ) ? $strChargeEndDate : $strBillingEndDate;
						$objRecurringScheduledCharge->setChargeEndDate( $strBillingEndDate );
						$objRecurringScheduledCharge->setEndsWithMoveOut( false );
					}
				} elseif( strtotime( $objRecurringScheduledCharge->getChargeStartDate() ) <= strtotime( $strChargeEndDate ) && true == in_array( $this->getLeaseStatusTypeId(), $arrintIncludedLeaseStatuses ) && strtotime( $strChargeEndDate ) <= strtotime( $objRecurringScheduledCharge->getLeaseEndDate() ) ) {
					$objRecurringScheduledCharge->setChargeEndDate( $strChargeEndDate );
				}

				if( false == valStr( $objRecurringScheduledCharge->getChargeEndDate() ) ) {
					$objRecurringScheduledCharge->setEndsWithMoveOut( true );
				}

				$objRecurringScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_UPDATED );

				if( false == $objRecurringScheduledCharge->update( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objRecurringScheduledCharge->getErrorMsgs() );
					continue;
				}

			}
		}

		return true;
	}

	public function postReservationCharges( $strTransactionDatetime, $objPropertyGlSetting, $intCompanyUserId, $objDatabase, $boolIsDeposit = false, $boolReturnArTransaction = false ) {

		$objArTransaction = new CArTransaction();
		$objArTransaction->setCid( $this->getCid() );
		$objArTransaction->setPropertyId( $this->getPropertyId() );
		$objArTransaction->setLeaseId( $this->getLeaseId() );
		$objArTransaction->setArOriginId( $this->getArOriginId() );
		$objArTransaction->setArOriginReferenceId( $this->getArOriginReferenceId() );
		$objArTransaction->getOrFetchPostMonthValidationData( $intCompanyUserId, $objDatabase );

		if( true == valObj( $this->m_objLease, \CLease::class ) ) {
			$objArTransaction->setLeaseIntervalId( $this->m_objLease->getActiveLeaseIntervalId() );
		} else {
			$objArTransaction->setLeaseIntervalId( $this->getLeaseIntervalId() );
		}

		$objScheduledCharge = NULL;

		$intArCodeId		= ( true == $boolIsDeposit ? $this->getDepositArCodeId() : $this->getOtherArCodeId() );
		$fltChargeAmount	= ( true == $boolIsDeposit ? $this->getDepositAmount() : $this->getOtherAmount() );

		$intArTriggerId = CArTrigger::APPLICATION_COMPLETED;
		if( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
			$intArTriggerId = CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT;
			$objArTransaction->setArTriggerId( $intArTriggerId );
		}
		$intArTriggerId	= ( true == $boolIsDeposit ? CArTrigger::MOVE_IN : $intArTriggerId );

		$objArTransaction->setTransactionAmount( $fltChargeAmount );
		$objArTransaction->setArTriggerId( $intArTriggerId );
		$objArTransaction->setArCodeId( $intArCodeId );
		$objArTransaction->setReportingPostDate( $strTransactionDatetime );
		$objArTransaction->setPostDate( $strTransactionDatetime );

		if( true == $boolIsDeposit ) {
			$objArTransaction->setArProcessId( CArProcess::MOVE_IN );
			$objScheduledCharge = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchMoveInScheduledChargeByAddOnLeaseAssociationIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objScheduledCharge, \CScheduledCharge::class ) ) {
				$objScheduledCharge->setLastPostedOn( date( 'm/d/Y', strtotime( $strTransactionDatetime ) ) );
				$objArTransaction->setScheduledChargeId( $objScheduledCharge->getId() );
			} else {
				return true;
			}
		} else {
			// This is for posting reservation fee
			$objScheduledCharge = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchLeaseAssociationScheduledChargeByAddOnLeaseAssociationIdByArTriggerIdByPropertyIdByCid( $this->getId(), $intArTriggerId, $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objScheduledCharge, \CScheduledCharge::class ) ) {
				$objScheduledCharge->setLastPostedOn( date( 'm/d/Y', strtotime( $strTransactionDatetime ) ) );
				$objScheduledCharge->setPostedThroughDate( date( 'm/d/Y', strtotime( $strTransactionDatetime ) ) );
				$objArTransaction->setScheduledChargeId( $objScheduledCharge->getId() );
			}
		}

		$objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $intArCodeId, $this->getCid(), $objDatabase );
		if( true == valObj( $objArCode, \CArCode::class ) ) {
			$objArTransaction->setArCodeTypeId( $objArCode->getArCodeTypeId() );
		}

		$strItemType = '';
		if( CAddOnType::RENTABLE_ITEMS == $this->getAddOnTypeId() ) {
			$strItemType = ' Rentable Item ';
		} elseif( CAddOnType::SERVICES == $this->getAddOnTypeId() ) {
			$strItemType = ' Services ';
		} elseif( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
			$strItemType = ' Assignable Item ';
		}

		$strChargeName = $strItemType . ' ( ' . $this->getAddOnCategoryName() . ' - ' . $this->getAddOnGroupName();
		$strChargeName .= ( true == $this->getAddOnName() ) ? ' ( ' . $this->getAddOnName() . ' )' : '';
		$strChargeName .= ' )';
		$objArTransaction->setMemo( $strChargeName );

		// Set charge postmonth to Greatest of Ar post Month and month of Transaction post date
		$strPostMonth = ( true == valObj( $objPropertyGlSetting, \CPropertyGlSetting::class ) && strtotime( $objPropertyGlSetting->getArPostMonth() ) >= strtotime( date( 'm/1/Y', strtotime( $strTransactionDatetime ) ) ) ? $objPropertyGlSetting->getArPostMonth() : date( 'm/1/Y', strtotime( $strTransactionDatetime ) ) );
		$objArTransaction->setPostMonth( $strPostMonth );

		// validate
		$strValidate = ( true == $boolIsDeposit ? 'post_move_in_charge' : 'post_charge_ar_transaction' );
		if( false == $objArTransaction->validate( $strValidate, $objDatabase ) ) {
			$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
			return false;
		}

		if( false == $objArTransaction->postCharge( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strChargeName . ' transaction could not be inserted.', NULL ) );
			return false;
		}

		$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_UPDATED );
		if( true == isset( $objScheduledCharge ) && true == valObj( $objScheduledCharge, \CScheduledCharge::class ) && false == $objScheduledCharge->update( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsg( $objScheduledCharge->getErrorMsgs() );
			return false;
		}

		if( true == $boolReturnArTransaction ) {
			return $objArTransaction;
		} else {
			return true;
		}

	}

	public function postNewReservationCharges( $strTransactionDatetime, $objPropertyGlSetting, $intCompanyUserId, $objDatabase, $boolIsDeposit = false, $boolReturnArTransaction = false, $boolIsFromTransfer = false ) {

		$intArTriggerId = CArTrigger::APPLICATION_COMPLETED;
		if( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
			$intArTriggerId = CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT;

		}
		$intArTriggerId	= ( true == $boolIsDeposit ? CArTrigger::MOVE_IN : $intArTriggerId );

		$strItemType = '';
		if( CAddOnType::RENTABLE_ITEMS == $this->getAddOnTypeId() ) {
			$strItemType = ' Rentable Item ';
		} elseif( CAddOnType::SERVICES == $this->getAddOnTypeId() ) {
			$strItemType = ' Services ';
		} elseif( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
			$strItemType = ' Assignable Item ';
		}

		$strChargeName = $strItemType . ' ( ' . $this->getAddOnCategoryName() . ' - ' . $this->getAddOnGroupName();
		$strChargeName .= ( true == $this->getAddOnName() ) ? ' ( ' . $this->getAddOnName() . ' )' : '';
		$strChargeName .= ' )';

		if( true == $boolIsFromTransfer ) {
			$arrobjNewScheduledCharges = $this->getScheduledCharges();
		} else {
			$arrobjNewScheduledCharges = $this->getRates();
		}

		$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchLeaseAssociationScheduledChargesByAddOnLeaseAssociationIdByArTriggerIdByPropertyIdByCid( $this->getId(), $intArTriggerId, $this->getPropertyId(), $this->getCid(), $objDatabase );

		foreach( $arrobjScheduledCharges as $objScheduledCharge ) {
			$objArTransaction = new CArTransaction();
			$objArTransaction->setCid( $this->getCid() );
			$objArTransaction->setPropertyId( $this->getPropertyId() );
			$objArTransaction->setLeaseId( $this->getLeaseId() );
			$objArTransaction->setArOriginId( $this->getArOriginId() );
			$objArTransaction->setArOriginReferenceId( $this->getArOriginReferenceId() );
			$objArTransaction->getOrFetchPostMonthValidationData( $intCompanyUserId, $objDatabase );

			if( true == valObj( $this->m_objLease, \CLease::class ) ) {
				$objArTransaction->setLeaseIntervalId( $this->m_objLease->getActiveLeaseIntervalId() );
			} else {
				$objArTransaction->setLeaseIntervalId( $this->getLeaseIntervalId() );
			}

			$intArCodeId     = $arrobjNewScheduledCharges[$objScheduledCharge->getRateId()]->getArCodeId();
			if( true == $boolIsFromTransfer ) {
				$fltChargeAmount = $arrobjNewScheduledCharges[$objScheduledCharge->getRateId()]->getChargeAmount();
			} else {
				$fltChargeAmount = $arrobjNewScheduledCharges[$objScheduledCharge->getRateId()]->getRateAmount();
			}

			$objArTransaction->setTransactionAmount( $fltChargeAmount );
			$objArTransaction->setArTriggerId( $intArTriggerId );
			$objArTransaction->setArCodeId( $intArCodeId );
			$objArTransaction->setReportingPostDate( $strTransactionDatetime );
			$objArTransaction->setPostDate( $strTransactionDatetime );
			$objArTransaction->setScheduledChargeId( $objScheduledCharge->getId() );

			if( true == $boolIsDeposit ) {
				$objArTransaction->setArProcessId( CArProcess::MOVE_IN );
				if( true == valObj( $objScheduledCharge, \CScheduledCharge::class ) ) {
					$objScheduledCharge->setLastPostedOn( date( 'm/d/Y', strtotime( $strTransactionDatetime ) ) );
				} else {
					continue;
				}
			} else {
				// This is for posting reservation fee
				if( true == valObj( $objScheduledCharge, \CScheduledCharge::class ) ) {
					$objScheduledCharge->setLastPostedOn( date( 'm/d/Y', strtotime( $strTransactionDatetime ) ) );
					$objScheduledCharge->setPostedThroughDate( date( 'm/d/Y', strtotime( $strTransactionDatetime ) ) );
				}
			}

			$objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $intArCodeId, $this->getCid(), $objDatabase );
			if( true == valObj( $objArCode, \CArCode::class ) ) {
				$objArTransaction->setArCodeTypeId( $objArCode->getArCodeTypeId() );
			}

			$objArTransaction->setMemo( $strChargeName );

			// Set charge postmonth to Greatest of Ar post Month and month of Transaction post date
			$strPostMonth = ( true == valObj( $objPropertyGlSetting, \CPropertyGlSetting::class ) && strtotime( $objPropertyGlSetting->getArPostMonth() ) >= strtotime( date( 'm/1/Y', strtotime( $strTransactionDatetime ) ) ) ? $objPropertyGlSetting->getArPostMonth() : date( 'm/1/Y', strtotime( $strTransactionDatetime ) ) );
			$objArTransaction->setPostMonth( $strPostMonth );

			// validate
			$strValidate = ( true == $boolIsDeposit ? 'post_move_in_charge' : 'post_charge_ar_transaction' );
			if( false == $objArTransaction->validate( $strValidate, $objDatabase ) ) {
				$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
				continue;
			}

			if( false == $objArTransaction->postCharge( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( '{%s,0} transaction could not be inserted.', [ $strChargeName ] ), NULL ) );
				continue;
			}

			$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_UPDATED );
			if( true == isset( $objScheduledCharge ) && true == valObj( $objScheduledCharge, \CScheduledCharge::class ) && false == $objScheduledCharge->update( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsg( $objScheduledCharge->getErrorMsgs() );
				continue;
			}
		}

		if( true == $boolReturnArTransaction ) {
			return $objArTransaction;
		} else {
			return true;
		}

	}

	public function preparePostScheduledChargesCriteria( $strPostThroughDate, $arrintScheduledChargeIds = [], $boolIsDryRun = false, $intActionMode, $intCompanyUserId, $objDatabase ) {

		$strRewindDate = date( 'm/d/Y', strtotime( '-1 day', strtotime( date( 'm/1/Y', strtotime( $strPostThroughDate ) ) ) ) );

		switch( $intActionMode ) {
			case self::MOVE_IN:
				$intChargePostingMode = CScheduledChargePostingMode::ADD_ON_MOVE_IN;
				break;

			case self::LOST_OR_RETURN:
				$intChargePostingMode = CScheduledChargePostingMode::ADD_ON_LOST_OR_RETURN;
				break;

			case self::UPDATE:
				$intChargePostingMode = CScheduledChargePostingMode::ADD_ON_UPDATE;
				break;

			case self::MOVE_OUT:
				$intChargePostingMode = CScheduledChargePostingMode::ADD_ON_MOVE_OUT;
				break;

			case self::REVERSAL:
				$intChargePostingMode = CScheduledChargePostingMode::ADD_ON_REVERSAL;
				$strRewindDate = '01/01/1970';
				break;

			case self::NOTICE:
				$strPostThroughDate = NULL;
				$intChargePostingMode = CScheduledChargePostingMode::ADD_ONS;
				break;

			default:
				$intChargePostingMode = CScheduledChargePostingMode::ADD_ONS;
				break;
		}

		$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( $intChargePostingMode, $objDatabase );
		$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->getCid(), [ $this->getPropertyId() ], $intCompanyUserId );

		$objPostScheduledChargesCriteria->setIsDryRun( $boolIsDryRun )
										->setLeaseIds( [ $this->getLeaseId() ] )
										->setScheduledChargeIds( $arrintScheduledChargeIds )
										->setPostWindowEndDateOverride( $strPostThroughDate )
										->setScheduledChargeRewindDateReference( $strRewindDate )
										->setArTriggerIds( CArTrigger::$c_arrintAddOnRewindArTriggers );

		$objDatabase->begin();

		if( true == $objPostScheduledChargesLibrary->validate( $objPostScheduledChargesCriteria ) ) {

			$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );
			$this->m_arrobjArTransactions = ( array ) $objPostScheduledChargesLibrary->getArTransactions();

			if( true == $boolIsDryRun ) {
				$objDatabase->rollback();
			} else {
				$objDatabase->commit();
			}

			return true;

		} else {

			$objDatabase->rollback();
			$this->addErrorMsgs( $objPostScheduledChargesLibrary->getErrorMsgs() );
			return false;
		}

	}

	public function addAddOnLeaseAssociationCharges( $boolIsRecurring, $boolIsMoveIn, $boolIsReservationFee, $intCompanyUserId, $objDatabase, $intLeaseIntervalId = NULL, $boolIsSkipRentCharge = false, $boolIsFromShuffle = false ) {

		$boolIsSuccess = true;
		$this->m_arrintRecurringScheduledChargeIds = [];

		$arrobjPropertyPreferences	= ( array ) rekeyObjects( 'key', $this->m_objProperty->fetchPropertyPreferencesByKeys( [ 'IS_IN_MIGRATION_MODE', 'ENABLE_SEMESTER_SELECTION' ], $objDatabase ) );
		$objPropertyChargeSetting 	= $this->m_objProperty->fetchPropertyChargeSetting( $objDatabase );

		$this->isStudentProperty( $this->m_objProperty, $arrobjPropertyPreferences, $objDatabase );

		$strItemType = '';
		if( CAddOnType::RENTABLE_ITEMS == $this->getAddOnTypeId() ) {
			$strItemType = ' Rentable Item ';
		} elseif( CAddOnType::SERVICES == $this->getAddOnTypeId() ) {
			$strItemType = ' Services ';
		} elseif( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
			$strItemType = ' Assignable Item ';
		}
		$strChargeName = $strItemType . ' ( ' . $this->getAddOnCategoryName() . ' - ' . $this->getAddOnGroupName();
		$strChargeName .= ( true == $this->getAddOnName() ) ? ' ( ' . $this->getAddOnName() . ' )' : '';
		$strChargeName .= ' )';

		$intLeaseIntervalId = ( 0 < $intLeaseIntervalId ) ?  $intLeaseIntervalId : $this->m_objLease->getActiveLeaseIntervalId();
		$arrintArCodeIds	= [ $this->getRentArCodeId(), $this->getDepositArCodeId(), $this->getOtherArCodeId() ];
		$arrobjArCodes		= \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByIdsByCid( array_filter( $arrintArCodeIds ), $this->getCid(), $objDatabase );

		// create scheduled charge for rent
		if( true == $boolIsRecurring && 0 < $this->getRentAmount() && true == valId( $this->getRentArCodeId() ) && false == $boolIsSkipRentCharge ) {

			$objScheduledCharge	= $this->createOldScheduledCharge( false, false, $objDatabase );

			if( strtotime( $this->getStartDate() ) > strtotime( $this->m_objLease->getLeaseStartDate() ) ) {
				if( $this->getAddOnTypeId() != CAddOnType::SERVICES && false == $this->getUseLeaseStartDate() ) {
					$objScheduledCharge->setStartsWithLease( false );
				} elseif( $this->getAddOnTypeId() == CAddOnType::SERVICES ) {
					$objScheduledCharge->setStartsWithLease( false );
				}
			}

			$boolProrateCharges = ( true == valObj( $objPropertyChargeSetting, \CPropertyChargeSetting::class ) && true == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $arrobjArCodes[$objScheduledCharge->getArCodeId()]->getProrateCharges() ) ) ? 1 : 0;

			if( true == $this->m_objLease->getIsImmediateMoveIn() ) {
				$boolImmediateMoveInProrateCharges = ( true == valObj( $objPropertyChargeSetting, \CPropertyChargeSetting::class ) && true == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $arrobjArCodes[$objScheduledCharge->getArCodeId()]->getProrateCharges(), $this->m_objLease->getIsImmediateMoveIn() ) ) ? true : false;
			}

			if( true == array_key_exists( $this->getRentArCodeId(), $arrobjArCodes ) ) {
				$objScheduledCharge->setArCodeTypeId( $arrobjArCodes[$this->getRentArCodeId()]->getArCodeTypeId() );
			}

			$objScheduledCharge->setLeaseIntervalId( $intLeaseIntervalId );

			if( false == $this->m_boolIsStudentSemesterSelectionEnabled && false == $boolProrateCharges ) {
				$objScheduledCharge->setEndsWithMoveOut( true );
			}

			if( true == $this->m_boolIsStudentSemesterSelectionEnabled && false == $boolProrateCharges && true == valStr( $objScheduledCharge->getChargeEndDate() ) ) {
				$strBillingEndDate = date( 'm/t/Y', strtotime( $objScheduledCharge->getChargeEndDate() ) );
				$objScheduledCharge->setChargeEndDate( $strBillingEndDate );
			}

			if( ( false == $boolProrateCharges && false == isset( $boolImmediateMoveInProrateCharges ) ) || ( true == isSet( $boolImmediateMoveInProrateCharges ) && false == $boolImmediateMoveInProrateCharges ) ) {
				$objScheduledCharge->setStartsWithLease( true );
				$objScheduledCharge->setChargeStartDate( date( 'm/1/Y', strtotime( $this->getStartDate() ) ) );
				if( true == $boolIsFromShuffle ) {
					$objScheduledCharge->setChargeStartDate( date( 'm/1/Y', strtotime( '+1 month', strtotime( $this->getStartDate() ) ) ) );
				}
			}

			$objScheduledCharge->setMemo( $strChargeName );

			if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'IS_IN_MIGRATION_MODE',  $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['IS_IN_MIGRATION_MODE']->getValue() ) ) {
				$objScheduledCharge->setLastPostedOn( date( 'm/1/Y' ) );
				$objScheduledCharge->setPostedThroughDate( date( 'm/t/Y' ) );
			}

			if( false == $objScheduledCharge->validate( VALIDATE_INSERT, $objDatabase ) ) {
				$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
				$boolIsSuccess &= false;
			}

			$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_INSERTED );
			if( false == $objScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
				$boolIsSuccess &= false;
			}

			$this->m_arrintRecurringScheduledChargeIds[] = $objScheduledCharge->getId();
		}

		// create scheduled charge for deposit
		if( true == $boolIsMoveIn && 0 < $this->getDepositAmount() && true == valId( $this->getDepositArCodeId() ) ) {

			$objScheduledCharge	= $this->createOldScheduledCharge( true, false, $objDatabase );

			if( true == array_key_exists( $this->getDepositArCodeId(), $arrobjArCodes ) ) {
				$objScheduledCharge->setArCodeTypeId( $arrobjArCodes[$this->getDepositArCodeId()]->getArCodeTypeId() );
			}

			$objScheduledCharge->setLeaseIntervalId( $intLeaseIntervalId );

			if( ( true == $boolProrateCharges && false == isSet( $boolImmediateMoveInProrateCharges ) ) || ( true == isSet( $boolImmediateMoveInProrateCharges ) && true == $boolImmediateMoveInProrateCharges ) ) {
				$objScheduledCharge->setStartsWithLease( true );
			}

			$objScheduledCharge->setMemo( $strChargeName );

			if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'IS_IN_MIGRATION_MODE',  $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['IS_IN_MIGRATION_MODE']->getValue() ) ) {
				$objScheduledCharge->setLastPostedOn( date( 'm/1/Y' ) );
				$objScheduledCharge->setPostedThroughDate( date( 'm/t/Y' ) );
			}

			if( false == $objScheduledCharge->validate( VALIDATE_INSERT, $objDatabase ) ) {
				$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
				$boolIsSuccess &= false;
			}

			$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_INSERTED );
			if( false == $objScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
				$boolIsSuccess &= false;
			}

		}

		if( true == $boolIsReservationFee && 0 < $this->getOtherAmount() && true == valId( $this->getOtherArCodeId() ) ) {
			$objScheduledCharge	= $this->createOldScheduledCharge( false, true, $objDatabase );

			if( true == array_key_exists( $this->getOtherArCodeId(), $arrobjArCodes ) ) {
				$objScheduledCharge->setArCodeTypeId( $arrobjArCodes[$this->getOtherArCodeId()]->getArCodeTypeId() );
			}

			$objScheduledCharge->setLeaseIntervalId( $intLeaseIntervalId );
			$objScheduledCharge->setMemo( $strChargeName );

			if( ( true == $boolProrateCharges && false == isSet( $boolImmediateMoveInProrateCharges ) ) || ( true == isSet( $boolImmediateMoveInProrateCharges ) && true == $boolImmediateMoveInProrateCharges ) ) {
				$objScheduledCharge->setStartsWithLease( true );
			}

			if( false == $objScheduledCharge->validate( VALIDATE_INSERT, $objDatabase ) ) {
				$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
				$boolIsSuccess &= false;
			}

			$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_INSERTED );
			if( false == $objScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
				$boolIsSuccess &= false;
			}
		}

		return $boolIsSuccess;
	}

	public function addNewAddOnLeaseAssociationCharges( $boolIsRecurring, $boolIsMoveIn, $boolIsReservationFee, $intCompanyUserId, $objDatabase, $intLeaseIntervalId = NULL, $boolIsSkipRentCharge = false, $boolIsFromShuffle = false, $boolIsFromTransfer = false ) {

		$boolIsSuccess = true;
		$this->m_arrintRecurringScheduledChargeIds = [];

		$arrobjPropertyPreferences	= ( array ) rekeyObjects( 'key', $this->m_objProperty->fetchPropertyPreferencesByKeys( [ 'IS_IN_MIGRATION_MODE', 'ENABLE_SEMESTER_SELECTION' ], $objDatabase ) );
		$objPropertyChargeSetting 	= $this->m_objProperty->fetchPropertyChargeSetting( $objDatabase );

		$this->isStudentProperty( $this->m_objProperty, $arrobjPropertyPreferences, $objDatabase );

		$strItemType = '';
		if( CAddOnType::RENTABLE_ITEMS == $this->getAddOnTypeId() ) {
			$strItemType = ' Rentable Item ';
		} elseif( CAddOnType::SERVICES == $this->getAddOnTypeId() ) {
			$strItemType = ' Services ';
		} elseif( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
			$strItemType = ' Assignable Item ';
		}
		$strChargeName = $strItemType . ' ( ' . $this->getAddOnCategoryName() . ' - ' . $this->getAddOnGroupName();
		$strChargeName .= ( true == $this->getAddOnName() ) ? ' ( ' . $this->getAddOnName() . ' )' : '';
		$strChargeName .= ' )';

		$intLeaseIntervalId = ( 0 < $intLeaseIntervalId ) ?  $intLeaseIntervalId : $this->m_objLease->getActiveLeaseIntervalId();

		$objLeaseInterval  = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByIdByCid( $intLeaseIntervalId, $this->m_objLease->getCid(), $objDatabase );

		if( true == $boolIsFromTransfer ) {
			$arrobjAddOnLeaseAssociationRates = ( array ) $this->getScheduledCharges();
		} else {
			$arrobjAddOnLeaseAssociationRates = ( array ) $this->getRates();
		}

		foreach( $arrobjAddOnLeaseAssociationRates as $objAddOnLeaseAssociationRate ) {
			$arrintArCodeIds[]	= $objAddOnLeaseAssociationRate->getArCodeId();
		}

		if( true == valArr( $arrintArCodeIds ) ) {
			$arrobjArCodes = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByIdsByCid( array_filter( $arrintArCodeIds ), $this->getCid(), $objDatabase );
		}

		foreach( $arrobjAddOnLeaseAssociationRates as $objAddOnLeaseAssociationRate ) {
			if( true == $boolIsFromTransfer ) {
				$fltChargeAmount = $objAddOnLeaseAssociationRate->getChargeAmount();
			} else {
				$fltChargeAmount = $objAddOnLeaseAssociationRate->getRateAmount();
			}
			// create scheduled charge for rent
			if( true == $boolIsRecurring && true == in_array( $objAddOnLeaseAssociationRate->getArTriggerId(), CArTrigger::$c_arrintAddOnRecurringArTriggerIds ) && 0 < $fltChargeAmount && true == valId( $objAddOnLeaseAssociationRate->getArCodeId() ) && false == $boolIsSkipRentCharge ) {

				$objScheduledCharge = $this->createScheduledCharge( false, false, $objDatabase, $objAddOnLeaseAssociationRate->getArTriggerId() );

				$objScheduledCharge->setArCodeId( $objAddOnLeaseAssociationRate->getArCodeId() );
				$objScheduledCharge->setChargeAmount( $fltChargeAmount );
				$objScheduledCharge->setRateId( $objAddOnLeaseAssociationRate->getId() );
				$objScheduledCharge->setMonthToMonthAmount( $fltChargeAmount );

				if( strtotime( $this->getStartDate() ) > strtotime( $this->m_objLease->getLeaseStartDate() ) ) {
					if( $this->getAddOnTypeId() != CAddOnType::SERVICES && false == $this->getUseLeaseStartDate() ) {
						$objScheduledCharge->setStartsWithLease( false );
					} elseif( $this->getAddOnTypeId() == CAddOnType::SERVICES ) {
						$objScheduledCharge->setStartsWithLease( false );
					}
				}

				$boolProrateCharges = ( true == valObj( $objPropertyChargeSetting, \CPropertyChargeSetting::class ) && true == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $arrobjArCodes[$objScheduledCharge->getArCodeId()]->getProrateCharges() ) ) ? 1 : 0;

				if( true == $objLeaseInterval->getIsImmediateMoveIn() ) {
					$boolImmediateMoveInProrateCharges = ( true == valObj( $objPropertyChargeSetting, \CPropertyChargeSetting::class ) && true == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $arrobjArCodes[$objScheduledCharge->getArCodeId()]->getProrateCharges(), $objLeaseInterval->getIsImmediateMoveIn() ) ) ? true : false;
				}

				if( true == array_key_exists( $objAddOnLeaseAssociationRate->getArCodeId(), $arrobjArCodes ) ) {
					$objScheduledCharge->setArCodeTypeId( $arrobjArCodes[$objAddOnLeaseAssociationRate->getArCodeId()]->getArCodeTypeId() );
				}

				$objScheduledCharge->setLeaseIntervalId( $intLeaseIntervalId );

				if( false == $this->m_boolIsStudentSemesterSelectionEnabled && false == $boolProrateCharges ) {
					$objScheduledCharge->setEndsWithMoveOut( true );
				}

				if( true == $this->m_boolIsStudentSemesterSelectionEnabled && false == $boolProrateCharges && true == valStr( $objScheduledCharge->getChargeEndDate() ) ) {
					$strBillingEndDate = date( 'm/t/Y', strtotime( $objScheduledCharge->getChargeEndDate() ) );
					$objScheduledCharge->setChargeEndDate( $strBillingEndDate );
				}

				if( false == $boolProrateCharges && ( false == isset( $boolImmediateMoveInProrateCharges ) ) || ( true == isSet( $boolImmediateMoveInProrateCharges ) && false == $boolImmediateMoveInProrateCharges ) ) {
					$objScheduledCharge->setStartsWithLease( true );
					$objScheduledCharge->setChargeStartDate( date( 'm/1/Y', strtotime( $this->getStartDate() ) ) );
					if( true == $boolIsFromShuffle ) {
						$objScheduledCharge->setChargeStartDate( date( 'm/1/Y', strtotime( '+1 month', strtotime( $this->getStartDate() ) ) ) );
					}
				}

				$objScheduledCharge->setMemo( $strChargeName );

				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'IS_IN_MIGRATION_MODE', $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['IS_IN_MIGRATION_MODE']->getValue() ) ) {
					$objScheduledCharge->setLastPostedOn( date( 'm/1/Y' ) );
					$objScheduledCharge->setPostedThroughDate( date( 'm/t/Y' ) );
				}

				if( false == $objScheduledCharge->validate( VALIDATE_INSERT, $objDatabase ) ) {
					$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
					$boolIsSuccess &= false;
				}

				$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_INSERTED );
				if( false == $objScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
					$boolIsSuccess &= false;
				}

				$this->m_arrintRecurringScheduledChargeIds[] = $objScheduledCharge->getId();
			}

			// create scheduled charge for deposit
			if( true == $boolIsMoveIn && $objAddOnLeaseAssociationRate->getArTriggerId() == CArTrigger::MOVE_IN && 0 < $fltChargeAmount && true == valId( $objAddOnLeaseAssociationRate->getArCodeId() ) ) {

				$objScheduledCharge = $this->createScheduledCharge( true, false, $objDatabase );

				$objScheduledCharge->setArCodeId( $objAddOnLeaseAssociationRate->getArCodeId() );
				$objScheduledCharge->setChargeAmount( $fltChargeAmount );
				$objScheduledCharge->setRateId( $objAddOnLeaseAssociationRate->getId() );

				if( true == array_key_exists( $objAddOnLeaseAssociationRate->getArCodeId(), $arrobjArCodes ) ) {
					$objScheduledCharge->setArCodeTypeId( $arrobjArCodes[$objAddOnLeaseAssociationRate->getArCodeId()]->getArCodeTypeId() );
				}

				$objScheduledCharge->setLeaseIntervalId( $intLeaseIntervalId );

				if( ( true == $boolProrateCharges && false == isSet( $boolImmediateMoveInProrateCharges ) ) || ( true == isSet( $boolImmediateMoveInProrateCharges ) && true == $boolImmediateMoveInProrateCharges ) ) {
					$objScheduledCharge->setStartsWithLease( true );
				}

				$objScheduledCharge->setMemo( $strChargeName );

				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'IS_IN_MIGRATION_MODE', $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['IS_IN_MIGRATION_MODE']->getValue() ) ) {
					$objScheduledCharge->setLastPostedOn( date( 'm/1/Y' ) );
					$objScheduledCharge->setPostedThroughDate( date( 'm/t/Y' ) );
				}

				if( false == $objScheduledCharge->validate( VALIDATE_INSERT, $objDatabase ) ) {
					$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
					$boolIsSuccess &= false;
				}

				$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_INSERTED );
				if( false == $objScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
					$boolIsSuccess &= false;
				}

			}

			if( true == $boolIsReservationFee && ( false == in_array( $objAddOnLeaseAssociationRate->getArTriggerId(), CArTrigger::$c_arrintAddOnRecurringArTriggerIds ) && $objAddOnLeaseAssociationRate->getArTriggerId() != CArTrigger::MOVE_IN ) && 0 < $fltChargeAmount && true == valId( $objAddOnLeaseAssociationRate->getArCodeId() ) ) {
				$objScheduledCharge = $this->createScheduledCharge( false, true, $objDatabase, $objAddOnLeaseAssociationRate->getArTriggerId() );

				$objScheduledCharge->setArCodeId( $objAddOnLeaseAssociationRate->getArCodeId() );
				$objScheduledCharge->setChargeAmount( $fltChargeAmount );
				$objScheduledCharge->setRateId( $objAddOnLeaseAssociationRate->getId() );

				if( true == array_key_exists( $objAddOnLeaseAssociationRate->getArCodeId(), $arrobjArCodes ) ) {
					$objScheduledCharge->setArCodeTypeId( $arrobjArCodes[$objAddOnLeaseAssociationRate->getArCodeId()]->getArCodeTypeId() );
				}

				$objScheduledCharge->setLeaseIntervalId( $intLeaseIntervalId );
				$objScheduledCharge->setMemo( $strChargeName );

				if( ( true == $boolProrateCharges && false == isSet( $boolImmediateMoveInProrateCharges ) ) || ( true == isSet( $boolImmediateMoveInProrateCharges ) && true == $boolImmediateMoveInProrateCharges ) ) {
					$objScheduledCharge->setStartsWithLease( true );
				}

				if( false == $objScheduledCharge->validate( VALIDATE_INSERT, $objDatabase ) ) {
					$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
					$boolIsSuccess &= false;
				}

				$objScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_INSERTED );
				if( false == $objScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objScheduledCharge->getErrorMsgs() );
					$boolIsSuccess &= false;
				}

			}
		}

		// Post Service Selected scheduled charges
		if( $this->getAddOnTypeId() == CAddOnType::SERVICES ) {
			$this->postServiceSelectedCharges( $intCompanyUserId, $objDatabase );
		}

		return $boolIsSuccess;
	}

	public function manageRecurringCharges( $objOriginalScheduledCharge, $intCompanyUserId, $objDatabase, $intActionMode ) {

		$boolIsValid = true;
		$arrobjAddOnLeaseAssociationCharges = $this->getScheduledCharges();

		// if rent is removed during editing reservation then delete its schedule charges as well
		if( true == valObj( $objOriginalScheduledCharge, \CScheduledCharge::class ) && true == is_null( $arrobjAddOnLeaseAssociationCharges[$objOriginalScheduledCharge->getId()]->getChargeAmount() ) ) {
			$objOriginalScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_UPDATED );

			if( false == $objOriginalScheduledCharge->delete( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsg( $objOriginalScheduledCharge->getErrorMsgs() );
				$boolIsValid &= false;
			}
		} else {
			// this is for updating scheduled charges and posting their charges
			if( true == valObj( $objOriginalScheduledCharge, \CScheduledCharge::class ) ) {

				if( self::NOTICE == $intActionMode ) {
					$this->preparePostScheduledChargesCriteria( $this->getEndDate(), [ $objOriginalScheduledCharge->getId() ], $boolIsDryRun = false, $intActionMode, $intCompanyUserId, $objDatabase );
				} elseif( self::REVERSAL == $intActionMode ) {
					$this->preparePostScheduledChargesCriteria( $this->getStartDate(), [ $objOriginalScheduledCharge->getId() ], $boolIsDryRun = false, $intActionMode, $intCompanyUserId, $objDatabase );
				} elseif( $this->getLeaseStatusTypeId() != CLeaseStatusType::FUTURE ) {
					$this->preparePostScheduledChargesCriteria( date( 'm/t/Y' ), [ $objOriginalScheduledCharge->getId() ], $boolIsDryRun = false, $intActionMode, $intCompanyUserId, $objDatabase );
				}

			} elseif( false == valObj( $objOriginalScheduledCharge, \CScheduledCharge::class ) ) {
				// This is for newly added amounts
				if( self::NOTICE != $intActionMode && true == $this->addNewAddOnLeaseAssociationCharges( $boolIsRecurring = true, $boolIsMoveIn = false, $boolIsReservationFee = false, $intCompanyUserId, $objDatabase ) ) {
					// do not add new charges during notice / reverse notice action
					if( true == valArr( $this->m_arrintRecurringScheduledChargeIds ) ) {
						$boolIsValid &= $this->preparePostScheduledChargesCriteria( date( 'm/t/Y' ), $this->m_arrintRecurringScheduledChargeIds, $boolIsDryRun = false, $intActionMode, $intCompanyUserId, $objDatabase );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function manageMoveInCharges( $objMoveInScheduledCharge, $intCompanyUserId, $objDatabase ) {
		// Manage Move-In SC
		$boolIsValid = true;
		$arrobjAddOnLeaseAssociationCharges = $this->getScheduledCharges();

		if( true == valObj( $objMoveInScheduledCharge, \CScheduledCharge::class ) ) {
			$objMoveInScheduledCharge->setScheduledChargeLogTypeId( CScheduledChargeLogType::ADD_ON_UPDATED );

			$strFunction = ( true == is_null( $arrobjAddOnLeaseAssociationCharges[$objMoveInScheduledCharge->getId()]->getChargeAmount() ) ) ? 'delete' : 'update';
			if( false == $objMoveInScheduledCharge->$strFunction( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsg( $objMoveInScheduledCharge->getErrorMsgs() );
				$boolIsValid &= false;
			}
		}
		return $boolIsValid;
	}

	public function setCommonReservationData( $objProperty, $objLease, $objClient ) {
		$this->setProperty( $objProperty );
		$this->setLease( $objLease );
		$this->setClient( $objClient );
	}

	public function logActivityForAddOnLeaseAssociation( $objDatabase, $intEventSubTypeId, $intCurrentUserId, $arrmixData = [] ) {

		$objCompanyUser	= \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCurrentUserId, $this->getCid(), $objDatabase );
		$strApplicantName = NULL;
		$objApplicant = NULL;
		if( '' != $this->getApplicantId() && false == is_null( $this->getApplicantId() ) ) {
			$objApplicant = CApplicants::fetchApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objApplicant, \CApplicant::class ) ) {
				$strApplicantName = $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast();
				$arrmixData['applicant_name'] = $strApplicantName;
			}

		}

		if( CEventLibrary::PROSPECT_PORTAL_USER_ID == $intCurrentUserId && '' != $strApplicantName ) {
			$arrmixData['is_applicant'] = true;
		} else {
			$strUserName = ( true == valObj( $objCompanyUser, \CCompanyUser::class ) ) ? $objCompanyUser->getUsername( false ) : '';
			$arrmixData['is_applicant'] = false;
			$arrmixData['user_name'] = $strUserName;
		}

		if( false == in_array( $intEventSubTypeId, [ CEventSubType::ADD_ON_ADDED, CEventSubType::ADD_ON_DELETED, CEventSubType::ADD_ON_UPDATED ] ) ) {
			return NULL;
		}

		$arrmixData['add_on_name'] = $this->getAddOnFullName();
		$arrmixData['add_on_category_name'] = $this->getAddOnCategoryName();

		require_once( PATH_LIBRARIES_PSI . 'EventLibrary/CEventLibrary.class.php' );

		$objEventLibrary  = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setApplicant( $objApplicant );
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setData( $arrmixData );
		$objSelectedEvent = $objEventLibrary->createEvent( [ $this ], CEventType::ADD_ON, $intEventSubTypeId );
		$objSelectedEvent->setCid( $this->getCid() );
		$objSelectedEvent->setEventDatetime( 'NOW()' );
		$objSelectedEvent->setDoNotExport( true );
		$objSelectedEvent->setCompanyUser( $objCompanyUser );

		if( false == is_null( $this->getLeaseId() ) ) {
			$objSelectedEvent->setDataReferenceId( $this->getLeaseId() );
		} else {
			$objSelectedEvent->setDataReferenceId( $this->getLeaseIntervalId() );
		}

		$objEventLibrary->buildEventDescription( $this );

		return $objEventLibrary;
	}

	public function getActivityChanges( $objOldAddOnLeaseAssociation, $objDatabase, $boolBulkPlaceOnNotice = false ) {

		if( false == valObj( $objOldAddOnLeaseAssociation, \CAddOnLeaseAssociation::class ) ) {
			return NULL;
		}

		$strEventName = '';
		$arrmixChanges = [];
		$arrmixChanges['status_change'] = false;
		$arrmixChanges['bulk_place_on_notice'] = false;
		$arrmixChanges['old_lease_status_type_id'] = $objOldAddOnLeaseAssociation->getLeaseStatusTypeId();
		$arrmixChanges['new_lease_status_type_id'] = $this->getLeaseStatusTypeId();
		$arrmixChanges['start_date_changed'] = false;
		$arrmixChanges['new_start_date'] = NULL;
		$arrmixChanges['old_start_date'] = NULL;
		$arrmixChanges['end_date_changed'] = false;
		$arrmixChanges['new_end_date'] = NULL;
		$arrmixChanges['old_end_date'] = NULL;
		$arrmixChanges['available_on_date_changed'] = false;
		$arrmixChanges['new_available_on_date'] = NULL;
		$arrmixChanges['old_available_on_date'] = NULL;

		$arrmixChanges['rent_changed']          = false;
		$arrmixChanges['rent_amount_changes']   = [];

		$arrmixChanges['deposit_changed']           = false;
		$arrmixChanges['deposit_amount_changes']    = [];

		$arrmixChanges['reservation_changed']           = false;
		$arrmixChanges['reservation_amount_changes']    = [];

		$arrmixChanges['leasing_agent_changed']     = false;
		$arrmixChanges['leasing_agent_added']       = false;
		$arrmixChanges['new_leasing_agent_name']  = NULL;
		$arrmixChanges['old_leasing_agent_name']   = NULL;
		$arrmixChanges['insert_event']              = false;

		if( ( $objOldAddOnLeaseAssociation->getLeaseStatusTypeId() != $this->getLeaseStatusTypeId() ) ) {
			if( true == $boolBulkPlaceOnNotice ) {
				$arrmixChanges['bulk_place_on_notice'] = true;
			}
			$arrmixChanges['status_change'] = true;
			$arrmixChanges['insert_event'] = true;
		}

		if( strtotime( $objOldAddOnLeaseAssociation->getStartDate() ) != strtotime( $this->getStartDate() ) ) {
			$arrmixChanges['start_date_changed'] = true;
			$arrmixChanges['new_start_date'] = $this->getStartDate();
			$arrmixChanges['old_start_date'] = $objOldAddOnLeaseAssociation->getStartDate();
			$arrmixChanges['insert_event'] = true;
		}

		if( ( false == is_null( $objOldAddOnLeaseAssociation->getEndDate() ) || false == is_null( $this->getEndDate() ) ) && strtotime( $objOldAddOnLeaseAssociation->getEndDate() ) != strtotime( $this->getEndDate() ) ) {
			$arrmixChanges['end_date_changed'] = true;
			$arrmixChanges['new_end_date'] = $this->getEndDate();
			$arrmixChanges['old_end_date'] = $objOldAddOnLeaseAssociation->getEndDate();
			$arrmixChanges['insert_event'] = true;
		}

		if( false == is_null( $this->getAvailableOn() ) && false == is_null( $objOldAddOnLeaseAssociation->getAvailableOn() ) && strtotime( $objOldAddOnLeaseAssociation->getAvailableOn() ) != strtotime( $this->getAvailableOn() ) ) {
			$arrmixChanges['available_on_date_changed'] = true;
			$arrmixChanges['new_available_on_date'] = $this->getAvailableOn();
			$arrmixChanges['old_available_on_date'] = $objOldAddOnLeaseAssociation->getAvailableOn();
			$arrmixChanges['insert_event'] = true;
		}

		$arrobjOldAddOnScheduledCharges = ( array ) $objOldAddOnLeaseAssociation->getScheduledCharges();
		$arrobjAddOnScheduledCharges    = ( array ) $this->getScheduledCharges();

		foreach( $arrobjOldAddOnScheduledCharges as $objOldAddOnScheduledCharge ) {

			if( isset( $arrobjAddOnScheduledCharges[$objOldAddOnScheduledCharge->getId()] )
			    && $objOldAddOnScheduledCharge->getId() == $arrobjAddOnScheduledCharges[$objOldAddOnScheduledCharge->getId()]->getId()
			    && ( false == is_null( $objOldAddOnScheduledCharge->getChargeAmount() )
			         || false == is_null( $arrobjAddOnScheduledCharges[$objOldAddOnScheduledCharge->getId()]->getChargeAmount() ) )
			    && $objOldAddOnScheduledCharge->getChargeAmount() != $arrobjAddOnScheduledCharges[$objOldAddOnScheduledCharge->getId()]->getChargeAmount() ) {

				$intChargeAmount = ( true == is_null( $arrobjAddOnScheduledCharges[$objOldAddOnScheduledCharge->getId()]->getChargeAmount() ) ) ? '0' : $arrobjAddOnScheduledCharges[$objOldAddOnScheduledCharge->getId()]->getChargeAmount();
				$strChargeName   = '';
				if( CArTrigger::MONTHLY == $objOldAddOnScheduledCharge->getArTriggerId() ) {
					$strChargeName   = 'rent';
				} elseif( CArTrigger::MOVE_IN == $objOldAddOnScheduledCharge->getArTriggerId() ) {
					$strChargeName   = 'deposit';
				} elseif( CArTrigger::APPLICATION_COMPLETED == $objOldAddOnScheduledCharge->getArTriggerId() || CArTrigger::ASSIGNABLE_ITEM_REPLACEMENT == $objOldAddOnScheduledCharge->getArTriggerId() ) {
					$strChargeName   = 'reservation';
				}
				if( true == valStr( $strChargeName ) ) {
					$arrmixChanges[$strChargeName . '_changed'] = true;
					$arrmixChanges[$strChargeName . '_amount_changes'][] = [ 'ar_code_id' => $objOldAddOnScheduledCharge->getArCodeId(), 'old_amount' => $objOldAddOnScheduledCharge->getChargeAmount(), 'new_amount' => $intChargeAmount ];
				}
			}
		}

		if( ( false == is_null( $objOldAddOnLeaseAssociation->getCompanyEmployeeId() ) || false == is_null( $this->getCompanyEmployeeId() ) ) && $objOldAddOnLeaseAssociation->getCompanyEmployeeId() != $this->getCompanyEmployeeId() ) {

			$objPropertyLeasingAgent = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->getCompanyEmployeeId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objPropertyLeasingAgent, CCompanyEmployee::class ) ) {
				$arrmixChanges['new_leasing_agent_name'] = $objPropertyLeasingAgent->getNameFirst() . ' ' . $objPropertyLeasingAgent->getNameLast();
				$arrmixChanges['insert_event'] = true;
			}

			if( true == valObj( $objOldAddOnLeaseAssociation, CAddOnLeaseAssociation::class ) ) {
				$objOldPropertyLeasingAgent = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $objOldAddOnLeaseAssociation->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
				if( true == valObj( $objOldPropertyLeasingAgent, CCompanyEmployee::class ) ) {
					$arrmixChanges['old_leasing_agent_name'] = $objOldPropertyLeasingAgent->getNameFirst() . ' ' . $objOldPropertyLeasingAgent->getNameLast();
					$arrmixChanges['insert_event'] = true;
				}
			}

			if( true == valObj( $objOldAddOnLeaseAssociation, CAddOnLeaseAssociation::class )
			    && false == is_null( $objOldAddOnLeaseAssociation->getCompanyEmployeeId() ) ) {
				$arrmixChanges['leasing_agent_changed'] = true;
				$arrmixChanges['insert_event'] = true;
			} else {
				$arrmixChanges['leasing_agent_changed'] = true;
				$arrmixChanges['leasing_agent_added'] = true;
				$arrmixChanges['insert_event'] = true;
			}
		}

		return $arrmixChanges;
	}

	public function valExistingAddOnLeaseAssociation( $objDatabase, $boolIsAllowFlexibleDates = false ) {
		$arrobjExistAddOnLeaseAssociations = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationsByLeaseIntervalIdExcludingQuoteIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );

		$boolIsValid = true;
		$strErrorMsg = '';

		if( valArr( $arrobjExistAddOnLeaseAssociations ) ) {
			foreach( $arrobjExistAddOnLeaseAssociations as $objAddOnLeaseAssociation ) {
				if( $objAddOnLeaseAssociation->getArOriginReferenceId() == $this->getArOriginReferenceId() && ( ( true == valStr( $objAddOnLeaseAssociation->getAvailableOn() ) && strtotime( $this->getStartDate() ) < strtotime( $objAddOnLeaseAssociation->getAvailableOn() ) ) || ( true == in_array( $objAddOnLeaseAssociation->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintLeaseSigningLeaseStatusTypes ) ) ) ) {
					if( CAddOnType::SERVICES != $this->getAddOnTypeId() && false == is_null( $objAddOnLeaseAssociation->getEndDate() ) && true == $boolIsAllowFlexibleDates ) {
						$strErrorMsg = __( 'This item is currently reserved until <b style="font-weight: bold;">{%t,0,DATE_NUMERIC_STANDARD}</b>. Please choose a later start date.', [ $objAddOnLeaseAssociation->getEndDate() ] );
					} elseif( CAddOnType::SERVICES != $this->getAddOnTypeId() && true == is_null( $objAddOnLeaseAssociation->getEndDate() ) && true == $boolIsAllowFlexibleDates ) {
						$strErrorMsg = __( 'Please choose a later start date.' );
					} elseif( CAddOnType::SERVICES != $this->getAddOnTypeId() && false == is_null( $objAddOnLeaseAssociation->getEndDate() ) && false == $boolIsAllowFlexibleDates ) {
						$strErrorMsg = __( 'This item is currently reserved until <b style="font-weight: bold;">{%t,0,DATE_NUMERIC_STANDARD}</b>.', [ $objAddOnLeaseAssociation->getEndDate() ] );
					} elseif( CAddOnType::SERVICES == $this->getAddOnTypeId() && strtotime( $this->getStartDate() ) == strtotime( $objAddOnLeaseAssociation->getStartDate() ) ) {
						$strErrorMsg = __( 'This service is already scheduled for this unit on {%t,0,DATE_NUMERIC_STANDARD}. Please choose another date.', [$this->getStartDate()] );
					}
					if( true == valStr( $strErrorMsg ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', $strErrorMsg ) );
						break;
					}
				}
			}
		}
		return $boolIsValid;
	}

	public function isStudentProperty( $objProperty, $arrobjPropertyPreferences = NULL, $objDatabase ) {

		$this->m_boolIsStudentSemesterSelectionEnabled = false;

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			if( false == valArr( $arrobjPropertyPreferences ) ) {
				$arrobjPropertyPreferences = $objProperty->fetchPropertyPreferencesByKeysKeyedByKey( [ 'ENABLE_SEMESTER_SELECTION' ], $objDatabase );
			}
			if( true == valArr( $objProperty->getOccupancyTypeIds() ) && true == in_array( COccupancyType::STUDENT, $objProperty->getOccupancyTypeIds() ) && true == getPropertyPreferenceValueByKey( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
				$this->m_boolIsStudentSemesterSelectionEnabled = true;
			}

			return $this->m_boolIsStudentSemesterSelectionEnabled;
		}
	}

	public function setHasInventoryFlagForItem( $boolHasInventoryFlagForItem = false ) {
		$this->m_boolHasInventoryFlagForItem = $boolHasInventoryFlagForItem;
	}

	public function getHasInventoryFlagForItem() {
		return $this->m_boolHasInventoryFlagForItem;
	}

	public function postServiceSelectedCharges( $intCompanyUserId, $objDatabase ) {
		$boolIsValid = true;
		if( CLeaseStatusType::APPLICANT == $this->getLeaseStatusTypeId() ) {
			return $boolIsValid;
		}

		$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseAssociationIdsByArTriggerIdsByPropertyIdByCid( [ $this->getId() ], [ CArTrigger::SERVICE_SELECTED ], $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( false === valArr( $arrobjScheduledCharges ) ) {
			return $boolIsValid;
		}

		$objPostScheduledChargesLibrary  = CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::CONDITIONAL_CHARGES, $objDatabase );
		$objPostScheduledChargesCriteria = $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->getCid(), [ $this->getPropertyId() ], $intCompanyUserId );

		$objPostScheduledChargesCriteria->setScheduledChargeIds( array_keys( $arrobjScheduledCharges ) )
			->setDataReturnTypeId( CPostScheduledChargesCriteria::DATA_RETURN_TYPE_AR_TRANSACTIONS )
			->setLeaseIds( [ $this->getLeaseId() ] )
			->setCacheableArTriggerIds( [ CArTrigger::SERVICE_SELECTED ] );

		if( false == $objPostScheduledChargesLibrary->validate( $objPostScheduledChargesCriteria ) || false == $objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria ) ) {
			$this->addErrorMsgs( $objPostScheduledChargesLibrary->getErrorMsgs() );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function getScheduledServiceDate() {
		$strScheduledServiceDate = '';

		$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchLastEventByDataReferenceIdByLeaseIntervalIdByEventTypeIdByEventSubTypeIdByCid( $this->getId(), $this->getLeaseIntervalId(), \CEventType::SERVICES, \CEventSubType::SERVICE, $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objEvent, 'CEvent' ) ) {
			$arrmixEventDetails = ( array ) $objEvent->getDetails();
			$strScheduledServiceDate = __( '{%t, 0, DATE_NUMERIC_STANDARD}', [ $arrmixEventDetails['service_event_date'] ] );
		}

		return $strScheduledServiceDate;
	}

	public function getRescheduledServiceOldDate() {
		$strRescheduledServiceOldDate = '';

		$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchLastEventByDataReferenceIdByLeaseIntervalIdByEventTypeIdByEventSubTypeIdByCid( $this->getId(), $this->getLeaseIntervalId(), \CEventType::SERVICES, \CEventSubType::SERVICE, $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objEvent, 'CEvent' ) ) {
			$arrmixEventDetails = ( array ) $objEvent->getDetails();
			$arrmixDisplayData  = ( array ) $arrmixEventDetails['display_data'];
			$strRescheduledServiceOldDate = __( '{%t, 0, DATE_NUMERIC_STANDARD}', [ $arrmixDisplayData['old_value'] ] );
		}

		return $strRescheduledServiceOldDate;
	}

	public function getServiceCharges() {

		if( false == valId( $this->getId() ) ) {
			return NULL;
		}

		$objTaxCalculationLibrary = new \Psi\Core\AccountsReceivables\TaxCalculation\CTaxCalculationLibrary( $this->getCId(), $this->getPropertyId(), $this->m_objDatabase );
		$objPropertyChargeSetting = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		$strChargeAmount         = '';
		$strChargeTiming         = '';
		$strScheduledCharge      = '';
		$strChargeAmountAddition = '';

		$arrstrChargeAmountChargeTiming = [];
		$arrstrChargeTiming             = [];

		$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseAssociationIdsByArTriggerIdsByPropertyIdByCid( [ $this->getId() ], \CArTrigger::$c_arrintAddOnsArTriggers, $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		foreach( $arrobjScheduledCharges as $objAddOnScheduledCharge ) {
			$objAddOnScheduledCharge->setTaxPercent( $objTaxCalculationLibrary->totalTaxPercentMultiplier( $objAddOnScheduledCharge->getArCodeId(), $strEffectiveDate = NULL ) );
			if( 0 < $objAddOnScheduledCharge->getChargeAmount() ) {

				if( 0 < $objAddOnScheduledCharge->getTaxPercent() && true == $objPropertyChargeSetting->getIncludeTaxInAdvertisedRent() ) {
					$strChargeAmount = $objAddOnScheduledCharge->getChargeAmount() + $objAddOnScheduledCharge->getChargeAmount() * $objAddOnScheduledCharge->getTaxPercent();
				} else {
					$strChargeAmount = $objAddOnScheduledCharge->getChargeAmount();
				}

				if( true == in_array( $objAddOnScheduledCharge->getArTriggerId(), \CArTrigger::$c_arrintAddOnsArTriggers ) ) {
					$strChargeTiming = \CArTrigger::createService()->getArTriggerNameByArTriggerId( $objAddOnScheduledCharge->getArTriggerId() );
				}

				if( true == valArr( $arrstrChargeTiming ) && true == in_array( $strChargeTiming, $arrstrChargeTiming ) ) {
					$strChargeAmountAddition = $strChargeAmountAddition + $strChargeAmount;
				} else {
					$strChargeAmountAddition = $strChargeAmount;
				}

				$arrstrChargeTiming[] = $strChargeTiming;

				$arrstrChargeAmountChargeTiming[$strChargeTiming] = webMoneyFormat( $strChargeAmountAddition ) . ' / ' . $strChargeTiming;
			}
		}

		if( true == valArr( $arrstrChargeAmountChargeTiming ) ) {
			$strScheduledCharge = implode( ', ', $arrstrChargeAmountChargeTiming );
		}

		return $strScheduledCharge;
	}

	public function getOldServiceDaysSelected() {

		if( false == valId( $this->getId() ) ) {
			return NULL;
		}

		$objEvent            = \Psi\Eos\Entrata\CEvents::createService()->fetchLastEventByDataReferenceIdByLeaseIntervalIdByEventTypeIdByEventSubTypeIdByCid( $this->getId(), $this->getLeaseIntervalId(), \CEventType::SERVICES, \CEventSubType::SERVICE, $this->getCid(), $this->m_objDatabase );
		$arrstrOldDaysOfWeek = [];

		if( true == valObj( $objEvent, 'CEvent' ) ) {
			$arrmixEventDetails = ( array ) $objEvent->getDetails();
			$arrmixDisplayData  = ( array ) $arrmixEventDetails['display_data'];
			sort( $arrmixDisplayData['old_days_of_week'] );
			foreach( $arrmixDisplayData['old_days_of_week'] as $intDayOfWeek ) {
				$arrstrOldDaysOfWeek[] = \CAddOn::createService()->getWeekDaysAbbr()[$intDayOfWeek];
			}

			if( true == valArr( $arrstrOldDaysOfWeek ) ) {
				return implode( ', ', $arrstrOldDaysOfWeek );
			}
		}
	}

}
?>