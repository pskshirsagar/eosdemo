<?php

class CPetLeaseAssociation extends CLeaseAssociation {

	protected $m_strPetTypeName;
	protected $m_strPetName;
	protected $m_strCustomerName;
	protected $m_boolIsAssistanceAnimal;

	/**
	 * Get Functions
	 */

	public function getPetTypeName() {
		return $this->m_strPetTypeName;
	}

	public function getPetName() {
		return $this->m_strPetName;
	}

	public function getCustomerName() {
		return $this->m_strCustomerName;
	}

	/**
	 * Set functions
	 */

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true === isset( $arrmixValues['pet_type_name'] ) )			$this->setPetTypeName( $arrmixValues['pet_type_name'] );
		if( true === isset( $arrmixValues['pet_name'] ) )				$this->setPetName( $arrmixValues['pet_name'] );
		if( true === isset( $arrmixValues['customer_name'] ) )			$this->setCustomerName( $arrmixValues['customer_name'] );
		if( true === isset( $arrmixValues['is_assistance_animal'] ) )	$this->setIsAssistanceAnimal( $arrmixValues['is_assistance_animal'] );
	}

	public function setPetTypeName( $strPetTypeName ) {
		$this->m_strPetTypeName = $strPetTypeName;
	}

	public function setPetName( $strPetName ) {
		$this->m_strPetName = $strPetName;
	}

	public function setCustomerName( $strCustomerName ) {
		$this->m_strCustomerName = $strCustomerName;
	}

	public function setIsAssistanceAnimal( $boolIsAssistanceAnimal ) {
		$this->set( 'm_boolIsAssistanceAnimal', CStrings::strToBool( $boolIsAssistanceAnimal ) );
	}

	public function getIsAssistanceAnimal() {
		return $this->m_boolIsAssistanceAnimal;
	}

	/**
	 * Create functions
	 */

	public function validate( $strAction, $objDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case 'affordable_validate_insert':
			case 'affordable_validate_update':
				$boolIsValid &= $this->valPetMaxAllowed( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valPetMaxAllowed( $objDatabase ) {

		if( true == is_null( $this->getUnitSpaceId() ) ) {
			$objPetRateAssociation = \Psi\Eos\Entrata\Custom\CPetRateAssociations::createService()->fetchPetRateAssociationByPropertyIdByArCascadeIdByArCascadeReferenceIdByPetTypeIdByCidWithPetTypeName( $this->getPropertyId(), CArCascade::PROPERTY, $this->getPropertyId(), $this->getArOriginReferenceId(), $this->getCid(), $objDatabase );
		} else {
			$objPetRateAssociation = \Psi\Eos\Entrata\Custom\CPetRateAssociations::createService()->fetchPetRateAssociationByPropertyIdByArCascadeIdByArCascadeReferenceIdByPetTypeIdByCidWithPetTypeName( $this->getPropertyId(), CArCascade::SPACE, $this->getUnitSpaceId(), $this->getArOriginReferenceId(), $this->getCid(), $objDatabase );
		}

		if( false == valObj( $objPetRateAssociation, 'CPetRateAssociation' ) ) return true;

		$intMaxAllowed = $objPetRateAssociation->getPetMaxAllowed();

		if( true == empty( $intMaxAllowed ) || 0 >= $intMaxAllowed ) return true;

		$intPetCount = ( int ) \Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetCountByLeaseIdByLeaseIntervalIdByCustomerIdByPetTypeIdByCid( $this->getLeaseId(), $this->getLeaseIntervalId(), $this->getArOriginObjectId(), $this->getCustomerId(), $this->getArOriginReferenceId(), $this->getCid(), $objDatabase );

		$intPetCount++;

		if( $intMaxAllowed < $intPetCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_max_allowed' . $objPetRateAssociation->getPetTypeName(), __( 'More than {%d,0} {%s,1} are not allowed', [ ( int ) $intMaxAllowed, $objPetRateAssociation->getDefaultPetTypeName() ] ) ) );
			return false;
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolPerformDelete = false ) {
		if( false == $boolPerformDelete ) {

			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {

			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

	}

}
?>