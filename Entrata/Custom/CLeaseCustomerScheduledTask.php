<?php
class CLeaseCustomerScheduledTask extends CScheduledTask {

	protected $m_intNumDays;
	protected $m_intEventDays;
	protected $m_intComposeEmailId;
	protected $m_intPsProductId;
	protected $m_intMaintenanceStatusTypeId;
	protected $m_intHolidayEventTypeId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intCompanyHolidayId;
	protected $m_intOverdueBusinessHours;
	protected $m_intEventTriggerType;

	protected $m_strHolidayEventName;
	protected $m_strFollowUpInstructions;
	protected $m_strEventTriggerType;
	protected $m_strEmailSubject;
	protected $m_strSystemMessageKey;
	protected $m_strSmsSystemMessageKey;
	protected $m_strSmsMessage;
	protected $m_strScheduledTaskIds;
	protected $m_strComposeEmailIds;
	protected $m_strSurveyResponse;

	protected $m_boolIsEnableEmail;
	protected $m_boolIsEnableSubject;
	protected $m_boolIsDeleted;
	protected $m_boolIsVerbose;
	protected $m_boolIsEnableFollowUp;
	protected $m_boolIsEnableManualContact;
	protected $m_boolIsEnableSms;
	protected $m_boolIsEntrataDefault;
	protected $m_boolIsTriggerOnStatusChange;

	protected $m_intEventDaysCount;
	protected $m_intIsEnableEmailCount;
	protected $m_intIsEnableManualContactCount;
	protected $m_intManualContactInstructionsCount;
	protected $m_intBccEmailAddressCount;
	protected $m_intSubjectCount;
	protected $m_intSystemMessageTemplateIdCount;
	protected $m_intOverdueBusinessHoursCount;
	protected $m_intAttachmentCount;

	protected $m_objEmailDatabase;
	protected $m_objAdminDatabase;
	protected $m_objPaymentDatabase;

	protected $m_arrintPsProductIds;

	const TASK_EXECUTE_INTERVAL = 1;
	const DAYS_NOT_PROGRESSING_DOCUMENT_INITIATED = 30;

	const EVENT_TRIGGER_TYPE_IMMEDIATE			= 'immediate';
	const EVENT_TRIGGER_TYPE_AFTER				= 'after';
	const EVENT_TRIGGER_TYPE_BEFORE				= 'before';
	const EVENT_TRIGGER_TYPE_DAY_OF_EVENT		= 'day_of_event';

	const SCHEDULED_TASK_ID						= 'id';
	const PROPERTY_ID							= 'property_id';
	const NUM_DAYS								= 'num_days';
	const EVENT_DAYS							= 'event_days';
	const COMPOSE_EMAIL_ID 						= 'compose_email_id';
	const EMAIL_SUBJECT 						= 'email_subject';
	const PS_PRODUCT_ID 						= 'ps_product_id';
	const EVENT_TRIGGER_TYPE 					= 'event_trigger_type';
	const EVENT_TYPE_ID							= 'event_type_id';
	const EVENT_SUB_TYPE_ID						= 'event_sub_type_id';
	const IS_ENABLE_EMAIL 						= 'is_enable_email';
	const IS_ENABLE_SUBJECT 					= 'is_enable_subject';
	const IS_DELETED 							= 'is_deleted';
	const MAINTENANCE_STATUS_TYPE_ID			= 'maintenance_status_type_id';
	const FOLLOWUP_INSTRUCTIONS 				= 'followup_instructions';
	const HOLIDAY_EVENT_TYPE_ID 				= 'holiday_event_type_id';
	const COMPANY_HOLIDAY_ID 					= 'company_holiday_id';
	const HOLIDAY_EVENT_NAME					= 'holiday_event_name';
	const LEASE_INTERVAL_TYPE_ID				= 'lease_interval_type_id';
	const IS_ENABLE_FOLLOW_UP					= 'is_enable_follow_up';
	const IS_ENABLE_MANUAL_CONTACT 				= 'is_enable_manual_contact';
	const MANUAL_CONTACT_INSTRUCTIONS 			= 'manual_contact_instructions';
	const SYSTEM_MESSAGE_KEY 					= 'system_message_key';
	const SMS_SYSTEM_MESSAGE_KEY 				= 'sms_system_message_key';
	const SYSTEM_EMAIL_TYPE_ID 					= 'system_email_type_id';
	const IS_ENABLE_SMS							= 'is_enable_sms';
	const SMS_MESSAGE							= 'sms_message';
	const OVERDUE_BUSINESS_HOURS				= 'overdue_business_hours';
	const IS_SEND_EMAIL_TO_PROPERTY 			= 'is_send_email_to_property';
	const IS_ENTRATA_DEFAULT					= 'is_entrata_default';
	const IS_TRIGGER_ON_STATUS_CHANGE 			= 'is_trigger_on_status_change';
	const SURVEY_RESPONSE			            = 'survey_response';

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Get Functions
	 *
	 */

	public function getNumDays() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'num_days' );
	}

	public function getEventDays() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'event_days' );
	}

	public function getComposeEmailId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'compose_email_id' );
	}

	public function getLeaseIntervalTypeId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'lease_interval_type_id' );
	}

	public function getCompanyHolidayId() {
		return $this->m_intCompanyHolidayId;
	}

	public function getHolidayEventName() {
		return $this->m_strHolidayEventName;
	}

	public function getFollowUpInstructions() {
		return $this->m_strFollowUpInstructions;
	}

	public function getPsProductId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'ps_product_id' );
	}

	public function getEventTriggerType() {
		return $this->getJsonbFieldValue( 'ScheduleDetails', 'event_trigger_type' );
	}

	public function getEventSubTypeId() {
		return $this->m_intEventSubTypeId;
	}

	public function getIsEnableEmail() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_email' );
	}

	public function getIsEnableSubject() {
		return ( int ) $this->m_boolIsEnableSubject;
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function getMaintenanceStatusTypeId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'maintenance_status_type_id' );
	}

	public function getSurveyResponse() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'survey_response' );
	}

	public function getEmailSubject() {
		return $this->m_strEmailSubject;
	}

	public function getIsEnableFollowup() {
		return $this->m_boolIsEnableFollowUp;
	}

	public function getIsEnableManualContact() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_manual_contact' );
	}

	public function getSystemMessageKey() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'system_message_key' );
	}

	public function getSmsSystemMessageKey() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'sms_system_message_key' );
	}

	public function getSystemEmailTypeId() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'system_email_type_id' );
	}

	public function getIsEnableSms() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'is_enable_sms' );
	}

	public function getSmsMessage() {
		return $this->m_strSmsMessage;
	}

	public function getOverdueBusinessHours() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'overdue_business_hours' );
	}

	public function getScheduledTaskIds() {
		return $this->m_strScheduledTaskIds;
	}

	public function getEventDaysCount() {
		return $this->m_intEventDaysCount;
	}

	public function getIsEnableEmailCount() {
		return $this->m_intIsEnableEmailCount;
	}

	public function getIsEnableManualContactCount() {
		return $this->m_intIsEnableManualContactCount;
	}

	public function getManualContactInstructionsCount() {
		return $this->m_intManualContactInstructionsCount;
	}

	public function getBccEmailAddressCount() {
		return $this->m_intBccEmailAddressCount;
	}

	public function getSubjectCount() {
		return $this->m_intSubjectCount;
	}

	public function getSystemMessageTemplateIdCount() {
		return $this->m_intSystemMessageTemplateIdCount;
	}

	public function getComposeEmailIds() {
		return $this->m_strComposeEmailIds;
	}

	public function getOverdueBusinessHoursCount() {
		return $this->m_intOverdueBusinessHoursCount;
	}

	public function getAttachmentCount() {
		return $this->m_intAttachmentCount;
	}

	public function getIsEntrataDefault() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'is_entrata_default' );
	}

	public function getIsTriggerOnStatusChange() {
		return $this->getJsonbFieldValue( 'TaskDetails', 'is_trigger_on_status_change' );
	}

	public function getIsIncludeRMContact() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'include_rm_contact' );
	}

	public function getIsExcludeFinancialDetails() {
		return ( int ) $this->getJsonbFieldValue( 'TaskDetails', 'exclude_financial_details' );
	}

	/**
	 * Set Functions
	 *
	 */

	public function setScheduledTaskIds( $strScheduledTaskIds ) {
		$this->m_strScheduledTaskIds = preg_replace( '/\s+/', '', $strScheduledTaskIds );
	}

	public function setComposeEmailIds( $strComposeEmailIds ) {
		$this->m_strComposeEmailIds = preg_replace( '/\s+/', '', $strComposeEmailIds );
	}

	public function setNumDays( $intNumDays ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'num_days', $intNumDays );
	}

	public function setEventDays( $intEventDays ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'event_days', $intEventDays );
	}

	public function setComposeEmailId( $intComposeEmailId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'compose_email_id', $intComposeEmailId );
	}

	public function setHolidayEventTypeId( $intHolidayEventTypeId ) {
		$this->m_intHolidayEventTypeId = $intHolidayEventTypeId;
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'lease_interval_type_id', $intLeaseIntervalTypeId );
	}

	public function setCompanyHolidayId( $intCompanyHolidayId ) {
		$this->m_intCompanyHolidayId = $intCompanyHolidayId;
	}

	public function setHolidayEventName( $strHolidayEventName ) {
		$this->m_strHolidayEventName = $strHolidayEventName;
	}

	public function setFollowUpInstructions( $strFollowUpInstructions ) {
		$this->m_strFollowUpInstructions = $strFollowUpInstructions;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'ps_product_id', $intPsProductId );
	}

	public function setEventTriggerType( $strEventTriggerType ) {
		$this->setJsonbFieldValue( 'ScheduleDetails', 'event_trigger_type', $strEventTriggerType );
	}

	public function setEventSubTypeId( $intEventSubTypeId ) {
		$this->m_intEventSubTypeId = $intEventSubTypeId;
	}

	public function setIsEnableEmail( $boolIsEnableEmail ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_email', $boolIsEnableEmail );
	}

	public function setIsEnableSubject( $boolIsEnableSubject ) {
		$this->m_boolIsEnableSubject = $boolIsEnableSubject;
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->m_boolIsDeleted = $boolIsDeleted;
	}

	public function setMaintenanceStatusTypeId( $intMaintenanceStatusTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'maintenance_status_type_id', $intMaintenanceStatusTypeId );
	}

	public function setSurveyResponse( $strSurveyResponse ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'survey_response', $strSurveyResponse );
	}

	public function setEmailSubject( $strEmailSubject ) {
		$this->m_strEmailSubject = $strEmailSubject;
	}

	public function setIsEnableFollowUp( $boolIsEnableFollowUp ) {
		$this->m_boolIsEnableFollowUp = $boolIsEnableFollowUp;
	}

	public function setIsEnableManualContact( $boolIsEnableManualContact ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_manual_contact', $boolIsEnableManualContact );
	}

	public function setSystemMessageKey( $strSystemMessageKey ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'system_message_key', $strSystemMessageKey );
	}

	public function setSmsSystemMessageKey( $strSmsSystemMessageKey ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'sms_system_message_key', $strSmsSystemMessageKey );
	}

	public function setSystemEmailTypeId( $intSystemEmailTypeId ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'system_email_type_id', $intSystemEmailTypeId );
	}

	public function setIsEnableSms( $boolIsEnableSms ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_enable_sms', $boolIsEnableSms );
	}

	public function setSmsMessage( $strSmsMessage ) {
		$this->m_strSmsMessage = $strSmsMessage;
	}

	public function setOverdueBusinessHours( $intOverdueBusinessHours ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'overdue_business_hours', $intOverdueBusinessHours );
	}

	public function setEventDaysCount( $intEventDaysCount ) {
		$this->m_intEventDaysCount = $intEventDaysCount;
	}

	public function setIsEnableEmailCount( $intIsEnableEmailCount ) {
		$this->m_intIsEnableEmailCount = $intIsEnableEmailCount;
	}

	public function setIsEnableManualContactCount( $intIsEnableManualContactCount ) {
		$this->m_intIsEnableManualContactCount = $intIsEnableManualContactCount;
	}

	public function setManualContactInstructionsCount( $intManualContactInstructionsCount ) {
		$this->m_intManualContactInstructionsCount = $intManualContactInstructionsCount;
	}

	public function setBccEmailAddressCount( $intBccEmailAddressCount ) {
		$this->m_intBccEmailAddressCount = $intBccEmailAddressCount;
	}

	public function setSubjectCount( $intSubjectCount ) {
		$this->m_intSubjectCount = $intSubjectCount;
	}

	public function setSystemMessageTemplateIdCount( $intSystemMessageTemplateIdCount ) {
		$this->m_intSystemMessageTemplateIdCount = $intSystemMessageTemplateIdCount;
	}

	public function setOverdueBusinessHoursCount( $intOverDueBusinessHoursCount ) {
		$this->m_intOverdueBusinessHoursCount = $intOverDueBusinessHoursCount;
	}

	public function setAttachmentCount( $intAttachmentCount ) {
		$this->m_intAttachmentCount = $intAttachmentCount;
	}

	public function setIsEntrataDefault( $boolIsEntrataDefault ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_entrata_default', $boolIsEntrataDefault );
	}

	public function setIsTriggerOnStatusChange( $boolIsTriggerOnStatusChange ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'is_trigger_on_status_change', $boolIsTriggerOnStatusChange );
	}

	public function setIsIncludeRMContact( $boolIsIncludeRMContact ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'include_rm_contact', $boolIsIncludeRMContact );
	}

	public function setIsExcludeFinancialDetails( $boolIsExcludeFinancialDetails ) {
		$this->setJsonbFieldValue( 'TaskDetails', 'exclude_financial_details', $boolIsExcludeFinancialDetails );
	}

	/**
	 * Other Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['id'] ) && $boolDirectSet ) {
			$this->m_intId = trim( $arrmixValues['id'] );
		} elseif( isset( $arrmixValues['id'] ) ) {
			$this->setId( $arrmixValues['id'] );
		}

		if( isset( $arrmixValues['num_days'] ) && $boolDirectSet ) {
			$this->m_intNumDays = trim( $arrmixValues['num_days'] );
		} elseif( isset( $arrmixValues['num_days'] ) ) {
			$this->setNumDays( $arrmixValues['num_days'] );
		}

		if( isset( $arrmixValues['event_days'] ) && $boolDirectSet ) {
			$this->m_intEventDays = trim( $arrmixValues['event_days'] );
		} elseif( isset( $arrmixValues['event_days'] ) ) {
			$this->setEventDays( $arrmixValues['event_days'] );
		}

		if( isset( $arrmixValues['compose_email_id'] ) && $boolDirectSet ) {
			$this->m_intComposeEmailId = trim( $arrmixValues['compose_email_id'] );
		} elseif( isset( $arrmixValues['compose_email_id'] ) ) {
			$this->setComposeEmailId( $arrmixValues['compose_email_id'] );
		}

		if( isset( $arrmixValues['holiday_event_type_id'] ) && $boolDirectSet ) {
			$this->m_intHolidayEventTypeId = trim( $arrmixValues['holiday_event_type_id'] );
		} elseif( isset( $arrmixValues['holiday_event_type_id'] ) ) {
			$this->setHolidayEventTypeId( $arrmixValues['holiday_event_type_id'] );
		}

		if( isset( $arrmixValues['lease_interval_type_id'] ) && $boolDirectSet ) {
			$this->m_intLeaseIntervalTypeId = trim( $arrmixValues['lease_interval_type_id'] );
		} elseif( isset( $arrmixValues['lease_interval_type_id'] ) ) {
			$this->setLeaseIntervalTypeId( $arrmixValues['lease_interval_type_id'] );
		}

		if( isset( $arrmixValues['company_holiday_id'] ) && $boolDirectSet ) {
			$this->m_intCompanyHolidayId = trim( $arrmixValues['company_holiday_id'] );
		} elseif( isset( $arrmixValues['company_holiday_id'] ) ) {
			$this->setCompanyHolidayId( $arrmixValues['company_holiday_id'] );
		}

		if( isset( $arrmixValues['holiday_event_name'] ) && $boolDirectSet ) {
			$this->m_strHolidayEventName = trim( $arrmixValues['holiday_event_name'] );
		} elseif( isset( $arrmixValues['holiday_event_name'] ) ) {
			$this->setHolidayEventName( $arrmixValues['holiday_event_name'] );
		}

		if( isset( $arrmixValues['followup_instructions'] ) && $boolDirectSet ) {
			$this->m_strFollowUpInstructions = trim( $arrmixValues['followup_instructions'] );
		} elseif( isset( $arrmixValues['followup_instructions'] ) ) {
			$this->setFollowUpInstructions( $arrmixValues['followup_instructions'] );
		}

		if( isset( $arrmixValues['email_subject'] ) && $boolDirectSet ) {
			$this->m_strEmailSubject = trim( $arrmixValues['email_subject'] );
		} elseif( isset( $arrmixValues['email_subject'] ) ) {
			$this->setEmailSubject( $arrmixValues['email_subject'] );
		}

		if( isset( $arrmixValues['ps_product_id'] ) && $boolDirectSet ) {
			$this->m_intPsProductId = trim( $arrmixValues['ps_product_id'] );
		} elseif( isset( $arrmixValues['ps_product_id'] ) ) {
			$this->setPsProductId( $arrmixValues['ps_product_id'] );
		}

		if( isset( $arrmixValues['is_enable_email'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableEmail = trim( $arrmixValues['is_enable_email'] );
		} elseif( isset( $arrmixValues['is_enable_email'] ) ) {
			$this->setIsEnableEmail( $arrmixValues['is_enable_email'] );
		}

		if( isset( $arrmixValues['is_enable_follow_up'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableFollowUp = trim( $arrmixValues['is_enable_follow_up'] );
		} elseif( isset( $arrmixValues['is_enable_follow_up'] ) ) {
			$this->setIsEnableFollowUp( $arrmixValues['is_enable_follow_up'] );
		}

		if( isset( $arrmixValues['is_enable_subject'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableSubject = trim( $arrmixValues['is_enable_subject'] );
		} elseif( isset( $arrmixValues['is_enable_subject'] ) ) {
			$this->setIsEnableSubject( $arrmixValues['is_enable_subject'] );
		}

		if( isset( $arrmixValues['event_trigger_type'] ) && $boolDirectSet ) {
			$this->m_intEventTriggerType = trim( $arrmixValues['event_trigger_type'] );
		} elseif( isset( $arrmixValues['event_trigger_type'] ) ) {
			$this->setEventTriggerType( $arrmixValues['event_trigger_type'] );
		}

		if( isset( $arrmixValues['event_sub_type_id'] ) && $boolDirectSet ) {
			$this->m_intEventSubTypeId = trim( $arrmixValues['event_sub_type_id'] );
		} elseif( isset( $arrmixValues['event_sub_type_id'] ) ) {
			$this->setEventSubTypeId( $arrmixValues['event_sub_type_id'] );
		}

		if( isset( $arrmixValues['is_deleted'] ) && $boolDirectSet ) {
			$this->m_boolIsDeleted = trim( $arrmixValues['is_deleted'] );
		} elseif( isset( $arrmixValues['is_deleted'] ) ) {
			$this->setIsDeleted( $arrmixValues['is_deleted'] );
		}

		if( isset( $arrmixValues['maintenance_status_type_id'] ) && $boolDirectSet ) {
			$this->m_intMaintenanceStatusTypeId = trim( $arrmixValues['maintenance_status_type_id'] );
		} elseif( isset( $arrmixValues['maintenance_status_type_id'] ) ) {
			$this->setMaintenanceStatusTypeId( $arrmixValues['maintenance_status_type_id'] );
		}

		if( isset( $arrmixValues['survey_response'] ) && $boolDirectSet ) {
			$this->m_strSurveyResponse = trim( $arrmixValues['survey_response'] );
		} elseif( isset( $arrmixValues['survey_response'] ) ) {
			$this->setSurveyResponse( $arrmixValues['survey_response'] );
		}

		if( isset( $arrmixValues['is_enable_manual_contact'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableManualContact = trim( $arrmixValues['is_enable_manual_contact'] );
		} elseif( isset( $arrmixValues['is_enable_manual_contact'] ) ) {
			$this->setIsEnableManualContact( $arrmixValues['is_enable_manual_contact'] );
		}

		if( isset( $arrmixValues['system_message_key'] ) && $boolDirectSet ) {
			$this->m_strSystemMessageKey = trim( $arrmixValues['system_message_key'] );
		} elseif( isset( $arrmixValues['system_message_key'] ) ) {
			$this->setSystemMessageKey( $arrmixValues['system_message_key'] );
		}

		if( isset( $arrmixValues['sms_system_message_key'] ) && $boolDirectSet ) {
			$this->m_strSmsSystemMessageKey = trim( $arrmixValues['sms_system_message_key'] );
		} elseif( isset( $arrmixValues['sms_system_message_key'] ) ) {
			$this->setSystemMessageKey( $arrmixValues['sms_system_message_key'] );
		}

		if( isset( $arrmixValues['system_email_type_id'] ) && $boolDirectSet ) {
			$this->m_intSystemEmailTypeId = trim( $arrmixValues['system_email_type_id'] );
		} elseif( isset( $arrmixValues['system_email_type_id'] ) ) {
			$this->setSystemEmailTypeId( $arrmixValues['system_email_type_id'] );
		}

		if( isset( $arrmixValues['sms_message'] ) && $boolDirectSet ) {
			$this->m_strSmsMessage = trim( $arrmixValues['sms_message'] );
		} elseif( isset( $arrmixValues['sms_message'] ) ) {
			$this->setSmsMessage( $arrmixValues['sms_message'] );
		}

		if( isset( $arrmixValues['overdue_business_hours'] ) && $boolDirectSet ) {
			$this->m_intOverdueBusinessHours = trim( $arrmixValues['overdue_business_hours'] );
		} elseif( isset( $arrmixValues['overdue_business_hours'] ) ) {
			$this->setOverdueBusinessHours( $arrmixValues['overdue_business_hours'] );
		}

		if( isset( $arrmixValues['is_enable_sms'] ) && $boolDirectSet ) {
			$this->m_boolIsEnableSms = trim( $arrmixValues['is_enable_sms'] );
		} elseif( isset( $arrmixValues['is_enable_sms'] ) ) {
			$this->setIsEnableSms( $arrmixValues['is_enable_sms'] );
		}

		if( isset( $arrmixValues['scheduled_task_ids'] ) && $boolDirectSet ) {
			$this->m_strScheduledTaskIds = trim( $arrmixValues['scheduled_task_ids'] );
		} elseif( isset( $arrmixValues['scheduled_task_ids'] ) ) {
			$this->setScheduledTaskIds( $arrmixValues['scheduled_task_ids'] );
		}

		if( isset( $arrmixValues['event_days_count'] ) && $boolDirectSet ) {
			$this->m_intEventDaysCount = trim( $arrmixValues['event_days_count'] );
		} elseif( isset( $arrmixValues['event_days_count'] ) ) {
			$this->setEventDaysCount( $arrmixValues['event_days_count'] );
		}

		if( isset( $arrmixValues['is_enable_email_count'] ) && $boolDirectSet ) {
			$this->m_intIsEnableEmailCount = trim( $arrmixValues['is_enable_email_count'] );
		} elseif( isset( $arrmixValues['is_enable_email_count'] ) ) {
			$this->setIsEnableEmailCount( $arrmixValues['is_enable_email_count'] );
		}

		if( isset( $arrmixValues['is_enable_manual_contact_count'] ) && $boolDirectSet ) {
			$this->m_intIsEnableManualContactCount = trim( $arrmixValues['is_enable_manual_contact_count'] );
		} elseif( isset( $arrmixValues['is_enable_manual_contact_count'] ) ) {
			$this->setIsEnableManualContactCount( $arrmixValues['is_enable_manual_contact_count'] );
		}

		if( isset( $arrmixValues['manual_contact_instructions_count'] ) && $boolDirectSet ) {
			$this->m_intManualContactInstructionsCount = trim( $arrmixValues['manual_contact_instructions_count'] );
		} elseif( isset( $arrmixValues['manual_contact_instructions_count'] ) ) {
			$this->setManualContactInstructionsCount( $arrmixValues['manual_contact_instructions_count'] );
		}

		if( isset( $arrmixValues['bcc_email_address_count'] ) && $boolDirectSet ) {
			$this->m_intBccEmailAddressCount = trim( $arrmixValues['bcc_email_address_count'] );
		} elseif( isset( $arrmixValues['bcc_email_address_count'] ) ) {
			$this->setBccEmailAddressCount( $arrmixValues['bcc_email_address_count'] );
		}

		if( isset( $arrmixValues['subject_count'] ) && $boolDirectSet ) {
			$this->m_intSubjectCount = trim( $arrmixValues['subject_count'] );
		} elseif( isset( $arrmixValues['subject_count'] ) ) {
			$this->setSubjectCount( $arrmixValues['subject_count'] );
		}

		if( isset( $arrmixValues['system_message_template_id_count'] ) && $boolDirectSet ) {
			$this->m_intSystemMessageTemplateIdCount = trim( $arrmixValues['system_message_template_id_count'] );
		} elseif( isset( $arrmixValues['system_message_template_id_count'] ) ) {
			$this->setSystemMessageTemplateIdCount( $arrmixValues['system_message_template_id_count'] );
		}

		if( isset( $arrmixValues['compose_email_ids'] ) && $boolDirectSet ) {
			$this->m_strComposeEmailIds = trim( $arrmixValues['compose_email_ids'] );
		} elseif( isset( $arrmixValues['compose_email_ids'] ) ) {
			$this->setComposeEmailIds( $arrmixValues['compose_email_ids'] );
		}

		if( isset( $arrmixValues['overdue_business_hours_count'] ) && $boolDirectSet ) {
			$this->m_intOverdueBusinessHoursCount = trim( $arrmixValues['overdue_business_hours_count'] );
		} elseif( isset( $arrmixValues['overdue_business_hours_count'] ) ) {
			$this->setOverdueBusinessHoursCount( $arrmixValues['overdue_business_hours_count'] );
		}

		if( isset( $arrmixValues['is_entrata_default'] ) && $boolDirectSet ) {
			$this->m_boolIsEntrataDefault = trim( $arrmixValues['is_entrata_default'] );
		} elseif( isset( $arrmixValues['is_entrata_default'] ) ) {
			$this->setIsEntrataDefault( $arrmixValues['is_entrata_default'] );
		}

		if( isset( $arrmixValues['is_trigger_on_status_change'] ) && $boolDirectSet ) {
			$this->m_boolIsTriggerOnStatusChange = trim( $arrmixValues['is_trigger_on_status_change'] );
		} elseif( isset( $arrmixValues['is_trigger_on_status_change'] ) ) {
			$this->setIsTriggerOnStatusChange( $arrmixValues['is_trigger_on_status_change'] );
		}
	}

	public static function removeUnsubscribeLinkInEmailContent( $strEmailContent ) {
		$strUnsubscribeLink = '';
		if( true == valStr( $strEmailContent ) ) {
			preg_match( '#<span id="unsubscribe">(.*?)</span>#', $strEmailContent, $arrstrMatchUnsubscribe );

			if( true == valArr( $arrstrMatchUnsubscribe ) ) {
				$strUnsubscribeLink = $arrstrMatchUnsubscribe[1];
			}
			$strEmailContent = str_replace( $strUnsubscribeLink, '', $strEmailContent );
		}

		return $strEmailContent;
	}

	public function validate( $strAction, $objClient = NULL, $objDatabase = NULL, $boolValidateGroupCount = false ) {
		$boolIsValid = true;
		$objClient = $objClient;
		$objDatabase = $objDatabase;
		$boolValidateGroupCount = $boolValidateGroupCount;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( self::EVENT_TRIGGER_TYPE_IMMEDIATE != trim( $this->getEventTriggerType() ) && self::EVENT_TRIGGER_TYPE_DAY_OF_EVENT != trim( $this->getEventTriggerType() ) ) {
					if( false == is_numeric( $this->getEventDays() ) || 0 >= $this->getEventDays() || true == is_float( $this->getEventDays() ) ) {
						$this->addErrorMsg( __( 'Enter positive numeric values only.' ) );
						$boolIsValid = false;
					}
				}

				if( true == valId( $this->getIsEnableManualContact() ) && false == valId( $this->getOverdueBusinessHours() ) ) {
						$this->addErrorMsg( __( 'Consider Overdue After must be a positive value.' ) );
						$boolIsValid = false;
				}
				break;

			default:
				// default action will go here
				break;
		}

		return $boolIsValid;
	}

	public function execute( $intCompanyUserId, $objDatabase, $objLeaseCustomer = NULL, $objEvent = NULL, $boolVerbose = false ) {

		$this->m_boolIsVerbose 	= $boolVerbose;

		$boolIsSkipMigrationMode = $this->getSystemMessageMetaData()['is_skip_migration_check'] ?? false;

		if( $this->m_boolIsVerbose ) {
			echo "\n -----------------------------------------------------------------------------------------------------------------";
			echo "\n" . 'Processing scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ] ';
			echo "\n -----------------------------------------------------------------------------------------------------------------";
		}

		// Check property product permission
		$arrintPermissionedPsProductIds	= [ CPsProduct::MESSAGE_CENTER, CPsProduct::LEASE_EXECUTION, CPsProduct::ENTRATA ];

		if( false == valId( $this->getPropertyId() ) ) {
			if( $this->m_boolIsVerbose ) {
				echo "\n -";
				echo "\n No property id found for this scheduled task so skipping task from processing.";
				echo "\n -";
			}

			return;
		}

		$boolIsSkipMigrationMode = $this->getSystemMessageMetaData()['skip_migration_check'] ?? false;
		if( true == \Psi\Eos\Entrata\CProperties::createService()->fetchIsPropertyMigratedByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase ) && false == $boolIsSkipMigrationMode ) {

			if( true == $this->m_boolIsVerbose ) {
				echo "\n ----------------------------------------------------------------------------------";
				echo "\n Property is in migration mode. So communication from contact points is disabled.";
				echo "\n ----------------------------------------------------------------------------------";
			}

			return;
		}

		$this->m_arrintPsProductIds 	= \Psi\Eos\Entrata\CPropertyProducts::createService()->fetchPsProductPermissionsByPsProductIdsByPropertyIdsByCid( $arrintPermissionedPsProductIds, [ $this->getPropertyId() ], $this->getCid(), $objDatabase );

		if( false == valArr( $this->m_arrintPsProductIds ) || ( false == array_key_exists( CPsProduct::MESSAGE_CENTER, $this->m_arrintPsProductIds ) && CEventSubType::RENEWAL_OFFER_EXPIRING_FOLLOW_UP != $this->getEventSubTypeId() && self::EVENT_TRIGGER_TYPE_BEFORE != $this->getEventTriggerType() ) ) {

			if( $this->m_boolIsVerbose ) {
				echo "\n -";
				echo "\n Neither message center has been enabled for property nor the scheduled task is of Renewal Offer Expiriing.";
				echo "\n -";
			}

			return;
		}

		if( false == valStr( $this->getComposeEmailId() ) || 1 != $this->getIsEnableEmail() ) {
			if( false == is_numeric( $this->getEventSubTypeId() ) && true == $this->m_boolIsVerbose && CEventType::NOT_PROGRESSING != $this->getEventTypeId() ) {
				echo "\n -";
				echo "\n Either system message id not found or email feature is disabled and manual contact is also disabled with scheduled task, so skipping task from processing.";
				echo "\n -";

				return;
			}
		}

		$intDaysInterval = 0;

		// For immediate scheduled tasks
		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) ) {
			$arrobjLeaseCustomers[] = $objLeaseCustomer;
		} else {

			if( self::EVENT_TRIGGER_TYPE_IMMEDIATE == $this->getEventTriggerType() ) {
				if( true == $this->m_boolIsVerbose ) {
					echo 'immediate';
				}

				return;
			}
			if( true == is_numeric( $this->getEventDays() ) ) {
				$intDaysInterval = ( self::EVENT_TRIGGER_TYPE_BEFORE == $this->getEventTriggerType() ) ? - $this->getEventDays() : $this->getEventDays();
			}

			$arrintEventSubTypeIds = [ CEventSubType::LEASE_GENERATED_FOLLOW_UP, CEventSubType::LEASE_COMPLETED_FOLLOW_UP, CEventSubType::LEASE_APPROVED_FOLLOW_UP, CEventSubType::TRANSFER_MOVE_IN, CEventSubType::TRANSFER_MOVE_OUT, CEventSubType::RENEWAL_OFFER_EXPIRING_FOLLOW_UP ];

			// Checking for the lease execution product permission if the lease generated, completed, approved scheduled task is triggere.
			if( true == in_array( $this->getEventSubTypeId(), $arrintEventSubTypeIds ) ) {
				if( false == array_key_exists( CPsProduct::LEASE_EXECUTION, $this->m_arrintPsProductIds ) && CEventSubType::RENEWAL_OFFER_EXPIRING_FOLLOW_UP != $this->getEventSubTypeId() ) {
					return;
				}

				if( false == array_key_exists( CPsProduct::LEASE_EXECUTION, $this->m_arrintPsProductIds ) && false == array_key_exists( CPsProduct::ENTRATA, $this->m_arrintPsProductIds ) && CEventSubType::RENEWAL_OFFER_EXPIRING_FOLLOW_UP == $this->getEventSubTypeId() ) {
					return;
				}
			}

			switch( $this->getEventTypeId() ) {

				case CEventType::MOVE_IN:
					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchMoveInLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					break;

				case CEventType::MOVE_OUT:
					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchMoveOutLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					break;

				case CEventType::RENEWAL_PROGRESS:
					if( self::EVENT_TRIGGER_TYPE_AFTER == $this->getEventTriggerType() ) {
						// @TODO Test it thoroughly in second phase refactoring
						$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchRenewalGeneratedLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					} else {
						// @TODO Test it thoroughly in second phase refactoring
						$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchRenewalExpiredLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					}
					break;

				case CEventType::RESIDENT_BIRTHDAY:
					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchBirthdayLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					break;

				case CEventType::RESIDENT_PAYMENT_DUE:
					break;

				case CEventType::LEASE_START:
					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseStartLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					break;

				case CEventType::LEASE_END:
					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseExpirationLeaseCustomersByPropretyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					break;

				case CEventType::LEASE_RENEWAL:
					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseRenewalLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					break;

				case CEventType::HOLIDAY:
					$objPropertyHoliday = CPropertyHolidays::fetchPropertyHolidayByIdByPropertyIdByCidWithCompanyHolidayDate( $this->getHolidayEventTypeId(), $this->getPropertyId(), $intDaysInterval, $this->getCid(), $objDatabase );

					if( false == valObj( $objPropertyHoliday, 'CPropertyHoliday' ) ) {
						return;
					}

					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchHolidayReminderLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
					break;

				case CEventType::SCHEDULED_FOLLOWUP:
					switch( $this->getEventSubTypeId() ) {

						case CEventSubType::LEASE_GENERATED_FOLLOW_UP:
							// @TODO Test it thoroughly in second phase refactoring
							$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchRenewalLeaseGeneratedLeaseCustomersByLeaseIntervalTypeIdByDaysIntervalByPropertyIdByCid( $this->getLeaseIntervalTypeId(), $intDaysInterval, $this->getPropertyId(), $this->getCid(), $objDatabase );
							break;

						case CEventSubType::LEASE_COMPLETED_FOLLOW_UP:
							// @TODO Test it thoroughly in second phase refactoring
							$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchRenewalLeaseCompletedLeaseCustomersByLeaseIntervalTypeIdByDaysIntervalByPropertyIdByCid( $this->getLeaseIntervalTypeId(), $intDaysInterval, $this->getPropertyId(), $this->getCid(), $objDatabase );
							break;

						case CEventSubType::LEASE_APPROVED_FOLLOW_UP:
							// @TODO Test it thoroughly in second phase refactoring
							$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchRenewalLeaseApprovedLeaseCustomersByLeaseIntervalTypeIdByDaysIntervalByPropertyIdByCid( $this->getLeaseIntervalTypeId(), $intDaysInterval, $this->getPropertyId(), $this->getCid(), $objDatabase );
							break;

						case CEventSubType::MAINTENANCE_FOLLOW_UP:
							// @TODO Test it thoroughly in second phase refactoring
							$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchMaintenenceRequestLeaseCustomersByPropertyIdByCidByMaintenenceStatusId( $this->getPropertyId(), $this->getCid(), $this->getMaintenanceStatusTypeId(), $intDaysInterval, $objDatabase );
							$arrobjLeaseCustomers = rekeyObjects( 'MaintenanceRequestId', $arrobjLeaseCustomers );
							break;

						case CEventSubType::TRANSFER_MOVE_IN:
							// @TODO Test it thoroughly in second phase refactoring
							$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchTransferMoveInByLeaseIntervalTypeIdByDaysIntervalByPropertyIdByCid( $this->getLeaseIntervalTypeId(), $intDaysInterval, $this->getPropertyId(), $this->getCid(), $objDatabase );
							break;

						case CEventSubType::TRANSFER_MOVE_OUT:
							// @TODO Test it thoroughly in second phase refactoring
							$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchTransferMoveOutByLeaseIntervalTypeIdByDaysIntervalByPropertyIdByCid( $this->getLeaseIntervalTypeId(), $intDaysInterval, $this->getPropertyId(), $this->getCid(), $objDatabase );
							break;

						case CEventSubType::SCHEDULED_APPOINTMENT_FOLLOW_UP:
							// @TODO Test it thoroughly in second phase refactoring
							$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchScheduledAppointmentLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
							break;

						default:
					}
					break;

				case CEventType::NOT_PROGRESSING:
					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchNotProgressingLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					break;

				case CEventType::DOCUMENT_EXPIRATION:
					// @TODO Test it thoroughly in second phase refactoring
					$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchDocumentExpiredLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $intDaysInterval, $objDatabase );
					break;

				case CEventType::CHECKLIST_ITEM_ACTIONS:
					if( CEventSubType::INCOMPLETE_MOVE_IN_CHECKLIST_ITEMS == $this->getEventSubTypeId() ) {
						$arrobjLeaseCustomers = ( array ) \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersWithIncompleteMoveInChecklistByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
					}
					break;

				case CEventType::SERVICES:
					switch( $this->getEventSubTypeId() ) {

						case CEventSubType::SERVICE:
							$arrobjLeaseCustomers = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchServiceLeaseCustomersByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $intDaysInterval );
							break;

						default:
					}
					break;

				default:
					$arrobjLeaseCustomers = [];
			}
		}

		if( true == valArr( $arrobjLeaseCustomers ) ) {
			$boolIsSendEmail = $this->getSystemMessageMetaData()['is_send_email'] ?? true;
			if( true == $boolIsSendEmail && ( true == CSystemMessage::IS_ENABLED_FOR_CONTACT_POINTS || true == in_array( $this->getCid(), CClient::$c_arrintSystemMessageConsumerClientIds ) ) && CEventType::ROOMMATE_NOTIFICATION_SENT != $this->getEventTypeId() ) {
				$this->processMessage( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase );
			} elseif( 1 == $this->getIsEnableEmail() && true == valId( $this->getComposeEmailId() ) ) {
				$boolIsEmailSent = $this->sendEmail( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase );

				if( false == $boolIsEmailSent && true == $this->m_boolIsVerbose ) {
					echo "\n -----------------------------------------------------------------------------------------------------------------";
					echo "\n" . 'Unable to send email for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
					echo "\n -----------------------------------------------------------------------------------------------------------------";
				}
			}

			if( 1 == $this->getIsEnableManualContact() && false == is_null( $this->getEventSubTypeId() ) ) {

				if( CEventSubType::RENEWAL_OFFER_EXPIRING_FOLLOW_UP == $this->getEventSubTypeId() ) {
					foreach( $arrobjLeaseCustomers as $objLeaseCustomers ) {
						if( CCustomerType::PRIMARY != $objLeaseCustomers->getCustomerTypeId() ) {
							unset( $arrobjLeaseCustomers[$objLeaseCustomers->getId()] );
						}
					}
				}

				if( false == $this->addFollowUp( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase, $objEvent ) && true == $this->m_boolIsVerbose ) {
					echo "\n" . 'No primary customer found for adding manual contact for schedule task[ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
				}
			}

			if( CEventType::NOT_PROGRESSING == $this->getEventTypeId() && false == $this->addNoProgressingAction( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase ) && true == $this->m_boolIsVerbose ) {
				echo "\n" . 'Failed to not progression contact for schedule task[ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
			}

			$arrobjLeaseCustomers = NULL;
			unset( $arrobjLeaseCustomers );
		}
	}

	public function processMessage( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase ) {
		if( 1 == $this->getIsEnableEmail() && true == valStr( $this->getSystemMessageKey() ) ) {
			$this->processMessageEmail( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase );
		}

		if( 1 == $this->getIsEnableSms() && true == valStr( $this->getSmsSystemMessageKey() ) ) {

			 if( false == array_key_exists( CPsProduct::MESSAGE_CENTER, $this->m_arrintPsProductIds ) || false == array_key_exists( 'ps_product_option_ids', $this->m_arrintPsProductIds[CPsProduct::MESSAGE_CENTER] )
			    || false == array_key_exists( CPsProductOption::SMS, $this->m_arrintPsProductIds[CPsProduct::MESSAGE_CENTER]['ps_product_option_ids'] ) || false == in_array( $this->getPropertyId(), $this->m_arrintPsProductIds[CPsProduct::MESSAGE_CENTER]['ps_product_option_ids'][CPsProductOption::SMS] ) ) {
				 if( $this->m_boolIsVerbose ) {
					 echo "\n -";
					 echo "\n" . ' SMS (Message Center Sub-product) is not assigned to property for schedule task[ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
					 echo "\n -";
				 }
				 return;
			 }

			$this->processMessageSms( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase );
		}
	}

	public function processMessageEmail( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase ) {

		$objSendSystemMessage = new \Psi\Libraries\SystemMessages\CSendSystemMessage();
		$objSendSystemMessage->setDatabase( $objDatabase );
		$objSendSystemMessage->setPropertyId( $this->getPropertyId() );
		$objSendSystemMessage->setCid( $this->getCid() );
		$objSendSystemMessage->setMetaData( 'system_email_type_id', CSystemEmailType::EVENT_SCHEDULER_EMAIL );
		$objSendSystemMessage->setKey( $this->getSystemMessageKey() );
		$objSendSystemMessage->setScheduledTaskId( $this->getId() );
		$objSendSystemMessage->setSystemMessageAudienceId( CSystemMessageAudience::RESIDENT );
		$objSendSystemMessage->setIsSkipMigrationModeCheck( $this->getSystemMessageMetaData()['skip_migration_check'] ?? false );

		if( true == valId( ( int ) $this->getSystemEmailTypeId() ) && false == $this->getIsSendEmailToResident() ) {
			return;
		}

		$arrmixSystemMessageMetaData = ( array ) $this->getSystemMessageMetaData();

		$arrobjEmailAttachments = ( array ) $this->createFileAttachments();

		foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
			$objSendSystemMessage->addEmailAttachment( $objEmailAttachment );
		}

		foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {

			$objSendQueueSystemMessage = clone $objSendSystemMessage;
			$objSendQueueSystemMessage->setMetaData( 'lease_customer_id', $objLeaseCustomer->getId() );
			$objSendQueueSystemMessage->setMetaData( 'lease_id', $objLeaseCustomer->getLeaseId() );
			$objSendQueueSystemMessage->setMetaData( 'lease_interval_id', $objLeaseCustomer->getLeaseIntervalId() );
			$objSendQueueSystemMessage->setMetaData( 'property_id', $objLeaseCustomer->getPropertyId() );
			$objSendQueueSystemMessage->setPreferredLocaleCode( $objLeaseCustomer->getPreferredLocaleCode() );
			$objSendQueueSystemMessage->setMetaData( 'application_id', $objLeaseCustomer->getApplicationId() );
			$objSendQueueSystemMessage->setIsSkipMigrationModeCheck( $this->getSystemMessageMetaData()['is_skip_migration_check'] ?? false );
			if( true == valId( $objLeaseCustomer->getMaintenanceRequestId() ) ) {
				$objSendQueueSystemMessage->setMetaData( 'maintenance_request_id', $objLeaseCustomer->getMaintenanceRequestId() );
			}
			if( true == valId( $objLeaseCustomer->getLeaseAssociationId() ) ) {
				$objSendQueueSystemMessage->setMetaData( 'lease_association_id', $objLeaseCustomer->getLeaseAssociationId() );
			}

			if( true == array_key_exists( 'application_id', $arrmixSystemMessageMetaData ) ) {
				$objSendQueueSystemMessage->setMetaData( 'application_id', $arrmixSystemMessageMetaData['application_id'] );
			}

			if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'register_event_callback_function' ) ) {
				foreach( $arrmixSystemMessageMetaData['register_event_callback_function'] as $strFunctionName => $mixValues ) {
					$objSendQueueSystemMessage->registerEventCallbackFunction( $strFunctionName, $mixValues );
				}
			}

			if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'lease_customers' ) ) {
				$objSendQueueSystemMessage->setMetaData( 'lease_customers', $arrmixSystemMessageMetaData['lease_customers'] );
			}

			switch( $this->getSystemEmailTypeId() ) {
				case CSystemEmailType::QUOTE_EMAIL:
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'applications' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'applications', $arrmixSystemMessageMetaData['applications'] );
					}
					$objSendQueueSystemMessage->setMetaData( 'application_id', $arrmixSystemMessageMetaData['application_id'] );
					break;

				case CSystemEmailType::LEASE_RENEWAL:
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'applications' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'applications', $arrmixSystemMessageMetaData['applications'] );
					}

					if( true == valArr( $arrmixSystemMessageMetaData ) ) {
						$objSendQueueSystemMessage->setMetaData( 'application_id', $arrmixSystemMessageMetaData['application_id'] ?? NULL );
					}

					if( CEventType::RENEWAL_PROGRESS == $this->getEventTypeId() && self::EVENT_TRIGGER_TYPE_BEFORE == $this->getEventTriggerType() ) {
						$objSendQueueSystemMessage->setMetaData( 'application_id', $objLeaseCustomer->getApplicationId() );
						$objSendQueueSystemMessage->registerEventCallbackFunction( 'addRenewalOfferSentEvent', [ $objLeaseCustomer->getApplicantId(), $objLeaseCustomer->getApplicationId() ] );
					}
					break;

				case CSystemEmailType::CONVENTIONAL_WAIT_LIST:
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'applications' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'applications', $arrmixSystemMessageMetaData['applications'] );
					}
					break;

				case CSystemEmailType::PARCEL_ALERT:
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'lease_customers' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'lease_customers', $arrmixSystemMessageMetaData['lease_customers'] );
					}
					break;

				case CSystemEmailType::CONTACT_SUBMISSIONS:
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'leases' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'leases', $arrmixSystemMessageMetaData['leases'] );
					}
					break;

				case CSystemEmailType::AR_PAYMENT_NOTIFICATION:
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'ar_payment_id' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'ar_payment_id', $arrmixSystemMessageMetaData['ar_payment_id'] );
					}

					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'ar_payments' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'ar_payments', $arrmixSystemMessageMetaData['ar_payments'] );
					}

					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'properties' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'properties', $arrmixSystemMessageMetaData['properties'] );
					}
					break;

				case CSystemEmailType::CUSTOMER_PROFILE_UPDATE:
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'lease_customers' ) ) {
						$arrmixMetaData = [
							'is_required_new_version'    => $arrmixSystemMessageMetaData['lease_customers']['is_required_new_version'] ?? NULL,
							'temporary_password'         => $arrmixSystemMessageMetaData['lease_customers']['temporary_password'] ?? NULL,
							'is_tenant'                  => $arrmixSystemMessageMetaData['lease_customers']['is_tenant'] ?? NULL,
							'system_email_id'            => $arrmixSystemMessageMetaData['lease_customers']['system_email_id'] ?? NULL,
							'customer_password_link'     => $arrmixSystemMessageMetaData['lease_customers']['customer_password_link'] ?? NULL,
							'is_send_to_group'           => $arrmixSystemMessageMetaData['lease_customers']['is_send_to_group'] ?? NULL
						];
						$objSendQueueSystemMessage->setMetaData( 'lease_customers', $arrmixMetaData );
					}
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'properties' ) ) {
						$arrmixMetaData = [
							'is_required_new_version'    => $arrmixSystemMessageMetaData['properties']['is_required_new_version'] ?? NULL,
							'is_required_address'        => $arrmixSystemMessageMetaData['properties']['is_required_address'] ?? NULL,
							'is_customer_password_reset' => $arrmixSystemMessageMetaData['properties']['is_customer_password_reset'] ?? NULL,
							'is_tenant'                  => $arrmixSystemMessageMetaData['properties']['is_tenant'] ?? NULL,
							'is_send_to_group'           => $arrmixSystemMessageMetaData['properties']['is_send_to_group'] ?? NULL
						];
						$objSendQueueSystemMessage->setMetaData( 'properties', $arrmixMetaData );
					}
					break;

				case CSystemEmailType::EVENT_SCHEDULER_EMAIL:
					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'lease_association_id' ) ) {
						$objSendQueueSystemMessage->setMetaData( 'lease_association_id', $arrmixSystemMessageMetaData['lease_association_id'] );
					}

					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'add_on_id' ) && true == valId( $arrmixSystemMessageMetaData['add_on_id'] ) ) {
						$objSendQueueSystemMessage->setMetaData( 'add_on_id', $arrmixSystemMessageMetaData['add_on_id'] );
					}

					if( true == valArrKeyExists( $arrmixSystemMessageMetaData, 'calendar_event_id' ) && true == valId( $arrmixSystemMessageMetaData['calendar_event_id'] ) ) {
						$objSendQueueSystemMessage->setMetaData( 'calendar_event_id', $arrmixSystemMessageMetaData['calendar_event_id'] );
					}
					break;

				default:
			}

			$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchSimpleClientById( $this->getCid(), $objDatabase );

			if( true == valObj( $objClient, CClient::class ) && CCompanyStatusType::CLIENT != $objClient->getCompanyStatusTypeId() ) {
				$objSendQueueSystemMessage->setSystemMessagePriority( CSystemMessage::SYSTEM_MESSAGE_QUEUE_TEST_CLIENT_PRIORITY );
			} else if( CEventTriggerScheduleType::IMMEDIATE_VALUE == $this->getEventTriggerType() ) {
				$objSendQueueSystemMessage->setSystemMessagePriority( CSystemMessage::SYSTEM_MESSAGE_QUEUE_IMMEDIATE_PRIORITY );
			}

			if( false == $objSendQueueSystemMessage->execute( $intCompanyUserId ) && true == $this->m_boolIsVerbose ) {
				echo "\n ----------------------------------------------------------------------------------------------------------------------";
				echo "\n" . 'Unable to process system message for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
				echo "\n ----------------------------------------------------------------------------------------------------------------------";
			} elseif( true == $this->m_boolIsVerbose ) {
					echo "\n ----------------------------------------------------------------------------------------------------------------------------";
					echo "\n" . 'System Message process successfully for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getId() . ' ]';
					echo "\n ----------------------------------------------------------------------------------------------------------------------------";
			}
		}

		if( true == $this->removeFileAttachments( $intCompanyUserId ) && true == $this->m_boolIsVerbose ) {
			echo "\n ----------------------------------------------------------------------------------------------------------------------------";
			echo "\n" . 'Temporary file attachments are removed.';
			echo "\n ----------------------------------------------------------------------------------------------------------------------------";
		}
	}

	public function processMessageSms( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase ) {
		$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $this->getCid(), $objDatabase );
		if( false == valObj( $objClient, 'CClient' ) ) {
			return false;
		}

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return false;
		}
		$objComanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $this->getCid(), $objDatabase );

		if( false == valObj( $objComanyUser, 'CCompanyUser' ) ) {
			if( $this->m_boolIsVerbose ) {
				echo "\n -----------------------------------------------------------------------------------------------------------------";
				echo "\n" . 'Company User is not available. for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
				echo "\n -----------------------------------------------------------------------------------------------------------------";
			}

			return;
		}

		$objCompanyEmployee = $objComanyUser->fetchCompanyEmployee( $objDatabase );

		$arrobjLeaseCustomers = array_filter( $arrobjLeaseCustomers );
		$arrintCustomerIds = [];
		foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
			$arrintCustomerIds[$objLeaseCustomer->getCustomerId()] = $objLeaseCustomer->getCustomerId();
		}

		$arrobjCustomers	= \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIdsByCid( $arrintCustomerIds, $this->getCid(), $objDatabase );

		$objSystemMessageLibrary = new CSystemMessageLibrary();
		$objSystemMessageLibrary->setDatabase( $objDatabase );
		$objSystemMessageLibrary->setSystemMessageKey( $this->getSmsSystemMessageKey() );
		$objSystemMessageLibrary->setPropertyId( $this->getPropertyId() );
		$objSystemMessageLibrary->setClientId( $this->getCid() );
		$objSystemMessageLibrary->setSystemMessageAudienceId( CSystemMessageAudience::RESIDENT );

		foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
			$objCustomer = getArrayElementByKey( $objLeaseCustomer->getCustomerId(), $arrobjCustomers );

			try {
				CLocaleContainer::createService()->setPreferredLocaleCode( $objCustomer->getPreferredLocaleCode(), $objDatabase, $objProperty );
			} catch( Exception $objException ) {
				CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
			}

			$objSystemMessage = $objSystemMessageLibrary->getSmsSystemMessage( CLocaleContainer::createService()->getLocaleCode(), CLocaleContainer::createService()->getDefaultLocaleCode() );

			CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );

			if( false == valObj( $objSystemMessage, 'CSystemMessage' ) ) {
				if( $this->m_boolIsVerbose ) {
					echo "\n ----------------------------------------------------------------------------------------------------------------------";
					echo "\n" . 'Unable to process SMS system message for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
					echo "\n ----------------------------------------------------------------------------------------------------------------------";
				}
				return;
			}

			$objSystemMessageTemplateCustomText = \Psi\Eos\Entrata\CSystemMessageTemplateCustomTexts::createService()->fetchSimpleSystemMessageTemplateCustomTextBySystemMessageIdByKeyByCid( $objSystemMessage->getId(), 'MAIN_BODY_CONTENT', $this->getCid(), $objDatabase );
			if( false == valObj( $objSystemMessageTemplateCustomText, 'CSystemMessageTemplateCustomText' ) ) {
				if( $this->m_boolIsVerbose ) {
					echo "\n -----------------------------------------------------------------------------------------------------------------";
					echo "\n" . 'Text Message is not available. for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
					echo "\n -----------------------------------------------------------------------------------------------------------------";
				}
				return;
			}

			if( false == valStr( $objSystemMessageTemplateCustomText->getValue() ) ) {
				if( $this->m_boolIsVerbose ) {
					echo "\n -----------------------------------------------------------------------------------------------------------------";
					echo "\n" . 'Text Message is not available. for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
					echo "\n -----------------------------------------------------------------------------------------------------------------";
				}
				return;
			}

			$strMobileNumber = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomerPhoneNumberInfoByIdByPhoneNumberTypeIdByCid( $objCustomer->getId(), CPhoneNumberType::MOBILE, $this->getCid(), $objDatabase );

			$objContactPointSms   = $objCustomer->fetchSmsEnrollmentByMessageTypeId( CMessageType::PROPERTY_NOTIFICATION, $objDatabase );

			if( CMessageType::PACKAGE_NOTIFICATION == $this->getMessageTypeId() ) {
				$objParcelAlertSms   = $objCustomer->fetchSmsEnrollmentByMessageTypeId( CMessageType::PACKAGE_NOTIFICATION, $objDatabase );
				$boolIsBlockParcelAlertSms = !( true == valObj( $objParcelAlertSms, 'CSmsEnrollment' ) && false == valStr( $objParcelAlertSms->getDisabledOn() ) );
				if( true == $boolIsBlockParcelAlertSms || false == valStr( $strMobileNumber ) ) {
					continue;
				}
			}

			$boolIsBlockContactPointsSms = !( true == valObj( $objContactPointSms, 'CSmsEnrollment' ) && false == valStr( $objContactPointSms->getDisabledOn() ) );

			if( true == $boolIsBlockContactPointsSms || false == valStr( $strMobileNumber ) ) {
				continue;
			}

			$objCustomer->setPropertyId( $this->getPropertyId() );
			$objCustomer->setLeaseId( $objLeaseCustomer->getLeaseId() );
			$objCustomer->setLeaseIntervalId( $objLeaseCustomer->getLeaseIntervalId() );

			$objMessage = new CMessage();
			$objMessage->setDefaults();
			$objMessage->setCid( $objCustomer->getCid() );
			$objMessage->setPhoneNumber( $strMobileNumber );
			$objMessage->setCustomerId( $objCustomer->getId() );

			$objMessage->setScheduledSendDatetime( 'NOW()' );

			if( self::EVENT_TRIGGER_TYPE_IMMEDIATE != trim( $this->getEventTriggerType() ) ) {
				$objDateTime = new DateTime( date( ' Y-m-d' ) );
				$objDateTime->setTime( 9, 0 );
				if( new DateTime( date( ' Y-m-d  H:i:s' ) ) > $objDateTime->format( 'Y-m-d H:i:s' ) ) {
					$objDateTime->add( new DateInterval( 'P1D' ) );
				}
				$objMessage->setScheduledSendDatetime( $objDateTime->format( 'Y-m-d H:i:s' ) );
			}

			$strContent = preg_replace( '/[\r]/', '', trim( $objSystemMessageTemplateCustomText->getValue() ) );

			$objSmsContentLibrary = new CSmsContentLibrary();
			$objSmsContentLibrary->setClient( $objClient );
			$objSmsContentLibrary->setClientDatabase( $objDatabase );
			$arrmixData = [ 'lease_id' => $objLeaseCustomer->getLeaseId(), 'lease_customer_id' => $objLeaseCustomer->getId(), 'property_id' => $this->getPropertyId() ];

			$objSmsContentLibrary->setRequiredInputData( $arrmixData );
			$strContent = $objSmsContentLibrary->replaceMergeFields( $strContent );

			$objMessage->setMessage( $strContent );
			$objMessage->appendReplyStopMessage( $objSystemMessage->getLocaleCode() );
			$objMessage->setPropertyId( $objLeaseCustomer->getPropertyId() );
			$objMessage->setMessageTypeId( ( valId( $this->getMessageTypeId() ) ) ? $this->getMessageTypeId() : CMessageType::CONTACT_POINT );

			$objEventLibrary 	= new CEventLibrary();

			$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
			$objEventLibraryDataObject->setDatabase( $objDatabase );
			$objEventLibraryDataObject->setCustomer( $objCustomer );

			$objEvent = $objEventLibrary->createEvent( [ $objCustomer ], CEventType::SMS_OUTGOING );
			$objEvent->setCid( $this->getCid() );
			$objEvent->setCompanyUser( $objComanyUser );
			$objEvent->setEventDatetime( 'NOW()' );
			$objEvent->setNotes( $objMessage->getMessage() );
			$objEvent->setReference( $objMessage );
			$objEvent->setScheduledTaskId( $this->getId() );
			$objEventLibrary->buildEventDescription( $objCustomer );

			if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
				$objEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
				$objEvent->setCompanyEmployee( $objCompanyEmployee );
			}

			if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
				if( $this->m_boolIsVerbose ) {
					echo "\n -----------------------------------------------------------------------------------------------------------------";
					echo "\n" . 'Unable to create manual contact for SMS for scheduled task [ID: ' . $this->getId() . ', Lease Customer ID: ' . $objLeaseCustomer->getId() . ' ]';
					echo "\n -----------------------------------------------------------------------------------------------------------------";
				}
				continue;
			}

			$objMessage->setEventId( $objEvent->getId() );

			if( false == $objMessage->insert( $intCompanyUserId, NULL, false, false ) ) {
				if( $this->m_boolIsVerbose ) {
					echo "\n -----------------------------------------------------------------------------------------------------------------";
					echo "\n" . 'Unable send SMS for scheduled task [ID: ' . $this->getId() . ', Lease Customer ID: ' . $objLeaseCustomer->getId() . ' ]';
					echo "\n -----------------------------------------------------------------------------------------------------------------";
				}
				continue;
			}

			$objEventLibrary = NULL;
			$objEvent        = NULL;
		}

	}

	public function sendEmail( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase, $boolIsFromPrintOrDownload = false ) {
		$strToEmailAddress 		= CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;

		$objScheduledTaskEmail = CScheduledTaskEmails::fetchScheduledTaskEmailByIdByCid( $this->getComposeEmailId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objScheduledTaskEmail, 'CScheduledTaskEmail' ) ) {
			if( $this->m_boolIsVerbose ) {
				echo "\n -----------------------------------------------------------------------------------------------------------------";
				echo "\n" . 'Unable to load scheduled task email object for scheduled task [ID: ' . $this->getId() . ', Property ID: ' . $this->getPropertyId() . ' ]';
				echo "\n -----------------------------------------------------------------------------------------------------------------";
			}

			return false;
		}

		$strFromEmailAddress = $objScheduledTaskEmail->getFromEmailAddress();
		$strReplyToEmailAddress	= NULL;

		if( false == valStr( $strFromEmailAddress ) ) {
			$strFromEmailAddress = CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;

			$objPropertyPreferenceEmailRelay 	= \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'EVENT_SCHEDULER_EMAIL_RELAY', $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objPropertyPreferenceEmailRelay, 'CPropertyPreference' ) && 1 == $objPropertyPreferenceEmailRelay->getValue() ) {
				$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
				$strPropertyName = convertNonAsciiCharactersToAscii( $objProperty->getPropertyName() );

				$strFromEmailAddress = \Psi\CStringService::singleton()->strtolower( getAlfaNumericValue( $strPropertyName ) ) . '@' . CConfig::get( 'email_relay_domain' );
				$strReplyToEmailAddress = 'Message-ID: ' . \Psi\CStringService::singleton()->strtolower( getAlfaNumericValue( $strPropertyName ) ) . '-' . $objProperty->getId() . '-LC-CU' . $intCompanyUserId . '@' . CConfig::get( 'email_relay_domain' );

			} else {
				$objPropertyPreferenceDefaultEmail 	= \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'FROM_EVENT_SCHEDULER_DEFAULT_EMAIL', $this->getPropertyId(), $this->getCid(), $objDatabase );

				if( true == valObj( $objPropertyPreferenceDefaultEmail, 'CPropertyPreference' ) && true == CValidation::validateEmailAddresses( $objPropertyPreferenceDefaultEmail->getValue() ) ) {
					$strFromEmailAddress = convertNonAsciiCharactersToAscii( $objPropertyPreferenceDefaultEmail->getValue() );
				}
			}
		}

		$strSubject = $objScheduledTaskEmail->getSubject();

		// Get content with property merge field replaced
		$strHtmlContent = $objScheduledTaskEmail->getEmailContentByPropertyId( $objDatabase );

		// Below objects are required for events
		$objComanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $this->getCreatedBy(), $this->getCid(), $objDatabase );

		$objComanyEmployee = NULL;
		if( true == is_numeric( $objComanyUser->getCompanyEmployeeId() ) ) {
			$objComanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $objComanyUser->getId(), $this->getCid(), $objDatabase );
		}
		$arrobjLeaseCustomers = array_filter( $arrobjLeaseCustomers );
		$arrintCustomerIds = [];

		foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
			$arrintCustomerIds[$objLeaseCustomer->getCustomerId()] = $objLeaseCustomer->getCustomerId();
		}

		$arrobjCustomers	= \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIdsByCid( $arrintCustomerIds, $this->getCid(), $objDatabase );
		$objSystemEmail		= NULL;
		$objScheduledTaskEmail->loadLeaseCustomerMergeFields( $arrobjLeaseCustomers, $this->getCid(), $strSubject, $objScheduledTaskEmail->getCreatedBy(), $objDatabase );

		foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
			$arrmixSubjectMergeFields = ( array ) $objLeaseCustomer->getMergeFields();

			$objScheduledTaskEmail->loadEmailSubjectMergeFields( $strSubject, $this->getCid(), $objDatabase, $arrmixSubjectMergeFields, $objScheduledTaskEmail );
		}

		$objScheduledTaskEmail->loadLeaseCustomerMergeFields( $arrobjLeaseCustomers, $this->getCid(), $strHtmlContent, $objScheduledTaskEmail->getCreatedBy(), $objDatabase );

		$arrmixCustomersEmailHtmlContent = [];
		if( true == $boolIsFromPrintOrDownload ) {
			foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
				$arrmixCustomersEmailHtmlContent[$objLeaseCustomer->getLeaseId()] = self::processMergeFieldsInEmailContent( $strHtmlContent, $objLeaseCustomer->getMergeFields() );
			}
			return $arrmixCustomersEmailHtmlContent;
		}

		if( false == valObj( $this->loadEmailDatabase(), 'CDatabase' ) ) {
			return false;
		}

		$this->m_objEmailDatabase->open();

		$arrobjSystemEmailBlocks = ( array ) CSystemEmailBlocks::fetchSystemEmailBlocksByCidByEmailBlockTypeId( $this->getCid(), CEmailBlockType::EVENT_SCHEDULING_EMAIL, $this->m_objEmailDatabase );

		if( true == valArr( $arrobjSystemEmailBlocks ) ) {
			$arrobjSystemEmailBlocks = rekeyObjects( 'EmailAddress', $arrobjSystemEmailBlocks );
		}

		$intCount 				= 0;
		$intTotalLeaseCustomers = \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds );
		$intScheduledTaskLogId 	= CScheduledTaskLogs::fetchRecentScheduledTaskLogIdByScheduledTaskIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$arrintAmsiIntegratedPropetyIds = \Psi\Eos\Entrata\CProperties::createService()->fetchIntegratedPropertyIdsByIdsByIntegrationClientTypeIdByCid( [ $this->getPropertyId() ], CIntegrationClientType::AMSI, $this->getCid(), $objDatabase );
		$boolIsAmsiIntegratedProperty   = true == in_array( $this->getPropertyId(), $arrintAmsiIntegratedPropetyIds );
		$boolIsNotWorkOrder             = ( $this->getEventTypeId() != CEventType::SCHEDULED_FOLLOWUP || $this->getEventSubTypeId() != CEventSubType::MAINTENANCE_FOLLOW_UP );

		foreach( $arrobjLeaseCustomers as $intKey => $objLeaseCustomer ) {
			$arrmixMergeFields 		= [];

			if( false == valStr( $objLeaseCustomer->getCustomerEmailAddress() ) || true == array_key_exists( $objLeaseCustomer->getCustomerEmailAddress(), $arrobjSystemEmailBlocks ) ) {
				continue;
			}

			if( 0 == $intCount ) {
				// If event is Not Work Order then we need only one system email for all customer.
				if( true == $boolIsNotWorkOrder ) {
					$objSystemEmail = $this->createSystemEmail( $objScheduledTaskEmail, $strHtmlContent, $strToEmailAddress, $strFromEmailAddress, $intCompanyUserId, $intScheduledTaskLogId, $strReplyToEmailAddress );

					if( false == valObj( $objSystemEmail, 'CSystemEmail' ) ) {

						if( CEventType::ROOMMATE_NOTIFICATION_SENT == $this->getEventTypeId() && self::EVENT_TRIGGER_TYPE_IMMEDIATE == $this->getEventTriggerType() ) {
							$this->m_objEmailDatabase->close();
							return false;
						}

						continue;
					}
				}
			}

			// If event is Work Order then we need to create new system email for every workorder.
			if( $this->getEventTypeId() == CEventType::SCHEDULED_FOLLOWUP && $this->getEventSubTypeId() == CEventSubType::MAINTENANCE_FOLLOW_UP ) {
				$objSystemEmail = $this->createSystemEmail( $objScheduledTaskEmail, $strHtmlContent, $strToEmailAddress, $strFromEmailAddress, $intCompanyUserId, $intScheduledTaskLogId, $strReplyToEmailAddress );
			}

			if( false == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
				continue;
			}

			$arrmixMergeFields = ( array ) $objLeaseCustomer->getMergeFields();

			$objSystemEmailRecipient = new CSystemEmailRecipient();
			$objSystemEmailRecipient->setId( $objSystemEmailRecipient->fetchNextId( $this->m_objEmailDatabase ) );
			$objSystemEmailRecipient->setCid( $this->getCid() );
			$objSystemEmailRecipient->setRecipientTypeId( CSystemEmailRecipientType::RESIDENT );
			$objSystemEmailRecipient->setReferenceNumber( $objLeaseCustomer->getCustomerId() );
			$objSystemEmailRecipient->setEmailAddress( $objLeaseCustomer->getCustomerEmailAddress() );

			$objSystemEmailRecipient->processViewInBrowser( $arrmixMergeFields );

			if( true == valArr( $arrmixMergeFields ) ) {
				$objSystemEmailRecipient->setMergeFields( serialize( $arrmixMergeFields ) );
			}

			$objSystemEmail->addSystemEmailRecipient( $objSystemEmailRecipient );

			// Adding this condition to avoid multiple attachments in case of ROOMMATE_NOTIFICATION_SENT
			if( 0 == $intCount || $this->getEventTypeId() != CEventType::ROOMMATE_NOTIFICATION_SENT ) {
				$this->insertEmailAttachments( $objSystemEmail, $objScheduledTaskEmail, $intCompanyUserId, $objDatabase );
			}

			if( false == $boolIsNotWorkOrder || ( true == $boolIsNotWorkOrder && $intCount == $intTotalLeaseCustomers - 1 ) ) {
				if( false == $objSystemEmail->validate( VALIDATE_INSERT ) || false == $objSystemEmail->insert( $intCompanyUserId, $this->m_objEmailDatabase ) ) {
					if( CEventType::ROOMMATE_NOTIFICATION_SENT == $this->getEventTypeId() && self::EVENT_TRIGGER_TYPE_IMMEDIATE == $this->getEventTriggerType() ) {
						$this->addErrorMsg( $objSystemEmail->getErrorMsgs() );
					}
					continue;
				}
			}

			$objCustomer = getArrayElementByKey( $objLeaseCustomer->getCustomerId(), $arrobjCustomers );

			if( true == valObj( $objCustomer, 'CCustomer' ) ) {

				$objCustomer->setPropertyId( $this->getPropertyId() );
				$objCustomer->setLeaseId( $objLeaseCustomer->getLeaseId() );
				$objCustomer->setLeaseIntervalId( $objLeaseCustomer->getLeaseIntervalId() );

				$boolIsPrimary = CCustomerType::PRIMARY == $objLeaseCustomer->getCustomerTypeId();

				$this->addEvent( $objSystemEmail, $objComanyUser, $objComanyEmployee, $objCustomer, $objDatabase, $boolIsAmsiIntegratedProperty, $boolIsPrimary, $objLeaseCustomer->getUnitSpaceId() );
			}

			$arrstrEmailCcAddresses  = [];
			$arrstrEmailBccAddresses = [];

			if( true == valStr( $objScheduledTaskEmail->getBccEmailAddress() ) ) {
				$arrstrEmailBccAddresses = explode( ',', $objScheduledTaskEmail->getBccEmailAddress() );
			}

			if( true == valStr( $objScheduledTaskEmail->getCcEmailAddress() ) ) {
				$arrstrEmailCcAddresses = explode( ',', $objScheduledTaskEmail->getCcEmailAddress() );
			}

			if( true == valArr( $arrstrEmailCcAddresses ) ) {
				foreach( $arrstrEmailCcAddresses as $strCcEmailAddress ) {
					$this->insertCcBccEmail( $objSystemEmail, $objScheduledTaskEmail, $arrmixMergeFields, $strCcEmailAddress, $intCompanyUserId, $objDatabase );
				}
			}

			if( true == valArr( $arrstrEmailBccAddresses ) ) {
				foreach( $arrstrEmailBccAddresses as $strBccEmailAddress ) {
					$this->insertCcBccEmail( $objSystemEmail, $objScheduledTaskEmail, $arrmixMergeFields, $strBccEmailAddress, $intCompanyUserId, $objDatabase );
				}
			}

			$intCount++;
		}

		$this->m_objEmailDatabase->close();

		$objScheduledTaskEmail  = NULL;
		$strHtmlContent         = NULL;
		$arrobjCustomers        = NULL;

		unset( $strHtmlContent, $arrobjCustomers, $objScheduledTaskEmail );

		if( ( $this->m_boolIsVerbose ) && true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
			echo "\n -----------------------------------------------------------------------------------------------------------------";
			echo "\n" . 'New system email inserted for scheduled task [ID: ' . $this->getId() . ', System Email ID: ' . $objSystemEmail->getId() . ' ]';
			echo "\n -----------------------------------------------------------------------------------------------------------------";
		}

		return true;
	}

	public function addFollowUp( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase, $objTriggeringEvent = NULL ) {
		if( true == valArr( $arrobjLeaseCustomers ) ) {
			foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {

				$objEventLibrary = new CEventLibrary();

				$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
				$objEventLibraryDataObject->setDatabase( $objDatabase );
				$objEventLibraryDataObject->setLeaseCustomer( $objLeaseCustomer );

				$objEvent = $objEventLibrary->createEvent( [ $objLeaseCustomer ], $this->getEventTypeId() );
				$objEvent->setCid( $this->getCid() );
				$objEvent->setPropertyId( $this->getPropertyId() );
				$objEvent->setPsProductId( CPsProduct::MESSAGE_CENTER );
				$objEvent->setEventDatetime( 'NOW()' );
				$objEvent->setScheduledDatetime( 'NOW()' );
				$objEvent->setReference( $objLeaseCustomer );
				$objEvent->setCustomerId( $objLeaseCustomer->getCustomerId() );
				$objEvent->setLeaseId( $objLeaseCustomer->getLeaseId() );
				$objEvent->setLeaseIntervalId( $objLeaseCustomer->getLeaseIntervalId() );
				$objEvent->setEventSubTypeId( $this->getEventSubTypeId() );
				$strManualContactInstructions = $this->getDetailsField( [ '_translated', $objLeaseCustomer->getPreferredLocaleCode(), 'manual_contact_instructions' ] );

				if( false == valStr( $strManualContactInstructions ) ) {
					$strManualContactInstructions = $this->getDetailsField( [ '_translated', CLocale::DEFAULT_LOCALE, 'manual_contact_instructions' ] );
				}

				$objEvent->setNotes( $strManualContactInstructions );

				$objEvent->setScheduledTaskId( $this->getId() );

				if( $this->getOverdueBusinessHours() > 0 ) {
					$arrstrEventDetails['overdue_on'] = $this->getOverdueDateTimeByPropertyId( $objDatabase );
				}

				if( true == valObj( $objTriggeringEvent, CEvent::class ) ) {
					$arrstrEventDetails['auto_creation']['triggering_event_id'] = $objTriggeringEvent->getId();
				}

				$arrstrEventDetails['auto_creation']['triggering_task_id'] = $this->getId();

				$objEvent->bindDetails( $arrstrEventDetails );

				if( true == valId( $objLeaseCustomer->getMaintenanceRequestId() ) ) {
					$objEvent->setDataReferenceId( $objLeaseCustomer->getMaintenanceRequestId() );
				}

				$objEventLibrary->buildEventDescription( $objLeaseCustomer );

				if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
					if( $this->m_boolIsVerbose ) {
						echo "\n -----------------------------------------------------------------------------------------------------------------";
						echo "\n" . 'Unable to create manual contact for scheduled task [ID: ' . $this->getId() . ', Lease Customer ID: ' . $objLeaseCustomer->getId() . ' ]';
						echo "\n -----------------------------------------------------------------------------------------------------------------";
					}
					continue;
				}

				$objEventLibrary = NULL;
				$objEvent        = NULL;

				unset( $objEventLibrary, $objEvent );
			}

			return true;
		}

		return false;
	}

	private function addNoProgressingAction( $arrobjLeaseCustomers, $intCompanyUserId, $objDatabase ) {
		if( true == valArr( $arrobjLeaseCustomers ) ) {
			foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
				$objEventLibrary = new CEventLibrary();

				$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
				$objEventLibraryDataObject->setDatabase( $objDatabase );
				$objEventLibraryDataObject->setLeaseCustomer( $objLeaseCustomer );

				$objEvent = $objEventLibrary->createEvent( [ $objLeaseCustomer ], CEventType::NOT_PROGRESSING );
				$objEvent->setCid( $objLeaseCustomer->getCid() );
				$objEvent->setPropertyId( $objLeaseCustomer->getPropertyId() );
				$objEvent->setPsProductId( CPsProduct::MESSAGE_CENTER );
				$objEvent->setLeaseIntervalId( $objLeaseCustomer->getLeaseIntervalId() );
				$objEvent->setCustomerId( $objLeaseCustomer->getPrimaryCustomerId() );
				$objEvent->setEventDatetime( 'NOW()' );
				$objEvent->setScheduledDatetime( 'NOW()' );
				$objEvent->setReference( $objLeaseCustomer );
				$objEvent->setCreatedBy( $intCompanyUserId );
				$objEvent->setDoNotExport( true );

				if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
					if( $this->m_boolIsVerbose ) {
						echo "\n -----------------------------------------------------------------------------------------------------------------";
						echo "\n" . 'Unable to not progressing event for scheduled task [ID: ' . $this->getId() . ', Lease Customer ID: ' . $objLeaseCustomer->getId() . ' ]';
						echo "\n -----------------------------------------------------------------------------------------------------------------";
					}
					continue;
				}

				$objEventLibrary = NULL;
				$objEvent        = NULL;

				unset( $objEventLibrary, $objEvent );
			}
			return true;
		}

		return false;
	}

	public function createSystemEmail( $objScheduledTaskEmail, $strHtmlContent, $strToEmailAddress, $strFromEmailAddress, $intCompanyUserId, $intScheduledTaskLogId = NULL, $strReplyToEmailAddress = NULL ) {

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setId( $objSystemEmail->fetchNextId( $this->m_objEmailDatabase ) );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::EVENT_SCHEDULER_EMAIL );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setPropertyId( $this->getPropertyId() );
		$objSystemEmail->setFromEmailAddress( $strFromEmailAddress );
		$objSystemEmail->setReplyToEmailAddress( $strReplyToEmailAddress );
		$objSystemEmail->setToEmailAddress( $strToEmailAddress );
		$objSystemEmail->setSubject( $objScheduledTaskEmail->getSubject() );
		$objSystemEmail->setHtmlContent( $strHtmlContent );
		$objSystemEmail->setScheduledSendDatetime( date( 'm/d/Y' ) );
		$objSystemEmail->setScheduledTaskLogId( $intScheduledTaskLogId );

		return $objSystemEmail;
	}

	public function insertEmailAttachments( $objSystemEmail, $objScheduledTaskEmail, $intCompanyUserId, $objDatabase ) {

		$arrobjFiles = ( array ) CFiles::fetchFilesByScheduledTaskEmailIdByFileTypeSystemCodeByCid( $objScheduledTaskEmail->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $objDatabase );

		if( false == valArr( $arrobjFiles ) ) {
			return;
		}

		foreach( $arrobjFiles as $objFile ) {

			$objEmailAttachment	= new CEmailAttachment();
			$objEmailAttachment->setTitle( $objFile->getTitle() );
			$objEmailAttachment->setFileName( $objFile->getFileName() );
			$objEmailAttachment->setFilePath( $objFile->getFullFilePath() );
			$objSystemEmail->addEmailAttachment( $objEmailAttachment );
		}
	}

	public function addEvent( $objSystemEmail, $objComanyUser, $objComanyEmployee, $objCustomer, $objDatabase, $boolIsAmsiIntegratedProperty = false, $boolIsPrimary = true, $intUnitSpaceId = NULL ) {

		// Do not export event for AMSI integrated properties unless it is Primary
		$boolIsDoNotExport = ( true == $boolIsAmsiIntegratedProperty && false == $boolIsPrimary );

		$objEventLibrary = new CEventLibrary();

		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setCustomer( $objCustomer );

		$objEvent = $objEventLibrary->createEvent( [ $objCustomer ], CEventType::EMAIL_OUTGOING );
		$objEvent->setCid( $objCustomer->getCid() );
		$objEvent->setCompanyUser( $objComanyUser );
		$objEvent->setPsProductId( CPsProduct::MESSAGE_CENTER );

		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setNotes( $objSystemEmail->getSubject() );

		if( CEventType::ROOMMATE_NOTIFICATION_SENT == $this->getEventTypeId() ) {
			$objEvent->setEventTypeId( $this->getEventTypeId() );
			$objEvent->setEventSubTypeId( $this->getEventSubTypeId() );
			$objEvent->setUnitSpaceId( $intUnitSpaceId );
		}

		if( true == valObj( $objComanyEmployee, 'CCompanyEmployee' ) ) {
			$objEvent->setCompanyEmployeeId( $objComanyEmployee->getId() );
			$objEvent->setCompanyEmployee( $objComanyEmployee );
		}

		$objEvent->setDataReferenceId( $objSystemEmail->getId() );
		$objEvent->setReference( $objSystemEmail );
		$objEvent->setDoNotExport( $boolIsDoNotExport );

		$objEventLibrary->buildEventDescription( $objCustomer );

		$objEvent->setDoNotExport( $boolIsDoNotExport );

		$objEventLibrary->insertEvent( $objComanyUser->getId(), $objDatabase );

		$objEvent = NULL;
		$objEventLibrary = NULL;

		unset( $objEvent, $objEventLibrary );
	}

	public static function processMergeFieldsInEmailContent( $strHtmlContent, $arrmixMergeFields ) {
		if( true == valArr( $arrmixMergeFields ) ) {
			foreach( $arrmixMergeFields as $strKey => $strValue ) {
				$strHtmlContent = \Psi\CStringService::singleton()->str_ireplace( $strKey, $strValue, $strHtmlContent );
			}
		}

		return $strHtmlContent;
	}

	public function insertCcBccEmail( $objSystemEmail, $objScheduledTaskEmail, $arrmixMergeFields, $strEmailAddress, $intCompanyUserId, $objDatabase ) {
		$objSystemCcBccEmail = clone $objSystemEmail;
		$objSystemCcBccEmail->setId( NULL );
		$objSystemCcBccEmail->setPropertyId( NULL );
		$objSystemCcBccEmail->setToEmailAddress( $strEmailAddress );

		// Setting this to null to void duplciate entry of system email receipients.
		$objSystemCcBccEmail->setSystemEmailRecipients( NULL );

		if( true == valStr( $objSystemCcBccEmail->getHtmlContent() ) && true == valArr( $arrmixMergeFields ) ) {
			$strHtmlContent = $objSystemCcBccEmail->getHtmlContent();
			$strHtmlContent = self::processMergeFieldsInEmailContent( $strHtmlContent, $arrmixMergeFields );

			// Remove unsubscribe link from email content for Cc & Bcc users
			$strEmailContent = self::removeUnsubscribeLinkInEmailContent( $strHtmlContent );

			$objSystemCcBccEmail->setHtmlContent( $strEmailContent );
		}

		$this->insertEmailAttachments( $objSystemCcBccEmail, $objScheduledTaskEmail, $intCompanyUserId, $objDatabase );

		if( false == $objSystemCcBccEmail->validate( VALIDATE_INSERT ) || false == $objSystemCcBccEmail->insert( $intCompanyUserId, $this->m_objEmailDatabase, false, false ) ) {
			return false;
		}

		return true;
	}

	public function loadEmailDatabase() {
		if( false == valObj( $this->m_objEmailDatabase, 'CDatabase' ) ) {
			$this->m_objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseServerType::POSTGRES, false );
		}

		return $this->m_objEmailDatabase;
	}

	public function loadAdminDatabase() {
		if( false == valObj( $this->m_objAdminDatabase, 'CDatabase' ) ) {
			$this->m_objAdminDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::ADMIN, CDatabaseUserType::PS_PROPERTYMANAGER );
		}

		return  $this->m_objAdminDatabase;
	}

	public function loadPaymentDatabase() {
		if( false == valObj( $this->m_objPaymentDatabase, 'CDatabase' ) ) {
			$this->m_objPaymentDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::PAYMENT, CDatabaseUserType::PS_PROPERTYMANAGER );
		}

		return  $this->m_objPaymentDatabase;
	}

	public function getOverdueDateTimeByPropertyId( $objDatabase ) {

		if( false == valId( $this->getPropertyId() ) || false == valId( $this->getOverdueBusinessHours() ) ) {
			return NULL;
		}

		$objProperty 				= \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		$objTimeZone 				= $objProperty->getOrfetchTimezone( $objDatabase );

		if( false == valObj( $objTimeZone, 'CTimeZone' ) ) {
			return NULL;
		}

		if( false == valStr( $objTimeZone->getTimeZoneName() ) ) {
			return NULL;
		}

		$boolIsPropertyHoursAvailable = true;

		$arrobjPropertyHours		= rekeyObjects( 'Day', ( array ) CPropertyHours::fetchCustomPropertyHoursByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase ) );
		$arrobjPropertyHolidays		= rekeyObjects( 'CompanyHolidayDate', ( array ) CPropertyHolidays::fetchCustomPropertyHolidaysByPropertyIdByCidWithCompanyHolidayDate( $this->getPropertyId(), $this->getCid(), $objDatabase ) );

		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrobjPropertyHours ) ) {
			$boolIsPropertyHoursAvailable = false;
		}

		$objCurrentPropertyDateTime = new DateTime( date( 'Y-m-d H:i:s T' ) );

		$objCurrentPropertyDateTime->setTimezone( new DateTimeZone( $objTimeZone->getTimeZoneName() ) );

		// GET BY DATETIME OBJECT
		$intCurrentHourMin			= $objCurrentPropertyDateTime->format( 'H:i' );
		// GET CURRENT DATE OF WEEK
		$intCurrentDayOfWeek 		= ( int ) $objCurrentPropertyDateTime->format( 'N' );
		$strTimezoneAbbr			= $objCurrentPropertyDateTime->format( 'T' );

		$intCurrentPropertyDateTime = ( int ) strtotime( $objCurrentPropertyDateTime->format( 'Y-m-d H:i:s T' ) );
		$intLoopedDatetime 			= $intCurrentPropertyDateTime;

		$boolIsCurrentDay			= true;
		$intOverdueBusinessMins		= $this->getOverdueBusinessHours() * 60;

		while( 0 < $intOverdueBusinessMins ) {

			// Holidays
			$boolIsPropertyHoliday = false;
			$strLoopedDate = getConvertedDateTime( date( 'Y-m-d H:i:s T', $intLoopedDatetime ), 'm/d/Y', date( 'e' ), $objTimeZone->getTimeZoneName() );

			if( true == array_key_exists( $strLoopedDate, $arrobjPropertyHolidays ) ) {
				$objPropertyHoliday = $arrobjPropertyHolidays[( string ) $strLoopedDate];

				if( false == valStr( $objPropertyHoliday->getOpenTime() ) && false == valStr( $objPropertyHoliday->getCloseTime() ) ) {
					$intCurrentDayOfWeek++;
					$intLoopedDatetime = strtotime( '+ 1 Day', $intLoopedDatetime );
					if( 7 < $intCurrentDayOfWeek ) {
						$intCurrentDayOfWeek = 1;
					}
					$boolIsCurrentDay = false;
					continue;
				}

				$strPropertyHourOpenTime	= $objPropertyHoliday->getOpenTime();
				$strPropertyHourCloseTime	= $objPropertyHoliday->getCloseTime();
				$boolIsPropertyHoliday 		= true;
			}
			// Non-Working day

			if( false == $boolIsPropertyHoliday ) {
				if( true == $boolIsPropertyHoursAvailable && ( false == isset( $arrobjPropertyHours[$intCurrentDayOfWeek] ) || 1 == $arrobjPropertyHours[$intCurrentDayOfWeek]->getByAppointmentOnly() ) ) {
					$intCurrentDayOfWeek++;
					$intLoopedDatetime = strtotime( '+ 1 Day', $intLoopedDatetime );
					if( 7 < $intCurrentDayOfWeek ) {
						$intCurrentDayOfWeek = 1;
					}
					$boolIsCurrentDay = false;
					continue;
				}

				if( true == $boolIsPropertyHoursAvailable ) {
					$strPropertyHourOpenTime  = $arrobjPropertyHours[$intCurrentDayOfWeek]->getOpenTime();
					$strPropertyHourCloseTime = $arrobjPropertyHours[$intCurrentDayOfWeek]->getCloseTime();
				} else {
					$strPropertyHourOpenTime  = '08:00:00';
					$strPropertyHourCloseTime = '17:00:00';
				}
			}

			// WORKING DAYs Hours

			if( true == $boolIsCurrentDay ) {
				$intOverDueStartHourMin = ( strtotime( $strPropertyHourOpenTime ) > strtotime( $intCurrentHourMin ) ) ? $strPropertyHourOpenTime : $intCurrentHourMin;
			} else {
				$intOverDueStartHourMin = $strPropertyHourOpenTime;
			}

			if( strtotime( $strPropertyHourCloseTime ) < strtotime( $intOverDueStartHourMin ) ) {
				$intCurrentDayOfWeek++;
				$intLoopedDatetime = strtotime( '+ 1 Day', $intLoopedDatetime );
				if( 7 < $intCurrentDayOfWeek ) {
					$intCurrentDayOfWeek = 1;
				}
				$boolIsCurrentDay = false;
				continue;
			}

			$intTimeDiffMin 			= ( strtotime( $strPropertyHourCloseTime ) - strtotime( $intOverDueStartHourMin ) ) / 60;

			if( false == $boolIsPropertyHoliday ) {
				$intCurrentLunchTimeOfDayOfWeek = $intCurrentDayOfWeek + 10;

				// Considering Lunch Hours
				if( true == isset( $arrobjPropertyHours[$intCurrentLunchTimeOfDayOfWeek] ) ) {

					$strPropertyLunchHourOpenTime	= $arrobjPropertyHours[$intCurrentLunchTimeOfDayOfWeek]->getOpenTime();
					$strPropertyLunchHourCloseTime	= $arrobjPropertyHours[$intCurrentLunchTimeOfDayOfWeek]->getCloseTime();

					$intFirstHalfPropertyHoursInMin	= ( strtotime( $strPropertyLunchHourOpenTime ) - strtotime( $intOverDueStartHourMin ) ) / 60;

					$intTimeDiffLunchMin = ( strtotime( $strPropertyLunchHourCloseTime ) - strtotime( $strPropertyLunchHourOpenTime ) ) / 60;
					$intTimeDiffMin		-= $intTimeDiffLunchMin;
				}
			}

			if( $intOverdueBusinessMins > $intTimeDiffMin ) {
				$intOverdueBusinessMins -= $intTimeDiffMin;
				$intCurrentDayOfWeek++;
				$intLoopedDatetime = strtotime( '+ 1 Day', $intLoopedDatetime );

				if( 7 < $intCurrentDayOfWeek ) {
					$intCurrentDayOfWeek = 1;
				}
				$boolIsCurrentDay = false;
				continue;
			}

			if( false == $boolIsPropertyHoliday && true == isset( $arrobjPropertyHours[$intCurrentLunchTimeOfDayOfWeek] ) ) {
				if( $intOverdueBusinessMins < $intFirstHalfPropertyHoursInMin ) {
					$intOverDueStartHourMin = $intOverDueStartHourMin;
				} else {
					$intOverdueBusinessMins -= $intFirstHalfPropertyHoursInMin;
					$intOverDueStartHourMin = $strPropertyLunchHourCloseTime;
				}
			}

			$strOverDueBusinessRawTime	= date( 'H:i:s', strtotime( '+' . $intOverdueBusinessMins . ' minutes', strtotime( $intOverDueStartHourMin ) ) );
			$strOverDueMin = date( 'i', strtotime( $strOverDueBusinessRawTime ) );

			if( ( $strOverDueMin > 0 && $strOverDueMin < 15 ) || ( $strOverDueMin < 45 && $strOverDueMin > 30 ) ) {
				$strOverDueMinTenthPlace = $strOverDueBusinessRawTime[3];

				if( $strOverDueMinTenthPlace <= 1 ) {
					$strOverDueBusinessModMin = '15';
				} else {
					$strOverDueBusinessModMin = '45';
				}
				$strOverDuetBusinessTime = \Psi\CStringService::singleton()->substr_replace( $strOverDueBusinessRawTime, $strOverDueBusinessModMin, 3, 2 );

			} elseif( $strOverDueMin > 15 && $strOverDueMin < 30 ) {
				$strOverDueBusinessModMin = '30';
				$strOverDuetBusinessTime = \Psi\CStringService::singleton()->substr_replace( $strOverDueBusinessRawTime, $strOverDueBusinessModMin, 3, 2 );
			} elseif( $strOverDueMin > 45 ) {
				$strOverDueHour = date( 'H', strtotime( $strOverDueBusinessRawTime ) );
				$strOverDueBusinessModHour = $strOverDueHour + 1;
				$strOverDuetBusinessTime    = $strOverDueBusinessModHour . ':00';
			} else {
				$strOverDuetBusinessTime = $strOverDueBusinessRawTime;
			}

			$strPropertyOverDueDateTime = getConvertedDateTime( date( 'Y-m-d H:i:s T', $intLoopedDatetime ), 'Y-m-d', date( 'e' ), $objTimeZone->getTimeZoneName() ) . ' ' . $strOverDuetBusinessTime . ' ' . $strTimezoneAbbr;
			$strDueDateTime				= getConvertedDateTimeByTimeZoneName( $strPropertyOverDueDateTime, 'Y-m-d H:i:s T' );

			return $strDueDateTime;
		}
	}

}
?>
