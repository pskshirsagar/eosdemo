<?php

class CAmenityRateAssociations extends CRateAssociations {

	public static function fetchAmenityRateAssociationByArCascadeIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, $intArOriginReferenceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, $intArOriginReferenceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdByArCascadeReferenceIdByPropertyIdByCid( $intArCascadeId, $intArCascadeReferenceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id =  ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationByIdByPropertyIdByCid( $intPropertyId, $intRateAssociationId, $intCid, $objDatabase ) {
	$strSql = ' SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.id = ' . ( int ) $intRateAssociationId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationByArCascadeIdByArCascadeReferenceIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, $intArCascadeReferenceId, $intArOriginReferenceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_cascade_reference_id =' . ( int ) $intArCascadeReferenceId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_origin_reference_id =' . ( int ) $intArOriginReferenceId . '
					LIMIT 1';

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdsByAmenityTypeIdByIntegrationDatabaseIdByCid( $arrintArCascadeIds, $intAmenityTypeId, $intIntegrationDatabaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN property_integration_databases pid ON ( ra.cid = pid.cid AND ra.property_id = pid.property_id )
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.remote_primary_key IS NOT NULL
						AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id IN ( ' . implode( ',', $arrintArCascadeIds ) . ' )
						AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdsByIntegrationDatabaseIdByCid( $arrintArCascadeIds, $intIntegrationDatabaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN property_integration_databases pid ON ( ra.cid = pid.cid AND ra.property_id = pid.property_id )
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.remote_primary_key IS NOT NULL
						AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id IN ( ' . implode( ',', $arrintArCascadeIds ) . ' )';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArOriginReferenceIdByCid( $intArOriginReferenceId, $intCid, $objDatabase ) {

		if( true == is_null( $intArOriginReferenceId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						rate_associations
					WHERE
						ar_origin_reference_id =' . ( int ) $intArOriginReferenceId . '
						AND cid = ' . ( int ) $intCid . '
						AND ar_origin_id = ' . ( int ) CArOrigin::AMENITY;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchIntegratedAmenityRateAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ( ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . ' OR ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . ' )
							AND a.amenity_type_id = ' . ( int ) CAmenityType::APARTMENT . '
					 	)
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.remote_primary_key IS NOT NULL';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdByAmenityTypeIdByPropertyIdByCid( $intArCascadeId, $intAmenityTypeId = NULL, $intPropertyId, $intCid, $objDatabase, $boolFetchData = false, $intPageNo = NULL, $intPageSize = NULL, $boolIsPublished = false, $arrintOccupancyTypeIds = NULL, $arrstrSearchFilter = [] ) {

		if( false == valId( $intPropertyId ) || false == valId( $intArCascadeId ) ) {
			return NULL;
		}

		$strSelectAppend	= '';
		$strOrderNum		= ' ORDER BY ra.order_num, ' . $objDatabase->getCollateSort( 'a.name' );
		$strSelectJoin		= '';
		$strWhere		   = '';
		$intOffset			= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit			= ( int ) $intPageSize;

		if( $intArCascadeId == CArCascade::PROPERTY ) {
			$strOrderNum		= ' ORDER BY a.amenity_type_id, ra.order_num';
		} elseif( $intArCascadeId == CArCascade::FLOOR_PLAN ) {
			$strSelectAppend	= ', pf.floorplan_name as floorplan_name';
			$strSelectJoin		= ' LEFT JOIN property_floorplans pf ON ( pf.id = ra.ar_cascade_reference_id AND pf.cid = ra.cid ) ';
		} elseif( $intArCascadeId == CArCascade::SPACE ) {
			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintOccupancyTypeIds ) && true == in_array( $arrintOccupancyTypeIds[0], [ COccupancyType::STUDENT ] ) ) {
				$strSelectAppend	= ', CONCAT( us.unit_number , \' - \', us.space_number )  as unit_space_name, us.building_name,
										us.property_unit_id';
			} else {
				$strSelectAppend	= ', CONCAT( us.unit_number )  as unit_space_name, us.building_name,
										us.property_unit_id';
			}
			$strSelectJoin		= ' LEFT JOIN unit_spaces us ON (ra.ar_cascade_reference_id = us.id AND ra.cid = us.cid)';
			$strOrderNum		.= ', substring( us.building_name , \'\' ) ASC NULLS FIRST,
									substring( us.building_name, \'[^0-9_].*$\') ASC NULLS FIRST,
									(substring( us.building_name, \'^[0-9]+\'))::NUMERIC ASC NULLS FIRST,
									COALESCE(SUBSTRING(us.unit_number FROM \'^(\\\d+)\')::NUMERIC, 99999999) ASC,
									SUBSTRING(us.unit_number FROM \'^\\\d* *(.*?)( \\\d+)?$\') ASC,
									COALESCE(SUBSTRING(us.unit_number FROM \' (\\\d+)$\')::NUMERIC, 0) ASC,
									LENGTH(us.unit_number),us.unit_number ASC, us.space_number ASC';
		} elseif( $intArCascadeId == CArCascade::UNIT_TYPE ) {
			$strSelectAppend	= ', COALESCE( ut.name, NULL ) as unittype_name';
			$strSelectJoin		= ' LEFT JOIN unit_types ut ON ( ut.id = ra.ar_cascade_reference_id AND ut.cid = ra.cid ) ';
		}

		if( true == valStr( $arrstrSearchFilter['amenity'] ) ) {
			$strWhere .= ' AND a.name ILIKE \'%' . $arrstrSearchFilter['amenity'] . '%\'';
		}

		if( true == valStr( $arrstrSearchFilter['floor_plan'] ) ) {
			$strWhere .= ' AND pf.floorplan_name ILIKE \'%' . $arrstrSearchFilter['floor_plan'] . '%\'';
		}

		if( false != valArr( $arrstrSearchFilter['ar_code_type_ids'] ) ) {
			$strWhere .= ' AND EXISTS (
				SELECT 1
				FROM rates r
				WHERE r.cid = ' . ( int ) $intCid . '
					AND r.property_id = ' . ( int ) $intPropertyId . '
					AND r.ar_origin_id = ra.ar_origin_id
					AND ra.ar_cascade_id = r.ar_cascade_id
					AND r.ar_origin_reference_id = ra.ar_origin_reference_id
					AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id
					AND r.is_allowed IS TRUE
					AND r.ar_code_type_id IN ( ' . implode( ',', $arrstrSearchFilter['ar_code_type_ids'] ) . ' )
					AND r.rate_amount != 0)';

		}

		if( true == valStr( $arrstrSearchFilter['unit_space'] ) ) {
			$strWhere .= ' AND ( CONCAT( us.building_name , \' - \', us.unit_number , \' - \', us.space_number ) ILIKE \'%' . $arrstrSearchFilter['unit_space'] . '%\'';
			$strWhere .= ' OR CONCAT( us.building_name , \'-\', us.unit_number , \'-\', us.space_number ) ILIKE \'%' . $arrstrSearchFilter['unit_space'] . '%\'';
			$strWhere .= ' OR us.space_number ILIKE \'%' . $arrstrSearchFilter['unit_space'] . '%\'';
			$strWhere .= ' OR us.unit_number ILIKE \'%' . $arrstrSearchFilter['unit_space'] . '%\' )';
		}

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						ra.*,
						' . ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ? 'util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ' : 'a.name' ) . ' AS amenity_name,
						util_get_translated( \'external_description\', a.external_description, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_external_description,
						a.amenity_type_id,
						a.default_amenity_id,
						util_get_system_translated( \'name\', da.name, da.details ) AS default_amenity_name
						' . $strSelectAppend . '
					FROM
						rate_associations ra
						LEFT JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
						JOIN default_amenities da ON ( da.id = a.default_amenity_id )
						' . $strSelectJoin . '
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id =  ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . $strWhere;

		$strSql .= ( false == is_null( $intAmenityTypeId ) ) ? ' AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId : '';
		$strSql .= ( false != $boolIsPublished ) ? ' AND ra.is_published = TRUE ' : '';
		$strSql .= $strOrderNum;

		if( 0 < $intLimit ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( false != $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdArCascadeReferenceIdsByAmenityTypeIdByPropertyIdByCid( $arrintArCascadeIdArCascadeReferenceIds, $intAmenityTypeId = NULL, $intPropertyId, $intCid, $objDatabase, $boolFetchData = false, $boolIsPublished = false ) {

		if( false == valArr( $arrintArCascadeIdArCascadeReferenceIds ) || \Psi\Libraries\UtilFunctions\count( $arrintArCascadeIdArCascadeReferenceIds, COUNT_NORMAL ) == \Psi\Libraries\UtilFunctions\count( $arrintArCascadeIdArCascadeReferenceIds, COUNT_RECURSIVE ) ) {
			return NULL;
		}

		$strSelectAppend	= '';
		$strSelectJoin		= '';

		if( true == array_key_exists( CArCascade::FLOOR_PLAN, $arrintArCascadeIdArCascadeReferenceIds ) ) {
			$strSelectAppend	= ', pf.floorplan_name as floorplan_name';
			$strSelectJoin		= ' LEFT JOIN property_floorplans pf ON ( pf.id = ra.ar_cascade_reference_id AND pf.cid = ra.cid ) ';
		} elseif( true == array_key_exists( CArCascade::SPACE, $arrintArCascadeIdArCascadeReferenceIds ) ) {
			$strSelectAppend	= ', us.unit_number as unit_space_name ';
			$strSelectJoin		= ' LEFT JOIN unit_spaces us ON (ra.ar_cascade_reference_id = us.id AND ra.cid = us.cid)';
		}

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
						ra.description AS amenity_external_description,
						a.company_media_file_id,
						a.amenity_type_id
						' . $strSelectAppend . '
					FROM
						rate_associations ra
						LEFT JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
						' . $strSelectJoin . '
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id =  ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ( ra.ar_cascade_id, ra.ar_cascade_reference_id ) IN ( ' . sqlIntMultiImplode( $arrintArCascadeIdArCascadeReferenceIds ) . ' )';

		$strSql .= ( true == $boolIsPublished ) ? ' AND ra.is_published = TRUE ' : NULL;
		$strSql .= ( false == is_null( $intAmenityTypeId ) ) ? ' AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId : '';
		$strSql .= ' ORDER BY ra.order_num';

		if( false != $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedAmenityRateAssociationsByArCascadeIdsByAmenityTypeIdsByFloorplanIdByPropertyIdByCid( $arrintArCascadeIds, $arrintAmenityTypeIds, $intPropertyId, $intCid, $objDatabase, $intFloorplanId = NULL ) {

		if( false == valArr( $arrintArCascadeIds ) || false == valArr( $arrintAmenityTypeIds ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strJoin = '';
		$strWhere = '';

		if( true == in_array( CArCascade::SPACE, $arrintArCascadeIds ) ) {

			if( false == valId( $intFloorplanId ) ) {
				return NULL;
			}

			$strJoin .= 'LEFT JOIN unit_spaces us ON ( ra.cid = us.cid AND us.deleted_on IS NULL AND ra.ar_cascade_reference_id = us.id )';
			$strWhere .= 'AND CASE
							  WHEN ( ra.ar_cascade_id = ' . CArCascade::SPACE . ' ) THEN us.property_floorplan_id = ' . ( int ) $intFloorplanId . '
							  ELSE TRUE
							END';
		}

		if( true == in_array( CArCascade::FLOOR_PLAN, $arrintArCascadeIds ) ) {

			if( false == valId( $intFloorplanId ) ) {
				return NULL;
			}

			$strWhere .= 'AND CASE
							  WHEN ( ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' ) THEN ra.ar_cascade_reference_id = ' . ( int ) $intFloorplanId . '
							  ELSE TRUE
							END';
		}

		$strSql = 'SELECT
						DISTINCT a.id,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
						a.amenity_type_id,
						ra.ar_cascade_id
					FROM
						amenities a
						JOIN rate_associations ra ON (
							ra.cid = a.cid
							AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = TRUE
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id IN ( ' . implode( ',', $arrintArCascadeIds ) . ' )
						)
						' . $strJoin . '
					WHERE
						ra.property_id =  ' . ( int ) $intPropertyId . '
						AND a.deleted_on IS NULL
						AND a.amenity_type_id IN ( ' . implode( ',', $arrintAmenityTypeIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						' . $strWhere . '
					ORDER BY
						ra.ar_cascade_id';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedAmenityRateAssociationsByArCascadeIdByAmenityTypeIdsByPropertyIdByCid( $intArCascadeId, $arrintAmenityTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAmenityTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					ra.*,
					util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
					a.default_amenity_id as default_amenity_id,
					da.name as default_amenity_name
				FROM
					rate_associations ra
					JOIN amenities a ON (
						ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
						AND ra.is_published = true
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND a.amenity_type_id IN ( ' . implode( ',', $arrintAmenityTypeIds ) . ' )
					 )
					JOIN default_amenities da ON ( da.id = a.default_amenity_id )
				WHERE
					ra.property_id =  ' . ( int ) $intPropertyId . '
					AND	ra.cid = ' . ( int ) $intCid . '
				ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedAmenityRateAssociationsByArCascadeIdByArCascadeReferenceIdsByPropertyIdByCid( $intArCascadeId, $arrintArCascadeReferenceIds, $intPropertyId, $intCid, $objDatabase, $arrintAmenityTypeIds = [] ) {

		if( false == valArr( $arrintArCascadeReferenceIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id =  ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintArCascadeReferenceIds ) . ' )
						AND ra.is_published = true';

						if( false != valArr( $arrintAmenityTypeIds ) ) {
							$strSql .= ' AND a.amenity_type_id IN ( ' . implode( ',', $arrintAmenityTypeIds ) . ' )';
						}

		$strSql .= ' ORDER BY
						ra.is_featured DESC,
						ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdByArCascadeReferenceIdsByAmenityTypeIdByPropertyIdByCid( $intArCascadeId, $intArCascadeReferenceIds, $intAmenityTypeId = NULL, $intPropertyId, $intCid, $objDatabase, $arrmixSearchFilter = NULL ) {

		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}

		if( false == valId( $intArCascadeId ) ) {
			return NULL;
		}

		if( false == valArr( $intArCascadeReferenceIds ) ) {
			return NULL;
		}

		$strSelectAppend	= '';
		$strSelectJoin		= '';
		$strWhere           = '';
		$strOrderBy			= ' ORDER BY ra.order_num';

		if( $intArCascadeId == CArCascade::FLOOR_PLAN ) {
			$strSelectAppend	= ', pf.floorplan_name as floorplan_name';
			$strSelectJoin		= ' LEFT JOIN property_floorplans pf ON ( pf.id = ra.ar_cascade_reference_id AND pf.cid = ra.cid ) ';
		} elseif( $intArCascadeId == CArCascade::SPACE ) {
			$strSelectAppend	= ', CONCAT( us.unit_number , \' - \', us.space_number )  as unit_space_name, us.property_unit_id ';
			$strSelectJoin		= ' LEFT JOIN unit_spaces us ON (ra.ar_cascade_reference_id = us.id AND ra.cid = us.cid)';
			$strOrderBy			= ' ORDER BY ra.order_num';
		}

		if( false != valArr( $arrmixSearchFilter['ar_code_type_ids'] ) ) {
			$strWhere .= ' AND EXISTS (
				SELECT 1
				FROM rates r
				WHERE r.cid = ' . ( int ) $intCid . '
					AND r.property_id = ' . ( int ) $intPropertyId . '
					AND r.ar_origin_id = ra.ar_origin_id
					AND ra.ar_cascade_id = r.ar_cascade_id
					AND r.ar_origin_reference_id = ra.ar_origin_reference_id
					AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id
					AND r.is_allowed IS TRUE
					AND r.ar_code_type_id IN ( ' . implode( ',', $arrmixSearchFilter['ar_code_type_ids'] ) . ' ) 
					AND r.rate_amount != 0 )';
		}
		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
						a.amenity_type_id
						' . $strSelectAppend . '
					FROM
						rate_associations ra
						LEFT JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
						' . $strSelectJoin . '
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id =  ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_cascade_reference_id IN  ( ' . implode( ',', $intArCascadeReferenceIds ) . ' )' . $strWhere;

		$strSql .= ( false == is_null( $intAmenityTypeId ) ) ? ' AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId : '';
		$strSql .= $strOrderBy;
		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceAmenityRateAssociationsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*,
						us.property_unit_id
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . ' )
						JOIN unit_spaces us ON ( ra.cid = us.cid AND ra.ar_cascade_reference_id = us.id AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . ')
					WHERE
						ra.cid = ' . ( int ) $intCid . '
					ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByPropertyIdsByAmenityTypeIdByCid( $arrintPropertyIds, $intAmenityTypeId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
							AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId . '
						 )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByUnitSpaceIdsByCid( $arrintUnitSpaceIds, $intCid, $objDatabase, $intPropertyId = NULL ) {
		$strPropertyCondition = '';
		if( false == is_null( $intPropertyId ) )
			$strPropertyCondition = ' AND us.property_id =  ' . ( int ) $intPropertyId;

		$strSql = 'SELECT
						us.property_unit_id,
						us.id,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
						util_get_translated( \'external_description\', a.external_description, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_external_description,
						a.amenity_type_id,
						rl.rate_amount,
						lsw.start_date as start_date,
						lsw.end_date as end_date
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					   	JOIN unit_spaces us ON ( ( ra.ar_cascade_reference_id = us.id AND ra.cid = us.cid AND ra.ar_cascade_id = ' . CArCascade::SPACE . ' )
					   								OR ( ra.ar_cascade_reference_id = us.property_floorplan_id AND ra.cid = us.cid AND ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' )
					   								OR ( ra.ar_cascade_reference_id = us.property_id AND ra.cid = us.cid AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' )
					   							)
						LEFT JOIN rate_logs rl ON(
								rl.cid = us.cid
								AND rl.property_id = us.property_id
								AND rl.ar_origin_id = ' . CArOrigin::AMENITY . '
								AND rl.ar_origin_reference_id = a.id
								AND rl.is_allowed = TRUE
								AND rl.rate_amount != 0
								AND rl.show_in_entrata = TRUE
								AND rl.show_on_website = TRUE
								AND CURRENT_DATE BETWEEN rl.effective_date AND rl.effective_through_date
								AND (
									( rl.ar_cascade_id = ' . CArCascade::SPACE . ' AND rl.ar_cascade_reference_id = us.id AND ra.ar_cascade_reference_id = us.id )
									OR ( rl.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND rl.ar_cascade_reference_id = us.property_id AND ra.ar_cascade_reference_id = us.property_id )
									OR ( rl.ar_cascade_id =  ' . CArCascade::FLOOR_PLAN . ' AND rl.ar_cascade_reference_id = us.property_floorplan_id AND ra.ar_cascade_reference_id = us.property_floorplan_id )
								)
							)
						LEFT JOIN lease_start_windows lsw ON( lsw.cid = rl.cid AND lsw.id = rl.lease_start_window_id )
					WHERE
						us.cid = ' . ( int ) $intCid . '
						' . $strPropertyCondition . '
						AND us.id IN(' . implode( ',', $arrintUnitSpaceIds ) . ')
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )
						AND ( ( rl.id IS NOT NULL AND rl.show_on_website = TRUE AND rl.show_in_entrata = TRUE )
						OR ( rl.id IS NULL ) ) 
					ORDER BY
                      ra.ar_cascade_id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceAmenityRateAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*,
						us.property_unit_id
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . ' )
						JOIN unit_spaces us ON ( ra.cid = us.cid AND ra.ar_cascade_reference_id = us.id AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . ')
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
					ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceAmenityRateAssociationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ra.*,
						us.property_unit_id
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . ' )
						JOIN unit_spaces us ON ( ra.cid = us.cid AND ra.ar_cascade_reference_id = us.id AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . ')
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchFloorplanAmenityRateAssociationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
						JOIN property_floorplans pf ON ( pf.cid = ra.cid AND pf.id = ra.ar_cascade_reference_id )
					WHERE
						ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . '
					ORDER BY
						ra.order_num';
		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsDependencyByArOriginReferenceIdByExcludeRateAssociationIdsByCid( $intArOriginReferenceId, $arrintRateAssociationIds, $intCid, $objDatabase ) {

		if( true == is_null( $intArOriginReferenceId ) ) {
			return NULL;
		}

		if( true == is_null( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT COUNT ( ra.id ) as id
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
						AND ra.id NOT IN ( ' . implode( ',', $arrintRateAssociationIds ) . ' )';

		if( 0 < ( int ) self::fetchColumn( $strSql, 'id', $objDatabase ) ) {
			return false;
		}
		return true;
	}

	public static function fetchAmenityRateAssociationCompanyMediaFileDependencyByCompanyMediaFileIdByArOriginReferenceIdByCid( $intCompanyMediaFileId, $intArOriginReferenceId, $intPropertyId, $intCid, $objDatabase ) {

		if( true == is_null( $intCompanyMediaFileId ) || true == is_null( $intArOriginReferenceId ) || true == is_null( $intPropertyId ) || true == is_null( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT COUNT ( ra.id ) as id
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
						AND ra.company_media_file_id =  ' . ( int ) $intCompanyMediaFileId;

		if( 0 < ( int ) self::fetchColumn( $strSql, 'id', $objDatabase ) ) {
			return false;
		}
		return true;
	}

	// Property Amenities Section

	public static function fetchPublishedAmenityRateAssociationsByArCascadeIdByAmenityTypeIdByPropertyIdsByCid( $intArCascadeId, $intAmenityTypeId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					ra.*,
					a.default_amenity_id as default_amenity_id,
					util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
				FROM
					rate_associations ra
					JOIN amenities a ON (
						ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
						AND ra.is_published = true
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId . '
					 )
				WHERE
					ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND ra.cid = ' . ( int ) $intCid . '
				ORDER BY
					ra.is_featured DESC,
					ra.order_num';
		$strSqlKey = $strSql;

		$arrstrAmenities = CCache::fetchObject( $strSqlKey );

		if( NULL == $arrstrAmenities ) {
			$arrstrAmenities = self::fetchRateAssociations( $strSql, $objDatabase );
			CCache::storeObject( $strSqlKey, $arrstrAmenities, $intSpecificLifetime = 3600 );
			$strCacheListKey = ( string ) $intCid;
			$arrstrCacheList = CCache::fetchObject( $strCacheListKey );
			$arrstrCacheList[sha1( $strSql )] = 42;
			CCache::storeObject( $strCacheListKey, $arrstrCacheList );
		}

		return $arrstrAmenities;

	}

	public static function fetchPublishedOptionalAmenityRateAssociationsByArCascadeIdByAmenityTypeIdsByPropertyIdByCid( $intArCascadeId, $arrintAmenityTypeIds, $intPropertyId, $intCid, $objDatabase, $boolIsPublished = true ) {

		if( false == valArr( $arrintAmenityTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					ra.*,
					util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
				FROM
					rate_associations ra
					JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
				WHERE
					ra.is_optional = false
					AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
					AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
					AND a.amenity_type_id IN ( ' . implode( ',', $arrintAmenityTypeIds ) . ' )
					AND ra.property_id =  ' . ( int ) $intPropertyId . '
					AND ra.cid = ' . ( int ) $intCid;
		$strSql .= ( true == $boolIsPublished ) ? ' AND ra.is_published = TRUE' : '';
		$strSql .= ' ORDER BY order_num ';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByRateAssociationIdsByCid( $arrintRateAssociationIds, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.id IN ( ' . implode( ',', $arrintRateAssociationIds ) . ' )
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedFeaturedAmenityRateAssociationsByArCascadeIdByAmenityTypeIdByPropertyIdByCid( $intArCascadeId, $intAmenityTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					ra.*,
					util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
				FROM
					rate_associations ra
					JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
				WHERE
					ra.is_published = true
					AND ra.is_featured = true
					AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
					AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
					AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId . '
					AND ra.property_id = ' . ( int ) $intPropertyId . '
					AND ra.cid = ' . ( int ) $intCid . '
				ORDER BY
					ra.is_featured DESC,
					ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchCommunityAmenityByRateAssociationIdByFieldNamesByCid( $intRateAssociationId, $arrstrFieldNames, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					' . implode( ',', $arrstrFieldNames ) . '
				FROM
					rate_associations ra
					JOIN add_ons ao ON ( ao.cid = ra.cid AND ra.ar_origin_reference_id = ao.id )
					LEFT JOIN property_amenity_availabilities paa ON( ra.id = paa.rate_association_id AND ra.cid = paa.cid )
				WHERE
					ra.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
					AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
					AND ra.id = ' . ( int ) $intRateAssociationId . '
					AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByRateAssociationIdsWithNameByCid( $arrintRateAssociationIds, $intCid, $objDatabase, $boolReturnNonCachedObject = true ) {

		if( false == valArr( $arrintRateAssociationIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						ra.*,util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.id IN ( ' . implode( ',', $arrintRateAssociationIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid;

		if( true == $boolReturnNonCachedObject ) {
			return self::fetchRateAssociations( $strSql, $objDatabase );
		}

		$strSqlKey = $strSql;
		$arrstrAmenities = CCache::fetchObject( $strSqlKey );

		if( NULL == $arrstrAmenities ) {
			$arrstrAmenities = self::fetchRateAssociations( $strSql, $objDatabase );
			CCache::storeObject( $strSqlKey, $arrstrAmenities, $intSpecificLifetime = 86400 );
			$strCacheListKey = ( string ) $intCid;
			$arrstrCacheList = CCache::fetchObject( $strCacheListKey );
			$arrstrCacheList[sha1( $strSql )] = 42;
			CCache::storeObject( $strCacheListKey, $arrstrCacheList );
		}

		return $arrstrAmenities;
	}

	public static function fetchAmenityRateAssociationsByPropertyIdsWithNameByAmenityTypeIdByCid( $arrintPropertyIds, $intAmenityTypeId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ra.*, util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId . '
						AND ra.property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
					ORDER BY ' . $objDatabase->getCollateSort( 'a.name' );

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationWithAmenityNameByIdByAmenityTypeIdByPropertyIdByCid( $intRateAssociationId, $intAmenityTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						ra.*,util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.id = ' . ( int ) $intRateAssociationId . '
						AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchAllAmenityRateAssociationsByAmenityTypeIdByPropertyIdByCid( $intAmenityTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						ra.*, util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					ORDER BY ' . $objDatabase->getCollateSort( 'a.name' );

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	// Unit Amenities Section

	public static function fetchAmenityRateAssociationsByUnitSpaceIdsByPropertyIdByCid( $arrintUnitSpaceIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						AND ra.ar_cascade_reference_id IN  ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByAmenityTypeIdByPropertyIdByCid( $intAmenityTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ra.*,util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId . '
				 		)
					WHERE
						ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedOptionalAmenityRateAssociationsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					ra.*,
					util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
				FROM
					rate_associations ra
				JOIN amenities a ON (
					ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
					AND ra.is_optional = false
					AND ra.is_published = true
					AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
					AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
				 )
				WHERE
					ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . '
					AND ra.property_id =  ' . ( int ) $intPropertyId . '
					AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedAmenityRateAssociationsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $intPropertyId, $intCid, $objDatabase, $boolIsOptional = false ) {

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = true
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
					 	)
					WHERE
						ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . '
						AND ra.property_id =  ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;
		$strSql .= ( false == is_null( $boolIsOptional ) ) ? ' AND ra.is_optional = ' . ( ( true == $boolIsOptional ) ? 'true' : 'false' ) : '';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedAmenityRateAssociationsByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase, $boolIsOptional = false, $boolIncludeNegativeFeatures = false ) {

		$strCheckNegativeFeaturesSql = ( false == $boolIncludeNegativeFeatures ) ? ' AND a.amenity_type_id <> ' . CAmenityType::NEGATIVE_FEATURES . ' ' : '';

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = true
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
							' . $strCheckNegativeFeaturesSql . '
						 )
					WHERE
						ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . '

						AND ra.cid = ' . ( int ) $intCid;
		$strSql .= ( false == is_null( $boolIsOptional ) ) ? ' AND ra.is_optional = ' . ( int ) $boolIsOptional : '';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdByArOriginReferenceIdsByPropertyIdByCid( $intArCascadeId, $arrintArOriginReferenceIds, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_origin_reference_id IN ( ' . implode( ',', $arrintArOriginReferenceIds ) . ' )
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenityRateAssociationsByArCascadeIdsByArOriginReferenceIdsByExcludeRateAssociationIdsByCid( $arrintArCascadeIds, $arrintArOriginReferenceIds, $arrintRateAssociationIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_cascade_id IN ( ' . implode( ',',  $arrintArCascadeIds ) . ' )
						AND ra.ar_origin_reference_id IN ( ' . implode( ',', $arrintArOriginReferenceIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.id NOT IN ( ' . implode( ',', $arrintRateAssociationIds ) . ' ) ';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedAmenityRateAssociationsWithAmenityNameByUnitSpaceIdsByPropertyIdByCid( $arrintUnitSpaceIds, $intPropertyId, $intCid, $objDatabase, $boolIncludeNegativeFeatures = false ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) {
			return NULL;
		}

		$strCheckNegativeFeaturesSql = ( false == $boolIncludeNegativeFeatures ) ? ' AND a.amenity_type_id <> ' . CAmenityType::NEGATIVE_FEATURES . ' ' : '';

		$strSql = 'SELECT
						ra.*, util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = true
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
							' . $strCheckNegativeFeaturesSql . '
					 	)
					WHERE
						ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
				order by ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedDistinctAmenityRateAssociationsWithAmenityNameByUnitSpaceIdsByPropertyIdByCid( $arrintUnitSpaceIds, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) {
			return NULL;
		}

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ra.ar_origin_reference_id,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = true
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . CArCascade::SPACE . ')
						JOIN unit_spaces us ON ( us.property_floor_id IS NOT NULL AND ra.ar_cascade_reference_id = us.id AND ra.cid = us.cid ' . $strCheckDeletedUnitsSql . ' )
					WHERE
						ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					GROUP BY
						ra.ar_origin_reference_id,
						a.name,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' )
					ORDER BY
					' . $objDatabase->getCollateSort( 'a.name', true );

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedUnitSpaceAmenityRateAssociationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
						a.id as amenity_id
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = true
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
					 	)
					WHERE
						ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
					GROUP BY a.name, a.id, a.details-->\'' . CLocaleContainer::createService()->getLocaleCode() . '-->name\'';

		return parent::fetchObjects( $strSql, 'CAmenityRateAssociation', $objDatabase );
	}

	public static function fetchAmenityRateAssociationsWithAmenityNameByRateAssociationIdByPropertyIdByCid( $intRateAssociationId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
					 	)
					WHERE
						ra.id = ' . ( int ) $intRateAssociationId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchPublishedOptionalAmenityRateAssociationsByFloorplanIdByPropertyIdByCid( $intPropertyFloorplanId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT ra.*,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = true
							AND ra.is_optional = false
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . CArCascade::SPACE . ' )
						JOIN unit_spaces us ON ( us.id = ra.ar_cascade_reference_id AND ra.property_id = us.property_id AND ra.cid = us.cid ' . $strCheckDeletedUnitsSql . ' )
					WHERE
						us.property_floorplan_id = ' . ( int ) $intPropertyFloorplanId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchReservableAmenitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::ADD_ONS ] );
		$objRateLogsFilter->setPropertyRatesOnly( true );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

			$strSql = 'SELECT
							DISTINCT ON( ra.id )
							ra.id,
							util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
							COALESCE( pr.rate_amount, 0 ) as amenity_rate,
							paa.require_property_approval,
							paa.reservation_fee_occurrence_type_id,
							paa.max_reservation_time_limit,
							paa.is_reserved_by_day,
							paa.rp_description,
							paaf.file_id,
							f.file_extension_id,
							f.file_name,
							f.file_path,
							a.id as amenity_id,
							paa.property_id as property_id,
							paa.is_future_allowed,
							paa.reservable_within_days,
							paa.amenity_availability,
							blackout_holidays,
							ra.ar_origin_reference_id,
							(   
								SELECT
									cc.fullsize_uri
								FROM
									rate_associations ras
									LEFT JOIN company_media_files cc ON ( cc.id = ras.company_media_file_id AND cc.cid = ras.cid )
								WHERE
									ras.cid = ' . ( int ) $intCid . '
									AND ras.property_id = ' . ( int ) $intPropertyId . '
									AND ras.ar_cascade_id = ' . CArCascade::PROPERTY . '
									AND ras.ar_cascade_reference_id =' . ( int ) $intPropertyId . '
									AND ras.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
									AND ras.ar_origin_reference_id = a.id
								LIMIT 1
							) as fullsize_uri
						FROM
							rate_associations ra
							JOIN property_amenity_availabilities paa ON( ra.id = paa.rate_association_id AND ra.cid = paa.cid )
							JOIN amenities a ON( a.id = paa.amenity_id AND a.cid = ra.cid )
							JOIN add_ons ao ON ( ao.id = ra.ar_origin_reference_id AND ao.cid = ra.cid )
							JOIN add_on_groups aog ON ( aog.id = ao.add_on_group_id AND aog.cid = ao.cid )
							JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id = aog.property_id OR aoc.property_id IS NULL ) )
							LEFT JOIN property_amenity_availability_file_associations paaf ON( paa.id = paaf.property_amenity_availability_id AND paa.cid = paaf.cid )
							LEFT JOIN files f ON( f.id = paaf.file_id AND f.cid = paaf.cid )
							LEFT JOIN prospect_rates pr ON( a.id = pr.ar_origin_reference_id AND a.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::AMENITY . ' )
						WHERE
							ra.cid = ' . ( int ) $intCid . '
							AND	ra.property_id = ' . ( int ) $intPropertyId . '
							AND aog.add_on_type_id = ' . CAddOnType::RESERVATIONS . '
							AND aoc.default_add_on_category_id = ' . CDefaultAddOnCategory::RESERVATIONS . '
							AND ra.ar_origin_id = ' . CArOrigin::ADD_ONS . '
							AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
							AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
							AND paa.hide_amenity = false
						ORDER BY
							ra.id,
							' . $objDatabase->getCollateSort( 'a.name' );

			return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	// Unit Space Amenities

	public static function fetchPublishedOptionalAmenityRateAssociationsByArCascadeIdByPropertyFloorplanIdByPropertyIdByCid( $intArCascadeId, $intPropertyFloorplanId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strSql = 'SELECT
						DISTINCT ra.*,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = true
							AND ra.is_optional = false
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . ' )
						JOIN unit_spaces us ON ( us.id = ra.ar_cascade_reference_id AND ra.property_id = us.property_id AND ra.cid = us.cid ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND us.property_floorplan_id = ' . ( int ) $intPropertyFloorplanId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;
		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	// Floorplan Amenities

	public static function fetchAmenityRateAssociationsWithFloorplanAmenityNameByPropertyFloorplanIdByPropertyIdByCid( $intPropertyFloorplanId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . ( int ) CArCascade::FLOOR_PLAN . ' )
					WHERE
						( lower( a.name ) LIKE lower( \'%Washer%\' ) OR lower( a.name ) LIKE lower( \'%Dryer%\' ) )
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyFloorplanId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'a.name' );

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	// Rates

	public static function fetchAmenityRateAssociationWithRatesByAmenityTypeIdByPropertyIdByCid( $intAmenityTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::ADD_ONS ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
						COALESCE( pr.rate_amount, 0 ) as amenity_rate
					FROM
						rate_associations ra
						JOIN amenities a ON (
								ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
								AND ra.is_published = true
								AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
								AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
								AND a.amenity_type_id = ' . ( int ) $intAmenityTypeId . ' )
					 	)
						LEFT JOIN prospect_rates pr ON( a.id = pr.ar_origin_reference_id AND a.cid = pr.cid AND ra.property_id = pr.property_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND	ra.property_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						ra.id,
						' . $objDatabase->getCollateSort( 'a.name' );

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchMaxOrderNumFromAmenityRateAssociationByArCascadeIdPropertyIdByCid( $intArCascadeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						max( ra.order_num ) + 1 as order_num
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['order_num'] ) ) {
			return $arrintResponse[0]['order_num'] + 1;
		}

		return 0;
	}

	public static function fetchOrderNumFromAmenityRateAssociationByArOriginIdByArCascadeIdPropertyIdByCid( $intArOriginReferenceId, $intArCascadeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.order_num as order_num
					FROM
						rate_associations ra
					WHERE
						ra.ar_origin_id = ' . CArOrigin::AMENITY . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintResponse ) && true == isset( $arrintResponse[0]['order_num'] ) ) {
			return $arrintResponse[0]['order_num'];
		}

		return 'false';
	}

	public static function fetchPublishedAmenityRateAssociationsByUnitSpaceIdsByCid( $arrintUnitSpaceIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ra.*, util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.is_published = true
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
					 	)
					WHERE
						ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						AND a.amenity_type_id <> ' . CAmenityType::NEGATIVE_FEATURES . '
					ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchPublishedPropertyAmenityRateAssociationsByAmenityTypeIdsByPropertyIdsByCid( $arrintAmenityTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIsPublished = true ) {

		if( false == valArr( $arrintAmenityTypeIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhereClause = ( true == $boolIsPublished ) ? ' AND ra.is_published = true ' : '';

		$strSql = 'SELECT
					ra.*,
					a.amenity_type_id as amenity_type_id,
					util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
					da.name as default_amenity_name
				FROM
					rate_associations ra
					JOIN amenities a ON (
						ra.cid = a.cid AND ra.ar_origin_reference_id = a.id' . $strWhereClause . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND a.amenity_type_id IN ( ' . implode( ',', $arrintAmenityTypeIds ) . ' )
					 )
					JOIN default_amenities da ON ( da.id = a.default_amenity_id )
				WHERE
					ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND	ra.cid = ' . ( int ) $intCid . '
				ORDER BY ra.order_num';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedAmenityRateAssociationsByArCascadeIdByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolCheckILSAmenities = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhereCondition = '';
		if( true == $boolCheckILSAmenities ) {
			$strWhereCondition = ' AND a.default_amenity_id BETWEEN 1 AND 138';
		}

		$strSql = 'SELECT
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
						a.id as amenity_id,
						ra.property_id
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid 
							AND ra.ar_origin_reference_id = a.id
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ( ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
								  OR ra.ar_cascade_id = ' . ( int ) CArCascade::FLOOR_PLAN . '
								  OR ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . ' )
					 	)
					WHERE
						ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . $strWhereCondition . '
					GROUP BY a.name, a.id, ra.property_id, a.details-->\'' . CLocaleContainer::createService()->getLocaleCode() . '-->name\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllAmenityRateAssociationsByAmenityTypeIdsByPropertyIdByCid( $arrintAmenityTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAmenityTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ra.*, 
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					WHERE
						ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
						AND amenity_type_id IN ( ' . implode( ',', $arrintAmenityTypeIds ) . ' ) 
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
					ORDER BY 
						' . $objDatabase->getCollateSort( 'a.name' );

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchAmenitiesByPropertyIdByCid( $intPropertyId, $intUnitSpaceId, $intCid, $objDatabase ) {
		$strSql = '(SELECT
    				    a.*,
    				    ra.ar_cascade_id
					    FROM amenities a
					    JOIN  rate_associations ra ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id ) 
					    WHERE 
					     a.amenity_type_id IN( ' . CAmenityType::APARTMENT . ' )
					     AND ra.cid = ' . ( int ) $intCid . '
					     AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.is_itemized = TRUE
						AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . '
					     order by ra.order_num LIMIT 7 )
					      UNION ALL
					     (SELECT
    				    a.*,
    				    ra.ar_cascade_id
					    FROM amenities a
					    JOIN  rate_associations ra ON ( ra.cid = a.cid AND ra.ar_origin_reference_id = a.id )
					    WHERE
					     a.amenity_type_id IN( ' . CAmenityType::COMMUNITY . ' )
					     AND ra.cid = ' . ( int ) $intCid . '
					     AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.is_itemized = TRUE
						AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
						AND ra.ar_cascade_reference_id != ' . ( int ) $intUnitSpaceId . '
					     order by ra.order_num LIMIT 7)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedAmenityRateAssociationsWithRatesByArCascadeIdByPropertyFloorplanId( $intArCascadeId, $intPropertyFloorplanId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $intTargetArTriggerId = NULL ) {
		/**
		 * To Do JPA: We will have to modify this function to consider amenities price by lease term.
		 */
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::AMENITY ] );
		$objRateLogsFilter->setMaxArCascadeId( CArCascade::SPACE );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strSelectSql = ' COALESCE( pr.rate_amount, 0 )';
		if( true == valId( $intTargetArTriggerId ) ) {
			$strSelectSql = ' COALESCE( get_normalized_charge_amount( pr.ar_trigger_id, CAST( pr.rate_amount AS INTEGER ), ' . $intTargetArTriggerId . '), 0 )';
		}

		$strSql = 'SELECT
						a.id,
						a.amenity_name,
						COALESCE ( MIN ( a.rate_amount ), 0 ) AS min_amenity_rent,
						COALESCE ( MAX ( a.rate_amount ), 0 ) AS max_amenity_rent
					FROM (
							SELECT
								DISTINCT (a.id),
								util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name,
								pr.ar_trigger_id,
								CASE
									WHEN pr.rate_amount IS NOT NULL THEN ' . $strSelectSql . '
									ELSE NULL
								END AS rate_amount
							FROM
								rate_associations ra
								JOIN amenities a ON (
									ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
									AND ra.is_published = true
									AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . ' )
								JOIN unit_spaces us ON ( us.id = ra.ar_cascade_reference_id AND ra.property_id = us.property_id AND ra.cid = us.cid ' . $strCheckDeletedUnitSpacesSql . ' AND us.unit_exclusion_reason_type_id = ' . CUnitExclusionReasonType::NOT_EXCLUDED . ' )
								LEFT JOIN prospect_rates pr ON ( a.id = pr.ar_origin_reference_id AND a.cid = pr.cid AND ra.property_id = pr.property_id AND us.id = pr.unit_space_id )
							WHERE
								ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
								AND us.property_floorplan_id = ' . ( int ) $intPropertyFloorplanId . '
								AND ra.property_id = ' . ( int ) $intPropertyId . '
								AND ra.cid = ' . ( int ) $intCid . '
						) AS a
					GROUP BY
						a.id,
						a.amenity_name
					ORDER BY
						amenity_name';

		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchPublishedAmenityRateAssociationsByArCascadeIdsByAmenityTypeIdsByPropertyIds( $arrintArCascadeIds, $arrintAmenityTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

	    if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintArCascadeIds ) || false == valArr( $arrintAmenityTypeIds )  ) {
            return NULL;
        }

	    $strSql = 'SELECT
						ra.property_id,
						ra.ar_cascade_reference_id,
						ra.ar_cascade_id,
						util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS amenity_name
					FROM
						rate_associations ra
						JOIN amenities a ON (
							ra.cid = a.cid AND ra.ar_origin_reference_id = a.id
							AND ra.ar_origin_id = ' . ( int ) CArOrigin::AMENITY . '
							AND ra.ar_cascade_id IN ( ' . implode( ',', $arrintArCascadeIds ) . ' )
							AND a.amenity_type_id IN ( ' . implode( ',', $arrintAmenityTypeIds ) . ' )
					 	)
					WHERE
						ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
						AND	ra.cid = ' . ( int ) $intCid . ' 
						AND ra.is_published = TRUE AND ra.is_featured = TRUE 
					ORDER BY ra.order_num';

        return fetchData( $strSql, $objDatabase );
    }

    }
?>