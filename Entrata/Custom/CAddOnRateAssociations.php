<?php

class CAddOnRateAssociations extends CRateAssociations {

	public static function fetchRateAssociationByAddOnIdByPropertyIdByCid( $intAddOnId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intAddOnId;

		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchRateAssociationsByAddOnIdsByPropertyIdByCid( $arrintAddOnIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintAddOnIds ) ) return NULL;

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id 	= ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::PROPERTY . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
						AND ra.ar_origin_reference_id IN ( ' . implode( ',', $arrintAddOnIds ) . ' )';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchRateAssociationsByAddOnIdPropertyIdByCid( $intAddOnId, $intPropertyId, $intCid, $objDatabase ) {

		if( true == is_null( $intAddOnId ) ) return NULL;

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id 	= ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intAddOnId;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchCustomRateAssociationsByUnitSpaceIdsByPropertyIdByCid( $arrintUnitSpaceIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;

		$strSql = 'SELECT
						ra.*,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						util_get_translated( \'description\', ao.description, aog.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_description,
						aog.name as add_on_group_name,
						util_get_translated( \'description\', aog.description, aog.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_group_description,
						aog.company_media_file_id,
						aog.add_on_type_id
					FROM
						add_ons ao
                    	JOIN add_on_groups aog ON ( ao.cid = aog.cid AND ao.property_id = aog.property_id AND ao.add_on_group_id = aog.id)
                    	JOIN rate_associations ra ON ( ra.cid = ao.cid AND ra.property_id = ao.property_id AND ra.ar_origin_reference_id = ao.id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id 	= ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) CArCascade::SPACE . '
						AND ra.ar_origin_id = ' . ( int ) CArOrigin::ADD_ONS . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )';

		return fetchData( $strSql, $objDatabase );

	}

}
?>