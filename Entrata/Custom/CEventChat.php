<?php

class CEventChat {

	protected $m_intChatId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCompanyUserId;
	protected $m_intUserId;
	protected $m_intChatSourceId;
	protected $m_intChatQueueId;
	protected $m_intChatStatusTypeId;

	protected $m_strChatStartDatetime;
	protected $m_strChatEndDatetime;

	protected $m_boolIsOnSiteAnswered;

	public function setValues( $arrmixValues ) {

		if( true == isset( $arrmixValues['chat_id'] ) )				$this->m_intChatId = $arrmixValues['chat_id'];
		if( true == isset( $arrmixValues['cid'] ) )					$this->m_intCid = $arrmixValues['cid'];
		if( true == isset( $arrmixValues['property_id'] ) )			$this->m_intPropertyId = $arrmixValues['property_id'];
		if( true == isset( $arrmixValues['company_user_id'] ) )		$this->m_intCompanyUserId = $arrmixValues['company_user_id'];
		if( true == isset( $arrmixValues['user_id'] ) )				$this->m_intUserId = $arrmixValues['user_id'];
		if( true == isset( $arrmixValues['chat_source_id'] ) )		$this->m_intChatSourceId = $arrmixValues['chat_source_id'];
		if( true == isset( $arrmixValues['chat_queue_id'] ) )		$this->m_intChatQueueId = $arrmixValues['chat_queue_id'];
		if( true == isset( $arrmixValues['chat_status_type_id'] ) )	$this->m_intChatStatusTypeId = $arrmixValues['chat_status_type_id'];
		if( true == isset( $arrmixValues['chat_start_datetime'] ) )	$this->m_strChatStartDatetime = $arrmixValues['chat_start_datetime'];
		if( true == isset( $arrmixValues['chat_end_datetime'] ) )	$this->m_strChatEndDatetime = $arrmixValues['chat_end_datetime'];
		if( true == isset( $arrmixValues['is_on_site_answered'] ) )	$this->m_boolIsOnSiteAnswered = $arrmixValues['is_on_site_answered'];
	}

	/**
	 * Setter functions.
	 */

	public function setChatId( $intChatId ) {
		$this->m_intChatId = $intChatId;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setChatSourceId( $intChatSourceId ) {
		$this->m_intChatSourceId = $intChatSourceId;
	}

	public function setChatQueueId( $intChatQueueId ) {
		$this->m_intChatQueueId = $intChatQueueId;
	}

	public function setChatStatusTypeId( $intChatStatusTypeId ) {
		$this->m_intChatStatusTypeId = $intChatStatusTypeId;
	}

	public function setChatStartDatetime( $strChatStartDatetime ) {
		$this->m_strChatStartDatetime = $strChatStartDatetime;
	}

	public function setChatEndDatetime( $strChatEndDatetime ) {
		$this->m_strChatEndDatetime = $strChatEndDatetime;
	}

	public function setIsOnSiteAnswered( $boolIsOnSiteAnswered ) {
		$this->m_boolIsOnSiteAnswered = $boolIsOnSiteAnswered;
	}

	/**
	 * Gettter Functions.
	 */

	public function getChatId() {
		return $this->m_intChatId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getChatSourceId() {
		return $this->m_intChatSourceId;
	}

	public function getChatQueueId() {
		return $this->m_intChatQueueId;
	}

	public function getChatStatusTypeId() {
		return $this->m_intChatStatusTypeId;
	}

	public function getChatStartDatetime() {
		return $this->m_strChatStartDatetime;
	}

	public function getChatEndDatetime() {
		return $this->m_strChatEndDatetime;
	}

	public function getIsOnSiteAnswered() {
		return $this->m_boolIsOnSiteAnswered;
	}

	public function toArray() {
		$arrmixValues = [
			'chat_id'				=> $this->getChatId(),
			'cid'					=> $this->getCid(),
			'property_id'			=> $this->getPropertyId(),
			'company_user_id'		=> $this->getCompanyUserId(),
			'user_id'				=> $this->getUserId(),
			'chat_source_id'		=> $this->getChatSourceId(),
			'chat_queue_id'			=> $this->getChatQueueId(),
			'chat_status_type_id'	=> $this->getChatStatusTypeId(),
			'chat_start_datetime'	=> $this->getChatStartDatetime(),
			'chat_end_datetime'		=> $this->getChatEndDatetime(),
			'is_on_site_answered'	=> $this->getIsOnSiteAnswered()
		];

		return $arrmixValues;
	}

}