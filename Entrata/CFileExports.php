<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileExports
 * Do not add any new functions to this class.
 */

class CFileExports extends CBaseFileExports {

	public static function fetchCustomCurrentExportedFileExportsByFileExportTypeIdsByCid( $arrintFileExportTypeIds, $intCId, $objDatabase ) {

		if( false == valArr( $arrintFileExportTypeIds ) ) return NULL;

		$strSql = ' SELECT
						fe.*,
						util_get_translated( \'batch_name\', fb.batch_name, fb.details ) as batch_name,
						fb.search_filter,
						CONCAT ( ce.name_first, \' \', ce.name_last) as created_by_fullname,
						cu.username
					FROM
						file_exports fe
						JOIN file_batches fb ON ( fb.id = fe.file_batch_id AND fe.cid = fb.cid )
						LEFT JOIN company_users cu ON ( cu.cid = fe.cid AND cu.id = fe.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						fe.cid = ' . ( int ) $intCId . '
						AND fe.file_export_type_id IN ( ' . implode( ',', $arrintFileExportTypeIds ) . ' )
						AND fe.expires_on IS NULL
						AND fe.deleted_on IS NULL
						ORDER BY fe.created_on DESC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomFileExportByIdByCid( $intId, $intCid, $objDatabase ) {

		if( true == is_null( $intId ) ) return NULL;

		$strSql = ' SELECT
						fe.*,
						fb.batch_name,
						fb.search_filter,
						fb.archive_file_name,
						fb.archive_file_path,
						fb.archive_file_size,
						fb.file_count,
						fb.search_filter
					FROM
						file_exports fe
						JOIN file_batches fb ON ( fb.id = fe.file_batch_id AND fe.cid = fb.cid )
					WHERE
						fe.cid = ' . ( int ) $intCid . '
						AND fe.id = ' . ( int ) $intId;

		return parent::fetchFileExport( $strSql, $objDatabase );
	}

	public static function fetchCustomPendingFileExportByFileExportTypeId( $intFileExportTypeId, $objDatabase ) {
		$strSql = ' SELECT
						fe.*,
						fb.batch_name
					FROM
						file_exports fe
						JOIN file_batches fb ON( fe.file_batch_id = fb.id AND fe.cid = fb.cid )
					WHERE
						is_exported = 0
						AND file_export_type_id = ' . ( int ) $intFileExportTypeId;

		return parent::fetchFileExports( $strSql, $objDatabase );
	}

	public static function fetchFileExportsByCidByExportFilter( $intCid, $arrmixDocumentExportSearchFilter, $arrintNotPermittedEntityTypes, $intPage, $intOffSet, $objDatabase ) {
		$arrmixSqlFilters	= [ 'select' => [], 'join' => [], 'where' => [] ];
		$strEntityTypeId	= ( true == valStr( $arrmixDocumentExportSearchFilter['entity_type_id'] ) ) ? $arrmixDocumentExportSearchFilter['entity_type_id'] : NULL;

		if( true == valId( $strEntityTypeId ) ) {
			if( CEntityType::RESIDENT == $strEntityTypeId && false == $arrmixDocumentExportSearchFilter['is_document_management'] ) {
				$arrmixSqlFilters['where'][] = '( ( fm.data->>\'entity_type_id\' )::INTEGER = ' . CEntityType::RESIDENT . ' OR ( fm.data->>\'entity_type_id\')::INTEGER = ' . CEntityType::LEAD . ' )';
			} elseif( CEntityType::BLUE_MOON_DOCUMENTS == $strEntityTypeId ) {
				$arrmixSqlFilters['where'][] = 'fm.data->>\'external_source_system\' = \'BLUEMOON_SERVICES\' ';
			} else {
				$arrmixSqlFilters['where'][] = '( fm.data->>\'entity_type_id\' )::INTEGER = ' . $strEntityTypeId;
			}

			if( CEntityType::RESIDENT == $strEntityTypeId ) {
				$strLeaseStatusTypeIds = '\'' . CLeaseStatusType::PAST . '\',\'' . CLeaseStatusType::CANCELLED . '\',\'' . CLeaseStatusType::APPLICANT . '\'';

				$arrmixSqlFilters['where'][] = ' CASE
				      WHEN
				      	fm.data?\'lease_status_type_id\'
				      THEN
				      	fm.data->>\'lease_status_type_id\' NOT IN (' . $strLeaseStatusTypeIds . ')
				      ELSE
				      TRUE
				  END';
			}

			if( CEntityType::LEAD == $strEntityTypeId ) {
				$arrintExcludeApplicationStageStatusIds = [
					CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
					CApplicationStage::PRE_QUALIFICATION	=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
					CApplicationStage::APPLICATION 			=> [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
					CApplicationStage::LEASE 				=> [ CApplicationStatus::APPROVED ]
				];

				$arrmixSqlFilters['where'][] = '( fm.data->>\'application_stage_id\', fm.data->>\'application_status_id\' ) NOT IN ( ' . sqlStrMultiImplode( $arrintExcludeApplicationStageStatusIds ) . ') ';
			}
		}

		if( true == valStr( $arrmixDocumentExportSearchFilter['start_date'] ) && true == CValidation::validateDate( $arrmixDocumentExportSearchFilter['start_date'] ) ) {
			$strMinDate = '\'' . date( 'm/d/Y', strtotime( $arrmixDocumentExportSearchFilter['start_date'] ) ) . ' 00:00:00' . '\'';
			$arrmixSqlFilters['where'][] = 'fe.updated_on >= ' . $strMinDate;
		}

		if( true == valStr( $arrmixDocumentExportSearchFilter['end_date'] ) && true == CValidation::validateDate( $arrmixDocumentExportSearchFilter['end_date'] ) ) {
			$strMaxDate = '\'' . date( 'm/d/Y', strtotime( $arrmixDocumentExportSearchFilter['end_date'] ) ) . ' 12:59:59' . '\'';
			$arrmixSqlFilters['where'][] = 'fe.updated_on <= ' . $strMaxDate;
		}

		if( true == valStr( $arrmixDocumentExportSearchFilter['document_name'] ) ) {
			$arrmixSqlFilters['where'][] = ' fb.batch_name like \'%' . trim( addslashes( $arrmixDocumentExportSearchFilter['document_name'] ) ) . '%\'';
		}

		$strSql = '		SELECT
							DISTINCT
							fe.id as file_export_id,
							fb.id as file_batch_id,
							fb.archive_file_size,
							fb.search_filter,
							fe.exported_on,
							fe.export_details,
							util_get_translated( \'batch_name\', fb.batch_name, fb.details ),
							fb.cid,
							string_agg(DISTINCT fm.entity_type_id::text, \',\') as entities,
							string_agg(DISTINCT fm.data->>\'external_source_system\', \',\') as external_source_system,
							util_get_translated( \'name\', fes.name, fes.details ) as file_export_statuse,
							fes.id as file_export_status_id,
							fb.file_count,
							COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) as exported_by_username,
							fe.created_on,
							MIN(fm.entity_id),
							MIN(util_get_translated( \'entity_name\', fm.entity_name, fm.details ))
						FROM
							file_exports fe
							JOIN file_batches fb ON ( fb.id = fe.file_batch_id AND fe.cid = fb.cid )
							JOIN file_export_statuses fes ON ( fes.id = fe.file_export_status_id )
							LEFT JOIN file_batch_files fbf ON ( fbf.file_batch_id = fb.id AND fbf.cid = fb.cid )
							LEFT JOIN file_metadatas fm ON ( fm.file_id = fbf.file_id AND fm.cid = fbf.cid ) ';

		$strSql .= '	LEFT JOIN company_users cu ON ( ( cu.cid = fe.cid ) AND ( cu.id = fb.created_by ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ( ce.cid = fe.cid ) AND ( ce.id = fb.created_by ) )

						WHERE
							fe.cid = ' . ( int ) $intCid . '
							AND fe.file_export_type_id = ' . CFileExportType::DOCUMENT_MANAGEMENT_EXPORT . ' ' . ( ( true == valArr( $arrmixSqlFilters['where'] ) ) ? ' AND ' . implode( ' AND ', $arrmixSqlFilters['where'] ) : '' ) . '
							AND fbf.deleted_by IS NULL AND fbf.deleted_on IS NULL';

		$strSql .= ' 	GROUP BY
							fe.id,
						    fb.id,
						    fb.archive_file_size,
						    fe.exported_on,
						    fe.export_details,
						    fb.batch_name,
							fb.cid,
						    fes.name,
						    fes.id,
						    COALESCE(ce.name_first || \' \' || ce.name_last, cu.username),
						    fe.created_on ';
		if( true == valArr( $arrintNotPermittedEntityTypes ) ) {

			$strSql .= ' HAVING count(fm.id) = sum( CASE WHEN ( ';
			$strSql .= ' ( ( fm.data->>\'entity_type_id\' )::INTEGER NOT IN ( ' . implode( ',', $arrintNotPermittedEntityTypes ) . ' ) ) ';

			if( true == in_array( CEntityType::BLUE_MOON_DOCUMENTS,  $arrintNotPermittedEntityTypes ) ) {
				$strSql .= ' AND fm.file_id NOT IN ( SELECT file_id from file_metadatas where data->>\'external_source_system\' = \'BLUEMOON_SERVICES\' ) ) ';
			} else {
				$strSql .= ' OR ( fm.data->>\'external_source_system\' = \'BLUEMOON_SERVICES\' ) ) ';
			}
			$strSql .= ' THEN 1 ELSE 0
						END ) ';
		}

		$strSql .= ' ORDER BY fe.created_on DESC ';

		$strSql .= ' ' . ( ( true == is_numeric( $intPage ) && true == is_numeric( $intOffSet ) ) ? ' OFFSET ' . ( int ) $intOffSet . ' LIMIT ' . ( int ) $intPage : '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExportedFileExportByCidByFileExportTypeIdByCompanyUserId( $strSearchFilter, $intFileExportTypeId, $intCid, $intCompanyUserId, $objDatabase ) {

		if( true == is_null( $strSearchFilter ) ) return NULL;

		$strSql = 'SELECT
						fe.*
					FROM
						file_exports fe
						LEFT JOIN file_batches fb ON ( fb.id = fe.file_batch_id AND fe.cid = fb.cid  )
					WHERE
						fb.search_filter = \'' . $strSearchFilter . '\'
						AND fe.is_exported = 1
						AND fe.expires_on IS NULL
						AND fe.file_export_type_id = ' . ( int ) $intFileExportTypeId . '
						AND fe.cid = ' . ( int ) $intCid . '
						AND fe.created_by = ' . ( int ) $intCompanyUserId . '
					LIMIT 1';

		return parent::fetchFileExport( $strSql, $objDatabase );
	}

	public static function fetchCustomCurrentExportedFileExportsByFileExportTypeIdByCidByCompanyUserId( $strSearchFilter, $intFileExportTypeId, $intCid, $intCompanyUserId, $objDatabase ) {

		if( true == is_null( $strSearchFilter ) ) return NULL;

		$strSql = ' SELECT
						fe.*,
						util_get_translated( \'batch_name\', fb.batch_name, fb.details ) as  batch_name,
						fb.file_count,
						 COALESCE(NULLIF(CONCAT(ce.name_first, \' \', ce.name_last), \' \'), cu.username) as created_by_fullname
					FROM
						file_exports fe
						JOIN file_batches fb ON ( fb.id = fe.file_batch_id AND fe.cid = fb.cid )
						LEFT JOIN company_users cu ON ( cu.cid = fe.cid AND cu.id = fe.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						fb.search_filter = \'' . $strSearchFilter . '\'
						AND fe.cid = ' . ( int ) $intCid . '
						AND fe.file_export_type_id = ' . ( int ) $intFileExportTypeId . '
						AND fe.expires_on IS NULL
						AND fe.is_exported = 1
						AND fe.created_by = ' . ( int ) $intCompanyUserId . '
					ORDER BY fe.created_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPendingFileExportByIdByFileExportTypeIdByCid( $intId, $intFileExportTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM file_exports WHERE cid = ' . ( int ) $intCid . ' AND is_exported = 0 AND id = ' . ( int ) $intId . ' AND file_export_type_id = ' . ( int ) $intFileExportTypeId;

		return parent::fetchFileExport( $strSql, $objDatabase );
	}

	public static function fetchCustomUpdateQueuedExportsByFileExportTypeId( $intFileExportTypeId, $objDatabase ) {

		$strSql = '	SELECT
						fe.*
					FROM
						file_exports fe
					WHERE
						fe.cid > 0
						AND fe.file_export_status_id = 1
						AND fe.updated_on >= ( NOW() - INTERVAL \'1 month\' )
		    			AND fe.deleted_on IS NULL
						AND fe.file_export_type_id = ' . ( int ) $intFileExportTypeId . '
					FOR UPDATE;';

		return parent::fetchFileExports( $strSql, $objDatabase );
	}

	public static function fetchSearchFilterByFileExportIdByCid( $intFileExportId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						fb.search_filter,
						fb.archive_file_size,
						fb.file_count,
						fe.exported_on,
						fe.created_on,
						CONCAT ( ce.name_first, \' \', ce.name_last) as created_by_fullname,
						cu.username
					FROM
						file_exports fe
						JOIN file_batches fb ON ( fb.id = fe.file_batch_id AND fe.cid = fb.cid )
						LEFT JOIN company_users cu ON ( cu.cid = fe.cid AND cu.id = fe.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						fe.cid = ' . ( int ) $intCid . '
						AND fe.id = ' . ( int ) $intFileExportId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPendingFileExportByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						fe.*
					FROM
						file_exports fe
						JOIN file_batches fb ON (fe.file_batch_id = fb.id and fe.cid = fb.cid )
					WHERE
						fe.cid = ' . ( int ) $intCid . '
						AND fe.is_exported = 0
						AND fe.file_export_type_id = ' . CFileExportType::BULK_DOCUMENT_EXPORT . '
						AND fb.search_filter IS NOT NULL
						AND fb.search_filter <> \'leads\'
						AND fb.search_filter <> \'residents\'';

		return self::fetchFileExports( $strSql, $objDatabase );
	}

	public static function fetchCustomScheduledFileExportsByExportTypeId( $intExportTypeId, $objDatabase ) {

		$strSql = 'SELECT
						fe.*
					FROM
						file_exports fe
						JOIN file_batches fb ON (fe.file_batch_id = fb.id and fe.cid = fb.cid )
					WHERE fe.is_exported = 0
						AND fe.file_export_type_id = ' . ( int ) $intExportTypeId . '
						AND fb.search_filter IS NOT NULL
						AND fe.file_export_status_id <> ' . CFileExportStatus::FAILED . '
						AND fe.expires_on IS NULL
						AND fe.deleted_on IS NULL
					ORDER BY 
						fe.created_on';

		return self::fetchFileExports( $strSql, $objDatabase );
	}

	public static function fetchExpiredFileExportsByFileExportTypeIdsByCid( $arrintFileExportTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileExportTypeIds ) ) return NULL;

		$strSql = ' SELECT
 						fe.*,
 						CONVERT_FROM( DECODE( fb.search_filter, \'BASE64\' ), \'UTF-8\' )::json->\'delivery_method\' as delivery_method,
 						util_get_translated( \'batch_name\', fb.batch_name, fb.details ) as batch_name,
 						CONCAT ( ce.name_first, \' \', ce.name_last) as created_by_fullname,
 						cu.username
 					FROM
 						file_exports fe
 						JOIN file_batches fb ON ( fb.id = fe.file_batch_id AND fe.cid = fb.cid )
 						LEFT JOIN company_users cu ON ( cu.cid = fe.cid AND cu.id = fe.created_by AND cu.company_user_type_id IN ( ' . implode( ' ,', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
 						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
 					WHERE
						fe.cid = ' . ( int ) $intCid . '
						AND fe.file_export_type_id IN ( ' . implode( ',', $arrintFileExportTypeIds ) . ' )
						AND fe.expires_on IS NOT NULL
					ORDER BY fe.created_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomFileExportByFileBatchIdByCid( $intFileBatchId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						file_exports
					WHERE
						cid = ' . ( int ) $intCid . '
						AND file_batch_id = ' . ( int ) $intFileBatchId . '
						AND expires_on IS NULL
						AND deleted_on IS NULL';

		return self::fetchFileExport( $strSql, $objDatabase );
	}

}
?>