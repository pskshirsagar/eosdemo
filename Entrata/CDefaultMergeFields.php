<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultMergeFields
 * Do not add any new functions to this class.
 */

class CDefaultMergeFields extends CBaseDefaultMergeFields {

	public static function fetchAllDefaultMergeFields( $objDatabase ) {
		return self::fetchDefaultMergeFields( 'SELECT * FROM default_merge_fields', $objDatabase );
	}

	public static function fetchDefaultMergeFieldsByFieldsKeyedByFields( $arrstrFields, $objDatabase ) {
		if( false == valArr( $arrstrFields ) ) return NULL;

		$strSql = 'SELECT * FROM default_merge_fields WHERE field IN (\'' . implode( "','", $arrstrFields ) . '\')';
        $arrobjDefaultMergeFields = self::fetchDefaultMergeFields( $strSql, $objDatabase );

		if( true == valArr( $arrobjDefaultMergeFields ) ) {
			$arrobjRekeyedDefaultMergeFields = [];

			foreach( $arrobjDefaultMergeFields as $objDefaultMergeField ) {

				$objBlockDefaultMergeField = NULL;

				if( true == is_null( $objDefaultMergeField->getBlockDefaultMergeFieldId() ) ) {
					$arrobjRekeyedDefaultMergeFields[$objDefaultMergeField->getField()] = $objDefaultMergeField;
				} elseif( true == array_key_exists( $objDefaultMergeField->getBlockDefaultMergeFieldId(), $arrobjDefaultMergeFields ) ) {
					$objBlockDefaultMergeField = $arrobjDefaultMergeFields[$objDefaultMergeField->getBlockDefaultMergeFieldId()];
					$arrobjRekeyedDefaultMergeFields[$objBlockDefaultMergeField->getField() . '~' . $objDefaultMergeField->getField()] = $objDefaultMergeField;
				}
			}

			$arrobjDefaultMergeFields = $arrobjRekeyedDefaultMergeFields;
		}

		return $arrobjDefaultMergeFields;
	}

	public static function fetchDefaultMergeFieldsByIds( $arrintDefaultMergeFieldIds, $objDatabase ) {

	    if( false == valArr( $arrintDefaultMergeFieldIds ) ) return [];

	    $strSql = 'SELECT * FROM default_merge_fields WHERE id IN ( ' . implode( ',', $arrintDefaultMergeFieldIds ) . ')';

	    return self::fetchDefaultMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDefaultMergeFieldsGroupByMergeFieldId( $objDatabase, $arrintExcludeMergeFieldGroups = NULL, $arrstrExcludeMergeFields = NULL ) {

		$strCondition = '';

		if( true == valArr( $arrintExcludeMergeFieldGroups ) ) {
			$strCondition .= ' WHERE mfg.id NOT IN (' . implode( ',', $arrintExcludeMergeFieldGroups ) . ')';
		}

		if( true == valArr( $arrstrExcludeMergeFields ) ) {
			$strCondition .= ' WHERE dmf.field NOT IN (\'' . implode( '\',\'', $arrstrExcludeMergeFields ) . '\' )';
		}

		$strSql = 'SELECT
		              	dmf.id,
						util_get_translated( \'name\', mfg.name, mfg.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS name,
						dmf.field,
						dmf.is_block_field,
						dmf.block_default_merge_field_id,
						mfg.id as merge_field_group_id
                    FROM
		              	default_merge_fields dmf
				    	JOIN merge_field_groups mfg ON ( mfg.id = dmf.merge_field_group_id AND mfg.is_external = 0 )
						' . $strCondition . '
                    GROUP BY
						mfg.id,
						mfg.name,
						dmf.field,
						dmf.id
                    ORDER BY
						mfg.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	// This function will not work until merge_field_dependency_groups table is implemented

	public static function writeDefaultMergeFieldValues( $arrmixMergeFields, $intCid, $intUserId, $objDatabase ) {
        $arrobjDefaultMergeFields = CDefaultMergeFields::fetchDefaultMergeFieldsByFieldsKeyedByFields( array_keys( $arrmixMergeFields, $objDatabase ) );

	    $arrmixInsertingMergeFields = array_filter( $arrmixMergeFields, function( $arrmixMergeFieldValues ) {
            return is_null( $arrmixMergeFieldValues['referenceId'] );
        } );
        $arrmixInsertingMergeFields = array_intersect_keys( $arrmixInsertingMergeFields, $arrobjDefaultMergeFields );
        $arrintMergeFieldDependencyGroupIds = array_reduce( array_intersect_keys( $arrobjDefaultMergeFields, $arrmixInsertingMergeFields ),
            function( $arrintMergeFieldDependencyGroupIds, $objDefaultMergeField ) {
                if( false == in_array( $objDefaultMergeField->getMergeFieldDependencyGroupId(), $arrintMergeFieldDependencyGroupIds ) ) {
                    $arrintMergeFieldDependencyGroupIds[] = $objDefaultMergeField->getMergeFieldDependencyGroupId();
                }
            }, [] );
        $arrmixMergeFieldDependencyGroups = CMergeFieldDependencyGroups::fetchMergeFieldDependencyGroupArrayByMergeFieldDependencyGroupIds( $arrintMergeFieldDependencyGroupIds, $objDatabase );
        foreach( $arrmixMergeFieldDependencyGroups as &$arrmixMergeFieldDependencyGroupMembers ) {
            if( $arrmixMergeFieldDependencyGroupMembers['required'] != array_intersect_keys( $arrmixMergeFieldDependencyGroupMembers['required'], $arrmixInsertingMergeFields ) ) {
                $arrmixMergeFieldDependencyGroupMembers = NULL;
            }
        }
        unset( $arrmixMergeFieldDependencyGroupMembers );
        $arrmixMergeFieldDependencyGroups = array_filter( $arrmixMergeFieldDependencyGroups );
        foreach( $arrmixMergeFieldDependencyGroups as $strTableName => $arrmixMergeFieldDependencyGroupMembers ) {
            $strSql = 'INSERT INTO ' . $strTableName . ' ( cid, created_on, created_by';
            $strInsertColumns = '';
            $strInsertValues = 'VALUES (' . ( int ) $intCid . ', NOW(), ' . ( int ) $intUserId;
            foreach( $arrmixMergeFieldDependencyGroupMembers['required'] as $strMergeField ) {
                $strInsertColumns .= ', ' . $arrobjDefaultMergeFields[$strMergeField]->getEditExpression()['column'];
                $strInsertValues .= ', ' . $arrmixInsertingMergeFields[$strMergeField]['value'];
            }
            foreach( $arrmixMergeFieldDependencyGroupMembers['member'] as $strMergeField ) {
                if( true == array_key_exists( $strMergeField, $arrmixInsertingMergeFields ) ) {
                    $strInsertColumns .= ', ' . $arrobjDefaultMergeFields[$strMergeField]->getEditExpression()['column'];
                    $strInsertValues .= ', ' . $arrmixInsertingMergeFields[$strMergeField]['value'];
                }
            }
            $strInsertColumns .= ') ';
            $strInsertValues .= ') ';
            $strSql .= $strInsertColumns . $strInsertValues;
            executeSql( $strSql, $objDatabase );
        }

        $arrmixUpdatingMergeFields = array_diff_key( $arrmixMergeFields, $arrmixInsertingMergeFields );
        $arrmixUpdatingMergeFields = array_intersect_keys( $arrmixUpdatingMergeFields, $arrobjDefaultMergeFields );

        foreach( $arrmixUpdatingMergeFields as $strMergeFieldName => $arrmixMergeFieldValues ) {
            $strSql = 'UPDATE ' . $arrobjDefaultMergeFields[$strMergeFieldName]->getEditExpression()['table'] . '
                       SET ' . $arrobjDefaultMergeFields[$strMergeFieldName]->getEditExpression()['column'] . ' = ' . $arrmixMergeFieldValues['value'] . ',
                       updated_by = ' . ( int ) $intUserId . ',
                       updated_on = NOW()
                       WHERE cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $arrmixMergeFieldValues['referenceId'];
            executeSql( $strSql, $objDatabase );
        }
    }

    public static function mapDefaultMergeFields( $arrobjMergeFields, $arrstrFilterParameters, $objPrimaryObject, $objDatabase ) {
        if( false == valArr( $arrobjMergeFields ) ) return false;
        $arrstrValues = [];
        foreach( $arrobjMergeFields as $objMergeField ) {
	        if( $objMergeField->getMergeFieldTypeId() == CMergeFieldType::BLOCK_MERGE_FIELD ) {
                $arrstrValues[$objMergeField->getField()] = $objMergeField->fetchFilteredBlockMergeSubFieldValues( $objPrimaryObject, $arrstrFilterParameters[$objMergeField->getField()], $objDatabase );
            } else {
                $arrstrValues[$objMergeField->getField()] = $objMergeField->fetchValue( $objPrimaryObject, $objDatabase );
            }
        }
        return $arrstrValues;
    }

	public static function fetchDefaultMergeFieldsWithGroupNameByMergeFieldGroupIdsByRequiredInputTables( $arrintMergeFieldGroupIds, $arrstrRequiredInputTables, $objDatabase ) {

		if( false == valArr( $arrintMergeFieldGroupIds ) || false == valArr( $arrstrRequiredInputTables ) ) return NULL;

		$strSql = 'SELECT
						dmf.id,
						dmf.merge_field_group_id,
						dmf.field,
						util_get_translated( \'title\', dmf.title, dmf.details ) AS title,
						dmf.data_type,
						util_get_translated( \'name\', mfg.name, mfg.details ) AS name
					FROM
						default_merge_fields dmf
						JOIN merge_field_groups mfg ON ( mfg.id = dmf.merge_field_group_id )
					WHERE
						dmf.merge_field_group_id IN ( ' . implode( ',', $arrintMergeFieldGroupIds ) . ' )
						AND dmf.required_input_table IN ( ' . '\'' . implode( '\',\'', $arrstrRequiredInputTables ) . '\'' . ' )
						AND dmf.block_default_merge_field_id IS NULL
						AND dmf.is_block_field = 0
						AND dmf.show_in_merge_display = 1
					ORDER BY mfg.name, dmf.field, dmf.title';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultMergeFieldsWithGroupNameByMergeFieldGroupId( $intMergeFieldGroupId, $objDatabase ) {

		if( false == is_int( $intMergeFieldGroupId ) ) return NULL;

		$strSql = 'SELECT
						dmf.id,
						dmf.merge_field_group_id,
						dmf.field,
						dmf.default_value,
						util_get_translated( \'title\', dmf.title, dmf.details ) AS title,
						dmf.data_type,
						mfg.id,
						util_get_translated( \'name\', mfg.name, mfg.details ) AS name
					FROM
						default_merge_fields dmf
						JOIN merge_field_groups mfg ON ( mfg.id = dmf.merge_field_group_id )
					WHERE
						dmf.merge_field_group_id = ' . ( int ) $intMergeFieldGroupId . '
						AND dmf.block_default_merge_field_id IS NULL
						AND dmf.is_block_field = 0
						AND dmf.show_in_merge_display = 1
					ORDER BY mfg.name, dmf.field, dmf.title';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultMergeFiledGroupCountByFieldsByMergeFieldGroupIds( $arrstrFields, $arrintMergeFieldGroupIds, $objDatabase ) {

		if( false == valArr( $arrstrFields ) || false == valArr( $arrintMergeFieldGroupIds ) ) return NULL;

		$strSql = 'SELECT
						count( dmf.field ) as CountMergeFields
					FROM
						default_merge_fields dmf 
					WHERE
						dmf.field IN ( ' . implode( ',', $arrstrFields ) . ' ) AND dmf.merge_field_group_id IN ( ' . implode( ',', $arrintMergeFieldGroupIds ) . ' )
						AND dmf.required_input_table IS NOT NULL
						AND dmf.view_expression IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedDefaultMergeFieldsWithGroupName( $objDatabase, $objPagination, $arrobjMergeFieldsFilter, $boolIsCount = false ) {

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strOrderByType = ( 0 == $objPagination->getOrderByType() ) ? ' ASC ' : ' DESC ';
			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit = ( int ) $objPagination->getPageSize();
		}
		$strMergeFieldIdForWhereClause       = $arrobjMergeFieldsFilter['id'] ?? 0;
		$strMergeFieldNameForWhereClause     = $arrobjMergeFieldsFilter['name'] ?? '';
		$strMergeFieldGroupIdForWhereClause  = $arrobjMergeFieldsFilter['merge_field_group'] ?? 0;
		$strBlockMergeFieldIdForWhereClause  = $arrobjMergeFieldsFilter['block_default_merge_field'] ?? 0;
		if( false === is_null( $strMergeFieldGroupIdForWhereClause ) && 0 < strlen( $strMergeFieldGroupIdForWhereClause ) ) {
			$strMergeFieldSubGroupIdForWhereClause = $arrobjMergeFieldsFilter['merge_field_sub_groups'] ?? 0;
		}

		$strMergeFieldIdWhereClause          = ( true == valId( $strMergeFieldIdForWhereClause ) ? ' dmf.id =  ' . ( int ) $strMergeFieldIdForWhereClause : '' );
		$strMergeFieldNameWhereClause        = ( true == valStr( $strMergeFieldNameForWhereClause ) ? ' dmf.field ilike  \'%' . $strMergeFieldNameForWhereClause . '%\'' : ' ' );
		$strMergeFieldGroupWhereClause       = ( true == valId( $strMergeFieldGroupIdForWhereClause ) ? 'WHERE  dmf.merge_field_group_id =  ' . ( int ) $strMergeFieldGroupIdForWhereClause : '' );
		$strMergeFieldSubGroupWhereClause    = ( true == valId( $strMergeFieldSubGroupIdForWhereClause ) ? ' AND dmf.merge_field_sub_group_id =  ' . ( int ) $strMergeFieldSubGroupIdForWhereClause : '' );
		$strBlockMergeFieldWhereClause       = ( true == valId( $strBlockMergeFieldIdForWhereClause ) ? ' dmf.block_default_merge_field_id =  ' . ( int ) $strBlockMergeFieldIdForWhereClause : '' );

		if( true == valId( $strBlockMergeFieldIdForWhereClause ) ) {
			$strBlockMergeFieldWhereClause       = ( true == valId( $strMergeFieldGroupIdForWhereClause ) ) ? ( ' AND ' . $strBlockMergeFieldWhereClause ) : ( 'WHERE ' . $strBlockMergeFieldWhereClause );
		}
		if( true == valId( $strMergeFieldIdForWhereClause ) ) {
			$strMergeFieldIdWhereClause = ( true == valId( $strMergeFieldGroupIdForWhereClause ) || true == valId( $strBlockMergeFieldIdForWhereClause ) ) ? ( ' AND ' . $strMergeFieldIdWhereClause ) : ( 'WHERE ' . $strMergeFieldIdWhereClause );
		}
		if( true == valStr( $strMergeFieldNameForWhereClause ) ) {
			$strMergeFieldNameWhereClause = ( true == valId( $strMergeFieldGroupIdForWhereClause ) || true == valId( $strBlockMergeFieldIdForWhereClause ) || true == valId( $strMergeFieldIdForWhereClause ) ) ? ( ' AND ' . $strMergeFieldNameWhereClause ) : ( 'WHERE ' . $strMergeFieldNameWhereClause );
		}

		if( true == $boolIsCount ) {
			$strSql = 'SELECT
						count(dmf.id) ';
		} else {
			$strSql = 'SELECT
						dmf.id,
						dmf.merge_field_group_id,
						dmf.field, 
						dmf.title,
						dmf.allow_user_update,
						dmf.allow_admin_update,
						dmf.allow_origin_update,
						dmf.show_in_merge_display,
						dmf.allow_default_text,
						dmf.is_locked,
						dmf.is_block_field,
						dmf.is_required,
						mfg.id AS group_id,
						mfg.name AS group_name';
		}
		$strSql     .= ' FROM
							default_merge_fields dmf LEFT JOIN merge_field_groups mfg ON ( mfg.id = dmf.merge_field_group_id ) 
						' . $strMergeFieldGroupWhereClause . ' ' . $strMergeFieldSubGroupWhereClause . $strBlockMergeFieldWhereClause . $strMergeFieldIdWhereClause . $strMergeFieldNameWhereClause;

		if( false == $boolIsCount && true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' ORDER BY
							dmf.order_num,
							' . $objPagination->getOrderByField() . ' ' . $strOrderByType .
			           ' OFFSET ' . ( int ) $intOffset .
			           ' LIMIT ' . ( int ) $intLimit;
		}

		$arrstrResultSet = fetchData( $strSql, $objDatabase );
		return ( false == $boolIsCount ) ? $arrstrResultSet : $arrstrResultSet[0]['count'];
	}

	public static function fetchDefaultMergeFieldsBySearchFilter( $arrstrFilteredExplodedSearch, $objDatabase ) {

			$strSql = 'SELECT
							dmf.id,
							dmf.field,
							dmf.title,
							mfg.id AS group_id,
							mfg.name AS group_name 
						FROM
							default_merge_fields dmf 
							LEFT JOIN merge_field_groups mfg ON ( mfg.id = dmf.merge_field_group_id )
						WHERE 
							dmf.field ILIKE \'%' . implode( '%\' OR dmf.field ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							OR dmf.title ILIKE \'%' . implode( '%\' OR dmf.title ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' 
							OR mfg.name ILIKE \'%' . implode( '%\' OR mfg.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						ORDER BY 
							CASE
								When dmf.field = \'' . implode( ' ', $arrstrFilteredExplodedSearch ) . '\' THEN 1
								ELSE 2
							END	
						LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultMergeFieldMaxId( $objDatabase ) {
		$strSql = 'SELECT
						MAX( id ) AS id
					FROM
						default_merge_fields';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllDefaultMergeFieldsDetails( $objDatabase ) {
		$strSql = 'SELECT
						id,
						field
					FROM
						default_merge_fields
					ORDER BY 
						field ASC';

		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchAllDefaultMergeFieldObjects( CDatabase $objDatabase ) : array {
        $strSql = 'SELECT
						id,
						field,
						block_default_merge_field_id
					FROM
						default_merge_fields
					ORDER BY 
						field ASC';

        return self::fetchDefaultMergeFields( $strSql, $objDatabase );
    }

	public static function fetchDefaultMergeFieldsByFields( $arrstrFields, $objDatabase, $boolIsSmsMergeField = false, $arrstrRequiredInputTables = NULL, $boolIsExcludeRequiredInputTables = false ) {
		if( false == valArr( $arrstrFields ) ) return false;
		$strSql = '	SELECT
						*
					FROM
						default_merge_fields
					WHERE
						field IN ( \'' . implode( "','", $arrstrFields ) . '\' ) 
						AND block_default_merge_field_id IS NULL
						AND is_block_field = 0';

		if( true == $boolIsSmsMergeField ) {
			$strSql .= ' AND details->>\'use_for_sms\'::integer = 1';
		}

		if( true == \valArr( $arrstrRequiredInputTables ) ) {
			if( true == $boolIsExcludeRequiredInputTables ) {
				$strSql .= ' AND 
						 		CASE 
						 			WHEN required_input_table IS NULL THEN TRUE
									ELSE required_input_table NOT IN ( ' . sqlStrImplode( $arrstrRequiredInputTables ) . ' ) 
						 		END';
			} else {
				$strSql .= ' AND required_input_table IN ( ' . sqlStrImplode( $arrstrRequiredInputTables ) . ' ) ';
			}
		}

		return self::fetchDefaultMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDefaultMergeFieldCountById( $intId, $objDatabase ) {

		if( false == valId( $intId ) ) return false;

		$strWhere = 'WHERE id = ' . ( int ) $intId;

		return parent::fetchRowCount( $strWhere, 'default_merge_fields', $objDatabase );
	}

    public static function fetchDefaultMergeFieldsByMergeFieldGroupIds( $arrintMergeFieldGroupIds, $objDatabase, $strSearchedKeyword = '', $arrstrExcludingMergeFields = [] ) {
        $strCoditionalSelect   = '';
        $strConditionalJoin    = '';
        $strConditionalOrderBy = '';
        $strSql                = '';

        if( false == valArr( $arrintMergeFieldGroupIds ) ) return NULL;
        if( true == valStr( $strSearchedKeyword ) ) {
            $strSql .= 'WITH filtered_default_merge_field AS (
                                SELECT
                                        id,
                                        block_default_merge_field_id
                                  FROM
                                        default_merge_fields
                                  WHERE
                                        field ILIKE \'%' . $strSearchedKeyword . '%\' or title ILIKE \'%' . $strSearchedKeyword . '%\' 
                           )';

            $strCoditionalSelect = ', util_get_system_translated( \'name\', mfg.name, mfg.details ) AS merge_field_group_name';

            $strConditionalJoin = 'JOIN merge_field_groups mfg ON ( mfg.id = dmf.merge_field_group_id )
                                   JOIN filtered_default_merge_field fdmf ON ( dmf.id = fdmf.id OR dmf.id =  fdmf.block_default_merge_field_id OR dmf.block_default_merge_field_id = fdmf.id  )';
        }

        $strSql .= 'SELECT
                          dmf.* ' . $strCoditionalSelect . '
                      FROM
                          default_merge_fields dmf ' . $strConditionalJoin . '
                     WHERE
                          dmf.merge_field_group_id IN ( ' . implode( ',', $arrintMergeFieldGroupIds ) . ' )';

        if( true == valArr( $arrstrExcludingMergeFields ) ) {
	        $strSql .= ' AND dmf.field NOT IN ( \'' . implode( '\',\'', $arrstrExcludingMergeFields ) . '\' ) ';
        }

        $strSql .= ' Order by dmf.field';

        return self::fetchDefaultMergeFields( $strSql, $objDatabase );
    }

    public static function fetchBlockDefaultMergeFields( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						default_merge_fields
					WHERE
						is_block_field = 1
					ORDER BY 
						field ASC';

		return self::fetchDefaultMergeFields( $strSql, $objDatabase );
	}

	public static function fetchCustomBlockDefaultMergeFields( $arrstrMergeFields, $arrintMergeFieldGroupIds, $objDatabase ) {

		$strSql = 'SELECT
						ds.id AS default_merge_field_id,
						ds.field AS simple_merge_field,
						d.field AS block_merge_field
					FROM
						default_merge_fields d
						JOIN default_merge_fields ds ON ( d.id = ds.block_default_merge_field_id )
					WHERE
						d.is_block_field = 1
						AND d.merge_field_group_id IN ( ' . implode( ',', $arrintMergeFieldGroupIds ) . ' )
						AND d.field IN (\'' . implode( "','", $arrstrMergeFields ) . '\')
				ORDER BY 
						d.field ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultMergeFieldByField( $strField, $objDatabase ) {
		if( false == valStr( $strField ) ) return;

		$strSql = '	SELECT
						*
					FROM
						default_merge_fields
					WHERE
						field = \'' . addslashes( $strField ) . '\'
						AND block_default_merge_field_id IS NULL
						AND is_block_field = 0
						AND required_input_table IS NOT NULL
						AND view_expression IS NOT NULL';

		return self::fetchDefaultMergeField( $strSql, $objDatabase );
	}

	public static function fetchDefaultMergeFieldByFieldName( $strField, $objDatabase ) {
		if( false == valStr( $strField ) ) return;

		$strSql = '	SELECT
						*
					FROM
						default_merge_fields
					WHERE
						field = \'' . addslashes( $strField ) . '\'
						AND block_default_merge_field_id IS NULL
						AND is_block_field = 0
						AND view_expression IS NOT NULL';

		return self::fetchDefaultMergeField( $strSql, $objDatabase );
	}

	public static function fetchDefaultMergeFieldMaxOrderNumByBlockMergeFieldId( $intBlockMergeFieldId, $intMergeFieldGroupId, $intMergeFieldSubGroupId, $objDatabase ) {
		$strSql = 'SELECT
						MAX( order_num ) AS max_order_num
					FROM
						default_merge_fields';
		if( false == is_null( $intBlockMergeFieldId ) ) {
			$strSql .= ' WHERE block_default_merge_field_id = ' . ( int ) $intBlockMergeFieldId;
		}

		if( false == is_null( $intMergeFieldGroupId ) ) {
			$strSql .= ' WHERE merge_field_group_id = ' . ( int ) $intMergeFieldGroupId;
			$strSql .= ( false == is_null( $intMergeFieldSubGroupId ) ) ? ' AND merge_field_sub_group_id = ' . ( int ) $intMergeFieldSubGroupId : '';
		}
		$arrstrResultSet = fetchData( $strSql, $objDatabase );

		return $arrstrResultSet[0]['max_order_num'];
	}

}
?>