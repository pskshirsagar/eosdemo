<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationPreferenceTypes
 * Do not add any new functions to this class.
 */

class CApplicationPreferenceTypes extends CBaseApplicationPreferenceTypes {

	public static function fetchApplicationPreferenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApplicationPreferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApplicationPreferenceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicationPreferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>