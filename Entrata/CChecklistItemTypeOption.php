<?php

class CChecklistItemTypeOption extends CBaseChecklistItemTypeOption {

	const FIRST_MONTH_RENT 						= 1;
	const TOTAL_BALANCE_MOVE_IN					= 2;
	const ADD_ON_RENT_PAID 						= 3;
	const DEPOSIT_COLLECTED 					= 4;
	const MAKE_READY_COMPLETED 					= 5;
	const LEASE_APPROVED_AND_SIGNED 			= 6;
	const APPLICATION_SCREENED_AND_APPROVED 	= 7;
	const CONTACT_INFO_SUBMITTED 				= 8;
	const EMERGENCY_CONTACT_INFO_SUBMITTED 		= 9;
	const VEHICLE_INFO_SUBMITTED 				= 10;
	const RENTERS_INSURANCE_INFO 				= 11;
	const KEYS_DISTRIBUTED 						= 12;
	const INSPECTION_COMPLETED 					= 13;
	const PET_INFO			 					= 14;
	const GUARANTOR_ADDENDUM			 		= 15;
	const MOVE_IN_STATEMENT			 			= 16;
	const FORWARDING_ADDRESS_COLLECTED          = 17;
	const UTILITY_BILLING                       = 18;
	const ELECTRIC                              = 19;
	const DOCUMENTS_PRINTED_AND_SIGNED			= 20;
	const ADD_ON_ADDENDUM			 			= 21;
	const CUSTOM_ACTION			 			    = 22;
	const KEYS_RETURNED			 			    = 23;
	const TOTAL_BALANCE_MOVE_OUT                = 24;

	const FIRST_MONTH_RENT_WITH_SECOND_MONTH_CHARGE      = 'first_month_rent_with_second_month_charge';
	const TOTAL_MOVE_IN_BALANCE_WITH_SECOND_MONTH_CHARGE = 'total_move_in_balance_with_second_month_charge';


	const RESIDENT_PHOTO                        = 'resident_photo';

	const LEASE_AND_APPLICATION_GROUP           = 'lease_and_application';
	const ADDENDA_GROUP                         = 'addenda';
	const CHARGES_GROUP                         = 'charges';
	const OPTIONAL_GROUP                        = 'optional';

	public static $c_arrintAutoCompleteExcludedOptions = [ self::CUSTOM_ACTION, self::DOCUMENTS_PRINTED_AND_SIGNED, self::MOVE_IN_STATEMENT, self::UTILITY_BILLING, self::ELECTRIC, self::GUARANTOR_ADDENDUM, self::KEYS_DISTRIBUTED, self::KEYS_RETURNED ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>