<?php

class CSubsidyPreviousHousingType extends CBaseSubsidyPreviousHousingType {

	const DEFAULT_SUBSIDY_PREVIOUS_HOUSING_TYPE = '3';

	const SUBSIDY_PREVIOUS_HOUSING_TYPE_SUBSTANDARD		        = 1;
	const SUBSIDY_PREVIOUS_HOUSING_TYPE_WITHOUT_HOUSING		    = 2;
	const SUBSIDY_PREVIOUS_HOUSING_TYPE_NO_NIGHTTME_RESIDENCE	= 5;
	const SUBSIDY_PREVIOUS_HOUSING_TYPE_FLEEING_VIOLENCE    	= 6;

	const DISABLED								= 3;
	const US_MILITARY_VETERAN					= 4;
	const HOUSED_TEMPORARILY_DUE_TO_DISASTER	= 5;
	const FRAIL_ELDERLY							= 6;
	const PART_TIME_STUDENT						= 7;
	const POLICE_OR_SECURITY_OFFICER			= 8;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>