<?php

use Psi\Eos\Entrata\CFileAssociations;

class CResidentInsurancePolicy extends CBaseResidentInsurancePolicy {

	const BACK_DATE_DAYS = '-60 days';
	const BACK_DATE_MIN_YEAR = '112';
	const FUTURE_DATE_MAX_YEAR = '15';

	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strAgentName;
	protected $m_strAgentPhone;
	protected $m_strAgentEmail;
	protected $m_intOccupantCustomerIds;

	protected $m_intMinimumLiability;

	public static $c_arrstrPolicyNumberPrefix = [ 'rth', 'prh', 'pr', 'rlo', 'rlc' ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadSourceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getInsurancePolicyTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_policy_type_id', __( 'Policy Type is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyNumber( $boolIsCustomPolicy = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPolicyNumber() ) ) {
			$boolIsValid = false;

			if( true == $boolIsCustomPolicy ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'carrier_policy_number', __( 'Policy Number is required.' ) ) );

			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'carrier_policy_number', __( 'Policy Number is required.' ) ) );
			}
		}

		if( true == $boolIsCustomPolicy && CInsuranceCarrier::$c_strInsuranceCarrierSSIS == \Psi\CStringService::singleton()->strtoupper( $this->getInsuranceCarrierName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'carrier_policy_number', __( 'Provider {%s,0} is Forced Placed provider, can not use for custom policy.', [ 'SSIS' ] ) ) );
			$boolIsValid = false;
		}

		if( true == $boolIsCustomPolicy && 'RI-Markel' == $this->getInsuranceCarrierName() ) {

			$boolIsValidPolicyNumber = strcmp( 'RTH', \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->strtoupper( $this->getPolicyNumber() ), 0, 3 ) );

			if( false == is_null( $this->getPolicyNumber() ) && true == $boolIsValidPolicyNumber ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'carrier_policy_number', __( '{%s,0} policy number should start with {%s,1}.', [ 'RI-Markel', 'RTH' ] ) ) );
				$boolIsValid = false;
			}
		}

		if( true == $boolIsCustomPolicy && 'RI-QBE' == $this->getInsuranceCarrierName() ) {

			$boolIsValidPolicyNumber = strcmp( 'PR', \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->strtoupper( $this->getPolicyNumber() ), 0, 2 ) );

			if( false == is_null( $this->getPolicyNumber() ) && true == $boolIsValidPolicyNumber ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'carrier_policy_number', __( '{%s,0} policy number should start with {%s,1}.', [ 'RI-QBE', 'PR' ] ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valIsIntegrated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReplacedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceCarrierName() {
		$boolIsValid = true;
		if( true == is_null( $this->getInsuranceCarrierName() ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getInsuranceCarrierName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_insurance_carrier_name', __( 'Insurance Provider is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPrimaryInsuredName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdditionalInsureds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProofOfCoverageFileName() {
		$boolIsValid = true;
		if( true == is_null( $this->getDeductible() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_deductible', __( 'Deductible amount is required.' ) ) );
		}

		if( true == $boolIsValid ) {
			if( false == is_numeric( $this->getDeductible() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_deductible', __( 'Deductible amount is not valid.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valProofOfCoverageFilePath() {
		$boolIsValid = true;

		if( true == is_null( $this->getLiabilityLimit() ) && false == is_numeric( $this->getLiabilityLimit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_liability_limit', __( 'Liability amount is required.' ) ) );
		}

		if( true == $boolIsValid ) {
			if( 0 >= $this->getLiabilityLimit() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_liability_limit', __( 'Liability amount is not valid.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valProofOfCoverageUploadedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( 'Start date is required.' ) ) );
		} else {
			$mixValidatedEffectiveDate = CValidation::checkDateFormat( $this->getEffectiveDate(), $boolFormat = true );
			if( false === $mixValidatedEffectiveDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( 'Start date must be in mm/dd/yyyy format or the date is invalid.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is required.' ) ) );
		} else {
			$mixValidatedEndDate = CValidation::checkDateFormat( $this->getEndDate(), $boolFormat = true );
			if( false === $mixValidatedEndDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date must be in mm/dd/yyyy format or the date is invalid.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valLapsedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCancelledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfirmedOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( 'Start date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomEffectiveDate() {
		$boolIsValid = true;

		$strStartDate = $this->getEffectiveDate();

		if( true == is_null( $strStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'effective_date', __( 'Coverage Start Date is required.' ), NULL ) );
		}

		if( false == is_null( $strStartDate ) && 0 < \Psi\CStringService::singleton()->strlen( $strStartDate ) && 1 !== CValidation::checkDate( $strStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( 'Coverage Start Date must be in mm/dd/yyyy form or the date is invalid.' ) ) );
		}
		return $boolIsValid;
	}

	public function valDeductible() {
		$boolIsValid = true;

		if( true == is_null( $this->getDeductible() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_deductible', __( 'Deductible amount is required.' ) ) );
		}

		if( true == $boolIsValid ) {
			if( false == is_numeric( $this->getDeductible() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_deductible', __( 'Deductible amount is not valid.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valLiabilityLimit() {

		$boolIsValid = true;

		if( true == is_null( $this->getLiabilityLimit() ) && false == is_numeric( $this->getLiabilityLimit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_liability_limit', __( 'Liability amount is required.' ) ) );
		}

		if( true == $boolIsValid ) {
			if( 0 >= $this->getLiabilityLimit() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_liability_limit', __( 'Liability amount is not valid.' ) ) );
			}
		}

		if( false == is_null( $this->getMinimumLiability() ) && false == is_null( $this->getLiabilityLimit() ) ) {
			if( $this->getMinimumLiability() > $this->getLiabilityLimit() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_liability_limit', __( 'Liability amount can not be less than Minimum Liability.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPersonalLimit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartAndEndDate() {
		$boolIsValid = true;
		$boolIsValid &= $this->valEffectiveDate();
		$boolIsValid &= $this->valEndDate();

		if( true == $boolIsValid ) {
			$strStartDate 	= strtotime( $this->getEffectiveDate() );
			$strEndDate 	= strtotime( $this->getEndDate() );

			if( $strEndDate < $strStartDate ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', __( 'Start date should not be greater than End date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPolicyMatchTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'resident_works_insert_update':
				$boolIsValid &= $this->valCustomEffectiveDate();
				break;

			case 'validate_custom_resident_insurance_policy':
				$boolIsValid &= $this->valInsuranceCarrierName();
				$boolIsValid &= $this->valPolicyNumber( $boolIsCustomPolicy = true );
				$boolIsValid &= $this->valLiabilityLimit();
				$boolIsValid &= $this->valInsurancePolicyTypeId();
				$boolIsValid &= $this->valStartAndEndDate();
				break;

			case 'validate_start_and_end_date':
				$boolIsValid &= $this->valStartAndEndDate();
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet );
		if( true == isset( $arrstrValues['custom_insurance_carrier_name'] ) )		$this->setInsuranceCarrierName( $arrstrValues['custom_insurance_carrier_name'] );
		if( true == isset( $arrstrValues['carrier_policy_number'] ) )				$this->setPolicyNumber( $arrstrValues['carrier_policy_number'] );
		if( true == isset( $arrstrValues['custom_liability_limit'] ) )				$this->setLiabilityLimit( $arrstrValues['custom_liability_limit'] );
		if( true == isset( $arrstrValues['custom_personal_limit'] ) )				$this->setPersonalLimit( $arrstrValues['custom_personal_limit'] );
		if( true == isset( $arrstrValues['custom_deductible'] ) )					$this->setDeductible( $arrstrValues['custom_deductible'] );
		if( true == isset( $arrstrValues['effective_date'] ) )						$this->setEffectiveDate( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $arrstrValues['effective_date'] ] ) );
		if( true == isset( $arrstrValues['end_date'] ) )								$this->setEndDate( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $arrstrValues['end_date'] ] ) );
		if( true == isset( $arrstrValues['customer_id'] ) )							$this->setCustomerId( $arrstrValues['customer_id'] );
		if( true == isset( $arrstrValues['lease_id'] ) )							$this->setLeaseId( $arrstrValues['lease_id'] );
		if( true == isset( $arrstrValues['name_first'] ) )							$this->setNameFirst( $arrstrValues['name_first'] );
		if( true == isset( $arrstrValues['name_last'] ) )							$this->setNameLast( $arrstrValues['name_last'] );
		if( true == isset( $arrstrValues['agent_name'] ) )							$this->setAgentName( $arrstrValues['agent_name'] );
		if( true == isset( $arrstrValues['agent_phone'] ) )							$this->setAgentPhone( $arrstrValues['agent_phone'] );
		if( true == isset( $arrstrValues['agent_email'] ) )					$this->setAgentEmail( $arrstrValues['agent_email'] );
	}

	/**
	 * Other Functions
	 *
	 */

	public function buildFileData( $objFile, $strInputTypeFileName = 'policy_file_name', $objDatabase ) {

		if( true == is_null( $objFile->getId() ) || false == is_numeric( $objFile->getId() ) ) {
			$objFile->setId( $objFile->fetchNextId( $objDatabase ) );
		}

		$objFile->setTitle( $_FILES[$strInputTypeFileName]['name'] );

		$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strFileName = 'policy_document_' . $objFile->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;

		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsgs( __( 'Invalid file extension.' ) );
			return false;
		}
		$strMimeType = $objFileExtension->getMimeType();
		$objFile->setFileExtensionId( $objFileExtension->getId() );

		$strPath = $objFile->getId() . '/policy_documents/';

		$objFile->setFilePath( $strPath );

		if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) )    $objFile->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'] );
		if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) ) 	     $objFile->setFileError( $_FILES[$strInputTypeFileName]['error'] );
		if( true == isset( $strFileName ) ) 								 $objFile->setFileName( $strFileName );
		if( true == isset( $strMimeType ) ) 								 $objFile->setFileType( $strMimeType );
	}

	public function buildFileAssociation( $intResidentInsurancePolicyId, $intTotalSelectedCustomersCount, $arrintNewSelectedCustomerIds, $intLeaseId, $intFileId, $intUserId, $objClient, $objDatabase, $boolIsFromResidentsTab = false, $arrintDeleteCustomerIds = [] ) {

		$intPreviousCustomerCount = 0;

		if( false == valId( $intResidentInsurancePolicyId ) || false == valId( $intLeaseId ) || false == valId( $intFileId ) || false == valId( $intUserId ) || false == valObj( $objClient, 'CClient' ) ) {
			return false;
		}

		$arrobjFileAssociations = CFileAssociations::createService()->fetchFileAssociationsByFileIdByCid( $intFileId, $objClient->getId(), $objDatabase );

		// @TODO Test it thoroughly in second phase refactoring
		$intLeaseCustomersCount = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCountOfCustomLeaseCustomersByLeaseIdsByCid( [ $intLeaseId ], $objClient->getId(), $objDatabase, true, true );

		if( true == valArr( $arrintDeleteCustomerIds ) ) {
			$arrobjInsurancePolicyCustomers = ( array ) CInsurancePolicyCustomers::fetchInsurancePolicyCustomersByResidentInsurancePolicyIdsByCid( [ $intResidentInsurancePolicyId ], $objClient->getId(), $objDatabase );
			$intPreviousCustomerCount       = \Psi\Libraries\UtilFunctions\count( $arrobjInsurancePolicyCustomers );
		}

		if( true == $boolIsFromResidentsTab ) {
			$arrobjFileAssociationsToDelete = CFileAssociations::createService()->fetchNonDeletedFileAssociationsByCustomerIdsByFileIdByCid( $arrintDeleteCustomerIds, $intFileId, $objClient->getId(), $objDatabase );
			if( true == valArr( $arrintDeleteCustomerIds ) ) {
				if( true == valArr( $arrobjFileAssociationsToDelete ) ) {
					foreach( $arrobjFileAssociationsToDelete as $objFileAssociationToDelete ) {
						if( false == $objFileAssociationToDelete->delete( $intUserId, $objDatabase, true ) ) {
							$this->addErrorMsgs( __( 'Failed to delete file association.' ) );
							break;
						}
					}
				}
			}

			if( $intLeaseCustomersCount == $intTotalSelectedCustomersCount || ( $intPreviousCustomerCount == $intLeaseCustomersCount && $intLeaseCustomersCount != $intTotalSelectedCustomersCount ) ) {
				if( true == valArr( $arrobjFileAssociations ) ) {
					foreach( $arrobjFileAssociations as $objFileAssociation ) {
						if( $intFileId == $objFileAssociation->getFileId() ) {
							if( false == $objFileAssociation->delete( $intUserId, $objDatabase, true ) ) {
								$this->addErrorMsgs( __( 'Failed to delete file association.' ) );
								break;
							}
						}
					}
				}
			}
		}

		if( true == valArr( $arrintNewSelectedCustomerIds ) ) {
			foreach( $arrintNewSelectedCustomerIds as $intSelectedCustomerId ) {

				$objFileAssociation = $objClient->createFileAssociation();
				$objFileAssociation->setFileId( $intFileId );

				if( false == $boolIsFromResidentsTab || $intLeaseCustomersCount != $intTotalSelectedCustomersCount ) {
					$objFileAssociation->setCustomerId( $intSelectedCustomerId );
				}

				$objFileAssociation->setLeaseId( $intLeaseId );
				$objFileAssociation->setResidentInsurancePolicyId( $intResidentInsurancePolicyId );
				if( false == $objFileAssociation->insert( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( __( 'Failed to create file association.' ) );
					return false;
				}

				if( $intLeaseCustomersCount == $intTotalSelectedCustomersCount ) {
					break;
				}
			}
		}

		return true;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == is_null( $this->getId() ) ) {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		} else {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function setMinimumLiabilityLimit( $objDatabse ) {

		$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'MINIMUM_LIABILITY', $this->getPropertyId(), $this->getCid(), $objDatabse );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			$this->setMinimumLiability( $objPropertyPreference->getValue() );
		}
	}

	public function getDeletedInsurancePolicy( $objResidentInsurancePolicy, $objDatabase ) {

		$objResidentInsurancePolicyHistory = CResidentInsurancePolicyHistories::fetchResidentInsurancePolicyHistoryByPolicyIdByLeaseIdByCid( $objResidentInsurancePolicy, $objDatabase );

		if( true == valObj( $objResidentInsurancePolicyHistory, 'CResidentInsurancePolicyHistory' ) ) {
			$objResidentInsurancePolicy->setRemotePrimaryKey( $objResidentInsurancePolicyHistory->getRemotePrimaryKey() );
		}
	}

	/**
	 * Create Functions
	 *
	 */

	public function createInsurancePolicyCustomer() {

		$objInsurancePolicyCustomer = new CInsurancePolicyCustomer();
		$objInsurancePolicyCustomer->setResidentInsurancePolicyId( $this->getId() );
		$objInsurancePolicyCustomer->setCid( $this->getCid() );
		$objInsurancePolicyCustomer->setPropertyId( $this->getPropertyId() );
		$objInsurancePolicyCustomer->setLeaseId( $this->getLeaseId() );
		$objInsurancePolicyCustomer->setCustomerId( $this->getCustomerId() );

		return $objInsurancePolicyCustomer;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = CStrings::strToIntDef( $intCustomerId, NULL, false );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = CStrings::strToIntDef( $intLeaseId, NULL, false );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function setMinimumLiability( $intMinimumLiability ) {
		$this->m_intMinimumLiability = $intMinimumLiability;
	}

	public function getMinimumLiability() {
		return $this->m_intMinimumLiability;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setOccupantCustomerIds( $intCustomerIds ) {
		$this->m_intOccupantCustomerIds = $intCustomerIds;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function setAgentName( $strAgentName ) {
		$this->m_strAgentName = $strAgentName;
	}

	public function getAgentName() {
		return $this->m_strAgentName;
	}

	public function setAgentPhone( $strAgentPhone ) {
		$this->m_strAgentPhone = $strAgentPhone;
	}

	public function getAgentPhone() {
		return $this->m_strAgentPhone;
	}

	public function setAgentEmail( $strAgentEmail ) {
		$this->m_strAgentEmail = $strAgentEmail;
	}

	public function getAgentEmail() {
		return $this->m_strAgentEmail;
	}

	public function getOccupantCustomerIds() {
		return $this->m_intOccupantCustomerIds;
	}

	public function createResidentInsurancePolicy( $arrmixResidentInsurancePolicy ) {
		$this->setCid( $arrmixResidentInsurancePolicy['cid'] );
		$this->setPropertyId( $arrmixResidentInsurancePolicy['property_id'] );
		$this->setPropertyUnitId( $arrmixResidentInsurancePolicy['property_unit_id'] );
		$this->setUnitSpaceId( $arrmixResidentInsurancePolicy['unit_space_id'] );
		$this->setUnitNumber( $arrmixResidentInsurancePolicy['unit_number'] );

		return $this;
	}

	public function checkProofOfInsurance( $intPropertyId, $intCid, $objDatabase, $arrobjPropertyPreferences = NULL, $objAdminDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return false;
		}

		// Check for active RI product contract, if the property is not RI contracted, we do not want validate the proof of insurance document
		$boolHasActiveContract = CResidentInsureLibrary::boolCheckIsContractEnable( $intCid, $intPropertyId, CPsProduct::RESIDENT_INSURE, $objAdminDatabase );

		if( false == $boolHasActiveContract ) {
			return false;
		}

		if( true == valArr( $arrobjPropertyPreferences ) && true == valObj( $arrobjPropertyPreferences['REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS'], 'CPropertyPreference' ) ) {
			return true;
		}

		$arrobjPropertyPreferences = ( array ) CPropertyPreferences::fetchPropertyPreferencesByKeysByPropertyIdByCid( [ 'REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS' ], $intPropertyId, $intCid, $objDatabase );
		$arrobjPropertyPreferences = rekeyObjects( 'key', $arrobjPropertyPreferences );

		if( true == valObj( $arrobjPropertyPreferences['REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS'], 'CPropertyPreference' ) ) {
			return true;
		}

		return false;
	}

}
?>