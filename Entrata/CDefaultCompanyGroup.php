<?php

class CDefaultCompanyGroup extends CBaseDefaultCompanyGroup {

	/**
	 * Create Functions
	 */

	/**
	 * @param $intModuleId
	 * @return \CDefaultGroupPermission
	 */
	public function createDefaultGroupPermission( $intModuleId ) {
		$objDefaultGroupPermission = new CDefaultGroupPermission();
		$objDefaultGroupPermission->setGroupSystemCode( $this->getSystemCode() );
		$objDefaultGroupPermission->setModuleId( $intModuleId );
		return $objDefaultGroupPermission;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;
		if( false == valStr( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateName( $objDatabase ) {
		$boolIsValid = true;
		$intCompanyGroupCount = CDefaultCompanyGroups::fetchDefaultCompanyGroupCountByNameById( $this->m_strName, $this->m_intId, $objDatabase );

		if( 0 < $intCompanyGroupCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'The default company group name \'' . $this->m_strName . '\' is already in use.' ) );
		}

		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strSystemCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'System Code should be only 10 alphabetic characters.' ) );
		} elseif( false == ctype_alpha( $this->m_strSystemCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'System Code should be only alphabetic characters.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateSystemCode( $objDatabase ) {
		$boolIsValid = true;
		$intCompanyGroupCount = CDefaultCompanyGroups::fetchDefaultCompanyGroupCountBySystemCodeById( $this->m_strSystemCode, $this->m_intId, $objDatabase );

		if( 0 < $intCompanyGroupCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'The default company group system code  \'' . $this->m_strSystemCode . '\' is already in use.' ) );
		}

		return $boolIsValid;

	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valSystemCode();
				$boolIsValid &= $this->valDuplicateName( $objDatabase );
				$boolIsValid &= $this->valDuplicateSystemCode( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDuplicateName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>