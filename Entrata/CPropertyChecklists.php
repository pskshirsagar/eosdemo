<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyChecklists
 * Do not add any new functions to this class.
 */

class CPropertyChecklists extends CBasePropertyChecklists {

	public static function fetchCustomPropertyChecklistsByChecklistTypeIdByChecklistTriggerIdByOccupancyTypeIdsByPropertyIdByCid( $intChecklistTypeId, $intTriggerId, $arrintOccupancyTypeIds, $arrintPropertyIds, $intCid, $objClientDatabase, $intPropertyChecklistId = NULL ) {

		$strOR       = '';
		$strWhereSql = ( true == valId( $intPropertyChecklistId ) ) ? ' AND pc.checklist_id != ' . $intPropertyChecklistId : '';

		$strWhereSql .= ' AND ( ';
		foreach( $arrintOccupancyTypeIds as $intOccupancyTypeId ) {
			$strWhereSql .= $strOR . ( int ) $intOccupancyTypeId . ' = ANY( c.occupancy_type_ids )';
			$strOR        = ' OR ';
		}

		$strWhereSql .= ' )';

		$strSql = '	SELECT
					    DISTINCT p.property_name
					FROM
					    checklists c
					    JOIN property_checklists pc ON ( c.cid = pc.cid AND c.id = pc.checklist_id )
						JOIN properties p ON ( pc.cid = p.cid AND pc.property_id = p.id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.deleted_by IS NULL
						AND c.checklist_type_id = ' . ( int ) $intChecklistTypeId . '
					    AND c.checklist_trigger_id = ' . ( int ) $intTriggerId . '
					    AND pc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					   	' . $strWhereSql;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAssociatedPropertiesByChecklistIdByCid( $intChecklistId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT pc.property_id,
						p.property_name
				   FROM
						property_checklists pc
						JOIN properties p ON ( p.cid = pc.cid AND p.id = pc.property_id )
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ]::INT [ ], ARRAY[' . CPsProduct::ENTRATA . '] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = p.id AND lp.cid = p.cid )
				   WHERE
						pc.cid = ' . ( int ) $intCid . '
						AND pc.checklist_id = ' . ( int ) $intChecklistId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChecklistsCountsByPropertyIdsByCheklistTriggerIdByCid( $arrintPropertyIds, $intCheckListTriggerId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT ( c.id ),
						p.id,
						p.property_name
					FROM
						properties p
    					LEFT JOIN property_checklists pc ON ( p.cid = pc.cid AND p.id = pc.property_id )
						LEFT JOIN checklists c ON ( c.cid = pc.cid AND pc.checklist_id = c.id AND c.is_enabled = TRUE AND c.checklist_trigger_id = ' . ( int ) $intCheckListTriggerId . ' AND c.deleted_by IS NULL )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN (  ' . implode( ', ', $arrintPropertyIds ) . ')
					GROUP BY
						p.id,
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

}
?>