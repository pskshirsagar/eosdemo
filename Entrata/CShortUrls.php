<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CShortUrls
 * Do not add any new functions to this class.
 */

class CShortUrls extends CBaseShortUrls {

	public static function fetchShortUrlBySlugByCid( $strSlug, $intCid, $objDatabase ) {
		if( true == is_null( $strSlug ) || true == is_null( $intCid ) ) return;

		$strSql = 'SELECT * FROM short_urls WHERE cid = ' . ( int ) $intCid . ' AND slug = \'' . addslashes( $strSlug ) . '\'';

		return parent::fetchShortUrl( $strSql, $objDatabase );
	}

	public static function fetchShortUrlByFullUrlByCid( $strFullUrl, $intCid, $objDatabase ) {
		if( true == is_null( $strFullUrl ) || true == is_null( $intCid ) ) return;

		$strSql = 'SELECT * FROM short_urls WHERE cid = ' . ( int ) $intCid . ' AND full_url = \'' . $strFullUrl . '\' LIMIT 1';

		return parent::fetchShortUrl( $strSql, $objDatabase );
	}

}
?>