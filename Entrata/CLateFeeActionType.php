<?php

class CLateFeeActionType extends CBaseLateFeeActionType {

	const POST		= 1;
	const WAIVE		= 2;
	const RETAIN	= 3;

	/**
	 * Other Functions
	 */

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'LATE_FEE_ACTION_TYPE_POST', 	self::POST );
		$objSmarty->assign( 'LATE_FEE_ACTION_TYPE_WAIVE', 	self::WAIVE );
		$objSmarty->assign( 'LATE_FEE_ACTION_TYPE_RETAIN',	self::RETAIN );
	}
}
?>