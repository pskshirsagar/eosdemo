<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentAssociationTypes
 * Do not add any new functions to this class.
 */

class CDocumentAssociationTypes extends CBaseDocumentAssociationTypes {

	public static function fetchDocumentAssociationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDocumentAssociationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDocumentAssociationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDocumentAssociationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>