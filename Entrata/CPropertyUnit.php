<?php
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CPropertyBuildings;
use Psi\Eos\Entrata\CPropertyFloors;
use Psi\Eos\Entrata\CUnitSpaces;

class CPropertyUnit extends CBasePropertyUnit {

	protected $m_intUnitSpecialsCount;
	protected $m_intUnitSpaceNumber;
	protected $m_intUnitCounts;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intMarketRent;
	protected $m_intDeposit;
	protected $m_intLeaseStatusTypeId;
	protected $m_intUnitSpaceId;
	protected $m_intJobGroupId;

	protected $m_strFloorTitle;
	protected $m_strJobGroupName;
	protected $m_strCustomerName;
	protected $m_strBuildingName;
	protected $m_strUnitTypeName;
	protected $m_strPropertyUnitAddress;

	protected $m_objPropertyBuilding;
	protected $m_objPropertyFloorplan;
	protected $m_objPropertyFloor;
	protected $m_objUnitAddress;

	protected $m_boolIsSelected;

	protected $m_arrobjUnitSpaces;
	protected $m_arrobjLeaseTermIds;
	protected $m_arrobjRates;
	protected $m_arrobjApartmentAmenities;
	protected $m_arrobjSpecials;
	protected $m_intUnitTypesCount;
	protected $m_strPropertyFloorplanName;
	protected $m_strUnitSpaceStatusTypeName;
	protected $m_strNotes;

	protected $m_strUnitNumberCache;
	protected $m_intIsCorporateRented;
	protected $m_strTaxCreditUnitNumber;

	/**
	* Add Functions
	*
	*/

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}
	}

	public function addUnitSpace( $objUnitSpace ) {
		$this->m_arrobjUnitSpaces[$objUnitSpace->getId()] = $objUnitSpace;
	}

	/**
	* Get Functions
	*
	*/

	public function getPropertyBuilding() {
		return $this->m_objPropertyBuilding;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getPropertyFloorplanName() {
		return $this->m_strPropertyFloorplanName;
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function getIsCorporateRented() {
		return $this->m_intIsCorporateRented;
	}

	public function getUnitSpaces() {
		return $this->m_arrobjUnitSpaces;
	}

	public function getUnitAddress() {
		return $this->m_objUnitAddress;
	}

	public function getUnitSpecialsCount() {
		return $this->m_intUnitSpecialsCount;
	}

	public function getPropertyFloorplan() {
		return $this->m_objPropertyFloorplan;
	}

	public function getPropertyFloor() {
		return $this->m_objPropertyFloor;
	}

	public function getRates() {
		return $this->m_arrobjRates;
	}

	public function getMarketRent() {
		return $this->m_intMarketRent;
	}

	public function getCustomerName() {
		return $this->m_strCustomerName;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getUnitSpaceNumber() {
		return $this->m_intUnitSpaceNumber;
	}

	public function getUnitCounts() {
		return $this->m_intUnitCounts;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	public function getUnitTypeName() {
		return $this->m_strUnitTypeName;
	}

	public function getDeposit() {
		return $this->m_intDeposit;
	}

	public function getApartmentAmenities() {
		return $this->m_arrobjApartmentAmenities;
	}

	public function getSpecials() {
		return $this->m_arrobjSpecials;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getUnitTypesCount() {
		return $this->m_intUnitTypesCount;
	}

	public function getLeaseTermIds() {
		return $this->m_arrobjLeaseTermIds;
	}

	public function getUnitSpaceStatusTypeName() {
		return $this->m_strUnitSpaceStatusTypeName;
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function getPropertyUnitAddress() {
		return $this->m_strPropertyUnitAddress;
	}

	public function getFloorTitle() {
		return $this->m_strFloorTitle;
	}

	public function getJobGroupId() {
		return $this->m_intJobGroupId;
	}

	public function getJobGroupName() {
		return $this->m_strJobGroupName;
	}

	public function getTaxCreditUnitNumber() {

		$strTaxCreditUnitNumber = $this->getDetailsField( [ 'subsidy_details', 'tax_credit_unit_number' ] );
		if( strlen( $strTaxCreditUnitNumber ) > 0 ) {
			return $this->m_strTaxCreditUnitNumber = $strTaxCreditUnitNumber;
		} else {
			return $this->getUnitNumber();
		}
	}

	/**
	* Set Functions
	*
	*/

	public function setPropertyBuilding( $objPropertyBuilding ) {
		$this->m_objPropertyBuilding = $objPropertyBuilding;
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum );
	}

	public function setPropertyFloorplanName( $strPropertyFloorplanName ) {
		$this->m_strPropertyFloorplanName = $strPropertyFloorplanName;
	}

	public function setPropertyFloorplan( $objPropertyFloorplan ) {
		$this->m_objPropertyFloorplan = $objPropertyFloorplan;
	}

	public function setPropertyFloor( $objPropertyFloor ) {
		$this->m_objPropertyFloor = $objPropertyFloor;
	}

	public function setUnitSpecialsCount( $intUnitSpecialsCount ) {
		$this->m_intUnitSpecialsCount = $intUnitSpecialsCount;
	}

	public function setRates( $arrobjRates ) {
		$this->m_arrobjRates = $arrobjRates;
	}

	public function setMarketRent( $intMarketRent ) {
		$this->m_intMarketRent = $intMarketRent;
	}

	public function setCustomerName( $strCustomerName ) {
		$this->m_strCustomerName = $strCustomerName;
	}

	public function setCustomerId( $strCustomerId ) {
		$this->m_intCustomerId = $strCustomerId;
	}

	public function setLeaseId( $strLeaseId ) {
		$this->m_intLeaseId = $strLeaseId;
	}

	public function setUnitSpaceNumber( $intSpaceNumber ) {
		$this->m_intUnitSpaceNumber = $intSpaceNumber;
	}

	public function setIsCorporateRented( $intIsCorporateRented ) {
		$this->m_intIsCorporateRented = $intIsCorporateRented;
	}

	public function setUnitCounts( $intUnitCounts ) {
		$this->m_intUnitCounts = $intUnitCounts;
	}

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function setUnitSpaces( $arrobjUnitSpaces ) {
		$this->m_arrobjUnitSpaces = $arrobjUnitSpaces;
	}

	public function setUnitAddress( $objUnitAddress ) {
		$this->m_objUnitAddress = $objUnitAddress;
	}

	public function setUnitTypeName( $strUnitTypeName ) {
		$this->m_strUnitTypeName = $strUnitTypeName;
	}

	public function setUnitSpaceStatusTypeName( $strUnitSpaceStatusTypeName ) {
		$this->m_strUnitSpaceStatusTypeName = $strUnitSpaceStatusTypeName;
	}

	public function setDeposit( $intDeposit ) {
		$this->m_intDeposit = $intDeposit;
	}

	public function setApartmentAmenities( $arrobjApartmentAmenities ) {
		$this->m_arrobjApartmentAmenities = $arrobjApartmentAmenities;
	}

	public function setSpecials( $arrobjSpecials ) {
		$this->m_arrobjSpecials = $arrobjSpecials;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setLeaseTermIds( $arrobjLeaseTermIds ) {
		$this->m_arrobjLeaseTermIds = NULL;
		if( true == valArr( $arrobjLeaseTermIds ) ) {
			$this->m_arrobjLeaseTermIds = implode( ',', $arrobjLeaseTermIds );
		}
	}

	public function setUnitTypesCount( $intUnitTypesCount ) {
		$this->m_intUnitTypesCount = $intUnitTypesCount;
	}

	public function setNotes( $strNotes ) {
		$this->m_strNotes = $strNotes;
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = $intUnitSpaceId;
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->m_strUnitNumberCache = $strUnitNumberCache;
	}

	public function setPropertyUnitAddress( $strPropertyUnitAddress ) {
		$this->m_strPropertyUnitAddress = $strPropertyUnitAddress;
	}

	public function setFloorTitle( $strFloorTitle ) {
		$this->m_strFloorTitle = $strFloorTitle;
	}

	public function setJobGroupId( $intJobGroupId ) {
		$this->m_intJobGroupId = $intJobGroupId;
	}

	public function setJobGroupName( $strJobGroupName ) {
		$this->m_strJobGroupName = $strJobGroupName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet = false ) {

		if( true == isset( $arrmixValues['unit_specials_count'] ) ) $this->setUnitSpecialsCount( $arrmixValues['unit_specials_count'] );
		if( true == isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( $arrmixValues['building_name'] );
		if( true == isset( $arrmixValues['customer_name'] ) ) $this->setCustomerName( $arrmixValues['customer_name'] );
		if( true == isset( $arrmixValues['unit_space_status_type_name'] ) ) $this->setUnitSpaceStatusTypeName( $arrmixValues['unit_space_status_type_name'] );
		if( true == isset( $arrmixValues['space_number'] ) ) $this->setUnitSpaceNumber( $arrmixValues['space_number'] );
		if( true == isset( $arrmixValues['unit_number_count'] ) ) $this->setUnitCounts( $arrmixValues['unit_number_count'] );
		if( true == isset( $arrmixValues['is_selected'] ) ) $this->setIsSelected( $arrmixValues['is_selected'] );
		if( true == isset( $arrmixValues['name'] ) ) $this->setUnitTypeName( $arrmixValues['name'] );
		if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['lease_id'] ) ) $this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( true == isset( $arrmixValues['floorplan_name'] ) ) $this->setPropertyFloorplanName( $arrmixValues['floorplan_name'] );
		if( true == isset( $arrmixValues['is_corporate_rented'] ) ) $this->setIsCorporateRented( $arrmixValues['is_corporate_rented'] );
		if( true == isset( $arrmixValues['order_num'] ) ) $this->setOrderNum( $arrmixValues['order_num'] );
		if( true == isset( $arrmixValues['notes'] ) ) $this->setNotes( $arrmixValues['notes'] );
		if( true == isset( $arrmixValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrmixValues['unit_space_id'] );
		if( true == isset( $arrmixValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrmixValues['unit_number_cache'] );
		if( true == isset( $arrmixValues['property_unit_address'] ) ) $this->setPropertyUnitAddress( $arrmixValues['property_unit_address'] );
		if( true == isset( $arrmixValues['floor_title'] ) ) $this->setFloorTitle( $arrmixValues['floor_title'] );
		if( true == isset( $arrmixValues['job_group_id'] ) ) $this->setJobGroupId( $arrmixValues['job_group_id'] );
		if( true == isset( $arrmixValues['job_group_name'] ) ) $this->setJobGroupName( $arrmixValues['job_group_name'] );
		if( true == isset( $arrmixValues['unit_type_name'] ) ) $this->setUnitTypeName( $arrmixValues['unit_type_name'] );

		return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function setTaxCreditUnitNumber( $strTaxCreditUnitNumber ) {

		if( strlen( $strTaxCreditUnitNumber ) > 0 ) {
			$this->setDetailsField( [ 'subsidy_details', 'tax_credit_unit_number' ], $strTaxCreditUnitNumber );
		} else {
			$this->setDetailsField( [ 'subsidy_details', 'tax_credit_unit_number' ], NULL );
		}
	}

	/**
	 * Create Functions
	 *
	 */

	public function createUnitSpace() {
		$objUnitSpace = new CUnitSpace();
		$objUnitSpace->setDefaults();
		$objUnitSpace->setCid( $this->m_intCid );
		$objUnitSpace->setPropertyId( $this->m_intPropertyId );
		$objUnitSpace->setPropertyUnitId( $this->m_intId );

		return $objUnitSpace;
	}

	public function createRate() {
		$objRate = new CRate();
		$objRate->setCid( $this->m_intCid );
		$objRate->setRateTypeId( CRateType::PROPERTY_UNIT );
		$objRate->setReferenceId( $this->getId() );

		return $objRate;
	}

	public function createUnitAddress() {
		$objUnitAddress = new CUnitAddress();
		$objUnitAddress->setCid( $this->m_intCid );
		$objUnitAddress->setPropertyId( $this->m_intPropertyId );
		$objUnitAddress->setPropertyUnitId( $this->m_intId );

		return $objUnitAddress;
	}

	public function createUnitSpaceOccupancyHistroy() {
		$objUnitSpaceOccupancyHistory = new CUnitSpaceOccupancyHistory();
		$objUnitSpaceOccupancyHistory->setCid( $this->getCid() );
		$objUnitSpaceOccupancyHistory->setPropertyId( $this->getPropertyId() );
		$objUnitSpaceOccupancyHistory->setPropertyUnitId( $this->getId() );

		return $objUnitSpaceOccupancyHistory;
	}

	public function createUnitMedia() {
		$objUnitMedia = new CUnitMedia();
		$objUnitMedia->setCid( $this->m_intCid );
		$objUnitMedia->setPropertyUnitId( $this->getId() );
		$objUnitMedia->setPropertyId( $this->getPropertyId() );

		return $objUnitMedia;
	}

	public function createUnitSpaceUnion() {
		$objUnitSpaceUnion = new CUnitSpaceUnion();
		$objUnitSpaceUnion->setDefaults();
		$objUnitSpaceUnion->setCid( $this->m_intCid );
		$objUnitSpaceUnion->setPropertyId( $this->m_intPropertyId );

		return $objUnitSpaceUnion;
	}

	public function createCommercialSuite() {
		$objCommercialSuite = new CCommercialSuite();
		$objCommercialSuite->setCid( $this->m_intCid );
		$objCommercialSuite->setPropertyId( $this->m_intPropertyId );

		return $objCommercialSuite;
	}

	public function createMaintenanceRequest() {

		$objMaintenanceRequest = new CMaintenanceRequest();

		$objMaintenanceRequest->setCid( $this->getCid() );
		$objMaintenanceRequest->setPropertyId( $this->getPropertyId() );

		$objMaintenanceRequest->setPropertyUnitId( $this->getId() );
		$objMaintenanceRequest->setUnitNumber( $this->getUnitNumber() );
		$objMaintenanceRequest->setPropertyBuildingId( $this->getPropertyBuildingId() );

		$objMaintenanceRequest->setIsFloatingMaintenanceRequest( true );

		return $objMaintenanceRequest;
	}

	/**
	* Fetch Functions
	*
	*/

	public function fetchConflictingUnitSpaceOccupancyHistoryCountByLeaseIdByLeaseActionIdByLeaseActionDate( $intLeaseId, $intActionId, $strEventDate, $objDatabase ) {
		return CUnitSpaceOccupancyHistories::fetchConflictingUnitSpaceOccupancyHistoryCountByLeaseIdByLeaseActionIdByCidByPropertyUnitIdByLeaseActionDate( $intLeaseId, $intActionId, $this->getCid(), $this->getId(), $strEventDate, $objDatabase );
	}

	public function fetchUnitKeyValues( $objDatabase ) {
		return CUnitKeyValues::fetchCustomUnitKeyValuesByPropertyUnitIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchUnitKeyValueByKey( $strKey, $objDatabase ) {
		return CUnitKeyValues::fetchUnitKeyValuesByPropertyUnitIdByCidByKey( $this->m_intId, $this->m_intCid, $strKey, $objDatabase );
	}

	public function fetchUnitAddress( $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitPrimaryAddressByPropertyUnitIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchUnitSpaceByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByPropertyUnitIdByRemotePrimaryKeyByCid( $this->m_intId, $strRemotePrimaryKey, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaces( $objDatabase ) {
		return CUnitSpaces::createService()->fetchCustomUnitSpacesByPropertyUnitIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceById( $intUnitSpaceId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByPropertyUnitIdByIdByCid( $this->m_intId, $intUnitSpaceId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceByIdByRemovingDefaultSpaceNumber( $intUnitSpaceId, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByPropertyUnitIdByIdByRemovingDefaultSpaceNumberByCid( $this->m_intId, $intUnitSpaceId, $this->m_intCid, $objDatabase );
	}

	public function fetchUnitSpaceCachedRate( $objDatabase ) {
		return CUnitSpaceCachedRates::fetchUnitSpaceCachedRateByPropertyUnitIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitType( $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitTypes::createService()->fetchUnitTypeByIdByPropertyIdByCid( $this->m_intUnitTypeId, $this->m_intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuilding( $objDatabase ) {
		if( false == is_numeric( $this->getPropertyBuildingId() ) ) {
			return;
		}

		$this->m_objPropertyBuilding = CPropertyBuildings::createService()->fetchPropertyBuildingByIdByCid( $this->getPropertyBuildingId(), $this->getCid(), $objDatabase );

		return $this->m_objPropertyBuilding;
	}

	public function fetchCustomerConciergeServices( $objDatabase ) {
		return CCustomerConciergeServices::fetchCustomerConciergeServicesByPropertyUnitIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchSpecials( $objDatabase ) {
		return \Psi\Eos\Entrata\CSpecials::createService()->fetchPublishedSpecialsByPropertyIdByPropertyUnitIdByCid( $this->m_intPropertyId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByPropertyUnitIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchActiveLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByPropertyIdByPropertyUnitIdByLeaseStatusTypeIdsByCid( $this->m_intPropertyId, $this->m_intId, [ CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::APPLICANT ], $this->getCid(), $objDatabase );
	}

	public function fetchActiveApplicationsLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchActiveApplicationsLeasesByPropertyIdByPropertyUnitIdByLeaseStatusTypeIdsByCid( $this->m_intPropertyId, $this->m_intId, [ CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::APPLICANT ], $this->getCid(), $objDatabase );
	}

	public function fetchActiveMaintenanceRequests( $objDatabase, $boolCountOnly ) {
		return \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchActiveCustomMaintenanceRequestsByPropertyUnitIdByCid( $this->m_intId, $this->getCid(), $objDatabase, $boolCountOnly );
	}

	public function fetchApplications( $objDatabase ) {
		return CApplications::fetchApplicationsByPropertyUnitIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchActiveApplications( $objDatabase ) {
		return CApplications::fetchApplicationsByPropertyUnitIdByApplicationStageStatusIdsByCid( $this->m_intId, [ CApplicationStage::PRE_APPLICATION => [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ], CApplicationStage::APPLICATION => [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ], CApplicationStage::LEASE => [ CApplicationStatus::APPROVED ] ], $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplan( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyFloorplans::createService()->fetchPropertyFloorplanByIdByCid( $this->m_intPropertyFloorplanId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitMedias( $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitMedias::createService()->fetchUnitMediasByPropertyUnitIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitMediaByCompanyMediaFileId( $intCompanyMediaFileId, $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitMedias::createService()->fetchUnitMediaByPropertyUnitIdByCompanyMediaFileIdByCid( $this->m_intId, $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFiles( $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyUnitIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchActiveMarketingMediaAssociationByMediaSubTypeId( $intMarketingMediaSubType, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::UNIT_FLOORPLAN_MEDIA, $intMarketingMediaSubType, $this->getCid(), $objDatabase );
	}

	public function fetchSpaceCount( $objDatabase, $boolIncludeDeletedUnits = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND deleted_on IS NULL' : '';

		$strSql = 'SELECT count(id) FROM unit_spaces WHERE property_unit_id = ' . $this->m_intId . ' AND cid = ' . $this->getCid() . $strCheckDeletedUnitsSql;

		$arrmixData = [];

		$objDataset = $objDatabase->createDataset();

		if( true == $objDataset->execute( $strSql ) ) {
			if( 0 < $objDataset->getRecordCount() ) {
				while( false == $objDataset->eof() ) {
					$arrmixData[] = $objDataset->fetchArray();

					$objDataset->next();
				}
			}
		}

		return ( true == isset ( $arrmixData[0]['count'] ) ) ? $arrmixData[0]['count'] : 0;
	}

    public static function fetchPropertyUnitMaxOccupantsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intUnitSpaceId = NULL, $intPropertyUnitId = NULL, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

		$strSql = 'SELECT
						pu.max_occupants
					FROM
						property_units AS pu';

		if( false == is_null( $intPropertyUnitId ) ) {
			$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

			$strSql .= ' WHERE
						 	pu.id = ' . ( int ) $intPropertyUnitId . '
							AND pu.cid = ' . ( int ) $intCid . $strCheckDeletedUnitsSql;
		} elseif( false == is_null( $intUnitSpaceId ) ) {
			$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

			$strSql .= ' JOIN unit_spaces us ON ( us.cid = pu.cid AND us.property_unit_id = pu.id ' . $strCheckDeletedUnitSpacesSql . ' )
	                     WHERE
							us.id = ' . ( int ) $intUnitSpaceId . '
	                        AND us.property_id = ' . ( int ) $intPropertyId . '
	                        AND us.cid = ' . ( int ) $intCid;
		} else {
			return 0;
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['max_occupants'] ) ) {
			return $arrintResponse[0]['max_occupants'];
		}

		return 0;
    }

	public function fetchAllSpecials( $objDatabase ) {
		return \Psi\Eos\Entrata\CSpecials::createService()->fetchAllSpecialsByPropertyIdByPropertyUnitIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAvailableUnitSpaceCount( $objDatabase ) {
		return CUnitSpaces::createService()->fetchAvailableUnitSpaceCountByPropertyUnitIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPositionedPropertyUnitCount( $objDatabase, $boolIncludeDeletedUnits = false, $boolShowOnWebsite = true ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPublishedPositionedPropertyUnitCountByIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIncludeDeletedUnits, $boolShowOnWebsite );
	}

	public function fetchAvailableUnitSpaces( $objDatabase ) {
		return CUnitSpaces::createService()->fetchAvailableUnitSpacesByPropertyUnitIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicationDuplicates( $objDatabase ) {
		return CApplicationDuplicates::fetchApplicationDuplicatesByPropertyUnitIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	// MEGARATES_COMMENTS: No occurrence found for this function.

	// public function fetchPropertyPetTypes( $objDatabase ) {
	//	return \Psi\Eos\Entrata\Custom\CPetRateAssociations::createService()->fetchSortedPetRateAssociationsByPropertyIdByUnitSpaceIdsByCidWithPetTypeName( $this->getPropertyId(), array( $this->getUnitTypeId() ), $this->getCid(), $objDatabase );
	// }

	public function fetchPropertyFloorplans( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyFloorplans::createService()->fetchPropertyFloorplansByUnitTypeIdByCid( $this->getUnitTypeId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloor( $objDatabase ) {
		return CPropertyFloors::createService()->fetchPropertyFloorByIdByCid( $this->getPropertyFloorId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitPackages( $objDatabase ) {
		return CUnitPackages::fetchUnitPackagesByPropertyUnitIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceIds( $objDatabase ) {
		return CUnitSpaces::createService()->fetchCustomUnitSpaceIdsByPropertyUnitIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	/**
	* Validation Functions
	*
	*/

	public function valUnitNumber( $objDatabase, $boolIncludeDeletedUnits = false, $boolIsFromCommercial = false ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strUnitNumber ) || ( 1 > \Psi\CStringService::singleton()->strlen( $this->m_strUnitNumber ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', __( 'A unit number is required.' ) ) );
		}

		$objProperty = $this->fetchProperty( $objDatabase );
		if( true == valObj( $objProperty, 'CProperty' ) && CPropertyType::SUBSIDIZED == $objProperty->getPropertyTypeId() ) {
			$arrobjPropertySubsidyTypes = rekeyObjects( 'SubsidyTypeId', ( array ) \Psi\Eos\Entrata\CPropertySubsidyTypes::createService()->fetchPropertySubsidyTypesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(),  $objDatabase ) );
			if( array_key_exists( CSubsidyType::HUD, $arrobjPropertySubsidyTypes ) ) {
				if( false == \Psi\CStringService::singleton()->preg_match( '/^[a-zA-Z0-9 ]{1,10}$/', $this->getUnitNumber() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', __( 'Unit number should be less than or equal to 10 alphanumeric characters required for properties with subsidy type HUD.' ) ) );
					$boolIsValid = false;
				}
			}
		}

		if( true == $boolIsValid && true == valObj( $objDatabase, 'CDatabase' ) ) {

			$strCheckDeletedUnitsSql	= ( false == $boolIncludeDeletedUnits ) ? ' AND deleted_on IS NULL' : '';
			$strBuildingWhereItem		= ( true == is_null( $this->m_intPropertyBuildingId ) ) ? 'property_building_id IS NULL' : 'property_building_id = ' . ( int ) $this->m_intPropertyBuildingId;

			$strFloorWhereItem = ( true === $boolIsFromCommercial && true === valId( $this->m_intPropertyFloorId ) ? ' AND property_floor_id = ' . ( int ) $this->m_intPropertyFloorId : '' );

			$intPropertyUnitCount = ( int ) \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitCount( 'WHERE unit_number ILIKE \'' . addslashes( $this->m_strUnitNumber ) . '\' AND property_id = ' . ( int ) $this->m_intPropertyId . ' AND cid =  ' . ( int ) $this->getCid() . $strCheckDeletedUnitsSql . ' AND ' . $strBuildingWhereItem . $strFloorWhereItem . ' AND id <> \'' . ( int ) $this->m_intId . '\' LIMIT 1;', $objDatabase );

			if( 0 < $intPropertyUnitCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', __( 'The unit number you entered is already in use.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNumberOfBedrooms() {
		$boolIsValid = true;
		if( false == is_null( $this->m_intNumberOfBedrooms ) && false == is_int( $this->m_intNumberOfBedrooms ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bedrooms', __( 'Number of bedrooms must be integer.' ) ) );
		}
		return $boolIsValid;
	}

	public function valNumberOfBathrooms() {
		$boolIsValid = true;

		if( false == is_null( $this->m_fltNumberOfBathrooms ) ) {

    		if( true == is_numeric( $this->m_fltNumberOfBathrooms ) ) {

    			if( 0 > $this->m_fltNumberOfBathrooms ) {
    				$boolIsValid = false;
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms must be blank or greater than zero' ) ) );

    			} elseif( false == in_array( fmod( $this->m_fltNumberOfBathrooms, 1 ), [ 0, 0.25, 0.5, 0.75 ] ) ) {
    				$boolIsValid = false;
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
    			}
    		} else {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
    		}
    	}

		return $boolIsValid;
	}

	public function valMaxOccupants( $objDatabase ) {
		$boolIsValid = true;

		if( true == isset( $this->m_intMaxOccupants ) ) {
			if( false == is_numeric( $this->m_intMaxOccupants ) || 0 >= $this->m_intMaxOccupants ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_occupants', __( 'Max occupants must be blank or greater than zero.' ) ) );
			} else {
				$objUnitType = $this->fetchUnitType( $objDatabase );
				if( true == valObj( $objUnitType, 'CUnitType' ) && false == is_null( $objUnitType->getMaximumOccupants() ) && $this->m_intMaxOccupants > $objUnitType->getMaximumOccupants() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_occupants', __( 'Unit Max Occupants Cannot Exceed {%d,0} as set on the Unit Type.', [ $objUnitType->getMaximumOccupants() ] ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valSquareFeet() {
		$boolIsValid = true;

		if( true == isset( $this->m_fltSquareFeet ) ) {
			if( false == is_numeric( $this->m_fltSquareFeet ) || 0 >= $this->m_fltSquareFeet ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'square_feet', __( 'Square feet must be blank or greater than zero.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {
		$boolIsValid = true;

		$arrobjActiveApplications		= $this->fetchActiveApplications( $objDatabase );
		$arrobjActiveLeases 			= $this->fetchActiveApplicationsLeases( $objDatabase );
		$arrintMaintenanceRequestsCount	= $this->fetchActiveMaintenanceRequests( $objDatabase, $boolCountOnly = true );

		$boolLeaseApprovedActiveApplication = false;

		if( true == valArr( $arrobjActiveApplications ) ) {
			foreach( $arrobjActiveApplications as $objApplication ) {
				if( CApplicationStage::LEASE == $objApplication->getApplicationStageId() && CApplicationStatus::APPROVED == $objApplication->getApplicationStatusId() ) {
					$boolLeaseApprovedActiveApplication = true;
					break;
				}
			}
		}

		if( true == $boolLeaseApprovedActiveApplication || 0 < $arrintMaintenanceRequestsCount[0]['count'] || true == valArr( $arrobjActiveLeases ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Unit cannot be deleted because other information depends on it.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validateStudentUnitSpaces( $arrobjUnitSpaces ) {

		$boolIsValid = true;
		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrobjUnitSpaces ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'delete_unit_spaces', __( 'Unit space cannot be deleted as at least one space is required.' ), '' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valUnitNumber( $objDatabase, false, true );
				$boolIsValid &= $this->valNumberOfBedrooms();
				$boolIsValid &= $this->valNumberOfBathrooms();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUnitNumber( $objDatabase, false, true );
				$boolIsValid &= $this->valNumberOfBedrooms();
				$boolIsValid &= $this->valNumberOfBathrooms();
				$boolIsValid &= $this->valMaxOccupants( $objDatabase );
				$boolIsValid &= $this->valSquareFeet();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
		}

		return $boolIsValid;
	}

	/**
	* Other Functions
	*
	*/

	public function importYieldStarPricing( $objDatabase, $objProperty = NULL, $objClient = NULL, $intCurrentUserId = NULL ) {

		if( false == valObj( $objProperty, 'CProperty' ) || $objProperty->getId() != $this->getPropertyId() ) {
			$objProperty = $this->fetchProperty( $objDatabase );
		}

		if( false == valObj( $objClient, 'CClient' ) ) {
			$objClient = $objProperty->fetchClient( $objDatabase );
		}

		if( false == valArr( $objProperty->getPropertyPreferences() ) ) {
			$objPropertyPreference = $objProperty->fetchPropertyPreferenceByKey( 'IMPORT_REALTIME_PRICING_YIELDSTAR', $objDatabase );
			if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
				return true;
			}
		}

		$arrobjCompanyTransmissionVendors = $objProperty->fetchCompanyTransmissionVendorByTransmissionVendorIds( [ CTransmissionVendor::YIELDSTAR ], $objDatabase );

		$objCompanyTransmissionVendor	= NULL;
		$objPropertyTransmissionVendor 	= NULL;

		if( true == valArr( $arrobjCompanyTransmissionVendors ) ) {
			$objCompanyTransmissionVendor 	  = array_pop( $arrobjCompanyTransmissionVendors );
			$objPropertyTransmissionVendor    = $objProperty->fetchPropertyTransmissionVendorByCompanyTransmissionVendorId( $objCompanyTransmissionVendor->getId(), $objDatabase );
		}

		if( false == valObj( $objCompanyTransmissionVendor, 'CCompanyTransmissionVendor' ) || false == valObj( $objPropertyTransmissionVendor, 'CPropertyTransmissionVendor' ) ) {
			return true;
		}

		// Don't update pricing if updated in last 15 min
		$strSql = 'SELECT
						MAX( r.updated_on ) AS updated_on
					FROM
						rates r
					WHERE
						r.cid = ' . ( int ) $this->getCid() . '
						AND r.ar_cascade_id = ' . CArCascade::SPACE . '
						AND r.ar_trigger_id = ' . CArTrigger::MONTHLY . '
						AND r.ar_phase_id = ' . CArPhase::ADVERTISED . '
						AND r.is_renewal = FALSE
						AND r.ar_cascade_reference_id IN(
							SELECT
								us.id
							FROM
								unit_spaces us
							WHERE
								us.cid = ' . ( int ) $this->getCid() . '
								AND us.property_unit_id = ' . $this->getId() . '
								AND us.deleted_on IS NULL
					    );';

		$arrstrMixedData = fetchData( $strSql, $objDatabase );

		// Don't process the call if pricing is updated in last 15 mins or its afternoon and already have the today's pricing.
		if( true == valArr( $arrstrMixedData ) && true == isset( $arrstrMixedData[0]['updated_on'] ) && true == valStr( $arrstrMixedData[0]['updated_on'] ) ) {
			$objPricingSyncDateTime = new DateTime( $arrstrMixedData[0]['updated_on'] );
			$objCurrentDate 		= new DateTime();
			$intSeconds 			= $objCurrentDate->getTimestamp() - $objPricingSyncDateTime->getTimestamp();

			if( 15 > $intSeconds / 60 || ( date( 'm/d/Y' ) == $objPricingSyncDateTime->format( 'm/d/Y' ) && 11 < $objCurrentDate->format( 'H' ) ) ) {
				return true;
			}
		}

		$objGenericWorker = new CYieldStarOptimizationController();

		$objGenericWorker->setClient( $objClient );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setPropertyUnit( $this );
		$objGenericWorker->setCompanyTransmissionVendor( $objCompanyTransmissionVendor );
		$objGenericWorker->setPropertyTransmissionVendor( $objPropertyTransmissionVendor );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setForceIntegrate( true );
		$objGenericWorker->setCurrentCompanyUserId( $intCurrentUserId );
		$objGenericWorker->process();
	}

	public function disassociateData( $objDatabase, $boolReturnSqlOnly = false ) {

		// update/delete dependant data

		$strSql = '';

		$strSql .= 'UPDATE inspections i SET property_unit_id = NULL WHERE i.cid = ' . ( int ) $this->m_intCid . ' AND i.property_unit_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE maintenance_requests SET property_unit_id = NULL, unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND property_unit_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE ap_details SET property_unit_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND property_unit_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE resident_insurance_policies SET property_unit_id = NULL, unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND property_unit_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE customer_concierge_services SET property_unit_id = NULL, unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND property_unit_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE packages SET unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND unit_space_id IN ( SELECT us.id FROM unit_spaces us WHERE us.cid = ' . ( int ) $this->m_intCid . ' AND us.property_unit_id =' . ( int ) $this->getId() . ' );';
		$strSql .= 'UPDATE application_duplicates SET property_unit_id = NULL, unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND property_unit_id = ' . ( int ) $this->getId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		fetchData( $strSql, $objDatabase );

		return true;
	}

	public function fetchCompanyMediaFileByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByPropertyUnitIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true, $boolReturnSqlOnly = false ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function getOrFetchNotes( $objDatabase ) {
		if( false == valStr( $this->m_strNotes ) ) {
			$this->m_strNotes = \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByPropertyUnitIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		}

		return $this->m_strNotes;
	}

	public function checkIsHardous( $objDatabase ) {
		$objHazardNoteEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchLatestEventsByEventTypeIdByPropertyUnitIdByPropertyIdByCid( CEventType::HAZARD_NOTES, $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
		if( true === valObj( $objHazardNoteEvent, 'CEvent' ) && false === valId( $objHazardNoteEvent->getDefaultEventResultId() ) ) {
			return true;
		}
		return false;
	}

	public function resolveUnitHazard( $objCompanyUser, $intAssociatedEventId, $objDatabase ) {
		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::HAZARD_NOTES );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setPropertyId( $this->getPropertyId() );
		$objEvent->setPropertyUnitId( $this->getId() );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setCompanyUser( $objCompanyUser );
		$objEvent->setScheduledEndDatetime( date( 'Y-m-d 00:00:00' ) );
		$objEvent->setAssociatedEventId( $intAssociatedEventId );
		$objEvent->setDefaultEventResultId( CDefaultEventResult::COMPLETED );

		$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );
		if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
			$objEvent->setCompanyEmployee( $objCompanyEmployee );
		}

		$objEventLibrary->buildEventDescription( $this );

		if( false == $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function markUnitHazardous( $objCompanyUser, $strHazardNote, $strHazardEndDate, $objDatabase, $strEventDateTime = NULL ) {
		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::HAZARD_NOTES );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setPropertyId( $this->getPropertyId() );
		$objEvent->setPropertyUnitId( $this->getId() );
		if( false != valStr( $strEventDateTime ) ) {
			$objEvent->setEventDatetime( $strEventDateTime );
		} else {
			$objEvent->setEventDatetime( 'NOW()' );
		}

		$objEvent->setCompanyUser( $objCompanyUser );
		$objEvent->setScheduledEndDatetime( ( true === valStr( $strHazardEndDate ) ) ? date( 'Y-m-d 00:00:00', strtotime( $strHazardEndDate ) ) : NULL );
		$objEvent->setScheduledDatetime( 'NOW()' );
		$objEvent->setNotes( $strHazardNote );

		$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );
		if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
			$objEvent->setCompanyEmployee( $objCompanyEmployee );
		}

		$objEventLibrary->buildEventDescription( $this );

		$intEventId = $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase );
		if( false == valId( $intEventId ) ) {
			return false;
		}

		return $intEventId;
	}

}
?>