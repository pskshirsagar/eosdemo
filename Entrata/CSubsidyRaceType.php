<?php

class CSubsidyRaceType extends CBaseSubsidyRaceType {

	const AMERICAN_INDIAN_OR_ALASKA_NATIVE 			= 1;
	const ASIAN 									= 2;
	const BLACK_OR_AFRICAN_AMERICAN 				= 3;
	const NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER = 4;
	const WHITE 									= 5;
	const OTHER 									= 6;
	const DECLINED_TO_REPORT 						= 7;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static $c_arrstrSubsidyRaceTypeNames = [
		self::AMERICAN_INDIAN_OR_ALASKA_NATIVE			=> 'american indian or alaska native',
		self::ASIAN 									=> 'asian',
		self::BLACK_OR_AFRICAN_AMERICAN 				=> 'black or african american',
		self::NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER	=> 'native hawaiian or other pacific islander',
		self::WHITE										=> 'white',
		self::OTHER										=> 'other',
		self::DECLINED_TO_REPORT						=> 'declined to report'
	];

	public static function getSubsidizeRaceTypeIdByName( $strSubsidyRaceType ) {

		switch( \Psi\CStringService::singleton()->strtolower( $strSubsidyRaceType ) ) {
			case 'american indian or alaska native':
				$intSubsidyRaceTypeId = self::AMERICAN_INDIAN_OR_ALASKA_NATIVE;
				break;

			case 'asian':
				$intSubsidyRaceTypeId = self::ASIAN;
				break;

			case 'black or african american':
				$intSubsidyRaceTypeId = self::BLACK_OR_AFRICAN_AMERICAN;
				break;

			case 'native hawaiian or other pacific islander':
				$intSubsidyRaceTypeId = self::NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER;
				break;

			case 'white':
				$intSubsidyRaceTypeId = self::WHITE;
				break;

			case 'other':
				$intSubsidyRaceTypeId = self::OTHER;
				break;

			case 'declined to report':
				$intSubsidyRaceTypeId = self::DECLINED_TO_REPORT;
				break;

			default:
				$intSubsidyRaceTypeId = NULL;
				break;
		}

		return $intSubsidyRaceTypeId;
	}

}
?>