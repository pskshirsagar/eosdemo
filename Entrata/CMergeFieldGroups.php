<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMergeFieldGroups
 * Do not add any new functions to this class.
 */

class CMergeFieldGroups extends CBaseMergeFieldGroups {

	public static function fetchMergeFieldGroups( $strSql, $objDatabase ) {
		$arrobjMergeFieldGroups = self::fetchCachedObjects( $strSql, 'CMergeFieldGroup', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );

		if( false == is_null( $arrobjMergeFieldGroups ) ) {
			return $arrobjMergeFieldGroups;
		}

		$objDebug = CDebugLogger::getInstance( 'blank_merge_field_group_for_database_' . $objDatabase->getId() );

		$objDebug->writeLine( $strSql . PHP_EOL );
		$objDebug->writeLine( sha1( $strSql ) . PHP_EOL );
		$objDebug->writeLine( 'END' . PHP_EOL );

		return parent::fetchMergeFieldGroups( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMergeFieldGroup', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchPublishedMergeFieldGroupsByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		\Psi\CStringService::singleton()->sort( $arrintIds );

		$strSql = 'SELECT * FROM merge_field_groups WHERE is_published = 1 AND id IN (' . implode( ',', $arrintIds ) . ') ORDER BY is_external DESC, order_num';
		return self::fetchMergeFieldGroups( $strSql, $objDatabase );
	}

	public static function fetchPublishedMergeFieldGroupsByIdsByStateCode( $arrintIds, $strStateCode, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return [];

		\Psi\CStringService::singleton()->sort( $arrintIds );

		$strSql = 'SELECT
						mfg.*,
						smfg.order_num AS order_num
					FROM
						merge_field_groups AS mfg
						LEFT JOIN state_merge_field_groups AS smfg ON ( mfg.id = smfg.merge_field_group_id AND smfg.state_code ILIKE \'' . $strStateCode . '\' )
					WHERE
						mfg.is_published = 1
						AND mfg.id IN ( ' . implode( ',', $arrintIds ) . ' )
						ORDER BY mfg.is_external DESC, smfg.order_num, mfg.order_num';

		return self::fetchMergeFieldGroups( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldGroupsBySearchFilter( $arrstrFilteredExplodedSearch, $objDatabase ) {
		$strSql = 'SELECT
							mfg.id AS group_id,
							mfg.name AS group_name, 
							mfg.details ->> \'transmission_vendor_id\' AS transmission_vendor_id
						FROM
							merge_field_groups mfg 
						WHERE 
							mfg.name ILIKE \'%' . implode( '%\' OR mfg.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						ORDER BY mfg.id
						LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedMergeFieldGroups( $objDatabase ) {
		$strSql = 'SELECT * FROM merge_field_groups WHERE is_published = 1 ORDER BY name ASC';

		return self::fetchMergeFieldGroups( $strSql, $objDatabase );
	}

	public static function fetchAllMergeFieldGroups( $objDatabase ) {

		$strSql = 'SELECT *,row_number() OVER ( PARTITION BY is_external, details ->> \'transmission_vendor_id\' ORDER BY order_num, is_published DESC ) FROM merge_field_groups';

		return self::fetchMergeFieldGroups( $strSql, $objDatabase );
	}

	public static function fetchAllDefaultMergeFieldGroups( $objDatabase, $boolIsExternal = false ) {
		$strWhereCondition = '';

		if( true == $boolIsExternal ) {
			$strWhereCondition = 'AND mfg.is_external = 0';
		}

		$strSql = 'SELECT 
						mfg.*
						FROM merge_field_groups mfg
						JOIN default_merge_fields dmf ON ( mfg.id = dmf.merge_field_group_id )
					WHERE
						mfg.is_published = 1
						' . $strWhereCondition . '
					ORDER BY  mfg.is_external ASC, mfg.name ASC';

		return self::fetchMergeFieldGroups( $strSql, $objDatabase );
	}

    public static function fetchAllPublishedExternalMergeFieldGroups( CDatabase $objDatabase ) : array {
        $strSql = 'SELECT * FROM merge_field_groups WHERE is_published = 1 AND is_external =1 ORDER BY name ASC';

        return self::fetchMergeFieldGroups( $strSql, $objDatabase );
    }

	public static function fetchAllPublishedNonExternalMergeFieldGroups( CDatabase $objDatabase ) : array {
		$strSql = 'SELECT
                    mfg.*
                FROM
                    merge_field_groups mfg
                    JOIN merge_field_sub_groups mfsg ON mfg.id = mfsg.merge_field_group_id
                WHERE
                    mfg.is_published = 1
                    AND mfg.is_external = 0
                ORDER BY
                    mfg.name ASC';

		return self::fetchMergeFieldGroups( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldGroupsByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return [];

		$strSql = 'SELECT * FROM merge_field_groups WHERE id IN (' . implode( ',', $arrintIds ) . ') ';
		return self::fetchMergeFieldGroups( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldGroupCountById( $intId, $objDatabase ) {

		if( false == valId( $intId ) ) {
			return false;
		}

		$strWhere = 'WHERE id = ' . ( int ) $intId;

		return parent::fetchRowCount( $strWhere, 'merge_field_groups', $objDatabase );
	}

	public static function fetchMergeFieldGroupMaxOrderNumberByType( $objDatabase ) {
		$strSql = 'SELECT
						MAX( order_num )+1 AS max_order_num,details ->> \'transmission_vendor_id\' AS transmission_vendor_id
					FROM
						merge_field_groups
					GROUP BY details ->> \'transmission_vendor_id\' ';
		$arrintResponse = fetchData( $strSql, $objDatabase );
		return $arrintResponse;
	}

}
?>