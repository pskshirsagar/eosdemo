<?php

class CSubsidyVoucherRentalAssistanceType extends CBaseSubsidyVoucherRentalAssistanceType {

	const NO_RENTAL_ASSISTANCE	= 1;
	const HUD_MULTI_FAMILY_PROJECT_BASED_RENTAL_ASSISTANCE = 2;
	const HUD_SECTION_8_MODERATE_REHABILITATION = 3;
	const PUBLIC_HOUSING_OPERATING_SUBSIDY = 4;
	const HOME_TENANT_BASED_RENTAL_ASSISTANCE = 5;
	const HUD_HOUSING_CHOICE_VOUCHER = 6;
	const HUD_PROJECT_BASED_VOUCHER = 7;
	const USDA_SECTION_521_RENTAL_ASSISTANCE_PROGRAM = 8;
	const OTHER_FEDERAL_RENTAL_ASSISTANCE = 9;
	const NON_FEDERAL_RENTAL_ASSISTANCE = 10;

	public static $c_arrintRentAssistanceSourceTypeIds = [
		self::OTHER_FEDERAL_RENTAL_ASSISTANCE,
		self::NON_FEDERAL_RENTAL_ASSISTANCE
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>