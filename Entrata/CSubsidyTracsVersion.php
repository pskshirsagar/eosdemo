<?php

class CSubsidyTracsVersion extends CBaseSubsidyTracsVersion {

	const TRACS_2_0_2D	= 1;
	const TRACS_2_0_3A	= 2;

	public static $c_arrstrAllSubsidyTracsVersions = [
		self::TRACS_2_0_2D    => 'TRACSv202D',
		self::TRACS_2_0_3A    => 'TRACSv203A'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYearPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getTypeNameByTypeId( $intTypeId ) {
		return self::$c_arrstrAllSubsidyTracsVersions[$intTypeId];
	}

}
?>