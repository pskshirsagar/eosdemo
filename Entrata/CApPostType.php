<?php

class CApPostType extends CBaseApPostType {

	const NO_POST_REQUITED				= 1;
	const PO_POST_THROUGH_ASSET_SYSTEM	= 2;
	const PO_POST_DIRECTLY				= 3;
	const INVOICE_STANDARD_POSTING		= 4;
	const INVOICE_PURCHASES_CLEARING	= 5;
	const INVOICE_POST_TO_ASSET_SYSTEM	= 6;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>