<?php

use Psi\Libraries\UtilDates\CDates;

class CLeaseStartWindow extends CBaseLeaseStartWindow {

	protected $m_strLeaseTermName;
	protected $m_boolIsRenewal;
	protected $m_boolIsProspect;
	protected $m_intLeaseTermMonths;

	/**
	 * Get Functions
	 */

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getWindowEndDays() {
		// calculate the difference between the window end date and current date else return default value from rates.window_end_days
		$intWindowEndDays = 10000;

		if( true == valStr( $this->getEndDate() ) && time() < strtotime( $this->getEndDate() ) ) {
			$arrstrDateDifference = CDates::createService()->getDateDifference( $this->getEndDate(), date( 'm/d/Y' ) );

			if( 0 < $arrstrDateDifference['days_total'] ) {
				$intWindowEndDays = $arrstrDateDifference['days_total'];
			}
		}

		return $intWindowEndDays;
	}

	/**
	 * @return mixed
	 */
	public function getIsRenewal() {
		return $this->m_boolIsRenewal;
	}

	/**
	 * @return mixed
	 */
	public function getIsProspect() {
		return $this->m_boolIsProspect;
	}

	public function getLeaseTermMonths() {
		return $this->m_intLeaseTermMonths;
	}

	/**
	 * @return boolean
	 */
	public function isDisassociated() : bool {
		return ( bool ) $this->getDetailsField( 'is_disassociated' );
	}

	/**
	 * Set Functions
	 */

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setLeaseTermMonths( $intLeaseTermMonths ) {
		$this->m_intLeaseTermMonths = $intLeaseTermMonths;
	}

	/**
	 * @param mixed $boolIsRenewal
	 */
	public function setIsRenewal( $boolIsRenewal ) {
		$this->m_boolIsRenewal = CStrings::strToBool( $boolIsRenewal );
	}

	/**
	 * @param mixed $boolIsProspect
	 */
	public function setIsProspect( $boolIsProspect ) {
		$this->m_boolIsProspect = CStrings::strToBool( $boolIsProspect );
	}

	/**
	 * @param boolean $boolIsDisassociated
	 */
	public function setDisassociated( $boolIsDisassociated ) {
		$this->setDetailsField( 'is_disassociated', $boolIsDisassociated );
	}

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );
		if( true == isset( $arrmixValues['lease_term_name'] ) ) 	$this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		if( true == isset( $arrmixValues['is_renewal'] ) ) 	        $this->setIsRenewal( $arrmixValues['is_renewal'] );
		if( true == isset( $arrmixValues['is_prospect'] ) ) 	    $this->setIsProspect( $arrmixValues['is_prospect'] );
		if( true == isset( $arrmixValues['lease_term_months'] ) ) 	$this->setLeaseTermMonths( $arrmixValues['lease_term_months'] );
	}

	// Val Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartStructureId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOffsetStartDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOffsetEndDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {

		if( false == is_numeric( strtotime( $this->getStartDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getStartDate() ] ), $boolFormat = true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is not valid' ), NULL ) );
			return false;
		} elseif( true == is_null( $this->getStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date is not valid' ), NULL ) );
			return false;
		}

		return true;
	}

	public function valEndDate() {

		if( false == is_numeric( strtotime( $this->getEndDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getEndDate() ] ), $boolFormat = true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is not valid.' ), NULL ) );
			return false;
		} elseif( true == is_null( $this->getEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date is not valid' ), NULL ) );
			return false;
		}

		return true;
	}

	public function valBillingEndDate() {

		if( true == is_null( $this->getBillingEndDate() ) || ( false == is_numeric( strtotime( $this->getBillingEndDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getBillingEndDate() ] ), $boolFormat = true ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_end_date', __( 'Billing end date is not valid' ), NULL ) );
			return false;
		} elseif( false == is_null( $this->getBillingEndDate() ) && strtotime( $this->getBillingEndDate() ) < strtotime( $this->getStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_end_date', __( 'Billing end date should not be less than the lease term Start Date.' ), NULL ) );
			return false;
		}

		return true;
	}

	public function valRenewalStartDate() {

			if( true == is_null( $this->getRenewalStartDate() ) || false == is_numeric( strtotime( $this->getRenewalStartDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getRenewalStartDate() ] ), $boolFormat = true ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_start_date', __( 'Renewal start date is not valid.' ), NULL ) );
				return false;
			} elseif( strtotime( $this->getRenewalStartDate() ) >= strtotime( $this->getEndDate() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_start_date', __( 'Renewal start date should be less than the lease term end date.' ), NULL ) );
				return false;
			}

			return true;

	}

	public function valRenewalBillingStartDate() {

			if( true == is_null( $this->getRenewalBillingStartDate() ) || false == is_numeric( strtotime( $this->getRenewalBillingStartDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getRenewalBillingStartDate() ] ), $boolFormat = true ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_billing_start_date', __( 'Renewal billing start date is not valid' ), NULL ) );
				return false;
			} elseif( strtotime( $this->getRenewalBillingStartDate() ) >= strtotime( $this->getEndDate() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_billing_start_date', __( 'Renewal billing start date should be less than the lease term end date.' ), NULL ) );
				return false;
			}

			return true;
	}

	public function valLeaseExtensionDate( $boolIsAllowLeaseExtensions = false ) {
		if( false == $boolIsAllowLeaseExtensions || true == is_null( $this->getLeaseExtensionDate() ) ) return true;

		if( false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getLeaseExtensionDate() ] ), $boolFormat = true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_extension_date', __( 'Lease extension date is not valid.' ), NULL ) );

			return false;
		} elseif( strtotime( $this->getLeaseExtensionDate() ) <= strtotime( $this->getEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_extension_date', __( 'The Extension Date must be set at least one day after the All Lease End date.' ), NULL ) );

			return false;
		}
		return true;
	}

	public function valDateRange() {

		if( strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date should be greater than or equal to Start date.' ), NULL ) );
			return false;
		}

		return true;
	}

	public function valIsDateBased() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinDays() {
		$boolIsValid = true;
		$intDateDifference = ( int ) ( CDates::createService()->getDateDifference( $this->getStartDate(), $this->getEndDate() )['days_total'] + 1 );
		if( strtotime( $this->getStartDate() ) >= strtotime( $this->getEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date should be less than the lease term end date.' ), NULL ) );
			return false;
		} else if( true == is_null( $this->getMinDays() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_days', __( 'Minimum Stay Length is required.' ), NULL ) );
			return false;
		} else if( 0 >= ( int ) $this->getMinDays() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_days', __( 'Minimum Stay Length should be greater than zero.' ), NULL ) );
			return false;
		} else if( ( int ) $this->getMinDays() > $intDateDifference ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_days', __( 'Minimum Stay Length should be less than or equal to the lease term length ( {%d, 0} days ).', [ $intDateDifference ] ), NULL ) );
			return false;
		}
			return $boolIsValid;
	}

	public function valMaxDays() {
		$boolIsValid = true;
		$intDateDifference = ( int ) ( CDates::createService()->getDateDifference( $this->getStartDate(), $this->getEndDate() )['days_total'] + 1 );
		if( 0 >= ( int ) $this->getMaxDays() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_days', __( 'Maximum Stay Length should be greater than zero.' ), NULL ) );
			return false;
		} else if( ( int ) $this->getMaxDays() > $intDateDifference ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_days', __( 'Maximum Stay Length should be less than or equal to the lease term length ( {%d, 0} days ).', [ $intDateDifference ] ), NULL ) );
			return false;
		} else if( $this->getMinDays() > $this->getMaxDays() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_days', 'Maximum Stay Length must be greater than or equal to Minimum Stay Length.' ) );

			return false;
		}
		return $boolIsValid;

	}

	public function valIsDateGreaterThanCurrentDate() {
		$boolIsValid = true;

		if( strtotime( $this->getEndDate() ) < strtotime( date( 'm/d/Y' ) ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'Lease term end date must be greater than or equal to current date.' ), NULL ) );
			$boolIsValid &= false;

		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $arrobjLeaseStartWindows = NULL, $arrmixFlexibleLeaseStartWindows = NULL, $intLeaseStartWindowId = NULL, $boolIsAllowLeaseExtensions = false ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( VALIDATE_INSERT == $strAction && true == is_null( $this->getLeaseTermTypeId() ) && true == is_null( $this->getLeaseTermId() ) ) {
					$boolIsValid &= $this->valLeaseStartWindows( $arrobjLeaseStartWindows, $objDatabase ); // Always gets VALIDATE_INSERT for Lease start structure
				} else if( CLeaseTermType::DATE_BASED == ( int ) $this->getLeaseTermTypeId() ) {
					$boolIsValid &= $this->valStartDate();
					$boolIsValid &= $this->valEndDate();
					$boolIsValid &= $this->valBillingEndDate();
					$boolIsValid &= $this->valRenewalStartDate();
					$boolIsValid &= $this->valRenewalBillingStartDate();
					$boolIsValid &= $this->valLeaseExtensionDate( $boolIsAllowLeaseExtensions );

					if( true == $boolIsValid ) {
						$boolIsValid &= $this->valDateRange();
					}
				} else if( CLeaseTermType::FLEXIBLE == ( int ) $this->getLeaseTermTypeId() ) {
					$boolIsValid &= $this->valStartDate();
					$boolIsValid &= $this->valEndDate();
					$boolIsValid &= $this->valMinDays();
					$boolIsValid &= $this->valMaxDays();
					// skip for update
					if( VALIDATE_INSERT == $strAction ) {
						$boolIsValid &= $this->valIsDateGreaterThanCurrentDate();
					}

					if( true == $boolIsValid && true == valArr( $arrmixFlexibleLeaseStartWindows ) ) {
						$arrmixResult = $this->valIsOverlapWindow( $arrmixFlexibleLeaseStartWindows, $intLeaseStartWindowId, $boolIsValid );

						if( false == $arrmixResult['boolIsValid'] ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', $arrmixResult['message'], NULL ) );
							$boolIsValid &= false;
						}
					}
				}
				break;

			case 'validate_organization_contract':
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valDateRange();
				break;

			case 'validate_commercial_insert_or_update':
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valDateRange();
				break;

			case 'validate_commercial_mtom_insert_or_update':
				$boolIsValid = $this->valStartDate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valLeaseStartWindows( $arrobjLeaseStartWindows, $objDatabase ) {
		$boolIsValid = true;
		if( false == valArr( $arrobjLeaseStartWindows ) ) {
			return true;
		}

		$intCounter = 0;
		$intOffsetEndDay = 0;
		foreach( $arrobjLeaseStartWindows as $objLeaseStartWindow ) {
			if( NULL == $objLeaseStartWindow->getDeletedBy() && NULL == $objLeaseStartWindow->getDeletedOn() ) {
				if( true == is_null( $objLeaseStartWindow->getOffsetStartDays() ) || true == is_null( $objLeaseStartWindow->getOffsetEndDays() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offset', __( 'Offset start-end days are required .' ) ) );
					return false;
				} elseif( ( false == valId( $objLeaseStartWindow->getOffsetStartDays() ) && 0 != $objLeaseStartWindow->getOffsetStartDays() ) || false == valId( $objLeaseStartWindow->getOffsetEndDays() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offset', __( 'Offset start-end days should be integer only.' ) ) );
					return false;
				} else {
				    $intOffsetStartDay = $objLeaseStartWindow->getOffsetStartDays();

				    if( $intCounter > 0 ) {
				        if( $intOffsetStartDay != $intOffsetEndDay + 1 ) {
				            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offset', __( 'There should not be any gap or overlapped.' ) ) );
				            return false;
				        }

                        if( $objLeaseStartWindow->getOffsetEndDays() < $intOffsetStartDay ) {
                            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'offset', __( 'End day should not be less than start day.' ) ) );
                            return false;
                        }
				    }

				    $intOffsetEndDay = $objLeaseStartWindow->getOffsetEndDays();
				    $intCounter++;
				}
			}
		}

		return $boolIsValid;
	}

	public function valIsOverlapWindow( $arrmixFlexibleLeaseStartWindows, $intLeaseStartWindowId, $boolIsValid ) {
		$intIndexCnt = 0;

		foreach( $arrmixFlexibleLeaseStartWindows as $arrmixFlexibleLeaseStartWindow ) {
			$arrmixPropertyIds = json_decode( $arrmixFlexibleLeaseStartWindow['property_ids'], true );
			// check it should not compare to itself
			if( ( true == valArr( $arrmixPropertyIds ) ) && ( ( $intLeaseStartWindowId <> ( $intIndexCnt ) ) && ( true == in_array( $this->getPropertyId(), $arrmixPropertyIds ) ) ) ) {

					if( ( ( strtotime( $this->getStartDate() ) <= strtotime( $arrmixFlexibleLeaseStartWindow['start_date'] ) && strtotime( $this->getEndDate() ) >= strtotime( $arrmixFlexibleLeaseStartWindow['start_date'] ) ) || ( strtotime( $this->getStartDate() ) <= strtotime( $arrmixFlexibleLeaseStartWindow['end_date'] ) && strtotime( $this->getEndDate() ) >= strtotime( $arrmixFlexibleLeaseStartWindow['end_date'] ) ) ) || ( ( strtotime( $arrmixFlexibleLeaseStartWindow['start_date'] ) <= strtotime( $this->getStartDate() ) && strtotime( $arrmixFlexibleLeaseStartWindow['end_date'] ) >= strtotime( $this->getStartDate() ) ) || ( strtotime( $arrmixFlexibleLeaseStartWindow['start_date'] ) <= strtotime( $this->getEndDate() ) && strtotime( $arrmixFlexibleLeaseStartWindow['end_date'] ) >= strtotime( $this->getEndDate() ) ) ) ) {
						$strMessage = __( 'Lease term is overlapping with entered one.' );
						$boolIsValid &= false;
					}

			}
			$intIndexCnt++;
		}
		$arrmixResult['boolIsValid'] = $boolIsValid;
		$arrmixResult['message'] = $strMessage;
		return $arrmixResult;
	}

	public function createStudentPropertyWindowSetting() {

		$objStudentPropertyWindowSetting = new CStudentPropertyWindowSetting();
		$objStudentPropertyWindowSetting->setCid( $this->getCid() );
		$objStudentPropertyWindowSetting->setPropertyId( $this->getPropertyId() );
		$objStudentPropertyWindowSetting->setLeaseStartWindowId( $this->getId() );
		$objStudentPropertyWindowSetting->setLeaseTermId( $this->getLeaseTermId() );
		return $objStudentPropertyWindowSetting;
	}

}
?>
