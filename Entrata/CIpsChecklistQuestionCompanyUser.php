<?php

class CIpsChecklistQuestionCompanyUser extends CBaseIpsChecklistQuestionCompanyUser {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpsChecklistQuestionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function validateBulkIpsChecklistQuestionCompanyUsers( $arrobjIpsChecklistQuestionCompanyUsers ) {
		$boolIsValid = true;

			foreach( $arrobjIpsChecklistQuestionCompanyUsers as $objIpsChecklistQuestionCompanyUsers ) {
				if( false == valId( $objIpsChecklistQuestionCompanyUsers->getCompanyUserId() ) ) {
					$boolIsValid = false;
					break;
				}
			}

		return $boolIsValid;
	}

}
?>