<?php

class CArAllocation extends CBaseArAllocation {

	protected $m_objChargeArTransaction;
	protected $m_objCreditArTransaction;

	protected $m_intArCodeId;
	protected $m_intRepaymentId;
	protected $m_intArCodeTypeId;
	protected $m_intGlAccountTypeId;
	protected $m_intPastPostMonth;
	protected $m_intFuturePostMonth;
	protected $m_intRecoveryArCodeId;
	protected $m_intOffsetArAllocationId;

	protected $m_fltTransactionAmount;
	protected $m_fltTransactionAmountDue;

	protected $m_strArCodeName;
	protected $m_strHeaderNumber;
	protected $m_strDeleteDatetime;
	protected $m_strDeletePostDate;
	protected $m_strCreditPostDate;
	protected $m_strDeletePostMonth;
	protected $m_strCreditPostMonth;
	protected $m_strDefaultArCodeId;
	protected $m_strOffsettingDatetime;

	protected $m_boolOverridePostMonthPermission;
	protected $m_boolValidatePastOrFuturePostMonth;

	protected $m_boolAutoAllocationWithinSamePostMonth;

	public function __construct() {
		parent::__construct();

		$this->m_boolValidatePastOrFuturePostMonth = false;

		return;
	}

	/**
	 * Get Functions
	 */

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function getDefaultArCodeId() {
		return $this->m_intDefaultArCodeId;
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function getTransactionAmountDue() {
		return $this->m_fltTransactionAmountDue;
	}

	public function getOffsettingDatetime() {
		return $this->m_strOffsettingDatetime;
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function getFormattedAllocationDate() {
		if( true == is_null( $this->m_strAllocationDatetime ) ) return NULL;
		return date( 'm/d/Y', strtotime( $this->m_strAllocationDatetime ) );
	}

	public function getFormattedDeleteDate() {
		if( true == is_null( $this->m_strDeleteDatetime ) ) return NULL;
		return date( 'm/d/Y', strtotime( $this->m_strDeleteDatetime ) );
	}

	public function getFormattedPostMonth() {
		if( true == is_null( $this->m_strPostMonth ) ) return NULL;
		return \Psi\CStringService::singleton()->preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getFormattedDeletePostMonth() {
		if( true == is_null( $this->m_strDeletePostMonth ) ) return NULL;
		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strDeletePostMonth );
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function getDeleteDatetime() {
		return $this->m_strDeleteDatetime;
	}

	public function getDeletePostDate() {
		return $this->m_strDeletePostDate;
	}

	public function getDeletePostMonth() {
		return $this->m_strDeletePostMonth;
	}

	public function getOverridePostMonthPermission() {
		return $this->m_boolOverridePostMonthPermission;
	}

	public function getRepaymentId() {
		return $this->m_intRepaymentId;
	}

	public function getPastPostMonth() {
		return $this->m_intPastPostMonth;
	}

	public function getFuturePostMonth() {
		return $this->m_intFuturePostMonth;
	}

	public function getValidatePastOrFuturePostMonth() {
		return $this->m_boolValidatePastOrFuturePostMonth;
	}

	public function getHeaderNumber() {
		return $this->m_strHeaderNumber;
	}

	public function getOffsetArAllocationId() {
		return $this->m_intOffsetArAllocationId;
	}

	public function getRecoveryArCodeId() {
		return $this->m_intRecoveryArCodeId;
	}

	public function getCreditPostMonth() {
		$this->m_strCreditPostMonth;
	}

	public function getCreditPostDate() {
		$this->m_strCreditPostDate;
	}

	public function getChargeArTransaction() {
		return $this->m_objChargeArTransaction;
	}

	public function getCreditArTransaction() {
		return $this->m_objCreditArTransaction;
	}

	public function getAutoAllocationWithinSamePostMonth() : bool {
		return ( bool ) $this->m_boolAutoAllocationWithinSamePostMonth;
	}

	/**
	 * Set Functions
	 */

	/** function sqlAllocationAmount() {
	 *   return ( string ) abs( $this->m_fltAllocationAmount ) * - 1;
	}*/
	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->m_intArCodeTypeId = $intArCodeTypeId;
	}

	public function setDefaultArCodeId( $intDefaultArCodeId ) {
		$this->m_intDefaultArCodeId = CStrings::strTrimDef( $intDefaultArCodeId, 240, NULL, true );
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = CStrings::strTrimDef( $strArCodeName, 240, NULL, true );
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
	   $this->m_fltTransactionAmount = ( ( float ) ( CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 4 ) ) );
	}

	public function setTransactionAmountDue( $fltTransactionAmountDue ) {
	   $this->m_fltTransactionAmountDue = ( ( float ) CStrings::strToFloatDef( $fltTransactionAmountDue, NULL, false, 4 ) );
	}

	public function setAllocationAmount( $fltAllocationAmount ) {
		$fltAllocationAmount = str_replace( '$', '', $fltAllocationAmount );
		$this->m_fltAllocationAmount = CStrings::strToFloatDef( $fltAllocationAmount, NULL, false, 2 );
	}

	public function setOffsettingDatetime( $strOffsettingDatetime ) {
		$this->m_strOffsettingDatetime = $strOffsettingDatetime;
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->m_intGlAccountTypeId = $intGlAccountTypeId;
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->m_intGlTransactionTypeId = $intGlTransactionTypeId;
	}

	public function setArCodeId( $intArCodeId ) {
		$this->m_intArCodeId = $intArCodeId;
	}

	public function setPostMonth( $strPostMonth ) {
		$arrstrPostMonth = explode( '/', $strPostMonth );
		if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
			$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
		}

		$this->m_strPostMonth = $strPostMonth;
	}

	public function setDeleteDatetime( $strDeleteDatetime ) {
		$this->m_strDeleteDatetime = CStrings::strTrimDef( $strDeleteDatetime, -1, NULL, true );
	}

	public function setDeletePostDate( $strDeletePostDate ) {
		$this->m_strDeletePostDate = CStrings::strTrimDef( $strDeletePostDate, -1, NULL, true );
	}

	public function setDeletePostMonth( $strDeletePostMonth ) {
		$this->m_strDeletePostMonth = CStrings::strTrimDef( $strDeletePostMonth, -1, NULL, true );
	}

	public function setOverridePostMonthPermission( $boolOverridePostMonthPermission ) {
		$this->m_boolOverridePostMonthPermission = $boolOverridePostMonthPermission;
	}

	public function setRepaymentId( $intRepaymentId ) {
		$this->m_intRepaymentId = $intRepaymentId;
	}

	public function setPastPostMonth( $intPastPostMonth ) {
		$this->m_intPastPostMonth = $intPastPostMonth;
	}

	public function setFuturePostMonth( $intFuturePostMonth ) {
		$this->m_intFuturePostMonth = $intFuturePostMonth;
	}

	public function setValidatePastOrFuturePostMonth( $boolValidatePastOrFuturePostMonth ) {
		$this->m_boolValidatePastOrFuturePostMonth = ( bool ) $boolValidatePastOrFuturePostMonth;
	}

	public function setHeaderNumber( $strHeaderNumber ) {
		$this->m_strHeaderNumber = $strHeaderNumber;
	}

	public function setOffsetArAllocationId( $intOffsetAllocationId ) {
		$this->m_intOffsetArAllocationId = $intOffsetAllocationId;
	}

	public function setChargeArTransaction( CArTransaction $objChargeArTransaction ) {
		$this->m_objChargeArTransaction = $objChargeArTransaction;
	}

	public function setCreditArTransaction( CArTransaction $objCreditArTransaction ) {
		$this->m_objCreditArTransaction = $objCreditArTransaction;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_code_name'] ) ) $this->setArCodeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['ar_code_name'] ) : $arrmixValues['ar_code_name'] );
		if( true == isset( $arrmixValues['transaction_amount_due'] ) ) $this->setTransactionAmountDue( $arrmixValues['transaction_amount_due'] );
		if( true == isset( $arrmixValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrmixValues['transaction_amount'] );
		if( true == isset( $arrmixValues['allocation_amount'] ) ) $this->setAllocationAmount( $arrmixValues['allocation_amount'] );
		if( true == isset( $arrmixValues['offsetting_datetime'] ) ) $this->setOffsettingDatetime( $arrmixValues['offsetting_datetime'] );
		if( true == isset( $arrmixValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrmixValues['gl_account_type_id'] );
		if( true == isset( $arrmixValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrmixValues['gl_transaction_type_id'] );
		if( true == isset( $arrmixValues['default_ar_code_id'] ) ) $this->setDefaultArCodeId( $arrmixValues['default_ar_code_id'] );
		if( true == isset( $arrmixValues['ar_code_id'] ) ) $this->setArCodeId( $arrmixValues['ar_code_id'] );
		if( true == isset( $arrmixValues['delete_datetime'] ) ) $this->setDeleteDatetime( $arrmixValues['delete_datetime'] );
		if( true == isset( $arrmixValues['delete_post_date'] ) ) $this->setDeletePostDate( $arrmixValues['delete_post_date'] );
		if( true == isset( $arrmixValues['delete_post_month'] ) ) $this->setDeletePostMonth( $arrmixValues['delete_post_month'] );
		if( true == isset( $arrmixValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrmixValues['ar_code_type_id'] );
		if( true == isset( $arrmixValues['repayment_id'] ) ) $this->setRepaymentId( $arrmixValues['repayment_id'] );
		if( true == isset( $arrmixValues['header_number'] ) ) $this->setHeaderNumber( $arrmixValues['header_number'] );
		if( true == isset( $arrmixValues['offset_allocation_id'] ) ) $this->setOffsetArAllocationId( $arrmixValues['offset_allocation_id'] );
		if( true == isset( $arrmixValues['recovery_ar_code_id'] ) ) $this->setRecoveryArCodeId( $arrmixValues['recovery_ar_code_id'] );
		if( true == isset( $arrmixValues['credit_post_month'] ) ) $this->setCreditPostMonth( $arrmixValues['credit_post_month'] );
		if( true == isset( $arrmixValues['credit_post_date'] ) ) $this->setCreditPostDate( $arrmixValues['credit_post_date'] );

		return;
	}

	public function setDataForValidatePastOrFuturePostMonth( $arrmixDataToValidatePastOrFuturePostMonth ) {
		$this->setPastPostMonth( getArrayElementByKey( 'past_post_month', $arrmixDataToValidatePastOrFuturePostMonth ) );
		$this->setFuturePostMonth( getArrayElementByKey( 'future_post_month', $arrmixDataToValidatePastOrFuturePostMonth ) );
		$this->setValidatePastOrFuturePostMonth( getArrayElementByKey( 'is_validate_past_or_future_post_month', $arrmixDataToValidatePastOrFuturePostMonth ) );
	}

	public function setRecoveryArCodeId( $intRecoveryArCodeId ) {
		$this->m_intRecoveryArCodeId = $intRecoveryArCodeId;
	}

	public function setCreditPostMonth( $strCreditPostMonth ) {
		$this->m_strCreditPostMonth = CStrings::strTrimDef( $strCreditPostMonth, -1, NULL, true );
	}

	public function setCreditPostDate( $strCreditPostDate ) {
		$this->m_strCreditPostDate = CStrings::strTrimDef( $strCreditPostDate, -1, NULL, true );
	}

	public function setAutoAllocationWithinSamePostMonth( bool $boolAutoAllocationWithinSamePostMonth ) {
		$this->m_boolAutoAllocationWithinSamePostMonth = $boolAutoAllocationWithinSamePostMonth;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction, $objDatabase = NULL, $objPropertyGlSetting ) {
		$boolIsValid = true;

		$objArAllocationValidator = new CArAllocationValidator();
		$objArAllocationValidator->setArAllocation( $this );
		$boolIsValid &= $objArAllocationValidator->validate( $strAction, $objDatabase, $objPropertyGlSetting );

		return $boolIsValid;
	}

	/**
	 * Validation Functions
	 */

	public function loadDefaultDeleteData( $objPropertyGlSetting, $strDeletePostDate = NULL, $strDeletePostMonth = NULL ) {

	$strDeleteDatetime 			= date( 'm/d/Y H:i:s' );

	// If delete post data is not set, set to property gl setting and today
	if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
	$strDeletePostMonth 	= ( true == is_null( $strDeletePostMonth ) ) ? $objPropertyGlSetting->getArPostMonth() : $strDeletePostMonth;
	$strDeletePostDate 		= ( true == is_null( $strDeletePostDate ) ) ? date( 'm/d/Y' ) : $strDeletePostDate;
	}

	// Make sure the delete post month and date fall before the post_date and post month of the original allocation.
	$strDeletePostMonth 		= ( strtotime( $strDeletePostMonth ) >= strtotime( $this->getPostMonth() ) ) ? $strDeletePostMonth : $this->getPostMonth();
	$strDeletePostDate 			= ( strtotime( $strDeletePostDate ) >= strtotime( $this->getPostDate() ) ) ? $strDeletePostDate : $this->getPostDate();

	$this->setDeletePostMonth( $strDeletePostMonth );
	$this->setDeleteDatetime( $strDeleteDatetime );
	$this->setDeletePostDate( $strDeletePostDate );

	return;
	}

	/**
	 * Post Functions
	 */

	private function postArAllocation( $strSql, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		$intOldId = $this->getId();

		if( true == is_null( $intOldId ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		if( false == $objDataset->execute( sprintf( $strSql, $this->getId() ) ) ) {
			$this->setId( $intOldId );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->setId( $intOldId );

			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();
			return false;
		}

		$objDataset->cleanup();
		return true;
	}

	public function postAllocation( $intCurrentUserId, $objDatabase ) {

		$strSql = 'SELECT * FROM ar_allocations_post_allocation( ' .
					'%d, ' .
					$this->sqlCid() . ', ' .
					$this->sqlPropertyId() . ', ' .
					$this->sqlLeaseId() . ', ' .
					$this->sqlGlTransactionTypeId() . ', ' .
					$this->sqlChargeArTransactionId() . ', ' .
					$this->sqlCreditArTransactionId() . ', ' .
					$this->sqlOriginArAllocationId() . ', ' .
					$this->sqlArProcessId() . ', ' .
					$this->sqlAllocationAmount() . ', ' .
					$this->sqlPostDate() . ', ' .
					$this->sqlPostMonth() . ', ' .
					$this->sqlIsInitialImport() . ', ' .
					( int ) $intCurrentUserId . ' ) AS result;';

		return $this->postArAllocation( $strSql, $objDatabase );
	}

	public function generateDeleteArAllocation() {

		$objDeleteArAllocation = clone $this;
		$objDeleteArAllocation->setOriginArAllocationId( $this->getId() );
		$objDeleteArAllocation->setId( NULL );
		$objDeleteArAllocation->setGlTransactionTypeId( CGlTransactionType::AR_ALLOCATION_REVERSAL );
		$objDeleteArAllocation->setAccrualDebitGlAccountId( NULL );
		$objDeleteArAllocation->setAccrualCreditGlAccountId( NULL );
		$objDeleteArAllocation->setCashDebitGlAccountId( NULL );
		$objDeleteArAllocation->setCashCreditGlAccountId( NULL );
		$objDeleteArAllocation->setArProcessId( NULL );

		if( true == is_null( $this->getDeleteDatetime() ) ) {
			$objDeleteArAllocation->setDeleteDatetime( 'NOW()' );
		}

		$objDeleteArAllocation->setAllocationDatetime( $this->getDeleteDatetime() );
		$objDeleteArAllocation->setAllocationAmount( $this->getAllocationAmount() * -1 );
		$objDeleteArAllocation->setPostDate( $this->getDeletePostDate() );
		$objDeleteArAllocation->setPostMonth( $this->getDeletePostMonth() );
		$objDeleteArAllocation->setCreditPostToCash( NULL );
		$objDeleteArAllocation->setChargePostToCash( NULL );
		$objDeleteArAllocation->setIsPosted( false );
		$objDeleteArAllocation->setIsInitialImport( false );
		$objDeleteArAllocation->setUpdatedBy( NULL );
		$objDeleteArAllocation->setUpdatedOn( NULL );
		$objDeleteArAllocation->setCreatedBy( NULL );
		$objDeleteArAllocation->setCreatedOn( NULL );

		return $objDeleteArAllocation;
	}

	// We should always be soft deleting pre-payment allocations, so that we don't cascade delete GL Detail records that show old liabilities.
	// This will totally screw up the ability to generate historical balance sheet reports if we ever hard delete pre-payment allocations.

	public function delete( $intCurrentUserId, $objDatabase, $boolPeformRapidDelete = false ) {

		if( true == $boolPeformRapidDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setIsDeleted( 1 );
		if( false == $this->update( $intCurrentUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		$objDeleteArAllocation = $this->generateDeleteArAllocation();

		if( false == $objDeleteArAllocation->insert( $intCurrentUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objDeleteArAllocation->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function isExported( $objDatabase ) {

		if( false == valId( $this->getId() ) ) {
			return NULL;
		}

		$arrobjGlDetails = ( array ) CGlDetails::fetchArGlDetailsByReferenceIdsByGlTransactionTypeIdsByCid( [ $this->getId() ], [ CGlTransactionType::AR_ALLOCATION_REVERSAL ], $this->getCId(), $objDatabase, $boolExportedOnly = true );

		if( true == valArr( $arrobjGlDetails ) ) {
			return true;
		}

		return false;
	}

}
?>