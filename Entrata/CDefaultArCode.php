<?php

class CDefaultArCode extends CBaseDefaultArCode {

	protected $m_strGlAccountTypeName;
	protected $m_strGlAccountName;

	const PAYMENT                        = 1101;
	const HAP_PAYMENT                    = 1102;
	const BAH_PAYMENT                    = 1103;
	const RENT                           = 1201;
	const MONTH_TO_MONTH_RENT            = 1202;
	const ACCELERATED_RENT               = 1203;
	const SUBSIDY_RENT                   = 1204;
	const UTILITY_ALLOWANCE              = 1205;
	const MILITARY_RENT                  = 1206;
	const LATE_FEE                       = 1301;
	const LATE_FEE_PER_DAY               = 1302;
	const DEPOSIT_INTEREST               = 1303;
	const INSUFFICIENT_NOTICE            = 1304;
	const EARLY_TERMINATION              = 1305;
	const RETURN_ITEM_FEE                = 1306;
	const MONTH_TO_MONTH_FEE             = 1307;
	const TRANSFER_FEE                   = 1308;
	const APPLICATION_FEE                = 1309;
	const REPAYMENT_AGREEMENT            = 1310;
	const OVERPAID_UTILITY_REIMBURSEMENT = 1311;
	const HUDWRITEOFF                    = 1312;
	const COLLECTION_COST_INCURRED       = 1313;
	const MILITARY_CONCESSION            = 1314;
	const ADJUSTMENT                     = 1397;
	const BEGINNING_BALANCE              = 1398;
	const BEGINNING_DEPOSIT_ADJUSTMENT   = 1399;
	const BEGINNING_DEPOSIT_HELD         = 1501;
	const SECURITY_DEPOSIT               = 1502;
	const REFUND_PAYABLE                 = 1701;
	const SUBSIDY_REFUND_PAYABLE         = 1702;
	const PET_RENT                       = 3201;
	const PET_DEPOSIT                    = 3501;
	const GARAGE                         = 4201;
	const PARKING                        = 4202;
	const APPLIANCES                     = 4203;
	const FURNITURE                      = 4204;
	const STORAGE                        = 4205;
	const RENT_RISK_PREMIUM              = 5201;
	const DEPOSIT_RISK_PREMIUM           = 5501;
	const CONCESSION                     = 6201;
	const TRASH_REMOVAL                  = 7301;
	const GAS                            = 7302;
	const ELECTRIC                       = 7303;
	const WATER                          = 7304;
	const SEWER                          = 7305;
	const HUD_COLLECTION_FEES            = 7306;
	const DAMAGES                        = 8301;
	const TAX                            = 9601;
	const HUDOVERPMT                     = 9602;
	const REIMBURSEMENTS                 = 19601;
	const UTILITY_REIMBURSEMENT          = 1401;
	const ENTITY_DRAW_REQUEST            = 10001;
	const INVESTOR_LOAN_REQUEST          = 10002;
	const GROUP_REFUND_PAYABLE           = 1703;
	const MILITARY_REFUND_PAYABLE        = 1704;

	public static $c_arrintReservedDefaultArCodeIds = [
		self::PAYMENT,
		self::BEGINNING_BALANCE,
		self::BEGINNING_DEPOSIT_HELD,
		self::REFUND_PAYABLE,
		self::BEGINNING_DEPOSIT_ADJUSTMENT,
		self::ACCELERATED_RENT
	];

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function setGlAccountTypeName( $strGlAccountTypeName ) {
		$this->m_strGlAccountTypeName = CStrings::strTrimDef( $strGlAccountTypeName, 50, NULL, true );
	}

	public function getGlAccountTypeName() {
		return $this->m_strGlAccountTypeName;
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = CStrings::strTrimDef( $strGlAccountName, 50, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['gl_account_type_name'] ) ) {
			$this->setGlAccountTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_account_type_name'] ) : $arrmixValues['gl_account_type_name'] );
		}
		if( true == isset( $arrmixValues['gl_account_name'] ) ) {
			$this->setGlAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_account_name'] ) : $arrmixValues['gl_account_name'] );
		}
	}

	public function createArCode() {

		$objArCode = new CArCode();

		$objArCode->setDefaultArCodeId( $this->getId() );
		$objArCode->setArCodeTypeId( $this->getArCodeTypeId() );
		$objArCode->setGlAccountTypeId( $this->getGlAccountTypeId() );
		$objArCode->setDefaultArCodeId( $this->getId() );
		$objArCode->setName( $this->getName() );
		$objArCode->setDescription( $this->getDescription() );
		$objArCode->setDefaultAmount( $this->getDefaultAmount() );
		$objArCode->setCaptureDelayDays( $this->getCaptureDelayDays() );
		$objArCode->setHideCredits( $this->getHideCredits() );
		$objArCode->setWaiveLateFees( $this->getWaiveLateFees() );
		$objArCode->setHideCharges( $this->getHideCharges() );
		$objArCode->setPostToCash( $this->getPostToCash() );
		$objArCode->setProrateCharges( $this->getProrateCharges() );
		$objArCode->setIsSystem( $this->getIsSystem() );
		$objArCode->setArOriginId( $this->getArOriginId() );
		$objArCode->setIsPaymentInKind( $this->getIsPaymentInKind() );
		$objArCode->setIsReserved( $this->getIsReserved() );
		$objArCode->setIncludeInEffectiveRent( $this->getIncludeInEffectiveRent() );

		return $objArCode;
	}

}

?>