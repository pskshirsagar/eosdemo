<?php

class CRoommate extends CBaseRoommate {

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPhoneNumber;
	protected $m_strBirthDate;
	protected $m_strCustomerNameFull;
	protected $m_strLastInvitationSentOn;

	protected $m_intCustomerTypeId;
	protected $m_intPropertyId;
	protected $m_intInvitedRoommateApplicantId;
	protected $m_intInvitedRoommateApplicationId;
	protected $m_intRoommateGroupLeaseId;

	protected $m_boolIsFromDifferentFloorPlanOrLeaseTerm;

     /**
     * Get Functions
     *
     */

    public function getNameFirst() {
    	return $this->m_strNameFirst;
    }

    public function getNameLast() {
    	return $this->m_strNameLast;
    }

    public function getPhoneNumber() {
    	return $this->m_strPhoneNumber;
    }

    public function getBirthDate() {
    	return $this->m_strBirthDate;
    }

    public function getCustomerTypeId() {
    	return $this->m_intCustomerTypeId;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

	public function getInvitedRoommateApplicantId() {
		return $this->m_intInvitedRoommateApplicantId;
	}

	public function getInvitedRoommateApplicationId() {
		return $this->m_intInvitedRoommateApplicationId;
	}

	public function getCustomerNameFull() {
		return $this->m_strCustomerNameFull;
	}

	public function getIsFromDifferentFloorPlanOrLeaseTerm() {
    	return $this->m_boolIsFromDifferentFloorPlanOrLeaseTerm;
	}

	public function getLastInvitationSentOn() {
		return $this->m_strLastInvitationSentOn;
	}

	public function getRoommateGroupLeaseId() {
		return $this->m_intRoommateGroupLeaseId;
	}

    /**
    * Set Functions
    *
    */

    public function setNameFirst( $strNameFirst ) {
    	$this->m_strNameFirst = $strNameFirst;
    }

    public function setNameLast( $strNameLast ) {
    	$this->m_strNameLast = $strNameLast;
    }

    public function setPhoneNumber( $strPhoneNumber ) {
    	$this->m_strPhoneNumber = $strPhoneNumber;
    }

    public function setBirthDate( $strBirthDate ) {
    	$this->m_strBirthDate = CStrings::strTrimDef( $strBirthDate, -1, NULL, true );
    }

   	public function setCustomerTypeId( $intCustomerTypeId ) {
    	$this->m_intCustomerTypeId = $intCustomerTypeId;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

	public function setInvitedRoommateApplicantId( $intInvitedRoommateApplicantId ) {
		$this->m_intInvitedRoommateApplicantId = $intInvitedRoommateApplicantId;
	}

	public function setInvitedRoommateApplicationId( $intInvitedRoommateApplicationId ) {
    	$this->m_intInvitedRoommateApplicationId = $intInvitedRoommateApplicationId;
	}

    public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

    	if( true == isset( $arrmixValues['name_first'] ) )   			      $this->setNameFirst( $arrmixValues['name_first'] );
    	if( true == isset( $arrmixValues['name_last'] ) )    			      $this->setNameLast( $arrmixValues['name_last'] );
    	if( true == isset( $arrmixValues['phone_number'] ) )    		      $this->setPhoneNumber( $arrmixValues['phone_number'] );
    	if( true == isset( $arrmixValues['birth_date'] ) ) 				      $this->setBirthDate( $arrmixValues['birth_date'] );
    	if( true == isset( $arrmixValues['customer_type_id'] ) ) 		      $this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
    	if( true == isset( $arrmixValues['property_id'] ) ) 			      $this->setPropertyId( $arrmixValues['property_id'] );
	    if( true == isset( $arrmixValues['customer_name_full'] ) ) 		      $this->setCustomerNameFull( $arrmixValues['customer_name_full'] );
	    if( true == isset( $arrmixValues['invited_roommate_applicant_id'] ) ) $this->setInvitedRoommateApplicantId( $arrmixValues['invited_roommate_applicant_id'] );
	    if( true == isset( $arrmixValues['last_invitation_sent_on'] ) )       $this->setLastInvitationSentOn( $arrmixValues['last_invitation_sent_on'] );
	    if( true == isset( $arrmixValues['is_from_different_fp_lt'] ) ) $this->setIsFromDifferentFloorPlanOrLeaseTerm( $arrmixValues['is_from_different_fp_lt'] );
	    if( true == isset( $arrmixValues['roommate_group_lease_id'] ) ) $this->setRoommateGroupLeaseId( $arrmixValues['roommate_group_lease_id'] );
    }

	public function setCustomerNameFull( $strCustomerNameFull ) {
		$this->m_strCustomerNameFull = $strCustomerNameFull;
	}

	public function setIsFromDifferentFloorPlanOrLeaseTerm( $boolIsFromDifferentFloorPlanOrLeaseTerm ) {
		$this->m_boolIsFromDifferentFloorPlanOrLeaseTerm = $boolIsFromDifferentFloorPlanOrLeaseTerm;
	}

	public function setRoommateGroupLeaseId( $intLeaseId ) {
		$this->m_intRoommateGroupLeaseId = $intLeaseId;
	}

	public function setLastInvitationSentOn( $strLastInvitationSentOn ) {
		$this->m_strLastInvitationSentOn = $strLastInvitationSentOn;
	}

    /**
    * Validation Functions
    *
    */

    public function valId() {
        $boolIsValid = true;
        if( true == is_null( $this->getId() ) || 0 >= ( int ) $this->getId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Roommate id required' ) ) );
        }
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'client id required.' ) ) );
		}

		return $boolIsValid;
    }

    public function valRoommateGroupId() {
        $boolIsValid = true;

		if( true == is_null( $this->getRoommateGroupId() ) || 0 >= ( int ) $this->getRoommateGroupId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Roommate group id required.' ) ) );
		}

		return $boolIsValid;
    }

    public function valParentLeaseId() {
        $boolIsValid = true;

		if( true == is_null( $this->getParentLeaseId() ) || 0 >= ( int ) $this->getParentLeaseId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Parent lease id required.' ) ) );
		}

		return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailAddress( $objDatabase, $boolIsValidateInvitationAlreadySent = true, $strCurrentRoommateEmailAddress = NULL ) {
        $boolIsValid = true;

        if( true == is_null( $this->getEmailAddress() ) || 0 == strlen( trim( $this->getEmailAddress() ) ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is required.' ) ) );
        } elseif( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address does not appear to be valid.' ) ) );
        } elseif( true == $boolIsValidateInvitationAlreadySent && false == is_null( $strCurrentRoommateEmailAddress ) && $this->getEmailAddress() == $strCurrentRoommateEmailAddress ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'You cannot invite yourself.' ) ) );
        } elseif( true == $boolIsValidateInvitationAlreadySent && true == is_numeric( $this->getRoommateGroupId() ) ) {

	        $objApplicant = CApplicants::fetchApplicantByNameFirstByNameLastByUsernameByCid( $this->getNameFirst(), $this->getNameLast(), $this->getEmailAddress(), $this->getCid(), $objDatabase );

	        if( true == valObj( $objApplicant, 'CApplicant' ) ) {

		        $strWhereSql = ' WHERE
								cid =' . ( int ) $this->getCid() . '
								AND trim( both \' \' from lower( email_address ) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $this->getEmailAddress() ) ) ) . '\'
								AND roommate_group_id =' . ( int ) $this->getRoommateGroupId() . '
								AND deleted_on IS NULL';
		        $strWhereSql .= ( ( 0 < $this->getId() ) ? ( ' AND id <> ' . ( int ) $this->getId() ) : '' );

		        if( 0 < CRoommates::fetchRoommateCount( $strWhereSql, $objDatabase ) ) {
			        $boolIsValid = false;
			        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'An invitation has already been sent to this email address.' ) ) );
		        }
	        }
        }

        return $boolIsValid;
    }

    public function valDuplicateRoommate( $objDatabase, $boolIsFromProspectPortal = false ) {
    	$boolIsValid = true;

    	if( true == is_numeric( $this->getLeaseId() ) ) {

    		$strWhereSql = 'WHERE
    							cid = ' . ( int ) $this->getCid() . '
    							AND lease_id = ' . ( int ) $this->getLeaseId() .
								( ( true == valId( $this->getRoommateGroupId() ) ) ? ' AND roommate_group_id <> ' . $this->getRoommateGroupId() : '' ) . '
    							AND confirmed_by IS NOT NULL
    							AND deleted_by IS NULL';

    		if( 0 < CRoommates::fetchRoommateCount( $strWhereSql, $objDatabase ) ) {
    			$boolIsValid = false;
    			if( false == $boolIsFromProspectPortal ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s, 0} is already connected to another group, he/she must leave the current group to become part of this group.', [ $this->getEmailAddress() ] ) ) );
    			} else {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'You are already connected to another group. You must leave your current group to become part of this group.' ) ) );
    			}
    		}
    	}
    	return $boolIsValid;
    }

    public function valOccupantLimit( $intPropertyFloorplanId = NULL, $intPropertyId = NULL, $boolIsInvitationAccepted, $objDatabase ) {
    	$boolIsValid = true;
    	$intMaxOccupants = 0;

    	if( true == is_numeric( $intPropertyFloorplanId ) ) {
	    	$intMaxOccupants = \Psi\Eos\Entrata\CUnitTypes::createService()->fetchMinimumCountForMaximumOccupantsByPropertyFloorPlanIdByCid( $intPropertyFloorplanId, $this->getCid(), $objDatabase );
	    }

    	if( 0 == $intMaxOccupants ) {
    		if( false == is_null( $intPropertyId ) ) {
    			$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'MAXIMUM_OCCUPANTS_ALLOWED', $intPropertyId, $this->getCid(), $objDatabase );
				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
    				$intMaxOccupants = $objPropertyPreference->getValue();
				}
    		}
    	}

    	if( 0 == $intMaxOccupants ) $intMaxOccupants = 10;

    	if( true == is_null( $this->getRoommateGroupId() ) ) {
    		$intRoommatesCount = 1;
    	} else {
    		$intRoommatesCount 	= ( int ) CRoommates::fetchRoommatesCountByCidByRoommateGroupId( $this->getCid(), $this->getRoommateGroupId(), $boolIsInvitationAccepted, $objDatabase );
        }

    	if( $intMaxOccupants <= $intRoommatesCount ) {
    		$boolIsValid = false;
    		if( false == $boolIsInvitationAccepted ) {
				$strErrorMsg = __( 'The maximum number of roommate invitations have been sent, you cannot invite more than {%d, 0} roommates.', [ $intMaxOccupants ] );
    		} else {
			    $strErrorMsg = __( 'The maximum number of roommate invitations have been accepted, you cannot accept this invitation.' );
    		}
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strErrorMsg ) );
    	}
    	return $boolIsValid;
    }

    public function valIsInvitationEmailSent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConfirmedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConfirmedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valDeletedOn() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

	public function valIsInvitationRejected() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valAcceptInvitation( $objDatabase, $boolIsFromWaitlist = false, $boolIsFromProspectPortal = false, $intPropertyId = NULL, $intUnitTypeId = NULL, $intSpaceConfigurationId = NULL, $intLeaseStartWindowId = NULL ) {
    	$boolIsValid = true;

    	if( true == is_numeric( $this->getRoommateGroupId() ) ) {

		    $objLeaseTermsFilter = new CLeaseTermsFilter();
		    $objLeaseTermsFilter->setCid( $this->getCid() );
		    $objLeaseTermsFilter->setPropertyIds( [ $intPropertyId ] );
		    $objLeaseTermsFilter->setLeaseStartWindowId( $intLeaseStartWindowId );
		    $objLeaseTermsFilter->setUnitTypeId( $intUnitTypeId );
		    $objLeaseTermsFilter->setSpaceConfigurationId( $intSpaceConfigurationId );
		    $objLeaseTermsFilter->setIsProspect( true );
		    $objLeaseTermsFilter->setIsFromProspectPortal( true );
		    $objLeaseTermsFilter->setIsShowOnWebsite( true );
		    $objLeaseTermsFilter->setIsConsiderPricingVisibility( true );

    		if( 0 < ( int ) CRoommates::fetchAssignedUnitSpaceRoommateCountByRoommateGroupIdByCid( $this->getRoommateGroupId(), $this->getCid(), $objDatabase ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The window for accepting an invitation has closed.' ) ) );
    		} elseif( false == $boolIsFromWaitlist && 0 == ( int ) \Psi\Eos\Entrata\CRoommates::createService()->fetchRoommatesCountByRoommateGroupIdByLeaseIdByCid( $this->getRoommateGroupId(), $this->getParentLeaseId(), $this->getCid(), $objDatabase ) ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Unable to accept the invitation, as the sender is no more part of the roommate group.' ) ) );
		    } elseif( true == $boolIsFromProspectPortal && true == valId( $intPropertyId ) && true == valId( $intUnitTypeId ) && true == valId( $intSpaceConfigurationId ) && true == valId( $intLeaseStartWindowId )
		              && false == valArr( \Psi\Eos\Entrata\CLeaseTerms::createService()->fetchActiveDateBasedLeaseTermsByLeaseTermsFilter( $objLeaseTermsFilter, $objDatabase ) ) ) {
    			$boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The lease term you were invited to join is no longer available.' ) ) );
		    }
    	}
    	return $boolIsValid;
    }

    public function valPhoneNumber( $strPrimaryPhoneNumber ) {
	    $objPhoneNumber = $this->createPhoneNumber( $strPrimaryPhoneNumber );

        if( true == valStr( $strPrimaryPhoneNumber ) && false == $objPhoneNumber->isValid( false ) ) {
		    $this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ) ) );
		    return false;
	    }

	    return true;
    }

	public function valNameLast() {
		$boolIsValid = true;

		if( true == \Psi\CStringService::singleton()->stristr( $this->getNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getNameLast(), 'Content-type' ) ) {
			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name cannot contain @ signs or email headers.' ) ) );
		} else {
			if( false == valStr( $this->getNameLast() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == \Psi\CStringService::singleton()->stristr( $this->getNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getNameFirst(), 'Content-type' ) ) {
			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name cannot contain @ signs or email headers.' ) ) );
		}

			return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL, $strCurrentRoommateEmailAddress = NULL, $intPropertyFloorplanId = NULL, $intPropertyId = NULL, $boolIsInvitationAccepted = false, $boolIsFromProspectPortal = false, $boolIsFromApplicantRoommateDelete = false, $boolIsNewRoommateGroup = false, $intUnitTypeId = NULL, $intSpaceConfigurationId = NULL, $intLeaseStartWindowId = NULL, $boolIsFromCancelLease = false ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valParentLeaseId();
            	$boolIsValid &= $this->valEmailAddress( $objDatabase, true, $strCurrentRoommateEmailAddress );
	            $boolIsValid &= $this->valNameLast();
	            $boolIsValid &= $this->valNameFirst();
            	$boolIsValid &= $this->valPhoneNumber( $this->getPhoneNumber() );
	            if( true == $boolIsNewRoommateGroup ) {
		            $boolIsValid &= $this->valDuplicateRoommate( $objDatabase, $boolIsFromProspectPortal );
	            }
            	if( false == is_null( $intPropertyFloorplanId ) || false == is_null( $intPropertyId ) ) {
            		$boolIsValid &= $this->valOccupantLimit( $intPropertyFloorplanId, $intPropertyId, $boolIsInvitationAccepted, $objDatabase );
            	}
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valRoommateGroupId();
            	$boolIsValid &= $this->valParentLeaseId();
            	if( false == $boolIsFromApplicantRoommateDelete && false == $boolIsFromCancelLease ) {
            		$boolIsValid &= $this->valEmailAddress( $objDatabase, false );
            	}
            	if( true == $boolIsInvitationAccepted ) {
            		$boolIsValid &= $this->valDuplicateRoommate( $objDatabase, $boolIsFromProspectPortal );
            		$boolIsValid &= $this->valAcceptInvitation( $objDatabase, false, $boolIsFromProspectPortal, $intPropertyId, $intUnitTypeId, $intSpaceConfigurationId, $intLeaseStartWindowId );
            	}
            	if( false == is_null( $intPropertyFloorplanId ) || false == is_null( $intPropertyId ) ) {
            		$boolIsValid &= $this->valOccupantLimit( $intPropertyFloorplanId, $intPropertyId, $boolIsInvitationAccepted, $objDatabase );
            	}
            	break;

            case VALIDATE_DELETE:
            	break;

            case 'validate_insert_roommate_from_waitlist':
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valRoommateGroupId();
            	$boolIsValid &= $this->valParentLeaseId();
	            $boolIsValid &= $this->valEmailAddress( $objDatabase, true, $strCurrentRoommateEmailAddress );
            	$boolIsValid &= $this->valDuplicateRoommate( $objDatabase, $boolIsFromProspectPortal );
            	$boolIsValid &= $this->valAcceptInvitation( $objDatabase, true );
            	if( false == is_null( $intPropertyFloorplanId ) || false == is_null( $intPropertyId ) ) {
            		$boolIsValid &= $this->valOccupantLimit( $intPropertyFloorplanId, $intPropertyId, $boolIsInvitationAccepted, $objDatabase );
            	}
            	break;

            case 'validate_update_roommate_from_waitlist':
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valId();
	            $boolIsValid &= $this->valEmailAddress( $objDatabase, true, $strCurrentRoommateEmailAddress );
	            $boolIsValid &= $this->valDuplicateRoommate( $objDatabase, $boolIsFromProspectPortal );
            	break;

            default:
            	// default break
            	break;
        }

        return $boolIsValid;
    }

    /**
    * Fetch Functions
    *
    */

    public function fetchOtherRoommates( $objDatabase ) {
    	return CRoommates::fetchOtherInvitationConfirmedRoommatesByCidByRoommateGroupId( $this->getCid(), $this->getRoommateGroupId(), $objDatabase );
    }

    public function fetchCustomRoommates( $objDatabase ) {
    	return CRoommates::fetchCustomRoommatesByCidByRoommateGroupId( $this->getCid(), $this->getRoommateGroupId(), $objDatabase );
    }

	public function fetchAllRoommates( $objDatabase ) {
		return \Psi\Eos\Entrata\CRoommates::createService()->fetchAllRoommatesOfSenderByCidByRoommateGroupIdByLeaseId( $this->getCid(), $this->getRoommateGroupId(), $this->getParentLeaseId(), $objDatabase );
	}

    /**
    * Other Functions
    *
    */

	public function logEventRoommate( $objReference, $objDatabase, $objCompanyUser = NULL, $objLeaseCustomer = NULL, $objApplicant = NULL, $intEventTypeId = NULL, $intEventSubTypeId = NULL, $strNote = NULL, $intSystemEmailId = NULL ) {

		$intEventTypeId = ( true == is_null( $intEventTypeId ) ) ? CEventType::STATUS_CHANGE : $intEventTypeId;
		$intEventSubTypeId = ( false == is_null( $intEventSubTypeId ) ) ? $intEventSubTypeId : CEventSubType::APPLICANT_GROUPING_EMAIL_SENT;

		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();

		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setLeaseCustomer( $objLeaseCustomer );

		if( true == valObj( $objReference, 'CLease' ) || true == valObj( $objReference, 'CApplication' ) ) {
			$objSelectedEvent = $objEventLibrary->createEvent( array( $objReference ), $intEventTypeId, $intEventSubTypeId );
			$objSelectedEvent->setNewStatusId( $objReference->getLeaseStatusTypeId() );
			$objSelectedEvent->setDataReferenceId( $intSystemEmailId );
			$objSelectedEvent->setPropertyId( $objReference->getPropertyId() );
			$objSelectedEvent->setCid( $this->getCid() );

			if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
				$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );

				$objSelectedEvent->setCompanyUser( $objCompanyUser );

				if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
					$objSelectedEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
					$objSelectedEvent->setCompanyEmployee( $objCompanyEmployee );
				}
			}

			if( true == valObj( $objApplicant, 'CApplicant' ) ) {
				$objSelectedEvent->setCustomerId( $objApplicant->getCustomerId() );
				$objSelectedEvent->setApplicant( $objApplicant );
			}

			if( true == valObj( $objReference, 'CLease' ) ) {
				$objSelectedEvent->setLeaseIntervalId( $objReference->getActiveLeaseIntervalId() );
			} elseif( true == valObj( $objReference, 'CApplication' ) ) {
				$objSelectedEvent->setLeaseIntervalId( $objReference->getLeaseIntervalId() );
			}

			$objSelectedEvent->setRoommateGroupId( $this->getRoommateGroupId() );
			$objSelectedEvent->setNotes( $strNote );
			$objSelectedEvent->setEventDatetime( 'NOW()' );
			$objSelectedEvent->setScheduledDatetime( NULL );
			$objEventLibrary->buildEventDescription( $objReference );
		}
		return $objEventLibrary;
	}

	private function createPhoneNumber( $strPhoneNumber ) {
		require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';
		$arrmixOptions = [];
		$arrmixOptions['extension'] = '';
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber, $arrmixOptions );
		return $objPhoneNumber;
	}

	public function getOtherRoommates( $boolWithStatues = false ) {

		$strRoommates = '';

		if( true == $boolWithStatues ) {
			$arrobjRoommates = $this->fetchAllRoommates( $this->m_objDatabase );
		} else {
			$arrobjRoommates = $this->fetchOtherRoommates( $this->m_objDatabase );
		}

		if( true == valArr( $arrobjRoommates ) ) {
			foreach( $arrobjRoommates as $objRoommate ) {
				$strRoommates .= '<tr>
									<td style="padding: 10px; margin: 0; margin-bottom: -4px; border-bottom: 1px solid #848484" >' . $objRoommate->getCustomerNameFull() . '</td>
									<td style="padding: 10px; margin: 0; margin-bottom: -4px; border-bottom: 1px solid #848484" >' . $objRoommate->getEmailAddress() . '</td>';

				$strRoommates .= __( '<td style="padding: 10px; margin: 0; margin-bottom: -4px; border-bottom: 1px solid #848484" >{%h,0}</td>', [ $objRoommate->getPhoneNumber() ?? '<center>-</center>' ] );

				if( true == $boolWithStatues ) {
					$strStatus = ( true == valId( $objRoommate->getConfirmedBy() ) && true == valStr( $objRoommate->getConfirmedOn() ) ) ? __( 'Accepted' ) : __( 'Pending' );
					$strRoommates .= '<td style="padding: 10px; margin: 0; margin-bottom: -4px; border-bottom: 1px solid #848484" >' . $strStatus . '</td></tr>';
				}
			}
		}

		return $strRoommates;
	}

	public function getOtherRoommatesWithStatus() {
		return $this->getOtherRoommates( true );
	}

	public function getRoommateInvitationSentApplicantName() {
		$arrobjRoommates = $this->fetchOtherRoommates( $this->m_objDatabase );
		$strRoommateInvitationSentApplicantName = '';
		if( true == valArr( $arrobjRoommates ) ) {
			$arrobjRoommates = rekeyObjects( 'leaseId', $arrobjRoommates );
			if( true == valObj( $arrobjRoommates[$this->getParentLeaseId()], 'CRoommate' ) ) {
				$strRoommateInvitationSentApplicantName = $arrobjRoommates[$this->getParentLeaseId()]->getCustomerNameFull();
			}
		}
		return $strRoommateInvitationSentApplicantName;
	}

	public function getRoommateInvitationAcceptedApplicantName() {
		$arrobjRoommates = $this->fetchOtherRoommates( $this->m_objDatabase );
		$strRoommateInvitationAcceptedApplicantName = '';
		if( true == valArr( $arrobjRoommates ) ) {
			$arrobjRoommates = rekeyObjects( 'leaseId', $arrobjRoommates );
			if( true == valObj( $arrobjRoommates[$this->getLeaseId()], 'CRoommate' ) ) {
				$strRoommateInvitationAcceptedApplicantName = $arrobjRoommates[$this->getLeaseId()]->getCustomerNameFull();
			}
		}

		return $strRoommateInvitationAcceptedApplicantName;
	}

	public function getRoommateInvitationReceiverName() {
		$objRoommate = \Psi\Eos\Entrata\CRoommates::createService()->fetchReceiverRoommateNameByRoommateGroupIdByEmailAddressByCid( $this->getRoommateGroupId(), $this->getEmailAddress(), $this->getCid(), $this->getDatabase(), $this->getApplicationId() );

		return valObj( $objRoommate, self::class ) ? $objRoommate->getNameFirst() : NULL;
	}

	public function getRoommateInvitationSenderName() {
		$strRoommateInvitationSenderNameFull = $this->getRoommateInvitationSentApplicantName();

		return trim( \Psi\CStringService::singleton()->substr( $strRoommateInvitationSenderNameFull, \Psi\CStringService::singleton()->strrpos( $strRoommateInvitationSenderNameFull, ' ' ) ) );
	}

}
?>