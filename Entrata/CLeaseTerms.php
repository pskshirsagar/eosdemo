<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseTerms
 * Do not add any new functions to this class.
 */

class CLeaseTerms extends CBaseLeaseTerms {

	public static function fetchPublishedLeaseTermsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIsDateBased = false, $strMoveInDate = NULL, $boolIsReturnKeyedArray = true, $intActiveQuoteId = NULL, $boolIsProspect = false ) {

		$strSqlActiveQuoteIdCondition = ( true == valId( $intActiveQuoteId ) ) ? ' AND q.id = ' . ( int ) $intActiveQuoteId . ' ': ' ';

		$strSqlLeaseStartWindowCondition = ( true == $boolIsDateBased ) ?  ' AND lsw.id IS NOT NULL ' : '';

		$strSqlIsProspectCondition = ( $boolIsProspect ) ?  ' AND lt.is_prospect = true ' : '';

		$strSql = 'SELECT DISTINCT ON (lt.cid,lt.id,lsw.id)
						lt.*,
						lsw.id as lease_start_window_id,
						lsw.start_date,
						lsw.end_date,
						CASE
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.start_date IS NOT NULL AND lsw.end_date IS NOT NULL
							THEN lt.name || \' (\' ||  to_char( lsw.start_date,\'MM/DD/YYYY\') || \' - \' || to_char( lsw.end_date,\'MM/DD/YYYY\') || \') \'
							ELSE lt.name
						END as name,
						lsw.property_id
					FROM
						quotes q
						JOIN quote_lease_terms qlt ON ( q.cid = qlt.cid AND q.id = qlt.quote_id AND q.cancelled_on IS NULL AND qlt.deleted_by IS NULL )
						JOIN cached_applications ca ON ( ca.cid = q.cid AND q.application_id = ca.id AND ca.application_stage_id = 3 AND ca.application_status_id = 1 )
						JOIN lease_terms lt ON ( q.cid = lt.cid AND qlt.lease_term_id = lt.id )
						JOIN property_charge_settings pcs ON ( ca.cid = pcs.cid AND ca.property_id = pcs.property_id )
						JOIN property_gl_settings pgs ON ( ca.cid = pgs.cid AND ca.property_id = pgs.property_id )
						LEFT JOIN lease_start_windows lsw ON ( lsw.cid = q.cid AND lsw.lease_term_id = lt.id AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND lsw.property_id = ca.property_id AND qlt.lease_start_window_id = lsw.id )
						LEFT OUTER JOIN lease_expiration_structures les ON ( pcs.cid = les.cid AND les.id = pgs.lease_expiration_structure_id )
						LEFT OUTER JOIN property_lease_term_expirations plte ON ( plte.cid = lt.cid
																					AND plte.property_id = pcs.property_id
																					AND DATE( date_trunc(\'month\',plte.month) ) =
																							CASE
																								WHEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' THEN
																									DATE(date_trunc(\'month\',\'' . $strMoveInDate . '\'::DATE) ) + (lt.term_month ||\'months\')::interval
																								ELSE
																									DATE(date_trunc(\'month\',lsw.end_date) )
																							END	)
					WHERE
						q.cid = ' . ( int ) $intCid . '
						AND q.application_id =' . ( int ) $intApplicationId . '
						' . $strSqlActiveQuoteIdCondition . '
						AND lt.is_renewal IS TRUE
						AND lt.is_disabled IS FALSE
						' . $strSqlIsProspectCondition . '
						' . $strSqlLeaseStartWindowCondition . '
						AND COALESCE(plte.exceeds_expiration_limit,FALSE) IS FALSE
						AND CASE
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' THEN lsw.end_date > ca.lease_start_date
							ELSE TRUE
							END';
		$strSql .= ( true == $boolIsDateBased ? ' AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED : ' AND lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL );

		$strSql .= ' ORDER BY lt.cid,lt.id,lsw.id, lsw.start_date, lsw.end_date';

		return self::fetchLeaseTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

//	public static function fetchLeaseTermsByIdsByPropertyIdByCid( $arrintLeaseTermIds, $intPropertyId, $intCid, $objDatabase ) {
//		if( false == valArr( $arrintLeaseTermIds ) ) return NULL;

//		$strSql = 'SELECT
//						*
//					FROM
//						lease_terms lt
//						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
//					WHERE
//						lt.id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ')
//						AND pcs.property_id = ' . ( int ) $intPropertyId . '
//						AND lt.deleted_on IS NULL
//						AND lt.cid = ' . ( int ) $intCid;

//		return self::fetchLeaseTerms( $strSql, $objDatabase );
//	}

	public static function fetchLeaseTermsByIdsByPropertyIdsByCid( $arrintLeaseTermIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false ) {
		if( false == valArr( $arrintLeaseTermIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.id IN( ' . implode( ',', $arrintLeaseTermIds ) . ')
						AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
						AND lt.deleted_on IS NULL
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql;

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermByPropertyIdByIdByCid( $intPropertyId, $intId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolIsFromScheduledTransfer = false ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strLeftJoinCondition = ( true == $boolIsFromScheduledTransfer ) ? 'LEFT' : '';

		$strSql = 'SELECT
						lt.*,
						lsw.start_date as start_date,
						lsw.end_date as end_date,
						lsw.lease_term_type_id,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						' . $strLeftJoinCondition . ' JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
					WHERE
						lt.id = ' . ( int ) $intId . '
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.deleted_on IS NULL
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql;

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseTermsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $arrintLeaseTermIds = [], $boolIsIncludeDateBased = true, $intOccupancyTypeId = NULL, $boolIsOrganizationLease = false, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == is_null( $intOccupancyTypeId ) ) ? ' AND lt.occupancy_type_id = ' . ( int ) $intOccupancyTypeId : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.deleted_on IS NULL
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.term_month <> 0
						AND lt.is_disabled = false
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql . ' ';

		if( true == $boolIsProspect && true == $boolIsRenewal ) {
			$strSql .= 'AND ( lt.is_prospect = true OR lt.is_renewal = true ) ';
		} elseif( true == $boolIsProspect ) {
			$strSql .= 'AND lt.is_prospect = true ';

			if( true == $boolShowOnWebsite ) {
				$strSql .= 'AND show_on_website = true ';
			}
		} elseif( true == $boolIsRenewal ) {
			$strSql .= 'AND is_renewal = true ';
		}

		if( true == valArr( $arrintLeaseTermIds ) ) {
			$strSql .= 'AND lt.id IN( ' . implode( ',', $arrintLeaseTermIds ) . ')';
		}

		if( false == $boolIsIncludeDateBased ) {
			$strSql .= 'AND lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL;
		}

		if( false == $boolIsOrganizationLease ) {
			$strSql .= 'AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )';
		} else {
			$strSql .= 'AND lt.default_lease_term_id = ' . CDefaultLeaseTerm::GROUP;
		}

		$strSql .= ' ORDER BY lt.term_month';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchCustomDateBasedPublishedLeaseTermsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsDateBased = false, $strTemplateTierStartDate = NULL, $boolIncludeCommercialLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
					WHERE
						lt.deleted_on IS NULL
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.cid = ' . ( int ) $intCid . '
						AND lt.is_renewal = true ' . $strWhereSql;

		if( true == $boolIsDateBased ) {
			$strSql .= 'AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.end_date > \'' . $strTemplateTierStartDate . '\'::timestamptz';
		} else {
			$strSql .= 'AND lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL;
		}

		$strSql .= ' ORDER BY term_month';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchPublishedLeaseTermsByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $boolIsDateBased = false, $strMoveInDate = NULL, $boolIncludeCommercialLeaseTerms = false, $boolIsEntrataOnly = false, $boolIncludeGroupLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false, $intOccupancyTypeId = NULL ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';
		$strWhereSql .= ( false == is_null( $intOccupancyTypeId ) ) ? ' AND lt.occupancy_type_id = ' . ( int ) $intOccupancyTypeId : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id,
						lsw.organization_contract_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						LEFT JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
					WHERE
						lt.deleted_on IS NULL
						AND lt.is_disabled = false
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.term_month <> 0
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql . ' ';

		if( true == $boolIsProspect && true == $boolIsRenewal ) {
			$strSql .= 'AND ( is_prospect = true OR lt.is_renewal = true ) ';
		} elseif( true == $boolIsProspect ) {
			$strSql .= 'AND is_prospect = true ';

			if( true == $boolShowOnWebsite ) {
				$strSql .= 'AND lt.show_on_website = true ';
			}
			if( true == $boolIsEntrataOnly ) {
                $strSql .= 'AND is_disabled = false ';
            }
		} elseif( true == $boolIsRenewal ) {
			$strSql .= 'AND lt.is_renewal = true ';
		}

		if( true == $boolIsDateBased && false == is_null( $strMoveInDate ) ) {
			$strSql .= 'AND ( ( lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.start_date <= \'' . $strMoveInDate . '\' AND lsw.end_date >= \'' . $strMoveInDate . '\' ) OR ( lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' ) ) AND lsw.deleted_on IS NULL ';
		}

		if( false == $boolIncludeGroupLeaseTerms ) {
			$strSql .= 'AND ( COALESCE( lt.default_lease_term_id, 0 ) <> ' . CDefaultLeaseTerm::GROUP . '
				        OR lsw.organization_contract_id IS NULL )';
		}

		$strSql .= 'ORDER BY lt.term_month';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchPublishedMaxLeaseTermByCidByPropertyId( $intCid, $intPropertyId, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.term_month <> 0
						AND lt.deleted_on IS NULL
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql . ' ';

		if( true == $boolIsProspect && true == $boolIsRenewal ) {

			$strSql .= 'AND ( lt.is_prospect = true OR lt.is_renewal = true ) ';

		} elseif( true == $boolIsProspect ) {

			$strSql .= 'AND lt.is_prospect = true ';
			$strSql .= ( true == $boolShowOnWebsite ) ? ' AND show_on_website = true ' : '';

		} elseif( true == $boolIsRenewal ) {
			$strSql .= 'AND lt.is_renewal = true ';
		}

		$strSql .= 'ORDER BY lt.term_month DESC LIMIT 1';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermByTermMonthByPropertyIdByCid( $intTermMonth, $intPropertyId, $intCid, $objDatabase, $boolIsProspect, $boolIsRenewal, $boolIsSkipFlexibleLeaseTerms = false ) {
		$strSql = 'SELECT
				      lt.*
				  FROM
				      lease_terms lt
				      JOIN property_charge_settings pcs ON ( lt.lease_term_structure_id = pcs.lease_term_structure_id AND lt.cid = pcs.cid )
				  WHERE
				      lt.cid = ' . ( int ) $intCid . '
				      AND pcs.property_id = ' . ( int ) $intPropertyId . '
				      AND lt.deleted_on IS NULL
				      AND lt.is_disabled = false
				      AND lt.is_unset IS FALSE
				      AND lt.term_month = ' . ( int ) $intTermMonth;

		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		if( true == $boolIsProspect && true == $boolIsRenewal ) {

			$strSql .= 'AND ( lt.is_prospect = true OR lt.is_renewal = true ) ';

		} elseif( true == $boolIsProspect ) {

			$strSql .= 'AND lt.is_prospect = true ';
			$strSql .= ' AND show_on_website = true ';

		} elseif( true == $boolIsRenewal ) {
			$strSql .= 'AND lt.is_renewal = true ';
		}

		$strSql .= ' LIMIT 1';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchPublishedMaxLeaseTermByOccupancyTypeIdByCidByPropertyId( $intCid, $intOccupancyTypeId = NULL, $intPropertyId, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';
		$strWhereSql .= ( false == is_null( $intOccupancyTypeId ) ) ? ' AND lt.occupancy_type_id = ' . ( int ) $intOccupancyTypeId : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.term_month <> 0
						AND lt.deleted_on IS NULL
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql . ' ';

		if( true == $boolIsProspect && true == $boolIsRenewal ) {

			$strSql .= 'AND ( lt.is_prospect = true OR lt.is_renewal = true ) ';

		} elseif( true == $boolIsProspect ) {

			$strSql .= 'AND lt.is_prospect = true ';
			$strSql .= ( true == $boolShowOnWebsite ) ? ' AND show_on_website = true ' : '';

		} elseif( true == $boolIsRenewal ) {
			$strSql .= 'AND lt.is_renewal = true ';
		}

		$strSql .= 'ORDER BY lt.term_month DESC LIMIT 1';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchPublishedLeaseTermsByPropertyIdByLeaseStartWindowIdsByCid( $intPropertyId, $arrintLeaseStartWindowIds, $intCid, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false ) {
		if( false == valArr( $arrintLeaseStartWindowIds ) ) return NULL;

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*,
						lsw.id as lease_start_window_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
					WHERE
						lsw.cid = lt.cid
						AND lsw.lease_term_id = lt.id
						AND lt.term_month <> 0
						AND lt.deleted_on IS NULL
						AND lsw.id IN (' . implode( ',', $arrintLeaseStartWindowIds ) . ')
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql;

		if( true == $boolIsProspect && true == $boolIsRenewal ) {
			$strSql .= ' AND ( is_prospect = true OR is_renewal = true ) ';
		} elseif( true == $boolIsProspect ) {
			$strSql .= ' AND is_prospect = true ';
			if( true == $boolShowOnWebsite ) {
				$strSql .= 'AND show_on_website = true ';
			}
		} elseif( true == $boolIsRenewal ) {
			$strSql .= ' AND is_renewal = true ';
		}

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolIsReturnKeyedArray = true, $boolIsSkipFlexibleLeaseTerms = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND pcs.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ')
						AND lt.deleted_on IS NULL ' . $strWhereSql . '
					ORDER BY lt.term_month';

		return self::fetchLeaseTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchCommercialLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false, $boolIsReturnObject = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereSql = ( true == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id = ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';
		$strWhereSql .= ' AND lt.is_disabled = false';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND pcs.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ')
						AND lt.deleted_on IS NULL ' . $strWhereSql . ' 
					ORDER BY lt.term_month';

		if( true == $boolIsReturnObject ) {
			return self::fetchLeaseTerms( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchLeaseTermByPropertyIdByTermMonthByCid( $intPropertyId, $intTermMonth, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.deleted_on IS NULL
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.term_month = ' . ( int ) $intTermMonth . '
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql . ' LIMIT 1 ';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermByPropertyIdByTermMonthByNameByCid( $intPropertyId, $intTermMonth, $strName, $intCid, $objDatabase ) {
		if( false == valStr( $strName ) ) return NULL;

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.deleted_on IS NULL
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.term_month = ' . ( int ) $intTermMonth . '
						AND lt.name = \'' . $strName . '\'
						AND lt.cid = ' . ( int ) $intCid . '
					LIMIT 1;';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermByLeaseTermStructureIdByTermMonthByCid( $intLeaseTermStructureId, $intTermMonth, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*
					FROM
						lease_terms lt
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.lease_term_structure_id = ' . ( int ) $intLeaseTermStructureId . '
						AND lt.term_month = ' . ( int ) $intTermMonth . '
						AND lt.deleted_on IS NULL' . $strWhereSql . '
					LIMIT 1;';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchActiveDateBasedLeaseTermsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolExcludeOnlyRenewalLeaseTerms = false, $arrintSelectedLeaseStartWindowIds = [] ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhereSql  = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( true == $boolExcludeOnlyRenewalLeaseTerms ) ? ' AND NOT ( lt.is_renewal = true AND lt.is_prospect = false ) ' : '';
		$strWhereSql .= ( true == valArr( $arrintSelectedLeaseStartWindowIds ) ) ? ' AND lsw.id IN ( ' . implode( ', ', $arrintSelectedLeaseStartWindowIds ) . ' ) ' : '';

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getdefaultLocaleCode() . '\');
					SELECT
						lsw.id,
						util_get_translated( \'name\', lt.name, lt.details ) as name,
						lsw.start_date,
						lsw.end_date,
						lt.id as lease_term_id
					FROM
						lease_terms lt
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id )
					WHERE
						lsw.property_id = ' . ( int ) $intPropertyId . '
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.cid = ' . ( int ) $intCid . '
						AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
						AND lt.is_disabled = false
						AND lsw.end_date >= CURRENT_DATE
						AND lt.deleted_on IS NULL
						AND lsw.deleted_on IS NULL
						AND lsw.is_active = true 
						AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL ) ' . $strWhereSql . '
					ORDER BY
						lsw.start_date,
						lsw.end_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssociatedLeaseTermsByLeaseStartWindowIdByPropertyIdByCid( $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		if( ( false == is_numeric( $intLeaseStartWindowId ) && false == is_numeric( $intPropertyId ) ) || false == is_numeric( $intCid ) )	return NULL;

		$objLeaseStartWindow = CLeaseStartWindows::fetchLeaseStartWindowByPropertyIdByIdByCid( $intPropertyId, $intLeaseStartWindowId, $intCid, $objDatabase );

		if( false == valObj( $objLeaseStartWindow, 'CLeaseStartWindow' ) || ( true == valObj( $objLeaseStartWindow, 'CLeaseStartWindow' ) && false == valId( $objLeaseStartWindow->getLeaseTermTypeId() ) ) ) return NULL;

		$strSql = 'SELECT
						lsw.id,
						lt.name,
						lt.id as lease_term_id,
						lsw.start_date,
						lsw.end_date
					FROM
						lease_terms lt
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id )
					WHERE
						lsw.end_date >= CURRENT_DATE
						AND ( lsw.start_date BETWEEN \'' . $objLeaseStartWindow->getStartDate() . '\'	AND \'' . $objLeaseStartWindow->getEndDate() . '\'
						OR lsw.start_date <= \'' . $objLeaseStartWindow->getStartDate() . '\'
						AND lsw.end_date >= \'' . $objLeaseStartWindow->getStartDate() . '\' )
						AND lt.cid = ' . ( int ) $intCid . '
						AND lsw.property_id = ' . ( int ) $intPropertyId . '
						AND pcs.property_id = ' . ( int ) $intPropertyId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveDateBasedLeaseTermsByLeaseTermsFilter( $objLeaseTermsFilter, $objDatabase ) {

		if( false == valObj( $objLeaseTermsFilter, 'CLeaseTermsFilter' ) ) {
			return NULL;
		}

		if( false == valArr( $objLeaseTermsFilter->getPropertyIds() ) || false == is_numeric( $objLeaseTermsFilter->getCid() ) ) return NULL;

		$strSelectSql = $strLeaseSortingSql = '';

		$strWhereSql = ( false == $objLeaseTermsFilter->getIsIncludeCommercialLeaseTerms() ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		if( true == $objLeaseTermsFilter->getIsAllowedOccupancyTypeLease() && true == valId( $objLeaseTermsFilter->getOccupancyTypeId() ) ) {
			$strWhereSql .= ' AND lt.occupancy_type_id = ' . $objLeaseTermsFilter->getOccupancyTypeId() . ' AND lsw.occupancy_type_id = ' . $objLeaseTermsFilter->getOccupancyTypeId();
		}

		if( true == $objLeaseTermsFilter->getIsRequiredPropertyName() ) {
			$strSelectSql = ' , p.property_name, p.property_id AS parent_property_id ';
		}

		if( true == $objLeaseTermsFilter->getIsFetchRenewalLeaseTermName() ) {
			$strSelectSql .= ', lsw.renewal_start_date ';
		}

		if( true == $objLeaseTermsFilter->getIsConsiderPricingVisibility() ) {
			$strCrWhereSql = '';

			if( true == valId( $objLeaseTermsFilter->getUnitTypeId() ) ) {
				$strCrWhereSql .= ' AND cr.unit_type_id = ' . ( int ) $objLeaseTermsFilter->getUnitTypeId() . ' ';
			}
			if( true == valId( $objLeaseTermsFilter->getSpaceConfigurationId() ) ) {
				$strCrWhereSql .= ' AND cr.space_configuration_id = ' . ( int ) $objLeaseTermsFilter->getSpaceConfigurationId() . ' ';
			}

			if( ( true == $objLeaseTermsFilter->getIsProspect() || true == $objLeaseTermsFilter->getIsRenewal() ) && true == $objLeaseTermsFilter->getIsShowOnWebsite() ) {
				$strCrWhereSql .= ( true == $objLeaseTermsFilter->getIsConsiderPricingVisibility() ) ? ' AND cr.show_on_website = true ' : '';
			}

			$strWhereSql .= ' AND
							EXISTS (
								SELECT 1
								FROM cached_rates cr
								WHERE lt.cid = cr.cid AND
									lsw.property_id = cr.property_id AND
									lsw.id = cr.lease_start_window_id AND
									lt.id = cr.lease_term_id AND
									( cr.effective_date = CURRENT_DATE OR
									cr.effective_date IS NULL )
									' . $strCrWhereSql . ' )';
		}

		if( false == $objLeaseTermsFilter->getIsOrganizationLease() ) {
			$strWhereSql .= ' AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )';
		} else {
			$strWhereSql .= ' AND lt.default_lease_term_id = ' . CDefaultLeaseTerm::GROUP;
		}

		if( true == $objLeaseTermsFilter->getIsSkipMoveInSchedulerAssociatedTerms() ) {
			$strWhereSql .= ' AND NOT EXISTS ( SELECT 1 FROM move_in_schedule_group_lease_terms misglt WHERE misglt.cid = lsw.cid AND misglt.lease_start_window_id =  lsw.id AND misglt.lease_term_id = lsw.lease_term_id AND misglt.deleted_by IS NULL )';
		}

		$strSql = 'DROP TABLE
					IF EXISTS pg_temp.load_properties_lsw;
					CREATE TEMP TABLE pg_temp.load_properties_lsw AS(
					  SELECT *
					  FROM load_properties( ARRAY[' . ( int ) $objLeaseTermsFilter->getCid() . '], ARRAY[' . implode( ',', $objLeaseTermsFilter->getPropertyIds() ) . '] ) lp
					);
					ANALYZE pg_temp.load_properties_lsw;';

		$strSql .= 'SELECT
						lt.* ' .
		           $strSelectSql . '
						,lsw.id as lease_start_window_id
						, lsw.start_date
						, lsw.end_date
						, lsw.billing_end_date
						, lsw.property_id
					FROM
						lease_terms lt
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
						JOIN properties p ON ( p.cid = lsw.cid AND p.id = lsw.property_id )
						JOIN pg_temp.load_properties_lsw AS lp ON ' . ( ( false == $objLeaseTermsFilter->getIsShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = p.id
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND lsw.property_id = pcs.property_id  )
					WHERE
						lt.cid = ' . ( int ) $objLeaseTermsFilter->getCid() . '
						AND lt.is_disabled = false
						AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
						AND lt.deleted_on IS NULL
						AND lsw.deleted_on IS NULL
						AND lsw.is_active = true' . $strWhereSql;

		if( true == valId( $objLeaseTermsFilter->getLeaseStartWindowId() ) ) {
			$strSql .= ' AND lsw.id = ' . ( int ) $objLeaseTermsFilter->getLeaseStartWindowId();
		}

		$strSql .= ( true == $objLeaseTermsFilter->getIsProspect() ) ? ' AND lt.is_prospect = true ' : '';

		$strSql .= ( true == $objLeaseTermsFilter->getIsRenewal() ) ? ' AND lt.is_renewal = true ' : '';

		if( ( true == $objLeaseTermsFilter->getIsProspect() || true == $objLeaseTermsFilter->getIsRenewal() ) && true == $objLeaseTermsFilter->getIsShowOnWebsite() ) {
			$strSql .= ' AND lt.show_on_website = true ';
		}

		if( true == valStr( $objLeaseTermsFilter->getStartDate() ) ) {
			$strSql .= ' AND lsw.end_date >= \'' . $objLeaseTermsFilter->getStartDate() . '\'';
		}

		if( true == valStr( $objLeaseTermsFilter->getSourceModule() ) ) {
			$intLeaseStatusTypeId = ( CLeaseTerm::MOVE_IN_BOARD_SOURCE_MODULE == $objLeaseTermsFilter->getSourceModule() ) ? CLeaseStatusType::FUTURE : CLeaseStatusType::NOTICE;
			$strMoveInMoveOutSql = \Psi\Eos\Entrata\CCachedLeases::createService()->checkIfResidentPresentForMoveInMoveOut( $intLeaseStatusTypeId, true );
			$strSql    .= ' AND EXISTS (
									' . $strMoveInMoveOutSql . '
	                                    AND l.property_id    = lsw.property_id
	                                    AND li.lease_start_window_id = lsw.id
	                                    AND li.cid = lt.cid
	                                    AND li.lease_term_id = lt.id 
                            )';
		} else {
			$strSql .= '  AND lsw.end_date >= CURRENT_DATE ';
		}

		$strLeaseSortingSql = ( true == $objLeaseTermsFilter->getIsFromProspectPortal() && CPropertyPreference::LEASE_TERM_SORTING_ORDER_DESC == $objLeaseTermsFilter->getLeaseTermSortingOrder() ) ? ' ( lsw.start_date, lsw.end_date ) DESC ' : ' ( lsw.start_date, lsw.end_date ) ASC ';

		$strOrderBySql	= ( true == $objLeaseTermsFilter->getIsRequiredPropertyName() ) ? ' ORDER BY ' . $strLeaseSortingSql : ' ORDER BY ' . ( CPropertyPreference::LEASE_TERM_SORTING_ORDER_DESC == $objLeaseTermsFilter->getLeaseTermSortingOrder() ? ' ( lsw.start_date, lsw.end_date ) DESC ' : ' lsw.start_date, lsw.end_date ' );

		$strSql .= $strOrderBySql;

		return self::fetchLeaseTerms( $strSql, $objDatabase, $objLeaseTermsFilter->getIsReturnKeyedArray() );
	}

	public static function fetchActiveDateBasedLeaseTermsForRenewalTransferByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsProspect = false, $boolIsRequiredPropertyName = false, $boolShowOnWebsite = false, $strStartDate = '', $boolIsRenewalOnly = false, $boolIsReturnKeyedArray = true, $boolIncludeCommercialLeaseTerms = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSelectSql = $strJoinSql = '';

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		if( true == $boolIsRequiredPropertyName ) {
			$strSelectSql = ' , p.property_name ';
			$strJoinSql	  = 'JOIN properties p ON ( p.cid = lsw.cid AND p.id = lsw.property_id ) ';
		}

		$strSql = 'SELECT DISTINCT ON (lt.cid,lt.id,lsw.id)
						lt.* ' .
							$strSelectSql . '
						,lsw.id as lease_start_window_id
						, lsw.start_date
						, lsw.end_date
						, lsw.property_id
					FROM
						lease_terms lt
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id )  ' .
							$strJoinSql . '
					WHERE
						lsw.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lt.cid = ' . ( int ) $intCid . '
						AND lt.is_disabled = false
						AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.end_date >= CURRENT_DATE
						AND lt.deleted_on IS NULL
						AND lsw.deleted_on IS NULL
						AND lsw.is_active = true' . $strWhereSql;

		if( true == $boolIsProspect ) {
			$strSql .= ' AND lt.is_prospect = true ';
			$strSql .= ( true == $boolShowOnWebsite ) ? ' AND show_on_website = true ' : '';
		}

		$strSql .= ( true == $boolIsRenewalOnly ) ? ' AND lt.is_renewal = true ' : '';

		if( true == valStr( $strStartDate ) ) {
			$strSql .= ' AND lsw.end_date >= \'' . $strStartDate . '\'';
		}

		$strSql .= ' ORDER BY lt.cid,lt.id,lsw.id, lsw.start_date, lsw.end_date';

		return self::fetchLeaseTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchActiveOverlappingDateBasedLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $strLeaseStartDate = '', $strLeaseEndDate = '', $boolIsReturnKeyedArray = true, $boolIncludeCommercialLeaseTerms = false, $boolIsFetchRenewalLeaseTermName = false, $intOccupancyTypeId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) || false == valStr( $strLeaseStartDate ) ) return NULL;

		$strSelectSql = ( true == $boolIsFetchRenewalLeaseTermName ) ? ', lsw.renewal_start_date' : '';

		$strWhereSql = ' AND ( \'' . $strLeaseStartDate . '\' BETWEEN lsw.start_date AND lsw.end_date )';
		if( true == valStr( $strLeaseEndDate ) ) {
			$strWhereSql = ' AND ( lsw.start_date BETWEEN \'' . $strLeaseStartDate . '\' AND \'' . $strLeaseEndDate . '\' OR ( \'' . $strLeaseStartDate . '\' BETWEEN lsw.start_date AND lsw.end_date ) )';
		}

		if( true == valId( $intOccupancyTypeId ) ) {
			$strWhereSql .= ' AND lt.occupancy_type_id = ' . $intOccupancyTypeId . ' AND lsw.occupancy_type_id = ' . $intOccupancyTypeId;
		}

		$strWhereSql .= ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = 'SELECT
						lt.*
						, lsw.id as lease_start_window_id
						, lsw.start_date
						, lsw.end_date' .
		                    $strSelectSql . '
						, lsw.property_id
					FROM
						lease_terms lt
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id )
					WHERE
						lsw.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lt.cid = ' . ( int ) $intCid . '
						AND lt.is_disabled = false
						AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.end_date >= CURRENT_DATE
						AND lt.deleted_on IS NULL
						AND lsw.deleted_on IS NULL
						AND lsw.is_active = true 
						AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL ) ' . $strWhereSql . '
					ORDER BY lsw.start_date, lsw.end_date';

		return self::fetchLeaseTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchStandardLeaseTermMonthByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.term_month
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.is_disabled = false
						AND lt.is_prospect = true
						AND lt.deleted_on IS NULL ' . $strWhereSql . '
					ORDER BY
						ABS( lt.term_month - 12 ),
						lt.term_month
					LIMIT 1';

		$arrintLeaseTerms = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintLeaseTerms ) ) ? $arrintLeaseTerms[0]['term_month'] : NULL;
	}

	public static function fetchCustomStandardLeaseTermMonthsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						property_id,
						term_month
					FROM
					(
						SELECT
							pcs.property_id,
							lt.term_month,
							RANK() OVER( PARTITION BY pcs.property_id ORDER BY ABS( lt.term_month - 12 ) ) AS rank
						FROM
							lease_terms lt
							JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						WHERE
							lt.cid = ' . ( int ) $intCid . '
							AND pcs.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							AND lt.is_disabled = false
							AND lt.is_prospect = true
							AND lt.deleted_on IS NULL ' . $strWhereSql . '
					) AS T
					WHERE
						T.rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermsNameByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolIsFetchLeaseStartWindowId = false, $boolIsSkipDateBasedLeaseTerms = false, $intId = NULL, $boolIsFromOrganizationContract = false, $boolFetchActiveWindow = false, $strOrderByField = NULL, $intLimit = NULL, $boolFetchObjects = false ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSelectSql = ( true == $boolIsFetchLeaseStartWindowId ) ? ', lsw.id AS lease_start_window_id ' : '';

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strWhereSql .= ( false == $boolIsSkipDateBasedLeaseTerms ) ? ' AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.deleted_on IS NULL AND lsw.end_date >= CURRENT_DATE  ' . ( true == $boolFetchActiveWindow ? ' AND lsw.is_active = true ' : '' ) : ' AND lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL;

		$strWhereSql .= ( true == valId( $intId ) ) ? ' AND lt.id = ' . ( int ) $intId : '';

		if( false == $boolIsFromOrganizationContract && false == $boolIsSkipDateBasedLeaseTerms ) {
			$strWhereSql .= ' AND lsw.organization_contract_id IS NULL';
		}

		$strSql = 'SELECT
						DISTINCT lt.id,
						lt.term_month,
						lt.name' . $strSelectSql . '
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						' . ( ( false == $boolIsSkipDateBasedLeaseTerms ) ? ' JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.property_id = pcs.property_id AND lsw.lease_term_id = lt.id ) ' : '' ) . '
					WHERE
						lt.deleted_on IS NULL
						AND lt.cid = ' . ( int ) $intCid . '
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.is_disabled = false' . $strWhereSql;

		$strSql .= ( true == valStr( $strOrderByField ) ) ? ' ORDER BY ' . $strOrderByField : '';
		$strSql .= ( true == valId( $intLimit ) ) ? ' LIMIT ' . $intLimit : '';

		if( true == $boolFetchObjects ) {
			return parent::fetchLeaseTerms( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchLeaseTermBySelectParametersByIdByPropertyIdByCid( $strSelectClause, $intId, $intPropertyId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $intOccupancyTypeId = NULL ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		if( true == valId( $intOccupancyTypeId ) ) {
			$strWhereSql .= ' AND lt.occupancy_type_id = ' . $intOccupancyTypeId . ' AND lsw.occupancy_type_id = ' . $intOccupancyTypeId;
		}

		$strSql = 'SELECT ' .
				$strSelectClause . '
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lsw.id = ' . ( int ) $intId . $strWhereSql . '
						AND lt.is_unset = false';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnsetLeaseTermIdByCid( $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.id
					FROM
						lease_terms lt
					WHERE
						lt.deleted_on IS NULL
						AND lt.cid = ' . ( int ) $intCid . '
						AND lt.is_unset IS TRUE ' . $strWhereSql . '
					ORDER BY lt.id ASC
						LIMIT 1';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintResponse ) ) {
			return $arrintResponse[0]['id'];
		}

		return NULL;
	}

	public static function fetchUnsetLeaseTermByCid( $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*
					FROM
						lease_terms lt
					WHERE
						lt.deleted_on IS NULL
						AND lt.cid = ' . ( int ) $intCid . '
						AND lt.is_unset IS TRUE ' . $strWhereSql . '
					ORDER BY lt.id ASC
						LIMIT 1';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchDefaultLeaseTermIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.deleted_on IS NULL
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.cid = ' . ( int ) $intCid . '
						AND lt.is_default IS TRUE ' . $strWhereSql . '
					ORDER BY lt.id DESC
						LIMIT 1';
		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchPropertyDateBasedLeaseTermsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $boolIncludeCommercialLeaseTerms = false, $boolIsEntrataOnly = false, $boolIncludeGroupLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false, $intOccupancyTypeId = NULL ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';
		$strWhereSql .= ( false == is_null( $intOccupancyTypeId ) ) ? ' AND lt.occupancy_type_id = ' . ( int ) $intOccupancyTypeId : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id,
						lt.name
					FROM
						lease_terms lt
						JOIN lease_term_structures lts ON ( lts.cid = lt.cid AND lts.id = lt.lease_term_structure_id )
						JOIN property_charge_settings pcs ON ( pcs.cid = lts.cid AND pcs.lease_term_structure_id = lts.id )
					WHERE
						pcs.property_id = ' . ( int ) $intPropertyId . '
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.term_month <> 0
						AND lt.deleted_on IS NULL
						AND lt.cid = ' . ( int ) $intCid . $strWhereSql . ' ';

		if( true == $boolIsProspect ) {
			$strSql .= 'AND ( lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' OR lt.is_prospect = true )';

			if( true == $boolShowOnWebsite ) {
				$strSql .= 'AND show_on_website = true ';
			}
            if( true == $boolIsEntrataOnly ) {
                $strSql .= 'AND is_disabled = false ';
            }
		} elseif( true == $boolIsRenewal ) {
			$strSql .= 'AND ( lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' OR lt.is_renewal = true )';
		}

		if( false == $boolIncludeGroupLeaseTerms ) {
			$strSql .= 'AND COALESCE( lt.default_lease_term_id, 0 ) <> ' . CDefaultLeaseTerm::GROUP;
		}

		$strSql .= 'ORDER BY lt.term_month';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchPublishedLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $boolIsDateBased = false, $strMovinDate = NULL, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipFlexibleLeaseTerms = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						LEFT JOIN lease_start_windows lsw ON ( lsw.cid = pcs.cid AND lsw.lease_start_structure_id = pcs.lease_start_structure_id )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.is_disabled = false
						AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lt.deleted_on IS NULL
						AND lt.term_month <> 0 ';

		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		if( true == $boolIsProspect && true == $boolIsRenewal ) {
			$strSql .= 'AND ( lt.is_prospect = true OR lt.is_renewal = true ) ';
		} elseif( true == $boolIsProspect ) {
			$strSql .= 'AND lt.is_prospect = true ';

			if( true == $boolShowOnWebsite ) {
				$strSql .= 'AND lt.show_on_website = true ';
			}
		} elseif( true == $boolIsRenewal ) {
			$strSql .= 'AND lt.is_renewal = true ';

			if( true == $boolShowOnWebsite ) {
				$strSql .= 'AND lt.show_on_website = true ';
			}
		}

		if( true == $boolIsDateBased ) {
			$strSql .= ' AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
						AND lsw.is_active = true
						AND ( lsw.start_date <= \'' . $strMovinDate . '\' OR lsw.start_date IS NULL )
						AND ( lsw.end_date >=	\'' . $strMovinDate . '\' OR lsw.end_date IS NULL )';
		}

		if( false == $boolIncludeCommercialLeaseTerms ) {
			$strSql .= ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' ';
		}

		$strSql .= ' AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL ) ';

		$strSql .= 'ORDER BY lt.term_month ';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchActiveDateBasedLeaseTermsBySelectParametersByPropertyIdByCid( $strSelectClause, $intPropertyId, $intCid, $objDatabase, $boolIsProspect = false, $boolIncludeCommercialLeaseTerms = false, $strSourceModule = '' ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		if( true == valStr( $strSourceModule ) ) {
			$intLeaseStatusTypeId = ( CLeaseTerm::MOVE_IN_BOARD_SOURCE_MODULE == $strSourceModule ) ? CLeaseStatusType::FUTURE : CLeaseStatusType::NOTICE;
			$strMoveInMoveOutSql  = \Psi\Eos\Entrata\CCachedLeases::createService()->checkIfResidentPresentForMoveInMoveOut( $intLeaseStatusTypeId, true );
			$strWhereSql          .= ' AND EXISTS (
									' . $strMoveInMoveOutSql . '
	                                    AND l.property_id    = lsw.property_id
	                                    AND li.lease_start_window_id = lsw.id
	                                    AND li.cid = lt.cid
	                                    AND li.lease_term_id = lt.id
	                                    	                               
                            )';
		} else {
			$strWhereSql .= '  AND lsw.end_date >= CURRENT_DATE ';
		}

		$strSql = 'SELECT ' .
		          $strSelectClause . '
						FROM
							lease_terms lt
							JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
							JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
						WHERE
							pcs.property_id = ' . ( int ) $intPropertyId . '
							AND lsw.property_id = ' . ( int ) $intPropertyId . '
							AND lt.cid = ' . ( int ) $intCid . '
							AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
							AND lt.is_disabled = false
						 	AND lt.deleted_on IS NULL
							AND lsw.deleted_by IS NULL
							AND lsw.is_active = TRUE' . $strWhereSql;

		if( true == $boolIsProspect ) {
			$strSql .= ' AND lt.is_prospect = true';
		}

		$strSql .= ' ORDER BY ' . $objDatabase->getCollateSort( 'lt.name', true ) . ' , lsw.start_date, lsw.end_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermByLeaseIdByLeaseIntervalTypeIdByApplicationStageStatusIdsByCid( $intLeaseId, $intLeaseIntervalTypeId, $arrintApplicationStageStatusIds, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		if( false == valArr( $arrintApplicationStageStatusIds ) ) return NULL;

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql	= 'SELECT
						lt.name,
						lt.term_month,
						lt.lease_term_type_id
					FROM
						lease_terms lt
						JOIN cached_applications ca ON ( lt.cid = ca.cid AND lt.id = ca.lease_term_id )
					WHERE
						ca.lease_id = ' . ( int ) $intLeaseId . '
						AND ca.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
						AND (ca.application_stage_id,ca.application_status_id) IN (' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ')
						AND lt.deleted_on IS NULL
						AND ca.lease_start_date > now()
						AND ca.cid = ' . ( int ) $intCid . $strWhereSql . '
						ORDER BY ca.lease_start_date ASC
						LIMIT 1';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false ) {
		if( false == valId( $intLeaseStartWindowId ) ) return;

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = 'SELECT
						lt.name,
						lt.id,
						lt.term_month,
						lt.lease_term_type_id,
						lt.is_renewal,
						lt.is_prospect,
						lsw.id AS lease_start_window_id,
						lsw.start_date,
						lsw.end_date
					FROM
						lease_terms lt
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
					WHERE
						lsw.id = ' . ( int ) $intLeaseStartWindowId . '
						AND lsw.cid = ' . ( int ) $intCid . $strWhereSql;

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchExistingLeaseTermsCountByTermMonthByLeaseTermStructureIdByOccupancyTypeIdByIdByCid( $intTermMonth, $intLeaseTermStructureId, $intId, $intCid, $objDatabase, $intOccupancyTypeId = COccupancyType::CONVENTIONAL ) {

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $intCid . '
							AND lease_term_structure_id = ' . ( int ) $intLeaseTermStructureId . '
							AND lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
							AND occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
							AND term_month = ' . ( int ) $intTermMonth . '
							AND deleted_on IS NULL
							AND id <> ' . ( int ) $intId;

		return parent::fetchLeaseTermCount( $strWhereSql, $objDatabase );
	}

	public static function fetchExistingCommercialLeaseTermsCountByTermMonthByLeaseTermStructureIdByIdByOccupancyTypeIdByCid( $intTermMonth, $intLeaseTermStructureId, $intId, $intOccupancyTypeId, $intCid, $objDatabase ) {

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $intCid . '
							AND lease_term_structure_id = ' . ( int ) $intLeaseTermStructureId . '
							AND lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
							AND occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
							AND term_month = ' . ( int ) $intTermMonth . '
							AND deleted_on IS NULL
							AND id <> ' . ( int ) $intId;

		return parent::fetchLeaseTermCount( $strWhereSql, $objDatabase );
	}

	public static function fetchDateBasedLeaseTermByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false, $boolIncludeDeletedLeaseTerms = false ) {
		if( false == valId( $intLeaseStartWindowId ) ) return NULL;

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strWhereSql .= ( false == $boolIncludeDeletedLeaseTerms ) ? ' AND lt.deleted_on IS NULL AND lsw.deleted_on IS NULL ' : '';

		$strSql = 'SELECT
						lt.*,
						lsw.id as lease_start_window_id,
						lsw.start_date,
						lsw.end_date,
						lsw.billing_end_date
					FROM
						lease_terms lt
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lt.id = lsw.lease_term_id )
					WHERE
						lsw.id = ' . ( int ) $intLeaseStartWindowId . '
						AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
						AND lsw.cid = ' . ( int ) $intCid . $strWhereSql;

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchExistingLeaseTermsCountByIdByCid( $intId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false ) {

		$strWhereSql = 'WHERE
							cid = ' . ( int ) $intCid . '
							AND lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
							AND id = ' . ( int ) $intId;

		$strWhereSql .= ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' ' : '';

		return parent::fetchLeaseTermCount( $strWhereSql, $objDatabase );
	}

	public static function fetchLeaseTermsDataByLeaseStartWindowIdByPropertyIdsByCid( $intLeaseStartWindowId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						lsw.lease_term_id,
						lsw.property_id,
						lsw.start_date
				FROM
						lease_start_windows lsw
					WHERE
						lsw.id = ' . ( int ) $intLeaseStartWindowId . '
						AND lsw.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lsw.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false, $boolIncludeCommercialLeaseTerms = false, $boolIsSkipDateBasedLeaseTerms = false ) {

		$strSql = 'SELECT
							lt.*
						FROM
							lease_terms lt
							JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						WHERE
							pcs.property_id = ' . ( int ) $intPropertyId . '
							AND lt.deleted_on IS NULL
							AND lt.cid = ' . ( int ) $intCid;

		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';
		$strSql .= ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';
		$strSql .= ( false == $boolIsSkipDateBasedLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::DATE_BASED : '';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseTermsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strSql = 'SELECT
						lt.*
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.deleted_on IS NULL
						AND lt.is_disabled IS FALSE
						AND lt.cid = ' . ( int ) $intCid;

		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchAssociatedLeaseTermsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIncludeCommercialLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = 'SELECT
					lts.id as lease_term_structure_id,
					pcs.lease_term_structure_id,
					util_get_translated( \'name\', lts.name, lts.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS lease_term_structure_name,
					lt.id as lease_term_id,
					lt.lease_term_type_id as lease_term_type_id,
					lt.name as default_locale_lease_term_name,
					util_get_translated( \'name\', lt.name, lt.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS lease_term_name,
					lsw.start_date AS window_start_date,
					lsw.end_date AS window_end_date,
					lsw.id AS lease_start_window_id,
					CASE WHEN ( true = lsw.is_active ) THEN 1 ELSE 0 END is_active,
					CASE WHEN ( true = lt.is_renewal ) THEN 1 ELSE 0 END is_renewal,
					CASE WHEN ( true = lt.is_prospect ) THEN 1 ELSE 0 END is_prospect,
					CASE WHEN ( true = lt.show_on_website ) THEN 1 ELSE 0 END show_on_website,
					CASE WHEN ( lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' ) THEN 1 ELSE 0 END is_date_based,
					COUNT(lsw.id) over(PARTITION BY lt.id, pcs.property_id ) as lease_start_windows,
					CASE WHEN ( true = lt.is_disabled ) THEN 1 ELSE 0 END is_disabled,
					util_get_translated( \'name\', ot.name, ot.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS occupancy_type_name,
					lt.occupancy_type_id
				FROM
					lease_terms lt
					JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id =lt.lease_term_structure_id )
					JOIN lease_term_structures lts ON ( lts.id = pcs.lease_term_structure_id AND lts.cid = pcs.cid )
					LEFT JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id AND lsw.property_id = ' . ( int ) $intPropertyId . ' )
					LEFT JOIN occupancy_types ot ON ( lt.occupancy_type_id = ot.id )
				WHERE
					lt.deleted_on IS NULL
					AND lt.deleted_by IS NULL
					AND
					  CASE
					  	WHEN lsw.id IS NULL
					  		THEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
					  	ELSE lt.lease_term_type_id IN ( ' . CLeaseTermType::FLEXIBLE . ', ' . CLeaseTermType::DATE_BASED . ' )
					  END
					AND pcs.property_id = ' . ( int ) $intPropertyId . '
					AND lt.cid = ' . ( int ) $intCid . '
					AND ( lt.default_lease_term_id <> ' . CDefaultLeaseTerm::GROUP . ' OR lt.default_lease_term_id IS NULL )
					AND lsw.deleted_on IS NULL
					AND lsw.deleted_by IS NULL' . $strWhereSql .
				'ORDER BY
					(substring( lt.name, \'^[0-9]+\'))::NUMERIC ASC,
					' . $objDatabase->getCollateSort( 'lt.name', true ) . ',
					lsw.start_date,
					lsw.end_date,
					lt.term_month';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRenewalAcceptLeaseTermWithLeaseExpirationLimitByIdByPropertyIdByCid( $intId, $intPropertyId, $intCid, $objDatabase, $strMoveInDate, $boolAllowLeaseExpirationOverride, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strLeaseExpirationLimitsJoinSql = NULL;
		$strLeaseExpirationLimitsWhereClauseSql = NULL;

		if( true == valId( $intPropertyId ) && true == valStr( $strMoveInDate ) && false == $boolAllowLeaseExpirationOverride ) {
			$strLeaseExpirationLimitsJoinSql .= ' JOIN property_gl_settings pgs ON ( pgs.cid = lt.cid AND pgs.property_id = ' . ( int ) $intPropertyId . ' )
					LEFT OUTER JOIN lease_expiration_structures les ON ( pgs.cid = les.cid AND les.id = pgs.lease_expiration_structure_id )
					LEFT OUTER JOIN property_lease_term_expirations plte ON ( plte.cid = lt.cid AND plte.property_id = pgs.property_id AND DATE( date_trunc(\'MONTH\',plte.month) ) = DATE(date_trunc(\'MONTH\',\'' . $strMoveInDate . '\'::DATE) ) + (lt.term_month ||\'MONTH -1 DAY\')::interval ) ';
			$strLeaseExpirationLimitsWhereClauseSql = ' AND ( ( plte.id IS NULL OR les.id IS NULL ) OR ( plte.exceeds_expiration_limit IS FALSE OR plte.exceeds_expiration_limit IS NULL ) ) ';
		}

		$strSql = 'SELECT
					lt.*
					FROM
					lease_terms lt
					' . $strLeaseExpirationLimitsJoinSql . '
					WHERE
						lt.id = ' . ( int ) $intId . '
						AND lt.cid = ' . ( int ) $intCid;
		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';
		$strSql .= $strLeaseExpirationLimitsWhereClauseSql;
		$strSql .= ' LIMIT 1';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermMonthByIdByCid( $intLeaseTermId, $intCid, $objDatabase, $boolNonDeletedOnly = false, $boolIsProspect= false, $boolNonZeroTermMonth = false ) {

		$strCondition = '';
		if( true == $boolNonDeletedOnly ) $strCondition 	.= ' AND deleted_on IS NULL';
		if( true == $boolIsProspect ) $strCondition 		.= ' AND is_prospect = true';
		if( true == $boolNonZeroTermMonth ) $strCondition 	.= ' AND term_month <> 0';

		$strSql = 'SELECT
						term_month
					FROM
						lease_terms
					WHERE
						id = ' . ( int ) $intLeaseTermId . '
						AND cid = ' . ( int ) $intCid . $strCondition;

		return self::fetchColumn( $strSql, 'term_month', $objDatabase );

	}

	public static function fetchPublishedRenewalLeaseTermsByPropertyIdsByCId( $arrintPropertyId, $intCid, $objDatabase, $boolExcludeDisabled = false, $boolIsSkipFlexibleLeaseTerms = false ) {

		if( false == valArr( $arrintPropertyId ) ) return NULL;

		$strCondition = ( true == $boolExcludeDisabled ) ? ' AND lt.is_disabled = false ' : '';
		$strCondition .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )

					WHERE
						lt.deleted_on IS NULL
						AND lt.term_month <> 0
						AND lt.is_renewal IS TRUE
						AND pcs.property_id IN (' . implode( ',', $arrintPropertyId ) . ')
						AND lt.cid = ' . ( int ) $intCid . '
						' . $strCondition . '
					ORDER BY lt.term_month';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false, $boolWebVisibleOnly = true, $boolIncludeCommercialLeaseTerms = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND COALESCE( lt.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '';
		$strWhereSql .= ( true == $boolWebVisibleOnly ) ? ' AND lt.show_on_website IS TRUE ' : '';
		$strWhereSql .= ( false == $boolIncludeCommercialLeaseTerms ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL : '';

		$strSql = '	SELECT
						lt.id AS lease_term_id,
						lt.name AS lease_term_name,
						lt.term_month,
						lt.show_on_website,
						lt.lease_term_type_id,
						lt.occupancy_type_id,
						lts.id AS lease_term_structure_id,
						lts.name AS lease_term_structure_name,
						lsw.id AS lease_start_window_id,
						lsw.start_date AS window_start_date,
						lsw.end_date AS window_end_date,
						lsw.offset_start_days,
						lsw.offset_end_days,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id =lt.lease_term_structure_id )
						JOIN lease_term_structures lts ON ( lts.id = pcs.lease_term_structure_id AND lts.cid = pcs.cid )
						LEFT JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id AND lsw.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ') )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.deleted_by IS NULL
						AND lt.deleted_on IS NULL
						AND
							CASE
								WHEN lsw.id IS NULL THEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
								ELSE lt.lease_term_type_id IN ( ' . CLeaseTermType::FLEXIBLE . ', ' . CLeaseTermType::DATE_BASED . ' )
							END
						AND ( COALESCE( lt.default_lease_term_id, 0 ) <> ' . CDefaultLeaseTerm::GROUP . ' )
						AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
						AND lsw.deleted_on IS NULL
						AND lsw.deleted_by IS NULL ' . $strWhereSql . '
					ORDER BY
						(substring( lt.name, \'^[0-9]+\'))::NUMERIC ASC,
						lt.name,
						lsw.start_date,
						lsw.end_date,
						lt.term_month';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsCommercial = false, $boolOrderByDesc = false ) {

		$strOrderByDesc = ( true == $boolOrderByDesc ) ? ' DESC ' : '';

		$strWhereIsCommercial = ( false == $boolIsCommercial ) ? ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . '' : '';

		$strSql = 'SELECT
						lt.*,
						lsw.id as lease_start_window_id,
						pcs.property_id,
						lsw.start_date,
						lsw.end_date,
						lsw.offset_start_days,
						lsw.offset_end_days
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
						JOIN lease_term_structures lts ON ( lts.id = pcs.lease_term_structure_id AND lts.cid = pcs.cid )
						LEFT JOIN lease_start_windows lsw ON( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id AND lsw.deleted_on IS NULL AND lsw.property_id = pcs.property_id AND lsw.end_date >= now() AND lsw.is_active IS TRUE )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND pcs.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ')
						AND CASE
								WHEN lsw.id IS NULL THEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
								ELSE lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
							END
						AND lt.deleted_on IS NULL
						AND lt.is_disabled IS FALSE ' . $strWhereIsCommercial . '
					ORDER BY lt.term_month' . $strOrderByDesc;

		return self::fetchLeaseTerms( $strSql, $objDatabase, false );

	}

	public static function fetchPropertyAssociatedLeaseTermsByPropertyIdByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
					DISTINCT( pcs.property_id ) as property_id,
					CASE WHEN ( true = lt.is_renewal ) THEN 1 ELSE 0 END as value
				FROM
					lease_terms lt
					JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id =lt.lease_term_structure_id )
					JOIN lease_term_structures lts ON ( lts.id = pcs.lease_term_structure_id AND lts.cid = pcs.cid )
					LEFT JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id AND lsw.property_id = pcs.property_id )
				WHERE
					lt.deleted_on IS NULL
					AND lt.is_renewal = true
					AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND lt.cid = ' . ( int ) $intCid . $strWhereSql . '
					AND lsw.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAvailableLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
				        pcs.property_id AS property_id
					FROM
					    lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id =lt.lease_term_structure_id )
						LEFT JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id AND lsw.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ') )
					WHERE
						lt.deleted_on IS NULL
				        AND lt.deleted_by IS NULL
				        AND CASE
				              WHEN lsw.id IS NULL THEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
				                ELSE lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
				              END
				        AND lt.cid = ' . ( int ) $intCid . '
						AND pcs.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ')
				        AND lt.is_renewal = TRUE
				        AND lsw.deleted_on IS NULL
				        AND lsw.deleted_by IS NULL
					GROUP BY
	                    pcs.property_id
	                HAVING
	                    count ( lt.name ) > 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermMonthsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						pcs.property_id,
						lt.term_month
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND pcs.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.is_disabled = false
						AND lt.is_prospect = true
						AND lt.deleted_on IS NULL ' . $strWhereSql . '
					ORDER BY
						lt.term_month DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseTermsByCid( $intCid, $objDatabase, $boolIsDisabled = false, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strCondition = ( true == $boolIsDisabled ) ? ' AND lt.is_disabled = true ' : ' AND lt.is_disabled = false ';
		$strCondition .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*
					FROM
						lease_terms lt
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.deleted_on IS NULL
						AND lt.deleted_by IS NULL
						' . $strCondition . '
					ORDER BY lt.id ASC';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchCommercialLeaseTermsByPropertyIdByTermMonthsByCid( $intPropertyId, $arrintTermMonths, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		if( false == valArr( $arrintTermMonths ) ) return NULL;

		$strSql = 'SELECT
						lt.*
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.term_month IN ( ' . sqlIntImplode( $arrintTermMonths ) . ' )
						AND lt.cid = ' . ( int ) $intCid . '
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND lt.deleted_by IS NULL
						AND lt.deleted_on IS NULL
						AND lt.occupancy_type_id = ' . COccupancyType::COMMERCIAL;

		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermByTermMonthByOccupancyTypeIdPropertyIdByCid( $intTermMonth, $intOccupancyTypeId = NULL, $intPropertyId, $intCid, $objDatabase, $boolIsProspect, $boolIsRenewal, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == is_null( $intOccupancyTypeId ) ) ? ' AND lt.occupancy_type_id = ' . ( int ) $intOccupancyTypeId : '';

		$strSql = 'SELECT
				      lt.*
				  FROM
				      lease_terms lt
				      JOIN property_charge_settings pcs ON ( lt.lease_term_structure_id = pcs.lease_term_structure_id AND lt.cid = pcs.cid )
				  WHERE
				      lt.cid = ' . ( int ) $intCid . '
				      AND pcs.property_id = ' . ( int ) $intPropertyId . '
				      AND lt.deleted_on IS NULL
				      AND lt.is_disabled = false
				      AND lt.is_unset IS FALSE
				      AND lt.term_month = ' . ( int ) $intTermMonth . '
				      AND lt.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . $strWhereSql;

		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		if( true == $boolIsProspect && true == $boolIsRenewal ) {

			$strSql .= 'AND ( lt.is_prospect = true OR lt.is_renewal = true ) ';

		} elseif( true == $boolIsProspect ) {

			$strSql .= 'AND lt.is_prospect = true ';
			$strSql .= ' AND show_on_website = true ';

		} elseif( true == $boolIsRenewal ) {
			$strSql .= 'AND lt.is_renewal = true ';
		}

		$strSql .= ' LIMIT 1';
		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermsByPropertyIdByCidByOptionalFilters( $intPropertyId, $intCid, $objDatabase, $boolIsStudentProperty = NULL, $boolIsRenewal = NULL, $boolIsCommercial = false, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strCondition = NULL;

		if( false == is_null( $boolIsRenewal ) ) {
			$strCondition .= ( false != $boolIsRenewal ? ' AND lt.is_renewal = true ' : ' AND lt.is_prospect = true ' );
		}
		if( false == is_null( $boolIsStudentProperty ) ) {
			$strCondition .= ' AND lt.lease_term_type_id = ' . ( false != $boolIsStudentProperty ? CLeaseTermType::DATE_BASED : CLeaseTermType::CONVENTIONAL );
		}
		$strCondition .= ( false != $boolIsCommercial ) ? '' : ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' ';
		$strCondition .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*,
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON pcs.cid = lt.cid AND pcs.property_id = ' . ( int ) $intPropertyId . ' AND lt.lease_term_structure_id = pcs.lease_term_structure_id
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.deleted_on IS NULL
						AND lt.is_disabled = FALSE
						AND lt.is_unset = FALSE
						AND lt.term_month > 0
						' . $strCondition . '
					ORDER BY
						lt.lease_term_type_id,
						lt.term_month,
						lt.id';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchCommercialLeaseTermsByTermMonthsByCid( $arrintTermMonths, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		if( false == valArr( $arrintTermMonths ) ) return NULL;

		$strSql = 'SELECT
						lt.*
					FROM
						lease_terms lt
					WHERE
						lt.term_month IN ( ' . sqlIntImplode( $arrintTermMonths ) . ' )
						AND lt.cid = ' . ( int ) $intCid . '
						AND lt.deleted_by IS NULL
						AND lt.deleted_on IS NULL
						AND lt.occupancy_type_id = ' . COccupancyType::COMMERCIAL;

		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchRenewalOfferedLeaseTermsByLeaseIntervalIdByLeaseIdByCid( $intLeaseIntervalId, $intLeaseId, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*
					FROM
						lease_intervals li
						JOIN applications a ON ( a.cid = li.cid AND a.lease_interval_id = li.id AND a.lease_id = li.lease_id )
						JOIN quotes q ON ( q.cid = a.cid AND q.application_id = a.id )
						JOIN quote_lease_terms qlt ON ( qlt.cid = q.cid AND qlt.quote_id = q.id )
						JOIN lease_terms lt ON ( lt.cid = qlt.cid AND lt.id = qlt.lease_term_id )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND li.id = ' . ( int ) $intLeaseIntervalId . '
						AND li.lease_id = ' . ( int ) $intLeaseId . '
						AND lt.deleted_on IS NULL
						AND lt.is_disabled = FALSE
						AND lt.is_unset = FALSE
						AND lt.term_month > 0
						AND q.cancelled_by IS NULL
						AND q.cancelled_ON IS NULL
						AND qlt.deleted_by IS NULL
						AND qlt.deleted_on IS NULL ' . $strWhereSql . '
					ORDER BY
						lt.term_month,
						lt.id';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermByNameByPropertyIdByCid( $strName, $intPropertyId, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		if( false == valStr( $strName ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						lt.*
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.name = \'' . $strName . '\'
						And lt.deleted_on IS NULL
						AND lt.term_month <> 0
				        AND lt.is_renewal IS TRUE
				        AND pcs.property_id = ' . ( int ) $intPropertyId . '
				        AND lt.is_disabled = false ' . $strWhereSql . '
                    ORDER BY lt.term_month
						LIMIT 1';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchDateBasedLeaseTermsByLeaseTermIdsByCid( $arrintLeaseTermIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintLeaseTermIds ) ) {
			return NULL;
		}

		$strSql     = 'SELECT
							lt.id
						FROM
							lease_terms lt
							JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
						WHERE
							lt.cid = ' . ( int ) $intCid . '
							AND lt.id IN ( ' . implode( ',', array_unique( $arrintLeaseTermIds ) ) . ' )
							AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultLeaseStartWindowByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		$strSql = 'SELECT
						lsw.*
					FROM
						lease_start_windows lsw
						JOIN property_charge_settings pcs ON ( lsw.cid = pcs.cid AND lsw.lease_start_structure_id = pcs.lease_start_structure_id )
					WHERE
						lsw.is_default = TRUE
						AND pcs.property_id = ' . ( int ) $intPropertyId . '
						AND pcs.cid = ' . ( int ) $intCid;

		$strSql .= ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND COALESCE( lsw.lease_term_type_id, 0 ) <> ' . CLeaseTermType::FLEXIBLE : '';

		return CLeaseStartWindows::fetchLeaseStartWindow( $strSql, $objDatabase );
	}

	public static function fetchDefaultGroupLeaseTermsByIdByPropertyIdByCid( $intId, $intPropertyId, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
							lt.*
						FROM
							lease_terms lt
							JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
							JOIN default_lease_terms dlt ON ( dlt.id = lt.default_lease_term_id AND dlt.deleted_on IS NULL ) 
						WHERE
							lt.cid = ' . ( int ) $intCid . '
							AND pcs.property_id = ' . ( int ) $intPropertyId . '
							AND lt.deleted_on IS NULL
							AND dlt.id = ' . ( int ) $intId . $strWhereSql . '
						LIMIT 1';

		return self::fetchLeaseTerm( $strSql, $objDatabase );
	}

	public static function fetchAllLeaseTermsByPropertyIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $boolIsProspect = true, $boolIsRenewal = false, $boolShowOnWebsite = false, $boolIsDateBased = false, $strMovinDate = NULL, $boolIncludeCommercialLeaseTerms = false, $boolConsiderPricingVisibility = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSqlWhereCondition = ( true == $boolIsProspect ) ? ' AND lt.is_prospect = true ' : '';

		$strSqlWhereCondition .= ( true == $boolIsRenewal ) ? ' AND lt.is_renewal = true ' : '';

		if( ( true == $boolIsProspect || true == $boolIsRenewal ) && true == $boolShowOnWebsite ) {
			$strSqlWhereCondition .= ' AND lt.show_on_website = true ';
		}

		if( true == $boolIsDateBased ) {
			$strSqlWhereCondition .= ' AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.is_active = true AND lsw.end_date >= CURRENT_DATE';
			if( true == valStr( $strMovinDate ) ) {
				$strSqlWhereCondition .= ' AND lsw.end_date >= \'' . $strMovinDate . '\'';
			}
		}
		if( false == $boolIncludeCommercialLeaseTerms ) {
			$strSqlWhereCondition .= ' AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' ';
		}

		if( true == $boolConsiderPricingVisibility && true == $boolIsDateBased ) {
				$strSqlWhereCondition .= ' AND EXISTS(
				SELECT FROM cached_rates cr
				WHERE lt.cid = cr.cid AND lsw.property_id = cr.property_id
				AND lt.id = cr.lease_term_id AND lsw.id = cr.lease_start_window_id
				AND cr.effective_date = CURRENT_DATE OR cr.effective_date IS NULL ) ';
		}

		$strSql = 'SELECT
					  array_to_string( ARRAY_AGG( lts.property_id || \'_\' || lts.id || CASE
					                                                                    WHEN lts.lease_start_window_id IS NOT NULL THEN \'_\' || lts.lease_start_window_id
					                                                                    ELSE \'\'
					                                                                  END ), \',\' ) AS lease_term_ids,
					  lts.lease_term_name AS name,
					  lts.term_month,
					  lts.start_date,
					  lts.end_date
					FROM
					  (
					    SELECT
					      lt.*,
					      util_get_translated( \'name\', lt.name, lt.details ) || CASE
					                   WHEN lsw.start_date IS NOT NULL AND lsw.end_date IS NOT NULL THEN \' ( \' || lsw.start_date || \' - \' || lsw.end_date || \' ) \'
					                   ELSE \'\'
					                 END AS lease_term_name,
					      lsw.id AS lease_start_window_id,
					      pcs.property_id,
					      lsw.start_date,
					      lsw.end_date,
					      lsw.offset_start_days,
					      lsw.offset_end_days
					    FROM
					      lease_terms lt
					      JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
					      JOIN load_properties( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . implode( ', ', $arrintPropertyGroupIds ) . ' ] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = pcs.property_id )
					      LEFT JOIN lease_start_windows lsw ON ( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id AND lsw.deleted_on IS NULL AND lsw.property_id = pcs.property_id AND lsw.end_date >= NOW( ) AND lsw.is_active IS TRUE )
					    WHERE
					      lt.cid = ' . ( int ) $intCid . ' AND
					      CASE
					        WHEN lsw.id IS NULL THEN lt.lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . '
					        ELSE lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
					      END AND
					      lt.deleted_on IS NULL AND
					      lt.is_disabled IS FALSE AND 
					      lt.default_lease_term_id IS DISTINCT FROM ' . CDefaultLeaseTerm::GROUP . '
						  ' . $strSqlWhereCondition . '
					    ORDER BY
					      lt.term_month
					  ) AS lts
					GROUP BY
					  lts.lease_term_name,
					  lts.term_month,
					  lts.start_date,
					  lts.end_date
					ORDER BY
					  lts.term_month';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchStudentLeaseTermsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsReturnSqlOnly = false ) {
		$strPropertyIds = '';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strPropertyIds = ', ARRAY [ ' . implode( ',', $arrintPropertyIds ) . ' ]';
		}

		$strSql = sprintf( 'SELECT 
						lt.cid,
						lsw.property_id,
						lt.id AS lease_term_id,
						lsw.id AS current_lease_start_window_id,
						lt.name AS lease_name,
						lsw.start_date,
						lsw.end_date,
						lsw.start_date AS current_window_start_date,
						lsw.end_date AS current_window_end_date,
						lt.term_month
					FROM lease_terms lt
						JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )
						JOIN load_properties( ARRAY [ %d ] %s ) lp ON lp.cid = lsw.cid AND lp.property_id = lsw.property_id AND lp.is_disabled = 0
						JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND lsw.property_id = pcs.property_id )
					WHERE 
					    lt.cid = %d
					    AND lt.is_disabled IS FALSE
					    AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
					    AND lsw.end_date >= CURRENT_DATE
					    AND lt.deleted_on IS NULL
					    AND lsw.deleted_on IS NULL
					    AND lsw.is_active IS TRUE
					    AND lt.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . ' 
					    AND ( COALESCE( lt.default_lease_term_id, 0 ) <> 201 OR lt.default_lease_term_id IS NULL )
					    AND lt.is_prospect IS TRUE
					    AND lt.show_on_website IS TRUE
					ORDER BY (lsw.start_date, lsw.end_date) ASC', $intCid, $strPropertyIds, $intCid );

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseTermIdByLeaseTermStructureIdByOccupancyTypeIdByLeaseTermTypeIdByCid( $intLeaseTermStructureId, $intOccupancyTypeId, $intLeaseTermTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						lt.id
					FROM
						lease_terms lt
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.lease_term_structure_id = ' . ( int ) $intLeaseTermStructureId . '
						AND lt.lease_term_type_id = ' . ( int ) $intLeaseTermTypeId . '
						AND lt.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND lt.deleted_on IS NULL
						ORDER BY lt.created_on';
		return parent::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchRateExistenceByLeaseTermIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 1 AS rate_flag
				   WHERE exists (
						SELECT r.id 
						FROM rates r
						WHERE
							r.lease_term_id = ' . ( int ) $intId . '
							AND r.cid = ' . ( int ) $intCid . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseTermsByCidByPropertyId( $intPropertyId, $intCid, $objDatabase, $boolIsReturnKeyedArray = true ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
							lt.*,
							lsw.id as lease_start_window_id,
							lsw.start_date,
							lsw.end_date,
							CASE
								when lsw.id IS NOT NULL then  lt.id || \'_\' || lsw.id::VARCHAR
								else  lt.id::VARCHAR
							end as custom_lease_term_id
						FROM
							lease_terms lt
							JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
							LEFT JOIN lease_start_windows lsw ON( lt.cid = lsw.cid AND lt.id = lsw.lease_term_id )
					   WHERE
							lt.cid = ' . ( int ) $intCid . '
							AND pcs.property_id = ' . ( int ) $intPropertyId . '
							AND lt.deleted_on IS NULL
					ORDER BY lt.term_month';

		return self::fetchLeaseTerms( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchGroupLeaseTermsByIdByPropertyIdsByCid( $intId, $arrintPropertyIds, $intCid, $objDatabase, $boolIsSkipFlexibleLeaseTerms = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strWhereSql = ( false == $boolIsSkipFlexibleLeaseTerms ) ? ' AND lt.lease_term_type_id <> ' . CLeaseTermType::FLEXIBLE : '';

		$strSql = 'SELECT
						*
					FROM (
							SELECT
								lt.*,
								RANK() OVER( PARTITION BY pcs.property_id ORDER BY lt.id ) AS rank
							FROM
								lease_terms lt
								JOIN property_charge_settings pcs ON ( pcs.cid = lt.cid AND lt.lease_term_structure_id = pcs.lease_term_structure_id )
								JOIN default_lease_terms dlt ON ( dlt.id = lt.default_lease_term_id AND dlt.deleted_on IS NULL ) 
							WHERE
								lt.cid = ' . ( int ) $intCid . '
								AND pcs.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
								AND lt.deleted_on IS NULL
								AND dlt.id = ' . ( int ) $intId . '
								' . $strWhereSql . '
						) AS sub
					WHERE
						sub.rank = 1';

		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseTermsByPropertyIdByCidByOptionalFilters( $intPropertyId, $intCid, $objDatabase, $boolIsStudentProperty ) {

		$strCondition = NULL;
		$strJoinCondition = $strSelectCondition = NULL;

		if( false != $boolIsStudentProperty ) {
			$strCondition .= ' AND lsw.end_date >= CURRENT_DATE ';
			$strJoinCondition .= 'JOIN lease_start_windows lsw ON ( lsw.cid = lt.cid AND lsw.lease_term_id = lt.id )';
			$strSelectCondition .= ' lsw.id AS lease_start_window_id, lsw.start_date, lsw.end_date, ';
		}

		$strSql = 'SELECT
						lt.*,
						' . $strSelectCondition . '
						pcs.property_id
					FROM
						lease_terms lt
						JOIN property_charge_settings pcs ON pcs.cid = lt.cid AND pcs.property_id = ' . ( int ) $intPropertyId . ' AND lt.lease_term_structure_id = pcs.lease_term_structure_id
						' . $strJoinCondition . '
					WHERE
						lt.cid = ' . ( int ) $intCid . '
						AND lt.deleted_on IS NULL
						AND lt.is_disabled = FALSE
						AND lt.is_unset = FALSE
						AND lt.term_month > 0
						' . $strCondition . '
					ORDER BY
						lt.lease_term_type_id,
						lt.term_month,
						lt.id';
		return self::fetchLeaseTerms( $strSql, $objDatabase );
	}

}
?>
