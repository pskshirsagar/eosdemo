<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerEvents
 * Do not add any new functions to this class.
 */

class CCustomerEvents extends CBaseCustomerEvents {

	public static function fetchCustomerEventByCompanyEventIdByCustomerIdByCid( $intCompanyEventId, $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerEvent( sprintf( 'SELECT ce.* FROM %s AS ce WHERE ce.company_event_id = %d AND ce.customer_id = %d AND ce.cid = %d LIMIT 1', 'customer_events', ( int ) $intCompanyEventId, ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerEventsByCidByCompanyEventIds( $arrintCompanyEventIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyEventIds ) )	return NULL;

		$strSql = 'SELECT
						*
					FROM customer_events
					WHERE cid = ' . ( int ) $intCid . '
						AND company_event_id IN  (' . implode( ',', $arrintCompanyEventIds ) . ')';

		return self::fetchCustomerEvents( $strSql, $objDatabase );
	}

	public static function fetchSimpleCustomerEventsByCompanyEventIdByCidByFieldNamesByPropertyIds( $intEventId, $intCid, $arrstrFieldName, $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrstrFieldName ) ) return [];
		if( false == valArr( $arrintPropertyIds ) ) return [];

		$strSql = 'SELECT
						DISTINCT ' . implode( ',', $arrstrFieldName ) . '
					FROM
						customer_events AS custevnt
						JOIN lease_customers AS lc ON ( lc.customer_id::integer = custevnt.customer_id::integer AND lc.cid::integer = custevnt.cid::integer )
						JOIN leases l ON ( l.id::integer = lc.lease_id::integer AND l.cid::integer = lc.cid::integer )
					WHERE
						custevnt.company_event_id = ' . ( int ) $intEventId . '
						AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND custevnt.cid = ' . ( int ) $intCid;

		$arrstrCustomerIds = fetchData( $strSql, $objDatabase );
		$arrstrFinalCustomerIds = [];

		if( true == valArr( $arrstrCustomerIds ) ) {
			foreach( $arrstrCustomerIds as $arrstrCustomerIds ) {
				$arrstrFinalCustomerIds[] = $arrstrCustomerIds['customer_id'];
			}
		}

		return $arrstrFinalCustomerIds;

	}

	public static function fetchCustomerEventIdsByCompanyEventIdByCidByPropertyIds( $intEventId, $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return [];
		$strSql = 'SELECT
						DISTINCT ce.customer_id,
						l.property_id as property_id
					FROM
						customer_events ce, lease_customers lc, leases l,property_events pe
					WHERE
						l.id = lc.lease_id
						AND l.cid = lc.cid
						AND l.property_id = pe.property_id
						AND lc.customer_id = ce.customer_id
						AND lc.cid = ce.cid
						AND pe.cid = ce.cid
						AND ce.company_event_id = ' . ( int ) $intEventId . '
						AND ce.cid = ' . ( int ) $intCid . '
						AND l.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		$arrstrCustomerIds = fetchData( $strSql, $objDatabase );
		$arrstrFinalCustomerIds = [];

		if( true == valArr( $arrstrCustomerIds ) ) {
			foreach( $arrstrCustomerIds as $arrstrCustomerIds ) {
				$arrstrFinalCustomerIds[$arrstrCustomerIds['customer_id']]['customer_id'] = $arrstrCustomerIds['customer_id'];
				$arrstrFinalCustomerIds[$arrstrCustomerIds['customer_id']]['property_id'] = $arrstrCustomerIds['property_id'];
			}
		}

		return $arrstrFinalCustomerIds;
	}

	public static function fetchCustomerEventsByCompanyEventIdByCidByPropertyIds( $intEventId, $intCid, $arrintPropertyIds, $objDatabase, $intExcludeCustomerId ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = 'SELECT
						DISTINCT lc.customer_id
					FROM
						customer_events AS custevnt
						JOIN lease_customers AS lc ON ( lc.customer_id::integer = custevnt.customer_id::integer AND lc.cid::integer = custevnt.cid::integer )
						JOIN leases l ON ( l.id::integer = lc.lease_id::integer AND l.cid::integer = lc.cid::integer )
					WHERE
						custevnt.company_event_id = ' . ( int ) $intEventId . '
						AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND custevnt.cid = ' . ( int ) $intCid;

		if( true == isset( $intExcludeCustomerId ) ) {
			$strSql .= ' AND lc.customer_id != ' . ( int ) $intExcludeCustomerId . ' ';
		}

		$arrstrCustomerIds = fetchData( $strSql, $objDatabase );
		return $arrstrCustomerIds;
	}

}
?>