<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COnSiteContactTypes
 * Do not add any new functions to this class.
 */

class COnSiteContactTypes extends CBaseOnSiteContactTypes {

	public static function fetchOnSiteContactTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'COnSiteContactType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchOnSiteContactType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'COnSiteContactType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}
}
?>