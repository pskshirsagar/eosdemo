<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyStudentExceptionTypes
 * Do not add any new functions to this class.
 */

class CSubsidyStudentExceptionTypes extends CBaseSubsidyStudentExceptionTypes {

	public static function fetchSubsidyStudentExceptionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CSubsidyStudentExceptionType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSubsidyStudentExceptionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CSubsidyStudentExceptionType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveSubsidyStudentExceptionTypesBySubsidyTypeIds( $arrintSubsidyTypeIds, $objDatabase ) {

		if( false == valArr( $arrintSubsidyTypeIds ) ) return NULL;

		$strSql = '
					SELECT 
						* 
					FROM 
						subsidy_student_exception_types 
					WHERE 
						ARRAY[ ' . implode( ',', $arrintSubsidyTypeIds ) . ' ] && subsidy_type_ids
						AND is_published = TRUE
					ORDER BY 
						id ASC';

		return self::fetchSubsidyStudentExceptionTypes( $strSql, $objDatabase );
	}

}
?>