<?php

class CDataChange extends CBaseDataChange {

	const TABLE_TYPE_LOG			= 'log';
	const TABLE_TYPE_SETUP			= 'setup';
	const TABLE_TYPE_TRANSACTION	= 'transaction';

	protected $m_intOldGlAccountTypeId;
	protected $m_intNewGlAccountTypeId;
	protected $m_intOldLedgerFilterId;
	protected $m_intNewLedgerFilterId;
	protected $m_intOldReferenceTypeId;
	protected $m_intNewReferenceTypeId;
	protected $m_intOldDebitGlAccountId;
	protected $m_intNewDebitGlAccountId;
	protected $m_intOldCreditGlAccountId;
	protected $m_intNewCreditGlAccountId;
	protected $m_intOldGlTransactionTypeId;
	protected $m_intNewGlTransactionTypeId;

	/**
	 * Get Functions
	 */

	public function getOldGlAccountTypeId() {
		return $this->m_intOldGlAccountTypeId;
	}

	public function getNewGlAccountTypeId() {
		return $this->m_intNewGlAccountTypeId;
	}

	public function getOldReferenceTypeId() {
		return $this->m_intOldReferenceTypeId;
	}

	public function getNewReferenceTypeId() {
		return $this->m_intNewReferenceTypeId;
	}

	public function getOldDebitGlAccountId() {
		return $this->m_intOldDebitGlAccountId;
	}

	public function getNewDebitGlAccountId() {
		return $this->m_intNewDebitGlAccountId;
	}

	public function getOldCreditGlAccountId() {
		return $this->m_intOldCreditGlAccountId;
	}

	public function getNewCreditGlAccountId() {
		return $this->m_intNewCreditGlAccountId;
	}

	public function getOldLedgerFilterId() {
		return $this->m_intOldLedgerFilterId;
	}

	public function getNewLedgerFilterId() {
		return $this->m_intNewLedgerFilterId;
	}

	public function getOldGlTransactionTypeId() {
		return $this->m_intOldGlTransactionTypeId;
	}

	public function getNewGlTransactionTypeId() {
		return $this->m_intNewGlTransactionTypeId;
	}

	/**
	 * Set Functions
	 */

	public function setOldGlAccountTypeId( $intOldGlAccountTypeId ) {
		$this->m_intOldGlAccountTypeId = CStrings::strToIntDef( $intOldGlAccountTypeId );
	}

	public function setNewGlAccountTypeId( $intNewGlAccountTypeId ) {
		$this->m_intNewGlAccountTypeId = CStrings::strToIntDef( $intNewGlAccountTypeId );
	}

	public function setOldReferenceTypeId( $intOldReferenceTypeId ) {
		$this->m_intOldReferenceTypeId = CStrings::strToIntDef( $intOldReferenceTypeId );
	}

	public function setNewReferenceTypeId( $intNewReferenceTypeId ) {
		$this->m_intNewReferenceTypeId = CStrings::strToIntDef( $intNewReferenceTypeId );
	}

	public function setOldDebitGlAccountId( $intOldDebitGlAccountId ) {
		$this->m_intOldDebitGlAccountId = CStrings::strToIntDef( $intOldDebitGlAccountId );
	}

	public function setNewDebitGlAccountId( $intNewDebitGlAccountId ) {
		$this->m_intNewDebitGlAccountId = CStrings::strToIntDef( $intNewDebitGlAccountId );
	}

	public function setOldCreditGlAccountId( $intOldCreditGlAccountId ) {
		$this->m_intOldCreditGlAccountId = CStrings::strToIntDef( $intOldCreditGlAccountId );
	}

	public function setNewCreditGlAccountId( $intNewCreditGlAccountId ) {
		$this->m_intNewCreditGlAccountId = CStrings::strToIntDef( $intNewCreditGlAccountId );
	}

	public function setOldLedgerFilterId( $intLedgerFilterId ) {
		$this->m_intOldLedgerFilterId = CStrings::strToIntDef( $intLedgerFilterId, NULL, false );
	}

	public function setNewLedgerFilterId( $intLedgerFilterId ) {
		$this->m_intNewLedgerFilterId = CStrings::strToIntDef( $intLedgerFilterId, NULL, false );
	}

	public function setOldGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->m_intOldGlTransactionTypeId = CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false );
	}

	public function setNewGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->m_intNewGlTransactionTypeId = CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false );
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataChangeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldReferenceId() {
		$boolIsValid = true;

		if( true == is_null( $this->getOldReferenceId() ) ) {

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::GL_ACCOUNT_CHANGE:
					$strMessage = __( '\'GL account to replace\' is required.' );
					break;

				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( '\'Charge code to replace\' is required.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'old_reference_id', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valNewReferenceId() {
		$boolIsValid = true;

		if( true == is_null( $this->getNewReferenceId() ) ) {

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::GL_ACCOUNT_CHANGE:
					$strMessage = __( '\'Replace account with\' is required.' );
					break;

				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( '\'Replace charge code with\' is required.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_reference_id', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valOldReferenceName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewReferenceName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumberOfTransactionsAffected() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChangeDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountTypeIds() {
		$boolIsValid = true;

		if( false == is_null( $this->getOldGlAccountTypeId() ) && false == is_null( $this->getNewGlAccountTypeId() ) && $this->getOldGlAccountTypeId() != $this->getNewGlAccountTypeId() ) {

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::GL_ACCOUNT_CHANGE:
					$strMessage = __( 'GL accounts must be of the same GL account type.' );
					break;

				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( 'Charge codes must be of the same GL account type.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valReferenceTypeIds() {
		$boolIsValid = true;

		if( false == is_null( $this->getOldReferenceTypeId() ) && false == is_null( $this->getNewReferenceTypeId() ) && $this->getOldReferenceTypeId() != $this->getNewReferenceTypeId() ) {

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::GL_ACCOUNT_CHANGE:
					$strMessage = __( 'GL accounts must have the same GL account usage type.' );
					break;

				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( 'Charge codes must have the same Charge Code Type.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valDuplicates( $objClientDatabase ) {
		$boolIsValid = true;

		$arrobjCDataChanges = array_keys( rekeyObjects( 'OldReferenceId', ( array ) CDataChanges::fetchDataChangesByDataChangeTypeIdByCid( $this->getDataChangeTypeId(), $this->getCid(), $objClientDatabase ) ) );

		// if both records are same then throw error.
		if( false == is_null( $this->getOldReferenceId() ) && false == is_null( $this->getNewReferenceId() ) && $this->getOldReferenceId() == $this->getNewReferenceId() ) {

			$boolIsValid = false;

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::GL_ACCOUNT_CHANGE:
					$strMessage = __( 'GL accounts cannot be same.' );
					break;

				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( 'Charge codes cannot be same.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strMessage ) );
		}

		// if old record is already processed then throw error.
		if( false == is_null( $this->getOldReferenceId() ) && true == in_array( $this->getOldReferenceId(), $arrobjCDataChanges ) ) {

			$boolIsValid = false;

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::GL_ACCOUNT_CHANGE:
					$strMessage = __( '\'GL account to replace\' has already been processed.' );
					break;

				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( '\'Charge code to replace\' has already been processed.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_reference_id', $strMessage ) );
		}

		// if new record is already processed then throw error.
		if( false == is_null( $this->getNewReferenceId() ) && true == in_array( $this->getNewReferenceId(), $arrobjCDataChanges ) ) {

			$boolIsValid = false;

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::GL_ACCOUNT_CHANGE:
					$strMessage = __( '\'Replace account with\' has already been processed.' );
					break;

				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( '\'Replace charge code with\' has already been processed.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_reference_id', $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valDebitGlAccountIds() {
		$boolIsValid = true;

		if( false == is_null( $this->getOldDebitGlAccountId() ) && false == is_null( $this->getNewDebitGlAccountId() ) && $this->getOldDebitGlAccountId() != $this->getNewDebitGlAccountId() ) {

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( 'Charge codes must have the same debit GL account.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valCreditGlAccountIds() {
		$boolIsValid = true;

		if( false == is_null( $this->getOldCreditGlAccountId() ) && false == is_null( $this->getNewCreditGlAccountId() ) && $this->getOldCreditGlAccountId() != $this->getNewCreditGlAccountId() ) {

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( 'Charge codes must have the same credit GL account.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valPropertyBankAccounts( $objClientDatabase ) {

		$boolIsValid = true;

		switch( $this->getDataChangeTypeId() ) {
			case CDataChangeType::GL_ACCOUNT_CHANGE:
				if( true == is_null( $this->getOldReferenceId() ) || true == is_null( $this->getNewReferenceId() ) ) {
					return $boolIsValid;
				}

				$arrobjPropertyBankAccounts = ( array ) CPropertyBankAccounts::fetchPropertyBankAccountsByOldAndNewGlAccountIdByCid( $this->getOldReferenceId(), $this->getNewReferenceId(), $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjPropertyBankAccounts ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'GL account \'{%s, 0}\' is being used in bank account(s) configuration.', [ $this->getNewReferenceName() ] ) ) );
				}
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valGlAccountProperties( $objClientDatabase ) {

		$boolIsValid = true;

		switch( $this->getDataChangeTypeId() ) {
			case CDataChangeType::GL_ACCOUNT_CHANGE:
				if( true == is_null( $this->getOldReferenceId() ) || true == is_null( $this->getNewReferenceId() ) ) {
					return $boolIsValid;
				}

				$arrobjGlAccountProperties = ( array ) CGlAccountProperties::fetchGlAccountPropertiesByOldAndNewGlAccountIdByCid( $this->getOldReferenceId(), $this->getNewReferenceId(), $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjGlAccountProperties ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'GL account \'{%s, 0}\' is being used in GL account(s)-property(s) association.', [ $this->getOldReferenceName() ] ) ) );
				}
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valApFormulaProperties( $objClientDatabase ) {

		$boolIsValid = true;

		switch( $this->getDataChangeTypeId() ) {
			case CDataChangeType::GL_ACCOUNT_CHANGE:
				if( true == is_null( $this->getOldReferenceId() ) || true == is_null( $this->getNewReferenceId() ) ) {
					return $boolIsValid;
				}

				$arrobjApFormulaProperties = ( array ) \Psi\Eos\Entrata\CApFormulaProperties::createService()->fetchApFormulaPropertiesByOldAndNewGlAccountIdByCid( $this->getOldReferenceId(), $this->getNewReferenceId(), $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjApFormulaProperties ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'GL account \'{%s, 0}\' is being used in invoice allocation settings.', [ $this->getOldReferenceName() ] ) ) );
				}
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valScheduledCharges( $objClientDatabase ) {

		$boolIsValid = true;

		switch( $this->getDataChangeTypeId() ) {
			case CDataChangeType::AR_CODE_CHANGE:
				if( true == is_null( $this->getOldReferenceId() ) || true == is_null( $this->getNewReferenceId() ) ) {
					return $boolIsValid;
				}

				$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByOldAndNewArCodeIdByArTriggerIdByCid( $this->getOldReferenceId(), $this->getNewReferenceId(), CArCodeType::RENT, $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjScheduledCharges ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Charge code \'{%s, 0}\' is being used in scheduled charges.', [ $this->getOldReferenceName() ] ) ) );
					break;
				}
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPropertyMerchantAccounts( $objClientDatabase ) {

		$boolIsValid = true;

		switch( $this->getDataChangeTypeId() ) {
			case CDataChangeType::AR_CODE_CHANGE:
				if( true == is_null( $this->getOldReferenceId() ) || true == is_null( $this->getNewReferenceId() ) ) {
					return $boolIsValid;
				}

				$arrobjPropertyMerchantAccounts = ( array ) CPropertyMerchantAccounts::fetchPropertyMerchantAccountsByOldAndNewArCodeIdByCid( $this->getOldReferenceId(), $this->getNewReferenceId(), $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjPropertyMerchantAccounts ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Charge code \'{%s, 0}\' is being used in property merchant accounts configuration.', [ $this->getOldReferenceName() ] ) ) );
				}
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valRevenueArCodes( $objClientDatabase ) {

		$boolIsValid = true;

		switch( $this->getDataChangeTypeId() ) {
			case CDataChangeType::AR_CODE_CHANGE:
				if( true == is_null( $this->getOldReferenceId() ) || true == is_null( $this->getNewReferenceId() ) ) {
					return $boolIsValid;
				}

				$arrobjRevenueArCodes = ( array ) CRevenueArCodes::fetchRevenueArCodesByOldAndNewArCodeIdByCid( $this->getOldReferenceId(), $this->getNewReferenceId(), $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjRevenueArCodes ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Charge code \'{%s, 0}\' is being used in revenue ar codes.', [ $this->getOldReferenceName() ] ) ) );
					break;
				}
				break;

			default:
				return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valLedgerFilters() {

		$boolIsValid = true;

		if( false == is_null( $this->getOldLedgerFilterId() ) && false == is_null( $this->getNewLedgerFilterId() ) && $this->getOldLedgerFilterId() != $this->getNewLedgerFilterId() ) {

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( 'Charge codes must have same associated ledger.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strMessage ) );
		}

		return $boolIsValid;
	}

	public function valIsSystem( $objClientDatabase ) {

		$boolIsValid	= true;

		if( false == is_null( $this->getOldReferenceId() ) && $this->getOldReferenceId() != $this->getNewReferenceId() ) {

			$strWhere		= 'WHERE cid = ' . $this->getCid() . ' AND id = ' . $this->getOldReferenceId();

			switch( $this->getDataChangeTypeId() ) {

				case CDataChangeType::AR_CODE_CHANGE:
					$strWhere	.= '  AND is_system = TRUE ';
					if( 0 < \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeCount( $strWhere, $objClientDatabase ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'System AR code can not be deleted.' ) ) );
					}
					break;

				case CDataChangeType::GL_ACCOUNT_CHANGE:
					$strWhere	.= '  AND system_code IS NOT NULL';
					if( 0 < CGlAccounts::fetchGlAccountCount( $strWhere, $objClientDatabase ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'System GL account can not be deleted.' ) ) );
					}
					break;

				default:
					$boolIsValid = true;
			}
		}

		return $boolIsValid;
	}

	public function valGlTransactionTypeId() {
		$boolIsValid = true;

		if( false == is_null( $this->getOldGlTransactionTypeId() ) && false == is_null( $this->getNewGlTransactionTypeId() ) && $this->getOldGlTransactionTypeId() != $this->getNewGlTransactionTypeId() ) {

			switch( $this->getDataChangeTypeId() ) {
				case CDataChangeType::AR_CODE_CHANGE:
					$strMessage = __( 'Charge codes must have the same GL transaction type.' );
					break;

				default:
					$strMessage = NULL;
					break;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strMessage ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDuplicates( $objClientDatabase );
				$boolIsValid &= $this->valOldReferenceId();
				$boolIsValid &= $this->valNewReferenceId();
				$boolIsValid &= $this->valGlAccountTypeIds();
				$boolIsValid &= $this->valReferenceTypeIds();
				$boolIsValid &= $this->valDebitGlAccountIds();
				$boolIsValid &= $this->valCreditGlAccountIds();
				$boolIsValid &= $this->valLedgerFilters();
				$boolIsValid &= $this->valPropertyBankAccounts( $objClientDatabase );
				$boolIsValid &= $this->valGlAccountProperties( $objClientDatabase );
				$boolIsValid &= $this->valApFormulaProperties( $objClientDatabase );
				$boolIsValid &= $this->valScheduledCharges( $objClientDatabase );
				$boolIsValid &= $this->valPropertyMerchantAccounts( $objClientDatabase );
				$boolIsValid &= $this->valRevenueArCodes( $objClientDatabase );
				$boolIsValid &= $this->valIsSystem( $objClientDatabase );
				$boolIsValid &= $this->valGlTransactionTypeId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

}
?>