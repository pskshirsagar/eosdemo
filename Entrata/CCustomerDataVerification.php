<?php

class CCustomerDataVerification extends CBaseCustomerDataVerification {

	const _3RD_PARTY_WRITTEN 		= 1;
	const _3RD_PARTY_DOCUMENTATION 	= 2;
	const _3RD_PARTY_ORAL 			= 3;
	const APPLICANT_DOCUMENTATION 	= 4;
	const SELF_DECLARATION 			= 5;

	public 	  $m_boolIsDelete;
	protected $m_strVerifiedByName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( false == valId( $this->getCustomerId() ) ) {
		 	$boolIsValid = false;
	 		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', 'Customer Id is not set.' ) );
	 	}

		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;

		if( false == valId( $this->getApplicationId() ) ) {
		 	$boolIsValid = false;
	 		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_id', 'Application Id is not set.' ) );
	 	}

		return $boolIsValid;
	}

	public function valVerificationTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getVerificationTypeId() ) ) {
		 	$boolIsValid = false;
	 		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'verification_type_id', 'Verification method is required.' ) );
	 	}

		return $boolIsValid;
	}

	public function valCustomerDataTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getCustomerDataTypeId() ) ) {
		 	$boolIsValid = false;
	 		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_data_type_id', 'Customer data type id is required.' ) );
	 	}

	 	return $boolIsValid;
	}

	public function valCustomerDataReferenceId() {
		$boolIsValid = true;

		if( false == valId( $this->getCustomerDataReferenceId() ) ) {
		 	$boolIsValid = false;
	 		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'data_reference_id', 'Customer data refernece id is required.' ) );
	 	}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVerifiedBy() {
		$boolIsValid = true;

		if( false == valId( $this->getVerifiedBy() ) ) {
		 	$boolIsValid = false;
	 		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'verified_by', 'Verified by is required.' ) );
	 	}

		return $boolIsValid;
	}

	public function valVerifiedOn() {
		$boolIsValid = true;

		if( true === is_null( $this->getVerifiedOn() ) || false == valStr( $this->getVerifiedOn() ) ) {
		 	$boolIsValid = false;
	 		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'verified_on', 'Verified on is required.' ) );
	 	}

		return $boolIsValid;
	}

	public function validateCustomerDataTypeId( $intCustomerDataTypeId, $objDatabase ) {

		$boolIsValid = true;

		if( true === is_null( $intCustomerDataTypeId ) || false == valStr( $intCustomerDataTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_data_type_id', 'Customer data type id is required.' ) );
		}

		$objCustomerDataType = CCustomerDataTypes::fetchCustomerDataTypeById( $intCustomerDataTypeId, $objDatabase );

		if( false == valObj( $objCustomerDataType, 'CCustomerDataType' ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = $boolIsDelete;
	}

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	public function setVerifiedByName( $strVerifiedByName ) {
		$this->m_strVerifiedByName = $strVerifiedByName;
	}

	public function getVerifiedByName() {
		return $this->m_strVerifiedByName;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->validateCustomerDataTypeId( $this->getCustomerDataTypeId(), $objDatabase );
				$boolIsValid &= $this->valVerifiedBy();
				$boolIsValid &= $this->valVerifiedOn();
				if( false == in_array( $this->getCustomerDataTypeId(), CCustomerDataType::$c_arrintCustomerDataTypeIdsWithoutVerificationType ) ) {
					$boolIsValid &= $this->valVerificationTypeId();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) && true == isset( $arrmixValues['name_last'] ) )
			$this->setVerifiedByName( $arrmixValues['name_first'] . ' ' . $arrmixValues['name_last'] );
	}

}
?>