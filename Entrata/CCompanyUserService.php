<?php

class CCompanyUserService extends CBaseCompanyUserService {

	/**
	 * Create Functions Get Functions
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCompanyUserServicesFields() {
		return [
			'user_services' => __( 'User Services' )
		];
	}

    public function __construct() {
        parent::__construct();
		// FIXME remove this static declaration once UI implemented at CA which allows user to select specific version.
	    $this->m_intMinorApiVersionId = CApiVersion::TMP_DEFAULT_MINOR_API_VERSION;
        return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valServiceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>