<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyUnitAssignmentTypes
 * Do not add any new functions to this class.
 */

class CSubsidyUnitAssignmentTypes extends CBaseSubsidyUnitAssignmentTypes {

	public static function fetchActiveSubsidyUnitAssignmentTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM subsidy_unit_assignment_types WHERE is_published = TRUE ORDER BY order_num';
		return self::fetchSubsidyUnitAssignmentTypes( $strSql, $objDatabase );
	}
}
?>