<?php

class CCustomAdType extends CBaseCustomAdType {

	const TEXT			= 1;
	const IMAGE 		= 2;

	public static $c_arrintCustomAdTypes = [
        'text'		=> CCustomAdType::TEXT,
        'image'		=> CCustomAdType::IMAGE

	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>