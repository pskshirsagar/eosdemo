<?php

class CArDepositType extends CBaseArDepositType {

	 const ELECTRONIC		= 1;
	 const MANUAL			= 2;
	 const PERIOD_CLOSE		= 3;

    public function assignSmartyData( $objSmarty ) {
		$objSmarty->assign( 'AR_DEPOSIT_TYPE_ELECTRONIC',  	self::ELECTRONIC );
		$objSmarty->assign( 'AR_DEPOSIT_TYPE_MANUAL', 		self::MANUAL );
		$objSmarty->assign( 'AR_DEPOSIT_TYPE_PERIOD_CLOSE',	self::PERIOD_CLOSE );
    }
}
?>