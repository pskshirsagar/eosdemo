<?php

class CSubsidySpecialClaim extends CBaseSubsidySpecialClaim {

	const REGULAR_VACANCY_MAXIMUM_DAYS_VACANCY = 60;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) || '' == $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client Id is required.' ) ) );
		} elseif( false == is_int( $this->getCid() ) || 0 >= $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client Id must be a valid Integer.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) || '' == $this->getPropertyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Property Id is required.' ) ) );
		} elseif( false == is_int( $this->getPropertyId() ) || 0 >= $this->getPropertyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property Id must be a valid Integer.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSubsidySpecialClaimTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSubsidySpecialClaimTypeId() ) || '' == $this->getSubsidySpecialClaimTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Subsidy Special Claim Type Id is required.' ) ) );
		} elseif( false == is_int( $this->getSubsidySpecialClaimTypeId() ) || 0 >= $this->getSubsidySpecialClaimTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_special_claim_type_id', __( 'Subsidy Special Claim Type Id must be a valid Integer.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSubsidySpecialClaimStatusTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSubsidySpecialClaimStatusTypeId() ) || '' == $this->getSubsidySpecialClaimStatusTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Subsidy Special Claim Status Type Id is required.' ) ) );
		} elseif( false == is_int( $this->getSubsidySpecialClaimStatusTypeId() ) || 0 >= $this->getSubsidySpecialClaimStatusTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_special_claim_status_type_id', __( 'Subsidy Special Claim Status Type Id must be a valid Integer.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSubsidyHapRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valData() {
		$boolIsValid = true;

		if( true == valStr( $this->getData() ) ) {
			$objData = json_decode( $this->getData() );
			if( false == valObj( $objData, 'stdClass' ) || true == empty( ( array ) $objData ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'data', __( 'Data must be a valid json string.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valSecurityDepositAmountRequiredCollected() {
		$boolIsValid = true;

		if( true == valStr( $this->getData() ) ) {
			$arrstrData = ( array ) json_decode( $this->getData() );

			if( ( float ) $arrstrData['security_deposit_amount_required'] > ( float ) $arrstrData['security_deposit_amount_collected'] ) {
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getTotalAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_amount', __( 'Total Amount must be valid number.' ) ) );
		} elseif( 0 >= $this->getTotalAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_amount', __( 'Total Amount must be greater than 0.' ) ) );
		} elseif( false == is_null( $this->getTotalAmount() ) && ( false == is_float( $this->getTotalAmount() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_amount', __( 'Total Amount must be a valid Float value.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaxAllowableClaim() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdjustedAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdjustmentReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDenialReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDaysVacant() {
		$boolIsValid = true;
		if( \CSubsidySpecialClaimType::REGULAR_VACANCY == $this->getSubsidySpecialClaimTypeId() ) {
			$arrmixSpecialClaimDetailsData = json_decode( $this->getData(), true );
			if( self::REGULAR_VACANCY_MAXIMUM_DAYS_VACANCY < ( int ) $arrmixSpecialClaimDetailsData['days_vacant'] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_vacant', __( 'Days Vacant should be less than ' ) . self::REGULAR_VACANCY_MAXIMUM_DAYS_VACANCY . '.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTotalAmount();
				$boolIsValid &= $this->valDaysVacant();
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valSubsidySpecialClaimTypeId();
				$boolIsValid &= $this->valSubsidySpecialClaimStatusTypeId();
				$boolIsValid &= $this->valData();
				$boolIsValid &= $this->valTotalAmount();
				$boolIsValid &= $this->valDaysVacant();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == valArr( $arrmixValues['data'] ) ) {
			$this->setData( json_encode( $arrmixValues['data'] ) );
		}
	}

}
?>