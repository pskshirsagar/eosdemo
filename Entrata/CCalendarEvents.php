<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEvents
 * Do not add any new functions to this class.
 */

class CCalendarEvents extends CBaseCalendarEvents {

	public static function fetchCalendarEvents( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, [ 'CCalendarEvents', 'calendarEventFactory' ], $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function calendarEventFactory( $arrmixValues ) {
		switch( $arrmixValues['event_type'] ) {
			case 'Work Order':
				return new CWorkOrderCalendarEvent();
				break;

			default:
				return new CCalendarEvent();
		}
	}

	private static function buildMaxSimultaneousEventCountSqlByStartDatetimeByEndDatetimeByCategoryIdsByPropertyIdByCid( $strStartDatetime, $strEndDatetime, $arrintCategoryIds, $intPropertyId, $intCid, $intExcludeEventId = NULL ) {
		if( false == valArr( $arrintCategoryIds ) ) return NULL;
		if( false == valStr( $strStartDatetime ) || false == valStr( $strEndDatetime ) ) return NULL;

		$strSql = '
			WITH
			-- Find all property events in the new event window of time
			ce2 AS (
				SELECT
					ce1.id,
					ce1.start_datetime,
					ce1.end_datetime
				FROM
					calendar_events AS ce1
					JOIN calendar_event_associations AS cea1 ON (
						cea1.calendar_event_id = ce1.id
						AND cea1.cid = ce1.cid
						AND cea1.property_id = ' . ( int ) $intPropertyId . ' )
					JOIN calendar_event_types as cet ON ( 
						cet.id = ce1.calendar_event_type_id
						AND cet.cid = ce1.cid )
				WHERE
					ce1.cid = ' . ( int ) $intCid . '
					AND ce1.calendar_event_category_id IN ( ' . implode( ', ', $arrintCategoryIds ) . " )
					AND ( ce1.start_datetime, ce1.end_datetime ) OVERLAPS ( '" . date( 'c', strtotime( $strStartDatetime ) ) . "', '" . date( 'c', strtotime( $strEndDatetime ) ) . "' )
					AND ce1.deleted_on IS NULL
					AND cet.default_calendar_event_type_id != " . CDefaultCalendarEventType::AMENITY_RESERVATION;

		if( true == is_int( $intExcludeEventId ) ) {
			$strSql .= ' AND ce1.id != ' . ( int ) $intExcludeEventId;
		}

		$strSql .= " AND ( ( ce1.event_details->>'event_all_day' ) IS NULL OR ce1.event_details->>'event_all_day' = '0' )
					AND ( ( ce1.event_details->>'event_recurring' ) IS NULL OR ce1.event_details->>'event_recurring' = '0' ) ),
			-- Get every overlapping event for each of the target events above
			intersecting_events AS (
				SELECT
					ce2.id,
					ce2.start_datetime,
					ce2.end_datetime,
					ce3.id as ie_id,
					ce3.start_datetime as ie_start,
					ce3.end_datetime as ie_end
				FROM
					ce2
					LEFT JOIN ce2 AS ce3 ON (
						ce2.id != ce3.id
						AND ( ce2.start_datetime, ce2.end_datetime ) OVERLAPS ( ce3.start_datetime, ce3.end_datetime ) ) ),
			-- Add a weight/offset field for calculating overlapping event scores
			event_dates AS (
				SELECT id, ie_start AS start_datetime, 1 AS count_offset FROM intersecting_events
				UNION ALL
				SELECT id, ie_end AS start_datetime, -1 AS count_offset FROM intersecting_events ),
			-- Calculate the number of consecutive event starts between event ends for each overlapping event
			event_counts AS (
				SELECT id,
				SUM( count_offset ) OVER(
					PARTITION BY id
					ORDER BY start_datetime, count_offset
					ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS simultaneous_count
				FROM event_dates ),
			-- Get the highest count per event
			simultaneous_counts AS (
				SELECT
					id,
					MAX( simultaneous_count ) + 1 AS max_simultaneous_count
				FROM
					event_counts
				GROUP BY
					id )
			-- Get the overall highest count
			SELECT
				MAX( max_simultaneous_count ) AS count
			FROM
				simultaneous_counts";

		return $strSql;
	}

	public static function fetchCalendarEventByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				cev.*,
				util_get_translated( \'name\', cet.name, cet.details ) AS event_type,
				add.name AS amenity_name,
				cet.default_calendar_event_type_id
			FROM
				calendar_events cev
				JOIN calendar_event_types cet ON cet.cid = cev.cid AND cet.id = cev.calendar_event_type_id
				LEFT JOIN property_add_on_reservations par ON ( par.id = cev.reference_id AND par.cid = cev.cid AND cet.default_calendar_event_type_id = ' . ( int ) CDefaultCalendarEventType::AMENITY_RESERVATION . ' )
				LEFT JOIN rate_associations AS ra ON ( ra.id = par.rate_association_id AND ra.cid = par.cid )
				LEFT JOIN add_ons add ON( add.id = ra.ar_origin_reference_id AND add.cid = ra.cid )
			WHERE
				cev.id = ' . ( int ) $intId . '
				AND cev.cid = ' . ( int ) $intCid . '
		';

		return self::fetchCalendarEvent( $strSql, $objDatabase );
	}

	public static function fetchCalendarEventsByCalendarCompanyUserIdsByCalendarPropertyIdsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $arrintCalendarEventTypeIds, $arrintCalendarCompanyUserIds, $arrintCalendarPropertyIds, $strStartDate = NULL, $strEndDate = NULL, $strTimezoneOffset = NULL ) {
		if( true == empty( $arrintCalendarEventTypeIds ) ) return false;

		$arrstrSqls = [];
		$objStart = ( true == valStr( $strTimezoneOffset ) ) ? new DateTime( $strStartDate, new DateTimeZone( $strTimezoneOffset ) ) : new DateTime( $strStartDate );
		$objEnd = ( true == valStr( $strTimezoneOffset ) ) ? new DateTime( $strEndDate, new DateTimeZone( $strTimezoneOffset ) ) : new DateTime( $strEndDate );

		if( true == valArr( $arrintCalendarCompanyUserIds ) ) {
			// Calendar events assigned to users
			$arrstrSqls[] = '
				SELECT
					cuc.id AS calendar_id,
					COALESCE( ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last, cu.username ) AS associated_person,
					CASE
						WHEN cev.calendar_event_category_id = ' . CCalendarEventCategory::RESIDENT . ' THEN cl.property_id
						WHEN cev.calendar_event_category_id = ' . CCalendarEventCategory::LEASING . ' THEN a.property_id
						ELSE NULL::INTEGER
					END AS property_id,
					cev.*,
					util_get_translated( \'name\', cet.name, cet.details ) AS event_type,
					cuc.calendar_company_user_id AS attendee_id,
					cuc.calendar_color,
					cet.default_calendar_event_type_id,
					add.name AS amenity_name,
					cl.unit_number_cache AS unit_number,
					( e.details ->>\'display_data\' )::json->>\'num_units\' AS tour_unit_counts
				FROM
					calendar_event_associations cea
					JOIN calendar_events cev ON cev.cid = cea.cid AND cev.id = cea.calendar_event_id AND cev.deleted_on IS NULL
					JOIN calendar_event_types cet ON cet.cid = cev.cid AND cet.id = cev.calendar_event_type_id
					JOIN company_users cu ON cu.cid = cea.cid AND cu.id = cea.company_user_id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					LEFT JOIN events e ON ( cev.cid = e.cid AND cev.event_id = e.id )
					LEFT JOIN company_employees ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
					JOIN company_user_calendars cuc ON cea.cid = cuc.cid AND cea.company_user_id = cuc.calendar_company_user_id
					LEFT JOIN applications a ON cev.application_id = a.id AND cev.cid = a.cid
					LEFT JOIN cached_leases cl ON cev.cid = cl.cid AND cl.id = cev.lease_id
					LEFT JOIN property_add_on_reservations par ON ( par.id = cev.reference_id AND par.cid = cev.cid AND cet.default_calendar_event_type_id = ' . ( int ) CDefaultCalendarEventType::AMENITY_RESERVATION . ' )
					LEFT JOIN rate_associations AS ra ON ( ra.id = par.rate_association_id AND ra.cid = par.cid )
					LEFT JOIN add_ons add ON( add.id = ra.ar_origin_reference_id AND add.cid = ra.cid )
				WHERE
					cea.cid = ' . ( int ) $intCid . '
					AND cea.company_user_id IN( ' . implode( ', ', $arrintCalendarCompanyUserIds ) . ' )
					AND cuc.company_user_id = ' . ( int ) $intCompanyUserId . '
					' . ( false == is_null( $strStartDate ) ? 'AND cev.end_datetime >= \'' . $objStart->format( 'c' ) . '\'' : '' ) . '
					' . ( false == is_null( $strEndDate ) ? 'AND cev.start_datetime <= \'' . $objEnd->format( 'c' ) . '\'' : '' ) . '
					AND cev.calendar_event_type_id IN( ' . implode( ', ', $arrintCalendarEventTypeIds ) . ' )
					AND ( cu.id = ' . ( int ) $intCompanyUserId . '
							OR cu.is_administrator = 1
							OR EXISTS
							(
								SELECT
									cupg.id
								FROM
									company_user_property_groups cupg
									JOIN property_group_associations pga ON cu.cid = pga.cid AND cupg.property_group_id = pga.property_group_id
									JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
									JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = pga.property_id
								WHERE
									cupg.cid = cea.cid
									AND cupg.company_user_id = cea.company_user_id
									AND cu.is_administrator = 0
							)
						)
			';
		}

		if( true == valArr( $arrintCalendarPropertyIds ) ) {
			// Calendar events assigned to properties
			$arrstrSqls[] = '
				SELECT
					cuc.id AS calendar_id,
					p.property_name AS associated_person,
					p.id AS property_id,
					cev.*,
					util_get_translated( \'name\', cet.name, cet.details ) AS event_type,
					cuc.calendar_property_id AS attendee_id,
					cuc.calendar_color,
					cet.default_calendar_event_type_id,
					add.name AS amenity_name,
					cl.unit_number_cache AS unit_number,
					( e.details ->>\'display_data\' )::json->>\'num_units\' AS tour_unit_counts
				FROM
					calendar_event_associations cea
					JOIN calendar_events cev ON cev.cid = cea.cid AND cev.id = cea.calendar_event_id AND cev.deleted_on IS NULL
					JOIN calendar_event_types cet ON cet.cid = cev.cid AND cet.id = cev.calendar_event_type_id
					JOIN properties p ON p.cid = cea.cid AND p.id = cea.property_id
					LEFT JOIN company_user_calendars cuc ON cea.cid = cuc.cid AND cea.property_id = cuc.calendar_property_id
					LEFT JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintCalendarPropertyIds ) . ' ], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = p.id
					LEFT JOIN property_add_on_reservations par ON ( par.id = cev.reference_id AND par.cid = cev.cid AND cet.default_calendar_event_type_id = ' . ( int ) CDefaultCalendarEventType::AMENITY_RESERVATION . ' )
					LEFT JOIN rate_associations AS ra ON ( ra.id = par.rate_association_id AND ra.cid = par.cid )
					LEFT JOIN add_ons add ON( add.id = ra.ar_origin_reference_id AND add.cid = ra.cid )
					LEFT JOIN cached_leases cl ON cev.cid = cl.cid AND cl.id = cev.lease_id
					LEFT JOIN events e ON ( cev.cid = e.cid AND cev.event_id = e.id )
				WHERE
					cea.cid = ' . ( int ) $intCid . '
					AND cea.property_id IN( ' . implode( ', ', $arrintCalendarPropertyIds ) . ' )
					AND cuc.company_user_id = ' . ( int ) $intCompanyUserId . '
					AND lp.property_id IS NOT NULL
					AND cev.calendar_event_type_id IN( ' . implode( ', ', $arrintCalendarEventTypeIds ) . ' )
					' . ( false == is_null( $strStartDate ) ? 'AND cev.end_datetime >= \'' . $objStart->format( 'c' ) . '\'' : '' ) . '
					' . ( false == is_null( $strEndDate ) ? 'AND cev.start_datetime <= \'' . $objEnd->format( 'c' ) . '\'' : '' ) . '
			';

			// Dynamically generate move-ins as all-day events for these properties
			$arrobjPropertyPreferences = rekeyObjects( 'PropertyId', CPropertyPreferences::fetchPropertyPreferencesByKeyByPropertyIdsByCid( 'ENTRATA_CALENDAR_MOVE_IN_DISPLAY', $arrintCalendarPropertyIds, $intCid, $objDatabase ) );
			$strWhereCondition = '';

			foreach( $arrintCalendarPropertyIds as $intCalendarPropertyId ) {

				if( true == valStr( $strWhereCondition ) ) {

					$strWhereCondition .= ' OR ';
				}

				if( true == isset( $arrobjPropertyPreferences[$intCalendarPropertyId] ) ) {

					$strWhereCondition .= ' ( cl.property_id = ' . $arrobjPropertyPreferences[$intCalendarPropertyId]->getPropertyId() . ' AND ( ass.id >= ' . ( int ) $arrobjPropertyPreferences[$intCalendarPropertyId]->getValue() . ' AND ass.id <= ' . ( int ) CApplicationStageStatus::PROSPECT_STAGE4_APPROVED . ' ) AND ass.id IN ( ' . implode( ',', array_keys( CApplicationStageStatus::createService()->getEntrataCalendarMoveInDisplayStageStatuses() ) ) . ' ) ) 
					';
				} else {

					$strWhereCondition .= ' ( cl.property_id = ' . ( int ) $intCalendarPropertyId . ' AND ass.id = ' . ( int ) CApplicationStageStatus::PROSPECT_STAGE4_APPROVED . ' ) 
					';
				}
			}

			$strMoveInSql = '
			SELECT
				cuc.id AS calendar_id,
				cl.property_name AS associated_person,
				cl.property_id AS property_id,
				cev.id,
				cl.cid,
				' . ( int ) $intCompanyUserId . ' AS company_user_id,
				cet.calendar_event_category_id,
				cet.id AS calendar_event_type_id,
				cet.event_type_id,
				NULL::INTEGER AS event_id,
				NULL::INTEGER AS parent_calendar_event_id,
				NULL::INTEGER AS applicant_id,
				NULL::INTEGER AS application_id,
				cl.id AS lease_id,
				lc.id AS lease_customer_id,
				NULL::INTEGER AS reference_id,
				COALESCE( cl.name_first || \' \' || cl.name_last, cl.name_first, cl.name_last ) AS title,
				cl.move_in_date AS start_datetime,
				cl.move_in_date AS end_datetime,
				\'{}\'::json AS schedule_details,
				COALESCE(cev.event_details, ( \'{"lease_customer_id":\' || lc.id || \', "event_all_day":1}\' )::json ) AS event_details,
				NULL::INTEGER AS deleted_by,
				NULL::TIMESTAMPTZ AS deleted_on,
				NULL::INTEGER AS updated_by,
				NULL::TIMESTAMPTZ AS updated_on,
				NULL::INTEGER AS created_by,
				NULL::TIMESTAMPTZ AS created_on,
				util_get_translated( \'name\', cet.name, cet.details ) AS event_type,
				cuc.company_user_id AS attendee_id,
				cuc.calendar_color,
				cet.default_calendar_event_type_id,
				NULL::TEXT AS amentiy_name,
				cl.unit_number_cache AS unit_number,
				NULL::TEXT AS  tour_unit_counts
			FROM
				cached_leases cl
				JOIN lease_customers lc ON lc.cid = cl.cid AND lc.lease_id = cl.id AND lc.customer_id = cl.primary_customer_id
				JOIN calendar_event_types cet ON cet.cid = cl.cid AND cet.default_calendar_event_type_id = ' . ( int ) CDefaultCalendarEventType::MOVE_IN . '
				LEFT JOIN cached_applications ca ON ca.cid = cl.cid AND ca.lease_interval_id = cl.active_lease_interval_id
				LEFT JOIN application_stage_statuses ass ON ca.application_stage_id = ass.application_stage_id AND ca.application_status_id = ass.application_status_id AND ca.lease_interval_type_id = ass.lease_interval_type_id
				LEFT JOIN company_user_calendars cuc ON cl.cid = cuc.cid AND cl.property_id = cuc.calendar_property_id
				LEFT JOIN calendar_events cev ON cev.cid = cl.cid AND cev.lease_id = cl.id AND cev.calendar_event_type_id = cet.id AND cev.deleted_on IS NULL
				LEFT JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintCalendarPropertyIds ) . ' ], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = cl.property_id
			WHERE
				cl.cid = ' . ( int ) $intCid . '
				AND cl.property_id IN( ' . implode( ', ', $arrintCalendarPropertyIds ) . ' )
				AND cuc.company_user_id = ' . ( int ) $intCompanyUserId . '
				AND cl.move_in_date IS NOT NULL
				AND cl.lease_status_type_id IN ( ' . implode( ', ', [ CLeaseStatusType::APPLICANT, CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::PAST ] ) . ' )
				';

			if( true == valStr( $strWhereCondition ) ) {
				$strMoveInSql .= ' AND ( cl.lease_status_type_id IN ( ' . CLeaseStatusType::CURRENT . ',' . CLeaseStatusType::NOTICE . ' ) OR ( cl.lease_status_type_id IN (' . CLeaseStatusType::APPLICANT . ',' . CLeaseStatusType::FUTURE . ',' . CLeaseStatusType::CURRENT . ',' . CLeaseStatusType::NOTICE . ',' . CLeaseStatusType::PAST . ' ) AND cl.is_transferring_in = 1 ) OR ' . $strWhereCondition . ' )';
			}

			$strMoveInSql .= '
					AND lp.property_id IS NOT NULL
					AND cev.id IS NULL
					AND cet.id IN( ' . implode( ', ', $arrintCalendarEventTypeIds ) . ' )
					' . ( false == is_null( $strStartDate ) ? 'AND cl.move_in_date >= \'' . $objStart->format( 'c' ) . '\'' : '' ) . '
					' . ( false == is_null( $strEndDate ) ? 'AND cl.move_in_date <= \'' . $objEnd->format( 'c' ) . '\'' : '' ) . '
				';

			$arrstrSqls[] = $strMoveInSql;

			// Dynamically generate move-outs as all-day events for these properties
			$arrstrSqls[] = '
				SELECT
					cuc.id AS calendar_id,
					cl.property_name AS associated_person,
					cl.property_id AS property_id,
					cev.id,
					cl.cid,
					' . ( int ) $intCompanyUserId . ' AS company_user_id,
					cet.calendar_event_category_id,
					cet.id AS calendar_event_type_id,
					cet.event_type_id,
					NULL::INTEGER AS event_id,
					NULL::INTEGER AS parent_calendar_event_id,
					NULL::INTEGER AS applicant_id,
					NULL::INTEGER AS application_id,
					cl.id AS lease_id,
					lc.id AS lease_customer_id,
					NULL::INTEGER AS reference_id,
					COALESCE( cl.name_first || \' \' || cl.name_last, cl.name_first, cl.name_last ) AS title,
					cl.move_out_date AS start_datetime,
					cl.move_out_date AS end_datetime,
					\'{}\'::json AS schedule_details,
					COALESCE(cev.event_details, ( \'{"lease_customer_id":\' || lc.id || \', "event_all_day":1}\' )::json ) AS event_details,
					NULL::INTEGER AS deleted_by,
					NULL::TIMESTAMPTZ AS deleted_on,
					NULL::INTEGER AS updated_by,
					NULL::TIMESTAMPTZ AS updated_on,
					NULL::INTEGER AS created_by,
					NULL::TIMESTAMPTZ AS created_on,
					util_get_translated( \'name\', cet.name, cet.details ) AS event_type,
					cuc.company_user_id AS attendee_id,
					cuc.calendar_color,
					cet.default_calendar_event_type_id,
					NULL::TEXT AS amenity_name,
					cl.unit_number_cache AS unit_number,
					NULL::TEXT AS  tour_unit_counts
				FROM

					cached_leases cl
					JOIN lease_customers lc ON lc.cid = cl.cid AND lc.lease_id = cl.id AND lc.customer_id = cl.primary_customer_id
					JOIN calendar_event_types cet ON cet.cid = cl.cid AND cet.default_calendar_event_type_id = ' . ( int ) CDefaultCalendarEventType::MOVE_OUT . '
					LEFT JOIN company_user_calendars cuc ON cl.cid = cuc.cid AND cl.property_id = cuc.calendar_property_id
					LEFT JOIN calendar_events cev ON cev.cid = cl.cid AND cev.lease_id = cl.id AND cev.calendar_event_type_id = cet.id AND cev.deleted_on IS NULL
					LEFT JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintCalendarPropertyIds ) . ' ], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = cl.property_id
				WHERE
					cl.cid = ' . ( int ) $intCid . '
					AND cl.property_id IN( ' . implode( ', ', $arrintCalendarPropertyIds ) . ' )
					AND cuc.company_user_id = ' . ( int ) $intCompanyUserId . '
					AND cl.move_out_date IS NOT NULL
					AND lp.property_id IS NOT NULL
					AND cev.id IS NULL
					AND cl.lease_status_type_id IN ( ' . implode( ', ', [ CLeaseStatusType::APPLICANT, CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::PAST ] ) . ' )
					AND cet.id IN( ' . implode( ', ', $arrintCalendarEventTypeIds ) . ' )
					' . ( false == is_null( $strStartDate ) ? 'AND cl.move_out_date >= \'' . $objStart->format( 'c' ) . '\'' : '' ) . '
					' . ( false == is_null( $strEndDate ) ? 'AND cl.move_out_date <= \'' . $objEnd->format( 'c' ) . '\'' : '' ) . '
				';

			// Dynamically generate requested AM all-day events
			$strRequestedDatetime = 'CASE
							WHEN true = vcmr.is_requested_am THEN
								(vcmr.requested_date::DATE || \' 00:01:00\')::TIMESTAMP
							ELSE
								(vcmr.requested_date::DATE || \' 12:00:00\')::TIMESTAMP
							END';
			$arrstrSqls[] = '
				SELECT
					cuc.id AS calendar_id,
					p.property_name AS associated_person,
					vcmr.property_id AS property_id,
					cev.id,
					vcmr.cid,
					' . ( int ) $intCompanyUserId . ' AS company_user_id,
					cet.calendar_event_category_id,
					cet.id AS calendar_event_type_id,
					cet.event_type_id,
					NULL::INTEGER AS event_id,
					NULL::INTEGER AS parent_calendar_event_id,
					NULL::INTEGER AS applicant_id,
					NULL::INTEGER AS application_id,
					NULL::INTEGER AS lease_id,
					NULL::INTEGER AS lease_customer_id,
					vcmr.id AS reference_id,
					\'WO \' || vcmr.id AS title,
					' . $strRequestedDatetime . ' AS start_datetime,
					' . $strRequestedDatetime . ' AS end_datetime,
					\'{}\'::json AS schedule_details,
					COALESCE(cev.event_details, ( \'{"event_all_day":1}\' )::json ) AS event_details,
					NULL::INTEGER AS deleted_by,
					NULL::TIMESTAMPTZ AS deleted_on,
					NULL::INTEGER AS updated_by,
					NULL::TIMESTAMPTZ AS updated_on,
					NULL::INTEGER AS created_by,
					NULL::TIMESTAMPTZ AS created_on,
					util_get_translated( \'name\', cet.name, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS event_type,
					cuc.company_user_id AS attendee_id,
					cuc.calendar_color,
					cet.default_calendar_event_type_id,
					NULL::TEXT AS amenity_name,
					util_get_translated( \'unit_number\', vcmr.unit_number, vcmr.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS unit_number,
					NULL::TEXT AS  tour_unit_counts
				FROM view_maintenance_requests vcmr
				JOIN properties p ON vcmr.cid = p.cid AND vcmr.property_id = p.id
				JOIN calendar_event_types cet ON cet.cid = vcmr.cid AND cet.default_calendar_event_type_id = ' . ( int ) CDefaultCalendarEventType::WORK_ORDER . '
				LEFT JOIN company_user_calendars cuc ON vcmr.cid = cuc.cid AND vcmr.property_id = cuc.calendar_property_id
				LEFT JOIN calendar_events cev ON cev.cid = vcmr.cid AND cev.reference_id = vcmr.id AND cev.calendar_event_type_id = cet.id AND cev.deleted_on IS NULL
				LEFT JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintCalendarPropertyIds ) . ' ], ARRAY[ ' . CPsProduct::ENTRATA . ' ] ) lp ON lp.property_id = vcmr.property_id
				WHERE
					vcmr.cid = ' . ( int ) $intCid . '
					AND vcmr.property_id IN( ' . implode( ', ', $arrintCalendarPropertyIds ) . ' )
					AND vcmr.parent_maintenance_request_id IS NULL
					AND cuc.company_user_id = ' . ( int ) $intCompanyUserId . '
					AND vcmr.is_requested_am IS NOT NULL
					AND vcmr.requested_date IS NOT NULL
					AND vcmr.deleted_on IS NULL
					AND vcmr.deleted_by IS NULL
					AND lp.property_id IS NOT NULL
					AND cev.id IS NULL
					AND cet.id IN( ' . implode( ', ', $arrintCalendarEventTypeIds ) . ' )
					' . ( false == is_null( $strStartDate ) ? 'AND  ' . $strRequestedDatetime . '  >= \'' . $objStart->format( 'c' ) . '\'' : '' ) . '
					' . ( false == is_null( $strEndDate ) ? 'AND  ' . $strRequestedDatetime . '  <= \'' . $objEnd->format( 'c' ) . '\'' : '' ) . '
					AND date_part(\'hour\', ' . $strRequestedDatetime . ' ) >= 0
					AND date_part ( \'hour\', ' . $strRequestedDatetime . ' ) <= 11';

			// Dynamically generate requested PM all-day events
			$arrstrSqls[] = '
				SELECT
					cuc.id AS calendar_id,
					p.property_name AS associated_person,
					vcmr.property_id AS property_id,
					cev.id,
					vcmr.cid,
					' . ( int ) $intCompanyUserId . ' AS company_user_id,
					cet.calendar_event_category_id,
					cet.id AS calendar_event_type_id,
					cet.event_type_id,
					NULL::INTEGER AS event_id,
					NULL::INTEGER AS parent_calendar_event_id,
					NULL::INTEGER AS applicant_id,
					NULL::INTEGER AS application_id,
					NULL::INTEGER AS lease_id,
					NULL::INTEGER AS lease_customer_id,
					vcmr.id AS reference_id,
					\'WO \' || vcmr.id AS title,
					' . $strRequestedDatetime . ' AS start_datetime,
					' . $strRequestedDatetime . ' AS end_datetime,
					\'{}\'::json AS schedule_details,
					COALESCE(cev.event_details, ( \'{"event_all_day":1}\' )::json ) AS event_details,
					NULL::INTEGER AS deleted_by,
					NULL::TIMESTAMPTZ AS deleted_on,
					NULL::INTEGER AS updated_by,
					NULL::TIMESTAMPTZ AS updated_on,
					NULL::INTEGER AS created_by,
					NULL::TIMESTAMPTZ AS created_on,
					util_get_translated( \'name\', cet.name, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS event_type,
					cuc.company_user_id AS attendee_id,
					cuc.calendar_color,
					cet.default_calendar_event_type_id,
					NULL::TEXT AS amenity_name,
					util_get_translated( \'unit_number\', vcmr.unit_number, vcmr.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS unit_number,
					NULL::TEXT AS  tour_unit_counts
				FROM view_maintenance_requests vcmr
				JOIN properties p ON vcmr.cid = p.cid AND vcmr.property_id = p.id
				JOIN calendar_event_types cet ON cet.cid = vcmr.cid AND cet.default_calendar_event_type_id = ' . ( int ) CDefaultCalendarEventType::WORK_ORDER . '
				LEFT JOIN company_user_calendars cuc ON vcmr.cid = cuc.cid AND vcmr.property_id = cuc.calendar_property_id
				LEFT JOIN calendar_events cev ON cev.cid = vcmr.cid AND cev.reference_id = vcmr.id AND cev.calendar_event_type_id = cet.id AND cev.deleted_on IS NULL
				LEFT JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintCalendarPropertyIds ) . ' ], ARRAY[ ' . CPsProduct::ENTRATA . ' ] ) lp ON lp.property_id = vcmr.property_id
				WHERE
					vcmr.cid = ' . ( int ) $intCid . '
					AND vcmr.property_id IN( ' . implode( ', ', $arrintCalendarPropertyIds ) . ' )
					AND vcmr.parent_maintenance_request_id IS NULL
					AND cuc.company_user_id = ' . ( int ) $intCompanyUserId . '
					AND vcmr.is_requested_am IS NOT NULL
					AND vcmr.requested_date IS NOT NULL
					AND vcmr.deleted_on IS NULL
					AND vcmr.deleted_by IS NULL
					AND lp.property_id IS NOT NULL
					AND cev.id IS NULL
					AND cet.id IN( ' . implode( ', ', $arrintCalendarEventTypeIds ) . ' )
					' . ( false == is_null( $strStartDate ) ? 'AND  ' . $strRequestedDatetime . '  >= \'' . $objStart->format( 'c' ) . '\'' : '' ) . '
					' . ( false == is_null( $strEndDate ) ? 'AND  ' . $strRequestedDatetime . '  <= \'' . $objEnd->format( 'c' ) . '\'' : '' ) . '
					AND date_part(\'hour\', ' . $strRequestedDatetime . ' ) > 11
					AND date_part ( \'hour\', ' . $strRequestedDatetime . ' ) <= 23';
		}

		$strSql = implode( PHP_EOL . 'UNION ALL' . PHP_EOL, $arrstrSqls );

		if( '' == $strSql ) {
			return false;
		}

		$strSql .= ' ORDER BY title';

		return self::fetchCalendarEvents( $strSql, $objDatabase, false ) ?: [];

	}

	public static function fetchCalendarEventsByCompanyUserIdByCompanyUserIdsByPropertyIdsByCid( $intCompanyUserId, $intCid, $objDatabase, $arrintCompanyUserIds, $arrintPropertyIds, $strStartDate = NULL, $strEndDate = NULL, $arrintDefaultCalendarEventTypeIds = NULL, $arrintCalendarEventTypeIds = NULL, $strTimezoneOffset = NULL ) {
		if( true == valStr( $strStartDate ) ) {
			$objStartDateTime = ( true == valStr( $strTimezoneOffset ) ) ? new \Psi\Libraries\UtilDates\CDateTime( $strStartDate, new DateTimeZone( $strTimezoneOffset ) ) : new \Psi\Libraries\UtilDates\CDateTime( $strStartDate );
		}
		if( true == valStr( $strEndDate ) ) {
			$objEndDateTime = ( true == valStr( $strTimezoneOffset ) ) ?  new \Psi\Libraries\UtilDates\CDateTime( $strEndDate, new DateTimeZone( $strTimezoneOffset ) ) : new \Psi\Libraries\UtilDates\CDateTime( $strEndDate );
		}

		$strSql = 'SELECT
						cev.*,
						CASE
							WHEN cea.company_user_id = cu.id THEN cea.company_user_id
							WHEN cea.property_id = p.id THEN cea.property_id
							ELSE NULL
						END AS attendee_id,
						CASE
							WHEN cu.id = cea.company_user_id THEN cem.name_first || \' \' || cem.name_last
							WHEN cea.property_id = p.id THEN p.property_name
							ELSE NULL
						END AS associated_person,
						CASE
							WHEN a.id = cev.applicant_id THEN a.name_first || \' \' || a.name_last
							WHEN c.id = lc.customer_id THEN c.name_first || \' \' || c.name_last
							ELSE cev.title
						END AS title,
						cpn.phone_number,
						a.email_address,
						cl.property_id AS property_id,
						util_get_translated( \'name\', cet.name, cet.details ) AS event_type,
						cuc.calendar_color,
						cet.default_calendar_event_type_id,
						add.name AS amenity_name,
						cl.unit_number_cache,
						( e.details ->>\'display_data\' )::json->>\'num_units\' AS tour_unit_counts,
						a.customer_id
					FROM
						calendar_events cev
					JOIN calendar_event_associations cea ON ( cea.cid = cev.cid AND cea.calendar_event_id = cev.id )
					LEFT JOIN company_users cu ON ( cu.id = cea.company_user_id AND cu.cid = cea.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					LEFT JOIN company_employees cem ON ( cu.company_employee_id = cem.id AND cu.cid = cem.cid )
					LEFT JOIN company_user_calendars cuc ON cea.cid = cuc.cid AND cuc.company_user_id =' . ( int ) $intCompanyUserId . ' AND ( cea.property_id = cuc.calendar_property_id OR cea.company_user_id = cuc.calendar_company_user_id )
					JOIN calendar_event_types cet ON ( cet.id = cev.calendar_event_type_id AND cet.cid = cev.cid )
					LEFT JOIN properties p ON cea.cid = p.cid AND ( cea.property_id = p.id )
					LEFT JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = p.id
					LEFT JOIN lease_customers lc ON lc.cid = cev.cid AND lc.id = cev.lease_customer_id
					LEFT JOIN customers c ON c.cid = cev.cid AND c.id = lc.customer_id
					LEFT JOIN applicants a ON a.cid = cev.cid AND a.id = cev.applicant_id
					LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_on IS NULL AND cpn.deleted_by IS NULL AND cpn.phone_number_type_id in ( ' . implode( ',', \CPhoneNumberType::$c_arrintCustomerPhoneNumberTypes ) . ' ) )
					LEFT JOIN cached_leases cl ON cl.cid = cev.cid AND cl.id = cev.lease_id
					LEFT JOIN property_add_on_reservations par ON ( par.id = cev.reference_id AND par.cid = cev.cid AND cet.default_calendar_event_type_id = ' . ( int ) CDefaultCalendarEventType::AMENITY_RESERVATION . ' )
					LEFT JOIN rate_associations AS ra ON ( ra.id = par.rate_association_id AND ra.cid = par.cid )
					LEFT JOIN add_ons add ON( add.id = ra.ar_origin_reference_id AND add.cid = ra.cid )
					LEFT JOIN events e ON ( cev.cid = e.cid AND cev.event_id = e.id )';

					if( valArr( $arrintDefaultCalendarEventTypeIds ) ) {
						$strSql .= ' JOIN default_calendar_event_types dcet ON dcet.id = cet.default_calendar_event_type_id ';
					}

					$strSql .= '
					WHERE cev.deleted_on IS NULL
					AND cev.cid = ' . ( int ) $intCid . '
					' . ( true == valObj( $objStartDateTime, \Psi\Libraries\UtilDates\CDateTime::class ) ? 'AND cev.end_datetime >= \'' . $objStartDateTime->format( 'c' ) . '\'' : '' ) . '
					' . ( true == valObj( $objEndDateTime, \Psi\Libraries\UtilDates\CDateTime::class ) ? 'AND cev.start_datetime <= \'' . $objEndDateTime->format( 'c' ) . '\'' : '' );

					if( valArr( $arrintDefaultCalendarEventTypeIds ) ) {
						$strSql .= 'AND dcet.id IN (' . implode( ',', $arrintDefaultCalendarEventTypeIds ) . ') ';
					}

					if( true == valArr( $arrintCalendarEventTypeIds ) ) {
						$strSql .= 'AND cet.id IN (' . implode( ',', $arrintCalendarEventTypeIds ) . ') ';
					}

					$strSql .= ' AND ( lp.property_id IS NOT NULL
						OR ( cu.id = ' . ( int ) $intCompanyUserId . '
							OR cu.is_administrator = 1
							OR EXISTS
							(
								SELECT
									cupg.id
								FROM
									company_user_property_groups cupg
									JOIN property_group_associations pga ON cu.cid = pga.cid AND cupg.property_group_id = pga.property_group_id
									JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
									JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = pga.property_id
								WHERE
									cupg.cid = cea.cid
									AND cupg.company_user_id = cea.company_user_id
									AND cu.is_administrator = 0
							)
						)
					)';

		if( valArr( $arrintPropertyIds ) && valArr( $arrintCompanyUserIds ) ) {
			$strSql .= ' AND ( cea.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' ) OR cea.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )';
		} elseif( valArr( $arrintPropertyIds ) && !valArr( $arrintCompanyUserIds ) ) {
			$strSql .= ' AND cea.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		} elseif( valArr( $arrintCompanyUserIds ) && !valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND cea.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )';
		} else {
			return false;
		}

		return parent::fetchObjects( $strSql, 'CCalendarEvent', $objDatabase, false ) ?: [];
	}

	public static function fetchEventCountByStartDatetimeByEndDatetimeByCategoryIdsByPropertyIdByCid( $strStartDatetime, $strEndDatetime, $arrintCategoryIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCategoryIds ) ) return NULL;
		if( false == valStr( $strStartDatetime ) || false == valStr( $strEndDatetime ) ) return NULL;

		$strSql = '
			SELECT
				count( ce.id ) as count
			FROM
				calendar_events AS ce
				JOIN calendar_event_associations AS cea ON (
					cea.calendar_event_id = ce.id
					AND cea.cid = ce.cid
					AND cea.property_id = ' . ( int ) $intPropertyId . ' )
			WHERE
				ce.cid = ' . ( int ) $intCid . '
				AND ce.calendar_event_category_id IN( ' . implode( ', ', $arrintCategoryIds ) . " )
				AND ( ( ce.event_details->>'event_all_day' ) IS NULL OR ce.event_details->>'event_all_day' = '0' )
				AND ( ( ce.event_details->>'event_recurring' ) IS NULL OR ce.event_details->>'event_recurring' = '0' )
				AND ce.end_datetime > '" . date( 'c', strtotime( $strStartDatetime ) ) . "'
				AND ce.start_datetime < '" . date( 'c', strtotime( $strEndDatetime ) ) . "'";

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return ( true == isset ( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
	}

	public static function fetchMaxSimultaneousEventCountByStartDatetimeByEndDatetimeByCategoryIdsByPropertyIdByCid( $strStartDatetime, $strEndDatetime, $arrintCategoryIds, $intPropertyId, $intCid, $objDatabase, $intExcludeEventId = NULL ) {
		if( false == valArr( $arrintCategoryIds ) ) return NULL;
		if( false == valStr( $strStartDatetime ) || false == valStr( $strEndDatetime ) ) return NULL;

		$intCount	= 0;
		$strSql		= self::buildMaxSimultaneousEventCountSqlByStartDatetimeByEndDatetimeByCategoryIdsByPropertyIdByCid( $strStartDatetime, $strEndDatetime, $arrintCategoryIds, $intPropertyId, $intCid, $intExcludeEventId );

		if( true == valStr( $strSql ) ) {
			$arrintResponse = fetchData( $strSql, $objDatabase );
			if( true == isset ( $arrintResponse[0]['count'] ) ) $intCount = $arrintResponse[0]['count'];
		}

		return $intCount;
	}

	public static function fetchCalendarEventsByLeaseCustomerIdByCalendarEventTypeIdByCid( $intLeaseCustomerId, $intCalendarEventTypeId, $intCid, $objDatabase ) {
		if( false == valStr( $intLeaseCustomerId ) || false == valStr( $intCalendarEventTypeId ) || false == valStr( $intCid ) ) return NULL;

		$strSql = '
			SELECT
				cev.*
			FROM
				calendar_events cev
			WHERE
				cev.cid = ' . ( int ) $intCid . '
				AND cev.lease_customer_id = ' . ( int ) $intLeaseCustomerId . '
				AND cev.calendar_event_type_id = ' . ( int ) $intCalendarEventTypeId . '
				AND cev.deleted_by IS NULL';

		return parent::fetchObject( $strSql, 'CCalendarEvent', $objDatabase, false ) ?: [];
	}

	public static function fetchCalendarEventByCalendarEventTypeByReferenceIdByCid( $intCalendarEventType, $intReferenceId, $intCid, $objDatabase ) {
		if( false == valStr( $intCalendarEventType ) || false == valStr( $intReferenceId ) ) return NULL;

		$strSql = '
			SELECT
				cev.*
			FROM
				calendar_events cev
			WHERE
				cev.cid = ' . ( int ) $intCid . '
				AND cev.calendar_event_type_id = ' . ( int ) $intCalendarEventType . '
				AND cev.reference_id = ' . ( int ) $intReferenceId . '
				AND cev.deleted_by IS NULL';

		return parent::fetchObject( $strSql, 'CCalendarEvent', $objDatabase, false ) ?: [];
	}

	public static function fetchCalendarEventsByCalendarEventTypeByReferenceIdsByCid( $intCalendarEventType, $arrintReferenceIds, $intCid, $objDatabase ) {
		if( false == valStr( $intCalendarEventType ) || false == valArr( $arrintReferenceIds ) ) return NULL;

		$strSql = '
			SELECT
				cev.*
			FROM
				calendar_events cev
			WHERE
				cev.cid = ' . ( int ) $intCid . '
				AND cev.calendar_event_type_id = ' . ( int ) $intCalendarEventType . '
				AND cev.reference_id IN ( ' . implode( ',', $arrintReferenceIds ) . ' )
				AND cev.deleted_by IS NULL';

		return parent::fetchObjects( $strSql, 'CCalendarEvent', $objDatabase, false ) ?: [];
	}

	public static function fetchAllActiveCalendarEvents( $intCid, $intPropertyId, $objDatabase, $intCategoryId = 0, $arrstrExcludeCategories = [] ) {

		$strJoin = '';
		if( 0 != $intCategoryId && true == valArr( $arrstrExcludeCategories ) ) {
			$strJoin = 'JOIN calendar_event_types cet ON cet.id = ce.calendar_event_type_id AND cet.name NOT IN ( \'' . implode( '\',\'', $arrstrExcludeCategories ) . '\' )';
		}

		$strSql = 'SELECT
					ce.* 
				FROM 
					calendar_events ce 
				JOIN 
					calendar_event_associations cea ON ce.id = cea.calendar_event_id AND ce.cid = cea.cid
						' . $strJoin . '
				WHERE 
					ce.cid = ' . ( int ) $intCid . ' AND 
					ce.end_datetime >= NOW() AND 
					cea.property_id = ' . ( int ) $intPropertyId . ' AND
					ce.deleted_by IS NULL AND 
					ce.deleted_on IS NULL';
		if( 0 != $intCategoryId ) {
			$strSql .= sprintf( ' AND ce.calendar_event_category_id = %d', $intCategoryId );
		}

		return self::fetchCalendarEvents( $strSql, $objDatabase );
	}

	public static function fetchCalendarEventsForPushNotification( $objDatabase ) {

		$strSql = 'SELECT
						ce.*,
						CASE
								WHEN a.id = ce.applicant_id THEN a.name_first || \' \' || a.name_last
								ELSE ce.title
						END AS title,
						cpn.phone_number,
						a.username AS email_address,
						util_get_translated( \'name\', cet.name, cet.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS event_type,
						cea.property_id
						FROM
							calendar_events ce
							JOIN calendar_event_associations cea ON ( cea.cid = ce.cid AND cea.calendar_event_id = ce.id )
							JOIN calendar_event_types cet ON ( cet.id = ce.calendar_event_type_id AND cet.cid = ce.cid )
							JOIN applicants a ON ( a.cid = ce.cid AND a.id = ce.applicant_id )
							LEFT JOIN customer_phone_numbers cpn ON ( a.cid = cpn.cid AND a.customer_id = cpn.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_on IS NULL AND cpn.deleted_by IS NULL AND cpn.phone_number_type_id in ( ' . implode( ',', \CPhoneNumberType::$c_arrintCustomerPhoneNumberTypes ) . ' ) )
						WHERE
							ce.deleted_on IS NULL
							AND cea.property_id IS NOT NULL
							AND ce.calendar_event_category_id = ' . CCalendarEventCategory::LEASING . '
							AND ce.start_datetime BETWEEN (now() + INTERVAL \'14 minutes\') AND (now() + INTERVAL \'17 minutes\')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function  fetchAllActiveLeasingAppointmentCalendarEvents( $intLeaseIntervalId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseIntervalId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = '
				SELECT
				  ce.*
				FROM
				  calendar_events ce
				  JOIN events AS e ON (e.cid = ce.cid AND e.id = ce.event_id AND e.event_type_id = ce.event_type_id AND e.event_type_id = ' . ( int ) CEventType::SCHEDULED_APPOINTMENT . ' AND e.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' AND e.scheduled_datetime <= NOW() AND
				    e.is_deleted = false)
				WHERE
				  e.cid = ' . ( int ) $intCid . '
				  AND e.property_id = ' . ( int ) $intPropertyId . '
				  AND ce.deleted_by IS NULL
				  AND ce.deleted_on IS NULL';

		return self::fetchCalendarEvents( $strSql, $objDatabase );
	}

	public static function fetchPotentialCalendarEventsByCidByCompanyUserBySearchTerm( $intCid, $objCompanyUser, $strSearchTerm, $objDatabase, $boolIsSkipMoveInSchedulerEvent = true ) {

		if( false == valId( $intCid ) || false == valObj( $objCompanyUser, 'CCompanyUser' ) || false == valStr( $strSearchTerm ) ) {
			return [];
		}

		$strSkipCalendarEventTypeSql = '';

		if( true == $boolIsSkipMoveInSchedulerEvent ) {
			$strSkipCalendarEventTypeSql = ' AND cet.default_calendar_event_type_id <> ' . CDefaultCalendarEventType::MOVE_IN_SCHEDULER;
		}

		$strSql = '
			WITH company_user_properties AS (
				SELECT DISTINCT 
					lp.property_id,
					lp.cid
				FROM load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ], true ) lp';

		if( true != $objCompanyUser->getIsAdministrator() ) {
			$strSql .= '
				JOIN property_group_associations pga ON pga . cid = lp . cid AND lp . property_id = pga . property_id
				JOIN company_user_property_groups cupg ON pga . cid = cupg . cid AND pga . property_group_id = cupg . property_group_id AND cupg . company_user_id = ' . ( int ) $objCompanyUser->getId();
		}

		$strSql .= '
			), property_calendar_events AS (
				SELECT DISTINCT ON (calendar_event_id)
					ce.cid,
					ce.id as calendar_event_id,
					ce.title as calendar_event_title,
					ce.event_details->>\'event_description\' as calendar_event_description,
					add.name as amenity_name,
					cl.name_first || \' \' || cl.name_last as customer_name,
					cl.unit_number_cache,
					ce.start_datetime AS calendar_event_start,
					ce.calendar_event_category_id
				FROM calendar_events ce
				JOIN calendar_event_associations cea ON cea.cid = ce.cid AND cea.calendar_event_id = ce.id AND cea.property_id IS NOT NULL
				JOIN company_user_properties cup ON cup.cid = cea.cid AND cea.property_id = cup.property_id
				JOIN calendar_event_types cet ON cet.cid = ce.cid AND cet.id = ce.calendar_event_type_id ' . $strSkipCalendarEventTypeSql . '
				LEFT JOIN property_add_on_reservations par ON ( par.id = ce.reference_id AND par.cid = ce.cid AND cet.default_calendar_event_type_id = 12 )
				LEFT JOIN rate_associations AS ra ON ( ra.id = par.rate_association_id AND ra.cid = par.cid )
				LEFT JOIN add_ons add ON( add.id = ra.ar_origin_reference_id AND add.cid = ra.cid )
				LEFT JOIN cached_leases cl ON cl.cid = ce.cid AND ce.lease_id = cl.id
				WHERE ce.cid = ' . ( int ) $intCid . '
			), user_calendar_events AS (
				SELECT DISTINCT ON (calendar_event_id)
					ce.cid,
					ce.id as calendar_event_id,
					ce.title as calendar_event_title,
					ce.event_details->>\'event_description\' as calendar_event_description,
					add.name as amenity_name,
					cl.name_first || \' \' || cl.name_last as customer_name,
					cl.unit_number_cache,
					ce.start_datetime AS calendar_event_start,
					ce.calendar_event_category_id
				FROM calendar_events ce
				JOIN calendar_event_associations cea ON cea.cid = ce.cid AND cea.calendar_event_id = ce.id AND cea.company_user_id IS NOT NULL
				JOIN company_users cu ON cu.cid = ce.cid AND cu.id = cea.company_user_id
				JOIN company_user_property_groups cupg ON cupg.cid = cu.cid AND cupg.company_user_id = cu.id
				JOIN property_group_associations pga ON pga.cid = cupg.cid AND pga.property_group_id = cupg.property_group_id
				JOIN company_user_properties cup ON cup.cid = cea.cid AND pga.property_id = cup.property_id
				JOIN calendar_event_types cet ON cet.cid = ce.cid AND cet.id = ce.calendar_event_type_id' . $strSkipCalendarEventTypeSql . '
				LEFT JOIN property_add_on_reservations par ON ( par.id = ce.reference_id AND par.cid = ce.cid AND cet.default_calendar_event_type_id = 12 )
				LEFT JOIN rate_associations AS ra ON ( ra.id = par.rate_association_id AND ra.cid = par.cid )
				LEFT JOIN add_ons add ON( add.id = ra.ar_origin_reference_id AND add.cid = ra.cid )
				LEFT JOIN cached_leases cl ON cl.cid = ce.cid AND ce.lease_id = cl.id
				WHERE ce.cid = ' . ( int ) $intCid . '
			)
			SELECT
			 	c.calendar_event_id,
			 	CASE 
			 	WHEN c.amenity_name IS NOT NULL then c.amenity_name
			 	ELSE
				c.calendar_event_title
				END AS title,
				to_date( c.calendar_event_start::TEXT, \'MM/DD/YYYY\'::TEXT ) AS start_date_label,
				to_char( to_date( c.calendar_event_start::TEXT, \'MM/DD/YYYY\'::TEXT ), \'YYYY-MM-DD\' ) AS start_date,
				c.calendar_event_category_id
			FROM (
				SELECT * FROM property_calendar_events pce
				UNION
				SELECT * FROM user_calendar_events uce
			) c
			WHERE ( concat( c.calendar_event_title, c.calendar_event_description, c.amenity_name, c.customer_name, c.unit_number_cache ) ILIKE \'%' . $strSearchTerm . '%\' )
		';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFutureCalendarEventsByCidByApplicationId( $intApplicationId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) || false == valId( $intCid ) ) return [];

		$strSql = '
				SELECT
					ce.*
				FROM
					calendar_events ce
				WHERE
					ce.cid = ' . ( int ) $intCid . '
					AND ce.application_id = ' . ( int ) $intApplicationId . '
					AND ce.start_datetime > NOW()';

		return self::fetchCalendarEvents( $strSql, $objDatabase );
	}

	public static function fetchCalendarEventByLeaseIdByLeaseCustomerIdByCid( $intLeaseId, $intLeaseCustomerId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseCustomerId ) || false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						start_datetime,
						end_datetime,
						lease_customer_id,
						lease_id,
						cid
					FROM
						calendar_events 
					WHERE
						cid = ' . ( int ) $intCid . '
						AND start_datetime > now()
						AND lease_customer_id =' . ( int ) $intLeaseCustomerId . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND deleted_by IS NULL 
						AND deleted_on IS NULL 
						AND cid = ' . ( int ) $intCid . '
						ORDER BY start_datetime LIMIT 1';

		return self::fetchCalendarEvent( $strSql, $objDatabase );

	}

	public static function fetchCalendarEventsByPropertyEventsByCid( $arrintPropertyEventIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyEventIds ) ) return NULL;
		$strSql = '
			SELECT
				cev.*
			FROM
				calendar_events cev
				JOIN property_events pe ON pe.cid = cev.cid AND pe.id = cev.reference_id
			WHERE
				cev.cid = ' . ( int ) $intCid . '
				AND pe.id IN ( ' . implode( ',', $arrintPropertyEventIds ) . ' )
				AND cev.deleted_by IS NULL
				AND cev.deleted_on IS NULL';
		return parent::fetchObjects( $strSql, 'CCalendarEvent', $objDatabase, false ) ?: [];
	}

}
?>