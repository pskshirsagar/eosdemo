<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationChanges
 * Do not add any new functions to this class.
 */

class CApplicantApplicationChanges extends CBaseApplicantApplicationChanges {

	public static function fetchSimpleApplicantApplicationChangesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						aac.*,
						a.name_first || \' \' || a.name_last as applicant_name,
						util_get_translated( \'name\', aact.name, aact.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS applicant_application_change_type,
						CASE
							WHEN cu.id < 100 THEN cu.username
							WHEN COALESCE( ce.id, NULL ) IS NULL THEN \'entrata (\' || cu.username || \')\'
							ELSE \'entrata (\' || ce.name_first || \' \' || ce.name_last || \')\'
						END AS username
					FROM
						applicant_application_changes aac
						JOIN applicants a ON ( aac.applicant_id = a.id AND aac.cid = a.cid )
						JOIN applicant_application_change_types aact ON ( aac.applicant_application_change_type_id = aact.id )
						JOIN company_users as cu ON ( aac.created_by = cu.id AND cu.cid = ' . ( int ) $intCid . ' AND aac.cid = cu.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees as ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						aac.application_id = ' . ( int ) $intApplicationId . '
						AND a.cid = ' . ( int ) $intCid . '
						AND aac.event_id IS NULL
					ORDER BY
						created_on DESC
					LIMIT 100';

		return self::fetchApplicantApplicationChanges( $strSql, $objDatabase );
	}

	public static function fetchAllApplicantApplicationChangesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $boolAddEventCondition = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;
		$strEventCondition = ( true == $boolAddEventCondition ) ? 'AND event_id IS NULL' : NULL;
		$strSql = 'SELECT
						*
					FROM
						applicant_application_changes
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id IN( ' . implode( ',', $arrintApplicationIds ) . ' )
					' . $strEventCondition;

		return self::fetchApplicantApplicationChanges( $strSql, $objDatabase );
	}

	public static function fetchApplicantApplicationChangesByEventIdByApplicationIdByCid( $intEventId, $intApplicationId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) || false == valId( $intCid ) ) return false;

		$strEventCondition = ( false == is_null( $intEventId ) ) ? ' AND aac.event_id = ' . ( int ) $intEventId : ' AND aac.event_id IS NULL';

		$strSql = 'SELECT
						aac.*,
						a.name_first || \' \' || a.name_last as applicant_name,
						CASE 
                        	WHEN aact.id = ' . CApplicantApplicationChangeType::SSN . ' THEN COALESCE( util_get_system_translated( \'abbreviation\', tit.abbreviation, tit.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ), \'' . CTaxIdType::createService()->getAbbreviations()[CTaxIdType::SSN] . '\' )
                            ELSE util_get_system_translated( \'name\', aact.name, aact.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) 
                        END AS applicant_application_change_type,
						CASE
							WHEN COALESCE( ce.id, NULL ) IS NULL THEN cu.username
							ELSE ce.name_first || \' \' || ce.name_last
						END AS username
					FROM
						applicant_application_changes aac
						LEFT JOIN view_applicants a ON ( aac.applicant_id = a.id AND aac.cid = a.cid )
						JOIN applicant_application_change_types aact ON ( aac.applicant_application_change_type_id = aact.id )
						JOIN company_users as cu ON ( aac.created_by = cu.id AND aac.cid = cu.cid  AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees as ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
						LEFT JOIN tax_id_types tit ON ( a.tax_id_type_id = tit.id )
					WHERE
						aac.application_id = ' . ( int ) $intApplicationId . '
						AND aac.cid = ' . ( int ) $intCid . $strEventCondition . '
					ORDER BY
						created_on DESC';

		return self::fetchApplicantApplicationChanges( $strSql, $objDatabase );
	}

	public static function fetchApplicantApplicationChangeForMoveInDateByApplicationIdByChangeTypeIdByCid( $intApplicationId, $intChangeTypeId, $intCid, $objDatabase ) {
		if( true == is_null( $intApplicationId ) ) return NULL;

		$strSql = 'SELECT
						new_value
					FROM
						applicant_application_changes
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND applicant_application_change_type_id = ' . ( int ) $intChangeTypeId . '
					ORDER BY
						created_on ASC
					LIMIT 1';

		return self::fetchApplicantApplicationChange( $strSql, $objDatabase );
	}

}
?>