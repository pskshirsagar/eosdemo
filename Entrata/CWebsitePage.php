<?php

class CWebsitePage extends CBaseWebsitePage {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteId() {

		if( false == valId( $this->getWebsiteId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Website ID', __( 'Website ID is required' ) ) );
			return false;
		}

		return true;
	}

	public function valWebsiteDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsitePageTypeId() {

		if( false == valId( $this->getWebsitePageTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Website page type ID', __( 'Website page type ID is required' ) ) );
			return false;
		}

		return true;
	}

	public function valPageName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPageTitle() {

		if( false == valStr( $this->getPageTitle() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Page title', __( 'Page title is required' ) ) );
			return false;
		}

		return true;
	}

	public function valKeywords() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {

		if( false == is_bool( $this->getIsPublished() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Page published', __( 'Page published is required' ) ) );
			return false;
		}

		return true;
	}

	public function valVersion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRoute( $objDatabase ) {

		$objWebsitePage = \Psi\Eos\Entrata\CWebsitePages::createService()->fetchWebsitePagesByRouteByWebsiteIdByCid( $this->getRoute(), $this->getWebsiteId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objWebsitePage, 'CWebsitePage' ) && $objWebsitePage->getId() != $this->getId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Route exists', __( 'The route added is already in use for other custom page' ) ) );
			return false;
		}

		return true;
	}

	public function valRedirectUrl() {

		if( true == valStr( $this->getRedirectUrl() ) && false == CValidation::checkUrl( $this->getRedirectUrl(), false, true ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'link_uri', CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
			return false;
		}

		return true;
	}

	public function valTarget() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCanonicalUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPageTitle();
				$boolIsValid &= $this->valIsPublished();
				$boolIsValid &= $this->valWebsiteId();
				$boolIsValid &= $this->valWebsitePageTypeId();
				$boolIsValid &= $this->valRedirectUrl();
				$boolIsValid &= $this->valRoute( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * @param $strVersion
	 * @param $objDatabase
	 * @return array|bool
	 */
	public function fetchWebsitePageComponentsByVersion( $strVersion, $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsitePageComponents::createService()->fetchWebsitePageComponentsByVersionByWebsitePageIdByCid( $this->getId(), $this->getCid(), $strVersion, $objDatabase );
	}

	/**
	 * @param $objDatabase
	 * @return bool
	 */
	public function deleteWebsitePageComponentsByVersion( $strVersion, $objDatabase ) {

		$this->setDatabase( $objDatabase );
		$strSql = 'DELETE FROM website_page_components WHERE website_page_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . ' AND version = \'' . $strVersion . '\'';

		return $this->executeSql( $strSql, $this, $objDatabase );

	}

	public function deleteWebsitePage( $intCompanyUserId, $objDatabase ) {
		$this->setDeletedBy( $intCompanyUserId );
		$this->setDeletedOn( 'NOW()' );
		return ( true == $this->update( $intCompanyUserId, $objDatabase ) );
	}

}
?>