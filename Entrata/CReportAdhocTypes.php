<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportAdhocTypes
 * Do not add any new functions to this class.
 */

class CReportAdhocTypes extends CBaseReportAdhocTypes {

	public static function fetchReportAdhocTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CReportAdhocType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReportAdhocType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CReportAdhocType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReportAdhocTypeByReportTypeAction( $strSelectReportTypeAction, $objDatabase ) {
		$strSql	= '	SELECT
						*
					FROM
						report_adhoc_types
					WHERE
						action = \'' . $strSelectReportTypeAction . '\'
					';
		return self::fetchReportAdhocType( $strSql, $objDatabase );
	}

    public static function fetchAllReportAdhocTypes( $objDatabase ) {
    	return self::fetchReportAdhocTypes( 'SELECT * FROM report_adhoc_types', $objDatabase );
    }
}
?>