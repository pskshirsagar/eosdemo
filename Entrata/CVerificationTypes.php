<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CVerificationTypes
 * Do not add any new functions to this class.
 */

class CVerificationTypes extends CBaseVerificationTypes {

	public static function fetchVerificationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CVerificationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchVerificationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CVerificationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedVerificationTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM verification_types WHERE is_published = TRUE';

		return self::fetchVerificationTypes( $strSql, $objDatabase );
	}

}
?>