<?php

class CCompanyGroup extends CBaseCompanyGroup {

	const LEARNING_CENTER_ADMIN_SYSTEM_CODE		= 'LCADMIN';
	const LEARNING_CENTER_TRAINER_SYSTEM_CODE = 'LCTRNR';

	protected $m_arrobjPermissions;
	protected $m_arrobjProperties;
	protected $m_arrobjUsers;
	protected $m_intAssociatedUsersCount;
	protected $m_boolCompanyGroupPermission;

	/**
	 * Get Functions
	 */

	public function getCompanyUsers( $objDataset ) {
		if( is_null( $this->m_arrobjUsers ) ) {
			$this->m_arrobjUsers =& \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersByCompanyGroupIdByCid( $this->getId(), $this->getCid(), $objDataset );
		}
		return $this->m_arrobjUsers;
	}

	public function getCompanyGroupPermissions( $objDataset ) {
		if( is_null( $this->m_arrobjPermissions ) ) {
			$this->m_arrobjPermissions =& \Psi\Eos\Entrata\CCompanyGroupPermissions::createService()->fetchCompanyGroupPermissionsByCompanyGroupIdByCid( $this->getId(), $this->getCid(), $objDataset );
		}
		return $this->m_arrobjPermissions;
	}

	public function getAssociatedUsersCount() {
		return $this->m_intAssociatedUsersCount;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getCompanyGroupPermission() {
		return $this->m_boolCompanyGroupPermission;
	}

	public function getCompanyGroupsFields() {
		return [
			'company_group_to_copy'     => __( 'Existing Group' ),
			'name'                      => __( 'Name' ),
			'description'               => __( 'Description' ),
			'is_active_directory_group' => __( 'Is Active Directory Group' )
		];
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrstrValues['company_user_id'] ) ) {
			$this->setCompanyUserId( $arrstrValues['company_user_id'] );
		}

		if( isset( $arrstrValues['associated_users_count'] ) && $boolDirectSet ) {
			$this->m_intAssociatedUsersCount = trim( $arrstrValues['associated_users_count'] );
		} elseif( isset( $arrstrValues['associated_users_count'] ) ) {
			$this->setAssociatedUsersCount( $arrstrValues['associated_users_count'] );
		}

		if( true == isset( $arrstrValues['company_group_permission'] ) ) {
			$this->setCompanyGroupPermission( $arrstrValues['company_group_permission'] );
		}
		return;
	}

	public function setName( $strName, $strLocalCode = NULL ) {
		$strName = preg_replace( '[^a-zA-Z0-9_-\s]', '', trim( $strName ) );
		parent::setName( $strName, $strLocalCode );
	}

	public function setAssociatedUsersCount( $intAssociatedUsersCount ) {
		$this->m_intAssociatedUsersCount = $intAssociatedUsersCount;
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
	}

	public function setCompanyGroupPermission( $boolCompanyGroupPermission ) {
		$this->m_boolCompanyGroupPermission = $boolCompanyGroupPermission;
	}

	/**
	 * Create Functions
	 */

	public function createCompanyGroupPermission( $intModuleId ) {

		$objCompanyGroupPermission = new CCompanyGroupPermission();
		$objCompanyGroupPermission->setCid( $this->getCid() );
		$objCompanyGroupPermission->setCompanyGroupId( $this->getId() );
		$objCompanyGroupPermission->setModuleId( $intModuleId );

		return $objCompanyGroupPermission;
	}

	public function createCompanyGroupPreference() {

		$objCompanyGroupPreference = new CCompanyGroupPreference();
		$objCompanyGroupPreference->setCid( $this->getCid() );
		$objCompanyGroupPreference->setCompanyGroupId( $this->getId() );
		return $objCompanyGroupPreference;
	}

	public function createCompanyGroupPropertyGroup() {

		$objCompanyGroupPropertyGroup = new CCompanyGroupPropertyGroup();
		$objCompanyGroupPropertyGroup->setCid( $this->getCid() );
		$objCompanyGroupPropertyGroup->setCompanyGroupId( $this->getId() );

		return $objCompanyGroupPropertyGroup;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCompanyGroupPreferences( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroupPreferences::createService()->fetchOrderedGroupPreferencesByCompanyGroupIdByCid( $this->getId(), $this->getCid(), $objDatabase, $strOrderBy = 'id' );
	}

	public function fetchCompanyGroupPreferencesByKeys( $arrstrKeys, $objDatabase, $boolIsCheckValue = true ) {
		return \Psi\Eos\Entrata\CCompanyGroupPreferences::createService()->fetchCompanyGroupPreferencesByCompanyGroupIdByKeysByCid( $this->getId(), $arrstrKeys, $this->getCid(), $objDatabase, $boolIsCheckValue );
	}

	public function fetchCompanyGroupPropertyGroups( $objDatabase ) {
		return CCompanyGroupPropertyGroups::fetchCompanyGroupPropertyGroupsByCompanyGroupIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchEnabledCompanyGroupPropertyGroups( $objDatabase, $boolShowDisabledData = false ) {
		return CCompanyGroupPropertyGroups::fetchEnabledCompanyGroupPropertyGroupsByCompanyGroupIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolShowDisabledData );
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Company group id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIntegrationDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strName ) || 0 == strlen( trim( $this->m_strName ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Group name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateName( $objDatabase ) {

		$boolIsValid = true;

		if( true == isset( $this->m_strName ) || ( 3 > strlen( $this->m_strName ) ) ) {

			if( true == isset ( $objDatabase ) ) {

				$intConflictingCompanyGroupCount = \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupCountByGroupNameByNonCompetingCompanyUserIdByCid( $this->m_strName, $this->m_intId, $this->m_intCid, $objDatabase );

				if( 0 < $intConflictingCompanyGroupCount ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'The group name \'{%s, group_name}\' is already in use. Please choose another.', [ 'group_name' => $this->m_strName ] ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( false == isset( $this->m_strDescription ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
		// }

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( false == isset( $this->m_intOrderNum ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
		// }

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDuplicateName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCompanyGroupBeforeCompanyGroupDelete( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valCompanyGroupBeforeCompanyGroupDelete( $objDatabase ) {

		$boolIsValid = true;

		$intCompanyGroupPropertyGroups	= CCompanyGroupPropertyGroups::fetchCompanyGroupPropertyGroupCount( ' WHERE company_group_id = ' . $this->sqlId() . ' AND cid = ' . $this->sqlCid(), $objDatabase );
		$intChoreAssignments			= CChoreAssignments::fetchChoreAssignmentCount( ' WHERE company_group_id = ' . $this->sqlId() . ' AND cid = ' . $this->sqlCid(), $objDatabase );
		$intChoreTypeAssignments		= CChoreTypeAssignments::fetchChoreTypeAssignmentCount( ' WHERE company_group_id = ' . $this->sqlId() . ' AND cid = ' . $this->sqlCid(), $objDatabase );
		$intSharedFilters				= CSharedFilters::fetchSharedFilterCount( ' WHERE company_group_id = ' . $this->sqlId() . ' AND cid = ' . $this->sqlCid(), $objDatabase );

		// List all shared filters associated to the company group.
		$strSharedFilters = '';
		if( 0 < $intSharedFilters ) {
			$arrstrSharedFilterDetails = ( array ) CSharedFilters::fetchSharedFilterDetailsByCompanyGroupIdByCid( $this->sqlId(), $this->sqlCid(), $objDatabase );
			foreach( $arrstrSharedFilterDetails as $intKey => $arrstrSharedFilterDetail ) {
				$strSharedFilters .= '<br/>' . ( ( int ) $intKey + 1 ) . ' . ' . $arrstrSharedFilterDetail['report_name'] . ' - ' . $arrstrSharedFilterDetail['filter_name'];
			}
		}

		$arrstrTableNames = [];
		( 0 != $intCompanyGroupPropertyGroups ) ? $arrstrTableNames[] = __( 'Property Groups ' ) : '';
		( 0 != $intChoreAssignments ) ? $arrstrTableNames[] = __( 'Chore Assignments ' ) : '';
		( 0 != $intChoreTypeAssignments ) ? $arrstrTableNames[] = __( 'Chore Type Assignments ' ) : '';
		( 0 != $intSharedFilters ) ? $arrstrTableNames[] = __( 'following shared filters (Please remove the group from these filters and then try removing the group again) {%s, shared_filters} ', [ 'shared_filters' => $strSharedFilters ] ) : '';

		if( true == valArr( $arrstrTableNames ) ) {
			$strTableNames = implode( ' , ', $arrstrTableNames );
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_group_id', __( 'Group \'{%s, group_name}\' cannot be deleted. It is associated to {%s, table_name}.', [ 'group_name' => $this->getName(), 'table_name' => $strTableNames ] ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * Database Functions
	 */

	public function delete( $intUserId, $objClient, $objDatabase = NULL ) {

		$arrobjCompanyGroupUsers 		= $objClient->fetchCompanyUserGroupsByCompanyGroupId( $this->getId(), $objDatabase );
		$arrobjCompanyGroupPermissions 	= $objClient->fetchCompanyGroupPermissionsByCompanyGroupId( $this->getId(), $objDatabase );
		$arrobjCompanyGroupAssessments 	= $objClient->fetchCompanyGroupAssessmentsByCompanyGroupId( $this->getId(), $objDatabase );
		$arrobjCompanyGroupPreferences 	= $objClient->fetchCompanyGroupPreferencesByCompanyGroupId( $this->getId(), $objDatabase );

		if( true == valArr( $arrobjCompanyGroupUsers ) ) {
			foreach( $arrobjCompanyGroupUsers as $objCompanyGroupUser ) {
				if( false == $objCompanyGroupUser->delete( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objCompanyGroupUser->getErrorMsgs() );
					return false;
				}
			}
		}

		if( true == valArr( $arrobjCompanyGroupPermissions ) ) {
			foreach( $arrobjCompanyGroupPermissions as $objCompanyGroupPermission ) {
				if( false == $objCompanyGroupPermission->delete( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objCompanyGroupPermission->getErrorMsgs() );
					return false;
				}
			}
		}

		if( true == valArr( $arrobjCompanyGroupAssessments ) ) {
			foreach( $arrobjCompanyGroupAssessments as $objCompanyGroupAssessment ) {
				if( false == $objCompanyGroupAssessment->delete( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objCompanyGroupAssessment->getErrorMsgs() );
					return false;
				}
			}
		}

		if( true == valArr( $arrobjCompanyGroupPreferences ) ) {
			foreach( $arrobjCompanyGroupPreferences as $objCompanyGroupPreference ) {
				if( false == $objCompanyGroupPreference->delete( $intUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objCompanyGroupPreference->getErrorMsgs() );
					return false;
				}
			}
		}

		return parent::delete( $intUserId, $objDatabase );
	}

}

?>