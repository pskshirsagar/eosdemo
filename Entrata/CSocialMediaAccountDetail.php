<?php

class CSocialMediaAccountDetail extends CBaseSocialMediaAccountDetail {

	protected $m_strUserAccountKey;
	protected $m_strUserTokenKey;
	protected $m_strUserProfileId;
	protected $m_boolIsActive;
	protected $m_jsonAccountDetails;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSocialPostTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPageId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUserAccessToken() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTokenSecret() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function createSocialMediaActivity() {
    	$objSocialMediaActivity = new CSocialMediaActivity();
    	$objSocialMediaActivity->setPropertyId( $this->getPropertyId() );
    	$objSocialMediaActivity->setCid( $this->getCid() );
    	$objSocialMediaActivity->setSocialMediaAccountDetailId( $this->getId() );
    	return $objSocialMediaActivity;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	/*
	 * Get Functions
	 */

	public function getUserAndBusinessProfileName() {
		switch( $this->getSocialPostTypeId() ) {
			case CSocialPostType::FACEBOOK:
				$strBusinessProfileTypeName = 'Page';
				$strAccountName = $this->getUserProfileName() . '|' . $this->getBusinessProfileName();
				break;

			case CSocialPostType::TWITTER:
				$strBusinessProfileTypeName = 'Account';
				$strAccountName = $this->getUserProfileName();
				break;

			case CSocialPostType::GOOGLE_MYBUSINESS:
				$strBusinessProfileTypeName = 'Account&Location';
				$strAccountName = $this->getUserProfileName() . '|' . $this->getBusinessProfileName();
				break;

			default:
				$strBusinessProfileTypeName = '';
				$strAccountName = '';
		}

		return [ $strBusinessProfileTypeName, $strAccountName ];
	}

	public function getUserAccountKey() {
		return $this->m_strUserAccountKey;
	}

	public function getUserTokenKey() {
		return $this->m_strUserTokenKey;
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function getAccountDetails() {
		return $this->m_jsonAccountDetails;
	}

	public function getUserProfileId() {
		return $this->m_strUserProfileId;
	}

	/**
	 *  Set Function
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['user_account_key'] ) ) {
			$this->setUserAccountKey( $arrmixValues['user_account_key'] );
		}

		if( isset( $arrmixValues['user_token_key'] ) ) {
			$this->setUserTokenKey( $arrmixValues['user_token_key'] );
		}

		if( isset( $arrmixValues['account_details'] ) ) {
			$this->setAccountDetails( $arrmixValues['account_details'] );
		}

		if( isset( $arrmixValues['is_active'] ) ) {
			$this->setIsActive( $arrmixValues['is_active'] );
		}

		if( isset( $arrmixValues['user_profile_id'] ) ) {
			$this->setUserProfileId( $arrmixValues['user_profile_id'] );
		}
	}

	public function setUserAccountKey( $strUserAccountKey ) {
		$this->m_strUserAccountKey = $strUserAccountKey;
	}

	public function setUserTokenKey( $strUserTokenKey ) {
		$this->m_strUserTokenKey = $strUserTokenKey;
	}

	public function setIsActive( $boolIsActive ) {
		$this->m_boolIsActive = CStrings::strToBool( $boolIsActive );
	}

	public function setAccountDetails( $jsonAccountDetails ) {
		return $this->m_jsonAccountDetails = $jsonAccountDetails;
	}

	public function setUserProfileId( $strUserProfileId ) {
		$this->m_strUserProfileId = $strUserProfileId;
	}

}
?>