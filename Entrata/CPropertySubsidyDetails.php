<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySubsidyDetails
 * Do not add any new functions to this class.
 */

class CPropertySubsidyDetails extends CBasePropertySubsidyDetails {

	public static function fetchHmfaCodeByPropertyIdByCId( $intPropertyId,  $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						hmfa_code
					FROM
						property_subsidy_details
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND hmfa_code IS NOT NULL';

		return self::fetchColumn( $strSql, 'hmfa_code', $objDatabase );
	}

	public static function fetchhudImaxIdsByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						hud_project_name,
						hud_project_imax_id_encrypted,
						hud_recipient_imax_id_encrypted,
						hud_project_imax_password_encrypted,
						hud_project_number,
						subsidy_tracs_version_id
					FROM
						property_subsidy_details
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		$arrstrPropertySubsidyDetails = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrstrPropertySubsidyDetails ) ) ? $arrstrPropertySubsidyDetails[0] : [];
	}

	public static function fetchPropertySubsidyDetailByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertySubsidyDetail( sprintf( 'SELECT * FROM property_subsidy_details WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertySubsidyDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_subsidy_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND hmfa_code IS NOT NULL';

		return self::fetchPropertySubsidyDetails( $strSql, $objDatabase );
	}

	public static function fetchPropertyRecertificationDataByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = '	SELECT
						psd.hud_subsidy_recertification_type_id,
						psd.tax_credit_subsidy_recertification_type_id,
						psd.tax_credit_annual_recertification_schedule,
						srd.month,
						srd.day
					FROM
						property_subsidy_details psd
						JOIN leases l ON ( psd.cid = l.cid AND psd.property_id = l.property_id )
						JOIN property_units pu ON ( l.cid = pu.cid AND l.property_id = pu.property_id AND l.property_unit_id = pu.id )
						LEFT JOIN subsidy_recertification_details srd ON ( pu.cid = srd.cid AND pu.property_id = srd.property_id AND ( pu.property_building_id = srd.property_building_id OR srd.property_building_id IS NULL ) )
					WHERE
						psd.cid = ' . ( int ) $intCid . '
						AND psd.property_id = ' . ( int ) $intPropertyId . '
						AND l.id = ' . ( int ) $intLeaseId . '
						AND srd.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertySubsidyDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = '	SELECT
						p.cid,
						p.id AS property_id,
						p.property_name,
						psd.tax_credit_is_hold_harmless,
						psd.tax_credit_use_hera_limits,
						pst.subsidy_income_limit_version_id,
						sila.metro_area_name
					FROM
						properties p
						JOIN property_subsidy_details psd ON ( psd.cid = p.cid AND psd.property_id = p.id )
						JOIN property_subsidy_types pst ON ( pst.cid = p.cid AND pst.property_id = p.id AND pst.subsidy_type_id IN (' . CSubsidyType::TAX_CREDIT . ',' . CSubsidyType::HUD . ') AND pst.deleted_on IS NULL )
						LEFT JOIN subsidy_income_limit_areas sila ON ( sila.subsidy_income_limit_version_id = pst.subsidy_income_limit_version_id AND psd.hmfa_code = sila.hmfa_code )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyTracsVersionIdsByPropertyIdsByCId( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						property_id,
						subsidy_tracs_version_id
					FROM
						property_subsidy_details
					WHERE
						property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		$arrstrSubsidyTracsVersionIds = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrstrSubsidyTracsVersionIds ) ) {
			$arrobjRekeyedPropertyDetails = [];

			foreach( $arrstrSubsidyTracsVersionIds as $arrmixPropertyDetails ) {
				$arrobjRekeyedPropertyDetails[$arrmixPropertyDetails['property_id']] = $arrmixPropertyDetails['subsidy_tracs_version_id'];
			}

			$arrstrSubsidyTracsVersionIds = $arrobjRekeyedPropertyDetails;
		}
		return $arrstrSubsidyTracsVersionIds;
	}

	public static function fetchPropertyTracsVersionIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
                        subsidy_tracs_version_id
                   FROM
                        property_subsidy_details
                   WHERE
                        property_id = ' . ( int ) $intPropertyId . '
                        AND cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'subsidy_tracs_version_id', $objDatabase );
	}

	public static function fetchFileTypeSystemCodeByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						COALESCE( MAX( ft.system_code), \'TX_TIC\' ) AS file_type_system_code
					FROM
						property_subsidy_details psd
						JOIN file_types ft ON ( psd.cid = ft.cid AND psd.file_type_id = ft.id )
					WHERE
						psd.property_id = ' . ( int ) $intPropertyId . '
						AND psd.cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'file_type_system_code', $objDatabase );
	}

	public static function fetchSimplePropertySubsidyDetailsByTracsVersionIdByCid( $intTracsVersionId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
					    p.cid,
					    p.id AS property_id,
					    p.property_name
					FROM
					    properties p
					    JOIN property_subsidy_details psd ON ( psd.cid = p.cid AND psd.property_id =  p.id )   
					WHERE
					    p.cid =' . ( int ) $intCid . '
						AND psd.subsidy_tracs_version_id = ' . ( int ) $intTracsVersionId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchhudProjectNumberByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT									
						hud_project_number
						
					FROM
						property_subsidy_details
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'hud_project_number', $objDatabase );
	}

	public static function fetchPropertySubsidyTaxCreditTransferDetailsByIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT	
						property_id,	
						tax_credit_same_building_transfer,
						tax_credit_building_to_building_transfer,
						tax_credit_program_to_program_transfer
					FROM
						property_subsidy_details
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		$arrmixPropertySubsidyDetails = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'property_id', $arrmixPropertySubsidyDetails );
	}

	public static function fetchCustomPropertySubsidyDetailByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $strFieldName ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valStr( $strFieldName ) ) return NULL;

		$strSql = 'SELECT
						' . $strFieldName . '
					FROM
						property_subsidy_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchColumn( $strSql, $strFieldName, $objDatabase );
	}

}

?>