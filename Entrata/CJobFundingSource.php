<?php

class CJobFundingSource extends CBaseJobFundingSource {
	protected $m_strCompanyName;
	protected $m_intApPayeeTypeId;
	protected $m_fltTotalDrawnAmount;

	// Validate functions

	public function valApPayeeId() {
		$boolIsValid = true;

		if( false == is_null( $this->getFundingAmount() ) && false == valId( $this->getApPayeeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', __( 'Funding source is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFundingAmount() {
		$boolIsValid = true;

		if( false == is_null( $this->getFundingAmount() ) && 0 >= $this->getFundingAmount() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'funding_amount', __( 'Funding amount should be greater than 0.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valFundingAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 *  Get Function
	 */

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getApPayeeTypeId() {
		return $this->m_intApPayeeTypeId;
	}

	public function getTotalDrawnAmount() {
		return $this->m_fltTotalDrawnAmount;
	}

	/*
	 * Set Function
	 */

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setApPayeeTypeId( $intApPayeeTypeId ) {
		$this->m_intApPayeeTypeId = $intApPayeeTypeId;
	}

	public function setTotalDrawnAmount( $fltTotalDrawnAmount ) {
		$this->m_fltTotalDrawnAmount = CStrings::strToFloatDef( $fltTotalDrawnAmount, NULL, false, 2 );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['ap_payee_type_id'] ) ) $this->setApPayeeTypeId( $arrmixValues['ap_payee_type_id'] );
		if( true == isset( $arrmixValues['total_drawn_amount'] ) ) $this->setTotalDrawnAmount( $arrmixValues['total_drawn_amount'] );
	}

}
?>