<?php

class COccupation extends CBaseOccupation {

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL ) {

		$boolIsValid = true;
		if( false == isset( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Occupation Name is required.' ) ) );

		} elseif( 3 > \Psi\CStringService::singleton()->strlen( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Occupation Name must be at least {%d, 0} characters in length.', [ 3 ] ) ) );
		}

		if( true == isset( $objDatabase ) ) {
			$objOccupation = COccupations::fetchDuplicateOccupationByCidByName( $this->getCid(), $this->getName(), $this->getId(), $objDatabase );
			if( true == is_numeric( $this->getId() ) ) {
				$objOriginalOccupation = COccupations::fetchOccupationByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
			}
		}

		if( ( false == isset ( $objOriginalOccupation ) || $this->getName() != $objOriginalOccupation->getName() ) && true == valObj( $objOccupation, 'COccupation' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Occupation Name already in use.' ) ) );
		}

		return $boolIsValid;
	}

 	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$arrobjPropertyOccupations = CPropertyOccupations::fetchPropertyOccupationsByOccupationIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjPropertyOccupations ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_occupation', __( 'Could not delete this occupation, it is being associated with Properties.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>