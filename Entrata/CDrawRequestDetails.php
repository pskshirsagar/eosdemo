<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDrawRequestDetails
 * Do not add any new functions to this class.
 */

class CDrawRequestDetails extends CBaseDrawRequestDetails {

	public static function fetchDrawSummaryDetailsByDrawRequestIdByJobIdByCid( $intDrawRequestId, $intJobId, $intCid, $objDatabase, $boolByFundingSourceSetting = false ) {

		if( false == valId( $intCid ) || false == valId( $intJobId ) || false == valId( $intDrawRequestId ) ) return NULL;

		$strCondition      = ( true == $boolByFundingSourceSetting ) ? ' AND dr.ap_payee_id = tdr.ap_payee_id ' : '';
		$strWhereCondition = ( true == $boolByFundingSourceSetting ) ? ' WHERE ( sub_query3.this_request <> 0 OR sub_query4.previous_request <> 0 ) ' : '';

		$strSql = ' WITH this_draw_request AS (
						SELECT
							dr.id as draw_id,
							dr.ap_payee_id,
							dr.draw_date,
							ac.id AS ap_code_id,
							ac.name AS job_cost_code,
							drd.draw_amount as drawn_amount,
							CASE
								WHEN ( ad.id IS NOT NULL AND ah.retention_released_on IS NULL ) THEN ad.retention_amount
								ELSE 0
							END AS retention_amount
						FROM draw_requests dr
							JOIN draw_request_details drd ON ( dr.cid = drd.cid AND dr.id = drd.draw_request_id )
							LEFT JOIN gl_details gd ON ( drd.gl_detail_id = gd.id AND drd.cid = gd.cid )
							LEFT JOIN ap_details ad ON ( drd.ap_detail_id = ad.id AND drd.cid = ad.cid )
							LEFT JOIN ap_headers ah ON ( ad.ap_header_id = ah.id AND ad.cid = ah.cid AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' AND ah.is_posted = true )
							JOIN job_phases jp ON ( drd.cid = jp.cid AND drd.job_phase_id = jp.id)
							JOIN ap_codes ac ON ( ( ad.ap_code_id = ac.id AND ac.cid = ad.cid ) OR ( gd.ap_code_id = ac.id AND ac.cid = gd.cid ) )
						WHERE
							jp.cid = ' . ( int ) $intCid . '
							AND jp.job_id = ' . ( int ) $intJobId . '
							AND dr.id = ' . ( int ) $intDrawRequestId . '
							AND drd.rejected_on IS NULL
					),
					previous_draw_requests AS (
						SELECT 
							dr.id as draw_id,
							dr.draw_date,
							ac.id AS ap_code_id,
							ac.name AS job_cost_code,
							drd.draw_amount as drawn_amount
						FROM draw_requests dr
							JOIN draw_request_details drd ON ( dr.cid = drd.cid AND dr.id = drd.draw_request_id )
							LEFT JOIN gl_details gd ON ( drd.gl_detail_id = gd.id AND drd.cid = gd.cid )
							LEFT JOIN ap_details ad ON ( drd.ap_detail_id = ad.id AND drd.cid = ad.cid )
							JOIN job_phases jp ON ( drd.cid = jp.cid AND drd.job_phase_id = jp.id )
							JOIN ap_codes ac ON ( ( ad.ap_code_id = ac.id AND ac.cid = ad.cid ) OR ( gd.ap_code_id = ac.id AND ac.cid = gd.cid ) )
							JOIN (
								    SELECT
                                        dr1.id,
                                        dr1.ap_payee_id,
                                        dr1.draw_date
									 FROM
                                        draw_requests dr1 
                                    WHERE
                                        dr1.cid = ' . ( int ) $intCid . ' 
                                        AND dr1.id = ' . ( int ) $intDrawRequestId . '
								 ) tdr ON ( dr.draw_date <= tdr.draw_date AND dr.id <> tdr.id ' . $strCondition . ' )
						WHERE
							jp.cid = ' . ( int ) $intCid . '
							AND jp.job_id = ' . ( int ) $intJobId . '
							AND drd.rejected_on IS NULL
					),
					current_job_budget AS (
						SELECT 
							ad.ap_code_id,
							ac.name AS job_cost_code,
							SUM(ad.transaction_amount) AS current_budget_amount
						FROM 
							ap_details ad
							JOIN ap_headers ah ON ( ah.id = ad.ap_header_id and ah.cid = ad.cid )
							JOIN job_phases jp ON ( ah.cid = jp.cid and ah.job_phase_id = jp.id )
							JOIN ap_codes ac ON ( ad.ap_code_id = ac.id and ac.cid = ad.cid )
						WHERE 
							jp.cid = ' . ( int ) $intCid . ' AND
							jp.job_id = ' . ( int ) $intJobId . ' AND
							( ( ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ',' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) AND
							ah.approved_on IS NOT NULL ) OR
							ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' )
						GROUP BY 
							ad.ap_code_id,
							ac.name
					),
					draw_summary_sub_query AS (
						SELECT 
							CASE 
	                           WHEN sub_query1.ap_code_id IS NOT NULL THEN sub_query1.ap_code_id 
	                           ELSE sub_query2.ap_code_id 
	                         END AS ap_code_id,
	                        CASE 
	                           WHEN sub_query1.job_cost_code IS NOT NULL THEN sub_query1.job_cost_code
	                           ELSE sub_query2.job_cost_code 
	                        END AS job_cost_code,
							COALESCE( sub_query1.current_budget_amount, 0 ) AS current_budget_amount,
	                        COALESCE( sub_query2.this_request, 0 ) AS this_request,
	                        COALESCE( sub_query2.retention_from_this_request, 0 ) AS retention_from_this_request
						FROM 
							current_job_budget AS sub_query1
							FULL OUTER JOIN
							(
								SELECT
									DISTINCT ap_code_id,
									job_cost_code,
									SUM( drawn_amount ) OVER ( PARTITION BY ap_code_id ) AS this_request,
									SUM( retention_amount ) OVER( PARTITION BY ap_code_id ) AS retention_from_this_request
								FROM this_draw_request
							) sub_query2 ON ( sub_query2.ap_code_id = sub_query1.ap_code_id )
					)
					SELECT
						CASE 
                           WHEN sub_query3.ap_code_id IS NOT NULL THEN sub_query3.ap_code_id 
                            ELSE sub_query4.ap_code_id 
                         END AS ap_code_id,
                        CASE 
                           WHEN sub_query3.job_cost_code IS NOT NULL THEN sub_query3.job_cost_code
                           ELSE sub_query4.job_cost_code 
                        END AS job_cost_code,
						COALESCE( sub_query3.current_budget_amount, 0 ) AS current_budget_amount,
                        COALESCE( sub_query4.previous_request, 0 ) AS previous_request,
                        COALESCE( sub_query3.this_request, 0 ) AS this_request,
                        COALESCE( sub_query3.retention_from_this_request, 0 ) AS retention_from_this_request
                    FROM
                        draw_summary_sub_query AS sub_query3
						FULL OUTER JOIN
						(
							SELECT
								DISTINCT ap_code_id,
								job_cost_code,
								SUM( drawn_amount ) OVER ( PARTITION BY ap_code_id ) AS previous_request
							FROM previous_draw_requests
						) sub_query4 ON ( sub_query4.ap_code_id = sub_query3.ap_code_id )' . $strWhereCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestDetailsByJobIdByCid( $intJobId, $intCid, $objDatabase, $arrmixInvoiceJeListingFilter = NULL, $intDrawRequestId = NULL ) {

		if( false == valId( $intJobId ) ) {
			return NULL;
		}

		$strSubSql = '';
		$strCondition = '';
		$strWhereCondition = '';
		$strInvoiceCondition = '';
		$strJECondition = '';
		$strInvoiceRetentionCondition = '';
		$strWhenCondition = '';
		$strLateralSelectFields = '';
		$strLateralGroupBy = '';
		$strDrawRequestId   = ' drd.draw_request_id,';
		$strSqlEditDRD = '';
		$strReversalCondition = '';

		if( true == valArr( $arrmixInvoiceJeListingFilter ) ) {
			if( true == valArr( $arrmixInvoiceJeListingFilter['job_cost_code_ids'] ) ) {
				$strInvoiceCondition          .= ' AND ad.ap_header_id IN ( SELECT 
																				ad1.ap_header_id 
																			FROM 
																				ap_details ad1
																				JOIN ap_headers ah1 ON ( ad1.cid = ah1.cid AND ah1.id = ad1.ap_header_id AND ah1.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' AND ah1.is_posted = true )
																			WHERE 
																				ad1.cid = ' . ( int ) $intCid . '  
																				AND ad1.reversal_ap_detail_id IS NULL 
																				AND ad1.deleted_on IS NULL 
																				AND ad1.ap_code_id IN( ' . sqlIntImplode( $arrmixInvoiceJeListingFilter['job_cost_code_ids'] ) . ' ) 
																		)';
				$strInvoiceRetentionCondition = $strInvoiceCondition;
				$strJECondition               .= ' AND gd.ap_code_id IN( ' . sqlIntImplode( $arrmixInvoiceJeListingFilter['job_cost_code_ids'] ) . ' )';
			}

			if( true == valStr( $arrmixInvoiceJeListingFilter['post_from_date'] ) && true == CValidation::validateDate( $arrmixInvoiceJeListingFilter['post_from_date'] ) ) {
				$strInvoiceCondition .= ' AND ah.post_date >= \'' . $arrmixInvoiceJeListingFilter['post_from_date'] . '\'::DATE';
				$strJECondition      .= ' AND gh.post_date >= \'' . $arrmixInvoiceJeListingFilter['post_from_date'] . '\'::DATE';
			}

			if( true == valStr( $arrmixInvoiceJeListingFilter['post_to_date'] ) && true == CValidation::validateDate( $arrmixInvoiceJeListingFilter['post_to_date'] ) ) {
				$strInvoiceCondition .= ' AND ah.post_date <= \'' . $arrmixInvoiceJeListingFilter['post_to_date'] . '\'::DATE';
				$strJECondition      .= ' AND gh.post_date <= \'' . $arrmixInvoiceJeListingFilter['post_to_date'] . '\'::DATE';
			}
		}

		$strLateralSelectFields   = ' DISTINCT( drd.ap_detail_id ) AS ap_detail_id, drd.rejected_on, drd.draw_request_id as draw_request_id, count(drd.draw_request_id) OVER (PARTITION By ad.id) as rank , ';

		if( true == valId( $intDrawRequestId ) ) {
			$strCondition               = ' AND ( drd.draw_request_id = ' . ( int ) $intDrawRequestId . ' OR drd.draw_request_id IS NULL ) ';
			$strWhereCondition          = ' AND ( drd.drawn_amount IS NULL OR ( ad.transaction_amount <> drd.drawn_amount AND ah.retention_released_on IS NOT NULL AND drd.rank <> 2 ) OR drd.draw_request_id = ' . ( int ) $intDrawRequestId . ' ) ';
			$strLateralWhereCondition   = ' OR drd.draw_request_id IS NULL ';
			$strWhenCondition           = 'WHEN drd.draw_request_id = ' . ( int ) $intDrawRequestId . ' THEN sum( drd.drawn_amount ) OVER (PARTITION BY ah.id)';
			$strLateralGroupBy        = ' GROUP BY drd.rejected_on, drd.draw_request_id, drd.ap_detail_id, ad.id';
			$strReversalCondition     = ' OR ( drd.draw_request_id = ' . ( int ) $intDrawRequestId . ' )';
		} else {
			$strCondition       = ' AND drd.draw_request_id IS NULL ';
			$strWhereCondition  = ' AND drd.rejected_on IS NULL AND ( drd.drawn_amount IS NULL OR (ad.transaction_amount <> drd.drawn_amount AND ah.retention_released_on IS NOT NULL ) )';
			$strLateralGroupBy        = ' GROUP BY drd.rejected_on, drd.draw_request_id, drd.ap_detail_id, drd.draw_amount, ad.id';
		}

		$strSubSql = '
					UNION
					( SELECT
						gd.id as detail_id,
						drd.draw_request_id,
						drd.rejected_on,
						gd.gl_header_id as header_id,
						gh.post_date,
						NULL AS due_date,
						gh.header_number::TEXT,
						NULL as header_sub_type_id,
						NULL AS ap_payee_name,
						apc.name as ap_contract_name,
						CASE
							WHEN fa.gl_header_id IS NOT NULL THEN 1
							ELSE 0
						END AS is_attachment,
						NULL AS is_lein_weiver,
						fa.file_id,
						NULL AS ap_detail_transaction_amount,
						gd.amount AS total_amount,
						NULL AS is_retention,
						NULL AS ap_detail_retention_amount,
						NULL AS total_retention_amount,
						NULL AS retention_amount,
						NULL AS retention_released_on
					FROM
						job_phases jp
						JOIN gl_details gd ON ( jp.cid = gd.cid AND gd.job_phase_id = jp.id AND gd.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' )
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' )
						LEFT JOIN draw_request_details  drd ON ( gd.cid = drd.cid AND drd.gl_detail_id = gd.id )
						LEFT JOIN draw_requests  dr ON ( dr.cid = drd.cid AND dr.id = drd.draw_request_id )
						LEFT JOIN ap_contracts apc ON ( apc.cid = gd.cid AND apc.id = gd.ap_contract_id AND apc.ap_contract_status_id <> ' . CApContractStatus::CANCELLED . ' )
						LEFT JOIN LATERAL
							(
								SELECT
									fa.gl_header_id,
									fa.file_id
								FROM
									file_associations fa
								WHERE
									gh.cid = fa.cid
									AND gh.id = fa.gl_header_id
									AND fa.deleted_by IS NULL
								ORDER BY
									fa.id DESC
								LIMIT 1
							) fa ON true
					WHERE
						jp.job_id = ' . ( int ) $intJobId . '
						AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . '
						AND gh.gl_header_status_type_id = ' . CGlHeaderStatusType::POSTED . '
						AND gh.gl_header_status_type_id <> ' . CGlHeaderStatusType::DELETED . '
						AND gh.is_template IS FALSE
						AND drd.rejected_on IS NULL
						AND gd.cid = ' . ( int ) $intCid . $strCondition . $strJECondition . ' )';

		if( true == valId( $intDrawRequestId ) ) {

			$strSqlEditDRD = '	SELECT 
									NULL::INTEGER as detail_id,
									NULL AS draw_request_id,
									drd.rejected_on,
									ah.id as header_id,
									ah.post_date,
									ah.due_date,
									ah.header_number,
									ah.ap_header_sub_type_id as header_sub_type_id,
									CASE
										WHEN ah.lease_customer_id IS NULL THEN (COALESCE(ap.company_name, \'\') || \' \' || COALESCE(apl.location_name, \'\'))
										ELSE func_format_refund_customer_names(ah.header_memo)
									END AS ap_payee_name,
									apc.contract_name as ap_contract_name,
									NULL AS is_attachment,
									NULL AS is_lein_weiver,
									NULL AS file_id,
									sum(ad.transaction_amount) OVER(PARTITION BY ah.id) AS ap_detail_transaction_amount,
									CASE
										WHEN drd.draw_request_id = ' . ( int ) $intDrawRequestId . ' AND drd.drawn_amount > 0 THEN sum(ad.transaction_amount) over(PARTITION BY ah.id) - sum(drd.drawn_amount) OVER(PARTITION BY ah.id)
									END AS total_amount,
									1 AS is_retention,
									CASE
										WHEN ah.retention_released_on IS NULL THEN ad.retention_amount
										ELSE 0
									END AS ap_detail_retention_amount,
									CASE
										WHEN ah.retention_released_on IS NULL THEN sum(ad.retention_amount) OVER(PARTITION BY ah.id)
										ELSE 0
									END AS total_retention_amount,
									sum( ad.retention_amount ) OVER(PARTITION BY ad.ap_header_id) AS retention_amount,
									ah.retention_released_on
								FROM ap_details ad
									JOIN ap_headers ah ON ( ad.cid = ah.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' AND ah.is_posted = true )
									JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ah.cid = ap.cid )
									JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
									JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id )
									LEFT JOIN LATERAL (
										SELECT 
											drd.draw_request_id as draw_request_id,
											drd.rejected_on,
											drd.ap_detail_id as ap_detail_id,
											count(drd.draw_request_id) OVER(PARTITION By ad.id) as rank,
											sum(drd.draw_amount) OVER (PARTITION BY drd.ap_detail_id) as drawn_amount
										FROM 
											draw_request_details drd
										WHERE 
											drd.cid = ad.cid 
											AND (drd.ap_detail_id = ad.id OR
						             		drd.draw_request_id IS NULL)
										GROUP BY 
											drd.draw_request_id,
											drd.ap_detail_id,
											drd.draw_amount,
											drd.rejected_on
									) drd ON true
									LEFT JOIN apc ON apc.cid = ad.cid AND apc.ap_header_id = ad.ap_header_id
								WHERE 
									ad.cid = ' . ( int ) $intCid . '
									AND jp.job_id = ' . ( int ) $intJobId . ' 
									AND ad.deleted_by IS NULL
									AND drd.rejected_on IS NULL 
									AND ( ad.transaction_amount <> drd.drawn_amount AND drd.ap_detail_id = ad.id AND ah.retention_released_on IS NOT NULL )
								UNION';
		}

		$strSql = '
				DROP TABLE
				IF EXISTS pg_temp.apc;
				CREATE TEMP TABLE apc AS (
					SELECT
						sub.ap_header_id,
						sub.contract_name,
						sub.cid
					FROM
						(
							SELECT
								ad.cid,
								ad.ap_header_id,
								CASE
									WHEN 1 = count ( * ) over ( PARTITION BY ad.ap_header_id ) THEN apc1.name
									ELSE \'Multiple\'
								END AS contract_name
							FROM
								ap_contracts apc1,
								ap_details ad
							WHERE
								apc1.cid = ' . ( int ) $intCid . '
								AND ad.ap_contract_id = apc1.id
								AND apc1.job_id = ' . ( int ) $intJobId . '
								AND apc1.ap_contract_status_id <> ' . CApContractStatus::CANCELLED . '
							GROUP BY
								ad.cid,
								ad.ap_header_id,
								apc1.name
						) AS sub
					GROUP BY
						sub.cid,
						sub.ap_header_id,
						sub.contract_name);
				ANALYZE apc(  cid, ap_header_id );

					SELECT sub.* from ( ( 
					' . $strSqlEditDRD . '
						SELECT
						NULL::INTEGER as detail_id,
						' . $strDrawRequestId . '
						drd.rejected_on,
						ah.id as header_id,
						ah.post_date,
						ah.due_date,
						CASE WHEN ah.reversal_ap_header_id IS NOT NULL AND ah.reversal_ap_header_id < ah.id THEN
							ah.header_number || \' ( REVERSED )\'
						ELSE
							ah.header_number
						END AS header_number,
						ah.ap_header_sub_type_id as header_sub_type_id,
						CASE
							WHEN ah.lease_customer_id IS NULL THEN ( COALESCE ( ap.company_name, \'\' ) || \' \' || COALESCE ( apl.location_name, \'\' ) )
								ELSE func_format_refund_customer_names ( ah.header_memo )
							END AS ap_payee_name,
						apc.contract_name as ap_contract_name,
						CASE
							WHEN fa.ap_header_id IS NOT NULL THEN 1
							ELSE 0
						END AS is_attachment,
						CASE
							WHEN fa1.ap_payment_id IS NOT NULL THEN 1
							ELSE 0
						END AS is_lein_weiver,
						fa.file_id,
						sum(ad.transaction_amount) OVER(PARTITION BY ah.id) AS ap_detail_transaction_amount,
					    CASE
							' . $strWhenCondition . '
							WHEN ah.retention_released_on IS NOT NULL AND drd.drawn_amount > 0 THEN sum(ad.transaction_amount) over(PARTITION BY ah.id) - sum(drd.drawn_amount) OVER(PARTITION BY ah.id)
							WHEN ah.retention_released_on IS NULL AND ad.retention_amount > 0 THEN sum(ad.transaction_amount) OVER(PARTITION BY ah.id) - sum(ad.retention_amount) OVER(PARTITION BY ah.id)
							ELSE sum(ad.transaction_amount) OVER(PARTITION BY ah.id)
						END AS total_amount,
						NULL AS is_retention,
						CASE
							WHEN ah.retention_released_on IS NULL THEN ad.retention_amount
							ELSE 0
						END AS ap_detail_retention_amount,
						CASE
							WHEN ah.retention_released_on IS NULL THEN sum( ad.retention_amount ) OVER ( PARTITION BY ah.id )
							ELSE 0
						END AS total_retention_amount,
						sum( ad.retention_amount )  OVER(PARTITION BY ah.id) AS retention_amount,
						ah.retention_released_on
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' AND ah.is_posted = true )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ah.cid = ap.cid )
						JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id )
						LEFT JOIN LATERAL (
							SELECT 
								' . $strLateralSelectFields;
								if( true == valId( $intDrawRequestId ) ) {
									$strSql .= ' sum( drd.draw_amount) as drawn_amount';
								} else {
									$strSql .= ' sum( drd.draw_amount) OVER (PARTITION BY drd.ap_detail_id ) as drawn_amount';
								}
								$strSql .= ' FROM draw_request_details drd
								WHERE drd.cid = ad.cid 
									AND drd.ap_detail_id = ad.id
									' . $strLateralGroupBy . '
							) drd ON true
						LEFT JOIN apc ON apc.cid = ad.cid AND apc.ap_header_id = ad.ap_header_id 
						LEFT JOIN LATERAL (
								SELECT
									fa.ap_header_id,
									fa.file_id
								FROM
									file_associations fa
								WHERE
									ah.cid = fa.cid
									AND ah.id = fa.ap_header_id
									AND fa.deleted_by IS NULL
								ORDER BY
									fa.id DESC
								LIMIT 1
							) fa ON true
						LEFT JOIN LATERAL (
								SELECT 
									fa1.ap_payment_id,
									fa1.file_id,
									ft.system_code AS file_type
								FROM 
									ap_allocations aa
									JOIN ap_details ad1 ON (aa.cid = ad1.cid AND aa.credit_ap_detail_id = ad1.id )
									JOIN ap_headers ah1 ON (ad1.cid = ah1.cid AND ah1.id=ad1.ap_header_id )
									JOIN file_associations fa1 ON (fa1.cid = ah1.cid AND fa1.ap_payment_id = ah1.ap_payment_id  )
									JOIN files f ON (f.cid = fa1.cid AND f.id = fa1.file_id )
									JOIN file_types ft ON (ft.cid = f.cid AND f.file_type_id = ft.id)
								WHERE 
									aa.cid = ad.cid AND aa.charge_ap_detail_id = ad.id 
							 	    AND ft.system_code =  \'' . CFileType::SYSTEM_CODE_LIEN_WAIVER . '\'
									AND ah1.gl_transaction_type_id = ' . CGlTransactionType::AP_PAYMENT . '
									LIMIT 1
							) fa1 ON true
						LEFT JOIN LATERAL (
								SELECT
									drd.ap_detail_id
								FROM
									draw_request_details drd
								WHERE
									drd.cid = ad.cid
									AND drd.ap_detail_id = ad.reversal_ap_detail_id
									AND ad.reversal_ap_detail_id IS NOT NULL
							) reversal_drd ON true
						WHERE
							ad.cid  = ' . ( int ) $intCid . '
							AND jp.job_id = ' . ( int ) $intJobId . '
							AND (
								ad.reversal_ap_detail_id IS NULL
								OR reversal_drd.ap_detail_id IS NOT NULL
								' . $strReversalCondition . '
								OR ( ad.transaction_amount <> drd.drawn_amount AND ah.retention_released_on IS NOT NULL )
							)
							AND ad.deleted_by IS NULL
							AND drd.rejected_on IS NULL ' . $strWhereCondition . $strInvoiceCondition . '
						GROUP BY 
							drd.draw_request_id,
							drd.rejected_on, 
							drd.drawn_amount, 
							ah.id,
							ah.cid, 
							ad.id, 
							ad.cid,
							fa1.ap_payment_id, 
							fa1.ap_payment_id, 
							ap.company_name,
							apl.location_name,
							apc.contract_name,
							fa.ap_header_id,
							fa.file_id ) ' . $strSubSql . ' ) AS sub 
					ORDER BY 
						sub.post_date DESC,
						sub.header_id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestDetailsByDrawRequestIdByJobIdByCid( $intJobId, $intCid, $objDatabase, $intDrawRequestId, $boolIsFromDocuments = false ) {

		if( false == valId( $intJobId ) || false == valId( $intDrawRequestId ) ) {
			return NULL;
		}
		$strCondition = ' AND ( drd.draw_request_id = ' . ( int ) $intDrawRequestId;
		$strCondition .= ( false == $boolIsFromDocuments ) ? ' OR drd.draw_request_id IS NULL )' : ' ) ';

		$strWhereCondition = ( false == $boolIsFromDocuments ) ? ' AND (drd.drawn_amount IS NULL OR (ad.transaction_amount <> drd.drawn_amount AND ah.retention_released_on IS NOT NULL AND drd.rank <> 2 ) OR drd.draw_request_id = ' . ( int ) $intDrawRequestId . ' )' : ' AND drd.draw_request_id = ' . ( int ) $intDrawRequestId;

		$strLateralSelectFields   = ' drd.draw_request_id as draw_request_id,  count(drd.draw_request_id) OVER (PARTITION By ad.id) as rank , ';
		$strLateralGroupBy        = ' GROUP BY drd.draw_request_id ';
		$strLateralWhereCondition = ' OR drd.draw_request_id IS NULL ';

		$strDrawRequestId = ' drd.draw_request_id,';

		$strWhenCondition = 'WHEN drd.draw_request_id = ' . ( int ) $intDrawRequestId . ' THEN sum( drd.drawn_amount ) OVER (PARTITION BY ah.id)';

		$strSubSql = '
					UNION
					( SELECT
						NULL AS detail_id,
						drd.draw_request_id,
						gd.gl_header_id as header_id,
						NULL AS payment_ap_header_id,
						gh.post_date,
						NULL AS due_date,
						gh.header_number::TEXT,
						NULL as header_sub_type_id,
						NULL AS ap_payee_name,
						apc.name as ap_contract_name,
						CASE
							WHEN fa.gl_header_id IS NOT NULL THEN 1
							ELSE 0
						END AS is_attachment,
						NULL AS is_lein_weiver,
						fa.file_id,
						NULL AS file_type,
						NULL AS ap_header_id,
						NULL AS ap_detail_transaction_amount,
						gd.amount AS total_amount,
						NULL AS ap_detail_retention_amount,
						NULL AS total_rtention_amount,
						ac.name AS job_cost_code_name
					FROM
						gl_details gd
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' )
						LEFT JOIN job_phases jp ON ( gd.cid = jp.cid AND gd.job_phase_id = jp.id )
						LEFT JOIN draw_request_details  drd ON ( gd.cid = drd.cid AND drd.gl_detail_id = gd.id )
						LEFT JOIN draw_requests  dr ON ( dr.cid = drd.cid AND dr.id = drd.draw_request_id )
						LEFT JOIN ap_contracts apc ON ( apc.cid = gd.cid AND apc.id = gd.ap_contract_id AND apc.ap_contract_status_id <> ' . CApContractStatus::CANCELLED . ' )
						LEFT JOIN ap_codes ac ON ( ac.cid = gd.cid AND ac.id = gd.ap_code_id )
						LEFT JOIN LATERAL
							(
								SELECT
									fa.gl_header_id,
									fa.file_id
								FROM
									file_associations fa
								WHERE
									gh.cid = fa.cid
									AND gh.id = fa.gl_header_id
									AND fa.deleted_by IS NULL
								ORDER BY
									fa.id DESC
								LIMIT 1
							) fa ON true
					WHERE
						jp.job_id = ' . ( int ) $intJobId . '
						AND gh.gl_header_status_type_id = ' . CGlHeaderStatusType::POSTED . '
						AND gh.gl_header_status_type_id <> ' . CGlHeaderStatusType::DELETED . '
						AND gh.is_template IS FALSE
						AND drd.rejected_on IS NULL
						AND gd.cid = ' . ( int ) $intCid . $strCondition . ' )';

		$strSubdata = '
					     ) as sub
			GROUP BY sub.draw_request_id,
			         sub.header_id,
			         sub.post_date,
			         sub.detail_id,
			         sub.due_date,
			         sub.header_number,
			         sub.header_sub_type_id,
			         sub.ap_contract_name,
			         sub.is_attachment,
			         sub.is_lein_weiver,
			         sub.file_id,
			         sub.file_type,
			         sub.ap_header_id,
			         sub.ap_detail_transaction_amount,
			         sub.total_amount,
			         sub.ap_detail_retention_amount,
			         sub.total_retention_amount,
			         sub.payment_ap_header_id,
			         sub.job_cost_code_name,
			         sub.ap_payee_name
			ORDER BY sub.ap_payee_name,
					 sub.payment_ap_header_id,
					 sub.detail_id';

		$strSql = 'SELECT sub.* from ( ( 
					select sub.* FROM (
						SELECT
						ad.id AS detail_id,
						' . $strDrawRequestId . '
						ah.id as header_id,
						apa1.ap_payment_id AS payment_ap_header_id,
						ah.post_date,
						ah.due_date,
						ah.header_number,
						ah.ap_header_sub_type_id as header_sub_type_id,
						CASE
							WHEN ah.lease_customer_id IS NULL THEN ( COALESCE ( ap.company_name, \'\' ) || \' \' || COALESCE ( apl.location_name, \'\' ) )
								ELSE func_format_refund_customer_names ( ah.header_memo )
							END AS ap_payee_name,
						apc.name as ap_contract_name,
						CASE
							WHEN fa.ap_header_id IS NOT NULL THEN 1
							ELSE 0
						END AS is_attachment,
						CASE
							WHEN fa1.ap_header_id IS NOT NULL THEN 1
							ELSE 0
						END AS is_lein_weiver,
						fa.file_id,
						fa1.file_type AS file_type,
						fa1.ap_header_id AS ap_header_id,
						ad.transaction_amount AS ap_detail_transaction_amount,
					     CASE
						 ' . $strWhenCondition . '
				         END AS total_amount,
						CASE
							WHEN ah.retention_released_on IS NULL THEN ad.retention_amount
							ELSE 0
						END AS ap_detail_retention_amount,
						CASE
							WHEN ah.retention_released_on IS NULL THEN sum( ad.retention_amount ) OVER ( PARTITION BY ah.id )
							ELSE 0
						END AS total_retention_amount,
						ac.name AS job_cost_code_name
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' AND ah.is_posted = true )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ah.cid = ap.cid )
						JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id )
						LEFT JOIN LATERAL
						      (
						        SELECT 
						        ' . $strLateralSelectFields . '
						          sum( drd.draw_amount) as drawn_amount
						        FROM draw_request_details drd
						        WHERE drd.cid = ad.cid AND
						              ( drd.ap_detail_id = ad.id ' . $strLateralWhereCondition . ' ) AND
						              drd.rejected_on IS NULL
						        ' . $strLateralGroupBy . '
						        ) drd ON true
						LEFT JOIN ap_contracts apc ON ( apc.cid = ad.cid AND apc.id = ad.ap_contract_id AND apc.ap_contract_status_id <> ' . CApContractStatus::CANCELLED . ' )
						LEFT JOIN ap_codes ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_code_id )
						LEFT JOIN LATERAL
							(
								SELECT
									fa.ap_header_id,
									fa.file_id
								FROM
									file_associations fa
								WHERE
									ah.cid = fa.cid
									AND ah.id = fa.ap_header_id
									AND fa.deleted_by IS NULL
								ORDER BY
									fa.id DESC
								LIMIT 1
							) fa ON true
						LEFT JOIN LATERAL
							(
								SELECT 
									fa1.ap_header_id,
									fa1.file_id,
									ft.system_code AS file_type
								FROM 
									ap_allocations aa
									JOIN ap_details ad1 ON (aa.cid = ad1.cid AND aa.credit_ap_detail_id = ad1.id )
									JOIN ap_headers ah1 ON (ad1.cid = ah1.cid AND ah1.id=ad1.ap_header_id )
									JOIN file_associations fa1 ON (fa1.cid = ah1.cid AND fa1.ap_header_id = ah1.id  )
									JOIN files f ON (f.cid = fa1.cid AND f.id = fa1.file_id )
									JOIN file_types ft ON (ft.cid = f.cid AND f.file_type_id = ft.id)
								WHERE 
									aa.cid = ad.cid AND aa.charge_ap_detail_id = ad.id 
							 	    AND ft.system_code =  \'' . CFileType::SYSTEM_CODE_LIEN_WAIVER . '\'
									AND ah1.gl_transaction_type_id = ' . CGlTransactionType::AP_PAYMENT . '
									LIMIT 1
							) fa1 ON true
							 LEFT JOIN LATERAL
				            (
				              SELECT apa.charge_ap_detail_id,
                              		 aps.id as ap_payment_id
				              from 
                              ap_allocations apa
									JOIN ap_details ad1 ON (apa.cid = ad1.cid AND apa.credit_ap_detail_id = ad1.id )
									JOIN ap_headers ah1 ON (ad1.cid = ah1.cid AND ah1.id=ad1.ap_header_id )
                                    JOIN ap_payments aps ON (apa.cid = aps.cid AND ah1.ap_payment_id = aps.id )
				              WHERE apa.charge_ap_detail_id = ad.id and
				                    apa.cid = ad.cid
				              LIMIT 1
				            ) apa1 ON true
						WHERE
							jp.job_id = ' . ( int ) $intJobId . '
							AND ad.reversal_ap_detail_id IS NULL
							AND ad.deleted_by IS NULL
							AND ad.cid  = ' . ( int ) $intCid . $strWhereCondition . $strSubdata . ' )' . $strSubSql . ' ) AS sub 

					GROUP BY sub.draw_request_id,
			         sub.header_id,
			         sub.post_date,
			         sub.detail_id,
			         sub.due_date,
			         sub.header_number,
			         sub.header_sub_type_id,
			         sub.ap_contract_name,
			         sub.is_attachment,
			         sub.is_lein_weiver,
			         sub.file_id,
			         sub.file_type,
			         sub.ap_header_id,
			         sub.ap_detail_transaction_amount,
			         sub.total_amount,
			         sub.ap_detail_retention_amount,
			         sub.total_retention_amount,
			         sub.payment_ap_header_id,
			         sub.job_cost_code_name,
			         sub.ap_payee_name
			ORDER BY sub.ap_payee_name,
			         sub.payment_ap_header_id,
			         sub.detail_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestDetailCountByGlDetailIdsOrByApDetailIdsByCid( $arrintDetailIds, $intCid, $objDatabase, $boolIsForApDetails = false ) {

		if( false == valArr( $arrintDetailIds ) || false == valId( $intCid ) ) return NULL;

		if( true == $boolIsForApDetails ) {
			$strJoinSql = 'JOIN ap_details ad ON ( drd.ap_detail_id = ad.id AND drd.cid = ad.cid )';
			$strSubSql  = 'drd.ap_detail_id IN ( ' . implode( ', ', $arrintDetailIds ) . ' )';
		} else {
			$strJoinSql = 'JOIN gl_details gd ON ( drd.gl_detail_id = gd.id AND drd.cid = gd.cid )';
			$strSubSql  = 'drd.gl_detail_id IN ( ' . implode( ', ', $arrintDetailIds ) . ' )';
		}

		$strSql = ' SELECT 
						count(*)
					FROM 
						draw_request_details drd
						' . $strJoinSql . '
					WHERE 
						drd.cid = ' . ( int ) $intCid . ' AND 
						' . $strSubSql;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchDrawRequestDetailsCountByGlHeaderIdByCid( $intGlHeaderId, $intCid, $objDatabase ) {

		if( false == valId( $intGlHeaderId ) ) {
			return NULL;
		}

		$strSql = ' SELECT 
						count( * )
					FROM 
						draw_request_details drd
						JOIN gl_details gd ON ( gd.cid = drd.cid AND gd.id = drd.gl_detail_id ) 
					WHERE 
						gd.cid = ' . ( int ) $intCid . '
						AND gd.gl_header_id = ' . ( int ) $intGlHeaderId;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchDrawRequestDetailsByDrawRequestIdByApHeaderIdsByGlDetailIdsByCid( $intDrawRequestId, $arrintApHeaderIds, $arrintGlDetailIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || ( false == valArr( $arrintApHeaderIds ) && false == valArr( $arrintGlDetailIds ) ) ) return NULL;
		$strSubSql = '';

		if( true == valArr( $arrintApHeaderIds ) ) {
			$strSubSql  = ' AND ( ad.ap_header_id IN ( ' . implode( ', ', $arrintApHeaderIds ) . ' )';
		}

		if( true == valArr( $arrintGlDetailIds ) ) {
			$strSubSql .= ( true == valStr( $strSubSql ) ) ? ' OR ' : ' AND ( ';
			$strSubSql .= 'gd.id IN ( ' . implode( ', ', $arrintGlDetailIds ) . ' )';
		}

		$strSubSql .= ( true == valStr( $strSubSql ) ) ? ' )' : '';

		$strSql = 'SELECT 
						drd.*,
						CASE
							WHEN drd.ap_detail_id IS NOT NULL THEN ad.ap_header_id ELSE NULL 
						END AS ap_header_id
					FROM
						draw_request_details drd
						LEFT JOIN ap_details ad ON ( ad.id = drd.ap_detail_id AND ad.cid = drd.cid )
						LEFT JOIN gl_details gd ON ( gd.id = drd.gl_detail_id AND gd.cid = drd.cid )
					WHERE
						drd.cid = ' . ( int ) $intCid . $strSubSql . '
						AND drd.draw_request_id = ' . ( int ) $intDrawRequestId;

		return self::fetchDrawRequestDetails( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestDetailsByApDetailIdsByCid( $arrintApDetailIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApDetailIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						draw_request_details
					WHERE
						ap_detail_id IN (' . sqlIntImplode( $arrintApDetailIds ) . ')
						AND cid = ' . ( int ) $intCid;

		return parent::fetchDrawRequestDetails( $strSql, $objDatabase );
	}

	public static function fetchDrawRequestDataByApDetailIdsByCid( $arrintDetailIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDetailIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT 
                        DISTINCT drd.draw_request_id as draw_request_id,
                        dr.job_id as job_id,
                        dr.posted_on as posted_on
					FROM 
						draw_request_details drd
						JOIN draw_requests dr ON ( drd.draw_request_id = dr.id AND drd.cid = dr.cid )
					WHERE 
						drd.cid = ' . ( int ) $intCid . ' AND 
						drd.rejected_on IS NULL AND
						drd.ap_detail_id IN ( ' . implode( ', ', $arrintDetailIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRetentionDrawRequestDetailsByDrawRequestIdByCid( $intDrawRequestId, $intCid, $objDatabase ) {
		if( false == valId( $intDrawRequestId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						drd.*,
						ad.ap_header_id
					FROM
						draw_request_details drd
						JOIN ap_details ad ON ( ad.cid = drd.cid AND ad.id = drd.ap_detail_id AND ad.retention_amount = drd.draw_amount )
					WHERE
						drd.cid = ' . ( int ) $intCid . '
						AND drd.draw_request_id <> ' . ( int ) $intDrawRequestId;

		return fetchData( $strSql, $objDatabase );
	}

}
?>