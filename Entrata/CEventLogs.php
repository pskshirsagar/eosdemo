<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CEventLogs
 * Do not add any new functions to this class.
 */
class CEventLogs extends CBaseEventLogs {
	// This is temp
	public static function fetchEventLogsByLeaseIntervalIdsByCid( $arrintLeaseIntervalIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIntervalIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						event_logs
					WHERE
						lease_interval_id IN( ' . implode( ', ', $arrintLeaseIntervalIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchEventLogs( $strSql, $objDatabase );

	}

}
?>