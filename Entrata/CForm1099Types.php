<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CForm1099Types
 * Do not add any new functions to this class.
 */

class CForm1099Types extends CBaseForm1099Types {

	public static function fetchForm1099Types( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CForm1099Type', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchForm1099Type( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CForm1099Type', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllForm1099Types( $objClientDatabase ) {
		return self::fetchForm1099Types( 'SELECT * FROM form_1099_types WHERE id <> ' . CForm1099Type::FORM_1099_TYPE_7 . '. ORDER BY id DESC', $objClientDatabase );
	}

}
?>