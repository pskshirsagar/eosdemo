<?php

class CRouteType extends CBaseRouteType {

	const PURCHASE_ORDER 			= 1;
	const PURCHASE_ORDER_LINE_ITEM	= 2;
	const INVOICE 					= 3;
	const INVOICE_LINE_ITEM			= 4;
	const PAYMENT 					= 5;
	const JOURNAL_ENTRY				= 6;
	const FINANCIAL_MOVE_OUTS		= 7;
	const JOB_BUDGET				= 8;
	const CHANGE_ORDERS				= 9;
	const ADJUSTMENTS				= 10;
	const TRANSACTIONS				= 11;
	const SCHEDULED_CHARGES			= 12;
	const ADVANCED_BUDGET_WORKSHEET = 13;
	const REASONABLE_ACCOMMODATIONS = 14;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentRouteTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getRouteTypeIdByReferenceObject( $objReference ) {

		switch( get_class( $objReference ) ) {
			case 'CApHeader':
				$intRouteTypeId = self::INVOICE;

				if( CApHeaderType::PURCHASE_ORDER == $objReference->getApHeaderTypeId() ) {
					$intRouteTypeId = self::PURCHASE_ORDER;
				}
				break;

			case 'CApDetail':
				$intRouteTypeId = self::INVOICE_LINE_ITEM;

				if( CApHeaderType::PURCHASE_ORDER == $objReference->getApHeaderTypeId() ) {
					$intRouteTypeId = self::PURCHASE_ORDER_LINE_ITEM;
				}
				break;

			case 'CApPayment':
				$intRouteTypeId = self::PAYMENT;
				break;

			case 'CGlHeader':
			case 'CGlDetail':
				$intRouteTypeId = self::JOURNAL_ENTRY;
				break;

			case 'CJob':
				$intRouteTypeId = self::JOB_BUDGET;
				break;

			case 'CBudgetChangeOrder':
				$intRouteTypeId = self::CHANGE_ORDERS;
				break;

			case 'CBudgetAdjustment':
				$intRouteTypeId = self::ADJUSTMENTS;
				break;

			case 'CBudgetTab':
				$intRouteTypeId = self::ADVANCED_BUDGET_WORKSHEET;
				break;

			default:
				$intRouteTypeId = NULL;
				break;
		}

		return $intRouteTypeId;
	}

	public static function getAssociatedRouteTypeIds( $intRouteTypeId ) {

		switch( $intRouteTypeId ) {

			case self::PURCHASE_ORDER:
				$arrintRouteTypeIds[] = self::PURCHASE_ORDER;
				$arrintRouteTypeIds[] = self::PURCHASE_ORDER_LINE_ITEM;
				break;

			case self::INVOICE:
				$arrintRouteTypeIds[] = self::INVOICE;
				$arrintRouteTypeIds[] = self::INVOICE_LINE_ITEM;
				break;

			case self::PAYMENT:
				$arrintRouteTypeIds[] = self::PAYMENT;
				break;

			case self::JOURNAL_ENTRY:
				$arrintRouteTypeIds[] = self::JOURNAL_ENTRY;
				break;

			case self::FINANCIAL_MOVE_OUTS:
				$arrintRouteTypeIds[] = self::FINANCIAL_MOVE_OUTS;
				break;

			case self::JOB_BUDGET:
				$arrintRouteTypeIds[] = self::JOB_BUDGET;
				break;

			case self::CHANGE_ORDERS:
				$arrintRouteTypeIds[] = self::CHANGE_ORDERS;
				break;

			case self::ADJUSTMENTS:
				$arrintRouteTypeIds[] = self::ADJUSTMENTS;
				break;

			case self::TRANSACTIONS:
				$arrintRouteTypeIds[] = self::TRANSACTIONS;
				break;

			case self::SCHEDULED_CHARGES:
				$arrintRouteTypeIds[] = self::SCHEDULED_CHARGES;
				break;

			case self::ADVANCED_BUDGET_WORKSHEET:
				$arrintRouteTypeIds[] = self::ADVANCED_BUDGET_WORKSHEET;
				break;

			case self::REASONABLE_ACCOMMODATIONS:
				$arrintRouteTypeIds[] = self::REASONABLE_ACCOMMODATIONS;
				break;

			default:
				$arrintRouteTypeIds = [];
				break;
		}

		return $arrintRouteTypeIds;
	}

	public function getNameByRouteTypeId() {

		$strRouteName = '';

		switch( $this->getId() ) {

			case self::PURCHASE_ORDER:
			case self::PURCHASE_ORDER_LINE_ITEM:
				$strRouteName = 'P.O.';
				break;

			case self::INVOICE:
			case self::INVOICE_LINE_ITEM:
				$strRouteName = 'Invoice';
				break;

			case self::PAYMENT:
				$strRouteName = 'Payment';
				break;

			case self::JOURNAL_ENTRY:
				$strRouteName = 'JE';
				break;

			case self::FINANCIAL_MOVE_OUTS:
				$strRouteName = 'FMO';
				break;

			case self::JOB_BUDGET:
				$strRouteName = 'Job Budget';
				break;

			case self::CHANGE_ORDERS:
				$strRouteName = 'C.O.';
				break;

			case self::ADJUSTMENTS:
				$strRouteName = 'Adjust.';
				break;

			case self::TRANSACTIONS:
				$strRouteName = 'Transaction';
				break;

			case self::SCHEDULED_CHARGES:
				$strRouteName = 'Scheduled Charge';
				break;

			case self::ADVANCED_BUDGET_WORKSHEET:
				$strRouteName = 'Advanced Budget Worksheets';
				break;

			case self::REASONABLE_ACCOMMODATIONS:
				$strRouteName = 'Reasonable Accommodations';
				break;

			default:
				$strRouteName = '';
				break;
		}

		return $strRouteName;
	}

}
?>