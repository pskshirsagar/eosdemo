<?php

class CSubsidySetAsideType extends CBaseSubsidySetAsideType {

	const TYPE_40_60           = 1;
	const TYPE_20_50           = 2;
	const TYPE_15_40           = 3;
	const TYPE_HTC             = 4;
	const TYPE_EXCHANGE        = 5;
	const TYPE_TCAP            = 6;
	const TYPE_SHTF            = 7;
	const TYPE_NSP             = 8;
	const TYPE_OTHER           = 9;
	const TYPE_TAX_EXEMPT_BOND = 10;
	const TYPE_NHTF            = 11;
	const TYPE_AHDP            = 12;

	public static $c_arrstrAllSubsidyTypes = [
		self::TYPE_40_60           => '40/60',
		self::TYPE_20_50           => '20/50',
		self::TYPE_15_40           => '15/40',
		self::TYPE_HTC             => 'HTC',
		self::TYPE_EXCHANGE        => 'Exchange',
		self::TYPE_TCAP            => 'TCAP',
		self::TYPE_SHTF            => 'SHTF',
		self::TYPE_NSP             => 'NSP',
		self::TYPE_OTHER           => 'Other',
		self::TYPE_TAX_EXEMPT_BOND => 'Tax Exempt Bond',
		self::TYPE_NHTF            => 'NHTF',
		self::TYPE_AHDP            => 'AHDP'
	];

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valQualifiedUnitsPercent() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valMedianIncomePercent() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valFirstYearLevel() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsCustom() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getTypeNameByTypeId( $intTypeId ) {
		return self::$c_arrstrAllSubsidyTypes[$intTypeId];
	}

}

?>