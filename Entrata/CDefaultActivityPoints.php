<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultActivityPoints
 * Do not add any new functions to this class.
 */

class CDefaultActivityPoints extends CBaseDefaultActivityPoints {
	const DEFAULT_ACTIVITY_POINT_CREATE_AN_EVENT 				= 1;
	const DEFAULT_ACTIVITY_POINT_RSVP_TO_AN_EVENT 				= 2;
	const DEFAULT_ACTIVITY_POINT_COMMENT_ON_AN_EVENT 			= 3;
	const DEFAULT_ACTIVITY_POINT_CREATE_A_CLUB 					= 4;
	const DEFAULT_ACTIVITY_POINT_JOIN_A_CLUB					= 5;
	const DEFAULT_ACTIVITY_POINT_REFER_A_FRIEND					= 6;
	const DEFAULT_ACTIVITY_POINT_DONATE_TO_A_CAUSE				= 7;
	const DEFAULT_ACTIVITY_POINT_ENTER_INSURANCE_INFO			= 8;
	const DEFAULT_ACTIVITY_POINT_PURCHASE_INSURANCE				= 9;
	const DEFAULT_ACTIVITY_POINT_WORK_SURVEY					= 10;
	const DEFAULT_ACTIVITY_POINT_RATINGS_SURVEY					= 11;
	const DEFAULT_ACTIVITY_POINT_MOVE_OUT_SURVEY				= 12;
	const DEFAULT_ACTIVITY_POINT_MOVE_IN_SURVEY					= 13;
	const DEFAULT_ACTIVITY_POINT_ADD_INTERESTS					= 14;
	const DEFAULT_ACTIVITY_POINT_ADD_PHOTO						= 15;
	const DEFAULT_ACTIVITY_POINT_RENEW_LEASE					= 16;
	const DEFAULT_ACTIVITY_POINT_PAY_RENT						= 17;
	const DEFAULT_ACTIVITY_POINT_PREPAY_RENT					= 18;
	const DEFAULT_ACTIVITY_POINT_RECURRING_PAYMENT				= 19;
	const DEFAULT_ACTIVITY_POINT_ENROLL_IN_PORTAL				= 20;
	const DEFAULT_ACTIVITY_POINT_LOGIN_TO_ACCOUNT				= 21;
	const DEFAULT_ACTIVITY_POINT_CREATE_CLASSIFIEDS				= 22;
}
?>