<?php

class CAncillaryPromoRegion extends CBaseAncillaryPromoRegion {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAncillaryPlanPromoDetailsId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCodes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function insert( $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.ancillary_promo_regions_id_seq\' )' : ( int ) $this->m_intId;

		$strSql = 'INSERT INTO
					  public.ancillary_promo_regions
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlAncillaryPlanPromoDetailsId() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlPostalCodes() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function delete( $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.ancillary_promo_regions WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>