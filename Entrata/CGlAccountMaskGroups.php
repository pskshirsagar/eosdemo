<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountMaskGroups
 * Do not add any new functions to this class.
 */

class CGlAccountMaskGroups extends CBaseGlAccountMaskGroups {

	public static function fetchGlAccountMaskGroupsByGlGroupIdsByCid( $arrintGlGroupIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGlGroupIds ) ) return NULL;

		$strSql = 'SELECT * FROM gl_account_mask_groups WHERE cid = ' . ( int ) $intCid . ' AND gl_group_id IN ( ' . implode( ',', $arrintGlGroupIds ) . ' )';

		return self::fetchGlAccountMaskGroups( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountMaskGroupsByGlAccountTreeIdsByCid( $arrintGlAccountTreeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGlAccountTreeIds ) ) return NULL;

		$strSql = 'SELECT * FROM gl_account_mask_groups WHERE cid = ' . ( int ) $intCid . ' AND gl_account_tree_id IN ( ' . implode( ',', $arrintGlAccountTreeIds ) . ' )';

		return self::fetchGlAccountMaskGroups( $strSql, $objClientDatabase );
	}
}
?>