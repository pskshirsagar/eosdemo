<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoveInSchedules
 * Do not add any new functions to this class.
 */

class CMoveInSchedules extends CBaseMoveInSchedules {
	const LEASE_APPROVED_TYPE_MOVE_IN_SCHEDULE_TYPE_ID = 5;

	public static function fetchMoveInScheduleByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intLeaseTermId = NULL, $intLeaseStartWindowId = NULL, $intMoveInSchedulerId = NULL, $boolIsConventionalScheduler = false ) {

		$strWhere = $strJoin = ' ';

		if( false == $boolIsConventionalScheduler ) {
			if( false == valId( $intLeaseTermId ) || false == valId( $intLeaseStartWindowId ) || false == valId( $intPropertyId ) ) {
				return NULL;
			}

			$strWhere = ' AND misglt.lease_term_id = ' . ( int ) $intLeaseTermId . '
					      AND misglt.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
						  AND misglt.deleted_by IS NULL';
			$strJoin = ' JOIN move_in_schedule_group_lease_terms misglt ON ( mis.cid = misglt.cid AND mis.move_in_schedule_group_id = misglt.move_in_schedule_group_id )';
		}

		if( true == valId( $intMoveInSchedulerId ) ) {
			$strWhere = ' AND mis.id = ' . $intMoveInSchedulerId . ' ';
		}

		$strSql = 'SELECT
					    mis.*
					FROM
					    move_in_schedules mis
					    ' . $strJoin . '
					WHERE
					    mis.cid = ' . ( int ) $intCid . '
					    AND mis.property_id = ' . ( int ) $intPropertyId . '
					    AND mis.deleted_by IS NULL
					     ' .
					    $strWhere . '
					    LIMIT 1';

		return parent::fetchMoveInSchedule( $strSql, $objDatabase );

	}

	public static function fetchActiveMoveInSchedulesByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $arrintLeaseTermIds, $arrintLeaseStartWindowIds, $intPropertyId, $intCid, $objDatabase, $boolIsFromListing = false ) {

		$strSelectField = '';
		$strJoinSql     = '';

		if( true == $boolIsFromListing ) {
			$strSelectField = ' ,lease_appointment_associations.appointment_selected_lease_count';
			$strJoinSql     = ' LEFT JOIN LATERAL
								(
								  SELECT
								      count ( * ) AS appointment_selected_lease_count
								  FROM
								      move_in_schedule_associations misa1
								  WHERE
								      misa.cid = misa1.cid
								      AND misa.property_id = misa1.property_id
								      AND misa.move_in_schedule_id = misa1.move_in_schedule_id
								      AND mis.details::JSONB ->> \'use_appointments\' IS NOT NULL
								      AND misa1.lease_id IS NOT NULL
								      AND CASE
											WHEN misa.property_building_id IS NOT NULL THEN misa.property_building_id = misa1.property_building_id
											WHEN misa.property_floor_id IS NOT NULL THEN misa.property_floor_id = misa1.property_floor_id
											WHEN misa.property_floorplan_id IS NOT NULL THEN misa.property_floorplan_id = misa1.property_floorplan_id
											WHEN misa.cap_size IS NULL AND mis.move_in_schedule_type_id = ' . self::LEASE_APPROVED_TYPE_MOVE_IN_SCHEDULE_TYPE_ID . ' THEN TRUE
											ELSE FALSE
										  END
									  AND misa1.deleted_on IS NULL
									  AND misa1.deleted_by IS NULL
								  GROUP BY
								      misa1.cid,
								      misa1.property_id,
								      misa1.move_in_schedule_id
								) AS lease_appointment_associations ON TRUE';
		}

		$strSql = 'SELECT 
						mis.*,
                        util_get_translated( \'floorplan_name\', pf.floorplan_name, pf.details ) AS property_floorplan_name,
                        pf.id As property_floorplan_id,
                        pb.id as property_building_id,
                        util_get_translated( \'building_name\', pb.building_name, pb.details ) AS building_name,
                        pfs.id as property_floor_id,
                        concat ( util_get_translated ( \'floor_title\', pfs.floor_title, pfs.details ), \' (\', util_get_translated ( \'building_name\', pfs.building_name, pfs.pb_details ), \')\' ) AS floor_title,
                        misa.cap_size,
                        mis.details::JSONB ->> \'use_appointments\' AS use_appointments,
                        (mis.details::JSONB ->> \'checklist_requirement\' )::VARCHAR AS checklist_requirement,
                        (mis.details::JSONB ->> \'appointment_cap\' )::VARCHAR AS appointment_cap
                        ' . $strSelectField . '
					FROM 
						move_in_schedule_groups misg
						JOIN move_in_schedule_group_lease_terms misglt ON ( misg.cid = misglt.cid AND misg.id = misglt.move_in_schedule_group_id ) 
						JOIN move_in_schedules mis ON ( misg.cid = mis.cid AND misg.id = mis.move_in_schedule_group_id )
                        JOIN move_in_schedule_associations misa ON ( mis.cid = misa.cid and mis.property_id = misa.property_id AND mis.id = misa.move_in_schedule_id AND misa.lease_id IS NULL )
                        ' . $strJoinSql . '
                        LEFT JOIN property_floorplans pf ON ( pf.cid = misa.cid AND pf.property_id = misa.property_id AND pf.id = misa.property_floorplan_id AND mis.move_in_schedule_type_id = ' . CMoveInScheduleType::FLOORPLAN . ' )
                        LEFT JOIN property_buildings pb ON ( pb.cid = misa.cid AND pb.property_id = misa.property_id AND pb.id = misa.property_building_id AND mis.move_in_schedule_type_id = ' . CMoveInScheduleType::BUILDING . ' )
                        LEFT JOIN LATERAL 
					    (
					      SELECT
					          pf.cid,
					          pf.property_id,
					          pf.id,
					          pf.floor_title,
					          pb.building_name,
					          pf.details,
					          pb.details AS pb_details
					      FROM
					          property_floors pf
					          JOIN property_buildings pb ON ( pf.cid = pb.cid AND pf.property_id = pb.property_id AND pf.property_building_id = pb.id )
					      WHERE
					          pf.cid = misa.cid
					          AND pf.property_id = misa.property_id
					          AND pf.id = misa.property_floor_id
					          AND mis.move_in_schedule_type_id = ' . CMoveInScheduleType::FLOOR . '
					    ) pfs ON TRUE
					WHERE misglt.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' ) AND
					      misglt.lease_start_window_id IN ( ' . implode( ',', $arrintLeaseStartWindowIds ) . ' ) AND
					      misg.cid = ' . ( int ) $intCid . ' AND
					      misg.property_id = ' . ( int ) $intPropertyId . ' AND
					      misg.deleted_by IS NULL and
					      mis.deleted_by IS NULL AND
					      misglt.deleted_by IS NULL AND
					      misa.deleted_by IS NULL
					ORDER BY mis.id, misa.id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveMoveInSchedulesByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCidForUpdate( $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT mis.*
					FROM move_in_schedules mis
                    WHERE mis.lease_term_id = ' . ( int ) $intLeaseTermId . ' AND
					      mis.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . ' AND
					      mis.cid = ' . ( int ) $intCid . ' AND
					      mis.property_id = ' . ( int ) $intPropertyId . ' AND
					      mis.deleted_on IS NULL and
					      mis.deleted_by IS NULL';

		return self::fetchMoveInSchedules( $strSql, $objDatabase );
	}

	public static function fetchMoveInSchedulesByIdsByCidByPropertyId( $arrintMoveInScheduleIds, $intCid, $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT * 
					FROM move_in_schedules 
					WHERE id IN ( ' . implode( ',', $arrintMoveInScheduleIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchMoveInSchedules( $strSql, $objDatabase );
	}

	public static function fetchActiveMoveInScheduleCountByMoveInScheduleTypeIdByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $intMoveInScheduleTypeId, $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    CASE
					      WHEN count ( mis.id ) > 0 THEN 1
					      ELSE 0
					    END move_in_schedule_count
					FROM
					    move_in_schedules mis
					    JOIN move_in_schedule_associations misa ON ( mis.cid = misa.cid AND mis.property_id = misa.property_id AND mis.id = misa.move_in_schedule_id )
					WHERE
					    mis.lease_term_id = ' . ( int ) $intLeaseTermId . '
					    AND mis.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
					    AND mis.move_in_schedule_type_id <> ' . ( int ) $intMoveInScheduleTypeId . '
					    AND mis.cid = ' . ( int ) $intCid . '
					    AND mis.property_id = ' . ( int ) $intPropertyId . '
					    AND mis.deleted_on IS NULL
					    AND mis.deleted_by IS NULL
					    AND misa.deleted_on IS NULL
					    AND misa.deleted_by IS NULL';

		return self::fetchColumn( $strSql, 'move_in_schedule_count', $objDatabase );
	}

	public static function fetchActiveMoveInSchedulesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    mis.*
					FROM
					    move_in_schedules mis
					    JOIN move_in_schedule_associations misa ON ( mis.cid = misa.cid AND mis.property_id = misa.property_id AND mis.id = misa.move_in_schedule_id )
					WHERE
					    mis.cid = ' . ( int ) $intCid . '
					    AND mis.property_id = ' . ( int ) $intPropertyId . '
					    AND mis.deleted_on IS NULL
					    AND mis.deleted_by IS NULL
					    AND misa.deleted_on IS NULL
					    AND misa.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
