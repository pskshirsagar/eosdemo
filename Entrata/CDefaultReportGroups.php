<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportGroups
 * Do not add any new functions to this class.
 */

class CDefaultReportGroups extends CBaseDefaultReportGroups {

	public static function fetchDefaultReportGroups( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultReportGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultReportGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultReportGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultReportGroupByModuleId( $intModuleId, $objDatabase ) {
		return self::fetchDefaultReportGroup( sprintf( 'SELECT * FROM default_report_groups WHERE module_id = %d', ( int ) $intModuleId ), $objDatabase );
	}

	public static function fetchDefaultReportGroupsByParentModuleId( $intParentModuleId, $objDatabase ) {
		$strSql = '
			SELECT
				drg.id,
				drg.name,
				drg.name AS title,
				drg.is_published,
				' . ( int ) $intParentModuleId . ' AS parent_module_id,
				count(dr.default_report_group_id) AS child_elements
			FROM
				default_report_groups drg
				LEFT JOIN default_reports dr ON( drg.id = dr.default_report_group_id )
			WHERE
				drg.parent_module_id =' . ( int ) $intParentModuleId . '
			GROUP BY
				drg.id,
				drg.parent_module_id,
				drg.name
			ORDER
				BY drg.id';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * @param $arrintParentModuleIds
	 * @param $objDatabase
	 * @return CDefaultReportGroup[]
	 */
	public static function fetchDefaultReportGroupsByParentModuleIds( $arrintParentModuleIds, $objDatabase ) {
		$strSql = '
			SELECT
				drg.*,
				count(dr.default_report_group_id) AS child_elements
			FROM
				default_report_groups drg
				LEFT JOIN default_reports dr ON( drg.id = dr.default_report_group_id )
			WHERE
				drg.parent_module_id IN( ' . implode( ',', $arrintParentModuleIds ) . ' )
			GROUP BY
				drg.id,
				drg.parent_module_id,
				drg.name
			ORDER BY
				drg.name';

		return self::fetchDefaultReportGroups( $strSql, $objDatabase ) ?: [];
	}

	public static function fetchDefaultReportGroupMaxId( $objDatabase ) {
		$strSql = '
			SELECT
				MAX( id ) AS id
			FROM
				default_report_groups';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchModuleIdByDefaultReportGroupId( $intDefaultReportgroupId, $objDatabase ) {
		$strSql = '
			SELECT
				drg.module_id
			FROM
				default_report_groups drg
			WHERE
				drg.id = ' . ( int ) $intDefaultReportgroupId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMaxPositionNumberByParentModuleId( $intParentModuleId, $objDatabase ) {
		$strSql = '
			SELECT
				MAX( position_number ) AS position_number
			FROM
				default_report_groups
			WHERE parent_module_id =' . ( int ) $intParentModuleId;

		return fetchData( $strSql, $objDatabase );
	}

}
