<?php

class CMaintenanceGroup extends CBaseMaintenanceGroup {

	const CREATED = 'maintenance_group_created';
	const UPDATED = 'maintenance_group_updated';
	const APPROVED = 'maintenance_group_approved';
	const COMPLETED = 'maintenance_group_completed';

	protected $m_strAction;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentMaintenanceGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( true == $boolReturnSqlOnly ) {
			$strDeleteSql = $this->update( $intUserId, $objDatabase, true );
			return $strDeleteSql;
		} else {
			if( $this->update( $intUserId, $objDatabase ) ) {
				return true;
			}
		}
	}

	public function setActionTakenOnMaintenanceGroup( $strAction ) {
		$this->m_strAction = $strAction;
	}

	public function getActionTakenOnMaintenanceGroup() {
		return $this->m_strAction;
	}

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

}
?>
