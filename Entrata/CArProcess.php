<?php

class CArProcess extends CBaseArProcess {

	const LEASE_APPROVAL		= 1;
	const MOVE_IN				= 2;
	const TRANSFER				= 3;
	const LEASE_END_DATE_CHANGE	= 4;
	const RENEWAL_DATE_CHANGE	= 5;
	const NOTICE				= 6;
	const MOVE_OUT				= 7;
	const FINAL_STATEMENT		= 8;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>