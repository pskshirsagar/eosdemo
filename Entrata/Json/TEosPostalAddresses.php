<?php

use Psi\Libraries\I18n\PostalAddress\CPostalAddress;

trait TEosPostalAddresses {

	/**
	 * Table annotation: @{"postal_addresses":true}@
	 *
	 * Sample field annotations:
	 * key: identification for multiple address fields in the same table
	 * field: Constant values from CPostalAddress POPO
	 *
	 * bank_accounts.bank_street_line1: @{"postal_addresses":{"key":"bank","field":"addressLine1"}}@
	 * bank_accounts.bank_state_code: @{"postal_addresses":{"key":"bank","field":"administrativeArea"}}@
	 * bank_accounts.bank_postal_code: @{"postal_addresses":{"key":"bank","field":"postalCode"}}@
	 *
	 * bank_accounts.payer_street_line1: @{"postal_addresses":{"key":"payer","field":"addressLine1"}}@
	 * bank_accounts.payer_state_code: @{"postal_addresses":{"key":"payer","field":"administrativeArea"}}@
	 * bank_accounts.payer_city: @{"postal_addresses":{"key":"payer","field":"locality"}}@
	 *
	 * If there is just one address in table:
	 * property_addresses.street_line1: @{"postal_addresses":{"key":"default","field":"addressLine1"}}@
	 * OR
	 * property_addresses.street_line1: @{"postal_addresses":{"field":"addressLine1"}}@ // if key is not present it
	 * will add default key.
	 *
	 * When PgDictionary sees a field annotation:
	 * - It will modify the setter and getter methods to call set/getPostalAddressField() similar to
	 * set/getTranslated()
	 * - Comment on bank_accounts.bank_street_line1: @{"postal_addresses":{"key":"bank","field":"addressLine1"}}@
	 * - Code for setter: $this->setPostalAddressField( 'm_strBankStreetLine1', 'addressLine1', $strBankStreetLine1,
	 * $strAddressKey = 'bank' );
	 * - Code for getter: return $this->getPostalAddressField( 'm_strBankStreetLine1', 'addressLine1', 'bank' );
	 *
	 * Json structure in details field:
	 * bank_accounts.details:
	 *{
	 *    "_postal_addresses": {
	 *        "bank": {
	 *            "addressLine1": "123 Test St.",
	 *            "addressLine2": "",
	 *            "locality": "Fountain Valley"
	 *        },
	 *        "payer": {
	 *            "addressLine1": "123 Test St.",
	 *            "addressLine2": "",
	 *            "locality": "Fountain Valley"
	 *        }
	 *    },
	 *    "_translate": {
	 *        "en_us": {
	 *            "name": "Shaunak"
	 *        },
	 *        "es_mx": {
	 *            "name": "Şĥảμ𝒏𝞪𝚔"
	 *        }
	 *    }
	 *}
	 *
	 * Flow:
	 * - fetchObjects will call all setter methods.
	 * - It will call $this->setPostalAddressField() will set values on member variable, POPO and details field
	 * - Which calls $this->getDetails() for getting data from details field for persisting back setter value
	 * - It will call $this->loadPostalAddressesFromJson( $this->m_jsonDetails ) which will hydrate
	 * $m_arrobjPostalAddresses
	 *
	 * When getter is called, it will get the value from coalesce(POPO value, Json value, member variable value)
	 *
	 */

	private $m_strPostalAddressJsonKey = '_postal_addresses';

	/**
	 * @var bool
	 */
	private $m_boolPostalAddressesInitialized = false;

	/**
	 * @var \Psi\Libraries\I18n\PostalAddress\CPostalAddress[]
	 */
	private $m_arrobjPostalAddresses = [];

	/**
	 * This method would be called from each setter method that has postal address field annotation.
	 * @param        $strAddressField
	 * @param        $strValue
	 * @param string $strAddressKey
	 * @param        $strEosFieldName
	 */
	private function setPostalAddressField( $strAddressField, $strValue, $strAddressKey = 'default', $strEosFieldName = NULL ) {

		$strMethodName = 'set' . \Psi\Libraries\UtilInflector\CInflector::createService()->camelize( preg_replace( '/^(m_[a-z]+)([a-zA-Z0-9]+)$/', '$2', $strAddressField ) );

		// Set value on POPO
		$this->getPostalAddress( $strAddressKey )->$strMethodName( $strValue );

		// Set value on field if it exists
		if( true == property_exists( $this, $strEosFieldName ) ) {
			$this->set( $strEosFieldName, $strValue );
		}

		// Set the value on jsonDetails.
		$this->setDetailsField( $this->getPostalAddressJsonKey(), convertObjectsToArray( $this->m_arrobjPostalAddresses ) );

	}

	/**
	 * This method would be called from each getter method that has postal address field annotation.
	 * @param        $strAddressField
	 * @param string $strAddressKey
	 * @param        $strEosFieldName
	 * @return mixed
	 */

	private function getPostalAddressField( $strAddressField, $strAddressKey = 'default', $strEosFieldName = NULL ) {

		$strMethodName = 'get' . \Psi\Libraries\UtilInflector\CInflector::createService()->camelize( preg_replace( '/^(m_[a-z]+)([a-zA-Z0-9]+)$/', '$2', $strAddressField ) );

		$strValueFromJson = $this->getPostalAddress( $strAddressKey )->$strMethodName();

		// Give preference to POPO which is coming from json value after the getDetails() call.
		if( false == is_null( $strValueFromJson ) ) {
			return $strValueFromJson;
		}

		if( true == property_exists( $this, $strEosFieldName ) ) {
			return $this->$strEosFieldName;
		}

		return NULL;
	}

	private function loadPostalAddress( $strKey ) : CPostalAddress {
		$this->initializePostalAddresses();
		if( false == isset( $this->m_arrobjPostalAddresses[$strKey] ) ) {
			$strDefaultCountry                      = CLocaleContainer::createService()->getLocaleRegion();
			$this->m_arrobjPostalAddresses[$strKey] = new CPostalAddress( [], $strDefaultCountry );
		}

		return $this->m_arrobjPostalAddresses[$strKey];

	}

	/**
	 * This would be called from getDetails()
	 * Truth table:
	 *Case  | Json Key    | Discrete Fields | Details
	 * ----------------------------------------------------
	 * 1    | No          | ALL             | When data is not migrated and its present on discrete fields.
	 * 2    | No          | Some            | When data is not migrated and its present on discrete fields. This case
	 *                                          will come when PRIMARY & SECONDARY are discrete fields, while HOME is
	 *                                          being inserted for first time. It will not come for loadAddresses().
	 * 3    | No          | None            | When data is stored in new table for the first time
	 *
	 * 4    | Yes         | ALL             | When data co-exists in details as well as in discrete fields. Case when
	 *                                          data is migrated and fields are not dropped.
	 * 5    | Yes         | Some            | When some data is present in details field and remaining in discrete
	 *                                          fields. e.g.: HOME in details, PRIMARY & SECONDARY on discrete fields
	 * 6    | Yes         | None            | When data is only present in details field.
	 *
	 * @param $jsonDetails
	 */
	private function initializePostalAddresses() {
		// This function would be called for each postal address. $jsonDetails is empty until that field is processed from setValues
		if( true == $this->isPostalAddressesInitialized() ) {
			return;
		}

		$strDefaultCountry = CLocaleContainer::createService()->getLocaleRegion();
		// This would be empty initially until details get set from setValues()
		$arrmixPostalAddressesFromDetails = ( array ) json_decode( json_encode( $this->getDetailsField( $this->getPostalAddressJsonKey() ) ), true );

		// Case1: Json key is not present, All fields are discrete fields. Action: load PostalAddress from discrete fields
		if( true == valArr( $this->m_arrstrPostalAddressFields ) ) {
			foreach( $this->m_arrstrPostalAddressFields as $strAddressKey => $arrmixFieldMapping ) {
				$arrmixHydratedFieldMappings                   = $this->hydrateEosFieldMappings( $arrmixFieldMapping );
				$this->m_arrobjPostalAddresses[$strAddressKey] = new CPostalAddress( $arrmixHydratedFieldMappings, $strDefaultCountry );
			}
		}

		// Case2: Json key not present, some data on discrete fields
		// Data from discrete field would be loaded in case1 above.

		// Case3: Json key not present, no data on discrete fields
		// This case won't come here in loadAddresses()

		// Case4: Json key is present, data co-exists on discrete fields

		if( true == valArr( $arrmixPostalAddressesFromDetails ) ) {
			foreach( $arrmixPostalAddressesFromDetails as $strKey => $mixPostalAddress ) {
				$this->m_arrobjPostalAddresses[$strKey] = new CPostalAddress( ( array ) $mixPostalAddress, $strDefaultCountry );
			}
		}

		// Case5: Json key is present, some data is present in details field and remaining in discrete columns
		// This case would be handled in combination of case1 & case4

		// Case6: Json key is present, no data on discrete column. Long term goal.
		// This case would be handled in case4

		$this->setPostalAddressesInitialized( true );

	}

	/**
	 * Function to use mappings and values to create addressKey=>addressValue structure
	 */
	private function hydrateEosFieldMappings( $arrmixFieldMapping ) : array {
		$arrmixHydratedMappings = [];
		foreach( $arrmixFieldMapping as $strEosMemberVariableName => $strAddressField ) {
			if( true == property_exists( $this, $strEosMemberVariableName ) ) {
				$arrmixHydratedMappings[$strAddressField] = $this->$strEosMemberVariableName;
			}
		}

		return $arrmixHydratedMappings;
	}

	/**
	 * This function would be used for creating postal addresses submitted from UI. i.e.: from setValues()
	 * @param $arrmixValues
	 */
	public function setPostalAddressValues( $arrmixValues, $arrmixFieldMappings ) {
		$this->initializePostalAddresses();

		$arrmixPostalAddressesFromRequest = $arrmixValues['postal_addresses'] ?? [];

		foreach( $arrmixPostalAddressesFromRequest as $strAddressKey => $arrmixPostalAddress ) {
			$arrstrEosFields = $arrmixFieldMappings[$strAddressKey];

			// flip to get addressField
			$arrstrEosFields = array_flip( $arrstrEosFields );

			foreach( $arrmixPostalAddress as $strAddressField => $strAddressValue ) {
				$this->setPostalAddressField( $strAddressField, $strAddressValue, $strAddressKey, $arrstrEosFields[$strAddressField] ?? NULL );
			}
		}
	}

	private function clonePostalAddresses() {
		$arrmixClonedObjects = [];
		// @TODO: Not sure how this function would work. Need to test.
		foreach( $this->m_arrobjPostalAddresses as $objPostalAddress ) {
			$arrmixClonedObjects[] = clone $objPostalAddress;
		}

		$this->m_arrobjPostalAddresses = $arrmixClonedObjects;

	}

	/**
	 * @return bool
	 */
	public function isPostalAddressesInitialized() : bool {
		return $this->m_boolPostalAddressesInitialized;
	}

	/**
	 * @param bool $boolPostalAddressesInitialized
	 */
	public function setPostalAddressesInitialized( bool $boolPostalAddressesInitialized ) {
		$this->m_boolPostalAddressesInitialized = $boolPostalAddressesInitialized;
	}

	public function getPostalAddress( $strKey = 'default' ) : CPostalAddress {
		return $this->loadPostalAddress( $strKey );
	}

	public function setPostalAddress( $strKey, CPostalAddress $objPostalAddress ) {
		$this->m_arrobjPostalAddresses[$strKey] = $objPostalAddress;
	}

	/**
	 * @return string
	 */
	private function getPostalAddressJsonKey() : string {
		return $this->m_strPostalAddressJsonKey;
	}

}