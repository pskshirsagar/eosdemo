<?php
trait TDetailsUserDefinedFields {

	protected $m_objUserDefinedFields;

	public function cloneUDF() {

		if( true == valObj( $this->m_objUserDefinedFields, 'CUserDefinedFields' ) ) {
			$this->m_objUserDefinedFields = clone $this->m_objUserDefinedFields;
		}
	}

	public function __call( $strMethod, $arrmixArguments ) {

		$strKey = \Psi\Libraries\UtilStrings\CStrings::createService()->inflectUnderscore( \Psi\CStringService::singleton()->substr( $strMethod, 3 ) );

		if( 0 === \Psi\CStringService::singleton()->strpos( $strMethod, 'get' ) ) {
			return $this->getTranslated( $strKey, $arrmixArguments[0] ?? NULL, false );
		} elseif( 0 === \Psi\CStringService::singleton()->strpos( $strMethod, 'set' ) ) {
			$this->setTranslated( $strKey, $arrmixArguments[0], $arrmixArguments[1] ?? NULL, false );
		}
	}

	public function getUserDefinedFields() {
		if( false == valObj( $this->m_objUserDefinedFields, 'CUserDefinedFields' ) && true == valObj( $this->getDetails(), 'stdClass' ) ) {
			$this->m_objUserDefinedFields = new CUserDefinedFields( ( array ) $this->getTranslated( 'user_defined_fields', NULL, false ) );
		} elseif( false == valObj( $this->m_objUserDefinedFields, 'CUserDefinedFields' ) ) {
			$this->m_objUserDefinedFields = new CUserDefinedFields();
		}
		return $this->m_objUserDefinedFields;
	}

	public function setUserDefinedFields( $objUserDefinedFields ) {
		$this->m_objUserDefinedFields = $objUserDefinedFields;
		$this->setTranslated( 'user_defined_fields', $this->m_objUserDefinedFields->toJson(), NULL, false );
	}

	public function loadUserDefinedFields() {
		if( false == valObj( $this->m_objUserDefinedFields, 'CUserDefinedFields' ) ) return;

		if( false == valObj( $this->m_jsonDetails, 'stdClass' ) ) {
			$this->m_jsonDetails = new stdClass();
		}

		$this->m_jsonDetails->user_defined_fields = $this->m_objUserDefinedFields->toJson();
	}

	final private function getTranslated( $strName, $strLocaleCode = NULL, $boolIsDiscreetField = true ) {
		$objLocaleContainer = CLocaleContainer::createService();
		$strDefaultLocale = $objLocaleContainer->getDefaultLocaleCode();
		$boolIsSystemTranslated = false;
		if( true == method_exists( $this, 'getIsSystemTranslated' ) && true == $this->getIsSystemTranslated() ) {
			$strLocaleCode = $strLocaleCode ?? $objLocaleContainer->getLocaleCode();
			$boolIsSystemTranslated = true;
			if( CLocale::DEFAULT_LOCALE == $strLocaleCode ) return $this->$strName;
		} else {
			$strLocaleCode = $strLocaleCode ?? $objLocaleContainer->getTargetLocaleCode() ?? $objLocaleContainer->getLocaleCode();
		}
		$arrDetails = json_decode( json_encode( $this->getDetails() ?? new stdClass() ), true );
		$strKeyName = $boolIsDiscreetField ? \Psi\Libraries\UtilStrings\CStrings::createService()->inflectUnderscore( preg_replace( '/^m_[a-z]+([a-zA-Z0-9]+)$/', '$1', $strName ) ) : $strName;
		$arrTranslated = $arrDetails['_translated'] ?? [];

		/*
		 TEMPORAILY DISABLE DYNAMIC DATA MOCKING (FURTHER TESTING REQUIRED)
		 // Apply mocking if cookie is enabled
		 if( !empty( $_COOKIE['I18N_MOCK'] ) && is_numeric( $_COOKIE['I18N_MOCK'] ) && '0' != ( $_COOKIE['I18N_MOCK'] ) ) {
		 	$mixDefaultValue = $boolIsDiscreetField ? $this->$strName : $arrDetails[$strKeyName];
		 	// We mock only 1 level deep into any nested structure
		 	if( valArr( $mixDefaultValue ) ) {
		 		foreach( $mixDefaultValue as $strKey => $strValue ) {
		 			$mixDefaultValue[$strKey] = valStr( $strValue ) ? __( $strValue ) : $strValue;
		 		}
		 	} else {
		 		return __( $mixDefaultValue );
		 	}
		 }
		 */

		array_walk( $arrTranslated,
			function( $mixValue, $strKey ) use ( &$strFuzzyLocaleCode, $strLocaleCode, $objLocaleContainer ) {
				if( valStr( $strFuzzyLocaleCode ) ) return;
				$strFuzzyLocaleCode = ( !$objLocaleContainer->compareLocales( $strKey, $strLocaleCode ) && $objLocaleContainer->compareLocaleLanguages( $strKey, $strLocaleCode ) )
					? $strKey : NULL;
			} );

		$strMachineTranslated = NULL;
		if( true == CLocaleContainer::createService()->getAcceptMachineTranslated() && true == isset( $arrTranslated[$strLocaleCode . '.machine'] ) && true == isset( $arrTranslated[$strLocaleCode . '.machine'][$strKeyName] ) ) {
			$strMachineTranslated = $arrTranslated[$strLocaleCode . '.machine'][$strKeyName];
		}

		$arrmixCallbacks = [
			'valid' =>
				function( $mixVal ) {
					switch( gettype( $mixVal ) ) {
						case 'NULL':
							return false;
						case 'array':
							return !empty( $mixVal );
						case 'string':
							return ( $mixVal != '' );
						default:
							return true;
					}
				},
			'normalize' =>
				function( $mixVal ) {
					return is_string( $mixVal ) ? trim( $mixVal ) : $mixVal;
				}
		];

		if( $boolIsDiscreetField ) {
			$strTranslated = coalesceCallbacks( $arrmixCallbacks,
				$arrTranslated[$strLocaleCode][$strKeyName] ?? NULL,
				$strMachineTranslated,
				$arrTranslated[$strFuzzyLocaleCode][$strKeyName] ?? NULL,
				( false == $boolIsSystemTranslated || CLocale::DEFAULT_LOCALE != $strDefaultLocale ) ? ( $arrTranslated[$strDefaultLocale][$strKeyName] ?? NULL ) : NULL,
				$this->$strName );
		} else {
			$strTranslated = coalesceCallbacks( $arrmixCallbacks,
				$arrTranslated[$strLocaleCode][$strKeyName] ?? NULL,
				$arrTranslated[$strFuzzyLocaleCode][$strKeyName] ?? NULL,
				$arrDetails[$strKeyName] ?? NULL );
		}

		return json_decode( json_encode( $strTranslated ) );
	}

	final private function setTranslated( $strName, $strValue, $strLocaleCode = NULL, $boolIsDiscreetField = true ) {
		$objLocaleContainer = CLocaleContainer::createService();
		$strDefaultLocale = $objLocaleContainer->getDefaultLocaleCode();
		$strLocaleCode = $strLocaleCode ?? $objLocaleContainer->getTargetLocaleCode() ?? $objLocaleContainer ->getLocaleCode();
		$arrDetails = json_decode( json_encode( $this->getDetails() ?? new stdClass() ), true );
		$strKeyName = $boolIsDiscreetField ? \Psi\Libraries\UtilStrings\CStrings::createService()->inflectUnderscore( preg_replace( '/^m_[a-z]+([a-zA-Z0-9]+)$/', '$1', $strName ) ) : $strName;
		$arrTranslated = $arrDetails['_translated'] ?? [];
		$arrTranslated[$strLocaleCode][$strKeyName] = $strValue;

		/*
		 TEMPORAILY DISABLE DYNAMIC DATA MOCKING (FURTHER TESTING REQUIRED)
		 // If mocking cookie enabled, don't allow setting anything (otherwise we'll persist the β and ξ junk values)
		 if( !empty( $_COOKIE['I18N_MOCK'] ) && is_numeric( $_COOKIE['I18N_MOCK'] ) && '0' != ( $_COOKIE['I18N_MOCK'] ) ) {
		 	return;
		 }
		 */

		if( $boolIsDiscreetField ) {
			if( $strLocaleCode == $strDefaultLocale || empty( $this->$strName ) ) {
				$this->set( $strName, $strValue );
				$arrTranslated['field_locales'][$strKeyName] = $strLocaleCode;
			}
		} else {
			if( $strLocaleCode == $strDefaultLocale || empty( $arrDetails[$strKeyName] ) ) {
				$arrDetails[$strKeyName] = $strValue;
				$arrTranslated['field_locales'][$strKeyName] = $strLocaleCode;
			}
		}

		$arrTranslated['default_locale'] = $strDefaultLocale;
		$arrDetails['_translated'] = $arrTranslated;
		$this->setDetails( json_decode( json_encode( $arrDetails ) ) );
	}

}
?>