<?php
trait TEosDetails {

	use TEosUserDefinedFields;

	public function __call( $strMethod, $arrmixArguments ) {
		$strKey = \Psi\Libraries\UtilStrings\CStrings::createService()->inflectUnderscore( \Psi\CStringService::singleton()->substr( $strMethod, 3 ) );

		if( 0 === \Psi\CStringService::singleton()->strpos( $strMethod, 'get' ) ) {
			return $this->getDetailsField( $strKey );
		} elseif( 0 === \Psi\CStringService::singleton()->strpos( $strMethod, 'set' ) ) {
			$this->setDetailsField( $strKey, $arrmixArguments[0] );
		}
	}

	final public function getObjectPath( $objInput, $mixPath = [] ) {
		if( empty( $mixPath ) ) return $objInput;
		if( !is_array( $mixPath ) ) $mixPath = [ $mixPath ];

		$objTemp = $objInput;
		foreach( $mixPath as $strKey ) {
			if( is_object( $objTemp ) && property_exists( $objTemp, $strKey ) ) {
				$objTemp = $objTemp->$strKey;
			} else if( is_array( $objTemp ) && array_key_exists( $strKey, $objTemp ) ) {
				$objTemp = $objTemp[$strKey];
			} else {
				return NULL;
			}
		}

		return $objTemp;
	}

	final public function setObjectPath( &$objInput, $mixPath = [], $mixValue = NULL ) {
		if( !is_array( $mixPath ) ) $mixPath = [ $mixPath ];
		$objTemp = &$objInput;

		foreach( $mixPath as $strKey ) {
			if( is_string( $strKey ) ) {
				if( !is_object( $objTemp ) ) $objTemp = new stdClass();
				$objTemp = &$objTemp->$strKey;
			} else if( is_integer( $strKey ) ) {
				if( !is_array( $objTemp ) ) $objTemp = [];
				$objTemp = &$objTemp[$strKey];
			} else {
				return false;
			}

		}

		$objTemp = $mixValue;
		return true;
	}

	/**
	 * Get value at specified path from details object tree (stdClass)
	 *  Examples:
	 *   $this->getDetailsField( 'fookey' );
	 *   $this->getDetailsField( [ '_translated', 'es_MX', 'description' ] );
	 *   $this->getDetailsField( 'user_defined_fields' );
	 *   $this->getDetailsField( [ '_postal_addresses', 'bank', 'address_line1' ] );
	 *   $this->getDetailsField( [ '_postal_addresses', 'bank' ] );
	 *   $this->getDetailsField( [ '_postal_addresses' ] );
	 *   $this->getDetailsField( [ 'colors', 0 ] ); // Get inner array value at given index
	 * @param array|string|integer $mixPath | Specifies the path of nested property names *or* array indexes which navigate to desired value
	 * @return mixed | Return value may be scalar or a subobject (stdClass)
	 */
	final public function getDetailsField( $mixPath ) {
		$jsonDetails = $this->getDetails() ?? new stdClass();
		$mixValue = $this->getObjectPath( $jsonDetails, $mixPath );
		return $mixValue;
	}

	/**
	 * Set the given value into the specified path. Nonexisting paths will be created automatically. Array input will be deep cast to an object tree (stdClass)
	 * Examples:
	 *  $this->setDetailsField( 'fookey', 'barval' );
	 *  $this->setDetailsField( [ '_translated', 'es_MX', 'description' ], 'Spanish description' );
	 *  $this->setDetailsField( 'user_defined_fields', [ 'color' => 'green', 'season' => 'winter' ] );
	 *  $this->setDetailsField( [ '_postal_addresses', 'bank', 'address_line1' ], '123 Sesame St.' );
	 *  $this->setDetailsField( [ '_postal_addresses', 'bank' ], [ 'address_line1 => '123 Sesame St.', 'city' => 'Rexburg', 'state' => 'ID', postal_code => '83440' ] );
	 *  $this->setDetailsField( [ '_postal_addresses' ], [ [ 'address_line1 => '123 Sesame St.', 'city' => 'Rexburg', 'state' => 'ID', postal_code => '83440' ], [ 'address_line1 => '123 Sesame St.', 'city' => 'Rexburg', 'state' => 'ID', postal_code => '83440' ] ] );
	 *  $this->setDetailsField( [ 'colors', 0 ], 'red' ); // Set inner array value at given index
	 * @param array|string|integer $mixPath | Specifies the path of nested property names *or* array indexes which navigate to desired value
	 * @param mixed $mixValue | Value to set
	 */
	final public function setDetailsField( $mixPath, $mixValue ) {
		$jsonDetails = $this->getDetails() ?? new stdClass();
		$mixValue = is_array( $mixValue ) ? json_decode( json_encode( $mixValue ) ) : $mixValue;
		$this->setObjectPath( $jsonDetails, $mixPath, $mixValue );
		$this->setDetails( $jsonDetails );
	}

	final public function getDetailsKeys( $mixPath ) {
		$jsonSubDetails = $this->getDetailsField( $mixPath );
		return is_object( $jsonSubDetails )
			? array_keys( get_object_vars( $jsonSubDetails ) )
			: NULL;
	}

	public function setDetails( $jsonDetails ) {
		if( true == valObj( $jsonDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonDetails', $jsonDetails, true );
		} elseif( true == valJsonString( $jsonDetails ) ) {
			$this->set( 'm_jsonDetails', CStrings::strToJson( $jsonDetails ), true );
		} else {
			$this->set( 'm_jsonDetails', NULL, true );
		}
		unset( $this->m_strDetails );
	}

	public function getDetails() {
		if( true == isset( $this->m_strDetails ) ) {
			$this->m_jsonDetails = CStrings::strToJson( $this->m_strDetails );
			unset( $this->m_strDetails );
		}
		return $this->m_jsonDetails;
	}

	public function sqlDetails() {
		$strDetails = CStrings::jsonToStrDef( $this->getDetails() );
		if( false == is_null( $strDetails ) ) {
			return ( ( $this->getDatabase() && $this->getDatabase()->getIsOpen() ) ? pg_escape_literal( $this->getDatabase()->getHandle(), $strDetails ) : '\'' . addslashes( $strDetails ) . '\'' );
		}
		return 'NULL';
	}

}
?>