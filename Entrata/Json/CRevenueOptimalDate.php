<?php

class CRevenueOptimalDate implements IEosJsonDetail {

	protected $m_strMoveInDate;
	protected $m_arrobjRevenueOptimalLeaseTerms;

	public function __construct( $strMoveInDate, $jsonRevenueOptimalLeaseTerms ) {
		$this->m_strMoveInDate = $strMoveInDate;
		foreach( $jsonRevenueOptimalLeaseTerms as $intLeaseTerm => $jsonReveueOptimalRates ) {
			$this->m_arrobjRevenueOptimalLeaseTerms[] = new CRevenueOptimalLeaseTerm( $intLeaseTerm, $jsonReveueOptimalRates );
		}
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getRevenueOptimalLeaseTerms() {
		return $this->m_arrobjRevenueOptimalLeaseTerms;
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->m_strMoveInDate = $strMoveInDate;
	}

	public function setRevenueOptimalLeaseTerms( $arrobjRevenueOptimalLeaseTerms ) {
		$this->m_arrobjRevenueOptimalLeaseTerms = $arrobjRevenueOptimalLeaseTerms;
	}

	public function toArray() {
		$arrmixOptimalLeaseTerms = [];

		foreach( $this->m_arrobjRevenueOptimalLeaseTerms as $objRevenueOptimalLeaseTerm ) {
			$arrmixOptimalLeaseTerms += $objRevenueOptimalLeaseTerm->toArray();
		}

		return [ $this->m_strMoveInDate => $arrmixOptimalLeaseTerms ];
	}

}