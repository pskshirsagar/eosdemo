<?php

class CRevenueOptimalLeaseTerm implements IEosJsonDetail {

	protected $m_intLeaseTermId;
	protected $m_objRevenueOptimalRate;

	public function __construct( $intLeaseTermId, $jsonRevenueOptimalRate ) {
		$this->m_intLeaseTermId = $intLeaseTermId;
		$this->m_objRevenueOptimalRate = new CRevenueOptimalRate( $jsonRevenueOptimalRate );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function getRevenueOptimalRate() {
		return $this->m_objRevenueOptimalRate;
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = $intLeaseTermId;
	}

	public function setRevenueOptimalRate( $objRevenueOptimalRate ) {
		$this->m_objRevenueOptimalRate = $objRevenueOptimalRate;
	}

	public function toArray() {
		return [ $this->m_intLeaseTermId => $this->m_objRevenueOptimalRate->toArray() ];
	}

}