<?php

class CFilterOptions implements IEosJsonDetail {

	protected $m_arrmixFilterOptions;
	protected $m_strFilterKey;

	public function __construct( string $strFilterKey, array $arrmixFilterOptions ) {
		$this->m_strFilterKey = $strFilterKey;
		$this->m_arrmixFilterOptions = $arrmixFilterOptions;
	}

	public function toArray() : array {
		return [ $this->m_strFilterKey => $this->m_arrmixFilterOptions ];
	}

	public function toJson() : string {
		return json_encode( $this->m_arrmixFilterOptions );
	}

	public function getFilterOptions() : array {
		return $this->m_arrmixFilterOptions;
	}

	public function getFilterKey() : string {
		return $this->m_strFilterKey;
	}
}