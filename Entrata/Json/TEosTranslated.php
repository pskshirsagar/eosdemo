<?php
trait TEosTranslated {

	private $m_strTranslatedKey = '_translated';
	private $m_strFieldLocalesKey = 'field_locales';

	final private function getTranslated( $strName, $strLocaleCode = NULL, $boolIsDiscreetField = true ) {
		$strDefaultLocale = CLocaleContainer::createService()->getDefaultLocaleCode();
		list( $boolIsSystemTranslated, $strLocaleCode ) = $this->calcTranslatedContext( $strLocaleCode );
		$strKeyName = $this->getKeyForPropertyName( $strName, $boolIsDiscreetField );
		$strFuzzyLocaleCode = ( CLocale::DEFAULT_LOCALE == $strLocaleCode && $strDefaultLocale == $strLocaleCode ) ? NULL : CLocaleContainer::createService()->findFuzzyLocale( $this->getDetailsKeys( $this->m_strTranslatedKey ), $strLocaleCode );
		$strAliasLocaleCode = NULL;

		if( $strLocaleCode == CLocaleContainer::createService()->getLocaleCode() ) {
			$strAliasLocaleCode = CLocaleContainer::createService()->getAliasLocaleCode();
		}

		if( $boolIsDiscreetField ) {
			$strTranslated = coalesceCallbacks( $this->getCoalesceCallbacks(), NULL,
				( $boolIsSystemTranslated && CLocale::DEFAULT_LOCALE == $strLocaleCode ) ? $this->$strName : NULL,
				$this->getDetailsField( [ $this->m_strTranslatedKey, $strLocaleCode, $strKeyName ] ),
				CLocaleContainer::createService()->getAcceptMachineTranslated()
					? $this->getDetailsField( [ $this->m_strTranslatedKey, $strLocaleCode . '.machine', $strKeyName ] ) : NULL,
				$this->getDetailsField( [ $this->m_strTranslatedKey, $strAliasLocaleCode, $strKeyName ] ),
				$this->getDetailsField( [ $this->m_strTranslatedKey, $strFuzzyLocaleCode, $strKeyName ] ),
				$this->getDetailsField( [ $this->m_strTranslatedKey, $strDefaultLocale, $strKeyName ] ),
				$this->$strName );
		} else {
			$strTranslated = coalesceCallbacks( $this->getCoalesceCallbacks(), NULL,
				$this->getDetailsField( [ $this->m_strTranslatedKey, $strLocaleCode, $strKeyName ] ),
				$this->getDetailsField( [ $this->m_strTranslatedKey, $strAliasLocaleCode, $strKeyName ] ),
				$this->getDetailsField( [ $this->m_strTranslatedKey, $strFuzzyLocaleCode, $strKeyName ] ),
				$this->getDetailsField( $strKeyName ) );
		}

		return $strTranslated;
	}

	final private function setTranslated( $strName, $strValue, $strLocaleCode = NULL, $boolIsDiscreetField = true ) {
		$strDefaultLocale = CLocaleContainer::createService()->getDefaultLocaleCode();
		list( $boolIsSystemTranslated, $strLocaleCode ) = $this->calcTranslatedContext( $strLocaleCode );
		$strKeyName = $this->getKeyForPropertyName( $strName, $boolIsDiscreetField );
		$this->setDetailsField( [ $this->m_strTranslatedKey, $strLocaleCode, $strKeyName ], $strValue );

		if( $boolIsDiscreetField ) {
			if( $strLocaleCode == $strDefaultLocale || empty( $this->$strName ) ) {
				$this->set( $strName, $strValue );
				$this->setDetailsField( [ $this->m_strTranslatedKey, $this->m_strFieldLocalesKey, $strKeyName ], $strLocaleCode );
			}
		} else {
			if( $strLocaleCode == $strDefaultLocale || empty( $this->getDetailsField( $strKeyName ) ) ) {
				$this->setDetailsField( $strKeyName, $strValue );
				$this->setDetailsField( [ $this->m_strTranslatedKey, $this->m_strFieldLocalesKey, $strKeyName ], $strLocaleCode );
			}
		}

		$this->setDetailsField( [ $this->m_strTranslatedKey, 'default_locale' ], $strDefaultLocale );
	}

	final private function calcTranslatedContext( $strLocaleCode ) {
		if( true == method_exists( $this, 'getIsSystemTranslated' ) && true == $this->getIsSystemTranslated() ) {
			$boolIsSystemTranslated = true;
			$strLocaleCode = $strLocaleCode ?? CLocaleContainer::createService()->getLocaleCode();
		} else {
			$boolIsSystemTranslated = false;
			$strLocaleCode = $strLocaleCode ?? CLocaleContainer::createService()->getTargetLocaleCode() ?? CLocaleContainer::createService()->getLocaleCode();
		}
		return [ $boolIsSystemTranslated, $strLocaleCode ];
	}

	final private function getKeyForPropertyName( $strPropertyName, $boolIsDiscreetField = true ) {
		return $boolIsDiscreetField
			? \Psi\Libraries\UtilStrings\CStrings::createService()->inflectUnderscore( preg_replace( '/^m_[a-z]+([a-zA-Z0-9]+)$/', '$1', $strPropertyName ) )
			: $strPropertyName;
	}

	final private function getCoalesceCallbacks() {
		return [
			'valid' =>
				function( $mixVal ) {
					switch( gettype( $mixVal ) ) {
						case 'NULL':
							return false;
						case 'object':
							return !empty( $mixVal );
						case 'string':
							return ( $mixVal != '' );
						default:
							return true;
					}
				},
			'normalize' =>
				function( $mixVal ) {
					return is_string( $mixVal ) ? trim( $mixVal ) : $mixVal;
				}
		];
	}
}
?>