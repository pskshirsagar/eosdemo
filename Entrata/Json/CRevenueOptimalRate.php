<?php

class CRevenueOptimalRate implements IEosJsonDetail {

	protected $m_strWindowStartDate;
	protected $m_strWindowEndDate;
	protected $m_intRent;

	public function __construct( $jsonRevenueOptimalRate ) {
		$this->m_strWindowStartDate     = $jsonRevenueOptimalRate[0];
		$this->m_strWindowEndDate       = $jsonRevenueOptimalRate[1];
		$this->m_intRent                = $jsonRevenueOptimalRate[2];
	}

	public function getWindowStartDate() {
		return $this->m_strWindowStartDate;
	}

	public function getWindowEndDate() {
		return $this->m_strWindowEndDate;
	}

	public function getRent() {
		return $this->m_intRent;
	}

	public function setWindowStartDate( $strWindowStartDate ) {
		$this->m_strWindowStartDate = $strWindowStartDate;
	}

	public function setWindowEndDate( $strWindowEndDate ) {
		$this->m_strWindowEndDate = $strWindowEndDate;
	}

	public function setRent( $intRent ) {
		$this->m_intRent = $intRent;
	}

	public function toArray() {
		return [ $this->m_strWindowStartDate, $this->m_strWindowEndDate, $this->m_intRent ];
	}

}