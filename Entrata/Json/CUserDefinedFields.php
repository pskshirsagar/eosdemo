<?php
class CUserDefinedFields implements IJsonable {

	protected $m_arrmixCollection;

	public function __construct( $arrmixParameters = [] ) {
		$this->m_arrmixCollection = [];
		foreach( $arrmixParameters as $strKey => $strValue ) {
			$this->m_arrmixCollection[$strKey] = $strValue;
		}
	}

	public function __call( $strMethod, $arrmixArguments ) {
		if( 0 !== \Psi\CStringService::singleton()->strpos( $strMethod, 'get' ) && 0 !== \Psi\CStringService::singleton()->strpos( $strMethod, 'set' ) ) {
			trigger_error( 'Call to undefined method ' . $strMethod, E_USER_ERROR );
		}

		$strKey = \Psi\Libraries\UtilStrings\CStrings::createService()->inflectUnderscore( \Psi\CStringService::singleton()->substr( $strMethod, 3 ) );

		if( 0 === \Psi\CStringService::singleton()->strpos( $strMethod, 'get' ) ) {
			return $this->getField( $strKey );
		} elseif( 0 === \Psi\CStringService::singleton()->strpos( $strMethod, 'set' ) ) {
			return $this->setField( $strKey, $arrmixArguments[0] );
		}
	}

	private function getField( $strKey ) {

		return true == array_key_exists( $strKey, $this->m_arrmixCollection ) ? $this->m_arrmixCollection[$strKey] : NULL;
	}

	private function setField( $strKey, $strValue ) {
		$this->m_arrmixCollection[$strKey] = $strValue;
	}

	public function toJson() {
		return ( object ) $this->m_arrmixCollection;
	}

	public function toString( $arrmixOptions = 0 ) {
		return json_encode( $this->m_arrmixCollection, $arrmixOptions );
	}

}
?>