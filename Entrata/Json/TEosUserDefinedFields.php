<?php
trait TEosUserDefinedFields {

	private $m_strUdfKey = 'user_defined_fields';
	protected $m_objUserDefinedFields;

	public function cloneUDF() {
		if( true == valObj( $this->m_objUserDefinedFields, 'CUserDefinedFields' ) ) {
			$this->m_objUserDefinedFields = clone $this->m_objUserDefinedFields;
		}
	}

	public function getUserDefinedFields() {
		if( false == valObj( $this->m_objUserDefinedFields, 'CUserDefinedFields' ) && valObj( $this->getDetails(), 'stdClass' ) ) {
			$this->m_objUserDefinedFields = new CUserDefinedFields( ( array ) $this->getDetailsField( $this->m_strUdfKey ) );
		} elseif( false == valObj( $this->m_objUserDefinedFields, 'CUserDefinedFields' ) ) {
			$this->m_objUserDefinedFields = new CUserDefinedFields();
		}
		return $this->m_objUserDefinedFields;
	}

	public function setUserDefinedFields( $objUserDefinedFields ) {
		$this->m_objUserDefinedFields = $objUserDefinedFields;
		$this->setDetailsField( $this->m_strUdfKey, $this->m_objUserDefinedFields->toJson() );
	}

}
?>