<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerVehicleTypes
 * Do not add any new functions to this class.
 */

class CCustomerVehicleTypes extends CBaseCustomerVehicleTypes {

	public static function fetchCustomerVehicleTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCustomerVehicleType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCustomerVehicleType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCustomerVehicleType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCustomerVehiclesByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						customer_id,
						(make::text ||\'-\'|| model::text ) as make_model,
						year,
						color,
						license_plate_number,
						customer_vehicle_type_id
				   FROM
						customer_vehicles 
				   WHERE customer_id IN (' . implode( ',', $arrintCustomerIds ) . ') AND cid = ' . ( int ) $intCid .
				   ' ORDER BY
						make_model';
		return fetchData( $strSql, $objDatabase );
	}

}
?>