<?php

class CMimeType extends CBaseMimeType {

	const MIME_TYPE_JPG 	= 1;
	const MIME_TYPE_PNG 	= 2;
	const MIME_TYPE_GIF 	= 3;
	const MIME_TYPE_SWF 	= 4;
	const MIME_TYPE_XLS 	= 5;
	const MIME_TYPE_MOV 	= 6;
	const MIME_TYPE_MP3 	= 7;
	const MIME_TYPE_WMA 	= 8;
	const MIME_TYPE_MP4 	= 9;
	const MIME_TYPE_MPEG 	= 10;
	const MIME_TYPE_3GP 	= 11;
	const MIME_TYPE_WAV 	= 12;
	const MIME_TYPE_AVI 	= 13;
	const MIME_TYPE_FLV 	= 14;
	const MIME_TYPE_WMV 	= 15;
	const MIME_TYPE_PDF 	= 16;
	const MIME_TYPE_PPT		= 17;
	const MIME_TYPE_DOC 	= 18;
	const MIME_TYPE_ICO 	= 19;
	const MIME_TYPE_CSS 	= 20;
	const MIME_TYPE_XLSX 	= 21;
	const MIME_TYPE_DOCX 	= 22;
	const MIME_TYPE_VCF 	= 23;
	const MIME_TYPE_IVR		= 24;
}
?>