<?php

class CApplicationSettingKey extends CBaseApplicationSettingKey {

	const HOUSEHOLD_INCOME	= 9;
	const TRANSFERS         = 12;

	public static $c_arrintIncomeKeyIds = [ 9 ];
	public static $c_arrintIncomeDefaultMergeFieldId = [ 'HOUSEHOLD_INCOME' => 2832 ];
	public static $c_arrstrPHAKeys 	= [ 'PREVIOUS_HOUSING_ARRANGEMENT_WITHOUT_HOUSING', 'PREVIOUS_HOUSING_ARRANGEMENT_FLEEING_VIOLENCE', 'PREVIOUS_HOUSING_ARRANGEMENT_NO_NIGHTTME_RESIDENCE' ];
	public static $c_arrstrDSKeys 	= [ 'DISPLACEMENT_STATUS_NATURAL_DISASTER', 'DISPLACEMENT_STATUS_GOVERNMENT_ACTION' ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationSettingGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToolTipId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTableName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valColumnName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>