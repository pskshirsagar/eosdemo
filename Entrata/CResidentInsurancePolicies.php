<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResidentInsurancePolicies
 * Do not add any new functions to this class.
 */

class CResidentInsurancePolicies extends CBaseResidentInsurancePolicies {

	public static function fetchResidentInsurancePoliciesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*,
						c.name_first,
						c.name_last
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						JOIN customers c ON ( c.id = ipc.customer_id AND c.cid = ipc.cid )
						LEFT JOIN property_preferences pp ON ( pp.cid = ipc.cid AND pp.property_id = ipc.property_id AND pp.key = \'' . \CPropertyPreference::PAID_VERIFICATION . '\' AND pp.value = \'1\' )
				   WHERE
						ipc.lease_id = ' . ( int ) $intLeaseId . '
						AND ( pp.id IS NULL OR ( ( rip.confirmed_on IS NOT NULL AND rip.insurance_policy_type_id = ' . \CInsurancePolicyType::CUSTOM . ' ) OR rip.insurance_policy_type_id <> ' . \CInsurancePolicyType::CUSTOM . ' ) )
						AND ipc.cid = ' . ( int ) $intCid;

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchCustomResidentInsurancePoliciesByInsurancePolicyIdsByCid( $arrintInsurancePolicyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintInsurancePolicyIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM resident_insurance_policies WHERE insurance_policy_id IN (' . implode( ',', $arrintInsurancePolicyIds ) . ') AND cid = ' . ( int ) $intCid;

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchNotVerifiedPoliciesCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) {
			return 0;
		}

		$arrintLeaseStatusTypes = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ];

		$arrintInsuracePolicyStatusTypeIds = [ CInsurancePolicyStatusType::ACTIVE, CInsurancePolicyStatusType::FUTURE_COVERAGE ];

		$strSql = 'SELECT
					   	COUNT( DISTINCT( ipc.id ) ) as not_verified_count
					FROM
						insurance_policy_customers ipc
					   	JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND ipc.cid = rip.cid )
					   	JOIN customers c ON ( ipc.customer_id = c.id AND ipc.cid = c.cid )
					   	JOIN leases l ON ( l.id = ipc.lease_id AND ipc.cid = l.cid )
						JOIN lease_customers lc ON ( l.cid = lc.cid AND l.id = lc.lease_id AND lc.customer_id = l.primary_customer_id )
					   	JOIN properties p ON ( ipc.property_id = p.id AND ipc.cid = p.cid )
					WHERE
						ipc.cid = ' . ( int ) $intCid . '
					   	AND ipc.customer_id IS NOT NULL
					   	AND	ipc.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
					   	AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsuracePolicyStatusTypeIds ) . ' )
					   	AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
					   	AND (rip.confirmed_on IS NULL)
					   	AND rip.is_integrated = ' . '0';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrFinalData = [];

		if( true == valArr( $arrstrData ) ) {
			foreach( $arrstrData as $arrstrChunk ) {
				$arrstrFinalData[] = $arrstrChunk['not_verified_count'];
			}
		}

		return $arrstrFinalData;
	}

	public static function fetchResidentInsurancePolicyByLeaseIdByInsurancePolicyIdByPropertyIdByCid( $intLeaseId, $intInsurancePolicyId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intInsurancePolicyId ) || false == is_numeric( $intCid ) || false == is_numeric( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						 resident_insurance_policies rip
						 JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						 ipc.lease_id = ' . ( int ) $intLeaseId . '
						 AND rip.cid = ' . ( int ) $intCid . '
						 AND rip.property_id = ' . ( int ) $intPropertyId . '
						 AND rip.insurance_policy_id = ' . ( int ) $intInsurancePolicyId;

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByCidByInsurancePolicyId( $intCid, $intInsurancePolicyId, $objDatabase ) {
		if( false == is_numeric( $intInsurancePolicyId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.insurance_policy_id = ' . ( int ) $intInsurancePolicyId;

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByPropertyIdByCidByInsurancePolicyId( $intPropertyId, $intCid, $intInsurancePolicyId, $objDatabase ) {
		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intInsurancePolicyId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
					WHERE
						rip.property_id = ' . ( int ) $intPropertyId . '
						AND rip.cid = ' . ( int ) $intCid . '
						AND rip.insurance_policy_id = ' . ( int ) $intInsurancePolicyId;

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByApplicationIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intApplicantId ) || false == is_numeric( $intApplicationId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						ipc.application_id = ' . ( int ) $intApplicationId . '
						AND	ipc.applicant_id = ' . ( int ) $intApplicantId . '
						AND ipc.cid = ' . ( int ) $intCid . '
					ORDER BY rip.id DESC LIMIT 1';

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByIdsByCid( $arrintResidentInsurancePolicyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintResidentInsurancePolicyIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
					WHERE
						 rip.id in (' . implode( ',', $arrintResidentInsurancePolicyIds ) . ' )
					 	 AND rip.cid = ' . ( int ) $intCid;

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchCustomResidentInsurancePoliciesByIdsByLeaseIdsByCid( $arrintResidentInsurancePolicyIds, $arrintLeaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintResidentInsurancePolicyIds ) || false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.id IN(' . implode( ',', $arrintResidentInsurancePolicyIds ) . ' )
					 	AND ipc.lease_id IN(' . implode( ',', $arrintLeaseIds ) . ' )';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchUnMatchedResidentInsurancePoliciesMatchTabCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == is_numeric( $intCid ) || false == valArr( $arrintPropertyIds ) ) {
			return 0;
		}

		$strSql = 'SELECT
						count(rip.id)
					FROM
						resident_insurance_policies rip
					WHERE
						rip.id NOT IN (SELECT
								 			resident_insurance_policy_id
						    			FROM
						    				insurance_policy_customers ipc
						    			WHERE ipc.resident_insurance_policy_id IS NOT NULL
											  AND ipc.cid = ' . ( int ) $intCid . ' )
						AND rip.cid = ' . ( int ) $intCid . '
						AND rip.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND rip.effective_date <= ( NOW() + \' 60 days\' )
						AND ( NOW()::DATE <= rip.end_date OR end_date IS NULL )
						AND rip.confirmed_on IS NOT NULL
						AND rip.is_integrated = 1
						AND rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ', ' . CInsurancePolicyStatusType::CANCEL_PENDING . ', ' . CInsurancePolicyStatusType::ENROLLED . ' )';

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchUnMatchedResidentInsurancePoliciesInvalidMatchMatchTabCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == is_numeric( $intCid ) || false == valArr( $arrintPropertyIds ) ) {
			return 0;
		}

		$strSql = 'SELECT
						count( DISTINCT( ipc.resident_insurance_policy_id ) )
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id
																 AND rip.cid = ipc.cid
																 AND ipc.is_invalid_match = 1
																 AND ( SELECT count(*)
																       FROM insurance_policy_customers ipc
																       WHERE ipc.resident_insurance_policy_id = rip.id
																	   AND ipc.cid = rip.cid
																	   AND ipc.cid = ' . ( int ) $intCid . '
																       AND ipc.is_invalid_match = 0 ) = 0 )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND rip.effective_date <= ( NOW() + \' 60 days\' )
						AND ( NOW()::DATE <= rip.end_date OR end_date IS NULL )
						AND rip.confirmed_on IS NOT NULL
						AND rip.is_integrated = 1';

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchResidentInsurancePoliciesByApplicationsIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
    					rip.*
    				FROM
    					resident_insurance_policies rip
    					JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
    				WHERE
    					ipc.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
    					AND ipc.cid = ' . ( int ) $intCid;

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchMatchResidentInsurancePoliciesByApplicationsIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
    					rip.*
    				FROM
    					resident_insurance_policies rip
    					JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
    				WHERE
    					ipc.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
    					AND ipc.cid = ' . ( int ) $intCid . '
    					AND ipc.is_invalid_match <> 1';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public function fetchLatestResidentInsurancePoliciesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $intCustomerId = NULL, $strSource = NULL, $boolIsExcludeExceptionPolicy = false ) {

		$strSubSql                   = ( false == is_null( $intCustomerId ) ) ? ' AND	ipc.customer_id = ' . ( int ) $intCustomerId : '';
		$strSqlIsRequiredColumnsData = ( true == valStr( $strSource ) && 'dashboard' === $strSource ) ? ' rip.id, rip.end_date, rip.insurance_policy_status_type_id, rip.insurance_carrier_name ' : ' rip.* ';
		$strExcludeExceptionPolicyCondition      = ( true == $boolIsExcludeExceptionPolicy ) ? ' AND rip.insurance_policy_type_id NOT IN ( ' . sqlIntImplode( CInsurancePolicyType::$c_arrintMasterAndAffordablePolicyTypes ) . ' ) ' : '';

		$strSql = 'SELECT
    					 ' . $strSqlIsRequiredColumnsData . '
    				FROM
    					resident_insurance_policies rip
    					JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
    				WHERE
    					ipc.cid = ' . ( int ) $intCid . '
    					AND ipc.lease_id = ' . ( int ) $intLeaseId . $strSubSql . '
    					AND ipc.is_invalid_match = 0
    					AND rip.replaced_insurance_policy_id IS NULL ' . $strExcludeExceptionPolicyCondition . '
    				ORDER BY
    					rip.id DESC';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchLatestResidentInsurancePoliciesCountByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $intCustomerId = NULL ) {

		$strSubSql = ( false == is_null( $intCustomerId ) ) ? ' AND	ipc.customer_id = ' . ( int ) $intCustomerId : '';

		$strSql = 'SELECT
    					count( rip.id )
    				FROM
    					resident_insurance_policies rip
    					JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
    				WHERE
    					ipc.cid = ' . ( int ) $intCid . '
    					AND ipc.lease_id = ' . ( int ) $intLeaseId . $strSubSql . '
    					AND ipc.is_invalid_match = 0
    					AND rip.replaced_insurance_policy_id IS NULL
    					AND rip.confirmed_on IS NULL';
		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchCustomUnmatchedResidentInsurancePolicies( $arrintCids, $objDatabase, $intInsurancePolicyId = NULL ) {

		$strAndCondition = '';

		if( true == valArr( $arrintCids ) ) {
			$strAndCondition 	= ' AND rip.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';
			$strIPCAndCondition = ' WHERE ipc.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';
		}

		if( false == is_null( $intInsurancePolicyId ) ) {
			$strAndCondition .= ' AND rip.insurance_policy_id = ' . ( int ) $intInsurancePolicyId;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
					WHERE
						rip.id NOT IN ( SELECT
											resident_insurance_policy_id
								    	FROM
											insurance_policy_customers ipc
								   			' . $strIPCAndCondition . '
								 		)
						AND ( NOW() <= rip.end_date OR rip.end_date IS NULL )
						AND rip.confirmed_on IS NOT NULL
						AND ( rip.effective_date::DATE >= NOW() OR rip.effective_date::DATE >= ( CURRENT_DATE - 10 )::DATE )
						AND rip.is_integrated = 1
						' . $strAndCondition . '
						AND rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ', ' . CInsurancePolicyStatusType::CANCEL_PENDING . ', ' . CInsurancePolicyStatusType::ENROLLED . ' ) ';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchResidentInsurancePolicyCountByLeaseIdByPolicyNumberExcludingResidentInsurancePolicyIdByCid( $intLeaseId, $strPolicyNumber, $intResidentInsurancePolicyId = NULL, $intCid, $objDatabase ) {
		if( false == is_numeric( $intLeaseId ) || true == is_null( $strPolicyNumber ) || false == is_numeric( $intCid ) ) {
			return 0;
		}

		$strSql = 'SELECT
						count( DISTINCT( ipc.resident_insurance_policy_id ) )
					FROM
					 	resident_insurance_policies rip
					 	JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					 WHERE
					 	ipc.lease_id = ' . ( int ) $intLeaseId . '
					 	AND ipc.cid = ' . ( int ) $intCid . '
					 	AND rip.policy_number ILIKE ( \'' . addslashes( $strPolicyNumber ) . '\' ) ';

		$strSql .= ( 0 < $intResidentInsurancePolicyId ) ? ' AND rip.id <> ' . ( int ) $intResidentInsurancePolicyId : '';

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchApplicantAndApplicationIdByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ca.id as application_id, aa.applicant_id
					FROM
						cached_applications ca
					JOIN
						applicant_applications aa ON ca.id = aa.application_id AND ca.cid = aa.cid ' . $strCheckDeletedAASql . '
					WHERE
				    	aa.customer_type_id = ' . CCustomerType::PRIMARY . '
				    	AND ca.cid = ' . ( int ) $intCid . '
						AND ca.lease_interval_type_id != ' . ( int ) CLeaseIntervalType::RENEWAL . '
						AND ca.lease_id = ' . ( int ) $intLeaseId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseIdsHavingReplacedInsurancePolicyIdIsNull( $arrintCidsToExclude, $objDatabase ) {
		$arrintInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::CANCELLED,
			CInsurancePolicyStatusType::EXPIRED,
			CInsurancePolicyStatusType::LAPSED,
			CInsurancePolicyStatusType::CANCEL_PENDING
		];

		$strRIPAndCondition = ( true == valArr( $arrintCidsToExclude ) ) ? ' AND rip.cid NOT IN ( ' . implode( ',', $arrintCidsToExclude ) . ' ) ' : '';
		$strIPCAndCondition = ( true == valArr( $arrintCidsToExclude ) ) ? ' AND rip.cid NOT IN ( ' . implode( ',', $arrintCidsToExclude ) . ' ) ' : '';

		$strSql = 'SELECT
				        rip.id as resident_insurance_policy_id,
						rip.cid,
	   	                ipc.lease_id,
						ipc.customer_id
	    		   FROM
						resident_insurance_policies rip
     					JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
				   WHERE
      					rip.insurance_policy_status_type_id = ' . CInsurancePolicyStatusType::ACTIVE . ' AND
      					ipc.is_invalid_match = 0 AND
						ipc.lease_id IN (
                        					Select
												 ipc.lease_id
                        					FROM
												resident_insurance_policies rip
     											JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
                        					WHERE
												rip.id = ipc.resident_insurance_policy_id
      											AND rip.cid = ipc.cid
                              					AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' ) ' . $strRIPAndCondition . '
                              					AND rip.replaced_insurance_policy_id IS NULL
												AND	ipc.is_invalid_match = 0
      										) ' . $strIPCAndCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByCidByLeaseIdByStatusTypes( $intCid, $intLeaseId, $arrintInsurancePolicyStatusTypes, $objDatabase ) {
		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
	               FROM
						resident_insurance_policies rip
        	            JOIN insurance_policy_customers ipc ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
	               WHERE
					   	rip.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrintInsurancePolicyStatusTypes ) ) {
			$strSql .= ' AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' )
						 AND rip.replaced_insurance_policy_id IS NULL';
		}

		$strSql .= ' AND ipc.lease_id = ' . ( int ) $intLeaseId . '
				     AND ipc.is_invalid_match = 0';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByInsurancePolicyStatusTypeIdsHavingReplaceInsuranceId( $arrintCids, $arrintInsurancePolicyStatusTypes, $objDatabase ) {
		if( false == valArr( $arrintInsurancePolicyStatusTypes ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
	            	FROM
						resident_insurance_policies rip
        	           	JOIN insurance_policy_customers ipc ON ( ipc.resident_insurance_policy_id = rip.id AND ipc.cid = rip.cid )
	              	WHERE
	                  	rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' )
	                  	AND rip.cid IN ( ' . implode( ',', $arrintCids ) . ' )
					  	AND rip.replaced_insurance_policy_id IS NOT NULL';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchCustomResidentInsurancePoliciesByCustomerIdsByPropertyIdsByInsurancePolicyStatusTypeIds( $intIsIntegrated, $arrintCustomerIds, $arrintPropertyIds, $arrintInsurancePolicyStatusTypeIds, $objDatabase, $arrintLeaseIds = NULL ) {
		if( false == valArr( $arrintCustomerIds ) || false == valArr( $arrintInsurancePolicyStatusTypeIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strAndCondition = ' AND ipc.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) ';

		if( true == valArr( $arrintLeaseIds ) ) {
			$strAndCondition = ' AND ipc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ';
		}

		$strSql = 'SELECT
    				  	ipc.customer_id,
    				  	ipc.lease_id
    			   	FROM
    			  		resident_insurance_policies rip
						JOIN clients mc ON ( rip.cid = mc.id )
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						JOIN lease_customers lc ON( lc.lease_id = ipc.lease_id AND lc.cid = ipc.cid AND lc.lease_status_type_id IN ( ' . CLeaseStatusType::NOTICE . ' , ' . CLeaseStatusType::CURRENT . ' ) )
    			   	WHERE
	    			    rip.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
	    			    AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' )
	    			    AND rip.is_integrated = ' . ( int ) $intIsIntegrated . $strAndCondition;

		$arrstrData = fetchData( $strSql, $objDatabase );

		$arrstrFinalData = [];

		if( true == valArr( $arrstrData ) ) {
			foreach( $arrstrData as $arrstrChunk ) {
				$arrstrFinalData[$arrstrChunk['customer_id']] = $arrstrChunk['customer_id'];
				$arrstrFinalData[$arrstrChunk['lease_id']]    = $arrstrChunk['lease_id'];
			}
		}

		return $arrstrFinalData;
	}

	public static function fetchResidentInsurancePolicyDetailByIdsByCid( $arrintResidentInsurancePolicyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintResidentInsurancePolicyIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.id,
						rip.insurance_carrier_name,
						rip.policy_number,
						rip.effective_date,
						rip.end_date,
						rip.liability_limit,
					    rip.personal_limit,
						rip.deductible,
						rip.proof_of_coverage_file_name,
						rip.additional_insureds,
						rip.is_integrated,
					    rip.confirmed_on,
						rip.property_unit_id,
						l.unit_number_cache as unit_number,
						c.name_first,
						c.name_last,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.country_code,
						pa.postal_code,
					    ua.street_line1 as unit_street_line1,
						ua.street_line2 as unit_street_line2,
						ua.city as unit_city,
						ua.state_code as unit_state_code,
						ua.country_code as unit_country_code,
						ua.postal_code as unit_postal_code
					FROM
						resident_insurance_policies rip
					    LEFT JOIN insurance_policy_customers ipc ON( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					    LEFT JOIN customers c ON ( ipc.customer_id = c.id AND ipc.cid = c.cid )
						LEFT JOIN cached_leases l ON ( ipc.lease_id = l.id AND ipc.cid = l.cid )
						LEFT JOIN property_addresses pa ON ( l.property_id = pa.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND l.cid = pa.cid AND pa.is_alternate = false )
						LEFT JOIN unit_addresses ua ON ( rip.property_unit_id = ua.property_unit_id AND ua.address_type_id = ' . CAddressType::PRIMARY . ' AND rip.cid = ua.cid )
					WHERE
						rip.id IN ( ' . implode( ',', $arrintResidentInsurancePolicyIds ) . ' )
						AND rip.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByReplacedInsurancePolicyIdByCid( $intReplacedInsurancePolicyId, $intCid, $objDatabase ) {
		return self::fetchResidentInsurancePolicies( sprintf( 'SELECT * FROM resident_insurance_policies WHERE replaced_insurance_policy_id = %d AND cid = %d', ( int ) $intReplacedInsurancePolicyId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByLeaseIdByCidByCustomerId( $intLeaseId, $intCid, $objDatabase, $intCustomerId ) {
		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strAndCustomerIdCondition = ( false == empty( $intCustomerId ) ) ? ' AND ipc.customer_id = ' . ( int ) $intCustomerId : '';

		$strSql = 'SELECT
						rip.*,  CASE
						WHEN insurance_carrier_name = \'Markel\' THEN \'RI-Markel\'
						WHEN insurance_carrier_name = \'Kemper\' THEN \'RI-Kemper\'
						WHEN insurance_carrier_name = \'QBE\' THEN \'RI-QBE\'
						ELSE insurance_carrier_name
						END as insurance_carrier_name
				   	FROM
				  	   resident_insurance_policies rip
					   JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
				   	WHERE ipc.is_invalid_match <> 1
						  AND ipc.lease_id = ' . ( int ) $intLeaseId . '
						  AND rip.cid = ' . ( int ) $intCid . '
						 ' . $strAndCustomerIdCondition . '
				   ORDER BY rip.id DESC LIMIT 1';

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	// Need to remove SQL post Unit vs Occupant Chnages ( task#1403472 )

	public static function fetchUninsuredResidentsByCidsByCustomerIds( $arrintCids, $arrintCustomerIds, $arrstrFrequencies, $objDatabase ) {

		$arrintInsurancePolicyStatusTypes = [ CInsurancePolicyStatusType::LAPSED, CInsurancePolicyStatusType::CANCELLED, CInsurancePolicyStatusType::EXPIRED ];
		$arrintLeaseStatusTypes           = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ];

		$strFrequencies = "'" . implode( "','", $arrstrFrequencies ) . "'";

		$strSql = 'SELECT
						ipc.resident_insurance_policy_id,ipc.is_invalid_match,l.id as lease_id,
						rip.id,rip.end_date,rip.insurance_policy_id,rip.insurance_carrier_name,rip.policy_number,rip.insurance_policy_status_type_id,rip.is_integrated,
						c.name_last,c.name_first,c.email_address,c.phone_number,c.id as customer_id,
						lc.lease_status_type_id,
						l.cid,l.property_id,
					    l.unit_space_id as unit_space_number,
						l.unit_number_cache as unit_number
					FROM
						cached_leases l
						JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.customer_id = l.primary_customer_id  )
				    	JOIN property_products pp ON ( pp.property_id = l.property_id AND pp.cid = l.cid AND pp.ps_product_id = ' . CPsProduct::RESIDENT_INSURE . ' )
				    	LEFT JOIN insurance_policy_customers ipc ON ( l.id = ipc.lease_id AND l.cid = ipc.cid )
						LEFT JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
						LEFT JOIN customers c ON ( l.primary_customer_id = c.id AND l.cid = c.cid  )
						LEFT JOIN properties p ON ( l.property_id = p.id AND l.cid = p.cid )
				    	LEFT JOIN property_preferences ppr ON ( l.cid = ppr.cid AND l.property_id = ppr.property_id AND ppr.key = \'REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS_EFFECTIVE_DATE\' )
				    	LEFT JOIN property_preferences ppr1 ON ( l.cid = ppr1.cid AND l.property_id = ppr1.property_id AND ppr1.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
				    	LEFT JOIN property_preferences ppr2 ON ( l.cid = ppr2.cid AND l.property_id = ppr2.property_id AND ppr2.key = \'REQUIRE_BLANKET_POLICIES\')
				    	LEFT JOIN applications app ON ( app.cid = l.cid AND app.property_id = l.property_id AND l.active_lease_interval_id = app.lease_interval_id )
					WHERE
				    	l.property_id IN ( SELECT
				    							property_id
				    						FROM
				    							property_preferences
				    						WHERE
				    							key = \'COMPLIANCE_EMAIL_FREQUENCY\'
				    							AND value IN ( ' . $strFrequencies . ' )
				    							AND cid IN ( ' . implode( ',', $arrintCids ) . ' )
										 )
						AND l.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
						AND l.unit_number_cache IS NOT NULL
						AND ( CASE WHEN app.lease_type_id = ' . CLeaseType::CORPORATE . ' AND ppr1.value IS NOT NULL THEN FALSE ELSE TRUE END )
						AND ppr2.value IS NULL
						AND ( ppr.value::date IS NULL OR ( ppr.value::date IS NOT NULL AND ppr.value::date <= l.lease_start_date::date ) )';

		$strSql .= ( true == valArr( $arrintCustomerIds ) ) ? ' AND lc.customer_id NOT IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) ' : '';

		$strSql .= ' AND ( rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' ) OR rip.insurance_policy_status_type_id IS NULL )
					 AND p.is_disabled = 0
					 AND rip.replaced_insurance_policy_id IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUninsuredResidentsByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $arrmixUninsuredResidentFilters, $boolIsByUnit, $objDatabase, $boolIsComplianceEmail = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		if( true == valArr( $arrmixUninsuredResidentFilters ) ) {
			$strWhereParameters = '';

			$strLatestExpiryPolicy = '';

			if( true == isset( $arrmixUninsuredResidentFilters['latest_expired_policy'] ) ) {
				$strLatestExpiryPolicy = $arrmixUninsuredResidentFilters['latest_expired_policy'];

				unset( $arrmixUninsuredResidentFilters['latest_expired_policy'] );
			}

			// When unit vs occupant setting is set, will not check replaced_insurance_policy_id condition
			if( false == $boolIsByUnit && true == array_key_exists( 'replaced_insurance_policy_id', $arrmixUninsuredResidentFilters ) ) {
				unset( $arrmixUninsuredResidentFilters['replaced_insurance_policy_id'] );
			}

			foreach( $arrmixUninsuredResidentFilters as $strUninsuredResidentFilter ) {
				$strWhereParameters .= $strUninsuredResidentFilter;
			}
		}

		$strSql = 'WITH lease_intervals_data AS(
												SELECT
													*,
													row_number() OVER ( PARTITION BY lease_id, cid ORDER BY id desc ) AS last_lease_interval
												FROM
													lease_intervals lii
												WHERE
													lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
                                                    AND lii.lease_interval_type_id <> ' . CLeaseIntervalType::MONTH_TO_MONTH . '
                                                    AND lii.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
                                                    AND lii.cid IN ( ' . sqlIntImplode( $arrintCids ) . '  )
												)
                    SELECT
                        *
					FROM
                        (
                        SELECT
                            min ( data.applicant_id ) OVER ( PARTITION BY data.name_first, data.name_last, data.lease_id, data.move_in_date, data.email_address, data.property_id, data.cid, data.applicant_email_address ) AS min_applicant_id, -- because we found multiple applicants with one customer id
                            *
                        FROM
                            ( SELECT
									DISTINCT( c.id ) as customer_id,
									ipc.resident_insurance_policy_id,
									ipc.is_invalid_match,
									l.id as lease_id,
									l.primary_customer_id,
									rip.id AS resident_insure_policy_id,
									rip.effective_date,
									rip.end_date,
									rip.lapsed_on,
									rip.insurance_policy_id,
									rip.insurance_carrier_name,
									rip.policy_number,
									rip.insurance_policy_status_type_id,
									rip.is_integrated,
									rip.insurance_policy_status_type_id = ANY(\'{' . CInsurancePolicyStatusType::ACTIVE . ',' . CInsurancePolicyStatusType::CANCEL_PENDING . '}\'::int[]) AS active_or_cancel_pending_status,
									c.name_last,
									c.name_first,
									c.email_address,
									c.phone_number,
									c.primary_street_line1,
									c.primary_city,
									c.primary_state_code,
									c.primary_postal_code,
									func_format_customer_name( c.name_first, c.name_last ) AS customer_name,
									lc.lease_status_type_id,
									l.cid,
			                        l.property_id,
			                        l.property_unit_id,
			                        l.unit_space_id AS unit_space_number,
			                        l.unit_number_cache AS unit_number,
			                        l.move_in_date,
			                        l.move_out_date,
			                        l.lease_start_date,
			                        li.lease_start_date as latest_lease_start_date,
			                        li.lease_interval_type_id as latest_lease_interval_type_id,
			                        l.active_lease_interval_id,
			                        l.lease_interval_type_id,
			                        fpp.policy_effective_date,
			                        fpp.id AS forced_placed_policy_id,
			                        a.id as applicant_id,
			                        a.username as applicant_email_address,
			                        row_number ( ) OVER ( PARTITION BY lc.id
												    ORDER BY
												        CASE 
															WHEN rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ',  ' . CInsurancePolicyStatusType::CANCEL_PENDING . ' ) THEN 1
															ELSE 2
														END, rip.end_date DESC ) AS latest_expired_policy
								FROM
									cached_leases l';

		if( true == $boolIsByUnit ) {
			$strSql .= ' JOIN lease_customers lc ON ( l.primary_customer_id = lc.customer_id AND lc.cid = l.cid AND lc.lease_id = l.id )
						 LEFT JOIN insurance_policy_customers ipc ON( l.id = ipc.lease_id AND l.cid = ipc.cid )
						 LEFT JOIN customers c ON ( l.primary_customer_id = c.id AND l.cid = c.cid  )
						 LEFT JOIN applicants a ON ( a.cid = l.cid AND a.customer_id = l.primary_customer_id )';
		} else {
			$strSql .= ' JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id )
						 LEFT JOIN insurance_policy_customers ipc ON( lc.lease_id = ipc.lease_id AND lc.cid = ipc.cid  and lc.customer_id = ipc.customer_id )
						 LEFT JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid  )
						 LEFT JOIN applicants a ON ( a.cid = lc.cid AND a.customer_id = lc.customer_id )
						 LEFT JOIN customer_relationships cr ON( lc.cid = cr.cid AND lc.customer_relationship_id = cr.id AND lc.customer_type_id = cr.customer_type_id )';
		}

		if( true == $boolIsComplianceEmail ) {
			$strSql .= ' JOIN property_products pp ON ( pp.property_id = l.property_id AND pp.cid = l.cid AND pp.ps_product_id = ' . CPsProduct::RESIDENT_INSURE . ' )';
		}

		$strSql .= ' LEFT JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
					 LEFT JOIN forced_placed_policies fpp ON ( fpp.cid = l.cid AND fpp.lease_id = l.id AND fpp.is_disabled = FALSE)
					 LEFT JOIN properties p ON ( l.property_id = p.id AND l.cid = p.cid )
					 LEFT JOIN applications app ON ( app.cid = l.cid AND app.property_id = l.property_id AND l.active_lease_interval_id = app.lease_interval_id )
					 LEFT JOIN property_preferences ppr ON ( l.cid = ppr.cid AND l.property_id = ppr.property_id AND ppr.key = \'REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS_EFFECTIVE_DATE\' )
				     LEFT JOIN property_preferences ppr1 ON ( l.cid = ppr1.cid AND l.property_id = ppr1.property_id AND ppr1.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
				     LEFT JOIN lease_intervals_data  li ON ( li.cid = l.cid AND li.lease_id = l.id AND li.property_id = l.property_id AND last_lease_interval = 1 AND l.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' )
					WHERE
						l.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . '
						AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND l.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ' . $strWhereParameters . ' ) AS data ' . $strLatestExpiryPolicy . '
					    ) AS uninsured_data
					WHERE uninsured_data.min_applicant_id = uninsured_data.applicant_id';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchUninsuredResidentsByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$arrintInsurancePolicyStatusTypeIds = [ CInsurancePolicyStatusType::LAPSED, CInsurancePolicyStatusType::CANCELLED, CInsurancePolicyStatusType::EXPIRED, CInsurancePolicyStatusType::FUTURE_COVERAGE ];
		$arrintForcePlacedPolicyStatusTypeIds = [ CInsurancePolicyStatusType::ACTIVE, CInsurancePolicyStatusType::CANCELLED ];
		$arrintLeaseStatusTypeIds = [ CLeaseStatusType::PAST, CLeaseStatusType::FUTURE, CLeaseStatusType::CANCELLED ];

		$strSql = 'SELECT
						ipc.resident_insurance_policy_id,
                        ipc.lease_id AS lease_id,
                        ipc.id,
                        lc.customer_id,
                        rip.insurance_policy_status_type_id,
                        rip.insurance_policy_type_id
					FROM
						lease_customers lc
                        LEFT JOIN insurance_policy_customers ipc ON ( lc.lease_id = ipc.lease_id AND ipc.customer_id = lc.customer_id )
                        LEFT JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
                        LEFT JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
                        LEFT JOIN customer_relationships cr ON( lc.cid = cr.cid AND lc.customer_relationship_id = cr.id AND lc.customer_type_id = cr.customer_type_id )
					WHERE
				    	lc.property_id = ' . ( int ) $intPropertyId . '
				    	AND lc.cid = ' . ( int ) $intCid . '
				    	AND lc.lease_status_type_id NOT IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
				    	AND lc.customer_type_id IN ( ' . implode( ',', CCustomerType::$c_arrintInsuranceResponsibleCustomerTypeIds ) . ' )
				    	AND cr.name NOT LIKE \'%Spouse%\'
				    	AND ( ipc.resident_insurance_policy_id IS NULL OR ( ( rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' )
				    	AND rip.insurance_policy_type_id <> ' . CInsurancePolicyType::FORCED_PLACED_BASIC . ' ) OR ( rip.insurance_policy_type_id = ' . CInsurancePolicyType::FORCED_PLACED_BASIC . ' AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintForcePlacedPolicyStatusTypeIds ) . ' ) ) ) )
				    	AND lc.customer_id NOT IN (
                                                    SELECT
                                                        customer_id
                                                    FROM
                                                        insurance_policy_customers ipc
                                                        JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
                                                    WHERE
                                                        ipc.property_id = ' . ( int ) $intPropertyId . '
                                                        AND ipc.cid = ' . ( int ) $intCid . '
                                                        AND lease_id = ' . ( int ) $intLeaseId . '
                                                        AND rip.insurance_policy_type_id <> ' . CInsurancePolicyType::FORCED_PLACED_BASIC . '
                                                        AND rip.insurance_policy_status_type_id NOT IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' )
                                                    )
                        AND lc.lease_id NOT IN (
                                                    SELECT
                                                        lease_id
                                                    FROM
                                                        resident_insurance_policies rip
                                                        JOIN insurance_policy_customers ipc ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
                                                    WHERE
                                                        ipc.property_id = ' . ( int ) $intPropertyId . '
                                                        AND ipc.cid = ' . ( int ) $intCid . '
                                                        AND lease_id = ' . ( int ) $intLeaseId . '
                                                        AND rip.is_integrated = 1
                                                        AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', CInsurancePolicyStatusType::$c_arrintAllActiveInsurancePolicyStatusTypeIds ) . ' )
                                                    )                            
				    	AND lc.lease_id = ' . ( int ) $intLeaseId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomResidentInsurancePoliciesByCidsByInsurancePolicyIds( $arrintCids, $arrintInsurancePolicyIds, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintInsurancePolicyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
					WHERE
						rip.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND rip.insurance_policy_id IN ( ' . implode( ',', $arrintInsurancePolicyIds ) . ' ) ';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchCustomersToSendEmailByCustomerIdsByPropertyIdsByCid( $arrintCustomerIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( lc.id, lc.cid ) lc.id as lease_customer_id,
						c.id, 
						c.email_address as email_address, 
						c.name_first, 
						c.name_last,
						a.username as applicant_email_address,
						l.property_id as property_id,
						rip.policy_number
					FROM
						customers c
						JOIN lease_customers lc ON ( lc.cid = c.cid AND lc.customer_id = c.id )
						JOIN leases l ON ( c.cid = l.cid AND l.id = lc.lease_id AND l.property_id = lc.property_id )
						LEFT JOIN applicants a ON ( c.id = a.customer_id AND c.cid = a.cid )
						LEFT JOIN insurance_policy_customers ipc ON ( l.id = ipc.lease_id AND l.cid = ipc.cid )
						LEFT JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND c.id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND lc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUninsuredResidentsByCidsByPropertyIds( $arrintCids, $arrintPropertyIds, $strLastWeekDay, $strCurrentDate, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$arrintInsurancePolicyStatusTypeIds = [ CInsurancePolicyStatusType::LAPSED, CInsurancePolicyStatusType::CANCELLED, CInsurancePolicyStatusType::EXPIRED ];

		$arrintLeaseStatusTypes = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ];

		$strSql = 'SELECT
						rip.id, rip.insurance_carrier_name,
						rip.policy_number,
						rip.property_id,
						c.name_first,
						c.name_last,
        				rip.effective_date,
						rip.end_date,
						rip.lapsed_on,
						rip.insurance_policy_status_type_id,
						rip.cancelled_on,
						l.unit_number_cache as unit_number
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						JOIN lease_customers lc ON ( lc.lease_id = ipc.lease_id AND lc.cid = ipc.cid )
						JOIN customers c ON ( c.id = lc.customer_id AND c.cid = lc.cid )
						JOIN cached_leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid AND lc.customer_id = l.primary_customer_id )
					WHERE
						lc.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND rip.replaced_insurance_policy_id IS NULL
						AND ( ipc.is_invalid_match <> 1 OR ipc.is_invalid_match IS NULL )
						AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' )
						AND	rip.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND rip.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
						AND ( rip.end_date::DATE BETWEEN \'' . $strLastWeekDay . '\' AND \'' . $strCurrentDate . '\' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPendingExpirationInsurancePoliciesByCidsByPropertyIds( $arrintCids, $arrintPropertyIds, $strFutureDate, $strCurrentDate, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$arrintInsurancePolicyStatusTypeIds = [ CInsurancePolicyStatusType::CANCEL_PENDING, CInsurancePolicyStatusType::ACTIVE ];

		$arrintLeaseStatusTypes = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ];

		$strSql = 'SELECT
						rip.id, rip.insurance_carrier_name, rip.policy_number, rip.property_id, c.name_first, c.name_last,
        				rip.effective_date,	rip.end_date, rip.lapsed_on, rip.insurance_policy_status_type_id, rip.cancelled_on, l.unit_number_cache as unit_number
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						JOIN lease_customers lc ON ( lc.lease_id = ipc.lease_id AND lc.cid = ipc.cid )
						JOIN customers c ON ( c.id = lc.customer_id AND c.cid = lc.cid )
						JOIN cached_leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid AND lc.customer_id = l.primary_customer_id )
					WHERE
						lc.customer_type_id = ' . CCustomerType::PRIMARY . '
						AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' )
						AND	rip.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND rip.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
						AND ( rip.lapsed_on::DATE BETWEEN \'' . $strCurrentDate . '\' AND \'' . $strFutureDate . '\' OR rip.end_date::DATE BETWEEN \'' . $strCurrentDate . '\' AND \'' . $strFutureDate . '\' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDashboardUpdatePoliciesCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) {
			return 0;
		}

		$arrintInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::FUTURE_COVERAGE, CInsurancePolicyStatusType::LAPSED,
			CInsurancePolicyStatusType::CANCELLED, CInsurancePolicyStatusType::EXPIRED
		];

		$arrintResidentInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::ACTIVE,
			CInsurancePolicyStatusType::CANCEL_PENDING,
			CInsurancePolicyStatusType::ENROLLED
		];

		$arrintLeaseStatusTypes = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ];

		$strSql = 'SELECT
						count(l.id) as count
					FROM
						cached_leases l
						JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.customer_id = l.primary_customer_id  )
						LEFT JOIN insurance_policy_customers ipc ON ( l.id = ipc.lease_id AND l.cid = ipc.cid )
						LEFT JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
						LEFT JOIN customers c ON ( l.primary_customer_id = c.id AND l.cid = c.cid  )
						LEFT JOIN properties p ON ( l.property_id = p.id AND l.cid = p.cid )
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
						AND ( rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' ) OR rip.insurance_policy_status_type_id IS NULL )
						AND ( rip.end_date <= ( NOW() + \' 30 days\' ) OR rip.end_date IS NULL )
						AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( ipc.is_invalid_match <> 1 OR ipc.is_invalid_match IS NULL )
						AND rip.replaced_insurance_policy_id IS NULL
						AND l.unit_number_cache IS NOT NULL
						AND l.id NOT IN (
											SELECT
												ipc.lease_id
											FROM
												insurance_policy_customers ipc
												JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid
																							AND rip.insurance_policy_status_type_id IN  ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' )
																							AND rip.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )
																							AND rip.cid = ' . ( int ) $intCid . ' )
											WHERE ipc.lease_id IS NOT NULL
										)';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrFinalData = [];

		if( true == valArr( $arrstrData ) ) {
			foreach( $arrstrData as $arrstrChunk ) {
				$arrstrFinalData[] = $arrstrChunk['count'];
			}
		}

		return $arrstrFinalData;
	}

	public static function fetchResidentInsurancePoliciesByCidByEntityId( $intCid, $intEntityId, $objDatabase ) {
		if( false == is_numeric( $intCid ) || false == is_numeric( $intEntityId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.entity_id = ' . ( int ) $intEntityId;

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchResidentInsurancePoliciesByLeaseIdsByCustomerIdsByCid( $arrintLeaseIds, $arrintCustomerIds, $intCid, $objDatabase, $boolIsSelectedAllData = false ) {
		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintCustomerIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						rip.policy_number,
						rip.id,
						rip.liability_limit,
						ipc.customer_id,
						ipc.lease_id
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( ipc.cid = rip.cid AND rip.id = ipc.resident_insurance_policy_id )
					WHERE
					 	rip.cid = ' . ( int ) $intCid . '
						AND ipc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND ipc.customer_id  IN( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND ipc.is_invalid_match <> 1';

		$arrmixData = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrFinalData = [];

		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $arrmixChunk ) {
				if( true == $boolIsSelectedAllData ) {
					$arrstrFinalData[$arrmixChunk['lease_id']][$arrmixChunk['customer_id']][] = $arrmixChunk;
				} else {
					$arrstrFinalData[$arrmixChunk['customer_id']][$arrmixChunk['lease_id']]['policy_numbers'][] = $arrmixChunk['policy_number'];
				}
			}
		}

		return $arrstrFinalData;
	}

	public static function fetchResidentInsurancePoliciesDataByApplicationIdsByApplicantIdsByCid( $arrintApplicationIds, $arrintApplicantIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) || false == valArr( $arrintApplicationIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.policy_number,
						ipc.application_id,
						ipc.applicant_id
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND ipc.cid = rip.cid )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND ipc.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND	ipc.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND ipc.is_invalid_match <> 1';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		$arrstrFinalData = [];

		if( true == valArr( $arrstrData ) ) {
			foreach( $arrstrData as $arrstrChunk ) {
				$arrstrFinalData[$arrstrChunk['application_id']][$arrstrChunk['applicant_id']]['policy_numbers'][] = $arrstrChunk['policy_number'];
			}
		}

		return $arrstrFinalData;
	}

	public static function fetchSimpleActiveCustomResidentInsurancePoliciesByCids( $intSelectedCid, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$arrintInsuracePolicyStatusTypeIds = [ CInsurancePolicyStatusType::ACTIVE, CInsurancePolicyStatusType::CANCEL_PENDING ];

		$strSql = 'SELECT ';
		$strSql .= ( 'All' != $intSelectedCid ) ? 'rip.property_id ,count(id)' : 'rip.cid, count(id)';
		$strSql	.= 'FROM
						resident_insurance_policies rip
					WHERE
						rip.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND rip.is_integrated = 0
						AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsuracePolicyStatusTypeIds ) . ' ) ';

		$strSql .= ( 'All' != $intSelectedCid ) ? 'GROUP BY rip.property_id' : 'GROUP BY rip.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomInsurancePoliciesByDateByStatusTypeIds( $strCurrentDate, $arrintInsurancePolicyStatusTypeIds, $arrintTestCids, $objClientDatabase, $strCarrierName = NULL, $arrintInsurancePolicyTypeIds = NULL ) {

		$strSubSql = ( false == is_null( $strCarrierName ) ) ? ' AND rip.insurance_carrier_name = \'' . $strCarrierName . '\'' : '';
		$strSubSql .= ( true == valArr( $arrintInsurancePolicyTypeIds ) ) ? ' AND rip.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypeIds ) . ' ) ':'';

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
					WHERE
						rip.is_integrated = 0';

		$strSql .= ( true == valArr( $arrintTestCids ) ) ? ' AND rip.cid NOT IN ( ' . implode( ',', $arrintTestCids ) . ' ) ' : '';

		$strSql .= ' AND rip.effective_date <= \'' . $strCurrentDate . '\'
					 AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' ) ' . $strSubSql;

		return self::fetchResidentInsurancePolicies( $strSql, $objClientDatabase, false );
	}

	public static function fetchCustomResidentInsurancePoliciesByStatusTypeIdsByLeaseStatusTypeIds( $arrintPolicyStatusTypeIds, $arrintLeaseStatusTypeIds, $objClientDatabase ) {

		$strSql = 'SELECT
						ipc.lease_id,
						ipc.customer_id,
						rip.id,
						rip.property_id,
						rip.policy_number,
						rip.insurance_policy_id
				   FROM
						resident_insurance_policies rip JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						JOIN lease_customers lc on ( lc.customer_id = ipc.customer_id AND lc.cid = ipc.cid AND lc.lease_id = ipc.lease_id AND lc.lease_status_type_id IN( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' ) )
						JOIN clients mc on ( rip.cid = mc.id )
				   WHERE
						rip.cancelled_on IS NULL
					    AND rip.end_date IS NULL
                        AND rip.is_integrated = 1
						AND rip.insurance_policy_status_type_id IN( ' . implode( ',', $arrintPolicyStatusTypeIds ) . ' )
						AND ipc.customer_id IS NOT NULL
					    AND ipc.is_invalid_match <> 1
						AND lc.customer_type_id = 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function is written for new dashboard

	public static function fetchUnmatchedInsurancePoliciesCountByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$arrintResidentInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::ACTIVE,
			CInsurancePolicyStatusType::CANCEL_PENDING,
			CInsurancePolicyStatusType::ENROLLED
		];

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
						COUNT( rip.id )' .
		           ( true == $boolIsGroupByProperties ?
			           ' AS residents_insurance,
							1 AS priority,
							p.id AS property_id,
							p.property_name'
			           : '' ) . '
					FROM
						resident_insurance_policies rip
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.property_id = rip.property_id ' . ( ( false == $boolShowDisabledData ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
						JOIN properties p ON ( rip.property_id = p.id AND rip.cid = p.cid )
						LEFT JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.effective_date <= ( NOW ( ) + \' 60 days\' )
						AND ( NOW ( ) ::DATE <= rip.end_date OR end_date IS NULL )
						AND	rip.confirmed_on IS NOT NULL
						AND	rip.is_integrated = 1
						AND	rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' )
						AND	ipc.id IS NULL
						AND rip.is_archived = 0' .
		           ( true == $boolIsGroupByProperties ?
			           ' GROUP BY
							p.id,
							p.property_name,
							priority'
			           : '' );

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for new dashboard

	public static function fetchUnverifiedInsurancePoliciesCountByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$arrintInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::LAPSED,
			CInsurancePolicyStatusType::CANCELLED,
			CInsurancePolicyStatusType::EXPIRED
		];

		$arrintLeaseStatusTypes = [
			CLeaseStatusType::CURRENT,
			CLeaseStatusType::FUTURE,
			CLeaseStatusType::NOTICE
		];

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= '	SELECT
						COUNT( cl.id )' .
		           ( true == $boolIsGroupByProperties ?
			           ' AS residents_insurance,
							1 AS priority,
							cl.property_id,
							p.property_name'
			           : '' ) . '
					FROM
						cached_leases cl
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.property_id = cl.property_id ' . ( ( false == $boolShowDisabledData ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
						JOIN properties p ON ( p.cid = cl.cid AND p.id = cl.property_id )
						LEFT JOIN insurance_policy_customers ipc ON ( cl.id = ipc.lease_id AND cl.cid = ipc.cid )
						LEFT JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						cl.cid = ' . ( int ) $intCid . '
						AND	cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
						AND ipc.customer_id IS NOT NULL
						AND rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::FUTURE_COVERAGE . ',' . CInsurancePolicyStatusType::ACTIVE . ' )
						AND ( rip.confirmed_on IS NULL )
						AND rip.is_integrated = 0' .
		           ( true == $boolIsGroupByProperties ?
			           ' GROUP BY
							cl.property_id,
							p.property_name,
							priority'
			           : '' );

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for new dashboard

	public static function fetchUpdateInsurancePoliciesCountByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$arrintInsurancePolicyStatusTypes         = [ CInsurancePolicyStatusType::LAPSED, CInsurancePolicyStatusType::CANCELLED, CInsurancePolicyStatusType::EXPIRED ];
		$arrintLeaseStatusTypes                   = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ];
		$arrintResidentInsurancePolicyStatusTypes = [ CInsurancePolicyStatusType::ACTIVE, CInsurancePolicyStatusType::CANCEL_PENDING, CInsurancePolicyStatusType::ENROLLED ];
		$arrintInsurancePolicyTypes               = [ CInsurancePolicyType::FORCED_PLACED_BASIC, CInsurancePolicyType::AFFORDABLE ];
		$arrintResponsibileCustomerTypes          = [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE ];

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql = 'SELECT
						COUNT( cl.id )' .
		          ( true == $boolIsGroupByProperties ?
			          ' AS residents_insurance,
							1 AS priority,
							cl.property_id,
							p.property_name'
			          : '' ) . '
					FROM
						cached_leases cl
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.property_id = cl.property_id ' . ( ( false == $boolShowDisabledData ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
						JOIN properties p ON ( p.cid = cl.cid AND p.id = cl.property_id )
						JOIN lease_customers lc ON ( lc.cid = cl.cid AND lc.lease_id = cl.id )
						JOIN customers c ON ( c.cid = lc.cid AND c.id = lc.customer_id )
                        LEFT JOIN property_preferences pp2 ON ( pp2.cid = cl.cid AND cl.property_id = pp2.property_id AND pp2.key = \'REQUIRE_EACH_INDIVIDUAL_RESIDENT_HAVE_ACTIVE_COVERAGE\' AND pp2.value = \'1\' )
						LEFT JOIN insurance_policy_customers ipc ON ( cl.id = ipc.lease_id AND cl.cid = ipc.cid AND CASE WHEN pp2.key = \'REQUIRE_EACH_INDIVIDUAL_RESIDENT_HAVE_ACTIVE_COVERAGE\' AND pp2.value = \'1\' THEN ipc.customer_id = lc.customer_id END )
						LEFT JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						LEFT JOIN forced_placed_policies fpp ON ( fpp.cid = cl.cid AND fpp.lease_id = cl.id AND fpp.customer_id = cl.primary_customer_id AND fpp.is_disabled = false AND fpp.resident_insurance_policy_id IS NULL AND fpp.postponed_on IS NULL )
				    	LEFT JOIN property_preferences pp ON ( pp.cid = cl.cid AND cl.property_id = pp.property_id AND pp.key = \'REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS_EFFECTIVE_DATE\' )
				    	LEFT JOIN property_preferences pp1 ON ( pp1.cid = cl.cid AND cl.property_id = pp1.property_id AND pp1.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
				    	LEFT JOIN applications app ON ( app.cid = cl.cid AND cl.active_lease_interval_id = app.lease_interval_id )
					WHERE
						cl.cid = ' . ( int ) $intCid . '
						AND CASE
                                WHEN pp2.id IS NOT NULL THEN ( ipc.lease_id, ipc.customer_id ) NOT IN (
                                                                                        SELECT
                                                                                            ipcc.lease_id,
                                                                                            ipcc.customer_id
                                                                                        FROM
                                                                                            resident_insurance_policies ripp
                                                                                            JOIN insurance_policy_customers ipcc ON ( ripp.cid = ipcc.cid AND ripp.id = ipcc.resident_insurance_policy_id )
                                                                                        WHERE
                                                                                            ipcc.lease_id = lc.lease_id
                                                                                            AND ipcc.customer_id = c.id
                                                                                            AND ripp.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' ) )
                                WHEN pp2.id IS NULL THEN ( cl.id ) NOT IN (
                                                            SELECT
                                                                ipcc.lease_id
                                                            FROM
                                                                resident_insurance_policies ripp
                                                                JOIN insurance_policy_customers ipcc ON ( ripp.cid = ipcc.cid AND ripp.id = ipcc.resident_insurance_policy_id )
                                                            WHERE
                                                                ipcc.lease_id = lc.lease_id
                                                                AND ripp.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' ) )
                                END
						AND	cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
						AND lc.customer_type_id IN ( ' . implode( ',', $arrintResponsibileCustomerTypes ) . ' )
						AND cl.id NOT IN (
                                            SELECT
                                                lease_id
                                            FROM
                                                resident_insurance_policies rip1
                                                JOIN insurance_policy_customers ipc1 ON ( rip1.cid = ipc1.cid AND rip1.id = ipc1.resident_insurance_policy_id )
                                            WHERE
                                                rip1.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypes ) . ' )
                                                AND rip1.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' )
                                            )
						AND ( rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' )
						OR ( rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ', ' . CInsurancePolicyStatusType::CANCEL_PENDING . ' ) AND fpp.id IS NOT NULL )
						OR rip.insurance_policy_status_type_id IS NULL )
						AND ( rip.end_date <= ( NOW ( ) + \' 30 days\' ) OR rip.end_date IS NULL )
						AND rip.replaced_insurance_policy_id IS NULL
						AND cl.unit_number_cache IS NOT NULL
						AND ( ipc.is_invalid_match <> 1 OR ipc.is_invalid_match IS NULL )
						AND cl.unit_number_cache IS NOT NULL
						AND ( CASE WHEN app.lease_type_id = ' . CLeaseType::CORPORATE . ' AND pp1.value IS NOT NULL THEN FALSE ELSE TRUE END )
						AND ( pp.value::date IS NULL OR ( pp.value::date IS NOT NULL AND pp.value::date <= cl.lease_start_date::date ) )' .
		          ( true == $boolIsGroupByProperties ?
			          ' GROUP BY
							cl.property_id,
							p.property_name,
							priority'
			          : '' );

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for new dashboard

	public static function fetchPaginatedUpdateInsurancePoliciesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase, $boolIsCount = false, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$arrintInsurancePolicyStatusTypes         = [ CInsurancePolicyStatusType::LAPSED, CInsurancePolicyStatusType::CANCELLED, CInsurancePolicyStatusType::EXPIRED, CInsurancePolicyStatusType::FUTURE_COVERAGE ];
		$arrintLeaseStatusTypes                   = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ];
		$arrintResidentInsurancePolicyStatusTypes = [ CInsurancePolicyStatusType::ACTIVE, CInsurancePolicyStatusType::CANCEL_PENDING, CInsurancePolicyStatusType::ENROLLED ];
		$arrintCurrentLeaseStatusTypes            = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ];
		$arrintInsurancePolicyTypes               = [ CInsurancePolicyType::FORCED_PLACED_BASIC, CInsurancePolicyType::AFFORDABLE ];
		$arrintResponsibileCustomerTypes          = [ CCustomerType::PRIMARY, CCustomerType::RESPONSIBLE ];

		$strSql = $strOrderBy = $strLimit = '';

		if( true == $boolIsCount ) {
			$intMaxExecutionTimeOut = 20;
			$strData = 'count(*)';
			$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';
		} else {
			$strData = '*';

			$strLimit = ' OFFSET
						' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intLimit;

			$strOrderBy = ' ORDER BY
						policy_effective_date ASC NULLS LAST,';
		}

		$strSql .= 'SELECT
						' . $strData . '
					FROM (
						SELECT
							cl.cid,
							rip.id,
							max ( rip.id ) over ( partition BY cl.cid, cl.property_id, cl.id, c.id ) AS max_rip_id,
 							rip.end_date,
							cl.id AS lease_id,
							c.id AS customer_id,
							CASE
					            WHEN pp10.key = \'PAID_VERIFICATION\' AND pp10.value = \'1\' THEN TRUE
					            ELSE FALSE
				            END AS is_paid_verifcation_on,
							func_format_customer_name ( c.name_first, c.name_last ) AS customer_name,
							CASE
								WHEN ' . CInsurancePolicyStatusType::LAPSED . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Lapsed' ) . '\'
								WHEN ' . CInsurancePolicyStatusType::CANCELLED . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Cancelled' ) . '\'
								WHEN ' . CInsurancePolicyStatusType::EXPIRED . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Expired' ) . '\'
								WHEN ' . CLeaseStatusType::FUTURE . ' = cl.lease_status_type_id THEN \'' . __( 'Uninsured - Future Resident' ) . '\'
								ELSE \'' . __( 'Uninsured' ) . '\'
							END as insurance_policy_status,
							cl.property_name,
							cl.property_id,
							rip.policy_number,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
							CASE
								WHEN fpp.postponed_on IS NOT NULL THEN NULL
								ELSE fpp.policy_effective_date
							END AS policy_effective_date,
							CASE 
								WHEN ( cl.lease_status_type_id IN ( ' . implode( ',', $arrintCurrentLeaseStatusTypes ) . ' ) AND cl.lease_interval_type_id != ' . CLeaseIntervalType::MONTH_TO_MONTH . ' AND pp1.value IS NOT NULL )
									THEN (
										CASE
											WHEN ( pp2.value IS NULL OR pp4.value IS NULL OR ( pp6.value IS NOT NULL and pp7.value IS NULL ) ) THEN \'' . __( 'Required master policy settings are missing' ) . '\'
											WHEN ( ( pp2.value::DATE > cl.lease_start_date::DATE ) ) THEN CONCAT( \'' . __( 'Lease precedes master policy enforcement date setting ' ) . '\', pp2.value::DATE )
											WHEN ( pp6.value IS NOT NULL AND cl.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' AND pp7.value IS NOT NULL AND pp7.value::DATE >= CURRENT_DATE ) THEN CONCAT( \'' . __( 'Lease precedes master policy renewals exclusion ' ) . '\', pp7.value::DATE )
											WHEN ( lease_interval_type_id != ' . CLeaseIntervalType::MONTH_TO_MONTH . ' AND pp4.value IS NOT NULL AND ( pp4.value::integer = 0 OR ( ( CURRENT_DATE = cl.move_in_date OR CURRENT_DATE = cl.lease_start_date ) AND pp5.value IS NULL )  ) ) THEN \'' . __( 'Master policy will be created today' ) . '\'
										END
									)
								WHEN( cl.lease_status_type_id IN ( ' . implode( ',', $arrintCurrentLeaseStatusTypes ) . ' ) AND pp8.value IS NOT NULL )
									THEN (
										CASE
											WHEN ( pp2.value IS NULL OR ( pp6.value IS NOT NULL and pp7.value IS NULL ) ) THEN \'' . __( 'Required master policy settings are missing' ) . '\'
											WHEN ( ( pp2.value IS NOT NULL AND pp2.value::DATE > cl.lease_start_date::DATE ) ) THEN CONCAT( \'' . __( 'Lease precedes master policy enforcement date setting ' ) . '\', pp2.value::DATE )
											WHEN ( pp6.value IS NOT NULL AND cl.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' AND pp7.value IS NOT NULL AND pp7.value::DATE >= CURRENT_DATE ) THEN CONCAT( \'' . __( 'Lease precedes master policy renewals exclusion ' ) . '\', pp7.value::DATE )
											ELSE \'' . __( 'Master policy will be created today' ) . '\'
										END
									)
				                ELSE NULL
				            END AS uninsured_reason,
							fpp.id AS forced_placed_policy_id,
							rip.insurance_policy_status_type_id,
							fpp.postponed_on,
							CASE
								WHEN ( rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::LAPSED . ',' . CInsurancePolicyStatusType::CANCELLED . ', ' . CInsurancePolicyStatusType::EXPIRED . ' ) AND cl.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' AND pp1.value IS NOT NULL AND pp2.value::date <= cl.lease_start_date::date ) THEN 1
								WHEN rip.insurance_policy_status_type_id IS NULL AND cl.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' AND pp1.value IS NOT NULL AND pp2.value::date <= cl.lease_start_date::date THEN 1
								ELSE 0
							END AS bool_master_policy_opt_in,
							CASE
								WHEN ( rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::LAPSED . ',' . CInsurancePolicyStatusType::CANCELLED . ', ' . CInsurancePolicyStatusType::EXPIRED . ' ) AND cl.lease_status_type_id IN ( ' . sqlIntImplode( CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) . ' ) AND pp1.value IS NOT NULL AND pp2.value::date <= cl.lease_start_date::date ) THEN 1
								WHEN rip.insurance_policy_status_type_id IS NULL AND cl.lease_status_type_id IN ( ' . sqlIntImplode( CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) . ' ) AND pp1.value IS NOT NULL AND pp2.value::date <= cl.lease_start_date::date THEN 1
								ELSE 0
							END AS bool_master_policy_enroll_in
						FROM
							cached_leases cl
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.property_id = cl.property_id ' . ( ( false == $boolShowDisabledData ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
							JOIN lease_customers lc ON ( lc.cid = cl.cid AND lc.lease_id = cl.id )
							JOIN customers c ON ( c.cid = lc.cid AND c.id = lc.customer_id )
                            LEFT JOIN property_preferences pp9 ON ( pp9.cid = cl.cid AND cl.property_id = pp9.property_id AND pp9.key = \'REQUIRE_EACH_INDIVIDUAL_RESIDENT_HAVE_ACTIVE_COVERAGE\' AND pp9.value = \'1\' )
							LEFT JOIN insurance_policy_customers ipc ON ( cl.id = ipc.lease_id AND cl.cid = ipc.cid AND ipc.customer_id = lc.customer_id )
							LEFT JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
							LEFT JOIN forced_placed_policies fpp ON ( fpp.cid = cl.cid AND fpp.lease_id = cl.id AND fpp.customer_id = cl.primary_customer_id AND fpp.is_disabled = false AND fpp.resident_insurance_policy_id IS NULL )
							LEFT JOIN applications app ON ( app.cid = cl.cid AND cl.active_lease_interval_id = app.lease_interval_id )
							LEFT JOIN property_preferences pp ON ( pp.cid = cl.cid AND cl.property_id = pp.property_id AND pp.key = \'REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS_EFFECTIVE_DATE\' )
							LEFT JOIN property_preferences pp1 ON ( pp1.cid = cl.cid AND cl.property_id = pp1.property_id AND pp1.key = \'' . CPropertyPreference::REQUIRE_FORCE_PLACED_POLICIES . '\' )
							LEFT JOIN property_preferences pp2 ON ( pp2.cid = cl.cid AND cl.property_id = pp2.property_id AND pp2.key = \'' . CPropertyPreference::MASTER_POLICY_AGREEMENT . '\' )
							LEFT JOIN property_preferences pp3 ON ( pp3.cid = cl.cid AND cl.property_id = pp3.property_id AND pp3.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
							LEFT JOIN property_preferences pp4 ON ( pp4.cid = cl.cid AND cl.property_id = pp4.property_id AND pp4.key = \'GRACE_PERIOD_DAYS\' )
					        LEFT JOIN property_preferences pp5 ON ( pp5.cid = cl.cid AND cl.property_id = pp5.property_id AND pp5.key = \'ALLOW_GRACE_PERIOD_DAYS\' )
					        LEFT JOIN property_preferences pp6 ON ( pp6.cid = cl.cid AND cl.property_id = pp6.property_id AND pp6.key = \'EXCLUDE_RENEWALS\' )
					        LEFT JOIN property_preferences pp7 ON ( pp7.cid = cl.cid AND cl.property_id = pp7.property_id AND pp7.key = \'EXCLUDE_RENEWALS_UNTIL\' )
					        LEFT JOIN property_preferences pp8 ON ( pp8.cid = cl.cid AND cl.property_id = pp8.property_id AND pp8.key = \'REQUIRE_BLANKET_POLICIES\' )
					        LEFT JOIN property_preferences pp10 ON ( pp10.cid = cl.cid AND cl.property_id = pp10.property_id AND pp10.key = \'PAID_VERIFICATION\' )
						WHERE
							cl.cid = ' . ( int ) $intCid . '
							AND cl.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )
							AND lc.customer_type_id IN ( ' . implode( ',', $arrintResponsibileCustomerTypes ) . ' )
							AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
							AND CASE
                                WHEN pp9.id IS NOT NULL THEN ( ipc.lease_id, ipc.customer_id, ipc.cid ) NOT IN (
                                                                                        SELECT
                                                                                            ipcc.lease_id,
                                                                                            ipcc.customer_id,
                                                                                            ipcc.cid
                                                                                        FROM
                                                                                            resident_insurance_policies ripp
                                                                                            JOIN insurance_policy_customers ipcc ON ( ripp.cid = ipcc.cid AND ripp.id = ipcc.resident_insurance_policy_id )
                                                                                        WHERE
                                                                                            ipcc.lease_id = lc.lease_id
                                                                                            AND ipcc.customer_id = c.id
                                                                                            AND ripp.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' ) 
                                                                                            AND ipcc.cid = ' . ( int ) $intCid . ' )
                                WHEN pp9.id IS NULL THEN ( cl.id, cl.cid ) NOT IN (
                                                            SELECT
                                                                ipcc.lease_id,
                                                                ipcc.cid
                                                            FROM
                                                                resident_insurance_policies ripp
                                                                JOIN insurance_policy_customers ipcc ON ( ripp.cid = ipcc.cid AND ripp.id = ipcc.resident_insurance_policy_id )
                                                            WHERE
                                                                ipcc.lease_id = lc.lease_id
                                                                AND ripp.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' ) 
                                                                AND ipcc.cid = ' . ( int ) $intCid . ' )
                                                                
                                END
							AND	cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
							AND ( cl.id, cl.cid ) NOT IN (
                                            SELECT
                                                ipc1.lease_id,
                                                ipc1.cid
                                            FROM
                                                resident_insurance_policies rip1
                                                JOIN insurance_policy_customers ipc1 ON ( rip1.cid = ipc1.cid AND rip1.id = ipc1.resident_insurance_policy_id )
                                            WHERE
                                                rip1.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypes ) . ' )
                                                AND rip1.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' )
                                                AND ipc1.cid = ' . ( int ) $intCid . '
                                                AND ipc1.lease_id IS NOT NULL
                                            )
							AND ( rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' )
							OR ( rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ', ' . CInsurancePolicyStatusType::CANCEL_PENDING . ' ) AND fpp.id IS NOT NULL )
							OR rip.insurance_policy_status_type_id IS NULL )
							AND ( ( rip.effective_date IS NOT NULL AND rip.effective_date > CURRENT_DATE ) OR rip.end_date <= ( NOW () + \' 30 days \' ) OR rip.end_date IS NULL )
							AND rip.replaced_insurance_policy_id IS NULL
							AND cl.unit_number_cache IS NOT NULL
							AND ( CASE WHEN app.lease_type_id = ' . CLeaseType::CORPORATE . ' AND pp3.value IS NOT NULL THEN FALSE ELSE TRUE END )
							AND ( ipc.is_invalid_match <> 1 OR ipc.is_invalid_match IS NULL )
							AND ( pp.value::date IS NULL OR ( pp.value::date IS NOT NULL AND pp.value::date <= cl.lease_start_date::date ) )
						) AS insurance
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ( id = insurance.max_rip_id OR insurance.max_rip_id IS NULL )
						' . $strOrderBy . ' ' . $strSortBy . ' ' . ( string ) $strLimit;

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for new dashboard

	public static function fetchPaginatedUnverifiedInsurancePoliciesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$arrintInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::LAPSED,
			CInsurancePolicyStatusType::CANCELLED,
			CInsurancePolicyStatusType::EXPIRED
		];

		$arrintLeaseStatusTypes = [
			CLeaseStatusType::CURRENT,
			CLeaseStatusType::FUTURE,
			CLeaseStatusType::NOTICE
		];

		$strSql = 'SELECT
						cl.id AS lease_id,
						cl.primary_customer_id as customer_id,
						rip.id,
						func_format_customer_name( cl.name_first, cl.name_last ) AS customer_name,
						rip.end_date,
						cl.property_name,
						rip.policy_number,
						COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache )  AS unit_number
					FROM
						cached_leases cl
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.is_disabled = 0 AND lp.property_id = cl.property_id )
						LEFT JOIN insurance_policy_customers ipc ON ( cl.id = ipc.lease_id AND cl.cid = ipc.cid )
						LEFT JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						cl.cid = ' . ( int ) $intCid . '
						AND cl.occupancy_type_id IN ( ' . implode( ',', COccupancyType::$c_arrintIncludedOccupancyTypes ) . ' )
						AND	cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
						AND ipc.customer_id IS NOT NULL
						AND rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::FUTURE_COVERAGE . ',' . CInsurancePolicyStatusType::ACTIVE . ' )
						AND ( rip.confirmed_on IS NULL )
						AND rip.is_integrated = 0
					ORDER BY
						' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for new dashboard

	public static function fetchPaginatedUnmatchedInsurancePoliciesByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase, $intIsArchieved = 0 ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$arrintResidentInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::ACTIVE,
			CInsurancePolicyStatusType::CANCEL_PENDING,
			CInsurancePolicyStatusType::ENROLLED
		];

		$strSql = 'SELECT
						rip.id AS match_policy,
						rip.insurance_policy_id,
						rip.primary_insured_name AS customer_name,
						rip.insurance_policy_status_type_id,
						p.property_name,
						rip.policy_number,
						rip.unit_number,
						rip.created_on AS created_on
					FROM
						resident_insurance_policies rip
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.is_disabled = 0 AND lp.property_id = rip.property_id )
						JOIN properties p ON ( rip.property_id = p.id AND rip.cid = p.cid )
						LEFT JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.effective_date <= ( NOW ( ) + \' 60 days\' )
						AND ( NOW ( ) ::DATE <= rip.end_date OR end_date IS NULL )
						AND	rip.confirmed_on IS NOT NULL
						AND	rip.is_integrated = 1
						AND	rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' )
						AND	ipc.id IS NULL
						AND rip.is_archived = ' . ( int ) $intIsArchieved;

		if( 1 != ( int ) $intIsArchieved ) {
			$strSql .= ' ORDER BY ' . $strSortBy . '
						 OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomUninsuredResidentsByCidsByCustomerIdsByPropertyIdsByLeaseStatusTypeIdsByApplicationStageStatusIds( $arrintCids, $arrintCustomerIds, $arrintPropertyIds, $arrintLeaseStatusTypeIds, $arrintApplicationStageStatusIds, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) || false == valArr( $arrintLeaseStatusTypeIds ) ) {
			return NULL;
		}

		$strSubSql = ( true == valArr( $arrintCustomerIds ) ) ? ' AND l.primary_customer_id NOT IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) ' : '';

		$strSql = 'WITH customer_lease_info
					As (
							SELECT
								DISTINCT( l.primary_customer_id) as customer_id,
								l.id, l.name_first,
								l.name_last,
								l.move_in_date,
								c.email_address,
								l.property_id,
								l.cid,
								a.username as applicant_email_address
							FROM view_leases l
								JOIN customers c ON ( c.id = l.primary_customer_id AND c.cid = l.cid )
								LEFT JOIN applicants a ON ( a.cid = l.cid AND a.customer_id = l.primary_customer_id )
								LEFT JOIN applications app ON ( app.cid= l.cid AND l.active_lease_interval_id = app.lease_interval_id )
								LEFT JOIN property_preferences ppr ON ( l.cid = ppr.cid AND l.property_id = ppr.property_id AND ppr.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
							WHERE l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND l.cid IN ( ' . implode( ',', $arrintCids ) . ' )
								AND ( ( date_trunc(\'day\', l.move_in_date ) - INTERVAL \'3 day\')::Date = NOW()::Date OR ( date_trunc(\'day\', l.move_in_date ) - INTERVAL \'10 day\')::Date = NOW()::Date )
								AND l.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
								AND ( CASE WHEN app.lease_type_id = ' . CLeaseType::CORPORATE . ' AND ppr.value IS NOT NULL THEN FALSE ELSE TRUE END )
								AND (app.application_stage_id,app.application_status_id) IN ( ' . sqlIntMultiImplode( $arrintApplicationStageStatusIds ) . ' ) ' . $strSubSql . '
						) SELECT
							cli.*
					 	  FROM
							customer_lease_info cli
							LEFT JOIN insurance_policy_customers ipc on ipc.lease_id = cli.id and ipc.cid = cli.cid
					  	  	LEFT JOIN resident_insurance_policies rip on ipc.resident_insurance_policy_id = rip.id AND ipc.cid = rip.cid
					  	  WHERE
							rip.id is NULL
							AND cli.property_id NOT IN ( SELECT
				    							            property_id
				    						            FROM
				    							            property_preferences
				    						            WHERE
				    							            key = \'REQUIRE_BLANKET_POLICIES\'
				    							            AND value IS NOT NULL
				    							            AND cid IN ( ' . implode( ',', $arrintCids ) . ' )
										            )
							AND cli.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnmatchedResidentInsurancePolicyByPolicyNumberByCidByPropertyId( $strPolicyNumber, $intCId, $intPropertyId, $objClientDatabase ) {

		$arrintInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::ACTIVE,
			CInsurancePolicyStatusType::CANCEL_PENDING,
			CInsurancePolicyStatusType::ENROLLED
		];
		$strSql = 'SELECT
						rip.*
					FROM
					 	resident_insurance_policies rip
					 WHERE
						rip.id NOT IN (	SELECT
											ipc.resident_insurance_policy_id
								   		FROM
								   			insurance_policy_customers ipc
								   		WHERE
		 									ipc.resident_insurance_policy_id IS NOT NULL
											AND ipc.cid = ' . ( int ) $intCId . '
										)
					 	AND rip.cid = ' . ( int ) $intCId . '
					 	AND rip.property_id = ' . ( int ) $intPropertyId . '
					 	AND rip.policy_number = \'' . addslashes( $strPolicyNumber ) . '\'
					 	AND	rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' )
					 	AND rip.is_integrated = 1';

		return self::fetchResidentInsurancePolicy( $strSql, $objClientDatabase );
	}

	public static function fetchUnmatchedResidentInsurancePoliciesByPolicyNumberByCarrierNameByCidByPropertyId( $strPolicyNumber, $strCarrierName, $intCId, $intPropertyId, $objClientDatabase ) {

		$arrintInsurancePolicyStatusTypes = [
			CInsurancePolicyStatusType::ACTIVE,
			CInsurancePolicyStatusType::CANCEL_PENDING,
			CInsurancePolicyStatusType::ENROLLED
		];
		$strSql = 'SELECT
						rip.*
					FROM
					 	resident_insurance_policies rip
					 WHERE
						rip.id NOT IN (	SELECT
											ipc.resident_insurance_policy_id
								   		FROM
								   			insurance_policy_customers ipc
								   		WHERE
		 									ipc.resident_insurance_policy_id IS NOT NULL
											AND ipc.cid = ' . ( int ) $intCId . '
										)
					 	AND rip.cid = ' . ( int ) $intCId . '
					 	AND rip.property_id = ' . ( int ) $intPropertyId . '
					 	AND rip.policy_number ILIKE ( \'' . addslashes( $strPolicyNumber ) . '%\' )
					 	AND	rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypes ) . ' )
					 	AND rip.insurance_carrier_name = \'' . $strCarrierName . '\'
					 	AND rip.is_integrated = 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchResidentInsurancePoliciesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ipc.lease_id,
						rip.policy_number,
						ipc.customer_id
					FROM
						resident_insurance_policies rip
						INNER JOIN insurance_policy_customers ipc ON ( rip.cid = ipc.cid AND rip.id = ipc.resident_insurance_policy_id )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllResidentInsurancePoliciesByCid( $intCid, $objDatabase ) {
		if( false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*,
						ipc.customer_id
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						rip.cid = ' . ( int ) $intCid;

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchAllResidentInsurancePoliciesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*,
						ipc.customer_id,
						ipc.lease_id
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
					AND rip.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
						JOIN forced_placed_policies fpp ON ( fpp.cid = rip.cid AND fpp.resident_insurance_policy_id = rip.id )
					WHERE
						fpp.is_disabled = false
						AND fpp.lease_id = ' . ( int ) $intLeaseId . '
						AND fpp.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchCustomUninsuredResidentsByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $intNoticeReminder, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$arrintLeaseStatusTypeIds                 = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ];
		$arrintResidentInsurancePolicyStatusTypes = [ CInsurancePolicyStatusType::ACTIVE, CInsurancePolicyStatusType::ENROLLED ];

		switch( $intNoticeReminder ) {

			case 1:
				$strSql = 'SELECT
							    *
							FROM
							    (
							      SELECT
							          cl.primary_customer_id AS customer_id,
							          cl.cid,
							          cl.property_id,
							          cl.id AS lease_id,
							          rip.id AS resident_insure_policy_id,
							          rip.effective_date,
							          rip.end_date,
							          rip.lapsed_on,
							          c.name_first,
							          c.name_last,
							          c.email_address,
							          cl.property_unit_id,
							          cl.unit_space_id,
						              cl.lease_interval_type_id,
							          cl.unit_number_cache AS unit_number,
							          rip.insurance_policy_status_type_id,
									  rip.insurance_policy_status_type_id = ANY(\'{' . CInsurancePolicyStatusType::ACTIVE . ',' . CInsurancePolicyStatusType::CANCEL_PENDING . '}\'::int[]) AS active_or_cancel_pending_status,
							          cl.move_out_date,
							          cl.lease_start_date,
							          cl.move_in_date,
							          fpp.policy_effective_date,
							          cl.active_lease_interval_id,
							          row_number ( ) OVER ( PARTITION BY cl.id
									    ORDER BY
									        CASE 
												WHEN rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ',  ' . CInsurancePolicyStatusType::CANCEL_PENDING . ' ) THEN 1
												ELSE 2
											END, rip.end_date DESC ) AS latest_expired_policy
							      FROM
							          cached_leases cl
							          JOIN lease_customers lc ON ( lc.cid = cl.cid AND lc.lease_id = cl.id AND lc.customer_id = cl.primary_customer_id )
							          LEFT JOIN insurance_policy_customers ipc ON ( ipc.cid = cl.cid AND ipc.lease_id = cl.id )
							          LEFT JOIN resident_insurance_policies rip ON ( rip.cid = ipc.cid AND rip.id = ipc.resident_insurance_policy_id )
							          LEFT JOIN customers c ON ( c.cid = cl.cid AND c.id = cl.primary_customer_id )
							          LEFT JOIN forced_placed_policies fpp ON ( fpp.cid = cl.cid AND fpp.lease_id = cl.id AND fpp.is_disabled = FALSE )
							          LEFT JOIN applications app ON ( app.cid = cl.cid AND cl.active_lease_interval_id = app.lease_interval_id )
							          LEFT JOIN property_preferences ppr ON ( cl.cid = ppr.cid AND cl.property_id = ppr.property_id AND ppr.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
							      WHERE
							          cl.unit_number_cache IS NOT NULL
							          AND fpp.id IS NULL
							          AND rip.replaced_insurance_policy_id IS NULL
							          AND ( CASE WHEN app.lease_type_id = ' . CLeaseType::CORPORATE . ' AND ppr.value IS NOT NULL THEN FALSE ELSE TRUE END )
							          AND ( ipc.is_invalid_match <> 1
							          OR ipc.is_invalid_match IS NULL )
							          AND cl.cid IN ( ' . implode( ', ', $arrintCids ) . ' )
							          AND lc.lease_status_type_id IN ( ' . implode( ', ', $arrintLeaseStatusTypeIds ) . ' )
							       	  AND cl.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							          AND cl.id NOT IN (
							                             SELECT
							                                 ipc.lease_id
							                             FROM
							                                 insurance_policy_customers ipc
							                                 JOIN resident_insurance_policies rip ON ( rip.cid = ipc.cid AND rip.id = ipc.resident_insurance_policy_id )
							       	  						 LEFT JOIN property_preferences pp ON ( pp.cid = rip.cid and pp.property_id = rip.property_id and pp.key = \'POLICY_ABOUT_TO_END_DAYS\' )
							                             WHERE
							                                 ipc.lease_id IS NOT NULL
							                                 AND rip.insurance_policy_status_type_id IN ( ' . implode( ', ', $arrintResidentInsurancePolicyStatusTypes ) . ' )
							                                 AND rip.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
							                                 AND rip.cid IN ( ' . implode( ', ', $arrintCids ) . ' )
							                                 AND rip.lapsed_on IS NULL
							                                 AND ( rip.end_date IS NULL OR rip.end_date < CURRENT_DATE + ( pp.value::INTERVAL ) )
							          )
							    ) AS sub_query
							WHERE
							    latest_expired_policy = 1';
				break;

			case 2:
				$strSql = 'SELECT
							    cl.primary_customer_id AS customer_id,
							    cl.cid,
							    cl.property_id,
							    cl.id AS lease_id,
							    rip.id AS resident_insure_policy_id,
							    rip.end_date,
							    c.name_first,
							    c.name_last,
							    c.email_address,
							    cl.property_unit_id,
							    cl.unit_space_id,
							    cl.unit_number_cache AS unit_number,
							    cl.move_out_date,
							    cl.lease_start_date,
								rip.insurance_policy_status_type_id = ANY(\'{' . CInsurancePolicyStatusType::ACTIVE . ',' . CInsurancePolicyStatusType::CANCEL_PENDING . '}\'::int[]) AS active_or_cancel_pending_status,
							    fpp.policy_effective_date,
							    fpp.id AS forced_placed_policy_id
							FROM
							    cached_leases cl
							    JOIN lease_customers lc ON ( lc.cid = cl.cid AND lc.lease_id = cl.id AND lc.customer_id = cl.primary_customer_id )
							    LEFT JOIN insurance_policy_customers ipc ON ( ipc.cid = cl.cid AND ipc.lease_id = cl.id )
							    LEFT JOIN resident_insurance_policies rip ON ( rip.cid = ipc.cid AND rip.id = ipc.resident_insurance_policy_id )
							    LEFT JOIN customers c ON ( c.cid = cl.cid AND c.id = cl.primary_customer_id )
							    JOIN forced_placed_policies fpp ON ( fpp.cid = lc.cid AND fpp.lease_id = lc.lease_id )
							WHERE
							    cl.unit_number_cache IS NOT NULL
							    AND ( fpp.is_disabled = FALSE
							    AND fpp.resident_insurance_policy_id IS NULL
							    AND fpp.first_notice_sent_on::DATE < CURRENT_DATE::DATE
							    AND fpp.postponed_on IS NULL )
							    AND ( rip.end_date = CURRENT_DATE::DATE + 1 OR rip.lapsed_on = CURRENT_DATE::DATE + 1 )
							    AND rip.replaced_insurance_policy_id IS NULL
							    AND ( ipc.is_invalid_match <> 1 OR ipc.is_invalid_match IS NULL )
							    AND cl.cid IN ( ' . implode( ', ', $arrintCids ) . ' )
							    AND lc.lease_status_type_id IN ( ' . implode( ', ', $arrintLeaseStatusTypeIds ) . ' )
							    AND cl.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ';
				break;

			// In third notice sql, we do not need to check for insured resident as, if resident added a custom policy, an end date will get set on the FPP policy records along with is_disabled to false
			case 3:
				$strSql = ' SELECT
    							cl.primary_customer_id AS customer_id,
    							cl.cid,
    							cl.property_id,
							    cl.id AS lease_id,
							    c.name_first,
							    c.name_last,
							    c.email_address,
								c.primary_street_line1,
								c.primary_city,
								c.primary_state_code,
								c.primary_postal_code,
								cl.property_unit_id,
							    cl.unit_space_id,
							    cl.unit_number_cache AS unit_number,
							    cl.move_out_date,
							    cl.lease_start_date,
							    fpp.policy_effective_date,
							    fpp.id AS forced_placed_policy_id,
								cl.active_lease_interval_id,
								rip.policy_status_type_id_array && ARRAY[' . CInsurancePolicyStatusType::ACTIVE . ',' . CInsurancePolicyStatusType::CANCEL_PENDING . '] as active_or_cancel_pending_status
							FROM cached_leases cl
								JOIN lease_customers lc ON ( lc.cid = cl.cid AND lc.lease_id = cl.id AND lc.customer_id = cl.primary_customer_id  )
								JOIN customers c ON ( c.cid = cl.cid AND c.id = cl.primary_customer_id )
								JOIN forced_placed_policies fpp ON ( fpp.cid = cl.cid AND fpp.lease_id = cl.id )
								LEFT JOIN
								 (
								   SELECT
								   ipc.cid,
									 ipc.lease_id,
									 array_agg( DISTINCT rip.insurance_policy_status_type_id ) policy_status_type_id_array
								   FROM
									 resident_insurance_policies AS rip
									 JOIN insurance_policy_customers ipc ON ( rip.cid = ipc.cid AND ipc.resident_insurance_policy_id = rip.id )
								   WHERE
									 rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ',' . CInsurancePolicyStatusType::CANCEL_PENDING . ' )
								   GROUP BY
										ipc.cid,
										ipc.lease_id
								 ) rip ON ( rip.cid = cl.cid AND rip.lease_id = cl.id )
							WHERE
							cl.unit_number_cache IS NOT NULL
								AND fpp.policy_effective_date::DATE <= CURRENT_DATE::DATE
								AND fpp.first_notice_sent_on::DATE <= CURRENT_DATE::DATE
								AND fpp.is_disabled = false
								AND fpp.enrolled_on IS NULL
								AND cl.cid IN ( ' . implode( ',', $arrintCids ) . ' )
								AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
								AND cl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
				break;

			default:
				// nothing to do
				break;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePolicyCountByPolicyNumberByLeaseIdByPropertyIdByCid( $strPolicyNumber, $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intCid ) || false == is_numeric( $intLeaseId ) || false == is_numeric( $intPropertyId ) || true == is_null( $strPolicyNumber ) ) {
			return NULL;
		}

		$strSql = '	SELECT
						rip.*
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						ipc.cid = ' . ( int ) $intCid . '
						AND ipc.lease_id = ' . ( int ) $intLeaseId . '
						AND ipc.property_id = ' . ( int ) $intPropertyId . '
						AND rip.policy_number ILIKE ( \'' . addslashes( $strPolicyNumber ) . '\' ) 
					ORDER BY
						rip.id DESC
					LIMIT
						1';

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchSimpleResidentInsurancePoliciesByApplicationsIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( ipc.cid, ipc.application_id )
						rip.id,
						rip.cid,
						ipc.application_id,
						rip.insurance_carrier_name,
						rip.policy_number,
						rip.effective_date,
						rip.end_date,
						rip.confirmed_on,
						CASE
							WHEN rip.confirmed_on IS NOT NULL THEN \'' . __( 'Entered' ) . '\'
							WHEN rip.insurance_carrier_name IS NOT NULL OR rip.policy_number IS NOT NULL OR rip.effective_date IS NOT NULL OR rip.end_date IS NOT NULL THEN  \'' . __( 'Unverified' ) . '\'
							ELSE NULL
						END AS insurance_info
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						ipc.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND ipc.cid = ' . ( int ) $intCid;

		$arrmixInsuranceInfo = ( array ) fetchData( $strSql, $objDatabase );

		$arrmixInsuranceInfo = rekeyArray( 'application_id', $arrmixInsuranceInfo );

		return $arrmixInsuranceInfo;
	}

	public static function fetchNewMoveInLeasesByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$arrintLeaseStatusTypeIds = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ];
		// we have constant in base
		$arrintLeaseIntervalTypes = [ CLeaseIntervalType::APPLICATION, CLeaseIntervalType::RENEWAL, CLeaseIntervalType::TRANSFER, CLeaseIntervalType::MONTH_TO_MONTH ];

		$strSql = 'WITH lease_intervals_data AS(
                                                 SELECT
												      *,
												      row_number() OVER ( PARTITION BY lease_id, cid ORDER BY id desc ) AS last_lease_interval
												 FROM
												      lease_intervals lii 
                                                 WHERE 
                                                       lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
                                                       AND lii.lease_interval_type_id <> ' . CLeaseIntervalType::MONTH_TO_MONTH . '
                                                       AND lii.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
                                                       AND lii.cid IN ( ' . sqlIntImplode( $arrintCids ) . '  )
                                               )
                    SELECT
    					DISTINCT ( l.primary_customer_id ) AS customer_id,
    					l.cid,
    					l.property_id,
						l.id AS lease_id,
						c.name_first,
						c.name_last,
						c.primary_street_line1,
						c.primary_city,
						c.primary_state_code,
						c.primary_postal_code,
						l.property_unit_id,
						l.unit_space_id,
						us.unit_number_cache AS unit_number,
						l.lease_start_date,
						li.lease_start_date as latest_lease_start_date,
						li.lease_interval_type_id as latest_lease_interval_type_id,
						l.active_lease_interval_id,
						l.lease_interval_type_id
					FROM 
						cached_leases l
						JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.customer_id = l.primary_customer_id  )
						JOIN customers c ON ( c.cid = l.cid AND c.id = l.primary_customer_id )
						JOIN lease_processes lp ON ( lp.cid = l.cid AND lp.lease_id = l.id )
						JOIN unit_spaces us ON ( us.cid = l.cid AND us.property_id = l.property_id AND us.id = l.unit_space_id )
						LEFT JOIN forced_placed_policies fpp ON ( fpp.cid = l.cid AND fpp.lease_id = l.id AND fpp.is_disabled = FALSE )
						LEFT JOIN applications app ON ( app.cid = l.cid AND l.active_lease_interval_id = app.lease_interval_id )
						LEFT JOIN property_preferences ppr ON ( l.cid = ppr.cid AND l.property_id = ppr.property_id AND ppr.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
						LEFT JOIN lease_intervals_data AS li ON ( li.cid = l.cid AND li.lease_id = l.id AND li.property_id = l.property_id AND last_lease_interval = 1 AND l.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' )      
					WHERE
						l.unit_space_id IS NOT NULL
						AND fpp.id IS NULL
						AND ( CASE WHEN app.lease_type_id = ' . CLeaseType::CORPORATE . ' AND ppr.value IS NOT NULL THEN FALSE ELSE TRUE END )
						AND l.lease_start_date::DATE <= CURRENT_DATE::DATE
						AND l.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND l.lease_interval_type_id IN ( ' . implode( ',', $arrintLeaseIntervalTypes ) . ' )
						AND l.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND l.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . '
						AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePolicyDetailsByCidByInsurancePolicyId( $intInsurancePolicyId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intInsurancePolicyId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.id,
						ipc.lease_id
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.insurance_policy_id = ' . ( int ) $intInsurancePolicyId;

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchAllResidentInsurancePoliciesByPropertyIdsByLeaseStatusIds( $arrintPropertyIds, $intCid, $objDatabase, $arrintLeaseStatusTypeIds = NULL, $strLastUpdatedOn = NULL ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*,
						ipc.customer_id,ipc.lease_id
						FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )';
		if( false != valArr( $arrintLeaseStatusTypeIds ) ) {
			$strSql .= 'JOIN lease_customers lc ON( ipc.lease_id = lc.lease_id AND ipc.customer_id = lc.customer_id AND ipc.cid = lc.cid )';
		}
		$strSql .= 'WHERE
						rip.cid = ' . ( int ) $intCid . '
					AND rip.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		if( false != valStr( $strLastUpdatedOn ) ) {
			$strSql .= ' AND rip.updated_on >= \'' . date( 'Y-m-d', strtotime( $strLastUpdatedOn ) ) . '\'';
		}
		if( false != valArr( $arrintLeaseStatusTypeIds ) ) {
			$strSql .= 'AND lc.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )';
		}

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByIdsByCids( $arrintResidentInsurancePolicyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintResidentInsurancePolicyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						resident_insurance_policies
					WHERE
						 id IN ( ' . implode( ',', $arrintResidentInsurancePolicyIds ) . ' )
					 	 AND cid IN (' . implode( ',', $arrintCids ) . ' )';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByLeaseIdsByCids( $arrintResidentInsurePolicyIds, $arrintLeaseIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintResidentInsurePolicyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.id,
						rip.policy_number,
						ipc.lease_id,
						rip.effective_date,
						rip.end_date,
						ipc.property_id,
						rip.cid
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						rip.id NOT IN ( ' . implode( ',', $arrintResidentInsurePolicyIds ) . ' )
						AND ipc.lease_id IN( ' . implode( ',', $arrintLeaseIds ) . ' )
					 	AND rip.cid IN( ' . implode( ',', $arrintCids ) . ' )
					 	AND rip.effective_Date::DATE >= CURRENT_DATE::DATE
					ORDER BY
						rip.effective_Date DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByLeaseIdsByStatusTypeIdsByPropertyIdsByCids( $arrintLeaseIds, $arrintInsurancePolicyStatusTypeIds, $arrintPropertyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) || false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) || false == valArr( $arrintInsurancePolicyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ipc.lease_id,
						rip.cid
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					WHERE
						ipc.lease_id IN( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND rip.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
					 	AND rip.cid IN( ' . implode( ',', $arrintCids ) . ' )
					 	AND rip.effective_date::DATE <= CURRENT_DATE::DATE
					 	AND ( rip.end_date IS NULL OR rip.end_date > CURRENT_DATE::DATE )
					 	AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' )
					GROUP BY
						rip.cid,ipc.lease_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedInsurancePoliciesByPropertyGroupIdsByCidByAboutToExpireDays( $arrintPropertyGroupIds, $intCid, $intNoOfDays, $intOffset, $intLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == is_numeric( $intCid ) || false == is_numeric( $intNoOfDays ) ) {
			return NULL;
		}

		$arrintLeaseStatusTypes                   = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ];
		$arrintInsurancePolicyTypes               = [ CInsurancePolicyType::HO_4, CInsurancePolicyType::CUSTOM ];
		$arrintResidentInsurancePolicyStatusTypes = [ CInsurancePolicyStatusType::ACTIVE, CInsurancePolicyStatusType::CANCEL_PENDING, CInsurancePolicyStatusType::ENROLLED ];

		$strSql = 'SELECT
						*
					FROM
					(
					  SELECT
					      *,
					      max ( sub_query.latest_end_date ) OVER ( PARTITION BY sub_query.lease_id ) AS over_all_latest_end_date
					  FROM
					      (
					        SELECT
					            rip.id,
					            rip.cid,
					            rip.property_id,
					            ipc.lease_id,
					            cl.primary_customer_id AS customer_id,
					            func_format_customer_name ( cl.name_first, cl.name_last ) AS customer_name,
					            COALESCE ( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
					            cl.property_name,
					            rip.policy_number,
					            rip.effective_date,
					            rip.end_date,
					            rip.lapsed_on,
					            rip.is_integrated,
					            rip.insurance_policy_status_type_id,
					            CASE
						            WHEN ' . CInsurancePolicyStatusType::ACTIVE . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Active' ) . '\'
									WHEN ' . CInsurancePolicyStatusType::CANCEL_PENDING . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Cancel Pending' ) . '\'
									WHEN ' . CInsurancePolicyStatusType::ENROLLED . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Enrolled' ) . '\'
									ELSE \'-\'
					            END AS insurance_policy_status,
					            count ( rip.id ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) AS total_count,
					            count ( rip.id ) OVER ( PARTITION BY ipc.lease_id, ipc.cid, ( rip.end_date IS NOT NULL OR rip.lapsed_on IS NOT NULL ) ) AS total_count_with_end_date,
					            CASE
					              WHEN ( rip.lapsed_on IS NOT NULL AND rip.end_date::DATE IS NOT NULL AND max ( rip.end_date ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) > max ( rip.lapsed_on ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) ) THEN max ( rip.end_date ) OVER ( PARTITION BY ipc.lease_id, ipc.cid )
					              WHEN ( rip.lapsed_on IS NOT NULL AND rip.end_date::DATE IS NOT NULL AND max ( rip.end_date ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) < max ( rip.lapsed_on ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) ) THEN max ( rip.lapsed_on ) OVER ( PARTITION BY ipc.lease_id, ipc.cid )
					              WHEN ( rip.lapsed_on IS NULL ) THEN max ( rip.end_date ) OVER ( PARTITION BY ipc.lease_id, ipc.cid )
					              WHEN ( rip.end_date IS NULL ) THEN max ( rip.lapsed_on ) OVER ( PARTITION BY ipc.lease_id, ipc.cid )
					            END AS latest_end_date
					        FROM
					            cached_leases cl
					            JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.is_disabled = 0 AND lp.property_id = cl.property_id )
					            JOIN insurance_policy_customers ipc ON ( cl.id = ipc.lease_id AND cl.cid = ipc.cid )
					            JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					        WHERE
					            rip.cid = ' . ( int ) $intCid . '
					            AND rip.replaced_insurance_policy_id IS NULL
					            AND rip.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypes ) . ' )
					            AND cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
					            AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' )
					      ) AS sub_query
					  WHERE
					      total_count_with_end_date = total_count
					) AS over_all_data
					WHERE
						over_all_data.over_all_latest_end_date >= CURRENT_DATE
						AND over_all_data.over_all_latest_end_date::DATE <= ( NOW () + \' ' . ( int ) $intNoOfDays . ' days\' )::DATE
					ORDER BY
						' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInsurancePoliciesCountByPropertyGroupIdsByCidByAboutToExpireDays( $arrintPropertyGroupIds, $intCid, $intNoOfDays, $objDatabase, $intMaxExecutionTimeOut = 0, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) || false == is_numeric( $intCid ) || false == is_numeric( $intNoOfDays ) ) {
			return NULL;
		}

		$arrintLeaseStatusTypes                   = [ CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE, CLeaseStatusType::NOTICE ];
		$arrintInsurancePolicyTypes               = [ CInsurancePolicyType::HO_4, CInsurancePolicyType::CUSTOM ];
		$arrintResidentInsurancePolicyStatusTypes = [ CInsurancePolicyStatusType::ACTIVE, CInsurancePolicyStatusType::CANCEL_PENDING, CInsurancePolicyStatusType::ENROLLED ];

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
						count(id)
					FROM
					(
					  SELECT
					      *,
					      max ( sub_query.latest_end_date ) OVER ( PARTITION BY sub_query.lease_id ) AS over_all_latest_end_date
					  FROM
					      (
					        SELECT
					            rip.id,
					            rip.cid,
					            rip.property_id,
					            ipc.lease_id,
					            cl.primary_customer_id AS customer_id,
					            func_format_customer_name ( cl.name_first, cl.name_last ) AS customer_name,
					            COALESCE ( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
					            cl.property_name,
					            rip.policy_number,
					            rip.effective_date,
					            rip.end_date,
					            rip.lapsed_on,
					            CASE
						            WHEN ' . CInsurancePolicyStatusType::ACTIVE . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Active' ) . '\'
									WHEN ' . CInsurancePolicyStatusType::CANCEL_PENDING . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Cancel Pending' ) . '\'
									WHEN ' . CInsurancePolicyStatusType::ENROLLED . ' = rip.insurance_policy_status_type_id THEN \'' . __( 'Enrolled' ) . '\'
									ELSE \'-\'
					            END AS insurance_policy_status,
					            count ( rip.id ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) AS total_count,
					            count ( rip.id ) OVER ( PARTITION BY ipc.lease_id, ipc.cid, ( rip.end_date IS NOT NULL OR rip.lapsed_on IS NOT NULL ) ) AS total_count_with_end_date,
					            CASE
					              WHEN ( rip.lapsed_on IS NOT NULL AND rip.end_date::DATE IS NOT NULL AND max ( rip.end_date ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) > max ( rip.lapsed_on ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) ) THEN max ( rip.end_date ) OVER ( PARTITION BY ipc.lease_id, ipc.cid )
					              WHEN ( rip.lapsed_on IS NOT NULL AND rip.end_date::DATE IS NOT NULL AND max ( rip.end_date ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) < max ( rip.lapsed_on ) OVER ( PARTITION BY ipc.lease_id, ipc.cid ) ) THEN max ( rip.lapsed_on ) OVER ( PARTITION BY ipc.lease_id, ipc.cid )
					              WHEN ( rip.lapsed_on IS NULL ) THEN max ( rip.end_date ) OVER ( PARTITION BY ipc.lease_id, ipc.cid )
					              WHEN ( rip.end_date IS NULL ) THEN max ( rip.lapsed_on ) OVER ( PARTITION BY ipc.lease_id, ipc.cid )
					            END AS latest_end_date
					        FROM
					            cached_leases cl
					            JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.property_id = cl.property_id ' . ( ( false == $boolShowDisabledData ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
					            JOIN insurance_policy_customers ipc ON ( cl.id = ipc.lease_id AND cl.cid = ipc.cid )
					            JOIN resident_insurance_policies rip ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					        WHERE
					            rip.cid = ' . ( int ) $intCid . '
					            AND rip.replaced_insurance_policy_id IS NULL
					            AND rip.insurance_policy_type_id IN ( ' . implode( ',', $arrintInsurancePolicyTypes ) . ' )
					            AND cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypes ) . ' )
					            AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintResidentInsurancePolicyStatusTypes ) . ' )
					      ) AS sub_query
					  WHERE
					      total_count_with_end_date = total_count
					) AS over_all_data
					WHERE
						over_all_data.over_all_latest_end_date >= CURRENT_DATE
						AND over_all_data.over_all_latest_end_date::DATE <= ( NOW () + \' ' . ( int ) $intNoOfDays . ' days\' )::DATE';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByInsurancePolicyIdByPropertyIdByCid( $intInsurancePolicyId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intInsurancePolicyId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
					WHERE
						rip.cid = ' . ( int ) $intCid . '
						AND rip.property_id = ' . ( int ) $intPropertyId . '
						AND rip.insurance_policy_id = ' . ( int ) $intInsurancePolicyId;

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchActiveHO4PolicyLeasesByCidsByPropertyIds( $arrintCIds, $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintCIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( ipc.lease_id )
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
						JOIN cached_leases l ON ( ipc.cid = l.cid AND ipc.property_id = l.property_id AND ipc.lease_id = l.id )
						LEFT JOIN property_preferences pp ON ( pp.cid = rip.cid and pp.property_id = rip.property_id and pp.key = \'POLICY_ABOUT_TO_END_DAYS\' )
					WHERE
						ipc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
                        AND ipc.cid IN ( ' . implode( ',', $arrintCIds ) . ' )
                        AND rip.is_integrated = 1
                        AND ( ( rip.end_date IS NOT NULL AND rip.end_date > CURRENT_DATE + ( pp.value::INTERVAL ) 
                        OR  ( rip.end_date IS NULL AND rip.effective_date <= l.lease_start_date ) 
						OR  ( rip.end_date IS NOT NULL AND rip.effective_date <> rip.end_date AND rip.end_date > l.lease_start_date AND rip.effective_date <= l.lease_start_date AND rip.end_date > CURRENT_DATE )))';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePolicyByLeaseIdByCidByPolicyStatusTypeIds( $intLeaseId, $intPropertyId, $intCid, $arrintInsurancePolicyStatusTypeIds, $objDatabase ) {
		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) || false == valArr( $arrintInsurancePolicyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						LEFT JOIN property_preferences pp ON ( pp.cid = ipc.cid AND pp.property_id = ipc.property_id AND pp.key = \'' . \CPropertyPreference::PAID_VERIFICATION . '\' AND pp.value = \'1\' )
				   WHERE
						ipc.lease_id = ' . ( int ) $intLeaseId . '
						AND ipc.cid = ' . ( int ) $intCid . '
						AND ipc.property_id = ' . ( int ) $intPropertyId . '
						AND rip.insurance_policy_type_id != ' . ( int ) CInsurancePolicyType::AFFORDABLE . '
						AND rip.insurance_policy_status_type_id IN ( ' . implode( ',', $arrintInsurancePolicyStatusTypeIds ) . ' )
						AND ( pp.id IS NULL OR ( ( rip.confirmed_on IS NOT NULL AND rip.insurance_policy_type_id = ' . \CInsurancePolicyType::CUSTOM . ' ) OR rip.insurance_policy_type_id <> ' . \CInsurancePolicyType::CUSTOM . ' ) )
					LIMIT 1';

		return self::fetchResidentInsurancePolicy( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByLeaseIdByCidByCustomerId( $intLeaseId, $intCid, $objDatabase, $intCustomerId = NULL ) {
		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strAndCustomerIdCondition = ( false == empty( $intCustomerId ) ) ? ' AND ipc.customer_id = ' . ( int ) $intCustomerId : '';

		$strSql = 'WITH rip_data AS ( SELECT
	                   max ( rip.id ) OVER ( PARTITION BY ipc.lease_id, insurance_policy_type_id <>  ' . ( int ) CInsurancePolicyType::AFFORDABLE . '  ) AS max_policy_id,
	                   rip.*,
	                   ipc.lease_id,
	                   CASE
	                     WHEN insurance_carrier_name = \'Markel\' THEN \'RI-Markel\'
	                     WHEN insurance_carrier_name = \'Kemper\' THEN \'RI-Kemper\'
	                     WHEN insurance_carrier_name = \'QBE\' THEN \'RI-QBE\'
	                     ELSE insurance_carrier_name
	                   END AS insurance_carrier_name
	               FROM
	                   resident_insurance_policies rip
	                   JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
	                   LEFT JOIN property_preferences pp ON ( pp.cid = ipc.cid AND pp.property_id = ipc.property_id AND pp.key = \'' . \CPropertyPreference::PAID_VERIFICATION . '\' AND pp.value = \'1\' )
	               WHERE
	                   ipc.is_invalid_match <> 1
	                   AND ipc.lease_id = ' . ( int ) $intLeaseId . '
	                   AND rip.cid = ' . ( int ) $intCid . ' 
	                   AND ( pp.id IS NULL OR ( ( rip.confirmed_on IS NOT NULL AND rip.insurance_policy_type_id = ' . \CInsurancePolicyType::CUSTOM . ' ) OR rip.insurance_policy_type_id <> ' . \CInsurancePolicyType::CUSTOM . ' ) )
	                   ' . $strAndCustomerIdCondition . ' )
				SELECT
				    *   
				FROM
				    rip_data
				WHERE
				    rip_data.id = rip_data.max_policy_id
				    AND rip_data.lease_id = ' . ( int ) $intLeaseId . '
				    AND rip_data.cid = ' . ( int ) $intCid . '';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurancePoliciesByApplicationIdByApplicantIdByCid( $intApplicationId, $intApplicantId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intApplicantId ) || false == is_numeric( $intApplicationId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}

		$strSql = 'WITH rip_data AS (
					      SELECT
					          max ( rip.id ) OVER ( PARTITION BY ipc.lease_id, insurance_policy_type_id <> ' . ( int ) CInsurancePolicyType::AFFORDABLE . ' ) AS max_policy_id,
					          rip.*,
					          ipc.application_id,
					          ipc.applicant_id
					      FROM
					          resident_insurance_policies rip
					          JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
					          LEFT JOIN property_preferences pp ON ( pp.cid = ipc.cid AND pp.property_id = ipc.property_id AND pp.key = \'' . \CPropertyPreference::PAID_VERIFICATION . '\' AND pp.value = \'1\' )
					      WHERE
					          ipc.application_id = ' . ( int ) $intApplicationId . '
					          AND ipc.applicant_id = ' . ( int ) $intApplicantId . '
					          AND ( pp.id IS NULL OR ( ( rip.confirmed_on IS NOT NULL AND rip.insurance_policy_type_id = ' . \CInsurancePolicyType::CUSTOM . ' ) OR rip.insurance_policy_type_id <> ' . \CInsurancePolicyType::CUSTOM . ' ) )
					          AND ipc.cid = ' . ( int ) $intCid . ' )
					SELECT
					    *
					FROM
					    rip_data
					WHERE
						rip_data.id = rip_data.max_policy_id
						AND rip_data.application_id = ' . ( int ) $intApplicationId . '
						AND rip_data.applicant_id = ' . ( int ) $intApplicantId . '
						AND rip_data.cid = ' . ( int ) $intCid . '';

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchPoliciesByPolicyTypeIdByPolicyStatusTypeIdByLeaseStatusTypeIdsByCids( $intInsurancePolicyTypeId, $intInsurancePolicyStatusTypeId, $arrintNonActiveLeaseIds, $arrintTestCids = NULL, $objDatabase ) {

		if( false == valArr( $arrintNonActiveLeaseIds ) ) {
			return NULL;
		}

		$strCondition = ( true == valArr( $arrintTestCids ) ) ? ' AND rip.cid NOT IN ( ' . implode( ',', $arrintTestCids ) . ' ) ' : '';

		$strSql = 'SELECT
						rip.id as rip_id,
						rip.cid,
						ipc.lease_id,
						rip.policy_number,
						rip.effective_date,
						rip.end_date,
						cl.lease_end_date,
						cl.lease_status_type_id
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
                        JOIN cached_leases cl ON( cl.id = ipc.lease_id AND cl.cid = rip.cid )
				   WHERE
                        ipc.lease_id IS NOT NULL
						AND rip.insurance_policy_type_id = ' . ( int ) $intInsurancePolicyTypeId . '
						AND rip.insurance_policy_status_type_id = ' . ( int ) $intInsurancePolicyStatusTypeId . '
						AND (
							( cl.lease_status_type_id NOT IN( ' . sqlIntImplode( $arrintNonActiveLeaseIds ) . ' ) AND cl.lease_end_date IS NOT NULL AND ( cl.lease_end_date + INTERVAL \'30 day\' ) != rip.end_date )
							OR ( cl.lease_status_type_id IN( ' . sqlIntImplode( $arrintNonActiveLeaseIds ) . ' ) AND cl.lease_end_date IS NOT NULL AND ( cl.lease_end_date ) != rip.end_date )
							OR ( cl.lease_end_date IS NULL AND rip.end_date <= CURRENT_DATE + INTERVAL \'5 DAY\' )
							OR rip.end_date IS NULL
						) ' . $strCondition . '';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPoliciesByPolicyStatusTypeIdsByPolicyTypeIdsByLeaseStatusIdsByPropertyIdsByCids( $arrintInsuracePolicyStatusTypeIds, $intInsurancePolicyTypeId, $arrintLeaseStatusTypeIds, $arrintPropertyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) || false == valArr( $arrintInsuracePolicyStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						fpp.lease_id,
						cl.primary_customer_id AS customer_id,
						rip.*
					FROM
						resident_insurance_policies rip
						JOIN forced_placed_policies fpp ON ( rip.cid = fpp.cid AND rip.id = fpp.resident_insurance_policy_id )
						JOIN cached_leases cl ON( cl.cid = fpp.cid AND cl.property_id = fpp.property_id AND cl.id = fpp.lease_id  )
					WHERE
						rip.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND rip.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND fpp.is_disabled = false
						AND fpp.ar_transaction_id IS NULL
						AND rip.insurance_policy_status_type_id IN ( ' . sqlIntImplode( $arrintInsuracePolicyStatusTypeIds ) . ' )
						AND rip.insurance_policy_type_id = ' . ( int ) $intInsurancePolicyTypeId . '
						AND fpp.first_notice_sent_on IS NULL
						AND cl.lease_status_type_id IN ( ' . implode( ', ', $arrintLeaseStatusTypeIds ) . ' )
						AND rip.effective_date <= CURRENT_DATE';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomResidentInsurancePoliciesByLeaseIdsByCid( $arrintLeaseId, $intCid, $objDatabase ) {
		if( false === valArr( $arrintLeaseId ) || false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.*,
						c.name_first,
						c.name_last,
						ipc.lease_id
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
						JOIN customers c ON ( c.id = ipc.customer_id AND c.cid = ipc.cid )
				   WHERE
						ipc.lease_id IN ( ' . sqlIntImplode( $arrintLeaseId ) . ' )
						AND ipc.cid = ' . ( int ) $intCid;

		return self::fetchResidentInsurancePolicies( $strSql, $objDatabase );
	}

	public static function fetchLatestActiveResidentInsurancePolicyEndDateByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( false === valId( $intLeaseId ) || false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.end_date
					FROM
						resident_insurance_policies rip
						JOIN insurance_policy_customers ipc ON ( rip.id = ipc.resident_insurance_policy_id AND rip.cid = ipc.cid )
				   WHERE
						ipc.lease_id = ' . ( int ) $intLeaseId . '
						AND ipc.cid = ' . ( int ) $intCid . '
						AND rip.insurance_policy_type_id != ' . CInsurancePolicyType::AFFORDABLE . '
						AND rip.insurance_policy_status_type_id IN ( ' . sqlIntImplode( CInsurancePolicyStatusType::$c_arrintAllActiveInsurancePolicyStatusTypeIds ) . ' )
					ORDER BY rip.end_date DESC
				LIMIT 1';

		$arrstrData = fetchData( $strSql, $objDatabase );

		$strEndDate = '';
		if( true === valArr( $arrstrData ) && true === valStr( $arrstrData[0]['end_date'] ) ) {
			$strEndDate = $arrstrData[0]['end_date'];
		}
		return $strEndDate;
	}

	public static function fetchCustomActiveResidentInsurancePoliciesDetailsByCustomerIdByLeaseIdByPropertyIdByCid( $intCustomerId, $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		if( false === valId( $intCustomerId ) || false === valId( $intLeaseId ) || false === valId( $intPropertyId ) || false === valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						rip.id
					FROM
						cached_leases l
                        JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id AND lc.property_id = l.property_id )
                        JOIN applications app ON ( app.cid = l.cid AND app.property_id = l.property_id AND l.active_lease_interval_id = app.lease_interval_id AND app.lease_id = lc.lease_id )
                        JOIN resident_insurance_policies rip ON ( l.cid = rip.cid )
                        JOIN insurance_policy_customers ipc ON ( l.id = ipc.lease_id AND l.cid = ipc.cid AND rip.id = ipc.resident_insurance_policy_id AND lc.customer_id = ipc.customer_id  )
  	                    LEFT JOIN property_preferences pp ON ( l.cid = pp.cid AND l.property_id = pp.property_id AND pp.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
				   WHERE
						CASE
							WHEN 
							l.move_in_date IS NOT NULL THEN
							(
							    ( rip.end_date IS NULL AND rip.effective_date <= CURRENT_DATE AND l.move_in_date <= CURRENT_DATE ) 
							    OR 
							    ( rip.end_date IS NULL AND rip.effective_date >= CURRENT_DATE AND rip.effective_date <= l.move_in_date )
							    OR 
							    ( rip.end_date IS NOT NULL AND rip.effective_date <> rip.end_date AND l.move_in_date<= CURRENT_DATE AND rip.effective_date <= l.move_in_date AND rip.end_date > l.move_in_date AND rip.end_date >= CURRENT_DATE )       
							    OR 
							    ( rip.end_date IS NOT NULL AND rip.effective_date <> rip.end_date AND rip.effective_date<= CURRENT_DATE AND rip.end_date >= CURRENT_DATE AND rip.end_date >= l.move_in_date ) 
							    OR
							    ( rip.end_date IS NOT NULL AND rip.effective_date <> rip.end_date AND CURRENT_DATE <= l.move_in_date AND rip.effective_date<= l.move_in_date AND rip.end_date >= l.move_in_date ) 
							 )
							WHEN 
							l.lease_start_date IS NOT NULL THEN
							(
							    ( rip.end_date IS NULL AND rip.effective_date <= CURRENT_DATE AND l.lease_start_date <= CURRENT_DATE ) 
							    OR 
							    ( rip.end_date IS NULL AND rip.effective_date >= CURRENT_DATE AND rip.effective_date <= l.lease_start_date )
							    OR 
							    ( rip.end_date IS NOT NULL AND rip.effective_date <> rip.end_date AND l.lease_start_date<= CURRENT_DATE AND rip.effective_date <= l.lease_start_date AND rip.end_date > l.lease_start_date AND rip.end_date >= CURRENT_DATE )       
							    OR 
							    ( rip.end_date IS NOT NULL AND rip.effective_date <> rip.end_date AND rip.effective_date<= CURRENT_DATE AND rip.end_date >= CURRENT_DATE AND rip.end_date >= l.lease_start_date ) 
							    OR
							    ( rip.end_date IS NOT NULL AND rip.effective_date <> rip.end_date AND CURRENT_DATE <= l.lease_start_date AND rip.effective_date<= l.lease_start_date AND rip.end_date >= l.lease_start_date ) 
							)
							ELSE FALSE
							END  
		                    AND ( ( pp.value IS NULL ) OR ( CASE WHEN pp.value IS NOT NULL AND app.lease_type_id = ' . CLeaseType::CORPORATE . ' THEN FALSE ELSE TRUE END ) )
		                    AND ipc.customer_id = ' . ( int ) $intCustomerId . '
		                    AND ipc.lease_id = ' . ( int ) $intLeaseId . '
		                    AND ipc.property_id = ' . ( int ) $intPropertyId . '
							AND ipc.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAchUninsuredResidentsByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $arrmixUninsuredResidentFilters, $boolIsByUnit, $objDatabase, $boolIsComplianceEmail = false, $arrintLeaseIdsToExcludeFromFirstNotice = [] ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		if( true == valArr( $arrmixUninsuredResidentFilters ) ) {
			$strWhereParameters = '';

			$strLatestExpiryPolicy = '';

			if( true == isset( $arrmixUninsuredResidentFilters['latest_expired_policy'] ) ) {
				$strLatestExpiryPolicy = $arrmixUninsuredResidentFilters['latest_expired_policy'];

				unset( $arrmixUninsuredResidentFilters['latest_expired_policy'] );
			}

			// When unit vs occupant setting is set, will not check replaced_insurance_policy_id condition
			if( false == $boolIsByUnit && true == array_key_exists( 'replaced_insurance_policy_id', $arrmixUninsuredResidentFilters ) ) {
				unset( $arrmixUninsuredResidentFilters['replaced_insurance_policy_id'] );
			}

			foreach( $arrmixUninsuredResidentFilters as $strUninsuredResidentFilter ) {
				$strWhereParameters .= $strUninsuredResidentFilter;
			}
		}

		$strSql           = '';
		$strWhereLeaseIds = '';

		if( true == valArr( $arrintLeaseIdsToExcludeFromFirstNotice ) ) {
			$arrintLeaseIdsToExcludeFromFirstNotice = array_unique( $arrintLeaseIdsToExcludeFromFirstNotice );
			$strSql .= 'CREATE TEMP TABLE temp_policy_lease_data( cid, lease_id ) ON COMMIT DROP AS  ( values ' . sqlIntMultiImplode( $arrintLeaseIdsToExcludeFromFirstNotice ) . ' );';

			$strWhereLeaseIds = 'AND l.id NOT IN ( SELECT lease_id from temp_policy_lease_data )';
		}

		$strSql .= 'WITH lease_intervals_data AS(
                                                SELECT
													*,
													row_number() OVER ( PARTITION BY lease_id, cid ORDER BY id asc ) AS first_lease_interval
												FROM
													lease_intervals lii
												WHERE
													( lii.cid, lii.lease_id ) IN (
																					SELECT
																						cid,
																						lease_id
																					FROM
																						lease_intervals
																					WHERE
																						lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
																						AND lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . '
																						AND property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
																						AND cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
																					)
                                               )
                    SELECT
                        *
					FROM
                        (
                        SELECT
                            min ( data.applicant_id ) OVER ( PARTITION BY data.name_first, data.name_last, data.lease_id, data.move_in_date, data.email_address, data.property_id, data.cid, data.applicant_email_address ) AS min_applicant_id, -- because we found multiple applicants with one customer id
                            *
                        FROM
                            ( SELECT
									DISTINCT( c.id ) as customer_id,
									ipc.resident_insurance_policy_id,
									ipc.is_invalid_match,
									l.id as lease_id,
									l.primary_customer_id,
									rip.id AS resident_insure_policy_id,
									rip.effective_date,
									rip.end_date,
									rip.lapsed_on,
									rip.insurance_policy_id,
									rip.insurance_carrier_name,
									rip.policy_number,
									rip.insurance_policy_status_type_id,
									rip.is_integrated,
									rip.insurance_policy_status_type_id = ANY(\'{' . CInsurancePolicyStatusType::ACTIVE . ',' . CInsurancePolicyStatusType::CANCEL_PENDING . '}\'::int[]) AS active_or_cancel_pending_status,
									c.name_last,
									c.name_first,
									c.email_address,
									c.phone_number,
									c.primary_street_line1,
									c.primary_city,
									c.primary_state_code,
									c.primary_postal_code,
									func_format_customer_name( c.name_first, c.name_last ) AS customer_name,
									lc.lease_status_type_id,
									l.cid,
			                        l.property_id,
			                        l.property_unit_id,
			                        l.unit_space_id AS unit_space_number,
			                        l.unit_number_cache AS unit_number,
			                        l.move_in_date,
			                        l.move_out_date,
			                        l.lease_start_date,
			                        li.lease_start_date as original_lease_start_date,
			                        l.active_lease_interval_id,
			                        l.lease_interval_type_id,
			                        fpp.policy_effective_date,
			                        fpp.id AS forced_placed_policy_id,
			                        a.id as applicant_id,
			                        a.username as applicant_email_address,
			                        row_number ( ) OVER ( PARTITION BY lc.id
												    ORDER BY
												        CASE 
															WHEN rip.insurance_policy_status_type_id IN ( ' . CInsurancePolicyStatusType::ACTIVE . ',  ' . CInsurancePolicyStatusType::CANCEL_PENDING . ' ) THEN 1
															ELSE 2
														END, rip.end_date DESC ) AS latest_expired_policy
								FROM
									cached_leases l';

		if( true == $boolIsByUnit ) {
			$strSql .= ' JOIN lease_customers lc ON ( l.primary_customer_id = lc.customer_id AND lc.cid = l.cid AND lc.lease_id = l.id )
						 LEFT JOIN insurance_policy_customers ipc ON( l.id = ipc.lease_id AND l.cid = ipc.cid )
						 LEFT JOIN customers c ON ( l.primary_customer_id = c.id AND l.cid = c.cid  )
						 LEFT JOIN applicants a ON ( a.cid = l.cid AND a.customer_id = l.primary_customer_id )';
		} else {
			$strSql .= ' JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id )
						 LEFT JOIN insurance_policy_customers ipc ON( lc.lease_id = ipc.lease_id AND lc.cid = ipc.cid  and lc.customer_id = ipc.customer_id )
						 LEFT JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid  )
						 LEFT JOIN applicants a ON ( a.cid = lc.cid AND a.customer_id = lc.customer_id )
						 LEFT JOIN customer_relationships cr ON( lc.cid = cr.cid AND lc.customer_relationship_id = cr.id AND lc.customer_type_id = cr.customer_type_id )';
		}

		if( true == $boolIsComplianceEmail ) {
			$strSql .= ' JOIN property_products pp ON ( pp.property_id = l.property_id AND pp.cid = l.cid AND pp.ps_product_id = ' . CPsProduct::RESIDENT_INSURE . ' )';
		}

		$strSql .= ' LEFT JOIN resident_insurance_policies rip ON ( ipc.resident_insurance_policy_id = rip.id AND rip.cid = ipc.cid )
					 LEFT JOIN forced_placed_policies fpp ON ( fpp.cid = l.cid AND fpp.lease_id = l.id AND fpp.is_disabled = FALSE)
					 LEFT JOIN properties p ON ( l.property_id = p.id AND l.cid = p.cid )
					 LEFT JOIN applications app ON ( app.cid = l.cid AND app.property_id = l.property_id AND l.active_lease_interval_id = app.lease_interval_id )
					 LEFT JOIN property_preferences ppr ON ( l.cid = ppr.cid AND l.property_id = ppr.property_id AND ppr.key = \'REQUIRE_INSURANCE_FOR_ALL_OCCUPIED_UNITS_EFFECTIVE_DATE\' )
				     LEFT JOIN property_preferences ppr1 ON ( l.cid = ppr1.cid AND l.property_id = ppr1.property_id AND ppr1.key = \'BY_PASS_INSURANCE_FOR_CORPORATE_LEAD\')
				     LEFT JOIN lease_intervals_data  li ON ( li.cid = l.cid AND li.lease_id = l.id AND li.property_id = l.property_id AND first_lease_interval = 1 AND l.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' )  
					WHERE
						l.occupancy_type_id <> ' . COccupancyType::COMMERCIAL . '
						 ' . $strWhereLeaseIds . '
						AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND l.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ' . $strWhereParameters . ' ) AS data ' . $strLatestExpiryPolicy . '
					    ) AS uninsured_data
					WHERE uninsured_data.min_applicant_id = uninsured_data.applicant_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>