<?php

class CRevenuePostRule extends CBaseRevenuePostRule {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostRuleTarget() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostRuleMethod() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostRuleValue() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valPostRuleCategory() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostRuleRentSource() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostRuleRentType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostRuleBoundary() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function setDisplayPercentageValues() {
    	$this->setPostRuleValue( $this->getPostRuleValue() * 100 );
    }

    public function setMathematicalPercentageValues() {
    	$this->setPostRuleValue( number_format( $this->getPostRuleValue() / 100, 6 ) );
    }

}
?>