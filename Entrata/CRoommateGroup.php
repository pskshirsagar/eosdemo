<?php

class CRoommateGroup extends CBaseRoommateGroup {

    /**
     * Create Functions
     */

    public function createRoommate() {

    	$objRoommate = new CRoommate();
    	$objRoommate->setCid( $this->getCid() );
    	$objRoommate->setParentLeaseId( $this->getLeaseId() );
    	$objRoommate->setLeaseId( $this->getLeaseId() );

    	return $objRoommate;
    }

	/**
	 * Val Functions
	 */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
        	$boolIsValid = false;
        	trigger_error( __( 'Client id required.' ), E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;

        if( true == is_null( $this->getApplicationId() ) || 0 >= ( int ) $this->getApplicationId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Application does not exist.' ) ) );
        }

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

        if( true == is_null( $this->getLeaseId() ) || 0 >= ( int ) $this->getLeaseId() ) {
        	$boolIsValid = false;
        	trigger_error( __( 'Lease id required.' ), E_USER_ERROR );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
	        case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valApplicationId();
            	$boolIsValid &= $this->valLeaseId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default break
            	break;
        }

        return $boolIsValid;
    }

}
?>