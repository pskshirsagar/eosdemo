<?php

class CSocialPostType extends CBaseSocialPostType {

	const FACEBOOK          = 1;
	const TWITTER 	        = 2;
	const GOOGLE_MYBUSINESS = 3;
	const INSTAGRAM         = 4;

	const FACEBOOK_URL          = 'https://www.facebook.com';
	const TWITTER_URL           = 'https://www.twitter.com';
	const GOOGLE_MYBUSINESS_URL = 'https://mybusiness.googleapis.com/v4/';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApiKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApiSecret() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function getSocialPostTypeName() {
		return \Psi\CStringService::singleton()->ucfirst( \Psi\CStringService::singleton()->strtolower( $this->getName() ) );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public static function getSocialPostNameById( $intSocialPostTypeId ) {
		switch( $intSocialPostTypeId ) {
			case self::FACEBOOK:
				return 'facebook';

			case self::INSTAGRAM:
				return 'instagram';

			case self::TWITTER:
				return 'twitter';

			case self::GOOGLE_MYBUSINESS:
				return 'google';

			default:
				// No Default;
		}
	}

}
?>