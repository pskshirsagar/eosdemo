<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPaymentTypes
 * Do not add any new functions to this class.
 */

class CApPaymentTypes extends CBaseApPaymentTypes {

	public static function fetchApPaymentTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApPaymentType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchApPaymentType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApPaymentType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchApPaymentTypesByIds( $arrintApPaymentTypeIds, $objClientDatabase ) {
		if( false == valArr( $arrintApPaymentTypeIds ) ) return NULL;
		return self::fetchObjects( 'SELECT * FROM ap_payment_types WHERE id IN ( ' . implode( ',', $arrintApPaymentTypeIds ) . ' ) ORDER BY LOWER( name )', 'CApPaymentType', $objClientDatabase );
	}

	public static function fetchActiveApPaymentTypes( $objClientDatabase, $boolIsFromBulkVoidPayments = false, $boolIsExcludeVirtualCard = false, $boolIsExcludeCapitalOne = false ) {

		$strCondition			= '';

		if( true == $boolIsFromBulkVoidPayments ) {
			$strCondition = 'AND id NOT IN ( ' . CApPaymentType::BILL_PAY_ACH . ', ' . CApPaymentType::BILL_PAY_CHECK . ', ' . CApPaymentType::BILL_PAY . ' )';
		}

		if( true == $boolIsExcludeVirtualCard ) {
			$strCondition .= ' AND id NOT IN ( ' . CApPaymentType::VIRTUAL_CARD . ' ) ';
		}

		if( true == $boolIsExcludeCapitalOne ) {
			$strCondition .= ' AND id NOT IN ( ' . CApPaymentType::CAPITAL_ONE . ' ) ';
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payment_types
					WHERE
						is_published = 1 ' . $strCondition . '
					ORDER BY order_num';

		return parent::fetchApPaymentTypes( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApPaymentTypesWithCompanyApPaymentTypeAssociationsByCid( $intCid, $objClientDatabase, $boolIsExcludeVirtualCard = false ) {

		$strCondition			= '';

		if( true == $boolIsExcludeVirtualCard ) {
			$strCondition = ' AND apt.id NOT IN ( ' . \CApPaymentType::VIRTUAL_CARD . ' )';
		}

		$strSql = 'SELECT
						apt.*,
						capta.is_enabled
					FROM
						ap_payment_types apt
						JOIN company_ap_payment_type_associations capta ON ( capta.cid = ' . ( int ) $intCid . ' AND apt.id = capta.ap_payment_type_id )
					WHERE
						capta.is_enabled = TRUE
						  ' . $strCondition . '
					ORDER BY
						apt.name,
						capta.ap_payment_type_id';

		// System does not want cached objects as this is company level setting and should be reflected instantly. Therefore use parent::fetchApPaymentTypes().
		return parent::fetchApPaymentTypes( $strSql, $objClientDatabase );
	}

	/**
	 * @deprecated - Use fetchActiveApPaymentTypesWithCompanyApPaymentTypeAssociationsByCid instead.
	 */
	public static function fetchActiveApPaymentTypesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						capta.is_enabled,
						capta.ap_payment_type_id,
						apt.name
					FROM ap_payment_types apt
						JOIN company_ap_payment_type_associations capta ON (capta.cid = ' . ( int ) $intCid . ' AND apt.id = capta.ap_payment_type_id)
					WHERE
						capta.is_enabled = TRUE
					ORDER BY
						capta.ap_payment_type_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>