<?php

class CCompanyUserType extends CBaseCompanyUserType {

	const ENTRATA   = 2;
	const MARKETING = 6;
	const APP = 9;
	const BROKER_PORTAL = 10;
	const RESERVATION_HUB = 11;
	const TENANT_PORTAL = 12;
	const SYSTEM = 13;
	const ADMIN = 14;
	const CUSTOMER = 15;
	const API_USER = 16;

	public static $c_arrintPsiUserTypeIds = [
		self::SYSTEM,
		self::ADMIN
	];

	public static $c_arrintEntrataAndPsiUserTypeIds  = [
		self::SYSTEM,
		self::ADMIN,
		self::ENTRATA
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>