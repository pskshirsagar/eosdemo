<?php

class CCompanyEventResponse extends CBaseCompanyEventResponse {

	protected $m_intTermsAndConditions;

    /**
     * Create Functions
     */

	public function createEventResponseEmail( $objProperty, $strEventName, $objDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

    	$objCustomer = $this->fetchCustomer( $objDatabase );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_WORKS, false );

		$objSmarty->assign_by_ref( 'company_event_response',		$this );
		$objSmarty->assign_by_ref( 'event_name',					$strEventName );
		$objSmarty->assign_by_ref( 'customer',						$objCustomer );

		$objSmarty->assign( 'base_uri', 			CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'image_url',	        CONFIG_COMMON_PATH );
		$objSmarty->assign( 'entrata_website_url',  CONFIG_ENTRATA_WEBSITE_URL );

		$strHtmlEmailOutput = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/events/event_response_email.tpl' );

		// Create property manager email
		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setSubject( 'Received response on the event: ' . $strEventName );
		$objSystemEmail->setHtmlContent( addslashes( $strHtmlEmailOutput ) );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::EVENT_RESPONSE_NOTIFICATION );
		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setCid( $this->getCid() );

		return $objSystemEmail;

	}

	/**
	 * Get Functions
	 */

	public function getTermsAndConditions() {
		return $this->m_intTermsAndConditions;
	}

	/**
	 * Set Functions
	 */

	public function setTermsAndConditions( $intTermsAndConditions ) {
		$this->m_intTermsAndConditions = $intTermsAndConditions;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

       	if( true == isset( $arrValues['terms_and_conditions'] ) ) 	$this->setTermsAndConditions( $arrValues['terms_and_conditions'] );
        return;
    }

	/**
	 * Fetch Functions
	 */

	public function fetchCustomer( $objDatabase ) {

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->m_intCustomerId, $this->m_intCid, $objDatabase );
	}

    /**
     * Validation Functions
     */

    public function valResponse() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        if( true == is_null( $this->getResponse() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'response', 'Please insert a comment.' ) );
        }

        return $boolIsValid;
    }

	public function valTermsAndConditions() {

    	$boolIsValid = true;

        if( true == is_null( $this->getTermsAndConditions() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'You must agree to the terms and conditions.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valResponse();
            	$boolIsValid &= $this->valTermsAndConditions();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valResponse();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>