<?php

class CPropertyGreetingType extends CBasePropertyGreetingType {

	const LEAD 								= 1;
	const MAINTENANCE 						= 2;
	const MAINTENANCE_EMERGENCY 			= 3;
	const LEAD_AFTER_HOURS 					= 4;
	const MAINTENANCE_AFTER_HOURS 			= 5;
	const MAINTENANCE_EMERGENCY_AFTER_HOURS = 6;
	const LEAD_DURING_HOURS_MISSED_CALL 	= 7;
}
?>