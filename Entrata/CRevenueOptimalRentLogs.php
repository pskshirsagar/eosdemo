<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueOptimalRentLogs
 * Do not add any new functions to this class.
 */

class CRevenueOptimalRentLogs extends CBaseRevenueOptimalRentLogs {

	public static function fetchMonthlyOptimalRentsByPropertyIdByCid( $intCid, $intPropertyId, $intUnitTypeId, $boolPropertyLevel, $objDatabase ) {

		$strSqlCondition = '';
		if( false == $boolPropertyLevel ) {
			$strSqlCondition = ' And us.unit_type_id = ' . ( int ) $intUnitTypeId;
		}

		$strSql = '
				SELECT rorl.cid,
				      rorl.property_id,
				      avg(rorl.std_optimal_rent) AS optimal_rent,
				      DATE_PART(\'MONTH\', process_datetime) AS month_num,
				      DATE_PART(\'YEAR\', process_datetime) AS year_num
				FROM (
				      SELECT us.cid,
							us.property_id, us.unit_type_id, us.id AS unit_space_id,
							r.process_datetime,
							row_number() OVER(PARTITION BY us.unit_type_id, us.id,
							date_part(\'MONTH\', r.process_datetime), date_part(\'YEAR\', r.process_datetime)
				      ORDER BY date_part(\'DAY\', r.process_datetime) DESC, date_part(\'MONTH\', r.process_datetime)) AS row_num
					  FROM revenue_post_rents r
					  JOIN unit_spaces us ON ( r.cid = us.cid AND r.unit_type_id = us.unit_type_id  AND us.deleted_on IS NULL )
					  WHERE us.cid = ' . ( int ) $intCid . ' AND
							us.property_id = ' . ( int ) $intPropertyId . $strSqlCondition . ' AND
							r.process_datetime IS NOT NULL
							AND CASE WHEN r.unit_space_id IS NOT NULL
								THEN r.unit_space_id = us.id
								ELSE TRUE
							END
				      ORDER BY r.process_datetime DESC
				    ) AS rpr
				    JOIN revenue_optimal_rent_logs as rorl on 
				    rpr.cid = rorl.cid AND
				      rpr.property_id = rorl.property_id AND
				       rorl.unit_type_id = rpr.unit_type_id AND 
				       rpr.unit_space_id = rorl.unit_space_id AND
				       date (rpr.process_datetime) = date (rorl.log_datetime) AND
				       rpr.row_num = 1
				WHERE rorl.cid = ' . ( int ) $intCid . ' AND
				     rorl.property_id = ' . ( int ) $intPropertyId . ' AND
				     rorl.post_month BETWEEN CURRENT_DATE - INTERVAL \'13 Months\' AND CURRENT_DATE AND
				     rorl.std_optimal_rent > 0
				GROUP BY
				        rorl.cid,
				        rorl.property_id,
				        year_num,
				        month_num';

		return fetchData( $strSql, $objDatabase );

	}

}
?>