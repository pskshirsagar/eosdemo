<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMinRentExemptionTypes
 * Do not add any new functions to this class.
 */

class CSubsidyMinRentExemptionTypes extends CBaseSubsidyMinRentExemptionTypes {

	public static function fetchActiveSubsidyMinRentExemptionTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM subsidy_min_rent_exemption_types WHERE is_published = TRUE ORDER BY order_num';
		return self::fetchSubsidyMinRentExemptionTypes( $strSql, $objDatabase );
	}

}
?>