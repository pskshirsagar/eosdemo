<?php

class CCompanyGroupPreference extends CBaseCompanyGroupPreference {

	const MIN_TIME_OUT_VALUE                      = 5; // minutes
	const ENABLE_ENTRATA_CALENDAR                 = 'ENABLE_ENTRATA_CALENDAR';
	const ACCESSIBLE_ENTRATA_CALENDAR_EVENT_TYPES = 'ACCESSIBLE_ENTRATA_CALENDAR_EVENT_TYPES';


	public static $c_arrintCalendarSettingKeys = [
		self::ENABLE_ENTRATA_CALENDAR,
		self::ACCESSIBLE_ENTRATA_CALENDAR_EVENT_TYPES
	];

	/**
	 * Create Functions Get Functions
	 */

	public function getCompanyGroupPreferencesFields() {
		return [
			'default_to_resident_friendly'              => __( 'Default to Resident Friendly Mode' ),
			'default_ledger_to_show_reversals'          => __( 'Default Ledger To Show Reversals' ),
			'custom_entrata_timeout_switch'             => __( 'Override Company Entrata Timeout Setting' ),
			'custom_entrata_timeout'                    => __( 'Custom Entrata Timeout' ),
			'notification_panel_chat_user'              => __( 'Chat/Text Panel User' ),
			'show_call_notifications'                   => __( 'Show Call Notifications' ),
			'allow_residentworks_ads'                   => __( 'Allow Entrata Announcements' ),
			'enable_entrata_calendar'                   => __( 'Enable Entrata Calendar' ),
			'site_tablet_user'                          => __( 'Site Tablet User' ),
			'site_tablet_user_packages_only'            => __( 'Site Tablet Packages Only' ),
			'enable_send_bulk_email_sms'                => __( 'Enable Send Bulk Email/SMS' ),
			'accessible_entrata_calendar_event_types'	=> __( 'Accessible Entrata Calendar Event Types' )
		];
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

 	public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Company group preference id appears invalid.' ) );
        }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id does not appear to be valid.' ) );
        }

        return $boolIsValid;
    }

    public function valCompanyGroupId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCompanyGroupId ) || ( 1 > $this->m_intCompanyGroupId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_group_id', __( 'Company group id does not appear to be valid.' ) ) );
        }

        return $boolIsValid;
    }

	public function valKey() {
        $boolIsValid = true;

        if( false == isset( $this->m_strKey ) || ( false == $this->m_strKey ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Company user preference key must be provided.' ) );
        }

        return $boolIsValid;
    }

    public function valValue() {

        $boolIsValid = true;

        if( true == is_null( $this->getValue() ) || true == preg_match( '/^\s+/', $this->getValue() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', sprintf( __( '{%s, key} is required.', [ 'key' => \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( str_replace( '_', ' ', $this->m_strKey ) ) ) ] ) ) ) );
        } else {

        	if( true == \Psi\CStringService::singleton()->strstr( $this->m_strKey, 'CRAIGS_LIST_USERNAME' ) && false == CValidation::validateEmailAddresses( $this->getValue() ) ) {
	        	$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', sprintf( __( '{%s, key} not in valid format. (test@example.com)', [ 'key' => \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( str_replace( '_', ' ', $this->m_strKey ) ) ) ] ) ) ) );
	        }

        }

        return $boolIsValid;
    }

	public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valId();

            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyGroupId();
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valValue();
	            $boolIsValid &= ( ( 'CUSTOM_ENTRATA_TIMEOUT' == $this->getKey() )  ?  $this->valMaxLoginTimeouts( 'Entrata' ) : true );
				break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function valMaxLoginTimeouts( $strPortalName = '' ) {
		$boolIsValid 		= true;
		$intGcMaxlifetime 	= floor( ini_get( 'session.gc_maxlifetime' ) / 60 );
		if( 0 >= ( int ) $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL_VALUE', __( '{%s, portal_name} Timeout value should not be blank.', [ 'portal_name' => $strPortalName ] ) ) );
		} elseif( self::MIN_TIME_OUT_VALUE > trim( $this->getValue() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL_VALUE', __( '{%s, portal_name} Timeout value should be more than {%d, min_time_out_value} minutes.', [ 'portal_name' => $strPortalName, 'min_time_out_value' => self::MIN_TIME_OUT_VALUE ] ) ) );
		} elseif( $intGcMaxlifetime < trim( $this->getValue() ) ) {
			$boolIsValid = false;

			if( 60 <= $intGcMaxlifetime ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'MAX_TIMEOUT_VALUE', __( '{%s, portal_name} Timeout value should not exceed {%d, limit} hours.', [ 'portal_name' => $strPortalName, 'limit' => ( ( int ) $intGcMaxlifetime / 60 ) ] ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'MAX_TIMEOUT_VALUE', __( '{%s, portal_name} Timeout value should not exceed {%d, limit} minutes.', [ 'portal_name' => $strPortalName, 'limit' => ( int ) $intGcMaxlifetime ] ) ) );
			}
		}

		return $boolIsValid;
	}

}
?>