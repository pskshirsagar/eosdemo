<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CClubs
 * Do not add any new functions to this class.
 */

class CClubComments extends CBaseClubComments {

    public static function fetchClubCommentByIdByCid( $intId, $intCid, $objDatabase ) {
        return self::fetchClubComment( sprintf( 'SELECT * FROM club_comments WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
    }

	public static function fetchClubCommentByClubIdByCid( $intClubId, $intCid, $objDatabase, $boolIsReturnKeyedArray = true ) {
		if( false == valId( $intClubId ) ) return NULL;
		$strSql = 'SELECT
                        * 
                    FROM 
                        club_comments 
                    WHERE 
                        club_id = ' . ( int ) $intClubId . ' 
                        AND cid = ' . ( int ) $intCid . '
                        ORDER BY id DESC';

		return parent::fetchObjects( $strSql, CClubComment::class, $objDatabase, $boolIsReturnKeyedArray );
	}

    public static function fetchClubCommentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsReturnKeyedArray = true ) {
        if( false == valId( $intPropertyId ) ) return NULL;
        $strSql = ' SELECT
						*
					FROM
						Club_Comments
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND deleted_on IS NULL
						ORDER BY id DESC';
        return parent::fetchObjects( $strSql, CClubComment::class, $objDatabase, $boolIsReturnKeyedArray );
    }

}

?>