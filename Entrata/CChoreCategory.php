<?php

class CChoreCategory extends CBaseChoreCategory {

	const GENERAL		= 1;
	const AR_CLOSE		= 2;
	const AP_CLOSE		= 3;
	const GL_CLOSE		= 4;
	const SUPPORT_TICKET = 5;

	public static $c_arrintClosingChoreCategories = [ self::AR_CLOSE,
													self::AP_CLOSE,
													self::GL_CLOSE ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function assignSmartyData( $objSmarty ) {

		$objSmarty->assign( 'CHORE_CATEGORY_AR_CLOSE',		self::AR_CLOSE );
		$objSmarty->assign( 'CHORE_CATEGORY_AP_CLOSE',		self::AP_CLOSE );
		$objSmarty->assign( 'CHORE_CATEGORY_GL_CLOSE',		self::GL_CLOSE );
	}

}
?>