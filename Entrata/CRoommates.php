<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommates
 * Do not add any new functions to this class.
 */

class CRoommates extends CBaseRoommates {

	public static function fetchRoommateInvitationsByCidByLeaseIdsByPropertyId( $intCid, $arrintLeaseId, $intPropertyId, $objDatabase, $boolIsReturnObj = false, $intParentLeaseId = NULL, $intPropertyFloorplanId = NULL, $intLeaseStartWindowId = NULL ) {

		if( false == valArr( $arrintLeaseId ) || false == is_numeric( $intCid ) || false == is_numeric( $intPropertyId ) ) return NULL;
		$strWhrSql = ( true == valId( $intParentLeaseId ) ) ? ' AND r.parent_lease_id = ' . $intParentLeaseId : '';

		$strWhereCondition = '';
		$strJoinSql        = '';
		if( true == valId( $intPropertyFloorplanId ) && true == valId( $intLeaseStartWindowId ) ) {
			$strWhereCondition = ' AND fplt_details.is_from_different_fp_lt IS FALSE';

			$strJoinSql = ' JOIN LATERAL 
							(
							  SELECT
							      CASE
							        WHEN a.property_floorplan_id <> ' . ( int ) $intPropertyFloorplanId . ' OR li.lease_start_window_id <> ' . ( int ) $intLeaseStartWindowId . ' THEN TRUE
							        ELSE FALSE
							      END AS is_from_different_fp_lt
							  FROM
							      applications a
							      JOIN lease_intervals li ON ( li.cid = a.cid AND li.lease_id = a.lease_id AND li.id = a.lease_interval_id )
							  WHERE
							      a.cid = r.cid
							      AND a.lease_id = r.parent_lease_id
							      AND a.application_status_id <> ' . CApplicationStatus::CANCELLED . '
							      AND li.lease_interval_type_id <> ' . CLeaseIntervalType::LEASE_MODIFICATION . '
							      AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
							  ORDER BY
							      a.id DESC
							  LIMIT
							      1
							) AS fplt_details ON TRUE';
		}

		$strSql = 'SELECT
						r.*,
						c.name_first,
						c.name_last,
						c.name_full as customer_name_full,
						cps.username,
						lc.customer_type_id
					FROM
						roommates r 
						JOIN lease_customers lc ON ( lc.cid = r.cid AND lc.lease_id = r.parent_lease_id AND lc.customer_type_id = ' . CCustomerType::PRIMARY . ' )
						JOIN customers c ON ( c.cid  = lc.cid AND lc.customer_id = c.id )
						JOIN customer_portal_settings cps ON ( cps.cid = c.cid AND c.id = cps.customer_id )
						' . $strJoinSql . '
						JOIN LATERAL
						(
							SELECT
								count( r1.id ) AS is_active_parent
							FROM
								roommates r1
							WHERE
								r.cid = r1.cid
								AND r.roommate_group_id = r1.roommate_group_id
								AND r.parent_lease_id = r1.lease_id
								AND r1.deleted_by IS NULL
								AND r1.deleted_on IS NULL
						) AS parent_r ON TRUE
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND lc.property_id = ' . ( int ) $intPropertyId . '
						AND r.lease_id IN ( ' . implode( ',', $arrintLeaseId ) . ' )
						AND r.confirmed_on IS NULL
						' . $strWhrSql . '
						AND r.deleted_on IS NULL
						AND r.parent_lease_id <> r.lease_id
						AND lc.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ' )
						' . $strWhereCondition . '
						AND parent_r.is_active_parent > 0
					ORDER BY r.id DESC';

		if( true == $boolIsReturnObj ) {
			return self::fetchRoommates( $strSql, $objDatabase );
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRoommateInvitationsOnApplicationCompletionByCidByPropertyIdByPropertyFloorplanIdByLeaseTermIdByEmailAddressByApplicationId( $intCid, $intPropertyId, $intPropertyFloorplanId, $intLeaseTermId, $strEmailAddress, $intApplicationId, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intPropertyFloorplanId ) || false == is_numeric( $intLeaseTermId ) || false == is_numeric( $intCid ) || false == valStr( $strEmailAddress ) || false == valId( $intApplicationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( r.id )
						r.*
					FROM
						roommates r
						JOIN leases l ON( l.cid = r.cid AND l.id = r.parent_lease_id )
						JOIN cached_applications ca ON ( ca.cid = r.cid AND ca.lease_id = l.id AND ca.property_id = l.property_id AND ca.property_id =' . ( int ) $intPropertyId . ' AND ca.property_floorplan_id =' . ( int ) $intPropertyFloorplanId . ' AND ca.lease_term_id =' . ( int ) $intLeaseTermId . ')
					 WHERE 
						r.cid=' . ( int ) $intCid . '
						AND trim( both \' \' from lower( r.email_address ) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strEmailAddress ) ) ) . '\'
						AND r.application_id = ' . ( int ) $intApplicationId . '
						AND r.confirmed_by IS NULL
						AND r.deleted_by IS NULL
						AND r.lease_id IS NULL';

		return self::fetchRoommates( $strSql, $objDatabase );
	}

	public static function fetchRoommateAcceptedInvitationByCidByEmailAddressByLeaseIdByApplicationId( $intCid, $strEmailAddress, $intLeaseId, $intApplicationId, $objDatabase ) {

		if( false == is_numeric( $intLeaseId ) || false == valStr( $strEmailAddress ) || false == is_numeric( $intCid ) || false == valId( $intApplicationId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						roommates
					WHERE
						cid=' . ( int ) $intCid . '
						AND lease_id =' . ( int ) $intLeaseId . '
						AND trim( both \' \' from lower( email_address ) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strEmailAddress ) ) ) . '\'
						AND application_id = ' . ( int ) $intApplicationId . '
						AND confirmed_on IS NOT NULL
						AND deleted_on IS NULL';

		return self::fetchRoommate( $strSql, $objDatabase );
	}

	public static function fetchCustomRoommatesByCidByRoommateGroupId( $intCid, $intRoommateGroupId, $objDatabase, $boolIncludeDeleted = true ) {

		if( false == is_numeric( $intRoommateGroupId ) || false == is_numeric( $intCid ) ) return NULL;
		$strWhr = '';
		if( false == $boolIncludeDeleted ) {
			$strWhr = ' AND r.deleted_by IS NULL ';
		}

		$strSql = 'SELECT
						r.*,
						c.name_first,
						c.name_last,
						c.name_full as customer_name_full,
						cpn.phone_number
					FROM
						roommates r
						LEFT JOIN lease_customers lc ON ( lc.cid = r.cid AND lc.lease_id = r.lease_id AND lc.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . ' )
						LEFT JOIN customers c ON ( c.cid = lc.cid AND c.id = lc.customer_id )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.cid = c.cid AND cpn.customer_id = c.id AND cpn.is_primary = TRUE AND cpn.deleted_on IS NULL AND cpn.deleted_by IS NULL )
					WHERE
						r.cid =' . ( int ) $intCid . '
						AND r.roommate_group_id =' . ( int ) $intRoommateGroupId . '
						' . $strWhr . '
					ORDER BY r.id ASC';

		return self::fetchRoommates( $strSql, $objDatabase );
	}

	public static function fetchNewPrimaryRoommateByCidByRoommateGroupId( $intCid, $intRoommateGroupId, $objDatabase ) {

		if( false == is_numeric( $intRoommateGroupId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						roommates
					WHERE
						cid =' . ( int ) $intCid . '
						AND roommate_group_id =' . ( int ) $intRoommateGroupId . '
						AND deleted_on IS NULL
						AND ( parent_lease_id <> lease_id )
					ORDER BY confirmed_on ASC
					LIMIT 1';

		return self::fetchRoommate( $strSql, $objDatabase );
	}

	public static function fetchOtherInvitationConfirmedRoommatesByCidByRoommateGroupId( $intCid, $intRoommateGroupId, $objDatabase ) {

		if( false == is_numeric( $intRoommateGroupId ) || false == is_numeric( $intCid ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						r.*,
						c.name_first,
						c.name_last,
						c.name_full as customer_name_full,
						cpn.phone_number
					FROM
						roommates r
						LEFT JOIN leases l ON ( l.cid = r.cid AND l.id = r.lease_id )
						LEFT JOIN customers c ON ( c.cid = l.cid AND c.id = l.primary_customer_id )
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.cid = c.cid AND cpn.customer_id = c.id AND cpn.is_primary = TRUE AND cpn.deleted_on IS NULL AND cpn.deleted_by IS NULL )
					WHERE
						r.cid =' . ( int ) $intCid . '
						AND r.roommate_group_id =' . ( int ) $intRoommateGroupId . '
						AND r.confirmed_on IS NOT NULL
						AND r.deleted_on IS NULL
					ORDER BY id';

		return self::fetchRoommates( $strSql, $objDatabase );
	}

	public static function fetchRoommatesCountByCidByRoommateGroupId( $intCid, $intRoommateGroupId, $boolIsInvitationAccepted = false, $objDatabase ) {

		if( false == is_numeric( $intRoommateGroupId ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhereSql = 'WHERE
							cid =' . ( int ) $intCid . '
							AND roommate_group_id =' . ( int ) $intRoommateGroupId . '
							AND deleted_on IS NULL';
		$strWhereSql .= ( true == $boolIsInvitationAccepted ) ? ' AND confirmed_by IS NOT NULL AND confirmed_on IS NOT NULL' : '';

		return self::fetchRoommateCount( $strWhereSql, $objDatabase );
	}

	public static function fetchConfirmRoommateByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIsIncludeConfimed = true ) {

		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) return NULL;
		$strWhrSql = ' AND r.confirmed_on IS NOT NULL ';
		if( false == $boolIsIncludeConfimed ) {
			$strWhrSql = '';
		}

		$strSql = 'SELECT
						*
					FROM
						roommates r
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND r.lease_id = ' . ( int ) $intLeaseId . '
						' . $strWhrSql . '
						AND r.deleted_by IS NULL
					ORDER BY r.id desc
					LIMIT 1';

		return self::fetchRoommate( $strSql, $objDatabase );
	}

	public static function fetchRoommatesDetailsByGroupIdByCid( $intGroupId, $intCid, $objDatabase, $boolIsFromProspectPortalRoommatesStep = false, $intPropertyFloorplanId = NULL, $intLeaseStartWindowId = NULL ) {

		if( false == is_numeric( $intGroupId ) || false == is_numeric( $intCid ) ) return NULL;

		if( true == $boolIsFromProspectPortalRoommatesStep ) {
			$strSelectSql  = 'a.id AS invited_roommate_applicant_id, a.name_first, a.name_last, concat( a.name_last, \', \', a.name_first ) AS customer_name_full, r.created_on, r.is_invitation_rejected, cpn.phone_number, CASE WHEN e.event_datetime IS NULL THEN r.created_on ELSE e.event_datetime END AS last_invitation_sent_on';
			$strJoinSql    = '  LEFT JOIN applications ap ON ( r.cid = ap.cid AND r.application_id = ap.id )
								LEFT JOIN applicant_applications aa ON ( ap.cid = aa.cid AND ap.id = aa.application_id AND aa.applicant_id = ap.primary_applicant_id )
								LEFT JOIN applicants a ON ( aa.cid = a.cid AND aa.applicant_id = a.id )
                                LEFT JOIN events e ON ( r.cid = e.cid AND ap.lease_interval_id = e.lease_interval_id AND e.event_type_id = ' . CEventType::APPLICANT_GROUPING . ' AND e.event_sub_type_id = ' . CEventSubType::APPLICANT_GROUPING_EMAIL_SENT . ' )';
			$strOrderBySql = ', e.event_datetime DESC';
		} else {
			$strSelectSql  = 'c.name_first, c.name_last, c.name_full as customer_name_full, cpn.phone_number';
			$strJoinSql    = '';
			$strOrderBySql = '';
		}

		if( true == valId( $intPropertyFloorplanId ) && true == valId( $intLeaseStartWindowId ) ) {
			$strSelectSql .= ',fplt_details.is_from_different_fp_lt';

			$strJoinSql .= '
						LEFT JOIN LATERAL 
						(
						  SELECT
						      CASE
						        WHEN a.property_floorplan_id <> ' . ( int ) $intPropertyFloorplanId . ' OR li.lease_start_window_id <> ' . ( int ) $intLeaseStartWindowId . ' THEN TRUE
						        ELSE FALSE
						      END AS is_from_different_fp_lt
						  FROM
						      applications a
						      JOIN lease_intervals li ON ( li.cid = a.cid AND li.lease_id = a.lease_id AND li.id = a.lease_interval_id )
						  WHERE
						      a.cid = r.cid
						      AND a.lease_id = r.lease_id
						      AND a.application_status_id <> ' . CApplicationStatus::CANCELLED . '
						      AND li.lease_interval_type_id <> ' . CLeaseIntervalType::LEASE_MODIFICATION . '
						      AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
						  ORDER BY
						      a.id DESC
						  LIMIT
						      1
						) AS fplt_details ON TRUE';
		}

		$strSql = 'SELECT
				   	 	DISTINCT ON(r.id)
						r.id, r.roommate_group_id, r.parent_lease_id, r.lease_id, r.email_address, r.deleted_by, r.confirmed_by, r.confirmed_on, lc.customer_type_id, c.company_name, ' . $strSelectSql . ', c.birth_date
				   	FROM
						roommates r
						LEFT JOIN lease_customers lc ON ( lc.cid = r.cid AND lc.lease_id = r.lease_id AND lc.customer_type_id != ' . CCustomerType::GUARANTOR . ' )
						LEFT JOIN customers c ON ( c.cid = r.cid AND c.id = lc.customer_id ) ' . $strJoinSql . '
						LEFT JOIN customer_phone_numbers cpn ON ( cpn.cid = r.cid AND cpn.customer_id = lc.customer_id AND cpn.is_primary IS TRUE AND cpn.deleted_on IS NULL AND cpn.deleted_by IS NULL )
				  	WHERE
						r.cid =' . ( int ) $intCid . '
						AND r.roommate_group_id =' . ( int ) $intGroupId . '
				  	ORDER BY r.id' . $strOrderBySql;

		return self::fetchRoommates( $strSql, $objDatabase );
	}

	public static function fetchRoommatesCountByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIsInvitationAccepted = false ) {

		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSqlForConfirmation = '';
		if( true == $boolIsInvitationAccepted ) {
			$strSqlForConfirmation = 'AND r1.confirmed_by IS NOT NULL';
		}

		$strSql = 'SELECT
						COUNT(r.roommate_group_id), r.roommate_group_id
					FROM
						roommates r
						JOIN roommates r1 ON ( r1.cid = r.cid AND r1.roommate_group_id = r.roommate_group_id AND r.deleted_on IS NULL ' . $strSqlForConfirmation . ' )
					WHERE
						r1.cid =' . ( int ) $intCid . '
						AND r1.lease_id =' . ( int ) $intLeaseId . '
						AND r1.deleted_on IS NULL';

		if( true == $boolIsInvitationAccepted ) {
			$strSql .= ' AND r.confirmed_by IS NOT NULL
						AND r.confirmed_on IS NOT NULL';
		}

		$strSql .= ' GROUP BY r.roommate_group_id';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return $arrmixResult;
	}

	public static function fetchAllActiveRoommatesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolIsInvitationAccepted = false ) {

		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSqlForConfirmation = '';
		if( true == $boolIsInvitationAccepted ) {
			$strSqlForConfirmation = 'AND r1.confirmed_by IS NOT NULL';
		}

		$strSql = 'SELECT
						r1.*
					FROM
						roommates r
						JOIN roommates r1 ON ( r1.cid = r.cid AND r1.roommate_group_id = r.roommate_group_id AND r1.deleted_on IS NULL ' . $strSqlForConfirmation . ' )
					WHERE
						r.cid =' . ( int ) $intCid . '
						AND r.lease_id =' . ( int ) $intLeaseId . '
						AND r.deleted_on IS NULL';

		if( true == $boolIsInvitationAccepted ) {
			$strSql .= ' AND r.confirmed_by IS NOT NULL
						AND r.confirmed_on IS NOT NULL';
		}

		$strSql .= ' ORDER BY r1.id';

		return self::fetchRoommates( $strSql, $objDatabase );

	}

	public static function fetchActiveRoommatesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intLeaseId ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						r.*
					FROM
						roommates r
					WHERE
						r.cid =' . ( int ) $intCid . '
						AND r.lease_id =' . ( int ) $intLeaseId . '
						AND r.deleted_on IS NULL';

		return self::fetchRoommates( $strSql, $objDatabase );
	}

	public static function fetchActiveRoommatesByRoommateGroupIdByCid( $intRoommateGroupId, $intCid, $objDatabase, $boolIsIncludeNonConfirmed = false ) {

		if( false == is_numeric( $intRoommateGroupId ) || false == is_numeric( $intCid ) )  return NULL;
		$strWhrSql = ' AND r.confirmed_by IS NOT NULL ';
		if( true == $boolIsIncludeNonConfirmed ) {
			$strWhrSql = '';
		}

		$strSql = 'SELECT
				      	r.*
				  	FROM
				      	roommates r
				  	WHERE
					  	r.cid = ' . ( int ) $intCid . '
				      	AND r.roommate_group_id = ' . ( int ) $intRoommateGroupId . '
				      	' . $strWhrSql . '
				      	AND r.deleted_by IS NULL';

		return self::fetchRoommates( $strSql, $objDatabase );
	}

	public static function fetchAssignedUnitSpaceRoommateCountByRoommateGroupIdByCid( $intRoommateGroupId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intRoommateGroupId ) || false == is_numeric( $intCid ) ) return NULL;

		$strFromSql = ' roommates r
						JOIN cached_leases l ON( l.cid = r.cid AND l.id = r.lease_id ) ';

		$strWhereSql = 'WHERE
							r.cid = ' . ( int ) $intCid . '
							AND r.roommate_group_id = ' . ( int ) $intRoommateGroupId . '
							AND r.confirmed_by IS NOT NULL
							AND r.deleted_by IS NULL
							AND l.property_unit_id IS NOT NULL
							AND l.unit_space_id IS NOT NULL
							AND l.unit_number_cache IS NOT NULL';

		return parent::fetchRowCount( $strWhereSql, $strFromSql, $objDatabase );
	}

	public static function fetchRoommatesByEmailAddressByApplicationIdByCid( $strEmailAddress, $intApplicationId, $intCid, $objDatabase, $intLeaseId = NULL ) {

		if( false == valStr( $strEmailAddress ) || false == is_numeric( $intCid ) || false == valId( $intApplicationId ) ) return NULL;
		$strWhr = ' AND lease_id IS NULL AND ( deleted_by IS NULL OR deleted_by = ' . CCompanyUser::SYSTEM . ' ) ';
		if( true == valId( $intLeaseId ) ) {
			$strWhr = ' AND lease_id = ' . $intLeaseId . ' AND deleted_by = ' . CCompanyUser::SYSTEM;
		}

		$strSql = ' SELECT 
                        r.* 
                    FROM 
                        roommates r 
                        JOIN ( 
                            SELECT 
                                id, 
                                rank() over ( PARTITION BY parent_lease_id,roommate_group_id ORDER BY id ) as ranking 
                            FROM roommates 
                            WHERE
                                cid = ' . ( int ) $intCid . '
                                AND trim( both \' \' from lower( email_address ) ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strEmailAddress ) ) ) . '\'
                                AND application_id = ' . ( int ) $intApplicationId . '
						AND confirmed_on IS NULL ' . $strWhr . ' ) as sub ON ( sub.id = r.id AND sub.ranking = 1 )';

		return self::fetchRoommates( $strSql, $objDatabase );
	}

	public static function fetchConfirmRoommateByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						roommates r
					WHERE
						r.cid = ' . ( int ) $intCid . '
						AND r.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND r.confirmed_by IS NOT NULL
						AND r.deleted_by IS NULL
					ORDER BY r.id desc';

		return self::fetchRoommates( $strSql, $objDatabase );
	}

	public static function fetchAllActiveRoommatesByGroupIdsByCid( $arrintGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintGroupIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						r.*
					FROM
						roommates r
					WHERE
						r.cid =' . ( int ) $intCid . '
						AND r.roommate_group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )
						AND r.deleted_on IS NULL';

		return self::fetchRoommates( $strSql, $objDatabase );

	}

}
?>