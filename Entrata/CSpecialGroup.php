<?php

use Psi\Eos\Entrata\CSpecials;
class CSpecialGroup extends CBaseSpecialGroup {
	protected $m_strUnitTypeNames;
	protected $m_intArcascadeId;

	protected $m_arrintUnitTypeIds;
	protected $m_arrobjSpecials;
	protected $m_intApplicationStageStatusId;

	// Add Functions

	public function addSpecial( $objSpecial ) {
		$this->m_arrobjSpecials[$objSpecial->getId()] = $objSpecial;
	}

	// Get Or Fetch Functions

	public function getOrFetchSpecials( $objDatabase ) {
		$this->m_arrobjSpecials = [];

		if( false == valArr( $this->m_arrobjSpecials ) ) {
			$this->m_arrobjSpecials = CSpecials::createService()->fetchRenewalSpecialsBySpecialGroupIdsByPropertyIdsByCid( [ $this->getId() ], [ $this->getPropertyId() ], $this->getCid(), $objDatabase );
		}

		return $this->m_arrobjSpecials;
	}

	public function fetchActiveSpecials( $objDatabase ) {
		return CSpecials::createService()->fetchNonDeletedSpecialsBySpecialGroupIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	// Get Functions

	public function getUnitTypeIds() {
		return $this->m_arrintUnitTypeIds;
	}

	public function getArCascadeId() {
		return $this->m_intArcascadeId;
	}

	public function getSpecials() {
		return $this->m_arrobjSpecials;
	}

	public function getUnitTypeNames() {
		return $this->m_strUnitTypeNames;
	}

	/**
	 * @return mixed
	 */
	public function getApplicationStageStatusId() {
		return $this->m_intApplicationStageStatusId;
	}

	// Set Functions

	public function setUnitTypeIds( $arrintUnitTypeIds ) {
		$this->m_arrintUnitTypeIds = $arrintUnitTypeIds;
	}

	public function setSpecials( $arrobjSpecials ) {
		$this->m_arrobjSpecials = $arrobjSpecials;
	}

	public function setArCascadeId( $intArCascadeId ) {
		$this->m_intArcascadeId = $intArCascadeId;
	}

	public function setUnitTypeNames( $strUnitTypeNames ) {
		$this->m_strUnitTypeNames = $strUnitTypeNames;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['unit_type_names'] ) && $boolDirectSet ) {
			$this->m_strUnitTypeNames = trim( stripcslashes( $arrmixValues['unit_type_names'] ) );
		} elseif( isset( $arrmixValues['unit_type_names'] ) ) {
			$this->setUnitTypeNames( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_type_names'] ) : $arrmixValues['unit_type_names'] );
		}

		return;
	}

	/**
	 * @param mixed $intApplicationStageStatusId
	 */
	public function setApplicationStageStatusId( $intApplicationStageStatusId ) {
		$this->m_intApplicationStageStatusId = $intApplicationStageStatusId;
	}

	// Val Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialGroupTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialRecipientId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		$strRegex = '/^[a-z A-Z0-9-_\\/\\\\.\'\"]+$/';

		if( '' == $this->getName() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required' ) ) );

		} elseif( false == \Psi\CStringService::singleton()->preg_match( $strRegex, $this->getName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required and should not contain special characters' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnitTypeIds( $objDatabase ) {
		$boolIsValid = true;
		$strUnitTypeNames = '';
		$strErrorMessage  = '';

		if( $this->getSpecialGroupTypeId() != CSpecialGroupType::CONVENTIONAL_LEASING ) {
			$intArCascadeReferenceIds = ( true == valArr( $this->getUnitTypeIds() ) ) ? $this->getUnitTypeIds() : NULL;

			$arrmixResults = \Psi\Eos\Entrata\CSpecialGroups::createService()->fetchActiveSpecialGroupByPropertyIdByCidByArCascadeId( $this->getPropertyId(), $this->getCid(), $this->getArCascadeId(), $this->getMinLeaseEndDate(), $this->getMaxLeaseEndDate(), $objDatabase, $intArCascadeReferenceIds, $this->getId(), $this->getSpecialRecipientId() );

			if( true == valArr( $arrmixResults ) ) {
				$arrmixResults = rekeyArray( 'unit_type_id', $arrmixResults );
				foreach( $arrmixResults As $mixResult ) {
					$strUnitTypeNames .= $mixResult['unit_type_name'] . ', ';
				}

				if( CArCascade::UNIT_TYPE == $this->getArCascadeId() ) {
					$strErrorMessage = $strUnitTypeNames . ' unit type is already associated to with ' . $this->getName() . ' [ ' . $this->getMinLeaseEndDate() . ' - ' . $this->getMaxLeaseEndDate() . ' ] ';
				} else {
					$strErrorMessage = __( 'The lease expiring between {%t, 0, DATE_NUMERIC_STANDARD} - {%t, 1, DATE_NUMERIC_STANDARD} cannot overlap another templates leases expiring between {%t, 0, DATE_NUMERIC_STANDARD} - {%t, 1, DATE_NUMERIC_STANDARD} , please update the dates to prevent overlap', [ $this->getMaxLeaseEndDate(), $this->getMaxLeaseEndDate()] );
				}

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strErrorMessage ) );
				$boolIsValid &= false;
			}
		} else {

			if( CArCascade::UNIT_TYPE == $this->getArCascadeId() && false == valArr( $this->getUnitTypeIds() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Please select at least one offer unit type' ) ) );
			}

			$arrobjAssociatedUnitTypes = ( array ) \Psi\Eos\Entrata\CUnitTypes::createService()->fetchSpecialGroupPublishedUnitTypesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, $this->getId(), $this->getSpecialGroupTypeId() );
			if( true == valArr( $arrobjAssociatedUnitTypes ) && true == valArr( $this->getUnitTypeIds() ) ) {
				foreach( $this->getUnitTypeIds() As $intUnitTypeId ) {
					if( true == array_key_exists( $intUnitTypeId, $arrobjAssociatedUnitTypes ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '{%s,0} unit type is already associated to other published special group.', [ $arrobjAssociatedUnitTypes[$intUnitTypeId]->getName() ] ) ) );
						$boolIsValid &= false;
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valUniquePropertySpecialGroup( $objDatabase ) {
		$boolIsValid = true;

		if( CArCascade::PROPERTY != $this->getArCascadeId() ) return $boolIsValid;

		if( CSpecialRecipient::RENEWALS != $this->getSpecialRecipientId() ) return $boolIsValid;

		$arrintCountPropertySpecialGroup = \Psi\Eos\Entrata\CSpecialGroups::createService()->fetchSpecialGroupPublishedByRecipientIdArCascadeIdByPropertyIdByCid( $this->getSpecialGroupTypeId(), $this->getSpecialRecipientId(), $this->getArCascadeId(), $this->getPropertyId(), $this->getCid(), $objDatabase, $this->getMinLeaseEndDate(), $this->getMaxLeaseEndDate() );

		if( true == valArr( $arrintCountPropertySpecialGroup ) && 0 < $arrintCountPropertySpecialGroup[0]['count'] ) {

			if( $this->getSpecialGroupTypeId() != CSpecialGroupType::CONVENTIONAL_LEASING ) {
				$strMessage = __( 'Leases Expiring dates conflict with another renewal offer template' );
			} elseif( $this->getSpecialGroupTypeId() == CSpecialGroupType::CONVENTIONAL_LEASING ) {
				$strMessage = __( 'Property level tiered discount already exists for this property' );
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unique', $strMessage ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valUnitTypeIds( $objDatabase );
				$boolIsValid &= $this->valUniquePropertySpecialGroup( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valUnitTypeIds( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// Other Functions

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true, $boolReturnSqlOnly = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		} else {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		}
	}

}
?>