<?php

use Psi\Eos\Entrata\CAddOns;

class CAddOnGroup extends CBaseAddOnGroup {

	protected $m_intAddOnId;
	protected $m_intAddOnCount;
	protected $m_intAddOnGroupRateCount;
	protected $m_intAddOnLeaseAssociationCount;
	protected $m_intRentArCodeId;
	protected $m_intDepositArCodeId;
	protected $m_intOtherArCodeId;
	protected $m_intRentArTriggerId;

	protected $m_strAddOnCategoryName;
	protected $m_strAddOnCategoryDescription;

	protected $m_fltMinRentAmount;
	protected $m_fltMaxRentAmount;
	protected $m_fltRentAmount;
	protected $m_fltTaxAmount;
	protected $m_fltDepositTaxAmount;
	protected $m_fltOtherTaxAmount;
	protected $m_fltDepositAmount;
	protected $m_fltOtherAmount;
	protected $m_fltAdvertisedRentAmount;

	protected $m_boolIsReserved;
	protected $m_boolIsActive;
	protected $m_boolIsAvailable;

	protected $m_arrobjRates;

	protected $m_strAddOnGroupDescription;
	protected $m_boolAvailable;

	// Validate Functions

	public function valAddOnCategoryId() {
		$boolIsValid = true;

		if( false == valId( $this->getAddOnCategoryId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'add_on_category_id', __( 'Type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objClientDatabase, $boolIsDelete = false ) {

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || true == is_null( $objClientDatabase ) ) {
			return true;
		}

		$boolIsValid 	= true;
		$objAddOnGroup 	= $this->fetchActiveReservationsAndAddOns( $objClientDatabase );

		if( true == valObj( $objAddOnGroup, 'CAddOnGroup' ) ) {

			if( true == $boolIsDelete && 0 < $objAddOnGroup->getAddOnCount() && true == $objAddOnGroup->getHasInventory() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  __( 'All inventory items for this option must first be removed before it can be deleted.' ) ) );
			} elseif( 0 < $objAddOnGroup->getAddOnLeaseAssociationCount() && 0 == $objAddOnGroup->getAddOnCount() && true == $this->getHasInventory() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  __( 'This Option cannot be inventoried as it has been directly reserved.' ) ) );
			} elseif( true == $objAddOnGroup->getAddOnCount() && 0 < $objAddOnGroup->getAddOnLeaseAssociationCount() && ( $this->getHasInventory() != $objAddOnGroup->getHasInventory() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  __( 'Inventory cannot be turned on for this option as it is currently reserved by resident/lead. All current/future reservations must be moved-out or cancelled in order to turn on Inventory.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valAddOnGroupName( $objClientDatabase ) {
		$strMessage  = NULL;
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {

			$boolIsValid = false;

			if( $this->getAddOnTypeId() == CAddOnType::ASSIGNABLE_ITEMS ) {
				$strMessage = __( 'Assignable item' );
			} elseif( $this->getAddOnTypeId() == CAddOnType::RENTABLE_ITEMS ) {
				$strMessage = __( 'Rentable item' );
			} elseif( $this->getAddOnTypeId() == CAddOnType::SERVICES ) {
				$strMessage = __( 'Service' );
			} elseif( $this->getAddOnTypeId() == CAddOnType::FEATURE_UPGRADES ) {
				$strMessage = __( 'Feature upgrade' );
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '{%s,0} group name is required.', [ $strMessage ] ) ) );
		}

		return $boolIsValid;
	}

	public function valNameDependantInformation( $objClientDatabase ) {

		$strMessage  = NULL;
		$boolIsValid = true;

		$objConflictingAddOnGroup = \Psi\Eos\Entrata\CAddOnGroups::createService()->fetchConflictingAddOnGroupByPropertyIdByCid( $this, $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		if( true == valObj( $objConflictingAddOnGroup, 'CAddOnGroup' ) && $this->getAddOnTypeId() == $objConflictingAddOnGroup->getAddOnTypeId() ) {

			if( $this->getAddOnTypeId() == CAddOnType::ASSIGNABLE_ITEMS ) {
				$strMessage = __( 'Assignable item' );
			} elseif( $this->getAddOnTypeId() == CAddOnType::RENTABLE_ITEMS ) {
				$strMessage = __( 'Rentable item' );
			} elseif( $this->getAddOnTypeId() == CAddOnType::SERVICES ) {
				$strMessage = __( 'Service' );
			} elseif( $this->getAddOnTypeId() == CAddOnType::FEATURE_UPGRADES ) {
				$strMessage = __( 'Feature upgrade' );
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '{%s,0} {%s,1} already exists with that name.', [ $strMessage, $objConflictingAddOnGroup->getName() ] ) ) );

			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolIsDelete = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objClientDatabase, $boolIsDelete );
				break;

			case 'validate_add_on_group':
				$boolIsValid &= $this->valAddOnCategoryId();
				$boolIsValid &= $this->valAddOnGroupName( $objClientDatabase );
				$boolIsValid &= $this->valNameDependantInformation( $objClientDatabase );
				if( false == is_null( $this->getId() ) ) {
					$boolIsValid &= $this->valDependantInformation( $objClientDatabase );
				}
				break;

			case 'validate_assignable_item_option':
				$boolIsValid &= $this->valAddOnGroupName( $objClientDatabase );
				$boolIsValid &= $this->valNameDependantInformation( $objClientDatabase );
				if( false == is_null( $this->getId() ) ) {
					$boolIsValid &= $this->valDependantInformation( $objClientDatabase );
				}
				break;

			case 'validate_feature_upgrade':
				$boolIsValid &= $this->valAddOnGroupName( $objClientDatabase );
				$boolIsValid &= $this->valNameDependantInformation( $objClientDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// Set Functions

	public function setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet = false ) {

		if( true == isset( $arrmixValues['add_on_category_name'] ) ) {
			$this->setAddOnCategoryName( $arrmixValues['add_on_category_name'] );
		}
		if( true == isset( $arrmixValues['add_on_group_description'] ) ) {
			$this->setAddOnGroupDescription( $arrmixValues['add_on_group_description'] );
		}
		if( true == isset( $arrmixValues['add_on_category_description'] ) ) {
			$this->setAddOnCategoryDescription( $arrmixValues['add_on_category_description'] );
		}
		if( true == isset( $arrmixValues['add_on_category_id'] ) ) {
			$this->setAddOnCategoryId( $arrmixValues['add_on_category_id'] );
		}
		if( $boolDirectSet && isset( $arrmixValues['is_available'] ) ) {
			$this->m_boolAvailable = trim( stripcslashes( $arrmixValues['is_available'] ) );
		} elseif( isset( $arrmixValues['is_available'] ) ) {
			$this->setIsAvailable( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_available'] ) : $arrmixValues['is_available'] );
		}
		if( true == isset( $arrmixValues['ao_count'] ) ) {
			$this->setAddOnCount( $arrmixValues['ao_count'] );
		}
		if( true == isset( $arrmixValues['la_count'] ) ) {
			$this->setAddOnLeaseAssociationCount( $arrmixValues['la_count'] );
		}

		if( true == isset( $arrmixValues['rent_amount'] ) ) {
			$this->setRentAmount( $arrmixValues['rent_amount'] );
		}
		if( true == isset( $arrmixValues['deposit_amount'] ) ) {
			$this->setDepositAmount( $arrmixValues['deposit_amount'] );
		}
		if( true == isset( $arrmixValues['other_amount'] ) ) {
			$this->setOtherAmount( $arrmixValues['other_amount'] );
		}

		if( true == isset( $arrmixValues['rent_ar_code_id'] ) ) {
			$this->setRentArCodeId( $arrmixValues['rent_ar_code_id'] );
		}
		if( true == isset( $arrmixValues['deposit_ar_code_id'] ) ) {
			$this->setDepositArCodeId( $arrmixValues['deposit_ar_code_id'] );
		}
		if( true == isset( $arrmixValues['other_ar_code_id'] ) ) {
			$this->setOtherArCodeId( $arrmixValues['other_ar_code_id'] );
		}

		if( true == isset( $arrmixValues['min_rent_amount'] ) ) {
			$this->setMinRentAmount( $arrmixValues['min_rent_amount'] );
		}
		if( true == isset( $arrmixValues['max_rent_amount'] ) ) {
			$this->setMaxRentAmount( $arrmixValues['max_rent_amount'] );
		}
		if( true == isset( $arrmixValues['add_on_id'] ) ) {
			$this->setAddOnId( $arrmixValues['add_on_id'] );
		}
		if( true == isset( $arrmixValues['is_active'] ) ) {
			$this->setIsActive( $arrmixValues['is_active'] );
		}
		if( true == isset( $arrmixValues['tax_amount'] ) ) {
			$this->setTaxAmount( $arrmixValues['tax_amount'] );
		}
		if( true == isset( $arrmixValues['deposit_tax_amount'] ) ) {
			$this->setDepositTaxAmount( $arrmixValues['deposit_tax_amount'] );
		}
		if( true == isset( $arrmixValues['other_tax_amount'] ) ) {
			$this->setOtherTaxAmount( $arrmixValues['other_tax_amount'] );
		}
		if( true == isset( $arrmixValues['advertised_rent_amount'] ) ) {
			$this->setAdvertisedRentAmount( $arrmixValues['advertised_rent_amount'] );
		}
		if( true == isset( $arrmixValues['rent_ar_trigger_id'] ) ) {
			$this->setRentArTriggerId( $arrmixValues['rent_ar_trigger_id'] );
		}
		return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function setAddOnCount( $intAddOnCount ) {
		$this->m_intAddOnCount = CStrings::strToIntDef( $intAddOnCount, NULL, false );
	}

	public function setAddOnGroupRateCount( $intAddOnGroupRateCount ) {
		$this->m_intAddOnGroupRateCount = CStrings::strToIntDef( $intAddOnGroupRateCount, NULL, false );
	}

	public function setAddOnLeaseAssociationCount( $intAddOnLeaseAssociationCount ) {
		$this->m_intAddOnLeaseAssociationCount = CStrings::strToIntDef( $intAddOnLeaseAssociationCount, NULL, false );
	}

	public function setAddOnCategoryName( $strAddOnCategoryName ) {
		$this->m_strAddOnCategoryName = $strAddOnCategoryName;
	}

	public function setAddOnGroupDescription( $strAddOnCategoryDescription ) {
		$this->m_strAddOnGroupDescription = $strAddOnCategoryDescription;
	}

	public function setAddOnCategoryDescription( $strAddOnCategoryDescription ) {
		$this->m_strAddOnCategoryDescription = $strAddOnCategoryDescription;
	}

	public function setAddOnCategoryId( $intAddOnCategoryName ) {
		$this->m_intAddOnCategoryId = $intAddOnCategoryName;
	}

	public function setIsAvailable( $boolIsAvailable ) {
		$this->m_boolIsAvailable = CStrings::strToBool( $boolIsAvailable );
	}

	public function setRentAmount( $fltRentAmount ) {
		$this->m_fltRentAmount = CStrings::strToFloatDef( $fltRentAmount, NULL, false, 65531 );
	}

	public function setDepositAmount( $fltDepositAmount ) {
		$this->m_fltDepositAmount = CStrings::strToFloatDef( $fltDepositAmount, NULL, false, 65531 );
	}

	public function setOtherAmount( $fltOtherAmount ) {
		$this->m_fltOtherAmount = CStrings::strToFloatDef( $fltOtherAmount, NULL, false, 65531 );
	}

	public function setRentArCodeId( $intRentArCodeId ) {
		$this->m_intRentArCodeId = CStrings::strToIntDef( $intRentArCodeId, NULL, false );
	}

	public function setDepositArCodeId( $intDepositArCodeId ) {
		$this->m_intDepositArCodeId = CStrings::strToIntDef( $intDepositArCodeId, NULL, false );
	}

	public function setOtherArCodeId( $intOtherArCodeId ) {
		$this->m_intOtherArCodeId = CStrings::strToIntDef( $intOtherArCodeId, NULL, false );
	}

	public function setMinRentAmount( $fltMinRentAmount ) {
		$this->m_fltMinRentAmount = CStrings::strToFloatDef( $fltMinRentAmount, NULL, false, 65531 );
	}

	public function setMaxRentAmount( $fltMaxRentAmount ) {
		$this->m_fltMaxRentAmount = CStrings::strToFloatDef( $fltMaxRentAmount, NULL, false, 65531 );
	}

	public function setAddOnId( $intAddOnId ) {
		$this->m_intAddOnId = CStrings::strToIntDef( $intAddOnId, NULL, false );
	}

	public function setIsReserved( $boolIsReserved ) {
		$this->m_boolIsReserved = $boolIsReserved;
	}

	public function setIsActive( $boolIsActive ) {
		$this->m_boolIsActive = CStrings::strToBool( $boolIsActive );
	}

	public function setAddOnGroupRates( $arrobjRates ) {
		$this->m_arrobjRates = $arrobjRates;
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->m_fltTaxAmount = CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 2 );
	}

	public function setDepositTaxAmount( $fltDepositTaxAmount ) {
		$this->m_fltDepositTaxAmount = CStrings::strToFloatDef( $fltDepositTaxAmount, NULL, false, 2 );
	}

	public function setOtherTaxAmount( $fltOtherTaxAmount ) {
		$this->m_fltOtherTaxAmount = CStrings::strToFloatDef( $fltOtherTaxAmount, NULL, false, 2 );
	}

	public function setAdvertisedRentAmount( $fltAdvertisedRentAmount ) {
		$this->m_fltAdvertisedRentAmount = $fltAdvertisedRentAmount;
	}

	public function setRentArTriggerId( $intRentArTriggerId ) {
		$this->m_intRentArTriggerId = $intRentArTriggerId;
	}

	// Get Functions

	public function getAddOnCount() {
		return $this->m_intAddOnCount;
	}

	public function getAddOnGroupRateCount() {
		return $this->m_intAddOnGroupRateCount;
	}

	public function getAddOnLeaseAssociationCount() {
		return $this->m_intAddOnLeaseAssociationCount;
	}

	public function getAddOnCategoryName() {
		return $this->m_strAddOnCategoryName;
	}

	public function getAddOnCategoryDescription() {
		return $this->m_strAddOnCategoryDescription;
	}

	public function getAddOnCategoryId() {
		return $this->m_intAddOnCategoryId;
	}

	public function getIsAvailable() {
		return $this->m_boolIsAvailable;
	}

	public function getRentAmount() {
		return $this->m_fltRentAmount;
	}

	public function getDepositAmount() {
		return $this->m_fltDepositAmount;
	}

	public function getOtherAmount() {
		return $this->m_fltOtherAmount;
	}

	public function getRentArCodeId() {
		return $this->m_intRentArCodeId;
	}

	public function getDepositArCodeId() {
		return $this->m_intDepositArCodeId;
	}

	public function getOtherArCodeId() {
		return $this->m_intOtherArCodeId;
	}

	public function getMinRentAmount() {
		return $this->m_fltMinRentAmount;
	}

	public function getMaxRentAmount() {
		return $this->m_fltMaxRentAmount;
	}

	public function getAddOnId() {
		return $this->m_intAddOnId;
	}

	public function getIsReserved() {
		return $this->m_boolIsReserved;
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function getAddOnGroupRates() {
		return $this->m_arrobjRates;
	}

	public function getTaxAmount() {
		return ( float ) $this->m_fltTaxAmount;
	}

	public function getDepositTaxAmount() {
		return ( float ) $this->m_fltDepositTaxAmount;
	}

	public function getOtherTaxAmount() {
		return ( float ) $this->m_fltOtherTaxAmount;
	}

	public function getAdvertisedRentAmount() {
		return $this->m_fltAdvertisedRentAmount;
	}

	public function getRentArTriggerId() {
		return $this->m_intRentArTriggerId;
	}

	// Create Functions

	public function createRate() {
		$objRate = new CRate();
		$objRate->setCid( $this->m_intCid );
		$objRate->setRateTypeId( CRateType::ADD_ON_GROUP );
		$objRate->setReferenceId( $this->getId() );

		return $objRate;
	}

	public function createAddOn() {
		$objAddOn = new CAddOn();
		$objAddOn->setCid( $this->m_intCid );
		$objAddOn->setPropertyId( $this->getPropertyId() );
		$objAddOn->setAddOnGroupId( $this->getId() );
		$objAddOn->setAddOnGroupName( $this->getName() );

		return $objAddOn;
	}

	public function createCompanyApplicationAddOnGroups() {
		$objAddOnGroupApplication = new CCompanyApplicationAddOnGroup();
		$objAddOnGroupApplication->setAddOnGroupId( $this->getId() );
		$objAddOnGroupApplication->setPropertyId( $this->getPropertyId() );
		$objAddOnGroupApplication->setCid( $this->getCid() );

		return $objAddOnGroupApplication;
	}

	// Fetch Functions

	public function fetchActiveReservationsAndAddOns( $objDatabase ) {
		return \Psi\Eos\Entrata\CAddOnGroups::createService()->fetchActiveReservaionsAndAddOnsCountByAddOnGroupIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAddOns( $objDatabase, $intPageNo = NULL, $intPageSize = NULL ) {
		return CAddOns::createService()->fetchCustomAddOnsByAddOnGroupIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase, $intPageNo, $intPageSize );
	}

	public function fetchCompanyApplications( $objDatabase ) {
		return CCompanyApplicationAddOnGroups::fetchCompanyApplicationAddOnGroupsByAddOnGroupIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAddOn( $objDatabase ) {
		return CAddOns::createService()->fetchCustomAddOnByAddOnGroupIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchRateAssociation( $objDatabase ) {
		return \Psi\Eos\Entrata\CRateAssociations::createService()->fetchRateAssociationByArOriginReferenceIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAddOnsRates( $intPropertyId, $objDatabase, $intPageNo = NULL, $intPageSize = NULL ) {

		$objAddOnsFilter = new CAddOnsFilter();
		$objAddOnsFilter->setAddOnTypeId( $this->getAddOnTypeId() );
		return CAddOns::createService()->fetchAddOnsRatesByAddOnGroupIdByPropertyIdByCId( $this->getId(), $intPropertyId, $this->getCid(), $objDatabase, $intPageNo, $intPageSize, $objAddOnsFilter );
	}

	// Other Functions

	public function getRawAddOnData( $arrobjAddOns ) {
		$arrstrRawAddOnData	= [];
		$arrstrSortFlag		= [];

		foreach( $arrobjAddOns as $intKey => $objAddOn ) {
			$arrstrAddOns = [];

			$arrstrAddOns['id'] 			= $objAddOn->getId();
			$arrstrAddOns['propertyId'] 	= $objAddOn->getPropertyId();
			$arrstrAddOns['addOnTypeId']	= $objAddOn->getAddOnOptionId();
			$arrstrAddOns['lookupCode'] 	= $objAddOn->getLookupCode();
			$arrstrAddOns['name'] 			= $objAddOn->getName();
			$arrstrSortFlag[$intKey]		= $objAddOn->getName();
			$arrstrAddOns['description'] 	= $objAddOn->getDescription();
			$arrstrAddOns['xPosition'] 		= $objAddOn->getXPos();
			$arrstrAddOns['yPosition'] 		= $objAddOn->getYPos();

			$arrstrRawAddOnData[] = $arrstrAddOns;
		}

		array_multisort( $arrstrSortFlag, SORT_DESC, SORT_STRING, $arrstrRawAddOnData );

		return $arrstrRawAddOnData;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

	public function deleteAddonGroup( $intPropertyId, $intCurrentUserId, $objDatabase ) {

		/**
		 *	Comment: DELETE ADDON GROUP
		 *	1. Delete associated addons
		 *	2. Delete associated company applications
		 *	3. Delete addon group
		 *	4. Add zend queue rates caching
		 */

		$arrobjAddOns							= ( array ) CAddOns::createService()->fetchCustomAllAddOnByAddOnGroupIdByPropertyIdByCid( $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );
		$arrobjCompanyApplicationAddOnGroups	= ( array ) $this->fetchCompanyApplications( $objDatabase );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				foreach( $arrobjAddOns as $objAddOn ) {

					if( false == $objAddOn->deleteAddon( $this->getAddOnTypeId(), $intPropertyId, $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}

				foreach( $arrobjCompanyApplicationAddOnGroups as $objCompanyApplicationAddOnGroup ) {

					if( false == $objCompanyApplicationAddOnGroup->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}

				if( false == $this->delete( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  __( 'Failed to delete the associated data.' ) ) );
		}

		return $boolIsValid;
	}

}
?>