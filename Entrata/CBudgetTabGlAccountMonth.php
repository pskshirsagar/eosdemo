<?php

class CBudgetTabGlAccountMonth extends CBaseBudgetTabGlAccountMonth {

	protected $m_strFormula;
	protected $m_fltOldAmount;
	protected $m_intBudgetTabId;
	protected $m_intBudgetDataSourceTypeId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetTabGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOverridden() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDirty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// Get methods

	public function getBudgetDataSourceTypeId() {
		return $this->m_intBudgetDataSourceTypeId;
	}

	public function getFormula() {
		return $this->m_strFormula;
	}

	public function getBudgetTabId() {
		return $this->m_intBudgetTabId;
	}

	public function getOldAmount() {
		return $this->m_fltOldAmount;
	}

	// Set methods

	public function setBudgetDataSourceTypeId( $intBudgetDataSourceTypeId ) {
		$this->m_intBudgetDataSourceTypeId = $intBudgetDataSourceTypeId;
	}

	public function setOldAmount( $fltOldAmount ) {
		$this->m_fltOldAmount = $fltOldAmount;
	}

	public function sqlOldAmount() {
		return ( true == isset( $this->m_fltOldAmount ) ) ? ( string ) $this->m_fltOldAmount : '0';
	}

	public function setFormula( $strFormula ) {
		$this->m_strFormula = $strFormula;
	}

	public function setBudgetTabId( $intBudgetTabId ) {
		$this->m_intBudgetTabId = $intBudgetTabId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['budget_data_source_type_id'] ) ) {
			$this->setBudgetDataSourceTypeId( $arrmixValues['budget_data_source_type_id'] );
		}

		if( isset( $arrmixValues['formula'] ) ) {
			$this->setFormula( $arrmixValues['formula'] );
		}

		if( isset( $arrmixValues['budget_tab_id'] ) ) {
			$this->setBudgetTabId( $arrmixValues['budget_tab_id'] );
		}

		if( isset( $arrmixValues['old_amount'] ) ) {
			$this->setOldAmount( $arrmixValues['old_amount'] );
		}

	}

}
?>