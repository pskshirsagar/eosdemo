<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateEvents
 * Do not add any new functions to this class.
 */

class CCorporateEvents extends CBaseCorporateEvents {

	public static function fetchLatestCorporateEventByEventTypeIdByCustomerIdByCid( $intEventTypeId, $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.*
					FROM
						corporate_events ce
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.event_type_id = ' . ( int ) $intEventTypeId . '
						AND ce.customer_id = ' . ( int ) $intCustomerId . '
					ORDER BY
						ce.event_datetime DESC
					LIMIT 1';

		return self::fetchCorporateEvent( $strSql, $objDatabase );
	}

	public static function fetchAllCorporateEventsByCustomerIdByCorporateIdByCid( $intCorporateId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.*
					FROM
						corporate_events ce
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.corporate_id = ' . ( int ) $intCorporateId . '
					ORDER BY
						ce.event_datetime DESC';

		return self::fetchCorporateEvents( $strSql, $objDatabase );
	}

	public static function fetchCorporateEventsByCorporateIdByEventTypeIdByCid( $intCorporateId, $intEventTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ce.*
					FROM
						corporate_events ce
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.corporate_id = ' . ( int ) $intCorporateId . '
						AND ce.event_type_id = ' . ( int ) $intEventTypeId . '
					ORDER BY
						ce.event_datetime DESC, ce.id DESC';

		return self::fetchCorporateEvents( $strSql, $objDatabase );

	}

}
?>