<?php

class CScheduledEmailEventType extends CBaseScheduledEmailEventType {

	const BIRTHDAY								= 1;
	const LEASE_EXPIRATION						= 2;
	const MOVE_IN_DATE							= 3;
	const LEAD_CREATED_DATE						= 4;
	const RESIDENT_CREATED_DATE 				= 5;
	const EMPLOYEE_CREATED_DATE 				= 6;
	const APPLICATION_LAST_UPDATE 				= 7;
	const LEASE_LAST_UPDATE 					= 8;
	const LEASE_RENEWAL_DATE 					= 9;
	const TOUR_COMPLETED 						= 10;
	const TOUR_MISSED							= 11;
	const LEASE_STARTED 						= 12;
	const APPLICATION_STARTED 					= 13;
	const APPOINTMENT_SCHEDULED					= 14;
	const RENEWAL_OFFER_EXPIRATION				= 15;
	const RENEWAL_OFFER_GENERATED				= 16;
	const MOVE_OUT								= 17;
	const APPLICATION_PARTIALLY_COMPLETED		= 18;
	const APPLICATION_COMPLETED					= 19;
	const LEASE_PARTIALLY_COMPLETED				= 20;
	const LEASE_COMPLETED						= 21;
	const DOCUMENT_EXPIRED_PASSPORT				= 22;
	const DOCUMENT_EXPIRED_VISA				    = 23;

	public static $c_arrintResidentEventTypes = [
		self::BIRTHDAY					=> self::BIRTHDAY,
		self::LEASE_EXPIRATION			=> self::LEASE_EXPIRATION,
		self::MOVE_IN_DATE				=> self::MOVE_IN_DATE,
		self::RESIDENT_CREATED_DATE		=> self::RESIDENT_CREATED_DATE,
		self::LEASE_RENEWAL_DATE		=> self::LEASE_RENEWAL_DATE,
		self::RENEWAL_OFFER_EXPIRATION	=> self::RENEWAL_OFFER_EXPIRATION,
		self::RENEWAL_OFFER_GENERATED	=> self::RENEWAL_OFFER_GENERATED,
		self::MOVE_OUT					=> self::MOVE_OUT,
		self::DOCUMENT_EXPIRED_PASSPORT	=> self::DOCUMENT_EXPIRED_PASSPORT,
		self::DOCUMENT_EXPIRED_VISA		=> self::DOCUMENT_EXPIRED_VISA
	];

	public static $c_arrintProspectEventTypes = [
		self::BIRTHDAY 								=> self::BIRTHDAY,
		self::MOVE_IN_DATE 							=> self::MOVE_IN_DATE,
		self::LEAD_CREATED_DATE 					=> self::LEAD_CREATED_DATE,
		self::APPLICATION_LAST_UPDATE 				=> self::APPLICATION_LAST_UPDATE,
		self::LEASE_LAST_UPDATE						=> self::LEASE_LAST_UPDATE,
		self::TOUR_COMPLETED						=> self::TOUR_COMPLETED,
		self::TOUR_MISSED							=> self::TOUR_MISSED,
		self::APPOINTMENT_SCHEDULED					=> self::APPOINTMENT_SCHEDULED,
		self::LEASE_STARTED 						=> self::LEASE_STARTED,
		self::APPLICATION_STARTED 					=> self::APPLICATION_STARTED,
		self::APPLICATION_PARTIALLY_COMPLETED 		=> self::APPLICATION_PARTIALLY_COMPLETED,
		self::APPLICATION_COMPLETED 				=> self::APPLICATION_COMPLETED,
		self::LEASE_PARTIALLY_COMPLETED 			=> self::LEASE_PARTIALLY_COMPLETED,
		self::LEASE_COMPLETED 						=> self::LEASE_COMPLETED
	];

	public static $c_arrintEmployeeEventTypes = [
		self::BIRTHDAY 					=> self::BIRTHDAY,
		self::EMPLOYEE_CREATED_DATE 	=> self::EMPLOYEE_CREATED_DATE
	];

	public static $c_arrintEventTypesWithAfterDayFlag = [
		self::RESIDENT_CREATED_DATE,
		self::EMPLOYEE_CREATED_DATE,
		self::LEAD_CREATED_DATE,
		self::APPLICATION_LAST_UPDATE,
		self::LEASE_LAST_UPDATE,
		self::LEASE_RENEWAL_DATE
	];

}
?>