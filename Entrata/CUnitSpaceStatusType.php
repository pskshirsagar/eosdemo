<?php

class CUnitSpaceStatusType extends CBaseUnitSpaceStatusType {

	// We should renumber these unit statuses in the order shown below, first gets 1, last gets 7

	const OCCUPIED_NO_NOTICE 		= 3; // Default Unavailable - NEVER SKIPPED
	const NOTICE_RENTED				= 5; // Unavailable - SOMETIMES SKIPPED - SKIPPED IF UNIT IS NOT RENTED DURING NOTICE PERIOD
	const NOTICE_UNRENTED			= 9; // Available - NEVER SKIPPED
	const VACANT_UNRENTED_READY		= 2; // Default Available - SOMETIMES SKIPPED IF UNIT IS RENTED DURING NOTICE PERIOD
	const VACANT_UNRENTED_NOT_READY	= 4; // Available - SOMETIMES SKIPPED IF UNIT IS RENTED DURING NOTICE PERIOD
	const VACANT_RENTED_NOT_READY	= 6; // Unavailable - SOMETIMES SKIPPED IF MAKE READY COMPLETED BEFORE RENTED
	const VACANT_RENTED_READY		= 8; // Unavailable - NEVER SKIPPED!!!

	// Log Effective Date Determination

	// const OCCUPIED_NO_NOTICE 				= 3; - ALWAYS MOVE IN DATE
	// const NOTICE_UNRENTED 					= 9; - ALWAYS NOTICE DATE
	// const NOTICE_RENTED 					    = 5; - ALWAYS NEW APPLICATION COMPLETED OR LEASE APPROVED
	// const VACANT_UNRENTED_NOT_READY 		    = 4; - ALWAYS MOVE OUT DATE
	// const VACANT_UNRENTED_READY 			    = 2; - WORK ORDER COMPLETION DATE IF MAKE READY EXISTS, OTHERWISE COULD BE MOVE OUT DATE, IF NO MAKE READY
	// const VACANT_RENTED_NOT_READY 			= 6; - MOVE OUT DATE ( IF RENTED BEFORE MOVE OUT ) OTHERWISE APPLICATION COMPLETED OR LEASE APPROVED (UNLESS MAKE READY WORK ORDER IS CREATED LATE)
	// const VACANT_RENTED_READY 				= 8; - MOVE OUT DATE ( IF RENTED BEFORE MOVE OUT & NO MAKE READY ) OTHERWISE APPLICATION COMPLETED OR LEASE APPROVED ( IF NO MAKE READY EXISTS ) OTHERWISE MAKE READY COMPLETION DATE

	// ANYTHING THAT CAUSES THE STATUS TO GO BACKWARDS IS A REVERSAL, FIND THE ORIGIN_REVERSAL_LOG_ID, AND USE EFFECTIVE DATE FOR THAT.
	// ALWAYS ATTACH NEW REVERSAL LOG TO ORIGIN REVERSAL LOG AND FLAG BOTH AS is_reversed = 1
	// ADD VALIDATION TO LOGS / MAYBE A DIAGNOSTIC TO MAKE SURE ESSENTIAL STEPS AREN'T MISSED IN STATUS PROGRESS
	// 1. OCCUPIED STATUS MUST GO TO NOTICE UNRENTED STATUS BEFORE NOTICE RENTED STATUS
	// 2. OCCUPIED STATUS MUST GO TO ANY NOTICE STATUS, PRIOR TO GOING TO ANY VACANT STATUS
	// 3. WE CAN'T GO FROM VACANT_UNRENTED_NOT_READY TO VACANT_RENTED_READY WITHOUT A ( VACANT_UNRENTED_READY OR VACANT_RENTED_NOT_READY ) DEPENDING ON WHETHER WORK ORDER GOT SIGNED BEFORE APPLICATION COMPLETED OR LEASE APPROVED
	// If we are missing status progression, create them immediately in the trigger.

	// ALWAYS CREATE MISSING STATUSES WHETHER WE'RE GOING FORWARD OR BACKWARD, BUT REVERSE FIRST, AND DISREGARD REVERSED LOGS WHILE FILLING IN MISSING STATUSES
	// VALIDATE FOR EXISTANCE OF EFFECTIVE DATES TO FORCE PROGRAMMERS TO UPDATE DATA IN THE CORRECT ORDER.

	const OCCUPANCY_STATUS_VACANT					= 1;
	const OCCUPANCY_STATUS_PARTIALLY_OCCUPIED		= 2;

	public static $c_arrintAvailableUnitSpaceStatusTypes				= [
		CUnitSpaceStatusType::VACANT_UNRENTED_READY,
		CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY,
		CUnitSpaceStatusType::NOTICE_UNRENTED,
	];

	public static $c_arrintUnavailableUnitSpaceStatusTypes				= [
		CUnitSpaceStatusType::OCCUPIED_NO_NOTICE,
		CUnitSpaceStatusType::NOTICE_RENTED,
		CUnitSpaceStatusType::VACANT_RENTED_READY,
		CUnitSpaceStatusType::VACANT_RENTED_NOT_READY
	];

	public static $c_arrintOccupiedUnitSpaceStatusTypes				= [
		CUnitSpaceStatusType::OCCUPIED_NO_NOTICE,
		CUnitSpaceStatusType::NOTICE_UNRENTED,
		CUnitSpaceStatusType::NOTICE_RENTED
	];

	public static $c_arrintVacantUnitSpaceStatusTypes					= [
		CUnitSpaceStatusType::VACANT_UNRENTED_READY,
		CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY,
		CUnitSpaceStatusType::VACANT_RENTED_READY,
		CUnitSpaceStatusType::VACANT_RENTED_NOT_READY
	];

	public static $c_arrintVacantReadyUnitSpaceStatusTypes				= [
		CUnitSpaceStatusType::VACANT_UNRENTED_READY,
		CUnitSpaceStatusType::VACANT_RENTED_READY
	];

	public static $c_arrintVacantNotReadyUnitSpaceStatusTypes			= [
		CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY,
		CUnitSpaceStatusType::VACANT_RENTED_NOT_READY
	];

	public static $c_arrintActiveUnitSpaceStatusTypes					= [
		CUnitSpaceStatusType::VACANT_UNRENTED_READY,
		CUnitSpaceStatusType::OCCUPIED_NO_NOTICE,
		CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY,
		CUnitSpaceStatusType::NOTICE_RENTED,
		CUnitSpaceStatusType::VACANT_RENTED_NOT_READY,
		CUnitSpaceStatusType::VACANT_RENTED_READY,
		CUnitSpaceStatusType::NOTICE_UNRENTED
	];

	public static $c_arrintAddOnAvailableUnitSpaceStatusTypes			= [
		CUnitSpaceStatusType::VACANT_UNRENTED_READY,
		CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY,
		CUnitSpaceStatusType::VACANT_RENTED_NOT_READY
	];

	public static $c_arrintAddOnUnavailableUnitSpaceStatusTypes		= [
		CUnitSpaceStatusType::OCCUPIED_NO_NOTICE,
		CUnitSpaceStatusType::NOTICE_RENTED,
		CUnitSpaceStatusType::VACANT_RENTED_READY,
		CUnitSpaceStatusType::NOTICE_UNRENTED
	];

	public static $c_arrintStudentUnavailableUnitSpaceStatusTypes		= [
		CUnitSpaceStatusType::OCCUPIED_NO_NOTICE,
		CUnitSpaceStatusType::NOTICE_RENTED
	];

	public static $c_arrintMakeReadyUnitSpaceStatusTypes					= [
		CUnitSpaceStatusType::VACANT_UNRENTED_READY,
		CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY,
		CUnitSpaceStatusType::NOTICE_RENTED,
		CUnitSpaceStatusType::VACANT_RENTED_NOT_READY,
		CUnitSpaceStatusType::VACANT_RENTED_READY,
		CUnitSpaceStatusType::NOTICE_UNRENTED
	];

	public static $c_arrintAvailableAndNoticeUnrentedUnitSpaceStatusTypes = [
		CUnitSpaceStatusType::VACANT_UNRENTED_READY,
		CUnitSpaceStatusType::NOTICE_UNRENTED,
	];

	public static $c_arrintAvailableAndUnrentedNotReadyUnitSpaceStatusTypes = [
		CUnitSpaceStatusType::VACANT_UNRENTED_READY,
		CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY,
	];

	public static $c_arrintMakeReadyWorkOrderUnitSpaceStatusTypes = [
		CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY,
		CUnitSpaceStatusType::VACANT_RENTED_NOT_READY,
		CUnitSpaceStatusType::NOTICE_RENTED,
		CUnitSpaceStatusType::NOTICE_UNRENTED
	];

	public static $c_arrintOccupiedAndUnavailableUnitSpaceStatusTypes = [
		CUnitSpaceStatusType::OCCUPIED_NO_NOTICE,
		CUnitSpaceStatusType::NOTICE_RENTED,
		CUnitSpaceStatusType::NOTICE_UNRENTED,
		CUnitSpaceStatusType::VACANT_RENTED_READY,
		CUnitSpaceStatusType::VACANT_RENTED_NOT_READY
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getUnitSpaceStatusTypeByUnitSpaceStatusTypeId( $intUnitSpaceStatusTypeId ) {
		switch( $intUnitSpaceStatusTypeId ) {
			case self::OCCUPIED_NO_NOTICE:
				return __( 'Occupied (Unavailable)' );
				break;
			case self::VACANT_UNRENTED_READY:
				return __( 'Vacant Ready (Available)' );
				break;
			case self::VACANT_UNRENTED_NOT_READY:
				return __( 'Vacant Not Ready (Available)' );
				break;
			case self::VACANT_RENTED_READY:
				return __( 'Vacant Ready (Unavailable)' );
				break;
			case self::VACANT_RENTED_NOT_READY:
				return __( 'Vacant Not Ready (Unavailable)' );
				break;
			case self::NOTICE_RENTED:
				return __( 'On Notice (Unavailable)' );
				break;
			case self::NOTICE_UNRENTED:
				return __( 'On Notice (Available)' );
				break;

			default:
				// default case
				break;
		}
	}

	public function getAllUnitSpaceStatusTypes() {

		return [
			self::OCCUPIED_NO_NOTICE            => __( 'Occupied No Notice' ),
			self::VACANT_UNRENTED_READY         => __( 'Vacant Unrented Ready' ),
			self::VACANT_UNRENTED_NOT_READY     => __( 'Vacant Unrented Not Ready' ),
			self::VACANT_RENTED_READY           => __( 'Vacant Rented Ready' ),
			self::VACANT_RENTED_NOT_READY       => __( 'Vacant Rented Not Ready' ),
			self::NOTICE_RENTED                 => __( 'Notice Rented' ),
			self::NOTICE_UNRENTED               => __( 'Notice Unrented' )
		];

	}

	public function getAllUnitOccupancyStatuses() {

		return [
			self::OCCUPANCY_STATUS_VACANT                    => __( 'Vacant' ),
			self::OCCUPANCY_STATUS_PARTIALLY_OCCUPIED        => __( 'Partially Occupied' )
		];

	}

}
?>