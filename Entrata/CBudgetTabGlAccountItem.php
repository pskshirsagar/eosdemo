<?php

class CBudgetTabGlAccountItem extends CBaseBudgetTabGlAccountItem {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetTabGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetDataSourceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valItemName( $arrmixBudgetItemNames, $arrmixBudgetTabGlAccountDetail ) {

		$boolIsValid = true;
		$arrintCountItemNames = array_count_values( $arrmixBudgetItemNames[$this->getBudgetTabGlAccountId()] );
		$strGlAccountName = $arrmixBudgetTabGlAccountDetail['gl_account_number'] . ' : ' . $arrmixBudgetTabGlAccountDetail['gl_account_name'];

		if( false == valStr( $this->getItemName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'item_name', __( 'Item name is required for ' . $strGlAccountName . ' GL account.' ) ) );
		} elseif( 1 < $arrintCountItemNames[$this->getItemName()] ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'item_name', __( 'Item name \'' . addslashes( $this->getItemName() ) . '\' is already exists for ' . $strGlAccountName . ' GL account. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnitPrice() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFormula() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $arrstrBudgetItemNames = [], $arrmixBudgetTabGlAccountDetail = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valItemName( $arrstrBudgetItemNames, $arrmixBudgetTabGlAccountDetail );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>