<?php

class CDrawRequestDetail extends CBaseDrawRequestDetail {
	protected $m_intApHeaderId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobPhaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDrawRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDrawAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->m_intApHeaderId = CStrings::strToIntDef( $intApHeaderId, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['ap_header_id'] ) ) $this->setApHeaderId( $arrmixValues['ap_header_id'] );
	}

}
?>