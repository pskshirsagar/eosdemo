<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COffers
 * Do not add any new functions to this class.
 */

class COffers extends CBaseOffers {

	public static function fetchRenewalOffersByOfferTemplateIdsByPropertyIdsByCid( $arrintRenewalOfferTemplateIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false === valArr( $arrintRenewalOfferTemplateIds ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT o.*,
						COALESCE((
				                    SELECT o1.days_offset
				                    FROM offers o1
				                    WHERE o1.cid = o.cid AND
				                          o1.offer_template_id = o.offer_template_id AND
				                          o1.deleted_by IS NULL AND
				                          o1.days_offset < o.days_offset
				                    ORDER BY o1.days_offset DESC
				                    LIMIT 1
				        ) + 1, 0) AS min_days_to_lease_start,   	
				       ra.hide_description,
				       ra.property_id,
				       oi.ar_origin_reference_id AS tier_special_id
				FROM offers o
				     JOIN offer_templates ot ON ( o.cid = ot.cid AND o.offer_template_id = ot.id )
				     JOIN offer_items oi ON ( o.cid = oi.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL )
				     JOIN specials s ON ( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id AND s.deleted_by IS NULL )
				     JOIN rate_associations ra ON ( ra.cid = o.cid AND ra.ar_origin_reference_id = oi.ar_origin_reference_id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
				WHERE 
					  ra.cid = ' . ( int ) $intCid . ' 
					  AND ra.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' ) 
				      AND o.is_active = true 
				      AND o.offer_template_id IN ( ' . sqlIntImplode( $arrintRenewalOfferTemplateIds ) . ' ) 
				      AND o.deleted_by IS NULL 
				      AND s.special_type_id = ' . CSpecialType::RENEWAL_TIER . ' 
				      AND (o.end_date IS NULL OR CURRENT_DATE < o.end_date)
				ORDER BY o.days_offset DESC,
				         o.start_date DESC ';

		return self::fetchOffers( $strSql, $objDatabase );
	}

	public static function fetchNonDeletedOffersByOfferTemplateIdByCid( $intOfferTemplateId, $intCid, $objDatabase ) {

		if( false === valId( $intOfferTemplateId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
					o.*,
					s.id AS special_id,
					s.application_stage_status_id,
					COALESCE((
				                    SELECT o1.days_offset
				                    FROM offers o1
				                    WHERE o1.cid = o.cid AND
				                          o1.offer_template_id = o.offer_template_id AND
				                          o1.deleted_by IS NULL AND
				                          o1.days_offset < o.days_offset
				                    ORDER BY o1.days_offset DESC
				                    LIMIT 1
				    ) + 1, 0) AS min_days_to_lease_start
					FROM offers o
				        JOIN offer_templates ot ON ( o.cid = ot.cid AND o.offer_template_id = ot.id )
				        LEFT JOIN offer_items oi ON ( o.cid = oi.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL )
				        LEFT JOIN specials s ON ( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id AND s.deleted_by IS NULL )
					WHERE 
						o.offer_template_id = ' . ( int ) $intOfferTemplateId . '
					    AND o.cid = ' . ( int ) $intCid . '
				        AND o.deleted_by IS NULL
				        ORDER BY o.days_offset DESC, o.start_date ';

		return self::fetchOffers( $strSql, $objDatabase );

	}

	public static function fetchNonDeletedOffersByIdsByCid( $arrintOfferIds, $intCid, $objDatabase ) {

		if( false === valArr( $arrintOfferIds ) ) return NULL;

		$strSql = 'SELECT 
					o.*
					FROM 
						offers o
					WHERE 
						o.id IN ( ' . sqlIntImplode( $arrintOfferIds ) . ' )
					    AND o.cid = ' . ( int ) $intCid . '
				        AND o.deleted_by IS NULL';

		return self::fetchOffers( $strSql, $objDatabase );
	}

	public static function fetchCustomOfferLogsByOfferTemplateIdBYCid( $intOfferTemplateId, $intCid, $objDatabase ) {

		if( false === valId( $intOfferTemplateId ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						(
							SELECT
								DATE ( ol.created_on ) as created_date,
								ol.created_by as user_id,
								oi.name,
								COALESCE( initcap( ce.name_first || \' \' || ce.name_last ), cu.username ) as user_name,
								ol.*,						     
								rank ( ) OVER ( PARTITION BY DATE ( ol.created_on ),
								ol.created_by
							ORDER BY
								ol.created_on DESC, ol.id DESC ) AS rank
							FROM
								offer_logs ol
								JOIN offer_items oi ON( oi.cid = ol.cid AND oi.offer_id = ol.offer_id )
								JOIN specials s ON( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id AND s.special_type_id = ' . CSpecialType::RENEWAL_TIER . '  )
								LEFT JOIN company_users cu ON ( cu.cid = ol.cid AND cu.id = ol.created_by )
								LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
							WHERE
								ol.cid = ' . ( int ) $intCid . '
								AND ol.offer_template_id = ' . $intOfferTemplateId . '
						) AS sub
					ORDER BY
					sub.created_on DESC,sub.created_by';

		return $arrmixOfferLogs = fetchData( $strSql, $objDatabase );
	}

	public static function fetchRemainingCapCountByOfferIdBYCid( $intId, $intCid, $objDatabase ) {

		if( false === valId( $intId ) ) return NULL;

		$strSql = 'SELECT
						count( DISTINCT a.id ) AS used_cap_count
					FROM
						offers o
                        JOIN offer_items oi ON ( o.cid = oi.cid AND oi.offer_id = o.id AND oi.deleted_by IS NULL )
                        JOIN specials s ON ( o.cid = s.cid AND s.id = oi.ar_origin_reference_id AND s.deleted_by IS NULL )	
                        LEFT JOIN quotes q ON ( q.cid = o.cid AND q.leasing_tier_special_id = oi.ar_origin_reference_id ANd q.cancelled_by IS NULL AND q.accepted_on IS NOT NULL )
						LEFT JOIN applications a ON ( q.cid = a.cid AND q.application_id = a.id AND ( a.application_stage_id, a.application_status_id ) IN (' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintOfferTemplateStatusIds ) . ' ) )
					WHERE o.cid = ' . ( int ) $intCid . ' AND
						  o.id = ' . ( int ) $intId . ' AND
						  s.special_type_id = ' . CSpecialType::RENEWAL_TIER . ' AND
					      o.deleted_by IS NULL AND
					      o.deleted_by IS NULL
					GROUP BY o.id';

		$arrintUsedCapCounts = fetchData( $strSql, $objDatabase );
		return $arrintUsedCapCounts[0]['used_cap_count'];
	}

}
?>