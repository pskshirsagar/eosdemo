<?php

class CLedgerFilter extends CBaseLedgerFilter {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName( $objDatabase ) {
     	$boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
        	 $boolIsValid = false;
        	 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please enter ledger name.' ) ) );
        } else {
        	 $strLedgerFilter = \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFilterByNameByCid( $this->getId(), $this->getName(), $this->getCid(), $objDatabase );

        	 if( true == valStr( $strLedgerFilter ) ) {
        		 $boolIsValid = false;
        		 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Ledger name is already in use. Enter a unique ledger name.' ) ) );
        	 }
        }

      	return $boolIsValid;
    }

    public function valAssociation( $objDatabase, $intArCodeCount, $strAction ) {
    	$boolIsValid = true;

    	if( 0 == $this->getIsPublished() && ( 0 < $intArCodeCount ) && VALIDATE_UPDATE == $strAction ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'To disable a ledger all charge codes must be assigned to another ledger first.' ) ) );
    	} elseif( ( 0 < $intArCodeCount ) && VALIDATE_DELETE == $strAction ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'You cannot delete this ledger as charge codes are associated to it.' ) ) );
    	}

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objClientDatabase, $intArCodeCount=0 ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valName( $objClientDatabase );
        		break;

        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valName( $objClientDatabase );
        		$boolIsValid &= $this->valAssociation( $objClientDatabase, $intArCodeCount, $strAction );
        		break;

        	case VALIDATE_DELETE:
        		$boolIsValid &= $this->valAssociation( $objClientDatabase, $intArCodeCount, $strAction );
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>