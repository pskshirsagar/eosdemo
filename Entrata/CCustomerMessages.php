<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerMessages
 * Do not add any new functions to this class.
 */

class CCustomerMessages extends CBaseCustomerMessages {

	public static function fetchCustomCustomerMessagesByCustomerIdByCid( $intCustomerId, $intOffset, $intLimit, $intCid, $objDatabase, $arrintSystemEmailIds, $boolIsIncludeSystemEmailIds = false, $boolIsOffSetLimit = false, $strKeywordSearch = '', $boolIsTenant = false ) {
		$intOffset 	= ( 0 < $intOffset ) ? $intLimit * ( $intOffset - 1 ) : 0;
		$intOffset 	= ( true == $boolIsOffSetLimit ) ? ' 0 ' : $intOffset;

		$strIncludeSystemEmailIds = ( true == $boolIsIncludeSystemEmailIds ) ? NULL : ' NOT ';
		$strSqlWhere = '';
		$strSqlJoin = '';
		if( true == $boolIsTenant ) {
			$strSqlJoin .= ' LEFT JOIN customer_message_types cmt ON ( cm.customer_message_type_id = cmt.id ) ';
			$strSqlWhere .= ' AND ( cmt.id NOT IN ( ' . ( int ) CCustomerMessageType::ANNOUNCEMENT . ', ' . ( int ) CCustomerMessageType::CLASSIFIED . ', ' . ( int ) CCustomerMessageType::CLUB . ', ' . ( int ) CCustomerMessageType::EVENT . ' ) OR cmt.id IS NULL )';
		}

		$strSql = 'SELECT
    					distinct( cm.id ),
						cm.*,
    					COALESCE( c.name_first || \' \' || c.name_last, p.property_name) as sender_name
					FROM
    					customer_messages cm
    					JOIN lease_customers lc ON( cm.cid = lc.cid AND cm.customer_id = lc.customer_id AND lc.lease_status_type_id <> 2 )
    					JOIN leases l ON(  cm.cid = l.cid AND lc.lease_id = l.id )
    					LEFT JOIN customers c ON ( cm.cid = c.cid AND cm.sender_type_id = 2 AND cm.sender_id = c.id )
    					LEFT JOIN properties p ON ( cm.cid = p.cid AND cm.sender_type_id = 1 AND cm.property_id = p.id and l.property_id = p.id )
    					' . $strSqlJoin . '
					WHERE
						cm.cid = ' . ( int ) $intCid . '
    					AND cm.customer_id = ' . ( int ) $intCustomerId . ' 
    					AND cm.deleted_on IS NULL
						AND COALESCE( c.name_first || \' \' || c.name_last, p.property_name) IS NOT NULL'
						. $strSqlWhere;

		$strSql .= ( true == valArr( $arrintSystemEmailIds ) ) ? ' AND ( cm.system_email_id' . $strIncludeSystemEmailIds . ' IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' ) OR cm.system_email_id IS NULL ) ' : '';

		if( true == valStr( $strKeywordSearch ) ) {
			$strSql .= ' AND ( regexp_replace( cm.subject, E\'<[^>]*>|<style>.*?</style>\', \'\', \'g\')  Ilike \'%' . addslashes( $strKeywordSearch ) . '%\' OR regexp_replace( cm.message, E\'<[^>]*>|<style>.*?</style>\', \'\', \'g\')  Ilike \'%' . addslashes( $strKeywordSearch ) . '%\')';
		}

		$strSql .= ' ORDER BY
    					cm.created_on DESC
    				 LIMIT ' . ( int ) $intLimit . '
    				 OFFSET ' . ( int ) ( $intOffset );

		return self::fetchCustomerMessages( $strSql, $objDatabase );
	}

	public static function fetchCustomerMessagesByIdsByCid( $strCustomerIds, $intCid, $objDatabase ) {
		if( false == valStr( $strCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						cm.*
					FROM
						customer_messages cm
					WHERE
						cm.cid = ' . ( int ) $intCid . '
    					AND cm.id IN ( ' . $strCustomerIds . ' )';

		return self::fetchCustomerMessages( $strSql, $objDatabase );
	}

	public static function fetchCustomerMessagesBySystemEmailIdsByCustomerIdByCid( $arrintSystemEmailIds, $arrstrFieldNames, $intCustomerId, $intCid, $objDatabase, $strInterval = NULL, $intLimit = NULL ) {
		if( false == valArr( $arrintSystemEmailIds ) || false == valArr( $arrstrFieldNames ) || false == is_numeric( $intCustomerId ) ) return NULL;

		$strIntervalCondition 	= ( true == valStr( $strInterval ) ) ? ' AND cm.created_on > ( now() - INTERVAL \'' . $strInterval . '\')' : '';
		$strLimit 				= ( true == is_numeric( $intLimit ) ) ? ' LIMIT ' . $intLimit : '';

		$strSql = 'SELECT
						' . implode( ',', $arrstrFieldNames ) . '
					FROM
						customer_messages cm
					WHERE
						cm.deleted_on IS NULL
						AND cm.is_read	= 0
						AND cm.system_email_id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
						AND cm.cid = ' . ( int ) $intCid . '
    					AND cm.customer_id = ' . ( int ) $intCustomerId . '' . $strIntervalCondition . '
    				ORDER BY cm.created_on DESC' . $strLimit;

		$arrstrCustomerMessages = fetchData( $strSql, $objDatabase );

		$arrstrReorderedCustomerMessages = [];

		if( true == valArr( $arrstrCustomerMessages ) ) {
			foreach( $arrstrCustomerMessages as $arrstrCustomerMessage ) {
				$arrstrReorderedCustomerMessages[$arrstrCustomerMessage['id']] = $arrstrCustomerMessage;
			}
		}

		return $arrstrReorderedCustomerMessages;
	}

	public static function fetchCustomerMessagesCountByCustomerIdByCid( $intCustomerId, $intCid, $arrintSystemEmailIds, $boolIsRead = false, $boolIsIncludeSystemEmailIds = false, $objDatabase, $strKeywordSearch = '', $boolIsTenant = false ) {
		$strIncludeSystemEmailIds = ( true == $boolIsIncludeSystemEmailIds ) ? NULL : ' NOT ';

		$strSqlWhere = '';
		$strSqlJoin = '';
		if( true == $boolIsTenant ) {
			$strSqlJoin .= ' LEFT JOIN customer_message_types cmt ON ( cm.customer_message_type_id = cmt.id ) ';
			$strSqlWhere .= ' AND ( cmt.id NOT IN ( ' . ( int ) CCustomerMessageType::ANNOUNCEMENT . ', ' . ( int ) CCustomerMessageType::CLASSIFIED . ', ' . ( int ) CCustomerMessageType::CLUB . ', ' . ( int ) CCustomerMessageType::EVENT . ' ) OR cmt.id IS NULL )';
		}

		$strSql = 'SELECT
						 count( distinct( cm.id ) ) as count
					FROM
						customer_messages cm
						JOIN lease_customers lc ON( cm.cid = lc.cid AND cm.customer_id = lc.customer_id AND lc.lease_status_type_id <> 2 )
    					JOIN leases l ON(  cm.cid = l.cid AND lc.lease_id = l.id )
						LEFT JOIN customers c ON ( cm.cid = c.cid AND cm.sender_type_id = 2 AND cm.sender_id = c.id )
						LEFT JOIN properties p ON ( cm.cid = p.cid AND cm.sender_type_id = 1 AND cm.property_id = p.id and l.property_id = p.id)
						' . $strSqlJoin . '
					WHERE
						cm.cid = ' . ( int ) $intCid . '
    					AND cm.customer_id = ' . ( int ) $intCustomerId . '
    					AND cm.deleted_on IS NULL
    					AND COALESCE( c.name_first || \' \' || c.name_last, p.property_name) IS NOT NULL'
		                . $strSqlWhere;

		$strSql .= ( true == valArr( $arrintSystemEmailIds ) && true == valArr( array_filter( $arrintSystemEmailIds ) ) ) ? ' AND ( cm.system_email_id' . $strIncludeSystemEmailIds . ' IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' ) OR cm.system_email_id IS NULL ) ' : '';
		$strSql .= ( true == $boolIsRead ) ? ' AND is_read = 0 ' : '';

		if( true == valStr( $strKeywordSearch ) ) {
			$strSql .= ' AND ( regexp_replace( cm.subject, E\'<[^>]*>|<style>.*?</style>\', \'\', \'g\')  Ilike \'%' . addslashes( $strKeywordSearch ) . '%\' OR regexp_replace( cm.message, E\'<[^>]*>|<style>.*?</style>\', \'\', \'g\')  Ilike \'%' . addslashes( $strKeywordSearch ) . '%\')';
		}

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchCustomerMessagesByReferenceIdByCustomerMessageTypeIdBySenderTypeIdByPropertyIdByCid( $intReferenceId, $intCustomerMessageTypeId, $intSenderTypeId, $arrintPropertyIds, $intCid, $objDatabase, $boolIsShowDeletedCustomerMessages = false ) {

		if( false == valId( $intReferenceId ) || false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						customer_messages
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND sender_type_id = ' . ( int ) $intSenderTypeId . '
						AND customer_message_type_id = ' . ( int ) $intCustomerMessageTypeId . '
						AND reference_id = ' . ( int ) $intReferenceId;
		if( false == $boolIsShowDeletedCustomerMessages ) {
			$strSql .= ' AND deleted_by IS NULL
						AND deleted_on IS NULL';
		}

		return self::fetchCustomerMessages( $strSql, $objDatabase );
	}

}
?>