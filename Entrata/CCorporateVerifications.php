<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateVerifications
 * Do not add any new functions to this class.
 */

class CCorporateVerifications extends CBaseCorporateVerifications {

	public static function fetchCorporateVerificationByCorporateIdByCid( $intCorporateId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cv.*
					FROM
						corporate_verifications cv
						JOIN corporates c ON ( c.id = cv.corporate_id AND cv.cid = c.cid AND cv.cid = ' . ( int ) $intCid . ' )
					WHERE
						cv.corporate_id = ' . ( int ) $intCorporateId . '
						AND cv.cid = ' . ( int ) $intCid . '
						ORDER BY cv.created_on';

		return parent::fetchCorporateVerifications( $strSql, $objDatabase );
	}

	public static function fetchCorporateVerificationByIdByCid( $intCorporateVerificationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						corporate_verifications
					WHERE
						id = ' . ( int ) $intCorporateVerificationId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchCorporateVerification( $strSql, $objDatabase );
	}

	public static function fetchActionableCorporateVerificationByCorporateIdByCid( $intCorporateId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cv.*
					FROM
						corporate_verifications cv
						JOIN corporates c ON ( c.id = cv.corporate_id AND cv.cid = c.cid AND cv.cid = ' . ( int ) $intCid . ' )
					WHERE
						cv.corporate_id = ' . ( int ) $intCorporateId . '
						AND cv.cid = ' . ( int ) $intCid . '
						AND cv.application_id IS NULL
					ORDER BY 
						cv.created_on';

		return parent::fetchCorporateVerifications( $strSql, $objDatabase );
	}

	public static function fetchCorporateVerificationCountByCorporateIdByCid( $intCorporateId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count(cv.id) as total_count
					FROM
						corporate_verifications cv
						JOIN corporates c ON ( c.id = cv.corporate_id AND cv.cid = c.cid )
					WHERE
						cv.corporate_id = ' . ( int ) $intCorporateId . '
						AND cv.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

}
?>