<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorkbookAssumptions
 * Do not add any new functions to this class.
 */

class CBudgetWorkbookAssumptions extends CBaseBudgetWorkbookAssumptions {

	public static function fetchBudgetWorkbookAssumptionsWithSystemAssumptionsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {

		if( false == valId( $intBudgetWorkbookId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = ' SELECT 
						bwa.*,
						util_get_translated( \'name\', ba.name, ba.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS budget_assumption_name,
						util_get_translated( \'description\', ba.description, ba.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS budget_assumption_description,
						ba.key As budget_assumption_key
					FROM
						budget_workbook_assumptions bwa
						LEFT JOIN budget_assumption_types ba ON ( bwa.budget_assumption_type_id = ba.id )
					WHERE 
						bwa.cid = ' . ( int ) $intCid . '
						AND bwa.budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . '
						AND bwa.deleted_by IS NULL
					ORDER BY
						bwa.name,
						ba.name';

		return parent::fetchBudgetWorkbookAssumptions( $strSql, $objDatabase );
	}

	public static function fetchCustomBudgetWorkbookAssumptionsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		if( false == valId( $intBudgetWorkbookId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bwa.*,
						CASE WHEN COUNT( bwga.id ) = 0 AND COUNT( bwi.id ) = 0 THEN
							FALSE
						ELSE
							TRUE
						END AS is_used_in_formula
					FROM
						budget_workbook_assumptions bwa
						LEFT JOIN budget_worksheets bw ON ( bw.cid = bwa.cid AND bw.budget_workbook_id = bwa.budget_workbook_id )
						LEFT JOIN budget_worksheet_gl_accounts bwga ON ( bwga.cid = bw.cid AND bwga.budget_worksheet_id = bw.id AND bwga.formula LIKE CONCAT( \'%(\', bwa.key, \')%\' ) )
						LEFT JOIN budget_worksheet_items bwi ON ( bwi.cid = bw.cid AND bwi.budget_worksheet_id = bw.id AND bwi.formula LIKE CONCAT( \'%(\', bwa.key, \')%\' ) )
					WHERE
						bwa.cid = ' . ( int ) $intCid . '
						AND bwa.deleted_by IS NULL
						AND bwa.budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . '
						AND bwa.budget_assumption_type_id = ' . CBudgetAssumptionType::CUSTOM . '
						AND bw.deleted_by IS NULL
					GROUP BY
						bwa.id,
						bwa.cid
					ORDER BY
						bwa.id';

		return self::fetchBudgetWorkbookAssumptions( $strSql, $objDatabase );
	}

	public static function fetchUsedBudgetWorkbookAssumptionsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase, $boolIsSystem = NULL ) {

		if( false == valId( $intBudgetWorkbookId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		if( true == $boolIsSystem ) {
			$strSelectField			                = 'bwa.budget_assumption_type_id, ';
			$strSqlJoinConditionsForGlAccounts	    = ' JOIN budget_assumption_types ba ON ( ba.id = bwa.budget_assumption_type_id )
														JOIN budget_worksheet_gl_accounts bwga ON ( bwga.cid = bw.cid AND bwga.budget_worksheet_id = bw.id AND bwga.formula LIKE CONCAT( \'%(\', ba.key, \')%\' ) )';
			$strSqlJoinConditionsForWorksheetItems	= ' JOIN budget_assumption_types ba ON ( ba.id = bwa.budget_assumption_type_id )
														JOIN budget_worksheet_items bwi ON ( bwi.cid = bw.cid AND bwi.budget_worksheet_id = bw.id AND bwi.formula LIKE CONCAT( \'%(\', ba.key, \')%\' ) )';
		} else {
			$strSelectField                        = '';
			$strSqlJoinConditionsForGlAccounts     = ' JOIN budget_worksheet_gl_accounts bwga ON ( bwga.cid = bw.cid AND bwga.budget_worksheet_id = bw.id AND bwga.formula LIKE CONCAT( \'%(\', bwa.key, \')%\' ) )';
			$strSqlJoinConditionsForWorksheetItems = ' JOIN budget_worksheet_items bwi ON ( bwi.cid = bw.cid AND bwi.budget_worksheet_id = bw.id AND bwi.formula LIKE CONCAT( \'%(\', bwa.key, \')%\' ) )';
		}

		$strSql = 'SELECT
						bwa.id, ' . $strSelectField . '
						bw.id AS budget_worksheet_id,
						util_get_translated( \'name\', bw.name, bw.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS budget_worksheet_name,
						bwga.gl_account_id,
						NULL AS budget_worksheet_item_id,
						NULL AS item_name,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS gl_account_name
					FROM
						budget_workbook_assumptions bwa
						JOIN budget_worksheets bw ON ( bw.cid = bwa.cid AND bw.budget_workbook_id = bwa.budget_workbook_id ) ' . $strSqlJoinConditionsForGlAccounts . '
						JOIN gl_account_trees gat ON ( gat.cid = bwga.cid AND gat.gl_account_id = bwga.gl_account_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
					WHERE
						bwa.cid = ' . ( int ) $intCid . '
						AND bwga.budget_data_source_type_id = ' . CBudgetDataSourceType::CUSTOM_FORMULAS . '
						AND bwa.budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . ( ( true == $boolIsSystem ) ? ' AND bwa.budget_assumption_type_id <> ' . CBudgetAssumptionType::CUSTOM . '' : ' AND bwa.budget_assumption_type_id = ' . CBudgetAssumptionType::CUSTOM . '' ) . '
						AND bwa.deleted_by IS NULL
						AND bw.deleted_by IS NULL
					UNION ALL
					SELECT
						bwa.id, ' . $strSelectField . '
						bw.id AS budget_worksheet_id,
						util_get_translated( \'name\', bw.name, bw.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS budget_worksheet_name,
						bwi.gl_account_id,
						bwi.id AS budget_worksheet_item_id,
						util_get_translated( \'item_name\', bwi.item_name, bwi.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS item_name,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS gl_account_name
					FROM
						budget_workbook_assumptions bwa
						JOIN budget_worksheets bw ON ( bw.cid = bwa.cid AND bw.budget_workbook_id = bwa.budget_workbook_id ) ' . $strSqlJoinConditionsForWorksheetItems . '
						JOIN gl_account_trees gat ON ( gat.cid = bwi.cid AND gat.gl_account_id = bwi.gl_account_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
					WHERE
						bwa.cid = ' . ( int ) $intCid . '
						AND bwa.deleted_by IS NULL
						AND bw.deleted_by IS NULL
						AND bwi.budget_data_source_type_id  = ' . CBudgetDataSourceType::CUSTOM_FORMULAS . '
						AND bwa.budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . ( ( true == $boolIsSystem ) ? ' AND bwa.budget_assumption_type_id <> ' . CBudgetAssumptionType::CUSTOM . '' : ' AND bwa.budget_assumption_type_id = ' . CBudgetAssumptionType::CUSTOM . '' );

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchAllBudgetWorkbookAssumptionsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase, $boolIsSystem = false ) {

		if( false == valId( $intBudgetWorkbookId ) ) return NULL;

		$strSqlCondition = ( true == $boolIsSystem ) ? ' AND budget_assumption_type_id <> ' . CBudgetAssumptionType::CUSTOM . '' : ' AND budget_assumption_type_id = ' . CBudgetAssumptionType::CUSTOM;

		$strSql = 'SELECT 
						* 
					FROM 
						budget_workbook_assumptions 
					WHERE 
					cid = ' . ( int ) $intCid .
					' AND deleted_by IS NULL
					 AND budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . $strSqlCondition;

		return parent::fetchBudgetWorkbookAssumptions( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorkbookAssumptionsByKeysByBudgteWorkbookIdByCid( $arrstrKeys, $intBudgetWorkbookId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) || false == valId( $intBudgetWorkbookId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						budget_workbook_assumptions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . '
						AND deleted_by IS NULL
						AND key IN ( ' . sqlStrImplode( $arrstrKeys ) . ' )';

		return self::fetchBudgetWorkbookAssumptions( $strSql, $objDatabase );
	}

}

?>