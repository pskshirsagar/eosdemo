<?php

class CToDoType extends CBaseToDoType {

	const SCHEDULED_APPOINTMENT							= 1;
	const SCHEDULED_FOLLOW_UP 							= 2;
	const VOICEMAIL_RECEIVED 							= 3;
	const INCOMING_EMAIL								= 4;
	const INCOMING_SMS									= 5;
	const NEW_LEAD 										= 6;  // Old Id 4
	const NO_RECENT_CONTACT 							= 7;  // Old Id 5
	const LEASE_EXPIRED 								= 8;  // Old Id 6
	const UNSIGNED_LEASE_DUE_TO_EXPIRE 					= 9;  // Old Id 7
	const ANSWERED_CALL		 							= 10;
	const UNANSWERED_CALL 								= 11;
	const RENEWAL_OFFER_EXPIRATION						= 12;
	const LEASE_PENDING									= 13;
	const LEASES_GENERATED								= 14;
	const LEASES_COMPLETED								= 15;
	const LEASES_APPROVED								= 16;
	const APPLICATION_STARTED							= 17;
	const WAITING_FOR_CO_APPLICANTS_PRIMARY_APPLICANT	= 18;
	const WAITING_FOR_CO_APPLICANTS						= 19;
	const APPLICATION_COMPLETED							= 20;
	const TOUR_MISSED									= 21;
	const TOUR_COMPLETED								= 22;

	public static $c_arrintManagerToDoTypes = [
		self::SCHEDULED_FOLLOW_UP,
		self::VOICEMAIL_RECEIVED,
		self::SCHEDULED_APPOINTMENT
	];

	public function toDoTypeIdToStr( $intToDoTypeId ) {
		switch( $intToDoTypeId ) {

			case self::SCHEDULED_APPOINTMENT:
				return 'Scheduled Appointment';
				break;

			case self::SCHEDULED_FOLLOW_UP:
				return 'Scheduled Manual Contact';
				break;

			case self::VOICEMAIL_RECEIVED:
				return 'Voicemail Received';
				break;

			case self::INCOMING_EMAIL:
				return 'Incoming Email';
				break;

			case self::INCOMING_SMS:
				return 'Incoming SMS';
				break;

			case self::NEW_LEAD:
				return 'New Lead';
				break;

			case self::NO_RECENT_CONTACT:
				return 'No Recent Contact';
				break;

			case self::LEASE_EXPIRED:
				return 'Lease Expired';
				break;

			case self::UNSIGNED_LEASE_DUE_TO_EXPIRE:
				return 'Lease Expiring';
				break;

			case self::RENEWAL_OFFER_EXPIRATION:
				return 'Renewal Offer about to expire';
				break;

			case self::LEASE_PENDING:
				return 'Lease Pending';
				break;

            default:
            	// default case
            	break;
		}
	}

}
?>