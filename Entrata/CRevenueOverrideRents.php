<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueOverrideRents
 * Do not add any new functions to this class.
 */

class CRevenueOverrideRents extends CBaseRevenueOverrideRents {

	const DEFAULT_UNIT_SPACE_ID = -1;

	public static function fetchRevenueOverrideRentsByLeaseTermByMoveInDateByUnitSpaceIdByPropertyIdByCid( $intLeaseTerm, $strMoveInDate, $intUnitSpaceId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						revenue_override_rents
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
                        AND lease_term_month = ' . ( int ) $intLeaseTerm . '
						AND move_in_date = \'' . $strMoveInDate . '\'';

		$strSql .= ( self::DEFAULT_UNIT_SPACE_ID == $intUnitSpaceId ) ? ' AND unit_space_id IS NULL ' : ' AND unit_space_id = ' . ( int ) $intUnitSpaceId;

		return self::fetchRevenueOverrideRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $arrintUnitSpaceIds = [], $intDefaultLeaseTermId = 0 ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSqlLeaseCondition     = '';
		$strSqlUnitSpaceCondition = '';
		if( 1 == $intDefaultLeaseTermId ) {
			$strSqlLeaseCondition = 'AND lease_term_month = ' . CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM;
		}
		if( true == valArr( $arrintUnitSpaceIds ) ) {
			$strSqlUnitSpaceCondition = 'AND unit_space_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )';
		}

		$strSql = sprintf( ' SELECT
						ror.*
					FROM
						revenue_override_rents ror
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid AND us.property_id = ror.property_id AND us.unit_type_id = ror.unit_type_id )
					WHERE
						ror.cid = %1$d
						AND us.show_on_website = TRUE
						AND us.unit_exclusion_reason_type_id IN ( %5$d, %6$d  )
						AND us.is_marketed = TRUE
						AND ror.property_id IN ( %2$s )
						%3$s
						%4$s', $intCid, sqlIntImplode( $arrintPropertyIds ), $strSqlLeaseCondition, $strSqlUnitSpaceCondition, \CUnitExclusionReasonType::NOT_EXCLUDED, \CUnitExclusionReasonType::CONSTRUCTION_UNIT );

		return self::fetchRevenueOverrideRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOverrideRentsDefaultRentByUnitTypeIdByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ror.*
					FROM
						revenue_override_rents ror
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id = ' . ( int ) $intPropertyId . '
						AND ror.unit_space_id IS NULL';

		$strSql .= ( 0 < $intUnitTypeId ) ? ' AND ror.unit_type_id = ' . ( int ) $intUnitTypeId : '';

		return self::fetchRevenueOverrideRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByUnitTypeIdsByCid( $arrintUnitTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintUnitTypeIds ) ) return NULL;

		$strSql = sprintf( 'SELECT
						ror.*
					FROM
						revenue_override_rents ror
						JOIN unit_spaces us ON (ror.cid = us.cid AND ror.unit_space_id = us.id AND ror.property_id=us.property_id AND ror.unit_type_id = us.unit_type_id)
					WHERE
						ror.cid = %1$d
						AND us.show_on_website = TRUE
						AND us.is_marketed = TRUE
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ')
						AND us.unit_type_id IN( %2$s )', $intCid, sqlIntImplode( $arrintUnitTypeIds ) );

		return parent::fetchRevenueOverrideRents( $strSql, $objDatabase );
	}

	public static function deleteFromRevenueOverrideRents( $intUnitSpaceId, $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strCondition = '';
		$strUnitSpaceCondition = '';
		if( false == empty( $intPropertyId ) && false == empty( $intUnitTypeId ) && false == empty( $intUnitSpaceId ) && -1 != $intUnitSpaceId ) {
			$strCondition = ' AND ror.unit_space_id = ' . ( int ) $intUnitSpaceId;
		} elseif( false == empty( $intPropertyId ) && false == empty( $intUnitTypeId ) ) {
			$strCondition = ' AND ror.unit_type_id = ' . ( int ) $intUnitTypeId;
		}
		if( CPricingLibrary::DEFAULT_UNIT_SPACE_ID == $intUnitSpaceId ) {
			$strUnitSpaceCondition = ' AND ror.unit_space_id IS NULL ';
		}
		$strDeleteSql = 'DELETE FROM
									revenue_override_rents ror
								WHERE
									ror.cid	= ' . ( int ) $intCid . '
									AND ror.property_id = ' . ( int ) $intPropertyId . $strCondition . $strUnitSpaceCondition;

		return fetchData( $strDeleteSql, $objDatabase );
	}

	public static function deleteRevenueOverrideRentsRenewableUnitsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $intUnitTypeId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		$strCondition = '';
		$strRenewableUnitCondition = '';
		if( false == empty( $intPropertyId ) && false == empty( $intUnitTypeId ) && false == empty( $intUnitSpaceId ) && -1 != $intUnitSpaceId ) {
			$strCondition = ' AND ror.unit_space_id = ' . ( int ) $intUnitSpaceId;
		} elseif( false == empty( $intPropertyId ) && false == empty( $intUnitTypeId ) ) {
			$strCondition = ' AND ror.unit_type_id = ' . ( int ) $intUnitTypeId;
		} elseif( false == empty( $intPropertyId ) && false == empty( $intUnitSpaceId ) ) {
			$strCondition = ' AND ror.unit_space_id = ' . ( int ) $intUnitSpaceId;
		}
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		if( -1 != $intUnitSpaceId ) {
			$strRenewableUnitCondition = ' AND ror.unit_space_id IN ( SELECT us.id FROM unit_spaces us WHERE us.cid = ' . ( int ) $intCid . ' AND us.property_id = ' . ( int ) $intPropertyId . ' AND us.unit_space_status_type_id NOT IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) )';
		}

		$strDeleteSql = 'DELETE
							FROM
								revenue_override_rents ror
							WHERE
								ror.cid	= ' . ( int ) $intCid . '
								AND ror.property_id = ' . ( int ) $intPropertyId . $strCondition . $strRenewableUnitCondition;

		return fetchData( $strDeleteSql, $objDatabase );
	}

	public static function deleteRevenueOverrideRentsAvailableUnitsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $intUnitTypeId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		$strCondition = '';
		$strAvailableUnitCondition = '';
		if( false == empty( $intPropertyId ) && false == empty( $intUnitTypeId ) && false == empty( $intUnitSpaceId ) && -1 != $intUnitSpaceId ) {
			$strCondition = ' AND ror.unit_space_id = ' . ( int ) $intUnitSpaceId;
		} elseif( false == empty( $intPropertyId ) && false == empty( $intUnitTypeId ) ) {
			$strCondition = ' AND ror.unit_type_id = ' . ( int ) $intUnitTypeId;
		} elseif( false == empty( $intPropertyId ) && false == empty( $intUnitSpaceId ) ) {
			$strCondition = ' AND ror.unit_space_id = ' . ( int ) $intUnitSpaceId;
		}
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		if( -1 != $intUnitSpaceId ) {
			$strAvailableUnitCondition = ' AND ror.unit_space_id IN ( SELECT us.id FROM unit_spaces us WHERE us.cid = ' . ( int ) $intCid . ' AND us.property_id = ' . ( int ) $intPropertyId . 'AND us.show_on_website = TRUE AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) )';
		}

		$strDeleteSql = 'DELETE
							FROM
								revenue_override_rents ror
							WHERE
								ror.cid	= ' . ( int ) $intCid . '
								AND ror.property_id = ' . ( int ) $intPropertyId . $strCondition . $strAvailableUnitCondition;

		return fetchData( $strDeleteSql, $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByPropertyIdByUnitTypeIdsByUnitSpaceIdCid( $arrintPropertyIds, $arrintUnitTypeIds, $arrintUnitSpaceIds, $intCid, $objDatabase, $intIsRenewal = 0 ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( 1 == $intIsRenewal ) {
			$strAvailableCond = ' AND us.is_available = FALSE ';
		} else {
			$strAvailableCond = ' AND us.is_available = TRUE ';
		}

		if( false == valArr( $arrintUnitTypeIds ) && true == valArr( $arrintUnitSpaceIds ) ) {
			$strWhereCondition = ' AND ror.unit_space_id IN( ' . implode( ',', $arrintUnitSpaceIds ) . ' )';
		} else {
			if( true == valArr( $arrintUnitTypeIds ) ) {
				$strWhereCondition = 'AND ror.unit_type_id IN( ' . implode( ',', $arrintUnitTypeIds ) . ' )';
			} else {
				return NULL;
			}
		}

		$strSql = 'WITH overriden_rates AS (
						SELECT
							ror.property_id,
							ror.unit_type_id,
							ror.unit_space_id,
							ror.std_optimal_rent,
							rov.override_rent_amount,
							rov.override_rent_reason,
							COALESCE( ut.lookup_code, ut.name ) AS lookup_code,
							us.unit_number_cache as unit_space_name,
						    RANK() OVER ( PARTITION BY ror.unit_space_id ORDER BY rov.move_in_date ASC ) AS rank
						FROM
							revenue_optimal_rents ror
							JOIN unit_spaces us ON ( us.cid = ror.cid AND us.property_id = ror.property_id AND us.id = ror.unit_space_id )
							JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.deleted_on IS NULL )
							LEFT JOIN revenue_override_rents rov ON ( rov.cid = ror.cid AND rov.property_id = ror.property_id AND rov.unit_space_id = ror.unit_space_id AND rov.lease_term_month = 12 )
						WHERE
							ror.cid = ' . ( int ) $intCid . '
							AND ror.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhereCondition . $strAvailableCond . ' 
		                ORDER BY
							ror.unit_type_id
					)
					SELECT
						*
					FROM
						overriden_rates
					WHERE rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteRevenueOverrideRentsByUnitSpaceIdsByPropertyIdsByCid( $arrintUnitSpaceIds, $arrintUnitTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strDeleteSql = 'DELETE
							FROM
								revenue_override_rents ror
							WHERE
								ror.cid	= ' . ( int ) $intCid . '
								AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND ror.unit_type_id IN ( ' . implode( ',', $arrintUnitTypeIds ) . ' )
								AND ror.unit_space_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' ) ';

		return fetchData( $strDeleteSql, $objDatabase );
	}

	public static function fetchRenewalRevenueOverrideRentsByPropertyIdByUnitTypeIdsByUnitSpaceIdCid( $arrintPropertyIds, $arrintUnitTypeIds, $arrintUnitSpaceIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( false == valArr( $arrintUnitTypeIds ) && true == valArr( $arrintUnitSpaceIds ) ) {
			$strWhereCondition = ' AND ror.unit_space_id IN( ' . implode( ',', $arrintUnitSpaceIds ) . ' )';
		} else {
			if( true == valArr( $arrintUnitTypeIds ) ) {
				$strWhereCondition = 'AND ror.unit_type_id IN( ' . implode( ',', $arrintUnitTypeIds ) . ' )';
			} else {
				return NULL;
			}
		}
		$strSqlCondition = 'CURRENT_DATE AND CURRENT_DATE + CAST (  \'90\' || \' days\' AS INTERVAL ) ';

		$strSql = 'WITH overriden_rates AS (
						SELECT
							ror.property_id,
							ror.unit_type_id,
							ror.unit_space_id,
							ror.std_optimal_rent,
							rov.override_rent_amount,
							rov.override_rent_reason,
							COALESCE( ut.lookup_code, ut.name ) AS lookup_code,
							us.unit_number_cache as unit_space_name,
						    RANK() OVER ( PARTITION BY ror.unit_space_id ORDER BY rov.move_in_date ASC ) AS rank
						FROM
							revenue_optimal_rents ror
							JOIN unit_spaces us ON ( us.cid = ror.cid AND us.property_id = ror.property_id AND us.id = ror.unit_space_id )
							JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.deleted_on IS NULL )
							LEFT JOIN revenue_override_rents rov ON ( rov.cid = ror.cid AND rov.property_id = ror.property_id AND rov.unit_space_id = ror.unit_space_id AND rov.lease_term_month = 12 )
							JOIN
							(
								SELECT
									l.cid,
									l.unit_space_id,
									l.id,
									l.active_lease_interval_id,
									li.lease_interval_type_id,
									li.lease_end_date,
									li.lease_start_date,
									li.lease_term_id,
									ROW_NUMBER() OVER( PARTITION BY l.cid, l.unit_space_id ORDER BY l.active_lease_interval_id DESC ) AS row_num
								FROM
									leases l
									JOIN lease_intervals AS li ON( li.cid = l.cid AND li.property_id = l.property_id AND li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ', ' . CLeaseStatusType::APPLICANT . ' ) )
								WHERE
									l.cid = ' . ( int ) $intCid . '
							) AS leases ON ( leases.cid = us.cid AND leases.unit_space_id = us.id AND leases.row_num = 1 )
							JOIN lease_processes AS lp ON( lp.cid = us.cid AND lp.lease_id = leases.id )
							LEFT JOIN lease_intervals rli ON ( rli.cid = leases.cid AND rli.lease_id = leases.id AND rli.lease_interval_type_id IN( ' . CLeaseIntervalType::RENEWAL . ' ) AND rli.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' )
							LEFT JOIN lease_terms AS lt ON( lt.cid = us.cid AND lt.id = leases.lease_term_id ) AND lt.is_disabled = FALSE AND lt.deleted_on IS NULL
							LEFT JOIN property_charge_settings pcs ON( pcs.cid = us.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND pcs.property_id = us.property_id )
						WHERE
							ror.cid = ' . ( int ) $intCid . '
							AND ror.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhereCondition . '
							AND COALESCE( us.occupancy_type_id, 0 ) != ' . ( int ) COccupancyType::AFFORDABLE . '
							AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE . '
							AND us.deleted_on IS NULL
							AND us.unit_exclusion_reason_type_id = ' . CUnitExclusionReasonType::NOT_EXCLUDED . '
							AND rli.id IS NULL
							AND ( ( ( leases.lease_end_date IS NOT NULL AND leases.lease_end_date BETWEEN ' . $strSqlCondition . ' )
									OR ( leases.lease_end_date IS NULL AND lp.move_out_date IS NOT NULL AND lp.move_out_date BETWEEN ' . $strSqlCondition . ' )
									OR ( leases.lease_end_date IS NULL AND lp.move_out_date IS NULL AND leases.lease_start_date IS NOT NULL AND leases.lease_start_date + ( interval \'1 months\' * COALESCE( lt.term_month, 12 ) ) BETWEEN ' . $strSqlCondition . ' ) 
									)
								OR ( leases.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ) )
			                ORDER BY
								ror.unit_type_id
						)
						SELECT
							*
						FROM
							overriden_rates
						WHERE rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByFilter( $objRevenueOverrideRentsFilter, $intCid, $objDatabase ) {

		$strSql = sprintf( " SELECT 
                        *
		            FROM revenue_override_rents ror
					WHERE 
						ror.unit_type_id IN( %s )
						AND ror.cid = %d
		                AND ror.property_id = %d
		                AND ror.lease_term_id = %d
		                AND ror.lease_start_window_id = %d
		                AND util.util_to_int( ror.details->>'unit_space_configuration_id' ) = %d
		                AND util.util_to_int(ror.details->>'is_renewal') = %d ", $objRevenueOverrideRentsFilter->unit_type_id, $intCid, $objRevenueOverrideRentsFilter->property_id, $objRevenueOverrideRentsFilter->lease_term_id, $objRevenueOverrideRentsFilter->lease_start_window_id, $objRevenueOverrideRentsFilter->space_configuration_id, $objRevenueOverrideRentsFilter->is_renewal );

		return self::fetchRevenueOverrideRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOverrideRentsByUnitTypeIdsByPropertyIdByCid( $arrintUnitTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintUnitTypeIds ) ) {
			return [];
		}

		$strSql = sprintf( " SELECT 
                        ror.id,
                        ror.unit_type_id,
                        ror.override_rent_reason,
                        ror.override_rent_amount,
                        ror.original_optimal_rent,
                        ror.property_id,
                        ror.cid,
                        ror.lease_term_id,
                        ror.lease_start_window_id,
                        ror.details->>'unit_space_configuration_id' as unit_space_configuration_id,
                        ror.details->>'is_renewal' as is_renewal
		            FROM revenue_override_rents ror
					WHERE 
						ror.unit_type_id IN ( %s )
						AND ror.cid = %d
		                AND ror.property_id = %d
		                ", sqlIntImplode( $arrintUnitTypeIds ), $intCid, $intPropertyId );

		return self::fetchObjects( $strSql, 'CRevenueOverrideRent', $objDatabase );
	}

}
?>