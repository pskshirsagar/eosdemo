<?php

class CTransmissionVendor extends CBaseTransmissionVendor {

    const BLUE_MOON								    = 1;
	const LEVEL_1									= 2;
	const LEAD_TO_LEASE								= 3;
	const LEAD_TRACKING_SOLUTIONS					= 4;
	const BLUE_MOON_LEASE							= 5;
	const LRO										= 6;
	const YIELDSTAR									= 7;
	const SAFERENT									= 8;
	const ONSITE									= 10;
	const FIRST_ADVANTAGE							= 11;
	const RESIDENT_CHECK							= 12;
	const RENT_GROW									= 13;
	const SCREENING_REPORTS							= 14;
	const LEASINGDESK								= 15;
	const VERIFIRST									= 16;
	const RENTAL_HISTORY_REPORTS					= 17;
	const APARTMENTS								= 18;
	const VELOCITY								    = 19;
	const BACKGROUND_INVESTIGATIONS					= 20;
	const RESIDENT_VERIFY							= 21;
	const SOUND_SCREENING							= 22;
	const FABCO										= 23;
	const AMRENT									= 24;
	const VAULTWARE 								= 25;
	const PEOPLESOFT								= 26;
	const NTN										= 27;
	const TRANSUNION_CREDIT_RETRIEVER_CR6			= 28;
	const TAZ_WORKS									= 29;
	const YARDI										= 30;
	const MRI										= 31;
	const BLUE_MOON_DOC_STORAGE						= 32;
	const CHATMETER									= 33;
	const YEXT										= 34;
	const APARTMENTRATINGS							= 35;
	const LEASINGDESK_MITS							= 36;
	const GL_EXPORT									= 37;
	const THE_SCREENING_PROS						= 38;
	const CONSERVICE        						= 41;
	const YIELDSTAR_EXPORT							= 42;
	const COMPLIANCE_DEPOT                          = 43;
	const FACEBOOK							        = 44;
	const UBSEXPORT     							= 45;
	const CUSTOM     							    = 46;
	const AP_EXPORT									= 47;
	const TENANT_TECH								= 48;
	const GOOGLE                                    = 49;
	const REALID     								= 50;
	const WELLS_FARGO_PAYMENT_MANAGER				= 51;

	const PP_EXPORT_CAPITAL_ONE                     = 52;
	const PP_EXPORT_KEY_BANK                        = 53;
	const PP_EXPORT_BBT_BANK                        = 54;
	const PP_EXPORT_JP_MORGAN_CHASE                 = 55;
	const PP_EXPORT_WELLS_FARGO                     = 56;
	const PP_EXPORT_WASHINGTON_FIRST                = 57;
	const PP_EXPORT_EAST_WEST_BANK                  = 58;
	const PP_EXPORT_FROST_BANK                      = 59;
	const PP_EXPORT_COM_DATA_BANK                   = 60;
	const PP_EXPORT_TD_BANK                         = 61;
	const PP_EXPORT_BANK_OF_AMERICA                 = 62;
	const PP_EXPORT_WASHINGTON_TRUST                = 63;
	const PP_EXPORT_US_BANK                         = 64;
	const PP_EXPORT_PNC_BANK                        = 65;
	const PP_EXPORT_BMO_HARRIS_BANK                 = 66;
	const PP_EXPORT_OTHER_BANKS                     = 67;
	const PP_EXPORT_CITIZENS_BANK                   = 68;
	const PP_EXPORT_COLORADO_STATE_BANK_AND_TRUST   = 69;
	const PP_EXPORT_PRIVATE_BANK_CIBC               = 70;
	const PP_EXPORT_CITY_NATIONAL_BANK              = 71;
	const PP_EXPORT_VECTRA_BANK                     = 72;
	const PP_EXPORT_CITI_BANK                       = 73;
	const PP_EXPORT_CENTRAL_BANK_OF_BOONE_COUNTY    = 74;
	const PP_EXPORT_FIRST_MERCHANT_BANK			    = 75;
	const PP_EXPORT_CADENCE_BANK					= 76;
	const PP_EXPORT_MORTON_COMMUNITY_BANK           = 77;
	const PP_EXPORT_FIRST_BANK					    = 78;
	const PP_EXPORT_CITYWIDE_BANK					= 79;
	const GL_EXPORT_QUICK_BOOKS                     = 80;
	const GL_EXPORT_SKYLINE                         = 81;
	const GL_EXPORT_TIMBERLINE_PM                   = 82;
	const GL_EXPORT_PEOPLESOFT                      = 83;
	const GL_EXPORT_MRI                             = 84;
	const GL_EXPORT_JENARK                          = 85;
	const GL_EXPORT_YARDI                           = 86;
	const GL_EXPORT_ONESITE                         = 87;
	const GL_EXPORT_AMSI                            = 88;
	const GL_EXPORT_PEAK                            = 89;
	const GL_EXPORT_JD_EDWARDS                      = 90;
	const GL_EXPORT_ORACLE                          = 91;
	const GL_EXPORT_GREAT_PLAINS                    = 92;
	const GL_EXPORT_YARDI_ETL                       = 93;
	const GL_EXPORT_YARDI_GL_EXPORT_7S              = 94;
	const GL_EXPORT_YARDI_ETL_MR                    = 95;
	const GL_EXPORT_YARDI_CA                        = 96;
	const AP_EXPORT_QUICK_BOOKS                     = 97;
	const AP_EXPORT_SKYLINE                         = 98;
	const AP_EXPORT_TIMBERLINE_PM                   = 99;
	const AP_EXPORT_PEOPLESOFT                      = 100;
	const AP_EXPORT_MRI                             = 101;
	const AP_EXPORT_JENARK                          = 102;
	const AP_EXPORT_YARDI                           = 103;
	const AP_EXPORT_ONESITE                         = 104;
	const AP_EXPORT_AMSI                            = 105;
	const AP_EXPORT_PEAK                            = 106;
	const AP_EXPORT_JD_EDWARDS                      = 107;
	const AP_EXPORT_YARDI_ETL_INVOICE_REGISTER      = 108;
	const AP_EXPORT_YARDI_ETL_FINPAYABLES           = 109;
	const PP_EXPORT_IBERIA_BANK                     = 110;
	const PP_EXPORT_HUNTINGTON_BANK                 = 111;
	const PP_EXPORT_FIFTH_THIRD_BANK                = 112;
	const PP_EXPORT_FIRST_COMMONWEALTH              = 113;
	const PP_EXPORT_SIGNATURE_BANK                  = 114;
	const PP_EXPORT_FIRST_TENNESSEE_BANK            = 115;
	const PP_EXPORT_TCF_COMMERCIAL_BANK             = 116;

	const GENERIC_SCREENING_LIBRARY                 = 117;
	const CARVAJAL                                  = 119;
	const GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL = 120;
	const PP_EXPORT_HSBC_BANK                       = 121;
	const PP_EXPORT_M_AND_T_BANK                    = 122;
	const PP_EXPORT_POSPAYEXTRACTCUSTOM             = 123;
	const PP_EXPORT_BRIDGEWATER_BANK                = 124;
	const PP_EXPORT_MIDFIRST_BANK                   = 125;
	const AP_ACH_PAYMENTS                           = 126;
	const GL_EXPORT_PEOPLESOFT_AR_JRNL              = 127;
	const AP_EXPORT_PEOPLESOFT_REFUNDS              = 128;
	const PP_EXPORT_CITIZENS_BUSINESS_BANK          = 129;
	const PP_EXPORT_REVERE_BANK                     = 130;
	const SPAIN_POLICE_EXPORT                       = 131;
	const API_EXPORT                                = 132;
	const GL_EXPORT_YARDI_GL_EXPORT_7S_MX           = 133;
	const GL_EXPORT_YARDI_ETL_GS                    = 134;
    const BCS                                       = 135;
    const PP_EXPORT_REGIONS_BANK                    = 136;
    const PP_EXPORT_COMERICA_BANK                   = 137;
	const PP_EXPORT_HANCOCK_WHINEY_BANK             = 138;
	const DATA_EXPORT                               = 139;
	const PP_EXPORT_AMEGY_BANK                      = 140;
	const GL_EXPORT_YARDI_SIPP                      = 141;
	const PP_EXPORT_CITY_DIRECT                     = 142;
	const SITEMINDER                                = 143;
	const AP_EXPORT_YARDI_SIPP                      = 144;
	const PP_EXPORT_TOWNE_BANK                      = 145;
	const PP_EXPORT_OAKSTAR_BANK                    = 146;
	const PP_EXPORT_MISSOURI_BANK                   = 147;
	const PP_EXPORT_HAWTHORN_BANK                   = 148;
	const AP_INTEGRATED_PAYABLES                    = 149;
	const PP_EXPORT_AXOS_BANK                       = 150;
	const PP_EXPORT_AMERIS_BANK                     = 151;
	const PP_EXPORT_PEOPLES_UNITED_BANK             = 152;
	const PP_EXPORT_SANTANDER_BANK                  = 153;
	const PP_EXPORT_GREAT_SOUTHERN_BANK             = 154;

	public static $c_arrintTransmissionVendorIdsHavingSensitiveScreeningInfo = [ self::SOUND_SCREENING, self::AMRENT, self::FABCO, self::LEASINGDESK, self::SAFERENT, self::RESIDENT_CHECK, self::RENT_GROW, self::RESIDENT_VERIFY, self::RENTAL_HISTORY_REPORTS, self::TAZ_WORKS, self::TRANSUNION_CREDIT_RETRIEVER_CR6, self::NTN, self::LEASINGDESK_MITS, self::THE_SCREENING_PROS, self::REALID, self::GENERIC_SCREENING_LIBRARY, self::BCS	];

	public static $c_arrintCsvExtentionTransmissionVendors = [
		self::PP_EXPORT_OTHER_BANKS            => self::PP_EXPORT_OTHER_BANKS,
		self::PP_EXPORT_VECTRA_BANK            => self::PP_EXPORT_VECTRA_BANK,
		self::PP_EXPORT_FIRST_MERCHANT_BANK    => self::PP_EXPORT_FIRST_MERCHANT_BANK,
		self::PP_EXPORT_CADENCE_BANK           => self::PP_EXPORT_CADENCE_BANK,
		self::PP_EXPORT_FIRST_BANK             => self::PP_EXPORT_FIRST_BANK,
		self::PP_EXPORT_SIGNATURE_BANK         => self::PP_EXPORT_SIGNATURE_BANK,
		self::PP_EXPORT_TCF_COMMERCIAL_BANK    => self::PP_EXPORT_TCF_COMMERCIAL_BANK,
		self::PP_EXPORT_BRIDGEWATER_BANK       => self::PP_EXPORT_BRIDGEWATER_BANK,
		self::PP_EXPORT_CITIZENS_BUSINESS_BANK => self::PP_EXPORT_CITIZENS_BUSINESS_BANK,
		self::PP_EXPORT_REVERE_BANK            => self::PP_EXPORT_REVERE_BANK,
		self::PP_EXPORT_REGIONS_BANK           => self::PP_EXPORT_REGIONS_BANK,
		self::PP_EXPORT_AMEGY_BANK             => self::PP_EXPORT_AMEGY_BANK,
		self::PP_EXPORT_TOWNE_BANK             => self::PP_EXPORT_TOWNE_BANK,
		self::PP_EXPORT_AXOS_BANK              => self::PP_EXPORT_AXOS_BANK,
		self::PP_EXPORT_AMERIS_BANK            => self::PP_EXPORT_AMERIS_BANK,
		self::PP_EXPORT_SANTANDER_BANK         => self::PP_EXPORT_SANTANDER_BANK,
		self::PP_EXPORT_PEOPLES_UNITED_BANK    => self::PP_EXPORT_PEOPLES_UNITED_BANK,
		self::PP_EXPORT_GREAT_SOUTHERN_BANK    => self::PP_EXPORT_GREAT_SOUTHERN_BANK
	];

	public static $c_arrintApiSupportedExportTypes = [ self::GL_EXPORT_MRI, self::GL_EXPORT_YARDI, self::GL_EXPORT_YARDI_SIPP ];

	public static $c_arrintExortTypesWithoutExportFormatMethod = [ self::GL_EXPORT_ORACLE, self::GL_EXPORT_YARDI_ETL_MR, self::GL_EXPORT_YARDI_CA, self::GL_EXPORT_PEOPLESOFT_AR_JRNL, self::GL_EXPORT_YARDI_SIPP ];

	public static $c_arrintExportTypesWithPropertyPrefrences = [ self::GL_EXPORT_MRI, self::GL_EXPORT_YARDI, self::GL_EXPORT_ONESITE, self::GL_EXPORT_TIMBERLINE_PM	];

	public static $c_arrintAllGlExportTypes = [ self::GL_EXPORT_AMSI, self::GL_EXPORT_GREAT_PLAINS, self::GL_EXPORT_JD_EDWARDS, self::GL_EXPORT_JENARK, self::GL_EXPORT_MRI, self::GL_EXPORT_ONESITE, self::GL_EXPORT_ORACLE, self::GL_EXPORT_PEAK, self::GL_EXPORT_PEOPLESOFT, self::GL_EXPORT_QUICK_BOOKS, self::GL_EXPORT_SKYLINE, self::GL_EXPORT_TIMBERLINE_PM, self::GL_EXPORT_YARDI, self::GL_EXPORT_YARDI_CA, self::GL_EXPORT_YARDI_ETL, self::GL_EXPORT_YARDI_ETL_MR, self::GL_EXPORT_YARDI_GL_EXPORT_7S, self::GL_EXPORT_YARDI_GL_EXPORT_7S_MX, self::GL_EXPORT_PEOPLESOFT_AR_JRNL, self::GL_EXPORT_YARDI_ETL_GS, self::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL, self::GL_EXPORT_YARDI_SIPP ];

	public static $c_arrintYardiApExportFormatTypes = [ self::AP_EXPORT_YARDI, self::AP_EXPORT_YARDI_ETL_INVOICE_REGISTER, self::AP_EXPORT_YARDI_ETL_FINPAYABLES ];

	public static $c_arrintApExportTypes = [ self::AP_EXPORT_TIMBERLINE_PM, self::AP_EXPORT_YARDI, self::AP_EXPORT_YARDI_ETL_FINPAYABLES, self::AP_EXPORT_YARDI_ETL_INVOICE_REGISTER, self::AP_EXPORT_PEOPLESOFT_REFUNDS ];

	public static $c_arrintAllPositivePayTypes = [ self::PP_EXPORT_BANK_OF_AMERICA, self::PP_EXPORT_BBT_BANK, self::PP_EXPORT_BMO_HARRIS_BANK, self::PP_EXPORT_CADENCE_BANK, self::PP_EXPORT_CAPITAL_ONE, self::PP_EXPORT_CENTRAL_BANK_OF_BOONE_COUNTY, self::PP_EXPORT_CITI_BANK, self::PP_EXPORT_CITIZENS_BANK, self::PP_EXPORT_CITY_NATIONAL_BANK, self::PP_EXPORT_CITYWIDE_BANK, self::PP_EXPORT_COLORADO_STATE_BANK_AND_TRUST, self::PP_EXPORT_COM_DATA_BANK, self::PP_EXPORT_EAST_WEST_BANK, self::PP_EXPORT_FIFTH_THIRD_BANK, self::PP_EXPORT_FIRST_BANK, self::PP_EXPORT_FIRST_COMMONWEALTH, self::PP_EXPORT_FIRST_MERCHANT_BANK, self::PP_EXPORT_FIRST_TENNESSEE_BANK, self::PP_EXPORT_FROST_BANK, self::PP_EXPORT_HUNTINGTON_BANK, self::PP_EXPORT_HSBC_BANK, self::PP_EXPORT_IBERIA_BANK, self::PP_EXPORT_JP_MORGAN_CHASE, self::PP_EXPORT_KEY_BANK, self::PP_EXPORT_MORTON_COMMUNITY_BANK, self::PP_EXPORT_M_AND_T_BANK, self::PP_EXPORT_OTHER_BANKS, self::PP_EXPORT_PNC_BANK, self::PP_EXPORT_PRIVATE_BANK_CIBC, self::PP_EXPORT_SIGNATURE_BANK, self::PP_EXPORT_TCF_COMMERCIAL_BANK, self::PP_EXPORT_TD_BANK, self::PP_EXPORT_US_BANK, self::PP_EXPORT_VECTRA_BANK, self::PP_EXPORT_WASHINGTON_FIRST, self::PP_EXPORT_WASHINGTON_TRUST, self::PP_EXPORT_WELLS_FARGO, self::PP_EXPORT_BRIDGEWATER_BANK, self::PP_EXPORT_MIDFIRST_BANK, self::PP_EXPORT_CITIZENS_BUSINESS_BANK, self::PP_EXPORT_REVERE_BANK, self::PP_EXPORT_REGIONS_BANK, self::PP_EXPORT_COMERICA_BANK, self::PP_EXPORT_AMEGY_BANK, self::PP_EXPORT_CITY_DIRECT, self::PP_EXPORT_OAKSTAR_BANK, self::PP_EXPORT_MISSOURI_BANK, self::PP_EXPORT_HAWTHORN_BANK, self::PP_EXPORT_AXOS_BANK, self::PP_EXPORT_AMERIS_BANK, self::PP_EXPORT_SANTANDER_BANK, self::PP_EXPORT_PEOPLES_UNITED_BANK, self::PP_EXPORT_GREAT_SOUTHERN_BANK ];

	public static $c_arrintRefactoredGlExportTypes = [ self::GL_EXPORT_AMSI, self::GL_EXPORT_GREAT_PLAINS, self::GL_EXPORT_JD_EDWARDS, self::GL_EXPORT_JENARK, self::GL_EXPORT_MRI, self::GL_EXPORT_ONESITE, self::GL_EXPORT_YARDI_GL_EXPORT_7S, self::GL_EXPORT_YARDI_SIPP, self::GL_EXPORT_PEOPLESOFT_AR_JRNL ];

	public static $c_arrintDefaultUnZippedGlExportTypes = [
		self::GL_EXPORT_PEOPLESOFT_AR_JRNL               => self::GL_EXPORT_PEOPLESOFT_AR_JRNL,
		self::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL => self::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL,
		self::GL_EXPORT_YARDI_GL_EXPORT_7S_MX            => self::GL_EXPORT_YARDI_GL_EXPORT_7S_MX,
		self::GL_EXPORT_YARDI_ETL_MR                     => self::GL_EXPORT_YARDI_ETL_MR
	];

	public static $c_arrintAllowRVPermissionToNonRvVendorIds = [ self::BCS, ];

	public static $c_arrintAllowAddRiskPremiumRatesTransmissionVendorIds = [ self::RESIDENT_VERIFY, self::BCS ];

	/**
	 * @  Removed constant const TRANSUNION_CREDIT_RETRIEVER	= 9 as third party ( cr5 ) no longer support it
	 **/

	public static function assignSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'TRANSMISSION_VENDOR_BLUE_MOON', self::BLUE_MOON );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_LEVEL_1', self::LEVEL_1 );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_LEAD_TO_LEASE', self::LEAD_TO_LEASE );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_LEAD_TRACKING_SOLUTIONS', self::LEAD_TRACKING_SOLUTIONS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_BLUE_MOON_LEASE', self::BLUE_MOON_LEASE );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_LRO', self::LRO );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_YIELDSTAR', self::YIELDSTAR );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_SAFERENT', self::SAFERENT );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_ONSITE', self::ONSITE );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_FIRST_ADVANTAGE', self::FIRST_ADVANTAGE );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_RESIDENT_CHECK', self::RESIDENT_CHECK );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_RENT_GROW', self::RENT_GROW );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_SCREENING_REPORTS', self::SCREENING_REPORTS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_LEASINGDESK', self::LEASINGDESK );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_VERIFIRST', self::VERIFIRST );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_RENTAL_HISTORY_REPORTS', self::RENTAL_HISTORY_REPORTS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_APARTMENTS', self::APARTMENTS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_VELOCITY', self::VELOCITY );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_BACKGROUND_INVESTIGATIONS', self::BACKGROUND_INVESTIGATIONS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_RESIDENT_VERIFY', self::RESIDENT_VERIFY );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_SOUND_SCREENING', self::SOUND_SCREENING );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_FABCO', self::FABCO );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_AMRENT', self::AMRENT );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_VAULTWARE', self::VAULTWARE );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_PEOPLESOFT', self::PEOPLESOFT );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_NTN', self::NTN );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_TRANSUNION_CREDIT_RETRIEVER_CR6', self::TRANSUNION_CREDIT_RETRIEVER_CR6 );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_TAZ_WORKS', self::TAZ_WORKS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_YARDI', self::YARDI );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_MRI', self::MRI );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_BLUE_MOON_DOC_STORAGE', self::BLUE_MOON_DOC_STORAGE );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_CHATMETER', self::CHATMETER );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_YEXT', self::YEXT );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_APARTMENTRATINGS', self::APARTMENTRATINGS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_LEASINGDESK_MITS', self::LEASINGDESK_MITS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_THE_SCREENING_PROS', self::THE_SCREENING_PROS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_CONSERVICE', self::CONSERVICE );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_YIELDSTAR_EXPORT', self::YIELDSTAR_EXPORT );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_COMPLIANCE_DEPOT', self::COMPLIANCE_DEPOT );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_UBSEXPORT', self::UBSEXPORT );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_CUSTOM', self::CUSTOM );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_TENANT_TECH', self::TENANT_TECH );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_REALID', self::REALID );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_GENERIC_SCREENING_LIBRARY', self::GENERIC_SCREENING_LIBRARY );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_ACH_PAYMENTS', self::AP_ACH_PAYMENTS );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_WELLS_FARGO_PAYMENT_MANAGER', self::WELLS_FARGO_PAYMENT_MANAGER );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_GL_YARDI_SIPP', self::GL_EXPORT_YARDI_SIPP );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_SITEMINDER', self::SITEMINDER );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_AP_YARDI_SIPP', self::AP_EXPORT_YARDI_SIPP );
		$objSmarty->assign( 'TRANSMISSION_VENDOR_CAPITAL_ONE', self::AP_INTEGRATED_PAYABLES );
    }

	public static function getCompanyTransmissionVendorCode( $intTransmissionVendorId ) {

		$strCompanyTransmissionVendorCode = NULL;

		switch( $intTransmissionVendorId ) {

			case self::BLUE_MOON:
				$strCompanyTransmissionVendorCode = 'BM';
				break;

			case self::LEVEL_1:
				$strCompanyTransmissionVendorCode = 'L1';
				break;

			case self::LEAD_TO_LEASE:
				$strCompanyTransmissionVendorCode = 'L2L';
				break;

			case self::LEAD_TRACKING_SOLUTIONS:
				$strCompanyTransmissionVendorCode = 'LTS';
				break;

			case self::BLUE_MOON_LEASE:
				$strCompanyTransmissionVendorCode = 'BML';
				break;

			case self::LRO:
				$strCompanyTransmissionVendorCode = 'LRO';
				break;

			case self::YIELDSTAR:
				$strCompanyTransmissionVendorCode = 'YS';
				break;

			case self::SAFERENT:
				$strCompanyTransmissionVendorCode = 'SR';
				break;

			case self::ONSITE:
				$strCompanyTransmissionVendorCode = 'OS';
				break;

			case self::FIRST_ADVANTAGE:
				$strCompanyTransmissionVendorCode = 'LN';
				break;

			case self::RESIDENT_CHECK:
				$strCompanyTransmissionVendorCode = 'RC';
				break;

			case self::RENT_GROW:
				$strCompanyTransmissionVendorCode = 'RG';
				break;

			case self::SCREENING_REPORTS:
				$strCompanyTransmissionVendorCode = 'SCR';
				break;

			case self::LEASINGDESK:
				$strCompanyTransmissionVendorCode = 'LD';
				break;

			case self::VERIFIRST:
				$strCompanyTransmissionVendorCode = 'VF';
				break;

			case self::RENTAL_HISTORY_REPORTS:
				$strCompanyTransmissionVendorCode = 'RHR';
				break;

			case self::APARTMENTS:
				$strCompanyTransmissionVendorCode = 'AP';
				break;

			case self::VELOCITY:
				$strCompanyTransmissionVendorCode = 'VC';
				break;

			case self::BACKGROUND_INVESTIGATIONS:
				$strCompanyTransmissionVendorCode = 'BI';
				break;

			case self::RESIDENT_VERIFY:
				$strCompanyTransmissionVendorCode = 'RV';
				break;

			case self::SOUND_SCREENING:
				$strCompanyTransmissionVendorCode = 'SS';
				break;

			case self::FABCO:
				$strCompanyTransmissionVendorCode = 'FAB';
				break;

			case self::AMRENT:
				$strCompanyTransmissionVendorCode = 'AMR';
				break;

			case self::PEOPLESOFT:
				$strCompanyTransmissionVendorCode = 'PS';
				break;

			case self::NTN:
				$strCompanyTransmissionVendorCode = 'NTN';
				break;

			case self::TRANSUNION_CREDIT_RETRIEVER_CR6:
				$strCompanyTransmissionVendorCode = 'TUCR6';
				break;

			case self::TAZ_WORKS:
				$strCompanyTransmissionVendorCode = 'TWS';
				break;

			case self::LEASINGDESK_MITS:
				$strCompanyTransmissionVendorCode = 'LM';
				break;

			case self::THE_SCREENING_PROS:
				$strCompanyTransmissionVendorCode = 'TSP';
				break;

			case self::REALID:
				$strCompanyTransmissionVendorCode = 'FAF';
				break;

			case self::GENERIC_SCREENING_LIBRARY:
				$strCompanyTransmissionVendorCode = 'GSL';
				break;

			default:
				// added default
				break;
		}

		return $strCompanyTransmissionVendorCode;
	}

	public static function getTransmissionVendorNameById( $intTransmissionVendorId ) {

		$strTransmissionVendor = NULL;
		switch( $intTransmissionVendorId ) {
			case self::GL_EXPORT_QUICK_BOOKS:
				$strTransmissionVendor = __( 'Quick Books' );
				break;

			case self::GL_EXPORT_SKYLINE:
				$strTransmissionVendor = __( 'Skyline' );
				break;

			case self::GL_EXPORT_TIMBERLINE_PM:
			case self::AP_EXPORT_TIMBERLINE_PM:
				$strTransmissionVendor = __( 'Timberline PM' );
				break;

			case self::GL_EXPORT_PEOPLESOFT:
				$strTransmissionVendor = __( 'Peoplesoft' );
				break;

			case self::GL_EXPORT_MRI:
				$strTransmissionVendor = __( 'MRI' );
				break;

			case self::GL_EXPORT_JENARK:
				$strTransmissionVendor = __( 'Jenark' );
				break;

			case self::GL_EXPORT_YARDI:
			case self::AP_EXPORT_YARDI:
				$strTransmissionVendor = __( 'Yardi' );
				break;

			case self::GL_EXPORT_ONESITE:
				$strTransmissionVendor = __( 'OneSite' );
				break;

			case self::GL_EXPORT_AMSI:
				$strTransmissionVendor = __( 'Amsi' );
				break;

			case self::GL_EXPORT_PEAK:
				$strTransmissionVendor = __( 'Peak' );
				break;

			case self::GL_EXPORT_ORACLE:
				$strTransmissionVendor = __( 'Oracle' );
				break;

			case self::GL_EXPORT_GREAT_PLAINS:
				$strTransmissionVendor = __( 'Great Plains' );
				break;

			case self::GL_EXPORT_YARDI_ETL:
				$strTransmissionVendor = __( 'Yardi ETL' );
				break;

			case self::GL_EXPORT_YARDI_GL_EXPORT_7S:
				$strTransmissionVendor = __( 'Yardi GL Export 7S' );
				break;

			case self::GL_EXPORT_YARDI_GL_EXPORT_7S_MX:
				$strTransmissionVendor = __( 'Yardi GL Export 7S MX' );
				break;

			case self::AP_EXPORT_YARDI_ETL_FINPAYABLES:
				$strTransmissionVendor = __( 'Yardi ETL FinPayables' );
				break;

			case self::AP_EXPORT_YARDI_ETL_INVOICE_REGISTER:
				$strTransmissionVendor = __( 'Yardi ETL Invoice Register' );
				break;

			case self::PP_EXPORT_CAPITAL_ONE:
				$strTransmissionVendor = __( 'Capital One' );
				break;

			case self::PP_EXPORT_KEY_BANK:
				$strTransmissionVendor = __( 'Key Bank' );
				break;

			case self::PP_EXPORT_BBT_BANK:
				$strTransmissionVendor = __( 'BB & T' );
				break;

			case self::PP_EXPORT_JP_MORGAN_CHASE:
				$strTransmissionVendor = __( 'JP Morgan Chase' );
				break;

			case self::PP_EXPORT_WELLS_FARGO:
				$strTransmissionVendor = __( 'Wells Fargo' );
				break;

			case self::PP_EXPORT_WASHINGTON_FIRST:
				$strTransmissionVendor = __( 'Washington First' );
				break;

			case self::PP_EXPORT_EAST_WEST_BANK:
				$strTransmissionVendor = __( 'East West Bank' );
				break;

			case self::PP_EXPORT_FROST_BANK:
				$strTransmissionVendor = __( 'Frost Bank' );
				break;

			case self::PP_EXPORT_COM_DATA_BANK:
				$strTransmissionVendor = __( 'ComData Bank' );
				break;

			case self::PP_EXPORT_TD_BANK:
				$strTransmissionVendor = __( 'TD Bank' );
				break;

			case self::PP_EXPORT_BANK_OF_AMERICA:
				$strTransmissionVendor = __( 'Bank of America' );
				break;

			case self::PP_EXPORT_WASHINGTON_TRUST:
				$strTransmissionVendor = __( 'Washington Trust' );
				break;

			case self::PP_EXPORT_US_BANK:
				$strTransmissionVendor = __( 'US Bank' );
				break;

			case self::PP_EXPORT_PNC_BANK:
				$strTransmissionVendor = __( 'PNC Bank' );
				break;

			case self::PP_EXPORT_BMO_HARRIS_BANK:
				$strTransmissionVendor = __( 'BMO Harris Bank' );
				break;

			case self::PP_EXPORT_OTHER_BANKS:
				$strTransmissionVendor = __( 'Other Banks' );
				break;

			case self::PP_EXPORT_CITIZENS_BANK:
				$strTransmissionVendor = __( 'Citizens Bank' );
				break;

			case self::PP_EXPORT_COLORADO_STATE_BANK_AND_TRUST:
				$strTransmissionVendor = __( 'Colorado State Bank & Trust' );
				break;

			case self::PP_EXPORT_PRIVATE_BANK_CIBC:
				$strTransmissionVendor = __( 'Private Bank (CIBC)' );
				break;

			case self::PP_EXPORT_CITY_NATIONAL_BANK:
				$strTransmissionVendor = __( 'City National Bank' );
				break;

			case self::PP_EXPORT_VECTRA_BANK:
				$strTransmissionVendor = __( 'Vectra Bank' );
				break;

			case self::PP_EXPORT_CITI_BANK:
				$strTransmissionVendor = __( 'CitiBank' );
				break;

			case self::PP_EXPORT_CENTRAL_BANK_OF_BOONE_COUNTY:
				$strTransmissionVendor = __( 'Central Bank of Boone County' );
				break;

			case self::PP_EXPORT_FIRST_MERCHANT_BANK:
				$strTransmissionVendor = __( 'First Merchants Bank' );
				break;

			case self::PP_EXPORT_CADENCE_BANK:
				$strTransmissionVendor = __( 'Cadence Bank' );
				break;

			case self::PP_EXPORT_MORTON_COMMUNITY_BANK:
				$strTransmissionVendor = __( 'Morton Community Bank' );
				break;

			case self::PP_EXPORT_FIRST_BANK:
				$strTransmissionVendor = __( 'First Bank' );
				break;

			case self::PP_EXPORT_CITYWIDE_BANK:
				$strTransmissionVendor = __( 'Citywide Bank' );
				break;

			case self::PP_EXPORT_IBERIA_BANK:
				$strTransmissionVendor = __( 'Iberia Bank' );
				break;

			case self::PP_EXPORT_HUNTINGTON_BANK:
				$strTransmissionVendor = __( 'Huntington Bank' );
				break;

			case self::PP_EXPORT_FIFTH_THIRD_BANK:
				$strTransmissionVendor = __( 'Fifth Third Bank' );
				break;

			case self::PP_EXPORT_FIRST_COMMONWEALTH:
				$strTransmissionVendor = __( 'First Common wealth' );
				break;

			case self::PP_EXPORT_SIGNATURE_BANK:
				$strTransmissionVendor = __( 'Signature Bank' );
				break;

			case self::PP_EXPORT_FIRST_TENNESSEE_BANK:
				$strTransmissionVendor = __( 'First Tennessee Bank' );
				break;

			case self::PP_EXPORT_TCF_COMMERCIAL_BANK:
				$strTransmissionVendor = __( 'TCF Commercial Bank' );
				break;

			case self::PP_EXPORT_HSBC_BANK:
				$strTransmissionVendor = __( 'HSBC Bank' );
				break;

			case self::PP_EXPORT_M_AND_T_BANK:
				$strTransmissionVendor = __( 'M & T Bank' );
				break;

			case self::GL_EXPORT_JD_EDWARDS:
				$strTransmissionVendor = __( 'JD Edwards' );
				break;

			case self::GL_EXPORT_YARDI_ETL_MR:
				$strTransmissionVendor = __( 'Yardi ETL MR' );
				break;

			case self::GL_EXPORT_YARDI_CA:
				$strTransmissionVendor = __( 'Yardi CA' );
				break;

			case self::PP_EXPORT_BRIDGEWATER_BANK:
				$strTransmissionVendor = __( 'Bridgewater Bank' );
				break;

			case self::PP_EXPORT_MIDFIRST_BANK:
				$strTransmissionVendor = __( 'MidFirst Bank' );
				break;

			case self::PP_EXPORT_POSPAYEXTRACTCUSTOM:
				$strTransmissionVendor = __( 'PosPayExtractCustom' );
				break;

			case self::GL_EXPORT_PEOPLESOFT_AR_JRNL:
				$strTransmissionVendor = __( 'Peoplesoft AR JRNL' );
				break;

			case self::AP_EXPORT_PEOPLESOFT_REFUNDS:
				$strTransmissionVendor = __( 'PeopleSoft Refunds' );
				break;

			case self::PP_EXPORT_CITIZENS_BUSINESS_BANK:
				$strTransmissionVendor = __( 'Citizens Business Bank' );
				break;

			case self::PP_EXPORT_REVERE_BANK:
				$strTransmissionVendor = __( 'Revere Bank' );
				break;

			case self::GL_EXPORT_YARDI_ETL_GS:
				$strTransmissionVendor = __( 'Yardi ETL GS' );
				break;

			case self::GL_EXPORT_YARDI_GL_EXPORT_7S_INTERNATIONAL:
				$strTransmissionVendor = __( 'Yardi GL Export 7s International' );
				break;

			case self::PP_EXPORT_COMERICA_BANK:
				$strTransmissionVendor = __( 'Comerica Bank' );
				break;

			case self::PP_EXPORT_AMEGY_BANK:
				$strTransmissionVendor = __( 'Amegy Bank' );
				break;

			case self::GL_EXPORT_YARDI_SIPP:
			case self::AP_EXPORT_YARDI_SIPP:
				$strTransmissionVendor = __( 'Yardi Sipp' );
				break;

			case self::PP_EXPORT_CITY_DIRECT:
				$strTransmissionVendor = __( 'Citi Direct' );
				break;

			case self::SITEMINDER:
				$strTransmissionVendor = __( 'SiteMinder' );
				break;

			case self::PP_EXPORT_TOWNE_BANK:
				$strTransmissionVendor = __( 'TowneBank' );
				break;

			case self::PP_EXPORT_OAKSTAR_BANK:
				$strTransmissionVendor = __( 'OakStar Bank' );
				break;

			case self::PP_EXPORT_MISSOURI_BANK:
				$strTransmissionVendor = __( 'The Bank of Missouri' );
				break;

			case self::PP_EXPORT_HAWTHORN_BANK:
				$strTransmissionVendor = __( 'Hawthorn Bank' );
				break;

			case self::PP_EXPORT_AXOS_BANK:
				$strTransmissionVendor = __( 'Axos Bank' );
				break;

			case self::PP_EXPORT_AMERIS_BANK:
				$strTransmissionVendor = __( 'Ameris Bank' );
				break;

			case self::PP_EXPORT_SANTANDER_BANK:
				$strTransmissionVendor = __( 'Santander Bank' );
				break;

			case self::PP_EXPORT_PEOPLES_UNITED_BANK:
				$strTransmissionVendor = __( 'People\'s United Bank' );
				break;

			case self::PP_EXPORT_GREAT_SOUTHERN_BANK:
				$strTransmissionVendor = __( 'Great Southern Bank' );
				break;

			default:
				/** * do nothing*/
				break;
		}
		return $strTransmissionVendor;
	}

	public static $c_arrintTransmissionVendorsFileFormatTypes = [
		self::PP_EXPORT_JP_MORGAN_CHASE    => [
			CAccountingExportFileFormatType::JP_MORGAN_CHASE     => CAccountingExportFileFormatType::JP_MORGAN_CHASE,
			CAccountingExportFileFormatType::JP_MORGAN_CHASE_ARP => CAccountingExportFileFormatType::JP_MORGAN_CHASE_ARP,
			CAccountingExportFileFormatType::JP_MORGAN_CHASE_CP  => CAccountingExportFileFormatType::JP_MORGAN_CHASE_CP,
			CAccountingExportFileFormatType::JP_MORGAN_CHASE_PCL => CAccountingExportFileFormatType::JP_MORGAN_CHASE_PCL
		],
		self::PP_EXPORT_WELLS_FARGO        => [
			CAccountingExportFileFormatType::LEGACY_WF_ARP_INBOUND => CAccountingExportFileFormatType::LEGACY_WF_ARP_INBOUND,
			CAccountingExportFileFormatType::STANDARD_ARP_INBOUND  => CAccountingExportFileFormatType::STANDARD_ARP_INBOUND,
			CAccountingExportFileFormatType::WELLS_FARGO_ASCII     => CAccountingExportFileFormatType::WELLS_FARGO_ASCII
		],
		self::PP_EXPORT_FROST_BANK         => [
			CAccountingExportFileFormatType::FROST_BANK      => CAccountingExportFileFormatType::FROST_BANK,
			CAccountingExportFileFormatType::FROST_BANK_ANSI => CAccountingExportFileFormatType::FROST_BANK_ANSI
		],
		self::PP_EXPORT_BANK_OF_AMERICA    => [
			CAccountingExportFileFormatType::BANK_OF_AMERICA_SB       => CAccountingExportFileFormatType::BANK_OF_AMERICA_SB,
			CAccountingExportFileFormatType::BANK_OF_AMERICA_NATIONAL => CAccountingExportFileFormatType::BANK_OF_AMERICA_NATIONAL,
			CAccountingExportFileFormatType::BANK_OF_AMERICA_IDS      => CAccountingExportFileFormatType::BANK_OF_AMERICA_IDS
		],
		self::PP_EXPORT_US_BANK            => [
			CAccountingExportFileFormatType::US_BANK_CSV => CAccountingExportFileFormatType::US_BANK_CSV,
			CAccountingExportFileFormatType::US_BANK_TXT => CAccountingExportFileFormatType::US_BANK_TXT
		],
		self::PP_EXPORT_CITY_NATIONAL_BANK => [
			CAccountingExportFileFormatType::CNB_CNZ => CAccountingExportFileFormatType::CNB_CNZ,
			CAccountingExportFileFormatType::CNB_TXT => CAccountingExportFileFormatType::CNB_TXT
		],
		self::PP_EXPORT_CITI_BANK          => [
			CAccountingExportFileFormatType::CITI_BANK_CSV => CAccountingExportFileFormatType::CITI_BANK_CSV,
			CAccountingExportFileFormatType::CITI_BANK_TXT => CAccountingExportFileFormatType::CITI_BANK_TXT
		],
		self::PP_EXPORT_IBERIA_BANK        => [
			CAccountingExportFileFormatType::IBERIA_AUTOMATED => CAccountingExportFileFormatType::IBERIA_AUTOMATED,
			CAccountingExportFileFormatType::IBERIA_MANUAL    => CAccountingExportFileFormatType::IBERIA_MANUAL
		],
		self::CARVAJAL                     => [
			CAccountingExportFileFormatType::CHARGE_EXPORT  => CAccountingExportFileFormatType::CHARGE_EXPORT,
			CAccountingExportFileFormatType::CREDIT_EXPORT  => CAccountingExportFileFormatType::CREDIT_EXPORT,
			CAccountingExportFileFormatType::PAYMENT_EXPORT => CAccountingExportFileFormatType::PAYMENT_EXPORT
		],
		self::PP_EXPORT_HSBC_BANK       => [
			CAccountingExportFileFormatType::HSBC_CANADA  => CAccountingExportFileFormatType::HSBC_CANADA,
			CAccountingExportFileFormatType::HSBC_US  => CAccountingExportFileFormatType::HSBC_US
		],
		self::PP_EXPORT_MIDFIRST_BANK => [
			CAccountingExportFileFormatType::MIDFIRST_BANK_TXT  => CAccountingExportFileFormatType::MIDFIRST_BANK_TXT,
			CAccountingExportFileFormatType::MIDFIRST_BANK_CSV  => CAccountingExportFileFormatType::MIDFIRST_BANK_CSV
		],
		self::PP_EXPORT_CAPITAL_ONE => [
			CAccountingExportFileFormatType::CAPITAL_ONE_TXT  => CAccountingExportFileFormatType::CAPITAL_ONE_TXT,
			CAccountingExportFileFormatType::CAPITAL_ONE_CSV  => CAccountingExportFileFormatType::CAPITAL_ONE_CSV
		]
	];

}
?>