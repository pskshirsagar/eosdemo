<?php

class CCompanyUserPropertyGroup extends CBaseCompanyUserPropertyGroup {

	/**
	 * Create Functions Get Functions
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCompanyUserPropertyGroupsFields() {
		return [
			'company_user_property_groups' => __( 'Property/Property Groups' )
		];
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>