<?php

use Psi\Libraries\Container\TContainerized;
use Psi\Libraries\I18n\PersonName\CPersonNameService;
use Psi\Libraries\I18n\PersonName\Enum\CPersonNameFormat;
use Psi\Eos\Entrata\CScheduledEmailAddressFilters;
use Psi\Eos\Entrata\CWebsites;
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CPropertyEmailRules;
use Psi\Eos\Entrata\CSmsEnrollments;
use Psi\Libraries\HtmlMimeMail\CHtmlMimeMail;
use Psi\Libraries\UtilHash\CHash;

class CCustomer extends CBaseCustomer {

	use TContainerized;

	// thid id a user defined constant and has no field in the database table
	const LEASE_STATUS_TYPE_ALL			= -1;
	const LEASE_STATUS_TYPE_CURRENT		= -2;
	const LEASE_STATUS_TYPE_RESIDENT	= 8;
	const REMOTE_FOREIGN_KEY_DELIMITER 	= '~..~';

	const TOTAL_LOGIN_ATTEMPTS 			= 10;
	const LOGIN_ATTEMPT_WAITING_TIME 	= 900;

	const TOTAL_NUMBER_OF_VEHICLES_ALLOWED = 4;
	const TOTAL_NUMBER_OF_VEHICLES_ALLOWED_FOR_RP_SKIN = 6;

	const MINIMUM_OCCUPANT_AGE_ALLOWED_TO_BE_RESPONSIBLE_ON_LEASE				= 18;
	const MINIMUM_OCCUPANT_AGE_REQUIRED_TO_OVERRIDE_MINIMUM_OCCUPANT_AGE_LIMIT	= 14;

	const TOTAL_NUMBER_OF_RESIDENTS_ALLOWED_FOR_BULK_FMO				= 500;
	const TOTAL_NUMBER_OF_RESIDENTS_ALLOWED_FOR_BULK_PLACE_ON_NOTICE	= 500;
	const TOTAL_NUMBER_OF_RESIDENTS_ALLOWED_FOR_BULK_MOVE_IN	= 500;
	const TOTAL_NUMBER_OF_RESIDENTS_ALLOWED_FOR_BULK_MOVE_OUT	= 500;
	const TOTAL_LEASES_PER_MESSAGE_FOR_BULK_MOVE_IN	= 5;

	const PROPERTY_PREF_BAL_PAY_REQ_CHARGES_MUST_BE_PAID_IN_FULL = 1;
	const PROPERTY_PREF_BAL_PAY_REQ_RENT_BAL_MUST_BE_PAID_IN_FULL = 2;
	const PROPERTY_PREF_BAL_PAY_REQ_CURR_BAL_MUST_BE_PAID_IN_FULL = 3;

	const CUSTOMER_FILTER_TYPE_RESIDENT        = 1;
	const CUSTOMER_FILTER_TYPE_GROUP_RESIDENT  = 2;
	const CUSTOMER_FILTER_TYPE_GROUP           = 3;

	const TABLE_ORIGINAL_CUSTOMERS	= 'customers';
	const VIEW_CUSTOMERS			= 'view_customers';

	protected $m_arrobjIntegrationCientTypes;
	protected $m_arrobjInsurancePolicies;
	protected $m_arrobjCustomerPaymentAccounts;

	protected $m_objProperty;
	protected $m_objUtilityAccount;
	protected $m_arrobjApplicantApplications;
	protected $m_arrobjApplications;

	protected $m_arrmixCustomerPhoneNumbers;

	protected $m_objDocumentManager;
	protected $m_objObjectStorageGateway;

	protected $m_boolIsLoggedIn;
	protected $m_boolTermsAndConditions;
	protected $m_boolIsCreateNewLease;
	protected $m_boolIsBlockMessageCenterEmail = false;
	protected $m_boolIsBlockRentReminderEmail = false;
	protected $m_boolIsBlockParcelAlertEmail = false;
	protected $m_boolIsBlockMaintenanceUpdateEmail = false;
	protected $m_boolIsBlockResidentCommunicationEmail = false;
	protected $m_boolIsBlockPropertyMarketingEmail = false;
	protected $m_boolIsBlockRentReminderSms;
	protected $m_boolIsBlockMessageCenterSms;
	protected $m_boolIsBlockMaintenanceNotificationSms;
	protected $m_boolIsBlockPackageNotificationSms;
	protected $m_boolIsBlockContactPointsSms;
	protected $m_boolIsBlockPropertyMarketingSms;
	protected $m_boolIsOptOutOfApartmates;
	protected $m_boolIsOverrideAndAllowUnderAgeOccupant;
	protected $m_boolIsBlockInspectionEmail = false;
	protected $m_boolIsBlockContactPointsEmail = false;
	protected $m_boolIsCreateNewCustomer;
	protected $m_boolIsResident;
	protected $m_boolIsCustomerContact;
	protected $m_boolIsCustomerDeduped = false;
	protected $m_boolIsOnlyAdditionalPhoneNumber = false;

	protected $m_strPropertyName;
	protected $m_strBuildingNumber;
	protected $m_strUnitNumber;
	protected $m_strSpaceNumber;
	protected $m_strPasswordAnswer;
	protected $m_strBuildingName;
	protected $m_arrstrStreetLine1;
	protected $m_strLeaseStatusTypeName;
	protected $m_strCustomerEventCreatedOn;
	protected $m_strLastPaidOn;
	protected $m_strSecondPassword;
	protected $m_strPrimaryPhoneNumber;

	protected $m_fltMiniminumPaymentAmount;
	protected $m_fltDelinquentAmount;

	protected $m_intLeaseId;
	protected $m_intLeaseCustomerId;
	protected $m_intCustomerTypeId;
	protected $m_strCustomerTypeName;
	protected $m_intCustomerRelationshipId;
	protected $m_intDefaultCustomerRelationshipId;
	protected $m_strCustomerRelationshipName;
	protected $m_strMaritalStatusTypeName;
	protected $m_intPropertyId;
	protected $m_intCustomerIdOnCurrentLease;
	protected $m_intPropertyBuildingId;
	protected $m_intAuthenticationLogId;
	protected $m_intInvoiceDeliveryTypeId;
	protected $m_intPropertyUnitId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intApplicantId;
	protected $m_intAchCustomerPaymentAccountCount;
	protected $m_intPadCustomerPaymentAccountCount;
	protected $m_intCcCustomerPaymentAccountCount;
	protected $m_intLeaseIntervalId;
	protected $m_intIsDisabled;
	protected $m_intCustomerContactId;
	public $m_intOrganizationId;
	public $m_intConnectedCustomerId;
	protected $m_intOrganizationContractId;

	protected $m_arrfltChargeDeposits;
	protected $m_arrintResponsibleLedgerFilterIds;

	protected $m_intInsurancePolicyLeaseId;
	protected $m_intDuplicateMessage;
	protected $m_intCustomerEventId;
	protected $m_intAllowAccessToUnit;
	protected $m_intParentLeadSourceId;

	protected $m_objEmailDatabase;
	protected $m_objI18nPrimaryPhoneNumber;
	protected $m_objI18nSecondaryPhoneNumber;
	protected $m_objI18nMobileNumber;

	protected $m_arrstrMergeFields;
    protected $m_arrstrCustomerNameFields;

	protected $m_strMobileConfirmationCode;

	protected $m_strOriginalUsername;
	protected $m_strOriginalPasswordEncrypted;
	protected $m_intOriginalLoginAttemptCount;
	protected $m_strOriginalLastLoginAttemptOn;
	protected $m_intOriginalIsDisabled;

	protected $m_strForwardingStreetLine1;
	protected $m_strForwardingStreetLine2;
	protected $m_strForwardingStreetLine3;
	protected $m_strForwardingCity;
	protected $m_strForwardingStateCode;
	protected $m_strForwardingPostalCode;
	protected $m_strForwardingCountryCode;
	protected $m_strCurrentStreetLine1;
	protected $m_strCurrentStreetLine2;
	protected $m_strCurrentStreetLine3;
	protected $m_strCurrentCity;
	protected $m_strCurrentStateCode;
	protected $m_strCurrentPostalCode;
	protected $m_strCurrentCountryCode;
    protected $m_strNameFull;
    protected $m_strTaxIdentificationTypeName;
    protected $m_strTaxIdTypeName;
    protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
    protected $m_arrmixExtraParameters;

    protected $m_intOccupancyTypeId;

	protected $m_arrobjACHCustomerPaymentAccounts;

	public static $_REQUEST_FORM_CUSTOMER_SETTINGS = [
		'is_block_message_center_email'				=> 0,
		'is_block_message_center_sms'				=> 0,
		'is_block_rent_reminder_email'				=> 0,
		'is_block_rent_reminder_sms'				=> 0,
		'is_block_chekbox'							=> 0,
		'is_block_maintenance_notification_sms'		=> 0,
		'is_block_parcel_alert_email'				=> 0,
		'is_block_package_notification_sms' 		=> 0,
		'is_block_contact_points_sms' 				=> 0,
		'is_block_property_marketing_sms'           => 0,
		'is_block_inspection_email' 				=> 0,
		'is_block_contact_points_email' 			=> 0,
		'is_block_maintenance_update_email' 		=> 0,
		'is_block_resident_communication_email'     => 0,
		'is_block_property_marketing_email'         => 0
	];

	public static $_REQUEST_FORM_CUSTOMER_EMAIL_SETTINGS = [
		'is_block_message_center_email'				=> 0,
		'is_block_rent_reminder_email'				=> 0,
		'is_block_chekbox'							=> 0,
		'is_block_parcel_alert_email'				=> 0,
		'is_block_inspection_email' 				=> 0,
		'is_block_contact_points_email' 			=> 0,
		'is_block_maintenance_update_email' 		=> 0,
		'is_block_resident_communication_email'     => 0,
		'is_block_property_marketing_email'         => 0
	];

	public static $_REQUEST_FORM_CUSTOMER_SMS_SETTINGS = [
		'is_block_message_center_sms'				=> 0,
		'is_block_rent_reminder_sms'				=> 0,
		'is_block_chekbox'							=> 0,
		'is_block_maintenance_notification_sms'		=> 0,
		'is_block_package_notification_sms' 		=> 0,
		'is_block_contact_points_sms' 				=> 0,
		'is_block_property_marketing_sms'           => 0
	];

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLoggedIn 			= false;
		$this->m_boolIsCreateNewCustomer	= false;
		$this->m_intLeaseId 				= false;

		$this->m_intDontAcceptPayments = '0';
		$this->m_intBlockPosting = '0';
		$this->m_intIsNetworkingEnrolled = '0';
		$this->m_intBlockNetworking = '0';
		$this->m_intBlockOnlinePayments = '0';
		$this->m_intAchCustomerPaymentAccountCount = 0;
		$this->m_intPadCustomerPaymentAccountCount = 0;
		$this->m_intCcCustomerPaymentAccountCount = 0;
		$this->m_intHasNameLastMatronymic       = 1;

		$this->m_arrstrCustomerNameFields = [ 'NameFirst', 'NameLast', 'NameLastMatronymic', 'CompanyName', 'AltNameFirst', 'AltNameMiddle', 'AltNameLast', 'PreferredName', 'NamePrefix', 'NameSuffix' ];
		return;
	}

	/**
	 * Get or Fetch Functions
	 */

	public function getOrFetchCustomerPaymentAccounts( $objDatabase ) {
		if( true == valArr( $this->m_arrobjCustomerPaymentAccounts ) ) return $this->m_arrobjCustomerPaymentAccounts;
		return $this->fetchCustomerPaymentAccounts( $objDatabase );
	}

	/**
	 * Get Functions
	 */

	public function getParentLeadSourceId() {
		return $this->m_intParentLeadSourceId;
	}

	public function getMobileConfirmationCode() {
		return $this->m_strMobileConfirmationCode;
	}

	public function getCustomerPaymentAccounts() {
		return $this->m_arrobjCustomerPaymentAccounts;
	}

	public function getAuthenticationLogId() {
		return $this->m_intAuthenticationLogId;
	}

	public function getTaxNumber( $boolReformatTaxNumber = true ) {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}
		$strDecryptTaxNumber = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
		if( true == $boolReformatTaxNumber ) {
			return preg_replace( '/[^a-z0-9]/i', '', $strDecryptTaxNumber );
		} else {
			return $strDecryptTaxNumber;
		}
	}

	public function getDlNumber() {
		if( false == valStr( $this->m_strDlNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strDlNumberEncrypted, CONFIG_SODIUM_KEY_DL_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_DL_NUMBER ] );
	}

	public function getIdentificationNumber( $boolIsMaskedIdentificationNumber = false ) {
		if( true == $boolIsMaskedIdentificationNumber ) {
			return CEncryption::maskText( $this->getIdentificationNumber(), -2 );
		}
		if( false == valStr( $this->m_strIdentificationValue ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strIdentificationValue, CONFIG_SODIUM_KEY_DL_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_DL_NUMBER ] );
	}

	public function getBirthDate( $boolIsMaskedBirthDate = false ) {
		if( true == valStr( $this->m_strBirthDate ) && true == $boolIsMaskedBirthDate ) {
			return '**/**/****';
		}
		return $this->m_strBirthDate;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPassword() {
		return $this->m_strPasswordEncrypted;
	}

	public function getPasswordQuestion() {
		return $this->m_strPasswordQuestion;
	}

	public function getPasswordAnswer() {
		return $this->m_strPasswordAnswer;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getAchCustomerPaymentAccountCount() {
		return $this->m_intAchCustomerPaymentAccountCount;
	}

	public function getPadCustomerPaymentAccountCount() {
		return $this->m_intPadCustomerPaymentAccountCount;
	}

	public function getCcCustomerPaymentAccountCount() {
		return $this->m_intCcCustomerPaymentAccountCount;
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function getChargeDeposits() {
		return $this->m_arrfltChargeDeposits;
	}

	public function getBuildingNumber() {
		return $this->m_strBuildingNumber;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getSpaceNumber() {
		return $this->m_strSpaceNumber;
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getIsLoggedIn() {
		return $this->m_boolIsLoggedIn;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getIntegrationClientTypes() {
		return $this->m_arrobjIntegrationCientTypes;
	}

	public function getIsCreateNewLease() {
		return $this->m_boolIsCreateNewLease;
	}

	public function getTermsAndConditions() {
		return $this->m_boolTermsAndConditions;
	}

	public function getCustomerIdOnCurrentLease() {
		return  $this->m_intCustomerIdOnCurrentLease;
	}

	public function getInvoiceDeliveryTypeId() {
		return  $this->m_intInvoiceDeliveryTypeId;
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function getCustomerTypeName() {
		return $this->m_strCustomerTypeName;
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function getDefaultCustomerRelationshipId() {
		return $this->m_intDefaultCustomerRelationshipId;
	}

	public function getCustomerRelationshipName() {
		return $this->m_strCustomerRelationshipName;
	}

	public function getMaritalStatusTypeName() {
		return $this->m_strMaritalStatusTypeName;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getMiniminumPaymentAmount() {
		return $this->m_fltMiniminumPaymentAmount;
	}

	public function getDelinquentAmount() {
		return $this->m_fltDelinquentAmount;
	}

	public function getIsBlockRentReminderEmail() {
		return $this->m_boolIsBlockRentReminderEmail;
	}

	public function getIsBlockResidentCommunicationEmail() {
		return $this->m_boolIsBlockResidentCommunicationEmail;
	}

	public function getIsBlockMessageCenterEmail() {
		return $this->m_boolIsBlockMessageCenterEmail;
	}

	public function getIsBlockParcelAlertEmail() {
		return $this->m_boolIsBlockParcelAlertEmail;
	}

	public function getIsBlockMaintenanceUpdateEmail() {
		return $this->m_boolIsBlockMaintenanceUpdateEmail;
	}

	public function getIsBlockPropertyMarketingEmail() {
		return $this->m_boolIsBlockPropertyMarketingEmail;
	}

	public function getIsBlockRentReminderSms() {
		return $this->m_boolIsBlockRentReminderSms;
	}

	public function getIsBlockMessageCenterSms() {
		return $this->m_boolIsBlockMessageCenterSms;
	}

	public function getIsBlockMaintenanceNotificationSms() {
		return $this->m_boolIsBlockMaintenanceNotificationSms;
	}

	public function getIsBlockPackageNotificationSms() {
		return $this->m_boolIsBlockPackageNotificationSms;
	}

	public function getIsBlockContactPointsSms() {
		return $this->m_boolIsBlockContactPointsSms;
	}

	public function getIsBlockPropertyMarketingSms() {
		return $this->m_boolIsBlockPropertyMarketingSms;
	}

	public function getInsurancePolicies() {
		return $this->m_arrobjInsurancePolicies;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getInsurancePolicyLeaseId() {
		return $this->m_intInsurancePolicyLeaseId;
	}

	public function getStreetLine1() {
		return $this->m_arrstrStreetLine1;
	}

	public function getDuplicateMessage() {
		return $this->m_intDuplicateMessage;
	}

	public function getAllowAccessToUnit() {
		return $this->m_intAllowAccessToUnit;
	}

	public function getInitials() {
		$arrstrNameFirst 	        = \Psi\CStringService::singleton()->str_split( $this->getNameFirst() );
		$arrstrNameMiddle 	        = ( 0 < strlen( $this->getNameMiddle() ) ) ? \Psi\CStringService::singleton()->str_split( $this->getNameMiddle() ) : NULL;
		$arrstrNameLast 	        = ( 0 < strlen( $this->getNameLast() ) ) ? \Psi\CStringService::singleton()->str_split( $this->getNameLast() ) : NULL;
		$arrstrNameLastMatronymic   = ( 0 < strlen( $this->getNameLastMatronymic() ) ) ? \Psi\CStringService::singleton()->str_split( $this->getNameLastMatronymic() ) : NULL;

		return $arrstrNameFirst[0] . ( ( false == is_null( $arrstrNameMiddle ) ) ? $arrstrNameMiddle[0] : '' ) . ( ( false == is_null( $arrstrNameLast ) ) ? $arrstrNameLast[0] : '' ) . ( ( false == is_null( $arrstrNameLastMatronymic ) ) ? $arrstrNameLastMatronymic[0] : '' );
	}

	public function getFirstNameAndLastInitial() {
		$arrstrNameLast 	= ( 0 < strlen( $this->getNameLast() ) ) ? $this->getNameLast() : NULL;
		return $this->getNameFirst() . ' ' . ( ( false == is_null( $arrstrNameLast ) ) ? $arrstrNameLast[0] : '' );
	}

	public function getPrimaryPhoneNumber() {
		return $this->m_strPrimaryPhoneNumber;
	}

	public function getCustomerEventId() {
		return $this->m_intCustomerEventId;
	}

	public function getCustomerEventCreatedOn() {
		return $this->m_strCustomerEventCreatedOn;
	}

	public function getLastPaidOn() {
		return $this->m_strLastPaidOn;
	}

	public function getIsOptOutOfApartmates() {
		return $this->m_boolIsOptOutOfApartmates;
	}

	public function getIsOverrideAndAllowUnderAgeOccupant() {
		return $this->m_boolIsOverrideAndAllowUnderAgeOccupant;
	}

	public function getApplicantApplications() {
		return $this->m_arrobjApplicantApplications;
	}

	public function getApplications() {
		return $this->m_arrobjApplications;
	}

	public function getIsBlockInspectionEmail() {
		return $this->m_boolIsBlockInspectionEmail;
	}

	public function getIsBlockContactPointsEmail() {
		return $this->m_boolIsBlockContactPointsEmail;
	}

	public function getLeaseStatusTypeName() {
		return $this->m_strLeaseStatusTypeName;
	}

	public function getMergeFields() {
		return $this->m_arrstrMergeFields;
	}

	public function getMergeFieldByKey( $strKey ) {
		if( true == array_key_exists( $strKey, $this->m_arrstrMergeFields ) ) {
			return $this->m_arrstrMergeFields[$strKey];
		}

		return NULL;
	}

	public function getIsCreateNewCustomer() {
		return $this->m_boolIsCreateNewCustomer;
	}

	public function getRecentLog( $objDatabase ) {
		$objCustomerLog = \Psi\Eos\Entrata\CCustomerLogs::createService()->fetchRecentCustomerLogByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objCustomerLog, 'CCustomerLog' ) ) {
			return $objCustomerLog;
		}

		return NULL;
	}

	public function getSecondPassword() {
		$this->m_strSecondPassword;
	}

	public function getGoogleAuthenticatorSecretKey() {
		return ( false == is_null( $this->m_strUsername ) ) ? \Psi\CStringService::singleton()->strtoupper( str_replace( [ 0, 1, 8, 9 ], [ 'A', 'D', 'Z', 'Q' ], md5( CEncryption::encryptText( $this->m_strUsername, CONFIG_KEY_GOOGLE_AUTHENTICATOR_SECRET ) ) ) ) : NULL;
	}

	public function getI18nPrimaryPhoneNumber() {
		return $this->m_objI18nPrimaryPhoneNumber;
	}

	public function getI18nMobileNumber() {
		return $this->m_objI18nMobileNumber;
	}

	public function getI18nSecondaryPhoneNumber() {
		return $this->m_objI18nSecondaryPhoneNumber;
	}

	public function getResponsibleLedgerFilterIds() {
		return $this->m_arrintResponsibleLedgerFilterIds;
	}

	public function getIsResident() {
		return $this->m_boolIsResident;
	}

	public function getTaxIdentificationTypeName() {
		return $this->m_strTaxIdentificationTypeName;
	}

	public function getTaxIdTypeName() {
		return $this->m_strTaxIdTypeName;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function getIsCustomerContact() {
		return $this->m_boolIsCustomerContact;
	}

	public function getCustomerContactId() {
		return $this->m_intCustomerContactId;
	}

	public function getExtraParameters() {
		return $this->m_arrmixExtraParameters;
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function getOrganizationId() {
		return $this->m_intOrganizationId;
	}

	public function getConnectedCustomerId() {
		return $this->m_intConnectedCustomerId;
	}

	public function getOrganizationContractId() {
		return $this->m_intOrganizationContractId;
	}

	public function getIsCustomerDeduped() {
		return $this->m_boolIsCustomerDeduped;
	}

	public function getIsOnlyAdditionalPhoneNumber() {
		return $this->m_boolIsOnlyAdditionalPhoneNumber;
	}

	public function getCustomerPhoneNumbers() {
		return $this->m_arrmixCustomerPhoneNumbers;
	}

	public function getForwardingStreetLine1() {
		return $this->m_strForwardingStreetLine1;
	}

	public function getForwardingStreetLine2() {
		return $this->m_strForwardingStreetLine2;
	}

	public function getForwardingStreetLine3() {
		return $this->m_strForwardingStreetLine3;
	}

	public function getForwardingCity() {
		return $this->m_strForwardingCity;
	}

	public function getForwardingStateCode() {
		return $this->m_strForwardingStateCode;
	}

	public function getForwardingPostalCode() {
		return $this->m_strForwardingPostalCode;
	}

	public function getForwardingCountryCode() {
		return $this->m_strForwardingCountryCode;
	}

	public function getCurrentStreetLine1() {
		return $this->m_strCurrentStreetLine1;
	}

	public function getCurrentStreetLine2() {
		return $this->m_strCurrentStreetLine2;
	}

	public function getCurrentStreetLine3() {
		return $this->m_strCurrentStreetLine3;
	}

	public function getCurrentCity() {
		return $this->m_strCurrentCity;
	}

	public function getCurrentStateCode() {
		return $this->m_strCurrentStateCode;
	}

	public function getCurrentPostalCode() {
		return $this->m_strCurrentPostalCode;
	}

	public function getCurrentCountryCode() {
		return $this->m_strCurrentCountryCode;
	}

	/**
	 * Set Functions
	 */

	public function setParentLeadSourceId( $intParentLeadSourceId ) {
		$this->m_intParentLeadSourceId = $intParentLeadSourceId;
	}

	public function setMobileConfirmationCode( $strMobileConfirmationCode ) {
		$this->m_strMobileConfirmationCode = $strMobileConfirmationCode;
	}

	public function setAuthenticationLogId( $intAuthenticationLogId ) {
		$this->m_intAuthenticationLogId = CStrings::strToIntDef( $intAuthenticationLogId, NULL, true );
	}

	public function setErrorMsgs( $arrobjErrorMsgs ) {
		$this->m_arrobjErrorMsgs = $arrobjErrorMsgs;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setNameFirst( $strNameFirst ) {
		parent::setNameFirst( \Psi\CStringService::singleton()->ucfirst( $strNameFirst ) );
	}

	public function setNameMiddle( $strNameMiddle ) {
		parent::setNameMiddle( \Psi\CStringService::singleton()->ucfirst( $strNameMiddle ) );
	}

	public function setNameLast( $strNameLast ) {
		parent::setNameLast( \Psi\CStringService::singleton()->ucfirst( $strNameLast ) );
	}

	public function setCompanyName( $strCompanyName ) {
		parent::setCompanyName( $strCompanyName );
	}

	public function setEmailAddress( $strEmailAddress ) {
		parent::setEmailAddress( \Psi\CStringService::singleton()->strtolower( $strEmailAddress ) );
	}

	public function setUsername( $strUsername ) {
		parent::setUsername( trim( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) );
	}

	public function setPassword( $strPassword ) {
		$this->m_strPasswordEncrypted = CStrings::strTrimDef( $strPassword, 240, NULL, true );
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setPasswordAnswer( $strPasswordAnswer ) {
		$this->m_strPasswordAnswer = trim( $strPasswordAnswer );

		// also set the encrypted password answer
		$this->m_strPasswordAnswerEncrypted = CHash::createService()->hashCustomer( trim( $strPasswordAnswer ) );
	}

	public function setBirthDate( $strBirthDate ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strBirthDate, '**' ) )
			return;
		$this->m_strBirthDate = CStrings::strTrimDef( $strBirthDate, -1, NULL, true );
	}

	public function setTaxNumber( $strPlainTaxNumber, $boolReformatTaxNumber = true ) {
		if( false == valStr( $strPlainTaxNumber ) || true == \Psi\CStringService::singleton()->stristr( $strPlainTaxNumber, '*' ) ) {
			return;
		}
		if( true == $boolReformatTaxNumber ) {
			$strTaxNumberEncrypted = NULL;
			$strTaxNumberMasked    = NULL;
			$strPlainTaxNumber     = preg_replace( '/[^a-z0-9]/i', '', $strPlainTaxNumber );
			if( true == valStr( $strPlainTaxNumber ) ) {
				$strTaxNumberEncrypted = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER );
				$strTaxNumberMasked    = CEncryption::maskTaxIdTypeIdText( $strPlainTaxNumber, $this->getTaxIdTypeId() );
			}
		} else {
			$strTaxNumberEncrypted = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER );
			$strTaxNumberMasked    = CEncryption::maskTaxIdTypeIdText( $strPlainTaxNumber, $this->getTaxIdTypeId() );
		}
		$this->setTaxNumberEncrypted( $strTaxNumberEncrypted );
		$this->setTaxNumberMasked( $strTaxNumberMasked );
	}

	public function setDlNumber( $strPlainDlNumber ) {
		if( false == valStr( $strPlainDlNumber ) || true == \Psi\CStringService::singleton()->stristr( $strPlainDlNumber, 'xxxx' ) ) return;

		$this->setDlNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainDlNumber, CONFIG_SODIUM_KEY_DL_NUMBER ) );
	}

	public function setIdentificationNumber( $strPlainDlNumber ) {
		if( false == valStr( $strPlainDlNumber ) ) {
			return;
		}
		$this->setIdentificationValue( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainDlNumber, CONFIG_SODIUM_KEY_DL_NUMBER ) );
	}

	public function setPropertyName( $strPropertyName ) {
		return $this->m_strPropertyName = $strPropertyName;
	}

	public function setBuildingNumber( $strBuildingNumber ) {
		return $this->m_strBuildingNumber = $strBuildingNumber;
	}

	public function setChargeDeposits( $arrintChargeDeposits ) {
		return $this->m_arrfltChargeDeposits = $arrintChargeDeposits;
	}

	public function setUnitNumber( $strUnitNumber ) {
		return $this->m_strUnitNumber = $strUnitNumber;
	}

	public function setSpaceNumber( $strSpaceNumber ) {
		return $this->m_strSpaceNumber = $strSpaceNumber;
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		return $this->m_intPropertyBuildingId = $intPropertyBuildingId;
	}

	public function setBuildingName( $strBuildingName ) {
		return $this->m_strBuildingName = $strBuildingName;
	}

	public function setTermsAndConditions( $boolTermsAndConditions ) {
		$this->m_boolTermsAndConditions = $boolTermsAndConditions;
	}

	public function setLoginAttemptData( $boolIsLoggedIn = false ) {

		if( true == $boolIsLoggedIn ) {
			$this->setLoginAttemptCount( 0 );

		} elseif( time() > ( strtotime( date( 'c', strtotime( $this->getLastLoginAttemptOn() ) ) ) + self::LOGIN_ATTEMPT_WAITING_TIME ) && 0 < $this->getLoginAttemptCount() ) {

			$this->setLoginAttemptCount( 1 );

		} else {
			$this->setLoginAttemptCount( $this->getLoginAttemptCount() + 1 );
		}

		$this->setLastLoginAttemptOn( date( 'c', time() ) );

	}

	public function setInsurancePolicyLeaseId( $intInsurancePolicyLeaseId ) {
		$this->m_intInsurancePolicyLeaseId = $intInsurancePolicyLeaseId;
	}

	public function setCustomerEventId( $intCustomerEventId ) {
		$this->m_intCustomerEventId = $intCustomerEventId;
	}

	public function setCustomerEventCreatedOn( $strCustomerEventCreatedOn ) {
		$this->m_strCustomerEventCreatedOn = $strCustomerEventCreatedOn;
	}

	public function setLastPaidOn( $strLastPaidOn ) {
		$this->m_strLastPaidOn = $strLastPaidOn;
	}

	public function setIsOptOutOfApartmates( $boolIsOptOutOfApartmates ) {
		$this->m_boolIsOptOutOfApartmates = $boolIsOptOutOfApartmates;
	}

	public function setAllowAccessToUnit( $intAllowAccessToUnit ) {
		$this->m_intAllowAccessToUnit = $intAllowAccessToUnit;
	}

	public function setIsOverrideAndAllowUnderAgeOccupant( $boolIsOverrideAndAllowUnderAgeOccupant ) {
		$this->m_boolIsOverrideAndAllowUnderAgeOccupant = $boolIsOverrideAndAllowUnderAgeOccupant;
	}

	public function setApplicantApplications( $arrobjApplicantApplications ) {
		$this->m_arrobjApplicantApplications = $arrobjApplicantApplications;
	}

	public function setApplications( $arrobjApplications ) {
		$this->m_arrobjApplications = $arrobjApplications;
	}

	public function setFormattedNameFull( $objDatabase ) {

		$strLocaleCode = $this->getPreferredLocaleCode() ?? \Psi\Eos\Entrata\CClients::createService()->fetchLocaleCodeById( $this->getCid(), $objDatabase );

        $arrmixCustomerNameDetails = [
        	'name_prefix' => $this->getNamePrefix(),
            'name_first' => $this->getNameFirst(),
            'preferred_name' => $this->getPreferredName(),
            'name_last' => $this->getNameLast(),
            'name_last_matronymic' => $this->getNameLastMatronymic(),
	        'name_suffix' => $this->getNameSuffix(),
            'company_name' => $this->getCompanyName()
        ];

		$objPersonNameService = CPersonNameService::createService();
		$this->setNameFull( $objPersonNameService->format( $objPersonNameService->createPersonName( $arrmixCustomerNameDetails ), CPersonNameFormat::GIVEN_NAME_FORMAT, $strLocaleCode ) );
	}

	public function setConnectedCustomerId( $intConnectedCustomerId ) {
		$this->m_intConnectedCustomerId = $intConnectedCustomerId;
	}

	public function setPrimaryPhoneNumber( $strPrimaryPhoneNumber ) {
		$this->m_strPrimaryPhoneNumber = $strPrimaryPhoneNumber;
	}

	public function setForwardingStreetLine1( $strForwardingStreetLine1 ) {
		$this->m_strForwardingStreetLine1 = $strForwardingStreetLine1;
	}

	public function setForwardingStreetLine2( $strForwardingStreetLine2 ) {
		$this->m_strForwardingStreetLine2 = $strForwardingStreetLine2;
	}

	public function setForwardingStreetLine3( $strForwardingStreetLine3 ) {
		$this->m_strForwardingStreetLine3 = $strForwardingStreetLine3;
	}

	public function setForwardingCity( $strForwardingCity ) {
		$this->m_strForwardingCity = $strForwardingCity;
	}

	public function setForwardingStateCode( $strForwardingStateCode ) {
		$this->m_strForwardingStateCode = $strForwardingStateCode;
	}

	public function setForwardingCountryCode( $strForwardingCountryCode ) {
		$this->m_strForwardingCountryCode = $strForwardingCountryCode;
	}

	public function setForwardingPostalCode( $strForwardingPostalCode ) {
		$this->m_strForwardingPostalCode = $strForwardingPostalCode;
	}

	public function setCurrentStreetLine1( $strCurrentStreetLine1 ) {
		$this->m_strCurrentStreetLine1 = $strCurrentStreetLine1;
	}

	public function setCurrentStreetLine2( $strCurrentStreetLine2 ) {
		$this->m_strCurrentStreetLine2 = $strCurrentStreetLine2;
	}

	public function setCurrentStreetLine3( $strCurrentStreetLine3 ) {
		$this->m_strCurrentStreetLine3 = $strCurrentStreetLine3;
	}

	public function setCurrentCity( $strCurrentCity ) {
		$this->m_strCurrentCity = $strCurrentCity;
	}

	public function setCurrentStateCode( $strCurrentStateCode ) {
		$this->m_strCurrentStateCode = $strCurrentStateCode;
	}

	public function setCurrentPostalCode( $strCurrentPostalCode ) {
		$this->m_strCurrentPostalCode = $strCurrentPostalCode;
	}

	public function setCurrentCountryCode( $strCurrentCountryCode ) {
		$this->m_strCurrentCountryCode = $strCurrentCountryCode;
	}

	public function getEncryptedBlindIndex( $strTaxNumberPlainText ) {
		if( true == valStr( $strTaxNumberPlainText ) ) {
			return \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $strTaxNumberPlainText, CONFIG_SODIUM_KEY_BLIND_INDEX );
		}

		return $strTaxNumberPlainText;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		// save original values to possibly update global user record on customer update
		if( true == is_null( $this->m_strOriginalUsername ) ) {
			$this->m_strOriginalUsername = $this->m_strUsername;
			$this->m_strOriginalPasswordEncrypted = $this->m_strPasswordEncrypted;
			$this->m_intOriginalLoginAttemptCount = $this->m_intLoginAttemptCount;
			$this->m_strOriginalLastLoginAttemptOn = $this->m_strLastLoginAttemptOn;
			$this->m_intOriginalIsDisabled = $this->m_intIsDisabled;
		}

		if( true == isset( $arrmixValues['tax_number'] ) ) $this->setTaxNumber( $arrmixValues['tax_number'] );
		if( true == isset( $arrmixValues['identification_number'] ) ) $this->setIdentificationNumber( $arrmixValues['identification_number'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) $this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['space_number'] ) ) $this->setSpaceNumber( $arrmixValues['space_number'] );
		if( true == isset( $arrmixValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrmixValues['property_building_id'] );
		if( true == isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( $arrmixValues['building_name'] );
		if( true == isset( $arrmixValues['password'] ) ) $this->setPassword( $arrmixValues['password'] );
		if( true == isset( $arrmixValues['password_question'] ) ) $this->setPasswordQuestion( $arrmixValues['password_question'] );
		if( true == isset( $arrmixValues['password_answer'] ) ) $this->setPasswordAnswer( $arrmixValues['password_answer'] );
		if( true == isset( $arrmixValues['lease_id'] ) ) $this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
		if( true == isset( $arrmixValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
		if( true == isset( $arrmixValues['customer_type_name'] ) ) $this->setCustomerTypeName( $arrmixValues['customer_type_name'] );
		if( true == isset( $arrmixValues['is_create_new_lease'] ) ) $this->setIsCreateNewLease( $arrmixValues['is_create_new_lease'] );
		if( true == isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( $arrmixValues['building_name'] );
		if( true == isset( $arrmixValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		if( true == isset( $arrmixValues['mobile_confirmation_code'] ) ) $this->setMobileConfirmationCode( $arrmixValues['mobile_confirmation_code'] );
		if( true == isset( $arrmixValues['lease_interval_id'] ) ) $this->setLeaseIntervalId( $arrmixValues['lease_interval_id'] );

		if( true == isset( $arrmixValues['name_first'] ) ) $this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) $this->setNameLast( $arrmixValues['name_last'] );
        if( true == isset( $arrmixValues['name_full'] ) ) $this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['phone_number'] ) ) $this->setPhoneNumber( $arrmixValues['phone_number'] );
		if( true == isset( $arrmixValues['birth_date'] ) ) $this->setBirthDate( $arrmixValues['birth_date'] );
		if( true == isset( $arrmixValues['lease_id'] ) ) $this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['invoice_delivery_type'] ) ) $this->setInvoiceDeliveryTypeId( $arrmixValues['invoice_delivery_type'] );
		if( true == isset( $arrmixValues['is_block_rent_reminder_email'] ) ) $this->setIsBlockRentReminderEmail( $arrmixValues['is_block_rent_reminder_email'] );
		if( true == isset( $arrmixValues['is_block_resident_communication_email'] ) ) $this->setIsBlockResidentCommunicationEmail( $arrmixValues['is_block_resident_communication_email'] );
		if( true == isset( $arrmixValues['is_block_inspection_email'] ) ) $this->setIsBlockInspectionEmail( $arrmixValues['is_block_inspection_email'] );
		if( true == isset( $arrmixValues['is_block_contact_points_email'] ) ) $this->setIsBlockContactPointsEmail( $arrmixValues['is_block_contact_points_email'] );
		if( true == isset( $arrmixValues['is_block_message_center_email'] ) ) $this->setIsBlockMessageCenterEmail( $arrmixValues['is_block_message_center_email'] );
		if( true == isset( $arrmixValues['is_block_parcel_alert_email'] ) ) $this->setIsBlockParcelAlertEmail( $arrmixValues['is_block_parcel_alert_email'] );
		if( true == isset( $arrmixValues['is_block_maintenance_update_email'] ) ) $this->setIsBlockMaintenanceUpdateEmail( $arrmixValues['is_block_maintenance_update_email'] );
		if( true == isset( $arrmixValues['is_block_property_marketing_email'] ) ) $this->setIsBlockPropertyMarketingEmail( $arrmixValues['is_block_property_marketing_email'] );
		if( true == isset( $arrmixValues['is_block_rent_reminder_sms'] ) ) $this->setIsBlockRentReminderSms( $arrmixValues['is_block_rent_reminder_sms'] );
		if( true == isset( $arrmixValues['is_block_message_center_sms'] ) ) $this->setIsBlockMessageCenterSms( $arrmixValues['is_block_message_center_sms'] );
		if( true == isset( $arrmixValues['is_block_maintenance_notification_sms'] ) ) $this->setIsBlockMaintenanceNotificationSms( $arrmixValues['is_block_maintenance_notification_sms'] );
		if( true == isset( $arrmixValues['is_block_package_notification_sms'] ) ) $this->setIsBlockPackageNotificationSms( $arrmixValues['is_block_package_notification_sms'] );
		if( true == isset( $arrmixValues['is_block_contact_points_sms'] ) ) $this->setIsBlockContactPointsSms( $arrmixValues['is_block_contact_points_sms'] );
		if( true == isset( $arrmixValues['is_block_property_marketing_sms'] ) ) $this->setIsBlockPropertyMarketingSms( $arrmixValues['is_block_property_marketing_sms'] );
		if( true == isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( true == isset( $arrmixValues['insurance_policy_lease_id'] ) ) $this->setInsurancePolicyLeaseId( $arrmixValues['insurance_policy_lease_id'] );
		if( true == isset( $arrmixValues['duplicate_message'] ) ) $this->setDuplicateMessage( $arrmixValues['duplicate_message'] );
		if( true == isset( $arrmixValues['customer_event_id'] ) ) 	$this->setCustomerEventId( $arrmixValues['customer_event_id'] );
		if( true == isset( $arrmixValues['customer_event_created_on'] ) ) $this->setCustomerEventCreatedOn( $arrmixValues['customer_event_created_on'] );
		if( true == isset( $arrmixValues['is_opt_out_of_apartmates'] ) ) $this->setIsOptOutOfApartmates( $arrmixValues['is_opt_out_of_apartmates'] );
		if( true == isset( $arrmixValues['parent_lead_source_id'] ) ) $this->setParentLeadSourceId( $arrmixValues['parent_lead_source_id'] );
		if( true == isset( $arrmixValues['allow_access_to_unit'] ) ) $this->setAllowAccessToUnit( $arrmixValues['allow_access_to_unit'] );
		if( true == isset( $arrmixValues['customer_relationship_id'] ) ) $this->setCustomerRelationshipId( $arrmixValues['customer_relationship_id'] );
		if( true == isset( $arrmixValues['default_customer_relationship_id'] ) ) $this->setDefaultCustomerRelationshipId( $arrmixValues['default_customer_relationship_id'] );
		if( true == isset( $arrmixValues['customer_relationship_name'] ) ) $this->setCustomerRelationshipName( $arrmixValues['customer_relationship_name'] );
		if( true == isset( $arrmixValues['marital_status_type_name'] ) ) $this->setMaritalStatusTypeName( $arrmixValues['marital_status_type_name'] );
		if( true == isset( $arrmixValues['is_override_and_allow_under_age_occupant'] ) ) $this->setIsOverrideAndAllowUnderAgeOccupant( $arrmixValues['is_override_and_allow_under_age_occupant'] );
		if( true == isset( $arrmixValues['lease_status_type_name'] ) ) $this->setLeaseStatusTypeName( $arrmixValues['lease_status_type_name'] );
		if( true == isset( $arrmixValues['delinquent_amount'] ) ) $this->setDelinquentAmount( $arrmixValues['delinquent_amount'] );
		if( true == isset( $arrmixValues['last_paid_on'] ) ) $this->setLastPaidOn( $arrmixValues['last_paid_on'] );
		if( true == isset( $arrmixValues['applicant_id'] ) ) $this->setApplicantId( $arrmixValues['applicant_id'] );
		if( true == isset( $arrmixValues['forwarding_street_line1'] ) ) $this->setForwardingStreetLine1( $arrmixValues['forwarding_street_line1'] );
		if( true == isset( $arrmixValues['forwarding_street_line2'] ) ) $this->setForwardingStreetLine2( $arrmixValues['forwarding_street_line2'] );
		if( true == isset( $arrmixValues['forwarding_street_line3'] ) ) $this->setForwardingStreetLine3( $arrmixValues['forwarding_street_line3'] );
		if( true == isset( $arrmixValues['forwarding_city'] ) ) $this->setForwardingCity( $arrmixValues['forwarding_city'] );
		if( true == isset( $arrmixValues['forwarding_state_code'] ) ) $this->setForwardingStateCode( $arrmixValues['forwarding_state_code'] );
		if( true == isset( $arrmixValues['forwarding_postal_code'] ) ) $this->setForwardingPostalCode( $arrmixValues['forwarding_postal_code'] );
		if( true == isset( $arrmixValues['forwarding_country_code'] ) ) $this->setForwardingCountryCode( $arrmixValues['forwarding_country_code'] );
		if( true == isset( $arrmixValues['current_street_line1'] ) ) $this->setCurrentStreetLine1( $arrmixValues['current_street_line1'] );
		if( true == isset( $arrmixValues['current_street_line2'] ) ) $this->setCurrentStreetLine2( $arrmixValues['current_street_line2'] );
		if( true == isset( $arrmixValues['current_street_line3'] ) ) $this->setCurrentStreetLine3( $arrmixValues['current_street_line3'] );
		if( true == isset( $arrmixValues['current_city'] ) ) $this->setCurrentCity( $arrmixValues['current_city'] );
		if( true == isset( $arrmixValues['current_state_code'] ) ) $this->setCurrentStateCode( $arrmixValues['current_state_code'] );
		if( true == isset( $arrmixValues['current_postal_code'] ) ) $this->setCurrentPostalCode( $arrmixValues['current_postal_code'] );
		if( true == isset( $arrmixValues['current_country_code'] ) ) $this->setCurrentCountryCode( $arrmixValues['current_country_code'] );
		if( true == isset( $arrmixValues['responsible_ledger_filter_ids'] ) ) $this->setResponsibleLedgerFilterIds( $arrmixValues['responsible_ledger_filter_ids'] );

		if( true == isset( $arrmixValues['is_resident'] ) ) $this->setIsResident( $arrmixValues['is_resident'] );
		if( true == isset( $arrmixValues['customer_identification_type_name'] ) ) $this->setTaxIdentificationTypeName( $arrmixValues['customer_identification_type_name'] );
		if( true == isset( $arrmixValues['tax_id_type_name'] ) ) $this->setTaxIdTypeName( $arrmixValues['tax_id_type_name'] );
		if( true == isset( $arrmixValues['is_customer_contact'] ) ) $this->setIsCustomerContact( $arrmixValues['is_customer_contact'] );
		if( true == isset( $arrmixValues['customer_contact_id'] ) ) $this->setCustomerContactId( $arrmixValues['customer_contact_id'] );
		if( true == isset( $arrmixValues['move_in_date'] ) ) $this->setMoveInDate( $arrmixValues['move_in_date'] );
		if( true == isset( $arrmixValues['move_out_date'] ) ) $this->setMoveOutDate( $arrmixValues['move_out_date'] );
		if( true == isset( $arrmixValues['organization_id'] ) ) $this->setOrganizationId( $arrmixValues['organization_id'] );
		if( true == isset( $arrmixValues['connected_customer_id'] ) ) $this->setConnectedCustomerId( $arrmixValues['connected_customer_id'] );

		if( true == isset( $arrmixValues['occupancy_type_id'] ) ) {
			$this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
		}
		if( true == isset( $arrmixValues['primary_phone_number'] ) ) $this->setPrimaryPhoneNumber( $arrmixValues['primary_phone_number'] );

		$this->setOrganizationContractId( $arrmixValues['organization_contract_id'] ?? NULL );
		return;
	}

	public function setIsLoggedIn( $boolLoggedIn ) {
		$this->m_boolIsLoggedIn = $boolLoggedIn;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setIntegrationClientTypes( $arrobjIntegrationCientTypes ) {
		$this->m_arrobjIntegrationCientTypes = $arrobjIntegrationCientTypes;
	}

	public function setIsCreateNewLease( $boolIsCreateNewLease ) {
		$this->m_boolIsCreateNewLease = $boolIsCreateNewLease;
	}

	public function setCustomerIdOnCurrentLease( $intCustomerIdOnCurrentLease ) {
		$this->m_intCustomerIdOnCurrentLease = $intCustomerIdOnCurrentLease;
	}

	public function setInvoiceDeliveryTypeId( $intInvoiceDeliveryTypeId ) {
		$this->m_intInvoiceDeliveryTypeId = $intInvoiceDeliveryTypeId;
	}

	public function setIsBlockRentReminderEmail( $boolIsBlockRentReminderEmail ) {
		$this->m_boolIsBlockRentReminderEmail = $boolIsBlockRentReminderEmail;
	}

	public function setIsBlockResidentCommunicationEmail( $boolIsBlockResidentCommunicationEmail ) {
		$this->m_boolIsBlockResidentCommunicationEmail = $boolIsBlockResidentCommunicationEmail;
	}

	public function setIsBlockMessageCenterEmail( $boolIsBlockMessageCenterEmail ) {
		$this->m_boolIsBlockMessageCenterEmail = $boolIsBlockMessageCenterEmail;
	}

	public function setIsBlockParcelAlertEmail( $boolIsBlockParcelAlertEmail ) {
		$this->m_boolIsBlockParcelAlertEmail = $boolIsBlockParcelAlertEmail;
	}

	public function setIsBlockMaintenanceUpdateEmail( $boolIsBlockMaintenanceUpdateEmail ) {
		$this->m_boolIsBlockMaintenanceUpdateEmail = $boolIsBlockMaintenanceUpdateEmail;
	}

	public function setIsBlockPropertyMarketingEmail( $boolIsBlockPropertyMarketingEmail ) {
		$this->m_boolIsBlockPropertyMarketingEmail = $boolIsBlockPropertyMarketingEmail;
	}

	public function setIsBlockRentReminderSms( $boolIsBlockRentReminderSms ) {
		$this->m_boolIsBlockRentReminderSms = $boolIsBlockRentReminderSms;
	}

	public function setIsBlockMessageCenterSms( $boolIsBlockMessageCenterSms ) {
		$this->m_boolIsBlockMessageCenterSms = $boolIsBlockMessageCenterSms;
	}

	public function setIsBlockMaintenanceNotificationSms( $boolIsBlockMaintenanceNotificationSms ) {
		$this->m_boolIsBlockMaintenanceNotificationSms = $boolIsBlockMaintenanceNotificationSms;
	}

	public function setIsBlockPackageNotificationSms( $boolIsBlockPackageNotificationSms ) {
		$this->m_boolIsBlockPackageNotificationSms = $boolIsBlockPackageNotificationSms;
	}

	public function setIsBlockContactPointsSms( $boolIsBlockContactPointsSms ) {
		$this->m_boolIsBlockContactPointsSms = $boolIsBlockContactPointsSms;
	}

	public function setIsBlockPropertyMarketingSms( $boolIsBlockPropertyMarketingSms ) {
		$this->m_boolIsBlockPropertyMarketingSms = $boolIsBlockPropertyMarketingSms;
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->m_intCustomerTypeId = $intCustomerTypeId;
	}

	public function setCustomerTypeName( $strCustomerTypeName ) {
		$this->m_strCustomerTypeName = $strCustomerTypeName;
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->m_intCustomerRelationshipId = $intCustomerRelationshipId;
	}

	public function setDefaultCustomerRelationshipId( $intDefaultCustomerRelationshipId ) {
		$this->m_intDefaultCustomerRelationshipId = $intDefaultCustomerRelationshipId;
	}

	public function setCustomerRelationshipName( $strCustomerRelationshipName ) {
		$this->m_strCustomerRelationshipName = $strCustomerRelationshipName;
	}

	public function setMaritalStatusTypeName( $strMaritalStatusTypeName ) {
		$this->m_strMaritalStatusTypeName = $strMaritalStatusTypeName;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setMiniminumPaymentAmount( $fltMiniminumPaymentAmount ) {
		$this->m_fltMiniminumPaymentAmount = $fltMiniminumPaymentAmount;
	}

	public function setDelinquentAmount( $fltDelinquentAmount ) {
		$this->m_fltDelinquentAmount = $fltDelinquentAmount;
	}

	public function setInsurancePolicies( $arrobjInsurancePolicies ) {
		$this->m_arrobjInsurancePolicies = $arrobjInsurancePolicies;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->m_intLeaseIntervalId = $intLeaseIntervalId;
	}

	public function setStreetLine1( $arrstrStreetLine1 ) {
		$this->m_arrstrStreetLine1 = $arrstrStreetLine1;
	}

	public function setDuplicateMessage( $intDuplicateMessage ) {
		$this->m_intDuplicateMessage = $intDuplicateMessage;
	}

	public function setIsBlockInspectionEmail( $boolIsBlockInspectionEmail ) {
		$this->m_boolIsBlockInspectionEmail = $boolIsBlockInspectionEmail;
	}

	public function setIsBlockContactPointsEmail( $boolIsBlockContactPointsEmail ) {
		$this->m_boolIsBlockContactPointsEmail = $boolIsBlockContactPointsEmail;
	}

	public function setLeaseStatusTypeName( $strLeaseStatusTypeName ) {
		$this->m_strLeaseStatusTypeName = $strLeaseStatusTypeName;
	}

	public function setMobileNumber( $strMobileNumber ) {
		if( true == valStr( $strMobileNumber ) ) {
			$strMobileNumber = preg_replace( '~[^0-9+ ]+~', '', $strMobileNumber );
		}

		parent::setMobileNumber( $strMobileNumber );
	}

	public function setMergeFields( $arrstrMergeFields ) {
		foreach( $arrstrMergeFields as $strKey => $strValue ) {
			$this->setMergeFieldByKey( $strKey, $strValue );
		}
	}

	public function setMergeFieldByKey( $strKey, $strValue ) {
		$this->m_arrstrMergeFields[$strKey] = $strValue;
	}

	public function setPhoneNumberByPhoneNumberTypeId( $strPhoneNumber, $intPhoneNumberTypeId ) {
		switch( $intPhoneNumberTypeId ) {
			case CPhoneNumberType::FAX:
				$this->setFaxNumber( $strPhoneNumber );
				break;

			default:
				// deafult break
				break;
		}
	}

	public function setPhoneNumberTypes() {
		$boolIsUpdateRequired = false;

		if( false == is_null( $this->getPrimaryPhoneNumberTypeId() ) && false == is_null( $this->getSecondaryPhoneNumberTypeId() ) && $this->getPrimaryPhoneNumberTypeId() == $this->getSecondaryPhoneNumberTypeId() ) {
			$boolIsUpdateRequired = true;
			$this->setSecondaryPhoneNumberTypeId( NULL );
		}

		// First to check if type is already set and according to that data is set or not, else set the type id
		// also, we are not showing the fax number in UI, so any of the type should not be set as Fax - NRW
		if( true == is_null( $this->getPrimaryPhoneNumber() ) ) {

			if( false == is_null( $this->getPhoneNumber() ) ) {
				$boolIsUpdateRequired = true;
				$this->setPrimaryPhoneNumberTypeId( CPhoneNumberType::HOME );

			} elseif( false == is_null( $this->getWorkNumber() ) ) {
				$boolIsUpdateRequired = true;
				$this->setPrimaryPhoneNumberTypeId( CPhoneNumberType::OFFICE );

			} elseif( false == is_null( $this->getMobileNumber() ) ) {
				$boolIsUpdateRequired = true;
				$this->setPrimaryPhoneNumberTypeId( CPhoneNumberType::MOBILE );
			}
		}

		if( true == is_null( $this->getSecondaryPhoneNumber() ) ) {

			if( false == is_null( $this->getPhoneNumber() ) && CPhoneNumberType::HOME != $this->getPrimaryPhoneNumberTypeId() ) {
				$boolIsUpdateRequired = true;
				$this->setSecondaryPhoneNumberTypeId( CPhoneNumberType::HOME );

			} elseif( false == is_null( $this->getWorkNumber() ) && CPhoneNumberType::OFFICE != $this->getPrimaryPhoneNumberTypeId() ) {
				$boolIsUpdateRequired = true;
				$this->setSecondaryPhoneNumberTypeId( CPhoneNumberType::OFFICE );

			} elseif( false == is_null( $this->getMobileNumber() ) && CPhoneNumberType::MOBILE != $this->getPrimaryPhoneNumberTypeId() ) {
				$boolIsUpdateRequired = true;
				$this->setSecondaryPhoneNumberTypeId( CPhoneNumberType::MOBILE );
			}
		}

		return $boolIsUpdateRequired;
	}

	public function setIsCreateNewCustomer( $boolIsCreateNewCustomer ) {
		$this->m_boolIsCreateNewCustomer = $boolIsCreateNewCustomer;
	}

	public function setSecondPassword( $strSecondPassword ) {
		$this->m_strSecondPassword = $strSecondPassword;
	}

	public function setIsResident( $boolIsResident ) {
		$this->m_boolIsResident = $boolIsResident;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$objDataset = $objDatabase->createDataset();

		// call service to insert name_full
		$this->setFormattedNameFull( $objDatabase );

		if( true == valStr( $this->getTaxNumberEncrypted() ) ) {
			$this->setTaxNumberBlindIndex( $this->getEncryptedBlindIndex( $this->getTaxNumber() ) );
		}

		$strSql = parent::insert( $intCurrentUserId, $objDatabase, true );

		// Insert applicant
		$strInsertApplicantSql 	= NULL;
		$objApplicant 			= NULL;

		if( true == is_null( $this->getApplicantId() ) ) {
			$objApplicant = CApplicationUtils::loadOrCreateApplicant( $this, $objDatabase );

			if( false == valObj( $objApplicant, 'CApplicant' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load applicant object.' ) ) );
				return false;
			}

			if( true == $boolReturnSqlOnly && true == is_null( $objApplicant->getId() ) ) {
				$strInsertApplicantSql = $objApplicant->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			}
		}

		if( true == $boolReturnSqlOnly ) {
			return $strSql . $strInsertApplicantSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {
			// Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer record. The following error was reported.' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert customer record. The following error was reported.' ) ) );
			// Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		if( true == valObj( $objApplicant, 'CApplicant' ) && true == is_null( $objApplicant->getId() ) && false == $objApplicant->insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		if( false == $this->insertOrUpdateCustomerPhoneNumbers( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		$objDataset->cleanup();

		if( false == is_null( $this->getPropertyId() ) ) {
			$this->syncGlobalUserRecords( $intCurrentUserId );
		}

		return true;
	}

	public function setI18nPrimaryPhoneNumber( $objI18nPrimaryPhoneNumber ) {
		$this->m_objI18nPrimaryPhoneNumber = $objI18nPrimaryPhoneNumber;
	}

	public function setI18nSecondaryPhoneNumber( $objI18nSecondaryPhoneNumber ) {
		$this->m_objI18nSecondaryPhoneNumber = $objI18nSecondaryPhoneNumber;
	}

	public function setI18nMobileNumber( $objI18nMobileNumber ) {
		$this->m_objI18nMobileNumber = $objI18nMobileNumber;
	}

	public function setResponsibleLedgerFilterIds( $arrintResponsibleLedgerFilterIds ) {
		$this->set( 'm_arrintResponsibleLedgerFilterIds', CStrings::strToArrIntDef( $arrintResponsibleLedgerFilterIds, NULL ) );
	}

	public function setTaxIdentificationTypeName( $strTaxIdentificationTypeName ) {
		$this->m_strTaxIdentificationTypeName = $strTaxIdentificationTypeName;
	}

	public function setTaxIdTypeName( $strTaxIdentificationTypeName ) {
		$this->m_strTaxIdTypeName = $strTaxIdentificationTypeName;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

	public function setIsCustomerContact( $boolIsCustomerContact ) {
		$this->m_boolIsCustomerContact = $boolIsCustomerContact;
	}

	public function setCustomerContactId( $intCustomerContactId ) {
		$this->m_intCustomerContactId = $intCustomerContactId;
	}

	public function setExtraParameters( $arrmixExtraParameters ) {
		$this->m_arrmixExtraParameters = $arrmixExtraParameters;
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->m_strMoveInDate = $strMoveInDate;
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->m_strMoveOutDate = $strMoveOutDate;
	}

	public function setOrganizationId( $intOrganizationId ) {
		$this->m_intOrganizationId = $intOrganizationId;
	}

	public function setOrganizationContractId( $intOrganizationContractId ) {
		$this->m_intOrganizationContractId = $intOrganizationContractId;
	}

	public function setIsCustomerDeduped( $boolIsCustomerDeduped ) {
		$this->m_boolIsCustomerDeduped = $boolIsCustomerDeduped;
	}

	public function setIsOnlyAdditionalPhoneNumber( $boolIsOnlyAdditionalPhoneNumber ) {
		return $this->m_boolIsOnlyAdditionalPhoneNumber = $boolIsOnlyAdditionalPhoneNumber;
	}

	public function setCustomerPhoneNumbers( $arrmixPhoneNumbers ) {
		$this->m_arrmixCustomerPhoneNumbers = $arrmixPhoneNumbers;
	}

	/**
	 * Integration Insert Update Functions
	 */

	public function syncGlobalUserRecords( $intCurrentUserId ) {

		$objConnectDatabase = CDatabases::createConnectDatabase( false );

		if( false === valObj( $objConnectDatabase, 'CDatabase' ) ) {
			return false;
		}

		if( false === valStr( $this->m_strUsername ) ) {
			// Deleting bad entries from global_users table
			$strSql = 'DELETE FROM public.global_users WHERE works_id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ' AND global_user_type_id = ' . CGlobalUserType::CUSTOMER;
			return $this->executeSql( $strSql, $this, $objConnectDatabase );
		}

		$arrobjGlobalUsers = ( array ) $this->fetchGlobalUsers( $objConnectDatabase );

		$objConnectDatabase->begin();

		$boolCustomerHasPropertyId = ( false == is_null( $this->getPropertyId() ) ) ? true : false;

		if( false == valArr( $arrobjGlobalUsers ) && true == $boolCustomerHasPropertyId ) {
			$this->insertGlobalUserRecord( $intCurrentUserId, $objConnectDatabase );
		} elseif( true == valArr( $arrobjGlobalUsers ) ) {
			$boolInsertNewGlobalUserForThisProperty = true;

			// update existing global user record(s)
			foreach( $arrobjGlobalUsers as $objGlobalUser ) {
				if( true == $boolCustomerHasPropertyId && $this->getPropertyId() == $objGlobalUser->getPropertyId() ) {
					// If this property already has a global user record we don't need another one
					$boolInsertNewGlobalUserForThisProperty = false;
				}

				$this->updateGlobalUserRecord( $objGlobalUser, $intCurrentUserId, $objConnectDatabase );
			}

			if( true == $boolInsertNewGlobalUserForThisProperty && true == $boolCustomerHasPropertyId ) {
				$this->insertGlobalUserRecord( $intCurrentUserId, $objConnectDatabase );
			}
		}

		$objConnectDatabase->commit();
		$objConnectDatabase->close();
	}

	public function insertGlobalUserRecord( $intCurrentUserId, $objConnectDatabase ) {
		// Insert new global user record
		$objGlobalUser = new CGlobalUser();
		$objGlobalUser->setCid( $this->getCid() );
		$objGlobalUser->setPropertyId( $this->getPropertyId() );
		$objGlobalUser->setWorksId( $this->getId() );
		$objGlobalUser->setGlobalUserTypeId( CGlobalUserType::CUSTOMER );
		$objGlobalUser->setUsername( $this->m_strUsername );
		$objGlobalUser->setPasswordEncrypted( $this->m_strPasswordEncrypted );
		$objGlobalUser->setLoginAttemptCount( $this->m_intLoginAttemptCount );
		$objGlobalUser->setLastLoginAttemptOn( $this->m_strLastLoginAttemptOn );
		$objGlobalUser->setIsDisabled( $this->m_intIsDisabled );

		if( false == $objGlobalUser->insert( $intCurrentUserId, $objConnectDatabase ) ) {
			$objConnectDatabase->rollback();
			$objConnectDatabase->close();
			trigger_error( 'Unable to insert customer\'s global user record. Error message:' . print_r( $objGlobalUser->getErrorMsgs(), true ), E_USER_WARNING );
			return;
		}
	}

	public function updateGlobalUserRecord( $objGlobalUser, $intCurrentUserId, $objConnectDatabase ) {
		$objGlobalUser->setUsername( $this->m_strUsername );
		$objGlobalUser->setPasswordEncrypted( $this->m_strPasswordEncrypted );
		$objGlobalUser->setLoginAttemptCount( $this->m_intLoginAttemptCount );
		$objGlobalUser->setLastLoginAttemptOn( $this->m_strLastLoginAttemptOn );
		$objGlobalUser->setIsDisabled( $this->m_intIsDisabled );

		if( false == $objGlobalUser->update( $intCurrentUserId, $objConnectDatabase ) ) {
			$objConnectDatabase->rollback();
			$objConnectDatabase->close();
			trigger_error( 'Unable to update customer\'s global user record. Error message:' . print_r( $objGlobalUser->getErrorMsgs(), true ), E_USER_WARNING );
			return;
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolForceGlobalUserSync = false ) {

        // check if name_full needs to be changed
        $arrstrChangedColumns = array_intersect( array_keys( $this->getChangedColumns() ), $this->m_arrstrCustomerNameFields );
        if( true == valArr( $arrstrChangedColumns ) ) {
            $this->setFormattedNameFull( $objDatabase );
        }
        // check if tax number is changed and taxNumberEncrypted has value but bliend index does not have value
        if( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() )
            || ( true == valStr( $this->getTaxNumberEncrypted() && false == valStr( $this->getTaxNumberBlindIndex() ) ) ) ) {
	        $this->setTaxNumberBlindIndex( $this->getEncryptedBlindIndex( $this->getTaxNumber() ) );
        }

		// call customers_update.sql function
		$boolIsValid = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolIsValid ) {
			// check if global user needs to be synced
			if( true == $boolForceGlobalUserSync || $this->m_strOriginalUsername !== $this->m_strUsername || $this->m_strOriginalPasswordEncrypted !== $this->m_strPasswordEncrypted || $this->m_intOriginalLoginAttemptCount !== $this->m_intLoginAttemptCount || strtotime( $this->m_strOriginalLastLoginAttemptOn ) !== strtotime( $this->m_strLastLoginAttemptOn ) || $this->m_intOriginalIsDisabled !== $this->m_intIsDisabled ) {
				$this->syncGlobalUserRecords( $intCurrentUserId );
			}
		}

		if( false == $this->insertOrUpdateCustomerPhoneNumbers( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return $boolIsValid;
	}

	public function simpleUpdate( $intCurrentUserId, $objDatabase ) {
		$this->setDatabase( $objDatabase );

		$objDataset = $objDatabase->createDataset();

        // check if name_full needs to be changed
        $arrstrChangedColumns = array_intersect( array_keys( $this->getChangedColumns() ), $this->m_arrstrCustomerNameFields );
        if( true == valArr( $arrstrChangedColumns ) ) {
            $this->setFormattedNameFull( $objDatabase );
        }

		if( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() )
		    || ( true == valStr( $this->getTaxNumberEncrypted() && false == valStr( $this->getTaxNumberBlindIndex() ) ) ) ) {
			$this->setTaxNumberBlindIndex( $this->getEncryptedBlindIndex( $this->getTaxNumber() ) );
		}

		$strSql = 'SELECT * ' .
		          'FROM customers_update( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlIntegrationDatabaseId() . ', ' .
		          $this->sqlLeadSourceId() . ', ' .
		          $this->sqlLeasingAgentId() . ', ' .
		          $this->sqlMessageOperatorId() . ', ' .
		          $this->sqlPaymentAllowanceTypeId() . ', ' .
		          $this->sqlMaritalStatusTypeId() . ', ' .
		          $this->sqlCompanyIdentificationTypeId() . ', ' .
		          $this->sqlPrimaryPhoneNumberTypeId() . ', ' .
		          $this->sqlSecondaryPhoneNumberTypeId() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlSecondaryNumber() . ', ' .
		          $this->sqlCompanyName() . ', ' .
		          $this->sqlNamePrefix() . ', ' .
		          $this->sqlNameFirst() . ', ' .
		          $this->sqlNameMiddle() . ', ' .
		          $this->sqlNameLast() . ', ' .
		          $this->sqlNameSuffix() . ', ' .
		          $this->sqlNameMaiden() . ', ' .
		          $this->sqlNameSpouse() . ', ' .
		          $this->sqlPrimaryStreetLine1() . ', ' .
		          $this->sqlPrimaryStreetLine2() . ', ' .
		          $this->sqlPrimaryStreetLine3() . ', ' .
		          $this->sqlPrimaryCity() . ', ' .
		          $this->sqlPrimaryStateCode() . ', ' .
		          $this->sqlPrimaryPostalCode() . ', ' .
		          $this->sqlPrimaryCountryCode() . ', ' .
		          $this->sqlPrimaryIsVerified() . ', ' .
		          $this->sqlPhoneNumber() . ', ' .
		          $this->sqlMobileNumber() . ', ' .
		          $this->sqlWorkNumber() . ', ' .
		          $this->sqlFaxNumber() . ', ' .
		          $this->sqlEmailAddress() . ', ' .
		          $this->sqlTaxNumberEncrypted() . ', ' .
		          $this->sqlTaxNumberMasked() . ', ' .
		          $this->sqlReturnedPaymentsCount() . ', ' .
		          $this->sqlBirthDate() . ', ' .
		          $this->sqlGender() . ', ' .
		          $this->sqlDlNumberEncrypted() . ', ' .
		          $this->sqlDlStateCode() . ', ' .
		          $this->sqlDlProvince() . ', ' .
		          $this->sqlIdentificationValue() . ', ' .
		          $this->sqlIdentificationExpiration() . ', ' .
		          $this->sqlNotes() . ', ' .
		          $this->sqlDontAllowLogin() . ', ' .
		          $this->sqlImportedOn() . ', ' .
		          $this->sqlExportedBy() . ', ' .
		          $this->sqlExportedOn() . ', ' .
		          $this->sqlIsOrganization() . ', ' .
		          $this->sqlAltNameFirst() . ', ' .
		          $this->sqlAltNameMiddle() . ', ' .
		          $this->sqlAltNameLast() . ', ' .
		          $this->sqlPreferredName() . ', ' .
		          $this->sqlPreferredLocaleCode() . ', ' .
		          $this->sqlNameLastMatronymic() . ', ' .
		          $this->sqlNameFull() . ', ' .
		          $this->sqlTaxIdTypeId() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlTaxNumberBlindIndex() . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update company customer record. The following error was reported.' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( false == $this->insertOrUpdateCustomerPhoneNumbers( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update company customer record. The following error was reported.' ) ) );
			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	/**
	 * Create Functions
	 */

	public function createMaintenanceRequest( $objLease, $objProperty, $objDatabase ) {

		if( false == isset ( $objDatabase ) ) return false;

		$objMaintenanceRequest = new CMaintenanceRequest();

		if( true == valObj( $objLease, 'CLease' ) ) {

			$objPropertyUnit = NULL;
			$objUnitSpace = NULL;

			if( false != valId( $objLease->getPropertyUnitId() ) ) {
				$objPropertyUnit = $objLease->getOrFetchPropertyUnit( $objDatabase );
			}

			if( false != valObj( $objPropertyUnit, 'CPropertyUnit' ) && false != valId( $objLease->getUnitSpaceId() ) ) {
				$objUnitSpace = $objPropertyUnit->fetchUnitSpaceByIdByRemovingDefaultSpaceNumber( $objLease->getUnitSpaceId(), $objDatabase );
			}

			$objMaintenanceRequest->setLeaseId( $objLease->getId() );
			$objMaintenanceRequest->setCustomerId( $this->m_intId );
			$objMaintenanceRequest->setPropertyId( $objLease->getPropertyId() );
			$objMaintenanceRequest->setLease( $objLease );
		} else {
			$objMaintenanceRequest->setPropertyId( $objProperty->getId() );
		}

		if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {
			$objMaintenanceRequest->setPropertyUnitId( $objPropertyUnit->getId() );
			$objMaintenanceRequest->setPropertyBuildingId( $objPropertyUnit->getPropertyBuildingId() );
		}

		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
			$objMaintenanceRequest->setUnitSpaceId( $objUnitSpace->getId() );
			$objMaintenanceRequest->setUnitNumber( $objUnitSpace->getUnitNumberCache() );
		}

		$objMaintenanceRequest->setCid( $this->m_intCid );

		$objMaintenanceRequest->setEmailAddress( $this->m_strUsername );

		// Set default assuming CPhoneNumberType::HOME in case of nullable PrimaryPhoneNumberTypeId field
		$strMainPhoneNumber = $this->getPhoneNumber();

		// For non nullable PrimaryPhoneNumberTypeId field
		if( true == valId( $this->getPrimaryPhoneNumberTypeId() ) ) {
			switch( $this->getPrimaryPhoneNumberTypeId() ) {
				case CPhoneNumberType::OFFICE:
					$strMainPhoneNumber = $this->getWorkNumber();
					break;

				case CPhoneNumberType::MOBILE:
					$strMainPhoneNumber = $this->getMobileNumber();
					break;

				default:
					// Default case
					break;
			}
		}

		if( true == valStr( $strMainPhoneNumber ) ) {
			$objMaintenanceRequest->setMainPhoneNumber( $strMainPhoneNumber );
			$objMaintenanceRequest->setMainPhoneNumberTypeId( $this->getPrimaryPhoneNumberTypeId() );
		}

		$objCustomerPhoneNumber = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomerPhoneNumberByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objCustomerPhoneNumber, 'CCustomerPhoneNumber' ) ) {
			$objMaintenanceRequest->setAltPhoneNumber( $objCustomerPhoneNumber->getPhoneNumber() );
			$objMaintenanceRequest->setAltPhoneNumberTypeId( $objCustomerPhoneNumber->getPhoneNumberTypeId() );
		}

		$objMaintenanceRequest->setCustomerNameFirst( $this->m_strNameFirst );
		$objMaintenanceRequest->setCustomerNameLast( $this->m_strNameLast );

		$objMaintenanceRequest->setCustomer( $this );

		$objMaintenanceRequest->setIsFloatingMaintenanceRequest( false );

		return $objMaintenanceRequest;
	}

	// This function creates an sms message object.

	public function createMessage( $objDatabase ) {

		$objMessage = new CMessage();
		$objMessage->setDefaults();

		$intMobileNumber = $this->getMobileNumber();
		if( CPhoneNumberType::MOBILE != $this->getPrimaryPhoneNumberTypeId() ) {
			$intMobileNumber = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomerPhoneNumberInfoByIdByPhoneNumberTypeIdByCid( $this->getId(), CPhoneNumberType::MOBILE, $this->getCid(), $objDatabase );
		}

		$objMessage->setCid( $this->getCid() );
		$objMessage->setMessageOperatorId( $this->getMessageOperatorId() );
		$objMessage->setPhoneNumber( $intMobileNumber );
		$objMessage->setCustomerId( $this->getId() );

		return $objMessage;
	}

	public function createUtilityAccount( $objLease ) {

		if( false == valObj( $objLease, 'CLease' ) || ( true == is_null( $objLease->getLeaseStartDate() ) ) ) {
			return false;
		}

		if( true == is_null( $objLease->getMoveInDate() ) ) $objLease->setMoveInDate( $objLease->getLeaseStartDate() );

		$objUtilityAccount = new CUtilityAccount();
		$objUtilityAccount->setDefaults();
		$objUtilityAccount->setCid( $this->getCid() );
		$objUtilityAccount->setPropertyId( $objLease->getPropertyId() );
		$objUtilityAccount->setLeaseId( $objLease->getId() );
		$objUtilityAccount->setCustomerId( $this->getId() );
		$objUtilityAccount->setPropertyUnitId( $objLease->getPropertyUnitId() );
		$objUtilityAccount->setUnitSpaceId( $objLease->getUnitSpaceId() );
		$objUtilityAccount->setUtilityAccountStatusTypeId( CUtilityAccountStatusType::CURRENT );
		$objUtilityAccount->setRemotePrimaryKey( $this->getRemotePrimaryKey() );
		$objUtilityAccount->setNameFirst( $this->getNameFirst() );
		$objUtilityAccount->setNameLast( $this->getNameLast() );

		if( true == valStr( $objLease->getMoveInDate() ) && true == valStr( $objLease->getLeaseStartDate() ) && strtotime( $objLease->getMoveInDate() ) < strtotime( $objLease->getLeaseStartDate() ) ) {
			$objUtilityAccount->setOriginalLeaseStartDate( $objLease->getMoveInDate() );
		} else {
			$objUtilityAccount->setOriginalLeaseStartDate( $objLease->getLeaseStartDate() );
		}

		$objUtilityAccount->setLeaseStartDate( $objLease->getLeaseStartDate() );
		$objUtilityAccount->setLeaseEndDate( $objLease->getLeaseEndDate() );
		$objUtilityAccount->setMoveInDate( $objLease->getMoveInDate() );
		$objUtilityAccount->setMoveOutDate( $objLease->getMoveOutDate() );

		if( true == is_null( $objUtilityAccount->getNameFirst() ) ) $objUtilityAccount->setNameFirst( 'no name' );
		if( true == is_null( $objUtilityAccount->getNameLast() ) ) $objUtilityAccount->setNameLast( 'no name' );

		return $objUtilityAccount;
	}

	public function createForm() {

		$objForm = new CForm();

		$objForm->setIpAddress( getRemoteIpAddress() );
		$objForm->setCid( $this->getCid() );
		$objForm->setCustomerId( $this->getId() );
		$objForm->setFormStatusTypeId( CFormStatusType::VIEWED );

		return $objForm;
	}

	public function createScheduledPayment( $boolRequestFromResidentPortal = false ) {

		$objScheduledPayment = new CScheduledPayment();
		$objScheduledPayment->setDefaults();

		$objScheduledPayment->setCid( $this->m_intCid );
		$objScheduledPayment->setCustomerId( $this->m_intId );
		$objScheduledPayment->setScheduledPaymentFrequencyId( CScheduledPaymentFrequency::MONTHLY );
		$objScheduledPayment->setStartMonth( date( 'n' ) );
		$objScheduledPayment->setBilltoEmailAddress( $this->m_strUsername );
		$objScheduledPayment->setBilltoNameFirst( $this->m_strNameFirst );
		$objScheduledPayment->setBilltoNameLast( $this->m_strNameLast );
		$objScheduledPayment->setBilltoStreetLine1( $this->m_strPrimaryStreetLine1 );
		$objScheduledPayment->setBilltoStreetLine2( $this->m_strPrimaryStreetLine2 );
		$objScheduledPayment->setBilltoCity( $this->m_strPrimaryCity );
		$objScheduledPayment->setBilltoStateCode( \Psi\CStringService::singleton()->strtoupper( $this->m_strPrimaryStateCode ) );
		$objScheduledPayment->setBilltoPostalCode( $this->m_strPrimaryPostalCode );
		$objScheduledPayment->setBilltoPhoneNumber( $this->m_strPhoneNumber );
		$objScheduledPayment->setSpecifyPaymentCeiling( false );

		if( true == $boolRequestFromResidentPortal ) {
			$objScheduledPayment->setIsAutoPaymentAllowed( true );
			$objScheduledPayment->setIsResidentPortalPayment( true );
			$objScheduledPayment->setStartMonth( NULL );
		}

		return $objScheduledPayment;
	}

	public function createCustomerMessage() {
		$objCustomerMessage = new CCustomerMessage();
		$objCustomerMessage->setCid( $this->getCid() );
		$objCustomerMessage->setSenderId( $this->getId() );
		$objCustomerMessage->setSenderTypeId( CCustomerMessageSenderType::CUSTOMER );

		return $objCustomerMessage;
	}

	public function createCustomerAddress( $intAddressTypeId, $objAddressReference = NULL ) {

		$objCustomerAddress = new CCustomerAddress();
		$objCustomerAddress->setAddressTypeId( $intAddressTypeId );
		$objCustomerAddress->setCid( $this->getCid() );
		$objCustomerAddress->setCustomerId( $this->getId() );

		if( false == is_null( $objAddressReference ) ) {
			$objCustomerAddress->setStreetLine1( $objAddressReference->getStreetLine1() );
			$objCustomerAddress->setStreetLine2( $objAddressReference->getStreetLine2() );
			$objCustomerAddress->setStreetLine3( $objAddressReference->getStreetLine3() );
			$objCustomerAddress->setCity( $objAddressReference->getCity() );
			$objCustomerAddress->setCounty( $objAddressReference->getCounty() );
			$objCustomerAddress->setStateCode( $objAddressReference->getStateCode() );
			$objCustomerAddress->setCountryCode( $objAddressReference->getCountryCode() );
			$objCustomerAddress->setPostalCode( $objAddressReference->getPostalCode() );

			if( true == valObj( $objAddressReference, 'CCustomerAddress' ) ) {
				$objCustomerAddress->setCompanyResidenceTypeId( $objAddressReference->getCompanyResidenceTypeId() );
				$objCustomerAddress->setMoveOutReasonListItemId( $objAddressReference->getMoveOutReasonListItemId() );
				$objCustomerAddress->setCommunityName( $objAddressReference->getCommunityName() );
				$objCustomerAddress->setReasonForLeaving( $objAddressReference->getReasonForLeaving() );
				$objCustomerAddress->setMoveInDate( $objAddressReference->getMoveInDate() );
				$objCustomerAddress->setMoveOutDate( $objAddressReference->getMoveOutDate() );
				$objCustomerAddress->setMonthlyPaymentAmount( $objAddressReference->getMonthlyPaymentAmount() );
				$objCustomerAddress->setPaymentRecipient( $objAddressReference->getPaymentRecipient() );
				$objCustomerAddress->setAddressOwnerType( $objAddressReference->getAddressOwnerType() );
				$objCustomerAddress->setIsNonUsAddress( $objAddressReference->getIsNonUsAddress() );
			}
		}

		return $objCustomerAddress;
	}

	public function createLease() {

		$objLease = new CLease();
		$objLease->setCid( $this->getCid() );

		return $objLease;
	}

	public function createLeaseCustomer( $intLeaseId ) {

		$objLeaseCustomer = new CLeaseCustomer();
		$objLeaseCustomer->setCid( $this->getCid() );
		$objLeaseCustomer->setLeaseId( $intLeaseId );
		$objLeaseCustomer->setCustomerId( $this->getId() );

		return $objLeaseCustomer;
	}

	public function createArPayment() {

		$objArPayment = new CArPayment();

		$objArPayment->setDefaults();
		$objArPayment->setCid( $this->getCid() );
		$objArPayment->setCustomerId( $this->getId() );
		$objArPayment->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
		$objArPayment->setBilltoNameFirst( $this->m_strNameFirst );
		$objArPayment->setBilltoNameLast( $this->m_strNameLast );
		$objArPayment->setBilltoPhoneNumber( $this->m_strPhoneNumber );
		$objArPayment->setBilltoEmailAddress( $this->m_strUsername );
		$objArPayment->setBilltoStreetLine1( $this->m_strPrimaryStreetLine1 );
		$objArPayment->setBilltoStreetLine2( $this->m_strPrimaryStreetLine2 );
		$objArPayment->setBilltoCity( $this->m_strPrimaryCity );
		$objArPayment->setBilltoStateCode( $this->m_strPrimaryStateCode );
		$objArPayment->setBilltoPostalCode( $this->m_strPrimaryPostalCode );
		$objArPayment->setPaymentDatetime( date( 'm/d/Y H:i s' ) );

		$objArPayment->setCcNameOnCard( $this->m_strNameFirst . ' ' . $this->m_strNameLast );
		$objArPayment->setCheckNameOnAccount( $this->m_strNameFirst . ' ' . $this->m_strNameLast );

		return $objArPayment;
	}

	public function createCustomerPaymentAccount() {

		$objCustomerPaymentAccount = new CCustomerPaymentAccount();

		$objCustomerPaymentAccount->setCid( $this->getCid() );
		$objCustomerPaymentAccount->setCustomerId( $this->getId() );
		$objCustomerPaymentAccount->setBilltoNameFirst( $this->getNameFirst() );
		$objCustomerPaymentAccount->setBilltoNameLast( $this->getNameLast() );
		$objCustomerPaymentAccount->setBilltoStreetLine1( $this->getPrimaryStreetLine1() );
		$objCustomerPaymentAccount->setBilltoStreetLine2( $this->getPrimaryStreetLine2() );
		$objCustomerPaymentAccount->setBilltoCity( $this->getPrimaryCity() );
		$objCustomerPaymentAccount->setBilltoStateCode( \Psi\CStringService::singleton()->strtoupper( $this->getPrimaryStateCode() ) );
		$objCustomerPaymentAccount->setBilltoPostalCode( $this->getPrimaryPostalCode() );
		$objCustomerPaymentAccount->setBilltoEmailAddress( $this->getUsername() );
		$objCustomerPaymentAccount->setBilltoPhoneNumber( $this->getCustomerPrimaryPhoneNumber() );
		$objCustomerPaymentAccount->setCheckNameOnAccount( $this->getNameFirst() . ' ' . $this->getNameLast() );
		$objCustomerPaymentAccount->setCcNameOnCard( $this->getNameFirst() . ' ' . $this->getNameLast() );

		return $objCustomerPaymentAccount;
	}

	public function getCustomerPrimaryPhoneNumber() {
		$objPhoneNumber = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomerPhoneNumberByCustomerIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );
		if( true === valObj( $objPhoneNumber, 'CCustomerPhoneNumber' ) ) {
			return $objPhoneNumber->getPhoneNumber();
		}
		return NULL;
	}

	public function createLeaseCustomerVehicle( $intCustomerVehicleTypeId = NULL, $intLeaseId = NULL ) {

		$objLeaseCustomerVehicle = new CLeaseCustomerVehicle();
		$objLeaseCustomerVehicle->setCid( $this->getCid() );
		$objLeaseCustomerVehicle->setCustomerId( $this->getId() );
		$objLeaseCustomerVehicle->setCustomerVehicleTypeId( $intCustomerVehicleTypeId );
		$objLeaseCustomerVehicle->setLeaseId( $intLeaseId );

		return $objLeaseCustomerVehicle;
	}

	public function sendCustomerActivateResidentPortalEmail( $objProperty, $objClient, $arrobjPropertyPreferences = NULL, $objDatabase ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$boolIsMigratedProperty 		= false;
		$boolHasEmailAddress			= false;
		$boolHasResidentPortalAccount	= false;
		$boolHasAlreadySentEmail		= false;
		$objCompanyMediaFile			= NULL;
		$objLogoMarketingMediaAssociation = NULL;

		if( true == is_null( $arrobjPropertyPreferences ) ) {
			$arrobjPropertyPreferences = $objProperty->getOrFetchPropertyPreferences( $objDatabase );
		}

		if( false == isset( $arrobjPropertyPreferences['SEND_RESIDENT_PORTAL_INVITE_EMAIL_TO_STATUS'] ) ) {
			return false;
		}

		$objPropertyGlSetting = $objProperty->getOrFetchPropertyGlSetting( $objDatabase );

		if( true == $objPropertyGlSetting->getIsArMigrationMode() ) {
			$boolIsMigratedProperty = true;
		}

		$objCustomerPortalSetting = CCustomerPortalSettings::fetchCustomCustomerPortalSettingByCustomerIdByCid( $this->getId(), $this->m_intCid, $objDatabase );

		if( false == valObj( $objCustomerPortalSetting, 'CCustomerPortalSetting' ) ) {
			return false;
		}

		if( true == valStr( $objCustomerPortalSetting->getUsername() ) ) {
			$boolHasEmailAddress = true;
		}

		if( true == valStr( $objCustomerPortalSetting->getPassword() ) ) {
			$boolHasResidentPortalAccount = true;
		}

		if( false == valObj( $this->m_objEmailDatabase, 'CDatabase' ) ) {
			$this->m_objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER );
		}

		$intSystemEmailBlockCount = CSystemEmailBlocks::fetchConflictingSystemEmailBlockCountByCidByCustomerIdByEmailBlockTypeIdByEmailAddress( $this->m_intCid, $objCustomerPortalSetting->getCustomerId(), CEmailBlockType::RESIDENT_PORTAL_INVITE_EMAIL, $objCustomerPortalSetting->getUsername(), $this->m_objEmailDatabase );

		if( 0 < $intSystemEmailBlockCount ) {
			$boolHasAlreadySentEmail = true;
		}

		if( true == $boolIsMigratedProperty || false == $boolHasEmailAddress || true == $boolHasAlreadySentEmail || true == $boolHasResidentPortalAccount ) {
			return false;
		}

		if( false == CLeaseExecutionLibrary::sendResidentPortalActivationInviteEmail( $this, $objProperty, $objClient, $objDatabase ) ) {
			return false;
		}

	}

	public function processViewInBrowser( $objSystemEmailRecipient ) {
		if( true == is_numeric( $objSystemEmailRecipient->getId() ) ) {

			if( 'production' == CConfig::get( 'environment' ) ) {
				$strBaseUrl = CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN;
			} else {
				$strBaseUrl = 'http://www.entrata.lcl';
			}

			$strViewInBrowserModule = $strBaseUrl . '/?module=scheduled_task_email&action=preview_email_content&key=';
			$strViewInBrowserParams = '&system_email_recipient_id=' . $objSystemEmailRecipient->getId();

			return $strViewInBrowserLink = $strViewInBrowserModule . urlencode( base64_encode( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strViewInBrowserParams, CONFIG_SODIUM_KEY_MESSAGE_CENTER_EMAIL ) ) );
		}
	}

	public function insertSystemEmailBlock( $objCustomerPortalSetting ) {

		$this->m_objSystemEmailBlock = new CSystemEmailBlock();
		$this->m_objSystemEmailBlock->setEmailAddress( $objCustomerPortalSetting->getUsername() );
		$this->m_objSystemEmailBlock->setEmailBlockTypeId( CEmailBlockType::RESIDENT_PORTAL_INVITE_EMAIL );
		$this->m_objSystemEmailBlock->setCid( $this->m_intCid );
		$this->m_objSystemEmailBlock->setCustomerId( $this->getId() );

		switch( NULL ) {
			default:
				if( false == $this->m_objSystemEmailBlock->validate( VALIDATE_INSERT ) || false == $this->m_objSystemEmailBlock->insert( SYSTEM_USER_ID, $this->m_objEmailDatabase ) ) {
					$this->addErrorMsgs( $this->m_objSystemEmailBlock->getErrorMsgs() );
					break;
				}
		}
	}

	public function createCustomerMedia() {

		$objCustomerMedia = new CCustomerMedia();
		$objCustomerMedia->setCid( $this->m_intCid );
		$objCustomerMedia->setCustomerId( $this->m_intId );

		return $objCustomerMedia;
	}

	public function createCustomerReferral() {

		$objCustomerReferral = new CCustomerReferral();
		$objCustomerReferral->setCustomerId( $this->getId() );
		$objCustomerReferral->setCid( $this->getCid() );

		return $objCustomerReferral;
	}

	public function createReview() {

		$objReview = new CReview();

		$objReview->setCustomerId( $this->getId() );
		$objReview->setCid( $this->getCid() );
		$objReview->setResidentApprovedOn( date( 'm/d/Y' ) );

		return $objReview;
	}

	public function createApplicant() {

		$objApplicant = new CApplicant();
		$objApplicant->setCid( $this->getCid() );
		$objApplicant->setMaritalStatusTypeId( $this->getMaritalStatusTypeId() );
		$objApplicant->setCustomerId( $this->getId() );
		$objApplicant->setOriginatingCustomerId( $this->getId() );
		$objApplicant->setCompanyName( $this->getCompanyName() );
		$objApplicant->setNamePrefix( $this->getNamePrefix() );
        $objApplicant->setNameFirst( $this->getNameFirst() );
		$objApplicant->setNameMiddle( $this->getNameMiddle() );
	    $objApplicant->setNameLast( $this->getNameLast() );
		$objApplicant->setNameSuffix( $this->getNameSuffix() );
		$objApplicant->setNameMaiden( $this->getNameMaiden() );
		$objApplicant->setFaxNumber( $this->getFaxNumber() );
		$objApplicant->setEmailAddress( $this->getEmailAddress() );
		$objApplicant->setTaxIdTypeId( $this->getTaxIdTypeId() );
		$objApplicant->setTaxNumber( $this->getTaxNumber() );
		$objApplicant->setBirthDate( $this->getBirthDate() );
		$objApplicant->setGender( $this->getGender() );
		$objApplicant->setDlNumber( $this->getDlNumber() );
		$objApplicant->setDlStateCode( $this->getDlStateCode() );
		$objApplicant->setUsername( $this->getUsername() );
		$objApplicant->setNotes( trim( $this->getNotes() ) );
		$objApplicant->setPasswordEncrypted( $this->getPassword() );
		$objApplicant->setPassword( $objApplicant->getPasswordEncrypted() );
		$objApplicant->setPasswordConfirm( $objApplicant->getPasswordEncrypted() );

		$objApplicant->setCurrentStreetLine1( $this->getPrimaryStreetLine1() );
		$objApplicant->setCurrentStreetLine2( $this->getPrimaryStreetLine2() );
		$objApplicant->setCurrentStreetLine3( $this->getPrimaryStreetLine3() );
		$objApplicant->setCurrentCity( $this->getPrimaryCity() );
		$objApplicant->setCurrentStateCode( $this->getPrimaryStateCode() );
		$objApplicant->setCurrentPostalCode( $this->getPrimaryPostalCode() );
		$objApplicant->setCurrentCountryCode( $this->getPrimaryCountryCode() );

		$objApplicant->setPreviousStreetLine1( $this->getPreviousStreetLine1() );
		$objApplicant->setPreviousStreetLine2( $this->getPreviousStreetLine2() );
		$objApplicant->setPreviousStreetLine3( $this->getPreviousStreetLine3() );
		$objApplicant->setPreviousCity( $this->getPreviousCity() );
		$objApplicant->setPreviousStateCode( $this->getPreviousStateCode() );
		$objApplicant->setPreviousPostalCode( $this->getPreviousPostalCode() );
		$objApplicant->setPreviousCountryCode( $this->getPreviousCountryCode() );

		if( true == valId( $this->getCompanyIdentificationTypeId() ) ) {
			$objApplicant->setCompanyIdentificationTypeId( $this->getCompanyIdentificationTypeId() );
		}

		if( true == valStr( $this->getIdentificationNumber() ) ) {
			$objApplicant->setIdentificationNumber( $this->getIdentificationNumber() );
		}
		if( true == valStr( $this->getStudentIdNumber() ) ) {
			$objApplicant->setStudentIdNumber( $this->getStudentIdNumber() );
		}

		if( true == valStr( $this->getIdentificationExpiration() ) ) {
			$objApplicant->setIdentificationExpiration( $this->getIdentificationExpiration() );
		}

		return  $objApplicant;
	}

	public function createCustomerSetting() {

		$objCustomerSetting = new CCustomerSetting();
		$objCustomerSetting->setCid( $this->m_intCid );
		$objCustomerSetting->setCustomerId( $this->m_intId );

		return $objCustomerSetting;
	}

	public function createCustomerSettingByDefaultOptOutOfApartmates( $intClientId, $intCustomerId, $intProductUserId ) {
		$objDate = new DateTime();
		$strDateTimeNow = $objDate->format( 'm/d/Y H:i:s' );

		$objCustomerSetting = new CCustomerSetting();
		$objCustomerSetting->setCid( $intClientId );
		$objCustomerSetting->setCustomerId( $intCustomerId );
		$objCustomerSetting->setKey( 'opt_out_of_apartmates' );
		$objCustomerSetting->setValue( '1' );
		$objCustomerSetting->setUpdatedBy( $intProductUserId );
		$objCustomerSetting->setUpdatedOn( $strDateTimeNow );
		$objCustomerSetting->setCreatedBy( $intProductUserId );
		$objCustomerSetting->setCreatedOn( $strDateTimeNow );

		return $objCustomerSetting;
	}

	public function createSmsEnrollment( $intMessageBlockTypeId = NULL ) {

		$objSmsEnrollment = new CSmsEnrollment();
		$objSmsEnrollment->setCid( $this->m_intCid );
		$objSmsEnrollment->setMessageTypeId( $intMessageBlockTypeId );
		$objSmsEnrollment->setCustomerId( $this->m_intId );
		$objSmsEnrollment->setEnrollmentDatetime( date( 'Y-m-d H:i:s' ) );

		return $objSmsEnrollment;
	}

	public function createAuthenticationLog() {

		$objAuthenticationLog = new CAuthenticationLog();
		$objAuthenticationLog->setCid( $this->getCid() );
		$objAuthenticationLog->setIpAddress( getRemoteIpAddress() );
		$objAuthenticationLog->setLoginDatetime( 'NOW()' );
		$objAuthenticationLog->setCustomerId( $this->getId() );

		return $objAuthenticationLog;
	}

	public function createSystemEmailBlock( $intEmailBlockTypeId ) {
		$strCustomerEmailAddress = ( false == valStr( $this->m_strUsername ) ) ? $this->m_strEmailAddress : $this->m_strUsername;
		$objSystemEmailBlock = new CSystemEmailBlock();
		$objSystemEmailBlock->setCid( $this->getCid() );
		$objSystemEmailBlock->setEmailBlockTypeId( $intEmailBlockTypeId );
		$objSystemEmailBlock->setEmailAddress( $strCustomerEmailAddress );
		$objSystemEmailBlock->setCustomerId( $this->getId() );
		$objSystemEmailBlock->setIsPublished( 1 );

		return $objSystemEmailBlock;
	}

	public function createScheduledEmailAddressFilter() {
		$objScheduledEmailAddressFilter = new CScheduledEmailAddressFilter();
		$objScheduledEmailAddressFilter->setCid( $this->m_intCid );
		$objScheduledEmailAddressFilter->setCustomerId( $this->m_intId );
		$objScheduledEmailAddressFilter->setIsUnsubscribed( $this->m_boolIsBlockMessageCenterEmail );

		return $objScheduledEmailAddressFilter;
	}

	public function createCustomerClub() {

		$objCustomerClub = new CCustomerClub();
		$objCustomerClub->setCid( $this->m_intCid );
		$objCustomerClub->setCustomerId( $this->m_intId );

		return $objCustomerClub;
	}

	public function createCustomerEvent() {

		$objCustomerEvent = new CCustomerEvent();
		$objCustomerEvent->setCid( $this->m_intCid );
		$objCustomerEvent->setCustomerId( $this->m_intId );

		return $objCustomerEvent;
	}

	public function createCustomerPet() {

		$objCustomerPet = new CCustomerPet();
		$objCustomerPet->setCid( $this->m_intCid );
		$objCustomerPet->setCustomerId( $this->m_intId );

		return $objCustomerPet;
	}

	public function createCustomerIncome() {

		$objCustomerIncome = new CCustomerIncome();
		$objCustomerIncome->setCid( $this->m_intCid );
		$objCustomerIncome->setCustomerId( $this->m_intId );

		return $objCustomerIncome;
	}

	public function createCustomerAsset() {

		$objCustomerAsset = new CCustomerAsset();
		$objCustomerAsset->setCid( $this->m_intCid );
		$objCustomerAsset->setCustomerId( $this->m_intId );

		return $objCustomerAsset;
	}

	public function createCustomerContact() {

		$objCustomerContact = new CCustomerContact();
		$objCustomerContact->setCid( $this->m_intCid );
		$objCustomerContact->setCustomerId( $this->m_intId );

		return $objCustomerContact;
	}

	public function createCustomerRoommateInterest() {

		$objCustomerRoommateInterest = new CCustomerRoommateInterest();
		$objCustomerRoommateInterest->setDefaults();

		$objCustomerRoommateInterest->setCid( $this->getCid() );
		$objCustomerRoommateInterest->setCustomerId( $this->getId() );

		return $objCustomerRoommateInterest;
	}

	public function createInspection() {

		$objInspection = new CInspection();
		$objInspection->setCid( $this->getCid() );
		$objInspection->setCustomerId( $this->getId() );

		return $objInspection;
	}

	public function createCustomerGuest() {

		$objCustomerGuest = new CCustomerGuest();
		$objCustomerGuest->setCid( $this->getCid() );
		$objCustomerGuest->setCustomerId( $this->getId() );

		return $objCustomerGuest;
	}

	public function createCustomerSubsidyRaceType() {

		$objCustomerSubsidyRaceType = new CCustomerSubsidyRaceType();
		$objCustomerSubsidyRaceType->setCid( $this->getCid() );
		$objCustomerSubsidyRaceType->setCustomerId( $this->getId() );

		return $objCustomerSubsidyRaceType;
	}

	public function createCustomerSubsidyDetail() {

		$objCustomerSubsidyDetail = new CCustomerSubsidyDetail();
		$objCustomerSubsidyDetail->setCid( $this->getCid() );
		$objCustomerSubsidyDetail->setCustomerId( $this->getId() );

		return $objCustomerSubsidyDetail;
	}

	public function createCustomerSubsidySpecialStatusType() {
		$objCustomerSubsidySpecialStatusType = new CCustomerSubsidySpecialStatusType();
		$objCustomerSubsidySpecialStatusType->setCid( $this->getCid() );
		$objCustomerSubsidySpecialStatusType->setCustomerId( $this->getId() );

		return $objCustomerSubsidySpecialStatusType;
	}

	public function createCustomerSubsidyDisabilityType() {

		$objCustomerSubsidyDisabilityType = new CCustomerSubsidyDisabilityType();
		$objCustomerSubsidyDisabilityType->setCid( $this->getCid() );
		$objCustomerSubsidyDisabilityType->setCustomerId( $this->getId() );

		return $objCustomerSubsidyDisabilityType;
	}

	public function createCustomerIdentificationValue() {

		$objCustomerIdentificationValue = new CCustomerIdentificationValue();
		$objCustomerIdentificationValue->setCid( $this->getCid() );
		$objCustomerIdentificationValue->setCustomerId( $this->getId() );

		return $objCustomerIdentificationValue;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCustomerContacts( $objDatabase ) {
		return CCustomerContacts::fetchCustomerContactsByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryAddress( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressByCustomerIdByAddressTypeIdByCid( $this->m_intId, CAddressType::PRIMARY, $this->getCid(), $objDatabase );
	}

	public function fetchAddressesByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressByCustomerIdByAddressTypeIdByCid( $this->getId(), $intAddressTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchAddressesByAddressTypeIds( $arrintAddressTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressesByCustomerIdByAddressTypeIdsByCid( $this->m_intId, $arrintAddressTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchViewCustomer( $objDatabase ) {
		$objViewCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
		$objViewCustomer->setIsLoggedIn( $this->m_boolIsLoggedIn );
		return $objViewCustomer;
	}

	public function fetchSimpleScheduledPayments( $objDatabase ) {
		return CScheduledPayments::fetchSimpleScheduledPaymentsByCustomerIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchScheduledPayment( $intScheduledPaymentId, $objDatabase ) {
		return CScheduledPayments::fetchScheduledPaymentByIdByCustomerIdByCid( $intScheduledPaymentId, $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchViewScheduledPaymentById( $intScheduledPaymentId, $objDatabase ) {
		if( true == is_null( $intScheduledPaymentId ) ) return NULL;
		return CScheduledPayments::fetchViewScheduledPaymentByIdByCustomerIdByCid( $intScheduledPaymentId, $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchIntegratedLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByCustomerIdWithRemotePrimaryKeyNotNullByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLeasesByIntegrationDatabaseId( $intIntegrationDatabaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByIntegrationDatabaseIdByCustomerIdWithRemotePrimaryKeyNotNullByCid( $intIntegrationDatabaseId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchProperties( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCustomerIdByCid( $this->m_intId, $this->getCid(), $objClientDatabase );
	}

	public function fetchCustomerPaymentAccountsByPaymentTypeIds( $arrintPaymentTypeIds, $objDatabase ) {

		if( false == valArr( $arrintPaymentTypeIds ) ) return [];

		$this->m_arrobjCustomerPaymentAccounts = CCustomerPaymentAccounts::fetchOrderedCustomerPaymentAccountsByCustomerIdByPaymentTypeIdsByCid( $this->getId(), $arrintPaymentTypeIds, $this->getCid(), $objDatabase );

		if( true == valArr( $this->m_arrobjCustomerPaymentAccounts ) ) {
			foreach( $this->m_arrobjCustomerPaymentAccounts as $objCustomerPaymentAccount ) {
				if( CPaymentType::ACH == $objCustomerPaymentAccount->getPaymentTypeId() ) {
					$this->m_intAchCustomerPaymentAccountCount++;
				} elseif( CPaymentType::PAD == $objCustomerPaymentAccount->getPaymentTypeId() ) {
					$this->m_intPadCustomerPaymentAccountCount++;
				} else {
					$this->m_intCcCustomerPaymentAccountCount++;
				}
			}
		}

		return $this->m_arrobjCustomerPaymentAccounts;
	}

	public function fetchAllOpenAndSomeClosedMaintenanceRequestsAfterDate( $intTimeStamp, $boolHideExternalWorkOrders, $objLease, $objDatabase, $boolIsUnitWide = false, $intPropertyId ) {

		return \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchAllOpenAndSomeClosedMaintenanceRequestsByCustomerIdByDateByPropertyIdByCid( $this->m_intId, $intTimeStamp, $boolHideExternalWorkOrders, $objLease, $intPropertyId, $this->getCid(), $objDatabase, $boolIsUnitWide );
	}

	public function fetchAllNonDeletedMaintenanceRequests( $objDatabase ) {

		return \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchAllNonDeletedMaintenanceRequestsByCustomerIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAuthentications( $objDatabase ) {

		return CCustomerAuthentications::fetchCustomerAuthenticationsByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchScheduledChargesByLeaseIds( $arrintLeaseIds, $objDatabase, $boolIsHideHistoricalRecurringCharges = false, $boolIsFetchInstallmentName = false, $boolIsUnselectedQuote = true, $boolIsResidentLedger = false, $boolIsActiveLeaseInterval = false ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesCustomerIdByLeaseIdsByCid( $this->getId(), $arrintLeaseIds, $this->getCid(), $objDatabase, $boolIsHideHistoricalRecurringCharges, $boolIsFetchInstallmentName, $boolIsUnselectedQuote, $boolIsResidentLedger, $boolIsActiveLeaseInterval );
	}

	public function fetchScheduledChargesByLeaseId( $intLeaseId, $objDatabase ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByCustomerIdByLeaseIdByCid( $this->m_intId, $intLeaseId, $this->m_intCid, $objDatabase );
	}

	public function fetchTotalScheduledChargesAmountByLeaseIds( $arrintLeaseIds, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchTotalScheduledChargesAmountCustomerIdByLeaseIdsByCid( $this->m_intId, $arrintLeaseIds, $this->m_intCid, $objDatabase );
	}

	public function fetchScheduledPayments( $objDatabase ) {
		return CScheduledPayments::fetchCustomScheduledPaymentsByCustomerIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseById( $intLeaseId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseCustomerIdByIdByCid( $this->getId(), $intLeaseId, $this->getCid(), $objDatabase );
	}

	public function fetchSingleLeaseByPropertyUnitId( $intPropertyUnitId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchSingleLeaseByCustomerIdByPropertyUnitIdByCid( $this->getId(), $intPropertyUnitId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomers( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByCustomerIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomersByLeaseStatusTypeIds( $arrintLeaseStatusTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByCustomerIdByLeaseStatusTypeIdsByCid( $this->m_intId, $arrintLeaseStatusTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomersByPropertyId( $intPropertyId, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByCustomerIdByPropertyIdByCid( $this->m_intId, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomersByLeaseIds( $arrintLeaseIds, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByCustomerIdByLeaseIdsByCid( $this->m_intId, $arrintLeaseIds, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomersByIntegrationDatabaseId( $intIntegrationDatabaseId, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByCustomerIdByIntegrationDatabaseIdByCid( $this->m_intId, $intIntegrationDatabaseId, $this->getCid(), $objDatabase );
	}

	public function fetchNonIntegratedAndActiveLeaseCustomers( $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchNonIntegratedAndActiveLeaseCustomersByCustomerIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchNonIntegratedAndActiveLeaseCustomersWithPropertyRemotePrimaryKeyNotNull( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchNonIntegratedAndActiveLeaseCustomersByCustomerIdWithPropertyRemotePrimaryKeyNotNULLByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomerByLeaseId( $intLeaseId, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $this->m_intId, $intLeaseId, $this->getCid(), $objDatabase );
	}

	public static function fetchLeaseCustomerByLeaseIdAndCustomerId( $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase );
	}

	public function fetchCustomLeaseCustomerByLeaseId( $intLeaseId, $objDatabase ) {
		// @TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCustomLeaseCustomerByCustomerIdByLeaseIdByCid( $this->m_intId, $intLeaseId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerPaymentAccountById( $intPaymentAccountId, $objDatabase ) {
		return CCustomerPaymentAccounts::fetchCustomerPaymentAccountByIdByCustomerIdByCid( $intPaymentAccountId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerPaymentAccounts( $objDatabase ) {

		$this->m_arrobjCustomerPaymentAccounts = CCustomerPaymentAccounts::fetchOrderedCustomerPaymentAccountsByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $this->m_arrobjCustomerPaymentAccounts ) ) {
			foreach( $this->m_arrobjCustomerPaymentAccounts as $objCustomerPaymentAccount ) {
				if( CPaymentType::ACH == $objCustomerPaymentAccount->getPaymentTypeId() ) {
					$this->m_intAchCustomerPaymentAccountCount++;
				} elseif( CPaymentType::PAD == $objCustomerPaymentAccount->getPaymentTypeId() ) {
					$this->m_intPadCustomerPaymentAccountCount++;
				} else {
					$this->m_intCcCustomerPaymentAccountCount++;
				}
			}
		}

		return $this->m_arrobjCustomerPaymentAccounts;
	}

	public function fetchPaginatedArPaymentsByPropertyIds( $arrintPropertyIds, $intPageNo, $intPageSize, $objArPaymentsFilter, $objDatabase ) {
		return CArPayments::fetchPaginatedArPaymentsByCustomerIdByPropertyIdsByCid( $arrintPropertyIds, $this->m_intId, $intPageNo, $intPageSize, $objArPaymentsFilter, $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedArPaymentsCountByPropertyIds( $arrintPropertyIds, $objArPaymentsFilter, $objDatabase ) {
		return CArPayments::fetchPaginatedArPaymentsCountByCustomerIdByPropertyIdsByCid( $arrintPropertyIds, $this->m_intId, $objArPaymentsFilter, $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedDistributedUtilityInvoicesByPropertyIds( $arrintPropertyIds, $objUtilityInvoiceFilter, $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchPaginatedDistributedUtilityInvoicesByCustomerIdByPropertyIds( $arrintPropertyIds, $this->m_intId, $objUtilityInvoiceFilter, $objDatabase );
	}

	public function fetchDistributedUtilityInvoicesByPropertyIds( $arrintPropertyIds, $intPrimaryCustomerId = NULL, $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchDistributedUtilityInvoicesByCidByCustomerIdByPropertyIds( $this->m_intCid, $arrintPropertyIds, $intPrimaryCustomerId, $objDatabase );
	}

	public function fetchPaginatedDistributedUtilityInvoicesCountByPropertyIds( $arrintPropertyIds, $objUtilityInvoiceFilter, $objDatabase ) {
		return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchPaginatedDistributedUtilityInvoicesCountByCustomerIdByPropertyIds( $arrintPropertyIds, $this->m_intId, $objUtilityInvoiceFilter, $objDatabase );
	}

	public function fetchApplications( $objDatabase, $boolIncludeDeletedAA = false ) {
		return CApplications::fetchApplicationsByCustomerIdByCid( $this->m_intId, $this->m_intCid, $objDatabase, $boolIncludeDeletedAA = false );
	}

	public function fetchLatestApplication( $objDatabase ) {
		return CApplications::fetchLatestApplicationByCustomerIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchApplicationsByLeaseIds( $arrintLeaseIds, $objDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		return CApplications::fetchApplicationsByCustomerIdByLeaseIdsByCid( $this->m_intId, $arrintLeaseIds, $this->getCid(), $objDatabase );
	}

	public function fetchEsignedLeaseFilesCountByApplicantIdsByLeaseIds( $arrintApplicantIds, $arrintLeaseIds, $objDatabase ) {

		return CFiles::fetchEsignedLeaseFilesCountByCustomerIdByApplicantIdsByLeaseIdsByCid( $this->getId(), $arrintApplicantIds, $arrintLeaseIds, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseAgreementIssuedApplicationByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CApplications::fetchLeaseAgreementIssuedApplicationByCustomerIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds,  $this->getCid(), $objDatabase );
	}

	public function fetchLeaseAgreementIssuedApplicant( $objDatabase ) {
		return CApplicants::fetchLeaseAgreementIssuedApplicantByCustomerIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchApplicants( $objDatabase ) {
		return CApplicants::fetchApplicantsByCustomerIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomerVehicle( $intCustomerVehicleId, $objDatabase ) {
		return CLeaseCustomerVehicles::createService()->fetchLeaseCustomerVehicleByCustomerIdByCustomerVehicleIdByCid( $this->getId(), $intCustomerVehicleId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomerVehicles( $objDatabase ) {
		return CLeaseCustomerVehicles::fetchCustomLeaseCustomerVehiclesByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchActiveLeaseCustomerVehicles( $objDatabase ) {
		return CLeaseCustomerVehicles::createService()->fetchActiveLeaseCustomerVehiclesByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerConciergeServiceByIdByPropertyId( $intConciergeServiceId, $intPropertyId, $objDatabase ) {
		return CCustomerConciergeServices::createService()->fetchCustomerConciergeServicesByIdByCustomerIdByPropertyIdByCid( $intConciergeServiceId, $intPropertyId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSimpleCustomerConciergeServicesByPropertyId( $intPropertyId, $objDatabase ) {
		return CCustomerConciergeServices::createService()->fetchSimpleCustomerConciergeServicesByCustomerIdByPropertyIdByCid( $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomerVehicleByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $objDatabase ) {
		return CLeaseCustomerVehicles::fetchLatestLeaseCustomerVehicleByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerVehiclesByCustomerVehicleTypes( $arrintCustomerVehicleTypeIds, $objDatabase ) {
		return CLeaseCustomerVehicles::fetchLeaseCustomerVehiclesByCustomerVehicleTypesByCustomerIdByCid( $arrintCustomerVehicleTypeIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerVehicleCount( $objDatabase ) {
		return CLeaseCustomerVehicles::fetchLeaseCustomerVehicleCount( 'WHERE customer_id=' . $this->getId() . 'AND  cid = ' . $this->getCid(), $objDatabase );
	}

	public function fetchLastArPaymentsByLimit( $arrintPropertyIds, $intArPaymentLimit, $objDatabase, $intLeaseId = NULL ) {
		return CArPayments::fetchLastArPaymentsByCustomerIdByLimitByCid( $arrintPropertyIds, $this->getId(), $intArPaymentLimit, $this->getCid(), $objDatabase, $intLeaseId );
	}

	public function fetchLastArPaymentsByLimitForRoommates( $intCustomerId, $arrintPropertyIds, $intLeaseId, $intCurrentPropertyId, $boolIsShowAllTypeOfPayments, $intArPaymentLimit, $objDatabase ) {
		return CArPayments::fetchLastArPaymentsByLimitByCid( $intCustomerId, $arrintPropertyIds, $intLeaseId, $intCurrentPropertyId, $boolIsShowAllTypeOfPayments, $intArPaymentLimit, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerBalance( $arrintLeaseIds, $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchCustomerBalance( $arrintLeaseIds, $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchResidentPortalLeasesByPropertyIds( $arrintPropertyIds, $objArTransactionsFilter, $objDatabase, $boolApplyDisallowLoginFromPastResidentsSetting = true, $boolShowPastLeasesWithBalancesOnly = false, $boolFetchPaymentEligibleLeasesOnly = false, $boolIsPaymentFromRp2 = false, $strUnitNumber = NULL, $intLeaseId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		// TODO - This function has around 13 parameters. Ideally we need to refactor this function and use some kind of filter. At this moment count of parameter is 10.
		$arrobjLeases = ( array ) \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByCustomerIdByPropertyIdsByLeaseStatusTypeIdsByCid( $this->m_intId, $arrintPropertyIds, CLeaseStatusType::$c_arrintResidentPortalLeaseStatusTypes, $objArTransactionsFilter, $this->m_intCid, $objDatabase, $boolApplyDisallowLoginFromPastResidentsSetting, $boolShowPastLeasesWithBalancesOnly, $boolFetchPaymentEligibleLeasesOnly, $boolIsPaymentFromRp2, $strUnitNumber, $intLeaseId );
		$arrobjLeases = rekeyObjects( 'Id', $arrobjLeases );
		return $arrobjLeases;

	}

	public function fetchDirectMeteredBillDataByPropertyIds( $arrintPropertyIds, $intPrimaryCustomerId = NULL, $objDatabase, $intUtilityAccountId = NULL ) {
		return \Psi\Eos\Utilities\CUtilityInvoices::createService()->fetchDirectMeteredBillDataByCidByCustomerIdByPropertyIds( $this->m_intCid, $arrintPropertyIds, $intPrimaryCustomerId, $objDatabase, $intUtilityAccountId );
	}

	/**
	 * @param boolShouldRemovePrimaryLease
	 */

	public function fetchLeasesByPropertyIdsWithRemotePrimaryKeyNotNull( $arrintPropertyIds, $objDatabase, $boolIsExcludeInActiveLease = false, $boolIncludeNonPrimaryLeases = false ) {

		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByCustomerIdByPropertyIdsWithRemotePrimaryKeyNotNullByCid( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase, $boolIsExcludeInActiveLease, $boolIncludeNonPrimaryLeases );
	}

	public function fetchLeasesPropertyIds( $arrintPropertyIds, $objDatabase ) {

		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesCustomerIdPropertyIdsByCid( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchLeaseCustomerVehicleById( $intCustomerVehicleId, $objDatabase ) {

		return CLeaseCustomerVehicles::fetchLeaseCustomerVehicleByIdByCid( $intCustomerVehicleId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerPetById( $intCustomerPetId, $objDatabase ) {

		return \Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetByIdByCid( $intCustomerPetId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByCustomerIdByMediaTypeIdByCid( $this->m_intId, $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFile( $intCompanyMediaFileId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerMedia( $intCompanyMediaFileId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerMedias::createService()->fetchCustomerMediaByCustomerIdMediaFileIdByCid( $this->m_intId, $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchOldCompanyMediaFileByMediaTypeIdByCompanyMediaFileId( $intCompanyMediaFileId, $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchOldCompanyMediaFileByCustomerIdByMediaTypeIdByCompanyMdiaFileIdByCid( $this->m_intId, $intMediaTypeId, $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileForEntrataByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileForEntrataByCustomerIdByMediaTypeIdByCid( $this->m_intId, $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAuthenticationByCustomerAuthenticationTypeIdByPropertyId( $intCCustomerAuthenticationTypeId, $intPropertyId, $objDatabase ) {
		return CCustomerAuthentications::fetchCustomerAuthenticationByCustomerIdByCustomerAuthenticationTypeIdByPropertyIdByCid( $this->m_intId, $intCCustomerAuthenticationTypeId, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerPortalSetting( $objDatabase ) {
		return CCustomerPortalSettings::fetchCustomCustomerPortalSettingByCustomerIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerSettings( $objDatabase ) {

		return CCustomerSettings::fetchCustomerSettingsByCustomerIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchSmsEnrollments( $objDatabase ) {

		return CSmsEnrollments::createService()->fetchSmsEnrollmentsByCidByCustomerId( $this->m_intCid, $this->m_intId, $objDatabase );
	}

	public function fetchSmsEnrollmentByMessageTypeId( $intMessageTypeId, $objDatabase ) {

		return CSmsEnrollments::createService()->fetchSmsEnrollmentByCidByCustomerIdByMessageTypeId( $this->m_intCid, $this->m_intId, $intMessageTypeId, $objDatabase );
	}

	public function fetchScheduledEmailAddressFilterByCid( $intCid, $objDatabase ) {

		return CScheduledEmailAddressFilters::createService()->fetchScheduledEmailAddressFilterByCustomerIdByCid( $this->m_intId, $intCid, $objDatabase );
	}

	public function fetchUnsubscribedScheduledEmailAddressFilterByCid( $intCid, $objDatabase ) {

		return CScheduledEmailAddressFilters::createService()->fetchUnsubscribedScheduledEmailAddressFilterByCid( $this->m_intId, $intCid, $objDatabase );
	}

	public function fetchSystemEmailBlockByEmailBlockTypeId( $intEmailBlockTypeId, $objDatabase ) {
		$strCustomerEmailAddress = ( false == valStr( $this->m_strUsername ) ) ? $this->m_strEmailAddress : $this->m_strUsername;
		$objSystemEmailBlock = CSystemEmailBlocks::fetchSystemEmailBlockByCidByCustomerIdByEmailBlockTypeIdByEmailAddress( $this->m_intCid, $this->m_intId, $intEmailBlockTypeId, $strCustomerEmailAddress, $objDatabase );
		return $objSystemEmailBlock;
	}

	public function fetchAuthenticationLogs( $objDatabase ) {
		return CAuthenticationLogs::fetchAuthenticationLogsByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerGuests( $objDatabase ) {
		return CCustomerGuests::fetchCustomerGuestsDeatilsByCustomerIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchCustomerGuestById( $intCustomerGuestId, $objDatabase ) {
		return CCustomerGuests::fetchCustomerGuestByIdByCustomerIdByCid( $intCustomerGuestId, $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function isAboveEighteenYearsOld() {

		$boolIsValid = true;

		if( true == is_null( $this->getBirthDate() )
			|| 0 == strlen( $this->getBirthDate() ) || 1 !== CValidation::checkISODate( $this->getBirthDate() ) ) {
			return false;
		}

		$boolIsValid = ( true == strtotime( $this->getBirthDate() ) <= strtotime( '- 18 years' ) ) ? true:false;

		return $boolIsValid;
	}

	// Validation

	public function validate( $strAction, $objDatabase = NULL, $objLeaseCustomer = NULL, $objProperty = NULL, $intOldCustomerTypeId = NULL, $boolIsValidateBirthDate = true, $boolIsValidateForExtension = false, $arrobjPropertyPreferences = NULL, $objLease = NULL, $boolIsValidateForSSN = true, $boolIsFromGroup = false, $boolIsEmailRequired = false, $strPhoneNumberType = NULL, $boolSkipValidateSecondaryNumber = false ) {
		$boolIsValid = true;

		$objCustomerValidator = new CCustomerValidator();
		$objCustomerValidator->setCustomer( $this );

		$boolIsValid &= $objCustomerValidator->validate( $strAction, $objDatabase, $objLeaseCustomer, $objProperty, $intOldCustomerTypeId, $boolIsValidateBirthDate, $boolIsValidateForExtension, $arrobjPropertyPreferences, $objLease, $boolIsValidateForSSN, $boolIsFromGroup, $boolIsEmailRequired, $strPhoneNumberType, $boolSkipValidateSecondaryNumber );

		return $boolIsValid;
	}

	public function fetchWebsite( $objDatabase ) {
		return CWebsites::createService()->fetchWebsiteByCustomerId( $this->m_intId, $objDatabase );
	}

	public function fetchGlobalUsers( $objConnectDatabase ) {
		return CGlobalUsers::fetchGlobalUsersByCidByWorksIdByGlobalUserTypeId( $this->getCid(), $this->getId(), CGlobalUserType::CUSTOMER, $objConnectDatabase );
	}

	public function fetchEvents( $objDatabase ) {

		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFirstUnSignedFileBySystemCodeByLeaseStatusTypeIds( $strSystemCode, $arrintLeaseStatusTypeIds, $objDatabase, $boolRequireSign = false, $boolCheckShowInPortal = false ) {
		return CFiles::fetchFirstUnSignedFileByCustomerIdByFileTypeSystemCodeByLeaseStatusTypeIdsByCid( $this->m_intId, $strSystemCode, $arrintLeaseStatusTypeIds, $this->getCid(), $objDatabase, $boolRequireSign, $boolCheckShowInPortal );
	}

	public function fetchFirstUnSignedFileByLeaseStatusTypeIds( $arrintLeaseStatusTypeIds, $objDatabase, $strOrderBy  = 'ASC' ) {
		return CFiles::fetchFirstUnSignedFileByCustomerIdByLeaseStatusTypeIdsByCid( $this->m_intId, $arrintLeaseStatusTypeIds, $this->getCid(), $objDatabase, $strOrderBy );
	}

	public function fetchArTransactions( $objDatabase ) {
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyEvents( $arrintPropertyIds, $objDatabase ) {
		return CCompanyEvents::fetchAllCompanyEventsByPropertyIdsByCid( $arrintPropertyIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function getFormattedMiniminumPaymentAmount() {
		if( 0 > $this->m_fltMiniminumPaymentAmount ) {
			return '(' . __( '{%m, 0}', [ $this->m_fltMiniminumPaymentAmount ] ) . ')';
		} else {
			return __( '{%m, 0}', [ $this->m_fltMiniminumPaymentAmount ] );
		}
	}

	// Here we are calculating resident's minimum payment amount while making a payment through RP

	public function calculateMinimumPaymentAmount( $objLease, $objDatabase, $objLeaseCustomer = NULL, $arrobjPropertyPreferences = NULL, $fltMinimumAmountResidentShouldPay = NULL, $boolIsPaymentFromRpPremium = false ) {
		( NULL != $fltMinimumAmountResidentShouldPay ) ? $this->setMiniminumPaymentAmount( $fltMinimumAmountResidentShouldPay ) : $this->setMiniminumPaymentAmount( $objLease->getMiniminumPaymentAmount() );
		$objPropertyPreferencePPAO	= getArrayElementByKey( 'PARTIAL_PAYMENT_ALLOWANCE_OVERRIDE', $arrobjPropertyPreferences );
		$objPropertyPreferenceWPBTLD = getArrayElementByKey( 'WAIVE_PAYMENT_FROM_BEGIN_TILL_LATE_DAY', $arrobjPropertyPreferences );
		$objPropertyPreferenceMAPP	= getArrayElementByKey( 'PARTIAL_PAYMENT_OVERRIDES_DIFFERENT_LAST_NAMES_EXIST', $arrobjPropertyPreferences );
		$objPropertyPreferenceCURRENT = getArrayElementByKey( 'CURRENT_RESIDENT_MUST_PAY_BALANCE_IN_FULL', $arrobjPropertyPreferences );
		$objPropertyPreferenceRentCycleBeginDate = getArrayElementByKey( 'RENT_CYCLE_BEGIN_DAY', $arrobjPropertyPreferences );
		$objPropertyPreferenceRntCycleEndDate = getArrayElementByKey( 'RENT_LATE_DAY', $arrobjPropertyPreferences );
		$objPropertyPreferenceBalancePaymentRequirement = getArrayElementByKey( 'BALANCE_PAYMENT_REQUIREMENT', $arrobjPropertyPreferences );

		$intPropertyRentCycleBeginDay = 25;
		$arrobjLeaseCustomers 	= $objLease->fetchLeaseCustomers( $objDatabase );
		$arrintCustomerIds 		= [];

		if( true == valObj( $objPropertyPreferenceRentCycleBeginDate, 'CPropertyPreference' ) && true == valId( $objPropertyPreferenceRentCycleBeginDate->getValue() ) ) {
			$intPropertyRentCycleBeginDay = $objPropertyPreferenceRentCycleBeginDate->getValue();
		}

		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && true == valArr( $arrobjLeaseCustomers ) ) {

			foreach( $arrobjLeaseCustomers as $objTempLeaseCustomer ) {
				if( $objLeaseCustomer->getLeaseStatusTypeId() == $objTempLeaseCustomer->getLeaseStatusTypeId() && ( CCustomerType::PRIMARY == $objTempLeaseCustomer->getCustomerTypeId() || CCustomerType::RESPONSIBLE == $objTempLeaseCustomer->getCustomerTypeId() ) ) {
					$arrintCustomerIds[] = $objTempLeaseCustomer->getCustomerId();
				}
			}
		}

		if( true === valObj( $objLease, 'CLease' ) && true == valObj( $objPropertyPreferenceBalancePaymentRequirement, 'CPropertyPreference' ) ) {
			if( true == in_array( $objPropertyPreferenceBalancePaymentRequirement->getValue(), [ self::PROPERTY_PREF_BAL_PAY_REQ_CHARGES_MUST_BE_PAID_IN_FULL, self::PROPERTY_PREF_BAL_PAY_REQ_CURR_BAL_MUST_BE_PAID_IN_FULL ] ) ) {
				if( true == valObj( $objPropertyPreferenceWPBTLD, 'CPropertyPreference' ) && 1 == $objPropertyPreferenceWPBTLD->getValue() && true == valObj( $objPropertyPreferenceRentCycleBeginDate, 'CPropertyPreference' ) && $objPropertyPreferenceRentCycleBeginDate->getValue() <= date( 'j' ) && true == valObj( $objPropertyPreferenceRntCycleEndDate, 'CPropertyPreference' ) && date( 'j' ) < $objPropertyPreferenceRntCycleEndDate->getValue() ) {
					$this->setMiniminumPaymentAmount( 0 );
				} elseif( true == valObj( $objPropertyPreferenceCURRENT, 'CPropertyPreference' ) && 1 == $objPropertyPreferenceCURRENT->getValue() && true == in_array( $objLease->getLeaseStatusTypeId(), [ CLeaseStatusType::FUTURE, CLeaseStatusType::APPLICANT, CLeaseStatusType::PAST ] ) ) {
					$this->setMiniminumPaymentAmount( 0 );
				} elseif( true == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) && 1 == $objPropertyPreferencePPAO->getValue() && 1 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {
					$this->setMiniminumPaymentAmount( 0 );
				} elseif( true == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) && 2 == $objPropertyPreferencePPAO->getValue() && 1 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {

					if( true == valObj( $objPropertyPreferenceMAPP, 'CPropertyPreference' ) && 1 == $objPropertyPreferenceMAPP->getValue() ) {
						$arrobjCustomers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIdsByCid( $arrintCustomerIds, $this->getCid(), $objDatabase );
						foreach( $arrobjCustomers as $objCustomer ) {
							$arrstrLastNames[\Psi\CStringService::singleton()->strtolower( trim( $objCustomer->getNameLast() ) )] = $objCustomer->getNameLast();
						}
						if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrLastNames ) ) {
								$this->setMiniminumPaymentAmount( $objLease->getBalance() );
						} elseif( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrLastNames ) ) {
								$this->setMiniminumPaymentAmount( $objLease->getBalance() / \Psi\Libraries\UtilFunctions\count( $arrstrLastNames ) );
						}
					} elseif( 0 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {
						$intNumberOfPartnersRemaining = $this->getNumberOfCustomersToDividePayment( $objLease, $objDatabase, $objPropertyPreferenceBalancePaymentRequirement, $arrintCustomerIds, $intPropertyRentCycleBeginDay, \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) );
						// same resident is making a payment again (who already made a payment in rent cycle) so partial payment is allowed for him now
						$fltMinimumPaymentAmount = ( float ) ( 0 == $intNumberOfPartnersRemaining )? 0 : $objLease->getBalance() / $intNumberOfPartnersRemaining;
						$this->setMiniminumPaymentAmount( $fltMinimumPaymentAmount );
					}
					$fltRepaymentBalance = $objLease->getTotalRepaymentCurrentAmountDue( $objDatabase );
					if( true == valId( $fltRepaymentBalance ) ) {
						$this->setMiniminumPaymentAmount( $fltRepaymentBalance );
					}
				} else {
					$fltRepaymentBalance = $objLease->getTotalRepaymentCurrentAmountDue( $objDatabase );
					$this->setMiniminumPaymentAmount( ( true == valId( $fltRepaymentBalance ) ) ? $fltRepaymentBalance : $objLease->getBalance() );
				}
				return true;
			} elseif( self::PROPERTY_PREF_BAL_PAY_REQ_RENT_BAL_MUST_BE_PAID_IN_FULL == $objPropertyPreferenceBalancePaymentRequirement->getValue() ) {
				// Rent Balance Must Be Paid in Full (All non-repayment rent charges + repayments.current_amount_due)
				$objArTransactionFilter = new CArTransactionsFilter();
				$objArTransactionFilter->setDefaults( true );
				$objArTransactionFilter->setBalancePaymentRequirement( $objPropertyPreferenceBalancePaymentRequirement->getValue() );
				$objArTransactionFilter->setShowOutstandingOnly( true );
				$objArTransactionFilter->setHideCreditBalances( false );
				if( true == $boolIsPaymentFromRpPremium ) {
					$objArTransactionFilter->setDefaultLedgerFilterIds( [ CDefaultLedgerFilter::RESIDENT ] );
				}
				$arrobjArTransactions = ( array ) $objLease->fetchArTransactions( $objArTransactionFilter, $objDatabase );
				$fltMinimumAmountToPay = 0.00;
				foreach( $arrobjArTransactions as $objArTransaction ) {
					if( ( 2 == ( int ) $objArTransaction->getArCodeTypeId() || 0 > $objArTransaction->getTransactionAmount() )
						&& false == valId( $objArTransaction->getRepaymentId() ) ) {
						$fltMinimumAmountToPay += ( float ) $objArTransaction->getTransactionAmountDue();
					}
				}

				if( true == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) && ( 1 == $objPropertyPreferencePPAO->getValue() || 2 == $objPropertyPreferencePPAO->getValue() ) && 1 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {
					$intNumberOfPartnersRemaining = $this->getNumberOfCustomersToDividePayment( $objLease, $objDatabase, $objPropertyPreferenceBalancePaymentRequirement, $arrintCustomerIds, $intPropertyRentCycleBeginDay, \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) );
					$fltMinimumAmountToPay = ( float ) ( 0 == $intNumberOfPartnersRemaining )? 0 : $fltMinimumAmountToPay / $intNumberOfPartnersRemaining;
					$this->setMiniminumPaymentAmount( $fltMinimumAmountToPay );
				} else {
					$this->setMiniminumPaymentAmount( $fltMinimumAmountToPay );
				}

				$fltRepaymentBalance = $objLease->getTotalRepaymentCurrentAmountDue( $objDatabase );
				if( true == valId( $fltRepaymentBalance ) ) {
					$this->setMiniminumPaymentAmount( $fltRepaymentBalance );
				}

				return true;
			}
		}

		if( true == valObj( $objPropertyPreferenceCURRENT, 'CPropertyPreference' ) && 1 == $objPropertyPreferenceCURRENT->getValue() && CLeaseStatusType::CURRENT != $objLeaseCustomer->getLeaseStatusTypeId() && CLeaseStatusType::NOTICE != $objLeaseCustomer->getLeaseStatusTypeId() ) {
			$this->setMiniminumPaymentAmount( 0 );
		}

		if( true == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) && 1 > ( int ) $objPropertyPreferencePPAO->getValue() ) {
			return true;
		}

		$objLease->setLeaseCustomers( NULL );
		$arrobjLeaseCustomers 	= $objLease->fetchLeaseCustomers( $objDatabase );

		if( true == valObj( $objPropertyPreferenceMAPP, 'CPropertyPreference' ) && 1 == ( int ) $objPropertyPreferenceMAPP->getValue() ) {

			$arrobjCustomers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIdsByCid( $arrintCustomerIds, $this->getCid(), $objDatabase );
			$arrstrLastNames = [];
			if( true == valObj( $arrobjCustomers, 'CCustomers' ) ) {
				foreach( $arrobjCustomers as $objCustomer ) {
					$arrstrLastNames[\Psi\CStringService::singleton()->strtolower( trim( $objCustomer->getNameLast() ) )] = $objCustomer->getNameLast();
				}
			}

			if( 2 > \Psi\Libraries\UtilFunctions\count( $arrstrLastNames ) ) return true;
		}

		$fltBalance 				= ( float ) ( NULL != $fltMinimumAmountResidentShouldPay ) ? $fltMinimumAmountResidentShouldPay : $objLease->getMiniminumPaymentAmount();
		$fltScheduledChargesTotal	= ( true == $boolIsPaymentFromRpPremium ) ? ( float ) $objLease->fetchScheduledChargeTotal( $objDatabase, CArTrigger::$c_arrintRecurringArTriggers, $boolCheckIsHidden = true ) : ( float ) $objLease->fetchScheduledChargeTotal( $objDatabase, CArTrigger::$c_arrintRecurringArTriggers );

		$fltMaxBalance				= ( $fltBalance > $fltScheduledChargesTotal ) ? $fltBalance : $fltScheduledChargesTotal;
		$fltBaseRent = 0;
		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {
			$intNumberOfPartnersRemaining = $this->getNumberOfCustomersToDividePayment( $objLease, $objDatabase, $objPropertyPreferenceBalancePaymentRequirement, $arrintCustomerIds, $intPropertyRentCycleBeginDay, \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) );
			// same resident is making a payment again (who already made a payment in rent cycle) so partial payment is allowed for him now
			$fltBaseRent = ( float ) ( 0 == $intNumberOfPartnersRemaining )? 0 : $fltMaxBalance / $intNumberOfPartnersRemaining;
		}

		if( true == valObj( $objPropertyPreferenceBalancePaymentRequirement, 'CPropertyPreference' ) && 1 == $objPropertyPreferenceBalancePaymentRequirement->getValue() && ( false == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) || 1 != $objPropertyPreferencePPAO->getValue() ) ) {
			( $fltBalance > $fltScheduledChargesTotal ) ? $this->setMiniminumPaymentAmount( $fltBalance ) : $this->setMiniminumPaymentAmount( $fltScheduledChargesTotal );
			if( true == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) && 1 < $objPropertyPreferencePPAO->getValue() ) {
				$this->setMiniminumPaymentAmount( $fltBaseRent );
				$fltRepaymentBalance = $objLease->getTotalRepaymentCurrentAmountDue( $objDatabase );
				if( true == valId( $fltRepaymentBalance ) ) {
					$this->setMiniminumPaymentAmount( $fltRepaymentBalance );
				}
			}
		} elseif( true == valObj( $objPropertyPreferencePPAO, 'CPropertyPreference' ) && 1 == $objPropertyPreferencePPAO->getValue() ) {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintCustomerIds ) ) {
				$this->setMiniminumPaymentAmount( 0 );
			}
		} elseif( true == valObj( $objPropertyPreferenceWPBTLD, 'CPropertyPreference', 'Value', 1 ) ) {
			$this->setMiniminumPaymentAmount( $fltMaxBalance );
			$fltRepaymentBalance = $objLease->getTotalRepaymentCurrentAmountDue( $objDatabase );
			if( true == valId( $fltRepaymentBalance ) ) {
				$this->setMiniminumPaymentAmount( $fltRepaymentBalance );
			}
		} else {
			if( $fltBaseRent < $this->getMiniminumPaymentAmount() ) {
				$this->setMiniminumPaymentAmount( $fltBaseRent );
				$fltRepaymentBalance = $objLease->getTotalRepaymentCurrentAmountDue( $objDatabase );
				if( true == valId( $fltRepaymentBalance ) ) {
					$this->setMiniminumPaymentAmount( $fltRepaymentBalance );
				}
			}
		}

		return true;
	}

	// This function clears all the sensitive RP data of the customer.
	// This was added specifically to handle the change in the name  from Integration

	public function clearSensitiveResidentPortalData( $intCompanyUserId, $objDatabase ) {
		$boolIsValid = true;

		// Set Password to NULL
		$objCustomerPortalSetting = $this->fetchCustomerPortalSetting( $objDatabase );

		if( true == valObj( $objCustomerPortalSetting, 'CCustomerPortalSetting' ) && true == valStr( $objCustomerPortalSetting->getPassword() ) ) {
			$objCustomerPortalSetting->setPassword( NULL );
			$this->setPassword( NULL );

			$boolIsValid = $objCustomerPortalSetting->validate( VALIDATE_UPDATE );

			if( false == $boolIsValid || false == $objCustomerPortalSetting->update( $intCompanyUserId, $objDatabase ) ) {
				trigger_error( 'Failed to update Customer Portal Setting Id ' . $objCustomerPortalSetting->getId(), E_USER_WARNING );

				return false;
			}

			$strSubject = 'ResidentPortal upgraded -- You must re-enroll.';
			$strMessage = 'Due to recent upgrades, your ResidentPortal(TM) account has been closed.  Next time you visit the ResidentPortal(TM), please begin by clicking "Enroll."  Thank you and sorry for the inconvenience.';

			$this->sendGenericCustomerEmailNotification( $strSubject, $strMessage, $objDatabase, NULL, NULL );
		}

		// Delete ScheduledPayments
		$arrobjScheduledPayments = $this->fetchScheduledPayments( $objDatabase );

		if( true == valArr( $arrobjScheduledPayments ) ) {

			foreach( $arrobjScheduledPayments as $objScheduledPayment ) {

				$boolIsValid = $objScheduledPayment->validate( VALIDATE_DELETE );

				if( false == $boolIsValid || false == $objScheduledPayment->delete( $intCompanyUserId, $objDatabase ) ) {
					trigger_error( 'Failed to Delete Scheduled Payment Id ' . $objScheduledPayment->getId(), E_USER_WARNING );
					return false;
				}

				$objProperty = $objScheduledPayment->getOrFetchProperty( $objDatabase );
				$arrstrPaymentNotificationEmails = $objProperty->fetchPaymentNotificationEmails( $objDatabase );

				if( false == valArr( $arrstrPaymentNotificationEmails ) ) $arrstrPaymentNotificationEmails = [];

				$arrstrResidentPaymentNotificationEmails = [];

				if( 0 < strlen( trim( $objScheduledPayment->getBilltoEmailAddress() ) ) ) {

					if( false == is_null( $objScheduledPayment->getBilltoNameFirst() ) && false == is_null( $objScheduledPayment->getBilltoNameLast() ) ) {
						$arrstrResidentPaymentNotificationEmails[] = $objScheduledPayment->getBilltoNameFirst() . ' ' . $objScheduledPayment->getBilltoNameLast() . '<' . ( string ) $objScheduledPayment->getBilltoEmailAddress() . '>';

					} else {
						$arrstrResidentPaymentNotificationEmails[] = ( string ) $objScheduledPayment->getBilltoEmailAddress();
					}
				}

				$strSubject = 'Your recurring payment has been cancelled';

				if( 0 < $objScheduledPayment->getPaymentAmount() ) {
					$strMessage = $objScheduledPayment->getBilltoNameFull() . ':  As a result of recent upgrades to our ResidentPortal(TM), your recurring payment for the amount of $' . number_format( $objScheduledPayment->getPaymentAmount(), 2 ) . ', billed to account ' . $objScheduledPayment->getCheckAccountNumberMasked() . $objScheduledPayment->getCcCardNumberMasked() . ' was deleted.  If you would like to continue to have your rent billed automatically each month, please re-enroll in the ResidentPortal(TM) and recreate your recurring payment.  We apologize for the inconvenience.';

				} else {
					$strMessage = $objScheduledPayment->getBilltoNameFull() . ':  As a result of recent upgrades to our ResidentPortal(TM), your recurring payment, billed to account ' . $objScheduledPayment->getCheckAccountNumberMasked() . $objScheduledPayment->getCcCardNumberMasked() . ' was deleted.  If you would like to continue to have your rent billed automatically each month, please re-enroll in the ResidentPortal(TM) and recreate your recurring payment.  We apologize for the inconvenience.';
				}

				if( true == valArr( $arrstrResidentPaymentNotificationEmails ) ) {
					$boolIsResident = true;
					$objScheduledPayment->sendGenericScheduledPaymentEmail( $arrstrResidentPaymentNotificationEmails, $strSubject, $strMessage, $boolIsResident, $objDatabase, true );
				}

				if( true == valArr( $arrstrPaymentNotificationEmails ) ) {
					$boolIsResident = false;
					$objScheduledPayment->sendGenericScheduledPaymentEmail( $arrstrPaymentNotificationEmails, $strSubject, $strMessage, $boolIsResident, $objDatabase, true );
				}
			}
		}

		// Delete Stored Billing Info
		$arrobjCustomerPaymentAccounts = $this->fetchCustomerPaymentAccounts( $objDatabase );

		if( true == valArr( $arrobjCustomerPaymentAccounts ) ) {

			foreach( $arrobjCustomerPaymentAccounts as $objCustomerpaymentAccount ) {

				$objCustomerpaymentAccount->setDeletedBy( $intCompanyUserId );
				$objCustomerpaymentAccount->setDeletedOn( 'NOW()' );

				$boolIsValid = $objCustomerpaymentAccount->validate( VALIDATE_DELETE, $objDatabase );

				if( false == $boolIsValid || false == $objCustomerpaymentAccount->update( $intCompanyUserId, $objDatabase ) ) {
					trigger_error( 'Failed to Delete Customer Payment Account Id ' . $objCustomerpaymentAccount->getId(), E_USER_WARNING );
					return false;
				}
			}
		}

		return true;
	}

	public function export( $objProperty, $objDatabase, $objLease = NULL, $objApplication = NULL ) {
		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) {
			return true;
		}

		if( true == valArr( $this->getInsurancePolicies() ) && ( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::YARDI_RPORTAL == $objIntegrationDatabase->getIntegrationClientTypeId() ) ) {
			return $this->exportInsurance( $this, $objLease, $objProperty, $objDatabase );
		}

		if( true == valArr( $this->getInsurancePolicies() ) || ( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::YARDI_RPORTAL == $objIntegrationDatabase->getIntegrationClientTypeId() ) ) {

			if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::YARDI, CIntegrationClientType::MRI, CIntegrationClientType::AMSI, CIntegrationClientType::YARDI_RPORTAL, CIntegrationClientType::EMH_ONE_WAY ] ) ) {
				return true;
			}
		}

		if( true == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::EMH_ONE_WAY ] ) ) {

			$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->m_intCid, CIntegrationService::UPDATE_OCCUPANCY_STATUS, SYSTEM_USER_ID, $objDatabase );
			$objGenericWorker->setLease( $objLease );
			$objGenericWorker->setProperty( $objProperty );
			$objGenericWorker->setDatabase( $objDatabase );
			return $objGenericWorker->process();
		} else {
			$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->m_intCid, CIntegrationService::UPDATE_CUSTOMER, SYSTEM_USER_ID, $objDatabase );
			$objGenericWorker->setCustomer( $this );
			$objGenericWorker->setProperty( $objProperty );
			$objGenericWorker->setLease( $objLease );
			$objGenericWorker->setApplication( $objApplication );
			$objGenericWorker->setDatabase( $objDatabase );

			return $objGenericWorker->process();
		}
	}

	public function importCustomersAdditionalData( $arrmixFilesRemotePrimaryKeys, $objProperty, $objDatabase, $objClient, $objLease = NULL, $objApplication = NULL ) {

		$boolIsValid = false;

		$this->loadObjectStorageGateway();

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->m_intCid, CIntegrationService::RETRIEVE_CUSTOMERS_ADDL_DATA, SYSTEM_USER_ID, $objDatabase );
		$objGenericWorker->setCustomer( $this );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setLease( $objLease );
		$objGenericWorker->setApplication( $objApplication );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setFilesRemotePrimaryKeys( $arrmixFilesRemotePrimaryKeys );
		$objGenericWorker->setClient( $objClient );
		$objGenericWorker->setObjectStorageGateway( $this->m_objObjectStorageGateway );

		$boolIsValid = $objGenericWorker->process();

		return $boolIsValid;
	}

	public function sendGenericCustomerEmailNotification( $strSubject, $strMessage, $objDatabase, $objProperty = NULL, $objClient = NULL, $objEmailDatabase = NULL ) {
		if( false == valStr( $this->m_strEmailAddress ) ) return true;

		$objPropertyEmailRule	= NULL;
		$intPropertyId			= ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->getId() : $this->getPropertyId();

		if( true == is_numeric( $intPropertyId ) ) {
			$objPropertyEmailRule = CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::CUSTOMER_PROFILE_UPDATE, $intPropertyId, $objDatabase );
		}

		// Load HTML & Text Versions of Emails from Smarty Template
		$strHtmlEmailOutput = $this->buildHtmlGenericCustomerEmailNotification( $strSubject, $strMessage, $objProperty, $objClient, $objDatabase, $objPropertyEmailRule );
		$strTextEmailOutput = strip_tags( $strHtmlEmailOutput );
		$arrstrEmailAddress = CEmail::createEmailArrayFromString( $this->m_strEmailAddress );

		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setToEmailAddress( $this->m_strEmailAddress );
		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		$objSystemEmail->setSubject( $strSubject );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::CUSTOMER_PROFILE_UPDATE );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setPropertyId( '' );
		$objSystemEmail->setCustomerId( $this->getId() );

		$objSystemEmail = CSystemEmailLibrary::processSystemEmailByPropertyEmailRules( $objSystemEmail, $objPropertyEmailRule );

		if( false == valObj( $objSystemEmail, 'CSystemEmail' ) ) return true;

		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			$objEmailDatabase = $this->loadEmailDatabase();
		}

		if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
			$objMimeEmail = new CHtmlMimeMail();
			$objMimeEmail->setFrom( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
			$objMimeEmail->setText( $strTextEmailOutput );
			$objMimeEmail->setHTML( $strHtmlEmailOutput );
			$objMimeEmail->setSubject( $strSubject );
			return $objMimeEmail->send( $arrstrEmailAddress );
		}

		return true;
	}

	public function buildHtmlGenericCustomerEmailNotification( $strSubject, $strMessage, $objProperty = NULL, $objClient = NULL, $objDatabase = NULL, $objPropertyEmailRule = NULL ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation		= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_WORKS, false );

		$objSmarty->assign_by_ref( 'customer', $this );
		$objSmarty->assign_by_ref( 'message', $strMessage );
		$objSmarty->assign_by_ref( 'subject', $strSubject );

		$objSmarty->assign_by_ref( 'marketing_media_association',	$objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file',	$objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'media_library_uri',	CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'property_email_rule',	$objPropertyEmailRule );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',	CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		// Display Template
		$strTextContent = $objSmarty->nestedFetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/generic_email_body.tpl', PATH_PHP_INTERFACES . 'Templates/Common/layouts/generic_email.tpl' );

		return $strTextContent;
	}

	public function sendCreateCustomerPasswordLink( $strSecureBaseName, $objProperty = NULL, $objClient = NULL, $objDatabase, $objEmailDatabase, $boolIsRequiredNewVersion = false, $boolIsMultiClient = false, $boolIsClientSpecificUpdatePassword = false, $boolIsTenant = false, $objConnectDatabase = NULL ) {
		$arrmixExtraMetaData = [
			'lease_customers'  => [ 'is_required_new_version' => $boolIsRequiredNewVersion, 'is_tenant' => $boolIsTenant, 'is_send_to_group' => false ],
			'properties'       => [ 'is_required_new_version' => $boolIsRequiredNewVersion, 'is_customer_password_reset' => true, 'is_tenant' => $boolIsTenant, 'is_send_to_group' => false ]
		];

		if( true == $boolIsRequiredNewVersion ) {
			$strTemporaryPassword = $this->generatePassword();
			if( false === valObj( $objConnectDatabase, CDatabase::class ) ) {
				$objConnectDatabase = \Psi\Eos\Connect\CDatabases::createService()->createConnectDatabase( false );
			}
			if( true == $boolIsClientSpecificUpdatePassword ) {
				$arrobjGlobalUsers = ( array ) \Psi\Eos\Connect\CGlobalUsers::createService()->fetchGlobalUsersByUsernameByGlobalUserTypeIdByCid( $this->m_strUsername, CGlobalUserType::CUSTOMER, $objClient->getId(), $objConnectDatabase );
			} else {
				$arrobjGlobalUsers = ( array ) \Psi\Eos\Connect\CGlobalUsers::createService()->fetchGlobalUsersByUsernameByGlobalUserTypeId( $this->m_strUsername, CGlobalUserType::CUSTOMER, $objConnectDatabase );
			}

			if( 0 == \Psi\Libraries\UtilFunctions\count( $arrobjGlobalUsers ) ) {
				trigger_error( 'Global user not found in connect db : ' . ' & User Name : ' . $this->m_strUsername . ' & Client Id : ' . ( int ) $objClient->getId() . ' & Customer Id : ' . $this->getId(), E_USER_WARNING );
				return false;
			}

			if( false == valObj( $objDatabase, 'CDatabase' ) || false == ( bool ) $objDatabase->getIsOpen() ) {
				$objDatabase = $objClient->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER, $objConnectDatabase );
			}
			foreach( $arrobjGlobalUsers as $objGlobalUser ) {
				$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $objGlobalUser->getCid(), $this->loadAdminDatabase() );
				$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $objGlobalUser->getWorksId(), $objGlobalUser->getCid(), $objDatabase );
				if( true == valObj( $objCustomer, __CLASS__ ) ) {
					$objCustomer->setPassword( CGlobalUser::getEncryptedPassword( $strTemporaryPassword ) );
					$objCustomer->setPasswordExpiration( 'NOW()' );
					if( false == $objCustomer->update( SYSTEM_USER_ID, $objDatabase ) ) {
						$this->addErrorMsg( new \CErrorMsg( NULL, NULL, 'Failed to update temporary password for customer :' . $this->getId(), E_USER_WARNING, NULL ) );
					}
				}
			}

			$arrmixExtraMetaData['lease_customers'] = array_merge( $arrmixExtraMetaData['lease_customers'], [ 'temporary_password' => $strTemporaryPassword ] );
			$arrmixExtraMetaData['properties']      = array_merge( $arrmixExtraMetaData['properties'], [ 'is_required_address' => $boolIsMultiClient ] );
		} else {
			if( false == valObj( $objEmailDatabase, CDatabase::class ) ) {
				$objEmailDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER );
			}
			$objSystemEmail = new CSystemEmail();
			$objCrypto = \Psi\Libraries\Cryptography\CCrypto::createService();
			$strEncodedUrlParam = urlencode( $objCrypto->encrypt( $this->getId(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
			$strCreateCustomerPasswordLink = $strSecureBaseName . 'resident_portal/?module=forgot_password&action=create_customer_password&' . urlencode( 'customer[referral_id]' ) . '=' . base64_encode( $objSystemEmail->fetchNextId( $objEmailDatabase ) ) . '&' . urlencode( 'customer[key] ' ) . '=' . $strEncodedUrlParam;

			if( true == valObj( $objProperty, CProperty::class ) ) {

				if( true == $boolIsTenant ) {
					$objPropertyPreference = $objProperty->fetchPropertyPreferenceByKey( 'ENABLE_TENANT_PORTAL', $objDatabase );
					if( true == valObj( $objPropertyPreference, CPropertyPreference::class ) ) {
						$strCreateCustomerPasswordLink = CONFIG_HOST_PREFIX . 'tenants' . CConfig::get( 'entrata_domain_postfix' ) . '/auth/reset-password';
					}
				} else {
					$boolIsRp40Enabled = $objProperty->getIsRp40EnabledOnCurrentWebsite( $strSecureBaseName, $objDatabase );
					if( true == $boolIsRp40Enabled ) {
						$strCreateCustomerPasswordLink = $strSecureBaseName . 'auth/reset-password';
					}
				}
			}
			$arrmixExtraMetaData['lease_customers'] = array_merge( $arrmixExtraMetaData['lease_customers'], [ 'customer_password_link' => $strCreateCustomerPasswordLink, 'system_email_id' => $objSystemEmail->getId() ] );
			$arrmixExtraMetaData['properties']      = array_merge( $arrmixExtraMetaData['properties'], [ 'is_required_address' => true ] );
		}

		return $this->createCustomerPasswordResetSystemMessage( $objClient, $arrmixExtraMetaData, $objDatabase );
	}

	public function buildHtmlCreateCustomerPasswordLinkEmailContent( $strSecureBaseName, $objProperty = NULL, $objClient = NULL, $objDatabase = NULL, $objSystemEmail = NULL, $objPropertyEmailRule = NULL, $boolIsTenant = false ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_PORTAL, false );

		$strCreateCustomerPasswordLink = $strSecureBaseName . 'resident_portal/?module=forgot_password&action=create_customer_password&' . urlencode( 'customer[referral_id]' ) . '=' . base64_encode( $objSystemEmail->getId() ) . '&' . urlencode( 'customer[key] ' ) . '=' . urlencode( CEncryption::encryptText( $this->getId(), CONFIG_KEY_LOGIN_PASSWORD ) );

		$strViewTermsAndConditionLink = $strSecureBaseName . 'resident_portal/?module=enroll&action=view_terms_and_conditions';

		if( true == valObj( $objProperty, 'CProperty' ) ) {

			if( true == $boolIsTenant ) {
				$objPropertyPreference = $objProperty->fetchPropertyPreferenceByKey( 'ENABLE_TENANT_PORTAL', $objDatabase );
				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
					$strCreateCustomerPasswordLink = CONFIG_HOST_PREFIX . 'tenants' . CConfig::get( 'entrata_domain_postfix' ) . '/auth/reset-password';
					$strViewTermsAndConditionLink  = CONFIG_HOST_PREFIX . 'tenants' . CConfig::get( 'entrata_domain_postfix' ) . '/app/setting/terms-and-conditions';
				}

			} else {
				$boolIsRp40Enabled = $objProperty->getIsRp40EnabledOnCurrentWebsite( $strSecureBaseName, $objDatabase );
				if( true == $boolIsRp40Enabled ) {
					$strCreateCustomerPasswordLink = $strSecureBaseName . 'auth';
					$strViewTermsAndConditionLink  = $strSecureBaseName . 'app/setting/terms-and-conditions';
				}
			}

			$objMarketingMediaAssociation		= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
			$arrmixPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressWithPhoneNumberByPropertyIdByAddressTypeIdByCid( $objProperty->getId(), CAddressType::PRIMARY, $this->getCid(), $objDatabase );
			$objSmarty->assign_by_ref( 'property_name', $objProperty->getPropertyName() );
			$intNumber           = $arrmixPropertyAddress[0]['office_phone_number'];
			$intFormattedNumber = preg_replace( '/^(\d{3})(\d{3})(\d{4})$/', '$1-$2-$3', $intNumber );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		if( false == valStr( $this->getNameFirst() ) ) {
			$objCustomerTemp = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByUsernameByCid( $this->getUsername(), $this->getCid(), $objDatabase );
			$objSmarty->assign_by_ref( 'customer', $objCustomerTemp );
		} else {
			$objSmarty->assign_by_ref( 'customer', $this );
		}

		$objSmarty->assign_by_ref( 'property_address', $arrmixPropertyAddress );
		$objSmarty->assign_by_ref( 'property_number', $intFormattedNumber );
		$objSmarty->assign_by_ref( 'create_customer_password_link', $strCreateCustomerPasswordLink );
		$objSmarty->assign_by_ref( 'marketing_media_association', $objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file', $objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'path_base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'media_library_uri', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'terms_and_condition_link', $strViewTermsAndConditionLink );
		$objSmarty->assign( 'property_email_rule', $objPropertyEmailRule );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$objSmarty->assign( 'is_tenant', $boolIsTenant );

		$strHtmlRequestContent = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/terms_and_conditions/terms_and_conditions.tpl' );

		return $strHtmlRequestContent;
	}

	public function buildHtmlCreateTemporaryPasswordEmailContent( $strSecureBaseName, $objProperty = NULL, $objClient = NULL, $objDatabase = NULL, $objPropertyEmailRule = NULL, $boolIsMultiClient = false, $boolIsClientSpecificUpdatePassword = false, $strProductName = '', $arrobjTenantGlobalUsers = [] ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_PORTAL, false );
		$strViewTermsAndConditionLink = $strSecureBaseName . 'resident_portal/?module=enroll&action=view_terms_and_conditions';

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation		= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
			$arrmixPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressWithPhoneNumberByPropertyIdByAddressTypeIdByCid( $objProperty->getId(), CAddressType::PRIMARY, $this->getCid(), $objDatabase );
			$objSmarty->assign_by_ref( 'property_name', $objProperty->getPropertyName() );
			$intNumber = $arrmixPropertyAddress[0]['office_phone_number'];
			$intFormattedNumber = preg_replace( '/^(\d{3})(\d{3})(\d{4})$/', '$1-$2-$3', $intNumber );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		$strTemporaryPassword = $this->generatePassword();

		if( false == valStr( $this->getNameFirst() ) ) {
			$objCustomerTemp = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByUsernameByCid( $this->getUsername(), $this->getCid(), $objDatabase );
			$objSmarty->assign_by_ref( 'customer', $objCustomerTemp );
		} else {
			$objSmarty->assign_by_ref( 'customer', $this );
		}

		$objConnectDatabase = CDatabases::createConnectDatabase( false );

		if( false === valObj( $objConnectDatabase, 'CDatabase' ) ) {
			return false;
		}
		$arrobjGlobalUsers = [];
		if( 'ReservationHub' == $strProductName ) {
			$arrobjGroupUsers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomersByUsernameByPasswordEncrytedByCid( $this->m_strUsername, '', $objClient->getId(), $objDatabase );
			foreach( $arrobjGroupUsers as $objGroupUser ) {
				if( true == valObj( $objGroupUser, 'CCustomer' ) ) {
					$objGroupUser->setPasswordEncrypted( CGlobalUser::getEncryptedPassword( $strTemporaryPassword ) );
					$objGroupUser->setPasswordExpiration( 'NOW()' );
					if( false == $objGroupUser->update( \CUser::ID_RESERVATIONHUB, $objDatabase ) ) {
						$this->addErrorMsg( SCRIPT_NONE, 'Failed to update temporary password for customer :' . $objGroupUser->getId() );
					}
				}
			}
		} elseif( true == valArr( $arrobjTenantGlobalUsers ) ) {
			$arrobjGlobalUsers = $arrobjTenantGlobalUsers;
		} else {
			if( true == $boolIsClientSpecificUpdatePassword ) {
				$arrobjGlobalUsers = ( array ) CGlobalUsers::fetchGlobalUsersByUsernameByGlobalUserTypeIdByCid( $this->m_strUsername, CGlobalUserType::CUSTOMER, $objClient->getId(), $objConnectDatabase );
			} else {
				$arrobjGlobalUsers = ( array ) CGlobalUsers::fetchGlobalUsersByUsernameByGlobalUserTypeId( $this->m_strUsername, CGlobalUserType::CUSTOMER, $objConnectDatabase, $boolIsTenant );
			}
		}

		foreach( $arrobjGlobalUsers as $objGlobalUser ) {
			$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $objGlobalUser->getCid(), $this->loadAdminDatabase() );
			$objEntrataDatabase = $objClient->loadDatabase( CDatabaseUserType::PS_PROPERTYMANAGER, $objConnectDatabase );
			if( true == valObj( $objEntrataDatabase, 'CDatabase' ) ) {
				$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $objGlobalUser->getWorksId(), $objGlobalUser->getCid(), $objEntrataDatabase );
				if( true == valObj( $objCustomer, 'CCustomer' ) ) {
					$objCustomer->setPassword( CGlobalUser::getEncryptedPassword( $strTemporaryPassword ) );
					$objCustomer->setPasswordExpiration( 'NOW()' );
					if( false == $objCustomer->update( SYSTEM_USER_ID, $objEntrataDatabase ) ) {
						$this->addErrorMsg( SCRIPT_NONE, 'Failed to update temporary password for customer :' . $this->getId() );
					}
				}
			}
		}

		$objSmarty->assign_by_ref( 'property_address', $arrmixPropertyAddress );
		$objSmarty->assign_by_ref( 'property_number', $intFormattedNumber );
		$objSmarty->assign_by_ref( 'temporary_password', $strTemporaryPassword );
		$objSmarty->assign_by_ref( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file',	$objLogoMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'is_multi_client',	$boolIsMultiClient );

		$objSmarty->assign( 'path_base_uri', 			CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'media_library_uri',		CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'terms_and_condition_link',	$strViewTermsAndConditionLink );
		$objSmarty->assign( 'property_email_rule',		$objPropertyEmailRule );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	            $boolIsTenant ? '' : CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'rp-icon-512.png' );
		$objSmarty->assign( 'image_url_path',           CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$objSmarty->assign( 'product_name', ( $strProductName ? $strProductName : 'Resident Portal' ) );
		$objSmarty->assign( 'is_tenant', $boolIsTenant );

		$strHtmlRequestContent = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/temp_password.tpl' );

		return $strHtmlRequestContent;
	}

	public function sendCashEquivalentRecurringPaymentNotification( $objClient = NULL, $objDatabase = NULL ) {

		// Generate Email Content

		$strSubject 		= 'Cash Equivalent - Recurring Payment';
		$strMessage 		= '<h1>Warning</h1>Your payment status has been changed to Cash Equivalent, your Recurring Payment will not be processed. <br>Please make this month\'s rent payment in person at the office.';
		$strHtmlEmailOutput = $this->buildHtmlGenericCustomerEmailNotification( $strSubject, $strMessage, NULL, $objClient, $objDatabase );

		// Create, Load, and Send Email

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::AR_PAYMENT_NOTIFICATION );
		$objSystemEmail->setSubject( 'Cash Equivalent - Recurring Payment' );
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setToEmailAddress( $this->getEmailAddress() );

		$objSystemEmail->insert( SYSTEM_USER_ID, $this->loadEmailDatabase() );
	}

	public function renderViewCustomer( $objViewCustomer, $objXmlWriter ) {
		if( true == valObj( $objViewCustomer, 'CCustomer' ) ) {
			$objXmlWriter->beginElement( 'customer' );

			$objXmlWriter->addElement( 'id', $objViewCustomer->getId() );
			$objXmlWriter->addElement( 'cid', $objViewCustomer->getCid() );
			$objXmlWriter->addElement( 'lead_source_id', $objViewCustomer->getLeadSourceId() );
			$objXmlWriter->addElement( 'marital_status_type_id', $objViewCustomer->getMaritalStatusTypeId() );
			$objXmlWriter->addElement( 'leasing_agent_id', $objViewCustomer->getLeasingAgentId() );
			// $objXmlWriter->addElement( 'account_number', $objViewCustomer->getAccountNumber() );
			$objXmlWriter->addElement( 'secondary_number', $objViewCustomer->getSecondaryNumber() );
			$objXmlWriter->addElement( 'company_name', $objViewCustomer->getCompanyName() );
			$objXmlWriter->addElement( 'name_first', $objViewCustomer->getNameFirst() );
			$objXmlWriter->addElement( 'name_middle', $objViewCustomer->getNameMiddle() );
			$objXmlWriter->addElement( 'name_last', $objViewCustomer->getNameLast() );
			$objXmlWriter->addElement( 'name_suffix', $objViewCustomer->getNameSuffix() );
			$objXmlWriter->addElement( 'name_maiden', $objViewCustomer->getNameMaiden() );
			$objXmlWriter->addElement( 'name_full', $objViewCustomer->getNameFull() );
			$objXmlWriter->addElement( 'name_spouse', $objViewCustomer->getNameSpouse() );
			// $objXmlWriter->addElement( 'primary_street_line1', $objViewCustomer->getPrimaryStreetLine1());
			// $objXmlWriter->addElement( 'primary_street_line2', $objViewCustomer->getPrimaryStreetLine2());
			// $objXmlWriter->addElement( 'primary_street_line3', $objViewCustomer->getPrimaryStreetLine3());
			// $objXmlWriter->addElement( 'primary_city', $objViewCustomer->getPrimaryCity());
			// $objXmlWriter->addElement( 'primary_state_code', $objViewCustomer->getPrimaryStateCode());
			// $objXmlWriter->addElement( 'primary_postal_code', $objViewCustomer->getPrimaryPostalCode());
			// $objXmlWriter->addElement( 'primary_country_code', $objViewCustomer->getPrimaryCountryCode());
			// $objXmlWriter->addElement( 'permanent_street_line1', $objViewCustomer->getPermanentStreetLine1());
			// $objXmlWriter->addElement( 'permanent_street_line2', $objViewCustomer->getPermanentStreetLine2());
			// $objXmlWriter->addElement( 'permanent_street_line3', $objViewCustomer->getPermanentStreetLine3());
			// $objXmlWriter->addElement( 'permanent_city', $objViewCustomer->getPermanentCity());
			// $objXmlWriter->addElement( 'permanent_state_code', $objViewCustomer->getPermanentStateCode());
			// $objXmlWriter->addElement( 'permanent_postal_code', $objViewCustomer->getPermanentPostalCode());
			// $objXmlWriter->addElement( 'permanent_country_code', $objViewCustomer->getPermanentCountryCode());
			// $objXmlWriter->addElement( 'phone_number', $objViewCustomer->getPhoneNumber());
			// $objXmlWriter->addElement( 'mobile_number', $objViewCustomer->getMobileNumber());
			// $objXmlWriter->addElement( 'work_number', $objViewCustomer->getWorkNumber());
			// $objXmlWriter->addElement( 'fax_number', $objViewCustomer->getFaxNumber());
			// $objXmlWriter->addElement( 'email_address', $objViewCustomer->getEmailAddress());
			// $objXmlWriter->addElement( 'tax_number_encrypted', $objViewCustomer->getTaxNumberEncrypted());
			// $objXmlWriter->addElement( 'tax_number_masked', $objViewCustomer->getTaxNumberMasked());
			// $objXmlWriter->addElement( 'birth_date', $objViewCustomer->getBirthDate());
			// $objXmlWriter->addElement( 'gender', $objViewCustomer->getGender());
			// $objXmlWriter->addElement( 'annual_income', $objViewCustomer->getAnnualIncome());
			// $objXmlWriter->addElement( 'dl_number_encrypted', $objViewCustomer->getDlNumberEncrypted());
			// $objXmlWriter->addElement( 'dl_number_masked', $objViewCustomer->getDlNumberMasked());
			// $objXmlWriter->addElement( 'dl_state_code', $objViewCustomer->getDlStateCode());
			// $objXmlWriter->addElement( 'dl_province', $objViewCustomer->getDlProvince());
			// $objXmlWriter->addElement( 'notes', $objViewCustomer->getNotes());
			$objXmlWriter->addElement( 'username', $objViewCustomer->getUsername() );
			$objXmlWriter->addElement( 'password', $objViewCustomer->getPassword() );
			// $objXmlWriter->addElement( 'dont_allow_login', $objViewCustomer->getDontAllowLogin());
			// $objXmlWriter->addElement( 'dont_accept_payments', $objViewCustomer->getDontAcceptPayments());
			// $objXmlWriter->addElement( 'in_poor_standing', $objViewCustomer->getInPoorStanding());
			// $objXmlWriter->addElement( 'exported_by', $objViewCustomer->getExportedBy());
			// $objXmlWriter->addElement( 'exported_on', $objViewCustomer->getExportedOn());
			// $objXmlWriter->addElement( 'updated_by', $objViewCustomer->getUpdatedBy());
			// $objXmlWriter->addElement( 'updated_on', $objViewCustomer->getUpdatedOn());
			// $objXmlWriter->addElement( 'created_by', $objViewCustomer->getCreatedBy());
			$objXmlWriter->addElement( 'created_on', $objViewCustomer->getCreatedOn() );

			$objXmlWriter->endElement();
		}
	}

	/**
	 * @param $objLease
	 * @param $objDatabase
	 * @param $objPropertyPreferenceBalancePaymentRequirement
	 * @param $arrintCustomerIds
	 * @param $objPropertyPreferenceRentCycleBeginDate
	 * @param $intNumberOfPartnersRemaining
	 * @return int
	 */
	public function getNumberOfCustomersToDividePayment( $objLease, $objDatabase, $objPropertyPreferenceBalancePaymentRequirement, $arrintCustomerIds, $intPropertyRentCycleBeginDay, $intNumberOfPartnersRemaining ) {
		// check if any roommate already made a payment and if yes we dont want to consider him again while dividing amount in roommates in case of BalancePaymentRequirement->getValue() != Partial Payment Allowed
		if( true === valObj( $objLease, 'CLease' ) && true == valObj( $objPropertyPreferenceBalancePaymentRequirement, 'CPropertyPreference' ) ) {
			$objCurrentDate                           = new \DateTime( 'now' );
			$intMonth                                 = $objCurrentDate->format( 'm' );
			$intYear                                  = $objCurrentDate->format( 'Y' );
			$intPropertyRentCycleBeginDay             = valId( $intPropertyRentCycleBeginDay ) ? $intPropertyRentCycleBeginDay : 25;
			$arrintCustomersMadePaymentAfterRentCycle = [];

			$arrmixCustomerArPayments                 = ( array ) CArPayments::fetchArPaymentsByCustomerIdsByCidByCreatedOnAfterDate( $arrintCustomerIds, $objLease->getCid(), $intMonth . '/' . $intPropertyRentCycleBeginDay . '/' . $intYear, $objDatabase );
			foreach( $arrmixCustomerArPayments as $objArPaymentsOfCustomer ) {
				if( $objArPaymentsOfCustomer->getCustomerId() == $this->getId() ) {
					return 0; // same resident is making a payment again so partial payment is allowed for him.
				} else {
					$arrintCustomersMadePaymentAfterRentCycle[$objArPaymentsOfCustomer->getCustomerId()] = $objArPaymentsOfCustomer->getCustomerId();
				}
			}
			$intNumberOfPartnersRemaining -= \Psi\Libraries\UtilFunctions\count( $arrintCustomersMadePaymentAfterRentCycle );
			$intNumberOfPartnersRemaining = 0 >= $intNumberOfPartnersRemaining ? 1 : $intNumberOfPartnersRemaining;
		}

		return $intNumberOfPartnersRemaining;
	}

	protected function renderBaseUri( $strBaseUri, $objXmlWriter ) {
		$objXmlWriter->addElement( 'base_uri', 'http://' . $strBaseUri );
	}

	protected function renderTermsAndConditionsPath( $strTermsAndConditionsPath, $objXmlWriter ) {
		$objXmlWriter->addElement( 'terms_and_conditions', 'http://' . $strTermsAndConditionsPath );
	}

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				$this->m_arrobjErrorMsgs[] = $objErrorMsg;
			}
		}
		return;
	}

	public function cancelLeases( $intCompanyUserId, $objDatabase ) {

		$arrobjLeaseCustomers = $this->fetchLeaseCustomers( $objDatabase );

		$boolIsValid = true;

		if( true == valArr( $arrobjLeaseCustomers ) ) {
			foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
				if( CLeaseStatusType::CANCELLED != $objLeaseCustomer->getLeaseStatusTypeId() ) {
					$objLeaseCustomer->setLeaseStatusTypeId( CLeaseStatusType::CANCELLED );
					if( false == $objLeaseCustomer->update( $intCompanyUserId, $objDatabase ) ) {
						$boolIsValid &= false;
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function cancelLeasesByPropertyId( $intPropertyId, $intCompanyUserId, $objDatabase ) {

		$arrobjLeaseCustomers = $this->fetchLeaseCustomersByPropertyId( $intPropertyId, $objDatabase );

		$boolIsValid = true;

		if( true == valArr( $arrobjLeaseCustomers ) ) {
			foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
				if( CLeaseStatusType::CANCELLED != $objLeaseCustomer->getLeaseStatusTypeId() ) {
					$objLeaseCustomer->setLeaseStatusTypeId( CLeaseStatusType::CANCELLED );
					if( false == $objLeaseCustomer->update( $intCompanyUserId, $objDatabase ) ) {
						$boolIsValid &= false;
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function cancelNonIntegratedLeasesWithPropertyRemotePrimaryKeyNotNull( $intCompanyUserId, $objDatabase ) {

		$arrobjLeaseCustomers = $this->fetchNonIntegratedAndActiveLeaseCustomersWithPropertyRemotePrimaryKeyNotNull( $objDatabase );

		$boolIsValid = true;

		if( true == valArr( $arrobjLeaseCustomers ) ) {
			foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
				$objLeaseCustomer->setLeaseStatusTypeId( CLeaseStatusType::CANCELLED );
				if( CCustomerType::PRIMARY != $objLeaseCustomer->getCustomerTypeId() ) {
					$objLeaseCustomer->setIsUngrouped( 1 );
				}
				if( false == $objLeaseCustomer->update( $intCompanyUserId, $objDatabase, false, true ) ) {
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function cancelLeasesByIntegrationDatabaseId( $intIntegrationDatabaseId, $intCompanyUserId, $objDatabase ) {

		$arrobjLeaseCustomers = $this->fetchLeaseCustomersByIntegrationDatabaseId( $intIntegrationDatabaseId, $objDatabase );

		$boolIsValid = true;

		if( true == valArr( $arrobjLeaseCustomers ) ) {
			foreach( $arrobjLeaseCustomers as $objLeaseCustomer ) {
				if( CLeaseStatusType::CANCELLED != $objLeaseCustomer->getLeaseStatusTypeId() ) {
					$objLeaseCustomer->setLeaseStatusTypeId( CLeaseStatusType::CANCELLED );
					if( false == $objLeaseCustomer->update( $intCompanyUserId, $objDatabase, false, true ) ) {
						$boolIsValid &= false;
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function cancelNonIntegratedLeases( $intCompanyUserId, $objDatabase ) {

		$arrobjNonIntegratedLeaseCustomers = $this->fetchNonIntegratedAndActiveLeaseCustomersWithPropertyRemotePrimaryKeyNotNull( $objDatabase );

		$boolIsValid = true;

		if( true == valArr( $arrobjNonIntegratedLeaseCustomers ) ) {
			foreach( $arrobjNonIntegratedLeaseCustomers as $objLeaseCustomer ) {
				if( CLeaseStatusType::CANCELLED != $objLeaseCustomer->getLeaseStatusTypeId() ) {
					$objLeaseCustomer->setLeaseStatusTypeId( CLeaseStatusType::CANCELLED );

					if( false == $objLeaseCustomer->update( $intCompanyUserId, $objDatabase, false, true ) ) {
						$boolIsValid &= false;
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function awardActivityPoints( $intCompanyUserId, $intActivityPointId, $objProperty, $objDatabase ) {

		$arrmixCustomerAwards = [];

		switch( NULL ) {
			default:
				if( false == valObj( $objProperty, 'CProperty' ) ) {
					// Without a valid property, we can't check preferences
					trigger_error( 'Property object was not valid', E_USER_WARNING );
					break;
				}

				$objPropertyPreference = $objProperty->fetchPropertyPreferenceByKey( 'TURN_OFF_GAMIFICATION', $objDatabase );

				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
					break;
				}

				$arrobjCustomerSettings = rekeyObjects( 'Key', $this->fetchCustomerSettings( $objDatabase ) );

				if( true == array_key_exists( 'opt_out_of_gamification', [ $arrobjCustomerSettings ] ) ) {
					// Resident is opt out of gamification, so we do nothing
					break;
				}

				$objDefaultActivityPoint = CDefaultActivityPoints::fetchDefaultActivityPointById( $intActivityPointId, $objDatabase );
				if( false == valObj( $objDefaultActivityPoint, 'CDefaultActivityPoint' ) || false == $objDefaultActivityPoint->getIsPublished() ) {
					// This activity is either invalid or not published
					trigger_error( 'Activity Point ID was invalid', E_USER_WARNING );
					break;
				}

				$objCompanyActivityPoint = CCompanyActivityPoints::fetchCompanyActivityPointByCidByDefaultActivityPointId( $this->getCid(), $intActivityPointId, $objDatabase );
				if( false == valObj( $objCompanyActivityPoint, 'CCompanyActivityPoint' ) ) {
					break;
				}

				$objCompanyActivityPoint->badgeGranted( false );

				if( false == valObj( $objCompanyActivityPoint, 'CCompanyActivityPoint' ) ) {
					trigger_error( 'Activity point failed to load.', E_USER_WARNING );
					break;
				}

				if( false == $objCompanyActivityPoint->getIsPublished() ) {
					// The activity is not published, so we do nothing
					break;
				}

				$objCompanyBadge = $objCompanyActivityPoint->fetchCompanyBadge( $objDatabase );

				if( false != valObj( $objCompanyBadge, 'CCompanyBadge' ) && false != $objCompanyBadge->getIsPublished() ) {

					$objDefaultBadge = CDefaultBadges::fetchDefaultBadgeById( $objCompanyBadge->getDefaultBadgeId(), $objDatabase );

					if( false != valObj( $objDefaultBadge, 'CDefaultBadge' ) && false != $objDefaultBadge->getIsPublished() ) {
						// There is a published badge associated with this activity.  We need to
						// check to see if the user has earned a new badge, and award it if they have.

						$objCustomerBadge = $objCompanyActivityPoint->fetchCustomerBadgeByCustomerId( $this->getId(), $objDatabase );
						$arrobjCustomerActivityPoints = CCustomerActivityPoints::fetchCustomerActivityPointsByCompanyActivityPointIdByCustomerIdByCid( $objCompanyActivityPoint->getId(), $this->getId(), $this->getCid(), $objDatabase );
						$intCustomerActivityCount = 1;

						if( false != valArr( $arrobjCustomerActivityPoints ) ) {
							$intCustomerActivityCount += \Psi\Libraries\UtilFunctions\count( $arrobjCustomerActivityPoints );
						}

						if( false == valObj( $objCustomerBadge, 'CCustomerBadge' ) ) {
							// The customer does not have a badge for this activity.  See
							// if they have earned a base-level badge, and award it.
							$intBaseCriteria = $objCompanyBadge->getBaseCriteria();

							if( false == is_null( $intBaseCriteria ) && 0 < $intBaseCriteria && $intCustomerActivityCount >= $intBaseCriteria ) {
								$objCustomerBadge = $this->createCustomerBadge( $objCompanyBadge->getId(), $objDatabase );

								if( false == $objCustomerBadge->insert( $intCompanyUserId, $objDatabase ) ) {
									trigger_error( 'Customer badge failed to insert.', E_USER_WARNING );
									break;
								} else {
									$objCompanyActivityPoint->badgeGranted( true );
									$arrmixCustomerAwards['badge'] = $objCustomerBadge;
								}
							}
						}

						if( false != valObj( $objCustomerBadge, 'CCustomerBadge' ) && false == $objCustomerBadge->getIsProAward() ) {
							// The customer has a base level badge for this activity.  See
							// if they have earned a pro-level badge, and award it.
							$intProCriteria = $objCompanyBadge->getProCriteria();

							if( false == is_null( $intProCriteria ) && 0 < $intProCriteria && $intCustomerActivityCount >= $intProCriteria ) {
								$objCustomerBadge->setIsProAward( true );

								if( false == $objCustomerBadge->update( $intCompanyUserId, $objDatabase ) ) {
									trigger_error( 'Customer badge failed to update.', E_USER_WARNING );
									break;
								} else {
									$objCompanyActivityPoint->proGranted( true );
									$arrmixCustomerAwards['badge'] = $objCustomerBadge;
								}
							}
						}
					}
				}

				if( false == $objCompanyActivityPoint->insertPoints( $intCompanyUserId, $this, $objDatabase ) ) {
					trigger_error( 'Activity point failed to insert.', E_USER_WARNING );
					break;
				} else {
					$arrmixCustomerAwards['activity_point'] = $objCompanyActivityPoint;
				}
		}

		return $arrmixCustomerAwards;
	}

	public function createCustomerActivityPoint( $intNumberOfPoints ) {
		$objCustomerActivityPoint = new CCustomerActivityPoint();
		$objCustomerActivityPoint->setDefaults();

		$objCustomerActivityPoint->setCid( $this->getCid() );
		$objCustomerActivityPoint->setCustomerId( $this->getId() );
		$objCustomerActivityPoint->setPointsEarned( $intNumberOfPoints );

		return $objCustomerActivityPoint;
	}

	public function createCustomerBadge( $intCompanyBadgeId ) {
		$objCustomerBadge = new CCustomerBadge();

		$objCustomerBadge->setCid( $this->getCid() );
		$objCustomerBadge->setCustomerId( $this->getId() );
		$objCustomerBadge->setCompanyBadgeId( $intCompanyBadgeId );

		return $objCustomerBadge;
	}

	/**
	 * @param \CWebsite $objWebsite
	 * @param int[] $arrintPropertyIds
	 * @param \CDatabase $objDatabase
	 * @return bool
	 */
	public function canLogin( $objWebsite, $arrintPropertyIds, $objDatabase, $boolIsEntrataAdmin = false ) {

		if( false == valObj( $objWebsite, \CWebsite::class ) ) {
			return false;
		}
		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}

		$objDataset = $objDatabase->createDataset();
		$objDataset->initialize();

		$strSql = 'SELECT
						c.*, cps.*
					FROM
						customers c
						JOIN customer_portal_settings cps ON (c.cid = cps.cid AND c.id = cps.customer_id )
						JOIN lease_customers lc ON (cps.cid = lc.cid AND cps.customer_id =lc.customer_id )
						JOIN leases l ON ( lc.cid = l.cid AND lc.lease_id =l.id )
					WHERE
						c.cid = ' . ( int ) $objWebsite->getCid() . '
						AND c.id = ' . ( int ) $this->m_intId . '
						AND ( l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							OR l.property_id IN ( SELECT p.id FROM properties p WHERE p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND p.cid = ' . ( int ) $objWebsite->getCid() . ' ) ) ';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		if( false === isset( $arrstrResult[0]['id'] ) || false === is_numeric( $arrstrResult[0]['id'] ) ) {
			return false;
		}

		// Check to make sure this customer is allowed to login
		if( true == $arrstrResult[0]['dont_allow_login'] ) {
			return false;
		}

		// Check to make sure that the properties this customer has at least one that is not hidden or 'not-listed'
		if( false == $objWebsite->canCustomerLogin( $objDatabase ) && false == $boolIsEntrataAdmin ) {
			return false;
		}

		return true;
	}

	public function login( $objWebsite, $arrintPropertIds, $objDatabase, $boolIsPSIAdministratorFlag = false, $boolIsForgetPassword = false, $strPasswordEncrypted = NULL ) {
		$this->setIsLoggedIn( false );
		$strUserPasswordEncrypted = str_replace( "'", '', $this->sqlPasswordEncrypted() );

		$strCondition = '';
		if( 0 >= strlen( trim( $this->m_strPasswordEncrypted ) ) && false == $boolIsPSIAdministratorFlag ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', 'Invalid email and/or password', 304 ) );
			return false;
		} elseif( true == $boolIsForgetPassword ) {
			$strCondition = ' AND trim( both \' \' FROM lower(cps.username) ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $this->getUsername() ) ) ) . '\'';
		} elseif( true == $boolIsPSIAdministratorFlag ) {
			$strCondition = ' AND cps.customer_id = ' . trim( $this->sqlId() ) . '';
		} else {
			$strCondition = ' AND trim( both \' \' FROM cps.password_encrypted ) = ' . trim( $this->sqlPasswordEncrypted() ) . ' AND trim( both \' \' FROM lower(cps.username) ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $this->getUsername() ) ) ) . '\'';
		}

		if( false == valArr( $arrintPropertIds ) ) return NULL;

		$objDataset = $objDatabase->createDataset();
		$objDataset->initialize();

		$strSql = 'SELECT
						DISTINCT(c.id) as id
					FROM
						customers c
						JOIN customer_portal_settings cps ON (c.cid = cps.cid AND c.id = cps.customer_id )
						JOIN lease_customers lc ON (cps.cid = lc.cid AND cps.customer_id =lc.customer_id )
						JOIN leases l ON ( lc.cid = l.cid AND lc.lease_id =l.id )
					WHERE
						c.cid = ' . ( int ) $objWebsite->getCid() . '
						' . $strCondition . '
						AND ( l.property_id IN ( ' . implode( ',', $arrintPropertIds ) . ' )
							OR l.property_id IN ( SELECT p.id FROM properties p WHERE p.property_id IN ( ' . implode( ',', $arrintPropertIds ) . ' ) AND p.cid = ' . ( int ) $objWebsite->getCid() . ' ) ) ';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrResult[0]['id'] ) && true == is_numeric( $arrstrResult[0]['id'] ) ) {
			$strSql = 'SELECT * FROM view_customers WHERE id = ' . ( int ) $arrstrResult[0]['id'] . ' AND cid = ' . ( int ) $objWebsite->getCid();

			if( !$objDataset->execute( $strSql ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to login user. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );
				$objDataset->cleanup();
				return false;
			}

			if( $objDataset->getRecordCount() == 0 ) {
				$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', 'Invalid email and/or password', 304 ) );
				return false;
			}

			$arrmixValues = $objDataset->fetchArray();

			$this->setValues( $arrmixValues );

			if( true == $boolIsForgetPassword ) {

				$strExpireDate = date( 'm/d/Y H:i:s', strtotime( $arrmixValues['password_expiration'] . ' +1 day' ) );

				if( true == valArr( $arrmixValues ) && false == is_null( $arrmixValues['password_encrypted'] ) && false == is_null( $arrmixValues['password_expiration'] ) ) {
					$boolIsExpiredPassword = false;
					if( $strExpireDate < date( 'm/d/Y H:i:s' ) ) {
						$boolIsExpiredPassword = true;
					}
				}

				if( true == $boolIsExpiredPassword ) {
					$this->addErrorMsg( new CErrorMsg( 313, NULL, 'Temporary password has expired. Please request new password.' ) );
					return false;
				}

				if( $strPasswordEncrypted != $strUserPasswordEncrypted ) {
					$this->addErrorMsg( new CErrorMsg( 313, NULL, 'Invalid email and/or password' ) );
					return false;
				}
			}

			$objDataset->cleanup();

			// Check to make sure this customer is allowed to login
			if( true == $this->getDontAllowLogin() ) {
				$this->addErrorMsg( new CErrorMsg( 312, NULL, 'You are unable to login at this time. Please contact your leasing office. Resident record has not yet been granted access to this website.' ) );
				return false;
			}

			// Check to make sure that the properties this customer has at least one that is not hidden or 'not-listed'
			if( false == $objWebsite->canCustomerLogin( $objDatabase ) && false == $boolIsPSIAdministratorFlag ) {
				$this->addErrorMsg( new CErrorMsg( 313, NULL, 'You are not an active resident in a property associated to this website. If you have received this message in error, please contact your leasing office.' ) );
				return false;
			}

			$this->setIsLoggedIn( true );

			return true;
		}

		$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', 'Invalid email and/or password', 304 ) );
		return false;
	}

	/**
	 * Login for GroupPortal
	 *
	 * @param $intCid
	 * @param $intOrganizationId
	 * @param $objDatabase
	 * @return bool
	 */
	public function groupLogin( $intCid, $intOrganizationId, $objDatabase ) {
		$this->setIsLoggedIn( false );

		$objDataset = $objDatabase->createDataset();
		$objDataset->initialize();

		// We want to check the customer based on customer_id, and organization_id
		$strSql = 'SELECT
						DISTINCT(c.id) as id
					FROM
						customers c
						JOIN organizations o ON (o.cid = c.cid AND o.customer_id = c.id )
						JOIN customer_portal_settings cps ON (o.cid = cps.cid AND o.customer_id = cps.customer_id )
					WHERE
						c.cid = ' . ( int ) $intCid . ' AND o.id = ' . ( int ) $intOrganizationId . ' AND o.is_archived = FALSE';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrstrResult[0]['id'] ) && true == is_numeric( $arrstrResult[0]['id'] ) ) {
			$strSql = 'SELECT * FROM view_customers WHERE id = ' . ( int ) $arrstrResult[0]['id'] . ' AND cid = ' . ( int ) $intCid;

			if( !$objDataset->execute( $strSql ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to login user. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );
				$objDataset->cleanup();
				return false;
			}

			if( $objDataset->getRecordCount() == 0 ) {
				$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', 'Invalid email and/or password', 304 ) );
				return false;
			}

			$arrmixValues = $objDataset->fetchArray();

			$this->setValues( $arrmixValues );

			$objDataset->cleanup();

			// Check to make sure this customer is allowed to login
			if( true == $this->getDontAllowLogin() ) {
				$this->addErrorMsg( new CErrorMsg( 312, NULL, 'You are unable to login at this time. Please contact your leasing office. Resident record has not yet been granted access to this website.' ) );
				return false;
			}

			$this->setIsLoggedIn( true );

			return true;
		}

		$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', 'Invalid email and/or password', 304 ) );
		return false;
	}

	public function sendGroupPasswordResetLink( $objClient, $objDatabase, $objEmailDatabase ) {
		$strTemporaryPassword = $this->generatePassword();
		$boolIsMultiClient = false;

		$this->setPassword( $strTemporaryPassword );
		$this->encryptPassword();
		$this->setPasswordExpiration( 'NOW()' );

		if( false == $this->update( SYSTEM_USER_ID, $objDatabase ) ) {
			return false;
		}

		$arrmixExtraMetaData = [ 'lease_customers' => [ 'is_required_new_version' => true, 'temporary_password' => $strTemporaryPassword, 'is_send_to_group' => true ], 'properties' => [ 'is_customer_password_reset' => true, 'is_required_address' => $boolIsMultiClient, 'is_send_to_group' => true, 'is_tenant' => false, 'is_required_new_version' => true ] ];

		return $this->createCustomerPasswordResetSystemMessage( $objClient, $arrmixExtraMetaData, $objDatabase );
	}

	/**
	 * @param \CClient $objClient
	 * @param \CDatabase $objDatabase
	 * @param $strTempCode
	 * @return string|string[]|null
	 */
	public function getGroupCustomerPasswordResetEmailContent( $objClient, $objDatabase, $strTempCode ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_PORTAL, false );

		if( true == \Psi\Libraries\UtilFunctions\valObj( $objClient, \CClient::class ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		if( false == \Psi\Libraries\UtilFunctions\valId( $this->getId() ) ) {
			$objCustomerTemp = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByUsernameByCid( $this->getUsername(), $this->getCid(), $objDatabase );
			$objSmarty->assign_by_ref( 'customer', $objCustomerTemp );
		} else {
			$objSmarty->assign_by_ref( 'customer', $this );
		}

		$boolIsMultiClient = false;

		$objSmarty->assign_by_ref( 'temporary_password', $strTempCode );
		$objSmarty->assign_by_ref( 'marketing_media_association', 			$objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file',	$objLogoMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'is_multi_client',	$boolIsMultiClient );

		$objSmarty->assign( 'path_base_uri', 			CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'media_library_uri',		CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		// @TODO: We need an image asset for GroupPortal
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'gp-icon-192.png' );
		$objSmarty->assign( 'image_url_path',           CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$objSmarty->assign( 'product_name', 'Reservation Hub' );

		$strHtmlRequestContent = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/temp_password.tpl' );

		return $strHtmlRequestContent;
	}

	public function logout() {
		$this->setIsLoggedIn( false );

		return true;
	}

	public function logAuthentication( $intCompanyUserId, $objDatabase, $boolIsLoginFromEntrata = false, $boolIsLoginFromApp = false, $objCompanyUser = NULL, $intPropertyId = NULL, $boolIsLoginFromTenantPortal = false, $boolLoginToTPFromEntrata = false ) {
		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		if( true == $boolIsLoginFromEntrata ) {
			$intEventType = CEventType::RESIDENT_LOG_IN_BY_MANAGER;
		} elseif( true == $boolIsLoginFromApp ) {
			$intEventType = CEventType::RESIDENT_LOG_IN_FROM_APP;
		} elseif( true == $boolIsLoginFromTenantPortal ) {
			$intEventType = CEventType::TENANT_PORTAL_LOG_IN;
		} elseif( true == $boolLoginToTPFromEntrata ) {
			$intEventType = CEventType::TENANT_PORTAL_LOG_IN_BY_MANAGER;
		} else {
			$intEventType = CEventType::RESIDENT_LOG_IN;
		}

		$objEvent = $objEventLibrary->createEvent( [ $this ], $intEventType );
		$objEvent->setCid( $this->getCid() );
		if( true === is_null( $intPropertyId ) ) {
			$objEvent->setPropertyId( $this->getPropertyId() );
		} else {
			$objEvent->setPropertyId( $intPropertyId );
		}
			$objEvent->setEventDatetime( 'NOW()' );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objEvent->setCompanyUser( $objCompanyUser );
		}

		$objEventLibrary->buildEventDescription( $this );
		if( true == valObj( $objEventLibrary, 'CEventLibrary' ) && false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
			$strErrorMessage = '';
			if( true == valArr( $objEventLibrary->getErrorMsgs() ) ) {
				$arrobjErrorkeys = array_keys( $objEventLibrary->getErrorMsgs() );
				$arrmixError = $objEventLibrary->getErrorMsgs();
				if( true == valArr( $arrobjErrorkeys ) ) {
					foreach( $arrobjErrorkeys as $strErrorKey ) {
						$strErrorMessage .= $arrmixError[$strErrorKey]->getMessage() . ' | ';
					}
				}
			}
			$objDatabase->rollback();
			trigger_error( 'Failed to insert event for the Customer : ' . $this->m_intId . ' & Event Type : ' . ( int ) $intEventType . ' & Property Id : ' . $this->getPropertyId() . ' & Company User Id : ' . ( int ) $intCompanyUserId . ' & Error messages : ' . $strErrorMessage, E_USER_WARNING );
			return;
		}

		return true;
	}

	public function logPasswordChange( $intCompanyUserId, $objDatabase, $boolIsUserNameChange = false, $boolIsPasswordChange = true, $objCompanyUser = NULL ) {
		// TODO: we can combine logAuthentication and this function.
		require_once( PATH_LIBRARIES_PSI . 'EventLibrary/CEventLibrary.class.php' );

		$objEventLibrary = new CEventLibrary();

		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$intEventType = CEventType::RESIDENT_USERNAME_PASSWORD_CHANGE;

		$objEvent = $objEventLibrary->createEvent( [ $this ], $intEventType );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setPropertyId( $this->getPropertyId() );
		$objEvent->setEventDatetime( 'NOW()' );

		$objEvent->setIsPasswordChange( false );
		$objEvent->setIsUserNameChange( false );
		$objEvent->setIsCompanyUser( false );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objEvent->setIsCompanyUser( true );
			$objEvent->setCompanyUser( $objCompanyUser );
		}
		$objEvent->setIsPasswordChange( $boolIsPasswordChange );
		$objEvent->setIsUserNameChange( $boolIsUserNameChange );

		$objEventLibrary->buildEventDescription( $this );

		if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
			trigger_error( 'Failed to insert event for the Customer : ' . $this->m_intId, E_USER_WARNING );
		}

		return true;
	}

	public function forceIntegration( $intCompanyUserId, $intCid, $objProperty, $objDatabase, $objLease = NULL, $intLedgerDisplayDays = NULL, $boolPullCustomerNotes = false, $arrstrTransportRules = NULL, $boolProcessThroughAMQP = false ) {
		// Import and Update CCustomer if this company has integration

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $intCid, CIntegrationService::RETRIEVE_CUSTOMER, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setCustomer( $this );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setLease( $objLease );
		$objGenericWorker->setLedgerDisplayDays( $intLedgerDisplayDays );

		if( true == isset( $arrstrTransportRules ) ) {
			$objGenericWorker->setTransportRules( $arrstrTransportRules );
		}

		$objGenericWorker->setPullCustomerNotes( $boolPullCustomerNotes );
		$objGenericWorker->setCallTimeLimit( 20 );

		$boolIsSuccessful = $objGenericWorker->process();

		return $boolIsSuccessful;
	}

	public function encryptPassword() {
		return $this->setPassword( CHash::createService()->hashCustomer( $this->getPassword() ) );
	}

	public function verifyNewPasswordsMatch( $arrstrPasswords ) {
		if( false == valArr( $arrstrPasswords ) ) {
			trigger_error( 'Invalid Request: Missing password array - CCustomer', E_USER_ERROR );
		}

		$strPassword1 = array_pop( $arrstrPasswords );
		$strPassword2 = array_pop( $arrstrPasswords );

		if( trim( $strPassword1 ) != trim( $strPassword2 ) ) {
			$this->addErrorMsg( new CErrorMsg( 300, 'password', 'Passwords do not match' ) );
			return false;
		}

		// NOTE: This is in here because validate() for the password field does not work correctly
		if( 6 > strlen( $strPassword1 ) ) {
			$this->addErrorMsg( new CErrorMsg( 303, 'password', 'Your password must be at least 6 characters long' ) );
			return false;
		}
		// NOTE: End validation hack

		return true;
	}

	// This function is used to update lease balance on all leases for a specific company customer on one single property ($this->m_intPropertyId).
	// This function was originally written for the posting of scheduled calls (voip).

	public function forcePropertyLeaseIntegration( $objDatabase ) {
		if( false == is_numeric( $this->m_intPropertyId ) ) return NULL;

		$arrobjLeases = $this->fetchLeasesByPropertyIdsWithRemotePrimaryKeyNotNull( [ $this->m_intPropertyId ], $objDatabase );

		$boolIsValid = true;

		if( true == valArr( $arrobjLeases ) ) {
			foreach( $arrobjLeases as $objLease ) {
				$boolIsValid &= $objLease->importLedger( 1, $this, $objDatabase );
			}
		}

		return $boolIsValid;
	}

	// This function was originally written for the posting of scheduled calls (voip).

	public function fetchLateRentAmountDue( $arrintLeaseTypeIds, $fltThresholdDue, $objDatabase, $intPendingDays = 30 ) {

		$strSql = 'SELECT
						sum( clt.transaction_amount_due ) AS late_rent_amount_due
					FROM
						leases cl,
						ar_transactions clt 
					WHERE
						cl.id = clt.lease_id
						AND cl.cid = clt.cid
						AND cl.property_id = ' . ( int ) $this->getPropertyId() . '
						AND cl.cid = ' . $this->getCid() . '
						AND cl.id IN ( SELECT 
											lease_id 
						FROM
											lease_customers 
						WHERE
											customer_id = ' . ( int ) $this->getId() . ' 
							AND cid = ' . ( int ) $this->getCid() . '
											AND property_id = ' . ( int ) $this->getPropertyId() . '
											AND lease_status_type_id IN ( ' . implode( ',', $arrintLeaseTypeIds ) . ' )
										)
						AND clt.transaction_amount_due <> 0 
						AND clt.is_temporary = false
					HAVING SUM( transaction_amount_due ) > ' . ( int ) $fltThresholdDue;

		$arrfltLateRentAmountDues = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrfltLateRentAmountDues ) ) {
			return 0;
		} else {
			return $arrfltLateRentAmountDues[0]['late_rent_amount_due'];
		}

	}

	/**
	 * This function was written to prep the resident's phone number for an outbound voip call
	 * @deprecated Doesn't handle international phone number formatting.
	 * @see \i18n\CPhoneNumber::format() should be used.
	 */
	public function formatPhoneNumberForVoip( $strPhoneNumber ) {

		// Remove all characters that are not numbers
		$strPhoneNumber = preg_replace( '/[^0-9]/', '', $strPhoneNumber );

		if( true == is_null( $strPhoneNumber ) || 0 == strlen( trim( $strPhoneNumber ) ) ) return false;

		// Make sure that 1 is dropped off the front if first digit is equal 1
		while( 1 == $strPhoneNumber[0] ) {
			$strPhoneNumber = \Psi\CStringService::singleton()->substr( $strPhoneNumber, 1 );
		}

		if( true == is_null( $strPhoneNumber ) || 0 == strlen( trim( $strPhoneNumber ) ) ) return false;

		// Continue if first 3 digits are 911
		if( '911' == \Psi\CStringService::singleton()->substr( $strPhoneNumber, 0, 3 ) ) {
			return false;
		}

		// Truncate length of number to 10 digits
		$strPhoneNumber = \Psi\CStringService::singleton()->substr( $strPhoneNumber, 0, 10 );

		// Add a 1 to the front of the number
		$strPhoneNumber = '1' . $strPhoneNumber;

		return $strPhoneNumber;
	}

	public function setCompetingUsernamesNull( $intCompanyUserId, $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strUsername ) || 0 == strlen( trim( $this->m_strUsername ) ) ) {
			return $boolIsValid;
		}
		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByUsernameNotByIdByCid( $this->m_strUsername, $this->m_intId, $this->m_intCid, $objDatabase );

		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$objCustomer->setEmailAddress( $objCustomer->getUsername() );
			$objCustomer->setUsername( NULL );
			$boolIsValid = $objCustomer->update( $intCompanyUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function fetchPortalVisibleLeasesByLeaseStatusTypeIds( $arrintLeaseStatusTypeIds, $arrintLeaseIds, $objDatabase ) {

		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByCustomerIdByLeaseIdsByLeaseStatusTypeIdsByCid( $this->getId(), $arrintLeaseIds, $arrintLeaseStatusTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerByloginWithId( $objWebsite, $objDatabase ) {

		$strSql = ' SELECT * FROM
						view_customers
						WHERE cid = ' . $objWebsite->getCid() . '
						AND id = ' . ( int ) $this->m_intId;

		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomer( $strSql, $objDatabase );

		// Arriving here indicates a session id that no longer retrieves a resident.... we should set session id to NULL and return false
		if( false == valObj( $objCustomer, 'CCustomer' ) ) {
			return false;
		}

		$objCustomer->setIsLoggedIn( false );

		// Check to make sure this customer is allowed to login
		if( true == $objCustomer->getDontAllowLogin() ) {
			$objCustomer->addErrorMsg( new CErrorMsg( 312, NULL, 'You are unable to login at this time. Please contact your leasing office.  Resident Portal access privileges denied.' ) );
			return false;
		}

		// Check to make sure that the properties this customer has at least one that is not hidden or 'not-listed'
		if( false == $objWebsite->canCustomerLogin( $objDatabase ) ) {
			$objCustomer->addErrorMsg( new CErrorMsg( 313, NULL, 'You are not an active resident in a property associated to this website.  If you have received this message in error, please contact your leasing office.' ) );
			return false;
		}

		$objCustomer->setIsLoggedIn( true );

		return $objCustomer;
	}

	public function fetchIntegratedArPayments( $objDatabase ) {
		return CArPayments::fetchIntegratedArPaymentsByCustomerIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchRenewalReadyLeasesByLeaseRenewalNotificationDays( $intLeaseRenewalNotificationDays, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchRenewalReadyLeasesByCustomerIdByLeaseRenewalNotificationDaysByCid( $this->getId(), $intLeaseRenewalNotificationDays, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseByPropertyIdByLeaseDateByPropertyPreference( $intPropertyId, $strLeaseStartDate, $strLeaseEndDate, $intPreferenceValue, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByCustomerByPropertyIdByLeaseDateByPropertyPreference( $this, $intPropertyId, $strLeaseStartDate, $strLeaseEndDate, $intPreferenceValue, $objDatabase );
	}

	public function fetchLeasePropertyUnits( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchLeasePropertyUnitsByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerIncomeByIncomeTypeId( $intIncomeTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomeByIncomeTypeIdByCustomerIdByCid( $intIncomeTypeId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerContactByCustomerContactTypeId( $intCustomerContactTypeId, $objDatabase ) {
		return CCustomerContacts::fetchCustomerContactByCustomerContactTypeIdByCustomerIdByCid( $intCustomerContactTypeId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationDatabase( $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchIntegrationDatabaseByPropertyIdByCid( $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function importCustomer( $intCompanyUserId, $objProperty, &$objLease, $objDatabase, $objApplication = NULL, $arrstrTransportRules = NULL ) {

		if( 0 < strlen( $objProperty->getRemotePrimaryKey() ) ) {
			$objIntegrationDatabase = $this->fetchIntegrationDatabase( $objProperty->getId(), $objDatabase );
		}

		if( true == isset( $objIntegrationDatabase ) && true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) {
			$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::RETRIEVE_CUSTOMER, $intCompanyUserId, $objDatabase );
			$objGenericWorker->setCustomer( $this );
			$objGenericWorker->setProperty( $objProperty );
			$objGenericWorker->setLease( $objLease );
			$objGenericWorker->setDatabase( $objDatabase );
			$objGenericWorker->setTransportRules( $arrstrTransportRules );
			$objGenericWorker->setApplication( $objApplication );

			$boolResult 	= $objGenericWorker->process();
			$objCustomer 	= $objGenericWorker->getCustomer();

			$this->m_arrfltChargeDeposits = $objCustomer->m_arrfltChargeDeposits;

			return true;
		}

		return false;

	}

	public function setCustomerIdentifications( $objTempStdCustomer, $objProperty, $objClient, $objDatabase ) {

		if( false == valObj( $objProperty, 'CProperty' ) ) return NULL;

		$objTempStdCustomer->idType				= ( true == isset( $objTempStdCustomer->idType ) ) ? trim( $objTempStdCustomer->idType ) : NULL;
		$objTempStdCustomer->idNumber			= ( true == isset( $objTempStdCustomer->idNumber ) ) ? trim( $objTempStdCustomer->idNumber ) : NULL;
		$objTempStdCustomer->idExpirationDate	= ( true == isset( $objTempStdCustomer->idExpirationDate ) ) ? trim( $objTempStdCustomer->idExpirationDate ) : NULL;

		$objTempStdCustomer->dlNumber			= ( true == isset( $objTempStdCustomer->dlNumber ) ) ? trim( $objTempStdCustomer->dlNumber ) : NULL;
		$objTempStdCustomer->dLState			= ( true == isset( $objTempStdCustomer->dLState ) ) ? trim( $objTempStdCustomer->dLState ) : NULL;

		if( true == valStr( $objTempStdCustomer->idType ) && true == valStr( $objTempStdCustomer->idNumber ) ) {

			$strIdNumber 		= $objTempStdCustomer->idNumber;
			$strIdExpiration 	= $objTempStdCustomer->idExpirationDate;
			$strStateCode 		= NULL;

			$objPropertyIdentificationType = $objProperty->fetchPropertyIdentificationTypeByRemotePrimaryKey( $objTempStdCustomer->idType, $objDatabase );

			if( false == valObj( $objPropertyIdentificationType, 'CPropertyIdentificationType' ) ) {

				$strSql = 'SELECT
								pit.company_identification_type_id
							 FROM
							 	property_identification_types pit
							 	JOIN company_identification_types cit ON( pit.cid = cit.cid AND	pit.company_identification_type_id = cit.id )
							 WHERE
							 	pit.cid = ' . ( int ) $objClient->getId() . '
							 	AND pit.property_id = ' . ( int ) $objProperty->getId() . '
							 	AND LOWER( cit.name ) ILIKE \'' . trim( addslashes( \Psi\CStringService::singleton()->strtolower( $objTempStdCustomer->idType ) ) ) . '\'
							 LIMIT 1';

				$objPropertyIdentificationType = CPropertyIdentificationTypes::fetchPropertyIdentificationType( $strSql, $objDatabase );

				if( true == valObj( $objPropertyIdentificationType, 'CPropertyIdentificationType' ) ) {
					$intCompanyIdentificationTypeId  = $objPropertyIdentificationType->getCompanyIdentificationTypeId();
				} else {
					return NULL;
				}
			}
		} else {

			$strIdNumber 		= $objTempStdCustomer->dlNumber;
			$strStateCode 		= $objTempStdCustomer->dLState;
			$strIdExpiration 	= NULL;
			$strIdCode			= NULL;

			if( true == valStr( $strIdNumber ) ) {
				$strTempIdNumber	= \Psi\CStringService::singleton()->strtoupper( trim( \Psi\CStringService::singleton()->substr( $strIdNumber, 0, 4 ) ) );

				if( true == in_array( $strTempIdNumber, [ 'VISA', 'PASS', 'STID', 'MLID' ] ) ) {
					$arrstrTempIdNumber = explode( '-', $strIdNumber );
					$strIdCode 		= ( true == valArr( $arrstrTempIdNumber ) ) ? $arrstrTempIdNumber[0] : NULL;
					$strIdNumber 	= ( true == valArr( $arrstrTempIdNumber ) ) ? $arrstrTempIdNumber[1] : NULL;
				}
			}

			if( true == valStr( $strIdCode ) ) {
				$strSystemCode = NULL;
				switch( \Psi\CStringService::singleton()->strtoupper( $strIdCode ) ) {

					case 'STID':
						$strSystemCode = CCompanyIdentificationType::CODE_STATE_ISSUED_PICTURE_ID;
						break;

					case 'PASS':
						$strSystemCode = CCompanyIdentificationType::CODE_PASSPORT_NUMBER;
						break;

					case 'VISA':
						$strSystemCode = CCompanyIdentificationType::CODE_VISA;
						break;

					case 'MLID':
						$strSystemCode = CCompanyIdentificationType::CODE_MILITARY_ID;
						break;

					default:
						$strSystemCode = CCompanyIdentificationType::CODE_DRIVERS_LICENSE;
						break;
				}
			} else {
				$strSystemCode  = CCompanyIdentificationType::CODE_DRIVERS_LICENSE;
			}

			$objCompanyIdentificationType = $objClient->fetchCompanyIdentificationTypeBySystemCode( $strSystemCode, $objDatabase );

			if( false == valObj( $objCompanyIdentificationType, 'CCompanyIdentificationType' ) ) return NULL;

			$intCompanyIdentificationTypeId  = $objCompanyIdentificationType->getId();
		}

		if( false == is_null( $intCompanyIdentificationTypeId ) ) {
			$this->setCompanyIdentificationTypeId( $intCompanyIdentificationTypeId );
			$this->setIdentificationNumber( $strIdNumber );
			$this->setIdentificationExpiration( $strIdExpiration );
			$this->setDlStateCode( $strStateCode );
		}

		return NULL;
	}

	public function loadResidentPortalUrl( $intPropertyId, $intCid, $objClientDatabase, $objWebsite = NULL ) {

		$strResidentPortalUrl = NULL;
		$strArrResidentPortalUrls = [];
		$intCid = ( int ) $intCid;
		if( false == valObj( $objWebsite, 'CWebsite' ) ) {
			$objWebsite	= CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $intPropertyId ], $intCid, $objClientDatabase );
		}

		if( true == valObj( $objWebsite, 'CWebsite' ) ) {
			$strResidentPortalPrefix 	= ( true == valStr( CConfig::get( 'SECURE_HOST_PREFIX' ) ) ) ? CConfig::get( 'SECURE_HOST_PREFIX' ) : 'https://';
			$strResidentPortalSuffix 	= ( true == valStr( CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) ) ) ? CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) : '.residentportal.com';
			$strResidentPortalUrl		= $strResidentPortalPrefix . $objWebsite->getSubDomain() . $strResidentPortalSuffix;

			$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $intCid, $objClientDatabase );

			$strRP40SubDomain = $objProperty->getRp40SubDomain( $objClientDatabase );
			if( true == valStr( $strRP40SubDomain ) ) {
				$strRP40ResidentPortalUrl                       = $strResidentPortalPrefix . $strRP40SubDomain . $strResidentPortalSuffix;
				$strArrResidentPortalUrls['base_url']           = $strRP40ResidentPortalUrl;
				$strArrResidentPortalUrls['view_login']         = $strRP40ResidentPortalUrl . '/auth';
				$strArrResidentPortalUrls['forgot_password']    = $strRP40ResidentPortalUrl . '/auth/reset-password';
				$strArrResidentPortalUrls['show_enroll']        = $strRP40ResidentPortalUrl . '/auth';
				$strArrResidentPortalUrls['unsubscribe']        = $strRP40ResidentPortalUrl . '/auth';
			} else {
				$strEncryptedCustomerId = CEncryption::encrypt( $this->getId(), CONFIG_KEY_LOGIN_USERNAME );
				$strArrResidentPortalUrls['base_url'] = $strResidentPortalUrl;
				$strArrResidentPortalUrls['view_login'] = $strResidentPortalUrl . '/resident_portal/?module=authentication&action=view_login';
				$strArrResidentPortalUrls['forgot_password'] = $strResidentPortalUrl . '/resident_portal/?module=forgot_password&action=forgot_password&return_url=&kill_session=1';
				$strArrResidentPortalUrls['show_enroll'] = $strResidentPortalUrl . '/resident_portal/?module=authentication&show_enroll';
				$strArrResidentPortalUrls['unsubscribe'] = $strResidentPortalUrl . '/resident_portal?module=authentication&action=view_login&system_email_block[email_block_type_id]=' . CEmailBlockType::RESIDENT_PORTAL_INVITE_EMAIL . '&system_email_block[cid]=' . $intCid . '&system_email_block[encrypted_customer_id]=' . $strEncryptedCustomerId . '&system_email_block[email_address]=';
			}
		}

		return $strArrResidentPortalUrls;
	}

	public function logCustomerActivity( $intEventType, $intPropertyId, $intCompanyUserId = SYSTEM_USER_ID, $objPayment = NULL, $strPaymentAccessType = NULL, $objClientDatabase ) {

		if( true == is_null( $intPropertyId ) ) {
			trigger_error( 'Failed to insert event for the Customer : ' . $this->getId(), E_USER_WARNING );
			return false;
		}

		$arrmixData = [];
        $objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCustomCompanyUserByIdByCid( $intCompanyUserId, $this->getCid(), $objClientDatabase );

		$objEventLibrary = new CEventLibrary();

		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objClientDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $this ], $intEventType );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setPropertyId( $intPropertyId );
		$objEvent->setEventDatetime( 'NOW()' );

		if( true == is_null( $objPayment ) ) {
			trigger_error( 'Failed to load payment object for the customer : ' . $this->getId(), E_USER_WARNING );
			return false;
		}

		$arrmixData['payment_id'] = $objPayment->getId();
		$intLeaseIntervalId = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchActiveLeaseIntervalIdByLeaseIdByCid( $objPayment->getLeaseId(), $objPayment->getCid(), $objClientDatabase );
		if( true == valId( $intLeaseIntervalId ) ) {
			$objEvent->setLeaseIntervalId( $intLeaseIntervalId );
		}
		$objEvent->setLeaseId( $objPayment->getLeaseId() );
		switch( $intEventType ) {

			case CEventType::PAYMENT_RECEIPT:
				if( true == is_null( $objPayment ) ) {
					trigger_error( 'Failed to load payment object for the customer : ' . $this->getId(), E_USER_WARNING );
					return false;
				}

				$arrmixData['user_name'] = $objCompanyUser->getUsername();
				$arrmixData['payment_status_type_id'] = $objPayment->getPaymentStatusTypeId();
				break;

			case CEventType::RECURRING_PAYMENT:
				if( true == is_null( $objPayment ) ) {
					trigger_error( 'Failed to load payment object for the customer : ' . $this->getId(), E_USER_WARNING );
					return false;
				}

				$arrintEventSubTypes = [
					'pre-processing reminder'	=> CEventSubType::RECURRING_PAYMENT_PRE_PROCESSING_REMINDER,
					'created'					=> CEventSubType::RECURRING_PAYMENT_CREATED,
					'edited'					=> CEventSubType::RECURRING_PAYMENT_EDITED,
					'deleted'					=> CEventSubType::RECURRING_PAYMENT_DELETED
				];

				if( true == in_array( $strPaymentAccessType, [ 'updated', 'terms_updated' ] ) ) {
					$arrmixData['payment_access_type'] = $strPaymentAccessType;
				} else {
					$objEvent->setEventSubTypeId( $arrintEventSubTypes[$strPaymentAccessType] );
				}
				break;

			case CEventType::PAYMENT_SUBMITTED:
				if( true == valObj( $objPayment, 'CPayment' ) ) {
					trigger_error( 'Failed to load payment object for the customer : ' . $this->getId(), E_USER_WARNING );
					return false;
				}

				$arrmixData['payment_status_type_id'] = $objPayment->getPaymentStatusTypeId();
				$arrmixData['ps_product_id'] = $objPayment->getPsProductId();
				$arrmixData['ps_product_option_id'] = $objPayment->getPsProductOptionId();
				break;

			default:
				// for RENT_REMINDER
				break;
		}

		$objEventLibraryDataObject->setData( $arrmixData );
		$objEventLibrary->buildEventDescription( $this );

		if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objClientDatabase ) ) {
			trigger_error( 'Failed to insert event for the Customer : ' . $this->getId(), E_USER_WARNING );
		}
	}

	public function deleteCustomerVehicles( $intCustomerId, $intCid, $objDatabase ) {
		return CLeaseCustomerVehicles::deleteLeaseCustomerVehiclesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase );
	}

	public function deleteCustomerContactsByCustomerContactTypeIds( $intCustomerId, $arrintCustomerContactTypeIds, $intCid, $objDatabase ) {
		return CCustomerContacts::deleteCustomerContactsByCustomerIdByCustomerContactTypeIdsByCid( $intCustomerId, $arrintCustomerContactTypeIds, $intCid, $objDatabase );
	}

	// To unset office number if Resident office number exists in array

	public function unsetOfficeNumberIfResidentOfficeNumberExists( $arrobjPropertyPhoneNumbers ) {

		$arrobjPropertyPhoneNumbers = rekeyObjects( 'PhoneNumberTypeId', $arrobjPropertyPhoneNumbers );
		if( true == array_key_exists( CPhoneNumberType::RESIDENT_OFFICE, $arrobjPropertyPhoneNumbers ) && true == array_key_exists( CPhoneNumberType::OFFICE, $arrobjPropertyPhoneNumbers ) )
			unset( $arrobjPropertyPhoneNumbers[CPhoneNumberType::OFFICE] );
		return $arrobjPropertyPhoneNumbers;
	}

	public function insertOrUpdateCustomerAppLoginToken( $intCustomerId, $strOldRememberMeToken, $strNewRememberMeToken, $boolIsInsertToken, $objDatabase ) {

		if( true == $boolIsInsertToken ) {
			$objCustomerApploginToken = new CCustomerApploginToken();
			$objCustomerApploginToken->setCid( $this->m_intCid );
			$objCustomerApploginToken->setCustomerId( $intCustomerId );
			$objCustomerApploginToken->setVerifiedOn( 'NOW()' );
		} else {
			$objCustomerApploginToken = CCustomerAppLoginTokens::fetchCustomerAppLoginTokenByCustomerIdByCidByToken( $intCustomerId, $this->m_intCid, $strOldRememberMeToken, $objDatabase );
		}

		if( false == valObj( $objCustomerApploginToken, 'CCustomerApploginToken' ) ) {
			$this->addErrorMsg( new CErrorMsg( 312, NULL, 'Failed to load Customer App Login Token object.' ) );
			return false;
		}

		$objCustomerApploginToken->setRememberMeToken( $strNewRememberMeToken );

		switch( NULL ) {
			default:

			if( true == $boolIsInsertToken ) {
				if( false == $objCustomerApploginToken->validate( VALIDATE_INSERT ) ) {
					$this->addErrorMsgs( $objCustomerApploginToken->getErrorMsgs() );
					return false;
				}

				if( false == $objCustomerApploginToken->insert( CCompanyUser::RESIDENT_SYNC_APP, $objDatabase ) ) {
					$this->addErrorMsgs( new CErrorMsg( 312, NULL, 'Failed to insert customer info.' ) );
					return false;
				}

			} else {
				if( false == $objCustomerApploginToken->validate( VALIDATE_UPDATE ) ) {
					$this->addErrorMsgs( $objCustomerApploginToken->getErrorMsgs() );
					return false;
				}

				if( false == $objCustomerApploginToken->update( CCompanyUser::RESIDENT_SYNC_APP, $objDatabase ) ) {
					$this->addErrorMsgs( new CErrorMsg( 312, NULL, 'Failed to update customer info.' ) );
					return false;
				}
			}

		}
		return true;
	}

	public function deleteCustomerAppLoginToken( $intCustomerId, $strRememberMeToken, $objDatabase ) {

		$objCustomerApploginToken = CCustomerAppLoginTokens::fetchCustomerAppLoginTokenByCustomerIdByCidByToken( $intCustomerId, $this->m_intCid, $strRememberMeToken, $objDatabase );

		if( false == valObj( $objCustomerApploginToken, 'CCustomerApploginToken' ) ) {
			$this->addErrorMsg( new CErrorMsg( 312, NULL, 'Failed to load Customer App Login Token object.' ) );
			return false;
		}

		switch( NULL ) {
			default:

				if( false == $objCustomerApploginToken->validate( VALIDATE_DELETE ) ) {
					$this->addErrorMsgs( $objCustomerApploginToken->getErrorMsgs() );
					return false;
				}

				if( false == $objCustomerApploginToken->delete( CCompanyUser::RESIDENT_SYNC_APP, $objDatabase ) ) {
					$this->addErrorMsgs( new CErrorMsg( 312, NULL, 'Failed to delete customer info.' ) );
					return false;
				}

		}
		return true;

	}

	public function loadDocumentManager( $objDatabase, $intCompanyUserId ) {

		if( true == valObj( $this->m_objDocumentManager, 'IDocumentManager' ) ) {
			return true;
		}

		if( true == is_null( $this->getCid() ) || false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Unable to initialize document manager. Invalid client or database object', E_USER_WARNING );
			return false;
		}

		$this->m_objDocumentManager = CDocumentManagerFactory::createDocumentManager( CDocumentManagerFactory::DOCUMENT_MANAGER_SECURE_FILE_SYSTEM, [ $this->getCid(), $intCompanyUserId, NULL, $objDatabase, NULL ] );

		return true;
	}

	public function loadObjectStorageGateway() {

		if( true == valObj( $this->m_objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			return true;
		}

		if( true == is_null( $this->getCid() ) ) {
			trigger_error( 'Unable to initialize Object Storage Gateway. Invalid client object', E_USER_WARNING );
			return false;
		}

		$this->m_objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );

		return true;
	}

	public function loadEmailDatabase() {
		if( false == valObj( $this->m_objEmailDatabase, 'CDatabase' ) ) {
			$this->m_objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER, CDatabaseServerType::POSTGRES, false );
		}
		return $this->m_objEmailDatabase;
	}

	private function loadAdminDatabase() {
		if( false == valObj( $this->m_objAdminDatabase, 'CDatabase' ) ) {
			$this->m_objAdminDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::ADMIN, CDatabaseUserType::PS_DEVELOPER );
		}

		if( false == is_resource( $this->m_objAdminDatabase->getHandle() ) ) {
			$this->m_objAdminDatabase->open();
		}

		return $this->m_objAdminDatabase;
	}

	public function checkExistingApplicant( $arrobjExistingApplicants ) {
		if( true == valArr( $arrobjExistingApplicants ) ) {
			foreach( $arrobjExistingApplicants as $objExistingApplicant ) {
				if( $this->getId() == $objExistingApplicant->getCustomerId() ) {
					return $objExistingApplicant;
				}
			}
		}
		return NULL;
	}

	public function fetchSimplePaymentEligibleLeaseIdsByLeaseIdsByPropertyPaymentTypePlatformIdByCid( $arrintLeaseIds, $intPropertyPaymentTypePlatformId, $intCid, $objDatabase ) {
		$arrintPaymentEligibleLeaseIds = [];

		if( false == valArr( $arrintLeaseIds ) ) return $arrintPaymentEligibleLeaseIds;

		if( CPaymentAllowanceType::BLOCK_ALL != $this->getPaymentAllowanceTypeId() ) {
			$arrmixPropertyPaymentTypePlatformRules = [
				$intPropertyPaymentTypePlatformId => true
			];

			$boolIsCertifiedFunds				= ( CPaymentAllowanceType::CASH_EQUIVALENT == $this->getPaymentAllowanceTypeId() ) ? true : NULL;
			$arrmixPropertyPaymentTypeFilter	= CPropertyPaymentType::getPropertyPaymentTypeFilter( $arrmixPropertyPaymentTypePlatformRules, $boolIsCertifiedFunds );
			$arrintPaymentEligibleLeaseIds		= \Psi\Eos\Entrata\CLeases::createService()->fetchSimplePaymentEligibleLeaseIdsByLeaseIdsByPropertyPaymentTypeFilterByCid( $arrintLeaseIds, $arrmixPropertyPaymentTypeFilter, $intCid, $objDatabase );
		}

		return $arrintPaymentEligibleLeaseIds;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

	public function getCustomerEmailAddress() {
		return ( true == valStr( $this->getUsername() ) ? $this->getUsername() : $this->getEmailAddress() );
	}

	public function generatePassword() {
		// Generate password with password complexity policy
		$strPassword		= md5( uniqid( mt_rand(), true ) );
		$strPassword		= \Psi\CStringService::singleton()->substr( $strPassword, 0, 8 );
		$intPasswordCount	= strlen( $strPassword );

		// @TODO: Please remove magic numbers
		for( $intCount = 0; $intCount < $intPasswordCount; $intCount++ ) {
			if( ( int ) ord( $strPassword[$intCount] ) <= 122 && ( int ) ord( $strPassword[$intCount] ) >= 97 ) {
				$strPassword[$intCount] = \Psi\CStringService::singleton()->ucwords( $strPassword[$intCount] );
				break;
			}
		}

		$strPasswordPattern = '/^(?!.*([a-z\d])\1{1})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d\S]{8,}$/';

		if( 1 != preg_match( $strPasswordPattern, $strPassword ) ) {
			$strPassword = $this->generatePassword();
		}

		return $strPassword;
	}

	public function createCustomerPhoneNumber() {

		$objCustomerPhoneNumber = new CCustomerPhoneNumber();
		$objCustomerPhoneNumber->setCid( $this->m_intCid );
		$objCustomerPhoneNumber->setCustomerId( $this->m_intId );

		return $objCustomerPhoneNumber;
	}

	public function fetchCustomerPhoneNumber( $objDatabase, $boolIsPrimary = false, $boolIsSecondary = false ) {
		return \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomerPhoneNumberByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsPrimary, $boolIsSecondary );
	}

	public function getCustomerProfilePictureImageString( $objDatabase ) {
		$strFileString = NULL;
		$strFileData = NULL;
		$objCompanyMediaFile = $this->fetchCompanyMediaFileByMediaTypeId( CMediaType::CUSTOMER_DEFAULT_IMAGE, $objDatabase );

		if( true == valObj( $objCompanyMediaFile, 'CCompanyMediafile' ) ) {
			$strMediaFileUri = PATH_MOUNTS . $objCompanyMediaFile->getFullsizeUri();
			if( true == valFile( $strMediaFileUri ) ) {
				$strFileData = file_get_contents( $strMediaFileUri );
			}
			$strFileString = base64_encode( $strFileData );
			$strFileString = ( false == is_null( $strFileString ) ) ? $strFileString : 'Unable to encode Logo file.';
		}

		return $strFileString;
	}

	public function getCustomerProfilePictureImageUrl( $objDatabase ) {
		$strMediaFileUri = NULL;
		$objCompanyMediaFile = $this->fetchCompanyMediaFileByMediaTypeId( CMediaType::CUSTOMER_DEFAULT_IMAGE, $objDatabase );
		if( true == valObj( $objCompanyMediaFile, 'CCompanyMediafile' ) ) {
			$strMediaFileUri = $objCompanyMediaFile->getCachedImageOrThumbnailerPath( 100, 100 );
		}
		return $strMediaFileUri;
	}

	public function fetchUndeletedCustomerGuestsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		return ( array ) CCustomerGuests::fetchUndeletedCustomerGuestsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase );
	}

	public function updateRoommatesByEmailAddressByLeaseIdByCid( $strEmailAddress, $intLeaseId, $intCid, $objDatabase, $intCompanyUserId = NULL ) {

		if( false == valId( $intLeaseId ) ) return true;
		$arrobjRoommates = ( array ) CRoommates::fetchRoommatesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase );

		$boolIsValid = true;

		foreach( $arrobjRoommates as $objRoommate ) {
			$objRoommate->setEmailAddress( $strEmailAddress );
			if( true == valObj( $objRoommate, CRoommate::class ) && false == $objRoommate->update( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objRoommate->getErrorMsgs() );
				$boolIsValid = false;
				break;
			}
		}
		return $boolIsValid;

	}

	public function dedupeOrCreateOrganizationCustomer( $objOrgnizationCustomer, $objDatabase, $isDedupe= true ) {
		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByCompanyNameByAddressByCid( $objOrgnizationCustomer, $objDatabase, $isDedupe );

		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			return $objCustomer;
		}
		return $objOrgnizationCustomer;
	}

	public function dedupeOrCreatePrimaryCustomer( $objPrimaryCustomer, $intCid, $objDatabase ) {
		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByEmailByCid( $objPrimaryCustomer, $intCid, $objDatabase );
		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			return $objCustomer;
		}
		return $objPrimaryCustomer;

	}

	public function bindDetails( $arrmixDetails ) {
		$arrmixDetails = ( object ) $arrmixDetails;
		$this->setDetails( $arrmixDetails );
	}

	public function getAgentAccessPortalLink() {

		$arrmixCustomData = [ 'username' => $this->getEmailAddress() ];
		$intTokenExpirationTime = 24 * 60 * 60 * 30; // Agent Access signup email link expiration time is 30 days
		$boolIsSuccess    = true;
		$strJwtToken      = '';
		$strErrorMessages = '';
		$strAgentAccessPortalUrl = '';
		try {
			// create jwt token
			$objJwtToken = new \Psi\Libraries\UtilJwt\CJsonWebToken();

			// setting payload data encrypted
			if( false == is_null( $arrmixCustomData ) ) {
				$objJwtToken->setPayloadClaim( 'details', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( json_encode( $arrmixCustomData ), \CConfig::get( 'SODIUM_KEY_ENTRATA_JWT_TOKEN' ) ) );
			}
			$strJwtToken = $objJwtToken->generateToken( $objJwtToken::ALGO_HS256, \CConfig::get( 'SODIUM_KEY_ENTRATA_JWT_TOKEN' ), $intTokenExpirationTime );
		} catch( \Psi\Libraries\UtilJwt\CJsonWebTokenException $objJsonWebToken ) {
			$boolIsSuccess    = false;
		} catch( \Psi\Libraries\Cryptography\Exceptions\CCryptoInvalidException $objCryptoException ) {
			$boolIsSuccess    = false;
		} catch( \Exception $objException ) {
			$boolIsSuccess    = false;
		}

		$strDomain = 'https://agentaccessglobal.com';

		if( false == is_null( \CConfig::get( 'agentaccess_domain' ) ) && true == $boolIsSuccess ) {
			$strDomain = \CConfig::get( 'secure_host_prefix' ) . \CConfig::get( 'agentaccess_domain' );
			$strAgentAccessPortalUrl = $strDomain . '/authentication/sign_up_user?token=' . $strJwtToken;
		} elseif( true == $boolIsSuccess ) {
			$strAgentAccessPortalUrl = $strDomain . '/authentication/sign_up_user?token=' . $strJwtToken;
		}

		return $strAgentAccessPortalUrl;
	}

	public function getAgentAccessEmailFooterText() {
		$strSpanContent = '<span style="font:normal normal normal 10px/15px arial;color:#fff;"> ' . __( 'Copyright {%t, 0, DATE_NUMERIC_YEAR } &nbsp; {%s, 1}. All rights reserved.', [ date( 'Y-m-d' ), CONFIG_COMPANY_NAME_FULL ] ) . ' </span>';
		$strAgentAccessEmailFooterText = '<div id="footer" >
												<table cellpadding="0" cellspacing="0" border="0" width="580" style="width:580px;">
													<tr>
														<td bgcolor="#152849" align="center" style="padding:5px 15px 5px 15px; font:normal normal normal 11px/18px arial;color:#808080;" height="25px">
															' . $strSpanContent . '
														</td>
													</tr>
												</table>
											</div>';

		return $strAgentAccessEmailFooterText;
	}

	public function getGroupName() {
		$strSql = '';
		$arrmixExtraParameters = $this->getExtraParameters();
		if( true == valArrKeyExists( $arrmixExtraParameters, 'lease_id' ) ) {
			$strSql = 'SELECT 
						COALESCE( cl.name_last || \',\' || cl.name_first, cl.name_last, cl.name_first, cl.company_name, NULL ) AS group_name
					FROM 
						customers c
						JOIN lease_customers lc ON ( c.id = lc.customer_id AND c.cid = lc.cid )
						JOIN cached_leases cl ON ( lc.lease_id = cl.id AND lc.cid = cl.cid )
						JOIN organization_contracts oc ON ( cl.organization_contract_id = oc.id AND oc.cid = cl.cid )
					WHERE cl.id = ' . $arrmixExtraParameters['lease_id'] .
			          'AND cl.active_lease_interval_id = ' . $arrmixExtraParameters['lease_interval_id'] .
			          ' AND c.id = ' . $this->getId();
		} else {
			$strSql = 'SELECT 
						COALESCE( c.name_last || \',\' || c.name_first, c.name_last, c.name_first, c.company_name, NULL ) AS group_name
					FROM 
						customers c
						JOIN customer_connections cc ON ( c.id = cc.connected_customer_id AND c.cid = cc.cid )
					WHERE cc.customer_id = ' . $this->getId() . ' OR cc.connected_customer_id = ' . $this->getId();
		}

		$arrstrGroupName = \fetchData( $strSql, $this->m_objDatabase );

		if( false == \valStr( $arrstrGroupName[0]['group_name'] ) ) {
			return NULL;
		}
		return $arrstrGroupName[0]['group_name'];
	}

	private function createCustomerPasswordResetSystemMessage( $objClient, $arrmixExtraMetaData, $objDatabase ) {
		if( false == valObj( $objClient, CClient::class ) ) {
			$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $this->getCid(), $objDatabase );
		}

		$objLeaseCustomer = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		if( false == valObj( $objLeaseCustomer, CLeaseCustomer::class ) ) {
			return false;
		}

		$objSystemMessageLibrary = new CSystemMessageLibrary();
		$objSystemMessageLibrary->setClient( $objClient );
		$objSystemMessageLibrary->setDatabase( $objDatabase );
		$objSystemMessageLibrary->setPropertyId( $objLeaseCustomer->getPropertyId() );
		$objSystemMessageLibrary->setSystemEmailTypeId( CSystemEmailType::CUSTOMER_PROFILE_UPDATE );
		$objSystemMessageLibrary->setCompanyUserId( SYSTEM_USER_ID );
		$objSystemMessageLibrary->setSystemMessageKey( 'FORGOT_PASSWORD_ASSISTANCE' );
		$objSystemMessageLibrary->setSystemMessageAudienceIds( [ CSystemMessageAudience::RESIDENT ] );
		$objSystemMessageLibrary->setLeaseCustomers( [ $objLeaseCustomer ] );
		$objSystemMessageLibrary->setIsResident( true );
		$objSystemMessageLibrary->setExtraMetaData( $arrmixExtraMetaData );
		$objSystemMessageLibrary->setIsSkipMigrationModeCheck( true );

		return $objSystemMessageLibrary->execute();
	}

	public function prepareContactPointPreviewEmail( $strSystemMessageKey, $objSystemMessage, $objArPayment, $arrintPaymentIds, $objClient, $objClientDatabase ) {
		$boolIsEntrataDefault = false;
		$objScheduledTasks = \Psi\Eos\Entrata\CScheduledTasks::createService()->fetchScheduledTaskBySystemMessageKeyByPropertyIdByCid( $strSystemMessageKey, $objArPayment->getPropertyId(), $objClient->getId(), $objClientDatabase );
		if( true == valObj( $objScheduledTasks, 'CScheduledTask' ) && true == $objScheduledTasks->getIsEntrataDefault() ) {
			$boolIsEntrataDefault = true;
		}
		$intArPaymentId                 = $objArPayment->getId();
		$intCustomerId                  = $objArPayment->getOrFetchCustomer( $objClientDatabase )->getId();
		$intLeaseId                     = $objArPayment->getOrFetchLease( $objClientDatabase )->getId();
		$objRecipient = \Psi\Eos\Entrata\CLeaseCustomers::createservice()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $objClient->getId(), $objClientDatabase );
		$boolIsNonElecPayment = in_array( $objArPayment->getPaymentTypeId(), [ \CPaymentType::CASH, \CPaymentType::MONEY_ORDER, \CPaymentType::CHECK ] );

		$arrmixExtraMetaData = [
			'ar_payment_id' => $intArPaymentId,
			'ar_payments' => [
				'split_ar_payment_ids' => $arrintPaymentIds
			],
			'properties' => [
				'ar_payment_id' => $intArPaymentId,
				'is_non_elec_payment' => $boolIsNonElecPayment
			]
		];

		return $this->prepareContactPointContent( $objSystemMessage, $boolIsEntrataDefault, $objClientDatabase, $objRecipient, $arrmixExtraMetaData );
	}

	public function prepareContactPointContent( $objSystemMessage, $boolIsEntrataDefault, $objDatabase, $objRecipient, $arrmixExtraMetaData = NULL ) {
		$arrmixEmailDetails = [];

		$objSystemMessageContentLibrary = new CSystemMessageContentLibrary();
		$objSystemMessageContentLibrary->setSystemMessage( $objSystemMessage );
		$objSystemMessageContentLibrary->setClientDatabase( $objDatabase );
		$objSystemMessageContentLibrary->setIsEntrataDefault( $boolIsEntrataDefault );
		$objSystemMessageContentLibrary->setSystemEmailTypeId( CSystemEmailType::AR_PAYMENT_NOTIFICATION );
		$objSystemMessageContentLibrary->setIsPreview( true );

		$strHtmlEmailContent = $objSystemMessageContentLibrary->getEmailContent();
		if( false == is_null( $objRecipient ) ) {
			$objSystemMessageContentLibrary->setRequiredParameters( $objRecipient, $arrmixExtraMetaData );
			$objSystemMessageContentLibrary->setRecipient( $objRecipient );
		}

		$strTextContent = $objSystemMessageContentLibrary->replaceMergeFields( $strHtmlEmailContent );
		$arrmixEmailDetails['content'] = $strTextContent;
		return $arrmixEmailDetails;
	}

	public function getCustomerAchAndPlaidAccounts() {
		$this->m_arrobjACHCustomerPaymentAccounts = \Psi\Eos\Entrata\CCustomerPaymentAccounts::createService()->fetchOrderedCustomerPaymentAccountsByCustomerIdByPaymentTypeIdsByCid( $this->getId(), [ CPaymentType::ACH ], $this->getCid(), $this->m_objDatabase );
		if( false == valArr( $this->m_arrobjACHCustomerPaymentAccounts ) ) {
			return NULL;
		}
		$arrmixPlaidVerificationStatus = [ 'pending_automatic_verification', 'pending_manual_verification', 'verification_expired' ];
		foreach( $this->m_arrobjACHCustomerPaymentAccounts as $intKey => $objCustomerPaymentAccount ) {
			$arrstrDetails = json_decode( json_encode( $objCustomerPaymentAccount->getDetails() ), true );
			if( true == valArr( $arrstrDetails['plaid_account'] ) ) {
				if( true == valStr( $arrstrDetails['plaid_account']['verification_status'] ) && true == in_array( $arrstrDetails['plaid_account']['verification_status'], $arrmixPlaidVerificationStatus ) ) {
					unset( $this->m_arrobjACHCustomerPaymentAccounts[$intKey] );
				}

				$intCustomerPlaidItemId = ( true == valId( $arrstrDetails['plaid_account']['customer_plaid_item_id'] ) ) ? $arrstrDetails['plaid_account']['customer_plaid_item_id'] : '';
				// TODO WE NEED TO REMOVE FETCH FUCNTION FROM FOREACH
				$arrobjCustomerPlaidItems = \Psi\Eos\Entrata\CCustomerPlaidItems::createService()->fetchCustomCustomerPlaidItemByIdsByCustomerId( [ $intCustomerPlaidItemId ], $this->getId(), $this->m_objDatabase );
				if( true == valArr( $arrobjCustomerPlaidItems ) ) {
					foreach( $arrobjCustomerPlaidItems as $intItemId => $objCustomerPlaidItem ) {
						if( true == valStr( $objCustomerPlaidItem->getConsentExpirationTime() ) && strtotime( $objCustomerPlaidItem->getConsentExpirationTime() ) >= time() ) {
							unset( $this->m_arrobjACHCustomerPaymentAccounts[$intKey] );
						}

						if( false == is_null( $objCustomerPlaidItem->getError() ) )
							$arrmixError = json_decode( json_encode( $objCustomerPlaidItem->getError() ), true );
							if( true == valArr( $arrmixError ) && true == valStr( $arrmixError['error']['error_code'] ) ) {
								if( 'ITEM_NO_ERROR' == $arrmixError['error']['error_code'] || 'AUTH_ERROR' == $arrmixError['error']['error_code']
								    || 'INSTITUTION_NOT_AVAILABLE' == $arrmixError['error']['error_code'] || 'INSTITUTION_NO_LONGER_SUPPORTED' == $arrmixError['error']['error_code'] ) {
									unset( $this->m_arrobjACHCustomerPaymentAccounts[$intKey] );
								}
							}
					}
				}
			}
		}
		return $this->m_arrobjACHCustomerPaymentAccounts;
	}

	/**
	 * @param int[] $arrintPropertyIds
	 * @param \CDatabase $objDatabase
	 * @return bool
	 */
	public function canTenantLogin( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}

		$objDataset = $objDatabase->createDataset();
		$objDataset->initialize();

		$strSql = 'SELECT
						c.*, cps.*
					FROM
						customers c
						JOIN customer_portal_settings cps ON (c.cid = cps.cid AND c.id = cps.customer_id )
						JOIN lease_customers lc ON (cps.cid = lc.cid AND cps.customer_id =lc.customer_id )
						JOIN leases l ON ( lc.cid = l.cid AND lc.lease_id =l.id )
					WHERE
						c.cid = ' . $this->m_intCid . '
						AND c.id = ' . ( int ) $this->m_intId . '
						AND ( l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							OR l.property_id IN ( SELECT p.id FROM properties p WHERE p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND p.cid = ' . $this->m_intCid . ' ) )';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		if( false === isset( $arrstrResult[0]['id'] ) || false === is_numeric( $arrstrResult[0]['id'] ) ) {
			return false;
		}

		// Check to make sure this customer is allowed to login
		if( true == $arrstrResult[0]['dont_allow_login'] ) {
			return false;
		}

		return true;
	}

	public function insertOrUpdateCustomerPhoneNumbers( $intCurrentUserId, $objDatabase ) {

		$boolIsValid = true;
		$boolIsDelete = true;

		if( true == $this->getIsCustomerDeduped() ) {
			$boolIsDelete = false;
		}

		$arrobjCustomerPhoneNumbers = ( new CCustomerPhoneNumber() )->loadCustomerPhoneNumbers( ( array ) $this->getCustomerPhoneNumbers(), $this->getId(), $this->getCid(), $objDatabase );
		if( true == valArr( $arrobjCustomerPhoneNumbers ) && false == ( new CCustomerPhoneNumber() )->insertOrUpdateCustomerPhoneNumber( $arrobjCustomerPhoneNumbers, $intCurrentUserId, $this->getCid(), $objDatabase, $boolIsDelete, $this->getIsOnlyAdditionalPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, __( 'Failed to insert or update customer phone number.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getAvailableCustomerPhoneNumber( $objClientDatabase ) {
		$strCustomerPhoneNumber = '';
		$arrobjCustomerPhoneNumbers = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchAllCustomerPhoneNumbersByCustomerIdByCidForRP( $this->getId(), $this->getCid(), $objClientDatabase );

		if( true == \Psi\Libraries\Utilfunctions\valArr( $arrobjCustomerPhoneNumbers ) ) {
			foreach( $arrobjCustomerPhoneNumbers as $objCustomerPhoneNumber ) {
				if( true == \Psi\Libraries\Utilfunctions\valObj( $objCustomerPhoneNumber, \CCustomerPhoneNumber::class ) ) {
					if( true == $objCustomerPhoneNumber->getIsPrimary() ) {
						$strCustomerPhoneNumber = $objCustomerPhoneNumber->getPhoneNumber();
						break;
					} else {
						$strCustomerPhoneNumber = $objCustomerPhoneNumber->getPhoneNumber();
					}
				}
			}
		}

		return $strCustomerPhoneNumber;
	}

	public function getAvailableCustomerPhoneNumbersAndTypes( $objClientDatabase ) {
		$arrmixCustomerPhoneNumbers = [];
		$arrobjCustomerPhoneNumbers = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchAllCustomerPhoneNumbersByCustomerIdByCidForRP( $this->getId(), $this->getCid(), $objClientDatabase );

		if( true == \Psi\Libraries\Utilfunctions\valArr( $arrobjCustomerPhoneNumbers ) ) {
			foreach( $arrobjCustomerPhoneNumbers as $objCustomerPhoneNumber ) {
				if( true == \Psi\Libraries\Utilfunctions\valObj( $objCustomerPhoneNumber, \CCustomerPhoneNumber::class ) && true == \Psi\Libraries\Utilfunctions\valStr( $objCustomerPhoneNumber->getPhoneNumber() ) ) {
					if( \CPhoneNumberType::HOME == $objCustomerPhoneNumber->getPhoneNumberType() ) {
						$arrmixCustomerPhoneNumbers['home'] = $objCustomerPhoneNumber->getPhoneNumber();
					} elseif( \CPhoneNumberType::MOBILE == $objCustomerPhoneNumber->getPhoneNumberType() ) {
						$arrmixCustomerPhoneNumbers['mobile'] = $objCustomerPhoneNumber->getPhoneNumber();
					} elseif( \CPhoneNumberType::OFFICE == $objCustomerPhoneNumber->getPhoneNumberType() ) {
						$arrmixCustomerPhoneNumbers['work'] = $objCustomerPhoneNumber->getPhoneNumber();
					}
				}
			}
		}

		return $arrmixCustomerPhoneNumbers;
	}

	public function sendCreateTenantCustomerPasswordLink( $strSecureBaseName, $objProperty = NULL, $objClient = NULL, $objDatabase, $objEmailDatabase, $boolIsMultiClient = false, $boolIsTenant = false, $arrobjTenantGlobalUsers = [] ) {

		$arrmixExtraMetaData      = [
			'lease_customers' => [ 'is_required_new_version' => false, 'is_tenant' => $boolIsTenant, 'is_send_to_group' => false ],
			'properties'      => [ 'is_required_new_version' => false, 'is_customer_password_reset' => true, 'is_tenant' => $boolIsTenant, 'is_send_to_group' => false ]
		];

		if( false == valObj( $objEmailDatabase, CDatabase::class ) ) {
			$objEmailDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER );
		}
		$objSystemEmail                = new CSystemEmail();

		$strCreateCustomerPasswordLink = CONFIG_HOST_PREFIX . 'tenants' . CConfig::get( 'entrata_domain_postfix' ) . '/auth/reset-password';

		$arrmixExtraMetaData['lease_customers'] = array_merge( $arrmixExtraMetaData['lease_customers'], [ 'customer_password_link' => $strCreateCustomerPasswordLink, 'system_email_id' => $objSystemEmail->getId() ] );
		$arrmixExtraMetaData['properties']      = array_merge( $arrmixExtraMetaData['properties'], [ 'is_required_address' => true ] );

		// Load HTML & Text Versions of Emails from Smarty Template
		$strHtmlEmailOutput = $this->buildHtmlCreateTemporaryPasswordEmailContent( $strSecureBaseName, $objProperty, $objClient, $objDatabase, $objPropertyEmailRule = NULL, $boolIsMultiClient, false, 'Tenant Portal', $arrobjTenantGlobalUsers );

		$objSystemEmailRecipient = $this->buildSystemEmailRecipient( $this->m_strEmailAddress, $objEmailDatabase );
		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( \CSystemEmailType::CUSTOMER_PROFILE_UPDATE );
		$objSystemEmail->setSubject( __( 'Tenant Portal Forgot Password Assistance' ) );
		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		$objSystemEmail->setToEmailAddress( $objSystemEmailRecipient->getEmailAddress() );
		$objSystemEmail->setFromEmailAddress( \CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		return $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase );
	}

	public function sendCreateGroupCustomerPasswordLink( $strSecureBaseName, $objProperty = NULL, $objClient = NULL, $objDatabase, $objEmailDatabase, $arrobjGroupUsers = [] ) {

		if( false == valObj( $objEmailDatabase, CDatabase::class ) ) {
			$objEmailDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER );
		}
		$objSystemEmail                = new CSystemEmail();

		// Load HTML & Text Versions of Emails from Smarty Template
		$strHtmlEmailOutput = $this->buildHtmlCreateTemporaryPasswordEmailContent( $strSecureBaseName, $objProperty, $objClient, $objDatabase, $objPropertyEmailRule = NULL, false, false, 'ReservationHub', $arrobjGroupUsers );

		$objSystemEmailRecipient = $this->buildSystemEmailRecipient( $this->m_strEmailAddress, $objEmailDatabase );
		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( \CSystemEmailType::CUSTOMER_PROFILE_UPDATE );
		$objSystemEmail->setSubject( __( 'Reservation Hub Forgot Password Assistance' ) );
		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		$objSystemEmail->setToEmailAddress( $objSystemEmailRecipient->getEmailAddress() );
		$objSystemEmail->setFromEmailAddress( \CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		return $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase );
	}

	private function buildSystemEmailRecipient( $strToEmailAddress, $objEmailDatabase ) : \CSystemEmailRecipient {
		$objSystemEmailRecipient = new \CSystemEmailRecipient();
		$objSystemEmailRecipient->setId( $objSystemEmailRecipient->fetchNextId( $objEmailDatabase ) );
		$objSystemEmailRecipient->setCid( $this->getCid() );
		$objSystemEmailRecipient->setRecipientTypeId( CSystemEmailRecipientType::RESIDENT );
		$objSystemEmailRecipient->setEmailAddress( $strToEmailAddress );

		return $objSystemEmailRecipient;
	}

	public function exportInsurance( $objCustomer, $objLease = NULL, $objProperty, $objDatabase ) {
		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::SEND_RENTERS_INSURANCE_POLICIES, SYSTEM_USER_ID, $objDatabase );
		$objGenericWorker->setCustomer( $objCustomer );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setLease( $objLease );
		$intResidentInsurancePolicyId = current( $this->getInsurancePolicies() )->getId();
		$objGenericWorker->setResidentInsurancePolicyId( $intResidentInsurancePolicyId );

		return $objGenericWorker->process();
	}

}
?>
