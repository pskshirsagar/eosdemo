<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStudentPropertyWindowSettings
 * Do not add any new functions to this class.
 */

class CStudentPropertyWindowSettings extends CBaseStudentPropertyWindowSettings {

	public static function fetchAllowEditingRoommatePreferencesSettingByPropertyIdByLeaseStartWindowIdByCid( $intPropertyId, $intLeaseStartWindowId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) || false == is_numeric( $intLeaseStartWindowId ) ) return NULL;

		$strSql = 'SELECT
						spws.allow_editing_roommate_preferences
					FROM
						student_property_window_settings spws
					WHERE
						spws.cid = ' . ( int ) $intCid . '
						AND spws.property_id = ' . ( int ) $intPropertyId . '
						AND spws.lease_start_window_id =' . ( int ) $intLeaseStartWindowId;

		return self::fetchColumn( $strSql, 'allow_editing_roommate_preferences', $objDatabase );

	}

	public static function fetchStudentPropertyWindowSettingByCidByPropertyIdByLeaseStartWindowId( $intCid, $intPropertyId, $intLeaseStartWindowId, $objDatabase ) {

		if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) || false == is_numeric( $intLeaseStartWindowId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						spws.*
					FROM
						student_property_window_settings spws
					WHERE
						spws.cid = ' . ( int ) $intCid . '
						AND spws.property_id = ' . ( int ) $intPropertyId . '
						AND spws.lease_start_window_id =' . ( int ) $intLeaseStartWindowId;

		return self::fetchStudentPropertyWindowSetting( $strSql, $objDatabase );

	}

}
?>