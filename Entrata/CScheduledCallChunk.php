<?php

use Psi\Libraries\ExternalFileUpload\CFileUpload;

class CScheduledCallChunk extends CBaseScheduledCallChunk {

	public function valCallChunkTypeId() {
		$boolIsValid = true;

		// Validation example
		if( true == is_null( $this->getCallChunkTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_chunk_type_id', 'Call file type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyMediaFileId() {
		$boolIsValid = true;

	   // Validation example
		if( true == is_null( $this->getCompanyMediaFileId() ) && false == is_null( $this->getCallChunkTypeId() ) && '2' == $this->getCallChunkTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_media_file_id', 'Wave file is required.' ) );
		}

		return $boolIsValid;
	}

	public function valText() {
		$boolIsValid = true;

	   // Validation example
		if( true == is_null( $this->getText() ) && false == is_null( $this->getCallChunkTypeId() ) && '1' == $this->getCallChunkTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'text', 'Custom text is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCallChunkTypeId();
				$boolIsValid &= $this->valCompanyMediaFileId();
				$boolIsValid &= $this->valText();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid 	= true;
		$boolIsInsert 	= false;

		if( true == is_null( $this->getId() ) && false == is_numeric( $this->getId() ) ) {
			$boolIsInsert = true;
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		if( false == empty( $_FILES['scheduled_call_file']['name'] ) ) {
			$this->setFileName( CFileUpload::cleanFilename( $_FILES['scheduled_call_file']['name'] ) );
			$this->setFilePath( 'scheduled_call/' . $this->getScheduledCallId() );
		}

		if( true == $boolIsValid ) {
			$boolIsValid = $this->uploadFiles( $objDatabase );
		}

		if( true == $boolIsValid && true == $boolIsInsert ) {
			$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} elseif( true == $boolIsValid ) {
			$boolIsValid = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $boolIsValid;
	}

	public function uploadFiles( $objDatabase ) {
		$boolIsValid = true;

		$strFilePath 	= PATH_MOUNTS . 'scheduled_call/' . $this->getScheduledCallId();

		if( false == is_dir( $strFilePath ) ) {

			if( false == CFileIo::recursiveMakeDir( $strFilePath ) ) {
				trigger_error( 'Couldn\'t create directory(' . $strFilePath . ' ). ', E_USER_ERROR );
				return false;
			}
		}

		$objUpload = new CFileUpload();

		$this->m_arrobjFileExtensions	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchAllFileExtensions( $objDatabase );

		foreach( $this->m_arrobjFileExtensions as $objFileExtension ) {
			$arrmixAllowedTypes[] = $objFileExtension->getExtension();
		}

		$objUpload->setAcceptableTypes( $arrmixAllowedTypes );

		$objUpload->setOverwriteMode( CFileUpload::MODE_OVERWRITE );

		if( '' != $_FILES['scheduled_call_file']['name'] ) {

			$strUploadedFile = $objUpload->upload( 'scheduled_call_file', $strFilePath );

			if( false == is_string( $strUploadedFile ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Attachment', 'Invalid file specified for application file.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function deleteScheduledCallFile( $strScheduledCallFileUri ) {
		$boolIsValid = true;

		if( true == file_exists( $strScheduledCallFileUri ) ) {
			if( false == unlink( $strScheduledCallFileUri ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to delete.' ) );
				trigger_error( 'Failed to delete existing application document', E_USER_WARNING );
			}
		}

		return $boolIsValid;
	}

	public function createCallChunk( $arrmixValues, $objCall, $objDatabase ) {

		$strText = $this->getText();

		$strText = str_replace( '<<RESIDENT_FIRST_NAME>>', 	$arrmixValues['customer_first_name'], $strText );
		$strText = str_replace( '<<RESIDENT_LAST_NAME>>', 	$arrmixValues['customer_last_name'], $strText );
		$strText = str_replace( '<<RESIDENT_NAME>>', 		$arrmixValues['customer_first_name'] . ' ' . $arrmixValues['customer_last_name'], $strText );
		$strText = str_replace( '<<PROPERTY_NAME>>', 		$arrmixValues['property_name'], $strText );
		$strText = str_replace( '<<YOUR_COMPANY_NAME>>', 	$arrmixValues['company_name'], $strText );
		$strText = str_replace( '<<MONEY_OWED>>', 		 	$arrmixValues['money_owed'], $strText );
		$strText = str_replace( '<<WEBSITE_URL>>', 			$arrmixValues['website_url'], $strText );

		$objCallChunk = new CCallChunk();
		$objCallChunk->setCallChunkTypeId( $this->getCallChunkTypeId() );
		$objCallChunk->setText( $strText );
		$objCallChunk->setOrderNum( $this->getOrderNum() );

		if( false == is_null( $this->getCompanyMediaFileId() ) ) {
			$objCompanyMediaFile = \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $this->getCompanyMediaFileId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objCompanyMediaFile, 'CCompanyMediaFile' ) ) {
				$arrmixFilePath = explode( '/', $objCompanyMediaFile->getFullsizeUri() );

				CFileIo::recursiveMakeDir( PATH_VOIP_MOUNTS . 'call_files/property_scheduled_calls/' . $objCall->getCid() . '/' . $objCall->getPropertyId() . '/' );

				if( false == CFileIo::copyFile( PATH_MOUNTS . $objCompanyMediaFile->getFullsizeUri(), PATH_VOIP_MOUNTS . 'call_files/property_scheduled_calls/' . $objCall->getCid() . '/' . $objCall->getPropertyId() . '/' . $arrmixFilePath[3] ) ) {
					return false;
				}

				$strFilePath = 'call_files/property_scheduled_calls/' . $objCall->getCid();
				$strFilePath .= ( true == valId( $objCall->getPropertyId() ) ) ? '/' . $objCall->getPropertyId() . '/' : '/';

				$objCallChunk->setFileName( $arrmixFilePath[3] );
				$objCallChunk->setFilePath( $strFilePath );

				chmod( PATH_VOIP_MOUNTS . $objCallChunk->getFilePath() . $objCallChunk->getFileName(), 02775 );
			}
		}

		return $objCallChunk;
	}

}
?>