<?php

class CLeaseExpirationStructure extends CBaseLeaseExpirationStructure {

	protected $m_intPropertiesCount;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName( $objClientDatabase ) {

        $boolIsValid = true;

        $strErrorMsg = __( 'Lease Expiration Group Name' );

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Expiration Group Name is required.' ) ) );
        } elseif( false == is_null( $objClientDatabase ) ) {

			$intConflictingLeaseExpirationStructureCount = \Psi\Eos\Entrata\CLeaseExpirationStructures::createService()->fetchConflictingLeaseExpirationStructureCountByCid( $this, $this->getCid(), $objClientDatabase );

			if( 0 < $intConflictingLeaseExpirationStructureCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '{%s, 0} already exists', [ $strErrorMsg ] ) ) );
				$boolIsValid = false;
			}
        }
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        if( true == is_null( $this->getDescription() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objClientDatabase = NULL ) {
        $boolIsValid = true;
        switch( $strAction ) {
        	case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
                break;

            case 'validate_insert_update_lease_expiration_structure':
                $boolIsValid &= $this->valName( $objClientDatabase );
                $boolIsValid &= $this->valDescription( $objClientDatabase );
                break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function getPropertiesCount() {
    	return $this->m_intPropertiesCount;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['propertiescount'] ) ) $this->setPropertiesCount( $arrmixValues['propertiescount'] );

    }

    public function setPropertiesCount( $intPropertiesCount ) {
    	$this->m_intPropertiesCount = $intPropertiesCount;
    }

    /**
     * Other Functions
     */

    public function createLeaseExpirationMonth() {

        $objLeaseExpirationMonth = new CLeaseExpirationMonth();
        $objLeaseExpirationMonth->setDefaults();
        $objLeaseExpirationMonth->setCid( $this->getCid() );

        return $objLeaseExpirationMonth;
    }

}
?>