<?php

class CDelinquencyThresholdType extends CBaseDelinquencyThresholdType {

	const FLAT_AMOUNT                       = 1;
	const FLAT_AMOUNT_OF_RENT               = 2;
	const PERCENT_OF_RENT_SCHEDULED_CHARGES = 3;
	const PERCENT_OF_SCHEDULED_CHARGES	    = 4;
	const DAY_OF_THE_MONTH                  = 5;
	const DAY_AFTER_CHARGES_DUE             = 6;
	const DAY_SINCE_LAST_NOTICE             = 7;
	const DAY_AFTER_FMO                     = 8;
	const DAY_OF_THE_FOLLOWING_MONTH        = 9;

	public static $c_arrintDelinquencyBalanceThresholdTypes    = [ self::FLAT_AMOUNT, self::FLAT_AMOUNT_OF_RENT, self::PERCENT_OF_RENT_SCHEDULED_CHARGES, self::PERCENT_OF_SCHEDULED_CHARGES ];

	public static $c_arrintCustomDelinquencyThresholdTypes     = [ self::FLAT_AMOUNT, self::PERCENT_OF_RENT_SCHEDULED_CHARGES, self::PERCENT_OF_SCHEDULED_CHARGES ];

	public static $c_arrintFlatDelinquencyThresholdTypes       = [ self::FLAT_AMOUNT, self::FLAT_AMOUNT_OF_RENT ];

	public static $c_arrintPercentageDelinquencyThresholdTypes = [ self::PERCENT_OF_RENT_SCHEDULED_CHARGES, self::PERCENT_OF_SCHEDULED_CHARGES ];

	/**
	 *  @FIXME Remove line no 27 & 29 code while commiting code changes for task #  2646637
	 */

	public static $c_arrintDelinquencyNonBalanceThresholdTypes = [ self::DAY_OF_THE_MONTH, self::DAY_AFTER_CHARGES_DUE, self::DAY_SINCE_LAST_NOTICE ];

	public static $c_arrintSmallBalanceThresholdTypes          = [ self::DAY_OF_THE_MONTH, self::DAY_AFTER_CHARGES_DUE ];

	/**
	 * @FIXME remove below code comment while commiting task#  2646637
	 */

	// public static $c_arrintDelinquencyNonBalanceThresholdTypes = [ self::DAY_OF_THE_MONTH, self::DAY_AFTER_CHARGES_DUE, self::DAY_SINCE_LAST_NOTICE , self::DAY_OF_THE_FOLLOWING_MONTH  ];
	// public static $c_arrintSmallBalanceThresholdTypes          = [ self::DAY_OF_THE_MONTH, self::DAY_AFTER_CHARGES_DUE, self::DAY_OF_THE_FOLLOWING_MONTH ];

	public static $c_arrintCollectionsNonBalanceThresholdTypes = [ self::DAY_AFTER_FMO, self::DAY_SINCE_LAST_NOTICE ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getCustomDelinquencyThresholdTypeNameById( $intThresholdTypeId ) {

		$strThresholdTypeName = NULL;

		switch( $intThresholdTypeId ) {
			case self::PERCENT_OF_RENT_SCHEDULED_CHARGES:
				$strThresholdTypeName = __( '% of Rent Scheduled Charges' );
				break;

			case self::PERCENT_OF_SCHEDULED_CHARGES:
				$strThresholdTypeName = __( '% of Scheduled Charges' );
				break;

			default:
				// This is default
				break;
		}

		return $strThresholdTypeName;
	}

	public static function getDelinquencyThresholdTypeNameById( $intThresholdTypeId ) {

		$strThresholdTypeName = NULL;

		switch( $intThresholdTypeId ) {
			case self::FLAT_AMOUNT:
				$strThresholdTypeName = __( 'Flat Amount' );
				break;

			case self::FLAT_AMOUNT_OF_RENT:
				$strThresholdTypeName = __( 'Flat Amount Of Rent' );
				break;

			case self::PERCENT_OF_RENT_SCHEDULED_CHARGES:
				$strThresholdTypeName = __( '% of Rent Scheduled Charges' );
				break;

			case self::PERCENT_OF_SCHEDULED_CHARGES:
				$strThresholdTypeName = __( '% of Scheduled Charges' );
				break;

			case self::DAY_OF_THE_MONTH:
				$strThresholdTypeName = __( 'Day Of The Month' );
				break;

			case self::DAY_AFTER_CHARGES_DUE:
				$strThresholdTypeName = __( 'Day(s) After Charges Due' );
				break;

			case self::DAY_SINCE_LAST_NOTICE:
				$strThresholdTypeName = __( 'Day(s) Since Last Notice' );
				break;

			case self::DAY_AFTER_FMO:
				$strThresholdTypeName = __( 'Day(s) After Financial Move Out' );
				break;

			case self::DAY_OF_THE_FOLLOWING_MONTH:
				$strThresholdTypeName = __( 'Day Of The Following Month' );
				break;

			default:
				// This is default
				break;
		}

		return $strThresholdTypeName;
	}

}
?>