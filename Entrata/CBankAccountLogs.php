<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBankAccountLogs
 * Do not add any new functions to this class.
 */

class CBankAccountLogs extends CBaseBankAccountLogs {

	public static function fetchBankAccountLogsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase, $objBankAccountLogsFilter = NULL ) {

 		$strConditions = '';

		if( true == valObj( $objBankAccountLogsFilter, 'CBankAccountsLogsFilter' ) && true == valArr( $objBankAccountLogsFilter->getActions() ) ) {

			$strConditions .= 'AND
									bal.action IN ( \'' . implode( '\',\'', $objBankAccountLogsFilter->getActions() ) . '\' )';

			if( in_array( 'Note', $objBankAccountLogsFilter->getActions() ) ) {

				$strConditions .= 'UNION
										SELECT
											evn.id,
											evn.notes,
											evn.updated_on,
											evn.created_on,
											ARRAY_TO_STRING ( ARRAY[ ce.name_first, ce.name_last ], \' \' ) AS company_employee_full_name
										FROM
											events evn
											JOIN company_users cu ON ( cu.id = evn.updated_by AND cu.cid = evn.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
											LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
										WHERE
											evn.cid = ' . ( int ) $intCid . '
											AND evn.data_reference_id = ' . ( int ) $intBankAccountId . '
											AND evn.event_type_id = ' . ( int ) CEventType::NOTES . '
											AND evn.is_deleted = false
										ORDER BY
											updated_on DESC';
			} else {
				$strConditions .= 'ORDER BY
										bal.updated_on DESC';
			}
		}
		$strTempSql = 'SELECT
						bal.id,
						bal.action,
						bal.updated_on,
						bal.created_on,
						ARRAY_TO_STRING ( ARRAY[ ce.name_first, ce.name_last ], \' \' ) AS company_employee_full_name
					FROM
						bank_account_logs bal
						JOIN company_users cu ON ( cu.id = bal.updated_by AND cu.cid = bal.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
					WHERE
						bal.cid = ' . ( int ) $intCid . '
						AND bal.bank_account_id = ' . ( int ) $intBankAccountId .
		              $strConditions;
		$strSql = 'SELECT id,
				       CAST (action AS VARCHAR (250)) AS action,
				       updated_on,
				       created_on,
				       company_employee_full_name
					FROM (' . $strTempSql . ' ) temp';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPreviousBankAccountLogByIdByBankAccountIdByCid( $intBankAccountLogId, $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						bal.*
					FROM
						bank_account_logs bal
					WHERE
						bal.cid = ' . ( int ) $intCid . '
						AND bal.bank_account_id = ' . ( int ) $intBankAccountId . '
						AND bal.id < ' . ( int ) $intBankAccountLogId . '
					ORDER BY
						bal.id DESC
					LIMIT 1';

		return self::fetchBankAccountLog( $strSql, $objClientDatabase );
	}

}
?>