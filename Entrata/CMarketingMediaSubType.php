<?php

class CMarketingMediaSubType extends CBaseMarketingMediaSubType {
	const BLOG_POST_MEDIA					= 1;
	const BASIC_WEBSITE_ADS					= 2;
	const MOBILE_WEBSITE_ADS				= 3;
	const TABLET_WEBSITE_ADS				= 4;
	const LAPTOP_WEBSITE_ADS				= 5;
	const DESKTOP_WEBSITE_ADS				= 6;
	const SPECIAL_WEBSITE_ADS				= 7;
	const DIRECTIONS_MAP					= 8;
	const CUSTOM_MAP_MARKER					= 9;
	const SPLASH_IMAGE						= 13;
	const CONVENTIONAL_CTA_MEDIA			= 14;
	const STUDENT_CTA_MEDIA					= 15;
	const FLEXIBLE_LEASING_CTA_MEDIA		= 16;
	const MILITARY_CTA_MEDIA				= 17;
	const FLOORPLAN_PDF						= 18;
	const PROPERTY_LOGO						= 19;
	const PORTFOLIO_LOGO					= 20;
	const OVERVIEW							= 21;
	const FLOORPLAN_2D                      = 22;
	const FLOORPLAN_3D                      = 23;
	const UNIT_MEDIA						= 24;
	const PROPERTY_360_TOUR					= 25;
	const GOOGLE_AND_MATTERPORT_360_TOUR	= 26;
	const PHOTO								= 27;
	const PROPERTY_VIDEO					= 28;
	const AMENITY_MEDIA						= 29;
	const PROPERTY_BUILDING_360_TOUR        = 31;
	const COMPANY_LOGO                      = 32;
	const PROPERTY_BUILDING_PHOTO           = 34;
	const PROPERTY_BUILDING_VIDEO           = 35;
	const LOBBY_DISPLAY_LOGO				= 33;
	const LOBBY_DISPLAY_BACKGROUND_IMAGE	= 37;
	const WEBSITE_APARTMENT_AMENITY_GROUP_MEDIA = 38;
	const WEBSITE_COMMUNITY_AMENITY_GROUP_MEDIA = 39;
	const CUSTOM_PAGE_COMPONENT_MEDIA = 44;
	const CHECK_LOGO = 45;

	public static function getCTAOccupancyType( $intMarketingMediaSubtypeId ) {

		switch( $intMarketingMediaSubtypeId ) {
			case self::CONVENTIONAL_CTA_MEDIA:
				$intOccupancyType = COccupancyType::CONVENTIONAL;
				break;

			case self::STUDENT_CTA_MEDIA:
				$intOccupancyType = COccupancyType::STUDENT;
				break;

			case self::FLEXIBLE_LEASING_CTA_MEDIA:
				$intOccupancyType = COccupancyType::HOSPITALITY;
				break;

			case self::MILITARY_CTA_MEDIA:
				$intOccupancyType = COccupancyType::MILITARY;
				break;

			default:
				return NULL;
				break;
		}
		return $intOccupancyType;
	}

}
?>
