<?php

class CJobGroup extends CBaseJobGroup {

	const NEW_STATUS	= 'New';
	const IN_PROGRESS	= 'In_Progress';
	const COMPLETE		= 'Complete';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Group Name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPlannedStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getPlannedStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planned_start_date', 'Planned Start Date is required. ' ) );
		} elseif( false == CValidation::validateDate( $this->getPlannedStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planned_start_date', 'Planned Start Date should be in mm/dd/yyyy format. ' ) );
		}

		return $boolIsValid;
	}

	public function valPlannedCompletionDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getPlannedCompletionDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planned_completion_date', 'Planned Completion Date is required. ' ) );
		} elseif( false == CValidation::validateDate( $this->getPlannedCompletionDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planned_completion_date', 'Planned Completion Date should be in mm/dd/yyyy format. ' ) );
		}

		if( false == is_null( $this->getPlannedStartDate() ) && false == is_null( $this->getPlannedCompletionDate() ) && ( strtotime( $this->getPlannedStartDate() ) > strtotime( $this->getPlannedCompletionDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planned_completion_date', 'Planned Completion Date cannot be earlier than Planned Start Date. ' ) );
		}

		return $boolIsValid;
	}

	public function valActualStartDate( $objJobGroup ) {
		$boolIsValid = true;

		if( false == is_null( $objJobGroup->getActualStartDate() ) && true == is_null( $this->getActualStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Actual Start Date should not be blank.' ) );
		} elseif( false == is_null( $objJobGroup->getActualStartDate() ) && false == CValidation::validateDate( $this->getActualStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Actual Start Date should be in mm/dd/yyyy format.' ) );
		}
		return $boolIsValid;
	}

	public function valActualCompletionDate( $objJobGroup ) {
		$boolIsValid = true;

		if( false == is_null( $objJobGroup->getActualCompletionDate() ) && true == is_null( $this->getActualCompletionDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_completion_date', 'Actual Completion Date should not be blank. ' ) );
		} elseif( false == is_null( $objJobGroup->getActualCompletionDate() ) && false == CValidation::validateDate( $this->getActualCompletionDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_completion_ date', 'Actual Completion Date should be in mm/dd/yyyy format.' ) );
		}

		if( false == is_null( $this->getActualStartDate() ) && false == is_null( $this->getActualCompletionDate() ) && ( strtotime( $this->getActualStartDate() ) > strtotime( $this->getActualCompletionDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_completion_date', 'Actual Completion Date cannot be earlier than Actual Start Date. ' ) );
		}

		return $boolIsValid;
	}

	public function valDatesByJobsStartDateAndEndDate( $objJob ) {
		$boolIsValid = true;

		if( false == is_null( $this->getPlannedStartDate() ) && ( strtotime( $this->getPlannedStartDate() ) < strtotime( $objJob->getOriginalStartDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planned_start_date', 'Planned Start Date cannot be earlier than Job Start Date. ' ) );
		}

		if( false == is_null( $this->getPlannedCompletionDate() ) && ( strtotime( $this->getPlannedCompletionDate() ) > strtotime( $objJob->getOriginalEndDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planned_completion_date', 'Planned Completion Date cannot be greater than Job Completion Date. ' ) );
		}

		if( false == is_null( $this->getActualStartDate() ) && ( strtotime( $this->getActualStartDate() ) < strtotime( $objJob->getOriginalStartDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Actual Start Date cannot be earlier than Job Start Date. ' ) );
		} elseif( false == is_null( $this->getActualStartDate() ) && ( strtotime( $this->getActualStartDate() ) > strtotime( $objJob->getOriginalEndDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Actual Start Date cannot be greater than Job Completion Date. ' ) );
		}

		if( false == is_null( $this->getActualCompletionDate() ) && ( strtotime( $this->getActualCompletionDate() ) > strtotime( $objJob->getOriginalEndDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_completion_date', 'Actual Completion Date cannot be greater than Job Completion Date. ' ) );
		} elseif( false == is_null( $this->getActualCompletionDate() ) && ( strtotime( $this->getActualCompletionDate() ) < strtotime( $objJob->getOriginalStartDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_date', 'Actual Completion Date cannot be earlier than Job Start Date. ' ) );
		}

		return $boolIsValid;
	}

	public function valIsUnitType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objJob = NULL, $objJobGroup = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPlannedStartDate();
				$boolIsValid &= $this->valPlannedCompletionDate();
				$boolIsValid &= $this->valDatesByJobsStartDateAndEndDate( $objJob );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPlannedStartDate();
				$boolIsValid &= $this->valPlannedCompletionDate();
				$boolIsValid &= $this->valDatesByJobsStartDateAndEndDate( $objJob );
				$boolIsValid &= $this->valActualStartDate( $objJobGroup );
				$boolIsValid &= $this->valActualCompletionDate( $objJobGroup );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPlannedStartDate();
				$boolIsValid &= $this->valPlannedCompletionDate();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createJobGroupLocation() {

		$objJobGroupLocation  = new CJobGroupLocation();
		$objJobGroupLocation->setJobGroupId( $this->getId() );
		$objJobGroupLocation->setCid( $this->getCid() );

		return $objJobGroupLocation;
	}

	/**
	 *  Fetch Functions
	 */

	public function fetchEventsByEventSubTypeIds( $arrintEventSubTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByReferenceIdByReferenceTypeIdByEventSubTypeIdsByCid( $this->getId(), CEventReferenceType::JOB_GROUP, $arrintEventSubTypeIds, $this->getCid(), $objDatabase );
	}

}
?>