<?php

class CRouteRuleReferenceTypes extends CBaseRouteRuleReferenceTypes {

	public static function fetchRouteRuleReferenceTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRouteRuleReferenceType', $objClientDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchRouteRuleReferenceType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRouteRuleReferenceType', $objClientDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>