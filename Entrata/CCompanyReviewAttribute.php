<?php

class CCompanyReviewAttribute extends CBaseCompanyReviewAttribute {

	protected $m_intValue;
	protected $m_strDefaultReviewAttributeName;

	public function valDefaultReviewAttributeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getDefaultReviewAttributeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_review_attribute_id', __( 'Please select attribute category. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valAttributeName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->m_strAttributeName ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'attribute_name', __( 'Attribute name is required. ' ) ) );
		}

		if( true == $boolIsValid ) {
			$intDuplicateCompanyReviewAttributeCount = \Psi\Eos\Entrata\CCompanyReviewAttributes::createService()->fetchDuplicateCompanyReviewAttributeByCidByAttributeName( $this->m_intId, $this->m_intCid, $this->m_strAttributeName, $objDatabase );

			if( 0 < $intDuplicateCompanyReviewAttributeCount ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'attribute_name', __( 'Attribute already exists. ' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAttributeName( $objDatabase );
				$boolIsValid &= $this->valDefaultReviewAttributeId();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 *
	 **/

	public function setValue( $intValue ) {
		$this->m_intValue = CStrings::strToIntDef( $intValue, NULL, false );
	}

	public function setPropertyReviewAttributeId( $intPropertyReviewAttributeId ) {
		$this->m_intPropertyReviewAttributeId = CStrings::strToIntDef( $intPropertyReviewAttributeId, NULL, false );
	}

	public function setDefaultReviewAttributeName( $strDefaultReviewAttributeName ) {
		$this->m_strDefaultReviewAttributeName = $strDefaultReviewAttributeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['value'] ) ) {
			$this->setValue( $arrmixValues['value'] );
		}

		if( true == isset( $arrmixValues['property_review_attribute_id'] ) ) {
			$this->setPropertyReviewAttributeId( $arrmixValues['property_review_attribute_id'] );
		}

		if( true == isset( $arrmixValues['default_review_attribute_name'] ) ) {
			$this->setDefaultReviewAttributeName( $arrmixValues['default_review_attribute_name'] );
		}
	}

	/**
	 * Get Functions
	 *
	 **/

	public function getValue() {
		return $this->m_intValue;
	}

	public function getDefaultReviewAttributeName() {
		return $this->m_strDefaultReviewAttributeName;
	}

}
?>