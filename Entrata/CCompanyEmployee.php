<?php

use Psi\Eos\Entrata\CCompanyEmployees;

class CCompanyEmployee extends CBaseCompanyEmployee {

	protected $m_intPropertyId;
	protected $m_intIsDisabled;
	protected $m_intCompanyUserId;
	protected $m_intIsAdministrator;

	protected $m_strUserName;
	protected $m_strPhoneNumber;
	protected $m_strPropertyName;
	protected $m_strIntegrationDatabaseName;

	protected $m_arrintPropertyIds;

	protected $m_arrstrMergeFields;
	protected $m_arrstrCompanyDepartmentNames;

	protected $m_strCompanyEmployeeDepartmentChangeLog;

	protected $m_strCompanyDepartmentName;
	protected $m_strCompanyGroupName;
	protected $m_strCompanyUserType;
	protected $m_strCompanyUserTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_arrintPropertyIds = [];

		return;
	}

	/**
	 * Create Functions Get Functions
	 */

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function getTaxNumber() {
		return parent::getDecryptedTaxNumberEncrypted();
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getIsAdministrator() {
		return $this->m_intIsAdministrator;
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getNameFull() {
		return $this->m_strNameFirst . ' ' . $this->m_strNameLast;
	}

	public function getFirstName() {
		return $this->m_strNameFirst;
	}

	public function getLastName() {
		return $this->m_strNameLast;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getIntegrationDatabaseName() {
		return $this->m_strIntegrationDatabaseName;
	}

	public function getCompanyDepartmentNames() {
		return $this->m_arrstrCompanyDepartmentNames;
	}

	public function getMergeFields() {
		return $this->m_arrstrMergeFields;
	}

	public function getMergeFieldByKey( $strKey ) {
		if( true == array_key_exists( $strKey, $this->m_arrstrMergeFields ) ) {
			return $this->m_arrstrMergeFields[$strKey];
		}

		return NULL;
	}

	public function getNameFirst() {
		return trim( $this->m_strNameFirst );
	}

	public function getCompanyGroupName() {
		return $this->m_strCompanyGroupName;
	}

	public function getCompanyUserTypeId() {
		return $this->m_strCompanyUserTypeId ?? CCompanyUserType::ENTRATA;
	}

	public function getCompanyEmployeesFields() {

		return [
			'name_first'              => __( 'First Name' ),
			'name_last'               => __( 'Last Name' ),
			'email_address'           => __( 'Email Address' ),
			'secondary_email_address' => __( 'Secondary Email Address' ),
			'is_employee'             => __( 'Employee' ),
			'birth_date'              => __( 'Birth Date' ),
			'tax_number'              => __( 'Tax Number' ),
			'phone_number1'           => __( 'Primary Phone' ),
			'phone_number1_type_id'   => __( 'Primary Phone Type' ),
			'phone_number2'           => __( 'Secondary Phone' ),
			'phone_number2_type_id'   => __( 'Secondary Phone Type' ),
			'phone_number1_extension' => __( 'Primary Phone Ext' ),
			'phone_number2_extension' => __( 'Secondary Phone Ext' ),
			'is_sso_user'             => __( 'Is Single Sign On User' ),
			'date_terminated'         => __( 'Termination Date' ),
			'departments'             => __( 'Departments' )
		];

	}

	/**
	 * Add Functions
	 */

	public function addPropertyId( $intPropertyId ) {
		$this->m_arrintPropertyIds[$intPropertyId] = $intPropertyId;
	}

	/**
	 * Set Functions
	 */

	public function setNameFirst( $strNameFirst ) {
		$strNameFirst   = trim( $strNameFirst );
		parent::setNameFirst( $strNameFirst );
		$this->m_strNameFirst = getSanitizedFormField( $strNameFirst, true );
	}

	public function setNameLast( $strNameLast ) {
		$strNameLast   = trim( $strNameLast );
		parent::setNameLast( $strNameLast );
		$this->m_strNameLast = getSanitizedFormField( $strNameLast, true );
	}

	public function setTaxNumber( $strTaxNumber ) {
		parent::setTaxNumberEncrypted( $strTaxNumber );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = ( ( 0 < $intPropertyId && true == is_numeric( $intPropertyId ) ) ? $intPropertyId : NULL );
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->m_intIsDisabled = $intIsDisabled;
	}

	public function setIsAdministrator( $intIsAdministrator ) {
		$this->m_intIsAdministrator = $intIsAdministrator;
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setIntegrationDatabaseName( $strIntegrationDatabaseName ) {
		$this->m_strIntegrationDatabaseName = $strIntegrationDatabaseName;
	}

	public function setCompanyDepartmentNames( $arrstrCompanyDepartmentNames ) {
		$this->m_arrstrCompanyDepartmentNames = $arrstrCompanyDepartmentNames;
	}

	public function setCompanyGroupName( $strCompanyGroupName ) {
		$this->m_strCompanyGroupName = $strCompanyGroupName;
	}

	public function setCompanyEmployeeDepartmentChangeLog( $strCompanyEmployeeDepartmentChangeLog ) {
		$this->m_strCompanyEmployeeDepartmentChangeLog = $strCompanyEmployeeDepartmentChangeLog;
	}

	public function setMergeFields( $arrstrMergeFields ) {
		foreach( $arrstrMergeFields as $strKey => $strValue ) {
			$this->setMergeFieldByKey( $strKey, $strValue );
		}
	}

	public function setMergeFieldByKey( $strKey, $strValue ) {
		$this->m_arrstrMergeFields[$strKey] = $strValue;
	}

	public function setCompanyUserTypeId( $intCompanyUserTypeId ) {
		$this->m_strCompanyUserTypeId = $intCompanyUserTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['tax_number'] ) ) {
			$this->setTaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['tax_number'] ) : $arrmixValues['tax_number'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_id'] ) : $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['username'] ) ) {
			$this->setUserName( $arrmixValues['username'] );
		}
		if( true == isset( $arrmixValues['is_administrator'] ) ) {
			$this->setIsAdministrator( $arrmixValues['is_administrator'] );
		}
		if( true == isset( $arrmixValues['company_user_id'] ) ) {
			$this->setCompanyUserId( $arrmixValues['company_user_id'] );
		}
		if( true == isset( $arrmixValues['phone_number'] ) ) {
			$this->setPhoneNumber( $arrmixValues['phone_number'] );
		}
		if( true == isset( $arrmixValues['is_disabled'] ) ) {
			$this->setIsDisabled( $arrmixValues['is_disabled'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['integration_database_name'] ) ) {
			$this->setIntegrationDatabaseName( $arrmixValues['integration_database_name'] );
		}
		if( true == isset( $arrmixValues['company_department_names'] ) ) {
			$this->setCompanyDepartmentNames( $arrmixValues['company_department_names'] );
		}
		if( true == isset( $arrmixValues['company_group_name'] ) ) {
			$this->setCompanyGroupName( $arrmixValues['company_group_name'] );
		}

		return;
	}

	/**
	 * Validation Functions
	 */

	public function valEmployeeStatusTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intEmployeeStatusTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_status_type_id', 'Employee type is required' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strNameFirst ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strNameLast ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBirthDate() {

		$boolIsValid = true;

		if( true == isset( $this->m_strBirthDate ) && 0 < strlen( $this->m_strBirthDate ) && 1 !== CValidation::checkDate( $this->m_strBirthDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'Birth date must be in mm/dd/yyyy form.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $objDatabase = NULL, $boolIsEmailRequired = false ) {

		$boolIsValid = true;

		if( true == $boolIsEmailRequired && false == isset( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is required.' ) ) );
		}

		if( false == is_null( $this->m_strEmailAddress ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email does not appear to be valid.' ) ) );
		}

		if( false == is_null( $objDatabase ) && 0 < \Psi\CStringService::singleton()->strlen( $this->m_strEmailAddress ) ) {
			$objPrexistingCompanyEmployee = CCompanyEmployees::createService()->fetchPreExistingCompanyEmployeeByEmailAddressByCid( $this->getEmailAddress(), $this->getId(), $this->getCid(), $objDatabase, $boolIsDisable = false, $this->getCompanyUserTypeId() );

			if( true == valObj( $objPrexistingCompanyEmployee, 'CCompanyEmployee' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email is already being used.' ), 319 ) );
			}
		}

		return $boolIsValid;
	}

	public function valSecondaryEmailAddress() {

		$boolIsValid = true;

		if( false == is_null( $this->m_strSecondaryEmailAddress ) && false == CValidation::validateEmailAddresses( $this->m_strSecondaryEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Secondary email does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valVacancyEmailAddress( $objDatabase = NULL ) {

		$boolIsValid = true;

		if( false == isset( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is required.' ) ) );
		}

		if( false == is_null( $objDatabase ) && 0 < strlen( $this->m_strEmailAddress ) ) {
			$objPrexistingCompanyEmployee = CCompanyEmployees::createService()->fetchPreExistingCompanyEmployeeByEmailAddressByCid( $this->getEmailAddress(), $this->getId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objPrexistingCompanyEmployee, 'CCompanyEmployee' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email is already being used.' ), 319 ) );
			}
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getPhoneNumber1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number1', __( 'Primary phone is required.' ) ) );
		} elseif( false == CValidation::checkPhoneNumberFullValidation( $this->getPhoneNumber1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number1', __( 'Valid primary phone is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDefaultCorporateContactPhoneNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getPhoneNumber1() ) ) return true;

		if( 0 < \Psi\CStringService::singleton()->strlen( str_replace( '-', '', $this->getPhoneNumber1() ) ) && ( 10 > \Psi\CStringService::singleton()->strlen( str_replace( '-', '', $this->getPhoneNumber1() ) ) || 15 < \Psi\CStringService::singleton()->strlen( str_replace( '-', '', $this->getPhoneNumber1() ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number must be between 10 and 15 characters.' ) ) );
		} elseif( false == ( CValidation::validateFullPhoneNumber( $this->getPhoneNumber1(), false ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDuplidateEmailAddress( $objDatabase ) {

		$boolIsValid = true;
		if( false == is_null( $objDatabase ) && 0 < strlen( $this->m_strEmailAddress ) ) {
			$objPrexistingCompanyEmployee = CCompanyEmployees::createService()->fetchPreExistingCompanyEmployeeByEmailAddressByCid( $this->getEmailAddress(), $this->getId(), $this->getCid(), $objDatabase, $boolIsDisable = false, $this->getCompanyUserTypeId() );

			if( true == valObj( $objPrexistingCompanyEmployee, 'CCompanyEmployee' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email is already being used.' ), 319 ) );
			}
		}

		return $boolIsValid;

	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsEmailRequired = false ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmployeeStatusTypeId();
				$boolIsValid &= $this->valBirthDate();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress( $objDatabase, $boolIsEmailRequired );
				$boolIsValid &= $this->valSecondaryEmailAddress();
				break;

			case 'vacancy_insert':
			case 'vacancy_update':
				$boolIsValid &= $this->valVacancyEmailAddress( $objDatabase );
				break;

			case 'update_company_employee_profile':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valVacancyEmailAddress( $objDatabase );
				break;

			case 'update_profile':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress( $objDatabase, $boolIsEmailRequired );
				break;

			case 'update_contact_info':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress( $objDatabase, $boolIsEmailRequired );
				$boolIsValid &= $this->valPhoneNumber();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_import':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				break;

			case 'validate_company_employee_contact':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= ( true == valStr( $this->getEmailAddress() ) ) ? $this->valEmailAddress() : true;
				break;

			case 'validate_corporate_care_contact':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress( $objDatabase, $boolIsEmailRequired );
				$boolIsValid &= $this->valDefaultCorporateContactPhoneNumber();
				break;

			case 'insert_username_with_email':
				$boolIsValid &= $this->valEmployeeStatusTypeId();
				$boolIsValid &= $this->valBirthDate();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valDuplidateEmailAddress( $objDatabase );
				$boolIsValid &= $this->valSecondaryEmailAddress();
				break;

			case 'update_two_factor_authentication':
				$boolIsValid &= $this->valEmailAddress( $objDatabase, $boolIsEmailRequired );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCompanyEmployeeAddressByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return CCompanyEmployeeAddresses::fetchCompanyEmployeeAddressByCompanyEmployeeIdByAddressTypeIdByCid( $this->getId(), $intAddressTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyDepartment( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyDepartments::createService()->fetchCompanyDepartmentByIdByCid( $this->getCompanyDepartmentId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeasingAgent( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentByCompanyEmployeeByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeasingAgents( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchCustomPropertyLeasingAgentsByCompanyEmployeeIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function getPhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId ) {

		$strPhoneNumber = NULL;

		switch( $intPhoneNumberTypeId ) {

			case CPhoneNumberType::PRIMARY:
			case $this->getPhoneNumber1TypeId():
				$strPhoneNumber = $this->getPhoneNumber1();
				break;

			case $this->getPhoneNumber2TypeId():
				$strPhoneNumber = $this->getPhoneNumber2();
				break;

			default:
				// default case
				break;
		}

		return $strPhoneNumber;
	}

	public function storeCompanyEmployeeLog( $intCurrentUserId, $objDatabase, $boolIsUpdate ) {

		$arrstrCompanyEmployeeLabels = [
			'name_first',
			'name_last',
			'email_address',
			'secondary_email_address',
			'is_employee',
			'birth_date',
			'tax_number',
			'phone_number1',
			'phone_number1_type_id',
			'phone_number2',
			'phone_number2_type_id',
			'phone_number1_extension',
			'phone_number2_extension',
			'is_sso_user',
			'date_terminated',
		];

		$strNewCompanyEmployeeValue = $strOldCompanyEmployeeValue = '';
		$objCompanyEmployee = CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		// Employee Department Log
		if( 1 == $this->getIsEmployee() && true == valStr( $this->m_strCompanyEmployeeDepartmentChangeLog ) ) {
			$strNewCompanyEmployeeValue = 'departments:: ' . $this->m_strCompanyEmployeeDepartmentChangeLog . '~^~';
		}

		// Employee Info Log
		foreach( $arrstrCompanyEmployeeLabels as $strField ) {

			$arrstrExplodeFieldName = explode( '_', $strField );
			array_walk( $arrstrExplodeFieldName, function ( &$strValue, $strIndex ) {
				$strValue = \Psi\CStringService::singleton()->ucfirst( $strValue );
			} );

			$strFunctionName = 'get' . implode( '', $arrstrExplodeFieldName );

			if( true == $boolIsUpdate ) {
				if( $this->$strFunctionName() != $objCompanyEmployee->$strFunctionName() ) {
					$strNewCompanyEmployeeValue .= $strField . '::' . $this->$strFunctionName() . '~^~';
					$strOldCompanyEmployeeValue .= $strField . '::' . $objCompanyEmployee->$strFunctionName() . '~^~';
				}
			} else {
				if( false == is_null( $this->$strFunctionName() ) ) {
					$strNewCompanyEmployeeValue .= $strField . '::' . $this->$strFunctionName() . '~^~';
				}
			}
		}

		if( true == $boolIsUpdate ) {
			$strLogAction                  = 'UPDATE';
			$strCompanyUserInfoDescription = 'A company employee info has been modified.';
		} else {
			$strLogAction                  = 'INSERT';
			$strCompanyUserInfoDescription = 'A company employee info has been inserted.';
		}

		if( true == valStr( $strNewCompanyEmployeeValue ) || true == valStr( $strOldCompanyEmployeeValue ) ) {

			$objTableLog = new CTableLog();
			if( false == $objTableLog->insert( $intCurrentUserId, $objDatabase, 'company_employees', $this->getId(), $strLogAction, $this->getCid(), $strOldCompanyEmployeeValue, $strNewCompanyEmployeeValue, $strCompanyUserInfoDescription ) ) {
				$this->addErrorMsgs( $objTableLog->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

    /**
     * @param $objDatabase
     * @return bool
     * @throws CEntrataSsoException
     */
	public function syncToSso( $objDatabase ) : bool {

		$objCompanyPreference   = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'IS_INTERNAL_SSO', $this->getCid(), $objDatabase );
		$boolIsInternalSso      = ( true == valObj( $objCompanyPreference, CCompanyPreference::class ) ) ? $objCompanyPreference->getValue() : false;

		if( true == $boolIsInternalSso ) {
			$boolIsSsoUser                  = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserSsoStatusByEmployeeIdByCid( $this->getId(), $this->getCId(), $objDatabase );
			$arrstrEmployeeChangedColumns   = [ 'NameFirst', 'NameLast', 'EmailAddress' ];
			$boolUpdateRequiredEmployee     = ( true !== empty( array_intersect( array_keys( $this->getChangedColumns() ), $arrstrEmployeeChangedColumns ) ) );

			if( $boolUpdateRequiredEmployee && true == $boolIsSsoUser ) {
				try {
					$objSyncCompanyUsersToSsoPublisher = new CSyncCompanyUsersToSsoPublisher();
					$objSyncCompanyUsersToSsoPublisher->setEmployeeId( $this->getId() );
					$objSyncCompanyUsersToSsoPublisher->setCid( $this->getCid() );
					$objSyncCompanyUsersToSsoPublisher->setCluster( CConfig::get( 'cluster_name' ) );
					$objSyncCompanyUsersToSsoPublisher->setConnectionConfig( $this->loadAmqpConnectionConfig() );
					$objSyncCompanyUsersToSsoPublisher->syncToSso();
				} catch( Throwable $objSsoException ) {
					throw $objSsoException;
				}
			}
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolCreateLog = true ) {
		try {
			$this->syncToSso( $objDatabase );
		} catch( Throwable $objSsoException ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $objSsoException->getMessage(), $objSsoException->getCode() ) );
			return false;
		}

		$this->checkAndUpdateDisabledEmployeeEmailAddress( $intCurrentUserId, $objDatabase );
		if( true == $boolReturnSqlOnly ) {
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( ( true == $boolCreateLog && false == $this->storeCompanyEmployeeLog( $intCurrentUserId, $objDatabase, $boolIsUpdate = true ) ) || false == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->checkAndUpdateDisabledEmployeeEmailAddress( $intCurrentUserId, $objDatabase );
		if( true == $boolReturnSqlOnly ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) || false == $this->storeCompanyEmployeeLog( $intCurrentUserId, $objDatabase, $boolIsUpdate = false ) ) {
			return false;
		}

		return true;
	}

	public function checkAndUpdateDisabledEmployeeEmailAddress( $intCurrentUserId, $objDatabase ) {
		if( false == valStr( $this->getEmailAddress() ) ) {
			return true;
		}
		$objCompanyEmployee = CCompanyEmployees::createService()->fetchPreExistingCompanyEmployeeByEmailAddressByCid( $this->getEmailAddress(), $this->getId(), $this->getCid(), $objDatabase, $boolIsDisabled = true, $this->getCompanyUserTypeId() );

		if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
			$objCompanyEmployee->setEmailAddress( NULL );
			$objCompanyEmployee->update( $intCurrentUserId, $objDatabase, false, $boolCreateLog = false );
		}

		return true;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	private function loadAmqpConnectionConfig() : array {
		$strUser     = CONFIG_AMQP_USERNAME;
		$strPassword = CONFIG_AMQP_PASSWORD;
		$strUser     = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strUser, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME, 'mode' => MCRYPT_MODE_CBC ] );
		$strPassword = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD, 'mode' => MCRYPT_MODE_CBC ] );

		return  [
			'provider'            => \Psi\Libraries\PubSub\Repository\CProvider::RABBIT_MQ,
			'host'                => CONFIG_AMQP_HOST,
			'port'                => CONFIG_AMQP_PORT,
			'user'                => $strUser,
			'password'            => $strPassword,
			'vhost'               => '/',
			'connection_time_out' => 3,
			'read_write_time_out' => 60,
			'context'             => NULL,
			'keepalive'           => true,
			'heartbeat'           => 30
		];
	}

}
?>