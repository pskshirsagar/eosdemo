<?php

class CRouteRuleReferenceType extends CBaseRouteRuleReferenceType {

	const PURCHASE_ORDER 			= 1;
	const PURCHASE_ORDER_LINE_ITEM	= 2;
	const INVOICE 					= 3;
	const INVOICE_LINE_ITEM			= 4;
	const PAYMENT 					= 5;
	const JOURNAL_ENTRY				= 6;
	const FINANCIAL_MOVE_OUTS		= 7;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteRuleReferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:

		}

		return $boolIsValid;
	}

	public static function getReferenceTypeIdByReferenceObject( $objReference ) {

		switch( get_class( $objReference ) ) {
			case 'CApHeader':
				if( CApHeaderType::PURCHASE_ORDER == $objReference->getApHeaderTypeId() ) {
					return self::PURCHASE_ORDER;
				}

				return self::INVOICE;
				break;

			case 'CApDetail':
				if( $objReference->getApHeaderTypeId() == CApHeaderType::PURCHASE_ORDER ) {
					return self::PURCHASE_ORDER_LINE_ITEM;
				}

				return self::INVOICE_LINE_ITEM;
				break;

			case 'CApPayment':
				return self::PAYMENT;
				break;

			case 'CGlHeader':
			case 'CGlDetail':
				return self::JOURNAL_ENTRY;
				break;

			case 'CLease':
			    return self::FINANCIAL_MOVE_OUTS;

			default:
				// Empty default case
		}
	}
}
?>