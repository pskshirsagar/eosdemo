<?php

class CCompanyApPaymentTypeAssociation extends CBaseCompanyApPaymentTypeAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEnabled() {
		$boolIsValid = true;
		if( false == $this->getIsEnabled() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_types', 'The payment type ' . CApPaymentType::$c_arrstrAllApPaymentTypes[$this->getApPaymentTypeId()] . ' is disabled.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valIsNotify() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentTypes( $arrintSelectedPaymentTypes ) {
		$boolIsValid = true;

		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrintSelectedPaymentTypes ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_types', 'Please select atleast one payment type.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $arrintSelectedPaymentTypes = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valPaymentTypes( $arrintSelectedPaymentTypes );
				break;

			case VALIDATE_INSERT:
			case VALIDATE_DELETE:
				break;

			case 'validate_payment_type':
				$boolIsValid = $this->valIsEnabled();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>