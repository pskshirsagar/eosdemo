<?php

class CUnitSpaceVacancyLog extends CBaseUnitSpaceVacancyLog {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMarketRent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDaysInCalendarMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEstimatedVacancyLoss() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEstimatedDaysVacant() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActualVacancyLoss() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActualDaysVacant() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFutureVacancyAdjustment() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>