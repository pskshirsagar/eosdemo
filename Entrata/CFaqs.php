<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFaqs
 * Do not add any new functions to this class.
 */

class CFaqs extends CBaseFaqs {

	public static function fetchPaginatedFaqsByCid( $intPageNo, $intPageSize, $strOrderByFieldName = NULL, $strOrderField = NULL, $intCid, $objDatabase ) {
		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit	= ( int ) $intPageSize;

    	$strSql = 'SELECT * FROM faqs WHERE cid = ' . ( int ) $intCid;
    	$strSql .= ' ORDER BY ' . ' ' . $strOrderByFieldName . ' ' . $strOrderField;

    	if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
    		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
    	}

    	return self::fetchFaqs( $strSql, $objDatabase );
    }

	public static function fetchFaqsByIdsByCid( $arrintFaqIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintFaqIds ) ) return NULL;
		$strSql = 'SELECT * FROM faqs WHERE id IN (' . implode( ', ', $arrintFaqIds ) . ') AND cid = ' . ( int ) $intCid;
    	return self::fetchFaqs( $strSql, $objDatabase );
    }

	public static function fetchQuickSearchFaqsWithCid( $arrstrFilteredExplodedSearch, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM faqs WHERE question ILIKE \'%' . implode( '%\'AND question ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' AND cid = ' . ( int ) $intCid . ' LIMIT 10';
		return self::fetchFaqs( $strSql, $objDatabase );
	}

	public static function fetchFaqCount( $strWhere = NULL, $objDatabase ) {
		$strWhere = 'WHERE cid IS NOT NULL';
        return parent::fetchRowCount( $strWhere, 'faqs', $objDatabase );
    }
}
?>