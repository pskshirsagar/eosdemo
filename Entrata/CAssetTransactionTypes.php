<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetTransactionTypes
 * Do not add any new functions to this class.
 */

class CAssetTransactionTypes extends CBaseAssetTransactionTypes {

    public static function fetchAssetTransactionTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CAssetTransactionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAssetTransactionType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CAssetTransactionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>