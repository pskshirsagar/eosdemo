<?php

class CReviewDetailStatus extends CBaseReviewDetailStatus {

	const NEW_REVIEW                      = 1;
	const SKIPPED_REVIEW_RESPONDER_QUEUE  = 2;
	const ADDED_TO_REVIEW_RESPONDER_QUEUE = 3;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatusName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>