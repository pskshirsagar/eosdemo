<?php

class CSocialMediaActivity extends CBaseSocialMediaActivity {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSocialMediaAccountDetailId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActivityOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLikesCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReachCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEngagementCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFriendsCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFollowersCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDisabledOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>