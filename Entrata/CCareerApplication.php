<?php

class CCareerApplication extends CBaseCareerApplication {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDocumentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTitle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInstructions() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequirePrintedForm() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->validateTitle();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// No Default case.
            	break;
        }

        return $boolIsValid;
    }

    public function validateTitle() {
    	$boolIsValid = true;

    	if( false == isset( $this->m_strTitle ) || true == is_null( $this->m_strTitle ) ) {
    		$boolIsValid &= false;

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );
    	}

    	return $boolIsValid;
    }
}
?>