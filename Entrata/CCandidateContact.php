<?php

class CCandidateContact extends CBaseCandidateContact {

    public function valId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCandidateId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCandidateId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'candidate_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCandidateContactTypeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCandidateContactTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'candidate_contact_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyName() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCompanyName() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valNameFirst() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getNameFirst() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', '' ) );
        // }

        return $boolIsValid;
    }

    public function valNameLast() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getNameLast() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', '' ) );
        // }

        return $boolIsValid;
    }

    public function valRelationship() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getRelationship() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOccupation() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getOccupation() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupation', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getPhoneNumber() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStreetLine1() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getStreetLine1() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStreetLine2() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getStreetLine2() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line2', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getCity() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getStateCode() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPostalCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( true == is_null( $this->getPostalCode() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valGrossSalary() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getGrossSalary() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gross_salary', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEarningsDescription() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getEarningsDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'earnings_description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDateStarted() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDateStarted() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_started', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDateTerminated() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDateTerminated() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_terminated', '' ) );
        // }

        return $boolIsValid;
    }

    public function valReasonForLeaving() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getReasonForLeaving() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reason_for_leaving', '' ) );
        // }

        return $boolIsValid;
    }

    public function valRoles() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getRoles() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'roles', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDuties() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDuties() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duties', '' ) );
        // }

        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getNotes() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCandidateApprovedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCandidateApprovedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'candidate_approved_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valApprovedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getApprovedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valApprovedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getApprovedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'approved_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>