<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentPreferences
 * Do not add any new functions to this class.
 */

class CDocumentPreferences extends CBaseDocumentPreferences {

	public static function fetchDocumentPreferenceByKeyByDocumentIdByCid( $intDocumentId, $strDocumentPreferenceKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM document_preferences WHERE document_id = ' . ( int ) $intDocumentId . ' AND cid = ' . ( int ) $intCid . ' AND key = \'' . $strDocumentPreferenceKey . '\'';
		return self::fetchDocumentPreference( $strSql, $objDatabase );
	}

	public static function fetchDocumentPreferenceByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM document_preferences WHERE document_id=' . ( int ) $intDocumentId . ' AND cid=' . ( int ) $intCid;
		return self::fetchDocumentPreferences( $strSql, $objDatabase );
	}

	public static function fetchDocumentPreferencesByCidByDocumentIds( $intCid, $arrintDocumentIds, $objDatabase ) {
		if( false == valArr( $arrintDocumentIds ) ) return NULL;
		$strSql = 'SELECT * FROM document_preferences WHERE cid=' . ( int ) $intCid . ' AND document_id IN (' . implode( ',', $arrintDocumentIds ) . ')';

		return self::fetchDocumentPreferences( $strSql, $objDatabase );
	}

	public static function fetchDocumentPreferencesByKeysByDocumentIdByCid( $arrstrKeys, $intDocumentId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						document_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND document_id = ' . ( int ) $intDocumentId . '
	    				AND key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		return self::fetchDocumentPreferences( $strSql, $objDatabase );
	}

}
?>