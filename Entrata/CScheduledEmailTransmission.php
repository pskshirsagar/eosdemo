<?php

class CScheduledEmailTransmission extends CBaseScheduledEmailTransmission {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledEmailId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalResidents() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalProspects() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalCompanyEmployees() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalResidentEmails() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalProspectEmails() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalCompanyEmployeeEmails() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>