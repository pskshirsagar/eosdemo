<?php

class CUnitSpaceAbatement extends CBaseUnitSpaceAbatement {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'start_date', __( 'Please select start date.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( true == valStr( $this->getEndDate() ) && true == valStr( $this->getStartDate() ) && strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'end_date', __( 'End date cannot be less than start date..' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		if( false == valStr( $this->getNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'notes', __( 'Please add abatement reason.' ), NULL ) );
		}
		return $boolIsValid;
	}

	public function valDateOverlap( $objDatabase ) {
		$boolIsValid = true;

		$intOverlappingUnitSpaceAbatementCount = ( int ) CUnitSpaceAbatements::fetchOverlappingUnitSpaceAbatementCountByUnitSpaceIdByPropertyIdByCid( $this->getId(), $this->getUnitSpaceId(), $this->getStartDate(), $this->getEndDate(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( 0 < $intOverlappingUnitSpaceAbatementCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'end_date', __( 'Abatement period overlaps with one of the existing abatement period. Please select different abatement period. ' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNotes();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valDateOverlap( $objDatabase );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getId() ) ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		} else {
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		}

	}

}
?>