<?php

class CImportStatusType extends CBaseImportStatusType {

	const INITIALIZED 		= 1;
	const IN_PROGRESS 		= 2;
	const COMPLETED 		= 3;
	const FAILED 			= 4;
	const REJECTED 			= 5;

	// below new statuses added for CSV new architecture

	const VALIDATION_STARTED        = 6;
	const VALIDATION_FAILED         = 7;
	const VALIDATION_COMPLETED      = 8;

	const DATA_PROCESS_STARTED      = 9;
	const DATA_PROCESS_FAILED       = 10;
	const DATA_PROCESS_COMPLETED    = 11;
	const FILE_RECEIVED				= 12;

	public static $c_arrstrImportStatusTypes = [
		'INITIALIZED'				=> CImportStatusType::INITIALIZED,
		'IN_PROGRESS'				=> CImportStatusType::IN_PROGRESS,
		'COMPLETED'					=> CImportStatusType::COMPLETED,
		'FAILED'					=> CImportStatusType::FAILED,
		'REJECTED'					=> CImportStatusType::REJECTED,
		'VALIDATION_STARTED'		=> CImportStatusType::VALIDATION_STARTED,
		'VALIDATION_FAILED'			=> CImportStatusType::VALIDATION_FAILED,
		'VALIDATION_COMPLETED'		=> CImportStatusType::VALIDATION_COMPLETED,
		'DATA_PROCESS_STARTED'		=> CImportStatusType::DATA_PROCESS_STARTED,
		'DATA_PROCESS_FAILED'		=> CImportStatusType::DATA_PROCESS_FAILED,
		'DATA_PROCESS_COMPLETED'	=> CImportStatusType::DATA_PROCESS_COMPLETED,
		'FILE_RECEIVED'				=> CImportStatusType::FILE_RECEIVED
	];

	public static $c_arrintImportStatusTypes = [
		self::INITIALIZED 				=> 'Initialized',
		self::IN_PROGRESS 				=> 'In Progress',
		self::COMPLETED					=> 'Completed',
		self::FAILED					=> 'Failed',
		self::REJECTED					=> 'Rejected',
		self::VALIDATION_STARTED		=> 'Validation Started',
		self::VALIDATION_FAILED			=> 'Validation Failed',
		self::VALIDATION_COMPLETED		=> 'Validation Completed',
		self::DATA_PROCESS_STARTED 		=> 'Data Process Started',
		self::DATA_PROCESS_FAILED 		=> 'Data Process Failed',
		self::DATA_PROCESS_COMPLETED 	=> 'Data Process Completed',
		self::FILE_RECEIVED 			=> 'File Received'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>