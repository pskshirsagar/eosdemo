<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyClassifiedCategories
 * Do not add any new functions to this class.
 */

class CPropertyClassifiedCategories extends CBasePropertyClassifiedCategories {

	public static function fetchPropertyClassifiedCategoryByCompanyClassifiedCategoryIdByPropertyIdByCid( $intCompanyClassifiedCategoryId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						property_classified_categories
					WHERE
						company_classified_category_id = ' . ( int ) $intCompanyClassifiedCategoryId . '
					  AND
						property_id = ' . ( int ) $intPropertyId . '
					  AND
					    cid = ' . ( int ) $intCid;

		return self::fetchPropertyClassifiedCategory( $strSql, $objDatabase );
	}

}
?>