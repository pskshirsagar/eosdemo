<?php

class CApPayment extends CBaseApPayment {

	const PROCESS_METHOD_MANUAL_PAYMENT		= 'manual_payment';
	const PROCESS_METHOD_VOID				= 'void_check';
	const PROCESS_REVERSE_VOIDED_PAYMENT	= 'reverse_voided_payment';
	const LOOKUP_TYPE_ACCOUNTING_CHECKES_PAYEE				= 'accounting_checks_payee';
	const LOOKUP_TYPE_ACCOUNTING_CHECKS_PAYMENT_NUMBER		= 'accounting_checks_payment_number';

	const DIRECT_DEBIT = 9;

	protected $m_boolIsSameDayCheck;
	protected $m_boolIsPaymentDelayed;
	protected $m_boolIsCrossAllocation;
	protected $m_boolIsCheckMemoVisible;
	protected $m_boolIsInitialImport;

//	protected $m_intRemittanceId; //ap_remittances.id
	protected $m_intGlTransactionTypeId;
	protected $m_intApHeaderId;
	protected $m_intParentApHeaderId;
	protected $m_intReversalApHeaderId;
	protected $m_intApPayeeId;
	protected $m_intApPayeeLocationId;
	protected $m_intIsPreprinted;
	protected $m_intApPayeeContactId;
	protected $m_intApPayeeAccountId;
	protected $m_intApDetailId;
	protected $m_intApPaymentTypeIdByRemittance;

	protected $m_strPostMonth;
	protected $m_strAccountName; // bank_accounts.account_name
	protected $m_strCheckBankName;
	protected $m_strFormattedPayeeName;
	protected $m_strHeaderNumber;
	protected $m_strMailDeliveryCode;
	protected $m_strApPaymentTypeName;
	protected $m_strPaymentStatusTypeName;
	protected $m_strReturnTypeName;
	protected $m_strPostDate;
	protected $m_strLookupCode;
	protected $m_strVendorCode;
	protected $m_strPropertyName;
	protected $m_strApPayeeKey; // This is unique string to distinguish between vendor and resident.
	protected $m_strLocationName;
	protected $m_strApPayeeEmailAddress;
	protected $m_strReconcilationStatusName;

	protected $m_strStreetLine1; // ap_remittances.street_line1
	protected $m_strStreetLine2; // ap_remittances.street_line2
	protected $m_strStreetLine3; // ap_remittances.street_line3
	protected $m_strCity; // ap_remittances.city
	protected $m_strStateCode; // ap_remittances.state_code
	protected $m_strPostalCode; // ap_remittances.postal_code
	protected $m_strCountryCode; // ap_remittances.country_code
//	protected $m_strCheckNameOnAccount; //ap_remittances.check_name_on_account
//	protected $m_strCheckRoutingNumber; // ap_remittances.check_routing_number
//	protected $m_strCheckAccountNumberEncrypted; // ap_remittances.check_account_number_encrypted

	protected $m_strAccountNumber; // ap_payee_accounts.account_number
	protected $m_strPayerStreetLine1; // bank_accounts.payer_street_line1
	protected $m_strPayerStreetLine2; // bank_accounts.payer_street_line2
	protected $m_strPayerCity; // bank_accounts.payer_city
	protected $m_strPayerStateCode; // bank_accounts.payer_state_code
	protected $m_strPayerPostalCode; // bank_accounts.payer_postal_code
	protected $m_strCheckBankNumber; // bank_accounts.check_bank_number
	protected $m_intSecondaryNumber; // ap_payees.secondary_number

	protected $m_objProcessingBankAccount;

	protected $m_arrintPaymentPropertyIds;
	protected $m_arrintUserAllowedPropertyIds;

	protected $m_arrfltApDetailsPaymentAmounts;

	protected $m_arrintPropertyIds;
	protected $m_arrobjInvoiceApHeaders;
	protected $m_arrobjInvoiceApDetails;
	protected $m_arrmixApPayeeDetails;

	protected $m_arrmixRequiredParameters;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPreprinted = '0';

		return;
	}

	/**
	 * Fetch Functions
	 */

	public function getProcessMethod() {

		switch( $this->getApPaymentTypeId() ) {

			case CApPaymentType::CHECK:
			default:
				$strProcessMethod = self::PROCESS_METHOD_MANUAL_PAYMENT;
				break;
		}

		return $strProcessMethod;
	}

	/**
	 * Get Functions
	 */
	public function getIsPreprinted() {
		return $this->m_intIsPreprinted;
	}

	public function getIsPaymentDelayed() {
		return $this->m_boolIsPaymentDelayed;
	}

	public function getIsCheckMemoVisible() {
		return $this->m_boolIsCheckMemoVisible;
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function getParentApHeaderId() {
		return $this->m_intParentApHeaderId;
	}

	public function getReversalApHeaderId() {
		return $this->m_intReversalApHeaderId;
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function getApPayeeContactId() {
		return $this->m_intApPayeeContactId;
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function getInvoiceApHeaders() {
		return $this->m_arrobjInvoiceApHeaders;
	}

	public function getProcessingBankAccount() {
		return $this->m_objProcessingBankAccount;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function getApPayeeKey() {
		return $this->m_strApPayeeKey;
	}

	public function getVendorCode() {
		return $this->m_strVendorCode;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function getFormattedPayeeName() {
		return $this->m_strFormattedPayeeName;
	}

	public function getUserAllowedPropertyIds() {
		return $this->m_arrintUserAllowedPropertyIds;
	}

	public function getPaymentPropertyIds() {
		return $this->m_arrintPaymentPropertyIds;
	}

	public function getCheckAccountNumber() {
		if( false == valStr( $this->m_strCheckAccountNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		if( true == valStr( $strCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );

		}
	}

	public function getHeaderNumber() {
		return $this->m_strHeaderNumber;
	}

	public function getApPaymentTypeName() {
		return $this->m_strApPaymentTypeName;
	}

	public function getApPaymentTypeIdByRemittance() {
		return $this->m_intApPaymentTypeIdByRemittance;
	}

	public function getPaymentStatusTypeName() {
		return $this->m_strPaymentStatusTypeName;
	}

	public function getReturnTypeName() {
		return $this->m_strReturnTypeName;
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function getLookupCode() {
		return $this->m_strLookupCode;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getApPayeeEmailAddress() {
		return $this->m_strApPayeeEmailAddress;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getPayerStreetLine1() {
		return $this->m_strPayerStreetLine1;
	}

	public function getPayerStreetLine2() {
		return $this->m_strPayerStreetLine2;
	}

	public function getPayerCity() {
		return $this->m_strPayerCity;
	}

	public function getPayerStateCode() {
		return $this->m_strPayerStateCode;
	}

	public function getPayerPostalCode() {
		return $this->m_strPayerPostalCode;
	}

	public function getCheckBankNumber() {
		return $this->m_strCheckBankNumber;
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function getIsCrossAllocation() {
		return $this->m_boolIsCrossAllocation;
	}

	public function getInvoiceApDetails() {
		return $this->m_arrobjInvoiceApDetails;
	}

	public function getApDetailsPaymentAmounts() {
		return $this->m_arrfltApDetailsPaymentAmounts;
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;

	}

	public function getReconcilationStatusName() {
		return $this->m_strReconcilationStatusName;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getSecondaryNumber() {
		return $this->m_intSecondaryNumber;
	}

	public function getMailDeliveryCode() {
		return $this->m_strMailDeliveryCode;
	}

	public function getIsSameDayCheck() {
		return $this->m_boolIsSameDayCheck;
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParameters;
	}

	/**
	 * Set Functions
	 */

	public function setIsPreprinted( $intIsPreprinted ) {
		$this->m_intIsPreprinted = CStrings::strToIntDef( $intIsPreprinted, NULL, false );
	}

	public function setIsPaymentDelayed( $boolIsPaymentDelayed ) {
		$this->m_boolIsPaymentDelayed = $boolIsPaymentDelayed;
	}

	public function setIsCheckMemoVisible( $boolIsCheckMemoVisible ) {
		$this->m_boolIsCheckMemoVisible = $boolIsCheckMemoVisible;
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->m_intGlTransactionTypeId = CStrings::strToIntDef( $intGlTransactionTypeId, NULL, false );
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->m_intApHeaderId = CStrings::strToIntDef( $intApHeaderId, NULL, false );
	}

	public function setParentApHeaderId( $intParentApHeaderId ) {
		$this->m_intParentApHeaderId = CStrings::strToIntDef( $intParentApHeaderId, NULL, false );
	}

	public function setReversalApHeaderId( $intReversalApHeaderId ) {
		$this->m_intReversalApHeaderId = CStrings::strToIntDef( $intReversalApHeaderId, NULL, false );
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = CStrings::strToIntDef( $intApPayeeId, NULL, false );
	}

	public function setApPayeeContactId( $intApPayeeContactId ) {
		$this->m_intApPayeeContactId = $intApPayeeContactId;
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->m_intApPayeeAccountId = CStrings::strToIntDef( $intApPayeeAccountId, NULL, false );
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		$this->m_intApPayeeLocationId = CStrings::strToIntDef( $intApPayeeLocationId, NULL, false );
	}

	public function setInvoiceApHeaders( $arrobjInvoiceApHeaders ) {
		$this->m_arrobjInvoiceApHeaders = $arrobjInvoiceApHeaders;
	}

	public function setProcessingBankAccount( $objProcessingBankAccount ) {
		$this->m_objProcessingBankAccount = $objProcessingBankAccount;
	}

	public function setPostMonth( $strPostMonth ) {
		$this->m_strPostMonth = CStrings::strTrimDef( $strPostMonth, -1, NULL, true );
	}

	public function setApPayeeKey( $strApPayeeKey ) {
		$this->m_strApPayeeKey = $strApPayeeKey;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->m_strCheckBankName = $strCheckBankName;
	}

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = $strLocationName;
	}

	public function setFormattedPayeeName( $strFormattedPayeeName ) {
		$this->m_strFormattedPayeeName = $strFormattedPayeeName;
	}

	public function setUserAllowedPropertyIds( $arrintUserAllowedPropertyIds ) {
		$this->m_arrintUserAllowedPropertyIds = $arrintUserAllowedPropertyIds;
	}

	public function setPaymentPropertyIds( $arrintApDetailsPropertyIds ) {
		$this->m_arrintPaymentPropertyIds = $arrintApDetailsPropertyIds;
	}

	public function setHeaderNumber( $strHeaderNumber ) {
		$this->m_strHeaderNumber = CStrings::strTrimDef( $strHeaderNumber, -1, NULL, true );
	}

	public function setApPaymentTypeName( $strApPaymentTypeName ) {
		$this->m_strApPaymentTypeName = CStrings::strTrimDef( $strApPaymentTypeName, -1, NULL, true );
	}

	public function setApPaymentTypeIdByRemittance( $intApPaymentTypeIdByRemittance ) {
		$this->m_intApPaymentTypeIdByRemittance = $intApPaymentTypeIdByRemittance;
	}

	public function setPaymentStatusTypeName( $strPaymentStatusTypeName ) {
		$this->m_strPaymentStatusTypeName = CStrings::strTrimDef( $strPaymentStatusTypeName, -1, NULL, true );
	}

	public function setReturnTypeName( $strReturnTypeName ) {
		$this->m_strReturnTypeName = CStrings::strTrimDef( $strReturnTypeName, -1, NULL, true );
	}

	public function setPostDate( $strPostDate ) {
		$this->m_strPostDate = CStrings::strTrimDef( $strPostDate, -1, NULL, true );
	}

	public function setLookupCode( $strLookupCode ) {
		$this->m_strLookupCode = $strLookupCode;
	}

	public function setVendorCode( $strVendorCode ) {
		$this->m_strVendorCode = $strVendorCode;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setApPayeeEmailAddress( $strApPayeeEmailAddress ) {
		$this->m_strApPayeeEmailAddress = $strApPayeeEmailAddress;
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->m_strStreetLine1 = $strStreetLine1;
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->m_strStreetLine2 = $strStreetLine2;
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->m_strStreetLine3 = $strStreetLine3;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	public function setPayerStreetLine1( $strPayerStreetLine1 ) {
		$this->m_strPayerStreetLine1 = $strPayerStreetLine1;
	}

	public function setPayerStreetLine2( $strPayerStreetLine2 ) {
		$this->m_strPayerStreetLine2 = $strPayerStreetLine2;
	}

	public function setPayerCity( $strPayerCity ) {
		$this->m_strPayerCity = $strPayerCity;
	}

	public function setPayerStateCode( $strPayerStateCode ) {
		$this->m_strPayerStateCode = $strPayerStateCode;
	}

	public function setPayerPostalCode( $strPayerPostalCode ) {
		$this->m_strPayerPostalCode = $strPayerPostalCode;
	}

	public function setCheckBankNumber( $strCheckBankNumber ) {
		$this->m_strCheckBankNumber = $strCheckBankNumber;
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->m_boolIsInitialImport = $boolIsInitialImport;
	}

	public function setIsCrossAllocation( $boolIsCrossAllocation ) {
		$this->m_boolIsCrossAllocation = $boolIsCrossAllocation;
	}

	public function setInvoiceApDetails( $arrobjInvoiceApDetails ) {
		$this->m_arrobjInvoiceApDetails = $arrobjInvoiceApDetails;
	}

	public function setPropertyIds( $arrintPropertyIds ) {

		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	public function setReconcilationStatusName( $strReconcilationStatusName ) {

		$this->m_strReconcilationStatusName = $strReconcilationStatusName;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setSecondaryNumber( $intSecondaryNumber ) {
		$this->m_intSecondaryNumber = $intSecondaryNumber;
	}

	public function setMailDeliveryCode( $strMailDeliveryCode ) {
		$this->m_strMailDeliveryCode = $strMailDeliveryCode;
	}

	public function setIsSameDayCheck( $boolIsSameDayCheck ) {
		$this->m_boolIsSameDayCheck = $boolIsSameDayCheck;
	}

	public function setApDetailId( $intApDetailId ) {
		$this->m_intApDetailId = $intApDetailId;
	}

	public function setApPayeeDetails( $arrmixDetails ) {
		$this->m_arrmixApPayeeDetails = $arrmixDetails;
	}

	public function getApPayeeDetails() {
		return $this->m_arrmixApPayeeDetails;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['post_month'] ) ) $this->setPostMonth( $arrmixValues['post_month'] );
		if( true == isset( $arrmixValues['account_name'] ) ) $this->setAccountName( $arrmixValues['account_name'] );
		if( true == isset( $arrmixValues['check_bank_name'] ) ) $this->setCheckBankName( $arrmixValues['check_bank_name'] );
		if( true == isset( $arrmixValues['is_initial_import'] ) ) $this->setIsInitialImport( $arrmixValues['is_initial_import'] );
		if( true == isset( $arrmixValues['formatted_payee_name'] ) ) $this->setFormattedPayeeName( $arrmixValues['formatted_payee_name'] );
		if( true == isset( $arrmixValues['gl_transaction_type_id'] ) ) $this->setGlTransactionTypeId( $arrmixValues['gl_transaction_type_id'] );
		if( true == isset( $arrmixValues['ap_header_id'] ) ) $this->setApHeaderId( $arrmixValues['ap_header_id'] );
		if( true == isset( $arrmixValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrmixValues['ap_payee_account_id'] );
		if( true == isset( $arrmixValues['parent_ap_header_id'] ) ) $this->setParentApHeaderId( $arrmixValues['parent_ap_header_id'] );
		if( true == isset( $arrmixValues['reversal_ap_header_id'] ) ) $this->setReversalApHeaderId( $arrmixValues['reversal_ap_header_id'] );
		if( true == isset( $arrmixValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrmixValues['ap_payee_id'] );
		if( true == isset( $arrmixValues['ap_payee_location_id'] ) ) $this->setApPayeeLocationId( $arrmixValues['ap_payee_location_id'] );
		if( true == isset( $arrmixValues['header_number'] ) ) $this->setHeaderNumber( $arrmixValues['header_number'] );
		if( true == isset( $arrmixValues['ap_payment_type_name'] ) ) $this->setApPaymentTypeName( $arrmixValues['ap_payment_type_name'] );
		if( true == isset( $arrmixValues['payment_status_type_name'] ) ) $this->setPaymentStatusTypeName( $arrmixValues['payment_status_type_name'] );
		if( true == isset( $arrmixValues['return_type_name'] ) ) $this->setReturnTypeName( $arrmixValues['return_type_name'] );
		if( true == isset( $arrmixValues['post_date'] ) ) $this->setPostDate( $arrmixValues['post_date'] );
		if( true == isset( $arrmixValues['lookup_code'] ) ) $this->setLookupCode( $arrmixValues['lookup_code'] );
		if( true == isset( $arrmixValues['vendor_code'] ) ) $this->setVendorCode( $arrmixValues['vendor_code'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['location_name'] ) ) $this->setLocationName( $arrmixValues['location_name'] );
		if( true == isset( $arrmixValues['ap_payee_contact_id'] ) ) $this->setApPayeeContactId( $arrmixValues['ap_payee_contact_id'] );
		if( true == isset( $arrmixValues['is_check_memo_visible'] ) ) $this->setIsCheckMemoVisible( $arrmixValues['is_check_memo_visible'] );
		if( true == isset( $arrmixValues['is_payment_delayed'] ) ) $this->setIsPaymentDelayed( $arrmixValues['is_payment_delayed'] );
		if( true == isset( $arrmixValues['ap_payee_key'] ) ) $this->setApPayeeKey( $arrmixValues['ap_payee_key'] );
		if( true == isset( $arrmixValues['ap_payee_email_address'] ) ) $this->setApPayeeEmailAddress( $arrmixValues['ap_payee_email_address'] );
		if( true == isset( $arrmixValues['is_preprinted'] ) ) $this->setIsPreprinted( $arrmixValues['is_preprinted'] );
		if( true == isset( $arrmixValues['street_line1'] ) ) $this->setStreetLine1( $arrmixValues['street_line1'] );
		if( true == isset( $arrmixValues['street_line2'] ) ) $this->setStreetLine2( $arrmixValues['street_line2'] );
		if( true == isset( $arrmixValues['street_line3'] ) ) $this->setStreetLine3( $arrmixValues['street_line3'] );
		if( true == isset( $arrmixValues['city'] ) ) $this->setCity( $arrmixValues['city'] );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['account_number'] ) ) $this->setAccountNumber( $arrmixValues['account_number'] );
		if( true == isset( $arrmixValues['payer_street_line1'] ) ) $this->setPayerStreetLine1( $arrmixValues['payer_street_line1'] );
		if( true == isset( $arrmixValues['payer_street_line2'] ) ) $this->setPayerStreetLine2( $arrmixValues['payer_street_line2'] );
		if( true == isset( $arrmixValues['payer_city'] ) ) $this->setPayerCity( $arrmixValues['payer_city'] );
		if( true == isset( $arrmixValues['payer_state_code'] ) ) $this->setPayerStateCode( $arrmixValues['payer_state_code'] );
		if( true == isset( $arrmixValues['payer_postal_code'] ) ) $this->setPayerPostalCode( $arrmixValues['payer_postal_code'] );
		if( true == isset( $arrmixValues['check_bank_number'] ) ) $this->setCheckBankNumber( $arrmixValues['check_bank_number'] );
		if( true == isset( $arrmixValues['reconcilation_status_name'] ) ) $this->setReconcilationStatusName( $arrmixValues['reconcilation_status_name'] );
		if( true == isset( $arrmixValues['country_code'] ) ) $this->setCountryCode( $arrmixValues['country_code'] );
		if( true == isset( $arrmixValues['secondary_number'] ) ) $this->setSecondaryNumber( $arrmixValues['secondary_number'] );
		if( true == isset( $arrmixValues['details'] ) ) $this->setDetails( $arrmixValues['details'] );
		if( true == isset( $arrmixValues['ap_detail_id'] ) ) $this->setApDetailId( $arrmixValues['ap_detail_id'] );
		if( true == isset( $arrmixValues['ap_payee_details'] ) ) $this->setApPayeeDetails( $arrmixValues['ap_payee_details'] );

		return;
	}

	public function setRequiredParameters( $arrmixParameters ) {
		$this->m_arrmixRequiredParameters = $arrmixParameters;
	}

	/**
	 * Validation Functions
	 */

	public static function validateVoidPayments( $arrobjPaymentApHeaders, $intCid, $objClientDatabase = NULL, $arrobjOriginalApHeaders ) {

		$arrintApPayments = CApPayments::fetchAllActiveApPaymentsByPaymentApHeaderIdsByCid( $arrobjPaymentApHeaders, $intCid, $objClientDatabase );

		if( true == valArr( $arrintApPayments ) ) {
			$arrobjErrorMsgs[] = new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You cannot reverse the selected invoice(s) because there are active payments ' . $arrintApPayments[0]['payment_numbers'] . ' associated to each other.' );
		} else {

			$arrintPaidApHeaders = ( array ) CApHeaders::fetchApHeadersAssociatedWithCreditMemosByIdByCid( array_keys( $arrobjOriginalApHeaders ), array_keys( $arrobjPaymentApHeaders ), $intCid, $objClientDatabase );

			if( true == valArr( $arrintPaidApHeaders ) ) {
				$arrobjErrorMsgs[] = new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You cannot reverse the selected invoice(s) because credit memo is associated to it.' );
			}
		}

		return $arrobjErrorMsgs;

	}

	public function valBankAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getBankAccountId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_id', 'Bank account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentMediumId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPaymentMediumId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_medium_id', 'Payment medium id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valApPaymentTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApPaymentTypeId() ) || 0 >= ( int ) $this->getApPaymentTypeId() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payment_type_id', 'Payment method is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentDate() {

		$boolIsValid = true;

		if( true == is_null( $this->getPaymentDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_date', 'Payment date is required.' ) );
		} elseif( true != CValidation::validateDate( $this->getPaymentDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_date', 'Payment date of format mm/dd/yyyy is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIssueDatetime() {

		$boolIsValid = true;

		if( true == is_null( $this->getIssueDatetime() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_date', 'Issue date is required.' ) );
		} elseif( 0 != strcmp( 'NOW()', $this->getIssueDatetime() ) && false == CValidation::validateDateTime( $this->getIssueDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_date', 'Issue date of format yyyy-mm-dd hh:mm:ss is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getPaymentAmount() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'Payment amount is required.' ) );
		} elseif( 0 >= $this->getPaymentAmount() && false == $this->getIsCrossAllocation() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', ' Invalid \'' . $this->getApPaymentTypeName() . '\' amount for vendor(s) \'' . $this->getPayeeName() . '\' and bank account(s) \'' . $this->getAccountName() . '\' for invoice(s) \'' . $this->getHeaderNumber() . '\'.' ) );
		}

		return $boolIsValid;
	}

	public function valPayeeName() {
		$boolIsValid = true;

		if( true == is_null( $this->getPayeeName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payee_name', 'Payee is required.' ) );
		}

		return $boolIsValid;
	}

	public function validateVoidCheck() {
		$boolIsValid = true;

		if( CPaymentStatusType::VOIDED == $this->getPaymentStatusTypeId() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type', 'This payment is already voided.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->getPaymentNumber() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_number', 'Payment number is required.' ) );
		} elseif( 40 < strlen( $this->getPaymentNumber() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_number', 'Payment number cannot exceed 40 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentNumberForPreprinted( $objClientDatabase = NULL ) {

		$boolIsValid = true;

		if( true == is_null( $this->getPaymentNumber() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_number', 'Please enter starting check # for each bank account.' ) );
			return false;
		} elseif( 40 < strlen( $this->getPaymentNumber() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_number', 'Check number cannot exceed 40 characters : \'' . $this->getPaymentNumber() ) );
			return false;
		}

		if( false == in_array( $this->getApPaymentTypeId(), [ CApPaymentType::CHECK, CApPaymentType::AVID_PAY ] ) || false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valIsManual() {

		if( true == $this->isElectronicPayment() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'ap_payment_type_id', 'Electronic payments cannot be processed as manual payments.' ) );
			return false;
		}

		return true;
	}

	public function validateApPayments() {
		$boolIsValid = true;
// 1. $this->addErrorMsg( new CErrorMsg( NULL, 'payment_queue_empty', 'Please add invoice(s) to the payment queue.' ) );
// 2. $this->addErrorMsg( new CErrorMsg( NULL, 'selected_ap_detail_ids', 'Please select at least one invoice for processing.' ) );
// 3. $this->addErrorMsg( new CErrorMsg( NULL, 'selected_ap_detail_ids', 'One or more of the invoices in payment queue has been updated by another user, please refresh the page and try again.' ) );
//		-- when false == is_numeric( $arrobjApDetails[$intApDetailId]->getPaymentApprovedBy() )
// 4. $strValidationMessage = 'Invalid payment amount for invoice #' . $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber() . '. ';
//		if payment amount is incorrect. If payment amount is more than due amount.


		return $boolIsValid;
	}

	public function validateFinalizePayment( $arrmixCheckCreditCardApAllocations, $arrmixDebitCardWireTransferApAllocations, $objClientDatabase ) {

		$boolIsValid										= true;
		$boolIsOtherPaymentType                             = true;
		$arrintApDetailIds									= [];
		$arrintCheckCreditCardApDetailIds					= [];
		$arrintChecksAndCreditCardApHeaderIds				= [];
		$arrintDebitCardAndWireTransferApHeaderIds			= [];
		$arrintApHeaderwiseApPaymentIds						= [];
		$arrobjApHeaderWiseApDetails						= [];
		$arrobjPropertyBankAccountsGroupedByBankAccount		= [];
		$arrmixApDetailsForCheckAndCreditCard				= [];
		$arrmixApDetailsForAch								= [];
		$arrmixApDetailsForDebitCardAndWireTransfer			= [];

		$arrstrApPaymentTypes								= CApPaymentType::loadApPaymentTypes();

		if( false == valArr( $arrmixCheckCreditCardApAllocations ) && false == valArr( $arrmixDebitCardWireTransferApAllocations ) ) {

			$this->addErrorMsg( new CErrorMsg( NULL, 'payment_queue_empty', 'Please add invoice(s) to the payment queue.' ) );
			$boolIsValid &= false;
		}

		foreach( $arrmixCheckCreditCardApAllocations as $arrintApDetailWiseAmount ) {
			foreach( $arrintApDetailWiseAmount as $intApDetailId => $fltAllocationAmount ) {
				if( 0 != $fltAllocationAmount ) {

					$arrintCheckCreditCardApDetailIds[$intApDetailId]	= $fltAllocationAmount;
					$arrintApDetailIds[$intApDetailId]					= $intApDetailId;
				}
			}
		}

		foreach( $arrmixDebitCardWireTransferApAllocations as $arrintPaymentNumberWiseApDetailsAmount ) {
			foreach( $arrintPaymentNumberWiseApDetailsAmount as $arrintApDetailWiseAmount ) {
				foreach( $arrintApDetailWiseAmount as $intApDetailId => $fltAllocationAmount ) {
					if( 0 != $fltAllocationAmount ) {
						$arrintApDetailIds[$intApDetailId]	= $intApDetailId;
					}
				}
			}
		}

		if( false == valArr( $arrintApDetailIds ) ) {

			$this->addErrorMsg( new CErrorMsg( NULL, 'selected_ap_detail_ids', 'Please select at least one invoice for processing.' ) );
			$boolIsValid &= false;
		}

		$arrobjApDetails			= ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchApDetailsByIdsByCid( $arrintApDetailIds, $this->getCid(), $objClientDatabase );
		$arrobjAllApHeaders			= ( array ) CApHeaders::fetchApHeadersWithPayeeNameByIdsByCid( array_filter( array_keys( rekeyObjects( 'ApHeaderId', $arrobjApDetails ) ) ), $this->getCid(), $objClientDatabase );
		$arrobjPropertyGlSettings 	= ( array ) rekeyObjects( 'PropertyId', CPropertyGlSettings::fetchPropertyGlSettingsByPropertyIdsByCid( array_keys( rekeyObjects( 'PropertyId', $arrobjApDetails ) ), $this->getCid(), $objClientDatabase ) );
		$arrobjPropertyBankAccounts	= ( array ) CPropertyBankAccounts::fetchPropertyBankAccountsByBankAccountIdsByCid( array_filter( array_keys( rekeyObjects( 'BankAccountId', $arrobjApDetails ) ) ), $this->getCid(), $objClientDatabase, true );

		foreach( $arrobjPropertyBankAccounts as $objPropertyBankAccount ) {
			$arrobjPropertyBankAccountsGroupedByBankAccount[$objPropertyBankAccount->getBankAccountId()][$objPropertyBankAccount->getPropertyId()] 	= $objPropertyBankAccount;
		}

		foreach( $arrobjApDetails as $objApDetail ) {
			if( true == array_key_exists( $objApDetail->getBankAccountId(), $arrobjPropertyBankAccountsGroupedByBankAccount ) ) {
				$objApDetail->setPropertyBankAccounts( $arrobjPropertyBankAccountsGroupedByBankAccount[$objApDetail->getBankAccountId()] );
			}

			if( true == array_key_exists( $objApDetail->getPropertyId(), $arrobjPropertyGlSettings ) ) {
				$objApDetail->setPropertyGlSetting( $arrobjPropertyGlSettings[$objApDetail->getPropertyId()] );
			}

			$objApDetail->setGlAccountName( $objApDetail->getGlAccountNumber() . ' : ' . $objApDetail->getGlAccountName() );
			$boolIsValid &= $objApDetail->validate( 'validate_gl_account' );
			$this->addErrorMsgs( $objApDetail->getErrorMsgs() );
		}

		$arrmixParameters = [
			'ap_header_ids'			=> array_filter( array_keys( $arrobjAllApHeaders ) )
		];

		$arrobjApDetails		= ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchSimpleApDetailsByApHeaderIdsByCid( $arrmixParameters, $this->getCid(), $objClientDatabase );
		$arrobjApPayees			= ( array ) \Psi\Eos\Entrata\CApPayees::createService()->fetchApPayeesByCid( $this->getCid(), $objClientDatabase );
		$arrobjBankAccounts		= ( array ) \Psi\Eos\Entrata\CBankAccounts::createService()->fetchBankAccountsByCid( $this->getCid(), $objClientDatabase );
		$objCompanyPreference	= \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'ALLOW_CREDITS_TO_CROSS_APPLY_PROPERTIES', $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrmixCheckCreditCardApAllocations ) ) {
			$boolIsOtherPaymentType = false;
		}

		foreach( $arrobjApDetails as $intApDetailId => $objApDetail ) {

			if( false == $boolIsOtherPaymentType && true == array_key_exists( $intApDetailId, $arrintCheckCreditCardApDetailIds ) ) {
				$arrmixApDetailsForCheckAndCreditCard[$arrobjAllApHeaders[$objApDetail->getApHeaderId()]->getApPayeeId()][$objApDetail->getBankAccountId()][$objApDetail->getPropertyId()][$objApDetail->getApHeaderId()][$objApDetail->getId()] = $objApDetail;
			} elseif( true == $boolIsOtherPaymentType ) {
				$arrmixApDetailsForCheckAndCreditCard[$arrobjAllApHeaders[$objApDetail->getApHeaderId()]->getApPayeeId()][$objApDetail->getBankAccountId()][$objApDetail->getPropertyId()][$objApDetail->getApHeaderId()][$objApDetail->getId()] = $objApDetail;
			}

			$arrobjApHeaderWiseApDetails[$arrobjAllApHeaders[$objApDetail->getApHeaderId()]->getApPayeeId()][$objApDetail->getBankAccountId()][$objApDetail->getPropertyId()][$objApDetail->getApHeaderId()][$intApDetailId]                 = $objApDetail;
		}

		foreach( $arrmixCheckCreditCardApAllocations as $intApPaymentTypeId => $arrmixApDetailPayments ) {
			foreach( array_keys( $arrmixApDetailPayments ) as $intApDetailId ) {

				if( true == array_key_exists( $intApDetailId, $arrobjApDetails ) && true == is_numeric( $arrobjApDetails[$intApDetailId]->getPaymentApprovedBy() ) ) {

					$arrintChecksAndCreditCardApHeaderIds[$arrobjApDetails[$intApDetailId]->getApHeaderId()]	= $arrobjApDetails[$intApDetailId]->getApHeaderId();
					$arrintApHeaderwiseApPaymentIds[$arrobjApDetails[$intApDetailId]->getApHeaderId()]			= $intApPaymentTypeId;

				} else {

					$this->addErrorMsg( new CErrorMsg( NULL, 'selected_ap_detail_ids', 'One or more of the invoices in payment queue has been updated by another user, please refresh the page and try again.' ) );
					$boolIsValid &= false;
					break 2;
				}
			}
		}

		if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjAllApHeaders ) ) {

			$boolPayWithSingleCheck = false;
			$intInvoiceCount        = 0;

			foreach( $arrobjAllApHeaders as $intApHeaderId => $objApHeader ) {

				$strPropertyName                        = '';

				$fltHeaderAmount                        = 0;
				$fltTotalPositiveTransactionAmountDue   = 0;
				$fltTotalNegativeTransactionAmountDue   = 0;

				foreach( $arrobjApDetails as $intApDetailId => $objApDetail ) {

					if( $intApHeaderId == $objApDetail->getApHeaderId() ) {
						if( 0 < $objApDetail->getTransactionAmountDue() ) {
							$fltTotalPositiveTransactionAmountDue = bcadd( $fltTotalPositiveTransactionAmountDue, $objApDetail->getTransactionAmountDue(), 2 );
						} else {
							$fltTotalNegativeTransactionAmountDue = bcadd( $fltTotalNegativeTransactionAmountDue, $objApDetail->getTransactionAmountDue(), 2 );
						}

						$fltHeaderAmount = bcadd( $fltTotalPositiveTransactionAmountDue, $fltTotalNegativeTransactionAmountDue, 2 );

						if( 0 > $objApDetail->getTransactionAmountDue() ) {
							$strPropertyName = $objApDetail->getPropertyName();
						}
					}
				}

				if( 0 > $fltHeaderAmount && true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() && false == $objApHeader->getPayWithSingleCheck() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'Amount should be greater than 0 for property \'' . $strPropertyName . '\' and Invoice \'' . $objApHeader->getHeaderNumber() . '\'.' ) );
				} elseif( true == $objApHeader->getPayWithSingleCheck() ) {
					$boolPayWithSingleCheck = true;
					$intInvoiceCount        = $intInvoiceCount + 1;
				}
			}
		}

		// Validate check and credit card payment
		foreach( $arrmixApDetailsForCheckAndCreditCard as $intApPayeeId => $arrmixApDetailsGroupedByApPayee ) {
			foreach( $arrmixApDetailsGroupedByApPayee as $intBankAccountId => $arrmixApDetailsGroupedByProperty ) {

				$strValidationMessage	                        = NULL;
				$arrstrHeaderNumber		                        = [];

				if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() ) {
					$fltTotalByBankByVendorByPropertyAmountToPay    = 0;
				}

				foreach( $arrmixApDetailsGroupedByProperty as $intPropertyId => $arrmixApDetailsGroupedByApHeader ) {

					if( false == valObj( $objCompanyPreference, 'CCompanyPreference' ) || ( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 0 == $objCompanyPreference->getValue() ) ) {
						$fltTotalByBankByVendorByPropertyAmountToPay    = 0;
					}

					foreach( $arrmixApDetailsGroupedByApHeader as $intApHeaderId => $arrobjApDetail ) {

						if( false == array_key_exists( $intApHeaderId, $arrintChecksAndCreditCardApHeaderIds ) ) {
							break 3;
						}

						$boolSelfAllocation								= true;
						$fltHeaderAmount								= 0.00;
						$fltTotalPositivePaymentAmount					= 0.00;
						$fltTotalNegativePaymentAmount					= 0.00;
						$fltTotalPositiveTransactionAmountDue			= 0.00;
						$fltTotalNegativeTransactionAmountDue			= 0.00;
						$fltTotalRemainingPositiveTransactionAmountDue	= 0.00;
						$fltTotalRemainingNegativeTransactionAmountDue	= 0.00;

						foreach( $arrobjApDetail as $intApDetailId => $objApDetail ) {

							if( 0 < $objApDetail->getTransactionAmountDue() ) {
								$fltTotalPositiveTransactionAmountDue = bcadd( $fltTotalPositiveTransactionAmountDue, $objApDetail->getTransactionAmountDue(), 2 );
							} else {
								$fltTotalNegativeTransactionAmountDue = bcadd( $fltTotalNegativeTransactionAmountDue, $objApDetail->getTransactionAmountDue(), 2 );
							}

							$fltHeaderAmount	= bcadd( $fltTotalPositiveTransactionAmountDue, $fltTotalNegativeTransactionAmountDue, 2 );

							// check if allocation amount is less than or equal to ApDetailAmountDue
							if( true == array_key_exists( $intApDetailId, $arrintCheckCreditCardApDetailIds ) ) {

								$fltApDetailPaymentAmount						= $arrintCheckCreditCardApDetailIds[$intApDetailId];
								$fltTotalByBankByVendorByPropertyAmountToPay	= bcadd( $fltTotalByBankByVendorByPropertyAmountToPay, $fltApDetailPaymentAmount, 2 );

								// Check payment amount is less than payment amount due
								if( abs( $fltApDetailPaymentAmount ) <= abs( $objApDetail->getTransactionAmountDue() ) ) {
									if( 0 < $fltApDetailPaymentAmount ) {

										$fltTotalPositivePaymentAmount = bcadd( $fltTotalPositivePaymentAmount, $fltApDetailPaymentAmount, 2 );
									} else {
										$fltTotalNegativePaymentAmount = bcadd( $fltTotalNegativePaymentAmount, $fltApDetailPaymentAmount, 2 );
									}

									// calculate amount due after current payment
									$fltTransactionAmountDue = $objApDetail->getTransactionAmountDue();
									$fltTransactionAmountDue = bcsub( $fltTransactionAmountDue, $fltApDetailPaymentAmount, 2 );

								} else {
									$strValidationMessage = 'Invalid payment amount for invoice #' . $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber() . '. ';
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
									$boolIsValid &= false;
								}
							}

							$fltTotalRemainingPositiveTransactionAmountDue	= bcsub( $fltTotalPositiveTransactionAmountDue, $fltTotalPositivePaymentAmount, 2 );
							$fltTotalRemainingNegativeTransactionAmountDue	= bcsub( $fltTotalNegativeTransactionAmountDue, $fltTotalNegativePaymentAmount, 2 );
						}

						if( 0 <= $fltHeaderAmount ) {
							if( $fltTotalPositivePaymentAmount < abs( $fltTotalNegativePaymentAmount ) ) {
								$boolSelfAllocation &= false;
							} elseif( $fltTotalRemainingPositiveTransactionAmountDue < abs( $fltTotalRemainingNegativeTransactionAmountDue ) ) {
								$boolSelfAllocation &= false;
							}
						} else {
							if( $fltTotalPositivePaymentAmount > abs( $fltTotalNegativePaymentAmount ) ) {
								$boolSelfAllocation &= false;
							} elseif( $fltTotalRemainingPositiveTransactionAmountDue > abs( $fltTotalRemainingNegativeTransactionAmountDue ) ) {
								$boolSelfAllocation &= false;
							}
						}

						if( false == $boolSelfAllocation && true == array_key_exists( $intApHeaderId, $arrobjAllApHeaders ) ) {
							$strValidationMessage = 'Invalid payment amount for invoice #' . $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber() . '. ';
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
							$boolIsValid &= false;
						}

						if( 0 <= ( bcsub( $fltTotalByBankByVendorByPropertyAmountToPay, ( bcadd( $fltTotalPositivePaymentAmount, $fltTotalNegativePaymentAmount, 2 ) ), 2 ) ) && 0 > $fltTotalByBankByVendorByPropertyAmountToPay ) {
							$arrstrHeaderNumber[$intApHeaderId] = $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber();
							$intApPaymentTypeId					= $arrintApHeaderwiseApPaymentIds[$intApHeaderId];
						}
					}

					if( ( false == valObj( $objCompanyPreference, 'CCompanyPreference' ) || ( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 0 == $objCompanyPreference->getValue() ) ) && $fltTotalByBankByVendorByPropertyAmountToPay <= 0
					    && true == array_key_exists( $intApPayeeId, $arrobjApPayees ) && true == array_key_exists( $intBankAccountId, $arrobjBankAccounts )
					    && ( CApPaymentType::CHECK == $intApPaymentTypeId || CApPaymentType::ACH == $intApPaymentTypeId || CApPaymentType::BILL_PAY == $intApPaymentTypeId || CApPaymentType::WIRE_TRANSFER == $intApPaymentTypeId ) ) {

						$strValidationMessage = 'Invalid \'' . $arrstrApPaymentTypes[$intApPaymentTypeId] . '\' amount for vendor \'' . $arrobjApPayees[$intApPayeeId]->getCompanyName() . '\' and bank account \'' . $arrobjBankAccounts[$intBankAccountId]->getAccountName() . '\' for invoices \'' . implode( ' , ', $arrstrHeaderNumber ) . '\' .';
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
						$boolIsValid &= false;
					}
				}

				if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() && $fltTotalByBankByVendorByPropertyAmountToPay <= 0
				    && true == array_key_exists( $intApPayeeId, $arrobjApPayees ) && true == array_key_exists( $intBankAccountId, $arrobjBankAccounts ) ) {

					$strValidationMessage = 'Invalid \'' . $arrstrApPaymentTypes[$intApPaymentTypeId] . '\' amount for vendor \'' . $arrobjApPayees[$intApPayeeId]->getCompanyName() . '\' and bank account \'' . $arrobjBankAccounts[$intBankAccountId]->getAccountName() . '\' for invoices \'' . implode( ' , ', $arrstrHeaderNumber ) . '\' .';
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
					$boolIsValid &= false;
				}
			}
		}

		// validation for debit card and wire transfer
		foreach( $arrmixDebitCardWireTransferApAllocations as $intApPaymentTypeId => $arrintPaymentNumberWiseApDetailsAmount ) {
			foreach( $arrintPaymentNumberWiseApDetailsAmount as $intPaymentNumberCounter => $arrintApDetailWiseAmount ) {
				foreach( $arrintApDetailWiseAmount as $intApDetailId => $fltAllocationAmount ) {

					if( true == array_key_exists( $intApDetailId, $arrobjApDetails ) ) {

						$intApHeaderId		= $arrobjAllApHeaders[$arrobjApDetails[$intApDetailId]->getApHeaderId()]->getId();
						$intBankAccountId	= $arrobjApDetails[$intApDetailId]->getBankAccountId();
						$intApPayeeId		= $arrobjAllApHeaders[$intApHeaderId]->getApPayeeId();
						$intPropertyId		= $arrobjApDetails[$intApDetailId]->getPropertyId();

						$arrintDebitCardAndWireTransferApHeaderIds[$intApHeaderId] = $intApHeaderId;
						if( CApPaymentType::ACH == $intApPaymentTypeId ) {
							$arrmixApDetailsForAch[$intApPayeeId][$intBankAccountId][$intPropertyId][$intApHeaderId] = $arrobjApHeaderWiseApDetails[$intApPayeeId][$intBankAccountId][$intPropertyId][$intApHeaderId];
						} else {
							$arrmixApDetailsForDebitCardAndWireTransfer[$intApPaymentTypeId][$intPaymentNumberCounter][$intApPayeeId][$intBankAccountId][$intPropertyId][$intApHeaderId] = $arrobjApHeaderWiseApDetails[$intApPayeeId][$intBankAccountId][$intPropertyId][$intApHeaderId];
						}

					} else {
						$this->addErrorMsg( new CErrorMsg( NULL, 'selected_ap_detail_ids', 'One or more of the invoices in payment queue has been updated by another user, please refresh the page and try again.' ) );
						$boolIsValid &= false;
						break 3;
					}
				}
			}
		}

		if( true == valArr( $arrmixApDetailsForAch ) ) {

			foreach( $arrmixApDetailsForAch as $intApPayeeId => $arrmixApDetailsGroupedByApPayee ) {
				foreach( $arrmixApDetailsGroupedByApPayee as $intBankAccountId => $arrmixApDetailsGroupedByProperty ) {

					$arrstrHeaderNumber		= [];
					$strValidationMessage	= NULL;

					if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() ) {
						$fltTotalByBankByVendorByPropertyAmountToPay    = 0;
					}

					foreach( $arrmixApDetailsGroupedByProperty as $intPropertyId => $arrmixApDetailsGroupedByApHeader ) {

						if( false == valObj( $objCompanyPreference, 'CCompanyPreference' ) || ( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 0 == $objCompanyPreference->getValue() ) ) {
							$fltTotalByBankByVendorByPropertyAmountToPay = 0;
						}

						foreach( $arrmixApDetailsGroupedByApHeader as $intApHeaderId => $arrobjApDetail ) {

							if( false == array_key_exists( $intApHeaderId, $arrintChecksAndCreditCardApHeaderIds ) ) {
								continue;
							}

							$boolSelfAllocation								= true;
							$fltHeaderAmount								= 0.00;
							$fltTotalPositivePaymentAmount					= 0.00;
							$fltTotalNegativePaymentAmount					= 0.00;
							$fltTotalPositiveTransactionAmountDue			= 0.00;
							$fltTotalNegativeTransactionAmountDue			= 0.00;
							$fltTotalRemainingPositiveTransactionAmountDue	= 0.00;
							$fltTotalRemainingNegativeTransactionAmountDue	= 0.00;

							foreach( $arrobjApDetail as $intApDetailId => $objApDetail ) {

								if( 0 < $objApDetail->getTransactionAmountDue() ) {
									$fltTotalPositiveTransactionAmountDue = bcadd( $fltTotalPositiveTransactionAmountDue, $objApDetail->getTransactionAmountDue(), 2 );
								} else {
									$fltTotalNegativeTransactionAmountDue = bcadd( $fltTotalNegativeTransactionAmountDue, $objApDetail->getTransactionAmountDue(), 2 );
								}

								$fltHeaderAmount	= bcadd( $fltTotalPositiveTransactionAmountDue, $fltTotalNegativeTransactionAmountDue, 2 );

								// check if allocation amount is less than or equal to ApDetailAmountDue
								if( true == array_key_exists( $intApDetailId, $arrintCheckCreditCardApDetailIds ) ) {

									$fltApDetailPaymentAmount						= $arrintCheckCreditCardApDetailIds[$intApDetailId];
									$fltTotalByBankByVendorByPropertyAmountToPay	= bcadd( $fltTotalByBankByVendorByPropertyAmountToPay, $fltApDetailPaymentAmount, 2 );

									// Check payment amount is less than payment amount due
									if( abs( $fltApDetailPaymentAmount ) <= abs( $objApDetail->getTransactionAmountDue() ) ) {
										if( 0 < $fltApDetailPaymentAmount ) {

											$fltTotalPositivePaymentAmount = bcadd( $fltTotalPositivePaymentAmount, $fltApDetailPaymentAmount, 2 );
										} else {
											$fltTotalNegativePaymentAmount = bcadd( $fltTotalNegativePaymentAmount, $fltApDetailPaymentAmount, 2 );
										}

										// calculate amount due after current payment
										$fltTransactionAmountDue = $objApDetail->getTransactionAmountDue();
										$fltTransactionAmountDue = bcsub( $fltTransactionAmountDue, $fltApDetailPaymentAmount, 2 );

									} else {
										$strValidationMessage = 'Invalid payment amount for invoice #' . $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber() . '. ';
										$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
										$boolIsValid &= false;
									}
								}

								$fltTotalRemainingPositiveTransactionAmountDue	= bcsub( $fltTotalPositiveTransactionAmountDue, $fltTotalPositivePaymentAmount, 2 );
								$fltTotalRemainingNegativeTransactionAmountDue	= bcsub( $fltTotalNegativeTransactionAmountDue, $fltTotalNegativePaymentAmount, 2 );
							}

							if( 0 <= $fltHeaderAmount ) {

								if( $fltTotalPositivePaymentAmount < abs( $fltTotalNegativePaymentAmount ) ) {
									$boolSelfAllocation &= false;
								} elseif( $fltTotalRemainingPositiveTransactionAmountDue < abs( $fltTotalRemainingNegativeTransactionAmountDue ) ) {
									$boolSelfAllocation &= false;
								}

							} else {

								if( $fltTotalPositivePaymentAmount > abs( $fltTotalNegativePaymentAmount ) ) {
									$boolSelfAllocation &= false;
								} elseif( $fltTotalRemainingPositiveTransactionAmountDue > abs( $fltTotalRemainingNegativeTransactionAmountDue ) ) {
									$boolSelfAllocation &= false;
								}
							}

							if( false == $boolSelfAllocation && true == array_key_exists( $intApHeaderId, $arrobjAllApHeaders ) ) {
								$strValidationMessage = 'Invalid payment amount for invoice #' . $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber() . '. ';
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
								$boolIsValid &= false;
							}

							if( 0 <= ( bcsub( $fltTotalByBankByVendorByPropertyAmountToPay, ( bcadd( $fltTotalPositivePaymentAmount, $fltTotalNegativePaymentAmount, 2 ) ), 2 ) ) && 0 > $fltTotalByBankByVendorByPropertyAmountToPay ) {
								$arrstrHeaderNumber[$intApHeaderId] = $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber();
								$intApPaymentTypeId					= $arrintApHeaderwiseApPaymentIds[$intApHeaderId];
							}
						}

						if( ( false == valObj( $objCompanyPreference, 'CCompanyPreference' ) || ( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 0 == $objCompanyPreference->getValue() ) ) && $fltTotalByBankByVendorByPropertyAmountToPay <= 0
						    && true == array_key_exists( $intApPayeeId, $arrobjApPayees ) && true == array_key_exists( $intBankAccountId, $arrobjBankAccounts ) ) {

							$strValidationMessage = 'Invalid \'' . $arrstrApPaymentTypes[$intApPaymentTypeId] . '\' amount for vendor \'' . $arrobjApPayees[$intApPayeeId]->getCompanyName() . '\' and bank account \'' . $arrobjBankAccounts[$intBankAccountId]->getAccountName() . '\' for invoices \'' . implode( ' , ', $arrstrHeaderNumber ) . '\' .';
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
							$boolIsValid &= false;
						}
					}

					if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() && $fltTotalByBankByVendorByPropertyAmountToPay <= 0
					    && true == array_key_exists( $intApPayeeId, $arrobjApPayees ) && true == array_key_exists( $intBankAccountId, $arrobjBankAccounts ) ) {

						$strValidationMessage = 'Invalid \'' . $arrstrApPaymentTypes[$intApPaymentTypeId] . '\' amount for vendor \'' . $arrobjApPayees[$intApPayeeId]->getCompanyName() . '\' and bank account \'' . $arrobjBankAccounts[$intBankAccountId]->getAccountName() . '\' for invoices \'' . implode( ' , ', $arrstrHeaderNumber ) . '\' .';
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
						$boolIsValid &= false;
					}
				}
			}

		} else {
			foreach( $arrmixApDetailsForDebitCardAndWireTransfer as $intApPaymentTypeId => $arrmixPaymentNumberWiseDetails ) {
				foreach( $arrmixPaymentNumberWiseDetails as $intPaymentNumberCounter => $arrmixApDetailsGroupedByPaymentNumber ) {
					foreach( $arrmixApDetailsGroupedByPaymentNumber as $intApPayeeId => $arrmixApDetailsGroupedByApPayee ) {
						foreach( $arrmixApDetailsGroupedByApPayee as $intBankAccountId => $arrmixApDetailsGroupedByProperty ) {
							$arrstrHeaderNumber		= [];
							$strValidationMessage	= NULL;

							if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() ) {
								$fltTotalByBankByVendorByPropertyAmountToPay    = 0;
							}

							foreach( $arrmixApDetailsGroupedByProperty as $intPropertyId => $arrmixApDetailsGroupedByApHeader ) {

								if( false == valObj( $objCompanyPreference, 'CCompanyPreference' ) || ( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 0 == $objCompanyPreference->getValue() ) ) {
									$fltTotalByBankByVendorByPropertyAmountToPay = 0;
								}

								foreach( $arrmixApDetailsGroupedByApHeader as $intApHeaderId => $arrobjApDetail ) {

									$boolSelfAllocation								= true;
									$fltHeaderAmount								= 0.00;
									$fltTotalPositiveTransactionAmountDue			= 0.00;
									$fltTotalNegativeTransactionAmountDue			= 0.00;
									$fltTotalPositivePaymentAmount					= 0.00;
									$fltTotalNegativePaymentAmount					= 0.00;
									$fltTotalRemainingPositiveTransactionAmountDue	= 0.00;
									$fltTotalRemainingNegativeTransactionAmountDue	= 0.00;

									foreach( $arrobjApDetail as $intApDetailId => $objApDetail ) {

										if( 0 < $objApDetail->getTransactionAmountDue() ) {
											$fltTotalPositiveTransactionAmountDue = bcadd( $fltTotalPositiveTransactionAmountDue, $objApDetail->getTransactionAmountDue(), 2 );
										} else {
											$fltTotalNegativeTransactionAmountDue = bcadd( $fltTotalNegativeTransactionAmountDue, $objApDetail->getTransactionAmountDue(), 2 );
										}

										$fltHeaderAmount	= bcadd( $fltTotalPositiveTransactionAmountDue, $fltTotalNegativeTransactionAmountDue, 2 );

										// check if allocation amount is less than or equal to ApDetailAmountDue
										if( true == array_key_exists( $intApDetailId, $arrmixDebitCardWireTransferApAllocations[$intApPaymentTypeId][$intPaymentNumberCounter] ) ) {

											$fltApDetailPaymentAmount						= $arrmixDebitCardWireTransferApAllocations[$intApPaymentTypeId][$intPaymentNumberCounter][$intApDetailId];
											$fltTotalByBankByVendorByPropertyAmountToPay	= bcadd( $fltTotalByBankByVendorByPropertyAmountToPay, $fltApDetailPaymentAmount, 2 );

											// Check payment amount is less than approved amount
											if( abs( $fltApDetailPaymentAmount ) <= abs( $objApDetail->getTransactionAmountDue() ) ) {
												if( 0 < $fltApDetailPaymentAmount ) {
													$fltTotalPositivePaymentAmount = bcadd( $fltTotalPositivePaymentAmount, $fltApDetailPaymentAmount, 2 );
												} else {
													$fltTotalNegativePaymentAmount = bcadd( $fltTotalNegativePaymentAmount, $fltApDetailPaymentAmount, 2 );
												}

												// calculate amount due after current payment
												$fltTransactionAmountDue = $objApDetail->getTransactionAmountDue();
												$fltTransactionAmountDue = bcsub( $fltTransactionAmountDue, $fltApDetailPaymentAmount, 2 );

											} else {
												$strValidationMessage = 'Invalid payment amount for invoice #' . $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber() . '. ';
												$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
												$boolIsValid &= false;
											}
										}

										$fltTotalRemainingPositiveTransactionAmountDue	= bcsub( $fltTotalPositiveTransactionAmountDue, $fltTotalPositivePaymentAmount, 2 );
										$fltTotalRemainingNegativeTransactionAmountDue	= bcsub( $fltTotalNegativeTransactionAmountDue, $fltTotalNegativePaymentAmount, 2 );
									}

									if( 0 <= $fltHeaderAmount ) {

										if( $fltTotalPositivePaymentAmount < abs( $fltTotalNegativePaymentAmount ) ) {
											$boolSelfAllocation &= false;
										} elseif( $fltTotalRemainingPositiveTransactionAmountDue < abs( $fltTotalRemainingNegativeTransactionAmountDue ) ) {
											$boolSelfAllocation &= false;
										}
									} else {
										if( $fltTotalPositivePaymentAmount > abs( $fltTotalNegativePaymentAmount ) ) {
											$boolSelfAllocation &= false;
										} elseif( $fltTotalRemainingPositiveTransactionAmountDue > abs( $fltTotalRemainingNegativeTransactionAmountDue ) ) {
											$boolSelfAllocation &= false;
										}
									}

									if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() ) {
										if( true == $boolPayWithSingleCheck && $intInvoiceCount > 1 && 0 > $fltHeaderAmount && CApPaymentType::WIRE_TRANSFER != $intApPaymentTypeId ) {

											$strValidationMessage = 'Invalid payment amount for invoice #' . $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber() . '. ';
											$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
											$boolIsValid &= false;
											break;
										}
									}

									if( false == $boolSelfAllocation && true == array_key_exists( $intApHeaderId, $arrobjAllApHeaders ) ) {

										$strValidationMessage = 'Invalid payment amount for invoice #' . $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber() . '. ';
										$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
										$boolIsValid &= false;
									}

									if( 0 <= ( bcsub( $fltTotalByBankByVendorByPropertyAmountToPay, ( bcadd( $fltTotalPositivePaymentAmount, $fltTotalNegativePaymentAmount, 2 ) ), 2 ) ) && 0 > $fltTotalByBankByVendorByPropertyAmountToPay ) {
										$arrstrHeaderNumber[$intApHeaderId] = $arrobjAllApHeaders[$intApHeaderId]->getHeaderNumber();
									}
								}

								if( ( false == valObj( $objCompanyPreference, 'CCompanyPreference' ) || ( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 0 == $objCompanyPreference->getValue() ) ) && $fltTotalByBankByVendorByPropertyAmountToPay <= 0
								    && true == array_key_exists( $intApPayeeId, $arrobjApPayees ) && true == array_key_exists( $intBankAccountId, $arrobjBankAccounts ) ) {

									$strValidationMessage = 'Invalid \'' . $arrstrApPaymentTypes[$intApPaymentTypeId] . '\' amount for vendor \'' . $arrobjApPayees[$intApPayeeId]->getCompanyName() . '\' and bank account \'' . $arrobjBankAccounts[$intBankAccountId]->getAccountName() . '\' for invoices \'' . implode( ' , ', $arrstrHeaderNumber ) . '\' .';
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
									$boolIsValid &= false;
								}
							}

							if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) && 1 == $objCompanyPreference->getValue() && $fltTotalByBankByVendorByPropertyAmountToPay <= 0
							    && true == array_key_exists( $intApPayeeId, $arrobjApPayees ) && true == array_key_exists( $intBankAccountId, $arrobjBankAccounts ) ) {

								$strValidationMessage = 'Invalid \'' . $arrstrApPaymentTypes[$intApPaymentTypeId] . '\' amount for vendor \'' . $arrobjApPayees[$intApPayeeId]->getCompanyName() . '\' and bank account \'' . $arrobjBankAccounts[$intBankAccountId]->getAccountName() . '\' for invoices \'' . implode( ' , ', $arrstrHeaderNumber ) . '\' .';
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );
								$boolIsValid &= false;
							}
						}
					}
				}
			}
		}
		return $boolIsValid;
	}

	public function valCreditCardBankAccount() {
		$boolIsValid = true;

		if( ( CApPaymentType::CREDIT_CARD == $this->getApPaymentTypeId() )
			&& true == valObj( $this->getProcessingBankAccount(), 'CBankAccount' )
			&& 0 == $this->getProcessingBankAccount()->getIsCreditCardAccount() ) {

			$strValidationMessage = 'Invalid credit card bank account \'' . $this->getProcessingBankAccount()->getAccountName() . '\'';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );

			$boolIsValid = false;
		} elseif( ( CApPaymentType::CREDIT_CARD != $this->getApPaymentTypeId() )
				&& true == valObj( $this->getProcessingBankAccount(), 'CBankAccount' )
				&& 1 == $this->getProcessingBankAccount()->getIsCreditCardAccount() ) {

			$strValidationMessage = 'Bank account \'' . $this->getProcessingBankAccount()->getAccountName() . '\' is of type credit card.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', $strValidationMessage ) );

			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valUserPropertyAcssociations() {
		$boolIsValid = true;

		if( false == array_intersect( ( array ) $this->getPaymentPropertyIds(), ( array ) $this->getUserAllowedPropertyIds() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'You do not have permission to view this payment. Please contact your system administrator if you have questions.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valPayeeName();
				$boolIsValid &= $this->valPaymentNumber();
				$boolIsValid &= $this->valPaymentDate();
				$boolIsValid &= $this->valIssueDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			case 'post_manual_payment':
				$boolIsValid &= $this->valPayeeName();
				$boolIsValid &= $this->valPaymentNumber();
				$boolIsValid &= $this->valPaymentDate();
				$boolIsValid &= $this->valIssueDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				$boolIsValid &= $this->valCreditCardBankAccount();
				break;

			case 'insert_quick_check':
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valPayeeName();
				$boolIsValid &= $this->valPaymentNumber();
				$boolIsValid &= $this->valPaymentDate();
				$boolIsValid &= $this->valIssueDatetime();
				$boolIsValid &= $this->valPaymentAmount();
				break;

			case 'validate_void_check':
				$boolIsValid &= $this->validateVoidCheck();
				break;

			case 'view_check':
				$boolIsValid &= $this->valUserPropertyAcssociations();
				break;

			case 'validate_payment_numbers':
				$boolIsValid &= $this->valPaymentNumberForPreprinted( $objClientDatabase );
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

	/**
	 * Process Functions
	 */

	public function processPayment( $strProcessMethod, $intCurrentUserId, $objClientDatabase ) {

		if( CApPaymentType::BILL_PAY == $this->getApPaymentTypeId() ) {
			$this->setApPaymentTypeId( CApPaymentType::BILL_PAY_ACH );
		}

		$boolIsValid = true;
		switch( $strProcessMethod ) {

			case self::PROCESS_METHOD_VOID:
				$boolIsValid &= CApPaymentProcesses::voidPayment( $this, $intCurrentUserId, $objClientDatabase );

				return $boolIsValid;
				break;
			case self::PROCESS_REVERSE_VOIDED_PAYMENT:
				$boolIsValid &= CApPaymentProcesses::reverseVoidedPayment( $this, $intCurrentUserId, $objClientDatabase );
				return $boolIsValid;
				break;
			case self::PROCESS_METHOD_MANUAL_PAYMENT:
			default:
				$boolIsValid &= CApPaymentProcesses::postManualPayment( $this, $intCurrentUserId, $objClientDatabase );
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function addInvoiceApHeader( $objInvoiceApHeader ) {
		if( false == is_null( $objInvoiceApHeader->getId() ) ) {
			$this->m_arrobjInvoiceApHeaders[$objInvoiceApHeader->getId()] = $objInvoiceApHeader;
		} else {
			$this->m_arrobjInvoiceApHeaders[] = $objInvoiceApHeader;
		}
	}

	public function addInvoiceApDetails( $objInvoiceApDetails ) {
		$this->m_arrobjInvoiceApDetails[] = $objInvoiceApDetails;
	}

	public function isElectronicPayment() {

		if( true == is_null( $this->getApPaymentTypeId() ) ) {
			return false;
		}

		return CApPaymentTypes::isElectronicPayment( $this->getApPaymentTypeId() );
	}

	public function fetchApHeader( $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						ap_payment_id = ' . ( int ) $this->getId() . '
						AND cid = ' . ( int ) $this->getCid() . '
						AND ap_header_sub_type_id <> ' . CApHeaderSubType::UNCLAIMED_PROPERTY_PAYMENT . '
					ORDER BY
						id DESC
					LIMIT 1';

		return CApHeaders::fetchApHeader( $strSql, $objClientDatabase );
	}

	public function createApPaymentLog() {

		$objApPaymentLog = new CApPaymentLog();

		$objApPaymentLog->setCid( $this->getCid() );
		$objApPaymentLog->setApPaymentId( $this->getId() );
		$objApPaymentLog->setMerchantAccountId( $this->getMerchantAccountId() );
		$objApPaymentLog->setBankAccountId( $this->getBankAccountId() );
		$objApPaymentLog->setMerchantGatewayId( $this->getMerchantGatewayId() );
		$objApPaymentLog->setPaymentMediumId( $this->getPaymentMediumId() );
		$objApPaymentLog->setApPaymentTypeId( $this->getApPaymentTypeId() );
		$objApPaymentLog->setPaymentStatusTypeId( $this->getPaymentStatusTypeId() );
		$objApPaymentLog->setReturnTypeId( $this->getReturnTypeId() );
		$objApPaymentLog->setCheckAccountTypeId( $this->getCheckAccountTypeId() );
		$objApPaymentLog->setEftReference( $this->getEftReference() );
		$objApPaymentLog->setRemotePrimaryKey( $this->getRemotePrimaryKey() );
		$objApPaymentLog->setReturnRemotePrimaryKey( $this->getReturnRemotePrimaryKey() );
		$objApPaymentLog->setSecureReferenceNumber( $this->getSecureReferenceNumber() );
		$objApPaymentLog->setPaymentMemo( $this->getPaymentMemo() );
		$objApPaymentLog->setPaymentDate( $this->getPaymentDate() );
		$objApPaymentLog->setIssueDatetime( $this->getIssueDatetime() );
		$objApPaymentLog->setPaymentAmount( $this->getPaymentAmount() );
		$objApPaymentLog->setPayeeName( $this->getPayeeName() );
		$objApPaymentLog->setBilltoIpAddress( $this->getBilltoIpAddress() );
		$objApPaymentLog->setPaymentNumber( $this->getPaymentNumber() );
		$objApPaymentLog->setCheckBankName( $this->getCheckBankName() );
		$objApPaymentLog->setCheckNameOnAccount( $this->getCheckNameOnAccount() );
		$objApPaymentLog->setCheckRoutingNumber( $this->getCheckRoutingNumber() );
		$objApPaymentLog->setCheckFractionalRoutingNumber( $this->getCheckFractionalRoutingNumber() );
		$objApPaymentLog->setCheckAccountNumberEncrypted( $this->getCheckAccountNumberEncrypted() );
		$objApPaymentLog->setIsReversed( $this->getIsReversed() );
		$objApPaymentLog->setIsQuickCheck( $this->getIsQuickCheck() );
		$objApPaymentLog->setReturnedOn( $this->getReturnedOn() );

		return $objApPaymentLog;
	}

	public function addApDetailPaymentAmount( $objApDetail ) {
		if( false == is_null( $objApDetail->getId() ) ) {
			$this->m_arrfltApDetailsPaymentAmounts[$objApDetail->getId()] = $objApDetail->getPaymentAmount();
		} else {
			$this->m_arrfltApDetailsPaymentAmounts[] = $objApDetail->getPaymentAmount();
		}
	}

	public function logHistory( $strAction, $intCurrentUserId, $objClientDatabase, $intPaymentApHeaderId = NULL ) {

		$objApPaymentLog = $this->createApPaymentLog();
		$objApPaymentLog->setApHeaderId( $intPaymentApHeaderId );
		$objApPaymentLog->setAction( $strAction );

		if( false == $objApPaymentLog->insert( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $objApPaymentLog->getErrorMsgs() );
			return false;
		}

		return true;
	}

	/**
	 * @param CBankAccount $objBankAccount
	 * @param CDatabase $objDatabase
	 * @return bool
	 */
	public function verifyBalanceByGiact( $objBankAccount, $objDatabase ) {
		// this function is not currently working and should not be used until correctly implemented.
		return false;

		/**
		 * $boolResult = false;
		 *
		 * $objGiact = new CGiactLibrary( CPsProduct::BILL_PAY, $this->getCid(), $objDatabase );
		 *
		 * $strReference = $objBankAccount->getCid() . '-' . $objBankAccount->getId();
		 * $arrstrResponses = $objGiact->verifyBalance( $objBankAccount->getCheckAccountNumberEncryptedWithoutSymbols(), $objBankAccount->getCheckRoutingNumber(), $objBankAccount->getCheckAccountTypeId(), number_format( $this->getPaymentAmount(), 2 ), $strReference );
		 *
		 * if( false == $arrstrResponses ) {
		 * 	return false;
		 * }
		 *
		 * if( true == $objGiact->isPass() ) {
		 * 	$boolResult = true;
		 * }
		 *
		 * return $boolResult;
		 **/
	}

	/**
	 * @param CMerchantAccount $objMerchantAccount
	 * @param CBankAccount $objBankAccount
	 * @param CApRemittance $objApRemittance
	 * @param CDatabase $objClientDatabase
	 * @return int
	 */
	private function determineBillPayRemittanceType( $objMerchantAccount, $objBankAccount, $objApRemittance, $intCompanyUserId, $objClientDatabase ) {

		if( CApPaymentType::CHECK == $objApRemittance->getApPaymentTypeId() ) {
			return CApPaymentType::BILL_PAY_CHECK;
		}

		if( false == $objBankAccount->getIsVerified() && true == is_null( $objBankAccount->getVerifiedOn() ) ) {
			// The bank account has not yet been verified, try to verify it now
			if( true == $objBankAccount->verifyAccount( $objClientDatabase ) ) {
				if( false == $objBankAccount->update( $intCompanyUserId, $objClientDatabase ) ) {
					trigger_error( 'Failed to update bank account (id: ' . $objBankAccount->getId() . ', cid: ' . $objBankAccount->getCid() . ') with account verification results: ' . print_r( $objBankAccount->getErrorMsgs(), true ), E_USER_WARNING );
				}
			}
		}

		if( false == $objApRemittance->getIsVerified() && true == is_null( $objApRemittance->getVerifiedOn() ) ) {
			// The ap remittance bank account has not yet been verified, try to verify it now
			if( true == $objApRemittance->verifyAccount( $objClientDatabase ) ) {
				if( false == $objApRemittance->update( $intCompanyUserId, $objClientDatabase ) ) {
					trigger_error( 'Failed to update ap remittance (id: ' . $objApRemittance->getId() . ', cid: ' . $objBankAccount->getCid() . ') with account verification results: ' . print_r( $objApRemittance->getErrorMsgs(), true ), E_USER_WARNING );
				}
			}
		}

		// If the vendor remittance is not verified, print a check
		if( false == $objApRemittance->getIsVerified() ) {
			return CApPaymentType::BILL_PAY_CHECK;
		}

		// If the bank account is not verified, then print a check
		if( false == $objBankAccount->getIsVerified() ) {
			return CApPaymentType::BILL_PAY_CHECK;
		}

		// If merchant_accounts.ap_ach_is_disabled is true then print checks
		if( true == $objMerchantAccount->getApAchIsDisabled() ) {
			return CApPaymentType::BILL_PAY_CHECK;
		}

		// If Amount of AP Payment > merchant_accounts.ap_ach_max_payment_amount then print check
		if( $this->getPaymentAmount() > $objMerchantAccount->getApAchMaxPaymentAmount() ) {
			return CApPaymentType::BILL_PAY_CHECK;
		}

		// If merchant_accounts.monthly_running_total + Amount of AP Payment > merchant_accounts.ap_ach_monthly_volume_limit then print check
		if( ( $objMerchantAccount->getApAchMonthlyRunningTotal() + $this->getPaymentAmount() ) > $objMerchantAccount->getApAchMonthlyVolumeLimit() ) {
			return CApPaymentType::BILL_PAY_CHECK;
		}

		// Check if bank account is blacklisted
		if( false == $objBankAccount->valCheckBlackListedBankAccount( $objClientDatabase ) ) {
			return CApPaymentType::BILL_PAY_CHECK;
		}

		// Check if vendor remittance is blacklisted
		if( false == $objApRemittance->valCheckBlackListedBankAccount( $objClientDatabase ) ) {
			return CApPaymentType::BILL_PAY_CHECK;
		}

		// If Amount of AP Payment > merchant_accounts.ap_ach_balance_check_amount then
		// Commenting out balance check for now (not MVP) - this is incorrect anyway, this balance check will be asynchronous
		//	if( $this->getPaymentAmount() > $objMerchantAccount->getApAchBalanceCheckAmount() ) {
		//	    $boolIsValid = $this->verifyBalanceByGiact( $objBankAccount );
		//	    if( false == $boolIsValid ) {
		//	        CApPaymentProcesses::sendApPaymentReturnNotification( $objApHeader, $objBankAccount, $objMerchantAccount, $objApRemittance, $boolIsNSFReturned = false, $boolIsPayerBankAccount = true, $intCompanyUserId, $objClientDatabase );
		//	        return;
		//	    } else {
		//	        $boolCheckPrint = false;
		//	    }
		//	 }

		// Everything checks out, we can proceed with ACH
		return CApPaymentType::BILL_PAY_ACH;
	}

	/**
	 * @param CApHeader $objApHeader
	 * @param CApDetail[] $arrobjApDetails
	 * @param CBankAccount $objBankAccount
	 * @param CApRemittance $objApRemittance
	 * @param int $intCompanyUserId
	 * @param CDatabase $objClientDatabase
	 *
	 * @return bool Returns true if ap payment was successfully added to the queue
	 */
	public function addApPaymentInFastFundsQueue( $objApHeader, $arrobjApDetails, $objBankAccount, $objApRemittance, $intCompanyUserId, $objClientDatabase ) {

		global $_objException;

		if( false == valArr( $arrobjApDetails ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid parameter. Missing $arrobjApDetails.' ) );
			return false;
		}

		$objMerchantAccount = CMerchantAccounts::fetchAccountsPayableMerchantAccountByCid( $objApHeader->getCid(), $objClientDatabase );

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load AP merchant account for cid ' . $objApHeader->getCid() ) );
			return false;
		}

		$objMessage = \Psi\Libraries\PaymentQueue\Message\Payments\Operations\DirectDebit\CDirectDebit::create();
		$objMessage->setMerchantGatewayId( CMerchantGateway::TABAPAY_CREDIT_CARD );
		$objMessage->setProcessingBankAccountId( CProcessingBankAccount::ZIONS_TABAPAY );
		$objMessage->setCompanyMerchantAccountId( $objMerchantAccount->getId() );
		$objMessage->setServiceClientId( 3 );
		$objMessage->setUserId( $intCompanyUserId );
		$objMessage->setCId( $this->getCid() );
		$objMessage->setEftTypeId( self::DIRECT_DEBIT );
		$objMessage->setPaymentMediumId( CPaymentMedium::WEB );
		$objMessage->setEftDatetime( date( 'Y-m-d H:i:s' ) );
		$objMessage->setReceiptDatetime( date( 'Y-m-d H:i:s' ) );
		$objMessage->setAmount( $this->getPaymentAmount() * 100 );
		$objMessage->setTotalAmount( $this->getPaymentAmount() * 100 );
		$objMessage->setEftMemo( $this->getPaymentMemo() );
		$objMessage->setIsTesting( 0 );
		$objMessage->setRemotePaymentNumber( $this->getId() );
		// client bank/cc data
		$objMessage->setPullBilltoCompanyName( $objBankAccount->getCompanyName() );
		$objMessage->setPullBilltoNameFirst( $objBankAccount->getCcFirstName() );
		$objMessage->setPullBilltoNameLast( $objBankAccount->getCcLastName() );
		$objMessage->setPullBilltoStreetLine1( $objBankAccount->getPayerStreetLine1() );
		$objMessage->setPullBilltoStreetLine2( $objBankAccount->getPayerStreetLine2() );
		$objMessage->setPullBilltoCity( $objBankAccount->getPayerCity() );
		$objMessage->setPullBilltoStateCode( $objBankAccount->getPayerStateCode() );
		$objMessage->setPullBilltoPostalCode( $objBankAccount->getPayerPostalCode() );
		$objMessage->setPullBilltoCountryCode( CCountry::CODE_USA );
		$objMessage->setPullBilltoPhoneNumber( $objBankAccount->getPayerPhoneNumber() );
		$objMessage->setPullPaymentTypeId( CCardType::$c_arrintCardTypeToPaymentTypeId[$objBankAccount->getCardTypeId()] );
		$objMessage->setPullCcCardNumberEncrypted( $objBankAccount->getCcCardNumberEncrypted() );
		$objMessage->setPullCcExpDateMonth( $objBankAccount->getCcExpDateMonth() );
		$objMessage->setPullCcExpDateYear( $objBankAccount->getCcExpDateYear() );
		$objMessage->setPullCcNameOnCard( $objBankAccount->getCcFirstName() . ' ' . $objBankAccount->getCcLastName() );
		$objMessage->setPullSecureReferenceNumber( $objBankAccount->getSecureReferenceNumber() );
		// resident remittance data
		$objMessage->setPushBilltoNameFirst( $objApRemittance->getCcFirstName() );
		$objMessage->setPushBilltoNameLast( $objApRemittance->getCcLastName() );
		$objMessage->setPushBilltoStreetLine1( $objApRemittance->getStreetLine1() );
		$objMessage->setPushBilltoStreetLine2( $objApRemittance->getStreetLine2() );
		$objMessage->setPushBilltoCity( $objApRemittance->getCity() );
		$objMessage->setPushBilltoStateCode( $objApRemittance->getStateCode() );
		$objMessage->setPushBilltoPostalCode( $objApRemittance->getPostalCode() );
		$objMessage->setPushBilltoCountryCode( CCountry::CODE_USA );
		$objMessage->setPushPaymentTypeId( $objApRemittance->getDebitCardPaymentTypeId() );
		$objMessage->setPushCcCardNumberEncrypted( $objApRemittance->getCcCardNumberEncrypted() );
		$objMessage->setPushCcExpDateMonth( $objApRemittance->getCcExpDateMonth() );
		$objMessage->setPushCcExpDateYear( $objApRemittance->getCcExpDateYear() );
		$objMessage->setPushCcNameOnCard( $objApRemittance->getCcFirstName() . ' ' . $objApRemittance->getCcLastName() );
		$objMessage->setPushSecureReferenceNumber( $objApRemittance->getSecureReferenceNumber() );

		$objAmqpMessageFactory = \Psi\Libraries\Queue\Factory\CAmqpMessageFactory::createService();
		$arrmixResponse = [];

		$this->setPaymentStatusTypeId( CPaymentStatusType::CAPTURING );

		if( false == $this->logHistory( CApPaymentLog::ACTION_TYPE_CAPTURING, $intCompanyUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$objClientDatabase->rollback();
			return;
		}

		if( false == $this->update( $intCompanyUserId, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to update AP Payment.  ID' . $this->getId() ) );
			return false;
		}

		try {
			$objSender = \Psi\Libraries\Queue\Factory\CMessageSenderFactory::createService()->createSimpleSender( $objAmqpMessageFactory, $objMessage::PAYMENTS_RPC_OPERATION_EFT, [], NULL, \Psi\Libraries\Queue\CAmqpConnection::PAYMENTS_VHOST );

			/*******As discussed with Ben wrapping the message sending in if condition so that it can execute on production environment only as there are no payment consumers running in dev env for now.*********/
			if( $_objException->getEnvironment() == 'production' ) {
				$objSender->sendRpc( $objMessage, [], 90 );
				$strJsonResponse = $objSender->getResponse();
				$arrmixResponse = json_decode( $strJsonResponse, true );

				if( true == array_key_exists( 'operation_status', $arrmixResponse ) && true == CStrings::strToBool( $arrmixResponse['operation_status'] ) ) {
					$this->setPaymentStatusTypeId( CPaymentStatusType::CAPTURED );
					if( false == $this->validate( VALIDATE_UPDATE ) || false == $this->update( $intCompanyUserId, $objClientDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to validate AP Payment for updating but payment has been captured.  ID' . $this->getId() ) );
						return false;
					}
					return true;
				}
			}
		} catch( Exception $objException ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $objException->getMessage() ) );
		}

		if( true == array_key_exists( 'error_msgs', $arrmixResponse ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $arrmixResponse['error_msgs'] ) );
		}

		$objClientDatabase->rollback();
		return false;
	}

	/**
	 * @param CApHeader $objApHeader
	 * @param CApDetail[] $arrobjApDetails
	 * @param array $arrmixInvoiceApDetails
	 * @param CBankAccount $objBankAccount
	 * @param CApRemittance $objApRemittance
	 * @param CApPayeeAccount[] $arrmixApPayeeAccount
	 * @param int $intCompanyUserId
	 * @param CDatabase $objClientDatabase
	 * @param string $strDontPrintUntilDays
	 *
	 * @return bool Returns true if ap payment was successfully added to the queue
	 */
	public function addApPaymentInBillPayQueue( $objApHeader, $arrobjApDetails, $arrmixInvoiceApDetails, $objBankAccount, $objApRemittance, $arrmixApPayeeAccount, $intCompanyUserId, $objClientDatabase, $strDontPrintUntilDays = NULL, $boolAutoDeterminePaymentTypeId = true ) {

		if( false == valArr( $arrobjApDetails ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid parameter. Missing $arrobjApDetails.' ) );
			return false;
		}

		$objMerchantAccount = CMerchantAccounts::fetchAccountsPayableMerchantAccountByCid( $objApHeader->getCid(), $objClientDatabase );

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to load AP merchant account for cid ' . $objApHeader->getCid() ) );
			return false;
		}

		$intApPaymentTypeId = $this->getApPaymentTypeId();
		if( true == $boolAutoDeterminePaymentTypeId ) {
			$intApPaymentTypeId = $this->determineBillPayRemittanceType( $objMerchantAccount, $objBankAccount, $objApRemittance, $intCompanyUserId, $objClientDatabase );
			$this->setApPaymentTypeId( $intApPaymentTypeId );
		}

		$objMessage = ( new \Psi\Libraries\PaymentQueue\Factory\CPaymentMessageFactory() )->createPostApPaymentMessage();
		$objAmqpMessageFactory = new \Psi\Libraries\Queue\Factory\CAmqpMessageFactory();

		if( CApPaymentType::BILL_PAY_ACH == $intApPaymentTypeId ) {
			$objMessageSender = ( new \Psi\Libraries\Queue\Factory\CMessageSenderFactory() )->createSimpleSender( $objAmqpMessageFactory, Psi\Libraries\PaymentQueue\Message\Payments\Operations\CProcess::PAYMENTS_RPC_OPERATION_EFT, [], NULL, \Psi\Libraries\Queue\CAmqpConnection::PAYMENTS_VHOST );
		} else {
			$objMessageSender = ( new \Psi\Libraries\Queue\Factory\CMessageSenderFactory() )->createSimpleSender( $objAmqpMessageFactory, Psi\Libraries\PaymentQueue\Message\Payments\Operations\CProcess::PAYMENTS_SAGA_CHECK_PRINTING );
		}

		$arrobjFiles = rekeyObjects( 'CheckComponentTypeId', ( array ) $objBankAccount->fetchFiles( $objClientDatabase ) );

		$objMessage->setCid( $this->getCid() );
		$objMessage->setApPayeeId( $objApHeader->getApPayeeId() );
		$objMessage->setApPaymentId( $this->getId() );
		$objMessage->setProcessingBankAccountId( $this->getBankAccountId() );
		$objMessage->setReceiptDatetime( $this->getPaymentDate() );
		$objMessage->setTotalAmount( $this->getPaymentAmount() );
		$objMessage->setCompanyChargeAmount( $objMerchantAccount->getApCheckFee() * 100 );
		$objMessage->setPayeeAccountNumber( $arrmixApPayeeAccount['account_number'] );
		$objMessage->setCheckNumber( $this->getPaymentNumber() );
		$objMessage->setBankName( $objBankAccount->getCheckBankName() );
		$objMessage->setPayerCheckRoutingNumber( $objBankAccount->getCheckRoutingNumber() );
		$objMessage->setBankFractionalRoutingNumber( $objBankAccount->getCheckFractionalRoutingNumber() );
		$objMessage->setPayerCheckAccountNumberEncrypted( $objBankAccount->getCheckAccountNumberEncrypted() );
		$objMessage->setPayerCheckNameOnAccount( $objBankAccount->getCheckNameOnAccount() );
		$objMessage->setPayeeCheckNameOnAccount( $objApRemittance->getCheckNameOnAccount() );
		$objMessage->setMerchantGatewayId( CMerchantGateway::ZIONS_ACH );
		$objMessage->setPaymentStatusTypeId( CPaymentStatusType::CAPTURING );
		$objMessage->setCompanyMerchantAccountId( $objMerchantAccount->getId() );
		$objMessage->setPayerAddressLine1( $objBankAccount->getPayerStreetLine1() );
		$objMessage->setPayerAddressLine2( $objBankAccount->getPayerStreetLine2() );
		$objMessage->setPayerCity( $objBankAccount->getPayerCity() );
		$objMessage->setPayerState( $objBankAccount->getPayerStateCode() );
		$objMessage->setPayerZip( $objBankAccount->getPayerPostalCode() );
		$objMessage->setPayeeAddressLine1( $objApRemittance->getStreetLine1() );
		$objMessage->setPayeeAddressLine2( $objApRemittance->getStreetLine2() );
		$objMessage->setPayeeCity( $objApRemittance->getCity() );
		$objMessage->setPayeeState( $objApRemittance->getStateCode() );
		$objMessage->setPayeeZip( $objApRemittance->getPostalCode() );
		$objMessage->setSignerName( $objBankAccount->getCheckNameOnAccount() );
		$objMessage->setCapturedDatetime( date( 'm/d/Y H:i:s' ) );
		$objMessage->setCreatedOn( date( 'm/d/Y H:i:s' ) );

		if( true == array_key_exists( CCheckComponentType::SIGNATURE_LINE_ONE, $arrobjFiles ) ) {

			$objSignatureFile = $arrobjFiles[CCheckComponentType::SIGNATURE_LINE_ONE];

			if( false == is_null( $objBankAccount->getSecondSignatureLimitAmount() ) && $this->getPaymentAmount() >= $objBankAccount->getSecondSignatureLimitAmount() ) {
				$objSignatureFile = $arrobjFiles[CCheckComponentType::SIGNATURE_LINE_TWO];
			}

			$strFullpath = getMountsPath( $this->getCid(), PATH_MOUNTS_FILES ) . $objSignatureFile->getFilePath() . $objSignatureFile->getFileName();

			if( true == file_exists( $strFullpath ) ) {
				$objMessage->setSignerImage( $objSignatureFile->getFilePath() . $objSignatureFile->getFileName() );
			}
		}

		$objMessage->setMemo( $this->getPaymentMemo() );

		if( false == is_null( $objMerchantAccount->getApPrintServiceLevelAgreement() ) ) {
			$objMessage->setExpectedPrintDate( date( 'Y-m-d', strtotime( ' + ' . $objMerchantAccount->getApPrintServiceLevelAgreement() . ' days' ) ) );
		}

		// Logic for don't print until date.

		if( false == is_null( $strDontPrintUntilDays ) ) {
			$objMessage->setDontPrintUntil( date( 'Y-m-d', strtotime( ' + ' . $strDontPrintUntilDays . ' days' ) ) );
		}

		// set ap details
		$arrmixRemittanceDetails = [];

		foreach( $arrobjApDetails as $objApDetail ) {
            $arrmixRemittanceDetails[$objApDetail->getId()]['property_name']   = $objApDetail->getPropertyName();
            $arrmixRemittanceDetails[$objApDetail->getId()]['property_id']	   = $objApDetail->getPropertyId();
            $arrmixRemittanceDetails[$objApDetail->getId()]['invoice_date']    = $objApHeader->getPostDate();
            $arrmixRemittanceDetails[$objApDetail->getId()]['invoice_number']  = $objApHeader->getHeaderNumber();
            $arrmixRemittanceDetails[$objApDetail->getId()]['account_number']  = $objApDetail->getGlAccountNumber();

        	if( true == valArr( $arrmixInvoiceApDetails ) ) {
            	$arrmixRemittanceDetails[$objApDetail->getId()]['amount_due'] = bcadd( $arrmixInvoiceApDetails[$objApDetail->getPropertyId()]['transaction_amount_due'], abs( $objApDetail->getTransactionAmount() ), 2 );
            } else {
            	$arrmixRemittanceDetails[$objApDetail->getId()]['amount_due'] = $objApDetail->getTransactionAmountDue();
            }

            $arrmixRemittanceDetails[$objApDetail->getId()]['amount_paid']     = $objApDetail->getTransactionAmount();
            $arrmixRemittanceDetails[$objApDetail->getId()]['description']     = $objApDetail->getDescription();
        }

		$objMessage->setRemittanceDetails( $arrmixRemittanceDetails );
		$objMessage->setUserId( $intCompanyUserId );

		if( CApPaymentType::BILL_PAY_ACH == $intApPaymentTypeId ) {
			// We need to strip out the C and D characters out of the account number
			// The AP system allows the user to specify where the On Us (C) and Dash (D) characters can show up on the
			// check
			$strCheckAccountNumber = $objBankAccount->getCheckAccountNumberEncryptedWithoutSymbols();
			$objMessage->setPayerCheckAccountNumberEncrypted( $strCheckAccountNumber );
			$objMessage->setTotalAmount( $this->getPaymentAmount() * 100 ); // We need to remove the decimal
			$objMessage->setCompanyChargeAmount( $objMerchantAccount->getApPaymentFee() * 100 );
			$objMessage->setRemotePaymentNumber( $this->getId() );
			$objMessage->setServiceClientId( 3 );
			$objMessage->setPayeeCheckRoutingNumber( $objApRemittance->getCheckRoutingNumber() );
			$objMessage->setPayeeCheckAccountNumberEncrypted( $objApRemittance->getCheckAccountNumberEncrypted() );
			$objMessageSender->sendRpc( $objMessage, [], 30 );
			$arrstrResponses = json_decode( $objMessageSender->getResponse(), true );

			if( true == array_key_exists( 'operation_status', $arrstrResponses ) && true == CStrings::strToBool( $arrstrResponses['operation_status'] ) ) {
				$this->setPaymentStatusTypeId( CPaymentStatusType::CAPTURED );
				if( false == $this->validate( VALIDATE_UPDATE ) || false == $this->update( $intCompanyUserId, $objClientDatabase ) ) {
					$this->setPaymentStatusTypeId( CPaymentStatusType::CAPTURING );
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to validate AP Payment for updating.  ID' . $this->getId() ) );
					return false;
				}

				return true;
			}

			// Failed to send via ACH, so fall back to check

			$objMessageSender = ( new \Psi\Libraries\Queue\Factory\CMessageSenderFactory() )->createSimpleSender( $objAmqpMessageFactory, Psi\Libraries\PaymentQueue\Message\Payments\Operations\CProcess::PAYMENTS_SAGA_CHECK_PRINTING );
			$this->setApPaymentTypeId( CApPaymentType::BILL_PAY_CHECK );

			// Set the amount to the original (with the decimal)
			$objMessage->setTotalAmount( $this->getPaymentAmount() );
			$objMessage->setCompanyChargeAmount( $objMerchantAccount->getApCheckFee() * 100 );
		}

		$this->setPaymentStatusTypeId( CPaymentStatusType::CAPTURING );

		if( false == $this->logHistory( CApPaymentLog::ACTION_TYPE_CAPTURING, $intCompanyUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$objClientDatabase->rollback();
			return;
		}

		$objMessageSender->send( $objMessage );

		if( false == $this->update( $intCompanyUserId, $objClientDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to update AP Payment.  ID' . $this->getId() ) );
			return false;
		}

		return true;
	}

	public static function fetchApPaymentDailyInvoiceAmountsByCid( $intCid, $objDatabase ) {

		$objDateTimeStart = new DateTime( date( 'm/d/Y' ) ); // get current date minus 4 months since this month is most likely not complete
		$objDateTimeStart->modify( '- 3 months' );
		$objDateTimeStart->modify( 'first day of this month' ); // we want the first day of this month
		// now get three months worth of time. as the end date
		$objDateTimeEnd = new DateTime( $objDateTimeStart->format( 'm/d/Y' ) );
		$objDateTimeEnd->modify( '+ 2 months' );// +2 months
		$objDateTimeEnd->modify( 'last day of this month' ); // we want the Last day of this month == +1 more month

		$strSql = 'SELECT
		  table1.max_daily_sum,
		  table2.*,
		  table3.*
		FROM
		  (
			 SELECT
				 payment_date::DATE AS payment_date,
				 sum ( payment_amount ) AS max_daily_sum
			 FROM
				 ap_payments
			 WHERE
				 cid::Integer = ' . ( int ) $intCid . '
				 AND payment_date::DATE >= \'' . $objDateTimeStart->format( 'm/d/Y' ) . '\'
				 AND payment_date::DATE <= \'' . $objDateTimeEnd->format( 'm/d/Y' ) . '\'
				 AND is_reversed = 0
			 GROUP BY
				 payment_date
			 ORDER BY
				 max_daily_sum DESC
			 LIMIT 1
		  ) as table1,
		  (
			 SELECT
				 sum ( payment_amount ) / count ( DISTINCT ( extract ( month FROM payment_date ) ) ) AS monthly_average
			 FROM
				 ap_payments
			 WHERE
				 cid::Integer = ' . ( int ) $intCid . '
				 AND payment_date::DATE >= \'' . $objDateTimeStart->format( 'm/d/Y' ) . '\'
				 AND payment_date::DATE <= \'' . $objDateTimeEnd->format( 'm/d/Y' ) . '\'
				 AND is_reversed = 0
		  ) as table2,
		  (
		  SELECT
			  sum( temp_table.date_1_to_7 ) / ( 7 * count( DISTINCT( temp_table.payment_month ) ) ) as average_week_1_1st_to_7th,
			  sum( temp_table.date_8_to_14 ) / ( 7 * count( DISTINCT( temp_table.payment_month ) ) ) as average_week_2_8th_to_14th,
			  sum( temp_table.date_15_to_21 ) / ( 7 * count( DISTINCT( temp_table.payment_month ) ) ) as average_week_3_15th_to_21st,
			  sum( temp_table.date_22_to_eom ) / ( ( \'' . $objDateTimeEnd->format( 'm/d/Y' ) . '\'::date - \'' . $objDateTimeStart->format( 'm/d/Y' ) . '\'::date ) - ( 21 * count( DISTINCT( temp_table.payment_month ) ) ) ) as average_week_4_22nd_to_EOM
		  FROM
		  (
			SELECT
				CASE
				  WHEN extract ( DAY FROM payment_date ) BETWEEN 1 AND 7 THEN payment_amount
				  ELSE 0
				END AS date_1_to_7,
				CASE
				  WHEN extract ( DAY FROM payment_date ) BETWEEN 8 AND 14 THEN payment_amount
				  ELSE 0
				END AS date_8_to_14,
				CASE
				  WHEN extract ( DAY FROM payment_date ) BETWEEN 15 AND 21 THEN payment_amount
				  ELSE 0
				END AS date_15_to_21,
				CASE
				  WHEN extract ( DAY FROM payment_date ) >= 22 THEN payment_amount
				  ELSE 0
				END AS date_22_to_eom,
				extract ( month FROM payment_date ) as payment_month
			FROM
				ap_payments
			WHERE
				cid::Integer = ' . ( int ) $intCid . '
				AND payment_date::DATE >= \'' . $objDateTimeStart->format( 'm/d/Y' ) . '\'
				AND payment_date::DATE <= \'' . $objDateTimeEnd->format( 'm/d/Y' ) . '\'
				AND is_reversed = 0
		  ) as temp_table
		) as table3';

		return fetchData( $strSql, $objDatabase );
	}

	public function getPaymentSubmittedContent() {
		$arrintBankAccountFormats = [];
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( false == valArrKeyExists( $arrmixRequiredParameters, 'ap_payment_ids' ) ) {
			return NULL;
		}

		$arrobjInvoiceApDetails       = [];

		$arrintApPaymentsIds   = $arrmixRequiredParameters['ap_payment_ids'];
		$arrstrApPaymentTypes  = CApPaymentType::loadApPaymentTypes();
		$arrstrAllPaymentTypes = CApPaymentType::$c_arrstrAllApPaymentTypes;

		$arrobjApPayments     = ( array ) \Psi\Eos\Entrata\CApPayments::createService()->fetchApPaymentsByIdsByCid( $arrintApPaymentsIds, $this->getCid(), $this->m_objDatabase );
		$arrintRemittanceIds  = array_filter( array_keys( rekeyObjects( 'apRemittanceId', $arrobjApPayments ) ) );
		$arrintBankAccountIds = array_filter( array_keys( rekeyObjects( 'bankAccountId', $arrobjApPayments ) ) );
		$arrobjApRemittances  = ( array ) \Psi\Eos\Entrata\CApRemittances::createService()->fetchApRemittancesByIdsByCid( $arrintRemittanceIds, true, $this->getCid(), $this->m_objDatabase );
		$arrobjApDetails      = ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchInvoiceApDetailsByApPaymentIdsByCid( $arrintApPaymentsIds, $this->getCid(), $this->m_objDatabase );
		$arrobjBankAccounts   = ( array ) \Psi\Eos\Entrata\CBankAccounts::createService()->fetchBankAccountsByIdsByCid( $arrintBankAccountIds, $this->getCid(), $this->m_objDatabase );

		$arrintCheckTemplateIds			= array_filter( ( array ) extractObjectKeyValuePairs( $arrobjBankAccounts, 'check_template_id', 'check_template_id' ) );
		$arrobjCheckComponents			= ( array ) \Psi\Eos\Entrata\CCheckComponents::createService()->fetchVisibleCheckComponentsByCheckTemplateIdsByCid( $arrintCheckTemplateIds, $this->getCid(), $this->m_objDatabase );

		foreach( $arrobjBankAccounts as $objBankAccount ) {
			foreach( $arrobjCheckComponents as $objCheckComponent ) {

				switch( $objCheckComponent->getCheckComponentTypeId() ) {
					case CCheckComponentType::DATE:
					case CCheckComponentType::DATE_FORMAT_2:
					case CCheckComponentType::DATE_FORMAT_3:
					case CCheckComponentType::DATE_FORMAT_4:
					case CCheckComponentType::DATE_FORMAT_5:
					case CCheckComponentType::DATE_FORMAT_6:
					case CCheckComponentType::DATE_FORMAT_7:
					case CCheckComponentType::DATE_FORMAT_8:
					case CCheckComponentType::DATE_FORMAT_9:
						$arrintBankAccountFormats[$objBankAccount->getId()] = $objCheckComponent->getCheckComponentTypeId();
						break;

					default:
				}
			}
		}

		$arrobjInvoiceApDetails = rekeyObjects( 'ApPaymentId', $arrobjApDetails, true );

		$strHtmlContent = '';
		$strDate = '';
		$strTrCss = '<tr style = "text-transform: uppercase;  font-size:16px; text-align:left;" >';
		$strTdCss = '<td style = "font-weight:400; padding:10px; text-align:right; border-top:1px solid #CCC;  color: #CCC; width:50%;  min-width:200px; " > ';

		foreach( $arrobjApPayments as $objApPayment ) {
			$strRemittanceType = $arrstrApPaymentTypes[$objApPayment->getApPaymentTypeId()];
			if( true == valArr( $arrobjApRemittances ) && CApPaymentType::WFPM == $objApPayment->getApPaymentTypeId() ) {
				$strRemittanceType .= ' - ' . $arrstrAllPaymentTypes[$arrobjApRemittances[$objApPayment->getApRemittanceId()]->getApPaymentTypeId()];
			}

			switch( $arrintBankAccountFormats[$objApPayment->getBankAccountId()] ) {
				case CCheckComponentType::DATE:
				case CCheckComponentType::DATE_FORMAT_2:
				case CCheckComponentType::DATE_FORMAT_3:
				case CCheckComponentType::DATE_FORMAT_4:
				case CCheckComponentType::DATE_FORMAT_5:
				case CCheckComponentType::DATE_FORMAT_6:
				case CCheckComponentType::DATE_FORMAT_7:
				case CCheckComponentType::DATE_FORMAT_8:
				case CCheckComponentType::DATE_FORMAT_9:
					$strDate = date( CCheckComponentType::$c_strDateFormats[$arrintBankAccountFormats[$objApPayment->getBankAccountId()]], strtotime( $objApPayment->getPaymentDate() ) );
					break;

				default:
				$strDate = date( 'm-d-Y', strtotime( $objApPayment->getPaymentDate() ) );

			}

			$arrstrPropertyNames	= array_keys( rekeyObjects( 'propertyName', ( array ) $arrobjInvoiceApDetails[$objApPayment->getId()] ) );
			$strApPaymentAmount     = __( '{%m, 0, p:2; notes}', [ $objApPayment->getPaymentAmount() ] );
			$strApPaymentDate       = __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $objApPayment->getPaymentDate() ] );
			$arrstrHtmlContent      = [ __( 'Payment #' ) => $objApPayment->getPaymentNumber(), __( 'Property' ) => array_unique( $arrstrPropertyNames ), __( 'Type' ) => $strRemittanceType, __( 'Amount' ) => $strApPaymentAmount, __( 'Date' ) => $strDate ];

			if( CApPaymentType::FAST_FUNDS == $objApPayment->getApPaymentTypeId() ) {
				$strAccountNumber         = CEncryption::decryptText( $arrobjBankAccounts[$objApPayment->getBankAccountId()]->getCcCardNumberEncrypted(), CONFIG_KEY_CC_CARD_NUMBER );
				$strAccountNumberLastFour = substr( preg_replace( '/([^a-zA-Z0-9]*)/', '', $strAccountNumber ), -4 );
				$arrstrHtmlContent        = [ __( 'Property' ) => array_unique( $arrstrPropertyNames ), __( 'Type' ) => __( 'DEBIT CARD' ), __( 'Account' ) => $strAccountNumberLastFour, __( 'Amount' ) => $strApPaymentAmount, __( 'Date' ) => $strApPaymentDate, __( 'Description' ) => __( 'Resident Refund' ) ];
			}

			$strHtmlContent .= '<table  width="100%" cellspacing="0" cellpadding="0" style=" font-size:12px; font-family: Montserrat, sans-serif; border-bottom: 2px solid #ccc">
								<tbody>';

			foreach( $arrstrHtmlContent as $strKey => $strValue ) {
				$strKeyDetails = $strTrCss . $strTdCss . $strKey . '  </td >';
				$strContent    = '<td style = "padding:5px; border-top:1px solid #CCC; text-align:left; " > ' . $strValue . ' </td> </tr>';

				if( 'Property' == $strKey ) {
					$intCounter = 0;
					$strPropertyNameHtml = '';
					foreach( $strValue as $strPropertyName ) {
						$strPropertyNameDisplay = $strTrCss . '<td> </td>
												<td style = "padding:5px; text-align:left; " >  ' . $strPropertyName . ' </td>
											</tr>';
						if( 0 == $intCounter ) {
							$strPropertyNameDisplay = '<td style = "padding:5px; border-top:1px solid #CCC; text-align:left; " > ' . $strPropertyName . ' </td>';
						}
						$strPropertyNameHtml .= $strPropertyNameDisplay . '<p style = "display:none;" ></p>';
						$intCounter++;
					}
					$strContent = $strPropertyNameHtml . '</tr>';
				}

				$strHtmlContent .= $strKeyDetails . $strContent;
			}
			$strHtmlContent .= '</tbody> </table>';
		}

		return $strHtmlContent;
	}

}
?>
