<?php

class CCompanyUserDashboardItem extends CBaseCompanyUserDashboardItem {

	public function valId() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getCid() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getCompanyUserId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valDashboardItemId() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getDashboardItemId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dashboard_item_id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getIsPublished() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
		// }

		return $boolIsValid;
	}

	public function valRowNum() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getRowNum() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'row_num', '' ) );
		// }

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getOrderNum() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getUpdatedBy() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getUpdatedOn() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getCreatedBy() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
		// }

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		/**
		 * Validation example
		 */

		// if( true == is_null( $this->getCreatedOn() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
		// }

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>