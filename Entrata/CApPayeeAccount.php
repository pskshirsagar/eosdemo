<?php

class CApPayeeAccount extends CBaseApPayeeAccount {

	protected $m_boolIsUtilities;
	protected $m_boolIsInvoiceProcessing;
	protected $m_boolIsGlAccountChanged;

	protected $m_intStoreId;
	protected $m_intApLegalEntityId;
	protected $m_intApPaymentTypeId;
	protected $m_intApRemittanceId;

	protected $m_strPostalCode;
	protected $m_strLocationName;
	protected $m_strPropertyName;
	protected $m_strRemittanceName;

	/** @var  \CApPayeeSubAccount[] */
	protected $m_arrobjApPayeeSubAccounts;
	protected $m_arrmixApPayeeAccountEvent;

	/**
	 * Get Functions
	 */

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function getRemittanceName() {
		return $this->m_strRemittanceName;
	}

	public function getApPaymentTypeId() {
		return $this->m_intApPaymentTypeId;
	}

	public function setApPaymentTypeId( $intApPaymentTypeId ) {
		$this->m_intApPaymentTypeId = $intApPaymentTypeId;
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function getStoreId() {
		return $this->m_intStoreId;
	}

	public function getIsUtilities() {
		return $this->m_boolIsUtilities;
	}

	public function getApPayeeSubAccounts() {
		return $this->m_arrobjApPayeeSubAccounts;
	}

	public function getApPayeeAccountEvent() {
		return $this->m_arrmixApPayeeAccountEvent;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getInvoiceProcessing() {
		return $this->m_boolIsInvoiceProcessing;
	}

	public function getIsGlAccountChanged() {
		return $this->m_boolIsGlAccountChanged;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	/**
	 * Set Functions
	 */

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = $strLocationName;
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->m_intApLegalEntityId = $intApLegalEntityId;
	}

	public function setStoreId( $intStoreId ) {
		$this->m_intStoreId = $intStoreId;
	}

	public function setRemittanceName( $strRemittanceName ) {
		$this->m_strRemittanceName = $strRemittanceName;
	}

	public function setIsUtilities( $boolIsUtilities ) {
		$this->m_boolIsUtilities = CStrings::strToBool( $boolIsUtilities );
	}

	public function setInvoiceProcessing( $boolIsInvoiceProcessing ) {
		$this->m_boolIsInvoiceProcessing = $boolIsInvoiceProcessing;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$strAccountNumber = str_replace( ' ', '', $strAccountNumber );
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 50, NULL, true ) );
	}

	public function setIsGlAccountChanged( $boolIsGlAccountChanged ) {
		$this->m_boolIsGlAccountChanged = ( bool ) $boolIsGlAccountChanged;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrmixValues['ap_legal_entity_id'] );
		if( true == isset( $arrmixValues['location_name'] ) ) $this->setLocationName( $arrmixValues['location_name'] );
		if( true == isset( $arrmixValues['is_utilities'] ) ) $this->setIsUtilities( $arrmixValues['is_utilities'] );
		if( true == isset( $arrmixValues['store_id'] ) ) {
			$this->setStoreId( $arrmixValues['store_id'] );
		}
		if( true == isset( $arrmixValues['remittance_name'] ) ) $this->setRemittanceName( $arrmixValues['remittance_name'] );
		if( true == isset( $arrmixValues['ap_payment_type_id'] ) ) $this->setApPaymentTypeId( $arrmixValues['ap_payment_type_id'] );

		if( true == isset( $arrmixValues['ap_remittance_id'] ) ) $this->setApRemittanceId( $arrmixValues['ap_remittance_id'] );
	}

	public function setApRemittanceId( $intApRemittanceId ) {
		$this->set( 'm_intApRemittanceId', CStrings::strToIntDef( $intApRemittanceId, NULL, false ) );
	}

	public function getApRemittanceId() {
		return $this->m_intApRemittanceId;
	}

	public function sqlApRemittanceId() {
		return ( true == isset( $this->m_intApRemittanceId ) ) ? ( string ) $this->m_intApRemittanceId : 'NULL';
	}

	/**
	 * Other Functions
	 */

	public function valAccountNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number', ' Account number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valApRemittanceId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getDefaultApRemittanceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_ap_remittance_id', __( ' Default Remittance is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApPayeeLocationId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getApPayeeLocationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', 'Location is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUtilityBillAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewApPayeeAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyAssociations( $objCompanyUser, $objClientDatabase ) {
		$boolIsValid = true;

		if( true == valId( $this->getApPayeeLocationId() ) && true == valId( $this->getDefaultPropertyId() ) ) {
			$arrobjApPayeeProperties = CApPayeePropertyGroups::fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdByPropertyIdByCid( $this->getApPayeeId(), $this->getApPayeeLocationId(), $this->getDefaultPropertyId(), $this->getCid(), $objCompanyUser->getId(), $objClientDatabase );

			if( false == valArr( $arrobjApPayeeProperties ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_property_id', 'Either property ' . $this->getPropertyName() . ' is not associated to selected location ' . $this->getLocationName() . ' or user does not have permission for this property.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objCompanyUser = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAccountNumber();
				$boolIsValid &= $this->valApRemittanceId();
				$boolIsValid &= $this->valApPayeeLocationId();
				break;

			case VALIDATE_DELETE:
			case 'validate_owner':
				break;

			case 'validate_property_association':
				$boolIsValid &= $this->valPropertyAssociations( $objCompanyUser, $objClientDatabase );
				break;

			case 'vendor_insert':
				$boolIsValid &= $this->valAccountNumber();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function createApPayeeSubAccount() {

		$objApPayeeSubAccount = new CApPayeeSubAccount();
		$objApPayeeSubAccount->setCid( $this->getCid() );
		$objApPayeeSubAccount->setApPayeeId( $this->getApPayeeId() );
		$objApPayeeSubAccount->setApPayeeLocationId( $this->getApPayeeLocationId() );

		return $objApPayeeSubAccount;
	}

	public function addApPayeeSubAccount( $objApPayeeSubAccount ) {
		$this->m_arrobjApPayeeSubAccounts[] = $objApPayeeSubAccount;
	}

	public function addApPayeeAccountEvent( $arrmixEvent ) {
		$this->m_arrmixApPayeeAccountEvent = $arrmixEvent;
	}

}
?>