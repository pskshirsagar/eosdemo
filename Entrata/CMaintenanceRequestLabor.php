<?php

class CMaintenanceRequestLabor extends CBaseMaintenanceRequestLabor {

	protected $m_strStartTimeAmpm;
	protected $m_strEndTimeAmpm;
	protected $m_strEmployeeName;
	protected $m_fltTotalCharge;
	protected $m_fltTotalHour;
	protected $m_strLaborStartDate;
	protected $m_strLaborEndDate;
	protected $m_strLaborStartTime;
	protected $m_strLaborEndTime;
	public $m_strPropertyTimeZone;
	/**
	 * Get Functions
	 */

	public function getStartTimeAmpm() {
		return $this->m_strStartTimeAmpm;
	}

	public function getEndTimeAmpm() {
		return $this->m_strEndTimeAmpm;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	public function getTotalCharge() {
		return $this->m_fltTotalCharge;
	}

	public function getTotalHour() {
		return $this->m_fltTotalHour;
	}

	public function getLaborStartDate() {
		return $this->m_strLaborStartDate;
	}

	public function getLaborEndDate() {
		return $this->m_strLaborEndDate;
	}

	public function getLaborStartTime() {
		return $this->m_strLaborStartTime;
	}

	public function getLaborEndTime() {
		return $this->m_strLaborEndTime;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );
		if( isset( $arrmixValues['start_time_ampm'] ) && $boolDirectSet ) $this->m_strStartTimeAmpm = trim( $arrmixValues['start_time_ampm'] );
		else if( isset( $arrmixValues['start_time_ampm'] ) ) $this->setStartTimeAmpm( $arrmixValues['start_time_ampm'] );
		if( isset( $arrmixValues['end_time_ampm'] ) && $boolDirectSet ) $this->m_strEndTimeAmpm = trim( $arrmixValues['end_time_ampm'] );
		else if ( isset( $arrmixValues['end_time_ampm'] ) ) $this->setEndTimeAmpm( $arrmixValues['end_time_ampm'] );
		if( isset( $arrmixValues['employee_name'] ) && $boolDirectSet ) $this->m_strEmployeeName = trim( $arrmixValues['employee_name'] );
		else if( isset( $arrmixValues['employee_name'] ) ) $this->setEmployeeName( $arrmixValues['employee_name'] );
		if( isset( $arrmixValues['total_charge'] ) && $boolDirectSet ) $this->m_fltTotalCharge = trim( $arrmixValues['total_charge'] );
		else if( isset( $arrmixValues['total_charge'] ) ) $this->setTotalCharge( $arrmixValues['total_charge'] );
		if( isset( $arrmixValues['total_hour'] ) && $boolDirectSet ) $this->m_fltTotalHour = trim( $arrmixValues['total_hour'] );
		else if( isset( $arrmixValues['total_hour'] ) ) $this->setTotalHour( $arrmixValues['total_hour'] );
		if( isset( $arrmixValues['labor_start_date'] ) && $boolDirectSet ) $this->m_strLaborStartDate = trim( $arrmixValues['labor_start_date'] );
		else if( isset( $arrmixValues['labor_start_date'] ) ) $this->setLaborStartDate( $arrmixValues['labor_start_date'] );
		if( isset( $arrmixValues['labor_end_date'] ) && $boolDirectSet ) $this->m_strLaborEndDate = trim( $arrmixValues['labor_end_date'] );
		else if( isset( $arrmixValues['labor_end_date'] ) ) $this->setLaborEndDate( $arrmixValues['labor_end_date'] );
		if( isset( $arrmixValues['labor_start_time'] ) && $boolDirectSet ) $this->m_strLaborStartTime = trim( $arrmixValues['labor_start_time'] );
		else if( isset( $arrmixValues['labor_start_time'] ) ) $this->setLaborStartTime( $arrmixValues['labor_start_time'] );
		if( isset( $arrmixValues['labor_end_time'] ) && $boolDirectSet ) $this->m_strLaborEndTime = trim( $arrmixValues['labor_end_time'] );
		else if( isset( $arrmixValues['labor_end_time'] ) ) $this->setLaborEndTime( $arrmixValues['labor_end_time'] );
	}

	public function setStartTimeAmpm( $strStartTimeAmpm ) {
		$this->m_strStartTimeAmpm = CStrings::strTrimDef( $strStartTimeAmpm, -1, NULL, true );
	}

	public function setEndTimeAmpm( $strEndTimeAmpm ) {
		$this->m_strEndTimeAmpm = CStrings::strTrimDef( $strEndTimeAmpm, -1, NULL, true );
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = CStrings::strTrimDef( $strEmployeeName, -1, NULL, true );
	}

	public function setTotalCharge( $fltTotalCharge ) {
		$this->m_fltTotalCharge = CStrings::strTrimDef( $fltTotalCharge, -1, NULL, true );
	}

	public function setTotalHour( $fltTotalHour ) {
		$this->m_fltTotalHour = CStrings::strTrimDef( $fltTotalHour, -1, NULL, true );
	}

	public function setLaborStartDate( $strLaborStartDate ) {
		$this->m_strLaborStartDate = CStrings::strTrimDef( $strLaborStartDate, -1, NULL, true );
	}

	public function setLaborEndDate( $strLaborEndDate ) {
		$this->m_strLaborEndDate = CStrings::strTrimDef( $strLaborEndDate, -1, NULL, true );
	}

	public function setLaborStartTime( $strLaborStartTime ) {
		$this->m_strLaborStartTime = CStrings::strTrimDef( $strLaborStartTime, -1, NULL, true );
	}

	public function setLaborEndTime( $strLaborEndTime ) {
		$this->m_strLaborEndTime = CStrings::strTrimDef( $strLaborEndTime, -1, NULL, true );
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceRequestId() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;
        // It is required
        if( ( $this->m_intCompanyEmployeeId == NULL ) || ( $this->m_intCompanyEmployeeId < 1 ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'comapny_employee_id', __( ' Please Select company employee.' ) ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valServices() {
        $boolIsValid = true;
        if( true == is_null( $this->getServices() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'services', __( 'Description details are required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valLaborStartTime() {
        $boolIsValid = true;

        $intStartTimeHours = NULL;
        $intStartTimeMins = NULL;

        if( true == is_null( $this->m_strLaborStartTime ) || false == CValidation::validate12HourTimeWithoutSeconds( $this->m_strLaborStartTime ) ) {
        	$boolIsValid &= false;
		}

        $arrintMaintenanceRequestLaborStartTime = explode( ':', $this->m_strLaborStartTime );

        if( true == valArr( $arrintMaintenanceRequestLaborStartTime ) ) {
        	$intStartTimeHours = $arrintMaintenanceRequestLaborStartTime[0];

        	if( true == isset( $arrintMaintenanceRequestLaborStartTime[1] ) ) {
        		$intStartTimeMins = $arrintMaintenanceRequestLaborStartTime[1];
      		}
        }

        if( empty( $intStartTimeHours ) || empty( $intStartTimeMins ) || false == $boolIsValid ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'labor_start_time', __( ' Valid Start time required ( Example: {%t, 0, TIME_SHORT} , {%t, 1, TIME_SHORT} etc. ).', [ date( 'Y-m-d' ) . ' 12:10 AM', date( 'Y-m-d' ) . ' 10:35 PM' ] ) ) );
        }

        return $boolIsValid;
    }

    public function valLaborEndTime() {
        $boolIsValid = true;
        if( true == is_null( $this->m_strLaborEndTime ) || false == CValidation::validate12HourTimeWithoutSeconds( $this->m_strLaborEndTime ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'labor_end_time', __( ' Valid End time required ( Example: {%t, 0, TIME_SHORT} , {%t, 1, TIME_SHORT} etc. ).', [ date( 'Y-m-d' ) . ' 12:10 AM', date( 'Y-m-d' ) . ' 10:35 PM' ] ) ) );
        	return $boolIsValid;
        }

	    $strTimeStartPoint = new DateTime( $this->getLaborStartDatetime(), new DateTimeZone( $this->m_strPropertyTimeZone ) );
	    $strTimeEndPoint = new DateTime( $this->getLaborEndDatetime(), new DateTimeZone( $this->m_strPropertyTimeZone ) );
	    if( strtotime( $strTimeStartPoint->format( 'Y-m-d H:i:s' ) ) >= strtotime( $strTimeEndPoint->format( 'Y-m-d H:i:s' ) ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'labor_end_time', __( ' Invalid Time Range.( End date time must be later than start date time. )' ) ) );
        }
        return $boolIsValid;
    }

    public function valTotalLabor() {
    	$boolIsValid = true;

    	if( true == is_null( $this->m_strTotalLabor ) || ':' == trim( $this->m_strTotalLabor ) || 0 == ( int ) str_replace( ':', '', trim( $this->m_strTotalLabor ) ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_labor', __( ' Total time is required.' ) ) );
        }

        return $boolIsValid;

    }

    public function valMobileLaborStartTime() {
    	$boolIsValid = true;

    	$intStartTimeHours = NULL;
    	$intStartTimeMins = NULL;

    	if( true == is_null( $this->m_strLaborStartTime ) ) {
    		$boolIsValid &= false;
    	}

    	$arrintMaintenanceRequestLaborStartTime = explode( ':', $this->m_strLaborStartTime );

    	if( true == valArr( $arrintMaintenanceRequestLaborStartTime ) ) {
    		$intStartTimeHours = $arrintMaintenanceRequestLaborStartTime[0];

    		if( true == isset( $arrintMaintenanceRequestLaborStartTime[1] ) ) {
    			$intStartTimeMins = $arrintMaintenanceRequestLaborStartTime[1];
    		}
    	}

    	if( empty( $intStartTimeHours ) || empty( $intStartTimeMins ) || false == $boolIsValid ) {
    		$boolIsValid &= false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'labor_start_time', __( ' Valid Start time required ( Example: {%t, 0, TIME_SHORT} , {%t, 1, TIME_SHORT} etc. ).', [ date( 'Y-m-d' ) . ' 08:10AM', date( 'Y-m-d' ) . ' 05:35PM' ] ) ) );
    	}

    	return $boolIsValid;
    }

    public function valMobileLaborEndTime() {
    	$boolIsValid = true;

    	if( true == is_null( $this->m_strLaborEndTime ) ) {
    		$boolIsValid &= false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'labor_end_time', __( ' Valid End time required ( Example: {%t, 0, TIME_SHORT} , {%t, 1, TIME_SHORT} etc. ).', [ date( 'Y-m-d' ) . ' 12:10 AM', date( 'Y-m-d' ) . ' 10:35 PM' ] ) ) );
    		return $boolIsValid;
    	}

	    $strTimeStartPoint = new DateTime( $this->getLaborStartDatetime(), new DateTimeZone( $this->m_strPropertyTimeZone ) );
	    $strTimeEndPoint = new DateTime( $this->getLaborEndDatetime(), new DateTimeZone( $this->m_strPropertyTimeZone ) );
	    if( strtotime( $strTimeStartPoint->format( 'Y-m-d H:i:s' ) ) >= strtotime( $strTimeEndPoint->format( 'Y-m-d H:i:s' ) ) ) {
		    $boolIsValid &= false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'labor_end_time', __( ' Invalid Time Range.( End date time must be later than start date time. )' ) ) );
	    }
    	return $boolIsValid;
    }

    public function valPerHourRate() {
        $boolIsValid = true;
        if( true == is_null( $this->m_fltPerHourRate ) && false == is_numeric( $this->m_fltPerHourRate ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'per_hour_rate', __( ' Per Hour Rate required ( must be in digit ).' ) ) );
        }
        return $boolIsValid;
    }

    public function valMaintenanceRequestLaborTypeId() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valRateMultiplier() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL, $boolCustomValidate = false ) {
        $boolIsValid = true;

        // The variable is never used to setting it to NULL
        $objDatabase = NULL;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCompanyEmployeeId();
            	if( true == $boolCustomValidate ) {
            		$boolIsValid &= $this->valLaborStartTime();
            		$boolIsValid &= $this->valLaborEndTime();
            	} else {
		            $boolIsValid &= $this->valLaborStratEndDate();
	            }
            	$boolIsValid &= $this->valTotalLabor();

            	// $boolIsValid &= $this->valPerHourRate();
            	break;

            case 'VALIDATE_MOBILE_INSERT':
            	$boolIsValid &= $this->valCompanyEmployeeId();
            	if( true == $boolCustomValidate ) {
	            	$boolIsValid &= $this->valMobileLaborStartTime();
	            	$boolIsValid &= $this->valMobileLaborEndTime();
            	} else {
		            $boolIsValid &= $this->valLaborStratEndDate();
	            }

            	$boolIsValid &= $this->valTotalLabor();
            	// $boolIsValid &= $this->valPerHourRate();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valCompanyEmployeeId();
            	if( true == $boolCustomValidate ) {
            		$boolIsValid &= $this->valLaborStartTime();
            		$boolIsValid &= $this->valLaborEndTime();
            	} else {
		            $boolIsValid &= $this->valLaborStratEndDate();
	            }
            	$boolIsValid &= $this->valTotalLabor();
            	// $boolIsValid &= $this->valPerHourRate();
            	break;

           	case 'VALIDATE_MOBILE_UPDATE':
           		$boolIsValid &= $this->valId();
           		$boolIsValid &= $this->valCompanyEmployeeId();
           		if( true == $boolCustomValidate ) {
	           		$boolIsValid &= $this->valMobileLaborStartTime();
	           		$boolIsValid &= $this->valMobileLaborEndTime();
           		} else {
		            $boolIsValid &= $this->valLaborStratEndDate();
	            }

           		$boolIsValid &= $this->valTotalLabor();
           		// $boolIsValid &= $this->valPerHourRate();
           		break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

    public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	$this->setDeletedBy( $intUserId );
    	$this->setDeletedOn( ' NOW() ' );
    	$this->setUpdatedBy( $intUserId );
    	$this->setUpdatedOn( ' NOW() ' );

    	if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
    		return true;
    	}
    }

    public function validateTime( $strTime, $boolIsAlphabet = false ) {
    	if( false != $boolIsAlphabet ) {
    		if( false != preg_match( '/[A-Za-z]/', $strTime ) ) {
				return true;
    		}

		    return false;
	    }

	    if( false != preg_match( '/[^0-9:]/i', $strTime ) ) {
		    return true;
	    }

	    return false;

    }

	public function valLaborStratEndDate() {
		$boolIsValid = true;
		$strTimeStartPoint = new DateTime( $this->getLaborStartDatetime(), new DateTimeZone( $this->m_strPropertyTimeZone ) );
		$strTimeEndPoint = new DateTime( $this->getLaborEndDatetime(), new DateTimeZone( $this->m_strPropertyTimeZone ) );
		if( strtotime( $strTimeStartPoint->format( 'Y-m-d H:i:s' ) ) > strtotime( $strTimeEndPoint->format( 'Y-m-d H:i:s' ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'labor_end_time', __( ' Invalid Date Range.( End date must be later than start date. )' ) ) );
		}

		if( !is_null( $this->m_strTotalLabor ) ) {
			$arrstrTotalLabor = explode( ':', $this->m_strTotalLabor );
			if( valArr( $arrstrTotalLabor ) && 2 <= \Psi\Libraries\UtilFunctions\count( $arrstrTotalLabor ) ) {
				$objDateDiff = date_diff( $strTimeEndPoint, $strTimeStartPoint );
				$intHrs = ( int ) $objDateDiff->h + ( ( int ) $objDateDiff->d + 1 ) * 24;
				$intMin = ( int ) $objDateDiff->m;
				if( ( int ) $arrstrTotalLabor[0] > $intHrs || ( ( int ) $arrstrTotalLabor[0] >= $intHrs && ( int ) $arrstrTotalLabor[1] > $intMin ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_labor', __( 'Total time is crossing start and end date limit. Please enter appropriate total time.' ) ) );
				}
			}
		}
		return $boolIsValid;
	}

}
?>