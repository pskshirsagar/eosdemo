<?php

class CRuleStopResult extends CBaseRuleStopResult {

	protected $m_boolAllowNotesOnApproval;
	protected $m_boolRequireNotesOnRejectedReturned;

	protected $m_intRouteId;
	protected $m_intRouteRuleId;
	protected $m_intCompanyGroupId;
	protected $m_intRuleStopCompanyUserId;

	protected $m_strCompanyUserName;
	protected $m_strActualCompanyUserName;
	protected $m_strCompanyGroupName;
	protected $m_strPropertyName;
	protected $m_strEmailFrequency;
	protected $m_strRelieverName;
	protected $m_strRelieverEmailAddress;
	protected $m_strCompanyUserEmailAddress;
	protected $m_strPreferredLocaleCode;

	// Get Functions

	public function getRouteId() {
		return $this->m_intRouteId;
	}

	public function getPreferredLocaleCode() {
		return $this->m_strPreferredLocaleCode;
	}

	public function setPreferredLocaleCode( $strPreferredLocaleCode ) {
		$this->m_strPreferredLocaleCode = $strPreferredLocaleCode;
	}

	public function getRouteRuleId() {
		return $this->m_intRouteRuleId;
	}

	public function getCompanyGroupId() {
		return $this->m_intCompanyGroupId;
	}

	public function getCompanyUserName() {
		return $this->m_strCompanyUserName;
	}

	public function getActualCompanyUserName() {
		return $this->m_strActualCompanyUserName;
	}

	public function getCompanyGroupName() {
		return $this->m_strCompanyGroupName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getRuleStopCompanyUserId() {
		return $this->m_intRuleStopCompanyUserId;
	}

	public function getAllowNotesOnApproval() {
		return $this->m_boolAllowNotesOnApproval;
	}

	public function getRequireNotesOnRejectedReturned() {
		return $this->m_boolRequireNotesOnRejectedReturned;
	}

	public function getEmailFrequency() {
		return $this->m_strEmailFrequency;
	}

	public function getCompanyUserEmailAddress() {
		return $this->m_strCompanyUserEmailAddress;
	}

	public function getRelieverName() {
		return $this->m_strRelieverName;
	}

	public function getRelieverEmailAddress() {
		return $this->m_strRelieverEmailAddress;
	}

	// Set Functions

	public function setRouteId( $intRouteId ) {
		$this->m_intRouteId = $intRouteId;
	}

	public function setRouteRuleId( $intRouteRuleId ) {
		$this->m_intRouteRuleId = $intRouteRuleId;
	}

	public function setCompanyGroupId( $intCompanyGroupId ) {
		$this->m_intCompanyGroupId = $intCompanyGroupId;
	}

	public function setCompanyUserName( $strCompanyUserName ) {
		$this->m_strCompanyUserName = $strCompanyUserName;
	}

	public function setActualCompanyUserName( $strActualCompanyUserName ) {
		$this->m_strActualCompanyUserName = $strActualCompanyUserName;
	}

	public function setCompanyGroupName( $strCompanyGroupName ) {
		$this->m_strCompanyGroupName = $strCompanyGroupName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setRuleStopCompanyUserId( $intRuleStopCompanyUserId ) {
		$this->m_intRuleStopCompanyUserId = $intRuleStopCompanyUserId;
	}

	public function setAllowNotesOnApproval( $boolAllowNotesOnApproval ) {
		$this->set( 'm_boolAllowNotesOnApproval', CStrings::strToBool( $boolAllowNotesOnApproval ) );
	}

	public function setRequireNotesOnRejectedReturned( $boolRequireNotesOnRejectedReturned ) {
		return $this->m_boolRequireNotesOnRejectedReturned = $boolRequireNotesOnRejectedReturned;
	}

	public function setEmailFrequency( $strEmailFrequency ) {
		$this->m_strEmailFrequency = $strEmailFrequency;
	}

	public function setCompanyUserEmailAddress( $strCompanyUserEmailAddress ) {
		$this->m_strCompanyUserEmailAddress = $strCompanyUserEmailAddress;
	}

	public function setRelieverName( $strRelieverName ) {
		$this->m_strRelieverName = $strRelieverName;
	}

	public function setRelieverEmailAddress( $strRelieverEmailAddress ) {
		$this->m_strRelieverEmailAddress = $strRelieverEmailAddress;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['route_id'] ) )							$this->setRouteId( $arrmixValues['route_id'] );
		if( true == isset( $arrmixValues['route_rule_id'] ) )						$this->setRouteRuleId( $arrmixValues['route_rule_id'] );
		if( true == isset( $arrmixValues['company_group_id'] ) )					$this->setCompanyGroupId( $arrmixValues['company_group_id'] );
		if( true == isset( $arrmixValues['company_user_name'] ) )					$this->setCompanyUserName( $arrmixValues['company_user_name'] );
		if( true == isset( $arrmixValues['actual_company_user_name'] ) )			$this->setActualCompanyUserName( $arrmixValues['actual_company_user_name'] );
		if( true == isset( $arrmixValues['company_group_name'] ) )					$this->setCompanyGroupName( $arrmixValues['company_group_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )						$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['rule_stop_company_user_id'] ) )			$this->setRuleStopCompanyUserId( $arrmixValues['rule_stop_company_user_id'] );
		if( true == isset( $arrmixValues['allow_notes_on_approval'] ) )				$this->setAllowNotesOnApproval( $arrmixValues['allow_notes_on_approval'] );
		if( true == isset( $arrmixValues['require_notes_on_rejected_returned'] ) )	$this->setRequireNotesOnRejectedReturned( $arrmixValues['require_notes_on_rejected_returned'] );
		if( true == isset( $arrmixValues['email_frequency'] ) )					$this->setEmailFrequency( $arrmixValues['email_frequency'] );
		if( true == isset( $arrmixValues['company_user_email_address'] ) )		$this->setCompanyUserEmailAddress( $arrmixValues['company_user_email_address'] );
		if( true == isset( $arrmixValues['reliever_name'] ) )					$this->setRelieverName( $arrmixValues['reliever_name'] );
		if( true == isset( $arrmixValues['reliever_email_address'] ) )			$this->setRelieverEmailAddress( $arrmixValues['reliever_email_address'] );
		if( true == isset( $arrmixValues['preferred_locale_code'] ) )			$this->setPreferredLocaleCode( $arrmixValues['preferred_locale_code'] );

		return;
	}

	// Val Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRuleStopId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRuleStopId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rule_stop_id', 'Rule stop id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRuleStopStatusTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRuleStopStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rule_stop_status_type_id', 'Rule stop status type id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;

		if( true == is_null( $this->getReferenceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_id', 'Reference id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRouteTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRouteTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'route_type_id', 'Route type id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', 'Company user id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valResultDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getResultDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'result_datetime', 'Result datetime is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNotes( $objDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objDatabase, 'CDatabase' ) ) return $boolIsValid;

		$objRoute = $this->fetchRoute( $objDatabase );

		if( false == is_null( $this->getNotes() ) && false == $objRoute->getAllowNotesOnApproval() && CRuleStopStatusType::APPROVED == $this->getRuleStopStatusTypeId() && false == in_array( $this->getRouteTypeId(), [ CRouteType::JOB_BUDGET, CRouteType::ADVANCED_BUDGET_WORKSHEET ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'Note is not allowed.' ) );
		}

		if( true == is_null( $this->getNotes() ) && true == $objRoute->getRequireNotesOnRejectedReturned() && CRuleStopStatusType::DENIED == $this->getRuleStopStatusTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'Note is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getIpAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'Ip address is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valRuleStopId();
				$boolIsValid &= $this->valRuleStopStatusTypeId();
				$boolIsValid &= $this->valReferenceId();
				$boolIsValid &= $this->valRouteTypeId();
				break;

			case 'process_rule_stop_result':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valRuleStopId();
				$boolIsValid &= $this->valRuleStopStatusTypeId();
				$boolIsValid &= $this->valReferenceId();
				$boolIsValid &= $this->valRouteTypeId();
				$boolIsValid &= $this->valCompanyUserId();
				$boolIsValid &= $this->valResultDatetime();
				$boolIsValid &= $this->valNotes( $objDatabase );
				$boolIsValid &= $this->valIpAddress();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// Fetch Functions

	public function fetchRoute( $objDatabase ) {
		return \Psi\Eos\Entrata\CRoutes::createService()->fetchRouteByRuleStopIdByRouteTypeIdByCid( $this->getRuleStopId(), $this->getRouteTypeId(), $this->getCid(), $objDatabase );
	}

	public function fetchRuleStop( $objDatabase ) {
		return \Psi\Eos\Entrata\CRuleStops::createService()->fetchRuleStopByIdByCid( $this->getRuleStopId(), $this->getCid(), $objDatabase );
	}

	public function fetchRouteRule( $objDatabase ) {
		return \Psi\Eos\Entrata\CRouteRules::createService()->fetchRouteRuleByRuleStopIdByRouteTypeIdByCid( $this->getRuleStopId(), $this->getRouteTypeId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyEmployeeReliever( $objDatabase ) {
		return current( ( array ) \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeRelieverByRuleStopIdsByCid( [ $this->getRuleStopId() ], $this->getCid(), $objDatabase ) );
	}

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchApprovalPreference( $objDatabase ) {
		return CApprovalPreferences::fetchApprovalPreferenceByRouteTypeIdByCid( $this->getRouteTypeId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUser( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $this->getCompanyUserId(), $this->getCid(), $objDatabase, $boolIsCheckForEnabled = true );
	}

	// Other Functions

	public function getEventReferenceTypeId() {
		$intEventReferenceTypeId = NULL;

		switch( $this->getRouteTypeId() ) {
			case \CRouteType::JOB_BUDGET:
				$intEventReferenceTypeId = \CEventReferenceType::JOB;
				break;

			case \CRouteType::ADVANCED_BUDGET_WORKSHEET:
				$intEventReferenceTypeId = \CEventReferenceType::ADVANCED_BUDGET_WORKSHEET;
				break;

			default:
				$intEventReferenceTypeId = NULL;
				break;
		}

		return $intEventReferenceTypeId;
	}

	public function syncMatchingRuleStopResults( $objNextRuleStopResult = NULL, $objDatabase ) {

		$arrobjOtherRuleStopResults = ( array ) \Psi\Eos\Entrata\CRuleStopResults::createService()->fetchOtherRuleStopResultsByCidById( $this->getCid(), $this->getId(), $objDatabase );

		$arrobjRuleStopResults = [];

		foreach( $arrobjOtherRuleStopResults as $objOtherRuleStopResult ) {

			$objRuleStopResult = clone $objOtherRuleStopResult;

			$objRuleStopResult->setId( NULL );
			$objRuleStopResult->setRuleStopStatusTypeId( $this->getRuleStopStatusTypeId() );
			$objRuleStopResult->setCompanyUserId( $this->getCompanyUserId() );
			$objRuleStopResult->setResultDatetime( 'NOW()' );
			$objRuleStopResult->setIpAddress( $this->getIpAddress() );
			$objRuleStopResult->setNotes( $this->getNotes() );

			$arrobjRuleStopResults[] = $objRuleStopResult;
		}

		if( true == valObj( $objNextRuleStopResult, 'CRuleStopResult' ) ) {

			foreach( $arrobjOtherRuleStopResults as $objOtherRuleStopResult ) {

				$objRuleStopResult = clone $objOtherRuleStopResult;

				$objRuleStopResult->setId( NULL );
				$objRuleStopResult->setRuleStopId( $objNextRuleStopResult->getRuleStopId() );
				$objRuleStopResult->setRuleStopStatusTypeId( $objNextRuleStopResult->getRuleStopStatusTypeId() );
				$objRuleStopResult->setCompanyUserId( $objNextRuleStopResult->getCompanyUserId() );
				$objRuleStopResult->setResultDatetime( 'NOW()' );
				$objRuleStopResult->setIpAddress( $objNextRuleStopResult->getIpAddress() );
				$objRuleStopResult->setNotes( $objNextRuleStopResult->getNotes() );

				$arrobjRuleStopResults[] = $objRuleStopResult;
			}
		}

		return $arrobjRuleStopResults;
	}

	// Send Notification Email

	public function sendNotificationEmail( $objReference, $intCompanyUserId, $objClientDatabase, $objEmailDatabase, $objPrevRuleStopResult = NULL ) {

		$objApprovalPreference		= $this->fetchApprovalPreference( $objClientDatabase );
		$objCompanyEmployeeReliever	= $this->fetchCompanyEmployeeReliever( $objClientDatabase );
		$objRuleStop				= $this->fetchRuleStop( $objClientDatabase );

		$arrintPropertyIds = [ $this->getPropertyId() ];

		// load Users
		list( $arrobjEmailCompanyUsers, $objCompanyGroup ) = $objRuleStop->fetchEmailCompanyUsersByApprovalPreferenceByPropertyIds( $objApprovalPreference, $arrintPropertyIds, $objClientDatabase );

		if( false == valArr( $arrobjEmailCompanyUsers ) ) return false;

		// load Urls, Route Type and Reference Number
		list( $strEntrataUrl, $strNotificationUrl, $strRouteType, $strReferenceNumber ) = $this->buildNotificationData( $objReference, $objClientDatabase );

		if( false == valStr( $strEntrataUrl ) ) return true;

		if( CRuleStopStatusType::APPROVED == $this->getRuleStopStatusTypeId() ) {

			$objNoteCompanyUser = $this->fetchCompanyUser( $objClientDatabase );

			$strSubject			= 'A ' . $strRouteType . ' is approved';
			$strMessage			= 'A ' . $strRouteType . ' is approved. Please follow the link below to visit this ' . $strRouteType . '.';
			$strNote			= $this->getNotes();
			$strNoteUser		= ( true == valObj( $objNoteCompanyUser, 'CCompanyUser' ) ) ? $objNoteCompanyUser->getNameFull() : '';

		} else {

			$strSubject			= 'A ' . $strRouteType . ' is pending your approval';
			$strMessage			= 'A new ' . $strRouteType . ' is pending your approval. Please follow the link below to visit this ' . $strRouteType . '.';
			$strNote			= ( true == valObj( $objPrevRuleStopResult, 'CRuleStopResult' ) ) ? $objPrevRuleStopResult->getNotes() : '';
			$strNoteUser		= '';
		}

		foreach( $arrobjEmailCompanyUsers as $objEmailCompanyUser ) {
			$arrstrRecipentEmailAddresses[] = $objEmailCompanyUser->getEmailAddress();
		}

		if( true == valObj( $objCompanyEmployeeReliever, 'CCompanyEmployee' ) && true == valStr( $objCompanyEmployeeReliever->getEmailAddress() ) ) {
			$arrstrRecipentEmailAddresses[] = $objCompanyEmployeeReliever->getEmailAddress();
		}

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::FEEDBACK );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::NORMAL );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setReplyToEmailAddress( '' );
		$objSystemEmail->setSubject( $strSubject );

		if( true == valObj( $objCompanyGroup, 'CCompanyGroup' ) ) {

			$strUserName = $objCompanyGroup->getName();

			$objSystemEmail->setToEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

			foreach( $arrobjEmailCompanyUsers as $objEmailCompanyUser ) {

				$objSystemEmailRecipient = new CSystemEmailRecipient();
				$objSystemEmailRecipient->setRecipientTypeId( CSystemEmailRecipientType::COMPANY_EMPLOYEE );
				$objSystemEmailRecipient->setCid( $this->getCid() );
				$objSystemEmailRecipient->setReferenceNumber( $objEmailCompanyUser->getId() );
				$objSystemEmailRecipient->setEmailAddress( $objEmailCompanyUser->getEmailAddress() );

				$objSystemEmail->addSystemEmailRecipient( $objSystemEmailRecipient );
			}

		} else {
			$strUserName = $objEmailCompanyUser->getNameFirst();

			$objSystemEmail->setToEmailAddress( implode( ',', $arrstrRecipentEmailAddresses ) );
		}

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );
		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign( 'entrata_url',					$strEntrataUrl );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX',	CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'user_name',					$strUserName );
		$objSmarty->assign( 'message',						$strMessage );
		$objSmarty->assign( 'note',							$strNote );
		$objSmarty->assign( 'note_user',					$strNoteUser );
		$objSmarty->assign( 'reference_number',				$strReferenceNumber );
		$objSmarty->assign( 'route_type',					$strRouteType );
		$objSmarty->assign( 'notification_url',				$strNotificationUrl );

		$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/approval_routing/approval_routing_notification_email.tpl' );

		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );

		switch( NULL ) {
			default:
				$boolIsValid = true;
				$boolIsValid &= $objSystemEmail->validate( 'validate_rca_email' );

				if( false == $boolIsValid ) {
					$this->addErrorMsgs( $objSystemEmail->getErrorMsgs() );
					break;
				}

				if( false == $objSystemEmail->insert( $intCompanyUserId, $this->getAdo()->m_objEmailDatabase ) ) {
					$boolIsValid &= false;
					break;
				}

				$arrobjSystemEmailRecipient = ( array ) $objSystemEmail->getSystemEmailRecipients();
		}

		return $boolIsValid;
	}

	public function sendNotificationEmailToCreator( $objReference, $intCompanyUserId, $objClientDatabase ) {

		$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $objReference->getCreatedBy(), $this->getCid(), $objClientDatabase );

		if( false == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) || false == valStr( $objCompanyEmployee->getEmailAddress() ) ) return true;

		// load Urls, Route Type and Reference Number
		list( $strEntrataUrl, $strNotificationUrl, $strRouteType, $strReferenceNumber, $strPropertyName ) = $this->buildNotificationData( $objReference, $objClientDatabase );

		if( false == valStr( $strEntrataUrl ) ) return true;

		$objNoteCompanyUser = $this->fetchCompanyUser( $objClientDatabase );

		$strNote			= $this->getNotes();
		$strNoteUser		= ( true == valObj( $objNoteCompanyUser, 'CCompanyUser' ) ) ? $objNoteCompanyUser->getNameFull() : '';

		if( CRuleStopStatusType::APPROVED == $this->getRuleStopStatusTypeId() ) {

			$strSubject			= 'A ' . $strRouteType . ' is approved';
			$strMessage			= 'A ' . $strRouteType . ' is approved. Please follow the link below to visit this ' . $strRouteType . '.';

		} else {

			$strSubject			= 'A ' . $strRouteType . ' has been sent back for revision';
			$strMessage			= 'A new ' . $strRouteType . ' is pending your approval. Please follow the link below to visit this ' . $strRouteType . '.';
		}

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::FEEDBACK );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::NORMAL );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setReplyToEmailAddress( '' );
		$objSystemEmail->setSubject( $strSubject );
		$objSystemEmail->setToEmailAddress( $objCompanyEmployee->getEmailAddress() );

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );
		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign( 'entrata_url',					$strEntrataUrl );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX',	CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'user_name',					$objCompanyEmployee->getNameFirst() );
		$objSmarty->assign( 'message',						$strMessage );
		$objSmarty->assign( 'property_name',				$strPropertyName );
		$objSmarty->assign( 'note',							$strNote );
		$objSmarty->assign( 'note_user',					$strNoteUser );
		$objSmarty->assign( 'reference_number',				$strReferenceNumber );
		$objSmarty->assign( 'route_type',					$strRouteType );
		$objSmarty->assign( 'notification_url',				$strNotificationUrl );

		$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/approval_routing/approval_routing_notification_email.tpl' );

		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );

		switch( NULL ) {
			default:
				$boolIsValid = true;
				$boolIsValid &= $objSystemEmail->validate( 'validate_rca_email' );

				if( false == $boolIsValid ) {
					$this->addErrorMsgs( $objSystemEmail->getErrorMsgs() );
					break;
				}

				if( false == $objSystemEmail->insert( $intCompanyUserId, $this->getAdo()->m_objEmailDatabase ) ) {
					$boolIsValid &= false;
					break;
				}
		}

		return $boolIsValid;
	}

	public function buildNotificationData( $objReference, $objClientDatabase ) {

		$objClient		= $this->fetchClient( $objClientDatabase );
		$objProperty	= $this->fetchProperty( $objClientDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) return NULL;

		$strSecureHostPrefix	= ( true == defined( CONFIG_SECURE_HOST_PREFIX ) ) ? CONFIG_SECURE_HOST_PREFIX : 'http://';
		$strRwxSuffix			= ( true == defined( CONFIG_RWX_LOGIN_SUFFIX ) ) ? CONFIG_RWX_LOGIN_SUFFIX : '.entrata.com';
		$strEntrataUrl			= $strSecureHostPrefix . $objClient->getRwxDomain() . $strRwxSuffix;

		switch( $this->getRouteTypeId() ) {

			case CRouteType::PURCHASE_ORDER:
			case CRouteType::PURCHASE_ORDER_LINE_ITEM:

				$strRouteType		= 'Purchase Order';
				$strReferenceNumber = 'PO Number: ' . $objReference->getHeaderNumber();
				$strNotificationUrl = $strEntrataUrl . '/?module=purchase_ordersxxx&ap_header_notification_id[id]=' . $objReference->getId();
				break;

			case CRouteType::INVOICE:
			case CRouteType::INVOICE_LINE_ITEM:
				$strRouteType		= 'Invoice';
				$strReferenceNumber = 'Invoice Number: ' . $objReference->getHeaderNumber();
				$strNotificationUrl = $strEntrataUrl . '/?module=invoicesxxx&ap_header_notification_id[id]=' . $objReference->getId();
				break;

			case CRouteType::PAYMENT:
				$strRouteType		= 'Payment';
				$strReferenceNumber = 'Payment: ' . $objReference->getPaymentNumber();
				$strNotificationUrl = $strEntrataUrl . '/?module=dashboard_approvals_checksxxx&ap_header_notification_id[id]=' . $objReference->getId();
				break;

			case CRouteType::JOURNAL_ENTRY:
				$strRouteType		= 'Journal Entry';
				$strReferenceNumber = 'Entry Number: ' . $objReference->getHeaderNumber();
				$strNotificationUrl = $strEntrataUrl . '/?module=general_journalxxx&gl_header_notification_id[id]=' . $objReference->getId();
				break;

			default:
				$strEntrataUrl = $strRouteType = $strReferenceNumber = $strNotificationUrl = NULL;
				break;
		}

		$strPropertyName = ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->getPropertyName() : NULL;

		return [ $strEntrataUrl, $strNotificationUrl, $strRouteType, $strReferenceNumber, $strPropertyName ];
	}

}
?>
