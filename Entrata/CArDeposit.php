<?php

class CArDeposit extends CBaseArDeposit {

	protected $m_intGlReconciliationId;
	protected $m_intPastPostMonth;
	protected $m_intFuturePostMonth;

	protected $m_boolValidatePastOrFuturePostMonth;

	protected $m_fltTotalAmount;
	protected $m_strBankAccountName;
	protected $m_strFormattedDepositNumber;

	protected $m_arrintArTransactionIds;

    public function __construct() {
        parent::__construct();

		$this->m_boolValidatePastOrFuturePostMonth = false;
        return;
    }

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['gl_reconciliation_id'] ) ) $this->setGlReconciliationId( $arrmixValues['gl_reconciliation_id'] );
        if( true == isset( $arrmixValues['bank_account_name'] ) ) $this->setBankAccountName( $arrmixValues['bank_account_name'] );
        if( true == isset( $arrmixValues['formatted_deposit_number'] ) ) $this->setFormattedDepositNumber( $arrmixValues['formatted_deposit_number'] );

        return;
	}

    public function setPostMonth( $strPostMonth ) {
    	$arrstrPostMonth = explode( '/', $strPostMonth );
    	if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
			$strPostMonth = $arrstrPostMonth[0] . '/1/' . $arrstrPostMonth[1];
    	}

    	$this->m_strPostMonth = $strPostMonth;
    }

	public function setPastPostMonth( $intPastPostMonth ) {
		$this->m_intPastPostMonth = $intPastPostMonth;
	}

	public function setFuturePostMonth( $intFuturePostMonth ) {
		$this->m_intFuturePostMonth = $intFuturePostMonth;
	}

	public function setGlReconciliationId( $intGlReconciliationId ) {
		$this->m_intGlReconciliationId = ( int ) $intGlReconciliationId;
	}

	public function setBankAccountName( $strBankAccountName ) {
		$this->m_strBankAccountName = ( string ) $strBankAccountName;
	}

	public function setFormattedDepositNumber( $strFormattedDepositNumber ) {
		$this->m_strFormattedDepositNumber = ( string ) $strFormattedDepositNumber;
	}

	public function setArTransactionIds( $arrintArTransactionIds ) {
		$this->m_arrintArTransactionIds = ( array ) $arrintArTransactionIds;
	}

	public function setPastPostMonthAndFuturePostMonth( $intCompanyUserId, $objDatabase ) {
		$arrstrCompanyUserPreferences = \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferencesByCompanyUserIdByCidByModuleIdByKeys( $intCompanyUserId, $this->getCid(), CModule::AR_POST_MONTH, [ CCompanyUserPreference::AR_FUTURE_POST_MONTH, CCompanyUserPreference::AR_PAST_POST_MONTH ], $objDatabase );

		if( true == valArr( $arrstrCompanyUserPreferences, 2 ) ) {
			$this->setPastPostMonth( $arrstrCompanyUserPreferences[CCompanyUserPreference::AR_PAST_POST_MONTH] );
			$this->setFuturePostMonth( $arrstrCompanyUserPreferences[CCompanyUserPreference::AR_FUTURE_POST_MONTH] );
		}
	}

	public function setValidatePastOrFuturePostMonth( $boolValidatePastOrFuturePostMonth ) {
		$this->m_boolValidatePastOrFuturePostMonth = ( bool ) $boolValidatePastOrFuturePostMonth;
	}

	/**
	 * Get Functions
	 */

	public function getGlReconciliationId() {
		return $this->m_intGlReconciliationId;
	}

	public function getBankAccountName() {
		return $this->m_strBankAccountName;
	}

	public function getFormattedPostMonth() {
		if( true == is_null( $this->m_strPostMonth ) ) return NULL;
		return \Psi\CStringService::singleton()->preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getFormattedDepositNumber() {
		return $this->m_strFormattedDepositNumber;
	}

	public function getArTransactionIds() {
		return $this->m_arrintArTransactionIds;
	}

	public function getPastPostMonth() {
		return $this->m_intPastPostMonth;
	}

	public function getFuturePostMonth() {
		return $this->m_intFuturePostMonth;
	}

	public function getValidatePastOrFuturePostMonth() {
		return $this->m_boolValidatePastOrFuturePostMonth;
	}

    /**
     * Validate Functions
     */

    public function valArDepositTypeId() {
    	$boolIsValid = true;

        if( true == is_null( $this->getArDepositTypeId() ) ) {

            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_deposit_type_id', __( 'Ar deposit type is required' ) ) );
        }

        return $boolIsValid;
    }

    public function valBankAccountId() {
        $boolIsValid = true;

        if( true == is_null( $this->getBankAccountId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_id', __( 'Gl account legal entity is required' ) ) );
        }

        return $boolIsValid;
    }

    public function valPostDate( $arrmixData, $arrmixPostDateandMonth ) {

        $boolIsValid = true;

        if( false == valStr( $this->getPostDate() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Post date is required.' ) ) );
        } elseif( false == CValidation::checkISODateFormat( $this->getPostDate(), true ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Post date must be in mm/dd/yyyy format.' ) ) );
        } elseif( 0 == $this->getIsDeleted() && true == valArr( $arrmixData ) && strtotime( $this->getPostDate() ) < strtotime( $arrmixData[0]['max_post_date'] ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Deposit post date should be greater than or equal to {%t, 0, DATE_NUMERIC_STANDARD}.', [ $arrmixData[0]['max_post_date'] ] ) ) );
        } elseif( 1 == $this->getIsDeleted() && true == valArr( $arrmixPostDateandMonth ) && strtotime( $arrmixPostDateandMonth[0]['post_date'] ) > strtotime( date( 'm/d/Y', strtotime( $this->getPostDate() ) ) ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', __( 'Reverse date must be greater than or equal to original deposit date {%t, 0, DATE_NUMERIC_STANDARD}.', [ $arrmixPostDateandMonth[0]['post_date'] ] ) ) );
        }

        return $boolIsValid;
    }

	public function valPostMonth( $arrmixData, $arrmixPostDateandMonth, $objDatabase = NULL, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT ) {

        $boolIsValid 		= true;
        $arrstrPostMonth 	= explode( '/', $this->getPostMonth() );
        $strPostMonth 		= $arrstrPostMonth[0] . '/' . $arrstrPostMonth[2];

        if( false == valStr( $this->getPostDate() ) ) {
        	return false;
        }

        if( false == valStr( $this->getPostMonth() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );
        } elseif( false == CValidation::validatePostMonth( $strPostMonth ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month must be in mm/yyyy format.' ) ) );
		} elseif( true == valArr( $arrmixData ) && strtotime( $this->getPostMonth() ) < strtotime( $arrmixData[0]['max_post_month'] ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Deposit post month should be greater than or equal to {%t, 0, DATE_NUMERIC_POSTMONTH}.', [ $arrmixData[0]['max_post_month'] ] ) ) );
		} elseif( strtotime( '1/1/1970' ) > strtotime( $this->getPostMonth() ) || strtotime( '12/1/2099' ) < strtotime( $this->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Entrata does not support transactions outside the date year range of 1970 to 2099.' ) ) );
		} elseif( true == valArr( $arrmixData ) && strtotime( $arrmixData[0]['max_ar_lock_month'] ) >= strtotime( $this->getPostMonth() ) && true == in_array( $intUserValidationMode, [ CPropertyGlSetting::VALIDATION_MODE_STRICT, CPropertyGlSetting::VALIDATION_MODE_OPEN_PERIODS ] ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Warning: Period locked. You cannot post a deposit on or before {%t, 0, DATE_NUMERIC_POSTMONTH}.', [ $arrmixData[0]['max_ar_lock_month'] ] ) ) );
		} elseif( 1 == $this->getIsDeleted() && true == valArr( $arrmixData ) && false == $arrmixData[0]['activate_standard_posting'] ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', __( 'Warning: Activate Gl posting is locked. You cannot reverse the transaction.' ) ) );
		} elseif( 1 == $this->getIsDeleted() && true == valArr( $arrmixPostDateandMonth ) && strtotime( date( 'm/01/Y', strtotime( $this->getPostMonth() ) ) ) < strtotime( $arrmixPostDateandMonth[0]['post_month'] ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'The reversal post month cannot be less than original post month {%t, 0, DATE_NUMERIC_POSTMONTH}.', [ $arrmixPostDateandMonth[0]['post_month'] ] ) ) );
		}

  		if( false == valArr( $arrmixData ) || false == $this->getValidatePastOrFuturePostMonth() || false == $boolIsValid || false == valObj( $objDatabase, 'CDatabase' ) ) return $boolIsValid;

		$intPastPostMonth 	= strtotime( $arrmixData[0]['max_ar_post_month'] );
		$intFuturePostMonth = $intPastPostMonth;

		if( false == is_null( $this->getPastPostMonth() ) && false == is_null( $this->getFuturePostMonth() ) ) {
			$intPastPostMonth 	= strtotime( $arrmixData[0]['max_ar_post_month'] . ' - ' . $this->getPastPostMonth() . ' month' );
			$intFuturePostMonth = strtotime( $arrmixData[0]['max_ar_post_month'] . ' + ' . $this->getFuturePostMonth() . ' month' );
		}

		if( strtotime( $this->getPostMonth() ) < $intPastPostMonth || strtotime( $this->getPostMonth() ) > $intFuturePostMonth ) {

			if( $intPastPostMonth != $intFuturePostMonth ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month should be between {%t, past_post_month, DATE_NUMERIC_POSTMONTH} and {%t, future_post_month, DATE_NUMERIC_POSTMONTH}.', [ 'past_post_month' => $intPastPostMonth, 'future_post_month' => $intFuturePostMonth ] ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Deposit Post month should be {%t, 0, DATE_NUMERIC_POSTMONTH}.', [ $intFuturePostMonth ] ) ) );
			}

			$boolIsValid &= false;
		}

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT ) {
        $boolIsValid    		= true;
        $arrmixData     		= [];
        $arrmixPostDateandMonth = [];

        if( true == valArr( $this->m_arrintArTransactionIds ) ) {
        	$arrmixData = $this->fetchAssociatedArTransactionsAndPropertyGlSettingDetails( $objDatabase );
        }

        if( 1 == $this->getIsDeleted() ) {
        	$arrmixPostDateandMonth = \Psi\Eos\Entrata\CArDeposits::createService()->fetchPostDateandPostMonthByIdByCid( $this->getOriginArDepositId(), $this->getCid(), $objDatabase );
        }

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valArDepositTypeId();
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valPostDate( $arrmixData, $arrmixPostDateandMonth );
				$boolIsValid &= $this->valPostMonth( $arrmixData, $arrmixPostDateandMonth, $objDatabase, $intUserValidationMode );
				break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valArDepositTypeId();
				$boolIsValid &= $this->valBankAccountId();
       			$boolIsValid &= $this->valPostDate( $arrmixData, $arrmixPostDateandMonth );
       			$boolIsValid &= $this->valPostMonth( $arrmixData, $arrmixPostDateandMonth, $objDatabase, $intUserValidationMode );
				break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchArPayments( $objPaymentDatabase ) {
    	return CArPayments::fetchAssociatedArPaymentsByArDepositIdByCid( $this->getId(), $this->getCid(), $objPaymentDatabase );
    }

    public function fetchArTransactions( $objDatabase ) {
    	return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionsByArDepositIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

    public function fetchAssociatedArTransactionsAndPropertyGlSettingDetails( $objDatabase ) {
    	return \Psi\Eos\Entrata\CArTransactions::createService()->fetchSimpleArTransactionsAndPropertyGlSettingDetailsByArTransactionIdsByCid( $this->m_arrintArTransactionIds, $this->getCid(), $objDatabase );
    }

    /**
     * Other Functions
     */

    public function fetchDepositNumber( $intCurrentUserId, $objDatabase ) {

    	$objCompanyNumber = CCompanyNumbers::fetchNextCompanyNumber( 'ar_deposit_number', $this->getCid(), $intCurrentUserId, $objDatabase );

        if( true == valObj( $objCompanyNumber, 'CCompanyNumber' ) ) {
	        $this->setDepositNumber( $objCompanyNumber->getArDepositNumber() );

        } else {

        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to get next company number by ar_deposit_number. The following error was reported.' ) ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );
        	return false;
	    }

	    return true;
    }

	public function insert( $intCurrentUserId, $objDatabase, $boolAutoIncrementDepositNumber = true ) {

		/**
		 * Checking Entrata Module write permission, if not then insert/update/delete would be restricted.
		 */

		if( false == checkEntrataModuleWritePermission() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'You have not been granted write access to this module.' ) ) );
			return false;
		}

	 	if( true == $boolAutoIncrementDepositNumber ) {
	 		$this->fetchDepositNumber( $intCurrentUserId, $objDatabase );
	 	}

	 	$strSql = 'SELECT * FROM func_ar_deposit_insert( ' .
				'%d, ' .
	 			$this->sqlCid() . ', ' .
	 			$this->sqlBankAccountId() . ', ' .
	 			$this->sqlArDepositTypeId() . ', ' .
	 			$this->sqlGlTransactionTypeId() . ', ' .
	 			$this->sqlSettlementDistributionId() . ', ' .
	 			$this->sqlOriginArDepositId() . ', ' .
	 			$this->sqlRemotePrimaryKey() . ', ' .
	 			$this->sqlDepositNumber() . ', ' .
	 			$this->sqlTransactionDatetime() . ', ' .
	 			$this->sqlDepositAmount() . ', ' .
	 			$this->sqlPostDate() . ', ' .
	 			$this->sqlPostMonth() . ', ' .
	 			$this->sqlDepositMemo() . ', ' .
	 			$this->sqlDetailsInserted() . ', ' .
	 			$this->sqlIsInitialImport() . ', ' .
	 			$this->sqlIsDeleted() . ', ' .
	 			( int ) $intCurrentUserId . ', \'{' . implode( ',', $this->getArTransactionIds() ) . '}\' );';

	 	$objDataset = $objDatabase->createDataset();

	 	$this->fetchNextId( $objDatabase );

	 	if( false == $objDataset->execute( sprintf( $strSql, $this->getId() ) ) ) {

	 		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

	 		$objDataset->cleanup();
	 		return false;
	 	}

	 	if( 0 < $objDataset->getRecordCount() ) {

	 		while( !$objDataset->eof() ) {
	 			$arrmixValues = $objDataset->fetchArray();
	 			$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
	 			$objDataset->next();
	 		}

	 		$objDataset->cleanup();
	 		return false;
	 	}

	 	$objDataset->cleanup();
	 	return true;

	}

    public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDeleteDeposit = false ) {

		if( true == $boolIsHardDeleteDeposit ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		// To insert offset/reversal into ar_deposits.
		$this->setIsDeleted( 1 );
		if( false == $this->insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

    }

    public function isReconciled( $objDatabase ) {
    	$objGlHeader = CGlHeaders::fetchReconciledArDepositByArDepositIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    	if( false == valObj( $objGlHeader, 'CGlHeader' ) ) {
    		return false;
    	}

    	return true;
    }

    public function isExported( $objDatabase ) {
    	$objGlHeader = CGlHeaders::fetchExportedArDepositByArDepositIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    	if( false == valObj( $objGlHeader, 'CGlHeader' ) ) {
    		return false;
    	}

    	return true;
    }

}
?>