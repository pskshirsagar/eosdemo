<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStoredObjects
 * Do not add any new functions to this class.
 */

class CStoredObjects extends CBaseStoredObjects {

	public static function fetchStoredObjectsByReferenceTableByReferenceIdByCid( $strReferenceTableName, $intEmailTemplateId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						stored_objects 
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND reference_id = ' . ( int ) $intEmailTemplateId . '
						AND reference_table = \'' . $strReferenceTableName . '\'
						AND deleted_on IS NULL';

		return parent::fetchStoredObjects( $strSql, $objDatabase );
	}

	public static function fetchStoredObjectsByIdsByCid( $arrintStoredObjectIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintStoredObjectIds ) ) {
			return;
		}

		$strSql = 'SELECT * FROM stored_objects WHERE id IN ( ' . implode( ',', $arrintStoredObjectIds ) . ' ) AND cid = ' . ( int ) $intCid;
		return parent::fetchStoredObjects( $strSql, $objDatabase );
	}

	public static function fetchStoredObjectByContainerByVendorByByKeyByCid( $strContainer, $strVendor, $strKey, $intCid, CDatabase $objDatabase ) {
		if( false == valStr( $strContainer ) || false == valStr( $strKey ) || false == valId( $intCid ) ) return false;
		$strSql = '
			SELECT *
			FROM stored_objects so
			WHERE
				so.cid = ' . ( int ) $intCid . '
				AND so.container = \'' . $strContainer . '\'
				AND so.vendor = \'' . $strVendor . '\'
				AND so.key = \'' . addslashes( $strKey ) . '\'
		';

		return parent::fetchStoredObject( $strSql, $objDatabase );
	}

	public function fetchStoredObjectByCidByReferenceIdTableTagOrNullTag( $intCid, $intReferenceId, $strReferenceTable, $strReferenceTag, $objDatabase ) {
		// This fetches the stored_object record with the given $strReferenceTag *or* if none found, then the NULL valued reference_tag record

		$strSql = sprintf( '
	              SELECT 
	                * 
	              FROM 
	                stored_objects 
	              WHERE 
	                cid = %d
	                AND reference_id = %d
	                AND reference_table = %s 
	                %s
	                AND deleted_on IS NULL
	              ORDER BY
	                reference_tag NULLS %s
	              LIMIT 1',
			$intCid,
			$intReferenceId,
			pg_escape_literal( $objDatabase->getHandle(), $strReferenceTable ),
			valStr( $strReferenceTag ) ? sprintf( 'AND COALESCE(reference_tag, %1$s) = %1$s', pg_escape_literal( $objDatabase->getHandle(), $strReferenceTag ) ) : '',
			valStr( $strReferenceTag ) ? 'LAST' : 'FIRST' );

		return $this->fetchStoredObject( $strSql, $objDatabase );
	}

	public function fetchStoredObjectByCidByReferenceIdTableTag( $intCid, $intReferenceId, $strReferenceTable, $strReferenceTag, $objDatabase ) {

		$strSql = sprintf( '
                 SELECT 
                   * 
                 FROM 
                   stored_objects 
                 WHERE 
                   cid = %d
                   AND reference_id = %d
                   AND reference_table = %s 
                   AND %s
                   AND deleted_on IS NULL
                 ORDER BY id
                 LIMIT 1',
			$intCid,
			$intReferenceId,
			pg_escape_literal( $objDatabase->getHandle(), $strReferenceTable ),
			valStr( $strReferenceTag ) ? sprintf( 'reference_tag = %s', pg_escape_literal( $objDatabase->getHandle(), $strReferenceTag ) ) : 'reference_tag IS NULL' );

		return $this->fetchStoredObject( $strSql, $objDatabase );
	}

}
?>