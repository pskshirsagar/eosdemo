<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationStageStatuses
 * Do not add any new functions to this class.
 */

class CApplicationStageStatuses extends CBaseApplicationStageStatuses {

	public static function fetchApplicationStageStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CApplicationStageStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApplicationStageStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApplicationStageStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllApplicationStageStatuses( $objDatabase, $boolIsExcludeExtensionLeaseIntervalType = false ) {
		$strSql = 'SELECT * FROM application_stage_statuses';
		if( false != $boolIsExcludeExtensionLeaseIntervalType ) {
			$strSql .= ' WHERE lease_interval_type_id <> ' . \CLeaseIntervalType::EXTENSION;
		}
		$strSql .= ' ORDER BY order_num';
		return self::fetchApplicationStageStatuses( $strSql, $objDatabase );
	}

	public static function fetchAllApplicationStageStatusesByExcludingStageIdsByExcludingStatusIdsByLeaseIntervalTypeId( $arrintExcludingStageIds,$arrintExcludingStatusIds, $intLeaseIntervalType, $objDatabase ) {
		if ( false == valArr( $arrintExcludingStageIds ) || false == valArr( $arrintExcludingStatusIds ) || false == valId( $intLeaseIntervalType ) ) return;

		$strSql = 'SELECT ass1.* FROM application_stage_statuses ass1 JOIN application_stage_statuses ass2 ON ( ass1.id != ass2.id ) WHERE  ass2.application_stage_id IN ( ' . implode( ',', $arrintExcludingStageIds ) . ') AND ass2.application_status_id IN ( ' . implode( ',', $arrintExcludingStatusIds ) . ') AND ass2.lease_interval_type_id = ' . ( int ) $intLeaseIntervalType . ' AND ass1.lease_interval_type_id = ' . ( int ) $intLeaseIntervalType;
		return self::fetchApplicationStageStatuses( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageStatusesByExcludingStatusIds( $arrintExcludingRenewalStatusIds, $objDatabase ) {
		return self::fetchApplicationStageStatuses( sprintf( 'SELECT * FROM application_stage_statuses WHERE application_status_id NOT IN ( ' . implode( ',', $arrintExcludingRenewalStatusIds ) . ')' ), $objDatabase );
	}

	public static function fetchApplicationStageStatusesByExcludingIds( $arrintIds, $objDatabase ) {
		return self::fetchApplicationStageStatuses( sprintf( 'SELECT * FROM application_stage_statuses WHERE id NOT IN ( ' . implode( ',', $arrintIds ) . ')' ), $objDatabase );
	}

	public static function fetchApplicationStageStatusIdsAndNamesByIds( $arrintApplicationStageStatusIds, $objDatabase ) {

		if ( false == valArr( $arrintApplicationStageStatusIds ) ) return;

		$strSql = '	SELECT
						id,
						util_get_translated( \'stage_name\', stage_name, details ) || \' \' || util_get_translated( \'status_name\', status_name, details ) AS name
					FROM
						application_stage_statuses
					WHERE
						id IN (  ' . implode( ',', $arrintApplicationStageStatusIds ) . ')
					ORDER BY
						order_num';

		$arrmixApplicationStageStatusDetails 	= [];
		$arrmixApplicationStageStatuses 		= fetchData( $strSql, $objDatabase );

		foreach( $arrmixApplicationStageStatuses as $arrmixApplicationStageStatus ) {
			$arrmixApplicationStageStatusDetails[$arrmixApplicationStageStatus['id']] = $arrmixApplicationStageStatus['name'];
		}

		return $arrmixApplicationStageStatusDetails;
	}

	public static function fetchApplicationStageStatusesByIds( $arrintApplicationStageStatusIds, $objDatabase ) {

		if ( false == valArr( $arrintApplicationStageStatusIds ) ) return;

		$strSql = '	SELECT
						*,
						util_get_translated( \'stage_name\', stage_name, details ) AS stage_name,
						util_get_translated( \'status_name\', status_name, details ) AS status_name
					FROM
						application_stage_statuses
					WHERE
						id IN (  ' . implode( ',', $arrintApplicationStageStatusIds ) . ')
					ORDER BY
						order_num';

		return self::fetchApplicationStageStatuses( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageStatusesByLeaseIntervalTypeId( $intLeaseIntervalType, $objDatabase, $intSeparateLeadsAndApplicantsStatus = NULL, $boolIsLeadTab = true ) {
		$strSeparateLeadsAndApplicantsCondition = '';
		$strSqlJoin = '';

		if( false == is_null( $intSeparateLeadsAndApplicantsStatus ) ) {

			if( true == $boolIsLeadTab ) {
				$strSqlJoin .= ' LEFT JOIN application_stage_statuses asst2 ON ( asst2.application_stage_id = asst.application_stage_id AND asst2.lease_interval_type_id = ' . ( int ) $intLeaseIntervalType . ' AND asst2.id = ' . ( int ) ( $intSeparateLeadsAndApplicantsStatus - 1 ) . ' ) ';
				$strSeparateLeadsAndApplicantsCondition = ' AND ( asst.id < ' . ( int ) $intSeparateLeadsAndApplicantsStatus . '  OR ( asst2.id is not null AND asst.next_application_stage_id = - 1 ) )';
			} else {
				$strSeparateLeadsAndApplicantsCondition = ' AND asst.id >= ' . ( int ) $intSeparateLeadsAndApplicantsStatus;
			}
		}

		$strSql = ' SELECT
					    asst.*
					FROM
					    application_stage_statuses asst ' . $strSqlJoin . '
					WHERE
						asst.lease_interval_type_id = ' . ( int ) $intLeaseIntervalType . $strSeparateLeadsAndApplicantsCondition . '
					ORDER BY
						asst.application_stage_id,
						asst.application_status_id,
						asst.id';

		return self::fetchApplicationStageStatuses( $strSql, $objDatabase );
	}

	public static function fetchApplicationStageStatusIdByLeaseIntervalIdByApplicationStageIdByApplicationStatusId( $intLeaseIntervalTypeId, $intApplicationStageId, $intApplicationStatusId, $objDatabase ) {

		$strSql = 'SELECT
						id AS application_stage_status_id
				   FROM
						application_stage_statuses
						WHERE
							lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . '
							AND application_stage_id = ' . ( int ) $intApplicationStageId . '
							AND application_status_id = ' . ( int ) $intApplicationStatusId;

		return self::fetchColumn( $strSql, 'application_stage_status_id', $objDatabase );

	}

	public static function fetchApplicationStageStatusIdByLeaseIntervalIdByLeaseIntervalTypeId( $intLeaseIntervalId, $intLeaseIntervalTypeId, $objDatabase ) {

		$strSql = 'SELECT
						ass.id AS application_stage_status_id
				   FROM
						application_stage_statuses ass
						JOIN applications app ON ( ass.lease_interval_type_id = ' . ( int ) $intLeaseIntervalTypeId . ' AND app.application_stage_id = ass.application_stage_id AND app.application_status_id = ass.application_status_id )
					WHERE
						app.lease_interval_id = ' . ( int ) $intLeaseIntervalId;

		return self::fetchColumn( $strSql, 'application_stage_status_id', $objDatabase );

	}

}
?>