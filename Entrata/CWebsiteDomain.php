<?php
use Psi\Eos\Entrata\CWebsiteDomains;
use Psi\Libraries\Cryptography\CCrypto;

class CWebsiteDomain extends CBaseWebsiteDomain {

	protected $m_intPropertyDomainCount;

	/**
	 * Get Functions
	 *
	 */

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyDomainCount() {
		return $this->m_intPropertyDomainCount;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_id'] ) )				$this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['property_domain_count'] ) ) 	$this->setPropertyDomainCount( $arrmixValues['property_domain_count'] );
	}

	public function setEmailDomain( $strEmailDomain ) {
		$strEmailDomain = str_replace( 'www.', '', \Psi\CStringService::singleton()->strtolower( $strEmailDomain ) );
		$this->m_strEmailDomain = CStrings::strTrimDef( $strEmailDomain, 240, NULL, true );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPropertyDomainCount( $intPropertyDomainCount ) {
		$this->m_intPropertyDomainCount = $intPropertyDomainCount;
	}

	public function setWebsiteDomain( $strWebsiteDomain ) {
		parent::setWebsiteDomain( str_replace( 'www.', '', \Psi\CStringService::singleton()->strtolower( $strWebsiteDomain ) ) );
		parent::setEmailDomain( str_replace( 'http://', '', str_replace( 'www.', '', $strWebsiteDomain ) ) );
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchWebsiteDomainRecords( $objDatabase ) {
		return CRecords::fetchRecordsByWebsiteDomain( $this->getWebsiteDomain(), $objDatabase );
	}

	public function fetchDirectiveByDirectiveTypeId( $intDirectiveTypeId, $objDatabase ) {
		return CDirectives::fetchDirectiveByWebsiteDomainIdByCidByDirectiveTypeId( $this->getId(), $this->getCid(), $intDirectiveTypeId, $objDatabase );
	}

	public function fetchCompetingDirectiveCount( $objDatabase ) {
		return CDirectives::fetchCompetingDirectiveCountByDomainByDirectiveTypeIdByCid( $this->getWebsiteDomain(), CDirectiveType::WEBSITE_FULL_DOMAIN, $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteDomainsCertificates( $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteDomainsCertificates::createService()->fetchWebsiteDomainsCertificatesByDomainIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchWebsiteCertificates( $objDatabase ) {
		return \Psi\Eos\Entrata\CWebsiteCertificates::createService()->fetchWebsiteCertificatesByDomainIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valWebsiteDomain( $strAction, $objDatabase, $objConnectDatabase ) {
		$boolIsValid	= true;

		if( VALIDATE_DELETE == $strAction ) {
			$arrstrDomainParts	= explode( '.', $this->getWebsiteDomain() );

			if( 'www' == $arrstrDomainParts[0] ) {
				// Subdomains  will exist on client database any way, so we dont need to verify this with connect.routes [SG]
				$arrobjWebsiteSubdomains = CWebsiteDomains::createService()->fetchWebsiteSubDomainsByWebsiteDomainIdByCid( $this->getId(), $this->getCid(), $objDatabase );
				if( true == valArr( $arrobjWebsiteSubdomains ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please delete all subdomain under this domain.' ) ) );
					$boolIsValid = false;
				}
			}
		} else {
			$strValidateResponse	= CValidation::checkDomain( $this->getWebsiteDomain() );

			if( 1 != $strValidateResponse ) {
				$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', $strValidateResponse ) );
				$boolIsValid = false;
			}

			if( true == CValidation::validateFullDomain( $this->getWebsiteDomain() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rwx_domain', __( 'This domain name is already reserved.' ) ) );
				$boolIsValid = false;
			}

			if( false == is_null( $this->getWebsiteDomain() ) ) {
				$arrstrDomainParts			= explode( '.', $this->getWebsiteDomain() );
				$arrstrLastTwoDomainParts	= array_slice( $arrstrDomainParts, -2 );
				$strMasterDomainName		= implode( '.', $arrstrLastTwoDomainParts );

				if( 3 <= \Psi\Libraries\UtilFunctions\count( $arrstrDomainParts ) && 'www' != $arrstrDomainParts[0] ) {
					// WE are adding extra check if a company us using other company's domain.
					if( 0 < CDirectives::fetchCompetingDirectiveCountByDomainByDirectiveTypeIdByCid( $strMasterDomainName, CDirectiveType::WEBSITE_FULL_DOMAIN, $this->getCid(), $objConnectDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( ' Website domain \'{%s,0}\' has already been taken.', [ $strMasterDomainName ] ) ) );
						return false;
					}

				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $objConnectDatabase = NULL, $arrobjConnectDatabases = NULL, $boolTransferValidation = false, $boolCheckDomainTransfer = false, $objDnsDatabase = NULL, $objCompanyUser = NULL, $objNewWebsite = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valWebsiteDomain( VALIDATE_INSERT, $objDatabase, $objConnectDatabase );
				if( false == $boolIsValid ) break;
				$boolIsValid &= ( true == $this->isDuplicate( $objDatabase, $objConnectDatabase ) ) ? false : true;
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valWebsiteDomain( VALIDATE_UPDATE, $objDatabase, $objConnectDatabase );
				if( true == $boolIsValid && true == $boolTransferValidation ) {
					$boolIsValid &= ( true == $this->isValidateDomainTransfer( $objDatabase, $objConnectDatabase, $arrobjConnectDatabases, $boolCheckDomainTransfer, $objDnsDatabase, $objCompanyUser, $objNewWebsite ) ) ? false : true;
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valWebsiteDomain( VALIDATE_DELETE, $objDatabase, $objConnectDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valWebsiteSubdomain( $objDatabase, $objConnectDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getWebsiteDomain() ) ) {
			$arrstrDomainParts			= explode( '.', $this->getWebsiteDomain() );
			$arrstrLastTwoDomainParts	= array_slice( $arrstrDomainParts, -2 );
			$strMasterDomainName		= implode( '.', $arrstrLastTwoDomainParts );

			if( 3 >= \Psi\Libraries\UtilFunctions\count( $arrstrDomainParts ) ) {
				if( false == $this->isExist( $strMasterDomainName, $objDatabase, $objConnectDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'Please add the domains {%s,0} ', [ $strMasterDomainName ] ) ) );
					$boolIsValid = false;
				}
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'Invalid Domain format.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createWebsiteDomainRecord() {
		return new CRecord();
	}

	public function createDirective( $objClientDatabase ) {
		$objDirective = new CDirective();
		$objDirective->setCid( $this->getCid() );
		$objDirective->setDirectiveTypeId( CDirectiveType::WEBSITE_FULL_DOMAIN );
		$objDirective->setWebsiteId( $this->getWebsiteId() );
		$objDirective->setPropertyId( $this->getPropertyId() );
		$objDirective->setWebsiteDomainId( $this->getId() );
		$objDirective->setDomain( \Psi\CStringService::singleton()->strtolower( $this->getWebsiteDomain() ) );
		$objDirective->setClusterId( $objClientDatabase->getClusterId() );
		$objDirective->setDatabaseId( $objClientDatabase->getId() );

		return $objDirective;
	}

	/**
	 * DML Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objClientDatabase, $arrobjConnectDatabases = NULL, $boolIsInterClientDomainTransfer = false ) {

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || false == valArr( $arrobjConnectDatabases ) ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			exit;
		}

		if( false == defined( 'CONFIG_DB_HOST_CONNECT' ) || false == array_key_exists( CONFIG_DB_HOST_CONNECT, $arrobjConnectDatabases ) ) {
			trigger_error( 'Failed to load master connect database object', E_USER_ERROR );
			exit;
		}

		$intCount	= substr_count( $this->getWebsiteDomain(), '.' );
		$strDomain	= $this->getWebsiteDomain();

		if( 1 < $intCount ) {
			$objParentDirective = CDirectives::fetchDirectiveByWebsiteDomainIdByCid( $this->getParentDomainId(), $this->getCid(),  $arrobjConnectDatabases[CONFIG_DB_HOST_CONNECT] );

			if( true == valObj( $objParentDirective, 'CDirective' ) && false == is_null( $objParentDirective->getDomain() ) ) {
				$strDomain = $objParentDirective->getDomain();
			}
		}

		$this->fetchNextId( $objClientDatabase );

		$objDirective = CDirectives::fetchCustomDirectiveByDomainByDirectiveTypeId( $this->getWebsiteDomain(), CDirectiveType::WEBSITE_FULL_DOMAIN, $arrobjConnectDatabases[CONFIG_DB_HOST_CONNECT] );

		if( true == valObj( $objDirective, 'CDirective' ) ) {
			if( true == $boolIsInterClientDomainTransfer || ( $objDirective->getCid() == $this->getCid() ) ) {
				$objDirective->setWebsiteDomainId( $this->getId() );
				$objDirective->setWebsiteId( $this->getWebsiteId() );
				$objDirective->setDeletedOn( NULL );
				$objDirective->setDeletedBy( NULL );
				$objDirective->setDirectiveStatusTypeId( CDirectiveStatusType::ACTIVE );

				if( false == $objDirective->update( $intCurrentUserId, $arrobjConnectDatabases[CONFIG_DB_HOST_CONNECT] ) ) {
					return false;
				}
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( ' Website domain \'{%s,0}\' has already been taken by another client.', [ $this->getWebsiteDomain() ] ) ) );
				return false;
			}
		}

		$objDirective = $this->createDirective( $objClientDatabase );
		$objDirective->fetchNextId( $arrobjConnectDatabases[CONFIG_DB_HOST_CONNECT] );

		if( 'production' == CONFIG_ENVIRONMENT ) {
			if( true == \Psi\CStringService::singleton()->strstr( $strDomain, '.properties' ) || true == \Psi\CStringService::singleton()->strstr( $strDomain, '.apartments' ) ) {
				$strWhoisResult = shell_exec( 'whois -h whois.donuts.co ' . trim( \Psi\CStringService::singleton()->strtolower( $strDomain ) ) );
			} else {
				$strWhoisResult = shell_exec( 'whois ' . trim( \Psi\CStringService::singleton()->strtolower( $strDomain ) ) );
			}

			preg_match( '/Registrar:.*/', $strWhoisResult, $arrstrResult );

			if( true == valArr( $arrstrResult ) ) {
				$objDirective->setHostedAt( trim( str_replace( ':', '', \Psi\CStringService::singleton()->strstr( $arrstrResult[0], ':' ) ) ) );
			}

			// Updating dns a record information
			$arrstrDigResult		= dns_get_record( str_replace( 'www.', '', $objDirective->getDomain() ), DNS_A );
			$strDnsInfo				= '';

			if( true == valArr( $arrstrDigResult ) ) {
				$strDnsInfo			= 'A:';

				foreach( $arrstrDigResult as $strDigARecord ) {
					$strDnsInfo		.= $strDigARecord['ip'] . ',';
				}
			}

			$arrstrDigCnameResult		= dns_get_record( 'www.' . str_replace( 'www.', '', $objDirective->getDomain() ), DNS_CNAME );

			if( true == valArr( $arrstrDigCnameResult ) && true == isset( $arrstrDigCnameResult[0]['target'] ) ) {
				$strDnsInfo 	= rtrim( $strDnsInfo, ',' ) . ':CNAME:' . $arrstrDigCnameResult[0]['target'];
			}

			// set is PSI Hosted
			$objDirective->setIsPsiHosted( 0 );

			if( true == \Psi\CStringService::singleton()->stristr( $strWhoisResult, 'vanxp.com' ) || true == \Psi\CStringService::singleton()->stristr( $strWhoisResult, CConfig::get( 'entrata_suffix' ) ) ) {
				$objDirective->setIsPsiHosted( 1 );
			}

			$objDirective->setDnsInfo( $strDnsInfo );
		}

		$intDirectiveRecords = CDirectives::fetchDirectiveByDomainByDirectiveTypeId( $objDirective->getDomain(), CDirectiveType::WEBSITE_FULL_DOMAIN, $arrobjConnectDatabases[CONFIG_DB_HOST_CONNECT] );

		foreach( $arrobjConnectDatabases as $objConnectDatabase ) {
			if( false == valId( $intDirectiveRecords ) && false == $objDirective->insert( $intCurrentUserId, $objConnectDatabase ) ) {
				return false;
			}
		}

		return parent::insert( $intCurrentUserId, $objClientDatabase );
	}

	public function update( $intCurrentUserId, $objClientDatabase, $arrobjConnectDatabases = NULL ) {

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || false == valArr( $arrobjConnectDatabases ) ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			exit;
		}

		$strSql	= parent::update( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = true );

		if( false === $strSql ) return true;
		if( false == $this->executeSql( $strSql, $this, $objClientDatabase ) ) return false;

		foreach( $arrobjConnectDatabases as $objConnectDatabase ) {
			// update domain from directives table
			$objDirective	= CDirectives::fetchDirectiveByWebsiteDomainIdByDirectiveTypeIdByByCid( $this->getId(), CDirectiveType::WEBSITE_FULL_DOMAIN, $this->getCid(), $arrobjConnectDatabases[CONFIG_DB_HOST_CONNECT] );

			if( false == valObj( $objDirective, 'CDirective' ) ) {
				return false;
			}

			$objDirective->setDomain( \Psi\CStringService::singleton()->strtolower( $this->getWebsiteDomain() ) );
			$objDirective->setWebsiteId( $this->getWebsiteId() );

			if( false == $objDirective->update( $intCurrentUserId, $objConnectDatabase ) ) {
				return false;
			}
		}
		return true;
	}

	public function insertOrUpdate( $intCurrentUserId, $objClientDatabase, $arrobjConnectDatabases = NULL ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objClientDatabase, $arrobjConnectDatabases );
		} else {
			return $this->update( $intCurrentUserId, $objClientDatabase, $arrobjConnectDatabases );
		}
	}

	public function delete( $intCurrentUserId, $objClientDatabase, $arrobjConnectDatabases = NULL, $boolIsSubDomain = true, $objDnsDatabase = NULL, $boolIsSoftDelete = true ) {
		if( false == valObj( $objClientDatabase, 'CDatabase' ) || CDatabaseType::CLIENT != $objClientDatabase->getDatabaseTypeId() || false == valArr( $arrobjConnectDatabases ) ) {
			trigger_error( 'Failed to load database object', E_USER_ERROR );
			exit;
		}

		if( false == parent::delete( $intCurrentUserId, $objClientDatabase ) ) return false;

		// Delete from connect database
		foreach( $arrobjConnectDatabases as $objConnectDatabase ) {
			$objDirective = CDirectives::fetchDirectiveByWebsiteDomainIdByDirectiveTypeIdByByCid( $this->getId(), CDirectiveType::WEBSITE_FULL_DOMAIN, $this->getCid(), $arrobjConnectDatabases[CONFIG_DB_HOST_CONNECT] );

			if( true == valObj( $objDirective, 'CDirective' ) && false == $objDirective->delete( $intCurrentUserId, $objConnectDatabase, false,  $boolIsSoftDelete ) ) {
				return false;
			}
		}

		// Delete from DNS database
		$strMasterDomainForRequestedDomain = CDomainLibrary::getMasterDomainFromDomain( $this->getWebsiteDomain() );

		if( true == $boolIsSubDomain || false == isset( $strMasterDomainForRequestedDomain ) || false == valObj( $objDnsDatabase, 'CDatabase' ) ) {
			return true;
		}

		if( false == $objDnsDatabase->execute( 'DELETE FROM domains WHERE name = \'' . $this->getWebsiteDomain() . '\'' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'Domain could not be removed from DNS.  Please contact {%s,0} support at {%h,1}.', [ CONFIG_COMPANY_NAME, [ 'phone_number' => '8778269700' ] ] ), NULL ) );
				return false;
		}

		return true;
	}

	public function isDuplicate( $objDatabase, $objConnectDatabase ) {
		$boolIsDuplicate	= false;

		if( false == is_null( $objWebsiteDomain = CWebsiteDomains::createService()->fetchCustomWebsiteDomainByDomain( $this->getWebsiteDomain(), $objDatabase ) ) ) {
			if( $objWebsiteDomain->getId() == $this->getId() ) return false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'The website domain \'{%s,0}\' has already been taken.', [ $this->getWebsiteDomain() ] ) ) );
			$boolIsDuplicate	= true;
		}
		if( false == $boolIsDuplicate && 0 < $this->fetchCompetingDirectiveCount( $objConnectDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'The website domain \'{%s,0}\' has already been taken by another client.', [ $this->getWebsiteDomain() ] ) ) );
			$boolIsDuplicate	= true;
		}

		return $boolIsDuplicate;
	}

	public function isValidateDomainTransfer( $objClientDatabase, $objConnectDatabase, $arrobjConnectDatabases, $boolCheckDomainTransfer, $objDnsDatabase, $objCompanyUser, $objNewWebsite ) {
		$boolIsDuplicate	= false;
		$objWebsite			= \Psi\Eos\Entrata\CWebsites::createService()->fetchWebsiteByIdByCid( $this->getWebsiteId(), $this->getCid(), $objClientDatabase );

		if( false == valObj( $objWebsite, 'CWebsite' ) ) {
			echo json_encode( [ 'type' => 'error', 'message' => __( 'Failed to load {%s,0} object.', [ 'CWebsite' ] ) ] );
			exit;
		}

		if( true == $boolCheckDomainTransfer || false == is_null( $objWebsite->getDeletedBy() ) ) {
			if( $objNewWebsite->getId() != $objWebsite->getId() ) {
				$arrobjWebsiteSubdomains = CWebsiteDomains::createService()->fetchWebsiteSubDomainsByWebsiteDomainIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjWebsiteSubdomains ) ) {
					echo json_encode( [ 'type' => 'error', 'message' => __( 'Please delete or transfer all subdomain under this domain.' ) ] );
					exit;
				}
			}

			$strOldWebsiteDomainName	= 'Deleted_Website_Domain' . '::' . $this->getWebsiteDomain();
			$objNewPrimaryDomain		= NULL;

			$objWebsite->setWebsiteOldFullDomain( $this->getWebsiteDomain() );

			if( 1 == $this->getIsPrimary() ) {
				$arrobjWebsiteDomains	= $objWebsite->fetchWebsiteDomains( $objClientDatabase );

				if( true == valArr( $arrobjWebsiteDomains, 2 ) ) {
					do{
						$objNewPrimaryDomain = array_shift( $arrobjWebsiteDomains );
					} while ( $objNewPrimaryDomain->getId() == $this->getId() );

					$objNewPrimaryDomain->setIsPrimary( 1 );
					$objWebsite->setWebsiteCurrentFullDomain( $objNewPrimaryDomain->getWebsiteDomain() );
				} else {
					$this->setIsPrimary( 1 );
				}
			}

			switch( NULL ) {
				default:
					$boolIsValid = true;
					if( true == valObj( $objNewPrimaryDomain, 'CWebsiteDomain' ) ) {
						$boolIsValid &= $objNewPrimaryDomain->validate( VALIDATE_UPDATE, $objClientDatabase, $objConnectDatabase );
					}

					if( false == $boolIsValid ) {
						break 1;
					}

					$objClientDatabase->begin();
					$objDnsDatabase->begin();

					foreach( $arrobjConnectDatabases as $objDatabase ) {
						$objDatabase->begin();
					}

					if( true == valObj( $objNewPrimaryDomain, 'CWebsiteDomain' ) && false == $objNewPrimaryDomain->update( $objCompanyUser->getId(), $objClientDatabase, $arrobjConnectDatabases ) ) {
						$objClientDatabase->rollback();
						$objDnsDatabase->rollback();

						foreach( $arrobjConnectDatabases as $objDatabase ) {
							$objDatabase->rollback();
						}
						break 1;
					}

					$strTableName 	= 'website_info';
					$objTableLog 	= new CTableLog();
					$objTableLog->insert( $objCompanyUser->getId(), $objClientDatabase, $strTableName, $this->getWebsiteId(), 'UPDATE', $this->getCid(), $strOldWebsiteDomainName, NULL, __( 'Website domain has been deleted' ) );
					unset( $objTableLog );
			}
		} else {
			$arrstrWebsiteDetails = [
				'website_name'   => $objWebsite->getName(),
				'website_id'     => $objWebsite->getId(),
				'website_status' => $objWebsite->getIsDisabled()
			];

			$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'The website domain \'{%s,0}\' has already been taken.', [ $this->getWebsiteDomain() ] ), NULL, $arrstrWebsiteDetails ) );
			$boolIsDuplicate = true;
		}

		if( 0 < $this->fetchCompetingDirectiveCount( $objConnectDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'The website domain \'{%s,0}\' has already been taken.', [ $this->getWebsiteDomain() ] ) ) );
			$boolIsDuplicate = true;
		}

		return $boolIsDuplicate;
	}

	public function isExist( $strDomainName, $objDatabase, $objConnectDatabase ) {

		$boolIsExist	= false;
		$intDomainCount	= CWebsiteDomains::createService()->fetchRowCount( ' WHERE lower( website_domain ) = lower( \'' . $strDomainName . '\')', 'website_domains', $objDatabase );

		if( 0 < $intDomainCount ) $boolIsExist = true;

		if( false == $boolIsExist ) {
			$intCount = CDirectives::fetchDirectiveByDomainByDirectiveTypeId( $strDomainName, CDirectiveType::WEBSITE_FULL_DOMAIN, $objConnectDatabase );

			if( 0 < $intCount )	$boolIsExist = true;
		}

		return $boolIsExist;
	}

	public function fetchDirective( $objConnectDatabase ) {
		return CDirectives::fetchDirectiveByWebsiteDomainIdByCid( $this->getId(), $this->getCid(), $objConnectDatabase );
	}

	public function updateWebsiteDomainClientIdWebsiteIdByCid( $intUpdateToClientId, $intClientId, $intWebsiteId, $intCurrentUserId, $objDatabase ) {

		$strSql = 'UPDATE
						public.website_domains 
					SET
						cid = ' . ( int ) $intUpdateToClientId . ', 
						website_id = ' . ( int ) $intWebsiteId . ', 
						is_primary = ' . $this->getIsPrimary() . ',
						is_secure  = ' . $this->sqlIsSecure() . ',
						updated_by = ' . ( int ) $intCurrentUserId . ', 
						updated_on = NOW(),
						created_by = ' . ( int ) $intCurrentUserId . '  
					WHERE
						id = ' . ( int ) $this->sqlId() . '
					AND
						cid = ' . ( int ) $intClientId;

		if( false == $this->executeSql( $strSql, $this, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function deleteWebsiteDomainKeepingRecords( $intCurrentUserId, $objDatabase ) {
		if( false == parent::delete( $intCurrentUserId, $objDatabase ) ) return false;

		return true;
	}

	public function setCertificateSigningRequestEncrypted( $objSigningRequest ) {
		if( true == valObj( $objSigningRequest, \AcmePhp\Ssl\CertificateRequest::class ) ) {
			$this->setCertificateSigningRequest( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( serialize( $objSigningRequest ), CConfig::get( 'sodium_key_ssl_certificate' ) ) );
		}
	}

	public function getCertificateSigningRequestDecrypted() {
		if( false == valStr( $this->getCertificateSigningRequest() ) ) {
			return NULL;
		}
		return unserialize( \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCertificateSigningRequest(), CConfig::get( 'sodium_key_ssl_certificate' ), [ 'legacy_secret_key' => CConfig::get( 'key_encrypt_decrypt_file' ) ] ) );
	}

}
?>