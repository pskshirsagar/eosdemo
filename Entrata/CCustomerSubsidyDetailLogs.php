<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidyDetailLogs
 * Do not add any new functions to this class.
 */

class CCustomerSubsidyDetailLogs extends CBaseCustomerSubsidyDetailLogs {

	public static function fetchRecentCustomerSubsidyDetailLogByCustomerIdByIdByCid( $intCustomerId, $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						csdl.*
					FROM
						customer_subsidy_detail_logs csdl
					WHERE
						csdl.cid = ' . ( int ) $intCid . '
						AND csdl.customer_id = ' . ( int ) $intCustomerId . '
						AND csdl.customer_subsidy_detail_id = ' . ( int ) $intId . '
						AND csdl.is_post_date_ignored = 0
						ORDER BY csdl.log_datetime DESC LIMIT 1 ';

		return self::fetchCustomerSubsidyDetailLog( $strSql, $objDatabase );
	}
}
?>