<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserPropertyGroups
 * Do not add any new functions to this class.
 */

class CCompanyUserPropertyGroups extends CBaseCompanyUserPropertyGroups {

    public static function fetchCompanyUserPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return self::fetchObjects( $strSql, 'CCompanyUserPropertyGroup', $objDatabase, $boolIsReturnKeyedArray );
    }

    public static function fetchCompanyUserPropertyGroup( $strSql, $objDatabase ) {
        return self::fetchObject( $strSql, 'CCompanyUserPropertyGroup', $objDatabase );
    }

    public static function fetchSimplePropertyGroupIdsByCidByCompanyUserId( $intCid, $intCompanyUserId, $objDatabase ) {
		$strSql = 'SELECT
						property_group_id
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON pg.cid = cupg.cid AND pg.id = cupg.property_group_id
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND cupg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

	    $arrmixPropertyGroups = fetchData( $strSql, $objDatabase );

		$arrintPropertyGroupIds = [];

		if( true == valArr( $arrmixPropertyGroups ) ) {
			foreach( $arrmixPropertyGroups as $arrintPropertyGroup ) {
				$arrintPropertyGroupIds[$arrintPropertyGroup['property_group_id']] = $arrintPropertyGroup['property_group_id'];
			}
		}
		return $arrintPropertyGroupIds;
    }

    public static function fetchAllSimplePropertiesFromCompanyUserPropertyGroupByCid( $intCid, $objDatabase, $intClusterId = CCluster::RAPID ) {

    	$strJoinSql = '';
    	if( CCluster::RAPID == $intClusterId ) {
		    $strJoinSql = 'JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )';
	    }

    	$strSql = 'SELECT
						pga.property_id,
						p.property_name,
						array_to_string( array_agg( DISTINCT cu.id ), \',\') AS company_employee_ids
					FROM
						company_user_property_groups cupg
				 		JOIN company_users cu ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
    					JOIN property_group_associations pga ON ( pga.cid = cupg.cid AND pga.property_group_id = cupg.property_group_id )
    					' . $strJoinSql . '
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
					GROUP BY
						pga.property_id,
						p.property_name';

	    return fetchData( $strSql, $objDatabase );
    }

	public static function fetchCompanyUserPropertiesByCidByCompanyUserId( $intCid, $intCompanyUserId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						view_company_user_properties cup
					WHERE
						cup.cid = ' . ( int ) $intCid . '
						AND cup.company_user_id = ' . ( int ) $intCompanyUserId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupByCidByPropertyIdByCompanyEmployeeId( $intCid, $intPropertyId, $intCompanyEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						cupg.*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON pg.cid = cupg.cid AND pg.id = cupg.property_group_id
						JOIN property_group_associations pga ON pga.cid = pg.cid AND pga.property_group_id = pg.id
						JOIN property_group_types pgt ON pgt.cid = pg.cid AND pgt.id = pg.property_group_type_id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL
						JOIN company_users cu ON cu.cid = cupg.cid AND cu.id = cupg.company_user_id AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND cu.company_employee_id = ' . ( int ) $intCompanyEmployeeId . '
						AND pgt.system_code = \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\'
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
					LIMIT 1';

		return self::fetchCompanyUserPropertyGroup( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupByCidByPropertyIdByCompanyUserId( $intCid, $intPropertyId, $intCompanyUserId, $objDatabase ) {

		$strSql = 'SELECT
						cupg.*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON pg.cid = cupg.cid AND pg.id = cupg.property_group_id
						JOIN property_group_associations pga ON pga.cid = pg.cid AND pga.property_group_id = pg.id
						JOIN property_group_types pgt ON pgt.cid = pg.cid AND pgt.id = pg.property_group_type_id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND cupg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND pgt.system_code = \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\'
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
					LIMIT 1';

		return self::fetchCompanyUserPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchCompanyUserPropertyGroupsByCidByPropertyIdsByCompanyUserId( $intCid, $arrintPropertyIds, $intCompanyUserId, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						cupg.*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON pg.cid = cupg.cid AND pg.id = cupg.property_group_id
						JOIN property_group_associations pga ON pga.cid = pg.cid AND pga.property_group_id = pg.id
						JOIN property_group_types pgt ON pgt.cid = pg.cid AND pgt.id = pg.property_group_type_id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND cupg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND pgt.system_code = \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\'
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
					LIMIT 1';

		return self::fetchCompanyUserPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchComplexCompanyUserPropertiesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cup.*,
						cu.username,
						ce.name_first,
						ce.name_last,
						cu.company_employee_id
		 			FROM
						view_company_user_properties cup,
						company_users cu,
						company_employees ce
					WHERE
						cup.company_user_id = cu.id
						AND cup.cid = cu.cid
						AND cu.company_employee_id = ce.id
						AND cu.company_employee_id = ce.id
						AND cu.cid = ce.cid
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cup.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEnabledCompanyUserPropertyGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		$strSql = 'SELECT
						cupg.*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON pg.cid = cupg.cid AND pg.id = cupg.property_group_id
						JOIN property_group_types pgt ON pgt.cid = pg.cid AND pgt.id = pg.property_group_type_id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL
					 	LEFT JOIN property_group_associations pga ON pga.cid = pg.cid AND pga.property_group_id = pg.id AND pgt.system_code = \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\'
						LEFT JOIN properties p ON p.cid = pg.cid AND p.id = pga.property_id ' . ( ( false == $boolShowDisabledData ) ? 'AND p.is_disabled = 0' : '' ) . '
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND cupg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND ( pgt.system_code IS NULL OR pgt.system_code != \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\' OR p.id IS NOT NULL )
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL;';

		return self::fetchCompanyUserPropertyGroups( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchCompanyUserPropertyGroupsByCidByPropertyGroupId( $intCid, $arrintDeactivatedPropertyIds, $objClientDatabase ) {

		$strSql	= 'SELECT
						*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND cupg.property_group_id IN ( ' . implode( ', ', $arrintDeactivatedPropertyIds ) . ' )';

		return self::fetchCompanyUserPropertyGroups( $strSql, $objClientDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchCompanyUserPropertyGroupsByCidByCompanyUserId( $intCid, $intCompanyUserId, $objClientDatabase ) {

		$strSql	= 'SELECT
						*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND cupg.company_user_id = ' . ( int ) $intCompanyUserId;

		return self::fetchCompanyUserPropertyGroups( $strSql, $objClientDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchInheritedCompanyUserPropertyGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql	= 'SELECT
						*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND is_inherited = true';

		return parent::fetchCompanyUserPropertyGroups( $strSql, $objDatabase );
	}

	public static function fetchIndividualCompanyUserPropertyGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql	= 'SELECT
						cupg.*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND cupg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cupg.is_inherited = false';

		return parent::fetchCompanyUserPropertyGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cupg.*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cupg.cid =' . ( int ) $intCid;

		return self::fetchCompanyUserPropertyGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPropertyGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cupg.*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cupg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cupg.cid = ' . ( int ) $intCid;

		return self::fetchCompanyUserPropertyGroups( $strSql, $objDatabase );
	}

	public static function fetchIndividualCompanyUserPropertyGroupByPropertyGroupIdByCompanyUserIdByCid( $intPropertyGroupId, $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql	= 'SELECT
						cupg.*
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cupg.cid = ' . ( int ) $intCid . '
						AND cupg.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cupg.property_group_id = ' . ( int ) $intPropertyGroupId . '
						AND cupg.is_inherited = FALSE
					LIMIT 1';

		return parent::fetchCompanyUserPropertyGroup( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPropertiesByCidByCompanyUserIds( $intCid, $arrintCompanyUserIds, $objDatabase ) {
		if( false == ( $arrintCompanyUserIds = getIntValuesFromArr( $arrintCompanyUserIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						view_company_user_properties 
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id IN ( ' . sqlIntImplode( $arrintCompanyUserIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function rebuildPropertyGroupPermissionsByCidByCompanyUserIds( $intCid, $arrintCompanyUserIds, $intCurrentUserId, $objDatabase ) {

    	if( false == valId( $intCid ) || false == valArr( $arrintCompanyUserIds ) ) {
    		return false;
	    }

		$strSql = 'SELECT
						*
					FROM
						rebuild_company_user_property_group_permissions( ' . ( int ) $intCid . ', ARRAY[' . implode( ', ', $arrintCompanyUserIds ) . '], ' . ( int ) $intCurrentUserId . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyGroupIdsForHistoricalDataByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						p.id AS property_group_id
					FROM
						properties p
						JOIN property_products ppl ON ( ppl.cid = p.cid AND ppl.property_id = p.id  )
						JOIN view_company_user_properties cup ON ( cup.cid = p.cid AND cup.property_id = p.id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND	p.is_disabled = 1
						AND ppl.ps_product_id = ' . CPsProduct::HISTORICAL_ACCESS;

		$arrmixPropertyGroups = fetchData( $strSql, $objDatabase );

		$arrintPropertyGroupIds = [];

		if( true == valArr( $arrmixPropertyGroups ) ) {
			foreach( $arrmixPropertyGroups as $arrintPropertyGroup ) {
				$arrintPropertyGroupIds[$arrintPropertyGroup['property_group_id']] = $arrintPropertyGroup['property_group_id'];
			}
		}
		return $arrintPropertyGroupIds;
	}

}
?>