<?php

class CScheduledTaskLog extends CBaseScheduledTaskLog {

	public $m_intDefaultScheduledTaskId;
	public $m_strLogValueTitle;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledTaskTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventSubTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorScheduledTaskLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduleDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getDefaultScheduledTaskId() {
		return $this->m_intDefaultScheduledTaskId;
	}

	public function getLogValueTitle() {
		return $this->m_strLogValueTitle;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['default_scheduled_task_id'] ) && $boolDirectSet ) {
			$this->set( 'm_intDefaultScheduledTaskId', trim( $arrmixValues['default_scheduled_task_id'] ) );
		} elseif( isset( $arrmixValues['default_scheduled_task_id'] ) ) {
			$this->setDefaultScheduledTaskId( $arrmixValues['default_scheduled_task_id'] );
		}
		return;
	}

	public function setLogValueTitle( $strLogValueTitle ) {
		$this->m_strLogValueTitle = $strLogValueTitle;
	}

	public function setDefaultScheduledTaskId( $intDefaultScheduledTaskId ) {
		$this->m_intDefaultScheduledTaskId = $intDefaultScheduledTaskId;
	}

}
?>