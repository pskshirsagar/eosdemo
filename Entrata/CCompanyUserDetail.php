<?php

class CCompanyUserDetail extends CBaseCompanyUserDetail {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valVacancyTermsAgreedOn() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

	public function valVacancySalesTermsAgreedOn() {
		$boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeadsPulledOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>