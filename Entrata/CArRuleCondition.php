<?php

class CArRuleCondition extends CBaseArRuleCondition {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArRuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinimumAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaximumAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConditionDetails() {
		$boolIsValid = true;

		$arrintConditionDetails = json_decode( $this->getConditionDetails(), true );

		foreach( $arrintConditionDetails as $strKey => $strValue ) {
			switch( $strKey ) {
				case CArRuleConditionType::OCCUPANCY_TYPE_IS:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one Occupancy type.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::MOVE_OUT_TYPE_IS:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select at least one Move out type.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::MOVE_OUT_REASON_IS:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select Move out reason.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::CANCELATION_REASON_IS:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select cancellation reason.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::TERMINATION_IS_DURING:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select termination month.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::MOVE_OUT_DATE_IS_BEFORE:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select date for move out date is before.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::MOVE_OUT_DATE_IS_AFTER:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select date for move out date is after.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::MOVE_OUT_DATE_IS_BETWEEN:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select dates for move out date is between.' ) ) );
						break;
					}

					$arrintMoveOutIsBetweenDates = explode( ',', $strValue );

					if( false == valStr( $arrintMoveOutIsBetweenDates[0] ) || false == valStr( $arrintMoveOutIsBetweenDates[1] ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please select min date and max date for move out is between.' ) ) );
						break;
					}

					if( $arrintMoveOutIsBetweenDates[0] > $arrintMoveOutIsBetweenDates[1] ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Move Out Date is Between max date should be greater than min date.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::MOVE_OUT_DATE_IS_MORE_THAN_DAYS_BEFORE_LEASE_END_DATE:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please enter days for move out is more than __days before lease end date.' ) ) );
						break;
					}
					if( 0 > $strValue ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Move out date is more than __ day(s) before lease end date should be greater than zero.' ) ) );
						break;

					}

					if( !( ( string ) round( $strValue ) == $strValue && false === \Psi\CStringService::singleton()->strpos( $strValue, '.' ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Move out date is more than __ day(s) before lease end date should not be decimal.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::CANCELATION_IS_DAYS_BEFORE_MOVE_IN_DATE:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please enter days for cancelation is __ days before move in date.' ) ) );
						break;
					}

					if( 0 > $strValue ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Cancelation is __ days before move in date should be greater than zero.' ) ) );
						break;
					}

					if( !( ( string ) round( $strValue ) == $strValue && false === \Psi\CStringService::singleton()->strpos( $strValue, '.' ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Cancelation is __ days before move in date should not be decimal.' ) ) );
						break;
					}
					break;

				case CArRuleConditionType::CANCELATION_IS_DAYS_AFTER_LEASE_SIGN_DATE:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please enter days for cancelation is __ days after lease sign date.' ) ) );
						break;
					}

					if( 0 > $strValue ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Cancelation is __ days after lease sign date should be greater than zero.' ) ) );
						break;

					}

					if( !( ( string ) round( $strValue ) == $strValue && false === \Psi\CStringService::singleton()->strpos( $strValue, '.' ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Cancelation is __ days after lease sign date should not be decimal.' ) ) );
						break;

					}
					break;

				case CArRuleConditionType::NOTICE_DATE_IS_LESS_THAN_BEFORE_MOVE_OUT_DATE:
					if( false == valStr( $strValue ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Please enter days for notice date is less than __ day(s) before move out date.' ) ) );
						break;

					}

					if( 0 > $strValue ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Notice Date is less than __ day(s) before move out date be should greater than zero.' ) ) );
						break;

					}

					if( !( ( string ) round( $strValue ) == $strValue && false === \Psi\CStringService::singleton()->strpos( $strValue, '.' ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'condition_details', __( 'Notice Date is less than __ day(s) before move out date should not be decimal.' ) ) );
						break;

					}
					break;

				default:
					NULL;
			}
		}

		if( false == $boolIsValid ) {
			return false;
		}
		return true;

	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valConditionDetails();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>