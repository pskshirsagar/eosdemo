<?php

class CPropertyPolicy extends CBasePropertyPolicy {

    public function valId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valPolicyName() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valMarketingName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyPolicy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>