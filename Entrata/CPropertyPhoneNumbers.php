<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPhoneNumbers
 * Do not add any new functions to this class.
 */

class CPropertyPhoneNumbers extends CBasePropertyPhoneNumbers {

	public static function fetchPropertyPhoneNumberByPropertyIdPhoneNumberTypeIdRemotePrimaryKeyByCid( $intPropertyId, $intPhoneNumberTypeId, $intCid, $strRemotePrimaryKey, $objDatabase ) {

		$strSql = 'SELECT * FROM
						property_phone_numbers
					WHERE property_id::int = ' . ( int ) $intPropertyId . '
						AND phone_number_type_id::int = ' . ( int ) $intPhoneNumberTypeId . '
						AND cid::int = ' . ( int ) $intCid . '
						AND lower(remote_primary_key) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strRemotePrimaryKey ) ) . '\'
					LIMIT 1';

		return self::fetchPropertyPhoneNumber( $strSql, $objDatabase );
	}

	public static function fetchPublishedPropertyPhoneNumbersByPropertyIdPhoneNumberTypeIdsByCid( $intPropertyId, $arrintPhoneNumberTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPhoneNumberTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						property_phone_numbers
					WHERE property_id::int = ' . ( int ) $intPropertyId . '
						AND phone_number_type_id::int IN ( ' . implode( ',', $arrintPhoneNumberTypeIds ) . ' )
						AND cid::int = ' . ( int ) $intCid . '
						AND is_published=1
						AND phone_number IS NOT NULL
					ORDER BY order_num';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyPhoneNumbersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT phone_number,phone_number_type_id FROM property_phone_numbers WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedPropertyPhoneNumbersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						property_phone_numbers
					WHERE property_id::int = ' . ( int ) $intPropertyId . '
						AND cid::int = ' . ( int ) $intCid . '
						AND phone_number_type_id IS NOT NULL
						AND is_published = 1
						AND phone_number IS NOT NULL
					ORDER BY order_num';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchPublishedPropertyPhoneNumbersWithDescTypeIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						property_phone_numbers
					WHERE property_id::int = ' . ( int ) $intPropertyId . '
						AND cid::int = ' . ( int ) $intCid . '
						AND phone_number_type_id IS NOT NULL
						AND is_published = 1
						AND phone_number IS NOT NULL
					ORDER BY phone_number_type_id ASC';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersByPropertyIdExcludePhoneNumberTypeIdsByCid( $intPropertyId, $arrintExcludePhoneNumberTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintExcludePhoneNumberTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						property_phone_numbers
					WHERE property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND phone_number_type_id NOT IN ( ' . implode( ',', $arrintExcludePhoneNumberTypeIds ) . ' )
					ORDER BY order_num';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdByCid( $arrintPropertyIds, $intPhoneNumberTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						property_phone_numbers
					WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND is_published=1
						AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdsByCid( $arrintPropertyIds, $arrintPhoneNumberTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == valArr( $arrintPhoneNumberTypeIds ) ) return NULL;

		$strSql = 'SELECT *
					 FROM
					 		property_phone_numbers
					WHERE
							property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND
							phone_number_type_id IN ( ' . implode( ',', $arrintPhoneNumberTypeIds ) . ' )
						AND
							cid = ' . ( int ) $intCid;

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchWebVisiblePropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdsByCid( $arrintPropertyIds, $arrintPhoneNumberTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == valArr( $arrintPhoneNumberTypeIds ) ) return NULL;

		$strSql = 'SELECT *
					 FROM
					 		property_phone_numbers
					WHERE
							property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND
							phone_number_type_id IN ( ' . implode( ',', $arrintPhoneNumberTypeIds ) . ' )
						AND
							cid = ' . ( int ) $intCid . '
						AND
							is_published = 1';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdByLeadSourceIdByCid( $arrintPropertyIds, $intPhoneNumberTypeId, $intLeadSourceId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( false == is_numeric( $intLeadSourceId ) ) {
			// return self::fetchPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdByCid( $arrintPropertyIds, $intPhoneNumberTypeId, $intCid, $objDatabase );

			return self::fetchPropertyPhoneNumbersByPropertyIdsByInternetListingServiceIdByCid( $arrintPropertyIds, CInternetListingService::PROSPECT_PORTAL, $intPhoneNumberTypeId, $intCid, $objDatabase );
		} else {

			$strSql = 'SELECT
						ppn.id,
						ppn.cid,
						ppn.property_id,
						ppn.phone_number_type_id,
						ppn.remote_primary_key,
						ppn.marketing_name,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
							THEN pls.phone_number
							ELSE ppn.phone_number
						END as phone_number,

						CASE
							WHEN ( pls.phone_number IS NOT NULL )
							THEN NULL
							ELSE ppn.extension
						END as extension,
						ppn.contact_name,
						ppn.is_published,
						ppn.order_num,
						ppn.imported_on,
						ppn.updated_by,
						ppn.updated_on,
						ppn.created_by,
						ppn.created_on
					FROM
						property_phone_numbers ppn
						LEFT OUTER JOIN property_lead_sources pls
							ON (ppn.property_id = pls.property_id AND ppn.cid = pls.cid AND pls.lead_source_id = ' . ( int ) $intLeadSourceId . ' AND pls.is_published = 1 AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL AND ppn.cid = ' . ( int ) $intCid . ' )
					WHERE
						ppn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . '
						AND ppn.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ppn.cid = ' . ( int ) $intCid . ';';

			return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
		}
	}

	public static function fetchPropertyLeadSourcePhoneNumbersByPropertyIdsByInternetListingServiceIdByCid( $arrintPropertyIds, $intInternetListingServiceId, $intCid, $objProspectPortalDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						pls.id,
						pls.cid,
						pls.property_id,
						pls.phone_number as phone_number,
						pls.lead_source_id,
						ls.internet_listing_service_id
					FROM
						property_lead_sources pls
						JOIN lead_sources ls ON (pls.lead_source_id = ls.id AND pls.is_published = 1 AND pls.cid = ls.cid)
					WHERE
						ls.internet_listing_service_id = ' . ( int ) $intInternetListingServiceId . '
						AND pls.phone_number IS NOT NULL
						AND pls.deleted_by IS NULL
						AND pls.deleted_on IS NULL
						AND pls.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND pls.cid = ' . ( int ) $intCid;

		return self::fetchPropertyPhoneNumbers( $strSql, $objProspectPortalDatabase );
	}

	public static function fetchWebVisiblePropertyPhoneNumbersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if ( false == isset ( $intPropertyId ) ) return NULL;
		$strSql = 'SELECT * FROM property_phone_numbers WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND is_published = 1 ORDER BY order_num ';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchWebVisiblePropertyPhoneNumbersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT * FROM property_phone_numbers WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $intCid . ' AND is_published = 1';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumberByIdByPropertyIdByCid( $intPropertyPhoneNumberId, $intPropertyId, $intCid, $objDatabase ) {

		if ( false == isset ( $intPropertyId ) ) return NULL;
		$strSql = 'SELECT * FROM property_phone_numbers WHERE property_id = ' . ( int ) $intPropertyId . ' AND id = ' . ( int ) $intPropertyPhoneNumberId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyPhoneNumber( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumberByLeadSourceIdByPropertyIdByCid( $intLeadSourceId, $intPropertyId, $intCid, $objDatabase ) {

		if ( false == isset ( $intPropertyId ) || false == isset( $intLeadSourceId ) ) return NULL;

		$strSql = 'SELECT
						ppn.id,
						ppn.cid,
						ppn.property_id,
						ppn.phone_number_type_id,
						ppn.remote_primary_key,
						ppn.marketing_name,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
								THEN pls.phone_number
							ELSE ppn.phone_number
						END as phone_number,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
								THEN NULL
							ELSE ppn.extension
						END as extension,
						ppn.contact_name,
						ppn.is_published,
						ppn.order_num,
						ppn.imported_on,
						ppn.updated_by,
						ppn.updated_on,
						ppn.created_by,
						ppn.created_on
					FROM
						property_phone_numbers ppn
						LEFT OUTER JOIN property_lead_sources pls
							ON (ppn.property_id = pls.property_id AND ppn.cid = pls.cid AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL AND pls.lead_source_id = ' . ( int ) $intLeadSourceId . ')
					WHERE
						ppn.phone_number_type_id = ' . CPhoneNumberType::PRIMARY . '
						AND ppn.property_id = ' . ( int ) $intPropertyId . '
						AND ppn.cid = ' . ( int ) $intCid;

		return self::fetchPropertyPhoneNumber( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumberByLeadSourceIdByPropertyIdByInternetListingServiceIdByCid( $intLeadSourceId, $intPropertyId, $intInternetListingServiceId, $intCid, $objDatabase ) {

		if ( false == isset( $intPropertyId ) || false == isset( $intLeadSourceId ) ) return NULL;

		$strSql = 'SELECT
						ppn.id,
						ppn.cid,
						ppn.property_id,
						ppn.phone_number_type_id,
						ppn.remote_primary_key,
						ppn.marketing_name,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
								THEN pls.phone_number
							ELSE ppn.phone_number
						END as phone_number,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
								THEN NULL
							ELSE ppn.extension
						END as extension,
						ppn.contact_name,
						ppn.is_published,
						ppn.order_num,
						ppn.imported_on,
						ppn.updated_by,
						ppn.updated_on,
						ppn.created_by,
						ppn.created_on
					FROM
						property_phone_numbers ppn ';

					if( NULL != $intInternetListingServiceId ) {
						$strSql .= 'LEFT OUTER JOIN ( lead_sources ls LEFT OUTER JOIN property_lead_sources pls ON ( pls.cid = ls.cid AND pls.lead_source_id = ls.id AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL ) )
										ON ( ppn.property_id = pls.property_id AND ppn.cid = pls.cid AND pls.lead_source_id = ' . ( int ) $intLeadSourceId . ' AND pls.is_published = 1 )
									WHERE ls.internet_listing_service_id = ' . ( int ) $intInternetListingServiceId . ' AND ';
					} else {
						$strSql .= 'LEFT OUTER JOIN property_lead_sources pls
										ON ( ppn.property_id = pls.property_id AND ppn.cid = pls.cid AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL AND pls.lead_source_id = ' . ( int ) $intLeadSourceId . ' )
									WHERE ';
					}

			$strSql .= 'ppn.phone_number_type_id IN( ' . CPhoneNumberType::PRIMARY . ' ,' . CPhoneNumberType::OFFICE . ' )
						AND ppn.property_id = ' . ( int ) $intPropertyId . '
						AND ppn.cid = ' . ( int ) $intCid . '
					ORDER BY ppn.phone_number_type_id
					LIMIT 1';

		return self::fetchPropertyPhoneNumber( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumberByPropertyIdByInternetListingServiceIdByCid( $intPropertyId, $intInternetListingServiceId, $intCid, $objDatabase ) {

		if ( false == isset ( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						ppn.id,
						ppn.cid,
						ppn.property_id,
						ppn.phone_number_type_id,
						ppn.remote_primary_key,
						ppn.marketing_name,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
								THEN pls.phone_number
							ELSE ppn.phone_number
						END as phone_number,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
								THEN NULL
							ELSE ppn.extension
						END as extension,
						ppn.contact_name,
						ppn.is_published,
						ppn.order_num,
						ppn.imported_on,
						ppn.updated_by,
						ppn.updated_on,
						ppn.created_by,
						ppn.created_on
					FROM
						property_phone_numbers ppn ';

					if( NULL != $intInternetListingServiceId ) {
						if( $intInternetListingServiceId == CInternetListingService::MOBILE_PROSPECT_PORTAL ) {
							$strWhere = ' AND ls.internet_listing_service_id IN ( ' . ( int ) $intInternetListingServiceId . ', ' . CInternetListingService::PROSPECT_PORTAL . ' )';
						} else {
							$strWhere = ' AND ls.internet_listing_service_id = ' . ( int ) $intInternetListingServiceId;
						}
						$strSql .= 'LEFT OUTER JOIN ( lead_sources ls LEFT OUTER JOIN property_lead_sources pls ON ( pls.cid = ls.cid AND pls.lead_source_id = ls.id AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL' . $strWhere . ' ) )
									ON ( ppn.property_id = pls.property_id AND ppn.cid = pls.cid AND pls.is_published = 1 ) ';
					} else {
						$strSql .= 'LEFT OUTER JOIN property_lead_sources pls ON ( ppn.property_id = pls.property_id AND ppn.cid = pls.cid AND pls.is_published = 1 AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL ) ';
					}

		$strSql .= 'WHERE
						 ppn.phone_number_type_id IN( ' . CPhoneNumberType::PRIMARY . ' ,' . CPhoneNumberType::OFFICE . ' )
						 AND ppn.property_id = ' . ( int ) $intPropertyId . '
						 AND ppn.cid = ' . ( int ) $intCid . '
						 AND ppn.is_published = 1
					ORDER BY ppn.phone_number_type_id
					LIMIT 1';

		return self::fetchPropertyPhoneNumber( $strSql, $objDatabase );

	}

	public static function fetchPropertyPhoneNumbersByPropertyIdsByInternetListingServiceIdByCid( $arrintPropertyIds, $intInternetListingServiceId, $intPhoneNumberTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ppn.id,
						ppn.cid,
						ppn.property_id,
						ppn.phone_number_type_id,
						ppn.remote_primary_key,
						ppn.marketing_name,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
								THEN pls.phone_number
							ELSE ppn.phone_number
						END as phone_number,
						CASE
							WHEN ( pls.phone_number IS NOT NULL )
								THEN NULL
							ELSE ppn.extension
						END as extension,
						ppn.contact_name,
						ppn.is_published
					FROM
						property_phone_numbers ppn ';

					if( NULL != $intInternetListingServiceId ) {
						$strSql .= 'LEFT OUTER JOIN ( lead_sources ls LEFT OUTER JOIN property_lead_sources pls ON ( pls.cid = ls.cid AND pls.lead_source_id = ls.id AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL AND ls.internet_listing_service_id = ' . ( int ) $intInternetListingServiceId . ' ) )
									ON ( ppn.property_id = pls.property_id AND ppn.cid = pls.cid AND pls.is_published = 1 ) ';
					} else {
						$strSql .= 'LEFT OUTER JOIN property_lead_sources pls ON ( ppn.property_id = pls.property_id AND ppn.cid = pls.cid AND pls.is_published = 1 AND pls.deleted_by IS NULL AND pls.deleted_on IS NULL ) ';
					}

		$strSql .= 'WHERE
						ppn.phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . '
						AND ppn.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ppn.cid = ' . ( int ) $intCid;

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchSingleAdditionalPropertyPhoneNumberByPropertyIdByCid( $intPropertyId, $intPhoneNumberTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM property_phone_numbers WHERE property_id = ' . ( int ) $intPropertyId . ' AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY order_num LIMIT 1';

		return self::fetchPropertyPhoneNumber( $strSql, $objDatabase );
	}

	public static function fetchSinglePrimaryPropertyPhoneNumberByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM property_phone_numbers WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND phone_number_type_id = ' . CPhoneNumberType::PRIMARY . ' AND is_published = 1 ORDER BY order_num DESC LIMIT 1';

		return self::fetchPropertyPhoneNumber( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumberByPropertyIdsByPhoneNumberTypeIdByCids( $arrintPropertyIds, $intPhoneNumberTypeId, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return;

		$strSql = 'SELECT * FROM property_phone_numbers WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ') AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . ' AND cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchPublishedPropertyPhoneNumberByPropertyIdsByPhoneNumberTypeIdByCid( $arrintPropertyIds, $intPhoneNumberTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT * FROM property_phone_numbers WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ') AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . ' AND is_published = 1 AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchPreExistingPropertyPhoneNumbersCountByPropertyIdByPhoneNumberTypeIdByCid( $intPropertyId, $intPhoneNumberTypeId, $intId, $intCid, $objDatabase ) {

		$strWhereSql = ' WHERE property_id = ' . ( int ) $intPropertyId . ' AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . ' AND cid = ' . ( int ) $intCid . ' AND id <>' . ( int ) $intId . ' LIMIT 1';

		return self::fetchPropertyPhoneNumberCount( $strWhereSql, $objDatabase );
	}

	public static function fetchPreExistingPropertyPhoneNumbersCountByPropertyIdByPhoneNumberTypeIdByPhoneNumberByCid( $intPropertyId, $intPhoneNumberTypeId, $intId, $strPhoneNumber, $intCid, $objDatabase ) {

		$strWhereSql = ' WHERE property_id = ' . ( int ) $intPropertyId . ' AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . ' AND phone_number = \'' . $strPhoneNumber . '\' AND cid = ' . ( int ) $intCid . ' AND id <>' . ( int ) $intId . ' LIMIT 1';

		return self::fetchPropertyPhoneNumberCount( $strWhereSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumberByPropertyIdByPhoneNumberTypeIdByCid( $intPropertyId, $intPhoneNumberTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM property_phone_numbers WHERE property_id = ' . ( int ) $intPropertyId . ' AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . ' AND cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchPropertyPhoneNumber( $strSql, $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		return self::fetchPropertyPhoneNumbers( 'SELECT * FROM property_phone_numbers WHERE cid = ' . ( int ) $intCid . ' AND property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )', $objDatabase );
	}

	public static function fetchPropertyPhoneNumbersFieldsByPropertyIdsByPhoneNumberTypeIdsByCid( $arrintPropertyIds, $arrintPhoneNumberTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintPhoneNumberTypeIds ) ) return NULL;

		$strSql = 'SELECT
						property_id,
						phone_number,
						phone_number_type_id
					FROM
						property_phone_numbers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND phone_number_type_id IN ( ' . implode( ', ', $arrintPhoneNumberTypeIds ) . ' )
						AND is_published = 1
						AND phone_number IS NOT NULL
					ORDER BY
						order_num';

		$arrmixData			= fetchData( $strSql, $objClientDatabase );
		$arrmixFinalData	= [];

		foreach( $arrmixData as $arrmixChunk ) {
			$arrmixFinalData[$arrmixChunk['property_id']][$arrmixChunk['phone_number_type_id']] = $arrmixChunk['phone_number'];
		}

		return $arrmixFinalData;
	}

	public static function fetchWebVisiblePropertyPhoneNumberByPropertyIdByCid( $intPropertyId,  $intCid, $objDatabase ) {
		$strSql = ' SELECT
						DISTINCT ON ( property_id )
						*,
						CASE WHEN phone_number_type_id = ' . CPhoneNumberType::MAINTENANCE . '
						THEN 0
						ELSE 1
						END AS phone_number_priority
					FROM
						property_phone_numbers
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND	phone_number_type_id IN ( ' . CPhoneNumberType::MAINTENANCE . ', ' . CPhoneNumberType::OFFICE . ' )
					  	AND	cid = ' . ( int ) $intCid . '
					  	AND phone_number IS NOT NULL
					 	AND	is_published = 1
					ORDER BY
					  	 property_id, phone_number_priority';

		return CPropertyPhoneNumbers::fetchPropertyPhoneNumber( $strSql, $objDatabase );
	}

	public static function fetchWebVisiblePropertyPhoneNumbersByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT * FROM property_phone_numbers WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid IN (' . implode( ',', $arrintCids ) . ') AND is_published = 1';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyPhoneNumbersByPropertyIdsByPhoneNumberTypeIdByCid( $arrintPropertyIds, $intPhoneNumberTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_phone_numbers
					WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND is_published=1
						AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyPhoneNumberByPhoneNumberTypeIdByPropertyIdsByCids( $intPhoneNumberTypeId, $arrintPropertyIds, $arrintCids, $objDatabase, $boolIsFromInspections = false ) {

		$strSql = 'SELECT 
						phone_number,
						cid,
						property_id
					FROM
						property_phone_numbers
					WHERE 
						cid IN( ' . implode( ',', $arrintCids ) . ' )
						AND property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND phone_number_type_id = ' . ( int ) $intPhoneNumberTypeId;

		$arrmixData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) ) {
			return ( true == $boolIsFromInspections ) ? $arrmixData[0]['phone_number'] : $arrmixData;
		}
		return NULL;
	}

	public static function fetchPropertyPhoneNumbersByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						property_phone_numbers 
					WHERE 
						property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ') 
						AND cid IN ( ' . implode( ',', $arrintCids ) . ') 
						AND phone_number IS NOT NULL 
						AND is_published = 1
					ORDER BY phone_number_type_id DESC';

		return self::fetchPropertyPhoneNumbers( $strSql, $objDatabase );
	}

}
?>