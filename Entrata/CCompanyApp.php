<?php

class CCompanyApp extends CBaseCompanyApp {

	protected $m_intAppinstalled;

	protected $m_strCompanyName;
	protected $m_strRwxDomain;
	protected $m_strAppName;
	protected $m_strAppURL;
	protected $m_strClientId;
	protected $m_strAuthEmail;
	protected $m_strClientSecret;
    protected $m_intOauthClientId;

    protected $m_arrobjPropertyApps;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAppId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valName() {
        $boolIsValid = true;
        if( false == isset( $this->m_strName ) || 0 == strlen( $this->m_strName ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
        }
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUrl() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInstallDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExposeUserList() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActivateQuickLink() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowAllProperties() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRequestedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActivatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function getCompanyName() {
    	return $this->m_strCompanyName;
    }

    public function getRwxDomain() {
    	return $this->m_strRwxDomain;
    }

    public function getAppName() {
    	return $this->m_strAppName;
    }

    public function getAppURL() {
    	return $this->m_strAppURL;
    }

    public function getOrFetchClientId( $objDatabase ) {
    	if( empty( $this->m_strClientId ) ) {
    		$objApp = \Psi\Eos\Admin\CApps::createService()->fetchAppById( $this->getAppId(), $objDatabase );

    		if( false == valObj( $objApp, 'CApp' ) ) {
				return NULL;
    		}

    		$this->m_strClientId = $objApp->getClientId();
    	}

    	return $this->m_strClientId;
    }

    public function getClientSecret() {
    	return $this->m_strClientSecret;
    }

    public function getPropertyApps() {
    	return $this->m_arrobjPropertyApps;
    }

	public function getAuthEmail() {
		return $this->m_strAuthEmail;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['company_name'] ) ) 	$this->setCompanyName( $arrmixValues['company_name'] );
    	if( true == isset( $arrmixValues['rwx_domain'] ) ) 	$this->setRwxDomain( $arrmixValues['rwx_domain'] );
    	if( true == isset( $arrmixValues['app_name'] ) ) 		$this->setAppName( $arrmixValues['app_name'] );
    	if( true == isset( $arrmixValues['app_url'] ) ) 		$this->setAppURL( $arrmixValues['app_url'] );
    	if( true == isset( $arrmixValues['client_id'] ) ) 		$this->setClientId( $arrmixValues['client_id'] );
    	if( true == isset( $arrmixValues['client_secret'] ) ) 	$this->setClientSecret( $arrmixValues['client_secret'] );
    	if( true == isset( $arrmixValues['oauth_client_id'] ) ) $this->setOauthClientId( $arrmixValues['oauth_client_id'] );
	    if( true == isset( $arrmixValues['auth_email'] ) )      $this->setAuthEmail( $arrmixValues['auth_email'] );

    	return;
    }

    public function setCompanyName( $strCompanyName ) {
    	$this->m_strCompanyName = $strCompanyName;
    }

    public function setRwxDomain( $strRwxDomain ) {
    	$this->m_strRwxDomain = $strRwxDomain;
    }

    public function setAppName( $strAppName ) {
    	$this->m_strAppName = $strAppName;
    }

    public function setAppURL( $strAppURL ) {
    	$this->m_strAppURL = $strAppURL;
    }

    public function setClientId( $strClientId ) {
    	$this->m_strClientId = $strClientId;
    }

    public function setClientSecret( $strClientSecret ) {
    	$this->m_strClientSecret = $strClientSecret;
    }

    public function setOauthClientId( $intOauthClientId ) {
    	$this->m_intOauthClientId = $intOauthClientId;
    }

    public function setPropertyApps( $arrobjPropertyApps ) {
    	$this->m_arrobjPropertyApps = $arrobjPropertyApps;
    }

	public function setAuthEmail( $strAuthEmail ) {
		$this->m_strAuthEmail = CStrings::strTrimDef( $strAuthEmail, 240, NULL, true );
	}

    public function getOauthClientId() {
    	return $this->m_intOauthClientId;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valAppId();
            	$boolIsValid &= $this->valUrl();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valAppId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function createPropertyApp() {

    	$objPropertyApp = new CPropertyApp();
    	$objPropertyApp->setCid( $this->getCid() );
    	$objPropertyApp->setCompanyAppId( $this->getId() );

    	return $objPropertyApp;
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( true == is_null( $this->getInstallDatetime() ) ) {
    		$this->setInstallDatetime( date( 'm/d/Y H:i:s' ) );
    	}

    	return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    }

	public function fetchPropertyApps( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyApps::createService()->fetchPropertyAppsByCompanyAppIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAppsByPropertyIds( $arrintUnInstallPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyApps::createService()->fetchPropertyAppsByCompanyAppIdByPropertyIdsByCid( $this->getId(), $arrintUnInstallPropertyIds, $this->getCid(), $objDatabase );
	}

}
?>