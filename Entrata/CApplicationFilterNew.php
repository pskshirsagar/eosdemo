<?php

class CApplicationFilterNew extends CBaseApplicationFilterNew {

	protected $m_boolIncludeLeadsWithNullLeasingAgent;

	protected $m_arrintPropertyIds;
	protected $m_arrintPropertyFloorplanIds;
	protected $m_arrintApplicationStageStatusIds;
	protected $m_arrintScreeningResponseTypeIds;
	protected $m_arrintLeaseIntervalTypeIds;
	protected $m_arrintLeadSourceIds;
	protected $m_arrintPsProductIds;
	protected $m_arrintCompanyEmployeeIds;
	protected $m_arrintApplicationIds;
	protected $m_arrintCompanyApplicationIds;
	protected $m_arrintDesiredBedroomNumbers;
	protected $m_arrintDesiredBathroomNumbers;

	protected $m_arrobjEligibleApplicationFilters;
	protected $m_arrobjEligibleApplicationStageStatuses;
	protected $m_arrobjEligiblePsProducts;
	protected $m_arrobjEligibleLeadSources;
	protected $m_arrobjEligibleLeaseIntervalTypes;
	protected $m_arrobjEligiblePropertyFloorplans;
	protected $m_arrobjEligibleCompanyEmployees;

	protected $m_arrobjPsProducts;

	protected $m_arrstrEligibleProperties;

	protected $m_intPageNumber;
	protected $m_intCountPerPage;

	protected $m_intIsShowLastContact;

	protected $m_strUnitNumbers;
	protected $m_strSortBy;
	protected $m_strSortDirection;

	const DEFAULT_PAGE_NO 			= 1;
	const DEFAULT_COUNT_PER_PAGE 	= 25;

	const LEADS_SELECTION_TYPE_SELECT_THIS_PAGE 	= 1;
	const LEADS_SELECTION_TYPE_SELECT_ALL_PAGES 	= 2;
	const LEADS_SELECTION_TYPE_DESELECT_ALL_PAGE 	= 3;

    public static $_REQUEST_FORM_APPLICATION_SEARCH	= [
        'id'									=> NULL,
        'property_ids'  						=> NULL,
        'property_floorplan_ids' 			=> NULL,
        'application_stage_status_ids'		=> NULL,
        'screening_response_type_ids'		=> NULL,
        'lease_interval_type_ids' 			=> NULL,
        'lead_source_ids'					=> NULL,
        'ps_product_ids'						=> NULL,
        'company_employee_ids'				=> NULL,
        'application_ids'					=> NULL,
        'filter_name'  						=> NULL,
        'quick_search'						=> NULL,
        'last_contact_min_days'  			=> NULL,
        'last_contact_max_days'  			=> NULL,
        'ad_delay_days'  					=> NULL,
        'desired_bedrooms'  					=> NULL,
        'desired_bathrooms'  				=> NULL,
        'desired_pets'  						=> NULL,
        'desired_rent_min'  					=> NULL,
        'desired_rent_max'  					=> NULL,
        'desired_occupants'  				=> NULL,
        'desired_floors'  					=> NULL,
        'application_start_date'  			=> NULL,
        'application_end_date'  				=> NULL,
        'move_in_date_min'  					=> NULL,
        'move_in_date_max'  					=> NULL,
        'unit_numbers'  						=> NULL,
        'move_in_date_min'  					=> NULL,
        'move_in_date_max'  					=> NULL,
        'sort_by'  							=> NULL,
        'sort_direction' 					=> NULL,
        'page_number'						=> NULL,
        'count_per_page'						=> NULL,
        'is_integrated'						=> NULL
    ];

	/**
	 * Set Functions
	 */

    public function setDefaults() {

    	$this->setSortDirection( 'DESC' );

    	$this->setPageNumber( self::DEFAULT_PAGE_NO );
    	$this->setCountPerPage( self::DEFAULT_COUNT_PER_PAGE );

    	$this->setPropertyIds( NULL );
    	$this->setPropertyFloorplanIds( NULL );
		$this->setApplicationStageStatusIds( NULL );
		$this->setScreeningResponseTypeIds( NULL );
		$this->setLeaseIntervalTypeIds( NULL );
		$this->setLeadSourceIds( NULL );
		$this->setPsProductIds( NULL );
		$this->setCompanyEmployeeIds( NULL );
		$this->setApplicationIds( NULL );
		$this->setCompanyApplicationIds( NULL );
		$this->setDesiredBedroomNumbers( NULL );
		$this->setDesiredBathroomNumbers( NULL );

        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet );

    	if( true == isset( $arrValues['unit_numbers'] ) ) 					$this->setUnitNumbers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_numbers'] ): $arrValues['unit_numbers'] );
    	if( true == isset( $arrValues['property_ids'] ) ) 					$this->setPropertyIds( $arrValues['property_ids'] );
    	if( true == isset( $arrValues['property_floorplan_ids'] ) ) 		$this->setPropertyFloorplanIds( $arrValues['property_floorplan_ids'] );
    	if( true == isset( $arrValues['application_stage_status_ids'] ) ) 	$this->setApplicationStageStatusIds( $arrValues['application_stage_status_ids'] );
    	if( true == isset( $arrValues['screening_response_type_ids'] ) ) 	$this->setScreeningResponseTypeIds( $arrValues['screening_response_type_ids'] );
    	if( true == isset( $arrValues['lease_interval_type_ids'] ) ) 		$this->setLeaseIntervalTypeIds( $arrValues['lease_interval_type_ids'] );
    	if( true == isset( $arrValues['lead_source_ids'] ) ) 				$this->setLeadSourceIds( $arrValues['lead_source_ids'] );
    	if( true == isset( $arrValues['ps_product_ids'] ) ) 				$this->setPsProductIds( $arrValues['ps_product_ids'] );
    	if( true == isset( $arrValues['company_employee_ids'] ) ) 			$this->setCompanyEmployeeIds( $arrValues['company_employee_ids'] );
    	if( true == isset( $arrValues['application_ids'] ) ) 				$this->setApplicationIds( $arrValues['application_ids'] );

 		return;
    }

	public function setPropertyIds( $arrintPropertyIds ) {
    	$this->m_arrintPropertyIds = $arrintPropertyIds;
    	$this->m_strProperties = $this->flattenIds( $arrintPropertyIds );
    }

    public function setProperties( $strProperties ) {
    	$this->m_strProperties = $strProperties;
    	$this->m_arrintPropertyIds = $this->expandIds( $strProperties );
    }

    public function setPropertyFloorplanIds( $arrintPropertyFloorplanIds ) {
    	$this->m_arrintPropertyFloorplanIds = $arrintPropertyFloorplanIds;
    	$this->m_strPropertyFloorplans = $this->flattenIds( $arrintPropertyFloorplanIds );
    }

    public function setPropertyFloorplans( $strPropertyFloorplans ) {
    	$this->m_strPropertyFloorplans = $strPropertyFloorplans;
    	$this->m_arrintPropertyFloorplanIds = $this->expandIds( $strPropertyFloorplans );
    }

    public function setApplicationStageStatusIds( $arrintApplicationStageStatusIds ) {
    	$this->m_arrintApplicationStageStatusIds = $arrintApplicationStageStatusIds;
    	$this->m_strApplicationStageStatuses = $this->flattenIds( $arrintApplicationStageStatusIds );
    }

    public function setApplicationStageStatuses( $strApplicationStageStatuses ) {
    	$this->m_strApplicationStageStatuses = $strApplicationStageStatuses;
    	$this->m_arrintApplicationStageStatusIds = $this->expandIds( $strApplicationStageStatuses );
    }

    public function setScreeningResponseTypeIds( $arrintScreeningResponseTypeIds ) {
    	$this->m_arrintScreeningResponseTypeIds = $arrintScreeningResponseTypeIds;
    	$this->m_strScreeningResponseTypes = $this->flattenIds( $arrintScreeningResponseTypeIds );
    }

    public function setScreeningResponseTypes( $strScreeningResponseTypes ) {
    	$this->m_strScreeningResponseTypes = $strScreeningResponseTypes;
    	$this->m_arrintScreeningResponseTypeIds = $this->expandIds( $strScreeningResponseTypes );
    }

    public function setLeaseIntervalTypeIds( $arrintLeaseIntervalTypeIds ) {
    	$this->m_arrintLeaseIntervalTypeIds = $arrintLeaseIntervalTypeIds;
    	$this->m_strLeaseIntervalTypes = $this->flattenIds( $arrintLeaseIntervalTypeIds );
    }

    public function setLeaseIntervalTypes( $strLeaseIntervalTypes ) {
    	$this->m_strLeaseIntervalTypes = $strLeaseIntervalTypes;
    	$this->m_arrintLeaseIntervalTypeIds = $this->expandIds( $strLeaseIntervalTypes );
    }

    public function setLeadSourceIds( $arrintLeadSourceIds ) {
    	$this->m_arrintLeadSourceIds = $arrintLeadSourceIds;
    	$this->m_strLeadSources = $this->flattenIds( $arrintLeadSourceIds );
    }

    public function setLeadSources( $strLeadSources ) {
    	$this->m_strLeadSources = $strLeadSources;
    	$this->m_arrintLeadSourceIds = $this->expandIds( $strLeadSources );
    }

    public function setPsProductIds( $arrintPsProductIds ) {
    	$this->m_arrintPsProductIds = $arrintPsProductIds;
    	$this->m_strPsProducts = $this->flattenIds( $arrintPsProductIds );
    }

    public function setPsProducts( $strPsProducts ) {
    	$this->m_strPsProducts = $strPsProducts;
    	$this->m_arrintPsProductIds = $this->expandIds( $strPsProducts );
    }

    public function setCompanyEmployeeIds( $arrintCompanyEmployeeIds ) {
    	$this->m_arrintCompanyEmployeeIds = $arrintCompanyEmployeeIds;
    	$this->m_strCompanyEmployees = $this->flattenIds( $arrintCompanyEmployeeIds );
    }

    public function setCompanyEmployees( $strCompanyEmployees ) {
    	$this->m_strCompanyEmployees = $strCompanyEmployees;
    	$this->m_arrintCompanyEmployeeIds = $this->expandIds( $strCompanyEmployees );
    }

    public function setApplicationIds( $arrintApplicationIds ) {
    	$this->m_arrintApplicationIds = $arrintApplicationIds;
    	$this->m_strApplications = $this->flattenIds( $arrintApplicationIds );
    }

    public function setApplications( $strApplications ) {
    	$this->m_strApplications = $strApplications;
    	$this->m_arrintApplicationIds = $this->expandIds( $strApplications );
    }

    public function setDesiredBedroomNumbers( $arrintDesiredBedroomNumbers ) {
    	$this->m_arrintDesiredBedroomNumbers 	= $arrintDesiredBedroomNumbers;
    	$this->m_strDesiredBedrooms 			= $this->flattenIds( $arrintDesiredBedroomNumbers );
    }

    public function setDesiredBathroomNumbers( $arrintDesiredBathroomNumbers ) {
    	$this->m_arrintDesiredBathroomNumbers 	= $arrintDesiredBathroomNumbers;
    	$this->m_strDesiredBathrooms 			= $this->flattenIds( $arrintDesiredBathroomNumbers );
    }

    public function setCompanyApplicationIds( $arrintCompanyApplicationIds ) {
    	$this->m_arrintCompanyApplicationIds = $arrintCompanyApplicationIds;
    }

    public function setEligiblePsProductIds( $arrintEligiblePsProductIds ) {
    	$this->m_arrintEligiblePsProductIds = $arrintEligiblePsProductIds;
    }

 	public function setIncludeLeadsWithNullLeasingAgent( $boolIncludeLeadsWithNullLeasingAgent ) {
		$this->m_boolIncludeLeadsWithNullLeasingAgent = $boolIncludeLeadsWithNullLeasingAgent;
    }

    public function setUnitNumbers( $strUnitNumbers ) {
		$this->m_strUnitNumbers = $strUnitNumbers;
    }

    public function setPageNumber( $intPageNumber ) {
		$this->m_intPageNumber = $intPageNumber;
    }

    public function setCountPerPage( $intCountPerPage ) {
		$this->m_intCountPerPage = $intCountPerPage;
    }

    public function setIsShowLastContact( $intIsShowLastContact ) {
    	$this->m_intIsShowLastContact = $intIsShowLastContact;
    }

	/**
	 * Get Functions
	 */

    public function getPropertyIds() {
    	return $this->m_arrintPropertyIds;
    }

    public function getPropertyFloorplanIds() {
    	return $this->m_arrintPropertyFloorplanIds;
    }

    public function getApplicationStageStatusIds() {
    	return $this->m_arrintApplicationStageStatusIds;
    }

    public function getScreeningResponseTypeIds() {
    	return $this->m_arrintScreeningResponseTypeIds;
    }

    public function getLeaseIntervalTypeIds() {
    	return $this->m_arrintLeaseIntervalTypeIds;
    }

    public function getLeadSourceIds() {
    	return $this->m_arrintLeadSourceIds;
    }

    public function getPsProductIds() {
    	return $this->m_arrintPsProductIds;
    }

    public function getCompanyEmployeeIds() {
    	return $this->m_arrintCompanyEmployeeIds;
    }

    public function getApplicationIds() {
    	return $this->m_arrintApplicationIds;
    }

    public function getDesiredBedroomNumbers() {
    	return $this->m_arrintDesiredBedroomNumbers;
    }

    public function getDesiredBathroomNumbers() {
    	return $this->m_arrintDesiredBathroomNumbers;
    }

    public function getCompanyApplicationIds() {
		return $this->m_arrintCompanyApplicationIds;
    }

	public function getIncludeLeadsWithNullLeasingAgent() {
		return $this->m_boolIncludeLeadsWithNullLeasingAgent;
    }

    public function getUnitNumbers() {
		return $this->m_strUnitNumbers;
    }

    public function getPageNumber() {
		return $this->m_intPageNumber;
    }

    public function getCountPerPage() {
		return $this->m_intCountPerPage;
    }

    public function getIsShowLastContact() {
    	return $this->m_intIsShowLastContact;
    }

    public function getEligiblePsProductIds() {
    	return $this->m_arrintEligiblePsProductIds;
    }

    public function getEligibleApplicationFilters() {
    	return $this->m_arrobjEligibleApplicationFilters;
    }

    public function getEligibleProperties() {
    	return $this->m_arrstrEligibleProperties;
    }

    public function getEligibleApplicationStageStatuses() {
    	return $this->m_arrobjEligibleApplicationStageStatuses;
    }

    public function getEligiblePsProducts() {
    	return $this->m_arrobjEligiblePsProducts;
    }

    public function getEligibleLeadSources() {
    	return $this->m_arrobjEligibleLeadSources;
    }

    public function getEligibleLeaseIntervalTypes() {
    	return $this->m_arrobjEligibleLeaseIntervalTypes;
    }

    public function getEligiblePropertyFloorplans() {
    	return $this->m_arrobjEligiblePropertyFloorplans;
    }

    public function getEligibleCompanyEmployees() {
    	return $this->m_arrobjEligibleCompanyEmployees;
    }

	/**
	 * Fetch Functions
	 */

    public function fetchEligibleCompanyEmployees( $objDatabase ) {

    	if( true == valArr( $this->m_arrintPropertyIds ) && 1 == \Psi\Libraries\UtilFunctions\count( $this->m_arrintPropertyIds ) ) {
    		return \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchActiveLeasingAgentsByPropertyIdsByCid( $this->m_arrintPropertyIds, $this->m_intCid, $objDatabase );
    	}

    	return [];
    }

    public function fetchEligiblePropertyFloorplans( $objDatabase ) {

    	if( true == valArr( $this->m_arrintPropertyIds ) && 1 == \Psi\Libraries\UtilFunctions\count( $this->m_arrintPropertyIds ) ) {
    		return \Psi\Eos\Entrata\CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByCid( $this->m_arrintPropertyIds, $this->getCid(), $objDatabase );
    	}

    	return [];
    }

    /**
     * Other Functions
     */

    public function initializeFilter( $arrintPropertyIds, $arrintPsProductIds, $intCompanyUserId, $objDatabase ) {

    	$this->m_arrobjEligibleApplicationFilters 		= CApplicationFiltersNew::fetchApplicationFiltersByCompanyUserId( $intCompanyUserId, $objDatabase );
    	$this->m_arrstrEligibleProperties 				= \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesByIdsByFieldNamesByCid( $arrintPropertyIds, [ 'id', 'property_name', 'remote_primary_key' ], $this->m_intCid, $objDatabase );

    	$arrintExcludingRenewalStageStatusIds		= [ CApplicationStageStatus::RENEWAL_STAGE1_STARTED ];

    	// Need to change this as per application_stage_status table changes
    	$this->m_arrobjEligibleApplicationStageStatuses	= CApplicationStageStatuses::fetchApplicationStageStatusesByExcludingIds( $arrintExcludingApplicationStageStatusIds, $objDatabase );

    	$this->m_arrobjEligiblePsProducts				= CPsProduct::loadPsProductsByIds( $arrintPsProductIds );

    	$this->m_arrobjEligibleLeadSources				= \Psi\Eos\Entrata\CLeadSources::createService()->fetchCustomLeadSourcesByCid( $this->m_intCid, $objDatabase );
    	$this->m_arrobjEligibleLeaseIntervalTypes				= \Psi\Eos\Entrata\CLeaseIntervalTypes::createService()->fetchAllLeaseIntervalTypes( $objDatabase );

    	// $this->m_arrobjEligibleCompanyEmployees       = $this->fetchEligibleCompanyEmployees( $objDatabase );
    	// $this->m_arrobjEligiblePropertyFloorplans     = $this->fetchEligiblePropertyFloorplans( $objDatabase );

    	$this->m_arrobjEligibleCompanyEmployees			= \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchActiveLeasingAgentsByPropertyIdsByCid( $arrintPropertyIds, $this->m_intCid, $objDatabase );
    	$this->m_arrobjEligiblePropertyFloorplans 		= \Psi\Eos\Entrata\CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
    }

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        if( true == is_null( $this->getId() ) ) {
            return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
        } else {
            return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
        }
    }

    public function flattenIds( $arrintIds ) {
    	if( false == valArr( $arrintIds ) ) return '';
    	return implode( ',', $arrintIds );
    }

    public function expandIds( $strIds ) {
    	return explode( ',', $strIds );
    }

    public function determineIsShowLastContact( $arrintPsProductIds ) {

    	$arrintLeadManagementPropertyIds 	= [];
    	$this->m_intIsShowLastContact 		= 0;

    	if( true == valArr( $arrintPsProductIds ) && true == array_key_exists( CPsProduct::LEAD_MANAGEMENT, $arrintPsProductIds ) ) {
    		$arrintLeadManagementPropertyIds = $arrintPsProductIds[CPsProduct::LEAD_MANAGEMENT];
    	}

    	if( true == valArr( $this->getPropertyIds() ) && true == valArr( $arrintLeadManagementPropertyIds ) ) {
    		$this->m_intIsShowLastContact = ( true == valArr( array_intersect( $arrintLeadManagementPropertyIds, $this->getPropertyIds() ) ) ) ? 1 : 0;
    	} elseif( true == valArr( $arrintLeadManagementPropertyIds ) ) {
    		$this->m_intIsShowLastContact = 1;
    	}

    	return $this->m_intIsShowLastContact;
    }

	/**
	 * Validate Functions
	 */

    public function valConflictingCompanyUserApplicationFilterName( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getFilterName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filter_name', 'Filter name is required.' ) );
        }

        $intConflictingApplicationFilterNameCount = \Psi\Eos\Entrata\CApplicationFilters::createService()->fetchConflictingCompanyUserApplicationFilterNameCount( $this->getCompanyUserId(), $this->getFilterName(), $objDatabase );

       	if( 0 < $intConflictingApplicationFilterNameCount ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filter_name', 'Filter name \'' . $this->getFilterName() . '\' already in use.' ) );
       	}

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valConflictingCompanyUserApplicationFilterName( $objDatabase );
             	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>