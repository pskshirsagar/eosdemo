<?php

class CArTrigger extends CBaseArTrigger {

	const PRE_QUALIFICATION				= 101;
	const APPLICATION_COMPLETED			= 102;
	const APPLICATION_APPROVAL			= 103;
	const LEASE_COMPLETED				= 104;
	const LEASE_APPROVAL				= 105;
	const APPLICATION_STARTED			= 106;
	const LEASE_STARTED				    = 107;
	const SCREENING_APPROVED		    = 108;

	// Type of One Time Lease Charges
	const ONE_TIME						= 201;
	const MOVE_IN						= 202;
	const LAST_MONTH_OF_OCCUPANCY		= 204;
	const NOTICE						= 205;
	const MOVE_OUT						= 206;
	const ANNIVERSARY_OF_MOVE_IN		= 207;
	const END_OF_LEASE_TERM				= 208;
	const END_OF_CALENDAR_YEAR			= 209;
	const FINAL_STATEMENT				= 210;
	const RENEWAL_LEASE_APPROVAL		= 211;
	const RENEWAL_START					= 212;
	const RENEWAL_OFFER_ACCEPTED		= 213;

	// Type of Recurring Lease Charges
	const HOURLY						= 301;
	const DAILY							= 302;
	const WEEKLY						= 303;
	const EVERY_TWO_WEEKS				= 304;
	const TWICE_PER_MONTH				= 305;
	const SPECIFIC_LEASE_MONTHS			= 306;
	const MONTHLY						= 307;
	const QUARTERLY						= 308;
	const TWICE_PER_YEAR				= 309;
	const YEARLY						= 310;
	const PERCENT_OF_SALES				= 311;
	const NIGHTLY				        = 312;

	// Type of Conditional Events
	const LATE_FEE						= 401;
	const DEPOSIT_INTEREST				= 402;
	const INSUFFICIENT_NOTICE			= 403;
	const EARLY_TERMINATION				= 404;
	const RETURN_ITEM_FEE				= 406;
	const MONTH_TO_MONTH_BEGIN			= 407;
	const PROCESS_TRANSFER				= 408;
	const SCHEDULE_TRANSFER				= 409;
	const RESERVATION					= 410;
	const ASSIGNABLE_ITEM_REPLACEMENT 	= 411;
	const DELINQUENCY_INTEREST          = 412;
	const SERVICE_SELECTED              = 413;
	const SERVICE_FULFILLED             = 414;
	const LEASE_VIOLATION			    = 1001;

	// Type of Utilities
	const WATER							= 501;
	const GAS							= 502;
	const ELECTRIC						= 503;
	const SEWER							= 504;
	const TOWING						= 505;
	const TRASH							= 506;
	const PHONE							= 507;
	const INTERNET						= 508;
	const CABLE							= 509;
	const LAUNDRY_SERVICES				= 510;
	const HOT_WATER						= 511;
	const COLD_WATER					= 512;
	const CONVERGENT_BILLING			= 513;
	const WATER_OR_WASTE_WATER			= 514;
	const HOT_WATER_ENERGY				= 515;
	const PEST_CONTROL					= 516;
	const STORMWATER					= 517;
	const EMS_FEE						= 518;
	const HVAC							= 519;
	const VCR_ELECTRIC					= 520;
	const VCR_GAS						= 521;
	const RECYCLING						= 522;
	const OTHER_UTILITY					= 599;

	const WORK_ORDER_FEES				= 601;

	const BAD_DEBT_WRITE_OFF			= 701;
	const TAX							= 702;
	const FIRST_EPAY_INCENTIVE			= 704;
	const ACCELERATED_RENT				= 705;
	const BAD_DEBT_RECOVERY				= 706;
	const COLLECTIONS_EXPENSE           = 707;
	const COLLECTIONS_PAYMENT           = 708;

	const MANUAL						= 901;
	const API_IMPORT					= 902;
	const CSV_UPLOAD					= 903;
	const LEDGER_IMPORT					= 904;
	const DEPOSIT_REFUND				= 906;
	// const SCREENING_CONDITION		= 907;
	const LEDGER_TRANSFER	 			= 908;
	const UNIT_CHANGE 		 			= 909;
	const DEPOSIT_APPLY_TO_CHARGE 		= 914;
	const MOVE_IN_DATE_ADJUSTMENT 		= 915;
	const TRANSPORT_LAYER				= 916;

	const UNKNOWN						= 1000;
	const AVERAGE_MONTH_LENGTH			= 30.436875;

	public static $c_arrintLeadSectionOtherLeadArTriggers		= [ self::APPLICATION_COMPLETED, self::PRE_QUALIFICATION, self::APPLICATION_APPROVAL, self::LEASE_COMPLETED, self::LEASE_APPROVAL, self::SCREENING_APPROVED ];
	public static $c_arrintLeadSectionOneTimeArTriggers			= [ self::MOVE_IN, self::MOVE_OUT, self::APPLICATION_COMPLETED, self::PRE_QUALIFICATION, self::APPLICATION_APPROVAL, self::LEASE_COMPLETED, self::LEASE_APPROVAL, self::SCREENING_APPROVED ];
	public static $c_arrintResidentSectionOneTimeArTriggers		= [ self::MOVE_IN, self::MOVE_OUT, self::ONE_TIME ];
	public static $c_arrintRecurringArTriggers					= [ self::HOURLY, self::DAILY, self::WEEKLY, self::EVERY_TWO_WEEKS, self::TWICE_PER_MONTH, self::SPECIFIC_LEASE_MONTHS, self::MONTHLY, self::QUARTERLY, self::TWICE_PER_YEAR, self::YEARLY, self::NIGHTLY ];
	public static $c_arrintRenewalArTriggers					= [ self::HOURLY, self::DAILY, self::WEEKLY, self::EVERY_TWO_WEEKS, self::TWICE_PER_MONTH, self::SPECIFIC_LEASE_MONTHS, self::MONTHLY, self::QUARTERLY, self::TWICE_PER_YEAR, self::YEARLY, self::NIGHTLY, self::MONTH_TO_MONTH_BEGIN ];
	public static $c_arrintUnitTransferRecurringArTriggers		= [ self::HOURLY, self::DAILY, self::WEEKLY, self::EVERY_TWO_WEEKS, self::TWICE_PER_MONTH, self::SPECIFIC_LEASE_MONTHS, self::MONTHLY, self::QUARTERLY, self::TWICE_PER_YEAR, self::YEARLY, self::MOVE_IN, self::MOVE_OUT ];
	public static $c_arrintNotificationLetterOneTimeTrigggers   = [ self::MOVE_IN, self::ONE_TIME, self::APPLICATION_COMPLETED, self::PRE_QUALIFICATION, self::APPLICATION_APPROVAL, self::LEASE_COMPLETED, self::LEASE_APPROVAL, self::SCREENING_APPROVED ];
	public static $c_arrintArInvoicingExcludedArTriggers        = [ self::BAD_DEBT_WRITE_OFF, self::BAD_DEBT_RECOVERY, self::COLLECTIONS_EXPENSE ];

	// Deposit, Base, Utilities
	public static $c_arrintProspectArTriggers						= [ self::MOVE_IN, self::APPLICATION_COMPLETED, self::MONTHLY, self::WATER, self::GAS, self::ELECTRIC, self::SEWER, self::TOWING, self::TRASH, self::PHONE, self::INTERNET, self::CABLE, self::LAUNDRY_SERVICES, self::HOT_WATER, self::COLD_WATER, self::CONVERGENT_BILLING, self::WATER_OR_WASTE_WATER, self::HOT_WATER_ENERGY, self::PEST_CONTROL, self::STORMWATER, self::EMS_FEE, self::HVAC, self::VCR_ELECTRIC, self::VCR_GAS, self::RECYCLING, self::OTHER_UTILITY ];
	public static $c_arrintProbableSecDepArTriggerIdsForRV			= [ self::MOVE_IN, self::APPLICATION_COMPLETED, self::APPLICATION_APPROVAL, self::LEASE_COMPLETED, self::LEASE_APPROVAL, self::HOURLY, self::DAILY, self::WEEKLY, self::EVERY_TWO_WEEKS, self::TWICE_PER_MONTH, self::SPECIFIC_LEASE_MONTHS, self::MONTHLY, self::QUARTERLY, self::TWICE_PER_YEAR, self::YEARLY, self::NIGHTLY ];
	// Commented previous one because we will implement other triggers shortly : [KD][CS]
	// static $c_arrintDocumentPacketArTriggers				= array( self::APPLICATION_COMPLETED => self::APPLICATION_COMPLETED, self::LEASE_STARTED => self::LEASE_STARTED, self::MOVE_IN => self::MOVE_IN, self::NOTICE => self::NOTICE, self::MOVE_OUT => self::MOVE_OUT );
	public static $c_arrintDocumentPacketArTriggers			        = [ self::LEASE_STARTED => self::LEASE_STARTED, self::APPLICATION_STARTED => self::APPLICATION_STARTED ];
	public static $c_arrintIntegratedDepositeArTriggerIds			= [ self::MOVE_IN, self::APPLICATION_COMPLETED, self::APPLICATION_APPROVAL ];

	// Triggers by ArTriggerTypes
	public static $c_arrintApplicationOrLeasingTypeArTriggers 		= [ self::PRE_QUALIFICATION, self::APPLICATION_COMPLETED, self::APPLICATION_APPROVAL, self::LEASE_COMPLETED, self::LEASE_APPROVAL, self::SCREENING_APPROVED ];
	public static $c_arrintOneTimeLeaseChargesTypeArTriggers 		= [ self::ONE_TIME, self::MOVE_IN, self::LAST_MONTH_OF_OCCUPANCY, self::NOTICE, self::MOVE_OUT, self::ANNIVERSARY_OF_MOVE_IN, self::END_OF_LEASE_TERM, self::END_OF_CALENDAR_YEAR, self::FINAL_STATEMENT, self::RENEWAL_LEASE_APPROVAL, self::RENEWAL_START ];
	public static $c_arrintRecurringLeaseChargesTypeArTriggers		= [ self::HOURLY, self::DAILY, self::WEEKLY, self::EVERY_TWO_WEEKS, self::TWICE_PER_MONTH, self::SPECIFIC_LEASE_MONTHS, self::MONTHLY, self::QUARTERLY, self::TWICE_PER_YEAR, self::YEARLY, self::NIGHTLY ];
	public static $c_arrintCacheableTypeArTriggers					= [ self::LATE_FEE, self::DEPOSIT_INTEREST, self::INSUFFICIENT_NOTICE, self::EARLY_TERMINATION, self::RETURN_ITEM_FEE, self::MONTH_TO_MONTH_BEGIN, self::PROCESS_TRANSFER, self::SCHEDULE_TRANSFER ];
	public static $c_arrintRentCalculationArTriggers				= [ self::MONTHLY, self::MOVE_IN, self::PRE_QUALIFICATION, self::APPLICATION_COMPLETED, self::APPLICATION_APPROVAL, self::LEASE_COMPLETED, self::LEASE_APPROVAL ];

	public static $c_arrintApiAllowedArTriggers                     = [ self::APPLICATION_COMPLETED, self::APPLICATION_STARTED, self::ONE_TIME, self::MOVE_IN, self::MOVE_OUT, self::MONTHLY, self::MONTH_TO_MONTH_BEGIN ];
	public static $c_arrintAddOnsArTriggers                         = [ self::APPLICATION_COMPLETED, self::ASSIGNABLE_ITEM_REPLACEMENT, self::ONE_TIME, self::MOVE_IN, self::MOVE_OUT, self::MONTHLY, self::WEEKLY, self::DAILY, self::NIGHTLY, self::SERVICE_SELECTED, self::SERVICE_FULFILLED ];
	public static $c_arrintOtherFeesArTriggers                      = [ self::MONTHLY, self::DAILY, self::WEEKLY, self::YEARLY, self::TWICE_PER_MONTH ];
	public static $c_arrintAddOnRewindArTriggers		            = [ self::ASSIGNABLE_ITEM_REPLACEMENT, self::HOURLY, self::DAILY, self::WEEKLY, self::EVERY_TWO_WEEKS, self::TWICE_PER_MONTH, self::SPECIFIC_LEASE_MONTHS, self::MONTHLY, self::QUARTERLY, self::TWICE_PER_YEAR, self::YEARLY, self::MOVE_IN, self::NIGHTLY ];
	public static $c_arrintApplicationOneTimeArTriggers             = [ self::MOVE_IN, self::MOVE_OUT, self::APPLICATION_COMPLETED, self::PRE_QUALIFICATION, self::APPLICATION_APPROVAL, self::LEASE_COMPLETED, self::LEASE_APPROVAL, self::SCREENING_APPROVED, self::RENEWAL_LEASE_APPROVAL ];
	public static $c_arrintApplicationShuffleArTriggers             = [ self::PRE_QUALIFICATION, self::APPLICATION_STARTED, self::APPLICATION_COMPLETED, self::APPLICATION_APPROVAL, self::LEASE_STARTED, self::LEASE_COMPLETED, self::LEASE_APPROVAL, self::MOVE_IN ];
	public static $c_arrintConditionalChargeArTriggers              = [ self::PROCESS_TRANSFER, self::SCHEDULE_TRANSFER, self::RETURN_ITEM_FEE, self::LATE_FEE, self::DEPOSIT_INTEREST, self::INSUFFICIENT_NOTICE, self::EARLY_TERMINATION, self::DELINQUENCY_INTEREST, self::RENEWAL_LEASE_APPROVAL, self::MONTH_TO_MONTH_BEGIN, self::RESERVATION, self::ASSIGNABLE_ITEM_REPLACEMENT ];
	public static $c_arrintDepositChargeArTriggers					= [ self::PRE_QUALIFICATION, self::MOVE_IN, self::APPLICATION_COMPLETED, self::APPLICATION_APPROVAL, self::LEASE_COMPLETED, self::LEASE_APPROVAL, self::SCREENING_APPROVED ];
	public static $c_arrintTenantQuoteOptionsArTriggers             = [ self::WEEKLY, self::MONTHLY, self::QUARTERLY, self::YEARLY ];
	public static $c_arrintOtherChargesArTriggerIds					= [ self::ONE_TIME, self::LAST_MONTH_OF_OCCUPANCY, self::NOTICE, self::MOVE_OUT, self::ANNIVERSARY_OF_MOVE_IN, self::END_OF_LEASE_TERM, self::END_OF_CALENDAR_YEAR, self::FINAL_STATEMENT, self::RENEWAL_LEASE_APPROVAL, self::RENEWAL_START, self::MONTHLY, self::DAILY, self::WEEKLY ];
	public static $c_arrintRecurringArTriggerIds					= [ self::DAILY, self::WEEKLY, self::TWICE_PER_MONTH, self::MONTHLY, self::NIGHTLY ];
	public static $c_arrintOneTimeArTriggerIds						= [ self::ONE_TIME, self::LAST_MONTH_OF_OCCUPANCY, self::NOTICE, self::MOVE_OUT, self::ANNIVERSARY_OF_MOVE_IN, self::END_OF_LEASE_TERM, self::END_OF_CALENDAR_YEAR, self::FINAL_STATEMENT, self::RENEWAL_LEASE_APPROVAL, self::RENEWAL_START, self::RENEWAL_OFFER_ACCEPTED ];
	public static $c_arrintAddOnRecurringArTriggerIds				= [ self::DAILY, self::WEEKLY, self::MONTHLY, self::NIGHTLY ];
	public static $c_arrintAllOneTimeArTriggerIds					= [ self::ONE_TIME, self::LAST_MONTH_OF_OCCUPANCY, self::NOTICE, self::MOVE_OUT, self::ANNIVERSARY_OF_MOVE_IN, self::END_OF_LEASE_TERM, self::END_OF_CALENDAR_YEAR, self::FINAL_STATEMENT, self::RENEWAL_LEASE_APPROVAL, self::RENEWAL_START, self::RENEWAL_OFFER_ACCEPTED, self::MOVE_IN, self::PRE_QUALIFICATION, self::SCREENING_APPROVED, self::LEASE_COMPLETED, self::APPLICATION_APPROVAL, self::APPLICATION_COMPLETED, self::LEASE_APPROVAL ];
	public static $c_arrintRenewalTransferRecurringArTriggerIds		= [ self::DAILY, self::WEEKLY, self::MONTHLY, self::NIGHTLY ];
	public static $c_arrintRenewalOneTimeTriggerIds					= [ self::RENEWAL_START, self::RENEWAL_OFFER_ACCEPTED, self::RENEWAL_LEASE_APPROVAL ];

	protected $m_arrintArCodeTypeIds;
	protected $m_arrstrRecurringChargesFrequencies;

	public function getArCodeTypeIds() {
		return $this->m_arrintArCodeTypeIds;
	}

	public function setArCodeTypeIds( array $arrintArCodeTypeIds =[] ) {
		$this->m_arrintArCodeTypeIds = $arrintArCodeTypeIds;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_code_type_ids'] ) ) $this->setArCodeTypeIds( CStrings::strToArrIntDef( $arrmixValues['ar_code_type_ids'] ) );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUtilityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOrigins() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowFlexibleLeaseCaching() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRateOffsets() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowArCodeMapping() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequireCustomerRelationship() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequireLeaseIntervalType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOptional() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public static function loadCustomizedArTriggers( $objDatabase ) {

		$arrstrCustomizedArTriggers	= [];
		$arrmixArTriggers			= ( array ) fetchData( 'SELECT id, name FROM ar_triggers ORDER BY order_num', $objDatabase );

		foreach( $arrmixArTriggers as $arrmixArTrigger ) {
			$arrstrCustomizedArTriggers[$arrmixArTrigger['id']] = '(' . $arrmixArTrigger['name'] . ')';
		}

		return $arrstrCustomizedArTriggers;
	}

	public static function getArTriggerNameByArTriggerId( $intArTriggerId ) {

		switch( $intArTriggerId ) {
			case self::ONE_TIME:
				return __( 'One Time' );
				break;

			case self::LEASE_APPROVAL:
				return __( 'Lease Approval' );
				break;

			case self::MOVE_IN:
				return __( 'Move In' );
				break;

			case self::LAST_MONTH_OF_OCCUPANCY:
				return __( 'Last Month of Occupancy' );
				break;

			case self::NOTICE:
				return __( 'Notice' );
				break;

			case self::MOVE_OUT:
				return __( 'Move Out' );
				break;

			case self::ANNIVERSARY_OF_MOVE_IN:
				return __( 'Anniversary of Move-in' );

			case self::END_OF_LEASE_TERM:
				return __( 'End of Term' );
				break;

			case self::END_OF_CALENDAR_YEAR:
				return __( 'End of Calendar Year' );
				break;

			case self::LEASE_COMPLETED:
				return __( 'Lease Completed' );
				break;

			case self::FINAL_STATEMENT:
				return __( 'Final Statement' );
				break;

			case self::RENEWAL_LEASE_APPROVAL:
				return __( 'Renewal Lease Approval' );
				break;

			case self::RENEWAL_START:
				return 'Renewal Start';
				break;

			case self::APPLICATION_COMPLETED:
				return __( 'Application Completed' );
				break;

			case self::APPLICATION_APPROVAL:
				return __( 'Application Approval' );
				break;

			case self::PROCESS_TRANSFER:
				return __( 'Process Transfer' );
				break;

			case self::SCHEDULE_TRANSFER:
				return __( 'Schedule Transfer' );
				break;

			case self::MONTHLY:
				return __( 'Monthly' );
				break;

			case self::WEEKLY:
				return __( 'Weekly' );
				break;

			case self::DAILY:
				return __( 'Daily' );
				break;

			case self::ACCELERATED_RENT:
				return __( 'Accelerated Rent' );
				break;

			case self::PRE_QUALIFICATION:
				return __( 'Pre-Qualification' );
				break;

			case self::BAD_DEBT_WRITE_OFF:
				return __( 'Bad Debt Write Off' );
				break;

			case self::EARLY_TERMINATION:
				return __( 'Early Termination' );
				break;

			case self::INSUFFICIENT_NOTICE:
				return __( 'Insufficient Notice' );
				break;

			case self::DEPOSIT_INTEREST:
				return __( 'Deposit Interest' );
				break;

			case self::LATE_FEE:
				return __( 'Late Fee' );
				break;

			case self::RETURN_ITEM_FEE:
				return __( 'Return Item Fee' );
				break;

			case self::TAX:
				return __( 'Tax' );
				break;

			case self::SCREENING_APPROVED:
				return __( 'Screening Approved' );
				break;

			case self::DELINQUENCY_INTEREST:
				return __( 'Delinquency Interest' );
				break;

			case self::TWICE_PER_MONTH:
				return __( 'Twice Per Month' );
				break;

			case self::RENEWAL_OFFER_ACCEPTED:
				return __( 'Renewal Offer Accepted' );
				break;

			case self::NIGHTLY:
				return __( 'Nightly' );
				break;

			case self::SERVICE_SELECTED:
				return __( 'Service Selected' );
				break;

			case self::SERVICE_FULFILLED:
				return __( 'Service Fulfilled' );
				break;

			default:
				// Not Found
				break;
		}

		return;
	}

	public function getRecurringChargesFrequency( $boolIsFromRP = false ) {
		if( true == $boolIsFromRP ) {
			$this->m_arrstrRecurringChargesFrequencies = [
				self::HOURLY                => __( 'hourly' ),
				self::DAILY                 => __( 'daily' ),
				self::NIGHTLY               => __( 'nightly' ),
				self::WEEKLY                => __( 'weekly' ),
				self::EVERY_TWO_WEEKS       => __( 'every_two_weeks' ),
				self::TWICE_PER_MONTH       => __( 'twice_per_month' ),
				self::SPECIFIC_LEASE_MONTHS => __( 'specific_lease_months' ),
				self::MONTHLY               => __( 'monthly' ),
				self::QUARTERLY             => __( 'quarterly' ),
				self::TWICE_PER_YEAR        => __( 'twice_per_year' ),
				self::YEARLY                => __( 'yearly' )
			];
		} else {
			$this->m_arrstrRecurringChargesFrequencies = [
				self::HOURLY                => __( 'hour' ),
				self::DAILY                 => __( 'day' ),
				self::WEEKLY                => __( 'week' ),
				self::EVERY_TWO_WEEKS       => __( 'two weeks' ),
				self::TWICE_PER_MONTH       => __( 'twice per month' ),
				self::SPECIFIC_LEASE_MONTHS => __( 'specific lease months' ),
				self::MONTHLY               => __( 'month' ),
				self::QUARTERLY             => __( 'quarter' ),
				self::TWICE_PER_YEAR        => __( 'twice per year' ),
				self::YEARLY                => __( 'year' ),
				self::NIGHTLY               => __( 'nightly' )
			];
		}
		return $this->m_arrstrRecurringChargesFrequencies;
	}

	public function getAbbreviatedFrequencyText() {
		return
		[
			self::HOURLY => __( 'Hr' ),
			self::DAILY => __( 'Day' ),
			self::NIGHTLY => __( 'Night' ),
			self::WEEKLY => __( 'Wk' ),
			self::EVERY_TWO_WEEKS => __( 'Q2W' ),
			self::TWICE_PER_MONTH => __( 'Bi-Mo' ),
			self::MONTHLY => __( 'Mo' ),
			self::QUARTERLY => __( 'Qtr' ),
			self::TWICE_PER_YEAR => __( 'Bi-Yr' ),
			self::YEARLY => __( 'Yr' )
		];
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCommercialRentStepsFrequency() {
		return [
			'DAILY'           => self::DAILY,
			'WEEKLY'          => self::WEEKLY,
			'TWICE_PER_MONTH' => self::TWICE_PER_MONTH,
			'MONTHLY'         => self::MONTHLY,
			'QUARTERLY'       => self::QUARTERLY,
			'TWICE_PER_YEAR'  => self::TWICE_PER_YEAR,
			'YEARLY'          => self::YEARLY,
			'NIGHTLY'         => self::NIGHTLY
		];
	}

	public function getCommercialRenewalOptionChargeTiming() {
		return [
			__( 'Monthly' )	    => self::MONTHLY,
			__( 'Weekly' )		=> self::WEEKLY,
			__( 'Quarterly' )	=> self::QUARTERLY,
			__( 'Yearly' )		=> self::YEARLY,
		];
	}

	public function getArTriggerPostfix() {
		$arrstrArTriggerPostFix = [
			self::MONTHLY         => __( 'per month' ),
			self::DAILY           => __( 'per day' ),
			self::WEEKLY          => __( "per week" ),
			self::TWICE_PER_MONTH => __( "twice per month" ),
			self::QUARTERLY       => __( "per quarter" ),
			self::TWICE_PER_YEAR  => __( "twice per year" ),
			self::YEARLY          => __( "per year" ),
			self::NIGHTLY         => __( "per night" )
		];

		return $arrstrArTriggerPostFix;
	}

}
?>