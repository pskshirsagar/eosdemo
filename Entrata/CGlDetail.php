<?php
class CGlDetail extends CBaseGlDetail {

	protected $m_boolIsDeleted;
	protected $m_boolIsScheduledJePaused;
	protected $m_boolIsFromBillbackListing;
	protected $m_boolIsDisabledGlAccount;
	protected $m_boolIsForParentProperty;

	protected $m_intToGlDetailId;
	protected $m_intToGlAccountId;
	protected $m_intArAllocationId;
	protected $m_intApAllocationId;
	protected $m_intFromGlDetailId;
	protected $m_intFromGlAccountId;
	protected $m_intArTransactionId;
	protected $m_intGlTransactionTypeId;

	protected $m_fltDebitAmount;
	protected $m_fltCreditAmount;

	protected $m_strApCodeName;
	protected $m_strJobPhaseName;
	protected $m_strApContractName;
	protected $m_strAccountNumber;
	protected $m_strAccountName;
	protected $m_strPropertyName;
	protected $m_strTransactionDatetime;
	protected $m_strPostMonth;
	protected $m_strPostDate;
	protected $m_strUnitNumber;
	protected $m_strCustomTagName;
	protected $m_strToGlAccountName;
	protected $m_strFromGlAccountName;
	protected $m_strCompanyDepartmentName;
	protected $m_strBillbackIndex;

	protected $m_objOffsettingGlDetail;

	protected $m_arrobjPropertyUnits;
	protected $m_arrobjGlDetailsProperties;

	protected $m_arrmixGlAccountProperties;
	protected $m_intAccountingExportBatchId;

	public static $c_strAccountingMethod = [
		'Cash' 		=> 1,
		'Accrual' 	=> 2
	];

	/**
	 * Get Functions
	 */

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function getArAllocationId() {
		return $this->m_intArAllocationId;
	}

	public function getApAllocationId() {
		return $this->m_intApAllocationId;
	}

	public function getGlTransactionTypeId() {
		return $this->m_intGlTransactionTypeId;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function getGlAccountProperties() {
		return $this->m_arrmixGlAccountProperties;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getPropertyUnits() {
		return $this->m_arrobjPropertyUnits;
	}

	public function getGlDetailsProperties() {
		return $this->m_arrobjGlDetailsProperties;
	}

	public function getTransactionAmount() {
		return $this->m_fltAmount;
	}

	public function getFormattedPostMonth() {
		if( true == is_null( $this->m_strPostMonth ) ) return NULL;
		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getCompanyDepartmentName() {
		return $this->m_strCompanyDepartmentName;
	}

	public function getBillbackIndex() {
		return $this->m_strBillbackIndex;
	}

	public function getCustomTagName() {
		return $this->m_strCustomTagName;
	}

	public function getJobPhaseName() {
		return $this->m_strJobPhaseName;
	}

	public function getApCodeName() {
		return $this->m_strApCodeName;
	}

	public function getApContractName() {
		return $this->m_strApContractName;
	}

	public function getFromGlAccountId() {
		return $this->m_intFromGlAccountId;
	}

	public function getToGlAccountId() {
		return $this->m_intToGlAccountId;
	}

	public function getFromGlAccountName() {
		return $this->m_strFromGlAccountName;
	}

	public function getToGlAccountName() {
		return $this->m_strToGlAccountName;
	}

	public function getFromGlDetailId() {
		return $this->m_intFromGlDetailId;
	}

	public function getToGlDetailId() {
		return $this->m_intToGlDetailId;
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function getIsScheduledJEPaused() {
		return $this->m_boolIsScheduledJePaused;
	}

	public function getAccountingExportBatchId() {
		return $this->m_intAccountingExportBatchId;
	}

	public function getDebitAmount() {
		return $this->m_fltDebitAmount;
	}

	public function getCreditAmount() {
		return $this->m_fltCreditAmount;
	}

	public function getIsFromBillbackListing() {
		return $this->m_boolIsFromBillbackListing;
	}

	public function getIsDisabledGlAccount() {
		return $this->m_boolIsDisabledGlAccount;
	}

	public function getIsForParentProperty() {
		return $this->m_boolIsForParentProperty;
	}

	public function getOffsettingGlDetail() {
		return $this->m_objOffsettingGlDetail;
	}

	/**
	 * Set Functions
	 */

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->m_strTransactionDatetime = $strTransactionDatetime;
	}

	public function setAccountNumber( $intAccountNumber ) {
		$this->m_strAccountNumber = $intAccountNumber;
	}

	public function setAccountName( $strAccountName ) {
		return $this->m_strAccountName = $strAccountName;
	}

	public function setPropertyName( $strPropertyName ) {
		return $this->m_strPropertyName = $strPropertyName;
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->m_intArTransactionId = $intArTransactionId;
	}

	public function setArAllocationId( $intArAllocationId ) {
		$this->m_intArAllocationId = $intArAllocationId;
	}

	public function setApAllocationId( $intApAllocationId ) {
		$this->m_intApAllocationId = $intApAllocationId;
	}

	public function setGlTransactionTypeId( $intGlTransactionTypeId ) {
		$this->m_intGlTransactionTypeId = $intGlTransactionTypeId;
	}

	public function setPostMonth( $strPostMonth ) {
		$this->m_strPostMonth = $strPostMonth;
	}

	public function setPostDate( $strPostDate ) {
		$this->m_strPostDate = $strPostDate;
	}

	public function setGlAccountProperties( $arrmixGlAccountProperties ) {
		$this->m_arrmixGlAccountProperties = $arrmixGlAccountProperties;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = CStrings::strTrimDef( $strUnitNumber, 50, NULL, true );
	}

	public function setPropertyUnits( $arrobjPropertyUnits ) {
		$this->m_arrobjPropertyUnits = $arrobjPropertyUnits;
	}

	public function setGlDetailsProperties( $arrobjGlDetailsProperties ) {
		$this->m_arrobjGlDetailsProperties = $arrobjGlDetailsProperties;
	}

	public function setCompanyDepartmentName( $strCompanyDepartmentName ) {
		$this->m_strCompanyDepartmentName = $strCompanyDepartmentName;
	}

	public function setBillbackIndex( $strBillbackIndex ) {
		$this->m_strBillbackIndex = $strBillbackIndex;
	}

	public function setCustomTagName( $strCustomTagName ) {
		$this->m_strCustomTagName = $strCustomTagName;
	}

	public function setJobPhaseName( $strJobPhaseName ) {
		$this->m_strJobPhaseName = $strJobPhaseName;
	}

	public function setApCodeName( $strApCodeName ) {
		$this->m_strApCodeName = $strApCodeName;
	}

	public function setApContractName( $strApContractName ) {
		$this->m_strApContractName = $strApContractName;
	}

	public function setFromGlAccountName( $strFromGlAccountName ) {
		$this->m_strFromGlAccountName = $strFromGlAccountName;
	}

	public function setToGlAccountName( $strToGlAccountName ) {
		$this->m_strToGlAccountName = $strToGlAccountName;
	}

	public function setFromGlAccountId( $intFromGlAccountId ) {
		$this->m_intFromGlAccountId = $intFromGlAccountId;
	}

	public function setToGlAccountId( $intToGlAccountId ) {
		$this->m_intToGlAccountId = $intToGlAccountId;
	}

	public function setFromGlDetailId( $intFromGlDetailId ) {
		$this->m_intFromGlDetailId = $intFromGlDetailId;
	}

	public function setToGlDetailId( $intToGlDetailId ) {
		$this->m_intToGlDetailId = $intToGlDetailId;
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->m_boolIsDeleted = $boolIsDeleted;
	}

	public function setIsScheduledJEPaused( $boolIsScheduledJePaused ) {
		$this->m_boolIsScheduledJePaused = $boolIsScheduledJePaused;
	}

	public function setAccountingExportBatchId( $intAccountingExportBatchId ) {
		$this->m_intAccountingExportBatchId = $intAccountingExportBatchId;
	}

	public function setDebitAmount( $fltDebitAmount ) {
		$this->m_fltDebitAmount = $fltDebitAmount;
	}

	public function setCreditAmount( $fltCreditAmount ) {
		$this->m_fltCreditAmount = $fltCreditAmount;
	}

	public function setIsFromBillbackListing( $boolIsFromBillbackListing ) {
		$this->m_boolIsFromBillbackListing = $boolIsFromBillbackListing;
	}

	public function setIsDisabledGlAccount( $boolIsDisabledGlAccount ) {
		$this->m_boolIsDisabledGlAccount = $boolIsDisabledGlAccount;
	}

	public function setErrorMsgs( array $arrobjErrorMsgs ) {
		$this->m_arrobjErrorMsgs = $arrobjErrorMsgs;
	}

	public function setIsForParentProperty( $boolIsForParentProperty ) {
		$this->m_boolIsForParentProperty = $boolIsForParentProperty;
	}

	public function setOffsettingGlDetail( $objOffsettingGlDetail ) {
		$this->m_objOffsettingGlDetail = $objOffsettingGlDetail;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['account_number'] ) )			$this->setAccountNumber( $arrmixValues['account_number'] );
		if( true == isset( $arrmixValues['account_name'] ) )			$this->setAccountName( $arrmixValues['account_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )			$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['transaction_datetime'] ) )	$this->setTransactionDatetime( $arrmixValues['transaction_datetime'] );
		if( true == isset( $arrmixValues['ar_transaction_id'] ) )		$this->setArTransactionId( $arrmixValues['ar_transaction_id'] );
		if( true == isset( $arrmixValues['ar_allocation_id'] ) )		$this->setArAllocationId( $arrmixValues['ar_allocation_id'] );
		if( true == isset( $arrmixValues['ap_allocation_id'] ) )		$this->setApAllocationId( $arrmixValues['ap_allocation_id'] );
		if( true == isset( $arrmixValues['gl_transaction_type_id'] ) )	$this->setGlTransactionTypeId( $arrmixValues['gl_transaction_type_id'] );
		if( true == isset( $arrmixValues['post_month'] ) )				$this->setPostMonth( $arrmixValues['post_month'] );
		if( true == isset( $arrmixValues['post_date'] ) )				$this->setPostDate( $arrmixValues['post_date'] );
		if( true == isset( $arrmixValues['unit_number'] ) )				$this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['company_department_name'] ) )	$this->setCompanyDepartmentName( $arrmixValues['company_department_name'] );
		if( true == isset( $arrmixValues['custom_tag_name'] ) ) 		$this->setCustomTagName( $arrmixValues['custom_tag_name'] );
		if( true == isset( $arrmixValues['ap_code_name'] ) )			$this->setApCodeName( $arrmixValues['ap_code_name'] );
		if( true == isset( $arrmixValues['job_phase_name'] ) )			$this->setJobPhaseName( $arrmixValues['job_phase_name'] );
		if( true == isset( $arrmixValues['ap_contract_name'] ) )		$this->setApContractName( $arrmixValues['ap_contract_name'] );
		if( true == isset( $arrmixValues['from_gl_account_id'] ) )		$this->setFromGlAccountId( $arrmixValues['from_gl_account_id'] );
		if( true == isset( $arrmixValues['to_gl_account_id'] ) )		$this->setToGlAccountId( $arrmixValues['to_gl_account_id'] );
		if( true == isset( $arrmixValues['from_gl_account_name'] ) )	$this->setFromGlAccountName( $arrmixValues['from_gl_account_name'] );
		if( true == isset( $arrmixValues['to_gl_account_name'] ) )		$this->setToGlAccountName( $arrmixValues['to_gl_account_name'] );
		if( true == isset( $arrmixValues['is_scheduled_je_paused'] ) ) 	$this->setIsScheduledJEPaused( $arrmixValues['is_scheduled_je_paused'] );
		if( true == isset( $arrmixValues['accounting_export_batch_id'] ) ) 	$this->setAccountingExportBatchId( $arrmixValues['accounting_export_batch_id'] );
		if( true == isset( $arrmixValues['debit_amount'] ) ) 				$this->setDebitAmount( $arrmixValues['debit_amount'] );
		if( true == isset( $arrmixValues['credit_amount'] ) ) 				$this->setCreditAmount( $arrmixValues['credit_amount'] );
		if( true == isset( $arrmixValues['is_from_billback_listing'] ) )	$this->setIsFromBillbackListing( $arrmixValues['is_from_billback_listing'] );
		if( true == isset( $arrmixValues['is_disabled_gl_account'] ) )		$this->setIsDisabledGlAccount( $arrmixValues['is_disabled_gl_account'] );
		if( true == isset( $arrmixValues['is_for_parent_property'] ) )		$this->setIsForParentProperty( $arrmixValues['is_for_parent_property'] );
		if( true == isset( $arrmixValues['is_deleted'] ) )		            $this->setIsDeleted( $arrmixValues['is_deleted'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlHeaderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getGlHeaderId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_header_id', __( 'GL header id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAccrualGlAccountId() {

		$boolIsValid 	= true;

		if( true == is_null( $this->getAccrualGlAccountId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'accrual_gl_account_id', __( 'GL account is required.' ) ) );
			return false;
		}

		$arrmixGlAccountProperties = ( array ) $this->getGlAccountProperties();

		if( true == valId( $this->getAccountNumber() )
		    && true == valArr( $arrmixGlAccountProperties )
		    && ( false == array_key_exists( $this->getAccrualGlAccountId(), $arrmixGlAccountProperties ) )
			&& false == $this->getIsDisabledGlAccount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0} : {%s, 1}" is either not associated with property "{%s, 2}" or disabled.', [ $this->getAccountNumber(), $this->getAccountName(), $this->getPropertyName() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valCashGlAccountId() {

		$boolIsValid 	= true;

		if( true == is_null( $this->getCashGlAccountId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cash_gl_account_id', __( 'GL Account is required.' ) ) );
			return false;
		}

		$arrmixGlAccountProperties = ( array ) $this->getGlAccountProperties();

		if( true == valArr( $arrmixGlAccountProperties )
		    && false == array_key_exists( $this->getCashGlAccountId(), $arrmixGlAccountProperties )
		    && false == $this->getIsDisabledGlAccount() ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0} : {%s, 1}" is not associated with Property {%s, 2}.', [ $this->getAccountNumber(), $this->getAccountName(), $this->getPropertyName() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valFromGlAccountId() {

		$boolIsValid 	= true;

		if( empty( $this->getFromGlAccountId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'From GL Account is required for "{%s, 0}".', [ $this->getPropertyName() ] ) ) );
			return false;
		}

		$arrmixGlAccountProperties = ( array ) $this->getGlAccountProperties();

		if( true == valArr( $arrmixGlAccountProperties ) && false == array_key_exists( $this->getFromGlAccountId(), $arrmixGlAccountProperties ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0} : {%s, 1}" is not associated with Property {%s, 2}.', [ $this->getAccountNumber(), $this->getAccountName(), $this->getPropertyName() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valToGlAccountId() {

		$boolIsValid 	= true;

		if( empty( $this->getToGlAccountId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'To GL Account is required for "{%s, 0}".', [ $this->getPropertyName() ] ) ) );
			return false;
		}

		$arrmixGlAccountProperties = ( array ) $this->getGlAccountProperties();

		if( true == valArr( $arrmixGlAccountProperties ) && false == array_key_exists( $this->getToGlAccountId(), $arrmixGlAccountProperties ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0} : {%s, 1}" is not associated with Property {%s, 2}.', [ $this->getAccountNumber(), $this->getAccountName(), $this->getPropertyName() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valGlReconciliationId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valAmount( $strTemplateType = NULL, $intAmountMethodTypeId = NULL ) {
		$boolIsValid = true;
		if( true == is_null( $this->getAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( 'Amount is required' ) ) );
		} elseif( 0 == ( float ) abs( $this->getAmount() ) && 'manual' != $strTemplateType && CGlHeader::ACCOUNT_BALANCE_METHOD != $intAmountMethodTypeId && false == $this->getIsScheduledJEPaused() ) {
			$boolIsValid = false;
			if( false == $this->getIsFromBillbackListing() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( 'Amount can not be zero.' ) ) );
			} elseif( 0 > $this->getAmount() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( 'Billback amount can not be zero. Allocation property unit or space should not be zero' ) ) );
			}

		} elseif( 100000000000 <= ( float ) abs( $this->getAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( 'Line item amount must be between -$99,999,999,999.99 and $99,999,999,999.99' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId() {

		$boolIsValid = true;

		if( false == is_null( $this->getPropertyUnitId() ) && true == valArr( $this->getPropertyUnits() ) && false == array_key_exists( $this->getPropertyUnitId(), $this->getPropertyUnits() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', __( 'Unit "{%s, 0}" is not associated with property "{%s, 1}".', [ $this->getUnitNumber(), $this->getPropertyName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valGlDetailsProperties( $boolShowDisabledData = false ) {
		$boolIsValid = true;

		$intPropertyId				= $this->getPropertyId();
		$arrobjGlDetailsProperties	= ( array ) $this->getGlDetailsProperties();

		if( true == array_key_exists( $intPropertyId, $arrobjGlDetailsProperties )
		    && 1 == $arrobjGlDetailsProperties[$intPropertyId]->getIsDisabled()
		    && !$boolShowDisabledData ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', __( 'Property \'{%s, 0}\' is disabled.', [ $arrobjGlDetailsProperties[$intPropertyId]->getPropertyName() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valApCodeId() {
		$boolIsValid = true;

		if( true == valId( $this->getJobPhaseId() ) && false == valId( $this->getApCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', __( 'Job Cost Code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valJobCostCode() {
		$boolIsValid = true;

		if( true == valId( $this->getApCodeId() ) && false == valId( $this->getJobPhaseId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_phase_name', __( 'Job is required.' ) ) );
		}

		return $boolIsValid;

	}

	public function valIsDisableGlAccount() {
		$boolIsValid = true;

		if( true == $this->getIsDisabledGlAccount() ) {
			$strGlAccountNumberNameString = $this->getAccountNumber() . ' : ' . $this->getAccountName();
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account \'{%s, 0}\' is disabled.', [ $strGlAccountNumberNameString ] ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $strTemplateType = NULL, $intAmountMethodTypeId = CGlHeader::FIXED_AMOUNT_METHOD, $boolShowDisabledData = false ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlHeaderId();
				$boolIsValid &= $this->valAmount();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valPropertyId();
				if( true == is_numeric( $this->getAccrualGlAccountId() ) ) {
					$boolIsValid &= $this->valAccrualGlAccountId();
					break;
				}
				$boolIsValid &= $this->valCashGlAccountId();
				break;

			case VALIDATE_UPDATE:
			case 'partial_insert_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAmount();
				break;

			case 'journal_entry_insert':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAmount( $strTemplateType, $intAmountMethodTypeId );
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valApCodeId();
				$boolIsValid &= $this->valGlDetailsProperties( $boolShowDisabledData );
				$boolIsValid &= $this->valJobCostCode();
				if( true == $this->getIsFromBillbackListing() ) {
					$boolIsValid &= $this->valIsDisableGlAccount();
				}
				if( CGlHeader::ACCOUNT_BALANCE_METHOD != $intAmountMethodTypeId ) {
					$boolIsValid &= $this->valAmount( $strTemplateType, $intAmountMethodTypeId );
					if( true == is_numeric( $this->getAccrualGlAccountId() ) ) {
						$boolIsValid &= $this->valAccrualGlAccountId();
						break;
					}
					$boolIsValid &= $this->valCashGlAccountId();
				} elseif( true == $this->valPropertyId() ) {
						$boolIsValid &= $this->valFromGlAccountId();
						$boolIsValid &= $this->valToGlAccountId();
				}
				break;

			case 'journal_entry_update':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAmount( $strTemplateType, $intAmountMethodTypeId );
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valApCodeId();
				$boolIsValid &= $this->valGlDetailsProperties( $boolShowDisabledData );
				$boolIsValid &= $this->valJobCostCode();

				if( true == $this->getIsFromBillbackListing() ) {
					$boolIsValid &= $this->valIsDisableGlAccount();
				}

				if( CGlHeader::ACCOUNT_BALANCE_METHOD != $intAmountMethodTypeId ) {
					$boolIsValid &= $this->valAmount( $strTemplateType, $intAmountMethodTypeId );
					if( true == is_numeric( $this->getAccrualGlAccountId() ) ) {
						$boolIsValid &= $this->valAccrualGlAccountId();
						break;
					}
					$boolIsValid &= $this->valCashGlAccountId();
					break;
				} elseif( true == $this->valPropertyId() ) {
					$boolIsValid &= $this->valFromGlAccountId();
					$boolIsValid &= $this->valToGlAccountId();
					break;
				}
				break;

			case 'journal_entry_pause':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valApCodeId();

				if( true == $this->getIsFromBillbackListing() ) {
					$boolIsValid &= $this->valGlDetailsProperties();
					$boolIsValid &= $this->valIsDisableGlAccount();
					$boolIsValid &= $this->valAmount( $strTemplateType, $intAmountMethodTypeId );
				}

				if( true == is_numeric( $this->getAccrualGlAccountId() ) ) {
					$boolIsValid &= $this->valAccrualGlAccountId();
					break;
				}

				$boolIsValid &= $this->valCashGlAccountId();
				$boolIsValid &= $this->valJobCostCode();
				break;

			case 'journal_entry_reverse':
				if( true == is_numeric( $this->getAccrualGlAccountId() ) ) {
					$boolIsValid &= $this->valAccrualGlAccountId();
					break;
				}

				$boolIsValid &= $this->valCashGlAccountId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function createAccountingExportBatchDetail() {

		$objAccountingExportBatchDetail = new CAccountingExportBatchDetail();
		$objAccountingExportBatchDetail->setCid( $this->getCid() );
		$objAccountingExportBatchDetail->setPropertyId( $this->getPropertyId() );
		$objAccountingExportBatchDetail->setAccrualGlAccountId( $this->getAccrualGlAccountId() );
		$objAccountingExportBatchDetail->setCashGlAccountId( $this->getCashGlAccountId() );
		$objAccountingExportBatchDetail->setAmount( $this->getAmount() );
		$objAccountingExportBatchDetail->setTransactionDatetime( $this->getPostDate() );
		$objAccountingExportBatchDetail->setPostMonth( $this->getPostMonth() );

		return $objAccountingExportBatchDetail;
	}

	public function createGlDetailLog() {

		$objGlDetailLog = new CGlDetailLog();

		$objGlDetailLog->setCid( $this->getCid() );
		$objGlDetailLog->setGlHeaderId( $this->getGlHeaderId() );
		$objGlDetailLog->setGlDetailId( $this->getId() );
		$objGlDetailLog->setPropertyId( $this->getPropertyId() );
		$objGlDetailLog->setPropertyUnitId( $this->getPropertyUnitId() );
		$objGlDetailLog->setAccrualGlAccountId( $this->getAccrualGlAccountId() );
		$objGlDetailLog->setCashGlAccountId( $this->getCashGlAccountId() );
		$objGlDetailLog->setGlReconciliationId( $this->getGlReconciliationId() );
		$objGlDetailLog->setApDetailId( $this->getApDetailId() );
		$objGlDetailLog->setOffsettingGlDetailId( $this->getOffsettingGlDetailId() );
		$objGlDetailLog->setPeriodId( $this->getPeriodId() );
		$objGlDetailLog->setAmount( $this->getAmount() );
		$objGlDetailLog->setMemo( $this->getMemo() );
		$objGlDetailLog->setCompanyDepartmentId( $this->getCompanyDepartmentId() );
		$objGlDetailLog->setGlDimensionId( $this->getGlDimensionId() );
		$objGlDetailLog->setIsConfidential( $this->getIsConfidential() );

		return $objGlDetailLog;
	}

	public function toArray() {
		$arrmixGlHeader = parent::toArray();

		$arrmixGlHeader['gl_account_id']                = $arrmixGlHeader['accrual_gl_account_id'] ?? $arrmixGlHeader['cash_gl_account_id'] ?? NULL;
		$arrmixGlHeader['accounting_export_batch_id']   = $this->getAccountingExportBatchId();

		if( 0 < $arrmixGlHeader['amount'] ) {
			$arrmixGlHeader['debit_amount'] = $arrmixGlHeader['amount'];
		} else {
			$arrmixGlHeader['credit_amount'] = ( -1 * $arrmixGlHeader['amount'] );
		}

		return $arrmixGlHeader;
	}

}
?>