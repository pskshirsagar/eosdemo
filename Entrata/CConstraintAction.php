<?php

class CConstraintAction extends CBaseConstraintAction {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRevenueConstraintId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valConditionResult() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOutput() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMessage() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>