<?php

use Psi\Core\AccountsReceivables\ArAllocations\CTransactionAutoAllocationLibrary;

class CArTransaction extends CBaseArTransaction {

	protected $m_arrobjArAllocations;
	protected $m_arrobjAcceptedPaymentTypes;

	protected $m_objLease;
	protected $m_objArCode;
	protected $m_objArPayment;
	protected $m_objPropertyGlSetting;

	// I'm using this variable to record whether we deleted or reversed a transaction.
	protected $m_boolIsReversed;
	protected $m_boolIsProcessed;
	protected $m_boolIsFileUploaded;
	protected $m_boolHasBankAccount;
	// Do not auto write off when payment gets added on past resident. Because in auto write off we are auto allocating the lease so allocations are not retaining properly.
	protected $m_boolExcludeAutoWriteOff;
	protected $m_boolUseUnallocatedAmount;
	protected $m_boolOverridePostMonthPermission;
	protected $m_boolSkipOverrideTransactionDatetime;
	protected $m_boolValidatePastOrFuturePostMonth;
	protected $m_boolApplyToFirstMonth;
	protected $m_boolApplyToLastMonth;
	protected $m_boolUseTransactionDateForAutoAllocation;

	protected $m_intRepaymentId;
	protected $m_intOffsetArDepositId;
	protected $m_intOffsetDepositNumber;
	// This variable helps with posting recurring charges (when we post we have to split month-to-month charges into separate lease charges, and this helps us identify the correct charge)
	protected $m_intParentScheduledChargeId;

	protected $m_intCustomerId;
	protected $m_intArCodeTypeId;
	protected $m_intPaymentTypeId;
	protected $m_intGlAccountTypeId;
	protected $m_intArAllocationId;
	protected $m_intPropertyUnitId;
	protected $m_intDefaultArCodeId;
	// This stores the id of an ar payment a reversal offset transaction should have set.
	protected $m_intArCodeGlAccountId;
	protected $m_intReverseArPaymentId;
	protected $m_intCustomerTypeId;
	protected $m_intDeleteArProcessId;
	protected $m_intCustomerRelationshipId;
	protected $m_intRequireNote;
	protected $m_intHideCredits;
	protected $m_intHideCharges;
	protected $m_intLedgerFilterId;
	protected $m_intDefaultLedgerFilterId;
	protected $m_intOccupancyTypeId;
	protected $m_intArCodePriorityNum;
	protected $m_intDefaultLedgerArCodeId;
	protected $m_intOrganizationContractId;
	protected $m_intInvoiceDefaultDueDays;
	protected $m_intUnitSpaceId;

	protected $m_intPastPostMonth;
	protected $m_intFuturePostMonth;

	protected $m_fltReturnItemFee;
	protected $m_fltTaxChargeAmount;
	protected $m_fltAllocationAmount;
	protected $m_fltRecoveredAmount;
	protected $m_fltDelinquentAmount;
	protected $m_fltBadDebtLimitAmount;
	protected $m_fltTransactionAmountDue;
	protected $m_fltTaxTransactionAmountDue;
	// This stores the amount we are going to offset the original transaction with.
	protected $m_fltReverseTransactionAmount;
	protected $m_fltPositiveTaxAmount;
	protected $m_fltNegativeTaxAmount;

	protected $m_fltMinPositiveTaxPercentage;
	protected $m_fltMaxPositiveTaxPercentage;
	protected $m_fltAvgPositiveTaxPercentage;
	protected $m_fltMinNegativeTaxPercentage;
	protected $m_fltMaxNegativeTaxPercentage;
	protected $m_fltAvgNegativeTaxPercentage;
	protected $m_fltRepaymentAmountDue;

	protected $m_strArCodeName;
	protected $m_strUnitNumber;
	protected $m_strDeleteMemo;
	protected $m_strInvoiceDate;
	protected $m_strInvoiceDueDate;
	protected $m_strPropertyName;
	protected $m_strDeletePostDate;
	protected $m_strDeletePostMonth;
	protected $m_strCustomerNameFull;
	protected $m_strCurrentPostMonth;
	protected $m_strAllocationPostDate;
	protected $m_strAllocationPostMonth;
	protected $m_strDeleteArProcessId;
	protected $m_strScheduledChargeLastPostedOn;
	protected $m_strLedgerFilterName;
	protected $m_strHeaderNumber;
	protected $m_strFileUploadedTokenName;

	protected $m_arrintChargeArTransactionIds;

	// Helps post scheduled charges
	protected $m_strScheduledChargePostedThroughDate;

	protected $m_intArDepositId;
	protected $m_intBankAccountId;

	protected $m_intArTransactionIds;
	protected $m_objOldArTransaction;

	protected $m_arrobjTaxArTransactions;
	protected $m_arrobjDependentArTransactions;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsProcessed                   = false;
		$this->m_boolValidatePastOrFuturePostMonth = false;

		return;
	}

	public function __clone() {
		parent::__clone();
		$this->setDetails( NULL );
	}

	/**
	 * Get Or Fetch Functions
	 */

	public function getOrFetchLease( $objDatabase ) {

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {
			return $this->m_objLease;

		} else {
			return $this->fetchLease( $objDatabase );
		}
	}

	public function getOrFetchArCode( $objDatabase ) {

		if( true == valObj( $this->m_objArCode, 'CArCode' ) ) {
			return $this->m_objArCode;

		} else {
			return $this->fetchArCode( $objDatabase );
		}
	}

	public function getOrFetchArPayment( $objPaymentDatabase ) {

		if( true == valObj( $this->m_objArPayment, 'CArPayment' ) ) {
			return $this->m_objArPayment;

		} else {
			return $this->fetchArPayment( $objPaymentDatabase );
		}
	}

	public function getOrFetchPropertyGlSetting( $objDatabase ) {

		if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			return $this->m_objPropertyGlSetting;

		} else {
			return $this->fetchPropertyGlSetting( $objDatabase );
		}
	}

	public function getOrFetchPostMonth( $objDatabase ) {

		if( true == is_null( $this->getPostMonth() ) ) {
			$objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
				trigger_error( __( 'Property GL Setting could not be loaded.' ) );
				exit;
			}
			$this->setPostMonth( $objPropertyGlSetting->getArPostMonth() );
		}

		return $this->m_strPostMonth;
	}

	// Sometimes we need the current post month while resetting pre-payment allocations.  If we load the post month of the original payment, it can cause issues.

	public function getOrFetchCurrentPostMonth( $objDatabase ) {

		if( true == is_null( $this->getCurrentPostMonth() ) ) {

			$objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
				trigger_error( __( 'Property GL Setting could not be loaded.' ) );
				exit;
			}

			$this->setCurrentPostMonth( $objPropertyGlSetting->getArPostMonth() );
		}

		return $this->m_strCurrentPostMonth;
	}

	public function getOrFetchPostMonthValidationData( $intCompanyUserId, $objDatabase ) {

		// check if user is admin or not
		$arrstrCompanyUser = ( array ) \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserDetailsByIdByCid( $intCompanyUserId, $this->getCid(), [ 'is_administrator' ], $objDatabase );
		if( true == isset( $arrstrCompanyUser[0] ) && 1 == getArrayElementByKey( 'is_administrator', $arrstrCompanyUser[0] ) ) return;

		if( false == isset( $this->m_intPastPostMonth ) && false == isset( $this->m_intFuturePostMonth ) ) {
			$this->setValidatePastOrFuturePostMonth( true );
			$this->setPastPostMonthAndFuturePostMonth( $intCompanyUserId, $objDatabase );
		}

		$arrstrPostMonthValidationData = [
			'past_post_month'		=> $this->getPastPostMonth(),
			'future_post_month'		=> $this->getFuturePostMonth(),
			'validate_post_month'	=> $this->getValidatePastOrFuturePostMonth()
		];

		return $arrstrPostMonthValidationData;
	}

	/**
	 * Get Functions
	 */

	public function getScheduledChargePostedThroughDate() {
		return $this->m_strScheduledChargePostedThroughDate;
	}

	public function getOffsetDepositNumber() {
		return $this->m_intOffsetDepositNumber;
	}

	public function getOffsetArDepositId() {
		return $this->m_intOffsetArDepositId;
	}

	public function getArAllocations() {
		return $this->m_arrobjArAllocations;
	}

	public function getBadDebtLimitAmount() {
		return $this->m_fltBadDebtLimitAmount;
	}

	public function getDelinquentAmount() {
		return $this->m_fltDelinquentAmount;
	}

	public function getArAllocationId() {
		return $this->m_intArAllocationId;
	}

	public function getParentScheduledChargeId() {
		return $this->m_intParentScheduledChargeId;
	}

	public function getReturnItemFee() {
		return $this->m_fltReturnItemFee;
	}

	public function getDefaultArCodeId() {
		return $this->m_intDefaultArCodeId;
	}

	public function getReverseArPaymentId() {
		return $this->m_intReverseArPaymentId;
	}

	public function getReverseTransactionAmount() {
		return $this->m_fltReverseTransactionAmount;
	}

	public function getChargeArTransactionIds() {
		return $this->m_arrintChargeArTransactionIds;
	}

	public function getFormattedPostMonth() {
		if( true == is_null( $this->m_strPostMonth ) ) return NULL;
		return \Psi\CStringService::singleton()->preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getFormattedTransactionDate() {
		if( true == is_null( $this->m_strPostDate ) ) return NULL;
		return date( 'm/d/Y', strtotime( $this->m_strPostDate ) );
	}

	// @FIXME: Can we club getFormattedDeletePostMonth & getFormattedAllocationPostMonth ?

	public function getFormattedDeletePostMonth() {
		if( true == is_null( $this->m_strDeletePostMonth ) ) return NULL;
		return \Psi\CStringService::singleton()->preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strDeletePostMonth );
	}

	// @FIXME: Can we club getFormattedDeletePostMonth & getFormattedAllocationPostMonth ?

	public function getFormattedAllocationPostMonth() {
		if( true == is_null( $this->m_strAllocationPostMonth ) ) return NULL;
		return \Psi\CStringService::singleton()->preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strAllocationPostMonth );
	}

	// @FIXME: Can below function be removed and webMoneyFormat function can be used directly?

	public function getFormattedTransactionAmount() {
		if( 0 > $this->getTransactionAmount() ) {
			return '(' . __( '{%m, 0, nots}', [ abs( $this->getTransactionAmount() ) ] ) . ')';
		} else {
			return __( '{%m, 0, nots}', [ $this->getTransactionAmount() ] );
		}
	}

	// @FIXME: Can below function be removed and webMoneyFormat function can be used directly?

	public function getFormattedTransactionAmountDue() {
		if( 0 > $this->getTransactionAmountDue() ) {
			return '(' . __( '{%m, 0, nots}', [ abs( $this->getTransactionAmountDue() ) ] ) . ')';
		} else {
			return __( '{%m, 0, nots}', [ $this->getTransactionAmountDue() ] );
		}
	}

	public function getDeleteArProcessId() {
		return $this->m_intDeleteArProcessId;
	}

	public function getDeletePostMonth() {
		return $this->m_strDeletePostMonth;
	}

	public function getDeletePostDate() {
		return $this->m_strDeletePostDate;
	}

	public function getInvoiceDate() {
		return $this->m_strInvoiceDate;
	}

	public function getInvoiceDueDate() {
		return $this->m_strInvoiceDueDate;
	}

	public function getDeleteMemo() {
		return $this->m_strDeleteMemo;
	}

	public function getAllocationPostDate() {
		return $this->m_strAllocationPostDate;
	}

	public function getAllocationPostMonth() {
		return $this->m_strAllocationPostMonth;
	}

	public function getUseUnallocatedAmount() {
		return $this->m_boolUseUnallocatedAmount;
	}

	public function getArPayment() {
		return $this->m_objArPayment;
	}

	public function getIsProcessed() {
		return $this->m_boolIsProcessed;
	}

	public function getIsReversed() {
		return $this->m_boolIsReversed;
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function getCurrentPostMonth() {
		return $this->m_strCurrentPostMonth;
	}

	public function getPostDateOnly() {
		return \Psi\CStringService::singleton()->substr( $this->getPostDate(), 0, 10 );
	}

	public function getConcessionAmount() {
		return -1.00 * ( float ) $this->getTransactionAmount();
	}

	public function getAllocationAmount() {
		return -1.00 * ( float ) $this->m_fltAllocationAmount;
	}

	public function getRecoveredAmount() {
		return ( float ) $this->m_fltRecoveredAmount;
	}

	public function getUnallocatedAmount() {
		return -1.00 * ( float ) $this->getTransactionAmountDue();
	}

	public function getPaymentAmount() {
		return -1.00 * ( float ) $this->getTransactionAmount();
	}

	public function getAcceptedPaymentTypes() {
		return $this->m_arrobjAcceptedPaymentTypes;
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function getScheduledChargeLastPostedOn() {
		return $this->m_strScheduledChargeLastPostedOn;
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function getLease() {
		return $this->m_objLease;
	}

	public function getIsConcessionTransaction() {
		$boolIsConcession = false;

		if( 0 > $this->getTransactionAmount() && CDefaultArCode::PAYMENT != $this->getDefaultArCodeId() ) {
			$boolIsConcession = true;
		}

		return $boolIsConcession;
	}

	public function getIsPaymentTransaction() {
		return ( CArCodeType::PAYMENT == $this->getArCodeTypeId() ) ? true : false;
	}

	public function getTransactionAmountDue() {
		return $this->m_fltTransactionAmountDue;
	}

	public function getPositiveTaxAmount() {
		return $this->m_fltPositiveTaxAmount;
	}

	public function getNegativeTaxAmount() {
		return $this->m_fltNegativeTaxAmount;
	}

	public function getTaxTransactionAmountDue() {
		return $this->m_fltTaxTransactionAmountDue;
	}

	public function getMinTaxPositivePercentage() {
		return $this->m_fltMinPositiveTaxPercentage;
	}

	public function getMaxTaxPositivePercentage() {
		return $this->m_fltMaxPositiveTaxPercentage;
	}

	public function getAvgTaxPositivePercentage() {
		return $this->m_fltAvgPositiveTaxPercentage;
	}

	public function getMinTaxNegativePercentage() {
		return $this->m_fltMinNegativeTaxPercentage;
	}

	public function getMaxTaxNegativePercentage() {
		return $this->m_fltMaxNegativeTaxPercentage;
	}

	public function getAvgTaxNegativePercentage() {
		return $this->m_fltAvgNegativeTaxPercentage;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function getHasBankAccount() {
		return $this->m_boolHasBankAccount;
	}

	public function getArCodeGlAccountId() {
		return $this->m_intArCodeGlAccountId;
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getCustomerNameFull() {
		return $this->m_strCustomerNameFull;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getTaxChargeAmount() {
		return $this->m_fltTaxChargeAmount;
	}

	public function getArDepositId() {
		return $this->m_intArDepositId;
	}

	public function getRequireNote() {
		return $this->m_intRequireNote;
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function getLedgerFilterName() {
		return $this->m_strLedgerFilterName;
	}

	public function getOverridePostMonthPermission() {
		return $this->m_boolOverridePostMonthPermission;
	}

	public function getHeaderNumber() {
	    return $this->m_strHeaderNumber;
	}

	public function getFileUploadedTokenName() {
		return $this->m_strFileUploadedTokenName;
	}

	public function getIsFileUploaded() {
		return $this->m_boolIsFileUploaded;
	}

	public function getHideCredits() {
		return $this->m_intHideCredits;
	}

	public function getHideCharges() {
		return $this->m_intHideCharges;
	}

	public function getRepaymentId() {
		return $this->m_intRepaymentId;
	}

	public function getSkipOverrideTransactionDatetime() {
		return $this->m_boolSkipOverrideTransactionDatetime;
	}

	public function getPastPostMonth() {
		return $this->m_intPastPostMonth;
	}

	public function getFuturePostMonth() {
		return $this->m_intFuturePostMonth;
	}

	public function getValidatePastOrFuturePostMonth() {
		return $this->m_boolValidatePastOrFuturePostMonth;
	}

	public function getApplyToFirstMonth() {
		return $this->m_boolApplyToFirstMonth;
	}

	public function getApplyToLastMonth() {
		return $this->m_boolApplyToLastMonth;
	}

	public function getDefaultLedgerFilterId() {
		return $this->m_intDefaultLedgerFilterId;
	}

	public function getExcludeAutoWriteOff() {
		return $this->m_boolExcludeAutoWriteOff;
	}

	public function getArTransactionIds() {
		return $this->m_intArTransactionIds;
	}

	public function getDataForValidatePastOrFuturePostMonth( $objCompanyUser, $objDatabase ) {

		$intPastPostMonth                   = $intFuturePostMonth = NULL;
		$boolIsAdministrator                = $objCompanyUser->getIsAdministrator();
		$boolValidatePastOrFuturePostMonth  = !$boolIsAdministrator;

		if( false == $boolIsAdministrator ) {
			$arrstrCompanyUserPreferences = \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferencesByCompanyUserIdByCidByModuleIdByKeys( $objCompanyUser->getId(), $this->getCid(), CModule::AR_POST_MONTH, [ CCompanyUserPreference::AR_FUTURE_POST_MONTH, CCompanyUserPreference::AR_PAST_POST_MONTH ], $objDatabase );
			if( true == valArr( $arrstrCompanyUserPreferences, 2 ) ) {
				$intPastPostMonth   = $arrstrCompanyUserPreferences[CCompanyUserPreference::AR_PAST_POST_MONTH];
				$intFuturePostMonth = $arrstrCompanyUserPreferences[CCompanyUserPreference::AR_FUTURE_POST_MONTH];
			}
		}

		return [ 'past_post_month' => $intPastPostMonth, 'future_post_month' => $intFuturePostMonth, 'is_validate_past_or_future_post_month' => $boolValidatePastOrFuturePostMonth ];
	}

	public function getOldArTransaction() {
		return $this->m_objOldArTransaction;
	}

	public function getMemo() {
		$objArTransactionMemo = new CArTransactionMemoLibrary;

		$arrstrDetails = json_decode( json_encode( $this->getDetailsField( [] ) ?? new stdClass() ), true );
		return $objArTransactionMemo->getMemo( $arrstrDetails, parent::getMemo() );
	}

	public function getHasTranslatedMemo() {
		return false == empty( $this->getDetailsField( [ 'memo_data', 'memo_template' ] ) );
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function getTaxArTransactions() {
		return $this->m_arrobjTaxArTransactions;
	}

	/**
	 * @return $this[]
	 */
	public function getDependentArTransactions() {
		return $this->m_arrobjDependentArTransactions;
	}

	public function getAmountPaid( int $intPrecision = 6 ) {
		return bcsub( ( string ) $this->getTransactionAmount(), ( string ) $this->getTransactionAmountDue(), $intPrecision );
	}

	public function getAllocationFactor() {
		return ( float ) bcdiv( ( string ) $this->getAmountPaid(), ( string ) $this->getTransactionAmount(), 6 );
	}

	public function getArCodePriorityNum() {
		return $this->m_intArCodePriorityNum;
	}

	public function getDefaultLedgerArCodeId() {
		return $this->m_intDefaultLedgerArCodeId;
	}

	public function getIsRepaymentTransaction() {
		return true == valId( $this->getRepaymentId() );
	}

	public function getRepaymentAmountDue() {
		return $this->m_fltRepaymentAmountDue;
	}

	public function getDueNow() {
		return $this->getRepaymentAmountDue() ?? $this->getTransactionAmountDue();
	}

	public function getLedgerFilterId() {
		return $this->m_intLedgerFilterId;
	}

	public function getOrganizationContractId() {
		return $this->m_intOrganizationContractId;
	}

	public function getInvoiceDefaultDueDays() {
		return $this->m_intInvoiceDefaultDueDays;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function getUseTransactionDateForAutoAllocation() {
		return $this->m_boolUseTransactionDateForAutoAllocation;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_allocation_id'] ) ) $this->setArAllocationId( $arrmixValues['ar_allocation_id'] );
		if( true == isset( $arrmixValues['scheduled_charge_id'] ) ) $this->setScheduledChargeId( $arrmixValues['scheduled_charge_id'] );
		if( true == isset( $arrmixValues['scheduled_charge_last_posted_on'] ) ) $this->setScheduledChargeLastPostedOn( $arrmixValues['scheduled_charge_last_posted_on'] );
		if( true == isset( $arrmixValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrmixValues['gl_account_type_id'] );
		if( true == isset( $arrmixValues['customer_relationship_id'] ) ) $this->setCustomerRelationshipId( $arrmixValues['customer_relationship_id'] );
		if( true == isset( $arrmixValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
		if( true == isset( $arrmixValues['ar_code_name'] ) ) $this->setArCodeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['ar_code_name'] ) : $arrmixValues['ar_code_name'] );
		if( true == isset( $arrmixValues['transaction_amount_due'] ) ) $this->setTransactionAmountDue( $arrmixValues['transaction_amount_due'] );
		if( true == isset( $arrmixValues['tax_transaction_amount_due'] ) ) $this->setTaxTransactionAmountDue( $arrmixValues['tax_transaction_amount_due'] );
		if( true == isset( $arrmixValues['concession_amount'] ) ) $this->setConcessionAmount( $arrmixValues['concession_amount'] );
		if( true == isset( $arrmixValues['allocation_amount'] ) ) $this->setAllocationAmount( $arrmixValues['allocation_amount'] );
		if( true == isset( $arrmixValues['recovered_amount'] ) ) $this->setRecoveredAmount( $arrmixValues['recovered_amount'] );
		if( true == isset( $arrmixValues['post_date_only'] ) ) $this->setPostDateOnly( $arrmixValues['post_date_only'] );
		if( true == isset( $arrmixValues['unallocated_amount'] ) ) $this->setUnallocatedAmount( $arrmixValues['unallocated_amount'] );
		if( true == isset( $arrmixValues['allocation_post_date'] ) ) $this->setAllocationPostDate( $arrmixValues['allocation_post_date'] );
		if( true == isset( $arrmixValues['allocation_post_month'] ) ) $this->setAllocationPostMonth( $arrmixValues['allocation_post_month'] );
		if( true == isset( $arrmixValues['use_unallocated_amount'] ) ) $this->setUseUnallocatedAmount( $arrmixValues['use_unallocated_amount'] );
		if( true == isset( $arrmixValues['delete_post_month'] ) ) $this->setDeletePostMonth( $arrmixValues['delete_post_month'] );
		if( true == isset( $arrmixValues['delete_post_date'] ) ) $this->setDeletePostDate( $arrmixValues['delete_post_date'] );
		if( true == isset( $arrmixValues['delete_memo'] ) ) $this->setDeleteMemo( $arrmixValues['delete_memo'] );
		if( true == isset( $arrmixValues['return_item_fee'] ) ) $this->setReturnItemFee( $arrmixValues['return_item_fee'] );
		if( true == isset( $arrmixValues['default_ar_code_id'] ) ) $this->setDefaultArCodeId( $arrmixValues['default_ar_code_id'] );
		if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		if( true == isset( $arrmixValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrmixValues['payment_type_id'] );
		if( true == isset( $arrmixValues['has_bank_account'] ) ) $this->setHasBankAccount( $arrmixValues['has_bank_account'] );
		if( true == isset( $arrmixValues['offset_deposit_number'] ) ) $this->setOffsetDepositNumber( $arrmixValues['offset_deposit_number'] );
		if( true == isset( $arrmixValues['offset_ar_deposit_id'] ) ) $this->setOffsetArDepositId( $arrmixValues['offset_ar_deposit_id'] );
		if( true == isset( $arrmixValues['ar_code_gl_account_id'] ) ) $this->setArCodeGlAccountId( $arrmixValues['ar_code_gl_account_id'] );
		if( true == isset( $arrmixValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrmixValues['ar_code_type_id'] );
		if( true == isset( $arrmixValues['delinquent_amount'] ) ) $this->setDelinquentAmount( $arrmixValues['delinquent_amount'] );
		if( true == isset( $arrmixValues['repayment_id'] ) ) $this->setRepaymentId( $arrmixValues['repayment_id'] );
		if( true == isset( $arrmixValues['repayment_amount_due'] ) ) $this->setRepaymentAmountDue( $arrmixValues['repayment_amount_due'] );

		if( true == isset( $arrmixValues['file_uploaded_token_name'] ) ) $this->setFileUploadedTokenName( $arrmixValues['file_uploaded_token_name'] );
		if( true == isset( $arrmixValues['is_file_uploaded'] ) ) $this->setIsFileUploaded( $arrmixValues['is_file_uploaded'] );

		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['customer_name_full'] ) ) $this->setCustomerNameFull( $arrmixValues['customer_name_full'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) $this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['tax_charge_amount'] ) ) $this->setTaxChargeAmount( $arrmixValues['tax_charge_amount'] );
		if( true == isset( $arrmixValues['ar_deposit_id'] ) ) $this->setArDepositId( $arrmixValues['ar_deposit_id'] );
		if( true == isset( $arrmixValues['require_note'] ) ) $this->setRequireNote( $arrmixValues['require_note'] );
		if( true == isset( $arrmixValues['bank_account_id'] ) ) $this->setBankAccountId( $arrmixValues['bank_account_id'] );
		if( true == isset( $arrmixValues['ledger_filter_name'] ) ) $this->setLedgerFilterName( $arrmixValues['ledger_filter_name'] );
		if( true == isset( $arrmixValues['default_ledger_filter_id'] ) ) $this->setDefaultLedgerFilterId( $arrmixValues['default_ledger_filter_id'] );
		if( true == isset( $arrmixValues['ledger_filter_id'] ) ) $this->setLedgerFilterId( $arrmixValues['ledger_filter_id'] );
		if( true == isset( $arrmixValues['header_number'] ) ) $this->setHeaderNumber( $arrmixValues['header_number'] );
		if( true == isset( $arrmixValues['organization_contract_id'] ) ) $this->setOrganizationContractId( $arrmixValues['organization_contract_id'] );
		if( true == isset( $arrmixValues['unit_space_id'] ) ) $this->setUnitSpaceId( $arrmixValues['unit_space_id'] );
		if( true == isset( $arrmixValues['invoice_default_due_days'] ) ) $this->setInvoiceDefaultDueDays( $arrmixValues['invoice_default_due_days'] );

		if( true == isset( $arrmixValues['hide_credits'] ) ) $this->setHideCredits( $arrmixValues['hide_credits'] );
		if( true == isset( $arrmixValues['hide_charges'] ) ) $this->setHideCharges( $arrmixValues['hide_charges'] );

		if( true == isset( $arrmixValues['ar_transaction_ids'] ) ) $this->setArTransactionIds( $arrmixValues['ar_transaction_ids'] );
		if( true == isset( $arrmixValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
		if( true == isset( $arrmixValues['ar_code_priority_num'] ) ) $this->setArCodePriorityNum( $arrmixValues['ar_code_priority_num'] );

		if( true == isset( $arrmixValues['min_positive_tax'] ) ) $this->setMinTaxPositivePercentage( $arrmixValues['min_positive_tax'] );
		if( true == isset( $arrmixValues['max_positive_tax'] ) ) $this->setMaxTaxPositivePercentage( $arrmixValues['max_positive_tax'] );
		if( true == isset( $arrmixValues['avg_positive_tax'] ) ) $this->setAvgTaxPositivePercentage( $arrmixValues['avg_positive_tax'] );
		if( true == isset( $arrmixValues['min_negative_tax'] ) ) $this->setMinTaxNegativePercentage( $arrmixValues['min_negative_tax'] );
		if( true == isset( $arrmixValues['max_negative_tax'] ) ) $this->setMaxTaxNegativePercentage( $arrmixValues['max_negative_tax'] );
		if( true == isset( $arrmixValues['avg_negative_tax'] ) ) $this->setAvgTaxNegativePercentage( $arrmixValues['avg_negative_tax'] );
		if( true == isset( $arrmixValues['default_ledger_ar_code_id'] ) ) $this->setDefaultLedgerArCodeId( $arrmixValues['default_ledger_ar_code_id'] );
		if( true == isset( $arrmixValues['positive_tax_amount'] ) ) $this->setPositiveTaxAmount( $arrmixValues['positive_tax_amount'] );
		if( true == isset( $arrmixValues['negative_tax_amount'] ) ) $this->setNegativeTaxAmount( $arrmixValues['negative_tax_amount'] );
		if( true == isset( $arrmixValues['invoice_date'] ) ) $this->setInvoiceDate( $arrmixValues['invoice_date'] );
		if( true == isset( $arrmixValues['invoice_due_date'] ) ) $this->setInvoiceDueDate( $arrmixValues['invoice_due_date'] );
		if( true == isset( $arrmixValues['apply_to_first_month'] ) ) $this->setApplyToFirstMonth( $arrmixValues['apply_to_first_month'] );
		if( true == isset( $arrmixValues['apply_to_last_month'] ) ) $this->setApplyToLastMonth( $arrmixValues['apply_to_last_month'] );

		$this->m_arrmixCustomVariables['user'] = ( isset( $arrmixValues['user'] ) ) ? trim( $arrmixValues['user'] ) : '';

		return;
	}

	public function setOffsetDepositNumber( $intOffsetDepositNumber ) {
		$this->m_intOffsetDepositNumber = $intOffsetDepositNumber;
	}

	public function setOffsetArDepositId( $intOffsetArDepositId ) {
		$this->m_intOffsetArDepositId = $intOffsetArDepositId;
	}

	public function setArAllocationId( $intArAllocationId ) {
		$this->m_intArAllocationId = $intArAllocationId;
	}

	public function setBadDebtLimitAmount( $fltBadDebtLimitAmount ) {
		return $this->m_fltBadDebtLimitAmount = $fltBadDebtLimitAmount;
	}

	public function setDelinquentAmount( $fltDelinquentAmount ) {
		return $this->m_fltDelinquentAmount = $fltDelinquentAmount;
	}

	public function setScheduledChargePostedThroughDate( $strScheduledChargePostedThroughDate ) {
		$this->m_strScheduledChargePostedThroughDate = $strScheduledChargePostedThroughDate;
	}

	public function setParentScheduledChargeId( $intParentScheduledChargeId ) {
		$this->m_intParentScheduledChargeId = $intParentScheduledChargeId;
	}

	public function setDefaultArCodeId( $intDefaultArCodeId ) {
		$this->m_intDefaultArCodeId = $intDefaultArCodeId;
	}

	public function setReverseArPaymentId( $intReverseArPaymentId ) {
		$this->m_intReverseArPaymentId = $intReverseArPaymentId;
	}

	public function setReverseTransactionAmount( $fltReverseTransactionAmount ) {
		$this->m_fltReverseTransactionAmount = $fltReverseTransactionAmount;
	}

	public function setPropertyGlSetting( $objPropertyGlSetting ) {
		$this->m_objPropertyGlSetting = $objPropertyGlSetting;
	}

	public function setArAllocations( $arrobjArAllocations ) {
		$this->m_arrobjArAllocations = $arrobjArAllocations;
	}

	public function setArPayment( $objArPayment ) {
		$this->m_objArPayment = $objArPayment;
	}

	public function setReturnItemFee( $fltReturnItemFee ) {
		$this->m_fltReturnItemFee = ( float ) str_replace( '$', '', $fltReturnItemFee );
	}

	public function setDeleteMemo( $strDeleteMemo ) {
		$this->m_strDeleteMemo = $strDeleteMemo;
	}

	public function setChargeArTransactionIds( $arrintChargeArTransactionIds ) {
		$this->m_arrintChargeArTransactionIds = $arrintChargeArTransactionIds;
	}

	public function setDeleteArProcessId( $intDeleteArProcessId ) {
		$this->m_intDeleteArProcessId = $intDeleteArProcessId;
	}

	public function setDeletePostMonth( $strDeletePostMonth ) {

		$arrstrDeletePostMonth = explode( '/', $strDeletePostMonth );

		if( true == valArr( $arrstrDeletePostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrDeletePostMonth ) ) {
			$strDeletePostMonth = $arrstrDeletePostMonth[0] . '/1/' . $arrstrDeletePostMonth[1];
		} elseif( true == valArr( $arrstrDeletePostMonth ) && 3 == \Psi\Libraries\UtilFunctions\count( $arrstrDeletePostMonth ) ) {
			$strDeletePostMonth = $arrstrDeletePostMonth[0] . '/1/' . $arrstrDeletePostMonth[2];
		} else {
			$strDeletePostMonth = NULL;
		}

		$this->m_strDeletePostMonth = $strDeletePostMonth;
	}

	public function setDeletePostDate( $strDeletePostDate ) {
		$this->m_strDeletePostDate = $strDeletePostDate;
	}

	public function setInvoiceDate( $strInvoiceDate ) {
		$this->set( 'm_strInvoiceDate', CStrings::strTrimDef( $strInvoiceDate, -1, NULL, true ) );
	}

	public function setInvoiceDueDate( $strInvoiceDueDate ) {
		$this->set( 'm_strInvoiceDueDate', CStrings::strTrimDef( $strInvoiceDueDate, -1, NULL, true ) );
	}

	public function setUseUnallocatedAmount( $boolUseUnallocatedAmount ) {
		$this->m_boolUseUnallocatedAmount = $boolUseUnallocatedAmount;
	}

	public function setAllocationPostDate( $strAllocationPostDate ) {
		$this->m_strAllocationPostDate = $strAllocationPostDate;
	}

	public function setAllocationPostMonth( $strAllocationPostMonth ) {
		$this->m_strAllocationPostMonth = $strAllocationPostMonth;
	}

	public function setIsProcessed( $boolIsProcessed ) {
		$this->m_boolIsProcessed = $boolIsProcessed;
	}

	public function setPostDateOnly( $strPostDateOnly ) {
		$this->setPostDate( $strPostDateOnly );
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->m_intGlAccountTypeId = $intGlAccountTypeId;
	}

	public function setCurrentPostMonth( $strCurrentPostMonth ) {
		$this->m_strCurrentPostMonth = $strCurrentPostMonth;
	}

	public function setConcessionAmount( $fltConcessionAmount ) {
		$this->setTransactionAmount( -1.00 * ( float ) CStrings::strToFloatDef( $fltConcessionAmount, NULL, false, 4 ) );
	}

	public function setAllocationAmount( $fltAllocationAmount ) {
		$this->m_fltAllocationAmount = ( -1.00 * ( float ) CStrings::strToFloatDef( $fltAllocationAmount, NULL, false, 4 ) );
	}

	public function setRecoveredAmount( $fltRecoveredAmount ) {
		$this->m_fltRecoveredAmount = ( float ) CStrings::strToFloatDef( $fltRecoveredAmount, NULL, false, 4 );
	}

	public function setUnallocatedAmount( $fltUnallocatedAmount ) {
		$this->setTransactionAmountDue( -1.00 * ( float ) CStrings::strToFloatDef( $fltUnallocatedAmount, NULL, false, 4 ) );
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->setTransactionAmount( -1.00 * ( float ) CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 4 ) );
	}

	public function setAcceptedPaymentTypes( $arrobjPaymentTypes ) {
		$this->m_arrobjAcceptedPaymentTypes = $arrobjPaymentTypes;
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->m_intCustomerRelationshipId = CStrings::strToIntDef( $intCustomerRelationshipId, NULL, true );
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->m_intCustomerTypeId = CStrings::strToIntDef( $intCustomerTypeId, NULL, true );
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = CStrings::strTrimDef( $strArCodeName, 240, NULL, true );
	}

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setScheduledChargeLastPostedOn( $strScheduledChargeLastPostedOn ) {
		$this->m_strScheduledChargeLastPostedOn = CStrings::strTrimDef( $strScheduledChargeLastPostedOn, 240, NULL, true );
	}

	public function setTransactionAmountDue( $fltTransactionAmountDue ) {
		$this->m_fltTransactionAmountDue = CStrings::strToFloatDef( $fltTransactionAmountDue, NULL, true, 4 );
	}

	public function setPositiveTaxAmount( $fltPositiveTaxAmount ) {
		$this->m_fltPositiveTaxAmount = CStrings::strToFloatDef( $fltPositiveTaxAmount, NULL, true, 4 );
	}

	public function setNegativeTaxAmount( $fltNegativeTaxAmount ) {
		$this->m_fltNegativeTaxAmount = CStrings::strToFloatDef( $fltNegativeTaxAmount, NULL, true, 4 );
	}

	public function setTaxTransactionAmountDue( $fltTaxTransactionAmountDue ) {
		$this->m_fltTaxTransactionAmountDue = CStrings::strToFloatDef( $fltTaxTransactionAmountDue, NULL, true, 4 );
	}

	public function setMinTaxPositivePercentage( $fltTaxPercentage ) {
		$this->m_fltMinPositiveTaxPercentage = $fltTaxPercentage;
	}

	public function setMaxTaxPositivePercentage( $fltTaxPercentage ) {
		$this->m_fltMaxPositiveTaxPercentage = $fltTaxPercentage;
	}

	public function setAvgTaxPositivePercentage( $fltTaxPercentage ) {
		$this->m_fltAvgPositiveTaxPercentage = $fltTaxPercentage;
	}

	public function setMinTaxNegativePercentage( $fltTaxPercentage ) {
		$this->m_fltMinNegativeTaxPercentage = $fltTaxPercentage;
	}

	public function setMaxTaxNegativePercentage( $fltTaxPercentage ) {
		$this->m_fltMaxNegativeTaxPercentage = $fltTaxPercentage;
	}

	public function setAvgTaxNegativePercentage( $fltTaxPercentage ) {
		$this->m_fltAvgNegativeTaxPercentage = $fltTaxPercentage;
	}

	public function setPostMonth( $strPostMonth ) {
		$arrstrPostMonth = explode( '/', $strPostMonth );
		if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
			$strPostMonth = $arrstrPostMonth[0] . '/1/' . $arrstrPostMonth[1];
		}

		$this->m_strPostMonth = $strPostMonth;
	}

	public function setMemo( $strMemo ) {
		$strMemo = \Psi\CStringService::singleton()->htmlspecialchars( $strMemo );
		parent::setMemo( CStrings::strTrimDef( $strMemo, 2000, NULL, true, false, true ) );
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->m_intPaymentTypeId = $intPaymentTypeId;
	}

	public function setHasBankAccount( $boolHasBankAccount ) {
		$this->m_boolHasBankAccount = $boolHasBankAccount;
	}

	public function setArCodeGlAccountId( $intArCodeGlAccountId ) {
		$this->m_intArCodeGlAccountId = $intArCodeGlAccountId;
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->m_intArCodeTypeId = $intArCodeTypeId;
	}

	public function setPropertyName( $strPropertyName ) {
		return $this->m_strPropertyName = $strPropertyName;
	}

	public function setCustomerNameFull( $strCustomerNameFull ) {
		return $this->m_strCustomerNameFull = $strCustomerNameFull;
	}

	public function setUnitNumber( $strUnitNumber ) {
		return $this->m_strUnitNumber = $strUnitNumber;
	}

	public function setTaxChargeAmount( $fltTaxChargeAmount ) {
		$this->m_fltTaxChargeAmount = ( float ) str_replace( '$', '', $fltTaxChargeAmount );
	}

	public function setArDepositId( $intArDepositId ) {
		return $this->m_intArDepositId = $intArDepositId;
	}

	public function setRequireNote( $intRequireNote ) {
		return $this->m_intRequireNote = CStrings::strToIntDef( $intRequireNote, NULL, false );
	}

	public function setBankAccountId( $intBankAccountId ) {
		return $this->m_intBankAccountId = $intBankAccountId;
	}

	public function setLedgerFilterName( $strLedgerFilterName ) {
		return $this->m_strLedgerFilterName = $strLedgerFilterName;
	}

	public function setOverridePostMonthPermission( $boolOverridePostMonthPermission ) {
		$this->m_boolOverridePostMonthPermission = $boolOverridePostMonthPermission;
	}

	public function setHeaderNumber( $strHeaderNumber ) {
	    $this->m_strHeaderNumber = $strHeaderNumber;
	}

	public function setHideCharges( $intHideCharges ) {
		$this->m_intHideCharges = $intHideCharges;
	}

	public function setFileUploadedTokenName( $strFileUploadedTokenName ) {
		$this->m_strFileUploadedTokenName = $strFileUploadedTokenName;
	}

	public function setIsFileUploaded( $boolIsFileUploaded ) {
		$this->m_boolIsFileUploaded = $boolIsFileUploaded;
	}

	public function setHideCredits( $intHideCredits ) {
		$this->m_intHideCredits = $intHideCredits;
	}

	public function setRepaymentId( $intRepaymentId ) {
		$this->m_intRepaymentId = CStrings::strToIntDef( $intRepaymentId, NULL, true );
	}

	public function setSkipOverrideTransactionDatetime( $boolSkipOverrideTransactionDatetime ) {
		$this->m_boolSkipOverrideTransactionDatetime = $boolSkipOverrideTransactionDatetime;
	}

	public function setPastPostMonth( $intPastPostMonth ) {
		$this->m_intPastPostMonth = $intPastPostMonth;
	}

	public function setFuturePostMonth( $intFuturePostMonth ) {
		$this->m_intFuturePostMonth = $intFuturePostMonth;
	}

	public function setPastPostMonthAndFuturePostMonth( $intCompanyUserId, $objDatabase ) {
		$arrstrCompanyUserPreferences = \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferencesByCompanyUserIdByCidByModuleIdByKeys( $intCompanyUserId, $this->getCid(), CModule::AR_POST_MONTH, [ CCompanyUserPreference::AR_FUTURE_POST_MONTH, CCompanyUserPreference::AR_PAST_POST_MONTH ], $objDatabase );

		if( true == valArr( $arrstrCompanyUserPreferences, 2 ) ) {
			$this->setPastPostMonth( $arrstrCompanyUserPreferences[CCompanyUserPreference::AR_PAST_POST_MONTH] );
			$this->setFuturePostMonth( $arrstrCompanyUserPreferences[CCompanyUserPreference::AR_FUTURE_POST_MONTH] );
		}
	}

	public function setValidatePastOrFuturePostMonth( $boolValidatePastOrFuturePostMonth ) {
		$this->m_boolValidatePastOrFuturePostMonth = ( bool ) $boolValidatePastOrFuturePostMonth;
	}

	public function setApplyToFirstMonth( $boolApplyToFirstMonth ) {
		$this->m_boolApplyToFirstMonth  = ( bool ) $boolApplyToFirstMonth;
	}

	public function setApplyToLastMonth( $boolApplyToLastMonth ) {
		$this->m_boolApplyToLastMonth = ( bool ) $boolApplyToLastMonth;
	}

	public function setDataForValidatePastOrFuturePostMonth( $arrmixDataToValidatePastOrFuturePostMonth ) {
		$this->setPastPostMonth( getArrayElementByKey( 'past_post_month', $arrmixDataToValidatePastOrFuturePostMonth ) );
		$this->setFuturePostMonth( getArrayElementByKey( 'future_post_month', $arrmixDataToValidatePastOrFuturePostMonth ) );
		$this->setValidatePastOrFuturePostMonth( getArrayElementByKey( 'is_validate_past_or_future_post_month', $arrmixDataToValidatePastOrFuturePostMonth ) );
	}

	public function setDefaultLedgerFilterId( $intDefaultLedgerFilterId ) {
		$this->m_intDefaultLedgerFilterId = $intDefaultLedgerFilterId;
	}

	public function setExcludeAutoWriteOff( $boolExcludeAutoWriteOff ) {
		$this->m_boolExcludeAutoWriteOff = $boolExcludeAutoWriteOff;
	}

	public function setArTransactionIds( $intArTransactionIds ) {
		$this->m_intArTransactionIds = $intArTransactionIds;
	}

	public function setOldArTransaction( $objOldArTransaction ) {
		$this->m_objOldArTransaction = $objOldArTransaction;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

	public function setTaxArTransactions( array $arrobjTaxArTransactions ) {
		$this->m_arrobjTaxArTransactions = $arrobjTaxArTransactions;
	}

	public function setDependentArTransactions( array $arrobjDependentArTransactions ) {
		$this->m_arrobjDependentArTransactions = $arrobjDependentArTransactions;
	}

	public function setArCodePriorityNum( int $intArCodePriorityNum ) {
		$this->m_intArCodePriorityNum = $intArCodePriorityNum;
	}

	public function setDefaultLedgerArCodeId( $intDefaultLedgerArCodeId ) {
		$this->m_intDefaultLedgerArCodeId = $intDefaultLedgerArCodeId;
	}

	public function setLedgerFilterId( $intLedgerFilterId ) {
		$this->m_intLedgerFilterId = $intLedgerFilterId;
	}

	public function setRepaymentAmountDue( $fltRepaymentAmountDue ) {
		$this->m_fltRepaymentAmountDue = CStrings::strToFloatDef( $fltRepaymentAmountDue, NULL, false, 4 );
	}

	public function setOrganizationContractId( $intOrganizationContractId ) {
		$this->m_intOrganizationContractId = $intOrganizationContractId;
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = $intUnitSpaceId;
	}

	public function setInvoiceDefaultDueDays( $intInvoiceDefaultDueDays ) {
		$this->m_intInvoiceDefaultDueDays = $intInvoiceDefaultDueDays;
	}

	public function setUseTransactionDateForAutoAllocation( $boolUseTransactionDateForAutoAllocation ) {
		$this->m_boolUseTransactionDateForAutoAllocation = $boolUseTransactionDateForAutoAllocation;
	}

	public function setInternalMemo( $strInternalMemo ) {
		$strInternalMemo = \Psi\CStringService::singleton()->htmlspecialchars( $strInternalMemo );
		parent::setInternalMemo( CStrings::strTrimDef( $strInternalMemo, 2000, NULL, true, false, true ) );
	}

	/**
	 * Sql Functions
	 */

	public function sqlMemo() {
		return  ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( str_replace( '%', '%%', $this->m_strMemo ) ) . '\'' : 'NULL';
	}

	public function sqlInternalMemo() {
		return  ( true == isset( $this->m_strInternalMemo ) ) ? '\'' . addslashes( str_replace( '%', '%%', $this->m_strInternalMemo ) ) . '\'' : 'NULL';
	}

	/**
	 * Create Functions
	 */

	public function createUtilityTransaction() {

		$objUtilityTransaction = new CUtilityTransaction();
		$objUtilityTransaction->setCid( $this->getCid() );
		$objUtilityTransaction->setPropertyId( $this->getPropertyId() );
		$objUtilityTransaction->setArCodeId( $this->getArCodeId() );
		$objUtilityTransaction->setArPaymentId( $this->getArPaymentId() );
		$objUtilityTransaction->setArTransactionId( $this->getId() );
		$objUtilityTransaction->setOriginalAmount( $this->getTransactionAmount() );
		$objUtilityTransaction->setUnadjustedAmount( $this->getTransactionAmount() );
		$objUtilityTransaction->setCurrentAmount( $this->getTransactionAmountDue() );
		$objUtilityTransaction->setTransactionDatetime( $this->getPostDate() );

		return $objUtilityTransaction;
	}

	public function createArDepositTransaction() {

		$objArDepositTransaction = new CArDepositTransaction();
		$objArDepositTransaction->setCid( $this->getCid() );
		$objArDepositTransaction->setArTransactionId( $this->getId() );

		return $objArDepositTransaction;
	}

	public function createArDeposit( $objPropertyGlSetting, $arrobjUnDepositedArTransactions, $objArDepositReference = NULL,  $intCurrentUserId, $objDatabase ) {

		if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) || false == valArr( $arrobjUnDepositedArTransactions ) ) return true;

		$objActiveBankAccount = NULL;
		if( false == valObj( $objArDepositReference, 'CArDeposit' ) ) {
			$objActiveBankAccount = CBankAccounts::fetchActiveBankAccountByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
			if ( false == valObj( $objActiveBankAccount, 'CBankAccount' ) )	return true;

			$objPropertyBankAccount = CPropertyBankAccounts::fetchActivePropertyBankAccountByPropertyIdByBankAccountIdByCid( $this->getPropertyId(), $objActiveBankAccount->getId(), $this->getCid(), $objDatabase );
			if( false == valObj( $objPropertyBankAccount, 'CPropertyBankAccount' ) ) return true;
		} else {
			$arrobjTempUndepositedArTransactions = ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchUndepositedArTransactionsByBankAccountIdByIdsByCid( $objArDepositReference->getBankAccountId(), array_keys( $arrobjUnDepositedArTransactions ), $objArDepositReference->getCid(), $objDatabase );
			if( false == valArr( $arrobjTempUndepositedArTransactions ) || true == array_key_exists( 0, rekeyObjects( 'HasBankAccount', $arrobjTempUndepositedArTransactions ) ) ) return true;
		}

		$arrobjArDepositTransactions = [];
		$fltTotalDepositAmount 		 = 0;
		$arrstrPostMonths            = $arrstrPostDates = [];

		foreach( $arrobjUnDepositedArTransactions as $objArTransaction ) {
			$objArDepositTransaction = $this->createArDepositTransaction();
			$objArDepositTransaction->setArDepositId( NULL );
			$objArDepositTransaction->setArTransactionId( $objArTransaction->getId() );

			if( false == $objArDepositTransaction->validate( VALIDATE_INSERT, $objDatabase, $boolValidateArDepositId = false ) ) return true;

			$arrobjArDepositTransactions[]	= $objArDepositTransaction;
			$fltTotalDepositAmount			= bcadd( $fltTotalDepositAmount, $objArTransaction->getTransactionAmount(), 2 );

			$arrstrPostMonths[strtotime( $objArTransaction->getPostMonth() )]	= $objArTransaction->getPostMonth();
			$arrstrPostDates[strtotime( $objArTransaction->getPostDate() )]		= $objArTransaction->getPostDate();
		}

		$arrstrPostMonths[strtotime( $objPropertyGlSetting->getArPostMonth() )] = $objPropertyGlSetting->getArPostMonth();
		$arrstrPostDates[strtotime( date( 'm/d/Y' ) )] = date( 'm/d/Y' );

		if( false == valArr( $arrstrPostMonths ) || false == valArr( $arrstrPostDates ) )	return true;

		$objArDeposit = ( true == valObj( $objArDepositReference, 'CArDeposit' ) ) ? clone $objArDepositReference : new CArDeposit();
		$objArDeposit->setId( NULL );
		$objArDeposit->setCid( $this->getCid() );
		$objArDeposit->setIsInitialImport( 0 );
		$objArDeposit->setTransactionDatetime( 'NOW()' );
		$objArDeposit->setDepositAmount( - 1 * $fltTotalDepositAmount );
		$objArDeposit->setPostDate( $arrstrPostDates[max( array_keys( $arrstrPostDates ) )] );
		$objArDeposit->setPostMonth( $arrstrPostMonths[max( array_keys( $arrstrPostMonths ) )] );

		// We need to set the deposit memo.

		if( false == valObj( $objArDepositReference, 'CArDeposit' ) ) $objArDeposit->setBankAccountId( $objActiveBankAccount->getId() );
		if( false == valObj( $objArDepositReference, 'CArDeposit' ) ) $objArDeposit->setArDepositTypeId( CArDepositType::MANUAL );
		$objArDeposit->setIsPosted( 0 );

		if( false == $objArDeposit->insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		foreach( $arrobjArDepositTransactions as $objArDepositTransaction ) {
			$objArDepositTransaction->setArDepositId( $objArDeposit->getId() );
			if( false == $objArDepositTransaction->insert( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}
		}

		if( false == $objArDeposit->postToGl( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	// @FIXME: Is the below comment needed now?
	// We should really require the allocation datetime here. We should never use the current datetime.

	public function createArAllocation( $strPostMonth, $strAllocationPostDate = NULL ) {

		// We should never set today as the allocation datetime (currently used as post month, but changing soon)
		$strAllocationPostDate = ( strtotime( $strAllocationPostDate ) > strtotime( $this->getPostDate() ) ) ? $strAllocationPostDate : $this->getPostDate();

		$objArAllocation = new CArAllocation();

		$objArAllocation->setCid( $this->getCid() );
		$objArAllocation->setLeaseId( $this->getLeaseId() );

		if( 0 == abs( $this->getTransactionAmountDue() ) ) {
			$objArAllocation->setAllocationAmount( $this->getTransactionAmount() );
		} else {
			$objArAllocation->setAllocationAmount( $this->getTransactionAmountDue() );
		}

		$objArAllocation->setTransactionAmountDue( $this->getTransactionAmountDue() );
		$objArAllocation->setPostMonth( $strPostMonth );
		$objArAllocation->setPostDate( date( 'm/d/Y', strtotime( $strAllocationPostDate ) ) );
		if( $this->getTransactionAmount() > 0 ) {
			$objArAllocation->setChargeArTransactionId( $this->getId() );
		} else {
			$objArAllocation->setCreditArTransactionId( $this->getId() );
		}

		$objArAllocation->setArCodeName( $this->getArCodeName() );

		return $objArAllocation;
	}

	public function createIntegratedArPayment() {

		$objArPayment = new CArPayment();
		$objArPayment->setDefaults();
		$objArPayment->setCid( $this->m_intCid );
		$objArPayment->setLeaseId( $this->m_intLeaseId );
		$objArPayment->setPaymentTypeId( CPaymentType::CHECK );
		$objArPayment->setPaymentStatusTypeId( CPaymentStatusType::RECEIVED );
		$objArPayment->setRemotePrimaryKey( $this->getRemotePrimaryKey() );
		$objArPayment->setPaymentDatetime( $this->getPostDate() );
		$objArPayment->setPaymentAmount( abs( $this->getTransactionAmount() ) );
		$objArPayment->setTotalAmount( abs( $this->getTransactionAmount() ) );
		$objArPayment->setBilltoIpAddress( $_SERVER['HTTP_HOST'] );

		return $objArPayment;
	}

	public function createRepaymentArTransaction() {

		$objRepaymentArTransaction = new CRepaymentArTransaction();
		$objRepaymentArTransaction->setCid( $this->getCid() );
		$objRepaymentArTransaction->setPropertyId( $this->getPropertyId() );
		$objRepaymentArTransaction->setArTransactionId( $this->getId() );

		return $objRepaymentArTransaction;
	}

	/**
	 * Fetch Functions
	 */

	// @FIXME: Do we need these fetch functions here? Can we move this to Plural classes?

	public function fetchUpdatedByCompanyUser( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $this->getUpdatedBy(), $this->getCid(), $objDatabase );
	}

	public function fetchCreatedByCompanyUser( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $this->getCreatedBy(), $this->getCid(), $objDatabase );
	}

	public function fetchArDeposit( $objDatabase ) {
		return \Psi\Eos\Entrata\CArDeposits::createService()->fetchArDepositByArTransactionIdByCid( $this->getArTransactionId(), $this->getCid(), $objDatabase );
	}

	public function fetchDefaultReturnItemFee( $objDatabase, $intCompanyUserId ) {

		$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::RETURN_ITEM_FEE, $objDatabase );
		$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->getCid(), [ $this->getPropertyId() ], $intCompanyUserId );

		$objPostScheduledChargesCriteria->setIsDryRun( true )
										->setLeaseIds( [ $this->getLeaseId() ] )
										->setPostPostedScheduledCharges( true );

		$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );
		$arrobjArTransactions = $objPostScheduledChargesLibrary->getArTransactions();

		$fltReturnItemFee = 0;

		foreach( $arrobjArTransactions as $objArTransaction ) {
			$fltReturnItemFee += $objArTransaction->getTransactionAmount();
		}

		$this->setReturnItemFee( $fltReturnItemFee );

		return $this->getReturnItemFee();
	}

	public function fetchArCode( $objDatabase, $boolIsCorporateTab = false ) {

		$objArCodesFilter = new CArCodesFilter( $boolExcludeReservedCodes = false, $boolShowDisabled = true );
		$objArCodesFilter->setArCodeIds( [ $this->getArCodeId() ] );
		$objArCodesFilter->setIncludeInterCompanyArCodeTypeIds( $boolIsCorporateTab );

		// @FIXME: function has fetchArCode but returns ArCodes
		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getCid(), $objArCodesFilter, $objDatabase );
	}

	// @FIXME: Fetch function shouldn't be setting any member variable. It should just return record and in getOrFetch it should be set.

	public function fetchPropertyGlSetting( $objDatabase ) {
		$this->m_objPropertyGlSetting = CPropertyGlSettings::fetchPropertyGlSettingByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
		return $this->m_objPropertyGlSetting;
	}

	public function fetchCompanyMerchantAccount( $objClientDatabase, $objPaymentDatabase, $intPaymentMediumId = NULL ) {

		if( NULL == $intPaymentMediumId ) {
			$intPaymentMediumId = CPaymentMedium::WEB;
		}

		if( true == is_null( $this->getCid() ) || ( true == is_null( $this->getArCodeId() ) ) ) {
			return  NULL;
		}

		if( false == is_null( $this->getPropertyId() ) ) {
			$intCompanyMerchantAccountId	= CPropertyMerchantAccounts::fetchCompanyMerchantAccountIdByPropertyIdByArCodeIdByCid( $this->getPropertyId(), $this->getArCodeId(), $this->getCid(), $objClientDatabase );
		} elseif( false == is_null( $this->getLeaseId() ) ) {
			$intCompanyMerchantAccountId	= CPropertyMerchantAccounts::fetchCompanyMerchantAccountIdByLeaseIdByArCodeIdsByCid( $this->getLeaseId(), [ $this->getArCodeId() ], $this->getCid(), $objClientDatabase );
		}

		return \Psi\Eos\Payment\CCompanyMerchantAccounts::createService()->fetchActiveCompanyMerchantAccountByIdByCidByPaymentMediumId( $intCompanyMerchantAccountId, $this->getCid(), $intPaymentMediumId, $objPaymentDatabase );
	}

	// This function gets all the existing allocations, plus potential allocations and returns them sorted by the credit's original transaction datetime.

	public function fetchSortedArAllocationsPlusUnallocatedPotentialAllocationsByLedgerFilterId( $intLedgerFilterId, $objDatabase ) {

		$boolIsCharge = ( 0 < $this->getTransactionAmount() ) ? true : false;
		$arrobjActualArAllocations = \Psi\Eos\Entrata\CArAllocations::createService()->fetchEditingArAllocationsByArTransactionIdByLedgerFilterIdByLeaseIdByCid( $this->getId(), $intLedgerFilterId, $this->getLeaseId(), $this->getCid(), $objDatabase, $boolIsCharge );

		return $arrobjActualArAllocations;
	}

	public function fetchAppliedArTransactions( $objDatabase, $boolIsDepositCreditOnly = false, $boolIsFetchDeletedArTransactions = false ) {
		if( 0 < $this->getTransactionAmount() ) {
			return \Psi\Eos\Entrata\CArTransactions::createService()->fetchAppliedCreditArTransactionsByArTransactionIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsDepositCreditOnly, $boolIsFetchDeletedArTransactions );
		} else {
			return \Psi\Eos\Entrata\CArTransactions::createService()->fetchAppliedChargeArTransactionsByArTransactionIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsDepositCreditOnly, $boolIsFetchDeletedArTransactions );
		}
	}

	public function fetchArAllocations( $objDatabase, $boolIsFetchDeletedArAllocations = false ) {
		return \Psi\Eos\Entrata\CArAllocations::createService()->fetchArAllocationsByLeaseIdByArTransactionIdByCid( $this->getLeaseId(), $this->getId(), $this->getCid(), $objDatabase, $boolIsFetchDeletedArAllocations );
	}

	public function fetchNormalAndPrePaidArAllocations( $objDatabase, array $arrintExcludedArTriggerIds = [] ) {
		return \Psi\Eos\Entrata\CArAllocations::createService()->fetchNormalAndPrePaidArAllocationsByLeaseIdByArTransactionIdByCid( $this->getLeaseId(), $this->getId(), $this->getCid(), $objDatabase, $arrintExcludedArTriggerIds );
	}

	public function fetchArPayment( $objPaymentDatabase ) {
		$this->m_objArPayment = CArPayments::fetchArPaymentByIdByCid( $this->getArPaymentId(), $this->getCid(), $objPaymentDatabase );
		return $this->m_objArPayment;
	}

	public function fetchWorksArPayment( $objDatabase ) {
		return CArPayments::fetchWorksArPaymentByIdByCid( $this->getArPaymentId(), $this->getCid(), $objDatabase );
	}

	public function fetchArTransaction( $objDatabase ) {
		if ( false == is_numeric( $this->getArTransactionId() ) ) return; // @FIXME: Should this return NULL?
		return \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionByIdByCid( $this->getArTransactionId(), $this->getCid(), $objDatabase );
	}

	// @FIXME: In fetchLease we shouldn't be setting $m_objLease. It breaks single responsibility of this function.

	public function fetchLease( $objDatabase ) {
		$this->m_objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchNonDeletedLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase, $boolExcludeCancelledInterval = false );
		return $this->m_objLease;
	}

	public function fetchFmoProcessedLease( $objDatabase ) {
		$this->m_objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchFmoProcessedLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
		return $this->m_objLease;
	}

	public function fetchUnallocatedChargeAmount( $objDatabase ) {

		$strSql = 'SELECT
							sum( vat.transaction_amount_due )
						FROM
							view_ar_transactions vat
						WHERE
							vat.cid = ' . ( int ) $this->getCid() . '
							AND vat.lease_id = ' . ( int ) $this->getLeaseId() . '
							AND vat.id <> ' . ( int ) $this->getId() . '
							AND vat.transaction_amount_due > 0';

		$arrstrUnallocatedFund = fetchData( $strSql, $objDatabase );

		$fltUnallocatedFund = ( 0 < ( float ) $arrstrUnallocatedFund[0]['sum'] ) ?( $arrstrUnallocatedFund[0]['sum'] ) : 0.00;

		return  sprintf( '%01.2f', $fltUnallocatedFund );
	}

	public function fetchArDepositsCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CArDeposits::createService()->fetchArDepositsCountByArTransactionIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchGlDimension( CDatabase $objDatabase ) {
		return \Psi\Eos\Entrata\CGlDimensions::createService()->fetchGlDimensionByCidByPropertyIdByArTransactionId( $this->getCid(), $this->getPropertyId(), $this->getId(), $objDatabase );
	}

	public function validatePostDate( $strPostdate ) {

		if( false == valStr( $strPostdate ) ) return $strPostdate;

		$strPostDateDateTime 		 = new DateTime( $strPostdate );
		$strPostDateDateTimeStamp    = $strPostDateDateTime->format( 'U' );

		$objDateTime 				 = new DateTime( '1st December 2099' ); // Max allowed date here
		$strMaxPostDateTimeStamp 	 = $objDateTime->format( 'U' );  // It gives the time difference in second from 1st January 1970 to 2099

		$objDateTime 				 = new DateTime( '1st January 1970' ); // Min allowed date here
		$strMinPostDateTimeStamp     = $objDateTime->format( 'U' );

		if( $strPostDateDateTimeStamp > $strMaxPostDateTimeStamp ) {
			$strPostdate = '12/01/2099';
		}

		if( $strPostDateDateTimeStamp < $strMinPostDateTimeStamp ) {
			$strPostdate = '01/01/1970';
		}

		return $strPostdate;

	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction, $objDatabase, $objPaymentDatabase = NULL, $boolIsValPostMonth = false, $boolIsIgnoreChargeAmount = false, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT, $intOldArTransactionId = NULL, $intLedgerFilterId = NULL ) {
		$boolIsValid = true;

		$objArTransactionValidator = new CArTransactionValidator();
		$objArTransactionValidator->setArTransaction( $this );
		$this->getOrFetchPropertyGlSetting( $objDatabase );

		switch( $strAction ) {
			case 'post_deposit_held_ar_transaction':
				$boolIsValid &= $objArTransactionValidator->valConflictingBeginingBalanceArTransaction( $objDatabase );

			case 'post_charge_ar_transaction':
			case 'post_concession_ar_transaction':
			case 'post_adjustment_ar_transaction':
			case 'post_payment_reverse_ar_transaction':
				$boolIsValid &= $objArTransactionValidator->valArCodeTypeId();
			case 'post_payment_ar_transaction':
			case 'transaction_approval_request':
				$boolIsValid &= $objArTransactionValidator->valCid();
				$boolIsValid &= $objArTransactionValidator->valPropertyId();
				$boolIsValid &= $objArTransactionValidator->valLeaseId();
				$boolIsValid &= $objArTransactionValidator->valArCodeId( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valPostDate( $strAction );
				if( false == $boolIsIgnoreChargeAmount ) {
					$boolIsValid &= $objArTransactionValidator->valTransactionAmount( $strAction );
				}
				$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase, false, $intUserValidationMode );
				$boolIsValid &= $objArTransactionValidator->valSubsidyChargeCode( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valMilitaryChargeCode( $objDatabase );
				break;

			// FIXME: Seems below case is not in use
			case 'post_move_in_charge_ar_transaction':
			case 'post_approval_request_charge_ar_transaction':
				$boolIsValid &= $objArTransactionValidator->valCid();
				$boolIsValid &= $objArTransactionValidator->valPropertyId();
				$boolIsValid &= $objArTransactionValidator->valLeaseId();
				$boolIsValid &= $objArTransactionValidator->valArCodeId();
				$boolIsValid &= $objArTransactionValidator->valArCodeTypeId();
				$boolIsValid &= $objArTransactionValidator->valPostDate( $strAction );
				$boolIsValid &= $objArTransactionValidator->valTransactionAmount( ( 0 > $this->getTransactionAmount() )? 'post_concession_ar_transaction':'post_charge_ar_transaction' );
				$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase );
				break;

			case 'resident_works_delete':
				$boolIsValid &= $objArTransactionValidator->valCid();
				$boolIsValid &= $objArTransactionValidator->valPropertyId();
				$boolIsValid &= $objArTransactionValidator->valLeaseId();
				$boolIsValid &= $objArTransactionValidator->valArCodeId();
				$boolIsValid &= $objArTransactionValidator->valArCodeTypeId();
				if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) && true == $this->m_objPropertyGlSetting->getActivateStandardPosting() && CPropertyGlSetting::VALIDATION_MODE_STRICT == $intUserValidationMode ) {
					$boolIsValid &= $objArTransactionValidator->valDeletePostMonth( $objDatabase );
					$boolIsValid &= $objArTransactionValidator->valDeletePostDate();
				}
				break;

			case 'entrata_ar_transaction_delete':
				$boolIsValid &= $objArTransactionValidator->valDeleteAbility( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valDeletePostMonth( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valDeletePostDate();
				break;

			case 'resident_works_reverse_lease_approval_delete':
				$boolIsValid &= $objArTransactionValidator->valDeletePostMonth( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valDeletePostDate();
				break;

			case 'resident_works_return':
				$boolIsValid &= $objArTransactionValidator->valDeleteAbility( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valDeletePostMonth( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valDeletePostDate();
				$boolIsValid &= $objArTransactionValidator->valReturnItemFee();
				$boolIsValid &= $objArTransactionValidator->valReturnAbility( $objPaymentDatabase );
				$boolIsValid &= $objArTransactionValidator->valArPaymentStatusTypeId( $objPaymentDatabase );
				break;

			case 'entrata_charge_concession':
				$boolIsValid &= $objArTransactionValidator->valResidentWorksChargeOrConcessionArTransaction( $this->m_arrobjArAllocations, $objDatabase, $boolDissallowZeroAmounts = true, $boolIsValPostMonth, $boolIsIgnoreChargeAmount, $intUserValidationMode );
				$boolIsValid &= $objArTransactionValidator->valMilitaryChargeCode( $objDatabase );
				break;

			case 'bad_debt_transaction':
				$boolIsValid &= $objArTransactionValidator->valBadDebtArTransaction();

				if( true == $boolIsValid ) {
					$boolIsValid &= $objArTransactionValidator->valPostDate( NULL );
					$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase );
				}
				break;

			// FIXME: Seems below case is not in use
			case 'resident_works_charge_concession':
				$boolIsValid &= $objArTransactionValidator->valResidentWorksChargeOrConcessionArTransaction( $this->m_arrobjArAllocations, $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valTransactionAmount( ( 0 > $this->getTransactionAmount() )? 'post_concession_ar_transaction':'post_charge_ar_transaction' );
				break;

			case 'resident_works_disassociate':
				$boolIsValid &= $objArTransactionValidator->valDisassociateAbility( $objPaymentDatabase );

				// These validations are required for entrata enabled properties only. We do not need this for integrated or non entrata enabled properties.
				if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) && true == $this->m_objPropertyGlSetting->getActivateStandardPosting() ) {
					$boolIsValid &= $objArTransactionValidator->valDeletePostMonth( $objDatabase );
					$boolIsValid &= $objArTransactionValidator->valDeletePostDate();
					if( true == $boolIsValPostMonth ) {
						$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase );
					}
				}
				break;

			case 'post_move_in_charge':
				$boolIsValid &= $objArTransactionValidator->valCid();
				$boolIsValid &= $objArTransactionValidator->valPropertyId();
				$boolIsValid &= $objArTransactionValidator->valLeaseId();
				$boolIsValid &= $objArTransactionValidator->valArCodeId();
				$boolIsValid &= $objArTransactionValidator->valArCodeTypeId();
				$boolIsValid &= $objArTransactionValidator->valPostDate( $strAction );
				// $boolIsValid &= $objArTransactionValidator->valPostDateTransactionDatetime( $objDatabase );
				if( false == $boolIsIgnoreChargeAmount ) {
					$boolIsValid &= $objArTransactionValidator->valTransactionAmount( 'post_move_in_charge' );
				}
				$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase );
				break;

			case 'add_customer_charge':
				$boolIsValid &= $objArTransactionValidator->valCid();
				$boolIsValid &= $objArTransactionValidator->valPropertyId();
				$boolIsValid &= $objArTransactionValidator->valLeaseId();
				$boolIsValid &= $objArTransactionValidator->valArCodeId();
				$boolIsValid &= $objArTransactionValidator->valArCodeTypeId();
				$boolIsValid &= $objArTransactionValidator->valPostDate( $strAction );
				$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase );
				if( false == $boolIsIgnoreChargeAmount ) {
					$boolIsValid &= $objArTransactionValidator->valTransactionAmount( 'post_move_in_charge' );
				}
				// $boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase );
				break;

			case 'post_add_customer_deposit_held_ar_transaction':
				$boolIsValid &= $objArTransactionValidator->valArCodeId();
				$boolIsValid &= $objArTransactionValidator->valTransactionAmount( 'post_deposit_held_ar_transaction' );
				$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase, $boolIsValPostMonth, $intUserValidationMode );
				$boolIsValid &= $objArTransactionValidator->valPostDate( 'post_deposit_held_ar_transaction', $objDatabase );
				break;

			case 'post_bulk_charge_ar_transaction':
					$boolIsValid &= $objArTransactionValidator->valPostMonthFormat();
					$boolIsValid &= $objArTransactionValidator->valArCodeId();
					$boolIsValid &= $objArTransactionValidator->valArCodeTypeId();
					$boolIsValid &= $objArTransactionValidator->valTransactionAmount( 'post_bulk_charge_ar_transaction' );
					$boolIsValid &= $objArTransactionValidator->valPostDate( $strAction );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $objArTransactionValidator->valId();
				$boolIsValid &= $objArTransactionValidator->valCid();
				$boolIsValid &= $objArTransactionValidator->valPropertyId();
				$boolIsValid &= $objArTransactionValidator->valLeaseId();
				$boolIsValid &= $objArTransactionValidator->valArCodeId();
				$boolIsValid &= $objArTransactionValidator->valArCodeTypeId();
				$boolIsValid &= $objArTransactionValidator->valDeleteAbility( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valDeletePostMonth( $objDatabase );
				break;

			case 'ar_transaction_offset':
				$boolIsValid &= $objArTransactionValidator->valDeletePostMonth( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valDeletePostDate();
				break;

			case 'post_outside_of_ar_post_month':
				$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase, false, $intUserValidationMode );
				break;

			case 'write_off_transaction':
				$boolIsValid &= $objArTransactionValidator->valWriteOffTransaction( $objDatabase );
				break;

			case 'entrata_drop_ar_allocations':
				$boolIsValid &= $objArTransactionValidator->valDeletePostMonth( $objDatabase );
				$boolIsValid &= $objArTransactionValidator->valDeletePostDate();
				break;

			case 'apply_deposit_transaction':
				$boolIsValid &= $objArTransactionValidator->valIntegrity( $objDatabase, $intLedgerFilterId );
				$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase, $boolIsValPostMonth, $intUserValidationMode );
				break;

			case 'hard_edit_charge':
				$boolIsValid &= $objArTransactionValidator->valChargeArTransactionInUserValidationMode( $strAction, $objDatabase, $intUserValidationMode );
				break;

			case 'hard_delete_charge':
				$boolIsValid &= $objArTransactionValidator->valChargeArTransactionInUserValidationMode( $strAction, $objDatabase, $intUserValidationMode );
				$boolIsValid &= $objArTransactionValidator->valPostMonth( $objDatabase, $boolIsValPostMonth = false, $intUserValidationMode );
				break;

			case 'hard_edit_beginning_deposit_held':
			case 'hard_delete_beginning_deposit_held':
				$boolIsValid &= $objArTransactionValidator->valHardEditDeleteDepositTransaction( $objDatabase, $strAction, $intOldArTransactionId );
				break;

			case 'require_note':
				$boolIsValid &= $objArTransactionValidator->valNotes();
				break;

			default:
				$boolIsValid &= true;
				break;
		}

		return $boolIsValid;
	}

	public function checkAndReportInvalidTemporaryTransactions( $objDatabase ) {
		if( true == $this->getIsTemporary() && false == is_null( $this->getArPaymentId() ) ) {
			$objArPayment = $this->fetchWorksArPayment( $objDatabase );

			if( true == valObj( $objArPayment, 'CArPayment' ) && CPaymentStatusType::CAPTURED == $objArPayment->getPaymentStatusTypeId() ) {
				trigger_error( __( 'Captured status payment {%d, 0} having transactions.is_temporary = true id:{%d, 1}', [ $objArPayment->getId(), $this->getId() ] ), E_USER_WARNING );
			}
		}
	}

	/**
	 * Other Functions
	 */

	// @FIXME: Do we need to override this function?

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				$this->addErrorMsg( $objErrorMsg );
			}
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		return false;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		return false;
	}

	public function formatPostDate() {
		if( strtotime( $this->getPostDate() ) == strtotime( date( 'm/d/Y' ) ) ) {
			$this->m_strPostDate = date( 'm/d/Y H:i:s' );
		}
	}

	public function loadDefaultDeleteData( $objDatabase, $boolLoadReturnItemFee = false, $intCompanyUserId = NULL ) {

		$this->m_objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objDatabase );
		$this->setDeletePostDate( $this->getPostDate() );

		if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {

			// If this is a future charge, we don't want to reverse this charge into a previous period.
			if( strtotime( $this->getPostMonth() ) > strtotime( $this->m_objPropertyGlSetting->getArPostMonth() ) ) {
				$strDeletePostMonth = $this->getPostMonth();

			} else {
				$strDeletePostMonth = $this->m_objPropertyGlSetting->getArPostMonth();
			}

			$this->setDeletePostMonth( $strDeletePostMonth );

			if( false == valStr( $this->getDeleteMemo() ) ) {
				$this->setDeleteMemo( $this->generateDeleteMemo() );
			}

			if( true == $boolLoadReturnItemFee ) {
				$this->fetchDefaultReturnItemFee( $objDatabase, $intCompanyUserId );
			}
		}

		return;
	}

	private function generateDeleteMemo( $strMemo = NULL ) {

		$strDeleteMemo = __( 'Reversal of transaction ID:{%d, 0, nots}', [ $this->getId() ] );

		$strDeleteMemo = ( true == valStr( $strMemo ) && $strDeleteMemo != $strMemo ) ? $strDeleteMemo . ' - ' . $strMemo : $strDeleteMemo;

		return $strDeleteMemo;
	}

	public function incrementReturnedPaymentsCount( $intCurrentUserId, $objClientDatabase ) {

		$this->getOrFetchLease( $objClientDatabase );

		assertObj( $this->m_objLease, 'CLease' );

		if( false == $this->m_objLease->incrementReturnedPaymentsCount( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->m_objLease->getErrorMsgs() );
			return false;
		}

		// Now update the return_item_count on the customer
		$this->getOrFetchArPayment( $objClientDatabase );

		if( true == valObj( $this->m_objArPayment, 'CArPayment' ) ) {
			$objCustomer = $this->m_objArPayment->getOrFetchCustomer( $objClientDatabase );

			if( true == valObj( $objCustomer, 'CCustomer' ) ) {
				$objCustomer->setReturnedPaymentsCount( ( int ) $objCustomer->getReturnedPaymentsCount() + 1 );

				if( false == $objCustomer->update( $intCurrentUserId, $objClientDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, '', __( 'Late payment count failed to increment on customer.' ) ) );
					return false;
				}
			}
		}

		return true;
	}

	public function postReturnItemFee( $intCurrentUserId, $objDatabase ) {

		$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::RETURN_ITEM_FEE, $objDatabase );
		$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->getCid(), [ $this->getPropertyId() ], $intCurrentUserId );

		$objPostScheduledChargesCriteria->setIsDryRun( false )
										->setLeaseIds( [ $this->getLeaseId() ] )
										->setPostMonth( $this->getDeletePostMonth() )
										->setPostDate( $this->getDeletePostDate() )
										->setPostPostedScheduledCharges( true );

		$objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria );

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $objPaymentDatabase = NULL, $boolDisassociateArPayment = false, $boolPeformRapidDelete = false, $boolReturnSqlOnly = false, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT ) {
		// @FIXME: Unused variable $boolIsValid
		$boolIsValid = true;

		$this->checkAndReportInvalidTemporaryTransactions( $objDatabase );

		if( true == $boolPeformRapidDelete || CPropertyGlSetting::VALIDATION_MODE_STRICT != $intUserValidationMode ) {

			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( true == $this->getIsDeleted() ) {
			trigger_error( __( 'You cannot delete an item that has already been deleted.' ), E_USER_ERROR );
			exit;
		}

		$this->getOrFetchPropertyGlSetting( $objDatabase );

		// @TODO: We can use assertObj function here.
		if( false == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			trigger_error( __( 'Property GL Setting failed to load.' ), E_USER_ERROR );
			exit;
		}

		// Should we override the delete post month if it's before
		$this->setDeletePostDate( $this->getPostDate() );
		if( true == is_null( $this->getDeletePostMonth() ) || true == is_null( $this->getDeletePostDate() ) || strtotime( $this->getPostMonth() ) > strtotime( $this->getDeletePostMonth() ) ) {
			$this->loadDefaultDeleteData( $objDatabase );
		}

		$this->setDeleteMemo( $this->generateDeleteMemo( $this->getDeleteMemo() ) );

		// If it is a non-electronic payment, get the payment and mark it as deleted
		// If it is electronic, disassociate it from the resident and lease (make it floating)
		if( CArCodeType::PAYMENT == $this->getArCodeTypeId() && true == is_numeric( $this->getArPaymentId() ) ) {

			if( true == $boolDisassociateArPayment ) {

				if( false == valObj( $objPaymentDatabase, 'CDatabase' ) ) {
					trigger_error( __( 'Payment database not loaded while trying to delete a payment ar transaction.' ), E_USER_ERROR );
					// @FIXME: Can we return false instead of exit?. See other validation msg below.
					exit;
				}

				$this->getOrFetchArPayment( $objPaymentDatabase );

				// @TODO: We can use assertObj function here.
				if( false == valObj( $this->m_objArPayment, 'CArPayment' ) ) {
					trigger_error( __( 'Unable to load AR Payment.' ), E_USER_WARNING );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Unable to load payment.' ) ) );
					return false;
				}

				// Setting delete memo on transaction
				if( false == is_null( $this->m_objArPayment->getDeleteMemo() ) ) {
					$strMemo = ( false == is_null( $this->getDeleteMemo() ) ) ? $strMemo = $this->getDeleteMemo() . ' - ' . $this->m_objArPayment->getDeleteMemo() : $this->m_objArPayment->getDeleteMemo();
					$this->setDeleteMemo( $strMemo );
				}

				if( false == $this->postUpdate( $intCurrentUserId, $objDatabase ) ) {
					trigger_error( __( 'Unable to update ar transaction.' ), E_USER_ERROR );
					return false;
				}

				$this->m_objArPayment->setCustomerId( NULL );
				$this->m_objArPayment->setLeaseId( NULL );

				if( true == $this->m_objArPayment->insertArPaymentTransaction( CPaymentTransactionType::DISASSOCIATE, $intCurrentUserId, $objPaymentDatabase ) ) {

					// Why are we setting the remote primary key NULL here?  This seems really weird.
					$this->m_objArPayment->setRemotePrimaryKey( NULL );

					if( false == $this->m_objArPayment->update( $intCurrentUserId, $objDatabase, $objPaymentDatabase ) ) {
						trigger_error( __( 'Unable to update ar payment.' ), E_USER_ERROR );
						return false;
					}
				}
			}
		}

		// Now we need to determine whether we are deleting or reversing the transaction.
		// We need to load the property gl setting to determine this.
		// if( true == $this->m_objPropertyGlSetting->getActivateStandardPosting() ){

		// Always offset transactions for non-integrated, non-pm software companies
		// Do not create offsetting ar_transactions for temporary ar_transactions
		if( 0 == $this->getIsTemporary() && true == $this->m_objPropertyGlSetting->getActivateStandardPosting() ) {

			// We need to check and see if this ar transaction has already been reversed.  If it has, we don't need to drop allocations and offset the trans.
			if( false == $this->getIsDeleted() ) {
				// If the company is using our PM Software, we shouldn't actually delete the transaction, we should reverse it.
				// If the transaction is being reversed, we should reallocate the reversal adjustment with the original charge.
				if( false == $this->dropAllocations( $intCurrentUserId, $objDatabase, false, NULL ) ) {
					return false;
				}

				if( false == $this->reverseArTransaction( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		} else {

			// We don't need to delete allocations here manually because the ar_transactions_assert_delete trigger will auto drop them.
			// We should never drop an offset transaction either.
			if( false == $this->getIsDeleted() ) {
				if( false == parent::delete( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function reverseArTransaction( $intCurrentUserId, $objDatabase ) {

		// We have to reverse the transaction.  We reverse by cloning the transaction, and multiplying the amount by negative one.  Then we allocate them together.
		// We need to make sure we set the date, and post month of the offsetting transaction correctly

		$this->setIsDeleted( true );

		$objClonedArTransaction = clone $this;
		$objClonedArTransaction->setId( $objClonedArTransaction->fetchNextId( $objDatabase ) );
		$objClonedArTransaction->setPostDate( $this->getPostDate() );
		$objClonedArTransaction->setPostMonth( $this->getDeletePostMonth() );
		$objClonedArTransaction->setArProcessId( $this->getDeleteArProcessId() );
		$objClonedArTransaction->setTransactionAmount( $this->getTransactionAmount() * -1 );
		$objClonedArTransaction->setCustomerId( NULL );
		$objClonedArTransaction->setArTransactionId( $this->getId() );
		$objClonedArTransaction->setRemotePrimaryKey( NULL );
		$objClonedArTransaction->setIsInitialImport( false );
		$objClonedArTransaction->setIsPosted( false );
		$objClonedArTransaction->setRepaymentId( NULL );
		$objClonedArTransaction->setIsReversal( true );
		$objClonedArTransaction->setMemo( $this->getDeleteMemo() );
		$objClonedArTransaction->setInternalMemo( $this->getInternalMemo() );
		$objClonedArTransaction->setGlDimensionId( $this->getGlDimensionId() );
		$objClonedArTransaction->setApDetailId( $this->getApDetailId() );

		$this->setArTransactionId( $objClonedArTransaction->getId() );

		// This allows us to associate the reverse to a different payment in the case of an electronic reversal
		if( true == is_numeric( $this->getReverseArPaymentId() ) ) {
			$objClonedArTransaction->setArPaymentId( $this->getReverseArPaymentId() );

			// If this is a reversal, we don't want to associate the reversal to the original.
			$objClonedArTransaction->setArTransactionId( NULL );
			$this->setArTransactionId( NULL );

			// This allows us to reverse a different amount than the original transaction, in the case of an electronic reversal
			if( false == is_null( $this->m_fltReverseTransactionAmount ) && 0 < ( float ) $this->m_fltReverseTransactionAmount ) {
				$objClonedArTransaction->setTransactionAmount( $this->m_fltReverseTransactionAmount );
			}
		}

		$objArDeposit = NULL;

		// Find out if this is a beginning balance transaction, and if so, flag the allocation associated with this charges offset as is_initial_import = 1
		$arrintAllowedArCodeTypeIds = array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::REFUND ] );
		if( true == in_array( $this->getArCodeTypeId(), $arrintAllowedArCodeTypeIds ) || ( CArCodeType::DEPOSIT == $this->getArCodeTypeId() && $this->getIsDepositCredit() == false ) ) {
			if( true == is_null( $this->getDefaultArCodeId() ) ) {

				$objArCode = $this->fetchArCode( $objDatabase );

				// @TODO: We can use assertObj function here.
				if( false == valObj( $objArCode, 'CArCode' ) ) {
					trigger_error( __( 'AR code failed to load.' ), E_USER_ERROR );
					exit; // @TODO: Can we return false instead of exit.
				}

				if( CDefaultArCode::BEGINNING_BALANCE == $objArCode->getDefaultArCodeId() && true == $this->m_objPropertyGlSetting->getIsArMigrationMode() ) {
					$objClonedArTransaction->setIsInitialImport( true );
				}

			} elseif( CDefaultArCode::BEGINNING_BALANCE == $this->getDefaultArCodeId() && true == $this->m_objPropertyGlSetting->getIsArMigrationMode() ) {
				$objClonedArTransaction->setIsInitialImport( true );
			}
		}

		switch( $this->getArCodeTypeId() ) {
			case CArCodeType::PAYMENT:
				// If the original transaction has an ar_deposit associated, then
				// we need to create a negative ar_deposit for the offsetting transaction as well.

				if( false == $objClonedArTransaction->postPayment( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}

				$objClonedArTransaction->setArTransactionId( $this->getId() );

				if( true == is_numeric( $this->getReverseArPaymentId() ) ) {
					$objClonedArTransaction->setArTransactionId( NULL );
				}
				break;

			case CArCodeType::RENT:
			case CArCodeType::OTHER_INCOME:
			case CArCodeType::EXPENSE:
			case CArCodeType::ASSET:
			case CArCodeType::EQUITY:
			case CArCodeType::REFUND:
			case CArCodeType::OTHER_LIABILITY:
			case CArCodeType::DEPOSIT:
			case CArCodeType::INTER_COMPANY_RECEIVABLE:
			case CArCodeType::MANAGEMENT_FEES:
				if( false == $objClonedArTransaction->postCharge( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
				break;

			default:
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Error reversing transaction.  Original transaction type invalid.' ) ) );
				return false;
				break;
		}

		// Reallocate the original to the clone.
		if( false == $this->allocateObjects( $intCurrentUserId, $this, $objClonedArTransaction, $objDatabase, $this->getDeletePostMonth(), $this->getDeletePostDate(), $boolOverrideValidation = false ) ) {
			return false;
		}

		// Update the original
		if( false == $this->postUpdate( $intCurrentUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Original transaction failed to update.' ) ) );
			return false;
		}

		return true;
	}

	public function allocateObjects( $intCurrentUserId, $objArTransaction1, $objArTransaction2, $objDatabase, $strPostMonth = NULL, $strPostDate = NULL, $boolOverrideValidation = false, $boolIsInitialImport = false, $intArProcessId = NULL ) {

		// Find out which one is a charge, and which one is a credit
		if( 0 < $objArTransaction1->getTransactionAmount() ) {
			$objChargeArTransaction = $objArTransaction1;
			$objCreditArTransaction = $objArTransaction2;
		} else {
			$objChargeArTransaction = $objArTransaction2;
			$objCreditArTransaction = $objArTransaction1;
		}

		// Make sure we allocate the smallest amount in common between the two transactions
		$fltAllocationAmount = ( abs( $objCreditArTransaction->getTransactionAmount() ) < abs( $objChargeArTransaction->getTransactionAmount() ) ) ? $objCreditArTransaction->getTransactionAmount() : $objChargeArTransaction->getTransactionAmount();

		if( false == valObj( $objChargeArTransaction, 'CArTransaction' ) || false == valObj( $objCreditArTransaction, 'CArTransaction' ) || 0 > $objChargeArTransaction->getTransactionAmount() || 0 < $objCreditArTransaction->getTransactionAmount() ) {
			trigger_error( __( 'Credit and Charge Ar Transactions are not loaded, or are both charges or both credits.' ), E_USER_WARNING );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Credit and Charge Ar Transactions are not loaded, or are both charges or both credits.' ) ) );
			return false;
		}

		$this->getOrFetchPropertyGlSetting( $objDatabase );

		if( false == valStr( $strPostMonth ) ) {
			$strPostMonth = $this->getOrFetchPostMonth( $objDatabase );
		}

		if( false == valStr( $strPostDate ) ) {
			$strPostDate = date( 'm/d/Y H:i:s' );
		}

		$strPostDate = ( strtotime( $strPostDate ) > strtotime( $objArTransaction1->getPostDate() ) ) ? $strPostDate : $objArTransaction1->getPostDate();
		$strPostDate = ( strtotime( $strPostDate ) > strtotime( $objArTransaction2->getPostDate() ) ) ? $strPostDate : $objArTransaction2->getPostDate();

		$strPostMonth = ( strtotime( $strPostMonth ) > strtotime( $objArTransaction1->getPostMonth() ) ) ? $strPostMonth : $objArTransaction1->getPostMonth();
		$strPostMonth = ( strtotime( $strPostMonth ) > strtotime( $objArTransaction2->getPostMonth() ) ) ? $strPostMonth : $objArTransaction2->getPostMonth();

		$objArAllocation = $objChargeArTransaction->createArAllocation( $strPostMonth );
		$objArAllocation->setCreditArTransactionId( $objCreditArTransaction->getId() );
		$objArAllocation->setGlTransactionTypeId( CGlTransactionType::AR_ALLOCATION );
		$objArAllocation->setAllocationAmount( abs( $fltAllocationAmount ) );
		$objArAllocation->setPostDate( date( 'm/d/Y', strtotime( $strPostDate ) ) );
		$objArAllocation->setArProcessId( $intArProcessId );
		$objArAllocation->setPropertyId( $objCreditArTransaction->getPropertyId() );
		$objArAllocation->setOverridePostMonthPermission( $this->getOverridePostMonthPermission() );

		if( ( true == $this->m_objPropertyGlSetting->getIsArMigrationMode() || true == $boolIsInitialImport ) && true == $objArTransaction1->getIsInitialImport() && true == $objArTransaction2->getIsInitialImport() ) {
			$intNonInitialImportTransationsCount = \Psi\Eos\Entrata\CArTransactions::createService()->fetchNonInitialImportArTransactionsArAllocationsCountByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
			if( 0 == $intNonInitialImportTransationsCount || true == $boolIsInitialImport ) $objArAllocation->setIsInitialImport( true );
		}

		$boolIsArMigrationMode = false;
		if( true == valObj( $this->getArPayment(), 'CArPayment' ) ) {
			$objArPayment = $this->m_objArPayment;
		} else {
			$objArPayment = CArPayments::fetchWorksArPaymentByIdByCid( $this->getArPaymentId(), $this->getCid(), $objDatabase );
		}

		if( true == valObj( $objArPayment, 'CArPayment' )
			&& true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' )
			&& true == $this->m_objPropertyGlSetting->getIsArMigrationMode()
			&& ( true == in_array( $objArPayment->getPaymentStatusTypeId(), CPaymentStatusType::$c_arrintMigrationTemporaryReversalPaymentTypes ) )
			&& true == $objArPayment->isElectronicPayment() ) {

			$boolIsArMigrationMode = true;
		}
		// Insert allocation
		$arrstrPostMonthValidationData = $this->getOrFetchPostMonthValidationData( $intCurrentUserId, $objDatabase );
		if( true == valArr( $arrstrPostMonthValidationData ) ) {
			$objArAllocation->setPastPostMonth( $arrstrPostMonthValidationData['past_post_month'] );
			$objArAllocation->setFuturePostMonth( $arrstrPostMonthValidationData['future_post_month'] );
			$objArAllocation->setValidatePastOrFuturePostMonth( $arrstrPostMonthValidationData['validate_post_month'] );
		}

		if( false == $boolIsArMigrationMode && ( ( false == $boolOverrideValidation && false == $objArAllocation->validate( VALIDATE_INSERT, $objDatabase, $this->m_objPropertyGlSetting ) ) || false == $objArAllocation->postAllocation( $intCurrentUserId, $objDatabase ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Allocation failed to post.' ) ) );
			$this->addErrorMsgs( $objArAllocation->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function autoAllocate( $intCurrentUserId, $objDatabase, $intArProcessId = NULL, $arrintArTransactionIds = [] ) {
		$objLease = $this->getOrFetchLease( $objDatabase );

		$boolIsValid            = true;
		$objAllocationLibrary   = CTransactionAutoAllocationLibrary::create( $objLease, $objDatabase, $intArProcessId );

		try {
			$boolIsValid &= $objAllocationLibrary->autoAllocate( $this, $intCurrentUserId, $arrintArTransactionIds );
		} catch( \Exception $objException ) {
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_GENERAL, NULL, $objException->getMessage() ) ] );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && $this->getIsCredit() ) {

			require_once( PATH_APPLICATION_ENTRATA . 'Library/CBadDebtLibrary.class.php' );
			$objBadDebtLibrary = new \CBadDebtLibrary();
			$objBadDebtLibrary->setPropertyGlSetting( $objLease->getOrFetchPropertyGlSetting( $objDatabase ) );

			$boolIsValid = $objBadDebtLibrary->generateBadDebtRecoveryTransactions( $objLease, $intCurrentUserId, $objDatabase, [ $this->getId() ] );
		}

		return $boolIsValid;
	}

	public function dropAllocations( $intCompanyUserId, $objDatabase, $boolOverrideValidation = false, $arrobjArAllocationsToDelete = [] ) {

		$arrobjArAllocations			= ( true == valArr( $arrobjArAllocationsToDelete ) ) ? ( array ) $arrobjArAllocationsToDelete : ( array ) $this->fetchNormalAndPrePaidArAllocations( $objDatabase );
		$objPropertyGlSetting			= $this->getOrFetchPropertyGlSetting( $objDatabase );
		$arrintArTriggerIds				= ( $this->getTransactionAmount() < 0 ) ? [ CArTrigger::BAD_DEBT_RECOVERY ] : [ CArTrigger::BAD_DEBT_WRITE_OFF ];
		$boolIncludeDepositCredits		= ( $this->getTransactionAmount() < 0 ) ? false : true;

		$arrobjAppliedArTransactions 	= ( array ) \Psi\Eos\Entrata\CArTransactions::createService()->fetchAppliedArTransactionsByArTransactionIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase, $arrintArTriggerIds, $boolIncludeDepositCredits );
		$arrstrPostMonthValidationData 	= $this->getOrFetchPostMonthValidationData( $intCompanyUserId, $objDatabase );

		foreach( $arrobjArAllocations as $objArAllocation ) {

			$objArAllocation->setOverridePostMonthPermission( $this->getOverridePostMonthPermission() );
			$objArAllocation->loadDefaultDeleteData( $objPropertyGlSetting, $this->getDeletePostDate(), $this->getDeletePostMonth() );
			$objArAllocation->setIsDeleted( true );
			if( true == valArr( $arrstrPostMonthValidationData ) ) {
				$objArAllocation->setPastPostMonth( $arrstrPostMonthValidationData['past_post_month'] );
				$objArAllocation->setFuturePostMonth( $arrstrPostMonthValidationData['future_post_month'] );
				$objArAllocation->setValidatePastOrFuturePostMonth( $arrstrPostMonthValidationData['validate_post_month'] );
			}

			if( ( false == $boolOverrideValidation && false == $objArAllocation->validate( VALIDATE_DELETE, $objDatabase, $this->getOrFetchPropertyGlSetting( $objDatabase ) ) ) || false == $objArAllocation->delete( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objArAllocation->getErrorMsgs() );
				return false;
			}
		}

		// Delete Applied Ar Transactions
		foreach( $arrobjAppliedArTransactions as $objArTransaction ) {
			$objArTransaction->setOverridePostMonthPermission( $this->getOverridePostMonthPermission() );

			$strDeletePostMonth = ( strtotime( $objArTransaction->getPostMonth() ) > strtotime( $objPropertyGlSetting->getArLockMonth() ) ) ? $objArTransaction->getPostMonth() : $objPropertyGlSetting->getArPostMonth();
			$objArTransaction->setDeletePostMonth( $strDeletePostMonth );

			$strDeleteMemo = __( 'Reversal of transaction ID: {%d, 0, nots}', [ $objArTransaction->getId() ] );
			$objArTransaction->setDeleteMemo( $strDeleteMemo );

			if( false == $objArTransaction->delete( $intCompanyUserId, $objDatabase ) ) {
				$this->addErrorMsgs( $objArTransaction->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	public function splitArTransaction( $arrfltLeaseAmounts, $objPropertyGlSettings, $strPostDate, $intCurrentUserId, $objClient, $objDatabase, $intArProcessType = NULL, $strMemo = NULL, $objPaymentDatabase = NULL ) {

		$boolIsValid 	= true;
		$objArPayment 	= $this->fetchArPayment( $objPaymentDatabase );
		$arrobjLeases 	= ( array ) $objClient->fetchLeasesByIds( array_keys( $arrfltLeaseAmounts ), $objDatabase );

		// @FIXME: Unused variable $strPostMonth
		$strPostMonth 	= date( 'm/1/Y', max( strtotime( $objPropertyGlSettings->getArPostMonth() ), strtotime( $this->getPostMonth() ), strtotime( $strPostDate ) ) );

		foreach( $arrfltLeaseAmounts AS $intLeaseId => $fltTransactionAmount ) {
			if( true == valObj( $objArPayment, 'CArPayment' ) ) {
				$objArPaymentSplit = $objArPayment->createArPaymentSplit();

				$objArPaymentSplit->setLeaseId( $intLeaseId );
				$objArPaymentSplit->setCustomerId( $arrobjLeases[$intLeaseId]->getPrimaryCustomerId() );
				$objArPaymentSplit->setPropertyId( $this->getPropertyId() );
				$objArPaymentSplit->setPaymentAmount( $fltTransactionAmount );
				$objArPaymentSplit->setArCodeId( $this->getArCodeId() );

				$boolIsValid &= $objArPaymentSplit->insert( $intCurrentUserId, $objDatabase );

				$objArTransaction = $objArPaymentSplit->createPaymentArTransaction( $objArPayment, $boolIsTemporary = false, $objDatabase );
				$objArTransaction->setArProcessId( $intArProcessType );
				$objArTransaction->setLeaseIntervalId( $arrobjLeases[$intLeaseId]->getActiveLeaseIntervalId() );
				if( true == valStr( $strMemo ) ) $objArTransaction->setMemo( $strMemo );

				$boolIsValid &= $objArTransaction->postPayment( $intCurrentUserId, $objDatabase );

			}
		}

		$boolIsValid &= $this->delete( $intCurrentUserId, $objDatabase );

		if( true == valObj( $objArPayment, 'CArPayment' ) && false == $objArPayment->getIsSplit() ) {
			$objArPayment->setIsSplit( true );
			$boolIsValid &= $objArPayment->update( $intCurrentUserId, $objDatabase, $objPaymentDatabase );

		} else {
			$arrobjArPaymentSplits = ( array ) \Psi\Eos\Entrata\CArPaymentSplits::createService()->fetchActiveArPaymentSplitsByArTransactionIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			foreach( $arrobjArPaymentSplits as $objArPaymentSplit ) {
				$boolIsValid &= $objArPaymentSplit->delete( $intCurrentUserId, $objDatabase );

			}
		}

		if( true == valObj( $objArPayment, 'CArPayment' ) ) {
			$objArPayment->setActivePaymentArTransactions( rekeyObjects( 'id', [ $this ] ) );
			$boolIsValid &= $objArPayment->createArDeposit( $intCurrentUserId, $objDatabase, $objPaymentDatabase, $strProcessMethod = CArPayment::PROCESS_METHOD_EDIT_PAYMENT );
		}

		return $boolIsValid;
	}

	/**
	 * Post Functions
	 */

	public function postCharge( $intCurrentUserId, $objDatabase ) {

		if( false == $this->getSkipOverrideTransactionDatetime() ) {
			$this->setTransactionDatetime( 'NOW()' );
		}

		$this->formatPostDate();

		$strSql = 'SELECT * FROM ar_transactions_post_ar_transaction( ' .
				'%d, ' .
				$this->sqlCid() . ', ' .
				$this->sqlPropertyId() . ', ' .
				$this->sqlLeaseId() . ',  ' .
				$this->sqlLeaseIntervalId() . ',  ' .
				$this->sqlArCodeTypeId() . ', ' .
				$this->sqlArCodeId() . ', ' .
				'NULL,' .
				'NULL,' .
				$this->sqlArProcessId() . ', ' .
				$this->sqlArTriggerId() . ', ' .
				$this->sqlArOriginId() . ', ' .
				$this->sqlArOriginReferenceId() . ', ' .
				$this->sqlArOriginObjectId() . ', ' .
				$this->sqlCustomerId() . ', ' .
				$this->sqlArTransactionId() . ', ' .
				$this->sqlScheduledChargeId() . ', ' .
				$this->sqlDependentArTransactionId() . ', ' .
				$this->sqlRemotePrimaryKey() . ', ' .
				$this->sqlTransactionDatetime() . ', ' .
				$this->sqlTransactionAmount() . ', ' .
				$this->sqlInitialAmountDue() . ', ' .
				$this->sqlPostDate() . ', ' .
				$this->sqlPostMonth() . ', ' .
				$this->sqlMemo() . ', ' .
				$this->sqlInternalMemo() . ', ' .
				$this->sqlIsDepositCredit() . ', ' .
				$this->sqlIsTemporary() . ', ' .
				$this->sqlIsInitialImport() . ', ' .
				$this->sqlHasDependencies() . ', ' .
				$this->sqlIsDeleted() . ', ' .
				$this->sqlGlDimensionId() . ', ' .
				$this->sqlApDetailId() . ', ' .
				( int ) $intCurrentUserId . ', ' .
				$this->sqlDetails() . ' ) AS result;';

		$boolIsPosted = $this->postArTransaction( $strSql, $objDatabase );

		if( true == $boolIsPosted ) {
			$boolIsPosted &= $this->generateRepaymentArTransaction( $intCurrentUserId, $objDatabase );
		}

		return $boolIsPosted;
	}

	public function postPayment( $intCurrentUserId, $objDatabase ) {

		if( false == $this->getSkipOverrideTransactionDatetime() ) {
			$this->setTransactionDatetime( 'NOW()' );
		}

		$this->formatPostDate();

		$this->checkAndReportInvalidTemporaryTransactions( $objDatabase );

		if( true == valObj( $this->getArPayment(), 'CArPayment' ) ) {
			$objArPayment = $this->m_objArPayment;
		} else {
			$objArPayment = CArPayments::fetchWorksArPaymentByIdByCid( $this->getArPaymentId(), $this->getCid(), $objDatabase );
		}

		if( true == valObj( $objArPayment, 'CArPayment' )
			&& true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' )
			&& true == $this->m_objPropertyGlSetting->getIsArMigrationMode()
			&& ( true == in_array( $objArPayment->getPaymentStatusTypeId(), CPaymentStatusType::$c_arrintMigrationTemporaryReversalPaymentTypes ) )
			&& true == $objArPayment->isElectronicPayment() ) {

			$this->setIsTemporary( 1 );
		}

		$strSql = 'SELECT * FROM ar_transactions_post_ar_transaction( ' .
				'%d, ' .
				$this->sqlCid() . ', ' .
				$this->sqlPropertyId() . ', ' .
				$this->sqlLeaseId() . ',  ' .
				$this->sqlLeaseIntervalId() . ',  ' .
				$this->sqlArCodeTypeId() . ', ' .
				$this->sqlArCodeId() . ', ' .
				$this->sqlArPaymentId() . ', ' .
				$this->sqlArPaymentSplitId() . ', ' .
				$this->sqlArProcessId() . ', ' .
				$this->sqlArTriggerId() . ', ' .
				$this->sqlArOriginId() . ', ' .
				$this->sqlArOriginReferenceId() . ', ' .
				$this->sqlArOriginObjectId() . ', ' .
				$this->sqlCustomerId() . ', ' .
				$this->sqlArTransactionId() . ', ' .
				$this->sqlScheduledChargeId() . ', ' .
				$this->sqlDependentArTransactionId() . ', ' .
				$this->sqlRemotePrimaryKey() . ', ' .
				$this->sqlTransactionDatetime() . ', ' .
				$this->sqlTransactionAmount() . ', ' .
				$this->sqlInitialAmountDue() . ', ' .
				$this->sqlPostDate() . ', ' .
				$this->sqlPostMonth() . ', ' .
				$this->sqlMemo() . ', ' .
				$this->sqlInternalMemo() . ', ' .
				$this->sqlIsDepositCredit() . ', ' .
				$this->sqlIsTemporary() . ', ' .
				$this->sqlIsInitialImport() . ', ' .
				$this->sqlHasDependencies() . ', ' .
				$this->sqlIsDeleted() . ', ' .
				$this->sqlGlDimensionId() . ', ' .
				$this->sqlApDetailId() . ', ' .
				( int ) $intCurrentUserId . ', ' .
				$this->sqlDetails() . ' ) AS result;';

		$boolIsPaymentPosted = $this->postArTransaction( $strSql, $objDatabase );

		if( true == $boolIsPaymentPosted ) {
			$boolIsPaymentPosted &= $this->generateRepaymentArTransaction( $intCurrentUserId, $objDatabase );
		}

		return $boolIsPaymentPosted;
	}

	/**
	 * @param int $intCurrentUserId
	 * @param CDatabase $objDatabase
	 * @return bool
	 */
	public function postUpdate( $intCurrentUserId, $objDatabase ) {

		$this->checkAndReportInvalidTemporaryTransactions( $objDatabase );
		$strSql = 'SELECT * FROM ar_transactions_post_update( ' .
					'%d, ' .
					$this->sqlCid() . ', ' .
					$this->sqlArPaymentId() . ', ' .
					$this->sqlArPaymentSplitId() . ', ' .
					$this->sqlCustomerId() . ', ' .
					$this->sqlArTransactionId() . ', ' .
					$this->sqlRemotePrimaryKey() . ', ' .
					$this->sqlPostMonth() . ', ' .
					$this->sqlDependentArTransactionId() . ', ' .
					$this->sqlMemo() . ', ' .
					$this->sqlInternalMemo() . ', ' .
					$this->sqlIsTemporary() . ', ' .
					$this->sqlHasDependencies() . ', ' .
					$this->sqlIsDeleted() . ', ' .
					$this->sqlGlDimensionId() . ', ' .
					$this->sqlArInvoiceId() . ', ' .
					( int ) $intCurrentUserId . ' ) AS result;';

		return $this->postArTransaction( $strSql, $objDatabase );
	}

	private function postArTransaction( $strSql, $objDatabase ) {

		if( false == checkEntrataModuleWritePermission() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'You have not been granted write access to this module.' ) ) );
			return false;
		}

		$objDataset = $objDatabase->createDataset();

		$intOldId = $this->getId();

		if( true == is_null( $intOldId ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		if( false == $objDataset->execute( sprintf( $strSql, $this->getId() ) ) ) {

			$this->setId( $intOldId );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->setId( $intOldId );

			while( !$objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();
			return false;
		}

		$objDataset->cleanup();
		return true;
	}

	private function generateRepaymentArTransaction( $intCurrentUserId, $objDatabase ) {

		$boolIsValid = true;
		if( false == $this->getIsTemporary() && 0 > $this->getTransactionAmount() && true == is_null( $this->getArTransactionId() ) && true == valId( $this->getRepaymentId() ) ) {
			$objRepaymentArTransaction = $this->createRepaymentArTransaction();
			$objRepaymentArTransaction->setRepaymentId( $this->getRepaymentId() );
			$boolIsValid &= $objRepaymentArTransaction->insert( $intCurrentUserId, $objDatabase );
			$boolIsValid &= $this->generateHudOverPmtArTransaction( $intCurrentUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function calculateTaxAmount( $objDatabase ) {

		if( false == valId( $this->getArCodeId() ) || false == valStr( $this->getTransactionAmount() ) ) {
			return false;
		}

		$arrmixCalculateTaxParameter = [
			'amount' 			=> $this->getTransactionAmount(),
			'post_date' 		=> $this->getPostDate(),
			'charge_code_id' 	=> $this->getArCodeId()
		];

		$objTaxCalculationLibrary   = new \Psi\Core\AccountsReceivables\TaxCalculation\CTaxCalculationLibrary( $this->getCid(), $this->getPropertyId(), $objDatabase, [ $arrmixCalculateTaxParameter['charge_code_id'] ] );
		$fltTaxChargeAmount         = $objTaxCalculationLibrary->getTotalTaxAmount( $arrmixCalculateTaxParameter['charge_code_id'], $arrmixCalculateTaxParameter['amount'], $arrmixCalculateTaxParameter['post_date'] );

		$this->setTaxChargeAmount( round( $fltTaxChargeAmount, 2 ) );
	}

	public function isExported( $objDatabase ) {

		if( false == valId( $this->getId() ) ) {
			return NULL;
		}

		$arrintGlTransactionTypeIds = [ CGlTransactionType::AR_CHARGE ];

		if( CArCodeType::PAYMENT == $this->getArCodeTypeId() ) {
			$arrintGlTransactionTypeIds = [ CGlTransactionType::AR_PAYMENT ];
		} elseif( CArCodeType::DEPOSIT == $this->getArCodeTypeId() ) {
			$arrintGlTransactionTypeIds = [ CGlTransactionType::AR_DEPOSIT_HELD ];
		}

		$arrobjGlDetails = ( array ) CGlDetails::fetchArGlDetailsByReferenceIdsByGlTransactionTypeIdsByCid( [ $this->getId() ], $arrintGlTransactionTypeIds, $this->getCid(), $objDatabase, $boolExportedOnly = true );

		if( true == valArr( $arrobjGlDetails ) ) {
			return true;
		}

		return false;
	}

	public function generateHudOverPmtArTransaction( $intCurrentUserId, $objDatabase ) {

		$boolIsValid    = true;
		$objLease       = ( true == valObj( $this->m_objLease, 'CLease' ) ) ? $this->m_objLease : \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

		// fetch subsidy certification
		$objApplication = ( true == valObj( $objLease, 'CLease' ) ) ? $objLease->fetchApplicationByLeaseIntervalId( $objLease->getActiveLeaseIntervalId(), $objDatabase ) : NULL;

		$objSubsidyCertification = ( true == valObj( $objApplication, 'CApplication' ) && true == valId( $objApplication->getId() ) ) ? \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchActiveSubsidyCertificationByApplicantionIdByCid( $objApplication->getId(), $objLease->getCid(), $objDatabase ) : NULL;

		if( COccupancyType::AFFORDABLE != $objLease->getOccupancyTypeId() || ( true == valObj( $objSubsidyCertification, 'CSubsidyCertification' ) && ( false == valId( $objSubsidyCertification->getSubsidyContractId() ) && true == valId( $objSubsidyCertification->getSetAsideId() ) ) && COccupancyType::AFFORDABLE == $objLease->getOccupancyTypeId() ) || CDefaultArCode::HUDWRITEOFF == $this->getDefaultArCodeId() ) return $boolIsValid;

		$objArCodeFilter = new CArCodesFilter();
		$objArCodeFilter->setDefaultArCodeIds( [ CDefaultArCode::HUDOVERPMT ] );
		$objHudOverPmtArCode        = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getCid(), $objArCodeFilter, $objDatabase );

		if( !valObj( $objHudOverPmtArCode, CArCode::class ) ) {
			return true;
		}

		$objHudOverPmtArTransaction = $objLease->createArTransaction( $objHudOverPmtArCode->getId(), $objDatabase );
		$objHudOverPmtArTransaction->setTransactionAmount( $this->getTransactionAmount() );
		$objHudOverPmtArTransaction->setArCodeTypeId( $objHudOverPmtArCode->getArCodeTypeId() );
		$objHudOverPmtArTransaction->setArOriginId( $objHudOverPmtArCode->getArOriginId() );
		$objHudOverPmtArTransaction->setDependentArTransactionId( $this->getId() );
		$objHudOverPmtArTransaction->setArTriggerId( CArTrigger::UNKNOWN );

		$strAction = ( true == $objHudOverPmtArTransaction->getTransactionAmount() < 0 ) ? 'post_concession_ar_transaction' : 'post_charge_ar_transaction';

		if( false == $objHudOverPmtArTransaction->validate( $strAction, $objDatabase ) || false == $objHudOverPmtArTransaction->postCharge( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		// Need to set has_dependencies to true on the parent transaction
		if( false == $this->getHasDependencies() ) {
			$this->setHasDependencies( true );
			$boolIsValid &= $this->postUpdate( $intCurrentUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function getTotalTransactionAmount() : float {
		$fltTotalTransactionAmount = $this->getTransactionAmount();
		if( true == valArr( $this->getTaxArTransactions() ) ) {
			foreach( $this->getTaxArTransactions() as $objTaxArTransaction ) {
				$fltTotalTransactionAmount = bcadd( ( string ) $fltTotalTransactionAmount, ( string ) $objTaxArTransaction->getTransactionAmount(), 6 );
			}
		}
		return ( float ) $fltTotalTransactionAmount;
	}

	public function getTotalTransactionAmountDue() : float {
		$fltTotalTransactionAmountDue = $this->getTransactionAmountDue();
		if( true == valArr( $this->getTaxArTransactions() ) ) {
			foreach( $this->getTaxArTransactions() as $objTaxArTransaction ) {
				$fltTotalTransactionAmountDue = bcadd( ( string ) $fltTotalTransactionAmountDue, ( string ) $objTaxArTransaction->getTransactionAmountDue(), 6 );
			}
		}
		return ( float ) $fltTotalTransactionAmountDue;
	}

	public function getIsCharge() : bool {
		return $this->getTransactionAmount() > 0;
	}

	public function getIsCredit() : bool {
		return $this->getTransactionAmount() < 0;
	}

	public function isFirstOrLastMonthDepositCredit() : bool {
		return $this->getIsDepositCredit() && ( $this->getApplyToFirstMonth() || $this->getApplyToLastMonth() );
	}

}
?>
