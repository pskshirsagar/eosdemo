<?php

class CArOriginRule extends CBaseArOriginRule {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArOriginId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowPerItem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowLookups() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowPercentCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowPercentCodeGroup() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowPercentCodeType() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowRenewalSameAsProspect() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowRenewalPercentageOfPrior() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseOneTimeLeaseTerms() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseRecurringLeaseTerms() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseOneTimeLeaseStartWindows() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseRecurringLeaseStartWindows() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseOneTimeSpaceConfigurations() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUseRecurringSpaceConfigurations() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowRateOffsets() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowRateSchedulingForRent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valAllowRateSchedulingForDeposits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRateSchedulingForOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valAllowRateCaps() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowPreQualificationTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowApplicationApprovalTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowLeaseApprovalTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowLastLeaseMonthTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowAllowNoticeTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowAnniversaryOfMoveInTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowEndOfYearTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowFinalStatementTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowHourlyTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowDailyTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowWeeklyTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowEveryTwoWeeksTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowTwicePerMonthTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowSpecificMonthsTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowQuarterlyTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowTwicePerYearTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowYearlyTrigger() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>