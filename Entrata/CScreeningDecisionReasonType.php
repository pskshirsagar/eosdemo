<?php

class CScreeningDecisionReasonType extends CBaseScreeningDecisionReasonType {

	const CREDIT                				= 1;
	const CRIMINAL   							= 2;
	const EVICTION              				= 3;
	const INCOME           						= 4;
	const DONOTRENT             				= 5;
	const RESIDENTIAL_HISTORY					= 6;
	const OTHER                 				= 7;
	const UNVERIFIED_INFORMATION_EMPLOYEMENT	= 8;
	const UNVERIFIED_INFORMATION_INCOME			= 9;
	const UNVERIFIED_INFORMATION_IDENTIFICATION	= 10;
	const RENTAL_HISTORY						= 11;
	const PRECISE_ID                            = 12;

	public static function getScreeningDecisionReasonTypeIdByScreenTypeId( $intScreenTypeId ) {
		if( true == is_null( $intScreenTypeId ) ) return NULL;

		switch( $intScreenTypeId ) {
			case CScreenType::CREDIT:
				$intScreeningDecisionReasonTypeId = 1;
				break;

			case CScreenType::CRIMINAL:
			case CScreenType::STATE_CRIMINAL_SEARCH:
			case CScreenType::COUNTY_CRIMINAL_SEARCH:
				$intScreeningDecisionReasonTypeId = 2;
				break;

			case CScreenType::EVICTION:
				$intScreeningDecisionReasonTypeId = 3;
				break;

			case CScreenType::INCOME:
				$intScreeningDecisionReasonTypeId = 4;
				break;

			case CScreenType::DO_NOT_RENT:
				$intScreeningDecisionReasonTypeId = 5;
				break;

			case CScreenType::RENTAL_HISTORY:
				$intScreeningDecisionReasonTypeId = 11;
				break;

			case CScreenType::PRECISE_ID:
				$intScreeningDecisionReasonTypeId = 12;
				break;

			case CScreenType::VERIFICATION_OF_INCOME:
				$intScreeningDecisionReasonTypeId = 9;
				break;

			default:
				$intScreeningDecisionReasonTypeId = NULL;
		}

		return $intScreeningDecisionReasonTypeId;

	}

	public static function getScreeningDecisionReasonNameById( $intScreeningDecisionReasonId ) {
        $strScreeningDecisionReasonName = '';

        switch( $intScreeningDecisionReasonId ) {
            case self::CREDIT:
                $strScreeningDecisionReasonName = 'Credit';
                break;

            case self::CRIMINAL:
                $strScreeningDecisionReasonName = 'Criminal';
                break;

            case self::EVICTION:
                $strScreeningDecisionReasonName = 'Evictions, Filings & Public Records';
                break;

            case self::INCOME:
                $strScreeningDecisionReasonName = 'Income';
                break;

            case self::DONOTRENT:
                $strScreeningDecisionReasonName = 'Rental History Watchlist';
                break;

            case self::RENTAL_HISTORY:
                $strScreeningDecisionReasonName = 'Rental History';
                break;

            case self::RESIDENTIAL_HISTORY:
                $strScreeningDecisionReasonName = 'Residential History';
                break;

			case self::OTHER:
                $strScreeningDecisionReasonName = 'Other';
                break;

            case self::UNVERIFIED_INFORMATION_EMPLOYEMENT:
               	$strScreeningDecisionReasonName = 'Unverified Information Employment';
               	break;

            case self::UNVERIFIED_INFORMATION_INCOME:
            	$strScreeningDecisionReasonName = 'Unverified Information Income';
           		break;

           	case self::UNVERIFIED_INFORMATION_IDENTIFICATION:
           		$strScreeningDecisionReasonName = 'Unverified Information Identification';
           		break;

	        case self::PRECISE_ID:
		        $strScreeningDecisionReasonName = 'Precise ID';
		        break;

			default:
                // added default case
        }

        return $strScreeningDecisionReasonName;
    }

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreenTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>