<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApplications
 * Do not add any new functions to this class.
 */

class CCompanyApplications extends CBaseCompanyApplications {

	public static function fetchSimpleCompanyApplicationsByCid( $intCid, $objDatabase, $boolOrderByTitle = false ) {
		$strSql = 'SELECT * FROM %s WHERE cid = %d AND deleted_on IS NULL ORDER BY';
		if( true == $boolOrderByTitle ) {
			$strSql .= ' title ASC';
		} else {
			$strSql .= ' order_num ASC';
		}
        return self::fetchCompanyApplications( sprintf( $strSql, 'company_applications', ( int ) $intCid ), $objDatabase );
    }

	public static function fetchCompanyApplicationsByIdsByCid( $arrintCompanyApplicationIds, $intCid, $objDatabase ) {
		if( false === valArr( $arrintCompanyApplicationIds ) )		return NULL;

		$strSql = 'SELECT
						*
					FROM
 						company_applications
 					WHERE
 						id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' )
 						AND cid = ' . ( int ) $intCid;

    	return self::fetchCompanyApplications( $strSql, $objDatabase );
    }

    public static function fetchCompanyApplicationsByPropertyIdsByCompanyUserIdByCid( $arrintPropertyIds, $intCompanyUserId, $intCid, $objDatabase ) {
    	if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSql = 'SELECT
    					DISTINCT ON ( ca.id )
    					ca.*
    				FROM
						company_applications ca
					    LEFT OUTER JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					WHERE
    					ca.cid = ' . ( int ) $intCid . '
					   	AND ca.deleted_on IS NULL
					   	AND ( ca.created_by = ' . ( int ) $intCompanyUserId . ' OR pa.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					ORDER BY
					   ca.id,
					   ca.order_num ASC';

        return self::fetchCompanyApplications( $strSql, $objDatabase );
    }

    public static function fetchCompanyApplicationByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {
	 	$strSql = 'SELECT * FROM
	 					company_applications
	 			    WHERE
	 			    	(
	 						primary_document_id = ' . ( int ) $intDocumentId . '
	 			       	OR
	 			       		co_applicant_document_id = ' . ( int ) $intDocumentId . '
	 			       	OR
	 			       		co_signer_document_id = ' . ( int ) $intDocumentId . '
	 			       	OR
	 			       		primary_lease_document_id = ' . ( int ) $intDocumentId . '
	 			       	OR
	 			       		co_applicant_lease_document_id = ' . ( int ) $intDocumentId . '
	 			       	OR
	 			       		co_signer_lease_document_id = ' . ( int ) $intDocumentId . '
						)
	 				  AND cid = ' . ( int ) $intCid;

	 	return self::fetchCompanyApplication( $strSql, $objDatabase );
    }

    public static function fetchSwitchCompanyApplicationByCompanyApplicationIdByDirectionByCid( $intCompanyApplicationId, $intCompanyApplicationOrderNumber, $strDirection, $intCid, $objDatabase ) {
		if( 'up' != $strDirection && 'down' != $strDirection ) return false;

		$strCondition = '';
		if( true == valId( $intCompanyApplicationId ) ) {
			$strCondition = ' AND id = ' . ( int ) $intCompanyApplicationId;
		}

		$strDirectionIndicator 	= ( 'up' == $strDirection ) ? ' < ' : ' > ';
		$strOrderBy 			= ( 'up' == $strDirection ) ? ' DESC ' : ' ASC ';

		$strSql = 'SELECT * FROM
					 	company_applications
					WHERE
						cid = ' . ( int ) $intCid . '
					  	AND order_num  ' . $strDirectionIndicator . ' ' . ( int ) $intCompanyApplicationOrderNumber . '
					  	AND deleted_by IS NULL
				 	ORDER BY
				 		order_num ' . $strOrderBy . '
				 	LIMIT 1';

		return self::fetchCompanyApplication( $strSql, $objDatabase );
	}

    public static function fetchFirstCompanyApplicationByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ca.*
					FROM
				 		company_applications ca
						JOIN property_applications pa ON ( pa.cid = ca.cid AND pa.company_application_id = ca.id )
					WHERE
						pa.property_id = ' . ( int ) $intPropertyId . '
						AND pa.cid = ' . ( int ) $intCid . '
				 		AND	ca.deleted_on IS NULL
					ORDER BY
						ca.order_num, ca.id ASC
					LIMIT 1';

		return self::fetchCompanyApplication( $strSql, $objDatabase );
	}

    public static function fetchFirstCompanyApplicationByOccupancyTypeIdByPropertyIdByCid( $intOccupancyTypeId, $intPropertyId, $intCid, $objDatabase ) {
        $strSql = 'SELECT
						ca.*
					FROM
				 		company_applications ca
						JOIN property_applications pa ON ( pa.cid = ca.cid AND pa.property_id = ' . ( int ) $intPropertyId . ' AND pa.company_application_id = ca.id )
					WHERE
					    ca.cid = ' . ( int ) $intCid . '
						AND ca.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
				 		AND	ca.deleted_on IS NULL
					ORDER BY
						ca.order_num, ca.id ASC
					LIMIT 1';

        return self::fetchCompanyApplication( $strSql, $objDatabase );
    }

	public static function fetchPublishedCompanyApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intOccupancyTypeId = NULL ) {
		$strWhere = '';
		if( false == is_null( $intOccupancyTypeId ) ) {
			$strWhere = ( COccupancyType::MILITARY == $intOccupancyTypeId ) ? ' AND ca.occupancy_type_id = ' . ( int ) $intOccupancyTypeId : ' AND ca.occupancy_type_id <> ' . COccupancyType::MILITARY;
		}

		$strSql = 'SELECT
						DISTINCT ON (ca.id)
						ca.*,
						pa.customer_relationships as customer_relationship_ids
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					 WHERE
					 	pa.property_id = ' . ( int ) $intPropertyId . $strWhere . '
					 	AND pa.cid = ' . ( int ) $intCid . '
					 	AND ca.deleted_on IS NULL
					 	AND ca.is_published = 1;';

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchPublishedCompanyApplicationsByPropertyIdByApplicationPreferencekeyByCid( $intPropertyId, $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ca.*
					FROM
						company_applications ca
					    JOIN property_application_preferences pap ON( pap.company_application_id = ca.id AND pap.cid = ca.cid )
					    JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					WHERE
						pa.property_id = ' . ( int ) $intPropertyId . '
						AND pap.property_id = ' . ( int ) $intPropertyId . '
						AND pa.cid = ' . ( int ) $intCid . '
						AND pap.key = \'' . addslashes( $strKey ) . '\'
						AND pa.is_published = 1
						AND ca.is_published = 1
						AND ca.deleted_on IS NULL';

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchCompanyApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'WITH comp_app AS (
                        SELECT
                            DISTINCT ON (ca.id)
                            ca.*,
                            pa.customer_relationships as customer_relationship_ids
                        FROM
                            company_applications ca
                            JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
                        WHERE
                            pa.property_id = ' . ( int ) $intPropertyId . '
                            AND pa.cid = ' . ( int ) $intCid . '
                            AND ca.deleted_on IS NULL )
					
					SELECT 
					    ca.*
					FROM
					    comp_app ca
					ORDER BY
					    ca.order_num;';

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchPublishedCompanyApplicationByPropertyIdByIdByCid( $intPropertyId, $intCompanyApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON (ca.id)
						ca.*
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					 WHERE
					 	pa.property_id = ' . ( int ) $intPropertyId . '
					 	AND ca.id = ' . ( int ) $intCompanyApplicationId . '
					 	AND ca.cid = ' . ( int ) $intCid . '
					 	AND ca.is_published = 1;';

		return self::fetchCompanyApplication( $strSql, $objDatabase );
	}

	public static function fetchCompanyApplicationCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return false;

		$strSql = 'SELECT
						count( DISTINCT ca.id )
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					WHERE
						pa.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ca.cid = ' . ( int ) $intCid . '
						AND pa.cid = ' . ( int ) $intCid . '
						AND ca.is_published = 1
						AND ca.deleted_on IS NULL
					GROUP BY
						ca.cid';

		$arrintApplicationCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintApplicationCount[0]['count'] ) ) {
			return $arrintApplicationCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function determineHasPublishedOnlineApplicationByPropertyIdByCid( $intPropertyId,  $intCid, $objDatabase ) {
		$intCompanyApplicationCount = self::fetchCompanyApplicationCountByPropertyIdsByCid( [ $intPropertyId ], $intCid, $objDatabase );

		if( true == is_numeric( $intCompanyApplicationCount ) && 0 < $intCompanyApplicationCount ) {
			return true;
		}

		return false;
	}

	public static function determinHasOnlineApplicationByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						COUNT( DISTINCT( ca.id ) )
					FROM
						company_applications ca
						JOIN property_applications pa ON ( ca.id = pa.company_application_id AND ca.cid = pa.cid )
					WHERE
						pa.property_id = ' . ( int ) $intPropertyId . '
						AND pa.cid = ' . ( int ) $intCid . '
						AND ca.deleted_on IS NULL';

		$arrintApplicationCount = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintApplicationCount[0]['count'] ) && 0 < $arrintApplicationCount[0]['count'] ) {
			return true;
		}

		return false;
	}

	public static function fetchCompanyApplicationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolOrderByTitle = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( ca.id )
						ca.*,
						pa.property_id AS property_id
					FROM
					    company_applications ca
					    LEFT JOIN property_applications pa ON ( pa.company_application_id = ca.id AND ca.cid = pa.cid )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
					   	AND ca.deleted_on IS NULL
					   	AND ( pa.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					ORDER BY';

		if( true == $boolOrderByTitle ) {
			$strSql .= ' ca.id, ca.title ASC';
		} else {
			$strSql .= ' ca.id, ca.order_num ASC';
		}

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchPublishedCompanyApplicationByByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON (ca.id)
						ca.*
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
					 	AND pa.property_id = ' . ( int ) $intPropertyId . '
					 	AND ca.is_published = 1
					 	AND ca.deleted_on IS NULL
					ORDER BY
					 	ca.id,
					 	ca.order_num ASC
					LIMIT 1;';

		return self::fetchCompanyApplication( $strSql, $objDatabase );
	}

	public static function fetchCompanyApplicationByTitleByCid( $strTitle, $intCid, $objDatabase ) {

		if( false == valStr( $strTitle ) ) return NULL;

		$strSql = 'SELECT
						id
					FROM
						company_applications
					WHERE
						title = \'' . trim( $strTitle ) . '\'
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL';

		return self::fetchCompanyApplication( $strSql, $objDatabase );
	}

	public static function fetchPublishedCompanyApplicationsByPropertyIdByOccupancyTypeIdsByCid( $intPropertyId, $arrintOccupancyTypeIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON (ca.id)
						ca.*,
						pa.customer_relationships as customer_relationship_ids
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					 WHERE
					 	pa.property_id = ' . ( int ) $intPropertyId . '
					 	AND pa.cid = ' . ( int ) $intCid . '
					 	ANd ca.occupancy_type_id IN ( ' . implode( ',', $arrintOccupancyTypeIds ) . ' )
					 	AND ca.deleted_on IS NULL
					 	AND ca.is_published = 1;';

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchCompanyApplicationsCountByOccupancyTypeIdByCid( $intOccupancyTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						count( ca.id )
					FROM
						company_applications ca
					WHERE
						ca.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND ca.cid = ' . ( int ) $intCid;

		$arrintSubsidizedApplicationCount = fetchData( $strSql, $objDatabase );

		if( true == isset ( $arrintSubsidizedApplicationCount[0]['count'] ) ) {
			return $arrintSubsidizedApplicationCount[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchActiveAvailableCompanyApplicationsByCid( $intCid, $objDatabase, $arrintCompanyApplicationIds = NULL, $strDocumentLocaleCode = NULL ) {
		$strWhereCondition = ( false == is_null( $arrintCompanyApplicationIds ) && true == valArr( $arrintCompanyApplicationIds ) ) ? ' OR ca.id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' )' : '';

		$strLocaleCondition = ( true == valStr( $strDocumentLocaleCode ) ) ? 'AND d.locale_code = \'' . $strDocumentLocaleCode . '\'' : '';

		$strSql = '	SELECT ca.*
					FROM company_applications ca
						LEFT JOIN documents d ON ( ca.cid = d.cid AND d.company_application_ids @> ARRAY[ca.id] ' . $strLocaleCondition . ')
					WHERE ca.cid = ' . ( int ) $intCid . '
						AND ca.deleted_on IS NULL
                        AND ( d.id IS NULL OR d.deleted_on IS NOT NULL ' . $strWhereCondition . ' )
					ORDER BY ca.title ASC';

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchPublishedPropertyIdsFromCompanyApplicationsByOccupancyTypeIds( $intOccupancyTypeId, $intCid, $objDatabase ) {

		$strWhere = ( COccupancyType::MILITARY == $intOccupancyTypeId ) ? ' AND ca.occupancy_type_id = ' . COccupancyType::MILITARY : ' AND ca.occupancy_type_id <> ' . COccupancyType::MILITARY;

		$strSql = 'SELECT
						DISTINCT(pa.property_id)
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					 WHERE
					 	pa.cid = ' . ( int ) $intCid .
		          $strWhere . '
					 	AND ca.deleted_on IS NULL
					 	AND ca.is_published = 1;';

		$arrintData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintData ) ) {
			return rekeyArray( 'property_id', $arrintData );
		}

		return false;
	}

	public static function fetchAllPublishedCompanyApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintExcludedOccupancyTypeIds = [] ) {

		$strExcludedOccupancyTypes = '';
		if( true == valArr( $arrintExcludedOccupancyTypeIds ) ) {
			$strExcludedOccupancyTypes = ' AND ca.occupancy_type_id NOT IN( ' . implode( ',', $arrintExcludedOccupancyTypeIds ) . ' )';
		}

		$strSql = 'SELECT
						DISTINCT ON (ca.id)
						ca.*
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					 WHERE
					    pa.property_id = ' . ( int ) $intPropertyId . $strExcludedOccupancyTypes . '
                        AND pa.cid = ' . ( int ) $intCid . '
                        AND ca.is_published = 1
                        AND ca.deleted_on IS NULL';

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchActiveCompanyApplicationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintExcludedOccupancyTypeIds = NULL ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						ca.*,
						pa.property_id
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.cid = ca.cid AND pa.company_application_id = ca.id )
					 WHERE
					 	pa.property_id = ' . ( int ) $intPropertyId . '
					 	' . ( ( true == valArr( $arrintExcludedOccupancyTypeIds ) ) ? ' AND ca.occupancy_type_id NOT IN ( ' . implode( ',', $arrintExcludedOccupancyTypeIds ) . ' )' : '' ) . '
					 	AND pa.cid = ' . ( int ) $intCid . '
					 	AND ca.deleted_on IS NULL
						AND ca.is_published = 1;';

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchCompanyApplicationNameById( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						title
					FROM
						company_applications
					 WHERE
					 	cid = ' . ( int ) $intCid . '
					 	AND id = ' . ( int ) $intId;

		return self::fetchColumn( $strSql, 'title', $objDatabase );
	}

	public static function fetchPublishedCompanyApplicationsByPropertyIdsByOccupancyTypeIdsByCid( $arrintPropertyIds, $arrintOccupancyTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}

		$strCondition = '';
		if( true == valArr( $arrintOccupancyTypeIds ) ) {
			$strCondition = ' AND ca.occupancy_type_id IN( ' . implode( ',', $arrintOccupancyTypeIds ) . ' )';
		}

		$strSql = 'SELECT
						DISTINCT ON (ca.id)
						ca.*
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.company_application_id = ca.id AND pa.cid = ca.cid )
					 WHERE
					 	pa.cid = ' . ( int ) $intCid . '
					 	AND ( pa.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					 	' . $strCondition . '
					 	AND ca.deleted_on IS NULL
					 	AND ca.is_published = 1;';

		return self::fetchCompanyApplications( $strSql, $objDatabase );
	}

	public static function fetchActiveCompanyApplicationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $arrintExcludedOccupancyTypeIds = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ca.id,
						ca.title,
						pa.property_id
					FROM
						company_applications ca
						JOIN property_applications pa ON ( pa.cid = ca.cid AND pa.company_application_id = ca.id )
					 WHERE
					 	pa.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					 	' . ( ( true == valArr( $arrintExcludedOccupancyTypeIds ) ) ? ' AND ca.occupancy_type_id NOT IN ( ' . implode( ',', $arrintExcludedOccupancyTypeIds ) . ' )' : '' ) . '
					 	AND pa.cid = ' . ( int ) $intCid . '
					 	AND ca.deleted_on IS NULL
						AND ca.is_published = 1;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveCompanyApplicationsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						ca.id,
						ca.title
					FROM
						company_applications ca
					 WHERE
					 	ca.id IN ( ' . implode( ',', $arrintIds ) . ' )
					 	AND ca.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

}
?>