<?php

class CApiQueue extends CBaseApiQueue {

	use TEosStoredObject;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntegrationResultId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinorApiVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMajorApiVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQueueMessageCorrelationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		return date( 'Y/m/d' ) . '/queue_response_' . $this->getId() . '.txt';
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		return CConfig::get( 'OSG_BUCKET_API' );
	}

	public function getTitle() {
		return 'queue_response_' . $this->getId();
	}

}
?>