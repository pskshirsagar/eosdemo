<?php

use Psi\Core\Payment\AcceptedPaymentTypes\CAcceptedPaymentTypesService;
use Psi\Eos\Entrata\CPropertyEmailRules;
use Psi\Libraries\UtilPsHtmlMimeMail\CPsHtmlMimeMail;

class CScheduledPayment extends CBaseScheduledPayment {
	use \Psi\Libraries\Container\TContainerized;

	protected $m_intDoesntHaveUnitNumber;
	protected $m_intCustomerPaymentAccountId;
	protected $m_intCorrespondingScheduledPaymentDetailId;

	protected $m_arrstrStartMonths;
	protected $m_arrstrEndMonths;

	protected $m_arrintRecurringPaymentDays;
	protected $m_arrintSplitPaymentAmountRates;
	protected $m_arrintBiMonthlyRecurringPaymentDays;

	protected $m_arrfltSplitPaymentAmounts;

	protected $m_boolIsFloating;
	protected $m_intCcBinNumber;

	protected $m_boolIsDebitCard;
	protected $m_boolIsCheckCard;
	protected $m_boolIsGiftCard;
	protected $m_boolIsPrepaidCard;
	protected $m_boolIsCreditCard;
	protected $m_boolIsInternationalCard;
	protected $m_boolIsCommercialCard;

	protected $m_boolIsStaticSplitPayment;
	protected $m_boolIsShowDonationAmount;
	protected $m_boolSpecifyPaymentCeiling;
	protected $m_boolIsCalculator;
	protected $m_boolIsConnectedPaymentAccountExist;

	// This is a flag to identify whether to use billing indformation or not.Is used in Resident Portal
	protected $m_boolUseStoredBillingInfo;

	protected $m_boolBillingInfoIsStored;
	protected $m_boolIsUpdateStoredBillingInfo;
	// This identifies this payment is portal payment or not PBY.
	protected $m_boolIsResidentPortalPayment;
	protected $m_boolBiMonthlySpecifyPaymentCeiling;

	protected $m_intBillDay;
	protected $m_intStartMonth;
	protected $m_intEndMonth;

	protected $m_intSplitBetweenRoommates;
	protected $m_intSplitRoommateBillDay;
	protected $m_intSplitRoommateStartMonth;
	protected $m_intSplitRoommateEndMonth;

	protected $m_intBiMonthlyAutoPayment;
	protected $m_intBiMonthlyFirstBillDay;
	protected $m_intBiMonthlySecondBillDay;
	protected $m_intBiMonthlyStartMonth;
	protected $m_intBiMonthlyEndMonth;

	protected $m_intLeaseStatusTypeId;
	protected $m_intSavingLeaseId;
	protected $m_intPendingRoommatesCount;
	protected $m_intCreatedCustomerId;
	protected $m_intUpdatedCustomerId;
	protected $m_intDeletedCustomerId;

	protected $m_objClient;
	protected $m_objProperty;
	protected $m_objLease;
	protected $m_objLeaseCustomer;
	protected $m_objAllowRecurringPaymentCeilingPropertyPreference;
	protected $m_objMerchantAccount;
	protected $m_objCustomer;
	protected $m_objScheduledPaymentDetail;
	protected $m_objSmarty;
	protected $m_objSystemEmail;
	protected $m_objEmailDatabase;

	protected $m_arrobjPaymentTypes;
	protected $m_arrobjArPayments;
	protected $m_arrobjRecurringPaymentSpecials;

	protected $m_fltConvenienceFeeAmount;
	protected $m_fltEstimatedPaymentAmount;

	protected $m_fltTotalLeaseBalance;
	protected $m_fltBiMonthlyFirstPaymentAmount;
	protected $m_fltBiMonthlySecondPaymentAmount;
	protected $m_fltBiMonthlyFirstPaymentCeilingAmount;
	protected $m_fltBiMonthlySecondPaymentCeilingAmount;
	protected $m_fltPaymentAmountConfirmation;
	protected $m_fltPaymentCeilingAmountConfirmation;

	protected $m_strAuthToken;
	protected $m_strLeaseEndDate;
	protected $m_strAccountNickname;
	// This is used to store the confirm check account number
	protected $m_strConfirmCheckAccountNumber;

	protected $m_strBilltoIpAddress;
	protected $m_strBilltoAccountNumber;
	protected $m_strBilltoCompanyName;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoStreetLine3;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strBilltoPhoneNumber;
	protected $m_strBilltoEmailAddress;
	protected $m_intSplitEmailsCount;
	protected $m_strSplitReminderOn;
	protected $m_strCcCardNumberEncrypted;
	protected $m_strCcExpDateMonth;
	protected $m_strCcExpDateYear;
	protected $m_strCcNameOnCard;
	protected $m_strCheckBankName;
	protected $m_strCheckNameOnAccount;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_intSecureReferenceNumber;
	protected $m_strMobileNumber;
	protected $m_intAccountVerificationLogId;
	protected $m_strPaymentBankAccountId;

	// Variable is used to store that the user agreed to pay the convenience fee.
	protected $m_boolAgreesToPayConvenienceFees;

	// Used to collect data on whether resident agreed to terms and conditions.
	protected $m_boolAgreesToTerms;

	// Charity donation flag
	protected $m_boolApplyDonationFlag;

	// This will hold a flag, in case of split payment whether we should apply donation flag
	protected $m_boolIsMapPostDonationData;

	// Variable is used to store that the user agreed to pay the donation amount.
	protected $m_boolAgreesToPayDonationAmount;

	// used to allow international credit cards
	protected $m_boolIsAllowInternationalCard;

	// used to indicate if today is last day from list of approved post on days or not.
	protected $m_boolIsApprovedPostOnLastDay;
	protected $m_fltMaximumMerchantPaymentAmount;
	protected $m_boolHasAmountExeceededMerchantLimit;
	protected $m_boolIsRecentArPayment;
	protected $m_boolAgreesToDuplicateAutopayment;
	protected $m_boolIsAutoPaymentAllowed;
	protected $m_boolIsPaused;

	protected $m_boolIsTranslateEmail = false;
	protected $m_strCustomerPreferredLocaleCode;
	protected $m_boolIsPlaidAccount;

	const STR_UNTIL_I_CANCEL = 'Until I Cancel';
	const STR_UNTIL_I_CANCEL_OR_MOVE_OUT = 'Until I Cancel or Move Out';
	// We are providing back-word support for Resident App, Below Constant are only used into Resident Portal > API
	const STR_LEASE_END_DATE = 'Lease end date';
	const STR_UNTIL_I_MOVE_OUT = 'Until I move out';
	const STR_WHEN_I_CANCEL = 'When I cancel';

	const STATIC_RECURR_PAYMENTS				= 1;
	const VARIABLE_RECURR_PAYMENTS				= 2;
	const STATIC_AND_VARIABLE_RECURR_PAYMENTS	= 3;
	const DO_NOT_ALLOW_RECURR_PAYMENTS			= 4;

	const ALLOWED_RECURRING_PAYMENT_CEILING		= 1;
	const REQUIRED_RECURRING_PAYMENT_CEILING	= 2;

	const SPECIFY_EXACT_AMOUNT					= 2;

	const LAST_DAY_OF_THE_MONTH = 31;
	const SECOND_LAST_DAY_OF_THE_MONTH = 30;
	const THIRD_LAST_DAY_OF_THE_MONTH = 29;

	public function __construct() {
		parent::__construct();

		$this->m_fltConvenienceFeeAmount 	= 0;
		$this->m_fltPaymentAmount 			= 0;
		$this->m_fltPaymentCeilingAmount 	= 0;
		$this->m_boolBillingInfoIsStored 	= true;
		$this->m_boolSpecifyPaymentCeiling 	= false;
		$this->m_boolIsCalculator			= false;

		$this->m_boolIsApprovedPostOnLastDay 	= false;
		$this->m_boolIsAutoPaymentAllowed 		= false;
		$this->m_boolIsPaused 					= false;
		$this->m_boolIsConnectedPaymentAccountExist = true;
		return;
	}

	/**
	 * Get Functions
	 */

	public function getIsCreditCard() {
		return $this->m_boolIsCreditCard;
	}

	public function setIsCreditCard( $boolIsCreditCard ) {
		$this->m_boolIsCreditCard = $boolIsCreditCard;
	}

	public function getIsGiftCard() {
		return $this->m_boolIsGiftCard;
	}

	public function setIsGiftCard( $boolIsGiftCard ) {
		$this->m_boolIsGiftCard = $boolIsGiftCard;
	}

	public function getIsPrepaidCard() {
		return $this->m_boolIsPrepaidCard;
	}

	public function setIsPrepaidCard( $boolIsPrepaidCard ) {
		$this->m_boolIsPrepaidCard = $boolIsPrepaidCard;
	}

	public function getIsInternationalCard() {
		return $this->m_boolIsInternationalCard;
	}

	public function setIsInternationalCard( $boolIsInternationalCard ) {
		$this->m_boolIsInternationalCard = $boolIsInternationalCard;
	}

	public function getIsCommercialCard() {
		return $this->m_boolIsCommercialCard;
	}

	public function setIsCommercialCard( $boolIsCommercialCard ) {
		$this->m_boolIsCommercialCard = $boolIsCommercialCard;
	}

	public function getIsCheckCard() {
		return $this->m_boolIsCheckCard;
	}

	public function setIsCheckCard( $boolIsCheckCard ) {
		$this->m_boolIsCheckCard = $boolIsCheckCard;
	}

	public function setAuthToken( $strAuthToken ) {
		$this->m_strAuthToken = $strAuthToken;
	}

	public function getAuthToken() {
		return $this->m_strAuthToken;
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->m_strMobileNumber = CStrings::strTrimDef( $strMobileNumber, 30, NULL, true );
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function setBilltoIpAddress( $strBilltoIpAddress ) {
		$this->m_strBilltoIpAddress = CStrings::strTrimDef( $strBilltoIpAddress, 23, NULL, true );
	}

	public function getBilltoIpAddress() {
		return $this->m_strBilltoIpAddress;
	}

	public function sqlBilltoIpAddress() {
		return ( true == isset( $this->m_strBilltoIpAddress ) ) ? '\'' . addslashes( $this->m_strBilltoIpAddress ) . '\'' : 'NULL';
	}

	public function setBilltoAccountNumber( $strBilltoAccountNumber ) {
		$this->m_strBilltoAccountNumber = CStrings::strTrimDef( $strBilltoAccountNumber, 50, NULL, true );
	}

	public function getBilltoAccountNumber() {
		return $this->m_strBilltoAccountNumber;
	}

	public function sqlBilltoAccountNumber() {
		return ( true == isset( $this->m_strBilltoAccountNumber ) ) ? '\'' . addslashes( $this->m_strBilltoAccountNumber ) . '\'' : 'NULL';
	}

	public function setBilltoCompanyName( $strBilltoCompanyName ) {
		$this->m_strBilltoCompanyName = CStrings::strTrimDef( $strBilltoCompanyName, 100, NULL, true );
	}

	public function getBilltoCompanyName() {
		return $this->m_strBilltoCompanyName;
	}

	public function sqlBilltoCompanyName() {
		return ( true == isset( $this->m_strBilltoCompanyName ) ) ? '\'' . addslashes( $this->m_strBilltoCompanyName ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->m_strBilltoStreetLine1 = CStrings::strTrimDef( $strBilltoStreetLine1, 100, NULL, true );
	}

	public function getBilltoStreetLine1() {
		return $this->m_strBilltoStreetLine1;
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->m_strBilltoStreetLine2 = CStrings::strTrimDef( $strBilltoStreetLine2, 100, NULL, true );
	}

	public function getBilltoStreetLine2() {
		return $this->m_strBilltoStreetLine2;
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' : 'NULL';
	}

	public function setBilltoStreetLine3( $strBilltoStreetLine3 ) {
		$this->m_strBilltoStreetLine3 = CStrings::strTrimDef( $strBilltoStreetLine3, 100, NULL, true );
	}

	public function getBilltoStreetLine3() {
		return $this->m_strBilltoStreetLine3;
	}

	public function sqlBilltoStreetLine3() {
		return ( true == isset( $this->m_strBilltoStreetLine3 ) ) ? '\'' . addslashes( $this->m_strBilltoStreetLine3 ) . '\'' : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->m_strBilltoCity = CStrings::strTrimDef( $strBilltoCity, 50, NULL, true );
	}

	public function getBilltoCity() {
		return $this->m_strBilltoCity;
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? '\'' . addslashes( $this->m_strBilltoCity ) . '\'' : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->m_strBilltoStateCode = CStrings::strTrimDef( $strBilltoStateCode, 2, NULL, true );
	}

	public function getBilltoStateCode() {
		return $this->m_strBilltoStateCode;
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->m_strBilltoProvince = CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->m_strBilltoPostalCode = CStrings::strTrimDef( $strBilltoPostalCode, 20, NULL, true );
	}

	public function getBilltoPostalCode() {
		return $this->m_strBilltoPostalCode;
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->m_strBilltoCountryCode = CStrings::strTrimDef( $strBilltoCountryCode, 2, NULL, true );
	}

	public function getBilltoCountryCode() {
		return $this->m_strBilltoCountryCode;
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' : 'NULL';
	}

	public function getBilltoPhoneNumber() {
		return $this->m_strBilltoPhoneNumber;
	}

	public function sqlBilltoPhoneNumber() {
		return ( true == isset( $this->m_strBilltoPhoneNumber ) ) ? '\'' . addslashes( $this->m_strBilltoPhoneNumber ) . '\'' : 'NULL';
	}

	public function setBilltoEmailAddress( $strBilltoEmailAddress ) {
		$this->m_strBilltoEmailAddress = CStrings::strTrimDef( $strBilltoEmailAddress, 240, NULL, true );
	}

	public function getBilltoEmailAddress() {
		return $this->m_strBilltoEmailAddress;
	}

	public function sqlBilltoEmailAddress() {
		return ( true == isset( $this->m_strBilltoEmailAddress ) ) ? '\'' . addslashes( $this->m_strBilltoEmailAddress ) . '\'' : 'NULL';
	}

	public function setSplitEmailsCount( $intSplitEmailsCount ) {
		$this->m_intSplitEmailsCount = CStrings::strToIntDef( $intSplitEmailsCount, NULL, false );
	}

	public function getSplitEmailsCount() {
		return $this->m_intSplitEmailsCount;
	}

	public function sqlSplitEmailsCount() {
		return ( true == isset( $this->m_intSplitEmailsCount ) ) ? ( string ) $this->m_intSplitEmailsCount : '0';
	}

	public function setSplitReminderOn( $strSplitReminderOn ) {
		$this->m_strSplitReminderOn = CStrings::strTrimDef( $strSplitReminderOn, -1, NULL, true );
	}

	public function getSplitReminderOn() {
		return $this->m_strSplitReminderOn;
	}

	public function sqlSplitReminderOn() {
		return ( true == isset( $this->m_strSplitReminderOn ) ) ? '\'' . $this->m_strSplitReminderOn . '\'' : 'NULL';
	}

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->m_strCcCardNumberEncrypted = CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function sqlCcCardNumberEncrypted() {
		return ( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCcCardNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setCcExpDateMonth( $strCcExpDateMonth ) {
		$this->m_strCcExpDateMonth = CStrings::strTrimDef( $strCcExpDateMonth, 2, NULL, true );
	}

	public function getCcExpDateMonth() {
		return $this->m_strCcExpDateMonth;
	}

	public function sqlCcExpDateMonth() {
		return ( true == isset( $this->m_strCcExpDateMonth ) ) ? '\'' . addslashes( $this->m_strCcExpDateMonth ) . '\'' : 'NULL';
	}

	public function setCcExpDateYear( $strCcExpDateYear ) {
		$this->m_strCcExpDateYear = CStrings::strTrimDef( $strCcExpDateYear, 4, NULL, true );
	}

	public function getCcExpDateYear() {
		return $this->m_strCcExpDateYear;
	}

	public function sqlCcExpDateYear() {
		return ( true == isset( $this->m_strCcExpDateYear ) ) ? '\'' . addslashes( $this->m_strCcExpDateYear ) . '\'' : 'NULL';
	}

	public function setCcNameOnCard( $strCcNameOnCard ) {
		$this->m_strCcNameOnCard = CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true );
	}

	public function getCcNameOnCard() {
		return $this->m_strCcNameOnCard;
	}

	public function sqlCcNameOnCard() {
		return ( true == isset( $this->m_strCcNameOnCard ) ) ? '\'' . addslashes( $this->m_strCcNameOnCard ) . '\'' : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->m_strCheckBankName = CStrings::strTrimDef( $strCheckBankName, 100, NULL, true );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? '\'' . addslashes( $this->m_strCheckBankName ) . '\'' : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->m_strCheckNameOnAccount = CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->m_intCheckAccountTypeId = CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->m_strCheckRoutingNumber = CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' : 'NULL';
	}

	public function getCheckBankNumber() {
		if( CPaymentType::PAD !== $this->getPaymentTypeId() ) {
			return NULL;
		}

		return substr( $this->m_strCheckRoutingNumber, 1, 3 );
	}

	public function getCheckBankTransitNumber() {
		if( CPaymentType::PAD !== $this->getPaymentTypeId() ) {
			return NULL;
		}

		return substr( $this->m_strCheckRoutingNumber, 4 );
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->m_strCheckAccountNumberEncrypted = CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setSecureReferenceNumber( $intSecureReferenceNumber ) {
		$this->m_intSecureReferenceNumber = CStrings::strToIntDef( $intSecureReferenceNumber, NULL, false );
	}

	public function getSecureReferenceNumber() {
		return $this->m_intSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_intSecureReferenceNumber ) ) ? ( string ) $this->m_intSecureReferenceNumber : 'NULL';
	}

	public function setPaymentBankAccountId( $strPaymentBankAccountId ) {
		$this->m_strPaymentBankAccountId = CStrings::strTrimDef( $strPaymentBankAccountId, NULL, NULL, true );
	}

	public function getPaymentBankAccountId() {
		return $this->m_strPaymentBankAccountId;
	}

	public function sqlPaymentBankAccountId() {
		return ( true == isset( $this->m_strPaymentBankAccountId ) ) ? '\'' . addslashes( $this->m_strPaymentBankAccountId ) . '\'' : 'NULL';
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getTotalLeaseBalance() {
		return $this->m_fltTotalLeaseBalance;
	}

	public function getLeaseStatusTypeName() {

		$strStatusTypeName = CLeaseStatusType::getLeaseStatusTypeNameByLeaseStatusTypeId( $this->getLeaseStatusTypeId() );

		return $strStatusTypeName;
	}

	public function getSavingLeaseId() {
		return $this->m_intSavingLeaseId;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getCorrespondingScheduledPaymentDetailId() {
		return $this->m_intCorrespondingScheduledPaymentDetailId;
	}

	public function getSpecifyPaymentCeiling() {
		return $this->m_boolSpecifyPaymentCeiling;
	}

	public function getIsCalculator() {
		return $this->m_boolIsCalculator;
	}

	public function getUseStoredBillingInfo() {
		return $this->m_boolUseStoredBillingInfo;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	public function getAgreesToPayConvenienceFees() {
		return $this->m_boolAgreesToPayConvenienceFees;
	}

	public function getAgreesToTerms() {
		return $this->m_boolAgreesToTerms;
	}

	public function getAgreesToDuplicateAutopayment() {
		return $this->m_boolAgreesToDuplicateAutopayment;
	}

	public function getConvenienceFeeAmount() {
		return $this->m_fltConvenienceFeeAmount;
	}

	public function getPaymentAmountConfirmation() {
		return $this->m_fltPaymentAmountConfirmation;
	}

	public function getEstimatedPaymentAmount() {
		return $this->m_fltEstimatedPaymentAmount;
	}

	public function getBiMonthlyFirstPaymentAmount() {
		return $this->m_fltBiMonthlyFirstPaymentAmount;
	}

	public function getBiMonthlySecondPaymentAmount() {
		return $this->m_fltBiMonthlySecondPaymentAmount;
	}

	public function getPaymentCeilingAmountConfirmation() {
		return $this->m_fltPaymentCeilingAmountConfirmation;
	}

	public function getMerchantAccount() {
		return $this->m_objMerchantAccount;
	}

	public function getHasAmountExeceededMerchantLimit() {
		return $this->m_boolHasAmountExeceededMerchantLimit;
	}

	public function getBillDay() {
		return $this->m_intBillDay;
	}

	public function getBiMonthlyAutoPayment() {
		return $this->m_intBiMonthlyAutoPayment;
	}

	public function getBiMonthlyFirstBillDay() {
		return $this->m_intBiMonthlyFirstBillDay;
	}

	public function getBiMonthlySecondBillDay() {
		return $this->m_intBiMonthlySecondBillDay;
	}

	public function getBiMonthlyFirstPaymentCeilingAmount() {
		return $this->m_fltBiMonthlyFirstPaymentCeilingAmount;
	}

	public function getBiMonthlySecondPaymentCeilingAmount() {
		return $this->m_fltBiMonthlySecondPaymentCeilingAmount;
	}

	public function getBiMonthlySpecifyPaymentCeiling() {
		return $this->m_boolBiMonthlySpecifyPaymentCeiling;
	}

	public function getSplitBetweenRoommates() {
		return $this->m_intSplitBetweenRoommates;
	}

	public function getIsStaticSplitPayment() {
		return $this->m_boolIsStaticSplitPayment;
	}

	public function getSplitRoommateBillDay() {
		return $this->m_intSplitRoommateBillDay;
	}

	public function getIsFloating() {
		return $this->m_boolIsFloating;
	}

	public function getIsDebitCard() {
		return $this->m_boolIsDebitCard;
	}

	public function getSplitRoommateStartMonth() {
		return $this->m_intSplitRoommateStartMonth;
	}

	public function getStartMonth() {
		return $this->m_intStartMonth;
	}

	public function getBiMonthlyStartMonth() {
		return $this->m_intBiMonthlyStartMonth;
	}

	public function getStartMonths() {
		return $this->m_arrstrStartMonths;
	}

	public function getSplitRoommateEndMonth() {
		return $this->m_intSplitRoommateEndMonth;
	}

	public function getEndMonth() {
		return $this->m_intEndMonth;
	}

	public function getBiMonthlyEndMonth() {
		return $this->m_intBiMonthlyEndMonth;
	}

	public function getEndMonths() {
		return $this->m_arrstrEndMonths;
	}

	public function getRecurringPaymentDays() {
		return $this->m_arrintRecurringPaymentDays;
	}

	public function getBiMonthlyRecurringPaymentDays() {
		return $this->m_arrintBiMonthlyRecurringPaymentDays;
	}

	public function getSplitPaymentAmounts() {
		return $this->m_arrfltSplitPaymentAmounts;
	}

	public function getSplitPaymentAmountRates() {
		return $this->m_arrintSplitPaymentAmountRates;
	}

	public function getPaymentTypes() {
		return $this->m_arrobjPaymentTypes;
	}

	public function getRecurringPaymentSpecials() {
		return $this->m_arrobjRecurringPaymentSpecials;
	}

	public function getLease() {
		return $this->m_objLease;
	}

	public function getPaymentCeiling() {
		return $this->m_boolSpecifyPaymentCeiling;
	}

	public function getAllowRecurringPaymentCeilingPropertyPreference() {
		return $this->m_objAllowRecurringPaymentCeilingPropertyPreference;
	}

	public function getCcCardNumber() {
		if( false == valStr( $this->getCcCardNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCcCardNumberEncrypted(), CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] );
	}

	public function getCheckAccountNumber() {
		if( false == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCcCardNumberMasked() {
		$strCcCardNumber = $this->getCcCardNumber();
		$intStringLength = strlen( $strCcCardNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCcCardNumber, -4 );

		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getBilltoNameFull() {
		return trim( $this->m_strBilltoNameFirst ) . ' ' . trim( $this->m_strBilltoNameLast );
	}

	public function getArPayments() {
		return $this->m_arrobjArPayments;
	}

	public function getLeaseCustomer() {
		return $this->m_objLeaseCustomer;
	}

	public function getIsResidentPortalPayment() {
		return $this->m_boolIsResidentPortalPayment;
	}

	public function getConfirmCheckAccountNumber() {
		return $this->m_strConfirmCheckAccountNumber;
	}

	public function getFormattedBilltoPhoneNumber() {

		$strPhoneNumber = $this->getBilltoPhoneNumber();

		if( 0 < strlen( $strPhoneNumber ) ) {
			$strPhoneNumber = preg_replace( '/[^0-9]+/', '', $strPhoneNumber );

			if( 0 == strlen( $strPhoneNumber ) ) {
				return NULL;
			}

			return  \Psi\CStringService::singleton()->substr( $strPhoneNumber, 0, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strPhoneNumber, 3, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strPhoneNumber, 6, strlen( $strPhoneNumber ) - 6 );
		}

		return NULL;
	}

	public function getIsAllowInternationalCard() {
		return $this->m_boolIsAllowInternationalCard;
	}

	public function getIsApprovedPostOnLastDay() {
		return $this->m_boolIsApprovedPostOnLastDay;
	}

	public function getMaximumMerchantAmount() {
		return $this->m_fltMaximumMerchantPaymentAmount;
	}

	public function getIsRecentArPayment() {
		return $this->m_boolIsRecentArPayment;
	}

	public function getIsAutoPaymentAllowed() {
		return $this->m_boolIsAutoPaymentAllowed;
	}

	public function getIsPaused() {
		return $this->m_boolIsPaused;
	}

	public function getIsConnectedPaymentAccountExist() {
		return $this->m_boolIsConnectedPaymentAccountExist;
	}

	public function getApplyDonationFlag() {
		return $this->m_boolApplyDonationFlag;
	}

	public function getIsMapPostDonationData() {
		return $this->m_boolIsMapPostDonationData;
	}

	public function getAgreesToPayDonationAmount() {
		return $this->m_boolAgreesToPayDonationAmount;
	}

	public function getPayableAmountForSplitPayment() {

		if( false == is_null( $this->m_fltPaymentAmountRate ) ) {
			$fltPayableAmount = ( $this->m_fltPaymentAmount * $this->m_fltPaymentAmountRate ) / 100;
			return round( $fltPayableAmount, 2 );
		}
	}

	public function getPendingRoommatesCount() {
		return $this->m_intPendingRoommatesCount;
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function getIsUpdateStoredBillingInfo() {
		return $this->m_boolIsUpdateStoredBillingInfo;
	}

	public function getBillingInfoIsStored() {
		return $this->m_boolBillingInfoIsStored;
	}

	public function getCustomerPaymentAccountId() {
		return $this->m_intCustomerPaymentAccountId;
	}

	public function getAccountNickname() {
		return $this->m_strAccountNickname;
	}

	public function sqlAccountVerificationLogId() {
		return ( true == isset( $this->m_intAccountVerificationLogId ) ) ? ( string ) $this->m_intAccountVerificationLogId : 'NULL';
	}

	public function getAccountVerificationLogId() {
		return $this->m_intAccountVerificationLogId;
	}

	public function getCreatedCustomerId() {
		return $this->m_intCreatedCustomerId;
	}

	public function getUpdatedCustomerId() {
		return $this->m_intUpdatedCustomerId;
	}

	public function getDeletedCustomerId() {
		return $this->m_intDeletedCustomerId;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

		$this->setBilltoIpAddress( getRemoteIpAddress() );

		$this->m_intCheckAccountTypeId = CCheckAccountType::PERSONAL_CHECKING;

		return;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['billto_ip_address'] ) && $boolDirectSet ) {
			$this->m_strBilltoIpAddress = trim( stripcslashes( $arrmixValues['billto_ip_address'] ) );
		} elseif( isset( $arrmixValues['billto_ip_address'] ) ) {
			$this->setBilltoIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_ip_address'] ) : $arrmixValues['billto_ip_address'] );
		}

		if( isset( $arrmixValues['billto_account_number'] ) && $boolDirectSet ) {
			$this->m_strBilltoAccountNumber = trim( stripcslashes( $arrmixValues['billto_account_number'] ) );
		} elseif( isset( $arrmixValues['billto_account_number'] ) ) {
			$this->setBilltoAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_account_number'] ) : $arrmixValues['billto_account_number'] );
		}

		if( isset( $arrmixValues['billto_company_name'] ) && $boolDirectSet ) {
			$this->m_strBilltoCompanyName = trim( stripcslashes( $arrmixValues['billto_company_name'] ) );
		} elseif( isset( $arrmixValues['billto_company_name'] ) ) {
			$this->setBilltoCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_company_name'] ) : $arrmixValues['billto_company_name'] );
		}

		if( isset( $arrmixValues['billto_street_line1'] ) && $boolDirectSet ) {
			$this->m_strBilltoStreetLine1 = trim( stripcslashes( $arrmixValues['billto_street_line1'] ) );
		} elseif( isset( $arrmixValues['billto_street_line1'] ) ) {
			$this->setBilltoStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_street_line1'] ) : $arrmixValues['billto_street_line1'] );
		}

		if( isset( $arrmixValues['billto_street_line2'] ) && $boolDirectSet ) {
			$this->m_strBilltoStreetLine2 = trim( stripcslashes( $arrmixValues['billto_street_line2'] ) );
		} elseif( isset( $arrmixValues['billto_street_line2'] ) ) {
			$this->setBilltoStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_street_line2'] ) : $arrmixValues['billto_street_line2'] );
		}

		if( isset( $arrmixValues['billto_street_line3'] ) && $boolDirectSet ) {
			$this->m_strBilltoStreetLine3 = trim( stripcslashes( $arrmixValues['billto_street_line3'] ) );
		} elseif( isset( $arrmixValues['billto_street_line3'] ) ) {
			$this->setBilltoStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_street_line3'] ) : $arrmixValues['billto_street_line3'] );
		}

		if( isset( $arrmixValues['billto_city'] ) && $boolDirectSet ) {
			$this->m_strBilltoCity = trim( stripcslashes( $arrmixValues['billto_city'] ) );
		} elseif( isset( $arrmixValues['billto_city'] ) ) {
			$this->setBilltoCity( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_city'] ) : $arrmixValues['billto_city'] );
		}

		if( isset( $arrmixValues['billto_state_code'] ) && $boolDirectSet ) {
			$this->m_strBilltoStateCode = trim( stripcslashes( $arrmixValues['billto_state_code'] ) );
		} elseif( isset( $arrmixValues['billto_state_code'] ) ) {
			$this->setBilltoStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_state_code'] ) : $arrmixValues['billto_state_code'] );
		}

		if( isset( $arrmixValues['billto_province'] ) && $boolDirectSet ) {
			$this->m_strBilltoProvince = trim( stripcslashes( $arrmixValues['billto_province'] ) );
		} elseif( isset( $arrmixValues['billto_province'] ) ) {
			$this->setBilltoProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_province'] ) : $arrmixValues['billto_province'] );
		}

		if( isset( $arrmixValues['billto_postal_code'] ) && $boolDirectSet ) {
			$this->m_strBilltoPostalCode = trim( stripcslashes( $arrmixValues['billto_postal_code'] ) );
		} elseif( isset( $arrmixValues['billto_postal_code'] ) ) {
			$this->setBilltoPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_postal_code'] ) : $arrmixValues['billto_postal_code'] );
		}

		if( isset( $arrmixValues['billto_country_code'] ) && $boolDirectSet ) {
			$this->m_strBilltoCountryCode = trim( stripcslashes( $arrmixValues['billto_country_code'] ) );
		} elseif( isset( $arrmixValues['billto_country_code'] ) ) {
			$this->setBilltoCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_country_code'] ) : $arrmixValues['billto_country_code'] );
		}

		if( isset( $arrmixValues['billto_phone_number'] ) && $boolDirectSet ) {
			$this->m_strBilltoPhoneNumber = trim( stripcslashes( $arrmixValues['billto_phone_number'] ) );
		} elseif( isset( $arrmixValues['billto_phone_number'] ) ) {
			$this->setBilltoPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_phone_number'] ) : $arrmixValues['billto_phone_number'] );
		}

		if( isset( $arrmixValues['billto_email_address'] ) && $boolDirectSet ) {
			$this->m_strBilltoEmailAddress = trim( stripcslashes( $arrmixValues['billto_email_address'] ) );
		} elseif( isset( $arrmixValues['billto_email_address'] ) ) {
			$this->setBilltoEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billto_email_address'] ) : $arrmixValues['billto_email_address'] );
		}

		if( isset( $arrmixValues['cc_card_number_encrypted'] ) && $boolDirectSet ) {
			$this->m_strCcCardNumberEncrypted = trim( stripcslashes( $arrmixValues['cc_card_number_encrypted'] ) );
		} elseif( isset( $arrmixValues['cc_card_number_encrypted'] ) ) {
			$this->setCcCardNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_card_number_encrypted'] ) : $arrmixValues['cc_card_number_encrypted'] );
		}

		if( isset( $arrmixValues['cc_exp_date_month'] ) && $boolDirectSet ) {
			$this->m_strCcExpDateMonth = trim( stripcslashes( $arrmixValues['cc_exp_date_month'] ) );
		} elseif( isset( $arrmixValues['cc_exp_date_month'] ) ) {
			$this->setCcExpDateMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_exp_date_month'] ) : $arrmixValues['cc_exp_date_month'] );
		}

		if( isset( $arrmixValues['cc_exp_date_year'] ) && $boolDirectSet ) {
			$this->m_strCcExpDateYear = trim( stripcslashes( $arrmixValues['cc_exp_date_year'] ) );
		} elseif( isset( $arrmixValues['cc_exp_date_year'] ) ) {
			$this->setCcExpDateYear( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_exp_date_year'] ) : $arrmixValues['cc_exp_date_year'] );
		}

		if( isset( $arrmixValues['cc_name_on_card'] ) && $boolDirectSet ) {
			$this->m_strCcNameOnCard = trim( stripcslashes( $arrmixValues['cc_name_on_card'] ) );
		} elseif( isset( $arrmixValues['cc_name_on_card'] ) ) {
			$this->setCcNameOnCard( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_name_on_card'] ) : $arrmixValues['cc_name_on_card'] );
		}

		if( isset( $arrmixValues['check_bank_name'] ) && $boolDirectSet ) {
			$this->m_strCheckBankName = trim( stripcslashes( $arrmixValues['check_bank_name'] ) );
		} elseif( isset( $arrmixValues['check_bank_name'] ) ) {
			$this->setCheckBankName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_bank_name'] ) : $arrmixValues['check_bank_name'] );
		}

		if( isset( $arrmixValues['check_name_on_account'] ) && $boolDirectSet ) {
			$this->m_strCheckNameOnAccount = trim( stripcslashes( $arrmixValues['check_name_on_account'] ) );
		} elseif( isset( $arrmixValues['check_name_on_account'] ) ) {
			$this->setCheckNameOnAccount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_name_on_account'] ) : $arrmixValues['check_name_on_account'] );
		}

		if( isset( $arrmixValues['check_account_type_id'] ) && $boolDirectSet ) {
			$this->m_intCheckAccountTypeId = trim( $arrmixValues['check_account_type_id'] );
		} elseif( isset( $arrmixValues['check_account_type_id'] ) ) {
			$this->setCheckAccountTypeId( $arrmixValues['check_account_type_id'] );
		}

		if( isset( $arrmixValues['check_routing_number'] ) && $boolDirectSet ) {
			$this->m_strCheckRoutingNumber = trim( stripcslashes( $arrmixValues['check_routing_number'] ) );
		} elseif( isset( $arrmixValues['check_routing_number'] ) ) {
			$this->setCheckRoutingNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_routing_number'] ) : $arrmixValues['check_routing_number'] );
		}

		if( isset( $arrmixValues['check_account_number_encrypted'] ) && $boolDirectSet ) {
			$this->m_strCheckAccountNumberEncrypted = trim( stripcslashes( $arrmixValues['check_account_number_encrypted'] ) );
		} elseif( isset( $arrmixValues['check_account_number_encrypted'] ) ) {
			$this->setCheckAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_account_number_encrypted'] ) : $arrmixValues['check_account_number_encrypted'] );
		}

		if( isset( $arrmixValues['secure_reference_number'] ) && $boolDirectSet ) {
			$this->m_intSecureReferenceNumber = trim( $arrmixValues['secure_reference_number'] );
		} elseif( isset( $arrmixValues['secure_reference_number'] ) ) {
			$this->setSecureReferenceNumber( $arrmixValues['secure_reference_number'] );
		}
		if( isset( $arrmixValues['is_commercial_card'] ) && $boolDirectSet ) {
			$this->m_boolIsCommercialCard = trim( $arrmixValues['is_commercial_card'] );
		} elseif( isset( $arrmixValues['is_commercial_card'] ) ) {
			$this->setIsCommercialCard( $arrmixValues['is_commercial_card'] );
		}
		if( isset( $arrmixValues['is_international_card'] ) && $boolDirectSet ) {
			$this->m_boolIsInternationalCard = trim( $arrmixValues['is_international_card'] );
		} elseif( isset( $arrmixValues['is_international_card'] ) ) {
			$this->setIsInternationalCard( $arrmixValues['is_international_card'] );
		}
		if( isset( $arrmixValues['auth_token'] ) && $boolDirectSet ) {
			$this->m_strAuthToken = trim( $arrmixValues['auth_token'] );
		} elseif( isset( $arrmixValues['auth_token'] ) ) {
			$this->setAuthToken( $arrmixValues['auth_token'] );
		}

		if( isset( $arrmixValues['mobile_number'] ) && $boolDirectSet ) {
			$this->m_intMobileNumber = trim( $arrmixValues['mobile_number'] );
		} elseif( isset( $arrmixValues['mobile_number'] ) ) {
			$this->setMobileNumber( $arrmixValues['mobile_number'] );
		}

		if( isset( $arrmixValues['payment_bank_account_id'] ) && $boolDirectSet ) {
			$this->m_strPaymentBankAccountId = trim( $arrmixValues['payment_bank_account_id'] );
		} elseif( isset( $arrmixValues['payment_bank_account_id'] ) ) {
			$this->setPaymentBankAccountId( $arrmixValues['payment_bank_account_id'] );
		}

		if( true == isset( $arrmixValues['bill_day'] ) ) $this->setBillDay( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bill_day'] ) : $arrmixValues['bill_day'] );
		if( true == isset( $arrmixValues['split_between_roommates'] ) ) $this->setSplitBetweenRoommates( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['split_between_roommates'] ) : $arrmixValues['split_between_roommates'] );
		if( true == isset( $arrmixValues['is_static_split_payment'] ) ) $this->setIsStaticSplitPayment( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_static_split_payment'] ) : $arrmixValues['is_static_split_payment'] );
		if( true == isset( $arrmixValues['split_roommate_bill_day'] ) ) $this->setSplitRoommateBillDay( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['split_roommate_bill_day'] ) : $arrmixValues['split_roommate_bill_day'] );
		if( true == isset( $arrmixValues['split_payment_amounts'] ) ) $this->setSplitPaymentAmounts( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['split_payment_amounts'] ) : $arrmixValues['split_payment_amounts'] );
		if( true == isset( $arrmixValues['split_payment_amount_rates'] ) ) $this->setSplitPaymentAmountRates( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['split_payment_amount_rates'] ) : $arrmixValues['split_payment_amount_rates'] );
		if( true == isset( $arrmixValues['specify_payment_ceiling'] ) ) $this->setSpecifyPaymentCeiling( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['specify_payment_ceiling'] ) : $arrmixValues['specify_payment_ceiling'] );
		if( true == isset( $arrmixValues['is_calculator'] ) ) $this->setIsCalculator( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_calculator'] ) : $arrmixValues['is_calculator'] );
		if( true == isset( $arrmixValues['use_stored_billing_info'] ) ) $this->setUseStoredBillingInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['use_stored_billing_info'] ) : $arrmixValues['use_stored_billing_info'] );

		if( true == isset( $arrmixValues['account_nickname'] ) ) $this->setAccountNickname( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_nickname'] ) : $arrmixValues['account_nickname'] );
		if( true == isset( $arrmixValues['billing_info_is_stored'] ) )  $this->setBillingInfoIsStored( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['billing_info_is_stored'] ) : $arrmixValues['billing_info_is_stored'] );
		if( true == isset( $arrmixValues['is_update_billing_info'] ) )  $this->setIsUpdateStoredBillingInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_update_billing_info'] ) : $arrmixValues['is_update_billing_info'] );
		if( true == isset( $arrmixValues['customer_payment_account_id'] ) ) $this->setCustomerPaymentAccountId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['customer_payment_account_id'] ) : $arrmixValues['customer_payment_account_id'] );

		if( true == isset( $arrmixValues['bi_monthly_auto_payment'] ) ) $this->setBiMonthlyAutoPayment( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_auto_payment'] ) : $arrmixValues['bi_monthly_auto_payment'] );
		if( true == isset( $arrmixValues['bi_monthly_first_bill_day'] ) ) $this->setBiMonthlyFirstBillDay( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_first_bill_day'] ) : $arrmixValues['bi_monthly_first_bill_day'] );
		if( true == isset( $arrmixValues['bi_monthly_second_bill_day'] ) ) $this->setBiMonthlySecondBillDay( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_second_bill_day'] ) : $arrmixValues['bi_monthly_second_bill_day'] );

		if( true == isset( $arrmixValues['agrees_to_terms'] ) ) $this->setAgreesToTerms( $arrmixValues['agrees_to_terms'] );
		if( true == isset( $arrmixValues['agrees_to_pay_convenience_fees'] ) ) $this->setAgreesToPayConvenienceFees( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['agrees_to_pay_convenience_fees'] ) : $arrmixValues['agrees_to_pay_convenience_fees'] );
		if( true == isset( $arrmixValues['agrees_to_duplicate_autopayment'] ) ) $this->setAgreesToDuplicateAutopayment( $arrmixValues['agrees_to_duplicate_autopayment'] );

		if( true == isset( $arrmixValues['estimated_payment_amount'] ) ) $this->setEstimatedPaymentAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['estimated_payment_amount'] ) : $arrmixValues['estimated_payment_amount'] );
		if( true == isset( $arrmixValues['estimated_payment_amount_confirmation'] ) ) $this->setEstimatedPaymentAmountConfirmation( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['estimated_payment_amount_confirmation'] ) : $arrmixValues['estimated_payment_amount_confirmation'] );

		if( true == isset( $arrmixValues['payment_amount_confirmation'] ) ) $this->setPaymentAmountConfirmation( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['payment_amount_confirmation'] ) : $arrmixValues['payment_amount_confirmation'] );
		if( true == isset( $arrmixValues['payment_ceiling_amount_confirmation'] ) ) $this->setPaymentCeilingAmountConfirmation( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['payment_ceiling_amount_confirmation'] ) : $arrmixValues['payment_ceiling_amount_confirmation'] );

		if( true == isset( $arrmixValues['agrees_to_pay_donation_amount'] ) )		$this->setAgreesToPayDonationAmount( $arrmixValues['agrees_to_pay_donation_amount'] );
		if( true == isset( $arrmixValues['apply_donation_flag'] ) )				$this->setApplyDonationFlag( $arrmixValues['apply_donation_flag'] );

		if( true == isset( $arrmixValues['split_roommate_start_month'] ) ) $this->setSplitRoommateStartMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['split_roommate_start_month'] ) : $arrmixValues['split_roommate_start_month'] );
		if( true == isset( $arrmixValues['split_roommate_end_month'] ) ) $this->setSplitRoommateEndMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['split_roommate_end_month'] ) : $arrmixValues['split_roommate_end_month'] );

		if( true == isset( $arrmixValues['bi_monthly_first_payment_amount'] ) ) $this->setBiMonthlyFirstPaymentAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_first_payment_amount'] ) : $arrmixValues['bi_monthly_first_payment_amount'] );
		if( true == isset( $arrmixValues['bi_monthly_second_payment_amount'] ) ) $this->setBiMonthlySecondPaymentAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_second_payment_amount'] ) : $arrmixValues['bi_monthly_second_payment_amount'] );

		if( true == isset( $arrmixValues['bi_monthly_first_payment_ceiling_amount'] ) ) $this->setBiMonthlyFirstPaymentCeilingAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_first_payment_ceiling_amount'] ) : $arrmixValues['bi_monthly_first_payment_ceiling_amount'] );
		if( true == isset( $arrmixValues['bi_monthly_second_payment_ceiling_amount'] ) ) $this->setBiMonthlySecondPaymentCeilingAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_second_payment_ceiling_amount'] ) : $arrmixValues['bi_monthly_second_payment_ceiling_amount'] );
		if( true == isset( $arrmixValues['bi_monthly_specify_payment_ceiling'] ) ) $this->setBiMonthlySpecifyPaymentCeiling( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_specify_payment_ceiling'] ) : $arrmixValues['bi_monthly_specify_payment_ceiling'] );

		if( true == isset( $arrmixValues['start_month'] ) ) $this->setStartMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['start_month'] ) : $arrmixValues['start_month'] );
		if( true == isset( $arrmixValues['end_month'] ) ) $this->setEndMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['end_month'] ) : $arrmixValues['end_month'] );

		if( true == isset( $arrmixValues['bi_monthly_start_month'] ) ) $this->setBiMonthlyStartMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_start_month'] ) : $arrmixValues['bi_monthly_start_month'] );
		if( true == isset( $arrmixValues['bi_monthly_end_month'] ) ) $this->setBiMonthlyEndMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bi_monthly_end_month'] ) : $arrmixValues['bi_monthly_end_month'] );

		if( true == isset( $arrmixValues['check_account_number'] ) ) $this->setCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['check_account_number'] ) : $arrmixValues['check_account_number'] );
		if( true == isset( $arrmixValues['confirm_check_account_number'] ) ) $this->setConfirmCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['confirm_check_account_number'] ) : $arrmixValues['confirm_check_account_number'] );
		if( true == isset( $arrmixValues['cc_card_number'] ) ) $this->setCcCardNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_card_number'] ) : $arrmixValues['cc_card_number'] );
		if( true == isset( $arrmixValues['cc_bin_number'] ) ) $this->setCcBinNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cc_bin_number'] ) : $arrmixValues['cc_bin_number'] );
		if( true == isset( $arrmixValues['is_floating'] ) ) $this->setIsFloating( $arrmixValues['is_floating'] );

		if( true == isset( $arrmixValues['convenience_fee_amount'] ) ) $this->setConvenienceFeeAmount( $arrmixValues['convenience_fee_amount'] );
		if( true == isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( true == isset( $arrmixValues['saving_lease_id'] ) ) $this->setSavingLeaseId( $arrmixValues['saving_lease_id'] );
		if( true == isset( $arrmixValues['is_allow_international_card'] ) ) $this->setIsAllowInternationalCard( $arrmixValues['is_allow_international_card'] );
		if( true == isset( $arrmixValues['is_paused'] ) ) $this->setIsPaused( $arrmixValues['is_paused'] );
		if( true == isset( $arrmixValues['is_debit_card'] ) ) $this->setIsDebitCard( $arrmixValues['is_debit_card'] );
		if( true == isset( $arrmixValues['is_credit_card'] ) ) $this->setIsCreditCard( $arrmixValues['is_credit_card'] );
		if( true == isset( $arrmixValues['is_check_card'] ) ) $this->setIsCheckCard( $arrmixValues['is_check_card'] );
		if( true == isset( $arrmixValues['is_prepaid_card'] ) ) $this->setIsPrepaidCard( $arrmixValues['is_prepaid_card'] );
		if( true == isset( $arrmixValues['is_gift_card'] ) ) $this->setIsGiftCard( $arrmixValues['is_gift_card'] );
		if( true == isset( $arrmixValues['account_verification_log_id'] ) ) $this->setAccountVerificationLogId( $arrmixValues['account_verification_log_id'] );
		if( true == isset( $arrmixValues['customer_preferred_locale_code'] ) ) $this->setCustomerPreferredLocaleCode( $arrmixValues['customer_preferred_locale_code'] );
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setTotalLeaseBalance( $fltTotalLeaseBalance ) {
		$this->m_fltTotalLeaseBalance = $fltTotalLeaseBalance;
	}

	public function setSavingLeaseId( $intSavingLeaseId ) {
		$this->m_intSavingLeaseId = $intSavingLeaseId;
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setCorrespondingScheduledPaymentDetailId( $intCorrespondingScheduledPaymentDetailId ) {
		$this->m_intCorrespondingScheduledPaymentDetailId = $intCorrespondingScheduledPaymentDetailId;
	}

	public function setSpecifyPaymentCeiling( $boolSpecifyPaymentCeiling ) {
		$this->m_boolSpecifyPaymentCeiling = $boolSpecifyPaymentCeiling;
	}

	public function setIsCalculator( $boolIsCalculator ) {
		$this->m_boolIsCalculator = $boolIsCalculator;
	}

	public function setUseStoredBillingInfo( $boolUseStoredBillingInfo ) {
		$this->m_boolUseStoredBillingInfo = $boolUseStoredBillingInfo;
	}

	public function setAgreesToTerms( $boolAgreesToTerms ) {
		$this->m_boolAgreesToTerms = $boolAgreesToTerms;
	}

	public function setAgreesToDuplicateAutopayment( $boolAgreesToDuplicateAutopayment ) {
		$this->m_boolAgreesToDuplicateAutopayment = $boolAgreesToDuplicateAutopayment;
	}

	public function setFeeWaived( $boolFeeWaived ) {
		parent::setFeeWaived( $boolFeeWaived );

		return;
	}

	public function setAgreesToPayConvenienceFees( $boolAgreesToPayConvenienceFees ) {
		$this->m_boolAgreesToPayConvenienceFees = $boolAgreesToPayConvenienceFees;
	}

	public function setPaymentAmountConfirmation( $fltPaymentAmountConfirmation ) {
		$this->m_fltPaymentAmountConfirmation = str_replace( '$', '', $fltPaymentAmountConfirmation );
	}

	public function setPaymentCeilingAmountConfirmation( $fltPaymentCeilingAmountConfirmation ) {
		$this->m_fltPaymentCeilingAmountConfirmation = str_replace( '$', '', $fltPaymentCeilingAmountConfirmation );
	}

	public function setBillDay( $intBillDay ) {
		$this->m_intBillDay = $intBillDay;
	}

	public function setBiMonthlyAutoPayment( $intBiMonthlyAutoPayment ) {
		$this->m_intBiMonthlyAutoPayment = $intBiMonthlyAutoPayment;
	}

	public function setBiMonthlyFirstBillDay( $intBiMonthlyFirstBillDay ) {
		$this->m_intBiMonthlyFirstBillDay = $intBiMonthlyFirstBillDay;
	}

	public function setBiMonthlySecondBillDay( $intBiMonthlySecondBillDay ) {
		$this->m_intBiMonthlySecondBillDay = $intBiMonthlySecondBillDay;
	}

	public function setBiMonthlyFirstPaymentCeilingAmount( $fltBiMonthlyFirstPaymentCeilingAmount ) {
		$this->m_fltBiMonthlyFirstPaymentCeilingAmount = CStrings::strToFloatDef( $fltBiMonthlyFirstPaymentCeilingAmount, NULL, false, 2 );
	}

	public function setBiMonthlySecondPaymentCeilingAmount( $fltBiMonthlySecondPaymentCeilingAmount ) {
		$this->m_fltBiMonthlySecondPaymentCeilingAmount = CStrings::strToFloatDef( $fltBiMonthlySecondPaymentCeilingAmount, NULL, false, 2 );
	}

	public function setBiMonthlySpecifyPaymentCeiling( $boolBiMonthlySpecifyPaymentCeiling ) {
		$this->m_boolBiMonthlySpecifyPaymentCeiling = $boolBiMonthlySpecifyPaymentCeiling;
	}

	public function setSplitBetweenRoommates( $intSplitBetweenRoommates ) {
		$this->m_intSplitBetweenRoommates = $intSplitBetweenRoommates;
	}

	public function setIsStaticSplitPayment( $boolIsStaticSplitPayment ) {
		$this->m_boolIsStaticSplitPayment = $boolIsStaticSplitPayment;
	}

	public function setSplitRoommateBillDay( $intSplitRoommateBillDay ) {
		$this->m_intSplitRoommateBillDay = $intSplitRoommateBillDay;
	}

	public function setPaymentTypes( $arrobjPaymentTypes ) {
		$this->m_arrobjPaymentTypes = $arrobjPaymentTypes;
	}

	public function setRecurringPaymentSpecials( $arrobjRecurringPaymentSpecials ) {
		$this->m_arrobjRecurringPaymentSpecials = $arrobjRecurringPaymentSpecials;
	}

	public function setRecurringPaymentDays( $arrmixRecurringPaymentDays ) {
		$this->m_arrintRecurringPaymentDays = $arrmixRecurringPaymentDays;
	}

	public function setBiMonthlyRecurringPaymentDays( $arrmixBiMonthlyRecurringPaymentDays ) {
		$this->m_arrintBiMonthlyRecurringPaymentDays = $arrmixBiMonthlyRecurringPaymentDays;
	}

	public function setSplitPaymentAmounts( $arrfltSplitPaymentAmounts ) {
		$this->m_arrfltSplitPaymentAmounts = $arrfltSplitPaymentAmounts;
	}

	public function setSplitPaymentAmountRates( $arrintSplitPaymentAmountRates ) {
		$this->m_arrintSplitPaymentAmountRates = $arrintSplitPaymentAmountRates;
	}

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setIsFloating( $boolIsFloating ) {
		$this->m_boolIsFloating = $boolIsFloating;
	}

	public function setIsDebitCard( $boolIsDebitCard ) {
		$this->m_boolIsDebitCard = $boolIsDebitCard;
	}

	public function setConvenienceFeeAmount( $fltConvenienceFeeAmount ) {
		$this->m_fltConvenienceFeeAmount = $fltConvenienceFeeAmount;
	}

	public function setEstimatedPaymentAmount( $fltEstimatedPaymentAmount ) {
		$this->m_fltEstimatedPaymentAmount = str_replace( '$', '', $fltEstimatedPaymentAmount );
	}

	public function setBiMonthlyFirstPaymentAmount( $fltBiMonthlyFirstPaymentAmount ) {
		$this->m_fltBiMonthlyFirstPaymentAmount = str_replace( '$', '', $fltBiMonthlyFirstPaymentAmount );
	}

	public function setBiMonthlySecondPaymentAmount( $fltBiMonthlySecondPaymentAmount ) {
		$this->m_fltBiMonthlySecondPaymentAmount = str_replace( '$', '', $fltBiMonthlySecondPaymentAmount );
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
		// We need to reformat phone number to get rid of everything but numbers
		$strBilltoPhoneNumber = preg_replace( '/[^a-zA-Z0-9]|\s/', '', trim( $strBilltoPhoneNumber ) );
		$strBilltoPhoneNumber = substr( $strBilltoPhoneNumber, -10 );
		$strBilltoPhoneNumber = \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 0, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 3, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 6, 10 );
		$this->m_strBilltoPhoneNumber = CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true );
	}

	public function setSplitRoommateStartMonth( $intSplitRoommateStartMonth ) {
		$this->m_intSplitRoommateStartMonth = $intSplitRoommateStartMonth;

		if( true == is_numeric( $this->m_intSplitRoommateBillDay ) && true == is_numeric( $this->m_intSplitRoommateStartMonth ) ) {
			$this->getProperBillStartOrEndDay( $this->m_intSplitRoommateStartMonth, true );
			$this->m_strPaymentStartDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', $this->m_intSplitRoommateStartMonth ), $this->m_intSplitRoommateBillDay, date( 'Y', $this->m_intSplitRoommateStartMonth ) ) );
		}
	}

	public function setSplitRoommateEndMonth( $intSplitRoommateEndMonth, $intBillDayOffset = 0 ) {
		$this->m_intSplitRoommateEndMonth = $intSplitRoommateEndMonth;

		if( true == is_numeric( $this->m_intSplitRoommateBillDay ) && true == is_numeric( $this->m_intSplitRoommateEndMonth ) ) {
			$this->getProperBillStartOrEndDay( $this->m_intSplitRoommateEndMonth, true );
			$this->m_strPaymentEndDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', $this->m_intSplitRoommateEndMonth ), $this->m_intSplitRoommateBillDay + $intBillDayOffset, date( 'Y', $this->m_intSplitRoommateEndMonth ) ) );
		} else {
			$this->m_strPaymentEndDate = NULL;
		}
	}

	public function setStartMonth( $intStartMonth ) {
		$this->m_intStartMonth = $intStartMonth;

		if( true == is_numeric( $this->m_intBillDay ) && true == is_numeric( $this->m_intStartMonth ) ) {
			// in case of months having days of 30/28/29
			$this->getProperBillStartOrEndDay( $this->m_intStartMonth, false );
			$this->m_strPaymentStartDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', $this->m_intStartMonth ), $this->m_intBillDay, date( 'Y', $this->m_intStartMonth ) ) );
		}
	}

	public function setBiMonthlyStartMonth( $intBiMonthlyStartMonth ) {
		$this->m_intBiMonthlyStartMonth = $intBiMonthlyStartMonth;

		if( true == is_numeric( $this->getBiMonthlyFirstBillDay() ) && true == is_numeric( $this->getBiMonthlyStartMonth() ) ) {

			$this->setPaymentStartDate( date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', $this->getBiMonthlyStartMonth() ), $this->getBiMonthlyFirstBillDay(), date( 'Y', $this->getBiMonthlyStartMonth() ) ) ) );
		}
	}

	public function setEndMonth( $intEndMonth, $intBillDayOffset = 0 ) {
		$this->m_intEndMonth = $intEndMonth;

		if( true == is_numeric( $this->m_intBillDay ) && true == valId( $this->m_intEndMonth ) ) {
			// in case of months having days of 30/28/29
			$this->getProperBillStartOrEndDay( $this->m_intEndMonth, false );
			$this->m_strPaymentEndDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', $this->m_intEndMonth ), $this->m_intBillDay + $intBillDayOffset, date( 'Y', $this->m_intEndMonth ) ) );
		} else {
			$this->m_strPaymentEndDate = NULL;
		}
	}

	public function setBiMonthlyEndMonth( $intBiMonthlyEndMonth, $intBillDayOffset = 0 ) {
		$this->m_intBiMonthlyEndMonth = $intBiMonthlyEndMonth;

		if( true == is_numeric( $this->m_intBiMonthlyFirstBillDay ) && true == is_numeric( $this->m_intBiMonthlyEndMonth ) ) {
			$this->m_strPaymentEndDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', $this->m_intBiMonthlyEndMonth ), $this->m_intBiMonthlyFirstBillDay + $intBillDayOffset, date( 'Y', $this->m_intBiMonthlyEndMonth ) ) );
		} else {
			$this->m_strPaymentEndDate = NULL;
		}
	}

	public function setCcCardNumber( $strPlainCcNumber ) {
		// if( true == \Psi\CStringService::singleton()->stristr( $strPlainCcNumber, 'X' ) ) return;
		$strPlainCcNumber = CStrings::strTrimDef( $strPlainCcNumber, 20, NULL, true );
		if( false == valStr( $strPlainCcNumber ) ) {
			return NULL;
		}
		$this->setCcCardNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPlainCcNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );

	}

	public function setCcBinNumber( $intCcBinNumber ) {
		$this->m_intCcBinNumber = CStrings::strToIntDef( $intCcBinNumber, NULL, false );
	}

	public function getCcBinNumber() {
		return $this->m_intCcBinNumber;
	}

	public function sqlCcBinNumber() {
		return ( true == isset( $this->m_intCcBinNumber ) ) ? ( string ) $this->m_intCcBinNumber : 'NULL';
	}

	public function setCheckAccountNumber( $strPlainCheckAccountNumber ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strPlainCheckAccountNumber, 'X' ) ) return;
		$strPlainCheckAccountNumber = CStrings::strTrimDef( $strPlainCheckAccountNumber, 20, NULL, true );
		if( false == valStr( $strPlainCheckAccountNumber ) ) {
			return NULL;
		}
		$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPlainCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

	public function setArPayments( $arrobjArPayments ) {
		$this->m_arrobjArPayments = $arrobjArPayments;
	}

	public function setIsResidentPortalPayment( $boolIsResidentPortalPayment ) {
		$this->m_boolIsResidentPortalPayment = $boolIsResidentPortalPayment;
	}

	public function setConfirmCheckAccountNumber( $strConfirmCheckAccountNumber ) {
		return $this->m_strConfirmCheckAccountNumber = $strConfirmCheckAccountNumber;
	}

	public function setIsAllowInternationalCard( $boolIsAllowInternationalCard ) {
		return $this->m_boolIsAllowInternationalCard = $boolIsAllowInternationalCard;
	}

	public function setErrorMsgs( $arrobjErrorMsgs ) {
		$this->m_arrobjErrorMsgs = $arrobjErrorMsgs;
	}

	public function setIsApprovedPostOnLastDay( $boolIsApprovedPostOnLastDay ) {
		$this->m_boolIsApprovedPostOnLastDay = $boolIsApprovedPostOnLastDay;
	}

	public function setMaximumMerchantPaymentAmount( $fltMaximunMerchantAmount ) {
		$this->m_fltMaximumMerchantPaymentAmount = $fltMaximunMerchantAmount;
	}

	public function setHasAmountExeceededMerchantLimit( $boolHasAmountExeceededMerchantLimit ) {
		$this->m_boolHasAmountExeceededMerchantLimit = $boolHasAmountExeceededMerchantLimit;
	}

	public function setIsRecentArPayment( $boolIsRecentArPayment ) {
		$this->m_boolIsRecentArPayment = $boolIsRecentArPayment;
	}

	public function setIsAutoPaymentAllowed( $boolIsAutoPaymentAllowed ) {
		$this->m_boolIsAutoPaymentAllowed = $boolIsAutoPaymentAllowed;
	}

	public function setIsPaused( $boolIsPaused ) {
		$this->m_boolIsPaused = $boolIsPaused;
	}

	public function setIsConnectedPaymentAccountExist( $boolIsConnectedPaymentAccountExist ) {
		$this->m_boolIsConnectedPaymentAccountExist = $boolIsConnectedPaymentAccountExist;
	}

	public function setPendingRoommatesCount( $intPendingRoommatesCount ) {
		$this->m_intPendingRoommatesCount = $intPendingRoommatesCount;
	}

	public function setApplyDonationFlag( $boolApplyDonationFlag ) {
		$this->m_boolApplyDonationFlag = $boolApplyDonationFlag;
	}

	public function setIsMapPostDonationData( $boolIsMapPostDonationDataToScheduledPayment ) {
		$this->m_boolIsMapPostDonationData = ( bool ) $boolIsMapPostDonationDataToScheduledPayment;
	}

	public function setAgreesToPayDonationAmount( $boolAgreesToPayDonationAmount ) {
		$this->m_boolAgreesToPayDonationAmount = $boolAgreesToPayDonationAmount;
	}

	public function setIsShowDonationAmount( $boolIsShowDonationAmount ) {
		$this->m_boolIsShowDonationAmount = $boolIsShowDonationAmount;
	}

	public function getIsShowDonationAmount() {
		return $this->m_boolIsShowDonationAmount;
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->m_strLeaseEndDate = $strLeaseEndDate;
	}

	public function setIsUpdateStoredBillingInfo( $boolIsUpdateStoredBillingInfo ) {
		$this->m_boolIsUpdateStoredBillingInfo = $boolIsUpdateStoredBillingInfo;
	}

	public function setBillingInfoIsStored( $boolBillingInfoIsStored ) {
		$this->m_boolBillingInfoIsStored = $boolBillingInfoIsStored;
	}

	public function setCustomerPaymentAccountId( $intCustomerPaymentAccountId ) {
		$this->m_intCustomerPaymentAccountId = $intCustomerPaymentAccountId;
	}

	public function setAccountNickname( $strAccountNickname ) {
		$this->m_strAccountNickname = CStrings::strTrimDef( $strAccountNickname, 10, NULL, true );
	}

	public function setAccountVerificationLogId( $intAccountVerificationLogId ) {
		$this->m_intAccountVerificationLogId = $intAccountVerificationLogId;
	}

	public function setCreatedCustomerId( $intCreatedCustomerId ) {
		$this->m_intCreatedCustomerId = $intCreatedCustomerId;
	}

	public function setUpdatedCustomerId( $intUpdatedCustomerId ) {
		$this->m_intUpdatedCustomerId = $intUpdatedCustomerId;
	}

	public function setDeletedCustomerId( $intDeletedCustomerId ) {
		$this->m_intDeletedCustomerId = $intDeletedCustomerId;
	}

	public function sqlCustomerPaymentAccountId() {
		return ( true == valId( $this->m_intCustomerPaymentAccountId ) ) ? ( string ) $this->m_intCustomerPaymentAccountId : 'NULL';
	}

	/**
	 * Create Functions
	 */

	public function createScheduledPaymentTransaction() {

		$objScheduledPaymentTransaction = new CScheduledPaymentTransaction();
		$objScheduledPaymentTransaction->setTransactionDatetime( date( 'm/d/Y H:i:s' ) );
		$objScheduledPaymentTransaction->setCid( $this->getCid() );
		$objScheduledPaymentTransaction->setScheduledPaymentId( $this->getId() );

		return $objScheduledPaymentTransaction;
	}

	public function createArPayment( $objClientDatabase, $objMerchantAccount = NULL, $boolIsProcessingPayment = false, $intCompanyUserId = 1, $boolMakeIntegrationCall = true, $boolIsFromScript = false ) {

		// Force Integration
		$objCustomer 		= $this->getOrFetchCustomer( $objClientDatabase );
		$objProperty 		= $this->getOrFetchProperty( $objClientDatabase );
		$objLease 			= $this->getOrFetchLease( $objClientDatabase );

		$intLedgerDisplayDays = NULL;
		$boolProcessThroughAMQP = false;
		if( true == $boolIsProcessingPayment ) {
			$intLedgerDisplayDays = 0;
		}

		$arrobjPropertyPreferences = $objProperty->getOrFetchPropertyPreferences( $objClientDatabase );
		if( CPsProduct::ENTRATA == $this->getPsProductId() ) {
			$boolProcessThroughAMQP = true;
		}
		// We have to force lease to integrate so we can make sure we don't process payments on accounts where settings indicate payments are not allowed (because of eviction process).
		if( true == $boolMakeIntegrationCall && true == valStr( $objProperty->getRemotePrimaryKey() ) && true == valObj( $objCustomer, 'CCustomer' ) && false == $objCustomer->forceIntegration( $intCompanyUserId, $this->getCid(), $objProperty, $objClientDatabase, $objLease, $intLedgerDisplayDays, false, NULL, $boolProcessThroughAMQP ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'integration', 'Integration failed.  Recurring payments cannot be set up at this time.', 30 ) );
			// We only return false if this payment has a variable amount.

			if( true == $boolIsFromScript ) {

				$intTimestamp = ( true == valObj( $objProperty, 'CProperty' ) ) ? $objProperty->getTimezoneSpecificTimestamp( date( 'm/d/Y H:i:s' ), $objClientDatabase ): NULL;
				if( false == is_numeric( $intTimestamp ) ) {
					$intTimestamp = time();
				}

				$intTimestamp = strtotime( '+1 day', $intTimestamp );

				$intPaymentReactivationDay = ( true == array_key_exists( 'RENT_CYCLE_BEGIN_DAY', $arrobjPropertyPreferences ) && true == valObj( $arrobjPropertyPreferences['RENT_CYCLE_BEGIN_DAY'], 'CPropertyPreference' ) && true == is_numeric( $arrobjPropertyPreferences['RENT_CYCLE_BEGIN_DAY']->getValue() ) ) ? ( int ) $arrobjPropertyPreferences['RENT_CYCLE_BEGIN_DAY']->getValue() : 25;

				if( true == valStr( $objCustomer->getPreferredLocaleCode() ) ) {
					$strPreferredLocaleCode = $objCustomer->getPreferredLocaleCode();
				} else {
					$strPreferredLocaleCode = ( true == valObj( $objProperty, 'CProperty' ) && true == valStr( $objProperty->getLocaleCode() ) ) ? $objProperty->getLocaleCode() : \CLanguage::ENGLISH;
				}

				try{
					\CLocaleContainer::createService()->setPreferredLocaleCode( $strPreferredLocaleCode, $objClientDatabase, $objProperty );

					if( true == isset( $arrobjPropertyPreferences['RENT_LATE_DAY'] ) && ( ( $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue() <= $intPaymentReactivationDay && date( 'j', $intTimestamp ) >= $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue() && date( 'j', $intTimestamp ) < $intPaymentReactivationDay )
					                                                                      || ( $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue() >= $intPaymentReactivationDay && ( date( 'j', $intTimestamp ) >= $arrobjPropertyPreferences['RENT_LATE_DAY']->getValue() || date( 'j', $intTimestamp ) < $intPaymentReactivationDay ) ) ) ) {
						$this->createWarningSystemEmailForFailedToPost( $objLease, $objProperty, $objClientDatabase );
					}

					\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
				} catch( Exception $objException ) {
					\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
				}
			}

			if( 1 == $this->m_intIsVariableAmount ) return false;
		}

		// As we have just made a integration call,
		// we should properly update customer, lease and leasecustomer object properties.
		if( true == valObj( $objCustomer, 'CCustomer' ) && false == is_null( $objCustomer->getRemotePrimaryKey() ) ) {
			$objCustomer 	= $this->fetchCustomer( $objClientDatabase );
			$objLease		= $this->fetchLease( $objClientDatabase );
		}

		// DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS
		$objPropertyPreference 	= getArrayElementByKey( 'DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS', $arrobjPropertyPreferences );
		$objLeaseCustomer 		= $this->getOrFetchLeaseCustomer( $objClientDatabase );
		$strLeaseEndDate 		= NULL;

		if( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && true == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) && true == valObj( $objLease, 'CLease' ) && true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 1 == $objPropertyPreference->getValue() ) {
			$strLeaseEndDate = ( false == is_null( $objLease->getMoveOutDate() ) ? $objLease->getMoveOutDate() : $objLease->getLeaseEndDate() );
			if( CLeaseIntervalType::MONTH_TO_MONTH == $objLease->getLeaseIntervalTypeId() && NULL == $objLease->getMoveOutDate() ) {
				$strLeaseEndDate = NULL;
			}
		}

		if( NULL != $strLeaseEndDate && 0 < strlen( $strLeaseEndDate ) && ( date( 'Y' ) > \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) || ( date( 'Y' ) == \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) && date( 'm' ) >= \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 0, 2 ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Your recurring payment could not be processed because your lease ends in the current month.', 20, [ 'lease_end' => true ] ) );
			return false;
		}

		$objArPayment = new CArPayment();
		$objArPayment->setDefaults();
		$objArPayment->setCid( $this->getCid() );
		$objArPayment->setPropertyId( $this->getPropertyId() );
		$objArPayment->setCustomerId( $this->getCustomerId() );
		$objArPayment->setCustomer( $this->getCustomer() );
		$objArPayment->setLeaseId( $this->getLeaseId() );
		$objArPayment->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
		$objArPayment->setPaymentMediumId( CPaymentMedium::RECURRING );
		$objArPayment->setScheduledPaymentId( $this->m_intId );
		$objArPayment->setPaymentDatetime( date( 'm/d/Y H:i:s' ) );
		$objArPayment->setPsProductId( $this->getPsProductId() );

		if( true == valId( $this->getCardTypeId() ) ) {
			$objArPayment->setCardTypeId( $this->getCardTypeId() );
		}

		if( CPsProduct::ENTRATA == $this->getPsProductId() ) {
			$objArPayment->setIsEntrataPayment( true );
		}

		if( \CPsProduct::RESIDENT_PORTAL == $this->getPsProductId() ) {
			$objArPayment->setIsResidentPortalPayment( true );
		}

		$objArPayment->getOrFetchPostMonth( $objClientDatabase );

		if( false == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			$objMerchantAccount	= $objArPayment->fetchDefaultMerchantAccount( $objClientDatabase );
		}

		if( true == valObj( $objMerchantAccount, 'CMerchantAccount' ) ) {
			$objArPayment->setCompanyMerchantAccountId( $objMerchantAccount->getId() );
			$objArPayment->setMerchantAccount( $objMerchantAccount );
			$intDonationAccountId = $objMerchantAccount->getDonationAccountId();
			$objArPayment->setCurrencyCode( $objMerchantAccount->getCurrencyCode() );
		}

		if( true == is_null( $this->getCustomerId() ) && true == is_null( $this->getLeaseId() ) ) {
			$objArPayment->setIsFloating( 1 );
		}

		if( 1 == $this->m_intFeeWaived ) {
			$objArPayment->setWaiveFee( true );
		}

		$this->getOrFetchProperty( $objClientDatabase );

		$objArPayment->setProperty( $this->m_objProperty );
		$objArPayment->getOrFetchMerchantAccount( $objClientDatabase );

		$objArPayment->setPaymentTypeId( $this->getPaymentTypeId() );
		if( true == valObj( $objLease, 'CLease' ) ) {
			$objArPayment->setBilltoUnitNumber( stripslashes( $objLease->getUnitNumberCache() ) );
		} else {
			$objArPayment->setBilltoUnitNumber( stripslashes( $this->getBilltoUnitNumber() ) );
		}
		$objArPayment->setBilltoAccountNumber( stripslashes( $this->getBilltoAccountNumber() ) );
		$objArPayment->setBilltoCompanyName( stripslashes( $this->getBilltoCompanyName() ) );
		$objArPayment->setBilltoNameFirst( stripslashes( $this->getBilltoNameFirst() ) );
		$objArPayment->setBilltoNameMiddle( stripslashes( $this->getBilltoNameMiddle() ) );
		$objArPayment->setBilltoNameLast( stripslashes( $this->getBilltoNameLast() ) );
		$objArPayment->setBilltoStreetLine1( stripslashes( $this->getBilltoStreetLine1() ) );
		$objArPayment->setBilltoStreetLine2( stripslashes( $this->getBilltoStreetLine2() ) );
		$objArPayment->setBilltoStreetLine3( stripslashes( $this->getBilltoStreetLine3() ) );
		$objArPayment->setBilltoCity( stripslashes( $this->getBilltoCity() ) );
		$objArPayment->setBilltoStateCode( stripslashes( $this->getBilltoStateCode() ) );
		$objArPayment->setBilltoProvince( stripslashes( $this->getBilltoProvince() ) );
		$objArPayment->setBilltoPostalCode( stripslashes( $this->getBilltoPostalCode() ) );
		$objArPayment->setBilltoCountryCode( stripslashes( $this->getBilltoCountryCode() ) );
		$objArPayment->setBilltoPhoneNumber( stripslashes( $this->getBilltoPhoneNumber() ) );

		// By default use the email address on the company customer unless it doesn't exist, then use the email on the recurring payment
		if( true == valObj( $objCustomer, 'CCustomer' ) && false == is_null( $objCustomer->getUsername() ) ) {
			$objArPayment->setBilltoEmailAddress( stripslashes( $objCustomer->getUsername() ) );
		} else {
			$objArPayment->setBilltoEmailAddress( stripslashes( $this->getBilltoEmailAddress() ) );
		}

		$objArPayment->setCcNameOnCard( stripslashes( $this->getCcNameOnCard() ) );
		$objArPayment->setCcCardNumberEncrypted( stripslashes( $this->getCcCardNumberEncrypted() ) );
		$objArPayment->setSecureReferenceNumber( stripslashes( $this->getSecureReferenceNumber() ) );
		$objArPayment->setCcExpDateMonth( stripslashes( $this->getCcExpDateMonth() ) );
		$objArPayment->setCcExpDateYear( stripslashes( $this->getCcExpDateYear() ) );
		$objArPayment->setCcBinNumber( stripslashes( $this->getCcBinNumber() ) );
		$objArPayment->setCheckBankName( stripslashes( $this->getCheckBankName() ) );
		$objArPayment->setCheckAccountTypeId( stripslashes( $this->getCheckAccountTypeId() ) );
		$objArPayment->setCheckAccountNumberEncrypted( stripslashes( $this->getCheckAccountNumberEncrypted() ) );
		$objArPayment->setCheckRoutingNumber( stripslashes( $this->getCheckRoutingNumber() ) );
		$objArPayment->setCheckNameOnAccount( stripslashes( $this->getCheckNameOnAccount() ) );
		$objArPayment->setPaymentMemo( stripslashes( $this->getPaymentMemo() ) );
		$objArPayment->setTermsAcceptedOn( $this->getTermsAcceptedOn() );

		$boolIsDebitCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsDebitCard() ) ) ? true : false;
		$boolIsCheckCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsCheckCard() ) ) ? true : false;
		$boolIsGiftCard  = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsGiftCard() ) ) ? true : false;
		$boolIsPrepaidCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsPrepaidCard() ) ) ? true : false;
		$boolIsCreditCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsCreditCard() ) ) ? true : false;
		$boolIsInternationalCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsInternationalCard() ) ) ? true : false;
		$boolIsCommercialCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsCommercialCard() ) ) ? true : false;

		$objArPayment->setIsDebitCard( $boolIsDebitCard );
		$objArPayment->setIsCheckCard( $boolIsCheckCard );
		$objArPayment->setIsGiftCard( $boolIsGiftCard );
		$objArPayment->setIsPrepaidCard( $boolIsPrepaidCard );
		$objArPayment->setIsCreditCard( $boolIsCreditCard );
		$objArPayment->setIsInternationalCard( $boolIsInternationalCard );
		$objArPayment->setIsCommercialCard( $boolIsCommercialCard );

		$objPropertyPreference 	= getArrayElementByKey( 'IS_CHARITY_ENABLED', $arrobjPropertyPreferences );

		if( false == is_null( $intDonationAccountId ) && true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 1 == $objPropertyPreference->getValue() ) {
			$objArPayment->setCompanyCharityId( stripslashes( $this->getCompanyCharityId() ) );
			$objArPayment->setDonationAmount( stripslashes( $this->getDonationAmount() ) );
		} else {
			$objArPayment->setCompanyCharityId( NULL );
			$objArPayment->setDonationAmount( 0 );

		}
		// Determine the payment amount.
		if( 1 != $this->m_intIsVariableAmount ) {
			$objArPayment->setPaymentAmount( $this->getPaymentAmount() );

		} else {

			// This is a variable payment so we need to query the lease and find out the outstanding balance.

			$objArTransactionsFilter = new CArTransactionsFilter();
			// Resident Portal Settings Will Result In the Proper Data Returning
			$objArTransactionsFilter->setDefaults( $boolIsResidentPortal = true );

			if( true === valObj( $this->m_objLease, 'CLease' ) ) {

				$this->m_objLease->getRepaymentDetails( $objClientDatabase );
				$objPropertyGlSetting = $objArPayment->getPropertyGlSetting();

				if( true === valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
					$this->m_objLease->setPropertyGlSetting( $objPropertyGlSetting );
				}

				if( CScheduledPaymentType::SPLIT == $this->getScheduledPaymentTypeId() ) {
					$fltCachedBalance 	= json_decode( $this->getCachedBalance() )->cachedBalance;
					$fltBalance			= ( true == valId( $fltCachedBalance ) ) ? $fltCachedBalance : 0;
				} else {
					$this->m_objLease->fetchAndRollbackCustomerDisplayArTransactions( $objArTransactionsFilter, NULL, NULL, SYSTEM_USER_ID, $objClientDatabase );
					$fltBalance = $this->m_objLease->getBalance();
				}

				$fltRepaymentBalance = $this->m_objLease->initializeLeaseRePaymentBalance( $objClientDatabase );
				$fltBalance += $fltRepaymentBalance;

				if( true == valArr( $this->m_objLease->getArTransactions() ) ) {
					foreach( $this->m_objLease->getArTransactions() as $objArTransaction ) {
						if( CArCodeType::RENT == $objArTransaction->getArCodeTypeId() ) {
							$objArPayment->setRentChargeDue( 1 );
							break;
						}
					}
				}
			}
			$objArPayment->setPaymentAmount( $fltBalance );

			$fltCeilingAmount = ( float ) $this->getPaymentCeilingAmount();

			if( 0 < $this->getForceReprocess() ) {
				$fltCeilingAmount = $this->fetchPaymentCeilingAmount( $objClientDatabase );
			}

			if( CScheduledPaymentType::SPLIT == $this->getScheduledPaymentTypeId() ) {
				$fltBalance = ( float ) ( $fltBalance * $this->getPaymentAmountRate() ) / 100;
			}

			if( true == is_null( $this->getPaymentAmountRate() ) && 0 < $this->getPaymentCeilingAmount() && $fltBalance > $fltCeilingAmount ) {

				if( true == $boolIsProcessingPayment ) {
					$objPropertyPreference 	= getArrayElementByKey( 'REJECT_PARTIAL_RECURRING_PAYMENT', $arrobjPropertyPreferences );

					if( false == is_null( $this->getId() ) && true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 1 == $objPropertyPreference->getValue() ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Your recurring payment could not be processed because your balance ($' . $fltBalance . ')  exceeds the limit you applied to your recurring payment ($' . $fltCeilingAmount . '). Partial payments are not accepted.', 10 ) );
						return false;
					}
				}

				$objArPayment->setPaymentAmount( $fltCeilingAmount );
			}
		}

		return $objArPayment;
	}

	public function createScheduledPaymentDetail( $intCurrentUserId, $objClientDatabase ) {

		$objScheduledPaymentDetail = new CScheduledPaymentDetail();

		$objScheduledPaymentDetail->setCid( $this->getCid() );
		$objScheduledPaymentDetail->setScheduledPaymentId( $this->getId() );
		$objScheduledPaymentDetail->setBilltoIpAddress( $this->getBilltoIpAddress() );
		$objScheduledPaymentDetail->setBilltoAccountNumber( $this->getBilltoAccountNumber() );
		$objScheduledPaymentDetail->setBilltoCompanyName( $this->getBilltoCompanyName() );
		$objScheduledPaymentDetail->setBilltoStreetLine1( $this->getBilltoStreetLine1() );
		$objScheduledPaymentDetail->setBilltoStreetLine2( $this->getBilltoStreetLine2() );
		$objScheduledPaymentDetail->setBilltoStreetLine3( $this->getBilltoStreetLine3() );
		$objScheduledPaymentDetail->setBilltoCity( $this->getBilltoCity() );
		$objScheduledPaymentDetail->setBilltoStateCode( $this->getBilltoStateCode() );
		$objScheduledPaymentDetail->setBilltoProvince( $this->getBilltoProvince() );
		$objScheduledPaymentDetail->setBilltoPostalCode( $this->getBilltoPostalCode() );
		$objScheduledPaymentDetail->setBilltoCountryCode( $this->getBilltoCountryCode() );
		$objScheduledPaymentDetail->setBilltoPhoneNumber( $this->getBilltoPhoneNumber() );
		$objScheduledPaymentDetail->setBilltoEmailAddress( $this->getBilltoEmailAddress() );

		$objScheduledPaymentDetail->setCcNameOnCard( stripslashes( $this->getCcNameOnCard() ) );
		$objScheduledPaymentDetail->setCcCardNumberEncrypted( stripslashes( $this->getCcCardNumberEncrypted() ) );
		$objScheduledPaymentDetail->setCcBinNumber( stripslashes( $this->getCcBinNumber() ) );
		$objScheduledPaymentDetail->setSecureReferenceNumber( stripslashes( $this->getSecureReferenceNumber() ) );
		$objScheduledPaymentDetail->setCcExpDateMonth( stripslashes( $this->getCcExpDateMonth() ) );
		$objScheduledPaymentDetail->setCcExpDateYear( stripslashes( $this->getCcExpDateYear() ) );

		$objScheduledPaymentDetail->setCheckBankName( stripslashes( $this->getCheckBankName() ) );
		$objScheduledPaymentDetail->setCheckAccountTypeId( stripslashes( $this->getCheckAccountTypeId() ) );
		$objScheduledPaymentDetail->setCheckAccountNumberEncrypted( stripslashes( $this->getCheckAccountNumberEncrypted() ) );
		$objScheduledPaymentDetail->setCheckRoutingNumber( stripslashes( $this->getCheckRoutingNumber() ) );
		$objScheduledPaymentDetail->setCheckNameOnAccount( stripslashes( $this->getCheckNameOnAccount() ) );
		$objScheduledPaymentDetail->setAccountVerificationLogId( $this->getAccountVerificationLogId() );
		$objScheduledPaymentDetail->setIsDebitCard( $this->getIsDebitCard() );
		$objScheduledPaymentDetail->setIsCheckCard( $this->getIsCheckCard() );
		$objScheduledPaymentDetail->setIsGiftCard( $this->getIsGiftCard() );
		$objScheduledPaymentDetail->setIsPrepaidCard( $this->getIsPrepaidCard() );
		$objScheduledPaymentDetail->setIsCreditCard( $this->getIsCreditCard() );
		$objScheduledPaymentDetail->setIsInternationalCard( $this->getIsInternationalCard() );
		$objScheduledPaymentDetail->setIsCommercialCard( $this->getIsCommercialCard() );
		$objScheduledPaymentDetail->setPaymentBankAccountId( $this->getPaymentBankAccountId() );

		if( true == valId( $this->getSecureReferenceNumber() ) ) {
			$arrmixBinFlags = \Psi\Core\Payment\CPaymentBinFlags::createService()->getAdditionalBinFlags( $this->getSecureReferenceNumber() );

			if( true == valArr( $arrmixBinFlags ) ) {
				$arrstrDetails = json_decode( json_encode( $objScheduledPaymentDetail->getDetails() ), true );

				$arrstrDetails['bin_flags'] = $arrmixBinFlags;

				$objScheduledPaymentDetail->setDetails( json_encode( $arrstrDetails ) );
			}

			// update bin flags in customer_payment_account
			$arrboolScheduledPaymentBinFlags['is_debit_card']           = $this->getIsDebitCard();
			$arrboolScheduledPaymentBinFlags['is_credit_card']          = $this->getIsCreditCard();
			$arrboolScheduledPaymentBinFlags['is_check_card']           = $this->getIsCheckCard();
			$arrboolScheduledPaymentBinFlags['is_gift_card']            = $this->getIsGiftCard();
			$arrboolScheduledPaymentBinFlags['is_prepaid_card']         = $this->getIsPrepaidCard();
			$arrboolScheduledPaymentBinFlags['is_international_card']   = $this->getIsInternationalCard();
			$arrboolScheduledPaymentBinFlags['is_commercial_card']      = $this->getIsCommercialCard();

			CCustomerPaymentAccount::updateCustomerPaymentAccountBinFlagsAndDetails( $this->getCustomerPaymentAccountId(), $this->getCid(), $intCurrentUserId, $objClientDatabase, $arrmixBinFlags, $arrboolScheduledPaymentBinFlags );

		}

		if( true == valId( $this->getCreatedCustomerId() ) ) {
			$arrstrDetails = json_decode( json_encode( $objScheduledPaymentDetail->getDetails() ), true );
			$arrstrDetails['created_customer_id'] = $this->getCreatedCustomerId();
			$objScheduledPaymentDetail->setDetails( json_encode( $arrstrDetails ) );
		}

		return $objScheduledPaymentDetail;
	}

	public function createRecurringPaymentSpecialsConccesions( $intCurrentUserId, $objClientDatabase ) {

		$this->m_arrobjRecurringPaymentSpecials 			= [];
		$arrobjRecurringScheduledSpecialsConccessionCharges	= [];

		$arrintLeaseStatuses = [ CLeaseStatusType::APPLICANT, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ];

		$this->m_objLease		= $this->getOrFetchLease( $objClientDatabase );
		$this->m_objProperty	= $this->getOrFetchProperty( $objClientDatabase );

		if( false == valObj( $this->m_objLease, 'CLease' ) || false == valObj( $this->m_objProperty, 'CProperty' ) ) return true;

		$arrobjPropertySpecials  = $this->m_objProperty->fetchRecurringPaymentSpecials( $objClientDatabase );

		if( false == valArr( $arrobjPropertySpecials ) ) return true;

		$arrobjScheduledPayments	= $this->m_objLease->fetchAllScheduledPayments( $objClientDatabase );
		$objLeaseCustomer			= $this->fetchLeaseCustomer( $objClientDatabase );

		if( true == isset( $objLeaseCustomer ) && false == in_array( $objLeaseCustomer->getLeaseStatusTypeId(), $arrintLeaseStatuses ) ) return true;
		if( false == is_null( $this->m_objLease->getLeaseEndDate() ) && strtotime( $this->m_objLease->getLeaseEndDate() ) < strtotime( date( 'Y-m-d' ) ) ) return true;

		$strCurrentMonthLastDateTime = date( 'm' ) . '/' . date( 't', strtotime( 'today' ) ) . '/' . date( 'Y' );

		if( false == is_null( $strCurrentMonthLastDateTime ) && false == is_null( $this->m_objLease->getLeaseEndDate() ) && strtotime( $this->m_objLease->getLeaseEndDate() ) <= strtotime( $strCurrentMonthLastDateTime ) ) return true;

		$strLeaseMoveInDateTime = strtotime( $this->m_objLease->getMoveInDate() );

		$arrintArCodeIds = [];

		foreach( $arrobjPropertySpecials as $objPropertySpecial ) {
			if( true == is_numeric( $objPropertySpecial->getArCodeId() ) ) {
				$arrintArCodeIds[$objPropertySpecial->getArCodeId()] = $objPropertySpecial->getArCodeId();
			}
		}

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setArCodeIds( $arrintArCodeIds );
		$arrobjArCodes = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->m_objProperty->getCid(), $objArCodesFilter, $objClientDatabase );

		foreach( $arrobjPropertySpecials as $objPropertySpecial ) {

			if( false == is_null( $objPropertySpecial->getRecurringPaymentDayRestriction() ) && 0 < $objPropertySpecial->getRecurringPaymentDayRestriction() ) {
				$intDaysWithinMovin = $objPropertySpecial->getRecurringPaymentDayRestriction();
			} else {
				$intDaysWithinMovin = 0;
			}

			if( ( $strLeaseMoveInDateTime + 3600 * 24 * $intDaysWithinMovin ) < strtotime( date( 'Y-m-d' ) ) || ( false == is_null( $this->m_objLease->getLeaseEndDate() ) && strtotime( $this->m_objLease->getLeaseEndDate() ) < strtotime( date( 'Y-m-d' ) ) ) ) {
				continue;
			}

			$boolIsPaymentExist = true;

			if( true == valArr( $arrobjScheduledPayments ) ) {
				foreach( $arrobjScheduledPayments as  $objScheduledPayment ) {

					if( $this->getId() != $objScheduledPayment->getId()
					    && strtotime( $objScheduledPayment->getCreatedOn() ) >= strtotime( $objPropertySpecial->getStartDate() )
					    && ( strtotime( $objScheduledPayment->getCreatedOn() ) <= strtotime( $objPropertySpecial->getEndDate() )
					         || true == is_null( $objPropertySpecial->getEndDate() ) ) ) {

						$boolIsPaymentExist &= false;
					}
				}
			}

			if( true == $boolIsPaymentExist && 0 != strlen( $objPropertySpecial->getRateAmount() ) && 0 < $objPropertySpecial->getRateAmount() ) {

				$this->m_arrobjRecurringPaymentSpecials[$objPropertySpecial->getId()] = $objPropertySpecial;

				$objRecurringPaymentSpecialsTransaction = $this->m_objLease->createArTransaction( $objPropertySpecial->getArCodeId(), $objClientDatabase );

				$objArCode = getArrayElementByKey( $objPropertySpecial->getArCodeId(), $arrobjArCodes );

				if( true == valObj( $objArCode, 'CArCode' ) ) {
					$objRecurringPaymentSpecialsTransaction->setArCodeTypeId( $objArCode->getArCodeTypeId() );
				}

				$objRecurringPaymentSpecialsTransaction->setTransactionAmount( $objPropertySpecial->getRateAmount() * -1 );

				if( false == $objRecurringPaymentSpecialsTransaction->postCharge( $intCurrentUserId, $objClientDatabase ) ) {
					$this->addErrorMsgs( new CErrorMsg( NULL, NULL, 'One time recurring payment specials failed to insert.' ) );
					trigger_error( 'One time recurring payment special failed to insert.', E_USER_WARNING );
				}

				$arrobjRecurringScheduledSpecialsConccessionCharges[$objRecurringPaymentSpecialsTransaction->getId()] = $objRecurringPaymentSpecialsTransaction;
			}
		}

		return $arrobjRecurringScheduledSpecialsConccessionCharges;
	}

	public function exportRecurringPaymentSpecialsConccesions( $arrobjArTransactions, $intCurrentUserId, $objClientDatabase, $boolQueueSyncOverride = false ) {

		$objLease = $this->fetchLease( $objClientDatabase );

		if( false == isset( $objLease ) ) return true;
		if( true == is_null( $objLease->getRemotePrimaryKey() ) || 0 == strlen( $objLease->getRemotePrimaryKey() ) ) return true;

		if( true == valArr( $arrobjArTransactions ) ) {
			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::SEND_AR_TRANSACTIONS, $intCurrentUserId, $objClientDatabase );
			$objGenericWorker->setProperty( $this->fetchProperty( $objClientDatabase ) );
			$objGenericWorker->setLease( $objLease );
			$objGenericWorker->setCustomer( $this->getOrFetchCustomer( $objClientDatabase ) );
			$objGenericWorker->setArTransactions( $arrobjArTransactions );
			$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

			return $objGenericWorker->process();

		} else {
			return false;
		}
	}

	/**
	 * Get or Fetch Functions
	 */

	public function getOrFetchCorrespondingScheduledPaymentDetailId( $objClientDatabase ) {
		if( true == is_numeric( $this->m_intCorrespondingScheduledPaymentDetailId ) ) {
			return $this->m_intCorrespondingScheduledPaymentDetailId;
		} else {
			$objScheduledPaymentDetail = $this->fetchScheduledPaymentDetail( $objClientDatabase );

			if( true == valObj( $objScheduledPaymentDetail, 'CScheduledPaymentDetail' ) ) {
				$this->m_intCorrespondingScheduledPaymentDetailId = $objScheduledPaymentDetail->getId();
				return $objScheduledPaymentDetail->getId();
			}
		}
	}

	public function getOrFetchCustomer( $objClientDatabase ) {
		if( true == valObj( $this->m_objCustomer, 'CCustomer' ) ) {
			return $this->m_objCustomer;
		} else {
			return $this->fetchCustomer( $objClientDatabase );
		}
	}

	public function getOrFetchProperty( $objClientDatabase ) {

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		} else {
			return $this->fetchProperty( $objClientDatabase );
		}
	}

	public function getOrFetchLease( $objClientDatabase ) {

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {
			return $this->m_objLease;
		} else {
			return $this->fetchLease( $objClientDatabase );
		}
	}

	public function getOrFetchLeaseCustomer( $objClientDatabase ) {

		if( true == valObj( $this->m_objLeaseCustomer, 'CLeaseCustomer' ) ) {
			return $this->m_objLeaseCustomer;
		} else {
			return $this->fetchLeaseCustomer( $objClientDatabase );
		}
	}

	public function getOrFetchPaymentTypes( $objClientDatabase, $objMerchantAccount = NULL, $boolIsPaymentPaused = false ) {

		if( true == valArr( $this->m_arrobjPaymentTypes ) ) {
			return $this->m_arrobjPaymentTypes;
		} else {
			return $this->fetchPaymentTypes( $objClientDatabase, $objMerchantAccount, $boolIsPaymentPaused );
		}
	}

	public function getOrFetchClient( $objClientDatabase ) {

		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient;
		} else {
			return $this->fetchClient( $objClientDatabase );
		}
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPaymentCeilingAmount( $objClientDatabase ) {

		$fltCeilingAmount = ( float ) $this->getPaymentCeilingAmount();

		if( 0 < $fltCeilingAmount && 0 < $this->getForceReprocess() ) {
			$strSql = 'SELECT SUM(payment_amount) as payment_amount FROM ar_payments WHERE scheduled_payment_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . ' AND created_on > ( NOW() - INTERVAL \'5 days\' ) AND payment_status_type_id = ' . CPaymentStatusType::CAPTURED;

			$arrstrArPaymentId = fetchData( $strSql, $objClientDatabase );

			if( true == isset( $arrstrArPaymentId[0]['payment_amount'] ) && 0 < ( float ) $arrstrArPaymentId[0]['payment_amount'] ) {
				$fltCeilingAmount = $fltCeilingAmount - $arrstrArPaymentId[0]['payment_amount'];
			}
		}

		return $fltCeilingAmount;
	}

	public function fetchScheduledPaymentDetail( $objClientDatabase ) {
		$this->m_objScheduledPaymentDetail = CScheduledPaymentDetails::fetchScheduledPaymentDetailByScheduledPaymentIdByCid( $this->m_intId, $this->getCid(), $objClientDatabase );
		return $this->m_objScheduledPaymentDetail;
	}

	public function fetchClient( $objClientDatabase ) {
		return CClients::fetchClientById( $this->m_intCid, $objClientDatabase );
	}

	public function fetchProperty( $objClientDatabase ) {

		$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->m_intCid, $objClientDatabase );
		return $this->m_objProperty;
	}

	public function fetchCustomer( $objClientDatabase ) {
		$this->m_objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomerByIdByCid( $this->m_intCustomerId, $this->m_intCid, $objClientDatabase );
		return $this->m_objCustomer;
	}

	public function fetchLeaseCustomer( $objClientDatabase ) {
		$this->m_objLeaseCustomer = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchCustomLeaseCustomerByCustomerIdByLeaseIdByCid( $this->m_intCustomerId, $this->m_intLeaseId, $this->getCid(), $objClientDatabase );
		return $this->m_objLeaseCustomer;
	}

	public function fetchLease( $objClientDatabase ) {
		$this->m_objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchCustomLeaseByIdByCid( $this->m_intLeaseId, $this->m_intCid, $objClientDatabase );
		return $this->m_objLease;
	}

	public function fetchSavingLease( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchCustomLeaseByIdByCid( $this->m_intSavingLeaseId, $this->m_intCid, $objClientDatabase );
	}

	public function fetchPaymentTypes( $objClientDatabase, $objMerchantAccount = NULL, $boolIsPaymentPaused = false ) {

		$objArPayment = $this->createArPayment( $objClientDatabase, $objMerchantAccount, false );

		if( false == valObj( $objArPayment, 'CArPayment' ) ) {
			return [];
		}

		$objArPayment->setIsResidentPortalPayment( $this->m_boolIsResidentPortalPayment );
		$objArPayment->setScheduledPayment( $this );
		$objArPayment->setPaymentTypeId( $this->getPaymentTypeId() );
		$objArPayment->setSecureReferenceNumber( $this->getSecureReferenceNumber() );
		$objArPayment->setCustomerPaymentAccountId( $this->getCustomerPaymentAccountId() );

		if( false == valObj( $objArPayment->getCustomer(), 'CCustomer' ) ) {
			$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );
		} else {
			$objCustomer = $objArPayment->getCustomer();
		}
		$objCustomer->fetchCustomerPaymentAccounts( $objClientDatabase );

		if( true == $this->getSplitBetweenRoommates() ) {
			$fltScheduledChargesTotal = 0;
			$arrfltSplitPaymentAmounts = $this->getSplitPaymentAmounts();
			$arrintSplitPaymentAmountRates = $this->getSplitPaymentAmountRates();

			$arrmixSplitPaymentAmountOrRates	= ( array ) ( 1 == $this->getIsVariableAmount() ? $arrintSplitPaymentAmountRates : $arrfltSplitPaymentAmounts );
			if( 1 == $this->getIsVariableAmount() ) {
				$fltScheduledChargesTotal = $this->getLease()->fetchScheduledChargeTotal( $objClientDatabase, CArTrigger::$c_arrintRecurringArTriggers );
			}
			if( false == $arrmixSplitPaymentAmountOrRates ) {
				$arrfltSplitPaymentInfo = CScheduledPayments::fetchSplitPaymentAmountInfoByCustomerIdLeaseIdCid( $this->m_objCustomer->getId(), $this->getLease()->getId(), $this->m_intCid, $objClientDatabase );
				$this->setIsVariableAmount( $arrfltSplitPaymentInfo[0]['is_variable_amount'] );
				$fltPaymentAmount	= ( 1 == $this->getIsVariableAmount() ? ( $fltScheduledChargesTotal * $arrfltSplitPaymentInfo[0]['payment_amount_rate'] ) / 100 : $arrfltSplitPaymentInfo[0]['payment_amount'] );
			} else {
				$fltPaymentAmount	= ( 1 == $this->getIsVariableAmount() ? ( $fltScheduledChargesTotal * $arrintSplitPaymentAmountRates[$this->m_objCustomer->getId()] ) / 100 : $arrfltSplitPaymentAmounts[$this->m_objCustomer->getId()] );
			}

			$objArPayment->setPaymentAmount( $fltPaymentAmount );
		} elseif( true == $this->m_intIsVariableAmount ) {
			if( true == $this->m_boolSpecifyPaymentCeiling ) {
				$objArPayment->setPaymentAmount( $this->m_fltPaymentCeilingAmount );
			} else {
				$objArPayment->setPaymentAmount( $this->m_fltEstimatedPaymentAmount );
			}

			if( true == $this->m_boolIsCalculator ) {
				$objArPayment->setPaymentAmount( $this->m_fltEstimatedPaymentAmount );
			}
		} else {
			$objArPayment->setPaymentAmount( $this->m_fltPaymentAmount );
			if( true == $this->m_boolIsCalculator ) {
				$objArPayment->setPaymentAmount( $this->m_fltEstimatedPaymentAmount );
			}
		}
		if( true == $boolIsPaymentPaused ) {
			$objArPayment->setPaymentAmount( $this->getPaymentAmount() );
		}

		$objArPayment->setCcBinNumber( $this->getCcBinNumber() );

		$this->m_arrobjPaymentTypes = ( array ) CPaymentController::fetchAcceptedPaymentTypes( $objArPayment, $objClientDatabase );
		$this->m_arrobjPaymentTypes = CAcceptedPaymentTypesService::createService()->fetchAcceptedPaymentTypesComparison( $this->m_arrobjPaymentTypes, CPaymentController::getPaymentTypeAvailabilityMessages(), $objArPayment, $objClientDatabase );

		$this->setIsDebitCard( $objArPayment->getIsDebitCard() );
		$this->setIsCheckCard( $objArPayment->getIsCheckCard() );
		$this->setIsGiftCard( $objArPayment->getIsGiftCard() );
		$this->setIsPrepaidCard( $objArPayment->getIsPrepaidCard() );
		$this->setIsCreditCard( $objArPayment->getIsCreditCard() );
		$this->setIsInternationalCard( $objArPayment->getIsInternationalCard() );
		$this->setIsCommercialCard( $objArPayment->getIsCommercialCard() );

		// Remove all non-electronic payment types
		$this->m_arrobjPaymentTypes = array_intersect_key( $this->m_arrobjPaymentTypes, CPaymentTypes::fetchElectronicPaymentTypes() );

		if( true == is_null( $objArPayment->getConvenienceFeeAmount() ) ) {
			$this->setConvenienceFeeAmount( 0 );
		} else {
			$this->setConvenienceFeeAmount( $objArPayment->getConvenienceFeeAmount() );
		}

		$this->m_objMerchantAccount = $objArPayment->getOrFetchMerchantAccount( $objClientDatabase );

		return $this->m_arrobjPaymentTypes;
	}

	public function fetchDuplicateVariableRecurringPayment( $objClientDatabase ) {
		return CScheduledPayments::fetchVariableRecurringPaymentByLeaseIdByDateByCid( $this->getLeaseId(), $this->getPaymentStartDate(), $this->getCid(), $objClientDatabase );
	}

	public function fetchState( $objClientDatabase ) {

		return \Psi\Eos\Entrata\CStates::createService()->fetchStateByCode( $this->m_strBilltoStateCode, $objClientDatabase );
	}

	public function fetchPaymentType( $objClientDatabase ) {

		return CPaymentTypes::fetchPaymentTypeById( $this->m_intPaymentTypeId, $objClientDatabase );
	}

	public function fetchCheckAccountType( $objClientDatabase ) {

		return \Psi\Eos\Entrata\CCheckAccountTypes::createService()->fetchCheckAccountTypeById( $this->m_intCheckAccountTypeId, $objClientDatabase );
	}

	public function fetchScheduledPaymentTransactions( $objClientDatabase ) {

		return CScheduledPaymentTransactions::fetchCustomScheduledPaymentTransactionsByScheduledPaymentIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function validateResidentWorksScheduledPayment( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		$objScheduledPaymentValidator = new CScheduledPaymentValidator();
		$objScheduledPaymentValidator->setScheduledPayment( $this );
		$boolIsValid &= $objScheduledPaymentValidator->validateResidentWorksScheduledPayment( $strAction, $objClientDatabase );

		return $boolIsValid;
	}

	public function validateResidentPortalScheduledPayment( $strAction, $objClientDatabase = NULL, $boolIsNotValidateBillingInfo = false, $boolIsReCreate = false ) {
		$boolIsValid = true;

		$objScheduledPaymentValidator = new CScheduledPaymentValidator();
		$objScheduledPaymentValidator->setScheduledPayment( $this );
		$boolIsValid &= $objScheduledPaymentValidator->validateResidentPortalScheduledPayment( $strAction, $objClientDatabase, $boolIsNotValidateBillingInfo, $boolIsReCreate );

		return $boolIsValid;
	}

	public function validatePastResidentScheduledPayment( $objLease, $arrobjPropertyPreferences ) {
		$boolIsValid = false;

		if( false == valObj( $objLease, 'CLease' ) || false == valArr( $arrobjPropertyPreferences ) ) {
			return $boolIsValid;
		}

		$intPastResidentToleranceDays = ( false == isset( $arrobjPropertyPreferences['PAST_RESIDENT_LOGIN_TOLERANCE_DAYS'] ) ) ? 60 : $arrobjPropertyPreferences['PAST_RESIDENT_LOGIN_TOLERANCE_DAYS']->getValue();
		if( true == isset( $arrobjPropertyPreferences['ALLOW_PAST_RESIDENT_SCHEDULED_PAYMENT'] ) && 0 < $intPastResidentToleranceDays ) {

			$intUnixTimeStampCompareDate = ( false == is_null( $objLease->getMoveOutDate() ) ) ? strtotime( '-1 day', strtotime( $objLease->getMoveOutdate() ) ) : strtotime( '-1 day', strtotime( $objLease->getLeaseEndDate() ) );
			$intUnixTimeStampCompareDate += ( $intPastResidentToleranceDays * 60 * 60 * 24 );
			$boolIsValid 				 = ( $intUnixTimeStampCompareDate > time() ) ? true : false;
		}

		return $boolIsValid;
	}

	public function valSplitRoommatesTotalAmount( $fltTotalLeaseBalance, $arrobjPropertyPreferences ) {
		$boolIsValidPercentsOrAmounts = true;
		if( false == $this->getSplitBetweenRoommates() || false == valArr( $arrobjPropertyPreferences ) || true == is_null( $fltTotalLeaseBalance ) ) {
			return $boolIsValidPercentsOrAmounts;
		}

		if( false == isset( $arrobjPropertyPreferences['REJECT_PARTIAL_RECURRING_PAYMENT'] )
		    && ( self::STATIC_RECURR_PAYMENTS == getArrayElementByKey( 'ALLOW_STATIC_AND_VARIABLE_RECURRING_PAYMENTS', $arrobjPropertyPreferences )
		         || ( self::STATIC_AND_VARIABLE_RECURR_PAYMENTS == getArrayElementByKey( 'ALLOW_STATIC_AND_VARIABLE_RECURRING_PAYMENTS', $arrobjPropertyPreferences ) && false == $this->getIsVariableAmount() ) ) ) {
			return $boolIsValidPercentsOrAmounts;
		}

		$mixTotalPercentsOrAmounts		= ( true == $this->getIsVariableAmount() ? array_sum( $this->getSplitPaymentAmountRates() ) : array_sum( $this->getSplitPaymentAmounts() ) );
		$boolIsValidPercentsOrAmounts	= ( true == $this->getIsVariableAmount() ? 100 == $mixTotalPercentsOrAmounts : $mixTotalPercentsOrAmounts >= $fltTotalLeaseBalance );

		return $boolIsValidPercentsOrAmounts;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolIsLeaseRenewed = false, $boolIsNewUpdate = false, $boolIsSplitAdjustSetup = false ) {
		$boolIsValid = true;

		$objScheduledPaymentValidator = new CScheduledPaymentValidator();
		$objScheduledPaymentValidator->setScheduledPayment( $this );
		$boolIsValid &= $objScheduledPaymentValidator->validate( $strAction, $objClientDatabase, $boolIsLeaseRenewed, $boolIsNewUpdate, $boolIsSplitAdjustSetup );

		return $boolIsValid;
	}

	/**
	 * Add Functions
	 */

	public function addErrorMsgs( $arrobjErrorMsgs ) {
		if( true == valArr( $arrobjErrorMsgs ) ) {
			foreach( $arrobjErrorMsgs as $objErrorMsg ) {
				if( true == valObj( $objErrorMsg, 'CErrorMsg' ) ) {
					$this->addErrorMsg( $objErrorMsg );
				}
			}
		}
	}

	/**
	 * Other Functions
	 */

	// Because we need the scheduled payment posting system to be efficient, we will not query actual ar payment objects which takes more time.
	// Instead we will do a simple query to get ID only.  If we find an ID that is_numeric, we will return true;

	public function determineAlreadyPosted( $objClientDatabase ) {
		if( 1 == $this->getIsVariableAmount() && 0 < $this->getForceReprocess() ) {
			return false;
		}

		$strSql = 'SELECT id FROM ar_payments WHERE scheduled_payment_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . ' AND created_on > ( NOW() - INTERVAL \'5 days\' )';
		if( true == $this->getIsPlaidAccount() ) {
			$strSql .= ' AND payment_status_type_id <> ' . ( int ) CPaymentStatusType::DECLINED;
		}

		$arrstrArPaymentId = fetchData( $strSql, $objClientDatabase );

		if( true == isset( $arrstrArPaymentId[0]['id'] ) && true == is_numeric( $arrstrArPaymentId[0]['id'] ) ) {
			return true;
		}

		return false;
	}

	public function emptyObjectContainers() {
		unset( $this->m_objCustomer );
		unset( $this->m_objLease );
		unset( $this->m_objProperty );
	}

	public function loadCustomerData( $objClientDatabase ) {

		if( true == \Psi\Libraries\UtilFunctions\valObj( $this->m_objCustomer, \CCustomer::class ) ) {
			$strCustomerPhoneNumber = $this->m_objCustomer->getAvailableCustomerPhoneNumber( $objClientDatabase );
			if( false == isset( $this->m_strBilltoEmailAddress ) ) 		$this->setBilltoEmailAddress( $this->m_objCustomer->getUsername() );
			if( false == isset( $this->m_strBilltoNameFirst ) ) 		$this->setBilltoNameFirst( $this->m_objCustomer->getNameFirst() );
			if( false == isset( $this->m_strBilltoNameLast ) ) 			$this->setBilltoNameLast( $this->m_objCustomer->getNameLast() );
			if( false == isset( $this->m_strBilltoStreetLine1 ) ) 		$this->setBilltoStreetLine1( $this->m_objCustomer->getPrimaryStreetLine1() );
			if( false == isset( $this->m_strBilltoStreetLine2 ) ) 		$this->setBilltoStreetLine2( $this->m_objCustomer->getPrimaryStreetLine2() );
			if( false == isset( $this->m_strBilltoCity ) ) 				$this->setBilltoCity( $this->m_objCustomer->getPrimaryCity() );
			if( false == isset( $this->m_strBilltoStateCode ) )			$this->setBilltoStateCode( $this->m_objCustomer->getPrimaryStateCode() );
			if( false == isset( $this->m_strBilltoPostalCode ) ) 		$this->setBilltoPostalCode( $this->m_objCustomer->getPrimaryPostalCode() );
			if( false == isset( $this->m_strBilltoPhoneNumber ) ) 		$this->setBilltoPhoneNumber( $strCustomerPhoneNumber );
		}
	}

	public function loadCustomerPrimaryAddress( $objClientDatabase ) {
		$strStreetLine1 = $strStreetLine2 = $strStreetLine3 = $strCity = $strStateCode = $strPostalCode = '';
		$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );
		if( true == \Psi\Libraries\UtilFunctions\valObj( $objCustomer, \CCustomer::class ) ) {
			$objCustomerAddress = \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressByCustomerIdByAddressTypeIdByCid( $objCustomer->getId(), \CAddressType::PRIMARY, $objCustomer->getCid(), $objClientDatabase, false, true );

			if( true == \Psi\Libraries\UtilFunctions\valObj( $objCustomerAddress, \CCustomerAddress::class ) ) {
				list( $strStreetLine1, $strStreetLine2, $strStreetLine3, $strCity, $strStateCode, $strPostalCode ) = $this->getAddressFields( $objCustomerAddress );
			} else {
				$objLease = $this->getOrFetchLease( $objClientDatabase );
				$objUnitAddress = NULL;
				if( true == valId( $objLease->getPropertyUnitId() ) ) {
					$objUnitAddress = \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchUnitAddressByPropertyUnitIdByAddressTypeIdByCid( $objLease->getPropertyUnitId(), \CAddressType::PRIMARY, $objCustomer->getCid(), $objClientDatabase, true );
					if( true == \Psi\Libraries\UtilFunctions\valObj( $objUnitAddress, \CUnitAddress::class ) ) {
						list( $strStreetLine1, $strStreetLine2, $strStreetLine3, $strCity, $strStateCode, $strPostalCode ) = $this->getAddressFields( $objUnitAddress );
					}
				}

				if( false == \Psi\Libraries\UtilFunctions\valObj( $objUnitAddress, \CUnitAddress::class ) ) {
					$objPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyAddressByPropertyIdByAddressTypeIdByCid( $objLease->getPropertyId(), ( array ) \CAddressType::PRIMARY, $objCustomer->getCid(), $objClientDatabase );
					if( true == \Psi\Libraries\UtilFunctions\valObj( $objPropertyAddress, \CPropertyAddress::class ) ) {
						list( $strStreetLine1, $strStreetLine2, $strStreetLine3, $strCity, $strStateCode, $strPostalCode ) = $this->getAddressFields( $objPropertyAddress );
					}
				}
			}

			$this->setBilltoStreetLine1( $strStreetLine1 );
			$this->setBilltoStreetLine2( $strStreetLine2 );
			$this->setBilltoStreetLine3( $strStreetLine3 );
			$this->setBilltoCity( $strCity );
			$this->setBilltoStateCode( $strStateCode );
			$this->setBilltoPostalCode( $strPostalCode );
		}
	}

	public function getAddressFields( $objCustomerAddress ) {

		$strStreetLine1 = $objCustomerAddress->getStreetLine1();
		$strStreetLine2 = $objCustomerAddress->getStreetLine2();
		$strStreetLine3 = $objCustomerAddress->getStreetLine3();
		$strCity        = $objCustomerAddress->getCity();
		$strStateCode   = $objCustomerAddress->getStateCode();
		$strPostalCode  = $objCustomerAddress->getPostalCode();

		return [ $strStreetLine1,$strStreetLine2,$strStreetLine3,$strCity,$strStateCode,$strPostalCode ];
	}

	public function loadPhoneNumber( $objClientDatabase ) {
		$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );
		if( true == \Psi\Libraries\UtilFunctions\valObj( $objCustomer, \CCustomer::class ) ) {
			$strCustomerPhoneNumber = $objCustomer->getAvailableCustomerPhoneNumber( $objClientDatabase );
			if( true == \Psi\Libraries\UtilFunctions\valStr( $strCustomerPhoneNumber ) ) {
				$this->setBilltoPhoneNumber( $strCustomerPhoneNumber );
			}
		}
	}

	public function loadCustomerNameAndEmail( $objClientDatabase ) {
		$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );
		if( true == \Psi\Libraries\UtilFunctions\valObj( $objCustomer, \CCustomer::class ) ) {
			$this->setBilltoEmailAddress( $objCustomer->getUsername() );
			$this->setBilltoNameFirst( $objCustomer->getNameFirst() );
			$this->setBilltoNameLast( $objCustomer->getNameLast() );
		}
	}

	public function loadRoomateData( $objCustomer, $objClientDatabase ) {
		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			if( false === is_null( $objCustomer->getPrimaryPostalCode() ) ) {
				$this->setBilltoPostalCode( $objCustomer->getPrimaryPostalCode() );
			} else {
				$this->setBilltoPostalCode( $this->getBilltoPostalCode() );
			}

			$this->setBilltoEmailAddress( $objCustomer->getUsername() );
			$this->setBilltoNameFirst( $objCustomer->getNameFirst() );
			$this->setBilltoNameLast( $objCustomer->getNameLast() );
			$this->setBilltoStreetLine1( $objCustomer->getPrimaryStreetLine1() );
			$this->setBilltoStreetLine2( $objCustomer->getPrimaryStreetLine2() );
			$this->setBilltoCity( $objCustomer->getPrimaryCity() );

			if( true == valStr( $objCustomer->getPrimaryStateCode() ) ) {
				$strWhereSql = 'WHERE code = \'' . $objCustomer->getPrimaryStateCode() . '\'';
				$intStatesCount = ( int ) \Psi\Eos\Entrata\CStates::createService()->fetchStateCount( $strWhereSql, $objClientDatabase );
				if( true == valId( $intStatesCount ) ) {
					$this->setBilltoStateCode( $objCustomer->getPrimaryStateCode() );
				}
			}

			$this->setBilltoPhoneNumber( $objCustomer->getAvailableCustomerPhoneNumber( $objClientDatabase ) );
		}
	}

	public function loadEmailDatabase() {
		if( false == valObj( $this->m_objEmailDatabase, 'CDatabase' ) ) {
			$this->m_objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, CDatabaseUserType::PS_DEVELOPER, CDatabaseServerType::POSTGRES, false );
		}
		return $this->m_objEmailDatabase;
	}

	public function setEndDatesFromLease( $objLease ) {
		$strLeaseEndDate = ( false == is_null( $objLease->getMoveOutDate() ) ? $objLease->getMoveOutDate() : $objLease->getLeaseEndDate() );

		if( true == is_null( $this->getBillDay() ) ) {
			$this->setBillDay( ( int ) date( 'd', strtotime( $this->getPaymentStartDate() ) ) );
		}

		$arrintLeaseEndDate = explode( '/', $strLeaseEndDate );
		if( $arrintLeaseEndDate[1] < $this->getBillDay() ) {
			$intEndMonth = $arrintLeaseEndDate[0] - 1;
			$intEndYear = $arrintLeaseEndDate[2];
			if( 0 == $intEndMonth ) {
				$intEndMonth = 12;
				$intEndYear = $arrintLeaseEndDate[2] - 1;
			}

			$intEndMonth = ( 9 < $intEndMonth ) ? $intEndMonth : '0' . $intEndMonth;
			$strLeaseEndDate = $intEndMonth . '/' . $this->getBillDay() . '/' . $intEndYear;
		}

		$this->setLeaseEndDate( $strLeaseEndDate );
		$this->setPaymentEndDate( $strLeaseEndDate );
	}

	public function insertScheduledPaymentTransaction( $intScheduledPaymentTransTypeId, $strAdditionalDescription, $intArPaymentId, $intCompanyUserId, $objClientDatabase, $boolIsFailure = true ) {

		$objScheduledPaymentTransaction = $this->createScheduledPaymentTransaction();
		$objScheduledPaymentTransaction->setScheduledPaymentTransTypeId( $intScheduledPaymentTransTypeId );
		$objScheduledPaymentTransaction->setArPaymentId( $intArPaymentId );
		$objScheduledPaymentTransaction->setAdditionalDescription( $strAdditionalDescription );
		$objScheduledPaymentTransaction->setIsFailure( $boolIsFailure );

		if( false == $objScheduledPaymentTransaction->insert( $intCompanyUserId, $objClientDatabase ) ) {
			trigger_error( 'Scheduled payment transaction failed to insert.', E_USER_ERROR );
			exit;
		}

		return true;
	}

	public function buildRecurringPaymentDays( $arrobjPropertyPreferences, $objClientDatabase = NULL ) {
		$objRecurringPaymentDaysPropertyPreference = NULL;

		if( true == valArr( $arrobjPropertyPreferences ) ) {
			foreach( $arrobjPropertyPreferences as $objPropertyPreference ) {
				if( $objPropertyPreference->getKey() == 'RECURRING_PAYMENTS_DAYS' ) {
					$objRecurringPaymentDaysPropertyPreference = $objPropertyPreference;
				}
				if( $objPropertyPreference->getKey() == 'BLOCKED_ACH_DAYS' ) {
					$objBlockedAchDaysPropertyPreference = $objPropertyPreference;
				}
				if( $objPropertyPreference->getKey() == 'BLOCKED_CC_DAYS' ) {
					$objBlockedCCDaysPropertyPreference = $objPropertyPreference;
				}
			}
		}

		$arrintBlockedAchDays = ( true == isset( $objBlockedAchDaysPropertyPreference ) && true == valObj( $objBlockedAchDaysPropertyPreference, 'CPropertyPreference' ) ) ? explode( ',', $objBlockedAchDaysPropertyPreference->getValue() ) : [];
		$arrintBlockedCCDays = ( true == isset( $objBlockedCCDaysPropertyPreference ) && true == valObj( $objBlockedCCDaysPropertyPreference, 'CPropertyPreference' ) ) ? explode( ',', $objBlockedCCDaysPropertyPreference->getValue() ) : [];

		$boolAreEqual = ( $arrintBlockedAchDays == $arrintBlockedCCDays ) ? true : false;

		if( true == valObj( $objRecurringPaymentDaysPropertyPreference, 'CPropertyPreference' ) ) {
			$arrintRecurringPaymentDays = explode( ',', $objRecurringPaymentDaysPropertyPreference->getValue() );
			if( true == valArr( $arrintRecurringPaymentDays ) ) {
				foreach( $arrintRecurringPaymentDays as $strKey => $intArRecurringPaymentDay ) {
					if( false == is_numeric( $intArRecurringPaymentDay ) || $intArRecurringPaymentDay > 31 || $intArRecurringPaymentDay < 1 ) {
						unset( $arrintRecurringPaymentDays[$strKey] );
					}
				}
			}
		}

		$arrintRecurringPaymentDays = ( true == isset( $arrintRecurringPaymentDays ) && true == valArr( $arrintRecurringPaymentDays ) ) ? array_unique( $arrintRecurringPaymentDays ) : [ 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31 ];

		// check if payment is blocked for all lease status
		$boolBlockAllLeaseStatusPayments = ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS', $arrobjPropertyPreferences ) && 2 == ( int ) $arrobjPropertyPreferences['BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS']->getValue() ) ? true : false;

		// check if payment is blocked for current status
		$boolBlockCurrentLeaseStatusPayments = ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS', $arrobjPropertyPreferences ) && 3 == ( int ) $arrobjPropertyPreferences['BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS']->getValue() ) ? true : false;

		// check if payment is blocked for current and notice status
		$boolBlockCurrentAndNoticeLeaseStatusPayments = ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS', $arrobjPropertyPreferences ) && 1 == ( int ) $arrobjPropertyPreferences['BLOCK_ONLINE_PAYMENTS_FOR_LEASE_STATUS']->getValue() ) ? true : false;

		// If "Lease Statuses Affected by Days Above" setting is not exists, then consider default behaviour as "Current & Notice status"
		if( false == $boolBlockAllLeaseStatusPayments && false == $boolBlockCurrentLeaseStatusPayments && false == $boolBlockCurrentAndNoticeLeaseStatusPayments ) {
			$boolBlockCurrentAndNoticeLeaseStatusPayments = true;
		}

		$boolCheckDisallowedPaymentDay = false;

		if( false == is_null( $objClientDatabase ) ) {

			$this->m_objLease = $this->fetchLease( $objClientDatabase );
			$boolIsCurrentOrNoticeResident	= true; // Few property settings should get apply only for current residents( current resident = lease status current and notice ). e.g. payment block days
			$boolIsPastResident 			= false;
			$boolIsCurrentResident			= false;

			if( true == valObj( $this->m_objLease, 'CLease' ) ) {
				if( false == in_array( $this->m_objLease->getLeaseStatusTypeId(), [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) ) {
					$boolIsCurrentOrNoticeResident = false;
				}
				if( CLeaseStatusType::PAST == $this->m_objLease->getLeaseStatusTypeId() ) {
					$boolIsPastResident = true;
				}
				if( CLeaseStatusType::CURRENT == $this->m_objLease->getLeaseStatusTypeId() ) {
					$boolIsCurrentResident = true;
				}
			}

			if( true == is_numeric( $this->getCustomerId() ) ) {
				if( ( true == $boolBlockAllLeaseStatusPayments ) || ( true == $boolIsCurrentResident && true == $boolBlockCurrentLeaseStatusPayments ) || ( true == $boolIsCurrentOrNoticeResident && true == $boolBlockCurrentAndNoticeLeaseStatusPayments ) ) {
					$boolCheckDisallowedPaymentDay = true;
				}
			} else {
				if( ( true == $boolBlockAllLeaseStatusPayments ) || ( true == $boolBlockCurrentLeaseStatusPayments ) || ( true == $boolBlockCurrentAndNoticeLeaseStatusPayments ) ) {
					$boolCheckDisallowedPaymentDay = true;
				}
			}
		}

		if( false == $boolAreEqual ) {
			$arrintBlockedPaymentDays = array_intersect( $arrintBlockedAchDays, $arrintBlockedCCDays );
		} else {
			$arrintBlockedPaymentDays = $arrintBlockedAchDays;
		}

		if( ( true == valArr( $arrintBlockedPaymentDays )
		      && ( ( true == $boolCheckDisallowedPaymentDay && true == $this->getIsResidentPortalPayment() )
		           || false == $this->getIsResidentPortalPayment() ) ) ) {
			foreach( $arrintRecurringPaymentDays as $strKey => $intArRecurringPaymentDay ) {
				if( in_array( $intArRecurringPaymentDay, $arrintBlockedPaymentDays ) && true == $boolCheckDisallowedPaymentDay ) {
					unset( $arrintRecurringPaymentDays[$strKey] );
				}
			}
		}

		$this->m_arrintRecurringPaymentDays = $arrintRecurringPaymentDays;
	}

	public function buildMonthSelects( $objClientDatabase = NULL, $boolFlagForRp2Working = false, $boolIsAllowPastResidentScheduledPayment = false, $boolIsCurrentMonth = false ) {

		$boolIsTodayLastDayOfMonth = NULL;
		if( CScheduledPaymentFrequency::ONCE == $this->getScheduledPaymentFrequencyId() && CScheduledPaymentType::ONETIME == $this->getScheduledPaymentTypeId() ) {
			$intStartMonth			= date( 'n' );
			$intStartYear			= date( 'Y' );
			$intCurrentMonthTime 	= mktime( 0, 0, 0, $intStartMonth, 1, $intStartYear );
		} else {
			if( true == $boolFlagForRp2Working ) {
				$boolIsTodayLastDayOfMonth = false;
				$intNextDayMonth = date( 'm', strtotime( '+1 day', strtotime( date( 'Y/m/d' ) ) ) );
				$intTodayMonth = date( 'm', strtotime( date( 'Y/m/d' ) ) );

				if( $intNextDayMonth == $intTodayMonth && true == valArr( $this->m_arrintRecurringPaymentDays ) && ( date( 'd' ) < max( $this->m_arrintRecurringPaymentDays ) ) ) {
					$intStartMonth 			= date( 'n', strtotime( 'first day of this month' ) );
					$intStartYear 			= date( 'Y', strtotime( 'first day of this month' ) );
				} else {
					$boolIsTodayLastDayOfMonth = true;
					$intStartMonth 			= date( 'n', strtotime( 'first day of next month' ) );
					$intStartYear 			= date( 'Y', strtotime( 'first day of next month' ) );
				}
			} else {
				$intStartMonth 			= date( 'n', strtotime( 'first day of next month' ) );
				$intStartYear 			= date( 'Y', strtotime( 'first day of next month' ) );
			}
			$intCurrentMonthTime 	= mktime( 0, 0, 0, $intStartMonth, 1, $intStartYear );
		}
		if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$arrobjPropertyPreferences 	= CPropertyPreferences::fetchPropertyPreferencesByPropertyIdsByKeysByCid( [ $this->m_intPropertyId ], [ 'DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS' ], $this->m_intCid, $objClientDatabase );
			$arrobjPropertyPreferences = rekeyObjects( 'key', $arrobjPropertyPreferences );

			$objLease = $this->getOrFetchLease( $objClientDatabase );

			if( true == valObj( $objLease, 'CLease' ) && true == valArr( $arrobjPropertyPreferences ) && true == valObj( $arrobjPropertyPreferences['DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS'], 'CPropertyPreference' ) && 1 == $arrobjPropertyPreferences['DISALLOW_LAST_LEASE_MONTH_RECURRING_PAYMENTS']->getValue() ) {
				$strLeaseEndDate = ( false == is_null( $objLease->getMoveOutDate() ) ? $objLease->getMoveOutDate() : $objLease->getLeaseEndDate() );
				// Determine what the last day of the lease end month is.
				$intEndMonthTime 	= mktime( 0, 0, 0, ( ( int ) \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 0, 2 ) - 1 ), 1, ( int ) \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) );
			}
		}

		if( true == $boolIsCurrentMonth && true == \Psi\Libraries\UtilFunctions\valId( $this->getBillDay() ) && date( 'd' ) < $this->getBillDay() ) {
			$boolIsAllowAddCurrentMonth = true;
		}

		for( $intX = 0; $intX < 24; $intX++ ) {

			if( true == isset( $intEndMonthTime ) && ( $intCurrentMonthTime > $intEndMonthTime ) ) {
				break;
			}

			if( $intX == 0 ) {

				$strNextMonth = '';
				$intLoopCounter = 12;

				if( true == $boolFlagForRp2Working ) {
					if( true == $boolIsTodayLastDayOfMonth ) {
						if( CScheduledPaymentFrequency::ONCE != $this->getScheduledPaymentFrequencyId() && CScheduledPaymentType::ONETIME == $this->getScheduledPaymentTypeId() ) {
							$strNextMonth = '(' . __( 'Next Month' ) . ')';
						}
					}
				} else {
					if( CScheduledPaymentFrequency::ONCE != $this->getScheduledPaymentFrequencyId() && CScheduledPaymentType::ONETIME == $this->getScheduledPaymentTypeId() ) {
						$strNextMonth = '(' . __( 'Next Month' ) . ')';
					}
				}

				if( true == $boolIsAllowAddCurrentMonth ) {
					$strNextMonth = NULL;
					$intLoopCounter = $intLoopCounter + 1;
					$intCurrentMonthTime = strtotime( '-1 month', $intCurrentMonthTime );
					$this->m_arrstrStartMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				}

				$this->m_arrstrStartMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime ) . $strNextMonth;

				if( false == is_null( $this->getId() ) && date( 'Ym', strtotime( $this->getPaymentStartDate() ) ) < date( 'Ym', $intCurrentMonthTime ) ) {
					$this->m_arrstrEndMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				}

			} elseif( $intX < 12 ) {
				$this->m_arrstrStartMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				if( true == is_null( $this->getId() ) || ( false == is_null( $this->getId() ) && date( 'Ym', strtotime( $this->getPaymentStartDate() ) ) < date( 'Ym', $intCurrentMonthTime ) ) ) {
					$this->m_arrstrEndMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				}

			} else {
				if( true == is_null( $this->getId() ) || ( false == is_null( $this->getId() ) && date( 'Ym', strtotime( $this->getPaymentStartDate() ) ) < date( 'Ym', $intCurrentMonthTime ) ) ) {
					$this->m_arrstrEndMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				}
			}

			$intCurrentMonthTime = strtotime( '+1 month', $intCurrentMonthTime );
		}

		if( true == $boolIsAllowPastResidentScheduledPayment && ( false == isset( $intEndMonthTime ) || ( $intCurrentMonthTime < $intEndMonthTime ) ) ) {
			$this->m_arrstrEndMonths[NULL] = CScheduledPayment::STR_UNTIL_I_CANCEL;
		}

		if( true == valArr( $this->m_arrstrEndMonths ) ) {
			ksort( $this->m_arrstrEndMonths );
		}
	}

	public function buildRecurringPaymentMonthSelects( $boolDisallowLastLeaseMonthRecurringPayments, $boolAllowPastResidentScheduledPayment = NULL, $objClientDatabase, $boolIsCurrentMonth = false ) {

		$boolIsTodayLastDayOfMonth = NULL;
		if( CScheduledPaymentFrequency::ONCE == $this->getScheduledPaymentFrequencyId() && CScheduledPaymentType::ONETIME == $this->getScheduledPaymentTypeId() ) {
			$intStartMonth 			= date( 'n' );
			$intStartYear 			= date( 'Y' );
			$intCurrentMonthTime 	= mktime( 0, 0, 0, $intStartMonth, 1, $intStartYear );
		} else {
			$boolIsTodayLastDayOfMonth = false;
			$intNextDayMonth = date( 'm', strtotime( '+1 day', strtotime( date( 'Y/m/d' ) ) ) );
			$intTodayMonth = date( 'm', strtotime( date( 'Y/m/d' ) ) );

			if( $intNextDayMonth == $intTodayMonth && true == valArr( $this->m_arrintRecurringPaymentDays ) && ( date( 'd' ) < max( $this->m_arrintRecurringPaymentDays ) ) ) {
				$intStartMonth 			= date( 'n', strtotime( 'first day of this month' ) );
				$intStartYear 			= date( 'Y', strtotime( 'first day of this month' ) );
			} else {
				$boolIsTodayLastDayOfMonth = true;
				$intStartMonth 			= date( 'n', strtotime( 'first day of next month' ) );
				$intStartYear 			= date( 'Y', strtotime( 'first day of next month' ) );
			}
			$intCurrentMonthTime 	= mktime( 0, 0, 0, $intStartMonth, 1, $intStartYear );
		}
		$objLease = $this->getOrFetchLease( $objClientDatabase );
		if( true == $boolDisallowLastLeaseMonthRecurringPayments && true == valObj( $objLease, 'CLease' ) ) {
			$strLeaseEndDate = ( false == is_null( $objLease->getMoveOutDate() ) ? $objLease->getMoveOutDate() : $objLease->getLeaseEndDate() );
			$intEndMonthTime = mktime( 0, 0, 0, ( ( int ) \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 0, 2 ) - 1 ), 1, ( int ) \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) );
		}

		if( true == $boolIsCurrentMonth && true == \Psi\Libraries\UtilFunctions\valId( $this->getBillDay() ) && date( 'd' ) < $this->getBillDay() ) {
			$boolIsAllowAddCurrentMonth = true;
		}

		for( $intCounter = 0; $intCounter < 24; $intCounter++ ) {

			if( true == isset( $intEndMonthTime ) && ( $intCurrentMonthTime > $intEndMonthTime ) ) {
				break;
			}

			if( $intCounter == 0 ) {
				$strNextMonth = '';
				$intLoopCounter = 12;

				if( true == $boolIsTodayLastDayOfMonth && CScheduledPaymentFrequency::ONCE != $this->getScheduledPaymentFrequencyId() ) {
					$strNextMonth = '(' . __( 'Next Month' ) . ')';
				}

				if( true == $boolIsAllowAddCurrentMonth ) {
					$strNextMonth = NULL;
					$intLoopCounter = $intLoopCounter + 1;
					$intCurrentMonthTime = strtotime( '-1 month', $intCurrentMonthTime );
					$this->m_arrstrStartMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				}

				$this->m_arrstrStartMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime ) . $strNextMonth;

				if( false == is_null( $this->getId() ) && date( 'Ym', strtotime( $this->getPaymentStartDate() ) ) < date( 'Ym', $intCurrentMonthTime ) ) {
					$this->m_arrstrEndMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				}

			} elseif( $intCounter < $intLoopCounter ) {
				$this->m_arrstrStartMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				if( true == is_null( $this->getId() ) || ( false == is_null( $this->getId() ) && date( 'Ym', strtotime( $this->getPaymentStartDate() ) ) < date( 'Ym', $intCurrentMonthTime ) ) ) {
					$this->m_arrstrEndMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				}

			} else {
				if( true == is_null( $this->getId() ) || ( false == is_null( $this->getId() ) && date( 'Ym', strtotime( $this->getPaymentStartDate() ) ) < date( 'Ym', $intCurrentMonthTime ) ) ) {
					$this->m_arrstrEndMonths[$intCurrentMonthTime] = date( 'F Y', $intCurrentMonthTime );
				}
			}

			$intCurrentMonthTime = strtotime( '+1 month', $intCurrentMonthTime );
		}

		if( true == valArr( $this->m_arrstrEndMonths ) ) {
			ksort( $this->m_arrstrEndMonths );
		}

		$arrstrTempEndMonths = $this->m_arrstrEndMonths;

		$this->m_arrstrEndMonths = [];

		if( true == $boolAllowPastResidentScheduledPayment && ( false == isset( $intEndMonthTime ) || ( $intCurrentMonthTime < $intEndMonthTime ) ) ) {
			$this->m_arrstrEndMonths[NULL] = CScheduledPayment::STR_UNTIL_I_CANCEL;
		}

		if( true == valArr( $arrstrTempEndMonths ) && true == valObj( $objLease, 'CLease' ) ) {
			$objLeaseCustomer = $objLease->getOrFetchLeaseCustomer( $this->getCustomerId(), $objClientDatabase );
			$boolIsLeaseEndDateAllowed = ( true == valObj( $objLeaseCustomer, 'CLeaseCustomer' ) && ( CLeaseStatusType::PAST == $objLeaseCustomer->getLeaseStatusTypeId() ) ) ? false : true;
			if( true == $boolIsLeaseEndDateAllowed ) {
				$this->m_arrstrEndMonths[0] = CScheduledPayment::STR_UNTIL_I_CANCEL_OR_MOVE_OUT;
			}
		}

		$arrintTempEndMonthsKeys = array_keys( ( array ) $arrstrTempEndMonths );
		foreach( $arrintTempEndMonthsKeys as $intEndMonthTime ) {
			$this->m_arrstrEndMonths[$intEndMonthTime] = $arrstrTempEndMonths[$intEndMonthTime];
		}

	}

	public function indicateCustomerPaymentAccountUsage( $intCompanyUserId, $objClientDatabase ) {

		if( true == is_numeric( $this->getCustomerPaymentAccountId() ) && 1 == $this->getUseStoredBillingInfo() ) {
			$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );

			if( true == valObj( $objCustomer, 'CCustomer' ) ) {
				$arrobjCustomerPaymentAccounts = $objCustomer->getOrFetchCustomerPaymentAccounts( $objClientDatabase );

				if( true == valArr( $arrobjCustomerPaymentAccounts ) && true == array_key_exists( $this->getCustomerPaymentAccountId(), $arrobjCustomerPaymentAccounts ) ) {
					$arrobjCustomerPaymentAccounts[$this->getCustomerPaymentAccountId()]->setLastUsedOn( date( 'm/d/Y H:i:s' ) );
					return $arrobjCustomerPaymentAccounts[$this->getCustomerPaymentAccountId()]->update( $intCompanyUserId, $objClientDatabase );
				}
			}
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = false ) {

		$objDataset = $objClientDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = "SELECT nextval( 'scheduled_payments_id_seq'::text ) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert scheduled payment record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrmixValues = $objDataset->fetchArray();
			$this->setId( $arrmixValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
		          'FROM scheduled_payments_insert( row_to_json ( ROW ( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPaymentTypeId() . ', ' .
		          $this->sqlScheduledPaymentTypeId() . ', ' .
		          $this->sqlScheduledPaymentFrequencyId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlPsProductOptionId() . ', ' .
		          $this->sqlCompanyCharityId() . ', ' .
		          $this->sqlPaymentAmount() . ', ' .
		          $this->sqlPaymentCeilingAmount() . ', ' .
		          $this->sqlDonationAmount() . ', ' .
		          $this->sqlPaymentAmountRate() . ', ' .
		          $this->sqlCachedBalance() . ', ' .
		          $this->sqlPaymentStartDate() . ', ' .
		          $this->sqlPaymentEndDate() . ', ' .
		          $this->sqlBilltoUnitNumber() . ', ' .
		          $this->sqlBilltoNameFirst() . ', ' .
		          $this->sqlBilltoNameMiddle() . ', ' .
		          $this->sqlBilltoNameLast() . ', ' .
		          $this->sqlPaymentMemo() . ', ' .
		          $this->sqlFailureCount() . ', ' .
		          $this->sqlFeeWaived() . ', ' .
		          $this->sqlIsVariableAmount() . ', ' .
		          $this->sqlIsPayByText() . ', ' .
		          $this->sqlForceReprocess() . ', ' .
		          $this->sqlUseLeaseEnd() . ', ' .
		          $this->sqlTermsAcceptedOn() . ', ' .
		          $this->sqlLastWarnedOn() . ', ' .
		          $this->sqlLastEmailedOn() . ', ' .
		          $this->sqlApprovedOn() . ', ' .
		          $this->sqlLastPostedBy() . ', ' .
		          $this->sqlLastPostedOn() . ', ' .
		          $this->sqlPausedBy() . ', ' .
		          $this->sqlPausedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
			      $this->sqlProcessDay() . ', ' .
			      $this->sqlCardTypeId() . ', ' .
		          $this->sqlCustomerPaymentAccountId() . ') ) ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			// Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert scheduled payment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert scheduled payment record. The following error was reported.' ) );
			// Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		// Now we need to insert the CScheduledPaymentDetail record
		$this->m_objScheduledPaymentDetail = $this->createScheduledPaymentDetail( $intCurrentUserId, $objClientDatabase );
		$strStateCode = $this->m_objScheduledPaymentDetail->getBilltoStateCode();
		$this->m_objScheduledPaymentDetail->setSplitReminderOn( date( 'm/d/Y' ) );
		if( true == isset( $strStateCode ) && false == is_null( $strStateCode ) && false === ( bool ) \Psi\Eos\Entrata\CStates::createService()->fetchStateCount( ' WHERE code LIKE \'' . $strStateCode . '\'', $objClientDatabase ) ) {
			$this->m_objScheduledPaymentDetail->setBilltoStateCode( NULL );
		}

		if( false == $this->m_objScheduledPaymentDetail->insert( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->m_objScheduledPaymentDetail->getErrorMsgs() );
			return false;
		}

		// Add scheduled payment incentive
		$arrobjRecurringScheduledSpecialsConccessionCharges = $this->createRecurringPaymentSpecialsConccesions( $intCurrentUserId, $objClientDatabase );

		// Export incentive charge to external system
		$objLease = $this->getOrFetchLease( $objClientDatabase );

		if( true == isset( $objLease ) && false == is_null( $objLease->getRemotePrimaryKey() ) && true == valArr( $arrobjRecurringScheduledSpecialsConccessionCharges ) ) {
			$objIntegrationClientType = $objLease->fetchIntegrationClientType( $objClientDatabase );

			if( true == isset( $objIntegrationClientType ) && true == in_array( $objIntegrationClientType->getId(), [ CIntegrationClientType::MRI, CIntegrationClientType::YARDI, CIntegrationClientType::AMSI ] ) ) {
				$this->exportRecurringPaymentSpecialsConccesions( $arrobjRecurringScheduledSpecialsConccessionCharges, $intCurrentUserId, $objClientDatabase, $boolQueueSyncOverride = false );
				$this->setRecurringPaymentSpecials( $this->m_arrobjRecurringPaymentSpecials );
			}
		}

		return true;
	}

	public function update( $intCurrentUserId, $objClientDatabase, $boolUpdateDetailRecord = false ) {

		$objDataset = $objClientDatabase->createDataset();

		$strSql = 'SELECT * ' .
		          'FROM scheduled_payments_update( row_to_json ( ROW ( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPaymentTypeId() . ', ' .
		          $this->sqlScheduledPaymentTypeId() . ', ' .
		          $this->sqlScheduledPaymentFrequencyId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlPsProductOptionId() . ', ' .
		          $this->sqlCompanyCharityId() . ', ' .
		          $this->sqlPaymentAmount() . ', ' .
		          $this->sqlPaymentCeilingAmount() . ', ' .
		          $this->sqlDonationAmount() . ', ' .
		          $this->sqlPaymentAmountRate() . ', ' .
		          $this->sqlCachedBalance() . ', ' .
		          $this->sqlPaymentStartDate() . ', ' .
		          $this->sqlPaymentEndDate() . ', ' .
		          $this->sqlBilltoUnitNumber() . ', ' .
		          $this->sqlBilltoNameFirst() . ', ' .
		          $this->sqlBilltoNameMiddle() . ', ' .
		          $this->sqlBilltoNameLast() . ', ' .
		          $this->sqlPaymentMemo() . ', ' .
		          $this->sqlFailureCount() . ', ' .
		          $this->sqlFeeWaived() . ', ' .
		          $this->sqlIsVariableAmount() . ', ' .
		          $this->sqlIsPayByText() . ', ' .
		          $this->sqlForceReprocess() . ', ' .
		          $this->sqlUseLeaseEnd() . ', ' .
		          $this->sqlTermsAcceptedOn() . ', ' .
		          $this->sqlLastWarnedOn() . ', ' .
		          $this->sqlLastEmailedOn() . ', ' .
		          $this->sqlApprovedOn() . ', ' .
		          $this->sqlLastPostedBy() . ', ' .
		          $this->sqlLastPostedOn() . ', ' .
		          $this->sqlPausedBy() . ', ' .
		          $this->sqlPausedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
			      $this->sqlProcessDay() . ', ' .
			      $this->sqlCardTypeId() . ', ' .
		          $this->sqlCustomerPaymentAccountId() . ') ) ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update scheduled payment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update scheduled payment record. The following error was reported.' ) );
			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();
			return false;
		}

		$objDataset->cleanup();

		if( true == isset( $this->m_objScheduledPaymentDetail ) ) {
			if( false == $this->m_objScheduledPaymentDetail->update( $intCurrentUserId, $objClientDatabase ) ) {
				$this->addErrorMsgs( $this->m_objScheduledPaymentDetail->getErrorMsgs() );
				return false;
			}
		}

		// Now we are also going to update the scheduled_payment_detail record.
		if( true == $boolUpdateDetailRecord ) {

			$objDataset = $objClientDatabase->createDataset();
			$intCorrespondingScheduledPaymentDetailId = $this->getOrFetchCorrespondingScheduledPaymentDetailId( $objClientDatabase );

			if( false == is_numeric( ( $intCorrespondingScheduledPaymentDetailId ) ) ) {
				return false;
			}

			$boolIsDebitCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsDebitCard() ) ) ? 'true' : 'false';
			$boolIsCheckCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsCheckCard() ) ) ? 'true' : 'false';
			$boolIsGiftCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsGiftCard() ) ) ? 'true' : 'false';
			$boolIsPrepaidCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsPrepaidCard() ) ) ? 'true' : 'false';
			$boolIsCreditCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsCreditCard() ) ) ? 'true' : 'false';
			$boolIsCommercialCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsCommercialCard() ) ) ? 'true' : 'false';
			$boolIsInternationalCard = ( true == \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $this->getIsInternationalCard() ) ) ? 'true' : 'false';

			$strSql = 'SELECT * ' .
			          'FROM scheduled_payment_details_update( ' .
			          $intCorrespondingScheduledPaymentDetailId . ', ' .
			          $this->sqlCid() . ', ' .
			          $this->sqlId() . ', ' .
			          $this->sqlAccountVerificationLogId() . ', ' .
			          $this->sqlSecureReferenceNumber() . ', ' .
			          $this->sqlBilltoIpAddress() . ', ' .
			          $this->sqlBilltoAccountNumber() . ', ' .
			          $this->sqlBilltoCompanyName() . ', ' .
			          $this->sqlBilltoStreetLine1() . ', ' .
			          $this->sqlBilltoStreetLine2() . ', ' .
			          $this->sqlBilltoStreetLine3() . ', ' .
			          $this->sqlBilltoCity() . ', ' .
			          $this->sqlBilltoStateCode() . ', ' .
			          $this->sqlBilltoProvince() . ', ' .
			          $this->sqlBilltoPostalCode() . ', ' .
			          $this->sqlBilltoCountryCode() . ', ' .
			          $this->sqlBilltoPhoneNumber() . ', ' .
			          $this->sqlBilltoEmailAddress() . ', ' .
			          $this->sqlSplitEmailsCount() . ', ' .
			          $this->sqlSplitReminderOn() . ', ' .
			          $this->sqlCcCardNumberEncrypted() . ', ' .
			          $this->sqlCcBinNumber() . ', ' .
			          $this->sqlCcExpDateMonth() . ', ' .
			          $this->sqlCcExpDateYear() . ', ' .
			          $this->sqlCcNameOnCard() . ', ' .
			          $this->sqlCheckBankName() . ', ' .
			          $this->sqlCheckNameOnAccount() . ', ' .
			          $this->sqlCheckAccountTypeId() . ', ' .
			          $this->sqlCheckRoutingNumber() . ', ' .
			          $this->sqlCheckAccountNumberEncrypted() . ', ' .
			          $boolIsDebitCard . ', ' .
			          $boolIsCheckCard . ', ' .
			          $boolIsGiftCard . ', ' .
			          $boolIsPrepaidCard . ', ' .
			          $boolIsCreditCard . ', ' .
			          $boolIsCommercialCard . ', ' .
			          $boolIsInternationalCard . ', ' .
			          ( int ) $intCurrentUserId . ', ' .
			          $this->sqlPaymentBankAccountId() . '::uuid ) AS result';

			if( false == $objDataset->execute( $strSql ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update scheduled payment detail record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

				$objDataset->cleanup();

				return false;
			}

			if( 0 < $objDataset->getRecordCount() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update scheduled payment detail record. The following error was reported.' ) );
				while( false == $objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
					$objDataset->next();
				}

				$objDataset->cleanup();

				return false;
			}

			$objDataset->cleanup();

			return true;
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objClientDatabase, $strMessage = NULL, $objWebsite = NULL, $boolIsResidentPortalPayment = false, $boolIsResidentPortalPremiumPayment = false, $boolSendEmails = true, $boolStoreActivityLog = true, $objEmailDatabase = NULL ) {
		$boolIsValid = true;

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$boolIsValid = $this->update( $intCurrentUserId, $objClientDatabase );

		if( false == $boolIsValid ) {
			return $boolIsValid;

		} else {

			// logic to send email for this deleted schedule payment
			if( true == $boolSendEmails ) {
				$arrstrManagerPaymentNotificationEmails	= [];
				$arrstrResidentPaymentNotificationEmail	= [];
				$strHtmlManagerEmailOutput 				= NULL;
				$strHtmlResidentEmailOutput 			= NULL;

				$objProperty 				= \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
				$arrobjPropertyPreferences	= $objProperty->fetchPropertyPreferencesByKeys( [ 'FROM_MANAGER_NOTIFICATION_EMAIL', 'FROM_PAYMENT_NOTIFICATION_EMAIL' ], $objClientDatabase );
				$arrobjPropertyPreferences 	= rekeyObjects( 'Key', $arrobjPropertyPreferences );

				if( true == valObj( $objProperty, 'CProperty' ) ) {
					$arrstrManagerPaymentNotificationEmails = $objProperty->fetchPaymentNotificationEmails( $objClientDatabase );
				}

				$objPropertyEmailRule = CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::DELETE_SCHEDULED_PAYMENT_NOTIFICATION, $this->getPropertyId(), $objClientDatabase );

				if( false == is_null( $this->getBilltoEmailAddress() ) && 0 < strlen( $this->getBilltoEmailAddress() ) ) {
					$arrstrResidentPaymentNotificationEmail[] = $this->getBilltoEmailAddress();
				}

				if( false == is_null( $arrstrResidentPaymentNotificationEmail ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrResidentPaymentNotificationEmail ) ) {
					if( true == $this->getIsTranslateEmail() ) {
						$strResidentPreferredLocaleCode = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomerPreferredLocaleCodeByIdByCid( $this->getCustomerId(), $this->getCid(), $objClientDatabase );

						if( false == valStr( $strResidentPreferredLocaleCode ) ) {
							$strResidentPreferredLocaleCode = ( true == valObj( $objProperty, 'CProperty' ) && true == valStr( $objProperty->getLocaleCode() ) ) ? $objProperty->getLocaleCode() : \CLanguage::ENGLISH;
						}

						try {
							\CLocaleContainer::createService()->setPreferredLocaleCode( $strResidentPreferredLocaleCode, $objClientDatabase, $objProperty );
							$strHtmlResidentEmailOutput = $this->createDeleteNotificationSystemEmail( $objClientDatabase, $strMessage, true, $objWebsite, $boolIsResidentPortalPayment, $boolIsResidentPortalPremiumPayment );
							\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
						} catch( Exception $objException ) {
							\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
						}
					} elseif( false == $this->getIsTranslateEmail() ) {
						$strHtmlResidentEmailOutput = $this->createDeleteNotificationSystemEmail( $objClientDatabase, $strMessage, true, $objWebsite, $boolIsResidentPortalPayment, $boolIsResidentPortalPremiumPayment );
					}
				}

				if( false == is_null( $arrstrManagerPaymentNotificationEmails ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrManagerPaymentNotificationEmails ) ) {
					if( true == $this->getIsTranslateEmail() ) {
						$strPropertyPreferredLocaleCode = ( true == valObj( $objProperty, 'CProperty' ) && true == valStr( $objProperty->getLocaleCode() ) ) ? $objProperty->getLocaleCode() : \CLanguage::ENGLISH;

						try {
							\CLocaleContainer::createService()->setPreferredLocaleCode( $strPropertyPreferredLocaleCode, $objClientDatabase, $objProperty );

							$strHtmlManagerEmailOutput = $this->createDeleteNotificationSystemEmail( $objClientDatabase, $strMessage, false, $objWebsite, $boolIsResidentPortalPayment, $boolIsResidentPortalPremiumPayment );

							\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
						} catch( Exception $objException ) {
							\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
						}
					} else if( false == $this->getIsTranslateEmail() ) {
						$strHtmlManagerEmailOutput = $this->createDeleteNotificationSystemEmail( $objClientDatabase, $strMessage, false, $objWebsite, $boolIsResidentPortalPayment, $boolIsResidentPortalPremiumPayment );
					}
				}

				$objSystemEmail = new CSystemEmail();

				$strEmailSubject = __( 'Deleted Recurring Payment - ' );

				if( true == $boolIsResidentPortalPremiumPayment ) {
					$strEmailSubject = __( 'Deleted Auto Payment - ' );
				}

				$strSubject = $strEmailSubject . $this->getBilltoNameFirst() . ' ' . $this->getBilltoNameLast();

				// Set Subject at resident level in case of translation
				if( true == $this->getIsTranslateEmail() ) {
					try {
						\CLocaleContainer::createService()->setPreferredLocaleCode( $strResidentPreferredLocaleCode, $objClientDatabase, $objProperty );

						$strEmailSubject = __( 'Deleted Recurring Payment - ' );

						if( true == $boolIsResidentPortalPremiumPayment ) {
							$strEmailSubject = __( 'Deleted Auto Payment - ' );
						}

						$strSubject = $strEmailSubject . $this->getBilltoNameFirst() . ' ' . $this->getBilltoNameLast();

						\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
					} catch( Exception $objException ) {
						\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
					}
				}

				$objSystemEmail->setSubject( $strSubject );
				$objSystemEmail->setHtmlContent( $strHtmlResidentEmailOutput );
				$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::DELETE_SCHEDULED_PAYMENT_NOTIFICATION );
				$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::URGENT );
				$objSystemEmail->setCid( $this->m_intCid );
				$objSystemEmail->setPropertyId( $this->m_intPropertyId );
				$objSystemEmail->setCustomerId( $this->getCustomerId() );

				$arrobjSystemEmails = [];

				if( true == valArr( $arrstrResidentPaymentNotificationEmail ) ) {
					$objSystemEmail->setCompanyEmployeeId( NULL );

					foreach( $arrstrResidentPaymentNotificationEmail as $strResidentPaymentNotificationEmail ) {
						$objSystemEmail->setToEmailAddress( $strResidentPaymentNotificationEmail );

						if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'FROM_PAYMENT_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_PAYMENT_NOTIFICATION_EMAIL']->getValue() ) ) {
							$objSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_PAYMENT_NOTIFICATION_EMAIL']->getValue() );
						}

						$arrobjSystemEmails[] = clone $objSystemEmail;
					}
				}

				// Set Subject at property level in case of translation
				if( true == $this->getIsTranslateEmail() ) {
					try {
						\CLocaleContainer::createService()->setPreferredLocaleCode( $strPropertyPreferredLocaleCode, $objClientDatabase, $objProperty );

						$strEmailSubject = __( 'Deleted Recurring Payment - ' );

						if( true == $boolIsResidentPortalPremiumPayment ) {
							$strEmailSubject = __( 'Deleted Auto Payment - ' );
						}

						$strSubject = $strEmailSubject . $this->getBilltoNameFirst() . ' ' . $this->getBilltoNameLast();
						$objSystemEmail->setSubject( $strSubject );

						\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
					} catch( Exception $objException ) {
						\CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );
					}
				}

				$objSystemEmail->setHtmlContent( $strHtmlManagerEmailOutput );

				if( true == valArr( $arrstrManagerPaymentNotificationEmails ) ) {
					$objSystemEmail->setCustomerId( NULL );
					$objSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

					foreach( $arrstrManagerPaymentNotificationEmails as $strManagerPaymentNotificationEmail ) {
						$objSystemEmail->setToEmailAddress( $strManagerPaymentNotificationEmail );

						if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) {
							$objSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() );
						}

						$arrobjSystemEmails[] = clone $objSystemEmail;
					}
				}

				$boolRecurringPaymentDeleteEmailSent = false;
				if( true == valArr( $arrobjSystemEmails ) ) {
					$boolRecurringPaymentDeleteEmailSent = true;

					if( true == valObj( $objPropertyEmailRule, 'CPropertyEmailRule' ) ) {
						$arrobjSystemEmails = CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjSystemEmails, $objPropertyEmailRule );
					}

					if( true == valObj( $objEmailDatabase, 'CDatabase' ) ) {
						$this->m_objEmailDatabase = $objEmailDatabase;
					} else {
						$this->m_objEmailDatabase = $this->loadEmailDatabase();
					}

					if( false == $this->m_objEmailDatabase->getIsOpen() ) {
						$this->m_objEmailDatabase->open();
					}

					if( true == valArr( $arrobjSystemEmails ) ) {
						foreach( $arrobjSystemEmails as $objSystemEmail ) {
							if( false == $objSystemEmail->validate( VALIDATE_INSERT ) || false == $objSystemEmail->insert( SYSTEM_USER_ID, $this->m_objEmailDatabase ) ) {
								$boolRecurringPaymentDeleteEmailSent = false;
								trigger_error( 'Scheduled payment deletion email failed to insert.', E_USER_WARNING );
							}
						}
					} else {
						$boolRecurringPaymentDeleteEmailSent = false;
					}
					if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
						$this->m_objEmailDatabase->close();
					}
				}
			}

			// if the email was sent successfully, or emails are not sent at all, log the activity.
			if( true == $boolStoreActivityLog ) {
				if( true == $boolRecurringPaymentDeleteEmailSent || false == $boolSendEmails ) {
					$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );

					if( true == valObj( $objCustomer, 'CCustomer' ) ) {
						$objCustomer->logCustomerActivity( CEventType::RECURRING_PAYMENT, $this->getPropertyId(), $intCompanyUserId = SYSTEM_USER_ID, $this, $strPaymentAccessType = 'deleted', $objClientDatabase );
					}
				}
			}

			return $boolIsValid;
		}
	}

	public function sqlPropertyId() {
		return( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function createDeleteNotificationSystemEmail( $objClientDatabase, $strMessage = NULL, $boolIsResident = NULL, $objWebsite = NULL, $boolIsResidentPortalPayment = false, $boolIsResidentPortalPremiumPayment = true ) {

		$this->generateBlankSystemEmail( $objClientDatabase, $strMessage );

		if( false == is_null( $boolIsResident ) && true == $boolIsResident ) {
			$this->m_objSmarty->assign( 'is_resident', $boolIsResident );
		}

		if( true == $boolIsResidentPortalPayment ) {
			$strResidentPortalUrl = $this->getResidentPortalUrl( $this->getPropertyId(), $this->getCid(), $objClientDatabase, $objWebsite );
			$this->m_objSmarty->assign( 'resident_portal_url', $strResidentPortalUrl );
		}

		$objPropertyEmailRule = CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::DELETE_SCHEDULED_PAYMENT_NOTIFICATION, $this->getPropertyId(), $objClientDatabase );

		$strCustomerFullName = '';
		if( true == $this->getIsTranslateEmail() ) {
			$strCustomerFullName = CResidentProcesses::fetchCustomerFullNameWithMaternalLastName( $this->getCustomerId(), $this->getCid(), $objClientDatabase );
		}

		$this->m_objSmarty->assign( 'is_from_payment_account_deletion',		( false == $this->getIsConnectedPaymentAccountExist() ) );
		$this->m_objSmarty->assign( 'is_resident_portal_premium_payment',	$boolIsResidentPortalPremiumPayment );
		$this->m_objSmarty->assign( 'property_email_rule',					$objPropertyEmailRule );
		$this->m_objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', 			CONFIG_COMPANY_NAME_FULL );
		$this->m_objSmarty->assign( 'image_url',	                        CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$this->m_objSmarty->assign( 'customer_full_name', $strCustomerFullName );

		$this->m_objLease = $this->getOrFetchLease( $objClientDatabase );

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {

			$strBuildingName = \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchSimpleBuildingNameByPropertyIdByLeaseIdByCustomerIdByCid( $this->getPropertyId(), $this->getLeaseId(), $this->getCustomerId(), $this->getCid(), $objClientDatabase );
			$this->m_objLease->setBuildingName( $strBuildingName );
		}

		$strHtmlEmailOutput = $this->m_objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/scheduled_payments/scheduled_payment_delete_notification.tpl' );

		return $strHtmlEmailOutput;
	}

	public function createConvFeeNotificationSystemEmail( $fltOldConvFee, $fltNewConvFee, $strResidentEmailAddress, $objSystemEmailRecipient, $objClientDatabase ) {
		$strSubject = __( 'Auto payment paused' );
		$objProperty = $this->getOrFetchProperty( $objClientDatabase );
		$objClient = $this->getOrFetchClient( $objClientDatabase );
		$objLease = $this->getOrFetchLease( $objClientDatabase );

		$strUnitNumber = ( true == valObj( $objLease, 'CLease' ) && true == valStr( $objLease->getUnitNumberCache() ) ) ? $objLease->getUnitNumberCache() : ' - ';
		$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( \CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
		$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( \CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		$objPropertyAddress = $objProperty->fetchPrimaryAddress( $objClientDatabase );
		$arrmixPropertyPhoneNumbers = $objProperty->fetchSimplePropertyPhoneNumbersByPropertyId( $objClientDatabase );
		$arrmixPropertyPhoneNumbers = rekeyArray( 'phone_number_type_id', $arrmixPropertyPhoneNumbers );

		$strResidentName = CResidentProcesses::fetchCustomerFullNameWithMaternalLastName( $this->getCustomerId(), $this->getCid(), $objClientDatabase );
		if( false == valStr( $strResidentName ) ) {
			$strResidentName = $this->getBilltoNameFirst() . ' ' . $this->getBilltoNameLast();
		}

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );

		$objSmarty->assign( 'base_url', 'http://' . $objClient->getRwxDomain() . \CConfig::get( 'rwx_login_suffix' ) . '/' );
		$objSmarty->assign( 'media_library_url', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'client', $objClient );
		$objSmarty->assign( 'scheduled_payment', $this );
		$objSmarty->assign( 'property', $objProperty );
		$objSmarty->assign( 'property_address', $objPropertyAddress );
		$objSmarty->assign( 'property_phone_numbers', $arrmixPropertyPhoneNumbers );
		$objSmarty->assign( 'is_mobile_browser', determineUserAgent() );
		$objSmarty->assign( 'resident_name', $strResidentName );
		$objSmarty->assign( 'resident_email_address', $strResidentEmailAddress );
		$objSmarty->assign( 'unit_number', $strUnitNumber );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$objSmarty->assign( 'marketing_media_association', $objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file', $objLogoMarketingMediaAssociation );
		$objSmarty->assign( 'CONFIG_COMMON_PATH', CONFIG_COMMON_PATH );
		$objSmarty->assign( 'payment_type', CPaymentTypes::paymentTypeIdToStr( $this->getPaymentTypeId() ) );

		$strLastFourDigitsOfCC = '';
		if( true == valStr( $this->getCcCardNumberMasked() ) ) {
			$strLastFourDigitsOfCC = \Psi\CStringService::singleton()->substr( $this->getCcCardNumberMasked(), - 4 );
		}
		$objSmarty->assign( 'last_4_digits_of_cc', $strLastFourDigitsOfCC );

		$strPaymentStartDaySuffix  = date( 'S', strtotime( $this->getPaymentStartDate() ) );
		$objSmarty->assign( 'payment_start_day_suffix', $strPaymentStartDaySuffix );
		$strPaymentStartDay = date( 'd', strtotime( $this->getPaymentStartDate() ) );
		$objSmarty->assign( 'payment_start_day', __( '{%d, 0, DATE_NUMERIC_DAY}', [ $strPaymentStartDay ] ) );

		$strPaymentEndDateTimeStamp = strtotime( $this->getPaymentEndDate() );
		if( false == is_null( $this->getPaymentEndDate() ) ) {
			$strPaymentEndDate = __( '{%t, 0, DATE_ALPHA_MONTH} {%t, 0, DATE_NUMERIC_DAY}{%s, 1} {%t, 0, DATE_NUMERIC_YEAR}', [ $strPaymentEndDateTimeStamp, date( 'S', $strPaymentEndDateTimeStamp ) ] );
		} else {
			$strPaymentEndDate = __( self::STR_UNTIL_I_CANCEL_OR_MOVE_OUT );
		}

		$objSmarty->assign( 'payment_end_date', $strPaymentEndDate );

		$objSmarty->assign( 'old_conv_fee', ( float ) $fltOldConvFee );
		$objSmarty->assign( 'new_conv_fee', ( float ) $fltNewConvFee );

		$strRp40SubDomain = $objProperty->getRp40SubDomain( $objClientDatabase );
		if( true == valStr( $strRp40SubDomain ) ) {
			$objSmarty->assign( 'is_rp_40_enabled', true );
			$strSecureHostPrefix        = ( \CConfig::get( 'secure_host_prefix' ) ? : 'https://' );
			$strResidentPortalSuffix    = ( \CConfig::get( 'resident_portal_suffix' ) ? : '.residentportal.com' );
			$strResidentPortalUrl		= $strSecureHostPrefix . $strRp40SubDomain . $strResidentPortalSuffix;
		} else {
			$objSmarty->assign( 'is_rp_40_enabled', false );
			$strResidentPortalUrl       = $this->getResidentPortalUrl( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		}
		$objSmarty->assign( 'resident_portal_url', $strResidentPortalUrl );
		// property emails
		if( true == valObj( $objSystemEmailRecipient, 'CSystemEmailRecipient' ) ) {
			// Open in Browser link
			$strViewInBrowserLink = $this->processViewInBrowser( $objSystemEmailRecipient );
			if( true == valStr( $strViewInBrowserLink ) ) {
				$objSmarty->assign( 'open_in_browser', $strViewInBrowserLink );
			}
			// END Open in Browser link
		}

		$objSystemEmail = $objClient->createSystemEmail( CSystemEmailType::SCHEDULED_PAYMENT_CONFIRMATION );
		$objSystemEmail->setSubject( $strSubject );

		$objSystemEmail->setHtmlContent( $objSmarty->fetch( 'system_emails/scheduled_payments/scheduled_payment_conv_fee_change_notification.tpl' ) );

		$objSystemEmail->setToEmailAddress( $strResidentEmailAddress );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		return $objSystemEmail;
	}

	public function processViewInBrowser( $objSystemEmailRecipient ) {
		if( true == is_numeric( $objSystemEmailRecipient->getId() ) ) {
			if( 'production' == CConfig::get( 'environment' ) ) {
				$strBaseUrl = CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN;
			} else {
				$strBaseUrl = CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN;
			}

			$strViewInBrowserModule = $strBaseUrl . '/?module=scheduled_task_email&action=preview_email_content&key=';
			$strViewInBrowserParams = '&system_email_recipient_id=' . $objSystemEmailRecipient->getId();

			return $strViewInBrowserLink = $strViewInBrowserModule . urlencode( base64_encode( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strViewInBrowserParams, CONFIG_SODIUM_KEY_MESSAGE_CENTER_EMAIL ) ) );
		}
	}

	public function createWarningSystemEmail( $objClientDatabase, $strMessage = NULL, $boolIsDisallowLastMonthPayment = false, $boolIsLastRecurringPaymentWarningEmail = false ) {
		$this->generateBlankSystemEmail( $objClientDatabase, $strMessage );

		$boolIsReadyToProcess  = true;

		$objLease = $this->getOrFetchLease( $objClientDatabase );

		$strLeaseEndDate = NULL;
		if( true == $boolIsDisallowLastMonthPayment && true == valObj( $objLease, 'CLease' ) ) {
			$strLeaseEndDate = ( false == is_null( $objLease->getMoveOutDate() ) ) ? $objLease->getMoveOutDate() : $objLease->getLeaseEndDate();
		}

		$intCurrentDate = strtotime( date( 'm/d/Y' ) );
		$intTommorowDate  = strtotime( '+1 day', $intCurrentDate );

		if( NULL != $strLeaseEndDate && 0 < strlen( $strLeaseEndDate ) && ( ( date( 'Y' ) > \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) || ( date( 'Y' ) == \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 6, 4 ) && date( 'm' ) >= \Psi\CStringService::singleton()->substr( $strLeaseEndDate, 0, 2 ) ) ) )
		    || ( date( 'm', $intTommorowDate ) == date( 'm', strtotime( $strLeaseEndDate ) ) && date( 'Y', $intTommorowDate ) == date( 'Y', strtotime( $strLeaseEndDate ) ) ) ) {
			$boolIsReadyToProcess  = false;
		}

		if( true == $boolIsDisallowLastMonthPayment && true == valObj( $objLease, 'CLease' ) && CLeaseIntervalType::MONTH_TO_MONTH == $objLease->getLeaseIntervalTypeId() && ( CLeaseStatusType::CURRENT == $objLease->getLeaseStatusTypeId() || CLeaseStatusType::NOTICE == $objLease->getLeaseStatusTypeId() ) && NULL == $objLease->getMoveOutDate() ) {
			$boolIsReadyToProcess = true;
		}

		$strBuildingName = NULL;

		if( true == valObj( $objLease, 'CLease' ) ) {
			$strBuildingName = \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchSimpleBuildingNameByPropertyIdByLeaseIdByCustomerIdByCid( $this->getPropertyId(), $this->getLeaseId(), $this->getCustomerId(), $this->getCid(), $objClientDatabase );
		}

		// We need to give warning if CC expires before payment end date.
		$boolCCExpireWarning 	= false;
		if( true == in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && true == valId( $this->getCcExpDateMonth() ) && true == valId( $this->getCcExpDateYear() ) ) {

			$intDateCCExpireDate			= strtotime( $this->getCcExpDateMonth() . '/' . date( 'd', strtotime( date( 'm/d/Y' ) ) ) . '/' . $this->getCcExpDateYear() );
			$intDate						= mktime( 0, 0, 0, $this->getCcExpDateMonth(), 1, $this->getCcExpDateYear() );
			$intDateCCExpireMonthEndDate	= strtotime( date( 'm/d/Y', strtotime( '+1 month', $intDate ) ) );

			$intDate 						= mktime( 0, 0, 0, date( 'm' ), date( 'd' ), date( 'Y' ) );
			$intDateSecondMonth 			= strtotime( date( 'm/d/Y', strtotime( '+2 month', $intDate ) ) );

			if( $intDateCCExpireDate <= $intDateSecondMonth ) {
				if( false == is_null( $this->getPaymentEndDate() ) ) {
					if( strtotime( $this->getPaymentEndDate() ) >= $intDateCCExpireMonthEndDate ) {
						$boolCCExpireWarning = true;
					}
				} else {
					$boolCCExpireWarning = true;
				}
			}
		}

		$boolIsPaymentBlocked = false;

		$objLease		= $this->getOrFetchLease( $objClientDatabase );
		$objCustomer	= $this->getOrFetchCustomer( $objClientDatabase );

		if( true == valObj( $objLease, 'CLease' ) && true == in_array( $objLease->getPaymentAllowanceTypeId(), [ CPaymentAllowanceType::BLOCK_ALL, CPaymentAllowanceType::CASH_EQUIVALENT ] ) ) {
			if( CPaymentAllowanceType::BLOCK_ALL == $objLease->getPaymentAllowanceTypeId() ) {
				$boolIsPaymentBlocked = true;
			} elseif( false == CPaymentTypes::isCreditCardPayment( $this->getPaymentTypeId() ) ) {
				$boolIsPaymentBlocked = true;
			}
		}

		if( false == $boolIsPaymentBlocked && true == valObj( $objCustomer, 'CCustomer' ) && true == in_array( $objCustomer->getPaymentAllowanceTypeId(), [ CPaymentAllowanceType::BLOCK_ALL, CPaymentAllowanceType::CASH_EQUIVALENT ] ) ) {
			if( CPaymentAllowanceType::BLOCK_ALL == $objCustomer->getPaymentAllowanceTypeId() ) {
				$boolIsPaymentBlocked = true;
			} elseif( false == CPaymentTypes::isCreditCardPayment( $this->getPaymentTypeId() ) ) {
				$boolIsPaymentBlocked = true;
			}
		}

		$boolIsSepaDirectDebit = false;

		if( \CPaymentType::SEPA_DIRECT_DEBIT == $this->getPaymentTypeId() ) {
			$boolIsSepaDirectDebit = true;
			if( true == valObj( $this->m_objScheduledPaymentDetail, \CScheduledPaymentDetail::class ) ) {
				if( true == valId( $this->m_objScheduledPaymentDetail->getPaymentBankAccountId() ) ) {
					$intPaymentBankAccountId = $this->m_objScheduledPaymentDetail->getPaymentBankAccountId();
					$this->m_objSmarty->assign( 'payment_bank_account_id', $intPaymentBankAccountId );
				}
			}
			/** @var \Psi\Core\Payment\BankAccountService\Types\CSepa $objSepaPaymentBankAccount */
			$objSepaPaymentBankAccount = $this->getPaymentBankAccount();
			if( true == valObj( $objSepaPaymentBankAccount, \CPaymentBankAccount::class ) ) {
				$intCreditorId = $objSepaPaymentBankAccount->getMandateCreditorId();
				$this->m_objSmarty->assign( 'creditor_id', $intCreditorId );
			}

			$strScheduledPaymentProcessDate = date( 'm/d/Y', $intTommorowDate );
			if( true == valStr( $strScheduledPaymentProcessDate ) ) {
				$this->m_objSmarty->assign( 'scheduled_payment_process_date', $strScheduledPaymentProcessDate );
			}
		}

		$objPropertyAddress = $this->m_objProperty->fetchPrimaryAddress( $objClientDatabase );
		$arrmixPropertyPhoneNumbers = $this->m_objProperty->fetchSimplePropertyPhoneNumbersByPropertyId( $objClientDatabase );
		$arrmixPropertyPhoneNumbers = rekeyArray( 'phone_number_type_id', $arrmixPropertyPhoneNumbers );

		if( true == valObj( $objCustomer, \CCustomer::class ) && false == is_null( $objCustomer->getUsername() ) ) {
			$this->m_objSystemEmail->setToEmailAddress( $objCustomer->getUsername() );
		} else {
			$this->m_objSystemEmail->setToEmailAddress( $this->getBilltoEmailAddress() );
		}

		$objPropertyEmailRule = CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::SCHEDULED_PAYMENT_WARNING, $this->getPropertyId(), $objClientDatabase );
		$strCustomerFullName = CResidentProcesses::fetchCustomerFullNameWithMaternalLastName( $objCustomer->getId(), $objCustomer->getCid(), $objClientDatabase );
		$strCustomerFirstname = '';
		if( true == valStr( $strCustomerFullName ) ) {
			list( $strCustomerFirstname ) = explode( ' ', $strCustomerFullName );
		}

		$this->m_objSmarty->assign( 'customer_full_name',    $strCustomerFullName );
		$this->m_objSmarty->assign( 'customer_first_name',    $strCustomerFirstname );
		$this->m_objSmarty->assign( 'property_email_rule', 		$objPropertyEmailRule );
		$this->m_objSmarty->assign( 'is_payment_blocked',	    $boolIsPaymentBlocked );
		$this->m_objSmarty->assign( 'is_ready_to_process',	    $boolIsReadyToProcess );
		$this->m_objSmarty->assign( 'building_name',			$strBuildingName );
		$this->m_objSmarty->assign( 'cc_expire_warning',		$boolCCExpireWarning );
		$this->m_objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$this->m_objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$this->m_objSmarty->assign( 'is_sepa_direct_debit',	    $boolIsSepaDirectDebit );
		$this->m_objSmarty->assign( 'property_address', 		$objPropertyAddress );
		$this->m_objSmarty->assign( 'property_phone_numbers', 	$arrmixPropertyPhoneNumbers );

		if( true == valObj( $this->m_objEmailDatabase, 'CDatabase' ) ) {
			$objEmailDatabase = $this->m_objEmailDatabase;
		} else {
			$objEmailDatabase = $this->loadEmailDatabase();
		}

		if( true == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			$objEmailDatabase->open();
			$objSystemEmailRecipient = new CSystemEmailRecipient();
			$objSystemEmailRecipient->setId( $objSystemEmailRecipient->fetchNextId( $objEmailDatabase ) );

			if( true == valObj( $objSystemEmailRecipient, 'CSystemEmailRecipient' ) ) {
				// Open in Browser link
				$arrmixMergeFields = [];
				$strViewInBrowserLink = $objSystemEmailRecipient->processViewInBrowser( $arrmixMergeFields, true );
				if( true == valStr( $strViewInBrowserLink ) ) {
					$this->m_objSmarty->assign( 'open_in_browser', $strViewInBrowserLink );
				}
				// END Open in Browser link
			}

			$objEmailDatabase->close();

		}

		$strHtmlEmailOutput = $this->m_objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/scheduled_payments/scheduled_payment_post_warning.tpl' );

		if( true == $boolIsLastRecurringPaymentWarningEmail ) {
			$this->m_objSystemEmail->setSubject( __( 'Recurring Payment Reminder (last payment)' ) );
		} else {
			$this->m_objSystemEmail->setSubject( __( 'Recurring Payment Reminder' ) );
		}

		$this->m_objSystemEmail->setHtmlContent( $strHtmlEmailOutput );

		return $this->m_objSystemEmail;
	}

	public function generateBlankSystemEmail( $objClientDatabase, $strMessage = NULL ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objClient 		= $this->getOrFetchClient( $objClientDatabase );
		$objProperty	= $this->getOrFetchProperty( $objClientDatabase );
		$objLease 		= $this->getOrFetchLease( $objClientDatabase );
		// TODO:
		// Load this from view instead of table
		$objCustomer 			= $this->getOrFetchCustomer( $objClientDatabase );
		$objPaymentType			= $this->fetchPaymentType( $objClientDatabase );

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
			$arrobjNotifications 	= $objProperty->fetchPropertyNotificationsByKeys( [ 'RECURRING_PAYMENT_WARNING_CUSTOM_TEXT', 'RECURRING_PAYMENT_DELETE_CUSTOM_TEXT' ], $objClientDatabase );
			if( false == valObj( $this->m_objProperty, CProperty::class ) ) {
				$this->m_objProperty = $objProperty;
			}

			$objRecurringPaymentWarningCustomTextPropertyNotification 	= NULL;
			$objRecurringPaymentDeleteCustomTextPropertyNotification	= NULL;

			if( true == valArr( $arrobjNotifications ) ) {
				if( true == array_key_exists( 'RECURRING_PAYMENT_WARNING_CUSTOM_TEXT', $arrobjNotifications ) ) {
					$objRecurringPaymentWarningCustomTextPropertyNotification	= $arrobjNotifications['RECURRING_PAYMENT_WARNING_CUSTOM_TEXT'];
				}

				if( true == array_key_exists( 'RECURRING_PAYMENT_DELETE_CUSTOM_TEXT', $arrobjNotifications ) ) {
					$objRecurringPaymentDeleteCustomTextPropertyNotification	= $arrobjNotifications['RECURRING_PAYMENT_DELETE_CUSTOM_TEXT'];
				}
			}
		}

		if( true == valObj( $objClient, 'CClient' ) ) {

			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		}

		$this->m_objSystemEmail = new CSystemEmail();
		$this->m_objSystemEmail->setDefaults();

		$this->m_objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$this->m_objSmarty->assign( 'scheduled_payment', 	$this );
		$this->m_objSmarty->assign( 'client', 	$objClient );
		$this->m_objSmarty->assign( 'lease', 				$objLease );
		$this->m_objSmarty->assign( 'customer', 				$objCustomer );
		$this->m_objSmarty->assign( 'property', 				$objProperty );
		$this->m_objSmarty->assign( 'payment_type', 			$objPaymentType );
		$this->m_objSmarty->assign( 'message', 				$strMessage );
		$this->m_objSmarty->assign( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$this->m_objSmarty->assign( 'default_company_media_file', 	$objLogoMarketingMediaAssociation );

		$this->m_objSmarty->assign( 'recurring_payment_warning_custom_text_property_notification', 	$objRecurringPaymentWarningCustomTextPropertyNotification );
		$this->m_objSmarty->assign( 'recurring_payment_delete_custom_text_property_notification', 	$objRecurringPaymentDeleteCustomTextPropertyNotification );

		$this->m_objSmarty->assign( 'terms_and_condition_path', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/Common/legal/resident_center_terms_conditions.html' );
		$this->m_objSmarty->assign( 'server_name', 				CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$this->m_objSmarty->assign( 'media_library_uri', 		CONFIG_MEDIA_LIBRARY_PATH );
		// TODO:
		// if username is blanck, check email_address field, on customer table
		// if email address field on customers is blanck then use billto email address from scheduled payment
		if( true == valObj( $objCustomer, 'CCustomer' ) && false == is_null( $objCustomer->getUsername() ) ) {
			$this->m_objSystemEmail->setToEmailAddress( $objCustomer->getUsername() );
		} else {
			$this->m_objSystemEmail->setToEmailAddress( $this->getBilltoEmailAddress() );
		}

		$this->m_objSystemEmail->setSystemEmailTypeId( CSystemEmailType::SCHEDULED_PAYMENT_WARNING );
		$this->m_objSystemEmail->setPropertyId( $this->getPropertyId() );
		$this->m_objSystemEmail->setCid( $this->getCid() );
		$this->m_objSystemEmail->setCustomerId( $this->getCustomerId() );
		$this->m_objSystemEmail->setScheduledSendDatetime( date( 'm/d/Y H:i:s' ) );
	}

	public function sendGenericScheduledPaymentEmail( $arrstrEmailAddresses, $strSubject = NULL, $strMessage = NULL, $boolIsResident = NULL, $objClientDatabase, $boolIsDeleteEmail = false, $boolIsFromBlockDayOrNotAllowedCase = false ) {
		if( false == valArr( $arrstrEmailAddresses ) ) {
			trigger_error( 'No email addresses were sent through to the sendGenericScheduledPaymentEmail() function. Notification emails were sent to' . CSystemEmail::DEVSUPPORT_EMAIL_ADDRESS . '. These emails must be found and forwarded to the client.', E_USER_WARNING );
			$arrstrEmailAddresses = [ CSystemEmail::DEVSUPPORT_EMAIL_ADDRESS ];
		}

		if( false == isset( $strSubject ) ) {
			trigger_error( 'Subject was not sent through to sendGenericScheduledPaymentEmail() function.', E_USER_WARNING );
			return false;
		}

		if( false == isset( $strMessage ) ) {
			trigger_error( 'Message was not sent through to sendGenericScheduledPaymentEmail() function.', E_USER_WARNING );
			return false;
		}

		$arrobjPaymentTypes = CPaymentTypes::fetchAllPaymentTypes( $objClientDatabase );

		$objClient            = $this->getOrFetchClient( $objClientDatabase );
		$objProperty          = $this->getOrFetchProperty( $objClientDatabase );
		$objLease             = $this->getOrFetchLease( $objClientDatabase );
		$objPropertyEmailRule = NULL;
		$strUnitNumberCache   = NULL;
		$strPropertyName      = NULL;
		$strBuildingName      = NULL;

		$intSystemEmailTypeId = ( true == $boolIsDeleteEmail ) ? CSystemEmailType::DELETE_SCHEDULED_PAYMENT_NOTIFICATION : CSystemEmailType::SCHEDULED_PAYMENT_WARNING;

		if( true == valObj( $objLease, 'CLease' ) ) {
			$strBuildingName = \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchSimpleBuildingNameByPropertyIdByLeaseIdByCustomerIdByCid( $this->getPropertyId(), $this->getLeaseId(), $this->getCustomerId(), $this->getCid(), $objClientDatabase );
			$objLease->setBuildingName( $strBuildingName );
			$strUnitNumberCache = $objLease->getUnitNumberCache();
		}

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
			$strPropertyName = $objProperty->getPropertyName();
		}

		$objPropertyEmailRule = CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( $intSystemEmailTypeId, $this->getPropertyId(), $objClientDatabase );

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		}

		$objSystemEmail = new CSystemEmail();
		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );

		$objSmarty->assign( 'scheduled_payment', $this );
		$objSmarty->assign( 'marketing_media_association', $objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file', $objLogoMarketingMediaAssociation );
		$objSmarty->assign( 'payment_types', $arrobjPaymentTypes );
		$objSmarty->assign( 'media_library_uri', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'terms_and_condition_path', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/Common/legal/resident_center_terms_conditions.html' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'property_email_rule', $objPropertyEmailRule );
		$objSmarty->assign( 'subject', $strSubject );
		$objSmarty->assign( 'image_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		if( true == $boolIsFromBlockDayOrNotAllowedCase ) {
			$strCustomerFullName = CResidentProcesses::fetchCustomerFullNameWithMaternalLastName( $this->getCustomerId(), $this->getCid(), $objClientDatabase );

			$objSmarty->assign( 'customer_full_name', $strCustomerFullName );
			$objSmarty->assign( 'recurring_payment_inner_text', $strMessage );
			$objSmarty->assign( 'email_title', $strSubject );
			$objSmarty->assign( 'SCHEDULED_PAYMENT_FREQUENCY_ONCE', CScheduledPaymentFrequency::ONCE );
			$objSmarty->assign( 'unit_number_cache', $strUnitNumberCache );
			$objSmarty->assign( 'building_name', $strBuildingName );
			$objSmarty->assign( 'property_name', $strPropertyName );

			$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/ar_payments/scheduled_payment_failed_not_allowed_or_blocked_day_notification.tpl' );
			$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		} else {
			if( true == valObj( $objProperty, CProperty::class ) ) {
				$objRecurringPaymentCustomTextPropertyNotification = $objProperty->fetchPropertyNotificationByKey( 'RECURRING_PAYMENT_CUSTOM_TEXT', $objClientDatabase );
			}

			if( true == $boolIsResident ) {
				$strResidentPortalUrl = $this->getResidentPortalUrl( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
				$objSmarty->assign( 'resident_portal_url', $strResidentPortalUrl );
			}

			$objSmarty->assign( 'property', $objProperty );
			$objSmarty->assign( 'message', $strMessage );
			$objSmarty->assign( 'lease', $objLease );
			$objSmarty->assign( 'recurring_payment_custom_text_property_notification', $objRecurringPaymentCustomTextPropertyNotification );

			if( true == $boolIsResident ) {
				$objSmarty->assign( 'is_resident', $boolIsResident );
			}

			$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/ar_payments/scheduled_payment_notification.tpl' );
			$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		}

		$objSystemEmail->setSystemEmailTypeId( $intSystemEmailTypeId );
		$objSystemEmail->setSubject( $strSubject );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::URGENT );
		$objSystemEmail->setCid( $this->m_intCid );
		$objSystemEmail->setPropertyId( $this->m_intPropertyId );

		$arrobjSystemEmails = [];

		if( true == valArr( $arrstrEmailAddresses ) ) {
			foreach( $arrstrEmailAddresses as $strEmailAddress ) {
				$objSystemEmail->setToEmailAddress( $strEmailAddress );
				$arrobjSystemEmails[] = clone $objSystemEmail;
			}
		}

		if( true == valArr( $arrobjSystemEmails ) ) {

			if( true == valObj( $objPropertyEmailRule, 'CPropertyEmailRule' ) ) {
				$arrobjSystemEmails = CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjSystemEmails, $objPropertyEmailRule );
			}

			$this->m_objEmailDatabase = $this->loadEmailDatabase();
			$this->m_objEmailDatabase->open();
			if( true == valArr( $arrobjSystemEmails ) ) {
				foreach( $arrobjSystemEmails as $objSystemEmail ) {
					$strToEmailAddress = trim( $objSystemEmail->getToEmailAddress() );
					if( true == empty( $strToEmailAddress ) ) continue;
					if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $this->m_objEmailDatabase ) ) {
						$this->sendEmail( $objSystemEmail );
						continue;
					}
				}
			}
			$this->m_objEmailDatabase->close();
		}

		return true;
	}

	public function sendScheduledPaymentConfirmationEmail( $strEmailAddress, $strEmailSubject, $strEmailTitle, $objClientDatabase, $objWebsite = NULL, $boolIsResidentPortalPayment = false, $boolIsResidentPortalPremiumPayment = false, $boolIsSendEmailToProperty = false, $boolIsUnitTransfered = false, $strUnitTransferedMoveOutDate = NULL, $objEmailDatabase = NULL ) {
		$objClient				= $this->getOrFetchClient( $objClientDatabase );
		$objProperty			= $this->getOrFetchProperty( $objClientDatabase );
		$arrobjPaymentTypes		= CPaymentTypes::fetchAllPaymentTypes( $objClientDatabase );
		$objPropertyEmailRule	= NULL;

		$fltTotalScheduledPaymentConcessionAmount = 0;

		if( true == valArr( $this->getRecurringPaymentSpecials() ) ) {
			foreach( $this->getRecurringPaymentSpecials() as $objRecurringPaymentSpecial ) {
				if( true == valObj( $objRecurringPaymentSpecial, 'CSpecial' ) ) {
					$fltTotalScheduledPaymentConcessionAmount += $objRecurringPaymentSpecial->getAmount();
				}
			}
		}

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objPropertyEmailRule									= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::SCHEDULED_PAYMENT_CONFIRMATION, $objProperty->getId(), $objClientDatabase );
			$objPropertyPreferenceFromPaymentNotificationEmail		= $objProperty->fetchPropertyPreferenceByKey( 'FROM_PAYMENT_NOTIFICATION_EMAIL',  $objClientDatabase );
			$objMarketingMediaAssociation							= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
			$objRecurringPaymentSetupCustomTextPropertyNotification	= $objProperty->fetchPropertyNotificationByKey( 'RECURRING_PAYMENT_SETUP_CUSTOM_TEXT', $objClientDatabase );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		}

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );
		$strCustomerFullName = '';
		if( true == $boolIsResidentPortalPayment ) {
			$strResidentPortalUrl = $this->getResidentPortalUrl( $this->getPropertyId(), $this->getCid(), $objClientDatabase, $objWebsite );
			$objSmarty->assign( 'resident_portal_url', 	$strResidentPortalUrl );
		}

		if( true == $this->getIsTranslateEmail() ) {
			$strCustomerFullName = CResidentProcesses::fetchCustomerFullNameWithMaternalLastName( $this->getCustomerId(), $this->getCid(), $objClientDatabase );
		}

		$objSmarty->assign( 'customer_full_name', $strCustomerFullName );
		$objSmarty->assign( 'is_resident_portal_premium_payment', 	$boolIsResidentPortalPremiumPayment );
		$objSmarty->assign( 'is_unit_transfered', 					$boolIsUnitTransfered );
		$objSmarty->assign( 'unit_transfered_move_out_date', 		$strUnitTransferedMoveOutDate );

		$strUnitNumberCache = NULL;
		$strBuildingName 	= NULL;

		$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objClientDatabase );

		if( true == valObj( $objLease, 'CLease' ) ) {
			$strUnitNumberCache = $objLease->getUnitNumberCache();
			$strBuildingName = \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchSimpleBuildingNameByPropertyIdByLeaseIdByCustomerIdByCid( $this->getPropertyId(), $this->getLeaseId(), $this->getCustomerId(), $this->getCid(), $objClientDatabase );
		}

		$objSmarty->assign( 'scheduled_payment', 			$this );
		$objSmarty->assign( 'subject', 						$strEmailSubject );
		$objSmarty->assign( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file', 	$objLogoMarketingMediaAssociation );
		$objSmarty->assign( 'payment_types', 				$arrobjPaymentTypes );
		$objSmarty->assign( 'unit_number_cache',			$strUnitNumberCache );
		$objSmarty->assign( 'building_name',				$strBuildingName );

		$objSmarty->assign( 'scheduled_payment_concession_amount', $fltTotalScheduledPaymentConcessionAmount );

		$objSmarty->assign( 'recurring_payment_setup_custom_text_property_notification', $objRecurringPaymentSetupCustomTextPropertyNotification );

		$objSmarty->assign( 'media_library_uri', 		CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'email_title', 				$strEmailTitle );
		$objSmarty->assign( 'base_uri', 				CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'terms_and_condition_path', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/Common/legal/resident_center_terms_conditions.html' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'property_email_rule',		$objPropertyEmailRule );

		$objSmarty->assign( 'SCHEDULED_PAYMENT_FREQUENCY_ONCE', CScheduledPaymentFrequency::ONCE );

		if( \CPaymentType::PAD == $this->getPaymentTypeId() ) {
			$strPropertyName = '';
			if( true == valObj( $objProperty, \CProperty::class ) ) {
				$strPropertyName = $objProperty->getPropertyName();
				$objPrimaryPropertyAddress = $objProperty->getOrFetchPrimaryPropertyAddress( $objClientDatabase );
				$objPrimaryPropertyPhoneNumber = $objProperty->getOrFetchPrimaryPropertyPhoneNumber( $objClientDatabase );
				$objSmarty->assign( 'property_address', $objPrimaryPropertyAddress );

				$strPhoneNumber = '';
				if( true == \valObj( $objPrimaryPropertyPhoneNumber, \CPropertyPhoneNumber::class ) ) {
					$strPhoneNumber = $objPrimaryPropertyPhoneNumber->getPhoneNumber();
				}
				$objSmarty->assign( 'property_phone_number', $strPhoneNumber );
			}
			$arrstrCustomerName = explode( ' ', $strCustomerFullName );
			$strResidentFirstName = ( true == isset( $arrstrCustomerName[0] ) ) ? $arrstrCustomerName[0] : '';
			$strResidentLastName = ( true == isset( $arrstrCustomerName[1] ) ) ? $arrstrCustomerName[1] : '';
			$strAccountTypeName = \CCheckAccountType::createService()->getCheckAccountTypeNameById( $this->getCheckAccountTypeId() );
			$objSmarty->assign( 'resident_first_name', $strResidentFirstName );
			$objSmarty->assign( 'resident_last_name', $strResidentLastName );
			$objSmarty->assign( 'pad_account_type', $strAccountTypeName );
			$objSmarty->assign( 'property_name', $strPropertyName );
			$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/ar_payments/pad_scheduled_payment_confirmation.tpl' );
		} else {
			$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/ar_payments/scheduled_payment_confirmation.tpl' );
		}

		$boolIsValid = true;

		switch( NULL ) {
			default:
				$objScheduledPaymentConfirmationEmail = new CSystemEmail();
				$objScheduledPaymentConfirmationEmail->setDefaults();
				$objScheduledPaymentConfirmationEmail->setSystemEmailTypeId( CSystemEmailType::SCHEDULED_PAYMENT_CONFIRMATION );
				$objScheduledPaymentConfirmationEmail->setToEmailAddress( $strEmailAddress );
				$objScheduledPaymentConfirmationEmail->setSubject( $strEmailSubject );
				$objScheduledPaymentConfirmationEmail->setHtmlContent( $strHtmlEmailOutput );
				$objScheduledPaymentConfirmationEmail->setCid( $this->getCid() );
				$objScheduledPaymentConfirmationEmail->setPropertyId( $this->getPropertyId() );

				if( true == isset( $objPropertyPreferenceFromPaymentNotificationEmail ) && 0 < strlen( $objPropertyPreferenceFromPaymentNotificationEmail->getValue() ) ) {
					$objScheduledPaymentConfirmationEmail->setFromEmailAddress( $objPropertyPreferenceFromPaymentNotificationEmail->getValue() );
				} else {
					$objScheduledPaymentConfirmationEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
				}

				$objScheduledPaymentConfirmationEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

				if( false == $boolIsSendEmailToProperty ) {
					$objScheduledPaymentConfirmationEmail->setCompanyEmployeeId( NULL );
					$objScheduledPaymentConfirmationEmail->setCustomerId( SYSTEM_USER_ID );
				}

				if( false == valObj( CSystemEmailLibrary::processSystemEmailByPropertyEmailRules( $objScheduledPaymentConfirmationEmail, $objPropertyEmailRule ), 'CSystemEmail' ) ) {
					break;
				}

				if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
					$this->m_objEmailDatabase = $this->loadEmailDatabase();
				} else {
					$this->m_objEmailDatabase = $objEmailDatabase;
				}
				if( false == valObj( $this->m_objEmailDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( new \CErrorMsg( NULL, '', 'Payment email notifications failed on scheduled payment.' ) );
				} else {
					if( false == $this->m_objEmailDatabase->getIsOpen() ) {
						$this->m_objEmailDatabase->open();
					}
					if( false == $objScheduledPaymentConfirmationEmail->insert( CUser::ID_SYSTEM, $this->m_objEmailDatabase, false, false ) ) {
						$boolIsValid = false;
						break;
					}
					if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
						$this->m_objEmailDatabase->close();
					}
				}
		}
		return $boolIsValid;
	}

	public function sendScheduledPaymentRoommateConfirmationEmail( $arrstrRoommatesEmailInfo, $strInitiatedBy, $intCustomerId, $strName, $strEmailAddress, $strEmailSubject, $strEmailTitle, $objClientDatabase, $objWebsite = NULL, $boolIsEditedEmail = false, $objEmailDatabase = NULL ) {
		if( false == valStr( $strEmailAddress ) ) return false;

		$objClient						= $this->getOrFetchClient( $objClientDatabase );
		$objProperty					= $this->getOrFetchProperty( $objClientDatabase );
		$arrobjPaymentTypes  			= CPaymentTypes::fetchAllPaymentTypes( $objClientDatabase );
		$objPropertyEmailRule			= NULL;

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		}

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
			$objPropertyEmailRule	= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::SCHEDULED_PAYMENT_CONFIRMATION, $objProperty->getId(), $objClientDatabase );
		}

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );
		$strResidentPortalUrl = $this->getResidentPortalUrl( $this->getPropertyId(), $this->getCid(), $objClientDatabase, $objWebsite );
		$objSmarty->assign( 'resident_portal_url', $strResidentPortalUrl );

		$strStartDateTimeStamp = strtotime( $arrstrRoommatesEmailInfo[$intCustomerId]['payment_start_date'] );
		$strEndDateTimeStamp = strtotime( $arrstrRoommatesEmailInfo[$intCustomerId]['payment_end_date'] );
		$strDaysBeforeDueDateTimeStamp = strtotime( '-7 day', $strStartDateTimeStamp );
		$strSplitRoommateStartMonthTimeStamp = strtotime( $arrstrRoommatesEmailInfo[$intCustomerId]['split_roommate_start_month'] );

		$strSplitRoommateStartMonth = ( true == isset( $arrstrRoommatesEmailInfo[$intCustomerId]['split_roommate_start_month'] ) ) ? $strSplitRoommateStartMonthTimeStamp : $strStartDateTimeStamp;
		$strSplitRoommateEndMonth = ( true == isset( $arrstrRoommatesEmailInfo[$intCustomerId]['payment_end_date'] ) ) ? __( '{%t, 0, DATE_ALPHA_MONTH_FULL} {%t, 0, DATE_NUMERIC_YEAR}', [ $strEndDateTimeStamp ] ) : __( self::STR_UNTIL_I_CANCEL );

		$objSmarty->assign( 'roommates_email_info', 		$arrstrRoommatesEmailInfo );
		$objSmarty->assign( 'customer_id', 					$intCustomerId );
		$objSmarty->assign( 'resident_name', 				$strName );
		$objSmarty->assign( 'initiated_by', 				$strInitiatedBy );
		$objSmarty->assign( 'email_title', 					$strEmailTitle );
		$objSmarty->assign( 'subject', 						$strEmailSubject );
		$objSmarty->assign( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file',	$objLogoMarketingMediaAssociation );

		$objSmarty->assign( '7_days_before_due_date',				__( '{%t, 0, DATE_NUMERIC_DAY}{%s, 1} of {%t, 0, DATE_ALPHA_MONTH_FULL} {%t, 0, DATE_NUMERIC_YEAR}', [ $strDaysBeforeDueDateTimeStamp, date( 'S', $strDaysBeforeDueDateTimeStamp ) ] ) );
		$objSmarty->assign( 'start_day', 							__( '{%t, 0, DATE_NUMERIC_DAY}{%s, 1}', [ $strStartDateTimeStamp, date( 'S', $strStartDateTimeStamp ) ] ) );
		$objSmarty->assign( 'start_month',							__( '{%t, 0, DATE_ALPHA_MONTH_FULL} {%t, 0, DATE_NUMERIC_YEAR}', [ $strSplitRoommateStartMonth ] ) );
		$objSmarty->assign( 'end_month',							$strSplitRoommateEndMonth );
		$objSmarty->assign( 'media_library_uri', 					CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri', 							CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', 			CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'property_email_rule',					$objPropertyEmailRule );
		$objSmarty->assign( 'image_url',	                        CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );
		$objSmarty->assign( 'schduled_edited_email',                $boolIsEditedEmail );

		$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/ar_payments/scheduled_payment_roommate_confirmation.tpl' );
		$boolIsValid = true;

		if( 0 == $arrstrRoommatesEmailInfo[$intCustomerId]['is_initiated'] ) {
			switch( NULL ) {
				default:

					$objScheduledPaymentRoommateConfirmationEmail = new CSystemEmail();
					$objScheduledPaymentRoommateConfirmationEmail->setDefaults();
					$objScheduledPaymentRoommateConfirmationEmail->setSystemEmailTypeId( CSystemEmailType::SCHEDULED_PAYMENT_WARNING );
					$objScheduledPaymentRoommateConfirmationEmail->setToEmailAddress( $strEmailAddress );
					$objScheduledPaymentRoommateConfirmationEmail->setSubject( $strEmailSubject );
					$objScheduledPaymentRoommateConfirmationEmail->setHtmlContent( $strHtmlEmailOutput );
					$objScheduledPaymentRoommateConfirmationEmail->setCid( $this->getCid() );
					$objScheduledPaymentRoommateConfirmationEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
					$objScheduledPaymentRoommateConfirmationEmail->setPropertyId( $this->getPropertyId() );
					$objScheduledPaymentRoommateConfirmationEmail->setCustomerId( $intCustomerId );

					if( false == valObj( CSystemEmailLibrary::processSystemEmailByPropertyEmailRules( $objScheduledPaymentRoommateConfirmationEmail, $objPropertyEmailRule ), 'CSystemEmail' ) ) {
						break;
					}

					if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
						$objEmailDatabase = $this->loadEmailDatabase();
						if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
							$this->addErrorMsg( new \CErrorMsg( NULL, '', 'Payment email notifications failed on scheduled payment.' ) );
						} else {
							$objEmailDatabase->open();
							if( false == $objScheduledPaymentRoommateConfirmationEmail->insert( CUser::ID_SYSTEM, $objEmailDatabase ) ) {
								$boolIsValid = false;
								break;
							}
							$objEmailDatabase->close();
						}
					} else {
						if( false == $objScheduledPaymentRoommateConfirmationEmail->insert( CUser::ID_SYSTEM, $objEmailDatabase ) ) {
							$boolIsValid = false;
							break;
						}

					}
			}
		}

		return $boolIsValid;
	}

	public function sendScheduledPaymentRoommateWarningEmail( $arrstrRoommatesEmailInfo, $strInitiatedBy, $arrstrEmailAddresses, $objClientDatabase, $objWebsite = NULL ) {
		$strEmailSubject = __( 'Split Auto Payment Setup Not Complete' );

		if( false == valArr( $arrstrEmailAddresses ) ) {
			return false;
		}
		$strEmailAddress = implode( ',', $arrstrEmailAddresses );

		$objClient				= $this->getOrFetchClient( $objClientDatabase );
		$objProperty			= $this->getOrFetchProperty( $objClientDatabase );
		$objPropertyEmailRule	= NULL;

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		}

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
			$objPropertyEmailRule	= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::SCHEDULED_PAYMENT_WARNING, $objProperty->getId(), $objClientDatabase );
		}

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );
		$strResidentPortalUrl = $this->getResidentPortalUrl( $this->getPropertyId(), $this->getCid(), $objClientDatabase, $objWebsite );
		$objSmarty->assign( 'resident_portal_url', 	$strResidentPortalUrl );
		$objSmarty->assign( 'count_roommates', 			        \Psi\Libraries\UtilFunctions\count( $arrstrRoommatesEmailInfo ) );
		$objSmarty->assign( 'roommates_email_info', 			$arrstrRoommatesEmailInfo );
		$objSmarty->assign( 'initiated_by', 				    $strInitiatedBy );
		$objSmarty->assign( 'email_addresses', 					$strEmailAddress );
		$objSmarty->assign( 'email_title', 					    $strEmailSubject );
		$objSmarty->assign( 'subject', 						    $strEmailSubject );
		$objSmarty->assign( 'marketing_media_association', 		$objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file',	    $objLogoMarketingMediaAssociation );
		$objSmarty->assign( 'media_library_uri', 				CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri', 						CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', 		CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'property_email_rule',				$objPropertyEmailRule );
		$objSmarty->assign( 'image_url',	                    CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/ar_payments/scheduled_payment_roommate_warning.tpl' );

		$objScheduledPaymentRoommateConfirmationEmail = new CSystemEmail();
		$objScheduledPaymentRoommateConfirmationEmail->setDefaults();
		$objScheduledPaymentRoommateConfirmationEmail->setSystemEmailTypeId( CSystemEmailType::SCHEDULED_PAYMENT_WARNING );
		$objScheduledPaymentRoommateConfirmationEmail->setToEmailAddress( $strEmailAddress );
		$objScheduledPaymentRoommateConfirmationEmail->setSubject( $strEmailSubject );
		$objScheduledPaymentRoommateConfirmationEmail->setHtmlContent( $strHtmlEmailOutput );
		$objScheduledPaymentRoommateConfirmationEmail->setCid( $this->getCid() );
		$objScheduledPaymentRoommateConfirmationEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objScheduledPaymentRoommateConfirmationEmail->setPropertyId( $this->getPropertyId() );

		if( false == valObj( CSystemEmailLibrary::processSystemEmailByPropertyEmailRules( $objScheduledPaymentRoommateConfirmationEmail, $objPropertyEmailRule ), 'CSystemEmail' ) ) {
			return false;
		}

		$objEmailDatabase = $this->loadEmailDatabase();
		$objEmailDatabase->open();
		if( false == $objScheduledPaymentRoommateConfirmationEmail->insert( CUser::ID_SYSTEM, $objEmailDatabase ) ) {
			return false;
		}
		$objEmailDatabase->close();
		return true;
	}

	public function sendScheduledPaymentRoommateAdjustPaymentWarningEmail( $arrstrRoommatesEmailInfo, $strInitiatedBy, $arrmixData, $strAdjustPaymentMessage = NULL ) {
		$strEmailSubject = __( 'Split Auto Payment Failed to Process' );

		if( false == valStr( $arrmixData['to_email_address'] ) ) {
			return false;
		}

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );

		$objSmarty->assign( 'resident_portal_url', $arrmixData['resident_portal_url'] );
		$objSmarty->assign( 'count_roommates', \Psi\Libraries\UtilFunctions\count( $arrstrRoommatesEmailInfo ) );
		$objSmarty->assign( 'roommates_email_info', $arrstrRoommatesEmailInfo );
		$objSmarty->assign( 'initiated_by', $strInitiatedBy );
		$objSmarty->assign( 'email_title', $strEmailSubject );
		$objSmarty->assign( 'subject', $strEmailSubject );
		$objSmarty->assign( 'company_media_file', $arrmixData['company_media_file'] );
		$objSmarty->assign( 'default_company_media_file', $arrmixData['default_company_media_file'] );
		$objSmarty->assign( 'media_library_uri', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'property_email_rule', $arrmixData['property_email_rule'] );
		$objSmarty->assign( 'setup_message', $strAdjustPaymentMessage );

		$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/ar_payments/scheduled_payment_roommate_adjust_setup_warning.tpl' );

		$objScheduledPaymentRoommateConfirmationEmail = new CSystemEmail();
		$objScheduledPaymentRoommateConfirmationEmail->setDefaults();
		$objScheduledPaymentRoommateConfirmationEmail->setSystemEmailTypeId( CSystemEmailType::SCHEDULED_PAYMENT_WARNING );
		$objScheduledPaymentRoommateConfirmationEmail->setToEmailAddress( $arrmixData['to_email_address'] );
		$objScheduledPaymentRoommateConfirmationEmail->setSubject( $strEmailSubject );
		$objScheduledPaymentRoommateConfirmationEmail->setHtmlContent( $strHtmlEmailOutput );
		$objScheduledPaymentRoommateConfirmationEmail->setCid( $this->getCid() );
		$objScheduledPaymentRoommateConfirmationEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objScheduledPaymentRoommateConfirmationEmail->setPropertyId( $this->getPropertyId() );

		if( false == valObj( CSystemEmailLibrary::processSystemEmailByPropertyEmailRules( $objScheduledPaymentRoommateConfirmationEmail, $arrmixData['property_email_rule'] ), CSystemEmail::class ) ) {
			return false;
		}

		$objEmailDatabase = $this->loadEmailDatabase();
		$objEmailDatabase->open();

		if( false == $objScheduledPaymentRoommateConfirmationEmail->insert( CUser::ID_SYSTEM, $objEmailDatabase ) ) {
			return false;
		}

		$objEmailDatabase->close();
		return true;
	}

	public function createWarningSystemEmailForFailedToPost( \CLease $objLease, \CProperty $objProperty, $objClientDatabase ) {
		$objClient = $this->getOrFetchClient( $objClientDatabase );

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objClientDatabase, true );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objClientDatabase );
		}

		$arrobjPaymentTypes = CPaymentTypes::fetchAllPaymentTypes( $objClientDatabase );

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );

		$strResidentPortalUrl = $this->getResidentPortalUrl( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		$objSmarty->assign( 'resident_portal_url', 	$strResidentPortalUrl );

		$objSmarty->assign( 'scheduled_payment', $this );
		$objSmarty->assign( 'lease', 			$objLease );
		$objSmarty->assign( 'payment_types',	$arrobjPaymentTypes );
		$objSmarty->assign( 'property', 			$objProperty );
		$objSmarty->assign( 'marketing_media_association', 	$objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file',	$objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'media_library_uri', 		CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'terms_and_condition_path', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/Common/legal/resident_center_terms_conditions.html' );
		$objSmarty->assign( 'base_uri', 				CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	            CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/scheduled_payments/scheduled_payment_post_late_warning.tpl' );

		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::SCHEDULED_PAYMENT_WARNING );
		$objSystemEmail->setSubject( __( 'Recurring Payment failed to Post' ) );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::NORMAL );
		$objSystemEmail->setCid( $this->m_intCid );
		$objSystemEmail->setPropertyId( $this->m_intPropertyId );

		$arrobjSystemEmails = [];

		if( false == is_null( $this->getBilltoEmailAddress() ) && 0 < strlen( $this->getBilltoEmailAddress() ) ) {
			$arrstrEmailAddresses[] = $this->getBilltoEmailAddress();
		}

		if( valArr( $arrstrEmailAddresses ) ) {
			foreach( $arrstrEmailAddresses as $strEmailAddress ) {
				$objSystemEmail->setToEmailAddress( $strEmailAddress );
				$arrobjSystemEmails[] = clone $objSystemEmail;
			}
		}

		if( true == valArr( $arrobjSystemEmails ) ) {
			$this->m_objEmailDatabase = $this->loadEmailDatabase();
			$this->m_objEmailDatabase->open();
			if( true == valArr( $arrobjSystemEmails ) ) {
				foreach( $arrobjSystemEmails as $objSystemEmail ) {
					$strToEmailAddress = trim( $objSystemEmail->getToEmailAddress() );
					if( true == empty( $strToEmailAddress ) ) continue;
					if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $this->m_objEmailDatabase ) ) {
						trigger_error( 'Scheduled payment failed to post email failed to insert.', E_USER_WARNING );
						continue;
					}
				}
			}
			$this->m_objEmailDatabase->close();
		}
	}

	public function getResidentPortalUrl( $intPropertyId, $intCid, $objClientDatabase, $objWebsite = NULL ) {

		$strResidentPortalUrl = NULL;
		if( false == valObj( $objWebsite, 'CWebsite' ) ) {
			$objWebsite	= \Psi\Eos\Entrata\CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $intPropertyId ], $intCid, $objClientDatabase );
		}

		if( true == valObj( $objWebsite, 'CWebsite' ) ) {
			$strResidentPortalPrefix 	= ( true == valStr( CConfig::get( 'SECURE_HOST_PREFIX' ) ) ) ? CConfig::get( 'SECURE_HOST_PREFIX' ) : 'https://';
			$strResidentPortalSuffix 	= ( true == valStr( CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) ) ) ? CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) : '.residentportal.com';
			$strResidentPortalUrl		= $strResidentPortalPrefix . $objWebsite->getSubDomain() . $strResidentPortalSuffix . '/';
		}

		return $strResidentPortalUrl;
	}

	public function sendEmail( $objSystemEmail ) {
		$objMimeEmail = new CPsHtmlMimeMail();
		$objMimeEmail->setCRLF( "\r\n" );
		$objMimeEmail->setFrom( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objMimeEmail->setText( $objSystemEmail->getTextContent() );
		$objMimeEmail->setHTML( $objSystemEmail->getHtmlContent() );
		$objMimeEmail->setSubject( $objSystemEmail->getSubject() );

		$objMimeEmail->send( [ $objSystemEmail->getToEmailAddress() ] );
		return true;
	}

	public function updateLastPostedOn( $intCompanyUserId, $objClientDatabase ) {

		if( true == is_null( $this->getLastPostedOn() ) ) {
			$intNextLastPostedOn = strtotime( $this->getPaymentStartDate() );
		} else {
			$intNextLastPostedOn = strtotime( $this->getLastPostedOn() );
		}
		$intProcessDay = $this->getProcessDay();

		while( $intNextLastPostedOn <= time() ) {
			if( self::LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
				$intLastPostedOn = $intNextLastPostedOn;
				$intNextLastPostedOn = strtotime( 'last day of +1 month', $intNextLastPostedOn );
			} elseif( self::SECOND_LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
				$intLastPostedOn = $intNextLastPostedOn;
				$intNextLastPostedOn = strtotime( 'last day of +1 month', $intNextLastPostedOn );
				$intNextLastPostedOn = strtotime( '-1 day', $intNextLastPostedOn );
			} elseif( self::THIRD_LAST_DAY_OF_THE_MONTH == $intProcessDay ) {
				$intLastPostedOn = $intNextLastPostedOn;
				$intNextLastPostedOn = strtotime( 'last day of +1 month', $intNextLastPostedOn );
				$intNextLastPostedOn = strtotime( '-2 day', $intNextLastPostedOn );
			} else {
				$intLastPostedOn     = $intNextLastPostedOn;
				$intNextLastPostedOn = strtotime( '+1 month', $intLastPostedOn );
			}
		}

		if( true == is_null( $intLastPostedOn ) ) {
			$intLastPostedOn = time();
			trigger_error( 'Scheduled Payment ID:' . $this->getId() . ' did not get a last_posted_on date.', E_USER_WARNING );
		}

		$this->setLastPostedOn( date( 'm/d/Y', $intLastPostedOn ) );
		$this->setLastPostedBy( $intCompanyUserId );
		$this->setForceReprocess( 0 );

		if( false == $this->update( $intCompanyUserId, $objClientDatabase ) ) {
			trigger_error( 'Scheduled Payment ID:' . $this->getId() . ' FAILED TO UPDATE LAST_POSTED_ON FIELD.  THIS HAS TO BE FIXED OR RECURRING PAYMENT SCRIPT WILL NOT FINISH AND THIS PAYMENT WILL PROCESS EVERY NIGHT.', E_USER_ERROR );
			return false;
		} else {
			return true;
		}
	}

	public function isElectronicPayment() {

		if( true == is_null( $this->m_intPaymentTypeId ) ) {
			return false;
		}

		return CPaymentTypes::isElectronicPayment( $this->m_intPaymentTypeId );
	}

	public function valAgreesToPayConvenienceFees() {
		$boolIsValid = true;

		if( 0 < $this->m_fltConvenienceFeeAmount && false == $this->m_boolAgreesToPayConvenienceFees ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Agreeing to pay convenience fee is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAgreesToTerms() {
		$boolIsValid = true;

		if( false == $this->m_boolAgreesToTerms ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Agreeing to terms is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAgreesToPaymentTerms() {
		$boolIsValid = true;

		if( true == $this->isElectronicPayment() ) {
			$boolIsValid &= $this->valAgreesToPayConvenienceFees();
		}

		$boolIsValid &= $this->valAgreesToTerms();

		return $boolIsValid;
	}

	public static function checkIsReturnSchedulePaymentByCustomerIdByLeaseIdByPropertyIdByCid( $intCustomerId, $arrintLeaseIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valId( $intCustomerId ) || false == valArr( $arrintLeaseIds ) || false == valArr( array_filter( $arrintPropertyIds ) ) ) return false;

		$strSql = 'WITH
						payment_expiration_details AS (
						SELECT
								count(id) as has_valid_scheduled_payments
						FROM
								scheduled_payments sp
						WHERE (
								sp.payment_start_date > now() OR sp.payment_end_date IS NULL )
								AND sp.customer_id = ' . ( int ) $intCustomerId . '
								AND sp.cid = ' . ( int ) $intCid . '
								AND sp.deleted_on IS NULL
								AND sp.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
								AND sp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							)
						SELECT
								count(id) as sp_count
						FROM
								scheduled_payments sp
						WHERE
							sp.customer_id = ' . ( int ) $intCustomerId . '
							AND sp.cid = ' . ( int ) $intCid . '
							AND sp.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
							AND sp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND sp.deleted_on IS NULL
							AND ( ( sp.payment_end_date IS NOT NULL
							AND sp.payment_end_date >= now() AND sp.payment_end_date <= ( date_trunc ( \'day\', NOW ( ) ) + INTERVAL \'1 MONTH\' ) ) )
							AND 0 = ( select has_valid_scheduled_payments from payment_expiration_details )';

		$arrintCount = fetchData( $strSql, $objDatabase );
		return ( true == isset ( $arrintCount[0]['sp_count'] ) && 0 < $arrintCount[0]['sp_count'] ) ? true : false;
	}

	// fetch the next variable scheduled payment for a particular lease
	// Eg
	// 1. Oct 4
	// 2. Nov 26
	// 3. Dec 17
	// If today is Nov 26, the next Payment fetched should be of Oct 4 but with next_payment_date as Dec 4

	public function fetchNextVariableScheduledPaymentId( $objDatabase ) {

		$strSql = 'SELECT
							temp_sp.id,
							temp_sp.next_payment_date
					FROM   (
							SELECT
									sp.id as id,
							CASE WHEN sp.payment_start_date <= NOW()
								THEN
									CASE WHEN ( EXTRACT ( MONTH FROM sp.payment_start_date ) = EXTRACT ( MONTH FROM now() )
												AND EXTRACT ( DAY FROM sp.payment_start_date ) <= EXTRACT ( DAY FROM now() )
												AND EXTRACT (YEAR FROM sp.payment_start_date ) = EXTRACT( YEAR FROM now() ))
										THEN sp.payment_start_date + INTERVAL \'1 month\'

										WHEN ( EXTRACT ( MONTH FROM sp.payment_start_date ) < EXTRACT ( MONTH FROM now() )
												AND EXTRACT ( DAY FROM sp.payment_start_date ) <= EXTRACT ( DAY FROM now() )
												AND EXTRACT ( YEAR FROM sp.payment_start_date ) = EXTRACT ( YEAR FROM now() ) )
										THEN sp.payment_start_date + CAST( ( ( EXTRACT ( MONTH FROM now() ) -  EXTRACT ( MONTH FROM sp.payment_start_date ) ) + 1 )||\' MONTH\' AS INTERVAL )

										WHEN ( EXTRACT ( MONTH FROM sp.payment_start_date ) < EXTRACT ( MONTH FROM now() )
												AND EXTRACT ( DAY FROM sp.payment_start_date ) > EXTRACT ( DAY FROM now() )
												AND EXTRACT ( YEAR FROM sp.payment_start_date ) = EXTRACT ( YEAR FROM now() ) )
										THEN sp.payment_start_date + CAST( ( ( EXTRACT ( MONTH FROM now() ) -  EXTRACT ( MONTH FROM sp.payment_start_date ) ) )||\' MONTH\' AS INTERVAL )

										WHEN ( EXTRACT ( DAY FROM sp.payment_start_date ) > EXTRACT ( DAY FROM now() )
												AND EXTRACT ( YEAR FROM sp.payment_start_date ) < EXTRACT ( YEAR FROM now() ) )
										THEN sp.payment_start_date + CAST( ( ( ( EXTRACT ( YEAR FROM now() ) -  EXTRACT ( YEAR FROM sp.payment_start_date ) ) *12 ) + ( EXTRACT ( MONTH FROM now() ) - EXTRACT( MONTH FROM sp.payment_start_date ) ))||\' MONTH\' AS INTERVAL )

										WHEN ( EXTRACT ( DAY FROM sp.payment_start_date ) <= EXTRACT ( DAY FROM now() )
												AND EXTRACT ( YEAR FROM sp.payment_start_date ) < EXTRACT ( YEAR FROM now() ) )
										THEN sp.payment_start_date + CAST( ( ( ( EXTRACT ( YEAR FROM now() ) -  EXTRACT ( YEAR FROM sp.payment_start_date ) ) *12 ) + ( EXTRACT ( MONTH FROM now() ) - EXTRACT( MONTH FROM sp.payment_start_date ) ) +1 )||\' MONTH\' AS INTERVAL )
									ELSE sp.payment_end_date
									END
								ELSE  sp.payment_start_date
								END as next_payment_date
							FROM
								scheduled_payments sp
							WHERE
								sp.lease_id = ' . ( int ) $this->getLeaseId() . '
								AND sp.cid = ' . ( int ) $this->getCid() . '
								AND sp.deleted_on IS NULL
								AND sp.is_variable_amount = 1
								AND sp.scheduled_payment_type_id = ' . CScheduledPaymentType::BIMONTHLY . '
								AND ( sp.payment_end_date IS NULL OR sp.payment_end_date >= NOW() )
							) as temp_sp
					ORDER BY temp_sp.next_payment_date';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		return $arrmixResponse;

	}

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'STATIC_RECURR_PAYMENTS', self::STATIC_RECURR_PAYMENTS );
		$objSmarty->assign( 'VARIABLE_RECURR_PAYMENTS', self::VARIABLE_RECURR_PAYMENTS );
		$objSmarty->assign( 'STATIC_AND_VARIABLE_RECURR_PAYMENTS', self::STATIC_AND_VARIABLE_RECURR_PAYMENTS );
		$objSmarty->assign( 'DO_NOT_ALLOW_RECURR_PAYMENTS', self::DO_NOT_ALLOW_RECURR_PAYMENTS );
		$objSmarty->assign( 'REQUIRED_RECURRING_PAYMENT_CEILING', self::REQUIRED_RECURRING_PAYMENT_CEILING );
		$objSmarty->assign( 'ALLOWED_RECURRING_PAYMENT_CEILING', self::ALLOWED_RECURRING_PAYMENT_CEILING );
	}

	/**
	 * Updates data related to customer payment accounts ( Stored billing information ).
	 * @return bool
	 */
	public function updateCustomerPaymentAccountFromScheduledPayment( array $arrmixStoredBillingInfo, \CScheduledPayment $objScheduledPayment = NULL, $objClientDatabase ) {
		$objClient 		= $this->getOrFetchClient( $objClientDatabase );
		$intCustomerId = $arrmixStoredBillingInfo['customer_id'];
		$intCustomerPaymentAccountId = $arrmixStoredBillingInfo['customer_payment_account_id'];
		$objCustomerPaymentAccount = \CCustomerPaymentAccounts::fetchCustomerPaymentAccountByIdByCustomerIdByCid( $intCustomerPaymentAccountId, $intCustomerId, $objClient->getId(), $objClientDatabase );
		if( false === valObj( $objCustomerPaymentAccount, 'CCustomerPaymentAccount' ) ) {
			return false;
		}

		$objCustomerPaymentAccount->setBilltoNameFirst( $arrmixStoredBillingInfo['billto_name_first'] );
		$objCustomerPaymentAccount->setBilltoNameLast( $arrmixStoredBillingInfo['billto_name_last'] );
		$objCustomerPaymentAccount->setBilltoEmailAddress( $arrmixStoredBillingInfo['billto_email_address'] );
		$objCustomerPaymentAccount->setLastUsedOn( date( 'm/d/Y H:i:s' ) );

		if( true === valObj( $objScheduledPayment, 'CScheduledPayment' ) ) {
			$objCustomerPaymentAccount->setBilltoStreetLine1( $objScheduledPayment->getBilltoStreetLine1() );
			$objCustomerPaymentAccount->setBilltoStreetLine2( $objScheduledPayment->getBilltoStreetLine2() );
			$objCustomerPaymentAccount->setBilltoStreetLine3( $objScheduledPayment->getBilltoStreetLine3() );
			$objCustomerPaymentAccount->setBilltoCity( $objScheduledPayment->getBilltoCity() );
			$objCustomerPaymentAccount->setBilltoStateCode( $objScheduledPayment->getBilltoStateCode() );
			$objCustomerPaymentAccount->setBilltoPostalCode( $objScheduledPayment->getBilltoPostalCode() );
			$objCustomerPaymentAccount->setBilltoCountryCode( $objScheduledPayment->getBilltoCountryCode() );
			$objCustomerPaymentAccount->setIsCreditCard( ( bool ) $objScheduledPayment->getIsCreditCard() );
			$objCustomerPaymentAccount->setIsDebitCard( ( bool ) $objScheduledPayment->getIsDebitCard() );
			$objCustomerPaymentAccount->setIsCheckCard( ( bool ) $objScheduledPayment->getIsCheckCard() );
			$objCustomerPaymentAccount->setIsGiftCard( ( bool ) $objScheduledPayment->getIsGiftCard() );
			$objCustomerPaymentAccount->setIsPrepaidCard( ( bool ) $objScheduledPayment->getIsPrepaidCard() );
			$objCustomerPaymentAccount->setIsInternationalCard( ( bool ) $objScheduledPayment->getIsInternationalCard() );
			$objCustomerPaymentAccount->setIsCommercialCard( ( bool ) $objScheduledPayment->getIsCommercialCard() );
		}

		if( false === $objCustomerPaymentAccount->update( CUser::ID_RESIDENT_PORTAL, $objClientDatabase ) ) {
			trigger_error( 'Failed to update Customer Payment Account ' . $arrmixStoredBillingInfo['customer_payment_account_id'], E_USER_WARNING );

			return false;
		}

		return true;
	}

	public function buildRoommateEmailData( $intCustomerId, $strEmailAddress, $boolIsUpdate = false ) {
		$arrstrRoommatesEmailInfo = [];
		$arrstrRoommatesEmailInfo[$intCustomerId]['name'] = $this->getBilltoNameFirst() . ' ' . $this->getBilltoNameLast();
		$arrstrRoommatesEmailInfo[$intCustomerId]['email'] = $strEmailAddress;
		$arrstrRoommatesEmailInfo[$intCustomerId]['is_initiated'] = ( true === valStr( $this->getApprovedOn() ) ) ? 1 : 0;
		$arrstrRoommatesEmailInfo[$intCustomerId]['payment_end_date'] = $this->getPaymentEndDate();
		$arrstrRoommatesEmailInfo[$intCustomerId]['payment_start_date'] = $this->getPaymentStartDate();

		if( false === isset( $arrstrRoommatesEmailInfo[$intCustomerId]['current_payment_amount_rate'] ) ) {
			$arrstrRoommatesEmailInfo[$intCustomerId]['current_payment_amount_rate'] = 0;
		}

		if( false === isset( $arrstrRoommatesEmailInfo[$intCustomerId]['current_payment_amount'] ) ) {
			$arrstrRoommatesEmailInfo[$intCustomerId]['current_payment_amount'] = 0;
		}

		if( 0 == $this->getIsVariableAmount() ) {
			$arrstrRoommatesEmailInfo[$intCustomerId]['payment_amount'] = $this->getPaymentAmount();
		} else {
			$arrstrRoommatesEmailInfo[$intCustomerId]['payment_amount_rate'] = $this->getPaymentAmountRate();
		}

		if( false == $boolIsUpdate || true === valStr( $this->getApprovedOn() ) ) {
			$arrstrRoommatesEmailInfo[$intCustomerId]['split_roommate_start_month'] = $this->getSplitRoommateStartMonth();
			$arrstrRoommatesEmailInfo[$intCustomerId]['split_roommate_end_month'] = $this->getSplitRoommateEndMonth();
		} else {
			$arrstrRoommatesEmailInfo[$intCustomerId]['split_roommate_start_month'] = strtotime( $this->getPaymentStartDate() );
			$arrstrRoommatesEmailInfo[$intCustomerId]['split_roommate_end_month'] = strtotime( $this->getPaymentEndDate() );
		}

		return $arrstrRoommatesEmailInfo;
	}

	public function fetchDefaultMerchantAccount( $objDatabase ) {
		if( false == is_int( $this->m_intCid ) ) return NULL;
		if( false == is_int( $this->m_intPropertyId ) ) return NULL;

		$this->m_objMerchantAccount	= CMerchantAccounts::fetchDefaultMerchantAccountByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objDatabase, $this->m_intPaymentMediumId );

		return $this->m_objMerchantAccount;
	}

	public function logRecurringPaymentInformation( $intUserId, $objDatabase, $intEventType ) {

		$objEventLibrary			= new CEventLibrary();
		$objEventLibraryDataObject	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( array( $this ), $intEventType );

		$objEvent->setCid( $this->getCid() );
		$objEvent->setPropertyId( $this->getPropertyId() );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setCustomerId( $this->getCustomerId() );
		$objEventLibrary->buildEventDescription( $this );

		if( false == valStr( $objEvent->getNotes() ) && \CEventType::RECURRING_PAYMENT_UPDATED == $intEventType ) {
			return true;
		}

		if( true == valStr( $objEvent->getEventHandle() ) && false == $objEventLibrary->insertEvent( $intUserId, $objDatabase ) ) {
			trigger_error( 'Failed to insert event for the Customer : ' . $this->getCustomerId(), E_USER_WARNING );
		}

		return true;
	}

	public function setPaymentInfoByCustomerPaymentAccount( \CCustomerPaymentAccount $objCustomerPaymentAccount = NULL ) {
		if( false === valObj( $objCustomerPaymentAccount, 'CCustomerPaymentAccount' ) ) {
			return NULL;
		}

		$this->setPaymentTypeId( $objCustomerPaymentAccount->getPaymentTypeId() );

		if( NULL !== $objCustomerPaymentAccount->getCheckAccountNumber() ) {
			$this->setCheckAccountNumber( $objCustomerPaymentAccount->getCheckAccountNumber() );
		}
		if( NULL !== $objCustomerPaymentAccount->getCheckAccountTypeId() ) {
			$this->setCheckAccountTypeId( $objCustomerPaymentAccount->getCheckAccountTypeId() );
		}
		if( NULL !== $objCustomerPaymentAccount->getCheckBankName() ) {
			$this->setCheckBankName( $objCustomerPaymentAccount->getCheckBankName() );
		}
		if( NULL !== $objCustomerPaymentAccount->getCheckNameOnAccount() ) {
			$this->setCheckNameOnAccount( $objCustomerPaymentAccount->getCheckNameOnAccount() );
		}
		if( NULL !== $objCustomerPaymentAccount->getCheckRoutingNumber() ) {
			$this->setCheckRoutingNumber( $objCustomerPaymentAccount->getCheckRoutingNumber() );
		}
		if( NULL !== $objCustomerPaymentAccount->getCcCardNumber() ) {
			$this->setCcCardNumber( $objCustomerPaymentAccount->getCcCardNumber() );
		}
		if( NULL !== $objCustomerPaymentAccount->getSecureReferenceNumber() ) {
			$this->setSecureReferenceNumber( $objCustomerPaymentAccount->getSecureReferenceNumber() );
		}
		if( NULL !== $objCustomerPaymentAccount->getCcExpDateMonth() ) {
			$this->setCcExpDateMonth( $objCustomerPaymentAccount->getCcExpDateMonth() );
		}
		if( NULL !== $objCustomerPaymentAccount->getCcExpDateYear() ) {
			$this->setCcExpDateYear( $objCustomerPaymentAccount->getCcExpDateYear() );
		}
		if( NULL !== $objCustomerPaymentAccount->getCcNameOnCard() ) {
			$this->setCcNameOnCard( $objCustomerPaymentAccount->getCcNameOnCard() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoNameFirst() ) {
			$this->setBilltoNameFirst( $objCustomerPaymentAccount->getBilltoNameFirst() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoNameLast() ) {
			$this->setBilltoNameLast( $objCustomerPaymentAccount->getBilltoNameLast() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoStreetLine1() ) {
			$this->setBilltoStreetLine1( $objCustomerPaymentAccount->getBilltoStreetLine1() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoStreetLine2() ) {
			$this->setBilltoStreetLine2( $objCustomerPaymentAccount->getBilltoStreetLine2() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoStreetLine3() ) {
			$this->setBilltoStreetLine3( $objCustomerPaymentAccount->getBilltoStreetLine3() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoCity() ) {
			$this->setBilltoCity( $objCustomerPaymentAccount->getBilltoCity() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoStateCode() ) {
			$this->setBilltoStateCode( $objCustomerPaymentAccount->getBilltoStateCode() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoPostalCode() ) {
			$this->setBilltoPostalCode( $objCustomerPaymentAccount->getBilltoPostalCode() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoCountryCode() ) {
			$this->setBilltoCountryCode( $objCustomerPaymentAccount->getBilltoCountryCode() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoPhoneNumber() ) {
			$this->setBilltoPhoneNumber( $objCustomerPaymentAccount->getBilltoPhoneNumber() );
		}
		if( NULL !== $objCustomerPaymentAccount->getBilltoEmailAddress() ) {
			$this->setBilltoEmailAddress( $objCustomerPaymentAccount->getBilltoEmailAddress() );
		}

		return true;
	}

	public function setPaymentEndDateData( $objLease, $strEndMonth ) {
		if( \Psi\CStringService::singleton()->strtolower( \CScheduledPayment::STR_UNTIL_I_MOVE_OUT ) == \Psi\CStringService::singleton()->strtolower( $strEndMonth ) || \Psi\CStringService::singleton()->strtolower( \CScheduledPayment::STR_UNTIL_I_CANCEL ) == \Psi\CStringService::singleton()->strtolower( $strEndMonth ) || \Psi\CStringService::singleton()->strtolower( \CScheduledPayment::STR_UNTIL_I_CANCEL_OR_MOVE_OUT ) == \Psi\CStringService::singleton()->strtolower( $strEndMonth ) || \Psi\CStringService::singleton()->strtolower( \CScheduledPayment::STR_WHEN_I_CANCEL ) == \Psi\CStringService::singleton()->strtolower( $strEndMonth ) ) {
			// "When I cancel" and Until I Cancel or Move Out option
			$this->setPaymentEndDate( NULL );
		} elseif( \Psi\CStringService::singleton()->strtolower( \CScheduledPayment::STR_LEASE_END_DATE ) == \Psi\CStringService::singleton()->strtolower( $strEndMonth ) ) {
			$this->setEndDatesFromLease( $objLease );
		} else {
			// "Date selected" option
			$this->setEndMonth( ( int ) strtotime( $strEndMonth ) );
		}

		if( \Psi\CStringService::singleton()->strtolower( \CScheduledPayment::STR_UNTIL_I_MOVE_OUT ) == \Psi\CStringService::singleton()->strtolower( $strEndMonth ) || \Psi\CStringService::singleton()->strtolower( \CScheduledPayment::STR_UNTIL_I_CANCEL_OR_MOVE_OUT ) == \Psi\CStringService::singleton()->strtolower( $strEndMonth ) ) {
			$this->setUseLeaseEnd( true );
		} else {
			$this->setUseLeaseEnd( false );
		}
	}

	public function setIsTranslateEmail( $boolIsTranslateEmail ) {
		$this->m_boolIsTranslateEmail = $boolIsTranslateEmail;
	}

	public function getIsTranslateEmail() {
		return $this->m_boolIsTranslateEmail;
	}

	public function setCustomerPreferredLocaleCode( $strCustomerPreferredLocaleCode ) {
		$this->m_strCustomerPreferredLocaleCode = $strCustomerPreferredLocaleCode;
	}

	public function getCustomerPreferredLocaleCode() {
		return $this->m_strCustomerPreferredLocaleCode;
	}

	public function getProperBillStartOrEndDay( $intStartOrEndMonth, $boolIsForSplit = false ) {
		$intBillDay = ( ( $boolIsForSplit ) ? 'm_intSplitRoommateBillDay' : 'm_intBillDay' );
		if( self::LAST_DAY_OF_THE_MONTH == $this->m_intProcessDay ) {
			$this->$intBillDay = date( 't', mktime( 0, 0, 0, date( 'm', $intStartOrEndMonth ), 1, date( 'Y', $intStartOrEndMonth ) ) );
		} elseif( self::SECOND_LAST_DAY_OF_THE_MONTH == $this->m_intProcessDay ) {
			$this->$intBillDay = date( 't', mktime( 0, 0, 0, date( 'm', $intStartOrEndMonth ), 1, date( 'Y', $intStartOrEndMonth ) ) ) - 1;
		} elseif( self::THIRD_LAST_DAY_OF_THE_MONTH == $this->m_intProcessDay ) {
			$this->$intBillDay = date( 't', mktime( 0, 0, 0, date( 'm', $intStartOrEndMonth ), 1, date( 'Y', $intStartOrEndMonth ) ) ) - 2;
		}
	}

	public function setIsPlaidAccount( $boolIsPlaidAccount ) {
		$this->m_boolIsPlaidAccount = $boolIsPlaidAccount;
	}

	public function getIsPlaidAccount() {
		return $this->m_boolIsPlaidAccount;
	}

}
