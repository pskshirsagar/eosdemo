<?php

class CRepayment extends CBaseRepayment {

	protected $m_intNumberOfMonths;
	protected $m_intIsFullBalance;

	protected $m_strPaymentOption;

	protected $m_boolIsRepayDueDateToday;

	protected $m_fltNextInstallmentDate;
	protected $m_fltNextInstallmentAmount;

	protected $m_arrobjOpenArTransactions;
	protected $m_fltOpenTaxAmount;
	// Set functions

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['number_of_months'] ) ) $this->setNumberOfMonths( $arrmixValues['number_of_months'] );
		if( true == isset( $arrmixValues['payment_option'] ) ) $this->setPaymentOption( $arrmixValues['payment_option'] );
		if( true == isset( $arrmixValues['is_full_balance'] ) ) $this->setIsFullBalance( $arrmixValues['is_full_balance'] );
		if( true == isset( $arrmixValues['next_installment_amount'] ) ) $this->setNextInstallmentAmount( $arrmixValues['next_installment_amount'] );
		if( true == isset( $arrmixValues['next_installment_date'] ) ) $this->setNextInstallmentDate( $arrmixValues['next_installment_date'] );
		if( true == isset( $arrmixValues['open_tax_amt'] ) ) $this->setOpenTaxAmount( $arrmixValues['open_tax_amt'] );
	}

	public function setNumberOfMonths( $intNumberOfMonths ) {
		$this->m_intNumberOfMonths = CStrings::strToIntDef( $intNumberOfMonths, NULL, false );
	}

	public function setPaymentOption( $strPaymentOption ) {
		$this->m_strPaymentOption = CStrings::strTrimDef( $strPaymentOption, 10, NULL, true );
	}

	public function setIsFullBalance( $intIsFullBalance ) {
		$this->m_intIsFullBalance = ( int ) $intIsFullBalance;
	}

	public function setIsRepayDueDateToday( $boolRepayDueDateToday ) {
		$this->m_boolIsRepayDueDateToday = $boolRepayDueDateToday;
	}

	public function setOpenArTransactions( $arrobjArTransactions ) {
		$this->m_arrobjOpenArTransactions = $arrobjArTransactions;
	}

	public function setNextInstallmentAmount( $fltNextInstallmentAmount ) {
		$this->m_fltNextInstallmentAmount = $fltNextInstallmentAmount;
	}

	public function setNextInstallmentDate( $fltNextInstallmentDate ) {
		$this->m_fltNextInstallmentDate = $fltNextInstallmentDate;
	}

	public function setOpenTaxAmount( $fltOpenTaxAmount ) {
		$this->m_fltOpenTaxAmount = $fltOpenTaxAmount;
	}

	// Get functions

	public function getNumberOfMonths() {
		return $this->m_intNumberOfMonths;
	}

	public function getPaymentOption() {
		return $this->m_strPaymentOption;
	}

	public function getIsFullBalance() {
		return $this->m_intIsFullBalance;
	}

	public function getIsRepayDueDateToday() {
		return $this->m_boolIsRepayDueDateToday;
	}

	public function getOpenArTransactions() {
		return $this->m_arrobjOpenArTransactions;
	}

	public function getNumberOfPaymentsToPayOff() {
		if( 0 >= $this->getCurrentBalance() || 0 >= $this->getAgreementAmountDue() ) return 0;

		return ( int ) ceil( $this->getCurrentBalance() / $this->getAgreementAmountDue() );
	}

	public function getNextInstallmentAmount() {
		return $this->m_fltNextInstallmentAmount;
	}

	public function getNextInstallmentDate() {
		return $this->m_fltNextInstallmentDate;
	}

	public function getOpenTaxAmount() {
		return $this->m_fltOpenTaxAmount;
	}

	// Validate functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOffsetDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAgreementAmount() {
		$boolIsValid = true;

		if( 0 >= $this->getAgreementAmountTotal() || 0 >= $this->getAgreementAmountDue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agreement_amount_total', __( 'Balance due amount must be greater than zero.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAgreementAmountDue() {
		$boolIsValid = true;

		if( $this->getAgreementAmountDue() > $this->getAgreementAmountTotal() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agreement_amount_due', __( 'Monthly Payment Amount cannot exceed the total repayment agreement amount.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCurrentBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrentAmountDue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastPaymentOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartsOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndsOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumberOfMonths( $objPropertyChargeSetting ) {
		$boolIsValid = true;

		if( 0 >= $this->getNumberOfMonths() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_months', __( 'Number of months value must be non-zero.' ) ) );

		} elseif( true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) && $this->getNumberOfMonths() > $objPropertyChargeSetting->getRepaymentMaximumAllowedMonths() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_months', __( 'Number of months exceeds the max allowed repayment months( Max Value: {%d, 0} )', [ ( int ) $objPropertyChargeSetting->getRepaymentMaximumAllowedMonths() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valRepaymentAgreement( $objPropertyChargeSetting ) {
		$boolIsValid = true;

		if( true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) && false == $objPropertyChargeSetting->getAllowRepaymentAgreements() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'repayment_agreement', __( 'Repayment agreements not allowed for the property.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFirstPaymentDueOn() {
		$boolIsValid = true;

		if( false == valStr( $this->getFirstPaymentDueOn() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_payment_due_on', __( 'First payment start date is required.' ) ) );

		} elseif( false == CValidation::checkISODateFormat( $this->getFirstPaymentDueOn(), true ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_payment_due_on', __( 'First payment start date is not valid.' ) ) );
		} else {
			$arrintSelectedDate = explode( '/', CValidation::getCannedFormatDate( $this->getFirstPaymentDueOn() ) );

			// Date should be  present or future with the date as same as offset day
			if( true == $this->getIsResident() && strtotime( $this->getFirstPaymentDueOn() ) <= strtotime( date( 'Y-m-d' ) ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_payment_due_on', __( 'First Payment Due Date should be greater than current date.' ) ) );
			}

			if( ltrim( $arrintSelectedDate[1], '0' ) != $this->getOffsetDays() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_payment_due_on', __( 'First Payment Date should be the same day of the month as the \'Payment Due On\' day.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objPropertyChargeSetting = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRepaymentAgreement( $objPropertyChargeSetting );
				$boolIsValid &= $this->valNumberOfMonths( $objPropertyChargeSetting );
				$boolIsValid &= $this->valAgreementAmount();
				$boolIsValid &= $this->valAgreementAmountDue();
				$boolIsValid &= $this->valFirstPaymentDueOn();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// Fetch functions

	public function fetchRepaymentArTransactions( $objDatabase ) {
		return \Psi\Eos\Entrata\CRepaymentArTransactions::createService()->fetchRepaymentArTransactionsByRepaymentIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function setRepaymentEndDate( $objDatabase ) {

		$strSql = 'SELECT * FROM func_get_repayment_end_date( ' . ( int ) $this->getNumberOfMonths() . ', \'' . $this->getFirstPaymentDueOn() . '\'::DATE )';

		$strRepaymentEndDate = current( current( fetchData( $strSql, $objDatabase ) ) );
		$this->setEndsOn( $strRepaymentEndDate );
	}

	public function getNextRepaymentDate() {
		$intOffsetDays = $this->getOffsetDays();
		if( $intOffsetDays > date( 't' ) ) {
			$intOffsetDays = date( 't' );
		}

		$strRepaymentDate = date( 'm/' . $intOffsetDays . '/Y' );

		if( strtotime( date( 'm/d/Y' ) ) > strtotime( $strRepaymentDate ) || strtotime( $this->getStartsOn() ) == strtotime( $strRepaymentDate ) ) {
			$strRepaymentDate = date( 'm/d/Y', strtotime( '+1 month', strtotime( $strRepaymentDate ) ) );
		}

		if( strtotime( $strRepaymentDate ) > strtotime( $this->getEndsOn() ) ) {
			$strRepaymentDate = date( 'm/d/Y' );
		}

		return $strRepaymentDate;
	}

	public function updateRepaymentAgreementBalance( $objDatabase, $intCompanyUserId ) {

		$strSql = 'SELECT * FROM func_update_repayment_balance( ARRAY[ ' . $this->getCid() . ' ]::INTEGER[], ARRAY[ ' . $this->getPropertyId() . ' ]::INTEGER[], ARRAY[ ' . $this->getLeaseId() . ' ]::INTEGER[], ARRAY[ ' . $this->getId() . ' ]::INTEGER[], ' . ( int ) $intCompanyUserId . ' )';

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();
			return false;
		}

		$objDataset->cleanup();

		return true;
	}

}
?>