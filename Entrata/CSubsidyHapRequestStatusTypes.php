<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyHapRequestStatusTypes
 * Do not add any new functions to this class.
 */

class CSubsidyHapRequestStatusTypes extends CBaseSubsidyHapRequestStatusTypes {

	public static function fetchSubsidyHapRequestStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSubsidyHapRequestStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSubsidyHapRequestStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSubsidyHapRequestStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>