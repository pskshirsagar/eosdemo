<?php

class CArAccrualTypes extends CBaseArAccrualTypes {

    public static function fetchArAccrualTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CArAccrualType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchArAccrualType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CArAccrualType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>