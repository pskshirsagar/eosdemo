<?php

class CPropertyWebsiteStat extends CBasePropertyWebsiteStat {

    /**
     * Validation Functions
     */

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Validation Functions
     */

	public function insertOrUpdateStandardStat( $boolIsUniqueStat, $intCurrentUserId, $objDatabase ) {
        if( true == is_null( $this->getId() ) ) {
            return $this->insertStandardStat( $intCurrentUserId, $objDatabase );
        } else {
            return $this->updateStandardStat( $boolIsUniqueStat, $intCurrentUserId, $objDatabase );
        }
    }

    public function insertStandardStat( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

        $intId = $this->getId();

        if( true == is_null( $intId ) ) {
            $strSql = 'SELECT nextval( \'property_website_stats_id_seq\'::text ) AS id';

            if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert property website stat record. The following error was reported.' ) );
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

                $objDataset->cleanup();
                return false;
            }

            $arrValues = $objDataset->fetchArray();
            $this->setId( $arrValues['id'] );

            $objDataset->cleanup();
        }

        $strSql = 'INSERT INTO property_website_stats( id, cid, property_id, stat_date, standard_uniques, standard_views, updated_by, updated_on, created_by, created_on ) VALUES ( ' .
                    $this->sqlId() . ', ' .
                    $this->sqlCid() . ', ' .
                    $this->sqlPropertyId() . ', now(), 1, 1, ' .
                    ( int ) $intCurrentUserId . ', now(), ' .
                    ( int ) $intCurrentUserId . ', now() )';

        if( false == $objDataset->execute( $strSql ) ) {
            // Reset id on error
            $this->setId( $intId );

            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert property website stat record. The following error was reported.' ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

            $objDataset->cleanup();

            return false;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert property website stat record. The following error was reported.' ) );
            // Reset id on error
            $this->setId( $intId );

            while( false == $objDataset->eof() ) {
                $arrValues = $objDataset->fetchArray();
                $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
                $objDataset->next();
            }

            $objDataset->cleanup();

            return false;
        }

        $objDataset->cleanup();

        return true;
    }

    public function updateStandardStat( $boolIsUniqueStat, $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

        $strSql = 'UPDATE property_website_stats SET';

        if( true == $boolIsUniqueStat ) {
        	$strSql .= ' standard_uniques = standard_uniques + 1, ';
        }

        $strSql .= ' standard_views = standard_views + 1
				  WHERE
				    id = ' . ( int ) $this->sqlId() .
        			' AND cid = ' . $this->sqlCid();

        if( false == $objDataset->execute( $strSql ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update property website stat record. The following error was reported.' ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

            $objDataset->cleanup();

            return false;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update property website stat record. The following error was reported.' ) );
            while( false == $objDataset->eof() ) {
                $arrValues = $objDataset->fetchArray();
                $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
                $objDataset->next();
            }

            $objDataset->cleanup();

            return false;
        }

        $objDataset->cleanup();

        return true;
    }

	public function insertOrUpdateMobileStat( $boolIsUniqueStat, $intCurrentUserId, $objDatabase ) {
        if( true == is_null( $this->getId() ) ) {
            return $this->insertMobileStat( $intCurrentUserId, $objDatabase );
        } else {
            return $this->updateMobileStat( $boolIsUniqueStat, $intCurrentUserId, $objDatabase );
        }
    }

    public function insertMobileStat( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

        $intId = $this->getId();

        if( true == is_null( $intId ) ) {
            $strSql = 'SELECT nextval( \'property_website_stats_id_seq\'::text ) AS id';

            if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert property website stat record. The following error was reported.' ) );
                $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

                $objDataset->cleanup();
                return false;
            }

            $arrValues = $objDataset->fetchArray();
            $this->setId( $arrValues['id'] );

            $objDataset->cleanup();
        }

        $strSql = 'INSERT INTO property_website_stats( id, cid, property_id, stat_date, mobile_uniques, mobile_views, updated_by, updated_on, created_by, created_on ) VALUES ( ' .
                    $this->sqlId() . ', ' .
                    $this->sqlCid() . ', ' .
                    $this->sqlPropertyId() . ', now(), 1, 1, ' .
                    ( int ) $intCurrentUserId . ', now(), ' .
                    ( int ) $intCurrentUserId . ', now() )';

        if( false == $objDataset->execute( $strSql ) ) {
            // Reset id on error
            $this->setId( $intId );

            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert property website stat record. The following error was reported.' ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

            $objDataset->cleanup();

            return false;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert property website stat record. The following error was reported.' ) );
            // Reset id on error
            $this->setId( $intId );

            while( false == $objDataset->eof() ) {
                $arrValues = $objDataset->fetchArray();
                $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
                $objDataset->next();
            }

            $objDataset->cleanup();

            return false;
        }

        $objDataset->cleanup();

        return true;
    }

    public function updateMobileStat( $boolIsUniqueStat, $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

        $strSql = 'UPDATE property_website_stats SET';

        if( true == $boolIsUniqueStat ) {
        	$strSql .= ' mobile_uniques = mobile_uniques + 1, ';
        }

        $strSql .= ' mobile_views = mobile_views + 1
				  WHERE
				    id = ' . ( int ) $this->sqlId() .
        			' AND cid = ' . $this->sqlCid();

        if( false == $objDataset->execute( $strSql ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update property website stat record. The following error was reported.' ) );
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

            $objDataset->cleanup();

            return false;
        }

        if( 0 < $objDataset->getRecordCount() ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update property website stat record. The following error was reported.' ) );
            while( false == $objDataset->eof() ) {
                $arrValues = $objDataset->fetchArray();
                $this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
                $objDataset->next();
            }

            $objDataset->cleanup();

            return false;
        }

        $objDataset->cleanup();

        return true;
    }
}
?>