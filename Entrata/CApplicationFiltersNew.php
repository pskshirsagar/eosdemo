<?php

class CApplicationFiltersNew extends CBaseApplicationFiltersNew {

	public static function fetchApplicationFiltersByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		$strSql = 'SELECT * FROM application_filters WHERE company_user_id = ' . ( int ) $intCompanyUserId . ' ORDER BY filter_name ASC';
		return self::fetchApplicationFilters( $strSql, $objDatabase );
	}

	public static function fetchConflictingCompanyUserApplicationFilterNameCount( $intCompanyUserId, $strFilterName, $objDatabase ) {
		$strWhere = 'WHERE company_user_id = ' . ( int ) $intCompanyUserId . ' AND filter_name = \'' . trim( addslashes( $strFilterName ) ) . '\'';
		return parent::fetchApplicationFilterCount( $strWhere, $objDatabase );
	}
}
?>