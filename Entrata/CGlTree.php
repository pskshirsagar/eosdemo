<?php

class CGlTree extends CBaseGlTree {

	protected $m_intIsFileAttached;
	protected $m_intPropertyCount;

	protected $m_objGlGroup;

	protected $m_arrstrAssociatedProperties;
	protected $m_arrobjGlGroups;

	const DEFAULT_IS_SYSTEM = 1;

	public function __construct() {
		parent::__construct();

		$this->m_intIsFileAttached = 1;
		return;
	}

	/**
	 * Get Functions
	 */

	public function getIsFileAttached() {
		return $this->m_intIsFileAttached;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getAssociatedProperties() {
		return $this->m_arrstrAssociatedProperties;
	}

	public function getGlGroup() {
		return $this->m_objGlGroup;
	}

	public function getGlGroups() {
		return $this->m_arrobjGlGroups;
	}

	public function getType(): string {
		return !$this->getIsSystem() ? 'Reporting Mask' : 'Chart of Accounts';
	}

	/**
	 * Set Functions
	 */

	public function setIsFileAttached( $intIsFileAttached ) {
		$this->m_intIsFileAttached = $intIsFileAttached;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setAssociatedProperties( $arrstrAssociatedProperties ) {
		$this->m_arrstrAssociatedProperties = $arrstrAssociatedProperties;
	}

	public function setGlGroup( $objGlGroup ) {
		$this->m_objGlGroup = $objGlGroup;
	}

	public function setGlGroups( $arrobjGlGroups ) {
		$this->m_arrobjGlGroups = $arrobjGlGroups;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_count'] ) ) $this->setPropertyCount( $arrmixValues['property_count'] );
		if( true == isset( $arrmixValues['associated_properties'] ) ) $this->setAssociatedProperties( $arrmixValues['associated_properties'] );

		return;
	}

	/**
	 * Create Functions
	 */

	public function createGlGroup() {

		$objGlGroup = new CGlGroup();
		$objGlGroup->setCid( $this->getCid() );
		$objGlGroup->setGlTreeId( $this->getId() );
		$objGlGroup->setShowGroupName( true );
		$objGlGroup->setShowSummaryRowName( true );

		return $objGlGroup;
	}

	public function createGlAccountTree() {

		$objGlAccountTree = new CGlAccountTree();
		$objGlAccountTree->setCid( $this->getCid() );
		$objGlAccountTree->setGlTreeId( $this->getId() );

		return $objGlAccountTree;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchGlGroups( $objClientDatabase ) {
		return CGlGroups::fetchCustomGlGroupsByGlTreeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchGlAccountTrees( $objClientDatabase ) {
		return CGlAccountTrees::fetchCustomGlAccountTreesByGlTreeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchGlGroupsByParentGlGroupId( $intParentGlGroupId, $objClientDatabase ) {
		return CGlGroups::fetchGlGroupsByGlTreeIdByParentGlGroupIdByCid( $this->getId(), $intParentGlGroupId, $this->getCid(), $objClientDatabase );
	}

	public function fetchGlGroupsByParentGlGroupIds( $arrintParentGlGroupIds, $objClientDatabase ) {
		return CGlGroups::fetchGlGroupsByGlTreeIdByParentGlGroupIdsByCid( $this->getId(), $arrintParentGlGroupIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchGlAccountTreesByGlGroupIds( $arrintGlGroupIds, $objClientDatabase ) {
		return CGlAccountTrees::fetchGlAccountTreesByCidByGlTreeIdByGlGroupIds( $this->getCid(), $this->getId(), $arrintGlGroupIds, $objClientDatabase );
	}

	public function fetchPropertyDetailsByPropertyIds( $arrintPropertyIds, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CPropertyDetails::createService()->fetchPropertyDetailsByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchGlAccountTreesRecursivelyByGlGroupId( $intGlGroupId, $objClientDatabase, $objGlTreeFilter = NULL, $objPagination = NULL, $arrintGlAccountIds = NULL, $boolIsBulkEdit = false ) {
		return CGlAccountTrees::fetchGlAccountTreesRecursivelyByCidByGlTreeIdByGlGroupId( $this->getCid(), $this->getId(), $intGlGroupId, $objClientDatabase, $objGlTreeFilter, $objPagination, $arrintGlAccountIds, $boolIsBulkEdit );
	}

	public function fetchGlAccountTreesCountRecursivelyByGlGroupId( $intGlGroupId, $objClientDatabase, $objGlTreeFilter = NULL ) {
		return CGlAccountTrees::fetchGlAccountTreesCountRecursivelyByCidByGlTreeIdByGlGroupId( $this->getCid(), $this->getId(), $intGlGroupId, $objClientDatabase, $objGlTreeFilter );
	}

	public function fetchGlAccountsRecursivelyByGlGroupId( $intGlGroupId, $objClientDatabase ) {
		return CGlAccounts::fetchGlAccountsRecursivelyByCidByGlTreeIdByGlGroupId( $this->getCid(), $this->getId(), $intGlGroupId, $objClientDatabase );
	}

	public function fetchParentGlGroups( $objClientDatabase ) {
		return CGlGroups::fetchParentGlGroupsByGlTreeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlTreeTypeId() {

		$boolIsValid = true;

		if( false == valStr( $this->getGlTreeTypeId() ) && false == is_numeric( $this->getGlTreeTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_tree_id', __( 'Tree type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRestrictedGlTreeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlChartId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( ( $this->getIsSystem() ? 'Chart' : 'Mask' ) . ' Name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAcccountNumberPattern() {

		$boolIsValid = true;

		if( false == valStr( $this->getAccountNumberPattern() ) && false == is_numeric( $this->getAccountNumberPattern() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_number_pattern', __( 'Account number pattern is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDeletedGlTree() {

		$boolIsValid = true;

		if( true == is_numeric( $this->getDeletedBy() ) || true == valStr( $this->getDeletedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_by', __( 'This Reporting Mask has already been deleted.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAccountNumberDelimiter() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRestricted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCategorized() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFileAttached() {
		$boolIsValid = true;

		if( 0 == $this->getIsFileAttached() ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_file', __( 'No file was uploaded. Select a file to upload.' ) ) );
		}

		return $boolIsValid;
	}

	public function valReorderGlGroups( $objDatabase ) {

		$boolIsValid			= true;
		$arrmixGlGroupsData	    = ( array ) CGlGroups::fetchCircularGlGroupsByGlTreeIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrmixGlGroupsData ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_reorder', __( 'Gl groups reorder failed.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valAccountNumber( $objDatabase ) {
		$boolIsValid = true;

		$objGlTree	= \Psi\Eos\Entrata\CGlTrees::createService()->fetchGlTreeByIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$intOldAccountNumberPatternCharCount = array_sum( explode( ',', str_replace( '[', '', str_replace( ']', '', str_replace( '][', ',', $objGlTree->getAccountNumberPattern() ) ) ) ) );
		$intNewAccountNumberPatternCharCount = array_sum( explode( ',', str_replace( '[', '', str_replace( ']', '', str_replace( '][', ',', $this->getAccountNumberPattern() ) ) ) ) );

		if( $intNewAccountNumberPatternCharCount >= $intOldAccountNumberPatternCharCount ) {
			return $boolIsValid;
		}

		$arrmixDuplicateGlAccountTrees = rekeyArray( 'new_formatted_account_number', ( array ) \Psi\Eos\Entrata\CGlAccountTrees::createService()->fetchDuplicateFormattedAccountNumberGlAccountTreesByGlTreeIdByCid( $this, $this->getCid(), $objDatabase ), false, true );

		if( true == valArr( $arrmixDuplicateGlAccountTrees ) ) {

			$strDuplicateGlAccounts = '';

			foreach( $arrmixDuplicateGlAccountTrees as $arrmixGlAccountTrees ) {
				$strDuplicateGlAccounts .= ' [ ';
				foreach( $arrmixGlAccountTrees as $arrmixGlAccountTree ) {
					$strDuplicateGlAccounts .= $arrmixGlAccountTree['formatted_account_number'] . ' ' . $arrmixGlAccountTree['account_name'] . ', ';
				}
				$strDuplicateGlAccounts = substr( $strDuplicateGlAccounts, 0, -2 ) . ' ], ';
			}

			$strDuplicateGlAccounts = substr( $strDuplicateGlAccounts, 0, -2 );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'formatted_account_number', __( 'Changing the formatting would cause the following account to have duplicate account numbers. Please edit the account numbers to avoid duplicates and try again. {%s, 0}', [ $strDuplicateGlAccounts ] ) ) );

			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valGlTreeTypeId();
				$boolIsValid &= $this->valAcccountNumberPattern();
				$boolIsValid &= $this->valIsFileAttached();
				if( false == is_null( $objDatabase ) ) {
					$boolIsValid &= $this->valAccountNumber( $objDatabase );
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeletedGlTree();
				break;

			case 'reorder_gl_groups':
				$boolIsValid &= $this->valReorderGlGroups( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function generateGlTreeHierarchy( $objDatabase, $arrobjGlGroups = NULL, $boolIsFromAccountingReports = false, $intCurrentTabGlGroupId = NULL, $objGlTreeFilter = NULL ) {

		$arrintGlGroupIds					= [];
		$arrobjGlTreeHierarchy				= [];
		$arrobjClonedGlGroups				= [];
		$arrobjParentWiseGlGroups 		    = [];
		$arrobjGroupWiseGlAccountTrees		= [];
		$arrobjModifiedParentWiseGlGroups   = [];
		$arrintModifiedParentWiseGlGroups	= [];

		if( false == valArr( $arrobjGlGroups ) ) {
			$arrobjGlGroups	= ( array ) CGlGroups::fetchCustomGlGroupsByGlTreeIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		}

		$arrobjGlAccountTrees				= ( array ) CGlAccountTrees::fetchCustomGlAccountTreesByGlTreeIdByCid( $this->getId(), $this->getCid(), $objDatabase, $objGlTreeFilter );
		$arrobjEnabledMappedGlAccountTrees	= rekeyObjects( 'GlAccountTreeId', ( array ) CGlAccountTrees::fetchEnabledGlAccountTreesByGlAccountTreeIdsByCid( array_filter( array_keys( $arrobjGlAccountTrees ) ), $this->getCid(), $objDatabase ) );

		foreach( $arrobjGlAccountTrees as $objGlAccountTree ) {

			//	if any of the mapped account is enabled then make child as enabled
			if( true == array_key_exists( $objGlAccountTree->getId(), $arrobjEnabledMappedGlAccountTrees ) ) {
				$objGlAccountTree->setDisabledBy( NULL );
			}

			$arrobjGroupWiseGlAccountTrees[$objGlAccountTree->getGlGroupId()][$objGlAccountTree->getId()] = $objGlAccountTree;
		}

		// adding gl accounts to net cash flow types Group
		// collecting gl groups of type net cash flow group
		foreach( $arrobjGlGroups as $objGlGroup ) {
			if( CGlGroupType::NET_CASH_FLOW_GROUP == $objGlGroup->getGlGroupTypeId() ) {
				$arrintGlGroupIds[] = $objGlGroup->getId();
			}
		}

		$arrobjNetCashFlowGlAccountTrees = ( array ) CGlAccountTrees::fetchNetCashFlowGlAccountTreesByGlGroupIdsByCid( $arrintGlGroupIds, $this->getCid(), $objDatabase, $objGlTreeFilter );

		foreach( $arrobjNetCashFlowGlAccountTrees as $objNetCashFlowGlAccountTree ) {
			$arrobjGroupWiseGlAccountTrees[$objNetCashFlowGlAccountTree->getGlGroupId()][$objNetCashFlowGlAccountTree->getId()] = $objNetCashFlowGlAccountTree;
		}

		foreach( $arrobjGlGroups as $intGlGroupId => $objGlGroup ) {

			$arrobjClonedGlGroups[$intGlGroupId] = clone $objGlGroup;

			if( true == array_key_exists( $objGlGroup->getId(), $arrobjGroupWiseGlAccountTrees ) ) {

				$objGlGroup->setGlAccountTrees( $arrobjGroupWiseGlAccountTrees[$objGlGroup->getId()] );
				$arrobjClonedGlGroups[$intGlGroupId]->setGlAccountTrees( $arrobjGroupWiseGlAccountTrees[$objGlGroup->getId()] );
			}

			$intParentGlGroupId = ( false == is_numeric( $objGlGroup->getParentGlGroupId() ) ) ? 'main_groups' : $objGlGroup->getParentGlGroupId();

			if( false == $boolIsFromAccountingReports ) {
				$arrobjParentWiseGlGroups[$intParentGlGroupId][$objGlGroup->getId()] = $arrobjClonedGlGroups[$intGlGroupId];
			} else {
				$arrobjParentWiseGlGroups[$intParentGlGroupId][$objGlGroup->getId()] = $objGlGroup;
			}
		}

		if( false == $boolIsFromAccountingReports ) {

			foreach( $arrobjClonedGlGroups as $objGlGroup ) {

				if( true == isset( $arrobjParentWiseGlGroups[$objGlGroup->getId()] ) ) {
					$arrobjClonedGlGroups[$objGlGroup->getId()]->setGlGroups( $arrobjParentWiseGlGroups[$objGlGroup->getId()] );
				}
			}

			if( true == array_key_exists( 'main_groups', $arrobjParentWiseGlGroups ) ) {

				foreach( $arrobjParentWiseGlGroups['main_groups'] as $objGlGroup ) {

					$arrintModifiedParentWiseGlGroups['main_groups'][$objGlGroup->getId()] = $objGlGroup->getId();

					if( true == valArr( $objGlGroup->getGlGroups() ) ) {
						$this->getRecursiveGlGroups( $objGlGroup->getGlGroups(), $objGlGroup->getId(), $arrintModifiedParentWiseGlGroups );
					}
				}

				foreach( $arrobjGlGroups as $intGlGroupId => $objGlGroup ) {
					if( true == array_key_exists( $intGlGroupId, $arrintModifiedParentWiseGlGroups ) ) {

						foreach( $arrintModifiedParentWiseGlGroups[$intGlGroupId] as $intChildGlGroupId ) {
							$arrobjModifiedParentWiseGlGroups[$intGlGroupId][$intChildGlGroupId] = $arrobjGlGroups[$intChildGlGroupId];
						}
					}
				}

				foreach( $arrobjGlGroups as $intGlGroupId => $objGlGroup ) {

					if( true == isset( $arrobjModifiedParentWiseGlGroups[$objGlGroup->getId()] ) ) {
						$arrobjGlGroups[$objGlGroup->getId()]->setGlGroups( $arrobjModifiedParentWiseGlGroups[$objGlGroup->getId()] );
					}
				}
			}

			if( true == array_key_exists( 'main_groups', $arrintModifiedParentWiseGlGroups ) ) {
				foreach( $arrintModifiedParentWiseGlGroups['main_groups'] as $intParentGlGroupId ) {
					$arrobjGlTreeHierarchy[$intParentGlGroupId] = $arrobjGlGroups[$intParentGlGroupId];
				}
			}

		} else {

			foreach( $arrobjGlGroups as $objGlGroup ) {
				if( true == isset( $arrobjParentWiseGlGroups[$objGlGroup->getId()] ) ) {
					$objGlGroup->setGlGroups( $arrobjParentWiseGlGroups[$objGlGroup->getId()] );
				}
			}

			if( true == array_key_exists( 'main_groups', $arrobjParentWiseGlGroups ) ) {

				foreach( $arrobjParentWiseGlGroups['main_groups'] as $objGlGroup ) {
					$objGlGroup->gl_account_grouppth( 0 );
					$arrobjGlTreeHierarchy[$objGlGroup->getId()] = $objGlGroup;
				}
			}
		}

		//	condition for Income Statement or Balance Sheet
		if( false == is_null( $intCurrentTabGlGroupId ) && true == array_key_exists( $intCurrentTabGlGroupId, $arrobjGlTreeHierarchy ) && true == $boolIsFromAccountingReports ) {
			$arrmixCurrentTabAllGlGroups[$intCurrentTabGlGroupId] = $arrobjGlTreeHierarchy[$intCurrentTabGlGroupId];

			if( true == valArr( $arrobjGlTreeHierarchy[$intCurrentTabGlGroupId]->getGlGroups() ) ) {
				$this->setGlGroups( $arrmixCurrentTabAllGlGroups );
			}
		} else {
			$this->setGlGroups( $arrobjGlTreeHierarchy );
		}
	}

	private function getRecursiveGlGroups( $arrobjGlGroups, $intParentGlGroupId, &$arrintParentWiseGlGroups ) {

		if( false == valArr( $arrobjGlGroups ) ) {
			return NULL;
		}

		$intTemporaryParentGlGroupId = $intParentGlGroupId;

		foreach( $arrobjGlGroups as $objGlGroup ) {

			if( CGlGroupType::STANDARD_GROUP == $objGlGroup->getGlGroupTypeId() ) {
				$intTemporaryParentGlGroupId = $objGlGroup->getId();
			}

			$intTemporaryParentGlGroupId = $this->getRecursiveGlGroups( $objGlGroup->getGlGroups(),  $intTemporaryParentGlGroupId, $arrintParentWiseGlGroups );

			if( $intTemporaryParentGlGroupId != $intParentGlGroupId ) {
				$intTemporaryParentGlGroupId = $intParentGlGroupId;
			}

			$objGlGroup->setGlGroups( [] );
			$arrintParentWiseGlGroups[$intTemporaryParentGlGroupId][$objGlGroup->getId()] = $objGlGroup->getId();
		}

		return $intTemporaryParentGlGroupId;
	}

	//	INPUT: Accepts gl_accounts array, in a format of [gl_group_id][] = array( gl_account_id => 1, amount => 100, amount2 => 200, account_name, number ), array( ... )
	//	INPUT: Accepts a optional array of of fields that need to be totalled (eg: amount ), within every group
	//	Returns a collapsed data structure that can be directly looped over
	//	Takes care of flattened groups ( GL Group type Total Group  i.e. 2 )
	//	Returns data in a form of
	//  'collapsed_tree'      => array( 'depth' => value, 'item' => $objGroup or $arrGlAccounts ), array(....),
	//  'gl_account_type_totals'  => ( CGlAccountType::ASSETS => 'sum', ..)
	//	NOTE: IGNORE OTHER OPTIONAL PARAMETERS, THEY ARE USED FOR RECURSION

	public function getCollapsedGlTreeHierarchyByGlSystemCodeByGlAccounts( $strSystemCode, $arrmixGlAccounts = NULL, $arrstrTotalFields = [], $objGroups = NULL, $intGroupDepth = 0, $boolParentGlGroupIsFlattened = false, &$arrmixCollapsedData = NULL ) {

		$boolFirstCall = false;
		//	If the depth is 0 (the start), initalize return array
		if( NULL == $arrmixCollapsedData ) {
			$boolFirstCall = true;
			//	Initialing base gl_account_types (Ignoring Income/Expenses, as the immediate child Groups under INCST are not static)
			$arrmixCollapsedData['gl_account_type_totals'][CGlAccountType::ASSETS]    	= NULL;
			$arrmixCollapsedData['gl_account_type_totals'][CGlAccountType::EQUITY]    	= NULL;
			$arrmixCollapsedData['gl_account_type_totals'][CGlAccountType::LIABILITIES]	= NULL;
			$arrmixCollapsedData['gl_account_type_totals'][CGlAccountType::INCOME]    	= NULL;
			$arrmixCollapsedData['gl_account_type_totals'][CGlAccountType::EXPENSES]		= NULL;
			//	We store the actual collapsed gl_tree hierachy here..
			$arrmixCollapsedData['collapsed_tree'] = [];

			$boolSystemCodeGroupFound = false;
			//	loop all groups until we find the passed system_code group
			foreach( $this->getGlGroups() as $objGlGroup ) {
				if( $objGlGroup->getSystemCode() === $strSystemCode ) {
					$objGroups = $objGlGroup->getGlGroups();
					$boolSystemCodeGroupFound = true;
					break;
				}
			}
			if( false == $boolSystemCodeGroupFound ) {
				return false;
			}
		}

		//	Used to exit from recursion  when empty group occurs
		if( false == valArr( $objGroups ) ) {
			return false;
		}

		//	Total of groups passed in the recursive  call
		$arrfltTotalsOfAllGroups = array_fill_keys( $arrstrTotalFields, 0 );

		//	Used if a group is flattened ( GL Group type Total Group  i.e. 2 ), stores its child group in a comma separated format (display text of group total)
		$strNamesOfChildGroups = NULL;

		foreach( $objGroups as $objGroup ) {

			//	Store only non-flattend groups, as flattened groups ( GL Group type Standard Group i.e. 1 ) are hidden
			if( CGlGroupType::STANDARD_GROUP == $objGroup->getGlGroupTypeId() ) {
				$arrmixCollapsedData['collapsed_tree'][] = [ 'depth' => $intGroupDepth, 'item' => $objGroup, 'gl_account_type_id' => $objGroup->getGlAccountTypeId() ];
			}

			//	if parent group is flattened ( GL Group type Total Group  i.e. 2 ), append the group names and return to parent,
			if( true == $boolParentGlGroupIsFlattened ) {
				$strNamesOfChildGroups .= $objGroup->getName() . ', ';
			}

			$arrfltTotalsOfCurrentGroup = array_fill_keys( $arrstrTotalFields, 0 );

			//	Calculate total of all gl_accounts within the group
			if( true == valArr( $arrmixGlAccounts ) && true == array_key_exists( $objGroup->getId(), $arrmixGlAccounts ) ) {
				foreach( $arrmixGlAccounts[$objGroup->getId()] as $arrmixGlAccount ) {
					//	Add entry of current gl_account in its group
					$arrmixCollapsedData['collapsed_tree'][] = [ 'depth' => $intGroupDepth + ( ( CGlGroupType::STANDARD_GROUP == $objGroup->getGlGroupTypeId() ) ? 1 : 0 ), 'item' => $arrmixGlAccount, 'underline' => false, 'bold' => false, 'gl_account_type_id' => $objGroup->getGlAccountTypeId() ];

					foreach( $arrstrTotalFields as $strFieldName ) {
						$arrfltTotalsOfCurrentGroup[$strFieldName] += ( true == isset( $arrmixGlAccount[$strFieldName] ) ? $arrmixGlAccount[$strFieldName] : 0 );
					}
				}
			}

			//	Recursive call to get Group-wise total of all Child GL Groups
			$arrmixChildGroupsDetail  = $this->getCollapsedGlTreeHierarchyByGlSystemCodeByGlAccounts( $strSystemCode, $arrmixGlAccounts, $arrstrTotalFields, $objGroup->getGlGroups(), ( $intGroupDepth + ( ( CGlGroupType::STANDARD_GROUP == $objGroup->getGlGroupTypeId() ) ? 1 : 0 ) ), $objGroup->getGlGroupTypeId(), $arrmixCollapsedData );

			foreach( $arrstrTotalFields as $strFieldName ) {
				if( true == isset( $arrmixChildGroupsDetail['group_totals'][$strFieldName] ) ) {
					$arrfltTotalsOfCurrentGroup[$strFieldName] += $arrmixChildGroupsDetail['group_totals'][$strFieldName];
				}
				$arrfltTotalsOfAllGroups[$strFieldName] += $arrfltTotalsOfCurrentGroup[$strFieldName];
			}

			if( true == valArr( $arrstrTotalFields ) ) {

				if( CGlGroupType::STANDARD_GROUP == $objGroup->getGlGroupTypeId() ) {
						//	Group Not flattened ( GL Group type Standard Group i.e. 1 ), we attach the total to the group caption
						$arrmixGroupTotal = [ 'group_summary_caption' => 'Total ' . $objGroup->getName(), 'group_summary_totals' => $arrfltTotalsOfCurrentGroup ];
						$arrmixCollapsedData['collapsed_tree'][] = [ 'depth' => -1, 'item' => $arrmixGroupTotal, 'gl_account_type_id' => $objGroup->getGlAccountTypeId() ];
				} else {
					//	Group is flattened, we store the group total caption as a comma separated text of all its immediate child groups ( Other Liabilities, Accounts Payable .. )
					$arrmixGroupTotal = [ 'group_summary_caption' => $objGroup->getName(), 'group_summary_totals' => $arrfltTotalsOfCurrentGroup ];
					$arrmixCollapsedData['collapsed_tree'][] = [ 'depth' => -1, 'item' => $arrmixGroupTotal, 'underline' => false, 'bold' => true, 'gl_account_type_id' => $objGroup->getGlAccountTypeId(), 'gl_group_type_id' => CGlGroupType::TOTAL_GROUP ];
				}
			}

			//	Store the grand total of only base groups (Assets, liabilities.... etc)
			if( true == $boolFirstCall ) {
				$arrmixCollapsedData['gl_account_typ_totals'][$objGroup->getGlAccountTypeId()] = $arrfltTotalsOfCurrentGroup;
			} else {
				$arrmixCollapsedData['gl_account_type_individual_totals'][$objGroup->getGlAccountTypeId()] = $arrfltTotalsOfCurrentGroup;
			}
		}
		if( $boolFirstCall == false ) {
			//	Return details of current group
			return [ 'group_totals' => $arrfltTotalsOfAllGroups, 'has_child_flattened' => $objGroup->getGlGroupTypeId() ];
		} else {
			//	Return collapsed_data
			return $arrmixCollapsedData;
		}
	}

	public function log( $intCompanyUserId, $objClientDatabase ) {

		$strGlTreeDescription		= '';
		$strOldGlTreeKeyValuePair	= '';

		$strGlTreeDescription		= 'Reporting Mask "' . $this->getName() . '" has been deleted.';
		$strOldGlTreeKeyValuePair	= $this->mapKeyValuePair();

		$objTableLog = new CTableLog();

		return $objTableLog->insert( $intCompanyUserId, $objClientDatabase, 'gl_trees', $this->getId(), 'DELETE', $this->getCid(), $strOldGlTreeKeyValuePair, NULL, $strGlTreeDescription );

	}

	public function mapKeyValuePair() {

		$strKeyValuePair = '';

		$strKeyValuePair .= 'GL Tree Id::' . $this->getId() . '~^~';
		$strKeyValuePair .= 'Client Id::' . $this->getCid() . '~^~';
		$strKeyValuePair .= 'GL Tree Type Id::' . $this->getGlTreeTypeId() . '~^~';
		$strKeyValuePair .= 'GL Tree Name::' . $this->getName() . '~^~';

		return $strKeyValuePair;
	}

}
?>