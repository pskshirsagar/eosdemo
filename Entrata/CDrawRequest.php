<?php

class CDrawRequest extends CBaseDrawRequest {

	protected $m_strCompanyName;
	protected $m_intApPayeeTypeId;
	protected $m_strArCodeName;
	protected $m_arrintFileIds;
	protected $m_boolIsRecentFinalized;
	protected $m_fltFundingAmount;
	protected $m_fltTotalDrawnAmount;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) )			$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['ap_payee_type_id'] ) )		$this->setApPayeeTypeId( $arrmixValues['ap_payee_type_id'] );
		if( true == isset( $arrmixValues['bank_account_name'] ) )		$this->setBankAccountName( $arrmixValues['bank_account_name'] );
		if( true == isset( $arrmixValues['ar_code_name'] ) )			$this->setArCodeName( $arrmixValues['ar_code_name'] );
		if( true == isset( $arrmixValues['is_recent_finalized'] ) )		$this->setIsRecentFinalized( $arrmixValues['is_recent_finalized'] );
		if( true == isset( $arrmixValues['funding_amount'] ) )          $this->setFundingAmount( $arrmixValues['funding_amount'] );
		if( true == isset( $arrmixValues['total_drawn_amount'] ) )      $this->setTotalDrawnAmount( $arrmixValues['total_drawn_amount'] );

	}

	public function getTotalDrawnAmount() {
		return $this->m_fltTotalDrawnAmount;
	}

	public function setTotalDrawnAmount( $fltTotalDrawnAmount ) {
		$this->m_fltTotalDrawnAmount = CStrings::strToFloatDef( $fltTotalDrawnAmount, NULL, false, 2 );
	}

	public function getFundingAmount() {
		return $this->m_fltFundingAmount;
	}

	public function setFundingAmount( $fltFundingAmount ) {
		$this->m_fltFundingAmount = CStrings::strToFloatDef( $fltFundingAmount, NULL, false, 2 );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function getApPayeeTypeId() {
		return $this->m_intApPayeeTypeId;
	}

	public function setApPayeeTypeId( $intApPayeeTypeId ) {
		$this->m_intApPayeeTypeId = $intApPayeeTypeId;
	}

	public function getBankAccountName() {
		return $this->m_strBankAccountName;
	}

	public function setBankAccountName( $strBankAccountName ) {
		$this->m_strBankAccountName = $strBankAccountName;
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function setFileIds( $arrintFileIds ) {
		$this->m_arrintFileIds = $arrintFileIds;
	}

	public function getFileIds() {
		return $this->m_arrintFileIds;
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = $strArCodeName;
	}

	public function setIsRecentFinalized( $boolIsRecentFinalized ) {
		$this->m_boolIsRecentFinalized = $boolIsRecentFinalized;
	}

	public function getIsRecentFinalized() {
		return $this->m_boolIsRecentFinalized;
	}

	public function valApPayeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApPayeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Funding Source is required.' ) );
		}

		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', 'Charge code is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBankAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getBankAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_id', 'Bank account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDrawAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getDrawAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'draw_amount', 'Draw amount is required.' ) );
		}

		if( 0 >= $this->getDrawAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'draw_amount', 'Draw amount should not be zero or negative.' ) );
		}

		return $boolIsValid;
	}

	public function valDrawDate() {
		$boolIsValid	= true;

		if( true == is_null( $this->getDrawDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_datetime', 'Draw date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valArCodeId();
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valDrawAmount();
				$boolIsValid &= $this->valDrawDate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createDrawRequestDetail() {

		$objDrawRequestDetail = new CDrawRequestDetail();
		$objDrawRequestDetail->setCid( $this->getCid() );
		$objDrawRequestDetail->setDrawRequestId( $this->getId() );

		return $objDrawRequestDetail;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchEvents( $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByReferenceIdByReferenceTypeIdByEventSubTypeIdsByCid( $this->getId(), CEventReferenceType::DRAW_REQUEST, CEventSubType::$c_arrintDrawRequestEventSubTypeIds, $this->getCid(), $objDatabase );
	}

}
?>