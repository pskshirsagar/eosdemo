<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportItemDescriptions
 * Do not add any new functions to this class.
 */

class CReportItemDescriptions extends CBaseReportItemDescriptions {

	/**
	 * @param $intDefaultReportVersionId
	 * @param $intClusterId
	 * @param $objDatabase
	 * @return CReportItemDescription[]
	 */
	public static function fetchReportItemDescriptionsByDefaultReportIdByClusterId( $intDefaultReportVersionId, $intClusterId, $objDatabase ) {
		return parent::fetchReportItemDescriptions( sprintf( 'SELECT * FROM report_item_descriptions WHERE default_report_version_id = %d AND deleted_by IS NULL AND deleted_on IS NULL ORDER BY order_num ASC', ( int ) $intDefaultReportVersionId ), $objDatabase );
	}

	public static function fetchReportItemDescriptionsUsersByDefaultReportVersionIdByClusterId( $intDefaultReportVersionId, $intSelectedClusterId, $objDatabase ) {
		$strSql = ' SELECT
						rid.*,
						rit.name AS report_item_type,
						COALESCE( func_format_customer_name( ce.name_first, ce.name_last ), cu.username ) AS updated_by
					FROM
						report_item_descriptions rid
						JOIN company_users cu ON ( cu.cid = rid.default_cid AND cu.id = COALESCE ( rid.updated_by, rid.created_by ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						JOIN report_item_types rit ON ( rit.id = rid.report_item_type_id )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						rid.default_cid = 1
						AND rid.default_report_version_id = ' . ( int ) $intDefaultReportVersionId . '
						AND rit.id IN ( ' . CReportItemType::COLUMNS . ', ' . CReportItemType::FILTERS . ' )
						AND rid.deleted_by IS NULL
						AND rid.deleted_on IS NULL
					ORDER BY
						rid.order_num ASC,
						rid.created_on DESC;';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMaxReportItemDescriptionId( $objDatabase ) {
		$strSql = 'SELECT
						MAX( id ) AS id
					FROM
						report_item_descriptions';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMaxOrderNumForReportItemDescriptionByDefaultReportVersionIdByReportItemIdByClusterId( $intDefaultReportVersionId, $intReportItemTypeId, $intClusterId, $objDatabase ) {
		$strSql = 'SELECT
						MAX( order_num ) AS max_order_num
					FROM
						report_item_descriptions
					WHERE
						default_report_version_id = ' . ( int ) $intDefaultReportVersionId . '
						AND report_item_type_id	= ' . ( int ) $intReportItemTypeId;
		return fetchData( $strSql, $objDatabase );
	}

}
?>