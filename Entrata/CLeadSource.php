<?php

class CLeadSource extends CBaseLeadSource {

	protected $m_intApplicationsCount;
	protected $m_intGuestCardsCount;
	protected $m_intPsProductId;
	protected $m_intCompletedApplicationsCount;
	protected $m_intPhoneNumber;
	protected $m_intPropertyId;

	protected $m_strUserName;
	protected $m_strLeadSourceCategoryName;
	protected $m_strAssociatedIlsName;
	protected $m_boolIsPublishedPropertyLeadSource;

    /**
     * GET Functions
     */

    public function getLeadSourceCategoryName() {
    	return $this->m_strLeadSourceCategoryName;
    }

    public function getApplicationsCount() {
		return $this->m_intApplicationsCount;
    }

    public function getGuestCardsCount() {
		return $this->m_intGuestCardsCount;
    }

    public function getPsProductId() {
    	return $this->m_intPsProductId;
    }

    public function getCompletedApplicationsCount() {
		return $this->m_intCompletedApplicationsCount;
	}

	public function getPhoneNumber() {
		return $this->m_intPhoneNumber;
	}

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function getUserName() {
    	return $this->m_strUserName;
    }

    public function getIsPublishedPropertyLeadSource() {
    	return $this->m_boolIsPublishedPropertyLeadSource;
    }

    public function getAssociatedIlsName() {
    	return $this->m_strAssociatedIlsName;
    }

    /**
     * SET Functions
     */

	public function setLeadSourceCategoryName( $strLeadSourceCategoryName ) {
		$this->m_strLeadSourceCategoryName = $strLeadSourceCategoryName;
	}

    public function setApplicationsCount( $intApplicationsCount ) {
		$this->m_intApplicationsCount = $intApplicationsCount;
    }

    public function setGuestCardsCount( $intGuestCardsCount ) {
		$this->m_intGuestCardsCount = $intGuestCardsCount;
    }

    public function setPsProductId( $intPsProductId ) {
    	$this->m_intPsProductId = $intPsProductId;
    }

	public function setCompletedApplicationsCount( $intCompletedApplicationsCount ) {
		$this->m_intCompletedApplicationsCount = $intCompletedApplicationsCount;
    }

    public function setPhoneNumber( $intPhoneNumber ) {
    	$this->m_intPhoneNumber = $intPhoneNumber;
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
    }

    public function setUserName( $strUserName ) {
    	$this->m_strUserName = $strUserName;
    }

    public function setIsPublishedPropertyLeadSource( $boolIsPublishedPropertyLeadSource ) {
    	$this->m_boolIsPublishedPropertyLeadSource = $boolIsPublishedPropertyLeadSource;
    }

	public function setAssociatedIlsName( $strAssociatedIlsName ) {
		$this->m_strAssociatedIlsName = $strAssociatedIlsName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrmixValues['lead_source_category_name'] ) ) $this->setLeadSourceCategoryName( $arrmixValues['lead_source_category_name'] );
    	if( true == isset( $arrmixValues['applications_count'] ) ) $this->setApplicationsCount( $arrmixValues['applications_count'] );
    	if( true == isset( $arrmixValues['guest_cards_count'] ) ) $this->setGuestCardsCount( $arrmixValues['guest_cards_count'] );
    	if( true == isset( $arrmixValues['ps_product_id'] ) ) $this->setPsProductId( $arrmixValues['ps_product_id'] );
    	if( true == isset( $arrmixValues['completed_applications_count'] ) ) $this->setCompletedApplicationsCount( $arrmixValues['completed_applications_count'] );
    	if( true == isset( $arrmixValues['phone_number'] ) ) $this->setPhoneNumber( $arrmixValues['phone_number'] );
    	if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['user_name'] ) ) $this->setUserName( $arrmixValues['user_name'] );
    	if( true == isset( $arrmixValues['is_published_property_lead_source'] ) ) $this->setIsPublishedPropertyLeadSource( $arrmixValues['is_published_property_lead_source'] );
    	if( true == isset( $arrmixValues['associated_ils_name'] ) ) $this->setAssociatedIlsName( $arrmixValues['associated_ils_name'] );
    }

    /**
     * Create Functions
     */

    public function createPropertyLeadSource() {

        $objPropertyLeadSource = new CPropertyLeadSource();
        $objPropertyLeadSource->setCid( $this->m_intCid );
        $objPropertyLeadSource->setLeadSourceId( $this->m_intId );

        return $objPropertyLeadSource;
    }

    public function createCampaign() {

    	$objCampaign = new CCampaign();
    	$objCampaign->setCid( $this->getCid() );
    	$objCampaign->setLeadSourceId( $this->m_intId );
    	$objCampaign->setName( $this->m_strName );
    	$objCampaign->setDescription( $this->m_strDescription );

    	return $objCampaign;
    }

    /**
     * Fetch Functions
     */

	public function fetchPropertyLeadSourceByPropertyId( $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceByLeadSourceIdByPropertyIdByCid( $this->m_intId, $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeadSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesByLeadSourceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationDuplicate( $objClientDatabase ) {
		return CApplicationDuplicates::fetchApplicationDuplicateByLeadSourceIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApplication( $objClientDatabase ) {
		return CApplications::fetchApplicationByLeadSourceIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchCustomer( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByLeadSourceIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyLeadSource( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyByLeadSourceIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

	}

	/**
	 * Other Functions
	 */

	public function prepareIlsLeadTrackerEmailId( $intPropertyId, $intInternetListingServiceId ) {
		$strIlsLeadTrackerEmailId = $intPropertyId . '-' . $this->m_intId . '-' . $intInternetListingServiceId . '-' . $this->m_intCid . '@guestcardlead.com';
		return $strIlsLeadTrackerEmailId;
	}

    /**
     * Validation Functions
     */

    public function valName( $objDatabase = NULL ) {

    	$boolIsValid = true;

        if( false == isset( $this->m_strName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please enter a Lead Source name.' ) ) );

        } elseif( 3 > \Psi\CStringService::singleton()->strlen( $this->m_strName ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Lead Source name must be at least 3 characters in length.' ) ) );
        }

		if( true == isset( $objDatabase ) ) {
			$objLeadSource = \Psi\Eos\Entrata\CLeadSources::createService()->fetchDuplicateLeadSourceByIdByNameByCid( $this->getId(), $this->getName(), $this->getCid(), $objDatabase );
			if( true == is_numeric( $this->getId() ) ) {
				$objOriginalLeadSource = \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
			}
		}

		if( ( false == isset ( $objOriginalLeadSource ) || $this->getName() != $objOriginalLeadSource->getName() ) && true == valObj( $objLeadSource, 'CLeadSource' ) ) {
			$boolIsValid = false;
       		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Lead Source name already exists.' ) ) );
		}

        return $boolIsValid;
    }

    public function valLeadSourceDisassocaition( $boolIsValidPropertyAssociation, $objClientDatabase ) {

    	if( true == $boolIsValidPropertyAssociation ) {
    		$boolIsValid = ( false == $this->fetchApplicationDuplicate( $objClientDatabase ) && false == $this->fetchApplication( $objClientDatabase ) && false == $this->fetchCustomer( $objClientDatabase ) && false == $this->fetchPropertyLeadSource( $objClientDatabase ) ) ? false : true;
    	} else {
			$boolIsValid = ( false == $this->fetchApplicationDuplicate( $objClientDatabase ) && false == $this->fetchApplication( $objClientDatabase ) && false == $this->fetchCustomer( $objClientDatabase ) ) ? false : true;
    	}

    	return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL, $boolIsValidPropertyAssociation = false ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_DELETE:
              	$boolIsValid &= $this->valLeadSourceDisassocaition( $boolIsValidPropertyAssociation, $objDatabase );
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }
	   return $boolIsValid;
    }

}
?>