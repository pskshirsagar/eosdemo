<?php

class CPropertyLeasingGoal extends CBasePropertyLeasingGoal {

    /**
    * Validation Functions
    *
    */

    public function valId() {

        if( true == is_null( $this->getId() ) || 0 >= ( int ) $this->getId() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Property leasing goal id required' ) );
        	return false;
        }

        return true;
    }

    public function valCid() {

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'client id required.' ) );
			return false;
		}

		return true;
    }

    public function valPropertyId() {

		if( true == is_null( $this->getPropertyId() ) || 0 >= ( int ) $this->getPropertyId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Property id required.' ) );
			return false;
		}

		return true;
    }

    public function valLeaseTermId() {

		if( true == is_null( $this->getLeaseTermId() ) || 0 >= ( int ) $this->getLeaseTermId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Lease term id required.' ) );
			return false;
		}

		return true;
    }

    public function valLeaseStartWindowId() {

		if( true == is_null( $this->getLeaseStartWindowId() ) || 0 >= ( int ) $this->getLeaseStartWindowId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Lease start window id required.' ) );
			return false;
		}

		return true;
    }

    public function valPropertyFloorplanId() {

       	if( true == is_null( $this->getPropertyFloorplanId() ) || 0 >= ( int ) $this->getPropertyFloorplanId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Property floorplan id required.' ) );
			return false;
		}

		return true;
    }

    public function valGoalDate() {
    	$boolIsValid = true;

		if( false == is_numeric( strtotime( $this->getGoalDate() ) ) || false == CValidation::validateDate( date( 'Y/m/d', strtotime( $this->getGoalDate() ) ), $boolFormat = false ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'goal_date', 'Valid goal date is required.', NULL ) );
			$boolIsValid &= false;
		} elseif( true == is_null( $this->getGoalDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'goal_date', 'Goal date is required.', NULL ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
    }

    public function valGoalCount() {

        if( true == is_null( $this->getGoalCount() ) || 0 > ( int ) $this->getGoalCount() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Goal count required.' ) );
        	return false;
        }

        return true;
    }

    public function valIsRenewal() {

        if( true == is_null( $this->getIsRenewal() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_renewal', 'Renewal can not be null.' ) );
        	return false;
        }

        return true;
    }

    public function valIsPublished() {

        if( true == is_null( $this->getIsPublished() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', 'Published can not be null.' ) );
        	return false;
        }

        return true;
    }

    public function validate( $strAction, $boolIsFromInsert = false, $boolIsInsertPropertyFloorplan = false ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		if( false == $boolIsFromInsert ) {
        			$boolIsValid &= $this->valId();
        		}
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valPropertyId();
        		$boolIsValid &= $this->valLeaseTermId();
        		$boolIsValid &= $this->valLeaseStartWindowId();
        		if( true == $boolIsInsertPropertyFloorplan ) {
        			$boolIsValid &= $this->valPropertyFloorplanId();
        		}
        		$boolIsValid &= $this->valGoalDate();
        		$boolIsValid &= $this->valGoalCount();
        		$boolIsValid &= $this->valIsPublished();
        		$boolIsValid &= $this->valIsRenewal();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }

}
?>