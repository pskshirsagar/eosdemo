<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataCleanupTypes
 * Do not add any new functions to this class.
 */

class CDataCleanupTypes extends CBaseDataCleanupTypes {

	public static function fetchDataCleanupTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDataCleanupType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDataCleanupType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDataCleanupType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedDataCleanupTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM data_cleanup_types WHERE is_published = 1';
		return CDataCleanupTypes::fetchDataCleanupTypes( $strSql, $objDatabase );
	}

}
?>