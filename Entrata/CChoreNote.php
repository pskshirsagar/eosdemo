<?php

class CChoreNote extends CBaseChoreNote {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valChoreId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTaskNoteId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNote() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTitle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameFirst() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNameLast() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPhoneExtension() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>