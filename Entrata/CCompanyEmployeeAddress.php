<?php

class CCompanyEmployeeAddress extends CBaseCompanyEmployeeAddress {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intCid ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intCompanyEmployeeId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valAddressTypeId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intAddressTypeId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStreetLine1() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strStreetLine1 ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStreetLine2() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strStreetLine2 ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line2', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStreetLine3() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strStreetLine3 ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line3', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strCity ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', '' ) );
        // }

        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strStateCode ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valProvince() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strProvince ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'province', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPostalCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strPostalCode ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCountryCode() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_strCountryCode ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}

?>