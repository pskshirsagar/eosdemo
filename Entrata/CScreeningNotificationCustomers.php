<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningNotificationCustomers
 * Do not add any new functions to this class.
 */

class CScreeningNotificationCustomers extends CBaseScreeningNotificationCustomers {

	public static function fetchScreeningNotificationsCustomersByCidByPropertyId( $intCid,$intPropertyId, $objDatabase ) {
		$strSql = 'SELECT
						sn.cid,
						sn.id,
						sn.property_id,
						sn.is_active,
						sn.screening_notification_type_id,
						sn.quick_response_id,
						snc.customer_type_id as customer_type_id
					FROM
						screening_notifications sn
						JOIN screening_notification_customers snc ON (snc.screening_notification_id = sn.id and snc.cid = sn.cid)
					WHERE
						is_active = TRUE
						AND sn.property_id = ' . ( int ) $intPropertyId . '
						AND sn.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

}
?>