<?php

class CFileEventType extends CBaseFileEventType {

	const EDITED 			= 1;
	const UPLOADED 			= 2;
	const EXPORTED 			= 4;
	const NOTE_ADDED 		= 5;
	const KEYWORD_ADDED 	= 6;
	const KEYWORD_DELETED 	= 7;
	const KEYWORD_EDITED 	= 8;
	const API_IMPORTED		= 9;
	const API_EDITED		= 10;
	const VERIFICATION_STATUS_CHANGE = 11;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>