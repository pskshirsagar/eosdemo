<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyImportBatches
 * Do not add any new functions to this class.
 */

class CPropertyImportBatches extends CBasePropertyImportBatches {

	public static function fetchPropertyImportBatchesByCidByImportBatchId( $intCid, $intImportBatchId, $objDatabase ) {
		$strSql = 'SELECT * FROM property_import_batches WHERE cid =' . ( int ) $intCid . ' AND import_batch_id = ' . ( int ) $intImportBatchId . 'AND processed_on IS NULL;';
		return self::fetchPropertyImportBatches( $strSql, $objDatabase );
	}
}
?>