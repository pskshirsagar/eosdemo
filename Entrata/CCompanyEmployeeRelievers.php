<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeRelievers
 * Do not add any new functions to this class.
 */

class CCompanyEmployeeRelievers extends CBaseCompanyEmployeeRelievers {

	public static function fetchCompanyEmployeeRelieversByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid,  $objDatabase, $boolIsSearchByDate = false, $strBeginDate = NULL, $strEndDate = NULL, $intCompanyEmployeeRelieverId = NULL ) {

		  $strSql = 'SELECT
				cer.*,
				ce.name_first,
				ce.name_last
			FROM
				company_employee_relievers cer
				LEFT JOIN company_employees ce ON ce.id = cer.reliever_company_employee_id AND ce.cid = cer.cid
			WHERE
		  		cer.company_employee_id in ( ' . ( int ) $intCompanyEmployeeId . ' )
				AND cer.cid = ' . ( int ) $intCid . '
		 		AND cer.deleted_on IS NULL';

				if( true == $boolIsSearchByDate ) {
					$strSql .= ' AND  (
								 		(
											( date( cer.begin_datetime ) >= \'' . date( 'Y-m-d', strtotime( $strBeginDate ) ) . '\' AND date( cer.begin_datetime ) <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\')
								 		OR
											( date( cer.end_datetime ) >= \'' . date( 'Y-m-d', strtotime( $strBeginDate ) ) . '\' AND date ( cer.end_datetime ) <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\')

										)
									 OR (
											( date( cer.begin_datetime ) <= \'' . date( 'Y-m-d', strtotime( $strBeginDate ) ) . '\' AND date( cer.end_datetime ) >= \'' . date( 'Y-m-d', strtotime( $strBeginDate ) ) . '\')
								 		OR
											( date( cer.begin_datetime ) <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\' AND date ( cer.end_datetime ) >= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\')
										)
									)';
				}

				if( false == is_null( $intCompanyEmployeeRelieverId ) ) {
					$strSql .= ' AND cer.id != ' . ( int ) $intCompanyEmployeeRelieverId;
				}

				$strSql .= ' ORDER BY cer.begin_datetime ASC ';

		return self::fetchCompanyEmployeeRelievers( $strSql, $objDatabase );
	}

 	public static function fetchCompanyEmployeeRelieversByRelieverCompanyEmployeeIdByCidByBeginDateByEndDate( $intRelieverCompanyEmployeeId, $intCid, $strBeginDate, $strEndDate, $objDatabase, $intCompanyEmployeeRelieverId = NULL ) {

		$strSql = 'SELECT
				cer.*,
				ce.name_first,
				ce.name_last,
				cu.id as company_user_id
			FROM
				company_employee_relievers cer
				LEFT JOIN company_employees ce ON ce.id = cer.company_employee_id AND ce.cid = cer.cid
				LEFT JOIN company_users cu ON cu.company_employee_id = ce.id AND ( cu.cid = ce.cid AND cu.default_company_user_id IS NULL  AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
			WHERE
		  		cer.reliever_company_employee_id in ( ' . ( int ) $intRelieverCompanyEmployeeId . ' )
				AND cer.cid = ' . ( int ) $intCid . '
				AND cer.deleted_on IS NULL
				AND (
						(
							( date( cer.begin_datetime ) >= \'' . date( 'Y-m-d', strtotime( $strBeginDate ) ) . '\' AND date( cer.begin_datetime ) <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\')
					 		OR
							( date( cer.end_datetime ) >= \'' . date( 'Y-m-d', strtotime( $strBeginDate ) ) . '\' AND date ( cer.end_datetime ) <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\')

						)
					 OR (
							( date( cer.begin_datetime ) <= \'' . date( 'Y-m-d', strtotime( $strBeginDate ) ) . '\' AND date( cer.end_datetime ) >= \'' . date( 'Y-m-d', strtotime( $strBeginDate ) ) . '\')
					 		OR
							( date( cer.begin_datetime ) <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\' AND date ( cer.end_datetime ) >= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\')
						)
					 )';

				if( false == is_null( $intCompanyEmployeeRelieverId ) ) {
					$strSql .= ' AND cer.id != ' . ( int ) $intCompanyEmployeeRelieverId;
				}

				$strSql .= ' ORDER BY cer.id ASC ';

		return self::fetchCompanyEmployeeRelievers( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeRelieversByRelieverCompanyEmployeeIdByCid( $intRelieverCompanyEmployeeId, $intCid, $objDatabase ) {

		$strEndDate = $strBeginDate = date( 'Y-m-d' );

		$strSql = 'SELECT
				cu.id
			FROM
				company_employee_relievers cer
				LEFT JOIN company_employees ce ON ( ce.id = cer.company_employee_id AND ce.cid = cer.cid )
				LEFT JOIN company_users cu ON cu.company_employee_id = ce.id AND ( cu.cid = ce.cid AND cu.default_company_user_id IS NULL ) AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
			WHERE
		  		cer.reliever_company_employee_id = ' . ( int ) $intRelieverCompanyEmployeeId . '
				AND cer.cid = ' . ( int ) $intCid . '
				AND cer.deleted_on IS NULL
				AND  (
							(
								( date( cer.begin_datetime ) >= \'' . $strBeginDate . '\' AND date( cer.begin_datetime ) <= \'' . $strEndDate . '\')
					 		OR
								( date( cer.end_datetime ) >= \'' . $strBeginDate . '\' AND date ( cer.end_datetime ) <= \'' . $strEndDate . '\')

							)
						 OR (
								( date( cer.begin_datetime ) <= \'' . $strBeginDate . '\' AND date( cer.end_datetime ) >= \'' . $strBeginDate . '\')
					 		OR
								( date( cer.begin_datetime ) <= \'' . $strEndDate . '\' AND date ( cer.end_datetime ) >= \'' . $strEndDate . '\')
							)
					 )';

		$arrintCompanyEmployeeRelievers = fetchData( $strSql, $objDatabase );

		return $arrintCompanyEmployeeRelievers;
	}

	public static function fetchRelieverCompanyUserPermissionsByRelieverCompanyEmployeeIdByCid( $intRelieverCompanyEmployeeId, $arrstrModulesName, $intCid, $objDatabase ) {

		$strModuleSql = 'SELECT
							m.id
						FROM
							modules m
						WHERE
							m.name = ANY( \' { ' . implode( ',', $arrstrModulesName ) . ' } \' )';

		$strUserSql = 'SELECT
							cu.id
						FROM
							company_users cu
						WHERE
							cu.cid = ' . ( int ) $intCid . '
							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
							AND cu.company_employee_id = ' . ( int ) $intRelieverCompanyEmployeeId . '
							AND cu.is_administrator = 1';

		$strSqlCondition = 'SELECT
								cu.id
							FROM
								company_users cu
							WHERE
								cu.cid = ' . ( int ) $intCid . '
								AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
								AND cu.company_employee_id = ' . ( int ) $intRelieverCompanyEmployeeId;

		$arrintAdminUser = fetchData( $strUserSql, $objDatabase );

		if( false == valArr( $arrintAdminUser ) ) {

			$strSql = 'SELECT
							cup.company_user_id
						FROM
							company_user_permissions cup
						WHERE
							cup.cid = ' . ( int ) $intCid . '
							AND cup.module_id IN( ' . $strModuleSql . ' )
							AND cup.company_user_id = ( ' . $strSqlCondition . ' )
							AND cup.is_allowed = 1
						UNION
							SELECT
								DISTINCT cug.company_user_id
							FROM
								company_user_groups cug
								JOIN company_group_permissions cgp ON ( cgp.company_group_id = cug.company_group_id AND cgp.cid = cug.cid )
							WHERE
								cug.cid = ' . ( int ) $intCid . '
								AND cug.company_user_id = ( ' . $strSqlCondition . ' )
								AND cgp.module_id IN( ' . $strModuleSql . ' )
								AND cgp.is_allowed = 1
								AND cgp.module_id NOT IN (
															SELECT
																cup.module_id
															FROM
																company_user_permissions cup
															WHERE
																cup.cid = cgp.cid
																AND cup.module_id = cgp.module_id
																AND cup.company_user_id = cug.company_user_id
																AND cup.is_inherited = 0
														)';

			return fetchData( $strSql, $objDatabase );
		} else {
			return [ $intRelieverCompanyEmployeeId ];
		}
	}

}
?>