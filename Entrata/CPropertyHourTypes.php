<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyHourTypes
 * Do not add any new functions to this class.
 */

class CPropertyHourTypes extends CBasePropertyHourTypes {

	public static function fetchPropertyHourTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPropertyHourType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPropertyHourType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPropertyHourType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>