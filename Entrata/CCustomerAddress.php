<?php

class CCustomerAddress extends CBaseCustomerAddress {

	use TPostalAddressHelper;

	protected $m_intApHeaderId;
	protected $m_intLeaseCustomerId;
	protected $m_strEmailAddress;
	protected $m_strAddressTypeName;

	protected $m_objOriginalCustomerAddress;

	const ADDRESS_OWNER_TYPE_UNKNOWN    = 'Unknown';
	const ADDRESS_OWNER_TYPE_OWNER      = 'Owner';
	const ADDRESS_OWNER_TYPE_RENTER     = 'Renter';
	const ADDRESS_OWNER_TYPE_GOVERNMENT = 'Government';
	const ADDRESS_POSTAL_CODE_FIELD     = 'postalCode';


	public static $c_arrintAddressOwnerTypeMappings = [
		self::ADDRESS_OWNER_TYPE_OWNER				=> 1,
		self::ADDRESS_OWNER_TYPE_RENTER 			=> 0,
		self::ADDRESS_OWNER_TYPE_GOVERNMENT 	    => 2,
	];

    public static $c_arrintAddressTypeRequireKeys = [
        CAddressType::CURRENT           => 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS',
        CAddressType::PREVIOUS          => 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS',
        CAddressType::BILLING           => 'REQUIRE_ADDITIONAL_INFO_BILLING_ADDRESS',
        CAddressType::FORWARDING        => 'REQUIRE_ADDITIONAL_INFO_FORWARDING_ADDRESS',
        CAddressType::MAILING           => 'REQUIRE_ADDITIONAL_INFO_MAILING_ADDRESS',
        CAddressType::MARKETING         => 'REQUIRE_ADDITIONAL_INFO_MARKETING_ADDRESS',
        CAddressType::OTHER             => 'REQUIRE_ADDITIONAL_INFO_OTHER_ADDRESS',
        CAddressType::PAYMENT           => 'REQUIRE_ADDITIONAL_INFO_PAYMENT_ADDRESS',
        CAddressType::PERMANENT         => 'REQUIRE_ADDITIONAL_INFO_PERMANENT_ADDRESS',
        CAddressType::PHYSICAL          => 'REQUIRE_ADDITIONAL_INFO_PHYSICAL_ADDRESS',
        CAddressType::PO_BOX            => 'REQUIRE_ADDITIONAL_INFO_PO_BOX_ADDRESS',
        CAddressType::PRIMARY           => 'REQUIRE_ADDITIONAL_INFO_PRIMARY_ADDRESS',
        CAddressType::SHIPPING          => 'REQUIRE_ADDITIONAL_INFO_SHIPPING_ADDRESS',
        CAddressType::RETURN            => 'REQUIRE_ADDITIONAL_INFO_RETURN_ADDRESS',
        CAddressType::POINT_OF_INTEREST => 'REQUIRE_ADDITIONAL_INFO_POINT_OF_INTEREST_ADDRESS'
    ];

	/**
	 * Get Functions
	 */

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getAddressTypeName() {
		return $this->m_strAddressTypeName;
	}

	public function getOriginalCustomerAddress() {
		return $this->m_objOriginalCustomerAddress;
	}

	/**
	 * Set Functions
	 */

	public function setApHeaderId( $intApHeaderId ) {
		$this->m_intApHeaderId = $intApHeaderId;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = CStrings::strTrimDef( $strEmailAddress, 240, NULL, true );
	}

	public function setAddressTypeName( $strAddressTypeName ) {
		return $this->m_strAddressTypeName = $strAddressTypeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_header_id'] ) )				$this->setApHeaderId( $arrmixValues['ap_header_id'] );
		if( true == isset( $arrmixValues['lease_customer_id'] ) )			$this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
		if( true == isset( $arrmixValues['email_address'] ) )				$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['address_type_name'] ) )			$this->setAddressTypeName( $arrmixValues['address_type_name'] );
	}

	public function setOriginalCustomerAddress() {

		$arrmixOriginalCurrentAddress                      = [];
		$arrmixOriginalCurrentAddress['street_line1']      = $this->getStreetLine1();
		$arrmixOriginalCurrentAddress['street_line2']      = $this->getStreetLine2();
		$arrmixOriginalCurrentAddress['street_line3']      = $this->getStreetLine3();
		$arrmixOriginalCurrentAddress['city']              = $this->getCity();
		$arrmixOriginalCurrentAddress['state_code']        = $this->getStateCode();
		$arrmixOriginalCurrentAddress['postal_code']       = $this->getPostalCode();
		$arrmixOriginalCurrentAddress['is_non_us_address'] = $this->getIsNonUsAddress();

		$this->m_objOriginalCustomerAddress = $arrmixOriginalCurrentAddress;
	}

	public function setPostalCode( $strPostalCode ) {
		parent::setPostalCode( trim( preg_replace( '/\s+/', '', $strPostalCode ) ) );
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || 0 >= ( int ) $this->m_intId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Address Request:  Id required - CCustomerAddress::valId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Address Request:  Management Id required - CCustomerAddress::valCid()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Address Request:  Customer Id required - CCustomerAddress::valCustomerId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intAddressTypeId ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Address Request:  Address Type Id required - CCustomerAddress::valAddressTypeId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valTimeZoneId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCustomerAddressStreetLine1() {
		$boolIsValid = true;

		if( ( true == is_null( $this->getStreetLine1() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Street line1 is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCustomerAddressPostalCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getPostalCode() ) ) {

			if( CCountry::CODE_USA == $this->getCountryCode() || CCountry::CODE_CANADA == $this->getCountryCode() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal code is required.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valCustomerAddressCity() {
		$boolIsValid = true;

		if( ( true == is_null( $this->getCity() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCustomerAddressStateCode() {
		$boolIsValid = true;

		if( true == in_array( $this->getCountryCode(), [ 'US', 'CA' ] ) && false == valStr( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valStreetLine1( $boolIsRequired = false ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && ( true == is_null( $this->getStreetLine1() ) || false == valStr( $this->getStreetLine1() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Street line1 is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryCity() {
		$boolIsValid = true;

		if( ( true == is_null( $this->getCity() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City is required for Primary Address.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPrimaryStateCode() {
		$boolIsValid = true;

		if( ( true == is_null( $this->getStateCode() ) || false == valStr( $this->getStateCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State is required for Primary.' ) ) );
		}
		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCity( $boolIsRequired = false ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && ( true == is_null( $this->getCity() ) || false == valStr( $this->getCity() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCounty() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStateCode( $boolIsRequired = false ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && ( true == is_null( $this->getStateCode() ) || false == valStr( $this->getStateCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valProvince() {
		$boolIsValid = true;

		if( false == in_array( $this->getCountryCode(), [ 'US', 'CA' ] ) && false == valStr( $this->getProvince() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'province', __( 'Province is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPostalCode( $boolIsRequired = false, $strCountryCode = NULL ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && true == is_null( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsRequired = false, $intCustomerTypeId = NULL, $arrobjPropertyApplicationPreferences = NULL, $objDatabase = NULL, $boolIsValidateRequiredField = true, $boolIsValidateCity = true ) {

		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;
		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valPostalCode();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valAddressTypeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'validate_forwarding_address':
				if( true == $boolIsRequired || true == valStr( $this->getStreetLine1() ) || true == valStr( $this->getStreetLine2() ) || true == valStr( $this->getStreetLine3() ) || true == valStr( $this->getCity() ) || true == valStr( $this->getStateCode() ) || true == valStr( $this->getPostalCode() ) ) {
					$boolIsValid &= $this->valStreetLine1( true );
					if( true == $boolIsValidateCity ) {
						$boolIsValid &= $this->valCity( true );
					}
					if( CCountry::CODE_USA == $this->getCountryCode() || CCountry::CODE_CANADA == $this->getCountryCode() ) {
						$boolIsValid &= $this->valStateCode( true );
						$boolIsValid &= $this->valPostalCode( true, $strCountryCode = $this->getCountryCode() );
					}
				}
				break;

			case 'validate_existing_address':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valCustomerAddressStreetLine1();
				$boolIsValid &= $this->valCustomerAddressCity();
				$boolIsValid &= $this->valCustomerAddressStateCode();
				$boolIsValid &= $this->valCustomerAddressPostalCode();
				break;

			case 'validate_payment_account_customer_address':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valCustomerAddressStreetLine1();
				$boolIsValid &= $this->valCustomerAddressCity();
				$boolIsValid &= $this->valCustomerAddressPostalCode();
				break;

			case 'validate_new_address':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valCustomerAddressStreetLine1();
				$boolIsValid &= $this->valCustomerAddressCity();
				$boolIsValid &= $this->valCustomerAddressStateCode();
				$boolIsValid &= $this->valCustomerAddressPostalCode();
				break;

			case 'validate_commercial_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				break;

			case 'new_customer_address_pre_screening':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valAddress( $boolIsValidateRequiredField );
				break;

			case 'new_secondary_customer_address_pre_screening':
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valAddress( $boolIsValidateRequiredField );
				break;

			case 'update_additional_info':
				switch( $this->getAddressTypeId() ) {
					case CAddressType::CURRENT:
						$boolIsValid &= $this->valCurrentAddressInformation( $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField, 'current_customer_address[postal_addresses][default]' );
						break;

					case CAddressType::PREVIOUS:
						if( $intCustomerTypeId != CCustomerType::GUARANTOR ) {
							$boolIsValid &= $this->valPreviousAddressInformation( $intCustomerTypeId, $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField, 'previous_customer_address[postal_addresses][default]' );
						}
						break;

					default:
						$boolIsValid &= $this->valCustomerAddressInformation( $arrobjPropertyApplicationPreferences, $boolIsValidateRequiredField );
						break;
				}
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function mapCorrespondingAddress( $objAddressReference ) {

		if( false == is_null( $objAddressReference ) ) {
			$this->setStreetLine1( $objAddressReference->getStreetLine1() );
			$this->setStreetLine2( $objAddressReference->getStreetLine2() );
			$this->setStreetLine3( $objAddressReference->getStreetLine3() );
			$this->setCity( $objAddressReference->getCity() );
			$this->setCounty( $objAddressReference->getCounty() );
			$this->setStateCode( $objAddressReference->getStateCode() );
			$this->setCountryCode( $objAddressReference->getCountryCode() );
			$this->setPostalCode( $objAddressReference->getPostalCode() );
			$this->setIsVerified( false );

		}
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intApplicationId = NULL, $intLeaseIntervalId = NULL, $boolActivateSoftDeletedAddress = true ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $intApplicationId, $intLeaseIntervalId, false, $boolActivateSoftDeletedAddress );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $intApplicationId, $intLeaseIntervalId );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intApplicationId = NULL, $intLeaseIntervalId = NULL, $boolIsFromMigration = false, $boolActivateSoftDeletedAddress = true ) {

		// Following code is to check existing customer address for new application with old customer id

		$objExistingCustomerAddress = \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressByCustomerIdByAddressTypeIdByCid( $this->getCustomerId(), $this->getAddressTypeId(), $this->getCid(), $objDatabase, $boolIncludeDeleted = true );

		if( true == valObj( $objExistingCustomerAddress, 'CCustomerAddress' ) ) {

			$this->setId( $objExistingCustomerAddress->getId() );

			if( true == $boolActivateSoftDeletedAddress ) {
				$this->setDeletedBy( NULL );
				$this->setDeletedOn( NULL );
			}

			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $intApplicationId, $intLeaseIntervalId, $boolIsFromMigration, true );

		}

		if( false == $boolReturnSqlOnly ) {

			if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
				return false;
			}

			if( false == valId( $intApplicationId ) || false == valId( $intLeaseIntervalId ) ) return true;

			return $this->insertOrUpdateApplicationAssociation( $intApplicationId, $intLeaseIntervalId, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $boolIsFromMigration );

		} else {

			$strSql = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			if( false == valId( $intApplicationId ) || false == valId( $intLeaseIntervalId ) ) return $strSql;
			return $strSql .= $this->insertOrUpdateApplicationAssociation( $intApplicationId, $intLeaseIntervalId, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		}

	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intApplicationId = NULL, $intLeaseIntervalId = NULL, $boolIsFromMigration = false, $boolSkipExistingAddressCheck = false ) {

	    if( false == $boolSkipExistingAddressCheck ) {

            $objExistingCustomerAddress = \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressByCustomerIdByAddressTypeIdByCid( $this->getCustomerId(), $this->getAddressTypeId(), $this->getCid(), $objDatabase, $boolIncludeDeleted = true );

            if( true == valObj( $objExistingCustomerAddress, 'CCustomerAddress' ) && $objExistingCustomerAddress->getId() != $this->getId() ) {

                $objCurrentCustomerAddress = \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchCustomerAddressByIdByCid( $this->getId(), $this->getCid(), $objDatabase );

                $this->setAddressTypeId( $objCurrentCustomerAddress->getAddressTypeId() );

                if( false == $boolReturnSqlOnly && false == $this->swapAddresses( $objExistingCustomerAddress, $this, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
                    return false;
                } elseif( true == $boolReturnSqlOnly ) {
                	$strSql = $this->swapAddresses( $objExistingCustomerAddress, $this, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
                }

            }

        }

		if( false == $boolReturnSqlOnly ) {
			if( false == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
				return false;
			}

			if( false == valId( $intApplicationId ) || false == valId( $intLeaseIntervalId ) ) return true;

			return $this->insertOrUpdateApplicationAssociation( $intApplicationId, $intLeaseIntervalId, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $boolIsFromMigration );

		} else {
			$strSql .= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			if( false == valId( $intApplicationId ) || false == valId( $intLeaseIntervalId ) ) return $strSql;
			return $strSql .= $this->insertOrUpdateApplicationAssociation( $intApplicationId, $intLeaseIntervalId, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		}

	}

	public static function createAddressApplicationAssociation( $intCid, $intApplicationId, $intLeaseIntervalId, $intCustomerAddressLogId = NULL, $intCustomerId = NULL ) {
		$objApplicationAssociation = new CApplicationAssociation();

		$objApplicationAssociation->setCid( $intCid );
		$objApplicationAssociation->setApplicationId( $intApplicationId );
		$objApplicationAssociation->setLeaseIntervalId( $intLeaseIntervalId );

		if( true === valId( $intCustomerAddressLogId ) && true === valId( $intCustomerId ) ) {
			$objApplicationAssociation->setApplicationAssociationData( $intCustomerAddressLogId, $intCustomerId );
		}

		return $objApplicationAssociation;
	}

	public function insertOrUpdateApplicationAssociation( $intApplicationId, $intLeaseIntervalId, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsFromMigration = false ) {

		$intLeaseStatusTypeId = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseStatusTypeIdByLeaseIntervalIdByCid( $intLeaseIntervalId, $this->getCid(), $objDatabase );

		if( false == $boolIsFromMigration && false == in_array( $intLeaseStatusTypeId, [ CLeaseStatusType::APPLICANT, CLeaseStatusType::CURRENT, CLeaseStatusType::FUTURE ] ) ) return true;

		$arrintCustomerAddressLogIds = CCustomerAddressLogs::fetchLatestCustomerAddressLogIdByCustomerAddressIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valId( $arrintCustomerAddressLogIds[0]['id'] ) ) {

			if( true == valId( $this->getId() ) ) {
				$objApplicationAssociation = \Psi\Eos\Entrata\CApplicationAssociations::createService()->fetchApplicationAssociationByApplicationIdByCustomerAddressIdByAddressTypeIdByCid( $intApplicationId, $this->getId(), $this->getAddressTypeId(), $this->getCid(), $objDatabase );
			}

			if( false == valObj( $objApplicationAssociation, 'CApplicationAssociation' ) ) {
				$objApplicationAssociation = self::createAddressApplicationAssociation( $this->getCid(), $intApplicationId, $intLeaseIntervalId );
			}

			$objApplicationAssociation->setApplicationAssociationData( $arrintCustomerAddressLogIds[0]['id'], $this->getCustomerId() );

			if( false == $boolReturnSqlOnly ) {

				if( false == $objApplicationAssociation->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, 'application_association', __( 'Failed to create application association.' ) ) );
					return false;
				}
			} else {
				return $objApplicationAssociation->insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			}

		}

		return true;

	}

	public function valCurrentAddressInformation( $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField, $strFieldNamePrefix = NULL ) {

		$boolIsValid = true;
		$strSectionName = '';

		if( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences )
            && 1 == $arrobjPropertyApplicationPreferences['HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue() ) {
			return true;
		}

		if( true == array_key_exists( 'RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue() ) {
			$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue();
		}

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_BLOCK_NON_US_RESIDENT', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_BLOCK_NON_US_RESIDENT']->getValue() ) ) {
			$boolIsValid &= $this->valBlockNonUsResident( CApplicationStep::ADDITIONAL_INFO );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddress( $strSectionName, $boolIsValidateRequiredField, $strFieldNamePrefix );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_RESIDENCE_TYPE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressResidenceType( $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_REASON_FOR_LEAVING', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressReasonForLeaving( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_REASON_FOR_LEAVING']->getPropertyId(), $objDatabase, $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMoveInDate( true, $strSectionName, $boolIsValidateRequiredField );
		} else if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMoveInDate( false, $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMoveOutDate( true, $strSectionName, $boolIsValidateRequiredField );
		} else if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMoveOutDate( false, $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MONTHLY_PAYMENT_AMOUNT', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMonthlyPaymentAmount( $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MONTHLY_PAYMENT_RECIPIENT', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressMonthlyPaymentRecipient( $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_OWNED_OR_RENTED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_OWNED_OR_RENTED']->getValue() ) {
			$boolIsValid &= $this->valCurrentAddressAddressOwnerType( $strSectionName, $boolIsValidateRequiredField );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_COMMUNITY_NAME', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valCurrentAddressCommunityName( $strSectionName, $boolIsValidateRequiredField );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressInformation( $intCustomerTypeId, $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequiredField = true, $strFieldNamePrefix = NULL ) {
		$boolIsValid = true;
		$strSectionName = '';
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Previous' );

		if( true == array_key_exists( 'RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue() ) {
			$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue();
		}

		if( ( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences )
              || ( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences )
                   && 0 == $arrobjPropertyApplicationPreferences['HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue() ) )
              && CCustomerType::GUARANTOR != $intCustomerTypeId ) {

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddress( $strSectionName, $boolIsValidateRequiredField, $strFieldNamePrefix );
			}

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_RESIDENCE_TYPE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressResidenceType( $strSectionName, $boolIsValidateRequiredField );
			}

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_REASON_FOR_LEAVING', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressReasonForLeaving( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_REASON_FOR_LEAVING']->getPropertyId(), $objDatabase, $strSectionName, $boolIsValidateRequiredField );
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMoveInDate( true, $strSectionName, $boolIsValidateRequiredField );
			} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMoveInDate( false, $strSectionName, $boolIsValidateRequiredField );
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMoveOutDate( true, $strSectionName, $boolIsValidateRequiredField );
			} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMoveOutDate( false, $strSectionName, $boolIsValidateRequiredField );
			}

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MONTHLY_PAYMENT_AMOUNT', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMonthlyPaymentAmount( $strSectionName, $boolIsValidateRequiredField );
			}

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MONTHLY_PAYMENT_RECIPIENT', $arrobjPropertyApplicationPreferences ) ) {

				$boolIsValid &= $this->valPreviousAddressMonthlyPaymentRecipient( $strSectionName, $boolIsValidateRequiredField );
			}

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_OWNED_OR_RENTED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_OWNED_OR_RENTED']->getValue() ) {
				$boolIsValid &= $this->valPreviousAddressAddressOwnerType( $strSectionName, $boolIsValidateRequiredField );
			}

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_COMMUNITY_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressCommunityName( $strSectionName, $boolIsValidateRequiredField );
			}

		} elseif( $intCustomerTypeId != CCustomerType::GUARANTOR || ( CCustomerType::GUARANTOR == $intCustomerTypeId && true == array_key_exists( 'SHOW_GUARANTOR_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ) {

			// ELSE CONDITION IS ADDED TO ENSURE THAT THE ZIP CODE / MOVE-IN / MOVE-OUT DATE IS VALIDATED ( IF INPUT IS PROVIDED ) EVEN WHEN FIELD IS NOT MANDATORY
			if( true == valStr( $this->getPostalCode() ) && false == CApplicationUtils::validateZipCode( $this->getPostalCode(), NULL ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( '{%s,0} address zip code must be {%d,1} to {%d,2} characters.', [ $strSectionNameToDisplay, 5, 10 ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else if( true == is_null( $this->getPostalCode() ) && true == $boolIsValidateRequiredField && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && 0 == ( int ) $this->getIsNonUsAddress() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMoveInDate( true, $strSectionName, $boolIsValidateRequiredField );
			} elseif( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_IN_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMoveInDate( false, $strSectionName, $boolIsValidateRequiredField );
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMoveOutDate( true, $strSectionName, $boolIsValidateRequiredField );
			} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MOVE_OUT_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valPreviousAddressMoveOutDate( false, $strSectionName, $boolIsValidateRequiredField );
			}

		}

		return $boolIsValid;
	}

	public function valBlockNonUsResident( $intApplicationStepId = NULL ) {

		$boolIsValid = true;

		if( false == is_null( $intApplicationStepId ) && CApplicationStep::BASIC_INFO == $intApplicationStepId ) {
			if( true == $this->getIsAlien() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_alien', __( 'U.S. citizen is required.' ), '' ) );
			}
		} else if( false == is_null( $intApplicationStepId ) && CApplicationStep::ADDITIONAL_INFO == $intApplicationStepId ) {
			if( true == $this->getIsNonUsAddress() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_non_us_address', __( 'A current U.S. address is required. Please contact the property staff for assistance with your application.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddress( $strSectionName = NULL, $boolIsValidateRequiredField = true, $strFieldNamePrefix = NULL ) {
		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Current Address' );
		$boolIsValid = true;

		if( false == $objPostalAddressService->isValid( $this->getPostalAddress() ) && true == $boolIsValidateRequiredField ) {
			$arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();

			foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
				if( ( \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() ) || ( $objErrorMsg->getField() == self::ADDRESS_POSTAL_CODE_FIELD && false == valStr( $this->getPostalAddress()->getPostalCode() ) ) ) {
					$arrmixExistingData = $objErrorMsg->getData();
					$arrmixNewData = [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ];
					$objErrorMsg->setData( array_merge( $arrmixExistingData, $arrmixNewData ) );
					continue;
				}
				unset( $arrobjErrorMsgs[$intKey] );
			}

			if( true == valArr( $arrobjErrorMsgs ) ) {
				$this->addPostalAddressErrorMsgs( $arrobjErrorMsgs, NULL, $strFieldNamePrefix );
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

    public function valCustomerAddressInformation( $arrobjPropertyApplicationPreferences, $boolIsValidateRequiredField, $strFieldNamePrefix = NULL ) {

        if( false == array_key_exists( self::$c_arrintAddressTypeRequireKeys[$this->getAddressTypeId()], $arrobjPropertyApplicationPreferences ) ) {
            return true;
        }

	    if( true == array_key_exists( self::$c_arrintAddressTypeRequireKeys[$this->getAddressTypeId()], $arrobjPropertyApplicationPreferences )
	        && false == $arrobjPropertyApplicationPreferences[self::$c_arrintAddressTypeRequireKeys[$this->getAddressTypeId()]]->getValue() ) {
		    return true;
	    }

        $objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();

        $strSectionName = __( '{%s,0} Address', CAddressType::createService()->getAddressTypeNameByAddressTypeId( $this->getAddressTypeId() ) );
        $boolIsValid = true;

        if( false == $objPostalAddressService->isValid( $this->getPostalAddress() ) && true == $boolIsValidateRequiredField ) {
            $arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();

            foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
                if( ( \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() ) || ( $objErrorMsg->getField() == self::ADDRESS_POSTAL_CODE_FIELD && false == valStr( $this->getPostalAddress()->getPostalCode() ) ) ) {
                    $arrmixExistingData = $objErrorMsg->getData();
                    $arrmixNewData = [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ];
                    $objErrorMsg->setData( array_merge( $arrmixExistingData, $arrmixNewData ) );
                    continue;
                }
                unset( $arrobjErrorMsgs[$intKey] );
            }

            if( false == valStr( $strFieldNamePrefix ) ) {
            	$intAddressTypeId = $this->getAddressTypeId();
                $strFieldNamePrefix = "customer_addresses[$intAddressTypeId]";
            }

            if( true == valArr( $arrobjErrorMsgs ) ) {
                $this->addPostalAddressErrorMsgs( $arrobjErrorMsgs, NULL, $strFieldNamePrefix );
                $boolIsValid = false;
            }
        }

        return $boolIsValid;
    }

	public function valCurrentPostalCode( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPostalCode() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->ucwords( $strSectionName ) : __( 'Current' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valCurrentAddressResidenceType( $strSectionName = NULL, $boolIsValidateRequiredField ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCompanyResidenceTypeId() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_company_residence_type_id', __( '{%s,0} residence type is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_company_residence_type_id', __( 'Current address residence type is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressReasonForLeaving( $intPropertyId, $objDatabase, $strSectionName = NULL, $boolIsValidateRequiredField ) {

		$boolIsValid 			= true;
		$intMoveOutReasonsCount = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemsCountByPropertyIdByCIdByListTypeId( $intPropertyId, $this->getCid(), $objDatabase, CListType::MOVE_OUT );

		if( ( ( 0 < $intMoveOutReasonsCount && true == is_null( $this->getMoveOutReasonListItemId() ) ) || ( 0 == $intMoveOutReasonsCount && true == is_null( $this->getReasonForLeaving() ) ) ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_reason_for_leaving', __( '{%s,0} reason for leaving is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_reason_for_leaving', __( 'Current address reason for leaving is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMoveInDate( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Current address' );

		if( true == $boolIsRequired && 0 == strlen( $this->getMoveInDate() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( 'Current address move-in date is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getMoveInDate() ) && 1 != CValidation::checkISODate( $this->getMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date is in invalid form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( 0 < valStr( $this->getMoveInDate() ) && strtotime( $this->getMoveInDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date cannot be greater than current date.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMoveOutDate( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->getMoveOutDate() ) && true == $boolIsValidateRequiredField ) {

			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) && true == $boolIsValidateRequiredField ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', __( '{%s,0} move-out date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', __( 'Current address move-out date is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getMoveOutDate() ) && 1 != CValidation::checkISODate( $this->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Current address' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', __( '{%s,0} move-out date is in invalid form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => 'Current Address', 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( true == $boolIsValid && 0 != strlen( $this->getMoveInDate() && 0 != strlen( $this->getMoveOutDate() ) ) ) {
			if( ( strtotime( $this->getMoveOutDate() ) ) <= ( strtotime( $this->getMoveInDate() ) ) ) {
				$boolIsValid &= false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date must be less than move-out date.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( 'Current address move-in date must be less than move-out date.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valCurrentAddressMonthlyPaymentAmount( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( ( 0 == strlen( $this->getMonthlyPaymentAmount() ) ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_amount', __( '{%s,0} monthly payment valid amount is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_amount', __( 'Current address monthly payment valid amount is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMonthlyPaymentRecipient( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPaymentRecipient() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_recipient', __( '{%s,0} monthly payment recipient is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_recipient', __( 'Current address monthly payment recipient is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressAddressOwnerType( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( self::ADDRESS_OWNER_TYPE_UNKNOWN == $this->getAddressOwnerType() && true == $boolIsValidateRequiredField ) {

			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_address_owner_type', __( '{%s,0} owned/rented is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_address_owner_type', __( 'Current address owned/rented is required..' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function valCurrentAddressCommunityName( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCommunityName() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_community_name', __( '{%s,0} community name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_community_name', __( 'Current address community name is required..' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function valPreviousAddress( $strSectionName = NULL, $boolIsValidateRequiredField = true, $strFieldNamePrefix = NULL ) {
		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Previous Address' );
		$boolIsValid = true;
		if( false == $objPostalAddressService->isValid( $this->getPostalAddress() ) && true == $boolIsValidateRequiredField ) {
			$arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();
			foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
				if( ( \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() ) || ( $objErrorMsg->getField() == self::ADDRESS_POSTAL_CODE_FIELD && false == valStr( $this->getPostalAddress()->getPostalCode() ) ) ) {
					$arrmixExistingData = $objErrorMsg->getData();
					$arrmixNewData = [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ];
					$objErrorMsg->setData( array_merge( $arrmixExistingData, $arrmixNewData ) );
					continue;
				}
				unset( $arrobjErrorMsgs[$intKey] );
			}

			if( true == valArr( $arrobjErrorMsgs ) ) {
				$this->addPostalAddressErrorMsgs( $arrobjErrorMsgs, NULL, $strFieldNamePrefix );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valPreviousPostalCode( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->ucwords( $strSectionName ) : __( 'Previous' );

		if( 0 == strlen( $this->getPostalCode() ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s,0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( false == CValidation::validatePostalCode( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( 'Invalid {%s,0} zip/postal code.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressResidenceType( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCompanyResidenceTypeId() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_company_residence_type_id', __( '{%s,0} residence type is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_company_residence_type_id', __( 'Previous Address residence type is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressReasonForLeaving( $intPropertyId, $objDatabase, $strSectionName = NULL, $boolIsValidateRequiredField ) {

		$boolIsValid 			= true;
		$intMoveOutReasonsCount = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemsCountByPropertyIdByCIdByListTypeId( $intPropertyId, $this->getCid(), $objDatabase, CListType::MOVE_OUT );

		if( ( ( 0 < $intMoveOutReasonsCount && true == is_null( $this->getMoveOutReasonListItemId() ) ) || ( 0 == $intMoveOutReasonsCount && true == is_null( $this->getReasonForLeaving() ) ) ) && true == $boolIsValidateRequiredField ) {

			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_reason_for_leaving', __( '{%s,0} reason for leaving is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_reason_for_leaving', __( 'Previous address reason for leaving is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMoveInDate( $boolIsRequired = false,  $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Previous address' );

		if( true == $boolIsRequired && 0 == strlen( $this->getMoveInDate() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( 0 < strlen( $this->getMoveInDate() ) && 1 !== CValidation::checkISODate( $this->getMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date is in invalid form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( 0 < valStr( $this->getMoveInDate() ) && strtotime( $this->getMoveInDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date cannot be greater than current date.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMoveOutDate( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Previous address' );

		if( true == $boolIsRequired && 0 == strlen( $this->getMoveOutDate() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( '{%s,0} move-out date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( 'Previous address move-out date is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getMoveOutDate() ) && 1 !== CValidation::checkISODate( $this->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( '{%s,0} move-out date is in invalid form.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( strtotime( $this->getMoveOutDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( '{%s,0} move-out date cannot be greater than current date.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( true == $boolIsValid && 0 != strlen( $this->getMoveInDate() && 0 != strlen( $this->getMoveOutDate() ) ) ) {
			if( ( strtotime( $this->getMoveOutDate() ) ) <= ( strtotime( $this->getMoveInDate() ) ) ) {
				$boolIsValid &= false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date must be less than move-out date.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( 'Previous move-in date must be less than move-out date.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}
		return $boolIsValid;
	}

	public function valPreviousAddressMonthlyPaymentAmount( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( ( ( 0 == strlen( $this->getMonthlyPaymentAmount() ) || 0 > $this->getMonthlyPaymentAmount() ) ) ) {
			if( true == $boolIsValidateRequiredField ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_amount', __( '{%s,0} monthly payment valid amount is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_amount', __( 'Previous address monthly payment valid amount is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMonthlyPaymentRecipient( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPaymentRecipient() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_recipient', __( '{%s,0} monthly payment recipient is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_recipient', __( 'Previous address monthly payment recipient is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressAddressOwnerType( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( self::ADDRESS_OWNER_TYPE_UNKNOWN == $this->getAddressOwnerType() && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_address_owner_type', __( '{%s,0} owned/rented is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_address_owner_type', __( 'Previous address owned/rented is required..' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function valPreviousAddressCommunityName( $strSectionName = NULL, $boolIsValidateRequiredField = true ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCommunityName() ) && true == $boolIsValidateRequiredField ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_community_name', __( '{%s,0} community name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_community_name', __( 'Previous address community name is required..' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}
		return $boolIsValid;
	}

	public function isBlankData() {

		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$boolIsBlank = $objPostalAddressService->isEmpty( $this->getPostalAddress() );

		switch( $this->getAddressTypeId() ) {
			case CAddressType::CURRENT:
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getCompanyResidenceTypeId() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getCommunityName() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveInDate() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveOutDate() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMonthlyPaymentAmount() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getPaymentRecipient() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveOutReasonListItemId() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getAddressOwnerType() ) || 'Unknown' == $this->getAddressOwnerType() );
				if( 0 == ( int ) $this->getMoveOutReasonListItemId() ) {
					$boolIsBlank &= ( bool ) ( false == valStr( $this->getReasonForLeaving() ) );
				}
				break;

			case CAddressType::PREVIOUS:
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getCompanyResidenceTypeId() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getCommunityName() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveInDate() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveOutDate() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMonthlyPaymentAmount() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getPaymentRecipient() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getReasonForLeaving() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getMoveOutReasonListItemId() ) );
				$boolIsBlank &= ( bool ) ( false == valStr( $this->getAddressOwnerType() ) || 'Unknown' == $this->getAddressOwnerType() );
				if( 0 == ( int ) $this->getMoveOutReasonListItemId() ) {
					$boolIsBlank &= ( bool ) ( false == valStr( $this->getReasonForLeaving() ) );
				}
				break;

			default:
				// #code...
				break;
		}
		return  $boolIsBlank;
	}

	public function valAddress( $boolIsValidateRequiredField ) {

		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$strSectionName = __( 'Current Address' );
		$boolIsValid = true;

		if( false == $objPostalAddressService->isValid( $this->getPostalAddress() ) && true == $boolIsValidateRequiredField ) {
			$arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();

			foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
				if( ( \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() ) || ( $objErrorMsg->getField() == self::ADDRESS_POSTAL_CODE_FIELD && false == valStr( $this->getPostalAddress()->getPostalCode() ) ) ) {
					$arrmixExistingData = $objErrorMsg->getData();
					$arrmixNewData = [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ];
					$objErrorMsg->setData( array_merge( $arrmixExistingData, $arrmixNewData ) );
					continue;
				}
				unset( $arrobjErrorMsgs[$intKey] );
			}

			if( true == valArr( $arrobjErrorMsgs ) ) {
				$this->addPostalAddressErrorMsgs( $arrobjErrorMsgs, NULL, $strSectionName );
				foreach( $this->getErrorMsgs() as $objError ) {
					$objError->setMessage( __( 'Current {%s,0}', [ $objError->getMessage() ] ) );
				}
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intApplicationId = NULL ) {

		if( true == valId( $intApplicationId ) ) {

			$objApplicationAssociation = \Psi\Eos\Entrata\CApplicationAssociations::createService()->fetchApplicationAssociationByApplicationIdByCustomerAddressIdByAddressTypeIdByCid( $intApplicationId, $this->getId(), $this->getAddressTypeId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objApplicationAssociation, 'CApplicationAssociation' ) && false == $objApplicationAssociation->delete( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public static function prepareAndInsertApplicationAssociations( $intApplicationId, $intLeaseIntervalId, $arrintCustomerIds, $intCompanyUserId, $intCid, $objDatabase, $arrintAddressTypeIds = [] ) {

		if( false == valArr( $arrintAddressTypeIds ) )
			$arrintAddressTypeIds = [ CAddressType::PREVIOUS, CAddressType::CURRENT ];

		if( false == valId( $intApplicationId ) || false == valId( $intLeaseIntervalId ) || false == valArr( $arrintCustomerIds ) || false == valId( $intCid ) || false == valArr( $arrintAddressTypeIds ) ) return false;

		$arrmixCustomerAddressLogsData = CCustomerAddressLogs::fetchLatestCustomerAddressLogIdsByCustomerIdsByAddressTypeIdsByCid( $arrintCustomerIds, $arrintAddressTypeIds, $intCid, $objDatabase );

		$arrobjNewApplicationAssociations = [];

		if( true == valArr( $arrmixCustomerAddressLogsData ) ) {
			foreach( $arrmixCustomerAddressLogsData as $mixCustomerAddressLogData ) {

				if( CAddressType::PREVIOUS == $mixCustomerAddressLogData['address_type_id'] && true == valStr( $mixCustomerAddressLogData['deleted_on'] ) ) continue;

				$objApplicationAssociation = self::createAddressApplicationAssociation( $intCid, $intApplicationId, $intLeaseIntervalId );
				$objApplicationAssociation->setApplicationAssociationData( $mixCustomerAddressLogData['id'], $mixCustomerAddressLogData['customer_id'] );
				$arrobjNewApplicationAssociations[] = $objApplicationAssociation;

			}
		}

		if( true == valArr( $arrobjNewApplicationAssociations ) && false == CApplicationAssociations::bulkInsert( $arrobjNewApplicationAssociations, $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		return true;

	}

    private function swapAddresses( $objExistingAddress, $objCurrentAddress, $intCompanyUserId, $objDatabase, $boolReturnSqlOnly ) {

        $intCurrentAddressId = $objCurrentAddress->getId();
        $intCurrentAddressTypeId = $objCurrentAddress->getAddressTypeId();

        $objCurrentAddress->setId( $objExistingAddress->getId() );
        $objCurrentAddress->setAddressTypeId( $objExistingAddress->getAddressTypeId() );

        $objExistingAddress->setId( $intCurrentAddressId );
        $objExistingAddress->setAddressTypeId( $intCurrentAddressTypeId );

        if( false == $boolReturnSqlOnly && false == $objExistingAddress->update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly, NULL, NULL, false, $boolSkipExistingAddressCheck = true ) ) {
            return false;
        } elseif( true == $boolReturnSqlOnly ) {
        	return $objExistingAddress->update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly, NULL, NULL, false, $boolSkipExistingAddressCheck = true );
        }

        return true;

    }

    public function isCustomerAddressChanged() {

	    $strOldAddress = '';
	    if( valArr( $this->getOriginalCustomerAddress() ) ) {
		    $strOldAddress = preg_replace( '/\s/', '', $this->getOriginalCustomerAddress()['street_line1'] . $this->getOriginalCustomerAddress()['street_line2'] . $this->getOriginalCustomerAddress()['street_line3'] . $this->getOriginalCustomerAddress()['city'] . $this->getOriginalCustomerAddress()['state_code'] . $this->getOriginalCustomerAddress()['postal_code'] . $this->getOriginalCustomerAddress()['is_non_us_address'] );
	    }

		$strNewAddress = preg_replace( '/\s/', '', $this->getStreetLine1() . $this->getStreetLine2() . $this->getStreetLine3() . $this->getCity() . $this->getStateCode() . $this->getPostalCode() . $this->getIsNonUsAddress() );

		if( 0 == strcmp( strtolower( $strOldAddress ), strtolower( $strNewAddress ) ) ) {
			return false;
		}

		return true;
    }

	public function mapCurrentCustomerAddressToApplicantAddress( $objApplicant ) {
		$objApplicant->setCurrentStreetLine1( $this->getStreetLine1() );
		$objApplicant->setCurrentStreetLine2( $this->getStreetLine2() );
		$objApplicant->setCurrentStreetLine3( $this->getStreetLine3() );
		$objApplicant->setCurrentCity( $this->getCity() );
		$objApplicant->setCurrentStateCode( $this->getStateCode() );
		$objApplicant->setCurrentPostalCode( $this->getPostalCode() );
		$objApplicant->setCurrentCountryCode( $this->getCountryCode() );
		$objApplicant->setCurrentMoveOutReasonListItemId( $this->getMoveOutReasonListItemId() );
		$objApplicant->setCurrentCompanyResidenceTypeId( $this->getCompanyResidenceTypeId() );
		$objApplicant->setCurrentMoveInDate( $this->getMoveInDate() );
		$objApplicant->setCurrentMoveOutDate( $this->getMoveOutDate() );
		$objApplicant->setCurrentMonthlyPaymentAmount( $this->getMonthlyPaymentAmount() );
		$objApplicant->setCurrentPaymentRecipient( $this->getPaymentRecipient() );
		$objApplicant->setCurrentCommunityName( $this->getCommunityName() );
		$objApplicant->setCurrentReasonForLeaving( $this->getReasonForLeaving() );
		$objApplicant->setCurrentPaymentRecipient( $this->getPaymentRecipient() );
		$objApplicant->setCurrentAddressOwnerType( $this->getAddressOwnerType() );
		$objApplicant->setCurrentIsNonUsAddress( $this->getIsNonUsAddress() );
		$objApplicant->setCurrentIsVerified( $this->getIsVerified() );
	}

	public function mapPreviousCustomerAddressToApplicantAddress( $objApplicant ) {
		$objApplicant->setPreviousStreetLine1( $this->getStreetLine1() );
		$objApplicant->setPreviousStreetLine2( $this->getStreetLine2() );
		$objApplicant->setPreviousStreetLine3( $this->getStreetLine3() );
		$objApplicant->setPreviousCity( $this->getCity() );
		$objApplicant->setPreviousStateCode( $this->getStateCode() );
		$objApplicant->setPreviousPostalCode( $this->getPostalCode() );
		$objApplicant->setPreviousCountryCode( $this->getCountryCode() );
		$objApplicant->setPreviousMoveOutReasonListItemId( $this->getMoveOutReasonListItemId() );
		$objApplicant->setPreviousCompanyResidenceTypeId( $this->getCompanyResidenceTypeId() );
		$objApplicant->setPreviousMoveInDate( $this->getMoveInDate() );
		$objApplicant->setPreviousMoveOutDate( $this->getMoveOutDate() );
		$objApplicant->setPreviousMonthlyPaymentAmount( $this->getMonthlyPaymentAmount() );
		$objApplicant->setPreviousPaymentRecipient( $this->getPaymentRecipient() );
		$objApplicant->setPreviousCommunityName( $this->getCommunityName() );
		$objApplicant->setPreviousReasonForLeaving( $this->getReasonForLeaving() );
		$objApplicant->setPreviousPaymentRecipient( $this->getPaymentRecipient() );
		$objApplicant->setPreviousAddressOwnerType( $this->getAddressOwnerType() );
		$objApplicant->setPreviousIsNonUsAddress( $this->getIsNonUsAddress() );
		$objApplicant->setPreviousIsVerified( $this->getIsVerified() );
	}

}
?>