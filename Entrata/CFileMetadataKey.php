<?php

class CFileMetadataKey extends CBaseFileMetadataKey {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valKeyName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDatatype() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowMultipleValues() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsEnabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>