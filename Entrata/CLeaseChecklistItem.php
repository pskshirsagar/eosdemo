<?php

class CLeaseChecklistItem extends CBaseLeaseChecklistItem {

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Cid is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseChecklistId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLeaseChecklistId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_checklist_id', __( 'Lease checklist id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valChecklistItemId() {
		$boolIsValid = true;

		if( true == is_null( $this->getChecklistItemId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'checklist_item_id', __( 'Checklist item id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseChecklistId();
				$boolIsValid &= $this->valChecklistItemId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseChecklistId();
				$boolIsValid &= $this->valChecklistItemId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = parent::insert( $intCurrentUserId, $objDatabase, true );

		$strSql = str_replace( ' RETURNING id;', ' ON CONFLICT ON CONSTRAINT uk_lease_checklist_items_cid_lease_checklist_id_checklist_item_ DO NOTHING RETURNING id;', $strSql );

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		return $this->executeSql( $strSql, $this, $objDatabase );
	}

}
?>