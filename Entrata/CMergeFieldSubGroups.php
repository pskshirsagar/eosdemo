<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMergeFieldSubGroups
 * Do not add any new functions to this class.
 */

class CMergeFieldSubGroups extends CBaseMergeFieldSubGroups {

	public static function fetchPaginatedMergeFieldSubGroups( $objDatabase, $objPagination, $boolIsCount = false ) {

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit  = ( int ) $objPagination->getPageSize();
		}

		if( true == $boolIsCount ) {
			$strSql = 'SELECT
						count(mfsb.id) ';
		} else {
			$strSql = 'SELECT
						mfsb.*,
						mfg.id AS group_id,
						mfg.name AS group_name ';
		}
		$strSql .= ' FROM
							merge_field_sub_groups mfsb LEFT JOIN merge_field_groups mfg ON ( mfg.id = mfsb.merge_field_group_id )';

		if( false == $boolIsCount && true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' ORDER BY mfg.name,
                         mfsb.merge_field_group_id,mfsb.order_num  
						 OFFSET ' . ( int ) $intOffset .
			           ' LIMIT ' . ( int ) $intLimit;
		}
		$arrstrResultSet = fetchData( $strSql, $objDatabase );

		return ( false == $boolIsCount ) ? $arrstrResultSet : $arrstrResultSet[0]['count'];
	}

	public static function fetchMergeFieldSubGroupsBySearchFilter( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
							mfsb.*,
							mfg.id AS group_id,
							mfg.name AS group_name 
						FROM
							merge_field_sub_groups mfsb LEFT JOIN merge_field_groups mfg ON ( mfg.id = mfsb.merge_field_group_id ) 
						WHERE 
							mfsb.name ILIKE \'%' . implode( '%\' OR mfsb.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							OR mfg.name ILIKE \'%' . implode( '%\' OR mfg.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						ORDER BY 
							CASE
								When mfsb.name = \'' . implode( ' ', $arrstrFilteredExplodedSearch ) . '\' THEN 1
								ELSE 2
							END	
						LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldSubGroupsMaxId( $objDatabase ) {
		$strSql = 'SELECT
						MAX( id ) AS id
					FROM
						merge_field_sub_groups';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldSubGroupCountById( $intId, $objDatabase ) {

		if( false == valId( $intId ) ) {
			return false;
		}

		$strWhere = 'WHERE id = ' . ( int ) $intId;

		return parent::fetchRowCount( $strWhere, 'merge_field_sub_groups', $objDatabase );
	}

	public static function fetchMergeFieldSubGroupCountByNameByGroupId( $strName, $intMergeFieldGroupId, $intId, $objDatabase ) {

		$strWhere = 'WHERE LOWER(name) = LOWER(\'' . $strName . '\') AND merge_field_group_id = ' . ( int ) $intMergeFieldGroupId;

		if( true == valId( $intId ) ) {
			$strWhere .= ' AND id != ' . ( int ) $intId;
		}

		return parent::fetchRowCount( $strWhere, 'merge_field_sub_groups', $objDatabase );
	}

	public static function fetchMergeFieldSubGroupsByGroupId( $intGroupId, $objDatabase ) {
		$strSql = 'SELECT
			            id,
			            name
				   FROM
                        merge_field_sub_groups';

		$strSql .= ( true == valId( $intGroupId ) ? ' WHERE merge_field_group_id =  ' . ( int ) $intGroupId : '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedMergeFieldSubGroupsByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT * FROM merge_field_sub_groups WHERE is_published = 1 AND id IN (' . implode( ',', $arrintIds ) . ') ORDER BY merge_field_group_id, order_num';
		return self::fetchMergeFieldSubGroups( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldsSubGroupsByGroupId( $intGroupId, $objDatabase, $boolIsPublished = false ) {
		$strSql = 'SELECT
            *
          FROM
            merge_field_sub_groups';

		$strSql .= ( true == valId( $intGroupId ) ? ' WHERE merge_field_group_id =  ' . ( int ) $intGroupId : '' );

		$strSql .= ( true == $boolIsPublished ) ? ' AND is_published =  ' . ( int ) $boolIsPublished : '';

		$strSql .= ' ORDER BY order_num ASC ';

		return parent::fetchMergeFieldSubGroups( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldsSubGroupsPagination( $intGroupId, $objDatabase, $objPagination ) {
		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit  = ( int ) $objPagination->getPageSize();
		}
		$strSql = 'SELECT
            *
          FROM
            merge_field_sub_groups';

		$strSql .= ( true == valId( $intGroupId ) ? ' WHERE merge_field_group_id =  ' . ( int ) $intGroupId . ' ORDER BY order_num ASC ' : '' );

		if( false == $intGroupId && true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' ORDER BY merge_field_group_id,
                         order_num  
						 OFFSET ' . ( int ) $intOffset .
			           ' LIMIT ' . $intLimit;
		}
		return parent::fetchMergeFieldSubGroups( $strSql, $objDatabase );
	}

	public static function fetchMergeFieldSubGroupMaxOrderNumberByGroupIds( $objDatabase ) {
		$strSql = 'SELECT
						MAX( order_num )+1 AS max_order_num,merge_field_group_id
					FROM
						merge_field_sub_groups
					GROUP BY merge_field_group_id';
		$arrintResponse = fetchData( $strSql, $objDatabase );
		return $arrintResponse;
	}

	public static function fetchAllPublishedMergeFieldSubGroups( $objDatabase ) {

		$strSql = 'SELECT
						id,
						name,
						merge_field_group_id
					FROM
						merge_field_sub_groups
					WHERE is_published = 1';

		$arrintResponse = self::fetchMergeFieldSubGroups( $strSql, $objDatabase );

		return $arrintResponse;
	}

}

?>