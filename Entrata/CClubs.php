<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CClubs
 * Do not add any new functions to this class.
 */

class CClubs extends CBaseClubs {

	public static function fetchPaginatedClubsByPropertyIdByCid( $intPropertyId, $intCid, $strClubsSearchFilter, $objPagination, $arrintPropertyIds, $objDatabase, $returnDataInArray = false ) {
		if( false == is_numeric( $intCid ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strAndWhere = ( false == is_null( $strClubsSearchFilter ) ) ? ' AND name ILIKE \'%' . trim( addslashes( $strClubsSearchFilter ) ) . '%\'' : '';

		$strOffsetLimit = ( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) ? '' : 'OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

		$strSelect = '';
		$strSqlJoin = '';

		if( true == valArr( $arrintPropertyIds ) ) {
			if( true == in_array( $intPropertyId, $arrintPropertyIds ) ) {
				$strSelect = ', COUNT( membercnt.custid ) OVER ( PARTITION BY cl.id ) as members_count ';
			} else {
				$strSelect = ', COUNT( membercnt.custid ) OVER ( PARTITION BY cl.id ) as members_count ';
			}

			$strSqlJoin = 'LEFT JOIN
									(
									  SELECT
										  DISTINCT ON ( lc.customer_id, clb.id ) lc.customer_id as custid,
										  clb.id AS club_id,
										  clb.cid
									  FROM
										  clubs clb
										  JOIN customer_clubs AS custclb ON ( custclb.club_id::integer = clb.id::integer AND custclb.cid::integer = clb.cid::integer )
										  JOIN lease_customers AS lc ON ( lc.customer_id = custclb.customer_id AND lc.cid = custclb.cid )
			  							  JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
									  WHERE
										  clb.cid = ' . ( int ) $intCid . '
										  AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
									) AS membercnt ON ( membercnt.club_id::integer = cl.id::integer AND membercnt.cid::integer = cl.cid::integer)';
		}

		$strSql = 'SELECT
						DISTINCT ON ( cl.id )
						cl.*,
						c.name_first,
						c.name_last
						' . $strSelect . '
					FROM
						clubs cl
						LEFT JOIN  customers c on ( c.id = cl.customer_id AND c.cid = cl.cid )
						' . $strSqlJoin . '
					WHERE
						cl.cid = ' . ( int ) $intCid . '
					  AND cl.property_id = ' . ( int ) $intPropertyId . '
					  AND cl.deleted_on IS NULL
					  ' . $strAndWhere . '
					  ORDER BY cl.id DESC, cl.approved_on DESC ' . $strOffsetLimit;

	return ( false == $returnDataInArray ) ? self::fetchClubs( $strSql, $objDatabase ) : fetchData( $strSql, $objDatabase );

	}

	public static function fetchClubsByIdsByCid( $arrintClubIds, $intCid, $objDatabase, $boolIsRejectedClubs = false, $returnDataInArray = false ) {

		if( false == is_numeric( $intCid ) || false == valArr( $arrintClubIds ) ) return NULL;

		$strSqlRejected = ( true == $boolIsRejectedClubs ) ? ' AND deleted_on IS NOT NULL ' : ' AND deleted_on IS NULL ';

		$strSql = 'SELECT *
						FROM clubs
					WHERE cid = ' . ( int ) $intCid . '
						AND id IN (' . implode( ',', $arrintClubIds ) . ')
						' . $strSqlRejected . '
					ORDER BY is_published ASC';

		if( false == $returnDataInArray ) {
			return self::fetchClubs( $strSql, $objDatabase );
		} else {
			$arrmixClubData = ( array ) fetchData( $strSql, $objDatabase );
			return $arrmixClubData[0];
		}
	}

	public static function fetchPaginatedUnapprovedClubsByCidByPropertyIds( $objPagination, $intCid, $arrintPropertyIds, $objDatabase, $boolIsRejectedClubs = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSqlOffsetLimit = '';
		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {

			$intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit = ( int ) $objPagination->getPageSize();
			$strSqlOffsetLimit = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strSqlRejected = ( true == $boolIsRejectedClubs ) ? ' AND deleted_on IS NOT NULL ' : ' AND deleted_on IS NULL ';

		$strSql = 'SELECT
						DISTINCT
						cl.*,
						c.name_first,
						c.name_last
					FROM clubs cl
						LEFT JOIN customer_clubs ccl ON ( cl.id = ccl.club_id AND cl.cid = ccl.cid )
						LEFT JOIN customers c ON ( c.id = cl.customer_id AND c.cid = cl.cid )
					WHERE cl.cid = ' . ( int ) $intCid . '
						AND cl.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						' . $strSqlRejected . '
						AND cl.approved_on IS NULL
						AND cl.customer_id IS NOT NULL
					ORDER BY cl.updated_on DESC' . $strSqlOffsetLimit;

		return self::fetchClubs( $strSql, $objDatabase );
	}

	public static function fetchUnapprovedClubsCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase, $boolIsRejectedClubs = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSqlRejected = ( true == $boolIsRejectedClubs ) ? ' AND deleted_on IS NOT NULL ' : ' AND deleted_on IS NULL ';

		$strWhere = ' WHERE cid = ' . ( int ) $intCid . '
						AND property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						' . $strSqlRejected . '
						AND customer_id IS NOT NULL
						AND approved_on IS NULL';

		return self::fetchClubCount( $strWhere, $objDatabase );
	}

	public static function fetchApprovedClubsByPropertyIdsByCustomerIdByCid( $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase, $intExcludedCustomerId = NULL, $intLimit = NULL ) {

		if( false == is_numeric( $intCid ) ) return NULL;
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == is_numeric( $intCustomerId ) ) return NULL;

		$strSqlLimit = '';
		if( true == isset( $intLimit ) && true == is_numeric( $intLimit ) ) {
			$strSqlLimit = ' LIMIT ' . ( int ) $intLimit;
		}

		$strSql = ' SELECT
						DISTINCT cl.id,
						cl.property_id,
						cl.name ';

		$strSql .= ', COUNT( membercnt.custid ) OVER ( PARTITION BY cl.id ) + CASE WHEN cl.customer_id IS NOT NULL THEN 1 ELSE 0 END as members_count
					FROM
						clubs cl
						LEFT JOIN
								(
								  SELECT
									  DISTINCT ON ( lc.customer_id, clb.id ) lc.customer_id as custid,
									  clb.id AS club_id,
									  clb.cid
								  FROM
									  clubs clb
									  JOIN customer_clubs AS custclb ON ( custclb.club_id::integer = clb.id::integer AND custclb.cid::integer = clb.cid::integer )
									  JOIN lease_customers AS lc ON ( lc.customer_id = custclb.customer_id AND lc.cid = custclb.cid )
		  							  JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
								  WHERE
									  clb.cid = ' . ( int ) $intCid . '
									  AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								) AS membercnt ON ( membercnt.club_id::integer = cl.id::integer AND membercnt.cid::integer = cl.cid::integer)
					WHERE
						cl.cid = ' . ( int ) $intCid . '
						AND cl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cl.deleted_on IS NULL
					   	AND cl.approved_on IS NOT NULL';

		// hides resident clubs if resident don't have permission to add club
		if( false == is_null( $intExcludedCustomerId ) ) {
			$strSql .= ' AND ( cl.customer_id != ' . ( int ) $intExcludedCustomerId . ' OR cl.customer_id IS NULL )';
		}

		$strSql .= ' ORDER BY members_count DESC ' . $strSqlLimit;

		return self::fetchClubs( $strSql, $objDatabase );
	}

	public static function fetchClubsByPropertyIdsByCustomerIdByCid( $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase, $intExcludedCustomerId = NULL, $strSortBy = NULL ) {

		if( false == is_numeric( $intCid ) ) return NULL;
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == is_numeric( $intCustomerId ) ) return NULL;

		$strSql = ' WITH
						clubs_data AS ( ';

		$strSql .= ' SELECT
						DISTINCT cl.id,
						cl.id,
						cl.cid,
						cl.property_id,
						cl.customer_id,
						cl.company_media_file_id,
						cl.begin_datetime,
						cl.name, cl.description,
						cl.is_published,
						cl.approved_by,
						cl.approved_on,
						cl.created_on ';

		$strSql .= ', CASE
						  WHEN 0 < ( SELECT COUNT(id) FROM customer_clubs WHERE club_id = cl.id
							AND cid = cl.cid AND customer_id = ' . ( int ) $intCustomerId . '
							 AND cl.cid = ' . ( int ) $intCid . ' )
						  THEN 1
						  ELSE 0
						END AS is_club_member,
						CASE
						  WHEN ' . ( int ) $intCustomerId . ' = cl.customer_id
						  THEN 1
						  ELSE 0
						END AS is_club_creator,
						CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_clubs WHERE club_id = cl.id
						  	AND cid = cl.cid AND customer_id = ' . ( int ) $intCustomerId . '
						  	AND cl.cid = ' . ( int ) $intCid . ' )
							THEN ( SELECT id FROM customer_clubs WHERE club_id = cl.id
						  	AND cid = cl.cid AND customer_id = ' . ( int ) $intCustomerId . '
						  	AND cl.cid = ' . ( int ) $intCid . ' )
							ELSE NULL
						END AS customer_club_id,
						CASE
							WHEN cl.club_approval_data IS NOT NULL
						  	THEN 1
							ELSE 0
						END AS is_club_approval_value';

		$strSql .= ', COUNT( membercnt.custid ) OVER ( PARTITION BY cl.id ) as members_count
					FROM
						clubs cl
						LEFT JOIN
								(
								  SELECT
									  DISTINCT ON ( lc.customer_id, clb.id ) lc.customer_id as custid,
									  clb.id AS club_id,
									  clb.cid
								  FROM
									  clubs clb
									  JOIN customer_clubs AS custclb ON ( custclb.club_id::integer = clb.id::integer AND custclb.cid::integer = clb.cid::integer )
									  JOIN lease_customers AS lc ON ( lc.customer_id = custclb.customer_id AND lc.cid = custclb.cid )
		  							  JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
								  WHERE
									  clb.cid = ' . ( int ) $intCid . '
									  AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								) AS membercnt ON ( membercnt.club_id::integer = cl.id::integer AND membercnt.cid::integer = cl.cid::integer)
					WHERE
						cl.cid = ' . ( int ) $intCid . '
						AND cl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cl.deleted_on IS NULL';

		// hides resident clubs if resident don't have permission to add club
		if( false == is_null( $intExcludedCustomerId ) ) {
			$strSql .= ' AND ( cl.customer_id != ' . ( int ) $intExcludedCustomerId . ' OR cl.customer_id IS NULL )';
		}
		// new skin > club sorting.
		if( false == is_null( $strSortBy ) ) {
			if( 'POPULAR' == $strSortBy ) {
				$strSql .= ' ORDER BY members_count DESC';
			} else {
				$strSql .= ' ORDER BY cl.created_on ' . $strSortBy;
			}
		} else {
			$strSql .= ' ORDER BY cl.created_on DESC';
		}
		$strSql .= ' ) ';

		$strSql .= ' SELECT
						*
					FROM
						clubs_data cd
					WHERE
						CASE
						WHEN cd.is_club_approval_value = 0
						THEN cd.approved_on IS NOT NULL
						ELSE cd.approved_on IS NULL END ';

		return self::fetchClubs( $strSql, $objDatabase );
	}

	public static function fetchClubsCountByPropertyIdsByCid( $intPropertyId, $intCid, $strClubsSearchFilter, $objDatabase ) {

		if( false == is_numeric( $intCid ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strAndWhere = ( false == is_null( $strClubsSearchFilter ) ) ? ' AND name ILIKE \'%' . trim( addslashes( $strClubsSearchFilter ) ) . '%\'' : '';

		$strWhere = ' WHERE cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND deleted_on IS NULL
						' . $strAndWhere;
		return self::fetchClubCount( $strWhere, $objDatabase );

	}

	public static function fetchCreatedClubsByPropertyIdByCustomerIdByCid( $intPropertyId, $intCustomerId, $intCid, $objDatabase ) {

		$strSql = '	WITH
						clubs_created_data AS ( SELECT cl.* , CASE
													WHEN cl.club_approval_data IS NOT NULL
													  	THEN 1
													ELSE 0
													END AS is_club_approval_value
													FROM
														clubs cl
													WHERE
														cl.property_id = ' . ( int ) $intPropertyId . '
														AND cl.customer_id = ' . ( int ) $intCustomerId . '
														AND cl.cid = ' . ( int ) $intCid . '
														AND cl. deleted_on IS NULL order by created_on DESC ) ';
		$strSql .= ' SELECT
						*
					FROM
						clubs_created_data ccd
					WHERE
						CASE
						WHEN ccd.is_club_approval_value = 0
						THEN ccd.approved_on IS NOT NULL
						ELSE ccd.approved_on IS NULL END ';

		return self::fetchClubs( $strSql, $objDatabase );
	}

	public static function fetchJoinedClubsByPropertyIdsByCustomerIdByCid( $arrintPropertyIds, $intCustomerId, $intCid, $objDatabase, $intExcludedCustomerId = NULL ) {

		if( false == is_numeric( $intCustomerId ) ) return NULL;
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'WITH
						clubs_joined_data AS ( SELECT
						cl.*,
						ccl.id as customer_club_id,
						CASE
						WHEN cl.club_approval_data IS NOT NULL
						THEN 1
						ELSE 0
						END AS is_club_approval_value
					FROM
						clubs cl
						LEFT JOIN customer_clubs ccl ON ( cl.id = ccl.club_id AND cl.cid = ccl.cid AND ccl.customer_id = ' . ( int ) $intCustomerId . ')
					WHERE';

		if( true == is_null( $intExcludedCustomerId ) || 0 == ( int ) $intExcludedCustomerId ) {
			$strSql .= ' (cl.customer_id = ' . ( int ) $intCustomerId . ' OR ccl.customer_id = ' . ( int ) $intCustomerId . ')';
		} else {
			$strSql .= ' ( ccl.customer_id = ' . ( int ) $intExcludedCustomerId . ')';
		}

		$strSql .= 'AND cl.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					  	AND cl.cid = ' . ( int ) $intCid . '
						AND cl.deleted_on IS NULL
					ORDER BY
						ccl.created_on DESC )';

		$strSql .= ' SELECT
						*
					FROM
						clubs_joined_data cjd
					WHERE
						CASE
						WHEN cjd.is_club_approval_value = 0
						THEN cjd.approved_on IS NOT NULL
						ELSE cjd.approved_on IS NULL END ';

		return self::fetchClubs( $strSql, $objDatabase );
	}

	public static function fetchApprovedClubByIdByCustomerIdByCid( $intClubId, $intCustomerId, $intCid, $objDatabase, $arrintPropertyIds = NULL ) {

		if( false == is_numeric( $intClubId ) ) return NULL;
		if( false == is_numeric( $intCustomerId ) ) return NULL;

		$strSqlJoin = '';
		$strSqlSelect = '';

		$strSql = ' WITH
						clubs_data AS ( ';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strSqlSelect = ' , COUNT( membercnt.custid ) OVER ( PARTITION BY cl.id ) as members_count ';
			$strSqlJoin = 'LEFT JOIN
									(
									  SELECT
										  DISTINCT ON ( lc.customer_id ) lc.customer_id as custid,
										  clb.id AS club_id,
										  clb.cid
									  FROM
										  clubs clb
										  JOIN customer_clubs AS custclb ON ( custclb.club_id::integer = clb.id::integer AND custclb.cid::integer = clb.cid::integer )
										  JOIN lease_customers AS lc ON ( lc.customer_id = custclb.customer_id AND lc.cid = custclb.cid )
		  								  JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
									  WHERE
										  clb.cid = ' . ( int ) $intCid . '
										  AND clb.id = ' . ( int ) $intClubId . '
										  AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
									) AS membercnt ON ( membercnt.club_id::integer = cl.id::integer AND membercnt.cid::integer = cl.cid::integer )';
		}

		$strSql .= 'SELECT
						cl.*, p.property_name,
						 CASE
						  WHEN 1 = ( SELECT COUNT(id) FROM customer_clubs WHERE club_id = cl.id AND cid = cl.cid AND customer_id = ' . ( int ) $intCustomerId . '
						  AND cl.cid = ' . ( int ) $intCid . ')
						  THEN 1
						  ELSE 0
						END AS is_club_member,
						CASE
							WHEN cl.club_approval_data IS NOT NULL
						  	THEN 1
							ELSE 0
						END AS is_club_approval_value,
						CASE
							WHEN 1 = ( SELECT COUNT(id) FROM customer_clubs WHERE club_id = cl.id AND cid = cl.cid AND customer_id = ' . ( int ) $intCustomerId . '
							AND cl.cid = ' . ( int ) $intCid . ' )
							THEN ( SELECT id FROM customer_clubs WHERE club_id = cl.id AND cid = cl.cid AND customer_id = ' . ( int ) $intCustomerId . '
							AND cl.cid = ' . ( int ) $intCid . ' )
							ELSE NULL
						END AS customer_club_id
							' . $strSqlSelect . '
					FROM
						clubs cl
						JOIN properties p ON ( cl.property_id = p.id AND cl.cid = p.cid )
						' . $strSqlJoin . '
					WHERE
						cl.id = ' . ( int ) $intClubId . '
						AND cl.cid = ' . ( int ) $intCid . '
						AND cl.deleted_on IS NULL';
		$strSql .= ' ) ';

		$strSql .= ' SELECT
						*
					FROM
						clubs_data cd
					WHERE
						CASE
						WHEN cd.is_club_approval_value = 0
						THEN cd.approved_on IS NOT NULL
						ELSE cd.approved_on IS NULL END ';

		return self::fetchClub( $strSql, $objDatabase );
	}

	public static function fetchClubsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = ' SELECT
						*
					FROM
						Clubs
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND is_published = 1
						AND approved_on IS NOT NULL
						AND deleted_on IS NULL';

		return self::fetchClubs( $strSql, $objDatabase );
	}

	public static function fetchAllClubsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = ' SELECT
						*
					FROM
						Clubs
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND is_published = 1
						AND deleted_on IS NULL';

		return self::fetchClubs( $strSql, $objDatabase );
	}

	public static function fetchSimpleClubByIdByCid( $intClubId, $intCid, $objDatabase ) {
		return self::fetchClub( sprintf( 'SELECT c.* FROM %s AS c WHERE id = %d AND cid = %d LIMIT 1', 'clubs', ( int ) $intClubId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchClubByIdByPropertyIdByCid( $intClubId, $intPropertyId, $intCid, $objDatabase, $returnDataInArray = false ) {

		$strSql = ' SELECT
						cl.*,
						c.name_first,
						c.name_last
					FROM
						clubs cl
						LEFT JOIN customers c ON( c.id = cl.customer_id AND c.cid = cl.cid )
					WHERE
						cl.id = ' . ( int ) $intClubId . '
						AND cl.cid = ' . ( int ) $intCid . '
						AND cl.property_id = ' . ( int ) $intPropertyId . '
					LIMIT 1';

		if( false == $returnDataInArray ) {
			return self::fetchClub( $strSql, $objDatabase );
		} else {
			$arrmixClubData = ( array ) fetchData( $strSql, $objDatabase );
			return $arrmixClubData[0];
		}
	}

	public static function fetchClubCountByNameByPropertyIdByCid( $strName, $intPropertyId, $intCid, $objDatabase ) {

		$strWhereSql = ' WHERE name ILIKE \'' . addslashes( trim( $strName ) ) . '\' AND cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND deleted_on IS NULL';

		return self::fetchClubCount( $strWhereSql, $objDatabase );
	}

	public static function fetchClubCountByNameByIdByPropertyIdByCid( $strName, $intClubId, $intPropertyId, $intCid, $objDatabase ) {

		$strWhereSql = ' WHERE name ILIKE \'' . addslashes( trim( $strName ) ) . '\' AND cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND id != ' . ( int ) $intClubId . ' AND deleted_on IS NULL';

		return self::fetchClubCount( $strWhereSql, $objDatabase );
	}

	public static function fetchPaginatedUnapprovedClubsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = '
					SELECT
						*
					FROM (
						SELECT
							CASE WHEN TRIM( dp.resident_portal_clubs->>\'urgent_club_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - c.created_on ) >= ( dp.resident_portal_clubs->>\'urgent_club_requested_since\' )::int THEN
								  3
								 WHEN TRIM( dp.resident_portal_clubs->>\'important_club_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - c.created_on ) >= ( dp.resident_portal_clubs->>\'important_club_requested_since\' )::int THEN
								  2
								 ELSE
								  1
							END AS priority,
							c.id,
							c.customer_id,
							cl.id AS lease_id,
							c.name AS club,
							c.club_approval_data,
							c.created_on AS requested,
							func_format_customer_name( cust.name_first, cust.name_last ) as customer_name,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) AS unit_number,
							cl.property_name AS property,
							MAX(cl.id) OVER( PARTITION BY c.id ORDER BY cl.id DESC ) AS max_lease_id,
 							tz.time_zone_name,
 							c.property_id AS c_property_id , lc.property_id AS lc_property_id
						FROM
						 	clubs c
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = c.property_id )
							JOIN customers cust ON ( cust.id = c.customer_id AND cust.cid = c.cid )
							JOIN lease_customers lc ON ( lc.customer_id = cust.id AND lc.cid = c.cid )
							JOIN properties p ON ( p.cid = lc.cid AND p.id = lc.property_id )
 							JOIN time_zones tz ON ( tz.id = p.time_zone_id )
							LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = c.cid )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = c.cid )
						WHERE
						  	c.cid = ' . ( int ) $intCid . '
						  	AND c.approved_by IS NULL
						  	AND c.customer_id IS NOT NULL
							AND c.deleted_on IS NULL
					) clubssubquery
				  	WHERE
						clubssubquery.lease_id = clubssubquery.max_lease_id
						' . $strPriorityWhere . '
						ORDER BY ' . $strSortBy . '
				  	OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnapprovedClubsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= '
				SELECT
					count( 1 ) AS ' .
					( false == $boolIsGroupByProperties ?
						' unapproved_clubs_count,
						MAX'
					:
						'resident_clubs,
						property_id,
						property_name,
					' ) .
					'( priority ) AS priority
				FROM (
					SELECT
						rank() OVER( PARTITION BY c.id ORDER BY cl.id DESC ),
						CASE
							WHEN TRIM( dp.resident_portal_clubs->>\'urgent_club_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - c.created_on ) >= ( dp.resident_portal_clubs->>\'urgent_club_requested_since\' )::int THEN
								3
							WHEN TRIM( dp.resident_portal_clubs->>\'important_club_requested_since\' ) != \'\' AND date_part( \'day\', CURRENT_DATE - c.created_on ) >= ( dp.resident_portal_clubs->>\'important_club_requested_since\' )::int THEN
								2
							ELSE
								1
						END AS priority,
						cl.property_id,
						p.property_name
					FROM
						clubs c
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( lp.property_id = c.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
						JOIN properties p ON ( p.cid = c.cid AND p.id = c.property_id )
						JOIN customers cust ON ( cust.id = c.customer_id AND cust.cid = c.cid )
						JOIN lease_customers lc ON ( lc.customer_id = cust.id AND lc.cid = c.cid )
						LEFT JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = c.cid )
						LEFT JOIN dashboard_priorities dp ON ( dp.cid = c.cid )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.approved_by IS NULL
						AND c.customer_id IS NOT NULL
						AND c.deleted_on IS NULL
					GROUP BY
						cl.id,
						c.id,
						c.created_on,
						dp.resident_portal_clubs->>\'urgent_club_requested_since\',
						dp.resident_portal_clubs->>\'important_club_requested_since\',
						cl.property_id,
						p.property_name
					) subQ
				WHERE
					subQ.rank = 1
					' . $strPriorityWhere . ' ' .
				( true == $boolIsGroupByProperties ?
					' GROUP BY
						property_id,
						property_name,
						priority'
				: '' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleUpdatedClubsByIdsByCid( $arrintClubIds, $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) || false == valArr( $arrintClubIds ) ) return NULL;

		$strSql = 'SELECT
						club_approval_data->>\'club_company_media_file_id\' AS club_company_media_file_id
					FROM clubs
					WHERE cid = ' . ( int ) $intCid . '
						AND id IN (' . implode( ',', $arrintClubIds ) . ')
						AND club_approval_data IS NOT NULL
					ORDER BY is_published ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomClubApprovalClubByIdByCid( $intClubId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intClubId ) ) return NULL;

		$strSql = 'SELECT 
						id,
						club_approval_data
					FROM 
						clubs 
					WHERE 
						id = ' . ( int ) $intClubId . ' 
						AND cid = ' . ( int ) $intCid;

		$arrmixClubData = ( array ) fetchData( $strSql, $objDatabase );
		return $arrmixClubData[0];
	}

}
?>