<?php

class CMaintenanceRequestMaterial extends CBaseMaintenanceRequestMaterial {

	protected $m_boolIsPostedOn;
	protected $m_boolIsSingleMaterialEntry;

	protected $m_strCode;

	protected $m_intPropertyId;
	protected $m_intAssetTypeId;
	protected $m_intApPhysicalStatusTypeId;

	protected $m_fltQuantityOnHand;

	protected $m_objAssetDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPostedOn = 1;
		$this->m_boolIsSingleMaterialEntry = false;
		return;
	}

	/**
	 * get Functions
	 *
	 */

	public function getAssetTypeId() {
		return $this->m_intAssetTypeId;
	}

	public function getAssetDetails() {
		return $this->m_objAssetDetails;
	}

	public function getIsPostedOn() {
		return $this->m_boolIsPostedOn;
	}

	public function getQuantityOnHand() {
		return $this->m_fltQuantityOnHand;
	}

	public function getCode() {
		return $this->m_strCode;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getApPhysicalStatusTypeId() {
		return $this->m_intApPhysicalStatusTypeId;
	}

	public function getIsSingleMaterialEntry() {
		return $this->m_boolIsSingleMaterialEntry;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setAssetTypeId( $intAssetTypeId ) {
		$this->m_intAssetTypeId = $intAssetTypeId;
	}

	public function setAssetDetails( $objAssetDetails ) {
		$this->m_objAssetDetails = $objAssetDetails;
	}

	public function setIsPostedOn( $boolIsPostedOn ) {
		$this->m_boolIsPostedOn = $boolIsPostedOn;
	}

	public function setQuantityOnHand( $fltQuantityOnHand ) {
		$this->m_fltQuantityOnHand = $fltQuantityOnHand;
	}

	public function setCode( $strCode ) {
		$this->m_strCode = $strCode;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setApPhysicalStatusTypeId( $intApPhysicalStatusTypeId ) {
		$this->m_intApPhysicalStatusTypeId = $intApPhysicalStatusTypeId;
	}

	public function setIsSingleMaterialEntry( $boolIsSingleMaterialEntry ) {
		$this->m_boolIsSingleMaterialEntry = CStrings::strToBool( $boolIsSingleMaterialEntry );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_posted_on'] ) ) 		$this->setIsPostedOn( $arrmixValues['is_posted_on'] );
		if( true == isset( $arrmixValues['quantity_on_hand'] ) )	$this->setQuantityOnHand( $arrmixValues['quantity_on_hand'] );
		if( true == isset( $arrmixValues['code'] ) )				$this->setCode( $arrmixValues['code'] );
		if( true == isset( $arrmixValues['property_id'] ) )			$this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['ap_physical_status_type_id'] ) ) $this->setApPhysicalStatusTypeId( $arrmixValues['ap_physical_status_type_id'] );
		if( true == isset( $arrmixValues['is_ap_code_disabled'] ) ) $this->setIsSingleMaterialEntry( $arrmixValues['is_single_material_entry'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountId( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getGlAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account is required.' ) ) );
		}

		if( true == is_null( $objDatabase ) ) {
			return $boolIsValid;
		}

		$objGlAccount = CGlAccounts::fetchCustomGlAccountByIdByCid( $this->getCid(), $this->getGlAccountId(), $objDatabase );

		if( true == valObj( $objGlAccount, 'CGlAccount' ) && false == is_null( $objGlAccount->getDisabledBy() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( 'Selected GL account is disabled.' ) ) );
			$boolIsValid = false;
		} elseif( false == valObj( $objGlAccount, 'CGlAccount' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( 'Selected GL account is deleted.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valQuantity( $boolIsTrackInventoryQuantity ) {
		$boolIsValid = true;

		if( false == in_array( $this->getAssetTypeId(), [ CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED, CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED, CAssetType::SERVICES_NOT_TRACKED ] ) && 0 != $this->getIsPostedOn() ) {

			if( true == is_null( $this->getQuantity() ) && false == is_numeric( $this->getQuantity() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', __( 'Quantity is required.' ) ) );
			} elseif( 0 == $this->getQuantity() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', __( 'Quantity should be greater than or equal to one.' ) ) );
			} elseif( true == valId( $this->getId() )
						&& true == $boolIsTrackInventoryQuantity
						&& true == in_array( $this->getAssetTypeId(), CAssetType::$c_arrintInventoryAssetTypeIds )
						&& $this->m_fltQuantity > $this->m_fltQuantityOnHand
						&& 1 == $this->getIsPostedOn() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', __( 'Not enough available quantity to pull from inventory.' ) ) );
			}

		}
		return $boolIsValid;
	}

	public function valRate() {
		$boolIsValid = true;

		if( false == valId( $this->getAssetTypeId() ) ) {
			if( true == is_null( $this->getRate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', __( 'Rate is required.' ) ) );
			} elseif( 0 == $this->getRate() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', __( 'Rate cannot be zero.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valApCodeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', __( 'Material is required.' ) ) );
		}

		if( true == $this->m_boolIsSingleMaterialEntry && false == valId( $this->m_fltQuantityOnHand ) && CAssetType::SERVICES_NOT_TRACKED != $this->getAssetTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Not enough available quantity to pull from inventory for this catalog item.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAssetDetails() {

		$boolIsValid = true;
		if( ( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->getAssetTypeId() || CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED == $this->getAssetTypeId() ) ) {

			if( false == valId( $this->getAssetId() ) && 1 == $this->getIsPostedOn() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_id', __( 'Asset is required.' ) ) );
			} elseif( true == valId( $this->getAssetId() ) && true == valObj( $this->getAssetDetails(), 'CAssetDetail' ) && NULL != $this->getAssetDetails()->getRetiredOn() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_id', __( 'Asset is retired.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPostedOn( $boolIsTrackInventoryQuantity ) {

		$boolIsValid = true;
		if( true == $boolIsTrackInventoryQuantity && true == in_array( $this->getAssetTypeId(), CAssetType::$c_arrintInventoryAssetTypeIds ) ) {

			if( true == $this->m_boolIsPostedOn && true == is_null( $this->m_strPostedOn ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_on', __( 'Post date is required.' ) ) );
			} elseif( strtotime( date( 'm/d/Y' ) ) < strtotime( $this->m_strPostedOn ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_on', __( 'Post date cannot be a future date.' ) ) );
			} elseif( true == isset( $this->m_strPostedOn ) && 0 < strlen( $this->m_strPostedOn ) && 1 !== CValidation::checkDate( $this->m_strPostedOn ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_on', __( 'Post date must be in mm/dd/yyyy form.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsUseItemCatalog = false, $boolIsFromResposiveMobile = false, $boolIsTrackInventoryQuantity = false, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( true != $boolIsUseItemCatalog ) {
					$boolIsValid &= $this->valGlAccountId( $objDatabase );
					$boolIsValid &= $this->valDescription();
				} else {
					$boolIsValid &= $this->valApCodeId();
					$boolIsValid &= $this->valAssetDetails();
					$boolIsValid &= $this->valPostedOn( $boolIsTrackInventoryQuantity );
				}

				if( false == $boolIsUseItemCatalog || false == $boolIsFromResposiveMobile ) {
					$boolIsValid &= $this->valRate();
				}

				$boolIsValid &= $this->valQuantity( $boolIsTrackInventoryQuantity );
				break;

			case VALIDATE_UPDATE:
				if( true != $boolIsUseItemCatalog ) {
					$boolIsValid &= $this->valGlAccountId( $objDatabase );
					$boolIsValid &= $this->valDescription();
				} else {
					$boolIsValid &= $this->valApCodeId();
					$boolIsValid &= $this->valAssetDetails();
					$boolIsValid &= $this->valPostedOn( $boolIsTrackInventoryQuantity );
				}

				if( false == $boolIsUseItemCatalog || false == $boolIsFromResposiveMobile ) {
					$boolIsValid &= $this->valRate();
				}

				$boolIsValid &= $this->valQuantity( $boolIsTrackInventoryQuantity );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>