<?php

class CApPayeeContactLocation extends CBaseApPayeeContactLocation {

	protected $m_strApPayeeLocationName;
	protected $m_intApPayeeId;

    public function getApPayeeLocationName() {
    	return $this->m_strApPayeeLocationName;
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_payee_id'] ) ) {
			$this->setApPayeeId( $arrmixValues['ap_payee_id'] );
		}
	}

	public function setApPayeeId( $intApPayeeId ) {
    	$this->m_intApPayeeId = $intApPayeeId;
	}

	public function getApPayeeId() {
    	return $this->m_intApPayeeId;
	}

    public function setApPayeeLocationName( $strApPayeeLocationName ) {
		$this->m_strApPayeeLocationName = $strApPayeeLocationName;
    }

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeContactId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeLocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPrimary() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;

    }

}
?>