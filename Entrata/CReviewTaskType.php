<?php

class CReviewTaskType extends CBaseReviewTaskType {

	const CONTACT_RESIDENT = 1;
	const MARK_ATTRIBUTES  = 2;
	const RESPOND_ONLINE   = 3;
	const PUBLISH_RESPONSE = 4;

	const CONTACT_RESIDENT_BY_EMAIL = 'by Email';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>