<?php

class CWebMethod extends CBaseWebMethod {

	const GET_AR_CODES								= 1;
	const CAPTURE_AR_PAYMENT						= 2;
	const GET_PREVIOUS_AR_PAYMENT_DETAILS			= 3;
	const GET_OPEN_AR_PAYMENT_TRANSMISSIONS			= 4;
	const CREATE_AR_PAYMENT_TRANSMISSION			= 5;
	const CLOSE_AR_PAYMENT_TRANSMISSION				= 6;
	const CALCULATE_CONVENIENCE_FEE					= 7;
	const PROCESS_AR_PAYMENTS						= 8;
	const PROCESS_AR_PAYMENT						= 9;
	const GET_AR_TRANSACTIONS						= 10;
	const GET_COMPANY_PREFERENCES					= 11;
	const GET_COMPANY_USER							= 12;
	const GET_COMPANY_USERS							= 13;
	const GET_CUSTOMERS	 							= 14;
	const SEARCH_CUSTOMERS	 						= 15;
	const GET_LEASE_STATUS_TYPES	 				= 16;
	const GET_LEASES 								= 17;
	const GET_CLIENT 								= 18;
	const GET_MERCHANT_ACCOUNTS 					= 19;
	const GET_PAYMENT_TYPES 						= 20;
	const GET_ELECTRONIC_PAYMENT_TYPES 				= 21;
	const GET_PROPERTIES 							= 22;
	const VALIDATE_PROPERTY_ID 						= 23;
	const GET_MITSFORMATTED_PROPERTIES 				= 24;
	const GET_PROPERTY_PREFERENCES					= 25;
	const GET_PROPERTY_UNITS						= 26;
	const GET_STATES								= 27;
	const GET_WORK_ORDERS							= 28;
	const GET_PREVIOUS_AR_PAYMENTS_DETAILS			= 29;
	const GET_LEADS									= 30;
	const GET_AR_PAYMENTS							= 31;
	const PROCESS_CLEARING_AR_PAYMENT				= 32;
	const SEND_MITSFORMATTED_PROPERTIES				= 33;
	const SEND_MITSLEADS							= 34;
	const MONEY_GRAM_VALIDATE						= 35;
	const MONEY_GRAM_LOAD							= 38;
	const ANCILLARY_LOGIN							= 39;
	const ANCILLARY_GET_PARTNER_PLANS				= 40;
	const ANCILLARY_VALIDATE_SERVICE_ADDRESS		= 41;
	const ANCILLARY_CREATE_ORDER					= 42;
	const ANCILLARY_MAKE_PAYMENT					= 43;
	const ANCILLARY_GET_ORDER_STATUS				= 44;
}
?>