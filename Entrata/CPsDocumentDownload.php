<?php

class CPsDocumentDownload extends CBasePsDocumentDownload {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsDocumentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDownloadedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            default:
            	// this is default case
            	break;
        }

        return $boolIsValid;
    }
}
?>