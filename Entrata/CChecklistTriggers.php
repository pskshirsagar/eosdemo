<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistTriggers
 * Do not add any new functions to this class.
 */

class CChecklistTriggers extends CBaseChecklistTriggers {

	public static function fetchChecklistTriggers( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CChecklistTrigger', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchChecklistTrigger( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CChecklistTrigger', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>