<?php

use Psi\Eos\Entrata\CFileAssociations;

use Psi\Libraries\ExternalPrinceWrapper\CPrinceFactory;

class CScreeningApplicationRequest extends CBaseScreeningApplicationRequest {
	const VERSION_1A    = 0;
	const VERSION_1B    = 1;

	const CONVENTIONAL_APPLICATION_TYPE_ID 	= 1;
	const CORPORATE_APPLICATION_TYPE_ID 	= 2;

	const SCREENING_DECISION_GUARANTOR_FAILED = 'SCREENING_DECISION_GUARANTOR_FAILED';

	const OFFER_CONDITION_VALID_TYPE_HOURS				    = 0;
	const OFFER_CONDITION_VALID_TYPE_DAYS					= 1;

	const OFFER_CONDITION_EXPIRED_TYPE_EXACT_HOUR			= 0;
	const OFFER_CONDITION_EXPIRED_TYPE_CLOSE_OF_BUSINESS	= 1;
	const OFFER_CONDITION_EXPIRED_TYPE_11_59PM			    = 2;
	const MAX_UPLOAD_FILE_SIZE        						= 2097152;

	public static $c_arrintScreeningDecisionEnabledVendorIds = [ CTransmissionVendor::RESIDENT_VERIFY, CTransmissionVendor::BCS ];

    protected $m_objDocumentManager;

	public static $c_arrmixQuickResponseCompanyPreferences = [ CScreeningDecisionType::APPROVE_WITH_CONDITIONS => 'SCREENING_DECISION_APPROVE_WITH_CONDITION', CScreeningDecisionType::DENIED => 'SCREENING_DECISION_DENIED' ];
    public static $c_arrmixDefaultQuickResponseCompanyPreferences = [ CScreeningDecisionType::APPROVE_WITH_CONDITIONS => 'DECISION_APPROVE_WITH_CONDITION', CScreeningDecisionType::DENIED => 'DECISION_DENY' ];

	protected $m_intPropertyId;
	protected $m_intPrimaryApplicantId;
	protected $m_arrobjScreeningApplicantResults;

	protected $m_strScreeningApplicationRecommendation;

	protected static $c_arrstrAllowedFileTypes  = [ 'application/pdf', 'image/jpeg', 'image/png' ];

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getScreeningApplicantResults() {
		return $this->m_arrobjScreeningApplicantResults;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setScreeningApplicantResults( $arrobjScreeningApplicantResults ) {
		$this->m_arrobjScreeningApplicantResults = $arrobjScreeningApplicantResults;
	}

	public function setScreeningApplicationRecommendation( $intScreeningApplicationRecommendation ) {
		$this->m_strScreeningApplicationRecommendation = CScreeningRecommendationType::getScreeningRecommendationTypeText( $intScreeningApplicationRecommendation );
	}

	public function getScreeningApplicationRecommendation() {
		return $this->m_strScreeningApplicationRecommendation;
	}

	public function setPrimaryApplicantId( $intPrimaryApplicantId ) {
		$this->m_intPrimaryApplicantId = $intPrimaryApplicantId;
	}

	public function getPrimaryApplicantId() {
		return $this->m_intPrimaryApplicantId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemoteScreeningId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningRecommendationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningDecisionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningDecisionUpdatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningDecisionUpdatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valApplicationTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningDecisionReasonTypeIds() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningConditionCodes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valErrorDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['screening_recommendation_type_id'] ) ) $this->setScreeningApplicationRecommendation( $arrmixValues['screening_recommendation_type_id'] );
		if( isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( isset( $arrmixValues['primary_applicant_id'] ) ) $this->setPrimaryApplicantId( $arrmixValues['primary_applicant_id'] );

		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// added default
		}

		return $boolIsValid;
	}

	public function fetchScreeningApplicantResults( $objDatabase ) {
		$arrobjScreeningApplicantResults = CScreeningApplicantResults::fetchScreeningApplicantResultsByScreeningApplicationRequestIdByApplicationIdByCid( $this->getId(), $this->getApplicationId(), $this->getCid(), $objDatabase );
		return rekeyObjects( 'applicantId', $arrobjScreeningApplicantResults );
	}

	public function fetchScreeningApplicantResultByApplicantId( $intApplicantId, $objDatabase ) {
		return CScreeningApplicantResults::fetchScreeningApplicantResultByScreeningApplicationRequestByApplicationIdByApplicantIdByCid( $this->getId(), $this->getApplicationId(), $intApplicantId, $this->getCid(), $objDatabase );
	}

	public function fetchTotalUnScreenedorUnApprovedApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase ) {
		return CScreeningApplicantResults::fetchTotalUnScreenedorUnApprovedApplicantsCountByApplicationIdByCustomerTypeIdsByCid( $this->getApplicationId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function archieveScreeningConditionOffers( $intCurrentUserId, $objDatabase ) {

		$strSql = 'Update
						screening_condition_offers
					SET
						is_published = 0,
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE
						is_published = 1
						AND screening_application_request_id = ' . ( int ) $this->getId() . '
						AND cid = ' . ( int ) $this->getCid();

		executeSql( $strSql, $objDatabase );

		return;
	}

	public function getScreeningConditionOffer( $objDatabase ) {
		return CScreeningConditionOffers::fetchPublishedScreeningConditionOfferByScreeningApplicationRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function isNewConditionWorkFlowEnabled( $objDatabase ) {
		$strWhere = 'WHERE is_published = TRUE AND screening_application_request_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid();
		return ( 0 < CScreeningConditionOffers::fetchScreeningConditionOfferCount( $strWhere, $objDatabase ) ) ? true : false;
	}

	public function getScreeningDecisionReasonNames() {
		$strScreeningDecisionReasons = '';

		if( false == valArr( $this->getScreeningDecisionReasonTypeIds() ) ) return $strScreeningDecisionReasons;

		$arrintReasonClone                  = $this->getScreeningDecisionReasonTypeIds();
		$arrintScreeningDecisionReasonIds   = $this->getScreeningDecisionReasonTypeIds();

		foreach( $arrintScreeningDecisionReasonIds as $intScreeningDecisionReasonId ) {
			if( CScreeningDecisionReasonType::PRECISE_ID == $intScreeningDecisionReasonId ) {
				$strScreeningDecisionReasons .= 'Id Verification';
			} else {
				$strScreeningDecisionReasons .= CScreeningDecisionReasonType::getScreeningDecisionReasonNameById( $intScreeningDecisionReasonId );
			}

			if( next( $arrintReasonClone ) ) {
				$strScreeningDecisionReasons .= ', ';
			}
		}

		return $strScreeningDecisionReasons;
	}

    public function getScreeningDecisionReasonNamesArray() {
        $arrstrScreeningDecisionReasons = [];

        if( false == valArr( $this->getScreeningDecisionReasonTypeIds() ) ) return $arrstrScreeningDecisionReasons;

        $arrintReasonClone                  = $this->getScreeningDecisionReasonTypeIds();
        $arrintScreeningDecisionReasonIds   = $this->getScreeningDecisionReasonTypeIds();

        foreach( $arrintScreeningDecisionReasonIds as $intScreeningDecisionReasonId ) {
            if( CScreeningDecisionReasonType::PRECISE_ID == $intScreeningDecisionReasonId ) {
                $arrstrScreeningDecisionReasons[] = 'Id Verification';
            } else {
                $arrstrScreeningDecisionReasons[] = CScreeningDecisionReasonType::getScreeningDecisionReasonNameById( $intScreeningDecisionReasonId );
            }

        }

        return $arrstrScreeningDecisionReasons;
    }

    public function getScreeningApplicationConditionSets( $objDatabase ) {
		return CScreeningApplicationConditionSets::fetchAppliedConditionsByApplicationIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
	}

	public function offerScreeningConditions( $objCompanyUser, $objApplication, $arrobjPropertyPreferences, $objDatabase ) {
		$objScreeningConditionOffer = new CScreeningConditionOffer();
		$objScreeningConditionOffer->setCid( $this->getCid() );
		$objScreeningConditionOffer->setScreeningApplicationRequestId( $this->getId() );
		$objScreeningConditionOffer->setIsPublished( '1' );

		$strExpiredOn = $this->calculateOfferExpirationDate( new DateTime( date( 'Y-m-d H:i:s' ) ), $arrobjPropertyPreferences, $objDatabase );

		if( false == is_null( $strExpiredOn ) ) $objScreeningConditionOffer->setExpiredOn( $strExpiredOn );

		if( false == $objScreeningConditionOffer->insert( $objCompanyUser->getId(), $objDatabase ) ) {
			trigger_error( 'Failed to add screening condition offer for screening application request id : ' . $this->getId() . ' And cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		$this->updateScreeningOfferStatusWithActivity( $objCompanyUser->getId(), CScreeningConditionOfferStatus::OFFERED, $objApplication, $objDatabase, $objCompanyUser );

		return true;
	}

	public function checkOfferredScreeningCondition( $intOfferedScreeningConditionTypeId, $objDatabase ) {
		$arrobjScreeningApplicationConditionValues = CScreeningApplicationConditionValues::fetchScreeningApplicationConditionValuesByCidByScreeningApplicationConditionSetIdByConditionTypeIds( $this->getCid(), $this->getApplicationId(), [ $intOfferedScreeningConditionTypeId ], $objDatabase );
		return ( true == valArr( $arrobjScreeningApplicationConditionValues ) ) ? true : false;
	}

	private function getOrFetchPropertyId( $objDatabase ) {
		return CApplications::fetchPropertyIdByApplicationIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
	}

	public function processScreeningNotifications( $intCurrentUserId, $intScreeningEventTypeId, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants = false ) {
		$arrobjScreenedApplicants    = CApplicants::fetchScreenedApplicantsByScreeningApplicationRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$arrobjApplicantApplications = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicantIdsByApplicationIdByCid( array_keys( $arrobjScreenedApplicants ), $this->getApplicationId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrobjScreenedApplicants ) || false == valArr( $arrobjApplicantApplications ) ) {
			trigger_error( 'Failed to applicant or applicant application for application id : ' . $this->getApplicationId() . ' And cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		$this->processNotifications( $intCurrentUserId, $arrobjScreenedApplicants, $arrobjApplicantApplications, $intScreeningEventTypeId, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants );
	}

	// Scenario : send letters to specific applicants only

	public function processScreeningApplicantNotifications( $intCurrentUserId, $intScreeningEventTypeId, $arrintApplicantIds, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants = false ) {
		$arrobjApplicants               = [];
		$arrobjApplicantApplications    = [];

		if( true == valArr( $arrintApplicantIds ) ) {
			$arrobjApplicants = CApplicants::fetchScreenedApplicantsByApplicantIdsByApplicationIdByCid( $arrintApplicantIds, $this->getApplicationId(), $this->getCid(), $objDatabase );
			$arrobjApplicantApplications = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicantIdsByApplicationIdByCid( array_keys( $arrobjApplicants ), $this->getApplicationId(), $this->getCid(), $objDatabase );

			if( false == valArr( $arrobjApplicants ) || false == valArr( $arrobjApplicantApplications ) ) {
				trigger_error( 'Failed to applicant or applicant application for application id : ' . $this->getApplicationId() . ' And cid : ' . $this->getCid(), E_USER_WARNING );
				return false;
			}
		}

		$this->processNotifications( $intCurrentUserId, $arrobjApplicants, $arrobjApplicantApplications, $intScreeningEventTypeId, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants );
	}

	private function processNotifications( $intCurrentUserId, $arrobjApplicants, $arrobjApplicantApplications, $intScreeningEventTypeId, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants = false ) {
		$arrobjScreeningNotificationTypes = CScreeningNotificationTypes::fetchScreeningNotificationTypesByScreeningEventTypeId( $intScreeningEventTypeId, $objDatabase );

		if( false == valArr( $arrobjScreeningNotificationTypes ) ) {
			trigger_error( 'Failed to load screening notification type for event type id : ' . ( int ) $intScreeningEventTypeId . ' for cid : ' . $this->getCid(), E_USER_WARNING );
			return;
		}

		$arrobjAllApplicants = [];

		if( true == in_array( $intScreeningEventTypeId, CScreeningEventType::$c_arrintEventAssociatedWithNotifications ) ) {
			$arrobjAllApplicants = CApplicants::fetchApplicantsByApplicationIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
		}

		if( CScreeningEventType::CONTINGENTLY_APPROVED == $intScreeningEventTypeId && true == valArr( $arrobjApplicants ) ) {
			$arrobjAllApplicants = CApplicants::fetchApplicantsByIdsByApplicationIdByCid( array_keys( $arrobjApplicants ), $this->getApplicationId(), $this->getCid(), $objDatabase );
		}

		foreach( $arrobjScreeningNotificationTypes as $objScreeningNotificationType ) {
			$arrobjApplicantList = ( CScreeningMessageType::ADVERSE_ACTION_LETTER == $objScreeningNotificationType->getScreeningMessageTypeId() ) ? $arrobjApplicants : $arrobjAllApplicants;
			if( true == valArr( $arrobjApplicantList ) ) {
				$objScreeningNotificationType->processScreeningNotification( $intCurrentUserId, $this, $arrobjApplicantList, $arrobjApplicantApplications, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants );
			}
		}
	}

	public function fetchApplication( $objDatabase ) {
		return CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
	}

	public function fetchScreenedApplicants( $objDatabase ) {
		return CApplicants::fetchScreenedApplicantsByScreeningApplicationRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase, $boolLoadFromView = true, $boolLoadOnlyResponsibleApplicant = false, $boolIncludeDeletedAA = false, $intNonResponsibleLimit = NULL, $boolIsUnderAgeLimit = false ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		return CApplicants::fetchApplicantsByApplicationIdByCustomerTypeIdsByCid( $this->getApplicationId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase, $boolLoadFromView, $boolLoadOnlyResponsibleApplicant, $boolIncludeDeletedAA, $intNonResponsibleLimit, $boolIsUnderAgeLimit );
	}

	// This function check screening result for latest screening applicant
	// If result is fail returns true else false;

	public function isScreeningFailed( $intCustomerTypeId, $objDatabase ) {
		$objScreeningApplicantResult = CScreeningApplicantResults::fetchLatestScreeningApplicantResultByCustomerTypeIdByScreeningApplicationRequestIdByCid( $intCustomerTypeId, $this->getId(), $this->getCid(), $objDatabase );
		return ( true == valObj( $objScreeningApplicantResult, 'CScreeningApplicantResult' ) && CScreeningRecommendationType::FAIL == $objScreeningApplicantResult->getScreeningRecommendationTypeId() ) ? true : false;
	}

	public function calculateOfferExpirationDate( $strConditionOfferedDate, $arrobjPropertyPreferences, $objDatabase ) {

		if( true == array_key_exists( 'OFFER_VALID_TYPE', $arrobjPropertyPreferences ) && true == array_key_exists( 'OFFER_VALID_FOR', $arrobjPropertyPreferences ) && true == array_key_exists( 'OFFER_EXPIRES_AT', $arrobjPropertyPreferences ) && '' != $arrobjPropertyPreferences['OFFER_VALID_FOR']->getValue() ) {

			$strOfferValidType  = $arrobjPropertyPreferences['OFFER_VALID_TYPE']->getValue();
			$strOfferValidFor   = round( $arrobjPropertyPreferences['OFFER_VALID_FOR']->getValue() );
			$strOfferExpiresAt  = $arrobjPropertyPreferences['OFFER_EXPIRES_AT']->getValue();

			switch( $strOfferValidType ) {

				case self::OFFER_CONDITION_VALID_TYPE_HOURS:
					$strConditionOfferedDate->modify( ' + ' . $strOfferValidFor . ' hours ' );
					return $strConditionOfferedDate->format( 'Y-m-d H:i:s' );
					break;

				case self::OFFER_CONDITION_VALID_TYPE_DAYS:
					switch( $strOfferExpiresAt ) {

						// exact hours
						case self::OFFER_CONDITION_EXPIRED_TYPE_EXACT_HOUR:
							$strOfferVaildInHour = $strOfferValidFor * 24;
							$strConditionOfferedDate->modify( ' + ' . $strOfferVaildInHour . ' hours ' );
							return $strConditionOfferedDate->format( 'Y-m-d H:i:s' );
							break 2;

						// Close of business
						case self::OFFER_CONDITION_EXPIRED_TYPE_CLOSE_OF_BUSINESS:
							$strConditionOfferedDate->modify( ' + ' . $strOfferValidFor . ' days ' );
							$strExpirationDay = $strConditionOfferedDate->format( 'N' );
							$intPropertyId = $this->getOrFetchPropertyId( $objDatabase );
							$objPropertyHours = CPropertyHours::fetchPublishedPropertyHoursKeyedByPropertyIdByDayNumberByCid( $intPropertyId, $strExpirationDay, $this->getCid(), $objDatabase );

							if( true == valObj( $objPropertyHours, 'CPropertyHour' ) && false == is_null( $objPropertyHours->getCloseTime() ) ) {
								return $strConditionOfferedDate->format( 'Y-m-d ' . $objPropertyHours->getCloseTime() . ':00 ' );
							} else {
								return $strConditionOfferedDate->format( 'Y-m-d 17:00:00' );
							}
							break 2;

						// 11:59
						case self::OFFER_CONDITION_EXPIRED_TYPE_11_59PM:
							$strConditionOfferedDate->modify( ' + ' . $strOfferValidFor . ' days ' );
							return $strConditionOfferedDate->format( 'Y-m-d 23:59:59' );
							break 2;

						default:
							NULL;
							break 2;
					}
					break;

				default:
					NULL;
					break;
			}
			return NULL;
		}
	}

	public function fetchApplicantsByRecommendationTypeId( $intScreeningRecommendationTypeId, $objDatabase ) {
		return CApplicants::fetchApplicantsByScreeningApplicationRequestIdByScreeningRecommendationTypeIdByCid( $this->getId(), $intScreeningRecommendationTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantsByRecommendationTypeIdByCustomerTypeId( $intScreeningRecommedationTypeId, $intCustomerTypeId, $objDatabase ) {
		return CApplicants::fetchApplicantsByScreeningApplicationRequestIdByScreeningRecommendationTypeIdByCustomerTypeIdByCid( $this->getId(), $intScreeningRecommedationTypeId, $intCustomerTypeId, $this->getCid(), $objDatabase );
	}

	public function logScreeningConditionActivity( $intCurrentUserId, $intScreeningConditionOfferStatusId, $objApplication, $objDatabase, $objCompanyUser = NULL, $intOfferDecisionBy = NULL, $arrintConditionSubsetValueIds = [] ) {

		$objEventLibrary 			= new CEventLibrary();
		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();

		if( true == valId( $intOfferDecisionBy ) ) {
			$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $intOfferDecisionBy, $objApplication->getCid(), $objDatabase );
			$objEventLibraryDataObject->setCustomer( $objCustomer );
		}

		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setLease( $objApplication->getOrFetchLease( $objDatabase ) );

		$objEvent = $objEventLibrary->createEvent( [ $objApplication ], CEventType::SCREENING_CONDITIONS );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setCompanyUser( $objCompanyUser );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setDoNotExport( true );
		// If you are changing this note then change the same in CApplicationEventDescriptorLibrary.class.php file - function buildEventDescription - case ADVERSE_ACTION_LETTER_GENERATED
		$objEvent->setNotes( CScreeningConditionOfferStatus::getTypeNameByTypeId( $intScreeningConditionOfferStatusId ) );

        $arrmixData = [];
        $arrmixData['condition_name'] = $this->getAdditionalEventDescription( $objEventLibrary, $objApplication, $intScreeningConditionOfferStatusId, $objDatabase, $arrintConditionSubsetValueIds );
        $objEventLibraryDataObject->setData( $arrmixData );

		$objEventLibrary->buildEventDescription( $objApplication );

		$objEventLibrary->setIsExportEvent( false );

		if( false == $objEventLibrary->insertEvent( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to create event for screening condition offer for screening request id : ' . $this->getScreeningApplicationRequestId() . ' And cid : ' . $this->getCid(), E_USER_ERROR );
			return false;
		}

		return true;
	}

	public function updateScreeningOfferStatusWithActivity( $intCurrentUserId, $intScreeningConditionOfferStatusId, $objApplication, $objDatabase, $objCompanyUser = NULL, $intOfferDecisionBy = NULL, $arrintConditionSubsetValueIds = [] ) {
		$objScreeningConditionOffer = $this->getScreeningConditionOffer( $objDatabase );

		if( false == valObj( $objScreeningConditionOffer, 'CScreeningConditionOffer' ) || CScreeningConditionOfferStatus::ACCEPTED == $objScreeningConditionOffer->getScreeningConditionOfferStatusId() ) {
			return;
		}

		$objScreeningConditionOffer->setScreeningConditionOfferStatusId( $intScreeningConditionOfferStatusId );
		$objScreeningConditionOffer->setIpAddress( $_SERVER['REMOTE_ADDR'] );
		$objScreeningConditionOffer->setOfferDecisionBy( $intOfferDecisionBy );
		$objScreeningConditionOffer->setOfferDecisionOn( 'NOW()' );

		if( false == $objScreeningConditionOffer->update( $intCurrentUserId, $objDatabase ) ) {
			trigger_error( 'Failed to update screening condition offer log for screening request id : ' . $this->getScreeningApplicationRequestId() . ' And cid : ' . $this->getCid(), E_USER_ERROR );
		}

		$this->logScreeningConditionActivity( $intCurrentUserId, $intScreeningConditionOfferStatusId, $objApplication, $objDatabase, $objCompanyUser, $intOfferDecisionBy, $arrintConditionSubsetValueIds );

		return;
	}

	public function setAdditionalEventDescription( $objEventLibrary, $objApplication, $intScreeningConditionOfferStatusId, $objDatabase, $arrintConditionSubsetValueIds = [] ) {

		$strEventDescription = $objEventLibrary->getEventLibraryDataObject()->getEvent()->getEventHandle();

		switch( $intScreeningConditionOfferStatusId ) {
			case CScreeningConditionOfferStatus::OFFERED:
				$strConditionNames = CScreeningApplicationConditionValues::fetchScreeningApplicationConditionNamesBySubsetValueIdsOrSatisfiedOnlyByApplicationIdByCid( $objApplication->getId(), $this->getCid(), $objDatabase );
				break;

			case CScreeningConditionOfferStatus::ACCEPTED:
				if( false == valArr( $arrintConditionSubsetValueIds ) ) {
					// from pp
					$strConditionNames = CScreeningApplicationConditionValues::fetchScreeningApplicationConditionNamesBySubsetValueIdsOrSatisfiedOnlyByApplicationIdByCid( $objApplication->getId(), $this->getCid(), $objDatabase, [],  true );
				} else {
					// from entrata
					$strConditionNames = CScreeningApplicationConditionValues::fetchScreeningApplicationConditionNamesBySubsetValueIdsOrSatisfiedOnlyByApplicationIdByCid( $objApplication->getId(), $this->getCid(), $objDatabase, $arrintConditionSubsetValueIds );
				}
				break;

			default:
				// default action to do
				break;
		}

		if( false == valStr( $strConditionNames ) )  return;

		$strEventDescription .= '<br/>';
		$strEventDescription .= $strConditionNames;
		$objEventLibrary->getEventLibraryDataObject()->getEvent()->setEventHandle( $strEventDescription );

	}

    public function getAdditionalEventDescription( $objEventLibrary, $objApplication, $intScreeningConditionOfferStatusId, $objDatabase, $arrintConditionSubsetValueIds = [] ) {

        $strEventDescription = $objEventLibrary->getEventLibraryDataObject()->getEvent()->getEventHandle();

        switch( $intScreeningConditionOfferStatusId ) {
            case CScreeningConditionOfferStatus::OFFERED:
                $strConditionNames = CScreeningApplicationConditionValues::fetchScreeningApplicationConditionNamesBySubsetValueIdsOrSatisfiedOnlyByApplicationIdByCid( $objApplication->getId(), $this->getCid(), $objDatabase );
                break;

            case CScreeningConditionOfferStatus::ACCEPTED:
                if( false == valArr( $arrintConditionSubsetValueIds ) ) {
                    // from pp
                    $strConditionNames = CScreeningApplicationConditionValues::fetchScreeningApplicationConditionNamesBySubsetValueIdsOrSatisfiedOnlyByApplicationIdByCid( $objApplication->getId(), $this->getCid(), $objDatabase, [],  true );
                } else {
                    // from entrata
                    $strConditionNames = CScreeningApplicationConditionValues::fetchScreeningApplicationConditionNamesBySubsetValueIdsOrSatisfiedOnlyByApplicationIdByCid( $objApplication->getId(), $this->getCid(), $objDatabase, $arrintConditionSubsetValueIds );
                }
                break;

            default:
                // default action to do
                break;
        }

        if( false == valStr( $strConditionNames ) )  return;

        return $strConditionNames;
    }

	public function resetAALetterSentOn( $objCompanyUser, $objDatabase ) {

		$strSql = 'Update
						screening_applicant_results
					SET
						adverse_action_letter_sent_on = NULL ,
						updated_by = ' . ( int ) $objCompanyUser->getId() . ',
						updated_on = NOW()
					WHERE
						application_id = ' . ( int ) $this->getApplicationId() . '
						AND cid = ' . ( int ) $this->getCid();

		executeSql( $strSql, $objDatabase );

		return;
	}

	public function compareScreeningApplicantResult( $intCustomerTypeId, $intScreeningRecommendationTypeId, $objDatabase ) {
		$objScreeningApplicantResult = CScreeningApplicantResults::fetchLatestScreeningApplicantResultByCustomerTypeIdByScreeningApplicationRequestIdByCid( $intCustomerTypeId, $this->getId(), $this->getCid(), $objDatabase );
		return ( true == valObj( $objScreeningApplicantResult, 'CScreeningApplicantResult' ) && $intScreeningRecommendationTypeId == $objScreeningApplicantResult->getScreeningRecommendationTypeId() ) ? true : false;
	}

	public function postScreeningApprovalCharges( $objApplication, $objDatabase ) {
		$boolHasScheduledCharges = true;

		if( true == in_array( $this->getScreeningDecisionTypeId(), [ CScreeningDecisionType::APPROVE, CScreeningDecisionType::APPROVE_WITH_CONDITIONS ] ) ) {
			$boolHasScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIdByLeaseIntervalIdByArTriggerIdByCid( $objApplication->getLeaseId(), $objApplication->getLeaseIntervalId(), CArTrigger::SCREENING_APPROVED, $this->getCid(), $objDatabase );
		}

		if( true == $boolHasScheduledCharges ) {
			$boolIsScreeningApproved = ( true == in_array( $this->getScreeningDecisionTypeId(), [ CScreeningDecisionType::APPROVE, CScreeningDecisionType::APPROVE_WITH_CONDITIONS ] ) ) ? true : false;
			CScreeningUtils::addRabbitMqScreeningMessage( $boolIsScreeningApproved, $this->getScreeningDecisionUpdatedBy(), $objApplication );
		}
	}

	public function fetchTotalUnScreenedApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase ) {
		return CScreeningApplicantResults::fetchTotalScreenedApplicantsCountByApplicationIdByCustomerTypeIdsByCid( $this->getApplicationId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

    public function maintainScreeningApplicantResultLogs( $intCurrentUserId, $arrintApplicantIds, $objStdScreeningResponse, $objDatabase ) {
	    $arrobjScreeningApplicantResults = $this->fetchScreeningApplicantResults( $objDatabase );

	    if( false == valArr( $arrobjScreeningApplicantResults ) ) return;

        $arrobjStdApplicationResult = ( true == array_key_exists( 'screeningApplicantResults', $objStdScreeningResponse ) ) ? $objStdScreeningResponse->screeningApplicantResults : '';

        if( false == valArr( $arrobjStdApplicationResult ) ) {
            trigger_error( 'Failed to build screening response', E_USER_WARNING );
            return;
        }

        $arrobjResultLogs = [];

        foreach( $arrobjStdApplicationResult as $intScreeningApplicantId => $objStdScreeningApplicantResult ) {
	        if( true == in_array( $intScreeningApplicantId, $arrintApplicantIds ) || false == valArr( $arrintApplicantIds ) ) {
                $arrobjRekeyedScreeningApplicantResult 	= rekeyObjects( 'applicantId', $arrobjScreeningApplicantResults );
                $objScreeningApplicantResult 			= ( true == valArr( $arrobjRekeyedScreeningApplicantResult ) && true == array_key_exists( $intScreeningApplicantId, $arrobjRekeyedScreeningApplicantResult ) ) ? $arrobjRekeyedScreeningApplicantResult[$intScreeningApplicantId] : NULL;

                if( true == valObj( $objScreeningApplicantResult, 'CScreeningApplicantResult' ) ) {
                    $objScreeningApplicantResultLog = $objScreeningApplicantResult->createScreeningApplicantResultLog();

                    $objScreeningApplicantResultLog->setScreeningPackageId( $objStdScreeningApplicantResult->screeningPackageId );
                    $objScreeningApplicantResultLog->setScreeningPackageName( $objStdScreeningApplicantResult->screeningPackageName );
                    $objScreeningApplicantResultLog->setScreeningRecommendationTypeId( ( NULL == $objStdScreeningApplicantResult->screeningApplicantRecommendationId ) ? CScreeningRecommendationType::PENDING_UNKNOWN : $objStdScreeningApplicantResult->screeningApplicantRecommendationId );
                    $objScreeningApplicantResultLog->setScreeningApplicantScreenTypeResult( json_encode( $objStdScreeningApplicantResult->screenTypeResult ) );

                    $arrobjResultLogs[] = $objScreeningApplicantResultLog;
                }
            }
        }

	    if( false == CScreeningApplicantResultLogs::bulkInsert( $arrobjResultLogs, $intCurrentUserId, $objDatabase ) ) {
            trigger_error( 'Failed to insert screening applicant result logs for request id : ' . $this->getId() . ' Cid : ' . $this->getCid(), E_USER_WARNING );
            return false;
        }

        return true;
    }

	public function sendScreeningStatusEmails( $arrintScreeningStatusTypeApplicantIds, $objStdScreeningData, $objDatabase ) {

        $objResidentVerifyEmail = CResidentVerifyEmailLibraryFactory::createEmailLibrary( CResidentVerifyEmailTriggers::SEND_SUPPORT_EMAIL );

        foreach( $arrintScreeningStatusTypeApplicantIds as $intScreeningStatusType => $arrintApplicantIds ) {
	        if( false == valArr( $arrintApplicantIds ) ) continue;
            switch( $intScreeningStatusType ) {
                case CScreeningStatusType::APPLICANT_RECORD_STATUS_ID_VERIFICATION_PENDING:
                    $objResidentVerifyEmail->sendIdVerificationApplicantEmail( $arrintApplicantIds, $objStdScreeningData, $objDatabase );
                    break;

                case CScreenType::CREDIT . '-' . CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW:
                    $objResidentVerifyEmail->sendExperianPINAccessEmail( CScreenType::CREDIT, $arrintApplicantIds, $objStdScreeningData, $objDatabase );
                    break;

	            case CScreenType::PRECISE_ID . '-' . CScreeningStatusType::APPLICANT_RECORD_STATUS_MANUAL_REVIEW:
		            $objResidentVerifyEmail->sendExperianPINAccessEmail( CScreenType::PRECISE_ID, $arrintApplicantIds, $objStdScreeningData, $objDatabase );
		            break;

                default:
            }
        }

        return true;
    }

	public function logRentalHistoryDocumentUploadEvent( $objApplication, $intEventTypeId, $intApplicantId, $intCompanyUserId, $objDatabase ) {

		// get the basic contact details and required details of the applicant
		$arrmixApplicantDetails = CApplicants::fetchCustomApplicantContactDetailsByApplicationIdByApplicantIdByCid( $objApplication->getId(), $intApplicantId, $objApplication->getCid(), $objDatabase );
		$objFileAssociation     = CFileAssociations::createService()->fetchLatestFileAssociationByApplicantIdByApplicationIdBySystemCodeByCid( $intApplicantId, $objApplication->getId(), CFileType::SYSTEM_CODE_RENTAL_COLLECTION_PAYMENT_PROOF, $objApplication->getCid(), $objDatabase );

		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEvent          = $objEventLibrary->createEvent( [ $objApplication ], $intEventTypeId );

		$arrmixLogData['isReUpload'] = false;
		if( true == valObj( $objFileAssociation, 'CFileAssociation' ) ) {
			$arrmixLogData['isReUpload'] = true;
		}

		$arrmixLogData['applicant_name'] = $arrmixApplicantDetails['name_first'] . ' ' . $arrmixApplicantDetails['name_last'];

		$objEvent->setDataReferenceId( NULL );
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setLease( $objApplication->getOrFetchLease( $objDatabase ) );
		$objEventLibraryDataObject->setData( $arrmixLogData );

		$objEvent->setCid( $objApplication->getCid() );
		$objEvent->setOldStatusId( $objApplication->getApplicationStatusId() );
		$objEvent->setOldStageId( $objApplication->getApplicationStageId() );
		$objEvent->setNewStageId( $objApplication->getApplicationStageId() );
		$objEvent->setNewStatusId( $objApplication->getApplicationStatusId() );
		$objEvent->setNewStatusId( $objApplication->getApplicationStatusId() );
		$objEvent->setCustomerId( $arrmixApplicantDetails['customer_id'] );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setDoNotExport( true );

		$objEventLibrary->buildEventDescription( $objApplication );
		$objEventLibrary->setIsExportEvent( false );

		if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
			$objApplication->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function updateScreeningConditionOfferExpireOnDate( $intCurrentUserId, $objDatabase ) {
		$objScreeningConditionOffer = $this->getScreeningConditionOffer( $objDatabase );
		if( true == valObj( $objScreeningConditionOffer, 'CScreeningConditionOffer' ) ) {
			$objScreeningConditionOffer->setScreeningConditionOfferStatusId( CScreeningConditionOfferStatus::EXPIRED );
			$objScreeningConditionOffer->setExpiredOn( 'now()' );

			if( false == $objScreeningConditionOffer->update( $intCurrentUserId, $objDatabase ) ) {
				trigger_error( 'Failed to update screening condition offer log for screening request id : ' . $objScreeningConditionOffer->getScreeningApplicationRequestId() . ' And cid : ' . $objScreeningConditionOffer->getCid(), E_USER_ERROR );
				$this->m_objDatabase->rollback();
			}
		}
	}

	public function sendGuarantorFailedScreeningNotifications( $intCurrentUserId, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants = true ) {
		$arrobjScreenedApplicants    = CApplicants::fetchScreenedApplicantsByScreeningApplicationRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$arrobjApplicantApplications = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicantIdsByApplicationIdByCid( array_keys( $arrobjScreenedApplicants ), $this->getApplicationId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrobjScreenedApplicants ) || false == valArr( $arrobjApplicantApplications ) ) {
			trigger_error( 'Failed to applicant or applicant application for application id : ' . $this->getApplicationId() . ' And cid : ' . $this->getCid(), E_USER_WARNING );
			return false;
		}

		$arrobjScreeningNotificationTypes = CScreeningNotificationTypes::fetchScreeningNotificationTypesByScreeningEventTypeId( CScreeningEventType::RESULT_GUARANTOR_FAIL, $objDatabase );

		if( false == valArr( $arrobjScreeningNotificationTypes ) ) {
			trigger_error( 'Failed to load screening notification type for event type id for cid : ' . $this->getCid(), E_USER_WARNING );
			return;
		}

		foreach( $arrobjScreeningNotificationTypes as $objScreeningNotificationType ) {
			$objScreeningNotificationType->setExcludeAALetter( true );
			$objScreeningNotificationType->processScreeningNotification( $intCurrentUserId, $this, $arrobjScreenedApplicants, $arrobjApplicantApplications, $objDatabase, $objEmailDatabase, $boolOfferConditionsToApplicants );
		}
	}

	public function updateScreeningApplicantResultStatus( $intScreeningApplicantScreeningStatus, $intCurrentUserId ) {

		$strSql = 'UPDATE
						screening_applicant_results
					SET
						request_status_type_id = ' . ( int ) $intScreeningApplicantScreeningStatus . ',
						updated_on = NOW(),
						updated_by = ' . ( int ) $intCurrentUserId . '
					WHERE
						application_id = ' . ( int ) $this->getApplicationId() . '
						AND cid = ' . ( int ) $this->getCid();

		if( false == executeSql( $strSql, $this->m_objDatabase ) ) {
			trigger_error( 'Failed to update screening applicant result', E_USER_WARNING );

			return false;
		}

		return true;

	}

}
?>