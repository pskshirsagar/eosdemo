<?php

class CBudgetDataSourceType extends CBaseBudgetDataSourceType {

	const SYSTEM_DATA		= 1;
	const CUSTOM_FORMULAS	= 2;
	const MANUAL_INPUT		= 3;
	const ITEMIZED			= 4;

	public static $c_arrintBudgetDataSourceTypes = [
		self::SYSTEM_DATA,
		self::CUSTOM_FORMULAS,
		self::MANUAL_INPUT,
		self::ITEMIZED
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>