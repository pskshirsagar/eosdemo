<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliationLogs
 * Do not add any new functions to this class.
 */

class CGlReconciliationLogs extends CBaseGlReconciliationLogs {

	public static function fetchGlReconciliationLogs( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return self::fetchObjects( $strSql, 'CGlReconciliationLog', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false, $boolIsReturnKeyedArray );
	}

	public static function fetchGlReconciliationLog( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CGlReconciliationLog', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGlReconciliationLogsByGlReconciliationIdByActionsByCid( $intGlReconciliationId, $arrstrActions, $intCid, $objClientDatabase ) {

		if( true == is_null( $intGlReconciliationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliation_logs
					WHERE
						gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
						AND cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrActions ) ) {
			$strSql .= ' AND action IN ( \'' . implode( '\',\'', $arrstrActions ) . '\' )';
		}

		$strSql .= ' ORDER BY
						id DESC';

		return self::fetchGlReconciliationLogs( $strSql, $objClientDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchPreviousGlReconciliationLogByIdByGlReconciliationIdByCid( $intGlReconciliationLogId, $intGlReconciliationId, $intCid, $objClientDatabase ) {

		if( true == is_null( $intGlReconciliationLogId ) || true == is_null( $intGlReconciliationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliation_logs
					WHERE
						gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
						AND cid = ' . ( int ) $intCid . '
						AND id < ' . ( int ) $intGlReconciliationLogId . '
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchGlReconciliationLog( $strSql, $objClientDatabase );
	}
}
?>