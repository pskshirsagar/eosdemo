<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultRevenueRateConstraints
 * Do not add any new functions to this class.
 */

class CDefaultRevenueRateConstraints extends CBaseDefaultRevenueRateConstraints {

	public static function fetchDefaultRevenueRateConstraints( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultRevenueRateConstraint', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchDefaultRevenueRateConstraint( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultRevenueRateConstraint', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllDefaultRevenueRateConstraints( $objDatabase ) {
		return self::fetchCachedObjects( 'SELECT * FROM default_revenue_rate_constraints', 'CDefaultRevenueRateConstraint', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllDefaultRevenueRateConstraintsByOccupancyTypeId( $objDatabase, $intIsStudentProperty = 0 ) {
		if( 1 == $intIsStudentProperty ) {
			$strConstraintTypeIdCondition = 'revenue_rate_constraint_type_id > 12';
		} else {
			$strConstraintTypeIdCondition = 'revenue_rate_constraint_type_id <= 12';
		}
		$strSql = 'SELECT * FROM
    					default_revenue_rate_constraints
    				WHERE ' . $strConstraintTypeIdCondition;
		return self::fetchDefaultRevenueRateConstraints( $strSql, $objDatabase );
	}

}
?>