<?php

class CBudgetAssumptionMonth extends CBaseBudgetAssumptionMonth {

	protected $m_strKey;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBudgetAssumptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;

		if( true == is_null( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Value is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOriginalValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_DELETE:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valValue();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function setKey( $strKey ) {
		$this->m_strKey = $strKey;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['key'] ) ) {
			$this->setKey( $arrmixValues['key'] );
		}
	}

}

?>