<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataBlobTypes
 * Do not add any new functions to this class.
 */

class CDataBlobTypes extends CBaseDataBlobTypes {

	public static function fetchDataBlobTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDataBlobType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDataBlobType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDataBlobType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>