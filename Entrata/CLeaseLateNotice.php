<?php

class CLeaseLateNotice extends CBaseLeaseLateNotice {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLateNoticeTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLateNoticeDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsCancelled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$this->valCid();
            	$this->valLeaseId();
            	$this->valPropertyId();
            	$this->valCustomerId();
            	$this->valLateNoticeTypeId();
            	$this->valLateNoticeDate();
	            break;

            case VALIDATE_DELETE:
    	        break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>