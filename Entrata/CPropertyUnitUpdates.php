<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyUnitUpdates
 * Do not add any new functions to this class.
 */

class CPropertyUnitUpdates extends CBasePropertyUnitUpdates {

    public static function fetchPropertyUnitUpdatesByPropertyBuildingIdByPropertyIdByCId( $intPropertyBuildingId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIsFromUnmergedUnitSpace = false ) {

    	$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

    	$strJoinCondition = ( false == $boolIsFromUnmergedUnitSpace ) ? ' JOIN property_buildings pb ON pu.cid = pb.cid AND pb.deleted_on IS NULL ' . ( true == is_numeric( $intPropertyBuildingId ) ? 'AND pu.property_building_id = pb.id' : 'AND pu.property_building_id IS NULL' ) : '';

    	$strSql = '	SELECT
						DISTINCT puu.unit_number_new,
						puu.property_unit_id_new
					FROM
						property_unit_updates puu
						JOIN property_units pu ON puu.cid = pu.cid AND puu.property_unit_id_current = pu.id ' . $strCheckDeletedUnitsSql
						. $strJoinCondition . '
					WHERE
						puu.cid = ' . ( int ) $intCid . '
						AND puu.property_id = ' . ( int ) $intPropertyId .
    							( ( true == is_numeric( $intPropertyBuildingId ) && false == $boolIsFromUnmergedUnitSpace ) ? 'AND pb.id = ' . ( int ) $intPropertyBuildingId : '' ) . '
					ORDER BY
						unit_number_new';

    	return fetchData( $strSql, $objDatabase );
    }

	public static function fetchPropertyUnitUpdatePropertiesByCId( $intCid, $objDatabase ) {

    	$strSql = '	SELECT
					    DISTINCT puu.property_id,
    					puu.unit_number_pattern_id
					FROM
					    property_unit_updates puu
					WHERE
					    puu.cid = ' . ( int ) $intCid;

    	return fetchData( $strSql, $objDatabase );
    }

	public static function fetchPropertyUnitUpdatesByPropertyUnitIdNewByPropertyIdByCId( $intPropertyUnitIdNew, $intPropertyId, $intCid, $objDatabase ) {

    	$strSql = '	SELECT
						puu.space_number
					FROM
						property_unit_updates puu
					WHERE
						puu.cid = ' . ( int ) $intCid . '
						AND puu.property_id = ' . ( int ) $intPropertyId . '
						AND puu.property_unit_id_new = ' . ( int ) $intPropertyUnitIdNew . '
					ORDER BY
						puu.updated_on desc,
						puu.id desc
					LIMIT 1';

    	return fetchData( $strSql, $objDatabase );
    }

	public static function fetchPropertyUnitUpdatesByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false ) {

    	$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

	    $strSql = '	SELECT
					    pb.building_name,
					    puu.unit_number_old,
					    puu.unit_number_new,
					    puu.space_number,
					    puu.unit_changed,
					    puu.property_unit_id_current,
					    puu.property_unit_id_new,
					    puu.unit_space_id,
					    puu.property_id,
					    pb.id AS building_id
				    FROM
					    property_unit_updates puu
					    JOIN property_units pu ON puu.cid = pu.cid AND puu.property_unit_id_new = pu.id ' . $strCheckDeletedUnitsSql . '
					    LEFT JOIN property_buildings pb ON pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL
				    WHERE
					    puu.cid = ' . ( int ) $intCid . '
						AND puu.property_id = ' . ( int ) $intPropertyId . '
					ORDER BY
						1,
						3,
						4';

    	return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnMergedUnitSpacesByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

    	$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
    	$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

    	$strSql = '	SELECT
					    DISTINCT us.id,
					    pb.building_name,
					    us.unit_number_cache,
					    us.property_id,
					    us.property_unit_id,
					    pb.id AS building_id,
					    puu.property_unit_id_current
					FROM
					    property_units pu
					    LEFT JOIN property_unit_updates puu ON ( puu.cid = pu.cid AND puu.property_id = pu.property_id AND pu.id = puu.property_unit_id_current )
					    JOIN unit_spaces us ON ( us.property_unit_id = pu.id AND us.cid = pu.cid AND us.property_id = pu.property_id ' . $strCheckDeletedUnitSpacesSql . ' )
					    LEFT JOIN property_buildings pb ON ( pu.cid = pb.cid AND pu.property_building_id = pb.id AND pb.deleted_on IS NULL )
					WHERE
					    pu.cid = ' . ( int ) $intCid . '
					    ' . $strCheckDeletedUnitsSql . '
					    AND pu.property_id = ' . ( int ) $intPropertyId . '
					    AND puu.property_unit_id_current IS NULL
					    AND pu.id IN (
					                   SELECT
					                       property_unit_id
					                   FROM
					                       unit_spaces us
					                   WHERE
					                       us.cid = ' . ( int ) $intCid . '
					                       AND us.property_id = ' . ( int ) $intPropertyId . '
					                       ' . $strCheckDeletedUnitSpacesSql . '
					                   GROUP BY
					                       us.property_unit_id
					                   HAVING
					                       Count ( us.property_unit_id ) = 1
					    )
					ORDER BY
					    1,
					    2';

    	return fetchData( $strSql, $objDatabase );
    }

	public static function updatePropertyUnitUpdatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intTotalUnitCount, $intLimitToProcessUnitMerge = 5 ) {

    	if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) || false == valId( $intTotalUnitCount ) ) return false;

    	$objDataset = $objDatabase->createDataset();

    	for( $i = 0; $i <= $intTotalUnitCount; $i += $intLimitToProcessUnitMerge ) {

		    $strSql = 'SELECT * FROM fun_process_property_unit_merge( ' . $intPropertyId . '::integer, ' . $intCid . '::integer, ' . $intLimitToProcessUnitMerge . '::integer ) AS result;';

    		if( false == $objDataset->execute( $strSql ) || 0 < $objDataset->getRecordCount() ) {
    			$objDataset->cleanup();
    			return false;
    		}

    		$objDataset->cleanup();
    	}

    	return true;
    }

	public static function validatePropertyUnitUpdatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

    	if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return false;

    	$strSql = ' SELECT
						COUNT( puu.id )
					FROM
						property_unit_updates puu
					WHERE
						puu.cid = ' . ( int ) $intCid . '
						AND puu.property_id = ' . ( int ) $intPropertyId;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return false;
    }

	public static function fetchDistinctPropertyUnitUpdatesCountByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

    	if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return false;

    	$strSql = ' SELECT
						count ( DISTINCT puu.property_unit_id_new )
					FROM
						property_unit_updates puu
					WHERE
						puu.cid = ' . ( int ) $intCid . '
						AND puu.property_id = ' . ( int ) $intPropertyId;

    	$arrstrData = fetchData( $strSql, $objDatabase );

    	if( true == valArr( $arrstrData ) && true == isset ( $arrstrData[0]['count'] ) ) {
    		return $arrstrData[0]['count'];
    	} else {
    		return 0;
    	}
    }

	public static function validatePropertyUnitUpdatesForUnitNumberByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false ) {

    	$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

    	if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return false;

    	$strSql = ' SELECT
					   count ( pu.id )
					FROM
					    property_unit_updates puu
					    JOIN property_units pu ON ( puu.unit_number_new = pu.unit_number AND puu.cid = pu.cid AND puu.property_id = pu.property_id ' . $strCheckDeletedUnitsSql . ' )
					WHERE
						puu.cid = ' . ( int ) $intCid . '
						AND puu.property_id = ' . ( int ) $intPropertyId;

    	$arrstrData = fetchData( $strSql, $objDatabase );

    	if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

    	return false;
    }

	public static function insertPropertyUnitUpdatesWithUnitSpaceNamePatternByPropertyIdByCid( $intCompanyUserId, $objUnitSpaceNamePattern, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

    	$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
    	$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

    	if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return false;

    	$objDataset = $objDatabase->createDataset();

    	$strSql = 'INSERT INTO property_unit_updates SELECT * FROM (
	                            WITH property_units_tweaked AS (
	                                SELECT
		                                pu.cid,
										pu.property_id,
										pu.property_building_id,
										pu.id as current_property_unit_id,
										pu.property_floorplan_id,
                                        CASE WHEN  pu1.id IS NOT NULL THEN pu1.id 
                                        ELSE pu.id END AS id,
										pu.unit_number AS unit_number_old,
										substring(pu.unit_number from \'' . $objUnitSpaceNamePattern->getUnitNumberPattern() . '\') AS unit_number,
										upper(substring(pu.unit_number from \'' . $objUnitSpaceNamePattern->getUnitSpacePattern() . '\' ) ) AS space_number,
										rank() OVER (PARTITION BY pu.property_id, substring(pu.unit_number from \'' . $objUnitSpaceNamePattern->getUnitNumberPattern() . '\') ORDER BY pu.unit_number) AS rank,
	                                    us.property_unit_id
                                    FROM
	                                    property_units pu
	                                    JOIN unit_spaces us ON ( pu.cid = us.cid AND pu.id = us.property_unit_id ' . $strCheckDeletedUnitSpacesSql . ' )
	                                    LEFT JOIN property_units pu1 ON ( pu1.property_id = ' . ( int ) $intPropertyId . ' and pu1.cid = ' . ( int ) $intCid . ' AND pu1.cid = pu.cid AND substring(pu.unit_number from \'' . $objUnitSpaceNamePattern->getUnitNumberPattern() . '\') = pu1.unit_number AND pu1.deleted_by IS NULL AND ( pu.property_building_id IS NULL AND pu1.property_building_id IS NULL OR pu.property_building_id = pu1.property_building_id ) )
	                               WHERE
	                                    pu.cid = ' . ( int ) $intCid . '
	                                    ' . $strCheckDeletedUnitsSql . '
	                                    AND us.property_id = ' . ( int ) $intPropertyId . '
	                                    AND pu.id IN (
	                                                   SELECT
	                                                       property_unit_id
	                                                   FROM
	                                                       unit_spaces us
	                                                   WHERE
	                                                       us.cid = ' . ( int ) $intCid . '
	                                                       ' . $strCheckDeletedUnitSpacesSql . '
	                                                       AND us.property_id = ' . ( int ) $intPropertyId . '
	                                                   GROUP BY
	                                                       us.property_unit_id
	                                                   HAVING
	                                                       Count ( us.property_unit_id ) = 1
	                                    )
	                            )
	                            SELECT
	                                nextval(\'property_unit_updates_id_seq\'),
	                                put.cid,
	                                put.property_id,
	                                put.current_property_unit_id AS property_unit_id_current,
	                                put_main.id AS property_unit_id_new,
	                                put.current_property_unit_id <> put_main.id AS unit_changed,
	                                put.unit_number_old,
	                                put.unit_number AS unit_number_new,
	                                us.id AS unit_space_id,
	                                put.space_number, ' .
	                                $objUnitSpaceNamePattern->getId() . ',' .
	                               	$intCompanyUserId . ',
	                               	NOW(),' .
	                               	$intCompanyUserId . ',
	                               	NOW()
	                            FROM
	                                unit_spaces us
	                                JOIN property_units_tweaked put ON put.cid = us.cid AND put.current_property_unit_id = us.property_unit_id
	                                JOIN property_units_tweaked put_main ON put_main.cid = put.cid AND put.unit_number = put_main.unit_number AND put.property_id = put_main.property_id AND put.property_floorplan_id = put_main.property_floorplan_id
	                                AND ( put.property_building_id = put_main.property_building_id OR ( put.property_building_id IS NULL AND put_main.property_building_id IS NULL ) ) AND put_main.rank = 1
	                                JOIN properties p ON p.cid = us.cid AND p.id = us.property_id
	                                LEFT JOIN property_buildings pb ON put.cid = pb.cid AND put.property_building_id = pb.id AND pb.deleted_on IS NULL
	                            WHERE
	                                us.cid = ' . ( int ) $intCid . '
	                                ' . $strCheckDeletedUnitSpacesSql . '
	                                AND us.property_id = ' . ( int ) $intPropertyId . '
	                                AND put.space_number IS NOT NULL
	                            ) t';

    	if( false == $objDataset->execute( $strSql ) || 0 < $objDataset->getRecordCount() ) {
    		$objDataset->cleanup();
    		return false;
    	}

    	$objDataset->cleanup();
    	return true;
    }

	public static function deletePropertyUnitUpdatesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

    	if( false == is_numeric( $intPropertyId ) || false == is_numeric( $intCid ) ) return false;

    	$objDataset = $objDatabase->createDataset();

    	$strSql = ' DELETE
    				FROM
						public.property_unit_updates
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND CID = ' . ( int ) $intCid;

    	if( false == $objDataset->execute( $strSql ) || 0 < $objDataset->getRecordCount() ) {
    		$objDataset->cleanup();
    		return false;
    	}

    	$objDataset->cleanup();
    	return true;
    }

}
?>