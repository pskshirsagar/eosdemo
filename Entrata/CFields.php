<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFields
 * Do not add any new functions to this class.
 */

class CFields extends CBaseFields {

	public static function fetchFieldsByFieldTypeIdByFieldGroupIdsByCid( $intFieldTypeId, $intFieldGroupIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						fields
					WHERE
						field_type_id = ' . $intFieldTypeId . '
						AND field_group_id IN ( ' . implode( ',', $intFieldGroupIds ) . ' )
						AND cid = ' . $intCid;

		return self::fetchFields( $strSql, $objDatabase );

	}

}
?>