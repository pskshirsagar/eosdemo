<?php

class CMarketingMedia extends CBaseMarketingMedia {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyMediaFolderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMimeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingStorageTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingStorageTypeReference() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLegacyIsHidden() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>