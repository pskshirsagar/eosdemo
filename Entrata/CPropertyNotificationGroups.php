<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyNotificationGroups
 * Do not add any new functions to this class.
 */

class CPropertyNotificationGroups extends CBasePropertyNotificationGroups {

	public static function fetchPropertyNotificationGroupsByIds( $arrintPropertyNotificationGroupIds, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyNotificationGroupIds ) ) return NULL;

		$strSql = 'SELECT * FROM property_notification_groups WHERE id IN ( ' . implode( ',', $arrintPropertyNotificationGroupIds ) . ' ) ORDER BY id';

		return self::fetchPropertyNotificationGroups( $strSql, $objClientDatabase );
	}

}
?>