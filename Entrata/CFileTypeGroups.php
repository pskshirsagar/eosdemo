<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileTypeGroups
 * Do not add any new functions to this class.
 */

class CFileTypeGroups extends CBaseFileTypeGroups {

	public static function fetchFileTypeGroups( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CFileTypeGroup::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFileTypeGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CFileTypeGroup::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllFileTypeGroups( $objDatabase ) {
		$strSql = 'select * from file_type_groups';
		return self::fetchFileTypeGroups( $strSql, $objDatabase );
	}

}
?>