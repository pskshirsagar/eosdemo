<?php

class CSubsidyProjectPropertyBuilding extends CBaseSubsidyProjectPropertyBuilding {

	const QUALIFIED_UNIT_TYPE_UNIT_PERCENTAGE = 1;
	const QUALIFIED_UNIT_TYPE_UNIT_COUNT = 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyProjectId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyBuildingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyProjectUnitAssignmentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQualifiedUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQualifiedUnitPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>