<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGroupAssetLocations
 * Do not add any new functions to this class.
 */

class CPropertyGroupAssetLocations extends CBasePropertyGroupAssetLocations {

	public static function fetchPropertyGroupAssetLocationsByAssetLocationIdsByCid( $arrintAssetLocationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintAssetLocationIds ) ) return;

		$strSql = 'SELECT
						pgal.*,
						pg.name as property_name
					FROM
						property_group_asset_locations pgal
						JOIN property_groups pg ON ( pgal.cid = pg.cid AND pgal.property_group_id = pg.id )
					WHERE
						pgal.cid = ' . ( int ) $intCid . '
						AND pgal.asset_location_id IN ( ' . implode( ',', array_filter( $arrintAssetLocationIds ) ) . ' )
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		return self::fetchPropertyGroupAssetLocations( $strSql, $objClientDatabase );
	}

}
?>