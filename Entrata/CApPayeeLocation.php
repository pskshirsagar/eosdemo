<?php

class CApPayeeLocation extends CBaseApPayeeLocation {

	protected $m_boolIsVerified;
	protected $m_boolReceives1099;
	protected $m_boolIsSelectAllProperties;

	protected $m_intApPayeeId;
	protected $m_intLocationId;
	protected $m_intApHeaderId;
	protected $m_intApPayeeAccountId;

	protected $m_strNameLast;
	protected $m_strFaxNumber;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strWebsiteUrl;
	protected $m_strContactType;
	protected $m_strPropertyIds;
	protected $m_strApPayeeName;
	protected $m_strApPayeeTerm;
	protected $m_strPhoneNumber;
	protected $m_strCompanyName;
	protected $m_strMobileNumber;
	protected $m_strEmailAddress;
	protected $m_strRemotePrimaryKey;
	protected $m_strApPayeeStatusType;
	protected $m_strApLegalEntityName;
	protected $m_strApPayeeAccountNumber;
	protected $m_strSecondaryNumber;

	protected $m_arrobjApPayeePropertyGroups;
	/** @var  \CApRemittance[] */
	protected $m_arrobjApRemittances;
	protected $m_arrmixApPayeeLocationEvent;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVpLocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocationName( $objClientDatabase, $arrobjApPayeeLocations ) {

		$boolIsValid	= true;

		if( false != is_null( $this->getLocationName() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'location_name', 'Location Name is required.' ) );
			return false;
		}

		$objApLegalEntity = CApLegalEntities::fetchApLegalEntityByIdByApPayeeIdByCid( $this->getApLegalEntityId(), $this->getApPayeeId(), $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrobjApPayeeLocations ) ) {

			foreach( $arrobjApPayeeLocations as $objApPayeeLocation ) {

				if( $objApPayeeLocation->getApLegalEntityId() == $this->getApLegalEntityId() && \Psi\CStringService::singleton()->strtolower( $objApPayeeLocation->getLocationName() ) == \Psi\CStringService::singleton()->strtolower( $this->getLocationName() ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'location_name', 'Location name \'' . $this->getLocationName() . '\' already exists and associated with legal entity \'' . $objApLegalEntity->getEntityName() . '\'' ) );
					break;
				}
			}
		}

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || false == $boolIsValid ) {
			return $boolIsValid;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND LOWER( location_name ) = LOWER( \'' . addslashes( $this->getLocationName() ) . '\' )
						AND ap_payee_id = ' . ( int ) $this->getApPayeeId() . '
						AND ap_legal_entity_id = ' . ( int ) $this->getApLegalEntityId() . '
						AND id != ' . ( int ) $this->getId() . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		if( 0 < CApPayeeLocations::fetchApPayeeLocationCount( $strWhere, $objClientDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'location_name', 'Location name \'' . $this->getLocationName() . '\' already exists and associated with legal entity \'' . $objApLegalEntity->getEntityName() . '\'' ) );
		}

		return $boolIsValid;
	}

	public function valVendorCode( $objClientDatabase ) {

		$boolIsValid	= true;

		if( true == valStr( $this->getVendorCode() ) ) {

			if( preg_match( '/[^a-z_\-\.0-9]/i', $this->getVendorCode() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_code', 'Invalid vendor code. Acceptable inputs are: letters, numbers, hyphen, underscore and dot. ' ) );
				return false;
			}

			if( false == valObj( $objClientDatabase, 'CDatabase' ) || false == $boolIsValid ) {
				return $boolIsValid;
			}

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( vendor_code ) = LOWER( \'' . addslashes( $this->getVendorCode() ) . '\' )
							AND id != ' . ( int ) $this->getId() . '
							AND deleted_by IS NULL
							AND deleted_on IS NULL';

			if( 0 < CApPayeeLocations::fetchApPayeeLocationCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_code', 'Vendor code already exists, please enter a valid vendor code.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valLocationDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDunsNumber( $objClientDatabase, $boolIsFromOwner = false ) {

		$intLength = strlen( trim( $this->getDunsNumber(), ' ' ) );
		$arrintActiveSubsidyContracts = [];
		$arrintExcludedSubsidyContractId = [];

		if( false == $boolIsFromOwner ) {
			$objApPayee = CApPayees::fetchApPayeeByIdByCid( $this->getApPayeeId(), $this->getCid(), $objClientDatabase );

			if( true == valObj( $objApPayee, 'CApPayee' ) && true == $objApPayee->getIsSystem() && true == valArr( $this->getApPayeePropertyGroups() ) ) {
				$arrintApPayeePropertyGroupIds = array_keys( rekeyObjects( 'PropertyGroupId', $this->getApPayeePropertyGroups() ) );

				$arrintApPayeePropertyIds = array_keys( rekeyObjects( 'PropertyId', ( array ) CPropertyGroupAssociations::fetchPropertyGroupAssociationsByPropertyGroupIdsByCid( $arrintApPayeePropertyGroupIds, $this->getCid(), $objClientDatabase ) ) );
				$arrmixPropertiesData     = \Psi\Eos\Entrata\CProperties::createService()->fetchAffordablePropertiesByCid( $this->getCid(), $objClientDatabase, $arrintApPayeePropertyIds, true, [ CSubsidyType::HUD ] );
				if( true == valArr( $arrmixPropertiesData ) ) {
					$arrintPropertySubsidyTracsVersions = \Psi\Eos\Entrata\CPropertySubsidyDetails::createService()->fetchSubsidyTracsVersionIdsByPropertyIdsByCId( array_keys( $arrmixPropertiesData ), $this->getCid(), $objClientDatabase );

					if( true == in_array( \CSubsidyTracsVersion::TRACS_2_0_3A, $arrintPropertySubsidyTracsVersions ) ) {
						$arrintExcludedSubsidyContractId = [ \CSubsidyContractType::HUD_BMIR, \CSubsidyContractType::HUD_SECTION_236 ];
					} elseif( true == in_array( \CSubsidyTracsVersion::TRACS_2_0_2D, $arrintPropertySubsidyTracsVersions ) ) {
						$arrintExcludedSubsidyContractId = CSubsidyContractType::$c_arrintNotDUNSSubsidyContractTypeIds;
					}

					$arrintActiveSubsidyContracts = \Psi\Eos\Entrata\CSubsidyContracts::createService()->fetchSubsidyContractsByPropertyIdsByCid( array_keys( $arrmixPropertiesData ), $this->getCid(), $objClientDatabase, $arrintExcludedSubsidyContractId );
				}
			}
		}

		if( true === is_null( $this->getDunsNumber() ) ) {
			if( true == valArr( $arrintActiveSubsidyContracts ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duns_number', 'DUNS Number is required for subsidized property.' ) );
			return false;
			} else {
				return true;
			}
        } elseif( 9 != $intLength ) {
			 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duns_number', 'DUNS Number must be 9 alphanumeric characters.' ) );
	        return false;
        }

		if( !preg_match( '/^[a-zA-Z0-9]{9}$/', $this->getDunsNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duns_number', 'DUNS Number must be 9 alphanumeric characters.' ) );
			return false;
		}

		return true;
	}

	public function valAccountNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPrimary() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeePropertyGroups() {
		$boolIsValid = true;

		if( false == valArr( $this->getApPayeePropertyGroups() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property', 'At least one property group is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLocationStatus( $objClientDatabase ) {

		$arrobjAssociatedFeeTemplates = ( array ) \Psi\Eos\Entrata\CFeeTemplates::createService()->fetchActiveFeeTemplatesByApPayeeLocationIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjAssociatedFeeTemplates ) ) {

			$arrobjAssociatedFeeTemplates = rekeyObjects( 'Name', $arrobjAssociatedFeeTemplates );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Location \'' . $this->getLocationName() . '\' is associated with management fee template(s) \'' . implode( '\', \'', array_keys( $arrobjAssociatedFeeTemplates ) ) . '\'.' ) );
			return false;
		}

		return true;
	}

	public function valCity() {

		$boolIsValid = true;

		if( true == is_numeric( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'City can not be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {

		$boolIsValid = true;

		if( false == is_null( $this->getPostalCode() ) && CCountry::CODE_USA == $this->getCountryCode() && 0 < strlen( str_replace( '-', '', $this->getPostalCode() ) )
			&& false == preg_match( '/^([[:alnum:]]){5,5}?$/', $this->getPostalCode() ) && false == preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->getPostalCode() ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 5 or 10 characters in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.' ) );

		} elseif( false == is_null( $this->getPostalCode() ) && CCountry::CODE_CANADA == $this->getCountryCode() && 0 < strlen( str_replace( ' ', '', $this->getPostalCode() ) )
					&& false == preg_match( '/^([[:alnum:]]){3,3}?$/', $this->getPostalCode() ) && false == preg_match( '/^([[:alnum:]]){3,3} ([[:alnum:]]){3,3}?$/', $this->getPostalCode() ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 6 characters in XXX XXX format and should contain only alphanumeric characters.' ) );
		}

		return $boolIsValid;
	}

	public function valLocationAddress() {

		$boolIsValid	= true;

		if( true == valStr( $this->getStreetLine1() ) || true == valStr( $this->getCity() ) || true == valStr( $this->getStateCode() ) || true == valStr( $this->getPostalCode() ) ) {
			if( false == valStr( $this->getStreetLine1() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_line', 'Address is required.' ) );
				$boolIsValid = false;
			}

			if( false == valStr( $this->getCity() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', ' City is required.' ) );
				$boolIsValid = false;
			}

			if( false == valStr( $this->getStateCode() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State code is required.' ) );
				$boolIsValid = false;
			}

			if( false == valStr( $this->getPostalCode() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code is required.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $arrobjApPayeeLocations = [], $arrstrEditableFields = [] ) {

		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( true == valArr( $arrstrEditableFields ) ) {
					if( true == in_array( 'vendor_code', $arrstrEditableFields ) ) $boolIsValid &= $this->valVendorCode( $objClientDatabase );
					if( true == in_array( 'location_name', $arrstrEditableFields ) ) $boolIsValid &= $this->valLocationName( $objClientDatabase, $arrobjApPayeeLocations );
					if( true == in_array( 'property_ids', $arrstrEditableFields ) ) $boolIsValid &= $this->valApPayeePropertyGroups();
					if( true == in_array( 'duns_number', $arrstrEditableFields ) ) $boolIsValid &= $this->valDunsNumber( $objClientDatabase );
					break;
				} else {
					$boolIsValid &= $this->valVendorCode( $objClientDatabase );
					$boolIsValid &= $this->valLocationName( $objClientDatabase, $arrobjApPayeeLocations );
					$boolIsValid &= $this->valApPayeePropertyGroups();
					$boolIsValid &= $this->valDunsNumber( $objClientDatabase );
					break;
				}

			case 'validate_insert_owner':
			case 'validate_update_owner':
				$boolIsValid &= $this->valDunsNumber( $objClientDatabase, true );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_location_status':
				if( NULL != $this->getDisabledBy() && NULL != $this->getDisabledOn() ) {

					$boolIsValid &= $this->valLocationStatus( $objClientDatabase );
				}
				break;

			case 'validate_associated_property_groups':
				$boolIsValid &= $this->valApPayeePropertyGroups();
				break;

			case 'validate_insert_location':
			case 'validate_update_location':
				$boolIsValid &= $this->valLocationName( $objClientDatabase, $arrobjApPayeeLocations );
				break;

			case 'update_postal_code':
				$boolIsValid &= $this->valPostalCode();
				break;

			default:
				// Do nothing
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getLocationId() {
		return $this->m_intLocationId;
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function getApPayeeName() {
		return $this->m_strApPayeeName;
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function getApPayeeAccountNumber() {
		return $this->m_strApPayeeAccountNumber;
	}

	public function getApPayeeStatusType() {
		return $this->m_strApPayeeStatusType;
	}

	public function getContactType() {
		return $this->m_strContactType;
	}

	public function getPropertyIds() {
		return $this->m_strPropertyIds;
	}

	public function getApPayeeTerm() {
		return $this->m_strApPayeeTerm;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getWebsiteUrl() {
		return $this->m_strWebsiteUrl;
	}

	public function getApPayeePropertyGroups() {
		return $this->m_arrobjApPayeePropertyGroups;
	}

	public function getIsSelectAllProperties() {
		return $this->m_boolIsSelectAllProperties;
	}

	public function getIsVerified() {
		return $this->m_boolIsVerified;
	}

	public function getReceives1099() {
		return $this->m_boolReceives1099;
	}

	public function getApLegalEntityName() {
		return $this->m_strApLegalEntityName;
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function getApRemittances() {
		return $this->m_arrobjApRemittances;
	}

	public function getApPayeeLocationEvent() {
		return $this->m_arrmixApPayeeLocationEvent;
	}

	/**
	 * Set Functions
	 */

	public function setLocationId( $intLocationId ) {
		$this->m_intLocationId = $intLocationId;
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->m_strSecondaryNumber = $strSecondaryNumber;
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = $intApPayeeId;
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->m_strRemotePrimaryKey = $strRemotePrimaryKey;
	}

	public function setApPayeeName( $strApPayeeName ) {
		$this->m_strApPayeeName = $strApPayeeName;
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->m_intApPayeeAccountId = $intApPayeeAccountId;
	}

	public function setApPayeeAccountNumber( $strApPayeeAccountNumber ) {
		$this->m_strApPayeeAccountNumber = $strApPayeeAccountNumber;
	}

	public function setApPayeeStatusType( $strApPayeeStatusType ) {
		$this->m_strApPayeeStatusType = $strApPayeeStatusType;
	}

	public function setContactType( $strContactType ) {
		$this->m_strContactType = $strContactType;
	}

	public function setPropertyIds( $strPropertyIds ) {
		$this->m_strPropertyIds = $strPropertyIds;
	}

	public function setApPayeeTerm( $strApPayeeTerm ) {
		$this->m_strApPayeeTerm = $strApPayeeTerm;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->m_strNameMiddle = $strNameMiddle;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->m_strMobileNumber = $strMobileNumber;
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->m_strFaxNumber = $strFaxNumber;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setWebsiteUrl( $strWebsiteUrl ) {
		$this->m_strWebsiteUrl = $strWebsiteUrl;
	}

	public function setApPayeePropertyGroups( $arrobjApPayeePropertyGroups ) {
		$this->m_arrobjApPayeePropertyGroups = ( array ) $arrobjApPayeePropertyGroups;
	}

	public function setIsSelectAllProperties( $boolIsSelectAllProperties ) {
		$this->m_boolIsSelectAllProperties = $boolIsSelectAllProperties;
	}

	public function setIsVerified( $boolIsVerified ) {
		$this->m_boolIsVerified = CStrings::strToBool( $boolIsVerified );
	}

	public function setReceives1099( $boolReceives1099 ) {
		$this->m_boolReceives1099 = CStrings::strToBool( $boolReceives1099 );
	}

	public function setApLegalEntityName( $strApLegalEntityName ) {
		$this->m_strApLegalEntityName = $strApLegalEntityName;
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->m_intApHeaderId = $intApHeaderId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['location_id'] ) )				$this->setLocationId( $arrmixValues['location_id'] );
		if( true == isset( $arrmixValues['ap_payee_id'] ) )				$this->setApPayeeId( $arrmixValues['ap_payee_id'] );
		if( true == isset( $arrmixValues['remote_primary_key'] ) )		$this->setRemotePrimaryKey( $arrmixValues['remote_primary_key'] );
		if( true == isset( $arrmixValues['ap_payee_name'] ) )			$this->setApPayeeName( $arrmixValues['ap_payee_name'] );
		if( true == isset( $arrmixValues['ap_payee_status_type'] ) )	$this->setApPayeeStatusType( $arrmixValues['ap_payee_status_type'] );
		if( true == isset( $arrmixValues['contact_type'] ) )			$this->setContactType( $arrmixValues['contact_type'] );
		if( true == isset( $arrmixValues['ap_payee_term'] ) )			$this->setApPayeeTerm( $arrmixValues['ap_payee_term'] );
		if( true == isset( $arrmixValues['company_name'] ) )			$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['name_first'] ) )				$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_middle'] ) )				$this->setNameFirst( $arrmixValues['name_middle'] );
		if( true == isset( $arrmixValues['name_last'] ) )				$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['postal_code'] ) )				$this->setPostalCode( $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['property_ids'] ) )			$this->setPropertyIds( $arrmixValues['property_ids'] );
		if( true == isset( $arrmixValues['phone_number'] ) )			$this->setPhoneNumber( $arrmixValues['phone_number'] );
		if( true == isset( $arrmixValues['mobile_number'] ) )			$this->setMobileNumber( $arrmixValues['mobile_number'] );
		if( true == isset( $arrmixValues['fax_number'] ) )				$this->setFaxNumber( $arrmixValues['fax_number'] );
		if( true == isset( $arrmixValues['email_address'] ) )			$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['website_url'] ) )				$this->setWebsiteUrl( $arrmixValues['website_url'] );
		if( true == isset( $arrmixValues['receives_1099'] ) )			$this->setReceives1099( $arrmixValues['receives_1099'] );
		if( true == isset( $arrmixValues['ap_legal_entity_name'] ) )	$this->setApLegalEntityName( $arrmixValues['ap_legal_entity_name'] );
		if( true == isset( $arrmixValues['ap_payee_account_id'] ) )		$this->setApPayeeAccountId( $arrmixValues['ap_payee_account_id'] );
		if( true == isset( $arrmixValues['ap_payee_account_number'] ) )	$this->setApPayeeAccountNumber( $arrmixValues['ap_payee_account_number'] );
		if( true == isset( $arrmixValues['secondary_number'] ) )		$this->setSecondaryNumber( $arrmixValues['secondary_number'] );
		if( true == isset( $arrmixValues['ap_header_id'] ) )			$this->setApHeaderId( $arrmixValues['ap_header_id'] );
	}

	/**
	 * Other function
	 */

	public function addApRemittance( $objApRemittance ) {
		$this->m_arrobjApRemittances[] = $objApRemittance;
	}

	/**
	 * Create Functions
	 */

	public function createApPayeePropertyGroup() {

		$objApPayeePropertyGroup = new CApPayeePropertyGroup();
		$objApPayeePropertyGroup->setCid( $this->getCid() );
		$objApPayeePropertyGroup->setApPayeeId( $this->getApPayeeId() );

		return $objApPayeePropertyGroup;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function addApPayeeLocationEvent( $arrmixEvent ) {
		$this->m_arrmixApPayeeLocationEvent = $arrmixEvent;
	}

}
?>