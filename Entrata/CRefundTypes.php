<?php

class CRefundTypes extends CBaseRefundTypes {

	public static function fetchRefundTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRefundType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRefundType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRefundType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>