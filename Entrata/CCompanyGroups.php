<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyGroups
 * Do not add any new functions to this class.
 */

class CCompanyGroups extends CBaseCompanyGroups {

	public static function fetchCompanyGroupByGroupNameByCid( $strGroupName, $intCid, $objDatabase ) {
		return self::fetchCompanyGroups( sprintf( 'SELECT * FROM %s WHERE cid = %d and name = \'%s\'', 'company_groups', ( int ) $intCid, $strGroupName ), $objDatabase );
	}

	public static function fetchCompanyGroupByGroupNameByIntegrationDatabaseIdByCid( $strGroupName, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		return self::fetchCompanyGroups( sprintf( 'SELECT * FROM %s WHERE integration_database_id = %d AND cid = %d AND name = \'%s\'', 'company_groups', ( int ) $intIntegrationDatabaseId, ( int ) $intCid, $strGroupName ), $objDatabase );
	}

	public static function fetchCompanyGroupCountByGroupNameByNonCompetingCompanyUserIdByCid( $strGroupName, $intGroupId, $intCid, $objDatabase ) {

		$strWhereSql = ' WHERE cid = ' . ( int ) $intCid . ' and LOWER(name) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strGroupName ) ) . '\'';
		$strWhereSql .= ( true == is_numeric( $intGroupId ) ) ? ' AND id <> ' . ( int ) $intGroupId : '';

		return CCompanyGroups::fetchCompanyGroupCount( $strWhereSql, $objDatabase );
	}

	public static function fetchSortedCompanyGroupsByCid( $intCid, $objDatabase, $boolNonActiveDirectoryGroup = false ) {

		$strWhereSql = '';

		if( true == $boolNonActiveDirectoryGroup ) {
			$strWhereSql = ' AND is_active_directory_group = 0 ';
		}

		$strSql = 'SELECT * FROM company_groups WHERE cid = ' . ( int ) $intCid . $strWhereSql . ' ORDER BY LOWER(name)';

	 	return self::fetchCompanyGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupsByCompanyUserCountByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						MAX(cg.id) as id,
						MAX(util_get_translated ( \'name\', cg.name, cg.details )) as name,
						MAX(util_get_translated ( \'description\', cg.description, cg.details )) as description,
						COUNT(cug.id) as associated_users_count,
						MAX(cg.is_active_directory_group) AS is_active_directory_group
					FROM
						company_groups cg
						LEFT JOIN company_user_groups cug ON ( cg.id = cug.company_group_id AND cg.cid = cug.cid )
						AND cug.cid = ' . ( int ) $intCid . '
					WHERE
						cg.cid = ' . ( int ) $intCid . '
						GROUP BY cg.id
						ORDER BY MAX( LOWER( ' . $objDatabase->getCollateSort( 'cg.name', true ) . ' ) )';

		return self::fetchCompanyGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupsByCompanyUserCountByCompanyGroupPermissionByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						MAX(cg.id) as id,
						MAX(cg.name) as name,
						MAX(cg.description) as description,
						COUNT(cug.id) as associated_users_count,
						MAX( cgp.is_allowed ) as company_group_permission
					FROM
						company_groups cg
						LEFT JOIN company_user_groups cug ON ( cg.id = cug.company_group_id AND cg.cid = cug.cid )
						LEFT JOIN company_group_permissions cgp ON ( cg.id = cgp.company_group_id AND cg.cid = cgp.cid AND cgp.module_id = 22 )
						AND cug.cid = ' . ( int ) $intCid . '
						AND cgp.is_allowed = 1
					WHERE
						cg.cid = ' . ( int ) $intCid . '
						GROUP BY cg.id
						ORDER BY MAX( LOWER( cg.name ) )';

		return self::fetchCompanyGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupsUserCountByCompanyGroupIdsByCid( $arrintComanyGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintComanyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						cg.id,
						COUNT(cug.id) AS associated_users_count,
						CASE
							WHEN COUNT( cug.id ) = 1
							THEN SUM( cug.company_user_id )
						END AS company_user_id
					FROM
						company_groups cg
						LEFT JOIN company_user_groups cug ON ( cg.id = cug.company_group_id AND cg.cid = cug.cid )
						JOIN company_users cu ON ( cu.id = cug.company_user_id AND ( cu.cid = cug.cid AND cu.default_company_user_id IS NULL ) AND cu.is_disabled = 0 AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						JOIN company_user_permissions cup ON ( cu.id = cup.company_user_id AND ( cu.cid = cup.cid AND cu.default_company_user_id IS NULL ) AND ( cup.is_allowed = 1 OR cup.is_inherited = 1 ) AND cup.module_id = 22 )
					WHERE
						cg.cid = ' . ( int ) $intCid . '
						AND cg.id IN(' . implode( ',', $arrintComanyGroupIds ) . ' )
					GROUP BY
						cg.id
					ORDER BY
						MAX( LOWER( cg.name ) )';

		return self::fetchCompanyGroups( $strSql, $objDatabase );
	}

	public static function fetchActiveDirectoryEnabledCompanyGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						distinct( LOWER( cg.name ) ) as company_group_name, cg.id as company_group_id, cug.company_user_id, cug.id as company_user_group_id
					FROM
						company_groups cg
						LEFT JOIN company_user_groups cug ON ( cg.cid = cug.cid	AND	cg.id = cug.company_group_id AND cug.company_user_id = ' . ( int ) $intCompanyUserId . ' )
					WHERE
						cg.cid = ' . ( int ) $intCid . '
						AND is_active_directory_group = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupByIdsByCid( $arrintComanyGroupIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
				   FROM company_groups
				   WHERE id IN(' . implode( ',', $arrintComanyGroupIds ) . ' )
					AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyGroups( $strSql, $objDatabase );
	}

	public static function fetchAllCompanyGroupsByCid( $intCid, $objDatabase ) {
		$strSql = '	SELECT id,
						util_get_translated( \'name\', name, details ) AS name
					FROM company_groups
					WHERE cid = ' . ( int ) $intCid .
					' ORDER BY name ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupsByIdsByComapnyUserIdByCid( $arrintCompanyGroupIds, $intCompanyUserId, $intCid, $objDatabase, $boolFetchGroupNameOnly = false ) {

		$strWhere = ( true == valArr( $arrintCompanyGroupIds ) ) ? ' AND cg.id IN( ' . ( implode( ',', $arrintCompanyGroupIds ) ) . ' )' : '';

		if( false == $boolFetchGroupNameOnly ) {
			$strSelect = '	DISTINCT( LOWER( util_get_translated( \'name\', cg.name, cg.details ) ) ) as company_group_name,
							cg.id as company_group_id,
							cg.remote_primary_key,
							cug.company_user_id,
							cug.id as company_user_group_id';
		} else {
			$strSelect = ' string_agg( DISTINCT( LOWER( util_get_translated( \'name\', cg.name, cg.details ) ) ), \', \') as company_group_name ';
			$strWhere .= ' AND cug.id IS NOT NULL ';
		}

		$strSql = 'SELECT
					' . $strSelect . '
					FROM
						company_groups cg
						LEFT JOIN company_user_groups cug ON ( cg.cid = cug.cid	AND	cg.id = cug.company_group_id AND cug.company_user_id = ' . ( int ) $intCompanyUserId . ' )
					WHERE
						cg.cid = ' . ( int ) $intCid . $strWhere;

		if( true == $boolFetchGroupNameOnly ) {
			return parent::fetchColumn( $strSql, 'company_group_name', $objDatabase );
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSortedCompanyGroupsByPropertyIdsByCid( $arrintActivePropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintActivePropertyIds ) ) return NULL;

			$strSql = 'SELECT DISTINCT (cg.* )
					FROM 
						company_groups cg
						LEFT JOIN company_user_groups cug ON ( cug.cid = cg.cid AND cug.company_group_id = cg.id )
						LEFT JOIN company_group_property_groups AS cgpg ON ( cgpg.company_group_id = cug.company_group_id AND cgpg.cid = cug.cid )
						LEFT JOIN property_group_associations AS pga ON ( pga.property_group_id = cgpg.property_group_id AND pga.property_id IN ( ' . implode( ',', $arrintActivePropertyIds ) . ' )  )
					WHERE
						cg.cid= ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ',', $arrintActivePropertyIds ) . ' ) ';

		return self::fetchCompanyGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserGroupsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cg.id AS group_id,
						cg.name AS group_name
					FROM
						company_user_groups cug
						JOIN company_groups cg ON ( cg.id = cug.company_group_id AND cg.cid = cug.cid )
					WHERE
						cug.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cug.cid = ' . ( int ) $intCid . ' ';

		return fetchData( $strSql, $objDatabase );
	}

}
?>