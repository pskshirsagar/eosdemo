<?php

class CTransmissionConditionType extends CBaseTransmissionConditionType {

	const VERIFY_IDENTITY 				= 1;
	const REQUIRE_CERTIFIED_FUNDS 		= 2;
	const INCREASE_DEPOSIT 				= 3;
	const INCREASE_RENT 				= 4;
	const REQUIRE_GUARANTOR 			= 5;
 	const REQUIRE_CRIMINAL_CONFIRMATION	= 6;
 	const INCREASE_DEPOSIT_2 			= 7;
 	const INCREASE_RENT_2 				= 8;
 	const CUSTOM_SCREENING_CONDITION	= 9;
    const OTHER_CHARGES                 = 10;
    const REPLACE_DEPOSIT 				= 11;
	const APRROVED_WITH_QUALIFYING_ROOMMATE = 12;
	const VERIFY_RENTAL_COLLECTION_PROOF = 13;
    const UPFRONT_RENT = 14;

    /**
     * Val Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// added default case

        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public static function transmissionConditionTypeIdToStr( $intTransmissionConditionTypeId ) {

        switch( $intTransmissionConditionTypeId ) {

    		case self::VERIFY_IDENTITY:
    			return 'Verify Identity';
    		case self::REQUIRE_CERTIFIED_FUNDS:
    			return 'Require Certified Funds';
    		case self::INCREASE_DEPOSIT:
    			return 'Increase Deposit';
    		case self::INCREASE_RENT:
    			return 'Increase Rent';
    		case self::REQUIRE_GUARANTOR:
    			return 'Require Guarantor';
    		case self::REQUIRE_CRIMINAL_CONFIRMATION:
    			return 'Require Criminal Confirmation';
    		case self::INCREASE_DEPOSIT_2:
    			return 'Increase Deposit-2';
    		case self::INCREASE_RENT_2:
    			return 'Increase Rent-2';
   			case self::CUSTOM_SCREENING_CONDITION:
    			return 'Custom Screening';
            case self::OTHER_CHARGES:
                return 'Other Charges';
	        case self::VERIFY_RENTAL_COLLECTION_PROOF:
		        return 'Verify Rental Collection Proof';
    		default:
    			trigger_error( 'Invalid transmission condition type id.', E_USER_WARNING );
    			break;
    	}
    }

}
?>