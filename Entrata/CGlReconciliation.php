<?php

class CGlReconciliation extends CBaseGlReconciliation {

	const SUMMARY				= 'summary_div';
	const DETAILS				= 'details_div';
	const SUMMARY_AND_DETAILS	= 'summary_and_details_div';
	const REPORT				= 'report_div';

	protected $m_boolIsValidForDelete;
	protected $m_boolIsReconcileUsingBankFile;

	protected $m_intSelectedBankMappingId;

	protected $m_strBankAccountName;
	protected $m_arrintBankAccountReconciliationsCounts;

	protected $m_arrmixBankMappingFile;

	public static $c_arrstrGlReconciliationPrintOptions = [
		self::SUMMARY				=> 'Summary',
		self::DETAILS				=> 'Detail',
		self::SUMMARY_AND_DETAILS	=> 'Summary and Detail',
		self::REPORT				=> 'Report'
	];

    /**
     * Get Functions
     */

    public function getBankAccountName() {
    	return $this->m_strBankAccountName;
    }

    public function getBankAccountReconciliationsCounts() {
    	return $this->m_arrintBankAccountReconciliationsCounts;
    }

    public function getIsValidForDelete() {
    	return $this->m_boolIsValidForDelete;
    }

	public function getIsReconcileUsingBankFile() {
		return $this->m_boolIsReconcileUsingBankFile;
	}

	public function getSelectedBankMappingId() {
		return $this->m_intSelectedBankMappingId;
	}

	public function getBankMappingFile() {
		return $this->m_arrmixBankMappingFile;
	}

    /**
     * Set Functions
     */

    public function setBankAccountName( $strBankAccountName ) {
    	return $this->m_strBankAccountName = $strBankAccountName;
    }

    public function setBankAccountReconciliationsCounts( $arrintBankAccountReconciliationCoounts ) {
    	return $this->m_arrintBankAccountReconciliationsCounts = $arrintBankAccountReconciliationCoounts;
    }

  	public function setBeginningDate( $strBeginningDate, $boolIsBankRecOnPostMonth = false ) {

    	if( false == $boolIsBankRecOnPostMonth ) {
			parent::setBeginningDate( $strBeginningDate );

    	} else {
    		if( true == valStr( $strBeginningDate ) ) {

    			$arrstrPostMonth = explode( '/', $strBeginningDate );

    			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
    				$strBeginningDate = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
    			}
    		}

    		$this->m_strBeginningDate = $strBeginningDate;
    	}
    }

    public function setStatementDate( $strStatementDate, $boolIsBankRecOnPostMonth = false ) {

    	if( false == $boolIsBankRecOnPostMonth ) {
    		parent::setStatementDate( $strStatementDate );

    	} else {

    		if( true == valStr( $strStatementDate ) ) {

    			$arrstrPostMonth = explode( '/', $strStatementDate );

    			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
    				$strStatementDate = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];

    				$strStatementDate = new DateTime( $strStatementDate );
    				$strStatementDate->modify( 'last day of this month' );
    				$strStatementDate = $strStatementDate->format( 'm/d/y' );
    			}
    		}

    		$this->m_strStatementDate = $strStatementDate;
		}

    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['bank_account_name'] ) ) $this->setBankAccountName( $arrmixValues['bank_account_name'] );
    	if( true == isset( $arrmixValues['is_reconcile_using_bank_file'] ) ) $this->setIsReconcileUsingBankFile( $arrmixValues['is_reconcile_using_bank_file'] );
    	if( true == isset( $arrmixValues['selected_bank_mapping_id'] ) ) $this->setSelectedBankMappingId( $arrmixValues['selected_bank_mapping_id'] );

    	return;
    }

    public function setIsValidForDelete( $boolIsValidForDelete ) {
    	return $this->m_boolIsValidForDelete = $boolIsValidForDelete;
    }

	public function setSelectedBankMappingId( $intSelectedBankMappingId ) {
		$this->m_intSelectedBankMappingId = $intSelectedBankMappingId;
	}

	public function setIsReconcileUsingBankFile( $boolIsReconcileUsingBankFile ) {
		$this->m_boolIsReconcileUsingBankFile = $boolIsReconcileUsingBankFile;
	}

	public function setBankMappingFile( $arrmixBankMappingFile ) {
		$this->m_arrmixBankMappingFile = $arrmixBankMappingFile;
	}

    /**
     * Create Functions
     */

	public function createGlReconciliationItem() {

		$objGlReconciliationItem = new CGlReconciliationItem();

		$objGlReconciliationItem->setGlReconciliationId( $this->getId() );
		$objGlReconciliationItem->setCid( $this->getCid() );

		return $objGlReconciliationItem;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setGlReconciliationId( $this->getId() );

		return $objFileAssociation;
	}

	public function createGlReconciliationLog( $strAction ) {

		$objGlReconciliationLog = new CGlReconciliationLog();

		$objGlReconciliationLog->setDefaults();
		$objGlReconciliationLog->setAction( $strAction );
		$objGlReconciliationLog->setGlReconciliationId( $this->getId() );
		$objGlReconciliationLog->setBeginningDate( $this->getBeginningDate() );
		$objGlReconciliationLog->setStatementDate( $this->getStatementDate() );
		$objGlReconciliationLog->setBeginningAmount( $this->getBeginningAmount() );
		$objGlReconciliationLog->setEndingAmount( $this->getEndingAmount() );
		$objGlReconciliationLog->setCid( $this->getCid() );

		return $objGlReconciliationLog;
	}

    /**
     * Validate Functions
     */

   public function valBankAccountId() {
        $boolIsValid = true;

        if( true == is_null( $this->getBankAccountId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_id', 'Bank Account is required.' ) );
        }

        return $boolIsValid;
   }

	public function valBeginningDate() {

		$boolIsValid = true;
		$this->setBeginningDate( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getBeginningDate() ] ) );
		if( false == $this->getIsPostMonth() && true == is_null( $this->getBeginningDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_date', 'Beginning date is required.' ) );

		} elseif( false == $this->getIsPostMonth() && false == CValidation::validateDate( $this->getBeginningDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_date', 'Beginning date of format mm/dd/yyyy is required.' ) );
		} elseif( false == $this->getIsPostMonth() && false == is_null( $this->getBeginningDate() ) ) {

			$intMonth	= ( int ) \Psi\CStringService::singleton()->substr( $this->getBeginningDate(), 0, 2 );
			$intDay		= ( int ) \Psi\CStringService::singleton()->substr( $this->getBeginningDate(), 3, 2 );
			$intYear	= ( int ) \Psi\CStringService::singleton()->substr( $this->getBeginningDate(), 6, 4 );

			if( false == checkdate( $intMonth, $intDay, $intYear ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_date', 'Beginning date is invalid.' ) );
			} elseif( false == is_null( $this->getStatementDate() ) && strtotime( $this->getBeginningDate() ) > strtotime( $this->getStatementDate() ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_date', 'Statement date should be greater than beginning date.' ) );
			}
		} elseif( true == $this->getIsPostMonth() && true == is_null( $this->getBeginningDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_date', 'Beginning post month is required.' ) );
		} elseif( true == $this->getIsPostMonth() && false == is_null( $this->getStatementDate() ) && strtotime( $this->getBeginningDate() ) > strtotime( $this->getStatementDate() ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_date', 'Statement date should be greater than beginning date.' ) );
		} elseif( true == $this->getIsPostMonth() && false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getBeginningDate() ) ) {

				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_date', 'Beginning post month of format mm/yyyy is required.' ) );
		}
		return $boolIsValid;
	}

	public function valBeginningAmount() {

		$boolIsValid = true;

		if( true == is_null( $this->getBeginningAmount() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_amount', 'Beginning Balance is required.' ) );
		} elseif( false == is_numeric( $this->getBeginningAmount() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'beginning_amount', 'Please Enter Valid Beginning Balance.' ) );
		}

		return $boolIsValid;
	}

	public function valSelectedTransactionsBeginningDate( $arrmixSelectedTransactionsReconciliation ){

		$boolIsValidFirstBankRec = true;
		$boolIsValid = true;

		if( true == valArr( $arrmixSelectedTransactionsReconciliation ) ) {

			foreach ( $arrmixSelectedTransactionsReconciliation as $arrmixSelectedTransactionsReconciliationItem ) {
				if( true == isset( $arrmixSelectedTransactionsReconciliationItem['is_selected'] ) && 'selected' == $arrmixSelectedTransactionsReconciliationItem['is_selected'] ) {
					if( true == $this->getIsPostMonth() && false == is_null( $this->getStatementDate() ) ) {
						if( true == is_null( $this->getBeginningDate() ) ) {
							if( strtotime($this->getStatementDate() ) < strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) ) {

								$boolIsValid = false;
							}
						} else {
							//when we add bank reconciliation first time with POST MONTH
							if( strtotime( $this->getStatementDate() ) < strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) ) {

								$boolIsValid = false;
							} elseif( strtotime( $this->getBeginningDate() ) > strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) ) {

								$boolIsValidFirstBankRec = false;
							} elseif( ( strtotime( $this->getBeginningDate() ) > strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) ) && ( strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) > strtotime( $this->getStatementDate() ) ) ) {

								$boolIsValidFirstBankRec = false;
							}
						}
					} elseif( strtotime( $this->getBeginningDate() ) > strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) ) {

						$boolIsValidFirstBankRec = false;
					} elseif( strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) > strtotime( $this->getStatementDate() ) ) {

						$boolIsValid = false;
					} elseif( ( strtotime( $this->getBeginningDate() ) > strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) ) && ( strtotime( $arrmixSelectedTransactionsReconciliationItem['post_date'] ) > strtotime( $this->getStatementDate() ) ) ) {

						$boolIsValidFirstBankRec = false;
					}
				}
			}
		}

		if ( $boolIsValid == false ) {

			$this->addErrorMsg(new CErrorMsg(ERROR_TYPE_VALIDATION, 'beginning_date', 'Transactions have already been selected, and post month dates cannot be changed. Please deselect transactions in order to edit the Through Post Month range.'));
			return $boolIsValid;
		} else if($boolIsValidFirstBankRec == false){

			$this->addErrorMsg(new CErrorMsg(ERROR_TYPE_VALIDATION, 'beginning_date', 'Transactions have already been selected, and post month dates cannot be changed. Please deselect transactions in order to edit the Beginning Post Month and Through Post Month range.'));
			return $boolIsValidFirstBankRec;
		} else {

			return $boolIsValid;
		}

	}

	public function valIsSystem() {

			$boolIsValid = true;
			return $boolIsValid;

	}

    public function valStatementDate() {

        $boolIsValid = true;
	    $this->setStatementDate( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getStatementDate() ] ) );
        if( false == $this->getIsPostMonth() && true == is_null( $this->getStatementDate() ) ) {

	        	$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'statement_date', 'Reconcile through date is required.' ) );
	    } elseif( false == $this->getIsPostMonth() && false == CValidation::validateDate( $this->getStatementDate() ) ) {

	        	$boolIsValid = false;
	        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'statement_date', 'Reconcile through date of format mm/dd/yyyy is required.' ) );
	    } elseif( false == is_null( $this->getStatementDate() ) ) {

	    		$intMonth	= ( int ) \Psi\CStringService::singleton()->substr( $this->getStatementDate(), 0, 2 );
	    		$intDay		= ( int ) \Psi\CStringService::singleton()->substr( $this->getStatementDate(), 3, 2 );
	    		$intYear	= ( int ) \Psi\CStringService::singleton()->substr( $this->getStatementDate(), 6, 4 );

	    		if( false == checkdate( $intMonth, $intDay, $intYear ) ) {

	    			$boolIsValid = false;
	    			if( false == $this->getIsPostMonth() ) {
	    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'statement_date', 'Reconcile through date is invalid.' ) );
	    			} else {
	    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'statement_date', 'Reconcile post month is invalid.' ) );
	    			}
	    		}
	    } elseif( true == $this->getIsPostMonth() && true == is_null( $this->getStatementDate() ) ) {

    			$boolIsValid &= false;
   				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'statement_date', 'Reconcile post month is required.' ) );
    	}
        return $boolIsValid;
    }

    public function valEndingAmount() {
        $boolIsValid = true;

        if( true == is_null( $this->getEndingAmount() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ending_amount', 'Ending Balance is required.' ) );
        } elseif( false == is_numeric( $this->getEndingAmount() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ending_amount', 'Please Enter Valid Ending Balance.' ) );
        }

        return $boolIsValid;
    }

    public function valDeletedBy() {
    	$boolIsValid = true;

    	if( true == is_numeric( $this->getDeletedBy() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_by', 'Bank reconciliation has already been deleted.' ) );
    	}

    	return $boolIsValid;
    }

    public function valDeletedOn() {
    	$boolIsValid = true;

    	if( false == is_null( $this->getDeletedOn() ) ) {
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valReconciliationTransaction( $arrintUnPausedReconciliationBankAccountIds ) {

    	$boolIsValid = true;

    	if( true == valStr( $this->getBeginningDate() ) && true == valArr( $arrintUnPausedReconciliationBankAccountIds )
    		&& true == in_array( $this->getBankAccountId(), $arrintUnPausedReconciliationBankAccountIds ) ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_id', 'This is not a first reconciliation, please try again.' ) );
    		$boolIsValid &= false;
    	}

    	return $boolIsValid;
    }

    public function valIsBankRecOnPostMonth( $objClientDatabase ) {

    	$boolIsValid = true;

    	$objBankAccount = CBankAccounts::fetchBankAccountByIdByCid( $this->getBankAccountId(), $this->getCid(), $objClientDatabase );

    	if( true == valObj( $objBankAccount, 'CBankAccount' ) && $objBankAccount->getIsBankRecOnPostMonth() != $this->getIsPostMonth() ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_bank_rec_on_post_month', 'Bank reconciliation settings have changed for selected bank account. Please cancel and restart the bank reconciliation.' ) );
    		$boolIsValid &= false;
    	}

    	return $boolIsValid;
    }

    public function valIsValidForDelete() {

    	$boolIsValid = true;

    	if( 0 == $this->getIsValidForDelete() ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, '\'' . $this->getBankAccountName() . '\' already has a later reconciliation, cannot be deleted.' ) );
    		$boolIsValid &= false;
    	}

    	return $boolIsValid;
    }

	public function valOpenReconciliation( $objClientDatabase ) {

		$boolIsValid = true;

		$arrintLastGlReconciliationIds = ( array ) CGlReconciliations::fetchLastGlReconciliationIdsByCid( $this->getCid(), $objClientDatabase );

		if( false == in_array( $this->getId(), $arrintLastGlReconciliationIds ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'You can edit last Reconciliation only.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valImportedClearedChecksFile( $arrmixFile ) {
		$boolIsValid = true;

		if( false == valArr( $arrmixFile ) || UPLOAD_ERR_NO_FILE == $arrmixFile['error'] ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_file', 'No file was uploaded. Select a file to upload.' ) );
			return $boolIsValid;
		}

		$strExtension				= pathinfo( $arrmixFile['name'], PATHINFO_EXTENSION );
		$strUploadMaxFilesize		= ini_get( 'upload_max_filesize' );
		$strUploadTempDir			= PATH_NON_BACKUP_MOUNTS_GLOBAL_TMP;
		$intMaxUpload				= ( int ) str_replace( 'M', '', $strUploadMaxFilesize ) * 1024 * 1024;
		$arrstrRequiredHeaders		= [ 'Check Date', 'Check Number', 'Amount' ];
		$arrmixAllowedExtensions	= [ 'csv' => [ 'mimes' => [ 'application/vnd.ms-excel' ], 'exts' => [ 'csv' ] ] ];

		if( false != valStr( $strExtension ) && false == array_key_exists( $strExtension, $arrmixAllowedExtensions ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type', 'Cannot upload other than CSV file.' ) );
			return $boolIsValid;
		}

		if( true == valArr( $arrmixFile ) && ( $intMaxUpload < $arrmixFile['size'] || UPLOAD_ERR_OK != $arrmixFile['error'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_size', 'Files larger than ' . $strUploadMaxFilesize . 'B cannot be uploaded.' ) );
			return $boolIsValid;
		}

		$strTempUri	= getUniqueString();

		if( true == valArr( $arrmixFile ) && ( false == move_uploaded_file( $arrmixFile['tmp_name'], $strUploadTempDir . $strTempUri ) || UPLOAD_ERR_NO_TMP_DIR == $arrmixFile['error'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_upload', $arrmixFile['name'] . ' could not be moved to the temporary directory.' ) );
			return $boolIsValid;
		}

		// Feed data from CSV file.
		$arrmixClearedChecksData = ( array ) $this->parseCsvFileImport( $arrstrRequiredHeaders, $strUploadTempDir . $strTempUri );

		// Check if file is empty
		if( false == valArr( $arrmixClearedChecksData ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'empty_file', 'File is missing data.' ) );
			return $boolIsValid;
		}

		foreach( $arrmixClearedChecksData as $intRefKey => $arrmixClearedCheckData ) {

			$strClearedCheckDate	= NULL;
			$strClearedCheckNumber	= NULL;
			$fltClearedCheckAmount	= NULL;

			if( true == isset( $arrmixClearedCheckData['check_date'] ) ) {
				$strClearedCheckDate = trim( $arrmixClearedCheckData['check_date'] );
			}

			if( true == isset( $arrmixClearedCheckData['check_number'] ) ) {
				$strClearedCheckNumber = trim( $arrmixClearedCheckData['check_number'] );
			}

			if( true == isset( $arrmixClearedCheckData['amount'] ) ) {
				$fltClearedCheckAmount = trim( $arrmixClearedCheckData['amount'] );
			}

			// Check number validation
			if( false == valStr( $strClearedCheckNumber ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_number', 'Check number is required Row #' . ( $intRefKey + 2 ) . ' Column #2.' ) );
			}

			// Amount validation
			if( false == valStr( $fltClearedCheckAmount ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount is required Row #' . ( $intRefKey + 2 ) . ' Column #3.' ) );
			} elseif( 0 == ( float ) $fltClearedCheckAmount ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount is invalid Row #' . ( $intRefKey + 2 ) . ' Column #3.' ) );
			} elseif( 12 < strlen( $fltClearedCheckAmount ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount can not be greater than 12 digits Row #' . ( $intRefKey + 2 ) . ' Column #3.' ) );
			}
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		return $arrmixClearedChecksData;
	}

	public function parseCsvFileImport( $arrstrRequiredHeaders, $strFileUri ) {

		$arrmixRawData			= [];
		$boolHeaderRowSelected	= false;

		if( false == file_exists( $strFileUri ) ) {
			$this->displayMessage( 'Application Error: Failed to load file.', NULL, NULL, NULL, NULL, 1 );
			exit;
		}

		if( false !== ( $resHandle = CFileIo::fileOpen( $strFileUri, 'r' ) ) ) {

			while( false !== ( $arrmixData = fgetcsv( $resHandle, 0, ',' ) ) ) {

				if( \Psi\Libraries\UtilFunctions\count( $arrmixData ) != \Psi\Libraries\UtilFunctions\count( $arrstrRequiredHeaders ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'headers', 'Column headers not present or do not match required headers.' ) );
					return;
				}

				if( false == $boolHeaderRowSelected ) {
					$boolHeaderRowSelected	= ftell( $resHandle );
					$arrstrCsvHeader		= array_map( 'str_replace', array_fill( 0, \Psi\Libraries\UtilFunctions\count( $arrmixData ), ' ' ), array_fill( 0, \Psi\Libraries\UtilFunctions\count( $arrmixData ), '_' ), array_map( 'strtolower', array_map( 'trim', $arrmixData ) ) );
					continue;
				}

				if( \Psi\Libraries\UtilFunctions\count( $arrstrCsvHeader ) == \Psi\Libraries\UtilFunctions\count( $arrmixData ) ) {
					$arrmixRawData[] = array_combine( $arrstrCsvHeader, $arrmixData );
				}
			}

			fclose( $resHandle );
		}

		return $arrmixRawData;
	}

	public function valBankReconciliation( $intGlReconciliationStatusTypeId, $boolIsEditReopened, $arrobjBankAccounts, $objClientDatabase = NULL ) {

		$boolIsValid = true;

		$objGlReconciliation = CGlReconciliations::fetchLastGlReconciliationsByBankAccountIdByGlReconciliationStatusTypeIdByCid( $this->getBankAccountId(), CGlReconciliationStatusType::PAUSED, $this->getCid(), $objClientDatabase );

		if( true == valObj( $objGlReconciliation, 'CGlReconciliation' ) && true == is_numeric( $intGlReconciliationStatusTypeId ) && true == is_numeric( $objGlReconciliation->getGlReconciliationStatusTypeId() ) ) {
			$this->setGlReconciliationStatusTypeId( $objGlReconciliation->getGlReconciliationStatusTypeId() );
		}

		$objMaxGlReconciliation = CGlReconciliations::fetchMaxGlReconciliationNotByIdByBankAccountIdByCid( $this->getId(), $this->getBankAccountId(), $this->getCid(), $objClientDatabase );

		if( true == valObj( $objMaxGlReconciliation, 'CGlReconciliation' )
			&& strtotime( $this->getStatementDate() ) <= strtotime( $objMaxGlReconciliation->getStatementDate() )
			&& true == array_key_exists( $this->getBankAccountId(), $arrobjBankAccounts )
			&& false == $boolIsEditReopened ) {

			if( false == $this->getIsPostMonth() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'statement_date', 'Cannot create reconciliation for bank account \'' . $arrobjBankAccounts[$this->getBankAccountId()]->getAccountName() . '\' of date ' . date( 'm/d/Y', strtotime( $this->getStatementDate() ) ) . '. Statement through date must be greater than ' . $objMaxGlReconciliation->getStatementDate() . '.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'statement_date', 'Cannot create reconciliation for bank account \'' . $arrobjBankAccounts[$this->getBankAccountId()]->getAccountName() . '\' of post month ' . date( 'm/Y', strtotime( $this->getStatementDate() ) ) . '. Statement through post month must be greater than ' . date( 'm/Y', strtotime( $objMaxGlReconciliation->getStatementDate() ) ) . '.' ) );
			}

			$boolIsValid &= false;
		}

		if( true == valObj( $objGlReconciliation, 'CGlReconciliation' )
			&& $objGlReconciliation->getId() != $this->getId()
			&& true == array_key_exists( $this->getBankAccountId(), $arrobjBankAccounts )
			&& false == $boolIsEditReopened ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The bank account "' . $arrobjBankAccounts[$this->getBankAccountId()]->getAccountName() . '" has an open bank reconciliation. Please complete the open bank reconciliation first.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valBankAccountForProperties( $intBankAccountId, $arrobjPropertyBankAccounts, $arrobjProperties ) {

		$boolIsValid = true;

		if( false == valId( $intBankAccountId ) && false == valArr( $arrobjPropertyBankAccounts ) ) {
			return false;
		}

		$strBankAccountName = '';

		$arrstrPropertiesName = [];
		$arrintPropertyBankAccountByIds = [];

		foreach( $arrobjPropertyBankAccounts as $arrobjPropertyBankAccount ) {
			$arrintPropertyBankAccountByIds[$arrobjPropertyBankAccount['bank_account_id']][$arrobjPropertyBankAccount['property_id']][] = $arrobjPropertyBankAccount['property_id'];
			$arrintPropertyBankAccountByIds[$arrobjPropertyBankAccount['bank_account_id']][$arrobjPropertyBankAccount['property_id']][] = $arrobjPropertyBankAccount['property_name'];
			$arrintPropertyBankAccountByIds[$arrobjPropertyBankAccount['bank_account_id']][$arrobjPropertyBankAccount['property_id']][] = $arrobjPropertyBankAccount['bank_account_name'];
		}

		if( true == valArr( array_diff( array_keys( $arrintPropertyBankAccountByIds[$intBankAccountId] ), $arrobjProperties ) ) ) {
			$arrmixUnassignedProperties = array_diff( array_keys( $arrintPropertyBankAccountByIds[$intBankAccountId] ), $arrobjProperties );
			foreach( $arrmixUnassignedProperties as $arrmixUnassignedProperty ) {
				$arrstrPropertiesName[] = $arrintPropertyBankAccountByIds[$intBankAccountId][$arrmixUnassignedProperty][1];
				$strBankAccountName = $arrintPropertyBankAccountByIds[$intBankAccountId][$arrmixUnassignedProperty][2];
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account', 'You are unable to select this bank account "' . $strBankAccountName . '" for reconciliation because you aren\'t assigned to property(ies) "' . implode( ', ', $arrstrPropertiesName ) . '" associated with the bank account.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valIsSetCreditCardVendor( $arrobjBankAccounts ) {
		$boolIsValid = true;
		if( true == isset( $arrobjBankAccounts[$this->getBankAccountId()] ) && false == valId( $arrobjBankAccounts[$this->getBankAccountId()]->getApPayeeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'In order to create Invoice please add the Credit Card Vendor at Setup > Company > Bank Account > [Credit Card Bank Account].' ) ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valUnclaimedPropertyPayments( $arrobjUnclaimedApHeaders ) {
		$boolIsValid = true;
		$arrintPaymentNumbers = [];
		if( true == valStr( $this->getStatementDate() ) ) {
			$strStatementDate = $this->getStatementDate();

			if( 1 == $this->getIsPostMonth() ) {
				$arrstrStatementDate = explode( '/', $this->getStatementDate() );
				$strStatementDate = $arrstrStatementDate[0] . '/01/' . $arrstrStatementDate[2];
			}

			foreach( $arrobjUnclaimedApHeaders as $objUnclaimedApHeader ) {
				$strUnclaimedPostMonthOrDate = $objUnclaimedApHeader->getPostDate();
				if( 1 == $this->getIsPostMonth() ) {
					$arrintUnclaimedHeaderPostDate = explode( '/', $objUnclaimedApHeader->getPostMonth() );
					$strUnclaimedPostMonthOrDate = $arrintUnclaimedHeaderPostDate[0] . '/01/' . $arrintUnclaimedHeaderPostDate[2];
				}

				if( strtotime( $strUnclaimedPostMonthOrDate ) <= strtotime( $strStatementDate ) ) {
					$boolIsValid = false;
					array_push( $arrintPaymentNumbers, $objUnclaimedApHeader->getPaymentNumber() );
				}
			}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Payment(s) ' . implode( ',', array_unique( $arrintPaymentNumbers ) ) . ' have been moved to unclaimed property.' ) );
		}
		return $boolIsValid;
	}

	public function valBankMappingFile() {
		$boolIsValid = true;
		$strExtension			= pathinfo( $this->getBankMappingFile(), PATHINFO_EXTENSION );

		if( true == $this->getIsReconcileUsingBankFile() && false == valStr( $this->getBankMappingFile() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_mapping_file', 'Bank mapping file is required.' ) );
		}

		if( true == valStr( $this->getBankMappingFile() ) && false == in_array( $strExtension, [ 'xls', 'xlsx', 'csv' ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mapping_file_extension', 'Only XLS or CSV format is accepted.' ) );
		}

		return $boolIsValid;
	}

	public function valOpenPausedBankRefresh( $boolIsCreditCardReconciliation ) {
		$boolIsValid = true;

		$strValidationPrefix = ( true == $boolIsCreditCardReconciliation ) ? 'Credit Card' : 'Bank';

		if( true == valId( $this->getDeletedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_refresh', $strValidationPrefix . ' Reconciliation has been deleted.' ) );
		}

		return $boolIsValid;
	}

	public function valReconciliationGlDetails( $arrobjSetGlReconciliationIdInGlDetails, $arrintGlAccountIds, $objBankAccount, $objClientDatabase ) {
		$boolIsValid = true;
		$intGlAccountId = NULL;
		$arrstrGlHeaderNumbers = [];
		$arrintGlHeaderIds = [];

		foreach( $arrobjSetGlReconciliationIdInGlDetails as $objGlDetail ) {

			if( true == is_numeric( $objGlDetail->getCashGlAccountId() ) && 1 == $objBankAccount->getIsCashBasis() ) {
				$intGlAccountId 	= $objGlDetail->getCashGlAccountId();
			} elseif( true == is_numeric( $objGlDetail->getAccrualGlAccountId() ) && 0 == $objBankAccount->getIsCashBasis() ) {
				$intGlAccountId 	= $objGlDetail->getAccrualGlAccountId();
			}

			if( false == in_array( $intGlAccountId, $arrintGlAccountIds ) ) {
				$arrintGlHeaderIds[$objGlDetail->getGlHeaderId()] = $objGlDetail->getGlHeaderId();
			}
		}

		if( true == valArr( $arrintGlHeaderIds ) ) {

			$arrobjGlHeaders = ( array ) CGlHeaders::fetchGlHeadersByIdsByCid( $arrintGlHeaderIds, $objBankAccount->getCid(), $objClientDatabase );

			if( true == valArr( $arrobjGlHeaders ) ) {
				$arrstrGlHeaderNumbers = array_filter( array_keys( rekeyObjects( 'HeaderNumber', $arrobjGlHeaders ) ) );

				if( true == valArr( $arrstrGlHeaderNumbers ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_insert', ' Journal entry #' . implode( ', #', $arrstrGlHeaderNumbers ) . ' has been edited and is no longer associated with this reconciliation.' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixParameters = [] ) {

		$boolIsValid = true;
		$boolIsEditReopened							= ( true == isset( $arrmixParameters['gl_reconciliation_status_type_id'] ) ) ? $arrmixParameters['gl_reconciliation_status_type_id'] : NULL;
		$boolIsCreditCardReconciliation				= ( true == isset( $arrmixParameters['is_credit_card_reconciliation'] ) ) ? $arrmixParameters['is_credit_card_reconciliation'] : NULL;
		$intBankAccountId							= ( true == isset( $arrmixParameters['bank_account_id'] ) ) ? $arrmixParameters['bank_account_id'] : NULL;
		$intGlReconciliationStatusTypeId			= ( true == isset( $arrmixParameters['gl_reconciliation_status_type_id'] ) ) ? $arrmixParameters['gl_reconciliation_status_type_id'] : NULL;
		$objClientDatabase							= ( true == isset( $arrmixParameters['clientDatabase'] ) ) ? $arrmixParameters['clientDatabase'] : NULL;
		$arrobjProperties							= ( true == isset( $arrmixParameters['properties'] ) ) ? $arrmixParameters['properties'] : NULL;
		$arrobjBankAccounts							= ( true == isset( $arrmixParameters['bank_accounts'] ) ) ? $arrmixParameters['bank_accounts'] : NULL;
		$arrobjPropertyBankAccounts					= ( true == isset( $arrmixParameters['property_bank_accounts'] ) ) ? $arrmixParameters['property_bank_accounts'] : NULL;
		$arrobjUnclaimedApHeaders 					= ( true == isset( $arrmixParameters['unclaimed_property_ap_headers'] ) ) ? $arrmixParameters['unclaimed_property_ap_headers'] : [];
		$arrintUnPausedReconciliationBankAccountIds	= ( true == isset( $arrmixParameters['unpaused_reconciliation_bank_account_ids'] ) ) ? $arrmixParameters['unpaused_reconciliation_bank_account_ids'] : [];
		$arrobjSetGlReconciliationIdInGlDetails		= ( true == isset( $arrmixParameters['set_gl_reconciliation_id_in_gl_details'] ) ) ? $arrmixParameters['set_gl_reconciliation_id_in_gl_details'] : [];
		$arrintGlAccountIds							= ( true == isset( $arrmixParameters['gl_account_ids'] ) ) ? $arrmixParameters['gl_account_ids'] : [];
		$objBankAccount								= ( true == isset( $arrmixParameters['bank_account'] ) ) ? $arrmixParameters['bank_account'] : [];
        $arrmixSelectedTransactionsReconciliation		= ( true == isset( $arrmixParameters['arr_selected_transactions_reconciliation'] ) ) ? $arrmixParameters['arr_selected_transactions_reconciliation'] : [];

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeletedBy();
				$boolIsValid &= $this->valDeletedOn();
				$boolIsValid &= $this->valIsValidForDelete();
				break;

			case 'first_bank_reconciliation_insert':
				$boolIsValid &= $this->valBeginningDate();
				$boolIsValid &= $this->valBeginningAmount();

			case 'bank_reconciliation_insert':
				$boolIsValid &= $this->valIsBankRecOnPostMonth( $objClientDatabase );
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valStatementDate();
				$boolIsValid &= $this->valEndingAmount();
				$boolIsValid &= $this->valReconciliationTransaction( $arrintUnPausedReconciliationBankAccountIds );
                $boolIsValid &= $this->valSelectedTransactionsBeginningDate($arrmixSelectedTransactionsReconciliation);
				break;

			case 'validate_unclaimed_property_payments':
				$boolIsValid &= $this->valUnclaimedPropertyPayments( $arrobjUnclaimedApHeaders );
				break;

			case 'bank_reconciliation_edit':
				$boolIsValid &= $this->valOpenReconciliation( $objClientDatabase );
				break;

			case 'validate_bank_reconciliation':
				$boolIsValid &= $this->valBankReconciliation( $intGlReconciliationStatusTypeId, $boolIsEditReopened, $arrobjBankAccounts, $objClientDatabase );
				break;

			case 'validate_bank_account':
				$boolIsValid &= $this->valBankAccountForProperties( $intBankAccountId, $arrobjPropertyBankAccounts, $arrobjProperties );
				break;

			case 'validate_credit_card_vendor':
				$boolIsValid &= $this->valIsSetCreditCardVendor( $arrobjBankAccounts );
				break;

			case 'validate_bank_statement_mapping':
				$boolIsValid &= $this->valBankMappingFile();
				break;

			case 'validate_open_paused_bank_refresh':
				$boolIsValid &= $this->valOpenPausedBankRefresh( $boolIsCreditCardReconciliation );
				break;

			case 'validate_gl_details':
				$boolIsValid &= $this->valReconciliationGlDetails( $arrobjSetGlReconciliationIdInGlDetails, $arrintGlAccountIds, $objBankAccount, $objClientDatabase );
				break;

			case 'bank_validate_edit_post_date':
				$boolIsValid &= $this->valSelectedTransactionsBeginningDate($arrmixSelectedTransactionsReconciliation);
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
    }

    public function fetchGlDetails( $objDatabase ) {
    	return CGlDetails::fetchGlDetailsByGlReconciliationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

    public function fetchFiles( $objDatabase ) {
    	return CFiles::fetchFilesByGlReconciliationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

	public function fetchOpenGlReconciliation( $objDatabase ) {
		return CGlReconciliations::fetchOpenGlReconciliationByBankAcccountIdByCid( $this->getBankAccountId(), $this->getCid(), $objDatabase );
	}

	public function fetchGlReconciliationExceptions( $objDatabase ) {
		return CGlReconciliationExceptions::fetchGlReconciliationExceptionsByGlReconciliationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

    // We should always be soft deleting gl_reconciliations

    public function delete( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = false ) {

	    $this->setDeletedBy( $intCurrentUserId );
	    $this->setDeletedOn( 'NOW()' );

	    return $this->update( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly );
    }

}

?>