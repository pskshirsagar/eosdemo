<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyCategories
 * Do not add any new functions to this class.
 */

class CPropertyCategories extends CBasePropertyCategories {

	public static function fetchPropertyCategoryByCompanyCategoryIdByPropertyIdByCid( $intCompanyCategoryId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM property_categories WHERE company_category_id = ' . ( int ) $intCompanyCategoryId . ' AND property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchPropertyCategory( $strSql, $objDatabase );
	}

}
?>