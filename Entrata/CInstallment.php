<?php

class CInstallment extends CBaseInstallment {

	const FIRST_INSTALLMENT = 1;

    /**
     * Get Functions
     *
     */

    public function getChargeStartDate( $boolProrateCharges = true, $strScheduledChargeStartDate = NULL, $boolHonorChargeStartDate = false ) {
    	// if charge start date is already set then it should not go in below block,this is written for conventional installment plans
    	if( false == valStr( $this->m_strChargeStartDate ) && true == valStr( $strScheduledChargeStartDate ) ) {
		    if( SELF::FIRST_INSTALLMENT == min( $this->getChargeMonths() ) ) {
			    $this->m_strChargeStartDate = $strScheduledChargeStartDate;
		    } else {
			    $this->m_strChargeStartDate = new DateTime( date( 'm/1/Y', strtotime( $strScheduledChargeStartDate ) ) );
			    $this->m_strChargeStartDate = date_add( $this->m_strChargeStartDate, date_interval_create_from_date_string( ( min( $this->getChargeMonths() ) - 1 ) . 'months' ) )->format( 'm/d/Y' );
		    }
    	}elseif( true == $boolHonorChargeStartDate && true == valStr( $strScheduledChargeStartDate ) && strtotime( $strScheduledChargeStartDate ) > strtotime( $this->m_strChargeStartDate ) ) {
		    $this->m_strChargeStartDate = $strScheduledChargeStartDate;
	    }

    	if( false == $boolProrateCharges ) {
    		return date( 'm/1/Y', strtotime( $this->m_strChargeStartDate ) );
    	} else {
    		return $this->m_strChargeStartDate;
    	}
    }

    public function getChargeEndDate( $boolProrateCharges = true, $strScheduledChargeStartDate = NULL, $strLeaseEndDate = NULL, $intLeaseTermMonths = NULL, $boolHonorChargeEndDate = false ) {
	    // if charge end date is already set then it should not go in below block,this is written for conventional installment plans
    	if( false == valStr( $this->m_strChargeEndDate ) && true == valStr( $strScheduledChargeStartDate ) ) {

		    if( SELF::FIRST_INSTALLMENT == max( $this->getChargeMonths() ) ) {
			    $this->m_strChargeEndDate = date( 'm/t/Y', strtotime( $strScheduledChargeStartDate ) );
		    } else if( $intLeaseTermMonths == max( $this->getChargeMonths() ) ) {
		    	$this->m_strChargeEndDate = $strLeaseEndDate;
		    } else {
			    $this->m_strChargeEndDate = new DateTime( date( 'm/1/Y', strtotime( $strScheduledChargeStartDate ) ) );
			    $this->m_strChargeEndDate = date_add( $this->m_strChargeEndDate, date_interval_create_from_date_string( ( max( $this->getChargeMonths() ) - 1 ) . 'months' ) )->format( 'm/t/Y' );
		    }
    	} elseif( true == $boolHonorChargeEndDate && true == valStr( $strLeaseEndDate ) ) {
		    $this->m_strChargeEndDate = $strLeaseEndDate;
	    }

    	if( false == $boolProrateCharges ) {
    		return date( 'm/t/Y', strtotime( $this->m_strChargeEndDate ) );
    	} else {
    		return $this->m_strChargeEndDate;
    	}
    }

    /**
    * Validation Functions
    *
    */

	public function valId() {

        if( true == is_null( $this->getId() ) || 0 >= ( int ) $this->getId() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Installment id required' ) ) );
        	return false;
        }

        return true;
    }

	public function valCid() {

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Client id required.' ) ) );
			return false;
		}

		return true;
    }

    public function valInstallmentPlanId() {

    	if( true == is_null( $this->getInstallmentPlanId() ) || 0 >= ( int ) $this->getInstallmentPlanId() ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Installment plan id required.' ) ) );
    		return false;
    	}

    	return true;
    }

    public function valName() {
    	if( true == is_null( $this->getName() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Installment title is required.' ) ) );
    		return false;
    	}

    	return true;
    }

    public function valChargeStartDate() {

        $boolIsValid = true;

		if( false == is_numeric( strtotime( $this->getChargeStartDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getChargeStartDate() ] ), $boolFormat = true ) ) {
			$strErrMsg = __( 'Charge start date in format \'mm/dd/yyyy\' is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', $strErrMsg, NULL ) );
			$boolIsValid &= false;
		} elseif( true == is_null( $this->getChargeStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Charge start date is required.' ), NULL ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
    }

    public function valChargeEndDate() {

        $boolIsValid = true;

		if( false == is_numeric( strtotime( $this->getChargeEndDate() ) ) || false == CValidation::checkDateFormat( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->getChargeEndDate() ] ), $boolFormat = true ) ) {
			$strErrMsg = __( 'Charge end date in format \'mm/dd/yyyy\' is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', $strErrMsg, NULL ) );
			$boolIsValid &= false;
		} elseif( true == is_null( $this->getChargeEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_end_date', __( 'Charge end date is required.' ), NULL ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
    }

    public function valUseBillingMonthAsPostMonth() {

    	if( true == is_null( $this->getUseBillingMonthAsPostMonth() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_billing_month_as_post_month', __( 'Post month is required.' ) ) );
    		return false;
    	}

    	return true;
    }

    public function valDateRange() {

    	if( strtotime( $this->getChargeStartDate() ) > strtotime( $this->getChargeEndDate() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_start_date', __( 'Charge end date should be greater than or equal to charge start date.' ), NULL ) );
    		return false;
    	}

    	return true;
    }

    public function valExistingScheduledChargesForInstallment( $objDatabase ) {

    	if( 0 < ( int ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchExistingScheduledChargesCountByInstallmentIdByCid( $this->getId(), $this->getCid(), $objDatabase ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'installment_plan_id', __( 'You can not delete, this option is associated to one or more scheduled charges.' ) ) );
    		return false;
    	}

    	return true;
    }

	public function valPostOnDate() {

		if( false == is_null( $this->getPostOnDate() ) ) {
			$objChargeStartDate = new DateTime( $this->getChargeStartDate() );
			$objPostOnDate      = new DateTime( $this->getPostOnDate() );
			$intDifference      = ceil( $objChargeStartDate->diff( $objPostOnDate )->format( '%a' ) / 30 );
			if( 13 < $intDifference ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_on_date', __( 'Difference between Installment Start Date and Bill Date can be maximum 13 months.' ), NULL ) );
				return false;
			}
		}

		return true;
	}

   	public function valChargeMonths() {
		if( false == valArr( $this->getChargeMonths() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_months', 'Please select charge months.', NULL ) );
			return false;
		}

	    return true;
    }

    public function valBillingStartMonth() {
	    if( true == valArr( $this->getChargeMonths() ) && true == valId( $this->getBillingStartMonth() ) ) {
	    	if( false == in_array( $this->getBillingStartMonth(), $this->getChargeMonths() ) ) {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'billing_start_month', 'Incorrect billing start month.', NULL ) );
			    return false;
		    }
	    }

	    return true;
    }

    public function validate( $strAction, $boolIsFromDeleteInstallment = false, $objDatabase = NULL, $boolIsStudentProperty = true ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		if( VALIDATE_UPDATE == $strAction ) {
        			$boolIsValid &= $this->valId();
        		}
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valInstallmentPlanId();
        		$boolIsValid &= $this->valName();

        		if( true == $boolIsStudentProperty ) {
        			$boolIsValid &= $this->valChargeStartDate();
					$boolIsValid &= $this->valChargeEndDate();
			        if( true == $this->getPostAllAtOnce() ) {
			        	$boolIsValid &= $this->valPostOnDate();
			        }

			        if( true == $boolIsValid ) {
				        $boolIsValid &= $this->valDateRange();
			        }
				} else {
					$boolIsValid &= $this->valChargeMonths();
					$boolIsValid &= $this->valBillingStartMonth();
				}

        		$boolIsValid &= $this->valUseBillingMonthAsPostMonth();
        		break;

        	case VALIDATE_DELETE:
        		$boolIsValid &= $this->valId();
        		$boolIsValid &= $this->valCid();
        		if( true == $boolIsFromDeleteInstallment ) {
        			$boolIsValid &= $this->valExistingScheduledChargesForInstallment( $objDatabase );
        		}
        		break;

        	default:
        		$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>