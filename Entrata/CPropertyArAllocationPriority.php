<?php

class CPropertyArAllocationPriority extends CBasePropertyArAllocationPriority {

	protected $m_strArAllocationCriteriaTypeName;
	protected $m_boolIsOptional;
	protected $m_intArAllocationCriteriaTypeId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArAllocationCriteriaTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setArAllocationCriteriaTypeName( $strArAllocationCriteriaTypeName ) {
		$this->m_strArAllocationCriteriaTypeName = ( string ) $strArAllocationCriteriaTypeName;
	}

	public function getArAllocationCriteriaTypeName() {
		return $this->m_strArAllocationCriteriaTypeName;
	}

	public function setArAllocationCriteriaTypeId( $intArAllocationCriteriaTypeId ) {
		$this->m_intArAllocationCriteriaTypeId = ( string ) $intArAllocationCriteriaTypeId;
	}

	public function getArAllocationCriteriaTypeId() {
		return $this->m_intArAllocationCriteriaTypeId;
	}

	public function setIsOptional( $boolIsOptional ) {
		$this->m_boolIsOptional = CStrings::strToBool( $boolIsOptional );
	}

	public function getIsOptional() {
		return $this->m_boolIsOptional;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_allocation_criteria_type_name'] ) ) $this->setArAllocationCriteriaTypeName( $arrmixValues['ar_allocation_criteria_type_name'] );
		if( true == isset( $arrmixValues['is_optional'] ) ) $this->setIsOptional( $arrmixValues['is_optional'] );
		if( true == isset( $arrmixValues['ar_allocation_criteria_type_id'] ) ) $this->setArAllocationCriteriaTypeId( $arrmixValues['ar_allocation_criteria_type_id'] );
		return;
	}

}
?>