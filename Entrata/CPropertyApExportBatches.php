<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApExportBatches
 * Do not add any new functions to this class.
 */

class CPropertyApExportBatches extends CBasePropertyApExportBatches {

	public static function fetchPropertyIdsByScheduledApExportBatchIdByCid( $intScheduledApExportBatchId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						property_id
					FROM
						property_ap_export_batches
					WHERE
						scheduled_ap_export_batch_id = ' . ( int ) $intScheduledApExportBatchId . '
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertyApExportBatchesByScheduledApExportBatchIdsByCids( $arrintScheduledApExportBatchIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintScheduledApExportBatchIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						paeb.*
					FROM
						property_ap_export_batches paeb
					WHERE
						paeb.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND paeb.scheduled_ap_export_batch_id IN ( ' . implode( ',', $arrintScheduledApExportBatchIds ) . ' )
					ORDER BY
						paeb.id ASC';

		return self::fetchPropertyApExportBatches( $strSql, $objClientDatabase, $boolIsReturnKeyedArray = false );
	}

}
?>