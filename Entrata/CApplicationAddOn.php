<?php

class CApplicationAddOn extends CBaseApplicationAddOn {

	protected $m_intAddOnGroupId;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAddOnId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

     /**
     * get functions
     */
    public function getAddOnGroupId() {
    	return $this->m_intAddOnGroupId;
    }

    /**
     * Set Functions
     **/

    public function setAddOnGroupId( $intAddOnGroupId ) {
    	$this->m_intAddOnGroupId = $intAddOnGroupId;
    }

}
?>