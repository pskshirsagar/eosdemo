<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeContacts
 * Do not add any new functions to this class.
 */

class CCompanyEmployeeContacts extends CBaseCompanyEmployeeContacts {

	public static function fetchCompanyEmployeeContactByIdByPropertyIdByCid( $intCompanyEmployeeContactId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cemc.*,
						ce.name_first,
						ce.name_last,
						ce.phone_number1,
						ce.phone_number2,
						ce.phone_number1_is_published,
						ce.phone_number2_is_published,
						ce.email_address
					FROM
						company_employee_contacts cemc
						JOIN company_employees ce ON ( cemc.company_employee_id = ce.id AND cemc.cid = ce.cid )
					WHERE
						cemc.cid = ' . ( int ) $intCid . '
						AND cemc.property_id = ' . ( int ) $intPropertyId . '
						AND cemc.id = ' . ( int ) $intCompanyEmployeeContactId;

		return self::fetchCompanyEmployeeContact( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyEmployeeContactsByCidByPropertyIdByCompanyEmployeeContactTypeId( $intCid, $intPropertyId, $intCompanyEmployeeContactTypeId, $objDatabase, $boolIsSortByOrderNum = false, $boolIsPhoneNumberRequired = false ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCompanyEmployeeContactTypeId ) ) return NULL;
		$strWhere = $strOrderBy = '';
		if( true == $boolIsPhoneNumberRequired ) {
			$strWhere .= ' AND ( ce.phone_number1 IS NOT NULL OR ce.phone_number2 IS NOT NULL )';
		}

		if( true == $boolIsSortByOrderNum ) {
			$strOrderBy = ' ORDER BY order_num';
		} else {
			$strOrderBy = ' ORDER BY LOWER( name_last ), LOWER( name_first )';
		}

		$strSql = 'WITH company_employee_contact_custom AS ( SELECT
																	cemc.*,
																	ce.name_first,
																	ce.name_last,
																	ce.phone_number1,
																	ce.phone_number2,
																	ce.phone_number1_is_published,
																	ce.phone_number2_is_published,
																	ce.email_address,
																	cu.is_disabled 
																FROM
																	company_employee_contacts cemc
																	JOIN company_employees ce ON ( cemc.company_employee_id = ce.id AND cemc.cid = ce.cid )
																	LEFT JOIN company_users cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
																WHERE
																	cemc.cid = ' . ( int ) $intCid . '
																	AND cemc.property_id = ' . ( int ) $intPropertyId . '
																	AND cemc.company_employee_contact_type_id = ' . ( int ) $intCompanyEmployeeContactTypeId . '
																	' . $strWhere . '
															)
					SELECT
						*
					FROM
						company_employee_contact_custom
					WHERE
						is_disabled = 0
						OR is_disabled IS NULL
					' . $strOrderBy . '	';

		return self::fetchCompanyEmployeeContacts( $strSql, $objDatabase );
	}

	public static function fetchConflictingCompanyEmployeeContactByCidByPropertyIdByCompanyEmployeeContactTypeIdByPhoneNumber1ByNameFirstByNameLast( $intCid, $intPropertyId, $intCompanyEmployeeContactTypeId, $strPhoneNumber1, $strNameFirst, $strNameLast, $objDatabase, $intCompanyEmployeeContactId = NULL ) {
		if( true == valId( $intCompanyEmployeeContactId ) ) {
			$strSqlExcludeIdClause = ' AND cec.id <> ' . ( int ) $intCompanyEmployeeContactId;
		}

		$strSql = 'SELECT
						ce.*
					FROM
						company_employee_contacts AS cec
						JOIN company_employees AS ce ON (cec.company_employee_id = ce.id AND cec.cid = ce.cid )
						LEFT JOIN company_users AS cu ON( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
						
					WHERE
						cec.cid = ' . ( int ) $intCid . '
						AND cec.property_id = ' . ( int ) $intPropertyId . '
						AND cec.company_employee_contact_type_id = ' . ( int ) $intCompanyEmployeeContactTypeId .
						$strSqlExcludeIdClause . '
						AND REGEXP_REPLACE( ce.phone_number1,\'[^0-9]+\', \'\', \'g\') = \'' . CStrings::strTrimDef( preg_replace( '/[^0-9]/', NULL, $strPhoneNumber1 ), 30, NULL, true ) . '\'
						AND LOWER( ce.name_first ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ), 50, NULL, true ) . '\'
						AND LOWER( ce.name_last ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ), 50, NULL, true ) . '\'
						AND ( cu.is_disabled = 0 OR cu.is_disabled IS NULL )
					LIMIT 1';

		return self::fetchCompanyEmployeeContact( $strSql, $objDatabase );
	}

	public static function fetchCustomCompanyEmployeeContactsByCompanyEmployeeContactTypeIdByPsProductId( $intCompanyEmployeeContactTypeId, $intPsProductsId, $objDatabase ) {

		$strSql = 'WITH company_employee_contact_custom AS ( SELECT
																	cemc.cid,
																	c.company_name,
																	cemc.property_id,
																	p.property_name,
																	cemc.role,
																	ce.name_first,
																	ce.name_last,
																	ce.phone_number1,
																	ce.phone_number2,
																	CASE WHEN cemc.is_on_call = true THEN \'Yes\'
																		WHEN cemc.is_on_call = false THEN \'No\'
																	END AS is_on_call,
																	cemc.notes,
																	cu.is_disabled
																FROM
																	company_employee_contacts cemc
																	JOIN property_products pp ON pp.cid = cemc.cid AND cemc.property_id = pp.property_id AND pp.ps_product_id = ' . ( int ) $intPsProductsId . '
																	JOIN company_employees ce ON ce.cid = cemc.cid AND ce.id = cemc.company_employee_id
																	LEFT JOIN company_users AS cu ON( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
																	JOIN company_employee_contact_types cect ON cect.id = cemc.company_employee_contact_type_id
																	JOIN clients c ON c.id = cemc.cid AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
																	JOIN properties p ON p.cid = cemc.cid AND p.id = cemc.property_id AND p.is_disabled <> 1
																WHERE
																	cemc.company_employee_contact_type_id = ' . ( int ) $intCompanyEmployeeContactTypeId . '
														)
				SELECT
						*
					FROM
						company_employee_contact_custom
					WHERE
						is_disabled = 0
						OR is_disabled IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactByCidByPropertyIdById( $intCid, $intPropertyId, $intCompanyEmployeeContactId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCompanyEmployeeContactId ) ) return NULL;

		$strSql = 'SELECT
						cemc.*,
						ce.name_first,
						ce.name_last,
						ce.phone_number1,
						ce.phone_number2,
						ce.phone_number1_is_published,
						ce.phone_number2_is_published,
						ceca.company_employee_contact_availability_type_id,
						ceca.week_day
					FROM
						company_employee_contacts cemc
						JOIN company_employees ce ON ( cemc.company_employee_id = ce.id AND cemc.cid = ce.cid )
						LEFT JOIN company_employee_contact_availabilities AS ceca ON ( cemc.id = ceca.company_employee_contact_id AND cemc.cid = ceca.cid ) 
					WHERE
						cemc.cid = ' . ( int ) $intCid . '
						AND cemc.property_id = ' . ( int ) $intPropertyId . '
						AND cemc.id = ' . ( int ) $intCompanyEmployeeContactId;

		return self::fetchCompanyEmployeeContact( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactsByPropertyIdByCidByIds( $intPropertyId, $intCid, $arrintCompanyEmployeeContactIds, $objDatabase ) {

		$strSql = 'SELECT
						cemc.*
					FROM
						company_employee_contacts AS cemc
					WHERE
						cemc.cid = ' . ( int ) $intCid . '
						AND cemc.property_id = ' . ( int ) $intPropertyId . '
						AND cemc.id IN( ' . $arrintCompanyEmployeeContactIds . ')';

		return self::fetchCompanyEmployeeContacts( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactMaxOrderNumByPropertyIdByCidByCompanyEmployeeContactTypeId( $intPropertyId, $intCid,  $intCompanyEmployeeContactTypeId, $objDatabase ) {
		$strSql = 'SELECT
						MAX ( order_num ) as max_order_num
					FROM
						company_employee_contacts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND company_employee_contact_type_id = ' . ( int ) $intCompanyEmployeeContactTypeId;

		$arrintOrderNum = fetchData( $strSql, $objDatabase );
		return $arrintOrderNum[0]['max_order_num'];
	}

	public static function fetchCompanyEmployeeContactByCidByPropertyIdByCompanyEmployeeContactTypeIdByCompanyEmployeeContactAvailabilityTypeIdsByWeekDay( $intCid, $intPropertyId, $intCompanyEmployeeContactTypeId, $arrintCompanyEmployeeContactAvailabilityTypeIds, $intCurrentDay, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCompanyEmployeeContactTypeId ) || false == valArr( $arrintCompanyEmployeeContactAvailabilityTypeIds ) ) return NULL;

		$strSql = 'WITH company_employee_contact_custom AS ( SELECT
																	cemc.*,
																	ce.name_first,
																	ce.name_last,
																	ce.phone_number1,
																	ce.phone_number2,
																	ce.phone_number1_is_published,
																	ce.phone_number2_is_published,
																	cu.is_disabled
																FROM
																	company_employee_contacts cemc
																	JOIN company_employees ce ON ( cemc.company_employee_id = ce.id AND cemc.cid = ce.cid )
																	LEFT JOIN company_employee_contact_availabilities as ceca ON( cemc.id = ceca.company_employee_contact_id AND cemc.cid = ceca.cid OR cemc.is_on_call = true )
																	LEFT JOIN company_users AS cu ON( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
																WHERE
																	cemc.cid = ' . ( int ) $intCid . '
																	AND cemc.property_id = ' . ( int ) $intPropertyId . '
																	AND cemc.company_employee_contact_type_id = ' . ( int ) $intCompanyEmployeeContactTypeId . '
																	AND ceca.company_employee_contact_availability_type_id IN ( ' . implode( ',', $arrintCompanyEmployeeContactAvailabilityTypeIds ) . ' )
																	AND ceca.week_day = ' . ( int ) $intCurrentDay . '
																	AND ( ce.phone_number1 IS NOT NULL OR ce.phone_number2 IS NOT NULL )
															)
																
					SELECT
						*
					FROM
						company_employee_contact_custom
					WHERE
						is_disabled = 0
						OR is_disabled IS NULL
					ORDER BY order_num	
					LIMIT 1	';

		return self::fetchCompanyEmployeeContact( $strSql, $objDatabase );
	}

	public static function fetchFirstCompanyEmployeeContactByCidByPropertyIdByCompanyEmployeeContactTypeId( $intCid, $intPropertyId, $intCompanyEmployeeContactTypeId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCompanyEmployeeContactTypeId ) ) return NULL;

		$strSql = ' WITH first_available_company_employee_contact AS ( SELECT
																cemc.*,
																ce.name_first,
																ce.name_last,
																ce.phone_number1,
																ce.phone_number2,
																ce.phone_number1_is_published,
																ce.phone_number2_is_published,
																cu.is_disabled
															FROM
																company_employee_contacts cemc
																JOIN company_employees ce ON ( cemc.company_employee_id = ce.id AND cemc.cid = ce.cid )
																LEFT JOIN company_users AS cu ON( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
															WHERE
																cemc.cid = ' . ( int ) $intCid . '
																AND cemc.property_id = ' . ( int ) $intPropertyId . '
																AND cemc.company_employee_contact_type_id = ' . ( int ) $intCompanyEmployeeContactTypeId . '
																AND ( ce.phone_number1 IS NOT NULL OR ce.phone_number2 IS NOT NULL )
															)
						SELECT
							*
						FROM
							first_available_company_employee_contact
						WHERE
							is_disabled = 0
							OR is_disabled IS NULL
						ORDER BY order_num	
						LIMIT 1	';

		return self::fetchCompanyEmployeeContact( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactsByCidByPropertyIdByCompanyEmployeeContactTypeIdByCompanyEmployeeContactAvailabilityTypeIdsByWeekDay( $intCid, $intPropertyId, $intCompanyEmployeeContactTypeId, $arrintCompanyEmployeeContactAvailabilityTypeIds, $intWeekDay, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCompanyEmployeeContactTypeId ) || false == valArr( $arrintCompanyEmployeeContactAvailabilityTypeIds ) ) return NULL;

		$strSql = 'WITH company_emplpoyee_contact_custom AS ( SELECT
																	DISTINCT( cec.id ),
																	cec.is_on_call,
																	cec.role,
																	cec.order_num,
																	cec.cid,
																	cec.property_id,
																	ce.name_first,
																	ce.name_last,
																	ce.phone_number1,
																	ce.phone_number2,
																	ce.phone_number1_is_published,
																	ce.phone_number2_is_published,
																	cu.is_disabled
																FROM
																	company_employee_contacts as cec
																	JOIN company_employees AS ce ON ( cec.company_employee_id = ce.id AND cec.cid = ce.cid )
																	LEFT JOIN company_users AS cu ON( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
																	LEFT JOIN company_employee_contact_availabilities as ceca ON ( cec.id = ceca.company_employee_contact_id AND cec.cid = ceca.cid OR cec.is_on_call = true )
																WHERE
																	cec.cid = ' . ( int ) $intCid . '
																	AND cec.property_id = ' . ( int ) $intPropertyId . '
																	AND cec.company_employee_contact_type_id = ' . ( int ) $intCompanyEmployeeContactTypeId . '
																	AND ceca.company_employee_contact_availability_type_id IN ( ' . implode( ',', $arrintCompanyEmployeeContactAvailabilityTypeIds ) . ' )
																	AND ceca.week_day = ' . ( int ) $intWeekDay . '
																	AND ( ce.phone_number1 IS NOT NULL OR ce.phone_number2 IS NOT NULL )
																)
					SELECT
						*
					FROM
						company_emplpoyee_contact_custom
					WHERE
						is_disabled = 0
						OR is_disabled IS NULL
					ORDER BY 
						order_num,
						id';

		return self::fetchCompanyEmployeeContacts( $strSql, $objDatabase );
	}

	public static function fetchPrimaryCorporateCompanyEmployeeContactByCidByPropertyId( $intCid, $intPropertyId, $objDatabase, $arrintCorporateComplaintTypeId= NULL ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						cemc.*
					FROM
						company_employee_contacts AS cemc
					WHERE
						cemc.cid = ' . ( int ) $intCid . '
						AND cemc.property_id = ' . ( int ) $intPropertyId . '
						AND cemc.company_employee_contact_type_id = ' . CCompanyEmployeeContactType::CORPORATE . '
						AND cemc.is_primary::BOOLEAN = true';

		if( true == valArr( $arrintCorporateComplaintTypeId ) ) {
			$strSql .= ' AND ( cemc.corporate_complaint_type_ids = ARRAY[' . sqlIntImplode( $arrintCorporateComplaintTypeId ) . '] )';
		} else {
			$strSql .= ' AND cemc.corporate_complaint_type_ids IS NULL  ';
		}

		return self::fetchCompanyEmployeeContact( $strSql, $objDatabase );
	}

	public static function fetchCorporateCompanyEmployeeContactByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_employee_contacts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IS NULL
						AND company_employee_contact_type_id = ' . CCompanyEmployeeContactType::CORPORATE;

		return self::fetchCompanyEmployeeContact( $strSql, $objDatabase );
	}

	public static function fetchDefaultCorporateCompanyEmployeeContactByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						cec.id,
						cec.is_primary,
						ce.id AS company_employee_id,
						ce.name_first,
						ce.name_last,
						ce.email_address,
						ce.title,
						ce.phone_number1,
						ce.phone_number2
					FROM
						company_employee_contacts AS cec
						JOIN company_employees AS ce ON ( ce.id = cec.company_employee_id AND cec.cid = ce.cid )
					WHERE
						cec.cid = ' . ( int ) $intCid . '
						AND cec.property_id IS NULL
						AND cec.company_employee_contact_type_id = ' . CCompanyEmployeeContactType::CORPORATE;

		return self::fetchCompanyEmployeeContact( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeContactsByCidByCorporateComplaintTypeIdsByCompanyEmployeeContactTypeId( $intCid, $arrintCorporateComplaintTypeIds, $intCompanyEmployeeContactTypeId, $objDatabase, $intPropertyId = NULL, $boolIsSortByOrderNum = false, $boolIsEmailAddressRequired = false ) {
		if( false == valId( $intCid ) || false == valId( $intCompanyEmployeeContactTypeId ) ) return NULL;

		if( false == valArr( $arrintCorporateComplaintTypeIds ) ) {
			$arrintCorporateComplaintTypeIds = [ CCorporateComplaintType::OTHER ];
		}

		$strWhere	= NULL;
		$strOrderBy	= NULL;

		if( true == valId( $intPropertyId ) ) {
			$strWhere .= ' AND ( cemc.property_id = ' . ( int ) $intPropertyId . ' OR cemc.property_id IS NULL )';
		} else {
			$strWhere .= ' AND cemc.property_id IS NULL';
		}

		if( true == $boolIsEmailAddressRequired ) {
			$strWhere .= ' AND ( ce.email_address IS NOT NULL )';
		}

		if( true == $boolIsSortByOrderNum ) {
			$strOrderBy = ' ORDER BY order_num';
		} else {
			$strOrderBy = ' ORDER BY LOWER( name_last ), LOWER( name_first )';
		}

		$strSql = 'SELECT
						cemc.*,
						ce.name_first,
						ce.name_last,
						ce.phone_number1,
						ce.phone_number2,
						ce.phone_number1_is_published,
						ce.phone_number2_is_published,
						ce.email_address 
					FROM
						company_employee_contacts AS cemc
						JOIN company_employees AS ce ON ( cemc.company_employee_id = ce.id AND cemc.cid = ce.cid )
						LEFT JOIN company_users AS cu ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
					WHERE
						cemc.cid = ' . ( int ) $intCid . '
						AND ( cemc.corporate_complaint_type_ids && ARRAY[' . sqlIntImplode( $arrintCorporateComplaintTypeIds ) . ']::INTEGER[]
						OR cemc.corporate_complaint_type_ids IS NULL )
						AND cemc.company_employee_contact_type_id = ' . ( int ) $intCompanyEmployeeContactTypeId . '
						AND ( cu.is_disabled = 0 OR cu.is_disabled IS NULL )
						' . $strWhere . $strOrderBy;

		return self::fetchCompanyEmployeeContacts( $strSql, $objDatabase );
	}

}
?>