<?php

class CDelinquencyPolicyDocument extends CBaseDelinquencyPolicyDocument {

	protected $m_strDocumentName;

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['document_name'] ) )	$this->setDocumentName( $arrmixValues['document_name'] );
		if( true == isset( $arrmixValues['email_to_resident'] ) )	$this->setEmailToResident( $arrmixValues['email_to_resident'] );
		if( true == isset( $arrmixValues['file_name'] ) )	$this->setFileName( $arrmixValues['file_name'] );
	}

	/**
	 * Get Functions
	 */

	public function setDocumentName( $strDocumentName ) {
		$this->m_strDocumentName = CStrings::strTrimDef( $strDocumentName, -1, NULL, true );
	}

	public function getDocumentName() {
		return $this->m_strDocumentName;
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function setEmailToResident( $boolEmailToResident ) {
		$this->m_boolEmailToResident = ( 0 < strlen( trim( $boolEmailToResident ) ) ) ? CStrings::strToBool( $boolEmailToResident ) : NULL;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDelinquencyPolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDocumentId() {

		$boolIsValid = true;

		if( false == valId( $this->getDocumentId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_id', __( 'Document is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valDocument( $objDatabase ) {

		$boolIsValid = true;

		if( true == valId( $this->getDocumentId() ) ) {
			$objDocument = CDocuments::fetchDocumentByIdByCid( $this->getDocumentId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objDocument, 'CDocument' ) ) {

				if( 'pdf' == pathinfo( $objDocument->getFileName() )['extension'] && ( true == $this->getEmailToResident() || true == is_null( $this->getEmailToResident() ) ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'doc_extension', __( 'Emailed notices must use .docx templates.' ) ) );
					$boolIsValid &= false;
				}

				if( false == $objDocument->getIsPublished() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', __( 'The following document is no longer valid, please select another one.' ) ) );
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valEmailSubject() {
		$boolIsValid = true;
		if( true == is_null( $this->getEmailSubject() ) && ( true == $this->getEmailToResident() || true == is_null( $this->getEmailToResident() ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_subject', __( 'Email Subject is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valDelinquencyThresholdTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valThresholdOffset() {

		$boolIsValid = true;

		if( true == is_null( $this->getThresholdOffset() ) || $this->getThresholdOffset() <= 0 ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'threshold_offset', __( 'Timing is required and must be greater than zero.' ) ) );
			return $boolIsValid;

		}
		return $boolIsValid;
	}

	public function valThresholdOffsetSmallBalance() {

		$boolIsValid = true;
		if( ( true == is_null( $this->getThresholdOffset() ) || $this->getThresholdOffset() <= 0 ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'threshold_offset', __( 'Timing is required and must be greater than zero.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valResendFrequency() {

		$boolIsValid = true;
		if( ( false == valId( $this->getResendFrequency() ) || $this->getResendFrequency() == 0 ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resend_frequency', __( 'Resend Frequency is required and must be greater than zero.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSetCertifiedFundsOnly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsStartEviction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsDocumentRequired = true, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'collections_policy_document_insert':
			case 'collections_policy_document_update':
			case 'delinquency_policy_document_insert':
			case 'delinquency_policy_document_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valThresholdOffset();
				if( true == $boolIsDocumentRequired ) {
					$boolIsValid &= $this->valDocumentId();
					$boolIsValid &= $this->valDocument( $objDatabase );
					$boolIsValid &= $this->valEmailSubject();
				}
				break;

			case 'delinquency_policy_document_insert_small_balance':
			case 'delinquency_policy_document_update_small_balance':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDocumentId();
				$boolIsValid &= $this->valDocument( $objDatabase );
				$boolIsValid &= $this->valThresholdOffsetSmallBalance();
				$boolIsValid &= $this->valResendFrequency();
				$boolIsValid &= $this->valEmailSubject();
				break;

			default:
				$boolIsValid &= false;
				break;
		}

		return $boolIsValid;
	}

}
?>