<?php

class CGlLedgerType extends CBaseGlLedgerType {

	const GL		= 1;
	const AR		= 2;
	const AP		= 3;
	const ASSET		= 4;
}
?>