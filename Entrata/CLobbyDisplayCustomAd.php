<?php

class CLobbyDisplayCustomAd extends CBaseLobbyDisplayCustomAd {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWebsiteId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomAdsTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTitle() {
        $boolIsValid = true;
        if( false == valStr( $this->m_strTitle ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_custom_ad', __( 'Please enter Title.' ) ) );
        	$boolIsValid = false;
        }
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        if( false == valStr( $this->m_strDescription ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_custom_ad', __( 'Please enter body text.' ) ) );
        	$boolIsValid = false;
        }
        return $boolIsValid;
    }

    public function valSubtext() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIcon() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valDescription();
        		$boolIsValid &= $this->valTitle();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['name'] ) )	$this->setName( $arrmixValues['name'] );
    	return;
    }

    public function getName() {
    	return $this->m_strName;
    }

    public function setName( $strName ) {
    	$this->m_strName = $strName;
    }

}
?>