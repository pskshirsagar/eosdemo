<?php

class CPropertyPaymentTypeArCode extends CBasePropertyPaymentTypeArCode {

	protected $m_intLedgerFilterId;
	protected $m_strArCodeName;
	protected $m_strPaymentTypeName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Get Functions
	*
	*/

	public function getLedgerFilterId() {
		return $this->m_intLedgerFilterId;
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function getPaymentTypeName() {
		return $this->m_strPaymentTypeName;
	}

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['ledger_filter_id'] ) )		$this->setLedgerFilterId( $arrmixValues['ledger_filter_id'] );
		if( true == isset( $arrmixValues['ar_code_name'] ) )			$this->setArCodeName( $arrmixValues['ar_code_name'] );
		if( true == isset( $arrmixValues['payment_type_name'] ) )		$this->setPaymentTypeName( $arrmixValues['payment_type_name'] );

		return;
	}

	public function setLedgerFilterId( $intLedgerFilterId ) {
		$this->m_intLedgerFilterId = $intLedgerFilterId;
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = $strArCodeName;
	}

	public function setPaymentTypeName( $strPaymentTypeName ) {
		$this->m_strPaymentTypeName = $strPaymentTypeName;
	}

}
?>