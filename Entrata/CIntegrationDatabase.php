<?php
use Psi\Eos\Entrata\CUnitSpaces;

class CIntegrationDatabase extends CBaseIntegrationDatabase {

	protected $m_strIntegrationClientTypeName;
	protected $m_strIntegrationVersionName;
	protected $m_strClientName;
	protected $m_strCompanyStatusTypeName;
	protected $m_intPropertyId;
	protected $m_intDatabaseId;

	/**
	* Create Functions
	*
	*/

    public function createPropertyIntegrationDatabase() {
    	$objPropertyIntegrationDatabase = new CPropertyIntegrationDatabase();
    	$objPropertyIntegrationDatabase->setCid( $this->getCid() );
    	$objPropertyIntegrationDatabase->setIntegrationDatabaseId( $this->getId() );

    	return $objPropertyIntegrationDatabase;
    }

 	public function createIntegrationServiceLog( $intIntegrationServiceId = NULL ) {
    	$objIntegrationServiceLog = new CIntegrationServiceLog();

		$objIntegrationServiceLog->setCid( $this->getCid() );
		$objIntegrationServiceLog->setIntegrationDatabaseId( $this->getId() );
		$objIntegrationServiceLog->setIntegrationServiceId( $intIntegrationServiceId );
		$objIntegrationServiceLog->setLogDate( date( 'Y-m-d' ) );

    	return $objIntegrationServiceLog;
    }

    public function createIntegrationRestore() {
		$objIntegrationRestore = new CIntegrationRestore();
		$objIntegrationRestore->setCid( $this->getCid() );
		$objIntegrationRestore->setIntegrationDatabaseId( $this->getId() );

		return $objIntegrationRestore;
	}

	public function createLease() {
		$objLease = new CLease();
		$objLease->setCid( $this->getCid() );
		$objLease->setIntegrationDatabaseId( $this->getId() );
		$objLease->setIsSyncApplicationAndLease( false );

		return $objLease;
	}

	/**
	* Get Functions
	*
	*/

	public function getUsername() {
		if( false == valStr( $this->m_strUsernameEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strUsernameEncrypted, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME ] );
	}

	public function getPassword() {
		if( false == valStr( $this->m_strPasswordEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPasswordEncrypted, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}


	public function getDevUsername() {
		if( false == valStr( $this->m_strDevUsernameEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strDevUsernameEncrypted, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME ] );
	}

	public function getDevPassword() {
		if( false == valStr( $this->m_strDevPasswordEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strDevPasswordEncrypted, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

	public function getDatabaseUsername() {
		if( false == valStr( $this->m_strDatabaseUsernameEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strDatabaseUsernameEncrypted, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME ] );
	}

	public function getDatabasePassword() {
		if( false == valStr( $this->m_strDatabasePasswordEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strDatabasePasswordEncrypted, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

	public function getIntegrationClientTypeName() {
		 return $this->m_strIntegrationClientTypeName;
    }

    public function getIntegrationVersionName() {
		 return $this->m_strIntegrationVersionName;
    }

    public function getClientName() {
		 return $this->m_strClientName;
    }

	public function getCompanyStatusTypeName() {
		 return $this->m_strCompanyStatusTypeName;
    }

    public function getIsMigrateIntegrationClientType() {
    	if( true == in_array( $this->getIntegrationClientTypeId(), CIntegrationClientType::$c_arrintMigrateIntegrationClientTypeIds ) ) {
    		return true;
    	}
    	return false;
    }

	public function getPropertyId() {
		 return $this->m_intPropertyId;
    }

    public function getDatabaseId() {
    	  return $this->m_intDatabaseId;
    }

	public function getIONCredentials() {
		if( ( is_object( $this->getDetailsField( [ 'additional_details' ] ) ) ) ) {
			return json_encode( $this->decryptIONCredentials( $this->getDetailsField( [ 'additional_details' ] ) ), JSON_UNESCAPED_SLASHES );
		} else {
			return isset( $_REQUEST['integration_database']['ion_credentials'] ) ? $_REQUEST['integration_database']['ion_credentials'] : '';
		}
	}

	/**
	* Set Functions
	*
	*/

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
        if( true == isset( $arrmixValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['username'] ) : $arrmixValues['username'] );
        if( true == isset( $arrmixValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['password'] ) : $arrmixValues['password'] );
        if( true == isset( $arrmixValues['dev_username'] ) ) $this->setDevUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dev_username'] ) : $arrmixValues['dev_username'] );
        if( true == isset( $arrmixValues['dev_password'] ) ) $this->setDevPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dev_password'] ) : $arrmixValues['dev_password'] );
		if( true == isset( $arrmixValues['database_username'] ) ) $this->setDatabaseUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['database_username'] ) : $arrmixValues['database_username'] );
        if( true == isset( $arrmixValues['database_password'] ) ) $this->setDatabasePassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['database_password'] ) : $arrmixValues['database_password'] );
        if( true == isset( $arrmixValues['integration_client_type_name'] ) ) $this->setIntegrationClientTypeName( $arrmixValues['integration_client_type_name'] );
        if( true == isset( $arrmixValues['integration_version_name'] ) ) $this->setIntegrationVersionName( $arrmixValues['integration_version_name'] );
        if( true == isset( $arrmixValues['client_name'] ) ) $this->setClientName( $arrmixValues['client_name'] );
        if( true == isset( $arrmixValues['company_status_type_name'] ) ) $this->setCompanyStatusTypeName( $arrmixValues['company_status_type_name'] );
        if( true == isset( $arrmixValues['database_server_name'] ) ) $this->setDatabaseServerName( $arrmixValues['database_server_name'] );
        if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
        if( true == isset( $arrmixValues['database_id'] ) ) $this->settDatabaseId( $arrmixValues['database_id'] );
	    if( true == isset( $arrmixValues['ion_credentials'] ) ) $this->setIONCredentials( $arrmixValues['ion_credentials'] );
    }

    public function setUsername( $strUsername ) {
		if( true == valStr( $strUsername ) ) {
			$this->setUsernameEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strUsername ), CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
		}
	}

	public function setPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->setPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strPassword ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function setDevUsername( $strDevUsername ) {
		if( true == valStr( $strDevUsername ) ) {
			$this->setDevUsernameEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strDevUsername ), CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
		}
	}

	public function setDevPassword( $strDevPassword ) {
		if( true == valStr( $strDevPassword ) ) {
			$this->setDevPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strDevPassword ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function setDatabaseUsername( $strDatabaseUsername ) {
		if( true == valStr( $strDatabaseUsername ) ) {
			$this->setDatabaseUsernameEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strDatabaseUsername ), CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
		}
	}

	public function setDatabasePassword( $strDatabasePassword ) {
		if( true == valStr( $strDatabasePassword ) ) {
			$this->setDatabasePasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strDatabasePassword ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

    public function setIntegrationClientTypeName( $strIntegrationClientTypeName ) {
		 return $this->m_strIntegrationClientTypeName = $strIntegrationClientTypeName;
    }

    public function setIntegrationVersionName( $strIntegrationVersionName ) {
		 return $this->m_strIntegrationVersionName = $strIntegrationVersionName;
    }

    public function setClientName( $strClientName ) {
		 return $this->m_strClientName = $strClientName;
    }

	public function setCompanyStatusTypeName( $strCompanyStatusTypeName ) {
		 return $this->m_strCompanyStatusTypeName = $strCompanyStatusTypeName;
    }

	public function setPropertyId( $intPropertyId ) {
		 $this->m_intPropertyId = $intPropertyId;
    }

    public function settDatabaseId( $intDatbaseId ) {
    	return $this->m_intDatabaseId = $intDatbaseId;
    }

	public function setIONCredentials( $strJsonIONCredentials ) {

    	if( CIntegrationClientType::AMSI != $this->getIntegrationClientTypeId() || !valStr( $_REQUEST['integration_database']['checkOAuth'] ) ) {
    		return true;
	    }

		if( !valJsonString( $strJsonIONCredentials ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'oauth', 'Invalid OAuth Credentials.' ) );
			$this->setDetailsField( [ 'additional_details' ], '' );
			return false;
		}

		$arrstrIONCredentials['additional_details'] = json_decode( $strJsonIONCredentials, true );
		$boolIsValid = $this->validateIONCredentials( $arrstrIONCredentials );

		if( !$boolIsValid ) {
			$this->setDetailsField( [ 'additional_details' ], '' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'oauth', 'Valid OAuth Credentials required.' ) );
			return false;
		}

		$arrstrIONCredentials = $this->encryptIONCredentials( $arrstrIONCredentials );
		$this->setDetails( json_encode( $arrstrIONCredentials ) );
	}

	/**
	* Validation Functions
	*
	*/

    public function validate( $strAction, $objClientDatabase = NULL ) {

    	$boolIsValid = true;

    	$objIntegrationDatabaseValidator = new CIntegrationDatabaseValidator();
    	$objIntegrationDatabaseValidator->setIntegrationDatabase( $this );

    	$boolIsValid &= $objIntegrationDatabaseValidator->validate( $strAction, $objClientDatabase );

        return $boolIsValid;
    }

	public function validateIONCredentials( $arrstrIONCredentials ) : bool {

		if( !isset( $arrstrIONCredentials['additional_details']['edex']['ion_credentials'] ) || !valArr( $arrstrIONCredentials['additional_details']['edex']['ion_credentials'] ) ) {
			$this->setDetailsField( 'additional_details', '' );
			return false;
		}

		$arrstrInvalidKeys = array_diff( array_values( [ 'ci', 'cs', 'iu', 'oa', 'or', 'ot', 'pu', 'saak', 'sask' ] ), array_keys( $arrstrIONCredentials['additional_details']['edex']['ion_credentials'] ) );

		return ( count( $arrstrInvalidKeys ) == 0 );
	}

    /**
	* Common Functions
	*
	*/

	public function encryptIONCredentials( $arrstrIONCredentials ) {

		foreach( $arrstrIONCredentials['additional_details']['edex']['ion_credentials'] as $strKey => $strValue ) {
			if( in_array( $strKey, [ 'ci', 'cs', 'saak', 'sask' ] ) ) {
				$arrstrIONCredentials['additional_details']['edex']['ion_credentials'][$strKey] = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strValue ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION );
			}
		}

		return $arrstrIONCredentials;
	}

	public function decryptIONCredentials( $arrstrIONCredentials ) {
		$arrstrIONCredentials = json_decode( json_encode( $arrstrIONCredentials ), true );

		if( !valArr( $arrstrIONCredentials ) ) {
			return [ 'additional_details' => '' ];
		}

		foreach( $arrstrIONCredentials['edex']['ion_credentials'] as $strKey => $strValue ) {
			if( in_array( $strKey, [ 'ci', 'cs', 'saak', 'sask' ] ) ) {
				$arrstrDecryptIONCredentials['edex']['ion_credentials'][$strKey] = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( trim( $strValue ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION );
			} else {
				$arrstrDecryptIONCredentials['edex']['ion_credentials'][$strKey] = $strValue;
			}
		}

		return $arrstrDecryptIONCredentials;
	}

    public function activateOnHoldQueueItemsByIntegrationDataBaseIdByIntegrationServiceIdsByCid( $intCompanyUserId, $arrintIntegrationServiceIds, $intCid, $objDatabase, $arrintPropertyIds = NULL ) {
    	if( false == valArr( $arrintIntegrationServiceIds ) ) return true;

    	$boolIsValid = true;

		$strSql = 'SELECT
						iq.*
					FROM
						integration_queues iq
						JOIN integration_clients ic ON( iq.integration_client_id = ic.id AND iq.cid = ic.cid )
						JOIN integration_databases id ON( id.id = ic.integration_database_id AND id.cid = ic.cid )
					WHERE
						iq.integration_service_id IN( ' . implode( ',', $arrintIntegrationServiceIds ) . ')
						AND id.id = ' . $this->getId() . '
						AND iq.cid = ' . ( int ) $intCid . '
						AND id.integration_client_status_type_id = ' . CIntegrationClientStatusType::ACTIVE . '
						AND ic.integration_client_status_type_id = ' . CIntegrationClientStatusType::ACTIVE . '
						AND iq.integration_queue_status_type_id = ' . CIntegrationQueueStatusType::WAITING;

      	if( true == valArr( $arrintPropertyIds ) ) {
    		$strSql .= ' AND iq.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
    	}

    	$arrobjIntegrationQueues = \Psi\Eos\Entrata\CIntegrationQueues::createService()->fetchIntegrationQueues( $strSql, $objDatabase );

		if( true == valArr( $arrobjIntegrationQueues ) ) {
			foreach( $arrobjIntegrationQueues as $objIntegrationQueue ) {
		  		$objIntegrationQueue->setIntegrationQueueStatusTypeId( CIntegrationQueueStatusType::READY );
				$objIntegrationQueue->setLockedOn( NULL );
				$objIntegrationQueue->setLockSequenceNumber( NULL );
		    	$boolIsValid &= $objIntegrationQueue->update( $intCompanyUserId, $objDatabase );
			}
		}

		return $boolIsValid;
    }

    public function activateOnHoldQueueItemsByIntegrationDataBaseIdByIntegrationServiceIdsByDateByCid( $intCompanyUserId, $arrintIntegrationServiceIds, $intCid, $arrintDate, $intPropertyId, $objDatabase ) {
        if( false == valArr( $arrintIntegrationServiceIds ) ) return true;

        $boolIsValid            = true;
        $strDateCondition       = NULL;
        $boolDoNotActivateQueue = false;
        if( true == valArr( $arrintDate ) ) {
            foreach( $arrintDate as $intDate ) {

                if( false == valStr( $intDate ) ) continue;
                $strDateCondition .= '( iq.created_on <= \'' . date( 'Y-m-' . $intDate ) . ' 23:59:59\' and iq.created_on >= \'' . date( 'Y-m-' . $intDate ) . ' 00:00:00\' ) OR';
            }
            $strDateCondition = trim( $strDateCondition, ' OR' );
        }

        $strSql = 'SELECT
						iq.*
					FROM
						integration_queues iq
						JOIN integration_clients ic ON( iq.integration_client_id = ic.id AND iq.cid = ic.cid )
						JOIN integration_databases id ON( id.id = ic.integration_database_id AND id.cid = ic.cid )
                        LEFT JOIN integration_client_key_values ickv ON ( ickv.cid = id.cid AND ickv.integration_database_id = id.id AND ickv.key = \'INTEGRATION_CLIENT_STATUS\' AND ickv.value = \'1\' )
                	WHERE
						iq.integration_service_id IN( ' . implode( ',', $arrintIntegrationServiceIds ) . ')
						AND id.id = ' . ( int ) $this->getId() . '
						AND iq.property_id = ' . ( int ) $intPropertyId . '
						AND ickv.id IS NULL
						AND iq.cid = ' . ( int ) $intCid . '
						AND id.integration_client_status_type_id = ' . CIntegrationClientStatusType::ACTIVE . '
						AND ic.integration_client_status_type_id = ' . CIntegrationClientStatusType::ACTIVE . '
						AND iq.integration_queue_status_type_id = ' . CIntegrationQueueStatusType::WAITING;

        if( false == is_null( $strDateCondition ) && true == valStr( $strDateCondition ) ) {

            $strSql .= ' AND ' . $strDateCondition;
        }
        $arrobjIntegrationQueues = \Psi\Eos\Entrata\CIntegrationQueues::createService()->fetchIntegrationQueues( $strSql, $objDatabase );

        if( true == valArr( $arrobjIntegrationQueues ) ) {
            foreach( $arrobjIntegrationQueues as $objIntegrationQueue ) {
                $objIntegrationQueue->setIntegrationQueueStatusTypeId( CIntegrationQueueStatusType::READY );
				$objIntegrationQueue->setLockedOn( NULL );
				$objIntegrationQueue->setLockSequenceNumber( NULL );
                $boolIsValid &= $objIntegrationQueue->update( $intCompanyUserId, $objDatabase );
            }
        }
        return $boolIsValid;
    }

    public function manageCompanyIntegrationDatabase( $intCompanyUserId, $intOriginalIntegrationclientStatusTypeId, $objDatabase ) {

    	if( ( CIntegrationClientStatusType::ON_HOLD == $intOriginalIntegrationclientStatusTypeId
    			|| CIntegrationClientStatusType::DISABLED == $intOriginalIntegrationclientStatusTypeId )
				&& CIntegrationClientStatusType::ACTIVE == $this->getIntegrationClientStatusTypeId() ) {
				$this->activateOnHoldQueueItemsByIntegrationDataBaseIdByIntegrationServiceIdsByCid( $intCompanyUserId, CIntegrationService::$c_arrintIntegrationServicesToBeQueued, $this->getCid(), $objDatabase );
		}
    }

    /**
	* Fetch Functions
	*
	*/

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchIntegrationVersionOptions( $objDatabase ) {
		return CIntegrationVersions::fetchIntegrationVersionsByIntegrationClientTypeId( $this->getIntegrationClientTypeId(), $objDatabase );
	}

	public function fetchCustomers( $objDatabase ) {
    	return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeases( $objDatabase ) {
        return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchArCodes( $objDatabase ) {
		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceProblems( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchMaintenanceProblemsByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceStatusTypes( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchMaintenanceStatusesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApPayees( $objDatabase ) {
        return CApPayees::fetchApPayeesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClients( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchCustomIntegrationClientsByIntegrationDatabaseIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientsByIntegrationClientStatusTypeIds( $arrintIntegrationClientStatusTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientsByIntegrationClientStatusTypeIdsByIntegrationDatabaseIdByCid( $arrintIntegrationClientStatusTypeIds, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationServiceLogs( $objDatabase ) {
    	return \Psi\Eos\Entrata\CIntegrationServiceLogs::createService()->fetchIntegrationServiceLogsByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchDefaultAvailableIntegrationClientTypeServices( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientTypeServices::createService()->fetchDefaultAvailableIntegrationClientTypeServicesByIntegrationDatabaseIdByIntegrationClientTypeIdByCid( $this->m_intId, $this->m_intIntegrationClientTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIntegrationDatabaseIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedProperties( $objPagination, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPaginatedPropertiesByIntegrationDatabaseIdByCid( $this->m_intId, $this->getCid(), $objPagination, false, $objDatabase );
	}

	public function fetchPaginatedPropertiesCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPaginatedPropertiesByIntegrationDatabaseIdByCid( $this->m_intId, $this->getCid(), NULL, true, $objDatabase );
	}

	/**
	 * Fetch the integrated properties
	 *
	 * @param CPagination $objPagination
	 * @param CDatabase $objDatabase
	 *
	 * @return array
	 */
	public function fetchPaginatedOnlyIntegratedProperties( $objPagination, $objDatabase ): array {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPaginatedOnlyIntegratedPropertiesByIntegrationDatabaseIdOrIntegrationDatabaseIdNullByCid( $this->m_intId, $this->getCid(), $objPagination, $objDatabase );
	}

	/**
	 * Fetch the integrated properties
	 *
	 * @param CDatabase $objDatabase
	 *
	 * @return integer
	 */
	public function fetchPaginatedOnlyIntegratedPropertiesCount( $objDatabase ): int {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPaginatedOnlyIntegratedPropertiesCountByIntegrationDatabaseIdOrIntegrationDatabaseIdNullByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedIntegratedProperties( $objPagination, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPaginatedIntegratedPropertiesByIntegrationDatabaseIdOrIntegrationDatabaseIdNullByCid( $this->m_intId, $this->getCid(), $objPagination, $objDatabase );
	}

	public function fetchPaginatedIntegratedPropertiesCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPaginatedIntegratedPropertiesCountByIntegrationDatabaseIdOrIntegrationDatabaseIdNullByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchActiveProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchActivePropertiesByIntegrationDatabaseIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchSeparatelyAssociatedProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSeparatelyAssociatedPropertiesByIntegrationDatabaseIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchIntegrationClientByIntegrationServiceId( $intIntegrationServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByIntegrationServiceIdByIntegrationDatabaseIdByCid( $intIntegrationServiceId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientsByIntegrationServiceIds( $arrintIntegrationServiceIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientsByIntegrationDatabaseIdByIntegrationServiceIdsByCid( $this->m_intId, $arrintIntegrationServiceIds, $this->m_intCid, $objDatabase );
	}

	public function fetchCompetingIntegratedProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchIntegratedPropertiesNotAssociatedToIntegrationDatabaseByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertiesByRemotePrimaryKeys( $arrstrPropertyRemotePrimaryKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByRemotePrimaryKeysByIntegrationDatabaseIdByCid( $arrstrPropertyRemotePrimaryKeys, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyIntegrationDatabases( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyIntegrationDatabases::createService()->fetchPropertyIntegrationDatabasesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyByRemotePrimaryKey( $strRemotePrimarykey, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIntegrationDatabaseIdByRemotePrimaryKeyByCid( $this->getId(), $strRemotePrimarykey, $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyCountByRemotePrimaryKey( $strRemotePrimarykey, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyCountByIntegrationDatabaseIdByRemotePrimaryKeyByCid( $this->getId(), $strRemotePrimarykey, $this->getCid(), $objClientDatabase );
	}

	public function fetchLeaseByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIntegrationDatabaseIdByRemotePrimaryKeyByCid( $this->m_intId, $strRemotePrimaryKey, $this->m_intCid, $objDatabase );
	}

	public function fetchNonCachedLeaseByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchNonCachedLeaseByIntegrationDatabaseIdOrNullByRemotePrimaryKeyByCid( $this->m_intId, $strRemotePrimaryKey, $this->m_intCid, $objDatabase );
	}

	public function fetchNonCachedLeaseByRemotePrimaryKeyByPropertyId( $strRemotePrimaryKey, $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchNonCachedLeaseByIntegrationDatabaseIdOrNullByRemotePrimaryKeyByPropertyIdByCid( $this->m_intId, $strRemotePrimaryKey, $intPropertyId, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomerByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIntegrationDatabaseIdByRemotePrimaryKeyByCid( $this->m_intId, $strRemotePrimaryKey, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomersByRemotePrimaryKeys( $arrstrRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIntegrationDatabaseIdByRemotePrimaryKeysByCid( $this->m_intId, $arrstrRemotePrimaryKey, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomerByCompanyNameBySecondaryNumber( $strCompanyName, $strSecondaryNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByCompanyNameBySecondaryNumberByIntegrationDatabaseIdByCid( $strCompanyName, $strSecondaryNumber, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerByFullNameFirstByFullNameLastBySecondaryNumber( $strNameFirst, $strNameLast, $strSecondaryNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByFullNameFirstByFullNameLastBySecondaryNumberByIntegrationDatabaseIdByCid( $strNameFirst, $strNameLast, $strSecondaryNumber, $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomerByCompanyNameByTaxNumber( $strCompanyName, $strTaxNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByCompanyNameByTaxNumberByIntegrationDatabaseIdByCid( $strCompanyName, $strTaxNumber, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerByCompanyNameByBirthDate( $strCompanyName, $strBirthDate, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByCompanyNameByBirthDateByIntegrationDatabaseIdByCid( $strCompanyName, $strBirthDate, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceStatusByName( $strName, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchMaintenanceStatusByCidByNameByIntegrationDatabaseId( $this->m_intCid, trim( $strName ), $this->m_intId, $objDatabase );
	}

	public function fetchMaintenanceStatusesKeyedByName( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchMaintenanceStatusesByCidByIntegrationDatabaseIdKeyedByName( $this->m_intCid, $this->m_intId, $objDatabase );
	}

	public function fetchMaintenanceProblemsKeyedByName( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchMaintenanceProblemByCidByIntegrationDatabaseIdKeyedByName( $this->m_intCid, $this->m_intId, $objDatabase );
	}

	public function fetchMaintenanceProblemByName( $strName, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchMaintenanceProblemByCidByIntegrationDatabaseIdByName( $this->m_intCid, $this->m_intId, $strName, $objDatabase );
	}

	public function fetchMaintenanceProblemByMaintenanceProblemTypeIdByName( $intMaintenanceProblemTypeId, $strName, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchMaintenanceProblemByCidByIntegrationDatabaseIdByMaintenanceProblemTypeIdByName( $this->m_intCid, $this->m_intId, $intMaintenanceProblemTypeId, $strName, $objDatabase );
	}

	public function fetchLeaseCustomersByPropertyIdsKeyedByCustomerIdByLeaseId( $arrintPropertyIds, $objDatabase ) {
		//@TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByIntegrationDatabaseIdByPropertyIdsKeyedByCustomerIdLeaseIdByCid( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchLeaseCustomersByPropertyIdsByCid( $arrintPropertyIds, $objDatabase ) {
		//@TODO Test it thoroughly in second phase refactoring
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByIntegrationDatabaseIdPropertyIdsByCid( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchLeaseCustomersByPropertyIdsByLeaseIdsByCid( $arrintPropertyIds, $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByIntegrationDatabaseIdByPropertyIdsByLeaseIdsByCid( $this->m_intId, $arrintPropertyIds, $arrintLeaseIds, $this->m_intCid, $objDatabase );
	}

	public function fetchLeaseCustomersByRemotePrimaryKeys( $arrstrRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomersByIntegrationDatabaseIdByRemotePrimaryKeysByCid( $this->m_intId, $arrstrRemotePrimaryKey, $this->m_intCid, $objDatabase );
	}

	public function fetchLeasesByRemotePrimaryKeys( $arrstrRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByIntegrationDatabaseIdByRemotePrimaryKeysByCid( $this->m_intId, $arrstrRemotePrimaryKey, $this->m_intCid, $objDatabase );
	}

	public function fetchNonRenewalBlacklistedLeasesByRemotePrimaryKeys( $arrstrRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchNonRenewalBlacklistedLeasesByIntegrationDatabaseIdByRemotePrimaryKeysByCid( $this->m_intId, $arrstrRemotePrimaryKey, $this->m_intCid, $objDatabase );
	}

	public function fetchLeasesByPropertyIdsKeyedByRemotePrimaryKey( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByIntegrationDatabaseIdByPropertyIdsKeyedByRemotePrimaryKeyByCid( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomLeasesByPropertyIdsKeyedByRemotePrimaryKey( $arrintPropertyIds, $objDatabase ) {
	    return \Psi\Eos\Entrata\CLeases::createService()->fetchCustomLeasesByIntegrationDatabaseIdByPropertyIdsKeyedByRemotePrimaryKeyByCid( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchLeasesByRemotePrimaryKeysByPropertyIdsKeyedByRemotePrimaryKey( $arrstrLeaseRemotePrimaryKeys, $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByIntegrationDatabaseIdByRemotePrimaryKeysByPropertyIdsKeyedByRemotePrimaryKeyByCid( $this->m_intId, $arrstrLeaseRemotePrimaryKeys, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomersByPropertyIdsKeyedByRemotePrimaryKey( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIntegrationDatabaseIdByPropertyIdsKeyedByRemotePrimaryKeyByCid( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchViewCustomersByOldPropertyIdsKeyedByRemotePrimaryKey( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchViewCustomersByIntegrationDatabaseIdByPropertyIdsByCidKeyedByRemotePrimaryKey( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomersByOldPropertyIdsKeyedByRemotePrimaryKey( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIntegrationDatabaseIdByPropertyIdsByCidKeyedByRemotePrimaryKey( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchIntegratedCustomersByOldPropertyIdsByLeaseIdsKeyedById( $arrintPropertyIds, $arrintLeaseIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomersByIntegrationDatabaseIdByPropertyIdsByLeaseIdsByCidKeyedById( $this->m_intId, $arrintPropertyIds, $arrintLeaseIds, $this->m_intCid, $objDatabase );
	}

	public function fetchIntegratedCustomerIdsWithNoLeasecustomersKeyedByRemotePrimaryKey( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchIntegratedCustomerIdsByIntegrationDatabaseIdByCidKeyedByRemotePrimaryKey( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomersWithVehiclesByOldPropertyIdsKeyedByRemotePrimaryKey( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersWithVehiclesByIntegrationDatabaseIdByPropertyIdsByCidKeyedByRemotePrimaryKey( $this->m_intId, $arrintPropertyIds, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomersKeyedByRemotePrimaryKey( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIntegrationDatabaseIdKeyedByRemotePrimaryKeyByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchIntegratedSpecials( $objDatabase ) {
		return \Psi\Eos\Entrata\CSpecials::createService()->fetchIntegratedSpecialsByIntegrationDatabaseIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchLeaseCustomerByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByRemotePrimaryKeyByIntegrationDatabaseIdByCid( $strRemotePrimaryKey, $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchCustomersByPropertyIdsByLeaseStatusTypeIdsKeyedByRemotePrimaryKey( $arrintPropertyIds, $arrintLeaseStatusTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIntegrationDatabaseIdByPropertyIdsByLeaseStatusTypeIdsKeyedByRemotePrimaryKeyByCid( $this->m_intId, $arrintPropertyIds, $arrintLeaseStatusTypeIds, $this->m_intCid, $objDatabase );
	}

	public function fetchLeasesByPropertyIdByLeaseStatusTypeIdsWithRemotePrimaryKeyNotNullKeyedByRemotePrimaryKey( $intPropertyId, $arrintLeaseStatusTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByIntegrationDatabaseIdByPropertyIdByLeaseStatusTypeIdsWithRemotePrimaryKeyNotNullKeyedByRemotePrimaryKeyByCid( $this->getId(), $intPropertyId, $arrintLeaseStatusTypeIds, $this->m_intCid, $objDatabase );
	}

	public function fetchAmenityRateAssociationsByArCascadeIdsByAmenityTypeId( $arrintArCascadeIds, $intAmenityTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArCascadeIdsByAmenityTypeIdByIntegrationDatabaseIdByCid( $arrintArCascadeIds, $intAmenityTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationsByArCascadeIds( $arrintArCascadeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArCascadeIdsByIntegrationDatabaseIdByCid( $arrintArCascadeIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchArCodeByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {

		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setRemotePrimaryKey( $strRemotePrimaryKey );
		$objArCodesFilter->setIntegrationDatabaseId( $this->getId() );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getCid(), $objArCodesFilter, $objDatabase );
	}

	public function fetchArCodesByRemotePrimaryKeys( $arrstrRemotePrimaryKeys, $objDatabase ) {
		if( false == valArr( $arrstrRemotePrimaryKeys ) ) return NULL;
		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByRemotePrimaryKeysByCid( $arrstrRemotePrimaryKeys, $this->getCid(), $objDatabase );
	}

	public function fetchArCodeByName( $strName, $objDatabase ) {
		$objArCodesFilter = new CArCodesFilter();
		$objArCodesFilter->setIntegrationDatabaseId( $this->getId() );
		$objArCodesFilter->setName( $strName );

		return \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByCidByArCodesFilter( $this->getCid(), $objArCodesFilter, $objDatabase );
	}

	public function fetchUnitTypes( $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitTypes::createService()->fetchUnitTypesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPreferencesByKeys( $arrstrKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeysByIntegrationDatabaseIdByCid( $arrstrKeys, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPreferencesByKeysValues( $arrstrKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeysValuesByIntegrationDatabaseIdByCid( $arrstrKeys, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyPreferencesByPropertyIdsByKeys( $arrintPropertyIds, $arrstrKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByPropertyIdsByKeysByIntegrationDatabaseIdByCid( $arrintPropertyIds, $arrstrKeys, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseCustomerVehiclesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CLeaseCustomerVehicles::fetchLeaseCustomerVehiclesByIntegrationDatabaseIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationServiceLog( $intWebServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationServiceLogs::createService()->fetchIntegrationServiceLogByIntegrationDatabaseIdByIntegrationServiceIdByLogDateByCid( $this->getId(), $intWebServiceId, $this->m_intCid, $objDatabase );

	}

	public function fetchMaintenancePriorities( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenancePriorities::createService()->fetchMaintenancePrioritiesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeadSources( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplans( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyFloorplans::createService()->fetchPropertyFloorplansByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplansByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyFloorplans::createService()->fetchPropertyFloorplansByPropertyIdsByCid( $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloors( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyFloors::createService()->fetchPropertyFloorsByPropertyIdsByIntegrationDatabaseIdByCid( $arrintPropertyIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildings( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchPropertyBuildingsByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyTransmissionVendorsByTransmissionTypeId( $intTransmissionTypeId, $objDatabase ) {
		return CPropertyTransmissionVendors::fetchPropertyTransmissionVendorsByIntegrationDatabaseIdByTransmissionTypeIdByCid( $this->getId(), $intTransmissionTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceRequestsByRemotePrimaryKeys( $arrstrMaintenanceRequestRPKs, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchMaintenanceRequestsByRemotePrimaryKeysByIntegrationDatabaseIdByCid( $arrstrMaintenanceRequestRPKs, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnits( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsByPropertyIdsByIntegrationDatabaseIdByCid( $arrintPropertyIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaces( $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpacesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpacesByPropertyIdsByIntegrationDatabaseIdByCid( $arrintPropertyIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingsByPropertyIds( $objDatabase, $arrintPropertyIds = NULL ) {
		return \Psi\Eos\Entrata\CPropertyBuildings::createService()->fetchPropertyBuildingsByPropertyIdsByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintPropertyIds );
	}

	public function fetchUnitTypesByPropertyIds( $objDatabase, $arrintPropertyIds = NULL, $boolIncludeDeletedUnitTypes = false ) {
		return \Psi\Eos\Entrata\CUnitTypes::createService()->fetchUnitTypesByPropertyIdsByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintPropertyIds, $boolIncludeDeletedUnitTypes );
	}

	public function fetchPropertyLeasingAgentsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentsByIntegrationDatabaseIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CApplications::fetchApplicationsByIntegrationDatabaseIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationsByLeaseRemotePrimaryKeysByLeaseIntervalTypeIdsByLeaseStatusTypeIds( $arrstrLeaseRemotePrimaryKeys, $arrintLeaseIntervalTypeIds, $arrintLeaseStatusTypeIds, $objDatabase ) {
		return CApplications::fetchApplicationsByIntegrationDatabaseIdByLeaseRemotePrimaryKeysByLeaseIntervalTypeIdsByLeaseStatusTypeIdsByCid( $this->m_intId, $arrstrLeaseRemotePrimaryKeys, $this->m_intCid, $arrintLeaseIntervalTypeIds, $arrintLeaseStatusTypeIds, $objDatabase );
	}

	public function fetchPropertyUtilities( $objDatabase ) {
		return CPropertyUtilities::fetchPropertyUtilitiesByIntegrationDatabaseIdByCid( $this->getCid(), $this->m_intId, $objDatabase );
	}

	public function fetchCompanyUserGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserGroupsByIntegrationDatabaseIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyGroupByCompanyGroupName( $strCompanyGroupName, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupByGroupNameByIntegrationDatabaseIdByCid( $strCompanyGroupName, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApPayeesByApPayeeTypeIds( $arrintApPayeeTypeIds, $objClientDatabase ) {
		return CApPayees::fetchIntegratedApPayeesByApPayeeTypeIdsByIntegrationDatabaseIdByCid( $this->getId(), $arrintApPayeeTypeIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchPropertyApplicationsByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {
		return CPropertyApplications::fetchPropertyApplicationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase );
	}

	public function fetchIntegratedBatchesByLastSyncOn( $strLastSyncOn, $objDatabase ) {
		return CArPayments::fetchIntegratedBatchesByLastSyncOnByIntegrationDatabaseIdByCid( $strLastSyncOn, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchActiveIntegrationClientByServiceIdByIntegrationDatabaseId( $intIntegrationServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchActiveIntegrationClientByServiceIdByIntegrationDatabaseIdByCid( $intIntegrationServiceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	* Other Functions
	*
	*/

	public function pingIntegrationDatabase( $intPropertyId, $objDatabase ) {

		$objIntegrationClient = \Psi\Eos\Entrata\CIntegrationClients::createService()->fetchIntegrationClientByPropertyIdByIntegrationServiceIdByCid( $intPropertyId, CIntegrationService::GET_PING, $this->m_intCid, $objDatabase );

		if( false == valObj( $objIntegrationClient, 'CIntegrationClient' ) ) {
			return true;
		}

		$objGenericClientWorker = CIntegrationFactory::createWorkerFromClient( $objIntegrationClient, SYSTEM_USER_ID, $objDatabase );

		$boolSuccess = $objGenericClientWorker->process();

		return $boolSuccess;
	}

	public function deleteNonIntegratedPropertyMaintenanceProblemsByPropertyIds( $arrintPropertyIds, $arrintDeletePropertyMaintenanceProblems, $intCompanyUserId, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return true;
		if( false == valArr( $arrintDeletePropertyMaintenanceProblems ) ) return true;

		// If some of the maintenance problems deleted from integrated application then we have to delete those property associations of the company maintenance problem.
		$arrobjNonIntegratedPropertyMaintenanceProblems = \Psi\Eos\Entrata\CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemsByPropertyIdsByIdsByCid( $arrintPropertyIds, $arrintDeletePropertyMaintenanceProblems, $this->m_intCid, $objDatabase );

		$boolIsValid = true;

		if( true == valArr( $arrobjNonIntegratedPropertyMaintenanceProblems ) ) {

			foreach( $arrobjNonIntegratedPropertyMaintenanceProblems as $objPropertyMaintenanceProblem ) {
				if( false == $objPropertyMaintenanceProblem->delete( $intCompanyUserId, $objDatabase ) ) {
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function deleteNonIntegratedPropertyMaintenanceStatusTypesByPropertyIds( $arrintPropertyIds, $arrintDeletePropertyMaintenanceStatusTypes, $intCompanyUserId, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return true;
		if( false == valArr( $arrintDeletePropertyMaintenanceStatusTypes ) ) return true;

		// If some of the maintenance problems deleted from integrated application then we have to delete those property associations of the company maintenance problem.
		$arrobjNonIntegratedPropertyMaintenanceStatusTypes = \Psi\Eos\Entrata\CPropertyMaintenanceStatuses::createService()->fetchPropertyMaintenanceStatusesByPropertyIdsByIdsByCid( $arrintPropertyIds, $arrintDeletePropertyMaintenanceStatusTypes, $this->m_intCid, $objDatabase );

		$boolIsValid = true;

		if( true == valArr( $arrobjNonIntegratedPropertyMaintenanceStatusTypes ) ) {

			foreach( $arrobjNonIntegratedPropertyMaintenanceStatusTypes as $objPropertyMaintenanceStatusType ) {
				if( false == $objPropertyMaintenanceStatusType->delete( $intCompanyUserId, $objDatabase ) ) {
					$boolIsValid = false;
				}
			}
		}

		return $boolIsValid;
	}

	public function getOrFetchIntegrationClientTypeName( $objDatabase ) {

		if( false == valStr( $this->m_strIntegrationClientTypeName ) ) {

			$strSql = 'SELECT name FROM integration_client_types WHERE id = ' . $this->getIntegrationClientTypeId();

			$objIntegrationClientType = \Psi\Eos\Entrata\CIntegrationClientTypes::createService()->fetchIntegrationClientType( $strSql, $objDatabase );
			$this->m_strIntegrationClientTypeName = $objIntegrationClientType->getName();
		}

		return $this->m_strIntegrationClientTypeName;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false === parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) return false;

		$arrstrRequestIntegrationDatabases = $this->objectToArrayMapping();
		$arrstrSubmittedKeys 	= array_keys( $arrstrRequestIntegrationDatabases );

		$strIntegrationDatabaseNewKeyValuePair = '';
		// Code added for formation of Integration Database key/value pair string -- starts
		if( true == valArr( $arrstrSubmittedKeys ) ) {
			foreach( $arrstrSubmittedKeys as $strKey => $strValue ) {
				$strNewKeyValue = ( true == array_key_exists( $strValue, $arrstrRequestIntegrationDatabases ) ) ? $arrstrRequestIntegrationDatabases[$strValue] : '';
				$strIntegrationDatabaseNewKeyValuePair .= $strValue . '::' . $strNewKeyValue . '~^~';
			}
		}
		// Code added for formation of Integration Database key/value pair string -- ends

		$objTableLog = new CTableLog();
		$objTableLog->insert( $intCurrentUserId, $objDatabase, 'integration_databases', $this->getId(), 'INSERT', $this->getCid(), NULL, $strIntegrationDatabaseNewKeyValuePair );

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objCurrentIntegrationDatabase = NULL ) {

		if( false === parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) )
			return false;

		if( true == valObj( $objCurrentIntegrationDatabase, 'CIntegrationDatabase' ) ) {
			// new values
			$arrstrRequestIntegrationDatabases = $this->objectToArrayMapping();
			// old values
			$arrstrCurrentIntegrationDatabase = $objCurrentIntegrationDatabase->objectToArrayMapping();

			$arrstrDifferenceInArrays = array_diff_assoc( $arrstrRequestIntegrationDatabases, $arrstrCurrentIntegrationDatabase );

			if( true == valArr( $arrstrDifferenceInArrays ) ) {
				$arrstrExistingKeys  	= array_keys( $arrstrCurrentIntegrationDatabase );
				$arrstrSubmittedKeys 	= array_keys( $arrstrRequestIntegrationDatabases );

				$arrstrUnionIntegrationDatabasesKeys = array_unique( array_merge( $arrstrExistingKeys, $arrstrSubmittedKeys ) );
				$strIntegrationDatabaseNewKeyValuePair = '';
				$strIntegrationDatabaseOldKeyValuePair = '';

				// Code added for formation of Integration Database key/value pair string -- starts
				if( true == valArr( $arrstrUnionIntegrationDatabasesKeys ) ) {
					foreach( $arrstrUnionIntegrationDatabasesKeys as $intKey => $strValue ) {
						$strNewKeyValue = ( true == array_key_exists( $strValue, $arrstrRequestIntegrationDatabases ) ) ? $arrstrRequestIntegrationDatabases[$strValue] : '';

						$strIntegrationDatabaseNewKeyValuePair .= $strValue . '::' . $strNewKeyValue . '~^~';
						$strOldKeyValue = ( true == array_key_exists( $strValue, $arrstrCurrentIntegrationDatabase ) ) ? $arrstrCurrentIntegrationDatabase[$strValue] : '';

						$strIntegrationDatabaseOldKeyValuePair .= $strValue . '::' . $strOldKeyValue . '~^~';
					}
				}
				// Code added for formation of Integration Database key/value pair string -- ends

				$objTableLog = new CTableLog();
				$objTableLog->insert( $intCurrentUserId, $objDatabase, 'integration_databases', $this->getId(), 'UPDATE', $this->getCid(), $strIntegrationDatabaseOldKeyValuePair, $strIntegrationDatabaseNewKeyValuePair, 'An integration database has been modified.' );
				return true;
			}
		}
		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		// log delete action to table_logs
		$arrstrCurrentIntegrationDatabase = $this->objectToArrayMapping();
		$arrstrExistingKeys  	= array_keys( $arrstrCurrentIntegrationDatabase );
		$strIntegrationDatabaseOldKeyValuePair = '';

		// Code added for formation of Integration Database key/value pair string -- starts
		if( true == valArr( $arrstrExistingKeys ) ) {
			foreach( $arrstrExistingKeys as $intKey => $strValue ) {
				$strOldKeyValue = ( true == array_key_exists( $strValue, $arrstrCurrentIntegrationDatabase ) ) ? $arrstrCurrentIntegrationDatabase[$strValue] : '';
				$strIntegrationDatabaseOldKeyValuePair .= $strValue . '::' . $strOldKeyValue . '~^~';
			}
		}
		// Code added for formation of Integration Database key/value pair string -- ends

		if( false === parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) )
			return false;
		$objTableLog = new CTableLog();
		$objTableLog->insert( $intCurrentUserId, $objDatabase, 'integration_databases', $this->getId(), 'DELETE', $this->getCid(), $strIntegrationDatabaseOldKeyValuePair, NULL );

		return true;
	}

	public function objectToArrayMapping() {

		$arrstrIntegrationDatabase = [];
		$arrstrIntegrationDatabase['id'] = $this->getId();
		$arrstrIntegrationDatabase['integration_version_id'] = $this->getIntegrationVersionName();
		$arrstrIntegrationDatabase['cid'] = $this->getCid();
		$arrstrIntegrationDatabase['integration_client_type_id'] = $this->getIntegrationClientTypeId();
		$arrstrIntegrationDatabase['integration_client_status_type_id'] = $this->getIntegrationClientStatusTypeId();
		$arrstrIntegrationDatabase['integration_version_id'] = $this->getIntegrationVersionId();
		$arrstrIntegrationDatabase['name'] = $this->getName();
		$arrstrIntegrationDatabase['description'] = $this->getDescription();
		$arrstrIntegrationDatabase['database_name'] = $this->getDatabaseName();
		$arrstrIntegrationDatabase['dev_base_uri'] = $this->getDevBaseUri();
		$arrstrIntegrationDatabase['dev_database_name'] = $this->getDevDatabaseName();
		$arrstrIntegrationDatabase['dev_username_encrypted'] = $this->getDevUsernameEncrypted();
		$arrstrIntegrationDatabase['dev_password_encrypted'] = $this->getDevPasswordEncrypted();
		$arrstrIntegrationDatabase['server_name'] = $this->getDatabaseServerName();
		$arrstrIntegrationDatabase['platform'] = $this->getPlatform();
		$arrstrIntegrationDatabase['base_uri'] = $this->getBaseUri();
		$arrstrIntegrationDatabase['username_encrypted'] = $this->getUsernameEncrypted();
		$arrstrIntegrationDatabase['password_encrypted'] = $this->getPasswordEncrypted();
		$arrstrIntegrationDatabase['database_server_name'] = $this->getDatabaseServerName();
		$arrstrIntegrationDatabase['database_username_encrypted'] = $this->getDatabaseUsernameEncrypted();
		$arrstrIntegrationDatabase['database_password_encrypted'] = $this->getDatabasePasswordEncrypted();

		return $arrstrIntegrationDatabase;
	}

	public function retrieveVersionStatus( $arrmixIntegrationUtilityReleasedVersions, $boolOnlyForVersion = true ) {

		$intIntegrationClientTypeId = $this->getIntegrationClientTypeId();
		$strCurrentVersion			= $this->getIntegrationVersionName();

		if( CIntegrationUtilityReleasedVersion::YARDI_CLIENT == $intIntegrationClientTypeId ) {
			if( true == preg_match( '/(\w*genesis\w*)/i', $strCurrentVersion ) ) {
				$strRelasedVersion = $arrmixIntegrationUtilityReleasedVersions[CIntegrationUtilityReleasedVersion::YARDI_GENESIS];
			} elseif( true == preg_match( '/(\w*enterpri\w*)/i', $strCurrentVersion ) ) {
				$strRelasedVersion = $arrmixIntegrationUtilityReleasedVersions[CIntegrationUtilityReleasedVersion::YARDI_ENTERPRIZE];
			} else {
				$strRelasedVersion = $arrmixIntegrationUtilityReleasedVersions[$intIntegrationClientTypeId];
			}
		} else {
			$strRelasedVersion = $arrmixIntegrationUtilityReleasedVersions[$intIntegrationClientTypeId];
		}

		$strRelasedVersion = trim( preg_replace( '![a-z]*!i', '', $strRelasedVersion ) );
		$strRelasedVersion = trim( preg_replace( '![^0-9\.][-\s]*.*!i', '', $strRelasedVersion ) );
		$strCurrentVersion = trim( preg_replace( '![a-z]*!i', '',  $strCurrentVersion ) );
		$strCurrentVersion = trim( preg_replace( '![^0-9\.][-\s]*.*!i', '', $strCurrentVersion ) );

		$strOriginalRelasedVersion = trim( $strRelasedVersion );
		$strOriginalCurrentVersion = trim( $strCurrentVersion );

		$arrstrRelasedVersion = explode( '.', $strRelasedVersion );
		$arrstrCurrentVersion = explode( '.', $strCurrentVersion );

		if( \Psi\Libraries\UtilFunctions\count( $arrstrRelasedVersion ) > \Psi\Libraries\UtilFunctions\count( $arrstrCurrentVersion ) ) {
			for( $intY = 0; $intY < ( \Psi\Libraries\UtilFunctions\count( $arrstrRelasedVersion ) - \Psi\Libraries\UtilFunctions\count( $arrstrCurrentVersion ) ); $intY++ ) {
				$arrstrCurrentVersion[] = '00';
			}
		} else {
			for( $intY = 0; $intY < ( \Psi\Libraries\UtilFunctions\count( $arrstrCurrentVersion ) - \Psi\Libraries\UtilFunctions\count( $arrstrRelasedVersion ) ); $intY++ ) {
				$arrstrRelasedVersion[] = '00';
			}
		}

		$intLoopLength = \Psi\Libraries\UtilFunctions\count( $arrstrRelasedVersion );
		$intStatus 	   = NULL;

		for( $intI = 0; $intI < $intLoopLength; $intI++ ) {
			if( ( int ) $arrstrRelasedVersion[$intI] > ( int ) $arrstrCurrentVersion[$intI] && true == is_null( $intStatus ) ) {
				$intStatus	= 1;
			} elseif( ( int ) $arrstrCurrentVersion[$intI] > ( int ) $arrstrRelasedVersion[$intI] && true == is_null( $intStatus ) ) {
				$intStatus	= 0;
			}
		}

		$arrstrCompareRelasedVersion = $arrstrRelasedVersion;
		$arrstrCompareCurrentVersion = $arrstrCurrentVersion;

		if( 1 == $intStatus ) {
			for( $intI = 0; $intI < $intLoopLength; $intI++ ) {
				if( \Psi\CStringService::singleton()->strlen( $arrstrCompareRelasedVersion[$intI] ) !== \Psi\CStringService::singleton()->strlen( $arrstrCompareCurrentVersion[$intI] ) ) {
					if( \Psi\CStringService::singleton()->strlen( $arrstrCompareRelasedVersion[$intI] ) < \Psi\CStringService::singleton()->strlen( $arrstrCompareCurrentVersion[$intI] ) ) {
						$arrstrCompareRelasedVersion[$intI] = \Psi\CStringService::singleton()->str_pad( $arrstrCompareRelasedVersion[$intI], \Psi\CStringService::singleton()->strlen( $arrstrCompareCurrentVersion[$intI] ), 0, STR_PAD_LEFT );
					} else {
						$arrstrCompareCurrentVersion[$intI] = \Psi\CStringService::singleton()->str_pad( $arrstrCompareCurrentVersion[$intI], \Psi\CStringService::singleton()->strlen( $arrstrCompareRelasedVersion[$intI] ), 0, STR_PAD_LEFT );
					}
				}
			}

			$strCompareReleasedVersion = implode( '.', $arrstrCompareRelasedVersion );
			$strCompareCurrentVersion  = implode( '.', $arrstrCompareCurrentVersion );

			$strCompareReleasedVersion = str_replace( '.', '', $strCompareReleasedVersion );
			$strCompareCurrentVersion = str_replace( '.', '', $strCompareCurrentVersion );

			$intStatus = ( ( $strCompareReleasedVersion - $strCompareCurrentVersion ) <= 50 ? 1 : 2 );
		}

		if( true == $boolOnlyForVersion ) {
			return $intStatus;
		} else {
			$arrstrVersionDetails = [
				'version_status'   => $intStatus,
				'current_version'  => $strOriginalCurrentVersion,
				'released_version' => $strOriginalRelasedVersion
			];
			if( $intStatus !== 0 ) {
				$arrstrVersionDetails['difference'] = ( $strCompareReleasedVersion - $strCompareCurrentVersion );
			}

			return $arrstrVersionDetails;
		}
	}

	public function fetchApPayeeByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		return CApPayees::fetchApPayeeByRemotePrimaryKeyByIntegrationDatabaseIdByCid( $strRemotePrimaryKey, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function checkIsIONCredential() : bool {
		return ( is_object( $this->getDetailsField( [ 'additional_details', 'edex', 'ion_credentials' ] ) ) && !empty( $this->getDetailsField( [ 'additional_details', 'edex', 'ion_credentials' ] ) ) ) ? true : false;
	}

}
?>
