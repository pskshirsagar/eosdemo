<?php

class CCompanyFileKeyword extends CBaseCompanyFileKeyword {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valKeyword( $objDatabase = NULL ) {
        $boolIsValid = true;

        if( false == valStr( $this->getKeyword() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', 'Company File keyword is required.' ) );
        	return $boolIsValid;
        }

        if( false == is_null( $objDatabase ) ) {

        	$intConfilictingCompanyFileKeywordCount = CCompanyFileKeywords::fetchConflictingCompanyFileKeywordCount( $this, $objDatabase );

        	if( 0 < $intConfilictingCompanyFileKeywordCount ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', 'Company File keyword is already in use.' ) );
        		return $boolIsValid;
        	}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valKeyword( $objDatabase );
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>