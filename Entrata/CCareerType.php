<?php

class CCareerType extends CBaseCareerType {

	protected $m_arrobjCareer;

    /**
     * Get Functions
     */

    public function getCareer() {
    	return $this->m_arrobjCareer;
    }

    /**
     * Set Functions
     */

    public function setCareer( $objCareer ) {
    	$this->m_arrobjCareer[$objCareer->getId()] = $objCareer;
    }

    /**
     * Validate Functions
     */

	public function valId( $objDatabase = NULL ) {

        $boolIsValid = true;

        if( true == isset( $objDatabase ) ) {
			$intCountCareerType = \Psi\Eos\Entrata\CCareers::createService()->fetchCareersCountByCareerTypeIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
		}

		if( 0 < $intCountCareerType ) {
			$boolIsValid = false;
       		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Cannot be deleted/unpublished, it is being associated with position. You need to disassociate from position to delete/unpublished. ' ) ) );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valName() {

    	$boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'The position type name must have at least 1 character.' ) ) );
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
            	break;

			case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId( $objDatabase );
            	break;

			default:
            	// No comment.
            	break;
        }

        return $boolIsValid;
    }

}
?>