<?php

class CCustomerVehicleType extends CBaseCustomerVehicleType {

	const PRIMARY 				= 1;
	const SECONDARY 			= 2;
	const OTHER 				= 3;

}
?>