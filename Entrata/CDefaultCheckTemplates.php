<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCheckTemplates
 * Do not add any new functions to this class.
 */

class CDefaultCheckTemplates extends CBaseDefaultCheckTemplates {

	public static function fetchDefaultCheckTemplate( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultCheckTemplate', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>