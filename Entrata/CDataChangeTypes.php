<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataChangeTypes
 * Do not add any new functions to this class.
 */
class CDataChangeTypes extends CBaseDataChangeTypes {

	public static function fetchDataChangeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDataChangeType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDataChangeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDataChangeType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>