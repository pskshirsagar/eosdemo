<?php

class CEmailTemplateCustomText extends CBaseEmailTemplateCustomText {

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valScheduledEmailId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getScheduledEmailId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEmailTemplateId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getEmailTemplateId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_template_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEmailTemplateSlotId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getEmailTemplateSlotId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_template_slot_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valKey() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getKey() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', '' ) );
        // }

        return $boolIsValid;
    }

    public function valValue() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getValue() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>