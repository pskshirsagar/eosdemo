<?php

class CPropertyProduct extends CBasePropertyProduct {

	protected $m_intAppId;
	protected $m_strPsProductName;

    /**
     * Set Function
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrValues['app_id'] ) )		$this->setAppId( $arrValues['app_id'] );
    	return;
    }

    public function setAppId( $intAppId ) {
    	$this->m_intAppId = $intAppId;
    }

    public function getAppId() {
    	return $this->m_intAppId;
    }

    public function setPsProductName( $strPsProductName ) {
    	$this->m_strPsProductName = $strPsProductName;
    }

    public function getPsProductName() {
    	return $this->m_strPsProductName;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
       $boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
		}

		return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        if( true == is_null( $this->getPropertyId() ) || 0 >= ( int ) $this->getPropertyId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Property id required.' ) );
        }
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPsProductId() ) || 0 >= ( int ) $this->getPsProductId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Product id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valPsProductOptionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            case 'validate_property_product':
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valPsProductId();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>