<?php

class CApplicantAssets extends CBaseApplicantAssets {

	public static function fetchApplicantAssetsByApplicantIdByApplicantAssetTypeIdsByCid( $intApplicantId, $arrintApplicantAssetTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicantAssetTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM applicant_assets WHERE applicant_id = ' . ( int ) $intApplicantId . ' AND cid = ' . ( int ) $intCid . ' AND applicant_asset_type_id IN ( ' . implode( ',', $arrintApplicantAssetTypeIds ) . ' ) ORDER BY applicant_asset_type_id';
		return self::fetchApplicantAssets( $strSql, $objDatabase );
	}

}
?>