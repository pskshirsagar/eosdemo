<?php

class CIntegrationPing extends CBaseIntegrationPing {

    /**
     * Set Functions
     */

    public function setDefaults() {

	    $this->setIntegrationClientPingStatus( '2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2' );
    	$this->setPingDatetime( date( 'Y-m-d' ) );
    }

    /**
     * Validate Functions
     */

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
        }

        return $boolIsValid;
    }

    public function valIntegrationClientId() {
        $boolIsValid = true;

        if( true == is_null( $this->getIntegrationClientId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_client_id', 'Integration client id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valIntegrationClientPingStatus() {
        $boolIsValid = true;

        if( true == is_null( $this->getIntegrationClientPingStatus() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integration_client_ping_status', 'Integration clinet ping status is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPingDateTime() {
        $boolIsValid = true;

        if( true == is_null( $this->getPingDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ping_datetime', 'Ping date is required.' ) );
        } elseif( false == is_null( $this->getPingDatetime() ) && false == CValidation::validateDate( $this->getPingDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ping_datetime', 'Ping date is not a valid date.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid	&= $this->valCid();
            	$boolIsValid	&= $this->valIntegrationClientId();
            	$boolIsValid	&= $this->valIntegrationClientPingStatus();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>