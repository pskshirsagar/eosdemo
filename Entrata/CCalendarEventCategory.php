<?php

class CCalendarEventCategory extends CBaseCalendarEventCategory {

	const GENERAL		= 1;
	const RESIDENT		= 2;
	const LEASING		= 3;
	const MAINTENANCE 	= 4;

	public static $c_arrintAllCalendarEventCategoryIds = [ CCalendarEventCategory::GENERAL, CCalendarEventCategory::RESIDENT, CCalendarEventCategory::LEASING, CCalendarEventCategory::MAINTENANCE ];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>