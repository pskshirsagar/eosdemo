<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserTokens
 * Do not add any new functions to this class.
 */

class CCompanyUserTokens extends CBaseCompanyUserTokens {

	public static function fetchCompanyUserTokensByTokenByCid( $strToken, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						company_user_tokens
					WHERE
						cid = ' . ( int ) $intCid . '
						AND token = \'' . $strToken . '\'
					LIMIT 1';

		return self::fetchCompanyUserToken( $strSql, $objDatabase );
	}

}
?>