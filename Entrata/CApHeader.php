<?php

class CApHeader extends CBaseApHeader {

	const LOOKUP_TYPE_INVOICES_VENDOR						= 'invoices_vendor';
	const LOOKUP_TYPE_ACCOUNTING_INVOICES					= 'accounting_invoices';
	const LOOKUP_TYPE_JOB_INVOICES					        = 'job_invoices';
	const LOOKUP_TYPE_PURCHASE_ORDERS_CREATED_BY			= 'purchase_orders_created_by';
	const LOOKUP_TYPE_PURCHASE_ORDERS_VENDOR				= 'purchase_orders_vendor';
	const LOOKUP_TYPE_PURCHASE_ORDERS_HEADER_NUMBER			= 'purchase_orders_header_number';
	const LOOKUP_TYPE_ATTACHED_PO_NUMBER					= 'attached_po_number';
	const LOOKUP_TYPE_FINANCIAL_TRANSACTION_INVOICE			= 'financial_transaction_invoice';
	const LOOKUP_TYPE_INVOICES_ACCOUNT_NUMBER				= 'invoices_account_number';
	const LOOKUP_TYPE_PURCHASE_ORDERS_PO_NUMBER				= 'purchase_orders_po_number';
	const LOOKUP_TYPE_VENDOR_PURCHASE_ORDERS_PO_NUMBER		= 'vendor_purchase_orders_po_number';

	protected $m_boolIsApproved;
	protected $m_boolIsExported;
	protected $m_boolIsModified;
	protected $m_boolIsAttachment;
	protected $m_boolIsAutoApproval;
	protected $m_boolIsConsolidated;
	protected $m_boolIsDisbursement;
	protected $m_boolIsRuleStopUpdated;
	protected $m_boolIsOldDisbursement;
	protected $m_boolIsOldReimbursement;
	protected $m_boolIsCcPaymentInvoice;
	protected $m_boolIsInitialImportPosted;
	protected $m_boolIsLocationDummyAccount;
	protected $m_boolIsAutoApprovedCreatorStop;

	protected $m_intApExceptionQueueItemId;
	protected $m_intJobId;
	protected $m_intStoreId;
	protected $m_intBuyerAccountId;
	protected $m_intAssetId;
	protected $m_intUnitTypeId;
	protected $m_intPropertyId;
	protected $m_intPoQuantity;
	protected $m_intCheckNumber;
	protected $m_intOldApHeaderId;
	protected $m_intApPayeeTypeId;
	protected $m_intApPaymentTypeId;
	protected $m_intBankAccountId;
	protected $m_intAutoProcessPayment;
	protected $m_intAutoSchedulePayment;
	protected $m_intApPayeeStatusTypeId;
	protected $m_intMaintenanceLocationId;
	protected $m_arrmixMigratedPropertyIds;
	protected $m_intApPayeeRequirePoForInvoice;
	protected $m_intCompanyRequirePosForInvoices;

	protected $m_fltPaidAmount;
	protected $m_fltPaymentAmount; // This will hold amount being paid for current invoice.
	protected $m_fltApprovedAmount;
	protected $m_fltReleasedAmount;
	protected $m_fltOriginalRetentionAmount;
	protected $m_fltUnallocatedAmount;

	protected $m_strCity;
	protected $m_strCountry;
	protected $m_strProvince;
	protected $m_strPaymentNumber;

	protected $m_strJobName;
	protected $m_strEndOn;
	protected $m_strUnitName;
	protected $m_strPoNumber;
	protected $m_strStateCode;
	protected $m_strPayeeName;
	protected $m_strVendorCode;
	protected $m_strPostalCode;
	protected $m_strApPayeeTerm;
	protected $m_strApPayeeName;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strJobPhaseName;
	protected $m_strApContractName;
	protected $m_strLocationName;
	protected $m_strApprovalNote;
	protected $m_strPropertyName;
	protected $m_strTemplateType;
	protected $m_strBulkUnitNumber;
	protected $m_strReversePostDate;
	protected $m_strPoApHeaderLabel;
	protected $m_strBankAccountName;
	protected $m_strApRoutingTagName;
	protected $m_strReversePostMonth;
	protected $m_strBulkJobPhaseName;
	protected $m_strBulkApContractName;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckNameOnAccount;
	protected $m_strCreateTemplateFrom;
	protected $m_strApPayeeLocationName;
	protected $m_strSecondaryNumber;
	protected $m_strRefundCustomerNames;
	protected $m_strVendorAccountNumber;
	protected $m_strApPayeeAccountNumber;
	protected $m_strReimbursedHeaderNumber;
	protected $m_strReimbursingPropertyName;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strCheckAccountNumber;
	protected $m_strCompanyName;

	protected $m_objApPayee;
	protected $m_objApPayment;
	protected $m_objEmailDatabase;
	protected $m_objApPayeeLocation;

	protected $m_arrintUtilityBillIds;
	protected $m_arrintBillPayPropertyIds;
	protected $m_arrintCompanyUserPropertyIds;

	protected $m_arrfltPaymentAmountsByApPaymentTypes; // This will hold amount being paid for current invoice by payment type.

	protected $m_arrstrPoApHeaderNumbers;

	/** @var CApDetail[] */
	protected $m_arrobjApDetails = [];
	protected $m_arrobjProperties;
	protected $m_arrobjSystemEmails;
	protected $m_arrobjApAllocations;
	protected $m_arrobjFileAssociations;
	protected $m_arrobjOriginalApHeaders;
	protected $m_arrobjAssetTransactions;
	protected $m_arrobjApPayeeProperties;
	protected $m_arrobjFailRoutedDetails;
	protected $m_arrobjPropertyGlSettings;
	protected $m_arrobjPropertyBankAccounts;
	protected $m_arrobjCompanyUserPreferences;
	protected $m_arrobjApHeaderLeaseCustomers;
	protected $m_arrobjFiles;

	protected $m_arrmixApCodeProperties;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRuleStopUpdated = true;
		$this->m_boolIsLocationDummyAccount = false;
		$this->m_arrobjOriginalApHeaders = [];
		$this->m_arrfltPaymentAmountsByApPaymentTypes = [];
		$this->m_boolIsAutoApprovedCreatorStop = false;

		return;
	}

	/**
	 * Create Functions
	 */

	public function createApDetail() {

		$objApDetail = new CApDetail();
		$objApDetail->setDefaults();
		$objApDetail->setApHeaderId( $this->getId() );
		$objApDetail->setCid( $this->getCid() );
		$objApDetail->setGlTransactionTypeId( CGlTransactionType::AP_CHARGE );
		$objApDetail->setApPhysicalStatusTypeId( CApPhysicalStatusType::NOT_ORDERED );
		$objApDetail->setTransactionDatetime( 'NOW()' );
		$objApDetail->setIsAllocation( 0 );
		$objApDetail->setIsPropertyGroup( 0 );

		return $objApDetail;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setDefaults();
		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setApHeaderId( $this->getId() );

		return $objFileAssociation;
	}

	public function createOrderHeader() {

		$objOrderHeader = new COrderHeader();

		$objOrderHeader->setApHeaderId( $this->getId() );
		$objOrderHeader->setCid( $this->getCid() );
		$objOrderHeader->setControlTotal( $this->getControlTotal() );
		$objOrderHeader->setCreatedBy( $this->getCreatedBy() );
		$objOrderHeader->setCreatedOn( $this->getCreatedOn() );
		$objOrderHeader->setDeletedBy( $this->getDeletedBy() );
		$objOrderHeader->setDeletedOn( $this->getDeletedOn() );
		$objOrderHeader->setDiscountAmount( $this->getDiscountAmount() );
		$objOrderHeader->setDueDate( $this->getDueDate() );
		$objOrderHeader->setHeaderMemo( $this->getHeaderMemo() );
		$objOrderHeader->setHeaderNumber( $this->getHeaderNumber() );
		$objOrderHeader->setApHeaderTypeId( $this->getApHeaderTypeId() );
		$objOrderHeader->setApHeaderSubTypeId( $this->getApHeaderSubTypeId() );
		$objOrderHeader->setApPhysicalStatusTypeId( $this->getApPhysicalStatusTypeId() );
		$objOrderHeader->setApFinancialStatusTypeId( $this->getApFinancialStatusTypeId() );
		$objOrderHeader->setShippingAmount( $this->getShippingAmount() );
		$objOrderHeader->setTaxAmount( $this->getTaxAmount() );
		$objOrderHeader->setTransactionAmount( $this->getTransactionAmount() );
		$objOrderHeader->setTransactionAmountDue( $this->getTransactionAmountDue() );
		$objOrderHeader->setTransactionDatetime( $this->getTransactionDatetime() );
		$objOrderHeader->setExternalUrl( $this->getExternalUrl() );
		$objOrderHeader->setIsPosted( $this->getIsPosted() );

		return $objOrderHeader;
	}

	/**
	 * Get Or Fetch Functions
	 */

	public function getOrFetchApDetails( $objClientDatabase ) {

		if( false == valArr( $this->m_arrobjApDetails ) ) {
			$this->m_arrobjApDetails = $this->fetchApDetails( $objClientDatabase );
		}
		$this->setIntercompanyFields();

		return $this->m_arrobjApDetails;
	}

	/**
	 * Get Functions
	 */

	public function getAutoSchedulePayment() {
		return $this->m_intAutoSchedulePayment;
	}

	// Overriding getHeaderDate and getHeaderDueDate function here to exclude time

	public function getHeaderDate() {
		$arrstrDateSlices = explode( ' ', parent::getHeaderDate() );
		return $arrstrDateSlices[0];
	}

	public function getHeaderDueDate() {
		$arrstrDateSlices = explode( ' ', parent::getHeaderDueDate() );
		return $arrstrDateSlices[0];
	}

	public function getAutoProcessPayment() {
		return $this->m_intAutoProcessPayment;
	}

	public function getApPaymentTypeId() {
		return $this->m_intApPaymentTypeId;
	}

	public function getPoNumber() {
		return $this->m_strPoNumber;
	}

	public function getApPayeeStatusTypeId() {
		return $this->m_intApPayeeStatusTypeId;
	}

	public function getApPayeeTypeId() {
		return $this->m_intApPayeeTypeId;
	}

	public function getPayeeName() {
		return $this->m_strPayeeName;
	}

	public function getApPayeeTerm() {
		return $this->m_strApPayeeTerm;
	}

	public function getUnallocatedAmount() {
		return $this->m_fltUnallocatedAmount;
	}

	public function getPaidAmount() {
		return $this->m_fltPaidAmount;
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function getApprovedAmount() {
		return $this->m_fltApprovedAmount;
	}

	public function getReleasedAmount() {
		return $this->m_fltReleasedAmount;
	}

	public function getOriginalRetentionAmount() {
		return $this->m_fltOriginalRetentionAmount;
	}

	public function getApPayee() {
		return $this->m_objApPayee;
	}

	/**
	 * @return CApPayment
	 */
	public function getApPayment() {
		return $this->m_objApPayment;
	}

	public function getEmailDatabase() {
		return $this->m_objEmailDatabase;
	}

	public function getApPayeeLocation() {
		return $this->m_objApPayeeLocation;
	}

	public function getReversePostMonth() {
		return $this->m_strReversePostMonth;
	}

	public function getBulkUnitNumber() {
		return $this->m_strBulkUnitNumber;
	}

	public function getReversePostDate() {
		return $this->m_strReversePostDate;
	}

	public function getFailRoutedDetails() {
		return $this->m_arrobjFailRoutedDetails;
	}

	public function getFormattedPostMonth() {

		if( true == is_null( $this->m_strPostMonth ) ) {
			return NULL;
		}

		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getFormattedReversePostMonth() {

		if( true == is_null( $this->m_strReversePostMonth ) ) {
			return NULL;
		}

		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strReversePostMonth );
	}

	public function getUtilityBillIds() {
		return $this->m_arrintUtilityBillIds;
	}

	public function getBillPayPropertyIds() {
		return $this->m_arrintBillPayPropertyIds;
	}

	public function getCompanyUserPropertyIds() {
		return $this->m_arrintCompanyUserPropertyIds;
	}

	public function getApDetails() {
		return $this->m_arrobjApDetails;
	}

	public function getApAllocations() {
		return $this->m_arrobjApAllocations;
	}

	// Added following function to make generic call to details in Ap Routing lib RSW 16-May-2013

	public function getDetails() {
		return $this->m_arrobjApDetails;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getPropertyGlSettings() {
		return $this->m_arrobjPropertyGlSettings;
	}

	public function getPropertyBankAccounts() {
		return $this->m_arrobjPropertyBankAccounts;
	}

	public function getApPayeeProperties() {
		return $this->m_arrobjApPayeeProperties;
	}

	public function getCompanyUserPreferences() {
		return $this->m_arrobjCompanyUserPreferences;
	}

	public function getFileAssociations() {
		return $this->m_arrobjFileAssociations;
	}

	public function getOriginalApHeaders() {
		return $this->m_arrobjOriginalApHeaders;
	}

	public function getApCodeProperties() {
		return $this->m_arrmixApCodeProperties;
	}

	public function getApPayeeName() {
		return $this->m_strApPayeeName;
	}

	public function getLocationName() {
		return $this->m_strLocationName;
	}

	public function getApPayeeLocationName() {
		return $this->m_strApPayeeLocationName;
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function getOldApHeaderId() {
		return $this->m_intOldApHeaderId;
	}

	public function getSystemEmails() {
		return $this->m_arrobjSystemEmails;
	}

	public function getIsRuleStopUpdated() {
		return $this->m_boolIsRuleStopUpdated;
	}

	public function getIsInitialImportPosted() {
		return $this->m_boolIsInitialImportPosted;
	}

	public function getIsApproved() {
		return $this->m_boolIsApproved;
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function getCountry() {
		return $this->m_strCountry;
	}

	public function getIsExported() {
		return $this->m_boolIsExported;
	}

	public function getIsAttachment() {
		return $this->m_boolIsAttachment;
	}

	public function getIsAutoApproval() {
		return $this->m_boolIsAutoApproval;
	}

	// Overriding getPostDate function here to exclude time

	public function getPostDate() {
		$arrstrDateSlices = explode( ' ', parent::getPostDate() );
		return $arrstrDateSlices[0];
	}

	// Overriding getDueDate function here to exclude time

	public function getDueDate() {
		$arrstrDateSlices = explode( ' ', parent::getDueDate() );
		return $arrstrDateSlices[0];
	}

	public function getApHeaderLeaseCustomers() {
		return $this->m_arrobjApHeaderLeaseCustomers;
	}

	public function getRefundCustomerNames() {
		return $this->m_strRefundCustomerNames;
	}

	public function getApPayeeRequirePoForInvoice() {
		return $this->m_intApPayeeRequirePoForInvoice;
	}

	public function getCompanySettingRequirePosForInvoices() {
		return $this->m_intCompanyRequirePosForInvoices;
	}

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	public function getEndOn() {
		return $this->m_strEndOn;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getJobName() {
		return $this->m_strJobName;
	}

	public function getApExceptionQueueItemId() {
		return $this->m_intApExceptionQueueItemId;
	}

	public function getJobId() {
		return $this->m_intJobId;
	}

	public function getJobPhaseName() {
		return $this->m_strJobPhaseName;
	}

	public function getApContractName() {
		return $this->m_strApContractName;
	}

	public function getReimbursingPropertyName() {
		return $this->m_strReimbursingPropertyName;
	}

	public function getAssetTransactions() {
		return $this->m_arrobjAssetTransactions;
	}

	public function getUnitName() {
		return $this->m_strUnitName;
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function getCheckAccountNumber() {
		return CEncryption::decryptText( $this->m_strCheckAccountNumberEncrypted, CONFIG_KEY_ACH_ACCOUNT_NUMBER );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function getCreateTemplateFrom() {
		return $this->m_strCreateTemplateFrom;
	}

	public function getTemplateType() {
		return $this->m_strTemplateType;
	}

	public function getAssetId() {
		return $this->m_intAssetId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPoQuantity() {
		return $this->m_intPoQuantity;
	}

	public function getApPhysicalStatusName() {
		return $this->m_strApPhysicalStatusName;
	}

	public function getApFinancialStatusName() {
		return $this->m_strApFinancialStatusName;
	}

	public function getApPayeeAccountNumber() {
		return $this->m_strApPayeeAccountNumber;
	}

	public function getReimbursedHeaderNumber() {
		return $this->m_strReimbursedHeaderNumber;
	}

	public function getPoApHeaderLabel() {
		return $this->m_strPoApHeaderLabel;
	}

	public function getApRoutingTagName() {
		return $this->m_strApRoutingTagName;
	}

	public function getVendorCode() {
		return $this->m_strVendorCode;
	}

	public function getBulkJobPhaseName() {
		return $this->m_strBulkJobPhaseName;
	}

	public function getBulkApContractName() {
		return $this->m_strBulkApContractName;
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function getPoApHeaderNumbers() {
		return $this->m_arrstrPoApHeaderNumbers;
	}

	public function getMigratedPropertyIds() {
		return $this->m_arrmixMigratedPropertyIds;
	}

	public function getPaymentAmountByApPaymentTypeId( $intApPayeeTypeId ) {
		$fltPaymentAmount = 0.00;

		if( true == isset( $this->m_arrfltPaymentAmountsByApPaymentTypes[$intApPayeeTypeId] ) ) {
			$fltPaymentAmount = $this->m_arrfltPaymentAmountsByApPaymentTypes[$intApPayeeTypeId];
		}

		return $fltPaymentAmount;
	}

	public function getIsLocationDummyAccount() {
		return $this->m_boolIsLocationDummyAccount;
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function getFiles() {
		return $this->m_arrobjFiles;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getVendorAccountNumber() {
		return $this->m_strVendorAccountNumber;
	}

	public function getIsConsolidated() {
		return $this->m_boolIsConsolidated;
	}

	public function getBuyerAccountId() {
		return $this->m_intBuyerAccountId;
	}

	public function getStoreId() {
		return $this->m_intStoreId;
	}

	public function getIsCcPaymentInvoice() {
		return $this->m_boolIsCcPaymentInvoice;
	}

	public function getIsDisbursement() {
		return $this->m_boolIsDisbursement;
	}

	public function getIsOldDisbursement() {
		return $this->m_boolIsOldDisbursement;
	}

	public function getIsOldReimbursement() {
		return $this->m_boolIsOldReimbursement;
	}

	public function getPaymentNumber() {
		return $this->m_strPaymentNumber;
	}

	public function getIsModified() {
		return $this->m_boolIsModified;
	}

	public function getIsAutoApprovedCreatorStop() {
		return $this->m_boolIsAutoApprovedCreatorStop;
	}

	/**
	 * Set Functions
	 */

	public function setErrorMsgs( $arrobjErrorMsgs ) {
		$this->m_arrobjErrorMsgs = $arrobjErrorMsgs;
	}

	public function setApDetails( $arrobjApDetails ) {
		$this->m_arrobjApDetails = $arrobjApDetails;
		$this->setIntercompanyFields();
	}

	public function setApAllocations( $arrobjApAllocations ) {
		$this->m_arrobjApAllocations = $arrobjApAllocations;
	}

	public function setDetails( $arrobjApDetails ) {
		$this->m_arrobjApDetails = $arrobjApDetails;
		$this->setIntercompanyFields();
	}

	public function setEndOn( $strEndOn ) {
		$this->m_strEndOn = $strEndOn;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setPropertyGlSettings( $arrobjPropertyGlSettings ) {
		$this->m_arrobjPropertyGlSettings = $arrobjPropertyGlSettings;
	}

	public function setPropertyBankAccounts( $arrobjPropertyBankAccounts ) {
		$this->m_arrobjPropertyBankAccounts = $arrobjPropertyBankAccounts;
	}

	public function setApPayeeProperties( $arrobjApPayeeProperties ) {
		$this->m_arrobjApPayeeProperties = $arrobjApPayeeProperties;
	}

	public function setCompanyUserPreferences( $arrobjCompanyUserPreferences ) {
		$this->m_arrobjCompanyUserPreferences = $arrobjCompanyUserPreferences;
	}

	public function setFailRoutedDetails( $arrobjFailRoutedDetails ) {
		$this->m_arrobjFailRoutedDetails = $arrobjFailRoutedDetails;
	}

	public function setFileAssociations( $arrobjFileAssociations ) {
		$this->m_arrobjFileAssociations = $arrobjFileAssociations;
	}

	public function setOriginalApHeaders( $arrobjOriginalApHeaders ) {
		$this->m_arrobjOriginalApHeaders = $arrobjOriginalApHeaders;
	}

	public function setApCodeProperties( $arrmixApCodeProperties ) {
		$this->m_arrmixApCodeProperties = $arrmixApCodeProperties;
	}

	public function setReversePostMonth( $strReversePostMonth ) {

		if( true == valStr( $strReversePostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strReversePostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strReversePostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}

			$this->m_strReversePostMonth = $strReversePostMonth;
		}

		return $this->m_strReversePostMonth;
	}

	public function setBulkUnitNumber( $strBulkUnitNumber ) {
		$this->m_strBulkUnitNumber = CStrings::strTrimDef( $strBulkUnitNumber, -1, NULL, true );
	}

	public function setReversePostDate( $strReversePostDate ) {
		$this->m_strReversePostDate = __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ CStrings::strTrimDef( $strReversePostDate, -1, NULL, true ) ] );
	}

	// Override setPostMonth function. It will acccept date in dd/yyyy format and set it to dd/01/yyyy format

	public function setPostMonth( $strPostMonth ) {

		if( true == valStr( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}
		}

		$this->m_strPostMonth = $strPostMonth;
	}

	public function setAutoSchedulePayment( $intAutoSchedulePayment ) {
		$this->m_intAutoSchedulePayment = $intAutoSchedulePayment;
	}

	public function setAutoProcessPayment( $intAutoProcessPayment ) {
		$this->m_intAutoProcessPayment = $intAutoProcessPayment;
	}

	public function setApPaymentTypeId( $intApPaymentTypeId ) {
		$this->m_intApPaymentTypeId = CStrings::strToIntDef( $intApPaymentTypeId, NULL, false );
	}

	public function setPoNumber( $strPoNumber ) {
		$this->m_strPoNumber = $strPoNumber;
	}

	public function setApPayeeStatusTypeId( $intApPayeeStatusTypeId ) {
		$this->m_intApPayeeStatusTypeId = $intApPayeeStatusTypeId;
	}

	public function setApPayeeTypeId( $intApPayeeTypeId ) {
		$this->m_intApPayeeTypeId = CStrings::strToIntDef( $intApPayeeTypeId, NULL, false );
	}

	public function setPayeeName( $strPayeeName ) {
		$this->m_strPayeeName = CStrings::strTrimDef( $strPayeeName, 100, NULL, true );
	}

	public function setApPayeeTerm( $strApPayeeTerm ) {
		$this->m_strApPayeeTerm = $strApPayeeTerm;
	}

	public function setUnallocatedAmount( $fltUnallocatedAmount ) {
		$this->m_fltUnallocatedAmount = CStrings::strToFloatDef( $fltUnallocatedAmount, NULL, false, 2 );
	}

	public function setPaidAmount( $fltPaidAmount ) {
		$this->m_fltPaidAmount = CStrings::strToFloatDef( $fltPaidAmount, NULL, false, 2 );
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->m_fltPaymentAmount = CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 3 );
	}

	public function setApprovedAmount( $fltApprovedAmount ) {
		return $this->m_fltApprovedAmount = CStrings::strToFloatDef( $fltApprovedAmount, NULL, false, 2 );
	}

	public function setUtilityBillIds( $arrintUtilityBillIds ) {
		$this->m_arrintUtilityBillIds = $arrintUtilityBillIds;
	}

	public function setBillPayPropertyIds( $arrintBillPayPropertyIds ) {
		$this->m_arrintBillPayPropertyIds = $arrintBillPayPropertyIds;
	}

	public function setCompanyUserPropertyIds( $arrintCompanyUserPropertyIds ) {
		$this->m_arrintCompanyUserPropertyIds = $arrintCompanyUserPropertyIds;
	}

	public function setApPayee( $objApPayee ) {
		$this->m_objApPayee = $objApPayee;
	}

	public function setApPayment( $objApPayment ) {
		$this->m_objApPayment = $objApPayment;
	}

	public function setEmailDatabase( $objEmailDatabase ) {
		$this->m_objEmailDatabase = $objEmailDatabase;
	}

	public function setApPayeeLocation( $objApPayeeLocation ) {
		$this->m_objApPayeeLocation = $objApPayeeLocation;
	}

	public function setControlTotal( $fltControlTotal ) {
		$this->m_fltControlTotal = $fltControlTotal;
	}

	public function setApPayeeName( $strApPayeeName ) {
		$this->m_strApPayeeName = $strApPayeeName;
	}

	public function setLocationName( $strLocationName ) {
		$this->m_strLocationName = $strLocationName;
	}

	public function setApPayeeLocationName( $strApPayeeLocationName ) {
		$this->m_strApPayeeLocationName = $strApPayeeLocationName;
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->m_strSecondaryNumber = $strSecondaryNumber;
	}

	public function setOldApHeaderId( $intOldApHeaderId ) {
		$this->m_intOldApHeaderId = $intOldApHeaderId;
	}

	public function setSystemEmails( $objSysyemEmail, $strKey ) {
		$this->m_arrobjSystemEmails[$strKey] = $objSysyemEmail;
	}

	public function setIsRuleStopUpdated( $boolIsRuleStopUpdated ) {
		$this->m_boolIsRuleStopUpdated = $boolIsRuleStopUpdated;
	}

	public function setIsInitialImportPosted( $boolIsInitialImportPosted ) {
		$this->m_boolIsInitialImportPosted = $boolIsInitialImportPosted;
	}

	public function setIsApproved( $boolIsApproved ) {
		$this->m_boolIsApproved = $boolIsApproved;
	}

	public function setStreetLine1( $strStreetline1 ) {
		$this->m_strStreetLine1 = $strStreetline1;
	}

	public function setStreetLine2( $strStreetline2 ) {
		$this->m_strStreetLine2 = $strStreetline2;
	}

	public function setStreetLine3( $strStreetline3 ) {
		$this->m_strStreetLine3 = $strStreetline3;
	}

	public function setCity( $strCity ) {
		$this->m_strCity = $strCity;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->m_strPostalCode = $strPostalCode;
	}

	public function setProvince( $strProvince ) {
		$this->m_strProvince = $strProvince;
	}

	public function setCountry( $strCountry ) {
		$this->m_strCountry = $strCountry;
	}

	public function setIsExported( $boolIsExported ) {
		$this->m_boolIsExported = $boolIsExported;
	}

	public function setIsAttachment( $boolIsAttachment ) {
		$this->m_boolIsAttachment = $boolIsAttachment;
	}

	public function setIsAutoApproval( $boolIsAutoApproval ) {
		$this->m_boolIsAutoApproval = $boolIsAutoApproval;
	}

	public function setApHeaderLeaseCustomers( $arrobjApHeaderLeaseCustomers ) {
		$this->m_arrobjApHeaderLeaseCustomers = $arrobjApHeaderLeaseCustomers;
	}

	public function setRefundCustomerNames( $strRefundCustomerNames ) {
		$this->m_strRefundCustomerNames = $strRefundCustomerNames;
	}

	public function setApPayeeRequirePoForInvoice( $intRequirePoForInvoice ) {
		$this->m_intApPayeeRequirePoForInvoice = $intRequirePoForInvoice;
	}

	public function setCompanySettingRequirePosForInvoices( $intCompanySettingRequirePosForInvoices ) {
		$this->m_intCompanyRequirePosForInvoices = $intCompanySettingRequirePosForInvoices;
	}

	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setJobName( $strJobName ) {
		$this->m_strJobName = $strJobName;
	}

	public function setApExceptionQueueItemId( $intApExceptionQueueItemId ) {
		$this->m_intApExceptionQueueItemId = $intApExceptionQueueItemId;
	}

	public function setJobId( $intJobId ) {
		$this->m_intJobId = $intJobId;
	}

	public function setJobPhaseName( $strJobPhaseName ) {
		$this->m_strJobPhaseName = $strJobPhaseName;
	}

	public function setApContractName( $strApContractName ) {
		$this->m_strApContractName = $strApContractName;
	}

	public function setReimbursingPropertyName( $strReimbursingPropertyName ) {
		$this->m_strReimbursingPropertyName = CStrings::strTrimDef( $strReimbursingPropertyName, 50, NULL, true );
	}

	public function setAssetTransactions( $arrobjAssetTransactions ) {
		$this->m_arrobjAssetTransactions = $arrobjAssetTransactions;
	}

	public function setUnitName( $strUnitName ) {
		$this->m_strUnitName = CStrings::strTrimDef( $strUnitName, 50, NULL, true );
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->m_strCheckRoutingNumber = CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true );
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->m_strCheckAccountNumberEncrypted = CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true );

		$this->setCheckAccountNumber( $this->getCheckAccountNumberEncrypted() );
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		$this->m_strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 240, NULL, true );
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->m_strCheckNameOnAccount = CStrings::strTrimDef( $strCheckNameOnAccount, 240, NULL, true );
	}

	public function setCreateTemplateFrom( $strCreateTemplateFrom ) {
		$this->m_strCreateTemplateFrom = $strCreateTemplateFrom;
	}

	public function setTemplateType( $strTemplateType ) {
		$this->m_strTemplateType = $strTemplateType;
	}

	public function setAssetId( $intAssetId ) {
		$this->m_intAssetId = $intAssetId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPoQuantity( $intPoQuantity ) {
		$this->m_intPoQuantity = CStrings::strToIntDef( $intPoQuantity, NULL, false );
	}

	public function setApPhysicalStatusName( $strApPhysicalStatus ) {
		$this->m_strApPhysicalStatusName = $strApPhysicalStatus;
	}

	public function setApFinancialStatusName( $strApFinancialStatus ) {
		$this->m_strApFinancialStatusName = $strApFinancialStatus;
	}

	public function setApPayeeAccountNumber( $strApPayeeAccountNumber ) {
		$this->m_strApPayeeAccountNumber = $strApPayeeAccountNumber;
	}

	public function setReimbursedHeaderNumber( $strReimbursedHeaderNumber ) {
		$this->m_strReimbursedHeaderNumber = $strReimbursedHeaderNumber;
	}

	public function setPoApHeaderLabel( $strPoApHeaderLabel ) {
		$this->m_strPoApHeaderLabel = $strPoApHeaderLabel;
	}

	public function setApRoutingTagName( $strApRoutingTagName ) {
		$this->m_strApRoutingTagName = $strApRoutingTagName;
	}

	public function setVendorCode( $strVendorCode ) {
		$this->m_strVendorCode = $strVendorCode;
	}

	public function setBulkJobPhaseName( $strBulkJobPhaseName ) {
		$this->m_strBulkJobPhaseName = CStrings::strTrimDef( $strBulkJobPhaseName, 50, NULL, true );
	}

	public function setBulkApContractName( $strBulkApContractName ) {
		$this->m_strBulkApContractName = CStrings::strTrimDef( $strBulkApContractName, 50, NULL, true );
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->m_intBankAccountId = $intBankAccountId;
	}

	public function setPoApHeaderNumbers( $arrstrPoApHeaderNumbers ) {
		if( false == valArr( $arrstrPoApHeaderNumbers ) ) $arrstrPoApHeaderNumbers = explode( ',', \Psi\CStringService::singleton()->substr( trim( $arrstrPoApHeaderNumbers ), 1, -1 ) );

		$this->m_arrstrPoApHeaderNumbers = $arrstrPoApHeaderNumbers;
	}

	public function setMigratedPropertyIds( $intMigratedPropertyIds ) {
		$this->m_arrmixMigratedPropertyIds = $intMigratedPropertyIds;
	}

	public function setIsLocationDummyAccount( $boolIsLocationDummyAccount ) {
		$this->m_boolIsLocationDummyAccount = $boolIsLocationDummyAccount;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setVendorAccountNumber( $strVendorAccountNumber ) {
		$this->m_strVendorAccountNumber = $strVendorAccountNumber;
	}

	public function setIsConsolidated( $boolIsConsolidated ) {
		$this->m_boolIsConsolidated = $boolIsConsolidated;
	}

	public function setStoreId( $intStoreId ) {
		$this->m_intStoreId = $intStoreId;
	}

	public function setBuyerAccountId( $intBuyerAccountId ) {
		$this->m_intBuyerAccountId = $intBuyerAccountId;
	}

	public function setReleasedAmount( $fltReleasedAmount ) {
		$this->m_fltReleasedAmount = CStrings::strToFloatDef( $fltReleasedAmount, NULL, false, 2 );
	}

	public function setOriginalRetentionAmount( $fltOriginalRetentionAmount ) {
		$this->m_fltOriginalRetentionAmount = CStrings::strToFloatDef( $fltOriginalRetentionAmount, NULL, false, 2 );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['auto_schedule_payment'] ) ) $this->setAutoSchedulePayment( $arrmixValues['auto_schedule_payment'] );
		if( true == isset( $arrmixValues['auto_process_payment'] ) ) $this->setAutoProcessPayment( $arrmixValues['auto_process_payment'] );
		if( true == isset( $arrmixValues['ap_payment_type_id'] ) ) $this->setApPaymentTypeId( $arrmixValues['ap_payment_type_id'] );
		if( true == isset( $arrmixValues['unallocated_amount'] ) ) $this->setUnallocatedAmount( $arrmixValues['unallocated_amount'] );
		if( true == isset( $arrmixValues['paid_amount'] ) ) $this->setPaidAmount( $arrmixValues['paid_amount'] );
		if( true == isset( $arrmixValues['approved_amount'] ) ) $this->setApprovedAmount( $arrmixValues['approved_amount'] );
		if( true == isset( $arrmixValues['payee_name'] ) ) $this->setPayeeName( $arrmixValues['payee_name'] );
		if( true == isset( $arrmixValues['ap_payee_term'] ) ) $this->setApPayeeTerm( $arrmixValues['ap_payee_term'] );
		if( true == isset( $arrmixValues['ap_payee_type_id'] ) ) $this->setApPayeeTypeId( $arrmixValues['ap_payee_type_id'] );
		if( true == isset( $arrmixValues['ap_payee_status_type_id'] ) ) $this->setApPayeeStatusTypeId( $arrmixValues['ap_payee_status_type_id'] );
		if( true == isset( $arrmixValues['reverse_post_month'] ) ) $this->setReversePostMonth( $arrmixValues['reverse_post_month'] );
		if( true == isset( $arrmixValues['reverse_post_date'] ) ) $this->setReversePostDate( $arrmixValues['reverse_post_date'] );
		if( true == isset( $arrmixValues['bulk_unit_number'] ) ) $this->setBulkUnitNumber( $arrmixValues['bulk_unit_number'] );
		if( true == isset( $arrmixValues['ap_payee_name'] ) ) $this->setApPayeeName( $arrmixValues['ap_payee_name'] );
		if( true == isset( $arrmixValues['location_name'] ) ) $this->setLocationName( $arrmixValues['location_name'] );
		if( true == isset( $arrmixValues['is_approved'] ) ) $this->setIsApproved( $arrmixValues['is_approved'] );
		if( true == isset( $arrmixValues['street_line1'] ) ) $this->setStreetLine1( $arrmixValues['street_line1'] );
		if( true == isset( $arrmixValues['street_line2'] ) ) $this->setStreetLine2( $arrmixValues['street_line2'] );
		if( true == isset( $arrmixValues['street_line3'] ) ) $this->setStreetLine3( $arrmixValues['street_line3'] );
		if( true == isset( $arrmixValues['city'] ) ) $this->setCity( $arrmixValues['city'] );
		if( true == isset( $arrmixValues['state_code'] ) ) $this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['postal_code'] ) ) $this->setPostalCode( $arrmixValues['postal_code'] );
		if( true == isset( $arrmixValues['province'] ) ) $this->setProvince( $arrmixValues['province'] );
		if( true == isset( $arrmixValues['country'] ) ) $this->setCountry( $arrmixValues['country'] );
		if( true == isset( $arrmixValues['is_exported'] ) ) $this->setIsExported( $arrmixValues['is_exported'] );
		if( true == isset( $arrmixValues['is_attachment'] ) ) $this->setIsAttachment( $arrmixValues['is_attachment'] );
		if( true == isset( $arrmixValues['refund_customer_names'] ) ) $this->setRefundCustomerNames( $arrmixValues['refund_customer_names'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['job_id'] ) ) $this->setJobId( $arrmixValues['job_id'] );
		if( true == isset( $arrmixValues['job_name'] ) ) $this->setJobName( $arrmixValues['job_name'] );
		if( true == isset( $arrmixValues['job_phase_name'] ) ) $this->setJobPhaseName( $arrmixValues['job_phase_name'] );
		if( true == isset( $arrmixValues['contract_name'] ) ) $this->setApContractName( $arrmixValues['contract_name'] );
		if( true == isset( $arrmixValues['unit_name'] ) ) $this->setUnitName( $arrmixValues['unit_name'] );
		if( true == isset( $arrmixValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( $arrmixValues['check_routing_number'] );
		if( true == isset( $arrmixValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( $arrmixValues['check_account_number_encrypted'] );
		if( true == isset( $arrmixValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( $arrmixValues['check_name_on_account'] );
		if( true == isset( $arrmixValues['reimbursing_property_name'] ) ) $this->setReimbursingPropertyName( $arrmixValues['reimbursing_property_name'] );
		if( true == isset( $arrmixValues['quantity'] ) ) $this->setPoQuantity( $arrmixValues['quantity'] );
		if( true == isset( $arrmixValues['asset_id'] ) ) $this->setAssetId( $arrmixValues['asset_id'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['ap_physical_status'] ) ) $this->setApPhysicalStatusName( $arrmixValues['ap_physical_status'] );
		if( true == isset( $arrmixValues['ap_financial_status'] ) ) $this->setApFinancialStatusName( $arrmixValues['ap_financial_status'] );
		if( true == isset( $arrmixValues['ap_payee_account_number'] ) ) $this->setApPayeeAccountNumber( $arrmixValues['ap_payee_account_number'] );
		if( true == isset( $arrmixValues['po_number'] ) ) $this->setPoNumber( $arrmixValues['po_number'] );
		if( true == isset( $arrmixValues['po_ap_header_label'] ) ) $this->setPoApHeaderLabel( $arrmixValues['po_ap_header_label'] );
		if( true == isset( $arrmixValues['ap_routing_tag_name'] ) ) $this->setApRoutingTagName( $arrmixValues['ap_routing_tag_name'] );
		if( true == isset( $arrmixValues['reimbursed_header_number'] ) ) $this->setReimbursedHeaderNumber( $arrmixValues['reimbursed_header_number'] );
		if( true == isset( $arrmixValues['vendor_code'] ) ) $this->setVendorCode( trim( $arrmixValues['vendor_code'] ) );
		if( true == isset( $arrmixValues['bulk_job_phase_name'] ) ) $this->setBulkJobPhaseName( trim( $arrmixValues['bulk_job_phase_name'] ) );
		if( true == isset( $arrmixValues['bulk_ap_contract_name'] ) ) $this->setBulkApContractName( trim( $arrmixValues['bulk_ap_contract_name'] ) );
		if( true == isset( $arrmixValues['template_type'] ) ) 					$this->setTemplateType( $arrmixValues['template_type'] );
		if( true == isset( $arrmixValues['create_template_from'] ) ) 			$this->setCreateTemplateFrom( $arrmixValues['create_template_from'] );
		if( true == isset( $arrmixValues['days_of_month'] ) )					$this->setDaysOfMonth( $arrmixValues['days_of_month'] );
		if( true == isset( $arrmixValues['end_on'] ) ) $this->setEndOn( $arrmixValues['end_on'] );
		if( true == isset( $arrmixValues['bank_account_id'] ) ) $this->setBankAccountId( $arrmixValues['bank_account_id'] );
		if( true == isset( $arrmixValues['po_ap_header_numbers'] ) ) $this->setPoApHeaderNumbers( $arrmixValues['po_ap_header_numbers'] );
		if( true == isset( $arrmixValues['migrated_properties'] ) ) $this->setMigratedPropertyIds( $arrmixValues['migrated_properties'] );
		if( true == isset( $arrmixValues['payment_amount'] ) ) $this->setPaymentAmount( $arrmixValues['payment_amount'] );
		if( true == isset( $arrmixValues['is_location_dummy_account'] ) ) $this->setIsLocationDummyAccount( $arrmixValues['is_location_dummy_account'] );
		if( true == isset( $arrmixValues['ap_exception_queue_item_id'] ) ) $this->setApExceptionQueueItemId( $arrmixValues['ap_exception_queue_item_id'] );
		if( true == isset( $arrmixValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrmixValues['unit_type_id'] );
		if( true == isset( $arrmixValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrmixValues['maintenance_location_id'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['vendor_account_number'] ) ) $this->setVendorAccountNumber( $arrmixValues['vendor_account_number'] );
		if( true == isset( $arrmixValues['is_consolidated'] ) ) $this->setIsConsolidated( $arrmixValues['is_consolidated'] );
		if( true == isset( $arrmixValues['store_id'] ) ) $this->setStoreId( $arrmixValues['store_id'] );
		if( true == isset( $arrmixValues['buyer_account_id'] ) ) $this->setBuyerAccountId( $arrmixValues['buyer_account_id'] );
		if( true == isset( $arrmixValues['ap_payee_location_name'] ) ) $this->setApPayeeLocationName( $arrmixValues['ap_payee_location_name'] );
		if( true == isset( $arrmixValues['secondary_number'] ) ) $this->setSecondaryNumber( $arrmixValues['secondary_number'] );
		if( true == isset( $arrmixValues['payment_number'] ) ) $this->setPaymentNumber( $arrmixValues['payment_number'] );
		if( true == isset( $arrmixValues['released_amount'] ) ) $this->setReleasedAmount( $arrmixValues['released_amount'] );
		if( true == isset( $arrmixValues['original_retention_amount'] ) ) $this->setOriginalRetentionAmount( $arrmixValues['original_retention_amount'] );
		return;
	}

	public function setDefaultValuesToRecurringApTransaction() {

		$this->setTemplateApHeaderId( $this->getId() );
		$this->setId( NULL );
		if( CApHeaderType::PURCHASE_ORDER == $this->getApHeaderTypeId() ) {
			$this->setApPhysicalStatusTypeId( CApPhysicalStatusType::NOT_ORDERED );
			$this->setApFinancialStatusTypeId( CApFinancialStatusType::PENDING );
		}
		$this->setGlTransactionTypeId( CGlTransactionType::AP_CHARGE );
		$this->setFrequencyId( CFrequency::ONCE );
		$this->setTransactionDatetime( 'NOW()' );
		$this->setTemplateName( NULL );
		$this->setStartDate( NULL );
		$this->setEndDate( NULL );
		$this->setLastPostedDate( NULL );
		$this->setNextPostDate( NULL );
		$this->setDayOfWeek( NULL );
		$this->setDaysOfMonth( [] );
		$this->setNumberOfOccurrences( NULL );
		$this->setIsTemplate( false );
		$this->setApprovedBy( NULL );
		$this->setApprovedOn( NULL );
		$this->setUpdatedBy( SCRIPT_USER_ID );
		$this->setUpdatedOn( 'NOW()' );
		$this->setCreatedBy( SCRIPT_USER_ID );
		$this->setCreatedOn( 'NOW()' );

		foreach( $this->getApDetails() as $objApDetail ) {

			$objApDetail->setTemplateApDetailId( $objApDetail->getId() );
			$objApDetail->setId( NULL );
			$objApDetail->setApHeaderId( NULL );
			$objApDetail->setPostMonth( $this->getPostMonth() );
			$objApDetail->setTransactionDatetime( 'NOW()' );
			$objApDetail->setApprovedBy( NULL );
			$objApDetail->setApprovedOn( NULL );
			$objApDetail->setUpdatedBy( SCRIPT_USER_ID );
			$objApDetail->setUpdatedOn( 'NOW()' );
			$objApDetail->setCreatedBy( SCRIPT_USER_ID );
			$objApDetail->setCreatedOn( 'NOW()' );
		}
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->m_intUnitTypeId = $intUnitTypeId;
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->m_intMaintenanceLocationId = $intMaintenanceLocationId;
	}

	public function setIsCcPaymentInvoice( $boolIsCcPaymentInvoice ) {
		$this->m_boolIsCcPaymentInvoice = $boolIsCcPaymentInvoice;
	}

	public function setIsDisbursement( $boolIsDisbursement ) {
		$this->m_boolIsDisbursement = ( bool ) $boolIsDisbursement;
	}

	public function setIsOldDisbursement( $boolIsOldDisbursement ) {
		$this->m_boolIsOldDisbursement = ( bool ) $boolIsOldDisbursement;
	}

	public function setIsOldReimbursement( $boolIsOldReimbursement ) {
		$this->m_boolIsOldReimbursement = ( bool ) $boolIsOldReimbursement;
	}

	public function setIntercompanyFields( $arrobjApDetails = NULL ) {

		if( false == valArr( $arrobjApDetails ) ) {
			if( true == valArr( $this->m_arrobjApDetails ) ) {
				$arrobjApDetails = $this->m_arrobjApDetails;
			} else {
				return;
			}
		}

		foreach( $arrobjApDetails as $objApDetail ) {

			switch( $objApDetail->getApTransactionTypeId() ) {

				case CApTransactionType::STANDARD:
					if( CReimbursementMethod::REIMBURSEMENT_METHOD_USE_SUBLEDGERS == $objApDetail->getReimbursementMethodId() ) {
						$this->setIsOldDisbursement( true );
						break 2;
					} elseif( CReimbursementMethod::REIMBURSEMENT_METHOD_USE_SUBLEDGERS_NEW == $objApDetail->getReimbursementMethodId() ) {
						$this->setIsDisbursement( true );
						break 2;
					}
					break;

				case CApTransactionType::STANDARD_REIMBURSEMENT:
					if( CReimbursementMethod::NO_REIMBURSEMENT == $objApDetail->getReimbursementMethodId() ) {
						$this->setIsOldReimbursement( true );
						break 2;
					}
					break;

				case CApTransactionType::STANDARD_REIMBURSEMENT_NEW:
					if( CReimbursementMethod::NO_REIMBURSEMENT == $objApDetail->getReimbursementMethodId() ) {
						$this->setIsReimbursement( true );
						break 2;
					}
					break;

				default:
					// Not an inter-company ap detail
					break;
			}
		}
	}

	public function setPaymentNumber( $strPaymentNumber ) {
		$this->m_strPaymentNumber = $strPaymentNumber;
	}

	public function setFiles( $arrobjFiles ) {
		$this->m_arrobjFiles = $arrobjFiles;
	}

	public function setIsAutoApprovedCreatorStop( $boolIsAutoApprovedCreatorStop ) {
		$this->m_boolIsAutoApprovedCreatorStop = $boolIsAutoApprovedCreatorStop;
	}

	// Validation functions

	public function valPostMonthLocked() {

		$boolIsValid					= true;
		$arrstrApPostMonthPropertyNames	= [];
		$arrstrGlPostMonthPropertyNames	= [];

		foreach( $this->getApDetails() as $objApDetail ) {

			$objPropertyGlSetting = $objApDetail->getPropertyGlSetting();

			if( false == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
				continue;
			}

			if( strtotime( $this->getPostMonth() ) <= strtotime( $objPropertyGlSetting->getApLockMonth() ) ) {

				$boolIsValid &= false;
				$arrstrApPostMonthPropertyNames[] = $objApDetail->getPropertyName();
			}

			if( strtotime( $this->getPostMonth() ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() ) ) {

				$boolIsValid &= false;
				$arrstrGlPostMonthPropertyNames[] = $objApDetail->getPropertyName();
			}
		}

		$arrstrApPostMonthPropertyNames = array_unique( $arrstrApPostMonthPropertyNames );
		$arrstrGlPostMonthPropertyNames = array_unique( $arrstrGlPostMonthPropertyNames );

		if( true == valArr( $arrstrApPostMonthPropertyNames ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month_locked', 'AP post month "' . date( 'm/Y', strtotime( $this->getPostMonth() ) ) . '" is locked for Property(s) "' . implode( ', ', $arrstrApPostMonthPropertyNames ) . '". ' ) );
		}

		if( true == valArr( $arrstrGlPostMonthPropertyNames ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month_locked', 'GL post month "' . date( 'm/Y', strtotime( $this->getPostMonth() ) ) . '" is locked for Property(s) "' . implode( ', ', $arrstrGlPostMonthPropertyNames ) . '". ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixParameters = [] ) {

		$boolIsValid = true;

		$objApHeaderValidator = new CApHeaderValidator();
		$objApHeaderValidator->setApHeader( $this );
		$boolIsValid &= $objApHeaderValidator->validate( $strAction, $arrmixParameters );

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchGlDetails( $objClientDatabase ) {

		$arrintApHeaderIds = [ $this->getId() ];

		if( false == is_null( $this->getReversalApHeaderId() ) ) {
			array_push( $arrintApHeaderIds, $this->getReversalApHeaderId() );
		}

		return CGlDetails::fetchGlDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchApAllocationGlDetails( $objClientDatabase ) {
		return CGlDetails::fetchApAllocationGlDetailsByApHeaderIdsByCid( [ $this->getId() ], $this->getCid(), $objClientDatabase );
	}

	public function fetchApDetails( $objClientDatabase, $boolIsFromLoadPoEdit = false ) {
		$boolHasJobCostingProduct = in_array( $this->getApHeaderSubTypeId(), [ CApHeaderSubType::STANDARD_JOB_INVOICE, CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, CApHeaderSubType::JOB_PHASE_BUDGET, CApHeaderSubType::CONTRACT_BUDGET, CApHeaderSubType::JOB_CHANGE_ORDER, CApHeaderSubType::CONTRACT_CHANGE_ORDER, CApHeaderSubType::JOB_BUDGET_SUMMARY, CApHeaderSubType::CONTRACT_BUDGET_SUMMARY, CApHeaderSubType::JOB_BUDGET_ADJUSTMENT, CApHeaderSubType::CATALOG_JOB_INVOICE, CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER ] );

		return \Psi\Eos\Entrata\CApDetails::createService()->fetchApDetailsByInvoiceOrPaymentApHeaderIdsByCid( [ $this->getId() ], $this->getCid(), $objClientDatabase, false, $boolHasJobCostingProduct, $boolIsFromLoadPoEdit );
	}

	public function fetchGlHeaders( $objClientDatabase ) {
		return CGlHeaders::fetchGlHeadersByApHeaderIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchApDetailByRemotePrimaryKey( $strRemotePrimaryKey, $objClientDatabase ) {
		return CApDetails::fetchApDetailByApHeaderIdByRemotePrimaryKeyByCid( $this->getId(), $strRemotePrimaryKey, $this->getCid(), $objClientDatabase );
	}

	public function fetchFiles( $objClientDatabase ) {
		return CFiles::fetchFilesByApHeaderIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchNonReturnedAssetTransactions( $objClientDatabase, $boolIsInventoryInvoiced = false, $boolIsConsumed = false, $boolOnlyNonUsed = false ) {
		return \Psi\Eos\Entrata\CAssetTransactions::createService()->fetchNonReturnedAssetTransactionsByApHeaderIdsByCid( [ $this->getId() ], $this->getCid(), $objClientDatabase, $boolIsInventoryInvoiced, $boolIsConsumed, $boolOnlyNonUsed );
	}

	public function fetchNonReceivedApDetails( $objClientDatabase, $objApTransactionFilter = NULL, $boolIsInventoryInvoiced = false ) {
		return \Psi\Eos\Entrata\CApDetails::createService()->fetchNonReceivedApDetailsByCid( $this->getCid(), $objClientDatabase, $this->getHeaderNumber(), $objApTransactionFilter, NULL, true, false, $boolIsInventoryInvoiced );
	}

	public function fetchApPayee( $objClientDatabase ) {
		return CApPayees::fetchActiveApPayeeByIdByCid( $this->getApPayeeId(), $this->getCid(), $objClientDatabase );
	}

	/**
	 * Other Functions
	 */

	public function addOriginalApHeader( $objOriginalApHeader ) {
		$this->m_arrobjOriginalApHeaders[] = $objOriginalApHeader;
	}

	public function addApDetails( $objApDetail ) {
		$this->m_arrobjApDetails[] = $objApDetail;
		$this->setIntercompanyFields();
	}

	public function addApDetail( $objApDetail ) {
		if( true == valId( $objApDetail->getId() ) ) {
			$this->m_arrobjApDetails[$objApDetail->getId()] = $objApDetail;
		} else {
			$this->m_arrobjApDetails[] = $objApDetail;
		}
		$this->setIntercompanyFields();
	}

	public function addFiles( $objFile ) {
		$this->m_arrobjFiles[] = $objFile;
	}

	public function unPost( $intCompanyUserId, $objClientDatabase, $boolIsDoNotPost = false ) {

		$arrstrApPostMonths					= [];
		$arrstrApLockMonths					= [];
		$arrobjApDetails					= ( array ) $this->getApDetails( $objClientDatabase );

		if( false == valArr( $arrobjApDetails ) ) {
			return false;
		}

		// 1. Set the is_posted = 0 for the current invoice.

		$this->setTransactionDatetime( 'NOW()' );
		if( false == $boolIsDoNotPost ) {
			$this->setIsTemporary( 1 );
			$this->setIsPosted( 0 );
			$this->setApprovedBy( NULL );
			$this->setApprovedOn( NULL );
		}
		$arrobjPropertyGlSettings 	= ( array ) CPropertyGlSettings::fetchPropertyGlSettingsByPropertyIdsByCid( array_filter( array_keys( rekeyObjects( 'PropertyId', $arrobjApDetails ) ) ), $this->getCid(), $objClientDatabase );

		foreach( $arrobjPropertyGlSettings as $objPropertyGlSetting ) {
			$arrstrApPostMonths[$objPropertyGlSetting->getPropertyId()]	= $objPropertyGlSetting->getApPostMonth();
			$arrstrApLockMonths[$objPropertyGlSetting->getPropertyId()]	= $objPropertyGlSetting->getApLockMonth();
		}

		// If all properties of invoice have same ap post month in property_gl_settings then set it as invoice's post month.
		$arrstrApPostMonths	= array_unique( $arrstrApPostMonths );
		$arrstrApLockMonths	= array_unique( $arrstrApLockMonths );

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrApLockMonths ) && 1 == \Psi\Libraries\UtilFunctions\count( $arrstrApPostMonths ) ) {

			$intApLockMonthTimeStamp	= mktime( 0, 0, 0, date( 'm', strtotime( current( $arrstrApLockMonths ) ) ), date( 'd', strtotime( current( $arrstrApLockMonths ) ) ), date( 'Y', strtotime( current( $arrstrApLockMonths ) ) ) );
			$intPostMonthTimeStamp			= mktime( 0, 0, 0, date( 'm', strtotime( $this->getPostMonth() ) ), date( 'd', strtotime( $this->getPostMonth() ) ), date( 'Y', strtotime( $this->getPostMonth() ) ) );

			if( $intPostMonthTimeStamp <= $intApLockMonthTimeStamp ) {
				$this->setPostMonth( current( $arrstrApPostMonths ) );
			}
		}

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( false == $this->update( $intCompanyUserId, $objClientDatabase, false, false, true ) ) {
					$boolIsValid = false;
					break;
				}

				if( false == $this->logHistory( CApHeaderLog::ACTION_UNPOSTED, $intCompanyUserId, $objClientDatabase ) ) {
					$boolIsValid = false;
					break;
				}

				$arrobjGlHeaders = ( array ) CGlHeaders::fetchActiveGlHeadersByApHeaderIdsByCid( [ $this->getId() ], $this->getCid(), $objClientDatabase );
				$arrobjGlDetails = ( array ) CGlDetails::fetchActiveGlDetailsByGlHeaderIdsByCid( array_filter( array_keys( $arrobjGlHeaders ) ), $this->getCid(), $objClientDatabase );

				// If exported_by or the gl_rec_Id is not set in the ap_details then,
				$boolGlReverse = $this->isReconciledOrExportedLineItem( $arrobjGlDetails );

				if( false == $boolGlReverse && CApHeaderSubType::STANDARD_JOB_INVOICE == $this->getApHeaderSubTypeId() ) {

					$arrobjReclassGlHeaders = ( array ) CGlHeaders::fetchGlHeadersByReclassGlHeaderIdsByGlTransactionTypeIdsByCid( array_filter( array_keys( $arrobjGlHeaders ) ), [ CGlTransactionType::AP_CHARGE ], $this->getCid(), $objClientDatabase );
					$arrobjReclassGlDetails = ( array ) CGlDetails::fetchGlDetailsByReclassGlDetailIdsByGlTransactionTypeIdsByCid( array_filter( array_keys( $arrobjGlDetails ) ), [ CGlTransactionType::AP_CHARGE ], $this->getCid(), $objClientDatabase );

					// Delete the re_class_gl_entries for job costing invoices
					foreach( $arrobjReclassGlDetails as $objReclassGlDetail ) {

						if( false == $objReclassGlDetail->delete( $intCompanyUserId, $objClientDatabase ) ) {
							$boolIsValid = false;
							break 2;
						}
					}

					foreach( $arrobjReclassGlHeaders as $objReclassGlHeader ) {

						if( false == $objReclassGlHeader->delete( $intCompanyUserId, $objClientDatabase ) ) {
							$boolIsValid = false;
							break 2;
						}
					}
				}

				if( true == $boolGlReverse ) {

					if( true == $boolIsDoNotPost ) {
						$boolIsValid = false;
						break;
					}

					// Reverse the Gl Posting for each line item in the invoice.
					foreach( $arrobjGlHeaders as $objGlHeader ) {

						$objClonedGlHeader	= clone $objGlHeader;
						$objClonedGlHeader->setOffsettingGlHeaderId( $objGlHeader->getId() );
						$objClonedGlHeader->setId( NULL );

						if( false == $objClonedGlHeader->insert( $intCompanyUserId, $objClientDatabase ) ) {
							$boolIsValid = false;
							break 2;
						}

						$objGlHeader->setOffsettingGlHeaderId( $objClonedGlHeader->getId() );

						if( false == $objGlHeader->update( $intCompanyUserId, $objClientDatabase ) ) {
							$boolIsValid = false;
							break 2;
						}
					}

					foreach( $arrobjGlDetails as $objGlDetail ) {

						$objClonedGlDetail	= clone $objGlDetail;
						$objClonedGlDetail->setAmount( -1 * $objClonedGlDetail->getAmount() );
						$objClonedGlDetail->setOffsettingGlDetailId( $objClonedGlDetail->getId() );
						$objClonedGlDetail->setGlHeaderId( $objClonedGlHeader->getId() );
						$objClonedGlDetail->setId( NULL );
						$objClonedGlDetail->setCreatedOn( 'NOW()' );
						$objClonedGlDetail->setUpdatedOn( 'NOW()' );

						if( false == $objClonedGlDetail->insert( $intCompanyUserId, $objClientDatabase ) ) {
							$boolIsValid = false;
							break 2;
						}

						$objGlDetail->setOffsettingGlDetailId( $objClonedGlDetail->getId() );

						if( false == $objGlDetail->update( $intCompanyUserId, $objClientDatabase ) ) {
							$boolIsValid = false;
 							break 2;
 						}
 					}

				} else {
					//  Delete the Gl Posting for each line item in the invoice.
					foreach( $arrobjGlDetails as $objGlDetail ) {
						/**@var $objAccountingExportAssociation \CAccountingExportAssociation*/
						$arrobjAccountingExportAssociations = \Psi\Eos\Entrata\CAccountingExportAssociations::createService()->fetchAccountingExportAssociationsByGlDetailIdByCid( $objGlDetail->getId(), $objGlDetail->getCid(), $objClientDatabase );
						if( false != valArr( $arrobjAccountingExportAssociations ) ) {
							foreach( $arrobjAccountingExportAssociations as $objAccountingExportAssociation ) {
								if( false != valObj( $objAccountingExportAssociation, 'CAccountingExportAssociation' ) && false == $objAccountingExportAssociation->delete( $intCompanyUserId, $objClientDatabase ) ) {
									$boolIsValid = false;
									break 2;
								}
							}
						}

						if( false == $objGlDetail->delete( $intCompanyUserId, $objClientDatabase ) ) {
							$boolIsValid = false;
							break 2;
						}
					}

					foreach( $arrobjGlHeaders as $objGlHeader ) {

						if( false == $objGlHeader->delete( $intCompanyUserId, $objClientDatabase ) ) {
							$boolIsValid = false;
							break 2;
						}
					}
				}
		}

		return $boolIsValid;
	}

	public function delete( $intCompanyUserId, $objClientDatabase, $boolIsAllowDelete = false ) {

		if( true == $boolIsAllowDelete ) {
			return parent::delete( $intCompanyUserId, $objClientDatabase );
		}

		// Steps Involved.
		// 1. Create negative invoice.
		// 2. Update the status of original as well as negative as 'Deleted'.
		// 3. Insert and post the negative invoice.
		// 4. Reference the original & negative one by setting ap_header_id on ap_header and line items by setting ap_detail_id on ap_details.

		$arrobjClonedApDetails				= [];
		$arrobjClonedApHeaderLeaseCustomers	= [];

		$arrobjApDetails		= ( array ) $this->fetchApDetails( $objClientDatabase );

		// Create negative invoice and set status 'Deleted'.
		$objClonedApHeader = clone $this;

		$objClonedApHeader->setId( NULL );
		$objClonedApHeader->setReversalApHeaderId( $this->getId() );
		$objClonedApHeader->setApPayeeId( $this->getApPayeeId() );
		$objClonedApHeader->setApPayeeLocationId( $this->getApPayeeLocationId() );
		$objClonedApHeader->setApPayeeAccountId( $this->getApPayeeAccountId() );
		$objClonedApHeader->setApRemittanceId( $this->getApRemittanceId() );
		$objClonedApHeader->setTransactionAmount( $this->getTransactionAmount() * -1 );
		$objClonedApHeader->setTransactionDatetime( 'NOW()' );
		$objClonedApHeader->setIsPosted( false );
		$objClonedApHeader->setIsTemporary( true );
		$objClonedApHeader->setPoApHeaderIds( NULL );

		if( false == is_null( $this->getReversePostDate() ) ) {
			$objClonedApHeader->setPostDate( date( 'm/d/Y', strtotime( $this->getReversePostDate() ) ) );
		} else {
			$objClonedApHeader->setPostDate( date( 'm/d/Y' ) );
		}

		if( false == is_null( $this->getReversePostMonth() ) ) {
			$objClonedApHeader->setPostMonth( $this->getReversePostMonth() );
		} else {
			$objClonedApHeader->loadDefaultDeleteData( $arrobjApDetails, $objClientDatabase );
		}

		foreach( $arrobjApDetails as $objApDetail ) {

			$objApDetail->setIsSkipTrigger( false );
			$objApDetail->setPoApDetailId( NULL );

			$objClonedApDetail = clone $objApDetail;
			$objClonedApDetail->setId( NULL );
			$objClonedApDetail->setApHeaderId( NULL );
			$objClonedApDetail->setReversalApDetailId( $objApDetail->getId() );
			$objClonedApDetail->setApExportBatchId( NULL );
			$objClonedApDetail->setTransactionDatetime( 'NOW()' );
			$objClonedApDetail->setRate( $objClonedApDetail->getRate() * -1 );
			$objClonedApDetail->setSubtotalAmount( $objClonedApDetail->getSubtotalAmount() * -1 );
			$objClonedApDetail->setTaxAmount( $objClonedApDetail->getTaxAmount() * -1 );
			$objClonedApDetail->setShippingAmount( $objClonedApDetail->getShippingAmount() * -1 );
			$objClonedApDetail->setDiscountAmount( $objClonedApDetail->getDiscountAmount() * -1 );
			$objClonedApDetail->setTransactionAmount( $objClonedApDetail->getTransactionAmount() * -1 );
			$objClonedApDetail->setRetentionAmount( $objClonedApDetail->getRetentionAmount() * -1 );
			$objClonedApDetail->setPreApprovalAmount( $objClonedApDetail->getPreApprovalAmount() * -1 );
			$objClonedApDetail->setUpdatedOn( NULL );
			$objClonedApDetail->setUpdatedBy( NULL );
			$objClonedApDetail->setExportedBy( NULL );
			$objClonedApDetail->setExportedOn( NULL );

			$arrobjClonedApDetails[] = $objClonedApDetail;
		}

		// fetch ap_headers_lease_customers for $this
		if( false == is_null( $this->getLeaseCustomerId() ) ) {

			$arrobjApHeaderLeaseCustomers = ( array ) \Psi\Eos\Entrata\CApHeaderLeaseCustomers::createService()->fetchApHeaderLeaseCustomersByApHeaderIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

			foreach( $arrobjApHeaderLeaseCustomers as $objApHeaderLeaseCustomer ) {

				$objClonedApHeaderLeaseCustomer = clone $objApHeaderLeaseCustomer;
				$objClonedApHeaderLeaseCustomer->setId( NULL );
				$objClonedApHeaderLeaseCustomer->setApHeaderId( NULL );

				$arrobjClonedApHeaderLeaseCustomers[] = $objClonedApHeaderLeaseCustomer;
			}

			$objClonedApHeader->setApHeaderLeaseCustomers( $arrobjClonedApHeaderLeaseCustomers );
		}

		switch( NULL ) {
			default:

				// Insert negative invoice header
				$objClonedApHeader->setDetails( NULL );
				if( false == $objClonedApHeader->insert( $intCompanyUserId, $objClientDatabase, false, false, false, true, false ) ) {
					return false;
				}

				$arrobjClonedApHeaderLeaseCustomers = ( array ) $objClonedApHeader->getApHeaderLeaseCustomers();
				foreach( $arrobjClonedApHeaderLeaseCustomers as $objApHeaderLeaseCustomer ) {

					$objApHeaderLeaseCustomer->setApHeaderId( $objClonedApHeader->getId() );

					if( false == $objApHeaderLeaseCustomer->insert( $intCompanyUserId, $objClientDatabase ) ) {
						return false;
					}
				}

				// calculate post_month and allocation date time for inserting charge to charge allocations
				$strPostMonth			= ( strtotime( $this->getPostMonth() ) > strtotime( $objClonedApHeader->getPostMonth() ) ) ? $this->getPostMonth() : $objClonedApHeader->getPostMonth();
				$strAllocationDatetime	= ( strtotime( $this->getPostDate() ) > strtotime( $objClonedApHeader->getPostDate() ) ) ? $this->getPostDate() : $objClonedApHeader->getPostDate();

				foreach( $arrobjClonedApDetails as $objClonedApDetail ) {

					$objClonedApDetail->setApHeaderId( $objClonedApHeader->getId() );
				}

				if( false == \Psi\Eos\Entrata\CApDetails::createService()->bulkInsert( $arrobjClonedApDetails, $intCompanyUserId, $objClientDatabase, true ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert ap details.', NULL ) );
					return false;
				}

				foreach( $arrobjClonedApDetails as $objClonedApDetail ) {

					if( true == array_key_exists( $objClonedApDetail->getReversalApDetailId(), $arrobjApDetails ) ) {
						$arrobjApDetails[$objClonedApDetail->getReversalApDetailId()]->setReversalApDetailId( $objClonedApDetail->getId() );
					}

					// if ap header is posted to GL then create charge to charge ap allocation
					if( 0 == $this->getIsTemporary() ) {
						$objApAllocation = $objClonedApDetail->createApAllocation( $strPostMonth );

						$fltAllocationAmount = 0.0;
						$fltAllocationAmount = $objClonedApDetail->getTransactionAmount();

						if( 0 > $fltAllocationAmount ) {
							$objApAllocation->setCreditApDetailId( $objClonedApDetail->getId() );
							$objApAllocation->setCreditApTransactionTypeId( $objClonedApDetail->getApTransactionTypeId() );
							$objApAllocation->setChargeApDetailId( $objClonedApDetail->getReversalApDetailId() );
							$objApAllocation->setChargeApTransactionTypeId( $arrobjApDetails[$objClonedApDetail->getReversalApDetailId()]->getApTransactionTypeId() );
						} else {
							$objApAllocation->setCreditApDetailId( $objClonedApDetail->getReversalApDetailId() );
							$objApAllocation->setCreditApTransactionTypeId( $arrobjApDetails[$objClonedApDetail->getReversalApDetailId()]->getApTransactionTypeId() );
							$objApAllocation->setChargeApDetailId( $objClonedApDetail->getId() );
							$objApAllocation->setChargeApTransactionTypeId( $objClonedApDetail->getApTransactionTypeId() );
						}

						$objApAllocation->setAllocationAmount( abs( $fltAllocationAmount ) * -1 );
						$objApAllocation->setPostDate( $strAllocationDatetime );
						$objApAllocation->setAllocationDatetime( $strAllocationDatetime );
						$objApAllocation->setPropertyId( $objClonedApDetail->getPropertyId() );

						if( 0 < $objApAllocation->getAllocationAmount() ) {
							$objApAllocation->setGlTransactionTypeId( CGlTransactionType::AP_ALLOCATION_REVERSAL );
						} else {
							$objApAllocation->setGlTransactionTypeId( CGlTransactionType::AP_ALLOCATION );
						}

						// inserting charge to charge allocations
						if( false == $objApAllocation->validate( 'ap_allocation_insert' ) || false == $objApAllocation->insert( $intCompanyUserId, $objClientDatabase ) ) {
							return false;
						}
					}
				}

				if( true == valArr( $arrobjApDetails ) && false == CApDetails::bulkUpdate( $arrobjApDetails, [ 'is_skip_trigger' ], $intCompanyUserId, $objClientDatabase ) ) {
					$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to update CApDetails.' ) ] );
					$objClientDatabase->rollback();
					break;
				}

				$this->setDetails( $arrobjApDetails );
				$this->refreshField( 'is_posted', 'ap_headers', $objClientDatabase );
				$this->refreshField( 'is_temporary', 'ap_headers', $objClientDatabase );
				if( false == $this->getIsInitialImportPosted() ) {
					// See if this clone is eligible for GL post. GL post happens when is_posted = 0 and is_temporary = 0.
					$objClonedApHeader->setIsTemporary( $this->getIsTemporary() );

					if( false == $objClonedApHeader->update( $intCompanyUserId, $objClientDatabase ) ) {
						return false;
					}
				} else {

					// In reversal_ap_header need to set is_initial_import = False AND is_temporary = False and need to forcefully GL posting
					$objClonedApHeader->setIsPosted( false );
					$objClonedApHeader->setIsTemporary( false );
					$objClonedApHeader->setIsInitialImport( false );
					if( false == $objClonedApHeader->update( $intCompanyUserId, $objClientDatabase ) ) {
						return false;
					}
				}

				// Offset negative invoice
				$this->setReversalApHeaderId( $objClonedApHeader->getId() );

				// Update original invoice
				if( false == $this->update( $intCompanyUserId, $objClientDatabase ) ) {
					return false;
				}
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objClientDatabase, $boolIsLog = true ) {

		if( false == parent::insert( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		$boolIsValid            = true;
		$boolHasReimbursements  = false;

		if( true == valArr( $this->getDetails() ) ) {

			$arrintApHeaderSubTypeIds = [ CApHeaderSubType::JOB_PHASE_BUDGET, CApHeaderSubType::JOB_BUDGET_SUMMARY, CApHeaderSubType::JOB_BUDGET_ADJUSTMENT, CApHeaderSubType::JOB_CHANGE_ORDER ];

			foreach( $this->getDetails() as $objApDetail ) {

				$objApDetail->setApHeaderId( $this->getId() );

				// added this condition for setting NULL values for ap_details records which we don't have set ap_headers.po_ap_header_ids
				// for the first half here, how do we expect that calling set null when we get null does something? is ARRAY[NULL]::INT[] being fetched from the db as simple NULL in php somehow?
				if( NULL == $this->getPoApHeaderIds() || $this->getPoApHeaderIds()[0] == 0 ) {
					$objApDetail->setPoApDetailId( NULL );
				}

				if( true == in_array( $this->getApHeaderSubTypeId(), $arrintApHeaderSubTypeIds ) ) {
					$objApDetail->setBudgetApHeaderId( $this->getId() );
				}

				$objManagementFee = $objApDetail->getManagementFee();

				if( true == valObj( $objManagementFee, 'CFee' ) ) {
					$objApDetail->setFeeId( $objManagementFee->getId() );
				}

				if( CApTransactionType::STANDARD_REIMBURSEMENT_NEW == $objApDetail->getApTransactionTypeId() ) {
					$boolHasReimbursements = true;
				}
			}

			if( false == CApDetails::bulkInsert( $this->getDetails(), $intCurrentUserId, $objClientDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert ap details.', NULL ) );
				$boolIsValid = false;
				return false;
			}

			// in case of purchasing tool need to set ap_detail_id
			if( CApHeaderType::PURCHASE_ORDER == $this->getApHeaderTypeId() ) {

				$arrintMaintenanceRequestMaterialIds = array_filter( array_keys( rekeyObjects( 'MaintenanceRequestMaterialId', $this->getDetails() ) ) );

				if( true == valIntArr( $arrintMaintenanceRequestMaterialIds ) ) {

					$arrobjMaintenanceRequestMaterials = rekeyObjects( 'Id', ( array ) \Psi\Eos\Entrata\CMaintenanceRequestMaterials::createService()->fetchHasNoPOMaintenanceRequestMaterialsByIdsByCid( $arrintMaintenanceRequestMaterialIds, $this->getCid(), $objClientDatabase ) );

					foreach( $this->getDetails() as $objApDetail ) {

						if( true == isset( $arrobjMaintenanceRequestMaterials[$objApDetail->getMaintenanceRequestMaterialId()] ) ) {

							$arrobjMaintenanceRequestMaterials[$objApDetail->getMaintenanceRequestMaterialId()]->setApDetailId( $objApDetail->getId() );

							if( false == $arrobjMaintenanceRequestMaterials[$objApDetail->getMaintenanceRequestMaterialId()]->update( $intCurrentUserId, $objClientDatabase ) ) {
								$this->addErrorMsgs( $arrobjMaintenanceRequestMaterials[$objApDetail->getMaintenanceRequestMaterialId()]->getErrorMsgs() );
								$boolIsValid = false;
								break 1;
							}
						}
					}
				}
			}

			if( $boolHasReimbursements ) {

				$arrobjApDetailReimbursements = CIntercompanyInvoiceHelper::createApDetailReimbursements( $this->getApDetails() );

				if( valArr( $arrobjApDetailReimbursements )
				 && !\Psi\Eos\Entrata\CApDetailReimbursements::createService()->bulkInsert( $arrobjApDetailReimbursements, $intCurrentUserId, $objClientDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to associate reimbursements.', NULL ) );
					return false;
				}
			}

			if( false == $boolIsValid ) {
				return false;
			}
		}

		// Log History

		if( true == $boolIsLog && true == is_null( $this->getApBatchId() ) && false == $this->logHistory( CApHeaderLog::ACTION_CREATED, $intCurrentUserId, $objClientDatabase ) ) {
			return false;
		}

		return true;
	}

	public function insertOrUpdate( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = false ) {

		if( false == parent::insertOrUpdate( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$objClientDatabase->rollback();
			return false;
		}

		if( true == valArr( $this->getDetails() ) ) {
			foreach( $this->getDetails() as $objApDetail ) {

				$objApDetail->setApHeaderId( $this->getId() );
				if( false == $objApDetail->insertOrUpdate( $intCurrentUserId, $objClientDatabase ) ) {
					$this->addErrorMsgs( $objApDetail->getErrorMsgs() );
					$objClientDatabase->rollback();
					return false;
				}
			}
		}

		return true;
	}

	public function update( $intCurrentUserId, $objClientDatabase, $boolIsAdvanceRouting = false, $boolIsPayment = false, $boolIsUpdate = true ) {

		$boolIsInvoicePosting = false;
		$arrintApHeaders = [];

		if( false == $this->getIsTemporary() && false == $this->getIsPosted() ) {
			$boolIsInvoicePosting = true;
		}

		if( false == parent::update( $intCurrentUserId, $objClientDatabase ) ) {
			$objClientDatabase->rollback();
			return false;
		}

		$arrintApHeaders[$this->getId()] = $this->getId();
		if( true == $boolIsUpdate && false == $boolIsInvoicePosting && true == valArr( $this->getDetails() ) ) {

			foreach( $this->getDetails() as $objApDetail ) {

				if( true == $boolIsAdvanceRouting && false == $boolIsPayment ) {
					$objApDetail->setPreApprovalAmount( NULL );
				}

				if( false == $objApDetail->insertOrUpdate( $intCurrentUserId, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					$this->addErrorMsgs( $objApDetail->getErrorMsgs() );

					return false;
				}
			}
		}

		$strSql		= 'select * from ap_header_amounts_calculations( ARRAY [ ' . sqlIntImplode( $arrintApHeaders ) . ' ] )';
		$arrstrData	= fetchData( $strSql, $objClientDatabase );

		return true;
	}

	public function addPaymentAmountByApPaymentTypeId( $intApPayeeTypeId, $fltPaymentAmount ) {
		if( false == isset( $this->m_arrfltPaymentAmountsByApPaymentTypes[$intApPayeeTypeId] ) ) {
			$this->m_arrfltPaymentAmountsByApPaymentTypes[$intApPayeeTypeId] = 0.00;
		}

		$this->m_arrfltPaymentAmountsByApPaymentTypes[$intApPayeeTypeId] = bcadd( $this->m_arrfltPaymentAmountsByApPaymentTypes[$intApPayeeTypeId], $fltPaymentAmount, 2 );
	}

	public function approvePurchaseOrder( $intCurrentUserId, $objClientDatabase, $boolIsCreatorAutoApproved = false ) {

		$boolIsAutoApproved = $this->getIsAutoApproval();

		$this->setApprovedBy( $intCurrentUserId );
		$this->setApprovedOn( 'NOW()' );
		$this->setApFinancialStatusTypeId( CApFinancialStatusType::APPROVED );
		if( CApHeaderSubType::CATALOG_PURCHASE_ORDER == $this->getApHeaderSubTypeId() && CApPhysicalStatusType::NOT_ORDERED == $this->getApPhysicalStatusTypeId() ) {
			$this->setApPhysicalStatusTypeId( CApPhysicalStatusType::ORDERED );
		}

		if( false == $this->update( $intCurrentUserId, $objClientDatabase, false, false, true ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		// Log History
		$this->setIsAutoApproval( $boolIsAutoApproved );
		$this->setIsPosted( true );	// Setting this explicitely just to avoid wrong entry in the logs.
		$this->setApDetails( $this->fetchApDetails( $objClientDatabase ) );

		if( true == $boolIsCreatorAutoApproved && false == $this->logHistory( CApHeaderLog::ACTION_FINAL_APPROVAL_PO, $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function unapprovePurchaseOrder( $intCurrentUserId, $objClientDatabase ) {
		if( CApHeaderType::PURCHASE_ORDER != $this->getApHeaderTypeId() ) return false;

		$this->setApprovedBy( NULL );
		$this->setApprovedOn( NULL );
		$this->setApFinancialStatusTypeId( CApFinancialStatusType::PENDING );
		if( CApHeaderSubType::CATALOG_PURCHASE_ORDER == $this->getApHeaderSubTypeId() || true == $this->getIsSyncToVa() ) {
			$this->setApPhysicalStatusTypeId( CApPhysicalStatusType::NOT_ORDERED );
		}

		if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function rejectPurchaseOrder( $intCurrentUserId, $objClientDatabase ) {

		if( CApHeaderType::PURCHASE_ORDER != $this->getApHeaderTypeId() ) return false;

		$this->setApprovedBy( NULL );
		$this->setApprovedOn( NULL );
		$this->setApFinancialStatusTypeId( CApFinancialStatusType::REJECTED );

		if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function approveInvoice( $intCurrentUserId, $objClientDatabase, $boolIsCreatorAutoApproved = false ) {
		if( 1 == $this->getIsPosted() ) return true;

		$boolIsAutoApproved = $this->getIsAutoApproval();

		$this->setApprovedBy( SYSTEM_USER_ID );
		$this->setApprovedOn( 'NOW()' );
		$this->setIsTemporary( false );
		$this->setApDetails( NULL );
		$this->setApFinancialStatusTypeId( CApFinancialStatusType::APPROVED );

		if( false == $this->update( $intCurrentUserId, $objClientDatabase, false, false, false ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		// Log History
		$this->setIsAutoApproval( $boolIsAutoApproved );
		$this->setIsPosted( true );	// Setting this explicitely just to avoid wrong entry in the logs.
		$this->setApDetails( $this->fetchApDetails( $objClientDatabase ) );

		if( false == $boolIsCreatorAutoApproved && false == $this->logHistory( CApHeaderLog::ACTION_POSTED, $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function rejectInvoice( $intCurrentUserId, $objClientDatabase ) {

		if( CApHeaderType::INVOICE != $this->getApHeaderTypeId() ) return false;

		$this->setApprovedBy( NULL );
		$this->setApprovedOn( NULL );
		$this->setApFinancialStatusTypeId( CApFinancialStatusType::REJECTED );

		if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function approvePayment( $intCurrentUserId, $objClientDatabase ) {
		if( true != $this->getIsPosted() ) return true;

		$boolIsAutoApproved = $this->getIsAutoApproval();

		$this->setApprovedBy( SYSTEM_USER_ID );
		$this->setApprovedOn( 'NOW()' );
		$this->setIsTemporary( false );
		$this->setApDetails( NULL );
		$this->setApFinancialStatusTypeId( CApFinancialStatusType::APPROVED );
		$this->setPreApprovalAmount( $this->getTransactionAmount() );

		if( false == $this->update( $intCurrentUserId, $objClientDatabase, false, false, false ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		// Log History
		$this->setIsAutoApproval( $boolIsAutoApproved );
		$this->setIsPosted( true );	// Setting this explicitly just to avoid wrong entry in the logs.
		$this->setApDetails( $this->fetchApDetails( $objClientDatabase ) );

		if( false == $this->logHistory( CApHeaderLog::ACTION_FINAL_APPROVAL_PAYMENT, $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function calculateDueDate( $strSystemCode, $strPostDate ) {

		if( false == valStr( $strPostDate ) || false == valStr( $strSystemCode ) ) {
			return false;
		}

		$objDateTime	= new DateTime( $strPostDate );
		$arrstrPostDate	= explode( '/', $strPostDate );
		$intMonth		= ( int ) getArrayElementByKey( 0, $arrstrPostDate );
		$intYear		= ( int ) getArrayElementByKey( 2, $arrstrPostDate );

		if( $intMonth == 12 ) {
			$intMonth = 1;
			$intYear++;
		} else {
			$intMonth++;
		}

		switch( $strSystemCode ) {

			case 'Net5':
				$objDateTime->modify( '+5 days' );
				break;

			case 'Net10':
			case '1%10days':
			case '2%10days':
				$objDateTime->modify( '+10 days' );
				break;

			case 'Net20':
				$objDateTime->modify( '+20 days' );
				break;

			case 'Net30':
				$objDateTime->modify( '+30 days' );
				break;

			case 'Net10EOM':
				$objDateTime = new DateTime( $intMonth . '/10/' . $intYear );
				break;

			case 'Net20EOM':
				$objDateTime = new DateTime( $intMonth . '/20/' . $intYear );
				break;

			case 'Net30EOM':
				$objDateTime = new DateTime( $intMonth . '/30/' . $intYear );
				break;

			default:
					/// custom term
				break;
		}

		return $objDateTime->format( 'm/d/Y' );
	}

	/**
	 * @param $intCurrentUserId
	 * @param $objClientDatabase
	 * @return bool
	 */

	public function updateOrderHeaderId( $intCurrentUserId, $objClientDatabase ) {

		if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
			$objClientDatabase->rollback();
			return false;
		}

		return true;
	}

	/**
	 * Log Related Methods
	 */

	public function isModifiedApHeaderOrApDetails( $objOldApHeader ) {

		$boolChangesMade = false;
		$arrobjOldApDetails = $objOldApHeader->getApDetails();
		$arrobjNewApDetails = $this->getApDetails();
		$arrobjOldApDetails = rekeyObjects( 'Id', $arrobjOldApDetails );
		$arrobjNewApDetails = rekeyObjects( 'Id', $arrobjNewApDetails );

		if( $objOldApHeader->getApPayeeId() != $this->getApPayeeId()
			|| $objOldApHeader->getApPayeeLocationId() != $this->getApPayeeLocationId()
		    || $objOldApHeader->getApRemittanceId() != $this->getApRemittanceId()
		    || $objOldApHeader->getApPayeeAccountId() != $this->getApPayeeAccountId()
			|| $objOldApHeader->getApPayeeTermId() != $this->getApPayeeTermId()
			|| $objOldApHeader->getApBatchId() != $this->getApBatchId()
			|| $objOldApHeader->getApPaymentId() != $this->getApPaymentId()
			|| $objOldApHeader->getGlTransactionTypeId() != $this->getGlTransactionTypeId()
			|| $objOldApHeader->getLeaseCustomerId() != $this->getLeaseCustomerId()
			|| $objOldApHeader->getBulkPropertyId() != $this->getBulkPropertyId()
			|| $objOldApHeader->getBulkCompanyDepartmentId() != $this->getBulkCompanyDepartmentId()
			|| $objOldApHeader->getBulkUnitNumberId() != $this->getBulkUnitNumberId()
			|| $objOldApHeader->getBulkGlDimensionId() != $this->getBulkGlDimensionId()
			|| $objOldApHeader->getBulkIs1099() != $this->getBulkIs1099()
			|| $objOldApHeader->getBulkIsConfidential() != $this->getBulkIsConfidential()
		    || $objOldApHeader->getBulkIsBillback() != $this->getBulkIsBillback()
			|| $objOldApHeader->getReversalApHeaderId() != $this->getReversalApHeaderId()
			|| $objOldApHeader->getRefundArTransactionId() != $this->getRefundArTransactionId()
			|| $objOldApHeader->getApHeaderTypeId() != $this->getApHeaderTypeId()
			|| $objOldApHeader->getRemotePrimaryKey() != $this->getRemotePrimaryKey()
			|| $objOldApHeader->getTransactionDatetime() != $this->getTransactionDatetime()
			|| $objOldApHeader->getPostDate() != $this->getPostDate()
			|| $objOldApHeader->getDueDate() != $this->getDueDate()
			|| $objOldApHeader->getScheduledPaymentDate() != $this->getScheduledPaymentDate()
			|| $objOldApHeader->getApRoutingTagId() != $this->getApRoutingTagId()
			|| $objOldApHeader->getPostMonth() != $this->getPostMonth()
			|| $objOldApHeader->getHeaderNumber() !== $this->getHeaderNumber()
			|| $objOldApHeader->getAccountNumber() != $this->getAccountNumber()
			|| $objOldApHeader->getHeaderMemo() != $this->getHeaderMemo()
			|| $objOldApHeader->getControlTotal() != $this->getControlTotal()
			|| $objOldApHeader->getTaxAmount() != $this->getTaxAmount()
			|| $objOldApHeader->getShippingAmount() != $this->getShippingAmount()
			|| $objOldApHeader->getDiscountAmount() != $this->getDiscountAmount()
			|| $objOldApHeader->getPayWithSingleCheck() != $this->getPayWithSingleCheck()
			|| $objOldApHeader->getIsOnHold() != $this->getIsOnHold()
			|| $objOldApHeader->getIsBatching() != $this->getIsBatching()
			|| $objOldApHeader->getIsTemporary() != $this->getIsTemporary()
			|| $objOldApHeader->getIsPosted() != $this->getIsPosted()
			|| $objOldApHeader->getIsInitialImport() != $this->getIsInitialImport()
			|| $objOldApHeader->getPostedOn() != $this->getPostedOn()
			|| $objOldApHeader->getImportedOn() != $this->getImportedOn()
			|| $objOldApHeader->getApprovedBy() != $this->getApprovedBy()
			|| $objOldApHeader->getApprovedOn() != $this->getApprovedOn()
			|| $objOldApHeader->getUpdatedBy() != $this->getUpdatedBy()
			|| $objOldApHeader->getUpdatedOn() != $this->getUpdatedOn()
			|| $objOldApHeader->getCreatedBy() != $this->getCreatedBy()
			|| $objOldApHeader->getCreatedOn() != $this->getCreatedOn()
			|| $objOldApHeader->getTemplateName() != $this->getTemplateName()
		    || $objOldApHeader->getDayOfWeek() != $this->getDayOfWeek()
		    || $objOldApHeader->getFrequencyId() != $this->getFrequencyId()
		    || $objOldApHeader->getFrequencyInterval() != $this->getFrequencyInterval()
		    || $objOldApHeader->getNumberOfOccurrences() != $this->getNumberOfOccurrences()
		    || $objOldApHeader->getIsTemplate() != $this->getIsTemplate()
		    || $objOldApHeader->getTemplateApHeaderId() != $this->getTemplateApHeaderId()
		    || $objOldApHeader->getTemplateType() != $this->getTemplateType()
		    || $objOldApHeader->getTransactionDatetime() != $this->getTransactionDatetime()
		    || $objOldApHeader->getCreateTemplateFrom() != $this->getCreateTemplateFrom()
			|| $objOldApHeader->getPoApHeaderIds() != $this->getPoApHeaderIds() ) {
				$boolChangesMade = true;
		}

		// Check if line item is inserted.
		if( true == valArr( $arrobjNewApDetails ) ) {
			foreach( $arrobjNewApDetails as $objNewApDetail ) {
				if( false == array_key_exists( $objNewApDetail->getId(), $arrobjOldApDetails ) ) {
					$boolChangesMade = true;
					break;
				}
			}
		}

		if( false == $boolChangesMade && true == valArr( $arrobjOldApDetails ) ) {

			foreach( $arrobjOldApDetails as $objOldApDetail ) {

				// Check if line item is deleted.
				if( true == array_key_exists( $objOldApDetail->getId(), $arrobjNewApDetails ) ) {
					$objNewApDetail = $arrobjNewApDetails[$objOldApDetail->getId()];

					// Check if line item is modified.
					if( $objOldApDetail->getCid() != $objNewApDetail->getCid()
						|| $objOldApDetail->getPropertyId() != $objNewApDetail->getPropertyId()
						|| $objOldApDetail->getPropertyUnitId() != $objNewApDetail->getPropertyUnitId()
						|| $objOldApDetail->getPropertyBuildingId() != $objNewApDetail->getPropertyBuildingId()
						|| $objOldApDetail->getBankAccountId() != $objNewApDetail->getBankAccountId()
						|| $objOldApDetail->getGlAccountId() != $objNewApDetail->getGlAccountId()
						|| $objOldApDetail->getBankGlAccountId() != $objNewApDetail->getBankGlAccountId()
						|| $objOldApDetail->getCompanyDepartmentId() != $objNewApDetail->getCompanyDepartmentId()
						|| $objOldApDetail->getGlDimensionId() != $objNewApDetail->getGlDimensionId()
					    || $objOldApDetail->getApCodeId() != $objNewApDetail->getApCodeId()
						|| $objOldApDetail->getJobPhaseId() != $objNewApDetail->getJobPhaseId()
						|| $objOldApDetail->getApContractId() != $objNewApDetail->getApContractId()
						|| $objOldApDetail->getIsConfidential() != $objNewApDetail->getIsConfidential()
						|| $objOldApDetail->getRefundArTransactionId() != $objNewApDetail->getRefundArTransactionId()
						|| $objOldApDetail->getFeeId() != $objNewApDetail->getFeeId()
						|| $objOldApDetail->getQuantityOrdered() != $objNewApDetail->getQuantityOrdered()
						|| $objOldApDetail->getRate() != $objNewApDetail->getRate()
						|| $objOldApDetail->getTransactionAmount() != $objNewApDetail->getTransactionAmount()
						|| $objOldApDetail->getPreApprovalAmount() != $objNewApDetail->getPreApprovalAmount()
						|| $objOldApDetail->getTaxAmount() != $objNewApDetail->getTaxAmount()
						|| $objOldApDetail->getShippingAmount() != $objNewApDetail->getShippingAmount()
						|| $objOldApDetail->getDiscountAmount() != $objNewApDetail->getDiscountAmount()
						|| $objOldApDetail->getSubtotalAmount() != $objNewApDetail->getSubtotalAmount()
						|| $objOldApDetail->getDescription() != $objNewApDetail->getDescription()
						|| $objOldApDetail->getPaymentApprovedBy() != $objNewApDetail->getPaymentApprovedBy()
						|| $objOldApDetail->getPaymentApprovedOn() != $objNewApDetail->getPaymentApprovedOn()
						|| $objOldApDetail->getIsApprovedForPayment() != $objNewApDetail->getIsApprovedForPayment()
						|| $objOldApDetail->getIs1099() != $objNewApDetail->getIs1099()
						|| $objOldApDetail->getApprovedBy() != $objNewApDetail->getApprovedBy()
						|| $objOldApDetail->getApprovedOn() != $objNewApDetail->getApprovedOn()
					    || $objOldApDetail->getRetentionPercent() != $objNewApDetail->getRetentionPercent()
					    || $objOldApDetail->getUnitTypeId() != $objNewApDetail->getUnitTypeId()
					    || $objOldApDetail->getMaintenanceLocationId() != $objNewApDetail->getMaintenanceLocationId()
					    || $objOldApDetail->getReimbursementMethodId() != $objNewApDetail->getReimbursementMethodId()
						|| $objOldApDetail->getPoApDetailId() != $objNewApDetail->getPoApDetailId() ) {
						$boolChangesMade = true;
						break;
					}
				} else {
					$boolChangesMade = true;
					break;
				}
			}
		}

		return $boolChangesMade;
	}

	public function isReconciledOrExportedLineItem( $arrobjGlDetails ) {
		$boolIsReconciledOrExportedLineItem = false;
		foreach( $arrobjGlDetails as $objGlDetail ) {
			if( NULL != $objGlDetail->getGlReconciliationId() || NULL != $objGlDetail->getAccountingExportBatchId() ) {
				$boolIsReconciledOrExportedLineItem = true;
				break;
			}
		}
		return $boolIsReconciledOrExportedLineItem;
	}

	public function createApHeaderLog() {
		$objApHeaderLog = new CApHeaderLog();
		$objApHeaderLog->mapApHeaderLog( $this );
		return $objApHeaderLog;
	}

	public function createApHeaderLeaseCustomer() {
		$objApHeaderLeaseCustomer = new CApHeaderLeaseCustomer();
		$objApHeaderLeaseCustomer->setCid( $this->getCid() );
		$objApHeaderLeaseCustomer->setApHeaderId( $this->getId() );
		return $objApHeaderLeaseCustomer;
	}

	public function createApDetailLogs() {

		$arrobjApDetailLog	= [];
		$arrobjApDetails	= ( array ) $this->getApDetails();

		foreach( $arrobjApDetails as $objApDetail ) {
			$objApDetailLog = new CApDetailLog();
			assertObj( $objApDetail, 'CApDetail' );
			$objApDetailLog->mapApDetailLog( $objApDetail );
			$arrobjApDetailLog[] = $objApDetailLog;
		}
		return $arrobjApDetailLog;
	}

	public function updateGlDetails( $arrobjOldApDetails, $intInterCompanyApHeaderId, $intCompanyUserId, $objClientDatabase ) {

		$arrobjGlHeaders				= [];
		$arrobjGlDetails				= [];
		$arrintApHeaderIds				= [ $this->getId() ];
		$arrintModifiedApDetailIds		= [];
		$arrobjModifiedBankApDetails	= [];
		$arrobjTempGlDetails            = [];

		if( true == valId( $intInterCompanyApHeaderId ) ) {
			$arrintApHeaderIds[]	= $intInterCompanyApHeaderId;
			$arrobjGlHeaders		= ( array ) CGlHeaders::fetchActiveGlHeadersByApHeaderIdsByCid( $arrintApHeaderIds, $this->getCid(), $objClientDatabase );
		} else {
			$arrobjGlHeaders		= ( array ) CGlHeaders::fetchActiveGlHeadersByApHeaderIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		}

		if( false == valArr( $arrobjGlHeaders ) && true != $this->getIsInitialImport() ) {
			return true;
		}

		// Update the GlHeader if post month is updated
		$arrobjGlDetails    = rekeyObjects( 'GlHeaderId', CGlDetails::fetchAllGlDetailsByGlHeaderIdsByCid( array_keys( $arrobjGlHeaders ), $this->getCid(), $objClientDatabase ), true );

		foreach( $arrobjGlHeaders as $objGlHeader ) {
			if( $objGlHeader->getPostMonth() != $this->getPostMonth() ) {
				$objGlHeader->setPostMonth( $this->getPostMonth() );
				foreach( $arrobjGlDetails[$objGlHeader->getId()] as $objGlDetail ) {
					$objGlDetail->setPostMonth( $this->getPostMonth() );
					$arrobjTempGlDetails[$objGlDetail->getId()]  = $objGlDetail;
				}
			}
		}

		if( true == valArr( $arrobjGlHeaders ) && false == CGlHeaders::bulkUpdate( $arrobjGlHeaders, [ 'post_month' ], $intCompanyUserId, $objClientDatabase ) ) {
			return false;
		}

		if( true == valArr( $arrobjTempGlDetails ) && false == CGlDetails::bulkUpdate( $arrobjTempGlDetails, [ 'post_month' ], $intCompanyUserId, $objClientDatabase ) ) {
			return false;
		}

		// Check if Gl Accounts are modified
		foreach( $this->m_arrobjApDetails as $objNewApDetail ) {

			if( $objNewApDetail->getGlAccountId() != $arrobjOldApDetails[$objNewApDetail->getId()]->getGlAccountId()
				|| $objNewApDetail->getCompanyDepartmentId() != $arrobjOldApDetails[$objNewApDetail->getId()]->getCompanyDepartmentId()
				|| $objNewApDetail->getPropertyBuildingId() != $arrobjOldApDetails[$objNewApDetail->getId()]->getPropertyBuildingId()
				|| $objNewApDetail->getPropertyUnitId() != $arrobjOldApDetails[$objNewApDetail->getId()]->getPropertyUnitId()
				|| $objNewApDetail->getGlDimensionId() != $arrobjOldApDetails[$objNewApDetail->getId()]->getGlDimensionId()
				|| $objNewApDetail->getUnitTypeId() != $arrobjOldApDetails[$objNewApDetail->getId()]->getUnitTypeId()
				|| $objNewApDetail->getMaintenanceLocationId() != $arrobjOldApDetails[$objNewApDetail->getId()]->getMaintenanceLocationId() ) {
				$arrintModifiedApDetailIds[] = $objNewApDetail->getId();
			}

			if( $objNewApDetail->getBankAccountId() != $arrobjOldApDetails[$objNewApDetail->getId()]->getBankAccountId() ) {
				$arrobjModifiedBankApDetails[] = $objNewApDetail;
			}
		}

		if( true == valArr( $arrobjModifiedBankApDetails ) ) {

			$this->m_arrobjPropertyBankAccounts	= ( array ) CPropertyBankAccounts::fetchPropertyBankAccountsByPropertyIdsByCid( array_keys( rekeyObjects( 'PropertyId', $arrobjModifiedBankApDetails ) ), $this->getCid(), $objClientDatabase, true );

			foreach( $arrobjModifiedBankApDetails as $objNewApDetail ) {
			// set new bank gl account here and update.
				$objPropertyBankAccount = $this->getPropertyBankAccountFromPropertyBankAccounts( $objNewApDetail->getBankAccountId(), $objNewApDetail->getPropertyId(), 0 );
				if( true == valObj( $objPropertyBankAccount, 'CPropertyBankAccount' ) ) {
					$objNewApDetail->setBankGlAccountId( $objPropertyBankAccount->getGlAccountId() );
				}

				if( false == $objNewApDetail->update( $intCompanyUserId, $objClientDatabase ) ) {
					return false;
				}
			}
		}

		if( true == valArr( $arrintModifiedApDetailIds ) ) {

			$arrobjTempGlDetails = ( array ) CGlDetails::fetchActiveGlDetailsByApDetailIdsByCid( $arrintModifiedApDetailIds, $this->getCid(), $objClientDatabase );

			foreach( $arrobjTempGlDetails as $objGlDetail ) {

				$intModifiedApDetailId = $objGlDetail->getApDetailId();
				if( $objGlDetail->getAccrualGlAccountId() == $arrobjOldApDetails[$intModifiedApDetailId]->getGlAccountId() ) {
					$objGlDetail->setAccrualGlAccountId( $this->m_arrobjApDetails[$intModifiedApDetailId]->getGlAccountId() );
				}

				$objGlDetail->setCompanyDepartmentId( $this->m_arrobjApDetails[$intModifiedApDetailId]->getCompanyDepartmentId() );
				$objGlDetail->setPropertyBuildingId( $this->m_arrobjApDetails[$intModifiedApDetailId]->getPropertyBuildingId() );
				$objGlDetail->setPropertyUnitId( $this->m_arrobjApDetails[$intModifiedApDetailId]->getPropertyUnitId() );
				$objGlDetail->setGlDimensionId( $this->m_arrobjApDetails[$intModifiedApDetailId]->getGlDimensionId() );
				$objGlDetail->setUnitTypeId( $this->m_arrobjApDetails[$intModifiedApDetailId]->getUnitTypeId() );
				$objGlDetail->setMaintenanceLocationId( $this->m_arrobjApDetails[$intModifiedApDetailId]->getMaintenanceLocationId() );
			}

			if( true == valArr( $arrobjTempGlDetails ) && false == CGlDetails::bulkUpdate( $arrobjTempGlDetails, [ 'accrual_gl_account_id', 'company_department_id', 'property_unit_id', 'property_building_id', 'gl_dimension_id', 'unit_type_id', 'maintenance_location_id' ], $intCompanyUserId, $objClientDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function logHistory( $strAction, $intCompanyUserId, $objClientDatabase, $arrintPaymentApHeaderIds = [], $fltRetentionAmountToRelease = NULL, $strReversalHeaderMemo = NULL ) {

		$boolIsValid 				= true;
		$objApHeaderLog 			= $this->createApHeaderLog();
		$arrobjApDetailLogs			= ( array ) $this->createApDetailLogs();
		$arrintTotalItemReceived	= [];

		if( false == is_null( $objApHeaderLog ) && true == valArr( $arrobjApDetailLogs ) ) {

			$objApHeaderLog->setAction( $strAction );
			$objApHeaderLog->setUpdatedBy( $intCompanyUserId );
			$objApHeaderLog->setUpdatedOn( 'NOW()' );

			if( CApHeaderLog::ACTION_RETENTION_RELEASED == $strAction && !empty( $fltRetentionAmountToRelease ) ) {
				$objApHeaderLog->setRetentionAmountReleased( $fltRetentionAmountToRelease );
			}

			if( CApHeaderLog::ACTION_PAID == $strAction && true == valArr( $arrintPaymentApHeaderIds ) ) {
				$objApHeaderLog->setHeaderNumber( $this->getHeaderNumber() . '~^~' . implode( ', ', $arrintPaymentApHeaderIds ) );
			}

			if( CApHeaderLog::ACTION_REVERSED == $strAction && true == valStr( $strReversalHeaderMemo ) ) {
				$objApHeaderLog->setHeaderMemo( $strReversalHeaderMemo );
			}

			if( false == $objApHeaderLog->insert( $intCompanyUserId, $objClientDatabase ) ) {
				$this->addErrorMsgs( $objApHeaderLog->getErrorMsgs() );
				return false;
			}

			foreach( $arrobjApDetailLogs as $objApDetailLog ) {

				$objApDetailLog->setApHeaderLogId( $objApHeaderLog->getId() );
				$objApDetailLog->setUpdatedBy( $intCompanyUserId );
				$objApDetailLog->setUpdatedOn( 'NOW()' );
				$objApDetailLog->setPostMonth( $this->getPostMonth() );
			}

			if( true == valArr( $arrobjApDetailLogs ) && false == \Psi\Eos\Entrata\CApDetailLogs::createService()->bulkInsert( $arrobjApDetailLogs, $intCompanyUserId, $objClientDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert po ap detail logs.', NULL ) );
				return false;
			}

			if( true == $this->getIsAutoApproval() && CApHeaderType::PURCHASE_ORDER == $this->getApHeaderTypeId() ) {
				return $boolIsValid;
			}
		}

		if( ( true == $this->getIsAutoApproval() || CApHeaderLog::ACTION_CLOSED == $strAction || CApHeaderLog::ACTION_OPENED == $strAction ) && CApHeaderType::PURCHASE_ORDER == $this->getApHeaderTypeId() ) {

			$objCloneApHeaderLog = clone $objApHeaderLog;
			$objCloneApHeaderLog->setId( NULL );

			if( CApHeaderLog::ACTION_CLOSED == $strAction ) {
				$objCloneApHeaderLog->setAction( CApHeaderLog::ACTION_CLOSED );
			} elseif( CApHeaderLog::ACTION_OPENED == $strAction ) {
				$objCloneApHeaderLog->setAction( CApHeaderLog::ACTION_OPENED );
				$objCloneApHeaderLog->setUpdatedBy( $intCompanyUserId );
				$objCloneApHeaderLog->setUpdatedOn( 'NOW()' );
			} else {
				$objCloneApHeaderLog->setAction( CApHeaderLog::ACTION_APPROVED );
			}

			if( false == $objCloneApHeaderLog->insert( $intCompanyUserId, $objClientDatabase ) ) {
				$this->addErrorMsgs( $objCloneApHeaderLog->getErrorMsgs() );
				return false;
			}

			foreach( $arrobjApDetailLogs as $objApDetailLog ) {

				$objApDetailLog->setApHeaderLogId( $objCloneApHeaderLog->getId() );
				$objApDetailLog->setUpdatedBy( $intCompanyUserId );
				$objApDetailLog->setUpdatedOn( 'NOW()' );
				$objApDetailLog->setPostMonth( $this->getPostMonth() );
				$objApDetailLog->setId( NULL );

			}

			if( true == valArr( $arrobjApDetailLogs ) && false == \Psi\Eos\Entrata\CApDetailLogs::createService()->bulkInsert( $arrobjApDetailLogs, $intCompanyUserId, $objClientDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert po ap detail logs.', NULL ) );
				return false;
			}
		}

		return $boolIsValid;
	}

	public function getPropertyBankAccountFromPropertyBankAccounts( $intBankAccountId, $intPropertyId, $intIsDueToFrom ) {

		if( NULL == $intPropertyId ) {
			foreach( $this->m_arrobjPropertyBankAccounts as $objPropertyBankAccount ) {
				if( $intBankAccountId == $objPropertyBankAccount->getBankAccountId() && $intIsDueToFrom == $objPropertyBankAccount->getIsReimbursedProperty() ) {
					return $objPropertyBankAccount;
				}
			}
		} else {
			foreach( $this->m_arrobjPropertyBankAccounts as $objPropertyBankAccount ) {
				if( $intBankAccountId == $objPropertyBankAccount->getBankAccountId() && $intPropertyId == $objPropertyBankAccount->getPropertyId() && $intIsDueToFrom == $objPropertyBankAccount->getIsReimbursedProperty() ) {
					return $objPropertyBankAccount;
				}
			}
		}
		return false;
	}

	public function loadDefaultDeleteData( $arrobjApDetails, $objClientDatabase ) {

		// if no line item present
		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrobjApDetails ) ) {
			$this->setPostMonth( date( 'm/01/Y' ) );
			return;
		}

		$arrobjPropertyGlSettings 	= ( array ) CPropertyGlSettings::fetchPropertyGlSettingsByPropertyIdsByCid( array_filter( array_keys( rekeyObjects( 'PropertyId', $arrobjApDetails ) ) ), $this->getCid(), $objClientDatabase );

		foreach( $arrobjPropertyGlSettings as $objPropertyGlSetting ) {
			$arrstrApPostMonths[$objPropertyGlSetting->getPropertyId()]	 = $objPropertyGlSetting->getApPostMonth();
		}

		if( false == valArr( $arrobjPropertyGlSettings ) ) {
			$this->setPostMonth( date( 'm/01/Y' ) );
			return;
		}

		$arrstrApPostMonths	= array_unique( $arrstrApPostMonths );
		// if invoice having single property
		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrApPostMonths ) ) {
			$this->setPostMonth( current( $arrstrApPostMonths ) );
		} else {
			$this->setPostMonth( max( $arrstrApPostMonths ) );
		}

		return;
	}

	public function validateApHeadersForPayments( $arrintApHeaderIds, $intCid, $objCompanyUser, $objClientDatabase ) {

		$boolIsValid			= true;
		$strErrorMsg			= '';
		$arrstrErrorApHeaders	= [];
		$arrmixParameters = [
			'company_user' => $objCompanyUser,
			'client_database' => $objClientDatabase
		];

		if( false == valIntArr( $arrintApHeaderIds ) ) {
			return 'At least one refund deposit should be selected ';
		}

		$arrobjApHeaders = ( array ) CApHeaders::fetchApHeadersWithPayeeNameByIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase );

		foreach( $arrobjApHeaders as $objApHeader ) {

			$boolIsValid &= $objApHeader->validate( 'validate_for_payment', $arrmixParameters );

			if( false == $boolIsValid ) {
				$arrstrErrorApHeaders[] = $objApHeader->getHeaderNumber();
			}
		}

		if( 1 <= \Psi\Libraries\UtilFunctions\count( $arrstrErrorApHeaders ) ) {
			return 'Please make sure refund invoices are posted and approved before creating payments.';
		}

		return $strErrorMsg;
	}

	public function updatePoStatus( $arrmixParameters, $objClientDatabase, $boolIsCreditMemoInvoice = false ) {

		$arrobjAssetTransactionsByPoApDetailIds	= getArrayElementByKey( 'asset_transactions_by_po_ap_details_id', $arrmixParameters );
		$arrobjApDetails						= getArrayElementByKey( 'ap_details', $arrmixParameters );
		$boolIsAllItemsReceived					= getArrayElementByKey( 'is_all_items_received', $arrmixParameters );
		$intCurrentUserId						= getArrayElementByKey( 'current_user_id', $arrmixParameters );

		if( CApHeaderType::PURCHASE_ORDER == $this->getApHeaderTypeId() && CApHeaderSubType::CATALOG_PURCHASE_ORDER == $this->getApHeaderSubTypeId() ) {

			if( true == $boolIsCreditMemoInvoice ) {
				return true;
			}

			if( false == valArr( $arrobjAssetTransactionsByPoApDetailIds ) ) {

				if( true == $boolIsAllItemsReceived ) {
					$this->setApFinancialStatusTypeId( CApFinancialStatusType::APPROVED );

					if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
						return false;
					}

					return true;
				}
				return false;
			}

			$arrobjAssetTransactionsToUpdate	= [];
			$arrobjAssetTransactionsNonInvoiced	= [];

			foreach( $arrobjAssetTransactionsByPoApDetailIds as $objAssetTransaction ) {

				if( true == array_key_exists( $objAssetTransaction->getInvoiceApDetailId(), $arrobjApDetails ) ) {
					$arrobjAssetTransactionsToUpdate[$objAssetTransaction->getInvoiceApDetailId()] = $objAssetTransaction;
				} elseif( true == is_null( $objAssetTransaction->getInvoiceApDetailId() ) ) {
					break;
				} else {
					$arrobjAssetTransactionsNonInvoiced[$objAssetTransaction->getInvoiceApDetailId()] = $objAssetTransaction;
				}
			}

			foreach( $arrobjAssetTransactionsToUpdate as $objAssetTransaction ) {

				$objAssetTransaction->setInvoiceApDetailId( NULL );

				if( false == $objAssetTransaction->update( $intCurrentUserId, $objClientDatabase ) ) {
					$this->addSessionErrorMsgs( $objAssetTransaction->getErrorMsgs() );
					$objClientDatabase->rollback();
					return false;
				}
			}

			if( true == valArr( $arrobjAssetTransactionsNonInvoiced ) ) {
				$this->setApFinancialStatusTypeId( CApFinancialStatusType::PARTIALLY_INVOICED );
			} else {
				$this->setApFinancialStatusTypeId( CApFinancialStatusType::APPROVED );
			}
		} elseif( CApHeaderType::PURCHASE_ORDER == $this->getApHeaderTypeId() && ( CApHeaderSubType::STANDARD_PURCHASE_ORDER == $this->getApHeaderSubTypeId() || CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER == $this->getApHeaderSubTypeId() ) ) {

			// getting current PO header's Ap details
			$arrobjOriginalPoApDetailIds 		= array_keys( ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchApDetailsByApHeaderIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase ) );
			// getting invoices by po_ap_header_id
			$arrintApHeaderIds				= array_keys( rekeyArray( 'id', ( array ) CApHeaders::fetchApHeadersByPoApHeaderIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase ) ) );
		   // getting Invoiced PO Ap Details
			$boolHasJobCostingProduct	= ( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER == $this->getApHeaderSubTypeId() );
			$arrobjInvoicedPoApDetails	= ( array ) CApDetails::fetchApDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $this->getCid(), $this->m_objDatabase, NULL, false, false, $boolHasJobCostingProduct );

			$arrintInvoicedPoApDetailIds	= valArr( $arrobjInvoicedPoApDetails ) ? array_intersect( $arrobjOriginalPoApDetailIds, array_keys( rekeyObjects( 'PoApDetailId', $arrobjInvoicedPoApDetails ) ) ) : [];

			if( \Psi\Libraries\UtilFunctions\count( $arrobjOriginalPoApDetailIds ) == \Psi\Libraries\UtilFunctions\count( array_keys( $arrintInvoicedPoApDetailIds ) ) ) {
				$this->setApFinancialStatusTypeId( CApFinancialStatusType::CLOSED );
			} elseif( valArr( $arrintInvoicedPoApDetailIds ) && 0 != \Psi\Libraries\UtilFunctions\count( $arrintInvoicedPoApDetailIds ) ) {
				$this->setApFinancialStatusTypeId( CApFinancialStatusType::PARTIALLY_INVOICED );
			} else {
				$this->setApFinancialStatusTypeId( CApFinancialStatusType::APPROVED );
			}

			if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {

				return false;
			}

			return true;
		}

		if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
			return false;
		}

		return true;
	}

	public function updateStatus() {

		if( false == valArr( $this->getApDetails() ) ) return true;

		$boolIsAnyItemReceived			= false;
		$arrintApPhysicalStatusTypeIds	= [];

		foreach( $this->getApDetails() as $objApDetail ) {

			if( false == $boolIsAnyItemReceived
				&& ( CApPhysicalStatusType::RECEIVED == $objApDetail->getApPhysicalStatusTypeId() || CApPhysicalStatusType::PARTIALLY_RECEIVED == $objApDetail->getApPhysicalStatusTypeId() ) ) {
				$boolIsAnyItemReceived = true;
			}

			$arrintApPhysicalStatusTypeIds[$objApDetail->getApPhysicalStatusTypeId()] = $objApDetail->getApPhysicalStatusTypeId();
		}

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintApPhysicalStatusTypeIds ) ) {
			$this->setApPhysicalStatusTypeId( current( $arrintApPhysicalStatusTypeIds ) );
		} elseif( true == $boolIsAnyItemReceived ) {
			$this->setApPhysicalStatusTypeId( CApPhysicalStatusType::PARTIALLY_RECEIVED );
		}

		return true;
	}

	public function isModifiedPurchaseOrder( $objOldApHeader, $objClientDatabase, $boolIsAdvanceRouting = false ) {

		$boolChangesMade = false;
		$this->m_boolIsModified = false;

		if( strtotime( $objOldApHeader->getPostDate() ) != strtotime( $this->getPostDate() )
		    || $objOldApHeader->getHeaderMemo() != $this->getHeaderMemo()
		    || $objOldApHeader->getPostMonth() != $this->getPostMonth() ) {
			$this->m_boolIsModified = true;
		}

		$objApprovalPreference = CApprovalPreferences::fetchApprovalPreferenceByRouteTypeIdByCid( CRouteType::PURCHASE_ORDER, $this->getCid(), $objClientDatabase );

		// Compare if the old object and new object has the different value.
		if( strtotime( $objOldApHeader->getPostDate() ) != strtotime( $this->getPostDate() )
			|| $objOldApHeader->getHeaderMemo() != $this->getHeaderMemo()
			|| $objOldApHeader->getPostMonth() != $this->getPostMonth() || ( true == $boolIsAdvanceRouting && true == valObj( $objApprovalPreference, 'CApprovalPreference' ) && true == $objApprovalPreference->getResetRouteOnEdit() ) ) {
			$boolChangesMade = true;
		}

		$arrobjOldApDetails	= $objOldApHeader->getApDetails();
		$arrobjNewApDetails	= $this->getApDetails();

		// Check if line item is inserted.
		if( true == valArr( $arrobjNewApDetails ) ) {
			foreach( $arrobjNewApDetails as $objNewApDetail ) {
				if( false == array_key_exists( $objNewApDetail->getId(), $arrobjOldApDetails ) ) {
					$boolChangesMade = true;
                    $this->m_boolIsModified = true;
					break;
				}
			}
		}

		foreach( $arrobjOldApDetails as $objOldApDetail ) {

			if( true == array_key_exists( $objOldApDetail->getId(), $arrobjNewApDetails ) ) {

				$objNewApDetail = $arrobjNewApDetails[$objOldApDetail->getId()];

				if( $objOldApDetail->getPropertyId() != $objNewApDetail->getPropertyId()
					|| $objOldApDetail->getPropertyUnitId() != $objNewApDetail->getPropertyUnitId()
					|| $objOldApDetail->getGlAccountId() != $objNewApDetail->getGlAccountId()
					|| $objOldApDetail->getApCodeId() != $objNewApDetail->getApCodeId()
					|| $objOldApDetail->getQuantityOrdered() != $objNewApDetail->getQuantityOrdered()
					|| $objOldApDetail->getRate() != $objNewApDetail->getRate()
					|| $objOldApDetail->getTransactionAmount() != $objNewApDetail->getTransactionAmount()
					|| $objOldApDetail->getDescription() != $objNewApDetail->getDescription()
					|| $objOldApDetail->getDeletedBy() != $objNewApDetail->getDeletedBy()
					|| $objOldApDetail->getApPhysicalStatusTypeId() != $objNewApDetail->getApPhysicalStatusTypeId()
					|| $objOldApDetail->getGlDimensionId() != $objNewApDetail->getGlDimensionid() ) {

					$boolChangesMade = true;
					$this->m_boolIsModified = true;
				}
			} else {
				$boolChangesMade = true;
				$this->m_boolIsModified = true;
			}
		}

		return $boolChangesMade;
	}

	public function calculateScriptStartDate() {

		$boolTakeLastDayOfMonth		= false;
		$boolTakeNextDayFromMonth	= false;
		$strScriptStartDate			= NULL;
		$strPostDate				= $this->getStartDate();
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrmixDayNames				= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];
		$arrmixRelativeFormats		= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];

		if( false == is_numeric( $intFrequencyInterval ) || false == valStr( $strPostDate ) ) {
			return NULL;
		}

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
			case CFrequency::YEARLY:
				$strScriptStartDate = $strPostDate;
				break;

			case CFrequency::WEEKLY:
				if( true == is_numeric( $this->getDayOfWeek() ) ) {
					$objStartDatetime	= new DateTime( $strPostDate );
					$objStartDatetime->modify( $arrmixDayNames[$this->getDayOfWeek()] );
					$strScriptStartDate	= $objStartDatetime->format( 'm/d/Y' );

					// if script start date is passed then calculate next day date
					if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
						$objStartDatetime->modify( 'next ' . $arrmixDayNames[$this->getDayOfWeek()] );
						$strScriptStartDate = $objStartDatetime->format( 'm/d/Y' );
					}
				}
				break;

			case CFrequency::MONTHLY:
				if( false == valArr( $this->getDaysOfMonth() ) ) {
					return NULL;
				}

				$arrintDaysOfMonth = $this->getDaysOfMonth();

				$intNextPostDay	= $arrintDaysOfMonth[0];
				$intDayKey		= array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

				if( true == is_numeric( $intDayKey ) ) {

					if( true == array_key_exists( $intDayKey, $arrintDaysOfMonth ) ) {
						$intNextPostDay = $arrintDaysOfMonth[$intDayKey];
					}
					$strScriptStartDate 			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
					$strScriptStartDate 			= date( 'm/d/Y', strtotime( $strScriptStartDate ) );
					$boolTakeNextDayFromMonth		= true;
				} else {

					$arrstrPostDate = explode( '/', $strPostDate );

					$intDay = $arrstrPostDate[1];

					foreach( $arrintDaysOfMonth as $intDate ) {
						if( $intDate > $intDay ) {
							$intNextPostDay				= $intDate;
							$boolTakeNextDayFromMonth	= true;
							break;
						}
					}

					$strScriptStartDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

					if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
						$objNextPostMonthDatetime = new DateTime( $strScriptStartDate );
						$objNextPostMonthDatetime->modify( $intFrequencyInterval . ' month' );
						$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
					}
				}

				if( false == $boolTakeNextDayFromMonth && ( 0 == $intNextPostDay || ( false == $intDayKey && false != array_search( 0, $arrintDaysOfMonth ) ) ) ) {
					$boolTakeLastDayOfMonth = true;
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				if( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getDayOfWeek() ) ) {
					return NULL;
				}

				$strPostDate 		= date( $strPostDate );
				$strDayName 		= $arrmixDayNames[$this->getDayOfWeek()];
				$strRelativeNumber	= $arrmixRelativeFormats[$intFrequencyInterval];

				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of this month' );
				$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );

				if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
					$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
					$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				}
				break;

			default:
		}// switch

		$arrstrNextPostDate = explode( '/', $strScriptStartDate );
		if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
			$boolTakeLastDayOfMonth = true;
		}

		if( true == $boolTakeLastDayOfMonth ) {

			$objDateTimeCurrentPostDate = new DateTime( $strPostDate );
			$objDateTimeCurrentPostDate->modify( 'last day of this month' );
			$strScriptStartDate = $objDateTimeCurrentPostDate->format( 'm/d/Y' );
		}

		return date( 'm/d/Y', strtotime( $strScriptStartDate ) );
	}

	public function calculateEndDate( $objFrequency ) {

		if( false == is_numeric( $this->getNumberOfOccurrences() ) || 0 >= $this->getNumberOfOccurrences() ) {
			return NULL;
		}

		$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() ) );

		switch( $this->getFrequencyId() ) {
			case CFrequency::DAILY:
			case CFrequency::YEARLY:
			case CFrequency::WEEKLY:
				$intNumberOfIntervalUnits	= ( $this->getNumberOfOccurrences() - 1 ) * $this->getFrequencyInterval();
				$strIntervalLabel			= '';

				if( true == valObj( $objFrequency, 'CFrequency' ) ) {
					$strIntervalLabel = $objFrequency->getIntervalFrequencyLabelPlural();
				}

				if( 0 < $intNumberOfIntervalUnits ) {
					$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() . ' +' . $intNumberOfIntervalUnits . ' ' . $strIntervalLabel ) );
				}
				break;

			case CFrequency::MONTHLY:
			case CFrequency::WEEKDAY_OF_MONTH:
				if( ( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getDayOfWeek() ) ) || ( CFrequency::MONTHLY == $this->getFrequencyId() && false == valStr( $this->getDaysOfMonth() ) ) ) {
					return NULL;
				}

				$strScriptStartDate		= $this->getNextPostDate();
				$intNumberOfOccurrences	= $this->getNumberOfOccurrences();

				for( $intCounter = 2; $intCounter <= $intNumberOfOccurrences; $intCounter++ ) {

					$strEndDate = $this->calculateNextPostDate();
					$this->setNextPostDate( $strEndDate );
				}
				$this->setNextPostDate( $strScriptStartDate );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strEndDate ) );
	}

	public function calculateNextPostDate() {

		if( false == is_numeric( $this->getFrequencyInterval() ) ) {
			return NULL;
		}

		$intNextPostDay				= NULL;
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrintDaysOfMonth			= ( true == valArr( $this->getDaysOfMonth() ) ) ? $this->getDaysOfMonth() : [];
		$strPostDate				= $this->getNextPostDate();

		$arrmixRelativeFormats	= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];
		$arrmixDayNames			= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' Days' ) );
				break;

			case CFrequency::WEEKLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' Weeks' ) );
				break;

			case CFrequency::YEARLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . ( int ) $intFrequencyInterval . ' Years' ) );

				if( date( 'm', strtotime( $strPostDate ) ) != date( 'm', strtotime( $strNextPostDate ) ) ) {

					$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
					$objDateTimeNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
				}
				break;

			case CFrequency::MONTHLY:
				if( true == valArr( $arrintDaysOfMonth ) ) {

					$intDayKey = array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

					if( true == is_numeric( $intDayKey ) ) {

						if( true == array_key_exists( $intDayKey + 1, $arrintDaysOfMonth ) ) {

							$intNextPostDay = $arrintDaysOfMonth[$intDayKey + 1];

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}
							} else {

								$strNextPostDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

								$arrstrNextPostDate = explode( '/', $strNextPostDate );

								if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {

									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/01/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( 'last day of this month' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}
							}
						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

						}
					} else {

						if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintDaysOfMonth ) ) {

							$intNextPostDay = current( $arrintDaysOfMonth );

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

							} else {

								$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
								$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
							}

						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
						}
					}
				}

				$objPostDate = new DateTime( $strPostDate );
				$objPostDate->modify( 'first day of this month' );

				$objNextPostDate = new DateTime( $strNextPostDate );
				$objNextPostDate->modify( 'first day of this month' );

				$objInterval = date_diff( $objPostDate, $objNextPostDate );

				if( $objInterval->format( '%m' ) != $intFrequencyInterval && $objInterval->format( '%m' ) != 0 ) {

					$intNextPostDay		= current( $arrintDaysOfMonth );
					$objNextPostDate	= new DateTime( $strNextPostDate );
					$objNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate	= $objNextPostDate->format( 'm/d/Y' );

					if( 0 != $intNextPostDay ) {

						$strNextPostDateCopy = $strNextPostDate;

						$strNextPostDate	= date( 'm', strtotime( $strNextPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strNextPostDate ) );
						$arrstrNextPostDate	= explode( '/', $strNextPostDate );

						if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
							$strNextPostDate = $strNextPostDateCopy;
						}
					}
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				$strDayName = $arrmixDayNames[$this->getDayOfWeek()];
				$strRelativeNumber = $arrmixRelativeFormats[$intFrequencyInterval];
				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
				$strNextPostDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strNextPostDate ) );

	}

	public function validUrl( $strUrl ) {

		$boolIsValid = true;

		if( false == is_null( $strUrl ) && false == filter_var( $strUrl, FILTER_VALIDATE_URL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Invalid URL.' ) );
		}
		return $boolIsValid;
	}

	public function updateInvoicedPoStatus( $intApHeaderSubTypeId, $arrintPoApHeaderIds, $boolIsAddLog = false, $boolIsAllowMultipleInvoicesForStandardPoLineItems, $intCid, $intCompanyUserId, $objClientDatabase ) {
		$arrobjPoApHeaders = [];
		if( CApHeaderSubType::STANDARD_INVOICE == $intApHeaderSubTypeId ) {
			$arrobjPoApHeaders = ( array ) \Psi\Eos\Entrata\CApHeaders::createService()->fetchPoApHeadersByApHeaderSubTypeIdByApHeaderIdsByCid( $arrintPoApHeaderIds, $intCid, $objClientDatabase, $boolIsAllowMultipleInvoicesForStandardPoLineItems );
		} else if( CApHeaderSubType::CATALOG_INVOICE == $intApHeaderSubTypeId ) {
			$arrobjPoApHeaders = $this->resetCatalogPoStatus( $arrintPoApHeaderIds, $intCid, $objClientDatabase );
		}
		foreach( $arrobjPoApHeaders as $objApHeader ) {
			if( false == $objApHeader->update( $intCompanyUserId, $objClientDatabase ) ) {
				$objClientDatabase->rollback();
				return false;
			}

			if( true == $boolIsAddLog && CApHeaderSubType::CATALOG_INVOICE == $intApHeaderSubTypeId ) {
				// Update PO Log
				if( false == $objApHeader->logHistory( CApHeaderLog::ACTION_ITEMS_STATUS_UPDATED, $intCompanyUserId, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					return false;
				}
			}
		}
		return true;
	}

	public function resetCatalogPoStatus( $arrintPoApHeaderIds, $intCid, $objClientDatabase ) : array {

		$arrmixPoApHeaders = $arrmixPoApDetailStatuses = [];
		$arrintPoClosedStatusIds			 = [ CApPhysicalStatusType::FULFILLED, CApPhysicalStatusType::CANCELLED, CApPhysicalStatusType::RETURNED ];
		$arrintPoReceivedStatusIds			 = [ CApPhysicalStatusType::RECEIVED ];
		$arrintPoLineItemClosedStatusIds	 = [ CApPhysicalStatusType::RECEIVED, CApPhysicalStatusType::RETURNED, CApPhysicalStatusType::CANCELLED ];
		$arrintPoLineItemApprovedStatusIds	 = [ CApPhysicalStatusType::ORDERED, CApPhysicalStatusType::RECEIVED, CApPhysicalStatusType::PARTIALLY_RECEIVED ];

		$arrmixPoApDetails = ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchActivePoDetailStatusByApHeaderIdsByCid( $arrintPoApHeaderIds, $intCid, $objClientDatabase );
		$arrintPoApHeaderIds = array_keys( rekeyArray( 'ap_header_id', $arrmixPoApDetails ) );

		$arrobjPoApHeaders = rekeyObjects( 'Id', ( array ) \Psi\Eos\Entrata\CApHeaders::createService()->fetchApHeadersByIdsByCid( $arrintPoApHeaderIds, $intCid, $objClientDatabase ) );
		$arrobjPoApDetails = ( array ) \Psi\Eos\Entrata\CApDetails::createService()->fetchActiveApDetailsByApHeaderIdsByCid( $arrintPoApHeaderIds,  $intCid, $objClientDatabase );

		foreach( $arrobjPoApDetails as $objPoApDetail ) {
			$arrmixPoApDetailStatuses[$objPoApDetail->getApHeaderId()][] = $objPoApDetail->getApPhysicalStatusTypeId();
		}
		foreach( $arrmixPoApDetails as $arrmixPoApDetail ) {

			if( false == valArr( $arrmixPoApDetail ) ) continue;

			if( false == array_key_exists( $arrmixPoApDetail['ap_header_id'], $arrmixPoApHeaders ) ) {

				$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'] = $arrobjPoApHeaders[$arrmixPoApDetail['ap_header_id']]->getApFinancialStatusTypeId();

				if( true == in_array( $arrmixPoApDetail['order_status'], $arrintPoClosedStatusIds ) ) {
					$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'] = CApFinancialStatusType::CLOSED;
				} elseif( true == in_array( $arrmixPoApDetail['order_status'], $arrintPoReceivedStatusIds ) ) {
					$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'] = CApFinancialStatusType::APPROVED;
				} elseif( CApPhysicalStatusType::IN_PROGRESS == $arrmixPoApDetail['order_status'] ) {
					$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'] = CApFinancialStatusType::PARTIALLY_INVOICED;
				}

				$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_physical_status_type_id'] = ( CApPhysicalStatusType::FULFILLED == $arrmixPoApDetail['order_status'] ) ? CApPhysicalStatusType::RECEIVED : $arrmixPoApDetail['order_status'];
			} else {

				$intCurrPOHeaderPhysicalStatusId = ( CApPhysicalStatusType::FULFILLED == $arrmixPoApDetail['order_status'] ) ? CApPhysicalStatusType::RECEIVED : $arrmixPoApDetail['order_status'];
				$intPrevPOHeaderPhysicalStatusId = $arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_physical_status_type_id'];

				$intCurrPoHeaderFinancialStatusId = $arrmixPoApDetail['order_status'];
				$intPrevPoHeaderFinancialStatusId = $arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'];

				switch( $intPrevPoHeaderFinancialStatusId ) {
					case ( CApFinancialStatusType::APPROVED == $intPrevPoHeaderFinancialStatusId && CApPhysicalStatusType::ORDERED == $intCurrPoHeaderFinancialStatusId ):
					case ( CApFinancialStatusType::APPROVED == $intPrevPoHeaderFinancialStatusId && CApPhysicalStatusType::PARTIALLY_RECEIVED == $intCurrPoHeaderFinancialStatusId ):
					case ( CApFinancialStatusType::APPROVED == $intPrevPoHeaderFinancialStatusId && true == in_array( $intCurrPoHeaderFinancialStatusId, $arrintPoLineItemClosedStatusIds ) ):
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'] = CApFinancialStatusType::APPROVED;
						break;

					case ( CApFinancialStatusType::APPROVED == $intPrevPoHeaderFinancialStatusId && CApPhysicalStatusType::IN_PROGRESS == $intCurrPoHeaderFinancialStatusId ):
					case ( CApFinancialStatusType::APPROVED == $intPrevPoHeaderFinancialStatusId && CApPhysicalStatusType::FULFILLED == $intCurrPoHeaderFinancialStatusId ):
					case ( CApFinancialStatusType::PARTIALLY_INVOICED == $intPrevPoHeaderFinancialStatusId && true == in_array( $intCurrPoHeaderFinancialStatusId, $arrintPoLineItemApprovedStatusIds ) ):
					case ( CApFinancialStatusType::PARTIALLY_INVOICED == $intPrevPoHeaderFinancialStatusId && CApPhysicalStatusType::IN_PROGRESS == $intCurrPoHeaderFinancialStatusId ):
					case ( CApFinancialStatusType::PARTIALLY_INVOICED == $intPrevPoHeaderFinancialStatusId && true == in_array( $intCurrPoHeaderFinancialStatusId, $arrintPoClosedStatusIds ) ):
					case ( CApFinancialStatusType::CLOSED == $intPrevPoHeaderFinancialStatusId && true == in_array( $intCurrPoHeaderFinancialStatusId, $arrintPoLineItemApprovedStatusIds ) ):
					case ( CApFinancialStatusType::CLOSED == $intPrevPoHeaderFinancialStatusId && CApPhysicalStatusType::IN_PROGRESS == $intCurrPoHeaderFinancialStatusId ):
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'] = CApFinancialStatusType::PARTIALLY_INVOICED;
						break;

					case ( CApFinancialStatusType::CLOSED == $intPrevPoHeaderFinancialStatusId && true == in_array( $intCurrPoHeaderFinancialStatusId, $arrintPoClosedStatusIds ) ):
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'] = CApFinancialStatusType::CLOSED;
						break;

					default:
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_financial_status_type_id'] = $arrobjPoApHeaders[$arrmixPoApDetail['ap_header_id']]->getApFinancialStatusTypeId();
						break;
				}

				switch( $intCurrPOHeaderPhysicalStatusId ) {
					case $intCurrPOHeaderPhysicalStatusId == $intPrevPOHeaderPhysicalStatusId:
						break;

					case ( CApPhysicalStatusType::IN_PROGRESS == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::IN_PROGRESS == $intPrevPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::IN_PROGRESS == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::ORDERED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::ORDERED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::IN_PROGRESS == $intPrevPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::IN_PROGRESS == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::CANCELLED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::CANCELLED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::IN_PROGRESS == $intPrevPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::IN_PROGRESS == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::RETURNED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::RETURNED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::IN_PROGRESS == $intPrevPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::IN_PROGRESS == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::RECEIVED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::RECEIVED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::IN_PROGRESS == $intPrevPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::IN_PROGRESS == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::PARTIALLY_RECEIVED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::PARTIALLY_RECEIVED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::IN_PROGRESS == $intPrevPOHeaderPhysicalStatusId ):
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_physical_status_type_id'] = CApPhysicalStatusType::IN_PROGRESS;
						break;

					case ( CApPhysicalStatusType::ORDERED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::PARTIALLY_RECEIVED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::ORDERED == $intPrevPOHeaderPhysicalStatusId && CApPhysicalStatusType::PARTIALLY_RECEIVED == $intCurrPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::ORDERED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::RECEIVED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::ORDERED == $intPrevPOHeaderPhysicalStatusId && CApPhysicalStatusType::RECEIVED == $intCurrPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::ORDERED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::RETURNED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::ORDERED == $intPrevPOHeaderPhysicalStatusId && CApPhysicalStatusType::RETURNED == $intCurrPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::PARTIALLY_RECEIVED == $intCurrPOHeaderPhysicalStatusId && true == in_array( $intPrevPOHeaderPhysicalStatusId, $arrintPoLineItemClosedStatusIds ) ) || ( CApPhysicalStatusType::PARTIALLY_RECEIVED == $intPrevPOHeaderPhysicalStatusId && true == in_array( $intCurrPOHeaderPhysicalStatusId, $arrintPoLineItemClosedStatusIds ) ):
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_physical_status_type_id'] = CApPhysicalStatusType::PARTIALLY_RECEIVED;
						break;

					case ( CApPhysicalStatusType::RECEIVED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::RETURNED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::RECEIVED == $intPrevPOHeaderPhysicalStatusId && CApPhysicalStatusType::RETURNED == $intCurrPOHeaderPhysicalStatusId ):
					case ( CApPhysicalStatusType::RECEIVED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::CANCELLED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::RECEIVED == $intPrevPOHeaderPhysicalStatusId && CApPhysicalStatusType::CANCELLED == $intCurrPOHeaderPhysicalStatusId ):
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_physical_status_type_id'] = CApPhysicalStatusType::RECEIVED;
						break;

					case ( CApPhysicalStatusType::RETURNED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::CANCELLED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::RETURNED == $intPrevPOHeaderPhysicalStatusId && CApPhysicalStatusType::CANCELLED == $intCurrPOHeaderPhysicalStatusId ):
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_physical_status_type_id'] = CApPhysicalStatusType::RETURNED;
						break;

					case ( CApPhysicalStatusType::ORDERED == $intCurrPOHeaderPhysicalStatusId && CApPhysicalStatusType::CANCELLED == $intPrevPOHeaderPhysicalStatusId ) || ( CApPhysicalStatusType::ORDERED == $intPrevPOHeaderPhysicalStatusId && CApPhysicalStatusType::CANCELLED == $intCurrPOHeaderPhysicalStatusId ):
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_physical_status_type_id'] = CApPhysicalStatusType::ORDERED;
						break;

					default:
						$arrmixPoApHeaders[$arrmixPoApDetail['ap_header_id']]['ap_physical_status_type_id'] = $intCurrPOHeaderPhysicalStatusId;
						break;
				}
			}
		}
		foreach( $arrobjPoApHeaders as $intApHeaderId => $objPoApHeader ) {

			if( $arrmixPoApHeaders[$intApHeaderId]['ap_financial_status_type_id'] == $objPoApHeader->getApFinancialStatusTypeId()
			    && $arrmixPoApHeaders[$intApHeaderId]['ap_physical_status_type_id'] == $objPoApHeader->getApPhysicalStatusTypeId() ) {
				unset( $arrobjPoApHeaders[$intApHeaderId] );
			} else {
				$objPoApHeader->setApFinancialStatusTypeId( $arrmixPoApHeaders[$intApHeaderId]['ap_financial_status_type_id'] );
				if( CApPhysicalStatusType::IN_PROGRESS == $arrmixPoApHeaders[$intApHeaderId]['ap_physical_status_type_id'] && true == array_key_exists( $intApHeaderId, $arrmixPoApDetailStatuses ) && true == valArr( $arrmixPoApDetailStatuses[$intApHeaderId] ) ) {
					$intNewApPhysicalStatusTypeId = ( true == in_array( CApPhysicalStatusType::PARTIALLY_RECEIVED, $arrmixPoApDetailStatuses[$intApHeaderId] ) ) ? CApPhysicalStatusType::PARTIALLY_RECEIVED : CApPhysicalStatusType::RECEIVED;
					$objPoApHeader->setApPhysicalStatusTypeId( $intNewApPhysicalStatusTypeId );

				} elseif( CApPhysicalStatusType::IN_PROGRESS != $arrmixPoApHeaders[$intApHeaderId]['ap_physical_status_type_id'] ) {
					$objPoApHeader->setApPhysicalStatusTypeId( $arrmixPoApHeaders[$intApHeaderId]['ap_physical_status_type_id'] );
				}
			}
		}

		return $arrobjPoApHeaders;
	}

	public function validateIsCreateUtilityBill( $arrmixInvoiceDetails, $arrmixPsProductPropertyInfo, $objUtilitiesDatabase ) : bool {

		$arrintPsProductIds = [];

		$arrintIpProperties = getArrayElementByKey( 'property_ids', getArrayElementByKey( CPsProduct::INVOICE_PROCESSING, $arrmixPsProductPropertyInfo ) );

		if( true == valArr( $arrintIpProperties ) && true == valArr( array_intersect( $arrmixInvoiceDetails['property_ids'], $arrintIpProperties ) ) ) {
			$arrintPsProductIds[] = CPsProduct::INVOICE_PROCESSING;
		}

		$arrintIpProperties = getArrayElementByKey( 'property_ids', getArrayElementByKey( CPsProduct::UTILITY_EXPENSE_MANAGEMENT, $arrmixPsProductPropertyInfo ) );

		if( true == valArr( $arrintIpProperties ) && true == valArr( array_intersect( $arrmixInvoiceDetails['property_ids'], $arrintIpProperties ) ) ) {
			$arrintPsProductIds[] = CPsProduct::UTILITY_EXPENSE_MANAGEMENT;
		}

		$arrintIpProperties = getArrayElementByKey( 'property_ids', getArrayElementByKey( CPsProduct::RESIDENT_UTILITY, $arrmixPsProductPropertyInfo ) );

		if( true == valArr( $arrintIpProperties ) && true == valArr( array_intersect( $arrmixInvoiceDetails['property_ids'], $arrintIpProperties ) ) ) {
			$arrintPsProductIds[] = CPsProduct::RESIDENT_UTILITY;
		}

		if( false == valArr( $arrintPsProductIds ) || true == in_array( CPsProduct::INVOICE_PROCESSING, $arrintPsProductIds ) ) {
			return false;
		}

		$intPropertyId = current( $arrmixInvoiceDetails['property_ids'] );

		// If utility system already have the bill for ap header id then we are not pulling current invoice to utility side.
		$objExistingUtilityBill = \Psi\Eos\Utilities\CUtilityBills::createService()->fetchUtilityBillByCidByPropertyIdByApHeaderId( $this->getCid(), $intPropertyId, $this->getId(), $objUtilitiesDatabase );

		if( true == valObj( $objExistingUtilityBill, 'CUtilityBill' ) ) {
			return false;
		}

		if( true == in_array( CPsProduct::RESIDENT_UTILITY, $arrintPsProductIds ) && false == in_array( CPsProduct::UTILITY_EXPENSE_MANAGEMENT, $arrintPsProductIds ) ) {
			return true;
		}

		$arrintCommodityIds = \Psi\Eos\Utilities\CUtilityBillAccountCommodities::createService()->fetchUtilityBillAccountCommoditiesByPropertyIdByApPayeeId( $intPropertyId, $arrmixInvoiceDetails['ap_header']->getApPayeeId(), $objUtilitiesDatabase );

		if( false == valArr( $arrintCommodityIds ) ) {
			return false;
		}

		$objPropertyUtilitySetting = \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchPropertyUtilitySettingByPropertyId( $intPropertyId, $objUtilitiesDatabase );

		if( false == valObj( $objPropertyUtilitySetting, 'CPropertyUtilitySetting' ) || true == $objPropertyUtilitySetting->getIsUemImplementation() ) {
			return false;
		}

		$arrintCommodityIds = getArrayFieldValuesByFieldName( 'commodity_id', $arrintCommodityIds );

		$strUemGoLiveDate = getArrayElementByKey( 'uem_go_live_date', current( ( array ) \Psi\Eos\Utilities\CPropertyCommodities::createService()->fetchMinUemGoLiveDateByIdsByPropertyId( $arrintCommodityIds, $intPropertyId, $objUtilitiesDatabase ) ) );

		if( false == is_null( $strUemGoLiveDate ) && strtotime( $strUemGoLiveDate ) < strtotime( $this->getPostDate() ) ) {
			return true;
		}

		return false;

	}

}
?>