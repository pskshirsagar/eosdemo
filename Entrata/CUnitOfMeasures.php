<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitOfMeasures
 * Do not add any new functions to this class.
 */

class CUnitOfMeasures extends CBaseUnitOfMeasures {

	public static function fetchUnitOfMeasuresHavingSameUnitOfMeasureTypeIdByIdByCid( $intId, $intCid, $objClientDatabase, $boolIsAllowSameUnitOfMeasure = false ) {

		$strWhere = ' AND uom2.id <> ' . ( int ) $intId;

		if( true == $boolIsAllowSameUnitOfMeasure ) {
			$strWhere = '';
		}

		$strSql = 'SELECT
						uom2.id,
						uom2.name
					FROM
						unit_of_measures uom1
						JOIN unit_of_measures uom2 ON ( uom1.cid = uom2.cid AND uom1.unit_of_measure_type_id = uom2.unit_of_measure_type_id )
					WHERE
						uom1.cid = ' . ( int ) $intCid . '
						AND uom1.id = ' . ( int ) $intId . $strWhere . '
						AND uom2.deleted_by IS NULL
						AND uom2.is_published IS TRUE
					ORDER BY
						uom2.name';

		return self::fetchUnitOfMeasures( $strSql, $objClientDatabase );
	}

	public static function fetchUnitOfMeasuresCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'unit_of_measures', $objDatabase );
	}

	public static function fetchUnitOfMeasuresByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						name
					FROM
						unit_of_measures
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN( ' . sqlIntImplode( $arrintIds ) . ' )
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return parent::fetchUnitOfMeasures( $strSql, $objClientDatabase );
	}

	public static function fetchActiveUnitOfMeasuresByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						uom.*,
						uomt.name AS unit_of_measure_type
					FROM
						unit_of_measures uom
						JOIN unit_of_measure_types uomt ON ( uom.unit_of_measure_type_id = uomt.id )
					WHERE
						uom.cid = ' . ( int ) $intCid . '
						AND uom.deleted_by IS NULL
						AND uom.is_published IS TRUE
					ORDER BY
						uomt.name,
						uom.name';

		return self::fetchUnitOfMeasures( $strSql, $objClientDatabase );
	}

	public static function fetchUnitOfMeasureBySearchedStringByUnitOfMeasureIdByCid( $arrstrSearchedString, $intUnitOfMeasureId, $intCid, $objDatabase ) {

		$arrstrFilteredExplodedSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $arrstrSearchedString );

		$strSqlPart = '';
		$arrstrAdvancedSearchParameters = [];

		foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
			if( '' != $strKeywordAdvancedSearch ) {
				array_push( $arrstrAdvancedSearchParameters, 'uom.name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
			}
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSqlPart .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		$strSql = 'SELECT
							uom.id,
							uom.name,
							uom.unit_of_measure_type_id
						FROM
							unit_of_measures uom
						WHERE uom.id IN (
											SELECT
												CASE WHEN ac.unit_of_measure_id = uomc.to_unit_of_measure_id THEN
													uomc.from_unit_of_measure_id
												WHEN ac.unit_of_measure_id = uomc.from_unit_of_measure_id THEN
													uomc.to_unit_of_measure_id
												END  AS unit_of_measure_id
											FROM
												unit_of_measure_conversions uomc
												JOIN ap_codes ac ON ( ( uomc.to_unit_of_measure_id = ac.unit_of_measure_id OR uomc.from_unit_of_measure_id = ac.unit_of_measure_id ) AND uomc.cid = ac.cid )
											WHERE
												ac.cid = ' . ( int ) $intCid . '
												AND ac.unit_of_measure_id = ' . ( int ) $intUnitOfMeasureId . '

											UNION

											SELECT
												ac.unit_of_measure_id
											FROM
												ap_codes ac
											WHERE
												ac.cid = ' . ( int ) $intCid . '
												AND ac.unit_of_measure_id = ' . ( int ) $intUnitOfMeasureId . '
										)
						 	AND uom.cid = ' . ( int ) $intCid . '
						 	' . $strSqlPart . '
						 ORDER BY
							uom.id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnitOfMeasuresByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						unit_of_measures
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_published IS TRUE
					ORDER BY
						name';

		return self::fetchUnitOfMeasures( $strSql, $objDatabase );
	}

	public static function fetchActivePurchaseUnitOfMeasuresByIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						uom2.id,
						uom2.name
					FROM
						unit_of_measures uom1
						JOIN unit_of_measures uom2 ON ( uom1.cid = uom2.cid AND uom1.unit_of_measure_type_id = uom2.unit_of_measure_type_id )
					WHERE
						uom1.cid = ' . ( int ) $intCid . '
						AND uom1.id = ' . ( int ) $intId . '
						AND uom2.deleted_by IS NULL
						AND uom2.is_published IS TRUE
					ORDER BY
						uom2.name';

		return self::fetchUnitOfMeasures( $strSql, $objClientDatabase );
	}

}
?>