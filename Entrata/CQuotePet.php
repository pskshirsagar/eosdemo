<?php

class CQuotePet extends CBaseQuotePet {

	protected $m_strPetTypeName;

    public function getPetTypeName() {
    	return $this->m_strPetTypeName;
    }

    public function setPetTypeName( $strPetTypeName ) {
    	$this->m_strPetTypeName = $strPetTypeName;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['pet_type_name'] ) ) $this->setPetTypeName( $arrmixValues['pet_type_name'] );

    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valQuoteId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPetTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBreed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>