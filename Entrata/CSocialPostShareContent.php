<?php

class CSocialPostShareContent extends CBaseSocialPostShareContent {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSocialPostId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSocialPostTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */
	public function createSocialPost() {
		$objSocialPost = new CSocialPost();
		$objSocialPost->setCid( $this->getCid() );
		$objSocialPost->setPropertyId( $this->getPropertyId() );
		$objSocialPost->setSocialPostTypeId( $this->getSocialPostTypeId() );

		return $objSocialPost;
	}

}
?>