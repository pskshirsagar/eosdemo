<?php

class CRevenueArCode extends CBaseRevenueArCode {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRevenueArCodeTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRevenueScheduledArTriggerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArCodeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>