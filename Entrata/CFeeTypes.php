<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeTypes
 * Do not add any new functions to this class.
 */

class CFeeTypes extends CBaseFeeTypes {

	public static function fetchFeeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CFeeType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFeeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CFeeType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>