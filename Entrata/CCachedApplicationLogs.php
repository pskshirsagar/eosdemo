<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedApplicationLogs
 * Do not add any new functions to this class.
 */

class CCachedApplicationLogs extends CBaseCachedApplicationLogs {

	// Fetching Executed rent for last 30 days and for the same days for the last year

	public static function fetchDailyExecutedRentByCidByPropertyId( $intCid, $intPropertyId, $intUnitTypeId, $boolPropertyLevel, $objDatabase ) {
		$strSqlCondition = '';
		if( false == $boolPropertyLevel ) {
			$strSqlCondition = ' AND ca.unit_type_id = ' . ( int ) $intUnitTypeId;
		}

		$strSqlDailyExecutedRents = '
				DROP TABLE
					IF EXISTS table_rate_date;

				CREATE Temp TABLE table_rate_date AS(
					SELECT ( generate_series( CURRENT_DATE - INTERVAL \'13 days\', CURRENT_DATE,
					\'1 day\'::interval ) )::date AS required_date
				UNION
					SELECT ( generate_series( CURRENT_DATE - INTERVAL \'1 Year 13 days\', CURRENT_DATE
					- INTERVAL \'1 Year\', \'1 day\'::interval ) )::date AS required_date );

				SELECT trd.required_date,
				       AVG( scl_data.executed_amount ) AS executed_rent
				FROM table_rate_date trd
				     LEFT JOIN LATERAL
				     (
				       SELECT ca.cid,
				              ca.property_id,
				              ca.unit_type_id,
				              ca.id,
				              SUM( scl.charge_amount ) AS executed_amount
				       FROM cached_Applications ca
				            LEFT JOIN scheduled_charge_logs scl ON ( 
				                ca.cid = scl.cid
				                AND ca.property_id = scl.property_id 
				                AND ca.lease_interval_id = scl.lease_interval_id 
				                AND scl.is_post_date_ignored = FALSE 
				                AND scl.ar_origin_id = ' . CArOrigin::BASE . ' 
				                AND scl.ar_trigger_id = ' . CArTrigger::MONTHLY . ' 
				                AND scl.is_unselected_quote is FALSE 
                                AND scl.is_deleted is false 
				                )
				            JOIN unit_types ut ON ut.cid = ca.cid AND ut.property_id = ca.property_id AND ut.is_occupiable = 1
				            JOIN unit_spaces us ON us.id = ca.unit_space_id AND ut.id = us.unit_type_id AND  us.unit_space_status_type_id IN ( ' . implode( ', ', [ CUnitSpaceStatusType::VACANT_UNRENTED_READY, CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY, CUnitSpaceStatusType::NOTICE_UNRENTED ] ) . ' )
				       WHERE 
				             ca.lease_approved_on::DATE = trd.required_date 
				             AND ca.lease_approved_on::DATE BETWEEN scl.reporting_post_date AND scl.apply_through_post_date 
				             AND ca.cid = ' . ( int ) $intCid . '
				             AND ca.property_id = ' . ( int ) $intPropertyId . $strSqlCondition . ' 
				             AND ca.term_month = 12
				       GROUP BY ca.cid,
				                ca.property_id,
				                ca.unit_type_id,
				                ca.id
				     ) AS scl_data ON ( TRUE )
				GROUP BY trd.required_date';

		return fetchData( $strSqlDailyExecutedRents, $objDatabase );
	}

	public static function fetchMonthlyExecutedRentByCidByPropertyId( $intCid, $intPropertyId, $intUnitTypeId, $boolPropertyLevel, $objDatabase ) {
		$strSqlCondition = '';

		if( false == $boolPropertyLevel ) {
			$strSqlCondition = ' AND ca.unit_type_id = ' . ( int ) $intUnitTypeId;
		}
		$strSqlMonthlyExecutedRents = '
			DROP TABLE if EXISTS records;
			CREATE TEMP TABLE records AS(
			SELECT 
				ca.cid,
				ca.id,
				ca.property_id,
				ca.unit_type_id,
				ca.unit_space_id,
				ca.lease_approved_on AS lease_approved_on,
				SUM( scl.charge_amount ) AS executed_rent,
				date_part( \'MONTH\' , ca.lease_approved_on ) AS month_num,
				date_part( \'YEAR\' , ca.lease_approved_on ) AS year_num,
				row_number() OVER( 
				                PARTITION BY 
				                        ca.unit_type_id, ca.unit_space_id, 
				                        date_part( \'MONTH\' , ca.lease_approved_on ), 
				                        date_part( \'YEAR\' , ca.lease_approved_on ) 
				                ORDER BY 
				                        date_part( \'DAY\', ca.lease_approved_on ) DESC, date_part( \'MONTH\' , ca.lease_approved_on ) 
				                 ) AS row_num
			FROM cached_applications ca
				LEFT JOIN scheduled_charge_logs scl ON (
					ca.cid = scl.cid 
					AND ca.property_id = scl.property_id 
					AND ca.lease_interval_id = scl.lease_interval_id 
					AND scl.is_post_date_ignored = FALSE 
					AND scl.ar_origin_id = ' . CArOrigin::BASE . ' 
					AND scl.ar_trigger_id = ' . CArTrigger::MONTHLY . ' )
				WHERE 
					ca.cid = ' . ( int ) $intCid . ' 
					AND ca.property_id = ' . ( int ) $intPropertyId . $strSqlCondition . ' 
					AND ca.lease_approved_on::DATE BETWEEN scl.reporting_post_date 
					AND scl.apply_through_post_date 
					AND ca.term_month = 12 
					AND scl.is_unselected_quote = FALSE 
					AND
					ca.lease_approved_on >
					(
						SELECT date_trunc( \'month\', CURRENT_DATE ) - INTERVAL \'1 year\'
					)
			GROUP BY 
				ca.cid,
				ca.id,
				ca.property_id,
				ca.unit_type_id,
				ca.unit_space_id,
				ca.lease_approved_on );

			DROP TABLE if EXISTS temp_dates;
			CREATE TEMP TABLE temp_dates AS
			SELECT generate_series( date_trunc( \'Month\', CURRENT_DATE - INTERVAL \'12 Months\'
			), CURRENT_DATE, \'1 Month\'::INTERVAL ) AS required_date;

			SELECT 
				date_part( \'MONTH\', td.required_date ) AS month_num,
				date_part( \'YEAR\', td.required_date ) AS year_num,
				AVG(r.executed_rent) AS executed_rent
			FROM temp_dates td
				LEFT JOIN records r ON ( r.month_num = date_part( \'MONTH\',td.required_date ) 
					AND r.year_num = date_part( \'YEAR\',td.required_date ) )
				LEFT JOIN unit_spaces us ON ( us.cid = r.cid 
					AND r.property_id = us.cid 
					AND r.unit_space_id = us.id 
					AND us.unit_space_status_type_id IN ( ' . implode( ', ', [ CUnitSpaceStatusType::VACANT_UNRENTED_READY, CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY, CUnitSpaceStatusType::NOTICE_UNRENTED ] ) . ' ) )
			WHERE r.row_num = 1
			GROUP BY 
				r.property_id,
				month_num,
				year_num,
				td.required_date;';

		return fetchData( $strSqlMonthlyExecutedRents, $objDatabase );
	}

	public static function fetchLeaseSignedOnByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) ) {
			return NULL;
		}
		$strSql = ' SELECT
					    min ( lease_completed_on ) as lease_completed_on,
					    application_id
					FROM
					    cached_application_logs
					WHERE
					    application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
					    AND cid = ' . ( int ) $intCid .
					' AND lease_completed_on IS NOT NULL
					GROUP BY application_id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchYtdYoyExecutedRentByCidByPropertyId( $intCid, $intPropertyId, $intUnitTypeId = NULL, $intFiscalYearEndMonth, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) return [];

		$strSqlCondition = '';
		$intCurrentYearMonth = date( 'm' );
		$strFiscalYearDate   = date( 'Y-' . $intFiscalYearEndMonth . '-01' );

		if( $intFiscalYearEndMonth >= $intCurrentYearMonth ) {
			$strFiscalYearStartDate = date( 'Y-m-d', strtotime( '-1 year +1 month', strtotime( $strFiscalYearDate ) ) );
			$strFiscalYearCondition = 'cusp.post_month BETWEEN \'' . $strFiscalYearStartDate . '\'::DATE-INTERVAL \' 1 Year\' AND CURRENT_DATE - INTERVAL \'1 Year\'';
		} else {
			$strFiscalYearStartDate = date( 'Y-m-d', strtotime( '+1 month', strtotime( $strFiscalYearDate ) ) );
			$strFiscalYearCondition = 'cusp.post_month BETWEEN \'' . $strFiscalYearStartDate . '\'::DATE-INTERVAL \' 1 Year\' AND CURRENT_DATE - INTERVAL \'1 Year\'';

		}

		if( false == is_null( $intUnitTypeId ) ) {
			$strSqlCondition = ' AND cusp.unit_type_id = ' . ( int ) $intUnitTypeId;
		}
		$strSql = 'WITH Year_To_Date AS
						(
							SELECT 
								cusp.cid,
								cusp.property_id,
								SUM( cusp.rent_base + cusp.rent_amenities + cusp.rent_specials ) as ytd_executed_rent
							FROM 
								cached_unit_space_periods cusp
							WHERE cusp.cid = ' . ( int ) $intCid . '
								AND cusp.property_id = ' . ( int ) $intPropertyId . $strSqlCondition . '
								AND cusp.post_month BETWEEN \'' . $strFiscalYearStartDate . '\'::DATE AND CURRENT_DATE
							GROUP BY 
								cusp.cid,
								cusp.property_id
						),
						Year_Over_Year AS (
							SELECT 
								cusp.cid,
								cusp.property_id,
								SUM( cusp.rent_base + cusp.rent_amenities + cusp.rent_specials ) as yoy_executed_rent
							FROM 
								cached_unit_space_periods cusp
							WHERE 
								cusp.cid = ' . ( int ) $intCid . '
								AND cusp.property_id = ' . ( int ) $intPropertyId . $strSqlCondition . '
								AND ' . $strFiscalYearCondition . '
							GROUP BY
								cusp.cid,
								cusp.property_id
						)
					SELECT
						ytd.cid,
						ytd.property_id,
						ytd.ytd_executed_rent,
						yoy.yoy_executed_rent,
						CASE
							WHEN yoy.yoy_executed_rent = 0 THEN NULL
							ELSE ROUND( ((ytd.ytd_executed_rent - yoy.yoy_executed_rent) / ytd.ytd_executed_rent) * 100 ) 
						END AS yoy_ytd_revenue_growth
					FROM Year_To_Date AS ytd
						JOIN Year_Over_Year AS yoy ON yoy.cid = ytd.cid AND yoy.property_id = ytd.property_id
					WHERE
						ytd.ytd_executed_rent > 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenueGrowthStatisticsRentData( $intCid, $intPropertyId, $intUnitTypeId, $intCurrentDate, $intLastYearCurrentDate, $objDatabase ) {

		$strCondition         = '';
		$strSecCondition      = '';
		$intStandardLeaseTerm = 12;

		if( false == empty( $intPropertyId ) ) {
			$strCondition = 'ca.property_id = ' . ( int ) $intPropertyId . ' AND ca.cid = ' . ( int ) $intCid;
			$strSecCondition = 'usl.property_id = ' . ( int ) $intPropertyId . ' AND usl.cid = ' . ( int ) $intCid;

			if( false == empty( $intUnitTypeId ) ) {
				$strCondition = $strCondition . ' AND ca.unit_type_id = ' . ( int ) $intUnitTypeId;
				$strSecCondition = $strSecCondition . ' AND usl.unit_type_id = ' . ( int ) $intUnitTypeId;
			}
		}

		$strSql = 'with all_dates as (SELECT generate_series(
			        date_trunc( \'month\', \'' . $intCurrentDate . '\'::DATE - interval \'12 months\'), 
			        \'' . $intCurrentDate . '\'::DATE, \'1 month\' )::date as date_series, ' . ( int ) $intCid . ' as cid) 
					SELECT
						all_dates.cid,
						L.property_id,
						L.executed_rent,
						L.budgeted_rent,
						L.previous_rent,
						L.month,
						L.year,
						all_dates.date_series
					FROM 
						all_dates 
					LEFT JOIN (SELECT
					u.cid,
					u.property_id,
					u.executed_rent,
					u.budgeted_rent,
					v.executed_rent AS previous_rent,
					u.month,
					u.year,
					date_trunc(\'month\', CONCAT(u.year,\'-\',u.month,\'-15\')::DATE)::date as DateMonth
				   FROM
				   (SELECT 
						s.cid,
						s.property_id,
						s.executed_rent,
						t.budgeted_rent,
						s.month,
						s.year 
					FROM
					(SELECT
					    ca.cid,
					    ca.property_id,
					    round( SUM( COALESCE( ( ca.rent_base + ca.rent_amenities + ca.rent_specials ), 0 ) )::NUMERIC ) AS executed_rent,
					    extract ( month FROM ca.post_month ) AS month,
					    extract ( YEAR FROM ca.post_month ) AS year
					FROM
					    cached_unit_space_periods ca
					    JOIN unit_types ut ON ut.cid = ca.cid AND ut.property_id = ca.property_id AND ut.id = ca.unit_type_id AND ut.is_occupiable =  ' . CUnitType::IS_OCCUPIABLE_YES . '
					    WHERE
					    ' . $strCondition . '
					    AND ca.post_month::DATE >= \'' . $intCurrentDate . '\'::DATE - INTERVAL \'13 months\'
					    AND ca.post_month::DATE <= \'' . $intCurrentDate . '\'::DATE
					GROUP BY
					    ca.cid,
					    ca.property_id,
					    month,
					    year)s,
					(SELECT
					    round( SUM( COALESCE( round( usl.budgeted_rent ), 0 ) )::NUMERIC ) AS budgeted_rent,
					    extract ( month FROM usl.post_date ) AS month,
					    extract ( YEAR FROM usl.post_date ) AS year
					FROM
					    unit_space_logs usl
					    JOIN unit_types ut ON ut.cid = usl.cid AND ut.property_id = usl.property_id AND ut.id = usl.unit_type_id AND ut.is_occupiable =  ' . CUnitType::IS_OCCUPIABLE_YES . '
					WHERE
					    ' . $strSecCondition . '
					    AND usl.deleted_by IS NULL
					    AND usl.is_post_month_ignored = 0
					    AND usl.is_marketed = 1
					    AND usl.post_date >= \'' . $intCurrentDate . '\'::DATE - INTERVAL \'13 months\' 
					    AND usl.post_date <= \'' . $intCurrentDate . '\'::DATE
					GROUP BY
					    usl.cid,
					    usl.property_id,
					    month,
					    year)t
				WHERE
				s.month = t.month AND
				s.year = t.year
				ORDER BY s.year, s.month)u
				LEFT JOIN (SELECT 
						s.cid,
						s.property_id,
						s.executed_rent,
						s.month,
						s.year 
					FROM
						(SELECT
						    ca.cid,
						    ca.property_id,
						    round( SUM( COALESCE( round( ca.rent_base + ca.rent_amenities + ca.rent_specials ), 0) )::NUMERIC ) AS executed_rent,
						    extract ( month FROM ca.post_month ) AS month,
						    extract ( YEAR FROM ca.post_month ) AS year
						FROM
						    cached_unit_space_periods ca
						    JOIN unit_types ut ON ut.cid = ca.cid AND ut.property_id = ca.property_id AND ut.id = ca.unit_type_id AND ut.is_occupiable =  ' . CUnitType::IS_OCCUPIABLE_YES . '
						WHERE
						    ' . $strCondition . '
						    AND ca.post_month::DATE >= \'' . $intLastYearCurrentDate . '\'::DATE - interval \'13 months\'
						    AND ca.post_month::DATE <= \'' . $intLastYearCurrentDate . '\'::DATE
						GROUP BY
						    ca.cid,
						    ca.property_id,
						    month,
						    year)s
					ORDER BY s.year, s.month)v
					ON
					u.month = v.month
					AND u.year -1 = v.year)L  ON (all_dates.date_series = L.DateMonth)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchFiscalYearRevenueGrowthStatisticsRentData( $intCid, $intPropertyId, $intUnitTypeId, $strStartDate, $strEndDate, $objDatabase ) {

		$strCondition = '';
		$strSecCondition      = '';

		if( false == empty( $intPropertyId ) ) {
			$strCondition = 'ca.property_id = ' . ( int ) $intPropertyId . ' AND ca.cid = ' . ( int ) $intCid;
			$strSecCondition = 'usl.property_id = ' . ( int ) $intPropertyId . ' AND usl.cid = ' . ( int ) $intCid;

			if( false == empty( $intUnitTypeId ) ) {
				$strCondition = $strCondition . ' AND ca.unit_type_id = ' . ( int ) $intUnitTypeId;
				$strSecCondition = $strSecCondition . ' AND usl.unit_type_id = ' . ( int ) $intUnitTypeId;
			}
		}

		$strSql = "with all_dates as (SELECT generate_series(
			        date_trunc( 'month', '" . $strStartDate . "'::DATE), 
			        '" . $strEndDate . "'::DATE, '1 month' )::date as date_series, " . ( int ) $intCid . " as cid) 
					SELECT
						all_dates.cid,
						L.property_id,
						L.executed_rent,
						L.budgeted_rent,
						L.previous_rent,
						L.month,
						L.year,
						all_dates.date_series
					FROM 
						all_dates 
					LEFT JOIN (SELECT
						u.cid,
						u.property_id,
						u.executed_rent,
						u.budgeted_rent,
						v.executed_rent AS previous_rent,
						u.month,
						u.year,
						date_trunc('month', CONCAT(u.year,'-',u.month,'-15')::DATE)::date as DateMonth
					   FROM
						(SELECT 
							s.cid,
							s.property_id,
							s.executed_rent,
							t.budgeted_rent,
							s.month,
							s.year 
						FROM
							(SELECT
								ca.cid,
								ca.property_id,
								round( SUM( COALESCE( ( ca.rent_base + ca.rent_amenities + ca.rent_specials ) , 0 ) )::NUMERIC ) AS executed_rent,
								extract ( month FROM ca.post_month ) AS month,
								extract ( YEAR FROM ca.post_month ) AS year
							FROM
								cached_unit_space_periods ca
								JOIN unit_types ut ON ut.cid = ca.cid AND ut.property_id = ca.property_id AND ut.id = ca.unit_type_id AND ut.is_occupiable = 1
							WHERE
								" . $strCondition . "
								AND ca.post_month::DATE >= '" . $strStartDate . "'::DATE
								AND ca.post_month::DATE <= '" . $strEndDate . "'::DATE
							GROUP BY
								ca.cid,
								ca.property_id,
								month,
								year)s,
							(SELECT
								round( SUM ( COALESCE ( ( usl.budgeted_rent ), 0 ) )::NUMERIC ) AS budgeted_rent,
								extract ( month FROM usl.post_date ) AS month,
								extract ( YEAR FROM usl.post_date ) AS year
							FROM
								unit_space_logs usl
								JOIN unit_types ut ON ut.cid = usl.cid AND ut.property_id = usl.property_id AND ut.id = usl.unit_type_id AND ut.is_occupiable = 1
							WHERE
								" . $strSecCondition . "
								AND usl.deleted_by IS NULL
								AND usl.is_post_month_ignored = 0
								AND usl.is_marketed = 1
								AND usl.post_date >= '" . $strStartDate . "'::DATE
								AND usl.post_date <= '" . $strEndDate . "'::DATE
							GROUP BY
								usl.cid,
								usl.property_id,
								month,
								year)t
						WHERE
							s.month = t.month
							AND s.year = t.year
						ORDER BY s.year, s.month)u
						LEFT JOIN (SELECT 
							s.cid,
							s.property_id,
							s.executed_rent,
							s.month,
							s.year 
						FROM
							(SELECT
							    ca.cid,
							    ca.property_id,
							    round( SUM( COALESCE( ( ca.rent_base + ca.rent_amenities + ca.rent_specials ), 0 ) )::NUMERIC ) AS executed_rent,
							    extract ( month FROM ca.post_month ) AS month,
							    extract ( YEAR FROM ca.post_month ) AS year
							FROM
							    cached_unit_space_periods ca
							    JOIN unit_types ut ON ut.cid = ca.cid AND ut.property_id = ca.property_id AND ut.id = ca.unit_type_id AND ut.is_occupiable = 1
							WHERE
							    " . $strCondition . "
							    AND ca.post_month::DATE BETWEEN '" . $strStartDate . "'::DATE - interval '1 year'
								AND '" . $strEndDate . "'::DATE - interval '1 year'
							GROUP BY
							    ca.cid,
							    ca.property_id,
							    month,
							    year)s
						ORDER BY s.year, s.month)v
						ON
						u.month = v.month
						AND u.year -1 = v.year)L  ON (all_dates.date_series = L.DateMonth)";
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchBudgetMonthlyRevenueDataByMonthByPropertyByUnitTypeIdByCid( $intCid, $intPropertyId, $intUnitTypeId, $strDate, $objDatabase ) {

		if( false == empty( $intPropertyId ) ) {
			$strCondition = 'usl.property_id = ' . ( int ) $intPropertyId . ' AND usl.cid = ' . ( int ) $intCid;

			if( false == empty( $intUnitTypeId ) ) {
				$strCondition = $strCondition . ' AND usl.unit_type_id = ' . ( int ) $intUnitTypeId;
			}
		}

		$strSql = 'SELECT
						ut.lookup_code,
						ut.name,
					    usl.unit_number_cache,
					    round ( usl.budgeted_rent ) AS executed_rent,
					    extract ( month FROM usl.post_date ) AS month,
					    extract ( YEAR FROM usl.post_date ) AS YEAR,
					    usl.post_date AS post_date
				   FROM
					    unit_space_logs usl
					    JOIN unit_types ut ON ( ut.id = usl.unit_type_id AND ut.is_occupiable =  ' . CUnitType::IS_OCCUPIABLE_YES . ' )
				   WHERE
				        ' . $strCondition . '
					    AND usl.deleted_by IS NULL
					    AND usl.is_post_month_ignored = 0
					    AND usl.is_marketed = 1
					    AND EXTRACT( month FROM usl.post_date ) = EXTRACT( month FROM \'' . $strDate . '\'::DATE )
					    AND EXTRACT( YEAR FROM usl.post_date ) = EXTRACT( YEAR FROM \'' . $strDate . '\'::DATE )
				   ORDER BY usl.post_date';

		$arrmixMonthData = fetchData( $strSql, $objDatabase );

		return isset( $arrmixMonthData ) ? $arrmixMonthData : NULL;
	}

	public static function fetchMonthlyRevenueDataByMonthByPropertyByUnitTypeIdByCid( $intCid, $intPropertyId, $intUnitTypeId, $strDate, $objDatabase ) {

		if( false == empty( $intPropertyId ) ) {
			$strCondition = 'ca.property_id = ' . ( int ) $intPropertyId . ' AND ca.cid = ' . ( int ) $intCid;

			if( false == empty( $intUnitTypeId ) ) {
				$strCondition = $strCondition . ' AND ca.unit_type_id = ' . ( int ) $intUnitTypeId;
			}
		}

		$strSql = 'SELECT
					    ut.lookup_code,
					    ut.name,
					    ca.unit_number_cache,
					    round ( ca.rent_base + ca.rent_amenities + ca.rent_specials ) AS executed_rent,
					    extract ( month FROM ca.post_month ) AS month,
					    extract ( YEAR FROM ca.post_month ) AS YEAR,
					    ca.post_month AS post_date
					FROM
					    cached_unit_space_periods ca
					    JOIN unit_types ut ON ut.cid = ca.cid AND ut.property_id = ca.property_id AND ut.id = ca.unit_type_id AND ut.is_occupiable =  ' . CUnitType::IS_OCCUPIABLE_YES . '
					WHERE
						' . $strCondition . '
					    AND EXTRACT( month FROM ca.post_month ) = EXTRACT( month FROM \'' . $strDate . '\'::DATE )
					    AND EXTRACT( YEAR FROM ca.post_month ) = EXTRACT( YEAR FROM \'' . $strDate . '\'::DATE )
					ORDER BY ca.post_month';

		$arrmixMonthData = fetchData( $strSql, $objDatabase );
		return isset( $arrmixMonthData ) ? $arrmixMonthData : NULL;
	}

}
?>