<?php

class CDefaultApPayeeTerm extends CBaseDefaultApPayeeTerm {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPercentage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDiscountPeriodDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFromInvoiceDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>