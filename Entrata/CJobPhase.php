<?php

class CJobPhase extends CBaseJobPhase {
	protected $m_strJobPhaseName;
	protected $m_strStatus;

	public function getJobPhaseName() {
		return $this->m_strJobPhaseName;
	}

	public function setJobPhaseName( $strJobPhaseName ) {
		$this->m_strJobPhaseName = $strJobPhaseName;
	}

    public function getStatus() {
        return $this->m_strStatus;
    }

    public function setStatus( $strStatus ) {
        $this->m_strStatus = $strStatus;
    }

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReClassGlHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Phase name is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFiscalYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalStartDate( $objJob ) {
		$boolIsValid = true;

		if( true == is_null( $this->getOriginalStartDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Phase expected start date is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_start_date', $strErrorMessage ) );
		} elseif( false == CValidation::validateDate( $this->getOriginalStartDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Expected start date should be in mm/dd/yyyy format.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_start_date', $strErrorMessage ) );
		}

		if( false == is_null( $this->getOriginalStartDate() ) && ( false == is_null( $objJob->getOriginalStartDate() ) && ( strtotime( $this->getOriginalStartDate() ) < strtotime( $objJob->getOriginalStartDate() ) ) ) || ( false == is_null( $objJob->getOriginalEndDate() ) && ( strtotime( $this->getOriginalStartDate() ) > strtotime( $objJob->getOriginalEndDate() ) ) ) ) {
			$boolIsValid     = false;
			$strErrorMessage = ( false == is_null( $this->getName() ) ) ? '\'' . $this->getName() . '\' phase ' : 'Phase ';
			$strErrorMessage .= __( "expected start date should be between Job's planned start date and completion date." );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_start_date', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valOriginalEndDate( $objJob ) {
		$boolIsValid = true;

		if( true == is_null( $this->getOriginalEndDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Phase expected completion date is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', $strErrorMessage ) );
		} elseif( false == CValidation::validateDate( $this->getOriginalEndDate() ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Expected completion date should be in mm/dd/yyyy format.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', $strErrorMessage ) );
		}

		if( false == is_null( $this->getOriginalStartDate() ) && false == is_null( $this->getOriginalEndDate() ) && ( strtotime( $this->getOriginalStartDate() ) > strtotime( $this->getOriginalEndDate() ) ) ) {
			$boolIsValid = false;
			$strErrorMessage = __( 'Phase expected completion date cannot be earlier than expected start date.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', $strErrorMessage ) );
		} elseif( false == is_null( $this->getOriginalEndDate() ) && ( false == is_null( $objJob->getOriginalStartDate() ) && ( strtotime( $this->getOriginalEndDate() ) < strtotime( $objJob->getOriginalStartDate() ) ) ) || ( false == is_null( $objJob->getOriginalEndDate() ) && ( strtotime( $this->getOriginalEndDate() ) > strtotime( $objJob->getOriginalEndDate() ) ) ) ) {
			$boolIsValid     = false;
			$strErrorMessage = ( false == is_null( $this->getName() ) ) ? '\'' . $this->getName() . '\' phase ' : 'Phase ';
			$strErrorMessage .= __( "expected completion date should be between Job's planned start date and completion date." );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valActualStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActualEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsJobPhaseExists( $objJob, $objDatabase ) {
		$boolIsValid = true;

		$strWhere = 'WHERE
						cid = ' . ( int ) $objJob->getCid() . '
						AND job_id = ' . ( int ) $objJob->getId() . '
						AND name = \'' . addslashes( $this->getName() ) . '\'
						AND id <>' . ( int ) $this->getId();

		if( 0 < CJobPhases::fetchJobPhaseCount( $strWhere, $objDatabase ) ) {
			$strErrorMessage = __( 'Phase name already exists.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_phase_exists', $strErrorMessage ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objJob, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valOriginalStartDate( $objJob );
				$boolIsValid &= $this->valOriginalEndDate( $objJob );
				$boolIsValid &= $this->valIsJobPhaseExists( $objJob, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( $boolDirectSet && isset( $arrmixValues['job_phase_name'] ) ) {
			$this->set( 'm_strJobPhaseName', trim( stripcslashes( $arrmixValues['job_phase_name'] ) ) );
		} elseif( isset( $arrmixValues['job_phase_name'] ) ) {
			$this->setJobPhaseName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['job_phase_name'] ) : $arrmixValues['job_phase_name'] );
		}

        if( $boolDirectSet && isset( $arrmixValues['status'] ) ) {
            $this->set( 'm_strStatus', trim( stripcslashes( $arrmixValues['status'] ) ) );
        } elseif( isset( $arrmixValues['status'] ) ) {
            $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['status'] ) : $arrmixValues['status'] );
        }
	}

	// Override setOriginalStartDate and setOriginalEndDate functions. It will acccept date in mm/yyyy format and set it to first or last day with mm/dd/yyyy format

	public function setOriginalStartDate( $strOriginalStartDate ) {

		if( false == valStr( $strOriginalStartDate ) ) {
			$this->m_strOriginalStartDate = NULL;

			return;
		}

		$strDateRegex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';

		if( false == preg_match( $strDateRegex, $strOriginalStartDate ) ) {

			$arrstrStartDate = explode( '/', $strOriginalStartDate );

			if( true == valArr( $arrstrStartDate ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrStartDate ) ) {
				$strOriginalStartDate = date( 'm/d/Y', strtotime( $arrstrStartDate[0] . '/01/' . $arrstrStartDate[1] ) );
			}
		}

		$this->m_strOriginalStartDate = $strOriginalStartDate;

	}

	public function setOriginalEndDate( $strOriginalEndDate ) {

		if( false == valStr( $strOriginalEndDate ) ) {
			$this->m_strOriginalEndDate = NULL;

			return;
		}

		$strDateRegex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';

		if( false == preg_match( $strDateRegex, $strOriginalEndDate ) ) {

			$arrstrEndDate = explode( '/', $strOriginalEndDate );

			if( true == valArr( $arrstrEndDate ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrEndDate ) ) {
				$strOriginalEndDate = date( 'm/d/Y', strtotime( $arrstrEndDate[0] . '/01/' . $arrstrEndDate[1] ) );
			}
		}

		$this->m_strOriginalEndDate = $strOriginalEndDate;

	}

	/**
	 * Create Functions
	 */

	public function createJobPhaseMaintenanceProblem() {

		$objJobPhaseMaintenanceProblem = new CJobPhaseMaintenanceProblem();

		$objJobPhaseMaintenanceProblem->setCid( $this->getCid() );
		$objJobPhaseMaintenanceProblem->setJobId( $this->getJobId() );
		$objJobPhaseMaintenanceProblem->setJobPhaseId( $this->getId() );

		return $objJobPhaseMaintenanceProblem;
	}

}
?>
