<?php

class CPropertyTransmissionVendorRemoteLocation extends CBasePropertyTransmissionVendorRemoteLocation {

	protected $m_intLogId;
	protected $m_intStatusId;
	protected $m_intPreviousStatusId;
	protected $m_intSystemEmailId;
	protected $m_intLogCreatedBy;

	protected $m_strResponseText;
	protected $m_strLogCreatedOn;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemoteLocationKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransmissionVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemEmailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseText() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubscriptionRemoteKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUsernameEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPasswordEncripted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 *
	 **/

	public function setLogId( $intLogId ) {
		$this->m_intLogId = CStrings::strToIntDef( $intLogId, NULL, false );
	}

	public function setStatusId( $intStatusId ) {
		$this->m_intStatusId = CStrings::strToIntDef( $intStatusId, NULL, false );
	}

	public function setPreviousStatusId( $intPreviousStatusId ) {
		$this->m_intPreviousStatusId = CStrings::strToIntDef( $intPreviousStatusId, NULL, false );
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->m_intSystemEmailId = CStrings::strToIntDef( $intSystemEmailId, NULL, false );
	}

	public function setResponseText( $strResponseText ) {
		$this->m_strResponseText = CStrings::strTrimDef( $strResponseText, -1, NULL, true );
	}

	public function setLogCreatedBy( $intLogCreatedBy ) {
		$this->m_intLogCreatedBy = CStrings::strToIntDef( $intLogCreatedBy, NULL, false );
	}

	public function setLogCreatedOn( $strLogCreatedOn ) {
		$this->m_strLogCreatedOn = CStrings::strTrimDef( $strLogCreatedOn, -1, NULL, true );
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrstrValues['log_id'] ) ) 					$this->setLogId( $arrstrValues['log_id'] );
		if( true == isset( $arrstrValues['status_id'] ) ) 				$this->setStatusId( $arrstrValues['status_id'] );
		if( true == isset( $arrstrValues['previous_status_id'] ) ) 		$this->setPreviousStatusId( $arrstrValues['previous_status_id'] );
		if( true == isset( $arrstrValues['system_email_id'] ) ) 		$this->setSystemEmailId( $arrstrValues['system_email_id'] );
		if( true == isset( $arrstrValues['response_text'] ) ) 			$this->setResponseText( $arrstrValues['response_text'] );
		if( true == isset( $arrstrValues['log_created_by'] ) ) 			$this->setLogCreatedBy( $arrstrValues['log_created_by'] );
		if( true == isset( $arrstrValues['log_created_on'] ) ) 			$this->setLogCreatedOn( $arrstrValues['log_created_on'] );

		return;
	}

	/**
	 * Get Functions
	 *
	 **/

	public function getLogId() {
		return $this->m_intLogId;
	}

	public function getStatusId() {
		return $this->m_intStatusId;
	}

	public function getPreviousStatusId() {
		return $this->m_intPreviousStatusId;
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function getResponseText() {
		return $this->m_strResponseText;
	}

	public function getLogCreatedBy() {
		return $this->m_intLogCreatedBy;
	}

	public function getLogCreatedOn() {
		return $this->m_strLogCreatedOn;
	}

	public static function buildReputationFilterWhereSql( $objReputationFilter, $boolMatchVendorIds = false ) {
		$strSql = '';
		$strSql .= ( true == valId( $objReputationFilter->getCid() ) ) ? ' AND rl.cid = ' . ( int ) $objReputationFilter->getCid() : '';
		$strSql .= ( true == valArr( $objReputationFilter->getPropertyIds() ) ) ? ' AND rl.property_id IN ( ' . implode( ', ', $objReputationFilter->getPropertyIds() ) . ' )' : '';
		$strSql .= ( false == $boolMatchVendorIds && true == valArr( $objReputationFilter->getSourceIds() ) ) ? ' AND rl.transmission_vendor_id IN ( ' . implode( ', ', $objReputationFilter->getSourceIds() ) . ' )' : '';
		$strSql .= ( true == valStr( $objReputationFilter->getFromDate() ) ) ? ' AND rll.created_on >= \'' . date( 'Y-m-d', strtotime( $objReputationFilter->getFromDate() ) ) . ' 00:00:00\'' : '';
		$strSql .= ( true == valStr( $objReputationFilter->getToDate() ) ) ? ' AND rll.created_on <= \'' . date( 'Y-m-d', strtotime( $objReputationFilter->getToDate() ) ) . ' 23:59:59\'' : '';
		$strSql .= ( true == valArr( $objReputationFilter->getStatuses() ) ) ? ' AND rll.property_transmission_vendor_remote_location_status_id IN  ( ' . implode( ', ', $objReputationFilter->getStatuses() ) . ' )' : '';

		return $strSql;
	}

}
?>