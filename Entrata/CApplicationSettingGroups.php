<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationSettingGroups
 * Do not add any new functions to this class.
 */

class CApplicationSettingGroups extends CBaseApplicationSettingGroups {

	public static function fetchPublishedApplicationSettingGroups( $objDatabase ) {
		return parent::fetchApplicationSettingGroups( 'select asg.* from application_setting_groups asg where asg.is_published = TRUE', $objDatabase );
	}

}
?>