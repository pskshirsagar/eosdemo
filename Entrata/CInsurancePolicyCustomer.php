<?php

class CInsurancePolicyCustomer extends CBaseInsurancePolicyCustomer {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valResidentInsurancePolicyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsInvalidMatch() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == is_null( $this->getId() ) ) {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		} else {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strResult = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		$objInsuranceDatabase      = CDatabases::createDatabase( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::INSURANCE );

		$arrmixData['cid']         = $this->getCid();
		$arrmixData['property_id'] = $this->getPropertyId();
		$arrmixData['lease_id']    = $this->getLeaseId();

		$objResidentInsureLibrary = new CResidentInsureLibrary();
		$objResidentInsureLibrary->createOrUpdateInsuranceLead( $arrmixData, $intCurrentUserId, $objDatabase );

		$objInsuranceDatabase->close();

		return $strResult;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strResult = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		$objInsuranceDatabase      = CDatabases::createDatabase( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::INSURANCE );
		$arrmixData['cid']         = $this->getCid();
		$arrmixData['property_id'] = $this->getPropertyId();
		$arrmixData['lease_id']    = $this->getLeaseId();

		$objResidentInsureLibrary = new CResidentInsureLibrary();
		$objResidentInsureLibrary->createOrUpdateInsuranceLead( $arrmixData, $intCurrentUserId, $objDatabase );

		$objInsuranceDatabase->close();

		return $strResult;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strResult = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		$objInsuranceDatabase      = CDatabases::createDatabase( CDatabaseUserType::PS_PROPERTYMANAGER, CDatabaseType::INSURANCE );
		$arrmixData['cid']         = $this->getCid();
		$arrmixData['property_id'] = $this->getPropertyId();
		$arrmixData['lease_id']    = $this->getLeaseId();

		$objResidentInsureLibrary = new CResidentInsureLibrary();
		$objResidentInsureLibrary->createOrUpdateInsuranceLead( $arrmixData, $intCurrentUserId, $objDatabase );

		$objInsuranceDatabase->close();

		return $strResult;
	}

}
?>