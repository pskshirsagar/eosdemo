<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTaskLogs
 * Do not add any new functions to this class.
 */

class CScheduledTaskLogs extends CBaseScheduledTaskLogs {

	public static function fetchRecentScheduledTaskLogIdByScheduledTaskIdByCid( $intScheduledTaskId, $intCid, $objDatabase ) {

		if( false == valId( $intScheduledTaskId ) ) return false;

		$strSql = 'SELECT
						id
					FROM
						scheduled_task_logs stl
					WHERE
						stl.cid = ' . ( int ) $intCid . '
						AND stl.scheduled_task_id = ' . ( int ) $intScheduledTaskId . '
						AND stl.is_post_date_ignored = 0
					ORDER BY stl.log_datetime DESC
					LIMIT 1';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) )	return $arrstrData[0]['id'];

		return NULL;

	}

	public static function fetchScheduledTaskLogsByScheduledTaskIdByPropertyIdByCid( $intScheduledTaskId, $strScheduledTaskDetailKey, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intScheduledTaskId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strScheduledTaskDetailColumn = 'st1.schedule_details ->> \'num_days\'';
		if( true == in_array( $strScheduledTaskDetailKey, [ 'list_item_id', 'list_type_id' ], false ) ) {
			$strScheduledTaskDetailColumn = 'st1.task_details ->> \'' . $strScheduledTaskDetailKey . '\'';
		}

		$strSql = ' SELECT
						*
					FROM
					    (
					      SELECT
					          *,
					          COALESCE ( LAG ( num_value ) OVER ( partition BY CID, scheduled_task_id ORDER BY id ), - 1 ) AS prev_num_value
					      FROM
					          (
					            SELECT
					                st1.*,
					                COALESCE ( ( ' . $strScheduledTaskDetailColumn . ' )::INTEGER, 0 ) AS num_value,
					                st.default_scheduled_task_id
					            FROM
					                scheduled_task_logs st1
					                JOIN scheduled_tasks st ON( st.id = st1.scheduled_task_id AND st.cid = st1.cid AND st.property_id = st1.property_id )
					            WHERE
					                st1.scheduled_task_id = ' . ( int ) $intScheduledTaskId . '
					                AND st1.cid = ' . ( int ) $intCid . ' AND ' . $strScheduledTaskDetailColumn . ' <> \'\' AND ' . $strScheduledTaskDetailColumn . ' IS NOT NULL
					          ) AS sub_query
					    ) AS inner_query
					WHERE
					    prev_num_value <> num_value AND num_value <> 0
					ORDER BY
					    inner_query.log_datetime DESC';

		return parent::fetchScheduledTaskLogs( $strSql, $objDatabase );
	}

}
?>