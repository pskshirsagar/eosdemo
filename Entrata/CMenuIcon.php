<?php

class CMenuIcon extends CBaseMenuIcon {

	public function valId() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valAdministrativeMenuTypeId() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getAdministrativeMenuTypeId() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'administrative_menu_type_id', '' ) );
		// }

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getName() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ) );
		// }

		return $boolIsValid;
	}

	public function valUri() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getUri() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'uri', '' ) );
		// }

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getFileName() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', '' ) );
		// }

		return $boolIsValid;
	}

	public function valSelectedIdentifier() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getSelectedIdentifier() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'selected_identifier', '' ) );
		// }

		return $boolIsValid;
	}

	public function valTarget() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getTarget() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'target', '' ) );
		// }

		return $boolIsValid;
	}

	public function valAjaxDivName() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getAjaxDivName() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ajax_div_name', '' ) );
		// }

		return $boolIsValid;
	}

	public function valRelatedModule() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getRelatedModule() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'related_module', '' ) );
		// }

		return $boolIsValid;
	}

	public function valRelatedAction() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getRelatedAction() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'related_action', '' ) );
		// }

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		// Validation example

		// if( true == is_null( $this->getIsPublished() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
		// }

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		// Validation example
		// if( true == is_null( $this->getOrderNum() ) ) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
		// }

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>