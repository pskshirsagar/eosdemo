<?php
/**
 * Created by PhpStorm.
 * User: aabdulkhalil
 * Date: 3/24/2020
 * Time: 5:19 PM
 */

class CApDetailReimbursements extends CEosPluralBase {

	public function customBulkInsert( array $arrobjReimbursementApDetails, int $intCurrentUserId, CDatabase $objDatabase ) :bool {

		$arrobjApDetailReimbursements	= [];

		foreach( $arrobjReimbursementApDetails as $objApDetail ) {

			$objApDetailReimbursement	= new CApDetailReimbursement();

			$objApDetailReimbursement->setCid( $objApDetail->getCid() );
			$objApDetailReimbursement->setOriginalApDetailId( $objApDetail->getId() );
			$objApDetailReimbursement->setReimbursementApDetailId( $objApDetail->getReimbursementApDetailId() ?? NULL );
			$objApDetailReimbursement->setReimbursementGlDetailId( $objApDetail->getReimbursementGlDetailId() ?? NULL );
			$objApDetailReimbursement->setAmount( $objApDetail->getTransactionAmount() );

			$arrobjApDetailReimbursements[] = $objApDetailReimbursement;
		}

		if( !parent::bulkInsert( $arrobjApDetailReimbursements, $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function fetchApDetailReimbursementsByOriginalApDetailIdsByCid( array $arrintApDetailIds, int $intCid, CDatabase $objDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_detail_reimbursements
					WHERE
						cid = ' . ( int ) $intCid . '
						AND original_ap_detail_id IN ( ' . implode( ',', $arrintApDetailIds ) . ' )
						AND is_deleted = false
					ORDER BY
						id ASC';

		return parent::fetchObjects( $strSql, CApDetailReimbursement::class, $objDatabase );
	}

	public function fetchApDetailReimbursementsByReimbursementApDetailIdsByCid( array $arrintReimbursementApDetailIds, int $intCid, CDatabase $objDatabase ) {

		if( false == valArr( $arrintReimbursementApDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_detail_reimbursements
					WHERE
						cid = ' . ( int ) $intCid . '
						AND reimbursement_ap_detail_id IN ( ' . implode( ',', $arrintReimbursementApDetailIds ) . ' )
						AND is_deleted = false
					ORDER BY
						id ASC';

		return parent::fetchObjects( $strSql, CApDetailReimbursement::class, $objDatabase );
	}

	public function fetchApDetailReimbursementsByReimbursementGlDetailIdsByCid( array $arrintReimbursementGlDetailIds, int $intCid, CDatabase $objDatabase ) {

		if( false == valArr( $arrintReimbursementGlDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_detail_reimbursements
					WHERE
						cid = ' . ( int ) $intCid . '
						AND reimbursement_gl_detail_id IN ( ' . implode( ',', $arrintReimbursementGlDetailIds ) . ' )
						AND is_deleted = false
					ORDER BY
						id ASC';

		return parent::fetchObjects( $strSql, CApDetailReimbursement::class, $objDatabase );
	}
}