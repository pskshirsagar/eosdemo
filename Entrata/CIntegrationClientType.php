<?php

class CIntegrationClientType extends CBaseIntegrationClientType {

	const AMSI 			 	= 1;
	const YARDI			 	= 2;
	const CREDIT_SYSTEM 	= 3;
	const RESIDENT_CHECK	= 4;
	const MRI			 	= 5;
	const TIMBERLINE	 	= 6;
	const YARDI_RPORTAL	 	= 7;
	const MRI_SIMPLE	 	= 8;
	const TIMBERLINE_PM	 	= 9;
	const TENANT_PRO	 	= 10;
	const PS_DESKTOP	 	= 11;
	const REAL_PAGE	 	 	= 13;
	const JENARK	 	 	= 15;
	const REAL_PAGE_API     = 18;

	const MIGRATION 		= 16;
	const EMH				= 17;
	const EMH_ONE_WAY		= 19;
	Const ENTRATA           = 20;

	public static $c_arrintMigrateIntegrationClientTypeIds	= [ self::MIGRATION ];

	public static $c_arrintAllIntegratedClientTypeIdsForMaintenance = [
		self::YARDI,
		self::MRI,
		self::AMSI,
		self::JENARK,
		self::TENANT_PRO,
		self::REAL_PAGE,
		self::TIMBERLINE,
		self::TIMBERLINE_PM,
		self::REAL_PAGE_API,
		self::YARDI_RPORTAL
	];

	public static $c_arrintIntegratedClientTypeIdsForMaintenanceLocations = [
		self::REAL_PAGE_API
	];

    public static $c_arrintAllIntegrationClientTypeIds = [
    	self::AMSI				=> self::AMSI,
		self::YARDI				=> self::YARDI,
		self::CREDIT_SYSTEM		=> self::CREDIT_SYSTEM,
		self::RESIDENT_CHECK	=> self::RESIDENT_CHECK,
		self::MRI				=> self::MRI,
		self::TIMBERLINE		=> self::TIMBERLINE,
		self::YARDI_RPORTAL		=> self::YARDI_RPORTAL,
		self::MRI_SIMPLE		=> self::MRI_SIMPLE,
		self::TIMBERLINE_PM		=> self::TIMBERLINE_PM,
		self::TENANT_PRO		=> self::TENANT_PRO,
		self::PS_DESKTOP		=> self::PS_DESKTOP,
		self::REAL_PAGE			=> self::REAL_PAGE,
		self::JENARK			=> self::JENARK,
	    self::MIGRATION         => self::MIGRATION,
	    self::EMH               => self::EMH,
	    self::REAL_PAGE_API     => self::REAL_PAGE_API,
	    self::EMH_ONE_WAY       => self::EMH_ONE_WAY,
	    self::ENTRATA           => self::ENTRATA
    ];

	public static $c_arrintIntegrationClientTypeNameById = [
		self::AMSI				=> 'AMSI',
		self::YARDI				=> 'Yardi',
		self::CREDIT_SYSTEM		=> 'Credit System',
		self::RESIDENT_CHECK	=> 'Resident Check',
		self::MRI				=> 'MRI',
		self::TIMBERLINE		=> 'Timberline',
		self::YARDI_RPORTAL		=> 'Yardi RPortal',
		self::MRI_SIMPLE		=> 'MRI Simple',
		self::TIMBERLINE_PM		=> 'Timberline PM',
		self::TENANT_PRO		=> 'Tenant Pro',
		self::PS_DESKTOP		=> 'PsDesktop',
		self::REAL_PAGE			=> 'RealPage',
		self::JENARK			=> 'Jenark',
		self::MIGRATION         => 'Migration',
		self::EMH               => 'eMH',
		self::REAL_PAGE_API     => 'RealPageApi',
		self::EMH_ONE_WAY       => 'eMH One-Way',
		self::ENTRATA           => 'Entrata'
	];

	public static $c_arrintIntegrationClientTypeIdsForReports = [
		self::AMSI	         => self::AMSI,
		self::YARDI	         => self::YARDI,
		self::MRI		     => self::MRI,
		self::JENARK	     => self::JENARK,
		self::TIMBERLINE	 => self::TIMBERLINE,
		self::YARDI_RPORTAL => self::YARDI_RPORTAL,
	];

}
?>