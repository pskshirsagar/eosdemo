<?php

class CLeaseStatusType extends CBaseLeaseStatusType {

	protected $m_intOccupancyTypeId;
	protected $m_arrstrAllLeaseStatusTypes;
	protected $m_arrstrActiveLeaseStatusTypes;

	const APPLICANT			= 1;
	const CANCELLED			= 2;
	const FUTURE			= 3;
	const CURRENT			= 4;
	const NOTICE			= 5;
	const PAST				= 6;

	public static $c_arrintAllLeaseStatusTypes = [
		self::APPLICANT,
		self::CANCELLED,
		self::FUTURE,
		self::CURRENT,
		self::NOTICE,
		self::PAST
	];

	public static $c_arrstrAllLeaseStatusTypes = [
		self::APPLICANT	=> 'Applicant',
		self::CANCELLED	=> 'Cancelled',
		self::FUTURE	=> 'Future',
		self::CURRENT	=> 'Current',
		self::NOTICE	=> 'Notice',
		self::PAST		=> 'Past'
	];

	public static $c_arrintFutureAndActiveLeaseStatusTypeNames = [
		self::FUTURE	=> 'Future',
		self::CURRENT	=> 'Current',
		self::NOTICE	=> 'Notice'
	];

	public static $c_arrintCustomerLeaseStatusTypes = [
		self::APPLICANT,
		self::CANCELLED,
		self::FUTURE,
		self::CURRENT,
		self::NOTICE,
		self::PAST
	];

	public static $c_arrintPreMoveInLeaseStatusTypes                    = [ self::APPLICANT, self::CANCELLED, self::FUTURE ];
	public static $c_arrintActiveLeaseStatusTypes                       = [ self::CURRENT, self::NOTICE ];
	public static $c_arrintCompletedLeaseStatusTypes                    = [ self::PAST ];
	public static $c_arrintInvalidLeaseStatusTypes                      = [ self::APPLICANT, self::CANCELLED ];
	public static $c_arrintActivatedLeaseStatusTypes                    = [ self::CURRENT, self::NOTICE, self::PAST ];
	public static $c_arrintActiveAddOnReservationStatusTypes            = [ self::FUTURE, self::CURRENT, self::NOTICE ];
	public static $c_arrintApprovedLeaseStatusTypeIds                   = [ self::FUTURE, self::NOTICE, self::PAST, self::CURRENT ];
	public static $c_arrintResidentPortalLeaseStatusTypes               = [ self::APPLICANT, self::FUTURE, self::NOTICE, self::PAST, self::CURRENT ];
	public static $c_arrintResidentTransferLeaseStatusTypes             = [ self::APPLICANT, self::FUTURE, self::NOTICE, self::CURRENT ];
	public static $c_arrintOtherIncomeLeaseStatusTypes                  = [ self::CURRENT, self::PAST ];
	public static $c_arrintInvalidWorkOrderLeaseStatusTypes             = [ self::PAST, self::FUTURE, self::CANCELLED ];
	public static $c_arrintInvalidViolationLeaseStatusTypes             = [ self::PAST, self::FUTURE, self::CANCELLED ];
	public static $c_arrintInvalidInspectionLeaseStatusTypes            = [ self::CANCELLED, self::APPLICANT ];
	public static $c_arrintLeaseSigningLeaseStatusTypes                 = [ self::APPLICANT, self::FUTURE, self::CURRENT, self::NOTICE ];
	public static $c_arrintDocumentSigningLeaseStatusTypes              = [ self::FUTURE, self::CURRENT, self::NOTICE ];
	public static $c_arrintRenewalTransferActivatedLeaseStatusTypes     = [ self::APPLICANT, self::CURRENT, self::NOTICE, self::PAST ];
	public static $c_arrintFutureAndActiveLeaseStatusTypes              = [ self::FUTURE, self::CURRENT, self::NOTICE ];
	public static $c_arrintNonActiveLeaseStatusTypes                    = [ self::PAST, self::CANCELLED ];
	public static $c_arrintMoveinPendingLeaseStatusTypeIds              = [ self::APPLICANT, self::FUTURE ];
	public static $c_arrintAMSICallStatus                               = [ self::CURRENT, self::PAST, self::NOTICE, self::FUTURE, self::APPLICANT ];
	public static $c_arrintBulkReverseLeaseStatusTypeIds                = [ self::APPLICANT, self::FUTURE, self::NOTICE, self::PAST, self::CURRENT, self::CANCELLED ];
	public static $c_arrintAdjustmentStatus								= [ self::CURRENT, self::PAST,self::NOTICE, self::FUTURE ];
	public static $c_arrintCustomNotificationLeaseStatusTypeIds	    	= [ self::FUTURE, self::CURRENT ];
	public static $c_arrintCollectionLeaseStatusTypes                   = [ self::PAST, self::CANCELLED ];
	public static $c_arrintPaymentAllowedLeaseStatusTypeIds				= [ self::FUTURE, self::CURRENT,self::PAST,self::APPLICANT ];
	public static $c_arrintActiveOrganizationContractLeaseStatusTypeIds	= [ self::APPLICANT, self::FUTURE, self::CURRENT, self::NOTICE ];
	public static $c_arrintCommercialAllLeaseStatusTypeIds	            = [ self::FUTURE, self::CURRENT, self::NOTICE, self::PAST ];
	public static $c_arrintOccupantLeaseStatusTypes						= [ self::FUTURE, self::NOTICE, self::PAST ];
	public static $c_arrintInsuranceNonActiveLeaseStatusTypes			= [ self::FUTURE, self::NOTICE, self::PAST ];
	public static $c_arrintCurrentApplicantAndNoticeLeaseStatusTypeIds	= [ self::CURRENT, self::APPLICANT, self::NOTICE ];
	public static $c_arrintNoticeAndPastLeaseStatusTypeIds				= [ self::NOTICE, self::PAST ];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getAllLeaseStatusTypes() {

		if( true == valArr( $this->m_arrstrAllLeaseStatusTypes ) )
			return $this->m_arrstrAllLeaseStatusTypes;

		$this->m_arrstrAllLeaseStatusTypes = [
			self::APPLICANT		=> __( 'Applicant' ),
			self::CANCELLED		=> __( 'Cancelled' ),
			self::FUTURE		=> __( 'Future' ),
			self::CURRENT		=> __( 'Current' ),
			self::NOTICE		=> __( 'Notice' ),
			self::PAST			=> __( 'Past' )
		];

		return $this->m_arrstrAllLeaseStatusTypes;
	}

	public function getActiveLeaseStatusTypes() {

		if( true == valArr( $this->m_arrstrActiveLeaseStatusTypes ) )
			return $this->m_arrstrActiveLeaseStatusTypes;

		$this->m_arrstrActiveLeaseStatusTypes = [
			self::FUTURE		=> __( 'Future' ),
			self::CURRENT		=> __( 'Current' ),
			self::NOTICE		=> __( 'Notice' ),
		];

		return $this->m_arrstrActiveLeaseStatusTypes;
	}

	public static function getLeaseStatusTypeNameByLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$strStatusTypeName = NULL;

		switch( $intLeaseStatusTypeId ) {

			case self::APPLICANT:
				$strStatusTypeName = __( 'Applicant' );
				break;

			case self::CANCELLED:
				$strStatusTypeName = __( 'Cancelled' );
				break;

			case self::FUTURE:
				$strStatusTypeName = __( 'Future' );
				break;

			case self::CURRENT:
				$strStatusTypeName = __( 'Current' );
				break;

			case self::NOTICE:
				$strStatusTypeName = __( 'Notice' );
				break;

			case self::PAST:
				$strStatusTypeName = __( 'Past' );
				break;

			default:
				// default case
				break;
		}

		return $strStatusTypeName;
	}

	public static function determineIsCurrent( $intLeaseStatusTypeId ) {
		$arrintCurrentLeaseStatusTypeIds = [ self::CURRENT, self::NOTICE ];

		if( true == in_array( $intLeaseStatusTypeId, $arrintCurrentLeaseStatusTypeIds ) ) {
			return true;
		}

		return false;
	}

	public static function determineIsPast( $intLeaseStatusTypeId ) {
		$arrintPastLeaseStatusTypeIds = [ self::PAST, self::CANCELLED ];

		if( true == in_array( $intLeaseStatusTypeId, $arrintPastLeaseStatusTypeIds ) ) {
			return true;
		}

		return false;
	}

	public static function getLeaseStatusTypeOrder( $intLeaseStatusTypeId ) {
		$intLeaseStatusTypeIdOrder = 8;

		switch( $intLeaseStatusTypeId ) {

			case self::CURRENT:
				$intLeaseStatusTypeIdOrder = 1;
				break;

			case self::NOTICE:
				$intLeaseStatusTypeIdOrder = 2;
				break;

			case self::FUTURE:
				$intLeaseStatusTypeIdOrder = 3;
				break;

			case self::APPLICANT:
				$intLeaseStatusTypeIdOrder = 4;
				break;

			case self::PAST:
				$intLeaseStatusTypeIdOrder = 5;
				break;

			case CLeaseStatusType::CANCELLED:
				$intLeaseStatusTypeIdOrder = 8;
				break;

			default:
				// default case
				break;
		}

		return $intLeaseStatusTypeIdOrder;
	}

	public function getLeaseOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	// set Method

	public function setLeaseOccupancyTypeId( $intApplicationStatusId ) {
		$this->m_intOccupancyTypeId = CStrings::strToIntDef( $intApplicationStatusId, NULL, false );
	}

}
?>