<?php

class CGlAccountType extends CBaseGlAccountType {

	const ASSETS		= 1;
	const LIABILITIES	= 2;
	const EQUITY		= 3;
	const INCOME		= 4;
	const EXPENSES		= 5;

	public static $c_arrintGlAccountTypeIdsByParentGlGroup	= [
		'INCST'	=> [
			self::INCOME,
			self::EXPENSES
		],
		'BALSH'	=> [
			self::ASSETS,
			self::LIABILITIES,
			self::EQUITY
		]
	];

	public static $c_arrintGlAccountTypes = [
		'ASSETS' => self::ASSETS,
		'LIABILITIES' => self::LIABILITIES,
		'EQUITY' => self::EQUITY,
		'INCOME' => self::INCOME,
		'EXPENSES' => self::EXPENSES
	];

	/**
	 * Other Functions
	 */

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'GL_ACCOUNT_TYPE_ASSETS', 			self::ASSETS );
		$objSmarty->assign( 'GL_ACCOUNT_TYPE_LIABILITIES', 		self::LIABILITIES );
		$objSmarty->assign( 'GL_ACCOUNT_TYPE_EQUITY', 			self::EQUITY );
		$objSmarty->assign( 'GL_ACCOUNT_TYPE_INCOME', 			self::INCOME );
		$objSmarty->assign( 'GL_ACCOUNT_TYPE_EXPENSES', 			self::EXPENSES );
	}

	public static function populateTemplateConstants( $arrmixTemplateParameters = array() ) {
		$arrmixTemplateParameters['GL_ACCOUNT_TYPE_ASSETS']      = self::ASSETS;
		$arrmixTemplateParameters['GL_ACCOUNT_TYPE_LIABILITIES'] = self::LIABILITIES;
		$arrmixTemplateParameters['GL_ACCOUNT_TYPE_EQUITY']      = self::EQUITY;
		$arrmixTemplateParameters['GL_ACCOUNT_TYPE_INCOME']      = self::INCOME;
		$arrmixTemplateParameters['GL_ACCOUNT_TYPE_EXPENSES']    = self::EXPENSES;
		return $arrmixTemplateParameters;
	}

	public static function getGlAccountTypeNameById( $intGlAccountTypeId ) {
		switch( $intGlAccountTypeId ) {
			case self::ASSETS:
				return 'ASSETS';
				break;

			case self::LIABILITIES:
				return 'LIABILITIES';
				break;

			case self::EQUITY:
				return 'EQUITY';
				break;

			case self::INCOME:
				return 'INCOME';
				break;

			case self::EXPENSES:
				return 'EXPENSES';
				break;

			default:
				// default case
				break;
		}
	}

	public static function getGlAccountTypes() {
		$arrstrGlAccountTypes = [
			self::INCOME      => __( 'INCOME' ),
			self::EXPENSES    => __( 'EXPENSES' ),
			self::LIABILITIES => __( 'LIABILITIES' ),
			self::ASSETS      => __( 'ASSETS' ),
			self::EQUITY      => __( 'EQUITY' )
		];

		return $arrstrGlAccountTypes;
	}

}
?>