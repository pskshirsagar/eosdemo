<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobTypes
 * Do not add any new functions to this class.
 */

class CJobTypes extends CBaseJobTypes {

	public static function fetchJobTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CJobType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchJobType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CJobType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedJobTypes( $objDatabase ) {

		$strSql = 'SELECT 
                        * 
                    FROM 
                        job_types 
                    WHERE 
                        is_published = true';

		return self::fetchJobTypes( $strSql, $objDatabase );
	}

}
?>