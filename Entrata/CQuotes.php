<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CQuotes
 * Do not add any new functions to this class.
 */

class CQuotes extends CBaseQuotes {

	public static function fetchQuotesDetailByApplcationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false, $boolIncludeCancelledQuotes = false ) {

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';
		$strCheckCancelledQuotes      = ( false == $boolIncludeCancelledQuotes ) ? ' AND q.cancelled_by IS NULL AND q.cancelled_on IS NULL ' : '';
		$strSql = 'SELECT
						q.*,
						pu.unit_number,
						us.building_name,
						COALESCE( pu.number_of_bathrooms, ut.number_of_bathrooms, pf.number_of_bathrooms ) AS number_of_bathrooms,
						COALESCE( pu.number_of_bedrooms, ut.number_of_bedrooms, pf.number_of_bedrooms) AS number_of_bedrooms,
						COALESCE( pu.square_feet, ut.max_square_feet, pf.max_square_feet ) AS max_square_feet,
						COALESCE( pu.square_feet, ut.min_square_feet, pf.min_square_feet ) AS min_square_feet,
						pf.floorplan_name,
						pf.id AS floorplan_id,
						us.unit_space_status_type_id,
						us.property_id
					FROM
						quotes q
						JOIN unit_spaces us ON ( q.unit_space_id = us.id AND q.cid = us.cid ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_units pu ON ( pu.id = us.property_unit_id AND pu.cid = us.cid ' . $strCheckDeletedUnitsSql . ' )
						JOIN unit_types ut ON ( ut.id = pu.unit_type_id AND ut.cid = pu.cid )
						JOIN property_floorplans pf ON ( pf.id = pu.property_floorplan_id AND pf.cid = pu.cid ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						q.cid =  ' . ( int ) $intCid . '
						AND q.application_id = ' . ( int ) $intApplicationId . $strCheckCancelledQuotes . '
						AND q.notified_on IS NOT NULL
					ORDER BY q.lease_start_date desc, q.accepted_on NULLS FIRST, q.expires_on desc, q.id desc';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchNonNotifiedQuotesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $strPropertyTimeZone = CTimeZone::DEFAULT_TIME_ZONE_NAME, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strPropertyDateTime		= getConvertedDateTime( date( 'Y-m-d H:i:s' ), 'Y-m-d H:i:s', CTimeZone::DEFAULT_TIME_ZONE_NAME, $strPropertyTimeZone );

		$strDateTimeSql = 'SELECT NOW() AT TIME ZONE ' . "'" . $strPropertyTimeZone . "'";

		$arrstrResult = ( array ) fetchData( $strDateTimeSql, $objDatabase );

		$strSql = 'SELECT
						q.*,
						us.property_id,
						us.property_unit_id,
						pu.max_pets
					FROM
						quotes q
						JOIN unit_spaces us ON ( q.cid = us.cid AND q.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_units pu ON ( pu.cid = q.cid AND pu.id = us.property_unit_id ' . $strCheckDeletedUnitsSql . ' )
					WHERE
						q.cid =  ' . ( int ) $intCid . '
						AND q.application_id = ' . ( int ) $intApplicationId . '
						AND q.expires_on::TIMESTAMP > ' . "'" . $arrstrResult[0]['timezone'] . "'" . '
						AND q.notified_on IS NULL
						AND q.cancelled_by IS NULL
						AND q.cancelled_on IS NULL';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchQuotesByIdsByCid( $arrintIds, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						q.*,
						pu.unit_number,
						us.building_name,
						COALESCE( pu.number_of_bathrooms, ut.number_of_bathrooms, pf.number_of_bathrooms ) AS number_of_bathrooms,
						COALESCE( pu.number_of_bedrooms, ut.number_of_bedrooms, pf.number_of_bedrooms) AS number_of_bedrooms,
						COALESCE( pu.square_feet, ut.max_square_feet, pf.max_square_feet ) AS max_square_feet,
						COALESCE( pu.square_feet, ut.min_square_feet, pf.min_square_feet ) AS min_square_feet,
						pu.max_pets,
						pf.floorplan_name,
						pf.description,
						us.unit_space_status_type_id,
						us.property_id
					FROM
						quotes q
						JOIN unit_spaces us ON ( q.cid = us.cid AND q.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_units pu ON ( pu.cid = q.cid AND pu.id = us.property_unit_id ' . $strCheckDeletedUnitsSql . ' )
						JOIN unit_types ut ON ( ut.id = pu.unit_type_id AND ut.cid = pu.cid )
						JOIN property_floorplans pf ON ( pf.id = pu.property_floorplan_id AND pf.cid = pu.cid ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						q.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND q.cid = ' . ( int ) $intCid;

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchQuoteByIdByCid( $intId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						q.*,
						us.property_id,
						us.property_unit_id,
						pu.max_pets,
						pu.unit_number,
						us.building_name,
						COALESCE( pu.number_of_bathrooms, ut.number_of_bathrooms, pf.number_of_bathrooms ) AS number_of_bathrooms,
						COALESCE( pu.number_of_bedrooms, ut.number_of_bedrooms, pf.number_of_bedrooms) AS number_of_bedrooms,
						COALESCE( pu.square_feet, ut.max_square_feet, pf.max_square_feet ) AS max_square_feet,
						COALESCE( pu.square_feet, ut.min_square_feet, pf.min_square_feet ) AS min_square_feet,
						pf.floorplan_name,
						pf.description,
						us.unit_space_status_type_id
					FROM
						quotes q
						LEFT JOIN unit_spaces us ON ( q.cid = us.cid AND q.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
						LEFT JOIN property_units pu ON ( pu.cid = q.cid AND pu.id = us.property_unit_id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN unit_types ut ON ( ut.id = pu.unit_type_id AND ut.cid = pu.cid )
						LEFT JOIN property_floorplans pf ON ( pf.id = pu.property_floorplan_id AND pf.cid = pu.cid ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						q.id = ' . ( int ) $intId . '
						AND q.cid = ' . ( int ) $intCid;
		return self::fetchQuote( $strSql, $objDatabase );
	}

	public static function fetchOldNonNotifiedQuotesByCid( $intCid, $objDatabase ) {
		$strSql = ' SELECT
						DISTINCT ( q.* )
					FROM
						quotes q JOIN cached_applications ca ON ( q.application_id = ca.id AND q.cid = ca.cid )
                        JOIN lease_intervals li ON ( li.id = ca.lease_interval_id AND li.cid = ca.cid )
					WHERE
						q.cid =' . ( int ) $intCid . '
						AND q.notified_on IS NULL
						AND q.is_renewal IS FALSE
						AND	q.created_on <= ( NOW ( ) - INTERVAL \'1 HOUR\' )
						AND q.cancelled_on IS NULL
		                AND li.lease_id IS NOT NULL';

		return self::fetchQuotes( $strSql, $objDatabase );

	}

	public static function fetchActiveQuotesByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						q.id
					FROM
						quotes q
						JOIN applications a ON ( a.cid = q.cid AND a.id = q.application_id )
						JOIN unit_spaces us ON ( q.cid = us.cid AND q.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						q.cid = ' . ( int ) $intCid . '
						AND us.id = ' . ( int ) $intUnitSpaceId . '
						AND a.application_status_id IN( ' . CApplicationStatus::STARTED . ', ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ' )
						AND (
								( q.expires_on > NOW() AND q.accepted_on IS NULL )
								OR ( q.accepted_on IS NOT NULL AND q.unit_space_id = a.unit_space_id )
							)
						AND q.cancelled_on IS NULL';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchActiveQuotesByUnitSpaceIdsByCid( $arrintUnitSpaceIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						q.id,
						us.id
					FROM
						quotes q
						JOIN applications a ON ( a.cid = q.cid AND a.id = q.application_id )
						JOIN unit_spaces us ON ( q.cid = us.cid AND q.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						q.cid = ' . ( int ) $intCid . '
						AND us.id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
						AND a.application_status_id IN( ' . CApplicationStatus::STARTED . ', ' . CApplicationStatus::PARTIALLY_COMPLETED . ', ' . CApplicationStatus::COMPLETED . ' )
						AND (
								( q.expires_on > NOW() AND q.accepted_on IS NULL )
								OR ( q.accepted_on IS NOT NULL AND q.unit_space_id = a.unit_space_id )
							)
						AND q.cancelled_on IS NULL';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchQuotesDataByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						q.*,
						us.property_id,
						pu.unit_number,
						pu.id as property_unit_id,
						us.building_name,
						us.property_floorplan_id,
						COALESCE( pu.number_of_bathrooms, ut.number_of_bathrooms, pf.number_of_bathrooms ) AS number_of_bathrooms,
						COALESCE( pu.number_of_bedrooms, ut.number_of_bedrooms, pf.number_of_bedrooms) AS number_of_bedrooms,
						COALESCE( pu.square_feet, ut.max_square_feet, pf.max_square_feet ) AS max_square_feet,
						COALESCE( pu.square_feet, ut.min_square_feet, pf.min_square_feet ) AS min_square_feet,
						pf.floorplan_name,
						us.unit_space_status_type_id,
						us.property_unit_id
					FROM
						quotes q
						JOIN unit_spaces us ON ( q.cid = us.cid AND q.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_units pu ON ( pu.cid = us.cid AND pu.id = us.property_unit_id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN unit_types ut ON ( ut.id = pu.unit_type_id AND ut.cid = pu.cid )
						LEFT JOIN property_floorplans pf ON ( pf.cid = pu.cid AND pf.id = pu.property_floorplan_id ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						q.cid = ' . ( int ) $intCid . '
						AND q.application_id = ' . ( int ) $intApplicationId . '
						AND q.cancelled_by IS NULL
						AND q.cancelled_on IS NULL
						AND q.notified_on IS NOT NULL

						AND q.accepted_on IS NULL
					ORDER BY q.lease_start_date desc, q.accepted_on NULLS FIRST, q.expires_on desc, q.id desc';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchQuotesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
						JOIN applications app ON ( q.cid = app.cid AND q.application_id = app.id )
					WHERE
						app.property_id = ' . ( int ) $intPropertyId . '
						AND q.cid = ' . ( int ) $intCid;

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchActiveQuotesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIsRenewal = true, $strPropertyTimeZone = CTimeZone::DEFAULT_TIME_ZONE_NAME ) {

		$strDateTimeSql = 'SELECT NOW() AT TIME ZONE ' . "'" . $strPropertyTimeZone . "'";

		$arrmixResult = fetchData( $strDateTimeSql, $objDatabase );
		$strSql = 'SELECT
						q.*
					FROM
						quotes q
						LEFT JOIN specials s ON ( q.cid = s.cid AND q.leasing_tier_special_id = s.id AND s.deleted_on IS NULL )
					WHERE
						q.cid =	' . ( int ) $intCid . '
						AND q.application_id = ' . ( int ) $intApplicationId . '
						AND ( q.expires_on >= ' . "'" . $arrmixResult[0]['timezone'] . "'" . ' OR q.expires_on IS NULL )
						AND q.accepted_on IS NULL
						AND q.cancelled_on IS NULL
						AND q.cancelled_by IS NULL';

		$strSql .= ( true == $boolIsRenewal ) ? ' AND q.is_renewal IS TRUE ' : ' AND q.is_renewal IS FALSE';
		$strSql .= ( true == $boolIsRenewal ) ? ' ORDER BY s.min_days_to_lease_start DESC,s.start_date DESC': NULL;
		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchCustomQuotesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIsRenewal = true ) {

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
						LEFT JOIN offer_items oi ON( q.cid = oi.cid AND q.leasing_tier_special_id = oi.ar_origin_reference_id  )
						LEFT JOIN offers o ON( oi.cid = o.cid AND o.id = oi.offer_id )
						LEFT JOIN specials s ON ( q.cid = s.cid AND q.leasing_tier_special_id = s.id )
					WHERE
						q.cid =	' . ( int ) $intCid . '
						AND q.application_id = ' . ( int ) $intApplicationId . '
						AND q.accepted_on IS NULL
						AND q.cancelled_on IS NULL
						AND CASE WHEN s.id IS NOT NULL THEN s.deleted_on IS NULL ELSE TRUE END
						AND CASE WHEN oi.id IS NOT NULL THEN ( oi.deleted_on IS NULL AND oi.deleted_by IS NULL ) ELSE TRUE END
						AND q.cancelled_by IS NULL';

		$strSql .= ( true == $boolIsRenewal ) ? ' AND q.is_renewal IS TRUE ' : ' AND q.is_renewal IS FALSE';
		$strSql .= ( true == $boolIsRenewal ) ? ' ORDER BY o.days_offset DESC, o.start_date': NULL;

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchNonAcceptedRenewalQuotesBySpecialIdsByCid( $arrintSpecialIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSpecialIds ) ) return NULL;

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
						JOIN lease_associations la ON( q.id = la.quote_id AND q.cid = la.cid AND la.ar_orgin_id = ' . CArOrigin::SPECIAL . ' AND ar_origin_reference_id IN ( ' . implode( ',', $arrintSpecialIds ) . ' ) )
					WHERE
						q.cid =	' . ( int ) $intCid . '
						AND q.application_id IS NOT NULL
						AND q.accepted_on IS NULL
						AND q.approval_customer_id IS NULL
						AND q.cancelled_by IS NULL';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchRenewalQuotesWithNonAcceptedRenewalsByApplicationIdByPropertyIdByCid( $intApplicationId, $intCid, $objDatabase, $boolShowFutureOffersInResidentPortal = 0, $strRenewalStartDate = NULL ) {

		$strShowFutureOffers	= ( $boolShowFutureOffersInResidentPortal == 0 ) ? ' NULLS LAST Limit 1':'';

		$strActiveColumn 		= NULL;
		$arrstrOrderBy 			= [];

		array_push( $arrstrOrderBy, 'tos.days_offset DESC' );
		array_push( $arrstrOrderBy, 'tos.offer_id' );

		if( true == valStr( $strRenewalStartDate ) ) {

			$objCurrentDate			= new DateTime();
			$objRenewalStartDate	= new DateTime( $strRenewalStartDate );
			$intDateDifference		= $objRenewalStartDate->diff( $objCurrentDate )->days;
			$strActiveColumn 		= ',CASE WHEN tos.leasing_tier_special_id IS NULL THEN 1
									 WHEN tos.offer_template_type_id IS NOT NULL AND tos.start_date <= \'' . date( 'm/d/Y' ) . '\' AND tos.end_date >= \'' . date( 'm/d/Y' ) . '\' THEN 2
									 WHEN tos.offer_template_type_id IS NULL AND tos.max_days_to_lease_start > ' . ( int ) $intDateDifference . ' THEN 2
									 WHEN tos.offer_template_type_id = \'CAP BASED TIERS\' AND tos.end_date >= \'' . date( 'm/d/Y' ) . '\' THEN 2
									 ELSE 0 END As renewal_tier_rank';
			array_push( $arrstrOrderBy, 'renewal_tier_rank DESC' );
		}

		array_push( $arrstrOrderBy, 'tos.expires_on' . $strShowFutureOffers );

		$strSql = '
		 			WITH temp_offers AS (
						SELECT q.*,
							COALESCE(lag(o.days_offset, 1) OVER(
							ORDER BY o.days_offset), (
														SELECT
															o1.days_offset
														FROM
															offers o1
														WHERE
															o1.cid = o.cid AND
															o1.offer_template_id = o.offer_template_id AND
															o1.deleted_by IS NULL AND
															o1.days_offset < o.days_offset
														ORDER BY o1.days_offset DESC
														LIMIT 1
									) + 1, 0) AS min_days_to_lease_start,
									o.days_offset AS max_days_to_lease_start,
									ot.occupancy_type_id,
									ot.offer_template_type_id,
									o.start_date,
									o.end_date,
									o.days_offset,
									o.id AS offer_id
						FROM quotes q
							JOIN quote_lease_terms qlt ON (q.cid = qlt.cid AND q.id = qlt.quote_id)
							LEFT JOIN offer_items oi ON (oi.cid = q.cid AND q.leasing_tier_special_id = oi.ar_origin_reference_id)
							LEFT JOIN offers o ON (o.cid = q.cid AND oi.offer_id = o.id)
							LEFT JOIN offer_templates ot ON (ot.cid = o.cid AND ot.id = o.offer_template_id)
							LEFT JOIN specials s ON ( s.cid = q.cid AND s.id = q.leasing_tier_special_id)
						WHERE q.application_id = ' . ( int ) $intApplicationId . ' AND
							q.cid = ' . ( int ) $intCid . ' AND
							q.is_renewal IS TRUE AND
							CASE
								WHEN o.id IS NOT NULL THEN o.deleted_on IS NULL
								ELSE TRUE
							END AND
							(q.expires_on >= CURRENT_DATE OR
							q.expires_on IS NULL)
							 AND
							CASE
								WHEN s.id IS NOT NULL AND s.quantity_remaining IS NOT NULL AND ( ot.offer_template_type_id = \'CAP BASED TIERS\' OR ot.offer_template_type_id = \'AUTO ADVANCING DATE BASED TIERS\' ) THEN s.quantity_remaining > 0
								ELSE TRUE
							END
							)
						SELECT
							tos.* ' . $strActiveColumn . '
						FROM temp_offers tos
							ORDER BY ' . implode( ',', $arrstrOrderBy );

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchAcceptedQuotesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM quotes q WHERE q.application_id = ' . ( int ) $intApplicationId . ' AND q.cid = ' . ( int ) $intCid . ' AND q.accepted_on IS NOT NULL LIMIT 1';

		return self::fetchQuote( $strSql, $objDatabase );
	}

	public static function fetchActiveRenewalQuotesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $boolIsRenewResident = false ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		// In case of renew resident we are requesting to consumer to generate pdf after accepting renewal offer in this case accepted on IS NOT NULL.
		$strAcceptedOn = ( $boolIsRenewResident ) ? '' : ' AND q.accepted_on IS NULL';

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
						LEFT JOIN specials s ON ( q.cid = s.cid AND q.leasing_tier_special_id = s.id AND s.deleted_on IS NULL )
					WHERE
						q.cid =	' . ( int ) $intCid . '
						AND q.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND q.expires_on >= CURRENT_DATE
						AND q.is_renewal IS TRUE
						' . $strAcceptedOn;

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchRenewalQuotesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
					WHERE
						q.application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
						AND q.is_renewal IS TRUE
						AND q.cancelled_by IS NULL';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchNonAcceptedRenewalQuotesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						q.*,
						CASE
							WHEN ot.offer_template_type_id = \'CAP BASED TIERS\' THEN oi.name
							WHEN o.days_offset IS NOT NULL THEN oi.name || \' DAYS BEFORE EXP\'
							WHEN o.start_date IS NOT NULL THEN oi.name
							ELSE \'Custom\'
					END AS tier_name,
						a.lease_id,
						s.quantity_remaining
					FROM
						quotes q
						JOIN applications a ON ( q.cid = a.cid AND a.id = q.application_id )
						JOIN quote_lease_terms qlt ON ( q.cid = qlt.cid AND q.id = qlt.quote_id )
						LEFT JOIN specials s ON ( q.cid = s.cid AND q.leasing_tier_special_id = s.id AND s.special_type_id = ' . CSpecialType::RENEWAL_TIER . ' )
						LEFT JOIN offer_items oi ON( s.cid = oi.cid AND oi.ar_origin_reference_id = s.id )
						LEFT JOIN offers o ON( oi.cid = o.cid AND oi.offer_id = o.id )
						LEFT JOIN offer_templates ot ON( ot.cid = o.cid AND o.offer_template_id = ot.id )
					WHERE
						q.application_id IN ( ' . sqlIntImplode( $arrintApplicationIds ) . ' )
						AND q.cid = ' . ( int ) $intCid . '
						AND q.is_renewal IS TRUE
						AND q.accepted_on IS NULL
						AND q.approval_customer_id IS NULL
						AND s.deleted_by IS NULL
						ORDER BY o.days_offset DESC,o.start_date';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchNonPdfRenewalQuotesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
						JOIN quote_lease_terms qlt ON ( q.cid = qlt.cid AND q.id = qlt.quote_id AND qlt.deleted_by IS NULL )
						LEFT JOIN file_associations fa ON ( fa.cid = q.cid AND fa.application_id = q.application_id AND fa.cid = ' . ( int ) $intCid . ' AND fa.deleted_by IS NULL AND fa.quote_id = q.id )
						LEFT JOIN files f ON ( f.cid = fa.cid AND f.id = fa.file_id AND f.cid = ' . ( int ) $intCid . ' )
						LEFT JOIN file_types ft ON ( ft.cid = f.cid AND ft.id = f.file_type_id AND ft.system_code = \'ROS\' AND ft.cid = ' . ( int ) $intCid . ' )
					WHERE
						q.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND q.cid = ' . ( int ) $intCid . '
						AND fa.id IS NULL
						AND q.is_renewal IS TRUE
						AND q.accepted_on IS NULL
						AND q.approval_customer_id IS NULL
						AND q.cancelled_by IS NULL';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchExpiredRenwalQuotesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						q.*
					FROM
						quotes q
						LEFT JOIN specials s ON ( q.cid = s.cid AND q.leasing_tier_special_id = s.id AND s.deleted_on IS NULL )
					WHERE
						q.cid =	' . ( int ) $intCid . '
						AND q.expires_on <= CURRENT_DATE
						AND q.accepted_on IS NULL
						AND ( q.is_renewal IS TRUE AND q.leasing_tier_special_id IS NOT NULL ) ';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchQuotesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
					WHERE
						q.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND q.is_renewal IS TRUE
						AND q.leasing_tier_special_id IS NULL';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchTransferQuotesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
					WHERE
						q.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND q.leasing_tier_special_id IS NULL';

		return self::fetchQuotes( $strSql, $objDatabase );
	}

	public static function fetchQuoteByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIsCancelledBy = false ) {
		if( true === $boolIsCancelledBy ) {
			$strWhereSql = ' AND q.cancelled_by IS NULL';
		}

		$strSql = 'SELECT
						q.*
					FROM
						quotes q
					WHERE
						q.application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . ( int ) $intCid . '
						AND q.is_renewal IS TRUE' . $strWhereSql . '
						AND q.leasing_tier_special_id IS NULL LIMIT 1';

		return self::fetchQuote( $strSql, $objDatabase );
	}

	public static function fetchActiveRenewalQuotesBySpecialGroupIdByCid( $intSpecialGroupId, $intCid, $objDatabase, $intPageNo = 0, $intPageSize = 2 ) {

		if( false == valId( $intSpecialGroupId ) ) return NULL;

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT DISTINCT ON ( ca.cid, ca.id )
						ca.name_first,
						ca.name_last,
						ca.application_datetime,
						ca.lease_end_date,
						ca.unit_number_cache,
						ca.property_name
					FROM
						specials s
						JOIN Quotes q ON ( q.leasing_tier_special_id = s.id AND q.cancelled_on IS NULL AND q.cid = s.cid )
						JOIN cached_applications ca ON ( ca.cid = q.cid AND ca.id = q.application_id AND ( ca.application_stage_id, ca.application_status_id ) IN (' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintRenewalactiveStatusIds ) . ' ) )
					WHERE
						s.special_group_id = ' . ( int ) $intSpecialGroupId . '
						AND s.deleted_by IS NULL
						AND s.cid = ' . ( int ) $intCid;

		$strSql .= ( 0 < $intOffset ? ' OFFSET ' . ( int ) $intOffset : '' );
		$strSql .= ( 0 < $intPageSize ? ' LIMIT ' . ( int ) $intPageSize : '' );

		$arrstrResult = ( array ) fetchData( $strSql, $objDatabase );

		return $arrstrResult;
	}

	public static function fetchRenewalQuotesBySpecialGroupIdByCid( $intSpecialGroupId, $intCid, $objDatabase ) {

		if( false == valId( $intSpecialGroupId ) ) return NULL;

		$strSql = 'SELECT
         q.*
     FROM
         specials s
         JOIN Quotes q ON ( q.leasing_tier_special_id = s.id AND q.cancelled_on IS NULL AND q.cid = s.cid )
         JOIN cached_applications ca ON ( ca.cid = q.cid AND ca.id = q.application_id AND ca.cancelled_on IS NULL AND ( ca.application_stage_id, ca.application_status_id ) IN (' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintRenewalactiveStatusIds ) . ' ) )
     WHERE
         s.special_group_id = ' . ( int ) $intSpecialGroupId . '
         AND s.cid = ' . ( int ) $intCid;

		return self::fetchQuotes( $strSql, $objDatabase );

	}

	public static function fetchRenewalQuotesByOfferTemplateIdByCid( $intOfferTemplateId, $intCid, $objDatabase ) {

		if( false == valId( $intOfferTemplateId ) ) return NULL;

		$strSql = 'SELECT
                            q.*
                    FROM
                        offer_templates ot
                        JOIN offers o ON( ot.cid = o.cid AND ot.id = o.offer_template_id AND o.deleted_by IS NULL )
                        JOIN offer_items oi ON( o.cid = oi.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL )
                        JOIN specials s ON( s.cid = o.cid AND s.id = oi.ar_origin_reference_id )
						JOIN Quotes q ON ( q.leasing_tier_special_id = s.id AND q.cancelled_on IS NULL AND q.cid = s.cid )
						JOIN cached_applications ca ON ( ca.cid = q.cid AND ca.id = q.application_id AND ( ca.application_stage_id, ca.application_status_id ) IN (' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintRenewalactiveStatusIds ) . ' ) )
					WHERE
                        ot.id = ' . ( int ) $intOfferTemplateId . '
                        AND ot.cid = ' . ( int ) $intCid;

		return self::fetchQuotes( $strSql, $objDatabase );

	}

	public static function fetchRenewalQuotesCountBySpecialGroupIdsByCid( $arrintSpecialGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSpecialGroupIds ) ) return NULL;

		$strSql = '	SELECT
						count( DISTINCT ca.id ) as renewal_offer_count
					FROM
						specials s
						JOIN Quotes q ON ( q.leasing_tier_special_id = s.id AND q.cancelled_on IS NULL AND q.cid = s.cid )
						JOIN cached_applications ca ON ( ca.cid = q.cid AND ca.id = q.application_id AND ( ca.application_stage_id, ca.application_status_id ) IN ( ' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintRenewalactiveStatusIds ) . ' ) )
					WHERE
						s.special_group_id IN ( ' . sqlIntImplode( $arrintSpecialGroupIds ) . ' )
					AND s.cid = ' . ( int ) $intCid;

		$arrstrResult = fetchData( $strSql, $objDatabase );

		return $arrstrResult[0]['renewal_offer_count'];
	}

	public static function fetchActiveOfferCountByOfferTemplateIdsByCid( $arrintOfferTemplateIds, $intCid, $objDatabase ) {

		if( false === valArr( $arrintOfferTemplateIds ) ) return NULL;

		$strSql = 'SELECT
						count( DISTINCT a.id ) AS active_offer_count,
						ot.id
					FROM
						offer_templates ot
                        JOIN offers o ON( ot.cid = o.cid AND ot.id = o.offer_template_id AND o.deleted_by IS NULL )
                        JOIN offer_items oi ON ( o.cid = oi.cid AND oi.offer_id = o.id AND oi.deleted_by IS NULL )
                        JOIN specials s ON ( o.cid = s.cid AND s.id = oi.ar_origin_reference_id AND s.deleted_by IS NULL )	
                        LEFT JOIN quotes q ON ( q.cid = o.cid AND q.leasing_tier_special_id = oi.ar_origin_reference_id ANd q.cancelled_by IS NULL )
						LEFT JOIN applications a ON ( q.cid = a.cid AND q.application_id = a.id AND ( a.application_stage_id, a.application_status_id ) IN (' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintRenewalactiveStatusIds ) . ' ) )
					WHERE ot.cid = ' . ( int ) $intCid . ' AND
						  ot.id IN ( ' . sqlIntImplode( $arrintOfferTemplateIds ) . ' ) AND
					      ot.is_published = true AND
					      o.deleted_by IS NULL AND
					      ot.deleted_by IS NULL
					GROUP BY
					      ot.id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActiveQuotesByOfferTemplateIdByCid( $intOfferTemplateId, $intCid, $objDatabase ) {

		if( false == valId( $intOfferTemplateId ) ) return NULL;

		$strSql = 'SELECT DISTINCT ON ( ca.cid, ca.id )
						ca.name_first,
						ca.name_last,
						ca.application_datetime,
						ca.lease_end_date,
						ca.unit_number_cache,
						ca.property_name,
						a.name_last_matronymic
					FROM
						offer_templates ot
                        JOIN offers o ON( ot.cid = o.cid AND ot.id = o.offer_template_id AND o.deleted_by IS NULL )
                        JOIN offer_items oi ON( o.cid = oi.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL )
                        JOIN specials s ON( s.cid = o.cid AND s.id = oi.ar_origin_reference_id AND s.deleted_by IS NULL AND s.special_type_id = ' . CSpecialType::RENEWAL_TIER . ')
						JOIN Quotes q ON ( q.leasing_tier_special_id = s.id AND q.cancelled_on IS NULL AND q.cid = s.cid )
						JOIN cached_applications ca ON ( ca.cid = q.cid AND ca.id = q.application_id AND ( ca.application_stage_id, ca.application_status_id ) IN (' . sqlIntMultiImplode( CApplicationStageStatus::$c_arrintRenewalactiveStatusIds ) . ' ) )
						JOIN applicants a ON ( a.cid = ca.cid AND a.id = ca.primary_applicant_id  )
					WHERE
						ot.id = ' . ( int ) $intOfferTemplateId . '
						AND s.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRenewalQuoteDetailsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {

		if( false == valId( $intApplicationId ) ) return NULL;

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlansSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = ' SELECT
						q.*,
						us.property_id,
						us.property_unit_id,
						pu.max_pets,
						pu.unit_number,
						us.building_name,
						COALESCE( pu.number_of_bathrooms, ut.number_of_bathrooms, pf.number_of_bathrooms ) AS number_of_bathrooms,
						COALESCE( pu.number_of_bedrooms, ut.number_of_bedrooms, pf.number_of_bedrooms) AS number_of_bedrooms,
						COALESCE( pu.square_feet, ut.max_square_feet, pf.max_square_feet ) AS max_square_feet,
						COALESCE( pu.square_feet, ut.min_square_feet, pf.min_square_feet ) AS min_square_feet,
						pf.floorplan_name,
						pf.description,
						us.unit_space_status_type_id
					FROM
						quotes q
						LEFT JOIN unit_spaces us ON ( q.cid = us.cid AND q.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
						LEFT JOIN property_units pu ON ( pu.cid = q.cid AND pu.id = us.property_unit_id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN unit_types ut ON ( ut.id = pu.unit_type_id AND ut.cid = pu.cid )
						LEFT JOIN property_floorplans pf ON ( pf.id = pu.property_floorplan_id AND pf.cid = pu.cid ' . $strCheckDeletedFloorPlansSql . ' )
					WHERE
						q.application_id = ' . ( int ) $intApplicationId . '
						AND q.cid = ' . ( int ) $intCid . '
						ORDER BY q.id DESC
						LIMIT 1';

		return self::fetchQuote( $strSql, $objDatabase );
	}

}
?>
