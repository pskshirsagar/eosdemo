<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPaymentLogs
 * Do not add any new functions to this class.
 */

class CApPaymentLogs extends CBaseApPaymentLogs {

	public static function fetchApPaymentLogsByApPaymentIdByActionsByCid( $intApPaymentId, $intCid, $objDatabase, $arrstrActions = NULL ) {

		$strActionsCondition = '';

		if( true == valArr( $arrstrActions ) ) {
			$strActionsCondition = ' AND apl.action IN( \'' . implode( '\'' . ', \'', $arrstrActions ) . '\' )';
		}

		$strSql = 'SELECT
						apl.*,
						ARRAY_TO_STRING ( ARRAY[ ce.name_first, ce.name_last ], \' \' ) AS company_employee_full_name
					FROM
						ap_payment_logs apl
						JOIN company_users cu ON ( cu.id = apl.updated_by AND cu.cid = apl.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
					WHERE
						apl.ap_payment_id = ' . ( int ) $intApPaymentId . '
						AND apl.cid = ' . ( int ) $intCid .
						$strActionsCondition . '
					ORDER BY
						apl.id DESC';

		return self::fetchApPaymentLogs( $strSql, $objDatabase );
	}

}
?>