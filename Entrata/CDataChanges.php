<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataChanges
 * Do not add any new functions to this class.
 */

class CDataChanges extends CBaseDataChanges {

	public static function fetchDataChangesByDataChangeTypeIdByCid( $intDataChangeTypeId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM
						data_changes
					WHERE
						data_change_type_id = ' . ( int ) $intDataChangeTypeId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY
						processed_on DESC,
						processed_on NULLS FIRST';

		return self::fetchDataChanges( $strSql, $objClientDatabase );
	}

	public static function fetchCustomUnprocessedDataChangesByDataChangeTypeIds( $arrintDataChangeTypeIds, $objClientDatabase ) {
		if( false == valArr( $arrintDataChangeTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						data_changes
					WHERE
						processed_on IS NULL
						AND data_change_type_id IN ( ' . implode( ',', $arrintDataChangeTypeIds ) . ' )
					ORDER BY
						id ASC';

		return self::fetchDataChanges( $strSql, $objClientDatabase, false );
	}

	public static function fetchTransacationsCountForGlAccountByGlAccountIdByCid( $intGlAccountId, $intCid, $objClientDatabase, $boolIsFromValidation = false ) {
		$arrstrSqls = [];

		$arrstrSqls[] = 'SELECT
							\'ap_allocations\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( aa.id ) AS row_count
						FROM
							ap_allocations aa
						WHERE
							aa.cid = ' . ( int ) $intCid . '
							AND ( aa.ap_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.purchases_clearing_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.pending_reimbursements_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.inter_co_ap_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.bank_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.charge_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.credit_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.charge_wip_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.credit_wip_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.accrual_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.accrual_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.cash_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
												OR aa.cash_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'ap_detail_logs\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_LOG . '\' AS table_type,
							COUNT( adl.id ) AS row_count
						FROM
							ap_detail_logs adl
						WHERE
							adl.cid = ' . ( int ) $intCid . '
							AND ( adl.gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.ap_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.wip_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.purchases_clearing_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.pending_reimbursements_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.inter_co_ap_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.bank_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.accrual_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.accrual_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.cash_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adl.cash_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'ap_details\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( ad.id ) AS row_count
						FROM
							ap_details ad
						WHERE
							ad.cid = ' . ( int ) $intCid . '
							AND ( ad.gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.ap_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.wip_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.purchases_clearing_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.pending_reimbursements_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.inter_co_ap_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.bank_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.accrual_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.accrual_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.cash_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ad.cash_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'ap_formula_properties\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( afp.id ) AS row_count
						FROM
							ap_formula_properties afp
						WHERE
							afp.cid = ' . ( int ) $intCid . '
							AND afp.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'ap_payees\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( ap.id ) AS row_count
						FROM
							ap_payees ap
						WHERE
							ap.cid = ' . ( int ) $intCid . '
							AND ap.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'ap_payee_accounts\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( apa.id ) AS row_count
						FROM
							ap_payee_accounts apa
						WHERE
							apa.cid = ' . ( int ) $intCid . '
							AND apa.default_gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'ap_payee_sub_accounts\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( apsa.id ) AS row_count
						FROM
							ap_payee_sub_accounts apsa
						WHERE
							apsa.cid = ' . ( int ) $intCid . '
							AND apsa.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'ar_allocations\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( aa.id ) AS row_count
						FROM
							ar_allocations aa
						WHERE
							aa.cid = ' . ( int ) $intCid . '
							AND ( aa.accrual_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR aa.accrual_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR aa.cash_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR aa.cash_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'ar_codes\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( ac.id ) AS row_count
						FROM
							ar_codes ac
						WHERE
							ac.cid = ' . ( int ) $intCid . '
							AND ( ac.credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ac.debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ac.modified_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'ar_deposit_transactions\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( adt.id ) AS row_count
						FROM
							ar_deposit_transactions adt
						WHERE
							adt.cid = ' . ( int ) $intCid . '
							AND ( adt.credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR adt.debit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'ar_transactions\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( at.id ) AS row_count
						FROM
							ar_transactions at
						WHERE
							at.cid = ' . ( int ) $intCid . '
							AND ( at.accrual_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR at.accrual_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR at.cash_credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR at.cash_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'gl_settings\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( gs.id ) AS row_count
						FROM
							gl_settings gs
						WHERE
							gs.cid = ' . ( int ) $intCid . '
							AND ( gs.ap_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gs.gain_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gs.loss_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gs.market_rent_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gs.retained_earnings_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gs.vacancy_loss_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gs.floating_reimbursements_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gs.inter_company_ar_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gs.inter_company_ap_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'fee_template_rules\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( ftr.id ) AS row_count
						FROM
							fee_template_rules ftr
						WHERE
							ftr.cid = ' . ( int ) $intCid . '
							AND ftr.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'fee_templates\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( ft.id ) AS row_count
						FROM
							fee_templates ft
						WHERE
							ft.cid = ' . ( int ) $intCid . '
							AND ft.gl_account_id = ' . ( int ) $intGlAccountId;

		if( !$boolIsFromValidation ) {

			$arrstrSqls[] = 'SELECT
							\'gl_account_properties\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( gap.id ) AS row_count
						FROM
							gl_account_properties gap
						WHERE
							gap.cid = ' . ( int ) $intCid . '
							AND gap.gl_account_id = ' . ( int ) $intGlAccountId;
		}

		$arrstrSqls[] = 'SELECT
							\'budget_gl_account_months\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( bgam.id ) AS row_count
						FROM
							budget_gl_account_months bgam
						WHERE
							bgam.cid = ' . ( int ) $intCid . '
							AND bgam.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'gl_detail_logs\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_LOG . '\' AS table_type,
							COUNT( gdl.id ) AS row_count
						FROM
							gl_detail_logs gdl
						WHERE
							gdl.cid = ' . ( int ) $intCid . '
							AND ( gdl.accrual_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gdl.cash_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'gl_details\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( gd.id ) AS row_count
						FROM
							gl_details gd
						WHERE
							gd.cid = ' . ( int ) $intCid . '
							AND ( gd.accrual_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gd.cash_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'accounting_export_batch_details\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( gebd.id ) AS row_count
						FROM
							accounting_export_batch_details gebd
						WHERE
							gebd.cid = ' . ( int ) $intCid . '
							AND ( gebd.accrual_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR gebd.cash_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'maintenance_request_materials\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( mrm.id ) AS row_count
						FROM
							maintenance_request_materials mrm
						WHERE
							mrm.cid = ' . ( int ) $intCid . '
							AND mrm.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'old_fee_rules\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( fr.id ) AS row_count
						FROM
							old_fee_rules fr
						WHERE
							fr.cid = ' . ( int ) $intCid . '
							AND fr.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'property_ar_codes\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( pac.id ) AS row_count
						FROM
							property_ar_codes pac
						WHERE
							pac.cid = ' . ( int ) $intCid . '
							AND ( pac.credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pac.debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pac.modified_debit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'unit_space_exclusions\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( pet.id ) AS row_count
						FROM
							unit_space_exclusions pet
						WHERE
							pet.cid = ' . ( int ) $intCid . '
							AND pet.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'property_gl_balances\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( pgb.id ) AS row_count
						FROM
							property_gl_balances pgb
						WHERE
							pgb.cid = ' . ( int ) $intCid . '
							AND pgb.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'property_gl_settings\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_SETUP . '\' AS table_type,
							COUNT( pgs.id ) AS row_count
						FROM
							property_gl_settings pgs
						WHERE
							pgs.cid = ' . ( int ) $intCid . '
							AND ( pgs.ap_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.retained_earnings_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.market_rent_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.gain_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.loss_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.vacancy_loss_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.delinquent_rent_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.prepaid_rent_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.inter_co_ar_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.inter_co_ap_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR pgs.purchases_clearing_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'scheduled_ap_transaction_details\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( satd.id ) AS row_count
						FROM
							scheduled_ap_transaction_details satd
						WHERE
							satd.cid = ' . ( int ) $intCid . '
							AND satd.gl_account_id = ' . ( int ) $intGlAccountId;

		$arrstrSqls[] = 'SELECT
							\'depreciation_categories\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( dc.id ) AS row_count
						FROM
							depreciation_categories dc
						WHERE
							dc.cid = ' . ( int ) $intCid . '
							AND ( dc.depreciation_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR dc.accum_depreciation_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'ap_code_categories\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( acc.id ) AS row_count
						FROM
							ap_code_categories acc
						WHERE
							acc.cid = ' . ( int ) $intCid . '
							AND ( acc.debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR acc.credit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR acc.consumption_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'ap_codes\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( ac.id ) AS row_count
						FROM
							ap_codes ac
						WHERE
							ac.cid = ' . ( int ) $intCid . '
							AND ac.ap_code_type_id <> ' . CApCodeType::GL_ACCOUNT . '
							AND ( ac.item_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ac.ap_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ac.wip_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR ac.consumption_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'asset_transactions\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( at.id ) AS row_count
						FROM
							asset_transactions at
						WHERE
							at.cid = ' . ( int ) $intCid . '
							AND ( at.debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR at.credit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$arrstrSqls[] = 'SELECT
							\'asset_allocations\' AS table_name,
							\'' . CDataChange::TABLE_TYPE_TRANSACTION . '\' AS table_type,
							COUNT( aa.id ) AS row_count
						FROM
							asset_allocations aa
						WHERE
							aa.cid = ' . ( int ) $intCid . '
							AND ( aa.debit_gl_account_id = ' . ( int ) $intGlAccountId . '
									OR aa.credit_gl_account_id = ' . ( int ) $intGlAccountId . '
								)';

		$strSql = implode( ' UNION ALL ', $arrstrSqls ) . ' ORDER BY table_name ASC;';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>