<?php

class CEventType extends CBaseEventType {

	private $m_arrmixTrafficEventTypes;

	const EMAIL_INCOMING						= 1;
	const EMAIL_OUTGOING 						= 2;
	const CALL_INCOMING  						= 3;
	const CALL_OUTGOING  						= 4;
	const SMS_INCOMING   						= 5;
	const SMS_OUTGOING   						= 6;
	const ONLINE_CHAT							= 7;
	const NOTES									= 8;
	const ONSITE_VISIT							= 9;
	const ONLINE_GUEST_CARD						= 10;
	const PRE_SCREENING							= 11;
	const APPLICATION_PROGRESS					= 12;
	const LEASE_PROGRESS						= 13;
	const RENEWAL_PROGRESS						= 14;
	const SCHEDULED_FOLLOWUP					= 15;
	const VOICE_MAIL							= 16;
	const SCHEDULED_APPOINTMENT					= 17;
	const DUPLICATE_LEAD						= 18;
	const RESIDENT_LOG_IN_BY_MANAGER			= 19;
	const RESIDENT_LOG_IN						= 20;
	const STATUS_CHANGE							= 21;
	const EVICTION_NOTICE						= 22;
	const STANDARD_NOTICE						= 23;
	const INVOICE_SENT							= 24;
	const RENEWAL_OFFER_SENT					= 25;
	const LEASE_RENEWED							= 26;
	const MOVED_TO_MONTH_TO_MONTH				= 27;
	const CLOSED								= 28;
	const FEEDBACK								= 29;
	const FINANCIAL_MOVE_OUT					= 30;
	const REVERSE_FINANCIAL_MOVE_OUT			= 31;
	const OTHER									= 32;
	const INVITATION_EMAIL						= 33;
	const DENIED								= 34;
	const REVERSE_MOVE_OUT						= 35;
	const PROPERTY_CHANGE						= 36;
	const SMS_OPT_IN							= 37;
	const SMS_OPT_OUT							= 38;
	const EMAIL_OPT_IN							= 39;
	const EMAIL_OPT_OUT							= 40;
	const SCREENING_DECISION					= 41;
	const ADVERSE_ACTION_LETTER_GENERATED		= 42;
	const LEASE_INFO							= 43;
	const PAYMENT_STATUS_CHANGE 				= 44;
	const SCHEDULE_CHARGE_INFO		 			= 45;
	const LEDGER_INFO				 			= 46;
	const CANCELED								= 47;
	const EVICTED								= 48;
	const SKIPPED								= 49;
	const MONTH_TO_MONTH						= 50;
	const CRAIGSLIST_POST_REMINDER				= 51;
	const UNIT_TRANSFER							= 52;
	const FIRST_NOTICE							= 53;
	const REPEAT_NOTICE							= 54;
	const FINAL_NOTICE							= 55;
	const LATE_NOTICE_RESET						= 56;
	const LEASING_CENTER_WELCOME_CALL			= 57;
	const COMBINED								= 58;
	const DELINQUENCY_NOTES						= 59;
	const LEASE_SENT_TO_COLLECTIONS				= 60;
	const LEASE_REMOVED_FROM_COLLECTIONS		= 61;
	const ADD_ON								= 62;
	const MOVE_IN								= 63;
	const MOVE_OUT								= 64;
	const LATE_FEE 								= 65;
	const CLONED								= 66;
	const UNIT_CHANGE							= 67;
	const DEMO_SCREENING_PROCESSED				= 68;
	const DEMAND_NOTICE							= 69;
	const CONTACT_SUBMISSION					= 70;
	const PAYMENT_RECEIPT						= 71;
	const RENT_REMINDER							= 72;
	const RECURRING_PAYMENT						= 73;
	const REMOVE_OCCUPANT						= 77;
	const TOUR									= 78;
	const MOVE_IN_REVIEW_COMPLETED				= 80;
	const CALL_CENTER							= 81;
	const FIRST_PRE_COLLECTION_LETTER_SENT		= 82;
	const SCREENING_COMPLETE_EMAIL_TO_MANAGER	= 83;
	const REVERSE_MOVE_IN						= 84;
	const CANCEL_LEASE							= 85;
	const RENEWAL_OFFER_REVIEWED				= 86;
	const MOVE_IN_ADJUSTMENT					= 87;
	const MONTH_PERIOD_CLOSE					= 88;
	const MONTH_YEAR_OPEN						= 89;
	const MONTH_YEAR_CLOSE						= 90;
	const CHORE_CREATED							= 91;
	const CHORE_COMPLETED						= 92;
	const MONTH_PERIOD_OPEN						= 93;
	const PRINT_APPLICATION						= 94;
	const DOWNLOAD_APPLICATION_DETAILS			= 95;
	const DOWNLOAD_APPLICATION_DOCUMENT			= 96;
	const SIGNED_LEASE_UPLOADED					= 97;
	const LEASE_EXCLUDED_FROM_COLLECTIONS		= 98;
	const EXCLUSION_FROM_COLLECTIONS_REMOVED	= 99;
	const SECOND_PRE_COLLECTION_LETTER_SENT		= 100;
	const FINAL_PRE_COLLECTION_LETTER_SENT		= 101;
	const LEASE_END								= 102;
	const MONTH_PERIOD_ADVANCED					= 103;
	const MONTH_PERIOD_ROLLBACK					= 104;
	const PROSPECT_LOGIN_BY_MANAGER				= 107;
	const SD_GROUP_DISSOCIATION					= 105;
	const ROOMMATE_NOTIFICATION_SENT			= 106;

	const GENERATE_WAITLIST_OFFER					= 108;
	const WAITLIST_APPLICATION_REMOVED_BY_APPLICANT	= 109;
	const WAITLIST_APPLICATION_REMOVED_BY_PROPERTY	= 110;
	const APPLICANT_DECLINED_WAITLIST_OFFER			= 111;
	const APPLICANT_ACCEPTED_WAITLIST_OFFER			= 112;
	const OFFER_EXPIRED								= 113;
	const OFFER_SKIPPED 							= 114;
	const REVERSE_MOVE_IN_CHECKLIST					= 115;
	const SD_BULK_UNIT_ASSIGNMENT					= 116;
	const DEMAND_FOR_POSSESSION						= 117;

	const UNIT_UNAVAILABLE							= 118;
	const RENEWAL_OFFER_EDITED						= 119;
	const DOCUMENT_DELETED							= 120;
	const RESIDENT_USERNAME_PASSWORD_CHANGE			= 121;
	const RENEWAL_END_DATE_CHANGED					= 122;

	const RESIDENT_PAYMENT_DUE						= 129;
	const RESIDENT_BIRTHDAY							= 130;

	const ADD_APPLICANT								= 131;
	const DELETE_APPLICANT							= 132;
	const REACTIVATE_APPLICANT						= 133;
    const PRIMARY_APPLICANT_CHANGED					= 239;

	const SKIP_GUARANTOR_LEASE						= 141;
	const APPLICATION_UPDATED						= 142;
	const SCREENING_STATUS							= 143;

	const FOLLOW_UP									= 145;
	const MUTE_FOLLOW_UPS					        = 146;
	const UNMUTE_FOLLOW_UPS					        = 147;
	const APPLICATION_PARTIALLY_COMPLETE_EMAIL_SENT = 148;

	const TERMINATION_STARTED  						= 149;
	const TERMINATION_CANCELLED  					= 150;

	const MOVE_OUT_ADJUSTMENT						= 155;
	const LEASE_START								= 156;
	const QUOTE_APPLIED								= 157;

	const SIGNING_INVITATION_EMAIL					= 158;
	const REVISE_FINANCIAL_MOVE_OUT					= 160;
	const SD_RESET_UNIT_ASSIGNMENT					= 161;
	const ALARM_KEY_CHANGE							= 163;
	const NON_RENEWAL_NOTICE						= 165;
	const CUSTOMER_TYPE_CHANGE						= 167;
	const CHECKLIST_ITEM_COMPLETED					= 168;

	const CHECKLIST_ITEM_REVERSED					= 169;
	const LEASE_COUNTERSIGN							= 172;
	const RESIDENT_VISIT							= 173;
	const NSF_NOTICE								= 174;
	const NSF_NOTES									= 175;
	const PARENTAL_CONSENT_FORM_SIGNED				= 176;
	const ASSET_PLACED_IN_SERVICE					= 177;
	const ASSET_TRANSFER							= 178;
	const RENEWAL_EVENT_TRIGGER_FOR_INSPECTION		= 179;
	const SPLIT_LEASE								= 181;
	const REPAYMENT_AGREEMENT_CREATED				= 182;
	const REPAYMENT_AGREEMENT_CANCELLED				= 183;
	const REPAYMENT_AGREEMENT_LATE_NOTICE			= 184;
	const REPAYMENT_AGREEMENT_NOTES					= 185;
	const ADD_UPDATE_INSURANCE_POLICY				= 186;
	const SCREENING_PROCESSED 						= 188;
	const SCREENING_VALIDATION						= 189;
	const LEASE_RENEWAL								= 190;

	const MARKED_AS_CONTACTED						= 200;
	const RESIDENT_LOG_IN_FROM_APP					= 201;
	const RESIDENT_LOG_IN_FROM_WHITE_LABELED_APP	= 489;

	const PRIMARY_RESIDENT_CORRECTION				= 480;
	const MID_LEASE_CUSTOMER_RELATIONSHIP_CHANGE	= 202;
	const MID_LEASE_MOVE_IN							= 203;
	const MID_LEASE_CANCEL_MOVE_IN					= 204;
	const MID_LEASE_CANCEL_NOTICE					= 205;
	const MID_LEASE_MOVE_OUT						= 206;
	const UNIT_SPACE_CONFIGURATION_NOTES			= 207;
	const SCREENING_DECISION_RESET					= 208;
    const SCREENING_RECOMMENDATION_RECALCULATED		= 356;

	const INSTALLMENT_CHANGE						= 209;
	const HOLIDAY									= 215;
	const PARENTAL_CONSENT_FORM_EMAIL_SENT			= 216;

	const GUEST_ADDED_IN_RESIDENT_PORTAL            = 217;
	const GUEST_ADDED_IN_ENTRATA 		            = 218;
	const GUEST_UNIT_ACCESS_IN_RESIDENT_PORTAL 		= 220;
	const GUEST_UNIT_ACCESS_IN_ENTRATA 		        = 221;

	const FINANCIAL_MOVE_OUT_STARTED                = 222;
	const FINANCIAL_MOVE_OUT_APPROVED               = 223;
	const FINANCIAL_MOVE_OUT_REJECTED               = 224;
	const ACCELERATED_RENT_ADJUSTMENT				= 225;

	const UPDATE_SPACE_CONFIGURATION   				= 226;
	const SCREENING_CONDITIONS                      = 227;
	const LIST_TYPE_CHANGE							= 228;
	const LATE_PAYMENT_COUNT_UPDATED                = 229;
	const WORK_ORDER_UPDATED						= 230;
	const ISSUE_REFUND								= 231;
	const GUEST_CARD_BACK_TRACK						= 238;

	const ADD_PET                                   = 240;
	const DELETE_PET                                = 241;
	const REASSOCIATE_PET                           = 242;
	const ADD_VEHICLE                               = 243;
	const DELETE_VEHICLE                            = 244;
	const REASSOCIATE_VEHICLE                       = 245;
	const CORPORATE_DETAILS_CHANGE                  = 247;

	const MERGE_LEASE                               = 248;
	const UNMERGE_LEASE                             = 249;
	const STATEMENT_EMAILED                         = 250;
	const RETURNED_PAYMENT_COUNT_UPDATED            = 251;

	const WAIVE_LATE_FEE_CHANGE						= 273;

	const QUOTE_GENERATED            				= 274;
	const NOT_PROGRESSING							= 275;

	const REASSOCIATE_PACKAGE						= 276;

	const RECERTIFICATION_120_DAYS_NOTICE			= 278;
	const RECERTIFICATION_90_DAYS_NOTICE			= 279;
	const RECERTIFICATION_60_DAYS_NOTICE			= 280;
	const DRAW_REQUEST_DEPOSIT_REVERSED				= 281;

	const VENDOR_STATUS_CHANGED						= 282;
	const VENDOR_ADDED								= 283;
	const VENDOR_EDITED								= 284;
	const VENDOR_DOCUMENT_ADDED						= 285;
	const VENDOR_DOCUMENT_DELETED					= 286;
	const VENDOR_NOTES								= 287;
	const VENDOR_OUTGOING_CALL                      = 469;
	const VENDOR_INCOMING_CALL                      = 470;
	const VENDOR_INCOMING_EMAIL                     = 471;

	const INSPECTION_UPDATED                        = 292;
	const TRANSFER_QUOTE_DECLINED					= 293;
	const TRANSFER_QUOTE_RESENT					    = 294;
	const DELINQUENCY_POLICY_CHANGE                 = 295;
	const DELINQUENCY_NOTICE                        = 296;
	const TRANSFER_QUOTES                           = 297;
	const RETURN_TO_PREVIOUS_STOP                   = 298;
	const RETURN_TO_BEGINNING                       = 299;

	const GUEST_PARCEL_PICKUP_PERMITTED_IN_RESIDENT_PORTAL		= 307;
	const GUEST_PARCEL_PICKUP_PERMITTED_IN_ENTRATA				= 308;
	const ESIGN_DOCUMENT_INITIATED                  = 309;
	const ESIGN_DOCUMENT_SIGNED                     = 310;
	const COLLECTION_POLICY_CHANGE                  = 312;
	const ENTRATAMATION                             = 313;

	const SPECIAL_QUALIFIED                         = 316;
	const COLLECTIONS_NOTICE						= 317;
	const WRITE_OFF_BALANCE							= 318;
	const MONEYGRAM_INSTRUCTION_EMAIL				= 319;

	const UNIT_NOTES                                = 320;
	const STORED_BILLING_ACCOUNT_ADDED				= 321;
	const STORED_BILLING_ACCOUNT_UPDATED			= 322;
	const STORED_BILLING_ACCOUNT_DELETED			= 323;
	const UNIT_GENDER_CHANGE			            = 328;

	const CORPORATE_CARE_REQUEST_ADDED				= 325;
	const CORPORATE_CARE_REQUEST_ASSIGNED			= 326;
	const CORPORATE_CARE_REQUEST_REASSIGNED			= 327;
	const CORPORATE_CARE_REQUEST_DISMISSED			= 354;

	const PAYMENT_SUBMITTED                         = 329;
	const LEASE_PARTIALLY_COMPLETED                 = 332;
	const LEASE_COMPLETED                           = 333;
	const VIOLATION_MANAGER                         = 334;
	const APPLICANT_GROUPING                        = 342;

	const LEASE_RETRIEVED_BY_COLLECTIONS_AGENCY = 335;

	const RECURRING_PAYMENT_ADDED					= 336;
	const RECURRING_PAYMENT_UPDATED					= 337;
	const RECURRING_PAYMENT_DELETED					= 338;

	const CONTACT_SUBMISSION_CALL_OUTGOING			= 339;
	const CONTACT_SUBMISSION_RESIDENT_VISIT			= 340;

	const SUBSIDY_CERTIFICATION_UPDATED				= 341;

	const CREATE_CORPORATE_LEADS                    = 343;
	const CORPORATE_SCREENING_DECISION              = 344;
	const CORPORATE_SCREENING_STATUS                = 345;
	const CORPORATE_EMAIL_OUTGOING                  = 346;
	const EARLY_TERMINATION                         = 362;

	const OWNER_ADDED								= 347;
	const OWNER_DISABLED							= 348;
	const OWNER_RE_ENABLED							= 349;
	const OWNER_DOCUMENT_ADDED						= 350;
	const OWNER_DOCUMENT_DELETED					= 351;
	const OWNER_DISTRIBUTION_GENERATED				= 352;
	const OWNER_NOTES								= 353;

	const CDMP_STATUS_CHANGE                        = 330;
	const UNIT_SPACE_STATUS_CHANGE                  = 331;
	const MOVE_IN_SCHEDULER_MOVE_IN_DATE_CHANGE     = 357;

	const OVERRIDE_RENT_LIMIT                       = 355;

	const REGENERATE_FMO_STATEMENT                  = 361;

	const ASSET_EDITED                              = 360;

	const INCOME_CHANGE                             = 363;
	const ASSET_CHANGE                              = 364;
	const EXPENSE_CHANGE                            = 365;
	const MTM_OFFER_ACCEPTED						= 368;
	const SUITE_INFO                                = 366;
	const SUITE_NOTES                               = 367;
	const SPECIAL_REMOVED                           = 369;
	const ESIGN_NOTIFICATION_SENT                   = 379;
	const ESIGN_COMPLETED                           = 380;

	const JOB_UPDATED								= 370;
	const CONTRACT_UPDATED							= 392;

	const COMMERCIAL_CLAUSE							= 371;
	const COMMERCIAL_SUITE							= 372;
	const COMMERCIAL_INSURANCE_POLICY				= 373;

	const UNIT_WEB_VISIBILITY                       = 374;
	const UNIT_EXCLUSION_REASON_CHANGE              = 377;
	const ALL_REQUIRED_CHECKLIST_ITEMS_COMPLETED    = 378;
	const REQUIRED_CHECKLIST_ITEMS_REVERSED         = 408;

	const LEASE_PACKET_GENERATED                    = 381;

	const GCIC_CONSENT_FORM_GENERATED               = 385;
	const GCIC_CONSENT_FORM_ESIGNED                 = 386;
	const GCIC_CONSENT_FORM_EMAIL_SENT              = 387;

	const RECERTIFICATION_30_DAYS_NOTICE  			= 388;
	const CONTACT_FORM_SUBMITTED					= 395;

	const REFERRER_ADDED_EDITED                     = 396;
	const REFERRER_PORTAL_ACCESS                    = 397;
	const CONTACT_ADDED_EDITED                      = 398;
	const PRIMARY_CONTACT_CHANGED                   = 399;
	const REFERRER_STAGE_STATUS_INTERACT            = 400;
	const SKIP_SCREENING_TRANSACTION                = 401;
	const DOCUMENT_UPLOADED                         = 402;
	const DOCUMENT_REUPLOADED                       = 415;
	const DOCUMENT_VERIFIED                         = 467;
	const DOCUMENT_REJECTED                         = 468;
	// Commercial Lease
	const COMMERCIAL_LEASE                          = 389;
	// Reimbursable Expense Pools
	const REIMBURSABLE_EXPENSE_POOLS				= 404;

	// NNN Reconciliation
	const NNN_RECONCILIATION						= 403;

	const REFERRED_INCOMING_CALL                    = 405;

	const GROUP_LEASING_CONTRACT_UPDATE				= 406;
	const GROUP_LEASING_CONTRACT_UNIT_CHANGE		= 407;

	const UTILITY_MOVE_OUT 							= 409;
	const SCHEDULED_CHARGE_APPROVAL_ROUTING			= 410;
    const RENTAL_HISTORY_COLLECTION_DOCUMENT_UPLOADED = 411;
    const DO_NOT_RENT                               = 412;
	const UNIT_OCCUPANCY_TYPE_CHANGE                = 413;

	const MERGED                                    = 414;

	const SCREENING_ID_VERIFICATION_PROCESS        = 416;
	const INDIVIDUAL_TRANSFER						= 417;

	const GROUP_CREATED                             = 418;
	const GROUP_ARCHIVED                            = 419;
	const GROUP_ENABLED                             = 420;
	const CUSTOMER_INVOICE_GENERATED                = 428;
	const CUSTOMER_INVOICE_DELETION                 = 481;
	const DOCUMENT_EXPIRATION                       = 429;

	const JOURNAL_ENTRY_TEMPLATE_CREATED			= 421;
	const JOURNAL_ENTRY_TEMPLATE_EDITED				= 422;
	const JOURNAL_ENTRY_TEMPLATE_DISABLED			= 423;
    const JOURNAL_ENTRY_TEMPLATE_ENABLED			= 499;
	const JOURNAL_ENTRY_TEMPLATE_DELETED			= 424;
	const JOURNAL_ENTRY_TEMPLATE_APPROVED			= 425;
	const JOURNAL_ENTRY_TEMPLATE_ATTACHMENT_ADDED	= 426;
	const JOURNAL_ENTRY_TEMPLATE_ATTACHMENT_REMOVED	= 427;

	const APPLICATION_POLICY_DOCUMENT_GENERATED     = 430;

	const REFERRER_ARCHIVED                         = 432;
	const REFERRER_REOPENED                         = 433;
	const DEPOSIT_SERVICE_SELECTED                  = 434;
	const DEPOSIT_SERVICE_REMOVED                   = 435;

	const CHECKLIST_ITEM_ACTIONS                    = 431;

	const EXTENSION_LEASE_PROGRESS					= 438;

	const HAZARD_NOTES                              = 436;

	// Commercial Cam Pool and Cam Share
	const CAM_POOL                                  = 439;
	const CAM_SHARE                                 = 440;
	const MID_LEASE_MODIFICATION_APPLICATION        = 441;
	const DATA_PRIVACY_REQUEST                      = 443;

	const PHONE_CALLS_OPTIN							= 444;
	const PHONE_CALLS_OPTOUT						= 445;
	const POSTAL_MAIL_OPTIN							= 446;
	const POSTAL_MAIL_OPTOUT						= 447;

	// Request Renewal offer
	const RENEWAL_REQUESTED							= 448;

	const SELF_GUIDED_TOUR                          = 442;
	const VIRTUAL_TOUR                              = 449;

	const APPROVAL_ROUTING							= 450;
	const TENANT_PORTAL_LOG_IN                      = 452;
	const RESERVATIONHUB_LOG_IN                     = 453;
	const TENANT_PORTAL_LOG_IN_BY_MANAGER           = 454;
	const RESERVATIONHUB_LOG_IN_BY_MANAGER          = 455;
	const CUSTOMER_COMMERCIAL_MEASURE               = 460;

	const OFFSITE									= 456;

	// Tenant Renewal Options
	const QUOTE                                     = 451;
	const RENEWAL_OPTION_OPEN						= 461;

	// Commercial Contact Points
	const COMMERCIAL_UNINSURED_TENANTS          = 458;
	const RENT_ESCALATIONS                      = 459;
	const EXPIRING_INSURANCE_AGENT_NOTIFICATION = 462;
	const EXPIRED_INSURANCE_AGENT_NOTIFICATION  = 463;
	const ESIGN_LEASE_SIGN_REFUSED              = 464;

	const UNANSWERED_QUESTION                   = 466;

	const REFERRED_CONTACT              = 472;
	const TIER_SCREENING                = 473;
    const RV_NOT_PROGRESSING                    = 475;
	const PRECISE_ID_ABANDONED                    = 476;
	const PROCESS_SCREENING_VERIFICATION_OF_INCOME = 477;
	const LEASING_CENTER_OUTBOUND_CALL    = 479;
	const EXTERNAL = 478;
	const INVOICE = 482;
	const PURCHASE_ORDER = 483;

	const REASONABLE_ACCOMMODATION = 495;

	// Services
	const SERVICES = 484;
	const ACCESSED_LINK_SCREENING_VERIFICATION_OF_INCOME = 486;
	const COMPLETED_SCREENING_VERIFICATION_OF_INCOME = 487;
	const SCREENING_CONDITIONS_RESET = 488;
	const TENANT_VISIT						  = 492;
	const VOI_ABANDONED              = 493;
	const STARTED_SCREENING_VERIFICATION_OF_INCOME = 500;
	const REINITIATE_VERIFICATION_OF_INCOME = 501;
	const VOI_NO_BANK_PROVIDED = 503;

	// Wisconsin, California response for consumer report
	const CONSUMER_REPORT_WI_CA                 = 490;

	const ESIGN_LEASE_PARTIALLY_COMPLETED   = 496;
	const ESIGN_LEASE_COMPLETED             = 497;
	const SCREENING_EMAIL_SEND_VALIDATION_ACTIVITY = 502;

	//BUA Resident notes
	const BUA_RESIDENT_NOTE                 = 504;
	const LEASE_EXTENSION_REQUESTED         = 505;

	const DESCRIPTION_AP_CLOSED						= 'AP Closed';
	const DESCRIPTION_AR_CLOSED						= 'AR Closed';
	const DESCRIPTION_GL_CLOSED						= 'GL Closed';
	const DESCRIPTION_AP_REOPENED					= 'AP Reopened';
	const DESCRIPTION_AR_REOPENED					= 'AR Reopened';
	const DESCRIPTION_GL_REOPENED					= 'GL Reopened';

	public static $c_arrstrVendorEventTypeNames = [
		self::VENDOR_NOTES => 'Add Note',
		self::VENDOR_OUTGOING_CALL => 'Enter Outgoing Call',
		self::VENDOR_INCOMING_CALL => 'Enter Incoming Call',
		self::VENDOR_INCOMING_EMAIL => 'Enter Incoming Email'
	];

	public static $c_arrstrVendorEventTypeClasses = [
		self::VENDOR_NOTES => 'note',
		self::VENDOR_OUTGOING_CALL => 'call-outgoing',
		self::VENDOR_INCOMING_CALL => 'call-incoming',
		self::VENDOR_INCOMING_EMAIL => 'email-incoming'
	];

	public static $c_arrintVendorEventTypeIds = [
		'VENDOR_NOTES'          => self::VENDOR_NOTES,
		'VENDOR_OUTGOING_CALL'  => self::VENDOR_OUTGOING_CALL,
		'VENDOR_INCOMING_CALL'  => self::VENDOR_INCOMING_CALL,
		'VENDOR_INCOMING_EMAIL' => self::VENDOR_INCOMING_EMAIL
	];

	public static $c_arrintMeaningfulEventTypeIds = [
		self::EMAIL_INCOMING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::ONLINE_CHAT,
		self::SMS_INCOMING,
		self::SMS_OUTGOING,
		self::ONSITE_VISIT,
		self::VOICE_MAIL,
		self::CALL_CENTER

	];

	public static $c_arrintCombineLeadEventTypeIds = [
		self::EMAIL_INCOMING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::ONLINE_CHAT,
		self::SMS_INCOMING,
		self::SMS_OUTGOING,
		self::ONSITE_VISIT,
		self::VOICE_MAIL,
		self::CALL_CENTER,
		self::NOTES,
		self::INVITATION_EMAIL

	];

	public static $c_arrintFirstContactEventTypeIds = [
		self::CALL_INCOMING,
		self::EMAIL_INCOMING,
		self::ONLINE_CHAT,
		self::ONSITE_VISIT,
		self::ONLINE_GUEST_CARD,
		self::SMS_INCOMING,
		self::VOICE_MAIL
	];

	public static $c_arrintContactEventTypeIds = [
		self::TOUR,
		self::RENEWAL_OFFER_SENT,
		self::EMAIL_INCOMING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::SMS_INCOMING,
		self::SMS_OUTGOING,
		self::ONLINE_CHAT,
		self::ONSITE_VISIT,
		self::VOICE_MAIL,
		self::INVITATION_EMAIL,
		self::MARKED_AS_CONTACTED,
		self::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR,
		self::OFFSITE
	];

	public static $c_arrintSuccessfulContactEventTypeIds = [
		self::TOUR,
		self::EMAIL_INCOMING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::SMS_INCOMING,
		self::ONLINE_CHAT,
		self::ONSITE_VISIT,
		self::VOICE_MAIL,
		self::MARKED_AS_CONTACTED
	];

	public static $c_arrintResidentSuccessfulContactEventTypeIds = [
		self::EMAIL_INCOMING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::SMS_INCOMING,
		self::SMS_OUTGOING,
		self::SCHEDULED_FOLLOWUP,
		self::SCHEDULED_APPOINTMENT,
		self::RESIDENT_VISIT
	];

	public static $c_arrintResidentDefaultSuccessfulContactEventTypeIds = [
		self::EMAIL_INCOMING,
		self::SCHEDULED_APPOINTMENT,
		self::RESIDENT_VISIT
	];

	public static $c_arrintUnSuccessfulContactEventTypeIds = [
		self::EMAIL_OUTGOING,
		self::CALL_OUTGOING,
		self::SMS_OUTGOING,
		self::NOTES
	];

	public static $c_arrintSuccessfulUnSuccessfulContactEventTypeIds = [
		self::TOUR,
		self::SCHEDULED_APPOINTMENT,
		self::ONSITE_VISIT,
		self::SCHEDULED_FOLLOWUP,
		self::ONLINE_GUEST_CARD,
		self::GUEST_CARD_BACK_TRACK,
		self::DUPLICATE_LEAD,
		self::SMS_INCOMING,
		self::EMAIL_INCOMING,
		self::INVITATION_EMAIL,
		self::SMS_OUTGOING,
		self::ONLINE_CHAT,
		self::CALL_OUTGOING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::VOICE_MAIL
	];

	public static $c_arrintSuccessfulResidentAttemptContactEventTypeIds = [
		self::TOUR,
		self::SCHEDULED_APPOINTMENT,
		self::ONSITE_VISIT,
		self::SCHEDULED_FOLLOWUP,
		self::ONLINE_GUEST_CARD,
		self::GUEST_CARD_BACK_TRACK,
		self::DUPLICATE_LEAD,
		self::SMS_INCOMING,
		self::EMAIL_INCOMING,
		self::INVITATION_EMAIL,
		self::SMS_OUTGOING,
		self::ONLINE_CHAT,
		self::CALL_OUTGOING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::VOICE_MAIL,
		self::RESIDENT_VISIT,
		self::RENEWAL_OFFER_SENT
	];

	public static $c_arrintOnsiteVisitAndTourEventTypeIds = [
		self::ONSITE_VISIT,
		self::TOUR,
		self::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR
	];

	public static $c_arrintTourEventTypeIds = [
		self::TOUR,
		self::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR
	];

	public static $c_arrintAddLeadEventTypeIds = [
		self::CALL_INCOMING,
		self::EMAIL_INCOMING,
		self::SMS_INCOMING,
		self::ONLINE_CHAT,
		self::ONSITE_VISIT,
		self::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR,
		self::OFFSITE
	];

	public static $c_arrintCustomerTerminatedEventsIds = [ self::CLOSED, self::CANCELED ];

	public static $c_arrintDashboardEventTypeIds = [
		self::VOICE_MAIL,
		self::EMAIL_INCOMING,
		self::SMS_INCOMING,
		self::CALL_INCOMING
	];

	public static $c_arrintLeadActivityEventTypeIds = [
		self::EMAIL_INCOMING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::SMS_INCOMING,
		self::SMS_OUTGOING,
		self::ONLINE_CHAT,
		self::NOTES,
		self::ONSITE_VISIT,
		self::ONLINE_GUEST_CARD,
		self::PRE_SCREENING,
		self::APPLICATION_PROGRESS,
		self::LEASE_PROGRESS,
		self::RENEWAL_PROGRESS,
		self::EXTENSION_LEASE_PROGRESS,
		self::SCHEDULED_FOLLOWUP,
		self::VOICE_MAIL,
		self::SCHEDULED_APPOINTMENT,
		self::DUPLICATE_LEAD,
		self::RENEWAL_OFFER_SENT,
		self::CLOSED,
		self::FINANCIAL_MOVE_OUT_STARTED,
		self::FINANCIAL_MOVE_OUT_APPROVED,
		self::FINANCIAL_MOVE_OUT_REJECTED,
		self::REVISE_FINANCIAL_MOVE_OUT,
		self::FINANCIAL_MOVE_OUT,
		self::REVERSE_FINANCIAL_MOVE_OUT,
		self::REGENERATE_FMO_STATEMENT,
		self::OTHER,
		self::INVITATION_EMAIL,
		self::DENIED,
		self::PROPERTY_CHANGE,
		self::SMS_OPT_IN,
		self::SMS_OPT_OUT,
		self::EMAIL_OPT_IN,
		self::EMAIL_OPT_OUT,
		self::PHONE_CALLS_OPTIN,
		self::PHONE_CALLS_OPTOUT,
		self::POSTAL_MAIL_OPTIN,
		self::POSTAL_MAIL_OPTOUT,
		self::SCREENING_DECISION,
		self::SCREENING_DECISION_RESET,
        self::SCREENING_RECOMMENDATION_RECALCULATED,
		self::ADVERSE_ACTION_LETTER_GENERATED,
		self::COMBINED,
		self::MERGED,
		self::ADD_ON,
		self::MOVE_IN,
		self::CLONED,
		self::DEMO_SCREENING_PROCESSED,
		self::PAYMENT_RECEIPT,
		self::TOUR,
		self::CALL_CENTER,
		self::CANCEL_LEASE,
		self::RENEWAL_OFFER_REVIEWED,
		self::PRINT_APPLICATION,
		self::DOWNLOAD_APPLICATION_DETAILS,
		self::DOWNLOAD_APPLICATION_DOCUMENT,
		self::SIGNED_LEASE_UPLOADED,
		self::LEASE_END,
		self::SD_GROUP_DISSOCIATION,
		self::APPLICANT_GROUPING,
		self::ROOMMATE_NOTIFICATION_SENT,
		self::PROSPECT_LOGIN_BY_MANAGER,
		self::GENERATE_WAITLIST_OFFER,
		self::WAITLIST_APPLICATION_REMOVED_BY_APPLICANT,
		self::WAITLIST_APPLICATION_REMOVED_BY_PROPERTY,
		self::APPLICANT_DECLINED_WAITLIST_OFFER,
		self::APPLICANT_ACCEPTED_WAITLIST_OFFER,
		self::OFFER_EXPIRED,
		self::OFFER_SKIPPED,
		self::SD_BULK_UNIT_ASSIGNMENT,
		self::UNIT_UNAVAILABLE,
		self::RENEWAL_OFFER_EDITED,
		self::DOCUMENT_DELETED,
		self::RENEWAL_END_DATE_CHANGED,
		self::ADD_APPLICANT,
		self::DELETE_APPLICANT,
		self::REACTIVATE_APPLICANT,
        self::PRIMARY_APPLICANT_CHANGED,
        self::SKIP_GUARANTOR_LEASE,
		self::APPLICATION_UPDATED,
		self::SCREENING_STATUS,
		self::MUTE_FOLLOW_UPS,
		self::UNMUTE_FOLLOW_UPS,
		self::QUOTE_APPLIED,
		self::SIGNING_INVITATION_EMAIL,
		self::SD_RESET_UNIT_ASSIGNMENT,
		self::LEASE_COUNTERSIGN,
		self::SCREENING_PROCESSED,
		self::PARENTAL_CONSENT_FORM_EMAIL_SENT,
		self::PARENTAL_CONSENT_FORM_SIGNED,
		self::MARKED_AS_CONTACTED,
		self::INSTALLMENT_CHANGE,
	    self::SCREENING_CONDITIONS,
	    self::GUEST_CARD_BACK_TRACK,
		self::SCHEDULE_CHARGE_INFO,
		self::TRANSFER_QUOTE_RESENT,
		self::TRANSFER_QUOTE_DECLINED,
		self::ESIGN_DOCUMENT_INITIATED,
		self::ESIGN_DOCUMENT_SIGNED,
		self::QUOTE_GENERATED,
		self::SPECIAL_QUALIFIED,
		self::UNIT_GENDER_CHANGE,
		self::PAYMENT_SUBMITTED,
		self::APPLICANT_GROUPING,
		self::OVERRIDE_RENT_LIMIT,
		self::INCOME_CHANGE,
		self::ASSET_CHANGE,
		self::EXPENSE_CHANGE,
		self::SPECIAL_REMOVED,
		self::MTM_OFFER_ACCEPTED,
		self::SCREENING_VALIDATION,
		self::SCREENING_STATUS,
		self::ESIGN_NOTIFICATION_SENT,
		self::ESIGN_COMPLETED,
		self::GCIC_CONSENT_FORM_GENERATED,
		self::GCIC_CONSENT_FORM_ESIGNED,
		self::GCIC_CONSENT_FORM_EMAIL_SENT,
		self::LEASE_PACKET_GENERATED,
		self::SKIP_SCREENING_TRANSACTION,
		self::CONTACT_FORM_SUBMITTED,
		self::UNIT_TRANSFER,
		self::REFERRED_INCOMING_CALL,
		self::SCHEDULED_CHARGE_APPROVAL_ROUTING,
		self::RENTAL_HISTORY_COLLECTION_DOCUMENT_UPLOADED,
        self::DO_NOT_RENT,
		self::SCREENING_ID_VERIFICATION_PROCESS,
		self::CUSTOMER_INVOICE_GENERATED,
		self::CUSTOMER_INVOICE_DELETION,
		self::APPLICATION_POLICY_DOCUMENT_GENERATED,
		self::DEPOSIT_SERVICE_SELECTED,
		self::DEPOSIT_SERVICE_REMOVED,
		self::MID_LEASE_MODIFICATION_APPLICATION,
		self::DATA_PRIVACY_REQUEST,
		self::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR,
		self::OFFSITE,
		self::DOCUMENT_VERIFIED,
		self::DOCUMENT_REJECTED,
		self::DOCUMENT_UPLOADED,
		self::ESIGN_LEASE_SIGN_REFUSED,
		self::UNANSWERED_QUESTION,
		self::TIER_SCREENING,
		self::PRECISE_ID_ABANDONED,
		self::REFERRED_CONTACT,
		self::PROCESS_SCREENING_VERIFICATION_OF_INCOME,
		self::ACCESSED_LINK_SCREENING_VERIFICATION_OF_INCOME,
		self::COMPLETED_SCREENING_VERIFICATION_OF_INCOME,
		self::SCREENING_CONDITIONS_RESET,
        self::CONSUMER_REPORT_WI_CA,
		self::VOI_ABANDONED,
        self::ESIGN_LEASE_PARTIALLY_COMPLETED,
        self::ESIGN_LEASE_COMPLETED,
		self::STARTED_SCREENING_VERIFICATION_OF_INCOME,
		self::REINITIATE_VERIFICATION_OF_INCOME,
		self::SCREENING_EMAIL_SEND_VALIDATION_ACTIVITY,
		self::VOI_NO_BANK_PROVIDED,
		self::BUA_RESIDENT_NOTE
	];

	public static $c_arrintEventsToBeExported = [
		self::EMAIL_INCOMING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::SMS_INCOMING,
		self::SMS_OUTGOING,
		self::ONLINE_CHAT,
		self::NOTES,
		self::ONSITE_VISIT,
		self::ONLINE_GUEST_CARD,
		self::APPLICATION_PROGRESS,
		self::SCHEDULED_APPOINTMENT,
		self::DUPLICATE_LEAD,
		self::CLOSED,
		self::OTHER,
		self::INVITATION_EMAIL,
		self::DENIED,
		self::CANCELED,
		self::TOUR,
		self::CALL_CENTER,
		self::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR
	];

	public static $c_arrintLeaseEventTypeIds = [
		self::SIGNED_LEASE_UPLOADED,
		self::LEASE_COUNTERSIGN,
		self::LEASE_START,
		self::LEASE_INFO,
		self::LEASE_PROGRESS,
		self::EXTENSION_LEASE_PROGRESS,
		self::LEASE_RENEWAL,
		self::LEASE_RENEWED,
		self::LEASE_END,
	    self::LATE_PAYMENT_COUNT_UPDATED,
	    self::RETURNED_PAYMENT_COUNT_UPDATED,
		self::LEASE_EXTENSION_REQUESTED,
	];

	public static $c_arrintMoneyIconNeededEventTypeIds = [
		self::RECURRING_PAYMENT_ADDED,
		self::RECURRING_PAYMENT_UPDATED,
		self::RECURRING_PAYMENT_DELETED,
		self::PAYMENT_SUBMITTED,
		self::STORED_BILLING_ACCOUNT_ADDED,
		self::STORED_BILLING_ACCOUNT_UPDATED,
		self::STORED_BILLING_ACCOUNT_DELETED
	];

	public static $c_arrintRenewalEventTypeIds = [
		self::RENEWAL_PROGRESS,
		self::RENEWAL_OFFER_SENT,
		self::RENEWAL_OFFER_REVIEWED,
		self::RENEWAL_OFFER_EDITED,
		self::RENEWAL_END_DATE_CHANGED,
		self::NON_RENEWAL_NOTICE,
		self::RENEWAL_EVENT_TRIGGER_FOR_INSPECTION,
		self::LEASE_RENEWAL
	];

	public static $c_arrintNotesEventTypesIds = [
		self::NOTES,
		self::DELINQUENCY_NOTES,
		self::NSF_NOTES,
		self::REPAYMENT_AGREEMENT_NOTES,
		self::OVERRIDE_RENT_LIMIT,
		self::INCOME_CHANGE,
		self::ASSET_CHANGE,
		self::EXPENSE_CHANGE
	];

	public static $c_arrintOptInOptOutEventTypesIds = [
		self::EMAIL_OPT_IN,
		self::EMAIL_OPT_OUT,
		self::SMS_OPT_IN,
		self::SMS_OPT_OUT
	];

	public static $c_arrintResidentActivityEventTypeIds = [
		self::NOTES,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::EMAIL_INCOMING,
		self::EMAIL_OUTGOING,
		self::SMS_INCOMING,
		self::SMS_OUTGOING,
		self::RESIDENT_LOG_IN_BY_MANAGER,
		self::RESIDENT_LOG_IN,
		self::STATUS_CHANGE,
		self::ONLINE_CHAT,
		self::ONSITE_VISIT,
		self::ONLINE_GUEST_CARD,
		self::PRE_SCREENING,
		self::APPLICATION_PROGRESS,
		self::LEASE_PROGRESS,
		self::RENEWAL_PROGRESS,
		self::CLOSED,
		self::OTHER,
		self::SCHEDULED_FOLLOWUP,
		self::VOICE_MAIL,
		self::SCHEDULED_APPOINTMENT,
		self::INVITATION_EMAIL,
		self::DUPLICATE_LEAD,
		self::FINANCIAL_MOVE_OUT_STARTED,
		self::FINANCIAL_MOVE_OUT_APPROVED,
		self::FINANCIAL_MOVE_OUT_REJECTED,
		self::FINANCIAL_MOVE_OUT,
		self::REVERSE_FINANCIAL_MOVE_OUT,
		self::REGENERATE_FMO_STATEMENT,
		self::SMS_OPT_IN,
		self::SMS_OPT_OUT,
		self::EMAIL_OPT_IN,
		self::EMAIL_OPT_OUT,
		self::PHONE_CALLS_OPTIN,
		self::PHONE_CALLS_OPTOUT,
		self::POSTAL_MAIL_OPTIN,
		self::POSTAL_MAIL_OPTOUT,
		self::LEASE_INFO,
		self::PAYMENT_STATUS_CHANGE,
		self::SCHEDULE_CHARGE_INFO,
		self::LEDGER_INFO,
		self::UNIT_TRANSFER,
		self::EVICTED,
		self::SKIPPED,
		self::FIRST_NOTICE,
		self::REPEAT_NOTICE,
		self::FINAL_NOTICE,
		self::DEMAND_NOTICE,
		self::LATE_NOTICE_RESET,
		self::LEASING_CENTER_WELCOME_CALL,
		self::COMBINED,
		self::MERGED,
		self::DELINQUENCY_NOTES,
		self::LEASE_SENT_TO_COLLECTIONS,
		self::LEASE_REMOVED_FROM_COLLECTIONS,
		self::ADD_ON,
		self::LATE_FEE,
		self::UNIT_CHANGE,
		self::CONTACT_SUBMISSION,
		self::RENT_REMINDER,
		self::RECURRING_PAYMENT,
		self::MOVE_IN_REVIEW_COMPLETED,
		self::REMOVE_OCCUPANT,
		self::FIRST_PRE_COLLECTION_LETTER_SENT,
		self::SECOND_PRE_COLLECTION_LETTER_SENT,
		self::FINAL_PRE_COLLECTION_LETTER_SENT,
		self::REVERSE_MOVE_IN,
		self::CANCEL_LEASE,
		self::MOVE_IN_ADJUSTMENT,
		self::LEASE_EXCLUDED_FROM_COLLECTIONS,
		self::EXCLUSION_FROM_COLLECTIONS_REMOVED,
		self::TOUR,
		self::RENEWAL_OFFER_SENT,
		self::SIGNED_LEASE_UPLOADED,
		self::RENEWAL_OFFER_REVIEWED,
		self::REVERSE_MOVE_IN_CHECKLIST,
		self::DEMAND_FOR_POSSESSION,
		self::DOCUMENT_DELETED,
		self::RENEWAL_OFFER_EDITED,
		self::RENEWAL_END_DATE_CHANGED,
		self::WORK_ORDER_UPDATED,
		self::SKIP_GUARANTOR_LEASE,
		self::RESIDENT_USERNAME_PASSWORD_CHANGE,
		self::TERMINATION_STARTED,
		self::TERMINATION_CANCELLED,
		self::MOVE_OUT_ADJUSTMENT,
		self::SIGNING_INVITATION_EMAIL,
		self::REVISE_FINANCIAL_MOVE_OUT,
		self::SD_RESET_UNIT_ASSIGNMENT,
		self::ALARM_KEY_CHANGE,
		self::NON_RENEWAL_NOTICE,
		self::CUSTOMER_TYPE_CHANGE,
		self::MUTE_FOLLOW_UPS,
		self::RESIDENT_VISIT,
		self::NSF_NOTES,
		self::NSF_NOTICE,
		self::CHECKLIST_ITEM_COMPLETED,
		self::CHECKLIST_ITEM_REVERSED,
		self::LEASE_COUNTERSIGN,
		self::SPLIT_LEASE,
		self::REPAYMENT_AGREEMENT_CREATED,
		self::REPAYMENT_AGREEMENT_CANCELLED,
		self::REPAYMENT_AGREEMENT_LATE_NOTICE,
		self::REPAYMENT_AGREEMENT_NOTES,
		self::ADD_UPDATE_INSURANCE_POLICY,
		self::RESIDENT_LOG_IN_FROM_APP,
		self::RESIDENT_LOG_IN_FROM_WHITE_LABELED_APP,
		self::MID_LEASE_CUSTOMER_RELATIONSHIP_CHANGE,
		self::MID_LEASE_CANCEL_MOVE_IN,
		self::MID_LEASE_MOVE_IN,
		self::MID_LEASE_CANCEL_NOTICE,
		self::MID_LEASE_MOVE_OUT,
		self::INSTALLMENT_CHANGE,
		self::LEASE_START,
		self::MOVE_IN,
		self::MOVE_OUT,
		self::LEASE_RENEWAL,
		self::LEASE_END,
		self::RESIDENT_BIRTHDAY,
		self::RENEWAL_PROGRESS,
		self::DOCUMENT_EXPIRATION,
	    self::LATE_PAYMENT_COUNT_UPDATED,
	    self::RETURNED_PAYMENT_COUNT_UPDATED,
		self::GUEST_ADDED_IN_ENTRATA,
		self::GUEST_ADDED_IN_RESIDENT_PORTAL,
		self::GUEST_UNIT_ACCESS_IN_ENTRATA,
		self::GUEST_UNIT_ACCESS_IN_RESIDENT_PORTAL,
		self::UPDATE_SPACE_CONFIGURATION,
		self::LIST_TYPE_CHANGE,
		self::ISSUE_REFUND,
	    self::ADD_PET,
	    self::DELETE_PET,
	    self::REASSOCIATE_PET,
	    self::ADD_VEHICLE,
	    self::DELETE_VEHICLE,
		self::REASSOCIATE_VEHICLE,
	    self::MERGE_LEASE,
	    self::UNMERGE_LEASE,
	    self::STATEMENT_EMAILED,
		self::REASSOCIATE_PACKAGE,
		self::WAIVE_LATE_FEE_CHANGE,
		self::RECERTIFICATION_120_DAYS_NOTICE,
		self::RECERTIFICATION_90_DAYS_NOTICE,
		self::RECERTIFICATION_60_DAYS_NOTICE,
		self::RECERTIFICATION_30_DAYS_NOTICE,
	    self::ACCELERATED_RENT_ADJUSTMENT,
		self::INSPECTION_UPDATED,
		self::TRANSFER_QUOTE_RESENT,
		self::TRANSFER_QUOTE_DECLINED,
	    self::DELINQUENCY_POLICY_CHANGE,
		self::DELINQUENCY_NOTICE,
		self::COLLECTION_POLICY_CHANGE,
		self::COLLECTIONS_NOTICE,
		self::WRITE_OFF_BALANCE,
		self::TRANSFER_QUOTES,
		self::GUEST_PARCEL_PICKUP_PERMITTED_IN_RESIDENT_PORTAL,
		self::GUEST_PARCEL_PICKUP_PERMITTED_IN_ENTRATA,
		self::ESIGN_DOCUMENT_INITIATED,
		self::ESIGN_DOCUMENT_SIGNED,
		self::MONEYGRAM_INSTRUCTION_EMAIL,
		self::STORED_BILLING_ACCOUNT_ADDED,
		self::STORED_BILLING_ACCOUNT_UPDATED,
		self::STORED_BILLING_ACCOUNT_DELETED,
		self::CORPORATE_CARE_REQUEST_ADDED,
		self::CORPORATE_CARE_REQUEST_ASSIGNED,
		self::CORPORATE_CARE_REQUEST_REASSIGNED,
		self::CORPORATE_CARE_REQUEST_DISMISSED,
		self::LEASE_RETRIEVED_BY_COLLECTIONS_AGENCY,
		self::RECURRING_PAYMENT_ADDED,
		self::RECURRING_PAYMENT_UPDATED,
		self::RECURRING_PAYMENT_DELETED,
		self::CONTACT_SUBMISSION_CALL_OUTGOING,
		self::CONTACT_SUBMISSION_RESIDENT_VISIT,
		self::SUBSIDY_CERTIFICATION_UPDATED,
		self::SPECIAL_QUALIFIED,
		self::OVERRIDE_RENT_LIMIT,
		self::INCOME_CHANGE,
		self::ASSET_CHANGE,
		self::EXPENSE_CHANGE,
        self::EARLY_TERMINATION,
		self::MTM_OFFER_ACCEPTED,
		self::COMMERCIAL_CLAUSE,
		self::COMMERCIAL_INSURANCE_POLICY,
		self::ALL_REQUIRED_CHECKLIST_ITEMS_COMPLETED,
		self::REQUIRED_CHECKLIST_ITEMS_REVERSED,
		self::VIOLATION_MANAGER,
		self::LEASE_PACKET_GENERATED,
		self::DOCUMENT_UPLOADED,
		self::COMMERCIAL_LEASE,
		self::UTILITY_MOVE_OUT,
		self::SCHEDULED_CHARGE_APPROVAL_ROUTING,
        self::DO_NOT_RENT,
		self::REIMBURSABLE_EXPENSE_POOLS,
		self::SCREENING_ID_VERIFICATION_PROCESS,
		self::NNN_RECONCILIATION,
		self::CUSTOMER_INVOICE_GENERATED,
		self::CUSTOMER_INVOICE_DELETION,
		self::REASONABLE_ACCOMMODATION,
		self::SPECIAL_REMOVED,
		self::INDIVIDUAL_TRANSFER,
		self::DEPOSIT_SERVICE_SELECTED,
		self::COMMERCIAL_SUITE,
		self::EXTENSION_LEASE_PROGRESS,
		self::DATA_PRIVACY_REQUEST,
		self::RENEWAL_REQUESTED,
		self::TENANT_PORTAL_LOG_IN,
		self::QUOTE,
		self::TENANT_PORTAL_LOG_IN_BY_MANAGER,
		self::CUSTOMER_COMMERCIAL_MEASURE,
		self::DOCUMENT_VERIFIED,
		self::DOCUMENT_REJECTED,
		self::ESIGN_LEASE_SIGN_REFUSED,
		self::TIER_SCREENING,
		self::LEASING_CENTER_OUTBOUND_CALL,
		self::PRIMARY_RESIDENT_CORRECTION,
		self::TENANT_VISIT,
		self::VOI_ABANDONED,
        self::ESIGN_LEASE_PARTIALLY_COMPLETED,
        self::ESIGN_LEASE_COMPLETED,
		self::BUA_RESIDENT_NOTE,
		self::LEASE_EXTENSION_REQUESTED,
	];

	public static $c_arrintEventSchedulerEventTypeIds = [
		self::LEASE_START,
		self::LEASE_END,
		self::MOVE_IN,
		self::MOVE_OUT,
		self::LEASE_RENEWAL,
		self::RESIDENT_BIRTHDAY,
		self::RENEWAL_PROGRESS,
		self::SCHEDULED_FOLLOWUP,
		self::DOCUMENT_EXPIRATION
	];

	public static $c_arrintGroupActivityEventTypeIds = [
		self::NOTES,
		self::DELINQUENCY_NOTES,
		self::PAYMENT_SUBMITTED,
		self::GROUP_CREATED,
		self::GROUP_ARCHIVED,
		self::GROUP_ENABLED,
		self::EMAIL_OUTGOING
	];

	public static $c_arrintGroupContractsActivityEventTypeIds = [
		self::NOTES,
		self::DELINQUENCY_NOTES,
		self::GROUP_LEASING_CONTRACT_UPDATE,
		self::GROUP_LEASING_CONTRACT_UNIT_CHANGE,
		self::DOCUMENT_UPLOADED,
		self::DOCUMENT_REUPLOADED,
		self::DOCUMENT_DELETED,
		self::PAYMENT_SUBMITTED,
		self::EMAIL_OUTGOING,
		self::EMAIL_INCOMING,
		self::RESERVATIONHUB_LOG_IN,
		self::RESERVATIONHUB_LOG_IN_BY_MANAGER
	];

	public static $c_strScheduledTaskEventTypes = [
	 	self::MOVE_OUT 					=> 'Move-out',
		self::MOVE_IN 					=> 'Move-in',
		self::SCHEDULED_APPOINTMENT 	=> 'Appointment'
	];

	public static $c_arrstrDashboardEventTypeNames = [
		'EMAIL_INCOMING'					=> self::EMAIL_INCOMING,
		'EMAIL_OUTGOING'					=> self::EMAIL_OUTGOING,
		'INVITATION_EMAIL'					=> self::INVITATION_EMAIL,
		'CALL_INCOMING'						=> self::CALL_INCOMING,
		'CALL_OUTGOING'						=> self::CALL_OUTGOING,
		'SMS_INCOMING'						=> self::SMS_INCOMING,
		'SMS_OUTGOING'						=> self::SMS_OUTGOING,
		'ONLINE_CHAT'						=> self::ONLINE_CHAT,
		'ONSITE_VISIT'						=> self::ONSITE_VISIT,
		'TOUR'								=> self::TOUR,
		'VOICE_MAIL'						=> self::VOICE_MAIL,
		'SCHEDULED_FOLLOWUP'				=> self::SCHEDULED_FOLLOWUP,
		'NOTES'								=> self::NOTES,
		'ONLINE_GUEST_CARD'					=> self::ONLINE_GUEST_CARD,
		'GUEST_CARD_BACK_TRACK'				=> self::GUEST_CARD_BACK_TRACK,
		'DUPLICATE_LEAD'					=> self::DUPLICATE_LEAD,
		'SCHEDULED_APPOINTMENT'				=> self::SCHEDULED_APPOINTMENT
	];

	public static $c_arrstrEventTypeClasses = [
		self::EMAIL_INCOMING 			=> 'email-incoming',
		self::EMAIL_OUTGOING 			=> 'email-outgoing',
		self::INVITATION_EMAIL			=> 'email-outgoing',
		self::CALL_INCOMING 			=> 'call-incoming',
		self::CALL_OUTGOING 			=> 'call-outgoing',
		self::SMS_INCOMING 				=> 'sms-incoming',
		self::SMS_OUTGOING 				=> 'sms-outgoing',
		self::ONLINE_CHAT 				=> 'chat',
		self::ONSITE_VISIT				=> 'onsite',
		self::TOUR 						=> 'tour',
		self::VOICE_MAIL 				=> 'email-incoming',
		self::SCHEDULED_FOLLOWUP		=> 'scheduled-followup',
		self::NOTES						=> 'note',
		self::ONLINE_GUEST_CARD			=> 'guestcard',
		self::GUEST_CARD_BACK_TRACK 	=> 'guestcard',
		self::DUPLICATE_LEAD			=> 'guestcard',
		self::SCHEDULED_APPOINTMENT 	=> 'scheduled'
	];

	public static $c_arrintApplicationHistoryOutGoingEvents = [
		CEventType::ONSITE_VISIT,
		CEventType::TOUR,
		CEventType::EMAIL_OUTGOING,
		CEventType::CALL_OUTGOING,
		CEventType::SMS_OUTGOING
	];

	public static $c_arrintEvictionPacketEventTypeIds = [
		self::NOTES,
		self::STATUS_CHANGE,
		self::EVICTION_NOTICE,
		self::STANDARD_NOTICE,
		self::REVERSE_MOVE_OUT,
		self::EVICTED,
		self::LATE_NOTICE_RESET,
		self::DELINQUENCY_NOTES,
		self::MOVE_OUT,
		self::LATE_FEE,
		self::TOUR,
		self::DEMAND_FOR_POSSESSION,
		self::TERMINATION_STARTED,
		self::TERMINATION_CANCELLED,
		self::MOVE_OUT_ADJUSTMENT,
		self::NSF_NOTICE,
		self::NSF_NOTES,
		self::DELINQUENCY_NOTICE
	];

	public static $c_arrintStoredBillingEventTypeIds = [
		self::STORED_BILLING_ACCOUNT_ADDED,
		self::STORED_BILLING_ACCOUNT_UPDATED,
		self::STORED_BILLING_ACCOUNT_DELETED
	];

	public static $c_arrintRecurringPaymentEventTypeIds = [
		self::RECURRING_PAYMENT_ADDED,
		self::RECURRING_PAYMENT_UPDATED,
		self::RECURRING_PAYMENT_DELETED
	];

	public static $c_arrintEventTypes = [
		'month_period_open' => CEventType::MONTH_PERIOD_OPEN,
		'month_period_close' => CEventType::MONTH_PERIOD_CLOSE,
		'month_period_advanced' => CEventType::MONTH_PERIOD_ADVANCED,
		'month_period_rollback' => CEventType::MONTH_PERIOD_ROLLBACK,
	];

	public static $c_arrmixInspectionSchedulingEventTypeData = [
		'LEASE_PROGRESS'                       => CEventType::LEASE_PROGRESS,
		'MOVE_IN'                              => CEventType::MOVE_IN,
		'LEASE_END'                            => CEventType::LEASE_END,
		'MOVE_OUT'                             => CEventType::MOVE_OUT,
		'STANDARD_NOTICE'                      => CEventType::STANDARD_NOTICE,
		'EVICTION_NOTICE'                      => CEventType::EVICTION_NOTICE,
		'EVICTED'                              => CEventType::EVICTED,
		'RENEWAL_EVENT_TRIGGER_FOR_INSPECTION' => CEventType::RENEWAL_EVENT_TRIGGER_FOR_INSPECTION,
		'APPLICATION_APPROVED'                 => CEventType::APPLICATION_PROGRESS,
		'LEASE_START'						   => CEventType::LEASE_START,
		'LEASE_PARTIALLY_COMPLETED'            => CEventType::LEASE_PARTIALLY_COMPLETED,
		'LEASE_COMPLETED'                      => CEventType::LEASE_COMPLETED
	];

	public static $c_arrintActiveDashboardEventTypes = [
		self::SPECIAL_QUALIFIED
	];

	public static $c_arrintApiEventTypeIds = [
		self::NOTES,
		self::EMAIL_INCOMING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::SCHEDULED_APPOINTMENT,
		self::TOUR,
		self::DEPOSIT_SERVICE_REMOVED,
		self::DEPOSIT_SERVICE_SELECTED,
		self::SMS_OUTGOING,
		self::SMS_INCOMING,
		self::ONSITE_VISIT,
		self::SELF_GUIDED_TOUR,
		self::VIRTUAL_TOUR
	];

	public static $c_arrintDepositEventTypeIds = [
		self::DEPOSIT_SERVICE_REMOVED,
		self::DEPOSIT_SERVICE_SELECTED,
	];

	public static $c_arrintActivityLogEventTypeIds = [
		self::EMAIL_INCOMING,
		self::EMAIL_OUTGOING,
		self::CALL_INCOMING,
		self::CALL_OUTGOING,
		self::SMS_INCOMING,
		self::SMS_OUTGOING,
		self::NOTES,
		self::DELINQUENCY_NOTES
	];

	public static $c_arrstrEventTypeTagClasses = [
		'Communication' => 'chat-icon',
		'Document' => 'document-icon',
		'Financial' => 'cash-icon',
		'Maintenance' => 'maintenance-icon',
		'Notes' => 'note-check-icon',
		'Profile Activity' => 'people-icon',
		'Website Activity' => 'layout-icon',
		'BUA Resident Note' => 'note-check-icon',
		NULL => 'people-icon'
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getTrafficTypes() {
		if( isset( $this->m_arrmixTrafficEventTypes ) ) {
			return $this->m_arrmixTrafficEventTypes;
		}

		$this->m_arrmixTrafficEventTypes = [
			self::CALL_INCOMING         => __( 'Incoming Call' ),
			self::EMAIL_INCOMING        => __( 'Incoming Email' ),
			self::ONSITE_VISIT          => __( 'Onsite Visit' ),
			self::TOUR                  => __( 'Tour' ),
			self::SCHEDULED_APPOINTMENT => __( 'Appointment' )
		];

		return $this->m_arrmixTrafficEventTypes;
	}

	/**
	 * Function to get the css for icons based on event_type_id.
	 * @TO DO: Should I make this function private to prevent developers calling this function individually?
	 * @param unknown $intEventTypeId
	 * @return string
	 */

	public static function getEventTypeIconCssClass( $intEventTypeId ) {

		switch( $intEventTypeId ) {
			case self::INSPECTION_UPDATED:
			case self::WORK_ORDER_UPDATED:
				$strDefaultIconClass = 'maintenance-gray';
				break;

			case in_array( $intEventTypeId, self::$c_arrintRenewalEventTypeIds ):
				$strDefaultIconClass = 'renewal';
				break;

			case self::REVERSE_MOVE_IN:
			case self::REVERSE_FINANCIAL_MOVE_OUT:
			case in_array( $intEventTypeId, self::$c_arrintLeaseEventTypeIds ):
				$strDefaultIconClass = 'lease';
				break;

			case in_array( $intEventTypeId, self::$c_arrintNotesEventTypesIds ):
				$strDefaultIconClass = 'note';
				break;

			case self::APPLICATION_PARTIALLY_COMPLETE_EMAIL_SENT:
			case self::APPLICATION_PROGRESS:
			case self::APPLICATION_UPDATED:
			case self::CLOSED:
			case self::OTHER:
			case self::PRE_SCREENING:
			case self::SCREENING_PROCESSED:
			case self::SKIP_GUARANTOR_LEASE:
			case self::ADD_UPDATE_INSURANCE_POLICY:
			case self::ESIGN_DOCUMENT_INITIATED:
			case self::LEASE_PACKET_GENERATED:
			case self::DOCUMENT_UPLOADED:
			case self::DOCUMENT_REUPLOADED:
			case self::DEPOSIT_SERVICE_SELECTED:
			case self::DEPOSIT_SERVICE_REMOVED:
			case self::DOCUMENT_VERIFIED:
			case self::DOCUMENT_REJECTED:
				$strDefaultIconClass = 'application';
				break;

			case self::SCHEDULED_FOLLOWUP:
				$strDefaultIconClass = 'scheduled-followup';
				break;

			case self::SCHEDULED_APPOINTMENT:
				$strDefaultIconClass = 'scheduled';
				break;

			case self::RESIDENT_LOG_IN:
			case self::RESIDENT_LOG_IN_BY_MANAGER:
			case self::TENANT_PORTAL_LOG_IN:
			case self::RESERVATIONHUB_LOG_IN:
			case self::TENANT_PORTAL_LOG_IN_BY_MANAGER:
			case self::RESERVATIONHUB_LOG_IN_BY_MANAGER:
				$strDefaultIconClass = 'website';
				break;

			case self::ONSITE_VISIT:
			case self::RESIDENT_VISIT:
				$strDefaultIconClass = 'onsite';
				break;

			case self::EMAIL_OUTGOING:
			case self::SIGNING_INVITATION_EMAIL:
			case self::INVITATION_EMAIL:
			case self::PAYMENT_RECEIPT:
			case self::RECURRING_PAYMENT:
			case self::RENT_REMINDER:
			case self::MONEYGRAM_INSTRUCTION_EMAIL:
				$strDefaultIconClass = 'email-outgoing';
				break;

			case self::EMAIL_INCOMING:
				$strDefaultIconClass = 'email-incoming';
				break;

			case self::CALL_OUTGOING:
			case self::LEASING_CENTER_OUTBOUND_CALL:
				$strDefaultIconClass = 'call-outgoing';
				break;

			case self::CALL_INCOMING:
				$strDefaultIconClass = 'call-incoming';
				break;

			case self::SMS_OUTGOING:
				$strDefaultIconClass = 'sms-outgoing';
				break;

			case self::SMS_INCOMING:
				$strDefaultIconClass = 'sms-incoming';
				break;

			case self::EMAIL_OPT_IN:
				$strDefaultIconClass = 'comset-mail';
				break;

			case self::EMAIL_OPT_OUT:
				$strDefaultIconClass = 'comset-mail-off';
				break;

			case self::SMS_OPT_IN:
				$strDefaultIconClass = 'comset-sms';
				break;

			case self::SMS_OPT_OUT:
				$strDefaultIconClass = 'comset-sms-off';
				break;

			case self::POSTAL_MAIL_OPTIN:
				$strDefaultIconClass = 'comset-mail';
				break;

			case self::POSTAL_MAIL_OPTOUT:
				$strDefaultIconClass = 'comset-mail-off';
				break;

			case self::PHONE_CALLS_OPTIN:
				$strDefaultIconClass = 'comset-phone';
				break;

			case self::PHONE_CALLS_OPTOUT:
				$strDefaultIconClass = 'comset-phone-off';
				break;

			case self::VOICE_MAIL:
				$strDefaultIconClass = 'voicemail';
				break;

			case self::TOUR:
				$strDefaultIconClass = 'tour';
				break;

			case self::ONLINE_CHAT:
				$strDefaultIconClass = 'chat';
				break;

			case self::ONLINE_GUEST_CARD:
				$strDefaultIconClass = 'guestcard';
				break;

			case self::DOWNLOAD_APPLICATION_DOCUMENT:
			case self::DOCUMENT_DELETED:
				$strDefaultIconClass = 'document';
				break;

			case self::PAYMENT_SUBMITTED:
				$strDefaultIconClass = 'money';
				break;

			default:
				$strDefaultIconClass = 'status';
				break;
		}

		return $strDefaultIconClass;

	}

	public static function getAllEventTypeIconCssClasses( $arrintEventTypeIds = NULL ) {

		$objEventTypeRefelectionClass  = new ReflectionClass( get_class() );
		$arrstrEventTypeConstants = $objEventTypeRefelectionClass->getConstants();

		$arrstrEventTypeConstants = array_intersect( $arrintEventTypeIds, $arrstrEventTypeConstants );

		$arrmixEventTypeIconCssClasses = [];
		foreach( $arrstrEventTypeConstants as $mixContantValue ) {
			if( true == valId( $mixContantValue ) ) {
				$arrmixEventTypeIconCssClasses[$mixContantValue] = self::getEventTypeIconCssClass( $mixContantValue );
			}
		}

		return $arrmixEventTypeIconCssClasses;
	}

	public static function eventTypeIdToStr( $intEventTypeId ) {
		switch( $intEventTypeId ) {
			case self::EMAIL_INCOMING:
				return 'Email Incoming';
				break;

			case self::EMAIL_OUTGOING:
				return 'Email Incoming';
				break;

			case self::CALL_INCOMING:
				return 'Call Incoming';
				break;

			case self::CALL_OUTGOING:
				return 'Call Outgoing';
				break;

			case self::SMS_INCOMING:
				return 'Sms Incoming';
				break;

			case self::SMS_OUTGOING:
				return 'Sms Outgoing';
				break;

			case self::ONLINE_CHAT:
				return 'Online Chat';
				break;

			case self::NOTES:
				return 'Notes';
				break;

			case self::ONSITE_VISIT:
				return 'Onsite Visit';
				break;

			case self::ONLINE_GUEST_CARD:
				return 'Online Guest Card';
				break;

			case self::CRAIGSLIST_POST_REMINDER:
				return 'Craigslist Post';
				break;

			case self::DELINQUENCY_NOTES:
				return 'Delinquency Notes';
				break;

			case self::APPLICATION_UPDATED:
				return 'Application Updated';
				break;

			case self::SCHEDULED_FOLLOWUP:
				return 'Scheduled Followup';
				break;

			case self::DOCUMENT_DELETED:
				return 'Document Deleted';
				break;

			case self::MTM_OFFER_ACCEPTED:
				return 'MTM Offer Accepted';
				break;

			case self::DEPOSIT_SERVICE_SELECTED:
				return 'Deposit Service Selected';
				break;

			case self::DEPOSIT_SERVICE_REMOVED:
				return 'Deposit Service Removed';
				break;

			case self::QUOTE:
				return 'Quote';
				break;

			default:
				return NULL;
				break;
		}
	}

}
?>