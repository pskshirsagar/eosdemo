<?php

class CLeaseStartStructure extends CBaseLeaseStartStructure {

    const STANDARD_STRUCTURE                                = 1;
	const HIDDEN_SYSTEM_STUDENT_HOUSING_CATCH_ALL_STRUCTURE = 2;

	protected $m_strLeaseStartStructureName;
	protected $m_strDefaultLocaleLeaseStartStructureName;

	protected $m_intLeaseStartStructureCount;
	protected $m_intTotalProperties;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultLeaseStartStructureId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName( $objClientDatabase ) {
        if( true == is_null( $this->getName() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Lease start window is required.' ) ) );
        	return false;
        } elseif( false == is_null( $this->getName() ) ) {
        	$intConflictingLeaseStartStructureCount = CLeaseStartStructures::fetchConflictingLeaseStartStructureCountByCid( $this, $this->getCid(), $objClientDatabase );

        	if( 0 < $intConflictingLeaseStartStructureCount ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Lease start structure already exists with that name.' ) ) );
        		return false;
        	}
        }
        return true;
    }

    public function valDescription() {
        if( true == is_null( $this->getDescription() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
        	return false;
        }
        return true;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objClientDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valName( $objClientDatabase );
        		$boolIsValid &= $this->valDescription();
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

    // Get Functions

    public function getLeaseStartStructureName() {
    	return $this->m_strLeaseStartStructureName;
    }

    public function getTotalProperties() {
    	return $this->m_intTotalProperties;
    }

    public function getLeaseStartStructureCount() {
    	return $this->m_intLeaseStartStructureCount;
    }

	public function getDefaultLocaleLeaseStartStructureName() {
		return $this->m_strDefaultLocaleLeaseStartStructureName;
	}

	// Set Functions

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['lease_start_structure_name'] ) ) $this->setLeaseStartStructureName( $arrmixValues['lease_start_structure_name'] );
		if( true == isset( $arrmixValues['total_properties'] ) ) $this->setTotalProperties( $arrmixValues['total_properties'] );
		if( true == isset( $arrmixValues['lease_start_structure_count'] ) ) $this->setLeaseStartStructureCount( $arrmixValues['lease_start_structure_count'] );
		if( true == isset( $arrmixValues['default_locale_lease_start_structure_name'] ) ) $this->setDefaultLocaleLeaseStartStructureName( $arrmixValues['default_locale_lease_start_structure_name'] );
	}

	public function setLeaseStartStructureName( $strLeaseStartStructureName ) {
		$this->m_strLeaseStartStructureName = $strLeaseStartStructureName;
	}

	public function setTotalProperties( $intTotalProperties = 0 ) {
		$this->m_intTotalProperties = $intTotalProperties;
	}

	public function setLeaseStartStructureCount( $intLeaseStartStructureCount = 0 ) {
		$this->m_intLeaseStartStructureCount = $intLeaseStartStructureCount;
	}

	public function setDefaultLocaleLeaseStartStructureName( $strDefaultLocaleLeaseStartStructureName ) {
		$this->m_strDefaultLocaleLeaseStartStructureName = $strDefaultLocaleLeaseStartStructureName;
	}

}
?>