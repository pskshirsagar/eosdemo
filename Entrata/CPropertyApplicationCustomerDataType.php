<?php

class CPropertyApplicationCustomerDataType extends CBasePropertyApplicationCustomerDataType {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerDataTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVerificationTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsVerificationRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>