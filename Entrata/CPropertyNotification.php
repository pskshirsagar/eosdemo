<?php

class CPropertyNotification extends CBasePropertyNotification {

	protected $m_strLabelName;
	protected $m_strTitle;
	protected $m_strHeading;
	protected $m_strInstruction;
	protected $m_strTemplateName;
	protected $m_strIsReviewRequired;
	protected $m_strDescription;
	protected $m_boolIsApplyToAllProperties;

	public static $c_arrstrPropertyNotificationKeyIconCssClasses		= [
		'CALL_CENTER_LEAD_NOTES_SCRIPTING'							=> 'yellowlight',
		'CALL_CENTER_MAINTENANCE_NOTES_SCRIPTING'					=> 'yellowlight',
		'CALL_CENTER_LEAD_NOTIFICATION'								=> 'info-medium',
		'CALL_CENTER_MAINTENANCE_NOTIFICATION'						=> 'info-medium',
		'CALL_CENTER_RESIDENT_CONTACT_NOTIFICATION'					=> 'info-medium',
		'CALL_CENTER_OTHER_CONTACT_NOTIFICATION'					=> 'info-medium',
		'CALL_CENTER_CORPORATE_CARE_NOTIFICATION'					=> 'info-medium',
		'CALL_CENTER_LEAD_NOTIFICATION_MAINTENANCE'					=> 'maintenance-gray',
		'CALL_CENTER_MAINTENANCE_NOTIFICATION_MAINTENANCE'			=> 'maintenance-gray',
		'CALL_CENTER_RESIDENT_CONTACT_NOTIFICATION_MAINTENANCE'		=> 'maintenance-gray',
		'CALL_CENTER_OTHER_CONTACT_NOTIFICATION_MAINTENANCE'		=> 'maintenance-gray',
		'CALL_CENTER_CORPORATE_CARE_NOTIFICATION_MAINTENANCE'		=> 'maintenance-gray',
		'CALL_CENTER_LEAD_NOTIFICATION_PRICING'						=> 'money',
		'CALL_CENTER_MAINTENANCE_NOTIFICATION_PRICING'				=> 'money',
		'CALL_CENTER_RESIDENT_CONTACT_NOTIFICATION_PRICING'			=> 'money',
		'CALL_CENTER_OTHER_CONTACT_NOTIFICATION_PRICING'			=> 'money',
		'CALL_CENTER_CORPORATE_CARE_NOTIFICATION_PRICING'			=> 'money',
		'CALL_CENTER_LEAD_NOTIFICATION_APPOINTMENTS'				=> 'calendar',
		'CALL_CENTER_MAINTENANCE_NOTIFICATION_APPOINTMENTS'			=> 'calendar',
		'CALL_CENTER_RESIDENT_CONTACT_NOTIFICATION_APPOINTMENTS'	=> 'calendar',
		'CALL_CENTER_OTHER_CONTACT_NOTIFICATION_APPOINTMENTS'		=> 'calendar',
		'CALL_CENTER_CORPORATE_CARE_NOTIFICATION_APPOINTMENTS'		=> 'calendar',
		'CALL_CENTER_LEAD_NOTIFICATION_OFFICE_HOURS'				=> 'officeopen',
		'CALL_CENTER_MAINTENANCE_NOTIFICATION_OFFICE_HOURS'			=> 'officeopen',
		'CALL_CENTER_RESIDENT_CONTACT_NOTIFICATION_OFFICE_HOURS'	=> 'officeopen',
		'CALL_CENTER_OTHER_CONTACT_NOTIFICATION_OFFICE_HOURS'		=> 'officeopen',
		'CALL_CENTER_CORPORATE_CARE_NOTIFICATION_OFFICE_HOURS'		=> 'officeopen',
		'CALL_CENTER_LEAD_NOTIFICATION_AVAILABILITY'				=> 'check-circle-green',
		'CALL_CENTER_MAINTENANCE_NOTIFICATION_AVAILABILITY'			=> 'check-circle-green',
		'CALL_CENTER_RESIDENT_CONTACT_NOTIFICATION_AVAILABILITY'	=> 'check-circle-green',
		'CALL_CENTER_OTHER_CONTACT_NOTIFICATION_AVAILABILITY'		=> 'check-circle-green',
		'CALL_CENTER_CORPORATE_CARE_NOTIFICATION_AVAILABILITY'		=> 'check-circle-green',
		'CALL_CENTER_LEAD_NOTIFICATION_WARNING'						=> 'user-off',
		'CALL_CENTER_MAINTENANCE_NOTIFICATION_WARNING'				=> 'user-off',
		'CALL_CENTER_RESIDENT_CONTACT_NOTIFICATION_WARNING'			=> 'user-off',
		'CALL_CENTER_OTHER_CONTACT_NOTIFICATION_WARNING'			=> 'user-off',
		'CALL_CENTER_CORPORATE_CARE_NOTIFICATION_WARNING'			=> 'user-off'
	];

	public static $c_arrstrPropertyNotificationLeadKeyIconCssClasses = [
		'CALL_CENTER_LEAD_NOTES_SCRIPTING'				=> 'yellowlight',
		'CALL_CENTER_LEAD_NOTIFICATION'					=> 'info-medium',
		'CALL_CENTER_LEAD_NOTIFICATION_MAINTENANCE'		=> 'maintenance-gray',
		'CALL_CENTER_LEAD_NOTIFICATION_PRICING'			=> 'money',
		'CALL_CENTER_LEAD_NOTIFICATION_APPOINTMENTS'	=> 'calendar',
		'CALL_CENTER_LEAD_NOTIFICATION_OFFICE_HOURS'	=> 'officeopen',
		'CALL_CENTER_LEAD_NOTIFICATION_AVAILABILITY'	=> 'check-circle-green',
		'CALL_CENTER_LEAD_NOTIFICATION_WARNING'			=> 'user-off'
	];

	/**
	 * Setter Functions
	 */

	public function setLabelName( $strLabelName ) {
		$this->m_strLabelName = $strLabelName;
	}

	public function setIsApplyToAllProperties( $boolIsApplyToAllProperties ) {
		$this->m_boolIsApplyToAllProperties = $boolIsApplyToAllProperties;
	}

	/**
	* Getter Functions
	*/

	public function getLabelName() {
		return $this->m_strLabelName;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getHeading() {
		return $this->m_strHeading;
	}

	public function getExampleInstruction() {
		return $this->m_strInstruction;
	}

	public function getTemplateName() {
		return $this->m_strTemplateName;
	}

	public function getIsReviewRequired() {
		if( true == valArr( json_decode( $this->getValue(), true ) ) ) {
			$this->m_strIsReviewRequired = json_decode( $this->getValue(), true )['is_review_required'];
		} else {
			$this->m_strIsReviewRequired = $this->getValue();
		}

		return $this->m_strIsReviewRequired;
	}

	public function getDescription() {
		if( true == valArr( json_decode( $this->getValue(), true ) ) ) {
			$this->m_strDescription = json_decode( $this->getValue(), true )['description'];
		} else {
			$this->m_strDescription = $this->getValue();
		}

		return $this->m_strDescription;
	}

	public function getIsApplyToAllProperties() {
		return $this->m_boolIsApplyToAllProperties;
	}

	public function loadLabels() {
		switch( $this->getKey() ) {
			// Lead
			case 'CALL_CENTER_LEAD_PROMOTIONS_AND_SELLING':
				$this->m_strLabelName		= 'Promotions';
				$this->m_strTitle			= 'Edit Promotions';
				$this->m_strHeading			= 'Promotions';
				$this->m_strInstruction		= 'We are offering free rent to new applicants who move in during the month of February';
				$this->m_strTemplateName	= 'promotions_and_selling_points.tpl';
				break;

			case 'CALL_CENTER_LEAD_NOTES_SCRIPTING':
				$this->m_strLabelName	= 'Scripting';
				$this->m_strTitle		= 'Edit Scripting';
				$this->m_strHeading		= 'Scripting Instruction';
				$this->m_strInstruction	= 'Thank you for calling Austin Pointe Apartments. My name is _____________. How may I assist you?';
				break;

			case 'CALL_CENTER_LEAD_NOTES_SELLING_POINTS':
				$this->m_strLabelName	= 'Selling Points';
				$this->m_strTitle		= 'Edit Selling Points';
				$this->m_strHeading		= 'Instruction for Selling Points';
				$this->m_strInstruction	= 'We are offering free rent to new applicants who move in during the month of February.';
				break;

			case 'CALL_CENTER_LEAD_NOTES_PROMOTIONS':
				$this->m_strLabelName	= 'Promotions';
				$this->m_strTitle		= 'Edit Promotions';
				$this->m_strHeading		= 'Promotions';
				$this->m_strInstruction	= 'We are offering free rent to new applicants who move in during the month of February.';
				break;

			case 'CALL_CENTER_LEAD_NOTES_OTHER':
				$this->m_strLabelName	= 'Other Notes';
				$this->m_strTitle		= 'Edit Other Notes';
				$this->m_strHeading		= 'Other Notes';
				$this->m_strInstruction	= '';
				break;

			// Workorder
			case 'CALL_CENTER_MAINTENANCE_NOTES_SCRIPTING':
				$this->m_strLabelName	= 'Scripting';
				$this->m_strTitle		= 'Edit Scripting';
				$this->m_strHeading		= 'Scripting';
				$this->m_strInstruction	= 'Thank you for calling Austin Pointe Apartments. My name is _____________. How may I assist you?';
				break;

			case 'CALL_CENTER_MAINTENANCE_NOTES_EMERGENCY_DEFINITION':
				$this->m_strLabelName	= 'Definition of Emergency';
				$this->m_strTitle		= 'Edit Definition of Emergency';
				$this->m_strHeading		= 'Definition of Emergency';
				$this->m_strInstruction	= 'A maintenance emergency is defined as fire, smoke, gas, explosion, crime in progress, sewer backing up, water leaking through the ceiling, broken air conditioning or heating depending on the outdoor temperature (greater than 90 degrees or less than 40 degrees), no hot water, broken or malfunctioning stove, oven, or refrigerator.';
				break;

			case 'CALL_CENTER_MAINTENANCE_NOTES_GENERAL_PROTOCOL':
				$this->m_strLabelName	= 'General Protocol';
				$this->m_strTitle		= 'Edit General Protocol';
				$this->m_strHeading		= 'General Protocol';
				$this->m_strInstruction	= 'Make sure that this call is not an emergency. For routine maintenance calls, create a work order and we will address the issue as soon as we can. Do NOT give an estimated time of completion.';
				break;

			case 'CALL_CENTER_MAINTENANCE_NOTES_SECURITY_PROTOCOL':
				$this->m_strLabelName	= 'Security Protocol';
				$this->m_strTitle		= 'Edit Security Protocol';
				$this->m_strHeading		= 'Security Protocol';
				$this->m_strInstruction	= 'Make sure the resident has contacted 911 if there has been a fire, smoke, suspected criminal activity, or other emergency involving imminent harm. For maintenance emergencies, call the designated on-call maintenance personnel directly. If you cannot reach them, try calling again in 30 minutes. If you are still unable to reach them at that time, contact the property manager.';
				break;

			case 'CALL_CENTER_MAINTENANCE_NOTES':
				$this->m_strLabelName	= 'Property Notes';
				$this->m_strTitle		= 'Edit Property Notes';
				$this->m_strHeading		= 'Property Notes';
				$this->m_strInstruction	= 'Thank you for calling Austin Pointe Apartments. My name is _____________. How may I assist you?';
				break;

			case 'CALL_CENTER_MAINTENANCE_NOTES_ROUTINE_MAINTENANCE':
				$this->m_strLabelName	= 'Routine Maintenance';
				$this->m_strTitle		= 'Edit Routine Maintenance Protocol';
				$this->m_strHeading		= 'Routine Maintenance Protocol';
				$this->m_strInstruction	= 'Make sure that this call is not an emergency. For routine maintenance calls, create a work order and we will address the issue as soon as we can. Do NOT give an estimated time of completion.';
				break;

			case 'CALL_CENTER_MAINTENANCE_NOTES_EMERGENCY_MAINTENANCE':
				$this->m_strLabelName		= 'Emergency Maintenance';
				$this->m_strTitle			= 'Edit Emergency Maintenance Protocol';
				$this->m_strHeading			= 'Emergency Maintenance Protocol';
				$this->m_strInstruction		= '';
				$this->m_strTemplateName	= 'emergency_maintenance.tpl';
				break;

			// Resident
			case 'CALL_CENTER_RESIDENT_NOTES':
				$this->m_strLabelName	= 'Security issues & Disturbances';
				$this->m_strTitle		= 'Edit Security Protocol';
				$this->m_strHeading		= 'Security Protocol';
				$this->m_strInstruction	= 'If a resident complains about disturbances in the community relating to noise, vandalism, suspicious / destructive activity, or voices concerns about their own personal safety, please instruct them to contact the local police department at (555) 555-5555. Please also submit a message detailing their concerns, and we will follow up with the individuals involved.';
				break;

			case 'CALL_CENTER_RESIDENT_COMPLAINTS':
				$this->m_strLabelName	= 'Complaints';
				$this->m_strTitle		= 'Edit Complaints Protocol';
				$this->m_strHeading		= 'Complaints Protocol';
				$this->m_strInstruction	= 'We will take general complaints about the property or neighbors. Please take a message, and we will take action as soon as possible.';
				break;

			case 'CALL_CENTER_RESIDENT_PARKING_AND_TOWING':
				$this->m_strLabelName	= 'Parking & Towing';
				$this->m_strTitle		= 'Edit Parking & Towing Protocol';
				$this->m_strHeading		= 'Parking & Towing Information';
				$this->m_strInstruction	= 'Guest parking is available next to the clubhouse. If someone is parked in your spot, please contact the property management at (555) 555-5555. Our policy is to wait 24 hours before calling the towing company.';
				break;

			case 'CALL_CENTER_RESIDENT_CONTACT_NOTES':
				$this->m_strLabelName	= 'Other';
				$this->m_strTitle		= 'Edit Other';
				$this->m_strHeading		= 'Other';
				$this->m_strInstruction	= '';
				break;

			case 'CALL_CENTER_RESIDENT_OTHER':
				$this->m_strLabelName	= 'Other';
				$this->m_strTitle		= 'Edit Other';
				$this->m_strHeading		= 'Other';
				$this->m_strInstruction	= '';
				break;

			case 'CALL_CENTER_RESIDENT_PAY_INFORMATION':
				$this->m_strLabelName		= 'ResidentPay Information';
				$this->m_strTitle			= '';
				$this->m_strHeading			= '';
				$this->m_strInstruction		= '';
				$this->m_strTemplateName	= 'resident_pay_information.tpl';
				break;

			// Other
			case 'CALL_CENTER_MAINTENANCE_NOTES_OTHER':
				$this->m_strLabelName	= 'Other';
				$this->m_strTitle		= 'Edit Other';
				$this->m_strHeading		= 'Other';
				$this->m_strInstruction	= '';
				break;

			default:
				$this->m_strLabelName	= '';
				$this->m_strTitle		= '';
				$this->m_strHeading		= '';
				$this->m_strInstruction	= '';
		}
	}

	public function valKey() {
		$boolIsValid = true;

		if( false == valStr( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Please select property notification type.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;

		if( true == valStr( $this->getStartDatetime() ) && true == valStr( $this->getEndDatetime() ) && ( true == strtotime( $this->getStartDatetime() ) > strtotime( $this->getEndDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Property notification should not expires before it starts.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_leasing_center_property_notification':
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valStartDatetime();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function createDuplicate( $boolIsDuplicateUpdatebByAndUpdatedOn = false, $boolIsDuplicateCreatedByAndCreatedOn = true ) {
		$objPropertyNotification = new CPropertyNotification();
		$objPropertyNotification->setCid( $this->getCid() );
		$objPropertyNotification->setPropertyId( $this->getPropertyId() );
		$objPropertyNotification->setPropertySettingKeyId( $this->getPropertySettingKeyId() );
		$objPropertyNotification->setPropertyNotificationGroupId( $this->getPropertyNotificationGroupId() );
		$objPropertyNotification->setKey( $this->getKey() );
		$objPropertyNotification->setValue( $this->getValue() );
		$objPropertyNotification->setStartDatetime( $this->getStartDatetime() );
		$objPropertyNotification->setEndDatetime( $this->getEndDatetime() );
		$objPropertyNotification->setIsHighlighted( $this->getIsHighlighted() );

		if( true == $boolIsDuplicateUpdatebByAndUpdatedOn ) {
			$objPropertyNotification->setUpdatedBy( $this->getUpdatedBy() );
			$objPropertyNotification->setUpdatedOn( $this->getUpdatedOn() );
		}

		if( true == $boolIsDuplicateCreatedByAndCreatedOn ) {
			$objPropertyNotification->setCreatedBy( $this->getCreatedBy() );
			$objPropertyNotification->setCreatedOn( $this->getCreatedOn() );
		}

		return $objPropertyNotification;
	}

}
?>