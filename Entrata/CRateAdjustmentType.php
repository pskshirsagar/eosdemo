<?php

class CRateAdjustmentType extends CBaseRateAdjustmentType {

	const NONE 			= 1;
	const DECREASE_BY 	= 2;
	const INCREASE_BY 	= 3;
	const FIXED_AT 		= 4;

}
?>