<?php

class CInspectionFormLocation extends CBaseInspectionFormLocation {

	protected $m_arrobjMaintenanceProblems;
	protected $m_strCustomLocationName;

    /**
     * Get Functions
     */

    public function getMaintenanceProblems() {
    	return $this->m_arrobjMaintenanceProblems;
    }

    public function getCustomLocationName() {
    	return $this->m_strCustomLocationName;
    }

    /**
     * Set Functions
     */

    public function setMaintenanceProblems( $arrobjMaintenanceProblems ) {
    	$this->m_arrobjMaintenanceProblems = $arrobjMaintenanceProblems;
    }

    public function setCustomLocationName( $strCustomLocationName ) {
    	$this->m_strCustomLocationName = $strCustomLocationName;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionFormId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceLocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>