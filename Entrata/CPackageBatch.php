<?php

class CPackageBatch extends CBasePackageBatch {
	protected $m_intTotalPackages;
	protected $m_intTotalPickedUp;
	protected $m_intTotalDelivered;
	protected $m_intTotalPrint;
	protected $m_intTotalResend;
	protected $m_intPackageBatchPropertyCount;

	protected $m_strPackageBatchPropertyNames;

    /**
     * Get Functions
     */

    public function getTotalPackages() {
    	return $this->m_intTotalPackages;
    }

    public function getTotalPickedUp() {
    	return $this->m_intTotalPickedUp;
    }

    public function getTotalDelivered() {
    	return $this->m_intTotalDelivered;
    }

    public function getTotalPrint() {
    	return $this->m_intTotalPrint;
    }

    public function getTotalResend() {
    	return $this->m_intTotalResend;
    }

    public function getPackageBatchPropertyNames() {
    	return $this->m_strPackageBatchPropertyNames;
    }

    public function getPackageBatchPropertyCount() {
    	return $this->m_intPackageBatchPropertyCount;
    }

    /**
     * Set Functions
     */

    public function setTotalPackages( $intTotalPackages ) {
    	$this->m_intTotalPackages = CStrings::strTrimDef( $intTotalPackages, -1, NULL, true );
    }

    public function setTotalPickedUp( $intTotalPickedUp ) {
    	$this->m_intTotalPickedUp = CStrings::strToIntDef( $intTotalPickedUp, NULL, false );
    }

    public function setTotalDelivered( $intTotalDelivered ) {
    	$this->m_intTotalDelivered = CStrings::strToIntDef( $intTotalDelivered, NULL, false );
    }

    public function setTotalPrint( $intTotalPrint ) {
    	$this->m_intTotalPrint = CStrings::strToIntDef( $intTotalPrint, NULL, false );
    }

    public function setTotalResend( $intTotalResend ) {
    	$this->m_intTotalResend = CStrings::strToIntDef( $intTotalResend, NULL, false );
    }

    public function setPackageBatchPropertyNames( $strPackageBatchPropertyNames ) {
    	$this->m_strPackageBatchPropertyNames = $strPackageBatchPropertyNames;
    }

    public function setPackageBatchPropertyCount( $intPackageBatchPropertyCount ) {
    	$this->m_intPackageBatchPropertyCount = $intPackageBatchPropertyCount;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrValues );

    	 if( isset( $arrValues['total_packages'] ) && $boolDirectSet )
    	 	$this->m_intTotalPackages = trim( $arrValues['total_packages'] );
    	 elseif ( isset( $arrValues['total_packages'] ) )
    	 	$this->setTotalPackages( $arrValues['total_packages'] );
         if( isset( $arrValues['total_picked_up'] ) && $boolDirectSet )
         	$this->m_intTotalPickedUp = trim( $arrValues['total_picked_up'] );
         elseif ( isset( $arrValues['total_picked_up'] ) )
         	$this->setTotalPickedUp( $arrValues['total_picked_up'] );
         if( isset( $arrValues['total_delivered'] ) && $boolDirectSet )
         	$this->m_intTotalDelivered = trim( $arrValues['total_delivered'] );
         elseif ( isset( $arrValues['total_delivered'] ) )
         	$this->setTotalDelivered( $arrValues['total_delivered'] );
         if( isset( $arrValues['total_print'] ) && $boolDirectSet )
         	$this->m_intTotalPrint = trim( $arrValues['total_print'] );
         elseif ( isset( $arrValues['total_print'] ) )
         	$this->setTotalPrint( $arrValues['total_print'] );
         if( isset( $arrValues['total_resend'] ) && $boolDirectSet )
         	$this->m_intTotalResend = trim( $arrValues['total_resend'] );
         elseif ( isset( $arrValues['total_resend'] ) )
         	$this->setTotalResend( $arrValues['total_resend'] );
         if( isset( $arrValues['property_names'] ) && $boolDirectSet )
         	$this->m_strPackageBatchPropertyNames = trim( $arrValues['property_names'] );
         elseif ( isset( $arrValues['property_names'] ) )
         	$this->setPackageBatchPropertyNames( $arrValues['property_names'] );
         if( isset( $arrValues['property_count'] ) && $boolDirectSet )
         	$this->m_strPackageBatchPropertyCount = trim( $arrValues['property_count'] );
         elseif ( isset( $arrValues['property_count'] ) )
         	$this->setPackageBatchPropertyCount( $arrValues['property_count'] );
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        if( $this->m_intCid == NULL ) {
        	trigger_error( 'A Client Id is required - CMaintenanceRequest', E_USER_ERROR );
        	$boolIsValid = false;
        } elseif( $this->m_intCid < 1 ) {
        	// Must be greater than zero
        	trigger_error( 'A Client Id has to be greater than zero - CMaintenanceRequest', E_USER_ERROR );
        	$boolIsValid = false;
        }
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valItemCount() {
        $boolIsValid = true;

        if( 0 == ( int ) $this->m_intItemCount ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'received_by', 'Please enter at least one package.', 507 ) );
        }
        return $boolIsValid;
    }

    public function valBatchDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPrintedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReceivedBy() {
        $boolIsValid = true;
        if( false == isset( $this->m_intReceivedBy ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'received_by', 'Please enter at least one package.', 507 ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valReceivedBy();
            	$boolIsValid &= $this->valItemCount();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>