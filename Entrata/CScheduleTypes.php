<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduleTypes
 * Do not add any new functions to this class.
 */

class CScheduleTypes extends CBaseScheduleTypes {

    public static function fetchScheduleTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CScheduleType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchScheduleType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CScheduleType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPublishedScheduleTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						schedule_types
					WHERE
						is_published = 1';

		return self::fetchScheduleTypes( $strSql, $objDatabase );
    }

    public static function fetchPublishedScheduleTypesByIds( $arrintIds, $objDatabase ) {

    	if( false == valArr( $arrintIds ) ) return false;

    	$strSql = 'SELECT
						*
					FROM
						schedule_types
					WHERE
						is_published = 1
    					AND id IN ( ' . implode( ',', $arrintIds ) . ' )';

    	return self::fetchScheduleTypes( $strSql, $objDatabase );
    }
}
?>