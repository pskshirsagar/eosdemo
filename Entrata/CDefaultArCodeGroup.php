<?php

class CDefaultArCodeGroup extends CBaseDefaultArCodeGroup {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeGroupTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeSummaryTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;

	}

}
?>