<?php

class CCustomerAlertType extends CBaseCustomerAlertType {
	//use TEosDetails;

	const ANNOUNCEMENT				= 1;
	const MESSAGE_CENTER			= 2;
	const LATE_RENT					= 3;
	const SIGN_LEASE				= 4;
	const RENEWAL_OFFER				= 5;
	const INSPECTION				= 6;
	const ESIGN						= 7;
	const PARCEL					= 8;
	const OPT_OUT_EMAIL_INVOICES	= 9;
	const OPT_IN_SMS				= 10;
	const SCHEDULED_CHARGE			= 11;
	const RATING_AND_REVIEW			= 12;
	const AR_PAYMENT_RETURN			= 13;
	const SCHEDULED_PAYMENT_RETURN	= 14;
	const ROOMMATE_PAYMENT			= 15;
	const PAUSED_SCHEDULED_PAYMENT	= 16;
	const MOVE_IN_CHECKLIST			= 17;
	const MOVE_OUT_CHECKLIST		= 18;
	const RELINK_PAYMENT_ACCOUNT	= 20;
	const SCHEDULED_MOVE_IN			= 21;
	const NEW_DOCUMENT_REQUESTED	= 23;
	const TRANSFER_QUOTE_OFFER		= 24;

	static $c_arrintAllAlertTypeIds = array(
		self::ANNOUNCEMENT,
		self::LATE_RENT,
		self::SIGN_LEASE,
		self::TRANSFER_QUOTE_OFFER,
		self::INSPECTION,
		self::ESIGN,
		self::PARCEL,
		self::OPT_OUT_EMAIL_INVOICES,
		self::OPT_IN_SMS,
		self::SCHEDULED_CHARGE,
		self::RATING_AND_REVIEW,
		self::AR_PAYMENT_RETURN,
		self::SCHEDULED_PAYMENT_RETURN,
		self::ROOMMATE_PAYMENT,
		self::PAUSED_SCHEDULED_PAYMENT,
		self::MOVE_IN_CHECKLIST,
		self::MOVE_OUT_CHECKLIST,
		self::RELINK_PAYMENT_ACCOUNT,
		self::SCHEDULED_MOVE_IN,
		self::NEW_DOCUMENT_REQUESTED,
		self::TRANSFER_QUOTE_OFFER
	);

	static $c_arrintResidentPortalPopupIds = array(
		self::ANNOUNCEMENT,
		self::PARCEL,
		self::OPT_OUT_EMAIL_INVOICES,
		self::OPT_IN_SMS
	);

	static $c_arrintShowInModalIds = array(
		self::ROOMMATE_PAYMENT
	);

	public function __construct() {
		parent::__construct();

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>