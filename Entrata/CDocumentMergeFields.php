<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentMergeFields
 * Do not add any new functions to this class.
 */

class CDocumentMergeFields extends CBaseDocumentMergeFields {

    public static function addTenantechMergeFieldsByMasterDocumentIdByStateCodeByExternalFormKey( int $intMasterDocumentId, array $arrstrStateCode, array $arrstrExternalFormKey, int $intCompanyUserId, int $intCid, CDatabase $objDatabase ) {
        $strSql = 'SELECT * FROM func_sync_tenantech_merge_fields(' . $intCid . '::INTEGER, ' .
            $intMasterDocumentId . '::INTEGER, ' .
            $intCompanyUserId . '::INTEGER, ' .
            'ARRAY[\'' . implode( '\',\'', $arrstrStateCode ) . '\']::TEXT[],' .
            'ARRAY[\'' . implode( '\',\'', $arrstrExternalFormKey ) . '\']::TEXT[]' . ')';
        return fetchData( $strSql, $objDatabase );
    }

	public static function syncBluemoonCustomMergeFields( $intDocumentId, $intDocumentAdendaId, $intCompanyUserId, $arrstrExternalMergeFields, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM func_sync_bluemoon_custom_merge_fields(' . $intCid . '::INTEGER, ' .
		          $intDocumentId . '::INTEGER, ' .
		          $intDocumentAdendaId . '::INTEGER, ' .
		          $intCompanyUserId . '::INTEGER, ' .
		          '(\'' . $arrstrExternalMergeFields . ' \')::JSON )';
		return fetchData( $strSql, $objDatabase );
	}

	public static function syncBluemoonStandardMergeFields( $intMasterDocumentId, $intCompanyUserId, $strApartmentAssociationStateCode, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM func_sync_bluemoon_merge_fields(' . $intCid . '::INTEGER,' .
		          $intMasterDocumentId . '::INTEGER,' .
		          $intCompanyUserId . '::INTEGER,' .
		          'ARRAY[' . $strApartmentAssociationStateCode . ']::TEXT[][] )';
		return fetchData( $strSql, $objDatabase );
	}

    public static function syncBluemoonCompanyMergeFields( $intMasterDocumentId, $intCompanyUserId, $arrstrApartmentAssociationStateCodes, $arrstrCustomExternalFormFields, $arrstrAllRequiredExternalFormKeys, $intCid, $objDatabase ) {
        $strSql = 'SELECT * FROM func_sync_bluemoon_company_merge_fields(' . $intCid . '::INTEGER,' .
            $intMasterDocumentId . '::INTEGER,' .
            $intCompanyUserId . '::INTEGER,' .
            'ARRAY[' . implode( ',', $arrstrApartmentAssociationStateCodes ) . ']::TEXT[][],' .
            'ARRAY[' . implode( ',', $arrstrAllRequiredExternalFormKeys ) . ']::VARCHAR[],' .
            'ARRAY[' . implode( ',', $arrstrCustomExternalFormFields['fields'] ) . ']::VARCHAR[],' .
            'ARRAY[' . implode( ',', $arrstrCustomExternalFormFields['external_form_keys'] ) . ']::VARCHAR[] )';
        return fetchData( $strSql, $objDatabase );

    }

	public static function fetchDocumentMergeFieldsByPropertyIdByDocumentAddendaIdsByCid( $intPropertyId, $arrintDocumentAddendaIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentAddendaIds ) ) return [];

		$strSql = 'SELECT
						dmf.*,
						COALESCE( dmf.merge_field_group_id, ' . CMergeFieldGroup::OTHERS . ' ) AS merge_field_group_id,
						util_get_system_translated( \'title\', dftmf.title, dftmf.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS title,
						dftmf.order_num AS order_num,
						dftmf.is_locked,
		                dftmf.view_expression as view_expression,
                
		                COALESCE( dftmf.merge_field_sub_group_id, dmf.merge_field_sub_group_id ) AS merge_field_sub_group_id,      

	                    COALESCE( dftmf.details, \'{}\'::JSONB ) || COALESCE( emf.details, \'{}\'::JSONB ) || COALESCE( dmf.details, \'{}\'::JSONB ) AS details
		             FROM
						document_merge_fields dmf
						JOIN ( SELECT

									CASE WHEN dmf1.id IS NOT NULL THEN
										dmf1.id
									ELSE
										dmf.id
									END AS document_merge_field_id,
									dmf.cid,
									rank() OVER ( PARTITION BY dmf.document_id, dmf.field, dmf.default_merge_field_id, dmf.external_field_name ORDER BY dmf.property_id ASC ) as rank1
								FROM
									document_merge_fields dmf
									LEFT JOIN document_merge_fields dmf1 ON ( dmf.id = dmf1.id
																			  AND dmf.cid = dmf1.cid
																			  AND dmf1.cid = ' . ( int ) $intCid . '
																			  AND dmf1.property_id = ' . ( int ) $intPropertyId . '
																			  AND dmf1.document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' ) 
																			  AND dmf1.deleted_by IS NULL 
																			  AND dmf1.deleted_on IS NULL )
								WHERE
									dmf.cid = ' . ( int ) $intCid . '
									AND dmf.document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
									AND ( dmf.property_id IS NULL OR dmf.property_id = dmf1.property_id )
									AND dmf.deleted_by IS NULL 
									AND dmf.deleted_on IS NULL

						) AS sub_query ON ( dmf.cid = sub_query.cid AND sub_query.document_merge_field_id = dmf.id AND rank1 = 1 )
						LEFT JOIN default_merge_fields dftmf ON ( dmf.default_merge_field_id = dftmf.id )
						LEFT JOIN external_merge_fields emf ON ( dmf.field = emf.external_merge_field_name ) 
						LEFT JOIN merge_field_sub_groups mfsg ON( mfsg.id = dftmf.merge_field_sub_group_id )
					WHERE
						dmf.cid = ' . ( int ) $intCid . '
						AND dmf.deleted_by IS NULL 
						AND dmf.deleted_on IS NULL
					ORDER BY
						CASE
							WHEN emf.id IS NOT NULL THEN emf.details->\'radio_group\'
						END,
						dmf.external_field_name NULLS LAST, 
						CASE
							WHEN dftmf.id IS NULL THEN dmf.id
						END, mfsg.order_num, dftmf.order_num';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase, $boolIsFetchCount = false ) {
		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						document_merge_fields
					WHERE
						cid = ' . ( int ) $intCid . '
						AND document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND deleted_by IS NULL 
						AND deleted_on IS NULL
					ORDER BY
						id';

		$arrobjDocumentMergeFields = self::fetchDocumentMergeFields( $strSql, $objDatabase );
		if( true == $boolIsFetchCount ) {
			return \Psi\Libraries\UtilFunctions\count( $arrobjDocumentMergeFields );
		} else {
			return $arrobjDocumentMergeFields;
		}
	}

	public static function fetchDocumentMergeFieldsByPropertyIdsByDocumentAddendaIdsByCid( $arrintPropertyIds, $arrintDocumentAddendaIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

		$strSql = 'SELECT *	FROM
						document_merge_fields
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
						AND property_id IS NOT NULL
						AND block_document_merge_field_id IS NULL
						AND deleted_by IS NULL 
						AND deleted_on IS NULL
					ORDER BY
						id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByDocumentAddendaIdsHavingPropertyIdIsNullByCid( $arrintDocumentAddendaIds, $intCid, $objDatabase, $boolBlockField = false ) {
		if( false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

		$strSql = 'SELECT 
                        dmf.*,
                        COALESCE( dftmf.details, \'{}\'::JSONB ) || COALESCE( dmf.details, \'{}\'::JSONB ) AS details
                    FROM
						document_merge_fields dmf
						LEFT JOIN default_merge_fields dftmf ON ( dmf.default_merge_field_id = dftmf.id )
					WHERE
						dmf.cid = ' . ( int ) $intCid . '
						AND dmf.document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
						AND dmf.property_id IS NULL
						AND dmf.deleted_by IS NULL 
						AND dmf.deleted_on IS NULL';

		$strSql .= ( true == $boolBlockField ) ? ' AND dmf.is_block_field = 1' : '';

		$strSql .= ' ORDER BY dmf.id ';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchInsertingDocumentMergeFieldsForPropertyCompanyMergeFieldExceptionByDocumentAddendaIdsByCompanyMergeFieldIdByCid( $arrintDocumentAddendaIds, $intCompanyMergeFieldId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentAddendaIds ) || false == valId( $intCompanyMergeFieldId ) ) return NULL;

		$strSql = 'SELECT 
						dmf.*,
						cmf.show_in_merge_display as show_in_merg_display,
					    cmf.allow_user_update as allow_user_update,
					    cmf.is_required as is_required,
					    util_get_translated( \'default_value\', cmf.default_value, cmf.details ) as default_value
					FROM
						document_merge_fields dmf
						JOIN company_merge_fields cmf ON ( dmf.cid = cmf.cid AND dmf.company_merge_field_id = cmf.id )
					WHERE
						dmf.cid = ' . ( int ) $intCid . '
						AND dmf.document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
						AND dmf.company_merge_field_id = ' . ( int ) $intCompanyMergeFieldId . '
						AND dmf.property_id IS NULL
						AND dmf.deleted_by IS NULL 
						AND dmf.deleted_on IS NULL';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByFieldByCid( $strField, $intCid, $objDatabase ) {
		if( false == valStr( $strField ) ) return NULL;

		$strSql = 'SELECT * FROM
						document_merge_fields
					WHERE
						cid = ' . ( int ) $intCid . '
						AND field LIKE ( \'' . $strField . '\' )
						AND deleted_by IS NULL 
						AND deleted_on IS NULL
					ORDER BY
						id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchSimpleDocumentMergeFieldsByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {

		if( false == valId( $intCompanyMergeFieldId ) ) return NULL;

		$strSql = 'SELECT 
						*
                    FROM
                        documents d 
                        JOIN document_addendas da ON ( d.cid = da.cid AND d.id = da.document_id AND da.deleted_on IS NULL AND d.deleted_on IS NULL )
						JOIN document_merge_fields dmf ON (da.cid = dmf.cid AND da.id = dmf.document_addenda_id AND dmf.deleted_on IS NULL )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND dmf.company_merge_field_id = ' . ( int ) $intCompanyMergeFieldId . '
					ORDER BY
						dmf.id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByDocumentAddendaIdsByCid( $arrintDocumentAddendaIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						document_merge_fields
					WHERE
						cid = ' . ( int ) $intCid . '
						AND document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' ) 
						AND deleted_by IS NULL 
						AND deleted_on IS NULL
					ORDER BY id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByCompanyMergeFieldIdsByCid( $arrintCompanyMergeFieldIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyMergeFieldIds ) ) return NULL;

		$strSql = 'SELECT
                        *
                    FROM
						document_merge_fields
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_merge_field_id IN ( ' . implode( ',', $arrintCompanyMergeFieldIds ) . ' )
						AND deleted_by IS NULL 
						AND deleted_on IS NULL
					ORDER BY
						id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByPropertyIdByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase, $intPropertyId = NULL ) {

		$strSql = 'SELECT
	                   dmf.*,
	                   ft.system_code,
	                   d.is_entrata_default
	               FROM
	                   documents d
	                   JOIN file_types ft ON ( d.cid = ft.cid AND d.file_type_id = ft.id )
	                   JOIN document_merge_fields dmf ON ( dmf.cid = d.cid AND dmf.document_id = d.id AND dmf.deleted_by IS NULL AND dmf.deleted_on IS NULL)
	               WHERE ';

		if( true == is_null( $intPropertyId ) ) {
			$strSql .= 'dmf.property_id IS NULL';
		} else {
			$strSql .= 'dmf.property_id = ' . ( int ) $intPropertyId;
		}

		$strSql .= ' AND dmf.cid = ' . ( int ) $intCid . '
	                 AND dmf.document_id = ' . ( int ) $intDocumentId . '
	               	ORDER BY dmf.field desc';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );

	}

	public static function fetchDocumentMergeFieldsByMergeFieldGroupIdByDocumentIdByDocumentAddendaIdByCid( $intMergeFieldGroupId, $intDocumentId, $intDocumentAddendaId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
                        *
                    FROM
                        document_merge_fields
                    WHERE
                        cid = ' . ( int ) $intCid . '
                        AND merge_field_group_id = ' . ( int ) $intMergeFieldGroupId . '
                        AND document_id = ' . ( int ) $intDocumentId . '
                        AND document_addenda_id = ' . ( int ) $intDocumentAddendaId . '
                        AND property_id IS NULL
                        AND deleted_by IS NULL 
						AND deleted_on IS NULL
                    ORDER BY
                        id';
		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByMergeFieldGroupIdByDocumentIdsByDocumentAddendaIdsByCid( $intMergeFieldGroupId, $arrintDocumentIds, $arrintDocumentAddendaIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDocumentIds ) || false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

		$strSql = 'SELECT
                        *
                    FROM
                        document_merge_fields
                    WHERE
                        cid = ' . ( int ) $intCid . '
                        AND merge_field_group_id = ' . ( int ) $intMergeFieldGroupId . '
                        AND document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
                        AND document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
                        AND property_id IS NULL
                        AND deleted_by IS NULL 
						AND deleted_on IS NULL
                    ORDER BY
                        page_number,y_pos,x_pos';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByMergeFieldGroupIdByDocumentIdByCid( $intMergeFieldGroupId, $intDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							*
						FROM
							document_merge_fields
						WHERE
							cid = ' . ( int ) $intCid . '
							AND merge_field_group_id = ' . ( int ) $intMergeFieldGroupId . '
							AND document_id = ' . ( int ) $intDocumentId . '
							AND deleted_by IS NULL 
							AND deleted_on IS NULL
						ORDER BY id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByPropertyIdByDocumentIdsByCid( $intPropertyId, $arrintDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
	                   dmf.*
	               FROM
	                   documents d
	                   JOIN document_merge_fields dmf ON ( dmf.cid = d.cid AND dmf.document_id = d.id AND dmf.deleted_by IS NULL AND dmf.deleted_on IS NULL)

	               WHERE
	                 dmf.property_id = ' . ( int ) $intPropertyId . '
	                 AND dmf.cid = ' . ( int ) $intCid . '
	                 AND dmf.document_id IN ( ' . implode( ',', $arrintDocumentId ) . ' ) ';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );

	}

	public static function fetchDocumentMergeFieldsByPropertyIdsByDocumentIdByCid( $arrintPropertyIds, $intDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							*
						FROM
							document_merge_fields
						WHERE
							cid = ' . ( int ) $intCid . '
							AND property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND document_id = ' . ( int ) $intDocumentId . '
							AND deleted_by IS NULL 
							AND deleted_on IS NULL
						ORDER BY id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByPropertyIdsByDocumentIdsByCid( $arrintPropertyIds, $arrintDocumentIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
							*
						FROM
							document_merge_fields
						WHERE
							cid = ' . ( int ) $intCid . '
							AND property_id IN  ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
							AND deleted_by IS NULL 
							AND deleted_on IS NULL
						ORDER BY id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDescrepentDocumentMergeFieldsByFieldsByCid( $arrstrFields, $intCid, $objDatabase, $boolIsSystem = '' ) {

		if( false == valArr( $arrstrFields ) ) return NULL;

		$strWhere = '';

		if( false == $boolIsSystem ) {
			$strWhere = ' AND dmf.default_merge_field_id IS NULL ';
		}

		if( true == $boolIsSystem ) {
			$strWhere = ' AND dmf.default_merge_field_id IS NOT NULL ';
		}

		$strSql = 'CREATE TEMP TABLE temp_document_merge_fields ( key, field ) ON COMMIT DROP AS ( values ' . sqlStrMultiImplode( [ $arrstrFields ] ) . ' );
					SELECT	
						dmf.company_merge_field_id,
						MAX(dmf.field) as field
					FROM
						document_merge_fields dmf 
					WHERE
						dmf.cid = ' . ( int ) $intCid . '
						AND dmf.deleted_on IS NULL
						AND dmf.property_id IS NOT NULL
						AND dmf.default_value IS NOT NULL
						' . $strWhere . '
						AND dmf.field IN (
                        	select field from temp_document_merge_fields
                        ) 
					    AND dmf.document_addenda_id IN (
					      SELECT
					          da.id
					       FROM
					          documents d
					          JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
					       WHERE
					          d.cid = ' . ( int ) $intCid . '
					          AND da.deleted_on IS NULL 
					          AND d.deleted_on IS NULL 
					          AND d.archived_on IS NULL 
					          AND da.archived_on IS NULL
					    )
					GROUP BY
						dmf.property_id,
						dmf.company_merge_field_id
					HAVING
						 count( distinct dmf.default_value ) > 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomDocumentMergeFieldsByFieldByCid( $strField, $intCid, $objDatabase ) {
		if( false == valStr( $strField ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                        dmf.*,
                        p.property_name,
                        d.name as template_name,
                        d.id as template_id
                    FROM
                        documents d
                        JOIN document_addendas da ON ( d.cid = da.cid AND d.id = da.document_id AND da.deleted_on IS NULL AND d.deleted_on IS NULL AND d.archived_on IS NULL AND da.archived_on IS NULL)
						JOIN document_merge_fields dmf ON ( d.cid = dmf.cid AND d.id = dmf.document_id AND dmf.document_addenda_id = da.id AND dmf.deleted_on IS NULL )
						LEFT JOIN properties p ON ( dmf.property_id = p.id AND dmf.cid = p.cid )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND dmf.field = \'' . $strField . '\'
						AND dmf.default_value IS NOT NULL
					ORDER BY
						id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleDocumentMergeFieldsByFieldByPropertyIdsByCid( $strField, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valStr( $strField ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
							*
						FROM
							document_merge_fields
						WHERE
							cid = ' . ( int ) $intCid . '
							AND is_block_field = 0
							AND block_document_merge_field_id IS NULL
							AND property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
							AND field = \'' . $strField . '\'
						ORDER BY id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByFieldByExcludingPropertyIdsByCid( $strField, $arrintExcludingPropertyIds, $intCid, $objDatabase ) {

		if( false == valStr( $strField ) ) return NULL;

		$strSql = 'SELECT
							dmf.*
						FROM
							documents d 
							JOIN document_addendas da ON ( d.id = da.document_id AND d.cid = da.cid )
							JOIN document_merge_fields dmf ON ( dmf.cid = d.cid AND dmf.document_id = d.id AND dmf.document_addenda_id = da.id )
						WHERE
							d.cid = ' . ( int ) $intCid . '
							AND dmf.is_block_field = 0
							AND dmf.block_document_merge_field_id IS NULL
							AND ( dmf.property_id NOT IN ( ' . implode( ',', $arrintExcludingPropertyIds ) . ' ) OR dmf.property_id IS NULL )
							AND dmf.field = \'' . $strField . '\'';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function bulkDelete( $arrobjTargetObjects, $objDatabase, $intCurrentUserId = 1 ) {

		$objDataset = $objDatabase->createDataset();
		$strSql = 'UPDATE
						public.document_merge_fields
					SET ';
		$strSql .= ' deleted_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' deleted_on = \'NOW()\' ';
		$strSql .= ' WHERE';
		$strWhereClauseWithCid = $strWhereClause = '';
		foreach( $arrobjTargetObjects as $objTargetObject ) {
			if( true == method_exists( $objTargetObject, 'getCid' ) && false == is_null( $objTargetObject->getCid() ) ) {
				$strWhereClauseWithCid .= '( ' . ( double ) $objTargetObject->sqlId() . ', ' . ( int ) $objTargetObject->sqlCid() . ' ), ';
			} else {
				$strWhereClause .= ( double ) $objTargetObject->sqlId() . ', ';
			}
		}

		if( true == valStr( $strWhereClauseWithCid ) ) $strSql .= '( id, cid ) IN ( ' . trim( $strWhereClauseWithCid, ', ' ) . ' ) ';

		if( true == valStr( $strWhereClause ) ) {
			if( true == valStr( $strWhereClauseWithCid ) ) $strSql .= ' OR ';

			$strSql .= ' id IN ( ' . trim( $strWhereClause, ', ' ) . ' ) ';
		}

		$strSql .= ';';

		return $objDataset->execute( $strSql, $objDatabase );
	}

	public static function fetchDocumentMergeFieldsByBlockDocumentMergeFieldIdsByCid( $arrintBlockDocumentMergeFieldIds,$intCid, $objDatabase ) {

		if( false == valArr( $arrintBlockDocumentMergeFieldIds ) || false == valArr( $arrintBlockDocumentMergeFieldIds ) ) return NULL;

		$strSql = 'SELECT
                        *
                    FROM
                        document_merge_fields
                    WHERE
                        cid = ' . ( int ) $intCid . '
                        AND block_document_merge_field_id IN ( ' . implode( ',', $arrintBlockDocumentMergeFieldIds ) . ' )
                        AND deleted_by IS NULL 
						AND deleted_on IS NULL
                    ORDER BY
                        id';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

    public static function fetchAllDocumentMergeFieldsByDocumentAddendaIdsByCid( $arrintDocumentAddendaIds, $intCid, $objDatabase ) {
        if( false == valArr( $arrintDocumentAddendaIds ) ) return NULL;

        $strSql = 'SELECT
						*
					FROM
						document_merge_fields
					WHERE
						cid = ' . ( int ) $intCid . '
						AND document_addenda_id IN ( ' . implode( ',', $arrintDocumentAddendaIds ) . ' )
					ORDER BY id';

        return ( array ) self::fetchDocumentMergeFields( $strSql, $objDatabase );
    }

	public static function fetchDocumentMergeFieldsByDefaultMergeFieldIdsByCid( $arrintDefaultMerfeFieldIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintDefaultMerfeFieldIds ) ) return NULL;
		$strSql = 'SELECT *
				   FROM
				        document_merge_fields dmf
				   WHERE
				        dmf.cid = ' . ( int ) $intCid . '
				        AND dmf.default_merge_field_id IN ( ' . implode( ',', $arrintDefaultMerfeFieldIds ) . ' )
				        AND dmf.deleted_on IS NULL ';

		return parent::fetchDocumentMergeFields( $strSql, $objDatabase );

	}

    public static function fetchAllDateDocumentMergeFieldNamesByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {
    if( false == valArr( $arrintDocumentIds ) ) return [];

    $strSql = 'SELECT
            dmf.field
          FROM
            document_merge_fields dmf
            LEFT JOIN external_merge_fields emf ON ( dmf.external_field_name = emf.external_merge_field_name AND emf.data_type = \'date\' )
            LEFT JOIN company_merge_fields cmf ON ( dmf.company_merge_field_id = cmf.id AND dmf.cid = cmf.cid AND ( cmf.merge_field_type_id = ' . ( int ) CMergeFieldType::DATE_MERGE_FIELD . ' OR cmf.details->>\'html_input_type\' = \'' . CCompanyMergeField::INPUT_TYPE_DATE . '\' ) )
          WHERE
            dmf.cid = ' . ( int ) $intCid . '
            AND ( emf.id IS NOT NULL OR cmf.id IS NOT NULL )
            AND dmf.document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )';

    return ( array ) fetchData( $strSql, $objDatabase );
    }

	public static function fetchDocumentMergeFieldsByMergeFieldGroupIdByDocumentIdsByCid( $intMergeFieldGroupId, $arrintDocumentIds, $intCid, $objDatabase ) {
		if( false == valId( $intMergeFieldGroupId ) || false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						document_merge_fields
					WHERE
						cid = ' . ( int ) $intCid . '
						AND merge_field_group_id = ' . ( int ) $intMergeFieldGroupId . '
						AND document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND deleted_by IS NULL 
						AND deleted_on IS NULL';

		return self::fetchDocumentMergeFields( $strSql, $objDatabase );
	}

	public static function fetchCurrencyFormatApplyDocumentMergeFieldsByDocumentIdByNullMasterDocumentId( $intDocumentId, $intCid, $objDatabase ) {
		if( false == valId( $intDocumentId ) ) return [];

		$strSql = 'SELECT
					*
					FROM
					documents d
					JOIN document_addendas da ON ( d.cid = da.cid AND d.id = da.document_id AND master_document_id IS NULL )
					JOIN document_merge_fields dmf ON ( d.cid = dmf.cid AND d.id = dmf.document_id AND dmf.document_addenda_id = da.id AND dmf.deleted_on IS NULL )
					WHERE
					d.cid = ' . ( int ) $intCid . '
					AND d.document_id = ' . ( int ) $intDocumentId . ' order by is_block_field DESC';

		$arrobjDocumentMergeFields = self::fetchDocumentMergeFields( $strSql, $objDatabase );

		$arrstrCurrencyFormatMergeFields = [];

		$arrintCurrencyBlockMergeFields = [];

		if( true == valArr( $arrobjDocumentMergeFields ) ) {

			foreach( $arrobjDocumentMergeFields as $objDocumentMergeField ) {

				if( CCompanyMergeField::DATA_TYPE_CURRENCY == $objDocumentMergeField->getDataType() ) {

					if( 1 == $objDocumentMergeField->getHasFormatting() ) {
						if( 1 == $objDocumentMergeField->getIsBlockField() ) {
							$arrintCurrencyBlockMergeFields[$objDocumentMergeField->getId()] = 1;
						}
						$arrstrCurrencyFormatMergeFields[$objDocumentMergeField->getField()] = 1;
						continue;
					}
					if( true == array_key_exists( $objDocumentMergeField->getBlockDocumentMergeFieldId(), $arrintCurrencyBlockMergeFields ) ) {
						$arrstrCurrencyFormatMergeFields[$objDocumentMergeField->getField()] = 1;
					}
				}
			}
		}

		return $arrstrCurrencyFormatMergeFields;
	}

}
?>