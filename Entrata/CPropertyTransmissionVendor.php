<?php

class CPropertyTransmissionVendor extends CBasePropertyTransmissionVendor {

	protected $m_objCompanyTransmissionVendor;
	protected $m_strCompanyTransmissionVendorName;
	protected $m_intTransmissionVendorId;

    /**
     * Get Functions
     */

    public function getCompanyTransmissionVendor() {
    	return $this->m_objCompanyTransmissionVendor;
    }

    public function getCompanyTransmissionVendorName() {
    	return $this->m_strCompanyTransmissionVendorName;
    }

    public function getTransmissionVendorId() {
    	return $this->m_intTransmissionVendorId;
    }

    /**
     * Set Functions
     */

    public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet );

    	if( isset( $arrstrValues['company_transmission_vendor_name'] ) && $boolDirectSet )
    		$this->m_strCompanyTransmissionVendorName = trim( $arrstrValues['company_transmission_vendor_name'] );
    	elseif( isset( $arrstrValues['company_transmission_vendor_name'] ) ) {
    		$this->setCompanyTransmissionVendorName( $arrstrValues['company_transmission_vendor_name'] );
        }

    	if( isset( $arrstrValues['transmission_vendor_id'] ) )
    		$this->setTransmissionVendorId( $arrstrValues['transmission_vendor_id'] );

    	return;
    }

    public function setCompanyTransmissionVendor( $objCompanyTransmissionVendor ) {
    	$this->m_objCompanyTransmissionVendor = $objCompanyTransmissionVendor;
    }

    public function setCompanyTransmissionVendorName( $strCompanyTransmissionVendorName ) {
    	$this->m_strCompanyTransmissionVendorName = $strCompanyTransmissionVendorName;
    }

    public function setTransmissionVendorId( $intTransmissionVendorId ) {
    	return $this->m_intTransmissionVendorId = $intTransmissionVendorId;
    }

    /**
     * Get Or Fetch Functions
     */

    public function getOrFetchCompanyTransmissionVendor( $objDatabase ) {
        if( false == valObj( $this->m_objCompanyTransmissionVendor, 'CCompanyTransmissionVendor' ) ) {
            $this->m_objCompanyTransmissionVendor = $this->fetchCompanyTransmissionVendor( $objDatabase );
        }

        return $this->m_objCompanyTransmissionVendor;
    }

    /**
     * Fetch Functions
     */

    public function fetchCompanyTransmissionVendor( $objDatabase ) {
    	return \Psi\Eos\Entrata\CCompanyTransmissionVendors::createService()->fetchCompanyTransmissionVendorByIdByCid( $this->getCompanyTransmissionVendorId(), $this->getCid(), $objDatabase );
    }

    public function valCid() {
        $boolIsValid = true;
        if( true == is_null( $this->getCid() ) ) {
           $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCompanyTransmissionVendorId() {
        $boolIsValid = true;
        if( true == is_null( $this->getCompanyTransmissionVendorId() ) || $this->getCompanyTransmissionVendorId() == '' ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Company transmission vendor id is required.' ) );
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valPropertyTransmissionVendorKey() {
        $boolIsValid = true;

        if( false == valStr( $this->getKey() ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_transmission_vendor_id', 'Property Transmission vendor key  or serial number is required.' ) );
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	// $boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valCompanyTransmissionVendorId();
                $boolIsValid &= $this->valPropertyTransmissionVendorKey();
                break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// need to comment
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

     public function fetchRequiredTransmissionFields( $objDatabase ) {

        $this->getOrFetchCompanyTransmissionVendor( $objDatabase );

        if( false == valObj( $this->m_objCompanyTransmissionVendor, 'CCompanyTransmissionVendor' ) ) return NULL;

        switch( $this->m_objCompanyTransmissionVendor->getTransmissionVendorId() ) {
			case CTransmissionVendor::SAFERENT:
                $arrstrRequiredData['name_first']           = 'name_first';
                $arrstrRequiredData['name_last']            = 'name_last';
                $arrstrRequiredData['tax_number']           = 'tax_number';
                $arrstrRequiredData['birth_date']           = 'birth_date';
                $arrstrRequiredData['current_state_code']   = 'current_state_code';
                $arrstrRequiredData['current_city']         = 'current_city';
                $arrstrRequiredData['current_postal_code']  = 'current_postal_code';
				break;

			case CTransmissionVendor::ONSITE:
                $arrstrRequiredData['name_first']           = 'name_first';
                $arrstrRequiredData['name_last']            = 'name_last';
                $arrstrRequiredData['tax_number']           = 'tax_number';
                $arrstrRequiredData['birth_date']           = 'birth_date';
                $arrstrRequiredData['gross_income']         = 'gross_income';
                // $arrstrRequiredData['lease_term']         = 'lease_term';
                $arrstrRequiredData['current_state_code']   = 'current_state_code';
                $arrstrRequiredData['current_city']         = 'current_city';
                $arrstrRequiredData['current_postal_code']  = 'current_postal_code';
                break;

            case CTransmissionVendor::FIRST_ADVANTAGE:
                $arrstrRequiredData['name_first']           = 'name_first';
                $arrstrRequiredData['name_last']            = 'name_last';
                $arrstrRequiredData['tax_number']           = 'tax_number';
                $arrstrRequiredData['birth_date']           = 'birth_date';
                $arrstrRequiredData['gross_income']         = 'gross_income';
                // $arrstrRequiredData['lease_term']             = 'lease_term';
                $arrstrRequiredData['current_state_code']   = 'current_state_code';
                $arrstrRequiredData['current_city']         = 'current_city';
                $arrstrRequiredData['current_postal_code']  = 'current_postal_code';
                break;

            case CTransmissionVendor::RESIDENT_CHECK:
                $arrstrRequiredData['name_first']           = 'name_first';
                $arrstrRequiredData['name_last']            = 'name_last';
                $arrstrRequiredData['tax_number']           = 'tax_number';
                $arrstrRequiredData['birth_date']           = 'birth_date';
                $arrstrRequiredData['gross_income']         = 'gross_income';
                // $arrstrRequiredData['lease_term']         = 'lease_term';
                $arrstrRequiredData['current_state_code']   = 'current_state_code';
                $arrstrRequiredData['current_city']         = 'current_city';
                $arrstrRequiredData['current_postal_code']  = 'current_postal_code';
                break;

            case CTransmissionVendor::RENT_GROW:
                $arrstrRequiredData['name_first']                    = 'name_first';
                $arrstrRequiredData['name_last']                     = 'name_last';
                $arrstrRequiredData['tax_number']                    = 'tax_number';
                $arrstrRequiredData['birth_date']                    = 'birth_date';
                $arrstrRequiredData['gross_income']                  = 'gross_income';
                $arrstrRequiredData['current_state_code']            = 'current_state_code';
                $arrstrRequiredData['current_city']                  = 'current_city';
                $arrstrRequiredData['current_postal_code']           = 'current_postal_code';
                $arrstrRequiredData['current_move_in_date']          = 'current_move_in_date';
                $arrstrRequiredData['current_move_out_date']         = 'current_move_out_date';
                $arrstrRequiredData['current_employment_start_date'] = 'current_employment_start_date';
                break;

            case CTransmissionVendor::TRANSUNION_CREDIT_RETRIEVER_CR6:
                $arrstrRequiredData['name_first']                    = 'name_first';
                $arrstrRequiredData['name_last']                     = 'name_last';
                $arrstrRequiredData['tax_number']                    = 'tax_number';
                $arrstrRequiredData['birth_date']                    = 'birth_date';
                $arrstrRequiredData['gross_income']                  = 'gross_income';
                $arrstrRequiredData['lease_term']                    = 'lease_term';
                $arrstrRequiredData['current_state_code']            = 'current_state_code';
                $arrstrRequiredData['current_city']                  = 'current_city';
                $arrstrRequiredData['current_postal_code']           = 'current_postal_code';
                break;

			case CTransmissionVendor::RENTAL_HISTORY_REPORTS:
    			$arrstrRequiredData['name_first'] 				    	= 'name_first';
		    	$arrstrRequiredData['name_last'] 				    	= 'name_last';
		    	$arrstrRequiredData['tax_number'] 				    	= 'tax_number';
		    	$arrstrRequiredData['birth_date'] 				    	= 'birth_date';
		    	$arrstrRequiredData['current_street_line1']		    	= 'current_street_line1';
		    	$arrstrRequiredData['current_street_line2'] 	    	= 'current_street_line2';
		    	$arrstrRequiredData['current_street_line3']		    	= 'current_street_line3';
		    	$arrstrRequiredData['current_state_code']		    	= 'current_state_code';
		    	$arrstrRequiredData['current_city'] 			    	= 'current_city';
		    	$arrstrRequiredData['current_postal_code'] 		    	= 'current_postal_code';
		    	$arrstrRequiredData['gross_income'] 					= 'gross_income';
		    	$arrstrRequiredData['enable_income_options']			= 'enable_income_options';
		    	break;

		   	case CTransmissionVendor::SOUND_SCREENING:
		   		$arrstrRequiredData['name_first']                    	= 'name_first';
		   		$arrstrRequiredData['name_last']                     	= 'name_last';
		   		$arrstrRequiredData['tax_number']                    	= 'tax_number';
		   		$arrstrRequiredData['birth_date']                    	= 'birth_date';
		   		$arrstrRequiredData['current_street_line1']		    	= 'current_street_line1';
		   		$arrstrRequiredData['current_street_line2'] 	    	= 'current_street_line2';
		   		$arrstrRequiredData['current_street_line3']		    	= 'current_street_line3';
		   		$arrstrRequiredData['current_state_code']		    	= 'current_state_code';
		   		$arrstrRequiredData['current_city'] 			    	= 'current_city';
		   		$arrstrRequiredData['current_postal_code'] 		    	= 'current_postal_code';
		   		$arrstrRequiredData['current_move_in_date']				= 'current_move_in_date';
		   		$arrstrRequiredData['current_move_out_date']			= 'current_move_out_date';
		    	break;

		    case CTransmissionVendor::SCREENING_REPORTS:
	   			$arrstrRequiredData['name_first']           = 'name_first';
   				$arrstrRequiredData['name_last']            = 'name_last';
   				$arrstrRequiredData['tax_number']           = 'tax_number';
   				$arrstrRequiredData['birth_date']           = 'birth_date';
   				$arrstrRequiredData['current_state_code']   = 'current_state_code';
   				$arrstrRequiredData['current_city']         = 'current_city';
   				$arrstrRequiredData['current_postal_code']  = 'current_postal_code';
   				break;

   			case CTransmissionVendor::VERIFIRST:
   				$arrstrRequiredData['name_first']           = 'name_first';
   				$arrstrRequiredData['name_last']            = 'name_last';
   				$arrstrRequiredData['tax_number']           = 'tax_number';
   				$arrstrRequiredData['birth_date']           = 'birth_date';
   				$arrstrRequiredData['current_state_code']   = 'current_state_code';
   				$arrstrRequiredData['current_city']         = 'current_city';
   				$arrstrRequiredData['current_postal_code']  = 'current_postal_code';
   				break;

   			case CTransmissionVendor::LEASINGDESK:
   				$arrstrRequiredData['name_first']           = 'name_first';
   				$arrstrRequiredData['name_last']            = 'name_last';
   				$arrstrRequiredData['tax_number']           = 'tax_number';
   				$arrstrRequiredData['birth_date']           = 'birth_date';
   				$arrstrRequiredData['current_state_code']   = 'current_state_code';
   				$arrstrRequiredData['current_city']         = 'current_city';
   				$arrstrRequiredData['current_postal_code']  = 'current_postal_code';
   				break;

   			case CTransmissionVendor::BACKGROUND_INVESTIGATIONS:
   				$arrstrRequiredData['name_first']           = 'name_first';
   				$arrstrRequiredData['name_last']            = 'name_last';
   				$arrstrRequiredData['tax_number']           = 'tax_number';
   				$arrstrRequiredData['birth_date']           = 'birth_date';
   				$arrstrRequiredData['current_state_code']   = 'current_state_code';
   				$arrstrRequiredData['current_city']         = 'current_city';
   				$arrstrRequiredData['current_postal_code']  = 'current_postal_code';
   				break;

            case CTransmissionVendor::REALID:
	        case CTransmissionVendor::GENERIC_SCREENING_LIBRARY:
	            $arrstrRequiredData['name_first']           = 'name_first';
	            $arrstrRequiredData['name_last']            = 'name_last';
	            $arrstrRequiredData['tax_number']           = 'tax_number';
	            $arrstrRequiredData['birth_date']           = 'birth_date';
	            $arrstrRequiredData['gross_income']         = 'gross_income';
	            $arrstrRequiredData['current_state_code']   = 'current_state_code';
	            $arrstrRequiredData['current_city']         = 'current_city';
	            $arrstrRequiredData['current_postal_code']  = 'current_postal_code';
	            break;

            default:
                $arrstrRequiredData = NULL;
        }

        return $arrstrRequiredData;
     }

}
?>