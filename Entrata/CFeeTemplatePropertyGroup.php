<?PHP

class CFeeTemplatePropertyGroup extends CBaseFeeTemplatePropertyGroup {

	protected $m_intIsDisabled;
	protected $m_intPropertyId;

	protected $m_strPropertyName;
	protected $m_strFeeTemplateName;
	protected $m_strPaymentPostMonth;

	protected $m_intBankAccountId;

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getFeeTemplateName() {
		return $this->m_strFeeTemplateName;
	}

	public function getPaymentPostMonth() {
		return $this->m_strPaymentPostMonth;
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setFeeTemplateName( $strFeeTemplateName ) {
		$this->m_strFeeTemplateName = CStrings::strTrimDef( $strFeeTemplateName, 50, NULL, true );
	}

	public function setPaymentPostMonth( $strPaymentPostMonth ) {

		if( true == valStr( $strPaymentPostMonth ) ) {

			$arrstrPaymentPostMonth = explode( '/', $strPaymentPostMonth );

			if( true == valArr( $arrstrPaymentPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPaymentPostMonth ) ) {
				$strPaymentPostMonth = $arrstrPaymentPostMonth[0] . '/01/' . $arrstrPaymentPostMonth[1];
			}
		}

		$this->m_strPaymentPostMonth = $strPaymentPostMonth;
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->m_intIsDisabled = CStrings::strToIntDef( $intIsDisabled, NULL, false );
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->m_intBankAccountId = $intBankAccountId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['fee_template_name'] ) ) $this->setFeeTemplateName( $arrmixValues['fee_template_name'] );
		if( true == isset( $arrmixValues['is_disabled'] ) ) $this->setIsDisabled( $arrmixValues['is_disabled'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		return;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFeeTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>