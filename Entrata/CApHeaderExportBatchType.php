<?php

class CApHeaderExportBatchType extends CBaseApHeaderExportBatchType {

	const CAPITAL_ONE                   = 1;
	const KEY_BANK                      = 2;
	const BBT_BANK                      = 3;
	const JP_MORGAN_CHASE               = 4;
	const WELLS_FARGO                   = 5;
	const WASHINGTON_FIRST              = 6;
	const EAST_WEST_BANK                = 7;
	const FROST_BANK                    = 8;
	const COM_DATA_BANK                 = 9;
	const TD_BANK                       = 10;
	const BANK_OF_AMERICA               = 11;
	const WASHINGTON_TRUST              = 12;
	const US_BANK                       = 13;
	const PNC_BANK                      = 14;
	const BMO_HARRIS_BANK               = 15;
	const OTHER_BANKS                   = 16;
	const CITIZENS_BANK                 = 17;
	const COLORADO_STATE_BANK_AND_TRUST = 18;
	const PRIVATE_BANK_CIBC             = 19;
	const CITY_NATIONAL_BANK            = 20;
	const VECTRA_BANK                   = 21;
	const CITI_BANK                     = 22;
	const CENTRAL_BANK_OF_BOONE_COUNTY  = 23;
	const FIRST_MERCHANT_BANK           = 24;
	const CADENCE_BANK                  = 25;
	const MORTON_COMMUNITY_BANK         = 26;
	const FIRST_BANK                    = 27;
	const CITYWIDE_BANK                 = 28;
	const IBERIA_BANK                   = 29;
	const HUNTINGTON_BANK               = 30;
	const FIFTH_THIRD_BANK              = 31;
	const FIRST_COMMONWEALTH            = 32;


	public static $c_arrintCsvExtentionApHeaderExportBatchTypes = [
		SELF::OTHER_BANKS         => CApHeaderExportBatchType::OTHER_BANKS,
		SELF::VECTRA_BANK         => CApHeaderExportBatchType::VECTRA_BANK,
		SELF::CITI_BANK           => CApHeaderExportBatchType::CITI_BANK,
		SELF::FIRST_MERCHANT_BANK => CApHeaderExportBatchType::FIRST_MERCHANT_BANK,
		SELF::CADENCE_BANK        => CApHeaderExportBatchType::CADENCE_BANK,
		SELF::FIRST_BANK          => CApHeaderExportBatchType::FIRST_BANK,
	];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDontAllowMultipleBank() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>