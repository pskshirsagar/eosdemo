<?php

class CCompanyCategory extends CBaseCompanyCategory {

	/**
	* Fetch Functions
	*
	*/

	public function fetchPropertyCategoryIdByPropertyId( $intPropertyId, $objDatabase ) {

		return CPropertyCategories::fetchPropertyCategoryByCompanyCategoryIdByPropertyIdByCid( $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyCategories( $objDatabase ) {

		return CPropertyCategories::fetchPropertyCategoriesByCompanyCategoryIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	* Validation Functions
	*
	*/

    public function valName( $objDatabase = NULL ) {
        $boolIsValid = true;

        if( 3 > mb_strlen( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Category name must be at least {%d, 0} characters.', [ 3 ] ) ) );
        }

        if( true == $boolIsValid && true == isset( $objDatabase ) ) {

        	$strWhereConditions = ' WHERE
										cid = ' . ( int ) $this->getCid() . '
										AND LOWER( name ) = \'' . trim( addslashes( mb_strtolower( $this->getName() ) ) ) . '\'' .
        								( ( 0 < $this->getId() ) ? ' AND id <> ' . $this->getId() : '' );

        	$intCompanyCategoriesCount = CCompanyCategories::fetchCompanyCategoryCount( $strWhereConditions, $objDatabase );
        	if( 0 < $intCompanyCategoriesCount ) {
        		$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( ' The name you entered is already being used.' ) ) );
        	}
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

        // if( false == isset( $this->m_strDescription ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        // if( false == isset( $this->m_intIsPublished ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>