<?php

class CWebsiteDomainsCertificate extends CBaseWebsiteDomainsCertificate {

	protected $m_intCertificateType;

	/**
	 * Get Functions
	 *
	 */

	public function getCertificateType() {
		return $this->m_intCertificateType;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['certificate_type'] ) ) 	$this->setCertificateType( $arrmixValues['certificate_type'] );
	}

	public function setCertificateType( $intCertificateType ) {
		$this->m_intCertificateType = $intCertificateType;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>