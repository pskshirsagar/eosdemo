<?php

class CApPaymentBatch extends CBaseApPaymentBatch {

	protected $m_strApPaymentBatchStatus;

	const ACTIVE 				= 1;
	const VOIDED 				= 2;
	const PARTIALLY_VOIDED 		= 3;
	const ARCHIVED				= 4;

	public function getApPaymentBatchStatus() {
		return $this->m_strApPaymentBatchStatus;
	}

	public function setApPaymentBatchStatus( $strApPaymentBatchStatus ) {
		$this->m_strApPaymentBatchStatus = CStrings::strTrimDef( $strApPaymentBatchStatus, 50, NULL, true );
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostMonth() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valControlTotal() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valControlCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBatchDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

    // Override setPostMonth function. It will acccept date in dd/yyyy format and set it to dd/01/yyyy format
    public function setPostMonth( $strPostMonth ) {

    	if( true == valStr( $strPostMonth ) ) {

    		$arrstrPostMonth = explode( '/', $strPostMonth );

    		if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
    			$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
    		}
    	}

    	$this->m_strPostMonth = $strPostMonth;

    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_payment_batch_status'] ) ) $this->setApPaymentBatchStatus( $arrmixValues['ap_payment_batch_status'] );
	}

}
?>