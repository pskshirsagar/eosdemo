<?php

class CApExportConfiguration extends CBaseApExportConfiguration {

	public function valApExportBatchId() {
		$boolIsValid = true;
		if( true == is_null( $this->getApExportBatchId() ) || 0 >= ( int ) $this->getApExportBatchId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_export_batch_type_id', 'Export Type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		$arrintExplodedPostMonthDate = explode( '/', $this->getPostMonth() );
		if( 2 == \Psi\Libraries\UtilFunctions\count( $arrintExplodedPostMonthDate ) ) {
			$strPostMonthDate = $arrintExplodedPostMonthDate[0] . '/1/' . $arrintExplodedPostMonthDate[1];
		}

		if( false == valStr( $this->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is required.' ) );
		} elseif( false == preg_match( '/^[0-9]{2}\/[0-9]{4}$/', $this->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month of format mm/yyyy is required.' ) );
		} elseif( false == is_null( $strPostMonthDate ) && false == CValidation::validateDate( $strPostMonthDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is invalid.' ) );
		}
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		if( false == valStr( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start Date is required.' ) );
		}
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		if( false == valStr( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End  Date is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			$boolIsValid &= $this->valApExportBatchId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>