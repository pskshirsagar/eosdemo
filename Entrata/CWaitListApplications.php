<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWaitListApplications
 * Do not add any new functions to this class.
 */

class CWaitListApplications extends CBaseWaitListApplications {

	public static function fetchWaitListApplicationByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		return self::fetchWaitListApplication( sprintf( 'SELECT * FROM wait_list_applications WHERE application_id = %d AND cid = %d', ( int ) $intApplicationId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchWaitListApplicationWithRejectionLimitByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
			            wa.*,
			            w.rejection_limit
			       FROM
			            wait_list_applications wa
			              join wait_lists w ON ( wa.cid = w.cid and wa.wait_list_id = w.id )
			       WHERE
			            wa.cid = ' . ( int ) $intCid . '
			              and wa.application_id = ' . ( int ) $intApplicationId;

		return self::fetchWaitListApplication( $strSql, $objDatabase );
	}

}
?>