<?php

class CPropertyConciergeService extends CBasePropertyConciergeService {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( false == isset( $this->m_intId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example

        // if( false == isset( $this->m_intCid ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        // Validation example

        // if( false == isset( $this->m_intPropertyId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyConciergeServiceId() {
        $boolIsValid = true;

        // Validation example

        // if( false == isset( $this->m_intCompanyConciergeServiceId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_concierge_service_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        // Validation example

        // if( false == isset( $this->m_intOrderNum ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

        // Validation example

        // if( false == isset( $this->m_intCreatedBy ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        // Validation example

        // if( false == isset( $this->m_strCreatedOn ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>