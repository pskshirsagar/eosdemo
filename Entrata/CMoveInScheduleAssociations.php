<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoveInScheduleAssociations
 * Do not add any new functions to this class.
 */

class CMoveInScheduleAssociations extends CBaseMoveInScheduleAssociations {

	public static function fetchLeaseApprovalDateTypeMoveInScheduleAssociationWithCalculatedCapSizeByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase, $intMoveInSchedulerId = NULL ) {

		$strWhere = ' ';
		if( true == valId( $intMoveInSchedulerId ) ) {
			$strWhere = ' AND mis.id = ' . ( int ) $intMoveInSchedulerId . ' ';
		}
		$strSql = 'SELECT mis.id,
			              mis.scheduled_move_in_date,
					      mis.scheduled_move_in_end_time,
					      mis.scheduled_move_in_start_time,
			              SUM( misa_previous.cap_size ) as sum_of_previous_cap_size
			       FROM move_in_schedules mis
			            JOIN move_in_schedule_group_lease_terms misglt ON ( mis.cid = misglt.cid AND mis.move_in_schedule_group_id = misglt.move_in_schedule_group_id )
			            JOIN move_in_schedule_associations misa ON ( mis.cid = misa.cid AND mis.property_id = misa.property_id AND mis.id = misa.move_in_schedule_id )
			            JOIN move_in_schedules mis_previous ON ( mis.cid = mis_previous.cid AND mis.property_id = mis_previous.property_id AND mis_previous.id <= mis.id AND mis.move_in_schedule_group_id = mis_previous.move_in_schedule_group_id )
			            JOIN move_in_schedule_associations misa_previous ON ( mis_previous.cid = misa_previous.cid AND mis_previous.property_id = misa_previous.property_id AND mis_previous.id = misa_previous.move_in_schedule_id )
			       WHERE mis.cid = ' . ( int ) $intCid . '
			             AND mis.property_id = ' . ( int ) $intPropertyId . '
			             AND misglt.lease_term_id = ' . ( int ) $intLeaseTermId . '
			             AND misglt.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
                         AND misglt.deleted_by IS NULL 
			             AND misa.deleted_on IS NULL
			             AND misa_previous.deleted_on IS NULL
                         AND mis.deleted_by IS NULL
                         AND mis_previous.deleted_by IS NULL ' .
		          $strWhere . '
			       GROUP BY mis.cid,
			                mis.id,
			                mis.scheduled_move_in_date,
					        mis.scheduled_move_in_end_time,
					        mis.scheduled_move_in_start_time
					 ORDER BY        
					        mis.id';

		return $arrmixResult = fetchData( $strSql, $objDatabase );

	}

	public static function fetchMoveInScheduleAssociationsByMoveInScheduleIdsByCidByPropertyId( $arrintMoveInScheduleIds, $intCid, $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT * FROM move_in_schedule_associations 
						WHERE move_in_schedule_id IN ( ' . implode( ',', $arrintMoveInScheduleIds ) . ' )
							AND cid = ' . ( int ) $intCid . '
							AND property_id = ' . ( int ) $intPropertyId . '
							AND deleted_by IS NULL 
							AND deleted_on IS NULL';

		return self::fetchMoveInScheduleAssociations( $strSql, $objDatabase );
	}

	public static function fetchMoveInScheduleAssociationByMoveInSchedulerTypeIdByLeaseTermIdByLeaseStartWindowIdByReferenceIdByPropertyIdByCid( $intMoveInScheduleTypeId, $intLeaseTermId, $intLeaseStartWindowId, $intReferenceId, $intPropertyId, $intCid, $objDatabase, $intMoveInSchedulerId = NULL ) {
		if( false == valId( $intMoveInScheduleTypeId ) || false == valId( $intReferenceId ) ) {
			return NULL;
		}

		$strWhereCondition = $strSelect = ' ';
		if( true == valId( $intMoveInSchedulerId ) ) {
			$strWhereCondition .= ' AND mis.id = ' . ( int ) $intMoveInSchedulerId . ' ';
		}
		if( $intMoveInScheduleTypeId == CMoveInScheduleType::BUILDING ) {
			$strJoinCondition  = ' JOIN property_units pu ON ( msa.cid = pu.cid AND msa.property_building_id = pu.property_building_id )';
			$strWhereCondition .= ' AND pu.id = ' . ( int ) $intReferenceId;
		} elseif( $intMoveInScheduleTypeId == CMoveInScheduleType::FLOOR ) {
			$strJoinCondition  = ' JOIN property_units pu ON ( msa.cid = pu.cid AND msa.property_id = pu.property_id AND msa.property_floor_id = pu.property_floor_id )';
			$strWhereCondition .= ' AND pu.id = ' . ( int ) $intReferenceId;
		} elseif( $intMoveInScheduleTypeId == CMoveInScheduleType::FLOORPLAN ) {
			$strWhereCondition .= ' AND msa.property_floorplan_id = ' . ( int ) $intReferenceId;
		}

		if( $intMoveInScheduleTypeId <> CMoveInScheduleType::PROPERTY ) {
			$strSelect         .= ' ,mis.scheduled_move_in_date';
			$strJoinCondition  .= ' JOIN move_in_schedule_group_lease_terms misglt ON ( misg.cid = misglt.cid AND misg.id = misglt.move_in_schedule_group_id )';
			$strWhereCondition .= ' AND misglt.deleted_by IS NULL 
	                              AND misglt.lease_term_id = ' . ( int ) $intLeaseTermId . ' 
	                              AND misglt.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId;
		}

		$strSql = 'SELECT
					       mis.scheduled_move_in_end_time,
					       mis.scheduled_move_in_start_time,
					       misg.move_in_schedule_type_id,
					       msa.property_floorplan_id
					       ' . $strSelect . '
					FROM move_in_schedules mis
					     JOIN move_in_schedule_associations msa ON ( mis.cid = msa.cid AND mis.property_id = msa.property_id AND mis.id = msa.move_in_schedule_id )
					     JOIN move_in_schedule_groups misg ON ( mis.cid = misg.cid AND misg.id = mis.move_in_schedule_group_id )

					     ' . $strJoinCondition . '
					WHERE msa.cid = ' . ( int ) $intCid . ' AND
					      msa.property_id = ' . ( int ) $intPropertyId . ' AND
					      msa.deleted_on IS NULL AND
                          misg.deleted_by IS NULL AND
					      misg.move_in_schedule_type_id = ' . ( int ) $intMoveInScheduleTypeId .
		          $strWhereCondition;

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			return $arrmixResult[0];
		}
	}

}
?>
