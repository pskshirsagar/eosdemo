<?php

class CLateNoticeType extends CBaseLateNoticeType {

	protected $m_intDocumentSubTypeId;

	protected $m_strPropertyGroupName;
	protected $m_strDocumentName;
	protected $m_intPropertyId;

    /**
     * Set Functions
     */

    public function setDocumentSubTypeId( $intDocumentSubTypeId ) {
    	$this->m_intDocumentSubTypeId = $intDocumentSubTypeId;
    }

    public function setPropertyGroupName( $strPropertyGroupName ) {
    	$this->m_strPropertyGroupName = $strPropertyGroupName;
    }

    public function setDocumentName( $strDocumentName ) {
    	$this->m_strDocumentName = $strDocumentName;
    }

    public function setDocumentContent( $strDocumentContent ) {
    	$this->m_strDocumentContent = $strDocumentContent;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet );

    	if( true == isset( $arrValues['document_name'] ) ) 			$this->setDocumentName( $arrValues['document_name'] );
    	if( true == isset( $arrValues['document_content'] ) )		$this->setDocumentContent( $arrValues['document_content'] );
    	if( true == isset( $arrValues['document_sub_type_id'] ) ) 	$this->setDocumentSubTypeId( $arrValues['document_sub_type_id'] );
    	if( true == isset( $arrValues['property_group_name'] ) )	$this->setPropertyGroupName( $arrValues['property_group_name'] );
    	if( true == isset( $arrValues['property_id'] ) )			$this->setPropertyId( $arrValues['property_id'] );

    	return;
    }

    /**
     * Get Functions
     */

    public function getDocumentSubTypeId() {
    	return $this->m_intDocumentSubTypeId;
    }

    public function getPropertyGroupName() {
    	return $this->m_strPropertyGroupName;
    }

    public function getDocumentName() {
    	return $this->m_strDocumentName;
    }

    public function getDocumentContent() {
    	return $this->m_strDocumentContent;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public static function fetchMaxOrderNumBySubTypeIdByCid( $intDocumentSubTypeId, $intCid, $objDatabase ) {

    	$strSql = 'SELECT
					    MAX ( lnt.order_num ) AS order_num
					FROM
					    late_notice_types lnt
					    JOIN documents d ON ( lnt.cid = d.cid AND lnt.document_id = d.id )
					WHERE
    					d.document_sub_type_id =' . $intDocumentSubTypeId . '
					    AND lnt.cid = ' . ( int ) $intCid . '
    					AND lnt.deleted_by IS NULL';

    	$arrintResponse		= fetchData( $strSql, $objDatabase );
    	$intMaxOrderNum	= ( true == isset( $arrintResponse[0]['order_num'] ) ) ? $arrintResponse[0]['order_num'] + 1 : 1;

    	return $intMaxOrderNum;
    }
}
?>