<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultHolidays
 * Do not add any new functions to this class.
 */

class CDefaultHolidays extends CBaseDefaultHolidays {

	public static function fetchDefaultHoliday( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultHoliday', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>