<?php

class CApPaymentLog extends CBaseApPaymentLog {

	const ACTION_TYPE_IMPORTED							= 'Imported';
	const ACTION_TYPE_CREATED							= 'Created';
	const ACTION_TYPE_VOIDED							= 'Voided';
	const ACTION_TYPE_REVERSE_VOIDED_PAYMENT			= 'Reverse Voided Payment';
	const ACTION_TYPE_ATTACHED_TO_INVOICE				= 'Attached To Invoice';
	const ACTION_TYPE_REPRINTED_CHECK					= 'Reprinted Check';
	const ACTION_TYPE_REISSUE_PAYMENT					= 'Reissue Payment';
	const ACTION_TYPE_MOVED_TO_UNCLAIMED_PROPERTY		= 'Moved to Unclaimed Property';
	const ACTION_TYPE_REMOVED_FROM_UNCLAIMED_PROPERTY	= 'Removed from Unclaimed Property';
	const ACTION_TYPE_CLEARED_BANK						= 'Cleared Bank';
	const ACTION_TYPE_RETURNED							= 'Returned';
	const ACTION_TYPE_VOIDING							= 'Voiding';
	const ACTION_TYPE_CAPTURED							= 'Captured';
	const ACTION_TYPE_CAPTURING							= 'Capturing';
	const ACTION_TYPE_RECEIVED							= 'Received';
	const ACTION_TYPE_PENDING							= 'Pending';
	const ACTION_TYPE_ATTACHMENT_ADDED					= 'Attachment added';
	const ACTION_TYPE_ATTACHMENT_REMOVED				= 'Attachment removed';
	const ACTION_TYPE_LW_DOCUMENT_ADDED					= 'Lien Waiver Document added';
	const ACTION_TYPE_LW_DOCUMENT_REMOVED				= 'Lien Waiver Document removed';
	const ACTION_TYPE_CHECK_COPY_DOCUMENT_ADDED			= 'Check Copy Document added';

	protected $m_strCompanyEmployeeFullName;

	/**
	 * Get Functions
	 */

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	/**
	 * Set Functions
	 */

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = CStrings::strTrimDef( $strCompanyEmployeeFullName, 100, NULL, true );
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['company_employee_full_name'] ) ) $this->setCompanyEmployeeFullName( $arrValues['company_employee_full_name'] );

		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>