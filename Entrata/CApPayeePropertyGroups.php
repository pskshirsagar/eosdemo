<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeePropertyGroups
 * Do not add any new functions to this class.
 */

class CApPayeePropertyGroups extends CBaseApPayeePropertyGroups {

	public static function fetchApPayeePropertiesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						appg.*,
						pga.property_id
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					WHERE
						appg.cid = ' . ( int ) $intCid;

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdByPropertyIdsByCid( $intApPayeeId, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						appg.*,
						pga.property_id
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND pga.property_id IN ( ' . implode( ',',  $arrintPropertyIds ) . ' )
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId;

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdByPropertyIdsByCid( $intApPayeeId, $intApPayeeLocationId, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name,
						p.is_disabled
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id  = ' . ( int ) $intApPayeeLocationId . '
						AND p.id IN ( ' . implode( ',',  $arrintPropertyIds ) . ' )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name,
						p.is_disabled
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id  = ' . ( int ) $intApPayeeLocationId;

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdsByPropertyIdsByCid( $intApPayeeId, $arrintApPayeeLocationIds, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name,
						p.is_disabled
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND p.id IN ( ' . implode( ',',  $arrintPropertyIds ) . ' )
					ORDER BY
						LOWER( p.property_name )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdsByApPayeeLocationIdsByPropertyIdsByCid( $arrintApPayeeIds, $arrintApPayeeLocationIds, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintApPayeeLocationIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name,
						p.is_disabled
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND appg.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND p.id IN ( ' . implode( ',',  $arrintPropertyIds ) . ' )
					ORDER BY
						LOWER( p.property_name )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdsAndApPayeeLocationIdsByPropertyIdsByCid( $arrmixApPayeeIdsAndLocationIds, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrmixApPayeeIdsAndLocationIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name,
						p.is_disabled
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND ( appg.ap_payee_id, appg.ap_payee_location_id ) IN ( ' . implode( ',', $arrmixApPayeeIdsAndLocationIds ) . ' )
						AND p.id IN ( ' . implode( ',',  $arrintPropertyIds ) . ' )
					ORDER BY
						LOWER( p.property_name )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdsByPropertyIdsByCid( $arrintApPayeeIds, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND pga.property_id IN ( ' . implode( ',',  $arrintPropertyIds ) . ' )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertyGroupsByApPayeeIdByApPayeeLocationIdsByCid( $intApPayeeId, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeId ) || false == valArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						appg.*
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeePropertyGroupsWithPropertyGroupNameByApPayeeIdByApPayeeLocationIdsByCid( $intApPayeeId, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeId ) || false == valArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT pg.name AS property_group_name,
						appg.*
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( appg.cid = pg.cid AND appg.property_group_id = pg.id )
						JOIN property_group_associations pga ON ( pg.id = pga.property_group_id AND pga.cid = appg.cid )
						JOIN properties p ON ( p.id = pga.property_id AND p.cid = appg.cid )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
						AND p.is_disabled = 0
						AND p.is_managerial = 0';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase );

	}

	public static function fetchApPayeePropertyGroupsWithPropertyGroupNameByApPayeeIdByApPayeeLocationIdsByKeyByCid( $intApPayeeId, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeId ) || false == valArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pga.property_id,
						p.property_name AS property_group_name,
						pp.key AS property_preference_key,
						pp.value AS property_preference_value
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( appg.cid = pg.cid AND appg.property_group_id = pg.id )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND appg.property_group_id = pga.property_group_id  )
						JOIN property_preferences pp ON ( pga.cid = pp.cid AND pga.property_id = pp.property_id)
						JOIN properties p ON ( p.id = pp.property_id AND p.cid = pp.cid )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND pp.key IN ( \'REQUIRE_POS_FOR_INVOICES\' )
						AND pp.value IN (\'1\')
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
						AND p.is_disabled = 0
					group by pga.property_id,
						p.property_name,
						pp.key,
						pp.value';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeePropertyGroupsWithPropertyGroupNameByApPayeeIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeId ) || false == is_numeric( $intApPayeeLocationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						appg.*,
						pg.name AS property_group_name,
						gat.account_number || \' : \' || gat.name AS default_gl_account_name
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( appg.cid = pg.cid AND appg.property_group_id = pg.id )
						LEFT JOIN gl_account_trees gat ON ( appg.cid = gat.cid AND appg.default_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeePropertiesByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						appg.*,
						pga.property_id
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_location_id  = ' . ( int ) $intApPayeeLocationId;

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						appg.*,
						pga.property_id
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id  IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						appg.*,
						pga.property_id
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pg.id = pga.property_group_id AND appg.cid = pga.cid )
					WHERE
						appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.cid = ' . ( int ) $intCid;

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertyGroupsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds = getIntValuesFromArr( $arrintApPayeeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						appg.*,
						pg.system_code
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( appg.cid = pg.cid AND appg.property_group_id = pg.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id IN( ' . sqlIntImplode( $arrintApPayeeIds ) . ' )
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchSearchApPayeePropertyDetailsByApPayeeIdByCid( $strPropertyName, $intApPayeeId, $intCid, $objClientDatabase, $intLimit ) {

		$strWhereCondition = '';
		if( false == is_null( $strPropertyName ) ) {
			$strWhereCondition = 'AND (
										lower( pg.name ) LIKE lower( \'%' . addslashes( $strPropertyName ) . '%\' )
									)';
		}

		$strSql = 'SELECT DISTINCT ON ( pga.property_group_id )
						pg.name,
						p.property_name,
						pa.*
					FROM
						property_groups pg
						JOIN ap_payee_property_groups appg ON ( appg.property_group_id = pg.id AND pg.cid = appg.cid  )
						JOIN property_groups pg ON ( pg.cid = ap.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pg.id = pga.property_group_id AND pga.cid = appg.cid  )
						JOIN property_addresses pa ON ( pga.property_id = pa.property_id AND pa.cid = appg.cid AND pa.is_alternate = false )
						JOIN properties p ON ( p.id = pa.property_id AND p.cid = pa.cid )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . $strWhereCondition . '
						AND p.is_disabled = 0
						AND p.is_managerial = 0
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
					ORDER BY pga.property_group_id ASC
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertyGroupsByApPayeeIdsByPropertyGroupIdsByApPayeeLocationIdsByCid( $arrintApPayeeIds, $arrintPropertyGroupIds, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						appg.*,
						pg.system_code
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( appg.cid = pg.cid AND appg.property_group_id = pg.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id IN( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND appg.ap_payee_location_id IN( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND appg.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdsByCid( $intApPayeeId, $arrintApPayeeLocationIds, $intCid, $objClientDatabase, $boolIsAllowDisabledProperties = true ) {

		if( false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strWhere = '';

		if( false == $boolIsAllowDisabledProperties ) {
			$strWhere = ' AND p.is_disabled = 0';
		}

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name,
						p.is_disabled
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						' . $strWhere . '
					ORDER BY
						LOWER( p.property_name )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchActiveApPayeePropertyIdsByCidByApPayeeIdsByPropertyIds( $intCid, $objClientDatabase, $arrintApPayeeIds = [], $arrintPropertyIds = [], $arrintApLegalEntityIds = [] ) {

		$strWhereCondition = '';

		if( true == valIntArr( $arrintApPayeeIds = array_filter( ( array ) $arrintApPayeeIds ) ) ) $strWhereCondition .= ' AND appg.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )';
		if( true == valIntArr( $arrintPropertyIds = array_filter( ( array ) $arrintPropertyIds ) ) ) $strWhereCondition .= ' AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		if( true == valIntArr( $arrintApLegalEntityIds = array_filter( ( array ) $arrintApLegalEntityIds ) ) ) $strWhereCondition .= ' AND apl.ap_legal_entity_id IN ( ' . implode( ',', $arrintApLegalEntityIds ) . ' )';

		$strAllPropertyGroupId = 'SELECT id FROM property_groups pg WHERE cid = ' . ( int ) $intCid . ' AND system_code = \'ALL\' AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL LIMIT 1';

		$strSql = 'SELECT
						appg.cid,
						appg.ap_payee_id,
						ale.id,
						array_to_string(array_agg( pga.property_id ), \',\') as property_ids
					FROM
						ap_payee_property_groups appg
						JOIN ap_legal_entities ale ON ( appg.cid = ale.cid AND appg.ap_payee_id = ale.ap_payee_id )
						JOIN ap_payee_locations apl ON ( apl.cid = ale.cid AND apl.ap_payee_id = ale.ap_payee_id AND apl.ap_legal_entity_id = ale.id AND apl.id = appg.ap_payee_location_id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND ( pg.id = pga.property_group_id OR appg.property_group_id = ( ' . $strAllPropertyGroupId . ' ) ) )
						JOIN properties p ON (p.cid = pga.cid AND p.id = pga.property_id AND p.is_disabled = 0 AND p.is_managerial = 0 )
					WHERE
						appg.cid = ' . ( int ) $intCid . $strWhereCondition . '
					GROUP BY
						appg.cid,
						appg.ap_payee_id,
						ale.id';

		return fetchData( $strSql, $objClientDatabase, false );
	}

	public static function fetchActivePropertiesByApPayeeLocationIdsByApPayeeIdByCid( $arrintApPayeeLocationIds, $intApPayeeId, $intCid, $objDatabase, $arrintAllowedProperties = NULL ) {

		if( false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strWhere = '';

		if( true == valArr( $arrintAllowedProperties ) ) {
			$strWhere = ' AND p.id IN ( ' . sqlIntImplode( $arrintAllowedProperties ) . ' )';
		}

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						p.is_disabled = 0
						AND appg.cid =' . ( int ) $intCid . '
						AND appg.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND appg.ap_payee_location_id IN ( ' . implode( ',',	$arrintApPayeeLocationIds ) . ' )
						' . $strWhere;

		return fetchData( $strSql, $objDatabase, false );
	}

	public static function fetchActivePropertiesByApPayeeLocationIdsByApPayeeIdsByCIds( $arrintApPayeeLocationId, $arrintApPayeeId, $arrintCId, $objDatabase ) {
		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrintApPayeeLocationId = getIntValuesFromArr( $arrintApPayeeLocationId ) ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintApPayeeId = getIntValuesFromArr( $arrintApPayeeId ) ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintCId = getIntValuesFromArr( $arrintCId ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name
					FROM
						ap_payee_property_groups appg
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND appg.property_group_id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						p.is_disabled = 0
						AND appg.cid = ANY ( array [ ' . sqlIntImplode( $arrintCId ) . ' ] )
						AND appg.ap_payee_id = ANY ( array [ ' . sqlIntImplode( $arrintApPayeeId ) . ' ] )
						AND appg.ap_payee_location_id = ANY ( array [ ' . sqlIntImplode( $arrintApPayeeLocationId ) . ' ] )';

		return fetchData( $strSql, $objDatabase, false );
	}

	public static function fetchPropertiesByApPayeeIdByCidByApPayeeLegalEntityIds( $intApPayeeId, $intCid, $objClientDatabase, $arrintApPayeeLegalEntityIds = NULL, $arrintAllowedProperties = NULL ) {

		$strSql = 'SELECT
 						pga.property_id,
 						ale.id,
 						p.property_name
 					FROM
 						ap_payee_property_groups appg
 						JOIN ap_legal_entities ale ON ( appg.cid = ale.cid AND appg.ap_payee_id = ale.ap_payee_id )
						JOIN ap_payee_locations apl ON ( apl.cid = ale.cid AND apl.ap_payee_id = ale.ap_payee_id AND apl.ap_legal_entity_id = ale.id AND apl.id = appg.ap_payee_location_id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
 						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
 						JOIN properties p ON ( p.cid = pga.cid AND pga.property_id = p.id )
 					WHERE
 						appg.ap_payee_id = ' . ( int ) $intApPayeeId . '

 						AND appg.cid = ' . ( int ) $intCid . '
 						AND p.is_disabled = 0
 						AND p.is_managerial = 0 ';

		if( true == valArr( $arrintApPayeeLegalEntityIds ) ) {
			$strSql .= ' AND apl.ap_legal_entity_id IN( ' . implode( ',', $arrintApPayeeLegalEntityIds ) . ' )';
		}

		if( true == valArr( $arrintAllowedProperties ) ) {
			$strSql .= ' AND p.id IN ( ' . sqlIntImplode( $arrintAllowedProperties ) . ' )';
		}

		$strSql .= ' GROUP BY
						pga.property_id,
 						ale.id,
 						p.property_name
 					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertiesByApPayeeLegalEntityIdsByApPayeeIdsByCids( $arrintApPayeeLegalEntityIds, $arrintApPayeeIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeLegalEntityIds ) || false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						appg.cid,
						ale.id,
						pga.property_id,
						p.property_name
 					FROM
 						ap_payee_property_groups appg
 						JOIN ap_legal_entities ale ON ( appg.cid = ale.cid AND appg.ap_payee_id = ale.ap_payee_id )
						JOIN ap_payee_locations apl ON ( apl.cid = ale.cid AND apl.ap_payee_id = ale.ap_payee_id AND apl.ap_legal_entity_id = ale.id AND apl.id = appg.ap_payee_location_id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
 						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
 						JOIN properties p ON ( p.cid = pga.cid AND pga.property_id = p.id )
 					WHERE
 						appg.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
 						AND apl.ap_legal_entity_id IN( ' . implode( ',', $arrintApPayeeLegalEntityIds ) . ' )
 						AND appg.cid IN ( ' . implode( ',', $arrintCids ) . ' )
 						AND p.is_disabled = 0
 						AND p.is_managerial = 0 ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeePropertyGroupsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						appg.*
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						appg.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND appg.cid = ' . ( int ) $intCid;

		return self::fetchApPayeePropertyGroups( $strSql, $objDatabase );
	}

	public static function fetchActiveApPayeePropertyGroupsByCidByPropertyGroupId( $intCid, $intPropertyGroupId, $objDatabase ) {

		$strSql = 'SELECT
					    appg.cid,
					    appg.ap_payee_id,
						pga.property_id,
						appg.ap_payee_location_id,
						apl.store_id,
						p.property_name
					FROM
					    ap_payee_property_groups appg
					    JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					    JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					    JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id AND p.is_disabled = 0 )
					    JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ]::INT[], ARRAY[ ' . ( int ) $intPropertyGroupId . ' ]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ' ]::INT[] ) lp ON lp.cid = p.cid AND lp.property_id = p.id AND lp.is_disabled = 0
					    LEFT JOIN ap_payee_locations apl ON ( apl.id = appg.ap_payee_location_id AND apl.cid = appg.cid )
					WHERE
					    appg.cid = ' . ( int ) $intCid . '
					    AND pg.id = ' . ( int ) $intPropertyGroupId . '
					GROUP BY
					    appg.cid,
					    appg.ap_payee_id,
					    pga.property_id,
					    appg.ap_payee_location_id,
					    apl.store_id,
					    p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByApPayeeLocationIdsAndCid( $arrintApPayeeLocationIds, $intCid, $objDatabase ) {
		if( false == ( $arrintApPayeeLocationIds = getIntValuesFromArr( $arrintApPayeeLocationIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						count ( pga.property_id ) as property_count,
						pga.property_id,
						appg.ap_payee_location_id
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( appg.cid = pg.cid AND appg.property_group_id = pg.id )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					    JOIN load_properties( array [ ]::int [ ], array [ ]::int [ ], array [ ' . CPsProduct::ENTRATA . ' ]::int [ ] ) lp ON ( pga.cid = lp.cid AND pga.property_id = lp.property_id AND lp.is_disabled = 0 )
					WHERE
						appg.cid = ' . ( int ) $intCid . ' 
						AND appg.ap_payee_location_id IN ( ' . sqlIntImplode( $arrintApPayeeLocationIds ) . ' ) 
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
					GROUP BY
						pga.property_id, appg.ap_payee_location_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApPayeePropertyGroupsByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						appg.*,
						pga.property_id
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					WHERE
						appg.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND appg.cid = ' . ( int ) $intCid;

		return self::fetchApPayeePropertyGroups( $strSql, $objDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeIdByApPayeeLocationIdByPropertyIdByCid( $intApPayeeId, $intApPayeeLocationId, $intPropertyId, $intCid, $intCompanyUserId, $objClientDatabase ) {
		if( false == valId( $intApPayeeLocationId ) || false == valId( $intPropertyId ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId );

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.is_disabled
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN ( ' . $strSql . ' ) as p ON ( pga.cid = p.cid AND pga.property_id = p.id ) 
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND ( appg.ap_payee_id, appg.ap_payee_location_id, p.id ) = ( ' . $intApPayeeId . ',' . $intApPayeeLocationId . ',' . $intPropertyId . ' )
					ORDER BY
						LOWER( p.property_name )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByIdByCid( $intPropertyId, $intCid, $intCompanyUserId, $objClientDatabase ) {

		$strSql = \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId );

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN ( ' . $strSql . ' ) as p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId;

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeePropertiesByApPayeeLocationIdByPropertyIdsByCid( $intApPayeeLocationId, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						appg.*,
						pga.property_id,
						p.property_name,
						p.is_disabled
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pg.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id )
					WHERE
						appg.cid = ' . ( int ) $intCid . '
						AND appg.ap_payee_location_id  = ' . ( int ) $intApPayeeLocationId . '
						AND p.id IN ( ' . implode( ',',  $arrintPropertyIds ) . ' )';

		return self::fetchApPayeePropertyGroups( $strSql, $objClientDatabase, false );
	}

	public static function fetchActiveApPayeePropertyGroupsByCidByPropertyIdByPropertyGroupIds( $intCid, $intPropertyId, $arrintPropertyGroupId, $objDatabase ) {

		if( false == valArr( $arrintPropertyGroupId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    appg.cid,
					    appg.ap_payee_id,
						pga.property_id,
						appg.ap_payee_location_id,
						apl.store_id,
						p.property_name
					FROM
					    ap_payee_property_groups appg
					    JOIN property_groups pg ON ( pg.cid = appg.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					    JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					    JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id AND p.is_disabled = 0 )
					    LEFT JOIN ap_payee_locations apl ON ( apl.id = appg.ap_payee_location_id AND apl.cid = appg.cid )
					WHERE
					    appg.cid = ' . ( int ) $intCid . '
					    AND pg.id IN ( ' . sqlIntImplode( $arrintPropertyGroupId ) . ' )
					    AND p.id = ' . ( int ) $intPropertyId . '
					GROUP BY
					    appg.cid,
					    appg.ap_payee_id,
					    pga.property_id,
					    appg.ap_payee_location_id,
					    apl.store_id,
					    p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationPropertyCountByApPayeeLocationIdsByPropertyGroupIdsByCid( $arrintApPayeeLocationIds, $arrintDeletedPropertyGroupIds, $intCid, $objDatabase, $intPropertyId = NULL ) {

		if( false == ( $arrintApPayeeLocationIds = getIntValuesFromArr( $arrintApPayeeLocationIds ) ) || false == ( $arrintDeletedPropertyGroupIds = getIntValuesFromArr( $arrintDeletedPropertyGroupIds ) ) ) {
			return NULL;
		}

		$strWhereClause = '';

		if( true == valId( $intPropertyId ) ) {
			$strWhereClause .= ' AND p.id = ' . ( int ) $intPropertyId;
		}

		$strSql = 'SELECT
						count ( pga.property_id ) as property_count,
						pga.property_id,
						appg.ap_payee_location_id
					FROM
						ap_payee_property_groups appg
						JOIN property_groups pg ON ( appg.cid = pg.cid AND appg.property_group_id = pg.id )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pg.id = pga.property_group_id )
					    JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id AND p.is_disabled = 0 AND p.is_managerial = 0 )
					WHERE
						appg.cid = ' . ( int ) $intCid . ' 
						AND appg.ap_payee_location_id IN ( ' . sqlIntImplode( $arrintApPayeeLocationIds ) . ' ) ' .
		                $strWhereClause . '
		                AND pga.property_group_id NOT IN ( ' . sqlIntImplode( $arrintDeletedPropertyGroupIds ) . ' ) 
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
					GROUP BY
						pga.property_id, appg.ap_payee_location_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>