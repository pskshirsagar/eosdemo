<?php

class CScreeningPackageCustomerTypesMapping extends CBaseScreeningPackageCustomerTypesMapping {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valLeaseTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_type_id', __( 'Customer type id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerRelationshipId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerRelationshipId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_relationship_id', __( 'Customer relationship id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDefaultScreeningPackageId( $strCustomerRelationshipName, $strCustomerRelationshipGroupName = NULL ) {
		$boolIsValid = true;

		if( true == empty( $this->getDefaultScreeningPackageId() ) && CScreeningApplicantRequirementType::NOT_REQUIRED != $this->getScreeningApplicantRequirementTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_screening_package_id', __( 'Default screening package is required for occupant [ {%s,customer_relationship_name} ] of Group [ {%s, customer_relationship_group_name} ]. ', [ 'customer_relationship_name' => $strCustomerRelationshipName, 'customer_relationship_group_name' => $strCustomerRelationshipGroupName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valScreeningApplicantRequirementTypeId( $strCustomerRelationshipName, $strCustomerRelationshipGroupName = NULL ) {

		$boolIsValid = true;

		if( true == empty( $this->getScreeningApplicantRequirementTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_applicant_requirement_type_id', __( 'Screening applicant requirement type is required for Occupant [ {%s, customer_relationship_name} ] of Group [ {%s, customer_relationship_group_name} ]. ', [ 'customer_relationship_name' => $strCustomerRelationshipName, 'customer_relationship_group_name' => $strCustomerRelationshipGroupName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $strCustomerRelationshipName, $strCustomerRelationshipGroupName = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCustomerTypeId();
				$boolIsValid &= $this->valCustomerRelationshipId();
				$boolIsValid &= $this->valDefaultScreeningPackageId( $strCustomerRelationshipName, $strCustomerRelationshipGroupName );
				$boolIsValid &= $this->valScreeningApplicantRequirementTypeId( $strCustomerRelationshipName, $strCustomerRelationshipGroupName );
				break;

			case VALIDATE_DELETE:
				// no action
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valArchiveScreeningPackageCustomerTypesMappings( $intCid, $intPropertyId, $intUserId, $objDatabase, $arrintScreeningPackageIds ) {
		$boolIsValid = true;

		$strSql = 'UPDATE
						screening_package_customer_types_mappings
					SET
						is_active = 0,
						updated_by = ' . ( int ) $intUserId . ',
						updated_on = NOW ()
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND default_screening_package_id IN (' . implode( ', ', $arrintScreeningPackageIds ) . ')';

		$objDatabase->begin();

		$boolIsValid = fetchData( $strSql, $objDatabase );

		if( false == $boolIsValid ) {
			$objDatabase->rollback();
		} else {
			$objDatabase->commit();
		}

		return $boolIsValid;
	}

	public function valArchiveScreeningPackageCustomerTypesMappingsByLeaseType( $intCid, $intPropertyId, $intLeaseTypeId, $intUserId, $objDatabase ) {

		$boolIsValid = true;
		$strLeaseTypeIdSql = '';

		if( true == empty( $intLeaseTypeId ) || 0 == $intLeaseTypeId ) $strLeaseTypeIdSql = ' IS NULL ';
		if( false == empty( $intLeaseTypeId ) && 0 != $intLeaseTypeId ) $strLeaseTypeIdSql = ' = ' . ( int ) $intLeaseTypeId;

		$strSql = 'UPDATE
						screening_package_customer_types_mappings
					SET
						is_active = 0,
						updated_by = ' . ( int ) $intUserId . ',
						updated_on = NOW ()
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		if( true == valStr( $strLeaseTypeIdSql ) ) $strSql .= ' AND lease_type_id ' . $strLeaseTypeIdSql;

		$strSql .= ' AND is_active = 1';

		$objDatabase->begin();

		$boolIsValid = fetchData( $strSql, $objDatabase );

		if( false == $boolIsValid ) $objDatabase->rollback();

		$objDatabase->commit();

		return $boolIsValid;
	}

}
?>