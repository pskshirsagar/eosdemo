<?php

class CApplicantAsset extends CBaseApplicantAsset {

	protected $m_boolIsDelete;
	protected $m_boolApplicantAssetTypeSelected;

 	/**
 	 * Set Functions
 	 */

    public function setAddress( $strAddress ) {
           $this->m_strAddress = ( 0 == strlen( $strAddress ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strAddress, 100, NULL, true ) ) ) );
    }

    public function setCity( $strCity ) {
            $this->m_strCity = ( 0 == strlen( $strCity ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCity, 50, NULL, true ) ) ) );
    }

    public function setContactName( $strContactName ) {
            $this->m_strContactName = ( 0 == strlen( $strContactName ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strContactName, 50, NULL, true ) ) ) );
    }

 	public function setInstitutionName( $strInstitutionName ) {
        $this->m_strInstitutionName = ( 0 == strlen( $strInstitutionName ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strInstitutionName, 100, NULL, true ) ) ) );
    }

	public function setNameOnAccount( $strNameOnAccount ) {
        $this->m_strNameOnAccount = ( 0 == strlen( $strNameOnAccount ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strNameOnAccount, 50, NULL, true ) ) ) );
    }

    /**
     * Get Functions
     */

	public function getIsDelete() {
		return $this->m_boolIsDelete;
    }

    public function getAccountNumber() {
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getAccountNumberMasked() {
		$strAccountNumber 	= \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
		$intStringLength 	= strlen( $strAccountNumber );
		$strLastFour 		= \Psi\CStringService::singleton()->substr( $strAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, '*', STR_PAD_LEFT );
	}

    public function getDefaultSectionName() {
    	return 'Asset\'s';
    }

	public function getApplicantAssetTypeSelected() {
    	return $this->m_boolApplicantAssetTypeSelected;
    }

    /**
     * Set Functions
     */

	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = $boolIsDelete;
    }

	public function setApplicantAssetTypeSelected( $boolApplicantAssetTypeSelected ) {
		$this->m_boolApplicantAssetTypeSelected = $boolApplicantAssetTypeSelected;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['is_delete'] ) ) 		$this->setIsDelete( $arrValues['is_delete'] );
   		if( true == isset( $arrValues['account_number'] ) ) 	$this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number'] ) : $arrValues['account_number'] );
   		if( true == isset( $arrValues['asset_select'] ) )	$this->setApplicantAssetTypeSelected( $arrValues['asset_select'] );
    }

    public function setAccountNumber( $strPlainAccountNumber ) {
		if ( true == \Psi\CStringService::singleton()->stristr( $strPlainAccountNumber, '*' ) ) return;
		$strPlainAccountNumber = CStrings::strTrimDef( $strPlainAccountNumber, 20, NULL, true );
	    $this->setAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPlainAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
	}

    /**
     * Validate Function
     */

	public function validate( $strAction, $objApplicantAssetType, $arrobjPropertyApplicationPreferences = NULL, $arrstrApplicantAssetTypeCustomFields = NULL, $intIsAlien = NULL ) {
		$boolIsValid = true;

		$objApplicantAssetValidator = new CApplicantAssetValidator();
		$objApplicantAssetValidator->setApplicantAsset( $this );
		$boolIsValid &= $objApplicantAssetValidator->validate( $strAction, $objApplicantAssetType, $arrobjPropertyApplicationPreferences, $arrstrApplicantAssetTypeCustomFields, $intIsAlien );

		return $boolIsValid;
	}

}
?>