<?php

class CLeaseTermStructure extends CBaseLeaseTermStructure {

	const DEFAULT_LEASE_TERM_STRUCTURE			= 1;
	const DEFAULT_STUDENT_LEASE_TERM_STRUCTURE	= 2;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {

        if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'client id required.' ) ) );
        	return false;
        }

 		return true;
    }

    public function valDefaultLeaseTermStructureId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
    	if( true == is_null( $this->getName() ) || 1 > strlen( $this->getName() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Lease term structure name required.' ) ) );
    		return false;
    	}

        return true;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDependentInformation( $objDatabase ) {

    	$strFromClause = ' specials s
    						JOIN rates rt ON ( rt.cid = s.cid AND rt.ar_origin_reference_id = s.id AND rt.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND s.deleted_by IS NULL AND s.deleted_on IS NULL AND rt.is_allowed = TRUE AND rt.rate_amount != 0 )
    						JOIN lease_terms lt ON ( lt.cid = rt.cid AND lt.id = rt.lease_term_id AND lt.deleted_by IS NULL AND lt.deleted_on IS NULL )
    						JOIN lease_term_structures lts ON ( lts.cid = lt.cid AND lts.id = lt.lease_term_structure_id )
    						JOIN property_charge_settings pcs ON ( pcs.cid = lts.cid AND pcs.lease_term_structure_id = lts.id )';

    	$strWhereClause = ' WHERE lts.cid = ' . ( int ) $this->getCid() . ' AND lts.id  = ' . ( int ) $this->getId() . ' AND lts.deleted_by IS NULL AND lts.deleted_on IS NULL';

    	if( 0 < ( int ) CLeaseTerms::fetchRowCount( $strWhereClause, $strFromClause, $objDatabase ) ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'specials',  __( 'You are attempting to delete a lease term structure that is associated to Specials, you must first delete any specials associated to this lease term.' ) ) );
    		return false;
    	}

    	$strFromClause = '  cached_applications ca
    						JOIN quotes q ON ( q.cid = ca.cid AND q.application_id = ca.id AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' AND ca.application_status_id IN ( ' . implode( ',', CApplicationStatus::$c_arrintActiveApplicationStatusIds ) . ' ) )
    						JOIN quote_lease_terms qlt ON ( qlt.cid = q.cid AND qlt.quote_id = q.id AND qlt.deleted_by IS NULL AND qlt.deleted_on IS NULL)
    						JOIN lease_terms lt ON ( lt.cid = qlt.cid AND lt.id = qlt.lease_term_id AND lt.deleted_by IS NULL AND lt.deleted_on IS NULL )
    						JOIN lease_term_structures lts ON ( lts.cid = lt.cid AND lts.id = lt.lease_term_structure_id )';

    	$strWhereClause = ' WHERE lts.cid = ' . ( int ) $this->getCid() . ' AND lts.id  = ' . ( int ) $this->getId() . ' AND lts.deleted_by IS NULL AND lts.deleted_on IS NULL';

    	if( 0 < ( int ) CLeaseTerms::fetchRowCount( $strWhereClause, $strFromClause, $objDatabase ) ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_offer', __( 'You are attempting to delete a lease term structure that is included in renewal offers. The existing renewal offers will continue to use this lease term unless you edit those offers manually. Do you want to delete the lease term(s)?' ) ) );
    		return false;
    	}

    	return true;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:

        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valName();
        		break;

        	case VALIDATE_DELETE:
        		$boolIsValid &= $this->valDependentInformation( $objDatabase );
        		break;

			case 'validate_lease_term_structure':
				$boolIsValid &= $this->valCid();
				break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>