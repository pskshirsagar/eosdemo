<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CVoiceTypes
 * Do not add any new functions to this class.
 */

class CVoiceTypes extends CBaseVoiceTypes {

	public static function fetchVoiceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CVoiceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchVoiceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CVoiceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>