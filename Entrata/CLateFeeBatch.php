<?php

class CLateFeeBatch extends CBaseLateFeeBatch {

	public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id required.' ) ) );
		}

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) || 0 >= ( int ) $this->getPropertyId() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valBatchDatetime() {
        $boolIsValid = true;

        if( true == is_null( $this->getBatchDatetime() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'batch_datetime', __( 'Batch Datetime is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valPostedThroughDate() {
        $boolIsValid = true;

        if( true == is_null( $this->getPostedThroughDate() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'posted_through_date', __( 'Batch Posted Through Date is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

	switch( $strAction ) {
		case VALIDATE_INSERT:
		case VALIDATE_UPDATE:
			$boolIsValid &= $this->valCid();
			$boolIsValid &= $this->valPropertyId();
			$boolIsValid &= $this->valBatchDatetime();
			$boolIsValid &= $this->valPostedThroughDate();
			break;

		case VALIDATE_DELETE:
			break;

        default:
        	$boolIsValid = true;
        	break;
	}

        return $boolIsValid;
    }

}
?>