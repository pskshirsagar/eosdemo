<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultChecklistItems
 * Do not add any new functions to this class.
 */

class CDefaultChecklistItems extends CBaseDefaultChecklistItems {

	public static function fetchDefaultChecklistItems( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultChecklistItem::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultChecklistItem( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultChecklistItem::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>