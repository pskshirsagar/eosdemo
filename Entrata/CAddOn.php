<?php

class CAddOn extends CBaseAddOn {

	protected $m_fltRentAmount;
	protected $m_fltTaxAmount;
	protected $m_fltDepositAmount;
	protected $m_fltOtherAmount;
	protected $m_fltDepositTaxAmount;
	protected $m_fltOtherTaxAmount;
	protected $m_fltReplacementFee;
	protected $m_fltMinRentAmount;
	protected $m_fltMaxRentAmount;
	protected $m_fltAdvertisedRentAmount;

	protected $m_intArCodeId;
	protected $m_intArCodeName;
	protected $m_intArOriginId;
	protected $m_intArCascadeId;
	protected $m_intRentArCodeId;
	protected $m_intDepositArCodeId;
	protected $m_intReservationCount;
	protected $m_intOtherArCodeId;
	protected $m_intAddOnTypeId;
	protected $m_intAddOnCategoryId;
	protected $m_intArCascadeReferenceId;
	protected $m_intArOriginReferenceId;
	protected $m_intDefaultAddOnCategoryId;

	protected $m_strEndDate;
	protected $m_strStartDate;
	protected $m_strBuildingName;
	protected $m_strAddOnCategoryName;
	protected $m_strLeaseStatusTypeName;
	protected $m_strUnitNumber;
	protected $m_strSpaceNumber;
	protected $m_strAddOnGroupName;
	protected $m_strUnitNumberCache;
	protected $m_strReservationDateTime;
	protected $m_strAddOnGroupDescription;
	protected $m_strLeaseTermName;
	protected $m_strAddOnTypeName;

	protected $m_intRateId;
	protected $m_intRentRateId;
	protected $m_intDepositRateId;
	protected $m_intOtherRateId;
	protected $m_intRateLogId;
	protected $m_intRentRateLogId;
	protected $m_intDepositRateLogId;
	protected $m_intOtherRateLogId;
	protected $m_intRentArTriggerId;
	protected $m_intDepositArTriggerId;
	protected $m_intOtherArTriggerId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseTermMonth;

	protected $m_intRateAmount;
	protected $m_strFullImageUri;
	protected $m_strAddonGroupName;

	protected $m_boolLockToLeaseDates;
	protected $m_boolIsAutomaticallyIncluded;
	protected $m_strLeaseAssociationMoveOutDate;
	protected $m_strLeaseAssociationEndDate;

	protected $m_strFirstName;
	protected $m_strLastName;
	protected $m_strCustomerLeaseStatusTypeName;

	protected $m_arrobjRates;
	protected $m_arrobjJsonRates;
	protected $m_strArCodeName;

	protected $m_fltTaxPercent;

	const SUNDAY     = 1;
	const MONDAY     = 2;
	const TUESDAY    = 3;
	const WEDNESDAY  = 4;
	const THURSDAY   = 5;
	const FRIDAY     = 6;
	const SATURDAY   = 7;

	public function __construct() {
		parent::__construct();

		$this->m_fltRentAmount    = '0';
		$this->m_fltDepositAmount = '0';
		$this->m_intArCodeId      = '0';
		$this->m_fltOtherAmount   = '0';

	}

	// ***************************************************
	// ******************* Get Functions *******************
	// ****************************************************

	public function getRentAmount() {
		return $this->m_fltRentAmount;
	}

	public function getDepositAmount() {
		return $this->m_fltDepositAmount;
	}

	public function getOtherAmount() {
		return $this->m_fltOtherAmount;
	}

	public function getReplacementFee() {
		return $this->m_fltReplacementFee;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function getRentArCodeId() {
		return $this->m_intRentArCodeId;
	}

	public function getDepositArCodeId() {
		return $this->m_intDepositArCodeId;
	}

	public function getOtherArCodeId() {
		return $this->m_intOtherArCodeId;
	}

	public function getArCodeName() {
		return $this->m_intArCodeName;
	}

	public function getAddOnCategoryName() {
		return $this->m_strAddOnCategoryName;
	}

	public function getLeaseStatusTypeName() {
		return $this->m_strLeaseStatusTypeName;
	}

	public function getAddOnTypeId() {
		return $this->m_intAddOnTypeId;
	}

	public function getAddOnCategoryId() {
		return $this->m_intAddOnCategoryId;
	}

	public function getDefaultAddOnCategoryId() {
		return $this->m_intDefaultAddOnCategoryId;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getSpaceNumber() {
		return $this->m_strSpaceNumber;
	}

	public function getAddOnGroupName() {
		return $this->m_strAddOnGroupName;
	}

	public function getRentRateId() {
		return $this->m_intRentRateId;
	}

	public function getRateId() {
		return $this->m_intRateId;
	}

	public function getDepositRateId() {
		return $this->m_intDepositRateId;
	}

	public function getOtherRateId() {
		return $this->m_intOtherRateId;
	}

	public function getReservationCount() {
		return $this->m_intReservationCount;
	}

	public function getRentRateLogId() {
		return $this->m_intRentRateLogId;
	}

	public function getRateLogId() {
		return $this->m_intRateLogId;
	}

	public function getDepositRateLogId() {
		return $this->m_intDepositRateLogId;
	}

	public function getOtherRateLogId() {
		return $this->m_intOtherRateLogId;
	}

	public function getRentArTriggerId() {
		return $this->m_intRentArTriggerId;
	}

	public function getDepositArTriggerId() {
		return $this->m_intDepositArTriggerId;
	}

	public function getOtherArTriggerId() {
		return $this->m_intOtherArTriggerId;
	}

	public function getUnitNumberCache() {
		return $this->m_strUnitNumberCache;
	}

	public function getReservationDateTime() {
		return $this->m_strReservationDateTime;
	}

	public function getDefaultRentAmount() {
		return $this->m_fltRentAmount;
	}

	public function getDefaultDepositAmount() {
		return $this->m_fltDepositAmount;
	}

	public function getFullImageUri() {
		return $this->m_strFullImageUri;
	}

	public function getRateAmount() {
		return $this->m_intRateAmount;
	}

	public function getArCascadeId() {
		return $this->m_intArCascadeId;
	}

	public function getArCascadeReferenceId() {
		return $this->m_intArCascadeReferenceId;
	}

	public function getArOriginId() {
		return $this->m_intArOriginId;
	}

	public function getArOriginReferenceId() {
		return $this->m_intArOriginReferenceId;
	}

	public function getAddOnGroupDescription() {
		return $this->m_strAddOnGroupDescription;
	}

	public function getLockToLeaseDates() {
		return $this->m_boolLockToLeaseDates;
	}

	public function getFirstName() {
		return $this->m_strFirstName;
	}

	public function getLastName() {
		return $this->m_strLastName;
	}

	public function getCustomerLeaseStatusTypeName() {
		return $this->m_strCustomerLeaseStatusTypeName;
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function getLeaseTermMonth() {
		return $this->m_intLeaseTermMonth;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getAddOnTypeName() {
		return $this->m_strAddOnTypeName;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getMinRentAmount() {
		return $this->m_fltMinRentAmount;
	}

	public function getMaxRentAmount() {
		return $this->m_fltMaxRentAmount;
	}

	public function getRates() {
		return $this->m_arrobjRates;
	}

	public function getJsonRates() {
		return $this->m_arrobjJsonRates;
	}

	public function getTaxPercent() {
		return ( float ) $this->m_fltTaxPercent;
	}

	public function getTaxAmount() {
		return ( float ) $this->m_fltTaxAmount;
	}

	public function getDepositTaxAmount() {
		return ( float ) $this->m_fltDepositTaxAmount;
	}

	public function getOtherTaxAmount() {
		return ( float ) $this->m_fltOtherTaxAmount;
	}

	public function getIsAutomaticallyIncluded() {
		return $this->m_boolIsAutomaticallyIncluded;
	}

	public function getLeaseAssociationMoveOutDate() {
		return $this->m_strLeaseAssociationMoveOutDate;
	}

	public function getLeaseAssociationEndDate() {
		return $this->m_strLeaseAssociationEndDate;
	}

	public function getAdvertisedRentAmount() {
		return $this->m_fltAdvertisedRentAmount;
	}

	// Set Functions

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['rent_amount'] ) ) {
			$this->setRentAmount( $arrmixValues['rent_amount'] );
		}
		if( true == isset( $arrmixValues['min_rent_amount'] ) ) {
			$this->setMinRentAmount( $arrmixValues['min_rent_amount'] );
		}
		if( true == isset( $arrmixValues['max_rent_amount'] ) ) {
			$this->setMaxRentAmount( $arrmixValues['max_rent_amount'] );
		}
		if( true == isset( $arrmixValues['deposit_amount'] ) ) {
			$this->setDepositAmount( $arrmixValues['deposit_amount'] );
		}
		if( true == isset( $arrmixValues['other_amount'] ) ) {
			$this->setOtherAmount( $arrmixValues['other_amount'] );
		}
		if( true == isset( $arrmixValues['replacement_fee'] ) ) {
			$this->setReplacementFee( $arrmixValues['replacement_fee'] );
		}
		if( true == isset( $arrmixValues['max_end_date'] ) ) {
			$this->setEndDate( $arrmixValues['max_end_date'] );
		}
		if( true == isset( $arrmixValues['end_date'] ) ) {
			$this->setEndDate( $arrmixValues['end_date'] );
		}
		if( true == isset( $arrmixValues['start_date'] ) ) {
			$this->setStartDate( $arrmixValues['start_date'] );
		}
		if( true == isset( $arrmixValues['ar_code_id'] ) ) {
			$this->setArCodeId( $arrmixValues['ar_code_id'] );
		}
		if( true == isset( $arrmixValues['ar_code_name'] ) ) {
			$this->setArCodeName( $arrmixValues['ar_code_name'] );
		}
		if( true == isset( $arrmixValues['add_on_category_name'] ) ) {
			$this->setAddOnCategoryName( $arrmixValues['add_on_category_name'] );
		}
		if( true == isset( $arrmixValues['add_on_type_id'] ) ) {
			$this->setAddOnTypeId( $arrmixValues['add_on_type_id'] );
		}
		if( true == isset( $arrmixValues['add_on_category_id'] ) ) {
			$this->setAddOnCategoryId( $arrmixValues['add_on_category_id'] );
		}
		if( true == isset( $arrmixValues['default_add_on_category_id'] ) ) {
			$this->setDefaultAddOnCategoryId( $arrmixValues['default_add_on_category_id'] );
		}
		if( true == isset( $arrmixValues['rent_ar_code_id'] ) ) {
			$this->setRentArCodeId( $arrmixValues['rent_ar_code_id'] );
		}
		if( true == isset( $arrmixValues['deposit_ar_code_id'] ) ) {
			$this->setDepositArCodeId( $arrmixValues['deposit_ar_code_id'] );
		}
		if( true == isset( $arrmixValues['other_ar_code_id'] ) ) {
			$this->setOtherArCodeId( $arrmixValues['other_ar_code_id'] );
		}
		if( true == isset( $arrmixValues['lease_status_type_name'] ) ) {
			$this->setLeaseStatusTypeName( $arrmixValues['lease_status_type_name'] );
		}
		if( true == isset( $arrmixValues['unit_space_id'] ) ) {
			$this->setUnitSpaceId( $arrmixValues['unit_space_id'] );
		}
		if( true == isset( $arrmixValues['unit_number'] ) ) {
			$this->setUnitNumber( $arrmixValues['unit_number'] );
		}
		if( true == isset( $arrmixValues['space_number'] ) ) {
			$this->setSpaceNumber( $arrmixValues['space_number'] );
		}
		if( true == isset( $arrmixValues['add_on_group_name'] ) ) {
			$this->setAddOnGroupName( $arrmixValues['add_on_group_name'] );
		}
		if( true == isset( $arrmixValues['rate_id'] ) ) {
			$this->setRateId( $arrmixValues['rate_id'] );
		}
		if( true == isset( $arrmixValues['rent_rate_id'] ) ) {
			$this->setRentRateId( $arrmixValues['rent_rate_id'] );
		}
		if( true == isset( $arrmixValues['deposit_rate_id'] ) ) {
			$this->setDepositRateId( $arrmixValues['deposit_rate_id'] );
		}
		if( true == isset( $arrmixValues['other_rate_id'] ) ) {
			$this->setOtherRateId( $arrmixValues['other_rate_id'] );
		}
		if( true == isset( $arrmixValues['reservation_count'] ) ) {
			$this->setReservationCount( $arrmixValues['reservation_count'] );
		}
		if( true == isset( $arrmixValues['rent_ar_trigger_id'] ) ) {
			$this->setRentArTriggerId( $arrmixValues['rent_ar_trigger_id'] );
		}
		if( true == isset( $arrmixValues['deposit_ar_trigger_id'] ) ) {
			$this->setDepositArTriggerId( $arrmixValues['deposit_ar_trigger_id'] );
		}
		if( true == isset( $arrmixValues['other_ar_trigger_id'] ) ) {
			$this->setOtherArTriggerId( $arrmixValues['other_ar_trigger_id'] );
		}

		if( true == isset( $arrmixValues['rent_rate_log_id'] ) ) {
			$this->setRentRateLogId( $arrmixValues['rent_rate_log_id'] );
		}
		if( true == isset( $arrmixValues['rate_log_id'] ) ) {
			$this->setRateLogId( $arrmixValues['rate_log_id'] );
		}
		if( true == isset( $arrmixValues['deposit_rate_log_id'] ) ) {
			$this->setDepositRateLogId( $arrmixValues['deposit_rate_log_id'] );
		}
		if( true == isset( $arrmixValues['other_rate_log_id'] ) ) {
			$this->setOtherRateLogId( $arrmixValues['other_rate_log_id'] );
		}
		if( true == isset( $arrmixValues['unit_number_cache'] ) ) {
			$this->setUnitNumberCache( $arrmixValues['unit_number_cache'] );
		}
		if( true == isset( $arrmixValues['reservation_datetime'] ) ) {
			$this->setReservationDateTime( $arrmixValues['reservation_datetime'] );
		}
		if( true == isset( $arrmixValues['full_image_uri'] ) ) {
			$this->setFullImageUri( $arrmixValues['full_image_uri'] );
		}
		if( true == isset( $arrmixValues['rate_amount'] ) ) {
			$this->setRateAmount( $arrmixValues['rate_amount'] );
		}
		if( true == isset( $arrmixValues['ar_cascade_id'] ) ) {
			$this->setArCascadeId( $arrmixValues['ar_cascade_id'] );
		}
		if( true == isset( $arrmixValues['ar_cascade_reference_id'] ) ) {
			$this->setArCascadeReferenceId( $arrmixValues['ar_cascade_reference_id'] );
		}
		if( true == isset( $arrmixValues['ar_origin_id'] ) ) {
			$this->setArOriginId( $arrmixValues['ar_origin_id'] );
		}
		if( true == isset( $arrmixValues['ar_origin_reference_id'] ) ) {
			$this->setArOriginReferenceId( $arrmixValues['ar_origin_reference_id'] );
		}
		if( true == isset( $arrmixValues['add_on_group_description'] ) ) {
			$this->setAddOnGroupDescription( $arrmixValues['add_on_group_description'] );
		}
		if( true == isset( $arrmixValues['name_first'] ) ) {
			$this->setFirstName( $arrmixValues['name_first'] );
		}
		if( true == isset( $arrmixValues['name_last'] ) ) {
			$this->setLastName( $arrmixValues['name_last'] );
		}
		if( true == isset( $arrmixValues['customer_lease_status_type_name'] ) ) {
			$this->setCustomerLeaseStatusTypeName( $arrmixValues['customer_lease_status_type_name'] );
		}
		if( true == isset( $arrmixValues['lease_term_id'] ) ) {
			$this->setLeaseTermId( $arrmixValues['lease_term_id'] );
		}
		if( true == isset( $arrmixValues['lease_term_month'] ) ) {
			$this->setLeaseTermMonth( $arrmixValues['lease_term_month'] );
		}
		if( true == isset( $arrmixValues['lease_term_name'] ) ) {
			$this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		}
		if( true == isset( $arrmixValues['add_on_type_name'] ) ) {
			$this->setAddOnTypeName( $arrmixValues['add_on_type_name'] );
		}
		if( true == isset( $arrmixValues['building_name'] ) ) {
			$this->setBuildingName( $arrmixValues['building_name'] );
		}
		if( true == isset( $arrmixValues['json_rate_log'] ) ) {
			$this->setJsonRates( $arrmixValues['json_rate_log'] );
		}
		if( isset( $arrmixValues['lock_to_lease_dates'] ) && $boolDirectSet ) {
			$this->m_boolLockToLeaseDates = trim( stripcslashes( $arrmixValues['lock_to_lease_dates'] ) );
		} else {
			if( isset( $arrmixValues['lock_to_lease_dates'] ) ) {
				$this->setLockToLeaseDates( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['lock_to_lease_dates'] ) : $arrmixValues['lock_to_lease_dates'] );
			}
		}
		if( true == isset( $arrmixValues['tax_percent'] ) ) {
			$this->setTaxPercent( $arrmixValues['tax_percent'] );
		}
		if( true == isset( $arrmixValues['tax_amount'] ) ) {
			$this->setTaxAmount( $arrmixValues['tax_amount'] );
		}
		if( true == isset( $arrmixValues['deposit_tax_amount'] ) ) {
			$this->setDepositTaxAmount( $arrmixValues['deposit_tax_amount'] );
		}
		if( true == isset( $arrmixValues['other_tax_amount'] ) ) {
			$this->setOtherTaxAmount( $arrmixValues['other_tax_amount'] );
		}
		if( true == isset( $arrmixValues['automatically_included'] ) ) {
			$this->setIsAutomaticallyIncluded( $arrmixValues['automatically_included'] );
		}
		if( true == isset( $arrmixValues['lease_association_move_out_date'] ) ) {
			$this->setLeaseAssociationMoveOutDate( $arrmixValues['lease_association_move_out_date'] );
		}
		if( true == isset( $arrmixValues['lease_association_end_date'] ) ) {
			$this->setLeaseAssociationEndDate( $arrmixValues['lease_association_end_date'] );
		}
		if( true == isset( $arrmixValues['advertised_rent_amount'] ) ) {
			$this->setAdvertisedRentAmount( $arrmixValues['advertised_rent_amount'] );
		}
	}

	public function setArCodeId( $intArCodeId ) {
		$this->m_intArCodeId = CStrings::strToIntDef( $intArCodeId, NULL, false );
	}

	public function setRentArCodeId( $intRentArCodeId ) {
		$this->m_intRentArCodeId = CStrings::strToIntDef( $intRentArCodeId, NULL, false );
	}

	public function setDepositArCodeId( $intDepositArCodeId ) {
		$this->m_intDepositArCodeId = CStrings::strToIntDef( $intDepositArCodeId, NULL, false );
	}

	public function setOtherArCodeId( $intOtherArCodeId ) {
		$this->m_intOtherArCodeId = CStrings::strToIntDef( $intOtherArCodeId, NULL, false );
	}

	public function setRentAmount( $fltRentAmount ) {
		$this->m_fltRentAmount = CStrings::strToFloatDef( $fltRentAmount, NULL, false, 2 );
	}

	public function setDepositAmount( $fltDepositAmount ) {
		$this->m_fltDepositAmount = CStrings::strToFloatDef( $fltDepositAmount, NULL, false, 2 );
	}

	public function setOtherAmount( $fltOtherAmount ) {
		$this->m_fltOtherAmount = CStrings::strToFloatDef( $fltOtherAmount, NULL, false, 2 );
	}

	public function setReplacementFee( $fltReplacementFee ) {
		$this->m_fltReplacementFee = CStrings::strToFloatDef( $fltReplacementFee, NULL, false, 2 );
	}

	public function setEndDate( $strEndDate ) {
		$this->m_strEndDate = $strEndDate;
	}

	public function setStartDate( $strStartDate ) {
		$this->m_strStartDate = $strStartDate;
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = $strArCodeName;
	}

	public function setAddOnCategoryName( $strAddOnCategoryName ) {
		$this->m_strAddOnCategoryName = $strAddOnCategoryName;
	}

	public function setLeaseStatusTypeName( $strLeaseStatusTypeName ) {
		$this->m_strLeaseStatusTypeName = $strLeaseStatusTypeName;
	}

	public function setAddOnTypeId( $intAddOnTypeId ) {
		$this->m_intAddOnTypeId = $intAddOnTypeId;
	}

	public function setAddOnCategoryId( $intAddOnCategoryId ) {
		$this->m_intAddOnCategoryId = $intAddOnCategoryId;
	}

	public function setDefaultAddOnCategoryId( $intDefaultAddOnCategoryId ) {
		$this->m_intDefaultAddOnCategoryId = CStrings::strToIntDef( $intDefaultAddOnCategoryId );
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setSpaceNumber( $strSpaceNumber ) {
		$this->m_strSpaceNumber = $strSpaceNumber;
	}

	public function setAddOnGroupName( $strAddOnGroupName ) {
		$this->m_strAddOnGroupName = $strAddOnGroupName;
	}

	public function setRentRateId( $intRentRateId ) {
		$this->m_intRentRateId = $intRentRateId;
	}

	public function setRateId( $intRateId ) {
		$this->m_intRateId = $intRateId;
	}

	public function setDepositRateId( $intDepositRateId ) {
		$this->m_intDepositRateId = $intDepositRateId;
	}

	public function setOtherRateId( $intOtherRateId ) {
		$this->m_intOtherRateId = $intOtherRateId;
	}

	public function setReservationCount( $intReservationCount ) {
		$this->m_intReservationCount = $intReservationCount;
	}

	public function setRentArTriggerId( $intRentArTriggerId ) {
		$this->m_intRentArTriggerId = $intRentArTriggerId;
	}

	public function setDepositArTriggerId( $intDepositArTriggerId ) {
		$this->m_intDepositArTriggerId = $intDepositArTriggerId;
	}

	public function setOtherArTriggerId( $intOtherArTriggerId ) {
		$this->m_intOtherArTriggerId = $intOtherArTriggerId;
	}

	public function setRateLogId( $intRateLogId ) {
		$this->m_intRateLogId = CStrings::strToIntDef( $intRateLogId );
	}

	public function setRentRateLogId( $intRentRateLogId ) {
		$this->m_intRentRateLogId = CStrings::strToIntDef( $intRentRateLogId );
	}

	public function setDepositRateLogId( $intDepositRateLogId ) {
		$this->m_intDepositRateLogId = CStrings::strToIntDef( $intDepositRateLogId );
	}

	public function setOtherRateLogId( $intOtherRateLogId ) {
		$this->m_intOtherRateLogId = CStrings::strToIntDef( $intOtherRateLogId );
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->m_strUnitNumberCache = $strUnitNumberCache;
	}

	public function setReservationDateTime( $strReservationDateTime ) {
		$this->m_strReservationDateTime = $strReservationDateTime;
	}

	public function setFullImageUri( $strFullImageUri ) {
		$this->m_strFullImageUri = $strFullImageUri;
	}

	public function setRateAmount( $intRateAmount ) {
		$this->m_intRateAmount = $intRateAmount;
	}

	public function setArCascadeId( $intArCascadeId ) {
		$this->m_intArCascadeId = $intArCascadeId;
	}

	public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
		$this->m_intArCascadeReferenceId = $intArCascadeReferenceId;
	}

	public function setArOriginId( $intArOriginId ) {
		$this->m_intArOriginId = $intArOriginId;
	}

	public function setArOriginReferenceId( $intArOriginReferenceId ) {
		$this->m_intArOriginReferenceId = $intArOriginReferenceId;
	}

	public function setAddOnGroupDescription( $strAddOnGroupDescription ) {
		$this->m_strAddOnGroupDescription = $strAddOnGroupDescription;
	}

	public function setLockToLeaseDates( $boolLockToLeaseDates ) {
		$this->m_boolLockToLeaseDates = CStrings::strToBool( $boolLockToLeaseDates );
	}

	public function setFirstName( $strFirstName ) {
		$this->m_strFirstName = CStrings::strTrimDef( $strFirstName, -1, NULL, true );
	}

	public function setLastName( $strLastName ) {
		$this->m_strLastName = CStrings::strTrimDef( $strLastName, -1, NULL, true );
	}

	public function setCustomerLeaseStatusTypeName( $strCustomerLeaseStatusTypeName ) {
		$this->m_strCustomerLeaseStatusTypeName = CStrings::strTrimDef( $strCustomerLeaseStatusTypeName, -1, NULL, true );
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = CStrings::strToIntDef( $intLeaseTermId );
	}

	public function setLeaseTermMonth( $intLeaseTermMonth ) {
		$this->m_intLeaseTermMonth = CStrings::strToIntDef( $intLeaseTermMonth );
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setAddOnTypeName( $strAddOnTypeName ) {
		$this->m_strAddOnTypeName = CStrings::strTrimDef( $strAddOnTypeName, 50, NULL, true );
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = CStrings::strTrimDef( $strBuildingName, 50, NULL, true );
	}

	public function setMinRentAmount( $fltMinRentAmount ) {
		$this->m_fltMinRentAmount = CStrings::strToFloatDef( $fltMinRentAmount, NULL, false, 2 );
	}

	public function setMaxRentAmount( $fltMaxRentAmount ) {
		$this->m_fltMaxRentAmount = CStrings::strToFloatDef( $fltMaxRentAmount, NULL, false, 2 );
	}

	public function setJsonRates( $arrobjJsonRates ) {
		$this->m_arrobjJsonRates = $arrobjJsonRates;
	}

	public function setRates( $arrobjRates ) {
		$this->m_arrobjRates = $arrobjRates;
	}

	public function setTaxPercent( $fltTaxPercent ) {
		$this->set( 'm_fltTaxPercent', CStrings::strToFloatDef( $fltTaxPercent, NULL, false, 6 ) );
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->m_fltTaxAmount = CStrings::strToFloatDef( $fltTaxAmount, NULL, false, 2 );
	}

	public function setDepositTaxAmount( $fltDepositTaxAmount ) {
		$this->m_fltDepositTaxAmount = CStrings::strToFloatDef( $fltDepositTaxAmount, NULL, false, 2 );
	}

	public function setOtherTaxAmount( $fltOtherTaxAmount ) {
		$this->m_fltOtherTaxAmount = CStrings::strToFloatDef( $fltOtherTaxAmount, NULL, false, 2 );
	}

	public function addRate( $objRate ) {
		$this->m_arrobjRates[$objRate->getId()] = $objRate;
	}

	public function setIsAutomaticallyIncluded( $boolIsAutomaticallyIncluded ) {
		$this->set( 'm_boolIsAutomaticallyIncluded', CStrings::strToBool( $boolIsAutomaticallyIncluded ) );
	}

	public function setLeaseAssociationMoveOutDate( $strLeaseAssociationMoveOutDate ) {
		$this->set( 'm_strLeaseAssociationMoveOutDate',  $strLeaseAssociationMoveOutDate );
	}

	public function setLeaseAssociationEndDate( $strLeaseAssociationEndDate ) {
		$this->set( 'm_strLeaseAssociationEndDate',  $strLeaseAssociationEndDate );
	}

	public function setAdvertisedRentAmount( $fltAdvertisedRentAmount ) {
		$this->m_fltAdvertisedRentAmount = $fltAdvertisedRentAmount;
	}

	// Validation Functions

	public function valRentAmount() {
		if( $this->getRentAmount() <= 0 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rent_amount', __( 'Rent can not be less than or equal to zero.' ) ) );

			return false;
		}

		return true;
	}

	public function valDepositAmount() {
		if( $this->getDepositAmount() < 0 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deposit_amount', __( 'Deposit can not be less than zero.' ) ) );

			return false;
		}

		return true;
	}

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		if( true == is_null( $this->getUnitSpaceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', __( 'Please select a unit.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAddOnGroupId() {
		return true;
	}

	public function valUnitSpaceStatusTypeId() {
		return true;
	}

	public function valAmenityId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAmenityId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amenity_id', __( 'Amenity is required. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceTemplateId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMaintenanceTemplateId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', __( 'Work order template is required. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valExpireMonths() {
		$boolIsValid = true;

		if( false == is_null( $this->getExpireMonths() ) && false == is_int( $this->getExpireMonths() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expire_months', __( 'Expire months is invalid. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduling() {
		$boolIsValid = true;
		if( true == $this->getAddSchedulingToThisService() ) {
			if( false == valId( $this->getFrequencyId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', __( 'Frequency and scheduling is required. ' ) ) );
			} else if( true == valId( $this->getFrequencyId() ) && \CFrequency::ONCE == $this->getFrequencyId() ) {
				if( false == valArr( $this->getDaysOfWeek() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_of_week', __( 'Please select the scheduled day of service. ' ) ) );
				}
			} else if( true == valId( $this->getFrequencyId() ) && \CFrequency::DAILY == $this->getFrequencyId() ) {
				if( \CFrequency::WEEKLY == $this->getRepeatFrequencyId() ) {
					if( false == valArr( $this->getDaysOfWeek() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_of_week', __( 'Please select the scheduled day of service. ' ) ) );
					}
					if( false == valId( $this->getOccurrenceId() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrence_id', __( 'Occurrences in a week is required. ' ) ) );
					}
					if( true == valArr( $this->getDaysOfWeek() ) && \Psi\Libraries\UtilFunctions\count( $this->getDaysOfWeek() ) < $this->getOccurrenceId() ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrence_id', __( 'Occurrences in a week cannot be greater than the number of available days of the week. ' ) ) );
					}
					if( false == valId( $this->getScheduledBy() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_by', __( 'Service to be scheduled by is required. ' ) ) );
					}
				}

				if( \CFrequency::WEEKDAY_OF_MONTH == $this->getRepeatFrequencyId() ) {
					if( false == valArr( $this->getDaysOfWeek() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_of_week', __( 'Please select the scheduled day of service. ' ) ) );
					}
					if( false == valArr( $this->getRepeatEvery() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'repeat_every', __( 'Every required. ' ) ) );
					}
					if( false == valId( $this->getScheduledBy() ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_by', __( 'Service to be scheduled by is required. ' ) ) );
					}
				}
			}
		}
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		return true;
	}

	public function valLookupCode() {
		return true;
	}

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		return true;
	}

	public function valXPos() {
		return true;
	}

	public function valYPos() {
		return true;
	}

	public function valAvailableOn() {
		return true;
	}

	public function valHoldUntilDate() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valIsHidden() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function valTaxAmount() {
		return true;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolValidateAvailableOnForFuture = false, $boolIsForUpdateFlag = false, $intOldUnitSpaceId = NULL, $intIsUnitAttached = 0 ) {
		$boolIsValid = true;
		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objClientDatabase, $strAction );
				break;

			case 'validate_add_on':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valAvailability();
				$boolIsValid &= $this->valAvailableOnDate( $boolValidateAvailableOnForFuture, $boolIsForUpdateFlag );
				$boolIsValid &= $this->valNameDependantInformation( $objClientDatabase );
				$boolIsValid &= $this->valDependantInformation( $objClientDatabase, $strAction, $intOldUnitSpaceId, $intIsUnitAttached );
				break;

			case 'validate_feature_upgrade':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valAmenityId();
				$boolIsValid &= $this->valMaintenanceTemplateId();
				$boolIsValid &= $this->valExpireMonths();
				break;

			case 'validate_scheduling':
				$boolIsValid &= $this->valScheduling();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objClientDatabase = NULL, $strAction, $intOldUnitSpaceId = NULL, $intIsUnitAttached = 0 ) {

		if( false == valObj( $objClientDatabase, 'CDatabase' ) || true == is_null( $objClientDatabase ) ) {
			return true;
		}

		$boolIsValid = true;

		if( 1 == $intIsUnitAttached ) {
			$boolIsValid &= $this->valUnitSpaceId();
		}

		if( false == $boolIsValid ) {
			return false;
		}

		// if unit change from RI
		if( false == is_null( $this->m_intUnitSpaceId ) && $intOldUnitSpaceId != $this->m_intUnitSpaceId ) {
			// find out if any current RIR exists with old unit space
			$arrobjAddOnLeaseAssociations = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationsByUnitSpaceIdByLeaseIdByPropertyIdByCid( $intOldUnitSpaceId, NULL, $this->m_intPropertyId, $this->m_intCid, [ CLeaseStatusType::CURRENT ], $objClientDatabase, $this->getId() );

			if( true == valArr( $arrobjAddOnLeaseAssociations ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', __( 'This Rentable Item is currently reserved with unit. Unable to change unit.' ) ) );

				return $boolIsValid;
			}
		}

		if( $strAction == VALIDATE_DELETE || ( true == valId( $this->m_intUnitSpaceId ) && $intOldUnitSpaceId != $this->m_intUnitSpaceId ) ) {
			if( false == valId( $this->getAddOnTypeId() ) ) {
				$objAddOnGroup = \Psi\Eos\Entrata\CAddOnGroups::createService()->fetchAddOnGroupByIdByCid( $this->getAddOnGroupId(), $this->getCid(), $objClientDatabase );
				$this->setAddOnTypeId( $objAddOnGroup->getAddOnTypeId() );
			}

			$strMessage = '';
			if( CAddOnType::RENTABLE_ITEMS == $this->getAddOnTypeId() ) {
				$strMessage = __( 'Rentable item' );
			}

			if( CAddOnType::ASSIGNABLE_ITEMS == $this->getAddOnTypeId() ) {
				$strMessage = __( 'Assignable item' );
			}

			if( CAddOnType::SERVICES == $this->getAddOnTypeId() ) {
				$strMessage = __( 'Service' );
			}

			if( CAddOnType::SERVICES == $this->getAddOnTypeId() ) {
				$arrobjAddOnRateAssociations = \Psi\Eos\Entrata\Custom\CAddOnRateAssociations::createService()->fetchRateAssociationsByArCascadeIdByArOriginIdByPropertyIdByCid( CArCascade::SPACE, CArOrigin::ADD_ONS, $this->getPropertyId(), $this->getCid(), $objClientDatabase, $this->getId() );

				if( true == valArr( $arrobjAddOnRateAssociations ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'This {%s,0} is reserved by a resident(s) and/or lead(s). All current & future reservations must be moved-out or cancelled in order to delete this item.', [ $strMessage ] ) ) );
				}
			}

			if( $strAction == VALIDATE_DELETE && false == is_null( $this->m_intUnitSpaceId ) ) {

				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s,0} cannot be deleted because other information depends on it ( Leases/AddOns/Quotes/Unit ).', [ $strMessage ] ) ) );

				return $boolIsValid;
			}

			$intAddOnLeaseAssociationsCount = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationsCountByAddOnIdByCid( $this->getId(), $this->getCid(), $objClientDatabase, true );

			if( 0 < $intAddOnLeaseAssociationsCount ) {
				$boolIsValid &= false;

				$strMsg = ( $strAction == VALIDATE_DELETE ) ? __( 'delete this item.' ) : __( 'attach this item to a unit.' );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'This {%s,0} is reserved by a resident(s) and/or lead(s). All current & future reservations must be moved-out or cancelled in order to {%s,1} ', [ $strMessage, $strMsg ] ) ) );

				return $boolIsValid;
			}

			$arrobjQuoteAddOnLeaseAssociations = \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationByAddOnIdByleaseStatusTypeIdByCid( $this->getId(), [ CLeaseStatusType::APPLICANT ], $this->getCid(), $objClientDatabase );

			if( $strAction == VALIDATE_DELETE && true == valArr( $arrobjQuoteAddOnLeaseAssociations ) ) {

				$arrintQuoteIds = array_filter( array_keys( rekeyObjects( 'QuoteId', $arrobjQuoteAddOnLeaseAssociations ) ) );

				if( true == valArr( $arrintQuoteIds ) ) {

					$arrobjActiveQuotes = CQuotes::fetchQuotesByIdsByCid( $arrintQuoteIds, $this->getCid(), $objClientDatabase );

					foreach( $arrobjActiveQuotes as $objActiveQuote ) {
						if( true == valObj( $objActiveQuote, 'CQuote' ) && ( true == valId( $objActiveQuote->getAcceptedBy() ) || ( 0 < ( strtotime( $objActiveQuote->getExpiresOn() ) - strtotime( date( 'Y-m-d H:i:s' ) ) ) ) ) ) {
							$boolIsValid &= false;
							break;
						}
					}

				} else {
					$boolIsValid &= false;
				}

				if( false == $boolIsValid ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s,0} cannot be deleted because other information depends on it ( Leases/AddOns/Quotes/Unit ).', [ $strMessage ] ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valAvailableOnDate( $boolValidateAvailableOnForFuture = false, $boolIsForUpdateFlag = false ) {
		$boolIsValid = true;

		$intLength = strlen( trim( $this->getAvailableOn() ) );

		if( 0 < $intLength && false == CValidation::validateDate( $this->getAvailableOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on', __( 'Available on date must be in correct date format.' ) ) );

		} elseif( true == $boolValidateAvailableOnForFuture && false == $boolIsForUpdateFlag && 0 < $intLength && strtotime( $this->getAvailableOn() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on', __( 'Available on date must not be in the past.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAvailability() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitSpaceStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please select availability status.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNameDependantInformation( $objClientDatabase ) {

		$boolIsValid = true;

		$intConflictingAddOnCount = \Psi\Eos\Entrata\CAddOns::createService()->fetchConflictingAddOnCountByCid( $this, $this->getCid(), $objClientDatabase );

		if( 0 < $intConflictingAddOnCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Inventory item name already exist.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// Create Functions

	public function createLeaseAssociation() {

		$objAddOnLeaseAssociation = new CAddOnLeaseAssociation();
		$objAddOnLeaseAssociation->setCid( $this->getCid() );
		$objAddOnLeaseAssociation->setPropertyId( $this->getPropertyId() );
		$objAddOnLeaseAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objAddOnLeaseAssociation->setArCascadeReferenceId( $this->getPropertyId() );
		$objAddOnLeaseAssociation->setArOriginId( CArOrigin::ADD_ONS );
		$objAddOnLeaseAssociation->setArOriginReferenceId( $this->getId() );
		$objAddOnLeaseAssociation->setRentAmount( $this->getRentAmount() );
		$objAddOnLeaseAssociation->setDepositAmount( $this->getDepositAmount() );
		$objAddOnLeaseAssociation->setOtherAmount( $this->getOtherAmount() );
		$objAddOnLeaseAssociation->setAddOnName( $this->getName() );

		if( 0 < $this->getRentAmount() ) {
			$objAddOnLeaseAssociation->setRentArCodeId( $this->getRentArCodeId() );
			$objAddOnLeaseAssociation->setRentRateId( $this->getRentRateId() );
		}

		if( 0 < $this->getDepositAmount() ) {
			$objAddOnLeaseAssociation->setDepositArCodeId( $this->getDepositArCodeId() );
			$objAddOnLeaseAssociation->setDepositRateId( $this->getDepositRateId() );
		}

		if( 0 < $this->getOtherAmount() ) {
			$objAddOnLeaseAssociation->setOtherArCodeId( $this->getOtherArCodeId() );
			$objAddOnLeaseAssociation->setOtherRateId( $this->getOtherRateId() );
		}

		return $objAddOnLeaseAssociation;
	}

	public function createNewLeaseAssociation() {
		$objAddOnLeaseAssociation = new CAddOnLeaseAssociation();
		$objAddOnLeaseAssociation->setCid( $this->getCid() );
		$objAddOnLeaseAssociation->setPropertyId( $this->getPropertyId() );
		$objAddOnLeaseAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objAddOnLeaseAssociation->setArCascadeReferenceId( $this->getPropertyId() );
		$objAddOnLeaseAssociation->setArOriginId( CArOrigin::ADD_ONS );
		$objAddOnLeaseAssociation->setArOriginReferenceId( $this->getId() );
		$objAddOnLeaseAssociation->setRentAmount( $this->getRentAmount() );
		$objAddOnLeaseAssociation->setAddOnName( $this->getName() );

		return $objAddOnLeaseAssociation;
	}

	// Other Functions

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	// NOT SURE IF WE ARE USING THIS FUNCTION - deleteRates

	public function deleteRates( $arrobjRates, $intCurrentUserId, $objDatabase ) {

		foreach( $arrobjRates as $objRate ) {
			if( false == $objRate->delete( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

	public function updateUnitSpaceStatusType( $objDatabase ) {

		// Here we can update the status on the rentable item to whatever is appropriate.
		$strSql = '	UPDATE add_ons ri_new
					SET
						unit_space_status_type_id = new_data.intended_unit_space_status_type_id,
						available_on = new_data.intended_available_on_date
					FROM (
							SELECT * FROM (
											SELECT
												ao.id as add_on_id,
												ao.property_id,
												ao.cid,
												ao.unit_space_status_type_id,
												CASE
													WHEN ao.unit_space_id IS NOT NULL AND us.id IS NOT NULL AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . ' THEN ' . CUnitSpaceStatusType::VACANT_RENTED_NOT_READY . '
													WHEN ao.unit_space_id IS NOT NULL AND us.id IS NOT NULL AND us.unit_space_status_type_id <> ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . ' THEN us.unit_space_status_type_id
													WHEN ao.is_active = FALSE THEN ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . '
													WHEN COALESCE( current_reservation.lease_status_type_id, 0 ) IN ( ' . CLeaseStatusType::CURRENT . ' )
														THEN ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE . '
													WHEN COALESCE( current_reservation.lease_status_type_id, 0 ) = ' . CLeaseStatusType::NOTICE . '
														THEN (	CASE
																	WHEN future_reservation.id IS NOT NULL
																		THEN ' . CUnitSpaceStatusType::NOTICE_RENTED . '
																	ELSE ' . CUnitSpaceStatusType::NOTICE_UNRENTED . '
																END )
													WHEN ( COALESCE( current_reservation.lease_status_type_id, 0 ) = 0 AND future_reservation.id IS NOT NULL )
														THEN ' . CUnitSpaceStatusType::VACANT_RENTED_READY . '
													WHEN ( COALESCE( current_reservation.lease_status_type_id, 0 ) = 0 AND future_reservation.id IS NULL )
														THEN ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . '
												END AS intended_unit_space_status_type_id,
												CASE
													WHEN COALESCE( current_reservation.lease_status_type_id, 0 ) = ' . CLeaseStatusType::NOTICE . ' AND future_reservation.id IS NULL AND current_reservation.end_date IS NOT NULL AND ao.available_on IS NULL
														THEN ( current_reservation.end_date + INTERVAL \'1 day\' )
													ELSE
														ao.available_on
												END As intended_available_on_date
											FROM
												add_ons ao
												LEFT JOIN (
																SELECT
																	la.id,
																	la.cid,
																	la.property_id,
																	la.lease_status_type_id,
																	la.ar_origin_reference_id,
																	la.end_date,
																	rank() OVER ( PARTITION BY la.cid, la.ar_origin_reference_id ORDER BY la.created_on DESC ) as rank
																FROM
																	lease_associations la
																	INNER JOIN add_ons ao ON ( la.ar_origin_reference_id = ao.id AND la.cid = ao.cid)
																WHERE
																	la.cid = ' . ( int ) $this->getCid() . '
																	AND ao.id = ' . ( int ) $this->getId() . '
																	AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
																	AND la.deleted_on IS NULL
																	AND la.deleted_by IS NULL
																	AND la.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) . ' )
															) as current_reservation
													ON ( current_reservation.property_id = ao.property_id AND current_reservation.cid = ao.cid AND current_reservation.ar_origin_reference_id = ao.id AND current_reservation.rank = 1 )
												LEFT JOIN (
																SELECT
																	la.id,
																	la.cid,
																	la.property_id,
																	la.lease_status_type_id,
																	la.ar_origin_reference_id,
																	rank() OVER ( PARTITION BY la.cid, la.ar_origin_reference_id ORDER BY la.created_on DESC ) as rank
																FROM
																	lease_associations la
																	INNER JOIN add_ons ao ON ( la.ar_origin_reference_id = ao.id AND la.cid = ao.cid)
																WHERE
																	la.cid = ' . ( int ) $this->getCid() . '
																	AND ao.id = ' . ( int ) $this->getId() . '
																	AND la.ar_origin_id = ' . CArOrigin::ADD_ONS . '
																	AND la.lease_status_type_id = ' . CLeaseStatusType::FUTURE . '
																	AND la.deleted_on IS NULL
																	AND la.deleted_by IS NULL
															) as future_reservation
													ON ( future_reservation.property_id = ao.property_id AND future_reservation.cid = ao.cid AND future_reservation.ar_origin_reference_id = ao.id AND future_reservation.rank = 1 )
													LEFT JOIN unit_spaces us ON ( us.cid = ao.cid AND us.property_id = ao.property_id AND us.id = ao.unit_space_id )
											WHERE ao.id = ' . ( int ) $this->getId() . '
												AND ao.cid = ' . ( int ) $this->getCid() . '
										) AS sub WHERE sub.cid = ' . ( int ) $this->getCid() . '
						) new_data
					WHERE new_data.add_on_id = ri_new.id
						AND new_data.cid = ri_new.cid
						AND ri_new.id = ' . $this->getId() . '
						AND ri_new.cid = ' . $this->getCid();

		return fetchData( $strSql, $objDatabase );
	}

	public function updateAddOnStatusType( $intCurrentUserId, $objDatabase ) {
        $strSql 			= 'SELECT update_add_on_status_type( ARRAY[ ' . ( int ) $this->getCid() . ' ]::INT[ ], ARRAY[ ' . ( int ) $this->getPropertyId() . ' ]::INT[ ], ARRAY[ ' . ( int ) $this->getId() . ' ]::INT[ ] , ' . ( int ) $intCurrentUserId . ' ) AS count';
		$arrintAffectedRows = fetchData( $strSql, $objDatabase );
		if( 0 > $arrintAffectedRows[0]['count'] ) {
			return false;
		}

		return true;
	}

	public function deleteAddon( $intAddonTypeId, $intPropertyId, $intCurrentUserId, $objDatabase ) {
		if( 0 >= ( int ) $intAddonTypeId || 0 >= ( int ) $intPropertyId ) {
			return false;
		}

		/**
		 *    Comment: DELETE ADD ON
		 *    1. Delete rates function
		 *    2. Delete rate associations
		 *    3. Delete Add on
		 */

		// Rates Criteria

		$objRatesCriteria = new CRatesCriteria();

		$objRatesCriteria->setCid( $this->getCid() );
		$objRatesCriteria->setPropertyId( $intPropertyId );
		$objRatesCriteria->setArCascadeId( CArCascade::PROPERTY );

		$objRatesCriteria->setArOriginId( CArOrigin::ADD_ONS );
		$objRatesCriteria->setArOriginReferenceId( $this->getId() );

		$objRatesLibrary = new CRatesLibrary( $objRatesCriteria );

		// Rate Associations
		$arrobjAddOnRateAssociations = ( array ) \Psi\Eos\Entrata\CRateAssociations::createService()->fetchRateAssociationsByArCascadeIdByArOriginIdByArOriginReferenceIdByPropertyIdByCid( CArCascade::PROPERTY, CArOrigin::ADD_ONS, $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );

		// Rates Criteria
		$objRatesCriteria = new CRatesCriteria();

		$objRatesCriteria->setCid( $this->getCid() );
		$objRatesCriteria->setPropertyId( $intPropertyId );
		$objRatesCriteria->setArCascadeId( CArCascade::PROPERTY );

		$objRatesCriteria->setArOriginId( CArOrigin::ADD_ONS );
		$objRatesCriteria->setArOriginReferenceId( $this->getId() );
		$objRatesCriteria->setAddOnTypeId( $intAddonTypeId );

		$objRatesLibrary = new CRatesLibrary( $objRatesCriteria );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( false == $objRatesLibrary->deleteRates( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}

				foreach( $arrobjAddOnRateAssociations as $objAddOnRateAssociation ) {

					if( false == $objAddOnRateAssociation->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}

				if( false == $this->delete( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to delete the associated data.' ) ) );
		}

		return $boolIsValid;
	}

	// Fetch Functions

	public function fetchRateAssociation( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAddOnRateAssociations::createService()->fetchRateAssociationByAddOnIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function getWeekDaysAbbr() {
		return [
			self::SUNDAY    => __( 'Sun' ),
			self::MONDAY    => __( 'Mon' ),
			self::TUESDAY   => __( 'Tue' ),
			self::WEDNESDAY => __( 'Wed' ),
			self::THURSDAY  => __( 'Thu' ),
			self::FRIDAY    => __( 'Fri' ),
			self::SATURDAY  => __( 'Sat' )
		];
	}

	public function getWeekDays() {
		return [
			self::SUNDAY    => __( 'Sunday' ),
			self::MONDAY    => __( 'Monday' ),
			self::TUESDAY   => __( 'Tuesday' ),
			self::WEDNESDAY => __( 'Wednesday' ),
			self::THURSDAY  => __( 'Thursday' ),
			self::FRIDAY    => __( 'Friday' ),
			self::SATURDAY 	=> __( 'Saturday' )
		];
	}

	public function getWeeksOfMonthPrefix( $intWeekOfMonth ) {
		switch( $intWeekOfMonth ) {
			case 1:
				return __( '1st' );
				break;

			case 2:
				return __( '2nd' );
				break;

			case 3:
				return __( '3rd' );
				break;

			case 4:
				return __( '4th' );
				break;

			case 5:
				return __( 'Last' );
				break;

			default:
				// default case
				break;
		}
	}

	public function getServiceFrequency() {
		$strServiceFrequency = '';
		switch( $this->getFrequencyId() ) {
			case \CFrequency::ONCE:
				$strServiceFrequency = __( 'one-time' );
				break;

			case \CFrequency::DAILY:
				switch( $this->getRepeatFrequencyId() ) {
					case \CFrequency::DAILY:
						$strServiceFrequency = __( 'daily' );
						break;

					case \CFrequency::WEEKLY:
						if( 1 == $this->getOccurrenceId() ) {
							$strDays = __( '{%d,0} day', [ $this->getOccurrenceId() ] );
						} else {
							$strDays = __( '{%d,0} days', [ $this->getOccurrenceId() ] );
						}
						$strServiceFrequency = __( '{%s,0} per week', [ $strDays ] );
						break;

					case \CFrequency::WEEKDAY_OF_MONTH:
						$arrintWeekOfMonth = [];
						foreach( $this->getRepeatEvery() as $intWeekOfMonth ) {
							$arrintWeekOfMonth[] = $this->getWeeksOfMonthPrefix( $intWeekOfMonth );
						}
						$strWeeks = implode( ',', $arrintWeekOfMonth );
						if( 1 == count( $arrintWeekOfMonth ) ) {
							$strWeeks = __( '{%s,0} week', [ $strWeeks ] );
						} else {
							$strWeeks = __( '{%s,0} weeks', [ $strWeeks ] );
						}
						$strServiceFrequency = __( '{%s,0} of the month', [ $strWeeks ] );
						break;

					default:
						// default case
						break;
				}
				break;

			default:
				// default case
				break;
		}
		return $strServiceFrequency;
	}

}

?>