<?php

class CCompanyPreference extends CBaseCompanyPreference {

	private $m_arrstrActiveDirectoryDisplayNameFormat;

	const MIN_TIME_OUT_VALUE                = 5; // minutes
	const MIN_PASSWORD_LENGTH               = 8;
	const MAX_PASSWORD_LENGTH               = 50;
	const SAML_USERNAME_TYPE_PLAIN_USERNAME = 0;
	const SAML_USERNAME_TYPE_EMAIL_ADDRESS  = 1;
	const SAML_USERNAME_TYPE_MIXED_USERNAME = 2;

	// To prevent users from reusing a specified number of previous passwords.
	const MIN_PASSWORD_REUSE_NUMBER				= 3;
	const MAX_PASSWORD_REUSE_NUMBER				= 10;
	const DEFAULT_PASSWORD_REUSE_NUMBER			= 4;

	const DEFAULT_SYSTEM_EMAIL_HISTORY_LIMIT	= 12;

	const DONT_ALLOW_EXEMPTION           = 'DONT_ALLOW_EXEMPTION';
	const EXCLUDED_EXEMPTION_TIME_FRAMES = 'EXCLUDED_EXEMPTION_TIME_FRAMES';
	const IS_REPORT_API_ENABLED          = 'IS_REPORT_API_ENABLED';

	const FIXED_ASSET_REQUIRE_WARRANTY_INFO = 'REQUIRE_WARRANTY_INFO';

	const VENDOR_ACCESS_AUTO_INVITE_EMAIL                   = 'VENDOR_ACCESS_AUTO_INVITE_EMAIL';
	const VENDOR_ACCESS_AUTO_INVITE_REMOTE                  = 'VENDOR_ACCESS_AUTO_INVITE_REMOTE';
	const VENDOR_ACCESS_AUTO_INVITE_ONSITE                  = 'VENDOR_ACCESS_AUTO_INVITE_ONSITE';
	const VENDOR_ACCESS_AUTO_INVITE_USER_ID                 = 'VENDOR_ACCESS_AUTO_INVITE_USER_ID';
	const VENDOR_ACCESS_AUTO_SYNCED_NOTIFICATION_USER_IDS   = 'VENDOR_ACCESS_AUTO_SYNCED_NOTIFICATION_USER_IDS';

	const THIRD_PARTY_VENDOR_VERIFICATION              		= 'THIRD_PARTY_VENDOR_VERIFICATION';

	const COMPLIANCE_ITEM_REVIEW_NOTIFICATION_EMAIL	        = 'COMPLIANCE_ITEM_REVIEW_NOTIFICATION_EMAIL';
	const COMPLIANCE_ITEM_REVIEW_NOTIFICATION_EMPLOYEE_ID	= 'COMPLIANCE_ITEM_REVIEW_NOTIFICATION_EMPLOYEE_ID';

	const COMPLIANCE_ITEMS_ABOUT_TO_EXPIRE_TIMEFRAME	    = 'COMPLIANCE_ITEMS_ABOUT_TO_EXPIRE_TIMEFRAME';
	const COMPLIANCE_ITEMS_EXPIRE_NOTIFICATION_EMPLOYEE_IDS	= 'COMPLIANCE_ITEMS_EXPIRE_NOTIFICATION_EMPLOYEE_IDS';
	const DO_NOT_REQUIRE_COMPLIANCE_INSURANCE_DOCUMENT      = 'DO_NOT_REQUIRE_COMPLIANCE_INSURANCE_DOCUMENT';

	const KEY_USE_ITEM_CATALOG								= 'USE_ITEM_CATALOG';

	const KEY_ENTRATA_PRIMARY_CONTACT_EMAIL_CONTENT         = 'ENTRATA_PRIMARY_CONTACT_EMAIL_CONTENT';
	const KEY_ENTRATA_HELPDESK_SUPPORT_EMAIL                = 'ENTRATA_HELPDESK_SUPPORT_EMAIL';

	const PENDING_CATALOG_NOTIFICATION_RECIPIENTS_EMPLOYEE_IDS	= 'PENDING_CATALOG_NOTIFICATION_RECIPIENTS_EMPLOYEE_IDS';

	const ALLOW_CREDITS_TO_CROSS_APPLY_PROPERTIES				= 'ALLOW_CREDITS_TO_CROSS_APPLY_PROPERTIES';

	const ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS	= 'ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS';
	const ALLOW_DUPLICATE_INVOICE_NUMBERS_PER_VENDOR            = 'ALLOW_DUPLICATE_INVOICE_NUMBERS_PER_VENDOR';
	const SHOW_SETTING_TEMPLATE 								= 'SHOW_SETTING_TEMPLATE';
	const REQUIRE_ATTACHMENTS_FOR_INVOICES 						= 'REQUIRE_ATTACHMENTS_FOR_INVOICES';

	const APPLY_SALES_TAX_IN_AP = 'APPLY_SALES_TAX_IN_AP';
	const APPLY_SHIPPING_IN_AP  = 'APPLY_SHIPPING_IN_AP';
	const APPLY_DISCOUNTS_IN_AP = 'APPLY_DISCOUNTS_IN_AP';

	const ALLOCATE_SHIPPING_TO_GL_ACCOUNT = 'ALLOCATE_SHIPPING_TO_GL_ACCOUNT';
	const ALLOCATE_SALES_TAX_TO_GL_ACCOUNT = 'ALLOCATE_SALES_TAX_TO_GL_ACCOUNT';
	const ALLOCATE_SHIPPING_TO_ACROSS_LINE_ITEMS = 'ALLOCATE_SHIPPING_TO_ACROSS_LINE_ITEMS';
	const ALLOCATE_SALES_TAX_TO_ACROSS_LINE_ITEMS = 'ALLOCATE_SALES_TAX_TO_ACROSS_LINE_ITEMS';

	public static $c_arrstrVendorInvitationsPreferences = [
		CCompanyPreference::VENDOR_ACCESS_AUTO_INVITE_REMOTE                => CCompanyPreference::VENDOR_ACCESS_AUTO_INVITE_REMOTE,
		CCompanyPreference::VENDOR_ACCESS_AUTO_INVITE_ONSITE                => CCompanyPreference::VENDOR_ACCESS_AUTO_INVITE_ONSITE,
		CCompanyPreference::VENDOR_ACCESS_AUTO_INVITE_USER_ID               => CCompanyPreference::VENDOR_ACCESS_AUTO_INVITE_USER_ID,
		CCompanyPreference::VENDOR_ACCESS_AUTO_INVITE_EMAIL                 => CCompanyPreference::VENDOR_ACCESS_AUTO_INVITE_EMAIL,
		CCompanyPreference::VENDOR_ACCESS_AUTO_SYNCED_NOTIFICATION_USER_IDS => CCompanyPreference::VENDOR_ACCESS_AUTO_SYNCED_NOTIFICATION_USER_IDS
	];

	public static function assignPOInvoiceSubTotalsPreferences( $arrmixTemplateParameters = [] ) {
		$arrmixTemplateParameters['ALLOCATE_SHIPPING_TO_GL_ACCOUNT']         = self::ALLOCATE_SHIPPING_TO_GL_ACCOUNT;
		$arrmixTemplateParameters['ALLOCATE_SALES_TAX_TO_GL_ACCOUNT']        = self::ALLOCATE_SALES_TAX_TO_GL_ACCOUNT;
		$arrmixTemplateParameters['ALLOCATE_SHIPPING_TO_ACROSS_LINE_ITEMS']  = self::ALLOCATE_SHIPPING_TO_ACROSS_LINE_ITEMS;
		$arrmixTemplateParameters['ALLOCATE_SALES_TAX_TO_ACROSS_LINE_ITEMS'] = self::ALLOCATE_SALES_TAX_TO_ACROSS_LINE_ITEMS;

		return $arrmixTemplateParameters;
	}

	/**
	 * @return CCompanyPreference
	 *
	 */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getActiveDirectoryDisplayNameFormat() {
		$this->m_arrstrActiveDirectoryDisplayNameFormat = [
			1 => [ 'label' => __( 'Firstname Lastname' ), 'separator' => ' ', 'sequence' => 'FL' ],
			2 => [ 'label' => __( 'Lastname, Firstname' ), 'separator' => ',', 'sequence' => 'LF' ]
		];

		return $this->m_arrstrActiveDirectoryDisplayNameFormat;
	}

	public static $c_arrstrReviewNotificationPreferences = [
		CCompanyPreference::COMPLIANCE_ITEM_REVIEW_NOTIFICATION_EMAIL,
		CCompanyPreference::COMPLIANCE_ITEM_REVIEW_NOTIFICATION_EMPLOYEE_ID
	];

	public static $c_arrstrComplianceItemsExpireNotificationPreferences = [
		CCompanyPreference::COMPLIANCE_ITEMS_ABOUT_TO_EXPIRE_TIMEFRAME,
		CCompanyPreference::COMPLIANCE_ITEMS_EXPIRE_NOTIFICATION_EMPLOYEE_IDS,
		CCompanyPreference::DO_NOT_REQUIRE_COMPLIANCE_INSURANCE_DOCUMENT
	];

	public function valThirdPartyApplicationRedirectionUrl() {
		$boolIsValid = true;

		if( false == CValidation::checkUrl( trim( $this->getValue() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'THIRD_PARTY_APPLICATION_REDIRECTION', __( '3rd Party Redirection url does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNumeric() {
		$boolIsValid = true;
		if( false == is_null( $this->getValue() ) && false == \Psi\CStringService::singleton()->preg_match( '/^\d+$/', $this->getValue() ) ) {
			$boolIsValid = false;

			if( 'NO_OF_PROPERTIES_TO_SEND_IN_DIFFERENTIAL' == $this->getKey() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getKey(), __( 'Please enter valid number of properties.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getKey(), __( 'Please enter valid days.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valInvoiceSubTotalGlAccounts( $arrobjCompanyPreferences ) {

		$boolIsValid = true;

		if( false == valArr( $arrobjCompanyPreferences ) ) {
			return $boolIsValid;
		}

		if( 'ALLOCATE_SALES_TAX_TO_GL_ACCOUNT' == $arrobjCompanyPreferences['ALLOCATE_SALES_TAX_TO']->getValue() && false == array_key_exists( 'SALES_TAX_GL_ACCOUNT', $arrobjCompanyPreferences ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Please select sales tax Gl Account.' ) ) );
		}

		if( 'ALLOCATE_SHIPPING_TO_GL_ACCOUNT' == $arrobjCompanyPreferences['ALLOCATE_SHIPPING_TO']->getValue() && false == array_key_exists( 'SHIPPING_GL_ACCOUNT', $arrobjCompanyPreferences ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Please select shipping Gl Account.' ) ) );
		}

		if( 'ALLOCATE_DISCOUNT_TO_GL_ACCOUNT' == $arrobjCompanyPreferences['ALLOCATE_DISCOUNT_TO']->getValue() && false == array_key_exists( 'DISCOUNT_GL_ACCOUNT', $arrobjCompanyPreferences ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Please select discount Gl Account.' ) ) );
		}

		return $boolIsValid;
	}

	public function valValue( $strFieldName = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getKey(), __( 'Please enter valid  {%s, 0,}.', [ $strFieldName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPostDateRange() {

		$boolIsValid = true;

		if( ( false == is_null( $this->getValue() ) && 0 == $this->getValue() ) || '' == $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getKey(), __( 'Please enter a positive number between {%d, 0} - {%d, 1, nots}.', [ 1, 9999 ] ) ) );
		}

		return $boolIsValid;
	}

	public function valMaxAllowedPaymentsPerBatch() {

		$boolIsValid = true;

		if( true == is_null( $this->getValue() ) || 0 >= $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getKey(), __( 'Max Allowed Payments Per Batch must be greater than {%d, 0}', [ 0 ] ) ) );
		}
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == valStr( $this->getValue() ) || ( false == CValidation::validateEmailAddresses( $this->getValue() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'The email address does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrobjCompanyPreferences = [] ) {
		$boolIsValid = true;

		$arrstrKeys = [ 'NUMBER_OF_DAYS_BEFORE_MOVE_IN_DATE', 'NUMBER_OF_DAYS_BEFORE_LEASE_EXPIRATION', 'NO_OF_PROPERTIES_TO_SEND_IN_DIFFERENTIAL' ];

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= ( ( 'THIRD_PARTY_APPLICATION_REDIRECTION' == $this->getKey() && '' != trim( $this->getValue() ) ) ? $this->valThirdPartyApplicationRedirectionUrl() : true );
				$boolIsValid &= ( ( 'RESIDENT_WORKS_SESSION_TIMEOUT' == $this->getKey() )  ?  $this->valMaxLoginTimeouts( 'Entrata' ) : true );
				$boolIsValid &= ( ( 'RESIDENT_PORTAL_SESSION_TIMEOUT' == $this->getKey() ) ? $this->valMaxLoginTimeouts( 'Resident Portal' ) : true );
				$boolIsValid &= ( ( 'PROSPECT_PORTAL_SESSION_TIMEOUT' == $this->getKey() ) ? $this->valMaxLoginTimeouts( 'Prospect Portal' ) : true );
				$boolIsValid &= ( ( 'OWNER_PORTAL_SESSION_TIMEOUT' == $this->getKey() ) ? $this->valMaxLoginTimeouts( 'Owner Portal' ) : true );
				$boolIsValid &= ( ( 'EMPLOYEE_PORTAL_SESSION_TIMEOUT' == $this->getKey() ) ? $this->valMaxLoginTimeouts( 'Employee Portal' ) : true );
				$boolIsValid &= ( ( 'MINIMUM_PASSWORD_LENGTH' == $this->getKey() ) ? $this->valMinPasswordLength() : true );
				$boolIsValid &= ( ( 'PREVENT_PASSWORD_REUSE_NUMBER' == $this->getKey() && true == valStr( $this->getValue() ) ) ? $this->valPasswordReuseNumber() : true );
				$boolIsValid &= ( 'MAX_ALLOWED_PAYMENTS_PER_BATCH' == $this->getKey() ? $this->valMaxAllowedPaymentsPerBatch() : true );
				$boolIsValid &= ( 'IS_CAPTURE_INVOICE_TOTAL_ONLY' == $this->getKey() ? $this->valMaxAllowedPaymentsPerBatch() : true );
				$boolIsValid &= ( true == in_array( $this->getKey(), $arrstrKeys ) ? $this->valNumeric() : true );
				$boolIsValid &= $this->valInvoiceSubTotalGlAccounts( $arrobjCompanyPreferences );
				$boolIsValid &= ( 'VENDOR_ACCESS_AUTO_INVITE_EMAIL' == $this->getKey() ? $this->valVendorAccessAutoInviteEmail() : true );
				$boolIsValid &= ( self::KEY_ENTRATA_HELPDESK_SUPPORT_EMAIL == $this->getKey() ? $this->valEmailAddress() : true );
				$boolIsValid &= ( ( 'LEAD_SOURCE_COOKIE_DURATION' == $this->getKey() ) ? $this->validateLeadSourceCookieDuration() : true );
				break;

			// To validate ACCESS_CONTROL_WHITELISTED_IPS key data
			case 'validate_whitelist_ips':
				$boolIsValid &= $this->validateWhitelistedIpAddresses();
				break;

			case 'validate_use_item_catalog':
				$boolIsValid &= $this->validateUseItemCatalog( $arrobjCompanyPreferences );
				break;

			case 'validate_use_asset_tracking':
				$boolIsValid &= $this->validateUseAssetTracking( $arrobjCompanyPreferences );
				break;

			case 'validate_saml_configuration':
				$boolIsValid &= ( ( 'SAML_SINGLE_SIGN_ON_URL' == $this->getKey() ) ? $this->valValue( 'SAML SSO Url' ) : true );
				$boolIsValid &= ( ( 'SAML_SINGLE_SIGN_OUT_URL' == $this->getKey() ) ? $this->valValue( 'SAML SignOut Url' ) : true );
				$boolIsValid &= ( ( 'SAML_CERT_FINGERPRINT' == $this->getKey() ) ? $this->valValue( 'Certificate Fingerprint' ) : true );
				break;

			case 'validate_oidc_configuration':
				$boolIsValid &= ( ( 'OIDC_PROVIDER' == $this->getKey() ) ? $this->valValue( 'OIDC Provider' ) : true );
				$boolIsValid &= ( ( 'OIDC_ENDPOINT' == $this->getKey() ) ? $this->valValue( 'OIDC Endpoint' ) : true );
				$boolIsValid &= ( ( 'OIDC_CLIENT_ID' == $this->getKey() ) ? $this->valValue( 'OIDC Client ID' ) : true );
				$boolIsValid &= ( ( 'OIDC_CLIENT_SECRET' == $this->getKey() ) ? $this->valValue( 'OIDC Client Secret' ) : true );
				break;

			case 'validate_post_date_range':
				$boolIsValid &= ( ( 'WARN_FOR_POST_DATE_OUTSIDE_EXPECTED_RANGE' == $this->getKey() ) ? $this->valPostDateRange() : true );
				break;

			case 'validate_default_line_items':
				$boolIsValid &= ( ( 'DEFAULT_JE_LINE_ITEMS' == $this->getKey() ) ? $this->valDefaultLineItems() : true );
				break;

			case 'validate_merchant_account_advanced_setting':
				$boolIsValid &= $this->validateAdvancedSetting();
				break;

			case 'validate_doc_scan_configuration':
				$boolIsValid &= $this->validateDocScanSsoSetting();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validateDocScanSsoSetting() {
		$boolIsValid = true;
		if( false == valStr( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'configurations', __( 'Please enter configuration details.' ) ) );
			return $boolIsValid;
		}
		$arrstrDocScanSsoValidateKeys = [ 'client_id', 'client_secret', 'sso_region' ];
		$arrstrMessageLabels = [ 'client_id' => __( 'Client Id' ), 'client_secret' => __( 'Client Secret' ), 'sso_region' => __( 'Region' ) ];
		$arrstrConfigurations = json_decode( $this->getValue(), true );
		if( false != valArr( $arrstrConfigurations ) && false != isset( $arrstrConfigurations['default_sso_provider'] ) ) {
			$strErrorMessage = __( 'Please enter valid&nbsp' );
			$arrstrKey = [];
			$strInvalidRegionMessage = NULL;
			foreach( $arrstrConfigurations[$arrstrConfigurations['default_sso_provider']] as $strKey => $strConfigurationValue ) {
				if( false != in_array( $strKey, $arrstrDocScanSsoValidateKeys ) && false == valStr( $strConfigurationValue ) ) {
					$boolIsValid = false;
					$arrstrKey[$strKey] = $arrstrMessageLabels[$strKey];
				}

				// Validation Reference: RFC 1035: Domain Implementation and Specification
				if( 'sso_region' == $strKey && ( 63 < mb_strlen( $strConfigurationValue ) || false != \Psi\CStringService::singleton()->preg_match( '/[^A-Za-z0-9-]/', $strConfigurationValue ) ) ) {
					$boolIsValid = false;
					$arrstrKey[$strKey] = $arrstrMessageLabels[$strKey];
					$strInvalidRegionMessage = __( ' The region should contain only alphabets, digits or hyphen (-).' );
				}
			}
			if( false == $boolIsValid && false != valArr( $arrstrKey ) ) {
				$strError = $strErrorMessage . implode( ', ', $arrstrKey ) . '.' . $strInvalidRegionMessage;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strError ) );
			}
		}
		return $boolIsValid;
	}

	public function validateWhitelistedIpAddresses() {
		$boolIsValid 		= true;
		$strIpAddressList 	= $this->getValue();

		if( true == valStr( $strIpAddressList ) ) {
			$arrstrIpAddressList = explode( "\r\n", $strIpAddressList );
			$strRemoteAddress	 = getRemoteIpAddress();

			foreach( $arrstrIpAddressList as $strIpAddress ) {
				$arrstrAccessibleIpAddressRange = [];
				if( false !== \Psi\CStringService::singleton()->strpos( $strIpAddress, '-' ) ) {
					if( 1 < substr_count( $strIpAddress, '-' ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ACCESS_CONTROL_WHITELISTED_IPS', __( 'Invalid IP address range.' ) ) );
						break;
					}
					$arrstrAccessibleIpAddressRange = explode( '-', trim( $strIpAddress ) );

				} elseif( false !== \Psi\CStringService::singleton()->strpos( $strIpAddress, '/' ) ) {
					$strCidrPattern = '/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([1-9]|[1-2][0-9]|3[0-2]))$/m';
					list( $strIp, $strIpmask ) = explode( '/', $strIpAddress );
					if( false == \Psi\CStringService::singleton()->preg_match( $strCidrPattern, $strIpAddress ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ACCESS_CONTROL_WHITELISTED_IPS', __( 'Invalid CIDR range.' ) ) );
						break;
					}

					$arrstrAccessibleIpAddressRange = getIpRangeByCidrIpFormat( trim( $strIpAddress ) );
				}

				if( true == valArr( $arrstrAccessibleIpAddressRange ) ) {
					$strIp1 = explode( '.', trim( $arrstrAccessibleIpAddressRange[0] ) );
					$strIp2 = explode( '.', trim( $arrstrAccessibleIpAddressRange[1] ) );

					if( trim( $strIp1[0] ) != trim( $strIp2[0] ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ACCESS_CONTROL_WHITELISTED_IPS', __( 'Invalid IP address range.' ) ) );
						break;
					}

					$boolIsValidIpAddress = true;
					foreach( $arrstrAccessibleIpAddressRange as $arrstrAccessibleIpAddress ) {
						$boolIsValidIpAddress &= CValidation::validateIpAddress( trim( $arrstrAccessibleIpAddress ) );
					}
				} else {
					$boolIsValidIpAddress = CValidation::validateIpAddress( trim( $strIpAddress ) );
				}

				if( false == $boolIsValidIpAddress ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ACCESS_CONTROL_WHITELISTED_IPS', __( 'Invalid IP address.' ) ) );
					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function valMaxLoginTimeouts( $strPortalName = '' ) {
		$boolIsValid 		= true;
		$intGcMaxlifetime 	= floor( ini_get( 'session.gc_maxlifetime' ) / 60 );

		if( 0 >= ( int ) $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL_VALUE', __( '{%s, 0} Timeout value should not be blank.', [ $strPortalName ] ) ) );
		} elseif( self::MIN_TIME_OUT_VALUE > trim( $this->getValue() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL_VALUE', __( '{%s, 0} Timeout value should be more than {%d, 1} minutes.', [ $strPortalName, self::MIN_TIME_OUT_VALUE ] ) ) );

		} elseif( $intGcMaxlifetime < trim( $this->getValue() ) ) {
			$boolIsValid = false;

			if( 60 <= $intGcMaxlifetime ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'MAX_TIMEOUT_VALUE', __( '{%s, 0} Timeout value should not exceed {%d, 1} hours.&nbsp', [ $strPortalName, ( ( int ) $intGcMaxlifetime / 60 ) ] ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'MAX_TIMEOUT_VALUE', __( '{%s, 0} Timeout value should not exceed {%d, 1} minutes.&nbsp', [ $strPortalName, ( int ) $intGcMaxlifetime ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMinPasswordLength() {
		$boolIsValid = true;

		if( 0 == ( int ) $this->getValue() || self::MIN_PASSWORD_LENGTH > ( int ) $this->getValue() || self::MAX_PASSWORD_LENGTH < ( int ) $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL_VALUE', __( 'Please set minimum password length from {%d, 0} to {%d, 1}.', [ self::MIN_PASSWORD_LENGTH, self::MAX_PASSWORD_LENGTH ] ) ) );
		}

		return $boolIsValid;
	}

	public function valPasswordReuseNumber() {
		$boolIsValid = true;

		if( self::MIN_PASSWORD_REUSE_NUMBER > ( int ) $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL_VALUE', __( 'Please set the number of saved passwords before reuse from {%d, 0} to {%d, 1}.', [ self::MIN_PASSWORD_REUSE_NUMBER, self::MAX_PASSWORD_REUSE_NUMBER ] ) ) );
		} elseif( self::MAX_PASSWORD_REUSE_NUMBER < ( int ) $this->getValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL_VALUE', __( 'Please set the number of saved passwords before reuse from {%d, 0} to {%d, 1}.', [ self::MIN_PASSWORD_REUSE_NUMBER, self::MAX_PASSWORD_REUSE_NUMBER ] ) ) );
		}
		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

	public function validateUseItemCatalog( $arrobjCompanyPreferences = [] ) {
		$boolIsValid = true;

		if( true == array_key_exists( 'USE_ASSET_TRACKING', $arrobjCompanyPreferences ) && 0 == $this->getValue() && true == valArr( $arrobjCompanyPreferences ) ) {
			$this->addErrorMsg( __( 'To disable "Use Item Catalog" setting, you must first disable "Track Fixed Assets" setting.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validateUseAssetTracking( $arrobjCompanyPreferences = [] ) {
		$boolIsValid = true;

		if( false == array_key_exists( 'USE_ITEM_CATALOG', $arrobjCompanyPreferences )
			&& 1 == $this->getValue()
			&& 'USE_ASSET_TRACKING' == $this->getKey()
			&& true == valArr( $arrobjCompanyPreferences ) ) {

			$this->addErrorMsg( __( 'To enable "Track Fixed Assets" setting, you must first enable "Use Item Catalog" setting.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valVendorAccessAutoInviteEmail() {
		$boolIsValid = true;
		$strEmailAddress = $this->getValue();
		if( false == valStr( $strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_access_auto_invite_email', __( 'Email address is required.&nbsp' ) ) );
		} elseif( true == is_numeric( $strEmailAddress ) || $strEmailAddress != filter_var( $strEmailAddress, FILTER_VALIDATE_EMAIL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_access_auto_invite_email', __( 'Please enter valid email address.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDefaultLineItems() {

		$boolIsValid = true;

		if( '' == $this->getValue()
		    || 0 == $this->getValue()
		    || ( 2 > ( int ) $this->getValue() || 100 <= ( int ) $this->getValue() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getKey(), __( 'Please enter the number of line item between {%d, 0} - {%d, 1}.', [ 2, 99 ] ) ) );
		}
		return $boolIsValid;
	}

	public function validateAdvancedSetting() {
		$boolIsValid = true;

		$arrstrEmailAddress = array_unique( array_filter( explode( ',', $this->getValue() ) ) );
		foreach( $arrstrEmailAddress as $strEmailAddress ) {
			if( false == filter_var( trim( $strEmailAddress ), FILTER_VALIDATE_EMAIL ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please enter valid email address: {%s, 0}', [ $strEmailAddress ] ) ) );
			}
		}
		return $boolIsValid;
	}

	public function validateLeadSourceCookieDuration() {
		$boolIsValid = true;
		$intLeadSourceCookieDuration = $this->getValue();

		if( false == is_null( $intLeadSourceCookieDuration ) && ( false == ctype_digit( $intLeadSourceCookieDuration ) || 1 > ( int ) $intLeadSourceCookieDuration || 365 < ( int ) $intLeadSourceCookieDuration ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Error:', __( ' The number of days must be between 1 and 365' ) ) );
		}

		return $boolIsValid;
	}

}
?>