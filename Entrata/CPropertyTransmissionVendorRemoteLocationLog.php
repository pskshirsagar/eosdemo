<?php

class CPropertyTransmissionVendorRemoteLocationLog extends CBasePropertyTransmissionVendorRemoteLocationLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyTransmissionVendorRemoteLocationStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviousPropertyTransmissionVendorRemoteLocationStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemEmailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseText() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>