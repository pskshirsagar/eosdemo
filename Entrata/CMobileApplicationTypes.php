<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMobileApplicationTypes
 * Do not add any new functions to this class.
 */

class CMobileApplicationTypes extends CBaseMobileApplicationTypes {

    public static function fetchMobileApplicationTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CMobileApplicationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchMobileApplicationType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CMobileApplicationType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>