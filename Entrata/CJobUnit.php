<?php

class CJobUnit extends CBaseJobUnit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;

		if( true == is_null( $this->getJobId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_id', 'Job Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_type_id', 'Unit Type Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valJobPhaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyUnitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Property unit Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valJobId();
				$boolIsValid &= $this->valUnitTypeId();
				$boolIsValid &= $this->valPropertyUnitId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>