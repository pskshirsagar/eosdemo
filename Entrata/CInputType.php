<?php

class CInputType extends CBaseInputType {

	const SINGLE_SELECT		= 1;
	const MULTI_SELECT		= 2;
	const SHORT_DATE		= 3;
	const LONG_DATE			= 4;
	const TEXT				= 5;
	const SELECT_LIST 		= 6;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:

            default:
				// default case;
        		break;
        }

        return $boolIsValid;
    }

}
?>