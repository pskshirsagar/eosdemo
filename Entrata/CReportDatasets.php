<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportDatasets
 * Do not add any new functions to this class.
 */

class CReportDatasets extends CBaseReportDatasets {

	public static function fetchAllReportDatasetsByDefaultReportVersionId( $arrintDefaultReportVersionIds, $objDatabase ) {
		$strSql = '
			SELECT 
				rd.id,
				rd.dataset_key,
				util_get_translated( \'name\', rd.name, rd.details ) AS name,
				util_get_translated( \'description\', rd.description, rd.details ) AS description,
				rd.default_report_version_id,
				count( rc.id ) AS column_count
			FROM 
				report_datasets rd
				LEFT JOIN report_columns rc ON ( rd.id = rc.report_dataset_id AND rc.deleted_by IS NULL ) 
			WHERE 
				default_report_version_id IN ( ' . implode( ',', $arrintDefaultReportVersionIds ) . ' )
				AND rd.deleted_by IS NULL
			GROUP BY 
				rd.id
			ORDER BY 
				rd.order_num';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetMaxOrderNumberForReportDataset( $intDefaultReportVersionId, $objDatabase ) {
		$strSql = 'SELECT 
						MAX( order_num ) AS id
					FROM 
						report_datasets
					WHERE 
						default_report_version_id = ' . ( int ) $intDefaultReportVersionId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportDatasetMaxId( $objDatabase ) {
		$strSql = '
			SELECT
				MAX( id ) + 1 AS id
			FROM
				report_datasets';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportDatasetsByIds( $arrintDatasetIds, $objDatabase ) {
		if( !valArr( $arrintDatasetIds ) ) return [];

		$strSql = 'SELECT 
						*
					FROM
						report_datasets
					WHERE 
						id IN ( ' . implode( ',', $arrintDatasetIds ) . ' )';

		return self::fetchReportDatasets( $strSql, $objDatabase );
	}

}
?>