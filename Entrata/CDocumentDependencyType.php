<?php

class CDocumentDependencyType extends CBaseDocumentDependencyType {

	const CHARGE_CODE 			    = 1;
	const PET 					    = 2;
	const ADD_ON 				    = 3;
	const LEASE_TYPE 			    = 4;
	const VEHICLE 				    = 5;
	const UNIT_DESIGNATION 		    = 6;
	const UNIT_AMENITIES 		    = 7;
	const PROPERTY_GROUPS 		    = 8;
	const APPLICANT_AGE 		    = 9;
	const PROPERTY_TYPE 		    = 10;
	const SUBSIDY_CONTRACT_TYPE	    = 11;
	const SUBSIDY_TYPE			    = 12;
	const INSTALLMENT_PLAN_ADDED    = 13;
	const LEASE_LENGTH              = 14;

	protected $m_arrobjDocumentDependencies;

	public function addDocumentDependency( $objDocumentDependency ) {
		$this->m_arrobjDocumentDependencies[$objDocumentDependency->getId()] = $objDocumentDependency;
	}

	public function setDocumentDependencies( $arrobjDocumentDependencies ) {
		$this->m_arrobjDocumentDependencies = $arrobjDocumentDependencies;
	}

	public function getDocumentDependencies() {
		return $this->m_arrobjDocumentDependencies;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDocumentAssociationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>