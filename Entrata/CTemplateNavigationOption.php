<?php

class CTemplateNavigationOption extends CBaseTemplateNavigationOption {

    public function valWebsiteTemplateId() {
        $boolIsValid = true;

        if( true == is_null( $this->getWebsiteTemplateId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_template_id', 'Website template id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valWebsiteTemplateNavigationId() {
        $boolIsValid = true;

        if( true == is_null( $this->getWebsiteTemplateNavigationId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_template_navigation_id', 'Website template navigation id is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valWebsiteTemplateId();
            	$boolIsValid &= $this->valWebsiteTemplateNavigationId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>