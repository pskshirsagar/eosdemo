<?php

class CLeaseSetting extends CBaseLeaseSetting {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) || 0 >= ( int ) $this->m_intId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Lease Setting Request:  Id required - CLeaseSetting::valId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Lease Setting Request:  client id required - CLeaseSetting::valCid()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intLeaseId ) || 0 >= ( int ) $this->m_intLeaseId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Lease Setting Request:  Company Lease Id required - CLeaseSetting::valLeaseId()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valKey() {
        $boolIsValid = true;

        if( false == isset( $this->m_strKey ) || 0 >= strlen( $this->m_strKey ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Lease Setting Request:  Company Lease Id required - CLeaseSetting::valKey()', E_USER_ERROR );
		}

        return $boolIsValid;
    }

    public function valValue() {
        $boolIsValid = true;

        // Validation example

        // if( false == isset( $this->m_strValue )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valKey();
				break;

            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valKey();
				break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>