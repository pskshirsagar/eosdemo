<?php

class CDefaultMaintenanceTemplate extends CBaseDefaultMaintenanceTemplate {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultMaintenanceTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultMaintenancePriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultMaintenanceStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultMaintenanceLocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultMaintenanceProblemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceRequestTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProblemDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocationSpecifics() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdditionalInfo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEstimatedHours() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCostPerHour() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPartsDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPartsCost() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEquipmentDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEquipmentCost() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdditionalCost() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsBillable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>