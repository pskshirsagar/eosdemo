<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CInvoiceDeliveryTypes
 * Do not add any new functions to this class.
 */

class CInvoiceDeliveryTypes extends CBaseInvoiceDeliveryTypes {

	public static function fetchInvoiceDeliveryTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CInvoiceDeliveryType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchInvoiceDeliveryType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CInvoiceDeliveryType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>