<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMergeFieldTypes
 * Do not add any new functions to this class.
 */

class CMergeFieldTypes extends CBaseMergeFieldTypes {

    public static function fetchMergeFieldTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CMergeFieldType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchMergeFieldType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CMergeFieldType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAllMergeFieldTypes( $objDatabase ) {
    	return self::fetchMergeFieldTypes( 'SELECT * FROM merge_field_types', $objDatabase );
    }

    public static function fetchPublishedMergeFieldTypes( $objDatabase ) {
        return self::fetchMergeFieldTypes( 'SELECT * FROM merge_field_types WHERE is_published', $objDatabase );
    }

	public static function fetchPublishedMergeFieldTypesByIds( $arrintIds, $objDatabase ) {
		return self::fetchMergeFieldTypes( 'SELECT * FROM merge_field_types WHERE is_published AND id IN ( ' . implode( ',', $arrintIds ) . ')', $objDatabase );
	}

}
?>