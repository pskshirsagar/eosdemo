<?php

class CDefaultRoommateInterest extends CBaseDefaultRoommateInterest {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInputTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultRoommateInterestCategoryId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valQuestion() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMatchingWeight() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsRequired() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

			default:
            	// default;
            	break;
        }

        return $boolIsValid;
    }
}
?>