<?php

class COwnerDocument extends CBaseOwnerDocument {

	public function setDefaults() {
        $this->setIsPublished( '1' );
        $this->setIsSystem( '0' );
        $this->setOrderNum( '0' );

        return;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
             $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client required.' ) );
        }

        return $boolIsValid;
    }

    public function valApPayeeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getApPayeeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Owner is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>