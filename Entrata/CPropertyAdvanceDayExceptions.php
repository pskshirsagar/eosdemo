<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAdvanceDayExceptions
 * Do not add any new functions to this class.
 */

class CPropertyAdvanceDayExceptions extends CBasePropertyAdvanceDayExceptions {

	/**
	 * @param int $intCid
	 * @param int $intPropertyId
	 * @param int $intCurrentUserId
	 * @param     $objDatabase
	 *
	 * @return CPropertyAdvanceDayException[]
	 */
	public static function fetchPropertiesAdvanceDayExceptionsForAllMonths( array $arrintPropertyIds, int $intCid, int $intCurrentUserId, CDatabase $objDatabase ) {

		$strSql = '
			SELECT
				pade.id,
				' . ( int ) $intCid . ' AS cid,
				p.id as property_id,
				month_series AS month,
				pade.ar_advance_day,
				pade.ap_advance_day,
				pade.gl_advance_day,
				COALESCE( pade.updated_by, ' . ( int ) $intCurrentUserId . ' ) AS updated_by,
				COALESCE( pade.updated_on, NOW() ) AS updated_on,
				COALESCE( pade.created_by, ' . ( int ) $intCurrentUserId . ' ) AS created_by,
				COALESCE( pade.created_on, NOW() ) AS created_on
			FROM 
			properties p
			LEFT JOIN  generate_series( 1,12 ) month_series ON true
			LEFT JOIN property_advance_day_exceptions pade ON month_series = pade.month AND pade.cid = p.cid AND pade.property_id = p.id
			WHERE
			p.cid = ' . ( int ) $intCid . ' 
			AND p.id IN (  ' . sqlIntImplode( $arrintPropertyIds ) . ' )
			ORDER BY
				month;';

		return parent::fetchObjects( $strSql, 'CPropertyAdvanceDayException', $objDatabase, false );
	}

	public static function fetchCustomPropertiesAdvanceDayExceptionsForAllMonths( array $arrintPropertyIds, int $intCid, CDatabase $objDatabase ) {

		$strSql = 'SELECT
					sub_pade.id,
					' . ( int ) $intCid . ' AS cid,
					sub_pade.property_id,
					month_series AS month,
					CASE 
						WHEN sub_pade.property_id IS NULL OR sub_pade.ar_value_count = 0 THEN NULL
						WHEN sub_pade.ar_value_count = sub_pade.count THEN sub_pade.ar_advance_day
						ELSE 32
					END as ar_advance_day,
					CASE
					 	WHEN sub_pade.property_id IS NULL OR sub_pade.ap_value_count = 0 THEN NULL
						WHEN sub_pade.ap_value_count = sub_pade.count THEN sub_pade.ap_advance_day 
						ELSE 32
					END as ap_advance_day,
					CASE
						WHEN sub_pade.property_id IS NULL OR sub_pade.gl_value_count = 0 THEN NULL
						WHEN sub_pade.gl_value_count = sub_pade.count THEN sub_pade.gl_advance_day 
						ELSE 32
					END as gl_advance_day
				FROM
					generate_series( 1,12 ) month_series
					LEFT JOIN (
							SELECT
								DISTINCT ON(month)
								MAX( rank ) OVER ( PARTITION BY true ) AS count, 
								sub.*
								FROM (
									SELECT
										dense_rank() OVER( ORDER BY p.id ) AS rank,
										COUNT( pade.id ) FILTER ( WHERE  pade.ar_advance_day IS NOT NULL ) OVER( PARTITION BY pade.ar_advance_day, pade.month )  AS ar_value_count,
										COUNT( pade.id ) FILTER ( WHERE  pade.ap_advance_day IS NOT NULL ) OVER( PARTITION BY pade.ap_advance_day, pade.month ) AS ap_value_count,
										COUNT( pade.id ) FILTER ( WHERE  pade.gl_advance_day IS NOT NULL ) OVER( PARTITION BY pade.gl_advance_day, pade.month ) AS gl_value_count,
										p.cid,
										pade.property_id,
										pade.id,
										pade.month,
										pade.ar_advance_day,
										pade.ap_advance_day,
										pade.gl_advance_day
									FROM
										properties p
										LEFT JOIN property_advance_day_exceptions pade ON pade.cid = p.cid AND pade.property_id = p.id
									WHERE
										p.cid = ' . ( int ) $intCid . ' 
										AND p.id IN (  ' . sqlIntImplode( $arrintPropertyIds ) . ' )
                                ) as sub     
                            ) as sub_pade ON month_series = sub_pade.month AND sub_pade.cid = ' . ( int ) $intCid . ' AND sub_pade.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return parent::fetchObjects( $strSql, 'CPropertyAdvanceDayException', $objDatabase, false );
	}

}