<?php

class CPropertyApSetting extends CBasePropertyApSetting {

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getPropertyId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsCashBasisFee() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getIsCashBasisFee() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_cash_basis_fee', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLastFeeDate() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getLastFeeDate() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_fee_date', '' ) );
        // }

        return $boolIsValid;
    }

    public function valFeeGreaterFlatOrPercent() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getFeeGreaterFlatOrPercent() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fee_greater_flat_or_percent', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>