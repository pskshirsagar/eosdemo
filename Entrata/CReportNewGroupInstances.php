<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportNewGroupInstances
 * Do not add any new functions to this class.
 */

class CReportNewGroupInstances extends CBaseReportNewGroupInstances {

	public static function fetchReportNewGroupInstanceIdsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						rgi.report_new_group_id
					FROM
						report_new_group_instances rgi
						JOIN report_new_instances ri ON ( rgi.cid=ri.cid AND rgi.report_new_instance_id = ri.id )
						JOIN report_new_groups rg ON ( rg.cid=ri.cid AND rgi.report_new_group_id = rg.id AND rg.report_group_type_id = ' . CReportGroupType::PACKET . ' )
					WHERE
						rgi.cid = ' . ( int ) $intCid;

		$arrintReportNewGroupInstanceIds = ( array ) fetchData( $strSql, $objDatabase );
		return array_keys( rekeyArray( 'report_new_group_id', $arrintReportNewGroupInstanceIds ) );
	}

	public static function fetchReportNewGroupInstancesByReportNewGroupIdsByCid( $arrintReportNewGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintReportNewGroupIds ) ) {
			return [];
		}

		$strSql = 'SELECT
						rngi.report_new_group_id,
						rng.name AS packet_name,
						rs.id AS schedule_id,
						rng.created_by,
						rst.name AS report_schedule_type,
						rni.*
					FROM
						report_new_group_instances rngi
						JOIN report_new_groups rng ON ( rng.cid = rngi.cid AND rngi.report_new_group_id = rng.id )
						JOIN report_schedules rs ON ( rng.cid = rs.cid AND rng.id = rs.report_new_group_id )
						JOIN report_schedule_types rst ON ( rs.report_schedule_type_id = rst.id )
						JOIN report_new_instances rni ON ( rngi.cid = rni.cid AND rngi.report_new_instance_id = rni.id )     
					WHERE
						rngi.cid = ' . ( int ) $intCid . '
						AND rngi.report_new_group_id IN( ' . implode( ',', $arrintReportNewGroupIds ) . ' )
						AND rngi.deleted_by IS NULL';

		$arrmixReportNewGroupInstances = ( array ) fetchData( $strSql, $objDatabase );
		return rekeyArray( 'report_new_group_id', $arrmixReportNewGroupInstances );
	}

	public static function fetchReportNewGroupInstancesByReportNewGroupIdByReportNewInstanceIdByCid( $intReportNewGroupId, $intReportNewInstanceId, $intCid, $objDatabase, $boolCheckDeleted = false ) {
		$strSql = '
			SELECT
				rgi.*
			FROM
				report_new_group_instances rgi
			WHERE
				rgi.cid = ' . ( int ) $intCid . '
				AND rgi.report_new_group_id = ' . ( int ) $intReportNewGroupId . '
				AND rgi.report_new_instance_id = ' . ( int ) $intReportNewInstanceId;

		if( $boolCheckDeleted ){
			$strSql .= ' AND rgi.deleted_by is NULL ';
		}

		return self::fetchReportNewGroupInstances( $strSql, $objDatabase );
	}

}
?>