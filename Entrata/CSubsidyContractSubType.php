<?php

class CSubsidyContractSubType extends CBaseSubsidyContractSubType {

	const HUD_SECTION_8_NEW_CONSTRUCTION			= 1;
	const HUD_SECTION_8_SUBSTANTIAL_REHABILITATION	= 2;
	const HUD_SECTION_8_LMSA						= 3;
	const HUD_SECTION_8_PDSA						= 4;
	const HUD_SECTION_8_202							= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyContractTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>