<?php

class CInspectionNote extends CBaseInspectionNote {

	protected $m_strInspectionProblemUnitLocation;

	/**
	 * Get Functions
	 *
	 */

	public function getInspectionProblemUnitLocation() {
		return $this->m_strInspectionProblemUnitLocation;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setInspectionProblemUnitLocation( $strInspectionProblemUnitLocation ) {
		return $this->m_strInspectionProblemUnitLocation = $strInspectionProblemUnitLocation;
	}

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['inspection_problem_unit_location'] ) ) 	$this->setInspectionProblemUnitLocation( $arrmixValues['inspection_problem_unit_location'] );
	}

	/**
	 * Validation Functions
	 *
	 */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionFormLocationProblemId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionResponseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNote() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>