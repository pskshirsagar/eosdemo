<?php

class CRevenuePostRent extends CBaseRevenuePostRent {

	protected $m_strPostedBy;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intUnitSpaceConfigurationId;
	protected $m_arrmixRequiredParameters;
	protected $m_arrmixUnitTypeIds;

    public function setPostedBy( $strPostedBy ) {
    	$this->m_strPostedBy = CStrings::strTrimDef( $strPostedBy, -1, NULL, true );
    }

    public function getPostedBy() {
    	return $this->m_strPostedBy;
    }

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = CStrings::strToIntDef( $intLeaseTermId, NULL, false );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->m_intLeaseStartWindowId = CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false );
	}

	public function getUnitSpaceConfigurationId() {
		return $this->m_intUnitSpaceConfigurationId;
	}

	public function setUnitSpaceConfigurationId( $intUnitSpaceConfigurationId ) {
		$this->m_intUnitSpaceConfigurationId = CStrings::strToIntDef( $intUnitSpaceConfigurationId, NULL, false );
	}

	public function getUnitTypeIds() {
		return $this->m_arrmixUnitTypeIds;
	}

	public function setUnitTypeIds( $arrmixUnitTypeIds ) {
		$this->m_arrmixUnitTypeIds = CStrings::strToJson( $arrmixUnitTypeIds );
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valProcessDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsOverride() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsApprove() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsRenewal() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

	public function valStdOverrideRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStdOptimalRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverrideReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAcceptedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAcceptedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConstrainedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
    	if( isset( $arrmixValues['posted_by'] ) && $boolDirectSet ) {
    		$this->m_strPostedBy = trim( $arrmixValues['posted_by'] );
    	} elseif( isset( $arrmixValues['posted_by'] ) ) {
    		$this->setPostedBy( $arrmixValues['posted_by'] );
    	}

	    if( isset( $arrmixValues['lease_term_id'] ) ) {
		    $this->m_intLeaseTermId = trim( stripcslashes( $arrmixValues['lease_term_id'] ) );
	    }

	    if( isset( $arrmixValues['lease_start_window_id'] ) ) {
		    $this->m_intLeaseStartWindowId = trim( stripcslashes( $arrmixValues['lease_start_window_id'] ) );
	    }

	    if( isset( $arrmixValues['unit_space_configuration_id'] ) ) {
		    $this->m_intUnitSpaceConfigurationId = trim( stripcslashes( $arrmixValues['unit_space_configuration_id'] ) );
	    }

	    if( isset( $arrmixValues['unit_type_ids'] ) ) {
		    $this->m_arrmixUnitTypeIds = trim( stripcslashes( $arrmixValues['unit_type_ids'] ) );
	    }

    	return;
    }

	public function setRequiredParamers( $arrmixParameters ) {
		$this->m_arrmixRequiredParameters = $arrmixParameters;
	}

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParameters;
	}

	public function getDeniedUnitSpaceDetails() {
		$arrmixRejectReasons = $this->getDetails();
		$strRejectReason     = $arrmixRejectReasons->rejection_reason;
		$intIsRenewal        = $this->getIsRenewal();
		$intPropertyId       = $this->getPropertyId();
		$intCid              = $this->getCid();
		$intUnitSpaceId      = $this->getUnitSpaceId();
		$intUnitTypeId       = $this->getUnitTypeId();
		$intOptimalRent      = $this->getStdOptimalRent();
		$intOverrideRent     = $this->getStdOverrideRent();
		$strOverrideReason   = $this->getOverrideReason();

		if( 1 == $intIsRenewal ) {
			// if status filter 'Renewal' is selected
			$arrmixAmenityAdjustments = ( array ) \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByUnitSpaceIdsByCid( [ $intUnitSpaceId ], $intCid, $this->m_objDatabase, $intPropertyId );
			if( true == valArr( $arrmixAmenityAdjustments ) ) {
				$intAmenityAmount = 0;
				foreach( $arrmixAmenityAdjustments as $arrmixAmenityAdjustment ) {
					if( $intUnitSpaceId == $arrmixAmenityAdjustment['id'] ) {
						$intAmenityAmount += $arrmixAmenityAdjustment['rate_amount'];
					}
				}
			}
		} else {
			$objRateLogsFilter = new CRateLogsFilter;
			$objRateLogsFilter->setCids( [ $intCid ] );
			$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
			$objRateLogsFilter->setArOriginIds( [ CArOrigin::AMENITY ] );
			// fetching rates only for entrata
			$objRateLogsFilter->setPublishedUnitsOnly( false );
			$objRateLogsFilter->setMarketedUnitsOnly( true );

			$objRateLogsFilter->setUnitTypeIds( [ $intUnitTypeId ] );
			$objRateLogsFilter->setUnitSpaceIds( [ $intUnitSpaceId ] );
			$arrintAmenities = ( array ) CPricingRentReviewLibrary::getAmentiesAmountByRatelogFilter( $objRateLogsFilter, $this->m_objDatabase, false );
			if( false == valArr( $arrintAmenities ) ) {
				$intAmenityAmount = 0;
			} else {
				$intAmenityAmount = $arrintAmenities['amenity_rent'];
			}
		}

		$intOptimalRent = round( $intOptimalRent + $intAmenityAmount );
		if( '' != $intOverrideRent ) {
			$intOverrideRent = round( $intOverrideRent + $intAmenityAmount );
		}

		$arrmixOverridenUnitSpaces = ( array ) \Psi\Eos\Entrata\CRevenueOverrideRents::createService()->fetchRevenueOverrideRentsByPropertyIdByUnitTypeIdsByUnitSpaceIdCid( [ $intPropertyId ], [], [ $intUnitSpaceId ], $intCid, $this->m_objDatabase, $intIsRenewal );

		$objPricingDeniedUnitSpace                  = new stdClass();
		$objPricingDeniedUnitSpace->unit_space_name = $arrmixOverridenUnitSpaces[0]['unit_space_name'];
		$objPricingDeniedUnitSpace->unit_type_name  = $arrmixOverridenUnitSpaces[0]['lookup_code'];
		$objPricingDeniedUnitSpace->optimal_rent    = __( '{%d, 0, p:0}', [ $intOptimalRent ] );
		$objPricingDeniedUnitSpace->override_rent   = __( '{%d, 0, p:0}', [ $intOverrideRent ] );
		$objPricingDeniedUnitSpace->override_reason = $strOverrideReason;
		$objPricingDeniedUnitSpace->reject_reason   = $strRejectReason;

		return $objPricingDeniedUnitSpace;
	}

	public function getRevenueDeniedUnitsHtml() {
		$objPricingDeniedUnitSpace   = $this->getDeniedUnitSpaceDetails();
		if( false == valObj( $objPricingDeniedUnitSpace, stdClass::class ) ) {
			return '';
		}
		$strDeniedHTML = __( '<tr>
					<td style="width:20px;"></td>
					<td style="font-size:14px; line-height:20px; text-align:left; font-family: \'Montserrat\', sans-serif;" class="responsive spacer">
						<span style=" display:block; padding-bottom:5px; color:#999;">THE FOLLOWING RENTS <b>WERE NOT</b> PUBLISHED AS THE OVERRIDE WAS DENIED. PLEASE REVIEW AND MAKE ANY DESIRED CHANGES IN ENTRATA PRICING.</span>
					</td>
					<td style="width:20px;"></td>
				</tr>
				<tr>
					<td style="width:20px;"></td>
					<td>
						<table style="width:100%; border-collapse: collapse;" cellspacing="0" cellpadding="0">
							<thead class="responsive-hide">
							<tr style="text-transform: uppercase; color:#ccc; font-size:14px; text-align:left;">
								<th style="border:1px solid #CCC; font-weight:400; padding:5px 10px; width:13%; color:#999;">Unit Space</th>
								<th style="border:1px solid #CCC; font-weight:400; padding:5px 10px; width:13%; color:#999;">Unit Type</th>
								<th style="border:1px solid #CCC; font-weight:400; padding:5px 10px; width:13%; color:#999;">Optimal Rent</th>
								<th style="border:1px solid #CCC; font-weight:400; padding:5px 10px; width:13%; color:#999;">Override Rent</th>
								<th style="border:1px solid #CCC; font-weight:400; padding:5px 10px; width:20%; color:#999;">Override Reason</th>
								<th style="border:1px solid #CCC; font-weight:400; padding:5px 10px; width:28%; color:#999;">Denied Reason</th>
							</tr>
							</thead>
							<tbody class="responsive-table">
							 <tr style="font-size:14px; text-align:left;">
								<td style="border:1px solid #CCC; padding:10px;" data-th="Unit Space">{%s,0}</td>
								<td style="border:1px solid #CCC; padding:10px;" data-th="Unit Type">{%s,1}</td>
								<td style="border:1px solid #CCC; padding:10px; {%s,6}" data-th="Optimal Rent"> {%m,2,p:0} </td>
								<td style="border:1px solid #CCC; padding:10px; {%s,7}" data-th="Override Rent">{%m,3,p:0}</td>
								<td style="border:1px solid #CCC; padding:10px; {%s,8}" data-th="Override Reason">{%s,4}</td>
								<td style="border:1px solid #CCC; padding:10px;" data-th="Reason Denied">{%s,5} </td>
							</tr>
							</tbody>
						</table>
					</td>
					<td style="width:20px;"></td>
				</tr>
				<tr>
					<td style="width:20px;"></td>
					<td style="height:1px; background:#CCC;"></td>
					<td style="width:20px;"></td>
				</tr>',
			[
				( ( true == isset( $objPricingDeniedUnitSpace->unit_space_name ) ) ? $objPricingDeniedUnitSpace->unit_space_name : '-' ),
				( ( true == isset( $objPricingDeniedUnitSpace->unit_type_name ) ) ? $objPricingDeniedUnitSpace->unit_type_name : '-' ),
				( ( string ) ( 0 < $objPricingDeniedUnitSpace->optimal_rent ) ? $objPricingDeniedUnitSpace->optimal_rent : '-' ),
				( ( string ) ( 0 < $objPricingDeniedUnitSpace->override_rent ) ? $objPricingDeniedUnitSpace->override_rent : '-' ),
				( ( true == isset ( $objPricingDeniedUnitSpace->override_reason ) ) ? $objPricingDeniedUnitSpace->override_reason : '-' ),
				( ( true == isset ( $objPricingDeniedUnitSpace->reject_reason ) ) ? $objPricingDeniedUnitSpace->reject_reason : '-' ),
				( ( 0 < $objPricingDeniedUnitSpace->optimal_rent ) ? 'text-align:right;' : 'text-align:center;' ),
				( ( 0 < $objPricingDeniedUnitSpace->override_rent ) ? 'text-align:right;' : 'text-align:center;' ),
				( ( isset ( $objPricingDeniedUnitSpace->override_reason ) && false == is_null( $objPricingDeniedUnitSpace->override_reason ) ) ? 'text-align:left;' : 'text-align:center;' )
			] );
		return $strDeniedHTML;
	}

}
?>