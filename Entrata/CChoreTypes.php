<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreTypes
 * Do not add any new functions to this class.
 */

class CChoreTypes extends CBaseChoreTypes {

	public static function fetchChoreTypesByChoreCategoryIdsByCid( array $arrintChoreCategoryIds, int $intCid, CDatabase $objClientDatabase ) {

		if( false == valArr( $arrintChoreCategoryIds ) ) return NULL;

		$strSql = 'SELECT
						ct.*,
						util_get_system_translated( \'name\', cc.name, cc.details ) AS chore_category_name
					FROM
						chore_types ct
						JOIN chore_categories cc ON ( ct.chore_category_id = cc.id )
					WHERE
						ct.cid = ' . ( int ) $intCid . '
						AND ct.chore_category_id IN ( ' . sqlIntImplode( $arrintChoreCategoryIds ) . ' )
					ORDER BY
						ct.chore_category_id,
						ct.name';

		return self::fetchChoreTypes( $strSql, $objClientDatabase );
	}

	public static function fetchChoreTypeByIdByCid( $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						ct.*,
						util_get_translated( \'name\', cc.name, cc.details ) AS chore_category_name
					FROM
						chore_types ct
						JOIN chore_categories cc ON( cc.id = ct.chore_category_id )
					WHERE
						ct.id = ' . ( int ) $intId . '
						AND ct.cid = ' . ( int ) $intCid;

		return self::fetchChoreType( $strSql, $objClientDatabase );
	}

	public static function fetchClosingChoreTypesByPeriodIdByCid( $intPeriodId, $intCid, CDatabase $objClientDatabase ) {
		if( false == is_int( $intPeriodId ) || false == is_int( $intCid ) ) return NULL;

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						ct.*,
						util_get_translated( \'name\', cc.name, cc.details ) AS chore_category_name
					FROM
						chore_types ct
						JOIN chore_categories cc ON ct.chore_category_id = cc.id
					WHERE
						ct.cid = ' . ( int ) $intCid . '
						AND ct.chore_category_id IN ( ' . implode( ', ', CChoreCategory::$c_arrintClosingChoreCategories ) . ' )
						AND ct.disabled_by IS NULL
						AND ct.disabled_on IS NULL
						AND NOT EXISTS ( SELECT 
											NULL 
										 FROM 
										 	chores c 
											JOIN chore_references cr ON cr.cid = c.cid AND cr.chore_id = c.id 
										 WHERE 
										 	c.cid = ct.cid
										 	AND c.chore_type_id = ct.id
										 	AND cr.reference_id = ' . ( int ) $intPeriodId . ' )';

		return self::fetchChoreTypes( $strSql, $objClientDatabase );
	}

}