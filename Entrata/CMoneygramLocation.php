<?php

class CMoneygramLocation extends CBaseMoneygramLocation {

    public function setMiles( $fltMiles ) {
    	$this->m_fltMiles = CStrings::strTrimDef( $fltMiles, 10, NULL, true );
    }

    public function getMiles() {
    	return $this->m_fltMiles;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['miles'] ) ) {
    		$this->setMiles( $arrValues['miles'] );
    	}
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCity() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStateCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCountryCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPostalCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPhoneNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWeekdayOpenTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWeekdayCloseTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSatOpenTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSatCloseTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSunOpenTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSunCloseTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsBlacklisted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLongitude() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLatitude() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFailureCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>