<?php

class CDefaultLeaseStartWindow extends CBaseDefaultLeaseStartWindow {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultLeaseStartStructureId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOffsetStartDays() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOffsetEndDays() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>