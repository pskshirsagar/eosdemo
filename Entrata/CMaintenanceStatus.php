<?php

class CMaintenanceStatus extends CBaseMaintenanceStatus {

	protected $m_strMaintenanceStatusTypeName;
	protected $m_strPropertyRemotePrimaryKey;
    protected $m_strFilterStatus;

    protected $m_intIntegrationClientStatusTypeId;
    protected $m_intPsProductId;

    protected $m_intIsDefaultClosed = 0;

    public static $c_arrintIntegratedClientTypes = [
    	CIntegrationClientType::YARDI,
    	CIntegrationClientType::MRI,
    	CIntegrationClientType::TIMBERLINE,
    	CIntegrationClientType::TENANT_PRO,
    	CIntegrationClientType::REAL_PAGE,
    	CIntegrationClientType::JENARK,
	    CIntegrationClientType::REAL_PAGE_API,
	    CIntegrationClientType::YARDI_RPORTAL
    ];

    /**
     * Create Functions
     */

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getMaintenanceStatusName( $intMaintenanceStatusId, $intCid, $objDatabase ) {
		$objMaintenanceStatus = \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchMaintenanceStatus( sprintf( 'SELECT name, details FROM maintenance_statuses WHERE id = %d AND cid = %d', ( int ) $intMaintenanceStatusId, ( int ) $intCid ), $objDatabase );
		$strMaintenanceStatus = NULL;
		if( true == valObj( $objMaintenanceStatus, CMaintenanceStatus::class ) ) {
			$strMaintenanceStatus = $objMaintenanceStatus->getName();
		}
		return $strMaintenanceStatus;
	}

	public function getLeaseActionId( $strLeaseAction ) {
		$this->loadLeaseActions();

		$intLeaseAction = 0;
		foreach( $this->m_arrstrLeaseActions as $intLeaseActionId => $strLeaseActionValue ) {
			if( $strLeaseAction == $strLeaseActionValue ) {
				$intLeaseAction = $intLeaseActionId;
				break;
			}
		}

		return $intLeaseAction;
	}

	private function loadLeaseActions() {
		if( false == empty( $this->m_arrstrLeaseActions ) ) {
			return;
		}

		$this->m_arrstrLeaseActions = [
			self::PLACED_on_NOTICE => __( 'Placed on notice' ),
			self::MOVE_OUT         => __( 'Move Out' ),
			self::TRANSFER         => __( 'Transfer' )
		];
	}

    public function createPropertyMaintenanceStatus() {

    	$objPropertyMaintenanceStatus = new CPropertyMaintenanceStatus();
    	$objPropertyMaintenanceStatus->setCid( $this->m_intCid );
    	$objPropertyMaintenanceStatus->setMaintenanceStatusId( $this->m_intId );
    	$objPropertyMaintenanceStatus->setMaintenanceStatusTypeId( $this->m_intMaintenanceStatusTypeId );

    	if( CMaintenanceStatusType::OPEN == $this->getMaintenanceStatusTypeId() ) {
    		$objPropertyMaintenanceStatus->setIsDefault( 1 );
    	}

    	return $objPropertyMaintenanceStatus;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIntegrationDatabaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceStatusTypeId() {
        $boolIsValid = true;

        if( false == valId( $this->getMaintenanceStatusTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_type_id', __( 'Maintenance status type is required.' ) ) );
        }

        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valName( $objDatabase = NULL ) {

		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		} else {
			$objMaintenanceStatus = \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchSimpleMaintenanceStatusesWithPropertyRemotePrimaryKeyAndPsProductIdByCid( $this->m_intCid, $objDatabase, false, $this->getName() );

			if( true == valObj( $objMaintenanceStatus, 'CMaintenanceStatus' ) && $objMaintenanceStatus->getId() != $this->getId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Name already exists.' ) ) );
			}
		}

       return $boolIsValid;
	}

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished( $objDatabase ) {
        $boolIsValid = true;
        $intCountMaintenanceDefaultStatus = \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchCustomDefaultMaintenanceStatusByStatusIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( 0 < $intCountMaintenanceDefaultStatus ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Default property status {%s, 0} cannot be disabled. ', [ $this->getName() ] ) ) );
        }
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsHiddenOnDashboard() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImportedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceStatusMaintenanceRequests( $objDatabase ) {
    	$boolIsValid = true;

    	if( true == valId( $this->getId() ) ) {
    		$arrobjMaintenanceRequests = \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchCustomMaintenanceRequestsByMaintenanceStatusIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrobjMaintenanceRequests ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_type_id', __( 'Maintenance request(s) exists for the maintenance status.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valMaintenanceStatusTypeMaintenanceTemplates( $objDatabase ) {
    	$boolIsValid = true;

    	if( true == valId( $this->getId() ) ) {
    		$arrobjMaintenanceTemplates = \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByMaintenanceStatusIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrobjMaintenanceTemplates ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_type_id', __( 'Maintenance template(s) exists for the maintenance status type.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
    	$boolIsValid = true;

    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    			$boolIsValid &= $this->valName( $objDatabase );
    			$boolIsValid &= $this->valMaintenanceStatusTypeId( $objDatabase );
    			break;

    		case 'maintenance_status_type':
    			$boolIsValid &= $this->valName( $objDatabase );
    			break;

    		case 'maintenance_status_type_toggle':
    			break;

    		case 'validate_toggle':
    			break;

    		case VALIDATE_DELETE:
    			$boolIsValid &= $this->valMaintenanceStatusMaintenanceRequests( $objDatabase );
    			$boolIsValid &= $this->valMaintenanceStatusTypeMaintenanceTemplates( $objDatabase );
    			break;

    		case 'validate_update_new':
    			$boolIsValid &= $this->valName( $objDatabase );
    			$boolIsValid &= $this->valMaintenanceStatusTypeId( $objDatabase );

				if( false == valId( $this->getIsPublished() ) ) {
					$boolIsValid &= $this->valIsPublished( $objDatabase );
				}
    			break;

    		default:
    			$boolIsValid = true;
    			break;
    	}

    	return $boolIsValid;
    }

    public function setMaintenanceStatusTypeName( $strMaintenanceStatusTypeName ) {
    	$this->m_strMaintenanceStatusTypeName = $strMaintenanceStatusTypeName;
    }

    public function getMaintenanceStatusTypeName() {
    	return $this->m_strMaintenanceStatusTypeName;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['maintenance_status_type_name'] ) ) $this->setMaintenanceStatusTypeName( $arrmixValues['maintenance_status_type_name'] );
    	if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['ps_product_id'] ) ) $this->setPsProductId( $arrmixValues['ps_product_id'] );
    	if( true == isset( $arrmixValues['property_remote_primary_key'] ) ) $this->setPropertyRemotePrimaryKey( $arrmixValues['property_remote_primary_key'] );
    	if( true == isset( $arrmixValues['integration_client_status_type_id'] ) ) $this->setIntegrationClientStatusTypeId( $arrmixValues['integration_client_status_type_id'] );
    	if( true == isset( $arrmixValues['is_default_closed'] ) ) $this->setIsDefaultClosed( $arrmixValues['is_default_closed'] );
    	if( true == isset( $arrmixValues['filter_status'] ) ) $this->setFilterStatus( $arrmixValues['filter_status'] );
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setPsProductId( $intPsProductId ) {
    	return $this->m_intPsProductId = $intPsProductId;
    }

    public function setPropertyRemotePrimaryKey( $strPropertyRemotePrimaryKey ) {
    	return $this->m_strPropertyRemotePrimaryKey = $strPropertyRemotePrimaryKey;
    }

    public function setIntegrationClientStatusTypeId( $intIntegrationClientStatusTypeId ) {
    	return $this->m_intIntegrationClientStatusTypeId = $intIntegrationClientStatusTypeId;
    }

    public function setIsDefaultClosed( $intIsDefaultClosed ) {
    	return $this->m_intIsDefaultClosed = $intIsDefaultClosed;
    }

    public function setFilterStatus( $strFilterStatus ) {
    	return $this->m_strFilterStatus = $strFilterStatus;
    }

    public function getPsProductId() {
    	return $this->m_intPsProductId;
    }

    public function getPropertyRemotePrimaryKey() {
    	return $this->m_strPropertyRemotePrimaryKey;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getIntegrationClientStatusTypeId() {
    	return $this->m_intIntegrationClientStatusTypeId;
    }

    public function getIsDefaultClosed() {
		return $this->m_intIsDefaultClosed;
    }

    public function getFilterStatus() {
    	return $this->m_strFilterStatus;
    }

    /**
     * Override Functions
     */
    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( 0 >= $this->getOrderNum() ) {
    		$this->setOrderNum( NULL );
    	}

    	return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    }

}
?>