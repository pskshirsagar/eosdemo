<?php

class CCustomerGuestActivityLog extends CBaseCustomerGuestActivityLog {

	protected $m_strGuestName;
	protected $m_strGuestType;
	protected $m_strCustomer;
	protected $m_intCustomerId;
	protected $m_strBldgUnit;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_intPropertyVisitCount;
	protected $m_intCheckInStatus;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['guest_name'] ) ) {
			$this->setGuestName( $arrmixValues['guest_name'] );
		}
		if( true == isset( $arrmixValues['guest_type'] ) ) {
			$this->setGuestType( $arrmixValues['guest_type'] );
		}
		if( true == isset( $arrmixValues['bldg_unit'] ) ) {
			$this->setBldgUnit( $arrmixValues['bldg_unit'] );
		}
		if( true == isset( $arrmixValues['customer_id'] ) ) {
			$this->setCustomerId( $arrmixValues['customer_id'] );
		}
		if( true == isset( $arrmixValues['customer'] ) ) {
			$this->setCustomer( $arrmixValues['customer'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['property_unit_id'] ) ) {
			$this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
		}
		if( true == isset( $arrmixValues['property_visit_count'] ) ) {
			$this->setPropertyVisitCount( $arrmixValues['property_visit_count'] );
		}

		return;
	}

	public function getGuestName() {
		return $this->m_strGuestName;
	}

	public function setGuestName( $strGuestName ) {
		$this->m_strGuestName = $strGuestName;
	}

	public function getGuestType() {
		return $this->m_strGuestType;
	}

	public function setGuestType( $strGuestType ) {
		$this->m_strGuestType = $strGuestType;
	}

	public function getCustomer() {
		return $this->m_strCustomer;
	}

	public function setCustomer( $strCustomer ) {
		$this->m_strCustomer = $strCustomer;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function getBldgUnit() {
		return $this->m_strBldgUnit;
	}

	public function setBldgUnit( $strBldgUnit ) {
		$this->m_strBldgUnit = $strBldgUnit;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function setCheckInStatus( $intCheckInStatus ) {
		$this->m_intCheckInStatus = $intCheckInStatus;
	}

	public function getCheckInStatus() {
		return $this->m_intCheckInStatus;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function getPropertyVisitCount() {
		return $this->m_intPropertyVisitCount;
	}

	public function setPropertyVisitCount( $intPropertyVisitCount ) {
		$this->m_intPropertyVisitCount = $intPropertyVisitCount;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerGuestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckInTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckOutTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckedInBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckedOutBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>