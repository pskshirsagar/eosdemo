<?php

class CCachedLease extends CBaseCachedLease {

	protected $m_strTerminationStartDate;
	protected $m_strPaymentDateTime;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReimbursingPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOwnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentAllowanceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransferLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTerminationListTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActiveLeaseIntervalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valM2mLeaseIntervalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntegrationDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastDelinquencyNoteEventId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStatusType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameMiddle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNoticeDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCollectionsStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBuildingName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitNumberCache() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisplayNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinancialBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRepaymentBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepositHeld() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInReviewCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutReviewCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransferredOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFmoStartedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFmoApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFmoProcessedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRenewalPromptMutedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRenewalPromptMutedUntil() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasRepaymentAgreement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsMonthToMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsTransferringIn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsMultiSlot() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['termination_start_date'] ) ) $this->setTerminationStartDate( $arrmixValues['termination_start_date'] );
		if( true == isset( $arrmixValues['payment_datetime'] ) ) $this->setPaymentDateTime( $arrmixValues['payment_datetime'] );
	}

	public function setTerminationStartDate( $strTerminationStartDate ) {
		$this->m_strTerminationStartDate = $strTerminationStartDate;
	}

	public function getTerminationStartDate() {
		return $this->m_strTerminationStartDate;
	}

	public function setPaymentDateTime( $strPaymentDateTime ) {
		$this->m_strPaymentDateTime = $strPaymentDateTime;
	}

	public function getPaymentDateTime() {
		return $this->m_strPaymentDateTime;
	}

}
?>