<?php

class CSubsidyMessageDetail extends CBaseSubsidyMessageDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubmissionSubsidyMessageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResultSubsidyMessageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResultMessage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumErrors() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFieldErrors() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubmissionRecordNumbers() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>