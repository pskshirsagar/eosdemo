<?php

use Psi\Eos\Entrata\CSpecials;
use Psi\Eos\Entrata\CPropertyFloorplans;
use Psi\Eos\Entrata\CPropertyUnits;
use Psi\Eos\Entrata\CUnitSpaces;

class CUnitSpace extends CBaseUnitSpace {

	const LOOKUP_TYPE_FIXED_ASSETS_UNITS	= 'fixed_assets_units';

	const UNIT_OCCUPANCY_VACANT             = 'vacant';
	const UNIT_OCCUPANCY_PARTIALLY_OCCUPIED = 'partially_occupied';

	protected $m_objProperty;
	protected $m_objPropertyUnit;
	protected $m_objCompanyMediaFile;
	protected $m_objRevenueOptimalRent;

	protected $m_arrobjRates;
	protected $m_arrobjSpaceLeases;
	protected $m_arrobjUnitAddOns;
	protected $m_arrobjUnitAddOnLeaseAssociations;
	protected $m_arrobjSpecials;
	protected $m_arrobjActiveLeaseIntervals;
	protected $m_arrobjUnitSpaceSpecials;
	protected $m_arrobjUnitSpecials;
	protected $m_arrobjLeaseTerms;
	protected $m_arrobjApartmentAmenities;

	protected $m_intBaseRent;
	protected $m_intTermMonth;
	protected $m_intUnitTypeId;
	protected $m_intMinSqureFeet;
	protected $m_intEffectiveRent;
	protected $m_intPropertyUnitId;
	protected $m_intPropertyFloorId;
	protected $m_intLeaseTermMonths;
	protected $m_intNumberOfBedrooms;
	protected $m_intBaseEffectiveRent;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyFloorplanId;
	protected $m_intPropertyFloorNumber;
	protected $m_intUnitSpaceSpecialsCount;
	protected $m_intBuildingId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intLeaseTermId;
	protected $m_intArTriggerId;
	protected $m_intSquareFeet;
	protected $m_intLeaseStartWindowId;
	protected $m_intLeaseStartWindowStartDate;
	protected $m_intLeaseStartWindowEndDate;
	protected $m_intGenderId;
	protected $m_intUnitSpaceUnionId;
	protected $m_intTotalUnitSpacesCount;
	protected $m_intAvailableUnitCount;
	protected $m_intAvailableUnitSpacesCount;
	protected $m_arrintLeaseTerms;

	protected $m_strNotes;
	protected $m_strLeaseTermName;
	protected $m_strArTriggerName;
	protected $m_strUnitNumber;
	protected $m_strBuildingName;
	protected $m_strUnitSpaceStatusType;
	protected $m_strFloorplanName;
	protected $m_strUnitTypeName;
	protected $m_strCurrentLeaseEndDate;
	protected $m_strPropertyFloorTitle;
	protected $m_strUnitSpecialsList;
	protected $m_strApartmentAmenitiesList;
	protected $m_strApartmentAmenitiesIds;
	protected $m_strSpaceConfigurationName;
	protected $m_strMergedUnitSpaceIds;
	protected $m_strMergedUnitSpaceNumbers;
	protected $m_strDisplayUnitSpaceNumber;
	protected $m_strDefaultAmenities;
	protected $m_strResidentName;

	protected $m_fltMinDeposit;
	protected $m_fltMaxDeposit;
	protected $m_fltRentAmount;
	protected $m_fltDepositAmount;
	protected $m_fltCachedMinRent;
	protected $m_fltCachedMaxRent;
	protected $m_fltCachedMinDeposit;
	protected $m_fltCachedMaxDeposit;
	protected $m_fltMonthToMonthRent;
	protected $m_fltNumberOfBathrooms;
	protected $m_fltCachedMinSquareFeet;
	protected $m_fltAmenityRentAmount;
	protected $m_fltSpecialRentAmount;
	protected $m_fltAddOnRentAmount;
	protected $m_fltAmenityAmount;
	protected $m_fltSpecialAmount;
	protected $m_fltAddOnAmount;
	protected $m_fltAmenityTaxAmount;
	protected $m_fltSpecialTaxAmount;
	protected $m_fltAddOnTaxAmount;
	protected $m_fltBaseAmount;
	protected $m_fltBaseTaxAmount;
	protected $m_fltLeaseTotalAmount;

	protected $m_arrobjRateLogs;
	protected $m_arrobjApartmentAmenityRateLogs;

	protected $m_arrmixEffectiveRents;
	protected $m_arrmixAdditionalSavings;

	protected $m_boolIsFurnished;
	protected $m_boolHasSpecials;
	protected $m_boolHasDisplaySpecials;
	protected $m_boolIsAvailable;
	protected $m_boolIsDeleted;
	protected $m_boolHasPets;
	protected $m_boolHasAmenities;

	protected $m_intRatesCount;
	protected $m_intSpaceConfigurationId;
	protected $m_boolExceedsExpirationLimit;
	protected $m_fltMinRent;
	protected $m_fltMaxRent;
	protected $m_fltSitePlanXPos;
	protected $m_fltSitePlanYPos;

	protected $m_arrmixMatrixDetails;

	const AVAILABLE_ON 		= '01/01/1970';

	/**
	* Get or Fetch Functions
	*
	*/

	public function getOrFetchProperty( $objDatabase ) {

		if( false == valObj( $this->m_objProperty, 'CProperty' ) ) {
				$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objProperty;
	}

	public function getOrFetchPropertyUnit( $objDatabase ) {

		if( false == valObj( $this->m_objPropertyUnit, 'CPropertyUnit' ) ) {
			$this->m_objPropertyUnit = CPropertyUnits::createService()->fetchPropertyUnitByUnitSpaceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
		}

		return $this->m_objPropertyUnit;
	}

	public function getOrFetchActiveLeaseIntervals( $objDatabase, $arrintSkippedLeaseStatusTypeIds = NULL, $intLeaseId = NULL ) {

		if( false == valArr( $this->m_arrobjActiveLeaseIntervals ) ) {
			$this->m_arrobjActiveLeaseIntervals = $this->fetchActiveLeaseIntervalsOrderedByLeaseStartDate( $objDatabase, $arrintSkippedLeaseStatusTypeIds, $intLeaseId );
		}

		return $this->m_arrobjActiveLeaseIntervals;
	}

	public function getOrFetchRates( $objDatabase, $objRateLogsFilter = NULL ) {

		$this->m_objProperty = $this->getOrFetchProperty( $objDatabase );

		if( false == valObj( $objRateLogsFilter, 'CRateLogsFilter' ) ) {
			$objRateLogsFilter	= new CRateLogsFilter();
			$objRateLogsFilter->setCids( [ $this->m_objProperty->getCid() ] );
			$objRateLogsFilter->setPropertyGroupIds( [ $this->m_objProperty->getId() ] );
			$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE ] );
		}

		$objRateLogsFilter->setUnitSpaceIds( [ $this->getId() ] );

		if( true == is_null( $this->m_arrobjRateLogs ) ) {
			$this->m_arrobjRateLogs = [];
		}

		if( false == valArr( $this->m_arrobjRateLogs ) ) {
			\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
			$this->m_arrobjRateLogs = \Psi\Eos\Entrata\CRateLogs::createService()->fetchTempProspectRates( $objDatabase );
		}

		return $this->m_arrobjRateLogs;
	}

	public function getOrFetchApartmentAmenityRates( $objDatabase, $objRateLogsFilter = NULL ) {

		$this->m_objProperty = $this->getOrFetchProperty( $objDatabase );

		if( false == valObj( $objRateLogsFilter, 'CRateLogsFilter' ) ) {
			$objRateLogsFilter	= new CRateLogsFilter();
			$objRateLogsFilter->setCids( [ $this->m_objProperty->getCid() ] );
			$objRateLogsFilter->setPropertyGroupIds( [ $this->m_objProperty->getId() ] );
			$objRateLogsFilter->setArOriginIds( [ CArOrigin::AMENITY ] );
		}

		$objRateLogsFilter->setUnitSpaceIds( [ $this->getId() ] );

		if( true == is_null( $this->m_arrobjApartmentAmenityRateLogs ) ) {
			$this->m_arrobjApartmentAmenityRateLogs = [];
		}

		if( false == isset( $this->m_arrobjApartmentAmenityRateLogs ) || true == is_null( $this->m_arrobjApartmentAmenityRateLogs ) ) {
			$arrobjApartmentAmenities = $this->getOrFetchApartmentAmenities( $objDatabase );
			$arrintApartmentAmenityIds = [];
			if( true == valArr( $arrobjApartmentAmenities ) ) {
				foreach( $arrobjApartmentAmenities as $objApartmentAmenity ) {
					if( false == $objApartmentAmenity->getIsOptional() && true == $objApartmentAmenity->getIncludeInRent() ) {
						$arrintApartmentAmenityIds[] = $objApartmentAmenity->getId();
					}
				}
			}

			$objRateLogsFilter->setArOriginReferenceIds( $arrintApartmentAmenityIds );

			\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
			$this->m_arrobjApartmentAmenityRateLogs = \Psi\Eos\Entrata\CRateLogs::createService()->fetchTempProspectRates( $objDatabase );
		}

		return $this->m_arrobjApartmentAmenityRateLogs;
	}

	public function getOrFetchApartmentAmenities( $objDatabase ) {

		if( false == valArr( $this->m_arrobjApartmentAmenities ) ) {
			$this->m_arrobjApartmentAmenities = ( array ) \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchPublishedAmenityRateAssociationsByUnitSpaceIdByPropertyIdByCid( $this->getPropertyUnitId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		return $this->m_arrobjApartmentAmenities;
	}

	public function getOrFetchUnitAddOns( $objDatabase ) {

		if( false == valArr( $this->m_arrobjUnitAddOns ) ) {
			$this->m_arrobjUnitAddOns = ( array ) \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnsByUnitSpaceIdsByPropertyIdByCid( [ $this->getId() ], $this->getPropertyId(), $this->getCid(), $objDatabase, false );
		}

		return $this->m_arrobjUnitAddOns;
	}

	public function getOrFetchUnitAddOnLeaseAssociations( $intLeaseId, $arrintLeaseStatusTypeIds, $objDatabase ) {

		if( false == valArr( $this->m_arrobjUnitAddOnLeaseAssociations ) ) {
			$this->m_arrobjUnitAddOnLeaseAssociations = ( array ) \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationsByUnitSpaceIdByLeaseIdByPropertyIdByCid( $this->getId(), $intLeaseId, $this->getPropertyId(), $this->getCid(), $arrintLeaseStatusTypeIds, $objDatabase );
		}

		return $this->m_arrobjUnitAddOnLeaseAssociations;
	}

	public function getOrFetchSpecials( $objDatabase, $boolIsCouponCodeSpecial = true, $arrintLeaseTermIds = NULL, $boolOnlyIncludeMarketed = false, $strMoveInDate = NULL ) {

		$this->m_arrobjSpecials = ( array ) CSpecials::createService()->fetchSpecialsByPropertyIdByUnitSpaceIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $arrintLeaseTermIds, $objDatabase, $boolIncludeDeletedUnits = false, $boolOnlyIncludeMarketed, $strMoveInDate );

		$this->m_objProperty = $this->getOrFetchProperty( $objDatabase );

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			$objPropertyPreference 		= $this->m_objProperty->fetchPropertyPreferenceByKey( 'SPECIAL_EXPIRATION_DAYS', $objDatabase );
			$intSpecialExpirationDays 	= ( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 0 < $objPropertyPreference->getValue() ) ? $objPropertyPreference->getValue() : 0;

			foreach( $this->m_arrobjSpecials as $intKey => $objSpecial ) {
				if( 1 < $intSpecialExpirationDays ) {
					$objSpecial->setSpecialStartDateEndDate( $intSpecialExpirationDays, NULL );
				}

				if( false == $boolIsCouponCodeSpecial && true == $objSpecial->getCouponCodeRequired() ) {
					unset( $this->m_arrobjSpecials[$intKey] );
				}
			}
		}

		return $this->m_arrobjSpecials;
	}

	public function getOrFetchBaseRent( $objDatabase ) {

		if( true == is_null( $this->m_intBaseRent ) ) {
			CRentController::generateUnitSpaceBaseRent( $this, $objDatabase );
		}

		return $this->m_intBaseRent;
	}

	public function getOrFetchBaseEffectiveRent( $objDatabase ) {

		if( true == is_null( $this->m_intBaseEffectiveRent ) ) {
			CRentController::generateUnitSpaceBaseEffectiveRent( $this, $objDatabase );
		}

		return $this->m_intBaseEffectiveRent;
	}

	public function getOrFetchEffectiveRent( $objDatabase, $intLeaseTerm, $strMoveInDate = NULL ) {

		$strMoveInDate = $this->standardizeDate( $strMoveInDate );

		if( false == isset( $this->m_arrmixEffectiveRents[$strMoveInDate] ) || true == is_null( $this->m_arrmixEffectiveRents[$strMoveInDate] ) || false == isset( $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm] ) || true == is_null( $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm] ) ) {
			CRentController::generateUnitSpaceEffectiveRent( $this, $strMoveInDate, $intLeaseTerm, $objDatabase );
		}

		return $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm];
	}

	public function getOrFetchMaxEffectiveRent( $objDatabase, $arrintLeaseTerms, $strMoveInDate = NULL ) {

		$strMoveInDate = $this->standardizeDate( $strMoveInDate );
		$arrintLeaseTerms = ( true == is_null( $arrintLeaseTerms ) ) ? [ 12 ] : $arrintLeaseTerms;

		$intMaxEffectiveRent = 0;
		foreach( $arrintLeaseTerms as $intLeaseTerm ) {
			if( true == valArr( $this->m_arrmixEffectiveRents ) && ( ( true == array_key_exists( $strMoveInDate, $this->m_arrmixEffectiveRents ) && true == is_null( $this->m_arrmixEffectiveRents[$strMoveInDate] ) ) || ( true == array_key_exists( ( int ) $intLeaseTerm, $this->m_arrmixEffectiveRents[$strMoveInDate] ) && true == is_null( $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm] ) ) ) ) {
				CRentController::generateUnitSpaceEffectiveRent( $this, $strMoveInDate, $intLeaseTerm, $objDatabase );
			}
			$intMaxEffectiveRent = max( $intMaxEffectiveRent, $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm] );
		}

		return $intMaxEffectiveRent;
	}

	public function getOrFetchMinEffectiveRent( $objDatabase, $arrintLeaseTerms, $strMoveInDate = NULL ) {

		$strMoveInDate = $this->standardizeDate( $strMoveInDate );
		$arrintLeaseTerms = ( true == is_null( $arrintLeaseTerms ) ) ? [ 12 ] : $arrintLeaseTerms;

		$intMinEffectiveRent = NULL;
		foreach( $arrintLeaseTerms as $intLeaseTerm ) {
			if( true == is_null( $this->m_arrmixEffectiveRents[$strMoveInDate] ) || false == array_key_exists( $intLeaseTerm, $this->m_arrmixEffectiveRents[$strMoveInDate] ) || true == is_null( $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm] ) ) {
				CRentController::generateUnitSpaceEffectiveRent( $this, $strMoveInDate, $intLeaseTerm, $objDatabase );
			}

			if( 0 < $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm] ) {
				$intMinEffectiveRent = ( true == is_null( $intMinEffectiveRent ) ) ? $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm] : min( $intMinEffectiveRent, $this->m_arrmixEffectiveRents[$strMoveInDate][$intLeaseTerm] );
			}
		}

		$intMinEffectiveRent = ( true == is_null( $intMinEffectiveRent ) ? 0 : $intMinEffectiveRent );

		return $intMinEffectiveRent;
	}

	public function getOrFetchAdditionalSavings( $objDatabase, $intLeaseTerm, $strMoveInDate = NULL ) {

		$strMoveInDate = $this->standardizeDate( $strMoveInDate );

		if( true == is_null( $this->m_arrmixAdditionalSavings[$strMoveInDate] ) || true == is_null( $this->m_arrmixAdditionalSavings[$strMoveInDate][$intLeaseTerm] ) ) {
			CRentController::generateUnitSpaceEffectiveRent( $this, $strMoveInDate, $intLeaseTerm, $objDatabase );
		}

		return $this->m_arrmixAdditionalSavings[$strMoveInDate][$intLeaseTerm];
	}

	public function getOrFetchNotes( $objDatabase ) {

		if( false == valStr( $this->m_strNotes ) ) {
			$this->m_strNotes = \Psi\Eos\Entrata\CEvents::createService()->fetchUnitSpaceNotesByPropertyIdByUnitSpaceIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $objDatabase );
		}

		return $this->m_strNotes;
	}

	public function getOrFetchPropertyFloorNumber( $objDatabase ) {

		if( false == valId( $this->m_intPropertyFloorNumber ) ) {
			$this->m_intPropertyFloorNumber = \Psi\Eos\Entrata\CPropertyFloors::createService()->fetchPropertyFloorNumberByIdByPropertyIdByCid( $this->getPropertyFloorId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		return $this->m_intPropertyFloorNumber;
	}

	public function fetchPublishedPropertyFloorplan( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplanByPropertyIdByPropertyUnitIdByCid( $this->getPropertyId(), $this->getPropertyUnitId(), $this->getCid(), $objDatabase );
	}

	public function fetchPublishedPropertyFloorplanByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPublishedPropertyFloorplanByPropertyIdsByPropertyUnitIdByCid( $arrintPropertyIds, $this->getPropertyUnitId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociationByArOriginReferenceId( $intArOriginReferenceId, $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationByArCascadeIdByArCascadeReferenceIdByArOriginReferenceIdByPropertyIdByCid( CArCascade::SPACE, $this->getId(), $intArOriginReferenceId, $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArCascadeIdByArCascadeReferenceIdByPropertyIdByCid( CArCascade::SPACE, $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchApartmentAmenities( $objDatabase ) {
		return \Psi\Eos\Entrata\CAmenities::createService()->fetchApartmentAmenitiesByUnitSpaceIdsByCid( [ $this->m_intId ], $this->getCid(), $objDatabase );
	}

	/**
	* Get Functions
	*
	*/

	public function getUnitSpaceRateLogs() {
		return $this->m_arrobjUnitSpaceRateLogs;
	}

	public function getLeaseTerms() {
		return $this->m_arrobjLeaseTerms;
	}

	public function getDepositRate() {
		$objDepositRate = NULL;

		if( true == valArr( $this->m_arrobjRates ) ) {
			foreach( $this->m_arrobjRates as $objRate ) {
				if( 'Security Deposit' == $objRate->getDescription() ) {
					$objDepositRate = $objRate;
				}
			}
		}

		return $objDepositRate;
	}

	public function getHasSpecials() {
		return $this->m_boolHasSpecials;
	}

	public function getHasDisplaySpecials() {
		return $this->m_boolHasDisplaySpecials;
	}

	public function getBaseRent() {
		return $this->m_intBaseRent;
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function getPropertyFloorTitle() {
		return $this->m_strPropertyFloorTitle;
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function getPropertyUnit() {
		return $this->m_objPropertyUnit;
	}

	public function getIsFurnished() {
	return $this->m_boolIsFurnished;
	}

	public function getIsAvailable() {
		return $this->m_boolIsAvailable;
	}

	public function getUnitSpaceStatusType() {
		return $this->m_strUnitSpaceStatusType;
	}

	public function getMinRent() {
		return $this->m_fltMinRent;
	}

	public function getMonthToMonthRent() {
		return $this->m_fltMonthToMonthRent;
	}

	public function getCachedMinRent() {
		return $this->m_fltCachedMinRent;
	}

	public function getMinDeposit() {
		return $this->m_fltMinDeposit;
	}

	public function getCachedMinDeposit() {
		return $this->m_fltCachedMinDeposit;
	}

	public function getMaxRent() {
		return $this->m_fltMaxRent;
	}

	public function getCachedMaxRent() {
		return $this->m_fltCachedMaxRent;
	}

	public function getMaxDeposit() {
		return $this->m_fltMaxDeposit;
	}

	public function getCachedMaxDeposit() {
		return $this->m_fltCachedMaxDeposit;
	}

	public function getNumberOfBathrooms() {
		return $this->m_fltNumberOfBathrooms;
	}

	public function getUnitSpecials() {
		return $this->m_arrobjUnitSpecials;
	}

	public function getEffectiveRent() {
		return $this->m_intEffectiveRent;
	}

	public function getApartmentAmenities() {
		return $this->m_arrobjApartmentAmenities;
	}

	public function getUnitAddOns() {
		return $this->m_arrobjUnitAddOns;
	}

	public function getSpecials() {
		return $this->m_arrobjSpecials;
	}

	public function getFloorplanName() {
		return $this->m_strFloorplanName;
	}

	public function getUnitSpecialsList() {
		return $this->m_strUnitSpecialsList;
	}

	public function getApartmentAmenitiesList() {
		return $this->m_strApartmentAmenitiesList;
	}

	public function getApartmentAmenitiesIds() {
		return $this->m_strApartmentAmenitiesIds;
	}

	public function getSpaceConfigurationName() {
		return $this->m_strSpaceConfigurationName;
	}

	// public function getLeaseTerms() {
		// return $this->m_arrintLeaseTerms;
	// }

	public function getUnitTypeName() {
		return $this->m_strUnitTypeName;
	}

	public function getCurrentLeaseEndDate() {
		return $this->m_strCurrentLeaseEndDate;
	}

	public function getUnitSpaceSpecialsCount() {
		return $this->m_intUnitSpaceSpecialsCount;
	}

	public function getUnitSpaceSpecials() {
		return $this->m_arrobjUnitSpaceSpecials;
	}

	public function getBuildingId() {
		return $this->m_intBuildingId;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function getTermMonth() {
		return $this->m_intTermMonth;
	}

	public function getLeaseTermMonths() {
		return $this->m_intLeaseTermMonths;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getLeaseTotalAmount() {
		return $this->m_fltLeaseTotalAmount;
	}

	public function getRentAmount() {
		return $this->m_fltRentAmount;
	}

	public function getHasPets() {
		return $this->m_boolHasPets;
	}

	public function getHasAmenities() {
		return $this->m_boolHasAmenities;
	}

	public function getUnitFloorPlanMedia() {
		return $this->m_objCompanyMediaFile;
	}

	public function getMinSquareFeet() {
		return $this->m_intMinSqureFeet;
	}

	public function getPropertyFloorNumber() {
		return $this->m_intPropertyFloorNumber;
	}

	public function getDepositAmount() {
		return $this->m_fltDepositAmount;
	}

	public function getCachedMinSquareFeet() {
		return $this->m_fltCachedMinSquareFeet;
	}

	public function getRatesCount() {
		return $this->m_intRatesCount;
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function getSpaceConfigurationId() {
		return $this->m_intSpaceConfigurationId;
	}

	public function getExceedsExpirationLimit() {
		return $this->m_boolExceedsExpirationLimit;
	}

	public function getBuildingUnitNumber() {
		return $this->m_strBuildingUnitNumber;
	}

	public function getMergedUnitSpaceIds() {
		return $this->m_strMergedUnitSpaceIds;
	}

	public function getMergedUnitSpaceNumbers() {
		return $this->m_strMergedUnitSpaceNumbers;
	}

	public function getSquareFeet() {
		return $this->m_intSquareFeet;
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function getLeaseStartWindowStartDate() {
		return $this->m_intLeaseStartWindowStartDate;
	}

	public function getLeaseStartWindowEndDate() {
		return $this->m_intLeaseStartWindowEndDate;
	}

	public function getResidentName() {
		return $this->m_strResidentName;
	}

	public function getGenderId() {
		return $this->m_intGenderId;
	}

	public function getUnitSpaceUnionId() {
		return $this->m_intUnitSpaceUnionId;
	}

	public function getTotalUnitSpacesCount() {
		return $this->m_intTotalUnitSpacesCount;
	}

	public function getAvailableUnitCount() {
		return $this->m_intAvailableUnitCount;
	}

	public function getAvailableUnitSpacesCount() {
		return $this->m_intAvailableUnitSpacesCount;
	}

	public function getRevenueOptimalRent() {
		return $this->m_objRevenueOptimalRent;
	}

	public function getDisplayUnitSpaceNumber() {
		return $this->m_strDisplayUnitSpaceNumber;
	}

	public function getDefaultAmenities() {
		return $this->m_strDefaultAmenities;
	}

	public function getMatrixDetails() {
		return $this->m_arrmixMatrixDetails;
	}

	public function getArTriggerId() {
		return $this->m_intArTriggerId;
	}

	public function getArTriggerName() {
		return $this->m_strArTriggerName;
	}

	public function getAmenityRentAmount() {
		return $this->m_fltAmenityRentAmount;
	}

	public function getSpecialRentAmount() {
		return $this->m_fltSpecialRentAmount;
	}

	public function getAddOnRentAmount() {
		return $this->m_fltAddOnRentAmount;
	}

	public function getAmenityAmount() {
		return $this->m_fltAmenityAmount;
	}

	public function getSpecialAmount() {
		return $this->m_fltSpecialAmount;
	}

	public function getAddOnAmount() {
		return $this->m_fltAddOnAmount;
	}

	public function getAmenityTaxAmount() {
		return $this->m_fltAmenityTaxAmount;
	}

	public function getSpecialTaxAmount() {
		return $this->m_fltSpecialTaxAmount;
	}

	public function getAddOnTaxAmount() {
		return $this->m_fltAddOnTaxAmount;
	}

	public function getBaseAmount() {
		return $this->m_fltBaseAmount;
	}

	public function getBaseTaxAmount() {
		return $this->m_fltBaseTaxAmount;
	}

	public function getSitePlanXPos() {
		return $this->m_fltSitePlanXPos;
	}

	public function getSitePlanYPos() {
		return $this->m_fltSitePlanYPos;
	}

	/**
	* Set Functions
	*
	*/

	public function setDefaults() {
		parent::setDefaults();
		$this->setUnitSpaceStatusTypeId( CUnitSpaceStatusType::VACANT_UNRENTED_READY );
		$this->setMergedUnitSpaceIds( NULL );
		$this->setMergedUnitSpaceNumbers( NULL );
	}

	public function setPropertyFloorTitle( $strPropertyFloorTitle ) {
		$this->m_strPropertyFloorTitle = CStrings::strTrimDef( $strPropertyFloorTitle, 50, NULL, true );
	}

	public function setPropertyUnit( $objPropertyUnit ) {
		$this->m_objPropertyUnit = $objPropertyUnit;
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->m_intNumberOfBedrooms = CStrings::strToIntDef( $intNumberOfBedrooms, NULL, false );
	}

	public function setIsFurnished( $intIsFurnished ) {
		$this->m_boolIsFurnished = $intIsFurnished;
	}

	public function setIsAvailable( $intIsAvailable ) {
		$this->m_boolIsAvailable = $intIsAvailable;
	}

	public function setUnitSpaceStatusType( $strUnitSpaceStatusType ) {
		$this->m_strUnitSpaceStatusType = $strUnitSpaceStatusType;
	}

	public function setMinRent( $fltMinRent ) {
		$this->m_fltMinRent = $fltMinRent;
	}

	public function setMonthToMonthRent( $fltMonthToMonthRent ) {
		$this->m_fltMonthToMonthRent = $fltMonthToMonthRent;
	}

	public function setCachedMinRent( $fltCachedMinRent ) {
		$this->m_fltCachedMinRent = $fltCachedMinRent;
	}

	public function setMinDeposit( $fltMinDeposit ) {
		$this->m_fltMinDeposit = $fltMinDeposit;
	}

	public function setCachedMinDeposit( $fltCachedMinDeposit ) {
		$this->m_fltCachedMinDeposit = $fltCachedMinDeposit;
	}

	public function setMaxRent( $fltMaxRent ) {
		$this->m_fltMaxRent = $fltMaxRent;
	}

	public function setCachedMaxRent( $fltCachedMaxRent ) {
		$this->m_fltCachedMaxRent = $fltCachedMaxRent;
	}

	public function setMaxDeposit( $fltMaxDeposit ) {
		$this->m_fltMaxDeposit = $fltMaxDeposit;
	}

	public function setCachedMaxDeposit( $fltCachedMaxDeposit ) {
		$this->m_fltCachedMaxDeposit = $fltCachedMaxDeposit;
	}

	public function setNumberOfBathrooms( $fltNumberOfBathrooms ) {
		$this->m_fltNumberOfBathrooms = CStrings::strToFloatDef( $fltNumberOfBathrooms, NULL, false, 4 );
	}

	public function setUnitSpecials( $arrobjUnitSpecials ) {
		$this->m_arrobjUnitSpecials = $arrobjUnitSpecials;
	}

	public function setUnitSpaceSpecialsCount( $intUnitSpaceSpecialsCount ) {
		$this->m_intUnitSpaceSpecialsCount = $intUnitSpaceSpecialsCount;
	}

	public function setUnitSpaceSpecials( $arrobjUnitSpaceSpecials ) {
		$this->m_arrobjUnitSpaceSpecials = $arrobjUnitSpaceSpecials;
	}

	public function setBuildingId( $intBuildingId ) {
		$this->m_intBuildingId = $intBuildingId;
	}

	public function setCustomerId( $strCustomerId ) {
		$this->m_intCustomerId = $strCustomerId;
	}

	public function setLeaseId( $strLeaseId ) {
		$this->m_intLeaseId = $strLeaseId;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->m_boolIsDeleted = $boolIsDeleted;
	}

	public function setRatesCount( $intRatesCount ) {
		$this->m_intRatesCount = $intRatesCount;
	}

	public function setSpaceConfigurationId( $intSpaceConfigurationId ) {
		$this->m_intSpaceConfigurationId = $intSpaceConfigurationId;
	}

	public function setBuildingUnitNumber( $strBuildingUnitNumber ) {
		$this->m_strBuildingUnitNumber = CStrings::strTrimDef( $strBuildingUnitNumber, 50, NULL, true );
	}

	public function setMergedUnitSpaceIds( $strMergedUnitSpaceIds ) {
		$this->m_strMergedUnitSpaceIds = $strMergedUnitSpaceIds;
	}

	public function setMergedUnitSpaceNumbers( $strMergedUnitSpaceNumbers ) {
		$this->m_strMergedUnitSpaceNumbers = $strMergedUnitSpaceNumbers;
	}

	public function setSquareFeet( $intSquareFeet ) {
		$this->m_intSquareFeet = $intSquareFeet;
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->m_intLeaseStartWindowId = $intLeaseStartWindowId;
	}

	public function setLeaseStartWindowStartDate( $intLeaseStartWindowStartDate ) {
		$this->m_intLeaseStartWindowStartDate = $intLeaseStartWindowStartDate;
	}

	public function setLeaseStartWindowEndDate( $intLeaseStartWindowEndDate ) {
		$this->m_intLeaseStartWindowEndDate = $intLeaseStartWindowEndDate;
	}

	public function setResidentName( $strResidentName ) {
		$this->m_strResidentName = $strResidentName;
	}

	public function setGenderId( $intGenderId ) {
		$this->m_intGenderId = $intGenderId;
	}

	public function setUnitSpaceUnionId( $intUnitSpaceUnionId ) {
		$this->m_intUnitSpaceUnionId = $intUnitSpaceUnionId;
	}

	public function setTotalUnitSpacesCount( $intTotalUnitSpacesCount ) {
		$this->m_intTotalUnitSpacesCount = $intTotalUnitSpacesCount;
	}

	public function setAvailableUnitCount( $intAvailableUnitCount ) {
		$this->m_intAvailableUnitCount = $intAvailableUnitCount;
	}

	public function setAvailableUnitSpacesCount( $intAvailableUnitSpacesCount ) {
		$this->m_intAvailableUnitSpacesCount = $intAvailableUnitSpacesCount;
	}

	public function setDisplayUnitSpaceNumber( $strDisplayUnitSpaceNumber ) {
		$this->m_strDisplayUnitSpaceNumber = $strDisplayUnitSpaceNumber;
	}

	public function setMatrixDetails( $arrmixMatrixDetails ) {
		$this->m_arrmixMatrixDetails = $arrmixMatrixDetails;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['building_unit_number'] ) ) $this->setBuildingUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['building_unit_number'] ) : $arrmixValues['building_unit_number'] );
		if( true == isset( $arrmixValues['building_id'] ) ) $this->setBuildingId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['building_id'] ) : $arrmixValues['building_id'] );
		if( true == isset( $arrmixValues['floor_number'] ) ) $this->setPropertyFloorNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['floor_number'] ) : $arrmixValues['floor_number'] );
		if( true == isset( $arrmixValues['property_floor_title'] ) ) $this->setPropertyFloorTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_floor_title'] ) : $arrmixValues['property_floor_title'] );
		if( true == isset( $arrmixValues['min_deposit'] ) ) $this->setMinDeposit( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['min_deposit'] ) : $arrmixValues['min_deposit'] );
		if( true == isset( $arrmixValues['max_deposit'] ) ) $this->setMaxDeposit( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['max_deposit'] ) : $arrmixValues['max_deposit'] );
		if( true == isset( $arrmixValues['is_furnished'] ) ) $this->setIsFurnished( $arrmixValues['is_furnished'] );
		if( true == isset( $arrmixValues['unit_space_status_type'] ) ) $this->setUnitSpaceStatusType( $arrmixValues['unit_space_status_type'] );
		if( true == isset( $arrmixValues['unit_specials'] ) ) $this->setUnitSpecials( $arrmixValues['unit_specials'] );
		if( true == isset( $arrmixValues['unit_space_specials_count'] ) ) $this->setUnitSpaceSpecialsCount( $arrmixValues['unit_space_specials_count'] );
		if( true == isset( $arrmixValues['has_specials'] ) ) $this->setHasSpecials( $arrmixValues['has_specials'] );
		if( true == isset( $arrmixValues['has_display_specials'] ) ) $this->setHasDisplaySpecials( $arrmixValues['has_display_specials'] );
		if( true == isset( $arrmixValues['floorplan_name'] ) ) $this->setFloorplanName( $arrmixValues['floorplan_name'] );
		if( true == isset( $arrmixValues['unit_type_name'] ) ) $this->setUnitTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unit_type_name'] ) : $arrmixValues['unit_type_name'] );
		if( true == isset( $arrmixValues['cached_min_rent'] ) ) $this->setCachedMinRent( $arrmixValues['cached_min_rent'] );
		if( true == isset( $arrmixValues['cached_max_rent'] ) ) $this->setCachedMaxRent( $arrmixValues['cached_max_rent'] );
		if( true == isset( $arrmixValues['min_rent'] ) ) $this->setMinRent( $arrmixValues['min_rent'] );
		if( true == isset( $arrmixValues['max_rent'] ) ) $this->setMaxRent( $arrmixValues['max_rent'] );
		if( true == isset( $arrmixValues['cached_min_deposit'] ) ) $this->setCachedMinDeposit( $arrmixValues['cached_min_deposit'] );
		if( true == isset( $arrmixValues['cached_max_deposit'] ) ) $this->setCachedMaxDeposit( $arrmixValues['cached_max_deposit'] );
		if( true == isset( $arrmixValues['is_available'] ) ) $this->setIsAvailable( $arrmixValues['is_available'] );

		if( true == isset( $arrmixValues['number_of_bedrooms'] ) ) $this->setNumberOfBedrooms( $arrmixValues['number_of_bedrooms'] );
		if( true == isset( $arrmixValues['number_of_bathrooms'] ) ) $this->setNumberOfBathrooms( $arrmixValues['number_of_bathrooms'] );
		if( true == isset( $arrmixValues['square_feet'] ) ) $this->setSquareFeet( $arrmixValues['square_feet'] );
		if( true == isset( $arrmixValues['gender_id'] ) ) $this->setGenderId( $arrmixValues['gender_id'] );
		if( true == isset( $arrmixValues['unit_space_union_id'] ) ) $this->setUnitSpaceUnionId( $arrmixValues['unit_space_union_id'] );

		if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['lease_id'] ) ) $this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( true == isset( $arrmixValues['is_deleted'] ) ) $this->setIsDeleted( $arrmixValues['is_deleted'] );
		if( true == isset( $arrmixValues['term_month'] ) ) $this->setTermMonth( $arrmixValues['term_month'] );
		if( true == isset( $arrmixValues['lease_term_months'] ) ) $this->setLeaseTermMonths( $arrmixValues['lease_term_months'] );
		if( true == isset( $arrmixValues['lease_term_name'] ) ) $this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		if( true == isset( $arrmixValues['rent_amount'] ) ) $this->setRentAmount( $arrmixValues['rent_amount'] );
		if( true == isset( $arrmixValues['has_pets'] ) ) $this->setHasPets( $arrmixValues['has_pets'] );
		if( true == isset( $arrmixValues['has_amenities'] ) ) $this->setHasAmenities( $arrmixValues['has_amenities'] );
		if( true == isset( $arrmixValues['deposit_amount'] ) ) $this->setDepositAmount( $arrmixValues['deposit_amount'] );
		if( true == isset( $arrmixValues['cached_min_square_feet'] ) ) $this->setCachedMinSquareFeet( $arrmixValues['cached_min_square_feet'] );
		if( true == isset( $arrmixValues['min_square_feet'] ) ) $this->setMinSquarefeet( $arrmixValues['min_square_feet'] );
		if( true == isset( $arrmixValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrmixValues['lease_term_id'] );
		if( true == isset( $arrmixValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrmixValues['ar_trigger_id'] );
		if( true == isset( $arrmixValues['ar_trigger_name'] ) ) $this->setArTriggerName( $arrmixValues['ar_trigger_name'] );
		if( true == isset( $arrmixValues['rates_count'] ) ) $this->setRatesCount( $arrmixValues['rates_count'] );
		if( true == isset( $arrmixValues['unit_specials_list'] ) ) $this->setUnitSpecialsList( $arrmixValues['unit_specials_list'] );
		if( true == isset( $arrmixValues['apartment_amenities_list'] ) ) $this->setApartmentAmenitiesList( $arrmixValues['apartment_amenities_list'] );
		if( true == isset( $arrmixValues['apartment_amenities_ids'] ) ) $this->setApartmentAmenitiesIds( $arrmixValues['apartment_amenities_ids'] );
		if( true == isset( $arrmixValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrmixValues['lease_start_window_id'] );
		if( true == isset( $arrmixValues['lease_start_window_start_date'] ) ) $this->setLeaseStartWindowStartDate( $arrmixValues['lease_start_window_start_date'] );
		if( true == isset( $arrmixValues['lease_start_window_end_date'] ) ) $this->setLeaseStartWindowEndDate( $arrmixValues['lease_start_window_end_date'] );
		if( true == isset( $arrmixValues['resident_name'] ) ) $this->setResidentName( $arrmixValues['resident_name'] );
		if( true == isset( $arrmixValues['lease_total_amount'] ) ) $this->setLeaseTotalAmount( $arrmixValues['lease_total_amount'] );
		if( true == isset( $arrmixValues['total_unit_spaces_count'] ) ) $this->setTotalUnitSpacesCount( $arrmixValues['total_unit_spaces_count'] );
		if( true == isset( $arrmixValues['available_unit_spaces_count'] ) ) $this->setAvailableUnitSpacesCount( $arrmixValues['available_unit_spaces_count'] );

		if( true == isset( $arrmixValues['space_configuration_id'] ) ) $this->setSpaceConfigurationId( $arrmixValues['space_configuration_id'] );
		if( true == isset( $arrmixValues['exceeds_expiration_limit'] ) ) $this->setExceedsExpirationLimit( $arrmixValues['exceeds_expiration_limit'] );
		if( true == isset( $arrmixValues['space_configuration_name'] ) ) $this->setSpaceConfigurationName( $arrmixValues['space_configuration_name'] );
		if( true == isset( $arrmixValues['merged_unit_space_ids'] ) ) $this->setMergedUnitSpaceIds( $arrmixValues['merged_unit_space_ids'] );
		if( true == isset( $arrmixValues['merged_unit_space_numbers'] ) ) $this->setMergedUnitSpaceNumbers( $arrmixValues['merged_unit_space_numbers'] );
		if( true == isset( $arrmixValues['default_amenities'] ) ) $this->setDefaultAmenities( $arrmixValues['default_amenities'] );

		return;
	}

	public function setHasSpecials( $boolHasSpecials ) {
		$this->m_boolHasSpecials = $boolHasSpecials;
	}

	public function setHasDisplaySpecials( $boolHasDisplaySpecials ) {
		$this->m_boolHasDisplaySpecials = $boolHasDisplaySpecials;
	}

	public function setBaseRent( $intBaseRent ) {
		$this->m_intBaseRent = $intBaseRent;
	}

	public function setBaseEffectiveRent( $intBaseEffectiveRent ) {
		$this->m_intBaseEffectiveRent = $intBaseEffectiveRent;
	}

	public function setEffectiveRent( $strMoveInDate, $intLeaseTerm, $intEffectiveRent ) {
		$this->m_arrmixEffectiveRents[$this->standardizeDate( $strMoveInDate )][$intLeaseTerm] = $intEffectiveRent;
	}

	public function setAdditionalSavings( $strMoveInDate, $intLeaseTerm, $intAdditionalSavings ) {
		$this->m_arrmixAdditionalSavings[$this->standardizeDate( $strMoveInDate )][$intLeaseTerm] = $intAdditionalSavings;
	}

	public function setApartmentAmenities( $arrobjApartmentAmenities ) {
		$this->m_arrobjApartmentAmenities = $arrobjApartmentAmenities;
	}

	public function setLeaseTerms( $objLeaseTerms ) {
		$this->m_arrobjLeaseTerms = $objLeaseTerms;
	}

	public function setUnitAddOns( $arrobjUnitAddOns ) {
		$this->m_arrobjUnitAddOns = $arrobjUnitAddOns;
	}

	public function setSpecials( $arrobjSpecials ) {
		$this->m_arrobjSpecials = $arrobjSpecials;
	}

	public function setNotes( $strNotes ) {
		$this->m_strNotes = $strNotes;
	}

	public function setFloorplanName( $strFloorplanName ) {
		$this->m_strFloorplanName = $strFloorplanName;
	}

	public function setUnitSpecialsList( $strUnitSpecialsList ) {
		$this->m_strUnitSpecialsList = $strUnitSpecialsList;
	}

	public function setApartmentAmenitiesList( $strApartmentAmenitiesList ) {
		$this->m_strApartmentAmenitiesList = $strApartmentAmenitiesList;
	}

	public function setApartmentAmenitiesIds( $strApartmentAmenitiesIds ) {
		$this->m_strApartmentAmenitiesIds = $strApartmentAmenitiesIds;
	}

	public function setSpaceConfigurationName( $strSpaceConfigurationName ) {
		$this->m_strSpaceConfigurationName = $strSpaceConfigurationName;
	}

	// public function setLeaseTerms( $arrintLeaseTerms ) {
		// $this->m_arrintLeaseTerms = $arrintLeaseTerms;
	// }

	public function setUnitTypeName( $strUnitTypeName ) {
		$this->m_strUnitTypeName = $strUnitTypeName;
	}

	public function setCurrentLeaseEndDate( $strCurrentLeaseEndDate ) {
		$this->m_strCurrentLeaseEndDate = $strCurrentLeaseEndDate;
	}

	public function setTermMonth( $intTermMonth ) {
		$this->m_intTermMonth = $intTermMonth;
	}

	public function setLeaseTermMonths( $intLeaseTermMonths ) {
		$this->m_intLeaseTermMonths = $intLeaseTermMonths;
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setRentAmount( $fltRentAmount ) {
		$this->m_fltRentAmount = $fltRentAmount;
	}

	public function setLeaseTotalAmount( $fltLeaseTotalAmount ) {
		$this->m_fltLeaseTotalAmount = $fltLeaseTotalAmount;
	}

	public function setHasPets( $boolHasPets ) {
		$this->m_boolHasPets = $boolHasPets;
	}

	public function setHasAmenities( $boolHasAmenities ) {
		$this->m_boolHasAmenities = $boolHasAmenities;
	}

	public function setUnitFloorPlanMedia( $objCompanyMediaFile ) {
		$this->m_objCompanyMediaFile = $objCompanyMediaFile;
	}

	public function setMinSquarefeet( $intMinSqureFeet ) {
		$this->m_intMinSqureFeet = $intMinSqureFeet;
	}

	public function setPropertyFloorNumber( $intPropertyFloorNumber ) {
		$this->m_intPropertyFloorNumber = $intPropertyFloorNumber;
	}

	public function setDepositAmount( $fltDepositAmount ) {
		$this->m_fltDepositAmount = $fltDepositAmount;
	}

	public function setCachedMinSquareFeet( $fltCachedMinSquareFeet ) {
		$this->m_fltCachedMinSquareFeet = $fltCachedMinSquareFeet;
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = $intLeaseTermId;
	}

	public function setExceedsExpirationLimit( $boolExceedsExpirationLimit ) {
		$this->m_boolExceedsExpirationLimit = $boolExceedsExpirationLimit;
	}

	public function setRevenueOptimalRent( $objRevenueOptimalRent ) {
		$this->m_objRevenueOptimalRent = $objRevenueOptimalRent;
	}

	public function setDefaultAmenities( $strDefaultAmenities ) {
		$this->m_strDefaultAmenities = $strDefaultAmenities;
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->m_intArTriggerId = $intArTriggerId;
	}

	public function setArTriggerName( $strArTriggerName ) {
		$this->m_strArTriggerName = $strArTriggerName;
	}

	public function setAmenityRentAmount( $fltAmenityRentAmount ) {
		$this->m_fltAmenityRentAmount = $fltAmenityRentAmount;
	}

	public function setSpecialRentAmount( $fltSpecialRentAmount ) {
		$this->m_fltSpecialRentAmount = $fltSpecialRentAmount;
	}

	public function setAddOnRentAmount( $fltAddOnRentAmount ) {
		$this->m_fltAddOnRentAmount = $fltAddOnRentAmount;
	}

	public function setAmenityAmount( $fltAmenityAmount ) {
		$this->m_fltAmenityAmount = $fltAmenityAmount;
	}

	public function setSpecialAmount( $fltSpecialAmount ) {
		$this->m_fltSpecialAmount = $fltSpecialAmount;
	}

	public function setAddOnAmount( $fltAddOnAmount ) {
		$this->m_fltAddOnAmount = $fltAddOnAmount;
	}

	public function setAmenityTaxAmount( $fltAmenityTaxAmount ) {
		$this->m_fltAmenityTaxAmount = $fltAmenityTaxAmount;
	}

	public function setSpecialTaxAmount( $fltSpecialTaxAmount ) {
		$this->m_fltSpecialTaxAmount = $fltSpecialTaxAmount;
	}

	public function setAddOnTaxAmount( $fltAddOnTaxAmount ) {
		$this->m_fltAddOnTaxAmount = $fltAddOnTaxAmount;
	}

	public function setBaseAmount( $fltBaseAmount ) {
		$this->m_fltBaseAmount = $fltBaseAmount;
	}

	public function setBaseTaxAmount( $fltBaseTaxAmount ) {
		$this->m_fltBaseTaxAmount = $fltBaseTaxAmount;
	}

	public function setSitePlanXPos( $fltSitePlanXPos ) {
		$this->set( 'm_fltSitePlanXPos', CStrings::strToFloatDef( $fltSitePlanXPos, NULL, false, 4 ) );
	}

	public function setSitePlanYPos( $fltSitePlanYPos ) {
		$this->set( 'm_fltSitePlanYPos', CStrings::strToFloatDef( $fltSitePlanYPos, NULL, false, 4 ) );
	}

	/**
	 * Add Functions
	 *
	 */

	public function addLeaseTerm( $objLeaseTerm ) {
		$this->m_arrobjLeaseTerms[$objLeaseTerm->getId()] = $objLeaseTerm;
	}

	public function addUnitSpaceRateLogs( $objUnitSpaceRateLog ) {
		$this->m_arrobjUnitSpaceRateLogs[$objUnitSpaceRateLog->getId()] = $objUnitSpaceRateLog;
	}

	/**
	* Create Functions
	*
	*/

	public function createRate() {
		$objRate = new CRate();
		$objRate->setCid( $this->getCid() );
		$objRate->setPropertyId( $this->getPropertyId() );
		$objRate->setUnitTypeId( $this->getUnitTypeId() );
		$objRate->setUnitSpaceId( $this->getId() );
		$objRate->setArCascadeId( CArCascade::SPACE );
		$objRate->setArCascadeReferenceId( $this->getId() );

		return $objRate;
	}

	public function createRateAssociation() {
		$objRateAssociation = new CRateAssociation();
		$objRateAssociation->setCid( $this->m_intCid );
		$objRateAssociation->setPropertyId( $this->m_intPropertyId );
		$objRateAssociation->setUnitTypeId( $this->m_intUnitTypeId );
		$objRateAssociation->setUnitSpaceId( $this->m_intId );
		$objRateAssociation->setArCascadeId( CArCascade::SPACE );
		$objRateAssociation->setArCascadeReferenceId( $this->m_intId );

		return $objRateAssociation;
	}

	public function createUnitSpaceRateLog() {
		$objUnitSpaceRateLog = new CUnitSpaceRateLog();
		$objUnitSpaceRateLog->setCid( $this->getCid() );
		$objUnitSpaceRateLog->setPropertyId( $this->getPropertyId() );
		$objUnitSpaceRateLog->setUnitSpaceId( $this->getId() );

		return $objUnitSpaceRateLog;
	}

	public function createPropertyCapSubsidy() {

		$objPropertyCapSubsidy = new CPropertyCapSubsidy();
		$objPropertyCapSubsidy->setCid( $this->getCid() );
		$objPropertyCapSubsidy->setPropertyId( $this->getPropertyId() );
		$objPropertyCapSubsidy->setPropertyUnitId( $this->getPropertyUnitId() );
		$objPropertyCapSubsidy->setUnitSpaceId( $this->getId() );

		return $objPropertyCapSubsidy;
	}

	public function createMaintenanceRequest() {

		$objMaintenanceRequest = new CMaintenanceRequest();

		$objMaintenanceRequest->setPropertyId( $this->getPropertyId() );
		$objMaintenanceRequest->setCid( $this->getCid() );
		$objMaintenanceRequest->setIsFloatingMaintenanceRequest( true );

		if( true == valId( $this->getPropertyBuildingId() ) ) {
			$objMaintenanceRequest->setPropertyBuildingId( $this->getPropertyBuildingId() );
		}

		$objMaintenanceRequest->setPropertyUnitId( $this->getPropertyUnitId() );
		$objMaintenanceRequest->setUnitSpaceId( $this->getId() );
		$objMaintenanceRequest->setUnitNumber( $this->getUnitNumberCache() );

		return $objMaintenanceRequest;
	}

	/**
	* Validation Functions
	*
	*/

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Unit space id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyUnitId ) || ( 1 > $this->m_intPropertyUnitId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', __( 'Property unit does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitFloorPlanId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyFloorplanId ) || ( 1 > $this->m_intPropertyFloorplanId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan_id', __( 'Property Floor Plan is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnitSpaceStatusTypeId( $objDatabase, $boolIsUpdatePropertyUnits, $boolIsUpdatePropertyUnitsForWorkOrder = false, $boolIsStudentSemesterSelectionEnabled = false, $arrmixDataToValidate = NULL ) {

		$boolIsValid = true;

		if( true == $boolIsUpdatePropertyUnits ) {

			if( false == valObj( $this->m_objProperty, 'CProperty' ) ) {
				$this->getOrFetchProperty( $objDatabase );
			}

			$objPropertyGlSetting = CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) && false == $objPropertyGlSetting->getActivateStandardPosting() ) {
				return $boolIsValid;
			}

			$arrintUnitSpaceData 	= ( array ) CUnitSpaces::createService()->fetchSimpleUnitSpaceDataByIdByPropertyUnitIdByPropertyIdByCid( $this->getId(), $this->getPropertyUnitId(), $this->getPropertyId(), $this->getCid(), $objDatabase, $boolIsStudentSemesterSelectionEnabled );
			$arrintUnitSpaceData 	= rekeyArray( 'unit_space_id', $arrintUnitSpaceData );

			if( false == valArr( $arrintUnitSpaceData ) && $this->getUnitSpaceStatusTypeId() <> CUnitSpaceStatusType::VACANT_UNRENTED_READY ) {
				$boolIsValid = false;
			} elseif( true == valArr( $arrintUnitSpaceData ) && true == array_key_exists( $this->getId(), $arrintUnitSpaceData ) ) {
				if( $arrintUnitSpaceData[$this->getId()]['intended_unit_space_status_type_id'] != $this->getUnitSpaceStatusTypeId() ) {
					$boolIsValid = false;
				}

				$arrintNotReadyUnitSpaceStatusTypes = [ CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY ];

				if( in_array( $this->getUnitSpaceStatusTypeId(), $arrintNotReadyUnitSpaceStatusTypes ) && array_search( $this->getUnitSpaceStatusTypeId(), $arrintNotReadyUnitSpaceStatusTypes ) == array_search( $arrintUnitSpaceData[$this->getId()]['unit_space_status_type_id'], $arrintNotReadyUnitSpaceStatusTypes ) ) {
					$boolIsValid = true;
				}
			}

			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_status_type_id', __( 'Unit space status type does not appear to be valid.' ) ) );
			}
		} else {

			if( false == $boolIsUpdatePropertyUnitsForWorkOrder && CUnitSpaceStatusType::VACANT_RENTED_NOT_READY == $this->getUnitSpaceStatusTypeId() ) {

				$arrmixData = NULL;
				if( true == getArrayElementByKey( 'can_treat_as_warning', $arrmixDataToValidate ) ) {
					$arrmixData = [ 'can_treat_as_warning' => true ];
				}

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_status_type_id', __( 'The unit you are trying to move-in is marked as not ready. Update the unit status to Ready to perform the move in.' ), NULL, $arrmixData ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valUnitExclusionReasonTypeId( $objDatabase ) {
		$boolIsValid = true;

		if( false == valId( $this->getId() ) ) return $boolIsValid;

		$arrobjLease = \Psi\Eos\Entrata\CLeases::createService()->fetchActiveLeasesByCidByPropertyIdByUnitSpaceIds( $this->getCid(), $this->getPropertyId(), [ $this->getId() ], $objDatabase );

		if( true == valArr( $arrobjLease ) && CUnitExclusionReasonType::NOT_EXCLUDED != $this->getUnitExclusionReasonTypeId() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Unit space is associated with lease must be "Not Excluded".' ) );
		}

		return $boolIsValid;
	}

	public function valSpaceNumber( $objDatabase, $arrstrUnitSpaceNumbers ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strSpaceNumber ) || ( 1 > \Psi\CStringService::singleton()->strlen( $this->m_strSpaceNumber ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_number', __( 'Space number is required.' ) ) );
		} elseif( true == isset( $this->m_strSpaceNumber ) && ( false !== \Psi\CStringService::singleton()->strpos( $this->m_strSpaceNumber, ',' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_number', __( 'Commas may not be used in Unit Space name.' ) ) );
		} else {
			// Validate duplicate space number for curent unit
			if( true == valArr( $arrstrUnitSpaceNumbers ) ) {
				$arrstrUnitSpaceNumbers = array_map( function( $intValue ) {
					return trim( $intValue );
				}, $arrstrUnitSpaceNumbers );

				$arrstrUnitSpaceNumbers = array_count_values( $arrstrUnitSpaceNumbers );
				if( 1 < $arrstrUnitSpaceNumbers[$this->m_strSpaceNumber] ) {
					$boolIsValid = false;
				}
			}

			if( true == $boolIsValid ) {
				$objPreexistingSpaceNumber = CUnitSpaces::createService()->fetchCompetingSpaceNumberByPropertyUnitIdByCid( $this->m_intId, $this->m_strSpaceNumber, $this->m_intPropertyUnitId, $this->getCid(), $objDatabase );
				$boolIsValid = ( true == valObj( $objPreexistingSpaceNumber, 'CUnitSpace' ) ) ? false : true;
			}

			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_number', __( 'Space number is already in use.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valAvailableOn() {
		$boolIsValid = true;

		if( true == valStr( $this->getAvailableOn() ) ) {
			$mixValidatedAvailableOn = CValidation::checkISODateFormat( $this->getAvailableOn(), true );
			if( false === $mixValidatedAvailableOn ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'available_on', __( 'Unit availability date must be in correct date format.' ) ) );
			} else {
				$this->setAvailableOn( date( 'm/d/Y', strtotime( $mixValidatedAvailableOn ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMakeReadyDate( $objDatabase, $objProperty = NULL ) {
		$boolIsValid = true;

		if( true == valObj( $objProperty, 'CProperty' ) && false == $objProperty->isIntegrated( $objDatabase ) && true == valStr( $this->getMakeReadyDate() ) && 1 !== CValidation::checkDate( $this->getMakeReadyDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'make_ready_date', __( 'Make ready date must be in correct date format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$arrobjAddOns 			= ( array ) \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnsByUnitSpaceIdsByPropertyIdByCid( [ $this->getId() ], $this->getPropertyId(), $this->getCid(), $objDatabase );
		$arrobjActiveLeases 	= $this->fetchActiveApplicationsLeases( $objDatabase );
		$arrobjQuotes  			= ( array ) CQuotes::fetchActiveQuotesByUnitSpaceIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$arrobjLeaseUnitSpaces  = ( array ) CLeaseUnitSpaces::fetchActiveLeaseUnitSpaceByUnitSpaceIdByLeaseStatusTypeIdsByPropertyIdByCid( $this->getId(), [ CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::APPLICANT ], $this->getPropertyId(), $this->getCid(), $objDatabase );
		$intCountOrganizationContractUnitSpaces = \Psi\Eos\Entrata\COrganizationContractUnitSpaces::createService()->fetchCountActiveOrganizationContractUnitSpacesByUnitSpaceIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjActiveLeases ) || true == valArr( $arrobjAddOns ) || true == valArr( $arrobjQuotes ) || true == valArr( $arrobjLeaseUnitSpaces ) || 0 < $intCountOrganizationContractUnitSpaces ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Unit space cannot be deleted because other information depends on it ( Leases/AddOns/Quotes/Contracts ).' ) ) );
		}

		return $boolIsValid;
	}

	public function valBudgetedRent() {
		$boolIsValid = true;
		if( true == is_null( $this->m_fltBudgetedRent ) || false == is_numeric( $this->m_fltBudgetedRent ) || 0 > $this->m_fltBudgetedRent ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budgeted_rent', __( 'Budgeted Rent is not valid for {%s,0}', [ $this->m_strUnitNumberCache ] ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valBudgetedBaseRent() {
		$boolIsValid = true;
		if( true == is_null( $this->m_fltBudgetedBaseRent ) || false == is_numeric( $this->m_fltBudgetedBaseRent ) || 0 > $this->m_fltBudgetedBaseRent ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budgeted_base_rent', __( 'Budgeted Base Rent is not valid for {%s,0} ', [ $this->m_strUnitNumberCache ] ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valBudgetedAmenityRent() {
		$boolIsValid = true;
		if( true == is_null( $this->m_fltBudgetedAmenityRent ) || false == is_numeric( $this->m_fltBudgetedAmenityRent ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budgeted_amenity_rent', __( 'Budgeted Amenity Rent is not valid for {%s,0} ', [ $this->m_strUnitNumberCache ] ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valUnitSpaceCount( $objDatabase ) {
		$boolIsValid = true;
		$intUnitSpaceCount = ( int ) CUnitSpaces::createService()->fetchUnitSpaceCount( ' WHERE property_unit_id = ' . $this->m_intPropertyUnitId . ' AND cid = ' . ( int ) $this->m_intCid, $objDatabase );

		if( 1 == $intUnitSpaceCount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Unit space cannot be deleted as at least one space is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRateAdjustmentAmount() {
		$boolIsValid = true;

		if( CRateAdjustmentType::NONE != $this->getRateAdjustmentTypeId() ) {
			if( false == is_numeric( $this->getRateAdjustmentAmount() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Rent Adjustment Amount is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validateMoveinDate( $boolIsInsert = false, $strMoveInDate, $arrobjPropertyPreferences = [] ) {

		// If the unit is available and available on date is older than current date then make it NULL. - NRW
		if( false == is_null( $this->getAvailableOn() ) && strtotime( $this->getAvailableOn() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$this->setAvailableOn( NULL );
		}

		if( false == $boolIsInsert && 0 < strlen( $strMoveInDate ) && false == CValidation::validateDate( $strMoveInDate ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must be in correct date format.' ) ) );
			return false;

		} elseif( false == $boolIsInsert && strtotime( $strMoveInDate ) < strtotime( date( 'm/d/Y' ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must be in the future.' ) ) );
			return false;

		} elseif( false == is_null( $this->getAvailableOn() ) && strtotime( $strMoveInDate ) < strtotime( $this->getAvailableOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must be greater than {%t,0,DATE_NUMERIC_STANDARD}', [ $this->getAvailableOn() ] ) ) );
			return false;

		} elseif( true == is_null( $this->getAvailableOn() ) && CUnitSpaceStatusType::NOTICE_UNRENTED == $this->getUnitSpaceStatusTypeId() && true == array_key_exists( 'DAYS_AFTER_AVAILABLE_ON_NOTICE_UNIT', $arrobjPropertyPreferences ) && true == is_numeric( $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_NOTICE_UNIT']->getValue() ) && 0 < $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_NOTICE_UNIT']->getValue() ) {
			$strValidateDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm' ), date( 'd' ) + $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_NOTICE_UNIT']->getValue(), date( 'Y' ) ) );
			if( strtotime( $strMoveInDate ) > mktime( 0, 0, 0, date( 'm' ), date( 'd' ) + $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_NOTICE_UNIT']->getValue(), date( 'Y' ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must not be greater than {%t,0,DATE_NUMERIC_STANDARD}', [ $strValidateDate ] ) ) );
				return false;
			}

		} elseif( true == is_null( $this->getAvailableOn() ) && CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes == $this->getUnitSpaceStatusTypeId() && true == array_key_exists( 'DAYS_AFTER_AVAILABLE_ON_AVAILABLE_UNIT', $arrobjPropertyPreferences ) && true == is_numeric( $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_AVAILABLE_UNIT']->getValue() ) && 0 < $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_AVAILABLE_UNIT']->getValue() ) {
			$strValidateMoveInDate = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm' ), date( 'd' ) + $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_AVAILABLE_UNIT']->getValue(), date( 'Y' ) ) );
			if( strtotime( $strMoveInDate ) > mktime( 0, 0, 0, date( 'm' ), date( 'd' ) + $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_AVAILABLE_UNIT']->getValue(), date( 'Y' ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must not be greater than {%t,0,DATE_NUMERIC_STANDARD}', [ $strValidateMoveInDate ] ) ) );
				return false;
			}

		} elseif( false == is_null( $this->getAvailableOn() ) && strtotime( $this->getAvailableOn() ) < strtotime( date( 'm/d/Y' ) ) && true == array_key_exists( 'DAYS_BEFORE_AVAILABLE_ON', $arrobjPropertyPreferences ) && true == is_numeric( $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue() ) && 0 < $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue() ) {
			$strMoveInDateRange = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm' ), date( 'd' ) + $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue(), date( 'Y' ) ) );
			if( strtotime( $strMoveInDate ) > mktime( 0, 0, 0, date( 'm' ), date( 'd' ) + $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue(), date( 'Y' ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must not be greater than {%t,0,DATE_NUMERIC_STANDARD}', [ $strMoveInDateRange ] ) ) );
				return false;
			}

		} elseif( false == is_null( $this->getAvailableOn() ) && true == array_key_exists( 'DAYS_BEFORE_AVAILABLE_ON', $arrobjPropertyPreferences ) && true == is_numeric( $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue() ) && 0 < $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue() ) {
			$strDateRange = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', strtotime( $this->getAvailableOn() ) ), date( 'd', strtotime( $this->getAvailableOn() ) ) + $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue(), date( 'Y', strtotime( $this->getAvailableOn() ) ) ) );
			if( strtotime( $strMoveInDate ) < strtotime( $this->getAvailableOn() ) || strtotime( $strMoveInDate ) > mktime( 0, 0, 0, date( 'm', strtotime( $this->getAvailableOn() ) ), date( 'd', strtotime( $this->getAvailableOn() ) ) + $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue(), date( 'Y', strtotime( $this->getAvailableOn() ) ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must be in the range of {%t,0,DATE_NUMERIC_STANDARD} - {%t,1,DATE_NUMERIC_STANDARD}', [ $this->getAvailableOn(), $strDateRange ] ) ) );
				return false;
			}
		}

		return true;
	}

	public function valUnitSpaceIsMerged( $objDatabase ) {
		$boolIsValid = true;
		$arrmixLeaseTerms = ( array ) CLeaseTerms::fetchLeaseTermsNameByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, false, true );

		if( true == valArr( $arrmixLeaseTerms ) ) {

			$arrintLeaseTermIds        = rekeyArray( 'id', $arrmixLeaseTerms );
			$arrintLeaseStartWindowIds = rekeyArray( 'lease_start_window_id', $arrmixLeaseTerms );
			$arrmixUnitSpaceUnions     = ( array ) CUnitSpaceUnions::fetchUnitSpaceUnionsByUnitSpaceIdByLeaseTermIdsByLeaseStartWindowIdsByPropertyIdByCid( $this->getId(), array_keys( $arrintLeaseTermIds ), array_keys( $arrintLeaseStartWindowIds ), $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrmixUnitSpaceUnions ) ) {
				foreach( $arrmixUnitSpaceUnions as $arrmixUnitSpaceUnion ) {

					if( true == valStr( $arrmixUnitSpaceUnion['unit_space_ids'] ) ) {
						$arrintUnitSpaceIds = explode( ',', $arrmixUnitSpaceUnion['unit_space_ids'] );

						if( true == valArr( $arrintUnitSpaceIds ) ) {
							if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintUnitSpaceIds ) || $this->getId() != $arrmixUnitSpaceUnion['group_unit_space_id'] ) {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Unit space cannot be deleted because it is merged with another unit space for another lease terms ' ) ) );
								break;
							}
						}
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $objProperty = NULL, $arrstrUnitSpaceNumbers = NULL, $boolIsUpdatePropertyUnits = false, $boolIsStudentSemesterSelectionEnabled = false, $arrmixDataToValidate = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valBudgetedRent();
				$boolIsValid &= $this->valBudgetedBaseRent();
				$boolIsValid &= $this->valBudgetedAmenityRent();
				$boolIsValid &= $this->valAvailableOn();
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valPropertyUnitFloorPlanId();
				$boolIsValid &= $this->valSpaceNumber( $objDatabase, $arrstrUnitSpaceNumbers );
				$boolIsValid &= $this->valAvailableOn();
				$boolIsValid &= $this->valMakeReadyDate( $objDatabase, $objProperty );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				$boolIsValid &= $this->valUnitSpaceCount( $objDatabase );
				$boolIsValid &= $this->valUnitSpaceIsMerged( $objDatabase );
				break;

			case 'validate_on_property_unit_delete':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			case 'validate_rate_adjustment':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valRateAdjustmentAmount();
				break;

			case 'validate_unit_space_status_type':
				$boolIsValid &= $this->valUnitSpaceStatusTypeId( $objDatabase, $boolIsUpdatePropertyUnits, false, $boolIsStudentSemesterSelectionEnabled, $arrmixDataToValidate );
				break;

			case 'validate_unit_space':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valSpaceNumber( $objDatabase, $arrstrUnitSpaceNumbers );
				$boolIsValid &= $this->valAvailableOn();
				$boolIsValid &= $this->valMakeReadyDate( $objDatabase, $objProperty );
				$boolIsValid &= $this->valUnitSpaceStatusTypeId( $objDatabase, $boolIsUpdatePropertyUnits, false, $boolIsStudentSemesterSelectionEnabled );
				break;

			case 'validate_unit_space_update':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valBudgetedRent();
				$boolIsValid &= $this->valBudgetedBaseRent();
				$boolIsValid &= $this->valBudgetedAmenityRent();
				$boolIsValid &= $this->valAvailableOn();
				$boolIsValid &= $this->valSpaceNumber( $objDatabase, $arrstrUnitSpaceNumbers );
				break;

			case 'validate_reset_occupany_certificate':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				break;

			default:
				// case default
				break;
		}

		return $boolIsValid;
	}

	/**
	* Fetch Functions
	*
	*/

	public function fetchActiveLeaseIntervalsOrderedByLeaseStartDate( $objDatabase, $arrintSkippedLeaseStatusTypeIds = NULL, $intLeaseId ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchActiveLeaseIntervalsByUnitSpaceIdOrderedByLeaseStartDateByCid( $this->getId(), $this->getCid(), $objDatabase, $arrintSkippedLeaseStatusTypeIds, $intLeaseId );
	}

	public function fetchPropertyUnit( $objDatabase ) {
		$this->m_objPropertyUnit = CPropertyUnits::createService()->fetchPropertyUnitByUnitSpaceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );

		return $this->m_objPropertyUnit;
	}

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchCustomerConciergeServices( $objDatabase ) {
		return CCustomerConciergeServices::fetchCustomerConciergeServicesByUnitSpaceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyApplications( $objDatabase ) {
		return CPropertyApplications::fetchPropertyApplicationsByUnitSpaceId( $this->m_intId, $objDatabase );
	}

	public function fetchApplications( $objDatabase ) {
		return CApplications::fetchApplicationsByUnitSpaceIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchCustomLeasesByUnitSpaceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseProcess( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByTransferUnitSpaceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchResidentInsurancePolicies( $objDatabase ) {
		return CResidentInsurancePolicies::fetchResidentInsurancePoliciesByUnitSpaceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPackages( $objDatabase ) {
		return CPackages::fetchPackagesByUnitSpaceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceOccupancyHistories( $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitSpaceOccupancyHistories::createService()->fetchUnitSpaceOccupancyHistoriesByUnitSpaceIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpacePriority( $objDatabase ) {
		return CUnitSpacePriorities::fetchUnitSpacePriorityByUnitSpaceIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnitByPropertyFloorId( $intPropertyFloorId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByUnitSpaceIdByPropertyFloorIdByCid( $this->m_intId, $intPropertyFloorId, $this->getCid(), $objDatabase );
	}

	/** This function does not have any usage so commenting this function
	public function fetchAppliedUnitSpaceCachedUnitRatesByMoveinDateWithLeaseTerms( $strMoveInDate, $objDatabase ) {

		$objProperty = $this->getOrFetchProperty( $objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return false;
		}

		$arrobjLeaseTerms 	= ( array ) $objProperty->getOrFetchLeaseTerms( $objDatabase );
		$arrobjUnitSpaceCachedRates = ( array ) CUnitSpaceCachedRates::fetchUnitSpaceCachedRatesByUnitSpaceIdByMoveinDateByCid( $this->getId(), $strMoveInDate, $this->getCid(), $objDatabase );

		$arrobjAppliedUnitSpaceCachedRates = array();

		foreach( $arrobjUnitSpaceCachedRates as $objUnitSpaceCachedRate ) {
			if( true == is_null( $objUnitSpaceCachedRate->getEffectiveDate() ) && true == is_null( $objUnitSpaceCachedRate->getLeaseTerm() ) ) {
				$arrobjAppliedUnitSpaceCachedRates[0] = $objUnitSpaceCachedRate;
				break;
			}
		}

		$objRateCollection = new CRateCollection;
		$objRateCollection->addAll( $arrobjUnitSpaceCachedRates );

		$objEffectiveDate = new DateTime( $strMoveInDate );
		foreach( $arrobjLeaseTerms as $objLeaseTerm ) {

			$objAppliedUnitSpaceCachedRate = $objRateCollection->find( $this->getId(), $objEffectiveDate, $objLeaseTerm->getTermMonth() );

			if( false == valObj( $objAppliedUnitSpaceCachedRate, 'CUnitSpaceCachedRate' ) ) {
				continue;
			}

			$arrobjAppliedUnitSpaceCachedRates[] = $objAppliedUnitSpaceCachedRate;
		}

		return $arrobjAppliedUnitSpaceCachedRates;
	}*/

	public function fetchUnitSpaceHistory( $objDatabase ) {
		return \Psi\Eos\Entrata\CUnitSpaceLogs::createService()->fetchUnitSpaceLogsWithResidentByUnitSpaceIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchAlls( $objDatabase ) {
		return CSpecials::createService()->fetchAllSpecialsByPropertyIdByUnitSpaceIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchActiveLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeasesByPropertyIdByUnitSpaceIdByLeaseStatusTypeIdsByCid( $this->m_intPropertyId, $this->m_intId, [ CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::APPLICANT ], $this->getCid(), $objDatabase );
	}

	public function fetchActiveApplicationsLeases( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchActiveApplicationsLeasesByPropertyIdByUnitSpaceIdByLeaseStatusTypeIdsByCid( $this->m_intPropertyId, $this->m_intId, [ CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::APPLICANT ], $this->getCid(), $objDatabase );
	}

	public function fetchPublishedAmenityRateAssociations( $objDatabase, $intIsOptional = NULL, $intIncludeInRent = NULL, $boolIncludeNegativeFeatures = false ) {
		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchPublishedAmenityRateAssociationsByUnitSpaceIdByCid( $this->getId(), $this->getCid(), $objDatabase, $intIsOptional, $intIncludeInRent, $boolIncludeNegativeFeatures );
	}

	/**
	* Other Functions
	*
	*/

	public function refreshField( $strFieldName, $strTableName, $objDatabase ) {
		parent::refreshField( $strFieldName, $strTableName, $objDatabase );
	}

	public function fetchWebsiteSpecialRateAssociations( $objDatabase ) {

		return \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchWebsiteSpecialsByPropertyIdByUnitTypeIdByFloorplanIdByUnitSpaceIdByCid( $this->getPropertyId(), $this->getUnitTypeId(), $this->getPropertyFloorplanId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function loadPricingAndLeaseTerms( $objDatabase, $strMoveInDate = NULL, $boolIsProspect = false, $boolIsRenewal = false, $boolIsEntrataOnly = true, $boolIsDateBased = false, $boolAllowLeaseExpirationOverride = false, $boolIsCouponCodeSpecial = true, $boolOnlyIncludeMarketed = false, $arrintArTriggerIds = [ CArTrigger::MONTHLY ], $boolUseMatrix = false, $intMatrixDays = NULL, $intDefaultMoveInDateValue = NULL, $strOccupancyType = NULL, $objProperty = NULL, $objWebsiteProperty = NULL, $boolIsReservationCode = NULL ) {
		$objArOriginRules = \Psi\Eos\Entrata\CPropertyArOriginRules::createService()->fetchPropertyArOriginRuleByPropertyIdByArOriginIdByCid( $this->getPropertyId(), CArOrigin::SPECIAL, $this->getCid(), $objDatabase );
		$arrmixLeaseTermPricing = ( array ) \Psi\Eos\Entrata\CRateLogs::createService()->fetchSimpleUnitSpaceLeaseTermPricing( $this, $objDatabase, $strMoveInDate, $boolIsProspect, $boolIsRenewal, $boolIsEntrataOnly, $boolIsDateBased, $boolAllowLeaseExpirationOverride, $arrintArTriggerIds, $boolIncludeCommercialLeaseTerms = false, $boolUseMatrix, $intMatrixDays, $intDefaultMoveInDateValue, NULL, NULL, $strOccupancyType, $objProperty, $objWebsiteProperty, $boolIsReservationCode );

		if( true == valObj( $objArOriginRules, 'CPropertyArOriginRule' ) && ( !$objArOriginRules->getUseLeaseTermsForRent() || !$objArOriginRules->getUseLeaseTermsForDeposits() || !$objArOriginRules->getUseLeaseTermsForOther() ) ) {
			$arrobjSpecials = $this->fetchWebsiteSpecialRateAssociations( $objDatabase );
		}

		if( true == $boolUseMatrix && false == is_null( $intMatrixDays ) ) {
			$arrintLeaseTermIds = array_keys( rekeyArray( 'lease_term_id', $arrmixLeaseTermPricing ) );

			if( true == valObj( $objArOriginRules, 'CPropertyArOriginRule' ) && ( true == $objArOriginRules->getUseLeaseTermsForRent() || true == $objArOriginRules->getUseLeaseTermsForDeposits() || true == $objArOriginRules->getUseLeaseTermsForOther() ) ) {
				$arrobjSpecials = $this->getOrFetchSpecials( $objDatabase, $boolIsCouponCodeSpecial, $arrintLeaseTermIds, $boolOnlyIncludeMarketed, $strMoveInDate );
			}

			$intBeforeMatrixDays            = ( 0 === $intMatrixDays % 2 ) ? floor( $intMatrixDays / 2 ) - 1 : floor( $intMatrixDays / 2 );

			$strStartDate                   = new DateTime( $strMoveInDate );
			$strStartDate->modify( '-' . $intBeforeMatrixDays . ' days' );
			$strMatrixEndDate               = date( 'm/d/Y', strtotime( $strMoveInDate . '-' . $intBeforeMatrixDays . ' days' ) );
			$strEndDate                     = new DateTime( $strMatrixEndDate );

			$strMatrixUnitAvailableOnDate   = date( 'm/d/Y', strtotime( $this->getAvailableOn() . '+' . $intBeforeMatrixDays . ' days' ) );

			if( strtotime( $strMoveInDate ) <= strtotime( 'today' . '+' . $intBeforeMatrixDays . ' days' ) ) {
				$strStartDate               = new DateTime();
				$strEndDate 	            = new DateTime();
			}

			if( strtotime( $strMoveInDate ) <= strtotime( $strMatrixUnitAvailableOnDate ) ) {
				if( strtotime( $this->getAvailableOn() ) <= strtotime( 'today' ) ) {
					$strStartDate           = new DateTime();
					$strEndDate             = new DateTime();
				} else {
					$strStartDate           = new DateTime( $this->getAvailableOn() );
					$strEndDate             = new DateTime( $this->getAvailableOn() );
				}
			}

			if( false == is_null( $intDefaultMoveInDateValue ) && strtotime( $strMoveInDate ) >= strtotime( $strMatrixUnitAvailableOnDate ) ) {
				$strDefaultMoveInDateValue  = date( 'm/d/Y', strtotime( 'today' . '+' . $intDefaultMoveInDateValue . ' days' ) );

				if( strtotime( $strMoveInDate ) <= strtotime( $strDefaultMoveInDateValue . '+' . $intBeforeMatrixDays . ' days' ) ) {
					$strStartDate           = new DateTime( $strDefaultMoveInDateValue );
					$strEndDate             = new DateTime( $strDefaultMoveInDateValue );
				}
			}

			$strEndDate->modify( '+' . $intMatrixDays . ' days' );
			$arrobjMatrixPeriod 	        = new DatePeriod( $strStartDate, new DateInterval( 'P1D' ), $strEndDate );
			$arrintSortLeaseIds = [];
			foreach( $arrobjMatrixPeriod as $objMatrixPeriod ) {
				$strMatrixDate = $objMatrixPeriod->format( 'm/d/Y' );

				foreach( $arrmixLeaseTermPricing AS $mixLeaseTerms ) {
					if( true == is_null( $mixLeaseTerms['effective_date'] ) && true == is_null( $mixLeaseTerms['rent'] ) ) {
						$mixLeaseTerms['effective_date'] = $strMatrixDate;
					}

					if( $strMatrixDate == $mixLeaseTerms['effective_date'] ) {
						$arrmixRentMatrixLeaseTermsData[$strMatrixDate][$mixLeaseTerms['lease_term_id']] = $mixLeaseTerms;

						foreach( $arrobjSpecials AS $objSpecial ) {
							if( $mixLeaseTerms['lease_term_id'] == $objSpecial->getLeaseTermId() || true == is_null( $objSpecial->getLeaseTermId() ) ) {
								$arrmixRentMatrixLeaseTermsData[$strMatrixDate][$mixLeaseTerms['lease_term_id']]['specials'][] = $objSpecial;
							}
						}
					}
				}

				// Fixes for when lease term sort order is Best Price rates were not showing proper
				if( true == valArr( $arrintSortLeaseIds ) ) {
					foreach( $arrintSortLeaseIds AS $intSortLeaseId ) {
						if( true == valArr( $arrmixRentMatrixLeaseTermsData[$strMatrixDate] ) && true == array_key_exists( $intSortLeaseId, $arrmixRentMatrixLeaseTermsData[$strMatrixDate] ) ) {
							$arrmixRentMatrixLeaseTermsSortedData[$strMatrixDate][$intSortLeaseId] = $arrmixRentMatrixLeaseTermsData[$strMatrixDate][$intSortLeaseId];
						}
					}
					if( true == valArr( $arrmixRentMatrixLeaseTermsData ) && true == valArr( $arrmixRentMatrixLeaseTermsSortedData ) ) {
						$arrmixRentMatrixLeaseTermsData = array_merge( $arrmixRentMatrixLeaseTermsData, $arrmixRentMatrixLeaseTermsSortedData );
					}
				} elseif( false == valArr( $arrintSortLeaseIds ) && true == valArr( $arrmixRentMatrixLeaseTermsData ) && true == valArr( $arrmixRentMatrixLeaseTermsData[$strMatrixDate] ) ) {
						$arrintSortLeaseIds = array_keys( $arrmixRentMatrixLeaseTermsData[$strMatrixDate] );
				}
			}
			return $arrmixRentMatrixLeaseTermsData;

		} else {
			$arrmixLeaseTermPricing 	= rekeyArray( 'lease_term_id', $arrmixLeaseTermPricing );
			$arrintLeaseTermIds 		= array_keys( $arrmixLeaseTermPricing );

			if( true == valObj( $objArOriginRules, 'CPropertyArOriginRule' ) && ( true == $objArOriginRules->getUseLeaseTermsForRent() || true == $objArOriginRules->getUseLeaseTermsForDeposits() || true == $objArOriginRules->getUseLeaseTermsForOther() ) ) {
				$arrobjSpecials = $this->getOrFetchSpecials( $objDatabase, $boolIsCouponCodeSpecial, $arrintLeaseTermIds, $boolOnlyIncludeMarketed, $strMoveInDate );
			}

			foreach( $arrmixLeaseTermPricing AS $intLeaseTermId => $mixLeaseTermPricing ) {
				foreach( $arrobjSpecials AS $objSpecial ) {
					if( $intLeaseTermId == $objSpecial->getLeaseTermId() || true == is_null( $objSpecial->getLeaseTermId() ) ) {
						$arrmixLeaseTermPricing[$intLeaseTermId]['specials'][] = $objSpecial;
					}
				}
			}

			return $arrmixLeaseTermPricing;
		}
	}

	public function fetchSpecials( $objDatabase, $boolIsCouponCodeSpecial = true, $boolOnlyIncludeMarketed = false ) {

		$objArOriginRules = \Psi\Eos\Entrata\CPropertyArOriginRules::createService()->fetchPropertyArOriginRuleByPropertyIdByArOriginIdByCid( $this->getPropertyId(), CArOrigin::SPECIAL, $this->getCid(), $objDatabase );

		if( true == $objArOriginRules->getUseLeaseTermsForRent() || true == $objArOriginRules->getUseLeaseTermsForDeposits() || true == $objArOriginRules->getUseLeaseTermsForOther() ) {
			$this->m_arrobjSpecials = ( array ) CSpecials::createService()->fetchSpecialsWithLeaseTermsByPropertyIdByPropertyFloorPlanIdByUnitTypeIdByUnitSpaceIdByCid( $this->getPropertyId(), $this->getPropertyFloorplanId(), $this->getUnitTypeId(), $this->getId(), $this->getCid(), $objDatabase, $boolOnlyIncludeMarketed );
		} else {
			$this->m_arrobjSpecials = $this->fetchWebsiteSpecialRateAssociations( $objDatabase );
		}

		$this->m_objProperty = $this->getOrFetchProperty( $objDatabase );

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			$objPropertyPreference 		= $this->m_objProperty->fetchPropertyPreferenceByKey( 'SPECIAL_EXPIRATION_DAYS', $objDatabase );
			$intSpecialExpirationDays 	= ( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 0 < $objPropertyPreference->getValue() ) ? $objPropertyPreference->getValue() : 0;

			foreach( $this->m_arrobjSpecials as $intKey => $objSpecial ) {
				if( 1 < $intSpecialExpirationDays ) {
					$objSpecial->setSpecialStartDateEndDate( $intSpecialExpirationDays, NULL );
				}

				if( false == $boolIsCouponCodeSpecial && true == $objSpecial->getCouponCodeRequired() ) {
					unset( $this->m_arrobjSpecials[$intKey] );
				}
			}
		}

		return $this->m_arrobjSpecials;
	}

	public function loadPricing( $objDatabase ) {
		$arrmixLeaseTermPricing = ( array ) \Psi\Eos\Entrata\CRateLogs::createService()->fetchSimpleUnitSpaceLeaseTermPricing( $this, $objDatabase, $strMoveInDate = date( 'm/d/Y' ), $boolIsApplication = false, $boolIsRenewal = false, $boolIsEntrataOnly = true, $boolIsDateBased = false, $boolAllowLeaseExpirationOverride = false, [ CArTrigger::APPLICATION_COMPLETED, CArTrigger::MOVE_IN, CArTrigger::MONTHLY ], $boolIncludeCommercialLeaseTerms = false, $boolUseMatrix = false, $intMatrixDays = NULL, $intDefaultMoveInDateValue = NULL, $arrintLeaseTermIds = NULL );
		$fltDepositAmount 	    = 0;

		if( true == valArr( $arrmixLeaseTermPricing ) ) {
			$fltDepositAmount = min( array_column( $arrmixLeaseTermPricing, 'deposit' ) );
		}

		$this->setMinDeposit( __( '{%f,0,p:2}', [ $fltDepositAmount ] ) );

		return;
	}

	public function disassociateData( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false, $boolDeleteUnitSpaceUnions = false ) {
		// update / delete associated data

		$strSql = '';

		$strSql .= 'UPDATE lease_processes SET transfer_unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND transfer_unit_space_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE application_duplicates SET property_unit_id = NULL, unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND unit_space_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE customer_concierge_services SET unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND unit_space_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE packages SET unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND unit_space_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE maintenance_requests SET unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND unit_space_id = ' . ( int ) $this->getId() . ';';
		$strSql .= 'UPDATE resident_insurance_policies SET property_unit_id = NULL, unit_space_id = NULL WHERE cid = ' . ( int ) $this->m_intCid . ' AND unit_space_id = ' . ( int ) $this->getId() . ';';
		$strSql .= ( true == $boolDeleteUnitSpaceUnions ) ? 'DELETE FROM unit_space_unions WHERE cid = ' . ( int ) $this->m_intCid . ' AND ( group_unit_space_id = ' . ( int ) $this->getId() . ' OR unit_space_id = ' . ( int ) $this->getId() . ' );' : '';
		// Unit space priorities
		$strSql .= 'UPDATE 
						unit_space_priorities usp
					SET 
						priority_order = ( usp.priority_order - 1 ),
						updated_by = ' . ( int ) $intCompanyUserId . ',
						updated_on = \'NOW()\'
					FROM
						unit_space_priorities usp1
					WHERE
						usp.property_id = ' . ( int ) $this->getPropertyId() . '
						AND usp.cid = ' . ( int ) $this->getCid() . '
						AND usp.priority_order > usp1.priority_order
						AND usp1.unit_space_id = ' . ( int ) $this->getId() . '
						AND usp1.cid = usp.cid
						AND usp1.property_id = usp1.property_id
						AND usp1.priority_order > 0;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		return $objDatabase->createDataset()->execute( $strSql );
	}

	public function calculateEffectiveRent( $objPropertyUnit, $boolUseRentAmountAsDefault = false ) {

		if( false == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {
			return false;
		}

		$intRentAmount = $this->getBaseRent();
		$objPropertyUnit->setMarketRent( $intRentAmount );

		if( $this->getRateAdjustmentTypeId() == CRateAdjustmentType::NONE ) {
			$this->m_intEffectiveRent = $intRentAmount;
			return $this->m_intEffectiveRent;
		}

		// Calculate the effective rent, according to "adjustment_type, adjustment_unit and adjustment_amount
		$this->m_intEffectiveRent = 0;
		if( false == is_numeric( $this->getRateAdjustmentAmount() ) ) {
			if( true == $boolUseRentAmountAsDefault ) {
				$this->m_intEffectiveRent = $intRentAmount;
			}

			return $this->m_intEffectiveRent;
		}

		if( $this->getRateAdjustmentTypeId() == CRateAdjustmentType::FIXED_AT || 0 == ( int ) $this->getRateAdjustmentAmount() ) {
			$this->m_intEffectiveRent = $this->getRateAdjustmentAmount();
			return $this->m_intEffectiveRent;
		}

		if( false == is_null( $this->getRateAdjustmentAmount() ) ) {
			if( $this->getRateAdjustmentUnitId() == CRateAdjustmentUnit::DOLLARS ) {
				$intEffectiveRentAmount = $this->getRateAdjustmentAmount();
			} else {
				$intEffectiveRentAmount = $intRentAmount * ( ( int ) $this->getRateAdjustmentAmount() / 100 );
			}

			if( $this->getRateAdjustmentTypeId() == CRateAdjustmentType::DECREASE_BY ) {
				$this->m_intEffectiveRent = $intRentAmount - $intEffectiveRentAmount;
			} elseif( $this->getRateAdjustmentTypeId() == CRateAdjustmentType::INCREASE_BY ) {
				$this->m_intEffectiveRent = $intRentAmount + $intEffectiveRentAmount;
			}
		}

		return $this->m_intEffectiveRent;
	}

	public static function standardizeDate( $strDate ) {
		$intTimeStamp = strtotime( $strDate );
		if( false == $intTimeStamp ) {
			$intTimeStamp = strtotime( 'today' );
		}

		return date( 'm/d/Y', $intTimeStamp );
	}

	public function forceIntegration( $objProperty, $intUserId, $objDatabase ) {
		if( true == is_null( $this->getRemotePrimaryKey() ) || true == is_null( $objProperty->getRemotePrimaryKey() ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $objProperty->getCid(), CIntegrationService::RETRIEVE_PROPERTY_UNIT, $intUserId, $objDatabase );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setUnitSpace( $this );
		$objGenericWorker->setPropertyUnit( $this->getOrFetchPropertyUnit( $objDatabase ) ); // we should call integration using only unit space. After making changes for all integration we will remove this line.
		$objGenericWorker->setCallTimeLimit( 3 );
		$boolIsSuccessful = $objGenericWorker->process();

		return $boolIsSuccessful;
	}

	public function deAssociateUnitSpace( $intCompanyUserId, $objApplication, $objDatabase ) {

		if( false == valObj( $objApplication, 'CApplication' ) ) return false;

		$strSql = 'UPDATE
						unit_spaces us
					SET
						application_lease_id = NULL,
						updated_by = ' . ( int ) $intCompanyUserId . ',
						updated_on = \'NOW()\'
					WHERE
						us.cid = ' . ( int ) $objApplication->getCid() . '
						AND property_id = ' . ( int ) $objApplication->getPropertyId() . '
						AND us.application_lease_id = ' . ( int ) $objApplication->getId() . '
						AND us.id = ' . ( int ) $this->getId();

		if( false == $this->executeSql( $strSql, $this, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrobjUnitSpaceExclusion = \Psi\Eos\Entrata\CUnitSpaceExclusions::createService()->fetchUnitSpaceExclusionsByUnitExclusionReasonTypeIdByPropertyIdsByCid( $this->getUnitExclusionReasonTypeId(), [ $this->getPropertyId() ], $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjUnitSpaceExclusion ) ) {
			$objUnitSpaceExclusion = current( $arrobjUnitSpaceExclusion );
		}

		if( $this->getUnitExclusionReasonTypeId() == CUnitExclusionReasonType::NOT_EXCLUDED || ( true == valObj( $objUnitSpaceExclusion, 'CUnitSpaceExclusion' ) && true == $objUnitSpaceExclusion->getIsReportable() ) ) {
			$this->setIsReportable( true );
		} else {
			$this->setIsReportable( false );
		}

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = [];
			$boolUpdate = false;
		}

		if( false == $boolReturnSqlOnly ) {
			$arrintActivateStandardPosting = fetchData( 'SELECT activate_standard_posting, is_ar_migration_mode FROM property_gl_settings WHERE cid = ' . ( int ) $this->getCid() . ' AND property_id = ' . ( int ) $this->getPropertyId(), $objDatabase );
		}

		$strSql = 'UPDATE
					public.unit_spaces
					SET ';

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' property_id = ' . $this->sqlPropertyId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName( 'property_id' ) ) {
			$arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId();
			$strSql .= ' property_id = ' . $this->sqlPropertyId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlPropertyUnitId() ) != $this->getOriginalValueByFieldName( 'property_unit_id' ) ) {
			$arrstrOriginalValueChanges['property_unit_id'] = $this->sqlPropertyUnitId();
			$strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlOccupancyTypeId() ) != $this->getOriginalValueByFieldName( 'occupancy_type_id' ) ) {
			$arrstrOriginalValueChanges['occupancy_type_id'] = $this->sqlOccupancyTypeId();
			$strSql .= ' occupancy_type_id = ' . $this->sqlOccupancyTypeId() . ',';
			$boolUpdate = true;
		}

		// We should never need to update the unit status with php if entrata core is activated.  It should be completely managed with triggers
		if( ( false == isset ( $arrintActivateStandardPosting[0]['activate_standard_posting'] ) || 0 == $arrintActivateStandardPosting[0]['activate_standard_posting'] )
			|| ( false == isset ( $arrintActivateStandardPosting[0]['is_ar_migration_mode'] ) || true == $arrintActivateStandardPosting[0]['is_ar_migration_mode'] ) ) {

			if( false == $this->getAllowDifferentialUpdate() ) {
				$strSql .= ' unit_space_status_type_id = ' . $this->sqlUnitSpaceStatusTypeId() . ',';
			} elseif( CStrings::reverseSqlFormat( $this->sqlUnitSpaceStatusTypeId() ) != $this->getOriginalValueByFieldName( 'unit_space_status_type_id' ) ) {
				$arrstrOriginalValueChanges['unit_space_status_type_id'] = $this->sqlUnitSpaceStatusTypeId();
				$strSql .= ' unit_space_status_type_id = ' . $this->sqlUnitSpaceStatusTypeId() . ',';
				$boolUpdate = true;
			}
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' rate_adjustment_type_id = ' . $this->sqlRateAdjustmentTypeId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlRateAdjustmentTypeId() ) != $this->getOriginalValueByFieldName( 'rate_adjustment_type_id' ) ) {
			$arrstrOriginalValueChanges['rate_adjustment_type_id'] = $this->sqlRateAdjustmentTypeId();
			$strSql .= ' rate_adjustment_type_id = ' . $this->sqlRateAdjustmentTypeId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' rate_adjustment_unit_id = ' . $this->sqlRateAdjustmentUnitId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlRateAdjustmentUnitId() ) != $this->getOriginalValueByFieldName( 'rate_adjustment_unit_id' ) ) {
			$arrstrOriginalValueChanges['rate_adjustment_unit_id'] = $this->sqlRateAdjustmentUnitId();
			$strSql .= ' rate_adjustment_unit_id = ' . $this->sqlRateAdjustmentUnitId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' unit_exclusion_reason_type_id = ' . $this->sqlUnitExclusionReasonTypeId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlUnitExclusionReasonTypeId() ) != $this->getOriginalValueByFieldName( 'unit_exclusion_reason_type_id' ) ) {
			$arrstrOriginalValueChanges['unit_exclusion_reason_type_id'] = $this->sqlUnitExclusionReasonTypeId();
			$strSql .= ' unit_exclusion_reason_type_id = ' . $this->sqlUnitExclusionReasonTypeId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlRemotePrimaryKey() ) != $this->getOriginalValueByFieldName( 'remote_primary_key' ) ) {
			$arrstrOriginalValueChanges['remote_primary_key'] = $this->sqlRemotePrimaryKey();
			$strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' property_name = ' . $this->sqlPropertyName() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlPropertyName() ) != $this->getOriginalValueByFieldName( 'property_name' ) ) {
			$arrstrOriginalValueChanges['property_name'] = $this->sqlPropertyName();
			$strSql .= ' property_name = ' . $this->sqlPropertyName() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' building_name = ' . $this->sqlBuildingName() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlBuildingName() ) != $this->getOriginalValueByFieldName( 'building_name' ) ) {
			$arrstrOriginalValueChanges['building_name'] = $this->sqlBuildingName();
			$strSql .= ' building_name = ' . $this->sqlBuildingName() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' marketing_unit_number = ' . $this->sqlMarketingUnitNumber() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlMarketingUnitNumber() ) != $this->getOriginalValueByFieldName( 'marketing_unit_number' ) ) {
			$arrstrOriginalValueChanges['marketing_unit_number'] = $this->sqlMarketingUnitNumber();
			$strSql .= ' marketing_unit_number = ' . $this->sqlMarketingUnitNumber() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' marketing_unit_number_cache = ' . $this->sqlMarketingUnitNumberCache() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlMarketingUnitNumberCache() ) != $this->getOriginalValueByFieldName( 'marketing_unit_number_cache' ) ) {
			$arrstrOriginalValueChanges['marketing_unit_number_cache'] = $this->sqlMarketingUnitNumberCache();
			$strSql .= ' marketing_unit_number_cache = ' . $this->sqlMarketingUnitNumberCache() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlUnitNumberCache() ) != $this->getOriginalValueByFieldName( 'unit_number_cache' ) ) {
			$arrstrOriginalValueChanges['unit_number_cache'] = $this->sqlUnitNumberCache();
			$strSql .= ' unit_number_cache = ' . $this->sqlUnitNumberCache() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' space_number = ' . $this->sqlSpaceNumber() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlSpaceNumber() ) != $this->getOriginalValueByFieldName( 'space_number' ) ) {
			$arrstrOriginalValueChanges['space_number'] = $this->sqlSpaceNumber();
			$strSql .= ' space_number = ' . $this->sqlSpaceNumber() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' display_number = ' . $this->sqlDisplayNumber() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlDisplayNumber() ) != $this->getOriginalValueByFieldName( 'display_number' ) ) {
			$arrstrOriginalValueChanges['display_number'] = $this->sqlDisplayNumber();
			$strSql .= ' display_number = ' . $this->sqlDisplayNumber() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' initial_rent = ' . $this->sqlInitialRent() . ',';
		} elseif( ( 0 != bccomp( CStrings::reverseSqlFormat( $this->sqlInitialRent() ), $this->getOriginalValueByFieldName( 'initial_rent' ), 2 ) ) ) {
			$arrstrOriginalValueChanges['initial_rent'] = $this->sqlInitialRent();
			$strSql .= ' initial_rent = ' . $this->sqlInitialRent() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' rate_adjustment_amount = ' . $this->sqlRateAdjustmentAmount() . ',';
		} elseif( ( 0 != bccomp( CStrings::reverseSqlFormat( $this->sqlRateAdjustmentAmount() ), $this->getOriginalValueByFieldName( 'rate_adjustment_amount' ), 2 ) ) ) {
			$arrstrOriginalValueChanges['rate_adjustment_amount'] = $this->sqlRateAdjustmentAmount();
			$strSql .= ' rate_adjustment_amount = ' . $this->sqlRateAdjustmentAmount() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' budgeted_base_rent = ' . $this->sqlBudgetedBaseRent() . ',';
		} elseif( ( 0 != bccomp( CStrings::reverseSqlFormat( $this->sqlBudgetedBaseRent() ), $this->getOriginalValueByFieldName( 'budgeted_base_rent' ), 2 ) ) ) {
			$arrstrOriginalValueChanges['budgeted_base_rent'] = $this->sqlBudgetedBaseRent();
			$strSql .= ' budgeted_base_rent = ' . $this->sqlBudgetedBaseRent() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' budgeted_amenity_rent = ' . $this->sqlBudgetedAmenityRent() . ',';
		} elseif( ( 0 != bccomp( CStrings::reverseSqlFormat( $this->sqlBudgetedAmenityRent() ), $this->getOriginalValueByFieldName( 'budgeted_amenity_rent' ), 2 ) ) ) {
			$arrstrOriginalValueChanges['budgeted_amenity_rent'] = $this->sqlBudgetedAmenityRent();
			$strSql .= ' budgeted_amenity_rent = ' . $this->sqlBudgetedAmenityRent() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' budgeted_rent = ' . $this->sqlBudgetedRent() . ',';
		} elseif( ( 0 != bccomp( CStrings::reverseSqlFormat( $this->sqlBudgetedRent() ), $this->getOriginalValueByFieldName( 'budgeted_rent' ), 2 ) ) ) {
			$arrstrOriginalValueChanges['budgeted_rent'] = $this->sqlBudgetedRent();
			$strSql .= ' budgeted_rent = ' . $this->sqlBudgetedRent() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' make_ready_notes = ' . $this->sqlMakeReadyNotes() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlMakeReadyNotes() ) != $this->getOriginalValueByFieldName( 'make_ready_notes' ) ) {
			$arrstrOriginalValueChanges['make_ready_notes'] = $this->sqlMakeReadyNotes();
			$strSql .= ' make_ready_notes = ' . $this->sqlMakeReadyNotes() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' reporting_start_month = ' . $this->sqlReportingStartMonth() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlReportingStartMonth() ) != $this->getOriginalValueByFieldName( 'reporting_start_month' ) ) {
			$arrstrOriginalValueChanges['reporting_start_month'] = $this->sqlReportingStartMonth();
			$strSql .= ' reporting_start_month = ' . $this->sqlReportingStartMonth() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlShowOnWebsite() ) != $this->getOriginalValueByFieldName( 'show_on_website' ) ) {
			$arrstrOriginalValueChanges['show_on_website'] = $this->sqlShowOnWebsite();
			$strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_marketed = ' . $this->sqlIsMarketed() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsMarketed() ) != $this->getOriginalValueByFieldName( 'is_marketed' ) ) {
			$arrstrOriginalValueChanges['is_marketed'] = $this->sqlIsMarketed();
			$strSql .= ' is_marketed = ' . $this->sqlIsMarketed() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_pricing_synced = ' . $this->sqlIsPricingSynced() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsPricingSynced() ) != $this->getOriginalValueByFieldName( 'is_pricing_synced' ) ) {
			$arrstrOriginalValueChanges['is_pricing_synced'] = $this->sqlIsPricingSynced();
			$strSql .= ' is_pricing_synced = ' . $this->sqlIsPricingSynced() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' order_num = ' . $this->sqlOrderNum() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName( 'order_num' ) ) {
			$arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum();
			$strSql .= ' order_num = ' . $this->sqlOrderNum() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' details = ' . $this->sqlDetails() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlDetails() ) != $this->getOriginalValueByFieldName( 'details' ) ) {
			$arrstrOriginalValueChanges['details'] = $this->sqlDetails();
			$strSql .= ' details = ' . $this->sqlDetails() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' application_lease_id = ' . $this->sqlApplicationLeaseId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlApplicationLeaseId() ) != $this->getOriginalValueByFieldName( 'application_lease_id' ) ) {
			$arrstrOriginalValueChanges['application_lease_id'] = $this->sqlApplicationLeaseId();
			$strSql .= ' application_lease_id = ' . $this->sqlApplicationLeaseId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' hold_lease_id = ' . $this->sqlHoldLeaseId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlHoldLeaseId() ) != $this->getOriginalValueByFieldName( 'hold_lease_id' ) ) {
			$arrstrOriginalValueChanges['hold_lease_id'] = $this->sqlHoldLeaseId();
			$strSql .= ' hold_lease_id = ' . $this->sqlHoldLeaseId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' reserve_until = ' . $this->sqlReserveUntil() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlReserveUntil() ) != $this->getOriginalValueByFieldName( 'reserve_until' ) ) {
			$arrstrOriginalValueChanges['reserve_until'] = $this->sqlReserveUntil();
			$strSql .= ' reserve_until = ' . $this->sqlReserveUntil() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' available_on = ' . $this->sqlAvailableOn() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlAvailableOn() ) != $this->getOriginalValueByFieldName( 'available_on' ) ) {
			$arrstrOriginalValueChanges['available_on'] = $this->sqlAvailableOn();
			$strSql .= ' available_on = ' . $this->sqlAvailableOn() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' make_ready_date = ' . $this->sqlMakeReadyDate() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlMakeReadyDate() ) != $this->getOriginalValueByFieldName( 'make_ready_date' ) ) {
			$arrstrOriginalValueChanges['make_ready_date'] = $this->sqlMakeReadyDate();
			$strSql .= ' make_ready_date = ' . $this->sqlMakeReadyDate() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' imported_on = ' . $this->sqlImportedOn() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlImportedOn() ) != $this->getOriginalValueByFieldName( 'imported_on' ) ) {
			$arrstrOriginalValueChanges['imported_on'] = $this->sqlImportedOn();
			$strSql .= ' imported_on = ' . $this->sqlImportedOn() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' vacated_on = ' . $this->sqlVacatedOn() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlVacatedOn() ) != $this->getOriginalValueByFieldName( 'vacated_on' ) ) {
			$arrstrOriginalValueChanges['vacated_on'] = $this->sqlVacatedOn();
			$strSql .= ' vacated_on = ' . $this->sqlVacatedOn() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlDeletedBy() ) != $this->getOriginalValueByFieldName( 'deleted_by' ) ) {
			$arrstrOriginalValueChanges['deleted_by'] = $this->sqlDeletedBy();
			$strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlDeletedOn() ) != $this->getOriginalValueByFieldName( 'deleted_on' ) ) {
			$arrstrOriginalValueChanges['deleted_on'] = $this->sqlDeletedOn();
			$strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' late_fee_formula_id = ' . $this->sqlLateFeeFormulaId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlLateFeeFormulaId() ) != $this->getOriginalValueByFieldName( 'late_fee_formula_id' ) ) {
			$arrstrOriginalValueChanges['late_fee_formula_id'] = $this->sqlLateFeeFormulaId();
			$strSql .= ' late_fee_formula_id = ' . $this->sqlLateFeeFormulaId() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_reportable = ' . $this->sqlIsReportable() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsReportable() ) != $this->getOriginalValueByFieldName( 'is_reportable' ) ) {
			$arrstrOriginalValueChanges['is_reportable'] = $this->sqlIsReportable();
			$strSql .= ' is_reportable = ' . $this->sqlIsReportable() . ',';
			$boolUpdate = true;
		}

		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' unit_space_log_type_id = ' . $this->sqlUnitSpaceLogTypeId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlUnitSpaceLogTypeId() ) != $this->getOriginalValueByFieldName( 'unit_space_log_type_id' ) ) {
			$arrstrOriginalValueChanges['unit_space_log_type_id'] = $this->sqlUnitSpaceLogTypeId();
			$strSql .= ' unit_space_log_type_id = ' . $this->sqlUnitSpaceLogTypeId() . ',';
			$boolUpdate = true;
		}

		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$arrobjUnitSpaceExclusion = \Psi\Eos\Entrata\CUnitSpaceExclusions::createService()->fetchUnitSpaceExclusionsByUnitExclusionReasonTypeIdByPropertyIdsByCid( $this->getUnitExclusionReasonTypeId(), [ $this->getPropertyId() ], $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjUnitSpaceExclusion ) ) {
			$objUnitSpaceExclusion = current( $arrobjUnitSpaceExclusion );
		}

		if( $this->getUnitExclusionReasonTypeId() == CUnitExclusionReasonType::NOT_EXCLUDED || ( true == valObj( $objUnitSpaceExclusion, 'CUnitSpaceExclusion' ) && true == $objUnitSpaceExclusion->getIsReportable() ) ) {
			$this->setIsReportable( true );
		} else {
			$this->setIsReportable( false );
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true, $boolReturnSqlOnly = false ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public static function reassociateLateFeeFormulaToUnitSpaces( $intCurrentUserId, $intLateFeeFormulaId, $arrintUnitSpaceId, $intCid, $objDatabase ) {
		$strSql = 'UPDATE unit_spaces
					SET
						late_fee_formula_id = ' . ( int ) $intLateFeeFormulaId . ',
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = \'NOW()\'
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintUnitSpaceId ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>