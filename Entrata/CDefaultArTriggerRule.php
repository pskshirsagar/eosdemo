<?php

class CDefaultArTriggerRule extends CBaseDefaultArTriggerRule {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArTriggerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCacheToLease() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>