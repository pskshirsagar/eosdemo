<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningConditionOperandTypes
 * Do not add any new functions to this class.
 */

class CScreeningConditionOperandTypes extends CBaseScreeningConditionOperandTypes {

	public static function fetchScreeningConditionOperandTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CScreeningConditionOperandType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScreeningConditionOperandType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CScreeningConditionOperandType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>