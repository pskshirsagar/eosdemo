<?php

class CPropertyOccupation extends CBasePropertyOccupation {

	protected $m_strOccupationName;

    /**
     * get Functions
     */

	public function getOccupationName() {
		 return $this->m_strOccupationName;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        }

        return $boolIsValid;
    }

    /**
     * set Functions
     */

 	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['occupation_name'] ) ) $this->setOccupationName( $arrValues['occupation_name'] );
        return;
    }

	public function setOccupationName( $strOccupationName ) {
		$this->m_strOccupationName = $strOccupationName;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        }

        return $boolIsValid;
    }

    public function valOccupationId( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getOccupationId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupation_id', '' ) );
        }

        if( true == isset( $objDatabase ) ) {

        	$objPropertyOccupation = CPropertyOccupations::fetchPropertyOccupationByPropertyIdByOccupationIdByCid( $this->getPropertyId(), $this->getOccupationId(), $this->getCid(), $objDatabase );

        	if( true == valObj( $objPropertyOccupation, 'CPropertyOccupation' ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_occupation', 'Occupation' . ' ' . $objPropertyOccupation->getSystemCode() . ' is already associated with the property.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valOccupationId( $objDatabase );
            	$boolIsValid &= $this->valCid();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>