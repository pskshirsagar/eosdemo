<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporates
 * Do not add any new functions to this class.
 */
class CCorporates extends CBaseCorporates {

	public static function fetchCorporatesCountByCorporatesFilter( $objCorporatesFilter, $objDatabase ) {

		if( false == valObj( $objCorporatesFilter, 'CCorporatesFilter' ) ) return NULL;

		$strSql = 'SELECT
						count( DISTINCT( ct.id ) )
					FROM
						corporates ct
						JOIN customers c ON ( ct.customer_id = c.id AND ct.cid = c.cid )
						LEFT JOIN screening_application_requests sar ON ( sar.application_id = ct.id AND sar.cid = ct.cid )
					WHERE
						1 = 1 ';

		$strSql .= self::buildCommonCorporateSearchSql( $objCorporatesFilter );

		$arrintCount = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintCount ) ) {
			return $arrintCount[0]['count'];
		}

		return 0;
	}

	public static function fetchCorporatesByCorporatesFilter( $objCorporatesFilter, $objDatabase ) {

		if( false == valObj( $objCorporatesFilter, 'CCorporatesFilter' ) ) return NULL;

		$objPagination = $objCorporatesFilter->getPagination();

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {

			$intPageNo  	= $objPagination->getPageNo();
			$intPageSize	= $objPagination->getPageSize();

			if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
				$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
				$intLimit  = ( int ) $intPageSize;
			}
		}

		$strSql = 'SELECT
						DISTINCT( ct.id),
						ct.*,
						sar.screening_decision_type_id,
						sar.id as screening_application_request_id,
						sar.remote_screening_id
					FROM
						corporates ct
						JOIN customers c ON ( ct.customer_id = c.id AND ct.cid = c.cid )
						LEFT JOIN screening_application_requests sar ON ( sar.application_id = ct.id AND sar.cid = ct.cid )
					WHERE
						1 = 1 ';

		$strSql .= self::buildCommonCorporateSearchSql( $objCorporatesFilter );

		if( false == is_null( $objCorporatesFilter->getSortBy() ) && false == is_null( $objCorporatesFilter->getSortOrder() ) ) {
			if( 'sar.screening_decision_type_id' == $objCorporatesFilter->getSortBy() ) {
				$strSortDetails = ' ORDER BY ' . $objCorporatesFilter->getSortBy() . ' ' . $objCorporatesFilter->getSortOrder() . ', sar.remote_screening_id ASC';
			} else {
				$strSortDetails = ' ORDER BY ' . $objCorporatesFilter->getSortBy() . ' ' . $objCorporatesFilter->getSortOrder();
			}

			$strSql .= $strSortDetails;
		} else {
			$strSql .= ' ORDER BY
						ct.id DESC';
		}

		if( true == isset( $intOffset ) && 0 < $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( true == isset( $intLimit ) && false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT ' . $intLimit;
		}

		return self::fetchCorporates( $strSql, $objDatabase );
	}

	public static function buildCommonCorporateSearchSql( $objCorporatesFilter ) {

		if( false == valObj( $objCorporatesFilter, 'CCorporatesFilter' ) ) {
			return;
		}

		$strSql = '';

		$arrstrAndSearchParameters = [];

		$arrstrAndSearchParameters[] = 'ct.is_archive = ' . ( int ) $objCorporatesFilter->getIsArchive();

		if( true == is_numeric( $objCorporatesFilter->getCid() ) ) $arrstrAndSearchParameters[] = 'ct.cid = ' . $objCorporatesFilter->getCid() . '';
		if( true == valStr( $objCorporatesFilter->getBusinessName() ) ) $arrstrAndSearchParameters[] = 'LOWER( ct.business_name ) LIKE LOWER( \'%' . str_replace( "'", "\\'", $objCorporatesFilter->getBusinessName() ) . '%\' )';

		$strAppendPendingCondition = '';

		if( true == valArr( $objCorporatesFilter->getScreeningDecisionTypeIds() ) && true == in_array( CScreeningDecisionType::PENDING_UNKNOWN, $objCorporatesFilter->getScreeningDecisionTypeIds() ) ) {
			$strAppendPendingCondition = ' OR ( sar.id IS NOT NULL AND sar.remote_screening_id IS NOT NULL  AND sar.screening_decision_type_id IS NULL ) ';
		}

		if( true == valArr( $objCorporatesFilter->getScreeningDecisionTypeIds() ) ) {

			if( false == $objCorporatesFilter->getIsNewBusinessApplicants() ) {
				$arrstrAndSearchParameters[] = '( sar.screening_decision_type_id IN ( ' . implode( ',', $objCorporatesFilter->getScreeningDecisionTypeIds() ) . ' ) ' . $strAppendPendingCondition . ' )';
			} else {
				$arrstrAndSearchParameters[] = '( sar.screening_decision_type_id IN ( ' . implode( ',', $objCorporatesFilter->getScreeningDecisionTypeIds() ) . ' ) ' . $strAppendPendingCondition . ' OR ( sar.id IS NULL OR (  sar.id IS NOT NULL AND sar.remote_screening_id IS NULL AND sar.screening_decision_type_id IS NULL ) ) )';
			}

		} elseif( true == $objCorporatesFilter->getIsNewBusinessApplicants() ) {
			$arrstrAndSearchParameters[] = '( sar.id IS NULL OR (  sar.id IS NOT NULL AND sar.remote_screening_id IS NULL AND sar.screening_decision_type_id IS NULL ) )';
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) {
			$strSql = ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );
		}

		return $strSql;
	}

	public static function fetchCorporateByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						corporates
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchCorporate( $strSql, $objDatabase );
	}

}
?>