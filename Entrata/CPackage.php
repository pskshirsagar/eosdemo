<?php

class CPackage extends CBasePackage {

    protected $m_objCustomer;
    protected $m_objProperty;

	protected $m_boolShippingVendorIsIcon;
	protected $m_boolCustomShippingVendor;

    protected $m_intLeaseCustomerId;
    protected $m_intPackageCount;
    protected $m_intLeaseCid;
    protected $m_intShippingVendorPropertyId;
	protected $m_intBuildingOrder;

    protected $m_strCustomerEmailAddress;
	protected $m_strCustomerNameFirst;
	protected $m_strCustomerNameLast;
	protected $m_strUnitNumber;
	protected $m_strSpaceNumber;
	protected $m_strCustomerName;
	protected $m_strPackageName;
	protected $m_strPropertyName;
	protected $m_strBuildingName;
	protected $m_strBuildingId;
	protected $m_strUserNameFirst;
	protected $m_strUserNameLast;
	protected $m_strSignatureFilePath;
	protected $m_strSignatureFileName;
	protected $m_strCustomerUserName;
	protected $m_strShippingVendorColor;
	protected $m_strLeaseStatusTypeId;
	protected $m_strCustomerAddress;
	protected $m_strFullsizeUri;

	/**
	 * Get Functions
	 */

	public function getCustomerEmailAddress() {
		return $this->m_strCustomerEmailAddress;
	}

	public function getCustomerNameFirst() {
		return $this->m_strCustomerNameFirst;
	}

	public function getCustomerNameLast() {
		return $this->m_strCustomerNameLast;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getSpaceNumber() {
		return $this->m_strSpaceNumber;
	}

	public function getCustomerName() {
		return $this->m_strCustomerName;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getPackageName() {
		return $this->m_strPackageName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getBuildingId() {
		return $this->m_strBuildingId;
	}

	public function getUserNameFirst() {
		return $this->m_strUserNameFirst;
	}

	public function getUserNameLast() {
		return $this->m_strUserNameLast;
	}

	public function getPackageCount() {
		return $this->m_intPackageCount;
	}

	public function getSignatureFilePath() {
		return $this->m_strSignatureFilePath;
	}

	public function getSignatureFileName() {
		return $this->m_strSignatureFileName;
	}

	public function getCustomerUserName() {
		return $this->m_strCustomerUserName;
	}

    public function getOrFetchCustomer( $objDatabase ) {
        if( false == isset( $this->m_objCustomer ) ) {
            $this->m_objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
        }

        return $this->m_objCustomer;
    }

    public function getLeaseCid() {
    	return $this->m_intLeaseCid;
    }

    public function getOrFetchProperty( $objDatabase ) {
        if( false == isset( $this->m_objProperty ) ) {
            $this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
        }

        return $this->m_objProperty;
    }

    public function getPackageCustomerAddress() {
    	return $this->m_strCustomerAddress;
    }

    public function getShippingVendorPropertyId() {
    	return $this->m_intShippingVendorPropertyId;
    }

	public function getBuildingOrder() {
		return $this->m_intBuildingOrder;
	}

	public function getCustomShippingVendor() {
		return $this->m_boolCustomShippingVendor;
	}

    public function getShippingVendorColor() {
    	return $this->m_strShippingVendorColor;
    }

	public function getLeaseStatusTypeId() {
		return $this->m_strLeaseStatusTypeId;
	}

	public function getShippingVendorIsIcon() {
		return $this->m_boolShippingVendorIsIcon;
	}

	public function getFullsizeUri() {
		return $this->m_strFullsizeUri;
	}

	/**
	 * Set Functions
	 */

	public function setCustomerEmailAddress( $strCustomerEmailAddress ) {
		$this->m_strCustomerEmailAddress = $strCustomerEmailAddress;
	}

	public function setCustomerNameFirst( $strCustomerNameFirst ) {
		return $this->m_strCustomerNameFirst = $strCustomerNameFirst;
	}

	public function setCustomerNameLast( $strCustomerNameLast ) {
		return $this->m_strCustomerNameLast = $strCustomerNameLast;
	}

	public function setUnitNumber( $strUnitNumber ) {
		return $this->m_strUnitNumber = $strUnitNumber;
	}

	public function setSpaceNumber( $strSpaceNumber ) {
		return $this->m_strSpaceNumber = $strSpaceNumber;
	}

	public function setCustomerName( $strCustomerName ) {
		$this->m_strCustomerName = CStrings::strTrimDef( $strCustomerName, -1, NULL, true );
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = CStrings::strTrimDef( $intLeaseCustomerId, -1, NULL, true );
	}

	public function setPackageName( $strPackageName ) {
		$this->m_strPackageName = CStrings::strTrimDef( $strPackageName, -1, NULL, true );
	}

	public function setPropertyName( $strPropertyName ) {
		return $this->m_strPropertyName = $strPropertyName;
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setBuildingId( $strBuildingId ) {
		$this->m_strBuildingId = $strBuildingId;
	}

    public function setCustomer( $objCustomer ) {
        $this->m_objCustomer = $objCustomer;
    }

    public function setProperty( $objProperty ) {
        $this->m_objProperty = $objProperty;
    }

    public function setUserNameFirst( $strUserNameFirst ) {
        $this->m_strUserNameFirst = $strUserNameFirst;
    }

    public function setUserNameLast( $strUserNameLast ) {
    	$this->m_strUserNameLast = $strUserNameLast;
    }

	public function setPackageCount( $intPackageCount ) {
    	$this->m_intPackageCount = $intPackageCount;
    }

    public function setSignatureFilePath( $strSignatureFilePath ) {
    	$this->m_strSignatureFilePath = $strSignatureFilePath;
    }

    public function setSignatureFileName( $strSignatureFileName ) {
    	$this->m_strSignatureFileName = $strSignatureFileName;
    }

    public function setCustomerUserName( $strCustomerUserName ) {
    	$this->m_strCustomerUserName = $strCustomerUserName;
    }

    public function setLeaseCid( $intLeaseCid ) {
    	$this->m_intLeaseCid = $intLeaseCid;
    }

    public function setPackageCustomerAddress( $strCustomerAddress ) {
    	$this->m_strCustomerAddress = $strCustomerAddress;
    }

	public function setBuildingOrder( $intBuildingOrder ) {
		$this->m_intBuildingOrder = $intBuildingOrder;
	}

    public function setShippingVendorPropertyId( $intShippingVendorPropertyId ) {
    	$this->m_intShippingVendorPropertyId = $intShippingVendorPropertyId;
    }

	public function setCustomShippingVendor( $boolShippingVendor ) {
		$this->m_boolCustomShippingVendor = $boolShippingVendor;
	}

    public function setShippingVendorColor( $strShippingVendorColor ) {
    	$this->m_strShippingVendorColor = $strShippingVendorColor;
    }

	public function setShippingVendorIsIcon( $boolShippingVendorIsIcon ) {
		$this->m_boolShippingVendorIsIcon = CStrings::strToBool( $boolShippingVendorIsIcon );
	}

	public function setFullsizeUri( $strFullsizeUri ) {
		$this->m_strFullsizeUri = CStrings::strTrimDef( $strFullsizeUri, NULL, NULL, true );
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_strLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrstrValues );

		if( isset( $arrstrValues['customer_email_address'] ) && $boolDirectSet )
			$this->m_strCustomerEmailAddress = trim( $arrstrValues['customer_email_address'] );
		elseif ( isset( $arrstrValues['customer_email_address'] ) )
			$this->setCustomerEmailAddress( $arrstrValues['customer_email_address'] );
		if( isset( $arrstrValues['name_first'] ) && $boolDirectSet )
			$this->m_strCustomerNameFirst = trim( $arrstrValues['name_first'] );
		elseif ( isset( $arrstrValues['name_first'] ) )
			$this->setCustomerNameFirst( $arrstrValues['name_first'] );
		if( isset( $arrstrValues['name_last'] ) && $boolDirectSet ) $this->m_strCustomerNameLast = trim( $arrstrValues['name_last'] );
		elseif ( isset( $arrstrValues['name_last'] ) )
			$this->setCustomerNameLast( $arrstrValues['name_last'] );
		if( isset( $arrstrValues['unit_number'] ) && $boolDirectSet )
			$this->m_strUnitNumber = trim( $arrstrValues['unit_number'] );
		elseif ( isset( $arrstrValues['unit_number'] ) )
			$this->setUnitNumber( $arrstrValues['unit_number'] );
		if( isset( $arrstrValues['space_number'] ) && $boolDirectSet )
			$this->m_strSpaceNumber = trim( $arrstrValues['space_number'] );
		elseif ( isset( $arrstrValues['space_number'] ) )
			$this->setSpaceNumber( $arrstrValues['space_number'] );
		if( isset( $arrstrValues['customer_name'] ) && $boolDirectSet )
			$this->m_strCustomerName = trim( $arrstrValues['customer_name'] );
		elseif ( isset( $arrstrValues['customer_name'] ) )
			$this->setCustomerName( $arrstrValues['customer_name'] );
		if( isset( $arrstrValues['lease_customer_id'] ) && $boolDirectSet )
			$this->m_intLeaseCustomerId = trim( $arrstrValues['lease_customer_id'] );
		elseif ( isset( $arrstrValues['lease_customer_id'] ) )
			$this->setLeaseCustomerId( $arrstrValues['lease_customer_id'] );
		if( isset( $arrstrValues['package_name'] ) && $boolDirectSet )
			$this->m_strPackageName = trim( $arrstrValues['package_name'] );
		elseif ( isset( $arrstrValues['package_name'] ) )
			$this->setPackageName( $arrstrValues['package_name'] );
		if( isset( $arrstrValues['property_name'] ) && $boolDirectSet )
			$this->m_strPropertyName = trim( $arrstrValues['property_name'] );
		elseif ( isset( $arrstrValues['property_name'] ) )
			$this->setPropertyName( $arrstrValues['property_name'] );
		if( isset( $arrstrValues['building_name'] ) && $boolDirectSet )
			$this->m_strBuildingName = trim( $arrstrValues['building_name'] );
		elseif ( isset( $arrstrValues['building_name'] ) ) $this->setBuildingName( $arrstrValues['building_name'] );
		if( isset( $arrstrValues['building_id'] ) && $boolDirectSet )
			$this->m_strBuildingId = trim( $arrstrValues['building_id'] );
		elseif ( isset( $arrstrValues['building_id'] ) ) $this->setBuildingId( $arrstrValues['building_id'] );
		if( isset( $arrstrValues['user_name_first'] ) && $boolDirectSet )
			$this->m_strUserNameFirst = trim( $arrstrValues['user_name_first'] );
		elseif ( isset( $arrstrValues['user_name_first'] ) ) $this->setUserNameFirst( $arrstrValues['user_name_first'] );
		if( isset( $arrstrValues['user_name_last'] ) && $boolDirectSet )
			$this->m_strUserNameLast = trim( $arrstrValues['user_name_last'] );
		elseif ( isset( $arrstrValues['user_name_last'] ) ) $this->setUserNameLast( $arrstrValues['user_name_last'] );
		if( isset( $arrstrValues['package_count'] ) && $boolDirectSet )
			$this->m_intPackageCount = trim( $arrstrValues['package_count'] );
		elseif ( isset( $arrstrValues['package_count'] ) ) $this->setPackageCount( $arrstrValues['package_count'] );
		if( isset( $arrstrValues['signature_file_path'] ) && $boolDirectSet )
			$this->m_strSignatureFilePath = trim( $arrstrValues['signature_file_path'] );
		elseif ( isset( $arrstrValues['signature_file_path'] ) ) $this->setSignatureFilePath( $arrstrValues['signature_file_path'] );
		if( isset( $arrstrValues['signature_file_name'] ) && $boolDirectSet )
			$this->m_strSignatureFileName = trim( $arrstrValues['signature_file_name'] );
		elseif ( isset( $arrstrValues['signature_file_name'] ) )
			$this->setSignatureFileName( $arrstrValues['signature_file_name'] );
		if( isset( $arrstrValues['customer_username'] ) && $boolDirectSet )
			$this->m_strCustomerUserName = trim( $arrstrValues['customer_username'] );
		elseif ( isset( $arrstrValues['customer_username'] ) ) $this->setCustomerUserName( $arrstrValues['customer_username'] );
		if( isset( $arrstrValues['lease_cid'] ) && $boolDirectSet )
			$this->m_intLeaseCid = trim( $arrstrValues['lease_cid'] );
		elseif ( isset( $arrstrValues['lease_cid'] ) )
			$this->setLeaseCid( $arrstrValues['lease_cid'] );
		elseif ( isset( $arrstrValues['package_customer_address'] ) )
			$this->setPackageCustomerAddress( $arrstrValues['package_customer_address'] );
		if ( isset( $arrstrValues['shipping_vendor_property_id'] ) )
			$this->setShippingVendorPropertyId( $arrstrValues['shipping_vendor_property_id'] );
		if ( isset( $arrstrValues['custom_vendor'] ) )
			$this->setCustomShippingVendor( $arrstrValues['custom_vendor'] );
		if ( isset( $arrstrValues['shipping_vendor_color'] ) )
			$this->setShippingVendorColor( $arrstrValues['shipping_vendor_color'] );
		if ( isset( $arrstrValues['lease_status_type_id'] ) )
			$this->setLeaseStatusTypeId( $arrstrValues['lease_status_type_id'] );
		if ( isset( $arrstrValues['shipping_vendor_is_icon'] ) )
			$this->setShippingVendorIsIcon( $arrstrValues['shipping_vendor_is_icon'] );
		if ( isset( $arrstrValues['fullsize_uri'] ) )
			$this->setFullsizeUri( $arrstrValues['fullsize_uri'] );
		if  ( isset( $arrstrValues['building_order'] ) )
			$this->setBuildingOrder( $arrstrValues['building_order'] );
	}

	public function setIncrementNotificationCount() {
		$intEmailCount = $this->getEmailCount();

		if( false == is_null( $intEmailCount ) && 0 < $intEmailCount ) {
			$intEmailCount = $intEmailCount + 1;
		} else {
			$intEmailCount = 1;
		}

		$this->setEmailCount( $intEmailCount );
		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPackageTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPackageBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSignatureFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', 'Customer is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPackageDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBarcode() {
		$boolIsValid = true;

		if( false == is_null( $this->getBarcode() ) && false == is_numeric( $this->getBarcode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'barcode', 'Tracking number is not valid.' ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSignature() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLobbyDisplayVisible() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPerishable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessagedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCalledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrintedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceivedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeliveredBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeliveredOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateDuplicatePackage( $objDataBase ) {
		$boolIsValid = true;
		$intPackageCount = CPackages::fetchPackageCountWithCustomerByCustomerIdByTrackingNumberByPackageTypeIdByCid( $this->getCustomerId(), $this->getBarcode(), $this->getPackageTypeId(), $this->getCid(), $objDataBase );

		if( 0 < $intPackageCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'The tracking number entered is already in use. Please enter a new tracking number. (Tracking Number is not required)' ) ) );
		}
		return $boolIsValid;
	}

	public function validateCustomerToEmailAddress( $objDataBase ) {
		$boolIsValid = true;
		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerDetailsByCustomerIdByCid( $this->getCustomerId(), $this->getCid(), $objDataBase );

		if( true == valObj( $objCustomer, 'CCustomer' ) && ( true == valStr( $objCustomer->getUsername() ) || true == valStr( $objCustomer->getEmailAddress() ) ) && false == ( CValidation::validateEmailAddresses( true == valStr( $objCustomer->getUsername() ) ? $objCustomer->getUsername() : $objCustomer->getEmailAddress() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_email_address', 'To email address is not a valid email address.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				// $boolIsValid &= $this->valCustomerId();
				// $boolIsValid &= $this->valBarcode();
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function buildDefaultSignatureFilePath() {
		return 'parcelalert/' . date( 'Y' ) . '/' . date( 'n' ) . '/' . date( 'j' ) . '/';
	}

	public function buildDefaultSignatureFullFilePath() {
		return getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->buildDefaultSignatureFilePath();
	}

	public function fetchPackageType( $intPackageTypeId, $objDataBase ) {
		return CPackageTypes::fetchPackageTypeById( $intPackageTypeId, $objDataBase );
	}

	public function fetchSignatureFilePath( $intFileId, $objDataBase ) {
		return CFiles::fetchFileByIdByCid( $intFileId, $this->getCid(), $objDataBase );
	}

}
?>