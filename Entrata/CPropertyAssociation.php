<?php

class CPropertyAssociation extends CBasePropertyAssociation {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArCascadeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArCascadeReferenceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArOriginId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArOriginCategoryId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArOriginGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArOriginReferenceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyMediaFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExternalVideoUrl() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHideDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPetRestrictions() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPetWeightLimit() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPetMaxAllowed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAverageAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsFeatured() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsOptional() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsResponsible() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsIncludedInRent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHideRates() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImportedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMappingId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>