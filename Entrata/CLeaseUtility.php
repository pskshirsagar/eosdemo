<?php

class CLeaseUtility extends CBaseLeaseUtility {

	protected $m_strUtilityName;
	protected $m_strUtilityDescription;
	protected $m_strUtilityTypeName;

    /**
     * Get functions
     */

	public function getUtilityName() {
		return $this->m_strUtilityName;
	}

	public function getUtilityDescription() {
		return $this->m_strUtilityDescription;
	}

	public function getUtilityTypeName() {
		return $this->m_strUtilityTypeName;
	}

    /**
     * Set functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

    	if( true == isset( $arrmixValues['utility_name'] ) ) 		$this->setUtilityName( $arrmixValues['utility_name'] );
    	if( true == isset( $arrmixValues['utility_description'] ) ) $this->setUtilityDescription( $arrmixValues['utility_description'] );
    	if( true == isset( $arrmixValues['utility_type_name'] ) ) 	$this->setUtilityTypeName( $arrmixValues['utility_type_name'] );
    }

	public function setUtilityName( $strUtilityName ) {
		$this->m_strUtilityName = $strUtilityName;
	}

	public function setUtilityDescription( $strUtilityDescription ) {
		$this->m_strUtilityDescription = $strUtilityDescription;
	}

	public function setUtilityTypeName( $strUtilityTypeName ) {
		$this->m_strUtilityTypeName = $strUtilityTypeName;
	}

    /**
     * Validate functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseIntervalId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUtilityId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUtilityFormulaId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsResponsible() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
        if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
            return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
        } else {
            return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
        }
    }

}
?>