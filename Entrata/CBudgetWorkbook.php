<?php

class CBudgetWorkbook extends CBaseBudgetWorkbook {

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_workbook_name', __( 'Budget Workbook Name is required.' ) ) );
		} else {
			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND name ILIKE \'' . addslashes( $this->getName() ) . '\'
							AND id <>' . ( int ) $this->getId();

			if( 0 < Psi\Eos\Entrata\CBudgetWorkbooks::createService()->fetchBudgetWorkbookCount( $strWhere, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_workbook_name', __( 'Budget Workbook name already exists.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function createBudgetWorkbookCompanyUser() {
		$objBudgetWorkbookCompanyUser = new CBudgetWorkbookCompanyUser();
		$objBudgetWorkbookCompanyUser->setCid( $this->getCid() );
		$objBudgetWorkbookCompanyUser->setBudgetWorkbookId( $this->getId() );

		return $objBudgetWorkbookCompanyUser;
	}

	public function createBudgetWorkbookPropertyGroup() {
		$objBudgetWorkbookPropertyGroup = new CBudgetWorkbookPropertyGroup();
		$objBudgetWorkbookPropertyGroup->setCid( $this->getCid() );
		$objBudgetWorkbookPropertyGroup->setBudgetWorkbookId( $this->getId() );

		return $objBudgetWorkbookPropertyGroup;
	}

	public function createBudgetWorkbookAssumption() {
		$objBudgetWorkbookAssumption = new CBudgetWorkbookAssumption();
		$objBudgetWorkbookAssumption->setCid( $this->getCid() );
		$objBudgetWorkbookAssumption->setBudgetWorkbookId( $this->getId() );

		return $objBudgetWorkbookAssumption;
	}

}
?>