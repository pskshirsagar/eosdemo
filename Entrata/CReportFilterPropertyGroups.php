<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportFilterPropertyGroups
 * Do not add any new functions to this class.
 */

class CReportFilterPropertyGroups extends CBaseReportFilterPropertyGroups {

	/**
	 * @param $arrintPropertyGroupIds
	 * @param $intFilterId
	 * @param $intCid
	 * @param $objDatabase
	 * @return property ids ARRAY|null
	 */
	public static function fetchPropertyGroupsByReportFilterIdByCompanyUserIdByCid( $arrintPropertyGroupIds, $intFilterId, $intCid, $objDatabase ) {
		if( false == isset( $intFilterId ) ) return NULL;
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = '
			SELECT
				pga.property_id
			FROM
				property_groups pg
				JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL )
				JOIN report_filter_property_groups rfpg ON ( pg.cid = rfpg.cid AND ( rfpg.property_group_id = pga.property_id OR rfpg.property_group_id = pga.property_group_id ) )
				JOIN property_group_associations property ON ( property.cid = pg.cid AND property.property_id = pga.property_id )
			WHERE
				pg.CID = ' . ( int ) $intCid . '
				AND rfpg.report_filter_id = ' . ( int ) $intFilterId . '
				AND ( property.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ) OR property.property_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ))';

		return fetchData( $strSql, $objDatabase );
	}

}
?>