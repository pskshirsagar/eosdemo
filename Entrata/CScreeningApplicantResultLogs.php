<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningApplicantResultLogs
 * Do not add any new functions to this class.
 */

class CScreeningApplicantResultLogs extends CBaseScreeningApplicantResultLogs {

    public static function fetchScreeningApplicantResultLog( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, CScreeningApplicantResultLog::class, $objDatabase );
    }

    public static function fetchScreeningApplicantResultLogsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

        if( false == valId( $intCid ) || false == valId( $intApplicationId ) )  return NULL;

        $strSql = 'SELECT
						sarl.*,
                        sar.application_id,
                        sar.applicant_id,
                        a.name_first,
                        a.name_last,
                        a.customer_id,
                        ct.name AS customer_type,
                        srt.id AS recommendation_type_id,
                        srt.name AS recommendation_type_name
					FROM		
						screening_applicant_result_logs sarl
                        JOIN screening_applicant_results sar ON ( sar.id = sarl.screening_applicant_result_id AND sar.cid = sarl.cid )
                        JOIN screening_recommendation_types srt ON ( srt.id = sarl.screening_recommendation_type_id )
                        JOIN applicants a ON ( a.id = sar.applicant_id AND a.cid = sar.cid  )
                        JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid AND aa.application_id = sar.application_id )
                        JOIN customer_types ct ON ( ct.id = aa.customer_type_id )
					WHERE
						sarl.cid = ' . ( int ) $intCid . '
						AND sar.application_id = ' . ( int ) $intApplicationId . '
					ORDER BY 
					    sarl.created_on DESC';

        return $arrmixResult = fetchData( $strSql, $objDatabase );
    }

}
?>