<?php

class CCustomerClassifiedFavorite extends CBaseCustomerClassifiedFavorite {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerClassifiedId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function fetchDeletedCustomerClassfiedFavorite( $intCustomerClassifiedId, $intCustomerId, $intCid, $objEntrataDatabase ) {
		$strSelectClause = 'SELECT * FROM customer_classified_favorites WHERE customer_classified_id = \'' . $intCustomerClassifiedId . '\' AND cid = \'' . $intCid . '\' AND customer_id = \'' . $intCustomerId . '\' AND deleted_on IS NOT NULL AND deleted_by IS NOT NULL';
		return CCustomerClassifiedFavorites::fetchCustomerClassifiedFavorite( $strSelectClause, $objEntrataDatabase );
	}

	public static function fetchCustomCustomerClassifiedFavoriteByCustomerClassifiedIdByCidByCustomerId( $intCustomerClassifiedId, $intCustomerId, $intCid, $objEntrataDatabase ) {
		$strSelectClause = 'SELECT * FROM customer_classified_favorites WHERE customer_classified_id = \'' . $intCustomerClassifiedId . '\' AND cid = \'' . $intCid . '\' AND customer_id = \'' . $intCustomerId . '\' AND deleted_on IS NULL AND deleted_by IS NULL';
		return CCustomerClassifiedFavorites::fetchCustomerClassifiedFavorite( $strSelectClause, $objEntrataDatabase );
	}

}
?>