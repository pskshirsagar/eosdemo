<?php

class CApLegalEntity extends CBaseApLegalEntity {

	protected $m_strPayeeName;
	protected $m_intDefaultOwnerTypeId;
	protected $m_boolIsTaxPayerIdChanged;
	protected $m_intApPayeeLocationId;
	protected $m_intApLegalEntityCount;
	protected $m_intApPayeeStatusTypeId;

	const DEFAULT_FORM_1099_TYPE_ID 	= 8;
	const DEFAULT_FORM_1099_BOX_TYPE_ID = 89;

	protected $m_arrobjApPayee1099Adjustments;
	protected $m_arrmixApPayeeLegalEntity;

	public function setApPayee1099Adjustments( $arrobjApPayee1099Adjustments ) {
		$this->m_arrobjApPayee1099Adjustments = ( array ) $arrobjApPayee1099Adjustments;
	}

	public function getApPayee1099Adjustments() {
		return $this->m_arrobjApPayee1099Adjustments;
	}

	public function getTaxNumberMasked() {

		if( 4 == strlen( $this->getDecryptedTaxpayerIdNumber() ) ) {
			return $this->getTaxNumberEncrypted();
		} else {
			return CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $this->getDecryptedTaxpayerIdNumber() ) );
		}
	}

	/**
		@deprecated use function getTaxNumber()
	 */
	public function getDecryptedTaxpayerIdNumber() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );

	}

	public function getPayeeName() {
		return $this->m_strPayeeName;
	}

	public function getApPayeeLocationId() {
		return $this->m_intApPayeeLocationId;
	}

	public function setApPayeeLocationId( $intApPayeeLocationId ) {
		return $this->m_intApPayeeLocationId = $intApPayeeLocationId;
	}

	public function getApPayeeLegalEntityEvent() {
		return $this->m_arrmixApPayeeLegalEntity;
	}

	public function getIsTaxPayerIdChanged() {
		return $this->m_boolIsTaxPayerIdChanged;
	}

	public function getApLegalEntityCount() {
		return $this->m_intApLegalEntityCount;
	}

	public function setIsTaxPayerIdChanged( $boolIsTaxPayerIdChanged ) {
		$this->m_boolIsTaxPayerIdChanged = $boolIsTaxPayerIdChanged;
	}

	public function setPayeeName( $strPayeeName ) {
		$this->m_strPayeeName = CStrings::strTrimDef( $strPayeeName, 50, NULL, true );
	}

	public function setDefaultOwnerTypeId( $intDefaultOwnerTypeId ) {
		$this->set( 'm_intDefaultOwnerTypeId', CStrings::strToIntDef( $intDefaultOwnerTypeId, NULL, false ) );
	}

	public function getDefaultOwnerTypeId() {
		return $this->m_intDefaultOwnerTypeId;
	}

	public function setApLegalEntityCount( $strApLegalEntityCount ) {
		$this->m_intApLegalEntityCount = $strApLegalEntityCount;
	}

	public function setApPayeeStatusTypeId( $intApPayeeStatusTypeId ) {
		$this->m_intApPayeeStatusTypeId = $intApPayeeStatusTypeId;
	}

	public function getApPayeeStatusTypeId() {
		return $this->m_intApPayeeStatusTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['payee_name'] ) ) {
			$this->setPayeeName( $arrmixValues['payee_name'] );
		}

		if( true == isset( $arrmixValues['ap_payee_location_id'] ) ) {
			$this->setApPayeeLocationId( $arrmixValues['ap_payee_location_id'] );
		}

		if( true == isset( $arrmixValues['ap_legal_entity_count'] ) ) {
			$this->setApLegalEntityCount( $arrmixValues['ap_legal_entity_count'] );
		}

		if( true == isset( $arrmixValues['is_tax_payer_id_changed'] ) ) $this->setIsPaymentDelayed( $arrmixValues['is_tax_payer_id_changed'] );

		if( isset( $arrmixValues['default_owner_type_id'] ) ) {
			$this->set( 'm_intDefaultOwnerTypeId', trim( $arrmixValues['default_owner_type_id'] ) );
		} elseif( isset( $arrmixValues['default_owner_type_id'] ) ) {
			$this->setId( $arrmixValues['default_owner_type_id'] );
		}

		if( true == isset( $arrmixValues['ap_payee_status_type_id'] ) ) {
			$this->setApPayeeStatusTypeId( $arrmixValues['ap_payee_status_type_id'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valForm1099TypeId( $boolIsFromGenerate1099EFile = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->getForm1099TypeId() ) && true == $this->getReceives1099() && false == $boolIsFromGenerate1099EFile ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_type', '1099 Type is required.' ) );
			$boolIsValid = false;
		} elseif( true == $boolIsFromGenerate1099EFile && CForm1099Type::FORM_1099_TYPE_1 != $this->getForm1099TypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_type', '1099 - MISC is required.' ) );
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valForm1099BoxTypeId( $boolIsFromGenerate1099EFile = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->getForm1099BoxTypeId() ) && true == $this->getReceives1099() && false == $boolIsFromGenerate1099EFile ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_box_type', 'Box Number is required.' ) );
			$boolIsValid = false;
		} elseif( true == $boolIsFromGenerate1099EFile && CForm1099BoxType::FORM_1099_BOX_7 != $this->getForm1099BoxTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_box_type', '7 - Nonemployee Compensation is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valFormReceives1099() {
		$boolIsValid = true;

		if( false == $this->getReceives1099() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_receives_1099', 'Required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valEntityName() {
		$boolIsValid = true;

		if( true == is_null( $this->getEntityName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_name', 'Name on tax return is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted( $boolTaxNumberChange = false, $objClientDatabase, $boolIsFromGenerate1099EFile = false ) {
		$boolIsValid = true;
		if( true == $boolIsFromGenerate1099EFile && true == is_null( $this->getTaxNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'taxpayer_id_number_encrypted', 'EIN is required.' ) );
			$boolIsValid = false;
			return $boolIsValid;
		}

		if( false == is_null( $this->getTaxNumberEncrypted() ) && false == $boolIsFromGenerate1099EFile ) {

			// Check with tax number hash
			$intCountApLegalEntities = ( int ) \Psi\Eos\Entrata\CApLegalEntities::createService()->fetchApLegalEntityCountByCidByTaxNumberHash( $this->getCid(), $this->getTaxNumberHash(), $objClientDatabase, $this->getId() );
			if( 1 <= $intCountApLegalEntities && true == $boolTaxNumberChange ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'taxpayer_id_number_encrypted', 'EIN already exists.' ) );
				$boolIsValid = false;
			} elseif( false == is_null( $this->getTaxNumber() ) && false == preg_match( '/^([\d]{2}-[\d]{7})$|([\d]{3}-[\d]{2}-[\d]{4})$/', $this->getTaxNumber() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'taxpayer_id_number_encrypted', 'EIN should be in XX-XXXXXXX or XXX-XX-XXXX and should be numeric.' ) );
				$boolIsValid = false;
			}

		}

		return $boolIsValid;
	}

	public function valApPayee1099Adjustments() {
		$boolIsValid = true;

		if( true == $this->getReceives1099() ) {
			$arrobjTempApPayee1099Adjustments = $this->getApPayee1099Adjustments();
			if( true == valArr( $this->getApPayee1099Adjustments() ) ) {
				foreach( $this->getApPayee1099Adjustments() as $intKey => $objApPayee1099Adjustment ) {
					foreach( $arrobjTempApPayee1099Adjustments as $intTempKey  => $objTempApPayee1099Adjustment ) {
						if( $intKey != $intTempKey ) {
							if( $objApPayee1099Adjustment->getPropertyId() == $objTempApPayee1099Adjustment->getPropertyId() && false == is_null( $objApPayee1099Adjustment->getYear() ) && $objApPayee1099Adjustment->getYear() == $objTempApPayee1099Adjustment->getYear() ) {
								$boolIsValid = false;
								$objApPayee1099Adjustment->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Property can not be used for same year.' ) );
							}
						}
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase, $boolTaxNumberChange = false, $boolIsFromGenerate1099EFile = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEntityName();
				$boolIsValid &= $this->valForm1099BoxTypeId();
				$boolIsValid &= $this->valForm1099TypeId();
				$boolIsValid &= $this->valTaxNumberEncrypted( $boolTaxNumberChange, $objClientDatabase );
				$boolIsValid &= $this->valApPayee1099Adjustments();
				break;

			case VALIDATE_DELETE:
				break;

			case 'bulk_update_1099':
				$boolIsValid &= $this->valTaxNumberEncrypted( $boolTaxNumberChange, $objClientDatabase );
				break;

			case 'generate_1099_eFile':
				$boolIsValid &= $this->valTaxNumberEncrypted( $boolTaxNumberChange, $objClientDatabase, $boolIsFromGenerate1099EFile );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function addApPayeeLegalEntityEvent( $arrmixEvent ) {
		$this->m_arrmixApPayeeLegalEntity = $arrmixEvent;
	}

	// We will be use the setTaxNumber function instead of encryptTaxpayerIdNumber function.

	public function setTaxNumber( $strPlainTaxNumber ) {
		$strTaxNumberEncrypted = $strPlainTaxNumber;
		if( true == \Psi\Libraries\UtilFunctions\valStr( $strPlainTaxNumber ) ) {
			$strTaxNumberEncrypted = \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER );
		}

		$this->setTaxNumberEncrypted( $strTaxNumberEncrypted );
		$this->setTaxNumberHash( $strPlainTaxNumber );
	}

	public function getTaxNumber() {
		if( false == \Psi\Libraries\UtilFunctions\valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function setTaxNumberHash( $strPlainTaxNumber ) {

		$intPlainTaxNumber = Psi\Libraries\UtilStrings\CStrings::createService()->strTrimDef( str_replace( '-', '', $strPlainTaxNumber ) );

		if( true == is_numeric( $intPlainTaxNumber ) ) {
			parent::setTaxNumberHash( \Psi\Libraries\Cryptography\CCrypto::createService()->blindIndex( $intPlainTaxNumber, CONFIG_SODIUM_KEY_BLIND_INDEX ) );
		} else {
			parent::setTaxNumberHash( $intPlainTaxNumber );
		}
	}

}
?>