<?php

class CTransmissionResponseType extends CBaseTransmissionResponseType {

	const APPROVED 					= 1;
	const CONDITIONALLY_APPROVED 	= 2;
	const ALERT 					= 3;
	const IN_PROGRESS 				= 4;
	const NO_CREDIT 				= 5;
	const INSUFFICIENT_INFO 		= 6;
	const SYSTEM_DOWN 				= 7;
	const DENIED 					= 8;
	const TIMEOUT 					= 9;
	const SUCCESSFUL				= 10;
	const FAILURE 					= 11;
	const GUARANTOR_REQUIRED		= 12;
	const FURTHER_REVIEW			= 13;
	const NOT_FOUND					= 14;
	const ERROR                     = 18;

	public static function getTransmissionResponseTypeText( $intTransmissionResponseTypeId ) {
		$strTransmissionResponseTypeText = '';

 		switch( $intTransmissionResponseTypeId ) {
			case self::APPROVED:
				$strTransmissionResponseTypeText = __( 'Approved' );
				break;

			case self::CONDITIONALLY_APPROVED:
				$strTransmissionResponseTypeText = __( 'Conditionally Approved' );
				break;

			case self::ALERT:
				$strTransmissionResponseTypeText = __( 'Alert' );
				break;

			case self::IN_PROGRESS:
				$strTransmissionResponseTypeText = __( 'Pending' );
				break;

			case self::NO_CREDIT:
				$strTransmissionResponseTypeText = __( 'No Credit' );
				break;

			case self::INSUFFICIENT_INFO:
				$strTransmissionResponseTypeText = __( 'Insufficient Info' );
				break;

			case self::SYSTEM_DOWN:
				$strTransmissionResponseTypeText = __( 'System Down' );
				break;

			case self::DENIED:
				$strTransmissionResponseTypeText = __( 'Denied' );
				break;

			case self::TIMEOUT:
				$strTransmissionResponseTypeText = __( 'Timeout' );
				break;

			case self::SUCCESSFUL:
				$strTransmissionResponseTypeText = __( 'Successful' );
				break;

			case self::FAILURE:
				$strTransmissionResponseTypeText = __( 'Failure' );
				break;

			case self::GUARANTOR_REQUIRED:
				$strTransmissionResponseTypeText = __( 'Guarantor Required' );
				break;

			case self::FURTHER_REVIEW:
				$strTransmissionResponseTypeText = __( 'Further Review' );
				break;

			case self::NOT_FOUND:
				$strTransmissionResponseTypeText = __( 'Not Found' );
				break;

            case self::ERROR:
                $strTransmissionResponseTypeText = __( 'Error' );
                break;

			default:
				$strTransmissionResponseTypeText = __( 'Not Screened Yet' );
				break;
		}

		return $strTransmissionResponseTypeText;
	}

	public static $c_arrintScreeningRecommendationAndTransmissionResponseTypeAssociations = [
		CScreeningRecommendationType::PASS               => self::APPROVED,
		CScreeningRecommendationType::PASSWITHCONDITIONS => self::CONDITIONALLY_APPROVED,
		CScreeningRecommendationType::FAIL               => self::DENIED,
		CScreeningRecommendationType::PENDING_UNKNOWN    => self::IN_PROGRESS,
		CScreeningRecommendationType::ERROR              => self::ERROR,
	];

    public static function getScreeningStatusAndRecommendationTypes( $intTransmissionResponseTypeId ) {

        if( $intTransmissionResponseTypeId == CTransmissionResponseType::APPROVED ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningRecommendationType::PASS ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::CONDITIONALLY_APPROVED ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningRecommendationType::PASSWITHCONDITIONS ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::ALERT ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningRecommendationType::ERROR ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::IN_PROGRESS ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, CScreeningRecommendationType::PENDING_UNKNOWN ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::NO_CREDIT ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningRecommendationType::PASS ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::INSUFFICIENT_INFO ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningRecommendationType::ERROR ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::SYSTEM_DOWN ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningRecommendationType::ERROR ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::DENIED ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningRecommendationType::FAIL ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::TIMEOUT ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningRecommendationType::ERROR ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::SUCCESSFUL ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningRecommendationType::PASS ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::FAILURE ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningRecommendationType::ERROR ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::GUARANTOR_REQUIRED ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningRecommendationType::PASSWITHCONDITIONS ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::FURTHER_REVIEW ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN, CScreeningRecommendationType::PENDING_UNKNOWN ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::NOT_FOUND ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_COMPLETED, CScreeningRecommendationType::PASS ];
        } elseif( $intTransmissionResponseTypeId == CTransmissionResponseType::ERROR ) {
            return [ CScreeningStatusType::APPLICANT_RECORD_STATUS_ERROR, CScreeningRecommendationType::ERROR ];
        }
    }

}

?>