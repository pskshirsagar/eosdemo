<?php

class CStudentPropertyWindowSetting extends CBaseStudentPropertyWindowSetting {

	public function valId() {
		$boolIsValid = true;
		if( false == valId( $this->getId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Student Property window setting can\'t be Empty.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( false == valId( $this->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Client is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == valId( $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		if( false == valId( $this->getLeaseStartWindowId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Lease Term can\'t be Empty.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valLeaseStartWindowId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>