<?php

class CDataCleanupRequest extends CBaseDataCleanupRequest {

	protected $m_strCleanupAction;

	protected $m_arrintPropertyIds;

	// Get Functions

	public function getCleanupAction() {
		return $this->m_strCleanupAction;
	}

	public function getPropertyIds() {
		return ( array ) $this->m_arrintPropertyIds;
	}

	// Set Functions

	public function setCleanupAction( $strCleanupAction ) {
		$this->m_strCleanupAction = $strCleanupAction;
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = ( array ) $arrintPropertyIds;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['cleanup_action'] ) ) $this->setCleanupAction( $arrmixValues['cleanup_action'] );
		if( isset( $arrmixValues['property_ids'] ) ) $this->setPropertyIds( $arrmixValues['property_ids'] );

		return;
	}

	// Val Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyIds() {
		$boolIsValid = true;

		if( false == valArr( $this->getPropertyIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_ids', 'Please select property(s).' ) );
		}

		return $boolIsValid;
	}

	public function valDataCleanupTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getDataCleanupTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'data_cleanup_type_id', 'Please select data cleanup Type.' ) );
		}

		return $boolIsValid;
	}

	public function valSummary() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;

		if( false == CValidation::validateDate( $this->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Please enter correct month and year for valid post month.' ) );
		}

		return $boolIsValid;
	}

	public function valIsArCleanup() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsApCleanup() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsApproved() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletionOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFailedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlHistoryCleanup() {
		$boolIsValid = true;

		if( false == $this->getIsArCleanup() && false == $this->getIsApCleanup() && false == $this->getIsJeCleanup() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'clear_option', 'Please select clear option.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'gl_history':
				$boolIsValid &= $this->valPropertyIds();
				$boolIsValid &= $this->valDataCleanupTypeId();
				$boolIsValid &= $this->valGlHistoryCleanup();
				$boolIsValid &= $this->valPostMonth();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
