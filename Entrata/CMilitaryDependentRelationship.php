<?php

class CMilitaryDependentRelationship extends CBaseMilitaryDependentRelationship {

	const SPOUSE 			= 1;
	const CHILD 			= 2;

	public static $c_arrintMilitaryDependentRelationships = [
		self::SPOUSE => [
			'id'    => '1',
			'name' => 'Spouse'
		],
		self::CHILD => [
			'id'    => '2',
			'name' => 'Child'
		]
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
