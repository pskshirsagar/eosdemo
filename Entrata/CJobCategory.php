<?php

class CJobCategory extends CBaseJobCategory {

	protected $m_strGlAccountName;
	protected $m_strFormattedAccountNumber;

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( '&nbsp;cid does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '&nbsp;Name is required.' ) ) );
		} elseif( false == preg_match( '/^[A-Za-z \d\:\-\.\_\@\$\!\(\)\[\}\^\&\]\{\/\*\#\%\=\+]+$/i', trim( $this->getName() ) ) || false == mb_check_encoding( $this->getName(), 'UTF-8' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Invalid job type name (It contains special symbol).' ) ) );
		}

		if( true == $boolIsValid ) {
			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							AND id  <> ' . ( int ) $this->getId();

			if( 0 < \Psi\Eos\Entrata\CJobCategories::createService()->fetchJobCategoryCount( $strWhere, $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Job Category already exists.' ) ) );
			}

		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Get Function
	 */

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getFormattedAccountNumber() {
		return $this->m_strFormattedAccountNumber;
	}

	/*
	 * Set Function
	 */

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = $strGlAccountName;
	}

	public function setFormattedAccountNumber( $strFormattedAccountNumber ) {
		$this->m_strFormattedAccountNumber = $strFormattedAccountNumber;
	}

	/*
	 * Other Function
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['gl_account_name'] ) ) {
			$this->setGlAccountName( $arrmixValues['gl_account_name'] );
		}
		if( true == isset( $arrmixValues['formatted_account_number'] ) ) {
			$this->setFormattedAccountNumber( $arrmixValues['formatted_account_number'] );
		}

	}

}

?>