<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerClubs
 * Do not add any new functions to this class.
 */

class CCustomerClubs extends CBaseCustomerClubs {

	public static function fetchCustomerClubByCustomerIdByClubIdByCid( $intCustomerId, $intClubId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM customer_clubs WHERE customer_id = ' . ( int ) $intCustomerId . ' AND club_id = ' . ( int ) $intClubId . ' AND cid = ' . ( int ) $intCid;
    	return self::fetchCustomerClub( $strSql, $objDatabase );
	}

	public static function fetchCustomerClubsByClubIdsByCid( $arrintClubsIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintClubsIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM customer_clubs
					WHERE cid = ' . ( int ) $intCid . '
						AND club_id IN  (' . implode( ',', $arrintClubsIds ) . ')';
		return self::fetchCustomerClubs( $strSql, $objDatabase );

	}

	public static function fetchSimpleCustomerClubByClubIdByCidByFieldNamesByPropertyIds( $intClubId, $intCid, $arrstrFieldName, $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrstrFieldName ) ) return array();
		if( false == valArr( $arrintPropertyIds ) ) return array();

		$strSql = 'SELECT
						DISTINCT lc.customer_id
					FROM
					    customer_clubs AS custclb
						JOIN lease_customers AS lc ON ( lc.customer_id = custclb.customer_id AND lc.cid = custclb.cid )
          				JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
					WHERE
						custclb.club_id = ' . $intClubId . '
					    AND custclb.cid = ' . ( int ) $intCid . '
					    AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		$arrstrCustomerIds = fetchData( $strSql, $objDatabase );
		$arrstrFinalCustomerIds = array();

		if( true == valArr( $arrstrCustomerIds ) ) {
			foreach( $arrstrCustomerIds as $arrstrCustomerIds ) {
				$arrstrFinalCustomerIds[] = $arrstrCustomerIds['customer_id'];
			}
		}

		return $arrstrFinalCustomerIds;

	}

	public static function fetchCustomerClubsByClubIdByPropertyIdsByCid( $intClubId, $arrintPropertyIds, $intCid, $objDatabase, $intExcludeCustomerId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT lc.customer_id
				   FROM
						customer_clubs AS custclb
						JOIN lease_customers AS lc ON ( lc.customer_id = custclb.customer_id AND lc.cid = custclb.cid )
          				JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
				  WHERE
					   custclb.club_id = ' . $intClubId . '
	                   AND custclb.cid = ' . ( int ) $intCid . '
	                   AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		if( true == isset( $intExcludeCustomerId ) ) {
			$strSql .= ' AND lc.customer_id != ' . ( int ) $intExcludeCustomerId . ' ';
		}

		$arrintCustomerIds = fetchData( $strSql, $objDatabase );
		return $arrintCustomerIds;

	}
}
?>