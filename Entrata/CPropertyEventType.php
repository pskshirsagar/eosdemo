<?php

class CPropertyEventType extends CBasePropertyEventType {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultPropertyEventResultId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventTypeId( $objDatabase = NULL ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getEventTypeId() ) && true == valObj( $objDatabase, 'CDatabase' ) ) {

    		$objPropertyEventType 	= CPropertyEventTypes::fetchPropertyEventTypeByPropertyIdByEventTypeIdByExcludingIdByCid( $this->getPropertyId(), $this->getEventTypeId(), $this->getDefaultPropertyEventResultId(), $this->getCid(), $objDatabase );
			$arrobjEventTypes 		= \Psi\Eos\Entrata\CEventTypes::createService()->fetchPublishedEventTypes( $objDatabase );

    		if( true == valObj( $objPropertyEventType, 'CPropertyEventType' ) ) {

    			$boolIsValid = false;

    			if( true == valArr( $arrobjEventTypes ) && array_key_exists( $objPropertyEventType->getEventTypeId(), $arrobjEventTypes ) ) {
    				$objEventType = getArrayElementByKey( $objPropertyEventType->getEventTypeId(), $arrobjEventTypes );
    				$strEventTypeName = $objEventType->getName();
    			}

    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_type_id', 'Default value for ' . $strEventTypeName . ' is already being used.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valEventTypeId( $objDatabase );
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>