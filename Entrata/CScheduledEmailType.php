<?php

class CScheduledEmailType extends CBaseScheduledEmailType {

	const RESIDENTS 	= 1;
	const PROSPECTS		= 2;
	const EMPLOYEES		= 3;
	const SITE_MANAGERS	= 4;
	const VENDORS		= 5;

	private $m_arrstrScheduledEmailTypes;
	private $m_arrstrScheduledEmailTypeName;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getScheduledEmailTypes() {
		if( isset( $this->m_arrstrScheduledEmailTypes ) ) {
			return $this->m_arrstrScheduledEmailTypes;
		}

		$this->m_arrstrScheduledEmailTypes = [
			self::RESIDENTS => __( 'Residents' ),
			self::PROSPECTS => __( 'Leads/Applicants' ),
			self::EMPLOYEES => __( 'Employees' ),
			self::VENDORS	=> __( 'Vendors' )
		];
	}

	public function getScheduledEmailTypeName() {
		if( isset( $this->m_arrstrScheduledEmailTypes ) ) {
			return $this->m_arrstrScheduledEmailTypes;
		}

		$this->m_arrstrScheduledEmailTypes = [
			self::RESIDENTS => __( 'Customer' ),
			self::PROSPECTS => __( 'Applicant' ),
			self::EMPLOYEES => __( 'Employee' ),
			self::VENDORS	=> __( 'Vendor' )
		];
	}

	public static $c_arrintTemplateLibraryEmailTypes = [
		0,
		self::RESIDENTS,
		self::PROSPECTS,
		self::EMPLOYEES,
		self::VENDORS
	];

	public static $c_arrintSmsTextMessageEmailTypes = [
		self::RESIDENTS,
		self::PROSPECTS
	];

	public static $c_arrintMergeFieldGroupIds = [
		self::RESIDENTS => [ CMergeFieldGroup::APPLICANTS_AND_OCCUPANTS, CMergeFieldGroup::RESIDENTS_OCCUPANTS, CMergeFieldGroup::LEASE_AND_UNIT, CMergeFieldGroup::OTHERS, CMergeFieldGroup::PROPERTY, CMergeFieldGroup::CHARGES_AND_FEES, CMergeFieldGroup::POLICIES, CMergeFieldGroup::MILITARY, CMergeFieldGroup::COMMERCIAL, CMergeFieldGroup::GROUPS, CMergeFieldGroup::CUSTOMER_CONTACTS ],
		self::PROSPECTS => [ CMergeFieldGroup::APPLICANTS_AND_OCCUPANTS, CMergeFieldGroup::LEASE_AND_UNIT, CMergeFieldGroup::OTHERS, CMergeFieldGroup::PROPERTY, CMergeFieldGroup::POLICIES, CMergeFieldGroup::MILITARY, CMergeFieldGroup::CHARGES_AND_FEES ],
		self::EMPLOYEES => [ CMergeFieldGroup::OTHERS, CMergeFieldGroup::PROPERTY, CMergeFieldGroup::EMPLOYEE ],
		self::VENDORS   => [ CMergeFieldGroup::VENDOR ]
	];

	public static $c_arrstrRequiredInputTables = [
		self::RESIDENTS => [ 'lease_customers', 'leases', 'properties', 'company_users', 'customer_contacts', 'customers' ],
		self::PROSPECTS => [ 'applicant_applications', 'applications', 'properties', 'company_users' ],
		self::EMPLOYEES => [ 'properties', 'company_employees', 'company_users' ],
		self::VENDORS   => [ 'properties', 'ap_payees', 'ap_payee_contacts', 'company_users' ]
	];

}
?>