<?php

class CMarket extends CBaseMarket {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valZipCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFipsCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMsa() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarket() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>