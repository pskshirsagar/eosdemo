<?php

class CCompanyUserCalendar extends CBaseCompanyUserCalendar {

	protected $m_strCalendarName;
	protected $m_strCalendarGroup;
	protected $m_intCategoryId;

	public function getCategoryId() {
		return $this->m_intCategoryId;
	}

	public function getCalendarName() {
		return $this->m_strCalendarName;
	}

	public function getCalendarGroup() {
		return $this->m_strCalendarGroup;
	}

	public function setCategoryId( $intCategoryId ) {
		$this->m_intCategoryId = $intCategoryId;
	}

	public function setCalendarName( $strCalendarName ) {
		$this->m_strCalendarName = $strCalendarName;
	}

	public function setCalendarGroup( $strCalendarGroup ) {
		$this->m_strCalendarGroup = $strCalendarGroup;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['calendar_name'] ) ) $this->setCalendarName( $arrmixValues['calendar_name'] );
		if( true == isset( $arrmixValues['calendar_group'] ) ) $this->setCalendarGroup( $arrmixValues['calendar_group'] );
		if( true == isset( $arrmixValues['category_id'] ) ) $this->setCategoryId( $arrmixValues['category_id'] );
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCalendarPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCalendarCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valForegroundColor() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBackgroundColor() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsVisible() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

	public function fetchAllCalendarGroups( CDatabase $objDatabase, $boolGetCategoryIds = NULL ) {
		if( true == is_null( $this->getCompanyUserId() ) ) {
			return [];
		}

		if( false == is_null( $this->getCalendarPropertyId() ) ) {
			$strSql = '
				SELECT
					to_json( array_agg(p.property_name) ) AS categories,
					to_json( array_agg(p.id) ) AS category_ids
				FROM
					properties p
				WHERE
					p.cid = ' . ( int ) $this->getCid() . '
					AND p.id = ' . ( int ) $this->getCalendarPropertyId();

			$arrmixData = fetchData( $strSql, $objDatabase );
			if( $boolGetCategoryIds ) {
				return json_decode( $arrmixData[0]['category_ids'] );
			} else {
				return json_decode( $arrmixData[0]['categories'] );
			}
		} elseif( false == is_null( $this->getCalendarCompanyUserId() ) ) {
			$strSql = '
				WITH assigned_properties AS (
					SELECT
						p.cid,
						p.id AS calendar_property_id,
						NULL::INTEGER AS calendar_company_user_id,
						p.property_name AS category,
						p.id AS category_id,
						p.property_name || \' Calendar\' AS label,
						2 AS primary_order,
						1 AS secondary_order
					FROM
						company_users cu
						LEFT JOIN company_user_property_groups cupg ON cu.cid = cupg.cid AND cu.id = cupg.company_user_id
						LEFT JOIN property_groups pg ON ( pg.cid = cu.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON cu.cid = pga.cid AND ( pg.id = pga.property_group_id OR cu.is_administrator = 1 )
						JOIN properties p ON pga.cid = p.cid AND pga.property_id = p.id
					WHERE
						cu.cid = ' . ( int ) $this->getCid() . '
						AND cu.id = ' . ( int ) $this->getCompanyUserId() . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					GROUP BY
						p.cid,
						p.id
					ORDER BY
						p.property_name
				), associated_users AS (
					SELECT
						cu.cid,
						NULL::INTEGER AS calendar_property_id,
						cu.id AS calendar_company_user_id,
						p.property_name,
						p.id AS category_id,
						CASE
							WHEN cu.id = ' . ( int ) $this->getCompanyUserId() . ' THEN COALESCE( \' My Calendar \' )
							ELSE COALESCE( ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last )
						END AS label,
						2 AS primary_order,
						2 AS secondary_order
					FROM
						assigned_properties ap
						JOIN properties p ON ap.cid = p.cid AND ap.calendar_property_id = p.id
						JOIN property_group_associations pga ON ap.cid = pga.cid AND ap.calendar_property_id = pga.property_id
						JOIN company_user_property_groups cupg ON pga.cid = cupg.cid AND pga.property_group_id = cupg.property_group_id
						JOIN property_groups pg ON pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL
						JOIN company_users cu ON cupg.cid = cu.cid AND cupg.company_user_id = cu.id AND cu.is_administrator = 0 AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						JOIN company_employees ce on cu.cid = ce.cid AND ce.id = cu.company_employee_id
					WHERE
						ap.cid = ' . ( int ) $this->getCid() . '
				), global_users AS (
					SELECT
						cu.cid,
						NULL::INTEGER AS calendar_property_id,
						cu.id AS calendar_company_user_id,
						\'Other Calendars\'::TEXT AS category,
						1 AS category_id,
						CASE
							WHEN cu.id = ' . ( int ) $this->getCompanyUserId() . ' THEN COALESCE( \' My Calendar \' )
							ELSE COALESCE( ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last )
						END AS label,
						1 AS primary_order,
						1 AS secondary_order
					FROM
						company_users cu
						JOIN company_employees ce ON cu.cid = ce.cid AND cu.company_employee_id = ce.id
					WHERE
						cu.is_administrator = 1
						AND cu.default_company_user_id IS NULL
						AND cu.cid = ' . ( int ) $this->getCid() . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
				)
				SELECT
					to_json( array_agg( c.category ) )::TEXT AS categories,
					to_json( array_agg( c.category_id ) )::TEXT AS category_ids
				FROM
					(
						SELECT * FROM (
							SELECT * FROM assigned_properties ap
							UNION
							SELECT * FROM associated_users au
							UNION
							SELECT * FROM global_users gu
						) c
						WHERE
							c.cid = ' . ( int ) $this->getCid() . '
							AND c.calendar_company_user_id = ' . ( int ) $this->getCalendarCompanyUserId() . '
						ORDER BY
							c.category
					) c
				';

			$arrmixData = fetchData( $strSql, $objDatabase );
			if( $boolGetCategoryIds ) {
				return json_decode( $arrmixData[0]['category_ids'] );
			} else {
				return json_decode( $arrmixData[0]['categories'] );
			}
		} else {
			return [];
		}
	}

}
?>