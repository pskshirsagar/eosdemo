<?php

class CApPayeeStatusType extends CBaseApPayeeStatusType {

	protected $m_strApPayeeStatusTypes;
	const ACTIVE	= 1;
	const INACTIVE	= 2;
	const LOCKED	= 3;

	public static $c_arrstrApPayeeStatusTypes = [
		self::ACTIVE	=> 'Active',
		self::INACTIVE	=> 'Inactive',
		self::LOCKED	=> 'On Hold',
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getApPayeeStatusTypes() {
		if( isset( $this->m_strApPayeeStatusTypes ) ) {
			return $this->m_strApPayeeStatusTypes;
		}

		$this->m_strApPayeeStatusTypes = [
			self::ACTIVE		=> __( 'Active' ),
			self::INACTIVE		=> __( 'Inactive' ),
			self::LOCKED		=> __( 'On Hold' )
		];

		return $this->m_strApPayeeStatusTypes;

	}

}
?>