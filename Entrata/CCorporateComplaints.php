<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateComplaints
 * Do not add any new functions to this class.
 */

class CCorporateComplaints extends CBaseCorporateComplaints {

	public static function fetchPaginatedCorporateComplaintsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $objDatabase, $boolIsCountOnly = false ) {

		if( false == valId( $intCid ) || false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$arrstrPriorityWhere	= [];
		$strPriorityWhere		= '';

		if( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) {
			foreach( $objDashboardFilter->getTaskPriorities() as $intTaskPriority ) {
				if( 3 == $intTaskPriority ) {
					$arrstrPriorityWhere[] = '5 <= EXTRACT ( DAY FROM NOW() - cc.created_on::DATE )';
				} elseif( 2 == $intTaskPriority ) {
					$arrstrPriorityWhere[] = '( 3 <= EXTRACT ( DAY FROM NOW() - cc.created_on::DATE ) AND 5 > EXTRACT ( DAY FROM NOW() - cc.created_on::DATE ) )';
				} else {
					$arrstrPriorityWhere[] = '( 0 <= EXTRACT ( DAY FROM NOW() - cc.created_on::DATE ) AND 3 > EXTRACT ( DAY FROM NOW() - cc.created_on::DATE ) )';
				}
			}

			$strPriorityWhere = ' AND ( ' . implode( ' OR ', $arrstrPriorityWhere ) . ' )';
		}

		$strSql = 'SELECT COUNT( cc.id ) AS total_corporate_complaints';

		if( false == $boolIsCountOnly ) {
			$strSql = 'SELECT
						cc.*,
						cs.name_first,
						cs.name_last,
						cs.phone_number,
						cs.office_number,
						cs.mobile_number,
						cs.email_address,
						cs.customer_id,
						cs.unit_number,
						cc.created_on AS contact_datetime,
						p.property_name,
						EXTRACT ( DAY FROM NOW() - cc.created_on::DATE ) AS priority';
		}

		$strSql .= ' FROM
						corporate_complaints AS cc
						LEFT JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) lp ON ( lp.property_id = cc.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
						JOIN contact_submissions AS cs ON ( cs.id = cc.contact_submission_id AND cs.cid = cc.cid )
						LEFT JOIN properties AS p ON ( p.id = cc.property_id AND p.cid = cc.cid )
					WHERE
						cc.cid = ' . ( int ) $intCid . '
						AND ( lp.property_id IS NOT NULL
						OR cc.property_id IS NULL )
						AND cc.resolved_by IS NULL
						AND cc.resolved_on IS NULL 
						AND cs.deleted_by IS NULL
						AND cs.deleted_on IS NULL' . $strPriorityWhere;

		if( true == $boolIsCountOnly ) {
			$arrmixData = fetchData( $strSql, $objDatabase );
			return ( true == valArr( $arrmixData ) ) ? $arrmixData[0] : [ 'total_corporate_complaints' => 0 ];
		}

		$strSql .= ' ORDER BY
						cc.created_on
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return self::fetchCorporateComplaints( $strSql, $objDatabase );
	}

	public static function fetchCorporateComplaintByIdByCid( $intId, $intCid, $objDatabase, $intPropertyId = NULL ) {
		if( false == valId( $intId ) ) {
			return NULL;
		}

		$strWhere = NULL;

		if( true == valId( $intPropertyId ) ) {
			$strWhere .= ' AND cc.property_id =' . ( int ) $intPropertyId;
		}

		$strSql = 'SELECT
						cc.*,
						cs.name_first,
						cs.name_last,
						cs.phone_number,
						cs.office_number,
						cs.mobile_number,
						cs.email_address,
						cs.customer_id,
						cs.unit_number,
						to_char( cc.created_on , \'dd/mm/yyyy\' ) AS contact_datetime,
						p.property_name
					FROM
						corporate_complaints AS cc
						JOIN contact_submissions AS cs ON ( cs.id = cc.contact_submission_id AND cs.cid = cc.cid )
						LEFT JOIN properties AS p ON ( p.id = cc.property_id AND p.cid = cc.cid )
					WHERE
						cc.id = ' . ( int ) $intId . '
						AND cc.cid = ' . ( int ) $intCid . $strWhere;

		return self::fetchCorporateComplaint( $strSql, $objDatabase );
	}

}
?>