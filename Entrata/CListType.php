<?php
class CListType extends CBaseListType {

	const EVICTION						= 1;
	const SKIP							= 2;
	const NO_SHOW						= 3;
	const MOVE_OUT						= 4;
	const TRANSFER						= 5;
	const LEAD_CANCELLATION				= 6;
	const LEAD_DENIAL_REASONS			= 7;
	const LEAD_MERGE_REASONS			= 8;
	const LEAD_ARCHIVE_REASONS			= 9;
	const UNKNOWN						= 10;
	const AFFORDABLE_ADVANCED_ELIGIBILITY	= 11;
	const AFFORDABLE_MOVE_OUT_REASONS 	    = 12;

	public static $c_arrintLeadReasonTypes = [
		CListType::LEAD_CANCELLATION,
		CListType::LEAD_DENIAL_REASONS,
		CListType::LEAD_ARCHIVE_REASONS
	];

	public static $c_arrintMoveOutReasonTypes = [
		CListType::MOVE_OUT,
		CListType::EVICTION,
		CListType::SKIP
	];

	public static $c_arrintLeadCancellationAndArchiveReasons = [
		CListType::LEAD_CANCELLATION	=> 'Cancel',
		CListType::LEAD_ARCHIVE_REASONS	=> 'Archive'
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLeaseViolation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function listTypeIdToStr( $intListTypeId, $intLeaseStatusTypeId ) {

		if( false == valId( $intListTypeId ) || false == valId( $intLeaseStatusTypeId ) ) return NULL;

		$strListTypeName = NULL;

		switch( $intListTypeId ) {

			case CListType::EVICTION:
				if( true == in_array( $intLeaseStatusTypeId, CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) ) {
					$strListTypeName .= ' (Evicting)';
				}

				if( CLeaseStatusType::PAST == $intLeaseStatusTypeId ) {
					$strListTypeName .= ' (Evicted)';
				}
				break;

			case CListType::SKIP:
				if( true == in_array( $intLeaseStatusTypeId, CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) ) {
					$strListTypeName .= ' (Skipping)';
				}

				if( CLeaseStatusType::PAST == $intLeaseStatusTypeId ) {
					$strListTypeName .= ' (Skipped)';
				}
				break;

			default:
				// default case
				break;
		}

		return $strListTypeName;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getListTypeByListTypeId( $intListTypeId ) {
		$strListTypeName = NULL;

		switch( $intListTypeId ) {

			case CListType::LEAD_CANCELLATION:
				$strListTypeName = __( 'Cancel' );
				break;

			case CListType::LEAD_ARCHIVE_REASONS:
				$strListTypeName = __( 'Archive' );
				break;

			case CListType::SKIP:
				$strListTypeName = __( 'Skip' );
				break;

			case CListType::EVICTION:
				$strListTypeName = __( 'Eviction' );
				break;

			case CListType::MOVE_OUT:
				$strListTypeName = __( 'Move Out' );
				break;

			default:
				// default case
				break;
		}
		return $strListTypeName;
	}

	public function getLeadCancellationAndArchiveReasons() {
		return [
			CListType::LEAD_CANCELLATION    => $this->getListTypeByListTypeId( CListType::LEAD_CANCELLATION ),
			CListType::LEAD_ARCHIVE_REASONS => $this->getListTypeByListTypeId( CListType::LEAD_ARCHIVE_REASONS )
		];
	}

	public function getMoveOutReasonTypes() {
		return [
			self::MOVE_OUT => $this->getListTypeByListTypeId( CListType::MOVE_OUT ),
			self::EVICTION => $this->getListTypeByListTypeId( CListType::EVICTION ),
			self::SKIP     => $this->getListTypeByListTypeId( CListType::SKIP )
		];
	}

}
?>