<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReimbursementMethods
 * Do not add any new functions to this class.
 */

class CReimbursementMethods extends CBaseReimbursementMethods {

	public static function fetchReimbursementMethods( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CReimbursementMethod::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReimbursementMethod( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CReimbursementMethod::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>