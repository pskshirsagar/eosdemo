<?php

class CPropertyMaintenanceLocationProblem extends CBasePropertyMaintenanceLocationProblem {

	protected $m_strMaintenanceLocationName;
	protected $m_strDescription;
	protected $m_strName;
	protected $m_intParentPropertyMaintenanceProblemId;
	protected $m_intParentMaintenanceProblemId;
	protected $m_strMaintenanceProblemName;
	protected $m_intPropertyMaintenanceProblemId;
	protected $m_intMaintenanceProblemId;
	protected $m_intMaintenanceLocationId;
	protected $m_intMaintenanceProblemTypeId;
	protected $m_intMaintenanceProblemIdActionId;
	protected $m_intSettingsTemplateId;
	protected $m_intInspectionFormId;
	protected $m_strMaintenanceLocationProblem;
	protected $m_boolIsPublishedProblemAction;

    /**
     * Set Functions
     */

    public function setParentMaintenanceProblemId( $intParentMaintenanceProblemId ) {
    	$this->m_intParentMaintenanceProblemId = $intParentMaintenanceProblemId;
    }

    public function setName( $strName ) {
    	$this->m_strName = CStrings::strTrimDef( $strName, 240, NULL, true );
    }

    public function setDescription( $strDescription ) {
    	$this->m_strDescription = CStrings::strTrimDef( $strDescription, 240, NULL, true );
    }

    public function setMaintenanceLocationName( $strMaintenanceLocationName ) {
    	$this->m_strMaintenanceLocationName = CStrings::strTrimDef( $strMaintenanceLocationName, 240, NULL, true );
    }

    public function setParentPropertyMaintenanceProblemId( $intParentPropertyMaintenanceProblemId ) {
    	$this->m_intParentPropertyMaintenanceProblemId = $intParentPropertyMaintenanceProblemId;
    }

	public function setMaintenanceProblemName( $strMaintenanceProblemName ) {
		$this->m_strMaintenanceProblemName = $strMaintenanceProblemName;
	}

	public function setPropertyMaintenanceProblemId( $intPropertyMaintenanceProblemId ) {
		$this->m_intPropertyMaintenanceProblemId = $intPropertyMaintenanceProblemId;
	}

	public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
		$this->m_intMaintenanceProblemId = $intMaintenanceProblemId;
	}

	public function setMaintenanceProblemIdActionId( $intMaintenanceProblemIdActionId ) {
		$this->m_intMaintenanceProblemIdActionId = $intMaintenanceProblemIdActionId;
	}

	public function setMaintenanceProblemTypeId( $intMaintenanceProblemTypeId ) {
		$this->m_intMaintenanceProblemTypeId = $intMaintenanceProblemTypeId;
	}

	public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
		$this->m_intMaintenanceLocationId = $intMaintenanceLocationId;
	}

	public function setIsPublishedProblemAction( $boolIsPublishedProblemAction ) {
		$this->m_boolIsPublishedProblemAction = CStrings::strToBool( $boolIsPublishedProblemAction );
	}

	public function setInspectionFormId( $intInspectionFormId ) {
		$this->m_intInspectionFormId = $intInspectionFormId;
	}

    /**
     * Get Functions
     */

    public function getName() {
    	return $this->m_strName;
    }

    public function getParentMaintenanceProblemId() {
    	return $this->m_intParentMaintenanceProblemId;
    }

    public function getDescription() {
    	return $this->m_strDescription;
    }

    public function getMaintenanceLocationName() {
    	return $this->m_strMaintenanceLocationName;
    }

    public function getParentPropertyMaintenanceProblemId() {
    	return $this->m_intParentPropertyMaintenanceProblemId;
    }

	public function getMaintenanceProblemName() {
		return $this->m_strMaintenanceProblemName;
	}

	public function getPropertyMaintenanceProblemId() {
		return $this->m_intPropertyMaintenanceProblemId;
	}

	public function getMaintenanceProblemId() {
		return $this->m_intMaintenanceProblemId;
	}

	public function getMaintenanceProblemIdActionId() {
		return $this->m_intMaintenanceProblemIdActionId;
	}

	public function getMaintenanceProblemTypeId() {
		return $this->m_intMaintenanceProblemTypeId;
	}

	public function getMaintenanceLocationId() {
		return $this->m_intMaintenanceLocationId;
	}

	public function setSettingsTemplateId( $intSettingsTemplateId ) {
		$this->m_intSettingsTemplateId = $intSettingsTemplateId;
	}

	public function getSettingsTemplateId() {
		return $this->m_intSettingsTemplateId;
	}

	public function setMaintenanceLocationProblem( $strMaintenanceLocationProblem ) {
		$this->m_strMaintenanceLocationProblem = $strMaintenanceLocationProblem;
	}

	public function getMaintenanceLocationProblem() {
		return $this->m_strMaintenanceLocationProblem;
	}

	public function getIsPublishedProblemAction() {
		return $this->m_boolIsPublishedProblemAction;
	}

	public function getInspectionFormId() {
		return $this->m_intInspectionFormId;
	}

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, true, $boolDirectSet );

    	if( true == isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
    	if( true == isset( $arrValues['parent_maintenance_problem_id'] ) ) $this->setParentMaintenanceProblemId( $arrValues['parent_maintenance_problem_id'] );
    	if( true == isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
    	if( true == isset( $arrValues['maintenance_location_name'] ) ) $this->setMaintenanceLocationName( $arrValues['maintenance_location_name'] );
    	if( true == isset( $arrValues['parent_property_maintenance_problem_id'] ) ) $this->setParentPropertyMaintenanceProblemId( $arrValues['parent_property_maintenance_problem_id'] );
		if( true == isset( $arrValues['maintenance_problem_name'] ) ) $this->setMaintenanceProblemName( $arrValues['maintenance_problem_name'] );
		if( true == isset( $arrValues['property_maintenance_problem_id'] ) ) $this->setPropertyMaintenanceProblemId( $arrValues['property_maintenance_problem_id'] );
		if( true == isset( $arrValues['maintenance_problem_id'] ) ) $this->setMaintenanceProblemId( $arrValues['maintenance_problem_id'] );
		if( true == isset( $arrValues['maintenance_problem_type_id'] ) ) $this->setMaintenanceProblemTypeId( $arrValues['maintenance_problem_type_id'] );
		if( true == isset( $arrValues['maintenance_location_id'] ) ) $this->setMaintenanceLocationId( $arrValues['maintenance_location_id'] );
		if( true == isset( $arrValues['maintenance_problem_action_id'] ) ) $this->setMaintenanceProblemIdActionId( $arrValues['maintenance_problem_action_id'] );
		if( true == isset( $arrValues['settings_template_id'] ) ) $this->setSettingsTemplateId( $arrValues['settings_template_id'] );
		if( true == isset( $arrValues['maintenance_location_problem_id'] ) ) $this->setMaintenanceLocationProblem( $arrValues['maintenance_location_problem_id'] );
	    if( true == isset( $arrValues['is_published_problem_action'] ) ) $this->setIsPublishedProblemAction( $arrValues['is_published_problem_action'] );
	    if( true == isset( $arrValues['inspection_form_id'] ) ) $this->setInspectionFormId( $arrValues['inspection_form_id'] );

    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceLocationProblemId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>