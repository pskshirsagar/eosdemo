<?php

class CGlBook extends CBaseGlBook {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlBookId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'GL Book Name is required.' ) ) );
		}

		if( true == $boolIsValid ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							AND id <> ' . ( int ) $this->getId() . ' ';

			if( 0 < \Psi\Eos\Entrata\CGlBooks::createService()->fetchGlBookCount( $strWhere, $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'GL Book Name \'{%s, 0}\' already exists.', [ $this->getName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlBookDependencies( $objDatabase, $boolIsDisableCheck = false ) {

		$boolIsValid			= true;

		$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . '
					AND gl_book_id = ' . ( int ) $this->getId() . ' ';

		if( 0 < CGlHeaders::fetchGlHeaderCount( $strWhere, $objDatabase ) ) {
			if( false == $boolIsDisableCheck ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '\'{%s, 0}\' cannot be deleted because it is associated to journal entries.', [ $this->getName() ] ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '\'{%s, 0}\' is associated to journal entries.', [ $this->getName() ] ) ) );
			}
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valGlBookDependencies( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>