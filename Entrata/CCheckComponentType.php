<?php

class CCheckComponentType extends CBaseCheckComponentType {

	const BANK_INFO						= 1;
	const COMPANY_INFO					= 2;
	const CHECK_NUMBER					= 3;
	const PAY_TO_THE_ORDER_OF 			= 4;
	const PAYEE_ADDRESS					= 5;
	const DATE_LABEL					= 6;
	const AMOUNT_LABEL					= 7;
	const CHECK_MEMO					= 8;
	const MICR_LINE						= 9;
	const SIGNATURE_LINE_ONE			= 10;
	const SIGNATURE_LINE_TWO			= 11;
	const AMOUNT_IN_WORDS				= 12;
	const FRACTIONAL_ROUTING_NUMBER		= 13;
	const CUSTOM_TEXT					= 14;
	const BANK_ADDRESS					= 15;
	const PAY_TO						= 16;
	const PAYEE_NAME					= 17;
	const DATE							= 18;
	const AMOUNT						= 19;
	const VOID							= 20;
	const NON_NEGOTIABLE				= 21;
	const CHECK_LOGO					= 22;
	const COUNTRY_NAME					= 23;
	const PAY_TO_THE_ORDER_OF_24 		= 24;
	const PAY_TO_THE_ORDER_OF_25 		= 25;
	const PAY_TO_THE_ORDER_OF_26 		= 26;
	const DATE_FORMAT_2					= 27;
	const DATE_FORMAT_3					= 28;
	const DATE_FORMAT_4					= 29;
	const DATE_FORMAT_5					= 30;
	const DATE_FORMAT_6					= 31;
	const DATE_FORMAT_7					= 32;
	const DATE_FORMAT_8					= 33;
	const DATE_FORMAT_9					= 34;


	public static $c_strDateOptions = [
		SELF::DATE				=> 'MM/DD/YYYY',
		SELF::DATE_FORMAT_2		=> 'DD/MM/YYYY',
		SELF::DATE_FORMAT_3		=> 'YYYY/MM/DD',
		SELF::DATE_FORMAT_4		=> 'MM-DD-YYYY',
		SELF::DATE_FORMAT_5		=> 'DD-MM-YYYY',
		SELF::DATE_FORMAT_6		=> 'YYYY-MM-DD',
		SELF::DATE_FORMAT_7		=> 'MMDDYYYY',
		SELF::DATE_FORMAT_8		=> 'DDMMYYYY',
		SELF::DATE_FORMAT_9		=> 'YYYYMMDD'
	];

	public static $c_strDateFormats = [
		CCheckComponentType::DATE			=> 'm/d/Y',
		CCheckComponentType::DATE_FORMAT_2	=> 'd/m/Y',
		CCheckComponentType::DATE_FORMAT_3	=> 'Y/m/d',
		CCheckComponentType::DATE_FORMAT_4	=> 'm-d-Y',
		CCheckComponentType::DATE_FORMAT_5	=> 'd-m-Y',
		CCheckComponentType::DATE_FORMAT_6	=> 'Y-m-d',
		CCheckComponentType::DATE_FORMAT_7	=> 'mdY',
		CCheckComponentType::DATE_FORMAT_8	=> 'dmY',
		CCheckComponentType::DATE_FORMAT_9	=> 'Ymd'
	];
}
?>