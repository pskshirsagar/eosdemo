<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPaymentBatches
 * Do not add any new functions to this class.
 */

class CApPaymentBatches extends CBaseApPaymentBatches {

	public static function fetchApPaymentBatches( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return self::fetchCachedObjects( $strSql, 'CApPaymentBatch', $objDatabase, DATA_CACHE_MEMORY, NULL, false, $boolIsReturnKeyedArray );
	}

	public static function fetchApPaymentBatch( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CApPaymentBatch', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchApPaymentBatchesByCidByApPaymentsFilter( $intCid, $objDatabase, $arrintPermissionedProperties, $objApPaymentsFilter, $intPageNo = NULL, $intCountPerPage = NULL ) {

		$strPaginationCondition = '';
		if( false == is_null( $intPageNo ) && false == is_null( $intCountPerPage ) ) {
			$intOffset	= ( 0 < $intPageNo ) ? $intCountPerPage * ( $intPageNo - 1 ) : 0;
			$intLimit	= ( int ) $intCountPerPage;

			$strPaginationCondition = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		$strWhereSql = '';

		$strPostMonth = $objApPaymentsFilter->getPostMonth();
		if( true == valStr( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}

			$strWhereSql .= ( true == valStr( $strPostMonth ) ) ? ' AND apb.post_month = \'' . date( 'd-M-Y', strtotime( $strPostMonth ) ) . '\' ' : '';
		}

		$strWhereSql .= ( true == valStr( $objApPaymentsFilter->getDateFrom() ) ) ? ' AND apb.post_date >= \'' . date( 'Y-m-d', strtotime( $objApPaymentsFilter->getDateFrom() ) ) . '\' ' : '';
		$strWhereSql .= ( true == valStr( $objApPaymentsFilter->getDateTo() ) ) ? ' AND apb.post_date <= \'' . date( 'Y-m-d', strtotime( $objApPaymentsFilter->getDateTo() ) ) . '\' ' : '';

		if( true == valArr( $objApPaymentsFilter->getBankAccountIds() ) ) {
			$strWhereSql .= ' AND apb.bank_account_ids && ARRAY[ ' . implode( ',', $objApPaymentsFilter->getBankAccountIds() ) . ' ]::INT[] ';
		}

		$strFilesWhereSql = '';
		if( 0 == $objApPaymentsFilter->getIsArchived() ) {
			$strFilesWhereSql .= ' AND f.details->\'show_in_entrata\' IS NULL';
		}

		if( true == valArr( $objApPaymentsFilter->getApPaymentFileTypeIds() ) ) {
			$strFilesWhereSql .= ' AND ft.system_code IN( \'' . implode( '\',\'', $objApPaymentsFilter->getApPaymentFileTypeIds() ) . '\' )';
		}

		if( true == valArr( $arrintPermissionedProperties ) ) {
			$strPropertyWhereSql = '
			AND NOT EXISTS (
					SELECT
						1
					FROM
						ap_payments ap
						JOIN ap_headers ah ON ah.cid = ap.cid AND ap.id = ah.ap_payment_id
						JOIN ap_details ad ON ad.cid = ah.cid AND ah.id = ad.ap_header_id
					WHERE
						apb.cid = ap.cid
						AND apb.id = ap.ap_payment_batch_id
						AND ad.property_id NOT IN ( ' . implode( ',', $arrintPermissionedProperties ) . ' )
				)';
		} else {
			$strPropertyWhereSql = 'AND false /* no permissioned properties means no rows for you */';
		}

		$strSql = '
			SELECT
				apb.*,
				( SELECT
						CASE
							WHEN sub.voided_ap_payments = apb.control_count THEN \'Voided\'
							WHEN ( sub.voided_ap_payments )::integer = 0 THEN \'Active\'
							WHEN sub.voided_ap_payments <> 0 AND sub.voided_ap_payments <> apb.control_count THEN \'Partially Voided\'

						END
					FROM
						(
							SELECT
								count ( id ) AS voided_ap_payments
							FROM
								ap_payments ap
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND ap.ap_payment_batch_id = apb.id
								AND ap.payment_status_type_id = ' . ( int ) CPaymentStatusType::VOIDED . '
						) AS sub
				) AS ap_payment_batch_status
			FROM
				ap_payment_batches apb
			WHERE
				apb.cid = ' . ( int ) $intCid . '
				' . $strWhereSql . '
				AND EXISTS (
					SELECT
						NULL
					FROM
						file_associations fa
						JOIN files f ON fa.cid = f.cid AND f.id = fa.file_id
						JOIN file_types ft ON ft.cid = f.cid AND f.file_type_id = ft.id
					WHERE
						apb.cid = fa.cid
						AND apb.id = fa.ap_payment_batch_id
						AND ft.system_code NOT LIKE \'CHECKCOPY\'
						' . $strFilesWhereSql . '
				)
				' . $strPropertyWhereSql . '
			ORDER BY
				id DESC ' . $strPaginationCondition;

		return parent::fetchApPaymentBatches( $strSql, $objDatabase );
	}

	public static function fetchActiveApPaymentBatchesByIdsByCid( $arrintApPaymentBatchIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApPaymentBatchIds ) ) return NULL;

		$strSql = 'SELECT
						apb.*
					FROM
						ap_payment_batches apb
					WHERE
						apb.cid = ' . ( int ) $intCid . '
						AND apb.id IN ( ' . implode( ',', $arrintApPaymentBatchIds ) . ' )';

		return parent::fetchApPaymentBatches( $strSql, $objDatabase );
	}

}
?>