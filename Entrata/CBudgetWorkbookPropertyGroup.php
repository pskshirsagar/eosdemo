<?php

class CBudgetWorkbookPropertyGroup extends CBaseBudgetWorkbookPropertyGroup {

	protected $m_strBudgetWorkbookName;
	protected $m_strPropertyGroupName;

	public function valPropertyGroupId( $arrobjActiveBudgetWorkbookPropertyGroups ) {
		$boolIsValid = true;

		if( true == array_key_exists( $this->getPropertyGroupId(), $arrobjActiveBudgetWorkbookPropertyGroups ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group_id', $arrobjActiveBudgetWorkbookPropertyGroups[$this->getPropertyGroupId()]->getPropertyGroupName() . ' ' . __( 'is already assigned to an Active workbook' ) . ' ' . $arrobjActiveBudgetWorkbookPropertyGroups[$this->getPropertyGroupId()]->getBudgetWorkbookName() . '.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixParameters = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_active_properties':
				$boolIsValid &= $this->valPropertyGroupId( $arrmixParameters['active_property_groups'] );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getBudgetWorkbookName() {
		return $this->m_strBudgetWorkbookName;
	}

	public function getPropertyGroupName() {
		return $this->m_strPropertyGroupName;
	}

	/**
	 * Set Functions
	 */

	public function setBudgetWorkbookName( $strBudgetWorkbookName ) {
		$this->m_strBudgetWorkbookName = $strBudgetWorkbookName;
	}

	public function setPropertyGroupName( $strPropertyGroupName ) {
		$this->m_strPropertyGroupName = $strPropertyGroupName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['budget_workbook_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strBudgetWorkbookName', trim( $arrmixValues['budget_workbook_name'] ) );
		} elseif( isset( $arrmixValues['budget_workbook_name'] ) ) {
			$this->setBudgetWorkbookName( $arrmixValues['budget_workbook_name'] );
		}

		if( isset( $arrmixValues['property_group_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strPropertyGroupName', trim( $arrmixValues['property_group_name'] ) );
		} elseif( isset( $arrmixValues['property_group_name'] ) ) {
			$this->setPropertyGroupName( $arrmixValues['property_group_name'] );
		}
	}

}

?>