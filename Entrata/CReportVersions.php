<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportVersions
 * Do not add any new functions to this class.
 */

class CReportVersions extends CBaseReportVersions {

	/**
	 * Returns the latest available version for a given report
	 *
	 * @param $intReportId
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReportVersion
	 */
	public static function fetchLatestReportVersionByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				report_version_details.*
			FROM
				(
				SELECT
					*,
					row_number ( ) OVER ( Partition BY rv.report_id
					ORDER BY
					rv.is_default DESC ) AS default_count
				FROM
					report_versions rv
				WHERE
					rv.cid = ' . ( int ) $intCid . '
					AND rv.report_id = ' . ( int ) $intReportId . '
					AND ( rv.is_default = TRUE
					OR rv.is_latest = TRUE )
				ORDER BY
					rv.is_default DESC
				) AS report_version_details
			WHERE
				report_version_details.default_count = 1
				AND COALESCE ( ( CASE
									WHEN report_version_details.is_default = FALSE THEN NULL
									ELSE TRUE
								END ), report_version_details.is_latest ) = TRUE';

		return self::fetchReportVersion( $strSql, $objDatabase );
	}

	public static function fetchReportVersionsByReportIdByCid( $intReportId, $intCid, $objDatabase, $intCompanyUserId = NULL ) {
		$strSql = '
			SELECT
				rv.*
			FROM
				report_versions rv
				JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
			WHERE
				rv.cid = ' . ( int ) $intCid . '
				AND rv.report_id = ' . ( int ) $intReportId . '
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND COALESCE( (rv.definition->>\'is_published\')::BOOLEAN, TRUE ) = TRUE
				AND CASE 
						WHEN (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedClient\')::text IS NOT NULL
							THEN  rv.cid = ANY ((\'{ \' || trim( both \'[]\' from (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedClient\')::text ) || \' }\' ) ::INT[]) 
						WHEN (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text IS NOT NULL OR (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text IS NOT NULL
							THEN  rv.cid = ANY ((\'{ \' || trim( both \'[]\' from (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text ) || \' }\' ) ::INT[])
						ELSE TRUE
					END
			ORDER BY
				rv.major DESC,
				rv.minor DESC
		';

		return self::fetchReportVersions( $strSql, $objDatabase );
	}

	public static function fetchReportVersionsByReportIdsByCid( $arrintReportIds, $intCid, $objDatabase, $intCompanyUserId = NULL ) {
		$strSql = '
			SELECT
				rv.*
			FROM
				report_versions rv
				JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
			WHERE
				rv.cid = ' . ( int ) $intCid . '
				AND rv.report_id IN ( ' . implode( ',', $arrintReportIds ) . ' )
				AND COALESCE((rv.definition ->> \'is_published\')::BOOLEAN, TRUE) = TRUE AND
					(((rv.definition ->> \'validation_callbacks\')::json ->> \'isAllowedClient\')::TEXT IS NULL OR 
					rv.cid = ANY ((\'{ \' || trim(both \'[]\' FROM ((rv.definition ->> \'validation_callbacks\')::json ->> \'isAllowedClient\'):: TEXT) || \' }\')::INT [ ]))
			ORDER BY
				rv.major DESC,
				rv.minor DESC ';

		return self::fetchReportVersions( $strSql, $objDatabase );
	}

	public static function fetchClientTopReportVersionByReportIdByCid( $intReportId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				rv.*
			FROM
				report_versions rv
				JOIN report_instances ri ON rv.id = ri.report_version_id AND rv.cid = ri.cid
			WHERE
				ri.report_id = ' . ( int ) $intReportId . '
				AND rv.cid = ' . ( int ) $intCid . '
				AND ri.deleted_by IS NULL
				AND ri.deleted_on IS NULL
			ORDER BY
				ri.report_version_id DESC
			LIMIT 1';

		return self::fetchReportVersion( $strSql, $objDatabase );
	}

	public static function fetchUsedReportVersionsByReportIdByCid( $intReportId, $intCid, $objDatabase, $intCompanyUserId = NULL ) {
		$strSql = '
			SELECT
				rv.*
			FROM
			( 
				SELECT 
					rv.*
				FROM
					report_versions rv
				WHERE
					rv.cid = ' . ( int ) $intCid . '
					AND rv.report_id = ' . ( int ) $intReportId . '
					AND rv.is_default = TRUE
				UNION
				SELECT
					DISTINCT
					rv.*
				FROM
					report_new_instances ri
					JOIN report_versions rv ON rv.cid = ri.cid AND rv.id = ri.report_version_id
					JOIN report_new_group_instances rgi ON rgi.cid = ri.cid AND rgi.report_new_instance_id = ri.id
					JOIN report_new_groups rg ON rg.cid = ri.cid AND rg.id = rgi.report_new_group_id
				WHERE
					ri.cid = ' . ( int ) $intCid . '
					AND ri.report_id = ' . ( int ) $intReportId . '
					AND rg.report_group_type_id IN ( ' . \CReportGroupType::STANDARD . ', ' . \CReportGroupType::MY_REPORTS . ' ) 
			) AS rv
			JOIN company_users cu ON rv.cid = cu.cid AND cu.id = ' . ( int ) $intCompanyUserId . ' AND
					CASE
						WHEN (rv.definition->>\'allow_only_admin\')::BOOLEAN THEN cu.is_administrator = 1
						ELSE TRUE
					END
					AND
					CASE
						WHEN (rv.definition->>\'allow_only_psi_admin\')::BOOLEAN THEN cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						ELSE cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					END
			WHERE
				rv.cid = ' . ( int ) $intCid . '
				AND rv.report_id = ' . ( int ) $intReportId . '
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND COALESCE( (rv.definition->>\'is_published\')::BOOLEAN, TRUE ) = TRUE
				AND CASE 
						WHEN (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedClient\')::text IS NOT NULL
							THEN  rv.cid = ANY ((\'{ \' || trim( both \'[]\' from (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedClient\')::text ) || \' }\' ) ::INT[]) 
						WHEN (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text IS NOT NULL OR (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text IS NOT NULL
							THEN  rv.cid = ANY ((\'{ \' || trim( both \'[]\' from (( rv.definition ->>\'validation_callbacks\')::json ->> \'isAllowedCid\')::text ) || \' }\' ) ::INT[])
						ELSE TRUE
					END
				ORDER BY
				rv.major DESC,
				rv.minor DESC';

		return self::fetchReportVersions( $strSql, $objDatabase );
	}

}
