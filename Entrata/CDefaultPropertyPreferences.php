<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultPropertyPreferences
 * Do not add any new functions to this class.
 */

class CDefaultPropertyPreferences extends CBaseDefaultPropertyPreferences {

	public static function fetchDefaultPropertyPreference( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultPropertyPreference', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchDefaultPropertyPreferencesByKeys( $arrstrKeys, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;
		$strSql = 'SELECT dpp.*
					FROM default_property_preferences dpp
					WHERE
					dpp.key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		return self::fetchDefaultPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchDefaultPropertySettingValuesBykeys( $arrstrPropertyPreferencesKeys, $objDatabase ) {
		if( false == valArr( $arrstrPropertyPreferencesKeys ) ) {
			return NULL;
		}
		$strSql = ' SELECT 
						dpf.id, dpf.key, dpf.value, psk.id as property_setting_id
					FROM 
						default_property_preferences dpf
					LEFT JOIN property_setting_keys psk ON ( dpf.key = psk.key )
					WHERE dpf.key IN (\'' . implode( "','", $arrstrPropertyPreferencesKeys ) . '\')
					';
		return fetchData( $strSql, $objDatabase );
	}

}
?>