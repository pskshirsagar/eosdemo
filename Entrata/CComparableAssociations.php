<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CComparableAssociations
 * Do not add any new functions to this class.
 */

class CComparableAssociations extends CBaseComparableAssociations {

	public static function fetchComparableAssociationsByDefaultAmenityIdsByCidByPropertyId( $arrintDefaultAmenityId, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valArr( $arrintDefaultAmenityId ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strWhere = ' default_amenity_id IN ( ' . implode( ',', $arrintDefaultAmenityId ) . ' ) AND cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . '';

		$strSql = 'SELECT
						*
					FROM
						comparable_associations
					WHERE ' . $strWhere;

		return self::fetchComparableAssociations( $strSql, $objDatabase );
	}

	public static function fetchComparableAssociationsByUtilityTypeIdsByPropertyIdByCid( $arrintUtilityTypeIds, $intPropertyId, $intCid, $objDatbase ) {
		if( false == valArr( $arrintUtilityTypeIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						comparable_associations ca
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND utility_type_id IN (' . implode( ',', $arrintUtilityTypeIds ) . ') ';

		return self::fetchComparableAssociations( $strSql, $objDatbase );
	}

	public static function fetchComparableAssociationsByComparableRoomTypeIdsByCidByPropertyId( $arrintComparableRoomTypeIds, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valArr( $arrintComparableRoomTypeIds ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = ' SELECT
						ca.*,
						util_get_system_translated( \'internal_description\', da.internal_description, da.details ) as default_amenity_name
					FROM
						comparable_associations as ca
					LEFT JOIN
						default_amenities as da ON ( da.id = ca.default_amenity_id )
					WHERE
						ca.default_amenity_id IS NOT NULL
						AND ca.comparable_room_type_id IN ( ' . implode( ',', $arrintComparableRoomTypeIds ) . ' )
						AND ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						ORDER BY da.internal_description ASC';

		return self::fetchComparableAssociations( $strSql, $objDatabase );
	}

	public static function fetchComparableAssociationsByIdsByCidByPropertyId( $arrintComparableAssociationIds, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valArr( $arrintComparableAssociationIds ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						comparable_associations
					WHERE
						id IN ( ' . implode( ',', $arrintComparableAssociationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchComparableAssociations( $strSql, $objDatabase );
	}

	public static function fetchComparableAssociationByIdByCidByPropertyId( $intComaprableAssociationId, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valId( $intComaprableAssociationId ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						comparable_associations
					WHERE
						id = ' . ( int ) $intComaprableAssociationId . '
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchComparableAssociation( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyAmenitiesComparableAssociationsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						ca.*, a.name as amenity_name
					FROM
						comparable_associations as ca
					JOIN rate_associations as ra
						ON ( ra.id = ca.comparable_reference_id and ra.cid = ca.cid )
					JOIN amenities as a
						ON ( a.id = ra.ar_origin_reference_id and a.cid = ca.cid )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.default_amenity_id IS NULL
						AND ca.utility_type_id IS NULL
						AND ca.comparable_reference_id IS NOT NULL
					ORDER BY ca.order_num';

		return self::fetchComparableAssociations( $strSql, $objDatabase );
	}

	public static function fetchUtilitiesComparableAssociationsByCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						ca.*, u.name as utility_name
					FROM
						comparable_associations as ca
					JOIN property_utilities as pu
						ON ( pu.id = ca.comparable_reference_id and pu.cid = ca.cid )
					JOIN utilities as u
						ON ( u.id = pu.utility_id and u.cid = ca.cid )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.default_amenity_id IS NULL
						AND ca.comparable_reference_id IS NOT NULL
						AND ca.utility_type_id IS NOT NULL
					ORDER BY ca.order_num';

		return self::fetchComparableAssociations( $strSql, $objDatabase );
	}

	public static function fetchComparableAssociationByComparableReferenceIdByCidByPropertyId( $intComparableReferenceId, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valId( $intComparableReferenceId ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						comparable_associations
					WHERE
						comparable_reference_id = ' . ( int ) $intComparableReferenceId . '
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchComparableAssociation( $strSql, $objDatabase );
	}

	public static function fetchComparableAssociationByComparableReferenceIdsByCidByPropertyId( $arrintComparableReferenceId, $intCid, $intPropertyId, $objDatabase ) {
		if( false == valArr( $arrintComparableReferenceId ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						comparable_associations
					WHERE
						comparable_reference_id IN ( ' . implode( ',', $arrintComparableReferenceId ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchComparableAssociations( $strSql, $objDatabase );
	}

}
?>