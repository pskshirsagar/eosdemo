<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetTypes
 * Do not add any new functions to this class.
 */

class CAssetTypes extends CBaseAssetTypes {

	const LAYER1 								= 1;
	const LAYER2								= 2;
	const LAYER3								= 3;

	const TOWER8SERVERROOM						= 1;
	const TOWER9SERVERROOM						= 2;
	const TOWER9HUBROOM							= 3;
	const STOREROOM								= 4;

	const START_OF_CPU_CORE						= 1;
	const TOTAL_DAYS_FOR_EXPIRY_STATUS			= 30;
	const END_OF_CPU_CORE						= 256;

	public static $c_arrintSwitchTypesForAssetTypeSwitch = [
		self::LAYER1	=> 'Layer 1',
		self::LAYER2 	=> 'Layer 2',
		self::LAYER3 	=> 'Layer 3',

	];

	public static $c_arrintLocationTypeIdsForAssetTypes = [
		self::TOWER8SERVERROOM	=> 'Tower 8 Server room',
		self::TOWER9SERVERROOM	=> 'Tower 9 Server Room',
		self::TOWER9HUBROOM		=> 'Tower 9 Hub Room',
		self::STOREROOM			=> 'Store Room',
	];

	public static function fetchAssetTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CAssetType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAssetType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CAssetType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedAssetTypes( $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						asset_types
					WHERE
						is_published = TRUE
					ORDER BY
						order_num';

		return self::fetchAssetTypes( $strSql, $objClientDatabase );
	}

	public static function fetchPublishedAssetTypesByIds( $arrintAssetTypeIds, $objClientDatabase ) {
		if( false == ( $arrintAssetTypeIds = getIntValuesFromArr( $arrintAssetTypeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						asset_types
					WHERE
						is_published IS TRUE
						AND id IN ( ' . sqlIntImplode( $arrintAssetTypeIds ) . ')
					ORDER BY
						order_num';

		return fetchdata( $strSql, $objClientDatabase );
	}

}
?>