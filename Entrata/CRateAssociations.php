<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRateAssociations
 * Do not add any new functions to this class.
 */

class CRateAssociations extends CBaseRateAssociations {

	public static function fetchRateAssociations( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, [ 'CRateAssociation', 'createConcreteRateAssociation' ], $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchRateAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, [ 'CRateAssociation', 'createConcreteRateAssociation' ], $objDatabase );
	}

	public static function fetchArOriginReferenceIdByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchRateAssociation( sprintf( 'SELECT ar_origin_reference_id FROM rate_associations WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRateAssociationsByArOriginReferenceIdsByCid( $arrintArOriginReferenceIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintArOriginReferenceIds ) ) {
			return NULL;
		}

		$strSql = '
					SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_reference_id IN ( ' . implode( ',', $arrintArOriginReferenceIds ) . ' )';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchRateAssociationsByArCascadeIdByArOriginIdByPropertyIdByCid( $intArCascadeId, $intArOriginId, $intPropertyId, $intCid, $objDatabase, $intArOriginReferenceId = NULL ) {

		$strArOriginReferenceCondition = '';
		if( false == is_null( $intArOriginReferenceId ) ) {
			$strArOriginReferenceCondition = 'AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId;
		}

		$strSql = '
			SELECT
				ra.*
			FROM
				rate_associations ra
			WHERE
				ra.cid = ' . ( int ) $intCid . '
				AND ra.property_id = ' . ( int ) $intPropertyId . '
				AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
				AND ra.ar_origin_id = ' . ( int ) $intArOriginId . '
				' . $strArOriginReferenceCondition;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchRateAssociationsByArCascadeIdByArOriginIdByArOriginReferenceIdByCid( $intArCascadeId, $intArOriginId, $intArOriginReferenceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_origin_id = ' . ( int ) $intArOriginId . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchRateAssociationsByArCascadeReferenceIdByArOriginIdByArOriginReferenceIdsByCid( $intArCascadeReferenceId, $intArOriginId, $arrintArOriginReferenceIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId . '
						AND ra.ar_origin_id = ' . ( int ) $intArOriginId . '
						AND ra.ar_origin_reference_id IN ( ' . implode( ',', $arrintArOriginReferenceIds ) . ' )';

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchRateAssociationsByArCascadeIdByArOriginIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, $intArOriginId, $intArOriginReferenceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ra.*
					FROM
						rate_associations ra
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_origin_id = ' . ( int ) $intArOriginId . '
						AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId;

		return self::fetchRateAssociations( $strSql, $objDatabase );
	}

	public static function fetchRateAssociationCountByArCascadeIdByArOriginIdByPropertyIdByCid( $intArCascadeId, $intArOriginId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) )		return NULL;

		$strSql = 'SELECT
						DISTINCT property_id
					FROM
						rate_associations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ar_origin_id = ' . ( int ) $intArOriginId;

		$arrintResponseData = rekeyArray( 'property_id', ( array ) fetchData( $strSql, $objDatabase ) );

		$arrintPetsAssociated = [];
		foreach( $arrintPropertyIds as $intPropertyId ) {
			$arrintPetsAssociated[$intPropertyId] = ( ( true == valArr( $arrintResponseData ) && true == isset( $arrintResponseData[$intPropertyId] ) ) ? 1 : 0 );
		}

		return $arrintPetsAssociated;
	}

	public static function fetchRateAssociationByArOriginIdByArOriginReferenceIdByPropertyByCidByArCascadeReferenceIds( $intArOriginId, $intArOriginReferenceId, $intPropertyId, $intCid, $arrintArCascadeReferenceIds, $objDatabase ) {

		$strSql = '
			SELECT
				ra.*
			FROM
				rate_associations ra
			WHERE
				ra.cid = ' . ( int ) $intCid . '
				AND ra.property_id = ' . ( int ) $intPropertyId . '
				AND ra.ar_origin_id = ' . ( int ) $intArOriginId . '
				AND ra.ar_origin_reference_id = ' . ( int ) $intArOriginReferenceId . '
				AND (  ( ra.ar_cascade_id = ' . CArCascade::SPACE . ' AND ra.ar_cascade_reference_id = ' . $arrintArCascadeReferenceIds['unit_space_id'] . ' )
						OR ( ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND ra.ar_cascade_reference_id = ' . $arrintArCascadeReferenceIds['unit_type_id'] . ' )
						OR ( ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' AND ra.ar_cascade_reference_id = ' . $arrintArCascadeReferenceIds['property_floorplan_id'] . ')
						OR ( ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND ra.ar_cascade_reference_id = ' . $arrintArCascadeReferenceIds['property_id'] . ')
					)';
		return self::fetchRateAssociation( $strSql, $objDatabase );
	}

	public static function fetchAmenitiesRateAssociationsByPropertyUnitIdByPropertyIdByCid( $intPropertyUnitId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valId( $intPropertyUnitId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						a.name as amenities_name,
					    arc.name as cascade_name,
					    ra.ar_cascade_id
					FROM 
					    rate_associations ra
					    JOIN amenities a ON (a.cid = ra.cid AND a.id = ra.ar_origin_reference_id)
					    JOIN ar_cascades arc ON (arc.id = ra.ar_cascade_id)
					    JOIN unit_spaces us ON (ra.cid = us.cid AND ra.property_id = us.property_id AND ra.ar_cascade_reference_id IN (us.property_floorplan_id,
					    us.unit_type_id, us.property_id, us.id))
					WHERE 
					    ra.cid = ' . ( int ) $intCid . ' AND
					    ra.property_id = ' . ( int ) $intPropertyId . ' AND
					    us.property_unit_id = ' . ( int ) $intPropertyUnitId . '
					GROUP BY 
					    a.name,
					    arc.name,
					    ra.ar_cascade_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
