<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyBadges
 * Do not add any new functions to this class.
 */

class CCompanyBadges extends CBaseCompanyBadges {

	public static function fetchCompanyBadgeByCompanyActivityPointIdByCid( $intCompanyActivityPointId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_badges
					WHERE
						company_activity_point_id = ' . ( int ) $intCompanyActivityPointId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyBadge( $strSql, $objDatabase );
	}

	public static function fetchCompanyBadgesByCidByCustomerId( $intCid, $intCustomerId, $objDatabase ) {
		$strSql = 'SELECT
						cob.id,
						cob.name,
						CASE
							WHEN 1 = cb.is_pro_award
							THEN cob.pro_description
							ELSE cob.base_description
						END AS description,
						CASE
							WHEN 0 = cb.is_pro_award
							THEN cob.pro_criteria
							ELSE cob.base_criteria
						END AS criteria,
						cb.customer_id,
						cb.is_pro_award,
						CASE
							WHEN cb.customer_id IS NULL
							THEN 0
							ELSE 1
						END AS is_customer_badge,
						cob.file_handle,
						COUNT(cap.id) AS progress
					FROM
						company_badges cob
						JOIN default_badges db ON ( cob.default_badge_id = db.id AND cob.cid = ' . ( int ) $intCid . ' )
						JOIN company_activity_points coap ON ( cob.company_activity_point_id = coap.id AND cob.cid = coap.cid )
						LEFT JOIN customer_badges cb ON ( cob.cid = cb.cid AND cob.id = cb.company_badge_id AND cb.customer_id = ' . ( int ) $intCustomerId . ' )
						LEFT JOIN customer_activity_points cap ON ( cap.company_activity_point_id = coap.id AND cap.cid = coap.cid AND cap.cid = ' . ( int ) $intCid . ' AND cap.customer_id = ' . ( int ) $intCustomerId . ')
					WHERE
						cob.display_to_all = 1
						AND cob.display_to_customer = 1
						AND db.is_published = 1
						AND cob.is_published = 1
					GROUP BY
						cob.id,
						cob.name,
						cob.pro_description,
						cob.base_description,
						cob.pro_criteria,
						cob.base_criteria,
						cb.customer_id,
						cb.is_pro_award,
						cob.file_handle,
						cob.order_num
					ORDER BY
						cob.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEnabledCompanyBadgesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cb.*
					FROM
						company_badges cb
						JOIN default_badges db ON ( cb.default_badge_id = db.id )
					WHERE
						cb.cid = ' . ( int ) $intCid . '
						AND db.is_published != 0
					ORDER BY
						cb.order_num';

		return self::fetchCompanyBadges( $strSql, $objDatabase );
	}

	public static function fetchCachedCompanyBadgesByCid( $intCid, $objDatabase ) {
		$arrmixCompanyBadges = CCache::fetchObject( 'CompanyBadges_' . $intCid );

		if( false === $arrmixCompanyBadges || false == valArr( $arrmixCompanyBadges ) ) {
			$arrmixCompanyBadges = self::fetchCompanyBadgesByCid( $intCid, $objDatabase );
			CCache::storeObject( 'CompanyBadges_' . $intCid, $arrmixCompanyBadges, $intSpecificLifetime = 900, [] );
		}

		return $arrmixCompanyBadges;
	}

}
