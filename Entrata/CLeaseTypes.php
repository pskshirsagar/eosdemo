<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseTypes
 * Do not add any new functions to this class.
 */

class CLeaseTypes extends CBaseLeaseTypes {

	public static function fetchLeaseTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CLeaseType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchLeaseType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CLeaseType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedLeaseTypes( $objDatabase ) {
		return self::fetchLeaseTypes( 'SELECT * FROM lease_types WHERE is_published = true ORDER BY order_num', $objDatabase );
	}

	public static function fetchConflictingNameLeaseType( $objConflictingLeaseType, $objDatabase ) {

		$intId = ( false == is_numeric( $objConflictingLeaseType->getId() ) ) ? 0 : $objConflictingLeaseType->getId();

		$strSql = 'SELECT
						*
				   FROM
						lease_types
				   WHERE
						id <> ' . ( int ) $intId . '
						AND name = \'' . ( string ) addslashes( $objConflictingLeaseType->getName() ) . '\'
						LIMIT 1';

		return self::fetchLeaseType( $strSql, $objDatabase );
	}

	public static function fetchLeaseTypeNameById( $intId, $objDatabase ) {
		$strSql = '	SELECT
						name
					FROM
						lease_types
					WHERE
						id = ' . ( int ) $intId . '
					LIMIT 1';

		return parent::fetchColumn( $strSql, 'name', $objDatabase );
	}

}
?>