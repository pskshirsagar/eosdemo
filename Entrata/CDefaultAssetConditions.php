<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultAssetConditions
 * Do not add any new functions to this class.
 */

class CDefaultAssetConditions extends CBaseDefaultAssetConditions {

	public static function fetchDefaultAssetConditions( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultAssetCondition::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultAssetCondition( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultAssetCondition::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>