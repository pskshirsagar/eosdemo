<?php

use Psi\Eos\Entrata\CBudgetGlAccountMonths;

class CBudget extends CBaseBudget {

	protected $m_strPropertyName;
	protected $m_strApprovalNote;
	protected $m_intCurrentUserId;

	const IS_DEFAULT = 1;

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getCurrentUserId() {
		return $this->m_intCurrentUserId;
	}

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setCurrentUserId( $intCurrentUserId ) {
		$this->m_intCurrentUserId = $intCurrentUserId;
	}

	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_name'] ) : $arrmixValues['property_name'] );
	}

	/**
	 * Validate Functions
	 */

	public function valName( $objClientDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Budget Name is required.' ) ) );
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND id <> ' . ( int ) $this->getId() . '
						AND property_id = ' . ( int ) $this->getPropertyId() . '
						AND name ILIKE \'' . addslashes( $this->getName() ) . '\'';

		if( 0 < ( int ) \Psi\Eos\Entrata\CBudgets::createService()->fetchBudgetCount( $strWhere, $objClientDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Budget name is already exist for \'' . $this->getPropertyName() . '\' property.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFiscalYear() {

		$boolIsValid = true;

		if( true == is_null( $this->getFiscalYear() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fiscal_year', __( 'Fiscal Year is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start Date is required.' ) ) );
		}

		if( strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start Date Should be less than End Date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {

		$boolIsValid = true;

		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End Date is required.' ) ) );
		}

		$strStartDate			= date_create( $this->getStartDate() );
		$strEndDate 			= date_create( $this->getEndDate() );
		$objInterval 			= date_diff( $strStartDate, $strEndDate );
		$intIntervalInYears 	= $objInterval->format( '%y years' );
		$intIntervalInMonths 	= $objInterval->format( '%m months' );

		$intTotalMonths = ( 12 * $intIntervalInYears ) + $intIntervalInMonths + 1;

		if( 3 > $intTotalMonths ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'At least 3 months is required.' ) ) );
		}

		if( 24 < $intTotalMonths ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'Maximum 24 month\'s allowed.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBudgetWorkbookId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getBudgetWorkbookId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'budget_workbook_id', __( 'Budget Workbook is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valExportBudgetName() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Budget name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valFiscalYear();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			case 'advanced_budget':
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valBudgetWorkbookId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				break;

			case 'export_budget':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valExportBudgetName();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createBudgetGlAccountMonth() {

		$objBudgetGlAccountMonth = new CBudgetGlAccountMonth();

		$objBudgetGlAccountMonth->setCid( $this->getCid() );
		$objBudgetGlAccountMonth->setBudgetId( $this->getId() );
		$objBudgetGlAccountMonth->setPropertyId( $this->getPropertyId() );

		return $objBudgetGlAccountMonth;
	}

	public function createBudgetAssumption() {

		$objBudgetAssumption = new CBudgetAssumption();
		$objBudgetAssumption->setCid( $this->getCid() );
		$objBudgetAssumption->setBudgetId( $this->getId() );

		return $objBudgetAssumption;
	}

	public function createBudgetGlAccountActivity() {

		$objBudgetGlAccountActivity = new CBudgetGlAccountActivity();

		$objBudgetGlAccountActivity->setCid( $this->getCid() );
		$objBudgetGlAccountActivity->setBudgetId( $this->getId() );

		return $objBudgetGlAccountActivity;
	}

	public function createBudgetTabGlAccountItem() {

		$objBudgetTabGlAccountItem = new CBudgetTabGlAccountItem();

		$objBudgetTabGlAccountItem->setCid( $this->getCid() );
		$objBudgetTabGlAccountItem->setBudgetId( $this->getId() );

		return $objBudgetTabGlAccountItem;
	}

	public function createBudgetTabGlAccountItemMonth() {

		$objBudgetTabGlAccountItemMonth = new CBudgetTabGlAccountItemMonth();

		$objBudgetTabGlAccountItemMonth->setCid( $this->getCid() );
		$objBudgetTabGlAccountItemMonth->setBudgetId( $this->getId() );

		return $objBudgetTabGlAccountItemMonth;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchBudgetGlAccountMonths( $objClientDatabase, $objBudgetFilter = NULL, $intPropertyId = NULL, $boolIsViewCurrent = false ) {
		return CBudgetGlAccountMonths::createService()->fetchBudgetGlAccountMonthsByCidByBudgetId( $this->getCid(), $this->getId(), $objBudgetFilter, $objClientDatabase, $intPropertyId, $boolIsViewCurrent );
	}

	public function fetchBudgetGlAccountMonthsByPropertyId( $objClientDatabase, $objBudgetsFilter = NULL, $boolShowDisabledData = false ) {
		$this->m_arrmixPropertyGroupIds	= rekeyArray( 'property_group_id', ( array ) \Psi\Eos\Entrata\CPropertyGroupAssociations::createService()->fetchPropertyGroupAssociationsByCidByPropertyIds( $this->getCid(), [ $this->getPropertyId() ], $objClientDatabase, $boolShowDisabledData ) );
		return CBudgetGlAccountMonths::createService()->fetchBudgetGlAccountMonthsByCidByBudgetIdByPropertyGroupIds( $this->getCid(), $this->getId(), array_keys( $this->m_arrmixPropertyGroupIds ), $objClientDatabase, $objBudgetsFilter );
	}

	public function fetchBudgetGlAccountMonthsByGlAccountIdsByPostMonths( $arrintGlAccountIds, $arrintGlAccountMonths, $objClientDatabase ) {
		return CBudgetGlAccountMonths::createService()->fetchBudgetGlAccountMonthsByCidByBudgetIdByGlAccountIdsByPostMonths( $this->getCid(), $this->getId(), $arrintGlAccountIds, $arrintGlAccountMonths, $objClientDatabase );
	}

	public function fetchGlDetailsByGlAccountIds( $arrintGlAccountIds, $objClientDatabase ) {
		return CGlDetails::fetchGlDetailsByPropertyIdByStartDateByEndDateByGlAccountIdsByCid( $this->getPropertyId(), $this->getStartDate(), $this->getEndDate(), $arrintGlAccountIds, $this->getCid(), $objClientDatabase );
	}

	public function fetchBudgetGlAccountMonthsByPostMonths( $arrstrPostMonths, $objClientDatabase ) {
		return CBudgetGlAccountMonths::createService()->fetchBudgetGlAccountMonthsByCidByBudgetIdByPostMonths( $this->getCid(), $this->getId(), $arrstrPostMonths, $objClientDatabase );
	}

	public function fetchGlDetails( $objClientDatabase, $arrmixParameters = [] ) {
		return CGlDetails::fetchGlDetailsByPropertyIdByBudgetIdByStartDateByEndDateByCid( $this->getPropertyId(), $this->getId(), $this->getStartDate(), $this->getEndDate(), $this->getCid(), $objClientDatabase, $arrmixParameters );
	}

	public function fetchBudgetGlAccountGroupTotals( $objClientDatabase ) {
		return CBudgetGlAccountMonths::createService()->fetchGlAccountGroupTotalsByCidByBudgetId( $this->getCid(), $this->getId(), $objClientDatabase );
	}

}
?>