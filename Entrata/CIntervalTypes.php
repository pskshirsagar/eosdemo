<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntervalTypes
 * Do not add any new functions to this class.
 */

class CIntervalTypes extends CBaseIntervalTypes {

    public static function fetchIntervalTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CIntervalType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchIntervalType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CIntervalType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchCustomIntervalTypeNameById( $intIntervalTypeId, $objDatabase ) {
    	$strSql = 'SELECT name from interval_types WHERE id = ' . ( int ) $intIntervalTypeId;
    	return fetchData( $strSql, $objDatabase );
    }

}
?>