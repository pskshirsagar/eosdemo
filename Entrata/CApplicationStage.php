<?php

class CApplicationStage extends CBaseApplicationStage {

	const PRE_APPLICATION	= 1;
	const PRE_QUALIFICATION	= 2;
	const APPLICATION		= 3;
	const LEASE				= 4;

	const CUSTOM_STAGE_SCREENING 				= 'SCREENING';
	const CUSTOM_STAGE_SELECT_TERM 				= 'SELECT_TERM';
	const CUSTOM_STAGE_TRANSFER					= 'TRANSFER';
	const CUSTOM_STAGE_TRANSFER_MOVE_IN			= 'TRANSFER_MOVE_IN';
	const CUSTOM_STAGE_TRANSFER_MOVE_OUT		= 'TRANSFER_MOVE_OUT';
	const CUSTOM_STAGE_CERTIFICATION_CHECKLIST	= 'CERTIFICATION_CHECKLIST';
	const CUSTOM_STAGE_CERTIFICATION			= 'Certification (Affordable)';
	const CUSTOM_STAGE_ELIGIBILITY				= 'Eligibility';
	const CUSTOM_STAGE_EXTENSION				= 'EXTENSION';

	protected $m_arrstrRenewalApplicationStages;
	protected $m_arrstrTransferApplicationStages;
	protected $m_arrstrExtensionApplicationStages;

	public static $c_arrstrCustomApplicationStages = [ self::CUSTOM_STAGE_SCREENING, self::CUSTOM_STAGE_SELECT_TERM ];

	public static $c_arrstrProspectApplicationStages = [
		self::PRE_APPLICATION	=> 'Guest Card',
		self::PRE_QUALIFICATION	=> 'Pre-Qualification',
		self::APPLICATION		=> 'Application',
		self::LEASE				=> 'Lease'
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getProspectApplicationStages() {
		$arrstrProspectApplicationStages = [
			self::PRE_APPLICATION	=> __( 'Guest Card' ),
			self::PRE_QUALIFICATION	=> __( 'Pre-Qualification' ),
			self::APPLICATION		=> __( 'Application' ),
			self::LEASE				=> __( 'Lease' )
		];

		return $arrstrProspectApplicationStages;
	}

	public function getApplicationStages() {
		$arrstrApplicationStages = [
			self::PRE_APPLICATION	=> __( 'Guest Card' ),
			self::PRE_QUALIFICATION	=> __( 'Application' ),
			self::APPLICATION		=> __( 'Application' )
		];
		return $arrstrApplicationStages;
	}

	public function getAffordableApplicationStages( $strKey ) {

		$arrstrAffordableApplicationStages = [
			self::CUSTOM_STAGE_CERTIFICATION	=> __( 'Certification (Affordable)' ),
			self::CUSTOM_STAGE_ELIGIBILITY	=> __( 'Eligibility' )
		];
		return $arrstrAffordableApplicationStages[$strKey];

	}

	public function getProspectApplicationStagesWithOccupancyTypes() {
		$arrstrProspectApplicationStagesWithOccupancyType = [
			COccupancyType::CONVENTIONAL => [ self::PRE_APPLICATION => __( 'Guest Card' ), self::PRE_QUALIFICATION => __( 'Pre-Qualification' ), self::APPLICATION => __( 'Application' ), self::LEASE => __( 'Lease' ) ],
		];

		return $arrstrProspectApplicationStagesWithOccupancyType;
	}

	public function getCertificationStagesWithOccupancyTypes() {
		$arrstrCertificationStagesWithOccupancyType = [
			COccupancyType::AFFORDABLE	 => [ self::PRE_APPLICATION => __( 'Eligibility' ), self::PRE_QUALIFICATION => __( 'Pre-Qualification' ), self::APPLICATION => __( 'Certification' ), self::LEASE => __( 'Certification' ) ]
		];

		return $arrstrCertificationStagesWithOccupancyType;
	}

	public static $c_arrstrRenewalApplicationStages = [
		self::PRE_APPLICATION	=> 'Renewal Proposal',
		self::APPLICATION		=> 'Renewal Offer',
		self::LEASE				=> 'Renewal Lease'
	];

	public function getRenewalApplicationStages() {

		if( true == valArr( $this->m_arrstrRenewalApplicationStages ) ) {
			return $this->m_arrstrRenewalApplicationStages;
		}

		$this->m_arrstrRenewalApplicationStages = [
			self::PRE_APPLICATION	=> __( 'Renewal Proposal' ),
			self::APPLICATION		=> __( 'Renewal Offer' ),
			self::LEASE				=> __( 'Renewal Lease' )
		];

		return $this->m_arrstrRenewalApplicationStages;
	}

	public static $c_arrstrTransferApplicationStages = [
		self::APPLICATION	=> 'Transfer Request',
		self::LEASE			=> 'Transfer Lease'
	];

	public function getTransferApplicationStages() {

		if( true == valArr( $this->m_arrstrTransferApplicationStages ) ) {
			return $this->m_arrstrTransferApplicationStages;
		}

		$this->m_arrstrTransferApplicationStages = [
			self::APPLICATION	=> __( 'Transfer Request' ),
			self::LEASE			=> __( 'Transfer Lease' )
		];

		return $this->m_arrstrTransferApplicationStages;
	}

	public function getExtensionApplicationStages() {

		if( true == valArr( $this->m_arrstrExtensionApplicationStages ) ) return $this->m_arrstrExtensionApplicationStages;

		$this->m_arrstrExtensionApplicationStages = [
			self::APPLICATION	=> __( 'Extension Offer' ),
			self::LEASE			=> __( 'Extension Lease' )
		];

		return $this->m_arrstrExtensionApplicationStages;
	}

	public function getLeaseModificationApplicationStages() {
		$arrstrLeaseModificationApplicationStages = [
			self::APPLICATION	=> __( 'Modification Application' ),
			self::LEASE			=> __( 'Modification Lease' )
		];

		return $arrstrLeaseModificationApplicationStages;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function applicationStageIdtoStr( $intLeaseIntervalTypeId, $intApplicationStageId, $intOccupancyTypeId, $intApplicationId = NULL, $intCid = NULL, $objDatabase = NULL ) {
		switch( $intLeaseIntervalTypeId ) {

			case CLeaseIntervalType::APPLICATION:
				if( true == array_key_exists( $intOccupancyTypeId, $this->getProspectApplicationStagesWithOccupancyTypes() ) ) {
					return $this->getProspectApplicationStagesWithOccupancyTypes()[$intOccupancyTypeId][$intApplicationStageId];
				} else {
					return $this->getProspectApplicationStagesWithOccupancyTypes()[COccupancyType::CONVENTIONAL][$intApplicationStageId];
				}
				break;

			case CLeaseIntervalType::RENEWAL:
				if( COccupancyType::AFFORDABLE == $intOccupancyTypeId && true == array_key_exists( $intOccupancyTypeId, $this->getProspectApplicationStagesWithOccupancyTypes() ) ) {
					return $this->getProspectApplicationStagesWithOccupancyTypes()[$intOccupancyTypeId][$intApplicationStageId];
				} elseif( true == array_key_exists( $intApplicationStageId, $this->getRenewalApplicationStages() ) ) {

					return $this->getRenewalApplicationStages()[$intApplicationStageId];
				}
				break;

			case CLeaseIntervalType::TRANSFER:
				if( true == array_key_exists( $intApplicationStageId, $this->getTransferApplicationStages() ) ) {
					return $this->getTransferApplicationStages()[$intApplicationStageId];
				}
				break;

			case CLeaseIntervalType::EXTENSION:
				if( true == array_key_exists( $intApplicationStageId, $this->getExtensionApplicationStages() ) ) return $this->getExtensionApplicationStages()[$intApplicationStageId];
				break;

			case CLeaseIntervalType::LEASE_MODIFICATION:
				if( COccupancyType::AFFORDABLE == $intOccupancyTypeId && true == valId( $intApplicationId ) && true == valId( $intCid ) && true == valObj( $objDatabase, 'CDatabase' ) ) {
					$objSubsidyCertification = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchActiveSubsidyCertificationByApplicantionIdByCid( $intApplicationId, $intCid, $objDatabase, NULL, false );
					if( true == valObj( $objSubsidyCertification, 'CSubsidyCertification' ) ) {
						return CSubsidyCertificationType::getNameById( $objSubsidyCertification->getSubsidyCertificationTypeId(), true );
					} elseif( true == array_key_exists( $intApplicationStageId, $this->getLeaseModificationApplicationStages() ) ) {
						return $this->getLeaseModificationApplicationStages()[$intApplicationStageId];
					}
				} elseif( true == array_key_exists( $intApplicationStageId, $this->getLeaseModificationApplicationStages() ) ) {
					return $this->getLeaseModificationApplicationStages()[$intApplicationStageId];
				}
				break;

			default:
				// default case
				break;
		}
	}

	/** below function was added to manage affordable side certification status changes which will show right corner status like Certification: certification status */
	public function getCertificationStageStatus( $intOccupancyTypeId, $intApplicationStageId, $intApplicationId = NULL, $intCid = NULL, $objDatabase = NULL ) {
		$objSubsidyCertification = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchActiveSubsidyCertificationByApplicantionIdByCid( $intApplicationId, $intCid, $objDatabase, NULL, false );
		if( true == valObj( $objSubsidyCertification, 'CSubsidyCertification' ) && in_array( $objSubsidyCertification->getSubsidyCertificationTypeId(), [ CSubsidyCertificationType::GROSS_RENT_CHANGE, CSubsidyCertificationType::UNIT_TRANSFER, CSubsidyCertificationType::INTERIM_RECERTIFICATION ] ) ) {
			return CSubsidyCertificationType::getNameById( $objSubsidyCertification->getSubsidyCertificationTypeId(), true );
		} else {
			return $this->getCertificationStagesWithOccupancyTypes()[$intOccupancyTypeId][$intApplicationStageId];
		}
	}

}
?>
