<?php

class CApPaymentType extends CBaseApPaymentType {

	const WIRE_TRANSFER		= 1;
	const CHECK				= 2;
	const DEBIT_CARD		= 3;
	const CREDIT_CARD		= 4;
	const ACH				= 5;
	const CASH				= 6;
	const WRITTEN_CHECK		= 7;
	const BILL_PAY_ACH		= 8;
	const BILL_PAY_CHECK	= 9;
	const AVID_PAY			= 10;
	const WFPM				= 11;
	const FAST_FUNDS		= 14;
	const THIRD_PARTY_API	= 15;
	const CAPITAL_ONE		= 16;
	const VIRTUAL_CARD		= 17;
	const NOT_INTEGRATED	= 0;

	const BILL_PAY	= 100; // Adding dummy constant to show on interface only. /* @Fixme : This hack was added by utility team, we should get rid of this and variable & its relative code. */

	private $m_boolIsEnabled;
	private $m_boolIsConsolidatePayment;

	public static $c_arrstrApPaymentTypes = [
		self::CASH			=> 'Cash',
		self::DEBIT_CARD	=> 'Debit Card',
		self::WIRE_TRANSFER	=> 'Wire Transfer',
		self::WRITTEN_CHECK	=> 'Written Check'
	];

	public static $c_arrstrAllApPaymentTypes = [
		self::CASH				=> 'Cash',
		self::DEBIT_CARD		=> 'Debit Card',
		self::WIRE_TRANSFER		=> 'Wire Transfer',
		self::WRITTEN_CHECK		=> 'Written Check',
		self::CHECK				=> 'Check',
		self::ACH				=> 'ACH',
		self::BILL_PAY_ACH		=> 'Bill Pay ACH',
		self::BILL_PAY_CHECK	=> 'Bill Pay Check',
		self::AVID_PAY			=> 'Avid Pay',
		self::CREDIT_CARD		=> 'Credit Card',
		self::CAPITAL_ONE       => 'Capital One',
		self::VIRTUAL_CARD      => 'Virtual Card'
	];

	public static $c_arrstrAllApPaymentRemittanceTypes = [
		self::CASH				=> 'Cash',
		self::DEBIT_CARD		=> 'Debit Card',
		self::WIRE_TRANSFER		=> 'Wire Transfer',
		self::WRITTEN_CHECK		=> 'Written Check',
		self::CHECK				=> 'Check',
		self::ACH				=> 'ACH (eCheck)',
		self::BILL_PAY_ACH		=> 'Bill Pay',
		self::AVID_PAY			=> 'AvidPay',
		self::CREDIT_CARD		=> 'Credit Card',
		self::VIRTUAL_CARD      => 'Virtual Card'
	];

	public static $c_arrintAllApPaymentTypes = [ self::WIRE_TRANSFER, self::CHECK, self::DEBIT_CARD, self::CREDIT_CARD, self::ACH, self::CASH, self::WRITTEN_CHECK, self::BILL_PAY_ACH, self::BILL_PAY_CHECK, self::AVID_PAY, self::VIRTUAL_CARD ];

	public static $c_arrintDownloadableApPaymentTypes = [ self::ACH, self::AVID_PAY, self::WFPM, self::CAPITAL_ONE ];

	public static $c_arrstrPaymentFileTypes = [
		self::NOT_INTEGRATED => 'Not Integrated',
		self::WFPM => 'Wells Fargo',
		self::CAPITAL_ONE => 'Capital One'
	];

	public static $c_arrintApPaymentTypeIds = [ CApPaymentType::CHECK, CApPaymentType::BILL_PAY_ACH, CApPaymentType::BILL_PAY, CApPaymentType::BILL_PAY_CHECK, CApPaymentType::WRITTEN_CHECK, CApPaymentType::AVID_PAY, CApPaymentType::WFPM ];

	public function __construct() {
		parent::__construct();
		$this->m_boolIsEnabled = false;
		$this->m_boolIsConsolidatePayment = false;
		return;
	}

	/**
	 * Get Functions
	 */

	public function getIsEnabled() {
		return $this->m_boolIsEnabled;
	}

	public function getIsConsolidatePayment() {
		return $this->m_boolIsConsolidatePayment;
	}

	public static function getDummyBillPayPaymentType() {
		$objApPaymentType = new CApPaymentType();

		$objApPaymentType->setDefaults();
		$objApPaymentType->setId( SELF::BILL_PAY );
		$objApPaymentType->setName( 'Bill Pay' );
		$objApPaymentType->setDescription( 'Bill Pay' );
		$objApPaymentType->setIsPublished( 1 );

		return $objApPaymentType;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['is_consolidate_payment'] ) && $boolDirectSet ) {
			$this->m_boolIsConsolidatePayment = trim( $arrmixValues['is_consolidate_payment'] );
		} elseif( isset( $arrmixValues['is_consolidate_payment'] ) ) {
			$this->setIsConsolidatePayment( $arrmixValues['is_consolidate_payment'] );
		}

		if( isset( $arrmixValues['is_enabled'] ) && $boolDirectSet ) {
			$this->m_boolIsEnabled = trim( $arrmixValues['is_enabled'] );
		} else if( isset( $arrmixValues['is_enabled'] ) ) {
			$this->setIsEnabled( $arrmixValues['is_enabled'] );
		}
	}

	public function setIsConsolidatePayment( $boolIsConsolidatePayment ) {
		$this->m_boolIsConsolidatePayment = $boolIsConsolidatePayment;
	}

	public function setIsEnabled( $boolIsEnabled ) {
		$this->m_boolIsEnabled = $boolIsEnabled;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function isElectronicPayment( $intApPaymentTypeId ) {

		// TODO:: Not sure about payment type WIRE. Will modify it later.
		if( true == in_array( $intApPaymentTypeId, [ self::DEBIT_CARD, self::ACH, self::CREDIT_CARD ] ) ) {
			return true;
		} else {
			return false;
		}
	}

	public static function loadApPaymentTypes() {

		$arrstrApPaymentTypesNameArray	= [];

		$arrstrApPaymentTypesNameArray[self::ACH]				= 'ACH (eCheck)';
		$arrstrApPaymentTypesNameArray[self::CASH]				= 'Cash';
		$arrstrApPaymentTypesNameArray[self::CHECK]				= 'Check';
		$arrstrApPaymentTypesNameArray[self::DEBIT_CARD]		= 'Debit Card';
		$arrstrApPaymentTypesNameArray[self::CREDIT_CARD]		= 'Credit Card';
		$arrstrApPaymentTypesNameArray[self::WIRE_TRANSFER]		= 'Wire Transfer';
		$arrstrApPaymentTypesNameArray[self::WRITTEN_CHECK]		= 'Written Check';
		$arrstrApPaymentTypesNameArray[self::BILL_PAY]			= 'Bill Pay';
		$arrstrApPaymentTypesNameArray[self::AVID_PAY]			= 'Avid Pay';
		$arrstrApPaymentTypesNameArray[self::WFPM]				= 'WFPM';
		$arrstrApPaymentTypesNameArray[self::BILL_PAY_CHECK]	= 'Bill Pay Check';
		$arrstrApPaymentTypesNameArray[self::BILL_PAY_ACH]		= 'Bill Pay ACH';
		return $arrstrApPaymentTypesNameArray;
	}

	public static function loadVendorAccessApPaymentTypes() {
		return $arrstrVendorAccessApPaymentTypes = [
			self::ACH => 'ACH (eCheck)',
			self::CASH => 'Cash',
			self::CHECK => 'Check',
			self::CREDIT_CARD => 'Credit Card',
			self::DEBIT_CARD => 'Debit Card',
			self::VIRTUAL_CARD => 'Virtual Card',
			self::WIRE_TRANSFER => 'Wire Transfer'
		];

	}

	public static function getPaymentTypeNameById( $intApPaymentTypeId ) {
		switch( $intApPaymentTypeId ) {
			case self::ACH:
				return 'ACH (eCheck)';
				break;

			case self::CASH:
				return 'Cash';
				break;

			case self::CHECK:
				return 'Check';
				break;

			case self::DEBIT_CARD:
				return 'Debit Card';
				break;

			case self::CREDIT_CARD:
				return 'Credit Card';
				break;

			case self::WIRE_TRANSFER:
				return 'Wire Transfer';
				break;

			case self::BILL_PAY:
				return 'Bill Pay';
				break;

			case self::WRITTEN_CHECK:
				return 'Written Check';
				break;

			case self::AVID_PAY:
				return 'Avid Pay';
			break;

			case self::WFPM:
				return 'WFPM';
				break;

			default:
				// default case
				break;
		}
	}

}
?>
