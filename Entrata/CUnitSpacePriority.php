<?php

class CUnitSpacePriority extends CBaseUnitSpacePriority {

	protected $m_strFloorplanName;
	protected $m_strUnitNumber;
	protected $m_strSpaceNumber;
	protected $m_strUnitTypeName;
	protected $m_boolIsUnitAvailable;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitSpaceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPriorityOrder() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
        		break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Get Functions
     */

    public function getFloorplanName() {
    	return $this->m_strFloorplanName;
    }

    public function getUnitNumber() {
    	return $this->m_strUnitNumber;
    }

    public function getSpaceNumber() {
    	return $this->m_strSpaceNumber;
    }

    public function getUnitTypeName() {
    	return $this->m_strUnitTypeName;
    }

    public function getIsUnitAvailable() {
    	return $this->m_boolIsUnitAvailable;
    }

    /**
     * Set Functions
     */

    public function setFloorplanName( $strFloorplanName ) {
    	$this->m_strFloorplanName = $strFloorplanName;
    }

    public function setUnitNumber( $strUnitNumber ) {
    	$this->m_strUnitNumber = $strUnitNumber;
    }

    public function setSpaceNumber( $strSpaceNumber ) {
    	$this->m_strSpaceNumber = $strSpaceNumber;
    }

    public function setUnitTypeName( $strUnitTypeName ) {
    	$this->m_strUnitTypeName = $strUnitTypeName;
    }

    public function setIsUnitAvailable( $boolIsUnitAvailable ) {
    	$this->m_boolIsUnitAvailable = $boolIsUnitAvailable;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['floorplan_name'] ) ) $this->setFloorplanName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['floorplan_name'] ) : $arrValues['floorplan_name'] );
    	if( true == isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
    	if( true == isset( $arrValues['space_number'] ) ) $this->setSpaceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['space_number'] ) : $arrValues['space_number'] );
    	if( true == isset( $arrValues['unit_type_name'] ) ) $this->setUnitTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_type_name'] ) : $arrValues['unit_type_name'] );
    	if( true == isset( $arrValues['is_unit_available'] ) ) $this->setIsUnitAvailable( $arrValues['is_unit_available'] );
    }
}
?>