<?php

class CEvent extends CBaseEvent {

	protected $m_boolIscheckAndReOpenApplication;

	protected $m_intUserId;
	protected $m_intEventReferenceTypeId;
	protected $m_intCustomerTypeId;
	protected $m_intLeasingAgentId;
	protected $m_intRoommateGroupId;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_strEventResultName;
	protected $m_strEventTypeName;
	protected $m_intEventReferenceReferenceId;
	protected $m_intListTypeId;
	protected $m_intListItemId;
	protected $m_intIsEventResultRequired;
	protected $m_intAvailableUnitCount;
	protected $m_intIsPropertyTourOnly;
	protected $m_intIsResultOfContactRequired;
	protected $m_intCanSelectUnit;
	protected $m_intCustomersCount;
	protected $m_intScreeningDecisionTypeId;
	protected $m_intCallTypeId;
	protected $m_intCallStatusTypeId;
	protected $m_intCallResultId;
	protected $m_intCallDuration;

	protected $m_objOldReferenceData;
	protected $m_objNewReferenceData;

	protected $m_objCompanyUser;
	protected $m_objCompanyEmployee;
	protected $m_objReference;
	protected $m_objApplicant;
	protected $m_objEmailEvent;
	protected $m_objEventCall;
	protected $m_objEventChat;
	protected $m_objEmailDatabase;

	protected $m_strMobileNumber;
	protected $m_strEmailAddress;
	protected $m_strFirstName;
	protected $m_strLastName;
	protected $m_strCustomerCompanyName;
	protected $m_strFromEmailAddress;
	protected $m_strMitsEventTypeName;
	protected $m_strGuestUserName;
	protected $m_strNoticeDeliveryMethod;
	protected $m_strAssignedUnitsDisplayText;

	protected $m_arrstrLogFields;
	protected $m_arrintUnitSpaceIds;
	protected $m_arrintUnitNumber;

	protected $m_arrobjEventReferences;

	protected $m_strPropertyTimeZone;
	protected $m_strCustomerIds;
	protected $m_strEventIds;

	protected $m_strPackageReassociatedFrom;
	protected $m_strPackageReassociatedTo;

	protected $m_boolIsFulfilled;
	protected $m_boolDoNotTriggerEvents;
	protected $m_strNameFull;
	protected $m_strTagName;
	protected $m_intCreatorUserTypeId;
	protected $m_strCompanyUsername;

	const MAX_MINUTES_TO_PROCESS_SYSTEM_EMAIL = 30;

	public function __construct() {
		parent::__construct();

		$this->m_boolIscheckAndReOpenApplication = true;

		return;
	}

	/**
	 * Create Functions
	 */

	public function createEventReference() {

		$objEventReference = new CEventReference();
		$objEventReference->setCid( $this->getCid() );
		$objEventReference->setEventId( $this->getId() );

		return $objEventReference;
	}

	/**
	 * Add Functions
	 */

	public function addEventReference( $objEventReference ) {
		$this->m_arrobjEventReferences[$objEventReference->getId()] = $objEventReference;
	}

	/**
	 * Set Functions
	 */

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setCompanyUser( $objCompanyUser ) {
		$this->m_objCompanyUser = $objCompanyUser;
	}

	public function setCompanyEmployee( $objCompanyEmployee ) {
		$this->m_objCompanyEmployee = $objCompanyEmployee;
		$this->setCompanyEmployeeId( $objCompanyEmployee->getId() );
	}

	public function setReference( $objReference ) {
		$this->m_objReference = $objReference;
	}

	public function setEventReferences( $arrobjEventReferences ) {
		$this->m_arrobjEventReferences = $arrobjEventReferences;
	}

	public function setPackageReassociatedFrom( $strCustomerName ) {
		$this->m_strPackageReassociatedFrom = $strCustomerName;
	}

	public function setPackageReassociatedTo( $strCustomerName ) {
		$this->m_strPackageReassociatedTo = $strCustomerName;
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->m_strMobileNumber = $strMobileNumber;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setFirstName( $strFirstName ) {
		$this->m_strFirstName = $strFirstName;
	}

	public function setLastName( $strLastName ) {
		$this->m_strLastName = $strLastName;
	}

	public function setCustomerCompanyName( $strCustomerCompanyName ) {
		$this->m_strCustomerCompanyName = $strCustomerCompanyName;
	}

	public function setEventReferenceTypeId( $intEventReferenceTypeId ) {
		$this->m_intEventReferenceTypeId = $intEventReferenceTypeId;
	}

	public function setOldReferenceData( $objOldReferenceData ) {
			$this->m_objOldReferenceData = $objOldReferenceData;
	}

	public function setNewReferenceData( $objNewReferenceData ) {
			$this->m_objNewReferenceData = $objNewReferenceData;
	}

	public function setUnitSpaceIds( $arrintUnitSpaceIds ) {
			$this->m_arrintUnitSpaceIds = $arrintUnitSpaceIds;
	}

	public function setLogFields( $arrstrLogFields ) {
		$this->m_arrstrLogFields = $arrstrLogFields;
	}

	public function setEventDatetime( $strEventDatetime ) {
		parent::setEventDatetime( $strEventDatetime );
		if( false == valStr( $this->getScheduledDatetime() ) ) {
			$this->setScheduledDatetime( $this->getEventDatetime() );
		}
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->m_intCustomerTypeId = $intCustomerTypeId;
	}

	public function setLeasingAgentId( $intLeasingAgentId ) {
		$this->m_intLeasingAgentId = $intLeasingAgentId;
	}

	public function setRoommateGroupId( $intRoommateGroupId ) {
		$this->m_intRoommateGroupId = $intRoommateGroupId;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setEventResultName( $strEventResultName ) {
		$this->m_strEventResultName = $strEventResultName;
	}

	public function setApplicant( $objApplicant ) {
		$this->m_objApplicant = $objApplicant;
	}

	public function setEmailEvent( $objEmailEvent ) {
		$this->m_objEmailEvent = $objEmailEvent;
	}

	public function setEventCall( $objEventCall ) {
		$this->m_objEventCall = $objEventCall;
		$this->setDetailsField( 'event_call', $objEventCall->toArray() );
	}

	public function setEventChat( $objEventChat ) {
		$this->m_objEventChat = $objEventChat;
		$this->setDetailsField( 'event_chat', $objEventChat->toArray() );
	}

	public function setEventTypeName( $strEventTypeName ) {
		$this->m_strEventTypeName = $strEventTypeName;
	}

	public function setEventReferenceReferenceId( $intEventReferenceReferenceId ) {
		$this->m_intEventReferenceReferenceId = $intEventReferenceReferenceId;
	}

	public function setListTypeId( $intListTypeId ) {
		$this->m_intListTypeId = $intListTypeId;
	}

	public function setListItemId( $intListItemId ) {
		$this->m_intListItemId = $intListItemId;
	}

	public function setIscheckAndReOpenApplication( $boolIscheckAndReOpenApplication ) {
		$this->m_boolIscheckAndReOpenApplication = $boolIscheckAndReOpenApplication;
	}

	public function setNoticeDeliveryMethod( $strNoticeDeliveryMethod ) {
		$this->m_strNoticeDeliveryMethod = $strNoticeDeliveryMethod;
	}

	public function setAssignedUnitsDisplayText( $strAssignedUnitDisplayText ) {
		$this->m_strAssignedUnitsDisplayText = $strAssignedUnitDisplayText;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['user_id'] ) ) 						$this->setUserId( $arrmixValues['user_id'] );
		if( true == isset( $arrmixValues['mobile_number'] ) ) 					$this->setMobileNumber( $arrmixValues['mobile_number'] );
		if( true == isset( $arrmixValues['email_address'] ) ) 					$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['reference_type_name'] ) )				$this->setReferenceTypeName( $arrmixValues['reference_type_name'] );
		if( true == isset( $arrmixValues['name_first'] ) )						$this->setFirstName( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) 						$this->setLastName( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['customer_company_name'] ) ) 			$this->setCustomerCompanyName( $arrmixValues['customer_company_name'] );
		if( true == isset( $arrmixValues['event_reference_type_id'] ) ) 		$this->setEventReferenceTypeId( $arrmixValues['event_reference_type_id'] );
		if( true == isset( $arrmixValues['customer_type_id'] ) ) 				$this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
		if( true == isset( $arrmixValues['leasing_agent_id'] ) ) 				$this->setLeasingAgentId( $arrmixValues['leasing_agent_id'] );
		if( true == isset( $arrmixValues['roommate_group_id'] ) ) 				$this->setRoommateGroupId( $arrmixValues['roommate_group_id'] );
		if( true == isset( $arrmixValues['applicant_id'] ) ) 					$this->setApplicantId( $arrmixValues['applicant_id'] );
		if( true == isset( $arrmixValues['application_id'] ) ) 					$this->setApplicationId( $arrmixValues['application_id'] );
		if( true == isset( $arrmixValues['event_result_name'] ) ) 				$this->setEventResultName( $arrmixValues['event_result_name'] );
		if( true == isset( $arrmixValues['event_type_name'] ) ) 				$this->setEventTypeName( $arrmixValues['event_type_name'] );
		if( true == isset( $arrmixValues['event_reference_reference_id'] ) )	$this->setEventReferenceReferenceId( $arrmixValues['event_reference_reference_id'] );
		if( true == isset( $arrmixValues['list_type_id'] ) )					$this->setListTypeId( $arrmixValues['list_type_id'] );
		if( true == isset( $arrmixValues['list_item_id'] ) )					$this->setListItemId( $arrmixValues['list_item_id'] );
		if( true == isset( $arrmixValues['is_event_esult_required'] ) )			$this->setIsEventResultRequired( $arrmixValues['is_event_esult_required'] );
		if( true == isset( $arrmixValues['unit_number'] ) )						$this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['available_unit_count'] ) )			$this->setAvailableUnitCount( $arrmixValues['available_unit_count'] );
		if( true == isset( $arrmixValues['customer_ids'] ) )					$this->setCustomerIds( $arrmixValues['customer_ids'] );
		if( true == isset( $arrmixValues['event_ids'] ) )						$this->setEventIds( $arrmixValues['event_ids'] );
		if( true == isset( $arrmixValues['customer_count'] ) )					$this->setCustomersCount( $arrmixValues['customer_count'] );
		if( true == isset( $arrmixValues['is_fulfilled'] ) )					$this->setIsFulfilled( $arrmixValues['is_fulfilled'] );
		if( true == isset( $arrmixValues['do_not_trigger_events'] ) )           $this->setDoNotTriggerEvents( $arrmixValues['do_not_trigger_events'] );
		// FIXME :Added the hotfixes for API. will remove this once the lead management team removes @translate from events.notes [ task#1926275]
		if( true == isset( $arrmixValues['temp_notes'] ) )                      $this->setNotes( stripslashes( $arrmixValues['temp_notes'] ) );
		if( true == isset( $arrmixValues['call_result_id'] ) )                  $this->setCallResultId( $arrmixValues['call_result_id'] );
		if( true == isset( $arrmixValues['call_status_type_id'] ) )             $this->setCallStatusTypeId( $arrmixValues['call_status_type_id'] );
		if( true == isset( $arrmixValues['call_type_id'] ) )                    $this->setCallTypeId( $arrmixValues['call_type_id'] );
		if( true == isset( $arrmixValues['call_duration'] ) )                   $this->setCallDuration( $arrmixValues['call_duration'] );
		if( true == isset( $arrmixValues['full_name'] ) )                   	$this->setNameFull( $arrmixValues['full_name'] );
		if( true == isset( $arrmixValues['tag'] ) )                   	        $this->setTagName( $arrmixValues['tag'] );
		if( true == isset( $arrmixValues['creator_user_type_id'] ) )            $this->setCreatorUserTypeId( $arrmixValues['creator_user_type_id'] );
		if( true == isset( $arrmixValues['company_username'] ) )                $this->setCompanyUsername( $arrmixValues['company_username'] );

	}

	public function setPropertyTimeZone( $strPropertyTimeZone ) {
		$this->m_strPropertyTimeZone = $strPropertyTimeZone;
	}

	public function setIsEventResultRequired( $intIsEventResultRequired ) {
		$this->m_intIsEventResultRequired = $intIsEventResultRequired;
	}

	public function setUnitNumber( $arrintUnitNumber ) {
		$this->m_arrintUnitNumber = $arrintUnitNumber;
	}

	public function setAvailableUnitCount( $intAvailableUnitCount ) {
		$this->m_intAvailableUnitCount = $intAvailableUnitCount;
	}

	public function setIsPropertyTourOnly( $intIsPropertyTourOnly ) {
		$this->m_intIsPropertyTourOnly = $intIsPropertyTourOnly;
	}

	public function setIsResultOfContactRequired( $intIsResultOfContactRequired ) {
		$this->m_intIsResultOfContactRequired = $intIsResultOfContactRequired;
	}

	public function setCanSelectUnit( $intCanSelectUnit ) {
		$this->m_intCanSelectUnit = $intCanSelectUnit;
	}

	public function setMitsEventTypeName( $strMitsEventTypeName ) {
		$this->m_strMitsEventTypeName = $strMitsEventTypeName;
	}

	public function setCustomersCount( $intCustomerCount ) {
		$this->m_intCustomersCount = $intCustomerCount;
	}

	public function setGuestUserName( $strGuestUserName ) {
		$this->m_strGuestUserName = $strGuestUserName;
	}

	public function setCustomerIds( $arrintCustomerIds ) {
		if( true == valArr( $arrintCustomerIds ) ) {
			$this->m_strCustomerIds = $this->flattenIds( $arrintCustomerIds );
		} elseif( false == empty( $arrintCustomerIds ) ) {
			$this->m_strCustomerIds = $arrintCustomerIds;
		} else {
			$this->m_strCustomerIds = NULL;
		}
	}

	public function setEventIds( $arrintEventIds ) {
		if( true == valArr( $arrintEventIds ) ) {
			$this->m_strEventIds = $this->flattenIds( $arrintEventIds );
		} elseif( false == empty( $arrintEventIds ) ) {
			$this->m_strEventIds = $arrintEventIds;
		} else {
			$this->m_strEventIds = NULL;
		}
	}

	public function setScreeningDecisionTypeId( $intScreeningDecisionTypeId ) {
		$this->m_intScreeningDecisionTypeId = $intScreeningDecisionTypeId;
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->m_strFromEmailAddress = $strFromEmailAddress;
	}

	public function setIsFulfilled( $boolIsFulfilled ) {
		$this->m_boolIsFulfilled = CStrings::strToBool( $boolIsFulfilled );
	}

	public function setEmailDatabase( $objEmailDatabase ) {
		$this->m_objEmailDatabase = $objEmailDatabase;
	}

	public function setCreatedCustomerId( $intCreatedCustomerId ) {
		$this->setDetailsField( 'created_customer_id', $intCreatedCustomerId );
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setTagName( $strTagName ) {
		$this->m_strTagName = $strTagName;
	}

	public function setCreatorUserTypeId( $intCreatorUserTypeId ) {
		$this->m_intCreatorUserTypeId = $intCreatorUserTypeId;
	}

	public function setCompanyUsername( $strCompanyUsername ) {
		$this->m_strCompanyUsername = $strCompanyUsername;
	}

	/**
	 * Get or fetch Functions
	 */

// 	function getOrFetchCompanyUser( $objDatabase ) {

// 		if( true == is_null( $this->getCompanyUserId() ) || false == is_numeric( $this->getCompanyUserId() ) ) return NULL;

// 		if( false == valObj( $this->m_objCompanyUser, 'CCompanyUser' ) ) {
// 			$this->m_objCompanyUser = $this->fetchCompanyUser( $objDatabase );
// 		}

// 		return $this->m_objCompanyUser;
// 	}

	public function getEventDisplay( $arrmixCustomVariables = NULL ) {
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES, false );
		$arrstrDetails = json_decode( json_encode( $this->getDetailsField( [] ) ?? new stdClass() ), true );
		// remove <strong> tags from data
		$arrstrDetails['display_data']['user_name'] = $this->stripStrongTag( $arrstrDetails['display_data']['user_name'] );
		$arrstrDetails['display_data']['company_user_name'] = $this->stripStrongTag( $arrstrDetails['display_data']['company_user_name'] );

		$strDisplayDataKey = 'display_data';
		if( true == valArr( $arrstrDetails[$strDisplayDataKey] ) && \Psi\Libraries\UtilStrings\CStrings::createService()->endsWith( $arrstrDetails[$strDisplayDataKey]['_template'], '.tpl' ) ) {
			$objSmarty->assign( 'event', $this );
			$objSmarty->assign( 'details', $arrstrDetails );
			$objSmarty->assign( 'full_path', PATH_INTERFACES_ENTRATA );
			$objSmarty->assign( 'custom_variables', $arrmixCustomVariables );
			return $objSmarty->fetch( PATH_INTERFACES_ENTRATA . $arrstrDetails[$strDisplayDataKey]['_template'] );

		} else {
			return $this->stripStrongTag( $this->m_strEventHandle );
		}
	}

	public function getNotesDisplay() {
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES, false );
		$arrstrDetails = json_decode( json_encode( $this->getDetailsField( [] ) ?? new stdClass() ), true );
		$strDisplayDataKey = 'display_data_system_notes';
		if( valArr( $arrstrDetails[$strDisplayDataKey] )
		    && true == array_key_exists( '_template', $arrstrDetails[$strDisplayDataKey] )
		    && \Psi\Libraries\UtilStrings\CStrings::createService()->endsWith( $arrstrDetails[$strDisplayDataKey]['_template'], '.tpl' ) ) {
			$objSmarty->assign( 'event', $this );
			$objSmarty->assign( 'details', $arrstrDetails );
			return $objSmarty->fetch( PATH_INTERFACES_ENTRATA . $arrstrDetails[$strDisplayDataKey]['_template'] );

		} else {
			return $this->m_strNotes;
		}
	}

	public function getTitleDisplay() {
		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES, false );
		$arrstrDetails = json_decode( json_encode( $this->getDetailsField( [] ) ?? new stdClass() ), true );
		$strDisplayDataKey = 'display_data_title';
		if( valArr( $arrstrDetails[$strDisplayDataKey] )
		    && true == array_key_exists( '_template', $arrstrDetails[$strDisplayDataKey] )
		    && \Psi\Libraries\UtilStrings\CStrings::createService()->endsWith( $arrstrDetails[$strDisplayDataKey]['_template'], '.tpl' ) ) {
			$objSmarty->assign( 'event', $this );
			$objSmarty->assign( 'details', $arrstrDetails );
			$objSmarty->assign( 'full_path', PATH_INTERFACES_ENTRATA );
			return $objSmarty->fetch( PATH_INTERFACES_ENTRATA . $arrstrDetails[$strDisplayDataKey]['_template'] );
		} else {
			return $this->m_strTitle;
		}
	}

	public function getTitle() {
		return $this->getTitleDisplay();
	}

	public function getNotes() {
		return $this->getNotesDisplay();
	}

	public function getEventHandle() {
		return $this->getEventDisplay();
	}

	public function getAssignedUnitsDisplayText() {
		return $this->m_strAssignedUnitsDisplayText;
	}

	public function getOrFetchCompanyEmployee( $objDatabase ) {

		if( true == is_null( $this->getCompanyEmployeeId() ) || false == is_numeric( $this->getCompanyEmployeeId() ) ) return NULL;

		if( false == valObj( $this->m_objCompanyEmployee, 'CCompanyEmployee' ) ) {
			$this->m_objCompanyEmployee = $this->fetchCompanyEmployee( $objDatabase );
		}

		return $this->m_objCompanyEmployee;
	}

	public function getOrFetchEventReferences( $objDatabase ) {

		if( false == valArr( $this->m_arrobjEventReferences ) ) {
			$this->m_arrobjEventReferences = $this->fetchEventReferences( $objDatabase );
		}

		return $this->m_arrobjEventReferences;
	}

	public function getOrFetchUnitSpaceIds( $objDatabase ) {

		if( false == valArr( $this->m_arrintUnitSpaceIds ) ) {
			$this->m_arrintUnitSpaceIds = $this->fetchUnitSpaceIds( $objDatabase );
		}

		return $this->m_arrintUnitSpaceIds;
	}

	/**
	 * Get Functions
	 */

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getFirstName() {
		return $this->m_strFirstName;
	}

	public function getLastName() {
		return $this->m_strLastName;
	}

	public function getCustomerCompanyName() {
		return $this->m_strCustomerCompanyName;
	}

	public function getEventReferenceTypeId() {
		return $this->m_intEventReferenceTypeId;
	}

	public function getCompanyUser() {
		return $this->m_objCompanyUser;
	}

	public function getCompanyEmployee() {
		return $this->m_objCompanyEmployee;
	}

	public function getReference() {
		return $this->m_objReference;
	}

	public function getEventReferences() {
		return $this->m_arrobjEventReferences;
	}

	public function getOldReferenceData() {
		return $this->m_objOldReferenceData;
	}

	public function getEmailEvent() {
		return $this->m_objEmailEvent;
	}

	public function getEventCall() {
		if( true == valObj( $this->m_objEventCall, 'CEventCall' ) ) {
			return $this->m_objEventCall;
		}
		$this->m_objEventCall = new CEventCall();
		$this->m_objEventCall->setValues( ( array ) $this->getDetailsField( 'event_call' ) );

		return $this->m_objEventCall;
	}

	public function getEventChat() {
		if( true == valObj( $this->m_objEventChat, 'CEventChat' ) ) {
			return $this->m_objEventChat;
		}

		$this->m_objEventChat = new CEventChat();
		$this->m_objEventChat->setValues( $this->getDetails()->event_chat );

		return $this->m_objEventChat;
	}

	public function getNewReferenceData() {
		return $this->m_objNewReferenceData;
	}

	public function getLogFields() {
		return $this->m_arrstrLogFields;
	}

	public function getUnitSpaceIds() {
		return $this->m_arrintUnitSpaceIds;
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function getLeasingAgentId() {
		return $this->m_intLeasingAgentId;
	}

	public function getRoommateGroupId() {
		return $this->m_intRoommateGroupId;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getEventResultName() {
		return $this->m_strEventResultName;
	}

	public function getApplicant() {
		return $this->m_objApplicant;
	}

	public function getPropertyTimeZone() {
		return $this->m_strPropertyTimeZone;
	}

	public function getEventTypeName() {
		return $this->m_strEventTypeName;
	}

	public function getEventReferenceReferenceId() {
		return $this->m_intEventReferenceReferenceId;
	}

	public function getListTypeId() {
		return $this->m_intListTypeId;
	}

	public function getListItemId() {
		return $this->m_intListItemId;
	}

	public function getIscheckAndReOpenApplication() {
		return $this->m_boolIscheckAndReOpenApplication;
	}

	public function getIsEventResultRequired() {
		return $this->m_intIsEventResultRequired;
	}

	public function getUnitNumber() {
		return $this->m_arrintUnitNumber;
	}

	public function getAvailableUnitCount() {
		return $this->m_intAvailableUnitCount;
	}

	public function getIsPropertyTourOnly() {
		return $this->m_intIsPropertyTourOnly;
	}

	public function getIsResultOfContactRequired() {
		return $this->m_intIsResultOfContactRequired;
	}

	public function getCanSelectUnit() {
		return $this->m_intCanSelectUnit;
	}

	public function getMitsEventTypeName() {
		return $this->m_strMitsEventTypeName;
	}

	public function getCustomersCount() {
		return $this->m_intCustomersCount;
	}

	public function getGuestUserName() {
		return $this->m_strGuestUserName;
	}

	public function getNoticeDeliveryMethod() {
		return $this->m_strNoticeDeliveryMethod;
	}

	public function getCustomerIds() {
		if( true == valArr( $this->m_strCustomerIds ) ) {
			return $this->m_strCustomerIds;
		} elseif( false == empty( $this->m_strCustomerIds ) ) {
			return $this->expandIds( $this->m_strCustomerIds );
		} else {
			return [];
		}
	}

	public function getEventIds() {
		if( true == valArr( $this->m_strEventIds ) ) {
			return $this->m_strEventIds;
		} elseif( false == empty( $this->m_strEventIds ) ) {
			return $this->expandIds( $this->m_strEventIds );
		} else {
			return [];
		}
	}

	public function getPackageReassociatedFrom() {
		return $this->m_strPackageReassociatedFrom;
	}

	public function getPackageReassociatedTo() {
		return $this->m_strPackageReassociatedTo;
	}

	public function getScreeningDecisionTypeId() {
		return $this->m_intScreeningDecisionTypeId;
	}

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function getIsFulfilled() {
		return $this->m_boolIsFulfilled;
	}

	public function getEmailDatabase() {
		return $this->m_objEmailDatabase;
	}

	public function getCreatedCustomerId() {
		return $this->getDetailsField( 'created_customer_id' );
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getTagName() {
		return ( true == valStr( $this->m_strTagName ) ) ? $this->m_strTagName : __( 'Profile Activity' );
	}

	public function getSanitizedTags( $arrstrTags ) {
		$arrstrSanitizedTags = [];
		if( true == valArr( $arrstrTags ) ) {
			foreach( $arrstrTags as $strTag ) {
				$arrstrSanitizedTags[] = trim( $strTag, '\'"' );
			}
		}
		return $arrstrSanitizedTags;
	}

	public function getCreatorUserTypeId() {
		return $this->m_intCreatorUserTypeId;
	}

	public function getCompanyUsername() {
		return $this->m_strCompanyUsername;
	}

	/**
	 * fetch Functions
	 */

// 	function fetchCompanyUser( $objDatabase ) {

// 		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $this->getCompanyUserId(), $this->getCid(), $objDatabase );
// 	}

	public function fetchCompanyEmployee( $objDatabase ) {

		return \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
	}

	public function fetchEventReferences( $objDatabase ) {

		return \Psi\Eos\Entrata\CEventReferences::createService()->fetchEventReferencesByEventIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpaceIds( $objDatabase ) {
		$arrobjEventReferences 	= [];
		$arrintUnitSpaceIds 		= [];

		$arrobjEventReferences = \Psi\Eos\Entrata\CEventReferences::createService()->fetchEventReferencesByEventIdByEventReferenceTypeIdByCid( $this->getId(), CEventReferenceType::UNIT_SPACE, $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjEventReferences ) ) {
			foreach( $arrobjEventReferences as $objEventReference ) {
				if( false == is_null( $objEventReference->getReferenceId() ) ) {
					$arrintUnitSpaceIds[] = $objEventReference->getReferenceId();
				}
			}
		}

		return $arrintUnitSpaceIds;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEventTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getEventTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_type_id', __( 'Event type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEventResultId() {
		$boolIsValid = true;

		if( true == in_array( $this->getEventTypeId(), CEventType::$c_arrintContactEventTypeIds ) && false == valId( $this->getEventResultId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_result_id', __( 'Event result is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventDatetime() {
		$boolIsValid = true;
		if( CEventType::ONLINE_GUEST_CARD != $this->getEventTypeId() ) {
			if( true == is_null( $this->getEventDatetime() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_datetime', __( 'Event Datetime is required' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valEventStartDate() {
		$boolIsValid = true;

		$arrstrStartDateParts = explode( ' ',  $this->getEventDatetime() );
		$strDate = trim( $arrstrStartDateParts[0] );
		$strTime = trim( $arrstrStartDateParts[1] );

		if( true == empty( $strDate ) || false == CValidation::checkDateFormat( $strDate, $boolFormat = false ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Valid Date is required.' ), NULL ) );
			$boolIsValid &= false;
		}

		if( 0 == preg_match( '/^(0[1-9]|1[0-2]):(0[0-9]|[0-5][0-9])(pm|am)$/i', trim( $strTime ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Event start time should be in (HH:MMam / HH:MMpm) format.' ) ) );
			$boolIsValid &= false;
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		$strCurrentDateByTimeZone = date( 'm/d/Y h:ia', strtotime( getConvertedDateTime( date( 'm/d/Y h:ia' ), $strFormat = 'm/d/Y h:ia', 'America/Denver', 'America/Denver' ) ) );

		if( strtotime( $this->getEventDatetime() ) > strtotime( $strCurrentDateByTimeZone ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Please select date other than future date.' ) ), NULL );
			$boolIsValid &= false;
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valActivity( $arrmixValidateParams ) {
		$boolIsValid = true;
		$arrstrStartDateParts = explode( ' ',  $this->getEventDatetime() );

		if( false == valArr( $arrstrStartDateParts ) ) {
			$boolIsValid &= false;
		}

		if( true == in_array( $this->getEventTypeId(), CEventType::$c_arrintTourEventTypeIds ) && false == $this->getIsPropertyTourOnly() ) {
			if( 0 == $this->getAvailableUnitCount() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_result_id', __( 'Unit can not be selected for Tour since No Unit is available for Visit.' ), NULL ) );
			}

			if( true == $arrmixValidateParams['boolCanAddEditUnit'] && false == valArr( $this->getUnitSpaceIds() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_result_id', __( 'Unit is Required.' ), NULL ) );
			}
		}
		return $boolIsValid;
	}

	public function valEventHandle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		if( true == is_null( $this->getNotes() )
			|| __( 'Add Note' ) == $this->getNotes()
			|| __( 'Add a Description' ) == $this->getNotes()
			|| __( 'Add Delinquency Note' ) == $this->getNotes() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', __( 'Note is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledDatetime() {
		$boolIsValid = true;

		if( 0 < strlen( $this->getScheduledDatetime() ) && strtotime( date( 'Y-m-d', strtotime( $this->getScheduledDatetime() ) ) ) < strtotime( date( 'Y-m-d' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_datetime', __( 'Appointment date must be in future.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixValidateParams = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valEventTypeId();
				$boolIsValid &= $this->valEventDatetime();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_event_notes':
				$boolIsValid &= $this->valNotes();
				break;

			case 'validate_vendor_activity':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valEventTypeId();
				$boolIsValid &= $this->valEventDatetime();
				if( CEventType::VENDOR_NOTES != $this->getEventTypeId() ) {
					$boolIsValid &= $this->valEventStartDate();
				}
				$boolIsValid &= $this->valNotes();
				break;

			case 'validate_application_event_history_insert':
				$boolIsValid &= $this->valEventStartDate();
				$boolIsValid &= $this->valActivity( $arrmixValidateParams );
				$boolIsValid &= $this->valNotes();
				$boolIsValid &= $this->valEventResultId();
				break;

			case 'validate_resident_event_history_insert':
			case 'validate_resident_event_history_update':
				$boolIsValid &= $this->valEventStartDate();
				$boolIsValid &= $this->valNotes();
				break;

			case 'validate_application_event_history_update':
				if( true == in_array( $this->getEventTypeId(), [ CEventType::CALL_INCOMING, CEventType::CALL_OUTGOING, CEventType::EMAIL_INCOMING, CEventType::EMAIL_OUTGOING,  CEventType::SMS_INCOMING, CEventType::SMS_OUTGOING, CEventType::NOTES, CEventType::ONSITE_VISIT, CEventType::ONLINE_GUEST_CARD, CEventType::TOUR, CEventType::SELF_GUIDED_TOUR, CEventType::VIRTUAL_TOUR ] ) ) {
					$boolIsValid &= $this->valNotes();
				}

				if( CEventType::ONLINE_GUEST_CARD != $this->getEventTypeId() ) {
					$boolIsValid &= $this->valEventStartDate();
				}

				$boolIsValid &= $this->valEventResultId();
				$boolIsValid &= $this->valActivity( $arrmixValidateParams );
				break;

			case 'validate_scheduled_date':
				$boolIsValid &= $this->valScheduledDatetime();
				break;

			case 'validate_cancel_missed_calendar_appointment':
				$boolIsValid &= $this->valNotes();
				$boolIsValid &= $this->valEventResultId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function flattenIds( $arrintIds ) {
		if ( false == valArr( $arrintIds ) ) return '';
		return implode( ',', $arrintIds );
	}

	public function expandIds( $strIds ) {
		return array_filter( explode( ',', $strIds ) );
	}

	private function export( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {

		$this->refreshField( 'remote_primary_key', 'events', $objDatabase );

		if( true == valStr( $this->getRemotePrimaryKey() ) || true == $this->getDoNotExport() ) return true;

		if( true == is_null( $this->getLeaseIntervalId() ) ) {
			return true;
		}

		$objApplication = CApplications::fetchApplicationByLeaseIntervalIdByPropertyIdByCid( $this->getLeaseIntervalId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) || ( false == valStr( $objApplication->getAppRemotePrimaryKey() ) && false == valStr( $objApplication->getGuestRemotePrimaryKey() ) ) ) return true;

		$objProperty = $objApplication->getOrFetchProperty( $objDatabase );

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) return true;

		// Check if application is completed then it should first integrate and then events.
		// We should not integrate submit application event before creating applicant into external system.
		$objPrimaryApplicantApplication = $objApplication->getOrFetchPrimaryApplicantApplication( $objDatabase );
		if( true == valObj( $objPrimaryApplicantApplication, 'CApplicantApplication' ) && true == valStr( $objPrimaryApplicantApplication->getCompletedOn() ) && false == valStr( $objApplication->getAppRemotePrimaryKey() ) ) return true;
		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( true == in_array( $this->getEventTypeId(), [ CEventType::SELF_GUIDED_TOUR, CEventType::VIRTUAL_TOUR ] ) && valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::MRI, CIntegrationClientType::REAL_PAGE_API ] ) ) {
			return true;
		}

		if( CEventType::APPLICATION_PROGRESS != $this->getEventTypeId() && true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::YARDI_RPORTAL == $objIntegrationDatabase->getIntegrationClientTypeId() ) {
			return $objApplication->exportUpdate( $intCompanyUserId, $objDatabase );
		}

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::YARDI, CIntegrationClientType::MRI, CIntegrationClientType::AMSI, CIntegrationClientType::REAL_PAGE_API ] )
		    || false == in_array( $this->getEventTypeId(), \Psi\Eos\Entrata\CEventTypes::createService()->fetchAllIntegratedEventTypeIds( $objDatabase ) ) ) {
			return true;
		}

		$objIntegrationClient = $objIntegrationDatabase->fetchIntegrationClientByIntegrationServiceId( CIntegrationService::SEND_EVENT, $objDatabase );

		if( true == is_null( $this->getCreatedOn() ) ) $this->refreshField( 'created_on', 'events', $objDatabase );

		if( true == valObj( $objIntegrationClient, 'CIntegrationClient' ) && true == valStr( $objIntegrationClient->getSyncFromDate() ) && strtotime( $objIntegrationClient->getSyncFromDate() ) > strtotime( $this->getCreatedOn() ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::SEND_EVENT, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setEvent( $this );
		$objGenericWorker->setApplication( $objApplication );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		if( false == is_null( $intIntegrationSyncTypeId ) ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportUpdate( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false ) {

		if( false == valStr( $this->getRemotePrimaryKey() ) ) if( false == $this->exportStack( $intCompanyUserId, $objDatabase ) ) return false;

		$this->refreshField( 'remote_primary_key', 'events', $objDatabase );

		if( false == valStr( $this->getRemotePrimaryKey() ) ) return false;

		if( true == is_null( $this->getLeaseIntervalId() ) ) {
			return true;
		}

		$objApplication = CApplications::fetchApplicationByLeaseIntervalIdByPropertyIdByCid( $this->getLeaseIntervalId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) || ( false == valStr( $objApplication->getAppRemotePrimaryKey() ) && false == valStr( $objApplication->getGuestRemotePrimaryKey() ) ) ) {
			return true;
		}

		$objProperty = $objApplication->getOrFetchProperty( $objDatabase );

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) return true;

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::YARDI, CIntegrationClientType::MRI, CIntegrationClientType::REAL_PAGE_API ] ) ) {
			return true;
		}

		if( true == in_array( $this->getEventTypeId(), [ CEventType::SELF_GUIDED_TOUR, CEventType::VIRTUAL_TOUR ] ) && valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::MRI, CIntegrationClientType::REAL_PAGE_API ] ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $objProperty->getId(), $this->getCid(), CIntegrationService::UPDATE_EVENT, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setEvent( $this );
		$objGenericWorker->setApplication( $objApplication );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportStack( $intCompanyUserId, $objDatabase, $arrobjEvents = NULL, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {

		if( false == valArr( $arrobjEvents ) ) {

			if( true == is_null( $this->getLeaseIntervalId() ) ) {
				return true;
			}

			$objApplication = CApplications::fetchApplicationByLeaseIntervalIdByPropertyIdByCid( $this->getLeaseIntervalId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( false == valObj( $objApplication, 'CApplication' ) || ( false == valStr( $objApplication->getAppRemotePrimaryKey() ) && false == valStr( $objApplication->getGuestRemotePrimaryKey() ) ) ) return true;

			if( 12859 == $this->getCid() ) {
				$arrobjEvents = \Psi\Eos\Entrata\CEvents::createService()->fetchNonExportedEventsByPropertyIdByLeaseIntervalIdByCidNew( $objApplication->getPropertyId(), $objApplication->getLeaseIntervalId(), $this->getCid(), $objDatabase, $this->getId() );
			} else {
				$arrobjEvents = \Psi\Eos\Entrata\CEvents::createService()->fetchNonExportedEventsByPropertyIdByLeaseIntervalIdByCid( $objApplication->getPropertyId(), $objApplication->getLeaseIntervalId(), $this->getCid(), $objDatabase );
			}
		}

		$arrobjEvents[$this->getId()] = $this;

		if( true == valArr( $arrobjEvents ) ) {
			ksort( $arrobjEvents );

			foreach( $arrobjEvents as $objEvent ) {
				if( false == $objEvent->export( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride, $intIntegrationSyncTypeId ) ) return false;
			}
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolInsertEventReferences = false ) {

		if( false == in_array( $this->getEventTypeId(), CEventType::$c_arrintEventsToBeExported ) ) $this->setDoNotExport( true );

		// TODO: Remove code after debugging.
		if( CEventType::CALL_INCOMING == $this->getEventTypeId() ) {
			$this->setEventInsertionUrl( ( true == isset( $_SERVER['REQUEST_URI'] ) ) ? $_SERVER['REQUEST_URI'] : NULL );
		}

		if( CEventType::SCHEDULED_FOLLOWUP == $this->getEventTypeId() && false == valId( $this->getScheduledTaskId() ) ) {
			$this->setOverdueOn( $objDatabase );
		}

		if( true == $boolReturnSqlOnly ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
		if( false == parent::insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		// re-open application if any EVENT_TYPE_EMAIL_INCOMING or EVENT_TYPE_CALL_INCOMING type event inserted for closed or archived lead [MSP]
		if( false == $this->checkAndReOpenApplication( SYSTEM_USER_ID, $objDatabase ) ) {
			return false;
		}

		if( true == in_array( $this->getPsProductId(), [ CPsProduct::PROSPECT_PORTAL, CPsProduct::SNIPPET ] ) ) {
    		$objAlertMessage = new CAlertMessage();
    		$objAlertMessage->setCid( $this->getCid() );
    		$objAlertMessage->syncAlertMessages( $intCurrentUserId, $objDatabase, $this->getId() );
		}

		if( true == $boolInsertEventReferences && true == valArr( $this->getEventReferences() ) ) {
			$arrobjEventReferences = $this->getEventReferences();

			foreach( $arrobjEventReferences as $objEventReference ) {
				$objEventReference->setEventId( $this->getId() );
				if( false == $objEventReference->insert( $intCurrentUserId, $objDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		// TODO: Remove code after debugging.
		if( CEventType::CALL_INCOMING == $this->getEventTypeId() ) {
			$this->setEventInsertionUrl( ( true == isset( $_SERVER['REQUEST_URI'] ) ) ? $_SERVER['REQUEST_URI'] : NULL );
		}

		if( true == $boolReturnSqlOnly ) return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( false == parent::update( $intCurrentUserId, $objDatabase ) ) return false;

		if( true == in_array( $this->getPsProductId(), [ CPsProduct::PROSPECT_PORTAL, CPsProduct::SNIPPET ] ) ) {
    		$objAlertMessage = new CAlertMessage();
    		$objAlertMessage->setCid( $this->getCid() );
    		$objAlertMessage->syncAlertMessages( $intCurrentUserId, $objDatabase, $this->getId() );
		}
		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolPerformSoftDelete = true ) {

		if( true == $boolPerformSoftDelete ) {

			$this->setIsDeleted( true );

			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		} else {

			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		}


	}

	public function getConsolidatedErrorMsg() {
		$strErrorMessage = NULL;

		if( true == valArr( $this->m_arrobjErrorMsgs ) ) {
			foreach( $this->m_arrobjErrorMsgs as $objErrorMsg ) {
				$strErrorMessage .= $objErrorMsg->getMessage() . "\n";
			}
		}
		return $strErrorMessage;
	}

	public function setApplicationDetailsDataForArchive( $objApplication, $objLeaseInterval ) {
		if( true == valObj( $objApplication->getDetails(), 'stdClass' ) ) {
			if( true == ( bool ) $objApplication->getIsSystemCanceledOrArchived() ) {
				$objApplication->setIsSystemCanceledOrArchived( false );
			}

			if( true == ( bool ) $objApplication->getFmoRequired() ) {
				$objApplication->setFmoRequired( false );
			}

			if( false == is_null( $objApplication->getArchivedOn() ) ) {
				$objApplication->setArchivedOn( NULL );
			}
		}
		if( true == valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {
			$objLeaseInterval->setLeaseStatusTypeId( CLeaseStatusType::APPLICANT );
		}
	}

	public function unsetApplicationCancellationDetails( $objApplication ) {
		if( true == valObj( $objApplication, 'CApplication' ) ) {
			$objApplication->setCancellationListTypeId( NULL );
			$objApplication->setCancellationListItemId( NULL );
		}
	}

	public function checkAndReOpenApplication( $intCurrentUserId, $objDatabase ) {

		// re-open application if any EVENT_TYPE_EMAIL_INCOMING, EVENT_TYPE_SMS_INCOMING or EVENT_TYPE_CALL_INCOMING type event inserted for closed or archived lead [MSP]

		$arrintEventTypeIds 		= [ CEventType::EMAIL_INCOMING, CEventType::CALL_INCOMING, CEventType::SMS_INCOMING ];
		$arrintApplicationStatusIds = [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ];

		if( false == in_array( $this->getEventTypeId(), $arrintEventTypeIds ) ) {
			return true;
		}

		$objLeaseInterval = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );

		if( ( false == is_null( $this->getIscheckAndReOpenApplication() ) && false == $this->getIscheckAndReOpenApplication() ) || ( true == is_null( $this->getLeaseIntervalId() ) ) || ( CLeaseIntervalType::APPLICATION != $objLeaseInterval->getLeaseIntervalTypeId() ) ) {
			return true;
		}

		$objApplication = CApplications::fetchApplicationByLeaseIntervalIdByPropertyIdByCid( $this->getLeaseIntervalId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( false == valObj( $objApplication, 'CApplication' ) || false == is_null( $objApplication->getCombinedApplicationId() ) ) {
			return true;
		}

		if( false == in_array( $objApplication->getApplicationStatusId(), $arrintApplicationStatusIds ) ) {
			return true;
		}

		// If lead is denied don't reopen it. [KAL]
		if( CApplicationStatus::CANCELLED == $objApplication->getApplicationStatusId() && CListType::LEAD_DENIAL_REASONS == $objApplication->getCancellationListTypeId() ) {
			return true;
		}

		$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'BLOCK_AUTO_REOPENING_OF_CANCELLED_ARCHIVED_LEADS', $objApplication->getPropertyId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' )
			&& ( 'BLOCK_REOPEN_ALL_LEADS' == $objPropertyPreference->getValue() ) ) {
			return true;
		}

		if( CApplicationStage::PRE_APPLICATION == $objApplication->getApplicationStageId() ) {
			if( ( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) || NULL == $objPropertyPreference->getValue() )
				|| ( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 'BLOCK_REOPEN_GUEST_CARD' != $objPropertyPreference->getValue() ) ) {
				$this->setApplicationDetailsDataForArchive( $objApplication, $objLeaseInterval );

				$objApplication->setApplicationStatusId( CApplicationStatus::COMPLETED );
				$this->unsetApplicationCancellationDetails( $objApplication );
				if( true == is_null( $objApplication->getUnitSpaceId() ) ) {
					$boolIsValid = $objApplication->update( $intCurrentUserId, $objDatabase );
					if( true == $boolIsValid && false == $objLeaseInterval->update( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid = false;
						$this->addErrorMsgs( $objLeaseInterval->getErrorMsgs() );
					}
					return $boolIsValid;
				}
			}
		} else {
			$objEventForApplicationStatus = \Psi\Eos\Entrata\CEvents::createService()->fetchEventWithActiveOldApplicationStatusByPropertyIdByLeaseIntervalIdByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objEventForApplicationStatus, 'CEvent' ) && ( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) || ( true == in_array( $objPropertyPreference->getValue(), [ NULL, 'BLOCK_REOPEN_GUEST_CARD' ] ) ) ) ) {
				$objApplication->setApplicationStageId( ( ( false == is_null( $objEventForApplicationStatus->getOldStageId() ) ) ? $objEventForApplicationStatus->getOldStageId() : $objApplication->getApplicationStageId() ) );
				$objApplication->setApplicationStatusId( ( ( false == is_null( $objEventForApplicationStatus->getOldStatusId() ) ) ? $objEventForApplicationStatus->getOldStatusId() : CApplicationStatus::STARTED ) );
				$this->setApplicationDetailsDataForArchive( $objApplication, $objLeaseInterval );
				$this->unsetApplicationCancellationDetails( $objApplication );
			}

			if( true == valObj( $objApplication, 'CApplication' ) && false == is_null( $objApplication->getPropertyUnitId() ) && false == is_null( $objApplication->getUnitSpaceId() ) ) {
				$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'CHANGE_UNIT_STATUS_TO_RENTED', $objApplication->getPropertyId(), $this->getCid(), $objDatabase );

				$intApplicationStageStautsId = NULL;

				if( false == is_null( $objPropertyPreference ) && false == is_null( $objPropertyPreference->getValue() ) ) {
					$intApplicationStageStautsId = $objPropertyPreference->getValue();
				}

				$intApplicationsCount = CApplications::fetchActiveApplicationsCountByIdPropertyUnitIdByUnitSpaceIdByPropertyIdByCid( $objApplication->getId(), $objApplication->getPropertyUnitId(), $objApplication->getUnitSpaceId(), $objApplication->getPropertyId(), $objApplication->getCid(), $objDatabase, $intApplicationStageStautsId );

				$objUnitSpace = $objApplication ->getOrFetchUnitSpace( $objDatabase );

				if( $intApplicationsCount > 0 ) {
					$objApplication->setPropertyUnitId( NULL );
					$objApplication->setUnitSpaceId( NULL );
					$objApplication->setUnitTypeId( NULL );

					$objEventLibrary           = new CEventLibrary();
					$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
					$objEventLibraryDataObject->setDatabase( $objDatabase );
					if( true === valObj( $objUnitSpace, 'CUnitSpace' ) ) {
						$objEventLibraryDataObject->setUnitNumber( $objUnitSpace->getUnitNumberCache() );
					}

					$objEvent = $objEventLibrary->createEvent( [ $objApplication ], CEventType::UNIT_UNAVAILABLE );
					$objEventLibrary->buildEventDescription( $objApplication );

					$objEvent->setCid( $this->getCid() );
					$objEvent->setEventDatetime( 'NOW()' );
					$objEvent->setDoNotExport( NULL );

					$objEventLibrary->setIsExportEvent( false );

					$strReturnValue = $objEventLibrary->insertEvent( $intCurrentUserId, $objDatabase );

					if( false == $strReturnValue ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to insert event by event type.', NULL ) );
						$boolIsValid = false;
					}
				}
			}
		}
		$boolIsValid = $objApplication->update( $intCurrentUserId, $objDatabase );
		if( true == $boolIsValid && false == $objLeaseInterval->update( $intCurrentUserId, $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( $objLeaseInterval->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function insertForStandard( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.events_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.events
					VALUES ( ' .
		 $strId . ', ' .
		 $this->sqlCid() . ', ' .
		 $this->sqlPropertyId() . ', ' .
		 $this->sqlPropertyUnitId() . ', ' .
		 $this->sqlUnitSpaceId() . ', ' .
		 $this->sqlEventTypeId() . ', ' .
		 $this->sqlEventSubTypeId() . ', ' .
		 $this->sqlEventResultId() . ', ' .
		 $this->sqlDefaultEventResultId() . ', ' .
		 $this->sqlAssociatedEventId() . ', ' .
		 $this->sqlPsProductId() . ', ' .
		 $this->sqlOldStageId() . ', ' .
		 $this->sqlNewStageId() . ', ' .
		 $this->sqlOldStatusId() . ', ' .
		 $this->sqlNewStatusId() . ', ' .
		 $this->sqlCompanyEmployeeId() . ', ' .
		 $this->sqlDataReferenceId() . ', ' .
		 $this->sqlIntegrationResultId() . ', ' .
		 $this->sqlLeaseId() . ', ' .
		 $this->sqlLeaseIntervalId() . ', ' .
		 $this->sqlCustomerId() . ', ' .
		 $this->sqlRemotePrimaryKey() . ', ' .
		 $this->sqlCalendarEventKey() . ', ' .
		 $this->sqlScheduledDatetime() . ', ' .
		 $this->sqlScheduledEndDatetime() . ', ' .
		 $this->sqlEventDatetime() . ', ' .
		 $this->sqlEventHandle() . ', ' .
		 $this->sqlTitle() . ', ' .
		 $this->sqlNotes() . ', ' .
		 $this->sqlIpAddress() . ', ' .
		 $this->sqlDoNotExport() . ', ' .
		 $this->sqlIsResident() . ', ' .
		 $this->sqlIsDeleted() . ', ' .
		 ( int ) $intCurrentUserId . ', ' .
		 $this->sqlUpdatedOn() . ', ' .
		 ( int ) $intCurrentUserId . ', ' .
		 $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function bindDetails( $arrmixDetails ) {
		$arrmixDetails = ( object ) $arrmixDetails;
		$this->setDetails( $arrmixDetails );
	}

	public function setOverdueOn( $objDatabase ) {

		if( true == valId( $this->getScheduledTaskId() ) ) return;
		if( false == valStr( $this->getScheduledDatetime() ) ) return;

		$strScheduledDate = $this->getScheduledDatetime();

		$strScheduledDate = ( 'NOW()' == $strScheduledDate ) ? date( 'm/d/Y H:i:s T', strtotime( 'now' ) ) : $strScheduledDate;
		$intDay = date( 'N', strtotime( $strScheduledDate ) );

		$strPropertyHourCloseTime = '17:00:00';

		$objPropertyHours = CPropertyHours::fetchOfficePropertyHoursByPropertyIdByCidByDay( $this->getPropertyId(), $this->getCid(), $intDay, $objDatabase );

		if( true == valObj( $objPropertyHours, 'CPropertyHour' ) && true == valStr( $objPropertyHours->getCloseTime() ) ) {
			$strPropertyHourCloseTime = $objPropertyHours->getCloseTime();
		}

		$objProperty = \Psi\Eos\Admin\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objProperty, CProperty::class ) ) {
			$strPropertyHourCloseTime = date( 'H:i:s', $objProperty->getMstTimestamp( $strPropertyHourCloseTime, $objDatabase ) );
		}

		$strScheduledDateTime = date( 'm/d/Y ', strtotime( $strScheduledDate ) ) . $strPropertyHourCloseTime;
		$strScheduledDateTime = date( 'm/d/Y H:i:s T', strtotime( $strScheduledDateTime ) );

		parent::setOverdueOn( $strScheduledDateTime );
	}

	public function associateQuoteWithEvent( $arrintQuoteIds ) {
		$arrobjQuotes = ( array ) CQuotes::fetchQuotesByIdsByCid( $arrintQuoteIds, $this->getCid(), $this->m_objDatabase );

		if( false == valArr( $arrobjQuotes ) ) {
			return false;
		}

		foreach( $arrobjQuotes as $objQuote ) {
			$objQuote->setEmailEventId( $this->getId() );
		}

		if( false == CQuotes::bulkUpdate( $arrobjQuotes, [ 'email_event_id', 'emailed_on', 'notified_on' ], $this->m_objCompanyUser->getId(), $this->m_objDatabase ) ) {
			return false;
		}
		return true;
	}

	public function addRenewalOfferSentEvent( $intApplicantId, $intApplicationId ) {
		$objApplication = CApplications::fetchApplicationByIdByCid( $intApplicationId, $this->getCid(), $this->m_objDatabase );
		$objApplicant = CApplicants::fetchApplicantByIdByCid( $intApplicantId, $this->getCid(), $this->m_objDatabase );

		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $this->m_objDatabase );
		$objEventLibraryDataObject->setApplicant( $objApplicant );

		$objEvent = $objEventLibrary->createEvent( [ $objApplication ], CEventType::RENEWAL_OFFER_SENT );

		$objEvent->setCid( $this->getCid() );
		$objEvent->setEventTypeId( CEventType::RENEWAL_OFFER_SENT );
		$objEvent->setEventDatetime( 'NOW()' );

		$objEventLibrary->buildEventDescription( $objApplication );

		if( false == $objEventLibrary->insertEvent( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function insertSystemEmailBlock( $strCustomerEmailAddress, $intCustomerId, $intCid ) {
		$objSystemEmailBlock = new CSystemEmailBlock();
		$objSystemEmailBlock->setEmailAddress( $strCustomerEmailAddress );
		$objSystemEmailBlock->setEmailBlockTypeId( CEmailBlockType::RESIDENT_PORTAL_INVITE_EMAIL );
		$objSystemEmailBlock->setCid( $intCid );
		$objSystemEmailBlock->setCustomerId( $intCustomerId );

		switch( NULL ) {
			default:
				if( false == $objSystemEmailBlock->validate( VALIDATE_INSERT ) || false == $objSystemEmailBlock->insert( SYSTEM_USER_ID, $this->m_objEmailDatabase ) ) {
					$this->addErrorMsgs( $objSystemEmailBlock->getErrorMsgs() );
					break;
				}
		}
	}

	public function afterAvailabilityEmailSent( $intApplicationId, $intCid ) {
		$objAvailabilityAlertRequest = \Psi\Eos\Entrata\CAvailabilityAlertRequests::createService()->fetchAvailabilityAlertRequestByApplicationIdByCid( $intApplicationId, $intCid, $this->m_objDatabase );

		if( true == valObj( $objAvailabilityAlertRequest, 'CAvailabilityAlertRequest' ) ) {
			$objAvailabilityAlertRequest->setLastEmailAlertedOn( 'NOW()' );

			if( false == $objAvailabilityAlertRequest->update( SYSTEM_USER_ID, $this->m_objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function getOrFetchEventTypeName( $objDatabase ) {

		if( true == valStr( $this->getEventTypeName() ) )
			return $this->getEventTypeName();
		if( true == valObj( $objDatabase, CDatabase::class )
		    && false == valObj( $this->getDatabase(), CDatabase::class ) ) {
			$this->setDatabase( $objDatabase );
		}
		$objEventType = \Psi\Eos\Entrata\CEventTypes::createService()->fetchEventTypeById( $this->getEventTypeId(), $this->getDatabase() );

		if( false == valObj( $objEventType, 'CEventType' ) ) {
			return NULL;
		}

		$this->setEventTypeName( $objEventType->getName( CLocale::DEFAULT_LOCALE ) );
		return $this->getEventTypeName();
	}

	public static function getCurrentUtcTime() {
		$objDateTime = new DateTime( 'now', new \DateTimeZone( 'UTC' ) );
		return $objDateTime->format( DateTime::ISO8601 );
	}

	public function associateRoommateInvitationEvent( $intApplicantId ) {
		$objApplicant = CApplicants::fetchApplicantByIdByCid( $intApplicantId, $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objApplicant, 'CApplicant' ) ) {
			return;
		}

		$arrobjEvents = ( array ) \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByCustomerIdByEventTypeIdsByPropertyIdByCid( $objApplicant->getCustomerId(), [ CEventType::APPLICANT_GROUPING ], $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		foreach( $arrobjEvents as $objEvent ) {
			$objEvent->setDataReferenceId( $this->getDataReferenceId() );
			if( false == $objEvent->update( $this->m_objCompanyUser->getId(), $this->m_objDatabase ) ) {
				return;
			}
		}
	}

	public function insertCustomerMessage( $strSubject, $strEmailContent, $arrintPackageIds ) {
		$objCustomerMessage = new CCustomerMessage();
		$objCustomerMessage->setCid( $this->getCid() );
		$objCustomerMessage->setSenderId( $this->m_objCompanyUser->getId() );
		$objCustomerMessage->setSenderTypeId( CCustomerMessageSenderType::PROPERTY );
		$objCustomerMessage->setSubject( $strSubject );
		$objCustomerMessage->setPropertyId( $this->getPropertyId() );
		$objCustomerMessage->setCustomerId( $this->getCustomerId() );
		$objCustomerMessage->setMessage( $strEmailContent );
		if( false == $objCustomerMessage->validate( VALIDATE_INSERT ) || false == $objCustomerMessage->insert( $this->m_objCompanyUser->getId(), $this->m_objDatabase ) ) {
			return;
		}

		$arrobjPackages = ( array ) \Psi\Eos\Entrata\CPackages::createService()->fetchPackagesByIdsByCid( $arrintPackageIds, $this->getCid(), $this->m_objDatabase );
		foreach( $arrobjPackages as $objPackage ) {
			$objPackage->setIncrementNotificationCount();
			$objPackage->setEmailedOn( date( 'Y-m-d H:i:s' ) );

			if( false == $objPackage->update( $this->m_objCompanyUser->getId(), $this->m_objDatabase ) ) {
				return;
			}
		}
	}

	public function stripStrongTag( $strText ) {
		return ( true == valStr( $strText ) && false != stristr( $strText, '<strong>' ) ) ? str_replace( '<strong>', '', str_replace( '</strong>', '', $strText ) ) : $strText;
	}

}
?>
