<?php

class CCompanyDepartment extends CBaseCompanyDepartment {

	const LEASING			= 'LEASING';
	const ADMINISTRATIVE	= 'ADMINISTRATIVE';

	const DEFAULT_COMPANY_DEPARTMENT_SYSTEM_ADMINISTRATOR_ID	= 0;
	const DEFAULT_COMPANY_DEPARTMENT_ADMINISTRATIVE_ID	        = 1;
	const DEFAULT_COMPANY_DEPARTMENT_MANAGERIAL_ID		        = 2;
	const DEFAULT_COMPANY_DEPARTMENT_MAINTENANCE_ID		        = 3;
	const DEFAULT_COMPANY_DEPARTMENT_LEASING_ID			        = 4;
	const DEFAULT_COMPANY_DEPARTMENT_ACCOUNTING_ID		        = 5;
	const DEFAULT_COMPANY_DEPARTMENT_MARKETING_ID		        = 6;
	const DEFAULT_COMPANY_DEPARTMENT_IT_ID				        = 7;

	/**
	 * Validation Functions
	 */

	public function valName( $objClientDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter department name.' ) );
		} else {

			$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . '
								AND name ILIKE \'' . addslashes( $this->getName() ) . '\'
								AND id <> ' . ( int ) $this->getId();

			$intCompanyDepartmentCount = ( int ) \Psi\Eos\Entrata\CCompanyDepartments::createService()->fetchCompanyDepartmentCount( $strWhere, $objClientDatabase );

			if( 0 < $intCompanyDepartmentCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Department name already exist.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>