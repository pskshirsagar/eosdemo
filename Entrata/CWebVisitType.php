<?php

class CWebVisitType extends CBaseWebVisitType {

	const CLICK 							= 1;
	const VIEW 								= 2;
	const DIRECT_REQUEST 					= 3;
	const PROSPECT_PORTAL_FACEBOOK_VISIT 	= 4;
	const RESIDENT_PORTAL_FACEBOOK_VISIT 	= 5;
	const PROSPECT_PORTAL_FACEBOOK_VIEW		= 6;
	const RESIDENT_PORTAL_FACEBOOK_VIEW		= 7;
	const RESIDENT_PORTAL_FACEBOOK_LOGIN	= 8;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>