<?php

class CCompanyEventType extends CBaseCompanyEventType {

	const PARTY			= 1;
	const BBQ			= 2;
	const MOVIE			= 3;
	const ANNOUNCEMENT	= 4;
	const HOLIDAY		= 5;
	const REMINDER		= 6;
	const SURPRISE		= 7;
	const OTHER			= 8;

	public function valName( $objDatabase = NULL ) {

		$boolIsValid = true;
		// It is required
		if( false == valStr( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ), 502 ) );
			$boolIsValid = false;
		}

		$intCompanyEventTypeCount = CCompanyEventTypes::fetchDuplicateCompanyEventTypeCountByIdByNameByCid( $this->getId(), $this->getName(), $this->getCid(), $objDatabase );

		if( 0 < $intCompanyEventTypeCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name already in use.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

}
?>