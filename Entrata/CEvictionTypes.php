<?php

class CEvictionTypes extends CBaseEvictionTypes {

	public static function fetchEvictionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEvictionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEvictionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEvictionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>