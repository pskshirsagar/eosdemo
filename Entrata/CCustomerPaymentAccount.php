<?php

class CCustomerPaymentAccount extends CBaseCustomerPaymentAccount {

	protected $m_strConfirmCheckAccountNumber;
	protected $m_strBilltoBuildingName;
	protected $m_strBilltoUnitSpaceName;
	protected $m_intPropertyId;
	protected $m_intCvvCode;
	protected $m_boolUseDebitPricing;

	protected $m_objPaymentBankAccount;
	protected $m_intScheduledPaymentId;
	protected $m_strIbanToken;

	/**
	 * Get Functions
	 */

	public function getCcCardNumber() {
		if( false == valStr( $this->getCcCardNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCcCardNumberEncrypted(), CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] );
	}

	public function getCheckAccountNumber() {
		if( false == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );

		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getAccountNumberLastFour() {
		if( in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) ) {
			$strCcCardNumber = $this->getCcCardNumber();
			$strLastFour = \Psi\CStringService::singleton()->substr( $strCcCardNumber, -4 );
		} else {
			$strCheckAccountNumber = $this->getCheckAccountNumber();
			$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );
		}

		return $strLastFour;
	}

	public function getCcCardNumberMasked() {
		$strCcCardNumber = $this->getCcCardNumber();
		$intStringLength = strlen( $strCcCardNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCcCardNumber, -4 );

		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getBilltoNameFull() {
		return trim( $this->m_strBilltoNameFirst ) . ' ' . trim( $this->m_strBilltoNameLast );
	}

	public function getConfirmCheckAccountNumber() {
		return $this->m_strConfirmCheckAccountNumber;
	}

	public function getBilltoBuildingName() {
		return $this->m_strBilltoBuildingName;
	}

	public function getBilltoUnitSpaceName() {
		return $this->m_strBilltoUnitSpaceName;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getFormattedBilltoPhoneNumber() {

		$strPhoneNumber = $this->getBilltoPhoneNumber();

		if( 0 < strlen( $strPhoneNumber ) ) {
			$strPhoneNumber = preg_replace( '/[^0-9]+/', '', $strPhoneNumber );

			if( 0 == strlen( $strPhoneNumber ) ) return NULL;

			if( 0 < strlen( $strPhoneNumber ) )
				return  \Psi\CStringService::singleton()->substr( $strPhoneNumber, 0, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strPhoneNumber, 3, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strPhoneNumber, 6, strlen( $strPhoneNumber ) - 6 );

		}
		return NULL;
	}

	public function getPaymentTypeName() {
		return CPaymentTypes::paymentTypeIdToStr( $this->getPaymentTypeId() );
	}

	public function getCvvCode() {
		return $this->m_intCvvCode;
	}

	public function getCreatedCustomerId() {
		return $this->getDetailsField( 'created_customer_id' );
	}

	public function getUpdatedCustomerId() {
		return $this->getDetailsField( 'updated_customer_id' );
	}

	public function getDeletedCustomerId() {
		return $this->getDetailsField( 'deleted_customer_id' );
	}

	/**
	 * @return mixed
	 */
	public function useDebitPricing() {
		return $this->m_boolUseDebitPricing;
	}

	public function getScheduledPaymentId() {
	    return $this->m_intScheduledPaymentId;
    }

	public function getIbanToken() {
	    return $this->m_strIbanToken;
    }

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['cc_card_number'] ) ) $this->setCcCardNumber( $arrstrValues['cc_card_number'] );
		if( true == isset( $arrstrValues['check_account_number'] ) ) $this->setCheckAccountNumber( $arrstrValues['check_account_number'] );
		if( true == isset( $arrstrValues['confirm_check_account_number'] ) ) $this->setConfirmCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['confirm_check_account_number'] ) : $arrstrValues['confirm_check_account_number'] );
		if( true == isset( $arrstrValues['property_id'] ) ) $this->setPropertyId( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['property_id'] ) : $arrstrValues['property_id'] );
		if( true == isset( $arrstrValues['is_debit_card'] ) ) $this->setIsDebitCard( $arrstrValues['is_debit_card'] );
		if( true == isset( $arrstrValues['is_check_card'] ) ) $this->setIsCheckCard( $arrstrValues['is_check_card'] );
		if( true == isset( $arrstrValues['is_gift_card'] ) ) $this->setIsGiftCard( $arrstrValues['is_gift_card'] );
		if( true == isset( $arrstrValues['is_prepaid_card'] ) ) $this->setIsPrepaidCard( $arrstrValues['is_prepaid_card'] );
		if( true == isset( $arrstrValues['is_credit_card'] ) ) $this->setIsCreditCard( $arrstrValues['is_credit_card'] );
		if( true == isset( $arrstrValues['is_international_card'] ) ) $this->setIsInternationalCard( $arrstrValues['is_international_card'] );
		if( true == isset( $arrstrValues['is_commercial_card'] ) ) $this->setIsCommercialCard( $arrstrValues['is_commercial_card'] );
        if( true == isset( $arrstrValues['scheduled_payment_id'] ) ) $this->setScheduledPaymentId( $arrstrValues['scheduled_payment_id'] );

		return;
	}

	public function setValuesFromArPayment( $objPayment ) {

		if( false == valObj( $objPayment, 'CArPayment' ) && false == valObj( $objPayment, 'CScheduledPayment' ) ) return NULL;
		$this->setPaymentTypeId( $objPayment->getPaymentTypeId() );
		// Used to update if stored billing info exists.

		if( 0 == $objPayment->getUseStoredBillingInfo() && true == $objPayment->getIsUpdateStoredBillingInfo() && false == is_null( $objPayment->getCustomerPaymentAccountId() ) ) {
			$this->setId( $objPayment->getCustomerPaymentAccountId() );
		}

		if( false == is_null( $objPayment->getCheckAccountNumber() ) ) $this->setCheckAccountNumber( $objPayment->getCheckAccountNumber() );
		if( false == is_null( $objPayment->getConfirmCheckAccountNumber() ) ) $this->setConfirmCheckAccountNumber( $objPayment->getConfirmCheckAccountNumber() );
		if( false == is_null( $objPayment->getCheckAccountTypeId() ) ) $this->setCheckAccountTypeId( $objPayment->getCheckAccountTypeId() );
		if( false == is_null( $objPayment->getCheckBankName() ) ) $this->setCheckBankName( $objPayment->getCheckBankName() );
		if( false == is_null( $objPayment->getCheckNameOnAccount() ) ) $this->setCheckNameOnAccount( $objPayment->getCheckNameOnAccount() );
		if( false == is_null( $objPayment->getCheckRoutingNumber() ) ) $this->setCheckRoutingNumber( $objPayment->getCheckRoutingNumber() );

		if( false == is_null( $objPayment->getCcCardNumber() ) ) $this->setCcCardNumber( $objPayment->getCcCardNumber() );
		if( false == is_null( $objPayment->getSecureReferenceNumber() ) ) $this->setSecureReferenceNumber( $objPayment->getSecureReferenceNumber() );
		if( false == is_null( $objPayment->getCcExpDateMonth() ) ) $this->setCcExpDateMonth( $objPayment->getCcExpDateMonth() );
		if( false == is_null( $objPayment->getCcExpDateYear() ) ) $this->setCcExpDateYear( $objPayment->getCcExpDateYear() );
		if( false == is_null( $objPayment->getCcNameOnCard() ) ) $this->setCcNameOnCard( $objPayment->getCcNameOnCard() );
		if( false == is_null( $objPayment->getIsDebitCard() ) ) $this->setIsDebitCard( $objPayment->getIsDebitCard() );
		if( false == is_null( $objPayment->getAccountNickname() ) ) $this->setAccountNickname( $objPayment->getAccountNickname() );

		if( false == is_null( $objPayment->getBilltoNameFirst() ) )   $this->setBilltoNameFirst( $objPayment->getBilltoNameFirst() );
		if( false == is_null( $objPayment->getBilltoNameLast() ) )    $this->setBilltoNameLast( $objPayment->getBilltoNameLast() );
		if( false == is_null( $objPayment->getBilltoStreetLine1() ) ) $this->setBilltoStreetLine1( $objPayment->getBilltoStreetLine1() );
		if( false == is_null( $objPayment->getBilltoStreetLine2() ) ) $this->setBilltoStreetLine2( $objPayment->getBilltoStreetLine2() );
		if( false == is_null( $objPayment->getBilltoStreetLine3() ) ) $this->setBilltoStreetLine3( $objPayment->getBilltoStreetLine3() );
		if( false == is_null( $objPayment->getBilltoCity() ) ) 		  $this->setBilltoCity( $objPayment->getBilltoCity() );
		if( false == is_null( $objPayment->getBilltoStateCode() ) )   $this->setBilltoStateCode( $objPayment->getBilltoStateCode() );
		if( false == is_null( $objPayment->getBilltoPostalCode() ) )  $this->setBilltoPostalCode( $objPayment->getBilltoPostalCode() );
		if( false == is_null( $objPayment->getBilltoCountryCode() ) ) $this->setBilltoCountryCode( $objPayment->getBilltoCountryCode() );
		if( false == is_null( $objPayment->getBilltoPhoneNumber() ) ) $this->setBilltoPhoneNumber( $objPayment->getBilltoPhoneNumber() );
		if( false == is_null( $objPayment->getBilltoEmailAddress() ) )$this->setBilltoEmailAddress( $objPayment->getBilltoEmailAddress() );
		if( false == is_null( $objPayment->getBilltoUnitNumber() ) )  $this->setBilltoUnitNumber( $objPayment->getBilltoUnitNumber() );

		if( true == valId( $objPayment->getSecureReferenceNumber() ) ) {
			$arrmixBinFlags = \Psi\Core\Payment\CPaymentBinFlags::createService()->getAdditionalBinFlags( $objPayment->getSecureReferenceNumber() );

			if( true == valArr( $arrmixBinFlags ) ) {
				$arrstrDetails = json_decode( json_encode( $this->getDetails() ), true );

				$arrstrDetails['bin_flags'] = $arrmixBinFlags;

				$this->setDetails( json_encode( $arrstrDetails ) );
			}
		}
	}

	public function setBilltoBuildingName( $strBuildingName ) {
		$this->m_strBilltoBuildingName = CStrings::strTrimDef( $strBuildingName, 30, NULL, true );
	}

	public function setBilltoUnitSpaceName( $strUnitSpaceName ) {
		$this->m_strBilltoUnitSpaceName = CStrings::strTrimDef( $strUnitSpaceName, 30, NULL, true );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setCcCardNumber( $strCcCardNumber ) {
		$strCcCardNumber = CStrings::strTrimDef( $strCcCardNumber, 20, NULL, true );
		// Ensure that a masked card number is always set on object
		if( false == is_null( $strCcCardNumber ) ) {
			$strCcCardNumber = str_repeat( 'X', strlen( $strCcCardNumber ) - 4 ) . \Psi\CStringService::singleton()->substr( $strCcCardNumber, -4 );
		}

		if( true == valStr( $strCcCardNumber ) ) {
			$this->setCcCardNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCcCardNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );
		}
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumber, 'X' ) ) return;
		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 20, NULL, true );
		if( true == valStr( $strCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	public function setBilltoPhoneNumber( $strBilltoPhoneNumber ) {
		$intLen = ( 0 < strlen( $strBilltoPhoneNumber ) && false == is_null( $strBilltoPhoneNumber ) ) ? strlen( $strBilltoPhoneNumber ) : 0;

		if( 0 < $intLen ) {
			// We need to reformat phone number to get rid of everything but numbers
			$strBilltoPhoneNumber = preg_replace( '/[^a-zA-Z0-9]|\s/', '', trim( $strBilltoPhoneNumber ) );
			$strBilltoPhoneNumber = \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 0, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 3, 3 ) . '-' . \Psi\CStringService::singleton()->substr( $strBilltoPhoneNumber, 6, 10 );
			$this->m_strBilltoPhoneNumber = CStrings::strTrimDef( $strBilltoPhoneNumber, 30, NULL, true );
		}
	}

	public function setConfirmCheckAccountNumber( $strConfirmCheckAccountNumber ) {
		return $this->m_strConfirmCheckAccountNumber = $strConfirmCheckAccountNumber;
	}

	public function setCvvCode( $intCvvCode ) {
		$this->m_intCvvCode = $intCvvCode;
	}

	public function setUseDebitPricing( $boolUseDebitPricing ) {
		$this->m_boolUseDebitPricing = $boolUseDebitPricing;
	}

    public function setScheduledPaymentId( $intScheduledPaymentId ) {
        $this->m_intScheduledPaymentId = $intScheduledPaymentId;
	}

	public function setCreatedCustomerId( $intCreatedCustomerId ) {
		$this->setDetailsField( 'created_customer_id', $intCreatedCustomerId );
	}

	public function setUpdatedCustomerId( $intUpdatedCustomerId ) {
		$this->setDetailsField( 'updated_customer_id', $intUpdatedCustomerId );
	}

	public function setDeletedCustomerId( $intDeletedCustomerId ) {
		$this->setDetailsField( 'deleted_customer_id', $intDeletedCustomerId );
	}

	public function setIbanToken( $strIbanToken ) {
		$this->m_strIbanToken = $strIbanToken;
	}

	public function valCustomerPlaidItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/**
	 * Validation Functions
	 */

	public function validate( $strAction, $objClientDatabase = NULL, $boolIsValidateBankName = true, $boolIsFromMakeAPayment = false, $objPaymentDatabase = NULL, $strClientCountryCode = 'US', $boolIsValidateCvv = false, $boolIsSkipRoutingNumberValidation = false, $boolIsAdd = false, $boolValidatePostalCode = true ) {
		$boolIsValid = true;

		$objCustomerPaymentAccountValidator = new CCustomerPaymentAccountValidator();
		$objCustomerPaymentAccountValidator->setCustomerPaymentAccount( $this );
		$boolIsValid &= $objCustomerPaymentAccountValidator->validate( $strAction, $objClientDatabase, $boolIsValidateBankName, $boolIsFromMakeAPayment, $objPaymentDatabase, $strClientCountryCode, $boolIsValidateCvv, $boolIsSkipRoutingNumberValidation, $boolIsAdd, $boolValidatePostalCode );

		return $boolIsValid;
	}

	/**
	 * Insert Functions
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintECheckPaymentTypeIds, false ) ) {

			$this->setCcNameOnCard( NULL );
			$this->setCcCardNumber( NULL );
			$this->setCcExpDateYear( NULL );
			$this->setCcExpDateMonth( NULL );

		} elseif( ( CPaymentType::DISCOVER == $this->getPaymentTypeId() ) || ( CPaymentType::AMEX == $this->getPaymentTypeId() ) || ( CPaymentType::VISA == $this->getPaymentTypeId() ) || ( CPaymentType::MASTERCARD == $this->getPaymentTypeId() ) ) {

			$this->setCheckAccountNumber( NULL );
			$this->setCheckRoutingNumber( NULL );
			$this->setCheckNameOnAccount( NULL );
			$this->setCheckAccountTypeId( NULL );
			$this->setCheckBankName( NULL );
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objOldCustomerPaymentAccount = CCustomerPaymentAccounts::fetchSimpleCustomerPaymentAccountByIdByCid( $this->getId(), $this->getCid(), $objDatabase, [ 'id', 'cid', 'customer_id', 'billto_name_first', 'billto_name_last' ] );
		if( true == valObj( $objOldCustomerPaymentAccount, 'CCustomerPaymentAccount' ) && $this->getCustomerId() != $objOldCustomerPaymentAccount->getCustomerId() ) {

			$strMessage			= ' Customer Payment Account Id: ' . $this->getId() . '<br /> Cid: ' . $this->getCid() . '<br /> Old Customer Id: ' . $objOldCustomerPaymentAccount->getCustomerId() . '<br /> New Customer Id: ' . $this->getCustomerId() . '<br /> Old Bill to  Name: ' . $objOldCustomerPaymentAccount->getBilltoNameFirst() . ' ' . $objOldCustomerPaymentAccount->getBilltoNameLast() . '<br /> New Bill to Name: ' . $this->getBilltoNameFirst() . ' ' . $this->getBilltoNameLast() . '<br /><br /> Backtrace <br />' . ( new Exception )->getTraceAsString();
			$objPaymentEmailer	= new CPaymentEmailer();
			$strSubject			= 'Customer Payment Account[stored billing info] updated with mismatching info';
			$objPaymentEmailer->emailEmergencyTeam( $strMessage, $strSubject, [ CSystemEmail::XENTO_RPARDESHI_EMAIL_ADDRESS, CSystemEmail::SCHAVHAN_EMAIL_ADDRESS, CSystemEmail::XENTO_RBHADAURIA_EMAIL_ADDRESS, CSystemEmail::XENTO_RVYAS_EMAIL_ADDRESS ] );
		}

		if( CPaymentType::ACH == $this->getPaymentTypeId() ) {

			$this->setCcNameOnCard( NULL );
			$this->setCcCardNumber( NULL );
			$this->setCcExpDateYear( NULL );
			$this->setCcExpDateMonth( NULL );

		} elseif( ( CPaymentType::DISCOVER == $this->getPaymentTypeId() ) || ( CPaymentType::AMEX == $this->getPaymentTypeId() ) || ( CPaymentType::VISA == $this->getPaymentTypeId() ) || ( CPaymentType::MASTERCARD == $this->getPaymentTypeId() ) ) {

			$this->setCheckAccountNumber( NULL );
			$this->setCheckRoutingNumber( NULL );
			$this->setCheckNameOnAccount( NULL );
			$this->setCheckAccountTypeId( NULL );
			$this->setCheckBankName( NULL );
		}

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	/**
	 * Other Functions
	 */

	public function clearSecureFields() {
		$this->setCcCardNumberEncrypted( '' );
		$this->setCheckAccountNumberEncrypted( '' );
		$this->setCheckRoutingNumber( '' );
	}

	public function isCreditCard() {
		if( in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintECheckPaymentTypeIds, false ) || false == is_numeric( $this->getPaymentTypeId() ) ) {
			return false;
		}

		return true;
	}

	public function getIsDebitOrCheckCard() {
		return $this->getIsDebitCard() || $this->getIsCheckCard();
	}

	/**
	 * Add Functions
	 */

	public function getConsolidatedErrorMsg() {
		$strErrorMessage = NULL;

		if( true == valArr( $this->m_arrobjErrorMsgs ) ) {
			foreach( $this->m_arrobjErrorMsgs as $objErrorMsg ) {
				$strErrorMessage .= $objErrorMsg->getMessage() . "\n";
			}
		}

		return $strErrorMessage;
	}

	public function clearStoredBillingInfo( $intCustomerPaymentAccountId, $intCustomerId, $intCid, $strCheckRoutingNumber, $strCheckAccountNumber, $intUserId, $objClientDatabase ) {
		$objPaymentAccount = NULL;

		if( is_null( $intCustomerPaymentAccountId ) && false == is_null( $intCustomerId ) && false == is_null( $intCid ) ) {
			$arrintPaymentTypeIds = [ CPaymentType::ACH ];
			$arrobjCustomerPaymentAccounts = CCustomerPaymentAccounts::fetchOrderedCustomerPaymentAccountsByCustomerIdByPaymentTypeIdsByCid( $intCustomerId, $arrintPaymentTypeIds, $intCid, $objClientDatabase );

			if( valArr( $arrobjCustomerPaymentAccounts ) ) {
				foreach( $arrobjCustomerPaymentAccounts as $objCustomerPaymentAccount ) {
					$strCustomerRoutingNumber = $objCustomerPaymentAccount->getCheckRoutingNumber();
					$strCustomerAccountNumber = $objCustomerPaymentAccount->getCheckAccountNumber();

					// make sure they match
					if( $strCustomerRoutingNumber == $strCheckRoutingNumber && $strCustomerAccountNumber == $strCheckAccountNumber ) {
						$objPaymentAccount = $objCustomerPaymentAccount;

						break;
					}
				}
			}
		} elseif( false == is_null( $intCustomerPaymentAccountId ) ) {
			$objPaymentAccount = CCustomerPaymentAccounts::fetchCustomerPaymentAccountByIdByCid( $intCustomerPaymentAccountId, $this->getCid(), $objClientDatabase );
		} else {
			$objPaymentAccount = $this;
		}

		if( NULL == $objPaymentAccount ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to find stored billing info for Customer Payment Account Id: ' . ( int ) $intCustomerPaymentAccountId . ', Customer Id: ' . ( int ) $intCustomerId . ', Client Id: ' . ( int ) $intCid ) );
			// unable to find stored billing info, so just exit
			return false;
		}

		if( $objPaymentAccount->getCheckRoutingNumber() != $strCheckRoutingNumber || $objPaymentAccount->getCheckAccountNumber() != $strCheckAccountNumber ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Found stored billing info for Customer Payment Account Id: ' . $objPaymentAccount->getId() . ', but did not delete because routing and/or account number did not match.' ) );

			return false;
		}

		$objPaymentAccount->setDeletedBy( $intUserId );
		$objPaymentAccount->setDeletedOn( date( 'Y-m-d H:i:sO' ) );

		if( false == $objPaymentAccount->update( SYSTEM_USER_ID, $objClientDatabase ) ) {
			$this->addErrorMsgs( $objPaymentAccount->getErrorMsgs() );

			return false;
		}

		return true;
	}

	public function createBillingInfoResponse( $boolIsDebitEnabled = false ) {
		$strBilltoUnitNumber = $this->getBilltoUnitNumber();
		if( true == valStr( $this->getBilltoBuildingName() ) && true == valStr( $this->getBilltoUnitNumber() ) ) {
			$strBilltoUnitNumber = $this->getBilltoBuildingName() . ' - ' . $this->getBilltoUnitNumber();
		}
		if( false == is_null( $this->getBilltoUnitSpaceName() ) && true == valStr( $this->getBilltoUnitNumber() ) ) {
			$strBilltoUnitNumber .= ' - ' . $this->getBilltoUnitSpaceName();
		}

		$strBilltoStreetLine1 = ( true == valArr( $this->getBilltoStreetLine1() ) ) ? reset( $this->getBilltoStreetLine1() ) : $this->getBilltoStreetLine1();
		$strBilltoStreetLine2 = ( true == valArr( $this->getBilltoStreetLine2() ) ) ? reset( $this->getBilltoStreetLine2() ) : $this->getBilltoStreetLine2();
		$strBilltoStreetLine3 = ( true == valArr( $this->getBilltoStreetLine3() ) ) ? reset( $this->getBilltoStreetLine3() ) : $this->getBilltoStreetLine3();

		if( in_array( $this->getPaymentTypeId(), CPaymentType::$c_arrintECheckPaymentTypeIds, false ) ) {
			$arrstrCustomerPaymentAccount = [
				'name_on_account' => $this->getCheckNameOnAccount(),
				'bill_to_street_line_1' => utf8_encode( $strBilltoStreetLine1 ),
				'bill_to_street_line_2' => utf8_encode( $strBilltoStreetLine2 ),
				'bill_to_street_line_3' => utf8_encode( $strBilltoStreetLine3 ),
				'bill_to_unit_number' => $strBilltoUnitNumber,
				'bill_to_city' => utf8_encode( $this->getBilltoCity() ),
				'bill_to_state_code' => $this->getBilltoStateCode(),
				'bill_to_province' => utf8_encode( $this->getBilltoProvince() ),
				'bill_to_postal_code' => $this->getBilltoPostalCode(),
				'bill_to_country_code' => $this->getBilltoCountryCode(),
				'type_of_account' => $this->getCheckAccountTypeId(),
				'account_verification_log_id' => $this->getAccountVerificationLogId(),
				'bank_name' => $this->getCheckBankName(),
				'routing_number' => $this->getCheckRoutingNumber(),
				'account_number' => $this->getCheckAccountNumber(),
				'account_number_masked' => $this->getCheckAccountNumberMasked(),
				'account_nickname' => $this->getAccountNickname(),
				'payment_type_id' => $this->getPaymentTypeId()
			];

		} else {
			$arrboolBinFlags = \Psi\Core\Payment\CPaymentBinFlags::createService()->getPaymentBinFlags( $this->getSecureReferenceNumber() );

			$boolIsDebit = false;
			if( true == valArr( $arrboolBinFlags ) && ( $arrboolBinFlags['is_debit_card'] === true ||
					$arrboolBinFlags['is_check_card'] === true ||
					$arrboolBinFlags['is_gift_card'] === true ||
					$arrboolBinFlags['is_prepaid_card'] === true )
				&& $arrboolBinFlags['is_credit_card'] === false ) {
				$boolIsDebit = true;
			}
			$arrstrCustomerPaymentAccount  = [
				'name_on_card' => $this->getCcNameOnCard(),
				'bill_to_street_line_1' => utf8_encode( $strBilltoStreetLine1 ),
				'bill_to_street_line_2' => utf8_encode( $strBilltoStreetLine2 ),
				'bill_to_street_line_3' => utf8_encode( $strBilltoStreetLine3 ),
				'bill_to_unit_number' => $strBilltoUnitNumber,
				'bill_to_city' => utf8_encode( $this->getBilltoCity() ),
				'bill_to_state_code' => $this->getBilltoStateCode(),
				'bill_to_province' => utf8_encode( $this->getBilltoProvince() ),
				'bill_to_postal_code' => $this->getBilltoPostalCode(),
				'bill_to_country_code' => $this->getBilltoCountryCode(),
				'card_number' => $this->getCcCardNumber(),
				'cc_card_number_masked' => $this->getCcCardNumberMasked(),
				'secure_reference_number' => $this->getSecureReferenceNumber(),
				'expiration_date_month' => $this->getCcExpDateMonth(),
				'expiration_date_year' => $this->getCcExpDateYear(),
				'cc_bin_number' => $this->getCcBinNumber(),
				'is_debit_card' => $boolIsDebit,
				'is_check_card' => ( true == isset( $arrboolBinFlags['is_check_card'] ) ) ? $arrboolBinFlags['is_check_card'] : $this->getIsCheckCard(), // if no flags returned, then use the value that was originally stored
				'is_gift_card' => ( true == isset( $arrboolBinFlags['is_gift_card'] ) ) ? $arrboolBinFlags['is_gift_card'] : $this->getIsGiftCard(),
				'is_prepaid_card' => ( true == isset( $arrboolBinFlags['is_prepaid_card'] ) ) ? $arrboolBinFlags['is_prepaid_card'] : $this->getIsPrepaidCard(),
				'is_credit_card' => ( true == isset( $arrboolBinFlags['is_credit_card'] ) ) ? $arrboolBinFlags['is_credit_card'] : $this->getIsCreditCard(),
				'is_international_card' => ( true == isset( $arrboolBinFlags['is_international_card'] ) ) ? $arrboolBinFlags['is_international_card'] : $this->getIsInternationalCard(),
				'is_commercial_card' => ( true == isset( $arrboolBinFlags['is_commercial_card'] ) ) ? $arrboolBinFlags['is_commercial_card'] : $this->getIsCommercialCard(),
				'zip_code' => utf8_encode( $this->getBilltoPostalCode() ),
				'account_nickname' => $this->getAccountNickname(),
				'payment_type_id' => $this->getPaymentTypeId()
			];
		}

		return $arrstrCustomerPaymentAccount;
	}

	public static function updateCustomerPaymentAccountDetails( $objCustomerPaymentAccount, $intAccountVerificationLogId, $objDatabase, $intUserId = NULL, $boolIsDeleted = false ) {
		$intUserId = ( true == valId( $intUserId ) ) ? $intUserId : CUser::ID_RESIDENT_PORTAL;
		$objCustomerPaymentAccount->setAccountVerificationLogId( $intAccountVerificationLogId );

		if( true == $boolIsDeleted ) {
			$objCustomerPaymentAccount->setDeletedBy( $intUserId );
			$objCustomerPaymentAccount->setDeletedOn( 'NOW()' );
		} else {
			if( true == in_array( $objCustomerPaymentAccount->getPaymentId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && true == valId( $objCustomerPaymentAccount->getSecureReferenceNumber() ) ) {
				$arrmixBinFlags = \Psi\Core\Payment\CPaymentBinFlags::createService()->getAdditionalBinFlags( $objCustomerPaymentAccount->getSecureReferenceNumber() );

				if( true == valArr( $arrmixBinFlags ) ) {
					$arrstrDetails = json_decode( json_encode( $objCustomerPaymentAccount->getDetails() ), true );

					$arrstrDetails['bin_flags'] = $arrmixBinFlags;

					$objCustomerPaymentAccount->setDetails( json_encode( $arrstrDetails ) );
				}
			}
		}

		return $objCustomerPaymentAccount->update( SYSTEM_USER_ID, $objDatabase );
	}

	public static function updateCustomerPaymentAccountBinFlagsAndDetails( $intCustomerPaymentAccountId, $intCid, $intUserId, $objDatabase, $arrmixAdditionalBinFlags = [], $arrboolBinFlags = [] ) {
		if( true == \Psi\Libraries\UtilFunctions\valId( $intCustomerPaymentAccountId ) && true == \Psi\Libraries\UtilFunctions\valId( $intCid ) && true == \Psi\Libraries\UtilFunctions\valObj( $objDatabase, CDatabase::class ) ) {
			$intUserId = ( true == \Psi\Libraries\UtilFunctions\valId( $intUserId ) ) ? $intUserId : SYSTEM_USER_ID;

			$objCustomerPaymentAccount = \Psi\Eos\Entrata\CCustomerPaymentAccounts::createService()->fetchSimpleCustomerPaymentAccountByIdByCid( $intCustomerPaymentAccountId, $intCid, $objDatabase );

			if( true == \Psi\Libraries\UtilFunctions\valObj( $objCustomerPaymentAccount, CCustomerPaymentAccount::class ) && true == in_array( $objCustomerPaymentAccount->getPaymentTypeId(), CPaymentType::$c_arrintCreditCardPaymentTypes ) && true == \Psi\Libraries\UtilFunctions\valId( $objCustomerPaymentAccount->getSecureReferenceNumber() ) ) {
				$objStoredBillingInfoProcesses = new \CStoredBillingInfoProcesses();
				if( true == \Psi\Libraries\UtilFunctions\valArr( $arrmixAdditionalBinFlags ) ) {
					$objCustomerPaymentAccount = $objStoredBillingInfoProcesses->setBinFlagDetails( $objCustomerPaymentAccount, $objCustomerPaymentAccount->getSecureReferenceNumber(), $arrmixAdditionalBinFlags );
				}
				if( true == \Psi\Libraries\UtilFunctions\valArr( $arrboolBinFlags ) ) {
					$objCustomerPaymentAccount->setIsCreditCard( $arrboolBinFlags['is_credit_card'] );
					$objCustomerPaymentAccount->setIsDebitCard( $arrboolBinFlags['is_debit_card'] );
					$objCustomerPaymentAccount->setIsCheckCard( $arrboolBinFlags['is_check_card'] );
					$objCustomerPaymentAccount->setIsGiftCard( $arrboolBinFlags['is_gift_card'] );
					$objCustomerPaymentAccount->setIsPrepaidCard( $arrboolBinFlags['is_prepaid_card'] );
					$objCustomerPaymentAccount->setIsInternationalCard( $arrboolBinFlags['is_international_card'] );
					$objCustomerPaymentAccount->setIsCommercialCard( $arrboolBinFlags['is_commercial_card'] );
				}
				if( ( true == \Psi\Libraries\UtilFunctions\valArr( $arrmixAdditionalBinFlags ) || true == \Psi\Libraries\UtilFunctions\valArr( $arrboolBinFlags ) ) && false == $objCustomerPaymentAccount->update( $intUserId, $objDatabase ) ) {
					trigger_error( 'Failed to update Customer Payment Account Id[' . $objCustomerPaymentAccount->getId() . '] ', E_USER_WARNING );
				}
			}
		}
	}

	/**
	 * @param $objPaymentBankAccount
	 */
	public function setPaymentBankAccount( $objPaymentBankAccount ) {
		$this->m_objPaymentBankAccount = $objPaymentBankAccount;
	}

	/**
	 * @return mixed
	 */
	public function getPaymentBankAccount() {
		return $this->m_objPaymentBankAccount;
	}

	/**
	 * @param string $strCountryCode
	 * @return |null
	 */
	public function determineCountryDebitPricingFlag( $strCountryCode = CCountry::CODE_USA ) {
		if( !in_array( $this->getPaymentTypeId(), CPaymentTYpe::$c_arrintCreditCardPaymentTypes ) ) {
			return NULL;
		}

		/**
		 * This indicator is used on the front end to determine if we show debit or credit fees
		 */
		$boolUseDebitPricing = false;
		if( ( true == $this->getIsDebitCard() || true == $this->getIsCheckCard() || true == $this->getIsGiftCard() || true == $this->getIsPrepaidCard() ) && false == $this->getIsCreditCard() ) {
			if( \CCountry::CODE_CANADA != $strCountryCode || ( \CCountry::CODE_CANADA == $strCountryCode && $this->getIsInternationalCard() !== true && $this->getIsPrepaidCard() !== true ) ) {
				$boolUseDebitPricing = true;
			}
		}

		$this->setUseDebitPricing( $boolUseDebitPricing );
	}

}
?>