<?php

class CSubsidyCertificationReasonType extends CBaseSubsidyCertificationReasonType {

	const TTP_EQUALS_EXCEEDS_GROSS_RENT_OR_MOVING_TO_MARKET_RENT			= 1;
	const DID_NOT_SUPPLY_CITIZENSHIP_DOCUMENTATION							= 2;
	const DID_NOT_RECERTIFY_ON_TIME											= 3;
	const TENANT_REFUSED_TO_TRANSFER_OR_SUBMITTED_FALSE_DATA				= 4;
	const SUBSIDY_CONTRACT_EXPIRED_NOT_RENEWED								= 5;
	const INELIGIBLE_STUDENT												= 6;
	const DOUBLE_SUBSIDY_AT_MOVE_IN											= 7;
	const NATURAL_DISASTER_OR_UNINHABITABLE_UNIT							= 8;
	const HUD_ABATED_UNIT													= 9;
	const SUBSTANTIAL_REHAB_OR_REPAIR_TENANT_EXPECTED_TO_RETURN				= 10;
	const RESIDENT_DID_NOT_QUALIFY_FOR_SUBSIDY_AT_MI_NOT_DOUBLE_SUBSIDY		= 11;
	const OTHER																= 12;
	const CONTRACT_TERMINATED_FOR_ENFORCEMENT_ACTION						= 13;
	const TRACS_GENERATED_TERMINATION_FOR_FAILURE_TO_RECERTIFY				= 14;
	const DID_NOT_RECERTIFY_ON_TIME_DEPRECATED								= 15;
	const OWNER_INITIATED_NON_PAYMENT_OF_RENT								= 16;
	const OWNER_INITIATED_OTHER												= 17;
	const TENANT_INITIATED_OTHER											= 18;
	const DEATH_OF_SOLE_FAMILY_MEMBER										= 19;
	const UNIT_TRANSFER_BETWEEN_TWO_PROJECTS								= 20;
	const TRACS_USE_ONLY													= 21;
	const ABANDONED_UNIT													= 22;
	const FAILURE_TO_SUBMIT_SSN												= 23;
	const UNINHABITABLE_UNIT_ABATED											= 24;
	const SUBSTANTIAL_REHAB_OR_REPAIR										= 25;
	const RAD_TO_HOUSING_CHOICE_VOUCHER_CHOICE_MOBILITY_OPTION_EXERCISED	= 26;
	const SECTION_8_RAD_TENANT_TRANSFERRED_TO_OTHER_HOUSING_DURING_REHAB	= 27;

	const DEATH_OF_SOLE_FAMILY_MEMBER_CODE = 4;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyCertificationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>