<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissionConditionTypes
 * Do not add any new functions to this class.
 */

class CTransmissionConditionTypes extends CBaseTransmissionConditionTypes {

	public static function fetchTransmissionConditionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTransmissionConditionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchTransmissionConditionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTransmissionConditionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>