<?php

class CEntrataTabletModuleType extends CBaseEntrataTabletModuleType {

	const NAVIGATION_ITEM 	= 1;
	const DASHBOARD_ITEM	= 2;
	const DASHBOARD_GRAPH	= 3;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
          		break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public static function getEntrataTabletModuleTypeName( $intEntrataTabletModuleType ) {
    	switch( $intEntrataTabletModuleType ) {
    		case self::NAVIGATION_ITEM:
    			return 'navigation_item';

    		case self::DASHBOARD_ITEM:
    			return 'dashboard_item';

    		case self::DASHBOARD_GRAPH:
    			return 'dashboard_graph';

    		default:
    			return 'other';
    		break;
    	}
    }

}
?>