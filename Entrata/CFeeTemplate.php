<?php

use Psi\Eos\Entrata\CProperties;

class CFeeTemplate extends CBaseFeeTemplate {

	const FIXED_AMOUNT			= 'Fixed';
	const CASH_RECEIPTS			= 'Cash Receipts';
	const PERCENT_OF_REVENUE	= 'Percent of Revenue';

	protected $m_intApPayeeId;
	protected $m_intBankAccountId;
	protected $m_intPropertyCount;
	protected $m_intPropertyGroupId;

	protected $m_strPropertyName;
	protected $m_strPostMonth;
	protected $m_strArPostMonth;
	protected $m_strApRoutingTagName;
	protected $m_strInvoicePostMonth;
	protected $m_strPaymentPostMonth;
	protected $m_strPaymentDate;
	protected $m_strApPaymentTypeName;
	protected $m_strFeeRuleTypeName;
	protected $m_strFeeCalculationTypeName;

	protected $m_arrintPropertyGroupIds;
	protected $m_arrobjFeeTemplateRules;

	/**
	 * Create Functions
	 */

	public function createFeeTemplateRule() {

		$objFeeTemplateRule = new CFeeTemplateRule();

		$objFeeTemplateRule->setDefaults();
		$objFeeTemplateRule->setFeeTemplateId( $this->getId() );
		$objFeeTemplateRule->setCid( $this->getCid() );

		return $objFeeTemplateRule;
	}

	public function createFeeTemplatePropertyGroup() {

		$objFeeTemplatePropertyGroup = new CFeeTemplatePropertyGroup();

		$objFeeTemplatePropertyGroup->setDefaults();
		$objFeeTemplatePropertyGroup->setFeeTemplateId( $this->getId() );
		$objFeeTemplatePropertyGroup->setCid( $this->getCid() );

		return $objFeeTemplatePropertyGroup;
	}

	public function createManagementFee( $intPeriodId, $strPostMonth, $intPropertyId ) : CFee {

		$objManagementFee = new CFee();

		$objManagementFee->setDefaults();
		$objManagementFee->setCid( $this->getCid() );
		$objManagementFee->setFeeTypeId( CFeeType::MANAGEMENT_FEES );
		$objManagementFee->setFeeTemplateId( $this->getId() );
		$objManagementFee->setPeriodId( $intPeriodId );
		$objManagementFee->setIsPostAr( $this->getIsPostAr() );
		$objManagementFee->setArCodeId( $this->getArCodeId() );
		$objManagementFee->setPostMonth( $strPostMonth );
		$objManagementFee->setPropertyId( $intPropertyId );
		$objManagementFee->setFeeCalculationTypeId( $this->getFeeCalculationTypeId() );
		$objManagementFee->setIsAutoPay( $this->getIsAutoPay() );
		$objManagementFee->setName( $this->getName() );
		$objManagementFee->setIsLatest( true );

		return $objManagementFee;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getApPaymentTypeName() {
		return $this->m_strApPaymentTypeName;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function getPaymentPostMonth() {
		return $this->m_strPaymentPostMonth;
	}

	public function getInvoicePostMonth() {
		return $this->m_strInvoicePostMonth;
	}

	public function getPaymentDate() {
		return $this->m_strPaymentDate;
	}

	public function getPropertyGroupId() {
		return $this->m_intPropertyGroupId;
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function getPropertyGroupIds() {
		return $this->m_arrintPropertyGroupIds;
	}

	public function getFeeTemplateRules() {
		return $this->m_arrobjFeeTemplateRules;
	}

	public function getFeeRuleTypeName() {
		return $this->m_strFeeRuleTypeName;
	}

	public function getApRoutingTagName() {
		return $this->m_strApRoutingTagName;
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function getFeeCalculationTypeName() {
		return $this->m_strFeeCalculationTypeName;
	}

	public function getArPostMonth() {
		return $this->m_strArPostMonth;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setPostMonth( $strPostMonth ) {

		if( true == valStr( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
				$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
			}
		}

		$this->m_strPostMonth = $strPostMonth;
	}

	public function setPaymentPostMonth( $strPaymentPostMonth ) {

		if( true == valStr( $strPaymentPostMonth ) ) {

			$arrstrPaymentPostMonth = explode( '/', $strPaymentPostMonth );

			if( true == valArr( $arrstrPaymentPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPaymentPostMonth ) ) {
				$strPaymentPostMonth = $arrstrPaymentPostMonth[0] . '/01/' . $arrstrPaymentPostMonth[1];
			}
		}
		$this->m_strPaymentPostMonth = $strPaymentPostMonth;
	}

	public function setInvoicePostMonth( $strInvoicePostMonth ) {

		if( true == valStr( $strInvoicePostMonth ) ) {

			$arrstrInvoicePostMonth = explode( '/', $strInvoicePostMonth );

			if( true == valArr( $arrstrInvoicePostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrInvoicePostMonth ) ) {
				$strInvoicePostMonth = $arrstrInvoicePostMonth[0] . '/01/' . $arrstrInvoicePostMonth[1];
			}
		}

		$this->m_strInvoicePostMonth = $strInvoicePostMonth;
	}

	public function setPaymentDate( $strPaymentDate ) {
		$this->m_strPaymentDate = $strPaymentDate;
	}

	public function setPropertyGroupId( $intPropertyGroupId ) {
		$this->m_intPropertyGroupId = $intPropertyGroupId;
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->m_intBankAccountId = $intBankAccountId;
	}

	public function setApPaymentTypeName( $strApPaymentTypeName ) {
		$this->m_strApPaymentTypeName = $strApPaymentTypeName;
	}

	public function setFeeTemplateId( $intFeeTemplateId ) {
		$this->m_intFeeTemplateId = $intFeeTemplateId;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPropertyGroupIds( $arrintPropertyGroupIds ) {
		$this->m_arrintPropertyGroupIds = $arrintPropertyGroupIds;
	}

	public function setFeeTemplateRules( $arrobjFeeTemplateRules ) {
		$this->m_arrobjFeeTemplateRules = $arrobjFeeTemplateRules;
	}

	public function setFeeRuleTypeName( $strFeeRuleTypeName ) {
		$this->m_strFeeRuleTypeName = $strFeeRuleTypeName;
	}

	public function setApRoutingTagName( $strApRoutingTagName ) {
		$this->m_strApRoutingTagName = $strApRoutingTagName;
	}

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = $intApPayeeId;
	}

	public function setFeeCalculationTypeName( $strFeeCalculationTypeName ) {
		$this->m_strFeeCalculationTypeName = $strFeeCalculationTypeName;
	}

	public function setArPostMonth( $strArPostMonth ) {

		if( true == valStr( $strArPostMonth ) ) {

			$arrstrArPostMonth = explode( '/', $strArPostMonth );

			if( true == valArr( $arrstrArPostMonth ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrArPostMonth ) ) {
				$strArPostMonth = $arrstrArPostMonth[0] . '/01/' . $arrstrArPostMonth[1];
			}
		}

		$this->m_strArPostMonth = $strArPostMonth;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_count'] ) ) $this->setPropertyCount( $arrmixValues['property_count'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['property_group_id'] ) ) $this->setPropertyGroupId( $arrmixValues['property_group_id'] );
		if( true == isset( $arrmixValues['ap_payment_type_name'] ) ) $this->setApPaymentTypeName( $arrmixValues['ap_payment_type_name'] );
		if( true == isset( $arrmixValues['post_month'] ) ) $this->setPostMonth( $arrmixValues['post_month'] );
		if( true == isset( $arrmixValues['payment_post_month'] ) ) $this->setPaymentPostMonth( $arrmixValues['payment_post_month'] );
		if( true == isset( $arrmixValues['invoice_post_month'] ) ) $this->setInvoicePostMonth( $arrmixValues['invoice_post_month'] );
		if( true == isset( $arrmixValues['payment_date'] ) ) $this->setPaymentDate( $arrmixValues['payment_date'] );
		if( true == isset( $arrmixValues['bank_account_id'] ) ) $this->setBankAccountId( $arrmixValues['bank_account_id'] );
		if( true == isset( $arrmixValues['property_group_ids'] ) ) $this->setPropertyGroupIds( $arrmixValues['property_group_ids'] );
		if( true == isset( $arrmixValues['fee_rule_type_name'] ) ) $this->setFeeRuleTypeName( $arrmixValues['fee_rule_type_name'] );
		if( true == isset( $arrmixValues['ap_routing_tag_name'] ) ) $this->setApRoutingTagName( $arrmixValues['ap_routing_tag_name'] );
		if( true == isset( $arrmixValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrmixValues['ap_payee_id'] );
		if( true == isset( $arrmixValues['fee_calculation_type_name'] ) ) $this->setFeeCalculationTypeName( $arrmixValues['fee_calculation_type_name'] );
		if( true == isset( $arrmixValues['ar_post_month'] ) ) $this->setArPostMonth( $arrmixValues['ar_post_month'] );
		return;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountId( $objClientDatabase ) {
		$boolIsValid = true;
		if( false == valId( $this->getGlAccountId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Management fee expense GL account is required.' ) ) );
		} else {
			$arrstrPropertyNames = ( array ) CProperties::createService()->fetchPropertiesNotAssociatedToGlAccountIdByPropertyIdsByCid( $this->getGlAccountId(), $this->getPropertyGroupIds(), $this->getCid(), $objClientDatabase );

			if( true == valArr( $arrstrPropertyNames ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', 'The Management Fee Expense Account selected is not associated to [' . implode( ',', array_keys( rekeyArray( 'property_name', $arrstrPropertyNames ) ) ) . '].  Please select a different property mix or a different GL Account.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valApPaymentTypeId() {
		$boolIsValid = true;

		if( 1 == $this->getIsAutoPay() && true == is_null( $this->getApPaymentTypeId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payment_type_id', __( 'Payment Type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Template name is required.' ) ) );
			return $boolIsValid;
		}

		if( true == is_null( $objClientDatabase ) ) {
			return $boolIsValid;
		}

		$strWhereSql = ' WHERE cid = ' . ( int ) $this->getCid() . '
							AND fee_type_id = ' . $this->getFeeTypeId() . '
							AND LOWER( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $this->getName() ) ) ) . '\'
							AND id <> ' . ( int ) $this->getId();

		if( 0 < \Psi\Eos\Entrata\CFeeTemplates::createService()->fetchFeeTemplateCount( $strWhereSql, $objClientDatabase ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Provided template name already exist.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMinAmount() {
		$boolIsValid = true;

		if( false == is_null( $this->getMinAmount() ) && 0 > $this->getMinAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_amount', __( 'Minimum fee should be positive.' ) ) );
		}

		if( false == is_null( $this->getMinAmount() ) && false == is_null( $this->getMaxAmount() ) && ( 1 == bccomp( $this->getMinAmount(), $this->getMaxAmount() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_amount', __( 'Minimum fee should be less than or equal to maximum fee.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaxAmount() {
		$boolIsValid = true;

		if( false == is_null( $this->getMaxAmount() ) && 0 > $this->getMaxAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_amount', __( 'Maximum fee should be positive.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsCashBasis() {

		$boolIsValid = true;

		if( true == is_null( $this->getIsCashBasis() ) || false == is_bool( $this->getIsCashBasis() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_cash_basis', __( 'Calculate fees using is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyGroupIds() {

		$boolIsValid = true;

		if( false == valArr( $this->getPropertyGroupIds() ) && true != $this->getIsDisabled() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group_ids', __( 'At least one property is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApPayeePropertyGroups( $objClientDatabase ) {

		$boolIsValid 						= true;
		$arrintUnAssociatedPropertyGroupIds		= [];
		$arrstrUnAssociatedPropertyNames	= [];

		if( false == valArr( $this->getPropertyGroupIds() ) || NULL == $this->getApPayeeLocationId() ) {
			return $boolIsValid;
		}

		$arrobjApPayeePropertyGroups	= ( array ) CApPayeePropertyGroups::fetchApPayeePropertiesByApPayeeLocationIdByPropertyIdsByCid( $this->getApPayeeLocationId(), $this->getPropertyGroupIds(), $this->getCid(), $objClientDatabase );
		$arrobjApPayeePropertyGroups	= rekeyObjects( 'PropertyId', $arrobjApPayeePropertyGroups );

		$arrintUnAssociatedPropertyGroupIds	= array_diff( $this->getPropertyGroupIds(), array_keys( $arrobjApPayeePropertyGroups ) );
		$arrobjProperties				= ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByPropertyGroupIdsByCid( $arrintUnAssociatedPropertyGroupIds, $this->getCid(), $objClientDatabase );

		foreach( $arrintUnAssociatedPropertyGroupIds as $intUnAssociatedPropertyGroupId ) {
			$arrstrUnAssociatedPropertyNames[$intUnAssociatedPropertyGroupId] = $arrobjProperties[$intUnAssociatedPropertyGroupId]->getPropertyName();
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrUnAssociatedPropertyNames ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_properties', __( "Property(s) '{%s, 0}' is/are not associated with selected vendor.", [ implode( '\', \'', $arrstrUnAssociatedPropertyNames ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valApPayeeLocations( $objClientDatabase ) {

		$boolIsValid			= true;

		if( false === valId( $this->getApPayeeLocationId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', __( 'Management company vendor is required.' ) ) );
			return false;
		}

		$arrobjApPayeeLocations	= ( array ) CApPayeeLocations::fetchEnabledApPayeeLocationsByIdsByCid( [ $this->getApPayeeLocationId() ], $this->getCid(), $objClientDatabase );

		if( false == array_key_exists( $this->getApPayeeLocationId(), $arrobjApPayeeLocations ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location', __( 'You have selected a disabled vendor location.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRoutingTagRequired() {

		$boolIsValid = true;

		if( false == valId( $this->getApRoutingTagId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_routing_tag_id', __( 'Routing tag is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRoutingTagPresent( $arrobjApRoutingTags = [] ) {

		$boolIsValid = true;

		if( true == $this->getApRoutingTagId() && false == array_key_exists( $this->getApRoutingTagId(), $arrobjApRoutingTags ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_routing_tag_id', __( 'You had selected a disabled routing tag.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsAutoPay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;

		if( false == valStr( $this->getPostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );
		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getPostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valInvoicePostMonth() {
		$boolIsValid = true;

		if( false == valStr( $this->getInvoicePostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_post_month', __( 'Invoice Post month is required.' ) ) );
		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getInvoicePostMonth() ) ) {

			$this->setInvoicePostMonth( '' );
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPaymentPostMonth() {
		$boolIsValid = true;

		if( 1 == $this->getIsAutoPay() && false == valStr( $this->getPaymentPostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_post_month', __( 'Payment Post month is required.' ) ) );
		} elseif( 1 == $this->getIsAutoPay() && false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getPaymentPostMonth() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_post_month', __( 'Payment Post month of format mm/yyyy is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPaymentDate() {
		$boolIsValid = true;

		if( 1 == $this->getIsAutoPay() && false == valStr( $this->getPaymentDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_date', __( 'Payment date is required.' ) ) );
		} elseif( 1 == $this->getIsAutoPay() && false == CValidation::validateDate( $this->getPaymentDate() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_date', __( 'Payment date should be in mm/dd/yyyy format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBankAccountId() {
		$boolIsValid = true;

		if( 1 == $this->getIsAutoPay() && true == is_null( $this->getBankAccountId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_id', __( 'Bank account is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApPayeeType( $objClientDatabase ) {
		$boolIsValid = true;

		if( NULL == $this->getApPayeeLocationId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', __( 'Management company vendor is required.' ) ) );
			return false;
		}

		$objApPayee = CApPayees::fetchApPayeeByApPayeeLocationIdByCid( $this->getApPayeeLocationId(), $this->getCid(), $objClientDatabase );

		if( true === valObj( $objApPayee, 'CApPayee' ) && CApPayeeType::INTERCOMPANY != $objApPayee->getApPayeeTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', __( "Vendor '{%s,0}' is not client vendor, please select another Vendor.", [ $objApPayee->getCompanyName() ] ) ) );
			$boolIsValid &= false;
		}

		if( true === valObj( $objApPayee, 'CApPayee' ) && CApPayeeStatusType::ACTIVE != $objApPayee->getApPayeeStatusTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', __( "Vendor '{%s, 0}' is not active client vendor, please select another Vendor.", [ $objApPayee->getCompanyName() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valFeeTemplate() {
		$boolIsValid = true;

		if( true == $this->getIsDisabled() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fee_template', __( "Fee template '{%s, 0}' is disabled.", [ $this->getName() ] ) ) );

			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valIntercompanyArEntity( $strPropertyName ) {
		$boolIsValid = true;

		if( false == valStr( $strPropertyName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_post_ar', __( 'Intercompany AR Entity is a mandatory field. Please update vendor record.' ) ) );

			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valManagementFeeChargeCode( $intArCodeTypeId ) {
		$boolIsValid = true;

		if( false == valId( $this->getArCodeId() ) || CArCodeType::MANAGEMENT_FEES != $intArCodeTypeId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Please select a management fee charge code.' ) ) );

			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPercent() {
		$boolIsValid = true;

		if( true == in_array( $this->getFeeCalculationTypeId(), [ CFeeCalculationType::PERCENT_OF_CASH_RECEIPTS, CFeeCalculationType::PERCENT_OF_REVENUE ] ) ) {
			if( true == is_null( $this->getPercent() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent', __( 'Management fee\'s ' . ( CFeeCalculationType::PERCENT_OF_REVENUE == $this->getFeeCalculationTypeId() ? 'percent of revenue' : 'percent of cash' ) . ' is required.' ) ) );
				$boolIsValid &= false;
			} elseif( 0 >= $this->getPercent() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent', __( 'Management fee\'s ' . ( CFeeCalculationType::PERCENT_OF_REVENUE == $this->getFeeCalculationTypeId() ? 'percent of revenue' : 'percent of cash' ) . ' should be positive or greater than zero.' ) ) );
				$boolIsValid &= false;
			} elseif( -100 > $this->getPercent() || 100 < $this->getPercent() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent', __( 'Percentage should be less than or equal to {%d, 0}.', [ 100 ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valFixedAmount() {
		$boolIsValid = true;

		if( CFeeCalculationType::FIXED_AMOUNT == $this->getFeeCalculationTypeId() ) {
			if( true == is_null( $this->getFixedAmount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_amount', __( 'Management fee\'s fixed amount is required.' ) ) );
				$boolIsValid &= false;
			} elseif( 0 >= $this->getFixedAmount() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_amount', __( 'Management fee\'s fixed amount should be positive or greater than zero.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valFeeCalculationTypeId() {
		$boolIsValid = true;

		if( false === valId( $this->getFeeCalculationTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fee_calculation_type_id', __( 'Management fee is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valDistributionFixedAmount() {
		$boolIsValid = true;

		if( 0 > $this->getFixedAmount() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_amount', 'Default amount should be greater than or equal to zero.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixParameters = [] ) {

		$boolIsValid				= true;
		$boolIsRoutingTagsRequired	= ( true == isset( $arrmixParameters['is_routing_tags_required'] ) ) ? $arrmixParameters['is_routing_tags_required'] : false;
		$boolUseRoutingTags			= ( true == isset( $arrmixParameters['use_routing_tags'] ) ) ? $arrmixParameters['use_routing_tags'] : false;

		$intArCodeTypeId			= ( true == isset( $arrmixParameters['ar_code_type_id'] ) ) ? $arrmixParameters['ar_code_type_id'] : NULL;

		$strPropertyName			= ( true == isset( $arrmixParameters['property_name'] ) ) ? $arrmixParameters['property_name'] : NULL;

		$objClientDatabase			= ( true == isset( $arrmixParameters['client_database'] ) ) ? $arrmixParameters['client_database'] : NULL;

		$arrobjApRoutingTags		= ( true == isset( $arrmixParameters['ap_routing_tags'] ) ) ? $arrmixParameters['ap_routing_tags'] : NULL;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valApPayeeType( $objClientDatabase );
				$boolIsValid &= $this->valPropertyGroupIds();
				$boolIsValid &= $this->valGlAccountId( $objClientDatabase );
				$boolIsValid &= $this->valApPaymentTypeId();
				$boolIsValid &= $this->valMinAmount();
				$boolIsValid &= $this->valMaxAmount();
				$boolIsValid &= $this->valIsCashBasis();
				$boolIsValid &= $this->valApPayeePropertyGroups( $objClientDatabase );
				$boolIsValid &= $this->valApPayeeLocations( $objClientDatabase );
				$boolIsValid &= $this->valFeeCalculationTypeId();
				$boolIsValid &= $this->valPercent();
				$boolIsValid &= $this->valFixedAmount();
				if( true == $boolIsRoutingTagsRequired ) {
					$boolIsValid &= $this->valRoutingTagRequired();
				}
				if( true == $boolUseRoutingTags ) {
					$boolIsValid &= $this->valRoutingTagPresent( $arrobjApRoutingTags );
				}
				if( true == $this->getIsPostAr() ) {
					$boolIsValid &= $this->valIntercompanyArEntity( $strPropertyName );
					$boolIsValid &= $this->valManagementFeeChargeCode( $intArCodeTypeId );
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_edit_calculation':
				$boolIsValid &= $this->valApPaymentTypeId();
				$boolIsValid &= $this->valApPayeeType( $objClientDatabase );
				$boolIsValid &= $this->valPostMonth();
				$boolIsValid &= $this->valInvoicePostMonth();
				$boolIsValid &= $this->valPaymentPostMonth();
				$boolIsValid &= $this->valPaymentDate();
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valFeeTemplate();
				break;

			case 'generate_management_fee':
				$boolIsValid &= $this->valFeeTemplate();
				break;

			case 'distribution_template':
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valPropertyGroupIds();
				$boolIsValid &= $this->valDistributionFixedAmount();
				break;

			case 'owner_distribution':
				$boolIsValid &= $this->valFeeTemplate();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function addFeeTemplateRule( $objFeeTemplateRule ) {
		$this->m_arrobjFeeTemplateRules[$objFeeTemplateRule->getId()] = $objFeeTemplateRule;
	}

	public function __clone()
	{
		parent::__clone();

		if( valArr( $this->getFeeTemplateRules() ) ) {
			foreach( $this->getFeeTemplateRules() as $intFeeTemplateRuleId => $objFeeTemplateRule ) {
				$this->m_arrobjFeeTemplateRules[$intFeeTemplateRuleId] = clone $objFeeTemplateRule;
			}
		}
	}

}
?>