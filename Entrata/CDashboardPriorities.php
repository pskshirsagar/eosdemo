<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDashboardPriorities
 * Do not add any new functions to this class.
 */

class CDashboardPriorities extends CBaseDashboardPriorities {

	public static function fetchDashboardPriorityByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						dashboard_priorities
					WHERE
						cid = ' . ( int ) $intCid . '
					LIMIT 1';
		return self::fetchDashboardPriority( $strSql, $objDatabase );
	}

	public static function fetchDashboardPriorityByFieldNamesByCid( $arrstrFields, $intCid, $objDatabase ) {

		$arrstrDashboardPriority = [];
		if( false == valArr( $arrstrFields ) ) return $arrstrDashboardPriority;

		$strSql = 'SELECT
						' . implode( ',', $arrstrFields ) . '
					FROM
						dashboard_priorities
					WHERE
						cid = ' . ( int ) $intCid . '
					LIMIT 1';

		$arrstrResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrResult ) ) {

			foreach( $arrstrResult[0] as $strKey => $arrstrPriority ) {
				$arrstrDashboardPriority[$strKey] = ( array ) json_decode( $arrstrPriority );
			}
		}

		return $arrstrDashboardPriority;
	}

}
?>