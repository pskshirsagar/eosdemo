<?php

class COffer extends CBaseOffer {
	protected $m_intTierSpecialId;
	protected $m_intSpecialId;
	protected $m_intMinDaysToLeaseStart;
	protected $m_intArOriginReferenceId;
	protected $m_intSpecialTypeId;
	protected $m_intQuantityRemaining;

	protected $m_arrobjTierOffers;
	protected $m_arrobjUnitTypes;
	protected $m_arrobjOfferItems;

	protected $m_strName;
	protected $m_strRentDiscount;
	protected $m_strRentDiscountTrigger;

	protected $m_fltTaxPercent;

	const FUTURE_OFFER			= 0;
	const CUSTOM_OFFER			= 1;
	const CURRENT_OFFER			= 2;

	public function __construct( $arrmixOffer = NULL ) {

		parent::__construct();
		$this->setId( $arrmixOffer['id'] );
		$this->setdaysOffset( $arrmixOffer['days_offset'] );
		$this->setStartDate( $arrmixOffer['start_date'] );
		$this->setEndDate( $arrmixOffer['end_date'] );
		$this->setMaxOfferCount( $arrmixOffer['max_offer_count'] );
		$this->setName( $arrmixOffer['name'] );
		$this->setMinDaysToLeaseStart( $arrmixOffer['min_days_to_lease_start'] );
		$this->setQuantityRemaining( $arrmixOffer['quantity_remaining'] );
		$this->setSpecialTypeId( $arrmixOffer['special_type_id'] );
		$this->setRentDiscount( $arrmixOffer['rent_discount'] );
		$this->setRentDiscountTrigger( $arrmixOffer['rent_discount_trigger'] );
	}

	public function getOfferItems() {
		return ( array ) $this->m_arrobjOfferItems;
	}

	public function getQuantityRemaining( $intCid = NULL, $objDatabase = NULL ) {
		if( false === valId( $this->m_intQuantityRemaining ) && true === valId( $intCid ) ) {
			$intUsedCapCount = COffers::fetchRemainingCapCountByOfferIdBYCid( $this->getId(), $intCid, $objDatabase );
			return $this->m_intQuantityRemaining = $this->getMaxOfferCount() - $intUsedCapCount;
		}
		return $this->m_intQuantityRemaining;
	}

	public function setOfferItems( $arrobjOfferItems ) {
		$this->m_arrobjOfferItems = $arrobjOfferItems;
	}

	public function getTierSpecialId() {
		return $this->m_intTierSpecialId;
	}

	public function getSpecialId() {
		return $this->m_intSpecialId;
	}

	public function getTierOffers() {
		return $this->m_arrobjTierOffers;
	}

	public function getUnitTypes() {
		return $this->m_arrobjUnitTypes;
	}

	public function getName() {
		return $this->m_strName;
	}

	public function getMinDaysToLeaseStart() {
		return $this->m_intMinDaysToLeaseStart;
	}

	public function getArOriginReferenceId() {
		return $this->m_intArOriginReferenceId;
	}

	public function getSpecialTypeId() {
		return $this->m_intSpecialTypeId;
	}

	public function getTaxPercent() {
		return ( float ) $this->m_fltTaxPercent;
	}

	public function getRentDiscount() {
		return $this->m_strRentDiscount;
	}

	public function getRentDiscountTrigger() {
		return $this->m_strRentDiscountTrigger;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['tier_special_id'] ) ) $this->setTierSpecialId( $arrmixValues['tier_special_id'] );
		if( isset( $arrmixValues['special_id'] ) ) $this->setSpecialId( $arrmixValues['special_id'] );
		if( isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
		if( isset( $arrmixValues['min_days_to_lease_start'] ) ) $this->setMinDaysToLeaseStart( $arrmixValues['min_days_to_lease_start'] );
		if( isset( $arrmixValues['ar_origin_reference_id'] ) ) $this->setArOriginReferenceId( $arrmixValues['ar_origin_reference_id'] );
		if( true == isset( $arrmixValues['tax_percent'] ) ) $this->setTaxPercent( $arrmixValues['tax_percent'] );
		if( isset( $arrmixValues['rent_discount'] ) ) $this->setRentDiscount( $arrmixValues['rent_discount'] );
		if( isset( $arrmixValues['rent_discount_trigger'] ) ) $this->setRentDiscountTrigger( $arrmixValues['rent_discount_trigger'] );
	}

	public function setTierSpecialId( $intTierSpecialId ) {
		$this->m_intTierSpecialId = $intTierSpecialId;
	}

	public function setSpecialId( $intSpecialId ) {
		$this->m_intSpecialId = $intSpecialId;
	}

	public function setTierOffers( $arrobjTierOffers ) {
		$this->m_arrobjTierOffers = $arrobjTierOffers;
	}

	public function setUnitTypes( $arrobjUnitTypes ) {
		$this->m_arrobjUnitTypes = $arrobjUnitTypes;
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setMinDaysToLeaseStart( $intMinDaysToLeaseStart ) {
		$this->m_intMinDaysToLeaseStart = $intMinDaysToLeaseStart;
	}

	public function setArOriginReferenceId( $intArOriginReferenceId ) {
		$this->m_intArOriginReferenceId = $intArOriginReferenceId;
	}

	public function setSpecialTypeId( $intSpecialTypeId ) {
		$this->m_intSpecialTypeId = $intSpecialTypeId;
	}

	public function setQuantityRemaining( $intQuantityRemaining ) {
		$this->m_intQuantityRemaining = $intQuantityRemaining;
	}

	public function setTaxPercent( $fltTaxPercent ) {
		$this->set( 'm_fltTaxPercent', CStrings::strToFloatDef( $fltTaxPercent, NULL, false, 6 ) );
	}

	public function setRentDiscount( $strRentDiscount ) {
		$this->m_strRentDiscount = $strRentDiscount;
	}

	public function setRentDiscountTrigger( $strRentDiscountTrigger ) {
		$this->m_strRentDiscountTrigger = $strRentDiscountTrigger;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOfferTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTemplateOffset() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDaysOffset() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxOfferCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportOfferId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) : bool {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function addOfferItem( COfferItem $objOfferItem ) {
		$this->m_arrobjOfferItems[$objOfferItem->getId()] = $objOfferItem;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>