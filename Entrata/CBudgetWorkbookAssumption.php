<?php

class CBudgetWorkbookAssumption extends CBaseBudgetWorkbookAssumption {

	const KEY_DEFAULT_ITEM_PRICE = 'default_item_price';

	protected $m_boolIsUsedInFormula;
	protected $m_strBudgetAssumptionName;
	protected $m_strBudgetAssumptionDescription;
	protected $m_strBudgetAssumptionKey;

	public function valName( $arrintAssumptionNameCounts, &$arrmixValidations ) {
		$boolIsValid = true;

		if( CBudgetAssumptionType::CUSTOM === $this->getBudgetAssumptionTypeId() ) {
			if( false == valStr( $this->getName() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Custom Assumption name is missing.' ) ) );
				$arrmixValidations['assumption_name_required_validation'] = true;
			} elseif( 1 < getArrayElementByKey( \Psi\CStringService::singleton()->strtolower( $this->getName() ), $arrintAssumptionNameCounts ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Assumption' ) . ' ' . $this->getName() . ' ' . __( 'already exists.' ) ) );

				if( false == valArr( preg_grep( '/' . $this->getName() . '/i', $arrmixValidations['assumption_names'] ) ) ) {
					$arrmixValidations['assumption_names'][] = $this->getName();
				}
			}
		}

		return $boolIsValid;
	}

	public function valKey( $arrintAssumptionKeyCounts, &$arrmixValidations ) {
		$boolIsValid = true;

		if( ( CBudgetAssumptionType::CUSTOM === $this->getBudgetAssumptionTypeId() && true == valStr( $this->getKey() ) && 1 < getArrayElementByKey( $this->getKey(), $arrintAssumptionKeyCounts ) ) || self::KEY_DEFAULT_ITEM_PRICE == $this->getKey() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', __( 'Assumption Key' ) . ' ' . $this->getKey() . ' ' . __( 'already exists.' ) ) );

			if( false == in_array( $this->getKey(), $arrmixValidations['assumption_keys'] ) ) {
				$arrmixValidations['assumption_keys'][] = $this->getKey();
			}
		}

		return $boolIsValid;
	}

	public function valDescription( &$arrmixValidations ) {
		$boolIsValid = true;

		if( CBudgetAssumptionType::CUSTOM === $this->getBudgetAssumptionTypeId() ) {
			if( false == valStr( $this->getDescription() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Custom description is missing.' ) ) );
				$arrmixValidations['assumption_description_required_validation'] = true;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixParameters = [], &$arrmixValidations = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_or_update_budget_workbook_assumption':
				$boolIsValid &= $this->valName( $arrmixParameters['assumption_name_counts'], $arrmixValidations );
				$boolIsValid &= $this->valKey( $arrmixParameters['assumption_key_counts'], $arrmixValidations );
				$boolIsValid &= $this->valDescription( $arrmixValidations );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Function
	 */
	public function getBudgetAssumptionName() {
		return $this->m_strBudgetAssumptionName;
	}

	public function getBudgetAssumptionDescription() {
		return $this->m_strBudgetAssumptionDescription;
	}

	public function getBudgetAssumptionKey() {
		return $this->m_strBudgetAssumptionKey;
	}

	public function getIsUsedInFormula() {
		return $this->m_boolIsUsedInFormula;
	}

	/**
	 * Set Function
	 */
	public function setBudgetAssumptionName( $strBudgetAssumptionName ) {
		$this->m_strBudgetAssumptionName = $strBudgetAssumptionName;
	}

	public function setBudgetAssumptionDescription( $strBudgetAssumptionDescription ) {
		$this->m_strBudgetAssumptionDescription = $strBudgetAssumptionDescription;
	}

	public function setBudgetAssumptionKey( $strBudgetAssumptionKey ) {
		$this->m_strBudgetAssumptionKey = $strBudgetAssumptionKey;
	}

	public function setIsUsedInFormula( $boolIsUsedInFormula ) {
		$this->m_boolIsUsedInFormula = CStrings::strToBool( $boolIsUsedInFormula );
	}

	/*
	 * Other Function
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['is_used_in_formula'] ) && $boolDirectSet ) {
			$this->set( 'm_boolIsUsedInFormula', trim( stripcslashes( $arrmixValues['is_used_in_formula'] ) ) );
		} elseif( isset( $arrmixValues['is_used_in_formula'] ) ) {
			$this->setIsUsedInFormula( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_used_in_formula'] ) : $arrmixValues['is_used_in_formula'] );
		}

		if( true == isset( $arrmixValues['budget_assumption_name'] ) ) {
			$this->setBudgetAssumptionName( $arrmixValues['budget_assumption_name'] );
		}
		if( true == isset( $arrmixValues['budget_assumption_description'] ) ) {
			$this->setBudgetAssumptionDescription( $arrmixValues['budget_assumption_description'] );
		}

		if( true == isset( $arrmixValues['budget_assumption_key'] ) ) {
			$this->setBudgetAssumptionKey( $arrmixValues['budget_assumption_key'] );
		}

	}

}
?>