<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeContacts
 * Do not add any new functions to this class.
 */

class CApPayeeContacts extends CBaseApPayeeContacts {

	public static function fetchApPayeeContactByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $intCid, $objClientDatabase ) {
		return self::fetchApPayeeContact( sprintf( "SELECT * FROM %s WHERE cid = %d AND remote_primary_key = '%s' LIMIT 1", 'ap_payee_contacts', ( int ) $intCid, \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strRemotePrimaryKey ) ) ) ), $objClientDatabase );
	}

	public static function fetchSimpleApPayeeContactsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM ap_payee_contacts WHERE ap_payee_id = ' . ( int ) $intApPayeeId . ' AND cid = ' . ( int ) $intCid . ' AND disabled_by IS NULL AND disabled_on IS NULL ORDER BY id';
		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchIntegratedApPayeeContactsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apc.remote_primary_key IS NOT NULL
						AND apc.disabled_by IS NULL
						AND apc.disabled_on IS NULL';

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase, $boolIsCheckName ) {

		$strWhere = '';
		if( false != $boolIsCheckName ) {
			$strWhere = ' AND ( name_first IS NOT NULL OR name_last IS NOT NULL )';
		}

		if( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						' . $strWhere . '
						
					ORDER BY
						apcl.is_primary DESC,
						LOWER( apc.name_first ),
						LOWER( apc.name_last )';

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactByIdByApPayeeIdByCid( $intId, $intApPayeeId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM ap_payee_contacts WHERE id = ' . ( int ) $intId . ' AND ap_payee_id = ' . ( int ) $intApPayeeId . ' AND cid = ' . ( int ) $intCid . ' AND disabled_by IS NULL AND disabled_on IS NULL';
		return self::fetchApPayeeContact( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactsByIdsByApPayeeIdByCid( $arrintIds, $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apc.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
					ORDER BY
						apcl.is_primary DESC,
						LOWER( apc.name_first ),
						LOWER( apc.name_last )';

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactsByApPayeeContactTypeIdByApPayeeIdByCid( $intApPayeeContactTypeId, $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_contacts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_payee_contact_type_id = ' . ( int ) $intApPayeeContactTypeId;

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactByApPayeeContactTypeIdByApPayeeIdByCid( $boolIsPrimary, $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apc.*,
						ap.company_name,
						ap.ap_payee_status_type_id,
						apl.location_name,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payees ap
						JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid AND apc.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
						LEFT JOIN ap_payee_locations apl ON ( apc.cid = apl.cid AND apc.ap_payee_id = apl.ap_payee_id AND apcl.ap_payee_location_id = apl.id )
					WHERE
						apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apc.cid = ' . ( int ) $intCid . '
						AND apcl.is_primary = ' . ( bool ) $boolIsPrimary . '::BOOLEAN
						AND apc.disabled_by IS NULL
						AND apc.disabled_on IS NULL
					LIMIT 1';

		return self::fetchApPayeeContact( $strSql, $objClientDatabase );
	}

	public static function fetchWarrantyApPayeeContactByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apc.*,
						ap.company_name,
						ap.ap_payee_status_type_id,
						apl.location_name
					FROM
						ap_payees ap
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid AND apc.ap_payee_id = ap.id AND apc.is_warranty IS TRUE )
						JOIN ap_payee_locations apl ON ( ap.id = apl.ap_payee_id AND apl.cid = ap.cid  )
					WHERE
						ap.id = ' . ( int ) $intApPayeeId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND apl.is_primary IS TRUE
					LIMIT 1';

		return self::fetchApPayeeContact( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactsByUsernameByApPayeeTypeIdByCid( $strUsername, $intApPayeeTypeId, $intCid, $objClientDatabase ) {

		if( false == valStr( $strUsername ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apc.*
					FROM
						ap_payees ap
						JOIN ap_payee_contacts apc ON ( ap.cid = apc.cid AND ap.id = apc.ap_payee_id )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND ( ap.username = \'' . addslashes( $strUsername ) . '\' OR apc.email_address = \'' . addslashes( $strUsername ) . '\' )
						AND ap.ap_payee_type_id = ' . ( int ) $intApPayeeTypeId;

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactByEmailAddressByCid( $strEmailAddress, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apc.*
					FROM
						ap_payee_contacts apc
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.email_address ILIKE E\'' . trim( addslashes( $strEmailAddress ) ) . '\'
					LIMIT 1';

		return self::fetchApPayeeContact( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactByEmailAddressByIdByCid( $strEmailAddress, $intApPayeeContactId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intApPayeeContactId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apc.*
					FROM
						ap_payee_contacts apc
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.email_address ILIKE E\'' . trim( addslashes( $strEmailAddress ) ) . '\'
						AND apc.id = ' . ( int ) $intApPayeeContactId . '
					LIMIT 1';

		return self::fetchApPayeeContact( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactByApPayeeIdByFirstNameByLastNameByCid( $intApPayeeId, $strNameFirst, $strNameLast, $intCid, $objClientDatabase ) {
		if( false == valStr( $strNameFirst ) && false == valStr( $strNameLast ) ) return NULL;

		$strSql = ( true == valStr( $strNameFirst ) ) ? 'AND name_first ILIKE E\'' . addslashes( trim( $strNameFirst ) ) . '\'' : '';
		$strSql = ( true == valStr( $strNameLast ) ) ? $strSql . ' AND name_last ILIKE E\'' . addslashes( trim( $strNameLast ) ) . '\'' : $strSql;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_contacts
					WHERE
						cid= ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId .
						$strSql . '
					LIMIT 1';

		return self::fetchApPayeeContact( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactDataByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase, $arrintLocationIds = NULL ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strWhereCondition = '';

		if( true == valArr( $arrintLocationIds ) ) {
			$strWhereCondition = ' AND apl.id IN (' . implode( ',', $arrintLocationIds ) . ')';
		}

		$strSql = ' SELECT
						apc.id,
						apc.name_last,
						apc.name_first,
						apc.title,
						apc.email_address,
						apc.phone_number,
						apc.extension,
						apc.worker_id,
						array_to_string( array_agg( apl.location_name ),  \',\' ) as location_names
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
						LEFT JOIN ap_payee_locations apl ON ( apc.cid = apl.cid AND apc.ap_payee_id = apl.ap_payee_id AND apcl.ap_payee_location_id = apl.id )
					WHERE
						apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apc.cid = ' . ( int ) $intCid . $strWhereCondition . '
						AND apc.name_first IS NOT NULL
					GROUP BY
						apc.id,
						apc.name_last,
						apc.name_first,
						apc.title,
						apc.email_address,
						apc.phone_number,
						apc.extension,
						apc.worker_id
					ORDER BY apc.id DESC';

		$arrmixRecords = fetchData( $strSql, $objClientDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		return $arrmixRecords;
	}

	public static function fetchApPayeeContactsByIdsByApPayeeIdsByCid( $arrintIds, $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apc.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
					ORDER BY
						LOWER( apc.name_first ),
						LOWER( apc.name_last )';

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchPrimaryApPayeeContactsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						JOIN owners o  ON (  o.cid = apc.cid  and apc.ap_payee_id = o.ap_payee_id )
						JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.disabled_by IS NULL
						AND apc.disabled_on IS NULL
						AND ( apc.name_first IS NOT NULL OR apc.name_last IS NOT NULL )
						AND apcl.is_primary = true
					ORDER BY
						LOWER( apc.name_first ),
						LOWER( apc.name_last )';

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactsByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apc.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND apc.cid = ' . ( int ) $intCid;

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase, $boolIsName = true ) {

		$strWhere = '';

		if( true == $boolIsName ) {
			$strWhere = 'AND ( name_first IS NOT NULL OR name_last IS NOT NULL )';
		}

		$strSql = 'SELECT
						id,
						cid,
						name_first || \' \'  ||name_last as name_first,
						email_address,
						worker_id
					FROM
						ap_payee_contacts
					WHERE
						ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid		= ' . ( int ) $intCid . ' ' .
						$strWhere;
		return self::fetchApPayeeContacts( $strSql, $objDatabase );
	}

	public static function fetchApPayeeContactByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = ' SELECT
						apc.*
					FROM
						ap_payee_locations apl
						JOIN ap_payee_contacts apc ON ( apl.cid = apc.cid AND apl.is_primary = TRUE )
						JOIN ap_payee_contact_locations apcl ON ( apc.cid = apcl.cid AND apl.id = apcl.ap_payee_location_id AND apc.id = apcl.ap_payee_contact_id AND apcl.is_primary = TRUE )
					WHERE
						apl.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apc.cid = ' . ( int ) $intCid;

		return self::fetchApPayeeContact( $strSql, $objClientDatabase );

	}

	public static function fetchApPayeeContactByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						apc.*,
						apl.location_name,
						apl.notes,
						apcl.is_primary,
						apl.disabled_by,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
						LEFT JOIN ap_payee_locations apl ON ( apc.cid = apl.cid AND apc.ap_payee_id = apl.ap_payee_id AND apcl.ap_payee_location_id = apl.id )
					WHERE
						apc.id = ' . ( int ) $intId . '
						AND apc.cid = ' . ( int ) $intCid;

		return self::fetchApPayeeContact( $strSql, $objDatabase );
	}

	public static function fetchApPayeeContactsWithPrimaryApPayeeLocationByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = apc.cid AND apl.ap_payee_id = apc.ap_payee_id )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id AND apcl.ap_payee_location_id = apl.id )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND ( apc.name_last IS NOT NULL
							  OR apc.name_first IS NOT NULL)
						AND apl.is_primary = true
					ORDER BY
						apcl.is_primary DESC,
						LOWER( apc.name_first ),
						LOWER( apc.name_last )';

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactWithPrimaryApPayeeLocationByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						apc.*,
						apl.location_name,
						apl.notes,
						apcl.is_primary,
						apl.disabled_by,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
						LEFT JOIN ap_payee_locations apl ON ( apc.cid = apl.cid AND apc.ap_payee_id = apl.ap_payee_id AND apcl.ap_payee_location_id = apl.id )
					WHERE
						apc.id = ' . ( int ) $intId . '
						AND apc.cid = ' . ( int ) $intCid . '
						AND apl.is_primary = true';

		return self::fetchApPayeeContact( $strSql, $objDatabase );
	}

	public static function fetchBlankApPayeeContactByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = ' SELECT
						apc.*
					FROM
						ap_payee_contacts apc
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.name_first IS NULL
						AND apc.name_last IS NULL
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
					LIMIT 1';

		return self::fetchApPayeeContact( $strSql, $objClientDatabase );

	}

	public static function fetchApPayeeContactsByApPayeeIdByApLegalEntityIdByCid( $intApPayeeId, $intApLegalEntityId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apc.*
					FROM
						ap_payee_contacts apc
						JOIN ap_payee_contact_locations apcl ON ( apc.id = apcl.ap_payee_contact_id AND apcl.cid = apc.cid )
						JOIN ap_payee_locations apl ON ( apcl.ap_payee_location_id = apl.id AND apl.cid = apcl.cid )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apc.disabled_by IS NULL
						AND apc.disabled_on IS NULL
						AND ( apc.name_first IS NOT NULL OR name_last IS NOT NULL )
						AND apc.email_address IS NOT NULL
						AND apl.ap_legal_entity_id =' . ( int ) $intApLegalEntityId;

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchSyncApPayeeContactsByApLegalEntityIdsByCid( $arrintApLegalEntityIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApLegalEntityIds ) ) return NULL;

		$strSql = 'SELECT
						apc.*,
						apl.id as ap_payee_location_id
					FROM
						ap_payee_contacts apc
						JOIN ap_payee_contact_locations apcl ON ( apc.id = apcl.ap_payee_contact_id AND apc.cid = apcl.cid )
						JOIN ap_payee_locations apl ON ( apcl.ap_payee_location_id = apl.id and apcl.cid = apl.cid )
					WHERE
						apl.ap_legal_entity_id IN (' . implode( ',', $arrintApLegalEntityIds ) . ' )
						AND  apl.cid = ' . ( int ) $intCid . '
						AND apl.disabled_by IS NULL
						AND apc.disabled_by IS NULL
						AND apc.worker_id IS NOT NULL';

		return self::fetchApPayeeContacts( $strSql, $objDatabase, false );
	}

	public static function fetchUnsyncApPayeeContactsByApLegalEntityIdsByCid( $arrintApLegalEntityIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApLegalEntityIds ) ) return NULL;

		$strSql = 'SELECT
						apc.*,
						apl.location_name,
						apcl.ap_payee_location_id,
						apl.ap_legal_entity_id
					FROM
						ap_payee_contacts apc
						JOIN ap_payee_contact_locations apcl ON ( apc.id = apcl.ap_payee_contact_id AND apc.cid = apcl.cid )
						JOIN ap_payee_locations apl ON ( apcl.ap_payee_location_id = apl.id and apcl.cid = apl.cid AND apl.store_id IS NOT NULL )
					WHERE
						apl.ap_legal_entity_id IN (' . implode( ',', $arrintApLegalEntityIds ) . ' )
						AND apc.cid = ' . ( int ) $intCid . '
						AND apc.worker_id IS NULL
						AND apc.disabled_by IS NULL';

		return self::fetchApPayeeContacts( $strSql, $objDatabase, false );
	}

	public static function fetchApPayeeContactsByWorkerIdsByApPayeeIdsByCid( $arrintWorkerIds, $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintWorkerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_contacts
					WHERE
						worker_id IN ( ' . implode( ',', $arrintWorkerIds ) . ' )
						AND ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchSyncedApPayeeContactsByApPayeeLocationIdsByApPayeeIdByCid( $arrintApPayeeLocationIds, $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						apc.id as id
					FROM
						ap_payee_contacts apc
						JOIN ap_payee_contact_locations apcl ON ( apc.id = apcl.ap_payee_contact_id AND apc.cid = apcl.cid )
					WHERE
						apc.worker_id IS NOT NULL
						AND apcl.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId . '
					 GROUP BY
						apc.id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSyncedApPayeeContactsByUnSyncedApPayeeLocationIdsByApPayeeIdByCid( $arrintApPayeeLocationIds, $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apc.id,
						apc.worker_id
					FROM
						ap_payee_contacts apc
						JOIN ap_payee_contact_locations apcl ON ( apc.id = apcl.ap_payee_contact_id AND apc.cid = apcl.cid )
						JOIN ap_payee_locations apl ON ( apl.id = apcl.ap_payee_location_id AND apl.cid = apcl.cid )
					WHERE
						apc.worker_id IS NOT NULL
						AND apl.store_id IS NOT NULL
						AND apcl.ap_payee_location_id NOT IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id = ' . ( int ) $intApPayeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeContactsByWorkerIdByCids( $intWorkerId, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_contacts
					WHERE
						worker_id = ' . ( int ) $intWorkerId . '
						AND cid IN( ' . sqlIntImplode( $arrintCids ) . ' )';

		return parent::fetchApPayeeContacts( $strSql, $objDatabase, false );
	}

	public static function fetchApPayeeContactsByApPayeeIdsByApLegalEntityIdsByCIds( $arrintApPayeeId, $arrintApLegalEntityId, $arrintCId, $objClientDatabase ) {

		if( false == ( $arrintApPayeeId = getIntValuesFromArr( $arrintApPayeeId ) ) || false == ( $arrintApLegalEntityId = getIntValuesFromArr( $arrintApLegalEntityId ) ) || false == ( $arrintCId = getIntValuesFromArr( $arrintCId ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apc.id,
						apc.cid,
						apc.ap_payee_id,
						apc.name_first,
						apc.name_last,
						apc.email_address,
						apl.ap_legal_entity_id
					FROM
						ap_payee_contacts apc
						JOIN ap_payee_contact_locations apcl ON ( apc.id = apcl.ap_payee_contact_id AND apcl.cid = apc.cid )
						JOIN ap_payee_locations apl ON ( apcl.ap_payee_location_id = apl.id AND apl.cid = apcl.cid )
					WHERE
						apc.cid IN ( ' . sqlIntImplode( $arrintCId ) . ' )
						AND apc.ap_payee_id IN ( ' . sqlIntImplode( $arrintApPayeeId ) . ' )
						AND apc.disabled_by IS NULL
						AND apc.disabled_on IS NULL
						AND ( apc.name_first IS NOT NULL OR name_last IS NOT NULL )
						AND apc.email_address IS NOT NULL
						AND apl.ap_legal_entity_id IN ( ' . sqlIntImplode( $arrintApLegalEntityId ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactsCountsByCids( $arrintCIds, $objDatabase ) {

		if( false == valArr( $arrintCIds ) ) return NULL;

		$strSql = 'SELECT 
						COUNT(apc.id) as total,
						COUNT( CASE WHEN apc.worker_id IS NOT NULL THEN apc.id END ) as synced,
						apc.cid
					FROM
						ap_payee_contacts apc
						JOIN ap_payees app ON( apc.cid = app.cid AND apc.ap_payee_id = app.id )
					WHERE
						apc.cid IN (' . sqlIntImplode( $arrintCIds ) . ' )
						AND app.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND apc.name_last IS NOT NULL
						AND apc.name_first IS NOT NULL
					GROUP BY 
						apc.cid	';

		return fetchData( $strSql, $objDatabase );
	}

	public static function updateApPayeeContactsByIdsByApPayeeIdByCId( $arrintIds, $intApPayeeId, $intCId, $intCompanyUserId ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) {
			return NULL;
		}

		$strSql = 'UPDATE
						ap_payee_contacts
					SET 
						worker_id = NULL,
						updated_on = NOW(),
						updated_by = ' . ( int ) $intCompanyUserId . '
					WHERE
						id IN ( ' . sqlIntImplode( $arrintIds ) . ' ) 
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCId . ';';

		return $strSql;
	}

	public static function fetchAllApPayeeContactsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apc.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
					ORDER BY
						apcl.is_primary DESC,
						LOWER( apc.name_first ),
						LOWER( apc.name_last )';

		return self::fetchApPayeeContacts( $strSql, $objClientDatabase );
	}

	public static function fetchPrimaryEmailAddressByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apc.ap_payee_id, apc.email_address
					FROM
						ap_payee_locations apl
						JOIN ap_payee_contact_locations apcl ON ( apcl.ap_payee_location_id = apl.id )
						JOIN ap_payee_contacts apc ON ( apc.id = apcl.ap_payee_contact_id )
					WHERE
						apl.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND apl.cid = ' . ( int ) $intCid . '
						AND apl.is_primary IS TRUE
						AND apcl.is_primary IS TRUE
						AND apc.email_address IS NOT NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeContactsByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objDatabase, $boolIsPrimary = false ) {
		if( false == valArr( $arrintApPayeeLocationIds ) ) {
			return false;
		}

		$strSql = 'SELECT 
						apc.*,
						apcl.ap_payee_location_id
					FROM ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON (apc.cid = apcl.cid AND apc.id = apcl.ap_payee_contact_id)
					WHERE 
						apcl.ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' ) AND
						apc.cid = ' . ( int ) $intCid;

		if( true == $boolIsPrimary ) {
			$strSql .= ' AND apcl.is_primary = true';
		}

		return self::fetchApPayeeContacts( $strSql, $objDatabase );

	}

	public static function fetchApPayeeContactsByApPayeeFilterByCid( $arrmixFilterData, $intPageNo = 0, $intPageSize = 25, $intCid, $objDatabase ) {

		$arrstrWhere			= [];
		if( true == valIntArr( $arrmixFilterData['ap_payee_ids'] ) ) {
			$arrstrWhere[] = ' AND apc.ap_payee_id IN ( ' . sqlIntImplode( $arrmixFilterData['ap_payee_ids'] ) . ' ) ';
		}

		if( true == valIntArr( $arrmixFilterData['ap_payee_type_ids'] ) ) {
			$arrstrWhere[] = ' AND ap.ap_payee_type_id IN ( ' . sqlIntImplode( $arrmixFilterData['ap_payee_type_ids'] ) . ' ) ';
		}

		if( true == valIntArr( $arrmixFilterData['ap_payee_status_type_ids'] ) ) {
			$arrstrWhere[] = ' AND ap.ap_payee_status_type_id IN ( ' . sqlIntImplode( $arrmixFilterData['ap_payee_status_type_ids'] ) . ' ) ';
		}

		$strSql = 'SELECT
						apc.*,
						apcl.is_primary,
						apcl.ap_payee_location_id,
						ap.company_name as ap_payee_name,
						COUNT( apc.id ) OVER ( PARTITION BY apc.cid ) AS ap_payee_contacts_count
					FROM
						ap_payee_contacts apc
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id )
						JOIN ap_payees ap ON ( apc.cid = ap.cid AND apc.ap_payee_id = ap.id )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						' . implode( PHP_EOL . ' ', $arrstrWhere ) . ' 
						AND apcl.id IS NOT NULL
						AND ( apc.name_last IS NOT NULL
							  OR apc.name_first IS NOT NULL)
					ORDER BY
						apcl.is_primary DESC,
						LOWER( apc.name_first ),
						LOWER( apc.name_last )';

		$strSql .= ' OFFSET ' . ( int ) ( $intPageNo ) . ' LIMIT ' . ( int ) $intPageSize;
		return self::fetchApPayeeContacts( $strSql, $objDatabase );
	}

}
?>