<?php

class CPropertyPhoneNumber extends CBasePropertyPhoneNumber {

	/**
	 * Validation Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Property phone number client id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property phone number property id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumberTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPhoneNumberTypeId ) || ( 1 > $this->m_intPhoneNumberTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_type_id', __( 'Phone number type does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMarketingName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function getPhoneNumberType() {

		$strPhoneNumberType = '';

		switch( $this->getPhoneNumberTypeId() ) {
			case CPhoneNumberType::PRIMARY:
				$strPhoneNumberType = 'Primary';
				break;

			case CPhoneNumberType::HOME:
				$strPhoneNumberType = 'Home';
				break;

			case CPhoneNumberType::MOBILE:
				$strPhoneNumberType = 'Mobile';
				break;

			case CPhoneNumberType::TOLL_FREE:
				$strPhoneNumberType = 'Toll-free';
				break;

			case CPhoneNumberType::OTHER:
				$strPhoneNumberType = 'Other';
				break;

			case CPhoneNumberType::MARKETING:
				$strPhoneNumberType = 'Marketing';
				break;

			case CPhoneNumberType::WEBSITE:
				$strPhoneNumberType = 'Website';
				break;

			case CPhoneNumberType::MAINTENANCE:
				$strPhoneNumberType = 'Maintenance';
				break;

			case CPhoneNumberType::MAINTENANCE_EMERGENCY:
				$strPhoneNumberType = 'Main. Emergency';
				break;

			default:
				// Added default case
				break;
		}

		return $strPhoneNumberType;

	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber( $boolIsFromVacancy = false, $objDatabase = NULL ) {

		$boolIsValid = true;

		if( true == isset( $boolIsFromVacancy ) && true == $boolIsFromVacancy && CPhoneNumberType::OFFICE == $this->getPhoneNumberTypeId() && ( false == isset( $this->m_strPhoneNumber ) || false == ( CValidation::validateFullPhoneNumber( $this->m_strPhoneNumber, false ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Valid phone number is required.' ) ) );
			$boolIsValid = false;
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) && true == $boolIsValid && CPhoneNumberType::INTERNAL != $this->getPhoneNumberTypeId() ) {
			$intPropertyPhoneNumbersCount = CPropertyPhoneNumbers::fetchPreExistingPropertyPhoneNumbersCountByPropertyIdByPhoneNumberTypeIdByCid( $this->getPropertyId(), $this->getPhoneNumberTypeId(), $this->getId(), $this->getCid(), $objDatabase );
			$strPhoneNumberType = $this->getPhoneNumberType();
			if( 0 != $intPropertyPhoneNumbersCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'A  {%s, phone_number_type} phone number already exists.', [ 'phone_number_type' => $strPhoneNumberType ] ) ) );
				$boolIsValid = false;
			}
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) && true == $boolIsValid && CPhoneNumberType::INTERNAL == $this->getPhoneNumberTypeId() ) {
			$intPropertyPhoneNumbersCount = CPropertyPhoneNumbers::fetchPreExistingPropertyPhoneNumbersCountByPropertyIdByPhoneNumberTypeIdByPhoneNumberByCid( $this->getPropertyId(), $this->getPhoneNumberTypeId(), $this->getId(), $this->getPhoneNumber(), $this->getCid(), $objDatabase );
			$strPhoneNumberType = $this->getPhoneNumberType();
			if( 0 != $intPropertyPhoneNumbersCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'A  {%s, phone_number_type} phone number already exists.', [ 'phone_number_type' => $strPhoneNumberType ] ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valIsInternalVanityNumber( $objVoipDatabase ) {

		$boolIsValid = true;

		if( true == valObj( $objVoipDatabase, 'CDatabase' ) && true == $boolIsValid && CPhoneNumberType::INTERNAL == $this->getPhoneNumberTypeId() ) {
			$strPropertyOfficeNumber = \Psi\CStringService::singleton()->substr( $this->m_strPhoneNumber, -10 );
			$objCallPhoneNumber      = CCallPhoneNumbers::fetchCallPhoneNumberByPhoneNumber( $strPropertyOfficeNumber, $objVoipDatabase );

			if( false == is_null( $objCallPhoneNumber ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'The number you have entered is a vanity number and cannot be saved as Internal Number Type.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valOfficePhoneNumber() {
		$boolIsValid = true;

		if( false == isset( $this->m_strPhoneNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Office phone number is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valChecklIsVanityPhoneNumber( $objVoipDatabase ) {

		$boolIsValid = true;

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( __( 'Application Error: Unproper database object provided.' ), E_USER_ERROR );
		}
		$strPropertyOfficeNumber = \Psi\CStringService::singleton()->substr( $this->m_strPhoneNumber, -10 );
		$objCallPhoneNumber		= CCallPhoneNumbers::fetchCallPhoneNumberByPhoneNumber( $strPropertyOfficeNumber, $objVoipDatabase );

		if( false == is_null( $objCallPhoneNumber ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Vanity numbers cannot be used in this field. Please enter another phone number.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */

	public function valPhoneNumberFormat() {
		$boolIsValid = true;

		if( true == isset( $this->m_strPhoneNumber ) && ( 1 !== CValidation::checkPhoneNumberFullValidation( $this->m_strPhoneNumber ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'A valid phone number is required. (XXX-XXX-XXXX)' ) ) );
		}

		return $boolIsValid;
	}

	public function valExtension() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBulkPhoneNumberFormat() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strPhoneNumber ) ) {
			if( true == isset( $this->m_strPhoneNumber ) && ( 1 !== CValidation::checkPhoneNumberFullValidation( $this->m_strPhoneNumber ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'A valid phone number is required. (XXX-XXX-XXXX)' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $objVoipDatabase = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				// $boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valPhoneNumber( false, $objDatabase );
				$boolIsValid &= $this->valIsInternalVanityNumber( $objVoipDatabase );
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valPhoneNumber( false, $objDatabase );
				$boolIsValid &= $this->valIsInternalVanityNumber( $objVoipDatabase );
				break;

			case VALIDATE_DELETE:
				// $boolIsValid &= $this->valId();
				break;

			case 'property_add_wizard_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valChecklIsVanityPhoneNumber( $objVoipDatabase );
				break;

			case 'property_add_wizard_update':
				// $boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valChecklIsVanityPhoneNumber( $objVoipDatabase );
				break;

			case 'vacany_update':
				// $boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				break;

			case 'VALIDATE_INSERT_PHONE':
				$boolIsValid &= $this->valPhoneNumberFormat();
				break;

			case 'validate_bulk_insert_phone':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPhoneNumberTypeId();
				$boolIsValid &= $this->valBulkPhoneNumberFormat();
				break;

			default:
				// added default case
				break;
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated Doesn't handle international phone number formatting.
	 * @see \i18n\CPhoneNumber::format() should be used.
	 */

	public function formatPropertyPhoneNumber( $objWebsiteTemplate = NULL ) {

		$strTempPropertyPhoneNumber = '';

		$strPropertyPhoneNumber = preg_replace( '/\D/', '', $this->getPhoneNumber() );

		if( 0 < mb_strlen( $strPropertyPhoneNumber ) ) {

			if( true == valObj( $objWebsiteTemplate, 'CWebsiteTemplate' ) && false == $objWebsiteTemplate->getIsCustom() && true == is_null( $objWebsiteTemplate->getPrivateCompanyId() ) ) {

				$strPropertyPhoneNumber = '(' . $strPropertyPhoneNumber;
				$intLength = mb_strlen( $strPropertyPhoneNumber ) - 6;
				$strTempPropertyPhoneNumber = mb_substr( $strPropertyPhoneNumber, 0, 4 );
				$strTempPropertyPhoneNumber .= ') ';
				$strTempPropertyPhoneNumber .= mb_substr( $strPropertyPhoneNumber, 4, 3 );
				$strTempPropertyPhoneNumber .= '-';
				$strTempPropertyPhoneNumber .= mb_substr( $strPropertyPhoneNumber, 7, $intLength );
			} else {

				$intLength = mb_strlen( $strPropertyPhoneNumber ) - 6;
				$strTempPropertyPhoneNumber  = mb_substr( $strPropertyPhoneNumber, 0, 3 );
				$strTempPropertyPhoneNumber .= '-';
				$strTempPropertyPhoneNumber .= mb_substr( $strPropertyPhoneNumber, 3, 3 );
				$strTempPropertyPhoneNumber .= '-';
				$strTempPropertyPhoneNumber .= mb_substr( $strPropertyPhoneNumber, 6, $intLength );
			}
		}
		return $strTempPropertyPhoneNumber;
	}

}
?>