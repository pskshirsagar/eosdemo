<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeRuleTypes
 * Do not add any new functions to this class.
 */

class CFeeRuleTypes extends CBaseFeeRuleTypes {

	public static function fetchFeeRuleTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CFeeRuleType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFeeRuleType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CFeeRuleType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFeeRuleTypesByIds( $arrintIds, $objClientDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		return self::fetchFeeRuleTypes( 'SELECT * FROM fee_rule_types WHERE id IN ( ' . implode( ',', $arrintIds ) . ' ) ORDER BY order_num', $objClientDatabase );
	}

}
?>