<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueOptimalRents
 * Do not add any new functions to this class.
 */

class CRevenueOptimalRents extends CBaseRevenueOptimalRents {

	public static function fetchAvailableUnitSpacesRevenueOptimalRentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $boolIsNewlyAvailable = false, $intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM, $boolShowOnWebsite = true, $intBestPrice = 0 ) {

		if( NULL == $intSelectedLeaseTerm ) {
			$intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM;
		}

		if( false == valArr( $arrintPropertyIds ) ) return;

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCondition = ( true == $boolIsNewlyAvailable ) ? ' AND us.available_on = to_char( NOW(), \'MM/DD/YYYY\' )::timestamp::date' : '';

		if( 1 == $intBestPrice ) {
			$strBestOptionCondition = ' JOIN (
										        SELECT *,
										               row_number() OVER(PARTITION BY cid, unit_space_id
										        ORDER BY rent_amt, custom_move_in_date) as rownum1
										        FROM (
										               SELECT *
										               FROM (
										                      SELECT *,
										                             row_number() OVER(PARTITION BY sub1.cid,
										                               sub1.unit_space_id, sub1.custom_move_in_date
										                      ORDER BY rent_amt, sub1.custom_move_in_date) as rownum
										                      FROM (
										                             SELECT sub.*,rentamount rent_amt
										                             FROM (
										                                    SELECT ror.cid,
										                                           ror.unit_space_id,
										                                           j2.moveindate,
										                                           CASE
										                                             WHEN CURRENT_DATE < to_date(
										                                               j2.moveindate, \'YYYYMMDD\') THEN
										                                               to_date(j2.moveindate, \'YYYYMMDD\'
										                                               )
										                                             ELSE CURRENT_DATE
										                                           END as custom_move_in_date,
										                                           j2.leasemonth,
										                                           j2.rentamount,
										                                           ror.id
										                                    FROM revenue_optimal_rents ror
										                                        JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.property_id = ror.property_id AND ut.id = ror.unit_type_id )
																				JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) ' . $strCheckDeletedUnitSpacesSql . ' )
										                                        JOIN property_charge_settings pcs ON ( pcs.cid = us.cid AND pcs.property_id = us.property_id )
																				JOIN lease_terms AS lt ON ( lt.cid = us.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id )
										                                        JOIN LATERAL
										                                         (
										                                           SELECT j1.moveindate,
										                                                  j2.leasemonth,
										                                                  (j2.value ->> 2)::numeric AS
										                                                    rentamount
										                                           FROM jsonb_each(ror.matrix) j1(moveindate, value)
										                                                LEFT JOIN LATERAL jsonb_each(j1.value) j2(leasemonth, value) ON TRUE
										                                         ) j2 ON j2.leasemonth = lt.term_month::text
										                                    WHERE
										                                        ror.cid = ' . ( int ) $intCid . '
										                                        AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
																				AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . '
																				AND us.is_available = TRUE
																				AND lt.is_prospect = TRUE
																				AND lt.is_disabled = FALSE
										                                  ) as sub
										                           ) sub1
										                    ) as inner_query
										               WHERE rownum = 1
										 
										             ) as tmp
										      ) AS temp ON (ror.unit_space_id = temp . unit_space_id AND rownum1 = 1) ';

			$strSelect = '  temp . rent_amt AS custom_optimal_rent,
					        temp . leasemonth AS custom_lease_month,
					        temp.custom_move_in_date ';
		} else {

			$strSelect = ' (matrix::json->(select key from json_each_text(matrix::json) limit 1))::json#>>\'{' . $intSelectedLeaseTerm . ',2}\' AS custom_optimal_rent ';
			$strBestOptionCondition = '';
		}

		$strSql = 'SELECT
						ror.*,' . $strSelect . ' 
						FROM
						revenue_optimal_rents ror
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.property_id = ror.property_id AND ut.id = ror.unit_type_id )
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_charge_settings pcs ON( pcs.cid = us.cid AND pcs.property_id = us.property_id ) '
						. $strBestOptionCondition .
					' WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . '
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
						' . $strCheckShowOnWebsiteSql . '
						AND us.is_available = TRUE
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )
						AND (
								us.unit_space_status_type_id = ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . '
								OR
								(  us.unit_space_status_type_id = ' . CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY . ' )
								OR
								(  us.unit_space_status_type_id = ' . CUnitSpaceStatusType::NOTICE_UNRENTED . ' )
							)
						' . $strCondition . '
					ORDER BY
						ror.property_id,
						ror.unit_type_id';

		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchAvailableUnitSpacesRevenueOptimalRentsDataByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM, $boolShowOnWebsite = true, $intBestPrice = 0 ) {

		if( false == valArr( $arrintPropertyIds ) ) return;

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		if( 1 == $intBestPrice ) {
			$strBestOptionCondition = ' JOIN (
										        SELECT *,
										               row_number() OVER(PARTITION BY cid, unit_space_id
										        ORDER BY rent_amt, custom_move_in_date) as rownum1
										        FROM (
										               SELECT *
										               FROM (
										                      SELECT *,
										                             row_number() OVER(PARTITION BY sub1.cid,
										                               sub1.unit_space_id, sub1.custom_move_in_date
										                      ORDER BY rent_amt, sub1.custom_move_in_date) as rownum
										                      FROM (
										                             SELECT sub.*,rentamount rent_amt
										                             FROM (
										                                    SELECT ror.cid,
										                                           ror.unit_space_id,
										                                           j2.moveindate,
										                                           CASE
										                                             WHEN CURRENT_DATE < to_date(
										                                               j2.moveindate, \'YYYYMMDD\') THEN
										                                               to_date(j2.moveindate, \'YYYYMMDD\'
										                                               )
										                                             ELSE CURRENT_DATE
										                                           END as custom_move_in_date,
										                                           j2.leasemonth,
										                                           j2.rentamount,
										                                           ror.id
										                                    FROM
																				revenue_optimal_rents ror
																				JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.property_id = ror.property_id AND ut.id = ror.unit_type_id )
																				JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) ' . $strCheckDeletedUnitSpacesSql . ' )
																				JOIN property_charge_settings pcs ON ( pcs.cid = us.cid AND pcs.property_id = us.property_id )
																				JOIN lease_terms AS lt ON ( lt.cid = us.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id )
										                                         JOIN LATERAL
										                                         (
										                                           SELECT j1.moveindate,
										                                                  j2.leasemonth,
										                                                  (j2.value ->> 2)::numeric AS
										                                                    rentamount
										                                           FROM jsonb_each(ror.matrix) j1(moveindate, value)
										                                                LEFT JOIN LATERAL jsonb_each(j1.value) j2(leasemonth, value) ON TRUE
										                                         ) j2 ON j2.leasemonth = lt.term_month::text
										                                    WHERE 
																				ror.cid = ' . ( int ) $intCid . '
																				AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
																				AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . '
																				AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
																				' . $strCheckShowOnWebsiteSql . '
																				AND us.is_available = TRUE
																				AND lt.is_prospect = TRUE
																				AND lt.is_disabled = FALSE
										                                  ) as sub
										                           ) sub1
										                    ) as inner_query
										               WHERE rownum = 1
										 
										             ) as tmp
										      ) AS temp ON (ror.unit_space_id = temp . unit_space_id AND rownum1 = 1) ';

			$strSelect = '  temp . rent_amt AS custom_optimal_rent,
					        temp . leasemonth AS custom_lease_month,
					        temp.custom_move_in_date ';
		} else {

			$strSelect = ' (matrix::json->(select key from json_each_text(matrix::json) limit 1))::json#>>\'{' . $intSelectedLeaseTerm . ',2}\' AS custom_optimal_rent ';
			$strBestOptionCondition = '';
		}

		$strSql = 'SELECT
						ror.cid,
						ror.property_id,
						ror.unit_type_id,
						ror.property_unit_id,
						ror.unit_space_id,
						ror.avg_executed_rent,
						ror.last_executed_rent,
						ror.std_advertised_rent,
						ror.avg_optimal_rent,
						ror.std_optimal_rent,
						ror.min_competitive_rent,
						ror.max_competitive_rent,
						ror.avg_competitive_rent,
						ror.available_unit_count,
						ror.occupied_unit_count,
						ror.rentable_unit_count,
						ror.exposed_30_unit_count,
						ror.exposed_60_unit_count,
						ror.exposed_90_unit_count,
						ror.demand_thirty_unit_count,
						ror.demand_sixty_unit_count,
						ror.demand_ninety_unit_count,
						ror.weekly_growth_rate,
						ror.monthly_growth_rate,
						ror.quarterly_growth_rate,
						ror.yearly_growth_rate,
						ror.first_move_in_date, 
						' . $strSelect . '
					FROM
						revenue_optimal_rents ror
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.property_id = ror.property_id AND ut.id = ror.unit_type_id )
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_charge_settings pcs ON( pcs.cid = us.cid AND pcs.property_id = us.property_id )
						' . $strBestOptionCondition . '
						
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . '
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
						' . $strCheckShowOnWebsiteSql . '
						AND us.is_available = TRUE
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )
					ORDER BY
						ror.property_id,
						ror.unit_type_id';
		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchAvailableFromPastUnitSpacesRevenueOptimalRentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ror.*
					FROM
						revenue_optimal_rents ror
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . ' )
						JOIN unit_spaces us ON ( us.cid = ror.cid AND us.id = ror.unit_space_id AND us.is_available = TRUE ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )
						AND GREATEST( us.make_ready_date, us.available_on ) < CURRENT_DATE
		';

		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentsAvailableUnitsByUnitTypeIdsByCid( $arrintUnitTypeIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $boolIsNewlyAvailable = false, $intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM, $boolShowOnWebsite = true, $intBestPrice = 0 ) {

		if( NULL == $intSelectedLeaseTerm ) {
			$intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM;
		}

		if( false == valArr( $arrintUnitTypeIds ) ) return NULL;

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCondition = ( true == $boolIsNewlyAvailable ) ? ' AND us.available_on = to_char( NOW(), \'MM/DD/YYYY\' )::timestamp::date' : '';
		$strCheckSubsidizedUnitSpacesSql = ( 1 == $objDatabase->getClusterId() ) ? ' AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )' : '';

		if( 1 == $intBestPrice ) {
			$strBestOptionCondition = ' JOIN (
										        SELECT *,
										               row_number() OVER(PARTITION BY cid, unit_space_id
										        ORDER BY rent_amt, custom_move_in_date) as rownum1
										        FROM (
										               SELECT *
										               FROM (
										                      SELECT *,
										                             row_number() OVER(PARTITION BY sub1.cid,
										                               sub1.unit_space_id, sub1.custom_move_in_date
										                      ORDER BY rent_amt, sub1.custom_move_in_date) as rownum
										                      FROM (
										                             SELECT sub.*,rentamount rent_amt
										                             FROM (
										                                    SELECT ror.cid,
										                                           ror.unit_space_id,
										                                           j2.moveindate,
										                                           CASE
										                                             WHEN CURRENT_DATE < to_date(
										                                               j2.moveindate, \'YYYYMMDD\') THEN
										                                               to_date(j2.moveindate, \'YYYYMMDD\'
										                                               )
										                                             ELSE CURRENT_DATE
										                                           END as custom_move_in_date,
										                                           j2.leasemonth,
										                                           j2.rentamount,
										                                           ror.id
										                                    FROM revenue_optimal_rents ror
										                                        JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . ' AND ut.deleted_on IS NULL )
																				JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) ' . $strCheckDeletedUnitSpacesSql . ' )
																				JOIN property_charge_settings pcs ON ( pcs.cid = us.cid AND pcs.property_id = us.property_id )
																				JOIN lease_terms AS lt ON ( lt.cid = us.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id )    
										                                        JOIN LATERAL
										                                         (
										                                           SELECT j1.moveindate,
										                                                  j2.leasemonth,
										                                                  (j2.value ->> 2)::numeric AS
										                                                    rentamount
										                                           FROM jsonb_each(ror.matrix) j1(moveindate, value)
										                                                LEFT JOIN LATERAL jsonb_each(j1.value) j2(leasemonth, value) ON TRUE
										                                         ) j2 ON j2.leasemonth = lt.term_month::text
										                                    WHERE ror.cid = ' . ( int ) $intCid . '
										                                        AND ror.unit_type_id IN( ' . implode( ',', $arrintUnitTypeIds ) . ' )
																				AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
																				' . $strCheckShowOnWebsiteSql . '
																				AND us.is_available = TRUE
																				AND lt.is_prospect = TRUE
																				ANd lt.is_disabled = FALSE
										                                  ) as sub
										                           ) sub1
										                    ) as inner_query
										               WHERE rownum = 1
										 
										             ) as tmp
										      ) AS temp ON (ror.unit_space_id = temp . unit_space_id AND rownum1 = 1) ';

			$strSelect = '  temp . rent_amt AS custom_optimal_rent,
					        temp . leasemonth AS custom_lease_month,
					        temp.custom_move_in_date ';
		} else {

			$strSelect = ' (matrix::json->(select key from json_each_text(matrix::json) limit 1))::json#>>\'{' . $intSelectedLeaseTerm . ',2}\' AS custom_optimal_rent ';
			$strBestOptionCondition = '';
		}
		$strSql = 'SELECT
						ror.*, '
		                . $strSelect .
					' FROM
						revenue_optimal_rents ror
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . ' AND ut.deleted_on IS NULL )
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_charge_settings pcs ON( pcs.cid = us.cid AND pcs.property_id = us.property_id ) '
						 . $strBestOptionCondition .
					' WHERE
						ror.cid 	= ' . ( int ) $intCid . '
						AND ror.unit_type_id IN( ' . implode( ',', $arrintUnitTypeIds ) . ' )
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
						' . $strCheckShowOnWebsiteSql . '
						AND us.is_available = TRUE
						' . $strCheckSubsidizedUnitSpacesSql . $strCondition;

		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentsByUnitSpaceIdsByCid( $arrintUnitSpaceIds, $intCid, $objDatabase, $intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM, $intBestPrice = NULL, $boolIsUnitTypeLevel = false ) {
		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;

		if( 1 == $intBestPrice ) {
			$strBestOptionCondition = ' JOIN (
									 SELECT *,
										row_number() OVER(PARTITION BY cid, unit_space_id
								        ORDER BY rent_amt, custom_move_in_date) as rownum1
								        FROM (
								               SELECT *
								               FROM (
								                      SELECT *,
								                             row_number() OVER(PARTITION BY sub1.cid,
								                               sub1.unit_space_id, sub1.custom_move_in_date
								                      ORDER BY rent_amt, sub1.custom_move_in_date) as rownum
								                      FROM (
								                             SELECT sub.*,rentamount rent_amt
								                             FROM (
								                                    SELECT ror.cid,
								                                           ror.unit_space_id,
								                                           j2.moveindate,
								                                           CASE
								                                             WHEN CURRENT_DATE < to_date(
								                                               j2.moveindate, \'YYYYMMDD\') THEN
								                                               to_date(j2.moveindate, \'YYYYMMDD\'
								                                               )
								                                             ELSE CURRENT_DATE
								                                           END as custom_move_in_date,
								                                           j2.leasemonth,
								                                           j2.rentamount,
								                                           ror.id
								                                    FROM revenue_optimal_rents ror
								                                         JOIN LATERAL
								                                         (
								                                           SELECT j1.moveindate,
								                                                  j2.leasemonth,
								                                                  (j2.value ->> 2)::numeric AS
								                                                    rentamount
								                                           FROM jsonb_each(ror.matrix) j1(moveindate, value)
								                                                LEFT JOIN LATERAL jsonb_each(j1.value) j2(leasemonth, value) ON TRUE
								                                         ) j2 ON TRUE
								                                    WHERE ror.cid = ' . ( int ) $intCid . '
								                                        AND ror.unit_space_id IN ( \' . implode( \',\', $arrintUnitSpaceIds ) . \' ) \'
								                                  ) as sub
								                           ) sub1
								                    ) as inner_query
								               WHERE rownum = 1
								 
								             ) as tmp
								      ) AS temp ON (ror.unit_space_id = temp . unit_space_id AND rownum1 = 1) ';

			$strCondition = ' ,temp . rent_amt AS custom_optimal_rent,
					        temp . leasemonth AS custom_lease_month,
					        temp.custom_move_in_date ';
		} else {
			$strCondition = ',(matrix::json->(select key from json_each_text(matrix::json) limit 1))::json#>>\'{' . $intSelectedLeaseTerm . ',2}\' AS custom_optimal_rent';
			$strBestOptionCondition = '';
		}
		$strGroupBy = '';
		if( true == $boolIsUnitTypeLevel ) {
			$strSelect  = 'ror.unit_type_id,AVG(ror.std_optimal_rent) as std_optimal_rent';
			$strGroupBy = 'Group BY ror.unit_type_id';
		} else {
			$strSelect = 'ror.* ' . $strCondition;
		}
		$strSql = sprintf( '
					SELECT 
						%1$s
					FROM
						revenue_optimal_rents ror 
						%2$s
					WHERE
						ror.cid = %3$d
						AND ror.unit_space_id IN ( %4$s )
						%5$s
					', $strSelect, $strBestOptionCondition, $intCid, implode( ', ', $arrintUnitSpaceIds ), $strGroupBy );

		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentsNonDefaultByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $intIsRenewal = 0, $boolShowOnWebsite = true ) {

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCondition = ( 0 == $intIsRenewal ) ? 'WHEN pp.value IN( \'' . CPricingPostRentLibrary::PRICING_LEASES_NEW . '\', \'' . CPricingPostRentLibrary::PRICING_LEASES_BOTH . '\' ) AND us.unit_space_status_type_id IN ( ' . implode( ', ', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) AND us.is_available = TRUE THEN 1' : 'WHEN pp.value IN( \'' . CPricingPostRentLibrary::PRICING_LEASES_RENEWAL . '\', \'' . CPricingPostRentLibrary::PRICING_LEASES_BOTH . '\' ) AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE . ' THEN 1';

		$strSql = 'SELECT
						ror.*
					FROM
						revenue_optimal_rents ror
						JOIN unit_spaces us ON( us.cid = ror.cid AND us.id = ror.unit_space_id ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_preferences pp ON( pp.cid = ror.cid AND pp.property_id = ror.property_id AND pp.key = \'' . CPricingPostRentLibrary::PROPERTY_PREFERENCE_PRICING_LEASES . '\' AND pp.value != \'None\' )
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.unit_type_id = ' . ( int ) $intUnitTypeId . '
						AND ror.unit_space_id IS NOT NULL
						AND ror.matrix IS NOT NULL
						AND ror.std_optimal_rent IS NOT NULL
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
						' . $strCheckShowOnWebsiteSql . '
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )
						AND 1 = CASE
							' . $strCondition . '
						END
					ORDER BY
						ror.unit_space_id';

		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchDefaultUnitTypeRevenueOptimalRentsByUnitTypeIdByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase, $intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM ) {
		$strSql = 'SELECT
						ror.*,
						(matrix::json->(select key from json_each_text(matrix::json) limit 1))::json#>>\'{' . $intSelectedLeaseTerm . ',2}\' AS custom_optimal_rent
					FROM
						revenue_optimal_rents ror
					WHERE
						ror.cid 	= ' . ( int ) $intCid . '
						AND ror.property_id 		= ' . ( int ) $intPropertyId . '
						AND ror.unit_space_id IS NULL';

		$strSql .= ( 0 < $intUnitTypeId ) ? ' AND ror.unit_type_id = ' . ( int ) $intUnitTypeId : '';

		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchDefaultUnitTypeRevenueOptimalRentsByUnitTypeIdsByPropertyIdByCid( $arrintUnitTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM, $intBestOptimal = 0 ) {
		if( false == valArr( $arrintUnitTypeIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strBestOptimalCondition = '';
		$strSelect = ' (matrix::json->(select key from json_each_text(matrix::json) limit 1))::json#>>\'{' . $intSelectedLeaseTerm . ',2}\' AS custom_optimal_rent ';
		if( 1 == $intBestOptimal ) {
			$strBestOptimalCondition = ' JOIN (
										        SELECT *,
										               row_number() OVER(PARTITION BY cid, unit_type_id
										        ORDER BY rent_amt, custom_move_in_date) as rownum1
										        FROM (
										               SELECT *
										               FROM (
										                      SELECT *,
										                             row_number() OVER(PARTITION BY sub1.cid,
										                               sub1.unit_type_id, sub1.custom_move_in_date
										                      ORDER BY rent_amt, sub1.custom_move_in_date) as rownum
										                      FROM (
										                             SELECT sub.*,rentamount rent_amt
										                             FROM (
										                                    SELECT ror.cid,
										                                           ror.unit_type_id,
										                                           j2.moveindate,
										                                           CASE
										                                             WHEN CURRENT_DATE < to_date(
										                                               j2.moveindate, \'YYYYMMDD\') THEN
										                                               to_date(j2.moveindate, \'YYYYMMDD\'
										                                               )
										                                             ELSE CURRENT_DATE
										                                           END as custom_move_in_date,
										                                           j2.leasemonth,
										                                           j2.rentamount,
										                                           ror.id
										                                    FROM revenue_optimal_rents ror
										                                         JOIN LATERAL
										                                         (
										                                           SELECT j1.moveindate,
										                                                  j2.leasemonth,
										                                                  (j2.value ->> 2)::numeric AS
										                                                    rentamount
										                                           FROM jsonb_each(ror.matrix) j1(moveindate, value)
										                                                LEFT JOIN LATERAL jsonb_each(j1.value) j2(leasemonth, value) ON TRUE
										                                         ) j2 ON TRUE
										                                    WHERE ror.cid = ' . ( int ) $intCid . '
										                                  ) as sub
										                           ) sub1
										                    ) as inner_query
										               WHERE rownum = 1
										 
										             ) as tmp
										      ) AS temp ON (ror.unit_type_id = temp . unit_type_id AND rownum1 = 1) ';

			$strSelect = '  temp . rent_amt AS custom_optimal_rent,
					        temp . leasemonth AS custom_lease_month,
					        temp.custom_move_in_date ';

		}

		$strSql = 'SELECT
						ror.*,
						' . $strSelect . '
					FROM
						revenue_optimal_rents ror ' . $strBestOptimalCondition . '
					 WHERE
						ror.cid 	= ' . ( int ) $intCid . '
						AND ror.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ror.unit_type_id IN( ' . implode( ',', $arrintUnitTypeIds ) . ' )
						AND ror.unit_space_id IS NULL
					ORDER BY
						ror.unit_type_id ';
		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchDefaultUnitTypeRevenueOptimalRentByUnitTypeIdByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ror.*						
					FROM
						revenue_optimal_rents ror
					WHERE
						ror.cid 	= ' . ( int ) $intCid . '
						AND ror.property_id 		= ' . ( int ) $intPropertyId . '
						AND ror.unit_type_id 		= ' . ( int ) $intUnitTypeId . '
						AND ror.unit_space_id IS NULL
					ORDER BY
						ror.id DESC
					LIMIT 1';

		return self::fetchRevenueOptimalRent( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		return self::fetchRevenueOptimalRent( sprintf( 'SELECT * FROM revenue_optimal_rents WHERE unit_space_id = %d AND cid = %d ORDER BY id DESC LIMIT 1', ( int ) $intUnitSpaceId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRevenueOptimalRentCountUnitTypeGroupedByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $intIsRenewal = 0, $boolIsNewlyAvailable = false, $boolShowOnWebsite = true ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';
		$strJoinCondition = ( 0 == $intIsRenewal ) ? 'AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) AND us.is_available = TRUE ' : 'AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE;
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces && 1 == $objDatabase->getClusterId() ) ? ' AND us.deleted_on IS NULL' : '';
		$strCondition = ( true == $boolIsNewlyAvailable ) ? 'AND us.available_on = to_char( NOW(), \'MM/DD/YYYY\' )::timestamp::date' : '';
		$strCheckSubsidizedUnitSpacesSql = ( 1 == $objDatabase->getClusterId() ) ? ' AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )' : '';

		$strSql = 'SELECT
						ror.unit_type_id,
						COUNT( ror.id ) AS optimal_rent_count
				   	FROM
						revenue_optimal_rents ror
						JOIN unit_spaces us ON ( us.cid = ror.cid AND us.id = ror.unit_space_id ' . $strJoinCondition . $strCheckDeletedUnitSpacesSql . ' )
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . ' AND ut.deleted_on IS NULL )
				   	WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
						AND ror.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						' . $strCheckShowOnWebsiteSql . '
						' . $strCheckSubsidizedUnitSpacesSql . '
						' . $strCondition . '
				   	GROUP BY
						ror.unit_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentsPropertiesByCidByPropertyIds( $intCid, $strContractedPropertyIds, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT( ror.property_id ),
						p.property_name
					FROM
						revenue_optimal_rents AS ror
						JOIN properties AS p ON ( p.cid = ror.cid AND p.id = ror.property_id )
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id IN ( ' . $strContractedPropertyIds . ' )
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentsUnitTypesByCidByPropertyId( $intCid, $intPropertyId, $intEnableDefaultUnitType, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $intRenewal = 0, $boolShowOnWebsite = true ) {

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		if( 1 == $intEnableDefaultUnitType ) {
			$strCondition = '';
		} elseif( 0 == $intRenewal ) {
			$strCondition = ' AND us.unit_space_status_type_id IN ( ' . implode( ',', CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) . ' ) AND us.is_available = TRUE ';
		} else {
			$strCondition = ' AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE;
		}

		$strSql = 'SELECT
						DISTINCT( ror.unit_type_id )
					FROM
						revenue_optimal_rents AS ror
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.property_id = ror.property_id AND ut.id = ror.unit_type_id )
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid ' . $strCondition . ' ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						ror.cid 	= ' . ( int ) $intCid . '
						AND ror.property_id 		= ' . ( int ) $intPropertyId . '
						AND ut.is_occupiable 		= ' . CUnitType::IS_OCCUPIABLE_YES . '
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
						' . $strCheckShowOnWebsiteSql . '
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )
					ORDER BY
						ror.unit_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExposureAndDemandCountsByUnitTypeIdByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
    					COALESCE( ROUND( SUM( ror.exposed_30_unit_count ) ), 0 ) AS exposed30,
    					COALESCE( ROUND( SUM( ror.exposed_60_unit_count ) ), 0 ) AS exposed60,
    					COALESCE( ROUND( SUM( ror.exposed_90_unit_count ) ), 0 ) AS exposed90,
						COALESCE( ROUND( SUM( ror.demand_thirty_unit_count ) ), 0 ) AS demand30,
						COALESCE( ROUND( SUM( ror.demand_sixty_unit_count ) ), 0 )  AS demand60,
						COALESCE( ROUND( SUM( ror.demand_ninety_unit_count ) ), 0 ) AS demand90
					FROM
   	 					revenue_optimal_rents ror
					WHERE
    					ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id = ' . ( int ) $intPropertyId . '
						AND ror.unit_space_id IS NOT NULL';

		$strSql .= ( 0 < $intUnitTypeId ) ? ' AND ror.unit_type_id = ' . ( int ) $intUnitTypeId : '';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentOccupiedUnitsCountByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $boolShowOnWebsite = true ) {

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						SUM( ror.occupied_unit_count ) AS occupied_unit_count
					FROM
						revenue_optimal_rents ror
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid ' . $strCheckShowOnWebsiteSql . $strCheckDeletedUnitSpacesSql . ' )
						JOIN unit_types ut ON ( ut.cid = us.cid AND ut.id = us.unit_type_id )
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.occupied_unit_count > 0
						AND ror.property_id = ' . ( int ) $intPropertyId . '
						AND ror.unit_space_id IS NOT NULL
						AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . '
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL ) ';
		$strSql .= ( 0 < $intUnitTypeId ) ? ' AND ror.unit_type_id = ' . ( int ) $intUnitTypeId : '';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRenewalUnitSpacesRevenueOptimalRentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM, $boolShowOnWebsite = true, $boolHideWithOffer = false,$strMinLeaseEndDate = NULL, $strMaxLeaseEndDate = NULL, $intIsPendingOffersUnitOnly = 0, $intBestPrice = 0 ) {
		if( NULL == $intSelectedLeaseTerm ) {
			$intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM;
		}

		if( false == valArr( $arrintPropertyIds ) ) return;

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		if( ( ( true == is_null( $strMinLeaseEndDate ) || '' == $strMinLeaseEndDate ) && ( true == is_null( $strMaxLeaseEndDate ) || '' == $strMaxLeaseEndDate ) ) || ( ( 'undefined' == $strMinLeaseEndDate ) && ( 'undefined' == $strMaxLeaseEndDate ) ) ) {
			$strSqlCondition = 'CURRENT_DATE AND CURRENT_DATE + CAST ( COALESCE( rd.days, \'90\' ) || \' days\' AS INTERVAL ) ';
		} else {
			$strMinLeaseEndDate = date( 'm/d/Y', strtotime( $strMinLeaseEndDate ) );
			$strMaxLeaseEndDate   = date( 'm/d/Y', strtotime( $strMaxLeaseEndDate ) );
			$strSqlCondition = '\'' . $strMinLeaseEndDate . '\'::DATE AND ' . '\'' . $strMaxLeaseEndDate . ' \'::DATE';
		}
		if( false == $boolHideWithOffer ) {
			$arrintSkipLeaseStatusTypeIds[] = CLeaseStatusType::APPLICANT;
		} else {
			$arrintSkipLeaseStatusTypeIds[] = CLeaseStatusType::FUTURE;
		}

		$strJoins = ' JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable =  ' . CUnitType::IS_OCCUPIABLE_YES . ' AND ut.deleted_on IS NULL )
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE . ' AND us.unit_exclusion_reason_type_id IN (' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )' . $strCheckShowOnWebsiteSql . $strCheckDeletedUnitSpacesSql . ' )
						JOIN
						(
							SELECT
								l.cid,
								l.unit_space_id,
								l.id,
								l.active_lease_interval_id,
								li.lease_interval_type_id,
								li.lease_end_date,
								li.lease_start_date,
								li.lease_term_id,
								ROW_NUMBER() OVER( PARTITION BY l.cid, l.unit_space_id ORDER BY l.active_lease_interval_id DESC ) AS row_num
							FROM
								leases l
								JOIN lease_intervals AS li ON( li.cid = l.cid AND li.property_id = l.property_id AND li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ', ' . CLeaseStatusType::APPLICANT . ' ) )
								JOIN lease_customers AS lc ON( lc.cid = l.cid AND lc.property_id = l.property_id AND lc.lease_id = l.id AND lc.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ', ' . CLeaseStatusType::APPLICANT . ' ) )
							WHERE
								l.cid = ' . ( int ) $intCid . '
						) AS leases ON ( leases.cid = us.cid AND leases.unit_space_id = us.id AND leases.row_num = 1 )
						JOIN lease_processes AS lp ON( lp.cid = us.cid AND lp.lease_id = leases.id )
						LEFT JOIN lease_intervals rli ON ( rli.cid = leases.cid AND rli.lease_id = leases.id AND rli.lease_interval_type_id IN( ' . CLeaseIntervalType::RENEWAL . ' ) AND rli.lease_status_type_id IN (' . implode( ',', $arrintSkipLeaseStatusTypeIds ) . ') )
						LEFT JOIN lease_terms AS lt ON( lt.cid = us.cid AND lt.id = leases.lease_term_id ) AND lt.is_disabled = FALSE AND lt.deleted_on IS NULL 
						LEFT JOIN property_charge_settings pcs ON( pcs.cid = us.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND pcs.property_id = us.property_id )
						LEFT JOIN renewal_days rd ON( rd.cid = ror.cid AND rd.property_id = ror.property_id ) ';
		$strSqlPendingOffersUnitOnlyCondition = ' rli.id IS NULL ';

		$strWhereCond = ' AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND COALESCE( us.occupancy_type_id, 0 ) != ' . ( int ) COccupancyType::AFFORDABLE . '
						AND ' . $strSqlPendingOffersUnitOnlyCondition . '
						AND ( ( ( leases.lease_end_date IS NOT NULL AND leases.lease_end_date BETWEEN ' . $strSqlCondition . ' )
								OR ( leases.lease_end_date IS NULL AND lp.move_out_date IS NOT NULL AND lp.move_out_date BETWEEN ' . $strSqlCondition . ' )
								OR ( leases.lease_end_date IS NULL AND lp.move_out_date IS NULL AND leases.lease_start_date IS NOT NULL AND leases.lease_start_date + ( interval \'1 months\' * COALESCE( lt.term_month, 12 ) ) BETWEEN ' . $strSqlCondition . ' ) 
								)
							OR ( leases.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ) ) ';
		if( 1 == $intIsPendingOffersUnitOnly ) {
			$strSqlPendingOffersUnitOnlyCondition = ' rli.id IS NOT NULL ';
		}
		if( 1 == $intBestPrice ) {
			$strBestOptionCondition = ' JOIN (
										        SELECT *,
										               row_number() OVER(PARTITION BY cid, unit_space_id
										        ORDER BY rent_amt, custom_move_in_date) as rownum1
										        FROM (
										               SELECT *
										               FROM (
										                      SELECT *,
										                             row_number() OVER(PARTITION BY sub1.cid,
										                               sub1.unit_space_id, sub1.custom_move_in_date
										                      ORDER BY rent_amt, sub1.custom_move_in_date) as rownum
										                      FROM (
										                             SELECT sub.*,rentamount rent_amt
										                             FROM (
										                                    SELECT ror.cid,
										                                           ror.unit_space_id,
										                                           j2.moveindate,
										                                           CASE
										                                             WHEN CURRENT_DATE < to_date(
										                                               j2.moveindate, \'YYYYMMDD\') THEN
										                                               to_date(j2.moveindate, \'YYYYMMDD\'
										                                               )
										                                             ELSE CURRENT_DATE
										                                           END as custom_move_in_date,
										                                           j2.leasemonth,
										                                           j2.rentamount,
										                                           ror.id
										                                    FROM revenue_optimal_rents ror
										                                        ' . $strJoins . '
										                                         LEFT JOIN lease_terms AS lt1 ON (lt1.cid = us.cid AND pcs.lease_term_structure_id = lt1.lease_term_structure_id)
										                                         JOIN LATERAL
										                                         (
										                                           SELECT j1.moveindate,
										                                                  j2.leasemonth,
										                                                  (j2.value ->> 2)::numeric AS
										                                                    rentamount
										                                           FROM jsonb_each(ror.matrix) j1(moveindate, value)
										                                                LEFT JOIN LATERAL jsonb_each(j1.value) j2(leasemonth, value) ON TRUE
										                                         ) j2 ON j2.leasemonth = lt1.term_month::text AND lt1.is_renewal = TRUE AND lt1.is_disabled = FALSE
										                                    WHERE ror.cid = ' . ( int ) $intCid . $strWhereCond . '
										                                  ) as sub
										                           ) sub1
										                    ) as inner_query
										               WHERE rownum = 1
										 
										             ) as tmp
										      ) AS temp ON (ror.unit_space_id = temp . unit_space_id AND rownum1 = 1) ';

			$strSelect = '  temp . rent_amt AS custom_optimal_rent,
					        temp . leasemonth AS custom_lease_month,
					        temp.custom_move_in_date ';
		} else {

			$strSelect = ' (matrix::json->(select key from json_each_text(matrix::json) limit 1))::json#>>\'{' . $intSelectedLeaseTerm . ',2}\' AS custom_optimal_rent ';
			$strBestOptionCondition = '';
		}

		$strSql = 'WITH renewal_days AS (
						SELECT
							p.cid,
							p.id AS property_id,
							GREATEST(CASE
										WHEN sgt.id = 1 THEN s.max_days_to_lease_start
										ELSE s.end_date - s.start_date
										END, CAST (pp.value AS INTEGER), CAST (pp1.value AS INTEGER), 120) AS days
						FROM
							properties AS p
							JOIN load_properties(ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ]) AS lp ON lp.cid = p.cid AND lp.property_id = p.id
							JOIN property_products AS ppr ON p.cid = ppr.cid AND p.id = ppr.property_id AND ppr.ps_product_id = ' . CPsProduct::PRICING . '
							LEFT JOIN public.special_groups AS sg ON p.cid = sg.cid AND p.id = sg.property_id AND sg.deleted_on IS NULL
							LEFT JOIN public.special_group_types AS sgt ON sg.special_group_type_id = sgt.id
							LEFT JOIN public.specials AS s ON sg.cid = s.cid AND s.special_group_id = sg.id AND s.deleted_on IS NULL AND s.special_type_id =
							' . CSpecialType::RENEWAL_TIER . ' AND s.special_recipient_id = ' . CSpecialRecipient::RENEWALS . '
							LEFT JOIN property_preferences AS pp ON p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'RENEWAL_OFFER_EXPIRATION_DAYS\'
							LEFT JOIN property_preferences AS pp1 ON p.cid = pp1.cid AND p.id = pp1.property_id AND pp1.key = \'PRICING_RENEWAL_CONSTRAINT_RENT_WINDOW\'
					)
					SELECT
						DISTINCT
						ror.*, ' . $strSelect . '
					FROM
						revenue_optimal_rents ror
						' . $strJoins . $strBestOptionCondition . '
					WHERE
						ror.cid = ' . ( int ) $intCid . $strWhereCond . '
						
					ORDER BY
						ror.property_id,
						ror.unit_type_id';
		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentsRenewableUnitsByUnitTypeIdsPropertyIdByCid( $arrintUnitTypeIds, $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM, $boolShowOnWebsite = true, $boolHideWithOffer = false, $strMinLeaseEndDate = NULL, $strMaxLeaseEndDate = NULL, $intBestPrice = 0 ) {

		if( NULL == $intSelectedLeaseTerm ) {
			$intSelectedLeaseTerm = CPricingLibrary::DEFAULT_PROPERTY_LEASE_TERM;
		}

		if( false == valArr( $arrintUnitTypeIds ) ) return NULL;

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		if( ( true == is_null( $strMinLeaseEndDate ) || '' == $strMinLeaseEndDate ) && ( true == is_null( $strMaxLeaseEndDate ) || '' == $strMaxLeaseEndDate ) ) {
			$strSqlCondition = 'CURRENT_DATE AND CURRENT_DATE + CAST ( COALESCE( rd.days, \'90\' ) || \' days\' AS INTERVAL ) ';
		} else {
			$strMinLeaseEndDate = date( 'm/d/Y', strtotime( $strMinLeaseEndDate ) );
			$strMaxLeaseEndDate   = date( 'm/d/Y', strtotime( $strMaxLeaseEndDate ) );
			$strSqlCondition = '\'' . $strMinLeaseEndDate . '\'::DATE AND ' . '\'' . $strMaxLeaseEndDate . ' \'::DATE';
		}
		if( false == $boolHideWithOffer ) {
			$arrintSkipLeaseStatusTypeIds[] = CLeaseStatusType::APPLICANT;
		} else {
			$arrintSkipLeaseStatusTypeIds[] = CLeaseStatusType::FUTURE;
		}
		$strJoins = ' JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . ' AND ut.deleted_on IS NULL )
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE . ' AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ') ' . $strCheckShowOnWebsiteSql . $strCheckDeletedUnitSpacesSql . ' )
						JOIN
						(
							SELECT
								l.cid,
								l.unit_space_id,
								l.id,
								l.active_lease_interval_id,
								li.lease_interval_type_id,
								li.lease_end_date,
								li.lease_start_date,
								li.lease_term_id,
								ROW_NUMBER() OVER( PARTITION BY l.cid, l.unit_space_id ORDER BY l.active_lease_interval_id DESC ) AS row_num
							FROM
								leases l
								JOIN lease_intervals AS li ON( li.cid = l.cid AND li.property_id = l.property_id AND li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ', ' . CLeaseStatusType::APPLICANT . ' ) )
								JOIN lease_customers AS lc ON( lc.cid = l.cid AND lc.property_id = l.property_id AND lc.lease_id = l.id AND lc.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ', ' . CLeaseStatusType::APPLICANT . ' ) )
							WHERE
								l.cid = ' . ( int ) $intCid . '
						) AS leases ON ( leases.cid = us.cid AND leases.unit_space_id = us.id AND leases.row_num = 1 )
						JOIN lease_processes AS lp ON( lp.cid = us.cid AND lp.lease_id = leases.id )
						LEFT JOIN lease_intervals rli ON ( rli.cid = leases.cid AND rli.lease_id = leases.id AND rli.lease_interval_type_id IN( ' . CLeaseIntervalType::RENEWAL . ' ) AND rli.lease_status_type_id IN (' . implode( ',', $arrintSkipLeaseStatusTypeIds ) . ') )
						LEFT JOIN lease_terms AS lt ON( lt.cid = us.cid AND lt.id = leases.lease_term_id ) AND lt.is_disabled = FALSE AND lt.deleted_on IS NULL 
						LEFT JOIN property_charge_settings pcs ON( pcs.cid = us.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND pcs.property_id = us.property_id )
						LEFT JOIN renewal_days rd ON( rd.cid = ror.cid AND rd.property_id = ror.property_id ) ';

		$strWhereCond = ' AND ror.property_id = ' . ( int ) $intPropertyId . '
						AND ror.unit_type_id IN ( ' . implode( ',', $arrintUnitTypeIds ) . ' )
					    AND COALESCE( us.occupancy_type_id, 0 ) != ' . ( int ) COccupancyType::AFFORDABLE . '
						AND rli.id IS NULL
						AND ( ( ( leases.lease_end_date IS NOT NULL AND leases.lease_end_date BETWEEN ' . $strSqlCondition . ' )
								OR ( leases.lease_end_date IS NULL AND lp.move_out_date IS NOT NULL AND lp.move_out_date BETWEEN ' . $strSqlCondition . ' )
								OR ( leases.lease_end_date IS NULL AND lp.move_out_date IS NULL AND leases.lease_start_date IS NOT NULL AND leases.lease_start_date + ( interval \'1 months\' * COALESCE( lt.term_month, 12 ) ) BETWEEN ' . $strSqlCondition . ' ) 
								)
							OR ( leases.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ) ) ';

		if( 1 == $intBestPrice ) {
			$strBestOptionCondition = ' JOIN (
										        SELECT *,
										               row_number() OVER(PARTITION BY cid, unit_space_id
										        ORDER BY rent_amt, custom_move_in_date) as rownum1
										        FROM (
										               SELECT *
										               FROM (
										                      SELECT *,
										                             row_number() OVER(PARTITION BY sub1.cid,
										                               sub1.unit_space_id, sub1.custom_move_in_date
										                      ORDER BY rent_amt, sub1.custom_move_in_date) as rownum
										                      FROM (
										                             SELECT sub.*,rentamount rent_amt
										                             FROM (
										                                    SELECT ror.cid,
										                                           ror.unit_space_id,
										                                           j2.moveindate,
										                                           CASE
										                                             WHEN CURRENT_DATE < to_date(
										                                               j2.moveindate, \'YYYYMMDD\') THEN
										                                               to_date(j2.moveindate, \'YYYYMMDD\'
										                                               )
										                                             ELSE CURRENT_DATE
										                                           END as custom_move_in_date,
										                                           j2.leasemonth,
										                                           j2.rentamount,
										                                           ror.id
										                                    FROM revenue_optimal_rents ror
										                                        ' . $strJoins . '
										                                         JOIN LATERAL
										                                         (
										                                           SELECT j1.moveindate,
										                                                  j2.leasemonth,
										                                                  (j2.value ->> 2)::numeric AS
										                                                    rentamount
										                                           FROM jsonb_each(ror.matrix) j1(moveindate, value)
										                                                LEFT JOIN LATERAL jsonb_each(j1.value) j2(leasemonth, value) ON TRUE
										                                         ) j2 ON TRUE
										                                    WHERE ror.cid = ' . ( int ) $intCid . $strWhereCond . '
										                                  ) as sub
										                           ) sub1
										                    ) as inner_query
										               WHERE rownum = 1
										 
										             ) as tmp
										      ) AS temp ON (ror.unit_space_id = temp . unit_space_id AND rownum1 = 1) ';

			$strSelect = '  temp . rent_amt AS custom_optimal_rent,
					        temp . leasemonth AS custom_lease_month,
					        temp.custom_move_in_date ';
		} else {

			$strSelect = ' (matrix::json->(select key from json_each_text(matrix::json) limit 1))::json#>>\'{' . $intSelectedLeaseTerm . ',2}\' AS custom_optimal_rent ';
			$strBestOptionCondition = '';
		}

		$strSql = 'WITH renewal_days AS (
						SELECT
							p.cid,
							p.id AS property_id,
							GREATEST(CASE
										WHEN sgt.id = 1 THEN s.max_days_to_lease_start
										ELSE s.end_date - s.start_date
										END, CAST (pp.value AS INTEGER), CAST (pp1.value AS INTEGER), 120) AS days
						FROM
							properties AS p
							JOIN load_properties(ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[ ' . ( int ) $intPropertyId . ' ]) AS lp ON lp.cid = p.cid AND lp.property_id = p.id
							JOIN property_products AS ppr ON p.cid = ppr.cid AND p.id = ppr.property_id AND ppr.ps_product_id = ' . CPsProduct::PRICING . '
							LEFT JOIN public.special_groups AS sg ON p.cid = sg.cid AND p.id = sg.property_id AND sg.deleted_on IS NULL
							LEFT JOIN public.special_group_types AS sgt ON sg.special_group_type_id = sgt.id
							LEFT JOIN public.specials AS s ON sg.cid = s.cid AND s.special_group_id = sg.id AND s.deleted_on IS NULL AND s.special_type_id =
							' . CSpecialType::RENEWAL_TIER . ' AND s.special_recipient_id = ' . CSpecialRecipient::RENEWALS . '
							LEFT JOIN property_preferences AS pp ON p.cid = pp.cid AND p.id = pp.property_id AND pp.key = \'RENEWAL_OFFER_EXPIRATION_DAYS\'
							LEFT JOIN property_preferences AS pp1 ON p.cid = pp1.cid AND p.id = pp1.property_id AND pp1.key = \'PRICING_RENEWAL_CONSTRAINT_RENT_WINDOW\'
					)
					SELECT
						DISTINCT
						ror.*, ' . $strSelect . '
					FROM
						revenue_optimal_rents ror
						' . $strJoins . $strBestOptionCondition . '
					WHERE
						ror.cid = ' . ( int ) $intCid . $strWhereCond . '
					ORDER BY
						ror.property_id,
						ror.unit_type_id';
		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentsNewlyAvailableUnitsByPropertyIds( $arrintPropertyIds, $intCid, $objDatabase, $boolShowOnWebsite = true, $arrintUnitTypeIds = [] ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND usl.is_marketed = 1 AND usl.show_on_website = 1' : ' AND usl.is_marketed = 1 ';
		$strSqlCondition = '';
		if( true == \valArr( $arrintUnitTypeIds ) ) {
			$strSqlCondition = ' AND ror.unit_type_id IN ( ' . implode( ',', $arrintUnitTypeIds ) . ' )';
		}
		$strSql = 'SELECT 
						*
					FROM (
						SELECT
							ror.unit_space_id,
							ror.unit_type_id,
							ror.property_id,
							ror.cid,
							usl.unit_number_cache,
							usl.building_name,
							COALESCE( ut.lookup_code, ut.name ) AS lookup_code,
							p.property_name,
							usl.unit_space_status_type_id,
							row_number ( ) OVER ( PARTITION BY ror.cid, ror.property_id, ror.unit_space_id ORDER BY usl_prior.id, rpr.process_datetime DESC NULLS LAST ) AS rank,
							rpr.process_datetime
						FROM
							revenue_optimal_rents ror
							JOIN unit_space_logs usl ON (usl.cid = ror.cid AND usl.property_id = ror.property_id AND usl.unit_space_id = ror.unit_space_id)
							JOIN unit_types ut ON( ut.id = usl.unit_type_id AND ut.cid = usl.cid )
	                        JOIN properties p ON( p.id = usl.property_id AND p.cid = usl.cid )
							JOIN property_charge_settings pcs ON( pcs.cid = usl.cid AND pcs.property_id = usl.property_id )
							JOIN unit_space_logs usl_prior ON (usl.cid = usl_prior.cid AND usl.property_id = usl_prior.property_id AND usl.unit_space_id = usl_prior.unit_space_id AND usl.id <> usl_prior.id)
							LEFT JOIN revenue_post_rents rpr ON ( usl.cid = rpr.cid AND usl.property_id = rpr.property_id AND usl.unit_space_id = rpr.unit_space_id AND rpr.is_renewal = 0
								AND rpr.is_override = 0
								AND rpr.updated_on::DATE BETWEEN usl.reporting_post_date AND CURRENT_DATE )
						WHERE
							ror.cid = ' . ( int ) $intCid . '
							AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							' . $strCheckShowOnWebsiteSql . '
							AND usl.is_available = TRUE
							AND usl.deleted_on IS NULL ' . $strSqlCondition . '
							AND ( usl.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR usl.occupancy_type_id IS NULL )
							AND (
									usl_prior.unit_space_status_type_id <> ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . '
									AND
									( usl_prior.unit_space_status_type_id <> ' . CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY . ' )
									AND
									( usl_prior.unit_space_status_type_id <> ' . CUnitSpaceStatusType::NOTICE_UNRENTED . ' )
								)
							AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . '
							AND CURRENT_DATE BETWEEN usl.reporting_post_date AND usl.apply_through_post_date
							AND usl.is_post_date_ignored = 0
							AND usl.reporting_post_date > usl_prior.reporting_post_date
							AND usl_prior.is_post_date_ignored = 0
							AND usl.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . \CUnitExclusionReasonType::CONSTRUCTION_UNIT . ') 
							AND ( ( rpr.process_datetime IS NULL AND rpr.id is NULL ) 
								OR ( rpr.updated_on::DATE > usl.reporting_post_date ) )
					) AS newly_available
					WHERE 
						newly_available.rank = 1
						AND newly_available.process_datetime IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleAvailableUnitTypesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsNewlyAvailable = false, $boolShowOnWebsite = true ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';

		$strCondition = ( true == $boolIsNewlyAvailable ) ? ' AND us.available_on = TO_CHAR( NOW(), \'MM/DD/YYYY\' )::TIMESTAMP::DATE' : NULL;

		$strSql = 'SELECT
						ror.property_id,
						ror.unit_type_id
					FROM
						revenue_optimal_rents ror
						JOIN unit_spaces us ON ( us.cid = ror.cid AND us.id = ror.unit_space_id AND us.deleted_on IS NULL )
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . ' AND ut.deleted_on IS NULL )
						JOIN property_charge_settings pcs ON ( pcs.cid = us.cid AND pcs.property_id = us.property_id )
					WHERE
						ror.cid 	= ' . ( int ) $intCid . '
						AND ror.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
						AND us.is_available = TRUE
						' . $strCheckShowOnWebsiteSql . '
						AND us.deleted_on IS NULL
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )
						' . $strCondition . '
					GROUP BY
						ror.property_id,
						ror.unit_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleRenewableUnitTypesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false, $boolShowOnWebsite = true, $intHideWithOffer = 0, $strMinLeaseEndDate = NULL, $strMaxLeaseEndDate = NULL, $intIsPendingOffersUnitOnly = 0 ) {

		if( false == valArr( $arrintPropertyIds ) ) return;

		$strCheckShowOnWebsiteSql = ( true == $boolShowOnWebsite ) ? ' AND us.is_marketed = TRUE AND us.show_on_website = TRUE' : ' AND us.is_marketed = TRUE ';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		if( ( ( true == is_null( $strMinLeaseEndDate ) || '' == $strMinLeaseEndDate ) && ( true == is_null( $strMaxLeaseEndDate ) || '' == $strMaxLeaseEndDate ) ) || ( ( 'undefined' == $strMinLeaseEndDate ) && ( 'undefined' == $strMaxLeaseEndDate ) ) ) {
			$strSqlCondition = 'CURRENT_DATE AND CURRENT_DATE + CAST ( COALESCE( rd.days, \'90\' ) || \' days\' AS INTERVAL ) ';
		} else {
			$strMinLeaseEndDate = date( 'm/d/Y', strtotime( $strMinLeaseEndDate ) );
			$strMaxLeaseEndDate   = date( 'm/d/Y', strtotime( $strMaxLeaseEndDate ) );
			$strSqlCondition = '\'' . $strMinLeaseEndDate . '\'::DATE AND ' . '\'' . $strMaxLeaseEndDate . ' \'::DATE';
		}
		if( 1 == $intHideWithOffer ) {
			$arrintSkipLeaseStatusTypeIds[] = CLeaseStatusType::APPLICANT;
		} else {
			$arrintSkipLeaseStatusTypeIds[] = CLeaseStatusType::FUTURE;
		}

		$strSqlPendingOffersUnitOnlyCondition = ' rli.id IS NULL ';

		if( 1 == $intIsPendingOffersUnitOnly ) {
			$strSqlPendingOffersUnitOnlyCondition = ' rli.id IS NOT NULL ';
		}

		$strSql = 'WITH renewal_days AS (
						SELECT
							DISTINCT
							s.cid,
							sg.property_id,
							CAST ( GREATEST ( CASE
												WHEN sgt.id = ' . CSpecialGroupType::CONVENTIONAL_LEASING . ' THEN s.max_days_to_lease_start
												ELSE s.end_date - s.start_date
												END, CAST ( pp.value AS INTEGER ), 90 ) AS VARCHAR ( 100 ) ) AS days
						FROM
							public.specials AS s
							JOIN public.special_groups AS sg ON s.cid = sg.cid AND s.special_group_id = sg.id
							JOIN public.special_group_types AS sgt ON sg.special_group_type_id = sgt.id
							LEFT JOIN property_products AS ppr ON sg.cid = ppr.cid AND sg.property_id = ppr.property_id AND ppr.ps_product_id = ' . CPsProduct::PRICING . '
							LEFT JOIN property_preferences AS pp ON sg.cid = pp.cid AND sg.property_id = pp.property_id AND pp.key = \'RENEWAL_OFFER_EXPIRATION_DAYS\'
						WHERE
							s.cid = ' . ( int ) $intCid . '
							AND s.deleted_on IS NULL
							AND sg.deleted_on IS NULL
							AND sg.deleted_by IS NULL
							AND s.special_type_id = ' . CSpecialType::RENEWAL_TIER . '
							AND s.special_recipient_id = ' . CSpecialRecipient::RENEWALS . '
					)
					SELECT
						ror.property_id,
						ror.unit_type_id
					FROM
						revenue_optimal_rents ror
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . ' AND ut.deleted_on IS NULL )
						JOIN unit_spaces us ON ( us.id = ror.unit_space_id AND us.cid = ror.cid AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::OCCUPIED_NO_NOTICE . ' AND us.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )' . $strCheckShowOnWebsiteSql . $strCheckDeletedUnitSpacesSql . ' )
						JOIN
						(
							SELECT
								l.cid,
								l.unit_space_id,
								l.id,
								l.active_lease_interval_id,
								li.lease_interval_type_id,
								li.lease_end_date,
								li.lease_start_date,
								li.lease_term_id,
								ROW_NUMBER() OVER( PARTITION BY l.cid, l.unit_space_id ORDER BY l.active_lease_interval_id DESC ) AS row_num
							FROM
								leases l
								JOIN lease_intervals AS li ON( li.cid = l.cid AND li.property_id = l.property_id AND li.lease_id = l.id AND li.id = l.active_lease_interval_id AND li.lease_status_type_id NOT IN( ' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ', ' . CLeaseStatusType::APPLICANT . ' ) )
							WHERE
								l.cid = ' . ( int ) $intCid . '
						) AS leases ON ( leases.cid = us.cid AND leases.unit_space_id = us.id AND leases.row_num = 1 )
						JOIN lease_processes AS lp ON( lp.cid = us.cid AND lp.lease_id = leases.id )
						LEFT JOIN lease_intervals rli ON ( rli.cid = leases.cid AND rli.lease_id = leases.id AND rli.lease_interval_type_id IN( ' . CLeaseIntervalType::RENEWAL . ' ) AND rli.lease_status_type_id IN (' . implode( ',', $arrintSkipLeaseStatusTypeIds ) . ') )
						LEFT JOIN lease_terms AS lt ON( lt.cid = us.cid AND lt.id = leases.lease_term_id ) AND lt.is_disabled = FALSE AND lt.deleted_on IS NULL 
						LEFT JOIN property_charge_settings pcs ON( pcs.cid = us.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND pcs.property_id = us.property_id )
						LEFT JOIN renewal_days rd ON( rd.cid = ror.cid AND rd.property_id = ror.property_id )
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND COALESCE( us.occupancy_type_id, 0 ) != ' . ( int ) COccupancyType::AFFORDABLE . '
						AND ' . $strSqlPendingOffersUnitOnlyCondition . '
						AND ( ( ( leases.lease_end_date IS NOT NULL AND leases.lease_end_date BETWEEN ' . $strSqlCondition . ' )
								OR ( leases.lease_end_date IS NULL AND lp.move_out_date IS NOT NULL AND lp.move_out_date BETWEEN ' . $strSqlCondition . ' )
								OR ( leases.lease_end_date IS NULL AND lp.move_out_date IS NULL AND leases.lease_start_date IS NOT NULL AND leases.lease_start_date + ( interval \'1 months\' * COALESCE( lt.term_month, 12 ) ) BETWEEN ' . $strSqlCondition . ' ) 
								)
							OR ( leases.lease_interval_type_id = ' . CLeaseIntervalType::MONTH_TO_MONTH . ' ) )
					GROUP BY
						ror.property_id,
						ror.unit_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * Fetch the list of company Id, property id  Missing creation optimal rents
	 */
	public static function fetchCustomMissingRevenueOptimalRents( $objDatabase, $strContractedPropertyIds = NULL ) {

		$strSql = ' 
			WITH ror AS ( 
	                    SELECT ror.cid,
	                           ror.property_id,
	                           MAX(ror.updated_on) AS max_date
	                    FROM revenue_optimal_rents ror
	                    WHERE 
	                        ror.updated_by = ' . CPricingLibrary::PRICING_USER_ETL . ' or ror.updated_by =  ' . CPricingLibrary::PRICING_USER_SYSTEM . ' 
	                    GROUP BY ror.cid,
	                             ror.property_id ) 
	                    SELECT pp.cid,
	                           pp.property_id,
	                           c.company_name,
	                           p.property_name
	                    FROM property_products pp
	                         JOIN property_preferences ppr ON ( ppr.cid = pp.cid AND
	                           ppr.property_id = pp.property_id AND ppr.key =
	                           \'PRICING_LEASES\' AND ppr.value IN (  \'New\', \'Renewal\', \'Both\' ) )
	                         JOIN clients c on ( c.id = pp.cid AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
	                         JOIN properties p on ( p.cid = pp.cid AND p.id = pp.property_id )
	                         LEFT JOIN ror ON ( ror.cid = pp.cid AND ror.property_id = pp.property_id AND ror.max_date::DATE >=CURRENT_DATE )
	                    WHERE 
	                        pp.ps_product_id = ' . CPsProduct::PRICING . ' 
	                        AND ror.cid IS NULL 
	                        AND p.is_disabled = 0
	                        AND p.is_test = 0 
	                        AND ( pp.ps_product_option_id = ' . CPsProductOption:: DATA_WAREHOUSE . 'OR pp.ps_product_option_id IS NULL ) 
	                        AND CASE
	                            WHEN ps_product_option_id IS NOT NULL THEN
	                              ps_product_option_id = ' . CPsProductOption:: DATA_WAREHOUSE . '
	                            ELSE TRUE
	                          END
	                        AND p.id IN ( ' . $strContractedPropertyIds . ' )
	                    ORDER BY pp.cid,
	                             pp.property_id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalRentsForAvailableUnitsByUnitSpaceIdsByUnitTypeIdsByPropertyIdsByCid( $arrintUnitSpaceIds, $arrintUnitTypeIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintUnitTypeIds ) || false == valArr( $arrintUnitSpaceIds ) ) {
			return NULL;
		}
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ror.*
					FROM
						revenue_optimal_rents ror
						JOIN unit_types ut ON ( ut.cid = ror.cid AND ut.id = ror.unit_type_id AND ut.is_occupiable = ' . CUnitType::IS_OCCUPIABLE_YES . ' )
						JOIN unit_spaces us ON ( us.cid = ror.cid AND us.id = ror.unit_space_id AND us.is_available = TRUE ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id	IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND ror.unit_type_id IN ( ' . implode( ', ', $arrintUnitTypeIds ) . ' )
						AND ror.unit_space_id IN ( ' . implode( ', ', $arrintUnitSpaceIds ) . ' ) ';

		return self::fetchRevenueOptimalRents( $strSql, $objDatabase );
	}

	public static function fetchRevenueOptimalAdvertiseOccupiedRentByUnitTypeIdByPropertyIdByCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						ror.property_id,
						ror.unit_type_id,
						avg(ror.optimal_occupancy_rent) AS optimal_occupancy_rent_amount,
						avg(ror.optimal_advertised_rent) AS optimal_advertised_rent_amount
					FROM revenue_optimal_rents AS ror
						JOIN unit_spaces as us on us.cid = ror.cid AND us.property_id = ror.property_id AND us.unit_type_id = ror.unit_type_id AND us.id = ror.unit_space_id
					where
						ror.cid = ' . ( int ) $intCid . ' 
						AND ror.property_id = ' . ( int ) $intPropertyId . ' 
						AND ror.unit_type_id = ' . ( int ) $intUnitTypeId . ' 
						AND ror.unit_space_id IS NOT NULL 
						AND us.is_available = TRUE
					GROUP BY ror.unit_type_id,
						ror.property_id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomHighRevenueOptimalRents( $objDatabase, $strContractedPropertyIds = NULL ) {

		$strSql = ' WITH optimal_diff AS (
						SELECT
							ror.cid,
							ror.property_id,
							ror.unit_space_id,
							ror.avg_advertised_rent,
							ror.std_optimal_rent,
							CASE
								WHEN ror.avg_advertised_rent > 0 AND ror.std_optimal_rent > 0 THEN ABS( ROUND( ( ror.avg_advertised_rent - ror.std_optimal_rent ) * 100.0 / ror.std_optimal_rent ) )
								--WHEN ror.avg_executed_rent > 0 AND ror.std_optimal_rent > 0 THEN ABS( ROUND( ( ror.avg_executed_rent - ror.std_optimal_rent ) * 100.0 / ror.std_optimal_rent ) )
								ELSE 0
							END AS optimal_difference
						FROM
							revenue_optimal_rents ror
						WHERE
							ror.unit_space_id IS NOT NULL
					)
					SELECT
						c.company_name,
						p.property_name,
						MAX( od.optimal_difference ) as max_difference, 
						COUNT( unit_space_id ), 
						array_agg( unit_number_cache ) as unit_spaces
					FROM
						optimal_diff od
						JOIN clients c ON ( c.id = od.cid AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
						JOIN properties p ON ( p.id = od.property_id AND p.is_disabled = 0 AND p.id IN ( ' . $strContractedPropertyIds . ' ) )
						JOIN unit_spaces us ON ( us.id = unit_space_id AND us.deleted_on IS NULL )
						JOIN property_preferences ppr ON ( ppr.cid = od.cid AND
	                           ppr.property_id = od.property_id AND ppr.key = \'PRICING_LEASES\' AND ppr.value IN (  \'New\', \'Renewal\', \'Both\' ) )
					WHERE
						optimal_difference > 10
					GROUP BY 
						c.company_name, p.property_name
					ORDER BY
						c.company_name, p.property_name ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomIdentifyUnitsMissedOptimalRents( $objDatabase, $strContractedPropertyIds = NULL ) {

        $strSql = ' WITH ror AS ( 
	                    SELECT ror.cid,
	                           ror.property_id,
                               ror.unit_space_id,
                               ror.std_optimal_rent,
                               ror.matrix
	                    FROM revenue_optimal_rents ror ) SELECT
                    us.id,
                    us.property_id,
                    us.unit_number_cache,
                    us.property_name,
                    c.company_name
                    FROM unit_spaces us
                    JOIN properties p on ( p.id = us.property_id AND p.is_disabled = 0 AND p.is_test = 0 AND p.id IN ( ' . $strContractedPropertyIds . ' ) )
                    JOIN clients c on ( c.id = us.cid AND p.cid = c.id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
                    JOIN property_products pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.ps_product_id = ' . CPsProduct::PRICING . ' )
                    JOIN property_preferences ppr ON ( ppr.cid = pp.cid AND
                    ppr.property_id = pp.property_id AND ppr.key = \'PRICING_LEASES\' AND ppr.value IN (  \'New\', \'Renewal\', \'Both\' ) )
                    JOIN property_charge_settings pcs ON ( pcs.cid = us.cid AND pcs.property_id = us.property_id )
                    LEFT JOIN ror ON ( ror.unit_space_id = us.id AND ror.cid = pp.cid AND ror.property_id = pp.property_id )
                    WHERE
                    ror.cid IS NULL 
                    AND ( pp.ps_product_option_id = ' . CPsProductOption:: DATA_WAREHOUSE . ' OR
                    pp.ps_product_option_id IS NULL ) 
                    AND CASE
                          WHEN ps_product_option_id IS NOT NULL THEN
                            ps_product_option_id = ' . CPsProductOption:: DATA_WAREHOUSE . '
                          ELSE TRUE
                        END
                    AND us.unit_space_status_type_id IN ( ' . CUnitSpaceStatusType:: VACANT_UNRENTED_READY . ' , ' . CUnitSpaceStatusType:: VACANT_UNRENTED_NOT_READY . ' , ' . CUnitSpaceStatusType:: NOTICE_UNRENTED . ' )
                    AND (
                            us.unit_space_status_type_id = ' . CUnitSpaceStatusType:: VACANT_UNRENTED_READY . '
                            OR
                            ( pcs.show_available_not_ready_spaces = TRUE AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType:: VACANT_UNRENTED_NOT_READY . ' )
                            OR
                            ( pcs.show_notice_available_spaces = TRUE AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType:: NOTICE_UNRENTED . ' )
                        )
                    AND us.is_marketed = TRUE
                    AND us.deleted_on IS NULL 
                    AND ( ror.std_optimal_rent IS NULL OR ror.std_optimal_rent = 0 OR ror.matrix IS NULL ) ';

        return fetchData( $strSql, $objDatabase );

    }

    public static function fetchCustomIdentifyUnitOptimalsOutsideMinMaxRents( $objDatabase, $strContractedPropertyIds = NULL ) {

        $strSql = ' WITH ror AS ( 
	                     SELECT ror.cid,
	                           ror.property_id,
                               ror.unit_space_id,
                               ror.std_optimal_rent,
                               ror.unit_type_id
	                    FROM revenue_optimal_rents ror ) SELECT
                        us.id,
                        us.property_id,
                        us.unit_number_cache,
                        us.property_name,
                        c.company_name,
                        ror.std_optimal_rent,
                        rutpmin.value AS "min_amount"
                        FROM unit_spaces us
                        JOIN properties p on ( p.id = us.property_id AND p.is_disabled = 0 AND p.is_test = 0 AND p.id IN ( ' . $strContractedPropertyIds . ' ) )
                        JOIN clients c on ( c.id = us.cid AND p.cid = c.id AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
                        JOIN property_products pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.ps_product_id = ' . CPsProduct::PRICING . ' )
                        JOIN property_preferences ppr ON ( ppr.cid = pp.cid AND
                        ppr.property_id = pp.property_id AND ppr.key = \'PRICING_LEASES\' AND ppr.value IN (  \'New\', \'Renewal\', \'Both\' ) )
                        JOIN property_charge_settings pcs ON ( pcs.cid = us.cid AND pcs.property_id = us.property_id )
                        LEFT JOIN ror ON ( ror.unit_space_id = us.id AND ror.cid = pp.cid AND ror.property_id = pp.property_id )
                        JOIN revenue_unit_type_preferences rutpmin ON ( rutpmin.unit_type_id = ror.unit_type_id AND rutpmin.key = \'PRICING_CONSTRAINT_MIN_OPTIMAL_RENT\' AND CAST( rutpmin.value AS NUMERIC ) > 0 )
                        WHERE
                        ( pp.ps_product_option_id = ' . CPsProductOption:: DATA_WAREHOUSE . ' OR
                        pp.ps_product_option_id IS NULL ) 
                        AND CASE
                              WHEN ps_product_option_id IS NOT NULL THEN
                                ps_product_option_id = ' . CPsProductOption:: DATA_WAREHOUSE . '
                              ELSE TRUE
                            END
                        AND us.unit_space_status_type_id IN ( ' . CUnitSpaceStatusType:: VACANT_UNRENTED_READY . ' , ' . CUnitSpaceStatusType:: VACANT_UNRENTED_NOT_READY . ' , ' . CUnitSpaceStatusType:: NOTICE_UNRENTED . ' )
                        AND (
                                us.unit_space_status_type_id = ' . CUnitSpaceStatusType:: VACANT_UNRENTED_READY . '
                                OR
                                ( pcs.show_available_not_ready_spaces = TRUE AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType:: VACANT_UNRENTED_NOT_READY . ' )
                                OR
                                ( pcs.show_notice_available_spaces = TRUE AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType:: NOTICE_UNRENTED . ' )
                            )
                        AND us.is_marketed = TRUE
                        AND us.deleted_on IS NULL
                        AND ( ror.std_optimal_rent < CAST( rutpmin.value AS NUMERIC ) )';

        return fetchData( $strSql, $objDatabase );

    }

	public static function fetchCustomNewlyAvailablePricingUnits( $objDatabase ) {

		$strSql = ' SELECT
						us.cid,
						us.property_id,
						array_agg(us.unit_type_id) AS unit_type_ids,
						array_agg(us.id) AS unit_space_ids
					FROM unit_spaces as us
						JOIN property_products AS ppd ON (ppd.cid = us.cid AND (ppd.property_id = us.property_id OR ppd.property_id IS NULL) AND ppd.ps_product_id = ' . CPsProduct::PRICING . ' )
						JOIN property_preferences as pp ON ( us.cid = pp.cid AND us.property_id = pp.property_id AND pp.key = \'' . CPricingPostRentLibrary::PROPERTY_PREFERENCE_PRICING_LEASES . '\' AND pp.value IN ( \'' . CPricingPostRentLibrary::PRICING_LEASES_BOTH . '\', \'' . CPricingPostRentLibrary::PRICING_LEASES_NEW . '\' ) )
						JOIN property_charge_settings pcs ON (pcs.cid = us.cid AND pcs.property_id = us.property_id)
						JOIN unit_types ut ON( ut.id = us.unit_type_id AND ut.cid = us.cid )
						JOIN unit_space_logs usl ON (usl.cid = us.cid AND us.id = usl.unit_space_id)
						JOIN properties p ON (p.id = usl.property_id AND p.cid = usl.cid)
						JOIN unit_space_logs usl_prior ON (usl.cid = usl_prior.cid AND usl.unit_space_id = usl_prior.unit_space_id AND usl.id <> usl_prior.id)
						LEFT JOIN revenue_optimal_rents as ror ON (us.cid = ror.cid AND us.id = ror.unit_space_id AND usl.unit_space_id = ror.unit_space_id)
					WHERE
						us.is_marketed = TRUE 
						AND us.is_available = TRUE 
						AND us.deleted_on IS NULL 
						AND ( ror.id IS NULL OR ror.std_optimal_rent IS NULL OR ror.std_optimal_rent = 0 OR ror.matrix IS NULL OR usl.updated_on > ror.updated_on ) 
						AND ( us.occupancy_type_id != ' . ( int ) CPropertyType::SUBSIDIZED . ' OR us.occupancy_type_id IS NULL )
						AND ( us.unit_space_status_type_id = ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . ' OR ( pcs.show_available_not_ready_spaces = TRUE AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY . ' )
						OR ( pcs.show_notice_available_spaces = TRUE AND us.unit_space_status_type_id = ' . CUnitSpaceStatusType::NOTICE_UNRENTED . ' ) )
						AND usl_prior.unit_space_status_type_id <> ' . CUnitSpaceStatusType::VACANT_UNRENTED_READY . '
						AND ( usl_prior.unit_space_status_type_id <> ' . CUnitSpaceStatusType::VACANT_UNRENTED_NOT_READY . ' )
						AND ( usl_prior.unit_space_status_type_id <> ' . CUnitSpaceStatusType::NOTICE_UNRENTED . ' )
						AND CURRENT_DATE BETWEEN usl.reporting_post_date AND usl.apply_through_post_date 
						AND CURRENT_DATE - INTERVAL \'1 day\' BETWEEN usl_prior.reporting_post_date AND usl_prior.apply_through_post_date 
						AND usl.unit_exclusion_reason_type_id IN ( ' . \CUnitExclusionReasonType::NOT_EXCLUDED . ', ' . CUnitExclusionReasonType::CONSTRUCTION_UNIT . ' )
					GROUP BY
						us.cid,
						us.property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchWaterfallReportDataByUnitTypeIdPropertyIdCid( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase, $arrintUnitSpaceIds = [] ) {
		$strSpaceCondition = '';
		if( true == valArr( $arrintUnitSpaceIds ) ) {
			$strSpaceCondition = ' AND ror.unit_space_id IN( ' . implode( ',', $arrintUnitSpaceIds ) . ' )';
		}

		$strSql = 'SELECT
						ut.unit_number,
						ror.unit_space_id,
						round ( ror.avg_advertised_rent ) AS avg_advertised_rent,
						round ( ror.avg_executed_rent ) AS avg_executed_rent,
						round ( ror.avg_competitive_rent ) AS avg_competitive_rent,
						round ( ror.base_rent ) AS anchor_rent,
						round ( ror.optimal_base_rent ) AS optimal_base_rent,
						round ( ror.notice_period_adjustment ) AS notice_period_adjustment,
						round ( ror.staleness_period_adjustment ) AS staleness_period_adjustment,
						round ( ror.sustainable_occupancy_adjustment ) AS sustainable_occupancy_adjustment,
						round ( ror.optimal_occupancy_rent ) AS optimal_occupancy_rent,
						round ( ror.optimal_occupancy_rent_constrained ) AS optimal_occupancy_rent_constrained,
						round ( ror.std_vacancy_loss_adjustment ) AS std_vacancy_loss_adjustment,
						round ( ror.std_expiration_adjustment ) AS std_expiration_adjustment,
						round ( ror.std_turn_cost_adjustment ) AS std_turn_cost_adjustment,
						round ( ror.std_expiration_adjustment ) AS std_expiration_adjustment,
						round ( ror.optimal_advertised_rent_unconstrained ) AS optimal_advertised_rent_unconstrained,
						round ( ror.std_optimal_rent ) AS std_optimal_rent,
						round ( ror.minimum_leased_adjustment ) AS minimum_leased_adjustment,
						ror.matrix::json
					FROM
						revenue_optimal_rents ror
						JOIN unit_spaces ut ON ut.id = ror.unit_space_id AND ut.cid = ror.cid AND ut.property_id = ror.property_id AND ut.unit_type_id = ror.unit_type_id
					WHERE
						ror.cid = ' . ( int ) $intCid . '
						AND ror.property_id = ' . ( int ) $intPropertyId . '
						AND ror.unit_type_id = ' . ( int ) $intUnitTypeId . ' ' . $strSpaceCondition . '
					ORDER BY ut.unit_number';

		return fetchData( $strSql, $objDatabase );
	}

}
?>