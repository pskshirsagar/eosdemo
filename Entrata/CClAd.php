<?php
class CClAd extends CBaseClAd {

	protected $m_intTotalAds;
	protected $m_arrobjClAdSlides;
	protected $m_intTimePassedSinceLastPost;

	/**
	 * Get Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['total_ads'] ) ) $this->setTotalAds( $arrmixValues['total_ads'] );
		if( true == isset( $arrmixValues['time_passed_since_last_post'] ) ) $this->setTimePassedSinceLastPost( $arrmixValues['time_passed_since_last_post'] );
	}

	public function getTimePassedSinceLastPost() {
		return $this->m_intTimePassedSinceLastPost;
	}

	/**
	 * Set Functions
	 */

	public function setTotalAds( $intTotalAds ) {
		$this->m_intTotalAds = $intTotalAds;
	}

	public function setTimePassedSinceLastPost( $intTimePassedSinceLastPost ) {
		$this->m_intTimePassedSinceLastPost = $intTimePassedSinceLastPost;
	}

	/**
	 * Add Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClHeadlineLinkId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCraigslistUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalClicks() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFavorite() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOnCraigslist() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRecommended() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPosted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEditingContinuedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDoneOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinalizedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFinalizedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateFromDateTo( $objClAdsFilter ) {
		$boolIsValid = true;

		$strStartDate 	= NULL;
		$strEndDate 	= NULL;

		if( false == is_null( $objClAdsFilter->getClAdsDateFrom() ) ) {
			$strStartDate 	= trim( strtotime( $objClAdsFilter->getClAdsDateFrom() ) );
		}

		if( false == is_null( $objClAdsFilter->getClAdsDateTo() ) ) {
			$strEndDate 	= trim( strtotime( $objClAdsFilter->getClAdsDateTo() ) );
		}

		if( $strStartDate > $strEndDate && true == isset( $strEndDate ) && '' != $strEndDate ) {

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Start Date is Greater than End Date.', NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;

	}

	public function validate( $strAction, $objClAdsFilter = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate':
				$boolIsValid &= $this->valDateFromDateTo( $objClAdsFilter );
				break;

			default:
				// do nothing;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createShortUrl( $strFullUrl, $objDatabase ) {

		$objShortUrl = new CShortUrl();

		$objShortUrl->setDefaults();
		$objShortUrl->setCid( $this->getCid() );
		$objShortUrl->setFullUrl( $strFullUrl );
		$objShortUrl->genrateUniqueSlug( $objDatabase );

		$arrstrFullUrl = parse_url( $strFullUrl );

		$strShortUrl = $arrstrFullUrl['host'] . '/su/' . $objShortUrl->getSlug();

		$objShortUrl->setShortUrl( $strShortUrl );

		return $objShortUrl;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setIpAddress( getRemoteIpAddress() );
		$boolIsValid = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		return $boolIsValid;
	}

}
?>
