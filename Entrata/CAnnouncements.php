<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAnnouncements
 * Do not add any new functions to this class.
 */

class CAnnouncements extends CBaseAnnouncements {

	public static function fetchAnnouncementsByCidByAnnouncementTypeId( $intCid, $intAnnouncementTypeId, $objDatabase ) {

		$strSql = '
					SELECT
						a.*,
						app.ps_product_id
					FROM
						announcements a
						LEFT JOIN announcement_ps_products app ON ( app.cid = a.cid AND app.announcement_id = a.id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.announcement_type_id = ' . ( int ) $intAnnouncementTypeId . '
						ORDER BY a.published_on DESC NULLS LAST';

		return self::fetchAnnouncements( $strSql, $objDatabase );
	}

	public static function fetchAnnouncementByCidById( $intCid, $intAnnouncementId, $objDatabase ) {
		return self::fetchAnnouncement( sprintf( 'SELECT a.* FROM %s AS a , %s AS pa WHERE pa.announcement_id::integer = a.id::integer AND pa.cid = a.cid AND a.cid = %d AND a.id = %d ORDER BY a.order_num', 'announcements', 'property_announcements', ( int ) $intCid, ( int ) $intAnnouncementId ), $objDatabase );
	}

	public static function fetchAnnouncementByAnnouncementIdByClientByPropertyId( $intAnnouncementId, $intCid, $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						a.*,
						app.ps_product_id
					FROM
						announcements AS a
						JOIN property_announcements AS pa ON ( pa.announcement_id::integer = a.id::integer AND pa.cid::integer = a.cid::integer )
						LEFT JOIN announcement_ps_products app ON ( app.cid = a.cid AND app.announcement_id = a.id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND pa.property_id = ' . ( int ) $intPropertyId . '
						AND a.id = ' . ( int ) $intAnnouncementId;

		return self::fetchAnnouncement( $strSql, $objDatabase );
	}

	// $intPsProductId is not getting used in SQL will be removing in Task Id: 344125

	public static function fetchPublishedAnnouncementsByPropertyIdsByCidByAnnouncementTypeIdsByPsProduct( $intCid, $arrintPropertyIds, $arrintAnnouncementTypeIds, $intPsProductId = NULL, $objDatabase ) {

		if( false == valArr( array_filter( $arrintPropertyIds ) ) && valArr( $arrintAnnouncementTypeIds ) ) return NULL;

		$strSql = 'SELECT
						a.id,
						a.announcement_type_id,
						a.frequency_id,
						a.title,
						a.announcement,
						a.date_start,
						a.date_end
					FROM
		 				announcements a
						JOIN property_announcements pa ON ( pa.announcement_id::integer = a.id::integer AND pa.cid = a.cid )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND pa.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND a.announcement_type_id IN (' . implode( ',', $arrintAnnouncementTypeIds ) . ')
						AND
							CASE
								WHEN ( a.frequency_id IS NULL OR a.frequency_id = ' . CFrequency::DAILY . ' )
									THEN a.date_start <= NOW()::date AND ( a.date_end >= NOW()::date OR a.date_end IS NULL )
								WHEN( a.frequency_id = ' . CFrequency::ONCE . ' )
									THEN a.date_start IS NOT NULL AND NOW()::date = a.date_start
								WHEN( a.frequency_id = ' . CFrequency::WEEKLY . ' )
									THEN date_part( \'dow\', NOW() ) = date_part( \'dow\', a.date_start ) AND ( ( a.date_end IS NOT NULL AND a.date_end >= NOW()::date ) OR a.date_end IS NULL )
								WHEN(a.frequency_id = ' . CFrequency::MONTHLY . ')
									THEN date_part( \'day\', NOW() ) = date_part( \'day\', a.date_start ) AND ( ( a.date_end IS NOT NULL AND a.date_end >= NOW()::date ) OR a.date_end IS NULL )
								ELSE TRUE
							END
						AND a.is_published=1
					ORDER BY
						a.order_num DESC';

		return self::fetchAnnouncements( $strSql, $objDatabase );
	}

	public static function fetchAnnouncementsByCidByPropertyIdByAnnouncementTypeIds( $intCid, $intPropertyId, $arrintAnnouncementTypeIds, $objDatabase, $objPagination = NULL,  $strAnnouncementSearchFilter = NULL ) {

		$strSql = 'SELECT
						a.*,
						app.ps_product_id
					FROM
						announcements AS a
						JOIN property_announcements AS pa ON ( pa.announcement_id::integer = a.id::integer AND pa.cid::integer = a.cid::integer )
						LEFT JOIN announcement_ps_products app ON ( app.cid = a.cid AND app.announcement_id = a.id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND pa.property_id = ' . ( int ) $intPropertyId . '
						AND a.announcement_type_id IN ( ' . implode( ',', $arrintAnnouncementTypeIds ) . ' ) ';

		$strAndWhere = '';
		if( true == valArr( $strAnnouncementSearchFilter ) ) {

			if( false == is_null( $strAnnouncementSearchFilter['announcement_content'] ) ) {
				$strAnnouncementContent = $strAnnouncementSearchFilter['announcement_content'];
				$strAndWhere .= 'AND a.announcement ILIKE \'%' . trim( addslashes( $strAnnouncementContent ) ) . '%\'';
			}

			if( 'Published' == $strAnnouncementSearchFilter['status'] ) {
				$strAndWhere .= ' AND ( a.is_published = 1 )';
			}

			if( 'Unpublished' == $strAnnouncementSearchFilter['status'] ) {
				$strAndWhere .= ' AND ( a.is_published = 0 )';
			}
		}

		if( true == valStr( $strAndWhere ) ) {
			$strSql .= $strAndWhere;
		}

		$strOffsetLimit = ( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) ? '' : 'OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		$strSql .= ' ORDER BY a.is_pinned DESC, a.published_on DESC NULLS LAST, a.id DESC, a.created_on DESC, a.order_num ' . $strOffsetLimit;

		return self::fetchAnnouncements( $strSql, $objDatabase );
	}

	public static function fetchPublishedFutureActiveSortedCompanyAnnouncementsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == isset( $intCid ) || false == is_numeric( $intCid ) ) return NULL;
		if( false == isset( $intPropertyId ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						a.*
					FROM
						property_announcements pa
						LEFT JOIN announcements a ON ( pa.announcement_id::integer = a.id::integer AND pa.cid::integer = a.cid::integer AND pa.property_id = ' . ( int ) $intPropertyId . ' AND a.cid = ' . ( int ) $intCid . ' )
					WHERE
						pa.cid = ' . ( int ) $intCid . '
						AND a.is_published = 1
						AND CASE
							WHEN a.date_end IS NULL
								THEN a.date_start IS NOT NULL AND a.date_start >= \'' . date( 'm/d/Y' ) . '\'
							WHEN a.date_end IS NOT NULL
								THEN a.date_start IS NOT NULL AND a.date_end >= \'' . date( 'm/d/Y' ) . '\'
							END
					ORDER BY
						a.date_start ';

		return self::fetchAnnouncements( $strSql, $objDatabase );
	}

	public static function fetchAnnouncementsCountByPropertyIdsByCid( $intPropertyId, $intCid, $strAnnouncementSearchFilter, $objDatabase, $arrintAnnouncementTypeIds ) {
		$strAndWhere = '';
		if( true == valArr( $strAnnouncementSearchFilter ) ) {

			if( false == is_null( $strAnnouncementSearchFilter['announcement_content'] ) ) {
				$strAnnouncementContent = $strAnnouncementSearchFilter['announcement_content'];
				$strAndWhere = 'AND a.announcement ILIKE \'%' . trim( addslashes( $strAnnouncementContent ) ) . '%\'';
			}

			if( 'Future' == $strAnnouncementSearchFilter['status'] ) {
				$strAndWhere .= ' AND ( a.date_start >= now()::date OR a.date_end >= now()::date )';
			}

			if( 'Current' == $strAnnouncementSearchFilter['status'] ) {
				$strAndWhere .= ' AND ( a.date_start = now()::date OR a.date_end = now()::date )';
			}

			if( 'Past' == $strAnnouncementSearchFilter['status'] ) {
				$strAndWhere .= ' AND a.date_end < now()::date';
			}
		}

		$strSql = 'SELECT
						count(a.id) as announcements_count
					FROM
						announcements AS a
						JOIN property_announcements AS pa ON ( pa.announcement_id::integer = a.id::integer AND pa.cid::integer = a.cid::integer )
						LEFT JOIN announcement_ps_products app ON ( app.cid = a.cid AND app.announcement_id = a.id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND pa.property_id = ' . ( int ) $intPropertyId . '
						AND a.announcement_type_id IN ( ' . implode( ',', $arrintAnnouncementTypeIds ) . ' ) ';

		if( true == valStr( $strAndWhere ) ) {
			$strSql .= $strAndWhere;
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['announcements_count'] ) ) return $arrintResponse[0]['announcements_count'];

		return 0;
	}

}
?>