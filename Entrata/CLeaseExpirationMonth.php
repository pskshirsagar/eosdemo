<?php

class CLeaseExpirationMonth extends CBaseLeaseExpirationMonth {

	const JANUARY		= 1;
	const FEBRUARY		= 2;
	const MARCH			= 3;
	const APRIL			= 4;
	const MAY			= 5;
	const JUNE			= 6;
	const JULY			= 7;
	const AUGUST		= 8;
	const SEPTEMBER		= 9;
	const OCTOBER		= 10;
	const NOVEMBER		= 11;
	const DECEMBER		= 12;

	protected $m_intPropertyUnitsCount;

	public function setPropertyUnitsCount( $intPropertyUnitsCount ) {
		$this->m_intPropertyUnitsCount = $intPropertyUnitsCount;
	}

	public function getPropertyUnitsCount() {
		return $this->m_intPropertyUnitsCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['pu_count'] ) ) {
			$this->setPropertyUnitsCount( $arrmixValues['pu_count'] );
		}
	}

	public function valExpirationTolerance() {
		$boolIsValid = true;

		if( !is_null( $this->getExpirationTolerance() ) && ( 100 < $this->getExpirationTolerance() || 0 > $this->getExpirationTolerance() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ExpirationTolerance', __( 'Expiration tolerance Percentage must be greater than zero or less than or equal to {%d, 0}.', [ 100 ] ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valExpirationTolerance();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

	return $boolIsValid;
	}

	public static function leaseExpirationMonthIdToStrArray() {
		return [
			self::JANUARY	=> __( 'January' ),
			self::FEBRUARY	=> __( 'February' ),
			self::MARCH		=> __( 'March' ),
			self::APRIL		=> __( 'April' ),
			self::MAY 		=> __( 'May' ),
			self::JUNE		=> __( 'June' ),
			self::JULY 		=> __( 'July' ),
			self::AUGUST	=> __( 'August' ),
			self::SEPTEMBER => __( 'September' ),
			self::OCTOBER 	=> __( 'October' ),
			self::NOVEMBER 	=> __( 'November' ),
			self::DECEMBER 	=> __( 'December' ),
		];
	}

}
?>