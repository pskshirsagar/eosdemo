<?php

class CReportArchive extends CBaseReportArchive {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSerializedData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>