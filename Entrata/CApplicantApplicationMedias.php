<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantApplicationMedias
 * Do not add any new functions to this class.
 */

class CApplicantApplicationMedias extends CBaseApplicantApplicationMedias {

	public static function fetchApplicantApplicationMediaByApplicantApplicationIdByCompanyMediaFileIdByCid( $intApplicantApplicationId, $intCompanyMediaFileId, $intCid, $objDatabase ) {
		return self::fetchApplicantApplicationMedia( sprintf( 'SELECT * FROM %s WHERE applicant_application_id::int = %d::int AND cid::int = %d::int AND company_media_file_id::int = %d::int LIMIT 1', 'applicant_application_medias', ( int ) $intApplicantApplicationId, ( int ) $intCid, ( int ) $intCompanyMediaFileId ), $objDatabase );
	}

	public static function fetchApplicantApplicationMediasByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						applicant_application_medias
					WHERE
						applicant_application_id IN ( ' . implode( ',', $arrintApplicantApplicationIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return parent::fetchApplicantApplicationMedias( $strSql, $objDatabase );
	}

	public static function fetchApplicantApplicationMediasByApplicantApplicationIdByCid( $intApplicantApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						applicant_application_medias
					WHERE
						applicant_application_id = ' . ( int ) $intApplicantApplicationId . '
						AND cid = ' . ( int ) $intCid;

		return parent::fetchApplicantApplicationMedias( $strSql, $objDatabase );
	}

	public static function fetchApplicantApplicationMediasCountByApplicationIdByCidByFileTypeName( $intApplicationId, $strFileTypeName, $intCid, $objDatabase ) {
		if( false == valId( $intApplicationId ) ) return NULL;
		if( false == valStr( $strFileTypeName ) ) return NULL;

		$strWhere = '{"file_type_name":"' . $strFileTypeName . '"}';
		$strSql = 'SELECT
						count(*)
					FROM
						applicant_application_medias aam
						JOIN applicant_applications aa on (aa.id = aam.applicant_application_id and  aa.cid = aam.cid)
					WHERE
						data @> \'' . $strWhere . '\'
						AND aa.application_id = ' . ( int ) $intApplicationId . '
						AND aa.cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

}
?>