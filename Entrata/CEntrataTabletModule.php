<?php

class CEntrataTabletModule extends CBaseEntrataTabletModule {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataTabletModuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataTabletModuleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIconUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
		    case VALIDATE_INSERT:
		    case VALIDATE_UPDATE:
		    case VALIDATE_DELETE:
				break;

		    default:
			    $boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>