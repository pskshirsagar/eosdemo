<?php

class CCompanyUserNotification extends CBaseCompanyUserNotification {

	public function __construct() {
		parent::__construct();
		$this->setAllowDifferentialUpdate( true );
		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserNotificationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpiresOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReadOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSnoozeUntil() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>