<?php

class CReviewEventType extends CBaseReviewEventType {

	const PROSPECT_PORTAL_PUBLISHED                       = 1;
	const VACANCY_PUBLISHED                               = 2;
	const VIEWED                                          = 3;
	const COMPLETED                                       = 4;
	const NOTIFICATION_SENT                               = 5;
	const ONLINE_RESPONSE                                 = 6;
	const EMAIL_RESPONSE                                  = 7;
	const PROSPECT_PORTAL_UNPUBLISHED                     = 8;
	const INCOMPLETE                                      = 9;
	const REVIEW_UPDATED                                  = 11;
	const TASK_CREATED                                    = 12;
	const TASK_COMPLETED                                  = 13;
	const TASK_DELETED                                    = 14;
	const ATTRIBUTE_UPDATED                               = 15;
	const WROTE_RESPONSE                                  = 16;
	const PROPERTY_APPROVED_RESPONSE                      = 17;
	const ESCALATED_TICKET                                = 18;
	const RESPONSE_EDITED                                 = 19;
	const RESPONSE_REMOVED                                = 20;
	const RESIDENT_LINKED                                 = 21;
	const RESIDENT_UNLINKED                               = 22;
	const REVIEW_CREATED                                  = 23;
	const RESPONSE_UNPUBLISHED                            = 24;
	const IGNORED_REVIEW                                  = 25;
	const TASK_UPDATED                                    = 26;
	const ONLINE_RESPONSE_FAILED                          = 27;
	const SENT_TO_REVIEW_RESPONDER_FOR_RESPONSE           = 28;
	const SENT_TO_REVIEW_RESPONDER_FOR_MARKING_ATTRIBUTES = 29;
	const RESPONSE_LIKED                                  = 30;
	const REVIEW_DELETED                                  = 31;
	const RESPONSE_INSERTED                               = 32;
	const DRAFTED_RESPONSE_REMOVED                        = 33;
	const REVIEW_EDITED                                   = 34;
	const REMOVED_FROM_REVIEW_RESPONDER                   = 35;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>