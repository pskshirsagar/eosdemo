<?php

class CGlDetailLog extends CBaseGlDetailLog {

	protected $m_strUnitNumber;
	protected $m_strPropertyName;
	protected $m_strGlAccountName;
	protected $m_strGlAccountNumber;
	protected $m_strGlDimensionName;
	protected $m_strCompanyDepartmentName;

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getGlAccountNumber() {
		return $this->m_strGlAccountNumber;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getGlDimensionName() {
		return $this->m_strGlDimensionName;
	}

	public function getCompanyDepartmentName() {
		return $this->m_strCompanyDepartmentName;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = CStrings::strTrimDef( $strPropertyName, 50, NULL, true );
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = CStrings::strTrimDef( $strGlAccountName, 50, NULL, true );
	}

	public function setGlAccountNumber( $strGlAccountNumber ) {
		$this->m_strGlAccountNumber = $strGlAccountNumber;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = CStrings::strTrimDef( $strUnitNumber, 50, NULL, true );
	}

	public function setGlDimensionName( $strGlDimensionName ) {
		$this->m_strGlDimensionName = CStrings::strTrimDef( $strGlDimensionName, 50, NULL, true );
	}

	public function setCompanyDepartmentName( $strCompanyDepartmentName ) {
		$this->m_strCompanyDepartmentName = CStrings::strTrimDef( $strCompanyDepartmentName, 50, NULL, true );
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['property_name'] ) )				$this->setPropertyName( $arrValues['property_name'] );
		if( true == isset( $arrValues['gl_account_name'] ) )			$this->setGlAccountName( $arrValues['gl_account_name'] );
		if( true == isset( $arrValues['gl_account_number'] ) )			$this->setGlAccountNumber( $arrValues['gl_account_number'] );
		if( true == isset( $arrValues['unit_number'] ) )				$this->setUnitNumber( $arrValues['unit_number'] );
		if( true == isset( $arrValues['gl_dimension_name'] ) )			$this->setGlDimensionName( $arrValues['gl_dimension_name'] );
		if( true == isset( $arrValues['company_department_name'] ) )	$this->setCompanyDepartmentName( $arrValues['company_department_name'] );
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDiagnosticLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccrualGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCashGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlReconciliationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOffsettingGlDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlExportBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>