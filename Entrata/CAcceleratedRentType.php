<?php

class CAcceleratedRentType extends CBaseAcceleratedRentType {

	const ALLOW_OPTIONAL 	= 1;
	const ALWAYS_POST	 	= 2;
	const NEVER_POST 		= 3;

}
?>