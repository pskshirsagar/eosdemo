<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserCalendars
 * Do not add any new functions to this class.
 */

class CCompanyUserCalendars extends CBaseCompanyUserCalendars {

	public static function fetchCompanyUserCalendarByCompanyUserIdByCalendarPropertyIdByCid( $intCompanyUserId, $intCalendarPropertyId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				cuc.*
			FROM company_user_calendars cuc
			WHERE cuc.cid = ' . ( int ) $intCid . '
			AND cuc.calendar_property_id = ' . ( int ) $intCalendarPropertyId . '
			AND cuc.company_user_id = ' . ( int ) $intCompanyUserId;

		return self::fetchCompanyUserCalendar( $strSql, $objDatabase ) ?: [];
	}

	public static function fetchCompanyUserCalendarsByIdsByCid( $arrintCompanyUserCalendarIds, $intCid, CDatabase $objDatabase ) {
		if( true == is_null( $arrintCompanyUserCalendarIds ) || true == is_null( $intCid ) ) return [];

		$strSql = '
			SELECT
				cuc.*,
				CASE
					WHEN cu.id = cuc.company_user_id THEN \'My Calendar\'	-- This is the calendar for the current user
					WHEN cu.id IS NULL THEN p.property_name					-- This is a property calendar
					ELSE ce.name_first || \' \' || ce.name_last				-- This is a calendar for another user
				END AS calendar_name
			FROM company_user_calendars cuc
			LEFT JOIN company_users cu ON cuc.cid = cu.cid AND cuc.calendar_company_user_id = cu.id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
			LEFT JOIN company_employees ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
			LEFT JOIN company_user_property_groups cupg ON cu.cid = cupg.cid AND cu.id = cupg.company_user_id AND cuc.calendar_property_id IS NOT NULL
			LEFT JOIN property_groups pg ON ( pg.cid = cuc.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
			LEFT JOIN property_group_associations pga ON pg.cid = pga.cid AND pg.id = pga.property_group_id
			LEFT JOIN properties p ON cuc.cid = p.cid AND ( cuc.calendar_property_id = p.id OR pga.property_id = p.id )
			WHERE cuc.cid = ' . ( int ) $intCid . '
			AND cuc.id IN ( ' . implode( ',', $arrintCompanyUserCalendarIds ) . ' )
			';

		return self::fetchCompanyUserCalendars( $strSql, $objDatabase, false ) ?: [];
	}

	public static function fetchCompanyUserCalendarsByCidByCompanyUserId( $objCompanyUser, CDatabase $objDatabase ) {
		if( true == is_null( $objCompanyUser->getCid() ) || true == is_null( $objCompanyUser->getId() ) ) return [];

		$objCompanyUser->setIsPSIUser();

		$strChildSql = \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->getCompanyUserPreferenceSql( 'ENABLE_ENTRATA_CALENDAR', $objCompanyUser->getCid() );
		$strSqlCalendars = '
						with temp_company_preference AS( ' . $strChildSql . ' )
								SELECT
									cuc.*,
									CASE
										WHEN cu.id = cuc.company_user_id THEN \'My Calendar\'		-- This is the calendar for the current user
										WHEN cu.id IS NULL THEN p.property_name						-- This is a property calendar
										ELSE ce.name_first || \' \' || ce.name_last					-- This is a calendar for another user
									END AS calendar_name,
									CASE
										WHEN cu.id = cuc.company_user_id THEN NULL					-- The user calendar has no group
										WHEN cu.is_administrator = 1 THEN \'Other Calendars\'		-- Users with no property group associations appear here
										ELSE p.property_name										-- Other calendars are grouped by property
									END AS calendar_group,
									CASE
										WHEN cu.id = cuc.company_user_id THEN 0						-- The user calendar has no group
										WHEN cupg.id IS NULL AND p.id IS NULL THEN 1				-- Users with no property group associations appear here
										ELSE p.id
									END AS category_id
								FROM
									company_user_calendars cuc
									LEFT JOIN company_users cu ON cuc.cid = cu.cid AND cuc.calendar_company_user_id = cu.id AND cu.is_disabled = 0 AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
									LEFT JOIN company_employees ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
									LEFT JOIN company_user_property_groups cupg ON cu.cid = cupg.cid AND cu.id = cupg.company_user_id AND cuc.calendar_property_id IS NULL AND cupg.company_user_id <> cuc.company_user_id AND cu.is_administrator = 0
									LEFT JOIN property_groups pg ON ( pg.cid = cuc.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
									LEFT JOIN property_group_associations pga ON pg.cid = pga.cid AND pg.id = pga.property_group_id
									LEFT JOIN properties p ON cuc.cid = p.cid AND ( cuc.calendar_property_id = p.id OR pga.property_id = p.id )
									LEFT JOIN load_properties( ARRAY[ ' . ( int ) $objCompanyUser->getCid() . ' ], ARRAY[]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = p.id
									LEFT JOIN temp_company_preference tcp ON tcp.cid = cuc.cid AND tcp.company_user_id = cuc.calendar_company_user_id AND tcp.value = \'1\' 
								WHERE
									cuc.cid = ' . ( int ) $objCompanyUser->getCid() . '
									AND cuc.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
									AND ( lp.property_id IS NOT NULL OR cuc.calendar_company_user_id = ' . ( int ) $objCompanyUser->getId() . ' OR cu.is_administrator = 1 )
									AND ( CASE
										WHEN p.id IS NOT NULL THEN p.property_type_id <> ' . CPropertyType::TEMPLATE . '
										ELSE true
									END )
									AND ( tcp.company_user_id IS NOT NULL OR cuc.calendar_company_user_id is NULL )
								ORDER BY
									CASE
										WHEN cu.id = cuc.company_user_id THEN 1
										WHEN cupg.id IS NULL AND p.id IS NULL THEN 2
										ELSE 3
									END,
									p.property_name,
									ce.name_first || ce.name_last NULLS FIRST
						';

		if( true == $objCompanyUser->getIsPSIUser() || true == $objCompanyUser->getIsAdministrator() ) {
			return self::fetchCompanyUserCalendars( $strSqlCalendars, $objDatabase, false ) ?: [];
		}

		$strSql = '
			WITH calendars AS
				( ' . $strSqlCalendars . ' )

			SELECT
				c.*
			FROM calendars c
			WHERE
				c.cid = ' . ( int ) $objCompanyUser->getCid() . '
				AND c.category_id IN ( 0, 1 )

			UNION ALL

			SELECT
				c.*
			FROM calendars c
			JOIN company_users cu ON c.cid = cu.cid AND ' . ( int ) $objCompanyUser->getId() . ' = cu.id AND cu.is_disabled = 0 AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
			JOIN company_user_property_groups cupg ON cupg.cid = c.cid AND cupg.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
			JOIN property_group_associations pga ON pga.cid = cupg.cid AND pga.property_group_id = cupg.property_group_id AND c.category_id = pga.property_id
			JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
			WHERE
				c.cid = ' . ( int ) $objCompanyUser->getCid() . '
				AND ( pga.property_id IS NOT NULL )
				OR ( c.calendar_company_user_id IS NOT NULL AND c.category_id = pga.property_id )
		';

		return self::fetchCompanyUserCalendars( $strSql, $objDatabase, false ) ?: [];
	}

	public static function fetchCompanyUserMyCalendarByCompanyUserIdByCid( $intCompanyUserId, $intCid, CDatabase $objDatabase ) {

		if( false == valId( $intCompanyUserId ) || false == valId( $intCid ) ) return [];

		$strSql = '
			SELECT
				cuc.*
			FROM company_user_calendars cuc
			WHERE cuc.cid = ' . ( int ) $intCid . '
			AND cuc.company_user_id = ' . ( int ) $intCompanyUserId . '
			AND cuc.calendar_company_user_id = ' . ( int ) $intCompanyUserId . '
		';

		return self::fetchCompanyUserCalendar( $strSql, $objDatabase ) ?: [];
	}

	public static function fetchPotentialCalendarsByCidByCompanyUserIdByPropertyIdByNameMatch( $intCid, $intCompanyUserId, CDatabase $objDatabase, $boolCanEditProperty, $intPropertyId = NULL, $strNameMatch = NULL, $boolIsAddEvent = false, $arrmixExcludeCalendars = NULL, $boolIsCommunityEvent = false ) {
		$arrintExcludeCalendarPropertyIds		= [];
		$arrintExcludeCalendarCompanyUserIds	= [];

		if( true == valArr( $arrmixExcludeCalendars ) ) {
			$arrintExcludeCalendarPropertyIds		= $arrmixExcludeCalendars['calendar_property_ids'];
			$arrintExcludeCalendarCompanyUserIds	= $arrmixExcludeCalendars['calendar_company_user_ids'];
		}

		$strSql = '
			WITH assigned_properties AS (
				SELECT
					p.cid,
					p.id AS calendar_property_id,
					NULL::INTEGER AS calendar_company_user_id,
					p.property_name AS category,';

		if( true == $boolCanEditProperty || false == $boolIsAddEvent ) {
			$strSql .= 'p.property_name || \' Calendar\' AS label,';
		} else {
			$strSql .= 'NULL::TEXT AS label,';
		}

		$strSql .= 'p.id AS property_id,
					p.id AS category_id,
					2 AS primary_order,
					1 AS secondary_order
				FROM
					company_users cu
					LEFT JOIN company_user_property_groups cupg ON cu.cid = cupg.cid AND cu.id = cupg.company_user_id
					LEFT JOIN property_groups pg ON ( pg.cid = cu.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					LEFT JOIN property_group_associations pga ON cu.cid = pga.cid AND ( pg.id = pga.property_group_id OR cu.is_administrator = 1 )
					JOIN properties p ON pga.cid = p.cid AND pga.property_id = p.id AND p.is_disabled = 0 AND p.property_type_id NOT IN( ' . implode( ', ', [ CPropertyType::CLIENT, CPropertyType::SETTINGS_TEMPLATE, CPropertyType::TEMPLATE ] ) . ' )
					LEFT JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = p.id
				WHERE
					cu.cid = ' . ( int ) $intCid . '
					AND cu.id = ' . ( int ) $intCompanyUserId . '
					AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					AND cu.is_disabled = 0
					AND lp.property_id IS NOT NULL
				GROUP BY
					p.cid,
					p.id
				ORDER BY
					p.property_name
			), associated_users AS (
				SELECT
					cu.cid,
					NULL::INTEGER AS calendar_property_id,
					cu.id AS calendar_company_user_id,
					p.property_name,
					CASE
						WHEN cu.id =' . ( int ) $intCompanyUserId . ' THEN COALESCE( \' My Calendar (\' || ce.name_first || \' \' || ce.name_last || \')\' )
						ELSE COALESCE( ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last )
					END AS label,
					p.id AS property_id,
					p.id AS category_id,
					2 AS primary_order,
					2 AS secondary_order
				FROM
					assigned_properties ap
					JOIN properties p ON ap.cid = p.cid AND ap.calendar_property_id = p.id AND p.property_type_id NOT IN ( ' . implode( ', ', [ CPropertyType::CLIENT, CPropertyType::SETTINGS_TEMPLATE, CPropertyType::TEMPLATE ] ) . ' )
					JOIN property_group_associations pga ON ap.cid = pga.cid AND ap.calendar_property_id = pga.property_id
					JOIN company_user_property_groups cupg ON pga.cid = cupg.cid AND pga.property_group_id = cupg.property_group_id
					JOIN property_groups pg ON ( pg.cid = ap.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					JOIN company_users cu ON cupg.cid = cu.cid AND cupg.company_user_id = cu.id AND cu.is_disabled = 0 AND cu.is_administrator = 0 AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					JOIN company_employees ce on cu.cid = ce.cid AND ce.id = cu.company_employee_id
					LEFT JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], ARRAY[]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = p.id
				WHERE
					ap.cid = ' . ( int ) $intCid . '
					AND ( lp.property_id IS NOT NULL OR cu.id = ' . ( int ) $intCompanyUserId . ' OR cu.is_administrator = 1 )
			), global_users AS (
				SELECT
					cu.cid,
					NULL::INTEGER AS calendar_property_id,
					cu.id AS calendar_company_user_id,
					\'Other Calendars\'::TEXT AS category,
					CASE
						WHEN cu.id =' . ( int ) $intCompanyUserId . ' THEN COALESCE( \' My Calendar (\' || ce.name_first || \' \' || ce.name_last || \')\' )
						ELSE COALESCE( ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last )
					END AS label,
					NULL::INTEGER AS property_id,
					1 AS category_id,
					1 AS primary_order,
					1 AS secondary_order
				FROM
					company_users cu
					JOIN company_employees ce ON cu.cid = ce.cid AND cu.company_employee_id = ce.id
				WHERE
					cu.is_administrator = 1
					AND cu.default_company_user_id IS NULL
					AND cu.cid=' . ( int ) $intCid . '
					AND cu.is_disabled = 0
					AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
			 )
			SELECT
				c.calendar_property_id,
				c.calendar_company_user_id,
				c.label,
				c.category,
				c.category_id
			FROM
				(
					SELECT * FROM assigned_properties ap
					UNION
					SELECT * FROM associated_users au
					UNION
					SELECT * FROM global_users gu
				) c';

		if( true == $boolIsAddEvent ) {
			$strSql .= ' WHERE
							c.label ILIKE \'%' . $strNameMatch . '%\'';
		} else {
			$strSql .= ' LEFT JOIN company_user_calendars cuc ON cuc.cid = ' . ( int ) $intCid . ' AND cuc.company_user_id = ' . ( int ) $intCompanyUserId . ' AND cuc.cid = c.cid AND ( cuc.calendar_property_id = c.calendar_property_id OR cuc.calendar_company_user_id = c.calendar_company_user_id )
						WHERE
							cuc.id IS NULL
							AND c.label ILIKE \'%' . $strNameMatch . '%\'';
		}

		if( valStr( $intPropertyId ) ) {
			$strSql .= ' AND c.property_id=' . ( int ) $intPropertyId;
		}

		if( true == valArr( $arrintExcludeCalendarPropertyIds ) ) {
			$strSql .= '
					AND ( c.calendar_property_id NOT IN (' . implode( ',', array_map( 'intval', $arrintExcludeCalendarPropertyIds ) ) . ') OR c.calendar_property_id IS NULL )';
		}

		if( true == valArr( $arrintExcludeCalendarCompanyUserIds ) ) {
			$strSql .= '
					AND ( c.calendar_company_user_id NOT IN (' . implode( ',', array_map( 'intval', $arrintExcludeCalendarCompanyUserIds ) ) . ') OR c.calendar_company_user_id IS NULL )';
		}

		if( true == $boolIsCommunityEvent ) {
			$strSql .= ' AND c.calendar_property_id IS NOT NULL ';
		}

		$strSql .= '
			ORDER BY
				c.primary_order,
				c.category,
				c.secondary_order,
				c.label
		';

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * Builds a list of company_user_calendars that can be inserted when a user views the full calendar for the first time
	 *
	 * @param CCompanyUser $objCompanyUser
	 * @param CDatabase $objDatabase
	 * @param array $arrmixColorPalette A color palette that will be assigned sequentially to each newly created calendar; should be of the form array( 'e45c55' => array( 'fg' => '702222', 'light-bg' => 'f7a3a2', 'light-fg' => '7f3639' ), ...)
	 * @return array
	 */
	public static function initializeCompanyUserCalendarsForCompanyUser( CCompanyUser $objCompanyUser, CDatabase $objDatabase, $arrmixColorPalette ) {
		$arrstrCalendarColors	= array_keys( $arrmixColorPalette );
		$intPaletteSize			= \Psi\Libraries\UtilFunctions\count( $arrstrCalendarColors );

		// Create record for user, all associated properties, and (maybe?) all company users associated to those properties
		$strSql = '
			WITH self AS (
				SELECT
					' . ( int ) $objCompanyUser->getCid() . ' AS cid,
					' . ( int ) $objCompanyUser->getId() . ' AS company_user_id,
					NULL::INTEGER AS property_id,
					1 AS order
			)
			, assigned_properties AS (
				SELECT
					pga.cid AS cid,
					NULL::INTEGER AS company_user_id,
					pga.property_id AS property_id,
					2 AS order
				FROM
					self s
					JOIN company_user_property_groups cupg ON cupg.cid = s.cid AND cupg.company_user_id = s.company_user_id
					JOIN property_group_associations pga ON cupg.cid = pga.cid AND cupg.property_group_id = pga.property_group_id
					JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					JOIN properties p ON pga.cid = p.cid AND pga.property_id = p.id AND p.property_type_id NOT IN( ' . implode( ', ', [ CPropertyType::CLIENT, CPropertyType::SETTINGS_TEMPLATE, CPropertyType::TEMPLATE ] ) . ' )
					JOIN load_properties( ARRAY[ ' . ( int ) $objCompanyUser->getCid() . ' ], ARRAY[]::INT[], ARRAY[ ' . CPsProduct::ENTRATA . ', ' . CPsProduct::LEASING_CENTER . ', ' . CPsProduct::LEAD_MANAGEMENT . ' ] ) lp ON lp.property_id = p.id
				WHERE
					s.cid = ' . ( int ) $objCompanyUser->getCid() . '
					AND lp.property_id IS NOT NULL
			)
			/*, property_users AS (
				SELECT
					cupg.cid AS cid,
					cupg.company_user_id AS company_user_id,
					NULL::INTEGER AS property_id,
					3 AS order
				FROM
					assigned_properties ap
					JOIN property_group_associations pga ON ap.cid = pga.cid AND ap.property_id = pga.property_group_id
					JOIN company_user_property_groups cupg ON cupg.cid = pga.cid AND cupg.property_group_id = pga.property_group_id
					JOIN property_groups pg ON ( pg.cid = ap.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					JOIN company_users cu ON cupg.cid = cu.cid AND cupg.company_user_id = cu.id AND cu.default_company_user_id IS NULL AND cu.id <> ' . ( int ) $objCompanyUser->getId() . ' AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
				WHERE
					ap.cid = ' . ( int ) $objCompanyUser->getCid() . '
					AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
			)*/
			SELECT
				t.cid,
				t.company_user_id AS calendar_company_user_id,
				t.property_id AS calendar_property_id,
				(row_number() OVER(ORDER BY t.order, t.property_id, t.company_user_id)-1)%' . $intPaletteSize . ' AS palette_id
			FROM
				(
					SELECT * FROM self
					UNION
					SELECT * FROM assigned_properties
					-- UNION
					-- SELECT * FROM property_users
				) t
			ORDER
				BY t.order
		';

		$arrintCompanyUserCalendarData = fetchData( $strSql, $objDatabase );

		$arrobjCompanyUserCalendars = [];
		foreach( $arrintCompanyUserCalendarData as $arrintCompanyUserCalendar ) {
			$arrintCompanyUserCalendar['calendar_color']	= $arrstrCalendarColors[$arrintCompanyUserCalendar['palette_id']];
			$arrintCompanyUserCalendar['company_user_id']	= $objCompanyUser->getId();

			$objCompanyUserCalendar = new CCompanyUserCalendar();
			$objCompanyUserCalendar->setValues( $arrintCompanyUserCalendar );
			$arrobjCompanyUserCalendars[] = $objCompanyUserCalendar;
		}

		return $arrobjCompanyUserCalendars;
	}

}
?>