<?php

class CEmailTemplateCustomTexts extends CBaseEmailTemplateCustomTexts {

	public static function fetchEmailTemplateCustomTextByScheduledEmailIdByEmailTemplateIdByKeyByCid( $intScheduledEmailId, $intEmailTemplateId, $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM email_template_custom_texts WHERE scheduled_email_id = ' . ( int ) $intScheduledEmailId . ' AND system_message_template_id = ' . ( int ) $intEmailTemplateId . ' AND cid = ' . ( int ) $intCid . ' AND key = \'' . $strKey . '\'';
		return self::fetchEmailTemplateCustomText( $strSql, $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextsByScheduledEmailIdByEmailTemplateIdByCid( $intScheduledEmailId, $intEmailTemplateId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM email_template_custom_texts WHERE scheduled_email_id = ' . ( int ) $intScheduledEmailId . ' AND system_message_template_id = ' . ( int ) $intEmailTemplateId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchEmailTemplateCustomTexts( $strSql, $objDatabase );
	}

	public static function fetchEmailTemplateCustomTextsByCampaignIdByEmailTemplateIdByCid( $intCampaignId, $intEmailTemplateId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM email_template_custom_texts WHERE campaign_id = ' . ( int ) $intCampaignId . ' AND system_message_template_id = ' . ( int ) $intEmailTemplateId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchEmailTemplateCustomTexts( $strSql, $objDatabase );
	}

	// MC2.0

	public static function fetchSimpleEmailTemplateCustomTextsByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						key, value
					FROM
						email_template_custom_texts etct
						JOIN scheduled_emails se on ( se.id = etct.scheduled_email_id AND se.system_message_template_id = etct.system_message_template_id AND se.cid = etct.cid )
					WHERE
						se.id = ' . ( int ) $intScheduledEmailId . '
						AND se.cid = ' . ( int ) $intCid;

		$arrmixResponses = ( array ) fetchData( $strSql, $objDatabase );

		$arrmixData = [];

		foreach( $arrmixResponses as $arrstrResponse ) {
			$arrmixData[$arrstrResponse['key']] = $arrstrResponse['value'];
		}

		return $arrmixData;
	}

	public static function fetchSimpleEmailTemplateCustomTextsByCampaignIdByCid( $intCampaignId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						key, value
					FROM
						email_template_custom_texts etct
						JOIN campaigns c on ( c.id = etct.campaign_id AND c.system_message_template_id = etct.system_message_template_id AND c.cid = etct.cid )
					WHERE
						c.id = ' . ( int ) $intCampaignId . '
						AND c.cid = ' . ( int ) $intCid;

		$arrmixResponses = ( array ) fetchData( $strSql, $objDatabase );

		$arrmixData = [];

		foreach( $arrmixResponses as $arrstrResponse ) {
			$arrmixData[$arrstrResponse['key']] = $arrstrResponse['value'];
		}

		return $arrmixData;
	}

	public static function fetchEmailTemplateCustomTextsByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM email_template_custom_texts WHERE scheduled_email_id = ' . ( int ) $intScheduledEmailId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchEmailTemplateCustomTexts( $strSql, $objDatabase );
	}

	public static function fetchSimpleEmailTemplateCustomTextsByScheduledTaskEmailIdByCid( $intScheduledTaskEmailId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						key, value
					FROM
						email_template_custom_texts
					WHERE
						scheduled_task_email_id = ' . ( int ) $intScheduledTaskEmailId . '
						AND cid = ' . ( int ) $intCid;

		$arrmixResponses = ( array ) fetchData( $strSql, $objDatabase );

		$arrmixData = [];

		foreach( $arrmixResponses as $arrstrResponse ) {
			$arrmixData[$arrstrResponse['key']] = $arrstrResponse['value'];
		}

		return $arrmixData;
	}

}
?>