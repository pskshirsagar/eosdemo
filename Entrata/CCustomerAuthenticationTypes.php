<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAuthenticationTypes
 * Do not add any new functions to this class.
 */

class CCustomerAuthenticationTypes extends CBaseCustomerAuthenticationTypes {

	public static function fetchCustomerAuthenticationTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CCustomerAuthenticationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomerAuthenticationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCustomerAuthenticationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>