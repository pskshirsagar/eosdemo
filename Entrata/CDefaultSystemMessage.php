<?php

class CDefaultSystemMessage extends CBaseDefaultSystemMessage {

	const NEW_LEASES_BUA_NOTIFICATION = 396;
	const RENEWALS_BUA_NOTIFICATION   = 397;

	protected $m_arrobjMergeFieldGroups;

	public function __construct() {
		parent::__construct();
		$this->setAllowDifferentialUpdate( true );
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->setJsonbDetailsValue( 'from_email_address', $strFromEmailAddress );
	}

	public function setBccEmailAddresses( $strBccEmailAddresses ) {
		$this->setJsonbDetailsValue( 'bcc_email_addresses', $strBccEmailAddresses );
	}

	public function setCcEmailAddresses( $strCcEmailAddresses ) {
		$this->setJsonbDetailsValue( 'cc_email_addresses', $strCcEmailAddresses );
	}

	public function setReplyTo( $strReplyToEmailAddress ) {
		$this->setJsonbDetailsValue( 'reply_to', $strReplyToEmailAddress );
	}

	public function setPropertyPreference( $strPropertyPreference ) {
		$this->setJsonbDetailsValue( 'property_preference', $strPropertyPreference );
	}

	public function setPropertyHeaderFooterOptionTypeId( $strPropertyHeaderFooterOptionTypeId ) {
		$this->setJsonbDetailsValue( 'property_header_footer_option_type_id', $strPropertyHeaderFooterOptionTypeId );
	}

	public function setDefaultMergeFieldGroupIds( $arrintDefaultMergeFieldGroupIds ) {
		$this->m_arrintDefaultMergeFieldGroupIds = CStrings::strToArrIntDef( $arrintDefaultMergeFieldGroupIds, NULL );
	}

	public function setMergeFieldGroups( $arrobjMergeFieldGroups ) {
		$this->m_arrobjMergeFieldGroups = $arrobjMergeFieldGroups;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['from_email_address'] ) ) {
			$this->setFromEmailAddress( $arrmixValues['from_email_address'] );
		}

		if( isset( $arrmixValues['cc_email_addresses'] ) ) {
			$this->setCcEmailAddresses( $arrmixValues['cc_email_addresses'] );
		}

		if( isset( $arrmixValues['bcc_email_addresses'] ) ) {
			$this->setBccEmailAddresses( $arrmixValues['bcc_email_addresses'] );
		}

		if( isset( $arrmixValues['property_header_footer_option_type_id'] ) ) {
			$this->setPropertyHeaderFooterOptionTypeId( $arrmixValues['property_header_footer_option_type_id'] );
		}

		if( isset( $arrmixValues['reply_to'] ) ) {
			$this->setReplyTo( $arrmixValues['reply_to'] );
		}

		if( isset( $arrmixValues['default_merge_fields'] ) ) {
			$this->setDefaultMergeFieldGroupIds( $arrmixValues['default_merge_fields'] );
		}

		if( isset( $arrmixValues['property_preference'] ) ) {
			$this->setPropertyPreference( $arrmixValues['property_preference'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSystemMessageTypeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_message_type_id', __( 'Please select system message type.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSystemMessageAudienceId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSystemMessageAudienceId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_message_audience_id', __( 'Please select system message audience.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSystemMessageCategoryId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSystemMessageCategoryId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_message_category_id', __( 'Please select system message category.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSystemMessageTemplateId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSystemMessageTemplateId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_message_template_id', __( 'Please select template.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		$this->m_strName	= trim( $this->m_strName );

		if( false == isset( $this->m_strName ) || 3 > strlen( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'A name must contain at least 3 characters.' ) ) );
			return false;
		}

		$strName = preg_replace( '/[^a-zA-Z0-9_\-\s]/', '', $this->m_strName );

		if( strlen( $this->m_strName ) != strlen( $strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Only alphanumeric characters [0 to 9, A to Z ,'-' , '_'and a-z ] are allowed in name." ) ) );
			return false;
		}

		$this->m_strName = preg_replace( '/(\s\s+)/', ' ', $this->m_strName );

		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMergeFieldGroupIds() {
		$arrintMergeFieldGroupIds = $this->getMergeFieldGroupIds();

		if( true == valArr( $this->m_arrobjMergeFieldGroups ) && true == valArr( $arrintMergeFieldGroupIds ) ) {

			foreach( $arrintMergeFieldGroupIds as $intMergeFieldGroupId ) {
				if( false == array_key_exists( $intMergeFieldGroupId, $this->m_arrobjMergeFieldGroups ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merge_field_group_ids', __( 'Please select valid Merge Field Groups.' ) ) );
					return false;
				}
			}
		}

		return true;
	}

	public function valRequiredInputTables() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valSystemMessageTypeId();
				$boolIsValid &= $this->valSystemMessageAudienceId();
				$boolIsValid &= $this->valSystemMessageCategoryId();
				$boolIsValid &= $this->valSystemMessageTemplateId();
				$boolIsValid &= $this->valMergeFieldGroupIds();
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valSubject();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createCustomSystemMessage( $intPropertyId, $intCid, $strLocaleCode = CLocale::DEFAULT_LOCALE ) {
		$objSystemMessage = new CSystemMessage();
		$objSystemMessage->setCid( $intCid );
		$objSystemMessage->setPropertyId( $intPropertyId );
		$objSystemMessage->setMergeFieldGroupIds( $this->getMergeFieldGroupIds() );
		$objSystemMessage->setSystemMessageTypeId( $this->getSystemMessageTypeId() );
		$objSystemMessage->setSystemMessageAudienceId( $this->getSystemMessageAudienceId() );
		$objSystemMessage->setSystemMessageCategoryId( $this->getSystemMessageCategoryId() );
		$objSystemMessage->setRequiredInputTables( CStrings::arrToStrIntDef( $this->getRequiredInputTables(), NULL ) );
		$objSystemMessage->setKey( $this->getKey() );
		$objSystemMessage->setName( $this->getName() );
		$objSystemMessage->setReplyTo( $this->getReplyTo() );
		$objSystemMessage->setCcEmailAddresses( $this->getCcEmailAddresses() );
		$objSystemMessage->setBccEmailAddresses( $this->getBccEmailAddresses() );
		$objSystemMessage->setLocaleCode( $strLocaleCode );
		$objSystemMessage->setDefaultSystemMessageId( $this->getId() );

		$objSystemMessage->setPropertyPreference( $this->getPropertyPreference() );
		$objSystemMessage->setPropertyHeaderFooterOptionTypeId( $this->getPropertyHeaderFooterOptionTypeId() );

		$objSystemMessage->setIsPublished( true );

		return $objSystemMessage;
	}

	public function createCustomSmsSystemMessage( $intPropertyId, $intCid, $strLocaleCode = CLocale::DEFAULT_LOCALE ) {
		$objSystemMessage = new CSystemMessage();
		$objSystemMessage->setCid( $intCid );
		$objSystemMessage->setPropertyId( $intPropertyId );
		$objSystemMessage->setMergeFieldGroupIds( $this->getMergeFieldGroupIds() );
		$objSystemMessage->setSystemMessageTypeId( $this->getSystemMessageTypeId() );
		$objSystemMessage->setSystemMessageAudienceId( $this->getSystemMessageAudienceId() );
		$objSystemMessage->setSystemMessageCategoryId( $this->getSystemMessageCategoryId() );
		$objSystemMessage->setRequiredInputTables( CStrings::arrToStrIntDef( $this->getRequiredInputTables(), NULL ) );
		$objSystemMessage->setKey( $this->getKey() );
		$objSystemMessage->setName( $this->getName() );
		$objSystemMessage->setLocaleCode( $strLocaleCode );
		$objSystemMessage->setDefaultSystemMessageId( $this->getId() );
		$objSystemMessage->setSystemMessageTemplateId( CSystemMessageTemplate::CUSTOM );
		$objSystemMessage->setPropertyPreference( $this->getPropertyPreference() );

		$objSystemMessage->setIsPublished( true );

		return $objSystemMessage;
	}

	public static function createDefaultSystemMessage() {
		$objDefaultSystemMessage = new CDefaultSystemMessage();
		$objDefaultSystemMessage->setSystemMessageTemplateId( 1 );

		return $objDefaultSystemMessage;
	}

	public function setJsonbDetailsValue( $strKey, $mixValue ) {
		$this->setDetailsField( $strKey, $mixValue );
	}

}
?>
