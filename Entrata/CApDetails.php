<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApDetails
 * Do not add any new functions to this class.
 */

class CApDetails extends CBaseApDetails {

	public static function fetchApDetailsCountLocked( $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApDetailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ad.id,
						ah.header_number
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.id IN ( ' . implode( ',', $arrintApDetailIds ) . ' )
						AND 0 <> ad.transaction_amount_due
						AND NOT pg_try_advisory_xact_lock( util.util_calc_advisory_lock_key( ' . ( int ) $intCid . ', ' . CAdvisoryLockKey::AP_PAYMENT_PROCESS_DETAIL_ID . ' ), ad.id )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchViewApDetailByIdByCid( $intApDetailId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.id = ' . ( int ) $intApDetailId . '
					ORDER BY
						ad.id ASC';

		return self::fetchApDetail( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsInformationByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase, $boolHasJobCostingProduct = false ) {
		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strJobCostingJoins = '';
		$strJobCostingFields = '';

		if( true == $boolHasJobCostingProduct ) {
			$strJobCostingJoins = 'LEFT JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
									LEFT JOIN jobs j ON ( j.cid = jp.cid AND j.id = jp.job_id )
									LEFT JOIN ap_contracts apc ON ( apc.cid = ad.cid AND apc.id = ad.ap_contract_id )
									LEFT JOIN maintenance_locations ml ON ( ml.cid = ad.cid AND ml.id = ad.maintenance_location_id AND ml.is_published = 1 AND ml.is_system = 0 AND ml.maintenance_location_type_id = ' . CMaintenanceLocationType::PROPERTY . ')';

			$strJobCostingFields = ', concat_ws( \' - \', j.name, jp.name ) AS job_phase_name,
									apc.name AS ap_contract_name,
									ml.name as maintenance_location_name';
		}

		$strSql = 'SELECT
						DISTINCT ON ( ad.id )
						ad.*,
						ah_po.header_number,
						p.lookup_code,
						p.property_name,
						p.is_disabled AS is_disabled_property,
						CASE
							WHEN ad.inter_co_property_id IS NOT NULL THEN
								( SELECT p.property_name FROM properties p WHERE p.cid = ad.cid AND ad.inter_co_property_id = p.id )
							ELSE
								NULL
						END AS inter_co_property_name,
						util_get_translated( \'name\', glat.name, glat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS gl_account_name,
						glat.formatted_account_number AS gl_account_number,
						CASE
							WHEN ad.property_unit_id IS NULL THEN pb.building_name
							WHEN ad.property_unit_id IS NOT NULL AND pb.building_name IS NOT NULL THEN pb.building_name ||\' - \'|| pu.unit_number
							WHEN ad.property_unit_id IS NOT NULL AND pb.building_name IS NULL THEN pu.unit_number
							ELSE NULL END AS unit_number,
						cd.name AS company_department_name,
						gd.name AS gl_dimension_name,
						apsa.account_number AS ap_sub_account_number,
						ac.name as item_name,
						aci.vendor_sku,
						uom.name as unit_of_measure_name,
						pgs.is_ap_migration_mode' . $strJobCostingFields . ',
						adr.reimbursement_ap_detail_ids
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
						LEFT JOIN ap_codes ac ON ( ad.cid = ac.cid AND ad.ap_code_id = ac.id )
						LEFT JOIN ap_details ad_po ON ( ad.cid = ad_po.cid AND ad.po_ap_detail_id = ad_po.id )
						LEFT JOIN ap_headers ah_po ON ( ah_po.cid = ad_po.cid AND ad_po.ap_header_id = ah_po.id )
						LEFT JOIN unit_of_measures uom ON ( ad.cid = uom.cid AND ad.unit_of_measure_id = uom.id )
						LEFT JOIN ap_catalog_items aci ON ( aci.cid = ad.cid AND aci.ap_code_id = ac.id AND aci.id = ad.ap_catalog_item_id )
						JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						LEFT JOIN gl_account_trees glat ON ( ad.cid = glat.cid AND ad.gl_account_id = glat.gl_account_id AND glat.is_default = 1 )
						LEFT JOIN property_units pu ON ( ad.cid = pu.cid AND ad.property_unit_id = pu.id AND pu.deleted_on IS NULL )
						LEFT JOIN property_buildings pb ON ( ad.cid = pb.cid AND ad.property_building_id = pb.id AND pb.deleted_on IS NULL )
						LEFT JOIN company_departments cd ON ( ad.cid = cd.cid AND ad.company_department_id = cd.id )
						LEFT JOIN gl_dimensions gd ON ( ad.cid = gd.cid AND ad.gl_dimension_id = gd.id )
						LEFT JOIN ap_payee_sub_accounts apsa ON ( ad.cid = apsa.cid AND ad.ap_payee_sub_account_id = apsa.id )
						LEFT JOIN LATERAL (
							SELECT
								STRING_AGG( adr.reimbursement_ap_detail_id::text, \',\' ) AS reimbursement_ap_detail_ids
							FROM
								ap_detail_reimbursements adr
							WHERE
								adr.cid = ad.cid
								AND adr.original_ap_detail_id = ad.id
								AND adr.is_deleted = false
						) AS adr ON TRUE
						' . $strJobCostingJoins . '
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id IN( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
					ORDER BY
						ad.id ASC';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailByApHeaderIdByRemotePrimaryKeyByCid( $intApHeaderId, $strRemotePrimaryKey, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND LOWER( remote_primary_key ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strRemotePrimaryKey ) ) ) . '\'
						AND ap_header_id = ' . ( int ) $intApHeaderId . '
						AND reversal_ap_detail_id IS NULL
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApDetail( $strSql, $objClientDatabase );
	}

	public static function fetchSpecificUsersApDetailsByApDetailIdsByRuleConditionIdByCid( $arrintApDetailIds, $intRuleConditionId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN rule_condition_details rcd ON ( ad.cid = rcd.cid AND ad.created_by = rcd.reference_number )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND rcd.rule_condition_id = ' . ( int ) $intRuleConditionId . '
						AND ad.id = ANY( ARRAY[' . implode( ',', $arrintApDetailIds ) . ']::int[] )';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByApPayeeIdByGlTransactionTypeIdByCid( $intApPayeeId, $intGlTransactionTypeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.gl_transaction_type_id = ' . ( int ) $intGlTransactionTypeId;

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchIntegratedApDetailsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN ap_headers ah ON( ad.cid = ah.cid AND ad.ap_header_id= ah.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ad.remote_primary_key IS NOT NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	/**
	 * @deprecated - Use fetchUnpaidApDetailsByCidByFilter instead. Talk to Rahul Ippar for more information.
	 */

	public static function fetchApDetailsForPaymentsByApDetailIdsByCid( $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) return NULL;

			$strSql = 'SELECT
							ad.*,
							ah.ap_payee_id,
							ah.header_memo,
							ah.ap_payee_location_id,
							ah.ap_payee_account_id,
							ah.ap_payee_term_id,
							ah.ap_remittance_id,
							apl.vendor_code,
							apl.location_name,
							ah.lease_customer_id,
							ah.header_number,
							ah.due_date,
							ah.post_date,
							ah.post_month,
							ah.pay_with_single_check,
							( CASE WHEN( ah.lease_customer_id IS NULL) THEN \'vendor_\'||ah.ap_payee_id ELSE \'cust_\'||ah.lease_customer_id END ) AS ap_payee_key,
							( CASE WHEN( ah.lease_customer_id IS NULL) THEN ap.company_name ELSE func_format_refund_customer_names( ah.header_memo ) END ) AS ap_payee_name,
							( CASE WHEN( ah.lease_customer_id IS NOT NULL) THEN ah.header_memo END ) AS refund_customer_names,
							ba.account_name as bank_account_name,
							ba.next_check_number,
							ba.check_name_on_account,
							ba.check_name_on_account_line_2,
							ba.payer_street_line1,
							ba.payer_street_line2,
							ba.payer_city,
							ba.payer_state_code,
							ba.payer_postal_code,
							ba.check_bank_number,
							p.id AS property_id,
							p.property_name,
							apc.id as ap_payee_contact_id,
							apc.email_address,
							cc.is_visible AS is_check_memo_visible,
							apa.account_number,
							ar.street_line1,
							ar.street_line2,
							ar.street_line3,
							ar.is_published as is_published_remittance,
							ar.city,
							ar.state_code,
							ar.postal_code,
							ap.ap_payee_type_id,
							ad.inter_co_property_id,
							adr.id IS NOT NULL AS is_reimbursement_present
						FROM
							ap_details ad
							JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
							JOIN properties p ON ( p.cid = ad.cid AND p.id = ad.property_id )
							JOIN bank_accounts ba ON ( ad.cid = ba.cid AND ad.bank_account_id = ba.id )
							LEFT JOIN check_components cc ON ( cc.cid = ba.cid AND cc.check_template_id = ba.check_template_id AND cc.check_component_type_id = ' . CCheckComponentType::CHECK_MEMO . ' )
							LEFT JOIN ap_payees ap ON ( ap.cid = ah.cid AND ap.id = ah.ap_payee_id )
							LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
							LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apl.cid AND apcl.ap_payee_location_id = apl.id AND apcl.is_primary = true )
							LEFT JOIN ap_payee_contacts apc ON ( apc.cid = apl.cid AND apc.ap_payee_id = apl.ap_payee_id AND apcl.ap_payee_contact_id = apc.id )
							LEFT JOIN ap_payee_accounts apa ON ( apa.cid = ah.cid AND apa.id = ah.ap_payee_account_id )
							LEFT JOIN ap_remittances ar ON ( ar.cid = ah.cid AND ar.id = ah.ap_remittance_id )
							LEFT JOIN LATERAL (
								SELECT
									id
								FROM
									ap_detail_reimbursements adr
								WHERE
									ad.reimbursement_method_id = ' . CReimbursementMethod::REIMBURSEMENT_METHOD_USE_SUBLEDGERS_NEW . '
									AND adr.cid = ad.cid
									AND adr.original_ap_detail_id = ad.id
									AND adr.is_deleted = false
								LIMIT 1
							) AS adr ON TRUE
						WHERE
							ad.cid = ' . ( int ) $intCid . '
							AND ad.id IN ( ' . implode( ',', $arrintApDetailIds ) . ' )
							AND ah.reversal_ap_header_id IS NULL
							AND ad.reversal_ap_detail_id IS NULL
							AND ad.deleted_by IS NULL
							AND ad.deleted_on IS NULL
							AND ad.payment_approved_by IS NOT NULL
							AND ad.payment_approved_on IS NOT NULL
						ORDER BY
							-- account_name,
							-- ap_payee_name,
							-- ad.ap_header_id,
							ad.id ASC';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountApDetailsByRuleConditionIdByApDetailIdsByCid( $intRuleConditionId, $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN rule_condition_details rcd ON ( ad.cid = rcd.cid AND ad.gl_account_id = rcd.reference_number )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND rcd.rule_condition_id = ' . ( int ) $intRuleConditionId . '
						AND ad.id = ANY( ARRAY[' . implode( ',', $arrintApDetailIds ) . ']::int[] )';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeApDetailsByRuleConditionIdByApDetailIdsByCid( $intRuleConditionId, $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN ap_headers ah ON( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
						JOIN rule_condition_details rcd ON ( ah.cid = rcd.cid AND ah.ap_payee_id = rcd.reference_number )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND rcd.rule_condition_id = ' . ( int ) $intRuleConditionId . '
						AND ad.id = ANY( ARRAY[' . implode( ',', $arrintApDetailIds ) . ']::int[] )';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchBankAccountApDetailsByRuleConditionIdByApDetailIdsByCid( $intRuleConditionId, $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN rule_condition_details rcd ON ( ad.bank_account_id = rcd.reference_number AND ad.cid = rcd.cid )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND rcd.rule_condition_id = ' . ( int ) $intRuleConditionId . '
						AND ad.id = ANY( ARRAY[' . implode( ',', $arrintApDetailIds ) . ']::int[] )';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByApDetailIdByPropertyIdsByApHeaderTypeByCid( $intApHeaderId, $arrintPropertyIds, $intApHeaderTypeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ad.property_id,
						ad.gl_account_id,
						SUM( ad.transaction_amount ) AS transaction_amount
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id = ' . ( int ) $intApHeaderId . '
						AND ad.property_id IN(' . implode( ',', $arrintPropertyIds ) . ')
						AND ad.deleted_by IS NULL
						AND ah.ap_header_type_id = ' . ( int ) $intApHeaderTypeId . '
					GROUP BY
						ad.property_id,
						ad.gl_account_id';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchUnpaidApDetailsByPropertyIdsByBankAccountIdByCid( $arrintPropertyIds, $intBankAccountId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ad.id,
						ad.property_id,
						ad.ap_header_id,
						ad.line_item_payment_status,
						ad.header_number
					FROM
						(
							SELECT
								ad.id,
								ad.property_id,
								ad.ap_header_id,
								ah.header_number,
								CASE
									WHEN ad.transaction_amount > 0::numeric AND ABS ( COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) ) = ad.transaction_amount THEN ' . CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PAID . '
									WHEN ad.transaction_amount < 0::numeric AND COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) = ad.transaction_amount THEN ' . CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PAID . '
									WHEN ad.transaction_amount > 0::numeric AND ABS ( COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) ) < ad.transaction_amount AND COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) <> 0 THEN ' . CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PARTIALLY_PAID . '
									WHEN ad.transaction_amount < 0::numeric AND COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) > ad.transaction_amount AND COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) <> 0 THEN ' . CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PARTIALLY_PAID . '
									ELSE ' . CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNPAID . '
								END AS line_item_payment_status,
								ad.ap_export_batch_id,
								ad.exported_by,
								ad.exported_on
							FROM
								ap_details ad
								JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
								LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid
																			AND aa.credit_ap_detail_id IS NOT NULL
																			AND aa.charge_ap_detail_id IS NOT NULL
																			AND aa.is_deleted = false
																			AND ad.reversal_ap_detail_id IS NULL
																			AND ( ( ad.transaction_amount > 0 AND ad.id = aa.charge_ap_detail_id )
																					OR
																				( ad.transaction_amount <= 0 AND ad.id = aa.credit_ap_detail_id ) ) )
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND ah.is_batching = false
								AND ah.ap_payment_id IS NULL
								AND ah.reversal_ap_header_id IS NULL
								AND ad.bank_account_id = ' . ( int ) $intBankAccountId . '
								AND ah.is_posted = false
								AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL
								AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
							GROUP BY
								ad.id,
								ad.property_id,
								ad.ap_header_id,
								ah.header_number,
								ad.transaction_amount,
								ad.ap_export_batch_id,
								ad.exported_by,
								ad.exported_on
						) AS ad
					WHERE
						ad.line_item_payment_status <> ' . CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PAID;

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchViewApDetailsByIdsByCid( $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
					WHERE
						ad.id IN ( ' . implode( ',', $arrintApDetailIds ) . ' )
						AND ad.cid = ' . ( int ) $intCid . '
					ORDER BY
						ad.id ASC';
		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsForInvoiceVouchersByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objClientDatabase, $boolIncludeDeletedUnits = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;
		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$strCondition = ( ( true == valArr( $arrintPropertyIds ) ) ? 'AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '' );

		$strSql = 'SELECT
						DISTINCT
						ad.*,
						ad.transaction_amount,
						p.property_name,
						pu.unit_number,
						glat.formatted_account_number,
						TO_CHAR( ah.transaction_datetime, \'YYYY/MM/DD\' ) AS accounting_date,
						(
							SELECT
							ARRAY_TO_STRING( ARRAY(
								SELECT
									COALESCE( c.name_first, \'\' ) || \' \' || COALESCE( c.name_last, \'\' )
								FROM
									customers c
									JOIN lease_customers lc1 ON ( c.cid = lc1.cid AND c.id = lc1.customer_id )
								WHERE
									lc1.lease_id = lc.lease_id
									AND c.cid IN ( ' . implode( ',', $arrintCids ) . ' )
								  ORDER BY
									lc1.customer_type_id ASC
								), \' & \' )
						) AS customer_name,
						COALESCE ( ca.street_line1, \'\' ) ||
						CASE
							WHEN ( ca.street_line1 IS NOT NULL AND ca.street_line2 IS NOT NULL ) THEN \', \'
							ELSE \'\'
						END ||
						COALESCE ( ca.street_line2, \'\' ) ||
						CASE
							WHEN ( ca.street_line1 IS NOT NULL OR ca.street_line2 IS NOT NULL ) AND ca.street_line3 IS NOT NULL THEN \', \'
							ELSE \'\'
						END ||
						COALESCE ( ca.street_line3, \'\' ) AS forwarding_address,
						ca.city AS city,
						COALESCE( ca.state_code, ca.province ) AS state_code,
						co.code_three_digit AS code_three_digit,
						ca.postal_code AS postal_code,
						CASE
							WHEN lp.move_out_date IS NOT NULL THEN
								TO_CHAR( lp.move_out_date, \'YYYY/MM/DD\' )
							ELSE
								\'1900/01/01\'
						END AS move_out_date,
						COALESCE( TO_CHAR( lp.move_out_date, \'MM/DD/YYYY\' ), \'01/01/1900\' ) || \' \' || COALESCE( pu.unit_number ) || \' - PSI \' || lc.customer_id AS invoice_id,
						SUM( ad.transaction_amount ) OVER ( PARTITION BY ad.ap_header_id ) AS total_amount,
						pu.unit_number || \' \' || p.property_name AS line_item_description
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id= ad.ap_header_id )
						JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN property_preferences pp ON ( pp.cid = ad.cid AND pp.property_id = ad.property_id AND pp.key IN ( \'AP_BUSINESS_UNIT\' ) )
						JOIN gl_account_trees glat ON ( glat.cid = ad.cid AND glat.gl_account_id = ad.gl_account_id )
						LEFT JOIN lease_customers lc ON ( ah.cid = lc.cid AND ah.lease_customer_id = lc.id )
						LEFT JOIN lease_processes lp ON ( lc.cid =lp.cid AND lc.lease_id = lp.lease_id AND lp.customer_id IS NULL )
						LEFT JOIN leases l ON ( l.cid =lc.cid AND lc.lease_id = l.id )
						LEFT JOIN property_units pu ON ( l.cid = pu.cid AND pu.id = l.property_unit_id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN customer_addresses ca ON ( pu.cid = ca.cid AND ca.customer_id = lc.customer_id AND address_type_id = ' . CAddressType::FORWARDING . ' )
						LEFT JOIN countries co ON ( ca.country_code = co.code )
					WHERE
						ad.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ad.ap_export_batch_id IS NULL
						AND ad.reversal_ap_detail_id IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND ah.lease_customer_id IS NOT NULL
						AND ah.is_posted = true
						AND p.is_disabled = 0
						AND ah.is_temporary = false ' .
						$strCondition . '
					ORDER BY
						ad.id ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsForInvoiceVouchersByApExportBatchIdByCid( $intApExportBatchId, $intCid, $objClientDatabase, $boolIncludeDeletedUnits = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT
						ad.*,
						ad.transaction_amount,
						ah.header_number,
						p.property_name,
						pu.unit_number,
						glat.formatted_account_number,
						glat.name,
						TO_CHAR( ah.transaction_datetime, \'YYYY/MM/DD\' ) AS accounting_date,
						(
							SELECT
								ARRAY_TO_STRING( ARRAY(
									SELECT
										COALESCE( c.name_first, \'\' ) || \' \' || COALESCE( c.name_last, \'\' )
								FROM
									customers c
									JOIN lease_customers lc1 ON ( c.cid = lc1.cid AND c.id = lc1.customer_id )
								WHERE
									lc1.lease_id = lc.lease_id
									AND lc1.cid = lc.cid
									AND c.cid = ' . ( int ) $intCid . '
								ORDER BY
										lc1.customer_type_id ASC
								), \' & \' )
						) AS customer_name,
						COALESCE ( ca.street_line1, \'\' ) ||
						CASE
							WHEN ( ca.street_line1 IS NOT NULL AND ca.street_line2 IS NOT NULL ) THEN \', \'
							ELSE \'\'
						END ||
						COALESCE ( ca.street_line2, \'\' ) ||
						CASE
							WHEN ( ca.street_line1 IS NOT NULL OR ca.street_line2 IS NOT NULL ) AND ca.street_line3 IS NOT NULL THEN \', \'
							ELSE \'\'
						END ||
						COALESCE ( ca.street_line3, \'\' ) AS forwarding_address,
						ca.city AS city,
						COALESCE( ca.state_code, ca.province ) AS state_code,
						co.code_three_digit AS code_three_digit,
						ca.postal_code AS postal_code,
						CASE
							WHEN lp.move_out_date IS NOT NULL THEN
								TO_CHAR( lp.move_out_date, \'YYYY/MM/DD\' )
							ELSE
								\'1900/01/01\'
						END AS move_out_date,
						COALESCE( TO_CHAR( lp.move_out_date, \'MM/DD/YYYY\' ), \'01/01/1900\' ) || \' \' || COALESCE( pu.unit_number ) || \' - PSI \' || lc.customer_id AS invoice_id,
						SUM( ad.transaction_amount ) OVER ( PARTITION BY ad.ap_header_id ) AS total_amount,
						pu.unit_number || \' \' || p.property_name AS line_item_description
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id= ad.ap_header_id )
						JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN property_preferences pp ON ( pp.cid = ad.cid AND pp.property_id = ad.property_id AND pp.key IN ( \'AP_BUSINESS_UNIT\' ) )
						JOIN gl_account_trees glat ON ( ad.cid = glat.cid AND ad.gl_account_id = glat.gl_account_id )
						LEFT JOIN lease_customers lc ON ( ah.cid = lc.cid AND ah.lease_customer_id = lc.id )
						LEFT JOIN lease_processes lp ON ( lc.cid =lp.cid AND lc.lease_id = lp.lease_id AND lp.customer_id IS NULL )
						LEFT JOIN leases l ON ( l.cid =lc.cid AND lc.lease_id = l.id )
						LEFT JOIN property_units pu ON ( l.cid = pu.cid AND pu.id = l.property_unit_id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN customer_addresses ca ON ( pu.cid = ca.cid AND ca.customer_id = lc.customer_id AND address_type_id = ' . CAddressType::FORWARDING . ' )
						LEFT JOIN countries co ON ( ca.country_code = co.code )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_export_batch_id = ' . ( int ) $intApExportBatchId . '
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
					ORDER BY
						ad.id ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase, $objApTransactionsFilter = NULL, $boolArrayFormat = false, $boolIsFromClearBank = false, $boolHasJobCostingProduct = false, $arrmixParameters = NULL ) {

		if( false == valIntArr( $arrintApHeaderIds ) ) return NULL;

		$strReversalCondition = '';
		$strJobCostingJoins = '';
		$strJobCostingFields = '';
		$strJobCostingConditions = '';

		if( false == $boolIsFromClearBank ) {
			$strReversalCondition = ' AND ad.reversal_ap_detail_id IS NULL';
		}

		if( ( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && ( true == $objApTransactionsFilter->getShowReversals() || true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_REVERSED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) ) || true == $boolIsFromClearBank ) {
			$strReversalCondition = ' AND ad.reversal_ap_detail_id IS NOT NULL';
		}

		if( true == $boolHasJobCostingProduct ) {
			$strJobCostingJoins = 'LEFT JOIN ap_codes ac ON( ad.cid= ac.cid AND ad.ap_code_id=ac.id)
									LEFT JOIN job_phases jp ON( ad.cid = jp.cid AND ad.job_phase_id = jp.id )
									LEFT JOIN unit_types ut ON (ut.cid = ad.cid AND ut.id = ad.unit_type_id)
									LEFT JOIN maintenance_locations ml ON (ml.cid = ad.cid AND ml.id = ad.maintenance_location_id)';

			$strJobCostingFields = ',
									ac.name AS ap_code_name,
									jp.name AS job_phase_name,
									ut.name AS unit_type_name,
									ml.name AS location_name,
									ac.item_gl_account_id';

			$strJobCostingConditions .= ( true == $arrmixParameters['is_from_contract'] ) ? ' AND ad.ap_contract_id IS NOT NULL' : '';

			$strJobCostingConditions .= ( 'job_budget' == $arrmixParameters['budget_type'] ) ? ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id IS NULL' : '';
			$strJobCostingConditions .= ( 'unit_budget' == $arrmixParameters['budget_type'] && true == valId( $arrmixParameters['budget_type_id'] ) ) ? ' AND ad.maintenance_location_id IS NULL AND ad.unit_type_id =' . $arrmixParameters['budget_type_id'] : '';
			$strJobCostingConditions .= ( 'maintenance_location_budget' == $arrmixParameters['budget_type'] && true == valId( $arrmixParameters['budget_type_id'] ) ) ? ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id=' . $arrmixParameters['budget_type_id'] : '';
		}

		$strSql = 'SELECT
						ad.*, adr.reimbursement_ap_detail_id AS adr_reimbursement_ap_detail_id,
						adr.reimbursement_gl_detail_id' . $strJobCostingFields . '
					FROM
						ap_details ad
						LEFT JOIN ap_detail_reimbursements adr ON ( ad.cid = adr.cid AND ad.id = adr.original_ap_detail_id AND adr.is_deleted = false )
						' . $strJobCostingJoins . '
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL' .
						$strReversalCondition . $strJobCostingConditions;

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objApTransactionsFilter->getApDetailsIds() ) ) {
			$strSql .= ' AND ad.id IN ( ' . implode( ',', $objApTransactionsFilter->getApDetailsIds() ) . ' )';
		}

		$strSql .= ' ORDER BY
						ad.id
					ASC';

		if( true == $boolArrayFormat ) {

			return fetchData( $strSql, $objClientDatabase );
		}

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByApHeaderIdsByBankAccountIdsByCid( $arrintApHeaderIds, $arrintBankAccountIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApHeaderIds ) || false == valIntArr( $arrintBankAccountIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND bank_account_id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' )
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND reversal_ap_detail_id IS NULL
						AND gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . ' 
						AND transaction_amount_due <> 0
					ORDER BY
						id';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchUnApprovedApDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_details ad
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ad.is_approved_for_payment = false
						AND ad.reversal_ap_detail_id IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchUnPaidCreditInvoicesApDetailsByApPayeeIdByApPayeeLocationIdByApHeaderIdsByCompanyUserIdByCid( $intApPayeeId, $intApPayeeLocationId, $intApPayeeAccountId, $arrintApHeaderIds, $intCompanyUserId, $intCid, $objClientDatabase, $arrintInvoiceTypePermission, $boolIsAdministrator = false ) {

		if( false == valArr( $arrintApHeaderIds ) || false == is_numeric( $intApPayeeId ) || false == is_numeric( $intApPayeeLocationId ) || false == is_numeric( $intApPayeeAccountId ) || false == valArr( $arrintInvoiceTypePermission ) ) return NULL;

		$strSql = 'SELECT
						ad.*,
						p.property_name,
						ah.header_number
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ap.cid = ah.cid )
						JOIN properties p ON ( p.id = ad.property_id AND p.cid = ad.cid )
					WHERE
						ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND ah.ap_payee_account_id = ' . ( int ) $intApPayeeAccountId . '
						AND ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ah.is_posted = true
						AND ah.is_on_hold = false
						AND ad.reversal_ap_detail_id IS NULL
						AND ah.ap_payment_id IS NULL
						AND ad.transaction_amount_due <> 0
						AND ad.ap_transaction_type_id NOT IN ( ' . CApTransactionType::STANDARD_REIMBURSEMENT . ', ' . CApTransactionType::BILL_BACK_REIMBURSEMENT . ', ' . CApTransactionType::STANDARD_REIMBURSEMENT_NEW . ' )
						AND ad.id NOT IN (
														SELECT
															DISTINCT ad.id
														FROM
															ap_details ad
															LEFT JOIN ( ' . CProperties::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS vcup ON( vcup.cid = ad.cid AND vcup.id = ad.property_id AND vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' )
														WHERE
															ad.cid = ' . ( int ) $intCid . '
															AND ( ( vcup.id IS NULL )
																	OR
																	( vcup.is_disabled = 1 AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL ) )
													)
						AND ad.deleted_by IS NULL
						AND ap.ap_payee_status_type_id =' . CApPayeeStatusType::ACTIVE . '
						AND ah.ap_header_sub_type_id IN( ' . sqlIntImplode( $arrintInvoiceTypePermission ) . ' )
					ORDER BY
						ah.post_date ASC,
						ad.ap_header_id ASC,
						ad.id ASC';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchUnPaidChargeInvoicesApDetailsByApPayeeIdByApPayeeLocationIdByCompanyUserIdByPropertyIdByCid( $arrmixParameters, $intCid, $objClientDatabase ) {

		$intApPayeeId					= getArrayElementByKey( 'ap_payee_id', $arrmixParameters );
		$intApPayeeLocationId			= getArrayElementByKey( 'ap_payee_location_id', $arrmixParameters );
		$intApPayeeAccountId			= getArrayElementByKey( 'ap_payee_account_id', $arrmixParameters );
		$intCompanyUserId				= getArrayElementByKey( 'company_user_id', $arrmixParameters );
		$intPropertyId					= getArrayElementByKey( 'property_id', $arrmixParameters );
		$arrintInvoiceTypePermission	= getArrayElementByKey( 'invoice_type_permissions', $arrmixParameters );
		$boolIsAdministrator			= ( getArrayElementByKey( 'is_administrator', $arrmixParameters ) ) ? true : false;
		$strUnpaidApHeaderIds			= ( getArrayElementByKey( 'unpaid_credit_ap_header_ids', $arrmixParameters ) ) ? getArrayElementByKey( 'unpaid_credit_ap_header_ids', $arrmixParameters ) : NULL;

		if( false == is_numeric( $intApPayeeId ) || false == is_numeric( $intApPayeeLocationId ) || false == is_numeric( $intCompanyUserId ) || false == is_numeric( $intPropertyId ) || false == valArr( $arrintInvoiceTypePermission ) ) return NULL;

		if( true == valStr( $strUnpaidApHeaderIds ) ) {
			$strOrderByCondition = 'array_position( ARRAY[' . $strUnpaidApHeaderIds . '], ah.id )';
		}

		$strSql = 'SELECT
						ad.*,
						ah.header_number,
						apa.account_number
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ap.cid = ah.cid )
						JOIN ap_payee_accounts apa ON ( ah.cid = apa.cid AND ah.ap_payee_account_id = apa.id )
					WHERE
						ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND ad.cid = ' . ( int ) $intCid . '
						AND ah.is_posted = true
						AND ah.is_on_hold = false
						AND ad.reversal_ap_detail_id IS NULL
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND ah.ap_payment_id IS NULL
						AND ad.transaction_amount_due <> 0
						AND ad.ap_header_id NOT IN (
														SELECT
															DISTINCT ad.ap_header_id
														FROM
															ap_details ad
															LEFT JOIN ( ' . CProperties::buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS vcup ON ( vcup.cid = ad.cid AND vcup.id = ad.property_id AND vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' )
														WHERE
															ad.cid = ' . ( int ) $intCid . '
															AND ( ( vcup.id IS NULL )
																	OR
																	( vcup.is_disabled = 1 AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL ) )
													)
						AND ad.deleted_by IS NULL
						AND ad.property_id  = ' . ( int ) $intPropertyId . '
						AND ap.ap_payee_status_type_id =' . CApPayeeStatusType::ACTIVE . '
						AND ah.ap_header_sub_type_id IN( ' . sqlIntImplode( $arrintInvoiceTypePermission ) . ' )
					ORDER BY
						' . $strOrderByCondition . ',
						array_position( ARRAY[' . ( int ) $intApPayeeAccountId . '], ah.ap_payee_account_id ),
						apa.account_number,
						ad.ap_header_id ASC,
						ad.id ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllApDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*,
						p.property_name,
						ah.header_number,
						ah.post_date,
						ah.due_date
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
						JOIN properties p ON ( p.cid = ad.cid AND p.id = ad.property_id )
					WHERE
						ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ad.cid = ' . ( int ) $intCid . '
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
					ORDER BY
						ad.property_id';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByApHeaderIdsByPropertyIdByCid( $arrintApHeaderIds, $intPropertyId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) || false == is_numeric( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
					WHERE
						ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ad.cid = ' . ( int ) $intCid . '
						AND ad.property_id = ' . ( int ) $intPropertyId . '
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
					ORDER BY
						ad.id ASC';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchTotalOutstandingCreditsPyPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						SUM( ad.transaction_amount_due ) AS total_outstanding_credits
					FROM
						ap_details ad
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.transaction_amount_due < 0
						AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ad.deleted_by IS NULL
						AND EXISTS (
								SELECT NULL FROM
									ap_headers ah 
									JOIN ap_payees ap ON ( ap.cid = ah.cid AND ap.id = ah.ap_payee_id AND ap.ap_payee_status_type_id = 1 )
								WHERE
									ah.id = ad.ap_header_id 
									AND ah.cid = ad.cid
									AND ah.is_posted 
									AND NOT ah.is_on_hold
									AND ah.reversal_ap_header_id IS NULL
									AND ah.ap_payment_id IS NULL
									AND NOT ah.is_template
							)';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		return ( true == isset( $arrmixData[0]['total_outstanding_credits'] ) ) ? $arrmixData[0]['total_outstanding_credits'] : NULL;
	}

	public static function fetchTotalOutstandingCreditsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = '
			DROP TABLE IF EXISTS pg_temp.load_properties_info;
			CREATE TEMP TABLE load_properties_info AS(
				SELECT lp.property_id FROM
					load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ], ARRAY [ ]::INT [ ], TRUE ) lp
			);
			ANALYZE pg_temp.load_properties_info;

			SELECT
				SUM( ad.transaction_amount_due ) AS total_outstanding_credits
			FROM
				ap_details AS ad
				JOIN pg_temp.load_properties_info AS lpi ON lpi.property_id = ad.property_id
				JOIN ap_headers AS ah ON ah.cid = ad.cid AND ah.id = ad.ap_header_id
				JOIN ap_payees AS ap ON ap.cid = ah.cid AND ap.id = ah.ap_payee_id AND ap.ap_payee_status_type_id = 1
			WHERE
				ad.cid = ' . ( int ) $intCid . '
				AND ad.transaction_amount_due < 0
				AND ad.deleted_by IS NULL
				AND NOT ah.is_on_hold
				AND ah.is_posted
				AND ah.reversal_ap_header_id IS NULL
				AND ah.ap_payment_id IS NULL';

		$arrmixData = fetchData( $strSql, $objClientDatabase );

		return ( true == isset( $arrmixData[0]['total_outstanding_credits'] ) ) ? $arrmixData[0]['total_outstanding_credits'] : NULL;
	}

	// This function is written for new dashboard

	public static function fetchUnpaidApDetailsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objClientDatabase, $objApTransactionsFilter = NULL, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$arrstrAndConditions = [];

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {

			if( true == valArr( $objApTransactionsFilter->getApHeadersIds() ) ) {
				$arrstrAndConditions[] = 'ad.ap_header_id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' )';
			}

			if( true == valArr( $objApTransactionsFilter->getApDetailsIds() ) ) {
				$arrstrAndConditions[] = 'ad.id IN ( ' . implode( ',', $objApTransactionsFilter->getApDetailsIds() ) . ' )';
			}

			$arrstrAndConditions[] = 'ad.is_approved_for_payment = false ';

			/**
			 * @Fixme - Is following condition is really needed?
			if( true == is_numeric( $objApTransactionsFilter->getIsCreditCardAccount() ) ) {
				$arrstrAndConditions[] = 'ba.is_credit_card_account = ' . ( int ) $objApTransactionsFilter->getIsCreditCardAccount();
			}
			 */
		}

		$strConditions = '';

		if( true == valArr( $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		$strSql = ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= 'SELECT
						COUNT ( ap_header_id ) ' .
						( false == $boolIsGroupByProperties ? ',
							MAX ( priority ) as priority'
						:
							' AS approvals_checks,
							property_id,
							property_name,
							priority' ) . '
					FROM (
						SELECT
							*
						FROM (
							SELECT
								ap_header_id,
								ad.cid,
								row_number() OVER ( PARTITION BY ad.ap_header_id, ad.bank_account_id ORDER BY ad.ap_header_id ),
								CASE
									WHEN TRIM ( dp.approvals_checks->>\'urgent_invoice_due_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.approvals_checks->>\'urgent_invoice_due_date_within\' ) )
										AND ( CURRENT_DATE - ( TRIM ( dp.approvals_checks->>\'urgent_invoice_due_date_within_days\' ) ::int ) ) >= ah.due_date::date THEN
										3
									WHEN TRIM ( dp.approvals_checks->>\'urgent_invoice_due_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.approvals_checks->>\'urgent_invoice_due_date_within\' ) )
										AND (
												( ah.due_date::date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.approvals_checks->>\'urgent_invoice_due_date_within_days\' ) ::int ) )
												OR ( ( \'past\' = TRIM( dp.approvals_checks->>\'important_invoice_due_date_within\' ) ) AND ( ah.due_date::date BETWEEN ( CURRENT_DATE - TRIM ( dp.approvals_checks->>\'urgent_invoice_due_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										3
									WHEN TRIM ( dp.approvals_checks->>\'urgent_invoice_amount_greater_than\' ) != \'\' AND ( SUM( sub.approved_amount_due ) OVER ( PARTITION BY ad.ap_header_id, ad.bank_account_id ) ) >= ( TRIM ( dp.approvals_checks->>\'urgent_invoice_amount_greater_than\' ) ::int ) THEN
										3
									WHEN TRIM ( dp.approvals_checks->>\'important_invoice_due_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.approvals_checks->>\'important_invoice_due_date_within\' ) )
										AND ( CURRENT_DATE - ( TRIM ( dp.approvals_checks->>\'important_invoice_due_date_within_days\' ) ::int ) ) >= ah.due_date::date THEN
										2
									WHEN TRIM ( dp.approvals_checks->>\'important_invoice_due_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.approvals_checks->>\'important_invoice_due_date_within\' ) )
										AND (
												( ah.due_date::date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.approvals_checks->>\'important_invoice_due_date_within_days\' ) ::int ) )
												OR ( ( \'past\' = TRIM( dp.approvals_checks->>\'urgent_invoice_due_date_within\' ) ) AND ( ah.due_date::date BETWEEN ( CURRENT_DATE - TRIM ( dp.approvals_checks->>\'important_invoice_due_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
											) THEN
										2
									WHEN TRIM ( dp.approvals_checks->>\'important_invoice_amount_greater_than\' ) != \'\' AND ( SUM( sub.approved_amount_due ) OVER ( PARTITION BY ad.ap_header_id, ad.bank_account_id ) ) >= ( TRIM ( dp.approvals_checks->>\'important_invoice_amount_greater_than\' ) ::int ) THEN
										2
									ELSE
										1
								END AS priority,
								ad.property_id,
								p.property_name
							FROM
								ap_details ad
								JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
								JOIN gl_account_trees glat ON ( ad.cid = glat.cid AND ad.gl_account_id = glat.gl_account_id )
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . '] ) lp ON ( lp.property_id = ad.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . ' )
								JOIN properties p ON ( p.id = lp.property_id AND p.cid = ad.cid )
								JOIN bank_accounts ba ON ( ad.bank_account_id = ba.id AND ad.cid = ba.cid )
								LEFT JOIN dashboard_priorities dp ON ( dp.cid = ad.cid )
								LEFT JOIN (
										SELECT
											ad.id,
											ad.cid,
											ad.pre_approval_amount - ABS( COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) ) AS approved_amount_due
										FROM
											ap_details ad
											LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id AND aa.is_deleted = false )
										WHERE
											ad.cid = ' . ( int ) $intCid . '
											AND ad.transaction_amount > 0
											AND ad.deleted_by IS NULL
											AND ad.deleted_on IS NULL
										GROUP BY
											ad.id,
											ad.cid
										UNION
											SELECT
												ad.id,
												ad.cid,
												ad.pre_approval_amount + ABS( COALESCE ( SUM( aa.allocation_amount ), 0::NUMERIC ) ) AS approved_amount_due
											FROM
												ap_details ad
												LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id AND aa.is_deleted = false )
											WHERE
												ad.cid = ' . ( int ) $intCid . '
												AND ad.transaction_amount <= 0
												AND ad.deleted_by IS NULL
												AND ad.deleted_on IS NULL
											GROUP BY
												ad.id,
												ad.cid
								) sub ON ( sub.cid = ad.cid AND sub.id = ad.id )
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND ad.reversal_ap_detail_id IS NULL
								AND ah.is_temporary = false
								AND ah.is_posted = true
								AND ah.ap_payment_id IS NULL
								AND ad.payment_approved_by IS NULL
								AND ad.payment_approved_on IS NULL
								--AND sub.approved_amount_due <> 0
								AND ad.is_approved_for_payment = false' .
									$strConditions . '
								--AND ( COALESCE( ad.pre_approval_amount, 0 ) - ( COALESCE( ad.transaction_amount, 0 ) - COALESCE( ad.transaction_amount_due, 0 ) ) <> 0 )
								AND COALESCE ( ad.pre_approval_amount, 0 )  < COALESCE ( ad.transaction_amount, 0 )
							) sub_query
							WHERE
								cid = ' . ( int ) $intCid . '
								AND row_number = 1
								' . $strPriorityWhere . ' ' .
							( false == $boolIsGroupByProperties ? '
							ORDER BY
								priority DESC
							LIMIT 100 '
							: '' ) . '
						) a_checks' .
						( true == $boolIsGroupByProperties ? '
							GROUP BY
								property_id,
								property_name,
								priority'
	 					: '' );

		if( true == $boolIsGroupByProperties ) {
			return fetchData( $strSql, $objClientDatabase );
		}

		$arrintAPDetailsCount = fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objClientDatabase );

		return $arrintAPDetailsCount[0];
	}

	public static function fetchViewApDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*,
						ah.ap_payee_id,
						ah.ap_payee_location_id,
						ah.lease_customer_id,
						ah.ap_batch_id,
						ah.ap_payment_id,
						ah.po_ap_header_ids,
						ah.post_date,
						ah.due_date,
						ah.header_number,
						ah.header_memo,
						ah.is_on_hold,
						ah.is_posted,
						ah.is_temporary,
						ah.is_batching,
						apl.location_name
					FROM
						ap_details ad
						JOIN ap_headers ah ON ad.cid = ah.cid AND ad.ap_header_id = ah.id
						LEFT JOIN ap_allocations aa ON aa.cid = ad.cid AND aa.charge_ap_detail_id = ad.id AND aa.is_deleted = false
						LEFT JOIN ap_payee_locations apl ON ah.ap_payee_location_id = apl.id AND ah.cid = apl.cid
					WHERE
						ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ad.cid = ' . ( int ) $intCid . '
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
					ORDER BY
						ad.id ASC';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchCountOfApprovedUnpaidApDetailsByApHeaderIdByCid( $intApHeaderId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApHeaderId ) ) {
			return NULL;
		}

		$strSql = 'WITH invoice_details AS ( SELECT
												ad.id,
												ad.cid,
												ad.ap_header_id,
												ad.gl_transaction_type_id,
												ad.post_month,
												ad.property_id,
												ad.pre_approval_amount,
												ad.transaction_amount,
												ad.transaction_amount_due,
												ad.pre_approval_amount - abs ( COALESCE ( sum ( aa.allocation_amount ), 0::numeric ) ) AS approved_amount_due
											FROM
												ap_details ad
												LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.is_deleted = false AND ad.reversal_ap_detail_id IS NULL AND ( ( ad.transaction_amount > 0 AND ad.id = aa.charge_ap_detail_id ) OR ( ad.transaction_amount <= 0 AND ad.id = aa.credit_ap_detail_id ) ) )
											WHERE
												ad.cid = ' . ( int ) $intCid . '
												AND ad.deleted_by IS NULL
												AND ad.deleted_on IS NULL
											GROUP BY
												ad.id,
												ad.cid,
												ad.ap_header_id,
												ad.property_id,
												ad.pre_approval_amount,
												ad.transaction_amount )

					SELECT
							COUNT( ad.id )
						FROM
							ap_details ad
							JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
							JOIN invoice_details id ON ( ah.cid = id.cid AND ah.id = id.ap_header_id AND ah.gl_transaction_type_id = id.gl_transaction_type_id AND ah.post_month = id.post_month )
						WHERE
							ad.cid = ' . ( int ) $intCid . '
							AND ad.ap_header_id = ' . ( int ) $intApHeaderId . '
							AND ad.is_approved_for_payment = false
							AND ad.payment_approved_by IS NOT NULL
							AND ad.payment_approved_on IS NOT NULL
							AND ah.ap_payment_id IS NULL
							AND ( COALESCE ( id.approved_amount, 0 ) - ( COALESCE ( id.transaction_amount, 0 ) - COALESCE ( id.transaction_amount_due, 0 ) ) <> 0 )';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchApDetailsForPurchaseOrdersByApHeaderIdByCid( $intApHeaderId, $intCid, $objClientDatabase, $boolIncludeDeletedUnits = false, $boolFetchPropertyNameWithLookupCode = false, $boolIsJcPo = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$strSelectPropertyName = 'p.property_name';

		if( true == $boolFetchPropertyNameWithLookupCode ) {
			$strSelectPropertyName = 'CASE
											WHEN p.lookup_code IS NULL THEN p.property_name
											ELSE p.lookup_code || \' - \' || p.property_name
										END AS property_name';
		}

		$strJcSelects        = '';
		$strJcJoinConditions = '';

		if( true == $boolIsJcPo ) {
			$strJcSelects = ',j.name AS job_name,
						j.id AS job_id,
						acc.name AS ap_contract_name,
						jp.name AS job_phase_name';

			$strJcJoinConditions = 'LEFT JOIN ap_contracts acc ON ( acc.cid = ad.cid AND acc.id = ad.ap_contract_id )
						LEFT JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
						LEFT JOIN jobs j ON ( j.cid = ad.cid AND j.id = jp.job_id )';
		}

		$strSql = 'WITH ap_codes_received AS (
						SELECT
							ad.id,
							ad.cid,
							ad.ap_code_id,
							CASE
								WHEN ( 1 < COUNT ( DISTINCT at.unit_cost ) ) THEN \'Multiple\'
								ELSE SUM ( DISTINCT at.unit_cost ) ::varchar
							END AS received_rate,
							SUM ( COALESCE ( at.display_quantity, 0 ) ) AS received_quantity,
							SUM ( at.unit_cost * COALESCE( at.display_quantity, 0 ) ) AS received_amount
						FROM
							ap_details ad
							JOIN ap_codes ac ON ( ad.cid = ac.cid AND ad.ap_code_id = ac.id )
							JOIN asset_transactions at ON ( ac.cid = at.cid AND ac.id = at.ap_code_id AND ad.id = at.po_ap_detail_id )
						WHERE
							ad.cid = ' . ( int ) $intCid . '
							AND ad.deleted_on IS NULL
							AND ad.ap_header_id = ' . ( int ) $intApHeaderId . '
							AND at.asset_transaction_type_id = ' . CAssetTransactionType::RECEIVE_INVENTORY . '
							OR  at.asset_transaction_type_id = ' . CAssetTransactionType::RETURN_INVENTORY . '
						GROUP BY
							ad.id,
							ad.cid,
							ad.ap_code_id
					),

					ap_codes_returned AS (

						SELECT
							at.cid,
							SUM ( at.display_quantity ) AS returned_display_quantity,
							SUM ( at.cached_remaining ) AS returned_quantity,
							SUM ( ( ( at.unit_cost ) * ( COALESCE ( at.cached_remaining, 0 ) ) ) ) AS returned_amount,
							at.po_ap_detail_id
						FROM
							asset_transactions at
							JOIN ap_details ad ON ( at.cid = ad.cid AND at.po_ap_detail_id = ad.id )
							JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						WHERE
							at.cid = ' . ( int ) $intCid . '
							AND ah.id = ' . ( int ) $intApHeaderId . '
							AND at.asset_transaction_type_id = ' . CAssetTransactionType::RETURN_INVENTORY . '
							AND at.asset_transaction_id IS NOT NULL
						GROUP BY
							at.cid,
							at.po_ap_detail_id
					)

					SELECT
						ad.*,
						' . $strSelectPropertyName . ',
						p.is_disabled AS is_disabled_property,
						ac.is_disabled AS is_disabled_item,
						gat.name AS gl_account_name,
						gat.formatted_account_number AS gl_account_number,
						pu.unit_number,
						cd.name AS department_name,
						ac.name AS item_name,
						acr.received_rate,
						( COALESCE( acr.received_quantity, 0 ) + COALESCE( act.returned_quantity, 0 ) ) AS received_quantity,
						( COALESCE( acr.received_amount, 0 ) + COALESCE( act.returned_amount, 0 ) ) AS received_amount,
						COALESCE( act.returned_quantity, 0 ) AS returned_quantity,
						( COALESCE( acr.received_quantity, 0 ) + ( COALESCE( act.returned_display_quantity, 0 ) * -1 ) ) AS quantity_received,
						uom.name AS unit_of_measure_name,
						uom.unit_of_measure_type_id,
						ac.unit_of_measure_id AS ap_code_unit_of_measure_id,
						cs.vendor_sku,
						gld.name AS gl_dimension_name,
						ah.ap_header_type_id,
						ac.asset_type_id
						' . $strJcSelects . '
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN gl_account_trees gat ON ( gat.cid = ad.cid AND gat.gl_account_id = ad.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN property_units pu ON ( ad.cid = pu.cid AND ad.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN ap_codes ac ON ( ad.cid = ac.cid AND ad.ap_code_id = ac.id )
						LEFT JOIN ap_codes_received acr ON ( ac.cid = acr.cid AND ac.id = acr.ap_code_id AND ad.id = acr.id )
						LEFT JOIN ap_codes_returned act ON ( ad.cid = act.cid AND ad.id = act.po_ap_detail_id )
						LEFT JOIN unit_of_measures uom ON ( ad.cid = uom.cid AND ad.unit_of_measure_id = uom.id )
						LEFT JOIN company_departments cd ON ( cd.cid = ad.cid AND cd.id = ad.company_department_id )
						LEFT JOIN ap_catalog_items cs ON (  cs.cid = ad.cid AND cs.ap_code_id = ac.id AND cs.ap_payee_id = ah.ap_payee_id AND ( ad.ap_catalog_item_id IS NULL OR cs.id = ad.ap_catalog_item_id ) )
						LEFT JOIN gl_dimensions gld ON ( gld.cid = ad.cid AND gld.id = ad.gl_dimension_id )
						' . $strJcJoinConditions . '
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.deleted_on IS NULL
						AND ad.ap_header_id = ' . ( int ) $intApHeaderId . '
					ORDER BY
						ad.id ASC';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase, $boolIncludeDeletedUnits = false ) {

		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ad.*,
						pu.unit_number
					FROM
						ap_details ad
						LEFT JOIN property_units pu ON ( ad.cid = pu.cid AND ad.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ad.deleted_on IS NULL';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByPoApHeaderIdsByCid( $arrintPoApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintPoApHeaderIds ) ) return NULL;

			$strSql = 'SELECT
						ad.*,
						po_ad.ap_header_id AS po_ap_header_id
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN ap_details po_ad ON ( ad.cid = po_ad.cid AND ad.po_ap_detail_id = po_ad.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ah.po_ap_header_ids IS NOT NULL 
						AND ARRAY[ ' . implode( ',', $arrintPoApHeaderIds ) . ']  && ah.po_ap_header_ids
						AND ad.reversal_ap_detail_id IS NULL
						AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL';

		return self::fetchApDetails( $strSql, $objClientDatabase, false );

	}

	public static function fetchApDetailsByApHeaderLogIdByCid( $intApHeaderLogId, $intCid, $objClientDatabase, $boolIncludeDeletedUnits = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						ad.*,
						adl.id as id, 
						p.property_name,
						pu.unit_number,
						ac.name AS item_name,
						at.display_quantity AS quantity_received,
						at.unit_cost,
						at.post_date AS received_date
					FROM
						ap_header_logs ahl
						JOIN ap_detail_logs adl ON( ahl.cid = adl.cid AND ahl.id = adl.ap_header_log_id )
						JOIN asset_transactions at ON ( adl.cid = at.cid AND adl.asset_transaction_id = at.id )
						JOIN ap_details ad ON( at.cid = ad.cid AND at.po_ap_detail_id = ad.id )
						JOIN properties p ON( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN ap_codes ac ON( ad.cid = ac.cid AND ad.ap_code_id = ac.id )
						LEFT JOIN property_units pu ON( ad.cid = pu.cid AND ad.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
					WHERE
						ahl.cid = ' . ( int ) $intCid . '
						AND ahl.id = ' . ( int ) $intApHeaderLogId . '
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeApDetailsByApPayeeIdApDetailIdsByCid( $intApPayeeId, $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) || false == valId( $intApPayeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					WHERE
						ad.cid				= ' . ( int ) $intCid . '
						AND ah.ap_payee_id	= ' . ( int ) $intApPayeeId . '
						AND ad.id 			= ANY( ARRAY[' . implode( ',', $arrintApDetailIds ) . ']::int[] )';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchRoutingTagApDetailsByRuleConditionIdByApDetailIdsByCid( $intRuleConditionId, $arrintApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApDetailIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN ap_headers ah ON( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
						JOIN rule_condition_details rcd ON ( ah.cid = rcd.cid AND ah.ap_routing_tag_id = rcd.reference_number )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND rcd.rule_condition_id = ' . ( int ) $intRuleConditionId . '
						AND ad.id = ANY( ARRAY[' . implode( ',', $arrintApDetailIds ) . ']::int[] )';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByCidBySearchKeywords( $intCid, $arrmixFastLookup, $arrstrFilteredExplodedSearch, $arrintPropertyIds, $objDatabase, $arrintInvoiceTypePermission, $objApTransactionsFilter = NULL ) {

		if( false == valArr( $arrintInvoiceTypePermission ) ) return NULL;

		if( false == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {
			$objApTransactionsFilter = new CApTransactionsFilter();
		}

		$arrstrInvoiceValues = [];
		$arrstrAdvancedSearchParameters	= [];
		$strLookUpType = $arrmixFastLookup['lookup_type'];

		if( false == is_null( $arrmixFastLookup['invoices_property_ids'] ) ) 				$arrstrInvoiceValues['invoice_property_ids'] 	= explode( ',', $arrmixFastLookup['invoices_property_ids'] );
		if( false == is_null( $arrmixFastLookup['post_date_from'] ) ) 						$arrstrInvoiceValues['post_date_from'] 			= $arrmixFastLookup['post_date_from'];
		if( false == is_null( $arrmixFastLookup['post_date_to'] ) ) 						$arrstrInvoiceValues['post_date_to'] 			= $arrmixFastLookup['post_date_to'];
		if( false == is_null( $arrmixFastLookup['due_date_from'] ) ) 						$arrstrInvoiceValues['due_date_from'] 			= $arrmixFastLookup['due_date_from'];
		if( false == is_null( $arrmixFastLookup['due_date_to'] ) ) 							$arrstrInvoiceValues['due_date_to'] 			= $arrmixFastLookup['due_date_to'];
		if( false == is_null( $arrmixFastLookup['header_number'] ) ) 						$arrstrInvoiceValues['header_number'] 			= $arrmixFastLookup['header_number'];
		if( false == is_null( $arrmixFastLookup['payee_name'] ) ) 							$arrstrInvoiceValues['payee_name'] 				= $arrmixFastLookup['payee_name'];
		if( false == is_null( $arrmixFastLookup['hide_on_hold_invoices'] ) ) 				$arrstrInvoiceValues['hide_on_hold_invoices'] 	= $arrmixFastLookup['hide_on_hold_invoices'];
		if( false == is_null( $arrmixFastLookup['transaction_amount']['min-amount'] ) ) 	$arrstrInvoiceValues['transaction_min_amount'] 	= $arrmixFastLookup['transaction_amount']['min-amount'];
		if( false == is_null( $arrmixFastLookup['transaction_amount']['max-amount'] ) ) 	$arrstrInvoiceValues['transaction_max_amount'] 	= $arrmixFastLookup['transaction_amount']['max-amount'];
		if( false == is_null( $arrmixFastLookup['bank_account_ids'] ) ) 					$arrstrInvoiceValues['bank_account_ids'] 		= explode( ',', $arrmixFastLookup['bank_account_ids'] );

		if( true == isset( $arrstrInvoiceValues['post_date_from'] ) ) 			$objApTransactionsFilter->setPostDateFrom( $arrstrInvoiceValues['post_date_from'] );
		if( true == isset( $arrstrInvoiceValues['post_date_to'] ) ) 			$objApTransactionsFilter->setPostDateTo( $arrstrInvoiceValues['post_date_to'] );
		if( true == isset( $arrstrInvoiceValues['due_date_from'] ) ) 			$objApTransactionsFilter->setDueDateFrom( $arrstrInvoiceValues['due_date_from'] );
		if( true == isset( $arrstrInvoiceValues['due_date_to'] ) ) 				$objApTransactionsFilter->setDueDateTo( $arrstrInvoiceValues['due_date_to'] );
		if( true == isset( $arrstrInvoiceValues['hide_on_hold_invoices'] ) ) 	$objApTransactionsFilter->setHideOnHoldInvoices( $arrstrInvoiceValues['hide_on_hold_invoices'] );
		if( true == isset( $arrstrInvoiceValues['transaction_min_amount'] ) ) 	$objApTransactionsFilter->setMinTransactionAmount( $arrstrInvoiceValues['transaction_min_amount'] );
		if( true == isset( $arrstrInvoiceValues['transaction_max_amount'] ) ) 	$objApTransactionsFilter->setMaxTransactionAmount( $arrstrInvoiceValues['transaction_max_amount'] );

		if( true == isset( $arrstrInvoiceValues['bank_account_ids'] ) && 'on' != trim( $arrstrInvoiceValues['bank_account_ids'][0] ) ) $objApTransactionsFilter->setBankAccountIds( $arrstrInvoiceValues['bank_account_ids'] );

		$strLookUpType = $arrmixFastLookup['lookup_type'];

		if( 'process_checks_cc_invoice_number' == $strLookUpType || 'process_checks_cc_vendor' == $strLookUpType ) {
			$objApTransactionsFilter->setIsCreditCardAccount( 1 );
		}

		// Fetch active and locked ap payees only
		$objApTransactionsFilter->setApPayeeStatusTypeIds( [ CApPayeeStatusType::ACTIVE, CApPayeeStatusType::LOCKED ] );

		$strOrderBy = 'ah.header_number';

		$strSql	= 'SELECT
						ad.id,
						ad.ap_header_id,
						ad.transaction_amount,
						( ad.pre_approval_amount - ( ad.transaction_amount - ad.transaction_amount_due ) ) AS amount_due,
						ad.pre_approval_amount AS approved_amount,
						ad.description,
						ad.gl_account_id,
						ad.bank_account_id,
						func_format_refund_customer_names( ah.header_memo ) AS header_memo,
						ah.header_number,
						ah.due_date,
						ah.is_on_hold,
						apl.vendor_code,
						apl.location_name,
						ba.account_name,
						p.id AS property_id,
						p.property_name,
						gat.name AS gl_account_name,
						gat.formatted_account_number AS gl_account_number,
						( CASE
							WHEN( ah.lease_customer_id IS NULL )
							THEN ( COALESCE( ap.company_name, \'\' ) || \' \' || COALESCE( apl.location_name, \'\' ) )
							ELSE func_format_refund_customer_names( ah.header_memo )
						END ) AS ap_payee_name,
						ap.ap_payee_status_type_id,
						SUM(( ad.pre_approval_amount - ( ad.transaction_amount - ad.transaction_amount_due ) ) ) OVER(PARTITION BY ad.ap_header_id, ad.bank_account_id ) AS total_amount_due,
						( CASE
							WHEN( ah.lease_customer_id IS NULL)
							THEN \'vendor_\' || ah.ap_payee_id
							ELSE \'cust_\' || ah.lease_customer_id
						END ) AS ap_payee_key,
						apl.phone_number,
						--apc.email_address,
						apl.street_line1,
						apl.street_line2,
						apl.city,
						apl.state_code,
						apl.postal_code,
						ap.company_name
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN gl_trees gt ON ( gt.cid = ah.cid AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
						JOIN gl_account_trees gat ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id AND gat.is_default = 1 )
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND ga.id = ad.gl_account_id AND ga.cid = ad.cid )
						JOIN properties p ON ( p.id = ad.property_id AND p.cid = ad.cid )
						JOIN bank_accounts ba ON( ad.bank_account_id = ba.id AND ad.cid = ba.cid )
						LEFT JOIN ap_payees ap ON ( ap.cid = ad.cid AND ap.id = ah.ap_payee_id )
						LEFT JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ah.ap_payee_location_id = apl.id )
						LEFT JOIN ap_allocations aa ON ( ad.cid = aa.cid AND ( ad.id = aa.charge_ap_detail_id OR ad.id = aa.credit_ap_detail_id ) AND aa.is_deleted = false )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id NOT IN (
							SELECT
								DISTINCT ad.ap_header_id
							FROM
								ap_details ad
								JOIN properties p ON ( ad.property_id = p.id AND ad.cid = p.cid )
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND p.is_disabled = 1
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL
							)
						AND ah.is_temporary IS false
						AND ah.is_posted IS true
						AND ah.ap_payment_id IS NULL
						AND ad.reversal_ap_detail_id IS NULL
						AND ad.payment_approved_by IS NOT NULL
						AND ad.payment_approved_on IS NOT NULL
						AND ad.is_approved_for_payment ';

		if( true == isset( $arrstrInvoiceValues['invoice_property_ids'] ) && true == valIntArr( $arrstrInvoiceValues['invoice_property_ids'] ) && 'on' != trim( $arrstrInvoiceValues['invoice_property_ids'][0] ) ) {
			$arrintFilterPropertyIds = $arrstrInvoiceValues['invoice_property_ids'];
			$strSql	.= ' AND ad.property_id IN ( ' . implode( ',', $arrintFilterPropertyIds ) . ' )';

		} else {
			$arrintFilterPropertyIds = $arrintPropertyIds;
			$strSql	.= ' AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		if( true == valStr( $objApTransactionsFilter->getPostDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateFrom() ) ) {
			$strSql	.= ' AND ah.post_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateFrom() ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPostDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateTo() ) ) {
			$strSql	.= ' AND ah.post_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateTo() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objApTransactionsFilter->getDueDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDateFrom() ) ) {
			$strSql	.= ' AND ah.due_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDateFrom() ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $objApTransactionsFilter->getDueDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDateTo() ) ) {
			$strSql	.= ' AND ah.due_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDateTo() ) ) . ' 23:59:59\'';
		}

		if( true == valIntArr( $objApTransactionsFilter->getBankAccountIds() ) ) {
			$strSql	.= ' AND ba.id IN ( ' . implode( ',', $objApTransactionsFilter->getBankAccountIds() ) . ' )';
		}

		if( true == valArr( $objApTransactionsFilter->getApPayeeStatusTypeIds() ) ) {
			$strSql	.= ' AND ( ap.ap_payee_status_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeStatusTypeIds() ) . ' ) OR ah.lease_customer_id IS NOT NULL )';
		}

		if( true == valStr( $objApTransactionsFilter->getMinTransactionAmount() ) ) {
			$strSql	.= ' AND COALESCE( ah.transaction_amount_due, 0 ) >= ' . ( float ) $objApTransactionsFilter->getMinTransactionAmount();
		}

		if( true == valStr( $objApTransactionsFilter->getMaxTransactionAmount() ) ) {
			$strSql	.= ' AND COALESCE( ah.transaction_amount_due, 0 ) <= ' . ( float ) $objApTransactionsFilter->getMaxTransactionAmount();
		}

		if( true == is_numeric( $objApTransactionsFilter->getHideOnHoldInvoices() ) && 0 == $objApTransactionsFilter->getHideOnHoldInvoices() ) {
			$strSql	.= ' AND ah.is_on_hold = ' . ( int ) $objApTransactionsFilter->getHideOnHoldInvoices();
			$strSql	.= ' AND ( ap.ap_payee_status_type_id IS NULL OR ap.ap_payee_status_type_id <>' . CApPayeeStatusType::LOCKED . ')';
		}

		if( true == is_numeric( $objApTransactionsFilter->getIsCreditCardAccount() ) ) {
			$strSql	.= ' AND ba.is_credit_card_account = ' . ( int ) $objApTransactionsFilter->getIsCreditCardAccount();
		}

		if( CApDetail::LOOKUP_TYPE_PROCESS_CHECKS_INVOICE_NUMBER == $strLookUpType || CApDetail::LOOKUP_TYPE_PROCESS_CHECKS_CC_INVOICE_NUMBER == $strLookUpType ) {

			if( true == isset( $arrstrInvoiceValues['payee_name'] ) && false == empty( $arrstrInvoiceValues['payee_name'] ) ) {
				$strSql .= ' AND ( ( ap.company_name ILIKE \'%' . addslashes( trim( $arrstrInvoiceValues['payee_name'] ) ) . '%\' AND ap.ap_payee_type_id <> ' . CApPayeeType::PROPERTY_SOLUTIONS . ' )';
				$strSql .= ' OR ah.header_memo	ILIKE \'%' . addslashes( trim( $arrstrInvoiceValues['payee_name'] ) ) . '%\' )';
			}

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ah.header_number ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}
		} elseif( CApDetail::LOOKUP_TYPE_PROCESS_CHECKS_VENDOR == $strLookUpType || CApDetail::LOOKUP_TYPE_PROCESS_CHECKS_CC_VENDOR == $strLookUpType ) {

			if( true == isset( $arrstrInvoiceValues['header_number'] ) && false == empty( $arrstrInvoiceValues['header_number'] ) ) {
				$strSql .= ' AND ah.header_number  ILIKE \'%' . addslashes( trim( $arrstrInvoiceValues['header_number'] ) ) . '%\' ';
			}

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, ' ( ap.company_name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\' AND ap.ap_payee_type_id <> ' . CApPayeeType::PROPERTY_SOLUTIONS . ' )' );
					array_push( $arrstrAdvancedSearchParameters, 'ah.header_memo   ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'apl.vendor_code ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			$strOrderBy = 'ap_payee_name';
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		$strSql .= ' AND ah.ap_header_sub_type_id IN( ' . sqlIntImplode( $arrintInvoiceTypePermission ) . ' )';

		$strSql	.= 'GROUP BY
							ad.id,
							ad.ap_header_id,
							ad.transaction_amount,
							ad.transaction_amount_due,
							ad.pre_approval_amount,
							ad.description,
							ad.gl_account_id,
							ad.bank_account_id,
							ah.lease_customer_id,
							ah.ap_payee_id,
							ap.company_name,
							ah.header_memo,
							ah.header_number,
							ah.due_date,
							ah.is_on_hold,
							apl.vendor_code,
							apl.location_name,
							ba.account_name,
							p.id,
							p.property_name,
							gat.name,
							gat.formatted_account_number,
							ap.ap_payee_status_type_id,
							apl.phone_number,
							--apc.email_address,
							apl.street_line1,
							apl.street_line2,
							apl.city,
							apl.state_code,
							apl.postal_code,
							ap.company_name
					HAVING
						( ABS( SUM ( COALESCE ( aa.allocation_amount, 0 ) ) ) < ABS( COALESCE ( ad.transaction_amount, 0 ) ) )
						AND ( COALESCE( ad.pre_approval_amount, 0 ) - ( COALESCE( ad.transaction_amount, 0 ) - COALESCE( ad.transaction_amount_due, 0 ) ) <> 0 )
					ORDER BY ' . $strOrderBy;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnclaimedPropertyDetailsByApPaymentIdsByCid( $arrintApPaymentIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPaymentIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						(
						SELECT
							row_number() over ( partition BY ap.id, ad.property_id ORDER BY ah.updated_on DESC ) rank,
							SUM( ad.rate ) OVER ( partition BY ah.id, ad.property_id ORDER BY ah.updated_on DESC ) rate,
							SUM( ad.subtotal_amount ) OVER ( partition BY ah.id, ad.property_id ORDER BY ah.updated_on DESC ) subtotal_amount,
							SUM( ad.transaction_amount ) OVER ( partition BY ah.id, ad.property_id ORDER BY ah.updated_on DESC ) transaction_amount,
							ad.property_unit_id,
							ad.property_building_id,
							ad.property_id,
							pgs.unclaimed_property_ap_payee_id,
							pgs.unclaimed_property_gl_account_id,
							ap.id AS ap_payment_id,
							p.property_name,
							glat.name AS gl_account_name,
							glat.formatted_account_number AS gl_account_number,
							app.company_name,
							ap.payment_number,
							ap.bank_account_id
						FROM
							ap_details ad
							JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
							JOIN ap_payees app ON ( app.cid = ah.cid AND app.id = ah.ap_payee_id )
							JOIN property_gl_settings pgs ON ( ad.cid = pgs.cid AND ad.property_id = pgs.property_id )
							JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
							JOIN ap_payments ap ON ( ah.cid = ap.cid AND ah.ap_payment_id = ap.id )
							LEFT JOIN gl_account_trees glat ON ( pgs.cid = glat.cid AND pgs.unclaimed_property_gl_account_id = glat.gl_account_id AND glat.is_default = 1 )
						WHERE
							ad.cid = ' . ( int ) $intCid . '
							AND ap.id IN ( ' . implode( ',', $arrintApPaymentIds ) . ' )
							AND ah.ap_header_sub_type_id <> ' . CApHeaderSubType::UNCLAIMED_PROPERTY_PAYMENT . '
							AND ad.transaction_amount > 0
						) sub_query
					WHERE
						rank = 1';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchJobBudgetApDetailsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {
		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' DROP TABLE IF EXISTS pg_temp.actual_amounts;
					CREATE TEMP TABLE actual_amounts AS (
					SELECT DISTINCT
					     sub.ap_code_id,
						 sub.cid,
					     sub.job_phase_id,
					     sub.name AS job_phase_name,
					     sub.post_month,
					     sum(sub.actual_amount) OVER(PARTITION BY sub.job_phase_id, sub.ap_code_id, (CASE
					                                                                                   WHEN sub.is_budget_by_period = TRUE THEN sub.post_month
					                                                                                 END)) AS actual_amount
					FROM(
					       SELECT DISTINCT
					            ad.ap_code_id,
					            jp.cid,
					            jp.id AS job_phase_id,
					            jp.name,
					            ad.post_month,
					            jp.is_budget_by_period,
					            SUM(ad.transaction_amount) OVER(PARTITION BY jp.cid, jp.id, ad.post_month, ad.ap_code_id) AS actual_amount
					       FROM
					           job_phases jp
					           JOIN ap_details ad ON (ad.cid = jp.cid AND ad.job_phase_id = jp.id)
					           JOIN ap_headers ah ON (ah.cid = ad.cid AND ah.id = ad.ap_header_id)
					       WHERE
					           ad.cid = ' . ( int ) $intCid . '
		                       AND jp.job_id = ' . ( int ) $intJobId . '
		                       AND ah.ap_header_sub_type_id IN (' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' , ' . CApHeaderSubType::CATALOG_JOB_INVOICE . ' )
		                       AND ah.is_posted IS TRUE
		                       AND ad.reversal_ap_detail_id IS NULL
		                       AND ah.reversal_ap_header_id IS NULL
		                       AND ad.deleted_on IS NULL
					       UNION
					       SELECT DISTINCT
					            gd.ap_code_id,
					            jp.cid,
					            jp.id AS job_phase_id,
					            jp.name,
					            gd.post_month,
					            jp.is_budget_by_period,
					            SUM(gd.amount) OVER(PARTITION BY jp.cid, jp.id, gd.post_month, gd.ap_code_id) AS actual_amount
					       FROM
					           job_phases jp
					           JOIN gl_details gd ON (jp.cid = gd.cid AND jp.id = gd.job_phase_id AND gd.reclass_gl_detail_id IS NULL)
					           JOIN gl_headers gh ON (gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.reclass_gl_header_id IS NULL AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' AND gh.gl_header_status_type_id = ' . CGlHeaderStatusType::POSTED . ' )
					       WHERE
					           jp.cid = ' . ( int ) $intCid . '
		                       AND jp.job_id = ' . ( int ) $intJobId . '
		                       AND gh.is_template IS FALSE
					     ) AS sub
					);
					ANALYZE actual_amounts(cid, job_phase_id, ap_code_id);
					SELECT
					     sum( ad.transaction_amount ) AS total_budget,
						 sum( aa.actual_amount) AS actual_amount,
					     to_char(ad.post_month, \'MM/YYYY\') AS post_month,
					     jp.id AS job_phase_id,
					     jp.name AS job_phase_name,
					     jp.is_budget_by_period,
					     ac.id AS job_cost_code_id,
					     ac.name AS job_cost_code,
					     ah.ap_header_sub_type_id,
					     ah.approved_on
					FROM
					    job_phases jp
					    JOIN ap_details ad ON (ad.cid = jp.cid AND ad.job_phase_id = jp.id)
					    JOIN ap_codes ac ON (ac.cid = ad.cid AND ac.id = ad.ap_code_id)
					    JOIN ap_headers ah ON (ah.cid = ad.cid AND ah.id = ad.ap_header_id)
					    LEFT JOIN actual_amounts aa ON( aa.cid = ad.cid AND aa.ap_code_id = ad.ap_code_id AND aa.job_phase_id = ad.job_phase_id AND aa.post_month = ad.post_month )
					WHERE
					    jp.cid = ' . ( int ) $intCid . '
					    AND jp.job_id = ' . ( int ) $intJobId . '
					    AND ac.ap_code_type_id = ' . CApCode::JOB_COST . '
					    AND ( ( ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' , ' . CApHeaderSubType::JOB_CHANGE_ORDER . ' ) AND ah.approved_on IS NOT NULL ) OR ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) )
					    AND ah.deleted_on IS NULL
					    AND ad.deleted_on IS NULL
					    AND ad.unit_type_id IS NULL
					    AND ah.ap_physical_status_type_id IS NULL
					    AND ad.maintenance_location_id IS NULL
					GROUP BY
					       jp.id,
					       jp.cid,
					       ac.id,
					       ac.cid,
					       ah.ap_header_sub_type_id,
					       ah.approved_on,
					       ad.post_month
					ORDER BY
					       ac.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSummaryApDetailsByJobPhaseIdByApCodeIdByCid( $intJobPhaseId, $intApCodeId, $intCid, $objDatabase ) {
		if( false == valId( $intJobPhaseId ) || false == valId( $intApCodeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN jobs j ON ( j.cid = ah.cid AND j.budget_summary_ap_header_id = ah.id )
						JOIN job_phases jp ON ( jp.cid = j.cid AND jp.job_id = j.id )
					WHERE
						jp.id = ' . ( int ) $intJobPhaseId . '
						AND jp.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_BUDGET_SUMMARY . '
						AND ad.deleted_on IS NULL
						AND ad.ap_code_id = ' . ( int ) $intApCodeId;

		return self::fetchApDetail( $strSql, $objDatabase );
	}

	public static function fetchJobBudgetApDetailByJobPhaseIdByApCodeIdByPostMonthByCid( $intJobPhaseId, $intApCodeId, $strPostMonth, $intCid, $objDatabase ) {
		if( false == valId( $intJobPhaseId ) || false == valId( $intApCodeId ) || false == valStr( $strPostMonth ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.job_phase_id = ' . ( int ) $intJobPhaseId . '
						AND ad.ap_code_id = ' . ( int ) $intApCodeId . '
						AND ad.post_month = \'' . $strPostMonth . '\'
						AND ad.deleted_on IS NULL
						AND ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_PHASE_BUDGET . ',' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ',' . CApHeaderSubType::JOB_CHANGE_ORDER . ' )
					LIMIT 1';

		return self::fetchApDetail( $strSql, $objDatabase );
	}

	public static function fetchApDetailsByPropertyIdsByApHeaderTypeIdByPostMonthByCid( $arrintPropertyIds, $intApHeaderTypeId, $strPostMonth, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ad.property_id,
						ad.gl_account_id,
						SUM( ad.transaction_amount ) AS transaction_amount
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.property_id IN(' . implode( ',', $arrintPropertyIds ) . ')
						AND ah.ap_header_type_id = ' . ( int ) $intApHeaderTypeId . '
						AND ah.ap_financial_status_type_id = ' . CApFinancialStatusType::APPROVED . '
						AND to_char(ah.post_month, \'YYYY-MM\') = \'' . date( 'Y-m', strtotime( $strPostMonth ) ) . '\'
						AND ad.deleted_by IS NULL
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ah.approved_by IS NOT NULL
						AND ah.approved_on IS NOT NULL
					GROUP BY
						ad.property_id,
						ad.gl_account_id';

		return ( array ) fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApContractBudgetApDetailsByApContractIdsByCid( $arrintApContractIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApContractIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ad.id AS ap_detail_id,
						ad.job_phase_id,
						jp.order_num AS job_phase_order_num,
						jp.original_start_date,
						jp.original_end_date,
						jp.is_budget_by_period,
						ad.ap_contract_id,
						ad.ap_code_id,
						ac.name AS ap_code_name,
						ad.transaction_amount
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN ap_codes ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_code_id )
						JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_contract_id IN ( ' . implode( ',', $arrintApContractIds ) . ' )
						AND ad.is_deleted = FALSE
						AND ad.deleted_by IS NULL
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::CONTRACT_BUDGET . '
						AND ah.is_deleted = FALSE
						AND ah.is_disabled = FALSE';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApDetailsByJobIdByBudgetAdjustmentIdByCid( $intJobId, $intBudgetAdjustmentId, $intCid, $objDatabase ) {

		if( false == valId( $intJobId ) || false == valId( $intBudgetAdjustmentId ) ) return NULL;

		$strSql = 'SELECT
						ah.id AS ap_header_id,
						ah.ap_physical_status_type_id,
						ah.ap_financial_status_type_id,
						ah.approved_on,
						ah.transaction_datetime,
						ah.header_number,
						ah.post_date AS post_date,
						ba.id AS adjustment_id,
						ba.description AS adjustment_description,
						ad.post_month,
						ad.transaction_amount AS adjustment_amount,
						CASE WHEN ( ad.transaction_amount > 0 ) THEN 1 ELSE 0 END AS  is_fund_to,
						ac.id AS job_cost_code_id,
						ac.name AS job_cost_code_name,
						jp.id AS job_phase_id,
						jp.name AS job_phase_name,
						jp.is_budget_by_period,
						cu.username AS created_by_username,
						cu1.username AS approved_by_username,
						COALESCE( ce.name_first, \'\' ) || \' \' || COALESCE( ce.name_last, \'\' ) AS created_by,
						COALESCE( ce1.name_first, \'\' ) || \' \' || COALESCE( ce1.name_last, \'\' ) AS approved_by
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN budget_adjustments ba ON ( ba.cid = ah.cid AND ( ba.from_ap_header_id = ah.id OR ba.to_ap_header_id = ah.id ) )
						JOIN job_phases jp ON ( jp.cid = ah.cid AND jp.id = ah.job_phase_id )
						JOIN jobs j ON ( j.cid = jp.cid AND j.id = jp.job_id )
						LEFT JOIN ap_codes ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_code_id )
						LEFT JOIN company_users cu ON ( cu.cid = ah.cid AND cu.id = ah.created_by  AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_users cu1 ON ( cu1.cid = ah.cid AND cu1.id = ah.approved_by AND cu1.company_user_type_id IN ( ' . implode( ' ,', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.default_company_user_id IS NULL )
						LEFT JOIN company_employees ce1 ON ( ce1.id = cu1.company_employee_id AND ce1.cid = cu1.cid AND cu1.default_company_user_id IS NULL )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ba.id = ' . ( int ) $intBudgetAdjustmentId . '
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . '
						AND j.id =  ' . ( int ) $intJobId . '
						AND ah.deleted_on IS NULL
						AND ad.deleted_on IS NULL
					ORDER BY
						ah.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApDetailsByAdjustmentFilterByCid( $objAdjustmentFilter, $intCid, $objDatabase ) {

		if( false == valObj( $objAdjustmentFilter, 'CJobCostingFilter' ) || false == valId( $objAdjustmentFilter->getJobId() ) || false == valId( $intCid ) ) return NULL;

		$strCondition	= '';

		if( true == valStr( $objAdjustmentFilter->getFromDate() ) ) $strCondition .= ' AND ah.created_on >= \'' . $objAdjustmentFilter->getFromDate() . '\'::TIMESTAMP';

		if( true == valStr( $objAdjustmentFilter->getToDate() ) ) $strCondition .= ' AND ah.created_on <= \'' . $objAdjustmentFilter->getToDate() . ' 23:59:59\'::TIMESTAMP';

		if( true == valId( $objAdjustmentFilter->getJobCostCodeId() ) ) $strCondition .= ' AND ac.id = ' . ( int ) $objAdjustmentFilter->getJobCostCodeId();

		if( true == valArr( $objAdjustmentFilter->getAdjustmentStatuses() ) && false == in_array( CJobCostingFilter::ALL, $objAdjustmentFilter->getAdjustmentStatuses() ) ) {
			$strCondition .= ' AND ah.ap_financial_status_type_id IN ( ' . sqlIntImplode( getIntValuesFromArr( $objAdjustmentFilter->getAdjustmentStatuses() ) ) . ' )';
		}

		$strSql = 'SELECT
						ah.id AS ap_header_id,
						CASE
							WHEN ' . CApFinancialStatusType::APPROVED . ' = ah.ap_financial_status_type_id THEN \'Approved\'
							WHEN ' . CApFinancialStatusType::CANCELLED . ' = ah.ap_financial_status_type_id THEN \'Cancelled\'
							ELSE \'Pending\'
						END AS adjustment_status,
						ba.id AS adjustment_id,
						ba.description,
						ah.transaction_datetime,
						ah.approved_by AS approved_by_company_user_id,
						ah.post_date AS post_date,
						ad.post_month,
						ad.transaction_amount AS adjustment_amount,
						ac.id AS job_cost_code_id,
						ac.name AS job_cost_code_name,
						jp.id AS job_phase_id,
						jp.name AS job_phase_name,
						jp.is_budget_by_period,
						COALESCE( ce.name_first, \'\' ) || \' \' || COALESCE( ce.name_last, \'\' ) AS created_by,
						COALESCE( ce1.name_first, \'\' ) || \' \' || COALESCE( ce1.name_last, \'\' ) AS approved_by
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN budget_adjustments ba ON ( ba.cid = ah.cid AND ( ba.from_ap_header_id = ah.id OR ba.to_ap_header_id = ah.id ) )
						JOIN job_phases jp ON ( jp.cid = ah.cid AND jp.id = ah.job_phase_id )
						LEFT JOIN ap_codes ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_code_id )
						LEFT JOIN company_users cu ON ( cu.cid = ah.cid AND cu.id = ah.created_by  AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_users cu1 ON ( cu1.cid = ah.cid AND cu1.id = ah.approved_by AND cu1.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.default_company_user_id IS NULL )
						LEFT JOIN company_employees ce1 ON ( ce1.id = cu1.company_employee_id AND ce1.cid = cu1.cid AND cu1.default_company_user_id IS NULL )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . '
						AND jp.job_id =  ' . ( int ) $objAdjustmentFilter->getJobId() . '
						AND ah.deleted_on IS NULL
						AND ad.deleted_on IS NULL
						' . $strCondition . '
					ORDER BY
						ah.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSyncedApDetailsByOrderDetailIdsByCid( $arrintOrderDetailIds, $intCid, $objDatabase ) {

		if( false == ( $arrintOrderDetailIds = getIntValuesFromArr( $arrintOrderDetailIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM 
						ap_details
					WHERE 
						cid = ' . ( int ) $intCid . ' 
						AND order_detail_id IN (' . sqlIntImplode( $arrintOrderDetailIds ) . ')';

		return self::fetchApDetails( $strSql, $objDatabase );
	}

	public static function fetchTotalTransactionAmountDueByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						SUM( transaction_amount_due ) AS transaction_amount_due,
						property_id
					FROM
						ap_details
					WHERE
						ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
					GROUP
						BY property_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSummaryApDetailsByJobIdsByApCodeIdsByCid( $arrintJobId, $arrintApCodeIds, $intCid, $objDatabase ) {
		if( false == valIntArr( $arrintJobId ) || false == valIntArr( $arrintApCodeIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ad.*,
						j.id as job_id
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN jobs j ON ( j.cid = ah.cid AND j.budget_summary_ap_header_id = ah.id )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.id IN ( ' . sqlIntImplode( $arrintJobId ) . ' )
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_BUDGET_SUMMARY . '
						AND ad.ap_code_id IN ( ' . sqlIntImplode( $arrintApCodeIds ) . ' )
						AND ad.deleted_on IS NULL';

		return self::fetchApDetails( $strSql, $objDatabase );
	}

	public static function fetchApDetailsByApHeaderIdsByJobIdByCid( $arrintApHeaderIds, $intJobId, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApHeaderIds ) || false == valId( $intJobId ) ) return NULL;

		$strSql = 'SELECT
						ad.* 
					FROM
						ap_details ad 
						JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id ) 
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND jp.job_id = ' . ( int ) $intJobId . '
						AND ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ad.deleted_by IS NULL';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsCountByJobIdByPropertyIdByCid( $intJobId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intJobId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT( DISTINCT( ad.id ) )
					FROM
						ap_details ad 
						JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id ) 
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND jp.job_id = ' . ( int ) $intJobId . '
						AND ad.property_id = ' . ( int ) $intPropertyId . '
						AND ad.deleted_by IS NULL';

		return self::fetchColumn( $strSql, 'count', $objDatabase );

	}

	public static function fetchUnlinkedApDetailsWithPoDetailIdByApHeaderIdByCid( $intApHeaderId, $intCid, $objDatabase ) {

		if( false == valId( $intApHeaderId ) ) return NULL;

		$strSql = ' SELECT
						ad.*,
						ac.name as item_name,
						glat.name AS gl_account_name,
						glat.formatted_account_number AS gl_account_number
					FROM
						ap_details ad
						LEFT JOIN ap_codes ac ON ( ad.cid = ac.cid AND ad.ap_code_id = ac.id )
						LEFT JOIN gl_account_trees glat ON ( ad.cid = glat.cid AND ad.gl_account_id = glat.gl_account_id AND glat.is_default = 1 )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id = ' . ( int ) $intApHeaderId . '
						AND ad.po_ap_detail_id IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
					ORDER BY
						ad.id ASC';

		return self::fetchApDetails( $strSql, $objDatabase );
	}

	public static function fetchInvoiceApDetailsByApPaymentIdsByCid( $arrintApPaymentIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ad_invoices.*,
						ah_invoices.header_number,
						ap.id AS ap_payment_id,
						p.property_name
					FROM
						(
							SELECT
								id,
								CID,
								ap_payment_id,
								STRING_TO_ARRAY ( STRING_AGG ( header_number, \',\' ), \',\' )::INTEGER [ ] AS invoice_ap_header_ids
							FROM
								ap_headers
							WHERE
								cid = ' . ( int ) $intCid . '
								AND ap_payment_id IN ( ' . implode( ', ', $arrintApPaymentIds ) . ' )
							GROUP BY
								id,
								cid,
								ap_payment_id
						) ah_payments
						JOIN ap_details ad_payments ON ( ad_payments.cid = ah_payments.cid AND ad_payments.ap_header_id = ah_payments.id )
						JOIN ap_headers ah_invoices ON ( ah_invoices.cid = ah_payments.cid AND ah_invoices.id = ANY ( ah_payments.invoice_ap_header_ids ) )
						JOIN ap_details ad_invoices ON ( ad_invoices.cid = ah_invoices.cid AND ad_invoices.ap_header_id = ah_invoices.id )
						JOIN ap_allocations aa ON ( aa.cid = ad_payments.cid AND ( aa.credit_ap_detail_id = ad_payments.id OR aa.charge_ap_detail_id = ad_invoices.id ) )
						JOIN ap_payments ap ON ( ap.cid = ah_payments.cid AND ap.id = ah_payments.ap_payment_id )
						JOIN properties p ON ( p.cid = ad_invoices.cid AND p.id = ad_invoices.property_id )
					WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND ap.id IN ( ' . implode( ', ', $arrintApPaymentIds ) . ' )';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByJobIdByCid( $intJobId, $intCid, $objClientDatabase ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) NULL;

			$strSql = 'SELECT 
							ad.*
						FROM jobs j
						     JOIN job_phases jp ON ( j.cid = jp.cid AND j.id = jp.job_id )
						     JOIN ap_headers ah ON ( jp.cid = ah.cid AND jp.id = ah.job_phase_id )
						     JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						WHERE 
							j.cid =  ' . ( int ) $intCid . ' 
							AND j.id = ' . ( int ) $intJobId . ' 
							AND ad.deleted_by IS NULL
							AND ah.deleted_by IS NULL
							AND j.job_status_id <> ' . CJobStatus::CANCELLED . '
							AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . '
							AND ad.transaction_amount = 0';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetDetailsByJobIdByCid( $intJobId, $intCid, $objDatabase, $boolIsMaintenanceLocationBudget = false ) {

		if( false == valId( $intJobId ) ) return NULL;

		$strCondition = ( true == $boolIsMaintenanceLocationBudget ) ? ' AND ad.maintenance_location_id IS NOT NULL' : ' AND ad.unit_type_id IS NOT NULL';

		$strSql = 'SELECT
						ad.unit_type_id,
						ad.maintenance_location_id,
						ad.ap_code_id,
						ad.job_phase_id,
						ad.post_month,
						ad.transaction_amount,
						ac.name AS ap_code_name,
						jp.name AS job_phase_name,
						ah.ap_header_sub_type_id
					FROM
						ap_details ad
						JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id AND jp.job_id = ' . ( int ) $intJobId . ' )
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' )
						JOIN ap_codes ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_code_id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.deleted_by IS NULL
						AND ah.deleted_by IS NULL' . $strCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchBudgetDetailsByJobIdByByMaintenanceLocationIdByCid( $intJobId, $intMaintenanceLocationId, $intCid, $objDatabase ) {
		if( false == valId( $intJobId ) || false == valId( $intMaintenanceLocationId ) ) return NULL;

		$strSql = 'SELECT
						ad.ap_code_id,
						ad.job_phase_id,
						ad.post_month, 
						ad.transaction_amount,
						ac.name AS ap_code_name
					FROM
						ap_details ad
						JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id AND jp.job_id = ' . ( int ) $intJobId . ' )
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' )
						JOIN ap_codes ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_code_id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.deleted_by IS NULL
						AND ah.deleted_by IS NULL
						AND ad.maintenance_location_id = ' . ( int ) $intMaintenanceLocationId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomizeApDetailsByApHeaderIdsByPropertyIdByCid( $arrintApHeaderIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) return NULL;

		$strCondition = ( true == valArr( $arrintApHeaderIds ) ) ? ' AND ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' ) ' : ' ';

		$strSql = 'SELECT 
						ad.*
					FROM 
						ap_details ad
						JOIN ap_headers ah ON (ad.ap_header_id = ah.id AND ad.cid = ah.cid)
					WHERE 
						ad.cid = ' . ( int ) $intCid . ' 
						AND ad.property_id = ' . ( int ) $intPropertyId . '
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL 
						AND ah.is_initial_import = true' . $strCondition;

		return self::fetchApDetails( $strSql, $objDatabase );
	}

	public static function fetchPOApDetailsByApContractIdByApCodeIdByCid( $intApContractId, $intApCodeId, $intCid, $objDatabase ) {
		if( false == valId( $intApContractId ) || false == valId( $intApCodeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ad.*,
						ah.header_number,
						ap.company_name
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id = ' . ( int ) CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' AND ah.approved_on IS NOT NULL AND ah.deleted_on IS NULL )
						JOIN ap_payees ap ON ( ap.cid = ah.cid and ap.id = ah.ap_payee_id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_contract_id = ' . ( int ) $intApContractId . '
						AND ad.ap_code_id = ' . ( int ) $intApCodeId;

		return self::fetchApDetails( $strSql, $objDatabase );
	}

	public static function fetchActualBudgetByJobIdByApCodeIdByCid( $intJobId, $intApCodeId, $intCid, $objDatabase, $arrmixParameters = NULL ) {
		if( false == valId( $intJobId ) || false == valId( $intApCodeId ) || false == valId( $intCid ) ) return NULL;

		if( true == valArr( $arrmixParameters ) ) {
			$intJobPhaseId                  = ( true == isset( $arrmixParameters['job_phase_id'] ) ) ? $arrmixParameters['job_phase_id'] : NULL;
			$strPostMonth                   = ( true == isset( $arrmixParameters['phase_post_month'] ) ) ? $arrmixParameters['phase_post_month'] : NULL;
			$intApContractId                = ( true == isset( $arrmixParameters['ap_contract_id'] ) ) ? $arrmixParameters['ap_contract_id'] : NULL;
			$intUnitTypeId                  = ( true == isset( $arrmixParameters['unit_type_id'] ) ) ? $arrmixParameters['unit_type_id'] : NULL;
			$intMaintenanceLocationId       = ( true == isset( $arrmixParameters['maintenance_location_id'] ) ) ? $arrmixParameters['maintenance_location_id'] : NULL;
			$boolIsJobByUnitLocation        = ( true == $arrmixParameters['is_job_by_unit_location'] ) ? $arrmixParameters['is_job_by_unit_location'] : false;
			$boolIsShowReversals            = ( true == $arrmixParameters['show_reversals'] ) ? $arrmixParameters['show_reversals'] : false;

		}

		$strJEJoins				= '';
		$strInvoiceJoins		= '';
		$strActualBudget		= 'sum( ad.transaction_amount ) AS actual_budget_amount,';
		$strJEConditions		= '';
		$strInvoiceConditions	= '';
		$strJobPhaseConditions	= '';

		if( true == valId( $intJobPhaseId ) ) {
			$strJobPhaseConditions .= ' AND jp.id = ' . ( int ) $intJobPhaseId;
			$strJEConditions .= ' AND gd.unit_type_id IS NULL AND gd.maintenance_location_id IS NULL ';
			$strInvoiceConditions .= ' AND ad.maintenance_location_id IS NULL ';
		} else {
			$strJobPhaseConditions .= ' AND jp.job_id = ' . ( int ) $intJobId;
		}

		if( true == valStr( $strPostMonth ) ) {
			$strJEConditions .= ' AND gd.post_month =\'' . $strPostMonth . '\'';
			$strInvoiceConditions .= ' AND ad.post_month =\'' . $strPostMonth . '\'';
		}

		if( true == valId( $intApContractId ) ) {
			$strJEJoins .= ' JOIN ap_contracts apc ON ( apc.cid = gd.cid AND apc.job_id = jp.job_id AND apc.id = gd.ap_contract_id AND apc.id = ' . ( int ) $intApContractId . ' )';
			$strInvoiceJoins .= ' JOIN ap_contracts apc ON ( apc.cid = ad.cid AND apc.job_id = jp.job_id AND apc.id = ad.ap_contract_id AND apc.id = ' . ( int ) $intApContractId . ' )';
		}

		if( true == valId( $intUnitTypeId ) ) {
			$strJEJoins .= ' JOIN property_units pu ON ( pu.cid = gd.cid AND pu.id = gd.property_unit_id AND pu.unit_type_id = ' . ( int ) $intUnitTypeId . ' )';

			$strInvoiceJoins .= ' JOIN gl_details gd ON ( gd.cid = ad.cid AND gd.ap_detail_id = ad.id )
								JOIN gl_headers gh ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' )
								JOIN property_units pu ON ( pu.cid = gd.cid AND pu.id = gd.property_unit_id AND pu.unit_type_id = ' . ( int ) $intUnitTypeId . ' )
								JOIN gl_accounts ga ON ( ga.cid = gd.cid AND ga.id = gd.accrual_gl_account_id AND ga.id = ad.gl_account_id )';

			$strActualBudget = 'sum( gd.amount ) AS actual_budget_amount,';
		}

		if( true == valId( $intMaintenanceLocationId ) ) {
			$strJEConditions .= ' AND gd.maintenance_location_id = ' . ( int ) $intMaintenanceLocationId;
			$strInvoiceConditions .= ' AND ad.maintenance_location_id = ' . ( int ) $intMaintenanceLocationId;
		} else {
			$strInvoiceConditions .= ' AND ad.maintenance_location_id IS NULL ';
		}
		$strShowReverseInvoicesSql = $boolIsShowReversals ? '' : 'AND ah.reversal_ap_header_id IS NULL';
		$strShowReverseJEsSql = $boolIsShowReversals ? 'AND gh.gl_header_status_type_id IN ( ' . ( int ) CGlHeaderStatusType::POSTED . ', ' . ( int ) CGlHeaderStatusType::REVERSED . ' )' :
			'AND gh.gl_header_status_type_id = ' . ( int ) CGlHeaderStatusType::POSTED . '';

		$strSql = 'SELECT sub.* from ( ( SELECT
						ah.post_date,
						ah.is_posted,
						ah.post_month,
						ah.header_number,
						ah.id as header_id,
						ah.ap_header_sub_type_id,
						NULL as gl_post_month,
						NULL as gl_header_id,
						ad.id as detail_id,
						ad.ap_code_id,
						ad.description,
						' . $strActualBudget . '
						CASE
							WHEN ah.lease_customer_id IS NULL THEN ( COALESCE ( ap.company_name, \'\' ) || \' \' || COALESCE ( apl.location_name, \'\' ) )
							ELSE func_format_refund_customer_names ( ah.header_memo )
						END AS ap_payee_name
					FROM
						job_phases jp
						JOIN ap_details ad ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id ' . ( ( false == valId( $intUnitTypeId ) && true == $boolIsJobByUnitLocation ) ? ' AND ad.property_unit_id IS NULL ' : '' ) . ' )
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ah.ap_header_sub_type_id = ' . ( int ) CApHeaderSubType::STANDARD_JOB_INVOICE . ' )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ah.cid = ap.cid )
						JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						' . $strInvoiceJoins . '
					WHERE
						jp.cid = ' . ( int ) $intCid . '
						' . $strShowReverseInvoicesSql . '
						AND ah.is_posted = true
						AND ad.is_deleted = false 
						AND ad.deleted_on IS NULL
						AND ad.ap_code_id = ' . ( int ) $intApCodeId . $strJobPhaseConditions . $strInvoiceConditions . '
					GROUP BY
						ah.id,
						ah.cid,
						ad.ap_code_id,
						ad.description,
						ap.id,
						ad.id,
						ap.cid,
						apl.id,
						apl.cid,
						ad.unit_type_id,
						ad.maintenance_location_id
					)
					UNION
					( SELECT
							gh.post_date,
							true as is_posted,
							gh.post_month,
							gh.header_number::TEXT as header_number,
							gh.id as header_id,
							NULL as ap_header_sub_type_id,
							gd.post_month as gl_post_month,
							gh.id as gl_header_id,
							gd.id as detail_id,
							gd.ap_code_id,
							gd.memo,
							sum( gd.amount ) AS actual_budget_amount,
							NULL AS ap_payee_name
						FROM
							job_phases jp
							JOIN gl_details gd ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id AND gd.reclass_gl_detail_id IS NULL )
							JOIN gl_headers gh ON ( gd.cid = gh.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' AND gh.reclass_gl_header_id IS NULL )
							' . $strJEJoins . '
						WHERE
							jp.cid = ' . ( int ) $intCid . '
							AND gh.gl_transaction_type_id = ' . ( int ) CGlTransactionType::GL_GJ . '
							' . $strShowReverseJEsSql . '
							AND gd.ap_code_id = ' . ( int ) $intApCodeId . $strJobPhaseConditions . $strJEConditions . '
							AND gh.is_template IS FALSE
						GROUP BY
							gh.id,
							gh.cid,
							gd.id,
							gd.cid
					) ) AS sub ORDER BY  sub.post_date DESC, sub.header_id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobUnitTypesBudgetByJobIdByCid( $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					DISTINCT ad.unit_type_id AS job_unit_type_id,
					COALESCE( SUM( ad.transaction_amount ), 0 ) AS budget
				FROM 
					ap_details ad 
					JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
					JOIN ap_headers ah ON ( ah.cid = ad.cid AND ad.ap_header_id = ah.id AND ah.ap_header_sub_type_id IN( ' . CApHeaderSubType::JOB_PHASE_BUDGET . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ', ' . CApHeaderSubType::JOB_CHANGE_ORDER . ' ) )
				WHERE 
					ad.cid = ' . ( int ) $intCid . '
					AND jp.job_id = ' . ( int ) $intJobId . '
					AND ad.deleted_on IS NULL
					AND ad.deleted_by IS NULL
					AND ah.deleted_on IS NULL
					AND ad.unit_type_id IS NOT NULL
				GROUP BY
					ad.unit_type_id, 
					ad.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobPropertyLocationsBudgetByJobIdByCid( $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT jml.maintenance_location_id AS maintenance_location_id,
						COALESCE( SUM( ad.transaction_amount ), 0 ) AS budget
					FROM 
						job_maintenance_locations jml 
						JOIN job_phases jp ON ( jml.cid = jp.cid AND jml.job_id = jp.job_id )
						JOIN ap_details ad ON ( jml.cid = ad.cid AND jml.maintenance_location_id = ad.maintenance_location_id AND jp.id = ad.job_phase_id )
						JOIN ap_headers ah ON ( ah.cid = ad. cid AND ad.ap_header_id = ah.id AND ah.ap_header_sub_type_id IN( ' . CApHeaderSubType::JOB_PHASE_BUDGET . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ', ' . CApHeaderSubType::JOB_CHANGE_ORDER . ' ) )
					WHERE 
						jml.cid = ' . ( int ) $intCid . '
						AND jml.job_id = ' . ( int ) $intJobId . '
						AND ad.deleted_on IS NULL
						AND ad.deleted_by IS NULL
						AND ah.deleted_on IS NULL
					GROUP BY 
						jml.maintenance_location_id, 
						jml.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRetentionApDetailsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ad.*
				   FROM
						ap_details ad
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
				   WHERE
						ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' )
						AND ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ah.pre_approval_amount > 0
						AND ah.pre_approval_amount < ah.transaction_amount
						AND ah.approved_on IS NOT NULL
						AND ad.deleted_on IS NULL
						AND ah.deleted_on IS NULL';

		return self::fetchApDetails( $strSql, $objDatabase );

	}

	public static function fetchUnitBudgetActualsByJobIdIdByCid( $intJobId, $intCid, $objDatabase, $arrintJobPhaseIds, $boolIsFromJobBudget = false ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$strSelect = ( true == $boolIsFromJobBudget ) ? ' ad.transaction_amount - SUM(gd.amount)As actual_amount ' : ' SUM(gd.amount)As actual_amount ';

		if( true == valArr( $arrintJobPhaseIds ) ) {
			$strJobPhaseCondition = ' jp.id IN ( ' . implode( ',', $arrintJobPhaseIds ) . ' ) ';
		} else {
			$strJobPhaseCondition = ' jp.job_id = ' . ( int ) $intJobId;
		}

		$strSql = 'SELECT 
						ad.id As ap_detail_id,
                        ad.cid,
                        ad.ap_code_id,
                        ad.post_month,
                        ah.id As ap_header_id,
                        ah.ap_header_sub_type_id AS ap_header_sub_type_id,
                        ' . $strSelect . '
                    FROM 
                        ap_details ad
                        JOIN job_phases jp ON (jp.cid = ad.cid AND jp.id = ad.job_phase_id )
                        JOIN ap_headers ah ON (ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ')
                        JOIN gl_details gd ON (gd.cid = ad.cid AND gd.ap_detail_id = ad.id AND gd.property_unit_id IS NOT NULL)
                        JOIN gl_headers gh ON (gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ')
                        JOIN gl_accounts ga ON (ga.cid = gd.cid AND ga.id = gd.accrual_gl_account_id AND ga.id = ad.gl_account_id)
                    WHERE
                       
                        ' . $strJobPhaseCondition . '
                        AND jp.cid = ' . ( int ) $intCid . '
                        AND ah.is_posted = TRUE
                        AND ah.deleted_on IS NULL
                        AND ad.details IS NOT NULL 
                        
                    GROUP BY 
                        ad.id,
                        ad.cid,
                        ah.id,
                        ah.ap_header_sub_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPendingBillbacksApDetailsByBillbacksFilterByCid( CBillbacksFilter $objBillbacksFilter, int $intCid, CDatabase $objClientDatabase ) {

		$strSql					= '';
		$strConditions			= '';
		$arrstrAndConditions	= [];

		if( true == valId( $objBillbacksFilter->getPropertyId() ) ) {
			$arrstrAndConditions[] = 'ad.property_id = ' . $objBillbacksFilter->getPropertyId();
		} else {
			return [];
		}

		if( true == valIntArr( $objBillbacksFilter->getApHeaderIds() ) ) {
			$arrstrAndConditions[] = 'ad.ap_header_id IN ( ' . implode( ',', $objBillbacksFilter->getApHeaderIds() ) . ' )';
		}

		if( true == valIntArr( $objBillbacksFilter->getApPayeeLocationIds() ) ) {
			$arrstrAndConditions[] = 'ah.ap_payee_location_id IN ( ' . implode( ',', $objBillbacksFilter->getApPayeeLocationIds() ) . ' )';
		}

		if( true == valStr( $objBillbacksFilter->getStartPostMonth() ) && true == CValidation::validateDate( $objBillbacksFilter->getStartPostMonth() ) ) {
			$arrstrAndConditions[] = 'ad.post_month >= \'' . date( 'Y-m-d', strtotime( $objBillbacksFilter->getStartPostMonth() ) ) . '\'';
		}

		if( true == valStr( $objBillbacksFilter->getEndPostMonth() ) && true == CValidation::validateDate( $objBillbacksFilter->getEndPostMonth() ) ) {
			$arrstrAndConditions[] = 'ad.post_month <= \'' . date( 'Y-m-d', strtotime( $objBillbacksFilter->getEndPostMonth() ) ) . '\'';
		}

		if( true == valArr( $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		$strSql .= 'SELECT
						ad.id,
						ad.ap_header_id,
						ad.post_month,
						ah.transaction_amount as header_transaction_amount,
						ad.transaction_amount,
						ad.description,
						ah.header_number,
						ah.ap_payee_id,
						apl.vendor_code,
						COALESCE( ap.company_name, \'\' ) || \', \' || COALESCE( apl.location_name, \'\' ) AS ap_payee_name,
						apl.location_name,
						p.id AS property_id,
						p.property_name,
						glat.name AS gl_account_name,
						glat.formatted_account_number AS gl_account_number,
						ROW_NUMBER() OVER ( PARTITION BY ad.cid, ad.ap_header_id ORDER BY ah.header_number ) AS ap_detail_row_number,
						COUNT( 1 ) OVER ( PARTITION BY ad.cid, ad.ap_header_id ORDER BY ah.header_number ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING ) AS total_ap_details
					FROM
						ap_details ad
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						JOIN gl_account_trees glat ON ( glat.cid = ad.cid AND glat.gl_account_id = ad.gl_account_id )
						JOIN properties p ON ( p.cid = ad.cid AND p.id = ad.property_id )
						JOIN ap_payees ap ON ( ap.cid = ad.cid AND ap.id = ah.ap_payee_id )
						JOIN ap_payee_locations apl ON ( ap.cid = apl.cid AND ah.ap_payee_id = apl.ap_payee_id AND ah.ap_payee_location_id = apl.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ah.transaction_amount_due = 0
						AND glat.is_default = 1
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND NOT ah.is_template
						AND ad.id NOT IN (
							SELECT DISTINCT ( original_ap_detail_id )
							FROM ap_detail_reimbursements
							WHERE is_deleted = false
						)
						AND ad.ap_transaction_type_id = ' . CApTransactionType::STANDARD . '
						AND ad.reimbursement_method_id = ' . CReimbursementMethod::BILLBACK . ' 
						' . $strConditions . '
					ORDER BY
						ah.header_number,
						ah.id,
						ap_detail_row_number';

		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

	public static function fetchApDetailsByReimbursementApDetailIdsByCid( $arrintReimbursementApDetailIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintReimbursementApDetailIds ) ) return NULL;

		$strSql = 'SELECT
						ad.*
					FROM
						ap_detail_reimbursements adr
						JOIN ap_details ad ON ( adr.cid = ad.cid AND adr.original_ap_detail_id = ad.id )
					WHERE
						adr.cid = ' . ( int ) $intCid . '
						AND adr.reimbursement_ap_detail_id IN ( ' . implode( ',', $arrintReimbursementApDetailIds ) . ' )
						AND adr.is_deleted = false
					ORDER BY
						ad.id ASC';
		return self::fetchApDetails( $strSql, $objClientDatabase );
	}

}

?>