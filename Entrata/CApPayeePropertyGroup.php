<?php

class CApPayeePropertyGroup extends CBaseApPayeePropertyGroup {

	protected $m_intPropertyId;
	protected $m_intIsDisabled;

	protected $m_strSystemCode;
	protected $m_strPropertyName;
	protected $m_strPropertyGroupName;
	protected $m_strPropertyPreferenceKey;
	protected $m_strPropertyPreferenceValue;
	protected $m_strDefaultGlAccountName;

	protected $m_intComplianceStatusId;

	/**
	 * Get Functions
	 */

	public function getComplianceStatusId() {
		return $this->m_intComplianceStatusId;
	}

	public function getPropertyGroupName() {
		return $this->m_strPropertyGroupName;
	}

	public function getPropertyPreferenceKey() {
		return $this->m_strPropertyPreferenceKey;
	}

	public function getPropertyPreferenceValue() {
		return $this->m_strPropertyPreferenceValue;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function getDefaultGlAccountName() {
		return $this->m_strDefaultGlAccountName;
	}

	/**
	 * Set Functions
	 */

	public function setComplianceStatusId( $intComplianceStatusId ) {
		$this->m_intComplianceStatusId = $intComplianceStatusId;
	}

	public function setPropertyGroupName( $strPropertyGroupName ) {
		$this->m_strPropertyGroupName = $strPropertyGroupName;
	}

	public function setPropertyPreferenceKey( $strPropertyPreferenceKey ) {
		$this->m_strPropertyPreferenceKey = CStrings::strTrimDef( $strPropertyPreferenceKey, 64, NULL, true );
	}

	public function setPropertyPreferenceValue( $strPropertyPreferenceValue ) {
		$this->m_strPropertyPreferenceValue = CStrings::strTrimDef( $strPropertyPreferenceValue, -1, NULL, true );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->m_intIsDisabled = $intIsDisabled;
	}

	public function setSystemCode( $strSystemCode ) {
		$this->m_strSystemCode = $strSystemCode;
	}

	public function setDefaultGlAccountName( $strDefaultGlAccountName ) {
		$this->m_strDefaultGlAccountName = $strDefaultGlAccountName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['property_group_name'] ) ) $this->setPropertyGroupName( $arrmixValues['property_group_name'] );
		if( true == isset( $arrmixValues['property_preference_key'] ) ) $this->setPropertyPreferenceKey( $arrmixValues['property_preference_key'] );
		if( true == isset( $arrmixValues['property_preference_value'] ) ) $this->setPropertyPreferenceValue( $arrmixValues['property_preference_value'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['is_disabled'] ) ) $this->setIsDisabled( $arrmixValues['is_disabled'] );
		if( true == isset( $arrmixValues['system_code'] ) ) $this->setSystemCode( $arrmixValues['system_code'] );
		if( true == isset( $arrmixValues['default_gl_account_name'] ) ) $this->setDefaultGlAccountName( $arrmixValues['default_gl_account_name'] );

	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPayeeLocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>