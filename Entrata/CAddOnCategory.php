<?php

class CAddOnCategory extends CBaseAddOnCategory {

	protected $m_arrobjAddOnGroups;

	protected $m_intAddOnCount;
	protected $m_intAddOnGroupCount;
	protected $m_intIsAvailable;
	protected $m_intAddOnGroupId;
	protected $m_intAddOnId;
	protected $m_intCompanyMediaFileId;

	protected $m_fltMinRentAmount;
	protected $m_fltMaxRentAmount;

    protected $m_boolIsReserved;

    /**
     * Fetch functions
     */

    public function fetchAddOnGroups( $objDatabase, $boolIsFetchIntegrated = false ) {
    	return \Psi\Eos\Entrata\CAddOnGroups::createService()->fetchAddOnGroupsByAddOnCategoryIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsFetchIntegrated );
    }

    public function createAddOnGroup() {

    	$objAddOnGroup = new CAddOnGroup();
    	$objAddOnGroup->setAddOnCategoryId( $this->getId() );

    	return $objAddOnGroup;
    }

 	/**
     * Set functions
     */

    public function setAddOnId( $intAddOnId ) {
    	$this->m_intAddOnId = CStrings::strToIntDef( $intAddOnId, NULL, false );
    }

    public function setAddOnGroups( $arrobjAddOnGroups ) {
    	$this->m_arrobjAddOnGroups = $arrobjAddOnGroups;
    }

    public function setAddOnCount( $intAddOnCount ) {
    	$this->m_intAddOnCount = CStrings::strToIntDef( $intAddOnCount, NULL, false );
    }

    public function setAddOnGroupId( $intAddOnGroupId ) {
    	$this->m_intAddOnGroupId = CStrings::strToIntDef( $intAddOnGroupId, NULL, false );
    }

    public function setAddOnGroupCount( $intAddOnGroupCount ) {
    	$this->m_intAddOnGroupCount = CStrings::strToIntDef( $intAddOnGroupCount, NULL, false );
    }

    public function setIsAvailable( $intIsAvailable ) {
    	$this->m_intIsAvailable = CStrings::strToIntDef( $intIsAvailable, NULL, false );
    }

    public function setMinRentAmount( $fltMinRentAmount ) {
    	$this->m_fltMinRentAmount = CStrings::strToFloatDef( $fltMinRentAmount, NULL, false, 65531 );
    }

    public function setMaxRentAmount( $fltMaxRentAmount ) {
    	$this->m_fltMaxRentAmount = CStrings::strToFloatDef( $fltMaxRentAmount, NULL, false, 65531 );
    }

    public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
    	$this->m_intCompanyMediaFileId = CStrings::strToFloatDef( $intCompanyMediaFileId, NULL, false, 65531 );
    }

    public function setIsReserved( $boolIsReserved ) {
        $this->m_boolIsReserved = $boolIsReserved;
    }

    /**
     * Get functions
     */

    public function getAddOnId() {
    	return $this->m_intAddOnId;
    }

    public function getAddOnGroups() {
    	return $this->m_arrobjAddOnGroups;
    }

  	public function getAddOnCount() {
    	return $this->m_intAddOnCount;
    }

    public function getAddOnGroupId() {
    	return $this->m_intAddOnGroupId;
    }

    public function getAddOnGroupCount() {
    	return $this->m_intAddOnGroupCount;
    }

    public function getIsAvailable() {
    	return $this->m_intIsAvailable;
    }

    public function getMinRentAmount() {
    	return $this->m_fltMinRentAmount;
    }

    public function getMaxRentAmount() {
    	return $this->m_fltMaxRentAmount;
    }

    public function getCompanyMediaFileId() {
    	return $this->m_intCompanyMediaFileId;
    }

    public function getIsReserved() {
        return $this->m_boolIsReserved;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = false, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['add_on_id'] ) ) {
    		$this->setAddOnId( $arrmixValues['add_on_id'] );
	    }
       	if( true == isset( $arrmixValues['add_on_count'] ) ) {
    		$this->setAddOnCount( $arrmixValues['add_on_count'] );
        }
       	if( true == isset( $arrmixValues['add_on_group_id'] ) ) {
    		$this->setAddOnGroupId( $arrmixValues['add_on_group_id'] );
        }
    	if( true == isset( $arrmixValues['add_on_group_count'] ) ) {
    		$this->setAddOnGroupCount( $arrmixValues['add_on_group_count'] );
	    }
    	if( true == isset( $arrmixValues['is_available'] ) ) {
    		$this->setIsAvailable( $arrmixValues['is_available'] );
	    }
    	if( true == isset( $arrmixValues['min_rent_amount'] ) ) {
    		$this->setMinRentAmount( $arrmixValues['min_rent_amount'] );
	    }
    	if( true == isset( $arrmixValues['max_rent_amount'] ) ) {
    		$this->setMaxRentAmount( $arrmixValues['max_rent_amount'] );
	    }
    	if( true == isset( $arrmixValues['company_media_file_id'] ) ) {
    		$this->setCompanyMediaFileId( $arrmixValues['company_media_file_id'] );
	    }
    }

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddOnTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objClientDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Custom service name is required.' ) ) );
		} else {
			$intConflictingAddOnCategoryCount = \Psi\Eos\Entrata\CAddOnCategories::createService()->fetchConflictingAddOnCategoryCountByAddOnTypeByPropertyIdByCidByName( $this->getAddOnTypeId(), $this->getPropertyId(), $this->getCid(), $this->getName(), $objClientDatabase, $this->getId() );
			if( 0 < $intConflictingAddOnCategoryCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Custom service already exists with that name.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsComparable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequirable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyMediaFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultAddOnCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

}
?>