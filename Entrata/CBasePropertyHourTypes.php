<?php

class CBasePropertyHourTypes extends CEosPluralBase {

	public static function fetchPropertyHourTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPropertyHourType', $objDatabase );
	}

	public static function fetchPropertyHourType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyHourType', $objDatabase );
	}

	public static function fetchPropertyHourTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_hour_types', $objDatabase );
	}

	public static function fetchPropertyHourTypeById( $intId, $objDatabase ) {
		return self::fetchPropertyHourType( sprintf( 'SELECT * FROM property_hour_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>