<?php

class CFileMetadata extends CBaseFileMetadata {

	protected $m_intPropertyId;
	protected $m_intExternalLeaseId;
	protected $m_intExternalEsignatureId;
	protected $m_intExternalApplicationId;
	protected $m_intSyncAttemptCount;

	protected $m_strPersonName;
	protected $m_strExternalDocumentName;

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getExternalLeaseId() {
    	return $this->m_intExternalLeaseId;
    }

    public function getExternalEsignatureId() {
    	return $this->m_intExternalEsignatureId;
    }

    public function getExternalApplicationId() {
    	return $this->m_intExternalApplicationId;
    }

    public function getSyncAttemptCount() {
    	return $this->m_intSyncAttemptCount;
    }

    public function getPersonName() {
    	return $this->m_strPersonName;
    }

    public function getExternalDocumentName() {
    	return $this->m_strExternalDocumentName;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setExternalLeaseId( $intExternalLeaseId ) {
    	$this->m_intExternalLeaseId = $intExternalLeaseId;
    }

    public function setExternalEsignatureId( $intExternalEsignatureId ) {
    	$this->m_intExternalEsignatureId = $intExternalEsignatureId;
    }

    public function setExternalApplicationId( $intExternalApplicationId ) {
    	$this->m_intExternalApplicationId = $intExternalApplicationId;
    }

    public function setSyncAttemptCount( $intSyncAttemptCount ) {
    	$this->m_intSyncAttemptCount = $intSyncAttemptCount;
    }

    public function setPersonName( $strPersonName ) {
    	$this->m_strPersonName = $strPersonName;
    }

    public function setExternalDocumentName( $strExternalDocumentName ) {
    	$this->m_strExternalDocumentName = $strExternalDocumentName;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

    	if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['external_lease_id'] ) ) $this->setExternalLeaseId( $arrmixValues['external_lease_id'] );
    	if( true == isset( $arrmixValues['external_esignature_id'] ) ) $this->setExternalEsignatureId( $arrmixValues['external_esignature_id'] );
    	if( true == isset( $arrmixValues['external_application_id'] ) ) $this->setExternalApplicationId( $arrmixValues['external_application_id'] );
    	if( true == isset( $arrmixValues['external_document_name'] ) ) $this->setExternalDocumentName( $arrmixValues['external_document_name'] );
    	if( true == isset( $arrmixValues['person_name'] ) ) $this->setPersonName( $arrmixValues['person_name'] );
    	if( true == isset( $arrmixValues['sync_attempt_count'] ) ) $this->setSyncAttemptCount( $arrmixValues['sync_attempt_count'] );

    	return;
    }

    public function setData( $jsonData, $strLocaleCode = NULL ) {

	    if( true == valStr( $jsonData ) ) {
	    	$arrmixData = hstoreToArray( $jsonData );
		    $jsonData = json_decode( json_encode( $arrmixData ) );
	    } elseif( true == valArr( $jsonData ) ) {
	    	$jsonData = json_decode( json_encode( $jsonData ) );
	    }

	    parent::setData( $jsonData, $strLocaleCode = NULL );
    }

	public function getData() {
		$jsonData = parent::getData();
		return json_decode( json_encode( $jsonData ), true );
	}

	public function setMultivalData( $jsonMultivalData ) {
	    if( false == valObj( $jsonMultivalData, 'stdClass' ) && true == valStr( $jsonMultivalData ) ) {
		    $arrmixData = hstoreToArray( $jsonMultivalData );
		    $jsonMultivalData = json_decode( json_encode( $arrmixData ) );
	    }

    	parent::setMultivalData( $jsonMultivalData );
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valData() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMultivalData() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function updateLeaseXmlDataInFileMetadata( $strLeaseXmlData, $intCompanyUserId, $objDatabase, $objFile = NULL ) {

		$objParsedData = simplexml_load_string( $strLeaseXmlData );

		if( false == isset( $objParsedData->STANDARD ) ) return false;

		$objXmlData = $objParsedData->STANDARD;
		$arrmixData[] = arrayToHstore( $this->getData() );
		$arrmixMultivalData = $this->getMultivalData();

		if( true == valObj( $arrmixMultivalData, 'stdClass' ) ) {
			$arrmixMultivalData[] = arrayToHstore( $this->getMultivalData() );
		}

		if( true == valObj( $objFile, 'CFile' ) && true == valStr( $objFile->getPropertyName() ) ) $arrmixData[] = '"property_name"=>"' . addslashes( $objFile->getPropertyName() ) . '"';

		// store all available nodes which are not blank
		foreach( $objXmlData->children() as $strKey => $strValue ) {
			if( '' != $strValue ) {
				$strCleanKey = \Psi\CStringService::singleton()->strtolower( str_replace( '-', '_', $strKey ) );
				$arrmixData[] = '"' . $strCleanKey . '"=>"' . addslashes( $strValue ) . '"';
			}
		}

		if( true == isset( $objXmlData->{'LEASE-BEGIN-DATE'} ) ) {
			$arrmixData[] = '"lease_start_date"=>"' . addslashes( $objXmlData->{'LEASE-BEGIN-DATE'} ) . '"';
		}

		if( true == isset( $objXmlData->{'LEASE-END-DATE'} ) ) {
			$arrmixData[] = '"lease_end_date"=>"' . addslashes( $objXmlData->{'LEASE-END-DATE'} ) . '"';
		}

		if( true == isset( $objXmlData->{'RESIDENT-1'} ) ) {
			$strPersonName = '';

			$arrstrSplitName = explode( ' ', ( string ) $objXmlData->{'RESIDENT-1'} );

			if( true == valArr( $arrstrSplitName ) ) {
				$strPersonFirstName = array_shift( $arrstrSplitName );
				$strPersonName .= $strPersonFirstName;
				$arrmixData[] = '"person_first_name1"=>"' . addslashes( $strPersonFirstName ) . '"';
				if( true == valArr( $arrstrSplitName ) ) {
					$strPersonName .= ' ' . implode( ' ', $arrstrSplitName );
					$arrmixData[] = '"person_last_name1"=>"' . addslashes( implode( ' ', $arrstrSplitName ) ) . '"';
				}
			}

			for( $intCount = 1; $intCount < 6; $intCount++ ) {
				$strResident = 'RESIDENT-' . $intCount;
				if( true == isset( $objXmlData->$strResident ) && true == valStr( $objXmlData->$strResident ) ) {
					$strResident = \Psi\CStringService::singleton()->preg_replace( '/[^a-z-_ ]/', '', \Psi\CStringService::singleton()->strtolower( $objXmlData->$strResident ) );
					$arrmixMultivalData[] = '"person_name|' . $strResident . '"=>"NULL"';
				}
			}

			$this->setPersonName( $strPersonName );
			$this->setEntityName( $strPersonName );
		}

		if( true == isset( $objXmlData->{'UNIT-NUMBER'} ) ) {
			$arrmixData[] = '"unit_number"=>"' . addslashes( $objXmlData->{'UNIT-NUMBER'} ) . '"';
		}

		$arrmixData[] = '"remote_details_synced_on"=>"' . date( 'm/d/Y H:i:s' ) . '"';
		$arrmixData[] = '"force_remote_sync"=>"0"';
		$arrmixData[] = '"sys_is_hidden"=>"0"';

		if( false != valArr( $arrmixData ) ) {
			$this->setData( implode( ',', $arrmixData ) );
		}
		if( false != valArr( $arrmixMultivalData ) ) {
			$this->setMultivalData( implode( ',', $arrmixMultivalData ) );
		}

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		return true;

	}

	public function updateApplicationXmlDataInFileMetadata( $strApplicationXmlData, $intCompanyUserId, $objDatabase, $objFile = NULL ) {

		$objParsedData = simplexml_load_string( $strApplicationXmlData );

		if( false == isset( $objParsedData->APPLICANT ) ) return false;

		$objXmlData = $objParsedData->APPLICANT;
		$arrmixData[] = arrayToHstore( $this->getData() );
		$arrmixMultivalData = $this->getMultivalData();

		if( true == valObj( $arrmixMultivalData, 'stdClass' ) ) {
			$arrmixMultivalData[] = arrayToHstore( $this->getMultivalData() );
		}

		if( true == valObj( $objFile, 'CFile' ) && true == valStr( $objFile->getPropertyName() ) ) $arrmixData[] = '"property_name"=>"' . addslashes( $objFile->getPropertyName() ) . '"';

		// store all available nodes which are not blank
		foreach( $objXmlData->children() as $strKey => $strValue ) {
			if( '' != $strValue ) {
				$strCleanKey = \Psi\CStringService::singleton()->strtolower( str_replace( '-', '_', $strKey ) );
				$arrmixData[] = '"' . $strCleanKey . '"=>"' . addslashes( $strValue ) . '"';
			}
		}

		if( true == isset( $objXmlData->{'LEASE-BEGIN-DATE'} ) ) {
			$arrmixData[] = '"lease_start_date"=>"' . addslashes( $objXmlData->{'LEASE-BEGIN-DATE'} ) . '"';
		}

		if( true == isset( $objXmlData->{'LEASE-END-DATE'} ) ) {
			$arrmixData[] = '"lease_end_date"=>"' . addslashes( $objXmlData->{'LEASE-END-DATE'} ) . '"';
		}

		if( true == isset( $objXmlData->{'APPLICANT-FIRST-NAME'} ) || true == isset( $objXmlData->{'APPLICANT-LAST-NAME'} ) ) {
			$strPersonName = '';

			if( true == isset( $objXmlData->{'APPLICANT-FIRST-NAME'} ) ) {
				$arrmixData[] = '"person_first_name1"=>"' . addslashes( $objXmlData->{'APPLICANT-FIRST-NAME'} ) . '"';
				$strPersonName = $objXmlData->{'APPLICANT-FIRST-NAME'};
			}

			if( true == isset( $objXmlData->{'APPLICANT-LAST-NAME'} ) ) {
				$arrmixData[] = '"person_last_name1"=>"' . addslashes( $objXmlData->{'APPLICANT-LAST-NAME'} ) . '"';
				$strPersonName .= ( '' != $strPersonName ) ? ' ' . $objXmlData->{'APPLICANT-LAST-NAME'}: $objXmlData->{'APPLICANT-LAST-NAME'};
			}

			$this->setPersonName( $strPersonName );
			$this->setEntityName( $strPersonName );
		}

		$arrmixData[] = '"remote_details_synced_on"=>"' . date( 'm/d/Y H:i:s' ) . '"';
		$arrmixData[] = '"force_remote_sync"=>"0"';
		$arrmixData[] = '"sys_is_hidden"=>"0"';

		if( false != valArr( $arrmixData ) ) {
			$this->setData( implode( ',', $arrmixData ) );
		}
		if( false != valArr( $arrmixMultivalData ) ) {
			$this->setMultivalData( implode( ',', $arrmixMultivalData ) );
		}

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		return true;

	}

	public function updateStoreUnitInfoInFileMetadata( $arrmixUnitData, $intCompanyUserId, $objDatabase ) {
		$arrmixData[] = arrayToHstore( $this->getData() );
		$arrmixMultivalData = $this->getMultivalData();

		if( true == valObj( $arrmixMultivalData, 'stdClass' ) ) {
			$arrmixMultivalData[] = arrayToHstore( $this->getMultivalData() );
		}

		if( true == isset( $arrmixUnitData->UnitNumber ) ) {
			$arrmixData[] = '"unit_number"=>"' . addslashes( $arrmixUnitData->UnitNumber ) . '"';
		}

		if( true == isset( $arrmixUnitData->LeaseTermBeginDate ) ) {
			$arrmixData[] = '"lease_start_date"=>"' . addslashes( $arrmixUnitData->LeaseTermBeginDate ) . '"';
		}

		if( true == isset( $arrmixUnitData->LeaseTermEndDate ) ) {
			$arrmixData[] = '"lease_end_date"=>"' . addslashes( $arrmixUnitData->LeaseTermEndDate ) . '"';
		}

		if( true == isset( $arrmixUnitData->Residents->item ) ) {
			$strPersonName = '';

			if( true == valArr( $arrmixUnitData->Residents->item ) ) {
				$strPersonName = ( string ) $arrmixUnitData->Residents->item[0]->Name;

				$arrstrSplitName = explode( ' ', $strPersonName );

				if( true == valArr( $arrstrSplitName ) ) {
					$arrmixData[] = '"person_first_name1"=>"' . addslashes( array_shift( $arrstrSplitName ) ) . '"';
					if( true == valArr( $arrstrSplitName ) ) {
						$arrmixData[] = '"person_last_name1"=>"' . addslashes( implode( ' ', $arrstrSplitName ) ) . '"';
					}
				}

				foreach( $arrmixUnitData->Residents->item as $arrmixItem ) {
					$strResident = \Psi\CStringService::singleton()->preg_replace( '/[^a-z-_ ]/', '', \Psi\CStringService::singleton()->strtolower( $arrmixItem->Name ) );
					$arrmixMultivalData[] = '"person_name|' . $strResident . '"=>"NULL"';
				}
			} else {
				$strPersonName = ( string ) $arrmixUnitData->Residents->item->Name;

				$arrstrSplitName = explode( ' ', $strPersonName );

				if( true == valArr( $arrstrSplitName ) ) {
					$arrmixData[] = '"person_first_name1"=>"' . addslashes( array_shift( $arrstrSplitName ) ) . '"';
					if( true == valArr( $arrstrSplitName ) ) {
						$arrmixData[] = '"person_last_name1"=>"' . addslashes( implode( ' ', $arrstrSplitName ) ) . '"';
					}
				}

				$arrmixData[] = '"person_name"=>"' . addslashes( $strPersonName ) . '"';

				$strResident = \Psi\CStringService::singleton()->preg_replace( '/[^a-z-_ ]/', '', \Psi\CStringService::singleton()->strtolower( $arrmixUnitData->Residents->item->Name ) );
				$arrmixMultivalData[] = '"person_name|' . $strResident . '"=>"NULL"';
			}

			$this->setPersonName( $strPersonName );
			$this->setEntityName( $strPersonName );
		}

		$arrmixData[] = '"remote_details_synced_on"=>"' . date( 'm/d/Y H:i:s' ) . '"';
		$arrmixData[] = '"force_remote_sync"=>"0"';
		$arrmixData[] = '"sys_is_hidden"=>"0"';

		if( false != valArr( $arrmixData ) ) {
			$this->setData( implode( ',', $arrmixData ) );
		}
		if( false != valArr( $arrmixMultivalData ) ) {
			$this->setMultivalData( implode( ',', $arrmixMultivalData ) );
		}

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function updateEsignatureDataInFileMetadata( $arrmixEsigData, $intCompanyUserId, $objDatabase ) {
		$arrmixData[] = arrayToHstore( $this->getData() );
		$arrmixMultivalData = $this->getMultivalData();

		if( true == valObj( $arrmixMultivalData, 'stdClass' ) ) {
			$arrmixMultivalData[] = arrayToHstore( $this->getMultivalData() );
		}

		if( true == isset( $arrmixEsigData->UnitNumber ) ) {
			$arrmixData[] = '"unit_number"=>"' . addslashes( $arrmixEsigData->UnitNumber ) . '"';
		}

		if( true == isset( $arrmixEsigData->LeaseTermBeginDate ) ) {
			$arrmixData[] = '"lease_start_date"=>"' . addslashes( $arrmixEsigData->LeaseTermBeginDate ) . '"';
		}

		if( true == isset( $arrmixEsigData->Id ) ) {
			$arrmixData[] = '"entity_id"=>"' . addslashes( $arrmixEsigData->Id ) . '"';
			$this->setEntityId( intval( $arrmixEsigData->Id ) );
		}

		if( true == isset( $arrmixEsigData->Signers->item ) ) {
			$strPersonName = $strPrimaryPersonName = '';

			if( true == valArr( $arrmixEsigData->Signers->item ) ) {

				$intPersonOrdinal = 1;
				foreach( $arrmixEsigData->Signers->item as $arrmixItem ) {
					$strPersonName = ( string ) $arrmixItem->Name;

					if( 1 == $intPersonOrdinal ) $strPrimaryPersonName = $strPersonName;

					$arrstrSplitName = NULL;

					if( false == is_object( $arrmixItem ) && true == valStr( $arrmixItem ) ) {
						$arrstrSplitName = explode( ' ', $arrmixItem );
					} else {
						$arrstrSplitName = explode( ' ', $strPersonName );
					}

					if( true == valArr( $arrstrSplitName ) ) {
						$arrmixData[] = '"person_first_name' . $intPersonOrdinal . '"=>"' . addslashes( array_shift( $arrstrSplitName ) ) . '"';
						if( true == valArr( $arrstrSplitName ) ) {
							$arrmixData[] = '"person_last_name' . $intPersonOrdinal . '"=>"' . addslashes( implode( ' ', $arrstrSplitName ) ) . '"';
						}

						$intPersonOrdinal++;
					}

					// $strResident = preg_replace( '/[^a-z-_ ]/', '', \Psi\CStringService::singleton()->strtolower( $arrmixItem->Name ) );
					// $arrmixMultivalData[] = '"person_name|' . $strResident . '"=>"NULL"';
				}
			} else {
				$strPersonName = $strPrimaryPersonName = ( string ) $arrmixEsigData->Signers->item->Name;
				$arrstrSplitName = NULL;

				if( true == valStr( $strPersonName ) ) {
					$arrstrSplitName = explode( ' ', $strPersonName );
				}

				if( true == valArr( $arrstrSplitName ) ) {
					$arrmixData[] = '"person_first_name1"=>"' . addslashes( array_shift( $arrstrSplitName ) ) . '"';
					if( true == valArr( $arrstrSplitName ) ) {
						$arrmixData[] = '"person_last_name1"=>"' . addslashes( implode( ' ', $arrstrSplitName ) ) . '"';
					}
				}

				$arrmixData[] = '"person_name"=>"' . addslashes( $strPersonName ) . '"';

				// $strResident = preg_replace( '/[^a-z-_ ]/', '', \Psi\CStringService::singleton()->strtolower( $arrmixEsigData->Signers->item->Name ) );
				// $arrmixMultivalData[] = '"person_name|' . $strResident . '"=>"NULL"';
			}

			$this->setPersonName( $strPrimaryPersonName );
			$this->setEntityName( $strPrimaryPersonName );
		}

		$arrmixData[] = '"remote_details_synced_on"=>"' . date( 'm/d/Y H:i:s' ) . '"';
		$arrmixData[] = '"force_remote_sync"=>"0"';
		$arrmixData[] = '"sys_is_hidden"=>"0"';

		if( false != valArr( $arrmixData ) ) {
			$this->setData( implode( ',', $arrmixData ) );
		}
		if( false != valArr( $arrmixMultivalData ) ) {
			$this->setMultivalData( implode( ',', $arrmixMultivalData ) );
		}

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function updateStoreApplicantInfoInFileMetadata( $objApplicantData, $intCompanyUserId, $objDatabase ) {
		$arrmixData[] = arrayToHstore( $this->getData() );
		$arrmixMultivalData = $this->getMultivalData();

		if( true == valObj( $arrmixMultivalData, 'stdClass' ) ) {
			$arrmixMultivalData[] = arrayToHstore( $this->getMultivalData() );
		}

		if( true == isset( $objApplicantData->UnitNumber ) ) {
			$arrmixData[] = '"unit_number"=>"' . addslashes( $objApplicantData->UnitNumber ) . '"';
		}

		if( true == isset( $objApplicantData->LeaseTermBeginDate ) ) {
			$arrmixData[] = '"application_date"=>"' . addslashes( $objApplicantData->ApplicationDate ) . '"';
		}

		if( true == isset( $objApplicantData->FirstName ) || true == isset( $objApplicantData->LastName ) ) {
			$strPersonName = '';

			if( true == isset( $objApplicantData->FirstName ) ) {
				$arrmixData[] = '"person_first_name1"=>"' . addslashes( $objApplicantData->FirstName ) . '"';
				$strPersonName = $objApplicantData->FirstName;
			}

			if( true == isset( $objApplicantData->LastName ) ) {
				$arrmixData[] = '"person_last_name1"=>"' . addslashes( $objApplicantData->LastName ) . '"';
				$strPersonName .= ( '' != $strPersonName ) ? ' ' . $objApplicantData->LastName: $objApplicantData->LastName;
			}

			$this->setPersonName( $strPersonName );
			$this->setEntityName( $strPersonName );
		}

		$arrmixData[] = '"remote_details_synced_on"=>"' . date( 'm/d/Y H:i:s' ) . '"';
		$arrmixData[] = '"force_remote_sync"=>"0"';
		$arrmixData[] = '"sys_is_hidden"=>"0"';

		if( false != valArr( $arrmixData ) ) {
			$this->setData( implode( ',', $arrmixData ) );
		}
		if( false != valArr( $arrmixMultivalData ) ) {
			$this->setMultivalData( implode( ',', $arrmixMultivalData ) );
		}

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function updateSyncAttempt( $intCompanyUserId, $objDatabase ) {

    	$arrmixData = $this->getData();

		$arrmixData['sync_attempt_count'] = $this->getSyncAttemptCount() + 1;
		$arrmixData['next_sync_datetime'] = date( 'Y-m-d H:i:s', strtotime( date( 'Y-m-d H:i:s' ) . ' +3 hour' ) );

		$this->setData( $arrmixData );

		if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function buildDataField( $arrstrDataList ) {
		if( false == valArr( $arrstrDataList ) ) return false;

		foreach( $arrstrDataList as $strKey => $strValue ) {

			// if field name start with 'array_' (ex array_persone_name_first ) then split mulitple person name etc as person_name_first1, person_name_first2 ... etc.
			if( 'array_' == \Psi\CStringService::singleton()->substr( $strKey, 0, 6 ) ) {
				$arrstrValues = explode( ',', trim( $strValue, '{}' ) );

				$intCountValues = \Psi\Libraries\UtilFunctions\count( $arrstrValues );

				for( $intK = 0; $intK < $intCountValues; $intK++ ) {
					$arrstrData[] = sprintf( '"%s"=>"%s"', str_replace( 'array_', '', $strKey ) . ( $intK + 1 ), addslashes( $arrstrValues[$intK] ) );
				}
			} else {
				$arrstrData[] = sprintf( '"%s"=>"%s"', $strKey, addslashes( $strValue ) );
			}
		}

		return implode( ',', $arrstrData );
	}

}
?>