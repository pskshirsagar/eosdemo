<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedDashboardPriorities
 * Do not add any new functions to this class.
 */

class CCachedDashboardPriorities extends CBaseCachedDashboardPriorities {

	public static function fetchPaginatedPotentialLeadsByDashboardFilterByCid( $arrintCallIds, $objDashboardFilter, $intCid, $intPageOffset, $intPageSize, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valId( $intCid ) ) return NULL;

		$strWhereSql  = '';
		$strWhereSql .= ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND cdp.priority_id IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';
		$strWhereSql .= ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND cdp.company_employee_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' )' : '';
		$strWhereSql .= ( true == valArr( $arrintCallIds ) ) ? ' AND CASE WHEN ( cdp.details ->> \'lead_type\' = \'Answered Call\' OR cdp.details ->> \'lead_type\' = \'Unanswered Call\' OR cdp.details ->> \'lead_type\' = \'Voice Mail\' ) AND util.util_to_int( cdp.details ->> \'refer_id\' ) NOT IN ( ' . implode( ',', $arrintCallIds ) . ' ) THEN false ELSE true END ' : ' AND CASE WHEN ( cdp.details ->> \'lead_type\' = \'Answered Call\' OR cdp.details ->> \'lead_type\' = \'Unanswered Call\' OR cdp.details ->> \'lead_type\' = \'Voice Mail\' ) THEN false ELSE true END';

		$strSql = 'SELECT
						cdp.id,
						cdp.cid,
						cdp.property_id,
						cdp.reference_id,
						cdp.priority_id AS priority,
						cdp.details ->> \'lead_type\' AS lead_type,
						cdp.details ->> \'lead_detail\' AS lead_detail,
						cdp.details ->> \'created_on\' AS created_on,
						cdp.details,
						p.property_name,
						tz.time_zone_name
					FROM
						cached_dashboard_priorities cdp
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, ' . ( ( false == $objDashboardFilter->getShowDisabledData() )  ? 'true' : 'false' ) . '  ) lp ON ( lp.cid = cdp.cid AND lp.property_id = cdp.property_id )
						JOIN properties p ON ( cdp.cid = p.cid AND cdp.property_id = p.id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND p.is_disabled = 0 ' : '' ) . ' )    
						LEFT JOIN time_zones tz ON ( tz.id = p.time_zone_id  )
						LEFT JOIN events e ON ( ( cdp.details ->> \'lead_type\' = \'Answered Call\' OR cdp.details ->> \'lead_type\' = \'Unanswered Call\' OR cdp.details ->> \'lead_type\' = \'Voice Mail\' OR cdp.details ->> \'lead_type\' = \'TEXT SMS\') AND e.cid = cdp.cid AND e.id = cdp.reference_id AND e.event_result_id IS NULL AND e.is_deleted = false )
						LEFT JOIN customer_referrals cr ON ( cdp.details ->> \'lead_type\' = \'Resident Portal Referral\' AND cr.cid = cdp.cid AND cr.id = cdp.reference_id AND cr.deleted_on IS NULL )
						LEFT JOIN customer_guests cg ON ( cdp.details ->> \'lead_type\' = \'Resident Portal Guest Referral\' AND cg.cid = cdp.cid AND cg.id = cdp.reference_id AND cg.deleted_on IS NULL )
						LEFT JOIN campaign_targets ct ON ( cdp.details ->> \'lead_type\' IN ( \'Email Lead\', \'TEXT SMS\' ) AND ct.cid = cdp.cid AND ct.id = cdp.reference_id AND ct.applicant_id IS NULL AND ct.archived_on IS NULL )
					WHERE
						cdp.cid = ' . ( int ) $intCid . '
						AND cdp.module_id = ' . CModule::DASHBOARD_LEADS_POTENTIAL_LEADS . '
						AND CASE 
								WHEN ( cdp.details ->> \'lead_type\' = \'Email Lead\' ) THEN ct.id IS NOT NULL AND ct.response_email IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'TEXT SMS\' ) THEN ct.id IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'Resident Portal Referral\' ) THEN cr.id IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'Resident Portal Guest Referral\' ) THEN cg.id IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'Answered Call\' OR cdp.details ->> \'lead_type\' = \'Unanswered Call\' OR cdp.details ->> \'lead_type\' = \'Voice Mail\' ) THEN e.id IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'Email Relay Lead\' ) THEN reference_id IS NOT NULL
							END
						' . $strWhereSql . '
					ORDER BY ' . $strSortBy . '
					OFFSET  ' . ( int ) $intPageOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPotentialLeadsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $intMaxExecutionTimeOut = 0, $boolDashboardAuditReport = false ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valId( $intCid ) ) return NULL;

		$strWhereSql  = '';
		$strWhereSql .= ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND cdp.priority_id IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';
		$strWhereSql .= ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND cdp.company_employee_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' )' : '';

		if( 0 != $intMaxExecutionTimeOut ) {

			executeSql( 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ', $objDatabase );
		}

		$strSelectSql	= 'COUNT( 1 ) AS count,
						MAX( cdp.priority_id ) AS priority ';
		$strGroupBySql	= '';
		if( true == $boolDashboardAuditReport ) {
			$strSelectSql	= '
				COUNT( 1 ) AS leads_potential_leads,
				p.id AS property_id,
				p.property_name,
				cdp.priority_id AS priority ';
			$strGroupBySql	= '
				GROUP BY
					p.id,
					p.property_name,
					cdp.priority_id';
		}

		$strSql = 'SELECT
						' . $strSelectSql . '
					FROM
						cached_dashboard_priorities cdp
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'true' : 'false' ) . ' ) lp ON ( lp.cid = cdp.cid AND lp.property_id = cdp.property_id )
						JOIN properties p ON ( cdp.cid = p.cid AND cdp.property_id = p.id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND p.is_disabled = 0 ' : '' ) . ' )
						LEFT JOIN events e ON ( ( cdp.details ->> \'lead_type\' = \'Answered Call\' OR cdp.details ->> \'lead_type\' = \'Unanswered Call\' OR cdp.details ->> \'lead_type\' = \'Voice Mail\' OR cdp.details ->> \'lead_type\' = \'TEXT SMS\' ) AND e.cid = cdp.cid AND e.id = cdp.reference_id AND e.event_result_id IS NULL AND e.is_deleted = false )
						LEFT JOIN customer_referrals cr ON ( cdp.details ->> \'lead_type\' = \'Resident Portal Referral\' AND cr.cid = cdp.cid AND cr.id = cdp.reference_id AND cr.deleted_on IS NULL )
						LEFT JOIN customer_guests cg ON ( cdp.details ->> \'lead_type\' = \'Resident Portal Guest Referral\' AND cg.cid = cdp.cid AND cg.id = cdp.reference_id AND cg.deleted_on IS NULL )
						LEFT JOIN campaign_targets ct ON ( cdp.details ->> \'lead_type\' IN ( \'Email Lead\', \'TEXT SMS\' ) AND ct.cid = cdp.cid AND ct.id = cdp.reference_id AND ct.applicant_id IS NULL AND ct.archived_on IS NULL )
					WHERE
						cdp.cid = ' . ( int ) $intCid . '
						AND cdp.module_id = ' . CModule::DASHBOARD_LEADS_POTENTIAL_LEADS . '
						AND CASE
								WHEN ( cdp.details ->> \'lead_type\' = \'Email Lead\' ) THEN ct.id IS NOT NULL AND ct.response_email IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'TEXT SMS\' ) THEN ct.id IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'Resident Portal Referral\' ) THEN cr.id IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'Resident Portal Guest Referral\' ) THEN cg.id IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'Answered Call\' OR cdp.details ->> \'lead_type\' = \'Unanswered Call\' OR cdp.details ->> \'lead_type\' = \'Voice Mail\' ) THEN e.id IS NOT NULL
								WHEN ( cdp.details ->> \'lead_type\' = \'Email Relay Lead\' ) THEN reference_id IS NOT NULL
							END
						' . $strWhereSql . $strGroupBySql;

		$arrintData = fetchData( $strSql, $objDatabase );

		if( true == $boolDashboardAuditReport ) return $arrintData;

		return ( true == isset( $arrintData[0] ) ? $arrintData[0] : [ 'count' => 0, 'priority' => 1 ] );
	}

	public static function fetchEventsPotentialLeadsByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valId( $intCid ) ) return NULL;

		$strWhereSql  = '';
		$strWhereSql .= ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND cdp.priority_id IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';
		$strWhereSql .= ( true == valArr( $objDashboardFilter->getCompanyEmployeeIds() ) ) ? ' AND cdp.company_employee_id IN ( ' . implode( ',', $objDashboardFilter->getCompanyEmployeeIds() ) . ' )' : '';

		$strSql = 'SELECT
						cdp.id,
						cdp.cid,
						cdp.property_id,
						cdp.reference_id,
						cdp.details
					FROM
						cached_dashboard_priorities cdp
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ], NULL, TRUE ) lp ON ( lp.cid = cdp.cid AND lp.property_id = cdp.property_id )
						JOIN properties p ON ( cdp.cid = p.cid AND cdp.property_id = p.id AND p.is_disabled = 0 )
						LEFT JOIN time_zones tz ON ( tz.id = p.time_zone_id  )
						LEFT JOIN events e ON ( ( cdp.details ->> \'lead_type\' = \'Answered Call\' OR cdp.details ->> \'lead_type\' = \'Unanswered Call\' OR cdp.details ->> \'lead_type\' = \'Voice Mail\' OR cdp.details ->> \'lead_type\' = \'TEXT SMS\') AND e.cid = cdp.cid AND e.id = cdp.reference_id AND e.event_result_id IS NULL AND e.is_deleted = false )
					WHERE
						cdp.cid = ' . ( int ) $intCid . '
						AND cdp.module_id = ' . CModule::DASHBOARD_LEADS_POTENTIAL_LEADS . '
						AND CASE
								WHEN ( cdp.details ->> \'lead_type\' = \'Answered Call\' OR cdp.details ->> \'lead_type\' = \'Unanswered Call\' OR cdp.details ->> \'lead_type\' = \'Voice Mail\' ) THEN e.id IS NOT NULL
								ELSE false
							END
						' . $strWhereSql;

		return fetchData( $strSql, $objDatabase );
	}

}
?>
