<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationInterests
 * Do not add any new functions to this class.
 */

class CApplicationInterests extends CBaseApplicationInterests {

	public static function fetchOfferedUnitsByPropertyIdsByCid( $arrintPropertyIds, $intOccupancyTypeId, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return;

		$arrintArCodeTypeIds = array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::PAYMENT, CArCodeType::REFUND ] );

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strCheckDeletedUnitsSql 	 = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedFloorPlanSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';
		$strMilitaryMergedUnitSpaceJoinSql = '';
		$strMilitaryMergedUnitSpaceSqlSelect = '';

		if( COccupancyType::MILITARY == $intOccupancyTypeId ) {
			$strMilitaryMergedUnitSpaceJoinSql = '
				LEFT JOIN (
                              
                        WITH merged_unit_spaces AS
                              (	
                              SELECT 
                                  usu.cid,
                                  usu.group_unit_space_id,
                                  usu.unit_space_id,
                                  us.space_number,
                                  count( usu.lease_term_id )

                              FROM
                                  unit_spaces us
                                  JOIN unit_space_unions usu ON ( us.cid = usu.cid AND us.id = usu.unit_space_id )
                              WHERE
                                  us.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )   
                                  AND us.cid = ' . ( int ) $intCid . ' 
                                  
                              GROUP BY	
                                  usu.cid,
                                  usu.group_unit_space_id,
                                  usu.unit_space_id,
                                  us.space_number
                              HAVING 
                                  count( usu.lease_term_id ) = ( 
                                        SELECT	
                                            count( DISTINCT( lt.id ) )
                                        FROM
                                             property_charge_settings pcs
                                             JOIN lease_terms lt ON ( pcs.cid = lt.cid AND pcs.lease_term_structure_id = lt.lease_term_structure_id AND lt.deleted_by IS NULL AND lt.deleted_on IS NULL )
                                        WHERE
                                            lt.cid = ' . ( int ) $intCid . '
                                            AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )     
                                  
                                   )     
                              )

                              SELECT
                                  mrg.cid,                        	
                                  array_to_string(array_agg(DISTINCT mrg.space_number), \', \') as merged_unit_spaces,
                                  mrg.group_unit_space_id
                                  
                              FROM
                                  merged_unit_spaces mrg
                              GROUP BY	
                                  mrg.cid,
                                  mrg.group_unit_space_id    
                    
                    ) AS mus ON ( us.cid = mus.cid AND us.id = mus.group_unit_space_id ) 
			
			';

			$strMilitaryMergedUnitSpaceSqlSelect = '
					,
                    CASE WHEN 
                    		mus.merged_unit_spaces IS NOT NULL THEN concat( us.unit_number, \'-\', mus.merged_unit_spaces )
                         ELSE	
                         	us.unit_number_cache
                    END AS merged_unit_spaces          
			';
		}

		$strSql = 'SELECT
				    us.unit_number_cache,
					ca.application_completed_on,
				    CASE WHEN sar.id IS NOT NULL THEN
						sar.screening_recommendation_type_id
					WHEN aat.id is NOT NULL THEN
						aat.transmission_response_type_id
					ELSE
						0
					END AS screening_recommendation_type_id,ca.lease_start_date,
				    lp.move_in_date,
				    wla.wait_start_on,
				    ca.occupancy_type_id,
				    apl.id as applicant_id,
				    sub_ai.application_id,
				    sub_ai.unit_space_id,
				    sub_ai.offer_submitted_on,
				    sub_ai.property_floorplan_id,
				    us.property_id,
					sub_ai.id,
					sub_ai.details,
				    apl.name_first || \' \' || apl.name_last AS applicant_name,
					COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS contact_number,
					apl.username AS email_address,
				    pf.floorplan_name,
				    pf.number_of_bedrooms,
				    pf.number_of_bathrooms::INTEGER,
				    COALESCE ( sub_art.balance, 0 ) AS balance,
				    sub_csdt.disabilities
				    ' . $strMilitaryMergedUnitSpaceSqlSelect . '
				FROM
				    (
				      SELECT
							ai.id,
				            ai.cid,
				            ai.property_id,
				            ai.application_id,
				            ai.property_floorplan_id,
				            ai.unit_space_id,
				            ai.event_type_id,
				            ai.offer_submitted_on,
				            ai.details,
				            rank () OVER ( PARTITION BY ai.application_id, ai.unit_space_id ORDER BY ai.offer_submitted_on DESC ) AS rank
				      FROM
				          application_interests ai
				      WHERE
				          ai.cid = ' . ( int ) $intCid . '
				          AND ai.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				          AND ai.unit_space_id IS NOT NULL
				    ) AS sub_ai
				    JOIN unit_spaces us ON ( us.cid = sub_ai.cid AND us.id = sub_ai.unit_space_id AND sub_ai.rank = 1 ' . $strCheckDeletedUnitSpacesSql . ' )
				    JOIN property_units pu ON ( pu.cid = us.cid AND pu.id = us.property_unit_id AND pu.property_floorplan_id = sub_ai.property_floorplan_id ' . $strCheckDeletedUnitsSql . ' )
				    JOIN cached_applications ca ON ( ca.cid =  sub_ai.cid AND ca.id = sub_ai.application_id )
				    LEFT JOIN lease_processes lp ON ( ca.cid = lp.cid AND ca.lease_id = lp.lease_id )
				    JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id ' . $strCheckDeletedAASql . ' )
				    JOIN applicants apl ON ( apl.cid = aa.cid AND apl.id = aa.applicant_id )
				    LEFT JOIN wait_list_applications wla ON ( wla.cid = ca.cid AND wla.application_id = ca.id )
				    LEFT JOIN property_floorplans pf ON ( pf.cid = sub_ai.cid AND pf.id = sub_ai.property_floorplan_id ' . $strCheckDeletedFloorPlanSql . ' )
				    LEFT JOIN screening_application_requests sar ON ( sar.cid = ca.cid AND ca.id = sar.application_id AND sar.screening_recommendation_type_id IS NOT NULL )
					LEFT JOIN LATERAL ( SELECT MAX( t.id ) as id FROM transmissions t WHERE t.cid = ca.cid AND ca.id = t.application_id AND t.transmission_type_id = ' . CTransmissionType::SCREENING . ' ) AS t ON TRUE
                    LEFT JOIN LATERAL ( SELECT MAX( aat.id ) as id, aat.transmission_response_type_id FROM applicant_application_transmissions aat WHERE aat.cid = ca.cid AND ca.id = aat.application_id AND aat.applicant_application_id = aa.id AND t.id = aat.transmission_id GROUP BY aat.id, aat.transmission_response_type_id ) AS aat ON TRUE
				    LEFT JOIN
				    (
				      SELECT
				          SUM ( art.transaction_amount ) AS balance,
				          art.cid,
				          art.lease_id
				      FROM
				          ar_transactions art
				      WHERE
				          art.cid = ' . ( int ) $intCid . '
				          AND art.ar_code_type_id IN ( ' . implode( ',', $arrintArCodeTypeIds ) . ' )
				          AND art.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				      GROUP BY
				          art.cid,
				          art.lease_id
				    ) sub_art ON ( sub_art.cid = ca.cid AND sub_art.lease_id = ca.lease_id )
				    LEFT JOIN 
                    (
                        SELECT 
                            cid, customer_id, array_to_string( array_agg( sdt.name ), \',\') AS disabilities
                        FROM
                            customer_subsidy_disability_types csdt
                        JOIN 
                            subsidy_disability_types sdt ON ( csdt.subsidy_disability_type_id = sdt.id )
                        WHERE
                            csdt.cid = ' . ( int ) $intCid . '
                        GROUP BY 
                            customer_id, cid
                    ) sub_csdt ON ( sub_csdt.cid = aa.cid and sub_csdt.customer_id = apl.customer_id )
                    ' . $strMilitaryMergedUnitSpaceJoinSql . ' 
				WHERE
				    sub_ai.cid = ' . ( int ) $intCid . '
				    AND us.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
					AND ca.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . '
					AND sub_ai.event_type_id = ' . ( int ) CEventType::GENERATE_WAITLIST_OFFER . '
					AND ca.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . ' 
				    AND us.reserve_until IS NOT NULL
					AND ca.unit_space_id IS NULL
					AND CASE WHEN aat.id IS NOT NULL AND sar.id IS NULL THEN
                        	 aat.id = ( 
	        	                SELECT
							          MAX ( aat1.id ) OVER ( PARTITION BY aat1.applicant_application_id ) AS max_transmission_id
							      FROM
							          applicant_application_transmissions aat1
						      WHERE
							          aat1.cid = ca.cid
							          AND aat1.application_id = ca.id
							          AND aat1.applicant_application_id = aa.id
                                      LIMIT 1
	                	         )
                           ELSE
                           		TRUE
                           END
				ORDER BY ca.wait_list_position NULLS LAST, wla.wait_start_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveApplicationInterestsByApplicationIdsByPropertyIdsByCid( $arrintApplicationIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( application_id )
						*
					FROM
						application_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ')
						AND event_type_id = ' . CEventType::GENERATE_WAITLIST_OFFER . '
						AND property_floorplan_id IS NOT NULL
						AND unit_space_id IS NOT NULL
					ORDER BY
						application_id DESC,
						id DESC';

		return self::fetchApplicationInterests( $strSql, $objDatabase );
	}

	public static function fetchApplicationInterestsByApplicationIdByFloorplanIdsByPropertyIdByCid( $intApplicationId, $arrintPropertyFloorplanIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyFloorplanIds ) ) return;

		$strSql = 'SELECT
						*
					FROM
						application_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND event_type_id = ' . CEventType::WAITLIST_APPLICATION_REMOVED_BY_PROPERTY . '
						AND property_floorplan_id IN ( ' . implode( ',', $arrintPropertyFloorplanIds ) . ')
						AND unit_space_id IS NOT NULL';

		return self::fetchApplicationInterests( $strSql, $objDatabase );
	}

	public static function fetchActiveApplicationInterestByIdByCid( $intApplicationInterestId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						application_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND event_type_id = ' . CEventType::GENERATE_WAITLIST_OFFER . '
						AND id = ' . ( int ) $intApplicationInterestId . '
						AND property_floorplan_id IS NOT NULL
						AND unit_space_id IS NOT NULL';

		return self::fetchApplicationInterest( $strSql, $objDatabase );
	}

	public static function fetchActiveAddOnApplicationInterestByIdByCid( $intApplicationInterestId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						application_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND event_type_id = ' . CEventType::GENERATE_WAITLIST_OFFER . '
						AND id = ' . ( int ) $intApplicationInterestId . '
						AND add_on_id IS NOT NULL';

		return self::fetchApplicationInterest( $strSql, $objDatabase );
	}

	public static function fetchActiveApplicationInterestsCountByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						COUNT( id ) as application_interest_count
					FROM
						application_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND event_type_id = ' . CEventType::GENERATE_WAITLIST_OFFER . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND property_floorplan_id IS NOT NULL
						AND unit_space_id IS NOT NULL';

		return self::fetchColumn( $strSql, 'application_interest_count', $objDatabase );
	}

	public static function fetchUnitOffereAcceptedApplicationInterestsCountByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						COUNT( id ) as application_interest_count
					FROM
						application_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND event_type_id = ' . ( int ) CEventType::APPLICANT_ACCEPTED_WAITLIST_OFFER . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND property_floorplan_id IS NOT NULL
						AND unit_space_id IS NOT NULL';

		return self::fetchColumn( $strSql, 'application_interest_count', $objDatabase );
	}

	public static function deleteAllApplicationInterestsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'DELETE FROM application_interests
					WHERE
						cid = ' . ( int ) $intCid . '
						AND application_id = ' . ( int ) $intApplicationId;

		fetchData( $strSql, $objDatabase );

		return true;
	}

	public static function fetchCustomExpiredApplicationInterests( $objDatabase, $boolIsReturnKeyedArray = false, $boolIncludeDeletedUnitSpaces = false, $arrintOccupancyTypeIds = [] ) {

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strOccupancyTypeIdCheckSql = '';
		if( true == valArr( $arrintOccupancyTypeIds ) ) {
			$strOccupancyTypeIdCheckSql = 'AND wl.occupancy_type_id IN ( ' . implode( ',', $arrintOccupancyTypeIds ) . ' )';
		}

		$strSql = 'SELECT
						sub_ai.*,
						us.id AS unit_space_id,
						us.reserve_until
					FROM
						(
							SELECT
								ai.*,
								rank () OVER ( PARTITION BY ai.application_id, ai.unit_space_id ORDER BY ai.offer_submitted_on DESC ) AS rank
							FROM
								application_interests ai
							WHERE
								unit_space_id IS NOT NULL
								AND ai.event_type_id =' . ( int ) CEventType::GENERATE_WAITLIST_OFFER . '
						) sub_ai

						JOIN unit_spaces us ON ( us.cid = sub_ai.cid AND us.id = sub_ai.unit_space_id AND rank = 1 ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN wait_lists wl ON ( wl.cid = sub_ai.cid AND wl.property_group_id = sub_ai.property_id AND wl.is_active IS TRUE AND 0 < wl.response_days ) ' . $strOccupancyTypeIdCheckSql . '
					WHERE
						us.reserve_until IS NOT NULL
						AND ( us.reserve_until::date = NOW()::date OR us.reserve_until::date < NOW()::date )
						AND offer_submitted_on IS NOT NULL';

		return self::fetchApplicationInterests( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchMasterWaitlistByPropertyIdByCid( $objDatabase, $objWaitlisFilter, $boolIsFromRenewal = false ) {
		if( false == valId( $objWaitlisFilter->getPropertyId() ) ) return NULL;
		$strConditionalJoin = '';
		$strConditionalSubAppsSelect = '';

		if( $objWaitlisFilter->getOccupancyTypeId() == COccupancyType::MILITARY ) {
			$strConditionalJoinMilitary  = ' LEFT JOIN customer_military_details cmd ON ( apl.cid = cmd.cid AND apl.customer_id = cmd.customer_id )
									LEFT JOIN military_ranks mr ON ( mr.id = cmd.military_rank_id  )
									LEFT JOIN military_pay_grades mpg ON ( mpg.id = cmd.military_pay_grade_id )';
			$strConditionalSelectFieldsMililtary = ', mr.name AS military_rank_name, mpg.name AS military_pay_grade_name, cmd.military_status_id, cmd.eligibility_date';
			$strConditionalSubAppsSelect = 'sub_apps.military_rank_name, sub_apps.military_pay_grade_name, sub_apps.military_status_id,sub_apps.eligibility_date,';
		}

		$strApplicationFloorplanWhereSql = '';
		if( COccupancyType::AFFORDABLE == $objWaitlisFilter->getOccupancyTypeId() ) {
			$strConditionalJoin  = 'LEFT JOIN 
                    (
                        SELECT 
                            cid, customer_id, array_to_string( array_agg( sdt.name ), \',\') AS disabilities
                        FROM
                            customer_subsidy_disability_types csdt
                        JOIN 
                            subsidy_disability_types sdt ON ( csdt.subsidy_disability_type_id = sdt.id )
                        WHERE
                            csdt.cid = ' . ( int ) $objWaitlisFilter->getCid() . '
                        GROUP BY 
                            customer_id, cid
                    ) sub_csdt ON ( sub_csdt.cid = sub_apps.cid and sub_csdt.customer_id = sub_apps.customer_id ) ';
			$strConditionalSubAppsSelect = 'sub_csdt.disabilities As subsidy_disability_type_name,';
		} else {
			$strApplicationFloorplanWhereSql = ' AND ( EXISTS (
					    SELECT 1 AS floorplan_count FROM application_floorplans WHERE  CID = ca.cid AND application_id = ca.id
                         ) OR ( ca.property_floorplan_id IS NOT NULL AND ca.unit_space_id IS NULL ) )';
		}

		$strOrderBySql = '';
		if( $objWaitlisFilter->getSortColumn() == '' ) {
			if( $objWaitlisFilter->getOccupancyTypeId() == COccupancyType::MILITARY ) {
				$strOrderBySql = ' ORDER BY point_value DESC NULLS LAST,sub_apps.value DESC, sub_apps.move_in_date, sub_apps.eligibility_date NULLS LAST';
			} else {
				$strOrderBySql = 'ORDER BY sub_apps.wait_list_position NULLS LAST';
			}
			$strOrderBySql .= ', sub_apps.wait_start_on';
		} else {
			if( 'lease_start_date' == $objWaitlisFilter->getSortColumn() ) {
				$objWaitlisFilter->setSortColumn( 'sub_apps . ' . $objWaitlisFilter->getSortColumn() );
			}
			$objWaitlisFilter->setSortOrder( ( 'desc' == trim( $objWaitlisFilter->getSortOrder() ) ) ? 'DESC NULLS LAST':'ASC NULLS FIRST' );
			if( 'applicant_name' == $objWaitlisFilter->getSortColumn() ) {
				$strOrderBySql = 'ORDER BY convert_to(' . $objWaitlisFilter->getSortColumn() . ', \'SQL_ASCII\') ' . $objWaitlisFilter->getSortOrder() . ', rank ';
			} else {
				$strOrderBySql = 'ORDER BY ' . $objWaitlisFilter->getSortColumn() . ' ' . $objWaitlisFilter->getSortOrder() . ', rank ';
			}

		}

		$arrintArCodeTypeIds = array_merge( CArCodeType::$c_arrintRentAndOtherFeesArCodeTypes, [ CArCodeType::PAYMENT, CArCodeType::REFUND ] );

		$strCheckDeletedAASql = ( false == $objWaitlisFilter->getIncludeDeletedAA() ) ? ' AND aa.deleted_on IS NULL' : '';
		$strPropertyIdCondition = ( true == valArr( $objWaitlisFilter->getParentChildPropertyIds() ) ) ? implode( ',', $objWaitlisFilter->getParentChildPropertyIds() ) : $objWaitlisFilter->getPropertyId();
		$strCustomerName = ' apl.name_first || \' \' || apl.name_last AS applicant_name, ';

		if( true == $boolIsFromRenewal ) {
			$strCustomerName = ' apl.name_first || \' \' || apl.name_last || \' \' || COALESCE( apl.name_last_matronymic, \'\') AS applicant_name, ';
		}

		$strSql = 'WITH sub_ais_reason As
			(
				SELECT *
				FROM
					(
						SELECT
							ais.cid,
							ais.property_id,
							ais.application_id,
							util_get_translated(\'skip_reason\', ais.skip_reason, ais.details, \'en_US\') AS skip_reason,
							rank() OVER(PARTITION BY ais.application_id ORDER BY
										ais.created_on DESC) AS rank
						FROM
							application_interest_skips ais
						WHERE
							ais.cid = ' . ( int ) $objWaitlisFilter->getCid() . '
							AND ais.property_id IN (' . $strPropertyIdCondition . ')
					)
					As sub_ais
				where
					sub_ais.rank = 1
			)';

		$strSql .= 'SELECT ROW_NUMBER() over(
					';
		if( $objWaitlisFilter->getOccupancyTypeId() == COccupancyType::MILITARY ) {
			$strSql .= ' ORDER BY point_value DESC NULLS LAST,sub_apps.value DESC, sub_apps.move_in_date, sub_apps.eligibility_date NULLS LAST';
		} else {
			$strSql .= 'ORDER BY sub_apps.wait_list_position NULLS LAST';
		}
		$strSql .= ', sub_apps.wait_start_on ) AS rank,
					sub_apps.id AS application_id,
					sub_apps.lease_id,
					sub_ais_reason.skip_reason,
					sub_apps.application_completed_on,
					sub_apps.wait_start_on,
					sub_apps.rejection_count,
					sub_apps.move_in_date,
					sub_apps.screening_recommendation_type_id,
					sub_apps.applicant_id AS applicant_id,
					sub_apps.applicant_name,
					sub_apps.contact_number,
					sub_apps.email_address,
					sub_apps.desired_bedrooms,
					sub_apps.desired_bathrooms,
					sub_apps.customer_id,
					event_notes.notes,
					events_skip_reason.skip_reason,
					sub_apps.name AS priority,
					sub_apps.locked_on,
					' . $strConditionalSubAppsSelect . '
					CASE
						WHEN 0 >= COALESCE(ar_transaction_balance.balance, 0) THEN 0
						ELSE ar_transaction_balance.balance
					END AS balance,
					COALESCE(ar_transaction_balance.balance, 0) AS actual_balance
				FROM (
					SELECT ca.id,
						ca.lease_interval_id,
						ca.property_id,
						ca.desired_bedrooms,
						ca.desired_bathrooms,
						ca.wait_list_position,
						ca.cid,
						ca.property_floorplan_id,
						ca.id application_id,
						ca.lease_id,
						ca.application_completed_on,
						lpr.move_in_date,
						CASE WHEN sar.id IS NOT NULL THEN
								sar.screening_recommendation_type_id
							WHEN aat.id is NOT NULL THEN
								aat.transmission_response_type_id
							ELSE
								0
						END AS screening_recommendation_type_id,
						wla.wait_start_on,
						wla.rejection_count,
						apl.id AS applicant_id,
						' . $strCustomerName . '
						COALESCE(apl.phone_number, apl.mobile_number, apl.work_number, NULL) AS contact_number,
						apl.username AS email_address,
						apl.customer_id,
						wlp.name,
						wlp.value,
						ca.wait_list_points AS point_value,
						wla.locked_on
						' . $strConditionalSelectFieldsMililtary . '
					FROM
						cached_applications ca
	 					JOIN application_stage_statuses ass ON ( ca.lease_interval_type_id = ass.lease_interval_type_id AND ca.application_stage_id = ass.application_stage_id AND ca.application_status_id = ass.application_status_id )
						JOIN applicant_applications aa ON (aa.cid = ca.cid AND aa.application_id = ca.id ' . $strCheckDeletedAASql . '  AND ca.primary_applicant_id = aa.applicant_id AND aa.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . ')
						JOIN applicants apl ON (aa.cid = apl.cid AND apl.id = aa.applicant_id AND ca.id = aa.application_id )
						' . $strConditionalJoinMilitary . '
						JOIN wait_lists wl ON ( wl.cid = ca.cid AND wl.is_active IS TRUE AND ca.occupancy_type_id = wl.occupancy_type_id )
						JOIN load_properties( ARRAY[ ' . ( int ) $objWaitlisFilter->getCid() . ' ]::INTEGER[], ARRAY[ ' . $strPropertyIdCondition . ' ]::INTEGER[] ) lp ON ( wl.cid = lp.cid AND lp.property_id = wl.property_group_id AND lp.is_disabled = 0 )
						JOIN wait_list_applications wla ON ( wl.cid = wla.cid AND wl.id = wla.wait_list_id AND wla.application_id = ca.id AND wla.wait_start_on IS NOT NULL AND wla.wait_end_on IS NULL )
						LEFT JOIN wait_list_points wlp ON wlp.cid = wla.cid AND wla.wait_list_point_id = wlp.id
						LEFT JOIN screening_application_requests sar ON ( sar.cid = ca.cid AND ca.id = sar.application_id AND sar.screening_recommendation_type_id IS NOT NULL )
						LEFT JOIN LATERAL ( SELECT MAX( t.id ) as id FROM transmissions t WHERE t.cid = ca.cid AND ca.id = t.application_id AND t.transmission_type_id = ' . CTransmissionType::SCREENING . ' ) AS transmission_data ON TRUE
			            LEFT JOIN LATERAL ( SELECT MAX( aat.id ) as id, aat.transmission_response_type_id FROM applicant_application_transmissions aat WHERE aat.cid = ca.cid AND ca.id = aat.application_id AND aat.applicant_application_id = aa.id AND transmission_data.id = aat.transmission_id GROUP BY aat.id, aat.transmission_response_type_id ) AS aat ON TRUE
			            LEFT JOIN lease_processes lpr ON ( ca.cid = lpr.cid AND ca.lease_id = lpr.lease_id )
					WHERE
						ca.cid = ' . ( int ) $objWaitlisFilter->getCid() . ' AND
						ca.property_id IN ( ' . $strPropertyIdCondition . ' ) AND
						( CASE WHEN ass.id = ' . CApplicationStageStatus::PROSPECT_STAGE3_PARTIALLY_COMPLETED . ' THEN ' . CApplicationStageStatus::PROSPECT_STAGE3_COMPLETED . ' ELSE ass.id END >= wl.application_stage_status_id OR wla.is_exception IS TRUE ) AND
						ca.unit_space_id IS NULL AND
						ca.lease_status_type_id = ' . ( int ) CLeaseStatusType::APPLICANT . ' AND
						ca.occupancy_type_id = ' . ( int ) $objWaitlisFilter->getOccupancyTypeId() . $strApplicationFloorplanWhereSql . ' AND 
						ca.application_status_id NOT IN ( ' . implode( ',', [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ] ) . ' ) AND
						CASE WHEN aat.id IS NOT NULL AND sar.id IS NULL THEN
                        	 aat.id = ( 
	        	                SELECT
							          MAX ( aat1.id ) OVER ( PARTITION BY aat1.applicant_application_id ) AS max_transmission_id
							      FROM
							          applicant_application_transmissions aat1
						      WHERE
							          aat1.cid = ca.cid
							          AND aat1.application_id = ca.id
							          AND aat1.applicant_application_id = aa.id
                                      LIMIT 1
	                	         )
                           ELSE
                           		TRUE
                           END
                    ORDER BY ca.wait_list_points DESC NULLS LAST, wla.wait_start_on
					) as sub_apps
					LEFT JOIN sub_ais_reason ON (sub_ais_reason.cid = sub_apps.cid AND sub_ais_reason.application_id = sub_apps.id )
					LEFT JOIN lateral
				    (
					    SELECT
					      e.notes
					    FROM
					      events e
					    WHERE
					      e.cid = sub_apps.cid AND
					      sub_apps.property_id = e.property_id AND
					      e.lease_interval_id = sub_apps.lease_interval_id AND
					      e.event_type_id = ' . CEventType::NOTES . '
					    ORDER BY
					      id DESC
					    LIMIT
					      1
				  ) AS event_notes ON TRUE
				  LEFT JOIN lateral
				  (
					    SELECT
					      esk.event_handle AS skip_reason
					    FROM
					      events esk
					    WHERE
					      esk.cid = sub_apps.cid AND
					      sub_apps.property_id = esk.property_id AND
					      esk.lease_interval_id = sub_apps.lease_interval_id AND
					      esk.event_type_id = ' . CEventType::OFFER_SKIPPED . '
					    ORDER BY
					      esk.created_on DESC
					    LIMIT
					      1
				  ) AS events_skip_reason ON TRUE
					LEFT JOIN
					(
						SELECT SUM(art.transaction_amount) AS balance,
								art.cid,
								art.lease_id
						FROM
								ar_transactions art
						WHERE
								art.cid = ' . ( int ) $objWaitlisFilter->getCid() . ' AND
								art.property_id IN ( ' . $strPropertyIdCondition . ') AND
								art.ar_code_type_id IN ( ' . implode( ',', $arrintArCodeTypeIds ) . ')
						GROUP BY
								art.cid,
								art.lease_id
					) ar_transaction_balance ON (ar_transaction_balance.cid = sub_apps.cid AND ar_transaction_balance.lease_id = sub_apps.lease_id and ar_transaction_balance.cid = ' . ( int ) $objWaitlisFilter->getCid() . ' ) ' .
		          $strConditionalJoin .
		          $strOrderBySql;

	 	return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApplicationInterestsByApplicationIdByEventTypeIdsByCid( $intApplicationId, $arrintEventTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintEventTypeIds ) ) return NULL;

		$strSql = 'SELECT
						ai.*
				    FROM
						application_interests ai
				    WHERE
						ai.cid = ' . ( int ) $intCid . '
						AND ai.application_id = ' . ( int ) $intApplicationId . '
						AND ai.event_type_id IN ( ' . implode( ',', $arrintEventTypeIds ) . ' ) ';

		return self::fetchApplicationInterests( $strSql, $objDatabase );
	}

	public static function fetchDeclinedInterestApplicationIdsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						ai.application_id,
						ai.unit_space_id
				    FROM
						application_interests ai
				    WHERE
						ai.cid = ' . ( int ) $intCid . '
						AND ai.application_id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )
						AND ai.event_type_id IN ( ' . CEventType::APPLICANT_DECLINED_WAITLIST_OFFER . ', ' . CEventType::WAITLIST_APPLICATION_REMOVED_BY_APPLICANT . ', ' . CEventType::WAITLIST_APPLICATION_REMOVED_BY_PROPERTY . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOfferedAddOnsByPropertyIdByCid( $intSelectedPropertyId, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeDeletedUnitSpaces = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND ao.deleted_on IS NULL' : '';

		$strSql = 'SELECT
				    ca.application_completed_on,
				    ca.lease_start_date,
					ca.lease_id,
				    apl.id AS applicant_id,
				    sub_ai.application_id,
				    sub_ai.offer_submitted_on,
				    sub_ai.id,
					ao.id 	as addon_id,
					util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS addon_name,
				    apl.name_first || \' \' || COALESCE ( apl.name_middle, \'\' ) || apl.name_last AS applicant_name,
				    COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS contact_number,
				    apl.username AS email_address,
					aog.name AS add_on_group_name,
					aog.id AS add_on_group_id,
					aog.add_on_category_id AS add_on_category_id,
					aog.add_on_type_id AS add_on_type_id
					FROM
				   	 (
				      	SELECT
				          ai.id,
				          ai.cid,
				          ai.property_id,
				          ai.application_id,
				          ai.add_on_id,
				          ai.event_type_id,
				          ai.offer_submitted_on
				      FROM
				          application_interests ai
				      WHERE
				          ai.cid = ' . ( int ) $intCid . '
				          AND ai.property_id = ' . ( int ) $intSelectedPropertyId . '
				          AND ai.add_on_id IS NOT NULL
				    ) AS sub_ai
				    JOIN add_ons ao ON ( ao.cid = sub_ai.cid AND ao.id = sub_ai.add_on_id ' . $strCheckDeletedUnitSpacesSql . ' )
				    LEFT JOIN add_on_groups aog ON ( ao.cid = aog.cid AND ao.property_id = aog.property_id AND ao.add_on_group_id = aog.id )
				    JOIN cached_applications ca ON ( ca.cid = sub_ai.cid AND ca.id = sub_ai.application_id )
				    JOIN applicant_applications aa ON ( aa.cid = ca.cid AND aa.application_id = ca.id ' . $strCheckDeletedAASql . ' )
				    JOIN applicants apl ON ( apl.cid = aa.cid AND apl.id = aa.applicant_id )
				    JOIN lease_customers lc ON ( lc.cid = apl.cid AND lc.customer_id = apl.customer_id AND lc.lease_id = ca.lease_id )
				WHERE
				    sub_ai.cid = ' . ( int ) $intCid . '
				    AND sub_ai.property_id = ' . ( int ) $intSelectedPropertyId . '
				    AND lc.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
				    AND sub_ai.event_type_id = ' . ( int ) CEventType::GENERATE_WAITLIST_OFFER . '
				  	AND ao.hold_until_date IS NOT NULL
				ORDER BY
				    ca.application_completed_on,
				    ca.lease_start_date,
				    apl.name_first,
				    apl.name_last ASC';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApplicationInterestsByAddOnIdByApplicationIdByPropertyIdByCid( $intAddOnId, $intApplicationId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    ai.*
					FROM
					    application_interests ai
					WHERE
					    ai.cid = ' . ( int ) $intCid . '
					    AND ai.property_id = ' . ( int ) $intPropertyId . '
					    AND ai.application_id = ' . ( int ) $intApplicationId . '
					    AND ai.add_on_id IN (
					                           SELECT
					                               ao.id
					                           FROM
					                               add_ons ao
					                           WHERE
					                               ao.add_on_group_id =
					                               (
					                                 SELECT
					                                     ao1.add_on_group_id
					                                 FROM
					                                     add_ons ao1
					                                 WHERE
					    								 ao1.cid = ' . ( int ) $intCid . '
					    								 AND ao1.property_id = 	' . ( int ) $intPropertyId . '
					                                     AND ao1.id = ' . ( int ) $intAddOnId . '
					                                     AND ao1.deleted_on IS NULL
					                                     AND ao1.deleted_by IS NULL
					                               )
					                               AND ao.cid = ' . ( int ) $intCid . '
					    						   AND ao.property_id = ' . ( int ) $intPropertyId . '
					                               AND ao.deleted_on IS NULL
					                               AND ao.deleted_by IS NULL
					    )';

		return self::fetchApplicationInterests( $strSql, $objDatabase );
	}

	public static function fetchCustomAllApplicationInterestsByAddOnGroupIdByApplicationIdByPropertyIdByCid( $intAddOnGroupId, $intApplicationId, $intPropertyId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						ai.*
					FROM
						application_interests ai
						JOIN cached_applications ca ON ( ai.cid = ca.cid AND ai.application_id = ca.id )
						JOIN add_ons ao ON ( ao.cid = ai.cid AND ao.id = ai.add_on_id )
					WHERE
				 		ai.cid = ' . ( int ) $intCid . '
				 		AND ai.property_id = ' . ( int ) $intPropertyId . '
				 		AND ai.application_id = ' . ( int ) $intApplicationId . '
						AND ao.add_on_group_id = ' . ( int ) $intAddOnGroupId;

		return self::fetchApplicationInterests( $strSql, $objClientDatabase );
	}

}
?>