<?php

class CBudgetWorksheetItem extends CBaseBudgetWorksheetItem {

	public function valBudgetDataSourceTypeId( &$arrmixBudgetWorksheetItemErrors ) {
		$boolIsValid = true;

		if( false == valId( $this->getBudgetDataSourceTypeId() ) || CBudgetDataSourceType::SYSTEM_DATA == $this->getBudgetDataSourceTypeId() ) {
			$boolIsValid = false;
			$arrmixBudgetWorksheetItemErrors['data_source_worksheet_item_names'][] = $this->getItemName();
		}

		if( CBudgetDataSourceType::CUSTOM_FORMULAS == $this->getBudgetDataSourceTypeId() && false == valStr( $this->getFormula() ) ) {
			$boolIsValid = false;
			$arrmixBudgetWorksheetItemErrors['custom_formula_worksheet_item_names'][] = $this->getItemName();
		}

		return $boolIsValid;
	}

	public function valItemName( $arrintDeleteBudgetWorksheetItemIds, $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getItemName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'item_name', __( 'Item Name is required.' ) ) );
		} else {
			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND budget_worksheet_id = ' . ( int ) $this->getBudgetWorksheetId() . '
							AND gl_account_id = ' . ( int ) $this->getGlAccountId() . '
							AND item_name ILIKE \'' . addslashes( $this->getItemName() ) . '\'';

			if( true == valId( $this->getId() ) ) {
				$arrintDeleteBudgetWorksheetItemIds[] = $this->getId();
			}

			if( true == valIntArr( $arrintDeleteBudgetWorksheetItemIds ) ) {
				$strWhere .= ' AND id NOT IN ( ' . sqlIntImplode( $arrintDeleteBudgetWorksheetItemIds ) . ' )';
			}

			if( 0 < \Psi\Eos\Entrata\CBudgetWorksheetItems::createService()->fetchBudgetWorksheetItemCount( $strWhere, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'item_name', __( 'Same Item Name already exists.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valFormula( $arrstrBudgetWorkbookAssumptionKeys ) {
		$boolIsValid = true;

		if( false == valStr( $this->getFormula() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'formula', __( 'Custom formula is required.' ) ) );
			return false;
		}

		$objExpressionEvaluator = new CExpressionEvaluator();
		$objExpressionEvaluator->addSymbolTable( 'last', $arrstrBudgetWorkbookAssumptionKeys, true );

		if( preg_match( '/([\+\-\*\/]\s*[\+\-\*\/])/', $this->getFormula() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'formula', 'Please correct the formula.' ) );
		}

		if( false == $objExpressionEvaluator->validateExpression( $this->getFormula() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'formula', $objExpressionEvaluator->getErrorMessage() ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixParameters = [], $objDatabase = NULL, &$arrmixBudgetWorksheetItemErrors = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBudgetDataSourceTypeId( $arrmixBudgetWorksheetItemErrors );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_insert_update_items':
				$boolIsValid &= $this->valItemName( $arrmixParameters['delete_budget_worksheet_item_ids'], $objDatabase );
				break;

			case 'validate_formula':
				$boolIsValid &= $this->valFormula( $arrmixParameters['budget_workbook_assumption_keys'] );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>