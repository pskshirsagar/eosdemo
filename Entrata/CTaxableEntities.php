<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxableEntities
 * Do not add any new functions to this class.
 */

class CTaxableEntities extends CBaseTaxableEntities {

	public static function fetchCustomTaxableEntitiesByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT te.*,
						p.property_name
					FROM taxable_entities te
					JOIN properties p ON (te.cid = p.cid AND p.id = te.property_id)
					WHERE te.cid = ' . ( int ) $intCid . ' AND 
						te.deleted_by IS NULL AND
						te.deleted_on IS NULL AND
						p.is_disabled = 0';

		return self::fetchTaxableEntities( $strSql, $objDatabase );
	}

	public static function fetchAllTaxableEntitiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
					    *
					FROM
					    taxable_entities
					WHERE
					    property_id = ' . ( int ) $intPropertyId . '
					    AND cid = ' . ( int ) $intCid . '
					    AND deleted_by IS NULL';
		return self::fetchTaxableEntities( $strSql, $objDatabase );
	}

	public static function fetchAllTaxableEntitiesDataByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
                        te.id,
					    te.name,
					    te.external_property_lookup_code,
					    te.tax_code,
					    ctv.request_url
					FROM
					    taxable_entities AS te
					    JOIN property_transmission_vendors AS ptv ON ( ptv.cid = te.cid AND te.id = ptv.taxable_entity_id )
					    JOIN company_transmission_vendors AS ctv ON ( te.cid = ctv.cid AND ptv.company_transmission_vendor_id = ctv.id )
					WHERE
					    te.property_id = ' . ( int ) $intPropertyId . '
					    AND te.cid = ' . ( int ) $intCid . '
					    AND te.deleted_by IS NULL';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTaxableEntityNameByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql  = ' SELECT 
                        id, 
                        name 
                    FROM 
                        taxable_entities
                    WHERE 
                        id = ' . ( int ) $intId . '
                        AND cid = ' . ( int ) $intCid;

		$arrmixTaxableEntities = ( array ) fetchData( $strSql, $objDatabase );
		$arrstrEntities = [];
		foreach( $arrmixTaxableEntities as $arrmixTaxableEntity ) {
			$arrstrEntities[$arrmixTaxableEntity['id']] = $arrmixTaxableEntity['name'];
		}

		return $arrstrEntities;
	}

	public static function fetchTaxableEntityByNameByPropertyIdByCid( $intTaxableEntityId, $strEntityName, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
					    te.*
					FROM
					    taxable_entities te
					WHERE
					    te.cid = ' . ( int ) $intCid . '
					    AND te.property_id = ' . ( int ) $intPropertyId . '
					    AND te.id <> ' . ( int ) $intTaxableEntityId . '
					    AND te.name = \'' . $strEntityName . '\'
					    AND te.deleted_by IS NULL
					    AND te.deleted_on IS NULL';
		return self::fetchTaxableEntity( $strSql, $objDatabase );
	}

}
?>