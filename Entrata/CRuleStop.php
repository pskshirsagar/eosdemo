<?php

class CRuleStop extends CBaseRuleStop {

	protected $m_intPropertyId;

	protected $m_strRouteName;
	protected $m_strCompanyGroupName;
	protected $m_strCompanyUserNameLast;
	protected $m_strCompanyUserNameFirst;

	// Val Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client id is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valRouteId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRouteId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'route_id', __( 'Route id is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valRouteRuleId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRouteRuleId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'route_rule_id', __( 'Route rule id is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valCompanyUserAndGroup() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyGroupId() ) && true == is_null( $this->getCompanyUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_or_group_id', __( 'Rule stop approver is required.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valCompanyUserGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valRouteId();
				if( VALIDATE_UPDATE == $strAction ) {
					$boolIsValid &= $this->valRouteRuleId();
				}
				$boolIsValid &= $this->valCompanyUserAndGroup();
				break;

			case VALIDATE_DELETE:
				break;

			default:
		}

		return $boolIsValid;
	}

	// Get Functions

	public function getCompanyGroupName() {
		return $this->m_strCompanyGroupName;
	}

	public function getCompanyUserNameFirst() {
		return $this->m_strCompanyUserNameFirst;
	}

	public function getCompanyUserNameLast() {
		return $this->m_strCompanyUserNameLast;
	}

	public function getRouteName() {
		return $this->m_strRouteName;
	}

	public function getCompanyUserIsDisabled() {
		return $this->m_intCompanyUserIsDisabled;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	// Set Functions

	public function setCompanyGroupName( $strCompanyGroupName ) {
		$this->m_strCompanyGroupName = $strCompanyGroupName;
	}

	public function setCompanyUserNameFirst( $strCompanyUserNameFirst ) {
		$this->m_strCompanyUserNameFirst = $strCompanyUserNameFirst;
	}

	public function setCompanyUserNameLast( $strCompanyUserNameLast ) {
		$this->m_strCompanyUserNameLast = $strCompanyUserNameLast;
	}

	public function setRouteName( $strRouteName ) {
		$this->m_strRouteName = $strRouteName;
	}

	public function setCompanyUserIsDisabled( $intCompanyUserIsDisabled ) {
		$this->m_intCompanyUserIsDisabled = $intCompanyUserIsDisabled;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_group_name'] ) )	$this->setCompanyGroupName( $arrmixValues['company_group_name'] );
		if( true == isset( $arrmixValues['company_user_name_first'] ) )	$this->setCompanyUserNameFirst( $arrmixValues['company_user_name_first'] );
		if( true == isset( $arrmixValues['company_user_name_last'] ) )	$this->setCompanyUserNameLast( $arrmixValues['company_user_name_last'] );
		if( true == isset( $arrmixValues['route_name'] ) )	$this->setRouteName( $arrmixValues['route_name'] );
		if( true == isset( $arrmixValues['company_user_is_disabled'] ) )	$this->setCompanyUserIsDisabled( $arrmixValues['company_user_is_disabled'] );
		if( true == isset( $arrmixValues['property_id'] ) )	$this->setPropertyId( $arrmixValues['property_id'] );
		return;
	}

	// Create Functions

	public function createRuleStopResult() {
		$objRuleStopResult = new CRuleStopResult();

		$objRuleStopResult->setDefaults();
		$objRuleStopResult->setCid( $this->getCid() );
		$objRuleStopResult->setRuleStopId( $this->getId() );
		$objRuleStopResult->setPropertyId( $this->getPropertyId() );
		$objRuleStopResult->setRouteRuleId( $this->getRouteRuleId() );
		$objRuleStopResult->setOrderNum( $this->getOrderNum() );

		return $objRuleStopResult;
	}

	// Fetch Functions

	public function fetchEmailCompanyUsersByApprovalPreferenceByPropertyIds( $objApprovalPreference, $arrintPropertyIds, $objDatabase ) {

		$arrobjEmailCompanyUsers = [];
		$objCompanyGroup = NULL;

		if( true == $objApprovalPreference->getSendNotificationEmails() ) {

			if( true == valId( $this->getCompanyUserId() ) ) {

				$arrobjEmailCompanyUsers = ( array ) \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdsByIsDisabledByCid( [ $this->getCompanyUserId() ], true, $this->getCid(), $objDatabase );

			} elseif( true == valId( $this->getCompanyGroupId() ) ) {

				$objCompanyGroup			= \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupByIdByCid( $this->getCompanyGroupId(), $this->getCid(), $objDatabase );
				$arrobjEmailCompanyUsers	= ( array ) \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersByPropertyIdsByCompanyGroupIdsByCid( $arrintPropertyIds, [ $this->getCompanyGroupId() ], $this->getCid(), $objDatabase );

				$arrobjUserPreferences		= \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferencesByKeyByCid( 'STOP_USER_GROUP_EMAIL_NOTIFICATIONS',  $this->getCid(), $objDatabase );
				$arrobjUserPreferences		= ( array ) rekeyObjects( 'CompanyUserId', $arrobjUserPreferences );

				$arrobjEmailCompanyUsers	= array_diff_key( $arrobjEmailCompanyUsers, $arrobjUserPreferences );
			}
		}

		return [ $arrobjEmailCompanyUsers, $objCompanyGroup ];
	}

	public function fetchEmailCompanyUsersByAllRuleStopsOfReferenceByPropertyIds( $arrintRuleStopIds, $arrintCompanyUserIds, $arrintCompanyGroupIds, $arrintPropertyIds, $objDatabase ) {

		$arrobjEmailCompanyUsers = $arrobjEmailGroupCompanyUsers = $arrobjEmailCompanyEmployeeRelievers = [];

		if( true == valArr( $arrintCompanyUserIds ) ) {
			$arrobjEmailCompanyUsers = ( array ) \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdsByIsDisabledByCid( $arrintCompanyUserIds, true, $this->getCid(), $objDatabase );
		}

		if( true == valArr( $arrintCompanyGroupIds ) ) {
			$arrobjEmailGroupCompanyUsers	= ( array ) \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUsersByPropertyIdsByCompanyGroupIdsByCid( $arrintPropertyIds, $arrintCompanyGroupIds, $this->getCid(), $objDatabase );
		}
		if( true == valArr( $arrintRuleStopIds ) ) {
			$arrobjEmailCompanyEmployeeRelievers = ( array ) \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeRelieverByRuleStopIdsByCid( $arrintRuleStopIds, $this->getCid(), $objDatabase );
		}

		$arrobjEmailCompanyUsers = rekeyObjects( 'Id', array_merge( $arrobjEmailCompanyUsers, $arrobjEmailGroupCompanyUsers ) );

		return [ $arrobjEmailCompanyUsers, $arrobjEmailCompanyEmployeeRelievers ];
	}

}
?>