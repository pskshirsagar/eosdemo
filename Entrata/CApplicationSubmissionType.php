<?php

class CApplicationSubmissionType extends CBaseApplicationSubmissionType {

	const APPLICATION		= 1;
	const LEASE				= 2;
	const POLICY_DOCUMENT	= 3;
}
?>