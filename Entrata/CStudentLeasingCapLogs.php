<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStudentLeasingCapLogs
 * Do not add any new functions to this class.
 */

class CStudentLeasingCapLogs extends CBaseStudentLeasingCapLogs {

	public static function fetchStudentLeasingCapLogDetailsByStudentLeasingCapIdByCid( $intStudentLeasingCapId, $intCid, $objDatabase ) {
		if( false == valId( $intStudentLeasingCapId ) || false == valId( $intCid ) ) {
			return;
		}
		$strSql = 'SELECT
						slcl.id,
						slcl.cap_amount,
						( CASE WHEN slcl.is_active = true THEN \'' . __( 'Enabled' ) . '\' ELSE \'' . __( 'Disabled' ) . '\' END ) AS is_active,
						slcl.created_by,
						to_char ( slcl.created_on, \'MM/DD/YYYY\' ) AS created_on,
						ce.name_first,
						ce.name_last,
						cu.username
					FROM
						student_leasing_cap_logs slcl
						LEFT JOIN company_users cu ON ( slcl.cid = cu.cid AND slcl.updated_by = cu.id )
						LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE
						slcl.student_leasing_cap_id = ' . ( int ) $intStudentLeasingCapId . '
						AND slcl.cid = ' . ( int ) $intCid . '
					ORDER BY
						slcl.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>