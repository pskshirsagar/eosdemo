<?php

class CDataExportFrequency extends CBaseDataExportFrequency {

	protected $m_strExistingStartedOn;

	/**
	 * Get Functions
	 */

	public function getExistingStartedOn() {
		return $this->m_strExistingStartedOn;
	}

	/**
	 * Set Functions
	 */

	public function setExistingStartedOn( $strExistingStartedOn ) {
		$this->m_strExistingStartedOn = $strExistingStartedOn;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataExportFrequencyTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getDataExportFrequencyTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'data_export_frequency_type_id', 'Repeat is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valEvery() {
		$boolIsValid = true;

		if( true == valStr( $this->getDataExportFrequencyTypeId() ) && CDataExportFrequencyType::SCHEDULED_EXPORT_FREQUENCY_TYPE_ONCE != $this->getDataExportFrequencyTypeId() && false == valStr( $this->getEvery() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'every', 'Every is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valScheduledDays() {
		$boolIsValid = true;

		$arrintDataExportFrequencyTypeIds = [ CDataExportFrequencyType::SCHEDULED_EXPORT_FREQUENCY_TYPE_WEEKLY, CDataExportFrequencyType::SCHEDULED_EXPORT_FREQUENCY_TYPE_WEEKDAY, CDataExportFrequencyType::SCHEDULED_EXPORT_FREQUENCY_TYPE_MONTHLY ];

		if( true == in_array( $this->getDataExportFrequencyTypeId(), $arrintDataExportFrequencyTypeIds ) && false == valStr( $this->getScheduledDays() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'scheduled_days', 'Day(s) is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStartOn() {

		if( CDataExportFrequencyType::SCHEDULED_EXPORT_FREQUENCY_TYPE_ONCE == $this->getDataExportFrequencyTypeId() ) return true;

		$boolIsValid = true;

		if( false == $this->getStartOnNextScheduledDay() && false == valStr( $this->getStartOn() ) ) {

			$this->addErrorMsg( new CErrorMsg( NULL, 'start_on', 'Start on is required.' ) );
			$boolIsValid = false;

		} elseif( false == $this->getStartOnNextScheduledDay() && strtotime( $this->getStartOn() ) < strtotime( date( 'm/d/Y', time() ) ) ) {

			$this->addErrorMsg( new CErrorMsg( NULL, 'start_on', 'Start on date is not allowed from past.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valExistingStartedOn() {

		if( strtotime( $this->getStartOn() ) < strtotime( $this->getExistingStartedOn() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'start_on', 'New StartOn date should not be less than existing StartOn date.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStartOnNextScheduledDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDataExportFrequencyTypeId();
				$boolIsValid &= $this->valEvery();
				$boolIsValid &= $this->valScheduledDays();
				$boolIsValid &= $this->valStartOn();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDataExportFrequencyTypeId();
				$boolIsValid &= $this->valEvery();
				$boolIsValid &= $this->valScheduledDays();
				$boolIsValid &= $this->valExistingStartedOn();
				break;

			case VALIDATE_DELETE:
				break;

			default:
		}

		return $boolIsValid;
	}

}
?>