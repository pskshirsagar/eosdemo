<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTaskEmails
 * Do not add any new functions to this class.
 */

class CScheduledTaskEmails extends CBaseScheduledTaskEmails {

	public static function fetchScheduledTaskEmailsByIdsByCid( $arrintScheduledTaskEmailIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScheduledTaskEmailIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						scheduled_task_emails
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintScheduledTaskEmailIds ) . ' )';

		return self::fetchScheduledTaskEmails( $strSql, $objDatabase );
	}

	public static function fetchScheduledTaskEmailByIdByCid( $intId, $intCid, $objDatabase ) {
		if( false == is_numeric( $intId ) || false == is_numeric( $intCid ) ) return NULL;

		return self::fetchScheduledTaskEmail( sprintf( 'SELECT * FROM scheduled_task_emails WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );
	}

}
?>