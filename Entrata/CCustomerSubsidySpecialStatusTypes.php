<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidySpecialStatusTypes
 * Do not add any new functions to this class.
 */

class CCustomerSubsidySpecialStatusTypes extends CBaseCustomerSubsidySpecialStatusTypes {

	public static function fetchActiveCustomerSubsidySpecialStatusTypesByCustomerIdByCId( $intCustomerId, $intCid, $objDatabase ) {

		if ( false == valId( $intCustomerId ) )
			return NULL;

		$strSql = ' SELECT
						*
					FROM
						customer_subsidy_special_status_types
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerSubsidySpecialStatusTypes( $strSql, $objDatabase );
	}

	public static function fetchActiveCustomerSubsidySpecialStatusTypesByCustomerIdsBySubsidySpecialStatusTypeIdByCId( $arrintCustomerIds, $intSubsidySpecialStatusTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) )
			return NULL;

		if( false == valId( $intSubsidySpecialStatusTypeId ) )
			return NULL;

		$strSql = ' SELECT
						*
					FROM
						customer_subsidy_special_status_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )
						AND subsidy_special_status_type_id = ' . ( int ) $intSubsidySpecialStatusTypeId;

		return self::fetchCustomerSubsidySpecialStatusTypes( $strSql, $objDatabase );
	}

	public static function fetchCustomerSubsidySpecialStatusTypesByCustomerIdByCId( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						cs.subsidy_special_status_type_id
					FROM
						customer_subsidy_special_status_types cs
					WHERE
						cs.cid = ' . ( int ) $intCid . '
						AND cs.customer_id = ' . ( int ) $intCustomerId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerSubsidySpecialStatusTypeIdsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT cssst.subsidy_special_status_type_id
					FROM
						customer_subsidy_special_status_types cssst
						JOIN applicants a ON ( a.cid = cssst.cid AND a.customer_id = cssst.customer_id )
						JOIN applicant_applications aa ON ( a.cid = aa.cid AND a.id = aa.applicant_id AND aa.deleted_by IS NULL )
					WHERE
						cssst.cid = ' . ( int ) $intCid . '
						AND aa.application_id = ' . ( int ) $intApplicationId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerSubsidySpecialStatusTypesByCustomerIdsByCId( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						customer_subsidy_special_status_types
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )';

		return self::fetchCustomerSubsidySpecialStatusTypes( $strSql, $objDatabase );
	}

}
?>