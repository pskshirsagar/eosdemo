<?php
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CPropertyBuildingAmenities;
use Psi\Eos\Entrata\CPropertyBuildings;
use Psi\Eos\Entrata\CPropertyFloors;

class CPropertyBuilding extends CBasePropertyBuilding {

	protected $m_intAmenitiesCount;
	protected $m_intMediasCount;
	protected $m_intFloorsCount;
	protected $m_fltLoadFactor;
	protected $m_strTaxId;

	// Other Functions

	public function isAssociatedWithFloorsOrUnits( $objDatabase ) {

		$boolIsAssociatedWithFloorsOrUnits = false;

		$arrobjPropertyFloors	= CPropertyFloors::createService()->fetchPropertyFloorsByPropertyIdByPropertyBuildingIdByCid( $this->m_intPropertyId, $this->m_intId, $this->m_intCid,  $objDatabase );
		$arrobjPropertyUnits	= \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitsByPropertyIdByPropertyBuildingIdByCid( $this->m_intPropertyId,  $this->m_intId, $this->m_intCid, $objDatabase );

		if( ( true == valArr( $arrobjPropertyFloors ) ) && ( false == valArr( $arrobjPropertyUnits ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'This building is associated to floors,  please disassociate before removing.' ) ) );
			$boolIsAssociatedWithFloorsOrUnits = true;
		} elseif( ( true == valArr( $arrobjPropertyUnits ) ) && ( false == valArr( $arrobjPropertyFloors ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'This building is associated to units, please disassociate before removing.' ) ) );
			$boolIsAssociatedWithFloorsOrUnits = true;
		} elseif( ( true == valArr( $arrobjPropertyUnits ) ) && ( true == valArr( $arrobjPropertyFloors ) ) ) {
		 	$this->addErrorMsg( new CErrorMsg( NULL, 'website_domain', __( 'This building is associated to units and floors, please disassociate before removing.' ) ) );
		 	$boolIsAssociatedWithFloorsOrUnits = true;
		}

		return $boolIsAssociatedWithFloorsOrUnits;
	}

	// Set Functions

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['amenities_count'] ) ) {
			$this->setAmenitiesCount( $arrmixValues['amenities_count'] );
		}
		if( true == isset( $arrmixValues['medias_count'] ) ) {
			$this->setMediasCount( $arrmixValues['medias_count'] );
		}

		if( true == isset( $arrmixValues['tax_id'] ) ) {
			$this->setTaxId( $arrmixValues['tax_id'] );
		}
		if( true == isset( $arrmixValues['floors_count'] ) ) {
			$this->setFloorsCount( $arrmixValues['floors_count'] );
		}
	}

	public function setAmenitiesCount( $intAmenitiesCount ) {
		$this->m_intAmenitiesCount = CStrings::strToIntDef( $intAmenitiesCount, NULL, false );
	}

	public function setMediasCount( $intMediasCount ) {
		$this->m_intMediasCount = CStrings::strToIntDef( $intMediasCount, NULL, false );
	}

	public function setLoadFactor( $fltLoadFactor ) {
		$this->m_fltLoadFactor = CStrings::strToFloatDef( $fltLoadFactor, NULL, false );
	}

	public function setTaxId( $strTaxId ) {
		$this->m_strTaxId = $strTaxId;
	}

	public function setFloorsCount( $intFloorsCount ) {
		$this->m_intFloorsCount = $intFloorsCount;
	}

	// Get Functions

	public function getAmenitiesCount() {
		return $this->m_intAmenitiesCount;
	}

	public function getMediasCount() {
		return $this->m_intMediasCount;
	}

	public function getLoadFactor() {
		return $this->m_fltLoadFactor;
	}

	public function getTaxId() {
		return $this->m_strTaxId;
	}

	public function getFloorsCount() {
		return $this->m_intFloorsCount;
	}

	public function getMaskedTaxId() {
		if( 4 == strlen( $this->getTaxId() ) ) {
			return $this->getTaxId();
		} else {
			return CEncryption::maskText( $this->getTaxId() );
		}
	}

	// Create Functions

	public function createRate() {

		$objRate = new CRate();
		$objRate->setCid( $this->m_intCid );
		$objRate->setRateTypeId( CRateType::PROPERTY_BUILDING );
		$objRate->setReferenceId( $this->getId() );

		return $objRate;
	}

	public function createPropertyBuildingAddress() {
		$objPropertyBuildingAddress = new CPropertyBuildingAddress();
		$objPropertyBuildingAddress->setCid( $this->getCid() );
		$objPropertyBuildingAddress->setPropertyId( $this->getPropertyId() );
		$objPropertyBuildingAddress->setPropertyBuildingId( $this->getId() );

		return $objPropertyBuildingAddress;
	}

	public function createPropertyBuildingMedia() {
		$objPropertyBuildingMedia = new CPropertyBuildingMedia();
		$objPropertyBuildingMedia->setCid( $this->getCid() );
		$objPropertyBuildingMedia->setPropertyId( $this->getPropertyId() );
		$objPropertyBuildingMedia->setPropertyBuildingId( $this->getId() );

		return $objPropertyBuildingMedia;
	}

	public function createPropertyBuildingAmenity() {
		$objPropertyBuildingAmenity = new CPropertyBuildingAmenity();
		$objPropertyBuildingAmenity->setCid( $this->getCid() );
		$objPropertyBuildingAmenity->setPropertyId( $this->getPropertyId() );
		$objPropertyBuildingAmenity->setPropertyBuildingId( $this->getId() );

		return $objPropertyBuildingAmenity;
	}

	public function createTaxIdAssociation() {
		$objTaxIdAssociation = new CTaxIdAssociation();
		$objTaxIdAssociation->setCid( $this->getCid() );
		$objTaxIdAssociation->setPropertyId( $this->getPropertyId() );
		$objTaxIdAssociation->setPropertyBuildingId( $this->getId() );

		return $objTaxIdAssociation;
	}

	// Fetch Functions

	public function fetchCompanyMediaFileByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByPropertyBuildingIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloors( $objDatabase ) {
		return CPropertyFloors::createService()->fetchPropertyFloorsByPropertyIdByPropertyBuildingIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryAddress( $objDatabase ) {
		return CPropertyBuildingAddresses::fetchPropertyBuildingAddressByAddressTypeIdByPropertyBuildingIdByCid( CAddressType::PRIMARY, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAddresses( $objDatabase ) {
		return CPropertyBuildingAddresses::fetchPropertyBuildingAddressesByPropertyBuildingIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingAddressById( $intPropertyBuildingAddressId, $objDatabase ) {
		return CPropertyBuildingAddresses::fetchPropertyBuildingAddressByIdByPropertyBuildingIdByCid( $intPropertyBuildingAddressId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFileByMediaTypeIdByBuildingMedia( $intMediaTypeId, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFileByPropertyBuildingIdByPropertyIdByMediaTypeIdByCid( $this->getId(), $this->getPropertyId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFilesByMediaTypeIds( $arrintMediaTypeIds, $objDatabase ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByPropertyIdByPropertyBuildingIdByMediaTypeIdsByCId( $this->getPropertyId(), $this->getId(), $arrintMediaTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingMedia( $intCompanyMediaFileId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyBuildingMedias::createService()->fetchPropertyBuildingMediaByPropertyIdByPropertyBuildingIdByMediaFileIdByCid( $this->getPropertyId(), $this->getId(), $intCompanyMediaFileId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingMedias( $arrintCompanyMediaFileIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyBuildingMedias::createService()->fetchPropertyBuildingMediasByPropertyIdByPropertyBuildingIdByMediaFileIdsByCid( $this->getPropertyId(), $this->getId(), $arrintCompanyMediaFileIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingAmenities( $objDatabase ) {
		return CPropertyBuildingAmenities::createService()->fetchPropertyBuildingAmenitiesByPropertyIdByPropertyBuildingIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingAmenityById( $intPropertyBuildingAmenityId, $objDatabase ) {
		return CPropertyBuildingAmenities::createService()->fetchPropertyBuildingAmenityByIdByPropertyBuildingIdByCid( $intPropertyBuildingAmenityId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyBuildingAmenitiesByAmenityIds( $arrintAmenityIds, $objDatabase ) {
		return CPropertyBuildingAmenities::createService()->fetchPropertyBuildingAmenitiesByAmenityIdsByPropertyBuildingIdByPropertyIdByCid( $arrintAmenityIds, $this->getId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchSimpleAmenities( $objDatabase, $intPageNo, $intPageSize, $boolIsFetchCount = false ) {
		return CPropertyBuildingAmenities::createService()->fetchSimplePropertyBuildingAmenitiesByPropertyIdByPropertyBuildingIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $objDatabase, $intPageNo, $intPageSize, $boolIsFetchCount );
	}

	public function fetchActiveMarketingMediaAssociationsByMediaSubTypeIds( $arrintMarketingMediaSubTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByMediaTypeIdByMediaSubTypeIdsByCidOrderByOrderNum( $this->getId(), CMarketingMediaType::PROPERTY_BUILDING_MEDIA, $arrintMarketingMediaSubTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchMarketingMediaAssociationOrderNumByMediaSubTypeIds( $arrintMarketingMediaSubTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchMarketingMediaAssociationOrderNumByMediaTypeIdByMediaSubTypeIds( CMarketingMediaType::PROPERTY_BUILDING_MEDIA, $arrintMarketingMediaSubTypeIds, $objDatabase );
	}

	// Validation Functions

	public function valBuildingName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getBuildingName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'building_name', __( 'You must enter a building name.' ) ) );
			return $boolIsValid;
		}

		$arrobjProperty = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdsByCid( ( array ) $this->getPropertyId(), $this->getCid(), $objDatabase );
		$objProperty	= array_pop( $arrobjProperty );

		if( true == valObj( $objProperty, 'CProperty' ) && true == valArr( $objProperty->getOccupancyTypeIds() ) && true == in_array( COccupancyType::COMMERCIAL, $objProperty->getOccupancyTypeIds() ) ) {
			$intCountSameBuildingNames = CPropertyBuildings::createService()->fetchPropertyBuildingsCountByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objDatabase, $this->getBuildingName(), $this->getId() );
			if( $intCountSameBuildingNames < 1 ) {
				return $boolIsValid;
			}
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'building_name', __( 'Building name already exists.' ) ) );
			return $boolIsValid;
		}

		if( true == valObj( $objProperty, 'CProperty' ) && CPropertyType::SUBSIDIZED == $objProperty->getPropertyTypeId() ) {
			$arrobjPropertySubsidyTypes = ( array ) rekeyObjects( 'SubsidyTypeId', \Psi\Eos\Entrata\CPropertySubsidyTypes::createService()->fetchPropertySubsidyTypesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(),  $objDatabase ) );

			if( true == array_key_exists( CSubsidyType::HUD, $arrobjPropertySubsidyTypes ) && false == preg_match( '/^[a-zA-Z0-9 ]{1,19}$/', $this->getBuildingName() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'building_name', __( 'Building name should be less than or equal to 19 alphanumeric characters required for properties with subsidy type HUD' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valBuildingSquareFeet( $objDatabase, $strUnitOfMeasureForArea ) {
		$boolIsValid = true;

		if( false === is_numeric( $this->getTotalSquareFeet() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_square_feet', __( 'The Total {%s, 0} required.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getTotalSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_square_feet', __( 'The Total {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getUsableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usable_square_feet', __( 'The Usable {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( 0 > $this->getRentableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet', __( 'The Total rentable {%s, 0} must be non-negative.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getUsableSquareFeet() > $this->getTotalSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usable_square_feet', __( 'The Usable {%s, 0} cannot exceed Total {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getUsableSquareFeet() > $this->getRentableSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet_total', __( 'The Usable {%s, 0} cannot exceed Rentable {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( $this->getRentableSquareFeet() > $this->getTotalSquareFeet() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet', __( 'The Rentable {%s, 0} cannot exceed Total {%s, 0}.', [ $strUnitOfMeasureForArea ] ) ) );
		}

		if( true === valId( $this->getId() ) ) {
			$arrmixBuildingAllotedSqftDetails = \Psi\Eos\Entrata\CPropertyFloors::createService()->fetchSumOfTotalSqftOfPropertyFloorsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
			$arrmixBuildingAllotedSqftDetails = rekeyArray( 'property_building_id', $arrmixBuildingAllotedSqftDetails );

			if( true === valArr( $arrmixBuildingAllotedSqftDetails[$this->getId()] ) && ( int ) $this->getTotalSquareFeet() < ( int ) $arrmixBuildingAllotedSqftDetails[$this->getId()]['floors_square_feet'] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_square_feet', __( 'The Total {%s, 1} cannot be less than allocated floors Total {%s, 1}: {%f, 0, p:2}.', [ $arrmixBuildingAllotedSqftDetails[$this->getId()]['floors_square_feet'], $strUnitOfMeasureForArea ] ) ) );
			}

			if( true === valArr( $arrmixBuildingAllotedSqftDetails[$this->getId()] ) && ( int ) $this->getUsableSquareFeet() < ( int ) $arrmixBuildingAllotedSqftDetails[$this->getId()]['floors_usable_square_feet'] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'usable_square_feet', __( 'The Usable {%s, 1} cannot be less than allocated floors Usable {%s, 1}: {%f, 0, p:2}.', [ $arrmixBuildingAllotedSqftDetails[$this->getId()]['floors_usable_square_feet'], $strUnitOfMeasureForArea ] ) ) );
			}

			if( true === valArr( $arrmixBuildingAllotedSqftDetails[$this->getId()] ) && ( int ) $this->getRentableSquareFeet() < ( int ) $arrmixBuildingAllotedSqftDetails[$this->getId()]['floors_rentable_square_feet'] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rentable_square_feet', __( 'The Rentable {%s, 1} cannot be less than allocated floors Rentable {%s, 1}: {%f, 0, p:2}.', [ $arrmixBuildingAllotedSqftDetails[$this->getId()]['floors_rentable_square_feet'], $strUnitOfMeasureForArea ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $strUnitOfMeasureForArea = 'square feet' ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBuildingName( $objDatabase );
				break;

			case 'VALIDATE_COMMERCIAL_BUILDING_INSERT':
			case 'VALIDATE_COMMERCIAL_BUILDING_UPDATE':
				$boolIsValid &= $this->valBuildingName( $objDatabase );
				$boolIsValid &= $this->valBuildingSquareFeet( $objDatabase, $strUnitOfMeasureForArea );
				break;

			case VALIDATE_DELETE:
				if( true == $this->isAssociatedWithFloorsOrUnits( $objDatabase ) ) {
					$boolIsValid &= false;
				} else {
					$boolIsValid &= true;
				}
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

}
?>