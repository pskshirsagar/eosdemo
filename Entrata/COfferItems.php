<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COfferItems
 * Do not add any new functions to this class.
 */

class COfferItems extends CBaseOfferItems {

	public static function fetchOfferItemsByOfferIdsByCid( $arrintOfferIds, $intCid, $objDatabase ) {

		if( false === valArr( $arrintOfferIds ) ) return NULL;

		$strSql = 'SELECT 
						oi.*
					FROM 
						offer_items oi
					WHERE 
						oi.offer_id IN ( ' . sqlIntImplode( $arrintOfferIds ) . ' )
					    AND oi.cid = ' . ( int ) $intCid . '
				        AND oi.deleted_by IS NULL ';

		return self::fetchOfferItems( $strSql, $objDatabase );
	}

	public static function fetchCustomOfferItemByIdByCid( $intOfferItemId, $intCid, $objDatabase ) {

		if( false === valId( $intOfferItemId ) ) return NULL;

		$strSql = 'SELECT 
						oi.id,
						oi.offer_id,
						oi.cid,
						oi.ar_origin_reference_id,
						oi.ar_origin_id,
						s.name,
						s.description,
						s.special_type_id,
						s.gift_value,
						s.is_auto_fulfilled
					FROM 
						offer_items oi
						JOIN specials s ON( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id )
					WHERE 
						oi.id = ' . $intOfferItemId . '
					    AND oi.cid = ' . ( int ) $intCid . '
				        AND oi.deleted_by IS NULL ';

		return self::fetchOfferItem( $strSql, $objDatabase );
	}

	public static function fetchCustomOfferItemLogsByOfferTemplateIdByCid( $intOfferTemplateId, $intCid, $objDatabase ) {

		if( false === valId( $intOfferTemplateId ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						(
							SELECT
								DATE ( oil.created_on ) as created_date,
								oil.created_by as user_id,
								initcap( oi.name ) ||\'(\' || util_get_translated( \'name\', st.name, st.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) ||\')\' as offer_item_name,
								COALESCE( initcap( ce.name_first || \' \' || ce.name_last ), cu.username ) as user_name,
								oil.*,
								rank ( ) OVER ( PARTITION BY DATE ( oil.created_on ),oil.created_by
							ORDER BY
								oil.created_on DESC, oil.id DESC ) AS rank
							FROM
								offer_item_logs oil
								JOIN offer_items oi ON ( oi.cid = oil.cid AND oi.id = oil.offer_item_id )
								JOIN specials s ON ( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id )
								JOIN special_types st ON ( st.id = s.special_type_id )
								JOIN offers o ON ( o.cid = oi.cid AND o.id = oi.offer_id)
								JOIN offer_templates ot ON ( ot.cid = o.cid AND ot.id = o.offer_template_id )
								LEFT JOIN company_users cu ON ( cu.cid = oi.cid AND cu.id = oil.created_by )
								LEFT JOIN company_employees ce ON (cu.cid = ce.cid AND cu.company_employee_id = ce.id )
							WHERE
								oil.cid = ' . ( int ) $intCid . '
								AND ot.id = ' . ( int ) $intOfferTemplateId . '
								AND st.id IN ( ' . CSpecialType::STANDARD . ', ' . CSpecialType::GIFT_INCENTIVE . ' )
						) AS sub
					ORDER BY
						sub.created_on DESC, sub.created_by';

		$arrmixOfferItemLogs = fetchData( $strSql, $objDatabase );

		return $arrmixOfferItemLogs;
	}

	public static function fetchOfferItemsBySpecialTypeIdsByPropertyIdByCid( $arrintSpecialTypeIds, $intPropertyId, $intCid, $objDatabase, $intOfferId = NULL ) {

		if( false === valArr( $arrintSpecialTypeIds ) && false === valId( $intPropertyId ) ) return NULL;
		$strCondition = '';
		if( true === valId( $intOfferId ) ) {
		 $strCondition = ' o.id <> ' . $intOfferId . ' AND ';
		}

		$strSql = 'SELECT
						DISTINCT (oi.id),
						oi.*,
						s.special_type_id,
						s.gift_value,
					    ra.hide_description
					FROM
						offer_items oi
					    JOIN offers o ON( oi.cid = o.cid AND oi.offer_id = o.id )
					    JOIN specials s ON ( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id )
					    LEFT JOIN rate_associations ra ON (ra.cid = s.cid AND ra.ar_origin_reference_id = s.id)
					    LEFT JOIN rates r ON (r.cid = ra.cid AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id AND r.ar_origin_reference_id =
					       ra.ar_origin_reference_id AND ra.property_id = r.property_id)
					WHERE ra.cid = ' . ( int ) $intCid . ' AND
					      ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND
					      s.special_type_id IN ( ' . sqlIntImplode( $arrintSpecialTypeIds ) . ' ) AND
					      s.deleted_on IS NULL AND
					      ' . $strCondition . '
					      ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . ' AND
					      (o.end_date IS NULL or
					      o.end_date > NOW());  ';

		return self::fetchOfferItems( $strSql, $objDatabase );
	}

	public static function fetchOfferItemsByOfferIdsBySpecialTypeIdsByCid( $arrintOfferIds, $arrintSpecialTypeIds, $intCid, $objDatabase ) {

		if( false === valArr( $arrintOfferIds ) ) return NULL;

		$strSql = 'SELECT
						oi.*
					FROM
						offer_items oi
						JOIN specials s ON ( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id )
					WHERE
						oi.offer_id IN ( ' . sqlIntImplode( $arrintOfferIds ) . ' )
					    AND oi.cid = ' . ( int ) $intCid . '
					    AND s.special_type_id IN ( ' . sqlIntImplode( $arrintSpecialTypeIds ) . ' )
				        AND oi.deleted_by IS NULL ';

		return self::fetchOfferItems( $strSql, $objDatabase );
	}

	public static function fetchCustomOfferItemsByOfferIdsByCid( $arrintOfferIds, $intCid, $objDatabase ) {

		if( false === valArr( $arrintOfferIds ) ) {
			return NULL;
		}

		$strSql = '
				WITH offers_having_gift_insentive AS (                          
					SELECT oi.*,
					       s.id AS special_id,
					       s.special_type_id
					FROM offers o
					     JOIN offer_templates ot ON (o.cid = ot.cid AND o.offer_template_id = ot.id)
					     JOIN offer_items oi ON (o.cid = oi.cid AND o.id = oi.offer_id AND oi.deleted_by IS NULL)
					     JOIN specials s ON (oi.cid = s.cid AND oi.ar_origin_reference_id = s.id AND s.deleted_by IS NULL AND s.special_type_id IN( ' . CSpecialType::STANDARD . ', ' . CSpecialType::GIFT_INCENTIVE . ' ) )
					WHERE o.cid = ' . ( int ) $intCid . '
					      AND o.deleted_by IS NULL
					      AND o.deleted_on IS NULL
					      AND o.id IN ( ' . sqlIntImplode( $arrintOfferIds ) . ' )
			      )
			      
			      SELECT 
			             ohgi.*,
			             s.id AS tier_special_id       
			      FROM 
			           offer_items oi
			           JOIN offers_having_gift_insentive ohgi ON ( oi.cid = ohgi.cid AND oi.offer_id = ohgi.offer_id )
			           JOIN specials s ON (oi.cid = s.cid AND oi.ar_origin_reference_id = s.id AND s.deleted_by IS NULL AND s.special_type_id = ' . CSpecialType::RENEWAL_TIER . ' )
			      WHERE oi.deleted_by IS NULL
			            AND oi.deleted_on IS NULL';

		return self::fetchOfferItems( $strSql, $objDatabase );
	}

}
?>