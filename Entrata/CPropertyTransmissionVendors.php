<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyTransmissionVendors
 * Do not add any new functions to this class.
 */

class CPropertyTransmissionVendors extends CBasePropertyTransmissionVendors {

	public static function fetchAllPropertyTransmissionVendorsByPropertyIdsByTransmissionVendorIdByCid( $arrintPropertyIds, $intTransmissionVendorId = NULL, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ptv.*
				  	FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
				  	WHERE
						ctv.cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		$strSql .= ( false == is_null( $intTransmissionVendorId ) ) ? ' AND ctv.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId : '';

		return self::fetchPropertyTransmissionVendors( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_transmission_vendors WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchPropertyTransmissionVendors( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorByPropertyIdsByTransmissionVendorIdByTransmissionTypeIdByCid( $arrintPropertyId, $intTransmissionVendorId = NULL, $intTransmissionTypeId = NULL, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyId ) ) return NULL;

        $strSql = 'SELECT
						ptv.key,
						ptv.property_id as property_id
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						ptv.property_id in ( ' . implode( ',', $arrintPropertyId ) . ' )
						AND ptv.cid = ' . ( int ) $intCid;

		$strSql .= ( false == is_null( $intTransmissionVendorId ) ) ? ' AND ctv.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId : '';
		$strSql .= ( false == is_null( $intTransmissionTypeId ) ) ? ' AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId : '';

		$arrobjPropertyTransmissionVendors = self::fetchPropertyTransmissionVendors( $strSql, $objDatabase );

        if( false == valArr( $arrobjPropertyTransmissionVendors ) ) return NULL;

        $arrobjPropertyTransmissionVendorsRekeyedByPropertyId = [];

        foreach( $arrobjPropertyTransmissionVendors as $arrobjPropertyTransmissionVendor ) {
			$arrobjPropertyTransmissionVendorsRekeyedByPropertyId[$arrobjPropertyTransmissionVendor->getPropertyId()] = $arrobjPropertyTransmissionVendor->getKey();
		}

		return $arrobjPropertyTransmissionVendorsRekeyedByPropertyId;
	}

	public static function fetchPropertyTransmissionVendorByPropertyIdByTransmissionVendorIdByTransmissionTypeIdByCid( $intPropertyId, $intTransmissionVendorId = NULL, $intTransmissionTypeId = NULL, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ptv.*,
						ctv.transmission_vendor_id
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						ptv.property_id = ' . ( int ) $intPropertyId . '
						AND ptv.cid = ' . ( int ) $intCid;

		$strSql .= ( false == is_null( $intTransmissionVendorId ) ) ? ' AND ctv.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId : '';
		$strSql .= ( false == is_null( $intTransmissionTypeId ) ) ? ' AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId : '';

		return self::fetchPropertyTransmissionVendor( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorByPropertyIdByCompanyTransmissionVendorIdByTransmissionTypeIdByCid( $intPropertyId, $intCompanyTransmissionVendorId = NULL, $intTransmissionTypeId = NULL, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						ptv.property_id = ' . ( int ) $intPropertyId . '
						AND ptv.cid = ' . ( int ) $intCid;

		$strSql .= ( 0 < $intCompanyTransmissionVendorId ) ? ' AND ctv.id = ' . ( int ) $intCompanyTransmissionVendorId : '';
		$strSql .= ( 0 < $intTransmissionTypeId ) ? ' AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId : '';

		return self::fetchPropertyTransmissionVendor( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorByPropertyIdByCompanyTransmissionVendorIdByCid( $intPropertyId, $intCompanyTransmissionVendorId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_transmission_vendors WHERE property_id = ' . ( int ) $intPropertyId . ' AND company_transmission_vendor_id = ' . ( int ) $intCompanyTransmissionVendorId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchPropertyTransmissionVendor( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorByPropertyIdByTransmissionVendorIdByCid( $intPropertyId, $intTransmissionVednorId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ptv.*
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
					WHERE
						ptv.property_id = ' . ( int ) $intPropertyId . '
						AND ptv.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id = ' . ( int ) $intTransmissionVednorId . '
					LIMIT 1';

		return self::fetchPropertyTransmissionVendor( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyTransmissionVendorsByTransmissionVendorIdsByPropertyIds( $arrintTransmissionVendorIds, $arrintPropertyIds, $objClientDatabase ) {
		if( false == valArr( $arrintTransmissionVendorIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ptv.*
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
						JOIN properties p ON ( ptv.property_id = p.id AND ptv.cid = p.cid )
					WHERE
						ctv.transmission_vendor_id IN (' . implode( ',', $arrintTransmissionVendorIds ) . ' )
						AND ptv.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )';

		$arrobjPropertyTransmissionVendors = self::fetchPropertyTransmissionVendors( $strSql, $objClientDatabase, false );

		if( false == valArr( $arrobjPropertyTransmissionVendors ) ) return NULL;

		$arrobjPropertyTransmissionVendorsRekeyed = NULL;

		foreach( $arrobjPropertyTransmissionVendors as $objPropertyTransmissionVendor ) {
			$arrobjPropertyTransmissionVendorsRekeyed[$objPropertyTransmissionVendor->getCid()][$objPropertyTransmissionVendor->getPropertyId()] = $objPropertyTransmissionVendor;
		}

		return $arrobjPropertyTransmissionVendorsRekeyed;
	}

	public static function fetchPropertyTransmissionVendorsByTransmissionVendorIdsByPropertyIdsByCid( $arrintTransmissionVendorIds, $arrintPropertyIds, $arrintCids, $objClientDatabase ) {
		if( false == valArr( $arrintTransmissionVendorIds ) || false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						ptv.*
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
						JOIN properties p ON( ptv.property_id = p.id AND ptv.cid = p.cid )
					WHERE
						ptv.cid IN (' . implode( ',', $arrintCids ) . ' )
						AND ptv.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )
						AND ctv.transmission_vendor_id IN (' . implode( ',', $arrintTransmissionVendorIds ) . ' );';

		return self::fetchPropertyTransmissionVendors( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyTransmissionVendorByPropertyIdByTransmissionTypeIdByCid( $intPropertyId, $intTransmissionTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ptv.*,
						ctv.name as company_transmission_vendor_name,
						ctv.transmission_vendor_id
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
					WHERE
						ptv.property_id = ' . ( int ) $intPropertyId . '
						AND ptv.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId;

		return self::fetchPropertyTransmissionVendor( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorByPropertyIdsByTransmissionTypeIdByCid( $arrintPropertyIds, $intTransmissionTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ptv.*,
						ctv.name as company_transmission_vendor_name,
						ctv.transmission_vendor_id
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
					WHERE
						ptv.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ptv.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId;

		return self::fetchPropertyTransmissionVendors( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorDataByPropertyIdsByTransmissionTypeIdByCid( $arrintPropertyIds, $intTransmissionTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ptv.id,
						ptv.key,
						ptv.property_id
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
					WHERE
						ptv.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ptv.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyTransmissionVendorsByPropertyIdsByTransmissionTypeId( $arrintPropertyIds, $intTransmissionTypeId, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ptv.*
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
					WHERE
						ptv.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId;

		return self::fetchPropertyTransmissionVendors( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorsByPropertyIdsByTransmissionTypeIdByCid( $arrintPropertyIds, $intTransmissionTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

	 	$strSql = 'SELECT
						ptv.*,
	 					ctv.transmission_vendor_id
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
					WHERE
						ptv.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ptv.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId;

		return self::fetchPropertyTransmissionVendors( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorsByIntegrationDatabaseIdByTransmissionTypeIdByCid( $intIntegrationDatabaseId, $intTransmissionTypeId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						ptv.*
					FROM
						property_transmission_vendors ptv
						JOIN property_integration_databases pid ON ( pid.cid = ptv.cid AND pid.property_id = ptv.property_id )
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id )
					WHERE
						pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND ptv.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_type_id = ' . ( int ) $intTransmissionTypeId;

		return self::fetchPropertyTransmissionVendors( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyTransmissionVendorByCompanyTransmissionVendorIdByPropertyIdByKeyByCid( $intPropertyId, $intCid, $intOldCompanyTransmissionVendorId = NULL, $objDatabase ) {

		$strExistingCompanySql = ( true == valId( $intOldCompanyTransmissionVendorId ) )? 'AND ptv.company_transmission_vendor_id = ' . ( int ) $intOldCompanyTransmissionVendorId: NULL;

		$strSql = 'SELECT
						ptv.*
					FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						ptv.property_id = ' . ( int ) $intPropertyId . '
						AND ptv.cid = ' . ( int ) $intCid . '
						AND transmission_vendor_id <> ' . CTransmissionVendor::RESIDENT_VERIFY . $strExistingCompanySql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyTransmissionVendorsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ptv.property_id,
						ctv.name AS name,
						ctv.transmission_vendor_id
					FROM
						property_transmission_vendors ptv
						LEFT JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid )
					WHERE
						ptv.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND ptv.cid = ' . ( int ) $intCid . '
						AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY;

		return fetchData( $strSql, $objDatabase );
	}

}
?>