<?php

class CScreeningConditionOffer extends CBaseScreeningConditionOffer {

	protected $m_strNameFirst;
	protected $m_strNameLast;

	private function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	private function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicationRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningConditionOfferStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOfferDecisionBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOfferDecisionOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['name_first'] ) ) $this->setNameFirst( $arrstrValues['name_first'] );
		if( true == isset( $arrstrValues['name_last'] ) ) $this->setNameLast( $arrstrValues['name_last'] );
		return;
	}

}
?>