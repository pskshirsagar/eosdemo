<?php

class CAssetLocation extends CBaseAssetLocation {

	protected $m_strPropertyName;
	protected $m_intPropertyGroupId;

	protected $m_objPropertyGroupAssetLocations;

	/**
	 * Create Functions
	 */

	public function createPropertyGroupAssetLocation() {

		$objPropertyGroupAssetLocation	= new CPropertyGroupAssetLocation();
		$objPropertyGroupAssetLocation->setCid( $this->getCid() );

		return $objPropertyGroupAssetLocation;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyGroupAssetLocations() {
		return $this->m_objPropertyGroupAssetLocations;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPropertyGroupId() {
		return $this->m_intPropertyGroupId;
	}

	/**
	 * set Functions
	 */

	public function setPropertyGroupAssetLocations( $objPropertyGroupAssetLocations ) {
		$this->m_objPropertyGroupAssetLocations	= $objPropertyGroupAssetLocations;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPropertyGroupId( $intPropertyGroupId ) {
		$this->m_intPropertyGroupId = $intPropertyGroupId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}

		if( true == isset( $arrmixValues['property_group_id'] ) ) {
			$this->setPropertyGroupId( $arrmixValues['property_group_id'] );
		}

		return;
	}

	/**
	 * Val Functions
	 */

	public function valName( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Location name is required.' ) ) );
		}

		if( true == $boolIsValid ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							AND id <> ' . ( int ) $this->getId() . '
							AND deleted_by IS NULL';

			if( 0 < \Psi\Eos\Entrata\CAssetLocations::createService()->fetchAssetLocationCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Location name '{%s, 0}' already exists.", [ $this->getName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valStreetLine1() {

		$boolIsValid = true;

		if( true == is_null( $this->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Street Line {%d, 0} is required.', [ 1 ] ) ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		if( false == is_null( $this->getCity() ) && false == valStr( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City has invalid characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( false == isset( $this->m_strStateCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		$intLength = mb_strlen( str_replace( '-', '', $this->m_strPostalCode ) );

		if( false == is_null( $this->m_strPostalCode ) && 0 < $intLength
			&& ( false == \Psi\CStringService::singleton()->preg_match( '/^([[:alnum:]]){5,5}?$/', $this->m_strPostalCode ) && false == \Psi\CStringService::singleton()->preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->m_strPostalCode ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Postal code must be {%d, 0} or {%d, 1} characters in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.', [ 5, 10 ] ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber() {

		$boolIsValid = true;

		if( false == is_null( $this->getPhoneNumber() )
			&& false == ( CValidation::validateFullPhoneNumber( $this->m_strPhoneNumber, false ) ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Valid phone number is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valNotes() {
		return true;
	}

	public function valPropertyGroupAssetLocation() {
		$boolIsValid = true;

		if( false == valArr( $this->getPropertyGroupAssetLocations() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Select at least one property.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLocation( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return true;
		}

		$intFixedAssetCount = \Psi\Eos\Entrata\CAssets::createService()->fetchFixedAssetCountByAssetLocationIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		$intReceivedAssetCount = \Psi\Eos\Entrata\CAssetTransactions::createService()->fetchReceivedAssetTransactionCachedRemainingCountByAssetLocationIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		if( 0 < $intFixedAssetCount || 0 < $intReceivedAssetCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Location cannot be deleted because it is associated to inventory and/or fixed assets.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase, $boolIsValidatePhoneNumber = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objClientDatabase );
				$boolIsValid &= $this->valPropertyGroupAssetLocation();

				// TODO : We need to implement i18n phone number validation
				if( true == $boolIsValidatePhoneNumber ) {
					$boolIsValid &= $this->valPhoneNumber();
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valLocation( $objClientDatabase );
				break;

			default:
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

}
?>