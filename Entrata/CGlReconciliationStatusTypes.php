<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliationStatusTypes
 * Do not add any new functions to this class.
 */

class CGlReconciliationStatusTypes extends CBaseGlReconciliationStatusTypes {

	public static function fetchGlReconciliationStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlReconciliationStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchGlReconciliationStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlReconciliationStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllGlReconciliationStatusTypes( $objDatabase, $boolIsPublished = false ) {

		$strSql = 'SELECT * FROM gl_reconciliation_status_types';

		if( false != $boolIsPublished ) {
			$strSql = 'SELECT * FROM gl_reconciliation_status_types WHERE is_published = 1 ORDER BY id';
		}

		return self::fetchGlReconciliationStatusTypes( $strSql, $objDatabase );
	}

}
?>