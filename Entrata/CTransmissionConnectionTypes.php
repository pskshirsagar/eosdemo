<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissionConnectionTypes
 * Do not add any new functions to this class.
 */

class CTransmissionConnectionTypes extends CBaseTransmissionConnectionTypes {

	public static function fetchTransmissionConnectionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTransmissionConnectionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTransmissionConnectionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTransmissionConnectionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedTransmissionConnectionTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM transmission_connection_types WHERE is_published = 1';
		return self::fetchTransmissionConnectionTypes( $strSql, $objDatabase );
	}
}
?>