<?php

class CIntegrationQueuesFilter extends CEosFilter {

	protected $m_intId;
	protected $m_intDatabaseId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intIntegrationDatabaseId;
	protected $m_intReferenceNumber;

	protected $m_arrintReferenceNumbers;
	protected $m_arrintIntegrationClientIds;
	protected $m_arrintIntegrationClientTypeIds;
	protected $m_arrintIntegrationSyncTypeIds;
	protected $m_arrintIntegrationServiceIds;
	protected $m_arrintIntegrationQueueStatusTypeIds;
	protected $m_strSerializedFilter;
	protected $m_strFromDate;
	protected $m_strToDate;

	public static $c_arrstrFieldsInfo = [
		'id'                                => [ 'type' => 'int', 'length' => '', 'default' => 'NULL' ],
		'database_id'                       => [ 'type' => 'int', 'length' => '', 'default' => 'NULL' ],
		'cid'                               => [ 'type' => 'int', 'length' => '', 'default' => 'NULL' ],
		'property_id'                       => [ 'type' => 'int', 'length' => '', 'default' => 'NULL' ],
		'reference_number'                  => [ 'type' => 'int', 'length' => '', 'default' => 'NULL' ],
		'reference_numbers'                 => [ 'type' => 'arrint', 'length' => '', 'default' => 'NULL' ],
		'integration_database_id'           => [ 'type' => 'int', 'length' => '', 'default' => 'NULL' ],
		'integration_client_ids'            => [ 'type' => 'arrint', 'length' => '-1', 'default' => 'NULL' ],
		'integration_client_type_ids'       => [ 'type' => 'arrint', 'length' => '-1', 'default' => 'NULL' ],
		'integration_sync_type_ids'         => [ 'type' => 'arrint', 'length' => '-1', 'default' => 'NULL' ],
		'integration_service_ids'           => [ 'type' => 'arrint', 'length' => '-1', 'default' => 'NULL' ],
		'integration_queue_status_type_ids' => [ 'type' => 'arrint', 'length' => '-1', 'default' => 'NULL' ],
		'serialized_filter'                 => [ 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ],
		'from_date'                         => [ 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ],
		'to_date'                           => [ 'type' => 'str', 'length' => '-1', 'default' => 'NULL' ]
	];

	/**
	 * Validate Functions
	 */

	public function valDate() {
		$boolIsValid = true;

		$this->setFromDate( getSanitizedFormField( $this->getFromDate(), true ) );
		$this->setToDate( getSanitizedFormField( $this->getToDate(), true ) );

		if( false == is_null( $this->getFromDate() ) && false == CValidation::validateDate( $this->getFromDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'From date is not a valid date.' ) );
			$boolIsValid = false;
		}

		if( false == is_null( $this->getToDate() ) && false == CValidation::validateDate( $this->getToDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'To date is not a valid date.' ) );
			$boolIsValid = false;
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		if( false == is_null( $this->getFromDate() ) && false == is_null( $this->getToDate() ) ) {
			if( ( strtotime( $this->getFromDate() ) ) > ( strtotime( $this->getToDate() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'From date should not be greater then to date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDatabaseId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDatabaseId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Client database is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate() {
		$boolIsValid = true;

		$boolIsValid &= $this->valDate();
		$boolIsValid &= $this->valDatabaseId();

		return $boolIsValid;
	}

}

?>