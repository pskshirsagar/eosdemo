<?php

class CCompanyGroupPermission extends CBaseCompanyGroupPermission {

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Company group permission id appears invalid.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyGroupId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCompanyGroupId ) || ( 1 > $this->m_intCompanyGroupId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', 'Company group is required.' ) );
		}

		return $boolIsValid;
	}

	public function valModuleId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intModuleId ) || ( 1 > $this->m_intModuleId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'module_id', 'Module is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsAllowed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyGroupId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>