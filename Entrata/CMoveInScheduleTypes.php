<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMoveInScheduleTypes
 * Do not add any new functions to this class.
 */

class CMoveInScheduleTypes extends CBaseMoveInScheduleTypes {

	public static function fetchMoveInScheduleTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMoveInScheduleType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMoveInScheduleType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMoveInScheduleType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllMoveInScheduleTypes( $objDatabase ) {
		return self::fetchMoveInScheduleTypes( 'SELECT * FROM move_in_schedule_types', $objDatabase );
	}

	public static function fetchActiveMoveInScheduleTypes( $objDatabase ) {
		return self::fetchMoveInScheduleTypes( 'SELECT * FROM move_in_schedule_types WHERE is_published IS TRUE ORDER BY order_num', $objDatabase );
	}

}
?>