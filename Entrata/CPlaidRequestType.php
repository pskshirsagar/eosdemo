<?php

class CPlaidRequestType extends CBasePlaidRequestType {

	const API_CREATE_PUBLIC_TOKEN          = 1;
	const API_EXCHANGE_PUBLIC_TOKEN        = 2;
	const API_ACCOUNTS_GET                 = 3;
	const API_ITEM_GET                     = 4;
	const API_ITEM_WEBHOOK_UPDATE          = 5;
	const API_ITEM_ACCESS_TOKEN_INVALIDATE = 6;
	const API_ITEM_REMOVE                  = 7;
	const API_AUTH_GET                     = 8;
	const API_ACCOUNTS_BALANCE_GET         = 9;
	const API_AUTH_SUCCESS_WEBHOOK         = 10;
	const API_AUTH_FAILED_WEBHOOK          = 11;
	const API_ITEM_WEBHOOK_UPDATED         = 12;
	const API_ITEM_EXPIRATION_WEBHOOK      = 13;
	const API_ITEM_ERROR_WEBHOOK           = 14;
	const API_CREATE_LINK_TOKEN            = 15;

	public static $c_arrintPlaidRequestTypes = [
		'API_CREATE_PUBLIC_TOKEN'          => self::API_CREATE_PUBLIC_TOKEN,
		'API_EXCHANGE_PUBLIC_TOKEN'        => self::API_EXCHANGE_PUBLIC_TOKEN,
		'API_ACCOUNTS_GET'                 => self::API_ACCOUNTS_GET,
		'API_ITEM_GET'                     => self::API_ITEM_GET,
		'API_ITEM_WEBHOOK_UPDATE'          => self::API_ITEM_WEBHOOK_UPDATE,
		'API_ITEM_ACCESS_TOKEN_INVALIDATE' => self::API_ITEM_ACCESS_TOKEN_INVALIDATE,
		'API_ITEM_REMOVE'                  => self::API_ITEM_REMOVE,
		'API_AUTH_GET'                     => self::API_AUTH_GET,
		'API_ACCOUNTS_BALANCE_GET'         => self::API_ACCOUNTS_BALANCE_GET,
		'API_AUTH_SUCCESS_WEBHOOK'         => self::API_AUTH_SUCCESS_WEBHOOK,
		'API_AUTH_FAILED_WEBHOOK'          => self::API_AUTH_FAILED_WEBHOOK,
		'API_ITEM_WEBHOOK_UPDATED'         => self::API_ITEM_WEBHOOK_UPDATED,
		'API_ITEM_EXPIRATION_WEBHOOK'      => self::API_ITEM_EXPIRATION_WEBHOOK,
		'API_ITEM_ERROR_WEBHOOK'           => self::API_ITEM_ERROR_WEBHOOK,
		'API_CREATE_LINK_TOKEN'            => self::API_CREATE_LINK_TOKEN
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>