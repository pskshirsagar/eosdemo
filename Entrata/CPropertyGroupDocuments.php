<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGroupDocuments
 * Do not add any new functions to this class.
 */

class CPropertyGroupDocuments extends CBasePropertyGroupDocuments {

	public static function fetchPropertyGroupDocumentByDocumentIdByPropertyGroupIdByCid( $intDocumentId, $intPropertyGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pgd.document_id =' . ( int ) $intDocumentId . '
						AND pgd.property_group_id = ' . ( int ) $intPropertyGroupId . '
					LIMIT 1';

		return self::fetchPropertyGroupDocument( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByDocumentIdNotInPropertyIdsByCid( $intDocumentId, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pgd.document_id =' . ( int ) $intDocumentId . '
						AND pga.property_id NOT IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentByDocumentIdByPropertyIdByDocumentAssociationTypeIdByCid( $intDocumentId, $intPropertyId, $intDocumentAssociationTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pgd.document_id =' . ( int ) $intDocumentId . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociationTypeId . '
					LIMIT 1';

		return self::fetchPropertyGroupDocument( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByDocumentIdByPropertyIdsByCid( $intDocumentId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pgd.document_id =' . ( int ) $intDocumentId . '
						AND pga.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByDocumentIdByPropertyGroupIdsByCid( $intDocumentId, $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_group_documents pgd
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pgd.document_id =' . ( int ) $intDocumentId . '
						AND pgd.property_group_id IN (' . implode( ',', $arrintPropertyGroupIds ) . ')';

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByPropertyIdByTransmissionVendorIdByCid( $intPropertyId, $intTransmissionVendorId, $intCid, $objDatabase ) {
		if( false == valId( $intTransmissionVendorId ) ) return NULL;

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
						JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND d.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
						AND pga.property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByPropertyIdByTransmissionVendorIdsByCid( $intPropertyId, $arrintTransmissionVendorIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintTransmissionVendorIds ) ) return NULL;

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
						JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.transmission_vendor_id IN (' . implode( ',', $arrintTransmissionVendorIds ) . ')';

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
						JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND d.transmission_vendor_id IS NOT NULL
						AND pga.property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsWithBlueMoonKeyByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    pga.property_id,
					    ptv.key AS blue_moon_key
					FROM
					    property_group_documents pgd
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
					    JOIN property_transmission_vendors ptv ON ( ptv.property_id = pga.property_id AND ptv.cid = pgd.cid AND ptv.key IS NOT NULL )
						JOIN company_transmission_vendors ctv ON ( ctv.id = ptv.company_transmission_vendor_id AND ctv.cid = ptv.cid AND ctv.transmission_vendor_id = ' . CTransmissionVendor::BLUE_MOON_LEASE . ' )
					WHERE
						pgd.document_id = ' . ( int ) $intDocumentId . '
					    AND pgd.cid = ' . ( int ) $intCid;

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsWithPropertyNameByDocumentIdByByCid( $intDocumentId, $intCid, $objDatabase ) {
		if( false == valStr( $intDocumentId ) ) return NULL;

		$strSql = 'SELECT pgd.*,
				   		pg.id AS property_id,
				   		pg.name AS property_name
				   FROM
						property_group_documents pgd
  						JOIN property_groups pg ON ( pgd.property_group_id = pg.id AND pg.cid = pgd.cid )
  				   WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pgd.document_id = ' . ( int ) $intDocumentId . '
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
				   ORDER BY
						' . $objDatabase->getCollateSort( 'pg.name', true );

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByPropertyGroupIdsByFileTypeIdByCid( $arrintPropertyGroupIds, $intFileTypeId, $intCid, $objDatabase, $boolIncludeArchivedDocument = true ) {
		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
					WHERE
						d.file_type_id = ' . ( int ) $intFileTypeId . '
						AND pgd.cid = ' . ( int ) $intCid . '
						AND pgd.property_group_id IN (' . implode( ',', $arrintPropertyGroupIds ) . ')';

		if( false == $boolIncludeArchivedDocument ) {
			$strSql .= ' AND d.archived_on IS NULL';
		}

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByPropertyGroupIdBySystemCodeByCid( $intPropertyGroupId, $strSystemCode, $intCid, $objDatabase ) {

	    $strSql = 'SELECT
	                   pgd.*
	               FROM
	                   property_group_documents pgd
	                   JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
	                   JOIN file_types ft ON ( d.file_type_id = ft.id AND d.cid = ft.cid )
	               WHERE
	                   pgd.property_group_id = ' . ( int ) $intPropertyGroupId . '
	                   AND ft.system_code = \'' . $strSystemCode . '\'
                       AND pgd.cid = ' . ( int ) $intCid . '
	                   AND d.is_system = 1';

	    return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentIdsByDocumentIdByCid( $intDocumentId, $intCid, $objDatabase ) {

	    $strSql = 'SELECT
	                   pgd.property_group_id
	               FROM
	                   property_group_documents pgd
	               WHERE
	                   pgd.document_id = ' . ( int ) $intDocumentId . '
	                   AND pgd.cid = ' . ( int ) $intCid;

	    $arrobjPropertyGroupDocuments = ( array ) self::fetchPropertyGroupDocuments( $strSql, $objDatabase );

	    $arrintPropertyGroupIds = [];

	    foreach( $arrobjPropertyGroupDocuments as $objPropertyGroupDocument ) {

	        $arrintPropertyGroupIds[$objPropertyGroupDocument->getPropertyGroupId()] = $objPropertyGroupDocument->getPropertyGroupId();

	    }

	    return $arrintPropertyGroupIds;

	}

	public static function fetchPropertyGroupDocumentsByPropertyGroupIdsByExcludingDocumentIdsByFileTypeIdByCid( $arrintPropertyGroupIds, $arrintExcludingDocumentIds, $intFileTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyGroupIds ) || false == valArr( $arrintExcludingDocumentIds ) ) return NULL;

		$strSql = 'SELECT
	                   pgd.*
	               FROM
	                   property_group_documents pgd
	                   JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid )
	                   JOIN file_types ft ON ( d.file_type_id = ft.id AND d.cid = ft.cid )
	               WHERE
	                   pgd.property_group_id IN ( \'' . implode( '\',\'', $arrintPropertyGroupIds ) . '\' )
	                   AND pgd.cid = ' . ( int ) $intCid . '
                       AND pgd.document_id NOT IN ( \'' . implode( '\',\'', $arrintExcludingDocumentIds ) . '\' )
                       AND d.file_type_id = ' . ( int ) $intFileTypeId . '
	                   AND d.is_system = 1';

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByPropertyGroupIdByDocumentIdsByCid( $intPropertyGroupId, $arrintDocumentIds, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyGroupId ) || false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
	                   pgd.*
	               FROM
	                   property_group_documents pgd
	                   JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid AND d.deleted_by IS NULL )
	               WHERE
	                   pgd.property_group_id = ' . ( int ) $intPropertyGroupId . '
	                   AND pgd.cid = ' . ( int ) $intCid . '
                       AND pgd.document_id IN ( \'' . implode( '\',\'', $arrintDocumentIds ) . '\' )';

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentsByDocumentIdsByCid( $arrintDocumentIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintDocumentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid AND d.deleted_by IS NULL )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pgd.document_id IN (' . implode( ',', $arrintDocumentIds ) . ' ) ';

			return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyGroupDocumentTemplateDetailsByPropertyIdByCidByDocumentTypeIdByDocumentSubTypeId( $intPropertyId, $intCid, $intDocumentTypeId, $intDocumentSubTypeId, $intDocumentAssociateTypeId, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		if( false == is_null( $intDocumentSubTypeId ) ) {
			$strWhereCondition = ' AND d.document_sub_type_id = ' . ( int ) $intDocumentSubTypeId;
		}

		$strSql = 'SELECT
						pgd.*
					FROM
						property_group_documents pgd
						JOIN property_group_associations pga ON ( pga.property_group_id = pgd.property_group_id AND pga.cid = pgd.cid )
						JOIN documents d ON ( pgd.document_id = d.id AND pgd.cid = d.cid AND d.deleted_by IS NULL )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
						AND pga.property_id = ' . ( int ) $intPropertyId . '
						AND d.is_published = 1
						AND d.document_type_id = ' . ( int ) $intDocumentTypeId . '
						AND pgd.document_association_type_id = ' . ( int ) $intDocumentAssociateTypeId . $strWhereCondition;

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

	public static function fetchPropertyCountByDocumentIds( $arrintDocumentIds, $objClientDatabase ) {

		if( false == valArr( $arrintDocumentIds ) ) return NULL;

		$strSql = 'SELECT
						pgd.document_id,
						count ( pg.id ) as count
					FROM
						property_group_documents pgd
						JOIN property_groups pg ON ( pgd.property_group_id = pg.id AND pg.cid = pgd.cid )
						JOIN documents d ON ( pgd.document_id = d.id )
					WHERE
						pgd.document_id IN ( ' . implode( ',', $arrintDocumentIds ) . ' )
						AND pg.deleted_by IS NULL
					GROUP BY
						pgd.document_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyGroupDocumentsToDeleteByDocumentIdByPropertyGroupId( $intDocumentId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valId( $intDocumentId ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT 
						pgd.*,
						count(1)
					FROM
						property_group_documents pgd
						JOIN document_addendas da ON ( da.cid = pgd.cid AND da.document_id = pgd.document_id AND da.deleted_by IS NULL AND da.master_document_id IS NOT NULL )
					WHERE
						pgd.cid = ' . ( int ) $intCid . '
					    AND pgd.document_id = ' . ( int ) $intDocumentId . '
					    AND pgd.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
					    AND pgd.document_association_type_id = ' . CDocumentAssociationType::DOCUMENT_TEMPLATE . '
					    GROUP BY pgd.id, pgd.cid, pgd.document_id, pgd.property_group_id,pgd.document_association_type_id,pgd.is_published,
					            pgd.is_system,pgd.order_num,pgd.updated_by,pgd.updated_on,pgd.created_by,pgd.created_on
					    HAVING count(1) < 2';

		return self::fetchPropertyGroupDocuments( $strSql, $objDatabase );
	}

}
?>