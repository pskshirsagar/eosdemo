<?php

class CArInvoiceType extends CBaseArInvoiceType {

	const STANDARD		= 1;
	const CREDIT_NOTE	= 2;
	const DOWN_PAYMENT	= 3;
	const CORRECTION	= 4;

	public static $c_arrintArInvoiceTypeIds = [ self::STANDARD, self::CREDIT_NOTE, self::DOWN_PAYMENT, self::CORRECTION ];

	public static $c_arrstrInvoiceTypeKeys = [
		self::STANDARD        => 'invoice_document_id',
		self::CREDIT_NOTE     => 'credit_note_invoice_document_id',
		self::DOWN_PAYMENT    => 'down_payment_invoice_document_id',
		self::CORRECTION      => 'corrective_invoice_document_id'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getArInvoiceTypeNameById( $intArInvoiceTypeId ) {

		$strArInvoiceTypeName = '';

		switch( $intArInvoiceTypeId ) {

			case self::STANDARD:
				$strArInvoiceTypeName = 'Standard';
				break;

			case self::CREDIT_NOTE:
				$strArInvoiceTypeName = 'Credit Note';
				break;

			case self::DOWN_PAYMENT:
				$strArInvoiceTypeName = 'Down Payment';
				break;

			case self::CORRECTION:
				$strArInvoiceTypeName = 'Correction';
				break;

			default:
				// default case
				break;
		}

		return $strArInvoiceTypeName;
	}

}
?>