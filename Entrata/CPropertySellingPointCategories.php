<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySellingPointCategories
 * Do not add any new functions to this class.
 */

class CPropertySellingPointCategories extends CBasePropertySellingPointCategories {

	public static function fetchPropertySellingPointCategories( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPropertySellingPointCategory', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchPropertySellingPointCategory( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPropertySellingPointCategory', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchActivePropertySellingPointCategories( $objDatabase ) {
		return self::fetchPropertySellingPointCategories( sprintf( 'SELECT * FROM %s WHERE is_published = TRUE ORDER BY order_num ASC', 'property_selling_point_categories' ), $objDatabase );
	}

}
?>