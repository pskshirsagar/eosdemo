<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyQuickResponses
 * Do not add any new functions to this class.
 */

class CPropertyQuickResponses extends CBasePropertyQuickResponses {

	public static function fetchPropertyQuickResponsesCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT(pqr.id)
					FROM
						quick_responses qr
						JOIN property_quick_responses pqr ON ( qr.cid = pqr.cid AND qr.id = pqr.quick_response_id )
					WHERE
						qr.cid = ' . ( int ) $intCid . '
						AND pqr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND qr.deleted_on IS NULL';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponse ) ) return $arrmixResponse[0]['count'];

		return NULL;
	}

	public static function fetchPropertyQuickResponseContentByCidById( $intCid, $intId, $objDatabase ) {

		$strSql = 'SELECT
						pqr.property_id,
						qr.name,
						qr.message
					FROM
						quick_responses qr
						JOIN property_quick_responses pqr ON ( qr.cid = pqr.cid AND qr.id = pqr.quick_response_id )
					WHERE
						qr.cid = ' . ( int ) $intCid . '
						AND pqr.id = ' . ( int ) $intId;

		return self::fetchPropertyQuickResponse( $strSql, $objDatabase );
	}

	public static function fetchPropertyQuickResponsesCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $strQuickSearch, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strQuickSearch	 = valStr( $strQuickSearch ) ? 'AND ( qr.name ILIKE ( \'%' . $strQuickSearch . '%\' ) OR qr.subject ILIKE ( \'%' . $strQuickSearch . '%\' ) )' : '';

		$strSql = 'SELECT
						COUNT( DISTINCT qr.id )
					FROM
						quick_responses qr
						LEFT JOIN property_quick_responses pqr ON ( qr.cid = pqr.cid AND qr.id = pqr.quick_response_id )
					WHERE
						qr.cid = ' . ( int ) $intCid . '
						AND ( qr.is_enabled_all_properties = TRUE OR pqr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						AND qr.deleted_on IS NULL
						AND qr.deleted_on IS NULL
						' . $strQuickSearch;

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponse ) ) return $arrmixResponse[0]['count'];

		return NULL;
	}

	public static function fetchPropertyQuickResponseFieldsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( qr.id, qr.name )
						qr.id,
						qr.cid,
						qr.name,
						CONCAT ( CASE WHEN qr.is_enabled_all_properties = TRUE THEN \' All \'
							WHEN COUNT ( pqr.id ) = 1 THEN MAX ( p.property_name )
							ELSE \' Multiple \'
						END, \' - \', qr.name ) AS title_name
					FROM
						quick_responses qr
						LEFT JOIN property_quick_responses pqr ON ( qr.cid = pqr.cid AND qr.id = pqr.quick_response_id )
						LEFT JOIN properties p ON ( qr.cid = p.cid AND ( qr.is_enabled_all_properties = false AND pqr.property_id = p.id ) )
					WHERE
						qr.cid = ' . ( int ) $intCid . '
						AND ( qr.is_enabled_all_properties = TRUE OR pqr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					GROUP BY
					 qr.id, qr.cid, qr.name
					ORDER BY qr.name ASC';

		$arrmixPropertyQuickResponsesData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixPropertyQuickResponsesData ) ) {
			foreach( $arrmixPropertyQuickResponsesData as &$arrmixData ) {
				$arrmixData['id'] = '?module=general_dashboardxxx&action=load_quick_response&quick_response[id]=' . $arrmixData['id'] . '&client[id]=' . $arrmixData['cid'] . '&property[id]=' . $arrmixData['property_id'];
				$arrmixData['name'] = \Psi\CStringService::singleton()->htmlentities( $arrmixData['name'], ENT_QUOTES, 'utf-8', false );
			}
		}

		return $arrmixPropertyQuickResponsesData;
	}

	public static function fetchPropertyQuickResponsesByPropertyIdsByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $intCid, $strQuickSearch, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$strQuickSearch	 = valStr( $strQuickSearch ) ? 'AND ( qr.name ILIKE ( \'%' . $strQuickSearch . '%\' ) OR qr.subject ILIKE ( \'%' . $strQuickSearch . '%\' ) )' : '';

		$strSql = 'SELECT
						DISTINCT ON( qr.id )
						qr.id as quick_response_id,
						qr.name,
						qr.subject,
						qr.message
					FROM
						quick_responses qr
						LEFT JOIN property_quick_responses pqr ON ( pqr.quick_response_id = qr.id AND pqr.cid = qr.cid )
					WHERE
						qr.cid = ' . ( int ) $intCid . '
						AND ( qr.is_enabled_all_properties = TRUE OR pqr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						AND qr.deleted_on IS NULL
						' . $strQuickSearch . '
					ORDER BY
						qr.id DESC
						LIMIT ' . ( int ) $intPageSize . '	OFFSET ' . ( int ) $intOffset;

		return self::fetchPropertyQuickResponses( $strSql, $objDatabase );

	}

	public static function fetchPropertyQuickResponsesByQuickResponseIdsByPropertyIdsByCid( $arrintQuickResponseIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pqr.*
					FROM
						property_quick_responses pqr
					WHERE
						pqr.cid = ' . ( int ) $intCid . '
						AND pqr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND quick_response_id IN (' . implode( ',', $arrintQuickResponseIds ) . ')';

		return self::fetchPropertyQuickResponses( $strSql, $objDatabase );
	}

	public static function fetchPropertyQuickResponsesByQuickResponseIdsByCid( $arrintQuickResponseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintQuickResponseIds ) ) return NULL;

		$strSql = 'SELECT
						pqr.*
					FROM
						property_quick_responses pqr
					WHERE
						pqr.cid = ' . ( int ) $intCid . '
						AND quick_response_id IN (' . implode( ',', $arrintQuickResponseIds ) . ')';

		return self::fetchPropertyQuickResponses( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdByQuickResponseIdsByPropertyIdsByCid( $arrintQuickResponseIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
				 		qr.id,
						CASE qr.is_enabled_all_properties
							WHEN FALSE THEN array_to_string( array_agg( pqr.property_id ), \',\' )
							ELSE \'' . implode( ',', $arrintPropertyIds ) . '\'
						END AS property_ids
					FROM
						property_quick_responses pqr
						RIGHT JOIN quick_responses qr ON ( qr.cid = pqr.cid AND qr.id = pqr.quick_response_id )
					WHERE
						qr.cid = ' . ( int ) $intCid . '
						AND qr.id IN (' . implode( ',', $arrintQuickResponseIds ) . ')
						AND ( qr.is_enabled_all_properties = TRUE OR pqr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					GROUP BY
						qr.id,
						qr.is_enabled_all_properties';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyQuickResponseByQuickResponseIdByPropertyIdByCid( $intQuickResponseId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pqr.*
					FROM
						property_quick_responses pqr
					WHERE
						pqr.cid = ' . ( int ) $intCid . '
						AND pqr.property_id = ' . ( int ) $intPropertyId . '
						AND quick_response_id = ' . ( int ) $intQuickResponseId;

		return self::fetchPropertyQuickResponse( $strSql, $objDatabase );
	}

}
?>