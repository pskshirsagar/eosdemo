<?php

class CMaintenanceProblemApCode extends CBaseMaintenanceProblemApCode {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceProblemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitOfMeasureId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuantity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>