<?php

class CInterCompanyStages extends CBaseInterCompanyStages {

	public static function fetchInterCompanyStages( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CInterCompanyStage', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchInterCompanyStage( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CInterCompanyStage', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>