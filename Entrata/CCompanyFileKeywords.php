<?php

class CCompanyFileKeywords extends CBaseCompanyFileKeywords {

	public static function fetchConflictingCompanyFileKeywordCount( $objConflictingCompanyFileKeyword, $objDatabase ) {

		$intId = ( false == is_numeric( $objConflictingCompanyFileKeyword->getId() ) ) ? 0 : $objConflictingCompanyFileKeyword->getId();

		$strWhereSql = ' WHERE
				   			cid = ' . ( int ) $objConflictingCompanyFileKeyword->getCid() . '
				   			AND id <> ' . ( int ) $intId .
					   	' AND keyword = \'' . ( string ) addslashes( $objConflictingCompanyFileKeyword->getKeyword() ) . '\'
				   		 LIMIT 1';

		return self::fetchCompanyFileKeywordCount( $strWhereSql, $objDatabase );
	}
}
?>