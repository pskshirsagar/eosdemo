<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetTabGlAccountItemMonths
 * Do not add any new functions to this class.
 */

class CBudgetTabGlAccountItemMonths extends CBaseBudgetTabGlAccountItemMonths {

	public static function fetchBudgetTabGlAccountItemMonthsByBudgetDataSourceTypeIdsByBudgetIdByCid( $arrintBudgetDataSourceTypeIds, $intBudgetId, $intCid, $objDatabase ) {
		if( false == valIntArr( $arrintBudgetDataSourceTypeIds ) || false == valId( $intBudgetId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						btgaim.*,
						btgai.formula,
						btgai.unit_price,
						btgam.id AS budget_tab_gl_account_month_id
					FROM
						budget_tab_gl_account_item_months btgaim
						JOIN budget_tab_gl_account_items btgai ON ( btgai.cid = btgaim.cid AND btgai.id = btgaim.budget_tab_gl_account_item_id )
						JOIN budget_tab_gl_account_months btgam ON ( btgam.cid = btgaim.cid AND btgam.budget_tab_gl_account_id = btgai.budget_tab_gl_account_id AND btgam.post_month = btgaim.post_month )
					WHERE
						btgaim.cid = ' . ( int ) $intCid . '
						AND btgaim.budget_id = ' . ( int ) $intBudgetId . '
						AND btgai.budget_data_source_type_id IN ( ' . sqlIntImplode( $arrintBudgetDataSourceTypeIds ) . ' )';

		return self::fetchBudgetTabGlAccountItemMonths( $strSql, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemMonthsByBudgetIdByBudgetAssumptionMonthIdsByCid( $intBudgetId, $arrintBudgetAssumptionMonthIds, $intCid, $objDatabase ) {

		if( false == valId( $intBudgetId ) || false == valIntArr( $arrintBudgetAssumptionMonthIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					DISTINCT ON ( btgaim.id, btgaim.cid ) btgaim.*,
					btgai.unit_price,
					btgai.formula,
					btgam.id AS budget_tab_gl_account_month_id,
					btga.budget_tab_id
				FROM
					budget_tab_gl_account_item_months btgaim
					JOIN budget_tab_gl_account_items btgai ON ( btgai.cid = btgaim.cid AND btgai.id = btgaim.budget_tab_gl_account_item_id AND btgai.budget_id = btgaim.budget_id )
					JOIN budget_tab_gl_account_months btgam ON ( btgam.cid = btgaim.cid AND btgam.budget_tab_gl_account_id = btgai.budget_tab_gl_account_id AND btgam.post_month = btgaim.post_month AND btgam.budget_id = btgaim.budget_id )
					JOIN budget_tab_gl_accounts btga ON ( btga.cid = btgam.cid AND btga.budget_id = btgam.budget_id AND btga.id = btgam.budget_tab_gl_account_id )
					JOIN budget_assumption_months bam ON ( bam.cid = btgaim.cid AND bam.post_month = btgaim.post_month AND bam.budget_id = btgai.budget_id  )
					JOIN budget_assumptions ba ON ( ba.cid = bam.cid AND ba.id = bam.budget_assumption_id AND bam.budget_id = ba.budget_id )
				WHERE
					btgaim.cid = ' . ( int ) $intCid . '
					AND btgaim.budget_id = ' . ( int ) $intBudgetId . '
					AND btgai.budget_data_source_type_id = ' . CBudgetDataSourceType::CUSTOM_FORMULAS . '
					AND bam.id IN ( ' . sqlIntImplode( $arrintBudgetAssumptionMonthIds ) . ' )
					AND btgai.formula LIKE \'%\' || ba.key || \'%\'';

		return self::fetchBudgetTabGlAccountItemMonths( $strSql, $objDatabase );
	}

	public static function fetchBudgetTabGlAccountItemMonthsByBudgetTabGlAccountItemIdsByCid( $arrintBudgetTabGlAccountItemIds, $intCid, $objDatabase ) {

		if( false === valIntArr( $arrintBudgetTabGlAccountItemIds ) || false === valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						btgaim.*,
						btgai.unit_price,
						btgam.id AS budget_tab_gl_account_month_id
					FROM
						budget_tab_gl_account_item_months btgaim
						JOIN budget_tab_gl_account_items btgai ON ( btgaim.cid = btgai.cid AND btgai.budget_id = btgaim.budget_id AND btgai.id = btgaim.budget_tab_gl_account_item_id )
						JOIN budget_tab_gl_account_months btgam ON ( btgam.cid = btgaim.cid AND btgam.budget_tab_gl_account_id = btgai.budget_tab_gl_account_id AND btgam.post_month = btgaim.post_month AND btgam.budget_id = btgaim.budget_id )
					WHERE
						btgaim.cid = ' . ( int ) $intCid . '
						AND btgaim.budget_tab_gl_account_item_id IN ( ' . sqlIntImplode( $arrintBudgetTabGlAccountItemIds ) . ' )';

		return self::fetchBudgetTabGlAccountItemMonths( $strSql, $objDatabase );
	}

}
?>