<?php

class CDelinquencyPolicyPropertyGroup extends CBaseDelinquencyPolicyPropertyGroup {

	protected $m_strPropertyGroupName;

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_group_name'] ) )		$this->setPropertyGroupName( $arrmixValues['property_group_name'] );
	}

	/**
	 * Get Functions
	 */

	public function setPropertyGroupName( $strPropertyGroupName ) {
		$this->m_strPropertyGroupName = CStrings::strTrimDef( $strPropertyGroupName, -1, NULL, true );
	}

	public function getPropertyGroupName() {
		return $this->m_strPropertyGroupName;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDelinquencyPolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>