<?php

class CDefaultLateNoticeType extends CBaseDefaultLateNoticeType {

	const FIRST_NOTICE		= 1;
	const REPEAT_NOTICE		= 2;
	const FINAL_NOTICE		= 3;
	const DEMAND_NOTICE		= 4;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDocumentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>