<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTabletButtons
 * Do not add any new functions to this class.
 */

class CTabletButtons extends CBaseTabletButtons {

    public static function fetchTabletButtonsWithNameByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
       	$strSql = 'SELECT
						tb.*,
       					tbt.name,
       					tbt.handle,
       					tbt.requires_url
                    FROM
       					tablet_buttons tb
       					JOIN tablet_button_types tbt ON ( tb.tablet_button_type_id = tbt.id )
                    WHERE
                        tb.website_id = ' . ( int ) $intWebsiteId . '
                        AND tb.cid = ' . ( int ) $intCid . '
                        AND tbt.is_published = 1
                    ORDER BY
						tb.order_num';

        return self::fetchTabletButtons( $strSql, $objDatabase );
    }

	public static function fetchCustomTabletButtonsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
         $strSql = 'SELECT * FROM tablet_buttons WHERE website_id = ' . ( int ) $intWebsiteId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY order_num';

        return self::fetchTabletButtons( $strSql, $objDatabase );
    }

    public static function fetchCustomTabletButtonByIdByCid( $intId, $intCid, $objDatabase ) {
         $strSql = 'SELECT
                    	tb.*,
         				tbt.name,
         				tbt.handle,
         				tbt.requires_url
                     FROM
         				tablet_buttons tb
         				JOIN tablet_button_types tbt ON ( tb.tablet_button_type_id = tbt.id )
                     WHERE
                     	tb.id = ' . ( int ) $intId . '
                        AND tb.cid = ' . ( int ) $intCid . '
                        AND tbt.is_published = 1';

    	return self::fetchTabletButton( $strSql, $objDatabase );
    }
}
?>