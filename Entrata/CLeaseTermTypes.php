<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseTermTypes
 * Do not add any new functions to this class.
 */

class CLeaseTermTypes extends CBaseLeaseTermTypes {

	public static function fetchLeaseTermTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CLeaseTermType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchLeaseTermType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CLeaseTermType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllLeaseTermTypes( $objDatabase ) {
		return fetchData( 'SELECT ltt.id, ltt.name FROM PUBLIC.lease_term_types ltt WHERE ltt.is_published = true ', $objDatabase );
	}

}
?>