<?php

class CGlTransactionType extends CBaseGlTransactionType {

	const GL_GJ								= 1;

	const AP_CHARGE 						= 2;
	const AP_PAYMENT						= 3;

	const AP_ALLOCATION						= 4;
	const AP_ALLOCATION_REVERSAL			= 5;

	const AR_CHARGE							= 7;
	const AR_PAYMENT						= 8;
	const AR_DEPOSIT_HELD					= 9; // Should we remove this?

	const AR_ALLOCATION						= 13;
	const AR_ALLOCATION_REVERSAL			= 14;

	const AR_DEPOSIT						= 15;
	const AR_DEPOSIT_REVERSAL				= 16;

	const DATA_FIX_AR_TRANSACTIONS			= 17;
	const DATA_FIX_AR_ALLOCATIONS			= 18;
	const DATA_FIX_AR_DEPOSITS				= 19;
	const DATA_FIX_AP_HEADERS				= 20;
	const DATA_FIX_AP_ALLOCATIONS			= 21;
	const DATA_FIX_GENERAL					= 22;
	const DATA_FIX_AR_ALLOCATIONS2			= 23; // We will deprecate this one.
	const DATA_FIX_AR_ALLOCATIONS3			= 24; // We will deprecate this one.

	const ASSET_CONSUMPTION					= 25;
	const ASSET_CONSUMPTION_REVERSAL		= 26;

	const ASSET_DEPRECIATION		        = 38;

	public static $c_arrintArTransactionAndDepositGlTransactionTypeIds = [ self::AR_CHARGE, self::AR_PAYMENT, self::AR_DEPOSIT, self::AR_DEPOSIT_REVERSAL ];
	public static $c_arrintArTransactionGlTransactionTypeIds = [ self::AR_CHARGE, self::AR_DEPOSIT_HELD, self::AR_PAYMENT ];
	public static $c_arrintArAllocationGlTransactionTypeIds = [ self::AR_ALLOCATION, self::AR_ALLOCATION_REVERSAL ];
	public static $c_arrintDataFixArAllocationGlTransactionTypeIds = [ self::DATA_FIX_AR_ALLOCATIONS, self::DATA_FIX_AR_ALLOCATIONS2, self::DATA_FIX_AR_ALLOCATIONS3 ];
	public static $c_arrintArDepositGlTransactionTypeIds = [ self::AR_DEPOSIT, self::AR_DEPOSIT_REVERSAL ];
	public static $c_arrintApHeaderGlTransactionTypeIds = [ self::AP_CHARGE, self::AP_PAYMENT ];
	public static $c_arrintApAllocationGlTransactionTypeIds = [ self::AP_ALLOCATION, self::AP_ALLOCATION_REVERSAL ];

	public static $c_arrintApGlTransactionTypeIds = [ self::AP_CHARGE, self::AP_PAYMENT, self::AP_ALLOCATION, self::AP_ALLOCATION_REVERSAL ];
	public static $c_arrintArGlTransactionTypeIds = [ self::AR_CHARGE, self::AR_PAYMENT, self::AR_ALLOCATION, self::AR_ALLOCATION_REVERSAL, self::AR_DEPOSIT, self::AR_DEPOSIT_REVERSAL ];

	public static $c_arrintNonPaymentArTransactionGlTransactionTypeIds = [ self::AR_CHARGE ];

	public static $c_arrintGjGlTransactionTypeIds = [ self::GL_GJ ];

	public static $c_arrintAllGlTransactionTypeIds	= [
		CGlLedgerType::AP => [ self::AP_CHARGE, self::AP_PAYMENT, self::AP_ALLOCATION, self::AP_ALLOCATION_REVERSAL ],
		CGlLedgerType::AR => [ self::AR_CHARGE, self::AR_PAYMENT, self::AR_ALLOCATION, self::AR_ALLOCATION_REVERSAL, self::AR_DEPOSIT, self::AR_DEPOSIT_REVERSAL ],
		CGlLedgerType::GL => [ self::GL_GJ ]
	];

	public static $c_arrintDepositeGlTransactionTypeIds = [ CGlTransactionType::AR_DEPOSIT_HELD, CGlTransactionType::AR_DEPOSIT, CGlTransactionType::AR_DEPOSIT_REVERSAL, CGlTransactionType::DATA_FIX_AR_DEPOSITS ];

	public static function loadSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'GL_TRANSACTION_TYPE_GL_GJ', 							self::GL_GJ );

		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AP_CHARGE', 						self::AP_CHARGE );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AP_PAYMENT', 						self::AP_PAYMENT );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AP_ALLOCATION', 					self::AP_ALLOCATION );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AP_ALLOCATION_REVERSAL', 			self::AP_ALLOCATION_REVERSAL );

		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AR_CHARGE', 						self::AR_CHARGE );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AR_PAYMENT', 						self::AR_PAYMENT );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AR_DEPOSIT_HELD', 					self::AR_DEPOSIT_HELD );

		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AR_ALLOCATION', 					self::AR_ALLOCATION );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AR_ALLOCATION_REVERSAL', 			self::AR_ALLOCATION_REVERSAL );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AR_DEPOSIT', 						self::AR_DEPOSIT );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_AR_DEPOSIT_REVERSAL', 				self::AR_DEPOSIT_REVERSAL );

		$objSmarty->assign( 'GL_TRANSACTION_TYPE_ASSET_CONSUMPTION', 				self::ASSET_CONSUMPTION );
		$objSmarty->assign( 'GL_TRANSACTION_TYPE_ASSET_CONSUMPTION_REVERSAL', 		self::ASSET_CONSUMPTION_REVERSAL );
	}

}
?>