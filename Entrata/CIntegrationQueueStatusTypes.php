<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationQueueStatusTypes
 * Do not add any new functions to this class.
 */

class CIntegrationQueueStatusTypes extends CBaseIntegrationQueueStatusTypes {

	public static function fetchIntegrationQueueStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CIntegrationQueueStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchIntegrationQueueStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CIntegrationQueueStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllIntegrationQueueStatusTypes( $objDatabase ) {
		return self::fetchIntegrationQueueStatusTypes( 'SELECT * FROM integration_queue_status_types', $objDatabase );
	}

	public static function fetchIntegrationQueueStatusTypesByIds( $arrintIntegrationQueueStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintIntegrationQueueStatusTypeIds ) ) return false;

		return self::fetchIntegrationQueueStatusTypes( 'SELECT * FROM integration_queue_status_types WHERE id IN ( ' . implode( ',', $arrintIntegrationQueueStatusTypeIds ) . ' )', $objDatabase );
	}
}
?>