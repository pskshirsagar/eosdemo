<?php

class CMessageBatch extends CBaseMessageBatch {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Company id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessageBatchTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMessageBatchTypeId() ) || 0 >= ( int ) $this->getMessageBatchTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_batch_type_id', __( 'Message batch type id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBatchStartedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valMessageBatchTypeId();
				return $boolIsValid;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>