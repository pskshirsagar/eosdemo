<?php

class CCompanyMediaFolder extends CBaseCompanyMediaFolder {

	protected $m_arrobjCompanyMediaFolders;
	protected $m_arrobjChildCompanyMediaFolders;
	protected $m_arrobjParentCompanyMediaFolders;

	protected $m_intSubFolderCount;
	protected $m_intMediaFilesCount;

	/**
	 * Add Functions
	 */

	public function addCompanyMediaFolder( $objCompanyMediaFolder ) {
		$this->m_arrobjCompanyMediaFolders[$objCompanyMediaFolder->getId()] = $objCompanyMediaFolder;
	}

	/**
	 * Get Functions
	 */

	public function getCompanyMediaFolders() {
		return $this->m_arrobjCompanyMediaFolders;
	}

	public function getSubFolderCount() {
		return $this->m_intSubFolderCount;
	}

	public function getMediaFilesCount() {
		return $this->m_intMediaFilesCount;
	}

	/**
	 * Set Functions
	 */

	public function setCompanyMediaFolders( $arrobjCompanyMediaFolders ) {
		$this->m_arrobjCompanyMediaFolders = $arrobjCompanyMediaFolders;
	}

	public function setSubFolderCount( $intSubFolderCount ) {
		$this->m_intSubFolderCount = $intSubFolderCount;
	}

	public function setMediaFilesCount( $intMediaFilesCount ) {
		$this->m_intMediaFilesCount = $intMediaFilesCount;
	}

	 public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['sub_folder_count'] ) ) {
			$this->setSubFolderCount( $arrmixValues['sub_folder_count'] );
		}

		if( true == isset( $arrmixValues['media_files_count'] ) ) {
			$this->setMediaFilesCount( $arrmixValues['media_files_count'] );
		}
	 }

	/**
	 * Validation Functions
	 */

	public function valFolderName() {
		$boolIsValid = true;

		if( true == is_null( $this->getFolderName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'folder_name', __( 'Invalid Folder Name.' ) ) );
		}

		return $boolIsValid;
	}

	public function valParentMediaFolderId() {
		$boolIsValid = true;

		if( true == is_null( $this->getParentMediaFolderId() ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validateMoveCompanyMediaFolders( $objDatabase ) {

		$boolIsValid = true;

		if( $this->getId() == $this->getParentMediaFolderId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'You cannot move a folder to itself.' ) ) );
			return false;
		}

		$arrobjParentCompanyMediaFolders = $this->loadTargetParentCompanyMediaFolders( $objDatabase );

		if( true == valArr( $arrobjParentCompanyMediaFolders ) && true == array_key_exists( $this->getId(), $arrobjParentCompanyMediaFolders ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Parent Company media folder could not be moved to its child folder.' ) ) );
			return false;
		}

		if( false == valId( $this->getParentMediaFolderId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please choose a folder to move to.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valFolderName();
				$boolIsValid &= $this->valParentMediaFolderId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFolderName();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_move':
				$boolIsValid &= $this->validateMoveCompanyMediaFolders( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * fetch Functions
	 */

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchParentMediaFolder( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFolders::createService()->fetchCompanyMediaFolderByIdByCid( $this->getParentMediaFolderId(), $this->getCid(), $objDatabase );
	}

	public function fetchSiblingCompanyMediaFoldersByParentMediaFolderId( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFolders::createService()->fetchSiblingCompanyMediaFoldersByParentMediaFolderIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function loadChildCompanyMediaFolders( $objDatabase, $objCompanyMediaFolder = NULL ) {

		if( false == valObj( $objCompanyMediaFolder, 'CCompanyMediaFolder' ) ) {
			$objCompanyMediaFolder = clone $this;
			$objCompanyMediaFolder->m_arrobjChildCompanyMediaFolders = [];
		}

		$arrobjChildCompanyMediaFolders = \Psi\Eos\Entrata\CCompanyMediaFolders::createService()->fetchChildCompanyMediaFoldersByIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjChildCompanyMediaFolders ) ) {
			$objCompanyMediaFolder->m_arrobjChildCompanyMediaFolders = array_merge( $objCompanyMediaFolder->m_arrobjChildCompanyMediaFolders, $arrobjChildCompanyMediaFolders );

			foreach( $arrobjChildCompanyMediaFolders as $objChildCompanyMediaFolder ) {
				$objChildCompanyMediaFolder->loadChildCompanyMediaFolders( $objDatabase, $objCompanyMediaFolder );
			}
		}

		if( true == valArr( $objCompanyMediaFolder->m_arrobjChildCompanyMediaFolders ) ) {
			$objCompanyMediaFolder->m_arrobjChildCompanyMediaFolders	= rekeyObjects( 'Id', $objCompanyMediaFolder->m_arrobjChildCompanyMediaFolders );
		}

		return $objCompanyMediaFolder->m_arrobjChildCompanyMediaFolders;
	}

	public function loadTargetParentCompanyMediaFolders( $objDatabase, $objCompanyMediaFolder = NULL ) {

		if( false == valObj( $objCompanyMediaFolder, 'CCompanyMediaFolder' ) ) {
			$objCompanyMediaFolder = clone $this;
			$objCompanyMediaFolder->m_arrobjParentCompanyMediaFolders = [];
		}

		$objParentCompanyMediaFolder = $this->fetchParentMediaFolder( $objDatabase );

		if( true == valObj( $objParentCompanyMediaFolder, 'CCompanyMediaFolder' ) ) {
			$objCompanyMediaFolder->m_arrobjParentCompanyMediaFolders = array_merge( $objCompanyMediaFolder->m_arrobjParentCompanyMediaFolders, [ $objParentCompanyMediaFolder ] );

			if( false == is_null( $objParentCompanyMediaFolder->getParentMediaFolderId() ) ) {
				$objParentCompanyMediaFolder->loadTargetParentCompanyMediaFolders( $objDatabase, $objCompanyMediaFolder );
			}
		}

		if( true == valArr( $objCompanyMediaFolder->m_arrobjParentCompanyMediaFolders ) ) {
			$objCompanyMediaFolder->m_arrobjParentCompanyMediaFolders = rekeyObjects( 'ParentMediaFolderId', $objCompanyMediaFolder->m_arrobjParentCompanyMediaFolders );
		}

		return $objCompanyMediaFolder->m_arrobjParentCompanyMediaFolders;
	}

}
?>