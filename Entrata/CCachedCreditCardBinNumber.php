<?php

class CCachedCreditCardBinNumber extends CBaseCachedCreditCardBinNumber {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSequenceNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBinLength() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPanLength() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBinNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTableId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDebitCard() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsCheckCard() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsCreditCard() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsGiftCard() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsCommercialCard() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsFleetCard() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPrepaidCard() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsHsaFsaAccount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPinlessBillPay() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsEbt() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsWicBin() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsInternationalBin() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDurbinBinRegulation() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPinlessPos() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>