<?php

class CReportInstance extends CBaseReportInstance {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentModuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valReportFilterId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDuplicateReport( $strAction, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function sqlModuleId() {
		return ( true == isset( $this->m_intModuleId ) ) ? ( string ) $this->m_intModuleId : '(
			SELECT
				min(m.id)
			FROM
				modules m
				LEFT JOIN report_groups rg ON rg.module_id = m.id AND rg.cid = ' . $this->m_intCid . '
				LEFT JOIN report_instances ri ON ri.module_id = m.id AND ri.cid = ' . $this->m_intCid . '
				LEFT JOIN report_new_instances rni ON rni.module_id = m.id AND rni.cid = ' . $this->m_intCid . '
			WHERE
				m.id >= ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '
				AND COALESCE( rg.id, ri.id, rni.id ) IS NULL
		)';
	}

	/**
	 * @param $intCurrentUserId
	 * @param CDatabase $objDatabase
	 * @param bool $boolReturnSqlOnly
	 * @return bool|mixed
	 */
	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = str_replace( 'RETURNING id', 'RETURNING id, module_id', parent::insert( $intCurrentUserId, $objDatabase, true ) );

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert report instance record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		} else {
			if( 0 < $objDataset->getRecordCount() ) {
				while( false == $objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					if( true == isset( $arrmixValues['id'] ) ) $this->setId( $arrmixValues['id'] );
					if( true == isset( $arrmixValues['module_id'] ) ) $this->setModuleId( $arrmixValues['module_id'] );
					$objDataset->next();
				}
			}
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolHardDelete = false ) {
		if( true == $boolHardDelete ) return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( date( 'm/d/Y H:i:s' ) );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function updateToNewestVersion( $intCurrentUserId, $objDatabase ) {
		$objLatestVersion = \Psi\Eos\Entrata\CReportVersions::createService()->fetchLatestReportVersionByReportIdByCid( $this->getReportId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objLatestVersion, 'CReportVersion' ) ) {
			if( $objLatestVersion->getId() == $this->getReportVersionId() ) {
				return true;
			}
			$this->setReportVersionId( $objLatestVersion->getId() );
		}
		return $this->update( $intCurrentUserId, $objDatabase );
	}

	public function valDuplicateReport( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;
		if( true == empty( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please select report to be added.' ) );
		} else {

			$strWhere = '
				SELECT
					*
				FROM
					report_instances
				WHERE
					lower( name ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $this->getName() ) ) . '\'
					AND report_group_id = ' . ( int ) $this->getReportGroupId() . '
					AND report_version_id = ' . ( int ) $this->getReportVersionId() . '
					AND deleted_on IS NULL
					AND cid = ' . ( int ) $this->getCid();

			if( VALIDATE_UPDATE == $strAction ) {
				$strWhere .= ' AND id != ' . ( int ) $this->getId();
			}

			if( false == is_null( $objDatabase ) ) {
				$arrobjExistingReportInstances = \Psi\Eos\Entrata\CReportInstances::createService()->fetchReportInstances( $strWhere, $objDatabase );
				if( true == valArr( $arrobjExistingReportInstances, 'CReportInstance' ) ) {
					$boolIsValid = $this->valReportForPermission( $arrobjExistingReportInstances, $objDatabase );
					if( false == $boolIsValid ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Duplicate Report is getting added under same Report Group.' ) ) );
					}
				}

			}
		}
		return $boolIsValid;
	}

	/**
	 * @param CReportInstance[] $arrobjExistingReportInstances
	 * @param $objDatabase
	 * @return bool
	 */
	public function valReportForPermission( $arrobjExistingReportInstances, $objDatabase ) {
		$boolIsValid = false;

		$arrobjReports = \Psi\Eos\Entrata\CReports::createService()->fetchReportsByReportInstanceIdsByCid( array_keys( $arrobjExistingReportInstances ), $this->m_intCid, $objDatabase );

		if( true == valArr( $arrobjReports ) ) {
			foreach( $arrobjReports as $objReport ) {
				foreach( $objReport->getValidationCallbacks() AS $strFunctionName => $arrstrArgs ) {

					if( true == valStr( $strFunctionName ) ) {
						if( 'isAllowedClient' == $strFunctionName ) {
							$boolIsValid = ( false == is_null( $this->m_intCid ) ) && ( true == in_array( $this->m_intCid, $arrstrArgs ) ) ? false : true;
						}
					} else {
						$boolIsValid = false;
					}

					if( false == $boolIsValid ) {
						break;
					}
				}
			}
		}
		return $boolIsValid;
	}

}
