<?php

class CDataExport extends CBaseDataExport {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataExportScheduleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataExportStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getStartDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileSize() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDownloadedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>