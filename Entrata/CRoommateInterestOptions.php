<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommateInterestOptions
 * Do not add any new functions to this class.
 */

class CRoommateInterestOptions extends CBaseRoommateInterestOptions {

	public static function fetchRoommateInterestOptionsByRoommateInterestIdByCid( $intRoommateInterestId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intRoommateInterestId ) || false == is_numeric( $intCid ) ) return false;

		$strSql = 'SELECT * FROM roommate_interest_options WHERE cid = ' . ( int ) $intCid . ' AND roommate_interest_id = ' . ( int ) $intRoommateInterestId . ' ORDER BY order_num';

		return self::fetchRoommateInterestOptions( $strSql, $objDatabase );
	}

	public static function fetchRoommateInterestOptionsByIdsByCid( $arrintOptionIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintOptionIds ) || false == is_numeric( $intCid ) ) return false;

		$strSql = 'SELECT
						rio.id,
						rio.roommate_interest_id,
						rio.system_code,
						rio.answer,
						rio.details
					FROM
						roommate_interest_options rio
					WHERE
						rio.cid = ' . ( int ) $intCid . '
						AND rio.id IN ( ' . implode( ',', $arrintOptionIds ) . ' ) ';

		return self::fetchRoommateInterestOptions( $strSql, $objDatabase );
	}

	public static function fetchRoommateInterestOptionsWithOrderByCid( $intCid, $objDatabase ) {

		if( false == is_numeric( $intCid ) ) return false;

		$strSql = 'SELECT rio.* FROM roommate_interest_options rio WHERE rio.cid = ' . ( int ) $intCid . ' AND rio.is_published = 1 ORDER BY rio.roommate_interest_id, rio.order_num';

		return parent::fetchObjects( $strSql, 'CRoommateInterestOption', $objDatabase );
	}

}
?>