<?php

use Psi\Eos\Entrata\CScheduledEmailAddressFilters;

class CScheduledEmailAddressFilter extends CBaseScheduledEmailAddressFilter {

	protected $m_intPropertyId;
	protected $m_intScheduledEmailTypeId;

	/**
	 * Set Values functions
	 */

	public function setValues( $arrintValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrintValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrintValues['property_id'] ) ) $this->setPropertyId( $arrintValues['property_id'] );
		if( true == isset( $arrintValues['scheduled_email_type_id'] ) ) $this->setScheduledEmailTypeId( $arrintValues['scheduled_email_type_id'] );

		return;
	}

	/**
	 * Setter functions
	 */

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setScheduledEmailTypeId( $intScheduledEmailTypeId ) {
		$this->m_intScheduledEmailTypeId = $intScheduledEmailTypeId;
	}

	/**
	 * Get functions
	 */

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getScheduledEmailTypeId() {
		return $this->m_intScheduledEmailTypeId;
	}

	/**
	 * Validations functions
	 */

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property details not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client details not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledEmailId() {
		$boolIsValid = true;

		if( true == is_null( $this->getScheduledEmailId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_id', 'Scheduled email details not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valIsAlreadyUnsubscribed( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->fetchScheduledEmailAddressFilter( $objDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'User already unsubscribed', E_USER_ERROR ) );
		}

		return $boolIsValid;
	}

	public function valRecipient( $objClientDatabase ) {
		$boolIsValid = true;

		switch( $this->getScheduledEmailTypeId() ) {
			case CScheduledEmailType::RESIDENTS:
				if( false == is_null( $this->getCustomerId() ) && 0 < strlen( $this->getCustomerId() ) ) {

					$intCustomerCount = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerCountByIdByCid( $this->getCustomerId(), $this->getCid(), $objClientDatabase );

					if( true == is_null( $intCustomerCount ) || 0 >= $intCustomerCount ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Customer details is invalid.' ) );
					}
				} else {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Customer details is not provided.' ) );
				}
				break;

			case CScheduledEmailType::PROSPECTS:
				if( false == is_null( $this->getApplicantId() ) && 0 < strlen( $this->getApplicantId() ) ) {

					$intApplicantCount = CApplicants::fetchApplicantCountByIdByCid( $this->getApplicantId(), $this->getCid(), $objClientDatabase );

					if( true == is_null( $intApplicantCount ) || 0 >= $intApplicantCount ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Applicant details is invalid.' ) );
					}
				} else {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Applicant details is not provided.' ) );
				}
				break;

			case CScheduledEmailType::EMPLOYEES:
				if( false == is_null( $this->getCompanyEmployeeId() ) && 0 < strlen( $this->getCompanyEmployeeId() ) ) {

					$intCompanyEmployeeCount = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeCountByIdByCid( $this->getCompanyEmployeeId(), $this->getCid(), $objClientDatabase );

					if( true == is_null( $intCompanyEmployeeCount ) || 0 >= $intCompanyEmployeeCount ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Company employee details is invalid.' ) );
					}
				} else {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Company employee details not provided.' ) );
				}
				break;

			default:
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Recipient details not provided.' ) );
				break;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'message_center_unsubscribe':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valScheduledEmailId();
				$boolIsValid &= $this->valRecipient( $objClientDatabase );
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

            default:
            	$boolIsValid = true;
            	break;
		}

		return $boolIsValid;
	}

	public function fetchScheduledEmailAddressFilter( $objDatabase ) {

		if( false == is_null( $this->getCustomerId() ) ) {
			$objExistingScheduledEmailAddressFilter = CScheduledEmailAddressFilters::createService()->fetchScheduledEmailAddressFilterByCustomerIdByCid( $this->m_intCustomerId, $this->getCid(), $objDatabase );
		} elseif( false == is_null( $this->getCompanyEmployeeId() ) ) {
			$objExistingScheduledEmailAddressFilter = CScheduledEmailAddressFilters::createService()->fetchScheduledEmailAddressFilterByCompanyEmployeeIdByCid( $this->m_intCompanyEmployeeId, $this->getCid(), $objDatabase );
		} else {
			$objExistingScheduledEmailAddressFilter = CScheduledEmailAddressFilters::createService()->fetchScheduledEmailAddressFilterByApplicantIdByCid( $this->m_intApplicantId, $this->getCid(), $objDatabase );
		}

		return $objExistingScheduledEmailAddressFilter;
	}

}
?>