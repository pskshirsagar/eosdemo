<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportTypes
 * Do not add any new functions to this class.
 */

class CReportTypes extends CBaseReportTypes {

    public static function fetchReportTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CReportType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchReportType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CReportType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

	public static function fetchReportTypesByID( $arrintReportTypeId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						report_types WHERE ID IN (' . implode( ',', $arrintReportTypeId ) . ') ORDER BY id';
		return fetchData( $strSql, $objDatabase );
	}

}
?>