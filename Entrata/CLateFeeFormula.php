<?php

class CLateFeeFormula extends CBaseLateFeeFormula {

	protected $m_boolIsDefault;
	protected $m_intLeaseId;
	protected $m_intPropertyId;
	protected $m_intActiveLeasesCount;
	protected $m_arrobjExceptionLateFeeFormulas;
	protected $m_boolChargeFirstDayLateFee;
	protected $m_boolChargeDailyLateFee;

	protected $m_boolFirstDayUseGreaterOfFixedAndPercentage;
	protected $m_boolDailyUseGreaterOfFixedAndPercentage;

    public function __construct() {
        parent::__construct();
        $this->m_intDailyLateFeeStartDay = $this->m_intFirstLateDay + 1;
	    $this->m_boolFirstDayUseGreaterOfFixedAndPercentage = false;
	    $this->m_boolDailyUseGreaterOfFixedAndPercentage = false;
        return;
    }

    /**
     * Get Functions
     */

    public function getIsDefault() {
        return $this->m_boolIsDefault;
    }

    public function getLeaseId() {
    	return $this->m_intLeaseId;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getExceptionLateFeeFormula() {
    	return $this->m_arrobjExceptionLateFeeFormula;
    }

    public function getActiveLeasesCount() {
    	return $this->m_intActiveLeasesCount;
    }

	public function getChargeFirstDayLateFee() {
		return $this->m_boolChargeFirstDayLateFee;
	}

	public function getChargeDailyLateFee() {
		return $this->m_boolChargeDailyLateFee;
	}

	public function getFirstDayUseGreaterOfFixedAndPercentage() {
		return $this->m_boolFirstDayUseGreaterOfFixedAndPercentage;
	}

	public function getDailyUseGreaterOfFixedAndPercentage() {
		return $this->m_boolDailyUseGreaterOfFixedAndPercentage;
	}

	/**
     * Set Functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['is_default'] ) ) $this->setIsDefault( $arrmixValues['is_default'] );
    	if( true == isset( $arrmixValues['lease_id'] ) ) $this->setLeaseId( $arrmixValues['lease_id'] );
    	if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['active_leases_count'] ) ) $this->setActiveLeasesCount( $arrmixValues['active_leases_count'] );
	    if( true == isset( $arrmixValues['charge_first_day_late_fee'] ) ) $this->setChargeFirstDayLateFee( $arrmixValues['charge_first_day_late_fee'] );
	    if( true == isset( $arrmixValues['charge_daily_late_fee'] ) ) $this->setChargeDailyLateFee( $arrmixValues['charge_daily_late_fee'] );
	    if( true == isset( $arrmixValues['first_day_use_greater_of_fixed_and_percentage'] ) ) $this->setFirstDayUseGreaterOfFixedAndPercentage( $arrmixValues['first_day_use_greater_of_fixed_and_percentage'] );
	    if( true == isset( $arrmixValues['daily_use_greater_of_fixed_and_percentage'] ) ) $this->setDailyUseGreaterOfFixedAndPercentage( $arrmixValues['daily_use_greater_of_fixed_and_percentage'] );
	    return;
    }

	public function setIsDefault( $boolIsDefault ) {
    	$this->m_boolIsDefault = CStrings::strToBool( $boolIsDefault );
    }

    public function setLeaseId( $intLeaseId ) {
    	$this->m_intLeaseId = $intLeaseId;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setExceptionLateFeeFormula( $objExceptionLateFeeFormula ) {
    	$this->m_arrobjExceptionLateFeeFormula[] = $objExceptionLateFeeFormula;
    }

    public function setActiveLeasesCount( $intActiveLeasesCount ) {
    	$this->m_intActiveLeasesCount = $intActiveLeasesCount;
    }

	public function setChargeFirstDayLateFee( $boolChargeFirstDayLateFee ) {
		$this->m_boolChargeFirstDayLateFee = CStrings::strToBool( $boolChargeFirstDayLateFee );
	}

	public function setChargeDailyLateFee( $boolChargeDailyLateFee ) {
		$this->m_boolChargeDailyLateFee = CStrings::strToBool( $boolChargeDailyLateFee );
	}

	public function setFirstDayUseGreaterOfFixedAndPercentage( $boolFirstDayUseGreaterOfFixedAndPercentage ) {
		$this->set( 'm_boolFirstDayUseGreaterOfFixedAndPercentage', CStrings::strToBool( $boolFirstDayUseGreaterOfFixedAndPercentage ) );
	}

	public function setDailyUseGreaterOfFixedAndPercentage( $boolDailyUseGreaterOfFixedAndPercentage ) {
		$this->set( 'm_boolDailyUseGreaterOfFixedAndPercentage', CStrings::strToBool( $boolDailyUseGreaterOfFixedAndPercentage ) );
	}

    /**
     * Create Functions
     */

    public function getLastDayOfMonthByDate( $strDate ) {

		$intLastDayOfMonth = date( 't', strtotime( $strDate ) );
	    return $intLastDayOfMonth;
    }

	public function createLateFeebatchDetails( $arrstrLeaseWithBalance, $fltMonthlyLateFeesTotal, $objLease, $strTransactionDate, $boolIsWaviedOrRetainedLateFee = false, $arrmixPropertyHolidays = NULL ) {

		$objLateFeebatchDetail = new CLateFeeBatchDetail();

		$boolIsOneTime  = false;
		$fltBaseAmount  = $arrstrLeaseWithBalance['balance_due'];
		$fltDailyPostedLateFeeTotal = $arrstrLeaseWithBalance['posted_late_fee_amount'];

		$intLastDayOfCurrentMonth = $this->getLastDayOfMonthByDate( $strTransactionDate );
		$intFirstLateDay          = $this->getFirstLateDay();

		if( $intFirstLateDay > $intLastDayOfCurrentMonth ) {
			$intFirstLateDay      = $intLastDayOfCurrentMonth;
		}

		$strFirstLateDate = date( 'm', strtotime( $strTransactionDate ) ) . '/' . $intFirstLateDay . '/' . date( 'Y', strtotime( $strTransactionDate ) );

		$boolCalculateFirstDay = true;
		if( true == $boolIsWaviedOrRetainedLateFee ) {
			$boolCalculateFirstDay = false;
		}

		if( 0 < $this->getFirstLateDay() && date( 'd', strtotime( $strTransactionDate ) ) > $intFirstLateDay && 0 >= $fltMonthlyLateFeesTotal && true == $boolCalculateFirstDay ) {
			$strFirstLateDate = $this->getValidLateFeeFirstDate( $strFirstLateDate, $arrmixPropertyHolidays );
		}

		if( 0 < $this->getFirstLateDay() && ( date( 'd', strtotime( $strTransactionDate ) ) == $intFirstLateDay
		                                      || strtotime( date( 'm/d/Y', strtotime( $strFirstLateDate ) ) ) == strtotime( $strTransactionDate ) ) ) {
			$fltPercent = ( float ) $this->getFixedPercent();
			$fltAmount = ( float ) $this->getFixedAmount();
			$intArCodeId = $this->getFirstDayArCodeId();
			$boolIsOneTime = true;

			if( true == in_array( $this->getFirstDayLateFeeCalculationMethodId(), CLateFeeCalculationType::$c_arrintArFormulaCalculationTypes ) ) {
				$fltBaseAmount = $arrstrLeaseWithBalance['first_day_normalized_amount'];
			} elseif( false == $this->getBaseChargeOnAmountDue() ) {
				$fltBaseAmount = $arrstrLeaseWithBalance['charge_amount'];
			}

		} elseif( 0 < $this->getDailyLateFeeStartDay() && date( 'd', strtotime( $strTransactionDate ) ) >= $this->getDailyLateFeeStartDay() && date( 'd', strtotime( $strTransactionDate ) ) <= $this->getDailyLateFeeEndDay() ) {
			$fltPercent = ( float ) $this->getPercentPerDay();
			$fltAmount = ( float ) $this->getAmountPerDay();
			$intArCodeId = $this->getDailyArCodeId();

			if( true == in_array( $this->getDailyLateFeeCalculationMethodId(), CLateFeeCalculationType::$c_arrintArFormulaCalculationTypes ) ) {
				$fltBaseAmount = $arrstrLeaseWithBalance['daily_normalized_amount'];
			} elseif( false == $this->getBaseChargeOnAmountDue() ) {
				$fltBaseAmount = $arrstrLeaseWithBalance['charge_amount'];
			}

		} else {
			return;
		}

		if( false == valId( $intArCodeId ) ) {
			return;
		}

		$fltPrecent = ( $fltPercent * $fltBaseAmount * .01 );

		if( ( CLateFeeCalculationType::FIXED_AMOUNT == $this->getFirstDayLateFeeCalculationMethodId() && true == $boolIsOneTime ) ||
		    ( CLateFeeCalculationType::FIXED_AMOUNT == $this->getDailyLateFeeCalculationMethodId() && false == $boolIsOneTime ) ) {
			$fltTransactionAmount = $fltAmount;
			$strMemo = __( 'Type: Fixed Amount' );
		} elseif( ( CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE == $this->getFirstDayLateFeeCalculationMethodId() && true == $boolIsOneTime ) ||
		          ( CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE == $this->getDailyLateFeeCalculationMethodId() && false == $boolIsOneTime ) ) {
			$fltTransactionAmount = $fltPrecent;
			$strMemo = __( 'Type: Percent of Late Balance' );
		} elseif( ( CLateFeeCalculationType::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE == $this->getFirstDayLateFeeCalculationMethodId() && true == $boolIsOneTime ) ||
		          ( CLateFeeCalculationType::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE == $this->getDailyLateFeeCalculationMethodId() && false == $boolIsOneTime ) ) {

			if( $fltAmount > $fltPrecent ) {
				$fltTransactionAmount = number_format( $fltAmount, 2 );
			} else {
				$fltTransactionAmount = number_format( $fltPrecent, 2 );
			}

			$strMemo = __( 'Type: Greater of Fixed Amount or Percent of Late Balance' );
		} elseif( ( CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE == $this->getFirstDayLateFeeCalculationMethodId() && true == $boolIsOneTime ) ||
		          ( CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE == $this->getDailyLateFeeCalculationMethodId() && false == $boolIsOneTime ) ) {

			$intBalanceThreshold = ( true == $boolIsOneTime ) ? $this->getFixedPercentThreshold() : $this->getDailyPercentThreshold();

			if( $fltBaseAmount > $intBalanceThreshold ) {
				$fltTransactionAmount = $fltAmount + ( $fltPercent * ( $fltBaseAmount - $intBalanceThreshold ) * .01 );
			} else {
				$fltTransactionAmount = $fltAmount;
			}

			$strMemo = __( 'Type: Fixed Amount and Percent of Late Balance' );
		} elseif( ( CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE == $this->getFirstDayLateFeeCalculationMethodId() && true == $boolIsOneTime ) ||
		          ( CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE == $this->getDailyLateFeeCalculationMethodId() && false == $boolIsOneTime ) ) {
			$fltTransactionAmount = $fltPrecent;
			$strMemo = __( 'Type: Percent of Charge Code' );
		} elseif( ( CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP == $this->getFirstDayLateFeeCalculationMethodId() && true == $boolIsOneTime ) ||
		          ( CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP == $this->getDailyLateFeeCalculationMethodId() && false == $boolIsOneTime ) ) {
			$fltTransactionAmount = $fltPrecent;
			$strMemo = __( 'Type: Percent of Charge Code Group' );
		} elseif( ( CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE == $this->getFirstDayLateFeeCalculationMethodId() && true == $boolIsOneTime ) ||
		          ( CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE == $this->getDailyLateFeeCalculationMethodId() && false == $boolIsOneTime ) ) {
			$fltTransactionAmount = $fltPrecent;
			$strMemo = __( 'Type: Percent of Charge Code Type' );
		}

		// As the bccomp function doesn't check for digit containing comma(,), remove those from veriables
		$fltDailyPostedLateFeeTotal = str_replace( ',', '', $fltDailyPostedLateFeeTotal );
		$fltTransactionAmount 		= str_replace( ',', '', $fltTransactionAmount );

		// calculate acctual late fee to apply per day
		// if fltDailyPostedLateFeeTotal is greater than fltTransactionAmount amount are same
		if( 1 == bccomp( $fltDailyPostedLateFeeTotal, $fltTransactionAmount, 2 ) || 0 == bccomp( $fltDailyPostedLateFeeTotal, $fltTransactionAmount, 2 ) ) {
			$fltTransactionAmount = 0;
		} elseif( -1 == bccomp( $fltDailyPostedLateFeeTotal, $fltTransactionAmount ) ) {
			// if fltTransactionAmount is greater than fltDailyPostedLateFeeTotal amount are same
			$fltTransactionAmount = $fltTransactionAmount - $fltDailyPostedLateFeeTotal;
		}

		// Enforce round to whole dollar
		// $fltTransactionAmount = ( 1 == $objPropertyLateFeeFormula->getRoundToWholeDollar() ) ? round( $fltTransactionAmount ) : $fltTransactionAmount;

		$fltMinimumBalanceDue   = 0;
		if( CLateFeeCalculationType::FIXED_AMOUNT == $this->getBalanceThresholdCalculationTypeId() ) {
			$fltMinimumBalanceDue = $this->getMinimumBalanceDue();
		} elseif( true == in_array( $this->getBalanceThresholdCalculationTypeId(), CLateFeeCalculationType::$c_arrintArFormulaCalculationTypes ) ) {
			$fltMinimumBalanceDue = ( $this->getMinimumBalanceDue() * $arrstrLeaseWithBalance['min_normalized_amount'] * .01 );
		}

		// Enforce minimum amount due
		if( -1 == bccomp( $arrstrLeaseWithBalance['balance_due'], $fltMinimumBalanceDue, 2 ) ) return false;

		// Enforce maximum monthly fee amount
		$fltMaximumMonthlyFeeAmount = 0;
		$fltChargeAmount            = 0;

		if( true == $this->getBaseChargeOnAmountDue() ) {
			$fltChargeAmount = $arrstrLeaseWithBalance['balance_due'];
		} else {
			$fltChargeAmount = $arrstrLeaseWithBalance['charge_amount'];
		}

		if( CLateFeeCalculationType::FIXED_AMOUNT == $this->getMonthlyMaxCalculationTypeId() ) {
			$fltMaximumMonthlyFeeAmount = $this->getMaxMonthlyLateFeeAmount();
		} elseif( CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE == $this->getMonthlyMaxCalculationTypeId() ) {
			$fltMaximumMonthlyFeeAmount = ( $this->getMaxMonthlyLateFeePercent() * $fltChargeAmount * .01 );
		} elseif( true == in_array( $this->getMonthlyMaxCalculationTypeId(), CLateFeeCalculationType::$c_arrintArFormulaCalculationTypes ) ) {
			$fltMaximumMonthlyFeeAmount = ( $this->getMaxMonthlyLateFeePercent() * $arrstrLeaseWithBalance['max_normalized_amount'] * .01 );
		}

		if( ( false == is_null( $this->getMaxMonthlyLateFeeAmount() ) && 0 != $this->getMaxMonthlyLateFeeAmount() ) || ( false == is_null( $this->getMaxMonthlyLateFeePercent() ) && 0 != $this->getMaxMonthlyLateFeePercent() ) ) {

			if( 1 == bccomp( $fltMonthlyLateFeesTotal, $fltMaximumMonthlyFeeAmount, 2 ) ) return;

			if( 1 == bccomp( ( $fltMonthlyLateFeesTotal + $fltTransactionAmount ), $fltMaximumMonthlyFeeAmount, 2 ) ) {
				$fltTransactionAmount = ( $fltMaximumMonthlyFeeAmount - $fltMonthlyLateFeesTotal );
			}
		}

		$fltTransactionAmount = number_format( $fltTransactionAmount, 2 );

		if ( 0 >= $fltTransactionAmount ) return;

		if( true == $boolIsOneTime ) {
			$strMemo .= '
    		Maximum monthly late fees=> $' . $fltMaximumMonthlyFeeAmount . '
       		Fixed amount => $' . $fltAmount . '
    		Fixed percentage amount [ ' . $fltPercent . '% X $' . $fltBaseAmount . ']=>  $' . number_format( ( $fltPercent * $fltBaseAmount * .01 ), 2 );

		} else {
			$strMemo .= '
    		Maximum monthly late fees=> $' . $fltMaximumMonthlyFeeAmount . '
    		Per day amount=> $' . $fltAmount . '
    		Percentage per day amount[  ' . $fltPercent . '% X $' . $fltBaseAmount . ']=>  $' . number_format( ( $fltPercent * $fltBaseAmount * .01 ), 2 );
		}

		$objLateFeebatchDetail->setIsInitialLateFee( $boolIsOneTime );
		$objLateFeebatchDetail->setLeaseId( $objLease->getId() );
		$objLateFeebatchDetail->setCustomerId( $objLease->getPrimaryCustomerId() );
		$objLateFeebatchDetail->setPropertyId( $objLease->getPropertyId() );
		$objLateFeebatchDetail->setCid( $objLease->getCid() );
		$objLateFeebatchDetail->setLateFeeFormulaId( $this->getId() );
		$objLateFeebatchDetail->setLateFeeActionTypeId( CLateFeeActionType::POST );
		$objLateFeebatchDetail->setMemo( $strMemo );
		$objLateFeebatchDetail->setSuggestedAmount( $fltTransactionAmount );
		$objLateFeebatchDetail->setPostDate( $strTransactionDate );
		$objLateFeebatchDetail->setArCodeId( $intArCodeId );

		return $objLateFeebatchDetail;
	}

    // This function for creating late fee batch detail entry for lease if 'APPLY_LEGAL_FEE_TO_ALL_LATE_FEES' is set for the property.
    // It post legal fee along with first day late fee

    public function createLateFeebatchDetailsForLegalFee( $objLease, $strTransactionDate, $intLegalChargeCode, $fltLegalChargeAmount, $objDatabase = NULL ) {

	    $intLastDayOfCurrentMonth = $this->getLastDayOfMonthByDate( $strTransactionDate );
	    $intFirstLateDay          = $this->getFirstLateDay();

	    if( $intFirstLateDay > $intLastDayOfCurrentMonth ) {
		    $intFirstLateDay      = $intLastDayOfCurrentMonth;
	    }

    	$fltTransactionAmount = 0.0;
    	$objLateFeebatchDetail = new CLateFeeBatchDetail();

    	if( 0 < $this->getFirstLateDay() && date( 'd', strtotime( $strTransactionDate ) ) == $intFirstLateDay ) {

    		$fltTransactionAmount = number_format( $fltLegalChargeAmount, 2 );

    		$strArCodeName = current( \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdsByFieldNamesByCid( ( array ) $intLegalChargeCode, [ 'name ' ], $this->getCid(), $objDatabase ) );
	    	$strMemo = 'Legal charge amount : $' . $fltTransactionAmount . ' against charge code : ' . $strArCodeName;

	    	$objLateFeebatchDetail->setIsInitialLateFee( true );
	    	$objLateFeebatchDetail->setIsLegalFee( true );
	    	$objLateFeebatchDetail->setLeaseId( $objLease->getId() );
	    	$objLateFeebatchDetail->setCustomerId( $objLease->getPrimaryCustomerId() );
	    	$objLateFeebatchDetail->setPropertyId( $objLease->getPropertyId() );
	    	$objLateFeebatchDetail->setCid( $objLease->getCid() );
	    	$objLateFeebatchDetail->setLateFeeFormulaId( $this->getId() );
	    	$objLateFeebatchDetail->setLateFeeActionTypeId( CLateFeeActionType::POST );
	    	$objLateFeebatchDetail->setMemo( $strMemo );
	    	$objLateFeebatchDetail->setPostedAmount( $fltTransactionAmount );
	    	$objLateFeebatchDetail->setSuggestedAmount( $fltTransactionAmount );
	    	$objLateFeebatchDetail->setPostDate( $strTransactionDate );
	    	$objLateFeebatchDetail->setArCodeId( $intLegalChargeCode );
	    	return  $objLateFeebatchDetail;
    	} else {
    		return NULL;
    	}
    }

    public function createLateFeeFormulaCodes() {
    	$objLateFeeFormulaArCode = new CLateFeeFormulaArCode();
	    $objLateFeeFormulaArCode->setLateFeeFormulaId( $this->getId() );
	    $objLateFeeFormulaArCode->setCid( $this->getCid() );

    	return $objLateFeeFormulaArCode;
    }

	public function getValidLateFeeFirstDate( $strFirstLateDate, $arrmixPropertyHolidays ) {

		if( false == $this->getChargeLateFeesOnHolidays() && true == valArr( $arrmixPropertyHolidays )
		    && true == valArr( $arrmixPropertyHolidays[date( 'm/d/Y', strtotime( $strFirstLateDate ) )] ) ) {
			$strFirstLateDate = strtotime( '+1 day', strtotime( $strFirstLateDate ) );
			$strFirstLateDate = date( 'm/d/Y', $strFirstLateDate );
			$strFirstLateDate = $this->getValidLateFeeFirstDate( $strFirstLateDate, $arrmixPropertyHolidays );
		}

		if( false == $this->getChargeLateFeesOnSaturdays() && 6 == date( 'w', strtotime( $strFirstLateDate ) ) ) {
			$strFirstLateDate = strtotime( '+1 day', strtotime( $strFirstLateDate ) );
			$strFirstLateDate = date( 'm/d/Y', $strFirstLateDate );
			$strFirstLateDate = $this->getValidLateFeeFirstDate( $strFirstLateDate, $arrmixPropertyHolidays );
		}

		if( false == $this->getChargeLateFeesOnSundays() && 0 == date( 'w', strtotime( $strFirstLateDate ) ) ) {
			$strFirstLateDate = strtotime( '+1 day', strtotime( $strFirstLateDate ) );
			$strFirstLateDate = date( 'm/d/Y', $strFirstLateDate );
			$strFirstLateDate = $this->getValidLateFeeFirstDate( $strFirstLateDate, $arrmixPropertyHolidays );
		}

		return $strFirstLateDate;

	}

   /**
    * Validation Functions
    */

    public function valName( $intPropertyId, $objDatabase = NULL ) {
        $boolIsValid = true;
    	if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );

        } elseif( true == valObj( $objDatabase, 'CDatabase' ) ) {

       		$objLateFeeFormula = \Psi\Eos\Entrata\CLateFeeFormulas::createService()->fetchLateFeeFormulaByNameByPropertyIdByCid( $this->getName(), $intPropertyId, $this->getCid(), $objDatabase );
       		if( true == valObj( $objLateFeeFormula, 'CLateFeeFormula' ) && ( int ) $this->getId() != $objLateFeeFormula->getId() ) {
       			$boolIsValid = false;
		        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'The late fee formula named "{%s, 0}" already exists.', [ $this->getName() ] ) ) );
       		}
        }
        return $boolIsValid;
    }

	public function valOccupancyType() {
		$boolIsValid = true;

		if( true == is_null( $this->getOccupancyTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupancy_type_id', __( 'Occupancy Type is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

    public function valLateFeeFormulaId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getParentLateFeeFormulaId() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'late_fee_formula_id', __( 'Parent Late Fee Formula is required.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

	public function valMinimumBalanceDue() {

    	if( ( CLateFeeCalculationType::FIXED_AMOUNT == $this->getBalanceThresholdCalculationTypeId() && true == is_null( $this->getMinimumBalanceDue() ) ) ||
			( true == in_array( $this->getBalanceThresholdCalculationTypeId(), [ CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE ] ) && ( true == is_null( $this->getBalanceThresholdCalculationTypeReferenceId() ) || true == is_null( $this->getMinimumBalanceDue() ) ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_balance_due', __( 'Balance Threshold is required.' ) ) );
			return false;
		}

		if( CLateFeeCalculationType::FIXED_AMOUNT == $this->getBalanceThresholdCalculationTypeId() ) {
			if( false == $this->valAmount( $strFieldName = 'minimum_balance_due', $this->getMinimumBalanceDue() ) ) {
				return false;
			}
		}

		if( true == in_array( $this->getBalanceThresholdCalculationTypeId(), [ CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE ] ) ) {
			if( false == $this->valPercent( $strFieldName = 'minimum_balance_due', $this->getMinimumBalanceDue() ) ) {
				return false;
			}
		}

		return true;
	}

    public function valLateFeeAssessedOn() {
    	$boolIsValid = true;

    	if( 0 == $this->getCurrentChargesOnly() && 0 == $this->getScheduledChargesOnly() ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_charges_only', __( 'You must select atlesat one type.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

	public function valMaxMonthlyLateFeeAmount() {
		if( false == is_null( $this->getMonthlyMaxCalculationTypeId() ) ) {
			if( ( CLateFeeCalculationType::FIXED_AMOUNT == $this->getMonthlyMaxCalculationTypeId() && true == is_null( $this->getMaxMonthlyLateFeeAmount() ) ) ||
			 ( CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE == $this->getMonthlyMaxCalculationTypeId() && true == is_null( $this->getMaxMonthlyLateFeePercent() ) ) ||
			 ( true == in_array( $this->getMonthlyMaxCalculationTypeId(), [ CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE ] ) && ( true == is_null( $this->getMonthlyMaxCalculationTypeReferenceId() ) || true == is_null( $this->getMaxMonthlyLateFeePercent() ) ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_monthly_late_fee_amount', __( 'Monthly Late Fee Limit is required.' ) ) );
				return false;
			}

			if( CLateFeeCalculationType::FIXED_AMOUNT == $this->getMonthlyMaxCalculationTypeId() ) {
				if( false == $this->valAmount( $strFieldName = 'max_monthly_late_fee_amount', $this->getMaxMonthlyLateFeeAmount() ) ) {
					return false;
				}
			}

			if( true == in_array( $this->getMonthlyMaxCalculationTypeId(), [ CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE ] ) ) {
				if( false == $this->valPercent( $strFieldName = 'max_monthly_late_fee_amount', $this->getMaxMonthlyLateFeePercent() ) ) {
					return false;
				}
			}
		}

		return true;
	}

    public function valLegalFeeChargeCodeId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getLegalFeeChargeCodeId() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'legal_fee_charge_code_id', __( 'Legal fee charge code is required.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valLegalFeeAmount() {

    	if( true == is_null( $this->getLegalFeeAmount() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'legal_fee_amount', __( 'Legal fee is required.' ) ) );
    		return false;
    	}

    	return $this->valAmount( $strFieldName = 'legal_fee_amount', $this->getLegalFeeAmount() );
    }

	public function valFirstDayLateFee() {

		if( false == valId( $this->getFirstLateDay() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_late_day', __( 'First Day Late is required.' ) ) );
			return false;
		}

		if( ( CLateFeeCalculationType::FIXED_AMOUNT == $this->getFirstDayLateFeeCalculationMethodId() && true == is_null( $this->getFixedAmount() ) ) ||
			( CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE == $this->getFirstDayLateFeeCalculationMethodId() && true == is_null( $this->getFixedPercent() ) ) ||
			( CLateFeeCalculationType::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE == $this->getFirstDayLateFeeCalculationMethodId() && ( true == is_null( $this->getFixedAmount() ) || true == is_null( $this->getFixedPercent() ) ) ) ||
			( CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE == $this->getFirstDayLateFeeCalculationMethodId() && ( true == is_null( $this->getFixedAmount() ) || true == is_null( $this->getFixedPercent() ) || true == is_null( $this->getFixedPercentThreshold() ) ) ) ||
			( true == in_array( $this->getFirstDayLateFeeCalculationMethodId(), [ CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE ] ) && ( true == is_null( $this->getFirstDayLateFeeCalculationTypeReferenceId() ) || true == is_null( $this->getFixedPercent() ) ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_amount', __( 'First day late fee is required.' ) ) );
				return false;
		}

		if( true == in_array( $this->getFirstDayLateFeeCalculationMethodId(), [ CLateFeeCalculationType::FIXED_AMOUNT, CLateFeeCalculationType::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE ] ) ) {
			if( false == $this->valAmount( $strFieldName = 'fixed_amount', $this->getFixedAmount() ) ) {
				return false;
			}
		}

		if( true == in_array( $this->getFirstDayLateFeeCalculationMethodId(), [ CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE ] ) ) {
			if( false == $this->valPercent( $strFieldName = 'fixed_amount', $this->getFixedPercent() ) ) {
				return false;
			}
		}

		if( CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE == $this->getFirstDayLateFeeCalculationMethodId() ) {
			if( false == $this->valAmount( $strFieldName = 'fixed_amount', $this->getFixedPercentThreshold() ) ) {
				return false;
			}
		}

		return true;
	}

    public function valAmount( $strFieldName, $fltAmount ) {

    	if( false == is_null( $fltAmount ) && 0 >= ( float ) $fltAmount ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Amount must be greater than zero.' ) ) );
    		return false;
    	}

    	return true;
    }

    public function valPercent( $strFieldName, $fltPercent ) {

    	if( false == is_null( $fltPercent ) && 0 >= ( float ) $fltPercent ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, __( 'Percent must be greater than zero.' ) ) );
    		return false;
    	}

    	return true;
    }

    public function valFirstDayArCodeId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getFirstDayArCodeId() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_day_ar_code_id', __( 'First day late fee charge code is required.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valDailyLateFee() {
	    if( ( CLateFeeCalculationType::FIXED_AMOUNT == $this->getDailyLateFeeCalculationMethodId() && true == is_null( $this->getAmountPerDay() ) ) ||
	        ( CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE == $this->getDailyLateFeeCalculationMethodId() && true == is_null( $this->getPercentPerDay() ) ) ||
	        ( CLateFeeCalculationType::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE == $this->getDailyLateFeeCalculationMethodId() && ( true == is_null( $this->getAmountPerDay() ) || true == is_null( $this->getPercentPerDay() ) ) ) ||
	        ( CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE == $this->getDailyLateFeeCalculationMethodId() && ( true == is_null( $this->getAmountPerDay() ) || true == is_null( $this->getPercentPerDay() ) || true == is_null( $this->getDailyPercentThreshold() ) ) ) ||
		    ( true == in_array( $this->getDailyLateFeeCalculationMethodId(), [ CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE ] ) && ( true == is_null( $this->getDailyLateFeeCalculationTypeReferenceId() ) || true == is_null( $this->getPercentPerDay() ) ) ) ) {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount_per_day', __( 'Daily late fee is required.' ) ) );
			    return false;
	    }

	    if( true == in_array( $this->getDailyLateFeeCalculationMethodId(), [ CLateFeeCalculationType::FIXED_AMOUNT, CLateFeeCalculationType::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE ] ) ) {
		    if( false == $this->valAmount( $strFieldName = 'amount_per_day', $this->getAmountPerDay() ) ) {
		    	return false;
		    }
	    }

	    if( true == in_array( $this->getDailyLateFeeCalculationMethodId(), [ CLateFeeCalculationType::PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::GREATER_OF_FIXED_AMOUNT_OR_PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_GROUP, CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE_TYPE ] ) ) {
		    if( false == $this->valPercent( $strFieldName = 'amount_per_day', $this->getPercentPerDay() ) ) {
		    	return false;
		    }
	    }

	    if( CLateFeeCalculationType::FIXED_AMOUNT_AND_PERCENT_OF_LATE_BALANCE == $this->getDailyLateFeeCalculationMethodId() ) {
		    if( false == $this->valAmount( $strFieldName = 'amount_per_day', $this->getDailyPercentThreshold() ) ) {
		    	return false;
		    }
	    }

		return true;
    }

    public function valDailyArCodeId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getDailyArCodeId() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'daily_ar_code_id', __( 'Daily late fee charge code is required.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valDailyLateFeeStartDay() {
    	$boolIsValid = true;

    	if( 1 == $this->getChargeDailyLateFee() ) {

		    if( false == valId( $this->getDailyLateFeeStartDay() ) ) {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'daily_late_fee_start_day', __( 'Daily late fee begin on is required.' ) ) );
			    $boolIsValid = false;
			    return $boolIsValid;
		    }

    		if( $this->getFirstLateDay() >= $this->getDailyLateFeeStartDay() ) {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'daily_late_fee_start_day', __( 'Daily late fee begin on day must be greater than first day late.' ) ) );
			    $boolIsValid = false;
			    return $boolIsValid;
		    }
    	}

    	return $boolIsValid;
    }

    public function valDailyLateFeeEndDay() {
    	$boolIsValid = true;

		if( $this->getDailyLateFeeStartDay() > $this->getDailyLateFeeEndDay() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'daily_late_fee_end_day', ' ' . __( 'Date must be greater than daily late fees begin on date.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valIsDisabled() {
		$boolIsValid = true;

    	if( 1 == $this->getIsDefault() && 1 == $this->getIsDisabled() ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_disabled', __( ' You can not disable default formula.' ) ) );
    		$boolIsValid = false;
		}

    	return $boolIsValid;
    }

    public function valChargeCode( $objDatabase ) {
    	$boolIsValid = true;

    	if( true == $this->getParentLateFeeFormulaId() && false == $this->getIsDisabled() ) {
    		$arrmixAllExceptionLateFeeFormulaCodes = \Psi\Eos\Entrata\CLateFeeFormulaArCodes::createService()->fetchLateFeeFormulaArCodesByParentLateFeeFormulaIdByCid( $this->getParentLateFeeFormulaId(), $this->getCid(), $objDatabase );
    		$arrobjLateFeeFormulaCodes             = \Psi\Eos\Entrata\CLateFeeFormulaArCodes::createService()->fetchPropertyLateFeeFormulaArCodesByLatefeeFormulaIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrmixAllExceptionLateFeeFormulaCodes ) && true == valArr( $arrobjLateFeeFormulaCodes ) ) {
    			$arrintLateFeeFormulaCodes = array_keys( rekeyObjects( 'ArCodeId', $arrobjLateFeeFormulaCodes ) );
    			foreach( $arrmixAllExceptionLateFeeFormulaCodes as $arrmixExceptionLateFeeFormulaCodes ) {
    				if( $arrmixExceptionLateFeeFormulaCodes['late_fee_formula_id'] != $this->getId() && true == in_array( $arrmixExceptionLateFeeFormulaCodes['ar_code_id'], $arrintLateFeeFormulaCodes ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_disabled', __( '{%s, 0} is already using {%s, 1} The same charge code can be used in only one exception formula. Remove the conflicting charge code(s) to enable this formula.', [ $arrmixExceptionLateFeeFormulaCodes['late_fee_formula_name'], $arrmixExceptionLateFeeFormulaCodes['ar_code_name'] ] ) ) );
    					$boolIsValid = false;
    				}
    			}
    		}
    	}

    	return $boolIsValid;
    }

    public function valGraceDays() {

	    if( true == is_null( $this->getGraceDays() ) ) {
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'grace_days', __( 'Days After Post Date Charges Become Eligible For Late Fees is required.' ) ) );
		    return false;
	    }

	    if( false == valId( $this->getGraceDays() ) ) {
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'grace_days', __( 'Days After Post Date Charges Become Eligible For Late Fees must be more than 0. ' ) ) );
		    return false;
	    }

	    return true;
    }

    public function validate( $strAction, $intPropertyId, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName( $intPropertyId, $objDatabase );
	            $boolIsValid &= $this->valOccupancyType();
            	$boolIsValid &= $this->valMinimumBalanceDue();
	            $boolIsValid &= $this->valGraceDays();
	            $boolIsValid &= $this->valMaxMonthlyLateFeeAmount();

				if( 1 == $this->getChargeFirstDayLateFee() ) {
            		$boolIsValid &= $this->valFirstDayLateFee();
            		$boolIsValid &= $this->valFirstDayArCodeId();
        		}

        		if( 1 == $this->getChargeDailyLateFee() ) {
	        		$boolIsValid &= $this->valDailyLateFee();
	            	$boolIsValid &= $this->valDailyArCodeId();
	            	$boolIsValid &= $this->valDailyLateFeeStartDay();
	            	$boolIsValid &= $this->valDailyLateFeeEndDay();
        		}

        		if( 1 == $this->getAllowToApplyLegalFee() ) {
        			$boolIsValid &= $this->valLegalFeeChargeCodeId();
        			$boolIsValid &= $this->valLegalFeeAmount();
        		}
            	break;

            case 'validate_insert_child':
            	$boolIsValid &= $this->valName( $intPropertyId, $objDatabase );
            	$boolIsValid &= $this->valLateFeeFormulaId();
            	$boolIsValid &= $this->valMinimumBalanceDue();
	            $boolIsValid &= $this->valGraceDays();
	            $boolIsValid &= $this->valMaxMonthlyLateFeeAmount();

            	if( 1 == $this->getChargeFirstDayLateFee() ) {
            		$boolIsValid &= $this->valFirstDayLateFee();
            		$boolIsValid &= $this->valFirstDayArCodeId();
            	}

            	if( 1 == $this->getChargeDailyLateFee() ) {
            		$boolIsValid &= $this->valDailyLateFee();
            		$boolIsValid &= $this->valDailyArCodeId();
            		$boolIsValid &= $this->valDailyLateFeeStartDay();
            		$boolIsValid &= $this->valDailyLateFeeEndDay();
            	}

            	if( 1 == $this->getAllowToApplyLegalFee() ) {
            		$boolIsValid &= $this->valLegalFeeChargeCodeId();
            		$boolIsValid &= $this->valLegalFeeAmount();
            	}
            	break;

            case VALIDATE_UPDATE:
            	// $boolIsValid &= $this->valIsDisabled();
        		$boolIsValid &= $this->valName( $intPropertyId, $objDatabase );
	            $boolIsValid &= $this->valOccupancyType();
            	$boolIsValid &= $this->valMinimumBalanceDue();
	            $boolIsValid &= $this->valGraceDays();
	            $boolIsValid &= $this->valMaxMonthlyLateFeeAmount();

				if( 1 == $this->getChargeFirstDayLateFee() ) {
            		$boolIsValid &= $this->valFirstDayLateFee();
            		$boolIsValid &= $this->valFirstDayArCodeId();
        		}

        		if( 1 == $this->getChargeDailyLateFee() ) {
	        		$boolIsValid &= $this->valDailyLateFee();
	            	$boolIsValid &= $this->valDailyArCodeId();
	            	$boolIsValid &= $this->valDailyLateFeeStartDay();
	            	$boolIsValid &= $this->valDailyLateFeeEndDay();
        		}

        		if( 1 == $this->getAllowToApplyLegalFee() ) {
        			$boolIsValid &= $this->valLegalFeeChargeCodeId();
        			$boolIsValid &= $this->valLegalFeeAmount();
        		}
            	break;

            case 'validate_update_child':
            	$boolIsValid &= $this->valName( $intPropertyId, $objDatabase );
            	$boolIsValid &= $this->valMinimumBalanceDue();
	            $boolIsValid &= $this->valGraceDays();
	            $boolIsValid &= $this->valMaxMonthlyLateFeeAmount();

            	if( 1 == $this->getChargeFirstDayLateFee() ) {
            		$boolIsValid &= $this->valFirstDayLateFee();
            		$boolIsValid &= $this->valFirstDayArCodeId();
            	}

            	if( 1 == $this->getChargeDailyLateFee() ) {
            		$boolIsValid &= $this->valDailyLateFee();
            		$boolIsValid &= $this->valDailyArCodeId();
            		$boolIsValid &= $this->valDailyLateFeeStartDay();
            		$boolIsValid &= $this->valDailyLateFeeEndDay();
            	}

            	if( 1 == $this->getAllowToApplyLegalFee() ) {
            		$boolIsValid &= $this->valLegalFeeChargeCodeId();
            		$boolIsValid &= $this->valLegalFeeAmount();
            	}
            	break;

            case VALIDATE_DISABLE:
            	$boolIsValid &= $this->valIsDisabled();
            	$boolIsValid &= $this->valChargeCode( $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function mapKeyValuePair( $arrobjArCodes, $objClientDatabase ) {

    	$strKeyValuePair = '';

	    $strKeyValuePair .= ( true == $this->getIsDisabled() ) ? 'Late Fee Formula Is Disabled::' . $this->getIsDisabled() . '~^~' : 'Late Fee Formula Is Disabled::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getFirstDayArCodeId() && valArr( $arrobjArCodes ) && array_key_exists( $this->getFirstDayArCodeId(), $arrobjArCodes ) ) ? 'First Day Late Fee Charge Code::' . $arrobjArCodes[$this->getFirstDayArCodeId()]->getName() . '~^~' : 'First Day Late Fee Charge Code::' . '~^~';

		$strKeyValuePair .= ( true == $this->getDailyArCodeId() && valArr( $arrobjArCodes ) && array_key_exists( $this->getDailyArCodeId(), $arrobjArCodes ) ) ? 'Daily Late Fee Charge Code::' . $arrobjArCodes[$this->getDailyArCodeId()]->getName() . '~^~' : 'Daily Late Fee Charge Code::' . '~^~';

	    $strKeyValuePair .= ( true == $this->getName() ) ? 'Late Fee Formula Name::' . $this->getName() . '~^~' : 'Late Fee Formula Name::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getDescription() ) ? 'Late Fee Formula Description::' . $this->getDescription() . '~^~' : 'Late Fee Formula Description::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getFixedAmount() ) ? 'First Day Late Fee Amount::' . $this->getFixedAmount() . '~^~' : 'First Day Late Fee Amount::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getAmountPerDay() )? 'Daily Late Fee Amount::' . $this->getAmountPerDay() . '~^~' : 'Daily Late Fee Amount::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getFixedPercent() ) ? 'First Day Late Fee Percent::' . $this->getFixedPercent() . '~^~' : 'First Day Late Fee Percent::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getPercentPerDay() ) ? 'Daily Late Fee Percent::' . $this->getPercentPerDay() . '~^~' : 'Daily Late Fee Percent::' . '~^~';

   		$strKeyValuePair .= ( true == $this->getFirstLateDay() ) ? 	'First Late Day::' . $this->getFirstLateDay() . '~^~' : 'First Late Day::' . '~^~';

   		$strKeyValuePair .= ( true == $this->getDailyLateFeeStartDay() ) ? 'Daily Late Fee Begins::' . $this->getDailyLateFeeStartDay() . '~^~' : 'Daily Late Fee Begins::' . '~^~';

   		$strKeyValuePair .= ( true == $this->getMinimumBalanceDue() ) ? 'Minimun Balance Due::' . $this->getMinimumBalanceDue() . '~^~' : 'Minimun Balance Due::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getMaxMonthlyLateFeeAmount() ) ? 'Maximum Monthly Late Fee Amount::' . $this->getMaxMonthlyLateFeeAmount() . '~^~' : 'Maximum Monthly Late Fee Amount::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getMaxMonthlyLateFeePercent() ) ? 'Maximum Monthly Late Fee Percent::' . $this->getMaxMonthlyLateFeePercent() . '~^~' : 'Maximum Monthly Late Fee Percent::' . '~^~';

   		$strKeyValuePair .= ( true == $this->getScheduledChargesOnly() ) ? 'Scheduled Charges::' . $this->getScheduledChargesOnly() . '~^~' : 'Scheduled Charges::' . '~^~';

    	$strKeyValuePair .= ( true == $this->getAutoPostNsfLateFees() ) ? 'Auto Post Late Fees At NSF::' . $this->getAutoPostNsfLateFees() . '~^~' : 'Auto Post Late Fees At NSF::' . '~^~';

   		$strKeyValuePair .= ( true == $this->getPostFeesToNextPeriod() ) ? 'Post Fees To Next Period::' . $this->getPostFeesToNextPeriod() . '~^~' : 'Post Fees To Next Period::' . '~^~';

   		$strKeyValuePair .= ( true == $this->getChargeFirstDayLateFee() ) ? 'Charge First Day Late Fee::' . $this->getChargeFirstDayLateFee() . '~^~' : 'Charge First Day Late Fee::' . '~^~';

   		$strKeyValuePair .= ( true == $this->getChargeDailyLateFee() ) ? 'Charge Daily Late Fee::' . $this->getChargeDailyLateFee() . '~^~' : 'Charge Daily Late Fee::' . '~^~';

   		$strKeyValuePair .= ( true == $this->getDailyLateFeeEndDay() ) ? 'Daily Late Fee End Day::' . $this->getDailyLateFeeEndDay() . '~^~' : 'Daily Late Fee End Day::' . '~^~';

   		return $strKeyValuePair;

    }

    public function log( $objOldLateFeeFormula, $objNewLateFeeFormula, $arrobjArCodes = NULL, $intCompanyUserId, $objClientDatabase ) {

    	$objTableLog = new CTableLog();

   		$strLateFeeFormulaDiscription = 'Late Fee Formula has been modified.';

   		$strOldLateFeeFormulaKeyValuePair = $objOldLateFeeFormula->mapKeyValuePair( $arrobjArCodes, $objClientDatabase );
   		$strNewLateFeeFormulaKeyValuePair = $objNewLateFeeFormula->mapKeyValuePair( $arrobjArCodes, $objClientDatabase );

   		$objTableLog->insert( $intCompanyUserId, $objClientDatabase, 'late_fee_formulas', $objNewLateFeeFormula->getId(), 'UPDATE', $objNewLateFeeFormula->getCid(), $strOldLateFeeFormulaKeyValuePair, $strNewLateFeeFormulaKeyValuePair, $strLateFeeFormulaDiscription );

   		return true;

    }

    public function adjustLateFeeFormulaDaysForDatabaseTransactions( $strPropertyTimeZone, $strPropertyPostTime ) {
    	$strSystemTimeZone	= date_default_timezone_get();
    	$objTmpDateTime = new DateTime();

	    if( true == valId( $this->getFirstLateDay() ) ) {
		    $objTmpDateTime->setTimezone( new DateTimeZone( $strPropertyTimeZone ) );
		    $objTmpDateTime->setTime( $strPropertyPostTime, 0, 0 );
		    $objTmpDateTime->setDate( ( int ) date( 'Y' ), 1, ( int ) $this->getFirstLateDay() );
		    $objTmpDateTime->setTimezone( new DateTimeZone( $strSystemTimeZone ) );
		    $this->setFirstLateDay( $objTmpDateTime->format( 'd' ) );
	    }

	    if( true == valId( $this->getDailyLateFeeStartDay() ) ) {
		    $objTmpDateTime->setTimezone( new DateTimeZone( $strPropertyTimeZone ) );
		    $objTmpDateTime->setTime( $strPropertyPostTime, 0, 0 );
		    $objTmpDateTime->setDate( ( int ) date( 'Y' ), 1, ( int ) $this->getDailyLateFeeStartDay() );
		    $objTmpDateTime->setTimezone( new DateTimeZone( $strSystemTimeZone ) );
		    $this->setDailyLateFeeStartDay( $objTmpDateTime->format( 'd' ) );
	    }
    }

    public function adjustLateFeeFormulaDaysForView( $strPropertyTimeZone, $strSystemPostTime ) {
    	$strSystemTimeZone	= date_default_timezone_get();
    	$objTmpDateTime = new DateTime();

	    if( true == valId( $this->getFirstLateDay() ) ) {
		    $objTmpDateTime->setTimezone( new DateTimeZone( $strSystemTimeZone ) );
		    $objTmpDateTime->setTime( $strSystemPostTime, 0, 0 );
		    $objTmpDateTime->setDate( ( int ) date( 'Y' ), 1, ( int ) $this->getFirstLateDay() );
		    $objTmpDateTime->setTimezone( new DateTimeZone( $strPropertyTimeZone ) );
		    $this->setFirstLateDay( $objTmpDateTime->format( 'd' ) );
	    }

	    if( true == valId( $this->getDailyLateFeeStartDay() ) ) {
		    $objTmpDateTime->setTimezone( new DateTimeZone( $strSystemTimeZone ) );
		    $objTmpDateTime->setTime( $strSystemPostTime, 0, 0 );
		    $objTmpDateTime->setDate( ( int ) date( 'Y' ), 1, ( int ) $this->getDailyLateFeeStartDay() );
		    $objTmpDateTime->setTimezone( new DateTimeZone( $strPropertyTimeZone ) );
		    $this->setDailyLateFeeStartDay( $objTmpDateTime->format( 'd' ) );
	    }
	}

}
?>