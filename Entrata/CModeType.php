<?php

class CModeType extends CBaseModeType {

	const PROPERTY	                = 1;
	const CORPORATE	                = 2;
	const PROPERTY_AND_CORPORATE	= 3;

	public static $c_arrstrModeTypes = [
		self::PROPERTY		=> 'Property',
		self::CORPORATE		=> 'Corporate',
		self::PROPERTY_AND_CORPORATE		=> 'Property and Corporate'
	];

	public static $c_arrintModeTypeIds = [ self::PROPERTY, self::CORPORATE, self::PROPERTY_AND_CORPORATE ];

}
?>