<?php

class CCustomerIdentificationValue extends CBaseCustomerIdentificationValue {

	protected $m_intApplicationId;
	protected $m_intApplicantId;
	protected $m_intPropertyId;
	protected $m_intCurrentApplicantId;
	protected $m_intPsProductId;

	protected $m_strFileName;
	protected $m_strSystemCode;
	protected $m_strCompanyIdentificationTypeName;

	protected $m_objApplication;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyIdentificationTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getCompanyIdentificationTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_identification_type_id', __( 'Identification Type is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valIdentificationNumber() {
		$boolIsValid = true;
		if( 0 == \Psi\CStringService::singleton()->strlen( $this->getIdentificationNumber() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identification_number', __( 'ID number is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valAdministrativeArea() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getAdministrativeArea() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'administrative_area', __( 'License state/province is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEncryptedValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaskedValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiredIdExpirationDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getIdExpirationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identification_expiration', __( 'ID expiration date is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIdExpirationDate( $objCustomer ) {
		$boolIsValid = true;

		if( true == valStr( $this->getIdExpirationDate() ) ) {
			$mixValidatedIdentificationExpiration = CValidation::checkISODateFormat( $this->getIdExpirationDate(), $boolFormat = true );
			if( false === $mixValidatedIdentificationExpiration ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id_expiration_date', __( 'Expiration date is not valid.' ) ) );
			} else {
				$this->setIdExpirationDate( date( 'm/d/Y', strtotime( $mixValidatedIdentificationExpiration ) ) );
			}

			if( true == $boolIsValid && true == valStr( $objCustomer->getBirthDate() ) && strtotime( $this->getIdExpirationDate() ) <= strtotime( $objCustomer->getBirthDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id_expiration_date', __( 'Expiration date must be greater than birth date.' ) ) );
			}
		}

		return $boolIsValid;

	}

	public function valIdentificationState( $strLabel, $objCustomerAddress, $intIdentificationTypeId ) {
		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		$strErrorLabel = strtolower( str_replace( ' ', '_', ( $strLabel ) ) );

		if( false == $objPostalAddressService->isValid( $objCustomerAddress ) ) {
			$arrobjErrorMsgs = $objPostalAddressService->getErrorMsgs();
			foreach( $arrobjErrorMsgs as $intKey => $objErrorMsg ) {
				if( ( \Psi\Libraries\I18n\PostalAddress\Enum\CPostalAddressErrorCode::MISSING == $objErrorMsg->getCode() ) && ( Psi\Libraries\I18n\PostalAddress\CPostalAddress::ADMINISTRATIVE_AREA == $objErrorMsg->getField() ) ) {
					$arrmixData = $objErrorMsg->getData();
					$strAdministrativeAreaType = $arrmixData['administrative_area_type'];
					$strErrorMessage = __( 'Administrative Area is required' );
					$objAdministrativeAreaErrorMsg = new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_licensing_authority_administrative_area_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification {%s,1} ', [ $strLabel, $strAdministrativeAreaType ] ), 'step_id' => CApplicationStep::BASIC_INFO ] );
					return $objAdministrativeAreaErrorMsg;
				}
			}
		}
	}

	public function validate( $strAction, $objCustomer = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_from_household_people':
				$boolIsValid &= $this->valCompanyIdentificationTypeId();
				$boolIsValid &= $this->valIdExpirationDate( $objCustomer );
				break;

			case 'validate_guest_card_identification_number_details':
				$boolIsValid &= $this->valCompanyIdentificationTypeId();
				$boolIsValid &= $this->valIdentificationNumber();
				$boolIsValid &= $this->valAdministrativeArea();
				$boolIsValid &= $this->valRequiredIdExpirationDate();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setIdentificationNumber( $strPlainIdentificationNumber ) {
		if( false == valStr( $strPlainIdentificationNumber ) ) {
			return;
		}
		if( true == valStr( $this->getIdentificationNumber() ) && $strPlainIdentificationNumber == $this->getMaskedValue() ) return;

		$strPlainIdentificationNumber = getSanitizedFormField( $strPlainIdentificationNumber );
		$this->setEncryptedValue( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainIdentificationNumber, CONFIG_SODIUM_KEY_DL_NUMBER ) );
		$this->setMaskedValue( CEncryption::maskText( $strPlainIdentificationNumber ) );

	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setApplication( $objApplication ) {
		$this->m_objApplication = $objApplication;
	}

	public function setCurrentApplicantId( $intCurrentApplicantId ) {
		$this->m_intCurrentApplicantId = $intCurrentApplicantId;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function setSystemCode( $strSystemCode ) {
		$this->m_strSystemCode = CStrings::strTrimDef( $strSystemCode, 50, NULL, true );
	}

	public function setCompanyIdentificationName( $strCompanyIdentificationTypeName ) {
		$this->m_strCompanyIdentificationTypeName = CStrings::strTrimDef( $strCompanyIdentificationTypeName, 50, NULL, true );
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = CStrings::strTrimDef( $strFileName, NULL, NULL, true );
	}

	public function getIdentificationNumber( $boolIsMaskedIdentificationNumber = false ) {
		if( false == valStr( $this->getEncryptedValue() ) ) {
			return NULL;
		}
		if( true == $boolIsMaskedIdentificationNumber ) {
			return CEncryption::maskText( $this->getIdentificationNumber(), -2 );
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getEncryptedValue(), CONFIG_SODIUM_KEY_DL_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_DL_NUMBER ] );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getApplication() {
		return $this->m_objApplication;
	}

	public function getCurrentApplicantId() {
		return $this->m_intCurrentApplicantId;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function addToDetails( $arrmixData ) {

		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $strKey => $strValue ) {
				$this->setDetailsField( $strKey, $strValue );
			}
		}
	}

	public function getDetail( $strKey ) {
		return $this->getDetailsField( $strKey );
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	public function getCompanyIdentificationTypeName() {
		return $this->m_strCompanyIdentificationTypeName;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['identification_number'] ) ) {
			$this->setIdentificationNumber( $arrmixValues['identification_number'] );
		}

		if( true == isset( $arrmixValues['system_code'] ) ) $this->setSystemCode( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_code'] ) : $arrmixValues['system_code'] );
		if( true == isset( $arrmixValues['company_identification_type_name'] ) ) $this->setCompanyIdentificationName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_identification_type_name'] ) : $arrmixValues['company_identification_type_name'] );
		if( true == isset( $arrmixValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['file_name'] ) : $arrmixValues['file_name'] );
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == valObj( $this->m_objApplication, 'CApplication' ) ) {
			$this->setApplicationId( $this->m_objApplication->getId() );

			if( true == valId( $this->getCurrentApplicantId() ) ) {
				$this->setApplicantId( $this->getCurrentApplicantId() );
			} else {
				$this->setApplicantId( $this->m_objApplication->getPrimaryApplicantId() );
			}

			$this->setPropertyId( $this->m_objApplication->getPropertyId() );
		}

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		$strApplicationAction = VALIDATE_UPDATE;
		if( true == is_null( $this->getId() ) ) {
			$strApplicationAction = VALIDATE_INSERT;
		}

		if( true == $boolReturnSqlOnly ) {
			$strSql = '';

			$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, $strApplicationAction, NULL, $this->getPsProductId() );

			$strSql .= parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			return $strSql;
		} else {

			$boolIsValid = true;

			$boolIsValid &= parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			$objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, $strApplicationAction, NULL, $this->getPsProductId() );

			return $boolIsValid;
		}
	}

}
?>
