<?php

class CFloorplanGroup extends CBaseFloorplanGroup {

	const FLOORPLAN_GROUP_TYPE_WAITLIST = 'waitlist';
	const FLOORPLAN_GROUP_TYPE_MARKETING = 'marketing';

	protected $m_intPropertyId;
	protected $m_intFloorplanCount;

    /**
     * Get Functions
     *
     */

	public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getFloorplanCount() {
    	return $this->m_intFloorplanCount;
	}

    /**
     * Set Functions
     *
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	if( true == isset( $arrmixValues['property_id'] ) )			$this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['no_of_floorplans'] ) )	$this->setFloorplanCount( $arrmixValues['no_of_floorplans'] );

    	return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
    }

    public function setPropertyId( $intPropertyid ) {
    	$this->m_intPropertyId = $intPropertyid;
    }

    public function setFloorplanCount( $intFloorplanCount ) {
    	$this->m_intFloorplanCount = $intFloorplanCount;
    }

    /**
     * Validate Functions
     */

    public function valName( $objDatabase, $strGroupType ) {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Floorplan group name is required.' ) ) );
        } elseif( false == is_null( $this->getName() ) ) {

        	$strName = addslashes( $this->getName() );

	        $objFloorplanGroup = \Psi\Eos\Entrata\CFloorplanGroups::createService()->fetchFloorplanGroupByCidByFloorplanGroupIdByNameByPropertyId( $this->getCid(), $this->getId(), $strName, $this->getPropertyId(), $objDatabase, $strGroupType );

			if( false == is_null( $objFloorplanGroup ) && true == is_object( $objFloorplanGroup ) ) {
				$intId = $this->getId();
				if( true == empty( $intId ) || $this->getId() != $objFloorplanGroup->getId() ) {
					$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name cannot be duplicated.' ) ) );
				}
			}
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished( $strAction, $objDatabase ) {

        $boolIsValid	= true;
		$boolCondition	= false;

		if( true == isset( $strAction ) && VALIDATE_UPDATE == $strAction )
			$boolCondition = ( false == $this->getIsPublished() );

		if( $boolCondition ) {

		    $intPropertyfloorplanGroupCount 	= \Psi\Eos\Entrata\CPropertyFloorplanGroups::createService()->fetchPropertyFloorplanGroupsCountByFloorplanGroupIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		    if( false == is_null( $intPropertyfloorplanGroupCount ) && 0 < $intPropertyfloorplanGroupCount ) {
		    	$boolIsValid	= false;
		    	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', __( '{%s,0} cannot be unpublished, because it is assigned to a property floorplan.', [ $this->getName() ] ) ) );
		    }
		}

        return $boolIsValid;
    }

    public function valDisplayOrder() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL, $strGroupType = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName( $objDatabase, $strGroupType );
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase, $strGroupType );
            	$boolIsValid &= $this->valIsPublished( VALIDATE_UPDATE, $objDatabase );
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>