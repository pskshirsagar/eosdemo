<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultGlDimensionGroups
 * Do not add any new functions to this class.
 */

class CDefaultGlDimensionGroups extends CBaseDefaultGlDimensionGroups {

	public static function fetchDefaultGlDimensionGroups( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultGlDimensionGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultGlDimensionGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultGlDimensionGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>