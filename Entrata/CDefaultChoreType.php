<?php

class CDefaultChoreType extends CBaseDefaultChoreType {

	// AR
	const MOVE_INS_PROCESSED		= 5;
	const MOVE_OUTS_PROCESSED		= 6;
	const TRANSFERS_PROCESSED		= 7;
	const FMO_PROCESSED				= 26;
	const APPLICANT_RENT			= 32;
	const POST_DEPOSIT_INTEREST		= 33;

	// AP
	const INVOICES_APPROVED			= 28;
	const PURCHSE_ORDER_APPROVED	= 29;
	const PAYMENT_APPROVED			= 30;
	const REFUND_DEPOSIT_INTEREST	= 34;

	protected $m_arrmixTaskTypes;

	public static $c_arrintDefaultChoreTypeIds = [
		self::MOVE_INS_PROCESSED,
		self::FMO_PROCESSED,
		self::PAYMENT_APPROVED,
		self::INVOICES_APPROVED,
		self::TRANSFERS_PROCESSED,
		self::MOVE_OUTS_PROCESSED,
		self::PURCHSE_ORDER_APPROVED,
		self::APPLICANT_RENT,
		self::POST_DEPOSIT_INTEREST,
		self::REFUND_DEPOSIT_INTEREST
	];

	/**
	 * Template Functions
	 *
	 */
	public static function assignTemplateConstants( $arrmixTemplateParameters ) {

		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_FMO_PROCESSED']			= self::FMO_PROCESSED;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_INVOICES_APPROVED']		= self::INVOICES_APPROVED;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_MOVE_INS_PROCESSED']		= self::MOVE_INS_PROCESSED;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_MOVE_OUTS_PROCESSED']		= self::MOVE_OUTS_PROCESSED;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_PURCHSE_ORDER_APPROVED']	= self::PURCHSE_ORDER_APPROVED;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_TRANSFERS_PROCESSED']		= self::TRANSFERS_PROCESSED;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_PAYMENT_APPROVED']		= self::PAYMENT_APPROVED;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_APPLICANT_RENT']			= self::APPLICANT_RENT;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_POST_DEPOSIT_INTEREST']	= self::POST_DEPOSIT_INTEREST;
		$arrmixTemplateParameters['DEFAULT_CHORE_TYPE_REFUND_DEPOSIT_INTEREST']	= self::REFUND_DEPOSIT_INTEREST;

		$arrmixTemplateParameters['task_types']									= CDefaultChoreType::createService()->getAllTaskTypes();
		$arrmixTemplateParameters['default_chore_type_ids']						= self::$c_arrintDefaultChoreTypeIds;

		return $arrmixTemplateParameters;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getAllTaskTypes() {

		if( isset( $this->m_arrmixTaskTypes ) ) {
			return $this->m_arrmixTaskTypes;
		}

		$this->m_arrmixTaskTypes	= [
			self::MOVE_INS_PROCESSED		=> __( 'Move-In' ),
			self::MOVE_OUTS_PROCESSED		=> __( 'Move-Out' ),
			self::FMO_PROCESSED				=> __( 'Move-Out' ),
			self::TRANSFERS_PROCESSED		=> __( 'Transfers Processed' ),
			self::INVOICES_APPROVED			=> __( 'Invoices Approved' ),
			self::PURCHSE_ORDER_APPROVED	=> __( 'Purchse Order Approved' ),
			self::APPLICANT_RENT			=> __( 'Applicant Rent' ),
			self::POST_DEPOSIT_INTEREST		=> __( 'Post Deposit Interest' ),
			self::REFUND_DEPOSIT_INTEREST	=> __( 'Refund Deposit Interest' )
		];

		return $this->m_arrmixTaskTypes;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChoreCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSendEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredToLockMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRequiredToAdvanceMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>