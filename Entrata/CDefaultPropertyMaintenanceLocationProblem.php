<?php

class CDefaultPropertyMaintenanceLocationProblem extends CBaseDefaultPropertyMaintenanceLocationProblem {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultMaintenanceLocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultMaintenanceProblemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>