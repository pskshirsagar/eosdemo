<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerExplanations
 * Do not add any new functions to this class.
 */

class CCustomerExplanations extends CBaseCustomerExplanations {

	public static function fetchCustomerExplanationsByCidByPropertyIdByCustomerId( $intCid, $intPropertyId, $intCustomerId, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intCustomerId ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						customer_explanations 
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND customer_id = ' . ( int ) $intCustomerId;

		return parent::fetchCustomerExplanations( $strSql, $objDatabase );
	}

}
?>