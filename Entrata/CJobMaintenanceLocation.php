<?php

class CJobMaintenanceLocation extends CBaseJobMaintenanceLocation {

	protected $m_strMaintenanceLocationName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceLocationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setMaintenanceLocationName( $strMaintenanceLocationName ) {
		$this->m_strMaintenanceLocationName = CStrings::strTrimDef( $strMaintenanceLocationName, 50, NULL, true );
	}

	public function getMaintenanceLocationName() {
		return $this->m_strMaintenanceLocationName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['maintenance_location_name'] ) && $boolDirectSet ) {
			$this->set( 'm_strMaintenanceLocationName', trim( $arrmixValues['maintenance_location_name'] ) );
		} elseif( isset( $arrmixValues['maintenance_location_name'] ) ) {
			$this->setMaintenanceLocationName( $arrmixValues['maintenance_location_name'] );
		}

	}

}
?>