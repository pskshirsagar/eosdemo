<?php

class CApCode extends CBaseApCode {

	protected $m_intQtyOnHand;
	protected $m_intApDetailId;
	protected $m_intArCodeTypeId;
	protected $m_intTotalQtyOnOrder;
	protected $m_intPropertyGroupId;
	protected $m_intApDetailLogId;
	protected $m_intMaintenanceRequestMaterialsId;
	protected $m_intAssetLocationId;

	protected $m_fltExtendedCost;
	protected $m_fltQtyOnOrder;

	protected $m_strArCodeName;
	protected $m_strAccountName;
	protected $m_strPropertyName;
	protected $m_strGlAccountName;
	protected $m_strAssetTypeName;
	protected $m_strApCodeTypeName;
	protected $m_strGlAccountNumber;
	protected $m_strUnitOfMeasureName;
	protected $m_strAssetLocationName;
	protected $m_strAssetGlAccountName;
	protected $m_strApCodeCategoryName;
	protected $m_strAssetGlAccountNumber;
	protected $m_strFormattedAccountNumber;

	protected $m_arrintApHeaderIds;

	protected $m_arrobjApCodes;
	protected $m_arrobjProperties;
	protected $m_arrobjApCatalogItems;
	protected $m_arrobjGlAccountTrees;
	protected $m_arrobjGlAccountProperties;
	protected $m_arrobjCatalogSkuPropertyGroups;

	protected $m_arrmixApCodes;
	protected $m_arrmixImportFiles;

	const JOB_COST      = 3;
	const MAX_LENGTH    = 50;

	/**
	 * Create Functions
	 */

	public function createApCatalogItem() {

		$objApCatalogItemApPayee	= new CApCatalogItem();
		$objApCatalogItemApPayee->setCid( $this->getCid() );

		return $objApCatalogItemApPayee;
	}

	/**
	 * Get Functions
	 */

	public function getPropertyGroupId() {
		return $this->m_intPropertyGroupId;
	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getGlAccountNumber() {
		return $this->m_strGlAccountNumber;
	}

	public function getApCatalogItems() {
		return $this->m_arrobjApCatalogItems;
	}

	public function getApDetailId() {
		return $this->m_intApDetailId;
	}

	public function getMaintenanceRequestMaterialsId() {
		return $this->m_intMaintenanceRequestMaterialsId;
	}

	public function getApCodeCategoryName() {
		return $this->m_strApCodeCategoryName;
	}

	public function getGlAccountProperties() {
		return $this->m_arrobjGlAccountProperties;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getCatalogSkuPropertyGroups() {
		return $this->m_arrobjCatalogSkuPropertyGroups;
	}

	public function getUnitOfMeasureName() {
		return $this->m_strUnitOfMeasureName;
	}

	public function getQtyOnHand() {
		return $this->m_intQtyOnHand;
	}

	public function getAssetId() {
		return $this->m_intAssetId;
	}

	public function getApHeaderIds() {
		return $this->m_arrintApHeaderIds;
	}

	public function getAssetTypeName() {
		return $this->m_strAssetTypeName;
	}

	public function getAssetGlAccountName() {
		return $this->m_strAssetGlAccountName;
	}

	public function getAssetGlAccountNumber() {
		return $this->m_strAssetGlAccountNumber;
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function getApCodeTypeName() {
		return $this->m_strApCodeTypeName;
	}

	public function getFormattedAccountNumber() {
		return $this->m_strFormattedAccountNumber;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function getGlAccountTrees() {
		return $this->m_arrobjGlAccountTrees;
	}

	public function getExtendedCost() {
		return $this->m_fltExtendedCost;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getAssetLocationName() {
		return $this->m_strAssetLocationName;
	}

	public function getApDetailLogId() {
		return $this->m_intApDetailLogId;
	}

	public function getQtyOnOrder() {
		return $this->m_fltQtyOnOrder;
	}

	public function getAssetLocationId() {
		return $this->m_intAssetLocationId;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyGroupId( $intPropertyGroupId ) {
		$this->m_intPropertyGroupId = $intPropertyGroupId;
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = $strGlAccountName;
	}

	public function setGlAccountNumber( $strGlAccountNumber ) {
		$this->m_strGlAccountNumber = $strGlAccountNumber;
	}

	public function setApCatalogItems( $arrobjApCatalogItems ) {
		$this->m_arrobjApCatalogItems = $arrobjApCatalogItems;
	}

	public function setApDetailId( $intApDetailId ) {
		$this->m_intApDetailId = $intApDetailId;
	}

	public function setMaintenanceRequestMaterialsId( $intMaintenanceRequestMaterialsId ) {
		$this->m_intMaintenanceRequestMaterialsId = $intMaintenanceRequestMaterialsId;
	}

	public function setApCodeCategoryName( $strApCodeCategoryName ) {
		$this->m_strApCodeCategoryName = $strApCodeCategoryName;
	}

	public function setGlAccountProperties( $arrobjGlAccountProperties ) {
		$this->m_arrobjGlAccountProperties = $arrobjGlAccountProperties;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setCatalogSkuPropertyGroups( $arrobjCatalogSkuPropertyGroups ) {
		 $this->m_arrobjCatalogSkuPropertyGroups = $arrobjCatalogSkuPropertyGroups;
	}

	public function setUnitOfMeasureName( $strUnitOfMeasureName ) {
		$this->m_strUnitOfMeasureName = CStrings::strTrimDef( $strUnitOfMeasureName, 100, NULL, true );
	}

	public function setQtyOnHand( $intQtyOnHand ) {
		$this->m_intQtyOnHand = $intQtyOnHand;
	}

	public function setAssetId( $intAssetId ) {
		$this->m_intAssetId = $intAssetId;
	}

	public function setApHeaderIds( $arrintApHeaderIds ) {
		$this->m_arrintApHeaderIds = $arrintApHeaderIds;
	}

	public function setAssetTypeName( $strAssetTypeName ) {
		$this->m_strAssetTypeName = $strAssetTypeName;
	}

	public function setAssetGlAccountName( $strAssetGlAccountName ) {
		$this->m_strAssetGlAccountName = $strAssetGlAccountName;
	}

	public function setAssetGlAccountNumber( $strAssetGlAccountNumber ) {
		$this->m_strAssetGlAccountNumber = $strAssetGlAccountNumber;
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = $strArCodeName;
	}

	public function setApCodeTypeName( $strApCodeTypeName ) {
		$this->m_strApCodeTypeName = $strApCodeTypeName;
	}

	public function setFormattedAccountNumber( $strFormattedAccountNumber ) {
		$this->m_strFormattedAccountNumber = $strFormattedAccountNumber;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setArCodeTypeId( $intArCodeTypeId ) {
		$this->m_intArCodeTypeId = $intArCodeTypeId;
	}

	public function setGlAccountTrees( $arrobjGlAccountTrees ) {
		$this->m_arrobjGlAccountTrees = $arrobjGlAccountTrees;
	}

	public function setExtendedCost( $fltExtendedCost ) {
		$this->m_fltExtendedCost = $fltExtendedCost;
	}

	public function setPropertyName( $strPropertyName ) {
		return $this->m_strPropertyName = $strPropertyName;
	}

	public function setAssetLocationName( $strAssetLocationName ) {
		return $this->m_strAssetLocationName = $strAssetLocationName;
	}

	public function setApDetailLogId( $intApDetailLogId ) {
		$this->m_intApDetailLogId = $intApDetailLogId;
	}

	public function setQtyOnOrder( $fltQtyOnOrder ) {
		$this->m_fltQtyOnOrder = CStrings::strToFloatDef( $fltQtyOnOrder, NULL, false, 2 );
	}

	public function setAssetLocationId( $intAssetLocationId ) {
		$this->m_intAssetLocationId = $intAssetLocationId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['gl_account_name'] ) ) $this->setGlAccountName( $arrmixValues['gl_account_name'] );
		if( true == isset( $arrmixValues['gl_account_number'] ) ) $this->setGlAccountNumber( $arrmixValues['gl_account_number'] );
		if( true == isset( $arrmixValues['property_group_id'] ) ) $this->setPropertyGroupId( $arrmixValues['property_group_id'] );
		if( true == isset( $arrmixValues['ap_detail_id'] ) ) $this->setApDetailId( $arrmixValues['ap_detail_id'] );
		if( true == isset( $arrmixValues['maintenance_request_materials_id'] ) ) $this->setMaintenanceRequestMaterialsId( $arrmixValues['maintenance_request_materials_id'] );
		if( true == isset( $arrmixValues['category_name'] ) ) $this->setApCodeCategoryName( $arrmixValues['category_name'] );
		if( true == isset( $arrmixValues['unit_of_measure_name'] ) ) $this->setUnitOfMeasureName( $arrmixValues['unit_of_measure_name'] );
		if( true == isset( $arrmixValues['qty_on_hand'] ) ) $this->setQtyOnHand( $arrmixValues['qty_on_hand'] );
		if( true == isset( $arrmixValues['asset_id'] ) ) $this->setAssetId( $arrmixValues['asset_id'] );
		if( true == isset( $arrmixValues['asset_type_name'] ) ) $this->setAssetTypeName( $arrmixValues['asset_type_name'] );
		if( true == isset( $arrmixValues['asset_gl_account_name'] ) ) $this->setAssetGlAccountName( $arrmixValues['asset_gl_account_name'] );
		if( true == isset( $arrmixValues['asset_gl_account_number'] ) ) $this->setAssetGlAccountNumber( $arrmixValues['asset_gl_account_number'] );
		if( true == isset( $arrmixValues['ar_code_name'] ) ) $this->setArCodeName( $arrmixValues['ar_code_name'] );
		if( true == isset( $arrmixValues['ap_code_type_name'] ) ) $this->setApCodeTypeName( $arrmixValues['ap_code_type_name'] );
		if( true == isset( $arrmixValues['formatted_account_number'] ) ) $this->setFormattedAccountNumber( $arrmixValues['formatted_account_number'] );
		if( true == isset( $arrmixValues['account_name'] ) ) $this->setAccountName( $arrmixValues['account_name'] );
		if( true == isset( $arrmixValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrmixValues['ar_code_type_id'] );
		if( true == isset( $arrmixValues['extended_cost'] ) ) $this->setExtendedCost( $arrmixValues['extended_cost'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['asset_location_name'] ) ) $this->setAssetLocationName( $arrmixValues['asset_location_name'] );
		if( true == isset( $arrmixValues['asset_location_id'] ) ) $this->setAssetLocationId( $arrmixValues['asset_location_id'] );
		if( true == isset( $arrmixValues['ap_detail_log_id'] ) ) $this->setApDetailId( $arrmixValues['ap_detail_log_id'] );

		if( true == isset( $arrmixValues['qty_on_order'] ) ) $this->setQtyOnOrder( $arrmixValues['qty_on_order'] );
		if( true == isset( $arrmixValues['ap_header_ids'] ) ) $this->setApHeaderIds( $arrmixValues['ap_header_ids'] );

		return;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'cid does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valApCodeCategoryId( $objClientDatabase, $boolIsNewCategory = false ) {
		$boolIsValid = true;

		if( false == valId( $this->getApCodeCategoryId() ) && false == $boolIsNewCategory ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_category_id', __( 'Category is required.' ) ) );
			return false;
		}

		if( true == valId( $this->getApCodeCategoryId() ) ) {

			$strSql = 'SELECT
							is_disabled
						FROM
							ap_code_categories
						WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND id = ' . ( int ) $this->getApCodeCategoryId() . '
							AND deleted_by IS NULL';

			$objApCodeCategory = \Psi\Eos\Entrata\CApCodeCategories::createService()->fetchApCodeCategory( $strSql, $objClientDatabase );

			if( 0 == $this->getIsDisabled() && 1 == $objApCodeCategory->getIsDisabled() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_disabled', __( 'Can not add catalog item(s) in disabled category.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDebitGlAccountId( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == in_array( $this->getAssetTypeId(), [ CAssetType::INVENTORY_INVENTORIED_AND_TRACKED, CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED ] ) && false == valId( $this->getItemGlAccountId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( 'AP GL account is required.' ) ) );
			return false;
		}

		// gl_account_usage_type must be of type ACCUMULATED_DEPRECIATION
		$objDebitGlAccount = CGlAccounts::fetchCustomGlAccountByIdByCid( $this->getCid(), $this->getItemGlAccountId(), $objClientDatabase );

		if( true == valObj( $objDebitGlAccount, 'CGlAccount' ) && false == is_null( $objDebitGlAccount->getDisabledBy() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( 'AP GL account is disabled.' ) ) );
			return false;
		}

		if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->getAssetTypeId() && true == valObj( $objDebitGlAccount, 'CGlAccount' )
			&& CGlAccountUsageType::FIXED_ASSET != $objDebitGlAccount->getGlAccountUsageTypeId() ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( 'Selected AP GL account usage type should be fixed asset.' ) ) );
			return false;
		} elseif( CAssetType::INVENTORY_INVENTORIED_AND_TRACKED == $this->getAssetTypeId() && true == valObj( $objDebitGlAccount, 'CGlAccount' )
					&& CGlAccountUsageType::INVENTORY != $objDebitGlAccount->getGlAccountUsageTypeId() ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( 'Selected AP GL account usage type should be inventory.' ) ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valCreditGlAccountId( $objClientDatabase ) {

		$boolIsValid = true;

		if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->getAssetTypeId() && true == is_null( $this->getApGlAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_gl_account_id', __( 'Depreciation GL account is required.' ) ) );
		}

		// gl_account_usage_type must be of type ACCUMULATED_DEPRECIATION
		$objCreditGlAccount = CGlAccounts::fetchCustomGlAccountByIdByCid( $this->getCid(), $this->getApGlAccountId(), $objClientDatabase );

		if( true == valObj( $objCreditGlAccount, 'CGlAccount' ) && false == is_null( $objCreditGlAccount->getDisabledBy() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_gl_account_id', __( 'Accumulated depreciation GL account is disabled.' ) ) );
			return false;
		}

		if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->getAssetTypeId() && true == valObj( $objCreditGlAccount, 'CGlAccount' )
			&& CGlAccountUsageType::ACCUMULATED_DEPRECIATION != $objCreditGlAccount->getGlAccountUsageTypeId() ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_gl_account_id', __( 'Selected depreciation GL account usage type should be accumulated depreciation.' ) ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valConsumptionGlAccountId( $strAction, $objClientDatabase ) {

		$boolIsValid 			= true;
		$strValidationMessage	= '';
		$arrstrPropertyNames	= [];
		$arrstrApCodeNames	= [];

		$strErrorMessage = 'AP GL Account is required.';
		if( CAssetType::INVENTORY_INVENTORIED_AND_TRACKED == $this->getAssetTypeId() ) {
			$strErrorMessage = 'Consumption GL Account is required.';
		}

		if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED != $this->getAssetTypeId() && false == valId( $this->getConsumptionGlAccountId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'consumption_gl_account_id', __( $strErrorMessage ) ) );
			return false;
		}

		// gl_account_usage_type must be of type FIXED_ASSET
		$objConsumptionGlAccount = CGlAccounts::fetchCustomGlAccountByIdByCid( $this->getCid(), $this->getConsumptionGlAccountId(), $objClientDatabase );

		if( true == valObj( $objConsumptionGlAccount, 'CGlAccount' ) && false == is_null( $objConsumptionGlAccount->getDisabledBy() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'consumption_gl_account_id', __( 'Expense GL account is disabed.' ) ) );
			return false;
		}

		if( false == valArr( $this->getGlAccountProperties() ) ) return $boolIsValid;

		$arrobjGlAccountProperties			= ( array ) $this->getGlAccountProperties();
		$arrobjProperties					= ( array ) $this->getProperties();

		if( 'validate_bulk_update' == $strAction ) {

			$arrobjCatalogSkuPropertyGroups	= ( array ) $this->getCatalogSkuPropertyGroups();

			foreach( $arrobjCatalogSkuPropertyGroups as $intPropertyId => $objCatalogSkuPropertyGroups ) {

				if( false == array_key_exists( $intPropertyId, $arrobjGlAccountProperties ) && 'ALL' != $objCatalogSkuPropertyGroups->getSystemCode() ) {
					$arrstrPropertyNames[]		= $arrobjProperties[$intPropertyId]->getPropertyName();
					$arrstrApCodeNames[]	= $this->getName();
				}
			}

			if( false == valArr( $arrstrPropertyNames ) ) {
				foreach( $arrobjProperties as $intPropertyId => $objProperty ) {

					if( false == array_key_exists( $intPropertyId, $arrobjGlAccountProperties ) ) {
						$arrstrPropertyNames[] 		= $objProperty->getPropertyName();
						$arrstrApCodeNames[]	= $this->getName();
					}
				}
			}

		} else {

			foreach( $arrobjProperties as $intPropertyId => $objProperty ) {

				if( false == array_key_exists( $intPropertyId, $arrobjGlAccountProperties ) ) {
					$arrstrPropertyNames[] = $objProperty->getPropertyName();
				}
			}
		}

		if( true == valArr( $arrstrPropertyNames ) ) {

			$boolIsValid &= false;

			if( true == valArr( $arrstrApCodeNames ) ) {
				$strErrorMessage = __( 'Property(s) \'{%s, 0}\' is not associated with selected AP GL Account for \'{%s, 1}\' catalog item.', [ implode( ', ', $arrstrPropertyNames ), implode( ', ', array_unique( $arrstrApCodeNames ) ) ] );
			} else {
				$strErrorMessage = __( 'Property(s) \'{%s, 0}\' is not associated with selected AP GL Account.', [ implode( ', ', $arrstrPropertyNames ) ] );
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valDepreciationCategoryId( $objClientDatabase ) {

		$boolIsValid 	= true;

		if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->getAssetTypeId() && false == valId( $this->getDepreciationCategoryId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'depreciation_category_id', __( 'Depreciation category is required.' ) ) );
			return false;
		} elseif( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->getAssetTypeId() && true == valId( $this->getDepreciationCategoryId() ) ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND id = ' . ( int ) $this->getDepreciationCategoryId() . '
							AND deleted_by IS NULL';

			$intDepreciationCategoryCount = \Psi\Eos\Entrata\CDepreciationCategories::createService()->fetchRowCount( $strWhere, 'depreciation_categories', $objClientDatabase );

			if( 1 != $intDepreciationCategoryCount ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Selected depreciation category not found.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valAssetTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAssetTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_type_id', __( 'Item type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnitOfMeasureId( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitOfMeasureId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_of_measure_id', __( 'Consumption unit of measure is required.' ) ) );
		} else {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND id = ' . ( int ) $this->getUnitOfMeasureId() . '
							AND deleted_by IS NULL';

			$intUnitOfMeasureCount = \Psi\Eos\Entrata\CUnitOfMeasures::createService()->fetchUnitOfMeasuresCount( $strWhere, $objClientDatabase );

			if( 1 != $intUnitOfMeasureCount ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Selected unit of measure not found.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valName( $objClientDatabase, $arrobjApCodeCategories = [], $strInsert = NULL ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {

			$boolIsValid = false;
			if( 'validate_insert_update_ap_code' == $strInsert ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Item name is required.' ) ) );
			}
		} elseif( true == $boolIsValid && true == isset( $strInsert ) ) {

			if( 'validate_insert_update_ap_code' == $strInsert || 'validate_insert_update_cam_expense_type' == $strInsert ) {
				$strCondition	= ' AND ap_code_type_id = ' . ( int ) $this->getApCodeTypeId();
				$strErrorMsg	= __( 'Name already exists.' );
			} else {
				$strCondition	= ' AND ap_code_category_id = ' . ( int ) $this->getApCodeCategoryId();
				if( true == valArr( $arrobjApCodeCategories ) && true == array_key_exists( $this->getApCodeCategoryId(), $arrobjApCodeCategories ) ) {
					$strErrorMsg	= __( 'Item name \'{%s, 0}\' already exists in selected  category {%s, 1}\'.', [ $this->getName(), $arrobjApCodeCategories[$this->getApCodeCategoryId()]->getName() ] );
				}
			}

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							' . $strCondition . '
							AND id <> ' . ( int ) $this->getId() . '
							AND deleted_by IS NULL';

			if( 0 < \Psi\Eos\Entrata\CApCodes::createService()->fetchApCodeCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $strErrorMsg ) );
			}
		}

		return $boolIsValid;
	}

	public function valApCatalogItems( $arrmixApCatalogItemData, $objClientDatabase ) {

		$arrmixCurrentVendorSkus = $arrmixExistingVendorSkus = [];

		$arrintApPayeeIds               = $arrmixApCatalogItemData['ap_payee_ids'];
		$arrstrVendorSkus               = $arrmixApCatalogItemData['vendor_sku'];
		$arrintSelectedPropertyGroupIds = $arrmixApCatalogItemData['selected_property_group_ids'];

		$strCatalogItemName = $this->getName();

		$boolIsValid 					= true;
		$this->m_arrobjApCatalogItems	= ( array ) $this->getApCatalogItems();

		if( false == valArr( $this->m_arrobjApCatalogItems ) ) {
			return $boolIsValid;
		}

		$arrmixApCatalogItems = ( array ) \Psi\Eos\Entrata\CApCatalogItems::createService()->fetchApCatalogItemsByApPayeeIdsByCidBySkus( $arrintApPayeeIds, $this->getCid(), $arrstrVendorSkus, $objClientDatabase, $this->getId() );

		foreach( $arrmixApCatalogItems  as $arrmixApCatalogItem ) {
			$strVendorSku = \Psi\CStringService::singleton()->strtolower( $arrmixApCatalogItem['vendor_sku'] );
			$arrmixExistingVendorSkus[$arrmixApCatalogItem['ap_payee_id'] . '_' . $strVendorSku]['vendor_sku'] = $strVendorSku;
			$arrmixExistingVendorSkus[$arrmixApCatalogItem['ap_payee_id'] . '_' . $strVendorSku]['catalog_item_name'] = $arrmixApCatalogItem['catalog_item_name'];
		}

		foreach( $this->m_arrobjApCatalogItems as $intRowId => $objApCatalogItem ) {

			$boolIsValid &= $objApCatalogItem->validate( VALIDATE_INSERT, $arrmixCurrentVendorSkus, $arrmixExistingVendorSkus, $strCatalogItemName );

			$strCurrentVendorSku = \Psi\CStringService::singleton()->strtolower( $objApCatalogItem->getVendorSku() );

			$arrmixCurrentVendorSkus[$objApCatalogItem->getApPayeeId() . '_' . $strCurrentVendorSku] = $strCurrentVendorSku;

			if( false == $boolIsValid ) {
				$this->addErrorMsgs( $objApCatalogItem->getErrorMsgs() );
			}

			if( ( true == valArr( $arrintSelectedPropertyGroupIds )
				&& false == array_key_exists( $intRowId . '_' . $objApCatalogItem->getApPayeeId(), $arrintSelectedPropertyGroupIds )
				&& true == valStr( $objApCatalogItem->getApPayeeName() ) )
				|| ( true == valStr( $objApCatalogItem->getApPayeeName() )
					 && false == valArr( $arrintSelectedPropertyGroupIds ) ) ) {

				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group_id', __( 'Select at least one property for vendor \'{%s, 0}.\'', [ $objApCatalogItem->getApPayeeName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valApCode( $strAction, $objClientDatabase = NULL, $arrobjApCodeCategories = [] ) {

		$boolIsValid = true;

		if( VALIDATE_DELETE == $strAction ) {
			if( false == valArr( $arrobjApCodeCategories ) ) return $boolIsValid;

			if( true == valId( $this->getApDetailId() ) && true == array_key_exists( $this->getApCodeCategoryId(), $arrobjApCodeCategories ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_detail_id', __( 'Category {%s, 0} cannot be deleted because it is associated to active Work Order(s) and/or Purchase Order(s).', [ $arrobjApCodeCategories[$this->getApCodeCategoryId()]->getName() ] ) ) );
				$boolIsValid &= false;
			}

		} elseif( true == valId( $this->getApDetailId() ) || true == valId( $this->getMaintenanceRequestMaterialsId() ) || true == valId( $this->getApDetailLogId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_detail_id', __( 'Item cannot be deleted because it is associated to Work Order(s) and/or Purchase Order(s).' ) ) );
			$boolIsValid &= false;
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND ap_code_id = ' . ( int ) $this->getId();

			if( 0 < \Psi\Eos\Entrata\CAssets::createService()->fetchAssetCount( $strWhere, $objClientDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Item \'{%s, 0}\' cannot be deleted because it is associated to assets.', [ $this->getName() ] ) ) );
				$boolIsValid &= false;
			}

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND ap_code_id = ' . ( int ) $this->getId();

			$intAssetTransactionCount = \Psi\Eos\Entrata\CAssetTransactions::createService()->fetchAssetTransactionCount( $strWhere, $objClientDatabase );

			if( 0 < $intAssetTransactionCount ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Item cannot be deleted because it is either associated to inventory adjustment or fixed asstes.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valJobCodeName( $arrstrExistingApCodes ) {
		$boolIsValid = true;

		if( true == valId( $this->getId() ) ) {
			unset( $arrstrExistingApCodes[$this->getId()] );
		}

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		} elseif( false == \Psi\CStringService::singleton()->preg_match( '/^[A-Za-z \d\:\-\.\_\@\$\!\(\)\[\}\^\&\]\{\/\*\#\%\=\+]+$/i', trim( $this->getName() ) ) || false == mb_check_encoding( $this->getName(), 'UTF-8' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Invalid name (It contains special symbol).' ) ) );
		} elseif( true == in_array( \Psi\CStringService::singleton()->strtolower( $this->getName() ), $arrstrExistingApCodes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Job cost code name \'{%s, 0}\' already exists.', [ $this->getName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valJobCodeApCodeTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApCodeTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_type_id', __( ' Type is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valJobCodeApCodeCategoryId( $boolIsFromJob = false ) {
		$boolIsValid = true;

		if( false == valId( $this->getApCodeCategoryId() ) ) {
			if( $boolIsFromJob ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_category_id', __( '&nbsp Job Category is required.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_category_id', __( '&nbsp Group is required.' ) ) );
			}
			return false;
		}
		return $boolIsValid;
	}

	public function valJobCodeDebitGlAccountId( $boolIsFromJob = false ) {
		$boolIsValid = true;

		if( false == valId( $this->getItemGlAccountId() ) ) {
			if( $boolIsFromJob ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( '&nbsp GL account is required.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( '&nbsp Asset GL account is required.' ) ) );
			}
			return false;
		}
		return $boolIsValid;
	}

	public function valJobCodeCreditGlAccountId() {
		$boolIsValid = true;

		if( false == valId( $this->getApGlAccountId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_gl_account_id', __( ' Expense GL account is required.' ) ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valJobCodeApCode( $strAction = NULL ) {
		$boolIsValid = true;
		$strMessage = ( false == is_null( $strAction ) ) ? __( 'Charge code is required.' ) : __( 'Ar code is required.' );
		if( false == valId( $this->getArCodeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', $strMessage ) );
			return false;
		} elseif( false == ( ( $this->getApCodeTypeId() == CApCodeType::INTER_COMPANY_INCOME && $this->getArCodeTypeId() == CArCodeType::OTHER_INCOME ) || ( $this->getApCodeTypeId() == CApCodeType::INTER_COMPANY_LOANS && $this->getArCodeTypeId() == CArCodeType::INTER_COMPANY_LOAN ) ) ) {
			$strMessage = __( 'Please select correct Charge code.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', $strMessage ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valGlAccountName() {
		$boolIsValid = true;

		if( false == valStr( $this->getGlAccountName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_name', __( ' Gl Account is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valInsertImportFile( $objDatabase ) {

		$boolIsValid 					= true;
		$strExtension					= NULL;
		$strUploadTempDir				= PATH_NON_BACKUP_MOUNTS_GLOBAL_TMP;
		$strUploadMaxFilesize			= ini_get( 'upload_max_filesize' );
		$intMaxUpload					= ( int ) str_replace( 'M', '', $strUploadMaxFilesize ) * 1024 * 1024;

		// Validate uploaded CSV file.
		$arrmixImportFiles = $this->getImportFiles();

		if( false == valArr( $arrmixImportFiles ) || UPLOAD_ERR_NO_FILE == $arrmixImportFiles['error'] ) {
			// $boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_file', __( 'No file was uploaded. Select a file to upload.' ) ) );
			return false;
		}

		if( true == array_key_exists( 'name', $arrmixImportFiles ) && true == valStr( $arrmixImportFiles['name'] ) ) {
			$strExtension = pathinfo( $arrmixImportFiles['name'], PATHINFO_EXTENSION );
		}

		$arrstrFileHeaders			= [ 'job_cost_code_name', 'job_cost_code_category', 'gl_account' ];
		$arrmixAllowedExtensions	= [ 'csv' => [ 'mimes' => [ 'application/vnd.ms-excel' ], 'exts' => [ 'csv' ] ] ];

		if( false != valStr( $strExtension ) && false == array_key_exists( $strExtension, $arrmixAllowedExtensions ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type', __( 'Cannot upload other than CSV file.' ) ) );
			return false;
		}

		if( $intMaxUpload < $arrmixImportFiles['size'] || UPLOAD_ERR_OK != $arrmixImportFiles['error'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_size', __( 'Files larger than {%d, 0}B cannot be uploaded.', [ $strUploadMaxFilesize ] ) ) );
			return false;
		}

		$strTempUri = ( string ) uniqid( '', false ) . sprintf( '%3d', mt_rand( 100, 999 ) );

		if( false == move_uploaded_file( $arrmixImportFiles['tmp_name'], $strUploadTempDir . $strTempUri ) || UPLOAD_ERR_NO_TMP_DIR == $arrmixImportFiles['error'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_upload', __( ' {%s, 0} could not be moved to the temporary directory.', [ $arrmixImportFiles['name'] ] ) ) );
			return false;
		}

		// Feed data from CSV file.
		$arrmixRawData	= ( array ) $this->parseCsvFileImport( $strUploadTempDir . $strTempUri );

		if( false == valArr( $arrmixRawData ) || ( true == array_key_exists( 0, $arrmixRawData ) && $arrstrFileHeaders != array_keys( $arrmixRawData[0] ) ) ) {
			$boolIsValid &= false;
		} else {
			// Check for blank lines and columns
			foreach( $arrmixRawData as $intRowNumber => $arrmixRow ) {

				$strRowContents	= implode( '', array_values( $arrmixRow ) );
				if( false == valStr( $strRowContents ) ) {
					$boolIsValid &= false;
					break;
				}

				foreach( array_keys( $arrmixRow ) as $strArrayKey ) {
					if( false == valStr( $strArrayKey ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}
			}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invalid format', __( 'Invalid format. Please refer to sample data format (CSV).' ) ) );
			return $boolIsValid;
		}

		$arrstrExistingApCodes = array_map( function( $arrmixApCode ) {
			return \Psi\CStringService::singleton()->strtolower( $arrmixApCode['name'] );
		}, rekeyArray( 'id', ( array ) \Psi\Eos\Entrata\CApCodes::createService()->fetchAllActiveJobCostCodesByCid( $this->getCid(), $objDatabase ) ) );

		$arrobjGlAccountTrees         = rekeyObjects( 'FormattedAccountNumber', $this->getGlAccountTrees() );
		$arrmixApCodes                = [];
		$arrstrJobCostCodeNames       = [];
		foreach( $arrmixRawData as $intRowNumber => $arrmixRow ) {

			$arrmixRow['job_cost_code_name']        = trim( $arrmixRow['job_cost_code_name'] );
			$arrmixRow['job_cost_code_category']    = trim( $arrmixRow['job_cost_code_category'] );
			$arrmixRow['gl_account']                = trim( $arrmixRow['gl_account'] );

			// Check gl account
			if( false == valStr( $arrmixRow['gl_account'] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'GL Account is required for Row #{%d, 0}. ', [ $intRowNumber + 2 ] ) ) );
				$boolIsValid &= false;
			} elseif( false == array_key_exists( $arrmixRow['gl_account'], $arrobjGlAccountTrees ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'GL Account is not valid for Row #{%d, 0}.', [ $intRowNumber + 2 ] ) ) );
				$boolIsValid &= false;
			}

			// Check Ap Code Category
			if( false == valStr( $arrmixRow['job_cost_code_category'] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'Category name is required for Row #{%d, 0}. ', [ $intRowNumber + 2 ] ) ) );
				$boolIsValid &= false;
			} elseif( self::MAX_LENGTH < \Psi\CStringService::singleton()->strlen( $arrmixRow['job_cost_code_category'] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'Job Cost Code Category Name should not be more than {%d, 0} characters for Row #{%d,1}. ', [ 50, $intRowNumber + 2 ] ) ) );
				$boolIsValid &= false;
			} elseif( false == \Psi\CStringService::singleton()->preg_match( '/^[A-Za-z \d\:\-\.\_\@\$\!\(\)\[\}\^\&\]\{\/\*\#\%\=\+]+$/i', trim( $arrmixRow['job_cost_code_category'] ) ) || false == mb_check_encoding( $arrmixRow['job_cost_code_category'], 'UTF-8' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'Invalid Category name (It contains special symbol) for Row #{%d, 0}. ', [ $intRowNumber + 2 ] ) ) );
			}

			// Check Job code name
			if( false == valStr( $arrmixRow['job_cost_code_name'] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'Job Cost Code Name is required for Row #{%d, 0}. ', [ $intRowNumber + 2 ] ) ) );
				$boolIsValid &= false;
			} elseif( self::MAX_LENGTH < \Psi\CStringService::singleton()->strlen( $arrmixRow['job_cost_code_name'] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'Job Cost Code Name should not be more than #{%d, 0} characters for Row #{%d, 1}. ', [ 50, $intRowNumber + 2 ] ) ) );
				$boolIsValid &= false;
			} elseif( true == in_array( \Psi\CStringService::singleton()->strtolower( $arrmixRow['job_cost_code_name'] ), $arrstrExistingApCodes ) || true == in_array( \Psi\CStringService::singleton()->strtolower( $arrmixRow['job_cost_code_name'] ), $arrstrJobCostCodeNames ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'Job cost code name already exists for row #{%d, 0}. ', [ $intRowNumber + 2 ] ) ) );
				$boolIsValid &= false;
			} elseif( false == \Psi\CStringService::singleton()->preg_match( '/^[A-Za-z \d\:\-\.\_\@\$\!\(\)\[\}\^\&\]\{\/\*\#\%\=\+]+$/i', trim( $arrmixRow['job_cost_code_name'] ) ) || false == mb_check_encoding( $arrmixRow['job_cost_code_name'], 'UTF-8' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'type', __( 'Invalid Job Cost Code Name (It contains special symbol) for Row #{%d, 0}. ', [ $intRowNumber + 2 ] ) ) );
			}

			if( false == $boolIsValid ) {
				return false;
			}

			$arrstrJobCostCodeNames[]   = \Psi\CStringService::singleton()->strtolower( $arrmixRow['job_cost_code_name'] );
			$arrmixApCodes[]            = $arrmixRow;
		}
		$this->setPostedApCodes( $arrmixApCodes );
		return $boolIsValid;
	}

	public function setPostedApCodes( $arrmixPostedApCodes ) {
		$this->m_arrmixApCodes = $arrmixPostedApCodes;
	}

	public function getPostedApCodes() {
		return $this->m_arrmixApCodes;
	}

	public function setImportFiles( $arrmixImportFiles ) {
		$this->m_arrmixImportFiles = $arrmixImportFiles;
	}

	public function getImportFiles() {
		return $this->m_arrmixImportFiles;
	}

	public function validate( $strAction, $objClientDatabase, $arrmixApCatalogItemData = [], $arrobjApCodeCategories = [], $boolIsNewCategory = NULL, $arrstrExistingApCodes = [] ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valAssetTypeId();
				$boolIsValid &= $this->valName( $objClientDatabase, $arrobjApCodeCategories, VALIDATE_INSERT );
				$boolIsValid &= $this->valUnitOfMeasureId( $objClientDatabase );
				$boolIsValid &= $this->valApCodeCategoryId( $objClientDatabase, $boolIsNewCategory );
				$boolIsValid &= $this->valDepreciationCategoryId( $objClientDatabase );
				$boolIsValid &= $this->valDebitGlAccountId( $objClientDatabase );
				$boolIsValid &= $this->valConsumptionGlAccountId( $strAction, $objClientDatabase );
				$boolIsValid &= $this->valApCatalogItems( $arrmixApCatalogItemData, $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valApCode( VALIDATE_DELETE, $objClientDatabase, $arrobjApCodeCategories );
				break;

			case 'validate_bulk_update':
				$boolIsValid &= $this->valName( $objClientDatabase, $arrobjApCodeCategories, $strAction );
				$boolIsValid &= $this->valApCodeCategoryId( $objClientDatabase );
				$boolIsValid &= $this->valConsumptionGlAccountId( 'validate_bulk_update', $objClientDatabase );
				break;

			case 'validate_delete_ap_code':
				$boolIsValid &= $this->valApCode( $strAction, $objClientDatabase, $arrobjApCodeCategories );
				break;

			case 'validate_insert_update_job_code':
				$boolIsValid &= $this->valJobCodeName( $arrstrExistingApCodes );
				$boolIsValid &= $this->valJobCodeApCodeTypeId();
				$boolIsValid &= $this->valJobCodeApCodeCategoryId( true );
				$boolIsValid &= $this->valJobCodeDebitGlAccountId( $objClientDatabase, true );
				$boolIsValid &= $this->valJobCodeCreditGlAccountId( $objClientDatabase );
				break;

			case 'validate_insert_update_ap_code':
				$boolIsValid &= $this->valName( $objClientDatabase, $arrobjApCodeCategories, $strAction );
				$boolIsValid &= $this->valGlAccountName( $strAction );
				$boolIsValid &= $this->valJobCodeApCode( $strAction );
				break;

			case 'import_ap_codes_file':
				$boolIsValid &= $this->valInsertImportFile( $objClientDatabase );
				break;

			case 'validate_insert_update_cam_expense_type':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objClientDatabase, $arrobjApCodeCategories, $strAction );
				$boolIsValid &= $this->valCreditGlAccountId( $objClientDatabase );
				$boolIsValid &= $this->valDebitGlAccountId( $objClientDatabase );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCatalogSkuPropertyGroups( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CCatalogSkuPropertyGroups::createService()->fetchCatalogSkuPropertyGroupsByApCatalogItemIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function parseCsvFileImport( $strFileUri ) {

		$arrmixRawData 			= [];
		$boolHeaderRowSelected	= false;

		if( false == file_exists( $strFileUri ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'no_file', __( 'Application Error: Failed to load File.' ) ) );
			return;
		}

		if( false !== ( $resHandle = CFileIo::fileOpen( $strFileUri, 'r' ) ) ) {

			while( false !== ( $arrmixData = fgetcsv( $resHandle, 0, ',' ) ) ) {

				if( false == $boolHeaderRowSelected ) {
					$boolHeaderRowSelected	 = ftell( $resHandle );
					$arrstrCsvHeader		 = array_map( 'str_replace', array_fill( 0, \Psi\Libraries\UtilFunctions\count( $arrmixData ), ' ' ), array_fill( 0, \Psi\Libraries\UtilFunctions\count( $arrmixData ), '_' ), array_map( [ \Psi\CStringService::singleton(), 'strtolower' ], array_map( 'trim', $arrmixData ) ) );
					continue;
				}

				if( \Psi\Libraries\UtilFunctions\count( $arrstrCsvHeader ) == \Psi\Libraries\UtilFunctions\count( $arrmixData ) ) {
					$arrmixRawData[] = array_combine( $arrstrCsvHeader, $arrmixData );
				}
			}

			fclose( $resHandle );
		}
		return $arrmixRawData;
	}

	public function isCatalogItemUseInTransactions( $objClientDatabase ) {

		$intApDetailsAndAssetTransactionCount = \Psi\Eos\Entrata\CApCodes::createService()->fetchCountOfApDetailsAndAssetTransactionByApCodeIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

		return 0 < $intApDetailsAndAssetTransactionCount;
	}

}
?>