<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTabletButtonTypes
 * Do not add any new functions to this class.
 */

class CTabletButtonTypes extends CBaseTabletButtonTypes {

	public static function fetchTabletButtonTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTabletButtonType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTabletButtonType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTabletButtonType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllTabletButtonTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM tablet_button_types tbt WHERE tbt.is_published = 1 order by order_num';
		return self::fetchTabletButtonTypes( $strSql, $objDatabase );
	}
}
?>