<?php

class CSettingsTemplateAssignment extends CBaseSettingsTemplateAssignment {

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Id does not appear to be valid.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id does not appear to be valid.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property id does not appear to be valid.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertySettingKeyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertySettingKeyId ) || ( 1 > $this->m_intPropertySettingKeyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_setting_key_id', 'Property setting key id does not appear to be valid.' ) );
		}

		return $boolIsValid;
	}

	public function valSettingsTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
			case VALIDATE_INSERT:
				$this->valCid();
				$this->valPropertyId();
				$this->valPropertySettingKeyId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function createSettingsTemplateAssignment( $intPropertySettingKeyId, $intSettingsTemplateId, $intPropertyId, $intCid ) {
		$objSettingsTemplateAssignment = new CSettingsTemplateAssignment();
		$objSettingsTemplateAssignment->setCid( $intCid );
		$objSettingsTemplateAssignment->setPropertyId( $intPropertyId );
		$objSettingsTemplateAssignment->setSettingsTemplateId( $intSettingsTemplateId );
		$objSettingsTemplateAssignment->setPropertySettingKeyId( $intPropertySettingKeyId );

		return $objSettingsTemplateAssignment;
	}

}
?>