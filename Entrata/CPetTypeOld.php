<?php

class CPetTypeOld extends CBasePetTypeOld {

	const DOG 					= 1;
	const CAT 					= 2;
	const BIRDS 				= 3;
	const REPTILES 				= 4;
	const FISH 					= 5;
	const SMALL_PET 			= 6;
	const OTHER 				= 7;
	const SERVICE_ANIMALS		= 8;

	const CODE_PET_TYPE_DOG				= 'DG';
	const CODE_PET_TYPE_CAT				= 'CT';
	const CODE_PET_TYPE_FISH			= 'FS';

}
?>