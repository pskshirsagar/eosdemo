<?php

class CScheduledEmailSubTypes extends CBaseScheduledEmailSubTypes {

	public static function fetchScheduledEmailSubTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CScheduledEmailSubType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScheduledEmailSubType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScheduledEmailSubType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>