<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreStatusTypes
 * Do not add any new functions to this class.
 */

class CChoreStatusTypes extends CBaseChoreStatusTypes {

	public static function fetchChoreStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CChoreStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchChoreStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CChoreStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}
}
?>