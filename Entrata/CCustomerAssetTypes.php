<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAssetTypes
 * Do not add any new functions to this class.
 */

class CCustomerAssetTypes extends CBaseCustomerAssetTypes {

	public static function fetchCustomerAssetTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCustomerAssetType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomerAssetType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCustomerAssetType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedCustomerAssetTypes( $objDatabase ) {
		return self::fetchCustomerAssetTypes( 'SELECT * FROM customer_asset_types WHERE is_published = TRUE ORDER BY order_num', $objDatabase );
	}

	public static function fetchAllCustomerAssetTypes( $objDatabase ) {
		return self::fetchCustomerAssetTypes( 'SELECT * FROM customer_asset_types ORDER BY order_num, name', $objDatabase );
	}

}
?>