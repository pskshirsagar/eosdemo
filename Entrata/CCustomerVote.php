<?php

class CCustomerVote extends CBaseCustomerVote {
	protected $m_strTitle;
	protected $m_strFloorplanName;
	protected $m_strUnitNumber;

	const LIKE 		= 1;
	const DISLIKE 	= -1;
	const COMMENT 	= 0;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['title'] ) ) $this->setTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['floorplan_name'] ) ) $this->setFloorplanName( $arrmixValues['floorplan_name'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) $this->setUnitNumber( $arrmixValues['unit_number'] );
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function setFloorplanName( $strFloorplanName ) {
		$this->m_strFloorplanName = $strFloorplanName;
	}

	public function getFloorplanName() {
		return $this->m_strFloorplanName;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerVoteTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVoteReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valComment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>