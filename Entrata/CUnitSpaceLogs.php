<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceLogs
 * Do not add any new functions to this class.
 */

class CUnitSpaceLogs extends CBaseUnitSpaceLogs {

	public static function fetchUnitSpaceLogsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $intPropertyId, $intCid, $objDatabase ) {
		if( true == is_null( $intUnitSpaceId ) || true == is_null( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						usl.unit_space_status_type_id,
						usl.log_datetime,
						GREATEST( us.make_ready_date, us.available_on, CURRENT_DATE ) AS available_on
					FROM
						unit_space_logs usl
						JOIN unit_spaces us ON( us.cid = usl.cid AND us.id = usl.unit_space_id )
					WHERE
						usl.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND	usl.property_id = ' . ( int ) $intPropertyId . '
						AND usl.cid = ' . ( int ) $intCid . '
						--AND unit_space_log_type_id = ' . CUnitSpaceLogType::DIRECT_UPDATE . '
						--AND usl.old_unit_space_status_type_id != usl.new_unit_space_status_type_id
					ORDER BY
						usl.log_datetime DESC,
						usl.id DESC';

		return self::fetchUnitSpaceLogs( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPropertyIdsByUnitSpaceIdsByReportingDateByCid( array $arrintPropertyIds, $intCid, $objDatabase, $arrintUnitSpaceIds = [], $strDate = NULL, $strDateFilterField = 'post_date', $boolReturnSql = false ) {

		$strSqlConditions = '';
		$strSqlConditions .= ( true == valArr( $arrintUnitSpaceIds ) ) ? ' AND usl.unit_space_id IN (' . implode( ',', $arrintUnitSpaceIds ) . ') ' : '';
		// Dynamically generate usl.reporting_post_date / usl.reporting_post_month and usl.apply_through_post_date / usl.apply_through_post_month. Comment added for tracing dynamic fields.
		$strSqlConditions .= ( false == is_null( $strDate ) ) ? ' AND ' . $strDate . ' BETWEEN usl.reporting_' . $strDateFilterField . ' AND usl.apply_through_' . $strDateFilterField . ' AND usl.is_' . $strDateFilterField . '_ignored = 0' : '';

		$strSql = 'SELECT
						usl.*,
						pf.floorplan_name,
						util_get_translated( \'name\', ut.name, ut.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS unit_type,
						util_get_system_translated( \'name\', usst.name, usst.details ) AS unit_space_status
					FROM
						unit_space_logs usl
						JOIN property_floorplans pf ON ( pf.cid = usl.cid AND pf.id = usl.property_floorplan_id )
						JOIN unit_types ut ON ( ut.cid = usl.cid AND ut.id = usl.unit_type_id )
						JOIN unit_space_status_types usst ON ( usst.id = usl.unit_space_status_type_id )
					WHERE
						usl.cid = ' . ( int ) $intCid . '
						AND usl.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND usl.deleted_on IS NULL
						' . $strSqlConditions . '
					ORDER BY
						usl.display_number';

		if( false == $boolReturnSql ) {
			return fetchData( $strSql, $objDatabase );
		}

		return $strSql;

	}

	public static function fetchUnitSpaceLogsWithResidentByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $intPropertyId, $intCid, $objDatabase ) {
		if( true == is_null( $intUnitSpaceId ) || true == is_null( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						usl.unit_space_status_type_id,
						usl.log_datetime,
						usl.current_lease_id,
						cl.name_first,
						cl.name_last
					FROM
						unit_space_logs usl
						LEFT JOIN cached_leases cl ON ( cl.cid = usl.cid AND cl.property_id = usl.property_id AND cl.id = usl.current_lease_id )
					WHERE
						usl.cid = ' . ( int ) $intCid . '
						AND usl.property_id = ' . ( int ) $intPropertyId . '
						AND usl.unit_space_id = ' . ( int ) $intUnitSpaceId . '
					ORDER BY
						usl.log_datetime DESC,
						usl.id DESC';

		return self::fetchUnitSpaceLogs( $strSql, $objDatabase );
	}

	public static function fetchCustomUnitSpaceLogsByUnitSpaceIdByPropertyIdByCid( $intUnitSpaceId, $intPropertyId, $intCid, $arrintActivityFilter, $objDatabase ) {

		if( false == valId( $intUnitSpaceId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strUnitNoteSql = 'SELECT
					      usl.id,
					      usl.unit_space_status_type_id,
					      usl.log_datetime,
					      usl.current_lease_id,
					      usl.details,
					      cl.name_first,
					      cl.name_last,
					      NULL AS old_unit_space_status_type_id,
					      cu.username,
					      usl.created_by
					    FROM
					      unit_space_logs usl
					      JOIN 
					      (
					        SELECT
					          usl.id,
					          usl.log_datetime,
					          usl.cid,
					          usl.property_id,
					          usl.unit_space_status_type_id,
					          usl.details,
					          LEAD( usl.details ->> \'unit_notes\' ) OVER(
					        ORDER BY
					          usl.log_datetime DESC, usl.id DESC ) AS next_details
					        FROM
					          unit_space_logs usl
					        WHERE
					          usl.is_opening_log = 0 AND
					          usl.cid = ' . ( int ) $intCid . ' AND
					          usl.property_id = ' . ( int ) $intPropertyId . ' AND
					          usl.unit_space_id = ' . ( int ) $intUnitSpaceId . ' AND
					          usl.details -> \'unit_notes\' ->> \'event_type_id\' IN ( \'' . CEventType::UNIT_NOTES . '\' )
					        ORDER BY
					          usl.log_datetime DESC,
					          usl.id DESC
					      ) AS sub_join ON ( usl.cid = sub_join.cid AND usl.property_id = sub_join.property_id AND usl.id = sub_join.id )
					      LEFT JOIN cached_leases cl ON ( cl.cid = usl.cid 
					                                      AND cl.property_id = usl.property_id 
					                                      AND cl.id = ( CASE
                                                                            WHEN usl.application_lease_id IS NOT NULL THEN usl.application_lease_id
                                                                            WHEN usl.future_lease_id IS NOT NULL THEN usl.future_lease_id
                                                                            WHEN usl.past_lease_id IS NOT NULL THEN usl.past_lease_id
                                                                            ELSE usl.current_lease_id
                                                                          END 
                                                                       )
					                                    )
					      JOIN company_users cu ON ( cu.id = usl.created_by AND cu.cid = usl.cid)
					    WHERE
					      sub_join.details ->> \'unit_notes\' <> COALESCE( sub_join.next_details, \'\' ) ';

		$strUnitSpaceStatusSql = 'SELECT
					      usl.id,
					      usl.unit_space_status_type_id,
					      usl.log_datetime,
					      usl.current_lease_id,
					      NULL AS details,
					      cl.name_first,
					      cl.name_last,
					      sub_join.next_status_type_id AS old_unit_space_status_type_id,
					      cu.username,
					      usl.created_by
					    FROM
					      unit_space_logs usl
					      JOIN 
					      (
					        SELECT
					          usl.id,
					          usl.log_datetime,
					          usl.cid,
					          usl.property_id,
					          usl.unit_space_status_type_id,
					          usl.details,
					          LEAD( usl.unit_space_status_type_id ) OVER(
					        ORDER BY
					          usl.log_datetime DESC, usl.id DESC ) AS next_status_type_id
					        FROM
					          unit_space_logs usl
					        WHERE
					          usl.is_opening_log = 0 AND
					          usl.cid = ' . ( int ) $intCid . ' AND
					          usl.property_id = ' . ( int ) $intPropertyId . ' AND
					          usl.unit_space_id = ' . ( int ) $intUnitSpaceId . '
					        ORDER BY
					          usl.log_datetime DESC,
					          usl.id DESC
					      ) AS sub_join ON ( usl.cid = sub_join.cid AND usl.property_id = sub_join.property_id AND usl.id = sub_join.id )
					      LEFT JOIN cached_leases cl ON ( cl.cid = usl.cid 
					                                      AND cl.property_id = usl.property_id 
					                                      AND cl.id = ( CASE
					                                                        WHEN usl.application_lease_id IS NOT NULL THEN usl.application_lease_id
                                                                            WHEN usl.future_lease_id IS NOT NULL THEN usl.future_lease_id
                                                                            WHEN usl.past_lease_id IS NOT NULL THEN usl.past_lease_id
                                                                            ELSE usl.current_lease_id
                                                                          END 
                                                                       )
                                                        )
					      JOIN company_users cu ON ( cu.id = usl.created_by AND cu.cid = usl.cid)
					    WHERE
					      sub_join.unit_space_status_type_id <> COALESCE( sub_join.next_status_type_id, 0 ) ';

		if( true == valArr( $arrintActivityFilter ) ) {
			if( false === ( $intKey = array_search( CEventType::UNIT_NOTES, $arrintActivityFilter ) ) ) {
				$strUnitNoteSql = '';
			} else {
				unset( $arrintActivityFilter[$intKey] );
			}
			if( true == valArr( $arrintActivityFilter ) ) {
				$strUnitSpaceStatusSql .= ' AND usl.unit_space_status_type_id IN ( ' . implode( ',', $arrintActivityFilter ) . ' )';
			} else {
				$strUnitSpaceStatusSql = '';
			}
		}

		$strSql = 'SELECT
					  *
					FROM
					  (
					    ';
		if( '' != $strUnitNoteSql && '' != $strUnitSpaceStatusSql ) {
			$strSql .= $strUnitNoteSql . ' UNION ' . $strUnitSpaceStatusSql;
		} else if( '' != $strUnitNoteSql && '' == $strUnitSpaceStatusSql ) {
			$strSql .= $strUnitNoteSql;
		} else if( '' == $strUnitNoteSql && '' != $strUnitSpaceStatusSql ) {
			$strSql .= $strUnitSpaceStatusSql;
		}

		$strSql .= ') AS unit_log
					ORDER BY
					  unit_log.log_datetime DESC,
					  unit_log.id DESC';

		return self::fetchUnitSpaceLogs( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceLogsByPropertyIdsByReportingDateByCid( array $arrintPropertyIds, $strDate = NULL, $strDateFilterField = 'post_date', $intCid, $objDatabase, $boolReturnSql = false, $objPropertyPricingRentBudgetRentFilter = NULL ) {
		$strSqlConditions = '';
		$strJoinConditions = '';
		// Dynamically generate usl.reporting_post_date / usl.reporting_post_month and usl.apply_through_post_date / usl.apply_through_post_month. Comment added for tracing dynamic fields.
		$strSqlConditions .= ( false == is_null( $strDate ) ) ? ' AND ' . $strDate . ' BETWEEN usl.reporting_' . $strDateFilterField . ' AND usl.apply_through_' . $strDateFilterField . ' AND usl.is_' . $strDateFilterField . '_ignored = 0' : '';
		$strSqlConditions .= ( 0 != $objPropertyPricingRentBudgetRentFilter->getFloorPlanIds() ) ? ' AND pf.id IN (' . implode( ',', $objPropertyPricingRentBudgetRentFilter->getFloorPlanIds() ) . ') '  : '';
		$strSqlConditions .= ( 0 != $objPropertyPricingRentBudgetRentFilter->getUnitTypeIds() ) ? ' AND ut.id IN (' . implode( ',', $objPropertyPricingRentBudgetRentFilter->getUnitTypeIds() ) . ') '  : '';
		$strSqlConditions .= ( true == valArr( $objPropertyPricingRentBudgetRentFilter->getUnitSpaceStatusTypeIds() ) ) ? ' AND usst.id IN (' . implode( ',', $objPropertyPricingRentBudgetRentFilter->getUnitSpaceStatusTypeIds() ) . ') ' : '';

		if( true == valArr( $objPropertyPricingRentBudgetRentFilter->getUnitSpaces() ) ) {
			$strSqlConditions .= " AND us.space_number IN ('" . implode( "','", $objPropertyPricingRentBudgetRentFilter->getUnitSpaces() ) . "') ";
			$strJoinConditions .= 'JOIN unit_spaces us ON ( usl.cid = us.cid AND us.id = usl.unit_space_id )';
		}

		$strSql = 'SELECT
						usl.*,
						pf.floorplan_name,
						util_get_translated( \'name\', ut.name, ut.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS unit_type,
						util_get_system_translated( \'name\', usst.name, usst.details ) AS unit_space_status
					FROM
						unit_space_logs usl '
						. $strJoinConditions . '
						JOIN property_floorplans pf ON ( pf.cid = usl.cid AND pf.id = usl.property_floorplan_id )
						JOIN unit_types ut ON ( ut.cid = usl.cid AND ut.id = usl.unit_type_id )
						JOIN unit_space_status_types usst ON ( usst.id = usl.unit_space_status_type_id )
					WHERE
						usl.cid = ' . ( int ) $intCid . '
						AND usl.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND usl.deleted_on IS NULL
						' . $strSqlConditions . '
					ORDER BY
						usl.display_number';

		if( false == $boolReturnSql ) {
			return fetchData( $strSql, $objDatabase );
		}

		return $strSql;

	}

	public static function fetchBudgetedRentHistoryByPropertyIdByUnitSpaceIdByPeriodStartDateByCid( $intPropertyId, $intUnitSpaceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						usl.effective_date,
						usl.budgeted_base_rent,
						usl.budgeted_amenity_rent,
						usl.budgeted_rent,
						cu.username,
						usl.log_datetime
					FROM
						unit_space_logs usl
						JOIN company_users cu ON ( cu.id = usl.created_by AND cu.cid = usl.cid )
					WHERE
						usl.cid = ' . ( int ) $intCid . '
						AND usl.property_id = ' . ( int ) $intPropertyId . '
						AND usl.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND usl.deleted_on IS NULL
						AND usl.is_post_date_ignored = 0
					ORDER BY
						usl.reporting_post_date DESC,
						usl.log_datetime DESC';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceLogsWithBudgetedRentByPropertyIdsByReportingDateByCid( $intPropertyId, $intCid, $objDatabase, $objPropertyPricingRentBudgetRentFilter = NULL, $strDate = NULL, $strDateFilterField = 'post_date', $boolIsFromCreateBulkBudgetedRent = false ) {
		$strSqlConditions = '';
		$strJoinConditions = '';
		// Dynamically generate usl.reporting_post_date / usl.reporting_post_month and usl.apply_through_post_date / usl.apply_through_post_month. Comment added for tracing dynamic fields.
		$strSqlConditions .= ( false == is_null( $strDate ) ) ? ' AND ' . $strDate . ' BETWEEN usl.reporting_' . $strDateFilterField . ' AND usl.apply_through_' . $strDateFilterField . ' AND usl.is_' . $strDateFilterField . '_ignored = 0' : '';
		$strSqlConditions .= ( 0 != $objPropertyPricingRentBudgetRentFilter->getFloorPlanIds() ) ? ' AND pf.id IN (' . implode( ',', $objPropertyPricingRentBudgetRentFilter->getFloorPlanIds() ) . ') '  : '';
		$strSqlConditions .= ( 0 != $objPropertyPricingRentBudgetRentFilter->getUnitTypeIds() ) ? ' AND ut.id IN (' . implode( ',', $objPropertyPricingRentBudgetRentFilter->getUnitTypeIds() ) . ') '  : '';
		$strSqlConditions .= ( true == valArr( $objPropertyPricingRentBudgetRentFilter->getUnitSpaceStatusTypeIds() ) ) ? ' AND usst.id IN (' . implode( ',', $objPropertyPricingRentBudgetRentFilter->getUnitSpaceStatusTypeIds() ) . ') ' : '';

		if( true == valArr( $objPropertyPricingRentBudgetRentFilter->getUnitSpaces() ) ) {
			$strSqlConditions .= " AND us.space_number IN ('" . implode( "','", $objPropertyPricingRentBudgetRentFilter->getUnitSpaces() ) . "') ";
			$strJoinConditions .= 'JOIN unit_spaces us ON ( usl.cid = us.cid AND us.id = usl.unit_space_id )';
		}

		$strSql = 'SELECT
						usl.*,
						pf.floorplan_name,
						util_get_translated( \'name\', ut.name, ut.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS unit_type,
						util_get_system_translated( \'name\', usst.name, usst.details ) AS unit_space_status_type
					FROM
						unit_space_logs usl '
		          . $strJoinConditions . '
						JOIN property_floorplans pf ON ( pf.cid = usl.cid AND pf.id = usl.property_floorplan_id )
						JOIN unit_types ut ON ( ut.cid = usl.cid AND ut.id = usl.unit_type_id )
						JOIN unit_space_status_types usst ON ( usst.id = usl.unit_space_status_type_id )
					WHERE
						usl.cid = ' . ( int ) $intCid . '
						AND usl.property_id = ' . ( int ) $intPropertyId . '
						AND usl.deleted_on IS NULL
						' . $strSqlConditions . '
					ORDER BY
						usl.display_number';

		$arrobjUnitSpaceLogs = self::fetchUnitSpaceLogs( $strSql, $objDatabase );

		if( false == $boolIsFromCreateBulkBudgetedRent ) {

			if( true == valArr( $arrobjUnitSpaceLogs ) ) {

				$arrintUnitSpaceIds = [];
				foreach( $arrobjUnitSpaceLogs as $arrobjUnitSpaceLog ) {
					if( $arrobjUnitSpaceLog->getUnitSpaceId() ) {
						$arrintUnitSpaceIds[] = $arrobjUnitSpaceLog->getUnitSpaceId();
					}
				}

				$strSql = '
					SELECT
					sub.*
					FROM
					(
						SELECT
							usl.id,
							usl.unit_space_id,
							usl.effective_date,
							usl.budgeted_base_rent,
							usl.budgeted_amenity_rent,
							usl.budgeted_rent,
							cu.username,
							usl.log_datetime,
							lag( usl.budgeted_base_rent ) OVER ( ORDER BY usl.unit_space_id, usl.effective_date ) as prev_budgeted_base_rent,
							lag( usl.budgeted_amenity_rent ) OVER ( ORDER BY usl.unit_space_id, usl.effective_date ) as prev_budgeted_amenity_rent,
							lag( usl.budgeted_rent ) OVER ( ORDER BY usl.unit_space_id, usl.effective_date ) as prev_budgeted_rent
						FROM
							unit_space_logs usl
							JOIN company_users cu ON ( cu.id = usl.created_by AND cu.cid = usl.cid )
						WHERE
							usl.cid = ' . ( int ) $intCid . '
							AND usl.property_id = ' . ( int ) $intPropertyId . '
							AND usl.unit_space_id IN (' . implode( ',', $arrintUnitSpaceIds ) . ' )
							AND usl.deleted_on IS NULL
							AND usl.is_post_date_ignored = 0
						ORDER BY
							usl.reporting_post_date DESC,
							usl.log_datetime DESC
					) AS sub
					WHERE
						sub.budgeted_base_rent IS DISTINCT FROM sub.prev_budgeted_base_rent 
						OR sub.budgeted_amenity_rent IS DISTINCT FROM sub.prev_budgeted_amenity_rent 
						OR sub.budgeted_rent IS DISTINCT FROM sub.prev_budgeted_rent
					ORDER BY
						sub.effective_date DESC';

				$arrobjBudgetedRents = self::fetchUnitSpaceLogs( $strSql, $objDatabase );

				if( false == valArr( $arrobjBudgetedRents ) ) {
					return $arrobjUnitSpaceLogs;
				} else {
					foreach( $arrobjUnitSpaceLogs as $objUnitSpaceLog ) {
						$arrobjTempBudgetedRents = [];
						foreach( $arrobjBudgetedRents as $objBudgetedRent ) {
							if( $objUnitSpaceLog->getUnitSpaceId() == $objBudgetedRent->getUnitSpaceId() ) {
								$arrobjTempBudgetedRents[$objBudgetedRent->getId()] = $objBudgetedRent;
							}
						}
						$objUnitSpaceLog->setBudgetedRents( $arrobjTempBudgetedRents );
					}
				}
			}
		}

		return $arrobjUnitSpaceLogs;

	}

	public static function fetchLeasePercent( $intCid, $intPropertyId, $intUnitTypeId, $boolPropertyLevel, $objDatabase ) {
		$strSqlCondition = '';
		if( false == $boolPropertyLevel ) {
			$strSqlCondition = ' AND usl.unit_type_id = ' . ( int ) $intUnitTypeId;
		}
		$strSql = 'SELECT
						usl.cid,
						usl.property_id,
						date_part( \'YEAR\', usl.post_month ) As year_num,
						date_part( \'MONTH\', usl.post_month ) As month_num,
						(
							( SUM ( 
								CASE
									WHEN usl.is_reportable = TRUE AND usl.unit_space_status_type_id IN ( ' . implode( ', ', [ CUnitSpaceStatusType::OCCUPIED_NO_NOTICE, CUnitSpaceStatusType::NOTICE_RENTED, CUnitSpaceStatusType::VACANT_RENTED_READY, CUnitSpaceStatusType::VACANT_RENTED_NOT_READY ] ) . ') THEN 1
									ELSE 0
								END ) 
								)::NUMERIC/
							( NULLIF( SUM ( CASE
							WHEN usl.is_reportable = TRUE THEN 1
							ELSE 0
							END ), 0 ) ) * 100 ) AS leased_percent
					FROM
						unit_space_logs AS usl
					WHERE
						usl.cid = ' . ( int ) $intCid . '
						AND usl.property_id = ' . ( int ) $intPropertyId . $strSqlCondition . '
						AND usl.is_post_month_ignored = 0
						AND ( usl.post_month BETWEEN CURRENT_DATE - INTERVAL \'13 months\' AND CURRENT_DATE )
					GROUP BY
						month_num,
						year_num,
						usl.cid,
						usl.property_id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentLeaseUnitAvailabityDateByUnitSpaceIdByLeaseIdByPropertyIdByCid( $intUnitSpaceId, $intLeaseId, $intUnitSpaceStatusTypeId, $intPropertyId, $intCid, $objDatabase ) {
		if( true == is_null( $intUnitSpaceId ) || true == is_null( $intPropertyId ) || true == is_null( $intLeaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
					available_on
				FROM 
					unit_space_logs
				WHERE 
					cid = ' . ( int ) $intCid . '
					AND unit_space_id = ' . ( int ) $intUnitSpaceId . '
					AND property_id = ' . ( int ) $intPropertyId . '
					AND unit_space_status_type_id = ' . ( int ) $intUnitSpaceStatusTypeId . '
					AND current_lease_id = ' . ( int ) $intLeaseId . '
				ORDER BY 
					log_datetime DESC
				LIMIT 1';
		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) && true == isset ( $arrmixData[0]['available_on'] ) ) {
			return $arrmixData[0]['available_on'];
		}

		return NULL;
	}

	public static function fetchUnitSpaceLogByPropertyIdByUnitSpaceIdByReportingDateByCid( $intPropertyId, $intUnitSpaceId, $strDate = NULL, $strDateFilterField = 'post_date', $intCid, $objDatabase ) {
		$strSqlConditions = '';
		// Dynamically generate usl.reporting_post_date / usl.reporting_post_month and usl.apply_through_post_date / usl.apply_through_post_month. Comment added for tracing dynamic fields.
		$strSqlConditions .= ( false == is_null( $strDate ) ) ? ' AND ' . $strDate . ' BETWEEN usl.reporting_' . $strDateFilterField . ' AND usl.apply_through_' . $strDateFilterField . ' AND usl.is_' . $strDateFilterField . '_ignored = 0' : '';

		$strSql = 'SELECT
						usl.*,
						pf.floorplan_name,
						util_get_translated( \'name\', ut.name, ut.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS unit_type,
						util_get_system_translated( \'name\', usst.name, usst.details ) AS unit_space_status
					FROM
						unit_space_logs usl
						JOIN unit_spaces us ON ( usl.cid = us.cid AND us.id = usl.unit_space_id )
						JOIN property_floorplans pf ON ( pf.cid = usl.cid AND pf.id = usl.property_floorplan_id )
						JOIN unit_types ut ON ( ut.cid = usl.cid AND ut.id = usl.unit_type_id )
						JOIN unit_space_status_types usst ON ( usst.id = usl.unit_space_status_type_id )
					WHERE
						usl.cid = ' . ( int ) $intCid . '
						AND usl.property_id = ' . ( int ) $intPropertyId . '
						AND usl.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND usl.deleted_on IS NULL
						' . $strSqlConditions . '
					ORDER BY
						usl.display_number';

		return self::fetchUnitSpaceLog( $strSql, $objDatabase );
	}

}
?>