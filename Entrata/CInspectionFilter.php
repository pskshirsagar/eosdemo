<?php

class CInspectionFilter extends CBaseInspectionFilter {

	protected $m_intDurationId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intPageNo;
	protected $m_intInspectedBy;
	protected $m_intIsResidentVisible;
	protected $m_intShowCurrentInspectionDue;
	protected $m_intInspectionStatusId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;

	protected $m_arrintPropertyUnitIds;
	protected $m_strUnitNumber;
	protected $m_strNameFull;
	protected $m_strInspectionFormIds;

	protected $_REQUEST_FORM_INSPECTION_SEARCH	= [
		'id'								=> NULL,
		'cid' 								=> NULL,
		'company_user_id' 					=> NULL,
		'property_group_ids'				=> NULL,
		'inspection_status_ids' 			=> NULL,
		'inspection_form_ids'				=> NULL,
		'company_employee_ids' 				=> NULL,
		'property_unit_id'					=> NULL,
		'title' 							=> NULL,
		'description' 						=> NULL,
		'keyword_search' 					=> NULL,
		'created_start_date' 				=> NULL,
		'created_end_date' 					=> NULL,
		'inspected_start_date' 				=> NULL,
		'inspected_end_date' 				=> NULL,
		'completed_start_date' 				=> NULL,
		'completed_end_date' 				=> NULL,
		'scheduled_start_date' 				=> NULL,
		'scheduled_end_date' 				=> NULL,
		'sort_by' 							=> NULL,
		'sort_direction' 					=> NULL,
		'due_start_date'					=> NULL,
		'due_end_date'						=> NULL,
		'filter_by_days'					=> NULL,
		'inspection_within'					=> NULL,
		'inspection_within_days'			=> NULL
	];

    /**
     *
     * Get Functions
     */

    public function getUnitNumber() {
    	return $this->m_strUnitNumber;
    }

    public function getNameFull() {
    	return $this->m_strNameFull;
    }

    public function getDurationId() {
    	return $this->m_intDurationId;
    }

    public function getIsResidentVisible() {
    	return $this->m_boolIsResidentVisible;
    }

    public function getInspectedBy() {
    	return $this->m_intInspectedBy;
    }

    public function getCustomerId() {
    	return  $this->m_intCustomerId;
    }

	public function getLeaseId() {
		return  $this->m_intLeaseId;
	}

    public function getPropertyUnitIds() {
    	return $this->m_arrintPropertyUnitIds;
    }

    public function getPageNo() {
    	return $this->m_intPageNo;
    }

    public function getPropertyGroupIds() {
    	if( true == valArr( $this->m_strPropertyGroupIds ) ) {
    		return $this->m_strPropertyGroupIds;
    	} elseif( false == empty( $this->m_strPropertyGroupIds ) ) {
    		return explode( ',', $this->m_strPropertyGroupIds );
    	} else {
    		return [];
    	}
    }

    public function getInspectionStatusIds() {
    	if( true == valArr( $this->m_strInspectionStatusIds ) ) {
    		return $this->m_strInspectionStatusIds;
    	} elseif( false == empty( $this->m_strInspectionStatusIds ) ) {
    		return explode( ',', $this->m_strInspectionStatusIds );
    	} else {
    		return [];
    	}
    }

    public function getCompanyEmployeeIds() {
    	if( true == valArr( $this->m_strCompanyEmployeeIds ) ) {
    		return $this->m_strCompanyEmployeeIds;
    	} elseif( false == empty( $this->m_strCompanyEmployeeIds ) ) {
    		return explode( ',', $this->m_strCompanyEmployeeIds );
    	} else {
    		return [];
    	}
    }

    public function getShowCurrentInspectionDue() {
    	return $this->m_intShowCurrentInspectionDue;
    }

    public function getInspectionFormIds() {
		if( true == valArr( $this->m_strInspectionFormIds ) ) {
			return $this->m_strInspectionFormIds;
		} elseif( false == empty( $this->m_strInspectionFormIds ) ) {
			return explode( ',', $this->m_strInspectionFormIds );
		} else {
			return [];
		}
    }

    public function getInspectionStatusId() {
    	return $this->m_intInspectionStatusId;
    }

    public function getPropertyUnitId() {
    	return $this->m_intPropertyUnitId;
    }

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

    /**
  	 *
  	 * Set functions
  	 */

    public function setUnitNumber( $strUnitNumber ) {
    	$this->m_strUnitNumber = $strUnitNumber;
    }

    public function setNameFull( $strNameFull ) {
    	$this->m_strNameFull = $strNameFull;
    }

    public function setDurationId( $intDurationId ) {
    	$this->m_intDurationId = $intDurationId;
    }

    public function setInspectedBy( $intInspectedBy ) {
    	$this->m_intInspectedBy = $intInspectedBy;
    }

    public function setCustomerId( $intCustomerId ) {
    	$this->m_intCustomerId = $intCustomerId;
    }

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setIsResidentVisible( $boolIsResidentVisible ) {
		$this->m_boolIsResidentVisible = $boolIsResidentVisible;
	}

    public function setPropertyUnitIds( $arrintPropertyUnitIds ) {
    	$this->m_arrintPropertyUnitIds = $arrintPropertyUnitIds;
    }

    public function setPageNo( $intPageNo ) {
    	$this->m_intPageNo = $intPageNo;
    }

	public function setPropertyGroupIds( $strPropertyGroupIds ) {
		if( true == valArr( $strPropertyGroupIds ) ) {
			$this->m_strPropertyGroupIds = implode( ',', $strPropertyGroupIds );
		} else {
			$this->m_strPropertyGroupIds = $strPropertyGroupIds;
		}
	}

    public function setInspectionStatusIds( $strInspectionStatusIds ) {
    	if( true == valArr( $strInspectionStatusIds ) ) {
    		$this->m_strInspectionStatusIds = implode( ',', $strInspectionStatusIds );
    	} else {
    		$this->m_strInspectionStatusIds = $strInspectionStatusIds;
    	}
    }

    public function setCompanyEmployeeIds( $strCompanyEmployeeIds ) {
    	if( true == valArr( $strCompanyEmployeeIds ) ) {
    		$this->m_strCompanyEmployeeIds = implode( ',', $strCompanyEmployeeIds );
    	} else {
     		$this->m_strCompanyEmployeeIds = $strCompanyEmployeeIds;
    	}
    }

	public function setShowCurrentInspectionDue( $intShowCurrentInspectionDue ) {
		$this->m_intShowCurrentInspectionDue = CStrings::strToIntDef( $intShowCurrentInspectionDue, NULL, false );
	}

	public function setInspectionFormIds( $strInspectionFormIds ) {
		if( true == valArr( $strInspectionFormIds ) ) {
			$this->m_strInspectionFormIds = implode( ',', $strInspectionFormIds );
		} else {
			$this->m_strInspectionFormIds = $strInspectionFormIds;
		}
	}

	public function setInspectionStatusId( $intInspectionStatusId ) {
		$this->m_intInspectionStatusId = $intInspectionStatusId;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = $intUnitSpaceId;
	}

    /**
     *
     * Sql Functions
     */

    public function sqlUnitNumber() {
    	return  ( true == isset( $this->m_strUnitNumber ) ) ? addslashes( trim( $this->m_strUnitNumber ) ) : 'NULL';
    }

    public function sqlNameFull() {
    	return  ( true == isset( $this->m_strNameFull ) ) ? addslashes( trim( $this->m_strNameFull ) ) : 'NULL';
    }

    public function sqlDurationId() {
    	return  ( true == isset( $this->m_intDurationId ) ) ? ( string ) $this->m_intDurationId : 'NULL';
    }

    public function sqlInspectedBy() {
    	return  ( true == isset( $this->m_intInspectedBy ) ) ? ( int ) $this->m_intInspectedBy : 'NULL';
    }

    /**
     *
     * Validate functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyIds() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionStatusIds() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyEmployeeIds() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTitle( $objDatabase ) {
        $boolIsValid = true;

    	if( true == is_null( $this->getTitle() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filter_name', __( 'Filter name is required.' ) ) );
    	}

    	$intConflictingInspectionFilterNameCount = \Psi\Eos\Entrata\CInspectionFilters::createService()->fetchConflictingInspectionFilterTitleCountByIdByCompanyUserIdByTitleByCid( $this->getId(), $this->getCompanyUserId(), $this->getTitle(), $this->m_intCid, $objDatabase );

    	if( 0 < $intConflictingInspectionFilterNameCount ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filter_name', __( 'Filter name {%s, 0} already exists.', [ $this->getTitle() ] ) ) );
    	}

    	return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valKeywordSearch() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedStartDate() {
        $boolIsValid = true;

        if( true == valStr( $this->getCreatedStartDate() ) && false === CValidation::checkDateFormat( $this->getCreatedStartDate(), $boolFormat = true ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on_from', __( 'Minimum created min date must be in mm/dd/yyyy format.' ) ) );
    	}

        $strCreatedStartDate = trim( $this->getCreatedStartDate() );
        $this->setCreatedStartDate( ( 'Min' == $this->getCreatedStartDate() || true == empty( $strCreatedStartDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getCreatedStartDate() ) ) );

        return $boolIsValid;
    }

    public function valCreatedEndDate() {
        $boolIsValid = true;

        if( true == valStr( $this->getCreatedEndDate() ) && false === CValidation::checkDateFormat( $this->getCreatedEndDate(), $boolFormat = true ) ) {
        	$boolIsValid = false;
        	$this->setCreatedStartDate( NULL );
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on_to', __( 'Maximum created max date must be in mm/dd/yyyy format.' ) ) );
        } elseif( true == $boolIsValid && true == valStr( $this->getCreatedEndDate() ) && true == valStr( $this->getCreatedStartDate() ) && ( strtotime( $this->getCreatedStartDate() ) > strtotime( $this->getCreatedEndDate() ) ) ) {
        	$boolIsValid = false;
        	$this->setCreatedStartDate( NULL );
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on_to', __( 'Maximum created date must be greater than minimum created date.' ) ) );
        }

        $strCreatedEndDate = trim( $this->getCreatedEndDate() );
        $this->setCreatedEndDate( ( 'Min' == $this->getCreatedEndDate() || true == empty( $strCreatedEndDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getCreatedEndDate() ) ) );

        return $boolIsValid;
    }

    public function valInspectedStartDate() {
        $boolIsValid = true;

        if( true == valStr( $this->getInspectedStartDate() ) && false === CValidation::checkDateFormat( $this->getInspectedStartDate(), $boolFormat = true ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inspection_date_min', __( 'Minimum inspection min date must be in mm/dd/yyyy format.' ) ) );
        }

        $strInspectedStartDate = trim( $this->getInspectedStartDate() );
        $this->setInspectedStartDate( ( 'Min' == $this->getInspectedStartDate() || true == empty( $strInspectedStartDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getInspectedStartDate() ) ) );

        return $boolIsValid;
    }

    public function valInspectedEndDate() {
        $boolIsValid = true;

        if( true == valStr( $this->getInspectedEndDate() ) && false === CValidation::checkDateFormat( $this->getInspectedEndDate(), $boolFormat = true ) ) {
        	$boolIsValid = false;
        	$this->setInspectedStartDate( NULL );
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inspection_date_max', __( 'Maximum inspection max date must be in mm/dd/yyyy format.' ) ) );

        } elseif( true == $boolIsValid && true == valStr( $this->getInspectedEndDate() ) && true == valStr( $this->getInspectedStartDate() ) && strtotime( $this->getInspectedStartDate() ) > strtotime( $this->getInspectedEndDate() ) ) {
        	$boolIsValid = false;
        	$this->setInspectedStartDate( NULL );
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inspection_date_min', __( 'Maximum inspection date must be greater than minimum inspection date.' ) ) );
        }

        $strInspectedEndDate = trim( $this->getInspectedEndDate() );
        $this->setInspectedEndDate( ( 'Min' == $this->getInspectedEndDate() || true == empty( $strInspectedEndDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getInspectedEndDate() ) ) );

        return $boolIsValid;
    }

    public function valCompletedStartDate() {
        $boolIsValid = true;

        if( true == valStr( $this->getCompletedStartDate() ) && false === CValidation::checkDateFormat( $this->getCompletedStartDate(), $boolFormat = true ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'completed_on_from', __( 'Minimum completed min date must be in mm/dd/yyyy format.' ) ) );
        }

        $strCompletedStartDate = trim( $this->getCompletedStartDate() );
        $this->setCompletedStartDate( ( 'Min' == $this->getCompletedStartDate() || true == empty( $strCompletedStartDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getCompletedStartDate() ) ) );

        return $boolIsValid;
	}

    public function valCompletedEndDate() {
        $boolIsValid = true;

        if( true == valStr( $this->getCompletedEndDate() ) && false === CValidation::checkDateFormat( $this->getCompletedEndDate(), $boolFormat = true ) ) {
        	$boolIsValid = false;
        	$this->setCompletedStartDate( NULL );
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'completed_on_to', __( 'Maximum completed max date must be in mm/dd/yyyy format.' ) ) );

        } elseif( true == $boolIsValid && true == valStr( $this->getCompletedEndDate() ) && true == valStr( $this->getCompletedStartDate() ) && strtotime( $this->getCompletedStartDate() ) > strtotime( $this->getCompletedEndDate() ) ) {
        	$boolIsValid = false;
        	$this->setCompletedStartDate( NULL );
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'completed_on_from', __( 'Maximum completed date must be greater than minimum completed date.' ) ) );
        }

        $strCompletedEndDate = trim( $this->getCompletedEndDate() );
        $this->setCompletedEndDate( ( 'Min' == $this->getCompletedEndDate() || true == empty( $strCompletedEndDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getCompletedEndDate() ) ) );

        return $boolIsValid;
    }

    public function valScheduledStartDate() {
        $boolIsValid = true;

        if( true == valStr( $this->getScheduledStartDate() ) && false === CValidation::checkDateFormat( $this->getScheduledStartDate(), $boolFormat = true ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_date_min', __( 'Minimum scheduled min date must be in mm/dd/yyyy format.' ) ) );
        }

        $strScheduledStartDate = trim( $this->getScheduledStartDate() );
        $this->setScheduledStartDate( ( 'Min' == $this->getScheduledStartDate() || true == empty( $strScheduledStartDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getScheduledStartDate() ) ) );

        return $boolIsValid;
    }

    public function valScheduledEndDate() {
        $boolIsValid = true;

        if( true == valStr( $this->getScheduledEndDate() ) && false === CValidation::checkDateFormat( $this->getScheduledEndDate(), $boolFormat = true ) ) {
        	$boolIsValid = false;
        	$this->setScheduledStartDate( NULL );
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_date_max', __( 'Maximum scheduled max date must be in mm/dd/yyyy format.' ) ) );

        } elseif( true == $boolIsValid && true == valStr( $this->getScheduledEndDate() ) && true == valStr( $this->getScheduledStartDate() ) && strtotime( $this->getScheduledStartDate() ) > strtotime( $this->getScheduledEndDate() ) ) {
        	$boolIsValid = false;
        	$this->setScheduledStartDate( NULL );
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_date_min', __( 'Maximum scheduled date must be greater than minimum scheduled date.' ) ) );
        }

        $strScheduledEndDate = trim( $this->getScheduledEndDate() );
        $this->setScheduledEndDate( ( 'Min' == $this->getScheduledEndDate() || true == empty( $strScheduledEndDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getScheduledEndDate() ) ) );

        return $boolIsValid;
    }

    public function valDueStartDate() {
    	$boolIsValid = true;

        if( true == valStr( $this->getDueStartDate() ) && false === CValidation::checkDateFormat( $this->getDueStartDate(), $boolFormat = true ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date_min', __( 'Minimum due date must be in mm/dd/yyyy format.' ) ) );
        }

        $strDueStartDate = trim( $this->getDueStartDate() );
        $this->setDueStartDate( ( 'Min' == $this->getDueStartDate() || true == empty( $strDueStartDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getDueStartDate() ) ) );

        return $boolIsValid;
    }

    public function valDueEndDate() {
    	$boolIsValid = true;

    	if( true == valStr( $this->getDueEndDate() ) && false === CValidation::checkDateFormat( $this->getDueEndDate(), $boolFormat = true ) ) {
    		$boolIsValid = false;
    		$this->setDueEndDate( NULL );
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date_max', __( 'Maximum due date must be in mm/dd/yyyy format.' ) ) );

    	} elseif( true == $boolIsValid && true == valStr( $this->getDueEndDate() ) && true == valStr( $this->getDueStartDate() ) && strtotime( $this->getDueStartDate() ) > strtotime( $this->getDueEndDate() ) ) {
    		$boolIsValid = false;
    		$this->getDueStartDate( NULL );
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date_max', __( 'Maximum due date must be greater than minimum due date.' ) ) );
    	}

    	$strDueEndDate = trim( $this->getDueEndDate() );
    	$this->setDueEndDate( ( 'Max' == $this->getDueEndDate() || true == empty( $strDueEndDate ) || false == $boolIsValid )? NULL : date( 'Y-m-d', strtotime( $this->getDueEndDate() ) ) );

    	return $boolIsValid;
    }

	public function validate( $strAction = '', $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTitle( $objDatabase );
				$boolIsValid &= $this->valCreatedStartDate();
				$boolIsValid &= $this->valCreatedEndDate();
				$boolIsValid &= $this->valInspectedStartDate();
				$boolIsValid &= $this->valInspectedEndDate();
				$boolIsValid &= $this->valCompletedStartDate();
				$boolIsValid &= $this->valCompletedEndDate();
				$boolIsValid &= $this->valScheduledStartDate();
				$boolIsValid &= $this->valScheduledEndDate();
				$boolIsValid &= $this->valDueStartDate();
				$boolIsValid &= $this->valDueEndDate();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle( $objDatabase );
				$boolIsValid &= $this->valCreatedStartDate();
				$boolIsValid &= $this->valCreatedEndDate();
				$boolIsValid &= $this->valInspectedStartDate();
				$boolIsValid &= $this->valInspectedEndDate();
				$boolIsValid &= $this->valCompletedStartDate();
				$boolIsValid &= $this->valCompletedEndDate();
				$boolIsValid &= $this->valScheduledStartDate();
				$boolIsValid &= $this->valScheduledEndDate();
				$boolIsValid &= $this->valDueStartDate();
				$boolIsValid &= $this->valDueEndDate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid &= $this->valCreatedStartDate();
				$boolIsValid &= $this->valCreatedEndDate();
				$boolIsValid &= $this->valInspectedStartDate();
				$boolIsValid &= $this->valInspectedEndDate();
				$boolIsValid &= $this->valCompletedStartDate();
				$boolIsValid &= $this->valCompletedEndDate();
				$boolIsValid &= $this->valScheduledStartDate();
				$boolIsValid &= $this->valScheduledEndDate();
				$boolIsValid &= $this->valDueStartDate();
				$boolIsValid &= $this->valDueEndDate();
				break;
        }

        return $boolIsValid;
    }

    /**
     *
     * Other functions
     */

    public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {
    	if( true == valArr( $arrmixRequestForm ) ) {
    		$arrmixAppliedRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
    		$this->prepareRequestData( $arrmixAppliedRequestForm );
    	}

    	return;
    }

    public function setRequestData( $arrmixRequestForm ) {

    	$arrmixRequestForm['property_group_ids'] 			= ( true == isset( $arrmixRequestForm['property_group_ids'] ) && true == valArr( $arrmixRequestForm['property_group_ids'] ) ) ? implode( ',', $arrmixRequestForm['property_group_ids'] ) : '';
    	$arrmixRequestForm['inspection_status_ids'] 		= ( true == isset( $arrmixRequestForm['inspection_status_type_id'] ) && true == valArr( $arrmixRequestForm['inspection_status_type_id'] ) ) ? implode( ',', $arrmixRequestForm['inspection_status_type_id'] ) : '';
    	$arrmixRequestForm['company_employee_ids'] 			= ( true == isset( $arrmixRequestForm['company_employee_ids'] ) && true == valArr( $arrmixRequestForm['company_employee_ids'] ) ) 			? implode( ',', $arrmixRequestForm['company_employee_ids'] ) : '';
    	$arrmixRequestForm['title'] 						= ( true == isset( $arrmixRequestForm['filter_name'] ) ) 																					? $arrmixRequestForm['filter_name'] : '';
    	$arrmixRequestForm['description'] 					= ( true == isset( $arrmixRequestForm['description'] ) )																					? $arrmixRequestForm['description'] : '';
    	$arrmixRequestForm['keyword_search'] 				= ( true == isset( $arrmixRequestForm['inspection_customer_search'] ) ) 																	? $arrmixRequestForm['inspection_customer_search'] : '';
    	$arrmixRequestForm['sort_by'] 						= ( true == isset( $arrmixRequestForm['sort_by'] ) ) 																						? $arrmixRequestForm['sort_by'] : '';
    	$arrmixRequestForm['sort_direction'] 				= ( true == isset( $arrmixRequestForm['sort_direction'] ) ) 																				? $arrmixRequestForm['sort_direction'] : '';
    	$arrmixRequestForm['created_start_date'] 			= ( true == isset( $arrmixRequestForm['created_on_from'] ) && 'Min' != $arrmixRequestForm['created_on_from'] ) 								? $arrmixRequestForm['created_on_from'] : '';
    	$arrmixRequestForm['created_end_date'] 				= ( true == isset( $arrmixRequestForm['created_on_to'] ) && 'Max' != $arrmixRequestForm['created_on_to'] ) 									? $arrmixRequestForm['created_on_to'] : '';
    	$arrmixRequestForm['inspected_start_date'] 			= ( true == isset( $arrmixRequestForm['inspection_date_min'] ) && 'Min' != $arrmixRequestForm['inspection_date_min'] ) 						? $arrmixRequestForm['inspection_date_min'] : '';
    	$arrmixRequestForm['inspected_end_date'] 			= ( true == isset( $arrmixRequestForm['inspection_date_max'] ) && 'Max' != $arrmixRequestForm['inspection_date_max'] ) 						? $arrmixRequestForm['inspection_date_max'] : '';
    	$arrmixRequestForm['completed_start_date'] 			= ( true == isset( $arrmixRequestForm['completed_on_from'] ) && 'Min' != $arrmixRequestForm['completed_on_from'] )							? $arrmixRequestForm['completed_on_from'] : '';
    	$arrmixRequestForm['completed_end_date'] 			= ( true == isset( $arrmixRequestForm['completed_on_to'] ) && 'Max' != $arrmixRequestForm['completed_on_to'] ) 								? $arrmixRequestForm['completed_on_to'] : '';
    	$arrmixRequestForm['scheduled_start_date'] 			= ( true == isset( $arrmixRequestForm['scheduled_date_min'] ) && 'Min' != $arrmixRequestForm['scheduled_date_min'] ) 						? $arrmixRequestForm['scheduled_date_min'] : '';
    	$arrmixRequestForm['scheduled_end_date'] 			= ( true == isset( $arrmixRequestForm['scheduled_date_max'] ) && 'Max' != $arrmixRequestForm['scheduled_date_max'] )						? $arrmixRequestForm['scheduled_date_max'] : '';
    	$arrmixRequestForm['due_start_date'] 				= ( true == isset( $arrmixRequestForm['due_date_min'] ) && 'Min' != $arrmixRequestForm['due_date_min'] ) 						? $arrmixRequestForm['due_date_min'] : '';
    	$arrmixRequestForm['due_end_date'] 					= ( true == isset( $arrmixRequestForm['due_date_max'] ) && 'Max' != $arrmixRequestForm['due_date_max'] )						? $arrmixRequestForm['due_date_max'] : '';
    	$arrmixRequestForm['property_unit_id'] 				= ( true == isset( $arrmixRequestForm['property_unit_id'] ) ) ? $arrmixRequestForm['property_unit_id'] : '';

    	return $arrmixRequestForm;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['unit_number'] ) && 'unit' != \Psi\CStringService::singleton()->strtolower( $arrmixValues['unit_number'] ) ) $this->setUnitNumber( $arrmixValues['unit_number'] );
    	if( true == isset( $arrmixValues['name_full'] ) && 'name' != \Psi\CStringService::singleton()->strtolower( $arrmixValues['name_full'] ) ) $this->setNameFull( $arrmixValues['name_full'] );
    	if( true == isset( $arrmixValues['duration_id'] ) ) $this->setDurationId( $arrmixValues['duration_id'] );
    	if( true == isset( $arrmixValues['hide_properties'] ) ) $this->setHideProperties( $arrmixValues['hide_properties'] );
    	if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
    	if( true == isset( $arrmixValues['is_resident_visible'] ) ) $this->setIsResidentVisible( $arrmixValues['is_resident_visible'] );
    	if( true == isset( $arrmixValues['inspection_form_ids'] ) ) $this->setInspectionFormIds( $arrmixValues['inspection_form_ids'] );
    	if( true == isset( $arrmixValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrmixValues['property_unit_id'] );
    	return;
    }

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( true == is_null( $this->getId() ) ) {
    		return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	} else {
    		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}
    }

    public function prepareRequestData( $arrmixRequestForm ) {

		$arrmixPreparedRequestForm = $this->setRequestData( $arrmixRequestForm );
		$arrmixPreparedRequestForm = mergeIntersectArray( $this->_REQUEST_FORM_INSPECTION_SEARCH, $arrmixPreparedRequestForm );

		$this->setValues( $arrmixPreparedRequestForm, false );
	}

    public function setDefaults() {

    	$this->setPageNo( 1 );
    	$this->setFilterByDays( 1 );
    	return;
    }

    public function setDefaultSortOrder() {
        $this->setSortBy( 'inspection_status' );
        $this->setSortDirection( 'ASC' );
    	$this->setDefaults();
    	return;
    }

    public function changeDateFormat() {
    	if( false == is_null( $this->getCreatedStartDate() ) ) $this->setCreatedStartDate( date( 'm/d/Y', strtotime( $this->getCreatedStartDate() ) ) );
    	if( false == is_null( $this->getCreatedEndDate() ) ) $this->setCreatedEndDate( date( 'm/d/Y', strtotime( $this->getCreatedEndDate() ) ) );
    	if( false == is_null( $this->getInspectedStartDate() ) ) $this->setInspectedStartDate( date( 'm/d/Y', strtotime( $this->getInspectedStartDate() ) ) );
    	if( false == is_null( $this->getInspectedEndDate() ) ) $this->setInspectedEndDate( date( 'm/d/Y', strtotime( $this->getInspectedEndDate() ) ) );
    	if( false == is_null( $this->getCompletedStartDate() ) ) $this->setCompletedStartDate( date( 'm/d/Y', strtotime( $this->getCompletedStartDate() ) ) );
    	if( false == is_null( $this->getCompletedEndDate() ) ) $this->setCompletedEndDate( date( 'm/d/Y', strtotime( $this->getCompletedEndDate() ) ) );
    	if( false == is_null( $this->getScheduledStartDate() ) ) $this->setScheduledStartDate( date( 'm/d/Y', strtotime( $this->getScheduledStartDate() ) ) );
    	if( false == is_null( $this->getScheduledEndDate() ) ) $this->setScheduledEndDate( date( 'm/d/Y', strtotime( $this->getScheduledEndDate() ) ) );
    	if( false == is_null( $this->getDueStartDate() ) ) $this->setDueStartDate( date( 'm/d/Y', strtotime( $this->getDueStartDate() ) ) );
    	if( false == is_null( $this->getDueEndDate() ) ) $this->setDueEndDate( date( 'm/d/Y', strtotime( $this->getDueEndDate() ) ) );
    }

	public function toArray() {
		$arrmixValues				= parent::toArray();
		$arrmixValues['page_no'] 	= $this->getPageNo();

		return $arrmixValues;
	}

}
?>