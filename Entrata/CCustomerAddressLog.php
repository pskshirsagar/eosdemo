<?php

class CCustomerAddressLog extends CBaseCustomerAddressLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerAddressId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorCustomerAddressLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyResidenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTimeZoneId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutReasonListItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommunityName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReasonForLeaving() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthlyPaymentAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentRecipient() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCounty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsVerified() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddressOwnerType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsNonUsAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>