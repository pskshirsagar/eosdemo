<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderSchedules
 * Do not add any new functions to this class.
 */

class CGlHeaderSchedules extends CBaseGlHeaderSchedules {

	public static function fetchCustomApprovedGlHeaderSchedulesByNextPostDate( $strNextPostDate, $objClientDatabase ) {

		if( false == valStr( $strNextPostDate ) ) return NULL;

		$strSql = 'SELECT
						ghs.*,
						c.company_name
					FROM
						gl_header_schedules ghs
						JOIN clients c ON ( c.id = ghs.cid )
					WHERE
						ghs.next_post_date <= \'' . $strNextPostDate . '\'
						AND ghs.approved_by IS NOT NULL
						AND ghs.approved_on IS NOT NULL
						AND ghs.disabled_by IS NULL
						AND ghs.disabled_on IS NULL
					ORDER BY
						ghs.id ASC';

		return self::fetchGlHeaderSchedules( $strSql, $objClientDatabase );
	}
}
?>