<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CForm1099BoxTypes
 * Do not add any new functions to this class.
 */

class CForm1099BoxTypes extends CBaseForm1099BoxTypes {

	public static function fetchForm1099BoxTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CForm1099BoxType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchForm1099BoxType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CForm1099BoxType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    // TODO - check for this condition

    public static function fetchAllForm1099BoxTypes( $objClientDatabase ) {
    	return self::fetchForm1099BoxTypes( 'SELECT * FROM form_1099_box_types WHERE form_1099_type_id <> ' . CForm1099Type::FORM_1099_TYPE_7 . '.', $objClientDatabase );
    }

    public static function fetchNameDescriptionForm1099BoxTypes( $objClientDatabase ) {
    	$strSql = 'SELECT
    					name, description
					FROM
						form_1099_box_types
					WHERE
						form_1099_type_id = 1';
    	return fetchData( $strSql, $objClientDatabase );
    }

}
?>