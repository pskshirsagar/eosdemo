<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUtilityFormulas
 * Do not add any new functions to this class.
 */

class CUtilityFormulas extends CBaseUtilityFormulas {

    public static function fetchUtilityFormulas( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CUtilityFormula', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchUtilityFormula( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CUtilityFormula', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllUtilityFormulas( $objDatabase ) {

    	$strSql = 'SELECT * FROM utility_formulas';

    	return self::fetchUtilityFormulas( $strSql, $objDatabase );
    }

}
?>