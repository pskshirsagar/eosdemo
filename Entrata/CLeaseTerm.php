<?php

class CLeaseTerm extends CBaseLeaseTerm {

	const DEFAULT_LEASE_TERM           = 100;
	const NEW_LEASES                   = 2;
	const RENEWALS                     = 3;
	const LEASE_TERM_STANDARD          = 12;
	const MOVE_IN_BOARD_SOURCE_MODULE  = 'mib';
	const MOVE_OUT_BOARD_SOURCE_MODULE = 'mob';

	protected $m_intLeaseStartWindowId;
	protected $m_intPropertyId;
	protected $m_fltUnitSpaceRentAmount;

	protected $m_strPropertyName;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strBillingEndDate;
	protected $m_strRenewalStartDate;
	protected $m_strRenewalLeaseTermName;
	protected $m_strCustomLeaseTermId;
	protected $m_strOccupancyName;
	protected $m_strLeaseTermStructureName;
	protected $m_boolIsActive;
	protected $m_arrintLeaseTermIds;

    /**
     * Get Functions
     *
     */

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    public function getLeaseStartWindowId() {
    	return $this->m_intLeaseStartWindowId;
    }

    public function getLeaseTerm() {
    	return $this->m_intTermMonth;
    }

    public function getUnitSpaceRentAmount() {
    	return $this->m_fltUnitSpaceRentAmount;
    }

    public function getStartDate() {
    	return $this->m_strStartDate;
    }

    public function getEndDate() {
    	return $this->m_strEndDate;
    }

    public function getBillingEndDate() {
    	return $this->m_strBillingEndDate;
    }

	public function getRenewalStartDate() {
		return $this->m_strRenewalStartDate;
	}

	public function getRenewalLeaseTermName() {
		return CLeaseTermService::create()->getFormattedName( $this->getRenewalStartDate(), $this->getEndDate(), $this->getName() );
	}

	public function getCustomLeaseTermId() {
		return $this->m_strCustomLeaseTermId;
	}

    public function getIsActive() {
        return $this->m_boolIsActive;
    }

    public function getLeaseTermName() {
		return CLeaseTermService::create()->getFormattedName( $this->getStartDate(), $this->getEndDate(), $this->getName() );
 	}

	public function getLeaseTermIds() {
		return $this->m_arrintLeaseTermIds;
	}

	public function getOccupancyName() {
    	return $this->m_strOccupancyName;
	}

	public function getLeaseTermStructureName() {
		return $this->m_strLeaseTermStructureName;
	}

    /**
     * Set Functions
     *
     */

    public function setPropertyId( $intPropertyId ) {
    	return $this->m_intPropertyId = $intPropertyId;
    }

    public function setPropertyName( $strPropertyName ) {
    	return $this->m_strPropertyName = $strPropertyName;
    }

    public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
    	$this->m_intLeaseStartWindowId = $intLeaseStartWindowId;
    }

    public function setUnitSpaceRentAmount( $fltUnitSpaceRentAmount ) {
    	$this->m_fltUnitSpaceRentAmount = $fltUnitSpaceRentAmount;
    }

    public function setStartDate( $strStartDate ) {
    	$this->m_strStartDate = $strStartDate;
    }

    public function setEndDate( $strEndDate ) {
    	$this->m_strEndDate = $strEndDate;
    }

    public function setBillingEndDate( $strBillingEndDate ) {
    	$this->m_strBillingEndDate = $strBillingEndDate;
    }

	public function setRenewalStartDate( $strRenewalStartDate ) {
		$this->m_strRenewalStartDate = $strRenewalStartDate;
	}

	public function setRenewalLeaseTermName( $strRenewalLeaseTermName ) {
		$this->m_strRenewalLeaseTermName = $strRenewalLeaseTermName;
	}

	public function setCustomLeaseTermId( $strCustomLeaseTermId ) {
		$this->m_strCustomLeaseTermId = $strCustomLeaseTermId;
	}

    public function setIsActive( $boolIsActive ) {
    	$this->m_boolIsActive = CStrings::strToBool( $boolIsActive );
    }

	public function setLeaseTermIds( $arrintLeaseTermIds ) {
		$this->m_arrintLeaseTermIds = $arrintLeaseTermIds;
	}

	public function setOccupancyName( $strOccupancyName ) {
		$this->m_strOccupancyName = $strOccupancyName;
	}

	public function setLeaseTermStructureName( $strLeaseTermStructureName ) {
		$this->m_strLeaseTermStructureName = $strLeaseTermStructureName;
	}

    public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

    	if( true == isset( $arrmixValues['property_id'] ) ) 			$this->setPropertyId( $arrmixValues['property_id'] );

    	if( true == isset( $arrmixValues['lease_start_window_id'] ) ) 	$this->setLeaseStartWindowId( $arrmixValues['lease_start_window_id'] );
    	if( true == isset( $arrmixValues['property_name'] ) ) 			$this->setPropertyName( $arrmixValues['property_name'] );
	    if( true == isset( $arrmixValues['occupancy_name'] ) ) 			$this->setOccupancyName( $arrmixValues['occupancy_name'] );

    	if( true == isset( $arrmixValues['start_date'] ) ) 				$this->setStartDate( $arrmixValues['start_date'] );
    	if( true == isset( $arrmixValues['end_date'] ) ) 				$this->setEndDate( $arrmixValues['end_date'] );
    	if( true == isset( $arrmixValues['billing_end_date'] ) ) 		$this->setBillingEndDate( $arrmixValues['billing_end_date'] );
	    if( true == isset( $arrmixValues['renewal_start_date'] ) ) 		$this->setRenewalStartDate( $arrmixValues['renewal_start_date'] );
	    if( true == isset( $arrmixValues['renewal_lease_term_name'] ) ) $this->setRenewalLeaseTermName( $arrmixValues['renewal_lease_term_name'] );
    	if( true == isset( $arrmixValues['is_active'] ) ) 				$this->setIsActive( $arrmixValues['is_active'] );
		if( true == isset( $arrmixValues['lease_term_ids'] ) )          $this->setLeaseTermIds( $arrmixValues['lease_term_ids'] );
	    if( true == isset( $arrmixValues['custom_lease_term_id'] ) )    $this->setCustomLeaseTermId( $arrmixValues['custom_lease_term_id'] );
	    if( true == isset( $arrmixValues['lease_term_structure_name'] ) ) $this->setLeaseTermStructureName( $arrmixValues['lease_term_structure_name'] );
    }

    /**
     * Validation Functions
     *
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseTermStructureId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultLeaseTermId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMappingPltId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowProspectEdit() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowRenewalEdit() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsProspect() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsRenewal() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsUnset() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valShowOnWebsite() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsActive() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valDependentInformation( $objDatabase ) {

    	$strFromClause = ' specials s
    						JOIN rates rt ON ( rt.cid = s.cid AND rt.ar_origin_reference_id = s.id AND rt.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND s.deleted_by IS NULL AND s.deleted_on IS NULL )';

    	$strWhereClause = ' WHERE rt.cid = ' . ( int ) $this->getCid() . ' AND rt.lease_term_id  = ' . ( int ) $this->getId() . ' AND rt.is_allowed = TRUE AND rt.deactivation_date >= NOW() AND rt.rate_amount != 0 AND ( rt.show_on_website = TRUE OR rt.show_in_entrata = TRUE )';

    	if( 0 < ( int ) \Psi\Eos\Entrata\CRates::createService()->fetchRowCount( $strWhereClause, $strFromClause, $objDatabase ) ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'specials', __( 'You are attempting to delete a lease term that is associated to Specials, you must first delete any specials associated to this lease term.' ) ) );
    		return false;
    	}

    	$strFromClause = '  cached_applications ca
    						JOIN quotes q ON ( q.cid = ca.cid AND q.application_id = ca.id AND ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' AND ca.application_status_id IN ( ' . implode( ',', CApplicationStatus::$c_arrintActiveApplicationStatusIds ) . ' ) )
    						JOIN quote_lease_terms qlt ON ( qlt.cid = q.cid AND qlt.quote_id = q.id )';

    	$strWhereClause = ' WHERE qlt.cid = ' . ( int ) $this->getCid() . ' AND qlt.lease_term_id  = ' . ( int ) $this->getId() . ' AND qlt.deleted_by IS NULL AND qlt.deleted_on IS NULL';

    	if( 0 < ( int ) CApplications::fetchRowCount( $strWhereClause, $strFromClause, $objDatabase ) ) {

    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renewal_offer', __( 'You are attempting to delete a lease term that is included in renewal offers. The existing renewal offers will continue to use this lease term unless you edit those offers manually. Do you want to delete the lease term(s)?' ) ) );
    		return false;
    	}

    	return true;
    }

    public function valName( $objDatabase = NULL, $boolAllowNamingOfConventionalLeaseTerm = false ) {

    	$boolIsValid = true;

    	if( true == $boolAllowNamingOfConventionalLeaseTerm && COccupancyType::CONVENTIONAL == $this->m_intOccupancyTypeId ) {
		    // Require unique name if its duplicate lease term.

		    if( $this->fetchDuplicateLeaseTermCount( $objDatabase ) > 0 ) {
			    if( false == valStr( $this->getName() ) ) {
				    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Lease term name is required for duplicate lease term.' ) ) );
				    return false;
			    } else {
				    $strSql = ' WHERE cid = ' . $this->getCid() . ' AND lease_term_structure_id = ' . $this->getLeaseTermStructureId() . ' AND term_month = ' . $this->getTermMonth() . ' AND deleted_by IS NULL AND lease_term_type_id = ' . CLeaseTermType::CONVENTIONAL . ' AND occupancy_type_id = ' . COccupancyType::CONVENTIONAL . ' AND LOWER( name ) = LOWER( \'' . $this->getName() . '\' )';

				    if( true == valId( $this->getId() ) ) {
					    $strSql .= ' AND id <> ' . ( int ) $this->getId();
				    }

				    $intExistingDuplicateLeaseWithSameNameCount = ( int ) \Psi\Eos\Entrata\CLeaseTerms::createService()->fetchLeaseTermCount( $strSql, $objDatabase );
				    if( $intExistingDuplicateLeaseWithSameNameCount > 0 ) {
					    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Lease term name should be unique for same length lease term.' ) ) );
					    return false;
				    }
			    }
		    }

	    } elseif( CLeaseTermType::DATE_BASED == $this->getLeaseTermTypeId() ) {
		    if( false == valStr( $this->getName() ) ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Date based lease term must have name.' ) ) );
		    }
	    }

    	return $boolIsValid;
    }

    public function valConflictingLeaseTerms( $objDatabase ) {
    	$boolIsValid = true;

    	$arrobjExistingLeaseTerms = ( array ) CLeaseTerms::fetchCustomLeaseTermsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase, false, false, true, [], false );

    	if( true == valArr( $arrobjExistingLeaseTerms ) ) {

    		foreach( $arrobjExistingLeaseTerms as $objExistingLeaseTerm ) {

			    if( CLeaseTermType::CONVENTIONAL == $objExistingLeaseTerm->getLeaseTermTypeId() && CLeaseTermType::CONVENTIONAL == $this->getLeaseTermTypeId() ) {
    				if( $this->getOccupancyTypeId() == $objExistingLeaseTerm->getOccupancyTypeId() && $this->getTermMonth() == $objExistingLeaseTerm->getTermMonth() && $this->getId() != $objExistingLeaseTerm->getId() ) {

    					$boolIsValid = false;
    					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'term month', __( 'Lease term(s) already exists.' ) ) );
    					break;
    				}
    			}
    		}
    	}

    	return $boolIsValid;
    }

    public function valTermMonth() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getTermMonth() ) || 0 == strlen( trim( $this->getTermMonth() ) ) || false == is_numeric( $this->getTermMonth() ) || 1 > $this->getTermMonth() ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'term_month', __( 'Please enter the valid lease terms.' ) ) );
    		$boolIsValid = false;

    	} elseif( 1200 < $this->getTermMonth() ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'term_month', __( 'Maximum lease term allowed is {%d, 0} months.', [ 1200 ] ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valExistingLeaseTerm( $objDatabase, $boolAllowNamingOfConventionalLeaseTerm = false ) {

		if( COccupancyType::CONVENTIONAL == $this->getOccupancyTypeId() && true == $boolAllowNamingOfConventionalLeaseTerm )  return true;

    	if( 0 < ( int ) CLeaseTerms::fetchExistingLeaseTermsCountByTermMonthByLeaseTermStructureIdByOccupancyTypeIdByIdByCid( $this->getTermMonth(), $this->getLeaseTermStructureId(), $this->getId(), $this->getCid(), $objDatabase, $this->getOccupancyTypeId() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'term_month', __( 'Lease term(s) already exists.' ) ) );
    		return false;
    	}

    	return true;
    }

	public function valExistingCommercialLeaseTerm( $objDatabase ) {

		if( 0 < ( int ) CLeaseTerms::fetchExistingCommercialLeaseTermsCountByTermMonthByLeaseTermStructureIdByIdByOccupancyTypeIdByCid( $this->getTermMonth(), $this->getLeaseTermStructureId(), $this->getId(), $this->getOccupancyTypeId(), $this->getCid(), $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'term_month', __( 'Lease term(s) already exists.' ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsSemesterSelectionEnabled = false, $boolIsNewPropertySetting = false, $boolIsCommercialLeaseTerm = false, $boolAllowNamingOfConventionalLeaseTerm = false ) {
		$boolIsValid = true;

		// Just to avoid code sniffer issue.
		$boolIsSemesterSelectionEnabled = false;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				if( true == $boolIsCommercialLeaseTerm ) {
					$boolIsValid &= $this->valExistingCommercialLeaseTerm( $objDatabase );
					$boolIsValid &= $this->valTermMonth();
				}

				if( ( CLeaseTermType::DATE_BASED == $this->getLeaseTermTypeId() ) ) {
					$boolIsValid &= $this->valName();
				} elseif( CLeaseTermType::CONVENTIONAL == $this->getLeaseTermTypeId() ) {
					$boolIsValid &= $this->valExistingLeaseTerm( $objDatabase, $boolAllowNamingOfConventionalLeaseTerm );
					$boolIsValid &= $this->valTermMonth();
					$boolIsValid &= $this->valName( $objDatabase, $boolAllowNamingOfConventionalLeaseTerm );
				}

				if( true == $boolIsNewPropertySetting ) {
					$boolIsValid &= $this->valConflictingLeaseTerms( $objDatabase );
				}

				// $boolIsValid &= $this->valRenewalTemplateOffers( $objDatabase ); Commented due to some issue in it.
				break;

			case VALIDATE_DELETE:
				// $boolIsValid &= $this->valDependentInformation( $objDatabase, $boolIsSemesterSelectionEnabled );
				$boolIsValid &= $this->valDependentInformation( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolUpdateLeaseTermDependency = true ) {

		$intOldTermMonth = ( int ) CLeaseTerms::fetchLeaseTermMonthByIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$mixSqlResult = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolUpdateLeaseTermDependency && $intOldTermMonth != $this->getTermMonth() ) {

			$mixSubSqlResult = $this->updateLeaseTermDependencies( $intCurrentUserId, $objDatabase, $intOldTermMonth, $boolReturnSqlOnly );

			return ( false == $boolReturnSqlOnly ) ? ( $mixSqlResult &= $mixSubSqlResult ) : ( $mixSqlResult .= $mixSubSqlResult );

		}

		return $mixSqlResult;
	}

	/**
	 * Other Functions
	 *
	 */

	public function calculateTermMonth( $boolIsProrateCharges = true, $strStartDate = NULL ) {

		if( is_null( $this->m_strStartDate ) || is_null( $this->m_strEndDate ) ) return;

		$strStartDate = ( true == valStr( $strStartDate ) ) ? $strStartDate : $this->m_strStartDate;
		$strEndDate   = ( true == valStr( $this->m_strBillingEndDate ) ) ?  $this->m_strBillingEndDate : $this->m_strEndDate;

		if( false == $boolIsProrateCharges ) {
			$strStartDate = date( 'm/01/Y', strtotime( $strStartDate ) );
			$strEndDate	  = date( 'm/t/Y', strtotime( $strEndDate ) );
		}

		$objStartDate = date_create( $strStartDate );
		$objEndDate   = date_create( date( 'm/d/Y', strtotime( $strEndDate . ' +1 day' ) ) );

		$objInterval  = date_diff( $objStartDate, $objEndDate );

		$intMonth 	  = ( $objInterval->format( '%y' ) * 12 ) + $objInterval->format( '%m' );
		$intDays  	  = $objInterval->format( '%d' );

		$fltTermMonth = $intMonth . '.' . $intDays;

		if( false == $boolIsProrateCharges && 0 < $intDays ) {
			$fltTermMonth = $intMonth + 1;
		}

		return $fltTermMonth;
	}

	public function updateLeaseTermDependencies( $intCurrentUserId, $objDatabase, $intOldTermMonth, $boolReturnSqlOnly = false ) {

		$strSql = 'SELECT * FROM update_lease_term_dependencies(' . ( int ) $this->getCid() . ',' . ( int ) $this->getId() . ',' . ( int ) $intCurrentUserId . ',' . ( int ) $intOldTermMonth . ');';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} elseif( false == $this->executeSql( $strSql, $this, $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}

		return true;
	}

	public function updateLeaseStartWindows( $strUpdateFields, $boolIsReturnSql = false ) {

		$strSql = 'UPDATE lease_start_windows lsw
                    SET
                       ' . $strUpdateFields . '
                    WHERE
						lsw.cid = ' . ( int ) $this->getCid() . '
						AND lsw.lease_term_id = ' . ( int ) $this->getId() . '
						AND lsw.deleted_on IS NULL
						AND lsw.deleted_by IS NULL
						AND lsw.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ';';

		if( true == $boolIsReturnSql )
			return $strSql;
	}

	public function fetchDuplicateLeaseTermCount( $objDatabase, $intLeaseTermTypeId = CLeaseTermType::CONVENTIONAL, $intOccupancyTypeId = COccupancyType::CONVENTIONAL ) {

		if( false == valId( $this->getTermMonth() ) ) return NULL;

		$strSql = ' WHERE cid = ' . $this->getCid() . ' AND lease_term_structure_id = ' . $this->getLeaseTermStructureId() . ' AND term_month = ' . $this->getTermMonth() . ' AND deleted_by IS NULL';

		if( true == valId( $intLeaseTermTypeId ) ) {
			$strSql .= ' AND lease_term_type_id = ' . ( int ) $intLeaseTermTypeId;
		}

		if( true == valId( $intOccupancyTypeId ) ) {
			$strSql .= ' AND occupancy_type_id = ' . ( int ) $intOccupancyTypeId;
		}

		if( true == valId( $this->getId() ) ) {
			$strSql .= ' AND id <> ' . ( int ) $this->getId();
		}

		return ( int ) \Psi\Eos\Entrata\CLeaseTerms::createService()->fetchLeaseTermCount( $strSql, $objDatabase );
	}

}
?>
