<?php
use Psi\Eos\Entrata\CInspectionForms;

class CInspectionForm extends CBaseInspectionForm {

	const PROPERTY_TYPE_INSPECTION = 1;
	const UNIT_TYPE_INSPECTION = 2;

	protected $m_arrobjInspectionFormLocationProblems;
	protected $m_arrobjPropertyInspectionForms;

    /**
    * Get or Fetch Functions
    *
    */

    public function getOrFetchInspectionFormLocationProblems( $intPropertyId, $objDatabase, $boolIsIncludePropertyDisabledLocationsProblems = false, $boolFetchData = false ) {
    	if( true == valArr( $this->m_arrobjInspectionFormLocationProblems ) ) return $this->m_arrobjInspectionFormLocationProblems;
	    $this->m_arrobjInspectionFormLocationProblems = \Psi\Eos\Entrata\CInspectionFormLocationProblems::createService()->fetchInspectionFormLocationProblemsByInspectionFormIdByPropertyIdByMaintenanceLocationIdsByCid( $this->getId(), $intPropertyId, NULL, $this->getCid(), $objDatabase, $boolIsIncludePropertyDisabledLocationsProblems, $boolFetchData );
	    return $this->m_arrobjInspectionFormLocationProblems;
    }

    public function getOrFetchPropertyInspectionForms( $objDatabase ) {
    	if( true == valArr( $this->m_arrobjPropertyInspectionForms ) ) return $this->m_arrobjPropertyInspectionForms;
	    $this->m_arrobjPropertyInspectionForms = \Psi\Eos\Entrata\CPropertyInspectionForms::createService()->fetchPropertyIdsByInspectionFormIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	    return $this->m_arrobjPropertyInspectionForms;
    }

    /**
    * Fetch Functions
    *
    */

	public function fetchUniTypeInspectionFormLocationProblems( $intPropertyId, $intPropertyUnitId, $arrintUnitSpaceIds = [], $objDatabase, $boolIsIncludePropertyDisabledLocationsProblems = false, $arrintMaintenanceLocationIds = NULL, $boolFetchData = true, $boolIsFromResidentPortal = false, $boolIsShowInspectionForm = false, $intInspectionId = NULL ) {
		return \Psi\Eos\Entrata\CInspectionFormLocationProblems::createService()->fetchInspectionFormLocationProblemsByInspectionFormIdByPropertyIdByPropertyUnitIdByUnitSpaceIdsByCid( $this->getId(), $intPropertyId, $intPropertyUnitId, $arrintUnitSpaceIds, $this->getCid(), $objDatabase, $boolIsIncludePropertyDisabledLocationsProblems, $arrintMaintenanceLocationIds, $boolFetchData, $boolIsFromResidentPortal, $boolIsShowInspectionForm, $intInspectionId );
	}

    /**
    * Validate Functions
    *
    */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valDisabledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDisabledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valName( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) || 0 == \Psi\CStringService::singleton()->strcasecmp( \Psi\CStringService::singleton()->ucfirst( $this->getName() ), 'Some Inspection Name' ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( ' Inspection form name is required.' ) ) );
        }

	    $objInspectionForm = CInspectionForms::createService()->fetchInspectionFormByNameByCid( $this->getName(), $this->getCid(), $objDatabase );

        if( true == valObj( $objInspectionForm, 'CInspectionForm' ) && $this->getId() != $objInspectionForm->getId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( ' Inspection form name already exists.' ) ) );
        }

        return $boolIsValid;
    }

    public function valInspectionFormPropertiesForScheduledTasks( $objDatabase, $arrintInspectionFormPropertyIds ) {
	    $boolIsValid = true;

	    $arrintInspectionFormScheduledTaskIds = array_keys( rekeyArray( 'property_id', \Psi\Eos\Entrata\CScheduledTasks::createService()->fetchScheduledTaskPropertyIdsByInspectionFormIdByPropertyIdsByCid( $this->getId(), $arrintInspectionFormPropertyIds, $this->getCid(), $objDatabase ) ) );

	    if( true == valArr( $arrintInspectionFormScheduledTaskIds ) ) {
		    foreach( $arrintInspectionFormScheduledTaskIds as $intInspectionFormScheduledTaskId ) {
		    	if( true == valId( $intInspectionFormScheduledTaskId ) ) {
				    $boolIsValid &= in_array( $intInspectionFormScheduledTaskId, $arrintInspectionFormPropertyIds );
			    } else {
				    $arrintPreviousInspectionFormPropertyIds = array_keys( $this->getOrFetchPropertyInspectionForms( $objDatabase ) );

				    if( \Psi\Libraries\UtilFunctions\count( $arrintPreviousInspectionFormPropertyIds ) != \Psi\Libraries\UtilFunctions\count( array_intersect( $arrintPreviousInspectionFormPropertyIds, $arrintInspectionFormPropertyIds ) ) ) {
					    $boolIsValid = false;
				    }
			    }
		    }
	    }

	    if( false == $boolIsValid ) {
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Scheduled task', __( ' Properties associated to an inspection schedule cannot be disassociated from the inspection template.' ) ) );
	    }

	    return $boolIsValid;
    }

    public function valInspectionMaintenanceLocationName( $boolIsDuplicateMaintenanceLocationExists ) {
    	$boolIsValid = true;

    	if( true == $boolIsDuplicateMaintenanceLocationExists ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inspection_maintenance_location_name', __( ' Maintenance location name can not be same.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valInspectionFormsResidentInspections( $objDatabase ) {
    	$boolIsValid = true;

	    $intInspectionsCount = ( int ) \Psi\Eos\Entrata\CInspections::createService()->fetchInspectionsCountByInspectionFormIdByCid( $this->getId(), $this->getCid(), $objDatabase );

    	if( 0 < $intInspectionsCount ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inspection_forms', __( ' This inspection template has corresponding data and can only be disabled.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valCustomText() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTermsAndConditions() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsResidentVisible() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledInterval() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIncludeResidentSignature() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIncludeApproveAll() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsCompleted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $boolIsDuplicateMaintenanceLocationExists = false, $objDatabase = NULL, $arrintInspectionFormPropertyIds = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valInspectionMaintenanceLocationName( $boolIsDuplicateMaintenanceLocationExists );

				if( true == valArr( $arrintInspectionFormPropertyIds ) ) {
					$boolIsValid &= $this->valInspectionFormPropertiesForScheduledTasks( $objDatabase, $arrintInspectionFormPropertyIds );
				}
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valInspectionFormsResidentInspections( $objDatabase );
            	break;

            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}

?>