<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CExpenseTypes
 * Do not add any new functions to this class.
 */

class CExpenseTypes extends CBaseExpenseTypes {

	public static function fetchExpenseTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CExpenseType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchExpenseType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CExpenseType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchActiveExpenseTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM expense_types WHERE is_published = true ORDER BY order_num';
		return self::fetchExpenseTypes( $strSql, $objDatabase );
	}
}
?>