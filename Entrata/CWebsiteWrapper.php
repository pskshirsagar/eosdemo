<?php

class CWebsiteWrapper extends CBaseWebsiteWrapper {

	/**
	* Set Functions
	*
	*/

	public function setWrapperCache( $strWrapperCache ) {
		$this->m_strWrapperCache = CStrings::strTrimDef( $strWrapperCache, -1, NULL, true, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrmixValues['wrapper_cache'] ) ) $this->setWrapperCache( $arrmixValues['wrapper_cache'] );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteWrapperTypeId() {
		$boolIsValid = true;

		if( 0 == $this->getWebsiteWrapperTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wrapper type', 'Wrapper type is required' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateWebsiteWrapperTypeId( $objDatabase ) {
 		$boolIsValid = true;

 		$intWebsiteWrapperCount = \Psi\Eos\Entrata\CWebsiteWrappers::createService()->fetchWebsiteWrapperCountByWebsiteWrapperTypeIdByWebsiteIdByCid( $this->getWebsiteWrapperTypeId(), $this->getWebsiteId(), $this->getCid(), $objDatabase );

 		if( 0 != $intWebsiteWrapperCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wrapper type', 'Wrapper type already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valUpdateDuplicateWebsiteWrapperTypeId( $objDatabase ) {
 		$boolIsValid = true;

 		$intWebsiteWrapperCount = \Psi\Eos\Entrata\CWebsiteWrappers::createservice()->fetchWebsiteWrapperCountByWebsiteWrapperTypeIdByWebsiteIdByCidById( $this->getWebsiteWrapperTypeId(), $this->getWebsiteId(), $this->getCid(), $this->getId(), $objDatabase );

		if( 0 != $intWebsiteWrapperCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wrapper type', 'Wrapper type already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valWrapperKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getWrapperKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wrapper_key', 'Wrapper key is required' ) );
		}

		return $boolIsValid;
	}

	public function valWrapperUrl() {
		$boolIsValid = true;

		if( true == is_null( $this->getWrapperUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wrapper_url', 'Wrapper url is required' ) );
			return $boolIsValid;
		} elseif( false == CValidation::checkUrl( $this->getWrapperUrl(), false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wrapper_url', CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDuplicateWebsiteWrapperTypeId( $objDatabase );
				$boolIsValid &= $this->valWrapperKey();
				$boolIsValid &= $this->valWrapperUrl();
				$boolIsValid &= $this->valWebsiteWrapperTypeId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUpdateDuplicateWebsiteWrapperTypeId( $objDatabase );
				$boolIsValid &= $this->valWrapperKey();
				$boolIsValid &= $this->valWrapperUrl();
				$boolIsValid &= $this->valWebsiteWrapperTypeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			default:
				// default;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Other Functions
	*
	*/

	public function refreshCache() {

		$arrmixParameters = [
			'url' => $this->getWrapperUrl(),
			'new_connection' => true,
			'connection_timeout' => 5,
			'execution_timeout' => 5,
			'ssl_verify_peer' => false,
			'ssl_verify_host' => false,
			'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
			'ssl_version' => CURL_SSLVERSION_TLSv1
		];

		$objExternalRequest = new CExternalRequest();
		$objExternalRequest->setParameters( $arrmixParameters );
		$strResponseData = $objExternalRequest->execute( $boolGetResponse = true );

		if( false === $strResponseData ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'wrapper_url', $objExternalRequest->getErrorMessage() ) );
			unset( $objExternalRequest );
			return false;
		}

		unset( $objExternalRequest );

		$strResponseData = \Psi\CStringService::singleton()->preg_replace( '@\<form(.*)\>@', '', $strResponseData );
		$strResponseData = str_replace( '</form>', '', $strResponseData );

		$this->setWrapperCache( $strResponseData );

		return true;
	}

}
?>