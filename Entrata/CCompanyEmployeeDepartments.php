<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeDepartments
 * Do not add any new functions to this class.
 */

class CCompanyEmployeeDepartments extends CBaseCompanyEmployeeDepartments {

	public static function fetchCompanyEmployeeDepartmentsNamesByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cd.id,
						util_get_translated( \'name\', cd.name, cd.details ) AS name
					FROM
					    company_departments cd
					    JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
					WHERE
					    ced.cid = ' . ( int ) $intCid . '
					    AND ced.company_employee_id =' . ( int ) $intCompanyEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeDepartmentsByCidByCompanyEmployeeIds( $intCid, $arrintCompanyEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyEmployeeIds ) || false == valArr( array_filter( $arrintCompanyEmployeeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    ced.company_employee_id,
					    string_agg ( cd.id::TEXT , \',\' ) AS company_department_ids,
					    string_agg ( util_get_translated( \'name\', cd.name, cd.details ), \', \' ) AS departments
					FROM
					    company_departments cd
					    JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
					WHERE
					    cd.cid = ' . ( int ) $intCid . '
					    AND ced.company_employee_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )
					GROUP BY
					    ced.company_employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeDepartmentByCidByCompanyEmployeeIdByDefaultCompanyDepartmentId( $intCid, $intCompanyEmployeeId, $intDefaultCompanyDepartmentId, $objDatabase ) {

		$strSql = 'SELECT
						ced.*
					FROM
					    company_departments cd
					    JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
					WHERE
					    cd.cid = ' . ( int ) $intCid . '
					    AND ced.company_employee_id =' . ( int ) $intCompanyEmployeeId . '
						AND cd.default_company_department_id = ' . ( int ) $intDefaultCompanyDepartmentId;

		return self::fetchCompanyEmployeeDepartment( $strSql, $objDatabase );
	}

	public static function fetchCompanyEmployeeDepartmentsByDefaultCompanyDepartmentIdByCid( $intDefaultCompanyDepartmentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ced.*
					FROM
						company_departments cd
						JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
					WHERE
						cd.cid = ' . ( int ) $intCid . '
						AND cd.default_company_department_id = ' . ( int ) $intDefaultCompanyDepartmentId;

		return self::fetchCompanyEmployeeDepartments( $strSql, $objDatabase );
	}

}
?>