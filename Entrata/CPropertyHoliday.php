<?php

class CPropertyHoliday extends CBasePropertyHoliday {

	const WEEKEND_SATURDAY	= 6;
	const WEEKEND_SUNDAY	= 7;

	protected $m_intDay;
	protected $m_strCompanyHolidayName;
	protected $m_strCompanyHolidayDescription;
	protected $m_strCompanyHolidayDate;

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getPropertyId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyHolidayId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getCompanyHolidayId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_holiday_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getOrderNum() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valOpenTime() {

    	$boolIsValid = true;
        $boolIsValid &= true;

		if( 0 < mb_strlen( trim( $this->getOpenTime() ) ) && false == CValidation::validate24HourTimeWithoutSeconds( $this->getOpenTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'open_time',  __( 'Open time improperly formatted.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
    }

    public function valCloseTime() {

    	$boolIsValid = true;
        $boolIsValid &= true;

		if( 0 < mb_strlen( trim( $this->getCloseTime() ) ) && false == CValidation::validate24HourTimeWithoutSeconds( $this->getCloseTime() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'close_time',  __( ' Close time improperly formatted.' ) ) );
			$boolIsValid &= false;
		}

		$arrstrCloseTime = explode( ':', $this->getCloseTime() );
		$arrstrOpenTime = explode( ':', $this->getOpenTime() );

		if( 0 < mb_strlen( trim( $this->getCloseTime() ) ) && 0 < mb_strlen( trim( $this->getOpenTime() ) ) && true == valArr( $arrstrCloseTime ) && true == valArr( $arrstrOpenTime ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrCloseTime ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrOpenTime ) ) {

			$intOpenTime 	= ( ( int ) ( $arrstrOpenTime[0] * 60 ) + ( int ) $arrstrOpenTime[1] );
			$intCloseTime 	= ( ( int ) ( $arrstrCloseTime[0] * 60 ) + ( int ) $arrstrCloseTime[1] );

			if( $intOpenTime > $intCloseTime ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Open time is greater than the close time.' ) ) );
				$boolIsValid &= false;
			} elseif( $intOpenTime == $intCloseTime ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Open time is same as close time.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
 				break;

			case 'validate_update_with_time':
				$boolIsValid &= $this->valOpenTime();
				$boolIsValid &= $this->valCloseTime();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Get Functions
	*
	*/

	public function getDay() {
		return $this->m_intDay;
	}

	public function getCompanyHolidayName() {
		return $this->m_strCompanyHolidayName;
	}

	public function getCompanyHolidayDescription() {
		return $this->m_strCompanyHolidayDescription;
	}

	public function getCompanyHolidayDate() {
		return $this->m_strCompanyHolidayDate;
	}

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );
		if( true == isset( $arrmixValues['day'] ) ) $this->setDay( $arrmixValues['day'] );
		if( true == isset( $arrmixValues['company_holiday_name'] ) ) $this->setCompanyHolidayName( $arrmixValues['company_holiday_name'] );
		if( true == isset( $arrmixValues['company_holiday_description'] ) ) $this->setCompanyHolidayDescription( $arrmixValues['company_holiday_description'] );
		if( true == isset( $arrmixValues['company_holiday_date'] ) ) $this->setCompanyHolidayDate( $arrmixValues['company_holiday_date'] );

		return;
	}

	public function setDay( $intDay ) {
		$this->m_intDay = $intDay;
	}

	public function setCompanyHolidayName( $strCompanyHolidayName ) {
		$this->m_strCompanyHolidayName = $strCompanyHolidayName;
	}

	public function setCompanyHolidayDescription( $strCompanyHolidayDescription ) {
		$this->m_strCompanyHolidayDescription = $strCompanyHolidayDescription;
	}

	public function setCompanyHolidayDate( $strCompanyHolidayDate ) {
		$this->m_strCompanyHolidayDate = CStrings::strTrimDef( $strCompanyHolidayDate, 240, NULL, true );
	}

	/**
	 * Overridden Functons.
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objVoipDatabase = NULL ) {

		$strSql = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			$objDatabase->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'database', __( 'Voip Database object not found.' ) ) );
			return false;
		}

		$objPropertyCallHoliday	= $this->loadPropertyCallHoliday( $objVoipDatabase, $objDatabase );
		if( true == valObj( $objPropertyCallHoliday, 'CPropertyCallHoliday' ) && false == $objPropertyCallHoliday->insert( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		return $strSql;

	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objVoipDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $objVoipDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly, $objVoipDatabase );
		}

	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objVoipDatabase = NULL ) {

		$strSql = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			$objDatabase->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'database', __( 'Voip Database object not found.' ) ) );
			return false;
		}

		$objPropertyCallHoliday	= $this->loadPropertyCallHoliday( $objVoipDatabase, $objDatabase );
		if( true == valObj( $objPropertyCallHoliday, 'CPropertyCallHoliday' ) && false == $objPropertyCallHoliday->update( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		return $strSql;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objVoipDatabase = NULL ) {

		$strSql = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			$objDatabase->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'database', __( 'Voip Database object not found.' ) ) );
			return false;
		}

		$objPropertyCallHoliday	= $this->loadPropertyCallHoliday( $objVoipDatabase, $objDatabase );
		if( true == valObj( $objPropertyCallHoliday, 'CPropertyCallHoliday' ) && false == $objPropertyCallHoliday->delete( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		return $strSql;
	}

	/**
	 * Other Functions.
	 */
	public function loadPropertyCallHoliday( $objVoipDatabase = NULL, $objClientDatabase = NULL ) {

		if( true == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			$objPropertyCallHoliday = CPropertyCallHolidays::fetchPropertyCallHolidayByCidByPropertyIdByPropertyHolidayId( $this->m_intCid, $this->m_intPropertyId, $this->m_intId, $objVoipDatabase );
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
			$objCompanyHoliday = CCompanyHolidays::fetchCompanyHolidayByIdByCid( $this->m_intCompanyHolidayId, $this->m_intCid, $objClientDatabase );
		}

		if( false == valObj( $objPropertyCallHoliday, 'CPropertyCallHoliday' ) ) {
			$objPropertyCallHoliday = new CPropertyCallHoliday();
		}

		$objPropertyCallHoliday->setCid( $this->m_intCid );
		$objPropertyCallHoliday->setPropertyId( $this->m_intPropertyId );
		$objPropertyCallHoliday->setPropertyHolidayId( $this->m_intId );
		$objPropertyCallHoliday->setOffDate( ( true == valObj( $objCompanyHoliday, 'CCompanyHoliday' ) ) ? $objCompanyHoliday->getDate() : '' );
		$objPropertyCallHoliday->setOpenTime( $this->m_strOpenTime );
		$objPropertyCallHoliday->setCloseTime( $this->m_strCloseTime );
		$objPropertyCallHoliday->setIsPublished( !( 0 == $this->m_intIsPublished ) );
		$objPropertyCallHoliday->setUpdatedBy( $this->m_intUpdatedBy );
		$objPropertyCallHoliday->setUpdatedOn( $this->m_strUpdatedOn );
		$objPropertyCallHoliday->setCreatedBy( $this->m_intCreatedBy );
		$objPropertyCallHoliday->setCreatedOn( $this->m_strCreatedOn );

		return $objPropertyCallHoliday;
	}

}
?>