<?php

class CSubsidyUnitAssignmentType extends CBaseSubsidyUnitAssignmentType {

	// This will be the new constants which will be used for TAX-Credit 2.0 Program setup and in other places where we need these type constants.
	const ALL_UNITS = 4;
	const FIXED_UNITS = 2;
	const FLOATING_ALL_UNITS = 1;
	const FLOATING_UNIT_TYPES = 3;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>