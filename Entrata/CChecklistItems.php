<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistItems
 * Do not add any new functions to this class.
 */

class CChecklistItems extends CBaseChecklistItems {

	public static function fetchCustomChecklistItemsByChecklistIdByCid( $intChecklistId, $intCid, $objDatabase, $strSelectSql = '*' ) {

		$strSql = '	SELECT
					    ' . $strSelectSql . '
					FROM
					    checklist_items ci
					WHERE
						ci.cid = ' . ( int ) $intCid . '
						AND ci.checklist_id = ' . ( int ) $intChecklistId . '
					  	AND ci.deleted_on IS NULL
						AND ci.deleted_by IS NULL
					ORDER BY
						order_num';

		if( '*' != $strSelectSql ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchChecklistItems( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );

	}

	public static function fetchChecklistItemsByChecklistsFilterByCid( $objChecklistsFilter, $intCid, $objDatabase, $boolIsMoveInCheckList = false ) {

		if( false == valObj( $objChecklistsFilter, 'CChecklistsFilter' ) || false == valId( $objChecklistsFilter->getPropertyId() ) || false == valId( $objChecklistsFilter->getChecklistTriggerId() ) )	return NULL;

		$strSqlSelect	= '';
		$strSqlJoin		= '';
		$strSqlWhere	= '';
		$strSqlOrderBy  = '';
		$strDistinctSql = '';

		if( true == valId( $objChecklistsFilter->getOccupancyTypeId() ) ) {
			$strSqlWhere .= ' AND ' . ( int ) $objChecklistsFilter->getOccupancyTypeId() . ' = ANY ( c.occupancy_type_ids ) ';
		}

		if( true == valId( $objChecklistsFilter->getChecklistId() ) ) {
			$strSqlWhere .= ' AND c.id = ' . ( int ) $objChecklistsFilter->getChecklistId();
		}

		if( true == valId( $objChecklistsFilter->getChecklistItemId() ) ) {
			$strSqlWhere .= ' AND ci.id = ' . ( int ) $objChecklistsFilter->getChecklistItemId();
		}

		if( true == $objChecklistsFilter->getFetchForAutoComplete() ) {
			$strSqlWhere .= ' AND ( ( ci.checklist_item_type_option_id IS NOT NULL AND ci.checklist_item_type_option_id <> ' . CChecklistItemTypeOption::CUSTOM_ACTION . ' ) OR ci.checklist_item_type_id IN ( ' . CChecklistItemType::UPLOAD_RESIDENT_PHOTO . ', ' . CChecklistItemType::ASSIGNABLE_ITEMS . ', ' . CChecklistItemType::RENTABLE_ITEMS . ', ' . CChecklistItemType::UPLOAD_DOCUMENT . ', ' . CChecklistItemType::GENERATE_MERGE_FIELD_DOCUMENT . ', ' . CChecklistItemType::UNSIGNED_DOCS . ', ' . CChecklistItemType::TEXT_OPT_IN . ' ) ) ';
		}

		if( false == is_null( $objChecklistsFilter->getIsCompleted() ) ) {
			$strSqlWhere .= ( true == $objChecklistsFilter->getIsCompleted() ) ? ' AND lci.id IS NOT NULL' : ' AND lci.id IS NULL';
		}

		if( true == $objChecklistsFilter->getIsFromResidentPortal() && true == valArr( $objChecklistsFilter->getChecklistItemTypeOptionIds() ) && true == valArr( $objChecklistsFilter->getChecklistItemTypeIds() ) ) {
			$strSqlWhere .= ' AND ( ci.checklist_item_type_option_id IN ( ' . implode( ',', $objChecklistsFilter->getChecklistItemTypeOptionIds() ) . ') OR ci.checklist_item_type_id IN ( ' . implode( ',', $objChecklistsFilter->getChecklistItemTypeIds() ) . ') )';
		} elseif( false == $objChecklistsFilter->getIsFromResidentPortal() && true == valArr( $objChecklistsFilter->getChecklistItemTypeOptionIds() ) ) {
			$strSqlWhere .= ' AND ci.checklist_item_type_option_id IN ( ' . implode( ',', $objChecklistsFilter->getChecklistItemTypeOptionIds() ) . ')';
		}

		if( false == is_null( $objChecklistsFilter->getIsChecklistItemRequired() ) ) {
			$strSqlWhere  .= ( true == $objChecklistsFilter->getIsChecklistItemRequired() ) ? ' AND ci.is_required IS TRUE ' : ' AND ci.is_required IS FALSE ';
		}

		if( true == $objChecklistsFilter->getIsFromResidentPortal() ) {
			$strSqlWhere  .= ' AND ci.show_on_portals = TRUE';
		}

		if( true == $objChecklistsFilter->getIsChecklistItemTypeOptionRequired() ) {
			$strSqlWhere .= ' AND ci.checklist_item_type_option_id IS NOT NULL ';
		}

		if( true == valArr( $objChecklistsFilter->getChecklistItemTypeIds() ) ) {
			$strSqlWhere .= ' AND ci.checklist_item_type_id IN ( ' . implode( ',', $objChecklistsFilter->getChecklistItemTypeIds() ) . ' ) ';
		}

		if( true == valArr( $objChecklistsFilter->getChecklistTypeId() ) ) {
			$strSqlWhere .= ' AND c.checklist_type_id = ' . ( int ) $objChecklistsFilter->getChecklistTypeId();
		}

		$strSqlSelect	= 'ci.*, ';

		if( true == $objChecklistsFilter->getIsReturnArray() ) {
			$strSqlSelect	= 'ci.id, ci.cid, ci.checklist_id, ci.checklist_item_type_id, ci.checklist_item_type_option_id, ci.default_checklist_item_id, ci.document_id, ci.file_id, ci.file_type_id, util_get_translated( \'action_label\', ci.action_label, ci.details ) as action_label, util_get_translated( \'item_title\', ci.item_title, ci.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as item_title, ci.details, ci.show_on_portals, ci.is_required, ci.order_num, ci.deleted_by, ci.deleted_on, ci.updated_by, ci.updated_on, ci.created_by, ci.created_on, ';
		}

		if( true == valId( $objChecklistsFilter->getLeaseIntervalId() ) && true == valId( $objChecklistsFilter->getLeaseId() ) ) {

			if( true == $boolIsMoveInCheckList ) {
				$strSqlSelect	.= ' i.id as inspection_id,';
			}

			$strSqlSelect	.= ' lc.id as lease_checklist_id
								, lci.value as lease_checklist_items_value
								, lci.completed_by
								, lci.completed_on
								, lci.file_id as lese_checklist_item_file_id
								, cu.username,';

			$strSqlJoin		.= ' LEFT JOIN lease_checklists lc ON ( lc.cid = ci.cid AND lc.checklist_id = c.id  AND lc.lease_id = ' . ( int ) $objChecklistsFilter->getLeaseId() . ' AND lc.lease_interval_id = ' . ( int ) $objChecklistsFilter->getLeaseIntervalId();
			$strSqlJoin     .= ( true == valId( $objChecklistsFilter->getOrganizationContractId() ) && true == valId( $objChecklistsFilter->getCustomerId() ) ) ? ' AND lc.customer_id = ' . ( int ) $objChecklistsFilter->getCustomerId() . ' ) ' : ' ) ';
			$strSqlJoin     .= ' LEFT JOIN lease_checklist_items lci ON ( lci.cid = ci.cid AND lci.checklist_item_id = ci.id and lci.lease_checklist_id = lc.id )
								 LEFT JOIN company_users cu ON ( lci.cid = cu.cid AND lci.completed_by = cu.id AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )';

			$strSqlOrderBy = ' lci.completed_on DESC,';

			if( true == $boolIsMoveInCheckList ) {
				$strSqlOrderBy  = ' ci.id, ';
				$strDistinctSql = ' DISTINCT ON (ci.id) ';
				$strSqlJoin		.= ' LEFT JOIN inspections i ON ( lc.lease_id = i.lease_id AND i.cid = lc.cid  AND i.reviewed_on IS NULL AND i.reviewed_by IS NULL )';
			}
		}

		if( true == valId( $objChecklistsFilter->getOrganizationContractId() ) ) {
			$strSqlJoin		.= ' JOIN organization_contract_checklists occ ON ( c.cid = occ.cid AND c.id = occ.checklist_id AND pc.property_id = occ.property_id AND occ.organization_contract_id = ' . ( int ) $objChecklistsFilter->getOrganizationContractId() . ' AND occ.deleted_by IS NULL AND occ.deleted_on IS NULL )';
		}

		$strSql = 'SELECT ' . $strDistinctSql . '
						' . $strSqlSelect . '
						util_get_translated( \'name\', cit.name, cit.details ) as action,
						c.checklist_trigger_id,
						c.is_system::int,
						ci.is_required::int,
						c.id as checklist_id
					FROM checklist_items ci
						JOIN checklists c ON ( c.cid = ci.cid AND ci.checklist_id = c.id AND c.deleted_by IS NULL )
						JOIN property_checklists pc ON ( pc.cid = ci.cid AND pc.checklist_id = c.id )
						JOIN checklist_item_types cit ON ( ci.checklist_item_type_id = cit.id )
						' . $strSqlJoin . '
					WHERE
						ci.cid = ' . ( int ) $intCid . '
						AND pc.property_id = ' . ( int ) $objChecklistsFilter->getPropertyId() . '
						AND c.checklist_trigger_id = ' . ( int ) $objChecklistsFilter->getChecklistTriggerId() . '
						AND c.is_enabled IS TRUE
						AND ci.deleted_by IS NULL
						AND ci.deleted_on IS NULL
						' . $strSqlWhere . '
					ORDER BY
						' . $strSqlOrderBy . '
						ci.order_num,
						' . $objDatabase->getCollateSort( 'ci.item_title', true );

		if( true == $objChecklistsFilter->getIsReturnArray() ) return fetchData( $strSql, $objDatabase );

		return self::fetchChecklistItems( $strSql, $objDatabase );
	}

	public static function fetchChecklistItemsStatusByLeaseCustomerIdsByLeaseIdsByLeaseIntervalIdsByChecklistTypeOptionIdsByPropertyIdByCid( $arrintLeaseCustomerIds, $arrintLeaseIds = NULL, $arrintLeaseIntervalIds, $arrintChecklistItemTypeOptionIds, $intPropertyId, $intCid, $objDatabase, $boolIsForResidentPortal = false ) {

		if( false == valArr( $arrintChecklistItemTypeOptionIds ) ) return NULL;

		$strSelectFieldsSql = '';
		$strJoinSql 		= '';
		$strSqlGroupBy      = '';

		if( true == in_array( CChecklistItemTypeOption::EMERGENCY_CONTACT_INFO_SUBMITTED, $arrintChecklistItemTypeOptionIds ) ) {
			$strSelectFieldsSql .= ',CASE
									  	WHEN cc.customer_id IS NULL THEN 0
									  	ELSE 1
									 END AS is_emergency_contact_info_available';
			$strJoinSql .= 'LEFT JOIN customer_contacts cc ON ( cc.cid = c.cid AND cc.customer_id = c.id AND cc.customer_contact_type_id = ' . CCustomerContactType::EMERGENCY_CONTACT . ' )';
			$strSqlGroupBy .= ',is_emergency_contact_info_available';
		}

		if( true == in_array( CChecklistItemTypeOption::MAKE_READY_COMPLETED, $arrintChecklistItemTypeOptionIds ) ) {

			$strSelectFieldsSql .= ',make_ready.open_make_ready_count';
			$strJoinSql .= 'LEFT JOIN
									(
									  SELECT
										  COUNT ( mr.id ) AS open_make_ready_count,
										  mr.unit_space_id,
										  mr.cid
									  FROM
										  maintenance_requests mr
										  LEFT JOIN maintenance_statuses mst ON ( mr.maintenance_status_id = mst.id AND mr.cid = mst.cid )
									  WHERE
										  mr.cid = ' . ( int ) $intCid . '
										  AND mr.property_id = ' . ( int ) $intPropertyId . '
										  AND mr.maintenance_request_type_id = ' . ( int ) CMaintenanceRequestType::MAKE_READY . '
										  AND mr.parent_maintenance_request_id IS NULL
										  AND mst.maintenance_status_type_id NOT IN ( ' . CMaintenanceStatusType::COMPLETED . ', ' . CMaintenanceStatusType::CANCELLED . ' ) 
									  GROUP BY
										  mr.unit_space_id,
										  mr.cid
									) make_ready ON ( make_ready.cid = l.cid AND make_ready.unit_space_id = l.unit_space_id ) ';
			$strSqlGroupBy .= ',make_ready.open_make_ready_count';
		}

		if( true == in_array( CChecklistItemTypeOption::APPLICATION_SCREENED_AND_APPROVED, $arrintChecklistItemTypeOptionIds ) || true == in_array( CChecklistItemTypeOption::LEASE_APPROVED_AND_SIGNED, $arrintChecklistItemTypeOptionIds ) ) {
			$strSelectFieldsSql .= ',CASE
								  		WHEN ap.id IS NOT NULL AND ap.lease_approved_on IS NOT NULL THEN 1
								  		ELSE 0
									 END AS is_lease_approved,
									 CASE
								  		WHEN ap.id IS NOT NULL AND ap.screening_approved_on IS NOT NULL THEN 1
								  	 	ELSE 0
									 END AS is_application_screened_and_approved';
			$strJoinSql .= 'LEFT JOIN
							    (
									SELECT
										  apps.id,
										  apps.cid,
										  apps.lease_id,
										  apps.screening_approved_on,
										  apps.application_approved_on,
										  apps.lease_approved_on,
										  rank () OVER ( PARTITION BY apps.lease_id ORDER BY apps.id DESC )
									FROM
										  applications apps
									WHERE
										  apps.cid = ' . ( int ) $intCid . '
										  AND apps.property_id = ' . ( int ) $intPropertyId . '
							    ) ap ON ( ap.cid = l.cid AND ap.lease_id = l.id AND ap.rank = 1 )';
			$strSqlGroupBy .= ',is_lease_approved
							   ,is_application_screened_and_approved';
		}

		if( true == in_array( CChecklistItemTypeOption::PET_INFO, $arrintChecklistItemTypeOptionIds ) ) {
			$strSelectFieldsSql .= ',CASE
								  		 WHEN ( oc.id IS NULL OR c.id = ANY ( pet.customer_ids ) ) AND pet.customer_pet_count > 0 THEN 1
								  		ELSE 0
									 END AS pet_info';
			$strJoinSql .= 'LEFT JOIN
								 (
									SELECT
							          cp.cid,
							          la.lease_id,
							          COUNT( cp.id ) as customer_pet_count,
							          array_agg( cp.customer_id ) AS customer_ids
									FROM
										customer_pets cp
										JOIN lease_associations la ON ( cp.cid = la.cid  AND ar_origin_id = ' . CArOrigin::PET . ' AND cp.id = la.ar_origin_object_id )
									WHERE
										cp.cid = ' . ( int ) $intCid . '
										AND la.deleted_by IS NULL
										AND la.deleted_on IS NULL
										AND la.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
									GROUP BY
                                        cp.cid,
                                        la.lease_id
								 ) pet ON ( pet.cid = l.cid AND pet.lease_id = l.id )';
			$strSqlGroupBy .= ',pet_info';
		}

		if( true == in_array( CChecklistItemTypeOption::VEHICLE_INFO_SUBMITTED, $arrintChecklistItemTypeOptionIds ) ) {
			$strSelectFieldsSql .= ',cust_vehicle.customer_vehicle_count';
			$strJoinSql .= 'LEFT JOIN
								(
								  SELECT
									  COUNT ( lcv.license_plate_number ) AS customer_vehicle_count,
									  lcv.cid,
									  lcv.customer_id,
									  lcv.lease_id
								  FROM
									  lease_customer_vehicles lcv
								  WHERE
									  lcv.cid = ' . ( int ) $intCid . '
									  AND lcv.license_plate_number IS NOT NULL
									  AND lcv.deleted_by IS NULL
								  GROUP BY
									  lcv.cid,
									  lcv.customer_id,
									  lcv.lease_id
								) cust_vehicle ON ( cust_vehicle.cid = c.cid AND cust_vehicle.customer_id = c.id AND cust_vehicle.lease_id = l.id )';
			$strSqlGroupBy .= ',cust_vehicle.customer_vehicle_count';
		}

		if( true == in_array( CChecklistItemTypeOption::RENTERS_INSURANCE_INFO, $arrintChecklistItemTypeOptionIds ) ) {
			$strSelectFieldsSql .= ',customer_policy.customer_policy_count';
			$strJoinSql .= 'LEFT JOIN
								(
								  SELECT
									  COUNT ( ipc.id ) AS customer_policy_count,
									  ipc.cid,
									  ipc.customer_id,
									  ipc.lease_id
								  FROM
									  insurance_policy_customers ipc
									  JOIN resident_insurance_policies rip ON ( rip.cid = ipc.cid AND rip.property_id = ipc.property_id AND rip.id = ipc.resident_insurance_policy_id )
								  WHERE
									  ipc.cid = ' . ( int ) $intCid . '
									  AND ipc.property_id = ' . ( int ) $intPropertyId . ( ( true == valArr( $arrintLeaseIds ) ) ? ' AND ipc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )' : '' ) . '
									  AND (rip.confirmed_on IS NOT NULL OR rip.is_integrated = 1)
									  GROUP BY
									  ipc.cid,
									  ipc.customer_id,
									  ipc.lease_id
								) customer_policy ON ( customer_policy.cid = lc.cid AND customer_policy.customer_id = lc.customer_id AND customer_policy.lease_id = lc.lease_id )';
			$strSqlGroupBy .= ',customer_policy.customer_policy_count';
		}

		if( true == in_array( CChecklistItemTypeOption::INSPECTION_COMPLETED, $arrintChecklistItemTypeOptionIds ) ) {
			$strSelectFieldsSql .= ',COUNT(i.id) OVER ( partition BY l.id) as incomplete_inspections_count';
			$strJoinSql .= 'LEFT JOIN inspections i ON ( l.id = i.lease_id AND i.cid = l.cid  AND i.reviewed_on IS NULL AND i.reviewed_by IS NULL AND i.deleted_by IS NULL AND i.deleted_on IS NULL AND ( oc.id IS NULL OR i.customer_id = lc.customer_id ) )';
			$strSqlGroupBy .= ', i.id';
		}

		if( true == in_array( CChecklistItemTypeOption::FORWARDING_ADDRESS_COLLECTED, $arrintChecklistItemTypeOptionIds ) ) {
			$strSelectFieldsSql .= ',CASE
										WHEN ca.country_code IS NOT NULL AND ca.street_line1 IS NOT NULL AND ca.city IS NOT NULL AND ca.postal_code IS NOT NULL  THEN 1
									 	ELSE 0
									 END AS forwarding_address_collected';
			$strJoinSql .= 'LEFT JOIN customer_addresses ca ON ( ca.cid = l.cid AND ca.customer_id = c.id AND ca.address_type_id = ' . CAddressType::FORWARDING . ' AND ca.deleted_by IS NULL )';
			$strSqlGroupBy .= ', forwarding_address_collected';
		}

		if( true == in_array( CChecklistItemTypeOption::RESIDENT_PHOTO, $arrintChecklistItemTypeOptionIds ) ) {

			$strSelectFieldsSql .= ',CASE
									  WHEN customer_photo.media_file_count IS NULL THEN 0
									  ELSE 1
									 END AS is_photo_id_available';
			$strWhere = ' AND ccm.is_for_entrata = 1';
			if( true == $boolIsForResidentPortal ) {
				$strWhere = 'AND ( ccm.is_for_entrata = 0 OR ccm.is_for_entrata = 1 )';
			}

			$strJoinSql .= 'LEFT JOIN
								(
									SELECT
										COUNT(cmf.*) as media_file_count,
										ccm.customer_id,
										cmf.cid
									FROM
										company_media_files cmf
										JOIN customer_medias ccm ON ( ccm.cid = cmf.cid AND ccm.company_media_file_id = cmf.id AND cmf.media_type_id::int = ' . CMediaType::CUSTOMER_DEFAULT_IMAGE . ' )
									WHERE
											cmf.cid = ' . ( int ) $intCid . '
											' . $strWhere . '
									GROUP BY
									   ccm.customer_id,
									   cmf.cid
								) customer_photo ON ( c.cid = customer_photo.cid AND c.id = customer_photo.customer_id )';
			$strSqlGroupBy .= ',is_photo_id_available';

		}

		if( true == in_array( CChecklistItemTypeOption::KEYS_DISTRIBUTED, $arrintChecklistItemTypeOptionIds ) ) {

			$strSelectFieldsSql .= ',CASE
										WHEN ld.number_of_unit_keys	IS NOT NULL AND ld.number_of_mail_keys IS NOT NULL THEN 1
									 	ELSE 0
									 END AS keys_distributed';
			$strJoinSql .= 'LEFT JOIN lease_details ld ON( ld.cid = l.cid AND ld.lease_id = l.id )';
			$strSqlGroupBy .= ',keys_distributed';
		}

		if( true == in_array( CChecklistItemTypeOption::CONTACT_INFO_SUBMITTED, $arrintChecklistItemTypeOptionIds ) ) {
			$strSelectFieldsSql .= ',CASE
										WHEN ( 0 = customer_phone_number.cnt_phone_numbers ) OR cps.username IS NULL THEN 0
									ELSE 1
									END AS is_contact_info_available';

			$strSqlGroupBy .= ',is_contact_info_available';

			$strJoinSql .= ' LEFT JOIN LATERAL(
								SELECT count(1) AS cnt_phone_numbers
								FROM 
									customer_phone_numbers cpn
								WHERE 
									cpn.cid = c.cid AND
									cpn.customer_id = c.id AND
									cpn.phone_number_type_id IN ( ' . CPhoneNumberType::PRIMARY . ',' . CPhoneNumberType::HOME . ',' . CPhoneNumberType::OFFICE . ',' . CPhoneNumberType::MOBILE . ',' . CPhoneNumberType::FAX . ' ) AND
									cpn.deleted_by IS NULL
								) AS customer_phone_number ON TRUE';
		}

		$strSql = 'SELECT
						lc.id AS lease_customer_id,
						lc.customer_id,
						l.id AS lease_id
						' . $strSelectFieldsSql . '
					FROM
						leases l
						LEFT JOIN organization_contracts oc ON ( l.cid = oc.cid AND l.organization_contract_id = oc.id AND oc.organization_contract_financial_responsibility_type_id IN ( ' . COrganizationContractFinancialResponsibilityType::GROUP_IS_HUNDRED_PERCENT_RESPONSIBLE . ', ' . COrganizationContractFinancialResponsibilityType::SHARED . ' ) )
						JOIN lease_customers lc ON ( lc.cid = l.cid AND lc.lease_id = l.id AND ( oc.id IS NOT NULL OR lc.customer_type_id = ' . CCustomerType::PRIMARY . ' ) )
						JOIN customers c ON ( c.cid = lc.cid AND lc.customer_id = c.id )
						LEFT JOIN customer_portal_settings cps ON ( cps.cid = c.cid AND cps.customer_id = c.id )
						' . $strJoinSql . '
					WHERE
						l.cid = ' . ( int ) $intCid . '
						AND l.property_id = ' . ( int ) $intPropertyId . '
						AND lc.id IN ( ' . implode( ',', $arrintLeaseCustomerIds ) . ' )' . ( ( true == valArr( $arrintLeaseIds ) ) ? ' AND l.id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )' : '' ) . ( ( true == valArr( $arrintLeaseIntervalIds ) ) ? ' AND l.active_lease_interval_id IN ( ' . implode( ',', $arrintLeaseIntervalIds ) . ' )' : '' ) . '
					GROUP BY
						lc.id,
						lc.customer_id,
						l.id
						' . $strSqlGroupBy;

		$arrmixResultData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintLeaseIds ) ) return $arrmixResultData;

		if( true == valArr( $arrmixResultData ) ) {
			return $arrmixResultData;
		} else {
			return [];
		}
	}

	public static function fetchChecklistItemOptionTypeIdsByChecklistIdByChecklistItemTypeIdByCid( $intChecklistId, $intChecklistItemTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT ci.checklist_item_type_option_id
				   FROM
						checklist_items ci
				   WHERE ci.cid = ' . ( int ) $intCid . '
				   		 AND ci.checklist_id  = ' . ( int ) $intChecklistId . '
				   		 AND ci.checklist_item_type_id = ' . ( int ) $intChecklistItemTypeId . '
				         AND ci.deleted_by IS NULL
				         AND ci.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchChecklistItemsByPropertyIdByTriggerIdByOccupancyTypeIdByCid( $intPropertyId, $intTriggerId, $intOccupancyTypeId, $intCid, $objDatabase, $boolIsRequired = false, $boolIsCount = false, $intOrganizationContractId = NULL ) {
		$strWhere = '';
		$strSelect = ' ci.* ';
		$strJoinSql = '';

		if( true == $boolIsCount ) $strSelect = ' count(ci.id) ';

		if( true == $boolIsRequired ) $strWhere = ' AND ci.is_required IS TRUE';

		if( true == valId( $intOrganizationContractId ) ) {
			$strJoinSql = ' JOIN organization_contract_checklists occ ON( c.cid = occ.cid AND c.id = occ.checklist_id AND pc.property_id = occ.property_id AND organization_contract_id = ' . ( int ) $intOrganizationContractId . ' AND occ.deleted_by IS NULL AND occ.deleted_on IS NULL )';
		}

		$strSql = 'SELECT
						 ' . $strSelect . '
					FROM checklist_items ci
						JOIN checklists c ON ( c.cid = ci.cid AND ci.checklist_id = c.id AND c.deleted_by IS NULL )
						JOIN property_checklists pc ON ( pc.cid = ci.cid AND pc.checklist_id = c.id )
						JOIN checklist_item_types cit ON ( ci.checklist_item_type_id = cit.id )
						' . $strJoinSql . '
					WHERE
						ci.cid = ' . ( int ) $intCid . '
						AND pc.property_id = ' . ( int ) $intPropertyId . '
						AND c.checklist_trigger_id = ' . ( int ) $intTriggerId . '
						AND c.is_enabled IS TRUE ' . $strWhere . '
						AND ' . ( int ) $intOccupancyTypeId . ' = ANY ( c.occupancy_type_ids ) 
						AND ci.deleted_by IS NULL
						AND ci.deleted_on IS NULL ';

		if( true == $boolIsCount ) {
			$arrstrData = fetchData( $strSql, $objDatabase );
			return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['count'] : 0;
		}

		return self::fetchChecklistItems( $strSql, $objDatabase );
	}

	public static function fetchCustomChecklistItemByColumnNameByIdByCid( $strSelect, $intChecklistItemId, $intCid, $objDatabase ) {

		if( false == valId( $intChecklistItemId ) || false == valStr( $strSelect ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					' . $strSelect . '
					FROM
						checklist_items
					WHERE
						id = ' . ( int ) $intChecklistItemId . '
						AND cid =' . ( int ) $intCid;

		return parent::fetchColumn( $strSql, $strSelect, $objDatabase );
	}

	public static function fetchChecklistItemsProgressByPropertyIdByChecklistIdByCid( $intPropertyId, $intChecklistId, $intCid, $objDatabase, $intChecklistTriggerId ) {

		if( false == valId( $intPropertyId ) || false == valId( $intChecklistId ) || false == valId( $intCid ) || false == valStr( $intChecklistTriggerId ) ) return NULL;

		$strJoinSql  = '';
		$strUnionSql = '';
		$strWhereSql = '';

		if( CChecklistTrigger::MOVE_IN == $intChecklistTriggerId ) {
			$intLeaseStatusTypeId = CLeaseStatusType::FUTURE;
			$strChecklistTrigger  = 'in';
			$strJoinSql  = ' LEFT JOIN cached_leases cl2 ON ( cl.cid = cl2.cid AND cl2.transfer_lease_id = cl.id AND cl2.lease_status_type_id = ' . CLeaseStatusType::NOTICE . ' ) ';
			$strUnionSql = ' UNION
					SELECT
					    0 AS checklist_item_id,
					    \'' . __( 'Review Move-in' ) . '\' AS item_title,
					    1 AS is_required,
					    count( l . id ),
					    0 as order_num
					FROM
					    cached_leases l
					    JOIN lease_checklists lch ON( l . cid = lch . cid AND l . id = lch . lease_id AND l . active_lease_interval_id = lch . lease_interval_id )
					    JOIN checklists c ON( c . cid = l . cid AND lch . checklist_id = c . id AND l . occupancy_type_id = ANY( c . occupancy_type_ids ) )
					    LEFT JOIN cached_leases cl2 ON( l . cid = cl2 . cid AND cl2 . transfer_lease_id = l . id AND cl2 . lease_status_type_id = ' . CLeaseStatusType::NOTICE . ' )
					WHERE
					    l.cid = ' . ( int ) $intCid . '
					    AND l.property_id = ' . ( int ) $intPropertyId . '
					    AND l.lease_status_type_id = ' . CLeaseStatusType::FUTURE . '
					    AND lch.checklist_id = ' . ( int ) $intChecklistId . '
					    AND l.move_in_review_completed_on IS NOT NULL
					    AND ( cl2.move_out_date IS NULL OR ( l.move_in_date::DATE - cl2.move_out_date::DATE ) <> 1 ) ';
			$strWhereSql = ' AND ( cl2.move_out_date IS NULL OR ( cl.move_in_date::DATE - cl2.move_out_date::DATE ) <> 1 ) ';
		}
		if( CChecklistTrigger::MOVE_OUT == $intChecklistTriggerId ) {
			$intLeaseStatusTypeId = CLeaseStatusType::NOTICE;
			$strChecklistTrigger  = 'out';
			$strJoinSql = ' JOIN LATERAL 
						    (
						      SELECT
						          lp.cid,
						          lp.lease_id,
						          lp.move_out_date,
						          lp_future_transfer.move_in_date
						      FROM
						          lease_processes lp
						          LEFT JOIN lease_intervals li1 ON ( lp.transfer_lease_id = li1.lease_id AND lp.cid = li1.cid )
						          LEFT JOIN lease_processes lp_future_transfer ON ( lp_future_transfer.cid = lp.cid AND lp.transfer_lease_id = lp_future_transfer.lease_id AND lp_future_transfer.customer_id IS NULL AND li1.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . ' AND li1.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' )
						      WHERE
						          lp.cid = ' . ( int ) $intCid . '
						          AND lp.cid = cl.cid
						          AND cl.id = lp.lease_id
						          AND lp.customer_id IS NULL
						          AND CASE
						                WHEN lp.transfer_lease_id IS NOT NULL THEN li1.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '
						                ELSE TRUE
						              END
						          AND CASE
						                WHEN lp_future_transfer.move_in_date::DATE IS NULL
						          OR ( lp_future_transfer.move_in_date::DATE - lp.move_out_date::DATE ) <> 1 THEN TRUE
						                ELSE FALSE
						              END
						    ) AS sub ON ( TRUE ) ';
		}

		$strSql = 'SELECT
					    ci.id AS checklist_item_id,
					    ci.item_title,
					    ci.is_required::INT,
					    SUM ( CASE
					            WHEN cl.move_in_review_completed_on IS NOT NULL AND ci.id = lci.checklist_item_id THEN 1
					            ELSE 0
					          END ) AS completed_count,
					     ci.order_num     
					FROM
					    cached_leases cl
					    JOIN checklists c ON ( c.cid = cl.cid AND cl.occupancy_type_id = ANY( c.occupancy_type_ids ) )
					    JOIN checklist_items ci ON ( cl.cid = ci.cid AND c.id = ci.checklist_id AND ci.deleted_by IS NULL AND ci.deleted_on IS NULL )
					    LEFT JOIN lease_checklists lc ON ( cl.cid = lc.cid AND cl.id = lc.lease_id AND c.id = lc.checklist_id AND cl.active_lease_interval_id = lc.lease_interval_id )
					    LEFT JOIN lease_checklist_items lci ON ( lc.cid = lci.cid AND lc.id = lci.lease_checklist_id AND ci.id = lci.checklist_item_id )
					    LEFT JOIN organization_contracts oc ON ( cl.cid = oc.cid AND cl.property_id = oc.property_id AND cl.organization_contract_id = oc.id )
						' . $strJoinSql . '
					WHERE
					    cl.cid = ' . ( int ) $intCid . '
					    AND cl.property_id = ' . ( int ) $intPropertyId . '
					    AND cl.lease_status_type_id = ' . ( int ) $intLeaseStatusTypeId . '
					    AND ci.checklist_id = ' . ( int ) $intChecklistId . '
					    AND ( oc.id IS NULL OR JSONB_EXTRACT_PATH_TEXT ( oc.details, \'use_move_' . $strChecklistTrigger . '_checklist\' ) = \'1\' )
					    ' . $strWhereSql . '
					GROUP BY
					    ci.checklist_id,
					    ci.id,
					    ci.item_title,
					    ci.order_num,
					    ci.is_required ' . $strUnionSql . '
					    ORDER BY order_num';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
