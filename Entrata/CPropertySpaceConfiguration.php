<?php

class CPropertySpaceConfiguration extends CBasePropertySpaceConfiguration {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

	    if( false == valId( $this->m_intPropertyId ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please select valid student property.' ) ) );
	    }

        return $boolIsValid;
    }

    public function valSpaceConfigurationId() {
        $boolIsValid = true;

        if( false == valId( $this->m_intSpaceConfigurationId ) ) {
	        $boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please select Space Configuration.' ) ) );
        }

        return $boolIsValid;
    }

    public function valShowInEntrata() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valShowOnWebsite() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDependantInformation( $objDatabase ) {

    	$boolIsValid = true;

    	if( false == is_null( $this->getSpaceConfigurationId() ) ) {

    		$strWhereConditions = ' WHERE
									cid = ' . ( int ) $this->getCid() . '
									AND property_id = ' . ( int ) $this->getPropertyId() . '
									AND space_configuration_id = ' . ( int ) $this->getSpaceConfigurationId();

    		$intUnitSpaceUnionCount 	 = CUnitSpaceUnions::fetchUnitSpaceUnionCount( $strWhereConditions, $objDatabase );

	    	if( 0 < $intUnitSpaceUnionCount ) {
	    		$boolIsValid = false;
	    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Space options are currently associated with one or more units, to dissociate the space options it must first be removed from all units.' ) ) );
	    	}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
		        $boolIsValid &= $this->valPropertyId();
		        $boolIsValid &= $this->valSpaceConfigurationId();
		        break;

        	case VALIDATE_UPDATE:
        		break;

        	case VALIDATE_DELETE:
        		$boolIsValid &= $this->valDependantInformation( $objDatabase );
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>