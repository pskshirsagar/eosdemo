<?php

class CMergeFieldType extends CBaseMergeFieldType {

	const DROPDOWN_MERGE_FIELD	= 1;
	const MATH_MERGE_FIELD      = 2;
    const REFERENCE_MERGE_FIELD = 3;
    const COMPOSITE_MERGE_FIELD = 4;
    const BLOCK_MERGE_FIELD     = 5;
	const METHOD_MERGE_FIELD    = 6;
	const CHECKBOX_MERGE_FIELD  = 7;
    const DATE_MERGE_FIELD      = 8;
    const NUMBER_MERGE_FIELD    = 9;
    const TEXT_MERGE_FIELD      = 10;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>