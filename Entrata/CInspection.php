<?php
use Psi\Eos\Entrata\CUnitSpaces;

class CInspection extends CBaseInspection {

	// INSPECTIONS TYPES
	const ONE_WEEK_OUT 		= 1;
	const TWO_WEEK_OUT 		= 2;
	const THREE_WEEK_OUT 	= 3;
	const FOUR_WEEK_OUT 	= 4;

	// INSPECTION STATUSES
	const ALL_STATUSES  	= 0;
	const NOT_STARTED 		= 1;
	const IN_PROGRESS 		= 2;
	const UNDER_REVIEW 		= 3;
	const PAST_DUE			= 4;
	const COMPLETE 			= 5;

	const PROPERTY = 1;
	const RESIDENT = 2;
	const BOTH = 3;

	// INSPECTION FILTER SEARCH TYPE
	const INSPECTION_FILTER_SEARCH_TYPE_UNIT      = 'unit';
	const INSPECTION_FILTER_SEARCH_TYPE_RESIDENT  = 'resident';
	const INSPECTION_FILTER_SEARCH_TYPE_REFERENCE = 'reference';

	// INSPECTION SMS TYPES
	const RESIDENT_INSPECTION_SMS_CREATION          = 'is_resident_creation';
	const RESIDENT_INSPECTION_SMS_CANCELLED         = 'is_resident_cancelled';
	const RESIDENT_INSPECTION_SMS_SUBMITTED         = 'is_resident_submitted';
	const RESIDENT_INSPECTION_SMS_SCHEDULED         = 'is_resident_scheduled';
	const RESIDENT_INSPECTION_SMS_SCHEDULED_CHANGED = 'is_resident_scheduled_changed';

	protected $m_arrobjInspectionResponses;

	protected $m_strName;
	protected $m_strUnitNumber;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPropertyName;
	protected $m_strEncryptedCustomerId;
	protected $m_strMoveOutDate;
	protected $m_strUnitSpaceNumber;
	protected $m_strInspectedByCompanyEmployee;
	protected $m_strBuildingName;
	protected $m_strCustomerPhone;
	protected $m_strCustomerFullName;
	protected $m_intEventId;
	protected $m_strSmsType;
	protected $m_strSmsText;

	protected $m_intInspectionStatusId;
	protected $m_strInspectionStatus;
	protected $m_intLeaseStatusTypeId;
	protected $m_intUpdatedPsProductId;

	private $m_objSmsDatabase;
	protected $m_objInspectionNote;
	protected $m_objInspectionAttachment;
	protected $m_strMaintenanceRequestTypeName;
    /**
     * Get or Fetch Functions
     */

    public function getOrFetchInspectionResponse( $objDatabase ) {

    	if( true == valArr( $this->m_arrobjInspectionResponses ) ) return $this->m_arrobjInspectionResponses;
    	return $this->fetchInspectionResponse( $objDatabase );
    }

	public function getInspectionStatusById( $intId ) {
		$strInspectionStatusName = NULL;

		switch( $intId ) {

			case CInspection::NOT_STARTED:
				$strInspectionStatusName = __( 'Not Started' );
				break;

			case CInspection::PAST_DUE:
				$strInspectionStatusName = __( 'Past Due' );
				break;

			case CInspection::IN_PROGRESS:
				$strInspectionStatusName = __( 'In Progress' );
				break;

			case CInspection::UNDER_REVIEW:
				$strInspectionStatusName = __( 'Review' );
				break;

			case CInspection::COMPLETE:
				$strInspectionStatusName = __( 'Approved' );
				break;

			default:
				// default case
				break;
		}

		return $strInspectionStatusName;
	}

    /**
     * Fetch Functions
     */

    public function fetchInspectionResponse( $objDatabase ) {
    	$this->m_arrobjInspectionResponses = \Psi\Eos\Entrata\CInspectionResponses::createService()->fetchInspectionResponsesByInspectionIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    	return $this->m_arrobjInspectionResponses;
    }

    public function fetchInspectionResponsesByInspectionIdByInspectionFormLocationProblemIdByCid( $intInspectionFormLocationProblemId, $objDatabase ) {
    	$arrobjInspectionResponses = \Psi\Eos\Entrata\CInspectionResponses::createService()->fetchInspectionResponsesByInspectionIdByInspectionFormLocationProblemIdByCid( $this->getId(), $intInspectionFormLocationProblemId, $this->getCid(), $objDatabase );
    	return $arrobjInspectionResponses;
    }

    public function fetchInspectionNotesByInspectionIdByInspectionFormLocationProblemIdByCid( $intInspectionFormLocationProblemId, $objDatabase ) {

    	return \Psi\Eos\Entrata\CInspectionNotes::createService()->fetchInspectionNotesByInspectionIdByInspectionFormLocationProblemIdByCid( $this->getId(), $intInspectionFormLocationProblemId, $this->getCid(), $objDatabase );
    }

    public function fetchInspectionFormLocationProblemDetail( $intInspectionFormLocationProblemId, $objDatabase ) {
    	return \Psi\Eos\Entrata\CInspectionFormLocationProblems::createService()->fetchInspectionFormLocationProblemDetailByIdByInspectionIdByCid( $this->getInspectionFormId(), $this->getId(), $intInspectionFormLocationProblemId, $this->getCid(), $objDatabase );
    }

    public function fetchInspectionProblemActions( $intInspectionFormLocationProblemId, $objDatabase ) {
    	return \Psi\Eos\Entrata\CPropertyMaintenanceLocationProblems::createService()->fetchPropertyMaintenanceLocationActionsByInspectionFormLocationProblemIdByInspectionIdByCid( $intInspectionFormLocationProblemId, $this->getId(), $this->getCid(), $objDatabase );
    }

    public function fetchSubMaintenanceRequests( $intMaintenanceRequestId, $objDatabase ) {
    	return \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchSubMaintenanceRequestsByMaintenanceRequestIdByCid( $intMaintenanceRequestId, $this->getCid(), $objDatabase );
    }

    public function fetchFailedInspectionResponses( $objDatabase ) {
    	return \Psi\Eos\Entrata\CInspectionResponses::createService()->fetchFailedInspectionResponsesByInspectionIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

    public function fetchCustomerById( $objDatabase ) {
    	return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
    }

    public function fetchActivePropertiesById( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchActivePropertiesByIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objDatabase );
    }

    public function fetchWebVisiblePropertyPhoneNumberByPropertyId( $objDatabase ) {
    	return CPropertyPhoneNumbers::fetchWebVisiblePropertyPhoneNumberByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
    }

    public function fetchEnabledSmsEnrollmentsByCustomerIdByMessageTypeId( $intMessageTypeId, $objDatabase ) {
    	return \Psi\Eos\Entrata\CSmsEnrollments::createService()->fetchEnabledSmsEnrollmentsByCustomerIdsByMessageTypeIdByCid( [ $this->getCustomerId() ], $intMessageTypeId, $this->getCid(), $objDatabase );
    }

    /**
     * Create Functions
     */

    public function createInspectionResponse() {

   		$objInspectionResponse = new CInspectionResponse();
    	$objInspectionResponse->setInspectionId( $this->getId() );
    	$objInspectionResponse->setCid( $this->getCid() );

    	return $objInspectionResponse;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionFormId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyUnitId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceRequestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSignatureFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPsProductId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledTaskId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledDate() {
        $boolIsValid = true;

        if( false == is_null( $this->getScheduledDate() ) && false == is_null( $this->getInspectionDueDate() ) ) {
        	if( ( strtotime( $this->getScheduledDate() ) ) > ( strtotime( $this->getInspectionDueDate() ) ) ) {
        		$boolIsValid &= false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_date', __( 'Scheduled Date needs to be smaller than or equal to the Inspection Due Date.' ) ) );
        	}
        }

        return $boolIsValid;
    }

    public function valRequireScheduledDate() {
    	$boolIsValid = true;

    	if( false != is_null( $this->m_strScheduledDate ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_date', __( 'Scheduled Date is required.' ) ) );
    		$boolIsValid &= false;
    	}

    	return $boolIsValid;
	}

    public function valInspectionDueDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsResidentVisible() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSignatureDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspection() {
    	$boolIsValid = true;

    	if( false == is_null( $this->m_intCustomerId ) && true == is_null( $this->m_intLeaseId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'Lease is required.' ), 500 ) );
			$boolIsValid &= false;
		}

		if( true == is_null( $this->m_intCustomerId ) && false == is_null( $this->m_intLeaseId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Resident is required.' ), 500 ) );
			$boolIsValid &= false;
		}

    	return $boolIsValid;
    }

    public function valCustomerId() {
    	$boolIsValid = true;
    	// It is required
    	if( $this->m_intCustomerId == NULL ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Resident is required.' ), 500 ) );
    		$boolIsValid = false;
    	} elseif( $this->m_intCustomerId < 1 ) {
    		// Must be greater than zero
			trigger_error( __( 'A Company Customer Id has to be greater than zero - {%s, 0}', [ 'CInspection' ] ), E_USER_ERROR );
    		$boolIsValid = false;
    	}
    	return $boolIsValid;
    }

    public function valInspectedBy( $boolIsBulk = false ) {
        $boolIsValid = true;

        $strErrorMessage = __( 'Inspected By is required.' );
        if( true == is_null( $this->m_intInspectedBy ) ) {

        	if( false != $boolIsBulk ) {
        		$strErrorMessage = __( 'Assign To is required.' );
        	}

        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'inspected_by', $strErrorMessage, 500 ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valReviewedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReviewedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL, $boolIsResidentInspection = false ) {
    	// The variable is never used to setting it to NULL
    	$objDatabase = NULL;

        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valInspection();

            	if( false == $boolIsResidentInspection ) {
            		$boolIsValid &= $this->valInspectedBy( false );
            	}
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:

           	case 'validate_scheduled':
            	$boolIsValid &= $this->valRequireScheduledDate();
            	break;

			case 'validate_inspected_by':
				$boolIsValid &= $this->valInspectedBy( true );
				break;

            default:
            	// default case
           		break;
        }

        return $boolIsValid;
    }

    /**
     * Get Functions
     */

    public function getName() {
    	return $this->m_strName;
    }

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    public function getUnitNumber() {
    	return $this->m_strUnitNumber;
    }

    public function getNameFirst() {
    	return $this->m_strNameFirst;
    }

    public function getNameLast() {
    	return $this->m_strNameLast;
    }

    public function getInspectionStatusId() {
    	return $this->m_intInspectionStatusId;
    }

	public function getInspectionStatus() {
		return $this->m_strInspectionStatus;
	}

    public function getUnitSpaceNumber() {
    	return $this->m_strUnitSpaceNumber;
    }

	public function getBuildingName() {
		return $this->m_strBuildingName;
    }

	public function getInspectionNote() {
		return $this->m_objInspectionNote;
	}

	public function getInspectionAttachment() {
		return $this->m_objInspectionAttachment;
	}

	public function getCustomerPhone() {
		return $this->m_strCustomerPhone;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getMaintenanceRequestTypeName() {
		return $this->m_strMaintenanceRequestTypeName;
	}

	public function getSmsType() {
		return $this->m_strSmsType;
	}

	public function getSmsText() {
		return $this->m_strSmsText;
	}

	public function getUpdatedPsProductId() {
		return $this->m_intUpdatedPsProductId;
	}

   /**
    * Set Functions
    */

    public function setName( $strName ) {
    	$this->m_strName = $strName;
    }

    public function setNameLast( $strNameLast ) {
    	$this->m_strNameLast = \Psi\CStringService::singleton()->ucfirst( $strNameLast );
    }

    public function setNameFirst( $strNameFirst ) {
    	$this->m_strNameFirst = \Psi\CStringService::singleton()->ucfirst( $strNameFirst );
    }

    public function setPropertyName( $strPropertyName ) {
    	return $this->m_strPropertyName = $strPropertyName;
    }

    public function setUnitNumber( $strUnitNumber ) {
    	return $this->m_strUnitNumber = $strUnitNumber;
    }

    public function setInspectionStatusId( $intInspectionStatusId ) {
    	$this->m_intInspectionStatusId = CStrings::strToIntDef( $intInspectionStatusId, NULL, false );
    }

	public function setInspectionStatus( $strInspectionStatus ) {
		$this->m_strInspectionStatus = $strInspectionStatus;
	}

	public function setUnitSpaceNumber( $strUnitSpaceNumber ) {
    	$this->m_strUnitSpaceNumber = $strUnitSpaceNumber;
    }

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setCustomerPhone( $strCustomerPhone ) {
		$this->m_strCustomerPhone = $strCustomerPhone;
	}

    public function setInspectedByCompanyEmployee( $strInspectedByCompanyEmployee ) {
    	$this->m_strInspectedByCompanyEmployee = CStrings::strTrimDef( $strInspectedByCompanyEmployee, -1, NULL, true );
    }

    public function getInspectedByCompanyEmployee() {
    	return $this->m_strInspectedByCompanyEmployee;
    }

    public function setMoveOutDate( $strMoveOutDate ) {
        $this->m_strMoveOutDate = CStrings::strTrimDef( $strMoveOutDate, -1, NULL, true );
    }

    public function getMoveOutDate() {
        return $this->m_strMoveOutDate;
    }

	public function getCustomerFullName() {
		return $this->m_strCustomerFullName;
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function setMaintenanceRequestTypeName( $strMaintenanceRequestTypeName ) {
		return $this->m_strMaintenanceRequestTypeName = $strMaintenanceRequestTypeName;
	}

   public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
   		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

   		if( true == isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
   		if( isset( $arrmixValues['name_first'] ) )  $this->setNameFirst( $arrmixValues['name_first'] );
   		if( isset( $arrmixValues['name_last'] ) ) $this->setNameLast( $arrmixValues['name_last'] );
   		if( isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
   		if( isset( $arrmixValues['inspection_status'] ) ) $this->setInspectionStatusId( $arrmixValues['inspection_status'] );
	    if( isset( $arrmixValues['status'] ) ) $this->setInspectionStatus( $arrmixValues['status'] );
   		if( isset( $arrmixValues['unit_number'] ) ) $this->setUnitNumber( $arrmixValues['unit_number'] );
   		if( isset( $arrmixValues['unit_space_number'] ) ) $this->setUnitSpaceNumber( $arrmixValues['unit_space_number'] );
   		if( isset( $arrmixValues['inspected_by_company_employee'] ) ) $this->setInspectedByCompanyEmployee( $arrmixValues['inspected_by_company_employee'] );
   		if( isset( $arrmixValues['move_out_date'] ) ) $this->m_strMoveOutDate = trim( $arrmixValues['move_out_date'] );
	    if( isset( $arrmixValues['building_name'] ) ) $this->setBuildingName( $arrmixValues['building_name'] );
	    if( isset( $arrmixValues['customer_phone'] ) ) $this->setCustomerPhone( $arrmixValues['customer_phone'] );
	    if( isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
	    if( true == isset( $arrmixValues['customer_full_name'] ) ) $this->setCustomerFullName( $arrmixValues['customer_full_name'] );
		if( isset( $arrmixValues['maintenance_request_type_name'] ) ) $this->setMaintenanceRequestTypeName( $arrmixValues['maintenance_request_type_name'] );
   		return;
   }

	public function setInspectionNote( $objInspectionNote ) {
		$this->m_objInspectionNote = $objInspectionNote;
	}

	public function setInspectionAttachment( $objInspectionAttachment ) {
		$this->m_objInspectionAttachment = $objInspectionAttachment;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setCustomerFullName( $strCustomerFullName ) {
		$this->m_strCustomerFullName = $strCustomerFullName;
	}

	public function setEventId( $intEventId ) {
		$this->m_intEventId = $intEventId;
	}

	public function setSmsType( $strSmsType ) {
		$this->m_strSmsType = $strSmsType;
	}

	public function setSmsText( $strSmsText ) {
		$this->m_strSmsText = $strSmsText;
	}

	public function setUpdatedPsProductId( $intUpdatedPsProductId ) {
		$this->m_intUpdatedPsProductId = $intUpdatedPsProductId;
	}

    /**
     * Other Functions
     */

    public function buildDefaultSignatureFilePath() {
    	return 'inspections/' . date( 'Y' ) . '/' . date( 'n' ) . '/' . date( 'j' ) . '/';
    }

    public function buildDefaultSignatureFullFilePath() {
    	return getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->buildDefaultSignatureFilePath();
    }

	public function sendSmsNotificationForInspection( $objCustomer=NULL, $objWebsite= NULL, $arrobjProperties= [], $objPropertyPhoneNumbers= NULL, $intCompanyUserId, $objDatabase, $strMobileNumber ) {

		if( true == valObj( $this->loadSmsDatabase(), 'CDatabase' ) ) {
			$arrobjMessages = [];
			$strMessage = '';
			$strResidentPortalLoginUrl = '';

			if( true == valObj( $objWebsite, 'CWebsite' ) ) {
				$strResidentPortalLoginUrl = 'https://' . $objWebsite->getSubDomain() . '.residentportal.com';
			}

			$strInspectedBy = ( '1' == $this->getIsStaffInspection() ? CInspectionTaskType::STAFF : CInspectionTaskType::RESIDENT );

			if( CInspectionTaskType::RESIDENT == $strInspectedBy ) {
				$strMessage = 'You have a Unit Inspection to complete.';

				if( false == is_null( $this->getInspectionDueDate() ) ) {
					$strDate = new DateTime( $this->getInspectionDueDate() );
					$strMessage .= 'The Due Date is ' . $strDate->format( 'm/d/y' ) . '.';
				}

				if( false == empty( $strResidentPortalLoginUrl ) ) {
					$strMessage .= ' Login at ' . $strResidentPortalLoginUrl . '.';
				}
			} else {
				if( true == valArr( $arrobjProperties ) && true == array_key_exists( $this->getPropertyId(), $arrobjProperties ) ) {
					$strMessage = 'The ' . $arrobjProperties[$this->getPropertyId()]->getPropertyName() . ' Staff will be contacting you to schedule an Inspection of your residence.';

					if( true == valObj( $objPropertyPhoneNumbers, 'CPropertyPhoneNumber' ) ) {
						$strMessage .= ' Call the office for more info - ' . $objPropertyPhoneNumbers->getPhoneNumber() . '.';
					}
				}
			}

			if( true == empty( $strMessage ) ) {
				return false;
			}

			$strMessage = trim( $strMessage );

			$objMessage = new CMessage();
			$objMessage->setDefaults();
			$objMessage->setCid( $this->getCid() );
			$objMessage->setMessageTypeId( CMessageType::MAINTENANCE_NOTIFICATION );
			$objMessage->setMessageStatusTypeId( CMessageStatusType::PENDING_SEND );
			$objMessage->setPhoneNumber( $strMobileNumber );
			$objMessage->setMessageOperatorId( $objCustomer->getMessageOperatorId() );
			$objMessage->setCustomerId( $this->getCustomerId() );
			$objMessage->setPropertyId( $this->getPropertyId() );

			if( true == valArr( $arrobjProperties ) && true == array_key_exists( $this->getPropertyId(), $arrobjProperties ) ) {
				$objProperty 		 	 = $arrobjProperties[$this->getPropertyId()];
				$objTimeZone 		 	 = $objProperty->getOrfetchTimezone( $objDatabase );
				$strConvertedDateTime    = $this->convertIntoAnotherTimeZone( date( 'Y-m-d H:i:s' ), ( ( $objTimeZone->getAbbr() == 'AST' ) ? timezone_name_from_abbr( 'ADT' ) : timezone_name_from_abbr( $objTimeZone->getAbbr() ) ), date_default_timezone_get(), 'Y-m-d H:i:s' );

				$strConvertedHour = date( 'H', strtotime( $strConvertedDateTime ) );

				if( ( $strConvertedHour < 11 ) ) {
					$strScheduledSendDatetime = date( 'Y/m/d H:i:s', mktime( 11, 0, 0, date( 'm' ), date( 'd' ), date( 'Y' ) ) );
					$objMessage->setScheduledSendDatetime( $strScheduledSendDatetime );
				} elseif( $strConvertedHour >= 11 && $strConvertedHour <= 17 ) {
					$objMessage->setScheduledSendDatetime( date( 'Y/m/d H:i:s' ) );
				} elseif( ( $strConvertedHour > 17 ) ) {
					$strScheduledSendDatetime = date( 'Y/m/d H:i:s', mktime( 11, 0, 0, date( 'm' ), date( 'd' ) + 1, date( 'Y' ) ) );
					$objMessage->setScheduledSendDatetime( $strScheduledSendDatetime );
				}
			} else {
				$objMessage->setScheduledSendDatetime( date( 'Y-m-d H:i:s' ) );
			}

			if( ( CMessage::SMS_LENGTH - 5 ) < \Psi\CStringService::singleton()->strlen( $strMessage ) ) {

				$strFirstMessage 	= \Psi\CStringService::singleton()->substr( $strMessage, 0, 150 );
				$strSecondMessage 	= \Psi\CStringService::singleton()->substr( $strMessage, 150 );

				$objMessageSecond 	= clone $objMessage;
				$objMessageThird	= clone $objMessage;

				$objMessage->setMessage( '(1/3)' . $strFirstMessage );
				array_push( $arrobjMessages, $objMessage );

				$objMessageSecond->setMessage( '(2/3)' . $strSecondMessage );
				array_push( $arrobjMessages, $objMessageSecond );

				$objMessageThird->setMessage( '(3/3)' );
				array_push( $arrobjMessages, $objMessageThird );
			} else {
				$objMessageSecond = clone $objMessage;

				$objMessage->setMessage( '(1/2)' . $strMessage );
				array_push( $arrobjMessages, $objMessage );

				$objMessageSecond->setMessage( '(2/2)' );
				array_push( $arrobjMessages, $objMessageSecond );
			}

			$this->m_objSmsDatabase->begin();

			if( false == \Psi\Eos\Sms\CMessages::createService()->bulkInsert( $arrobjMessages, $intCompanyUserId, $this->m_objSmsDatabase ) ) {
				$this->m_objSmsDatabase->rollback();
				$this->m_objSmsDatabase->close();
				return false;
			}

			$this->m_objSmsDatabase->commit();
			$this->m_objSmsDatabase->close();

			return true;
		}
	}

	private function loadSmsDatabase() {

		if( false == valObj( $this->m_objSmsDatabase, 'CDatabase' ) ) {
			$this->m_objSmsDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::SMS, CDatabaseUserType::PS_PROPERTYMANAGER );
		}

		return $this->m_objSmsDatabase;
	}

	public function checkIsBlockedPhoneNumbers( $strMobileNumber ) {
		$arrstrBlockedNumbers = [];
		if( true == valStr( $strMobileNumber ) ) {
			$arrintMessageTypeIds = [ CMessageType::MAINTENANCE_NOTIFICATION, CMessageType::STOP ];
			$arrmixResponses = fetchData( 'SELECT phone_number FROM blocks WHERE ( phone_number =  \'' . $strMobileNumber . '\' or phone_number =  \'1' . $strMobileNumber . '\' )  AND message_type_id IN ( ' . implode( ',', $arrintMessageTypeIds ) . ' )', $this->loadSmsDatabase() );
			if( true == valArr( $arrmixResponses ) ) {
				return true;
			}
		}
		return false;
	}

	public function convertIntoAnotherTimeZone( $strDateTime, $strToTimeZone, $strFromTimeZone, $strDateFormat ) {
		$objDate = new DateTime( $strDateTime, new DateTimeZone( $strFromTimeZone ) );
		$objDate->setTimezone( new DateTimeZone( $strToTimeZone ) );
		$strDateTime = $objDate->format( $strDateFormat );
		return $strDateTime;
	}

	public function deleteInspectionNotesByInspectionResponseIds( $arrintInspectionResponseIds, $objDatabase ) {
		if( false == valArr( $arrintInspectionResponseIds ) ) return false;

		$strInspectionResponseIds = implode( ',', $arrintInspectionResponseIds );
		$strSql = 'DELETE FROM public.inspection_notes WHERE inspection_response_id IN ( ' . $strInspectionResponseIds . ' ) AND cid = ' . ( int ) $this->sqlCid() . ';';

		return $this->executeSql( $strSql, $this, $objDatabase );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strSql = 'UPDATE
						public.inspections
					SET ';
		$strSql .= ' deleted_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' deleted_on = \'NOW()\', ';
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function createInspectionEvent( $intEventSubTypeId, $objCompanyUser = NULL, $objOldInspection = NULL, $objDatabase, $objInspectionNote = NULL ) {

		$objEventLibrary 			= new CEventLibrary();
		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::INSPECTION_UPDATED, $intEventSubTypeId );
		$objEvent->setCid( $this->getCid() );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objEvent->setCompanyUser( $objCompanyUser );
			$intUserId = $objCompanyUser->getId();

			if( false == is_null( $objCompanyUser->getCompanyEmployeeId() ) ) {
				$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $objCompanyUser->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
				if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
					$objEvent->setCompanyEmployee( $objCompanyEmployee );
				}
			}
		} else {
			$intUserId = SYSTEM_USER_ID;
			$objEvent->setCompanyUser( NULL );
			if( CEventSubType::INSPECTION_EMAIL_SENT == $intEventSubTypeId ) {
				$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intUserId, $this->getCid(), $objDatabase );
				$objEvent->setCompanyUser( $objCompanyUser );
			}
		}

		$objEvent->setEventDatetime( 'NOW()' );

		if( false == is_null( $this->getCustomerId() ) ) {
			$objEvent->setCustomerId( $this->getCustomerId() );
			$objEvent->setLeaseId( $this->getLeaseId() );
		} else {
			$objEvent->setCustomerId( NULL );
			$objEvent->setLeaseId( NULL );
		}

		$objEvent->setDataReferenceId( $this->getId() );
		$objEvent->setPsProductId( $this->getUpdatedPsProductId() );
		$objEvent->setReference( $this );

		if( CEventSubType::INSPECTION_NOTE_ADDED == $intEventSubTypeId && true == valObj( $objInspectionNote, 'CInspectionNote' ) ) {
			$objEvent->setNotes( getSanitizedFormField( $objInspectionNote->getNote() ) );
		} else {
			if( CEventSubType::INSPECTION_GENERAL_NOTE_ADDED == $intEventSubTypeId || CEventSubType::INSPECTION_GENERAL_NOTE_UPDATED == $intEventSubTypeId ) {
				$objEvent->setNotes( getSanitizedFormField( $this->getNotes() ) );
			}
		}

		$objEvent->setOldReferenceData( $objOldInspection );
		$objEventLibrary->buildEventDescription( $this, false );
		$this->m_intEventId = $objEventLibrary->insertEvent( $intUserId, $objDatabase );
		if( false == valId( $this->m_intEventId ) ) {
			$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
			$objDatabase->rollback();
			return false;
		} else {
			if( CEventType::INSPECTION_UPDATED == $objEvent->getEventTypeId() && true == in_array( $objEvent->getEventSubTypeId(), [ CEventSubType::INSPECTION_CREATED, CEventSubType::INSPECTION_DUE_DATE, CEventSubType::INSPECTION_EMAIL_SENT ] ) ) {
				return $this->m_intEventId;
			}
		}
		return true;
	}

	public function prepareSmsData( $objCompanyUser, $objDatabase, $strSmsType = NULL ) {

		if( false == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			return false;
		}

		if( false == valStr( $strSmsType ) ) {
			return false;
		}

		$arrintMigrationModeOnPropertyIds = \Psi\Eos\Entrata\CProperties::createService()->fetchCustomMigrationModeOnPropertiesByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objDatabase );
		if( true == valArr( $arrintMigrationModeOnPropertyIds ) ) {
			return false;
		}

		$arrintSMSBlockedPropertyIds = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyIdsByPropertyPreferenceKeyByCids( 'SMS_MAINTENANCE_REQUESTS', [ $this->getCid() ], $objDatabase );
		if( true == array_key_exists( $this->getPropertyId(), $arrintSMSBlockedPropertyIds ) ) {
			return false;
		}

		$boolIsConsolidatedInspection = false;
		if( true == valArr( $this->getJsonInspectionDetails( 'customer_ids' ) ) && true == valArr( $this->getJsonInspectionDetails( 'unit_space_ids' ) ) && true == valArr( $this->getJsonInspectionDetails( 'lease_ids' ) ) ) {
			$boolIsConsolidatedInspection = true;
		}

		if( false == $this->getIsStaffInspection() && true == valId( $this->getLeaseId() ) && true == valId( $this->getCustomerId() ) ) {
			if( false == in_array( $strSmsType, [ self::RESIDENT_INSPECTION_SMS_CREATION, self::RESIDENT_INSPECTION_SMS_CANCELLED, self::RESIDENT_INSPECTION_SMS_SUBMITTED ] ) ) {
				return false;
			}
		} elseif( true == $this->getIsStaffInspection() && ( ( true == valId( $this->getUnitSpaceId() ) && true == valId( $this->getLeaseId() ) && true == valId( $this->getCustomerId() ) ) || true == $boolIsConsolidatedInspection ) ) {
			if( CInspection::RESIDENT_INSPECTION_SMS_CREATION == $strSmsType && true == valStr( $this->getScheduledDate() ) ) {
				$strSmsType = CInspection::RESIDENT_INSPECTION_SMS_SCHEDULED;
			} elseif( false == in_array( $strSmsType, [ self::RESIDENT_INSPECTION_SMS_SCHEDULED, self::RESIDENT_INSPECTION_SMS_SCHEDULED_CHANGED ] ) ) {
				return false;
			}

			$objUnitSpace = CUnitSpaces::createService()->fetchUnitSpaceByIdByPropertyIdByCid( $this->getUnitSpaceId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) && false == in_array( $objUnitSpace->getUnitSpaceStatusTypeId(), CUnitSpaceStatusType::$c_arrintOccupiedUnitSpaceStatusTypes ) ) {
				return false;
			}
		} else {
			return false;
		}
		$arrobjCustomers = [];

		if( false == is_null( $this->getCustomerId() ) && true == valObj( $this->getCustomer(), 'CCustomer' ) ) {
			$arrobjCustomers[$this->getCustomerId()] = $this->getCustomer();
		} elseif( false == is_null( $this->getCustomerId() ) ) {
			$arrintLeaseStatusTypeIds = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::FUTURE ];
			$arrobjCustomers[$this->getCustomerId()] = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByLeaseIdByLeaseStatusTypeIdsByCid( $this->getCustomerId(), $this->getLeaseId(), $arrintLeaseStatusTypeIds, $this->getCid(), $objDatabase );
		} elseif( true == $boolIsConsolidatedInspection ) {
			$arrobjCustomers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchActiveCustomersByIdsByUnitSpaceStatusTypeIdsByPropertyIdByCid( $this->getJsonInspectionDetails( 'customer_ids' ), CUnitSpaceStatusType::$c_arrintOccupiedUnitSpaceStatusTypes, $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		if( false == valArr( array_filter( $arrobjCustomers ) ) ) {
			return false;
		}
		$arrintCustomerIds = array_keys( $arrobjCustomers );

		$arrobjSmsEnrollments = \Psi\Eos\Entrata\CSmsEnrollments::createService()->fetchSmsEnrollmentsByCidByCustomerIdsByMessageTypeId( $this->getCid(), $arrintCustomerIds, CMessageType::MAINTENANCE_NOTIFICATION, $objDatabase );

		if( false == valArr( $arrobjSmsEnrollments ) ) {
			return false;
		}
		$arrobjSmsEnrollments = rekeyObjects( 'CustomerId', $arrobjSmsEnrollments );

		if( false == valStr( $this->getPropertyName() ) && true == in_array( $strSmsType, [ self::RESIDENT_INSPECTION_SMS_SUBMITTED, self::RESIDENT_INSPECTION_SMS_SCHEDULED, self::RESIDENT_INSPECTION_SMS_SCHEDULED_CHANGED ] ) ) {
			$arrstrPropertyNames = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyNameByPropertyIds( [ $this->getPropertyId() ], $objDatabase );
			$arrstrPropertyNames = rekeyArray( 'id', $arrstrPropertyNames );
			$this->setPropertyName( $arrstrPropertyNames[$this->getPropertyId()]['property_name'] );

			if( true == in_array( $strSmsType, [ self::RESIDENT_INSPECTION_SMS_SCHEDULED, self::RESIDENT_INSPECTION_SMS_SCHEDULED_CHANGED ] ) ) {
				$objTimeZone         = CTimeZones::fetchTimeZoneByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
				$strPropertyTimezone = ( true == valObj( $objTimeZone, 'CTimeZone' ) ) ? $objTimeZone->getTimeZoneName() : NULL;
			}
		}

		switch( $strSmsType ) {
			case self::RESIDENT_INSPECTION_SMS_CREATION:
				$strMessage = __( 'Inspection { %s, 0 } is now available for you to complete.', [ $this->getId() ] );
				break;

			case self::RESIDENT_INSPECTION_SMS_CANCELLED:
				$strMessage = __( 'Inspection { %s, 0 } has been cancelled.', [ $this->getId() ] );
				break;

			case self::RESIDENT_INSPECTION_SMS_SUBMITTED:
				$strMessage = __( 'Inspection { %s, 0 } has been submitted for { %s, 1 } to review.', [ $this->getId(), $this->getPropertyName() ] );
				break;

			case self::RESIDENT_INSPECTION_SMS_SCHEDULED:
				$strScheduledDate = $this->getScheduledDate();
				$arrobjCalendarEventTypes = rekeyObjects( 'DefaultCalendarEventTypeId', \Psi\Eos\Entrata\CCalendarEventTypes::createService()->fetchCalendarEventTypesByCalendarEventCategoryIdByCid( CCalendarEventCategory::MAINTENANCE, $this->getCid(), $objDatabase ) );
				if( true == valArr( $arrobjCalendarEventTypes ) ) {
					$objCalendarEvent = rekeyObjects( 'ReferenceId', \Psi\Eos\Entrata\CCalendarEvents::createService()->fetchCalendarEventByCalendarEventTypeByReferenceIdByCid( $arrobjCalendarEventTypes[CCalendarEventType::DEFAULT_MAINTENANCE_INSPECTION]->getId(), $this->getId(), $this->getCid(), $objDatabase ) );
					if( true == isset( $objCalendarEvent ) && true == valObj( $objCalendarEvent, 'CCalendarEvent' ) ) {
						if( true == valStr( $strPropertyTimezone ) ) {
							$strScheduledDate = CInternationalDateTime::create( $objCalendarEvent->getStartDatetime(), $strPropertyTimezone );
						} else {
							$strScheduledDate = $objCalendarEvent->getStartDatetime();
						}
					}
				}
				if( true == isset( $objCalendarEvent ) && true == valObj( $objCalendarEvent, 'CCalendarEvent' ) ) {
					if( true == valStr( $objCalendarEvent->getEndDatetime() ) ) {
						$strMessage = __( '{ %s, 0 } has scheduled an inspection of your residence for { %t, 1, DATETIME_NUMERIC_SHORT_NOTZ } ', [ $this->getPropertyName(), $strScheduledDate ] );
					} else {
						$strMessage = __( '{ %s, 0 } has scheduled an inspection of your residence for { %t, 1, DATE_NUMERIC_STANDARD }', [ $this->getPropertyName(), $strScheduledDate ] );
					}
				} else {
					$strMessage = __( '{ %s, 0 } has scheduled an inspection of your residence for { %t, 1, DATE_NUMERIC_STANDARD }', [ $this->getPropertyName(), $strScheduledDate ] );
				}
				break;

			case self::RESIDENT_INSPECTION_SMS_SCHEDULED_CHANGED:
				$strScheduledDate = $this->getScheduledDate();
				$arrobjCalendarEventTypes = rekeyObjects( 'DefaultCalendarEventTypeId', \Psi\Eos\Entrata\CCalendarEventTypes::createService()->fetchCalendarEventTypesByCalendarEventCategoryIdByCid( CCalendarEventCategory::MAINTENANCE, $this->getCid(), $objDatabase ) );
				if( true == valArr( $arrobjCalendarEventTypes ) ) {
					$objCalendarEvent = rekeyObjects( 'ReferenceId', \Psi\Eos\Entrata\CCalendarEvents::createService()->fetchCalendarEventByCalendarEventTypeByReferenceIdByCid( $arrobjCalendarEventTypes[CCalendarEventType::DEFAULT_MAINTENANCE_INSPECTION]->getId(), $this->getId(), $this->getCid(), $objDatabase ) );
					if( true == isset( $objCalendarEvent ) && true == valObj( $objCalendarEvent, 'CCalendarEvent' ) ) {
						if( true == valStr( $strPropertyTimezone ) ) {
							$strScheduledDate = CInternationalDateTime::create( $objCalendarEvent->getStartDatetime(), $strPropertyTimezone );
						} else {
							$strScheduledDate = $objCalendarEvent->getStartDatetime();
						}
					}
				}
				if( true == isset( $objCalendarEvent ) && true == valObj( $objCalendarEvent, 'CCalendarEvent' ) ) {
					if( true == valStr( $objCalendarEvent->getEndDatetime() ) ) {
						$strMessage = __( '{ %s, 0 } has changed the scheduled inspection of your residence to { %t, 1, DATETIME_NUMERIC_SHORT_NOTZ }', [ $this->getPropertyName(), $strScheduledDate ] );
					} else {
						$strMessage = __( '{ %s, 0 } has changed the scheduled inspection of your residence to { %t, 1, DATE_NUMERIC_STANDARD }', [ $this->getPropertyName(), $strScheduledDate ] );
					}
				} else {
					$strMessage = __( '{ %s, 0 } has changed the scheduled inspection of your residence to { %t, 1, DATE_NUMERIC_STANDARD }', [ $this->getPropertyName(), $strScheduledDate ] );
				}
				break;

			default:
				return false;
				break;
		}
		if( false == valStr( $strMessage ) ) {
			return false;
		}

		$arrmixCustomerPhoneNumbers = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomMobileNumberByCustomerIdsByCid( $arrintCustomerIds, $this->getCid(), $objDatabase );

		// Creating a new message which will be inserted into messages table in sms database.
		$arrobjMessages = [];
		if( true == valArr( $arrobjCustomers ) ) {
			foreach( $arrobjCustomers as $objCustomer ) {
				if( true == valObj( $objCustomer, 'CCustomer' ) ) {
					$strMobileNumber = '';
					if( true == isset( $arrmixCustomerPhoneNumbers[$objCustomer->getId()] ) && true == valStr( $arrmixCustomerPhoneNumbers[$objCustomer->getId()]['mobile_phone_number'] ) ) {
						$strMobileNumber = $arrmixCustomerPhoneNumbers[$objCustomer->getId()]['mobile_phone_number'];
					}

					if( $arrobjSmsEnrollments[$objCustomer->getId()] && true == is_null( $arrobjSmsEnrollments[$objCustomer->getId()]->getDisabledOn() ) && true == valStr( $strMobileNumber ) ) {
						// Creating a new message which will be inserted into messages table in sms database.
						$objMessage = new CMessage();
						$objMessage->setDefaults();
						$objMessage->setCid( $this->getCid() );
						$objMessage->setMessageTypeId( CMessageType::MAINTENANCE_NOTIFICATION );
						$objMessage->setMessageStatusTypeId( CMessageStatusType::PENDING_SEND );
						$objMessage->setPropertyId( $this->getPropertyId() );
						$objMessage->setCompanyEmployeeId( $this->getInspectedBy() );
						$objMessage->setPhoneNumber( str_replace( '-', '', $strMobileNumber ) );
						$objMessage->setMessageDatetime( date( 'Y-m-d H:i:s' ) );
						$objMessage->setIsInbound( 0 );
						$objMessage->setIsResidentSms( true );
						$objMessage->setCustomerId( $objCustomer->getId() );
						$objMessage->setLeaseId( $this->getLeaseId() );
						$objMessage->setFileTypeSystemCode( CFileType::SYSTEM_CODE_WORK_ORDER );
						$objMessage->setClientDatabase( $objDatabase );
						$objMessage->setMessage( $strMessage );
						$this->setCustomerFullName( $objCustomer->getNameFull() );
						$this->setSmsType( $strSmsType );
						$this->setSmsText( $strMessage );
						$arrobjMessages[$objCustomer->getNameFull()] = $objMessage;
					}
				}
			}
		}

		return $arrobjMessages;
	}

	public function setLocaleCode( $objProperty, $objDatabase ) {
		$strPreferredLocaleCode = CLanguage::ENGLISH;
		if( true == valObj( $objProperty, 'CProperty' ) && true == valStr( $objProperty->getLocaleCode() ) ) {
			$strPreferredLocaleCode = $objProperty->getLocaleCode();
		}
		CLocaleContainer::createService()->setPreferredLocaleCode( $strPreferredLocaleCode, $objDatabase );
	}

	function sendPrepareSms( $objCompanyUser, $objDatabase, $arrobjMessages ) {
		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$intCompanyUserId = $objCompanyUser->getId();
		} else if( true == valId( $objCompanyUser ) ) {
			$intCompanyUserId = $objCompanyUser;
		}

		if( true == valArr( $arrobjMessages ) ) {
			foreach( $arrobjMessages as $strCustomerName => $objMessage ) {
				if( true == valObj( $objMessage, 'CMessage' ) && false == $objMessage->insert( $intCompanyUserId, NULL, false, false ) ) {
					$this->addErrorMsgs( $objMessage->getErrorMsgs() );
					return false;
				}
				$this->setCustomerFullName( $strCustomerName );
				if( false == valId( $this->getCustomerId() ) ) {
					$this->setCustomerId( $objMessage->getCustomerId() );
				}
				$this->createInspectionEvent( CEventSubType::INSPECTION_OUTGOING_SMS_TO_RESIDENT, $objCompanyUser, NULL, $objDatabase );
				if( true == valArr( $this->getJsonInspectionDetails( 'customer_ids' ) ) && true == valId( $this->getCustomerId() ) ) {
					$this->setCustomerId( NULL );
				}
			}
		}

		return true;
	}

	public function setJsonbFieldValue( $strMethodName, $strKey, $mixValue ) {
		$strGetMethodName = 'get' . $strMethodName;
		$strSetMethodName = 'set' . $strMethodName;
		$objStdClass = $this->$strGetMethodName();

		if( false == valObj( $objStdClass, 'stdClass' ) ) {
			$objStdClass = new stdClass();
		}

		$objStdClass->$strKey = $mixValue;

		$this->$strSetMethodName( $objStdClass );

	}

	public function getJsonbFieldValue( $strMethodName, $strKey ) {
		$strMethodName = 'get' . $strMethodName;
		return ( false == is_null( $this->$strMethodName() ) && true == property_exists( $this->$strMethodName(), $strKey ) ? $this->$strMethodName()->$strKey : NULL );
	}

	public function setJsonInspectionDetails( $strKey, $arrmixKeyValues ) {

		$this->setJsonbFieldValue( 'InspectionDetails', $strKey, $arrmixKeyValues );
	}

	public function getJsonInspectionDetails( $strKey ) {
		$this->getJsonbFieldValue( 'InspectionDetails', $strKey );
		$arrintInspectionDetail = ( array ) $this->m_jsonInspectionDetails;
		return $arrintInspectionDetail[$strKey];
	}

	public function getConsolidatedInspectionUnitSpaces() {
		if( false == is_null( $this->getInspectiondetails() ) ) {
			$arrintUnitSpaceIds = $this->getJsonInspectionDetails( 'unit_space_ids' );
		} elseif( true == valId( $this->getUnitSpaceId() ) ) {
			$arrintUnitSpaceIds = [ $this->getUnitSpaceId() ];
		} else {
			$arrintUnitSpaceIds = NULL;
		}
		return $arrintUnitSpaceIds;
	}

	public function loadConsolidatedInspectionDetails( $intUnitSpaceId, $intCustomerId, $intLeaseId ) {
		$arrintUnitSpaceIds = ( $this->getUnitSpaceId() ) ? [ $this->getUnitSpaceId() ] : [];
		$arrintCustomerIds = ( $this->getCustomerId() ) ? [ $this->getCustomerId() ] : [];
		$arrintLeaseIds = ( $this->getLeaseId() ) ?[ $this->getLeaseId() ] : [];

		if( false == valArr( $arrintUnitSpaceIds ) ) {
			$arrintUnitSpaceIds = ( array ) $this->getJsonInspectionDetails( 'unit_space_ids' );
		}

		if( false == valArr( $arrintCustomerIds ) ) {
			$arrintCustomerIds = ( array ) $this->getJsonInspectionDetails( 'customer_ids' );
		}

		if( false == valArr( $arrintLeaseIds ) ) {
			$arrintLeaseIds = ( array ) $this->getJsonInspectionDetails( 'lease_ids' );
		}

		$arrintUnitSpaceIds = array_merge( $arrintUnitSpaceIds, [ $intUnitSpaceId ] );
		$arrintCustomerIds = array_merge( $arrintCustomerIds, [ $intCustomerId ] );
		$arrintLeaseIds = array_merge( $arrintLeaseIds, [ $intLeaseId ] );

		$this->setJsonInspectionDetails( 'unit_space_ids', $arrintUnitSpaceIds );
		$this->setJsonInspectionDetails( 'customer_ids', $arrintCustomerIds );
		$this->setJsonInspectionDetails( 'lease_ids', $arrintLeaseIds );

		$this->setUnitSpaceId( NULL );
		$this->setCustomerId( NULL );
		$this->setLeaseId( NULL );

		return $this;
	}

    public function getUpdatedCustomerId() {
        return $this->getDetailsField( 'updated_customer_id' );
    }

    public function setUpdatedCustomerId( $intUpdatedCustomerId ) {
        $this->setDetailsField( 'updated_customer_id', $intUpdatedCustomerId );
    }

	public function createInspectionEmailLibrary( $objDatabase ) {
		$objInspectionEmailLibrary = new CInspectionEmailLibrary();
		$objInspectionEmailLibrary->setInspection( $this );
		$objInspectionEmailLibrary->setDatabase( $objDatabase );

		return $objInspectionEmailLibrary;
	}

}
?>