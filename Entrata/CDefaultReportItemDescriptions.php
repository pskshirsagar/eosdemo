<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportItemDescriptions
 * Do not add any new functions to this class.
 */

class CDefaultReportItemDescriptions extends CBaseDefaultReportItemDescriptions {

	public static function fetchDefaultReportItemDescriptionsUsers( $objDatabase ) {

		$strSql = ' SELECT
						drid.*,
						rit.name AS report_item_type,
						COALESCE( func_format_customer_name( ce.name_first, ce.name_last ), cu.username ) AS updated_by,
						drid.details -> \'report_filter_type\' AS report_filter_type,
						drid.details -> \'report_column_format\' AS report_column_format
					FROM
						default_report_item_descriptions drid
						JOIN company_users cu ON ( cu.cid = drid.default_cid AND cu.id = COALESCE ( drid.updated_by, drid.created_by ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						JOIN report_item_types rit ON ( cu.cid = drid.default_cid AND rit.id = drid.report_item_type_id )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						drid.default_cid = 1
						AND rit.id IN ( ' . CReportItemType::FILTERS . ', ' . CReportItemType::COLUMNS . ' )
						AND drid.deleted_by IS NULL
						AND drid.deleted_on IS NULL
					ORDER BY
						drid.order_num ASC;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportItemDescriptionUserByItemId( $intItemId, $objDatabase ) {

		$strSql = ' SELECT
						drid.*,
						rit.name AS report_item_type,
						COALESCE( func_format_customer_name( ce.name_first, ce.name_last ), cu.username ) AS updated_by
					FROM
						default_report_item_descriptions drid
						JOIN company_users cu ON ( cu.cid = drid.default_cid AND cu.id = COALESCE ( drid.updated_by, drid.created_by ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						JOIN report_item_types rit ON ( cu.cid = drid.default_cid AND rit.id = drid.report_item_type_id )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						drid.default_cid = 1
						AND drid.id = ' . ( int ) $intItemId . '
						AND rit.id IN ( ' . CReportItemType::FILTERS . ', ' . CReportItemType::COLUMNS . ' )
						AND drid.deleted_by IS NULL
						AND drid.deleted_on IS NULL
					ORDER BY
						drid.id;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportItemDescriptionsByItemKeyByItemTypeId( $strItemKey, $intReportItemTypeId, $objDatabase ) {
		return self::fetchDefaultReportItemDescription( sprintf( 'SELECT drid.*, rit.name AS report_item_type FROM default_report_item_descriptions drid JOIN report_item_types rit ON ( rit.id = drid.report_item_type_id ) WHERE drid.default_cid = 1 AND drid.report_item_type_id = %d AND drid.item_key = %s AND drid.deleted_by IS NULL AND drid.deleted_on IS NULL', ( int ) $intReportItemTypeId, '\'' . $strItemKey . '\'' ), $objDatabase );
	}

	public static function fetchDefaultReportItemDescriptionsByIds( $arrintDefaultReportItemDescriptionIds, $objDatabase ) {
		$strSql = 'SELECT 
						*
					FROM
						default_report_item_descriptions
					WHERE 
						id IN ( ' . implode( ',', $arrintDefaultReportItemDescriptionIds ) . ' )';

		return self::fetchDefaultReportItemDescriptions( $strSql, $objDatabase );
	}

	public static function fetchDefaultReportItemDescriptionsDetailByReportItemTypeId( $intReportItemTypeId, $objDatabase ) {
		$strSql = '
			SELECT
				util_get_translated( \'item_name\', lower( item_name ), details ) AS item_name,
				item_key
			FROM
				default_report_item_descriptions
			WHERE
				report_item_type_id = ' . ( int ) $intReportItemTypeId . '
				AND deleted_by IS NULL
				AND deleted_on IS NULL
			ORDER BY 
				item_name';
		return fetchData( $strSql, $objDatabase );
	}

}
?>