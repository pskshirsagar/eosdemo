<?php

class CImportDataType extends CBaseImportDataType {

	const GL_ACCOUNTS 					= 1;
	const CHARGE_CODES 					= 2;
	const PROPERTIES 					= 3;
	const LEAD_SOURCES 					= 4;
	const BANK_ACCOUNTS 				= 5;
	const RESIDENTS_AND_APPLICANTS 		= 6;
	const VENDORS			 			= 7;
	const AMENITIES 					= 8;
	const ADD_ONS 						= 9;
	const RENTAL_RATES 					= 10;
	const MAINTENANCE_REQUESTS 			= 11;
	const INVOICES 						= 15;
	const TRANSACTIONS					= 16;
	const FLOOR_PLANS					= 17;
	const LEASING_AGENTS 				= 18;
	const COMPANY_USERS 				= 22;
	const OCCUPANTS 					= 23;
	const RECURRING_CHARGES 			= 32;
	const LEASE_INTERVALS 				= 33;
	const VENDOR_1099_BALANCE 			= 34;
	const GL_GROUPS		 				= 35;
	const DEMOGRAPHICS					= 36;
	const RESIDENT_INSURANCE			= 37;
	const AP_CODES						= 39;
	const FIXED_ASSETS					= 40;
	const PETS							= 41;
	const EMPLOYERS						= 42;
	const VEHICLES						= 43;
	const ADDRESSES						= 64;
	const EMERGENCY_CONTACTS			= 65;
	const COMMERCIAL_ADDRESSES			= 68;
	const COMMERCIAL_RENEWAL_OPTIONS	= 70;
	const LEASE_MODIFICATIONS	        = 333;

	const MIGRATION_PREFERENCES			= 19;
	const PROPERTY_AMENITIES 			= 20;
	const UNIT_AMENITIES 				= 21;

	// Extended Automation Constants
	const MAINTENANCE_PROBLEMS 			= 12;
	const MAINTENANCE_STATUSES 			= 13;
	const MAINTENANCE_PRIORITIES 		= 14;

	// Property Transfer data types
	const RESIDENTS 					= 24;
	const LEADS 						= 25;
	const SPECIALS 						= 26;
	const COMPANY_OWNERS 				= 27;
	const LEASE_AND_GL_BALANCES 		= 28;
	const VENDOR_INVOICES 				= 29;
	const OWNER_INVOICES 				= 30;
	const PROPERTY_UNITS 				= 31;
	const MAINTENANCE_REQUEST_SETTINGS 	= 38;
	const CATALOG_ITEMS					= 39;

	// Property / Websites
	const PROPERTY_PREFERENCES			= 44;
	const WEBSITE_DATA					= 45;
	const WEBSITE_PREFERENCES			= 46;

	const UNIT_TYPES					= 47;
	const SCHEDULED_PAYMENTS			= 48;
	const PROPERTY_LEVEL_PRICING		= 49;
	const PROPERTY_TAXES				= 50;

	const FLOORS						= 51;
	const EVENT_RESULTS					= 52;
	const OCCUPATIONS					= 53;
	const UTILITY_RESPONSIBILITIES		= 54;
	const PET_TYPES						= 55;
	const LEASE_TERMS					= 56;
	const LEASE_PERIODS					= 57;
	const IDENTIFICATION_TYPES			= 58;
	const LEASE_CUSTOMER_RELATIONSHIPS	= 59;
	const SCHEDULED_TASKS				= 60;
	const RENEWAL_TEMPLATES				= 61;
	const RENEWAL_TEMPLATE_TIERS		= 62;
	const RENEWAL_TEMPLATE_OFFERS		= 63;
	const COMMERCIAL_CONTACTS			= 69;
	const PROPERTY_MEDIA				= 66;
	const TENANTS_AND_SUITES			= 67;
	const RENT_STEPS					= 71;
	const BASELINE_FILE					= 72;
	const AFFORDABLE_TRANSACTIONS		= 73;
	const AFFORDABLE_RECURRING_CHARGES	= 74;
	const ANNUAL_INCOME 				= 77;
	const CUSTOMER_ASSETS_INCOME 		= 78;
	const TAX_CREDIT_INFORMATION 		= 79;
	const HUD_WAIT_LIST					= 80;
	const INSPECTIONS                   = 81;
	const MILITARY_DEMOGRAPHICS			= 82;
	const COMPETITORS					= 83;
	const CUSTOMER_DEMOGRAPHICS			= 84;
	const COMPETITOR_UNIT_TYPES			= 85;
	const AP_PAYMENTS 					= 86;
	const OWNERS						= 87;
	const LOCALIZATION_DATA				= 239;
	const CLD_DATA						= 244;
	const CONVENTIONAL_WAITLIST			= 309;

	// This is used in migration workbook operation as property types.
	const CONVENTIONAL					= 88;
	const STUDENT						= 89;
	const COMMERCIAL					= 90;

	const PURCHASE_ORDERS				= 91;
	const GUEST_CARDS					= 92;
	const ACTIVITY_LOGS					= 93;
	const REVIEWS						= 94;
	const BUILDING_SETTINGS				= 95;
	const DEFAULT_FIRST_COLUMN			= 'property name';

	// This is used in migration workbook Client report storage.
	const OPERATIONS							= 96;
	const ACCOUNTING							= 97;

	const INVENTORY 							= 98;
	const AFFORDABLE_RESIDENTS_AND_APPLICANTS	= 99;

	// This is used for utility setting template
	const UTILITY_SETTING_TEMPLATE					= 100;

	// Miscellaneous Data clean up tool
	const ASSIGN_PROPERTY_FLOORPLAN_TO_APPLICATION                              = 101;
	const ASSIGN_PROPERTY_FLOORPLANS_TO_ALL_APPLICATIONS                        = 102;
	const ASSIGN_PROPERTY_UNIT_TO_LEASE                                         = 103;
	const CANCEL_GUESTCARD                                                      = 104;
	const DELETE_ADD_ONS_WITH_RATES                                             = 105;
	const DELETE_EMPLOYMENT_INFORMATIONS                                        = 106;
	const DELETE_OCCUPANT                                                       = 107;
	const DELETE_ORPHAN_GL_ACCOUNTS                                             = 108;
	const DELETE_PET_INFORMATIONS                                               = 109;
	const DELETE_PROPERTY_BUILDING                                              = 110;
	const DELETE_PROPERTY_UNITS                                                 = 111;
	const DELETE_SCHEDULED_CHARGES                                              = 112;
	const DELETE_UNIT_SPACE_LEVEL_MARKET_RENT_DEPOSIT_AND_BUDGETED_RENT         = 113;
	const DELETE_UNREQUIRED_OR_ORPHAN_GL_ENTRIES_OR_MARK_ALL_AS_INITIAL_IMPORT  = 114;
	const DELETE_CANCELLED_ARCHIVED_LEADS_APPLICATIONS                          = 115;
	const INTEGRATE_PROPERTY                                                    = 116;
	const MERGER_CUSTOMER_RECORDS                                               = 117;
	const RE_NAME_UNIT                                                          = 118;
	const REMOVE_STREET_LINE2_ADDRESS                                           = 119;
	const REPAIR_LOGS                                                           = 120;
	const SOFT_DELETE_WORK_ORDERS                                               = 121;
	const SPLIT_SCHEDULED_CHARGES                                               = 122;
	const TAG_UTILITY_RESPONSIBILITIES_AT_LEASE_LEVEL                           = 123;
	const UPDATE_APPLICATION_AND_LEASE_SIGN_DATES                               = 124;
	const UPDATE_CUSTOMER_ID_CORPORATE_LEAD                                     = 125;
	const UPDATE_CUSTOMER_TYPE                                                  = 126;
	const UPDATE_GUEST_CARD_TO_ARCHIVED                                         = 127;
	const UPDATE_OCCUPANT_STATUS                                                = 128;
	const UPDATE_PAYMENT_BLOCK_ASSOCIATION                                      = 129;
	const UPDATE_REMOTE_PRIMARY_KEY                                             = 130;
	const UPDATE_UNIT_ADDRESS_AS_PER_USPS                                       = 131;
	const UPDATE_SPACE_OPTION                                                   = 160;
	const REPLACE_LEASING_AGENT                                                 = 163;

	// Transaction Data Cleanup Tool
	const VIEW_ADD_UNDEPOSITED_INITIAL_IMPORTED_PAYMENTS    = 132;
	const CLEAN_GL_ACCOUNTS                                 = 133;
	const VIEW_CLEANUP_FINANCIAL_DATA                       = 134;
	const DELETE_ALL_REVERSAL_ENTRIES                       = 135;
	const VIEW_DELETE_TRANSACTIONS                          = 136;
	const VIEW_SET_POSTED_DATES_ON_RECURRING_CHARGES        = 137;
	const VIEW_SET_RATE_ID_ON_SCHEDULED_CHARGES             = 138;
	const VIEW_SOFT_DELETE_HISTORIC_INVOICES                = 139;
	const VIEW_UPDATE_CHARGE_CODE                           = 140;

	// Lease Data Cleanup Tool
	const VIEW_DELETE_LEASE_WITH_TRANSACTIONS               = 143;
	const VIEW_DELETE_LEASE_INTERVAL                        = 144;
	const VIEW_PERFORM_FMO                                  = 146;
	const VIEW_SET_LATE_FEE_FORMULA                         = 147;
	const VIEW_SET_TRANSFER_ON_LEASES                       = 148;
	const VIEW_UPDATE_APPLICATIONS                          = 149;
	const VIEW_UPDATE_COLLECTION_STATUS                     = 150;
	const VIEW_UPDATE_LEASE_INTERVALS                       = 151;
	const VIEW_UPDATE_LEASE_TERMS                           = 152;
	const VIEW_UPDATE_LEASE_DETAILS                         = 153;
	const VIEW_UPDATE_MOVE_IN_DATE                          = 155;
	const VIEW_DELETE_NON_ACTIVE_LEASE_INTERVALS            = 156;

	// Property Data Management Tool
	const MIGRATE_LEASE_DOCUMENT                = 157;
	const MIGRATE_RESIDENT_STORED_BILLING_INFO  = 158;
	const MIGRATE_RESIDENT_PORTAL_LOGINS        = 159;
	const MAINTENANCE_LOCATIONS					= 161;
	const MAINTENANCE_TEMPLATES					= 162;
	const MAINTENANCE_ATTACHMENTS				= 172;
	const APPLICANTS				= 175;
	const PAST_RESIDENTS				= 180;
	const CANCELED_RESIDENTS = 181;

	const MIGRATE_INSTALLMENT_PLANS				        = 166;
	const MIGRATE_TAX_IDENTIFICATION_AND_DOB	        = 179;

	const APPLICANT_CONTACTS                            = 182;
	const CUSTOMER_CONTACTS                             = 183;
	const MIGRATE_APPLICATION_INTEREST                  = 184;
	const MIGRATE_CALLS                                 = 185;
	const MIGRATE_FLOORPLAN_UNIT_TYPE_ASSOCIATION       = 186;
	const MIGRATE_INSURANCE_POLICIES                    = 187;
	const MIGRATE_RECURRING_PAYMENTS                    = 188;
	const MIGRATE_RENEWAL_OFFERS                        = 189;
	const MIGRATE_RENEWAL_OFFERS_FOR_LEADS              = 190;
	const MIGRATE_RESIDENT_VERIFY_SCREENING_RECORDS     = 191;
	const MIGRATE_SCREENING_SETUP_WITH_RECORDS          = 192;
	const MIGRATE_PROPERTY_LEVEL_PRICING                = 193;
	const MIGRATE_PROPERTY_TAXES                        = 194;
	const PROPERTY_REVIEWS                              = 195;
	const SCRUB_RESIDENT_OR_APPLICANT_EMAILS            = 196;
	const MIGRATE_ACTIVITY_LOGS                         = 197;
	const MIGRATE_AMENITIES                             = 198;
	const MIGRATE_APPLICATIONS_DEMOGRAPHICS_INFO        = 199;
	const MIGRATE_APPLICANTS_OCCUPATION_INFORMATION     = 200;
	const MIGRATE_APPLICANT_GENDER_INFORMATION          = 201;
	const MIGRATE_APPLICANT_MARITAL_STATUS_INFORMATION  = 202;
	const MIGRATE_APPLICANT_BIRTH_DATE_INFORMATION      = 203;
	const MIGRATE_CUSTOMER_PETS                         = 204;
	const MIGRATE_ONE_TIME_SCHEDULED_CHARGES            = 205;
	const MIGRATE_PARCEL_ALERT                          = 206;
	const MIGRATE_SPECIALS                              = 207;
	const UPDATE_APPLICATIONS_LEAD_SOURCE               = 208;
	const UPDATE_APPLICATIONS_LEASING_AGENT             = 209;
	const MIGRATE_SMS_ENROLLMENT_AND_EMAIL_BLOCKS       = 210;
	const MIGRATE_VANITY_NUMBERS                        = 211;
	const TRANSFER_RESIDENT_UTILITY_SETUP               = 212;
	const MIGRATE_MISSING_CALL_FILES                    = 213;
	const MIGRATE_ALARM_OR_ENTRY_CODES                  = 214;
	const MIGRATE_LEASE_TERMS                           = 215;
	const MIGRATE_APPLICATION_DATES                     = 216;
	const MIGRATE_AMENITY_MEDIAS                        = 262;

	// Scrub Data Cleanup
	const TEST_CLIENT_DATA_SCRUB = 167;

	// This is used in migration workbook Client report storage.
	const SECURE_DOCUMENT							= 164;

	const VALIDATE_MILITARY_DEMOGRAPHICS			= 165;
	const MILITARY_WAITLIST							= 238;
	const MILITARY_FLOOR_PLANS						= 261;
	const MILITARY_RECURRING_CHARGES				= 306;
	const MILITARY_RESIDENTS_AND_APPLICANTS			= 307;
	const MILITARY_LEASE_INTERVALS					= 308;
	const MILITARY_RENTAL_RATES						= 334;

	// Migration Mapping
	const PROPERTY_TYPE_MAPPING		= 168;

	// This is used in migration workbook Entrata report storage.
	const ENTRATA_OPERATIONS						= 169;
	const ENTRATA_ACCOUNTING						= 170;
	const SPECIAL_ASSOCIATIONS						= 171;

	const RESIDENT_PORTAL_INFO						= 173;
	const VANITY_NUMBERS							= 174;
	const MOVE_OUT_REASONS                          = 176;

	// This is used in migration workbook Entrata report storage.
	const ENTRATA_IMPORT_FILES						= 177;
	const ENTRATA_OTHER_FILES						= 178;

	const GO_LIVE_WITH_ENTRATA_CORE					= 218;
	const REVERT_ENTRATA_CORE						= 220;

	// This is used in migration workbook Client report storage.
	const VALIDATION_REPORT							= 219;

	const COMMERCIAL_GUARANTOR					    = 221;

	const ASSOCIATE_LEASE_TERM_STRUCTURE			= 225;
	const OVERRIDE_PROPERTY_PREFERENCES				= 226;
	const OVERRIDE_WEBSITE_PREFERENCES				= 227;
	const IMPORT_CURRENT_RESIDENTS					= 228;
	const IMPORT_FUTURE_RESIDENTS					= 229;
	const IMPORT_NOTICE_RESIDENTS					= 230;
	const PULL_ACTIVE_APPLICANT						= 231;
	const PULL_ARCHIVE_APPLICANT					= 232;
	const PULL_CANCELED_APPLICANT					= 233;
	const PULL_PAST_RESIDENTS						= 234;
	const PULL_CANCELED_RESIDENTS					= 235;
	const PULL_FULL_LEASE_LEDGER					= 236;
	const PULL_WORK_ORDER							= 237;

	const AR_MIGRATION_MODE							= 240;

	const COMMERCIAL_MAINTENANCE_REQUESTS			= 241;
	const COMMERCIAL_TRANSACTIONS					= 242;
	const COMMERCIAL_RESIDENT_INSURANCE				= 243;
	const COMMERCIAL_GL_GROUPS						= 245;
	const COMMERCIAL_GL_ACCOUNTS					= 246;
	const COMMERCIAL_CHARGE_CODES					= 247;
	const COMMERCIAL_BANK_ACCOUNTS					= 248;
	const COMMERCIAL_VENDORS						= 249;
	const COMMERCIAL_CATALOG_ITEMS					= 250;
	const COMMERCIAL_FIXED_ASSETS					= 251;
	const COMMERCIAL_INVENTORY						= 252;
	const COMMERCIAL_ADD_ONS						= 253;
	const COMMERCIAL_RECURRING_CHARGES				= 254;
	const COMMERCIAL_VENDOR_1099_BALANCE			= 255;
	const COMMERCIAL_COMPANY_USERS					= 256;
	const COMMERCIAL_MAINTENANCE_REQUEST_SETTINGS	= 257;
	const COMMERCIAL_OWNERS							= 258;
	const COMMERCIAL_ACTIVITY_LOGS					= 259;
	const VIEW_UPDATE_LEASES_TO_CANCEL				= 260;
	const COMMERCIAL_OTHER_CLAUSE					= 281;
	const COMMERCIAL_SUITES							= 323;
	const COMMERCIAL_BUILDINGS						= 324;

	// Workbook lite project
	const SAMPLE_REPORT									= 261;
	const SAMPLE_OPERATIONS_REPORT						= 262;
	const SAMPLE_ACCOUNTING_REPORT						= 263;
	const PRE_MIGRATION_REPORT							= 264;
	const PRE_MIGRATION_OPERATIONS_REPORT				= 265;
	const PRE_MIGRATION_ACCOUNTING_REPORT				= 266;
	const POST_MIGRATION_REPORT							= 267;
	const POST_MIGRATION_OPERATIONS_REPORT				= 268;
	const POST_MIGRATION_ACCOUNTING_REPORT				= 269;
	const SAMPLE_OPERATIONS_APPROVALS_REPORT			= 270;
	const SAMPLE_ACCOUNTING_APPROVALS_REPORT			= 271;
	const PRE_MIGRATION_OPERATIONS_APPROVALS_REPORT		= 272;
	const PRE_MIGRATION_ACCOUNTING_APPROVALS_REPORT		= 273;
	const POST_MIGRATION_OPERATIONS_APPROVALS_REPORT	= 274;
	const POST_MIGRATION_ACCOUNTING_APPROVALS_REPORT	= 275;
	const POST_MIGRATION_VALIDATION_REPORT				= 276;
	const PRE_MIGRATION_MAPPING_REPORT					= 296;
	const POST_MIGRATION_OPERATIONS_VARIANCES_REPORT	= 297;
	const POST_MIGRATION_GUIDE_REPORT					= 298;
	const PRE_MIGRATION_MISSED_REPORTS					= 300;
	const MIGRATION_MISSED_REPORTS						= 301;
	const POST_MIGRATION_MISSED_REPORTS                 = 302;

	const WRITEOFF_LEASES					= 277;
	const PULL_APPLICANT_WRITEOFF			= 278;
	const PULL_PAST_WRITEOFF				= 279;
	const PULL_CANCELLED_WRITEOFF			= 280;
	const CANCELLATION_REASONS				= 282;
	const PULL_GUEST_CARD_COMPLETED			= 318;
	const PULL_GUEST_CARD_ARCHIVE			= 319;
	const IMPORT_APPLICANT_LEASES_ACTIVITY_LOGS		= 325;
	const IMPORT_CANCELLED_LEASES_ACTIVITY_LOGS		= 326;
	const IMPORT_FUTURE_LEASES_ACTIVITY_LOGS		= 327;
	const IMPORT_CURRENT_LEASES_ACTIVITY_LOGS		= 328;
	const IMPORT_NOTICE_LEASES_ACTIVITY_LOGS		= 329;
	const IMPORT_PAST_LEASES_ACTIVITY_LOGS			= 330;
	const IMPORT_GUEST_CARD_ACTIVITY_LOGS			= 331;
	const MAINTENANCE_PROBLEMS_LOCATIONS			= 332;

	// constant for email send in migration workbooks module
	const SAMPLE_MIGRATION_REPORT_TAB						= 283;
	const CREATE_WORKBOOK_EMAIL								= 284;
	const APPROVED_SAMPLE_REPORTS							= 285;
	const REJECTED_SAMPLE_REPORTS							= 286;
	const APPROVED_PRE_MIGRATION_REPORTS					= 287;
	const REJECTED_PRE_MIGRATION_REPORTS					= 288;
	const APPROVED_POST_MIGRATION_REPORTS					= 289;
	const REJECTED_POST_MIGRATION_REPORTS					= 290;
	const SAMPLE_UPLOAD_FILE								= 291;
	const PRE_MIGRATION_UPLOAD_FILE							= 292;
	const POST_MIGRATION_UPLOAD_FILE						= 293;
	const SAMPLE_REPORT_APPROVALS_TAB						= 294;
	const PRE_MIGRATION_REPORT_APPROVALS_TAB				= 295;
	const PRE_MIGRATION_MISSED_REPORT_TAB					= 302;
	const POST_MIGRATION_MISSED_REPORT_TAB					= 303;
	const SAMPLE_REPORT_UPLOAD_AFTER_FINISHED_UPLOAD		= 304;
	const PRE_MIGRATION_REPORT_UPLOAD_AFTER_FINISHED_UPLOAD	= 305;
	const CREATE_WORKBOOK_EMAIL_CLIENT						= 308;
	const SELF_MIGRATION_BANK_ACCOUNT_SETUP					= 309;
	const SELF_MIGRATION_ACCOUNT_ASSOCIATION				= 310;
	const SELF_MIGRATION_SETUP								= 311;
	const SELF_MIGRATION_UPLOAD_DATA						= 312;
	const SELF_MIGRATION_VERIFY_DATA						= 313;
	const SELF_MIGRATION_SUMMARY							= 314;
	const SIM_SIMPLIFIED_SETUP								= 315;
	const SIM_LEASE_UP										= 316;
	const SIM_FULL_MIGRATION								= 317;
	const PULL_ACTIVE_GUEST_CARDS							= 318;
	const PULL_ARCHIVE_GUEST_CARDS 							= 319;
	const SELF_MIGRATION_FINALIZATION						= 321;
	const COMPLETE_WORKBOOK_EMAIL							= 322;

	public static $c_arrintExtendedAutomationConstants = [
		self::MAINTENANCE_PROBLEMS,
		self::MAINTENANCE_STATUSES,
		self::MAINTENANCE_PRIORITIES
	];

	public static $c_arrmixOperationTypeByImportDataType = [
		self::GL_ACCOUNTS					=> 'GlAccounts',
		self::CHARGE_CODES					=> 'ArCodes',
		self::PROPERTIES					=> 'PropertiesInfo',
		self::LEAD_SOURCES					=> 'LeadSources',
		self::BANK_ACCOUNTS					=> 'BankAccounts',
		self::RESIDENTS_AND_APPLICANTS		=> 'ResidentsData',
		self::VENDORS						=> 'Vendors',
		self::AMENITIES						=> 'Amenities',
		self::ADD_ONS						=> 'AddOns',
		self::RENTAL_RATES					=> 'RentalRates',
		self::MAINTENANCE_REQUESTS			=> 'MaintenanceRequests',
		self::PURCHASE_ORDERS				=> 'Purchase Orders',
		self::INVOICES						=> 'Invoices',
		self::TRANSACTIONS					=> 'Transactions',
		self::FLOOR_PLANS					=> 'FloorPlans',
		self::COMPANY_USERS					=> 'CompanyUsers',
		self::DEMOGRAPHICS					=> 'Demographics',
		self::OCCUPANTS						=> 'Occupants',
		self::RECURRING_CHARGES				=> 'RecurringCharges',
		self::LEASE_INTERVALS				=> 'LeaseIntervals',
		self::VENDOR_1099_BALANCE			=> 'Vendor1099Balance',
		self::GL_GROUPS						=> 'GlGroups',
		self::RESIDENT_INSURANCE			=> 'ResidentInsurance',
		self::MAINTENANCE_REQUEST_SETTINGS	=> 'MaintenanceRequestSettings',
		self::AP_CODES						=> 'CatalogItems',
		self::FIXED_ASSETS					=> 'FixedAssets',
		self::INVENTORY                     => 'Inventory',
		self::EMPLOYERS						=> 'Employers',
		self::PETS							=> 'Pets',
		self::VEHICLES						=> 'Vehicles',
		self::ADDRESSES						=> 'Addresses',
		self::EMERGENCY_CONTACTS			=> 'EmergencyContacts',
		self::SPECIALS						=> 'Specials',
		self::TENANTS_AND_SUITES			=> 'TenantsAndSuites',
		self::RENT_STEPS					=> 'RentSteps',
		self::COMMERCIAL_ADDRESSES			=> 'CommercialAddresses',
		self::COMMERCIAL_RENEWAL_OPTIONS	=> 'CommercialRenewalOptions',
		self::COMMERCIAL_CONTACTS			=> 'CommercialContacts',
		self::BASELINE_FILE					=> 'BaselineFile',
		self::ANNUAL_INCOME					=> 'AnnualIncome',
		self::CUSTOMER_ASSETS_INCOME		=> 'CustomerAssetsIncome',
		self::HUD_WAIT_LIST					=> 'HUDWaitList',
		self::TAX_CREDIT_INFORMATION        => 'TaxCreditInformation',
		self::MILITARY_DEMOGRAPHICS         => 'MilitaryDemographics',
		self::COMPETITORS					=> 'Competitors',
		self::CUSTOMER_DEMOGRAPHICS         => 'CustomerDemographics',
		self::COMPETITOR_UNIT_TYPES			=> 'CompetitorUnitTypes',
		self::AP_PAYMENTS					=> 'ApPayments',
		self::OWNERS						=> 'Owners',
		self::GUEST_CARDS					=> 'GuestCards',
		self::ACTIVITY_LOGS					=> 'ActivityLogs',
		self::LOCALIZATION_DATA				=> 'LocalizationData',
		self::BUILDING_SETTINGS				=> 'BuildingSettings',
		self::AFFORDABLE_RESIDENTS_AND_APPLICANTS => 'AffordableResidents',
		self::VALIDATE_MILITARY_DEMOGRAPHICS	=> 'ValidateMilitaryDemographics',
		self::MILITARY_WAITLIST				=> 'MilitaryWaitlist',
		self::MILITARY_FLOOR_PLANS			=> 'MilitaryFloorPlans',
		self::MILITARY_RECURRING_CHARGES	=> 'MilitaryRecurringCharges',
		self::MILITARY_RESIDENTS_AND_APPLICANTS			=> 'MilitaryResidents&Applicants',
		self::MILITARY_LEASE_INTERVALS					=> 'MilitaryLeaseIntervals',
		self::MILITARY_RENTAL_RATES						=> 'MilitaryRentalRates',
		self::SPECIAL_ASSOCIATIONS			=> 'SpecialAssociations',
		self::COMMERCIAL_GUARANTOR			=> 'CommercialGuarantors',
		self::COMMERCIAL_MAINTENANCE_REQUESTS => 'CommercialMaintenanceRequest',
		self::COMMERCIAL_TRANSACTIONS		=> 'Transactions',
		self::COMMERCIAL_RESIDENT_INSURANCE    => 'CommercialResidentInsurance',
		self::COMMERCIAL_GL_GROUPS						=> 'CommercialGlGroups',
		self::COMMERCIAL_GL_ACCOUNTS					=> 'CommercialGlAccounts',
		self::COMMERCIAL_CHARGE_CODES					=> 'CommercialArCodes',
		self::COMMERCIAL_BANK_ACCOUNTS					=> 'CommercialBankAccounts',
		self::COMMERCIAL_VENDORS						=> 'CommercialVendors',
		self::COMMERCIAL_CATALOG_ITEMS					=> 'CommercialCatalogItems',
		self::COMMERCIAL_FIXED_ASSETS					=> 'CommercialFixedAssets',
		self::COMMERCIAL_ADD_ONS						=> 'CommercialAddOns',
		self::COMMERCIAL_RECURRING_CHARGES				=> 'CommercialRecurringCharges',
		self::COMMERCIAL_VENDOR_1099_BALANCE			=> 'CommercialVendor1099Balance',
		self::COMMERCIAL_COMPANY_USERS					=> 'CommercialCompanyUsers',
		self::COMMERCIAL_MAINTENANCE_REQUEST_SETTINGS	=> 'CommercialMaintenanceRequestSettings',
		self::COMMERCIAL_OWNERS							=> 'CommercialOwners',
		self::COMMERCIAL_ACTIVITY_LOGS					=> 'CommercialActivityLogs',
		self::COMMERCIAL_OTHER_CLAUSE					=> 'CommercialOtherClauses',
		self::RENTAL_RATES								=> 'CompamyRates',
		self::CONVENTIONAL_WAITLIST						=> 'ConventionalWaitlist',
		self::COMMERCIAL_BUILDINGS						=> 'CommercialBuildings',
		self::COMMERCIAL_SUITES							=> 'CommercialSuites',
		self::LEASE_MODIFICATIONS							=> 'LeaseModifications'
	];

	public static $c_arrmixImportControllerByImportDataType = [
		self::GL_ACCOUNTS					=> 'CImportDataGlAccountsController',
		self::CHARGE_CODES					=> 'CImportDataArCodesController',
		self::LEAD_SOURCES					=> 'CImportDataLeadSourcesController',
		self::BANK_ACCOUNTS					=> 'CImportDataBankAccountsController',
		self::RESIDENTS_AND_APPLICANTS		=> 'CImportDataResidentsDataController',
		self::VENDORS						=> 'CImportDataVendorsController',
		self::AMENITIES						=> 'CImportDataAmenitiesController',
		self::ADD_ONS						=> 'CImportDataAddOnsController',
		self::RENTAL_RATES					=> 'CImportDataRatesController',
		self::MAINTENANCE_REQUESTS			=> 'CImportDataMaintenanceRequestsController',
		self::PURCHASE_ORDERS				=> 'CImportDataPurchaseOrdersController',
		self::INVOICES						=> 'CImportDataInvoicesController',
		self::TRANSACTIONS					=> 'CImportDataTransactionsController',
		self::FLOOR_PLANS					=> 'CImportDataFloorPlansController',
		self::COMPANY_USERS					=> 'CImportDataCompanyUsersController',
		self::DEMOGRAPHICS					=> 'CImportDataDemographicsController',
		self::OCCUPANTS						=> 'CImportDataOccupantsController',
		self::RECURRING_CHARGES				=> 'CImportDataRecurringChargesController',
		self::LEASE_INTERVALS				=> 'CImportDataLeaseIntervalsController',
		self::VENDOR_1099_BALANCE			=> 'CImportDataVendors1099BalanceController',
		self::GL_GROUPS						=> 'CImportDataGlGroupsController',
		self::RESIDENT_INSURANCE			=> 'CImportDataResidentInsuranceController',
		self::MAINTENANCE_REQUEST_SETTINGS	=> 'CImportDataMaintenanceRequestSettingsController',
		self::AP_CODES						=> 'CImportDataCatalogItemsController',
		self::FIXED_ASSETS					=> 'CImportDataFixedAssetsController',
		self::INVENTORY					    => 'CImportDataInventoryController',
		self::EMPLOYERS						=> 'CImportDataEmployersController',
		self::PETS							=> 'CImportDataPetsController',
		self::VEHICLES						=> 'CImportDataVehiclesController',
		self::ADDRESSES						=> 'CImportDataAddressesController',
		self::EMERGENCY_CONTACTS			=> 'CImportDataEmergencyContactsController',
		self::SPECIALS						=> 'CImportDataSpecialsController',
		self::TENANTS_AND_SUITES			=> 'CImportDataTenantsAndSuitesController',
		self::COMMERCIAL_ADDRESSES			=> 'CImportDataCommercialAddressesController',
		self::COMMERCIAL_RENEWAL_OPTIONS	=> 'CImportDataCommercialRenewalOptionsController',
		self::RENT_STEPS					=> 'CImportDataRentStepsController',
		self::COMMERCIAL_CONTACTS			=> 'CImportDataCommercialContactsController',
		self::BASELINE_FILE					=> 'CImportDataBaselineFile',
		self::ANNUAL_INCOME					=> 'CImportDataAnnualIncome',
		self::CUSTOMER_ASSETS_INCOME		=> 'CImportDataCustomerAssetsIncome',
		self::TAX_CREDIT_INFORMATION		=> 'CImportDataTaxCreditInformation',
		self::HUD_WAIT_LIST					=> 'CImportDataHUDWaitListController',
		self::MILITARY_DEMOGRAPHICS 		=> 'CImportDataMilitaryDemographicsController',
		self::COMPETITORS					=> 'CImportDataCompetitorsController',
		self::CUSTOMER_DEMOGRAPHICS 		=> 'CImportDataCustomerDemographicsController',
		self::COMPETITOR_UNIT_TYPES			=> 'CImportDataCompetitorUnitTypesController',
		self::OWNERS						=> 'CImportDataOwnersController',
		self::GUEST_CARDS					=> 'CImportDataGuestCardsController',
		self::ACTIVITY_LOGS					=> 'CImportDataActivityLogsController',
		self::BUILDING_SETTINGS				=> 'CImportDataBuildingSettingsController',
		self::AFFORDABLE_RESIDENTS_AND_APPLICANTS			=> 'CImportDataResidentsDataController',
		self::VALIDATE_MILITARY_DEMOGRAPHICS => 'CImportDataValidateMilitaryDemographicsController',
		self::MILITARY_WAITLIST				=> 'CImportDataMilitaryWaitlistController',
		self::MILITARY_FLOOR_PLANS			=> 'CImportDataFloorPlansController',
		self::MILITARY_RECURRING_CHARGES	=> 'CMilitaryRecurringChargesController',
		self::MILITARY_RESIDENTS_AND_APPLICANTS			=> 'CImportDataResidentsTransportController',
		self::MILITARY_LEASE_INTERVALS					=> 'CImportDataLeaseIntervalsController',
		self::SPECIAL_ASSOCIATIONS			=> 'CImportDataSpecialAssociationsController',
		self::COMMERCIAL_GUARANTOR			=> 'CImportDataOccupantsController',
		self::LOCALIZATION_DATA				=> 'CImportDataLocalizationDataController',
		self::CLD_DATA						=> 'CImportDataCldDataController',
		self::COMMERCIAL_MAINTENANCE_REQUESTS	=> 'CImportDataMaintenanceRequestsController',
		self::COMMERCIAL_TRANSACTIONS		    => 'CImportDataTransactionsController',
		self::COMMERCIAL_RESIDENT_INSURANCE    => 'CImportDataResidentInsuranceController',
		self::COMMERCIAL_GL_GROUPS						=> 'CImportDataGlGroupsController',
		self::COMMERCIAL_GL_ACCOUNTS					=> 'CImportDataGlAccountsController',
		self::COMMERCIAL_CHARGE_CODES					=> 'CImportDataArCodesController',
		self::COMMERCIAL_BANK_ACCOUNTS					=> 'CImportDataBankAccountsController',
		self::COMMERCIAL_VENDORS						=> 'CImportDataVendorsController',
		self::COMMERCIAL_CATALOG_ITEMS					=> 'CImportDataCatalogItemsController',
		self::COMMERCIAL_FIXED_ASSETS					=> 'CImportDataFixedAssetsController',
		self::COMMERCIAL_INVENTORY						=> 'CImportDataInventoryController',
		self::COMMERCIAL_ADD_ONS						=> 'CImportDataAddOnsController',
		self::COMMERCIAL_RECURRING_CHARGES				=> 'CImportDataRecurringChargesController',
		self::COMMERCIAL_VENDOR_1099_BALANCE			=> 'CImportDataVendors1099BalanceController',
		self::COMMERCIAL_COMPANY_USERS					=> 'CImportDataCompanyUsersController',
		self::COMMERCIAL_MAINTENANCE_REQUEST_SETTINGS	=> 'CImportDataMaintenanceRequestSettingsController',
		self::COMMERCIAL_OWNERS							=> 'CImportDataOwnersController',
		self::COMMERCIAL_ACTIVITY_LOGS					=> 'CImportDataActivityLogsController'
	];

	public static $c_arrmixImportFirstColumnByImportDataType = [
		self::GL_ACCOUNTS					=> 'account number',
		self::CHARGE_CODES					=> 'priority num',
		self::BANK_ACCOUNTS					=> 'account name',
		self::VENDORS						=> 'business name(dba)',
		self::PURCHASE_ORDERS				=> 'vendor code',
		self::INVOICES						=> 'invoice type',
		self::VENDOR_1099_BALANCE			=> 'vendor code',
		self::GL_GROUPS						=> 'account type',
		self::AP_CODES						=> 'property id',
		self::FIXED_ASSETS					=> 'catalog_item_name',
		self::INVENTORY					    => 'catalog_item_name',
		self::CATALOG_ITEMS					=> 'item type',
		self::AP_PAYMENTS					=> 'Ap Payment type',
		self::OWNERS						=> 'owner name',
		self::LOCALIZATION_DATA				=> 'category',
		self::COMMERCIAL_GL_GROUPS			=> 'gl account type',
		self::COMMERCIAL_GL_ACCOUNTS		=> 'account number',
		self::COMMERCIAL_CHARGE_CODES		=> 'priority num',
		self::COMMERCIAL_BANK_ACCOUNTS		=> 'account name',
		self::COMMERCIAL_VENDORS			=> 'business name(dba)',
		self::COMMERCIAL_VENDOR_1099_BALANCE	=> 'vendor code',
		self::COMMERCIAL_FIXED_ASSETS		=> 'catalog_item_name',
		self::COMMERCIAL_INVENTORY			=> 'catalog_item_name',
		self::COMMERCIAL_CATALOG_ITEMS		=> 'item type',
		self::COMMERCIAL_OWNERS				=> 'owner name'
	];

	public static $c_arrmixTransferEntitiesByImportDataType = [
		CImportDataType::PROPERTIES							=> [ 'name' => 'Import Property Info', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::PROPERTY_UNITS						=> [ 'name' => 'Import Property Units', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::ADD_ONS							=> [ 'name' => 'Import Add Ons', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::LEASE_TERMS						=> [ 'name' => 'Import Lease Terms', 'options' => 0, 'isMigrationSetp' => 1, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::CHARGE_CODES						=> [ 'name' => 'Import Charge Codes', 'options' => 0, 'isMigrationSetp' => 1, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::GL_ACCOUNTS						=> [ 'name' => 'Import GL Accounts', 'options' => 1, 'isMigrationSetp' => 1, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::VENDORS							=> [ 'name' => 'Import Vendors', 'options' => 0, 'isMigrationSetp' => 1, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::COMPANY_USERS						=> [ 'name' => 'Import Company Users', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::PROPERTY_PREFERENCES				=> [ 'name' => 'Import Property Preferences', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::PROPERTY_MEDIA						=> [ 'name' => 'Import Property Media', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::RESIDENTS							=> [ 'name' => 'Import Residents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::APPLICANTS							=> [ 'name' => 'Import Applicants', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::PAST_RESIDENTS						=> [ 'name' => 'Import Past Residents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::CANCELED_RESIDENTS					=> [ 'name' => 'Import Canceled Residents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::WRITEOFF_LEASES					=> [ 'name' => 'Import Write-Off Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		// CImportDataType::MIGRATE_LEASE_DOCUMENT			=> [ 'name' => 'Import Lease Documents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => NULL ],
		CImportDataType::TRANSACTIONS						=> [ 'name' => 'Import Resident Ledger', 'options' => 1, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::RECURRING_CHARGES					=> [ 'name' => 'Import Recurring Charges', 'options' => 1, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::MIGRATE_ONE_TIME_SCHEDULED_CHARGES	=> [ 'name' => 'Import One Time Scheduled Charges', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::LEADS								=> [ 'name' => 'Import Leads', 'options' => 1, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::REVIEWS							=> [ 'name' => 'Import Reviews', 'options' => 0, 'isMigrationSetp' => 1, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::MAINTENANCE_REQUESTS				=> [ 'name' => 'Import Maintenance', 'options' => 1, 'isMigrationSetp' => 1, 'displayPosition' => 2, 'parent_id' => NULL ],
		CImportDataType::SPECIALS							=> [ 'name' => 'Import Specials', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::LEASE_AND_GL_BALANCES				=> [ 'name' => 'Import Lease And Gl Balances', 'options' => 1, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::VENDOR_INVOICES					=> [ 'name' => 'Import Vendor Ledger', 'options' => 1, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::OWNER_INVOICES						=> [ 'name' => 'Import Owner Ledger', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::WEBSITE_DATA						=> [ 'name' => 'Import Website Data', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::WEBSITE_PREFERENCES				=> [ 'name' => 'Import Website Preferences', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::PROPERTY_LEVEL_PRICING				=> [ 'name' => 'Import Property Level Pricing', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::PROPERTY_TAXES						=> [ 'name' => 'Import Property Taxes', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => NULL ],
		CImportDataType::SCHEDULED_PAYMENTS					=> [ 'name' => 'Import scheduled Payments', 'options' => 1, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		CImportDataType::INSPECTIONS						=> [ 'name' => 'Import Inspections', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 2, 'parent_id' => NULL ],
		CImportDataType::RESIDENT_PORTAL_INFO				=> [ 'name' => 'Import Resident Portal Info', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => NULL ],
		CImportDataType::VANITY_NUMBERS						=> [ 'name' => 'Migrate Vanity Numbers', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => NULL ],
		CImportDataType::ASSOCIATE_LEASE_TERM_STRUCTURE		=> [ 'name' => 'Associate Lease Term Structure', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => self::LEASE_TERMS ],
		CImportDataType::OVERRIDE_PROPERTY_PREFERENCES		=> [ 'name' => 'Override Property Preferences', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => self::PROPERTY_PREFERENCES ],
		CImportDataType::OVERRIDE_WEBSITE_PREFERENCES		=> [ 'name' => 'Override Website Preferences', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 0, 'parent_id' => self::WEBSITE_PREFERENCES ],
		CImportDataType::IMPORT_CURRENT_RESIDENTS			=> [ 'name' => 'Import Current Residents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::RESIDENTS ],
		CImportDataType::IMPORT_FUTURE_RESIDENTS			=> [ 'name' => 'Import Future Residents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::RESIDENTS ],
		CImportDataType::IMPORT_NOTICE_RESIDENTS			=> [ 'name' => 'Import Notice Residents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::RESIDENTS ],
		CImportDataType::PULL_ACTIVE_APPLICANT				=> [ 'name' => 'Import Active Applicant', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::APPLICANTS ],
		CImportDataType::PULL_ARCHIVE_APPLICANT				=> [ 'name' => 'Import Archive Applicant', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::APPLICANTS ],
		CImportDataType::PULL_CANCELED_APPLICANT			=> [ 'name' => 'Import Canceled Applicant', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::APPLICANTS ],
		CImportDataType::PULL_PAST_RESIDENTS				=> [ 'name' => 'Import Past Residents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::PAST_RESIDENTS ],
		CImportDataType::PULL_CANCELED_RESIDENTS			=> [ 'name' => 'Import Canceled Residents', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::CANCELED_RESIDENTS ],
		CImportDataType::PULL_APPLICANT_WRITEOFF			=> [ 'name' => 'Import Applicant Write-Off Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::WRITEOFF_LEASES ],
		CImportDataType::PULL_PAST_WRITEOFF					=> [ 'name' => 'Import Past Write-Off Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::WRITEOFF_LEASES ],
		CImportDataType::PULL_CANCELLED_WRITEOFF			=> [ 'name' => 'Import Cancelled Write-Off Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::WRITEOFF_LEASES ],
		CImportDataType::PULL_FULL_LEASE_LEDGER				=> [ 'name' => 'Import Full Lease Ledger', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::TRANSACTIONS ],
		CImportDataType::PULL_WORK_ORDER					=> [ 'name' => 'Import Work Order', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 2, 'parent_id' => NULL ],
		CImportDataType::MAINTENANCE_LOCATIONS				=> [ 'name' => 'Import Locations', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 2, 'parent_id' => self::MAINTENANCE_REQUESTS ],
		CImportDataType::MAINTENANCE_STATUSES				=> [ 'name' => 'Import Status', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 2, 'parent_id' => self::MAINTENANCE_REQUESTS ],
		CImportDataType::MAINTENANCE_PRIORITIES				=> [ 'name' => 'Import Priorities', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 2, 'parent_id' => self::MAINTENANCE_REQUESTS ],
		CImportDataType::MAINTENANCE_PROBLEMS				=> [ 'name' => 'Import Problems', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 2, 'parent_id' => self::MAINTENANCE_REQUESTS ],
		CImportDataType::MAINTENANCE_TEMPLATES				=> [ 'name' => 'Import Templates', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 2, 'parent_id' => self::MAINTENANCE_REQUESTS ],
		CImportDataType::MAINTENANCE_ATTACHMENTS			=> [ 'name' => 'Import Work order Attachments', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 2, 'parent_id' => NULL ],
		CImportDataType::MIGRATE_ACTIVITY_LOGS				=> [ 'name' => 'Migrate Activity Logs', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => NULL ],
		CImportDataType::IMPORT_APPLICANT_LEASES_ACTIVITY_LOGS	=> [ 'name' => 'Activity Logs For Applicant Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => self::MIGRATE_ACTIVITY_LOGS ],
		CImportDataType::IMPORT_CANCELLED_LEASES_ACTIVITY_LOGS	=> [ 'name' => 'Activity Logs For Cancelled Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => self::MIGRATE_ACTIVITY_LOGS ],
		CImportDataType::IMPORT_FUTURE_LEASES_ACTIVITY_LOGS		=> [ 'name' => 'Activity Logs For Future Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => self::MIGRATE_ACTIVITY_LOGS ],
		CImportDataType::IMPORT_CURRENT_LEASES_ACTIVITY_LOGS	=> [ 'name' => 'Activity Logs For Current Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => self::MIGRATE_ACTIVITY_LOGS ],
		CImportDataType::IMPORT_NOTICE_LEASES_ACTIVITY_LOGS		=> [ 'name' => 'Activity Logs For Notice Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => self::MIGRATE_ACTIVITY_LOGS ],
		CImportDataType::IMPORT_PAST_LEASES_ACTIVITY_LOGS		=> [ 'name' => 'Activity Logs For Past Leases', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => self::MIGRATE_ACTIVITY_LOGS ],
		CImportDataType::IMPORT_GUEST_CARD_ACTIVITY_LOGS		=> [ 'name' => 'Activity Logs For Guest Cards', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => self::MIGRATE_ACTIVITY_LOGS ],
		CImportDataType::MIGRATE_RENEWAL_OFFERS				=> [ 'name' => 'Migrate Renewal Offers', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 3, 'parent_id' => NULL ],
		CImportDataType::PULL_GUEST_CARD_COMPLETED			=> [ 'name' => 'Import Guest Card Completed', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::LEADS ],
		CImportDataType::PULL_GUEST_CARD_ARCHIVE			=> [ 'name' => 'Import Guest Card Archived', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => self::LEADS ],
		self::VENDORS                                       => [ 'name' => 'Import Data Vendors', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ],
		self::INVENTORY                                     => [ 'name' => 'Import Data Inventory', 'options' => 0, 'isMigrationSetp' => 0, 'displayPosition' => 1, 'parent_id' => NULL ]
	];

	public static $c_arrmixDataCleanupOptionsByImportDataType = [
		self::VIEW_ADD_UNDEPOSITED_INITIAL_IMPORTED_PAYMENTS                        => 'Add Undeposited Initial Imported Payments',
		self::CLEAN_GL_ACCOUNTS                                                     => 'Clean GL Entries',
		self::VIEW_CLEANUP_FINANCIAL_DATA                                           => 'Cleanup Financial Data',
		self::DELETE_ALL_REVERSAL_ENTRIES                                           => 'Delete All Reversal Transaction Entries',
		self::VIEW_DELETE_TRANSACTIONS                                              => 'Delete Transactions',
		self::VIEW_SET_POSTED_DATES_ON_RECURRING_CHARGES                            => 'Set Posted Dates On Recurring Charges',
		self::VIEW_SET_RATE_ID_ON_SCHEDULED_CHARGES                                 => 'Set Rate id on Scheduled Charges',
		self::VIEW_SOFT_DELETE_HISTORIC_INVOICES                                    => 'Soft delete Historic Invoices',
		self::VIEW_UPDATE_CHARGE_CODE                                               => 'Update Charge Code',
		self::ASSIGN_PROPERTY_FLOORPLAN_TO_APPLICATION                             	=> 'Assign Property Floorplan To Application',
		self::ASSIGN_PROPERTY_FLOORPLANS_TO_ALL_APPLICATIONS                       	=> 'Assign Property Floorplans To All Applications',
		self::ASSIGN_PROPERTY_UNIT_TO_LEASE                                        	=> 'Assign Property Unit To Lease',
		self::CANCEL_GUESTCARD                                                     	=> 'Cancel Guestcard',
		self::DELETE_ADD_ONS_WITH_RATES                                            	=> 'Delete Add Ons With Rates',
		self::DELETE_EMPLOYMENT_INFORMATIONS                                       	=> 'Delete Employment Informations',
		self::DELETE_OCCUPANT                                                      	=> 'Delete Occupant',
		self::DELETE_ORPHAN_GL_ACCOUNTS                                            	=> 'Delete Orphan Gl Accounts',
		self::DELETE_PET_INFORMATIONS                                              	=> 'Delete Pet Informations',
		self::DELETE_PROPERTY_BUILDING                                             	=> 'Delete Property Building',
		self::DELETE_PROPERTY_UNITS                                                	=> 'Delete Property Units',
		self::DELETE_SCHEDULED_CHARGES                                             	=> 'Delete Scheduled Charges',
		self::DELETE_UNIT_SPACE_LEVEL_MARKET_RENT_DEPOSIT_AND_BUDGETED_RENT        	=> 'Delete Unit Space Level Market Rent, Deposit and Budgeted Rent',
		self::DELETE_UNREQUIRED_OR_ORPHAN_GL_ENTRIES_OR_MARK_ALL_AS_INITIAL_IMPORT 	=> 'Delete Unrequired or Orphan GL Entries, Or Mark all as Initial Import',
		self::DELETE_CANCELLED_ARCHIVED_LEADS_APPLICATIONS                         	=> 'Delete cancelled/archived leads/applications',
		self::INTEGRATE_PROPERTY                                                   	=> 'Integrate Property',
		self::MERGER_CUSTOMER_RECORDS                                              	=> 'Merger Customer Records',
		self::RE_NAME_UNIT                                                         	=> 'Re-Name Unit',
		self::REMOVE_STREET_LINE2_ADDRESS                                          	=> 'Remove Street Line2 Address',
		self::REPAIR_LOGS                                                          	=> 'Repair Logs',
		self::SOFT_DELETE_WORK_ORDERS                                              	=> 'Soft Delete Work Orders',
		self::SPLIT_SCHEDULED_CHARGES                                              	=> 'Split Scheduled Charges',
		self::TAG_UTILITY_RESPONSIBILITIES_AT_LEASE_LEVEL                          	=> 'Tag Utility responsibilities at lease level',
		self::UPDATE_APPLICATION_AND_LEASE_SIGN_DATES                              	=> 'Update Application and Lease Sign Dates',
		self::UPDATE_CUSTOMER_ID_CORPORATE_LEAD                                    	=> 'Update Customer Id Corporate Lead',
		self::UPDATE_CUSTOMER_TYPE                                                 	=> 'Update Customer Type',
		self::UPDATE_GUEST_CARD_TO_ARCHIVED                                        	=> 'Update Guest Card To Archived',
		self::UPDATE_OCCUPANT_STATUS                                               	=> 'Update Occupant Status',
		self::UPDATE_PAYMENT_BLOCK_ASSOCIATION                                     	=> 'Update Payment block association',
		self::UPDATE_REMOTE_PRIMARY_KEY                                            	=> 'Update Remote Primary Key',
		self::UPDATE_UNIT_ADDRESS_AS_PER_USPS                                      	=> 'Update Unit Address as per USPS',
		self::VIEW_DELETE_LEASE_WITH_TRANSACTIONS                                   => 'Delete Lease',
		self::VIEW_DELETE_LEASE_INTERVAL                                            => 'Delete Lease Interval',
		self::VIEW_PERFORM_FMO                                                      => 'Perform FMO',
		self::VIEW_SET_LATE_FEE_FORMULA                                             => 'Set Late Fee Formula',
		self::VIEW_SET_TRANSFER_ON_LEASES                                           => 'Set Transfer On Leases',
		self::VIEW_UPDATE_APPLICATIONS                                              => 'Update Applications',
		self::VIEW_UPDATE_COLLECTION_STATUS                                         => 'Update Collection Status',
		self::VIEW_UPDATE_LEASE_INTERVALS                                           => 'Update Lease Intervals',
		self::VIEW_UPDATE_LEASE_TERMS                                               => 'Update Lease Terms',
		self::VIEW_UPDATE_LEASE_DETAILS                                             => 'Update Leases & Applications',
		self::VIEW_UPDATE_MOVE_IN_DATE                                              => 'Update move in date',
		self::VIEW_DELETE_NON_ACTIVE_LEASE_INTERVALS                                => 'Delete Non-Active Lease Intervals',
		self::MIGRATE_LEASE_DOCUMENT                                                => 'Migrate Lease Documents',
		self::MIGRATE_RESIDENT_STORED_BILLING_INFO                                  => 'Migrate Resident Stored Billing Info',
		self::MIGRATE_INSTALLMENT_PLANS                                             => 'Migrate Installment Plans',
		self::MIGRATE_RESIDENT_PORTAL_LOGINS                                        => 'Migrate Resident Portal Logins',
		self::APPLICANT_CONTACTS                                                    => 'Applicant Contacts',
		self::CUSTOMER_CONTACTS                                                     => 'Customer Contacts',
		self::MIGRATE_APPLICATION_INTEREST                                          => 'Migrate Application Interest',
		self::MIGRATE_CALLS                                                         => 'Migrate Calls',
		self::MIGRATE_FLOORPLAN_UNIT_TYPE_ASSOCIATION                               => 'Migrate Floorplan Unit Type Association',
		self::MIGRATE_INSURANCE_POLICIES                                            => 'Migrate Insurance Policies',
		self::MIGRATE_RECURRING_PAYMENTS                                            => 'Migrate Recurring Payments',
		self::MIGRATE_RENEWAL_OFFERS                                                => 'Migrate Renewal Offers',
		self::MIGRATE_RENEWAL_OFFERS_FOR_LEADS                                      => 'Migrate Renewal Offers For Leads',
		self::MIGRATE_RESIDENT_VERIFY_SCREENING_RECORDS                             => 'Migrate Resident Verify Screening Records',
		self::MIGRATE_SCREENING_SETUP_WITH_RECORDS                                  => 'Migrate Screening Setup With Records',
		self::MIGRATE_PROPERTY_LEVEL_PRICING                                        => 'Migrate Property Level Pricing',
		self::MIGRATE_PROPERTY_TAXES                                                => 'Migrate Property Taxes',
		self::PROPERTY_REVIEWS                                                      => 'Property Reviews',
		self::SCRUB_RESIDENT_OR_APPLICANT_EMAILS                                    => 'Scrub Resident Or Applicant Emails',
		self::MIGRATE_ACTIVITY_LOGS                                                 => 'Migrate Activity Logs',
		self::MIGRATE_AMENITIES                                                     => 'Migrate Amenities',
		self::MIGRATE_APPLICATIONS_DEMOGRAPHICS_INFO                                => 'Migrate Applications Demographics Information',
		self::MIGRATE_APPLICANTS_OCCUPATION_INFORMATION                             => 'Migrate Applications Occupation Information',
		self::MIGRATE_APPLICANT_GENDER_INFORMATION                                  => 'Migrate Applicant Gender Information',
		self::MIGRATE_APPLICANT_MARITAL_STATUS_INFORMATION                          => 'Migrate Applicant Marital Status Information',
		self::MIGRATE_APPLICANT_BIRTH_DATE_INFORMATION                              => 'Migrate Applicant Birth Date Information',
		self::MIGRATE_CUSTOMER_PETS                                                 => 'Migrate Customer Pets',
		self::MIGRATE_ONE_TIME_SCHEDULED_CHARGES                                    => 'Migrate One Time Scheduled Charges',
		self::MIGRATE_PARCEL_ALERT                                                  => 'Migrate Parcel Alert',
		self::MIGRATE_SPECIALS                                                      => 'Migrate Specials',
		self::UPDATE_APPLICATIONS_LEAD_SOURCE                                       => 'Update Application Lead Source',
		self::UPDATE_APPLICATIONS_LEASING_AGENT                                     => 'Update Application Leasing Agent',
		self::MIGRATE_SMS_ENROLLMENT_AND_EMAIL_BLOCKS                               => 'Migrate SMS Enrollment And Email Blocks',
		self::MIGRATE_VANITY_NUMBERS                                                => 'Migrate Vanity Numbers',
		self::TRANSFER_RESIDENT_UTILITY_SETUP                                       => 'Transfer Resident Utility Setup',
		self::MIGRATE_MISSING_CALL_FILES                                            => 'Migrate Missing Call Files',
		self::MIGRATE_ALARM_OR_ENTRY_CODES                                          => 'Migrate Alarm Or Entry Codes',
		self::MIGRATE_LEASE_TERMS                                                   => 'Migrate Lease Terms',
		self::MIGRATE_APPLICATION_DATES                                             => 'Migrate Application Dates',
		self::UPDATE_SPACE_OPTION                                                   => 'Update Space option',
		self::REPLACE_LEASING_AGENT                                                 => 'Replace Leasing Agent',
		self::TEST_CLIENT_DATA_SCRUB 												=> 'Test Client Data Scrub',
		self::MIGRATE_TAX_IDENTIFICATION_AND_DOB									=> 'Migrate Tax Identification And DOB',
		self::GO_LIVE_WITH_ENTRATA_CORE												=> 'Go Live With Entrata Core',
		self::REVERT_ENTRATA_CORE													=> 'Revert Entrata Core',
		self::VIEW_UPDATE_LEASES_TO_CANCEL											=> 'Update Leases To Cancel',
	];

	public static $c_arrmixPartialTransferEntities = [
		CImportDataType::DEMOGRAPHICS				=> [ 'name' => 'Import Demographic Info', 'options' => 0, 'isMigrationSetp' => 0 ]
	];

	public static $c_arrintExcludeDataType = [
		CImportDataType::MAINTENANCE_LOCATIONS,
		CImportDataType::MAINTENANCE_STATUSES,
		CImportDataType::MAINTENANCE_PRIORITIES,
		CImportDataType::MAINTENANCE_PROBLEMS,
		CImportDataType::CHARGE_CODES,
		CImportDataType::GL_ACCOUNTS,
		CImportDataType::SPECIALS,
		CImportDataType::OWNER_INVOICES,
		CImportDataType::PROPERTY_TAXES,
		CImportDataType::SCHEDULED_PAYMENTS,
		CImportDataType::PROPERTY_LEVEL_PRICING,
		CImportDataType::LEASE_AND_GL_BALANCES,
		CImportDataType::VENDOR_INVOICES,
		CImportDataType::COMPANY_USERS,
		CImportDataType::VENDORS,
		CImportDataType::INVENTORY
	];

	// Used this as Property types in migration workbook
	public static $c_arrmixMigrationImportDataType = [
		CImportDataType::CONVENTIONAL 			=> array( 'id' => CImportDataType::CONVENTIONAL, 'name' => 'Conventional' ),
		CImportDataType::STUDENT 				=> array( 'id' => CImportDataType::STUDENT, 'name' => 'Student' ),
		CImportDataType::COMMERCIAL 			=> array( 'id' => CImportDataType::COMMERCIAL, 'name' => 'Commercial' ),
		CImportDataType::MILITARY_DEMOGRAPHICS 	=> array( 'id' => CImportDataType::MILITARY_DEMOGRAPHICS, 'name' => 'Military' ),
		CImportDataType::TAX_CREDIT_INFORMATION	=> array( 'id' => CImportDataType::TAX_CREDIT_INFORMATION, 'name' => 'Tax Credit' ),
		CImportDataType::HUD_WAIT_LIST			=> array( 'id' => CImportDataType::HUD_WAIT_LIST, 'name' => 'HUD Baseline' )
	];

	public static $c_arrstrReportSections = [
		'cror'		=> CImportDataType::OPERATIONS,
		'crar'		=> CImportDataType::ACCOUNTING,
		'crsd'		=> CImportDataType::SECURE_DOCUMENT,
		'crvr'		=> CImportDataType::VALIDATION_REPORT,
		'erovr'		=> CImportDataType::ENTRATA_OPERATIONS,
		'eravr'		=> CImportDataType::ENTRATA_ACCOUNTING,
		'erif'		=> CImportDataType::ENTRATA_IMPORT_FILES,
		'erof'		=> CImportDataType::ENTRATA_OTHER_FILES,
		'srp'		=> CImportDataType::SAMPLE_REPORT,
		'sorp'		=> CImportDataType::SAMPLE_OPERATIONS_REPORT,
		'sarp'		=> CImportDataType::SAMPLE_ACCOUNTING_REPORT,
		'pmrp'		=> CImportDataType::PRE_MIGRATION_REPORT,
		'pmorp'		=> CImportDataType::PRE_MIGRATION_OPERATIONS_REPORT,
		'pmarp'		=> CImportDataType::PRE_MIGRATION_ACCOUNTING_REPORT,
		'pmmrp'		=> CImportDataType::PRE_MIGRATION_MAPPING_REPORT,
		'pomrp'		=> CImportDataType::POST_MIGRATION_REPORT,
		'pomorp'	=> CImportDataType::POST_MIGRATION_OPERATIONS_REPORT,
		'pomarp'	=> CImportDataType::POST_MIGRATION_ACCOUNTING_REPORT,
		'pomovrp'	=> CImportDataType::POST_MIGRATION_OPERATIONS_VARIANCES_REPORT,
		'pomgrp'	=> CImportDataType::POST_MIGRATION_GUIDE_REPORT
	];

	public static $c_arrstrWorkbookReportSectionName = [
		self::SAMPLE_REPORT									=> 'Sample Report',
		self::SAMPLE_OPERATIONS_REPORT						=> 'Sample Operation Report',
		self::SAMPLE_ACCOUNTING_REPORT						=> 'Sample Accounting Report',
		self::PRE_MIGRATION_REPORT							=> 'Pre-migration Report',
		self::PRE_MIGRATION_OPERATIONS_REPORT				=> 'Pre-migration Operation Report',
		self::PRE_MIGRATION_ACCOUNTING_REPORT				=> 'Pre-migration Accounting Report',
		self::PRE_MIGRATION_MAPPING_REPORT					=> 'Pre-migration Mapping Report',
		self::POST_MIGRATION_REPORT							=> 'Post-migration Report',
		self::POST_MIGRATION_OPERATIONS_REPORT				=> 'Post-migration Operation Report',
		self::POST_MIGRATION_ACCOUNTING_REPORT				=> 'Post-migration Accounting Report',
		self::POST_MIGRATION_OPERATIONS_VARIANCES_REPORT	=> 'Post-migration Operations Variances Report',
		self::POST_MIGRATION_GUIDE_REPORT					=> 'Post-migration Guide',
		self::SAMPLE_OPERATIONS_APPROVALS_REPORT			=> 'Sample Operation Approvals Document',
		self::SAMPLE_ACCOUNTING_APPROVALS_REPORT			=> 'Sample Accounting Approvals Document',
		self::PRE_MIGRATION_OPERATIONS_APPROVALS_REPORT		=> 'Pre-migration Operation Approvals Document',
		self::PRE_MIGRATION_ACCOUNTING_APPROVALS_REPORT		=> 'Pre-migration Accounting Approvals Document',
		self::POST_MIGRATION_OPERATIONS_APPROVALS_REPORT	=> 'Post-migration Operation Approvals Document',
		self::POST_MIGRATION_ACCOUNTING_APPROVALS_REPORT	=> 'Post-migration Accounting Approvals Document',
		self::POST_MIGRATION_VALIDATION_REPORT				=> 'Post-migration Validation Report'
	];

	public static $c_arrstrWorkbookEmailSendConstants = [
		self::SAMPLE_MIGRATION_REPORT_TAB			=> 'Sample Migration Report Tab',
		self::CREATE_WORKBOOK_EMAIL					=> 'Create Workbook Email',
		self::APPROVED_SAMPLE_REPORTS				=> 'Approved Sample Reports',
		self::REJECTED_SAMPLE_REPORTS				=> 'Rejected Sample Reports',
		self::APPROVED_PRE_MIGRATION_REPORTS		=> 'Approved Pre Migration Reports',
		self::REJECTED_PRE_MIGRATION_REPORTS		=> 'Rejected Pre Migration Reports',
		self::APPROVED_POST_MIGRATION_REPORTS		=> 'Approved Post Migration Reports',
		self::REJECTED_POST_MIGRATION_REPORTS		=> 'Rejected Post Migration Reports',
		self::SAMPLE_UPLOAD_FILE					=> 'Sample Upload File',
		self::PRE_MIGRATION_UPLOAD_FILE				=> 'Pre Migration Upload File',
		self::POST_MIGRATION_UPLOAD_FILE			=> 'Post Migration Upload File',
		self::SAMPLE_REPORT_APPROVALS_TAB			=> 'Sample Report Approvals Tab',
		self::PRE_MIGRATION_REPORT_APPROVALS_TAB	=> 'Pre Migration Report Approvals Tab',
		self::PRE_MIGRATION_MISSED_REPORTS			=> 'Pre-migration Missed Reports',
		self::MIGRATION_MISSED_REPORTS				=> 'Migration Missed Reports',
		self::POST_MIGRATION_MISSED_REPORTS			=> 'Post-migration Missed Reports'
	];

	public static $c_arrmixPropertyTransferCategories = [
		0	=> [ 'id' => 0, 'name' => 'property_info' ],
		1	=> [ 'id' => 1, 'name' => 'customer_info' ],
		2	=> [ 'id' => 2, 'name' => 'maintenance_info' ],
		3	=> [ 'id' => 3, 'name' => 'additional_info' ]
	];

	public static $c_arrstrSelfMigrationSubStatusNames = [
		self::SELF_MIGRATION_BANK_ACCOUNT_SETUP		=> 'Bank Account Setup',
		self::SELF_MIGRATION_ACCOUNT_ASSOCIATION	=> 'Bank Account Association',
		self::SELF_MIGRATION_SETUP					=> 'Migration Setup',
		self::SELF_MIGRATION_UPLOAD_DATA			=> 'Data Upload',
		self::SELF_MIGRATION_VERIFY_DATA			=> 'Data Verification',
		self::SELF_MIGRATION_SUMMARY				=> 'Migration Summary',
		self::SELF_MIGRATION_FINALIZATION			=> 'Finalization'
	];

	public static $c_arrintAwsCsvArchitecture = [
		self::AMENITIES,
		self::FLOOR_PLANS,
		self::MILITARY_FLOOR_PLANS,
		self::MILITARY_RESIDENTS_AND_APPLICANTS,
		self::RESIDENTS_AND_APPLICANTS,
		self::RECURRING_CHARGES,
		self::TRANSACTIONS,
		self::MAINTENANCE_REQUESTS,
		self::ADDRESSES,
		self::PETS,
		self::GUEST_CARDS,
		self::LEAD_SOURCES,
		self::VEHICLES,
		self::CUSTOMER_DEMOGRAPHICS,
		self::OCCUPANTS,
		self::COMMERCIAL_GUARANTOR,
		self::RENT_STEPS,
		self::CHARGE_CODES,
		self::EMERGENCY_CONTACTS,
		self::RESIDENT_INSURANCE,
		self::COMMERCIAL_RESIDENT_INSURANCE,
		self::SPECIALS,
		self::COMMERCIAL_OTHER_CLAUSE,
		self::LEASE_INTERVALS,
		self::LOCALIZATION_DATA,
		self::COMPANY_USERS,
		self::COMMERCIAL_COMPANY_USERS,
		self::ADD_ONS,
		self::COMMERCIAL_ADD_ONS,
		self::MILITARY_DEMOGRAPHICS,
		self::RENTAL_RATES,
		self::MILITARY_WAITLIST,
		self::MILITARY_RECURRING_CHARGES,
		self::MILITARY_LEASE_INTERVALS,
		self::MILITARY_RENTAL_RATES,
		self::COMPETITORS,
		self::TENANTS_AND_SUITES,
		self::ACTIVITY_LOGS,
		self::COMMERCIAL_ACTIVITY_LOGS,
		self::TAX_CREDIT_INFORMATION,
		self::EMPLOYERS,
		self::SPECIAL_ASSOCIATIONS,
		self::COMMERCIAL_CONTACTS,
		self::CUSTOMER_ASSETS_INCOME,
		self::BUILDING_SETTINGS,
		self::ANNUAL_INCOME,
		self::COMPETITOR_UNIT_TYPES,
		self::HUD_WAIT_LIST,
		self::BASELINE_FILE,
		self::AFFORDABLE_RESIDENTS_AND_APPLICANTS,
		self::COMMERCIAL_RENEWAL_OPTIONS,
		self::GL_GROUPS,
		self::GL_ACCOUNTS,
		self::COMMERCIAL_ADDRESSES,
		self::COMMERCIAL_TRANSACTIONS,
		self::CONVENTIONAL_WAITLIST,
		self::COMMERCIAL_RECURRING_CHARGES,
		self::COMMERCIAL_MAINTENANCE_REQUESTS,
		self::COMMERCIAL_GL_GROUPS,
		self::COMMERCIAL_GL_ACCOUNTS,
		self::COMMERCIAL_CHARGE_CODES,
		self::COMMERCIAL_CATALOG_ITEMS,
		self::CATALOG_ITEMS,
		self::COMMERCIAL_BUILDINGS,
		self::FIXED_ASSETS,
		self::COMMERCIAL_SUITES,
		self::VENDORS,
		self::INVENTORY,
		self::LEASE_MODIFICATIONS
	];

	public static $c_arrstrImportDataToolTipKeys = [
		CImportDataType::GL_GROUPS                               => 'IMPORT_DATA_TYPE_GL_GROUPS',
		CImportDataType::GL_ACCOUNTS                             => 'IMPORT_DATA_TYPE_GL_ACCOUNTS',
		CImportDataType::CHARGE_CODES                            => 'IMPORT_DATA_TYPE_CHARGE_CODES',
		CImportDataType::FLOOR_PLANS                             => 'IMPORT_DATA_TYPE_FLOOR_PLANS',
		CImportDataType::LEAD_SOURCES                            => 'IMPORT_DATA_TYPE_LEAD_SOURCES',
		CImportDataType::BANK_ACCOUNTS                           => 'IMPORT_DATA_TYPE_BANK_ACCOUNTS',
		CImportDataType::RESIDENTS_AND_APPLICANTS                => 'IMPORT_DATA_TYPE_RESIDENTS_AND_APPLICANTS',
		CImportDataType::OCCUPANTS                               => 'IMPORT_DATA_TYPE_OCCUPANTS',
		CImportDataType::VENDORS                                 => 'IMPORT_DATA_TYPE_VENDORS',
		CImportDataType::AMENITIES                               => 'IMPORT_DATA_TYPE_AMENITIES',
		CImportDataType::ADD_ONS                                 => 'IMPORT_DATA_TYPE_ADD_ONS',
		CImportDataType::RENTAL_RATES                            => 'IMPORT_DATA_TYPE_RENTAL_RATES',
		CImportDataType::MAINTENANCE_REQUESTS                    => 'IMPORT_DATA_TYPE_MAINTENANCE_REQUESTS',
		CImportDataType::PURCHASE_ORDERS                         => 'IMPORT_DATA_TYPE_PURCHASE_ORDERS',
		CImportDataType::INVOICES                                => 'IMPORT_DATA_TYPE_INVOICES',
		CImportDataType::TRANSACTIONS                            => 'IMPORT_DATA_TYPE_TRANSACTIONS',
		CImportDataType::RECURRING_CHARGES                       => 'IMPORT_DATA_TYPE_RECURRING_CHARGES',
		CImportDataType::LEASE_INTERVALS                         => 'IMPORT_DATA_TYPE_LEASE_INTERVALS',
		CImportDataType::VENDOR_1099_BALANCE                     => 'IMPORT_DATA_TYPE_VENDOR_1099_BALANCE',
		CImportDataType::COMPANY_USERS                           => 'IMPORT_DATA_TYPE_COMPANY_USERS',
		CImportDataType::DEMOGRAPHICS                            => 'IMPORT_DATA_TYPE_DEMOGRAPHICS',
		CImportDataType::RESIDENT_INSURANCE                      => 'IMPORT_DATA_TYPE_RESIDENT_INSURANCE',
		CImportDataType::MAINTENANCE_REQUEST_SETTINGS            => 'IMPORT_DATA_TYPE_MAINTENANCE_REQUEST_SETTINGS',
		CImportDataType::AP_CODES                                => 'IMPORT_DATA_TYPE_AP_CODES',
		CImportDataType::EMPLOYERS                               => 'IMPORT_DATA_TYPE_EMPLOYERS',
		CImportDataType::VEHICLES                                => 'IMPORT_DATA_TYPE_VEHICLES',
		CImportDataType::ADDRESSES                               => 'IMPORT_DATA_TYPE_ADDRESSES',
		CImportDataType::COMMERCIAL_ADDRESSES                    => 'IMPORT_DATA_TYPE_COMMERCIAL_ADDRESSES',
		CImportDataType::COMMERCIAL_RENEWAL_OPTIONS              => 'IMPORT_DATA_TYPE_COMMERCIAL_RENEWAL_OPTIONS',
		CImportDataType::BASELINE_FILE                           => 'IMPORT_DATA_TYPE_BASELINE_FILE',
		CImportDataType::ANNUAL_INCOME                           => 'IMPORT_DATA_TYPE_ANNUAL_INCOME',
		CImportDataType::CUSTOMER_ASSETS_INCOME                  => 'IMPORT_DATA_TYPE_CUSTOMER_ASSETS_INCOME',
		CImportDataType::TAX_CREDIT_INFORMATION                  => 'IMPORT_DATA_TYPE_TAX_CREDIT_INFORMATION',
		CImportDataType::AFFORDABLE_TRANSACTIONS                 => 'IMPORT_DATA_TYPE_AFFORDABLE_TRANSACTIONS',
		CImportDataType::AFFORDABLE_RECURRING_CHARGES            => 'IMPORT_DATA_TYPE_AFFORDABLE_RECURRING_CHARGES',
		CImportDataType::MILITARY_DEMOGRAPHICS                   => 'IMPORT_DATA_TYPE_MILITARY_DEMOGRAPHICS',
		CImportDataType::MILITARY_WAITLIST                       => 'IMPORT_DATA_TYPE_MILITARY_WAITLIST',
		CImportDataType::MILITARY_FLOOR_PLANS                    => 'IMPORT_DATA_TYPE_MILITARY_FLOOR_PLANS',
		CImportDataType::MILITARY_RESIDENTS_AND_APPLICANTS       => 'IMPORT_DATA_TYPE_MILITARY_RESIDENTS_AND_APPLICANTS',
		CImportDataType::COMPETITORS                             => 'IMPORT_DATA_TYPE_COMPETITORS',
		CImportDataType::COMPETITOR_UNIT_TYPES                   => 'IMPORT_DATA_TYPE_COMPETITORS_UNIT_TYPE',
		CImportDataType::OWNERS                                  => 'IMPORT_DATA_TYPE_OWNERS',
		CImportDataType::CUSTOMER_DEMOGRAPHICS                   => 'IMPORT_DATA_TYPE_CUSTOMER_DEMOGRAPHICS',
		CImportDataType::GUEST_CARDS                             => 'IMPORT_DATA_TYPE_GUEST_CARDS',
		CImportDataType::ACTIVITY_LOGS                           => 'IMPORT_DATA_TYPE_ACTIVITY_LOGS',
		CImportDataType::LEASE_TERMS                             => 'IMPORT_DATA_TYPE_LEASE_TERMS',
		CImportDataType::SPECIAL_ASSOCIATIONS                    => 'IMPORT_DATA_TYPE_SPECIAL_ASSOCIATIONS',
		CImportDataType::CATALOG_ITEMS                           => 'IMPORT_DATA_TYPE_CATALOG_ITEMS',
		CImportDataType::INVENTORY                               => 'IMPORT_DATA_TYPE_INVENTORY',
		CImportDataType::FIXED_ASSETS                            => 'IMPORT_DATA_TYPE_FIXED_ASSETS',
		CImportDataType::SPECIALS                                => 'IMPORT_DATA_TYPE_SPECIALS',
		CImportDataType::PETS                                    => 'IMPORT_DATA_TYPE_PETS',
		CImportDataType::EMERGENCY_CONTACTS                      => 'IMPORT_DATA_TYPE_EMERGENCY_CONTACTS',
		CImportDataType::LOCALIZATION_DATA                       => 'IMPORT_DATA_TYPE_LOCALIZATION_DATA',
		CImportDataType::CLD_DATA                                => 'IMPORT_DATA_TYPE_CLD_DATA',
		CImportDataType::CONVENTIONAL_WAITLIST                   => 'IMPORT_DATA_TYPE_CONVENTIONAL_WAITLIST',
		CImportDataType::TENANTS_AND_SUITES                      => 'IMPORT_DATA_TYPE_TENANTS_AND_SUITES',
		CImportDataType::COMMERCIAL_CONTACTS                     => 'IMPORT_DATA_TYPE_COMMERCIAL_CONTACTS',
		CImportDataType::RENT_STEPS                              => 'IMPORT_DATA_TYPE_RENT_STEPS',
		CImportDataType::COMMERCIAL_GUARANTOR                    => 'IMPORT_DATA_TYPE_COMMERCIAL_GUARANTOR',
		CImportDataType::COMMERCIAL_MAINTENANCE_REQUESTS         => 'IMPORT_DATA_TYPE_COMMERCIAL_MAINTENANCE_REQUESTS',
		CImportDataType::COMMERCIAL_TRANSACTIONS                 => 'IMPORT_DATA_TYPE_COMMERCIAL_TRANSACTIONS',
		CImportDataType::COMMERCIAL_RESIDENT_INSURANCE           => 'IMPORT_DATA_TYPE_COMMERCIAL_RESIDENT_INSURANCE',
		CImportDataType::HUD_WAIT_LIST                           => 'IMPORT_DATA_TYPE_HUD_WAIT_LIST',
		CImportDataType::BUILDING_SETTINGS                       => 'IMPORT_DATA_TYPE_BUILDING_SETTINGS',
		CImportDataType::AFFORDABLE_RESIDENTS_AND_APPLICANTS     => 'IMPORT_DATA_TYPE_AFFORDABLE_RESIDENTS_AND_APPLICANTS',
		CImportDataType::VALIDATE_MILITARY_DEMOGRAPHICS          => 'IMPORT_DATA_TYPE_VALIDATE_MILITARY_DEMOGRAPHICS',
		CImportDataType::MILITARY_WAITLIST                       => 'IMPORT_DATA_TYPE_VALIDATE_MILITARY_WAITLIST',
		CImportDataType::COMMERCIAL_GL_GROUPS                    => 'IMPORT_DATA_TYPE_COMMERCIAL_GL_GROUPS',
		CImportDataType::COMMERCIAL_GL_ACCOUNTS                  => 'IMPORT_DATA_TYPE_COMMERCIAL_GL_ACCOUNTS',
		CImportDataType::COMMERCIAL_CHARGE_CODES                 => 'IMPORT_DATA_TYPE_COMMERCIAL_CHARGE_CODES',
		CImportDataType::COMMERCIAL_BANK_ACCOUNTS                => 'IMPORT_DATA_TYPE_COMMERCIAL_BANK_ACCOUNTS',
		CImportDataType::COMMERCIAL_VENDORS							=> 'IMPORT_DATA_TYPE_COMMERCIAL_VENDORS',
		CImportDataType::COMMERCIAL_CATALOG_ITEMS					=> 'IMPORT_DATA_TYPE_COMMERCIAL_CATALOG_ITEMS',
		CImportDataType::COMMERCIAL_FIXED_ASSETS					=> 'IMPORT_DATA_TYPE_COMMERCIAL_FIXED_ASSETS',
		CImportDataType::COMMERCIAL_INVENTORY						=> 'IMPORT_DATA_TYPE_COMMERCIAL_INVENTORY',
		CImportDataType::COMMERCIAL_ADD_ONS							=> 'IMPORT_DATA_TYPE_COMMERCIAL_ADD_ONS',
		CImportDataType::COMMERCIAL_RECURRING_CHARGES				=> 'IMPORT_DATA_TYPE_COMMERCIAL_RECURRING_CHARGES',
		CImportDataType::COMMERCIAL_VENDOR_1099_BALANCE				=> 'IMPORT_DATA_TYPE_COMMERCIAL_VENDOR_1099_BALANCE',
		CImportDataType::COMMERCIAL_COMPANY_USERS					=> 'IMPORT_DATA_TYPE_COMMERCIAL_COMPANY_USERS',
		CImportDataType::COMMERCIAL_MAINTENANCE_REQUEST_SETTINGS	=> 'IMPORT_DATA_TYPE_COMMERCIAL_MAINTENANCE_REQUEST_SETTINGS',
		CImportDataType::COMMERCIAL_OWNERS							=> 'IMPORT_DATA_TYPE_COMMERCIAL_OWNERSS',
		CImportDataType::COMMERCIAL_ACTIVITY_LOGS					=> 'IMPORT_DATA_TYPE_COMMERCIAL_ACTIVITY_LOGS',
		CImportDataType::CONVENTIONAL_WAITLIST                       => 'IMPORT_DATA_TYPE_CONVENTIONAL_WAITLIST',
	];

	public static $c_arrintImportDataTypesForScheduledCharges = [
		CImportDataType::AFFORDABLE_RESIDENTS_AND_APPLICANTS,
		CImportDataType::RESIDENTS_AND_APPLICANTS,
		CImportDataType::RECURRING_CHARGES,
		CImportDataType::ADD_ONS,
		CImportDataType::TENANTS_AND_SUITES,
		CImportDataType::RENT_STEPS,
		CImportDataType::BASELINE_FILE,
		CImportDataType::SPECIAL_ASSOCIATIONS,
		CImportDataType::COMMERCIAL_ADD_ONS,
		CImportDataType::COMMERCIAL_RECURRING_CHARGES,
		CImportDataType::MILITARY_RESIDENTS_AND_APPLICANTS
	];

	public static $c_arrintImportDataTypesForAutoFillCsvs = [
		'recurring_charges'			=> CImportDataType::RECURRING_CHARGES,
		'transactions'				=> CImportDataType::TRANSACTIONS,
		'occupants'					=> CImportDataType::OCCUPANTS,
		'vehicles'					=> CImportDataType::VEHICLES,
		'maintenance_requests'		=> CImportDataType::MAINTENANCE_REQUESTS,
		'amenities'					=> CImportDataType::AMENITIES,
		'renewal_lease_intervals'	=> CImportDataType::LEASE_INTERVALS,
		'resident_insurance'		=> CImportDataType::RESIDENT_INSURANCE,
		'specials'					=> CImportDataType::SPECIALS,
		'pets_data'					=> CImportDataType::PETS,
		'employers'					=> CImportDataType::EMPLOYERS,
		'addresses'					=> CImportDataType::ADDRESSES,
		'emergency_contacts'		=> CImportDataType::EMERGENCY_CONTACTS,
		'activity_logs'				=> CImportDataType::ACTIVITY_LOGS
	];

	public static $c_arrmixSIMLeaseUpOptions = [
		'required' => [ CImportDataType::FLOOR_PLANS 			=> [ 'id' => CImportDataType::FLOOR_PLANS, 'name' => 'FloorPlans' ] ],
		'optional' => [
			CImportDataType::ADD_ONS 				=> [ 'id' => CImportDataType::ADD_ONS, 'name' => 'Add Ons' ],
			CImportDataType::AMENITIES 				=> [ 'id' => CImportDataType::AMENITIES, 'name' => 'Amenities' ],
			CImportDataType::GUEST_CARDS 			=> [ 'id' => CImportDataType::GUEST_CARDS, 'name' => 'Guest Cards' ],
			CImportDataType::TRANSACTIONS 			=> [ 'id' => CImportDataType::TRANSACTIONS, 'name' => 'Transactions' ],
			CImportDataType::PETS 					=> [ 'id' => CImportDataType::PETS, 'name' => 'Pets' ],
			CImportDataType::EMPLOYERS 				=> [ 'id' => CImportDataType::EMPLOYERS, 'name' => 'Employers' ],
			CImportDataType::VEHICLES 				=> [ 'id' => CImportDataType::VEHICLES, 'name' => 'Vehicles' ],
			CImportDataType::EMERGENCY_CONTACTS 	=> [ 'id' => CImportDataType::EMERGENCY_CONTACTS, 'name' => 'Emergency Contacts' ]
		]
	];

	public static $c_arrmixSIMFullMigrationSimple = [
		'required' => [ CImportDataType::RESIDENTS_AND_APPLICANTS 	=> [ 'id' => CImportDataType::RESIDENTS_AND_APPLICANTS, 'name' => 'Residents and Applicants' ] ],
		'optional' => [
			CImportDataType::ADD_ONS 				=> [ 'id' => CImportDataType::ADD_ONS, 'name' => 'Add Ons' ],
			CImportDataType::AMENITIES 				=> [ 'id' => CImportDataType::AMENITIES, 'name' => 'Amenities' ],
			CImportDataType::GUEST_CARDS 			=> [ 'id' => CImportDataType::GUEST_CARDS, 'name' => 'Guest Cards' ],
			CImportDataType::TRANSACTIONS 			=> [ 'id' => CImportDataType::TRANSACTIONS, 'name' => 'Transactions' ],
			CImportDataType::PETS 					=> [ 'id' => CImportDataType::PETS, 'name' => 'Pets' ],
			CImportDataType::EMPLOYERS 				=> [ 'id' => CImportDataType::EMPLOYERS, 'name' => 'Employers' ],
			CImportDataType::VEHICLES 				=> [ 'id' => CImportDataType::VEHICLES, 'name' => 'Vehicles' ],
			CImportDataType::EMERGENCY_CONTACTS 	=> [ 'id' => CImportDataType::EMERGENCY_CONTACTS, 'name' => 'Emergency Contacts' ],
			CImportDataType::OCCUPANTS 				=> [ 'id' => CImportDataType::OCCUPANTS, 'name' => 'Occupants' ],
			CImportDataType::ADDRESSES 				=> [ 'id' => CImportDataType::ADDRESSES, 'name' => 'Addresses' ],
			CImportDataType::MAINTENANCE_REQUESTS 	=> [ 'id' => CImportDataType::MAINTENANCE_REQUESTS, 'name' => 'Maintenance Requests' ],
			CImportDataType::RECURRING_CHARGES 		=> [ 'id' => CImportDataType::RECURRING_CHARGES, 'name' => 'Recurring Charges' ],
			CImportDataType::RESIDENT_INSURANCE 	=> [ 'id' => CImportDataType::RESIDENT_INSURANCE, 'name' => 'Resident Insurance' ]
		]
	];

	public static $c_arrmixSIMFullMigrationAdvance = [
		'required' => [
			CImportDataType::FLOOR_PLANS 				=> [ 'id' => CImportDataType::FLOOR_PLANS, 'name' => 'FloorPlans' ],
			CImportDataType::RESIDENTS_AND_APPLICANTS 	=> [ 'id' => CImportDataType::RESIDENTS_AND_APPLICANTS, 'name' => 'Residents and Applicants' ],
			CImportDataType::RECURRING_CHARGES 			=> [ 'id' => CImportDataType::RECURRING_CHARGES, 'name' => 'Recurring Charges' ],
			CImportDataType::TRANSACTIONS 				=> [ 'id' => CImportDataType::TRANSACTIONS, 'name' => 'Transactions' ],
		],
		'optional' => [
			CImportDataType::ADD_ONS 				=> [ 'id' => CImportDataType::ADD_ONS, 'name' => 'Add Ons' ],
			CImportDataType::ADDRESSES 				=> [ 'id' => CImportDataType::ADDRESSES, 'name' => 'Addresses' ],
			CImportDataType::AMENITIES 				=> [ 'id' => CImportDataType::AMENITIES, 'name' => 'Amenities' ],
			CImportDataType::LEASE_INTERVALS 		=> [ 'id' => CImportDataType::LEASE_INTERVALS, 'name' => 'Lease Intervals' ],
			CImportDataType::MAINTENANCE_REQUESTS 	=> [ 'id' => CImportDataType::MAINTENANCE_REQUESTS, 'name' => 'Maintenance Requests' ],
			CImportDataType::OCCUPANTS 				=> [ 'id' => CImportDataType::OCCUPANTS, 'name' => 'Occupants' ],
			CImportDataType::GUEST_CARDS 			=> [ 'id' => CImportDataType::GUEST_CARDS, 'name' => 'Guest Cards' ],
			CImportDataType::PETS 					=> [ 'id' => CImportDataType::PETS, 'name' => 'Pets' ],
			CImportDataType::VEHICLES 				=> [ 'id' => CImportDataType::VEHICLES, 'name' => 'Vehicles' ],
			CImportDataType::RESIDENT_INSURANCE 	=> [ 'id' => CImportDataType::RESIDENT_INSURANCE, 'name' => 'Resident Insurance' ],
			CImportDataType::EMERGENCY_CONTACTS 	=> [ 'id' => CImportDataType::EMERGENCY_CONTACTS, 'name' => 'Emergency Contacts' ]
		]
	];

	public static $c_arrmixCommercialSIMLeaseUpOptions = [
		'required' => [
			CImportDataType::COMMERCIAL_BUILDINGS		=> [ 'id' => CImportDataType::COMMERCIAL_BUILDINGS, 'name' => 'Commercial Buildings' ],
			CImportDataType::COMMERCIAL_SUITES			=> [ 'id' => CImportDataType::COMMERCIAL_SUITES, 'name' => 'Commercial Suites' ]
		],
		'optional' => [
			CImportDataType::COMMERCIAL_ADD_ONS 					=> [ 'id' => CImportDataType::COMMERCIAL_ADD_ONS, 'name' => 'Commercial Add Ons' ],
			CImportDataType::COMMERCIAL_RECURRING_CHARGES 			=> [ 'id' => CImportDataType::COMMERCIAL_RECURRING_CHARGES, 'name' => 'Commercial Recurring Charges' ],
			CImportDataType::COMMERCIAL_ACTIVITY_LOGS 				=> [ 'id' => CImportDataType::COMMERCIAL_ACTIVITY_LOGS, 'name' => 'Commercial Activity Logs' ],
			CImportDataType::COMMERCIAL_ADDRESSES 					=> [ 'id' => CImportDataType::COMMERCIAL_ADDRESSES, 'name' => 'Commercial Addresses' ],
			CImportDataType::COMMERCIAL_CONTACTS 					=> [ 'id' => CImportDataType::COMMERCIAL_CONTACTS, 'name' => 'Commercial Contacts' ],
			CImportDataType::COMMERCIAL_RENEWAL_OPTIONS 			=> [ 'id' => CImportDataType::COMMERCIAL_RENEWAL_OPTIONS, 'name' => 'Commercial Renewal Options' ],
			CImportDataType::RENT_STEPS 							=> [ 'id' => CImportDataType::RENT_STEPS, 'name' => 'Rent Steps' ],
			CImportDataType::COMMERCIAL_GUARANTOR 					=> [ 'id' => CImportDataType::COMMERCIAL_GUARANTOR, 'name' => 'Commercial Guarantor' ],
			CImportDataType::COMMERCIAL_MAINTENANCE_REQUESTS		=> [ 'id' => CImportDataType::COMMERCIAL_MAINTENANCE_REQUESTS, 'name' => 'Commercial Maintenance Requests' ],
			CImportDataType::COMMERCIAL_TRANSACTIONS				=> [ 'id' => CImportDataType::COMMERCIAL_TRANSACTIONS, 'name' => 'Commercial Transactions' ],
			CImportDataType::COMMERCIAL_RESIDENT_INSURANCE			=> [ 'id' => CImportDataType::COMMERCIAL_RESIDENT_INSURANCE, 'name' => 'Commercial Resident Insurance' ],
			CImportDataType::COMMERCIAL_OTHER_CLAUSE				=> [ 'id' => CImportDataType::COMMERCIAL_OTHER_CLAUSE, 'name' => 'Commercial Other Clause' ]
		]
	];

	public static $c_arrmixCommercialSIMOccupiedOptions = [
		'required' => [ CImportDataType::TENANTS_AND_SUITES 		=> [ 'id' => CImportDataType::TENANTS_AND_SUITES, 'name' => 'Tenants And Suites' ] ],
		'optional' => [
			CImportDataType::COMMERCIAL_ADD_ONS 					=> [ 'id' => CImportDataType::COMMERCIAL_ADD_ONS, 'name' => 'Commercial Add Ons' ],
			CImportDataType::COMMERCIAL_RECURRING_CHARGES 			=> [ 'id' => CImportDataType::COMMERCIAL_RECURRING_CHARGES, 'name' => 'Commercial Recurring Charges' ],
			CImportDataType::COMMERCIAL_ACTIVITY_LOGS 				=> [ 'id' => CImportDataType::COMMERCIAL_ACTIVITY_LOGS, 'name' => 'Commercial Activity Logs' ],
			CImportDataType::COMMERCIAL_ADDRESSES 					=> [ 'id' => CImportDataType::COMMERCIAL_ADDRESSES, 'name' => 'Commercial Addresses' ],
			CImportDataType::COMMERCIAL_CONTACTS 					=> [ 'id' => CImportDataType::COMMERCIAL_CONTACTS, 'name' => 'Commercial Contacts' ],
			CImportDataType::COMMERCIAL_RENEWAL_OPTIONS 			=> [ 'id' => CImportDataType::COMMERCIAL_RENEWAL_OPTIONS, 'name' => 'Commercial Renewal Options' ],
			CImportDataType::RENT_STEPS 							=> [ 'id' => CImportDataType::RENT_STEPS, 'name' => 'Rent Steps' ],
			CImportDataType::COMMERCIAL_GUARANTOR 					=> [ 'id' => CImportDataType::COMMERCIAL_GUARANTOR, 'name' => 'Commercial Guarantor' ],
			CImportDataType::COMMERCIAL_MAINTENANCE_REQUESTS		=> [ 'id' => CImportDataType::COMMERCIAL_MAINTENANCE_REQUESTS, 'name' => 'Commercial Maintenance Requests' ],
			CImportDataType::COMMERCIAL_TRANSACTIONS				=> [ 'id' => CImportDataType::COMMERCIAL_TRANSACTIONS, 'name' => 'Commercial Transactions' ],
			CImportDataType::COMMERCIAL_RESIDENT_INSURANCE			=> [ 'id' => CImportDataType::COMMERCIAL_RESIDENT_INSURANCE, 'name' => 'Commercial Resident Insurance' ],
			CImportDataType::COMMERCIAL_OTHER_CLAUSE				=> [ 'id' => CImportDataType::COMMERCIAL_OTHER_CLAUSE, 'name' => 'Commercial Other Clause' ],
			CImportDataType::COMMERCIAL_BUILDINGS					=> [ 'id' => CImportDataType::COMMERCIAL_BUILDINGS, 'name' => 'Commercial Buildings' ],
			CImportDataType::COMMERCIAL_SUITES						=> [ 'id' => CImportDataType::COMMERCIAL_SUITES, 'name' => 'Commercial Suites' ]
		]
	];


	public static $c_arrintListOfTablesByImportDataType = [
		self::FLOOR_PLANS				=> [ 'property_buildings', 'property_building_addresses', 'property_floors', 'property_floorplans', 'unit_spaces', 'property_units', 'unit_types', 'rates', 'maintenance_requests' ],
		self::RESIDENTS_AND_APPLICANTS	=> [ 'lease_processes', 'customer_addresses', 'leases', 'customers', 'applications', 'applicants', 'applicant_applications', 'lease_details' ],
		self::RECURRING_CHARGES			=> [ 'scheduled_charges' ],
		self::GUEST_CARDS				=> [ 'applications', 'applicants', 'customer_addresses' ],
		self::CUSTOMER_DEMOGRAPHICS		=> [ 'customers', 'applications', 'applicants' ],
		self::OCCUPANTS					=> [ 'applications', 'applicants' ],
		self::PROPERTY_UNITS            => [ 'property_units, unit_types', 'unit_spaces' ],
		self::RESIDENTS                 => [ 'lease_processes', 'customer_addresses', 'leases', 'customers', 'applications', 'applicants', 'applicant_applications', 'lease_details' ],
		self::PAST_RESIDENTS            => [ 'lease_processes', 'customer_addresses', 'leases', 'customers', 'applications', 'applicants', 'applicant_applications', 'lease_details' ],
		self::CANCELED_RESIDENTS        => [ 'lease_processes', 'customer_addresses', 'leases', 'customers', 'applications', 'applicants', 'applicant_applications', 'lease_details' ],
		self::APPLICANTS                => [ 'lease_processes', 'customer_addresses', 'leases', 'customers', 'applications', 'applicants', 'applicant_applications', 'lease_details' ],
		self::WRITEOFF_LEASES           => [ 'lease_processes', 'customer_addresses', 'leases', 'customers', 'applications', 'applicants', 'applicant_applications', 'lease_details' ]
	];

	public static $c_arrintAllowedCallWithMigrationMode = [
		self::RESIDENTS,
		self::IMPORT_CURRENT_RESIDENTS,
		self::IMPORT_FUTURE_RESIDENTS,
		self::IMPORT_NOTICE_RESIDENTS,
		self::APPLICANTS,
		self::PULL_ACTIVE_APPLICANT,
		self::PULL_ARCHIVE_APPLICANT,
		self::PULL_CANCELED_APPLICANT,
		self::PAST_RESIDENTS,
		self::PULL_PAST_RESIDENTS,
		self::CANCELED_RESIDENTS,
		self::WRITEOFF_LEASES,
		self::PULL_APPLICANT_WRITEOFF,
		self::PULL_PAST_WRITEOFF,
		self::PULL_CANCELLED_WRITEOFF,
		self::TRANSACTIONS,
		self::PULL_FULL_LEASE_LEDGER,
		self::RECURRING_CHARGES,
		self::MIGRATE_ONE_TIME_SCHEDULED_CHARGES,
		self::MIGRATE_RENEWAL_OFFERS
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>