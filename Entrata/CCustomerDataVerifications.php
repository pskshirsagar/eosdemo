<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerDataVerifications
 * Do not add any new functions to this class.
 */

class CCustomerDataVerifications extends CBaseCustomerDataVerifications {

	public static function fetchCustomerDataVerificationsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cdv.*,
						ce.name_first,
						ce.name_last
					FROM
						customer_data_verifications cdv
						JOIN company_employees ce ON ( cdv.verified_by = ce.id AND cdv.cid = ce.cid )
					WHERE
						cdv.cid = ' . ( int ) $intCid . '
						AND cdv.customer_id = ' . ( int ) $intCustomerId . '
					';

		return self::fetchCustomerDataVerifications( $strSql, $objDatabase );
	}

	public static function fetchExistingCustomerDataVerificationByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
					cdv.*,
					ce.name_first,
					ce.name_last
				FROM
					customer_data_verifications cdv
					JOIN company_employees ce ON ( cdv.verified_by = ce.id AND cdv.cid = ce.cid )
				WHERE
					cdv.cid = ' . ( int ) $intCid . '
					AND cdv.id IN ( ' . implode( ',', $arrintIds ) . ' )
				';
		return self::fetchCustomerDataVerifications( $strSql, $objDatabase );
	}

	public static function fetchCustomerDataVerificationByDataReferenceIdByCustomerDataTypeIdByCid( $intCustomerDataReferenceId, $intCustomerDataTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					cdv.*,
					ce.name_first,
					ce.name_last
				FROM
					customer_data_verifications cdv
					JOIN company_employees ce ON ( cdv.verified_by = ce.id AND cdv.cid = ce.cid )
				WHERE
					cdv.cid = ' . ( int ) $intCid . '
					AND cdv.customer_data_type_id = ' . ( int ) $intCustomerDataTypeId . '
					ANd cdv.customer_data_reference_id = ' . ( int ) $intCustomerDataReferenceId . '
				';
		return self::fetchCustomerDataVerification( $strSql, $objDatabase );

	}

	public static function fetchCustomerDataVerificationsByCustomerDataTypeIdsByCustomerIdByCid( $arrintCustomerDataTypeIds, $intCustomerId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						cdv.*
					FROM
						customer_data_verifications cdv
					WHERE
						cdv.cid = ' . ( int ) $intCid . '
						AND cdv.customer_id = ' . ( int ) $intCustomerId . '
						ANd cdv.customer_data_type_id IN ( ' . implode( ',', $arrintCustomerDataTypeIds ) . ')
				 ';

		return self::fetchCustomerDataVerifications( $strSql, $objDatabase );
	}

}
?>