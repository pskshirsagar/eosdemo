<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedCreditCardBinNumbers
 * Do not add any new functions to this class.
 */

class CCachedCreditCardBinNumbers extends CBaseCachedCreditCardBinNumbers {

	public static function fetchPseudoAllCreditCardBinNumbers( $intOffset, $intLimit, $objPaymentDatabase ) {
		$strSql = 'SELECT * FROM credit_card_bin_numbers ORDER BY id LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;

		return self::fetchCachedCreditCardBinNumbers( $strSql, $objPaymentDatabase );
	}

}
