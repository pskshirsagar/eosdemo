<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationQueues
 * Do not add any new functions to this class.
 */

class CIntegrationQueues extends CBaseIntegrationQueues {

	public static function fetchCustomIntegrationQueuesByIntegrationQueuesFilter( $objIntegrationQueuesFilter, $objDatabase ) {
		$objIntegrationQueuesFilter = ( true == valObj( $objIntegrationQueuesFilter, 'CIntegrationQueuesFilter' ) ) ? $objIntegrationQueuesFilter : new CIntegrationQueuesFilter();

		$strTableNames      = 'integration_queues iq
								JOIN integration_clients ic ON( ic.id = iq.integration_client_id AND ic.cid = iq.cid )';
		$strFields          = ' iq.*';
		$strCustomFields    = '';
		$arrstrCustomFields = [];
		$arrmixReturnData   = NULL;
		$strConditions      = ' true';
		$strSortBy          = ' ORDER BY iq.created_on DESC';
		$intOffset          = ( 0 < $objIntegrationQueuesFilter->getPageNo() ) ? ( int ) $objIntegrationQueuesFilter->getPageSize() * ( $objIntegrationQueuesFilter->getPageNo() - 1 ) : 0;
		$intLimit           = ( int ) $objIntegrationQueuesFilter->getPageSize();

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $objIntegrationQueuesFilter );

		if( false == is_null( $objIntegrationQueuesFilter->getIntegrationClientTypeIds() ) ) {
			$strTableNames .= ' JOIN integration_client_types ict ON( ic.integration_client_type_id = ict.id )';
		}

		if( false == is_null( $objIntegrationQueuesFilter->getIntegrationSyncTypeIds() ) ) {
			$strTableNames .= ' JOIN integration_sync_types ist ON( ic.integration_sync_type_id = ist.id )';
		}

		if( true == valArr( $objIntegrationQueuesFilter->getCustomFields() ) ) {
			foreach( $objIntegrationQueuesFilter->getCustomFields() as $strCustomFieldName ) {
				switch( $strCustomFieldName ) {
					case 'integration_database_name':
						$arrstrCustomFields['database_name'] = 'id.database_name';
						$strTableNames                       .= ' JOIN integration_databases id ON( ic.integration_database_id = id.id AND ic.cid = id.cid )';
						break;

					default:
						// default case
						break;
				}
			}

			if( true == valArr( $arrstrCustomFields ) ) {
				$strCustomFields = implode( ',', $arrstrCustomFields );
			}
		}

		$strConditions      .= ( true == valArr( $arrstrAndSearchParameters ) ) ? '  AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '';
		$strOffSetCondition = ( 0 < $intLimit ) ? ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit : '';

		switch( $objIntegrationQueuesFilter->getReturnCriteria() ) {
			case CEosFilter::RETURN_CRITERIA_COUNT:
				$strSql = 'SELECT COUNT(iq.id) FROM ' . $strTableNames . ' WHERE ' . $strConditions;

				$arrintCount      = fetchData( $strSql, $objDatabase );
				$arrmixReturnData = ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
				break;

			case CEosFilter::RETURN_CRITERIA_CUSTOM_ARRAY:
			if( true == valStr( $strCustomFields ) ) {
				$strSql = 'SELECT ' . $strCustomFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy . $strOffSetCondition;
			}

			case CEosFilter::RETURN_CRITERIA_ARRAY:

				if( false == valStr( $strSql ) ) {
					$strFields = ( true == valStr( $strCustomFields ) ) ? $strCustomFields . ', ' . $strFields : $strFields;
					$strSql    = 'SELECT ' . $strFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy . $strOffSetCondition;
				}

				$arrstrMixedData      = fetchData( $strSql, $objDatabase );
				$arrstrPropertiesData = [];

				if( true == valArr( $arrstrMixedData ) ) {
					$intCounter = 0;

					foreach( $arrstrMixedData as $arrstrPropertyData ) {
						if( true == isset( $arrstrPropertyData['cid'] ) ) {
							$intIndex                                                    = ( true == isset( $arrstrPropertyData['id'] ) ) ? $arrstrPropertyData['id'] : $intCounter++;
							$arrstrPropertiesData[$arrstrPropertyData['cid']][$intIndex] = $arrstrPropertyData;
						} else {
							$intIndex                        = ( true == isset( $arrstrPropertyData['id'] ) ) ? $arrstrPropertyData['id'] : $intCounter++;
							$arrstrPropertiesData[$intIndex] = $arrstrPropertyData;
						}
					}
				}

				$arrmixReturnData = $arrstrPropertiesData;
				break;

			default:
				$strFields = ( true == valStr( $strCustomFields ) ) ? $strCustomFields . ', ' . $strFields : $strFields;
				$strSql    = 'SELECT ' . $strFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy;

				if( true == $objIntegrationQueuesFilter->getReturnSingleRecord() ) {
					$strSql = $strSql . ' LIMIT 1;';

					return parent::fetchIntegrationQueue( $strSql, $objDatabase );
				}

				$strSql .= $strOffSetCondition;

				$arrmixReturnData = parent::fetchIntegrationQueues( $strSql, $objDatabase, false );
				break;
		}

		return $arrmixReturnData;
	}

	public static function fetchIntegrationQueuesByIntegrationClientIdByIntegrationQueueStatusTypeIdByCid( $intIntegrationClientId, $intIntegrationQueueStatusTypeId, $intCid, $objDatabase ) {
		$objIntegrationQueuesFilter = new CIntegrationQueuesFilter();
		$objIntegrationQueuesFilter->setCid( $intCid );
		$objIntegrationQueuesFilter->setIntegrationClientIds( [ $intIntegrationClientId ] );
		$objIntegrationQueuesFilter->setIntegrationQueueStatusTypeIds( [ $intIntegrationQueueStatusTypeId ] );

		return self::fetchCustomIntegrationQueuesByIntegrationQueuesFilter( $objIntegrationQueuesFilter, $objDatabase );
	}

	public static function fetchCustomPaginatedIntegrationQueuesByCid( $intPageNo, $intPageSize, $objDatabase, $objIntegrationQueuesFilter = NULL ) {
		if( false == valObj( $objIntegrationQueuesFilter, 'CIntegrationQueuesFilter' ) ) {
			$objIntegrationQueuesFilter = new CIntegrationQueuesFilter();
		}

		$objIntegrationQueuesFilter->setPageNo( $intPageNo );
		$objIntegrationQueuesFilter->setPageSize( $intPageSize );
		if( false == valArr( $objIntegrationQueuesFilter->getIntegrationQueueStatusTypeIds() ) ) {
			$objIntegrationQueuesFilter->setIntegrationQueueStatusTypeIds( [
				CIntegrationQueueStatusType::READY,
				CIntegrationQueueStatusType::WAITING,
				CIntegrationQueueStatusType::PENDING_REVIEW
			] );
		}

		return self::fetchCustomIntegrationQueuesByIntegrationQueuesFilter( $objIntegrationQueuesFilter, $objDatabase );
	}

	public static function fetchCustomPaginatedIntegrationQueuesCountByCid( $objDatabase, $objIntegrationQueuesFilter = NULL ) {
		if( false == valObj( $objIntegrationQueuesFilter, 'CIntegrationQueuesFilter' ) ) {
			$objIntegrationQueuesFilter = new CIntegrationQueuesFilter();
		}

		$objIntegrationQueuesFilter->setReturnCriteria( CEosFilter::RETURN_CRITERIA_COUNT );
		if( false == valArr( $objIntegrationQueuesFilter->getIntegrationQueueStatusTypeIds() ) ) {
			$objIntegrationQueuesFilter->setIntegrationQueueStatusTypeIds( [
				CIntegrationQueueStatusType::READY,
				CIntegrationQueueStatusType::WAITING,
				CIntegrationQueueStatusType::PENDING_REVIEW
			] );
		}

		return self::fetchCustomIntegrationQueuesByIntegrationQueuesFilter( $objIntegrationQueuesFilter, $objDatabase );
	}

	public static function fetchSearchCriteria( $objIntegrationQueuesFilter ) {

		$arrstrAndSearchParameters = [];

		// Create SQL parameters.
		if( true == valStr( $objIntegrationQueuesFilter->getId() ) ) {
			$arrstrAndSearchParameters[] = ' iq.id::integer = ' . ( int ) $objIntegrationQueuesFilter->getId() . ' ';
		}
		if( true == valStr( $objIntegrationQueuesFilter->getCid() ) ) {
			$arrstrAndSearchParameters[] = ' iq.cid::integer = ' . ( int ) $objIntegrationQueuesFilter->getCid() . ' ';
		}
		if( true == valStr( $objIntegrationQueuesFilter->getIntegrationDatabaseId() ) ) {
			$arrstrAndSearchParameters[] = ' ic.integration_database_id::integer = ' . ( int ) $objIntegrationQueuesFilter->getIntegrationDatabaseId() . ' ';
		}
		if( true == valArr( $objIntegrationQueuesFilter->getIntegrationClientIds() ) ) {
			$arrstrAndSearchParameters[] = ' ic.id IN( ' . implode( ',', $objIntegrationQueuesFilter->getIntegrationClientIds() ) . ' ) ';
		}
		if( false == is_null( $objIntegrationQueuesFilter->getPropertyId() ) ) {
			$arrstrAndSearchParameters[] = ' iq.integration_client_id::integer IN ( SELECT ic1.id FROM integration_clients ic1, property_integration_databases pid1 WHERE ic1.integration_database_id = pid1.integration_database_id AND ic1.cid = pid1.cid AND pid1.property_id = ' . $objIntegrationQueuesFilter->getPropertyId() . ' AND pid1.cid = ' . ( int ) $objIntegrationQueuesFilter->getCid() . ' ) ';
		}
		if( false == is_null( $objIntegrationQueuesFilter->getReferenceNumber() ) ) {
			$arrstrAndSearchParameters[] = ' iq.reference_number =' . ( int ) $objIntegrationQueuesFilter->getReferenceNumber() . ' ';
		}
		if( true == valArr( $objIntegrationQueuesFilter->getReferenceNumbers() ) ) {
			$arrstrAndSearchParameters[] = ' iq.reference_number IN(' . implode( ',', $objIntegrationQueuesFilter->getReferenceNumbers() ) . ') ';
		}
		if( true == valArr( $objIntegrationQueuesFilter->getIntegrationClientTypeIds() ) ) {
			$arrstrAndSearchParameters[] = ' ict.id IN(' . implode( ',', $objIntegrationQueuesFilter->getIntegrationClientTypeIds() ) . ')';
		}
		if( true == valArr( $objIntegrationQueuesFilter->getIntegrationSyncTypeIds() ) ) {
			$arrstrAndSearchParameters[] = ' ist.id IN(' . implode( ',', $objIntegrationQueuesFilter->getIntegrationSyncTypeIds() ) . ')';
		}
		if( true == valArr( $objIntegrationQueuesFilter->getIntegrationServiceIds() ) ) {
			$arrstrAndSearchParameters[] = ' iq.integration_service_id IN(' . implode( ',', $objIntegrationQueuesFilter->getIntegrationServiceIds() ) . ')';
		}
		if( true == valArr( $objIntegrationQueuesFilter->getIntegrationQueueStatusTypeIds() ) ) {
			$arrstrAndSearchParameters[] = ' iq.integration_queue_status_type_id IN (' . implode( ',', $objIntegrationQueuesFilter->getIntegrationQueueStatusTypeIds() ) . ')';
		}
		if( false == is_null( $objIntegrationQueuesFilter->getFromDate() ) ) {
			$arrstrAndSearchParameters[] = ' iq.created_on >= \'' . date( 'Y-m-d', strtotime( $objIntegrationQueuesFilter->getFromDate() ) ) . ' 00:00:00\'';
		}
		if( false == is_null( $objIntegrationQueuesFilter->getToDate() ) ) {
			$arrstrAndSearchParameters[] = ' iq.created_on<= \'' . date( 'Y-m-d', strtotime( $objIntegrationQueuesFilter->getToDate() ) ) . ' 23:59:59\'';
		}

		return $arrstrAndSearchParameters;
	}

	public static function fetchIntegrationQueueByIntegrationServiceIdByReferenceNumberByCid( $intIntegrationServiceId, $intReferenceNumber, $intCid, $objDatabase ) {
		$objIntegrationQueuesFilter = new CIntegrationQueuesFilter();
		$objIntegrationQueuesFilter->setCid( $intCid );
		$objIntegrationQueuesFilter->setIntegrationServiceIds( [ ( int ) $intIntegrationServiceId ] );
		$objIntegrationQueuesFilter->setReferenceNumbers( [ $intReferenceNumber ] );
		$objIntegrationQueuesFilter->setReturnSingleRecord( true );

		return self::fetchCustomIntegrationQueuesByIntegrationQueuesFilter( $objIntegrationQueuesFilter, $objDatabase );
	}

	public static function fetchIntegrationQueuesByIntegrationServiceIdsByReferenceNumberByCid( $arrintIntegrationServiceIds, $intReferenceNumber, $intCid, $objDatabase ) {
		$objIntegrationQueuesFilter = new CIntegrationQueuesFilter();
		$objIntegrationQueuesFilter->setCid( $intCid );
		$objIntegrationQueuesFilter->setIntegrationServiceIds( $arrintIntegrationServiceIds );
		$objIntegrationQueuesFilter->setReferenceNumbers( [ $intReferenceNumber ] );

		return self::fetchCustomIntegrationQueuesByIntegrationQueuesFilter( $objIntegrationQueuesFilter, $objDatabase );
	}

	public static function fetchIntegrationQueuesByIntegrationServiceIdsByReferenceNumbersByCid( $arrintIntegrationServiceIds, $arrintReferenceNumbers, $intCid, $objDatabase ) {
		$objIntegrationQueuesFilter = new CIntegrationQueuesFilter();
		$objIntegrationQueuesFilter->setCid( $intCid );
		$objIntegrationQueuesFilter->setIntegrationServiceIds( $arrintIntegrationServiceIds );
		$objIntegrationQueuesFilter->setReferenceNumbers( $arrintReferenceNumbers );
		$objIntegrationQueuesFilter->setIntegrationQueueStatusTypeIds( [ CIntegrationQueueStatusType::READY, CIntegrationQueueStatusType::WAITING, CIntegrationQueueStatusType::PENDING_REVIEW, CIntegrationQueueStatusType::COMPLETED ] );

		return self::fetchCustomIntegrationQueuesByIntegrationQueuesFilter( $objIntegrationQueuesFilter, $objDatabase );
	}

	public static function fetchIntegrationQueuesByIntegrationServiceIdByIntegrationQueueStatusTypeIds( $intServiceId, $arrintIntegrationQueueStatusTypeIds, $objDatabase ) {

		if( false == valArr( $arrintIntegrationQueueStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						integration_queues
					WHERE
						integration_service_id = ' . ( int ) $intServiceId . '
						AND integration_queue_status_type_id IN (' . implode( ', ', $arrintIntegrationQueueStatusTypeIds ) . ')';

		return self::fetchIntegrationQueues( $strSql, $objDatabase );
	}

	public static function fetchIntegrationQueuesByIntegrationServiceIdByIntegrationQueueStatusTypeIdsByCid( $intServiceId, $arrintIntegrationQueueStatusTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintIntegrationQueueStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						reference_number
					FROM
						integration_queues
					WHERE
						cid = ' . ( int ) $intCid . '
						AND integration_service_id = ' . ( int ) $intServiceId . '
					    AND integration_queue_status_type_id IN (' . implode( ', ', $arrintIntegrationQueueStatusTypeIds ) . ')';

		return rekeyArray( 'reference_number', fetchData( $strSql, $objDatabase ) );
	}

}

?>