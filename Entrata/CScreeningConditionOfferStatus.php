<?php

class CScreeningConditionOfferStatus extends CBaseScreeningConditionOfferStatus {

	const OFFERED 	= 1;
	const VIEWED 	= 2;
	const ACCEPTED 	= 3;
	const REJECTED 	= 4;
	const EXPIRED 	= 5;

	public static $c_arrintScreeningOfferStatuses = [ self::OFFERED, self::VIEWED ];

	public static $c_arrintOfferInActiveStatuses = [ self::ACCEPTED, self::REJECTED, self::EXPIRED ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getScreeningConditionOfferStatusName( $intScreeningConditionOfferStatusId ) {

		$strStatusName = '';

		switch( $intScreeningConditionOfferStatusId ) {
			case self::OFFERED:
				$strStatusName = 'Offered';
				break;

			case self::ACCEPTED:
				$strStatusName = 'Accepted';
				break;

			case self::REJECTED:
				$strStatusName = 'Declined';
				break;

			case self::VIEWED:
				$strStatusName = 'Viewed';
				break;

			default:
				// handle
				break;
		}

		return $strStatusName;
	}

}
?>