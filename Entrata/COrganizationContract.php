<?php

class COrganizationContract extends CBaseOrganizationContract {

	protected $m_strPropertyName;
	protected $m_strCompanyApplicationTitle;
	protected $m_strLeaseStartWindowStartDate;
	protected $m_strLeaseStartWindowEndDate;
	protected $m_strFinancialResponsibilityTypeName;
	protected $m_intCustomerTypeId;
	protected $m_strCustomerRelationshipName;
	protected $m_strCustomerTypeName;
	protected $m_intMoveInChecklistId;
	protected $m_intMoveOutChecklistId;

	public function valId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intId ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Group Contract Request:  Id required - COrganizationContract::valId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCid ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Request:  Management Id required - COrganizationContract::valCid()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == valId( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCustomerId ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Group Contract Request:  Customer Id required - COrganizationContract::valCustomerId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valOrganizationId() {
		$boolIsValid = true;

		if( false == valId( $this->getOrganizationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'organization_id', __( 'Organization code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOrganizationContractStatusTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getOrganizationContractStatusTypeId() ) ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Company Group Contract Request:  Group Contract Status Type Id required - COrganizationContract::valOrganizationContractStatusTypeId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valOrganizationContractFinancialResponsibilityTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getOrganizationContractFinancialResponsibilityTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'organization_contract_financial_responsibility_type_id', __( 'Please select Financial Responsibility.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyApplicationId() {
		$boolIsValid = true;

		if( false == valId( $this->getCompanyApplicationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_application_id', __( 'Application is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase, $arrmixData ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Contract Name is required.' ) ) );
		}

		if( true == $boolIsValid && false == $arrmixData['is_skip_to_validate_name'] && 0 < $this->getDuplicateNameCount( $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Contract Name already in use.' ) ) );
		}

		return $boolIsValid;
	}

	public function valReservationCode( $objDatabase, $arrmixData ) {
		$boolIsValid = true;

		if( false == $arrmixData['is_allow_null_reservation_code'] && false == valStr( $this->getReservationCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reservation_code', __( 'Reservation Code is required.' ) ) );
		}

		if( true == valStr( $this->getReservationCode() ) && false == $arrmixData['is_skip_to_validate_reservation_code'] ) {
			$intDuplicateReservationCount = \Psi\Eos\Entrata\COrganizationContracts::createService()->fetchDuplicateContractsCountByReservationCodeByCid( $this->getReservationCode(), $this->getCid(), $objDatabase );
			if( 0 < $intDuplicateReservationCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reservation_code', __( 'Reservation Code already in use.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valCustomerTypeId() {
		$boolIsValid = true;
		if( $this->getOrganizationContractFinancialResponsibilityTypeId() != COrganizationContractFinancialResponsibilityType::RESIDENT_IS_HUNDRED_PERCENT_RESPONSIBLE ) {
			if( false == valId( $this->getCustomerTypeId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_type_id', __( 'Occupant Type is required.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valCustomerRelationshipId( $objDatabase ) {
		$boolIsValid = true;

		if( $this->getOrganizationContractFinancialResponsibilityTypeId() != COrganizationContractFinancialResponsibilityType::RESIDENT_IS_HUNDRED_PERCENT_RESPONSIBLE ) {
			$arrobjPropertyApplications = [];

			if( true == valId( $this->getCompanyApplicationId() ) ) {
				$arrobjPropertyApplications = ( array ) \Psi\Eos\Entrata\CPropertyApplications::createService()->fetchPropertyApplicationByCompanyApplicationIdsByPropertyIdByCid( [ $this->getCompanyApplicationId() ], $this->getPropertyId(), $this->getCid(), $objDatabase );
			}
			if( false == valArr( $arrobjPropertyApplications ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_relationship_id', __( 'The application is no longer associated with this property. To continue, please associate an application to the property' ) ) );
			} elseif( false == valId( $this->getPrimaryCustomerRelationshipId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_relationship_id', __( 'Customer Relationship is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $arrmixData = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_organization_contract':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valOrganizationId();
				$boolIsValid &= $this->valOrganizationContractStatusTypeId();
				$boolIsValid &= $this->valCompanyApplicationId();
				$boolIsValid &= $this->valOrganizationContractFinancialResponsibilityTypeId();
				$boolIsValid &= $this->valCustomerTypeId();
				$boolIsValid &= $this->valCustomerRelationshipId( $objDatabase );
				$boolIsValid &= $this->valName( $objDatabase, $arrmixData );
				$boolIsValid &= $this->valReservationCode( $objDatabase, $arrmixData );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	private function getDuplicateNameCount( $objDatabase ) {

		$strWhere = ' WHERE
						cid = ' . ( int ) $this->m_intCid . '
						AND organization_id = ' . ( int ) $this->m_intOrganizationId . '
						AND property_id = ' . ( int ) $this->m_intPropertyId . '
						AND lower( name ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $this->m_strName ) ) . '\'';

		return \Psi\Eos\Entrata\COrganizationContracts::createService()->fetchOrganizationContractCount( $strWhere, $objDatabase );
	}

	/**
	 * Getter Functions
	 *
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getCompanyApplicationTitle() {
		return $this->m_strCompanyApplicationTitle;
	}

	public function getLeaseStartWindowStartDate() {
		return $this->m_strLeaseStartWindowStartDate;
	}

	public function getLeaseStartWindowEndDate() {
		return $this->m_strLeaseStartWindowEndDate;
	}

	public function getFinancialResponsibilityTypeName() {
		return $this->m_strFinancialResponsibilityTypeName;
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function useMoveInChecklist() : bool {
		return ( bool ) $this->getDetailsField( 'use_move_in_checklist' );
	}

	public function useMoveOutChecklist() : bool {
		return ( bool ) $this->getDetailsField( 'use_move_out_checklist' );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function getCustomerRelationshipName() {
		return $this->m_strCustomerRelationshipName;
	}

	public function getCustomerTypeName() {
		return $this->m_strCustomerTypeName;
	}

	public function getMoveInChecklistId() {
		return $this->m_intMoveInChecklistId;
	}

	public function getMoveOutChecklistId() {
		return $this->m_intMoveOutChecklistId;
	}

	/**
	 * Setter Functions
	 *
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, NULL, NULL, true ) );
	}

	public function setCompanyApplicationTitle( $strCompanyApplicationTitle ) {
		$this->set( 'm_strCompanyApplicationTitle', CStrings::strTrimDef( $strCompanyApplicationTitle, NULL, NULL, true ) );
	}

	public function setLeaseStartWindowStartDate( $strLeaseStartWindowStartDate ) {
		$this->set( 'm_strLeaseStartWindowStartDate', CStrings::strTrimDef( $strLeaseStartWindowStartDate, NULL, NULL, true ) );
	}

	public function setLeaseStartWindowEndDate( $strLeaseStartWindowEndDate ) {
		$this->set( 'm_strLeaseStartWindowEndDate', CStrings::strTrimDef( $strLeaseStartWindowEndDate, NULL, NULL, true ) );
	}

	public function setFinancialResponsibilityTypeName( $strFinancialResponsibilityTypeName ) {
		$this->set( 'm_strFinancialResponsibilityTypeName', CStrings::strTrimDef( $strFinancialResponsibilityTypeName, NULL, NULL, true ) );
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function setCustomerRelationshipName( $strCustomerRelationshipName ) {
		$this->set( 'm_strCustomerRelationshipName', CStrings::strTrimDef( $strCustomerRelationshipName, NULL, NULL, true ) );
	}

	public function setCustomerTypeName( $strCustomerTypeName ) {
		$this->set( 'm_strCustomerTypeName', CStrings::strTrimDef( $strCustomerTypeName, NULL, NULL, true ) );
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->set( 'm_intCustomerTypeId', CStrings::strTrimDef( $intCustomerTypeId, -1, NULL, true ) );
	}

	public function setMoveInChecklistId( $intMoveInChecklistId ) {
		$this->m_intMoveInChecklistId = $intMoveInChecklistId;
	}

	public function setMoveOutChecklistId( $intMoveOutChecklistId ) {
		$this->m_intMoveOutChecklistId = $intMoveOutChecklistId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['company_application_title'] ) ) $this->setCompanyApplicationTitle( $arrmixValues['company_application_title'] );
		if( true == isset( $arrmixValues['lease_start_window_start_date'] ) ) $this->setLeaseStartWindowStartDate( $arrmixValues['lease_start_window_start_date'] );
		if( true == isset( $arrmixValues['lease_start_window_end_date'] ) ) $this->setLeaseStartWindowEndDate( $arrmixValues['lease_start_window_end_date'] );
		if( true == isset( $arrmixValues['financial_responsibility_type_name'] ) ) $this->setFinancialResponsibilityTypeName( $arrmixValues['financial_responsibility_type_name'] );
		if( true == isset( $arrmixValues['start_date'] ) ) $this->setStartDate( $arrmixValues['start_date'] );
		if( true == isset( $arrmixValues['end_date'] ) ) $this->setEndDate( $arrmixValues['end_date'] );
		if( true == isset( $arrmixValues['use_move_in_checklist'] ) ) $this->setUseMoveInChecklist( $arrmixValues['use_move_in_checklist'] );
		if( true == isset( $arrmixValues['move_in_checklist_id'] ) ) $this->setMoveInChecklistId( $arrmixValues['move_in_checklist_id'] );
		if( true == isset( $arrmixValues['use_move_out_checklist'] ) ) $this->setUseMoveOutChecklist( $arrmixValues['use_move_out_checklist'] );
		if( true == isset( $arrmixValues['move_out_checklist_id'] ) ) $this->setMoveOutChecklistId( $arrmixValues['move_out_checklist_id'] );
		if( true == isset( $arrmixValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
		if( true == isset( $arrmixValues['customer_relationship_name'] ) ) $this->setCustomerRelationshipName( $arrmixValues['customer_relationship_name'] );
		if( true == isset( $arrmixValues['customer_type_name'] ) ) $this->setCustomerTypeName( $arrmixValues['customer_type_name'] );
	}

	public function setUseMoveInChecklist( $boolUseMoveInChecklist ) {
		$this->setDetailsField( 'use_move_in_checklist', $boolUseMoveInChecklist );
	}

	public function setUseMoveOutChecklist( $boolUseMoveOutChecklist ) {
		$this->setDetailsField( 'use_move_out_checklist', $boolUseMoveOutChecklist );
	}

	/**
	 * Fetch Functions
	 */
	public static function fetchGroupContractBalance( $objGroupLedgersFilter, $objDatabase ) {

		if( false == valObj( $objGroupLedgersFilter, CGroupLedgersFilter::class ) ) {
			trigger_error( 'Group ledger filter failed to load.', E_USER_ERROR );
		}

		$strSql = 'SELECT
						sum( at.transaction_amount_due )
					FROM
						organization_contracts oc
						JOIN cached_leases cl ON ( oc.cid = cl.cid AND oc.id = cl.organization_contract_id )
						JOIN view_ar_transactions at ON ( cl.cid = at.cid AND cl.id = at.lease_id )
						JOIN ledger_filters lf ON ( at.cid = lf.cid AND lf.id = at.ledger_filter_id )
					WHERE
						oc.cid = ' . ( int ) $objGroupLedgersFilter->getCid() . '
						AND oc.id = ' . ( int ) $objGroupLedgersFilter->getOrganizationContractId() . '
						AND lf.default_ledger_filter_id = ' . CDefaultLedgerFilter::GROUP;

		$arrstrBalance           = fetchData( $strSql, $objDatabase );
		return sprintf( '%01.2f', $arrstrBalance[0]['sum'] );
	}

	public function createOrganizationContractChecklist( $intChecklistId ) {
		$objOrganizationContractChecklist = new COrganizationContractChecklist();
		$objOrganizationContractChecklist->setCid( $this->getCid() );
		$objOrganizationContractChecklist->setPropertyId( $this->getPropertyId() );
		$objOrganizationContractChecklist->setOrganizationContractId( $this->getId() );
		$objOrganizationContractChecklist->setChecklistId( $intChecklistId );

		return $objOrganizationContractChecklist;
	}

	public function logAuthentication( $intCompanyUserId, $objDatabase, $boolIsLoginFromEntrata = false, $objCompanyUser = NULL, $intPropertyId = NULL ) {
		$objEventLibrary = new CEventLibrary();
		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		if( true == $boolIsLoginFromEntrata ) {
			$intEventType = CEventType::RESERVATIONHUB_LOG_IN_BY_MANAGER;
		} else {
			$intEventType = CEventType::RESERVATIONHUB_LOG_IN;
		}

		$objEvent = $objEventLibrary->createEvent( [ $this ], $intEventType );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setEventDatetime( 'NOW()' );

		if( true === is_null( $intPropertyId ) ) {
			$objEvent->setPropertyId( $this->getPropertyId() );
		} else {
			$objEvent->setPropertyId( $intPropertyId );
		}

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objEvent->setCompanyUser( $objCompanyUser );
		}

		$objEventLibrary->buildEventDescription( $this );
		if( true == valObj( $objEventLibrary, 'CEventLibrary' ) && false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
			$strErrorMessage = '';
			if( true == valArr( $objEventLibrary->getErrorMsgs() ) ) {
				$arrobjErrorkeys = array_keys( $objEventLibrary->getErrorMsgs() );
				$arrmixError = $objEventLibrary->getErrorMsgs();
				if( true == valArr( $arrobjErrorkeys ) ) {
					foreach( $arrobjErrorkeys as $strErrorKey ) {
						$strErrorMessage .= $arrmixError[$strErrorKey]->getMessage() . ' | ';
					}
				}
			}
			$objDatabase->rollback();
			trigger_error( 'Failed to insert event for the Contract : ' . $this->m_intId . ' & Event Type : ' . ( int ) $intEventType . ' & Property Id : ' . $this->getPropertyId() . ' & Company User Id : ' . ( int ) $intCompanyUserId . ' & Error messages : ' . $strErrorMessage, E_USER_WARNING );
			return;
		}

		return true;
	}

}
?>