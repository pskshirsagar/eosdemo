<?php

class CSubsidyContractType extends CBaseSubsidyContractType {

	const HUD_MARKET_RENT           = 0;
	const HUD_SECTION_8				= 1;
	const HUD_RENT_SUPPLEMENT		= 2;
	const HUD_RAP					= 3;
	const HUD_SECTION_236			= 4;
	const HUD_BMIR					= 5;
	const HUD_PRA_DEMO_811			= 6;
	const HUD_SECTION_202_PRAC		= 7;
	const HUD_SECTION_811_PRAC		= 8;
	const HUD_SECTION_202_162_PAC	= 9;

	const TAX_CREDIT				= 99;

	const HIGH_HOME					= 10;
	const LOW_HOME					= 11;

	public static $c_arrintAllHudContractTypes = [
		self::HUD_SECTION_8           => 'Section 8',
		self::HUD_RENT_SUPPLEMENT     => 'Rent Supplement',
		self::HUD_RAP                 => 'RAP',
		self::HUD_SECTION_236         => 'Section 236',
		self::HUD_BMIR                => 'BMIR',
		self::HUD_PRA_DEMO_811        => 'Pra Demo 811',
		self::HUD_SECTION_202_PRAC    => 'Section 202 PRAC',
		self::HUD_SECTION_811_PRAC    => 'Section 811 PRAC',
		self::HUD_SECTION_202_162_PAC => 'Section 202 162 PAC'
	];

	public static $c_arrintAllTaxCreditContractTypes = [
		self::TAX_CREDIT => 'Tax Credit'
	];

	public static $c_arrintContractNumberSubsidyContractTypes		= [ self::HUD_SECTION_8, self::HUD_SECTION_202_162_PAC, self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC ];
	public static $c_arrintContractUnitCountSubsidyContractTypes	= [ self::HUD_SECTION_8, self::HUD_SECTION_202_162_PAC, self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC, self::HUD_RENT_SUPPLEMENT, self::HUD_RAP, self::HUD_PRA_DEMO_811 ];

	public static $c_arrintNonSelectableUnitSubsidyContractTypes	= [ self::HUD_BMIR, self::HUD_SECTION_236 ];
	public static $c_arrintOperatingRentSubsidyContractTypes		= [ self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC ];
	public static $c_arrintProjectNumberSubsidyContractTypes		= [ self::HUD_BMIR, self::HUD_SECTION_236, self::HUD_RENT_SUPPLEMENT, self::HUD_RAP, self::HUD_SECTION_202_162_PAC, self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC ];
	public static $c_arrintExcludedLateFeeContractTypes				= [ self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC, self::HUD_SECTION_8, self::HUD_SECTION_202_162_PAC ];

 	public static $c_arrintExcludedSubsidyContractsFromLateFees		= [ self::HUD_SECTION_8, self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC, self::HUD_SECTION_202_162_PAC ];
	public static $c_arrintProjectNumberSubsidyContractsType		= [ self::HUD_SECTION_8, self::HUD_SECTION_202_PRAC, self::HUD_SECTION_811_PRAC, self::HUD_SECTION_202_162_PAC,  self::HUD_BMIR, self::HUD_SECTION_236, self::HUD_RENT_SUPPLEMENT, self::HUD_RAP ];

	public static $c_arrintHAPSubsidyContractTypeIds = [
		self::HUD_RENT_SUPPLEMENT,
		self::HUD_RAP
	];

	public static $c_arrintOtherHAPSubsidyContractTypeIds = [
		self::HUD_SECTION_8,
		self::HUD_SECTION_202_162_PAC,
		self::HUD_SECTION_811_PRAC,
		self::HUD_SECTION_202_PRAC,
		self::HUD_PRA_DEMO_811
	];

	public static $c_arrintSubsidySpecialClaimTypeUnpaidRentAndDamagesSubsidyContractTypeIds = [
		self::HUD_SECTION_8,
		self::HUD_SECTION_202_162_PAC,
		self::HUD_SECTION_811_PRAC,
		self::HUD_SECTION_202_PRAC
	];

	public static $c_arrintSubsidySpecialClaimTypeRentUpVacancySubsidyContractTypeIds = [
		self::HUD_SECTION_8,
		self::HUD_SECTION_202_PRAC,
		self::HUD_SECTION_811_PRAC
	];

	public static $c_arrintSubsidySpecialClaimTypeRegularVacancySubsidyContractTypeIds = [
		self::HUD_SECTION_8,
		self::HUD_SECTION_202_162_PAC,
		self::HUD_SECTION_811_PRAC,
		self::HUD_SECTION_202_PRAC
	];

	public static $c_arrintSubsidySpecialClaimTypeDebtServiceSubsidyContractTypeIds = [
		self::HUD_SECTION_8,
		self::HUD_SECTION_202_162_PAC
	];

	public static $c_arrintLowIncomeLimitSubsidyContractTypeIds = [
		self::HUD_RAP,
		self::HUD_RENT_SUPPLEMENT,
		self::HUD_SECTION_202_162_PAC,
		self::HUD_SECTION_236
	];

	public static $c_arrintVeryLowIncomeLimitSubsidyContractTypeIds = [
		self::HUD_SECTION_202_PRAC,
		self::HUD_SECTION_811_PRAC,
	];

	public static $c_arrintExtremelyLowIncomeLimitSubsidyContractTypeIds = [
		self::HUD_PRA_DEMO_811
	];

	public static $c_arrintNotDUNSSubsidyContractTypeIds = [
	    self::HUD_PRA_DEMO_811,
	    self::HUD_BMIR,
	    self::HUD_SECTION_202_162_PAC,
		self::HUD_SECTION_236
	];

	public static $c_arrintHomeContractTypeIds = [
		self::HIGH_HOME,
		self::LOW_HOME
	];

	public static $c_arrintTRACSSubsidyContractTypeIds = [
		self::HUD_RENT_SUPPLEMENT,
		self::HUD_RAP,
		self::HUD_SECTION_811_PRAC,
		self::HUD_SECTION_202_PRAC,
		self::HUD_SECTION_202_162_PAC
	];

	public static $c_arrintAffordableContractTypes = [
		self::HUD_SECTION_8,
		self:: HUD_RENT_SUPPLEMENT,
		self:: HUD_RAP,
		self:: HUD_SECTION_236,
		self:: HUD_BMIR,
		self:: HUD_PRA_DEMO_811,
		self:: HUD_SECTION_202_PRAC,
		self:: HUD_SECTION_811_PRAC,
		self:: HUD_SECTION_202_162_PAC,
		self::TAX_CREDIT
	];

	public static function getSubsidyContractTypes() : array {
		return [
			self::HUD_SECTION_8           => __( 'Section 8' ),
			self::HUD_RENT_SUPPLEMENT     => __( 'Rent Supplement' ),
			self::HUD_RAP                 => __( 'RAP' ),
			self::HUD_PRA_DEMO_811        => __( 'Pra Demo 811' ),
			self::HUD_SECTION_202_PRAC    => __( 'Section 202 PRAC' ),
			self::HUD_SECTION_811_PRAC    => __( 'Section 811 PRAC' ),
			self::HUD_SECTION_202_162_PAC => __( 'Section 202 162 PAC' )
		];
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIncomePercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getSubsidyContractNameById( $intSubsidyContractTypeId ) {
		switch( $intSubsidyContractTypeId ) {

			case self::HUD_SECTION_8:
				$strSubsidyContractTypeName = 'Section 8';
				break;

			case self::HUD_RENT_SUPPLEMENT:
				$strSubsidyContractTypeName = 'Rent Supplement';
				break;

			case self::HUD_RAP:
				$strSubsidyContractTypeName = 'RAP';
				break;

			case self::HUD_SECTION_236:
				$strSubsidyContractTypeName = 'Section 236';
				break;

			case self::HUD_BMIR:
				$strSubsidyContractTypeName = 'BMIR';
				break;

			case self::HUD_PRA_DEMO_811:
				$strSubsidyContractTypeName = '811 PRA Demo';
				break;

			case self::HUD_SECTION_202_PRAC:
				$strSubsidyContractTypeName = 'Section 202 PRAC';
				break;

			case self::HUD_SECTION_811_PRAC:
				$strSubsidyContractTypeName = 'Section 811 PRAC';
				break;

			case self::HUD_SECTION_202_162_PAC:
				$strSubsidyContractTypeName = 'Section 202/162 PAC';
				break;

			case self::TAX_CREDIT:
				$strSubsidyContractTypeName = 'Tax Credit';
				break;

			case self::HIGH_HOME:
				$strSubsidyContractTypeName = 'High home';
				break;

			case self::LOW_HOME:
				$strSubsidyContractTypeName = 'Low home';
				break;

			default:
				$strSubsidyContractTypeName = '';
				break;
		}

		return $strSubsidyContractTypeName;
	}

	/**
	 * Get subsidy rent labels by subsidy contract type
	 * @param $intSubsidyContractTypeId
	 * @return array
	 */

	public static function getRentLabelsBySubsidyContractType( $intSubsidyContractTypeId ) {
		$strTotalRent 	= 'Gross Rent';
		$strSubsidyRent = 'Contract Rent';

		if( self::HUD_SECTION_236 == $intSubsidyContractTypeId ) {
			$strTotalRent 	= 'Market Rent';
			$strSubsidyRent = 'Basic Rent';
		} elseif( self::HUD_BMIR == $intSubsidyContractTypeId ) {
			$strTotalRent 	= '110% BMIR Rent';
			$strSubsidyRent = 'BMIR Rent';
		} elseif( true === in_array( $intSubsidyContractTypeId, self::$c_arrintOperatingRentSubsidyContractTypes ) ) {
			$strTotalRent 	= 'Operating Rent';
		}

		return [ $strTotalRent, $strSubsidyRent ];
	}

	public static function getSubsidyContractTypeIdByName( $strPreviousSubsidyContractType ) {

		switch( \Psi\CStringService::singleton()->strtolower( $strPreviousSubsidyContractType ) ) {
			case 'section 8':
				$intPreviousSubsidyContractTypeId = self::HUD_SECTION_8;
				break;

			case 'rent supplement':
				$intPreviousSubsidyContractTypeId = self::HUD_RENT_SUPPLEMENT;
				break;

			case 'rap':
				$intPreviousSubsidyContractTypeId = self::HUD_RAP;
				break;

			case 'section 236':
				$intPreviousSubsidyContractTypeId = self::HUD_SECTION_236;
				break;

			case 'bmir':
				$intPreviousSubsidyContractTypeId = self::HUD_BMIR;
				break;

			case '811 pra demo':
				$intPreviousSubsidyContractTypeId = self::HUD_PRA_DEMO_811;
				break;

			case 'section 202 prac':
				$intPreviousSubsidyContractTypeId = self::HUD_SECTION_202_PRAC;
				break;

			case 'section 811 prac':
				$intPreviousSubsidyContractTypeId = self::HUD_SECTION_811_PRAC;
				break;

			case 'section 202/162 pac':
				$intPreviousSubsidyContractTypeId = self::HUD_SECTION_202_162_PAC;
				break;

			case 'tax credit':
				$intPreviousSubsidyContractTypeId = self::TAX_CREDIT;
				break;

			default:
				$intPreviousSubsidyContractTypeId = NULL;
				break;
		}

		return $intPreviousSubsidyContractTypeId;
	}

}
?>