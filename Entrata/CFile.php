<?php

use Psi\Libraries\ExternalDompdf\CDomPdf;

class CFile extends CBaseFile {
	use Psi\Libraries\Container\TContainerized;
	use Psi\Libraries\EosFoundation\TEosStoredObject;
	protected $m_intPageCount;
	protected $m_intQuoteId;
	protected $m_intApplicationId;
	protected $m_intBankAccountId;
	protected $m_intApplicantId;
	protected $m_intDocumentId;
	protected $m_intLeaseId;
	protected $m_intFileAssociationId;
	protected $m_intMasterFileId;
	protected $m_intScheduledEmailId;
	protected $m_intCustomerId;
	protected $m_intUnsignedFileSignaturesCount;
	protected $m_intCheckComponentTypeId;
	protected $m_intRequireSign;
	protected $m_intDeletedBy;
	protected $m_intGlHeaderScheduleId;
	protected $m_intResponceId;
	protected $m_intActionCount;
	protected $m_intInspectionFormLocationProblemId;
	protected $m_intInspectionFormLocationId;
	protected $m_intInspectionId;
	protected $m_intMaintenanceProblemId;
	protected $m_intEntityTypeId;
	protected $m_intCustomerDataTypeId;
	protected $m_intCampaignId;
	protected $m_intApprovedBy;
	protected $m_intSubsidyCertificationId;
	protected $m_intSubsidyHapRequestId;
	protected $m_intSpecialClaimId;
	protected $m_intSubsidyRequestId;
	protected $m_intCheckCopyFileCount;
	protected $m_intFileMetadataCompanyMediaFileId;
	protected $m_intApPaymentBatchId;
	protected $m_intApPaymentTypeId;
	protected $m_intScheduledTaskId;
	protected $m_intBudgetChangeOrderId;

	protected $m_strExtension;
	protected $m_strTempFileName;
	protected $m_strFileType;
	protected $m_strFileError;
	protected $m_strMimeType;
	protected $m_strFileTypeName;
	protected $m_strViolationDatetime;
	protected $m_strInvoiceNumber;
	protected $m_strPaymentNumber;
	protected $m_strFileTypeSystemCode;
	protected $m_strFileGenerationType;
	protected $m_strDocumentDescription;
	protected $m_strFileSignedOn;
	protected $m_strSignInitiatedOn;
	protected $m_strApprovedOn;
	protected $m_strApPayeeLocationName;
	protected $m_strFileData;
	protected $m_strPropertyName;
	protected $m_strFileMetadataUnitNumber;
	protected $m_strFileMetadataUnitNumberCache;
	protected $m_strFileMetadataPersonFirstName;
	protected $m_strFileMetadataPersonLastName;
	protected $m_strFileMetadataPersonName;
	protected $m_strInspectionProblemUnitLocation;
	protected $m_strQuoteOfferAcceptedOn;
	protected $m_strQuoteCreatedOn;
	protected $m_strLeaseTermName;
	protected $m_strMarketingName;
	protected $m_strLocaleCode;
	protected $m_strDocumentFileTypeSystemCode;
	protected $m_strSignatureAssociatedFileFileType;

	protected $m_boolIsBluemoon;
	protected $m_boolIsShowInEntrata;
	protected $m_boolUploadedFromDocStorage;
	protected $m_boolIsAllowedUserCountersign;
	protected $m_boolIsShowAttachment;
	protected $m_boolIsSftpUpload;
	protected $m_boolIsSystemMessageAttachment = false;

	protected $m_strFileMetadataBuildingName;
	protected $m_strFileMetadataFileTypeName;
	protected $m_strFileMetadataLeaseStatusTypeName;
	protected $m_strEntityName;
	protected $m_strExternalDocumentName;
	protected $m_strTempFilePath;

	protected $m_arrobjApplicants;
	protected $m_arrobjCustomers;
	/** @var  \CFileAssociation[] */
	protected $m_arrobjFileAssociations;
	/** @var  \CFileNote[] */
	protected $m_arrobjFileNotes;
	protected $m_arrobjSignedFileSignatures;
	protected $m_arrobjFiles;
	protected $m_intApBatchId;
	protected $m_intApHeaderId;
	protected $m_intApPaymentId;
	protected $m_intDocumentSubTypeId;
	protected $m_strDocumentName;
	protected $m_strCustomerName;

	protected $m_arrintApplicantIds;
	protected $m_intOrganizationId;
	protected $m_intLeaseBalance;
	protected $m_intLeaseAccommodationId;

	protected $m_strIPAddress;
	protected $m_strIdentificationExpiration;
	protected $m_strLeaseEndDate;

	protected $m_boolIsHistoricalReports;
	protected $m_intReportHistoryId;

	const ESA_VERSION_ONE = '1';
	const ESA_VERSION_TWO = '11.2013';
	const ESA_VERSION_THREE = '05.2017';
	const FILES_LOCK_TIMEOUT_MS = 60000;
	const FILES_UPLOAD_LIMIT_COUNT_FOR_API = 10;

	public function __clone() {
		if( true == isset( $this->m_jsonDetails ) ) {
			$this->m_jsonDetails = clone $this->m_jsonDetails;
		}
	}

	public function getResponceId() {
		return $this->m_intResponceId;
	}

	public function getInspectionFormLocationProblemId() {
		return $this->m_intInspectionFormLocationProblemId;
	}

	public function getInspectionFormLocationId() {
		return $this->m_intInspectionFormLocationId;
	}

	public function getSubsidyCertificationId() {
		return $this->m_intSubsidyCertificationId;
	}

	public function getSubsidyHapRequestId() {
		return $this->m_intSubsidyHapRequestId;
	}

	public function getSpecialClaimId() {
		return $this->m_intSpecialClaimId;
	}

	public function getIsSystemMessageAttachment() {
		return $this->m_boolIsSystemMessageAttachment;
	}

	public function getScheduledTaskId() {
		return $this->m_intScheduledTaskId;
	}

	public function getTempFilePath() {
		return $this->m_strTempFilePath;
	}

	/**
	 * @return mixed
	 */
	public function getSubsidyRequestId() {
		return $this->m_intSubsidyRequestId;
	}

	/**
	 * @param mixed $intSubsidyRequestId
	 */
	public function setSubsidyRequestId( $intSubsidyRequestId ) {
		$this->m_intSubsidyRequestId = $intSubsidyRequestId;
	}

	public function getActionCount() {
		return $this->m_intActionCount;
	}

	public function getCheckCopyFileCount() {
		return $this->m_intCheckCopyFileCount;
	}

	public function getMaintenanceProblemId() {
		return $this->m_intMaintenanceProblemId;
	}

	public function getInspectionId() {
		return $this->m_intInspectionId;
	}

	public function getLeaseBalance() {
		return $this->m_intLeaseBalance;
	}

	public function getIPAddress() {
		return $this->m_strIPAddress;
	}

	public function getIsHistoricalReports( ){
		return $this->m_boolIsHistoricalReports;
	}

	public function getReportHistoryId( ){
		return $this->m_intReportHistoryId;
	}

	public function getLeaseAccommodationId() {
		return $this->m_intLeaseAccommodationId;
	}

	public function setReportHistoryId( $intReportHistoryId ){
		$this->m_intReportHistoryId = $intReportHistoryId;
	}

	public function setIsHistoricalReports( $boolSetIsHistoricalReports = false ){
		$this->m_boolIsHistoricalReports = $boolSetIsHistoricalReports;
	}

	public function setIPAddress( $strIPAddress ) {
		$this->m_strIPAddress = $strIPAddress;
	}

	public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
		$this->m_intMaintenanceProblemId = $intMaintenanceProblemId;
	}

	public function setActionCount( $intActionCount ) {
		$this->m_intActionCount = $intActionCount;
	}

	public function setCheckCopyFileCount( $intCheckCopyFileCount ) {
		$this->m_intCheckCopyFileCount = $intCheckCopyFileCount;
	}

	public function setResponceId( $intResponceId ) {
		$this->m_intResponceId = $intResponceId;
	}

	public function setInspectionFormLocationProblemId( $intInspectionFormLocationProblemId ) {
		$this->m_intInspectionFormLocationProblemId = $intInspectionFormLocationProblemId;
	}

	public function setInspectionFormLocationId( $intInspectionFormLocationId ) {
		$this->m_intInspectionFormLocationId = $intInspectionFormLocationId;
	}

	public function setInspectionId( $intInspectionId ) {
		$this->m_intInspectionId = $intInspectionId;
	}

	public function setSubsidyCertificationId( $intSubsidyCertificationId ) {
		$this->m_intSubsidyCertificationId = $intSubsidyCertificationId;
	}

	public function setSubsidyHapRequestId( $intSubsidyHapRequestId ) {
		$this->m_intSubsidyHapRequestId = $intSubsidyHapRequestId;
	}

	public function setSpecialClaimId( $intSpecialClaimId ) {
		$this->m_intSpecialClaimId  = $intSpecialClaimId;
	}

	public function setEntityTypeId( $intEntityTypeId ) {
		$this->m_intEntityTypeId = $intEntityTypeId;
	}

	public function setCustomerDataTypeId( $intCustomerDataTypeId ) {
		$this->m_intCustomerDataTypeId = $intCustomerDataTypeId;
	}

	public function setFileAssociations( $arrobjFileAssociations ) {
		$this->m_arrobjFileAssociations = $arrobjFileAssociations;
	}

	public function setIsBluemoon( $boolIsBluemoon ) {
		$this->m_boolIsBluemoon = $boolIsBluemoon;
	}

	public function setDocumentName( $strDocumentName ) {
		$this->m_strDocumentName = $strDocumentName;
	}

	public function setCampaignId( $intCampaignId ) {
		$this->m_intCampaignId = $intCampaignId;
	}

	public function setQuoteId( $intQuoteId ) {
		$this->m_intQuoteId = $intQuoteId;
	}

	public function setMarketingName( $strMarketingName ) {
		$this->m_strMarketingName = $strMarketingName;
	}

	public function setIsAllowedUserCountersign( $boolIsAllowedUserCountersign ) {
		$this->m_boolIsAllowedUserCountersign = $boolIsAllowedUserCountersign;
	}

	public function setOrganizationId( $intOrganizationId ) {
		$this->m_intOrganizationId = $intOrganizationId;
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->m_strLocaleCode = $strLocaleCode;
	}

	public function setLeaseBalance( $intLeaseBalance ) {
		$this->m_intLeaseBalance = $intLeaseBalance;
	}

	public function setIsSftpUpload( $boolIsSftpUpload ) {
		$this->m_boolIsSftpUpload = $boolIsSftpUpload;
	}

	public function setApPaymentTypeId( $intApPaymentTypeId ) {
		$this->m_intApPaymentTypeId = $intApPaymentTypeId;
	}

	public function setIsSystemMessageAttachment( $boolIsSystemMessageAttachment ) {
		$this->m_boolIsSystemMessageAttachment = $boolIsSystemMessageAttachment;
	}

	public function setScheduledTaskId( $intScheduledTaskId ) {
		$this->m_intScheduledTaskId = $intScheduledTaskId;
	}

	public function setTempFilePath( $strTempFilePath ) {
		$this->m_strTempFilePath = $strTempFilePath;
	}

	public function setDetails( $jsonDetails ) {
		if( true == valObj( $jsonDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonDetails', $jsonDetails, true );
		} elseif( true == valJsonString( $jsonDetails ) ) {
			$this->set( 'm_jsonDetails', CStrings::strToJson( $jsonDetails ), true );
		} else {
			$this->set( 'm_jsonDetails', NULL, true );
		}
		unset( $this->m_strDetails );
	}

	public function setLeaseAccommodationId( $intLeaseAccommodationId ) {
		$this->m_intLeaseAccommodationId = $intLeaseAccommodationId;
	}

	/**
	 * Get Functions
	 */

	public function getIsSftpUpload() {
		return $this->m_boolIsSftpUpload;
	}

	public function getCheckComponentTypeId() {
		return $this->m_intCheckComponentTypeId;
	}

	public function getPageCount() {
		return $this->m_intPageCount;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getDocumentDescription() {
		return $this->m_strDocumentDescription;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function getFileType() {
		return $this->m_strFileType;
	}

	public function getFileTypeSystemCode() {
		return $this->m_strFileTypeSystemCode;
	}

	public function getFileTypeName() {
		return $this->m_strFileTypeName;
	}

	public function getViolationDatetime() {
		return $this->m_strViolationDatetime;
	}

	public function getInvoiceNumber() {
		return $this->m_strInvoiceNumber;
	}

	public function getMimeType() {
		return $this->m_strMimeType;
	}

	public function getPaymentNumber() {
		return $this->m_strPaymentNumber;
	}

	public function getApplicants() {
		return $this->m_arrobjApplicants;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getBankAccountId() {
		return $this->m_intBankAccountId;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function getUnsignedFileSignaturesCount() {
		return $this->m_intUnsignedFileSignaturesCount;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getFileAssociationId() {
		return $this->m_intFileAssociationId;
	}

	public function getMasterFileId() {
		return $this->m_intMasterFileId;
	}

	public function getApBatchId() {
		return $this->m_intApBatchId;
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function getApPaymentId() {
		return $this->m_intApPaymentId;
	}

	public function getRequireSign() {
		return $this->m_intRequireSign;
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function getGlHeaderScheduleId() {
		return $this->m_intGlHeaderScheduleId;
	}

	public function getEntityTypeId() {
		return $this->m_intEntityTypeId;
	}

	public function getCustomerDataTypeId() {
		return $this->m_intCustomerDataTypeId;
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function getFileSignedOn() {
		return $this->m_strFileSignedOn;
	}

	public function getSignInitiatedOn() {
		return $this->m_strSignInitiatedOn;
	}

	public function getApPayeeLocationName() {
		return $this->m_strApPayeeLocationName;
	}

	public function getFileData() {
		return $this->m_strFileData;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getFileMetadataUnitNumber() {
		return $this->m_strFileMetadataUnitNumber;
	}

	public function getFileMetadataUnitNumberCache() {
		return $this->m_strFileMetadataUnitNumberCache;
	}

	public function getFileMetadataPersonFirstName() {
		return $this->m_strFileMetadataPersonFirstName;
	}

	public function getFileMetadataPersonLastName() {
		return $this->m_strFileMetadataPersonLastName;
	}

	public function getFileMetadataPersonName() {
		return $this->m_strFileMetadataPersonName;
	}

	public function getUploadedFromDocStorage() {
		return $this->m_boolUploadedFromDocStorage;
	}

	public function getFileMetadataBuildingName() {
		return $this->m_strFileMetadataBuildingName;
	}

	public function getFileMetadataFileTypeName() {
		return $this->m_strFileMetadataFileTypeName;
	}

	public function getFileMetadataLeaseStatusTypeName() {
		return $this->m_strFileMetadataLeaseStatusTypeName;
	}

	public function getFileMetadataCompanyMediaFileId() {
		return $this->m_intFileMetadataCompanyMediaFileId;
	}

	public function getApPaymentBatchId() {
		return $this->m_intApPaymentBatchId;
	}

	public function getExtension() {
		return $this->m_strExtension;
	}

	public function getIsBluemoon() {
		return $this->m_boolIsBluemoon;
	}

	public function getDocumentName() {
		return $this->m_strDocumentName;
	}

	public function getCampaignId() {
		return $this->m_intCampaignId;
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function getEntityName() {
		return $this->m_strEntityName;
	}

	public function getExternalDocumentName() {
		return $this->m_strExternalDocumentName;
	}

	public function getAppliesToApplicantNames() {
		$strApplicantName 		= NULL;
		$arrstrApplicantNames 	= [];

		if( true == valArr( $this->m_arrobjApplicants ) ) {
			foreach( $this->m_arrobjApplicants as $objApplicant ) {
				$arrstrApplicantNames[] = $objApplicant->getNameFirst();
			}
		}

		if( true == valArr( $this->m_arrobjCustomers ) ) {
			foreach( $this->m_arrobjCustomers as $objCustomer ) {
				$arrstrApplicantNames[] = $objCustomer->getNameFirst();
			}
		}

		if( true == valArr( $arrstrApplicantNames ) ) {
			$strApplicantName = implode( ', ', $arrstrApplicantNames );
		}

		return $strApplicantName;
	}

	public function getAppliesToApplicantFullNames() {
		$strApplicantName 		= NULL;
		$arrstrApplicantNames 	= [];

		if( true == valArr( $this->m_arrobjApplicants ) ) {
			foreach( $this->m_arrobjApplicants as $objApplicant ) {
				$arrstrApplicantNames[] = $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast();
			}
		}

		if( true == valArr( $this->m_arrobjCustomers ) ) {
			foreach( $this->m_arrobjCustomers as $objCustomer ) {
				$arrstrApplicantNames[] = $objCustomer->getNameFirst() . ' ' . $objCustomer->getNameLast();
			}
		}

		if( true == valArr( $arrstrApplicantNames ) ) {
			$strApplicantName = implode( ', ', $arrstrApplicantNames );
		}

		return $strApplicantName;
	}

	public function getUnSignLeaseDocumentApplicantIds( $intLeaseLeaseIntervalTypeId ) {
		$strApplicantName 		= NULL;
		$arrintUnSignLeaseDocumentApplicantIds = [];

		if( true == valArr( $this->m_arrobjFileAssociations ) ) {
			foreach( $this->m_arrobjFileAssociations as $objFileAssociation ) {
				if( ( true == in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE, CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET, CFileType::SYSTEM_CODE_LEASE_ADDENDUM, CFileType::SYSTEM_CODE_PRE_SIGNED, CFileType::SYSTEM_CODE_LEASE_PACKET, CFileType::SYSTEM_CODE_PARTIALLY_SIGNED ] )
				      || 1 == $objFileAssociation->getRequireSign() ) && true == is_null( $objFileAssociation->getFileSignedOn() ) ) {
					if( ( true == valArr( $this->m_arrobjApplicants ) && false == valArr( $this->m_arrobjCustomers ) && false == is_null( $objFileAssociation->getApplicantId() ) ) || ( CLeaseIntervalType::APPLICATION == $intLeaseLeaseIntervalTypeId && false == is_null( $objFileAssociation->getApplicantId() ) ) ) {
						$arrintUnSignLeaseDocumentApplicantIds[] = $objFileAssociation->getApplicantId();
					} elseif( true == valArr( $this->m_arrobjCustomers ) && false == is_null( $objFileAssociation->getCustomerId() ) ) {
						$arrintUnSignLeaseDocumentApplicantIds[] = $objFileAssociation->getCustomerId();
					}
				}
			}
		}

		return $arrintUnSignLeaseDocumentApplicantIds;
	}

	public function getLeaseIntervalStatus( $intApplicationId, $objDatabase ) {
		$this->m_intLeaseStatusTypeId = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseStatusTypeIdByApplicationIdByCid( $intApplicationId, $this->getCid(), $objDatabase );
		return $this->m_intLeaseStatusTypeId;
	}

	public function getFileAssociations() {
		return $this->m_arrobjFileAssociations;
	}

	public function getSignedFileSignatures() {
		return $this->m_arrobjSignedFileSignatures;
	}

	public function getFiles() {
		return $this->m_arrobjFiles;
	}

	public function getDocumentSubTypeId() {
		return $this->m_intDocumentSubTypeId;
	}

	public function getOrFetchDocumentSubTypeId( $objDatabase ) {

		if( true == is_null( $this->m_intDocumentSubTypeId ) ) {
			$this->m_intDocumentSubTypeId = CDocuments::fetchDocumentSubTypeIdByCidByFileId( $this->getCid(), $this->getId(), $objDatabase );
		}

		return $this->m_intDocumentSubTypeId;

	}

	public function getFileGenerationType() {

		if( false == is_null( $this->m_strFileGenerationType ) && 0 < \Psi\CStringService::singleton()->strlen( trim( $this->m_strFileGenerationType ) ) ) {
			return $this->m_strFileGenerationType;
		}

		$strLeaseGenerationType = NULL;

		if( false == valArr( $this->m_arrobjFileAssociations ) ) {
			$strLeaseGenerationType = 'Upload';
			return $strLeaseGenerationType;
		}

		foreach( $this->m_arrobjFileAssociations as $objFileAssociation ) {
			if( false == is_null( $objFileAssociation->getApplicantId() ) && false == is_null( $objFileAssociation->getApplicationId() ) && $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_APPLICATION ) {
				$strLeaseGenerationType = 'Application Upload';
			} else {
				$strLeaseGenerationType = 'Upload';
				if( CFileType::SYSTEM_CODE_POLICY == $this->getFileTypeSystemCode() ) {
					$strLeaseGenerationType = 'Application: Policy';
				} elseif( CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE == $this->getFileTypeSystemCode() ) {
					$strLeaseGenerationType = 'eSign: Addenda';
				} elseif( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_OFFER_TO_RENT || $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_GUEST_CARD || $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT || $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED ) {
					$strLeaseGenerationType = 'Automated';
				} elseif( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_PRE_SIGNED ) {
					$strLeaseGenerationType = 'Pre-signed';
				} elseif( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_PARTIALLY_SIGNED ) {
					$strLeaseGenerationType = 'Partially-eSigned';
				} elseif( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_SIGNED || $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_LEASE_PACKET ) {
					$strLeaseGenerationType = 'eSign: Lease';
				} elseif( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_PARENTAL_CONSENT_FORM ) {
					$strLeaseGenerationType = 'Parental Consent Form';
				}
			}
		}

		return $strLeaseGenerationType;
	}

	public function getFileTypeToName() {

		if( false == valArr( $this->m_arrobjFileAssociations ) ) {
			return 'Upload';
		}

		switch( $this->getFileTypeSystemCode() ) {
			case CFileType::SYSTEM_CODE_LEASE_PACKET:
			case CFileType::SYSTEM_CODE_LEASE_ADDENDUM:
			case CFileType::SYSTEM_CODE_PRE_SIGNED:
			case CFileType::SYSTEM_CODE_PARTIALLY_SIGNED:
			case CFileType::SYSTEM_CODE_SIGNED:
				return 'eSign: Lease';
				break;

			case CFileType::SYSTEM_CODE_POLICY:
				return 'Application: Policy';
				break;

			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET:
				return 'eSign: Addenda';
				break;

			case CFileType::SYSTEM_CODE_OFFER_TO_RENT:
			case CFileType::SYSTEM_CODE_GUEST_CARD:
				return 'Automated';
				break;

			case CFileType::SYSTEM_CODE_APPLICATION:
				return 'Application Upload';
				break;

			case CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT:
				return 'Renewal Offer Sent';
				break;

			case CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED:
				return 'Renewal Offer Accepted';
				break;

			case CFileType::SYSTEM_CODE_UNIT_TRANSFER:
				return 'Transfer Document';
				break;

			case CFileType::SYSTEM_CODE_HUD50059:
			case CFileType::SYSTEM_CODE_HUD50059A:
				return 'HUD Certification Document';
				break;

			case CFileType::SYSTEM_CODE_HUD52670:
			case CFileType::SYSTEM_CODE_HUD52670A_PART1:
			case CFileType::SYSTEM_CODE_HUD52670A_PART2:
			case CFileType::SYSTEM_CODE_HUD52670A_PART3:
			case CFileType::SYSTEM_CODE_HUD52670A_PART4:
			case CFileType::SYSTEM_CODE_HUD52670A_PART5:
			case CFileType::SYSTEM_CODE_HUD52670A_PART6:
				return 'HAP Document';
				break;

			default:
				if( true == in_array( $this->getFileTypeSystemCode(), CFileType::$c_arrstrTaxCreditCertificationFileTypes ) ) {
					return 'Tax Credit Income Certification Document';
					break;
				}
				return 'Upload';
				break;
		}
	}

	public function getQuoteId() {
		return $this->m_intQuoteId;
	}

	public function getIsAllowedUserCountersign() {
		return $this->m_boolIsAllowedUserCountersign;
	}

	public function getInspectionProblemUnitLocation() {
		return $this->m_strInspectionProblemUnitLocation;
	}

	public function getQuoteOfferAcceptedOn() {
		return $this->m_strQuoteOfferAcceptedOn;
	}

	public function getQuoteCreatedOn() {
		return $this->m_strQuoteCreatedOn;
	}

	public function getLeaseTermName() {
		return $this->m_strLeaseTermName;
	}

	public function getMarketingName() {
		return $this->m_strMarketingName;
	}

	public function getApplicantIds() {
		return $this->m_arrintApplicantIds;
	}

	public function getFileNotes() {
		return $this->m_arrobjFileNotes;
	}

	public function getOrganizationId() {
		return $this->m_intOrganizationId;
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function getFilesFields() {

		return [
			'user_documents' => __( 'User Documents' )
		];
	}

	public function getCustomerName() {
		return $this->m_strCustomerName;
	}

	public function setCustomerName( $strCustomerName ) {
		$this->m_strCustomerName = $strCustomerName;
	}

	public function getApPaymentTypeId() {
		return $this->m_intApPaymentTypeId;
	}

	public function getIdentificationExpiration() {
		return $this->m_strIdentificationExpiration;
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function getDocumentFileTypeSystemCode() {
		return $this->m_strDocumentFileTypeSystemCode;
	}

	public function getSignatureAssociatedFileFileType() {
		return $this->m_strSignatureAssociatedFileFileType;
	}

	public function getBudgetChangeOrderId() {
		return $this->m_intBudgetChangeOrderId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrmixValues['check_component_type_id'] ) )			$this->setCheckComponentTypeId( $arrmixValues['check_component_type_id'] );
		if( true == isset( $arrmixValues['file_type_system_code'] ) ) 			$this->setFileTypeSystemCode( $arrmixValues['file_type_system_code'] );
		if( true == isset( $arrmixValues['file_generation_type'] ) ) 			$this->setFileGenerationType( $arrmixValues['file_generation_type'] );
		if( true == isset( $arrmixValues['application_id'] ) ) 					$this->setApplicationId( $arrmixValues['application_id'] );
		if( true == isset( $arrmixValues['bank_account_id'] ) ) 				$this->setBankAccountId( $arrmixValues['bank_account_id'] );
		if( true == isset( $arrmixValues['document_id'] ) ) 					$this->setDocumentId( $arrmixValues['document_id'] );
		if( true == isset( $arrmixValues['applicant_id'] ) ) 					$this->setApplicantId( $arrmixValues['applicant_id'] );
		if( true == isset( $arrmixValues['customer_id'] ) ) 					$this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['lease_id'] ) )						$this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['file_association_id'] ) )				$this->setFileAssociationId( $arrmixValues['file_association_id'] );
		if( true == isset( $arrmixValues['entity_type_id'] ) )					$this->setEntityTypeId( $arrmixValues['entity_type_id'] );
		if( true == isset( $arrmixValues['customer_data_type_id'] ) )			$this->setCustomerDataTypeId( $arrmixValues['customer_data_type_id'] );
		if( true == isset( $arrmixValues['is_bluemoon'] ) )						$this->setIsBluemoon( $arrmixValues['is_bluemoon'] );
		if( true == isset( $arrmixValues['file_type_name'] ) ) 					$this->setFileTypeName( $arrmixValues['file_type_name'] );
		if( true == isset( $arrmixValues['violation_datetime'] ) ) 				$this->setViolationDatetime( $arrmixValues['violation_datetime'] );
		if( true == isset( $arrmixValues['invoice_number'] ) ) 					$this->setInvoiceNumber( $arrmixValues['invoice_number'] );
		if( true == isset( $arrmixValues['mime_type'] ) ) 					    $this->setMimeType( $arrmixValues['mime_type'] );
		if( true == isset( $arrmixValues['payment_number'] ) ) 					$this->setPaymentNumber( $arrmixValues['payment_number'] );
		if( true == isset( $arrmixValues['check_copy_file_count'] ) ) 			$this->setCheckCopyFileCount( $arrmixValues['check_copy_file_count'] );
		if( true == isset( $arrmixValues['document_description'] ) ) 			$this->setDocumentDescription( $arrmixValues['document_description'] );
		if( true == isset( $arrmixValues['unsigned_file_signatures_count'] ) ) 	$this->setUnsignedFileSignaturesCount( $arrmixValues['unsigned_file_signatures_count'] );
		if( true == isset( $arrmixValues['require_sign'] ) )  					$this->setRequireSign( $arrmixValues['require_sign'] );
		if( true == isset( $arrmixValues['deleted_by'] ) )  					$this->setDeletedBy( $arrmixValues['deleted_by'] );
		if( true == isset( $arrmixValues['gl_header_schedule_id'] ) )  			$this->setGlHeaderScheduleId( $arrmixValues['gl_header_schedule_id'] );
		if( true == isset( $arrmixValues['file_signed_on'] ) ) 					$this->setFileSignedOn( $arrmixValues['file_signed_on'] );
		if( true == isset( $arrmixValues['sign_initiated_on'] ) ) 				$this->setSignInitiatedOn( $arrmixValues['sign_initiated_on'] );
		if( true == isset( $arrmixValues['ap_payee_location_name'] ) )			$this->setApPayeeLocationName( $arrmixValues['ap_payee_location_name'] );
		if( true == isset( $arrmixValues['ap_batch_id'] ) )						$this->setApBatchId( $arrmixValues['ap_batch_id'] );
		if( true == isset( $arrmixValues['ap_header_id'] ) )					$this->setApHeaderId( $arrmixValues['ap_header_id'] );
		if( true == isset( $arrmixValues['ap_payment_id'] ) )					$this->setApPaymentId( $arrmixValues['ap_payment_id'] );
		if( true == isset( $arrmixValues['document_sub_type_id'] ) ) 			$this->m_intDocumentSubTypeId = ( $arrmixValues['document_sub_type_id'] );
		if( true == isset( $arrmixValues['responce_id'] ) ) 					$this->m_intResponceId = ( $arrmixValues['responce_id'] );
		if( true == isset( $arrmixValues['action_count'] ) ) 					$this->m_intActionCount = ( $arrmixValues['action_count'] );
		if( true == isset( $arrmixValues['inspection_form_location_problem_id'] ) ) 			$this->m_intInspectionFormLocationProblemId = ( $arrmixValues['inspection_form_location_problem_id'] );
		if( true == isset( $arrmixValues['inspection_form_location_id'] ) ) 	$this->m_intInspectionFormLocationId = ( $arrmixValues['inspection_form_location_id'] );
		if( true == isset( $arrmixValues['maintenance_problem_id'] ) ) 			$this->m_intMaintenanceProblemId = ( $arrmixValues['maintenance_problem_id'] );
		if( true == isset( $arrmixValues['inspection_id'] ) ) 					$this->m_intInspectionId = ( $arrmixValues['inspection_id'] );
		if( true == isset( $arrmixValues['subsidy_certification_id'] ) ) 		$this->m_intSubsidyCertificationId = ( $arrmixValues['subsidy_certification_id'] );
		if( true == isset( $arrmixValues['subsidy_hap_request_id'] ) ) 			$this->m_intSubsidyHapRequestId = ( $arrmixValues['subsidy_hap_request_id'] );
		if( true == isset( $arrmixValues['special_claim_id'] ) ) 				$this->m_intSpecialClaimId = ( $arrmixValues['special_claim_id'] );
		if( true == isset( $arrmixValues['property_name'] ) ) 					$this->m_strPropertyName = ( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['file_metadata_unit_number_cache'] ) ) $this->m_strFileMetadataUnitNumberCache = ( $arrmixValues['file_metadata_unit_number_cache'] );
		if( true == isset( $arrmixValues['file_metadata_unit_number'] ) ) 		$this->m_strFileMetadataUnitNumber = ( $arrmixValues['file_metadata_unit_number'] );
		if( true == isset( $arrmixValues['file_metadata_person_first_name'] ) ) $this->m_strFileMetadataPersonFirstName = ( $arrmixValues['file_metadata_person_first_name'] );
		if( true == isset( $arrmixValues['file_metadata_person_last_name'] ) ) 	$this->m_strFileMetadataPersonLastName = ( $arrmixValues['file_metadata_person_last_name'] );
		if( true == isset( $arrmixValues['file_metadata_person_name'] ) ) 		$this->m_strFileMetadataPersonName = ( $arrmixValues['file_metadata_person_name'] );

		if( true == isset( $arrmixValues['uploaded_from_doc_storage'] ) ) 		$this->m_boolUploadedFromDocStorage = ( $arrmixValues['uploaded_from_doc_storage'] );
		if( true == isset( $arrmixValues['file_metadata_building_name'] ) ) 	$this->m_strFileMetadataBuildingName = ( $arrmixValues['file_metadata_building_name'] );
		if( true == isset( $arrmixValues['file_metadata_file_type_name'] ) ) 	$this->m_strFileMetadataFileTypeName = ( $arrmixValues['file_metadata_file_type_name'] );
		if( true == isset( $arrmixValues['file_metadata_lease_status_type_name'] ) ) 	$this->m_strFileMetadataLeaseStatusTypeName = ( $arrmixValues['file_metadata_lease_status_type_name'] );
		if( true == isset( $arrmixValues['document_name'] ) )					$this->setDocumentName( $arrmixValues['document_name'] );
		if( true == isset( $arrmixValues['campaign_id'] ) )						$this->setCampaignId( $arrmixValues['campaign_id'] );
		if( true == isset( $arrmixValues['quote_id'] ) )						$this->setQuoteId( $arrmixValues['quote_id'] );
		if( true == isset( $arrmixValues['approved_by'] ) )						$this->setApprovedBy( $arrmixValues['approved_by'] );
		if( true == isset( $arrmixValues['approved_on'] ) )						$this->setApprovedOn( $arrmixValues['approved_on'] );
		if( true == isset( $arrmixValues['master_file_id'] ) )					$this->setMasterFileId( $arrmixValues['master_file_id'] );
		if( true == isset( $arrmixValues['entity_name'] ) )						$this->setEntityName( $arrmixValues['entity_name'] );
		if( true == isset( $arrmixValues['external_document_name'] ) )			$this->setExternalDocumentName( $arrmixValues['external_document_name'] );
		if( true == isset( $arrmixValues['is_allowed_user_countersign'] ) )		$this->setIsAllowedUserCountersign( $arrmixValues['is_allowed_user_countersign'] );
		if( true == isset( $arrmixValues['inspection_problem_unit_location'] ) ) $this->setInspectionProblemUnitLocation( $arrmixValues['inspection_problem_unit_location'] );
		if( true == isset( $arrmixValues['file_metadata_company_media_file_id'] ) )		$this->setFileMetadataCompanyMediaFileId( $arrmixValues['file_metadata_company_media_file_id'] );
		if( true == isset( $arrmixValues['quote_offer_accepted_on'] ) )		            $this->setQuoteOfferAcceptedOn( $arrmixValues['quote_offer_accepted_on'] );
		if( true == isset( $arrmixValues['quote_created_on'] ) )                        $this->setQuoteCreatedOn( $arrmixValues['quote_created_on'] );
		if( true == isset( $arrmixValues['lease_term_name'] ) )		                    $this->setLeaseTermName( $arrmixValues['lease_term_name'] );
		if( true == isset( $arrmixValues['marketing_name'] ) )		                    $this->setMarketingName( $arrmixValues['marketing_name'] );
		if( true == isset( $arrmixValues['applicant_ids'] ) )		                    $this->setApplicantIds( $arrmixValues['applicant_ids'] );
		if( true == isset( $arrmixValues['ap_payment_batch_id'] ) )						$this->setApPaymentBatchId( $arrmixValues['ap_payment_batch_id'] );
		if( true == isset( $arrmixValues['extension'] ) )								$this->setExtension( $arrmixValues['extension'] );
		if( true == isset( $arrmixValues['organization_id'] ) )							$this->setOrganizationId( $arrmixValues['organization_id'] );
		if( true == isset( $arrmixValues['locale_code'] ) )								$this->setLocaleCode( $arrmixValues['locale_code'] );
		if( true == isset( $arrmixValues['customer_name'] ) )							$this->setCustomerName( $arrmixValues['customer_name'] );
		if( true == isset( $arrmixValues['ip_address'] ) ) 								$this->setIPAddress( $arrmixValues['ip_address'] );
		if( true == isset( $arrmixValues['identification_expiration'] ) ) 				$this->setIdentificationExpiration( $arrmixValues['identification_expiration'] );
		if( true == isset( $arrmixValues['lease_end_date'] ) ) 						    $this->setLeaseEndDate( $arrmixValues['lease_end_date'] );
        if( true == isset( $arrmixValues['budget_change_order_id'] ) ) 					$this->setBudgetChangeOrderId( $arrmixValues['budget_change_order_id'] );
		return;
	}

	public function setCheckComponentTypeId( $intCheckComponentTypeId ) {
		$this->m_intCheckComponentTypeId = CStrings::strToIntDef( $intCheckComponentTypeId, NULL, false );
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->m_intApprovedBy = CStrings::strToIntDef( $intApprovedBy, NULL, false );
	}

	public function setPageCount( $intPageCount ) {
		$this->m_intPageCount = $intPageCount;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setDocumentDescription( $strDocumentDescription ) {
		$this->m_strDocumentDescription = $strDocumentDescription;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function setFileType( $strFileType ) {
		$this->m_strFileType = $strFileType;
	}

	public function setFileTypeName( $strFileTypeName ) {
		$this->m_strFileTypeName = $strFileTypeName;
	}

	public function setViolationDatetime( $strViolationDatetime ) {
		$this->m_strViolationDatetime = $strViolationDatetime;
	}

	public function setInvoiceNumber( $strInvoiceNumber ) {
		$this->m_strInvoiceNumber = $strInvoiceNumber;
	}

	public function setMimeType( $strMimeType ) {
		$this->m_strMimeType = $strMimeType;
	}

	public function setPaymentNumber( $strPaymentNumber ) {
		$this->m_strPaymentNumber = $strPaymentNumber;
	}

	public function setFileTypeSystemCode( $strFileTypeSystemCode ) {
		$this->m_strFileTypeSystemCode = $strFileTypeSystemCode;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setBankAccountId( $intBankAccountId ) {
		$this->m_intBankAccountId = $intBankAccountId;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setFileAssociationId( $intFileAssociationId ) {
		$this->m_intFileAssociationId = $intFileAssociationId;
	}

	public function setMasterFileId( $intMasterFileId ) {
		$this->m_intMasterFileId = $intMasterFileId;
	}

	public function setApBatchId( $intApBatchId ) {
		$this->m_intApBatchId = $intApBatchId;
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->m_intApHeaderId = $intApHeaderId;
	}

	public function setApPaymentId( $intApPaymentId ) {
		$this->m_intApPaymentId = $intApPaymentId;
	}

	public function setRequireSign( $intRequireSign ) {
		$this->m_intRequireSign = $intRequireSign;
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->m_intDeletedBy = CStrings::strToIntDef( $intDeletedBy, NULL, false );
	}

	public function setGlHeaderScheduleId( $intGlHeaderScheduleId ) {
		$this->m_intGlHeaderScheduleId = $intGlHeaderScheduleId;
	}

	public function setFileSignedOn( $strFileSignedOn ) {
		$this->m_strFileSignedOn = $strFileSignedOn;
	}

	public function setSignInitiatedOn( $strSignInitiatedOn ) {
		$this->m_strSignInitiatedOn = $strSignInitiatedOn;
	}

	public function setApPayeeLocationName( $strApPayeeLocationName ) {
		$this->m_strApPayeeLocationName = CStrings::strTrimDef( $strApPayeeLocationName, 100, NULL, true );
	}

	public function setFileGenerationType( $strFileGenerationType ) {
		$this->m_strFileGenerationType = $strFileGenerationType;
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->m_intScheduledEmailId = $intScheduledEmailId;
	}

	public function setUnsignedFileSignaturesCount( $intUnsignedFileSignaturesCount ) {
		$this->m_intUnsignedFileSignaturesCount = $intUnsignedFileSignaturesCount;
	}

	public function setApplicants( $arrobjApplicants ) {
		$this->m_arrobjApplicants = $arrobjApplicants;
	}

	public function setDocumentSubTypeId( $intDocumentSubTypeId ) {
		$this->m_intDocumentSubTypeId = CStrings::strToIntDef( $intDocumentSubTypeId, NULL, false );
	}

	public function setFileData( $strFileData ) {
		$this->m_strFileData = $strFileData;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setFileMetadataUnitNumber( $strFileMetadataUnitNumber ) {
		$this->m_strFileMetadataUnitNumber = $strFileMetadataUnitNumber;
	}

	public function setFileMetadataUnitNumberCache( $strFileMetadataUnitNumberCache ) {
		$this->m_strFileMetadataUnitNumberCache = $strFileMetadataUnitNumberCache;
	}

	public function setFileMetadataPersonFirstName( $strFileMetadataPersonFirstName ) {
		$this->m_strFileMetadataPersonFirsttName = $strFileMetadataPersonFirstName;
	}

	public function setFileMetadataPersonLastName( $strFileMetadataPersonLastName ) {
		$this->m_strFileMetadataPersonLastName = $strFileMetadataPersonLastName;
	}

	public function setFileMetadataPersonName( $strFileMetadataPersonName ) {
		$this->m_strFileMetadataPersonName = $strFileMetadataPersonName;
	}

	public function setUploadedFromDocStorage( $intUploadedFromDocStorage ) {
		$this->m_boolUploadedFromDocStorage = $intUploadedFromDocStorage;
	}

	public function setFileMetadataBuildingName( $strFileMetadataBuildingName ) {
		$this->m_strFileMetadataBuildingName = $strFileMetadataBuildingName;
	}

	public function setFileMetadataFileTypeName( $strFileMetadataFileTypeName ) {
		$this->m_strFileMetadataFileTypeName = $strFileMetadataFileTypeName;
	}

	public function setFileMetadataLeaseStatusTypeName( $strFileMetadataLeaseStatusTypeName ) {
		$this->m_strFileMetadataLeaseStatusTypeName = $strFileMetadataLeaseStatusTypeName;
	}

	public function setFileMetadataCompanyMediaFileId( $intFileMetadataCompanyMediaFileId ) {
		$this->m_intFileMetadataCompanyMediaFileId = $intFileMetadataCompanyMediaFileId;
	}

	public function setApPaymentBatchId( $intApPaymentBatchId ) {
		$this->m_intApPaymentBatchId = $intApPaymentBatchId;
	}

	public function setExtension( $strExtension ) {
		$this->m_strExtension = $strExtension;
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->m_strApprovedOn = $strApprovedOn;
	}

	public function setEntityName( $strEntityName ) {
		$this->m_strEntityName = $strEntityName;
	}

	public function setExternalDocumentName( $strExternalDocumentName ) {
		$this->m_strExternalDocumentName = $strExternalDocumentName;
	}

	public function setFileSigningStatus() {
		$strSignInitiatedOn = NULL;
		$strFileSignedOn 	= NULL;
		$boolFileSigned 	= true;

		if( true == valArr( $this->getFileAssociations() ) ) {

			foreach( $this->getFileAssociations() as $objFileAssociation ) {

				if( in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_LEASE_PACKET, CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET ] ) == $this->getFileTypeSystemCode() && true == is_null( $objFileAssociation->getCustomerId() ) && true == is_null( $objFileAssociation->getApplicantId() ) ) {
					$this->setSignInitiatedOn( $objFileAssociation->getSignInitiatedOn() );
					$this->setFileSignedOn( $objFileAssociation->getFileSignedOn() );
					return;
				} else {
					if( true == is_null( $strSignInitiatedOn ) || ( false == is_null( $objFileAssociation->getSignInitiatedOn() ) && ( strtotime( $strSignInitiatedOn ) > strtotime( $objFileAssociation->getSignInitiatedOn() ) ) ) ) {
						$strSignInitiatedOn = $objFileAssociation->getSignInitiatedOn();
					}

					if( true == is_null( $objFileAssociation->getFileSignedOn() ) || 0 == strlen( trim( $objFileAssociation->getFileSignedOn() ) ) ) {
						$boolFileSigned = false;
					}

					if( strtotime( $strFileSignedOn ) < strtotime( $objFileAssociation->getFileSignedOn() ) ) {
						$strFileSignedOn = $objFileAssociation->getFileSignedOn();
					}
				}
			}
		}

		$this->setSignInitiatedOn( $strSignInitiatedOn );
		( false == $boolFileSigned ) ? $this->setFileSignedOn( NULL ) : $this->setFileSignedOn( $strFileSignedOn );
	}

	public function setInspectionProblemUnitLocation( $strInspectionProblemUnitLocation ) {
		return $this->m_strInspectionProblemUnitLocation = $strInspectionProblemUnitLocation;
	}

	public function setQuoteOfferAcceptedOn( $strQuoteOfferAcceptedOn ) {
		$this->m_strQuoteOfferAcceptedOn = $strQuoteOfferAcceptedOn;
	}

	public function setQuoteCreatedOn( $strQuoteCreatedOn ) {
		$this->m_strQuoteCreatedOn = $strQuoteCreatedOn;
	}

	public function setLeaseTermName( $strLeaseTermName ) {
		$this->m_strLeaseTermName = $strLeaseTermName;
	}

	public function setApplicantIds( $arrintApplicantIds ) {
		$this->m_arrintApplicantIds = $arrintApplicantIds;
	}

	public function setIsShowAttachment( $boolIsShowAttachment ) {
		$this->m_boolIsShowAttachment = $boolIsShowAttachment;
	}

	public function setIdentificationExpiration( $strIdentificationExpiration ) {
		$this->m_strIdentificationExpiration = $strIdentificationExpiration;
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->m_strLeaseEndDate = $strLeaseEndDate;
	}

	// This is a system code as of the documents table record

	public function setDocumentFileTypeSystemCode( $strDocumentFileTypeSystemCode ) {
		$this->m_strDocumentFileTypeSystemCode = $strDocumentFileTypeSystemCode;
	}

	public function setSignatureAssociatedFileFileType( $strSignatureAssociatedFileFileType ) {
		$this->m_strSignatureAssociatedFileFileType = $strSignatureAssociatedFileFileType;
	}

	public function setBudgetChangeOrderId( $intBudgetChangeOrderId ) {
		$this->m_intBudgetChangeOrderId = $intBudgetChangeOrderId;
	}

	/**
	 * Add Functions
	 */

	public function addApplicant( $objApplicant ) {
		$this->m_arrobjApplicants[$objApplicant->getId()] = $objApplicant;
	}

	public function addCustomer( $objCustomer ) {
		$this->m_arrobjCustomers[$objCustomer->getId()] = $objCustomer;
	}

	public function addFileAssociation( $objFileAssociation ) {
		$this->m_arrobjFileAssociations[$objFileAssociation->getId()] = $objFileAssociation;
	}

	public function addSignedFileSignature( $objFileSignature ) {
		$this->m_arrobjSignedFileSignatures[$objFileSignature->getId()] = $objFileSignature;
	}

	public function addFile( $objFile ) {
		$this->m_arrobjFiles[$objFile->getId()] = $objFile;
	}

	public function addFileNote( $objFileNote ) {
		$this->m_arrobjFileNotes[] = $objFileNote;
	}

	/**
	 * Validate Functions
	 */

	public function valFileTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_type_id', __( 'File type id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Please select a file to upload.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Please select a file to upload.' ) ) );
		} elseif( 240 <= \Psi\CStringService::singleton()->strlen( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'File name should be less than 240 characters.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;

		if( true == is_null( $this->getFilePath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', __( 'File path is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSignDocument() {

		if( false == in_array( $this->getFileType(), [ 'application/msword', 'application/pdf' ] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Only .PDF and .DOCX documents are supported at this time. Please convert your document accordingly and re-upload.' ) ) );
			return false;
		}

		// allowed for 50mb
		if( isset( $_SERVER['CONTENT_LENGTH'] ) && $_SERVER['CONTENT_LENGTH'] > 52428800 ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Your document must be no larger than 50MB. Please modify your document accordingly and re-upload.' ) ) );
			return false;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload properly. Files cannot be larger than {%s,0}B.', [ PHP_MAX_POST ] ) ) );
			return false;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded , Missing a temporary folder.' ) ) );
			return false;
		}

		if( true == is_null( $this->getTempFileName() ) || false == CFileIo::fileExists( $this->getTempFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid.' ) ) );
			return false;
		}

		if( 'application/msword' == $this->getFileType() ) {
			return $this->valDocxDocument();

		} else {
			return $this->valPdfDocument();

		}

		return true;
	}

	public function valSignedDocument() {

		$arrstrImageFileType = [ 'application/pdf', 'image/gif', 'image/pjpeg', 'image/jpeg', 'image/jpg', 'image/png', 'image/bmp' ];

		if( false == in_array( $this->getFileType(), $arrstrImageFileType ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Only PDF and Image documents are supported at this time. Please convert your document accordingly and re-upload.' ) ) );
			return false;
		}

		if( isset( $_SERVER['CONTENT_LENGTH'] ) && $_SERVER['CONTENT_LENGTH'] > 20000000 ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Your document must be no larger than 20MB. Please modify your document accordingly and re-upload.' ) ) );
			return false;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload properly. Files cannot be larger than {%s,0}B.', [ PHP_MAX_POST ] ) ) );
			return false;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded , Missing a temporary folder.' ) ) );
			return false;
		}

		if( true == is_null( $this->getTempFileName() ) || false == CFileIo::fileExists( $this->getTempFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid.' ) ) );
			return false;
		}

		return true;
	}

	public function valIdentificationDocument() {

		$arrstrImageFileType = [ 'application/pdf', 'image/gif', 'image/pjpeg', 'image/jpeg', 'image/jpg', 'image/png', 'image/bmp' ];

		if( false == in_array( $this->getFileType(), $arrstrImageFileType ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Only PDF and Image documents are supported at this time. Please convert your document accordingly and re-upload.' ) ) );
			return false;
		}

		if( isset( $_SERVER['CONTENT_LENGTH'] ) && $_SERVER['CONTENT_LENGTH'] > 20000000 ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Your document must be no larger than 20MB. Please modify your document accordingly and re-upload.' ) ) );
			return false;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload properly. Files cannot be larger than {%s,0}B.', [ PHP_MAX_POST ] ) ) );
			return false;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded , Missing a temporary folder.' ) ) );
			return false;
		}

		if( true == is_null( $this->getTempFileName() ) || false == CFileIo::fileExists( $this->getTempFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid.' ) ) );
			return false;
		}

		return true;
	}

	public function valDocxDocument() {

		$resZip = zip_open( $this->getTempFileName() );

		if( !$resZip || is_numeric( $resZip ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid docx file.' ) ) );
			return false;
		}

		$boolValidDocx = false;

		while( $resZipEntry = zip_read( $resZip ) ) {

			// As .emf image file containing .docx file creates converted .pdf heavy in size, we have added validation. Once we get way to make it workable, will remove this validation. [CKS/NNV]
			if( 'emf' == \Psi\CStringService::singleton()->strtolower( pathinfo( ( string ) zip_entry_name( $resZipEntry ), PATHINFO_EXTENSION ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Your document contains at least one .EMF image. Such images are not supported and create unworkable .PDF documents. Please replace your .EMF with a supported image type (e.g., .JPG, .PNG, .BMP, .GIF) and re-upload your document' ) ) );
				return false;
			}

			if( false == zip_entry_open( $resZip, $resZipEntry ) ) continue;

			if( 'word/document.xml' == zip_entry_name( $resZipEntry ) ) {
				$boolValidDocx = true;
				continue;
			}
			zip_entry_close( $resZipEntry );
		}

		if( true == $boolValidDocx ) {
			return true;
		}

		$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid docx file.' ) ) );

		return false;

	}

	public function valPdfDocument() {

		$resFile = CFileIo::fileOpen( $this->getTempFileName(), 'rb' );

		if( true != $resFile ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not valid pdf file.' ) ) );
			return false;
		}

		return true;
	}

	public function valLienWaiverFileType() {
		$boolIsValid = true;

		if( 'pdf' != \Psi\CStringService::singleton()->strtolower( pathinfo( $this->getFileName(), PATHINFO_EXTENSION ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please select a PDF file to upload.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFileSize() {

		if( 0 === ( $this->getFileSize() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Uploaded file is empty.' ) ) );
			return false;
		}

		if( 52428800 <= ( $this->getFileSize() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'The attachment size exceeds the allowable limit of 50MB.' ) ) );
			return false;
		}

		return true;
	}

	public function valMimeType() {
		$arrstrMimeTypes	= [ '', 'application/octet-stream', 'application/msword', 'application/ms-word', 'application/pdf', 'application/x-pdf', 'application/download', 'application/x-download', 'application/force-download', 'application/x-msdownload', 'file/x-msdownload', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/plain', 'image/jpeg', 'image/gif', 'image/png', 'image/x-png', 'image/x-citrix-png', 'image/x-citrix-jpeg', 'image/pjpeg', 'text/html' ];

		if( true == valStr( $this->getMimeType() ) && false == in_array( $this->getMimeType(), $arrstrMimeTypes ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'The file format is not a supported.' ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $intFileTypeId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				if( CFileType::SYSTEM_CODE_LIEN_WAIVER != $intFileTypeId ) {
					$boolIsValid &= $this->valFileName();
				}
				$boolIsValid &= $this->valFilePath();
				$boolIsValid &= $this->valFileTypeId();
				if( CFileType::SYSTEM_CODE_LIEN_WAIVER == $intFileTypeId ) {
					$boolIsValid &= $this->valLienWaiverFileType();
				}
				break;

			case 'upload_file':
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valFileTypeId();
				$boolIsValid &= $this->valFileSize();
				break;

			case 'bank_account_signature_file':
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valFilePath();
				break;

			case 'upload_po_file':
				$boolIsValid &= $this->valFileName();
				break;

			case 'upload_scheduled_gl_entry_file':
			case 'upload_gl_entry_file':
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valFileSize();
				break;

			case 'valid_upload_sign_document':
				$boolIsValid &= $this->valSignDocument();
				break;

			case 'valid_upload_signed_document':
				$boolIsValid &= $this->valSignedDocument();
				break;

			case 'valid_identification_document':
				$boolIsValid &= $this->valIdentificationDocument();
				break;

			case 'upload_gl_reconcile_file':
				$boolIsValid &= $this->valFileName();
				break;

			case 'prospect_portal_upload_files':
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valFileTypeId();
				$boolIsValid &= $this->valFileSize();
				$boolIsValid &= $this->valMimeType();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function generateFile( $strContents, $strFolderPath = PATH_MOUNTS_FILES, $boolIsIntegrated = false ) {

		$strFilePath = $this->getFullFilePath( $strFolderPath );
		$strFileName = $this->getFileName();

		$boolIsValid = true;

		if( true == $boolIsIntegrated && false == CFileIo::fileExists( $strFilePath . $strFileName ) ) {

			if( true == CFileIo::recursiveMakeDir( $strFilePath ) ) {

				$strFileName = $strFilePath . $strFileName;
				CFileIo::filePutContents( $strFileName, $strContents );
			}

		} elseif( false == CFileIo::fileExists( $strFilePath . $strFileName ) ) {

			CFileIo::recursiveMakeDir( $strFilePath );

			$strFileName = $strFilePath . $strFileName;

			if( false === CFileIo::filePutContents( $strFileName, stripslashes( $strContents ) ) ) {
				trigger_error( __( 'Couldn\'t write in to file( {%s,0} ).', [ $strContents ] ), E_USER_ERROR );
				return false;
			}

			return true;
		}

		return $boolIsValid;
	}

	public function copyFile( $objCopyFile ) {

		// TODO: This function is going to be deprecated, Please use copyObject() with IObjectStorageGateway.

		$boolIsValid = true;

		$strCopyPath = $objCopyFile->getFullFilePath();

		if( false == CFileIo::fileExists( $this->getFullFilePath() . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No source file exists.' ) ) );
			return $boolIsValid &= false;
		}

		if( false == CFileIo::isDirectory( $strCopyPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strCopyPath ) ) {
				$this->addErrorMsg( new ErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				$boolIsValid &= false;
			}
		}

		if( false == CFileIo::copyFile( $this->getFullFilePath() . $this->getFileName(), $objCopyFile->getFullFilePath() . $objCopyFile->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File could not copied.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function copyObject( $objObjectStorageGateway, $objCopyFile, $strFolderPath = PATH_MOUNTS_FILES, $boolEncrypt = true ) {

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );
			return false;
		}

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( [
			'cid'             => $this->getCid(),
			'objectId'        => $this->getId(),
			'container'       => $strFolderPath,
			'key'             => $this->getFilePath() . $this->getFileName()
		] );

		if( false == $objObjectStorageGatewayResponse->hasErrors() ) {

			$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( [
				'cid'       => $objCopyFile->getCid(),
				'objectId'  => $objCopyFile->getId(),
				'container' => $strFolderPath,
				'key'       => $objCopyFile->getFilePath() . $objCopyFile->getFileName(),
				'data'      => $objObjectStorageGatewayResponse['data'],
				'noEncrypt' => !$boolEncrypt
			] );
		}

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File could not copied.' ) ) );
			return false;
		}

		return true;
	}

	public function generateEncryptedObject( $objObjectStorageGateway, $strContents, $strFolderPath = PATH_MOUNTS_FILES, $boolEncrypt = true ) {

		// TODO: This function is going to be deprecated as we do not need to pass noEncrypt parameter.

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Couldn\'t create file( {%s,0} ).', [ $this->getFilePath() . $this->getFileName() ] ) ) );
			return false;

		}

		$intId = ( true == valId( $this->getId() ) ) ? $this->getId() : -1;

		$objObjectStorageGatewayResponseFileExist = $objObjectStorageGateway->getObject( [
			'cid'         => $this->getCid(),
			'objectId'    => $intId,
			'container'   => $strFolderPath,
			'key'         => $this->getFilePath() . $this->getFileName(),
			'checkExists' => true
		] );

		if( true == $objObjectStorageGatewayResponseFileExist['isExists'] ) {
			return true;
		}

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( [
			'cid'        => $this->getCid(),
			'objectId'   => $intId,
			'container'  => $strFolderPath,
			'key'        => $this->getFilePath() . $this->getFileName(),
			'data'       => $strContents,
			'noEncrypt' => !$boolEncrypt
		] );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to write document' ) ) );
			return false;
		}

		$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );

		return true;

	}

	public function uploadFile( $strFolderName = PATH_MOUNTS_FILES ) {

		// TODO: This function is going to be deprecated, Please use uploadObject() with IObjectStorageGateway.

		$boolIsValid = true;

		$strUploadPath = $this->getFullFilePath( $strFolderName );

		if( false == CFileIo::isDirectory( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				$boolIsValid = false;
			}
		}

		if( false == move_uploaded_file( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function uploadObject( $objObjectStorageGateway, $boolEncrypt = true ) {

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );
			return false;
		}

		$arrmixRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $this->getTempFileName() ), 'noEncrypt' => !$boolEncrypt ] );
		if( false == is_null( $this->getFilePath() ) && false == is_null( $this->getFileName() ) ) {

			$objResponse = $objObjectStorageGateway->putObject( $arrmixRequest );

			if( true == $objResponse->hasErrors() ) {
				trigger_error( __( 'Failed to upload file' ), E_USER_WARNING );
				return false;
			}

			$this->setObjectStorageGatewayResponse( $objResponse );
			return true;
		}

		return false;

	}

	public function encryptUploadedFile( $objDocumentManager, $strFolderName = PATH_MOUNTS_FILES, $boolEncrypt = true ) {

		// TODO: This function is going to be deprecated, Please use encryptUploadedObject() with IObjectStorageGateway.

		$strUploadPath = CDocumentManagerUtils::getTemporaryPath( PATH_MOUNTS_FILES, $this->getCid() );

		if( false == CFileIo::isDirectory( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				return false;
			}
		}

		if( false == move_uploaded_file( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );
			return false;
		}

		$strFileContent = CFileIo::fileGetContents( $strUploadPath . $this->getFileName() );

		if( false == $objDocumentManager->putDocument( CDocumentCriteria::forPut( $this->getId(), $strFolderName, $this->getFilePath() . $this->getFileName(), $strFileContent, $boolEncrypt ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt document' ) ) );
			return false;
		}

		return true;
	}

	public function encryptUploadedObject( $objObjectStorageGateway, $strFolderName = PATH_MOUNTS_FILES, $boolEncrypt = true ) {

		$strUploadPath = $objObjectStorageGateway->calcTemporaryPath( PATH_MOUNTS_FILES, $this->getCid() );

		if( false == CFileIo::isDirectory( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				return false;
			}
		}

		if( false == move_uploaded_file( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );
			return false;
		}

		$strFileContent = CFileIo::fileGetContents( $strUploadPath . $this->getFileName() );
		CFileIo::deleteFile( $strUploadPath . $this->getFileName() );

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( [
			'cid'       => $this->getCid(),
			'objectId'  => $this->getId(),
			'container' => $strFolderName,
			'key'       => $this->getFilePath() . $this->getFileName(),
			'data'      => $strFileContent,
			'noEncrypt' => !$boolEncrypt
		] );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt document' ) ) );
			return false;
		}

		return true;
	}

	public function deleteFile( $strFolderPath = PATH_MOUNTS_FILES ) {

		// TODO: This function is going to be deprecated, Please use deleteObject() with IObjectStorageGateway.

		if( false == is_null( $this->getFilePath() ) && false == is_null( $this->getFileName() ) ) {
			$arrstrFileExtensionsToDelete = [ '.tif', '.png', '.doc', '.docx', '.jpg', '.jpeg', '.bmp', '.pdf','BMP', 'GIF', 'JPE', 'ICO', 'TXT', 'TIFF', 'PSD', 'PDF' ];
			$arrstrFileNameExtension = explode( '.', $this->getFileName() );

			$arrstrFileExtensionsToDelete = [ '.tif', '.png', '.doc', '.docx', '.jpg', '.jpeg', '.bmp', '.pdf','BMP', 'GIF', 'JPE', 'ICO', 'TXT', 'TIFF', 'PSD', 'PDF' ];

			$arrstrFileNameExtension = explode( '.', $this->getFileName() );

			foreach( $arrstrFileExtensionsToDelete as $strFileExtension ) {
				if( CFileIo::fileExists( $this->getFullFilePath( $strFolderPath ) . $arrstrFileNameExtension[0] . $strFileExtension ) ) {
					unlink( $this->getFullFilePath( $strFolderPath ) . $arrstrFileNameExtension[0] . $strFileExtension );
				}
			}
		}
	}

	public function deleteEncryptedFile( $objDocumentManager, $strFolderPath = NULL ) {

		// TODO: This function is going to be deprecated, Please use deleteObject() with IObjectStorageGateway.

		if( true == is_null( $this->getFilePath() ) || true == is_null( $this->getFileName() ) ) return true;

		if( false == valObj( $objDocumentManager, 'IDocumentManager' ) ) {
			trigger_error( __( 'Failed to get IDocumentManager' ), E_USER_WARNING );

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Couldn\'t delete file( {%s,0} ).', [ $this->getFilePath() . $this->getFileName() ] ) ) );
			return false;

		}

		$arrstrFileExtensionsToDelete = [ '.tif', '.png', '.doc', '.docx', '.jpg', '.jpeg', '.bmp', '.pdf' ];

		$arrstrFileNameExtension 	= explode( '.', $this->getFileName() );
		$strFileExtension 			= '.' . $arrstrFileNameExtension[1];

		if( false == in_array( $strFileExtension, $arrstrFileExtensionsToDelete ) ) return true;

		if( false == $objDocumentManager->removeDocument( CDocumentCriteria::forRemove( $this->getId(), $strFolderPath, $this->getFilePath() . $this->getFileName() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to remove the file.' ) ) );
			return false;
		}

	}

	/** @deprecated Use the following pattern instead:
	 * $arrmixRequest = $objFile->fetchStoredObject( $objDatabase...)->createGatewayRequest(...);
	 * $objGateway->deleteObject( $arrmixRequest );
	 * $objFile->deleteStoredObject( $intCurrentUserId, $objDatabase, NULL...);
	 */
	public function deleteObject( $objObjectStorageGateway, $strFolderPath = PATH_MOUNTS_FILES ) {
		if( true == is_null( $this->getFilePath() ) || true == is_null( $this->getFileName() ) ) return false;

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Couldn\'t delete file( {%s,0} ).', [ $this->getFilePath() . $this->getFileName() ] ) ) );
			return false;
		}

		$arrstrFileExtensionsToDelete   = [ 'tif', 'png', 'doc', 'docx', 'jpg', 'jpeg', 'bmp', 'pdf','BMP', 'GIF', 'JPE', 'ICO', 'TXT', 'TIFF', 'PSD', 'PDF' ];
		$arrstrFilePathInfo             = pathinfo( $this->getFileName() );

		if( false == in_array( $arrstrFilePathInfo['extension'], $arrstrFileExtensionsToDelete ) ) return false;

		$objDeleteObjectResponse = $objObjectStorageGateway->deleteObject( [
			'cid'             => $this->getCid(),
			'objectId'        => $this->getId(),
			'container'       => $strFolderPath,
			'key'             => $this->getFilePath() . $this->getFileName()
		] );

		if( true == valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway ' ) && true == $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to remove the file.' ) ) );
			return false;
		}

		return true;

	}

	public function getFileParentFolderName( $strFileTypeSystemCode = NULL ) {

		$strFileTypeSystemCode = ( false == is_null( $strFileTypeSystemCode ) ) ?  $strFileTypeSystemCode : $this->getFileTypeSystemCode();

		switch( $strFileTypeSystemCode ) {
			case CFileType::SYSTEM_CODE_PRE_SIGNED:
			case CFileType::SYSTEM_CODE_PARTIALLY_SIGNED:
			case CFileType::SYSTEM_CODE_SIGNED:
			case CFileType::SYSTEM_CODE_LEASE_ADDENDUM:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE:
			case CFileType::SYSTEM_CODE_LEASE_ESA:
			case CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT:
			case CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET:
			case CFileType::SYSTEM_CODE_LEASE_PACKET:
			case CFileType::SYSTEM_CODE_UNIT_TRANSFER:
			case CFILEType::SYSTEM_CODE_HUD50059:
			case CFILEType::SYSTEM_CODE_HUD50059A:
			case CFILEType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE:
			case CFILEType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE:
			case CFILEType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE:
			case CFILEType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE:
			case CFILEType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE:
			case CFILEType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE:
			case CFILEType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE:
			case CFILEType::SYSTEM_CODE_PARENTAL_CONSENT_FORM:
			case CFileType::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID:
			case CFileType::SYSTEM_CODE_FLORIDA_TIC:
			case CFileType::SYSTEM_CODE_HUD91067:
			case CFileType::SYSTEM_CODE_TEXAS_TIC:
			case CFileType::SYSTEM_CODE_MICHIGAN_TIC:
			case CFileType::SYSTEM_CODE_TENNESSEE_TIC:
			case CFileType::SYSTEM_CODE_OREGON_TIC:
			case CFileType::SYSTEM_CODE_MASSACHUSETTS_TIC:
			case CFileType::SYSTEM_CODE_FLORIDA_TIC:
			case CFileType::SYSTEM_CODE_NEWJERSEY_TIC:
			case CFileType::SYSTEM_CODE_ARKANSAS_TIC:
			case CFileType::SYSTEM_CODE_WASHINGTON_TIC:
			case CFileType::SYSTEM_CODE_VIRGINIA_TIC:
			case CFileType::SYSTEM_CODE_GEORGIA_TIC:
			case CFileType::SYSTEM_CODE_MARYLAND_TIC:
			case CFileType::SYSTEM_CODE_CALIFORNIA_TIC:
			case CFileType::SYSTEM_CODE_RHODE_ISLAND_TIC:
			case CFileType::SYSTEM_CODE_OKLAHOMA_TIC:
			case CFileType::SYSTEM_CODE_LOUISIANA_TIC:
			case CFileType::SYSTEM_CODE_KENTUCKY_TIC:
			case CFileType::SYSTEM_CODE_MISSISSIPPI_TIC:
			case CFileType::SYSTEM_CODE_MISSOURI_TIC:
			case CFileType::SYSTEM_CODE_MISSOURI_EXHIBIT_IUNIT_TIC:
			case CFileType::SYSTEM_CODE_SOUTH_CAROLINA_TIC:
			case CFileType::SYSTEM_CODE_CALIFORNIA2_TIC:
			case CFileType::SYSTEM_CODE_TEXAS_STUDENT_CERTIFICATION_TIC:
			case CFileType::SYSTEM_CODE_ILLINOIS_TIC:
			case CFileType::SYSTEM_CODE_NEVADA_TIC:
			case CFileType::SYSTEM_CODE_CONNECTICUT_TIC:
			case CFileType::SYSTEM_CODE_NORTH_CAROLINA_TIC:
			case CFileType::SYSTEM_CODE_PENNSYLVANIA_TIC:
			case CFileType::SYSTEM_CODE_IDAHO_TIC:
			case CFileType::SYSTEM_CODE_VERMONT_TIC:
			case CFileType::SYSTEM_CODE_NEW_HAMPSHIRE_TIC:
			case CFileType::SYSTEM_CODE_OHIO_TIC:
			case CFileType::SYSTEM_CODE_NEW_YORK_CITY_TIC:
			case CFileType::SYSTEM_CODE_UTAH_TIC:
			case CFileType::SYSTEM_CODE_WYOMING_TIC:
			case CFileType::SYSTEM_CODE_ARIZONA_TIC:
			case CFileType::SYSTEM_CODE_MONTANA_TIC:
			case CFileType::SYSTEM_CODE_PUERTO_RICO_TIC:
			case CFileType::SYSTEM_CODE_MINNESOTA_TIC:
			case CFileType::SYSTEM_CODE_HAWAII_TIC:
			case CFileType::SYSTEM_CODE_SOUTH_DAKOTA_TIC:
			case CFileType::SYSTEM_CODE_NORTH_DAKOTA_TIC:
			case CFileType::SYSTEM_CODE_INDIANA_TIC:
			case CFileType::SYSTEM_CODE_WEST_VIRGINIA_TIC:
			case CFileType::SYSTEM_CODE_VIRGIN_ISLANDS_TIC:
			case CFileType::SYSTEM_CODE_COLORADO_TIC:
			case CFileType::SYSTEM_CODE_IOWA_TIC:
			case CFileType::SYSTEM_CODE_ALASKA_TIC:
			case CFileType::SYSTEM_CODE_DELAWARE_TIC:
			case CFileType::SYSTEM_CODE_DISTRICT_OF_COLUMBIA_TIC:
			case CFileType::SYSTEM_CODE_WISCONSIN_TIC:
			case CFileType::SYSTEM_CODE_ALABAMA_TIC:
			case CFileType::SYSTEM_CODE_NEW_YORK_TIC:
			case CFileType::SYSTEM_CODE_MAINE_TIC:
			case CFileType::SYSTEM_CODE_NEBRASKA_TIC:
			case CFileType::SYSTEM_CODE_KANSAS_TIC:
			case CFileType::SYSTEM_CODE_MEXICO_TIC:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET:
			case CFileType::SYSTEM_CODE_INSPECTION_SIGNATURE:
				return PATH_MOUNTS_DOCUMENTS;

			case CFileType::SYSTEM_CODE_MILITARY_MIMO:
			case CFileType::SYSTEM_CODE_MILITARY_PROMO_DEMO:
			case CFileType::SYSTEM_CODE_MILITARY_MAC_PAYMENT:
				return PATH_MOUNTS_MILITARY;

			case CFileType::SYSTEM_CODE_CORPORATE_JOB_DOCUMENT:
				return ( valId( $this->getBudgetChangeOrderId() ) ) ? PATH_MOUNTS_FILES : 'storage_container/job_documents/';
			case CFileType::SYSTEM_CODE_CORPORATE_AP_CONTRACT_DOCUMENT:
				return 'storage_container/ap_contract_documents/';
			case CFileType::SYSTEM_CODE_DRAW_REQUEST_DOCUMENT:
				return 'storage_container/draw_request_documents/';
			case CFileType::SYSTEM_CODE_LIEN_WAIVER:
				return 'storage_container/lien_waivers/';

			case CFileType::SYSTEM_CODE_USER_DOCUMENT: // User tool team having this system code.
					return PATH_MOUNTS_USER_FILES;

			default:
				return PATH_MOUNTS_FILES;
		}
	}

	public function getSignedFilePath() {

//		if( 0 == preg_match( '/signed/', $this->getFilePath() ) ) {
//			return $this->getFilePath() . 'signed/';
//		}
		return $this->getFilePath();
	}

	public function getOldSignedFilePath() {

		if( 0 == preg_match( '/signed/', $this->getFilePath() ) ) {
			return $this->getFilePath() . 'signed/';
		}
		return $this->getFilePath();
	}

	public function getUnsignedFilePath() {
		if( 0 < preg_match( '/signed/', $this->getFilePath() ) ) {
			return str_replace( '/signed', '', $this->getFilePath() );
		}

		return $this->getFilePath();
	}

	public function getRelFilePath() {
		$this->getFilePath() . $this->getFileName();
	}

	public function getRelSignedFilePath() {
		$this->getSignedFilePath() . $this->getFileName();
	}

	public function getFullFilePath( $strFolderName = PATH_MOUNTS_FILES ) {

		switch( $this->getFileTypeSystemCode() ) {
			case CFileType::SYSTEM_CODE_PRE_SIGNED:
			case CFileType::SYSTEM_CODE_PARTIALLY_SIGNED:
			case CFileType::SYSTEM_CODE_SIGNED:
			case CFileType::SYSTEM_CODE_LEASE_ADDENDUM:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE:
			case CFileType::SYSTEM_CODE_LEASE_ESA:
			case CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT:
			case CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET:
			case CFileType::SYSTEM_CODE_LEASE_PACKET:
			case CFileType::SYSTEM_CODE_UNIT_TRANSFER:
			case CFileType::SYSTEM_CODE_HUD50059:
			case CFileType::SYSTEM_CODE_TRIAL_HUD50059:
			case CFileType::SYSTEM_CODE_HUD50059A:
			case CFileType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_PARENTAL_CONSENT_FORM:
			case CFileType::SYSTEM_CODE_SIGNATURE:
			case CFileType::SYSTEM_CODE_FLORIDA_TIC:
			case CFileType::SYSTEM_CODE_HUD91067:
			case CFileType::SYSTEM_CODE_TEXAS_TIC:
			case CFileType::SYSTEM_CODE_MICHIGAN_TIC:
			case CFileType::SYSTEM_CODE_TENNESSEE_TIC:
			case CFileType::SYSTEM_CODE_OREGON_TIC:
			case CFileType::SYSTEM_CODE_MASSACHUSETTS_TIC:
			case CFileType::SYSTEM_CODE_FLORIDA_TIC:
			case CFileType::SYSTEM_CODE_NEWJERSEY_TIC:
			case CFileType::SYSTEM_CODE_ARKANSAS_TIC:
			case CFileType::SYSTEM_CODE_WASHINGTON_TIC:
			case CFileType::SYSTEM_CODE_VIRGINIA_TIC:
			case CFileType::SYSTEM_CODE_GEORGIA_TIC:
			case CFileType::SYSTEM_CODE_MARYLAND_TIC:
			case CFileType::SYSTEM_CODE_CALIFORNIA_TIC:
			case CFileType::SYSTEM_CODE_RHODE_ISLAND_TIC:
			case CFileType::SYSTEM_CODE_OKLAHOMA_TIC:
			case CFileType::SYSTEM_CODE_LOUISIANA_TIC:
			case CFileType::SYSTEM_CODE_KENTUCKY_TIC:
			case CFileType::SYSTEM_CODE_MISSISSIPPI_TIC:
			case CFileType::SYSTEM_CODE_MISSOURI_TIC:
			case CFileType::SYSTEM_CODE_MISSOURI_EXHIBIT_IUNIT_TIC:
			case CFileType::SYSTEM_CODE_SOUTH_CAROLINA_TIC:
			case CFileType::SYSTEM_CODE_CALIFORNIA2_TIC:
			case CFileType::SYSTEM_CODE_TEXAS_STUDENT_CERTIFICATION_TIC:
			case CFileType::SYSTEM_CODE_ILLINOIS_TIC:
			case CFileType::SYSTEM_CODE_NEVADA_TIC:
			case CFileType::SYSTEM_CODE_CONNECTICUT_TIC:
			case CFileType::SYSTEM_CODE_NORTH_CAROLINA_TIC:
			case CFileType::SYSTEM_CODE_PENNSYLVANIA_TIC:
			case CFileType::SYSTEM_CODE_IDAHO_TIC:
			case CFileType::SYSTEM_CODE_VERMONT_TIC:
			case CFileType::SYSTEM_CODE_NEW_HAMPSHIRE_TIC:
			case CFileType::SYSTEM_CODE_OHIO_TIC:
			case CFileType::SYSTEM_CODE_NEW_YORK_CITY_TIC:
			case CFileType::SYSTEM_CODE_UTAH_TIC:
			case CFileType::SYSTEM_CODE_WYOMING_TIC:
			case CFileType::SYSTEM_CODE_ARIZONA_TIC:
			case CFileType::SYSTEM_CODE_MONTANA_TIC:
			case CFileType::SYSTEM_CODE_PUERTO_RICO_TIC:
			case CFileType::SYSTEM_CODE_MINNESOTA_TIC:
			case CFileType::SYSTEM_CODE_HAWAII_TIC:
			case CFileType::SYSTEM_CODE_SOUTH_DAKOTA_TIC:
			case CFileType::SYSTEM_CODE_NORTH_DAKOTA_TIC:
			case CFileType::SYSTEM_CODE_INDIANA_TIC:
			case CFileType::SYSTEM_CODE_WEST_VIRGINIA_TIC:
			case CFileType::SYSTEM_CODE_VIRGIN_ISLANDS_TIC:
			case CFileType::SYSTEM_CODE_COLORADO_TIC:
			case CFileType::SYSTEM_CODE_IOWA_TIC:
			case CFileType::SYSTEM_CODE_ALASKA_TIC:
			case CFileType::SYSTEM_CODE_DELAWARE_TIC:
			case CFileType::SYSTEM_CODE_DISTRICT_OF_COLUMBIA_TIC:
			case CFileType::SYSTEM_CODE_WISCONSIN_TIC:
			case CFileType::SYSTEM_CODE_ALABAMA_TIC:
			case CFileType::SYSTEM_CODE_NEW_YORK_TIC:
			case CFileType::SYSTEM_CODE_MAINE_TIC:
			case CFileType::SYSTEM_CODE_NEBRASKA_TIC:
			case CFileType::SYSTEM_CODE_KANSAS_TIC:
			case CFileType::SYSTEM_CODE_MEXICO_TIC:
				$strFolderName = PATH_MOUNTS_DOCUMENTS;
				break;

			case CFileType::SYSTEM_CODE_WORK_ORDER:
			case CFileType::SYSTEM_CODE_WAITLIST:
				$strFolderName = 'emails/' . $this->getLeaseId() . '/';
				break;

			default:
				$strFolderName = PATH_MOUNTS_FILES;
				break;
		}

		return getMountsPath( $this->getCid(), $strFolderName ) . $this->getFilePath();
	}

	public function getSignedFullFilePath( $strFolderName = PATH_MOUNTS_FILES ) {

		switch( $this->getFileTypeSystemCode() ) {
			case CFileType::SYSTEM_CODE_PRE_SIGNED:
			case CFileType::SYSTEM_CODE_PARTIALLY_SIGNED:
			case CFileType::SYSTEM_CODE_SIGNED:
			case CFileType::SYSTEM_CODE_LEASE_ADDENDUM:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE:
			case CFileType::SYSTEM_CODE_LEASE_ESA:
			case CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT:
			case CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET:
			case CFileType::SYSTEM_CODE_LEASE_PACKET:
			case CFileType::SYSTEM_CODE_UNIT_TRANSFER:
			case CFileType::SYSTEM_CODE_HUD50059:
			case CFileType::SYSTEM_CODE_TRIAL_HUD50059:
			case CFileType::SYSTEM_CODE_HUD50059A:
			case CFileType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE:
			case CFileType::SYSTEM_CODE_PARENTAL_CONSENT_FORM:
			case CFileType::SYSTEM_CODE_SIGNATURE:
			case CFileType::SYSTEM_CODE_HUD91067:
			case CFileType::SYSTEM_CODE_TEXAS_TIC:
			case CFileType::SYSTEM_CODE_MICHIGAN_TIC:
			case CFileType::SYSTEM_CODE_TENNESSEE_TIC:
			case CFileType::SYSTEM_CODE_OREGON_TIC:
			case CFileType::SYSTEM_CODE_MASSACHUSETTS_TIC:
			case CFileType::SYSTEM_CODE_FLORIDA_TIC:
			case CFileType::SYSTEM_CODE_NEWJERSEY_TIC:
			case CFileType::SYSTEM_CODE_ARKANSAS_TIC:
			case CFileType::SYSTEM_CODE_WASHINGTON_TIC:
			case CFileType::SYSTEM_CODE_VIRGINIA_TIC:
			case CFileType::SYSTEM_CODE_GEORGIA_TIC:
			case CFileType::SYSTEM_CODE_MARYLAND_TIC:
			case CFileType::SYSTEM_CODE_CALIFORNIA_TIC:
			case CFileType::SYSTEM_CODE_RHODE_ISLAND_TIC:
			case CFileType::SYSTEM_CODE_OKLAHOMA_TIC:
			case CFileType::SYSTEM_CODE_LOUISIANA_TIC:
			case CFileType::SYSTEM_CODE_KENTUCKY_TIC:
			case CFileType::SYSTEM_CODE_MISSISSIPPI_TIC:
			case CFileType::SYSTEM_CODE_MISSOURI_TIC:
			case CFileType::SYSTEM_CODE_MISSOURI_EXHIBIT_IUNIT_TIC:
			case CFileType::SYSTEM_CODE_SOUTH_CAROLINA_TIC:
			case CFileType::SYSTEM_CODE_CALIFORNIA2_TIC:
			case CFileType::SYSTEM_CODE_TEXAS_STUDENT_CERTIFICATION_TIC:
			case CFileType::SYSTEM_CODE_ILLINOIS_TIC:
			case CFileType::SYSTEM_CODE_NEVADA_TIC:
			case CFileType::SYSTEM_CODE_CONNECTICUT_TIC:
			case CFileType::SYSTEM_CODE_NORTH_CAROLINA_TIC:
			case CFileType::SYSTEM_CODE_PENNSYLVANIA_TIC:
			case CFileType::SYSTEM_CODE_IDAHO_TIC:
			case CFileType::SYSTEM_CODE_VERMONT_TIC:
			case CFileType::SYSTEM_CODE_NEW_HAMPSHIRE_TIC:
			case CFileType::SYSTEM_CODE_OHIO_TIC:
			case CFileType::SYSTEM_CODE_NEW_YORK_CITY_TIC:
			case CFileType::SYSTEM_CODE_UTAH_TIC:
			case CFileType::SYSTEM_CODE_WYOMING_TIC:
			case CFileType::SYSTEM_CODE_ARIZONA_TIC:
			case CFileType::SYSTEM_CODE_MONTANA_TIC:
			case CFileType::SYSTEM_CODE_PUERTO_RICO_TIC:
			case CFileType::SYSTEM_CODE_MINNESOTA_TIC:
			case CFileType::SYSTEM_CODE_HAWAII_TIC:
			case CFileType::SYSTEM_CODE_SOUTH_DAKOTA_TIC:
			case CFileType::SYSTEM_CODE_NORTH_DAKOTA_TIC:
			case CFileType::SYSTEM_CODE_INDIANA_TIC:
			case CFileType::SYSTEM_CODE_WEST_VIRGINIA_TIC:
			case CFileType::SYSTEM_CODE_VIRGIN_ISLANDS_TIC:
			case CFileType::SYSTEM_CODE_COLORADO_TIC:
			case CFileType::SYSTEM_CODE_IOWA_TIC:
			case CFileType::SYSTEM_CODE_ALASKA_TIC:
			case CFileType::SYSTEM_CODE_DELAWARE_TIC:
			case CFileType::SYSTEM_CODE_DISTRICT_OF_COLUMBIA_TIC:
			case CFileType::SYSTEM_CODE_WISCONSIN_TIC:
			case CFileType::SYSTEM_CODE_ALABAMA_TIC:
			case CFileType::SYSTEM_CODE_NEW_YORK_TIC:
			case CFileType::SYSTEM_CODE_MAINE_TIC:
			case CFileType::SYSTEM_CODE_NEBRASKA_TIC:
			case CFileType::SYSTEM_CODE_KANSAS_TIC:
			case CFileType::SYSTEM_CODE_MEXICO_TIC:
				$strFolderName = PATH_MOUNTS_DOCUMENTS;
				break;

			case CFileType::SYSTEM_CODE_POLICY:
				$strFolderName = PATH_MOUNTS_FILES;
				break;

			default:
				$strFolderPath = PATH_MOUNTS_FILES;
				break;
		}

		return getMountsPath( $this->getCid(), $strFolderName ) . $this->getSignedFilePath();
	}

	public function getContents( $strFolderName = PATH_MOUNTS_FILES ) {
		$strFullpath = ( false !== \Psi\CStringService::singleton()->strpos( $strFolderName, PATH_MOUNTS_GLOBAL_VENDOR_DOCUMENTS ) ) ? $strFolderName . $this->getTitle() : $this->getFullFilePath( $strFolderName ) . $this->getFileName();
		return CFileIo::fileGetContents( $strFullpath );
	}

	public function downloadFile( $objDatabase, $strFolderName = PATH_MOUNTS_FILES, $intCurrentUserId = NULL ) {

		// TODO: This function is going to be deprecated, Please use downloadObject with IObjectStorageGateway.

 		$strFullpath = ( false !== \Psi\CStringService::singleton()->strpos( $strFolderName, PATH_MOUNTS_GLOBAL_VENDOR_DOCUMENTS ) ) ? $strFolderName . $this->getTitle() : $this->getFullFilePath( $strFolderName ) . $this->getFileName();
		$strPdfFileName = '';
		$strFullDirpath = '';
		if( false == CFileIo::fileExists( $strFullpath ) ) {
			trigger_error( __( 'File is not present or has been deleted.' ), E_USER_ERROR );
			exit;
		}

		$arrstrFileParts = pathinfo( $this->getFileName() );

		$strFileName = $this->getFileName();

		if( false !== \Psi\CStringService::singleton()->strpos( $strFileName, '.html' ) ) {
			$strPdfFileName = str_replace( '.html', '.pdf', $strFileName );
			$strFullDirpath = preg_replace( '#\/[^/]*$#', '', $strFullpath );
		}

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_APPLICATION && true == CFileIo::fileExists( $strFullDirpath ) && true == CFileIo::isFile( $strFullpath ) && NULL != $intCurrentUserId ) {
			$objDompdf = new CDomPdf();
			$strHtmlContent = CFileIo::fileGetContents( $strFullpath );
			$strHtmlContent = str_replace( '<head>', '<head><style type="text/css">div {page-break-inside: avoid;}</style>', $strHtmlContent );
			$strHtmlContent = str_replace( '<head>', '<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />', $strHtmlContent );
			$strHtmlContent = \Psi\CStringService::singleton()->preg_replace( '/(width=|height=)(\"|\')([0-9]*)(\"|\')/', '${1}${2}${3}px${4}', $strHtmlContent );

			$objDompdf->load_html( $strHtmlContent );
			$objDompdf->render();
			$strOutput = $objDompdf->output();
			CFileIo::filePutContents( $strFullDirpath . '/' . $strPdfFileName, $strOutput );

			$this->setFileExtensionId( CFileExtension::APPLICATION_PDF );
			$this->setFileName( $strPdfFileName );
			$this->update( $intCurrentUserId, $objDatabase );

			$objDompdf->stream( $strPdfFileName );

			unlink( $strFullpath );
			exit;
		} else {
			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: public' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Length:' . filesize( $strFullpath ) );
			header( 'Content-Type:' . $strMimeType );
			header( 'Content-Disposition: attachment; filename= "' . $strFileName . '"' );
			header( 'Content-Transfer-Encoding:binary' );

			if( 'text/html' == $strMimeType )
				echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

			echo CFileIo::fileGetContents( $strFullpath );
			exit;
		}
	}

	public function downloadEncryptedFile( $objDocumentManager, $strFolderName = PATH_MOUNTS_FILES, $objDatabase, $intCurrentUserId = NULL, $strDownloadFilePath = NULL, $boolIsReturnFilePath = false, $boolIsOpenFileInBrowser = false ) {

		// TODO: This function is going to be deprecated, Please use downloadObject() with IObjectStorageGateway.

		$strPath = $this->getFilePath() . $this->getFileName();

		$strTempFullPath = $objDocumentManager->getDocumentToTempFile( CDocumentCriteria::forGet( $this->getId(), $strFolderName, $strPath ) );
		$strPdfFilePath = NULL;

		if( false === valStr( $strTempFullPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$arrstrFileParts = pathinfo( $strTempFullPath );

		$strFileName = $this->getFileName();
		$strPdfFileName = '';

		if( false !== \Psi\CStringService::singleton()->strpos( $strFileName, '.html' ) ) {
			$strPdfFileName = str_replace( '.html', '.pdf', $strFileName );
			$strPdfFilePath = preg_replace( '#\/[^/]*$#', '', $strPath );
		}

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_APPLICATION && true == valStr( $strPdfFilePath )
		    && true == $objDocumentManager->documentExists( CDocumentCriteria::forExists( -1, PATH_MOUNTS_DOCUMENTS, $strPath ) ) && NULL != $intCurrentUserId ) {

			$objDompdf = new CDomPdf();
			$strHtmlContent = trim( CFileIo::fileGetContents( $strTempFullPath ), '\0\t\n' );
			$strHtmlContent = str_replace( '<head>', '<head><style type="text/css">div {page-break-inside: avoid;}</style>', $strHtmlContent );
			$strHtmlContent = str_replace( '<head>', '<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />', $strHtmlContent );
			$strHtmlContent = \Psi\CStringService::singleton()->preg_replace( '/(width=|height=)(\"|\')([0-9]*)(\"|\')/', '${1}${2}${3}px${4}', $strHtmlContent );

			$objDompdf->load_html( $strHtmlContent );
			$objDompdf->render();
			$strOutput = $objDompdf->output();

			if( false == $objDocumentManager->putDocument( CDocumentCriteria::forPut( $this->getId(), $strFolderName, $strPdfFilePath . $strPdfFileName, $strOutput ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}

			$this->setFileExtensionId( CFileExtension::APPLICATION_PDF );
			$this->setFileName( $strPdfFileName );
			$this->update( $intCurrentUserId, $objDatabase );

			$objDompdf->stream( $strPdfFileName );

			if( false == $objDocumentManager->removeDocument( CDocumentCriteria::forRemove( $this->getId(), $strFolderName, $strPath ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}

			if( true == CFileIo::fileExists( $strDownloadFilePath ) ) {
				unlink( $strDownloadFilePath );
			}

			exit;
		} else {

			if( true == CFileIo::fileExists( $strDownloadFilePath ) ) {
				unlink( $strDownloadFilePath );
			}

			if( true == $boolIsReturnFilePath ) {
				return $strTempFullPath;
			}

			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: public' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Length:' . filesize( $strTempFullPath ) );
			header( 'Content-Type:' . $strMimeType );
			header( 'Content-Disposition: ' . ( ( true == $boolIsOpenFileInBrowser ) ? 'inline' : 'attachment' ) . '; filename= "' . $strFileName . '"' );
			header( 'Content-Transfer-Encoding:binary' );

			if( 'text/html' == $strMimeType )
				echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

			echo CFileIo::fileGetContents( $strTempFullPath );
			exit;
		}

	}

	public function bindDetails( $arrmixDetails ) {
		$arrmixDetails = ( object ) $arrmixDetails;
		$this->setDetails( $arrmixDetails );
	}

	/**
	 * @deprecated This function is deprecated.
	 * @see Please use downloadFileObject().
	 */
	public function downloadObject( $objObjectStorageGateway, $objDatabase, $strFolderName = PATH_MOUNTS_FILES, $intCurrentUserId = NULL, $strDownloadFilePath = NULL, $boolIsReturnFilePath = false ) {

		// This was ported from CFile::downloadFile - Jad

		$strPath     = $this->getFilePath() . $this->getFileName();

		$intObjectId = ( true == valId( $this->getId() ) ) ? $this->getId() : -1;

		$arrmixGatewayRequest = $this->fetchStoredObject( $objDatabase ?? $this->m_objDatabase )->createGatewayRequest( [ 'outputFile'    => 'temp' ] );
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );

		$objStoredObject = $this->fetchStoredObject( $objDatabase ?? $this->m_objDatabase );
		$strFolderName = ( true == valId( $objStoredObject->getId() ) ) ? $objStoredObject->getContainer() : $strFolderName;

		$strTempFullPath = $objObjectStorageGatewayResponse['outputFile'];

		if( false == CFileIo::fileExists( $strTempFullPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$arrstrFileParts    = pathinfo( $strTempFullPath );
		$strFileName        = $this->getFileName();
		$strPdfFileName     = '';
		$strPdfFilePath     = NULL;

		if( false !== \Psi\CStringService::singleton()->strpos( $strFileName, '.html' ) ) {
			$strPdfFileName = str_replace( '.html', '.pdf', $strFileName );
			$strPdfFilePath = preg_replace( '#\/[^/]*$#', '', $strPath );
		}

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_APPLICATION && true == valStr( $strPdfFilePath )
		    && true == valStr( $strTempFullPath ) && NULL != $intCurrentUserId ) {

			$objDompdf = new CDomPdf();
			$strHtmlContent = trim( CFileIo::fileGetContents( $strTempFullPath ), '\0\t\n' );
			$strHtmlContent = str_replace( '<head>', '<head><style type="text/css">div {page-break-inside: avoid;}</style>', $strHtmlContent );
			$strHtmlContent = str_replace( '<head>', '<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />', $strHtmlContent );
			$strHtmlContent = \Psi\CStringService::singleton()->preg_replace( '/(width=|height=)(\"|\')([0-9]*)(\"|\')/', '${1}${2}${3}px${4}', $strHtmlContent );

			$objDompdf->load_html( $strHtmlContent );
			$objDompdf->render();
			$strOutput = $objDompdf->output();

			// Write data contained directly in request
			$objObjectPutResponse = $objObjectStorageGateway->putObject( [
				'cid'        => $this->getCid(),
				'objectId'   => $intObjectId,
				'container'  => $strFolderName,
				'key'        => $strPdfFilePath . $strPdfFileName,
				'data'       => $strOutput,
				'noEncrypt'  => true
			] );

			if( true == $objObjectPutResponse->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}

			$this->setFileExtensionId( CFileExtension::APPLICATION_PDF );
			$this->setFileName( $strPdfFileName );
			$this->update( $intCurrentUserId, $objDatabase );

			$objDompdf->stream( $strPdfFileName );

			$objDeleteObjectResponse = $objObjectStorageGateway->deleteObject( [
				'cid'             => $this->getCid(),
				'objectId'        => $intObjectId,
				'container'       => $strFolderName,
				'key'             => $strPath
			] );

			if( true == $objDeleteObjectResponse->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}

			if( true == CFileIo::fileExists( $strDownloadFilePath ) ) {
				unlink( $strDownloadFilePath );
			}

			exit;
		} else {

			if( true == CFileIo::fileExists( $strDownloadFilePath ) ) {
				unlink( $strDownloadFilePath );
			}

			if( true == $boolIsReturnFilePath ) {
				return $strTempFullPath;
			}

			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: public' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Length:' . CFileIo::getFileSize( $strTempFullPath ) );
			header( 'Content-Type:' . $strMimeType );
			header( 'Content-Disposition: attachment; filename= "' . $strFileName . '"' );
			header( 'Content-Transfer-Encoding:binary' );

			if( 'text/html' == $strMimeType )
				echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

			echo CFileIo::fileGetContents( $strTempFullPath );
			CFileIo::deleteFile( $strTempFullPath );
			exit;
		}

	}

	public function downloadFileObject( $objObjectStorageGateway, $objDatabase, $strReferenceTag  = NULL,  $intCurrentUserId = NULL, $boolIsReturnFilePath = false ) {

		$strPath     = $this->getFilePath() . $this->getFileName();

		$intObjectId = ( true == valId( $this->getId() ) ) ? $this->getId() : -1;

		$arrmixGatewayRequest = $this->fetchStoredObject( $objDatabase ?? $this->m_objDatabase, $strReferenceTag )->createGatewayRequest( [ 'outputFile'    => 'temp' ] );
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );

		$strTempFullPath = $objObjectStorageGatewayResponse['outputFile'];

		if( false == CFileIo::fileExists( $strTempFullPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$arrstrFileParts    = pathinfo( $strTempFullPath );
		$strFileName        = $this->getFileName();
		$strPdfFileName     = '';
		$strPdfFilePath     = NULL;

		if( false !== \Psi\CStringService::singleton()->strpos( $strFileName, '.html' ) ) {
			$strPdfFileName = str_replace( '.html', '.pdf', $strFileName );
			$strPdfFilePath = preg_replace( '#\/[^/]*$#', '', $strPath );
		}

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_APPLICATION && true == valStr( $strPdfFilePath )
		    && true == valStr( $strTempFullPath ) && NULL != $intCurrentUserId ) {

			$objDompdf = new CDomPdf();
			$strHtmlContent = trim( CFileIo::fileGetContents( $strTempFullPath ), '\0\t\n' );
			$strHtmlContent = str_replace( '<head>', '<head><style type="text/css">div {page-break-inside: avoid;}</style>', $strHtmlContent );
			$strHtmlContent = str_replace( '<head>', '<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />', $strHtmlContent );
			$strHtmlContent = \Psi\CStringService::singleton()->preg_replace( '/(width=|height=)(\"|\')([0-9]*)(\"|\')/', '${1}${2}${3}px${4}', $strHtmlContent );

			$objDompdf->load_html( $strHtmlContent );
			$objDompdf->render();
			$strOutput = $objDompdf->output();

			// Write data contained directly in request
			$arrmixGatewayRequest = $this->createPutGatewayRequest( [ 'data' => $strOutput ] );
			$objObjectPutResponse = $objObjectStorageGateway->putObject( $arrmixGatewayRequest );

			if( true == $objObjectPutResponse->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}
			$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );
			$this->insertOrUpdateStoredObject( CCompanyUser::SYSTEM, $this->m_objDatabase );

			$this->setFileExtensionId( CFileExtension::APPLICATION_PDF );
			$this->setFileName( $strPdfFileName );
			$this->update( $intCurrentUserId, $objDatabase );

			$objDompdf->stream( $strPdfFileName );

			$arrmixGatewayRequest = $this->createGatewayRequest();
			$objDeleteObjectResponse = $this->getObjectStorageGateway()->deleteObject( $arrmixGatewayRequest );

			if( true == $objDeleteObjectResponse->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}
			$this->deleteStoredObject( $this->m_objCompanyUser->getId(), $this->m_objDatabase );

			exit;
		} else {

			if( true == $boolIsReturnFilePath ) {
				return $strTempFullPath;
			}

			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: public' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Length:' . CFileIo::getFileSize( $strTempFullPath ) );
			header( 'Content-Type:' . $strMimeType );
			header( 'Content-Disposition: attachment; filename= "' . $strFileName . '"' );
			header( 'Content-Transfer-Encoding:binary' );

			if( 'text/html' == $strMimeType )
				echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

			echo CFileIo::fileGetContents( $strTempFullPath );
			CFileIo::deleteFile( $strTempFullPath );
			exit;
		}

	}

	public function buildFilePath( $intFileTypeId ) {

		$strPath = '';

		switch( $this->getFileTypeSystemCode() ) {

			case CFileType::SYSTEM_CODE_MESSAGE_CENTER:
				if( true == valId( $this->getScheduledEmailId() ) ) {
					$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), 'message_center/scheduled_email_attachments/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getScheduledEmailId() );
				} else if( true == valId( $this->getCampaignId() ) ) {
					$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), 'message_center/campaign/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getCampaignId() );
				} else if( true == $this->m_boolIsSystemMessageAttachment ) {
					$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), 'message_center/system_message_attachments/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getScheduledTaskId() );
				} else {
					$strPath = '/scheduled_email_attachments/';
				}
				break;

			case CFileType::SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_FINANCIAL_MOVE_OUT_STATEMENT, date( 'Y' ), date( 'm' ), date( 'd' ), $this->getLeaseId() );
				break;

			case CFileType::SYSTEM_CODE_SENT_TO_COLLECTION:
				$strPath = CFileType::PATH_SENT_TO_COLLECTION . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_PRE_COLLECTION_LETTER_SENT:
				$strPath = CFileType::PATH_PRE_COLLECTION_LETTER_SENT . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_UNIT_TRANSFER:
				$strPath = 'Transfer_statements/' . $this->getLeaseId() . '/';
				break;

			case \CFileType::SYSTEM_CODE_APPLICATION_UPLOAD:
				$strPath = $this->getCid() . '/applications/app_upload/' . $this->getPropertyId() . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_LATE_NOTICE:
				$strPath = 'late_notices/' . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_COLLECTIONS_NOTICE:
				$strPath = 'collection_notices/' . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_REFUND_STATEMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_REFUND_STATEMENT, date( 'Y' ), date( 'm' ), date( 'd' ), $this->getLeaseId() );
				break;

			case CFileType::SYSTEM_CODE_WORK_ORDER:
				$strPath = CFileType::PATH_WORK_ORDER . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_WAITLIST:
				$strPath = CFileType::PATH_WAITLIST . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_MOVE_IN_STATEMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_MOVE_IN_STATEMENT, date( 'Y' ), date( 'm' ), date( 'd' ), $this->getLeaseId() );
				break;

			case CFileType::SYSTEM_CODE_USER_DOCUMENT:
				$strPath = sprintf( '%d/%s/%d/', $this->getCid(), 'user_files', $this->getCompanyUserId() );
//				$strPath = $this->getCompanyUserId() . '/';
				break;

			case CFileType::SYSTEM_CODE_RENEWAL_MONTH_CHARGES_STATEMENT:
				$strPath = CFileType::PATH_RENEWAL_MONTH_CHARGES_STATEMENT . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_HUD50059:
			case CFileType::SYSTEM_CODE_HUD50059A:
				$strPath = CFileType::PATH_HUD50059 . $this->getSubsidyCertificationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_HUD52670:
			case CFileType::SYSTEM_CODE_HUD52670A_PART1:
			case CFileType::SYSTEM_CODE_HUD52670A_PART2:
			case CFileType::SYSTEM_CODE_HUD52670A_PART3:
			case CFileType::SYSTEM_CODE_HUD52670A_PART4:
			case CFileType::SYSTEM_CODE_HUD52670A_PART5:
			case CFileType::SYSTEM_CODE_HUD52670A_PART6:
				$strPath = CFileType::PATH_HUD52670 . $this->getSubsidyHapRequestId() . '/';
				break;

			case CFileType::SYSTEM_CODE_HUD52671A:
			case CFileType::SYSTEM_CODE_HUD52671B:
			case CFileType::SYSTEM_CODE_HUD52671C:
			case CFileType::SYSTEM_CODE_HUD52671D:
				$strPath = CFileType::PATH_HUD52671A . $this->getSpecialClaimId() . '/';
				break;

			case CFileType::SYSTEM_CODE_TENANT_STATEMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_TENANT_STATEMENT, date( 'Y' ), date( 'm' ), date( 'd' ), $this->getLeaseId() );
				break;

			case CFileType::SYSTEM_CODE_MILITARY_MIMO:
				$strPath = CFileType::PATH_MILITARY_MIMO;
				break;

			case CFileType::SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_COL:
			case CFileType::SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_NAHMA:
			case CFileType::SYSTEM_CODE_AFFORDABLE_ELECTRONIC_REPORT_WBARS:
				$strPath = CFileType::PATH_AFFORDABLE_ELECTRONIC_REPORTING;
				break;

			case CFileType::SYSTEM_CODE_NSF:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::SYSTEM_CODE_NSF, date( 'Y' ), date( 'm' ), date( 'd' ), $this->getLeaseId() );
				break;

			case CFileType::SYSTEM_CODE_RV_CONSUMER_AUTHORIZATION:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/rv_screening_authorization/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/' . $this->getApplicantId() . '/';
				break;

			case CFileType::SYSTEM_CODE_RENTAL_COLLECTION_PAYMENT_PROOF:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/rv_rental_collection_proof/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/' . $this->getApplicantId() . '/';
				break;

			case CFileType::SYSTEM_CODE_MAT_FILE:
				$strPath = CFileType::PATH_MAT_FILE . $this->getSubsidyRequestId() . '/';
				break;

			case CFileType::SYSTEM_CODE_MAT_HAP:
				$strPath = CFileType::PATH_MAT_HAP . $this->getSubsidyHapRequestId() . '/';
				break;

			case CFileType::SYSTEM_CODE_MANAGEMENT_FEE_INVOICE:
				$strPath = CFileType::PATH_MANAGEMENT_FEE_INVOICE . $this->getApHeaderId() . '/';
				break;

			case CFileType::SYSTEM_CODE_LEASE_DOCUMENT:
				$strPath = sprintf( '%d/%d/leasing/%d/%d/%s/%s/', $this->getCid(), $this->getPropertyId(), $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_APPLICANT_IDENTIFICATION:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/application/identification_document/';
				break;

			case CFileType::SYSTEM_CODE_GUEST_CARD:
				$strPath = $this->getCid() . '/guestcards/' . $this->getPropertyId() . '/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_PETIMAGE:
				$strPath = 'media_library/' . $this->getCid() . '/' . $this->getPropertyId() . '/' . date( 'Y' ) . date( 'm' ) . '/' . date( 'd' ) . '/';
				break;

			case CFileType::SYSTEM_CODE_LEASE_CLAUSE_DOCUMENT:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/' . CFileType::PATH_LEASE_CLAUSE_DOCUMENT . date( 'Y' ) . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_TRIPLE_NET_RECONCILIATION:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/' . CFileType::PATH_TRIPLE_NET_RECONCILIATION . date( 'Y' ) . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getLeaseId() . '/';
				break;

			case CFileType::SYSTEM_CODE_LEASE_DATES_ADJUSTMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_LEASE_DATES_ADJUSTMENT, date( 'Y' ), date( 'm' ), date( 'd' ), $this->getLeaseId() );
				break;

			case CFileType::SYSTEM_CODE_MOVE_IN_ADJUSTMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_MOVE_IN_ADJUSTMENT, date( 'Y' ), date( 'm' ), date( 'd' ), $this->getLeaseId() );
				break;

			case CFileType::SYSTEM_CODE_EARLY_TERMINATION_AGREEMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_EARLY_TERMINATION_AGREEMENT, date( 'Y' ), date( 'm' ), date( 'd' ), $this->getLeaseId() );
				break;

			case CFileType::SYSTEM_CODE_INVOICE:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_INVOICE, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_JOURNAL_ENTRY_ATTACHMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_JOURNAL_ENTRY_ATTACHMENT, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_PURCHASE_ORDER:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_PURCHASE_ORDER, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_CORPORATE_JOB_DOCUMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_CORPORATE_JOB_DOCUMENT, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_CORPORATE_AP_CONTRACT_DOCUMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_CORPORATE_AP_CONTRACT_DOCUMENT, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_LIEN_WAIVER:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_LIEN_WAIVER, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_DRAW_REQUEST_DOCUMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_DRAW_REQUEST_DOCUMENT, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_ADVERSE_ACTION_LETTER:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/rv_adverse_action_letter/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_SCREENING_DECISION_SUMMARY_LETTER:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/rv_screening_decision_summary_letter/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_PARENTAL_CONSENT_FORM:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/rv_parental_consent_form/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_GCIC:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/rv_gcic/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_REPAYMENT_AGREEMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCId(), CFileType::PATH_REPAYMENTS, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_PROOF_OF_LAWFUL_PRESENCE:
				$strPath = $this->getCid() . '/' . $this->getPropertyId() . '/rv_proof_of_lawful_presence/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/';
				break;

			case CFileType::SYSTEM_CODE_LEASE_ACCOMMODATION_ATTACHMENT;
				$strPath = sprintf( '%d/%s%d/', $this->getCid(), CFileType::PATH_LEASE_ACCOMMODATIONS, $this->getLeaseAccommodationId() );
				break;

			case CFileType::SYSTEM_CODE_AR_TRANSACTION_ATTACHMENT;
				$strPath = sprintf( '%d/', $this->getCid() );
				break;

			case CFileType::SYSTEM_CODE_AP_PAYMENT_DOCUMENT:
			case CFileType::SYSTEM_CODE_AP_PAYMENT_CHECK_COPY_DOCUMENT:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_AP_PAYMENT_DOCUMENT, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_GL_RECONCILIATION:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_RECONCILIATIONS, $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_NACHA:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_AP_PAYMENTS . 'nacha/', $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_WFPM:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_AP_PAYMENTS . 'wfpm/', $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_AP_PAYMENT_AVID_PAY_FILE:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_AP_PAYMENTS . 'avidpay/', $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_CAPITAL_ONE:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), CFileType::PATH_AP_PAYMENTS . 'capital_one/', $intFileTypeId, date( 'Y' ), date( 'm' ), date( 'd' ) );
				break;

			case CFileType::SYSTEM_CODE_DOCUMENT_MANAGEMENT_UNCATEGORIZED_DOCUMENT:
				$strPath = $this->getCid() . '/document_management_upload/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getId() . '/';
				break;

			default:
				if( true == in_array( $this->getFileTypeSystemCode(), CFileType::$c_arrstrTaxCreditCertificationFileTypes ) ) {
					$strPath = CFileType::PATH_HUD50059 . $this->getSubsidyCertificationId() . '/';
					break;
				}

				if( CFileType::SYSTEM_CODE_REPORT == $this->getFileTypeSystemCode() && $this->getIsHistoricalReports() ) {
					$strPath = 'historical_reports/' . $this->getReportHistoryId() . '/';
					break;
				}

				$strPath = ( int ) $intFileTypeId . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/';
				break;
		}

		return $strPath;
	}

	public function buildFileName( CFile $objFile, $strExtension = 'docx' ) {

		$strFileName = '';
		$arrstrExcludeDefinedCodes = [
			CFileType::SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT,
			CFileType::SYSTEM_CODE_UNIT_TRANSFER,
			CFileType::SYSTEM_CODE_LATE_NOTICE,
			CFileType::SYSTEM_CODE_COLLECTIONS_NOTICE,
			CFileType::SYSTEM_CODE_SENT_TO_COLLECTION,
			CFileType::SYSTEM_CODE_PRE_COLLECTION_LETTER_SENT,
			CFileType::SYSTEM_CODE_REFUND_STATEMENT,
			CFileType::SYSTEM_CODE_TENANT_STATEMENT,
			CFileType::SYSTEM_CODE_MILITARY_MIMO,
			CFileType::SYSTEM_CODE_NSF,
			CFileType::SYSTEM_CODE_MANAGEMENT_FEE_INVOICE,
			CFileType::SYSTEM_CODE_PARENTAL_CONSENT_FORM
		];

		if( false == is_null( $objFile->getFileTypeSystemCode() ) && false == in_array( $objFile->getFileTypeSystemCode(), $arrstrExcludeDefinedCodes ) ) {

			$strFileTypeName	= \Psi\CStringService::singleton()->strtolower( \Psi\CStringService::singleton()->preg_replace( '/\s+/', '_', $objFile->getFileTypeName() ) );
			$strFileTypeName	= \Psi\CStringService::singleton()->preg_replace( '/\//', '_', $strFileTypeName );

			$strFileName = $objFile->getId() . '_' . $strFileTypeName . '_' . date( 'Ymdhis' ) . '.' . $strExtension;
		} elseif( false != in_array( $objFile->getFileTypeSystemCode(), $arrstrExcludeDefinedCodes ) || CFileType::SYSTEM_CODE_NSF == $objFile->getFileTypeSystemCode() ) {
			$strFileName = $this->getTitle();
		} else {

			$strFileName = $objFile->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;
		}

		return $strFileName;
	}

	public function buildFileData( $strInputTypeFileName = 'lease_file_name', $objDatabase, $intFileIndex = NULL, $strCustomFileTitle = '' ) {

		if( true == empty( $_FILES ) && empty( $_POST ) && isset( $_SERVER['REQUEST_METHOD'] ) && 'post' == \Psi\CStringService::singleton()->strtolower( $_SERVER['REQUEST_METHOD'] ) ) {
			$strPostMaxSize = ini_get( 'post_max_size' );
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload. File size should not exceed more than {%s,0}B.', [ $strPostMaxSize ] ) ) );
			return false;
		}

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		if( false == is_numeric( $intFileIndex ) ) {
			if( true == valStr( $strCustomFileTitle ) ) {
				$this->setTitle( $strCustomFileTitle );
			} else {
				$this->setTitle( $_FILES[$strInputTypeFileName]['name'] );
			}
			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		} else {
			$this->setTitle( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
		}

		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strFileName = $this->buildFileName( $this, $strExtension );

		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extention', __( 'Please select a file to upload.' ) ) );
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( false == is_numeric( $intFileIndex ) ) {
			if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'] );
			if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) ) 		 $this->setFileError( $_FILES[$strInputTypeFileName]['error'] );
		} else {
			if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'][$intFileIndex] );
			if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) ) 		 $this->setFileError( $_FILES[$strInputTypeFileName]['error'][$intFileIndex] );
		}

		if( true == isset( $strFileName ) ) 								 $this->setFileName( $strFileName );
		if( true == isset( $strMimeType ) ) 								 $this->setFileType( $strMimeType );

		$this->setFileExtensionId( $objFileExtension->getId() );
		$this->setFilePath( $this->buildFilePath( $this->getFileTypeId() ) );

		return true;
	}

	public function buildIdentificationFileData( $strInputTypeFileName = 'identification_documents', $objDatabase, $intFileIndex = NULL, $strCustomFileTitle = '' ) {

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		if( false == is_numeric( $intFileIndex ) ) {
			if( true == valStr( $strCustomFileTitle ) ) {
				$this->setTitle( $strCustomFileTitle );
			} else {
				$this->setTitle( $_FILES[$strInputTypeFileName]['name'] );
			}
			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		} else {
			$this->setTitle( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
		}

		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strFileName = $this->buildFileName( $this, $strExtension );

		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extention', __( 'Please select a file to upload.' ) ) );
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( false == is_numeric( $intFileIndex ) ) {
			if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'] );
			if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) ) 		 $this->setFileError( $_FILES[$strInputTypeFileName]['error'] );
		} else {
			if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'][$intFileIndex] );
			if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) ) 		 $this->setFileError( $_FILES[$strInputTypeFileName]['error'][$intFileIndex] );
		}

		if( true == isset( $strFileName ) ) 								 $this->setFileName( $strFileName );
		if( true == isset( $strMimeType ) ) 								 $this->setFileType( $strMimeType );

		$this->setFileExtensionId( $objFileExtension->getId() );
		$this->setFilePath( $this->buildFilePath( $this->getFileTypeId() ) );

		return true;
	}

	public function buildFileDataFromBase64( $objDatabase, $intFileIndex = NULL, $arrmixFiles ) {

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		if( false == is_numeric( $intFileIndex ) ) {
			$this->setTitle( $arrmixFiles['name'] );
			$arrstrFileInfo = pathinfo( $arrmixFiles['name'] );
		}

		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strFileName = $this->buildFileName( $this, $strExtension );

		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( true == isset( $strFileName ) ) 								 $this->setFileName( $strFileName );
		if( true == isset( $strMimeType ) ) 								 $this->setFileType( $strMimeType );

		$this->setFileExtensionId( $objFileExtension->getId() );
		$this->setFilePath( $this->buildFilePath( $this->getFileTypeId() ) );
		return true;
	}

	public function buildFileDataByIndex( $strInputTypeFileName, $intIndex, $objDatabase ) {

		if( true == empty( $_FILES ) && empty( $_POST ) && isset( $_SERVER['REQUEST_METHOD'] ) && 'post' == \Psi\CStringService::singleton()->strtolower( $_SERVER['REQUEST_METHOD'] ) ) {
			$strPostMaxSize = ini_get( 'post_max_size' );
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload. File size should not exceed more than {%s,0}B.', [ $strPostMaxSize ] ) ) );
			return false;
		}

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		$this->setTitle( $_FILES[$strInputTypeFileName]['name'][$intIndex] );

		$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'][$intIndex] );

		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strFileName = $this->buildFileName( $this, $strExtension );

		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extention', __( 'Please select a file to upload.' ) ) );
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'][$intIndex] ) ) 	 $this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'][$intIndex] );
		if( true == isset( $_FILES[$strInputTypeFileName]['error'][$intIndex] ) ) 		 $this->setFileError( $_FILES[$strInputTypeFileName]['error'][$intIndex] );
		if( true == isset( $strFileName ) ) 								 $this->setFileName( $strFileName );
		if( true == isset( $strMimeType ) ) 								 $this->setFileType( $strMimeType );

		$this->setFileExtensionId( $objFileExtension->getId() );
		$this->setFilePath( $this->buildFilePath( $this->getFileTypeId() ) );

		return true;
	}

	public function buildFileDataIntegration( $strInputTypeFileName, $strDocumentFileName, $intFileTypeSystemCode, $objDatabase ) {

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		$arrstrFileInfo = pathinfo( $strDocumentFileName );
		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$this->setTitle( $strDocumentFileName );
		$this->setFileTypeName( $strInputTypeFileName );
		$this->setFileTypeSystemCode( $intFileTypeSystemCode );

		$strFileName = $this->buildFileName( $this, $strExtension );

		$strMimeType 		= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( true == isset( $strDocumentFileName ) ) 	$this->setTempFileName( $strDocumentFileName );
		if( true == isset( $strFileName ) ) 			$this->setFileName( $strFileName );
		if( true == isset( $strMimeType ) ) 			$this->setFileType( $strMimeType );

		$this->setFileExtensionId( $objFileExtension->getId() );
		$this->setFilePath( $this->buildFilePath( $this->getFileTypeId() ) );
	}

	public function buildAttachFileData( $strInputTypeFileName, $arrstrExtensions, $objDatabase ) {

		if( false == valArr( $arrstrExtensions ) ) {
			$arrstrExtensions = [ 'jpg', 'jpeg' ];
		}

		$strMessage = __( 'Allowed file extensions are: {%s,0}.', [ implode( ', ', $arrstrExtensions ) ] );

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		// The default 'POST_MAX_SIZE' and 'UPLOAD_MAX_FILESIZE' are set to 50M So when we are uploading a file having size greater than 50 MB we didn't get $_FILES info
		// So as per the limit, adding validation message.
		if( false == isset( $_FILES[$strInputTypeFileName] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'The attachment size exceeds the allowable limit of 50MB.' ) ) );
			return false;
		}

		if( false == isset( $_FILES[$strInputTypeFileName]['name'] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strMessage ) );
			return false;
		}
		$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );

		// in Db length of field 'title' is 50 character
		if( 50 >= \Psi\CStringService::singleton()->strlen( $_FILES[$strInputTypeFileName]['name'] ) ) {
			$this->setTitle( $_FILES[$strInputTypeFileName]['name'] );
		} else {
			$this->setTitle( \Psi\CStringService::singleton()->substr( $_FILES[$strInputTypeFileName]['name'], 0, 40 ) . '.' . $arrstrFileInfo['extension'] );
		}

		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? \Psi\CStringService::singleton()->strtolower( $arrstrFileInfo['extension'] ) : NULL;

		if( false == is_null( $strExtension ) && false == in_array( $strExtension, $arrstrExtensions ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strMessage ) );
			return false;
		}

		$strFileName 		= $this->buildFileName( $this, $strExtension );

		$strMimeType 		= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select a file to upload.' ) ) );
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) 	$this->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'] );
		if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) ) 	 	$this->setFileError( $_FILES[$strInputTypeFileName]['error'] );
		if( true == isset( $strFileName ) ) 								$this->setFileName( $strFileName );
		if( true == isset( $strMimeType ) ) 								$this->setFileType( $strMimeType );

		$this->setFileExtensionId( $objFileExtension->getId() );

		$this->setFilePath( $this->buildFilePath( $this->getFileTypeId() ) );

		return true;
	}

	public function buildFileDataInvoiceForWebservice( $strInputFileName, $objDatabase ) {

		$arrstrExtension	= [ 'pdf' ];

		if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		if( false == isset( $strInputFileName ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File name not found.' ) ) );
			return false;
		}
		$arrstrFileInfo = pathinfo( $strInputFileName );

		// in Db length of field 'title' is 255 character.
		if( 255 >= \Psi\CStringService::singleton()->strlen( $arrstrFileInfo['filename'] ) ) {
			$this->setTitle( $arrstrFileInfo['filename'] );
		} else {
			$this->setTitle( \Psi\CStringService::singleton()->substr( $arrstrFileInfo['filename'], 0, 255 ) );
		}

		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? \Psi\CStringService::singleton()->strtolower( $arrstrFileInfo['extension'] ) : NULL;

		if( false == is_null( $strExtension ) && false == in_array( $strExtension, $arrstrExtension ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Only {%s,0} file formats are supported for invoice images.', [ implode( ',', $arrstrExtension ) ] ) ) );
			return false;
		}

		$strFileName 		= $this->buildFileName( $this, $strExtension );

		$strMimeType 		= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load file extensions.' ) ) );
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( true == isset( $strFileName ) ) $this->setFileName( $strFileName );
		if( true == isset( $strMimeType ) ) $this->setFileType( $strMimeType );

		$this->setFileExtensionId( $objFileExtension->getId() );

		$this->setFilePath( $this->buildFilePath( $this->getFileTypeId() ) );

		return true;
	}

	public function buildIdentificationDocumentFileData( $strInputTypeFileName ='identification_documents', $strValidateAction, $objClient, $objDatabase, $intFileIndex = NULL ) {

		if( false == $this->buildIdentificationFileData( $strInputTypeFileName, $objDatabase, $intFileIndex ) ) {
			return false;
		}

		if( false == is_numeric( $intFileIndex ) ) {
			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		} else {
			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
		}

		$strExtension = ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$this->setCid( $objClient->getId() );

		if( true == is_null( $this->getFileTypeId() ) ) {
			$objFileType = $objClient->fetchFileTypeBySystemCode( CFileType::SYSTEM_CODE_APPLICANT_IDENTIFICATION, $objDatabase );
			$this->setFileTypeId( $objFileType->getId() );
			$this->setFileTypeName( $objFileType->getFileTypeName() );
			$this->setFileTypeSystemCode( $objFileType->getSystemCode() );
		}

		$this->setFilePath( $this->buildFilePath( $this->getFileTypeId() ) );
		$this->setFileName( $this->buildFileName( $this, $strExtension ) );
		$this->setShowInPortal( 1 );

		$this->setFileUploadDate( 'Now()' );
		if( false == $this->validate( $strValidateAction ) ) {
			return false;
		}
		return true;
	}

	public function buildLeaseDocumentFileData( $strInputTypeFileName ='lease_file_name', $strValidateAction, $objClient, $objApplicantApplication, $objDatabase ) {

		if( true == empty( $_FILES ) && empty( $_POST ) && isset( $_SERVER['REQUEST_METHOD'] ) && 'post' == \Psi\CStringService::singleton()->strtolower( $_SERVER['REQUEST_METHOD'] ) ) {
			$strPostMaxSize = ini_get( 'post_max_size' );
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload the document. Document size should not exceed {%s,0}B.', [ $strPostMaxSize ] ) ) );
			return false;
		}

		if( false == isset( $_FILES[$strInputTypeFileName] ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $_FILES[$strInputTypeFileName]['name'] ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select a file to upload.' ) ) );
			return false;
		}

		if( false == $this->buildFileData( $strInputTypeFileName, $objDatabase ) ) {
			return false;
		}

		$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		if( CClients::$c_intNullCid == $this->getCid() ) {
			$this->setCid( $objClient->getId() );
		}

		if( true == is_null( $this->getFileTypeId() ) ) {
			$objFileType = $objClient->fetchFileTypeBySystemCode( CFileType::SYSTEM_CODE_LEASE_ADDENDUM, $objDatabase );
			$this->setFileTypeId( $objFileType->getId() );
		}

		$this->setFilePath( $objClient->getId() . '/' . $this->getPropertyId() . '/leasing/' . $objApplicantApplication->buildFilePath() );
		$this->setFileName( $objApplicantApplication->buildFileAssociationFileName( $strExtension ) );
		$this->setShowInPortal( 1 );

		if( 'valid_upload_signed_document' == $strValidateAction ) $this->setFileUploadDate( 'Now()' );

		if( false == $this->validate( $strValidateAction ) ) {
			return false;
		}

		return true;
	}

	public function buildUploadFileData( $strInputTypeFileName, $boolRequiresignature, $objDatabase ) {

		if( false == $this->buildFileData( $strInputTypeFileName, $objDatabase ) ) {
			return false;
		}

		if( true == $boolRequiresignature ) {
			if( false == $this->validate( 'valid_upload_sign_document' ) ) {
				return false;
			}
		}

		return true;
	}

	public function createFileMergeValue() {

		$objFileMergeValue = new CFileMergeValue();
		$objFileMergeValue->setCid( $this->m_intCid );
		$objFileMergeValue->setFileId( $this->m_intId );
		$objFileMergeValue->setApplicationId( $this->m_intApplicationId );
		$objFileMergeValue->setLeaseId( $this->m_intLeaseId );

		return $objFileMergeValue;
	}

	public function uploadFileDocument() {

		// TODO: This function is going to be deprecated, Please use uploadObject() with IObjectStorageGateway.

		$boolIsValid 	= true;

		$strUploadPath 	= getMountsPath( $this->getCid(), PATH_MOUNTS_FILES ) . $this->getFilePath();

		if( false == CFileIo::isDirectory( $strUploadPath ) ) {
			if( false == CFileIo::recursiveMakeDir( $strUploadPath ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				$boolIsValid = false;
			}
		}

		if( false == move_uploaded_file( $this->getTempFileName(), $strUploadPath . $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select a file to upload.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function computePageCount( $objObjectStorageGateway, $objDatabase ) {

		$objZendPdf = CLeaseExecutionCommonFunctions::loadZendPdf( $objObjectStorageGateway, $this, $objDatabase );

		if( true == is_null( $objZendPdf ) ) return false;

		$this->setPageCount( \Psi\Libraries\UtilFunctions\count( $objZendPdf->pages ) );

		$this->setTotalPageCount( \Psi\Libraries\UtilFunctions\count( $objZendPdf->pages ) );
		$this->setUnsignedPageCount( '' );
	}

	public function addFileEvent( $intFileEventType, $arrstrEventParameters, $objCompanyUser, $objDatabase, $boolIsPerformTransaction = true ) {

		if( false == valStr( $intFileEventType ) ) return false;

		$intCompanyUserId = CCompanyUser::SYSTEM;
		$strCompanyUserName = NULL;

		if( false != valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$intCompanyUserId = $objCompanyUser->getId();
			$strCompanyUserName = $objCompanyUser->getUsername( false );
		}

		$objFileEvent = new CFileEvent();
		$objFileEvent->setCid( $this->getCid() );
		$objFileEvent->setFileId( $this->getId() );
		$objFileEvent->setCompanyUserId( $intCompanyUserId );
		$objFileEvent->setFileEventTypeId( $intFileEventType );

		$objEventDescriptorLibrary = new CFileEventDescriptorLibrary( $objFileEvent, $arrstrEventParameters );
		$objEventDescriptorLibrary->setDatabase( $objDatabase );
		$objEventDescriptorLibrary->setReference( $this );

		$objEventDescriptorLibrary->setCompanyEmployeeName( $strCompanyUserName );

		$objEventDescriptorLibrary->buildEventDescription();

		if( false == $boolIsPerformTransaction ) return $objFileEvent;

		if( false == $objFileEvent->insert( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function getPdfFileName() {
		return str_replace( \Psi\CStringService::singleton()->strtolower( pathinfo( $this->getFileName(), PATHINFO_EXTENSION ) ), 'pdf', $this->getFileName() );
	}

	private function cleanFileNameAndLimitChars( $strValue, $intCharLength ) {
		$strValue = convertNonAsciiCharactersToAscii( trim( $strValue ) );
		$strValue = \Psi\CStringService::singleton()->preg_replace( '/[^a-zA-Z0-9_\-\.\s]/', '', $strValue );
		$strValue = \Psi\CStringService::singleton()->substr( $strValue, 0, $intCharLength );
		$strValue = \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( $strValue ) );
		$strValue = \Psi\CStringService::singleton()->preg_replace( '/\s+/', '-', $strValue );

		return $strValue;
	}

	public function generateDocumentName( $strDocumentNamingConventionFormat, $arrmixDocumentNamingData ) {

		if( false == valStr( $strDocumentNamingConventionFormat ) ) {
			return pathinfo( $this->getTitle(), PATHINFO_FILENAME );
		}

		$strDocumentName = $strDocumentNamingConventionFormat;
		$arrstrExcludedKeysForValidation = [ '*IMPORTED_DATE*', '*ROW_NUMBER*' ];

		foreach( $arrmixDocumentNamingData as $strKey => &$strValue ) {

			if( ( false == in_array( $strKey, $arrstrExcludedKeysForValidation ) && false == valStr( $strValue ) && false == valArr( $strValue ) ) || '*ROW_NUMBER*' == $strKey ) {
				continue;
			}

			switch( $strKey ) {
				case '*PROPERTY_NAME*':
					$intCharLength = 35;
					break;

				case '*IMPORTED_DATE*':
					$intCharLength = 10;
					break;

				case '*DOCUMENT_NUMBER*':
					$intCharLength = 12;
					break;

				case '*BUILDING_UNIT*':
					$intCharLength = 15;
					break;

				case '*DOCUMENT_TYPE*':
					$intCharLength = 15;
					break;

				case '*ENTITY_NAME_LAST_FIRST*':
					$intCharLength = 20;
					break;

				default:
					$intCharLength = 30;
			}

			// clean non-ascii characters; replace multiple spaces with hyphen(-); remove any non-alphanumeric values
			if( true == valArr( $strValue, 0 ) ) {
				foreach( $strValue as &$strSubValue ) {
					$strSubValue = $this->cleanFileNameAndLimitChars( $strSubValue, $intCharLength );
				}
			} else {
				$strValue = $this->cleanFileNameAndLimitChars( $strValue, $intCharLength );
			}
		}

		// replace tags
		if( true == valStr( $arrmixDocumentNamingData['*BUILDING_UNIT*'] ) ) {
			$strDocumentName = str_replace( '*BUILDING_UNIT*', $arrmixDocumentNamingData['*BUILDING_UNIT*'], $strDocumentName );
		} else {
			$strDocumentName = \Psi\CStringService::singleton()->preg_replace( '/\*BUILDING_UNIT\*#*/', '', $strDocumentName );
		}

		if( true == valStr( $arrmixDocumentNamingData['*DOCUMENT_TYPE*'] ) ) {
			$strDocumentName = str_replace( '*DOCUMENT_TYPE*', $arrmixDocumentNamingData['*DOCUMENT_TYPE*'], $strDocumentName );
		} else {
			$strDocumentName = \Psi\CStringService::singleton()->preg_replace( '/\*DOCUMENT_TYPE\*#*/', '', $strDocumentName );
		}

		if( true == valId( $this->getId() ) ) {
			$strDocumentName = str_replace( '*IMPORTED_DATE*', date( 'Y-m-d', strtotime( $this->getCreatedOn() ) ), $strDocumentName );
		} else {
			$strDocumentName = \Psi\CStringService::singleton()->preg_replace( '/\*IMPORTED_DATE\*#*/', date( 'Y-m-d' ), $strDocumentName );
		}

		if( 'Choose-One' == $arrmixDocumentNamingData['*PROPERTY_NAME*'] ) {
			$strDocumentName = str_replace( '*PROPERTY_NAME*', '', $strDocumentName );
		} else if( true == valStr( $arrmixDocumentNamingData['*PROPERTY_NAME*'] ) ) {
			$strDocumentName = str_replace( '*PROPERTY_NAME*', $arrmixDocumentNamingData['*PROPERTY_NAME*'], $strDocumentName );
		} else {
			$strDocumentName = \Psi\CStringService::singleton()->preg_replace( '/\*PROPERTY_NAME\*#*/', '', $strDocumentName );
		}

		if( true == valStr( $arrmixDocumentNamingData['*DOCUMENT_NUMBER*'] ) ) {
			$strDocumentName = str_replace( '*DOCUMENT_NUMBER*', $arrmixDocumentNamingData['*DOCUMENT_NUMBER*'], $strDocumentName );
		} else {
			$strDocumentName = \Psi\CStringService::singleton()->preg_replace( '/\*DOCUMENT_NUMBER\*#*/', '', $strDocumentName );
		}

		$strEntityName = '';

		if( true == valArr( $arrmixDocumentNamingData['*ENTITY_NAME_LAST_FIRST*'] ) ) {
			$arrstrEntityNames = $arrmixDocumentNamingData['*ENTITY_NAME_LAST_FIRST*'];

			// if row number given then use entity name by row number; else join then by underscore (_)
			if( true == valStr( $arrmixDocumentNamingData['*ROW_NUMBER*'] ) && true == isset( $arrstrEntityNames[$arrmixDocumentNamingData['*ROW_NUMBER*']] ) ) {
				$strEntityName = $arrstrEntityNames[$arrmixDocumentNamingData['*ROW_NUMBER*']];
			} else {
				$strEntityName = implode( '_', $arrstrEntityNames );
			}
		} elseif( true == valStr( $arrmixDocumentNamingData['*ENTITY_NAME_LAST_FIRST*'] ) ) {
			$strEntityName = $arrmixDocumentNamingData['*ENTITY_NAME_LAST_FIRST*'];
		}

		if( true == valStr( $strEntityName ) ) {
			$strDocumentName = str_replace( '*ENTITY_NAME_LAST_FIRST*', $strEntityName, $strDocumentName );
		} else {
			$strDocumentName = \Psi\CStringService::singleton()->preg_replace( '/\*ENTITY_NAME_LAST_FIRST\*#*/', '', $strDocumentName );
		}

		$strDocumentName = trim( $strDocumentName, '#' );
		$strDocumentName = str_replace( '#', '_', $strDocumentName );

		return ( true == valStr( $strDocumentName ) && true == \Psi\CStringService::singleton()->preg_match( '/[^_]/', trim( $strDocumentName ) ) ) ? $strDocumentName : pathinfo( $this->getTitle(), PATHINFO_FILENAME );
	}

	public function checkCountersignRequiredForFile() {

		if( true == is_null( $this->getCountersignedBy() ) && true == is_null( $this->getCountersignedOn() ) ) return true;

		if( false == valArr( $this->getFiles() ) ) return false;

		foreach( $this->getFiles() as $objFile ) {
			if( true == is_null( $objFile->getCountersignedBy() ) && true == is_null( $objFile->getCountersignedOn() ) ) {
				return true;
			}
		}

		return false;
	}

	public function copyEncryptedFile( $objDocumentManager, $strFolderName = PATH_MOUNTS_FILES, $objDatabase, $intCurrentUserId = NULL, $strDownloadFilePath = NULL, $strCopyPath = NULL ) {

		$strPath = $this->getFilePath() . $this->getFileName();

		$strTempFullPath = $objDocumentManager->getDocumentToTempFile( CDocumentCriteria::forGet( $this->getId(), $strFolderName, $strPath ) );
		$strPdfFilePath = NULL;

		if( false === valStr( $strTempFullPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$arrstrFileParts = pathinfo( $strTempFullPath );

		$strFileName = $this->getFileName();
		$strPdfFileName = '';

		if( false !== \Psi\CStringService::singleton()->strpos( $strFileName, '.html' ) ) {
			$strPdfFileName = str_replace( '.html', '.pdf', $strFileName );
			$strPdfFilePath = preg_replace( '#\/[^/]*$#', '', $strPath );
		}

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_APPLICATION && true == valStr( $strPdfFilePath )
		    && true == $objDocumentManager->documentExists( CDocumentCriteria::forExists( -1, PATH_MOUNTS_DOCUMENTS, $strPath ) ) && NULL != $intCurrentUserId ) {

			if( false == $objDocumentManager->putDocument( CDocumentCriteria::forPut( $this->getId(), $strFolderName, $strPdfFilePath . $strPdfFileName, $strOutput ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}

			$this->setFileExtensionId( CFileExtension::APPLICATION_PDF );
			$this->setFileName( $strPdfFileName );
			$this->update( $intCurrentUserId, $objDatabase );

			if( false == $objDocumentManager->removeDocument( CDocumentCriteria::forRemove( $this->getId(), $strFolderName, $strPath ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}

			if( true == CFileIo::fileExists( $strDownloadFilePath ) ) {
				unlink( $strDownloadFilePath );
			}

		} else {

			if( true == CFileIo::fileExists( $strDownloadFilePath ) ) {
				unlink( $strDownloadFilePath );
			}

			CFileIo::filePutContents( $strCopyPath, CFileIo::fileGetContents( $strTempFullPath ) );
		}
	}

	// FIXME : We will be fixing the coding standard issues after committing our project on trunk.

	public function copyEncryptedObject( $objStorageGateway, $strFolderName = PATH_MOUNTS_FILES, $objDatabase, $intCurrentUserId = NULL, $strDownloadFilePath = NULL, $strCopyPath = NULL ) {

		$strPath = $this->getFilePath() . $this->getFileName();
		$intCid = $this->getCid();

		$intObjectId = ( true == valId( $this->getId() ) ) ? $this->getId() : -1;

		$objStorageGatewayResponse = $objStorageGateway->getObject( [
			'cid'           => $intCid,
			'objectId'      => $this->getId(),
			'container'     => $strFolderName,
			'key'           => $strPath,
			'outputFile'    => 'temp'
		] );

		$strTempFullPath = $objStorageGatewayResponse['outputFile'];

		$strPdfFilePath = NULL;

		if( false === valStr( $strTempFullPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$arrstrFileParts = pathinfo( $strTempFullPath );

		$strFileName = $this->getFileName();
		$strPdfFileName = '';

		if( false !== \Psi\CStringService::singleton()->strpos( $strFileName, '.html' ) ) {
			$strPdfFileName = str_replace( '.html', '.pdf', $strFileName );
			$strPdfFilePath = preg_replace( '#\/[^/]*$#', '', $strPath );
		}

		if( true == valArr( $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
			$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		$objStorageGatewayResponse = $objStorageGateway->getObject( [
			'cid'         => $intCid,
			'objectId'    => -1,
			'container'   => PATH_MOUNTS_DOCUMENTS,
			'key'         => $strPath,
			'checkExists' => true
		] );

		if( $this->getFileTypeSystemCode() == CFileType::SYSTEM_CODE_APPLICATION && true == valStr( $strPdfFilePath )
		    && true == $objStorageGatewayResponse['isExists'] && NULL != $intCurrentUserId ) {

			$arrmixRequest = $this->createPutGatewayRequest( [ 'data' => $strOutput ] );
			$objObjectStorageGatewayResponse = $this->getObjectStorageGateway()->putObject( $arrmixRequest );
			$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );

			if( true == is_null( $objObjectStorageGatewayResponse ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}

			$this->setFileExtensionId( CFileExtension::APPLICATION_PDF );
			$this->setFileName( $strPdfFileName );
			$this->update( $intCurrentUserId, $objDatabase );

			$arrmixRequest = $this->fetchStoredObject( $this->getDatabase() )->createGatewayRequest();
			$objObjectStorageGatewayResponse = $objStorageGateway->deleteObject( $arrmixRequest );

			if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to encrypt pre-signed document' ) ) );
				return false;
			}

			if( true == CFileIo::fileExists( $strDownloadFilePath ) ) {
				unlink( $strDownloadFilePath );
			}

		} else {

			if( true == CFileIo::fileExists( $strDownloadFilePath ) ) {
				unlink( $strDownloadFilePath );
			}

			if( true == valStr( $strCopyPath ) ) {
				CFileIo::filePutContents( $strCopyPath, CFileIo::fileGetContents( $strTempFullPath ) );
			}
		}
		CFileIo::deleteFile( $strTempFullPath );

	}

	public function createMissingPolicyDocument( $objObjectStorageGateway ) {

		$objPolicyDocument = CDocuments::fetchDocumentByIdByCid( $this->getDocumentId(), $this->getCid(), $this->getDatabase() );
		if( false == valObj( $objPolicyDocument, 'CDocument' ) ) {
			return false;
		}

		// check if encrypted pdf policy document exist for company application
		$objStoredObject = $objPolicyDocument->fetchStoredObject( $this->m_objDatabase );
		$arrmixGatewayRequest = $objStoredObject->createGatewayRequest( [ 'outputFile' => 'temp' ] );
		$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );

		if( false == valStr( $arrobjObjectStorageResponse['outputFile'] ) || false == CFileIo::fileExists( $arrobjObjectStorageResponse['outputFile'] ) ) {
			// pdf does not exist, so check if docx policy document exist for the company application
			if( 'docx' == \Psi\CStringService::singleton()->strtolower( $objPolicyDocument->getFilenameExtension() ) ) {

				$objStoredObject = $objPolicyDocument->fetchStoredObject( $this->m_objDatabase );
				$arrmixGatewayRequest = $objStoredObject->createGatewayRequest( [ 'outputFile' => 'temp' ] );
				$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );
				if( false == valStr( $arrobjObjectStorageResponse['outputFile'] ) || false == CFileIo::fileExists( $arrobjObjectStorageResponse['outputFile'] ) ) {
					return false; // docx file also not exists
				}

				// docx exists, so convert it to pdf
				$objClient = CClients::fetchClientById( $this->getCid(), $this->getDatabase() );
				if( false == $this->convertDocxToPdf( $objObjectStorageGateway, $objClient, $objPolicyDocument ) ) {
					return false;
				}

				// docx to pdf converted successfully so try to create missing policy file
				return $this->createMissingPolicyDocument( $objObjectStorageGateway );
			}
		}
		$arrmixGatewayRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $arrobjObjectStorageResponse['outputFile'] ) ] );
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( $arrmixGatewayRequest );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			return false;
		}
		$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );
		$this->insertOrUpdateStoredObject( CCompanyUser::SYSTEM, $this->m_objDatabase );

		// missing policy file successfully created, return that
		return $arrobjObjectStorageResponse;
	}

	public function isDocumentExists( $objStorageGateway ) {

		$arrstrObjectStorageGatewayArgs = [
			'cid'         => $this->getCid(),
			'objectId'    => $this->getId(),
			'container'   => PATH_MOUNTS_FILES,
			'key'         => $this->getFilePath() . $this->getFileName(),
			'checkExists' => true
		];

		if( valObj( $this->m_objDatabase, 'CDatabase' ) ) $objStoredObject = $this->fetchStoredObject( $this->m_objDatabase );
		if( true == valId( $objStoredObject->getId() ) ) {
			$arrstrObjectStorageGatewayArgs['container'] = $objStoredObject->getContainer();
			$arrstrObjectStorageGatewayArgs['key'] = $objStoredObject->getKey();
		}

		$arrobjResponse = $objStorageGateway->getObject( $arrstrObjectStorageGatewayArgs );

		if( false == $arrobjResponse['isExists'] ) {
			return false;
		}
		return true;
	}

	public function createFileNote() {
		$objFileNote = new CFileNote();
		$objFileNote->setFileId( $this->getId() );
		$objFileNote->setCid( $this->getCid() );
		return $objFileNote;
	}

	public function convertDocxToPdf( $objObjectStorageGateway, $objClient, $objDocument ) {

		if( valObj( $objDocument->getDatabase(), 'CDatabase' ) ) $objStoredObject = $objDocument->fetchStoredObject( $objDocument->getDatabase() );
		$objFileConverter = CFileConverterLibraryFactory::createLibrary( CFileConverterLibraryFactory::ECRION_LIBRARY, $objObjectStorageGateway );
		$objFileConverter->setClient( $objClient );
		$objFileConverter->setSourceDocument( clone $objDocument);
		$this->setFileName( str_replace( '.docx', '.pdf', $this->getFileName() ) );
		$objFileConverter->setDestinationFile( $this );

		$strFilePath = $objDocument->getFilePath();
		$strFileName = $objDocument->getFileName();

		$objFileConverter->setSourceFilePath( $strFilePath );
		$objFileConverter->setSourceFileName( $strFileName );

		$strFileName = str_replace( '.docx', '.pdf', \Psi\CStringService::singleton()->strtolower( $strFileName ) );

		$objFileConverter->setDestinationFilePath( $strFilePath );
		$objFileConverter->setDestinationFileName( $strFileName );
		$objFileConverter->setFileParentFolder( ( true == valId( $objStoredObject->getId() ) ) ? $objStoredObject->getContainer() : PATH_MOUNTS_DOCUMENTS );

		$objFileConverter->setDocumentId( $objDocument->getId() );

		if( false == $objFileConverter->convertDocxToPdf() ) {
			$this->addErrorMsg( $objFileConverter->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function getIsShowAttachment() {
		return $this->m_boolIsShowAttachment;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $strReferenceTag = NULL ) {
		$boolReturn = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( $boolReturn && false == $boolReturnSqlOnly ) {
			$boolReturn &= $this->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, NULL, $strReferenceTag );
		}

		return $boolReturn;
	}

	public function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		if( CFileType::SYSTEM_CODE_PETIMAGE === $this->getFileTypeSystemCode() ) {
			$strPath = $this->getFilePath() . $this->getFileName();
			if( 'thumbnail' == $strReferenceTag ) {
				$strPath = $this->getFilePath() . 'thumbnail/' . $this->getFileName();
			}
			return $strPath;
		}

		if( CFileType::SYSTEM_CODE_REPAYMENT_AGREEMENT == $this->getDocumentFileTypeSystemCode()
			&& CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE == $this->getFileTypeSystemCode() ) {
			$strPath = $this->getCid() . '/' . CFileType::PATH_REPAYMENTS . $this->getFileTypeId() . '/' . date( 'Y/m/d' ) . '/' . $this->getFileName();
			return $strPath;
		}

		if( CFileType::SYSTEM_CODE_RENTAL_COLLECTION_PAYMENT_PROOF == $this->getFileTypeSystemCode() && CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM == $strVendor ) {
			$strPath = $this->getFilePath() . $this->getApplicationId() . '/' . $this->getApplicantId() . '/' . $this->getTitle();
			return $strPath;
		}

		// TODO: This is obviously naive as we likely need to consider the file type. The point is we Centralize the calculation of the storage key to avoid this logic being spread out into controllers and various libraries
		if( 'pdf_preview' == $strReferenceTag ) {
			return $this->getFilePath() . $this->getPdfFileName();
		}

		if( true == in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_LEASE_DOCUMENT, CFileType::SYSTEM_CODE_SIGNED, CFileType::SYSTEM_CODE_PARTIALLY_SIGNED, CFileType::SYSTEM_CODE_LEASE_PACKET, CFileType::SYSTEM_CODE_LEASE_ADDENDUM ] ) && \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 == $strVendor ) {
			if( !in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_LEASE_PACKET, CFileType::SYSTEM_CODE_SIGNED, CFileType::SYSTEM_CODE_PARTIALLY_SIGNED, CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET ] ) && 1 == $this->getRequireCountersign() && 'approved' == $strReferenceTag ) {
				return $this->getCid() . '/' . $this->getFileParentFolderName() . $this->getOldSignedFilePath() . 'approved_' . $this->getFileName();
			} elseif( 'signed' == $strReferenceTag && !in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_SIGNED, CFileType::SYSTEM_CODE_PARTIALLY_SIGNED, CFileType::SYSTEM_CODE_POLICY ] ) ) {
				return $this->getCid() . '/' . $this->getFileParentFolderName() . $this->getOldSignedFilePath() . $this->getFileName();
			}

			return $this->getCid() . '/' . $this->getFileParentFolderName() . $this->getFilePath() . $this->getFileName();
		}

		// So far the Vendor parameter is not included in the calculation of the key as we are calculating the generalized path. We will utilize the $strVendor parameter once we define a vendor specific path
		if( !in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_LEASE_PACKET, CFileType::SYSTEM_CODE_SIGNED, CFileType::SYSTEM_CODE_PARTIALLY_SIGNED, CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET ] ) && 1 == $this->getRequireCountersign() && 'approved' == $strReferenceTag ) {
			return ( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM == $strVendor ) ? $this->getOldSignedFilePath() . 'approved_' . $this->getFileName() : $this->getFilePath() . 'approved_' . $this->getFileName();
		} elseif( ( ( false == is_null( $this->getFileSignedOn() ) && 1 == $this->getRequireSign() ) || 'signed' == $strReferenceTag ) ) {
			if( true == in_array( $this->getFileTypeSystemCode(), [
				CFileType::SYSTEM_CODE_LEASE_DOCUMENT,
				CFileType::SYSTEM_CODE_LEASE_ADDENDUM,
				CFileType::SYSTEM_CODE_LEASE_PACKET,
				CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE,
				CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET
			] )
			    && ( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM <> $strVendor ) ) {
				return $this->getFilePath() . 'signed_' . $this->getFileName();
			} elseif( 'signed' == $strReferenceTag && !in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_SIGNED, CFileType::SYSTEM_CODE_PARTIALLY_SIGNED, CFileType::SYSTEM_CODE_POLICY ] ) ) {
				return $this->getOldSignedFilePath() . $this->getFileName();
			}
		}

		if( ( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM == $strVendor ) && in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_CORPORATE_JOB_DOCUMENT, CFileType::SYSTEM_CODE_CORPORATE_AP_CONTRACT_DOCUMENT, CFileType::SYSTEM_CODE_LIEN_WAIVER ] ) ) {
			return ( valId( $this->getBudgetChangeOrderId() ) ) ? $this->getFilePath() . $this->getFileName() : $this->getFilePath() . $this->getTitle();
		}

		if( ( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM == $strVendor ) && CFileType::SYSTEM_CODE_DRAW_REQUEST_DOCUMENT == $this->getFileTypeSystemCode() ) {
			return $this->getFilePath() . $this->getFileName();
		}

		if( 'signed' != $strReferenceTag && !in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_PARTIALLY_SIGNED, CFileType::SYSTEM_CODE_SIGNATURE, CFileType::SYSTEM_CODE_SIGNED ] ) ) {
			return $this->getUnsignedFilePath() . $this->getFileName();
		}

		return $this->getFilePath() . $this->getFileName();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
				return CONFIG_AWS_CLIENTS_BUCKET_NAME;

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				$strFileTypeSystemcode = ( CFileType::SYSTEM_CODE_SIGNATURE == $this->getFileTypeSystemCode() && false == is_null( $this->getSignatureAssociatedFileFileType() ) ) ? $this->getSignatureAssociatedFileFileType() : NULL;
				return $this->getFileParentFolderName( $strFileTypeSystemcode );

			default:
				if( in_array( $this->getFileTypeSystemCode(), [
					CFileType::SYSTEM_CODE_SIGNED,
					CFileType::SYSTEM_CODE_PARTIALLY_SIGNED,
					CFileType::SYSTEM_CODE_LEASE_PACKET,
					CFILEType::SYSTEM_CODE_SIGNATURE,
					CFileType::SYSTEM_CODE_OTHER_ESIGNED_PACKET,
					CFileType::SYSTEM_CODE_LEASE_DOCUMENT,
					CFileType::SYSTEM_CODE_LEASE_ADDENDUM,
					CFileType::SYSTEM_CODE_POLICY,
					CFileType::SYSTEM_CODE_APPLICANT_IDENTIFICATION,
					CFileType::SYSTEM_CODE_MOVE_IN_STATEMENT,
					CFileType::SYSTEM_CODE_REFUND_STATEMENT,
					CFileType::SYSTEM_CODE_FINANCIAL_MOVE_OUT_STATEMENT,
					CFileType::SYSTEM_CODE_TENANT_STATEMENT,
					CFileType::SYSTEM_CODE_TRIPLE_NET_RECONCILIATION,
					CFileType::SYSTEM_CODE_LEASE_CLAUSE_DOCUMENT,
					CFileType::SYSTEM_CODE_ADD_ON,
					CFileType::SYSTEM_CODE_GUEST_CARD,
					CFileType::SYSTEM_CODE_LEASE_DATES_ADJUSTMENT,
					CFileType::SYSTEM_CODE_EARLY_TERMINATION_AGREEMENT,
					CFileType::SYSTEM_CODE_MOVE_IN_ADJUSTMENT,
					CFileType::SYSTEM_CODE_ADVERSE_ACTION_LETTER,
					CFileType::SYSTEM_CODE_SCREENING_DECISION_SUMMARY_LETTER,
					CFileType::SYSTEM_CODE_CUSTOMER_INVOICE,
					CFileType::SYSTEM_CODE_RV_CONSUMER_AUTHORIZATION,
					CFileType::SYSTEM_CODE_PARENTAL_CONSENT_FORM,
					CFileType::SYSTEM_CODE_GCIC,
					CFileType::SYSTEM_CODE_RENTAL_COLLECTION_PAYMENT_PROOF,
					CFileType::SYSTEM_CODE_UNIT_TRANSFER,
					CFileType::SYSTEM_CODE_RENEWAL_OFFER_ACCEPTED,
					CFileType::SYSTEM_CODE_RENEWAL_OFFER_SENT,
					CFileType::SYSTEM_CODE_NSF,
					CFileType::SYSTEM_CODE_REPAYMENT_AGREEMENT,
					CFileType::SYSTEM_CODE_PROOF_OF_LAWFUL_PRESENCE,
					CFileType::SYSTEM_CODE_CERTIFICATE_OF_RENT_PAID,
					CFileType::SYSTEM_CODE_LEASE_ESA,
					CFileType::SYSTEM_CODE_MILITARY_DD_1746,
					CFileType::SYSTEM_CODE_DOCUMENT_MANAGEMENT_UNCATEGORIZED_DOCUMENT
				] ) || ( in_array( $this->getDocumentFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_LEASE_DOCUMENT, CFileType::SYSTEM_CODE_REPAYMENT_AGREEMENT ] )
						 && CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE == $this->getFileTypeSystemCode() ) ) {
					return CONFIG_OSG_BUCKET_LEASING;
				} elseif( CFileType::SYSTEM_CODE_REPORT === $this->getFileTypeSystemCode() ) {
					return CONFIG_OSG_BUCKET_REPORTS;
				} elseif( CFileType::SYSTEM_CODE_CHAT === $this->getFileTypeSystemCode() || CFileType::SYSTEM_CODE_MESSAGE_CENTER === $this->getFileTypeSystemCode() ) {
					if( CFileType::SYSTEM_CODE_CHAT === $this->getFileTypeSystemCode() || true == valId( $this->getCampaignId() ) || true == valId( $this->getScheduledEmailId() ) || true == $this->m_boolIsSystemMessageAttachment ) {
						return CONFIG_OSG_BUCKET_COMMUNICATIONS;
					} else {
						return $this->getFileParentFolderName();
					}
				} elseif( in_array( $this->getFileTypeSystemCode(), [ CFileType::SYSTEM_CODE_MAINTENANCE_ATTACHMENT, CFileType::SYSTEM_CODE_INSPECTION_SIGNATURE ], true ) ) {
					return CONFIG_OSG_BUCKET_MAINTENANCE;
				} elseif( CFileType::SYSTEM_CODE_INSURANCE_CLAIM === $this->getFileTypeSystemCode() ) {
					return CONFIG_OSG_BUCKET_INSURANCE;
				} elseif( CFileType::SYSTEM_CODE_USER_DOCUMENT === $this->getFileTypeSystemCode() || CFileType::SYSTEM_CODE_VIOLATION_MANAGER === $this->getFileTypeSystemCode() ) {
					return CONFIG_OSG_BUCKET_DOCUMENTS;
 				} elseif( CFileType::SYSTEM_CODE_PETIMAGE === $this->getFileTypeSystemCode() ) {
					return CONFIG_OSG_BUCKET_MEDIA_LIBRARY;
				} elseif( in_array( $this->getFileTypeSystemCode(), [
					CFileType::SYSTEM_CODE_INVOICE,
					CFileType::SYSTEM_CODE_JOURNAL_ENTRY_ATTACHMENT,
					CFileType::SYSTEM_CODE_PURCHASE_ORDER,
					CFileType::SYSTEM_CODE_CORPORATE_JOB_DOCUMENT,
					CFileType::SYSTEM_CODE_CORPORATE_AP_CONTRACT_DOCUMENT,
					CFileType::SYSTEM_CODE_LIEN_WAIVER,
					CFileType::SYSTEM_CODE_DRAW_REQUEST_DOCUMENT,
					CFileType::SYSTEM_CODE_AP_PAYMENT_DOCUMENT,
					CFileType::SYSTEM_CODE_GL_RECONCILIATION,
					CFileType::SYSTEM_CODE_AP_PAYMENT_CHECK_COPY_DOCUMENT,
					CFileType::SYSTEM_CODE_AP_PAYMENT_AVID_PAY_FILE,
					CFileType::SYSTEM_CODE_NACHA,
					CFileType::SYSTEM_CODE_WFPM,
					CFileType::SYSTEM_CODE_CAPITAL_ONE

				] ) ) {
					return CONFIG_OSG_BUCKET_ACCOUNTING;
				} else {
					return $this->getFileParentFolderName();
				}
		}

	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	/**
	 * This function returns true if the storage gateway object set successfuly for Resident Verify.
	 */
	public function generateAndPutRvStorageGatewayObject( $arrmixRequestData, $objObjectStorageGateway ) {

		$arrmixPutGatewayRequest = $this->createPutGatewayRequest( $arrmixRequestData );
		$objPutGatewayResponse   = $objObjectStorageGateway->putObject( $arrmixPutGatewayRequest );

		if( true == $objPutGatewayResponse->hasErrors() ) {
			return false;
		}
		$this->setObjectStorageGatewayResponse( $objPutGatewayResponse );

		return true;
	}

	public function deleteFileAssociations( $intApplicationId, $intCompanyUserId ) {
		if( false == \valId( $intApplicationId ) ) {
			return NULL;
		}

		$strSql = 'UPDATE 
						file_associations 
					SET 
						deleted_on = NOW(),
						deleted_by = ' . ( int ) $intCompanyUserId . '
					WHERE 
						file_id = ' . $this->getId() . '
						AND application_id = ' . ( int ) $intApplicationId . '
						AND cid = ' . $this->getCid();

		$arrmixResult = executeSql( $strSql, $this->getDatabase() );
		return ( true == getArrayElementByKey( 'failed', $arrmixResult ) ) ? false : true;
	}

	public function deleteIdentificationFile( $intCompanyUserId ) {

		$boolIsValid      = true;
		$arrobjFileEvents = ( array ) \Psi\Eos\Entrata\CFileEvents::createService()->fetchFileEventsByFileIdByCid( $this->getId(), $this->getCid(), $this->getDatabase() );

		$boolIsValid &= array_walk( $arrobjFileEvents, function ( $objFileEvent ) use ( $intCompanyUserId ) {
			if( false == $objFileEvent->validate( VALIDATE_DELETE ) ) {
				return false;
			}

			if( false == $objFileEvent->delete( $intCompanyUserId, $this->getDatabase() ) ) {
				return false;
			}
		} );

		if( false == $boolIsValid ) {
			return false;
		}

		if( false == $this->delete( $intCompanyUserId, $this->getDatabase() ) ) {
			return false;
		}

		return true;
	}

	/* Handles multiple file at a time from attachments of JE*/
	public function buildAttachFilesData( $strInputTypeFileName, $arrstrExtensions, $intTotalFileSize, $objDatabase ) {

		if( !valArr( $arrstrExtensions ) ) {
			$arrstrExtensions = [ 'jpg', 'jpeg' ];
		}

		$strMessage = __( 'Allowed file extensions are: {%s,0}.', [ implode( ', ', $arrstrExtensions ) ] );

		// The default 'POST_MAX_SIZE' and 'UPLOAD_MAX_FILESIZE' are set to 50M So when we are uploading a file having size greater than 50 MB we didn't get $_FILES info
		// So as per the limit, adding validation message.
		if( !isset( $_FILES[$strInputTypeFileName] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'The attachment size exceeds the allowable limit of 50MB.' ) ) );
			return;
		}

		if( !isset( $_FILES[$strInputTypeFileName]['name'] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strMessage ) );
			return;
		}

		$arrobjFiles = [];
		$intTotalFiles = count( $_FILES[$strInputTypeFileName]['name'] );

		for ( $intFileIndex = 0; $intFileIndex < $intTotalFiles; $intFileIndex++ ) {

			$objFile = new CFile();
			$objFile->setId( $this->fetchNextId( $objDatabase ) );
			$objFile->setCid( $this->getCid() );

			if ( isset($_FILES['gl_entry_file_name']['size'][$intFileIndex] ) ) {
				$objFile->setFileSize($_FILES['gl_entry_file_name']['size'][$intFileIndex] + $intTotalFileSize);
			}

			$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );

			// in Db length of field 'title' is 50 character
			if( 50 >= \Psi\CStringService::singleton()->strlen( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] ) ) {
				$objFile->setTitle( $_FILES[$strInputTypeFileName]['name'][$intFileIndex] );
			} else {
				$objFile->setTitle( \Psi\CStringService::singleton()->substr( $_FILES[$strInputTypeFileName]['name'][$intFileIndex], 0, 40 ) . '.' . $arrstrFileInfo['extension'] );
			}

			$strExtension	= ( isset( $arrstrFileInfo['extension'] ) ) ? \Psi\CStringService::singleton()->strtolower( $arrstrFileInfo['extension'] ) : NULL;

			if( !is_null( $strExtension ) && !in_array( $strExtension, $arrstrExtensions ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strMessage ) );
				return;
			}

			$strMimeType 		= '';
			$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );
			if( valObj( $objFileExtension, 'CFileExtension' ) ) {
				$strMimeType = $objFileExtension->getMimeType();
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select a file to upload.' ) ) );
				return;
			}

			$strFileName 	= $objFile->buildFileName( $objFile, $strExtension );

			if( isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) ) {
				$objFile->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'][$intFileIndex] );
			}

			if( isset( $_FILES[$strInputTypeFileName]['error'] ) ) {
				$objFile->setFileError( $_FILES[$strInputTypeFileName]['error'][$intFileIndex] );
			}

			if( isset( $strFileName ) ) {
				$objFile->setFileName( $strFileName );
			}

			if( isset( $strMimeType ) ) {
				$objFile->setFileType( $strMimeType );
			}

			$objFile->setFileExtensionId( $objFileExtension->getId() );
			$objFile->setFilePath( $objFile->buildFilePath( $objFile->getFileTypeId() ) );

			$arrobjFiles[] = $objFile;
		}

		return $arrobjFiles;
	}

}
?>
