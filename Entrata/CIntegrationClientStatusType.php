<?php

class CIntegrationClientStatusType extends CBaseIntegrationClientStatusType {

	const ACTIVE   = 1;
	const ON_HOLD  = 2;
	const DISABLED = 3;

	public static $c_arrstrIntegrationClientStatusTypeName = [
		self::ACTIVE	=> 'Active',
		self::ON_HOLD	=> 'On Hold',
		self::DISABLED	=> 'Disabled'
	];

}
?>