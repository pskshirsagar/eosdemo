<?php

class COwnerAssociation extends CBaseOwnerAssociation {

	protected $m_strOwnerName;
	protected $m_intPropertyId;
	protected $m_intShouldForwardDistributions;

	protected $m_fltCalculatedPercentage;

	protected $m_arrintParentOwnerIds;

	/**
	 * Get Functions
	 */

	public function getOwnerName() {
		return $this->m_strOwnerName;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getCalculatedPercentage() {
		return $this->m_fltCalculatedPercentage;
	}

	public function getParentOwnerIds() {
		return $this->m_arrintParentOwnerIds;
	}

	public function getShouldForwardDistributions() {
		return $this->m_intShouldForwardDistributions;
	}

	/**
	 * Set Functions
	 */

	public function setOwnerName( $strOwnerName ) {
		$this->m_strOwnerName = CStrings::strTrimDef( $strOwnerName, 100, NULL, true );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setCalculatedPercentage( $fltCalculatedPercentage ) {
		$this->m_fltCalculatedPercentage = $fltCalculatedPercentage;
	}

	public function setParentOwnerIds( $arrintParentOwnerIds ) {
		$this->m_arrintParentOwnerIds = $arrintParentOwnerIds;
	}

	public function setShouldForwardDistributions( $intShouldForwardDistributions ) {
		$this->m_intShouldForwardDistributions = CStrings::strToIntDef( $intShouldForwardDistributions, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['owner_name'] ) )							$this->setOwnerName( $arrmixValues['owner_name'] );
		if( true == isset( $arrmixValues['property_id'] ) )							$this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['calculated_percentage'] ) )				$this->setCalculatedPercentage( $arrmixValues['calculated_percentage'] );
		if( true == isset( $arrmixValues['should_forward_distributions'] ) )		$this->setShouldForwardDistributions( $arrmixValues['should_forward_distributions'] );

		return;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOwnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentOwnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPercentageOwnership() {

		$boolIsValid = true;

		if( 0 == $this->getPercentageOwnership() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage_ownership', 'Ownership percentage is required.' ) );
		} elseif( 0 > $this->getPercentageOwnership() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percentage_ownership', 'Ownership percentage should not be negative.' ) );
		}

		return $boolIsValid;
	}

	public function valIsParentOwnerAlreadyAssociated() {

		$boolIsValid = true;

		if( true == valArr( $this->getParentOwnerIds() ) && true == in_array( $this->getOwnerId(), $this->getParentOwnerIds() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_owner_exists', 'Selected owner is already added as a parent owner and cannot be added as a child owner beneath the same legal entity.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPercentageOwnership();
				$boolIsValid &= $this->valIsParentOwnerAlreadyAssociated();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>