<?php

class CJobPhaseUnitComment extends CBaseJobPhaseUnitComment {

	public function valJobPhaseId() : bool {
		$boolIsValid = true;
		if( true == is_null( $this->getJobPhaseId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_phase_id', __( 'Job phase id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valJobPhaseUnitId() : bool {
		$boolIsValid = true;
		if( true == is_null( $this->getJobPhaseUnitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_phase_unit_id', __( 'Job phase unit id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valComment() : bool {
		$boolIsValid = true;
		if( true == is_null( $this->getComment() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'comment', __( 'Comment is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valJobPhaseId();
				$boolIsValid &= $this->valJobPhaseUnitId();
				$boolIsValid &= $this->valComment();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>