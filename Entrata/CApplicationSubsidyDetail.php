<?php

class CApplicationSubsidyDetail extends CBaseApplicationSubsidyDetail {

	protected $m_strPreviousHeadNameFirst;
	protected $m_strPreviousHeadNameLast;
	protected $m_strPreviousHeadMiddleInitial;
	protected $m_strPreviousHeadBirthDate;
	protected $m_strProjectMoveInDate;
	protected $m_strSubsidyCertificationEffectiveDate;
	protected $m_strTransactionType;
	protected $m_strCorrectionTypeCode;
	protected $m_strPreviousSubsidy;
	protected $m_strEivIndicator;
	protected $m_strPreviousHousingCode;
	protected $m_strSection8Assistance1984Indicator;
	protected $m_strIncomeExceptionCode;
	protected $m_strNextRecertificationDate;
	protected $m_strTenantSignedDate;
	protected $m_strOwnerSignedDate;
	protected $m_strHhCitizenshipEligibility;
	protected $m_strAnticipatedVoucherDate;
	protected $m_strSecondarySubsidyType;
	protected $m_strSurvivorIndicator;
	protected $m_strWaiverTypeCode;
	protected $m_strBaselineCertificationIndicator;
	protected $m_strPlanOfActionIndicator;
	protected $m_strHudOwnedIndicator;
	protected $m_strUnitTransferCode;
	protected $m_strPreviousUnitNumber;
	protected $m_strMobilityDisability;
	protected $m_strHearingDisability;
	protected $m_strVisualDisability;
	protected $m_strEligibilityCheckNotRequired;
    protected $m_strIsRentOverride;
	protected $m_strPreviousActiveMat10EffectiveDate;

	protected $m_intOwnerGeneratedTenantIdNumber;
	protected $m_intPreviousHeadId;
	protected $m_intSubsidyPassbookRateId;
	protected $m_intActionProcessedCode;
	protected $m_intDisplacementStatusCode;
	protected $m_intNumberOfFamilyMembers;
	protected $m_intNumberOfNonFamilyMembers;
	protected $m_intNumberOfDependents;
	protected $m_intEligibilityUniverseCode;
	protected $m_intCurrentIncomeStatusCode;
	protected $m_intMinRentHardshipExemptionCode;
	protected $m_intPoliceOrSecurityTenant;
	protected $m_intBedroomCount;
	protected $m_intUnitNumber;
	protected $m_intFamilyAdditionAdoption;
	protected $m_intFamilyAdditionPregnancy;
	protected $m_intFamilyAdditionFosterChild;
	protected $m_intExtenuatingCircumstancesCode;

	protected $m_fltTotalCashValueOfAssets;
	protected $m_fltTotalIncomeFromAssets;
	protected $m_fltReportedPassbookRatePercent;
	protected $m_fltImputedIncomeFromAssets;
	protected $m_fltTotalEmploymentIncome;
	protected $m_fltTotalPensionIncome;
	protected $m_fltTotalPublicAssistanceIncome;
	protected $m_fltTotalOtherIncome;
	protected $m_fltNonAssetIncome;
	protected $m_fltAssetIncome;
	protected $m_fltAnnualIncomeAmount;
	protected $m_fltLowIncomeLimitAmount;
	protected $m_fltVeryLowIncomeLimitAmount;
	protected $m_fltExtremelyLowIncomeLimitAmount;
	protected $m_fltDependentDeduction;
	protected $m_fltMarketRent;
	protected $m_fltThreePercentOfIncome;
	protected $m_fltDisabilityEexpense;
	protected $m_fltDisabilityDeduction;
	protected $m_fltMedicalExpense;
	protected $m_fltMedicalDeduction;
	protected $m_fltElderlyFamilyDeduction;
	protected $m_fltTotalDeductions;
	protected $m_fltAdjustedIncomeAmount;
	protected $m_fltContractRentAmount;
	protected $m_fltUtilityAllowance;
	protected $m_fltGrossRent;
	protected $m_fltWelfareRent;
	protected $m_fltRentOverride;
	protected $m_fltTotalTenantPayment;
	protected $m_fltTenantRent;
	protected $m_fltUtilityReimbursement;
	protected $m_fltAssistancePaymentAmount;
	protected $m_fltSection236BasicBmirRent;
	protected $m_fltSecurityDeposit;
	protected $m_fltChildCareExpenseA;
	protected $m_fltChildCareExpenseB;
	protected $m_fltTtpAtRadConversion;
	protected $m_fltTtpBeforeOverride;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['previous_head_name_first'] ) ) 			$this->setPreviousHeadNameFirst( $arrmixValues['previous_head_name_first'] );
		if( true == isset( $arrmixValues['previous_head_last_name'] ) ) 			$this->setPreviousHeadNameLast( $arrmixValues['previous_head_last_name'] );
		if( true == isset( $arrmixValues['previous_head_middle_initial'] ) )		$this->setPreviousHeadMiddleInitial( $arrmixValues['previous_head_middle_initial'] );
		if( true == isset( $arrmixValues['previous_head_birth_date'] ) )			$this->setPreviousHeadBirthDate( $arrmixValues['previous_head_birth_date'] );
		if( true == isset( $arrmixValues['project_move_in_date'] ) )				$this->setProjectMoveInDate( $arrmixValues['project_move_in_date'] );
		if( true == isset( $arrmixValues['previous_active_mat10_effective_date'] ) ) $this->setPreviousActiveMat10EffectiveDate( $arrmixValues['previous_active_mat10_effective_date'] );
		if( true == isset( $arrmixValues['subsidy_certification_effective_date'] ) ) $this->setTransactionEffectiveDate( $arrmixValues['subsidy_certification_effective_date'] );
		if( true == isset( $arrmixValues['transaction_type'] ) )					$this->setTransactionType( $arrmixValues['transaction_type'] );
		if( true == isset( $arrmixValues['correction_type_code'] ) )				$this->setCorrectionTypeCode( $arrmixValues['correction_type_code'] );
		if( true == isset( $arrmixValues['previous_subsidy'] ) )					$this->setPreviousSubsidy( $arrmixValues['previous_subsidy'] );
		if( true == isset( $arrmixValues['eiv_indicator'] ) )						$this->setEivIndicator( $arrmixValues['eiv_indicator'] );
		if( true == isset( $arrmixValues['previous_housing_code'] ) )				$this->setPreviousHousingCode( $arrmixValues['previous_housing_code'] );
		if( true == isset( $arrmixValues['section_8_assistance_1984_indicator'] ) )	$this->setSection8Assistance1984Indicator( $arrmixValues['section_8_assistance_1984_indicator'] );
		if( true == isset( $arrmixValues['income_exception_code'] ) )				$this->setIncomeExceptionCode( $arrmixValues['income_exception_code'] );
		if( true == isset( $arrmixValues['next_recertification_date'] ) )			$this->setNextRecertificationDate( $arrmixValues['next_recertification_date'] );
		if( true == isset( $arrmixValues['tenant_signed_date'] ) )					$this->setTenantSignedDate( $arrmixValues['tenant_signed_date'] );
		if( true == isset( $arrmixValues['owner_signed_date'] ) )					$this->setOwnerSignedDate( $arrmixValues['owner_signed_date'] );
		if( true == isset( $arrmixValues['hh_citizenship_eligibility'] ) )			$this->setHhCitizenshipEligibility( $arrmixValues['hh_citizenship_eligibility'] );
		if( true == isset( $arrmixValues['anticipated_voucher_date'] ) )			$this->setAnticipatedVoucherDate( $arrmixValues['anticipated_voucher_date'] );
		if( true == isset( $arrmixValues['secondary_subsidy_type'] ) )				$this->setSecondarySubsidyType( $arrmixValues['secondary_subsidy_type'] );
		if( true == isset( $arrmixValues['survivor_indicator'] ) )					$this->setSurvivorIndicator( $arrmixValues['survivor_indicator'] );
		if( true == isset( $arrmixValues['waiver_type_code'] ) )					$this->setWaiverTypeCode( $arrmixValues['waiver_type_code'] );
		if( true == isset( $arrmixValues['baseline_certification_indicator'] ) )	$this->setBaselineCertificationIndicator( $arrmixValues['baseline_certification_indicator'] );
		if( true == isset( $arrmixValues['plan_of_action_indicator'] ) )			$this->setPlanOfActionIndicator( $arrmixValues['plan_of_action_indicator'] );
		if( true == isset( $arrmixValues['hud_owned_indicator'] ) )					$this->setHudOwnedIndicator( $arrmixValues['hud_owned_indicator'] );
		if( true == isset( $arrmixValues['unit_transfer_code'] ) )					$this->setUnitTransferCode( $arrmixValues['unit_transfer_code'] );
		if( true == isset( $arrmixValues['previous_unit_number'] ) )				$this->setPreviousUnitNumber( $arrmixValues['previous_unit_number'] );
		if( true == isset( $arrmixValues['mobility_disability'] ) )					$this->setMobilityDisability( $arrmixValues['mobility_disability'] );
		if( true == isset( $arrmixValues['hearing_disability'] ) )					$this->setHearingDisability( $arrmixValues['hearing_disability'] );
		if( true == isset( $arrmixValues['visual_disability'] ) )					$this->setVisualDisability( $arrmixValues['visual_disability'] );
		if( true == isset( $arrmixValues['eligibility_check_not_required'] ) )		$this->setEligibilityCheckNotRequired( $arrmixValues['eligibility_check_not_required'] );
		if( true == isset( $arrmixValues['owner_generated_tenant_id_number'] ) ) 	$this->setOwnerGeneratedTenantIdNumber( $arrmixValues['owner_generated_tenant_id_number'] );
		if( true == isset( $arrmixValues['previous_head_id'] ) ) 					$this->setPreviousHeadId( $arrmixValues['previous_head_id'] );
		if( true == isset( $arrmixValues['subsidy_passbook_rate_id'] ) ) 			$this->setSubsidyPassbookRateId( $arrmixValues['subsidy_passbook_rate_id'] );
		if( true == isset( $arrmixValues['action_processed_code'] ) ) 				$this->setActionProcessedCode( $arrmixValues['action_processed_code'] );
		if( true == isset( $arrmixValues['displacement_status_code'] ) ) 			$this->setDisplacementStatusCode( $arrmixValues['displacement_status_code'] );
		if( true == isset( $arrmixValues['number_of_family_members'] ) ) 			$this->setNumberOfFamilyMembers( $arrmixValues['number_of_family_members'] );
		if( true == isset( $arrmixValues['number_of_non_family_members'] ) ) 		$this->setNumberOfNonFamilyMembers( $arrmixValues['number_of_non_family_members'] );
		if( true == isset( $arrmixValues['number_of_dependents'] ) ) 				$this->setNumberOfDependents( $arrmixValues['number_of_dependents'] );
		if( true == isset( $arrmixValues['eligibility_universe_code'] ) ) 			$this->setEligibilityUniverseCode( $arrmixValues['eligibility_universe_code'] );
		if( true == isset( $arrmixValues['current_income_status_code'] ) ) 			$this->setCurrentIncomeStatusCode( $arrmixValues['current_income_status_code'] );
		if( true == isset( $arrmixValues['min_rent_hardship_exemption_code'] ) ) 	$this->setMinRentHardshipExemptionCode( $arrmixValues['min_rent_hardship_exemption_code'] );
		if( true == isset( $arrmixValues['police_or_security_tenant'] ) ) 			$this->setPoliceOrSecurityTenant( $arrmixValues['police_or_security_tenant'] );
		if( true == isset( $arrmixValues['bedroom_count'] ) ) 						$this->setBedroomCount( $arrmixValues['bedroom_count'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) 						$this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['family_addition_adoption'] ) ) 			$this->setFamilyAdditionAdoption( $arrmixValues['family_addition_adoption'] );
		if( true == isset( $arrmixValues['family_addition_pregnancy'] ) ) 			$this->setFamilyAdditionPregnancy( $arrmixValues['family_addition_pregnancy'] );
		if( true == isset( $arrmixValues['family_addition_foster_child'] ) ) 		$this->setFamilyAdditionFosterChild( $arrmixValues['family_addition_foster_child'] );
		if( true == isset( $arrmixValues['extenuating_circumstances_code'] ) ) 		$this->setExtenuatingCircumstancesCode( $arrmixValues['extenuating_circumstances_code'] );
		if( true == isset( $arrmixValues['total_cash_value_of_assets'] ) ) 			$this->setTotalCashValueOfAssets( $arrmixValues['total_cash_value_of_assets'] );
		if( true == isset( $arrmixValues['total_annual_income_from_assets'] ) ) 	$this->setTotalIncomeFromAssets( $arrmixValues['total_annual_income_from_assets'] );
		if( true == isset( $arrmixValues['reported_passbook_rate_percent'] ) ) 		$this->setReportedPassbookRatePercent( $arrmixValues['reported_passbook_rate_percent'] );
		if( true == isset( $arrmixValues['imputed_income_from_assets'] ) ) 			$this->setImputedIncomeFromAssets( $arrmixValues['imputed_income_from_assets'] );
		if( true == isset( $arrmixValues['employment_income'] ) ) 					$this->setTotalEmploymentIncome( $arrmixValues['employment_income'] );
		if( true == isset( $arrmixValues['pension_income'] ) ) 						$this->setTotalPensionIncome( $arrmixValues['pension_income'] );
		if( true == isset( $arrmixValues['public_assistance_income'] ) ) 			$this->setTotalPublicAssistanceIncome( $arrmixValues['public_assistance_income'] );
		if( true == isset( $arrmixValues['other_income'] ) ) 						$this->setTotalOtherIncome( $arrmixValues['other_income'] );
		if( true == isset( $arrmixValues['non_asset_income'] ) ) 					$this->setNonAssetIncome( $arrmixValues['non_asset_income'] );
		if( true == isset( $arrmixValues['asset_income'] ) ) 						$this->setAssetIncome( $arrmixValues['asset_income'] );
		if( true == isset( $arrmixValues['annual_income_amount'] ) ) 				$this->setAnnualIncomeAmount( $arrmixValues['annual_income_amount'] );
		if( true == isset( $arrmixValues['low_income_limit_amount'] ) ) 			$this->setLowIncomeLimitAmount( $arrmixValues['low_income_limit_amount'] );
		if( true == isset( $arrmixValues['very_low_income_limit_amount'] ) ) 		$this->setVeryLowIncomeLimitAmount( $arrmixValues['very_low_income_limit_amount'] );
		if( true == isset( $arrmixValues['extremely_low_income_limit_amount'] ) )	$this->setExtremelyLowIncomeLimitAmount( $arrmixValues['extremely_low_income_limit_amount'] );
		if( true == isset( $arrmixValues['dependent_deduction'] ) ) 				$this->setDependentDeduction( $arrmixValues['dependent_deduction'] );
		if( true == isset( $arrmixValues['market_rent'] ) ) 						$this->setMarketRent( $arrmixValues['market_rent'] );
		if( true == isset( $arrmixValues['three_percent_of_income'] ) ) 			$this->setThreePercentOfIncome( $arrmixValues['three_percent_of_income'] );
		if( true == isset( $arrmixValues['disability_expense'] ) ) 					$this->setDisabilityEexpense( $arrmixValues['disability_expense'] );
		if( true == isset( $arrmixValues['disability_deduction'] ) ) 				$this->setDisabilityDeduction( $arrmixValues['disability_deduction'] );
		if( true == isset( $arrmixValues['medical_expense'] ) ) 					$this->setMedicalExpense( $arrmixValues['medical_expense'] );
		if( true == isset( $arrmixValues['medical_deduction'] ) ) 					$this->setMedicalDeduction( $arrmixValues['medical_deduction'] );
		if( true == isset( $arrmixValues['elderly_deduction'] ) ) 					$this->setElderlyFamilyDeduction( $arrmixValues['elderly_deduction'] );
		if( true == isset( $arrmixValues['total_deduction'] ) ) 					$this->setTotalDeductions( $arrmixValues['total_deduction'] );
		if( true == isset( $arrmixValues['adjusted_income_amount'] ) ) 				$this->setAdjustedIncomeAmount( $arrmixValues['adjusted_income_amount'] );
		if( true == isset( $arrmixValues['contract_rent'] ) ) 				        $this->setContractRentAmount( $arrmixValues['contract_rent'] );
		if( true == isset( $arrmixValues['utility_allowance'] ) ) 					$this->setUtilityAllowance( $arrmixValues['utility_allowance'] );
		if( true == isset( $arrmixValues['gross_rent'] ) ) 							$this->setGrossRent( $arrmixValues['gross_rent'] );
		if( true == isset( $arrmixValues['welfare_rent'] ) ) 						$this->setWelfareRent( $arrmixValues['welfare_rent'] );
		if( true == isset( $arrmixValues['rent_override'] ) ) 						$this->setRentOverride( $arrmixValues['rent_override'] );
		if( true == isset( $arrmixValues['is_rent_override'] ) ) 					$this->setIsRentOverride( $arrmixValues['is_rent_override'] );
		if( true == isset( $arrmixValues['total_tenant_payment'] ) ) 				$this->setTotalTenantPayment( $arrmixValues['total_tenant_payment'] );
		if( true == isset( $arrmixValues['tenant_rent'] ) ) 						$this->setTenantRent( $arrmixValues['tenant_rent'] );
		if( true == isset( $arrmixValues['utility_reimbursement'] ) ) 				$this->setUtilityReimbursement( $arrmixValues['utility_reimbursement'] );
		if( true == isset( $arrmixValues['assistance_payment_amount'] ) ) 			$this->setAssistancePaymentAmount( $arrmixValues['assistance_payment_amount'] );
		if( true == isset( $arrmixValues['basic_or_bmir_rent'] ) ) 		            $this->setSection236BasicBmirRent( $arrmixValues['basic_or_bmir_rent'] );
		if( true == isset( $arrmixValues['security_deposit'] ) ) 					$this->setSecurityDeposit( $arrmixValues['security_deposit'] );
		if( true == isset( $arrmixValues['child_care_work'] ) ) 				    $this->setChildCareExpenseA( $arrmixValues['child_care_work'] );
		if( true == isset( $arrmixValues['child_care_school'] ) ) 				    $this->setChildCareExpenseB( $arrmixValues['child_care_school'] );
		if( true == isset( $arrmixValues['ttp_at_rad_conversion'] ) ) 				$this->setTtpAtRadConversion( $arrmixValues['ttp_at_rad_conversion'] );
		if( true == isset( $arrmixValues['ttp_before_override'] ) ) 				$this->setTtpBeforeOverride( $arrmixValues['ttp_before_override'] );

		return;
	}

	public function setPreviousHeadNameFirst( $strPreviousHeadNameFirst ) {
		$this->m_strPreviousHeadNameFirst = $strPreviousHeadNameFirst;
	}

	public function setPreviousHeadNameLast( $strPreviousHeadNameLast ) {
		$this->m_strPreviousHeadNameLast = $strPreviousHeadNameLast;
	}

	public function setPreviousHeadMiddleInitial( $strPreviousHeadMiddleInitial ) {
		$this->m_strPreviousHeadMiddleInitial = $strPreviousHeadMiddleInitial;
	}

	public function setPreviousHeadBirthDate( $strPreviousHeadBirthDate ) {
		$this->m_strPreviousHeadBirthDate = $strPreviousHeadBirthDate;
	}

	public function setPreviousActiveMat10EffectiveDate( $strPreviousActiveMat10EffectiveDate ) {
		$this->m_strPreviousActiveMat10EffectiveDate = $strPreviousActiveMat10EffectiveDate;
	}

	public function setProjectMoveInDate( $strProjectMoveInDate ) {
		$this->m_strProjectMoveInDate = $strProjectMoveInDate;
	}

	public function setTransactionEffectiveDate( $strSubsidyCertificationEffectiveDate ) {
		$this->m_strSubsidyCertificationEffectiveDate = $strSubsidyCertificationEffectiveDate;
	}

	public function setTransactionType( $strTransactionType ) {
		$this->m_strTransactionType = $strTransactionType;
	}

	public function setCorrectionTypeCode( $strCorrectionTypeCode ) {
		$this->m_strCorrectionTypeCode = $strCorrectionTypeCode;
	}

	public function setPreviousSubsidy( $strPreviousSubsidy ) {
		$this->m_strPreviousSubsidy = $strPreviousSubsidy;
	}

	public function setEivIndicator( $strEivIndicator ) {
		$this->m_strEivIndicator = $strEivIndicator;
	}

	public function setPreviousHousingCode( $strPreviousHousingCode ) {
		$this->m_strPreviousHousingCode = $strPreviousHousingCode;
	}

	public function setSection8Assistance1984Indicator( $strSection8Assistance1984Indicator ) {
		$this->m_strSection8Assistance1984Indicator = $strSection8Assistance1984Indicator;
	}

	public function setIncomeExceptionCode( $strIncomeExceptionCode ) {
		$this->m_strIncomeExceptionCode = $strIncomeExceptionCode;
	}

	public function setNextRecertificationDate( $strNextRecertificationDate ) {
		$this->m_strNextRecertificationDate = $strNextRecertificationDate;
	}

	public function setTenantSignedDate( $strTenantSignedDate ) {
		$this->m_strTenantSignedDate = $strTenantSignedDate;
	}

	public function setOwnerSignedDate( $strOwnerSignedDate ) {
		$this->m_strOwnerSignedDate = $strOwnerSignedDate;
	}

	public function setHhCitizenshipEligibility( $strHhCitizenshipEligibility ) {
		$this->m_strHhCitizenshipEligibility = $strHhCitizenshipEligibility;
	}

	public function setAnticipatedVoucherDate( $strAnticipatedVoucherDate ) {
		$this->m_strAnticipatedVoucherDate = $strAnticipatedVoucherDate;
	}

	public function setSecondarySubsidyType( $strSecondarySubsidyType ) {
		$this->m_strSecondarySubsidyType = $strSecondarySubsidyType;
	}

	public function setSurvivorIndicator( $strSurvivorIndicator ) {
		$this->m_strSurvivorIndicator = $strSurvivorIndicator;
	}

	public function setWaiverTypeCode( $strWaiverTypeCode ) {
		$this->m_strWaiverTypeCode = $strWaiverTypeCode;
	}

	public function setBaselineCertificationIndicator( $strBaselineCertificationIndicator ) {
		$this->m_strBaselineCertificationIndicator = $strBaselineCertificationIndicator;
	}

	public function setPlanOfActionIndicator( $strPlanOfActionIndicator ) {
		$this->m_strPlanOfActionIndicator = $strPlanOfActionIndicator;
	}

	public function setHudOwnedIndicator( $strHudOwnedIndicator ) {
		$this->m_strHudOwnedIndicator = $strHudOwnedIndicator;
	}

	public function setUnitTransferCode( $strUnitTransferCode ) {
		$this->m_strUnitTransferCode = $strUnitTransferCode;
	}

	public function setPreviousUnitNumber( $strPreviousUnitNumber ) {
		$this->m_strPreviousUnitNumber = $strPreviousUnitNumber;
	}

	public function setMobilityDisability( $strMobilityDisability ) {
		$this->m_strMobilityDisability = $strMobilityDisability;
	}

	public function setHearingDisability( $strHearingDisability ) {
		$this->m_strHearingDisability = $strHearingDisability;
	}

	public function setVisualDisability( $strVisualDisability ) {
		$this->m_strVisualDisability = $strVisualDisability;
	}

	public function setEligibilityCheckNotRequired( $strEligibilityCheckNotRequired ) {
		$this->m_strEligibilityCheckNotRequired = $strEligibilityCheckNotRequired;
	}

	public function setOwnerGeneratedTenantIdNumber( $intOwnerGeneratedTenantIdNumber ) {
		$this->m_intOwnerGeneratedTenantIdNumber = $intOwnerGeneratedTenantIdNumber;
	}

	public function setPreviousHeadId( $intPreviousHeadId ) {
		$this->m_intPreviousHeadId = $intPreviousHeadId;
	}

	public function setSubsidyPassbookRateId( $intSubsidyPassbookRateId ) {
		$this->m_intSubsidyPassbookRateId = $intSubsidyPassbookRateId;
	}

	public function setActionProcessedCode( $intActionProcessedCode ) {
		$this->m_intActionProcessedCode = $intActionProcessedCode;
	}

	public function setDisplacementStatusCode( $intDisplacementStatusCode ) {
		$this->m_intDisplacementStatusCode = $intDisplacementStatusCode;
	}

	public function setNumberOfFamilyMembers( $intNumberOfFamilyMembers ) {
		$this->m_intNumberOfFamilyMembers = $intNumberOfFamilyMembers;
	}

	public function setNumberOfNonFamilyMembers( $intNumberOfNonFamilyMembers ) {
		$this->m_intNumberOfNonFamilyMembers = $intNumberOfNonFamilyMembers;
	}

	public function setNumberOfDependents( $intNumberOfDependents ) {
		$this->m_intNumberOfDependents = $intNumberOfDependents;
	}

	public function setEligibilityUniverseCode( $intEligibilityUniverseCode ) {
		$this->m_intEligibilityUniverseCode = $intEligibilityUniverseCode;
	}

	public function setCurrentIncomeStatusCode( $intCurrentIncomeStatusCode ) {
		$this->m_intCurrentIncomeStatusCode = $intCurrentIncomeStatusCode;
	}

	public function setMinRentHardshipExemptionCode( $intMinRentHardshipExemptionCode ) {
		$this->m_intMinRentHardshipExemptionCode = $intMinRentHardshipExemptionCode;
	}

	public function setPoliceOrSecurityTenant( $intPoliceOrSecurityTenant ) {
		$this->m_intPoliceOrSecurityTenant = $intPoliceOrSecurityTenant;
	}

	public function setBedroomCount( $intBedroomCount ) {
		$this->m_intBedroomCount = $intBedroomCount;
	}

	public function setUnitNumber( $intUnitNumber ) {
		$this->m_intUnitNumber = $intUnitNumber;
	}

	public function setFamilyAdditionAdoption( $intFamilyAdditionAdoption ) {
		$this->m_intFamilyAdditionAdoption = $intFamilyAdditionAdoption;
	}

	public function setFamilyAdditionPregnancy( $intFamilyAdditionPregnancy ) {
		$this->m_intFamilyAdditionPregnancy = $intFamilyAdditionPregnancy;
	}

	public function setFamilyAdditionFosterChild( $intFamilyAdditionFosterChild ) {
		$this->m_intFamilyAdditionFosterChild = $intFamilyAdditionFosterChild;
	}

	public function setExtenuatingCircumstancesCode( $intExtenuatingCircumstancesCode ) {
		$this->m_intExtenuatingCircumstancesCode = $intExtenuatingCircumstancesCode;
	}

	public function setTotalCashValueOfAssets( $fltTotalCashValueOfAssets ) {
		$this->m_fltTotalCashValueOfAssets = $fltTotalCashValueOfAssets;
	}

	public function setTotalIncomeFromAssets( $fltTotalIncomeFromAssets ) {
		$this->m_fltTotalIncomeFromAssets = $fltTotalIncomeFromAssets;
	}

	public function setReportedPassbookRatePercent( $fltReportedPassbookRatePercent ) {
		$this->m_fltReportedPassbookRatePercent = $fltReportedPassbookRatePercent;
	}

	public function setImputedIncomeFromAssets( $fltImputedIncomeFromAssets ) {
		$this->m_fltImputedIncomeFromAssets = $fltImputedIncomeFromAssets;
	}

	public function setTotalEmploymentIncome( $fltTotalEmploymentIncome ) {
		$this->m_fltTotalEmploymentIncome = $fltTotalEmploymentIncome;
	}

	public function setTotalPensionIncome( $fltTotalPensionIncome ) {
		$this->m_fltTotalPensionIncome = $fltTotalPensionIncome;
	}

	public function setTotalPublicAssistanceIncome( $fltTotalPublicAssistanceIncome ) {
		$this->m_fltTotalPublicAssistanceIncome = $fltTotalPublicAssistanceIncome;
	}

	public function setTotalOtherIncome( $fltTotalOtherIncome ) {
		$this->m_fltTotalOtherIncome = $fltTotalOtherIncome;
	}

	public function setNonAssetIncome( $fltNonAssetIncome ) {
		$this->m_fltNonAssetIncome = $fltNonAssetIncome;
	}

	public function setAssetIncome( $fltAssetIncome ) {
		$this->m_fltAssetIncome = $fltAssetIncome;
	}

	public function setAnnualIncomeAmount( $fltAnnualIncomeAmount ) {
		$this->m_fltAnnualIncomeAmount = $fltAnnualIncomeAmount;
	}

	public function setLowIncomeLimitAmount( $fltLowIncomeLimitAmount ) {
		$this->m_fltLowIncomeLimitAmount = $fltLowIncomeLimitAmount;
	}

	public function setVeryLowIncomeLimitAmount( $fltVeryLowIncomeLimitAmount ) {
		$this->m_fltVeryLowIncomeLimitAmount = $fltVeryLowIncomeLimitAmount;
	}

	public function setExtremelyLowIncomeLimitAmount( $fltExtremelyLowIncomeLimitAmount ) {
		$this->m_fltExtremelyLowIncomeLimitAmount = $fltExtremelyLowIncomeLimitAmount;
	}

	public function setDependentDeduction( $fltDependentDeduction ) {
		$this->m_fltDependentDeduction = $fltDependentDeduction;
	}

	public function setMarketRent( $fltMarketRent ) {
		$this->m_fltMarketRent = $fltMarketRent;
	}

	public function setThreePercentOfIncome( $fltThreePercentOfIncome ) {
		$this->m_fltThreePercentOfIncome = $fltThreePercentOfIncome;
	}

	public function setDisabilityEexpense( $fltDisabilityEexpense ) {
		$this->m_fltDisabilityEexpense = $fltDisabilityEexpense;
	}

	public function setDisabilityDeduction( $fltDisabilityDeduction ) {
		$this->m_fltDisabilityDeduction = $fltDisabilityDeduction;
	}

	public function setMedicalExpense( $fltMedicalExpense ) {
		$this->m_fltMedicalExpense = $fltMedicalExpense;
	}

	public function setMedicalDeduction( $fltMedicalDeduction ) {
		$this->m_fltMedicalDeduction = $fltMedicalDeduction;
	}

	public function setElderlyFamilyDeduction( $fltElderlyFamilyDeduction ) {
		$this->m_fltElderlyFamilyDeduction = $fltElderlyFamilyDeduction;
	}

	public function setTotalDeductions( $fltTotalDeductions ) {
		$this->m_fltTotalDeductions = $fltTotalDeductions;
	}

	public function setAdjustedIncomeAmount( $fltAdjustedIncomeAmount ) {
		$this->m_fltAdjustedIncomeAmount = $fltAdjustedIncomeAmount;
	}

	public function setContractRentAmount( $fltContractRentAmount ) {
		$this->m_fltContractRentAmount = $fltContractRentAmount;
	}

	public function setUtilityAllowance( $fltUtilityAllowance ) {
		$this->m_fltUtilityAllowance = $fltUtilityAllowance;
	}

	public function setGrossRent( $fltGrossRent ) {
		$this->m_fltGrossRent = $fltGrossRent;
	}

	public function setWelfareRent( $fltWelfareRent ) {
		$this->m_fltWelfareRent = $fltWelfareRent;
	}

	public function setRentOverride( $fltRentOverride ) {
		$this->m_fltRentOverride = $fltRentOverride;
	}

	public function setIsRentOverride( $strIsRentOverride ) {
		$this->m_strIsRentOverride = $strIsRentOverride;
	}

	public function setTotalTenantPayment( $fltTotalTenantPayment ) {
		$this->m_fltTotalTenantPayment = $fltTotalTenantPayment;
	}

	public function setTenantRent( $fltTenantRent ) {
		$this->m_fltTenantRent = $fltTenantRent;
	}

	public function setUtilityReimbursement( $fltUtilityReimbursement ) {
		$this->m_fltUtilityReimbursement = $fltUtilityReimbursement;
	}

	public function setAssistancePaymentAmount( $fltAssistancePaymentAmount ) {
		$this->m_fltAssistancePaymentAmount = $fltAssistancePaymentAmount;
	}

	public function setSection236BasicBmirRent( $fltSection236BasicBmirRent ) {
		$this->m_fltSection236BasicBmirRent = $fltSection236BasicBmirRent;
	}

	public function setSecurityDeposit( $fltSecurityDeposit ) {
		$this->m_fltSecurityDeposit = $fltSecurityDeposit;
	}

	public function setChildCareExpenseA( $fltChildCareExpenseA ) {
		$this->m_fltChildCareExpenseA = $fltChildCareExpenseA;
	}

	public function setChildCareExpenseB( $fltChildCareExpenseB ) {
		$this->m_fltChildCareExpenseB = $fltChildCareExpenseB;
	}

	public function setTtpAtRadConversion( $fltTtpAtRadConversion ) {
		$this->m_fltTtpAtRadConversion = $fltTtpAtRadConversion;
	}

	public function setTtpBeforeOverride( $fltTtpBeforeOverride ) {
		$this->m_fltTtpBeforeOverride = $fltTtpBeforeOverride;
	}

	/**
	 * get Functions
	 *
	 */

	public function getPreviousHeadNameFirst() {
		return $this->m_strPreviousHeadNameFirst;
	}

	public function getPreviousHeadNameLast() {
		return $this->m_strPreviousHeadNameLast;
	}

	public function getPreviousHeadMiddleInitial() {
		return $this->m_strPreviousHeadMiddleInitial;
	}

	public function getPreviousHeadBirthDate() {
		return $this->m_strPreviousHeadBirthDate;
	}

	public function getProjectMoveInDate() {
		return $this->m_strProjectMoveInDate;
	}

	public function getPreviousActiveMat10EffectiveDate() {
		return $this->m_strPreviousActiveMat10EffectiveDate;
	}

	public function getSubsidyCertificationEffectiveDate() {
		return $this->m_strSubsidyCertificationEffectiveDate;
	}

	public function getTransactionType() {
		return $this->m_strTransactionType;
	}

	public function getCorrectionTypeCode() {
		return $this->m_strCorrectionTypeCode;
	}

	public function getPreviousSubsidy() {
		return $this->m_strPreviousSubsi;
	}

	public function getEivIndicator() {
		return $this->m_strEivIndicator;
	}

	public function getPreviousHousingCode() {
		return $this->m_strPreviousHousingCode;
	}

	public function getSection8Assistance1984Indicator() {
		return $this->m_strSection8Assistance1984Indicator;
	}

	public function getIncomeExceptionCode() {
		return $this->m_strIncomeExceptionCode;
	}

	public function getNextRecertificationDate() {
		return $this->m_strNextRecertificationDate;
	}

	public function getTenantSignedDate() {
		return $this->m_strTenantSignedDate;
	}

	public function getOwnerSignedDate() {
		return $this->m_strOwnerSignedDate;
	}

	public function getHhCitizenshipEligibility() {
		return $this->m_strHhCitizenshipEligibility;
	}

	public function getAnticipatedVoucherDate() {
		return $this->m_strAnticipatedVoucherDate;
	}

	public function getSecondarySubsidyType() {
		return $this->m_strSecondarySubsidyType;
	}

	public function getSurvivorIndicator() {
		return $this->m_strSurvivorIndicator;
	}

	public function getWaiverTypeCode() {
		return $this->m_strWaiverTypeCode;
	}

	public function getBaselineCertificationIndicator() {
		return $this->m_strBaselineCertificationIndicator;
	}

	public function getPlanOfActionIndicator() {
		return $this->m_strPlanOfActionIndicator;
	}

	public function getHudOwnedIndicator() {
		return $this->m_strHudOwnedIndicator;
	}

	public function getUnitTransferCode() {
		return $this->m_strUnitTransferCode;
	}

	public function getPreviousUnitNumber() {
		return $this->m_strPreviousUnitNumber;
	}

	public function getMobilityDisability() {
		return $this->m_strMobilityDisability;
	}

	public function getHearingDisability() {
		return $this->m_strHearingDisability;
	}

	public function getVisualDisability() {
		return $this->m_strVisualDisability;
	}

	public function getEligibilityCheckNotRequired() {
		return $this->m_strEligibilityCheckNotRequired;
	}

	public function getOwnerGeneratedTenantIdNumber() {
		return $this->m_intOwnerGeneratedTenantIdNumber;
	}

	public function getPreviousHeadId() {
		return $this->m_intPreviousHeadId;
	}

	public function getSubsidyPassbookRateId() {
		return $this->m_intSubsidyPassbookRateId;
	}

	public function getActionProcessedCode() {
		return $this->m_intActionProcessedCode;
	}

	public function getDisplacementStatusCode() {
		return $this->m_intDisplacementStatusCode;
	}

	public function getNumberOfFamilyMembers() {
		return $this->m_intNumberOfFamilyMembers;
	}

	public function getNumberOfNonFamilyMembers() {
		return $this->m_intNumberOfNonFamilyMembers;
	}

	public function getNumberOfDependents() {
		return $this->m_intNumberOfDependents;
	}

	public function getEligibilityUniverseCode() {
		return $this->m_intEligibilityUniverseCode;
	}

	public function getCurrentIncomeStatusCode() {
		return $this->m_intCurrentIncomeStatusCode;
	}

	public function getMinRentHardshipExemptionCode() {
		return $this->m_intMinRentHardshipExemptionCode;
	}

	public function getPoliceOrSecurityTenant() {
		return $this->m_intPoliceOrSecurityTenant;
	}

	public function getBedroomCount() {
		return $this->m_intBedroomCount;
	}

	public function getUnitNumber() {
		return $this->m_intUnitNumber;
	}

	public function getFamilyAdditionAdoption() {
		return $this->m_intFamilyAdditionAdoption;
	}

	public function getFamilyAdditionPregnancy() {
		return $this->m_intFamilyAdditionPregnancy;
	}

	public function getFamilyAdditionFosterChild() {
		return $this->m_intFamilyAdditionFosterChild;
	}

	public function getExtenuatingCircumstancesCode() {
		return $this->m_intExtenuatingCircumstancesCode;
	}

	public function getTotalCashValueOfAssets() {
		return $this->m_fltTotalCashValueOfAssets;
	}

	public function getTotalIncomeFromAssets() {
		return $this->m_fltTotalIncomeFromAssets;
	}

	public function getReportedPassbookRatePercent() {
		return $this->m_fltReportedPassbookRatePercent;
	}

	public function getImputedIncomeFromAssets() {
		return $this->m_fltImputedIncomeFromAssets;
	}

	public function getTotalEmploymentIncome() {
		return $this->m_fltTotalEmploymentIncome;
	}

	public function getTotalPensionIncome() {
		return $this->m_fltTotalPensionIncome;
	}

	public function getTotalPublicAssistanceIncome() {
		return $this->m_fltTotalPublicAssistanceIncome;
	}

	public function getTotalOtherIncome() {
		return $this->m_fltTotalOtherIncome;
	}

	public function getNonAssetIncome() {
		return $this->m_fltNonAssetIncome;
	}

	public function getAssetIncome() {
		return $this->m_fltAssetIncome;
	}

	public function getAnnualIncomeAmount() {
		return $this->m_fltAnnualIncomeAmount;
	}

	public function getLowIncomeLimitAmount() {
		return $this->m_fltLowIncomeLimitAmount;
	}

	public function getVeryLowIncomeLimitAmount() {
		return $this->m_fltVeryLowIncomeLimitAmount;
	}

	public function getExtremelyLowIncomeLimitAmount() {
		return $this->m_fltExtremelyLowIncomeLimitAmount;
	}

	public function getDependentDeduction() {
		return $this->m_fltDependentDeduction;
	}

	public function getMarketRent() {
		return $this->m_fltMarketRent;
	}

	public function getThreePercentOfIncome() {
		return $this->m_fltThreePercentOfIncome;
	}

	public function getDisabilityEexpense() {
		return $this->m_fltDisabilityEexpense;
	}

	public function getDisabilityDeduction() {
		return $this->m_fltDisabilityDeduction;
	}

	public function getMedicalExpense() {
		return $this->m_fltMedicalExpense;
	}

	public function getMedicalDeduction() {
		return $this->m_fltMedicalDeduction;
	}

	public function getElderlyFamilyDeduction() {
		return $this->m_fltElderlyFamilyDeduction;
	}

	public function getTotalDeductions() {
		return $this->m_fltTotalDeductions;
	}

	public function getAdjustedIncomeAmount() {
		return $this->m_fltAdjustedIncomeAmount;
	}

	public function getContractRentAmount() {
		return $this->m_fltContractRentAmount;
	}

	public function getUtilityAllowance() {
		return $this->m_fltUtilityAllowance;
	}

	public function getGrossRent() {
		return $this->m_fltGrossRent;
	}

	public function getWelfareRent() {
		return $this->m_fltWelfareRent;
	}

	public function getRentOverride() {
		return $this->m_fltRentOverride;
	}

	public function getIsRentOverride() {
		return $this->m_strIsRentOverride;
	}

	public function getTotalTenantPayment() {
		return $this->m_fltTotalTenantPayment;
	}

	public function getTenantRent() {
		return $this->m_fltTenantRent;
	}

	public function getUtilityReimbursement() {
		return $this->m_fltUtilityReimbursement;
	}

	public function getAssistancePaymentAmount() {
		return $this->m_fltAssistancePaymentAmount;
	}

	public function getSection236BasicBmirRent() {
		return $this->m_fltSection236BasicBmirRent;
	}

	public function getSecurityDeposit() {
		return $this->m_fltSecurityDeposit;
	}

	public function getChildCareExpenseA() {
		return $this->m_fltChildCareExpenseA;
	}

	public function getChildCareExpenseB() {
		return $this->m_fltChildCareExpenseB;
	}

	public function getTtpAtRadConversion() {
		return $this->m_fltTtpAtRadConversion;
	}

	public function getTtpBeforeOverride() {
		return $this->m_fltTtpBeforeOverride;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviousSubsidyContractTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getPreviousSubsidyContractTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_subsidy_contract_type_id', __( 'Previous subsidy contract type is required.' ) ) ] );
		}

		return $boolIsValid;
	}

	public function valMoveInIncomeLevelTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrentIncomeLevelTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyWaiverTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getSubsidyWaiverTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_waiver_type_id', __( 'Subsidy waiver type is required.' ) ) ] );
		}

		return $boolIsValid;
	}

	public function valSubsidyCitizenshipEligibilityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyMinRentExemptionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyIncomeExceptionTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getSubsidyIncomeExceptionTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_income_exception_type_id', __( 'Subsidy income exception type is required.' ) ) ] );
		}

		return $boolIsValid;
	}

	public function valSubsidyDisplacementStatusTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getSubsidyDisplacementStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_displacement_status_type_id', __( 'Displacement status type is required.' ) ) ] );
		}

		return $boolIsValid;
	}

	public function valSubsidyPreviousHousingTypeId( $strPreviousHousingDate ) {
		$boolIsValid = true;

		if( false == valStr( $this->getSubsidyPreviousHousingTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_previous_housing_type_id', __( 'Previous housing arrangement type is required.' ) ) ] );
		} else {
			$objActiveSubsidyCertification = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchActiveSubsidyCertificationByApplicantionIdByCid( $this->getApplicationId(), $this->getCid(), $this->m_objDatabase );

			if( true == valObj( $objActiveSubsidyCertification, 'CSubsidyCertification' ) && \CSubsidyTracsVersion::TRACS_2_0_3A == $objActiveSubsidyCertification->getSubsidyTracsVersionId() && true == is_null( $objActiveSubsidyCertification->getCorrectedSubsidyCertificationId() ) && strtotime( $strPreviousHousingDate ) >= strtotime( $objActiveSubsidyCertification->getEffectiveDate() ) ) {
				if( CSubsidyPreviousHousingType::SUBSIDY_PREVIOUS_HOUSING_TYPE_WITHOUT_HOUSING == $this->getSubsidyPreviousHousingTypeId() ) {
					$boolIsValid = false;
					$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_previous_housing_type_id', __( 'select either Code 5 (Lacking a Fixed Nighttime Residence) or Code 6 (Fleeing/Attempting to Flee Violence) in order to proceed with the certification.' ) ) ] );
				}
			}
		}
		return $boolIsValid;
	}

	public function valAnticipatedAdditionAdoption( $intAnticipatedAdditionAdoption ) {
		$boolIsValid = true;

		if( true == valStr( $intAnticipatedAdditionAdoption ) ) {
			if( false == is_numeric( $intAnticipatedAdditionAdoption ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anticipated_addition_adoption', __( ' Adoption should be integer value.' ) ) );
			} elseif( 2 < strlen( trim( $intAnticipatedAdditionAdoption ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anticipated_addition_adoption', __( ' Adoption should be less than or equal to 2 digits.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valAnticipatedAdditionPregnancy( $intAnticipatedAdditionPregnancy ) {
		$boolIsValid = true;

		if( true == valStr( $intAnticipatedAdditionPregnancy ) ) {
			if( false == is_numeric( $intAnticipatedAdditionPregnancy ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anticipated_addition_pregnancy', __( ' Pregnancy should be integer value.' ) ) );
			} elseif( 2 < strlen( trim( $intAnticipatedAdditionPregnancy ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anticipated_addition_pregnancy', __( ' Pregnancy should be less than or equal to 2 digits.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valAnticipatedAdditionFosterChild( $intAnticipatedAdditionFosterChild ) {
		$boolIsValid = true;

		if( true == valStr( $intAnticipatedAdditionFosterChild ) ) {
			if( false == is_numeric( $intAnticipatedAdditionFosterChild ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anticipated_addition_foster_child', __( ' Foster Children should be integer value.' ) ) );
			} elseif( 2 < strlen( trim( $intAnticipatedAdditionFosterChild ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'anticipated_addition_foster_child', __( ' Foster Children should be less than or equal to 2 digits.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $arrintApplicationSubsidyDetails = [], $arrmixApplicationSubsidyDetails = [], $strPreviousHousingDate = '' ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'update_subsidy_details':
				$boolIsValid &= $this->valPreviousSubsidyContractTypeId();
				$boolIsValid &= $this->valSubsidyPreviousHousingTypeId( $strPreviousHousingDate );
				$boolIsValid &= $this->valSubsidyDisplacementStatusTypeId();
				break;

			case 'anticipated_family_additions':
				$boolIsValid &= $this->valAnticipatedAdditionPregnancy( $arrintApplicationSubsidyDetails['anticipated_addition_pregnancy'] );
				$boolIsValid &= $this->valAnticipatedAdditionAdoption( $arrintApplicationSubsidyDetails['anticipated_addition_adoption'] );
				$boolIsValid &= $this->valAnticipatedAdditionFosterChild( $arrintApplicationSubsidyDetails['anticipated_addition_foster_child'] );
				break;

			case 'process_certification':
				$boolIsValid &= ( true == $arrmixApplicationSubsidyDetails['is_household_waiver'] ) ? $this->valSubsidyWaiverTypeId():true;
				$boolIsValid &= ( true == $arrmixApplicationSubsidyDetails['is_husehold_income_exception'] )? $this->valSubsidyIncomeExceptionTypeId():true;
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
