<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRoommateGroups
 * Do not add any new functions to this class.
 */

class CRoommateGroups extends CBaseRoommateGroups {

	public static function fetchRoommateGroupLeaseIdsByOverBookedLeaseIdsByCid( $strOverBookedLeaseIds, $intCid, $objDatabase ) {
		if( false == valStr( $strOverBookedLeaseIds ) ) return false;

		$strSql = 'SELECT
						lease_id
					FROM
						roommates
					WHERE
						roommate_group_id IN (
												SELECT
													r.roommate_group_id
												FROM
													roommates r
												WHERE
													r.cid = ' . ( int ) $intCid . '
													AND r.lease_id IN (' . $strOverBookedLeaseIds . ')
													AND r.confirmed_by IS NOT NULL
													AND r.deleted_by IS NULL
											)
					AND cid = ' . ( int ) $intCid . '
					AND confirmed_by IS NOT NULL
					AND deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}
}
?>