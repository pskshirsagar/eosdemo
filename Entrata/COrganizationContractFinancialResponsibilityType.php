<?php

class COrganizationContractFinancialResponsibilityType extends CBaseOrganizationContractFinancialResponsibilityType {

	const GROUP_IS_HUNDRED_PERCENT_RESPONSIBLE		= 1;
	const RESIDENT_IS_HUNDRED_PERCENT_RESPONSIBLE	= 2;
	const SHARED									= 3;

	private $m_arrstrOrganizationContractFinancialResponsibilityTypes;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getOrganizationContractFinancialResponsibilityTypes() {

		if( true == valArr( $this->m_arrstrOrganizationContractFinancialResponsibilityTypes ) )
			return $this->m_arrstrOrganizationContractFinancialResponsibilityTypes;

		$this->m_arrstrOrganizationContractFinancialResponsibilityTypes = [
			self::GROUP_IS_HUNDRED_PERCENT_RESPONSIBLE		=> __( 'Group Is 100% Responsible' ),
			self::SHARED									=> __( 'Shared' ),
			self::RESIDENT_IS_HUNDRED_PERCENT_RESPONSIBLE	=> __( 'Resident Is 100% Responsible' )
		];

		return $this->m_arrstrOrganizationContractFinancialResponsibilityTypes;
	}

}
?>