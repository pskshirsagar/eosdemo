<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlLedgerTypes
 * Do not add any new functions to this class.
 */

class CGlLedgerTypes extends CBaseGlLedgerTypes {

	public static function fetchGlLedgerTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlLedgerType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchGlLedgerType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlLedgerType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllGlLedgerTypes( $objDatabase, $boolIsPublished = false ) {

		$strSql = 'SELECT * FROM gl_ledger_types';

		if( false != $boolIsPublished ) {
			$strSql = 'SELECT * FROM gl_ledger_types WHERE is_published = 1 ORDER BY id';
		}

		return self::fetchGlLedgerTypes( $strSql, $objDatabase );
	}

}
?>