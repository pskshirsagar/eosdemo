<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateEducationTypes
 * Do not add any new functions to this class.
 */

class CCandidateEducationTypes extends CBaseCandidateEducationTypes {

	public static function fetchCandidateEducationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCandidateEducationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCandidateEducationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCandidateEducationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>