<?php

class CCompanyArea extends CBaseCompanyArea {

	protected $m_intParentId;
	protected $m_intGrandParentId;

	protected $m_strParentName;
	protected $m_strGrandParentName;
	protected $m_strCompanyMediaFileFullsizeUri;

	// Following variables are used in local search module to display google map with search
	protected $m_strSearchTerm;
	protected $m_boolIsDisplayDirectory;
	protected $m_intSearchPageNumber;

	protected $m_intPropertyAreaId;

	protected $m_arrobjCompanyAreas;
	protected $m_arrobjProperties;
	protected $m_arrobjCompanyMediaFiles;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjCompanyAreas = [];
	}

	/**
	 * Create Functions
	 */

	public function createCompanyAreaSearch() {

		$objCompanyAreaSearch = new CCompanyAreaSearch();
		$objCompanyAreaSearch->setCid( $this->getCid() );
		$objCompanyAreaSearch->setCompanyAreaId( $this->getId() );

		return $objCompanyAreaSearch;
	}

	public function createCompanyAreaMedia() {

		$objCompanyAreaMedia = new CCompanyAreaMedia();
		$objCompanyAreaMedia->setCid( $this->getCid() );
		$objCompanyAreaMedia->setCompanyAreaId( $this->getId() );

		return $objCompanyAreaMedia;
	}

	/**
	 * Add Functions
	 */

	public function addCompanyArea( $objCompanyArea ) {
		$this->m_arrobjCompanyAreas[$objCompanyArea->getId()] = $objCompanyArea;
	}

	/**
	 * Get Functions
	 */

	public function getCompanyAreas() {
		return $this->m_arrobjCompanyAreas;
	}

	public function getParentId() {
		return $this->m_intParentId;
	}

	public function getParentName() {
		return $this->m_strParentName;
	}

	public function getGrandParentId() {
		return $this->m_intGrandParentId;
	}

	public function getGrandParentName() {
		return $this->m_strGrandParentName;
	}

	public function getPropertyAreaId() {
		return $this->m_intPropertyAreaId;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getCompanyMediaFiles() {
		return $this->m_arrobjCompanyMediaFiles;
	}

	public function getSearchTerm() {
		return $this->m_strSearchTerm;
	}

	public function getIsDisplayDirectory() {
		return $this->m_boolIsDisplayDirectory;
	}

	public function getSearchPageNumber() {
		return $this->m_intSearchPageNumber;
	}

	public function getSeoName() {
		return \Psi\CStringService::singleton()->strtolower( preg_replace( [ '/[^0-9a-zA-Z\s\-]+/i', '/\s+/' ], [ '', '-' ], $this->getName() ) );
	}

	public function getCompanyMediaFileFullsizeUri() {
		return $this->m_strCompanyMediaFileFullsizeUri;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {
		$this->setSearchPageNumber( 1 );
		$this->setIsDisplayDirectory( false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['parent_id'] ) ) $this->setParentId( $arrmixValues['parent_id'] );
		if( true == isset( $arrmixValues['parent_name'] ) ) $this->setParentName( $arrmixValues['parent_name'] );
		if( true == isset( $arrmixValues['grand_parent_id'] ) ) $this->setGrandParentId( $arrmixValues['grand_parent_id'] );
		if( true == isset( $arrmixValues['grand_parent_name'] ) ) $this->setGrandParentName( $arrmixValues['grand_parent_name'] );
		if( true == isset( $arrmixValues['property_area_id'] ) ) $this->setPropertyAreaId( $arrmixValues['property_area_id'] );
		if( true == isset( $arrmixValues['search_term'] ) ) $this->setSearchTerm( $arrmixValues['search_term'] );
		if( true == isset( $arrmixValues['search_page_number'] ) ) $this->setSearchPageNumber( $arrmixValues['search_page_number'] );
		if( true == isset( $arrmixValues['is_display_directory'] ) ) $this->setIsDisplayDirectory( $arrmixValues['is_display_directory'] );
	}

	public function setCompanyAreas( $arrobjCompanyAreas ) {
		$this->m_arrobjCompanyAreas = $arrobjCompanyAreas;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setCompanyMediaFiles( $arrobjCompanyMediaFiles ) {
		$this->m_arrobjCompanyMediaFiles = $arrobjCompanyMediaFiles;
	}

	public function setParentId( $intParentId ) {
		$this->m_intParentId = $intParentId;
	}

	public function setParentName( $strParentName ) {
		$this->m_strParentName = $strParentName;
	}

	public function setGrandParentId( $intGrandParentId ) {
		$this->m_intGrandParentId = $intGrandParentId;
	}

	public function setGrandParentName( $strGrandParentName ) {
		$this->m_strGrandParentName = $strGrandParentName;
	}

	public function setPropertyAreaId( $intPropertyAreaId ) {
		$this->m_intPropertyAreaId = $intPropertyAreaId;
	}

	public function setSearchTerm( $strSearchTerm ) {
		$this->m_strSearchTerm = $strSearchTerm;
	}

	public function setIsDisplayDirectory( $boolIsDisplayDirectory ) {
		$this->m_boolIsDisplayDirectory = $boolIsDisplayDirectory;
	}

	public function setSearchPageNumber( $intSearchPageNumber ) {
		$this->m_intSearchPageNumber = $intSearchPageNumber;
	}

	public function setCompanyMediaFileFullsizeUri( $strCompanyMediaFileFullsizeUri ) {
		$this->m_strCompanyMediaFileFullsizeUri = $strCompanyMediaFileFullsizeUri;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCompanyAreaSearches( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyAreaSearches::createService()->fetchCustomCompanyAreaSearchesByCompanyAreaIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyMediaFiles( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByCompanyAreaIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyAreaMedias( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyAreaMedias::createService()->fetchCompanyAreaMediasByCompanyAreaIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchStateMetaSetting( $objDatabase ) {
		return \Psi\Eos\Entrata\CStateMetaSettings::createService()->fetchStateMetaSettingByCidByStateCodeByCompanyAreaId( $this->getCid(), $this->getStateCode(), $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Company area id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
		   $boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Company area client does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getStateCode() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
			return false;
		}

		if( false !== \Psi\CStringService::singleton()->strrpos( $this->getName(), '_' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', __( "Name should not contain '_' character." ) ) );
			return false;
		}

		$this->m_strName = preg_replace( '/(\s\s+)/', ' ', $this->m_strName );

		$intConflictingCompanyAreaCount = \Psi\Eos\Entrata\CCompanyAreas::createService()->fetchConflictingCompanyAreaCount( $this, $objDatabase );

		if( 0 < $intConflictingCompanyAreaCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is already in use.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valLongitude() {
		$boolIsValid = true;

		if( 0 < strlen( $this->getLongitude() ) && false == is_numeric( $this->getLongitude() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'longitude', __( 'Longitude should be in numbers.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLatitude() {
		$boolIsValid = true;

		if( 0 < strlen( $this->getLatitude() ) && false == is_numeric( $this->getLatitude() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'latitude', __( 'Latitude should be in numbers.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {
		$boolIsValid	= true;
		$intCount		= \Psi\Eos\Entrata\CPropertyAreas::createService()->fetchDependentPropertyAreasCountByCompanyAreaByCid( $this, $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL,  $this->m_strName . __( "Area cannot be removed because a property's address depends on it." ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valLongitude();
				$boolIsValid &= $this->valLatitude();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>