<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CExternalMergeFields
 * Do not add any new functions to this class.
 */

class CExternalMergeFields extends CBaseExternalMergeFields {

    public static function bulkUpdate( $arrobjTargetObjects, $arrmixFieldsToUpdate, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly = false ) {
        // Not sure if returning false is good idea? as validating is not failure as such in this case! as while calling we need check if its array if we are validating return from here it may mislead.
        if( false == valArr( $arrobjTargetObjects ) ) {
            return false;
        }

        $strSql = '';
        // Determine table name
        $strTableName = \Psi\CStringService::singleton()->substr( \Psi\Libraries\UtilInflector\CInflector::createService()->underscore( get_called_class() ), 2 );

        // Get available fields in table using first record
        $arrobjTargetObject = reset( $arrobjTargetObjects );

        // Check if Singluar Base class has toArray() function. Newly generated classes will have it.
        if( false == method_exists( get_class( $arrobjTargetObject ), 'toArray' ) ) {
            trigger_error( get_class( $arrobjTargetObject ) . '->toArray()' . ' is required to use this bulkUpdate() function. Regenarate base class files with updated PgDictionary tool.', E_USER_WARNING );
        }

        $arrstrAvailableFields = array_keys( $arrobjTargetObject->toArray() );

        // @TODO: Check if all fields in $arrmixFieldsToUpdate exists in table

        // Check if updated_by is present in table and not passed in input parameter $arrmixFieldsToUpdate
        if( true == in_array( 'updated_by', $arrstrAvailableFields ) && false == in_array( 'updated_by', $arrmixFieldsToUpdate ) ) {
            array_push( $arrmixFieldsToUpdate, 'updated_by' );
        }

        // Check if updated_on is present in table and not passed in input parameter $arrmixFieldsToUpdate
        if( true == in_array( 'updated_on', $arrstrAvailableFields ) && false == in_array( 'updated_on', $arrmixFieldsToUpdate ) ) {
            array_push( $arrmixFieldsToUpdate, 'updated_on' );
        }

        // We should not update the created_by and/or created_on fields
        if( true == in_array( 'created_by', $arrstrAvailableFields ) || true == in_array( 'created_on', $arrstrAvailableFields ) ) {
            $arrstrAvailableFields = array_diff( $arrstrAvailableFields, [ 'created_by', 'created_on' ] );
        }

        // Sanitize required fields
        $arrmixFieldsToUpdate = array_intersect( $arrmixFieldsToUpdate, $arrstrAvailableFields );

        // Prepare UPDATE statement for each object
        foreach( $arrobjTargetObjects as $arrobjTargetObject ) {
            $arrstrFieldValues = $arrobjTargetObject->toArray();

            // Prepare the UPDATE sql
            $strSql .= 'UPDATE ' . $strTableName . ' SET ';

            // Loop on each field
            foreach( $arrmixFieldsToUpdate as $strFieldToUpdate ) {
                // Force updated_by as $intCurrentUserId
                if( 'updated_by' == $strFieldToUpdate ) {
                    $strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
                    continue;
                }

                // Force updated_on as NOW()
                if( 'updated_on' == $strFieldToUpdate ) {
                    $strSql .= ' updated_on = NOW(), ';
                    continue;
                }

                $strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strFieldToUpdate ) ) );
                $strSql .= ' ' . $strFieldToUpdate . ' = ' . ( true == is_null( $arrstrFieldValues[$strFieldToUpdate] ) ? 'NULL' : $arrobjTargetObject->$strSqlFunctionName() ) . ', ';
            }

            // Trim the ending comma
            $strSql = trim( $strSql, ', ' );
            $strWhereApartmentAssociation = ( false == is_null( $arrstrFieldValues['apartment_association'] ) ) ? ( ' AND apartment_association = \'' . $arrstrFieldValues['apartment_association'] . '\'' ) : '';

            // $strSql .= ' WHERE id = ' . $arrstrFieldValues['id'];
            $strSql .= ' WHERE transmission_vendor_id = ' . $arrstrFieldValues['transmission_vendor_id'];
            $strSql .= ' AND state_code = ' . '\'' . $arrstrFieldValues['state_code'] . '\'';
            $strSql .= ' AND external_merge_field_name = ' . '\'' . $arrstrFieldValues['external_merge_field_name'] . '\'';
            $strSql .= $strWhereApartmentAssociation;

            if( true == isset( $arrstrFieldValues['cid'] ) ) {
                $strSql .= ' AND cid = ' . $arrstrFieldValues['cid'];
            }

            $strSql .= ';' . PHP_EOL;
        }

        // Return SQL or execute it
        if( true == $boolIsReturnSqlOnly ) {
            return $strSql;
        }

        $objDataset = $objDatabase->createDataset();
        if( false == $objDataset->execute( $strSql ) ) {
            $objDataset->cleanup();
            return false;
        } else {
            $objDataset->cleanup();
            return true;
        }
    }

    public static function fetchAllPaginatedExternalMergeFieldsWithGroupName( $objDatabase, $objPagination, $boolIsCount = false ) {

        if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
            $strOrderByType = ( 0 == $objPagination->getOrderByType() ) ? ' ASC ' : ' DESC ';
            $intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
            $intLimit = ( int ) $objPagination->getPageSize();
        }

        if( true == $boolIsCount ) {
            $strSql = 'SELECT
						count( emf.id ) ';
        } else {
            $strSql = 'SELECT
						emf.id,
					    emf.external_merge_field_name,
					    emf.state_code,
					    emf.merge_field_group_id,
					    emf.transmission_vendor_id,
					    emf.block_default_merge_field_id,
					    emf.default_merge_field_ids,
					    emf.deleted_on,
					    emf.external_form_keys,
					    mfg.name AS group_name,
					    array_to_json( string_to_array ( string_agg ( dmf_s.field, \',\' ), \',\' ) ) AS default_merge_field_names,
					    dmf_b.field AS default_block_merge_field_name';
        }

        $strSql     .= ' FROM
							    external_merge_fields emf
							    LEFT JOIN merge_field_groups mfg ON ( mfg.id = emf.merge_field_group_id ) 
							    LEFT JOIN default_merge_fields dmf_s ON ( dmf_s.id = ANY( emf.default_merge_field_ids ) )
							    LEFT JOIN default_merge_fields dmf_b ON ( dmf_b.id = emf.block_default_merge_field_id )
							WHERE
								emf.transmission_vendor_id in ( ' . CTransmissionVendor::BLUE_MOON_LEASE . ')';

        if( false == $boolIsCount && true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
            $strSql .= 'GROUP BY
							emf.id,
						    emf.external_merge_field_name,
						    emf.state_code,
						    emf.merge_field_group_id,
						    emf.block_default_merge_field_id,
						    emf.default_merge_field_ids,
						    emf.deleted_on,
						    emf.external_form_keys,
						    mfg.name,
						    mfg.id,
						    dmf_b.field 
						ORDER BY 
							' . $objPagination->getOrderByField() . ' ' . $strOrderByType .
                ' OFFSET ' . ( int ) $intOffset .
                ' LIMIT ' . ( int ) $intLimit;
        }

        $arrstrResultSet = fetchData( $strSql, $objDatabase );

        return ( false == $boolIsCount ) ? $arrstrResultSet : $arrstrResultSet[0]['count'];
    }

    public static function fetchExternalMergeFieldsBySearchFilter( $arrstrFilteredExplodedSearch, $objDatabase ) {

        $strSql = 'SELECT
							emf.id,
							emf.external_merge_field_name,
							emf.state_code,
							emf.transmission_vendor_id,
							emf.apartment_association,
							mfg.id AS group_id,
							mfg.name AS group_name 
						FROM
							external_merge_fields emf 
							LEFT JOIN merge_field_groups mfg ON ( mfg.id = emf.merge_field_group_id )
						WHERE 
							emf.external_merge_field_name ILIKE \'%' . implode( '%\' OR emf.external_merge_field_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							OR mfg.name ILIKE \'%' . implode( '%\' OR mfg.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						ORDER BY 
							CASE
								When emf.external_merge_field_name = \'' . implode( ' ', $arrstrFilteredExplodedSearch ) . '\' THEN 1
								ELSE 2
							END	
						LIMIT 10';

        return fetchData( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldsByTransmissionVendorIdByExternalMergeFieldNames( $intTransmissionVendorId, $arrstrExternalMergeFieldNames, $objDatabase ) {
        $strSql = 'SELECT
						*
					FROM
						external_merge_fields emf 
					WHERE
						transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
						AND external_merge_field_name IN ( \'' . implode( '\',\'', $arrstrExternalMergeFieldNames ) . '\' )';

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldsByStateCodeByTransmissionVendorId( $strStateCode, $intTransmissionVendorId, $objDatabase ) {

        if( false == valStr( $strStateCode ) || false == valId( $intTransmissionVendorId ) ) {
            return NULL;
        }

        $strSql = 'SELECT
						*
					FROM
						external_merge_fields
					WHERE
						state_code = \'' . $strStateCode . '\'
						AND transmission_vendor_id = ' . ( int ) $intTransmissionVendorId;

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldsByStateCodeByExternalKeyByTransmissionVendorId( $strStateCode, $strExternalKey, $intTransmissionVendorId, $objDatabase ) {

        if( false == valStr( $strStateCode ) || false == valId( $intTransmissionVendorId ) || false == valStr( $strExternalKey ) ) {
            return NULL;
        }

        $strSql = 'SELECT
						*
					FROM
						external_merge_fields
					WHERE
						state_code = \'' . $strStateCode . '\'
						AND \'' . $strExternalKey . '\' = ANY( external_form_keys )
						AND transmission_vendor_id = ' . ( int ) $intTransmissionVendorId;

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldsByStateCodeByApartmentAssociation( $intTransmissionVendorId, $strStateCode, $strAssociation, $objDatabase ) {

        if( false == valStr( $strStateCode ) || false == valStr( $strAssociation ) || false == valId( $intTransmissionVendorId ) ) {
            return NULL;
        }

        $strSql = 'SELECT
						*
					FROM
						external_merge_fields
					WHERE
						state_code = \'' . $strStateCode . '\'
						AND apartment_association = \'' . $strAssociation . '\'
						AND transmission_vendor_id = \'' . $intTransmissionVendorId . '\'';

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

    public static function fetchTenantTechExternalMergeFieldsByDefaultMergeFieldsIds( $objDatabase ) {

        $strSql = 'SELECT 
                      dmf.field,
                      emf.external_merge_field_name 
                   FROM 
                      default_merge_fields dmf 
                      JOIN external_merge_fields emf ON dmf.id = ANY ( emf.default_merge_field_ids ) 
                   WHERE 
                      emf.default_merge_field_ids IS NOT NULL 
                      AND emf.transmission_vendor_id=' . CTransmissionVendor::TENANT_TECH;

        return fetchData( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldsMappingByTransmissionVendorIdByStateCodeByAssociationByExternalKeys( $intTransmissionVendorId, $strStateCode, $strAssociation, $arrstrExternalFormKeys, $objDatabase ) {
        if( false == valId( $intTransmissionVendorId ) || false == valStr( $strStateCode ) || false == valStr( $strAssociation ) ) {
            return NULL;
        }

        $strCondition = ( true == valArr( $arrstrExternalFormKeys ) ) ? 'AND external_form_key IN ( \'' . implode( '\',\'', $arrstrExternalFormKeys ) . '\' )' : '';

        $strSql  = 'SELECT
						*
					FROM
						(
							SELECT
								dmf.id,
								COALESCE ( dmf.field, emf.external_merge_field_name ) AS field_name,
								emf.external_merge_field_name as external_merge_field_name,
								COALESCE ( emf.merge_field_group_id, 94 ) AS merge_field_group_id,
								emf.max_length,
								unnest( emf.external_form_keys ) AS external_form_key,
								emf.data_type,
								ROW_NUMBER() OVER( PARTITION BY unnest( external_form_keys ), COALESCE ( dmf.field, emf.external_merge_field_name ), emf.block_default_merge_field_id ORDER BY emf.external_merge_field_name ) AS row_num
							FROM
								external_merge_fields AS emf
								LEFT JOIN default_merge_fields AS dmf ON ( dmf.id = ANY ( emf.default_merge_field_ids ) )
							WHERE
								emf.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
								AND state_code = \'' . $strStateCode . '\'
								AND emf.apartment_association = \'' . $strAssociation . '\'

							UNION
      
							SELECT
								dmf.id,
								dmf.field AS field_name,
								emf.external_merge_field_name as external_merge_field_name,
								COALESCE ( emf.merge_field_group_id, 94 ) AS merge_field_group_id,
								emf.max_length,
								unnest( emf.external_form_keys ) AS external_form_key,
								emf.data_type,
								ROW_NUMBER() OVER( PARTITION BY unnest( external_form_keys ), dmf.field ORDER BY emf.external_merge_field_name ) AS row_num
							FROM
								external_merge_fields AS emf
								JOIN default_merge_fields AS dmf ON ( dmf.id = emf.block_default_merge_field_id )
							WHERE
								emf.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
								AND state_code = \'' . $strStateCode . '\'
								AND emf.apartment_association = \'' . $strAssociation . '\'
								
						) AS subquery
					WHERE
						row_num = 1 ' . $strCondition . '
					ORDER BY id';

        return self::fetchExternalMergeFields( $strSql, $objDatabase );

    }

    public static function fetchExternalMergeFieldsWithCustomMergeFieldMappingByTransmissionVendorIdByCustomMergeFields( $intTransmissionVendorId, $arrstrCustomMergeFields, $objDatabase ) {
        if( false == valId( $intTransmissionVendorId ) || false == valArr( $arrstrCustomMergeFields ) ) {
            return NULL;
        }

        $strSql  = 'SELECT
						*
					FROM
						(
							SELECT
								dmf.id,
								COALESCE ( dmf.field, emf.external_merge_field_name ) AS field_name,
								emf.external_merge_field_name as external_merge_field_name,
								emf.merge_field_group_id,
								emf.max_length,
								emf.data_type,
								ROW_NUMBER() OVER( PARTITION BY  COALESCE ( dmf.field, emf.external_merge_field_name ), emf.block_default_merge_field_id ORDER BY emf.external_merge_field_name ) AS row_num
							FROM
								external_merge_fields AS emf
								JOIN 
								(
									SELECT
										unnest ( ARRAY [ \'' . implode( '\',\'', $arrstrCustomMergeFields ) . '\' ] ) AS custom_merge_field
								) AS cmf ON ( emf.external_merge_field_name = cmf.custom_merge_field )
								LEFT JOIN default_merge_fields AS dmf ON ( dmf.id = ANY ( emf.default_merge_field_ids ) )
							WHERE
								emf.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '

							UNION
      
							SELECT
								dmf.id,
								dmf.field AS field_name,
								emf.external_merge_field_name as external_merge_field_name,
								emf.merge_field_group_id,
								emf.max_length,
								emf.data_type,
								ROW_NUMBER() OVER( PARTITION BY dmf.field ORDER BY emf.external_merge_field_name ) AS row_num
							FROM
								external_merge_fields AS emf
								JOIN
								(
									SELECT
										unnest ( ARRAY [ \'' . implode( '\',\'', $arrstrCustomMergeFields ) . '\' ] ) AS custom_merge_field
								) AS cmf ON ( emf.external_merge_field_name = cmf.custom_merge_field )
								JOIN default_merge_fields AS dmf ON ( dmf.id = emf.block_default_merge_field_id ) 
							WHERE
								emf.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
                            UNION
                
                            SELECT 
                              null as id,
                              cmf.custom_merge_field as field_name,
                              cmf.custom_merge_field AS external_merge_field_name,
                              ' . CMergeFieldGroup::OTHERS . ' as merge_field_group_id,
                              null as max_length,
                              null as data_type,
                              1 AS row_num
                            FROM 
                            (
                                SELECT
                                    unnest ( ARRAY [ \'' . implode( '\',\'', $arrstrCustomMergeFields ) . '\' ] ) AS custom_merge_field
                              ) AS cmf
                              LEFT JOIN  external_merge_fields AS emf ON ( emf.external_merge_field_name = cmf.custom_merge_field )
                            WHERE
                                emf.id IS NULL
						) AS subquery
					WHERE
						row_num = 1
					ORDER BY id';

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldsMappingDetailsByTransmissionVendorIdByStateCodeByApartmentAssociationByExternalFormKeys( $intTransmissionVendorId, $strStateCode, $strAssociation, $arrstrExternalFormKeys, $objDatabase ) {

        if( false == valStr( $intTransmissionVendorId ) || false == valStr( $strStateCode ) || false == valArr( $arrstrExternalFormKeys ) ) {
            return NULL;
        }

        $strWhereAssociation = ' true ';
        if( true == valStr( $strAssociation ) ) {
            $strWhereAssociation = ' apartment_association = \'' . $strAssociation . '\' ';
        }

			$strSql = ' SELECT
							emf.external_merge_field_name,
							emf.max_length,
							emf.data_type,
                            array_to_json( array_agg ( COALESCE ( dmf.field, emf.external_merge_field_name ) ORDER BY a.nr ) ) AS default_merge_field_names,
                            dmfb.field AS default_block_merge_field_name,
							COALESCE( emf.details, \'{"block_index" : 0 }\' ) -> \'block_index\' as block_index
						FROM
							external_merge_fields emf
							LEFT JOIN LATERAL unnest ( emf.default_merge_field_ids ) WITH ORDINALITY AS a ( id, nr ) ON TRUE
							LEFT JOIN default_merge_fields dmf ON ( a.id = dmf.id )
							LEFT JOIN default_merge_fields dmfb ON ( dmfb.id = emf.block_default_merge_field_id )
						WHERE
							state_code = \'' . $strStateCode . '\'
							AND ' . $strWhereAssociation . '
							AND emf.external_form_keys && ARRAY[' . sqlStrImplode( $arrstrExternalFormKeys ) . ']::VARCHAR[]
							AND emf.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . '
						GROUP BY
							emf.external_merge_field_name,
							emf.max_length,
							emf.data_type,
							dmfb.field,
							block_index
						ORDER BY
							block_index';

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

    public static function fetchAllPaginatedExternalMergeFieldsWithGroupNameByMergeFieldNameByStateCodeByMergeFieldGroupIdByTransmissionVendorId( $objDatabase, $objPagination, $intTransmissionVendorId, $strExternalMergeFieldName, $strStateCode, $intMergeFieldGroupId, $boolIsCount = false ) {

        if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
            $strOrderByType = ( 0 == $objPagination->getOrderByType() ) ? ' ASC ' : ' DESC ';
            $intOffset = ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
            $intLimit = ( int ) $objPagination->getPageSize();
        }

        $strFilterByTransmissionVendorId    = ( true == valId( $intTransmissionVendorId ) ? ' emf.transmission_vendor_id = ' . $intTransmissionVendorId : ' 1 = 1' );
        $strFilterByExternalMergeFieldName  = ( true == valStr( $strExternalMergeFieldName ) ? ' AND emf.external_merge_field_name ILIKE \'%' . $strExternalMergeFieldName . '%\'' : '' );
        $strFilterByStateCode               = ( true == valStr( $strStateCode ) ? ' AND emf.state_code ILIKE \'%' . $strStateCode . '%\'' : '' );
        $strFilterByMergeFieldGroupId       = ( true == valId( $intMergeFieldGroupId ) ? ' AND emf.merge_field_group_id =  ' . ( int ) $intMergeFieldGroupId : '' );

        if( true == $boolIsCount ) {
            $strSql = 'SELECT
						count( emf.id ) ';
        } else {
            $strSql = 'SELECT
						emf.id,
					    emf.external_merge_field_name,
					    emf.state_code,
					    emf.merge_field_group_id,
					    emf.block_default_merge_field_id,
					    emf.transmission_vendor_id,
					    emf.default_merge_field_ids,
					    emf.deleted_on,
					    emf.external_form_keys,
					    emf.apartment_association,
					    mfg.name AS group_name,
					    array_to_json( string_to_array ( string_agg ( dmf_s.field, \',\' ), \',\' ) ) AS default_merge_field_names,
					    dmf_b.field AS default_block_merge_field_name';
        }

        $strSql     .= ' FROM
							    external_merge_fields emf
							    LEFT JOIN merge_field_groups mfg ON ( mfg.id = emf.merge_field_group_id ) 
							    LEFT JOIN default_merge_fields dmf_s ON ( dmf_s.id = ANY( emf.default_merge_field_ids ) )
							    LEFT JOIN default_merge_fields dmf_b ON ( dmf_b.id = emf.block_default_merge_field_id )
							WHERE ' . $strFilterByTransmissionVendorId . $strFilterByExternalMergeFieldName . $strFilterByStateCode . $strFilterByMergeFieldGroupId . ' ';

        if( false == $boolIsCount && true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
            $strSql .= 'GROUP BY
							emf.id,
						    emf.external_merge_field_name,
						    emf.state_code,
						    emf.merge_field_group_id,
						    emf.block_default_merge_field_id,
						    emf.default_merge_field_ids,
						    emf.deleted_on,
						    emf.external_form_keys,
						    mfg.name,
						    mfg.id,
						    dmf_b.field 
						ORDER BY 
							' . $objPagination->getOrderByField() . ' ' . $strOrderByType .
                ' OFFSET ' . ( int ) $intOffset .
                ' LIMIT ' . ( int ) $intLimit;
        }

        $arrstrResultSet = fetchData( $strSql, $objDatabase );

        return ( false == $boolIsCount ) ? $arrstrResultSet : $arrstrResultSet[0]['count'];
    }

    public static function fetchExternalMergeFieldByExternalMergeFieldNameByStateCodeByTransmissionVendorId( string $strExternalMergeFieldName, string $strStateCode, int $intTransmissionVendorId, $strApartmentAssociation, CDatabase $objDatabase ) {

        if( false == valStr( $strExternalMergeFieldName ) || false == valStr( $strStateCode ) || false == valId( $intTransmissionVendorId ) ) {
            return NULL;
        }
        if( false == valStr( $strApartmentAssociation ) && CTransmissionVendor::TENANT_TECH == $intTransmissionVendorId ) {
            $strApartmentAssociationQuery = ' true';
        } else {
            $strApartmentAssociationQuery = ' apartment_association =  \'' . $strApartmentAssociation . '\'';
        }

        $strSql = 'SELECT
						*
					FROM
						external_merge_fields
					WHERE
						transmission_vendor_id = ' . $intTransmissionVendorId . '
						AND state_code = \'' . $strStateCode . '\'
						AND external_merge_field_name =  \'' . $strExternalMergeFieldName . '\' 
                        AND ' . $strApartmentAssociationQuery;

        return self::fetchExternalMergeField( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldMaxId( $objDatabase ) {
        $strSql = 'SELECT
						MAX( id ) AS id
					FROM
						external_merge_fields';

        return fetchData( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldCountByExternalMergeFieldNameByStateCodeByTransmissionVendorId( string $strExternalMergeFieldName, string $strStateCode, int $intTransmissionVendorId, CDatabase $objDatabase ) {

        if( false == valStr( $strExternalMergeFieldName ) || false == valStr( $strStateCode ) || false == valId( $intTransmissionVendorId ) ) {
            return NULL;
        }

        $strWhere = 'WHERE
						transmission_vendor_id = ' . $intTransmissionVendorId . '
                        AND state_code = \'' . $strStateCode . '\'
						AND external_merge_field_name =  \'' . $strExternalMergeFieldName . '\'';

        return parent::fetchRowCount( $strWhere, 'default_merge_fields', $objDatabase );
    }

    public static function fetchExternalMergeFieldsByExternalMergeFieldNameByTransmissionVendorId( string $strExternalMergeFieldName, int $intTransmissionVendorId, CDatabase $objDatabase ) {

        if( false == valStr( $strExternalMergeFieldName ) || false == valId( $intTransmissionVendorId ) ) {
            return NULL;
        }

        $strSql = 'SELECT
						*
					FROM
						external_merge_fields
					WHERE
						transmission_vendor_id = ' . $intTransmissionVendorId . '
						AND external_merge_field_name =  \'' . $strExternalMergeFieldName . '\'
                    ORDER BY state_code';

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldsByTransmissionVendorIdByExternalMergeFieldNameByStateCodes( int $intTransmissionVendorId, string $strExternalMergeFieldName, $arrobjApartmentAssociationAndStateCodes, CDatabase $objDatabase ) : array {
        if( false == valId( $intTransmissionVendorId ) || false == valStr( $strExternalMergeFieldName ) ) {
            return [];
        }

        $strSqlWhereStateCode = '';

        if( true == valArr( $arrobjApartmentAssociationAndStateCodes ) ) {
            $strSqlWhereStateCode = 'AND ( COALESCE( apartment_association, \'null\' ), state_code ) IN ( ' . implode( ', ', $arrobjApartmentAssociationAndStateCodes ) . ' )';
        }

        $strSql = 'SELECT
						*
					FROM
						external_merge_fields emf
					WHERE
						transmission_vendor_id = ' . $intTransmissionVendorId . '
						AND external_merge_field_name like \'' . $strExternalMergeFieldName . '\'' . $strSqlWhereStateCode;

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

    public static function fetchExternalMergeFieldsByIds( array $arrintExternalMergeFieldIds, CDatabase $objDatabase ) : array {
        if( false == valArr( $arrintExternalMergeFieldIds ) ) {
            return [];
        }

        $strSql = 'SELECT
						*
					FROM
						external_merge_fields emf 
					WHERE
						id in ( \'' . implode( '\',\'', $arrintExternalMergeFieldIds ) . '\' )';

        return self::fetchExternalMergeFields( $strSql, $objDatabase );
    }

}
?>