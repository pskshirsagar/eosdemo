<?php

if( false == defined( 'TEMP_UPLOAD_DIR' ) ) {
	if( false == isset( $_ENV['TMP'] ) ) {
		$_ENV['TMP'] = '';
	}
	define( 'TEMP_UPLOAD_DIR', 	CONFIG_CACHE_DIRECTORY );
}

define( 'FFMPEG_EXE_FILE_PATH', 'c://wamp//php//ffmpeg_files//ffmpeg' );

class CCompanyMediaFile extends CBaseCompanyMediaFile {

	use TEosStoredObject;

	const IMAGE_MAX_X 	= 4096;
	const IMAGE_MAX_Y	= 4096;

	const IMAGE_THUMB_X	= 150;
	const IMAGE_THUMB_Y	= 150;

	const IMAGE_QUALITY_CROPPED	= 85;
	const IMAGE_QUALITY_MAX		= 85;

	const IMAGE_MAX_WIDTH = 910;
	const IMAGE_MAX_HEIGHT = 1024;

	protected $m_intOrderNum;
	protected $m_intPropertyId;
	protected $m_intCompanyAreaId;
	protected $m_intPropertyUnitId;
	protected $m_intMediaCategoryId;
	protected $m_intFloorPlanMediaId;
	protected $m_intCompanyAreaMediaId;
	protected $m_intTemplateSlotImageId;
	protected $m_intBrochureSlotImageId;
	protected $m_intPropertyFloorplanId;

	protected $m_fltImageScale;
	protected $m_strMediaLibraryUri;
	protected $m_strMediaCategoryName;

	protected $m_intCustomerId;
	protected $m_intApplicantId;
	protected $m_intArTransactionId;

	protected $m_strData;
	protected $m_strViewDirection;
	protected $m_strUploadDocumentPreferenceKey;

	protected $m_boolIsDefault;

	/**
	 * Get Functions
	 */

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function getFloorplanMediaId() {
		return $this->m_intFloorPlanMediaId;
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function getCompanyAreaMediaId() {
		return $this->m_intCompanyAreaMediaId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getCompanyAreaId() {
		return $this->m_intCompanyAreaId;
	}

	public function getTemplateSlotImageId() {
		return $this->m_intTemplateSlotImageId;
	}

	public function getImageScale() {
		return $this->m_fltImageScale;
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function getViewDirection() {
		return $this->m_strViewDirection;
	}

	public function getUploadDocumentPreferenceKey() {
		return $this->m_strUploadDocumentPreferenceKey;
	}

	public function getData() {
		return $this->m_strData;
	}

	// To serve every image from media library domain only.

	public function getMediaLibraryThumbnailUri() {
		return ( true == valStr( $this->getThumbnailUri() ) ? CConfig::get( 'media_library_path' ) . $this->getThumbnailUri() : NULL );
	}

	public function getMediaLibraryFullSizeUri() {
		// imageOptimizer Condition for converting images to webp format on the fly.
		if( true === defined( 'CONFIG_COMMON_PATH' ) && true === defined( 'CONFIG_LOAD_WEBP' ) && \CMediaType::WEBSITE_FAVICON != $this->getMediaTypeId() ) {
			return ( true === valStr( $this->getFullsizeUri() ) ? CONFIG_COMMON_PATH . '/images/imageOptimizer.php?f=webp&src=' . $this->getFullsizeUri() : NULL );
		}

		return ( true == valStr( $this->getFullsizeUri() ) ? CConfig::get( 'media_library_path' ) . '/' . $this->getFullsizeUri() : NULL );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getMediaCategoryName() {
		return $this->m_strMediaCategoryName;
	}

	public function getMediaCategoryId() {
		return $this->m_intMediaCategoryId;
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['data'] ) )					        $this->setData( $arrstrValues['data'] );
		if( true == isset( $arrstrValues['order_num'] ) )				        $this->setOrderNum( $arrstrValues['order_num'] );
		if( true == isset( $arrstrValues['is_default'] ) )				        $this->setIsDefault( $arrstrValues['is_default'] );
		if( true == isset( $arrstrValues['property_id'] ) )				        $this->setPropertyId( $arrstrValues['property_id'] );
		if( true == isset( $arrstrValues['image_scale'] ) )				        $this->setImageScale( $arrstrValues['image_scale'] );
		if( true == isset( $arrstrValues['customer_id'] ) )				        $this->setCustomerId( $arrstrValues['customer_id'] );
		if( true == isset( $arrstrValues['applicant_id'] ) )			        $this->setApplicantId( $arrstrValues['applicant_id'] );
		if( true == isset( $arrstrValues['view_direction'] ) )			        $this->setViewDirection( $arrstrValues['view_direction'] );
		if( true == isset( $arrstrValues['company_area_id'] ) )			        $this->setCompanyAreaId( $arrstrValues['company_area_id'] );
		if( true == isset( $arrstrValues['property_unit_id'] ) )		        $this->setPropertyUnitId( $arrstrValues['property_unit_id'] );
		if( true == isset( $arrstrValues['media_category_id'] ) )		        $this->setMediaCategoryId( $arrstrValues['media_category_id'] );
		if( true == isset( $arrstrValues['ar_transaction_id'] ) )		        $this->setArTransactionId( $arrstrValues['ar_transaction_id'] );
		if( true == isset( $arrstrValues['floorplan_media_id'] ) )		        $this->setFloorplanMediaId( $arrstrValues['floorplan_media_id'] );
		if( true == isset( $arrstrValues['media_category_name'] ) )		        $this->setMediaCategoryName( $arrstrValues['media_category_name'] );
		if( true == isset( $arrstrValues['company_area_media_id'] ) )	        $this->setCompanyAreaMediaId( $arrstrValues['company_area_media_id'] );
		if( true == isset( $arrstrValues['property_floorplan_id'] ) )	        $this->setPropertyFloorplanId( $arrstrValues['property_floorplan_id'] );
		if( true == isset( $arrstrValues['template_slot_image_id'] ) )	        $this->setTemplateSlotImageId( $arrstrValues['template_slot_image_id'] );
		if( true == isset( $arrstrValues['upload_document_preference_key'] ) )	$this->setUploadDocumentPreferenceKey( $arrstrValues['upload_document_preference_key'] );
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = $intOrderNum;
	}

	public function setFloorplanMediaId( $intFloorplanMediaId ) {
		$this->m_intFloorPlanMediaId = $intFloorplanMediaId;
	}

	public function setImageScale( $fltImageScale ) {
		$this->m_fltImageScale = $fltImageScale;
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->set( 'm_boolIsDefault', CStrings::strToBool( $boolIsDefault ) );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setCompanyAreaId( $intCompanyAreaId ) {
		$this->m_intCompanyAreaId = $intCompanyAreaId;
	}

	public function setTemplateSlotImageId( $intTemplateSlotImageId ) {
		$this->m_intTemplateSlotImageId = $intTemplateSlotImageId;
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->m_intPropertyFloorplanId = $intPropertyFloorplanId;
	}

	public function setCompanyAreaMediaId( $intCompanyAreaMediaId ) {
		return $this->m_intCompanyAreaMediaId = $intCompanyAreaMediaId;
	}

	public function setFileName( $arrstrFileName, $boolFilenameCheck = false ) {
		$intFileNameLength = \Psi\CStringService::singleton()->strlen( $arrstrFileName );
		for( $intCount = 0; $intCount < $intFileNameLength; ++$intCount ) {
			if( 126 < ord( $arrstrFileName[$intCount] ) ) {
				$arrstrFileName[$intCount] = '';
			}
		}

		if( true == $boolFilenameCheck ) {
			$arrstrCharsFilenameCheck   = [ '"', "'", '<','>' ];
			$arrstrCharsFilenameReplace = [ '', '', '', '' ];
		} else {
			$arrstrCharsFilenameCheck   = [ '"', "'", '&', '<','>' ];
			$arrstrCharsFilenameReplace = [ '', '', '', '', '' ];
		}
		$arrstrFileName = convertNonAsciiCharactersToAscii( str_replace( $arrstrCharsFilenameCheck, $arrstrCharsFilenameReplace, $arrstrFileName ) );
		parent::setFileName( $arrstrFileName );
	}

	public function setCaption( $arrstrFileName ) {
		$intFileNameLength = \Psi\CStringService::singleton()->strlen( $arrstrFileName );
		for( $intCount = 0; $intCount < $intFileNameLength; ++$intCount ) {
			if( 126 < ord( $arrstrFileName[$intCount] ) ) {
				$arrstrFileName[$intCount] = '';
			}
		}

		$arrstrFileName = str_replace( [ '"', "'", '&', '<', '>' ], [ '', '', '', '', '' ], $arrstrFileName );
		parent::setCaption( $arrstrFileName );
	}

	public function setMediaAlt( $arrstrMediaAlt, $strLocaleCode = NULL ) {

		if( CLocale::DEFAULT_LOCALE == $strLocaleCode ) {
			$intMediaAltLength = \Psi\CStringService::singleton()->strlen( $arrstrMediaAlt );
			for( $intCount = 0; $intCount < $intMediaAltLength; ++$intCount ) {
				if( 126 < ord( $arrstrMediaAlt[$intCount] ) ) {
					$arrstrMediaAlt[$intCount] = '';
				}
			}
			$arrstrMediaAlt = str_replace( [ '"', '&', '<', '>' ], [ '\'', '', '', '', '' ], \Psi\CStringService::singleton()->preg_replace( '/(\/|\"|\s{2,})/i', '', strip_tags( $arrstrMediaAlt ) ) );
		}

		parent::setMediaAlt( $arrstrMediaAlt, $strLocaleCode );
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setViewDirection( $strViewDirection ) {
		$this->m_strViewDirection = $strViewDirection;
	}

	public function setUploadDocumentPreferenceKey( $strUploadDocumentPreferenceKey ) {
		$this->m_strUploadDocumentPreferenceKey = $strUploadDocumentPreferenceKey;
	}

	public function setData( $strData ) {
		$this->m_strData = $strData;
	}

	public function setMediaCategoryName( $strMediaCategoryName ) {
		$this->m_strMediaCategoryName = $strMediaCategoryName;
	}

	public function setMediaCategoryId( $intMediaCategoryId ) {
		$this->m_intMediaCategoryId = $intMediaCategoryId;
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->m_intArTransactionId = $intArTransactionId;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyMedia( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyMedias::createService()->fetchPropertyMediaByCompanyMediaFileIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchChildAssociatedMedias( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchChildAssociatedMediaFilesByParentCompanyMediaFileId( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMarketingMedia( $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMedias::createService()->fetchMarketingMediaByIdByCid( $this->getMarketingMediaId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valMediaCaption() {
		$boolIsValid = true;

		if( false == isset( $this->m_strMediaCaption ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_caption', __( 'Caption is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strFileName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Invalid File Name.' ) ) );
		}

		return $boolIsValid;
	}

	public function valExternalUri() {
		$boolIsValid = true;

		if( true == valStr( $this->getExternalUri() ) && false == CValidation::checkUrl( $this->getExternalUri(), false, true ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFileName();
				$boolIsValid &= $this->valExternalUri();
				break;

			case VALIDATE_DELETE:
				break;

			case 'virtual_tour_insert_or_update':
			case 'video_insert_or_update':
			case 'google_and_matterport_360_insert_or_update':
				$boolIsValid &= $this->valMediaCaption();
				break;

            case 'validate_floorplan_media':
                $boolIsValid &= $this->valFileName();
                $boolIsValid &= $this->valExternalUri();
                $boolIsValid &= $this->valMediaCaption();
                break;

            default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	// Overrides base delete class to also delete generated image files.  [Port from old code.]

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$intId = $this->getId();

		if( false == is_numeric( $intId ) || 1 > $intId ) {
			trigger_error( __( 'Invalid File Remove Request: Id not present.' ), E_USER_ERROR );
		}

		// Delete the object and the file
		// Delete only if path has got the right cid. This excludes any operations from test client on parent client
		if( parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			if( true == preg_match( '/\/' . $this->getCid() . '\//', $this->getFullsizeUri() ) ) {
				if( 0 < strlen( $this->getFullsizeUri() ) && true == file_exists( $strFullSizeFilePath = PATH_MOUNTS . $this->getFullsizeUri() ) ) {
					if( false == unlink( $strFullSizeFilePath ) ) {
						trigger_error( __( 'Invalid File Remove Request:  Fullsize File could not be removed from the Media Library.' ), E_USER_ERROR );
					}
				}

				if( 0 < strlen( $this->getThumbnailUri() ) && true == file_exists( $strThumbnailFilePath = PATH_MOUNTS . $this->getThumbnailUri() ) && false == unlink( $strThumbnailFilePath ) ) {
					trigger_error( __( 'Invalid File Remove Request:  Thumbnail File could not be removed from the Media Library.' ), E_USER_ERROR );
				}
			}
			return true;
		}

		return false;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == $this->validate( NULL ) ) {
			return false;
		}
		return parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function getCropperCookieData() {

		$arrmixCookieData = [];

		// Get all the cookie info
		if( isset( $_COOKIE['media_library_file']['bgcolor'] ) && 0 < strlen( $_COOKIE['media_library_file']['bgcolor'] ) ) {
			$arrmixCookieData['bgColor'] = $_COOKIE['media_library_file']['bgcolor'];
		} else {
			$arrmixCookieData['bgColor'] = '0xFFFFFF';
		}

		// Get all the cookie info for the selected file id
		$arrmixCookieData['boolIsCookieDataError'] = false;
		if( ( !isset( $_COOKIE['media_library_file']['selected_file_id'] ) || $_COOKIE['media_library_file']['selected_file_id'] <= 0 ) || ( !isset( $_COOKIE['media_library_file']['location_width'] ) || $_COOKIE['media_library_file']['location_width'] <= 0 ) || ( !isset( $_COOKIE['media_library_file']['location_height'] ) || $_COOKIE['media_library_file']['location_height'] <= 0 ) || ( !isset( $_COOKIE['media_library_file']['media_type_id'] ) || $_COOKIE['media_library_file']['media_type_id'] <= 0 ) ) {
			$arrmixCookieData['boolIsCookieDataError'] = true;
		} else {
			$arrmixCookieData['selectedFileId']	= $_COOKIE['media_library_file']['selected_file_id'];
			$arrmixCookieData['locationWidth']	= $_COOKIE['media_library_file']['location_width'];
			$arrmixCookieData['locationHeight']	= $_COOKIE['media_library_file']['location_height'];
			$arrmixCookieData['mediaTypeId']	= $_COOKIE['media_library_file']['media_type_id'];
		}

		return $arrmixCookieData;
	}

	public function calculateImageDimensions( $fltMovieCropScale, $fltMovieCropX, $fltMovieCropY, $fltMovieImageScale, $fltMovieImageX, $fltMovieImageY, $intImageWidth, $intImageHeight ) {
		$arrfltNewImageInfo = [];

		if( $fltMovieCropScale <= 0 ) {
			trigger_error( __( 'Error movie crop scale is zero CCropper.class.php - calculateImageDimensions' ), E_USER_WARNING );
			return false;
		}

		$fltImageToCropRatio = $fltMovieImageScale / $fltMovieCropScale;

		// Get the new size of the image
		$arrfltNewImageInfo['resize_x'] = round( $intImageWidth * $fltImageToCropRatio );
		$arrfltNewImageInfo['resize_y'] = round( $intImageHeight * $fltImageToCropRatio );

		// Calculate where the image will fit on the canvas
		$arrfltNewImageInfo['overlay_x'] = - 1 * floor( ( ( $intImageWidth * $fltMovieImageScale / 100 / 2 ) + ( $fltMovieCropX - $fltMovieImageX ) ) * 100 / $fltMovieCropScale );
		$arrfltNewImageInfo['overlay_y'] = - 1 * floor( ( ( $intImageHeight * $fltMovieImageScale / 100 / 2 ) + ( $fltMovieCropY - $fltMovieImageY ) ) * 100 / $fltMovieCropScale );

		// Set up the strings so that they can be used in the functions (add a '+' to the front of the dimension if it is positive to work on commandline)
		if( $arrfltNewImageInfo['overlay_x'] >= 0 ) $arrfltNewImageInfo['overlay_x'] = ( string ) '+' . $arrfltNewImageInfo['overlay_x'];
		if( $arrfltNewImageInfo['overlay_y'] >= 0 ) $arrfltNewImageInfo['overlay_y'] = ( string ) '+' . $arrfltNewImageInfo['overlay_y'];

		return $arrfltNewImageInfo;
	}

	public function getNewDimensions( $intOldX, $intOldY, $intMaxX, $intMaxY ) {

		// Make sure that at lease one of the old is bigger than the max
		if( $intOldX <= $intMaxX && $intOldY <= $intMaxY ) {
			return [ $intOldX, $intOldY ];
		}

		$intNewX = $intMaxX;
		$intNewY = $intMaxY;

		if( $intOldX > $intMaxX ) {
			$intNewY = ( int ) ( ( $intMaxX / $intOldX ) * $intOldY );
		} else {
			$intNewX = ( int ) ( ( $intMaxY / $intOldY ) * $intOldX );
		}
		return [ ( int ) $intNewX, ( int ) $intNewY ];
	}

	public function fileIsOtherImage( $strMimeType ) {

		if( \Psi\CStringService::singleton()->stristr( $strMimeType, 'image/' ) ) {
			return true;
		}
		return false;
	}

	public function imageConvert( $strFilePath, $strNewType ) {
		return !( false == CImagick::convert( $strFilePath, $strNewType . ':' . $strFilePath, '-quality 100' ) );
	}

	public function fileIsOfType( $arrstrMimeAndExt, $strMimeType, $strFileExtension ) {
		return ( in_array( \Psi\CStringService::singleton()->strtolower( $strMimeType ), $arrstrMimeAndExt['mimes'] ) ? true : false );
	}

	public function imageCheckSize( $strFilePath, $intX, $intY, $strExtension='', $boolSupportImageTransparency = false, $boolImageOptimize = false, $intCompression = 99 ) {

		// in case of png image transparancy when the height & width of image > 2048 OR in case of optimizing images with imagick library while uploading (jpg, jpeg, png images).

		if( ( true == $boolSupportImageTransparency && ( $intX > self::IMAGE_MAX_X || $intY > self::IMAGE_MAX_Y ) ) || true == $boolImageOptimize ) {

			require_once( PATH_LIBRARIES_PSI . 'ImageProcessor/CImageProcessor.class.php' );
			$objImageProcessor = CImageProcessor::getInstance( CImageProcessor::LIBRARY_TYPE_IMAGICK );
			$objImageProcessor->setSourceFilePath( $strFilePath );
			$objImageProcessor->setTargetFilePath( $strFilePath );
			if( true == is_null( $intCompression ) ) {
				$intCompression = 99;
			}
			$objImageProcessor->setCompression( $intCompression );
		}

		if( $intX > self::IMAGE_MAX_X || $intY > self::IMAGE_MAX_Y ) {

			//  resize file.
			if( ( true == $boolSupportImageTransparency && ( $intX > self::IMAGE_MAX_X || $intY > self::IMAGE_MAX_Y ) ) || true == $boolImageOptimize ) {
				// With imagick library

				$objImageProcessor->setWidth( self::IMAGE_MAX_X );
				$objImageProcessor->setHeight( self::IMAGE_MAX_Y );
				return $objImageProcessor->resize();

			} else {
				// With gd library
				// Since gd does not support saving aspect ratio, I have to recalculate the sizes first before the convert
				list( $intNewX, $intNewY ) = $this->getNewDimensions( $intX, $intY, self::IMAGE_MAX_X, self::IMAGE_MAX_Y );
				return CImagick::convert( $strFilePath, $strFilePath, '-quality 100 -resize ' . ( int ) $intNewX . 'x' . ( int ) $intNewY, $strExtension );
			}

		} else {
			// For now, while uploading jpg, jpeg (>= 100 kb), png (>=300 kb) images
			try{
				if( true == $boolImageOptimize ) {
					return $objImageProcessor->resize();
				}
			}catch( Exception $objException ){
				return false;
			}
		}
		return true;
	}

	public function imageThumbnail( $strDirectory, $strFilename, $intX, $intY, $strExtension='', $boolSupportImageTransparency = false ) {

		if( $intX > self::IMAGE_THUMB_X || $intY > self::IMAGE_THUMB_Y ) {
			list( $intX, $intY ) = $this->getNewDimensions( $intX, $intY, self::IMAGE_THUMB_X, self::IMAGE_THUMB_Y );
		}

		if( true == $boolSupportImageTransparency ) {

			require_once( PATH_LIBRARIES_PSI . 'ImageProcessor/CImageProcessor.class.php' );
			$objImageProcessor = CImageProcessor::getInstance( CImageProcessor::LIBRARY_TYPE_IMAGICK );
			$objImageProcessor->setSourceFilePath( $strDirectory . $strFilename );
			$objImageProcessor->setTargetFilePath( $strDirectory . $strFilename . '_thumb' );
			$objImageProcessor->setWidth( $intX );
			$objImageProcessor->setHeight( $intY );
			if( false == $objImageProcessor->resize() ) {
				return false;
			}

		} elseif( false == CImagick::convert( $strDirectory . $strFilename, $strDirectory . $strFilename . '_thumb', '-quality 65 -resize ' . ( int ) $intX . 'x' . ( int ) $intY, $strExtension ) ) {
			return false;
		}

		return $strFilename . '_thumb';
	}

	public function createFlvFromVideo( $strDirectory, $strFilename, $strFileExtension = NULL ) {
		$strTmpFilePath = $strDirectory . $strFilename;
		exec( 'avconv -i ' . $strTmpFilePath . ' -qscale 1 -ar 22050 -b 200 -r 15 -s 320x240 -f flv ' . $strTmpFilePath . '.flv' );
	}

	public function createThumbnailFromVideo( $strDirectory, $strFilename, $strFileExtension = NULL ) {
		$strTmpFilePath = $strDirectory . $strFilename;

		if( true == file_exists( $strTmpFilePath . '.flv' ) && 0 < filesize( $strTmpFilePath . '.flv' ) ) {
			switch( $strFileExtension ) {

				case 'flv':
				case '3gp':
				case 'avi':
				case 'mpeg':
					exec( 'avconv -i ' . $strTmpFilePath . '.flv -f mjpeg -y -ss 00:00:03 -s 320x240 ' . $strTmpFilePath . '_thumb.jpg' );
					return $strFilename . '_thumb.jpg';
					break;

				default:
					exec( 'avconv -i ' . $strTmpFilePath . '.flv -f mjpeg -y -ss 00:00:03 -s 320x240 ' . $strTmpFilePath . '_thumb.jpg' );
					return $strFilename . '_thumb.jpg';
					break;
			}
		} else {
			return false;
		}
	}

	public function createCroppedImage( $strImageToCrop, $strFileExtension, $arrstrImageInfo, $arrintThumbDims, $intCanvasWidth, $intCanvasHeight, $strHexColor, $boolSupportImageTransparency = false ) {

		$strCanvasRoot = TEMP_UPLOAD_DIR . getUniqueString();

		$strCanvasName = $strCanvasRoot . '.' . $strFileExtension;

		$strCanvasThumbName = NULL;

		if( false == CImagick::composite( $strImageToCrop, $strCanvasName, $arrstrImageInfo, $intCanvasWidth, $intCanvasHeight, $strHexColor, self::IMAGE_QUALITY_MAX, $strFileExtension ) ) {
			return false;
		}

		// If there needs to be a thumbnail made
		if( true == valArr( $arrintThumbDims ) ) {
			$strCanvasThumbName = $strCanvasRoot . '_thumb.' . $strFileExtension;
			if( true == $boolSupportImageTransparency ) {
				require_once( PATH_LIBRARIES_PSI . 'ImageProcessor/CImageProcessor.class.php' );
				$objImageProcessor = CImageProcessor::getInstance( CImageProcessor::LIBRARY_TYPE_IMAGICK );
				$objImageProcessor->setSourceFilePath( $strCanvasName );
				$objImageProcessor->setTargetFilePath( $strCanvasThumbName );
				$objImageProcessor->setWidth( $arrintThumbDims['w'] );
				$objImageProcessor->setHeight( $arrintThumbDims['h'] );
				if( false == $objImageProcessor->resize() ) {
					return false;
				}
			} elseif( false == CImagick::convert( $strCanvasName, $strCanvasThumbName, '-quality ' . self::IMAGE_QUALITY_CROPPED . ' -resize ' . $arrintThumbDims['w'] . 'x' . $arrintThumbDims['h'], $strFileExtension ) ) {
				return false;
			}
		}

		return [ 'fullsize_uri' => $strCanvasName, 'thumbnail_uri' => $strCanvasThumbName ];
	}

	public function createWebmFromVideo( $strDirectory, $strFilename, $strFileExtension = NULL ) {
		$strTmpFilePath = $strDirectory . $strFilename;
		exec( 'avconv -i ' . $strTmpFilePath . ' -acodec libvorbis -aq 5 -ac 2 -qmax 25 -threads 2 ' . $strTmpFilePath . $strFileExtension );
	}

	public function createThumbnailFromMp4Video( $strDirectory, $strFilename ) {
		$strTmpFilePath = $strDirectory . $strFilename;
		if( true == file_exists( $strTmpFilePath . '.mp4' ) && 0 < filesize( $strTmpFilePath . '.mp4' ) ) {
			exec( 'avconv -i ' . $strTmpFilePath . '.mp4 -f mjpeg -y -ss 00:00:03 -s 320x240 ' . $strTmpFilePath . '_thumb.jpg' );
			return $strFilename . '_thumb.jpg';
		} else {
			return false;
		}
	}

	public function createMp3FromAudio( $strDirectory, $strFilename, $strFileExtension = NULL ) {
		$strTmpFilePath = $strDirectory . $strFilename;
		exec( 'avconv -i ' . $strTmpFilePath . ' -id3v2_version 3 -qscale 1 -acodec libmp3lame -ac 1 -ar 44100 -f mp3 ' . $strTmpFilePath . '.mp3' );
	}

	public function getCachedImageOrThumbnailerPath( $intWidth = 100, $intHeight = 100, $boolIsThumbnailUri = false ) {

		if( false != is_null( $this->getFileName() ) ) {
			return '/Common/images/no_image.gif';
		}

			$strThumbnailerPath = '/Common/images/thumbNailer.php?src=';

			if( true == defined( 'CONFIG_COMMON_PATH' ) ) {
				$strThumbnailerPath = CONFIG_COMMON_PATH . '/images/thumbNailer.php?src=';
			}

			// imageOptimizer Condition for converting images to webp format on the fly.
			if( true === defined( 'CONFIG_COMMON_PATH' ) && true === defined( 'CONFIG_LOAD_WEBP' ) ) {
				$strThumbnailerPath = CONFIG_COMMON_PATH . '/images/imageOptimizer.php?f=webp&src=';
			}

			if( true == $boolIsThumbnailUri ) {
				$strThumbnailerPath .= $this->getThumbnailUri() . '&w=' . ( int ) $intWidth . '&h=' . ( int ) $intHeight;
			} else {
				$strThumbnailerPath .= $this->getFullsizeUri() . '&w=' . ( int ) $intWidth . '&h=' . ( int ) $intHeight;
			}

			return $strThumbnailerPath;
	}

	public function flipImage( $intCurrentUserId, $objDatabase ) {

		// get prev image and thumbnail path
		$arrstrPathInfo   = pathinfo( $this->getFullsizeUri() );
		$strFilePath	  = $this->getFullsizeUri();
		$strThumbnailPath = $this->getThumbnailUri();

		// flip the image
		$objImg = new Imagick( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $this->getFullsizeUri(), 1, \Psi\CStringService::singleton()->strlen( $this->getFullsizeUri() ) - 1 ) );

		if( false == $objImg->flopImage() ) {
			return false;
		} else {

			// generate image and thumnail file names
			$strFileName 				= getUniqueString();
			$strThumbnailFileName 		= $strFileName . '_thumb.' . $arrstrPathInfo['extension'];
			$strFileName 				= $strFileName . '.' . $arrstrPathInfo['extension'];

			// create new image
			if( false == $objImg->writeImage( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName ) ) {
				return false;
			} else {
				// create image thumbnail
				$objNewImg = new Imagick( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName );
				$objNewImg->adaptiveResizeImage( .5 * $objNewImg->getImageWidth(), 0, false );

				if( false == $objNewImg->writeImage( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strThumbnailFileName ) ) {
					return false;
				} else {
					// set new values
					$this->setFullsizeUri( $arrstrPathInfo['dirname'] . '/' . $strFileName );
					$this->setThumbnailUri( $arrstrPathInfo['dirname'] . '/' . $strThumbnailFileName );
					$this->setFileName( $strFileName );
					$this->setFileSize( filesize( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName ) );

					if( false == $this->update( $intCurrentUserId, $objDatabase ) ) {
						return false;
					} else {
						// delete old images
						unlink( PATH_MOUNTS . $strFilePath );
						unlink( PATH_MOUNTS . $strThumbnailPath );
						return true;
					}
				}
			}
		}
	}

	public function resizeImage( $intCurrentUserId, $arrintCompanyMediaFileData, $objDatabase ) {

		// get prev image and thumbnail path
		$arrstrPathInfo   = pathinfo( $this->getFullsizeUri() );
		$strFilePath	  = $this->getFullsizeUri();
		$strThumbnailPath = $this->getThumbnailUri();

		// resize the image
		$objImg = new Imagick( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $this->getFullsizeUri(), 1, \Psi\CStringService::singleton()->strlen( $this->getFullsizeUri() ) - 1 ) );

		if( false == $objImg->adaptiveResizeImage( $arrintCompanyMediaFileData['width'], $arrintCompanyMediaFileData['height'], false ) ) {
			return false;
		} else {

			// generate image and thumnail file names
			$strFileName 				= getUniqueString();
			$strThumbnailFileName 		= $strFileName . '_thumb.' . $arrstrPathInfo['extension'];
			$strFileName 				= $strFileName . '.' . $arrstrPathInfo['extension'];

			// create new image
			if( false == $objImg->writeImage( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName ) ) {
				return false;
			} else {
				// create image thumbnail
				$objNewImg = new Imagick( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName );
				$intWidth 	= $objNewImg->getImageWidth();
				$intHeight 	= $objNewImg->getImageHeight();

				$objNewImg->adaptiveResizeImage( .5 * $objNewImg->getImageWidth(), 0, false );

				if( false == $objNewImg->writeImage( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strThumbnailFileName ) ) {
					return false;
				} else {
					// set new values
					$this->setFullsizeUri( $arrstrPathInfo['dirname'] . '/' . $strFileName );
					$this->setThumbnailUri( $arrstrPathInfo['dirname'] . '/' . $strThumbnailFileName );
					$this->setFileName( $strFileName );
					$this->setFileSize( filesize( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName ) );
					$this->setMediaWidth( $intWidth );
					$this->setMediaHeight( $intHeight );

					if( false == $this->update( $intCurrentUserId, $objDatabase ) ) {
						return false;
					} else {
						// delete old images
						unlink( PATH_MOUNTS . $strFilePath );
						unlink( PATH_MOUNTS . $strThumbnailPath );
						return true;
					}

				}

			}

		}
	}

	public function cropImage( $intCurrentUserId, $arrmixCompanyMediaFileData, $objDatabase ) {

		list( $intWidth, $intHeight ) = getimagesize( PATH_MOUNTS . $this->getFullsizeUri() );

		$intViewPortWidth 	= ( int ) $arrmixCompanyMediaFileData['viewPortW'];
		$intViewPortHeight	= ( int ) $arrmixCompanyMediaFileData['viewPortH'];
		$intImageWidth		= ( int ) $arrmixCompanyMediaFileData['imageW'];
		$intImageHeight		= ( int ) $arrmixCompanyMediaFileData['imageH'];
		$intImageX			= $arrmixCompanyMediaFileData['imageX'];
		$intImageY			= $arrmixCompanyMediaFileData['imageY'];
		$intSelectorWidth	= ( int ) $arrmixCompanyMediaFileData['selectorW'];
		$intSelectorHeight  = ( int ) $arrmixCompanyMediaFileData['selectorH'];

		if( false == valId( $intViewPortWidth ) || false == valId( $intViewPortHeight ) || false == valId( $intImageWidth ) || false == valId( $intImageHeight ) || false == valId( $intSelectorWidth ) || false == valId( $intSelectorHeight ) ) {
			return false;
		}

		$strArrSourceParts = explode( '.', $this->getFullsizeUri() );
		$strFileExtension  = end( $strArrSourceParts );

		$resImage  = $this->createImageFrom( $strFileExtension );
		$intWidth  = imagesx( $resImage );
		$intHeight = imagesy( $resImage );

		// Resample
		$resImage_p = imagecreatetruecolor( $intImageWidth, $intImageHeight );

		if( false == $resImage_p ) {
			return false;
		}

		$this->setTransparency( $resImage, $resImage_p, $strFileExtension, $arrmixCompanyMediaFileData['backgroundColor'] );
		imagecopyresampled( $resImage_p, $resImage, 0, 0, 0, 0, $intImageWidth, $intImageHeight, $intWidth, $intHeight );
		imagedestroy( $resImage );
		$intWidthR 	= imagesx( $resImage_p );
		$intHeightR = imagesy( $resImage_p );

		$intSelectorX = $arrmixCompanyMediaFileData['selectorX'];
		$intSelectorY = $arrmixCompanyMediaFileData['selectorY'];

		$intDstX = $intSrcX = $intDstY = $intSrcY = 0;

		if( $intImageX > 0 ) {
			$intDstX = abs( $intImageX );
		} else {
			$intSrcX = abs( $intImageX );
		}
		if( $intImageY > 0 ) {
			$intDstY = abs( $intImageY );
		} else {
			$intSrcY = abs( $intImageY );
		}

		$resViewport = imagecreatetruecolor( $intViewPortWidth, $intViewPortHeight );

		if( false == $resViewport ) {
			return false;
		}

		if( $strFileExtension == 'jpg' || $strFileExtension == 'gif' ) {
			$arrintBackgroundColors = explode( ',', $arrmixCompanyMediaFileData['backgroundColor'] );

			if( \Psi\Libraries\UtilFunctions\count( $arrintBackgroundColors ) > 1 ) {
				imagefill( $resViewport, 0, 0, imagecolorallocate( $resViewport, $arrintBackgroundColors[0], $arrintBackgroundColors[1], $arrintBackgroundColors[2] ) );
			} else {
				imagefill( $resViewport, 0, 0, imagecolorallocate( $resViewport, 255, 255, 255 ) );
			}

		}
		$this->setTransparency( $resImage_p, $resViewport, $strFileExtension, $arrmixCompanyMediaFileData['backgroundColor'] );

		imagecopy( $resViewport, $resImage_p, $intDstX, $intDstY, $intSrcX, $intSrcY, $intImageWidth, $intImageHeight );
		imagedestroy( $resImage_p );

		$resSelector = imagecreatetruecolor( $intSelectorWidth, $intSelectorHeight );

		if( false == $resSelector ) {
			return false;
		}

		if( $strFileExtension == 'jpg' || $strFileExtension == 'gif' ) {
			$arrintBackgroundColors = explode( ',', $arrmixCompanyMediaFileData['backgroundColor'] );

			if( \Psi\Libraries\UtilFunctions\count( $arrintBackgroundColors ) > 1 ) {
				imagefill( $resSelector, 0, 0, imagecolorallocate( $resSelector, $arrintBackgroundColors[0], $arrintBackgroundColors[1], $arrintBackgroundColors[2] ) );
			} else {
				imagefill( $resSelector, 0, 0, imagecolorallocate( $resSelector, 255, 255, 255 ) );
			}
		}
		$this->setTransparency( $resViewport, $resSelector, $strFileExtension, $arrmixCompanyMediaFileData['backgroundColor'] );
		imagecopy( $resSelector, $resViewport, 0, 0, $intSelectorX, $intSelectorY, $intViewPortWidth, $intViewPortHeight );

		$arrstrPathInfo = pathinfo( $this->getFullsizeUri() );

		// generate image and thumnail file names
		$strFileName 			= getUniqueString();
		$strThumbnailFileName 	= $strFileName . '_thumb.' . $arrstrPathInfo['extension'];
		$strFileName 			= $strFileName . '.' . $arrstrPathInfo['extension'];

		// create new image
		if( false == $this->parseImage( $strFileExtension, $resSelector, PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName ) ) {
			return false;
		} else {

			$objImg = new Imagick( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName );

			// if x, y position of cropped image is in minus
			if( ( $intImageX < 0 && ( $intImageWidth - abs( $intImageX ) - $intSelectorX ) < $intSelectorWidth ) || ( $intImageY < 0 && ( $intImageHeight - abs( $intImageY ) - $intSelectorY ) < $intSelectorHeight ) ) {

				$intStartColorPointX	= 0;
				$intStartColorPointY	= 0;
				$intColorRegionWidth	= 0;
				$intColorRegionHeight	= 0;
				$strDefaultBgColor		= '#ffffff';
				$boolImageHasBlackLine	= false;

				// overwrite strDefaultBgColor
				if( '' != $arrmixCompanyMediaFileData['backgroundColorInHex'] ) {
					$strDefaultBgColor = '#' . $arrmixCompanyMediaFileData['backgroundColorInHex'];
				}

				// if image x point is in minus
				if( $intImageX < 0 && ( $intImageWidth - abs( $intImageX ) - $intSelectorX ) < $intSelectorWidth ) {
					$intStartColorPointX = $intImageWidth - abs( $intImageX ) - $intSelectorX;
				}

				// if image y point is in minus
				if( $intImageY < 0 && ( $intImageHeight - abs( $intImageY ) - $intSelectorY ) < $intSelectorHeight ) {
					$intStartColorPointY = $intImageHeight - abs( $intImageY ) - $intSelectorY;
				}

				if( 0 != $intStartColorPointX ) {

					$intColorRegionWidth	= $intSelectorWidth - $intStartColorPointX + 2;
					$intColorRegionHeight	= $intSelectorHeight;

					// create selected color canvas for horizantal portion
					$objCanvas = new Imagick();
					$objCanvas->newPseudoImage( $intColorRegionWidth, $intColorRegionHeight, 'gradient:' . $strDefaultBgColor . '-' . $strDefaultBgColor );

					// add selected color canvas on black portion of cropped image
					$objImg->compositeImage( $objCanvas, Imagick::COMPOSITE_OVER, $intStartColorPointX, 0 );

					$boolImageHasBlackLine = true;
				}
				if( 0 != $intStartColorPointY ) {

					$intColorRegionHeight = $intSelectorHeight - $intStartColorPointY + 2;
					$intColorRegionWidth = $intSelectorWidth;

					// create selected color canvas for vertical portion
					$objCanvas = new Imagick();
					$objCanvas->newPseudoImage( $intColorRegionWidth, $intColorRegionHeight, 'gradient:' . $strDefaultBgColor . '-' . $strDefaultBgColor );

					// add selected color canvas on black portion of cropped image
					$objImg->compositeImage( $objCanvas, Imagick::COMPOSITE_OVER, 0, $intStartColorPointY );

					$boolImageHasBlackLine = true;
				}

				// create image
				if( $boolImageHasBlackLine ) {
					$objImg->writeImage( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName );
				}
			}

			// create thumbnail of image
			$intWidth 	= $objImg->getImageWidth();
			$intHeight 	= $objImg->getImageHeight();

			$objImg->adaptiveResizeImage( .6 * $intWidth, 0, false );

			if( false == $objImg->writeImage( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strThumbnailFileName ) ) {
				return false;
			} else {

				$strPrevFileName = $this->getFullsizeUri();
				$strPrevThumbName = $this->getThumbnailUri();

				// set values
				$this->setFullsizeUri( $arrstrPathInfo['dirname'] . '/' . $strFileName );
				$this->setThumbnailUri( $arrstrPathInfo['dirname'] . '/' . $strThumbnailFileName );
				$this->setFileName( $strFileName );
				$this->setFileSize( filesize( PATH_MOUNTS . \Psi\CStringService::singleton()->substr( $arrstrPathInfo['dirname'], 1, \Psi\CStringService::singleton()->strlen( $arrstrPathInfo['dirname'] ) - 1 ) . '/' . $strFileName ) );
				$this->setMediaWidth( $intWidth );
				$this->setMediaHeight( $intHeight );

				imagedestroy( $resViewport );

				if( false == $this->update( $intCurrentUserId, $objDatabase ) ) {
					return false;
				} else {
					// @TODO: Need to delete the existing media file which was created for Media Association
					if( true == isset( $strPrevFileName ) && true == valStr( $strPrevFileName ) && true == CFileIo::fileExists( PATH_MOUNTS . $strPrevFileName ) && false == CFileIo::deleteFile( PATH_MOUNTS . $strPrevFileName ) ) {
						return false;
					}

					if( true == isset( $strPrevThumbName ) && true == valStr( $strPrevThumbName ) && true == CFileIo::fileExists( PATH_MOUNTS . $strPrevThumbName ) && false == CFileIo::deleteFile( PATH_MOUNTS . $strPrevThumbName ) ) {
						return false;
					}

					// Syncing with AWS S3.

					$strKey			= str_replace( '/media_library/', '', $this->getFullsizeUri() );
					$strInputFile	= PATH_MOUNTS . $this->getFullsizeUri();

					$this->syncMediaLibraryWithAws( $strKey, $strInputFile . $arrstrFilePathInfo['basename'] );

					$strKey			= str_replace( '/media_library/', '', $this->getThumbnailUri() );
					$strInputFile	= PATH_MOUNTS . $this->getThumbnailUri();

					$this->syncMediaLibraryWithAws( $strKey, $strInputFile . $arrstrThumbPathInfo['basename'] );

					return true;
				}
			}

		}
	}

	public function createImageFrom( $strExtension ) {

		switch( $strExtension ) {
			case 'png':
				return imagecreatefrompng( PATH_MOUNTS . $this->getFullsizeUri() );
				break;

			case 'jpeg':
				return imagecreatefromjpeg( PATH_MOUNTS . $this->getFullsizeUri() );
				break;

			case 'jpg':
				return imagecreatefromjpeg( PATH_MOUNTS . $this->getFullsizeUri() );
				break;

			case 'gif':
				return imagecreatefromgif( PATH_MOUNTS . $this->getFullsizeUri() );
				break;

			default:
				return imagecreatefrompng( PATH_MOUNTS . $this->getFullsizeUri() );

		}

	}

	public function parseImage( $strExtension, $resImage, $strFileName = NULL ) {

		switch( $strExtension ) {
			case 'png':
				return imagepng( $resImage, ( $strFileName != NULL ? $strFileName : '' ) );
				break;

			case 'jpeg':
				return imagejpeg( $resImage, ( $strFileName ? $strFileName : '' ), 90 );
				break;

			case 'jpg':
				return imagejpeg( $resImage, ( $strFileName ? $strFileName : '' ), 90 );
				break;

			case 'gif':
				return imagegif( $resImage, ( $strFileName ? $strFileName : '' ) );
				break;

			default:
				return imagepng( $resImage, ( $strFileName != NULL ? $strFileName : '' ) );

		}
	}

	public function setTransparency( $resImgSrc, $resImgDest, $strExtension, $strBackgroundColor = NULL ) {

		if( 'png' == $strExtension ) {

			$intTransparentIndex = imagecolortransparent( $resImgSrc );
			if( $intTransparentIndex >= 0 ) {

				$arrstrTransparentColor    	= imagecolorsforindex( $resImgSrc, $intTransparentIndex );
				$intTransparentIndex    	= imagecolorallocate( $resImgDest, $arrstrTransparentColor['red'], $arrstrTransparentColor['green'], $arrstrTransparentColor['blue'] );

				imagefill( $resImgDest, 0, 0, $intTransparentIndex );
				imagecolortransparent( $resImgDest, $intTransparentIndex );

			} elseif( $strExtension == 'png' ) {

				$arrintBackgroundColors = explode( ',', $strBackgroundColor );
				if( \Psi\Libraries\UtilFunctions\count( $arrintBackgroundColors ) > 1 ) {

					imagesavealpha( $resImgDest, true );
					imagealphablending( $resImgDest, true );
					$strColor = imagecolorallocate( $resImgDest, $arrintBackgroundColors[0], $arrintBackgroundColors[1], $arrintBackgroundColors[2] );
					imagefill( $resImgDest, 0, 0, $strColor );

				} else {
					imagesavealpha( $resImgDest, true );
					imagealphablending( $resImgDest, true );
					$strColor = imagecolorallocatealpha( $resImgDest, 0, 0, 0, 127 );
					imagefill( $resImgDest, 0, 0, $strColor );
				}
			}
		}
	}

	/**
	 *
	 * @param string $strKey : Aws key path.
	 * @param string $strInputFile : Mounts source path.
	 * @param string $boolIsUpload : To check , call is for upload(true) or delete(false).
	 * @param string $boolIsRemoveSource : Flag to check if need to remove the file from source.
	 */
	public function syncMediaLibraryWithAws( $strKey, $strInputFile = '', $boolIsUpload = true, $boolIsRemoveSource = false ) {

		if( 'production' == CONFIG_ENVIRONMENT && true == CONFIG_SYNC_MEDIA_LIBRARY_WITH_AWS ) {
			$arrmixObjectConfiguration = [
				'Container'		=> CAmazonS3CloudStorageGateway::ENTRATA_MEDIA_LIBRARY,
				'Permission'	=> 'public-read',
				'Key'			=> 'media_library/' . $strKey,
				'inputFile'		=> $strInputFile
			];

			$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );

			if( true == $boolIsUpload ) {
				if( true == is_file( $arrmixObjectConfiguration['inputFile'] ) ) {
					$boolIsSuccess = $objObjectStorageGateway->putObject( $arrmixObjectConfiguration )->isSuccessful();

					if( true == CONFIG_REMOVE_SOURCE_MEDIA && true == $boolIsRemoveSource && true == file_exists( $strInputFile ) ) {
						unlink( $strInputFile );
					}
					return $boolIsSuccess;
				}
			} else {
				$arrstrFilesList = $objObjectStorageGateway->listObjects( $arrmixObjectConfiguration );

				if( true == valArr( $arrstrFilesList ) && true == isset( $arrstrFilesList[0] ) ) {
					return $objObjectStorageGateway->deleteObject( $arrmixObjectConfiguration )->isSuccessful();
				}
			}
		} else {
			return true;
		}
	}

	public function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		return $this->getFullsizeUri();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3:
				return CONFIG_AWS_CLIENTS_BUCKET_NAME;

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return PATH_MOUNTS_FILES;

			default:
				return ( CMediaType::PHOTO == $this->getMediaTypeId() ) ? CONFIG_AWS_CLIENTS_BUCKET_NAME : PATH_MOUNTS_FILES;
		}
	}

	public function uploadObject( $objObjectStorageGateway, $strInputFile ) {

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );
			return false;
		}

		$arrmixPutGatewayRequest	= $this->createPutGatewayRequest( [ 'inputFile' => $strInputFile ] );

		if( false == is_null( $this->getFileName() ) ) {
			$objGatewayResponse = $objObjectStorageGateway->putObject( $arrmixPutGatewayRequest );

			if( true == $objGatewayResponse->hasErrors() ) {
				trigger_error( __( 'Failed to upload file' ), E_USER_WARNING );
				return false;
			}

			$this->setObjectStorageGatewayResponse( $objGatewayResponse );
			return true;
		}

		return false;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $strReferenceTag = NULL ) {
		$boolReturn = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( $boolReturn && false == $boolReturnSqlOnly ) {
			$boolReturn &= $this->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, NULL, $strReferenceTag );
		}

		return $boolReturn;
	}

}
?>