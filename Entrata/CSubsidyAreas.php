<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyAreas
 * Do not add any new functions to this class.
 */

class CSubsidyAreas extends CBaseSubsidyAreas {

	public static function fetchSubsidyAreas( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CSubsidyArea::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSubsidyArea( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CSubsidyArea::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSubsidyAreaIdByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						sa.id AS subsidy_area_id
					FROM
						subsidy_areas sa
						JOIN property_subsidy_details psd ON ( sa.hmfa_code = psd.hmfa_code )
					WHERE
						psd.cid = ' . ( int ) $intCid . '
						AND psd.property_id = ' . ( int ) $intPropertyId;

		return self::fetchColumn( $strSql, 'subsidy_area_id', $objDatabase );
	}

}
?>