<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceAbatements
 * Do not add any new functions to this class.
 */
class CUnitSpaceAbatements extends CBaseUnitSpaceAbatements {

	public static function fetchUnitSpaceAbatementsByUnitSpaceIdByPropertyIdsByCid( $intUnitSpaceId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT *
					FROM
						unit_space_abatements usa
					WHERE
						usa.cid = ' . ( int ) $intCid . '
						AND usa.property_id = ' . ( int ) $intPropertyId . '
						AND usa.unit_space_id = ' . ( int ) $intUnitSpaceId . ' ORDER BY usa.start_date';

		return self::fetchUnitSpaceAbatements( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceAbatementByUnitSpaceIdByIdsByPropertyIdByCid( $intUnitSpaceId, $intUnitSpaceAbatementId, $intPropertyId, $intCid, $objDatabase ) {

		if( true == is_null( $intUnitSpaceAbatementId ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						unit_space_abatements usa
					WHERE
						usa.cid = ' . ( int ) $intCid . '
						AND usa.property_id = ' . ( int ) $intPropertyId . '
						AND usa.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND usa.id = ' . ( int ) $intUnitSpaceAbatementId;

		return self::fetchUnitSpaceAbatement( $strSql, $objDatabase );
	}

	public static function fetchOverlappingUnitSpaceAbatementCountByUnitSpaceIdByPropertyIdByCid( $intId, $intUnitSpaceId, $strStartDate, $strEndDate, $intPropertyId, $intCid, $objDatabase ) {

		if( true == is_null( $intUnitSpaceId ) || true == is_null( $intPropertyId ) || true == is_null( $intCid ) ) return NULL;

		if( false == is_null( $strEndDate ) ) {
			$strSqlOrCondition = ' OR ( \'' . $strEndDate . '\' BETWEEN usa.start_date AND usa.end_date )';
		}

		if( false == is_null( $intId ) ) {
			$strSqlExcludeSelectedUnitSpaceAbatement = ' AND usa.id <> ' . ( int ) $intId;
		}

		$strSql = 'SELECT
					COUNT(id) as count
					FROM
						unit_space_abatements usa
					WHERE
						usa.cid = ' . ( int ) $intCid . '
						AND usa.property_id = ' . ( int ) $intPropertyId . '
						AND usa.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND ( ( \'' . $strStartDate . '\' BETWEEN usa.start_date AND usa.end_date )
								' . $strSqlOrCondition . '
								OR ( usa.start_date = \'' . $strStartDate . '\' )
								OR ( usa.end_date IS NULL AND usa.start_date <= \'' . $strStartDate . '\' ) )
						' . $strSqlExcludeSelectedUnitSpaceAbatement;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

}
?>