<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBuildingTypes
 * Do not add any new functions to this class.
 */

class CBuildingTypes extends CBaseBuildingTypes {

	public static function fetchBuildingTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CBuildingType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchBuildingType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CBuildingType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>