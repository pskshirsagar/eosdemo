<?php

class CSubsidyMessage extends CBaseSubsidyMessage {

	const DEFAULT_DAYS = 10;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyMessageTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyMessageSubTypeId() {
		$boolIsValid = true;
		if( false == valStr( $this->getSubsidyMessageSubTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_message_sub_type_id', __( 'Subsidy message sub type is required.' ) ) ] );
		}
		return $boolIsValid;
	}

	public function valSubsidyMessageStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyMessageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSender() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRecipients() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSentToImaxDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSentToTracsDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsArchived() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasBeenViewed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valSubsidyMessageSubTypeId();
				$boolIsValid &= $this->valRecipients();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>