<?php

class CAccountingExportMethodType extends CBaseAccountingExportMethodType {
//todo: Check if this FILE and API type is used
	const FILE                        = 1;
	const API                         = 2;
	const ACCOUNTING_EXPORT_MANUAL    = 1;
	const ACCOUNTING_EXPORT_SCHEDULED = 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMethodName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>