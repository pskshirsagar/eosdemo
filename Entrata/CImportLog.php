<?php

class CImportLog extends CBaseImportLog {
	protected $m_strProcessingTime;

	public function setProcessingTime( $strProcessingTime ) {
		$this->m_strProcessingTime = $strProcessingTime;
	}

	public function getProcessingTime() {
		return $this->m_strProcessingTime;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrmixValues['processing_time'] ) ) {
			$this->setProcessingTime( $arrmixValues['processing_time'] );
		}

		return;
	}

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Import log id is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Cid is required.' ) );
		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getImportTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'import_type_id', 'Import type id is required.' ) );
		}
		return $boolIsValid;
	}

	public function valImportDataTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getImportDataTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'import_data_type_id', 'Import data type id is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDocumentReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseAmazonFileStorage() {
		$boolIsValid = true;

		if( true == is_null( $this->getUseAmazonFileStorage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_amazon_file_storage', 'Use amazon file storage value is required.' ) );
		}
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSummary() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valImportTypeId();
				$boolIsValid &= $this->valImportDataTypeId();
				$boolIsValid &= $this->valUseAmazonFileStorage();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createImportLog( $intCid, $intPropertyId, $intImportTypeId, $intImportDataTypeId ) {
		$this->setCid( $intCid );
		$this->setPropertyId( $intPropertyId );
		$this->setImportTypeId( $intImportTypeId );
		$this->setImportDataTypeId( $intImportDataTypeId );
	}

	public function insertImportLog( $strImportDataMountsPath = NULL, $strImportLogContent = NULL, $intCompanyUserId, $objClientDatabase ) {

		if( false == parent::insert( $intCompanyUserId, $objClientDatabase ) ) {
			return false;
		}

		$strLocalFilePath		= CMigrationCommonLibrary::getMigrationDocumentPath( $this->getImportTypeId(), $this->getCid(), $this->getPropertyId() );
		$strLocalFilePath		.= date( 'Y/m/d/' );

		if( false == is_dir( $strLocalFilePath ) ) {
			CFileIo::recursiveMakeDir( $strLocalFilePath );
		}

		$strLocalFile = $strLocalFilePath . $this->getId() . '_' . $this->getImportDataTypeId() . ( ( NULL == $this->getFileName() ) ? ( ( CImportType::E2E == $this->getImportTypeId() ) ? '_import_log.txt' : '_import_log.csv' ) : '_' . $this->getFileName() );

		switch( $this->getImportTypeId() ) {
			case CImportType::E2E:
				$strImportLogContent	= json_encode( json_decode( $strImportLogContent ), JSON_PRETTY_PRINT );
				if( false === CFileIo::filePutContents( $strLocalFile, $strImportLogContent ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Could not write data to file for import log id : ' . $this->getId() ) );
					return false;
				}
				break;

			case CImportType::CSV:
			default:
				if( true == CFileIo::fileExists( $strImportDataMountsPath ) ) {
					if( false === CFileIo::copyFile( $strImportDataMountsPath, $strLocalFile ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Could not copy file for import log id : ' . $this->getId() ) );
						return false;
					}
				}
				break;
		}

		return true;
	}

}
?>