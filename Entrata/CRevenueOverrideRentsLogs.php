<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueOverrideRentsLogs
 * Do not add any new functions to this class.
 */

class CRevenueOverrideRentsLogs extends CBaseRevenueOverrideRentsLogs {

	const LEASE_TERM = 12;

	public static function fetchRevenueOverrideRentsLogsByPropertyIdByCidByUnitTypeIdByUnitSpaceIdsByEffectiveData( $intCid, $objDatabase, $intPropertyId, $intUnitTypeId, $arrintUnitSpaceIds, $strStartDate, $strEndDate, $boolReturnSqlOnly = false ) {

		$strSql = '
				SELECT
					*
				FROM
				(
					SELECT
					us.unit_number,
					us.property_name AS property_name,
			        ROUND(rorl.override_rent_amount) AS override_rent_amount,
			        rorl.override_rent_reason AS override_rent_reason,
			        ROUND( rorl.original_optimal_rent ) AS original_optimal_rent,
			        rorl.effective_date AS effective_date,
			        rank ( ) OVER ( PARTITION BY rorl.effective_date, us.unit_number ORDER BY rorl.move_in_date ASC, rorl.updated_on DESC ) AS rnk
					FROM revenue_override_rents_logs rorl
					JOIN unit_spaces us on us.id = rorl.unit_space_id AND us.property_id = rorl.property_id AND us.cid = rorl.cid
					WHERE
							rorl.cid = ' . ( int ) $intCid . '
							AND rorl.property_id =' . ( int ) $intPropertyId . '
							AND rorl.unit_type_id = ' . ( int ) $intUnitTypeId . '
							AND rorl.unit_space_id IN (' . implode( ',', $arrintUnitSpaceIds ) . ')
							AND rorl.effective_date BETWEEN \'' . $strStartDate . '\'::DATE AND \'' . $strEndDate . '\'::DATE
							AND rorl.lease_term_month = ' . self::LEASE_TERM . '
				) as r
				where r.rnk = 1';

		if( false == $boolReturnSqlOnly ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return $strSql;
		}
	}

}
?>