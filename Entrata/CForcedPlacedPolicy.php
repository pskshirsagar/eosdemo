<?php
use Psi\Eos\Entrata\CUnitSpaces;
use Psi\Eos\Admin\CContractProperties;
class CForcedPlacedPolicy extends CBaseForcedPlacedPolicy {

	protected $m_intLeaseStatusTypeId;

	protected $m_intInsurancePolicyTypeId;

	const VALIDATE_FORCED_PLACED_POSTPONE_DATE = 'VALIDATE_FORCED_PLACED_POSTPONE_DATE';

	public static $c_arrintAssetCampusHousingPropertyIds = [ 829513, 575742, 111334, 653376, 662849, 614503, 672230, 260613, 614504, 614506, 53020, 241830, 53029, 614505, 672231, 640850, 77450, 672232, 614507 ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentInsurancePolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTransactionRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTransactionExportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUninsuredOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReinsuredOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFirstNoticeSentOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnrolledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostponedOn() {
		$boolIsValid = true;

		if( '' == $this->getPolicyEffectiveDate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please select postpone date. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valNotes( $strNote ) {
		$boolIsValid = true;

		if( '' == $strNote ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please enter notes.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostponedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $strNote = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case self::VALIDATE_FORCED_PLACED_POSTPONE_DATE:
				$boolIsValid &= $this->valPostponedOn();
				$boolIsValid &= $this->valNotes( $strNote );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	/**
	 * Set Functions
	 *
	 */
	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrstrValues['lease_status_type_id'] ) )    $this->setLeaseStatusTypeId( $arrstrValues['lease_status_type_id'] );
		if( true == isset( $arrstrValues['insurance_policy_type_id'] ) )    $this->setInsurancePolicyTypeId( $arrstrValues['insurance_policy_type_id'] );
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->m_intInsurancePolicyTypeId = $intInsurancePolicyTypeId;
	}

	/**
	 * Other Functions
	 *
	 */

	public function createForcedPlacedPolicy( $arrmixInsuredDetail ) {
		$this->setCid( $arrmixInsuredDetail['cid'] );
		$this->setPropertyId( $arrmixInsuredDetail['property_id'] );
		$this->setCustomerId( $arrmixInsuredDetail['customer_id'] );
		$this->setLeaseId( $arrmixInsuredDetail['lease_id'] );

		return $this;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

	public static function updateForcedPlacedPolicy( $objForcedPlacedPolicy, $intLeaseId, $intCid, $intCompanyUserId, $objDatabase, $objInsuranceDatabase, $intCustomerId = NULL, $boolOptOutPolicy = false, $boolIsBlanketPolicy = false, $boolIsActiveNonRenewedMasterPolicy = false, $arrobjMoveOutDates = [] ) {

		$objResidentInsurancePolicy = NULL;
		$objMasterPolicy	= NULL;

		$boolIsDisableFuturePolicy = false;

		$objForcedPlacedPolicy->setIsDisabled( true );

		// Set the end date as current date
		$strPolicyEndDate = date( 'm/d/Y' );
		if( true == valArr( $arrobjMoveOutDates ) && true == isset( $arrobjMoveOutDates[$intLeaseId] ) ) {

			// If move out date is of past then will keep the policy end date as current date
			if( strtotime( date( 'm/d/Y', strtotime( $strPolicyEndDate ) ) ) < strtotime( date( 'm/d/Y', strtotime( $arrobjMoveOutDates[$intLeaseId]->getMoveOutDate() ) ) ) ) {
				$strPolicyEndDate = $arrobjMoveOutDates[$intLeaseId]->getMoveOutDate();
			}
		}

		if( true == $boolIsActiveNonRenewedMasterPolicy ) {
			$strPolicyEndDate = $objForcedPlacedPolicy->getPolicyEndDate();
		}

		if( false == is_null( $intCustomerId ) ) {

			$objResidentInsurancePolicy = CResidentInsurancePolicies::fetchResidentInsurancePolicyByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase );

			// We will reverse/delete ar charge if current date will be less than policy effective date or we are doing manually ( using opt out button )
			if( strtotime( date( 'm/d/Y', strtotime( $strPolicyEndDate ) ) ) < strtotime( date( 'm/d/Y', strtotime( $objForcedPlacedPolicy->getPolicyEffectiveDate() ) ) ) || true == $boolOptOutPolicy ) {

				$boolIsDisableFuturePolicy = true;

				$objForcedPlacedPolicy->setPolicyEndDate( $objForcedPlacedPolicy->getPolicyEffectiveDate() );

				if( ( true == $boolOptOutPolicy || ( true == valObj( $objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) && strtotime( date( 'm/d/Y', strtotime( $objResidentInsurancePolicy->getEffectiveDate() ) ) ) == strtotime( date( 'm/d/Y', strtotime( $objForcedPlacedPolicy->getPolicyEffectiveDate() ) ) ) ) ) ) {
					$strPolicyEndDate = date( 'm/d/Y', strtotime( $objForcedPlacedPolicy->getPolicyEffectiveDate() ) );
				}

				// Delete charge
				if( false == is_null( $objForcedPlacedPolicy->getArTransactionId() ) ) {
					$arrmixData['cid']               = $intCid;
					$arrmixData['ar_transaction_id'] = $objForcedPlacedPolicy->getArTransactionId();
					$arrmixData['company_user_id']   = $intCompanyUserId;

					if( false == CForcedPlacedPolicy::deleteCharge( $arrmixData, $objDatabase ) ) {
						return false;
					}
				}
			}

			if( true == valObj( $objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) ) {
				$objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( CInsurancePolicyStatusType::CANCELLED );
				$objResidentInsurancePolicy->setEndDate( $strPolicyEndDate );
			}

			$objMasterPolicy = \Psi\Eos\Insurance\CMasterPolicies::createService()->fetchNonCancelledMasterPolicyByLeaseIdByCid( $intLeaseId, $intCid, $objInsuranceDatabase );

			if( true == valObj( $objMasterPolicy, 'CMasterPolicy' ) ) {

				if( true == $boolOptOutPolicy || ( strtotime( date( 'm/d/Y' ) ) <= strtotime( date( 'm/d/Y', strtotime( $objMasterPolicy->getEffectiveDate() ) ) ) ) ) {
					$objMasterPolicy->setIsDisabled( true );
				}

				$objMasterPolicy->setPolicyStatusTypeId( CInsurancePolicyStatusType::CANCELLED );
				$objMasterPolicy->setEndDate( $strPolicyEndDate );

				if( true == $boolIsDisableFuturePolicy ) {
					// Disable record in mpc
					$objMasterPolicyCoverage = \Psi\Eos\Insurance\CMasterPolicyCoverages::createService()->fetchMasterPolicyByStartDateByMasterPolicyId( $objForcedPlacedPolicy->getPolicyEffectiveDate(), $objMasterPolicy->getId(), $objInsuranceDatabase );

					if( true == valObj( $objMasterPolicyCoverage, 'CMasterPolicyCoverage' ) ) {
						$objMasterPolicyCoverage->setIsDisabled( true );

						if( false == $objMasterPolicyCoverage->update( $intCompanyUserId, $objInsuranceDatabase ) ) {
							return false;
						}
					}
				}
			}
		}

		if( false == $objForcedPlacedPolicy->update( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		if( true == valObj( $objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) ) {
			if( false == $objResidentInsurancePolicy->update( $intCompanyUserId, $objDatabase ) ) {
				return false;
			}
		}

		if( true == valObj( $objMasterPolicy, 'CMasterPolicy' ) ) {
			if( false == $objMasterPolicy->update( $intCompanyUserId, $objInsuranceDatabase ) ) {
				return false;
			}

			// Insert record in master policy coverages For Credit Export
			if( false == CForcedPlacedPolicy::insertCreditExportRecord( $strPolicyEndDate, $objMasterPolicy->getId(), $intCompanyUserId, $objMasterPolicy->getEffectiveDate(), $objMasterPolicy->getEndDate(), $objInsuranceDatabase, $objMasterPolicy->getIsDisabled() ) ) {
				return false;
			}
		}

		// For first notice cancellation email should not get send
		if( false == $boolIsBlanketPolicy && true == valObj( $objForcedPlacedPolicy, 'CForcedPlacedPolicy' ) && false == is_null( $objForcedPlacedPolicy->getResidentInsurancePolicyId() ) ) {
			$objInsuranceEmailController = new CInsuranceEmailController();
			$objInsuranceEmailController->sendForcedPlacedPolicyCancelEmail( $objForcedPlacedPolicy, $objDatabase );

		}

		return true;
	}

	public function enrollMasterPolicy( $arrmixUninsuredResident, $objLease, $objController, $intUserId, $arrobjDatabases ) {

		$objDatabase 			= $arrobjDatabases['client_database'];
		$objInsuranceDatabase 	= $arrobjDatabases['insurance_database'];
		$objEmailDatabase 		= $arrobjDatabases['email_database'];
		$objPaymentDatabase 	= $arrobjDatabases['payment_database'];

		$arrstrPreferenceKeys = [ 'FORCE_PLACED_FEE', 'POLICY_AR_CHARGE_CODE', 'PRORATE_BY_DAY', 'REQUIRE_EACH_INDIVIDUAL_RESIDENT_HAVE_ACTIVE_COVERAGE' ];

		$objMasterPolicyLibrary = new CMasterPolicyLibrary();
		$arrobjPropertyPreferences = $objMasterPolicyLibrary->getPreferenceKeysData( $arrstrPreferenceKeys, $objLease->getPropertyId(), $arrmixUninsuredResident['cid'], $objDatabase );

		if( false == valArr( $arrobjPropertyPreferences ) ) {
			$objController->addSessionErrorMsg( __( 'Forced Placed property setting not found for property : {%d, 0}', [ $objLease->getPropertyId() ] ) );
			return false;
		}

		// If Lease is insured then we will not enroll master policy
		if( true == $objMasterPolicyLibrary->checkIsLeaseInsured( $arrobjPropertyPreferences, $objLease->getId(), $objLease->getPropertyId(), $arrmixUninsuredResident['cid'], $objDatabase ) ) {
			return false;
		}

		$strPolicyEffectiveDate = ( false == is_null( $objLease->getMoveInDate() ) ) ? $objLease->getMoveInDate() : $objLease->getLeaseStartDate();

		if( strtotime( date( 'm/d/Y' ) ) > strtotime( $strPolicyEffectiveDate ) ) {
			$strPolicyEffectiveDate = date( 'Y-m-d' );
		}

		$boolIsProrateByDay = false;
		foreach( $arrobjPropertyPreferences as $objPropertyPreference ) {

			if( CPropertyPreference::FORCE_PLACED_FEE == $objPropertyPreference->getKey() ) {
				$fltForcedPlacedFee = $objPropertyPreference->getValue();
			}

			if( CPropertyPreference::POLICY_AR_CHARGE_CODE == $objPropertyPreference->getKey() ) {
				$intArChargeCodeId = $objPropertyPreference->getValue();
			}

			if( CPropertyPreference::PRORATE_BY_DAY == $objPropertyPreference->getKey() ) {
				$boolIsProrateByDay = true;
			}
		}

		$arrmixInsurancePropertyLimits = ( array ) \Psi\Eos\Insurance\CPropertyInsuranceLimits::createService()->fetchPropertyInsuranceLimitsByInsuranceCarrierIdByPropertyIdByCid( CInsuranceCarrier::SSIS, $objLease->getPropertyId(), $arrmixUninsuredResident['cid'], $objInsuranceDatabase );

		if( false == valArr( $arrmixInsurancePropertyLimits ) ) {
			$objController->addSessionErrorMsg( __( 'No property rate found for property : {%d, 0}', [ $objLease->getPropertyId() ] ) );
			return false;
		}

		$arrobjArCodes = ( array ) \Psi\Eos\Entrata\CArCodes::createService()->fetchRequiredArCodesByIdsByCids( [ $intArChargeCodeId ], [ $arrmixUninsuredResident['cid'] ], $objDatabase );
		$arrobjArCodes = rekeyObjects( 'PropertyId', $arrobjArCodes );

		$arrmixPolicyPropertyInsuranceLimit = [];

		foreach( $arrmixInsurancePropertyLimits as $arrmixPropertyInsuranceLimit ) {
			$arrmixPolicyPropertyInsuranceLimit[$arrmixPropertyInsuranceLimit['insurance_limit_type_id']] = $arrmixPropertyInsuranceLimit;
		}

		$intLiabilityAmount = $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::LIABILITY]['limit_amount'];

		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyContactInfoByPropertyIdByCid( $objLease->getPropertyId(), $arrmixUninsuredResident['cid'], $objDatabase );

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$arrmixPropertyContactInfo['property_name'] = $objProperty->getPropertyName();
			$arrmixPropertyContactInfo['property_email_address'] = $objProperty->getPrimaryEmailAddress();
			$arrmixPropertyContactInfo['property_phone_number'] = $objProperty->getOfficePhoneNumber();
		}

		$arrmixUninsuredResident['notice_number'] 			= 3;
		$arrmixUninsuredResident['forced_placed_fee'] 		= $fltForcedPlacedFee;
		$arrmixUninsuredResident['liability_amount'] 		= $intLiabilityAmount;
		$arrmixUninsuredResident['property_contact_info'] 	= $arrmixPropertyContactInfo;

		// Insert record in forced placed policy table
		$objForcedPlacedPolicy = new CForcedPlacedPolicy();
		$objForcedPlacedPolicy = $objForcedPlacedPolicy->createForcedPlacedPolicy( $arrmixUninsuredResident );
		$objForcedPlacedPolicy->setPolicyEffectiveDate( $strPolicyEffectiveDate );
		$objForcedPlacedPolicy->setEnrolledOn( $strPolicyEffectiveDate );
		$objForcedPlacedPolicy->setPolicyEndDate( date( 'Y-m-t H:i:s', strtotime( $strPolicyEffectiveDate ) ) );

		if( false == $objForcedPlacedPolicy->insert( $intUserId, $objDatabase ) ) {
			$objController->addSessionErrorMsg( __( 'Failed to insert in Forced Placed Policy.' ) );
			$objInsuranceDatabase->rollback();
			$objDatabase->rollback();
			return false;
		}

		$arrmixInsuranceCarrierClientBaseRates = \Psi\Eos\Insurance\CInsuranceCustomPolicyClientDetails::createService()->fetchInsuranceCustomPolicyClientDetailsByCids( [ $arrmixUninsuredResident['cid'] ], $objInsuranceDatabase );
		$arrmixInsuranceCarrierClientBaseRates = rekeyArray( 'cid', $arrmixInsuranceCarrierClientBaseRates );

		$arrmixInsuranceClientBaseRate  		= $arrmixInsuranceCarrierClientBaseRates[$arrmixUninsuredResident['cid']];

		if( false == valArr( $arrmixInsuranceClientBaseRate ) ) {
			$objController->addSessionErrorMsg( __( 'Failed to load rates for the property.' ) );
			$objInsuranceDatabase->rollback();
			$objDatabase->rollback();
			return false;
		}

		$intClientNumber = $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::LIABILITY]['client_number'];

		if( CClient::ID_ASSET_CAMPUS_HOUSING == $arrmixUninsuredResident['cid'] && true == in_array( $arrmixUninsuredResident['property_id'], self::$c_arrintAssetCampusHousingPropertyIds ) ) {
			$arrmixInsuranceClientBaseRate = CForcedPlacedPolicy::getChangedClientBaseRate();
			$arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::PERSONAL]['limit_amount'] = 5000;
			$arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::PERSONAL]['personal_insurance_carrier_limit_id'] = 11;
			$intClientNumber = $arrmixInsuranceClientBaseRate['client_number'];
		}

		$strPrefix = 'RLO';

		if( true == array_key_exists( CInsuranceLimitType::PERSONAL, $arrmixPolicyPropertyInsuranceLimit ) && 0 != $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::PERSONAL]['limit_amount'] ) {
			$strPrefix = 'RLC';
		}

		$intClientNumber = \Psi\CStringService::singleton()->substr( $intClientNumber, 0, 4 );

		$objMasterPolicy       = CMasterPolicy::createMasterPolicy( $arrmixUninsuredResident );
		$intNextMasterPolicyId = $objMasterPolicy->fetchNextId( $objInsuranceDatabase );

		$strPolicyNumber = $strPrefix . $intClientNumber . sprintf( '%06d', $intNextMasterPolicyId );

		// Insert record in resident insurance policy
		$objResidentInsurancePolicy = new CResidentInsurancePolicy();
		$objResidentInsurancePolicy = $objResidentInsurancePolicy->createResidentInsurancePolicy( $arrmixUninsuredResident );

		$intInsurancePolicyStatusTypeId = ( strtotime( date( 'Y-m-d' ) ) >= strtotime( $strPolicyEffectiveDate ) ) ? CInsurancePolicyStatusType::ACTIVE : CInsurancePolicyStatusType::ENROLLED;

		$objResidentInsurancePolicy->setPolicyNumber( $strPolicyNumber );
		$objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( $intInsurancePolicyStatusTypeId );
		$objResidentInsurancePolicy->setInsuranceCarrierName( CInsuranceCarrier::$c_strInsuranceCarrierSSIS );
		$objResidentInsurancePolicy->setEffectiveDate( $strPolicyEffectiveDate );
		$objResidentInsurancePolicy->setConfirmedOn( $strPolicyEffectiveDate );
		$objResidentInsurancePolicy->setLeadSourceTypeId( $arrmixUninsuredResident['lead_source_type_id'] );
		$objResidentInsurancePolicy->setPrimaryInsuredName( $arrmixUninsuredResident['name_first'] . ' ' . $arrmixUninsuredResident['name_last'] );

		$objResidentInsurancePolicy->setLiabilityLimit( $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::LIABILITY]['limit_amount'] );

		if( true == array_key_exists( CInsuranceLimitType::PERSONAL, $arrmixPolicyPropertyInsuranceLimit ) ) {
			$objResidentInsurancePolicy->setPersonalLimit( $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::PERSONAL]['limit_amount'] );
		}

		if( true == array_key_exists( CInsuranceLimitType::DEDUCTIBLE, $arrmixPolicyPropertyInsuranceLimit ) ) {
			$objResidentInsurancePolicy->setDeductible( $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::DEDUCTIBLE]['limit_amount'] );
		}

		$objResidentInsurancePolicy->setInsurancePolicyTypeId( $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::LIABILITY]['insurance_policy_type_id'] );

		if( false == $objResidentInsurancePolicy->insert( $intUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			$objInsuranceDatabase->rollback();
			$objController->addSessionErrorMsg( __( 'Failed to insert in resident insurance policy.' ) );
			return false;
		}

		$objForcedPlacedPolicy->setResidentInsurancePolicyId( $objResidentInsurancePolicy->getId() );

		$arrobjInsuranceCarrierProperties = \Psi\Eos\Insurance\CInsuranceCarrierProperties::createService()->fetchInsuranceCarrierPropertiesByPropertyId( $objLease->getPropertyId(), $objInsuranceDatabase );
		$arrobjInsuranceCarrierProperties = rekeyObjects( 'PropertyId', $arrobjInsuranceCarrierProperties );

		$objInsuranceCarrierProperty = $arrobjInsuranceCarrierProperties[$objLease->getPropertyId()];

		if( false == valObj( $objInsuranceCarrierProperty, 'CInsuranceCarrierProperty' ) ) {
			$objController->addSessionErrorMsg( __( 'No insurance set up for property id {%d, 0}', [ $objLease->getPropertyId() ] ) );
			$objInsuranceDatabase->rollback();
			$objDatabase->rollback();
			return false;
		}

		if( true == is_null( $arrmixUninsuredResident['unit_number'] ) && false == is_null( $arrmixUninsuredResident['unit_space_id'] ) && false == is_null( $arrmixUninsuredResident['property_unit_id'] ) ) {

			$arrmixUnitDetails = ( array ) CUnitSpaces::createService()->fetchUnitNumberByIdByPropertyUnitIdByCid( $arrmixUninsuredResident['unit_space_id'], $arrmixUninsuredResident['property_unit_id'], $arrmixUninsuredResident['cid'], $objDatabase );
			$arrmixUnitDetails = rekeyArray( 'unit_space_id', $arrmixUnitDetails );

			$arrmixUnitSpaceDetails = $arrmixUnitDetails[$arrmixUninsuredResident['unit_space_id']];

			$arrmixUninsuredResident['unit_number'] = ( false == is_null( $arrmixUnitSpaceDetails['unit_space_number'] ) ) ? $arrmixUnitSpaceDetails['unit_space_number'] : $arrmixUnitSpaceDetails['property_unit_number'];
		}

		// Checking for Address
		$arrmixAddressDetails = \Psi\Eos\Entrata\CUnitAddresses::createService()->fetchAddressDetailsByPropertyIdByLeaseIdByCid( $arrmixUninsuredResident['property_id'], $arrmixUninsuredResident['lease_id'], $arrmixUninsuredResident['cid'], $objDatabase );

		if( true == valArr( $arrmixAddressDetails ) ) {
			$arrmixUninsuredResident['street_line1'] = $arrmixAddressDetails[0]['street_line1'];
			$arrmixUninsuredResident['city']         = $arrmixAddressDetails[0]['city'];
			$arrmixUninsuredResident['state_code']   = $arrmixAddressDetails[0]['state_code'];
			$arrmixUninsuredResident['postal_code']  = $arrmixAddressDetails[0]['postal_code'];
		}

		// Insert record in insurance custom policy, insurance custome policy export
		$objMasterPolicy->setUnitSpaceId( $arrmixUninsuredResident['unit_space_id'] );
		$objMasterPolicy->setPolicyNumber( $strPolicyNumber );
		$objMasterPolicy->setLiabilityCarrierLimitId( $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::LIABILITY]['default_liability_insurance_carrier_limit_id'] );
		$objMasterPolicy->setNameFirst( $arrmixUninsuredResident['name_first'] );
		$objMasterPolicy->setNameLast( $arrmixUninsuredResident['name_last'] );
		$objMasterPolicy->setStreetAddress( $arrmixUninsuredResident['street_line1'] );
		$objMasterPolicy->setCity( $arrmixUninsuredResident['city'] );
		$objMasterPolicy->setStateCode( $arrmixUninsuredResident['state_code'] );
		$objMasterPolicy->setPostalCode( $arrmixUninsuredResident['postal_code'] );
		$objMasterPolicy->setEffectiveDate( $strPolicyEffectiveDate );
		$objMasterPolicy->setPolicyStatusTypeId( CInsurancePolicyStatusType::ACTIVE );
		$objMasterPolicy->setUnitNumber( $arrmixUninsuredResident['unit_number'] );
		$objMasterPolicy->setInsurancePolicyTypeId( $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::LIABILITY]['insurance_policy_type_id'] );

		if( true == array_key_exists( CInsuranceLimitType::PERSONAL, $arrmixPolicyPropertyInsuranceLimit ) ) {
			$objMasterPolicy->setPersonalCarrierLimitId( $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::PERSONAL]['personal_insurance_carrier_limit_id'] );
		}

		if( true == array_key_exists( CInsuranceLimitType::DEDUCTIBLE, $arrmixPolicyPropertyInsuranceLimit ) ) {
			$objMasterPolicy->setDeductibleCarrierLimitId( $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::DEDUCTIBLE]['deductible_insurance_carrier_limit_id'] );
		}

		if( false == $objMasterPolicy->insert( $intUserId, $objInsuranceDatabase ) ) {
			$objInsuranceDatabase->rollback();
			$objDatabase->rollback();
			$objController->addSessionErrorMsg( __( 'Failed to insert in insurance custom Policy.' ) );
			return false;
		}

		$fltAdminFee    = $arrmixPolicyPropertyInsuranceLimit[CInsuranceLimitType::LIABILITY]['invoice_premium'];

		// Proration
		$arrmixData['policy_effective_date'] = $strPolicyEffectiveDate;
		$arrmixData['force_place_fee'] = $fltForcedPlacedFee;
		$arrmixData['client_base_rates_details'] = $arrmixInsuranceClientBaseRate;
		$arrmixData['is_blanket'] = false;
		$arrmixData['is_prorate_by_day'] = $boolIsProrateByDay;

		$arrmixPremium = self::proratePremium( $arrmixData );

		$arrmixUninsuredResident['total_premium_amount'] = $arrmixPremium['total_premium_amount'];
		$fltProviderPremiumAmount = $arrmixPremium['provider_premium_amount'];
		$fltBaseRate = $arrmixPremium['base_rate'];
		$fltPolicyFee = $arrmixPremium['policy_fee'];
		$fltBaseTax = $arrmixPremium['basic_tax'];
		$fltStampingTax = $arrmixPremium['stamping_tax'];
		$fltOtherTax = $arrmixPremium['other_tax'];

		// Insert record in insurance custom policy export
		$objMasterPolicyCoverage = new CMasterPolicyCoverage();
		$objMasterPolicyCoverage->setMasterPolicyId( $objMasterPolicy->getId() );
		$objMasterPolicyCoverage->setStartDate( $strPolicyEffectiveDate );
		$objMasterPolicyCoverage->setEndDate( date( 'Y-m-t', strtotime( $strPolicyEffectiveDate ) ) );

		$objMasterPolicyCoverage->setProviderPremiumAmount( $fltProviderPremiumAmount );
		$objMasterPolicyCoverage->setTotalPremiumAmount( $arrmixUninsuredResident['total_premium_amount'] );
		$objMasterPolicyCoverage->setBaseRate( $fltBaseRate );
		$objMasterPolicyCoverage->setPolicyFee( $fltPolicyFee );
		$objMasterPolicyCoverage->setBaseTax( $fltBaseTax );
		$objMasterPolicyCoverage->setStampingTax( $fltStampingTax );
		$objMasterPolicyCoverage->setOtherTax( $fltOtherTax );
		$objMasterPolicyCoverage->setAdminFee( $fltAdminFee );
		$objMasterPolicyCoverage->setMasterPolicyRecordTypeId( CMasterPolicyRecordType::MIDMONTH );

		if( false == $objMasterPolicyCoverage->insert( $intUserId, $objInsuranceDatabase ) ) {
			$objInsuranceDatabase->rollback();
			$objDatabase->rollback();
			$objController->addSessionErrorMsg( __( 'Failed to insert in insurance custom policy export.' ) );
			return false;
		}

		$objResidentInsurancePolicy->setCustomerId( $arrmixUninsuredResident['customer_id'] );
		$objResidentInsurancePolicy->setLeaseId( $arrmixUninsuredResident['lease_id'] );

		// For Integration call
		$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $arrmixUninsuredResident['customer_id'], $arrmixUninsuredResident['cid'], $objDatabase );
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $arrmixUninsuredResident['property_id'], $arrmixUninsuredResident['cid'], $objDatabase );

		if( true == valStr( $objCustomer->getRemotePrimaryKey() ) ) {
			$objCustomer->setInsurancePolicies( [ $objResidentInsurancePolicy ] );
			$objCustomer->export( $objProperty, $objDatabase, $objLease );
		}

		if( false == $objResidentInsurancePolicy->update( $intUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			$objInsuranceDatabase->rollback();
			$objController->addSessionErrorMsg( __( 'Failed to update in resident insurance policy.' ) );
			return false;
		}

		// Inser reocrd in insurance policy customers
		$objInsurancePolicyCustomer = new CInsurancePolicyCustomer();
		$objInsurancePolicyCustomer->setResidentInsurancePolicyId( $objResidentInsurancePolicy->getId() );
		$objInsurancePolicyCustomer->setCid( $arrmixUninsuredResident['cid'] );
		$objInsurancePolicyCustomer->setPropertyId( $objLease->getPropertyId() );
		$objInsurancePolicyCustomer->setLeaseId( $objLease->getId() );
		$objInsurancePolicyCustomer->setCustomerId( $arrmixUninsuredResident['customer_id'] );

		if( false == $objInsurancePolicyCustomer->insert( $intUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			$objInsuranceDatabase->rollback();
			$objController->addSessionErrorMsg( __( 'Failed to insert in insurance policy customer.' ) );
			return false;
		}

		if( 0 != $arrmixUninsuredResident['total_premium_amount'] && true === in_array( $objLease->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) ) {

			$objChargeArTransaction = $objLease->createArTransaction( $arrobjArCodes[$objLease->getPropertyId()]->getId(), $objDatabase );
			$objIntegrationDatabase = \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchIntegrationDatabaseByPropertyIdByCid( $objLease->getPropertyId(), $objLease->getCid(), $objDatabase );

			$boolIsTemporary = ( true === valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::YARDI == $objIntegrationDatabase->getIntegrationClientTypeId() );

			$objChargeArTransaction->setMemo( 'Forced placed insurance premium: Coverage - ' . date( 'm/d/Y' ) );
			$objChargeArTransaction->setArCodeTypeId( $arrobjArCodes[$objLease->getPropertyId()]->getArCodeTypeId() );
			$objChargeArTransaction->setTransactionAmount( $arrmixUninsuredResident['total_premium_amount'] );
			$objChargeArTransaction->setArTriggerId( CArTrigger::ONE_TIME );
			$objChargeArTransaction->setArOriginId( CArOrigin::BASE );
			$objChargeArTransaction->setCustomerId( $arrmixUninsuredResident['customer_id'] );
			$objChargeArTransaction->setIsTemporary( $boolIsTemporary );
			$objChargeArTransaction->setPostDate( $objForcedPlacedPolicy->getPolicyEffectiveDate() );

			if( false === $objChargeArTransaction->postCharge( $intUserId, $objDatabase ) ) {
				$objInsuranceDatabase->rollback();
				$objDatabase->rollback();
				$objController->addSessionErrorMsg( 'Failed to post the charges.' );
				return false;
			}

			$objForcedPlacedPolicy->setArTransactionId( $objChargeArTransaction->getId() );
		}

		if( false == $objForcedPlacedPolicy->update( $intUserId, $objDatabase ) ) {
			$objController->addSessionErrorMsg( __( 'Failed to update in Forced Placed Policy.' ) );
			$objInsuranceDatabase->rollback();
			$objDatabase->rollback();
			return false;
		}

		$intSystemEmailId = NULL;

		if( true == is_null( $arrmixUninsuredResident['email_address'] ) ) {
			$objController->addSessionErrorMsg( __( 'Email address not found: Unable to send enrollment email.' ) );
		} else {
			$objInsuranceEmailController = new CInsuranceEmailController();

			list( $boolIsSuccess, $intSystemEmailId ) = $objInsuranceEmailController->sendForcedPlacedNotice( $arrmixUninsuredResident, NULL, $objEmailDatabase );
			if( false == $boolIsSuccess ) {
				$objController->addSessionErrorMsg( __( 'Failed to send enrollment notice.' ) );
			}
		}

		$strEventHandle = 'Opt in master policy';

		if( true === in_array( $objLease->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) ) {
			$strEventHandle = 'Manually enrolled in master policy';
		}

		$strNote		= 'Master policy created and enrollment notice sent';

		// Create event log
		if( false == CResidentInsuranceLibrary::logEvent( $objForcedPlacedPolicy, $strNote, $strEventHandle, $intUserId, $objController->m_objCompanyUser, $objDatabase, CEventType::EMAIL_OUTGOING, $intSystemEmailId ) ) {
			$objController->addSessionErrorMsg( __( 'Failed to log event.' ) );
			$objInsuranceDatabase->rollback();
			$objDatabase->rollback();
			return false;
		}

		$objInsuranceDatabase->commit();
		$objDatabase->commit();

		if( false == $objLease->exportInsuranceTransactions( $intUserId, $objDatabase, $objPaymentDatabase, false ) ) {
			$objController->addSessionErrorMsg( __( 'Failed to Integrate Transactions.' ) );
			return false;
		}

		return true;

	}

	public function optOutMasterPolicy( $arrmixOptOutData, $objDatabase, $objController, $objInsuranceDatabase ) {

		$intResidentInsurancePolicyId 	= $arrmixOptOutData['resident_insruance_policy_id'];
		$intCustomerId 					= $arrmixOptOutData['customer_id'];
		$intCid 						= $arrmixOptOutData['cid'];
		$objLease 						= $arrmixOptOutData['lease'];

		if( true == is_null( $intResidentInsurancePolicyId ) ) {
			$objController->addSessionErrorMsg( __( 'No resident insurance policy id found.' ) );
			return false;
		}

		$objForcedPlacedPolicy 	= CForcedPlacedPolicies::fetchActiveForcedPlacedPolicyByResidentInsurancePolicyIdByCid( $intResidentInsurancePolicyId, $intCid, $objDatabase );

		if( false == valObj( $objForcedPlacedPolicy, 'CForcedPlacedPolicy' ) ) {
			$objController->addSessionErrorMsg( __( 'Failed to load Forced Placed Policies.' ) );
			return false;
		}

		$objDatabase->begin();
		$objInsuranceDatabase->begin();

		if( false == CForcedPlacedPolicy::updateForcedPlacedPolicy( $objForcedPlacedPolicy, $objLease->getId(), $intCid, $objController->m_objCompanyUser->getId(), $objDatabase, $objInsuranceDatabase, $intCustomerId, true ) ) {
			$objDatabase->rollback();
			$objInsuranceDatabase->rollback();
			$objController->addSessionErrorMsg( __( 'Failed to opt out Forced Placed Policies.' ) );
			return false;
		}

		$objDatabase->commit();
		$objInsuranceDatabase->commit();

		$objController->addSessionSuccessMsg( __( 'Master Policy opted out' ) );
		return true;
	}

	public function deleteCharge( $arrmixData, $objDatabase ) {

		// Deleting the charge
		$objArTransaction = \Psi\Eos\Entrata\CArTransactions::createService()->fetchNonDeletedArTransactionByIdByCid( $arrmixData['ar_transaction_id'], $arrmixData['cid'], $objDatabase );

		// If we don't find transaction detail then we need to cancel the FP policy, so returning "true" as a response
		if( true == valObj( $objArTransaction, 'CArTransaction' ) ) {
			$objArTransaction->setOverridePostMonthPermission( true );

			if( false == $objArTransaction->delete( $arrmixData['company_user_id'], $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public static function disableForcedPlacedFirstNotice( $intInsurancePolicyId, $intCid, $intUserId, $objClientDatabase ) {

		$objResidentInsurancePolicy = CResidentInsurancePolicies::fetchResidentInsurancePolicyDetailsByCidByInsurancePolicyId( $intInsurancePolicyId, $intCid, $objClientDatabase );

		if( true == valObj( $objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) && false == is_null( $objResidentInsurancePolicy->getLeaseId() ) ) {

			$objForcedPlacedPolicy	= CForcedPlacedPolicies::fetchForcedPlacedPolicyByLeaseIdByCid( $objResidentInsurancePolicy->getLeaseId(), $intCid, $objClientDatabase );

			if( true == valObj( $objForcedPlacedPolicy, 'CForcedPlacedPolicy' ) ) {
				$objForcedPlacedPolicy->setIsDisabled( true );

				if( false == $objForcedPlacedPolicy->update( $intUserId, $objClientDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public static function cancelForcedPlacedPolicy( $objResidentInsurancePolicy, $objForcedPlacedPolicy, $objMasterPolicy, $strForcedPlacedPolicyEndDate, $intUserId, $objClientDatabase, $objInsuranceDatabase ) {

		if( false == valObj( $objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) || false == valObj( $objForcedPlacedPolicy, 'CForcedPlacedPolicy' ) || false == valObj( $objMasterPolicy, 'CMasterPolicy' ) ) {
			return false;
		}
		// At the time of policy renewal, if we do not find required property settings, we will set end date as month end date on forced placed policy record
		$objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( CInsurancePolicyStatusType::CANCELLED );
		$objResidentInsurancePolicy->setEndDate( date( 'm/d/Y', strtotime( $strForcedPlacedPolicyEndDate ) ) );

		if( false == $objResidentInsurancePolicy->update( $intUserId, $objClientDatabase ) ) {
			return false;
		}

		$objForcedPlacedPolicy->setIsDisabled( true );

		if( false == $objForcedPlacedPolicy->update( $intUserId, $objClientDatabase ) ) {
			return false;
		}

		$objMasterPolicy->setPolicyStatusTypeId( CInsurancePolicyStatusType::CANCELLED );
		$objMasterPolicy->setEndDate( date( 'm/d/Y', strtotime( $strForcedPlacedPolicyEndDate ) ) );

		if( false == $objMasterPolicy->update( $intUserId, $objInsuranceDatabase ) ) {
			return false;
		}

		// Insert record in insurance custom policy export For Credit Export
		if( true == valObj( $objMasterPolicy, 'CMasterPolicy' ) ) {
			if( false == CForcedPlacedPolicy::insertCreditExportRecord( $strForcedPlacedPolicyEndDate, $objMasterPolicy->getId(), $intUserId, $objMasterPolicy->getEffectiveDate(), $objMasterPolicy->getEndDate(), $objInsuranceDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public static function insertCreditExportRecord( $strPolicyEndDate, $intMasterPolicyId, $intCompanyUserId, $strMasterPolicyEffectiveDate, $strMasterPolicyEndDate, $objInsuranceDatabase, $boolIsDisableMasterPolicy = false ) {

		// Insert record in insurance custom policy export For Credit Export
		$objMasterPolicyCoverage = \Psi\Eos\Insurance\CMasterPolicyCoverages::createService()->fetchMasterPolicyByStartDateByEndDateByExportTypeIdByMasterPolicyId( date( 'm-01-Y', strtotime( $strPolicyEndDate ) ), date( 'm-t-Y', strtotime( $strPolicyEndDate ) ), $intMasterPolicyId, CInsuranceExportType::EXPORT, $objInsuranceDatabase );

		if( true == valObj( $objMasterPolicyCoverage, 'CMasterPolicyCoverage' ) ) {

			$intDaysInMonth = cal_days_in_month( CAL_GREGORIAN, date( 'm', strtotime( $strPolicyEndDate ) ), date( 'y', strtotime( $strPolicyEndDate ) ) );

			// For days for which we need to add reversal ammount (for -ve value)
			$intNoOfPolicyDaysToReturn = ( date( 'd', strtotime( $strPolicyEndDate ) ) - $intDaysInMonth ) - 1;

			// To calculate no. of days policy used
			$strExportStartDate = new DateTime( $objMasterPolicyCoverage->getStartDate() );
			$strExportEndDate   = new DateTime( $objMasterPolicyCoverage->getEndDate() );
			$strDateDiff        = $strExportStartDate->diff( $strExportEndDate );
			$intNoOfPolicyDays  = $strDateDiff->days + 1;

			$fltBaseRateToReturn            = round( ( $objMasterPolicyCoverage->getBaseRate() / $intNoOfPolicyDays ) * $intNoOfPolicyDaysToReturn, 2 );
			$fltPolicyFeeToReturn           = round( ( $objMasterPolicyCoverage->getPolicyFee() / $intNoOfPolicyDays ) * $intNoOfPolicyDaysToReturn, 2 );
			$fltBaseTaxToReturn             = round( ( $objMasterPolicyCoverage->getBaseTax() / $intNoOfPolicyDays ) * $intNoOfPolicyDaysToReturn, 2 );
			$fltStampingTaxToReturn         = round( ( $objMasterPolicyCoverage->getStampingTax() / $intNoOfPolicyDays ) * $intNoOfPolicyDaysToReturn, 2 );
			$fltOtherTaxToReturn            = round( ( $objMasterPolicyCoverage->getOtherTax() / $intNoOfPolicyDays ) * $intNoOfPolicyDaysToReturn, 2 );
			$fltTotalPremiumAmountToReturn  = round( ( $objMasterPolicyCoverage->getTotalPremiumAmount() / $intNoOfPolicyDays ) * $intNoOfPolicyDaysToReturn, 2 );
			$fltProviderPremiumAmount       = round( ( $fltBaseRateToReturn + $fltPolicyFeeToReturn + $fltBaseTaxToReturn + $fltStampingTaxToReturn + $fltOtherTaxToReturn ), 2 );

			$fltAdminFee = 0;

			if( strtotime( date( 'Y-m-d', strtotime( $strMasterPolicyEffectiveDate ) ) ) == strtotime( date( 'Y-m-d', strtotime( $strMasterPolicyEndDate ) ) ) && $objMasterPolicyCoverage->getAdminFee() > 0 ) {
				$fltAdminFee = ( -1 ) * $objMasterPolicyCoverage->getAdminFee();
			}

			// Getting some cents difference. So refunding same amount as collected in case of flat cancellation
			if( abs( $intNoOfPolicyDaysToReturn ) == abs( $intNoOfPolicyDays ) ) {
				$fltProviderPremiumAmount = - 1 * $objMasterPolicyCoverage->getProviderPremiumAmount();
			}

			if( true == $boolIsDisableMasterPolicy ) {
				$objMasterPolicyCoverage->setIsDisabled( true );

				if( false == $objMasterPolicyCoverage->update( $intCompanyUserId, $objInsuranceDatabase ) ) {
					return false;
				}
			}

			$objNewMasterPolicyCoverage = new CMasterPolicyCoverage();
			$objNewMasterPolicyCoverage->setMasterPolicyId( $intMasterPolicyId );
			$objNewMasterPolicyCoverage->setStartDate( $objMasterPolicyCoverage->getStartDate() );
			$objNewMasterPolicyCoverage->setEndDate( $objMasterPolicyCoverage->getEndDate() );

			$objNewMasterPolicyCoverage->setProviderPremiumAmount( $fltProviderPremiumAmount );
			$objNewMasterPolicyCoverage->setTotalPremiumAmount( $fltTotalPremiumAmountToReturn );
			$objNewMasterPolicyCoverage->setBaseRate( $fltBaseRateToReturn );
			$objNewMasterPolicyCoverage->setPolicyFee( $fltPolicyFeeToReturn );
			$objNewMasterPolicyCoverage->setBaseTax( $fltBaseTaxToReturn );
			$objNewMasterPolicyCoverage->setStampingTax( $fltStampingTaxToReturn );
			$objNewMasterPolicyCoverage->setOtherTax( $fltOtherTaxToReturn );
			$objNewMasterPolicyCoverage->setAdminFee( $fltAdminFee );
			$objNewMasterPolicyCoverage->setInsuranceExportTypeId( CInsuranceExportType::CREDIT_EXPORT );
			$objNewMasterPolicyCoverage->setMasterPolicyCoverageId( $objMasterPolicyCoverage->getId() );
			$objNewMasterPolicyCoverage->setMasterPolicyRecordTypeId( CMasterPolicyRecordType::CANCEL );
			$objNewMasterPolicyCoverage->setIsDisabled( $boolIsDisableMasterPolicy );

			if( false == $objNewMasterPolicyCoverage->insert( $intCompanyUserId, $objInsuranceDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function getChangedClientBaseRate() {

		$arrmixInsuranceCarrierClientBaseRate['client_number'] = 3660;
		$arrmixInsuranceCarrierClientBaseRate['base_rate'] = 8.05;
		$arrmixInsuranceCarrierClientBaseRate['policy_fee'] = 1;
		$arrmixInsuranceCarrierClientBaseRate['basic_tax_percent'] = 4.85;
		$arrmixInsuranceCarrierClientBaseRate['stamping_tax_percent'] = 0.15;
		$arrmixInsuranceCarrierClientBaseRate['other_tax_percent'] = 0;
		$arrmixInsuranceCarrierClientBaseRate['basic_tax'] = 0.44;
		$arrmixInsuranceCarrierClientBaseRate['stamping_fee'] = 0.01;
		$arrmixInsuranceCarrierClientBaseRate['other_tax'] = 0;
		$arrmixInsuranceCarrierClientBaseRate['total_premium'] = 9.5;

		return $arrmixInsuranceCarrierClientBaseRate;
	}

	public static function getTerminatedAndDisabledPropertyIds( $arrintCids, $arrintPropertyIds, $objDatabase ) {

		$arrintDisabledPropertyIds = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchTerminatedAndDisabledPropertiesByIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase );
		$arrintDisabledPropertyIds = array_keys( rekeyArray( 'id', $arrintDisabledPropertyIds ) );

		$arrintTerminatedContractPropertyIds = ( array ) CContractProperties::createService()->fetchTerminatedContractPropertiesByPsProductIdByCids( CPsProduct::RESIDENT_INSURE, $arrintCids, $objDatabase );
		$arrintTerminatedContractPropertyIds = array_keys( rekeyArray( 'property_id', $arrintTerminatedContractPropertyIds ) );

		$arrintTerminatedAndDisabledPropertyIds = array_merge( $arrintDisabledPropertyIds, $arrintTerminatedContractPropertyIds );

		if( false == valArr( $arrintTerminatedAndDisabledPropertyIds ) ) {
			return [];
		}

		return array_unique( $arrintTerminatedAndDisabledPropertyIds );
	}

	public function proratePremium( $arrmixData ) : array {

		$strPolicyEffectiveDate = $arrmixData['policy_effective_date'];
		$fltForcedPlacedFee = $arrmixData['force_place_fee'];
		$arrmixInsuranceClientBaseRate = $arrmixData['client_base_rates_details'];
		$boolIsProrateByDay = $arrmixData['is_prorate_by_day'];

		$arrmixUninsuredResident['total_premium_amount'] = $fltForcedPlacedFee;

		$intDaysInMonth     = cal_days_in_month( CAL_GREGORIAN, date( 'm', strtotime( $strPolicyEffectiveDate ) ), date( 'y', strtotime( $strPolicyEffectiveDate ) ) );
		$intNoOfPolicyDays  = ( $intDaysInMonth - date( 'd', strtotime( $strPolicyEffectiveDate ) ) ) + 1;

		$fltProratedPremium     = ( ( ( $arrmixInsuranceClientBaseRate['base_rate'] + $arrmixInsuranceClientBaseRate['policy_fee'] ) / $intDaysInMonth ) * ( $intNoOfPolicyDays ) );
		$arrmixUninsuredResident['provider_premium_amount'] = ( $fltProratedPremium + ( $fltProratedPremium * $arrmixInsuranceClientBaseRate['basic_tax_percent'] / 100 ) + ( $fltProratedPremium * $arrmixInsuranceClientBaseRate['stamping_tax_percent'] / 100 ) + ( $fltProratedPremium * $arrmixInsuranceClientBaseRate['other_tax_percent'] / 100 ) );

		if( true == $boolIsProrateByDay && false == $arrmixData['is_blanket'] ) {

			if( 0 != $fltForcedPlacedFee && false == is_null( $fltForcedPlacedFee ) ) {
				if( $fltForcedPlacedFee > $arrmixInsuranceClientBaseRate['total_premium'] ) {
					$fltPropertyFee = $fltForcedPlacedFee - $arrmixInsuranceClientBaseRate['total_premium'];
				} else {
					$fltPropertyFee = $fltForcedPlacedFee;
				}

				$fltProratedPropertyFee = ( ( $fltPropertyFee / $intDaysInMonth ) * ( $intNoOfPolicyDays ) );

				if( $fltForcedPlacedFee > $arrmixInsuranceClientBaseRate['total_premium'] ) {
					$arrmixUninsuredResident['total_premium_amount'] = ( $fltProratedPremium + ( $fltProratedPremium * $arrmixInsuranceClientBaseRate['basic_tax_percent'] / 100 ) + ( $fltProratedPremium * $arrmixInsuranceClientBaseRate['stamping_tax_percent'] / 100 ) + ( $fltProratedPremium * $arrmixInsuranceClientBaseRate['other_tax_percent'] / 100 ) ) + $fltProratedPropertyFee;
				} else {
					$arrmixUninsuredResident['total_premium_amount'] = $fltProratedPropertyFee;
				}
			}
		}

		if( 0 == $fltForcedPlacedFee && false == is_null( $fltForcedPlacedFee ) ) {
			$arrmixUninsuredResident['total_premium_amount'] = 0;
		}

		$arrmixUninsuredResident['base_rate']               = ( $arrmixInsuranceClientBaseRate['base_rate'] / $intDaysInMonth ) * ( $intNoOfPolicyDays );
		$arrmixUninsuredResident['policy_fee']              = ( $arrmixInsuranceClientBaseRate['policy_fee'] / $intDaysInMonth ) * ( $intNoOfPolicyDays );
		$arrmixUninsuredResident['basic_tax']               = $fltProratedPremium * $arrmixInsuranceClientBaseRate['basic_tax_percent'] / 100;
		$arrmixUninsuredResident['stamping_tax']            = $fltProratedPremium * $arrmixInsuranceClientBaseRate['stamping_tax_percent'] / 100;
		$arrmixUninsuredResident['other_tax']               = $fltProratedPremium * $arrmixInsuranceClientBaseRate['other_tax_percent'] / 100;

		return $arrmixUninsuredResident;
	}

}
?>