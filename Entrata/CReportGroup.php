<?php

class CReportGroup extends CBaseReportGroup {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReportGroupTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultReportGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valParentModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
		$boolIsValid = true;
		if( true == empty( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Report group name is missing.' ) ) );
		}
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPositionNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPublishedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPublishedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function validateDuplicateName( $objDatabase = NULL ) {
		$boolIsValid = true;
		$strWhere = '
					WHERE
						cid = ' . ( int ) $this->m_intCid . '
						AND lower( name ) = \'' . \Psi\CStringService::singleton()->strtolower( pg_escape_string( $this->getName() ) ) . '\'
						AND parent_module_id = ' . ( int ) $this->m_intParentModuleId . '
						' . ( false == is_null( $this->m_intCompanyUserId ) ? ' AND company_user_id = ' . ( int ) $this->m_intCompanyUserId : ' AND company_user_id IS NULL ' ) . '
						AND deleted_on IS NULL' .
						( false == is_null( $this->m_intId ) ? ' AND id != ' . $this->m_intId : '' ) .
		 				' AND report_group_type_id = ' . ( int ) $this->m_intReportGroupTypeId;

		if( false == is_null( $objDatabase ) ) {
			$intExistingReportGroupCount = \Psi\Eos\Entrata\CReportGroups::createService()->fetchReportGroupCount( $strWhere, $objDatabase );

			if( 0 != $intExistingReportGroupCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ( CReportGroupType::PACKET == $this->m_intReportGroupTypeId ? 'Packet ' : 'Report Group ' ) . 'name already exists.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->validateDuplicateName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function sqlModuleId() {
		return ( false == is_null( $this->getModuleId() ) ) ? ( string ) $this->getModuleId() : '(
			SELECT
				min(m.id)
			FROM
				modules m
				LEFT JOIN report_groups rg ON rg.module_id = m.id AND rg.cid = ' . $this->getCid() . '
				LEFT JOIN report_instances ri ON ri.module_id = m.id AND ri.cid = ' . $this->getCid() . '
				LEFT JOIN report_new_instances rni ON rni.module_id = m.id AND rni.cid = ' . $this->m_intCid . '
			WHERE
				m.id >= ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '
				AND COALESCE( rg.id, ri.id, rni.id ) IS NULL
		)';
	}

	/**
	 * @param $intCurrentUserId
	 * @param CDatabase $objDatabase
	 * @param bool $boolReturnSqlOnly
	 * @return bool|mixed
	 */
	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = str_replace( 'RETURNING id', 'RETURNING id, module_id', parent::insert( $intCurrentUserId, $objDatabase, true ) );

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert report group record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		} else {
			if( 0 < $objDataset->getRecordCount() ) {
				while( false == $objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					if( true == isset( $arrmixValues['id'] ) ) $this->setId( $arrmixValues['id'] );
					if( true == isset( $arrmixValues['module_id'] ) ) $this->setModuleId( $arrmixValues['module_id'] );
					$objDataset->next();
				}
			}
		}

		$objDataset->cleanup();

		return true;
	}

	/**
	 * Other Functions
	 */

	public function insertReportGroup( $intCid, $intCompanyUserId, $intReportGroupUserId, $intParentModuleId, $strReportGroupName, $strReportGroupDescription, $intNextReportGroupOrderNum, $boolIsPublished, $boolIsDisabled, $objDatabase, $intReportGroupTypeId = NULL ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = '
			INSERT INTO report_groups(
				id,
				cid,
				report_group_type_id,
				default_report_group_id,
				module_id,
				parent_module_id,
				company_user_id,
				name,
				description,
				position_number,
				is_published,
				is_disabled,
				published_by,
				published_on,
				deleted_by,
				deleted_on,
				updated_by,
				updated_on,
				created_by,
				created_on
			)
			VALUES (
				nextval(\'report_groups_id_seq\'),
				' . ( int ) $intCid . ',
				' . ( false == is_null( $intReportGroupTypeId ) ? $intReportGroupTypeId : ( true == is_null( $intReportGroupUserId ) ? CReportGroupType::STANDARD : CReportGroupType::MY_REPORTS ) ) . ',
				NULL,
				(
					SELECT
						min(m.id)
					FROM
						modules m
						LEFT JOIN report_groups rg ON rg.module_id = m.id AND rg.cid = ' . ( int ) $intCid . '
						LEFT JOIN report_instances ri ON ri.module_id = m.id AND ri.cid = ' . ( int ) $intCid . '
						LEFT JOIN reports r ON r.id = ri.report_id AND r.cid = ' . ( int ) $intCid . '
					WHERE
						m.id >= ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '
						AND COALESCE( rg.id, ri.id, r.id ) IS NULL
				),
				' . ( int ) $intParentModuleId . ',
				' . ( false == is_null( $intReportGroupUserId ) ? $intReportGroupUserId : 'NULL' ) . ',
				\'' . pg_escape_string( $strReportGroupName ) . '\',
				\'' . pg_escape_string( $strReportGroupDescription ) . '\',
				' . ( int ) $intNextReportGroupOrderNum . ',
				' . ( $boolIsPublished ? 1 : 0 ) . ',
				' . ( $boolIsDisabled ? 1 : 0 ) . ',
				' . ( int ) $intCompanyUserId . ',
				NOW(),
				NULL,
				NULL,
				' . ( int ) $intCompanyUserId . ',
				NOW(),
				' . ( int ) $intCompanyUserId . ',
				NOW()
			) RETURNING id, module_id';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to report group. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		} else {
			// Have changed the existing implementation in order to get the id and module id of the newly inserted record set to the Report Group object
			if( 0 < $objDataset->getRecordCount() ) {
				while( false == $objDataset->eof() ) {
					$arrmixValues = $objDataset->fetchArray();
					if( true == isset( $arrmixValues['id'] ) ) $this->setId( $arrmixValues['id'] );
					if( true == isset( $arrmixValues['module_id'] ) ) $this->setModuleId( $arrmixValues['module_id'] );
					$objDataset->next();
				}
			}
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( date( 'm/d/Y H:i:s' ) );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
