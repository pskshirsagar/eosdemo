<?php

class CNoticeDeliveryType extends CBaseNoticeDeliveryType {

	const HAND_DELIVER              = 1;
	const EMAIL                     = 2;
	const EMAIL_AND_HAND_DELIVER   = 3;

	public static $c_arrintAllNoticeDeliveryTypes = [
		self::HAND_DELIVER,
		self::EMAIL,
		self::EMAIL_AND_HAND_DELIVER
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getDisplayTextById( $intNoticeDeliveryTypeId ) {

		$strDisplayName = '';
		switch( $intNoticeDeliveryTypeId ) {
			case self::HAND_DELIVER:
				$strDisplayName = ' hand delivered ';
				break;

			case self::EMAIL:
				$strDisplayName = ' emailed ';
				break;

			case self::EMAIL_AND_HAND_DELIVER:
				$strDisplayName = ' emailed and hand delivered ';
				break;

			default:
				$strDisplayName = ' ';
				break;
		}

		 return $strDisplayName;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function noticeDeliveryTypeIdToStr( $intNoticeDeliveryTypeId ) {
		switch( $intNoticeDeliveryTypeId ) {
			case self::HAND_DELIVER:
				return __( 'hand delivered' );
			case self::EMAIL:
				return __( 'emailed' );
			case self::EMAIL_AND_HAND_DELIVER:
				return __( 'emailed and hand delivered' );
			default:
				return NULL;
		}
	}

}
?>