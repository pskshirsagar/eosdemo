<?php

use Psi\Eos\Entrata\CSystemMessageTemplateSlots;
class CSystemMessageTemplate extends CBaseSystemMessageTemplate {

	const AMBIANCE_EMAIL_TEMPLATE 		= 1;
	const BASIC_TEMPLATE_ID				= 2;
	const CUSTOM_TEMPLATE_ID 			= 3;
	const SIMPLE_TEMPLATE_ID 			= 4;
	const ANNOUNCEMENT_EMAIL_TEMPLATE 	= 5;
	const FOCUS_EMAIL_TEMPLATE 			= 6;
	const HIGHLIGHT_TEMPLATE_ID 		= 7;
	const DETAIL_EMAIL_TEMPLATE 		= 8;
	const INFORMATION_EMAIL_TEMPLATE 	= 9;
	const POSTER_TEMPLATE_ID 			= 10;
	const BULLETIN_TEMPLATE_ID 			= 11;
	const CUSTOM 						= 12;
	const WHATSAPP                      = 13;

	protected $m_arrobjSystemMessageTemplateSlots;
	protected $m_arrstrSystemMessageTemplateNames;

	public static $c_arrstrSystemMessageTemplateKeys = [
		self::AMBIANCE_EMAIL_TEMPLATE 		=> 'AMBIANCE_EMAIL_TEMPLATE',
		self::BASIC_TEMPLATE_ID				=> 'BASIC_EMAIL_TEMPLATE',
		self::CUSTOM_TEMPLATE_ID			=> 'CUSTOM_EMAIL_TEMPLATE',
		self::ANNOUNCEMENT_EMAIL_TEMPLATE 	=> 'ANNOUNCEMENT_EMAIL_TEMPLATE',
		self::FOCUS_EMAIL_TEMPLATE			=> 'FOCUS_EMAIL_TEMPLATE',
		self::HIGHLIGHT_TEMPLATE_ID			=> 'HIGHLIGHTS_EMAIL_TEMPLATE',
		self::DETAIL_EMAIL_TEMPLATE			=> 'DETAIL_EMAIL_TEMPLATE',
		self::INFORMATION_EMAIL_TEMPLATE	=> 'INFORMATION_EMAIL_TEMPLATE',
		self::POSTER_TEMPLATE_ID			=> 'POSTER_EMAIL_TEMPLATE',
		self::BULLETIN_TEMPLATE_ID			=> 'BULLETIN_EMAIL_TEMPLATE'
	];

	public static $c_arrstrSystemMessageTemplateFolderNames = [
		self::AMBIANCE_EMAIL_TEMPLATE 		=> 'ambiance',
		self::BASIC_TEMPLATE_ID				=> 'basic',
		self::CUSTOM_TEMPLATE_ID			=> 'custom',
		self::ANNOUNCEMENT_EMAIL_TEMPLATE 	=> 'announcement',
		self::FOCUS_EMAIL_TEMPLATE			=> 'focus',
		self::HIGHLIGHT_TEMPLATE_ID			=> 'highlights',
		self::DETAIL_EMAIL_TEMPLATE			=> 'details',
		self::INFORMATION_EMAIL_TEMPLATE	=> 'informational',
		self::POSTER_TEMPLATE_ID			=> 'poster',
		self::BULLETIN_TEMPLATE_ID			=> 'bulletin'
	];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function setSystemMessageTemplateSlots( $arrobjSystemMessageTemplateSlots ) {
		$this->m_arrobjSystemMessageTemplateSlots = $arrobjSystemMessageTemplateSlots;
	}

	/**
	 * Get Functions
	 */

	public function getSystemMessageTemplateSlots() {
		return $this->m_arrobjSystemMessageTemplateSlots;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPreviewImageUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function fetchSystemMessageTemplateSlots( $objDatabase ) {
		return CSystemMessageTemplateSlots::createService()->fetchSystemMessageTemplateSlotsBySystemMessageTemplateId( $this->getId(), $objDatabase );
	}

	public function fetchPublishedSystemMessageTemplateSlots( $objDatabase ) {
		return CSystemMessageTemplateSlots::createService()->fetchPublishedSystemMessageTemplateSlotsBySystemMessageTemplateId( $this->getId(), $objDatabase );
	}

	public function getSystemMessageTemplateNames() {

		if( isset( $this->m_arrstrSystemMessageTemplateNames ) ) {
			return $this->m_arrstrSystemMessageTemplateNames;
		}

		$this->m_arrstrSystemMessageTemplateNames = [
			self::AMBIANCE_EMAIL_TEMPLATE 		=> __( 'Ambiance Email Layout' ),
			self::BASIC_TEMPLATE_ID				=> __( 'Basic Email Layout' ),
			self::CUSTOM_TEMPLATE_ID			=> __( 'Custom Email Layout' ),
			self::ANNOUNCEMENT_EMAIL_TEMPLATE 	=> __( 'Announcement Email Layout' ),
			self::FOCUS_EMAIL_TEMPLATE			=> __( 'Focus Email Layout' ),
			self::HIGHLIGHT_TEMPLATE_ID			=> __( 'Highlights Email Layout' ),
			self::DETAIL_EMAIL_TEMPLATE			=> __( 'Detail Email Layout' ),
			self::INFORMATION_EMAIL_TEMPLATE	=> __( 'Information Email Layout' ),
			self::POSTER_TEMPLATE_ID			=> __( 'Poster Email Layout' ),
			self::BULLETIN_TEMPLATE_ID			=> __( 'Bulletin Email Layout' )
		];

		return $this->m_arrstrSystemMessageTemplateNames;
	}

}
?>