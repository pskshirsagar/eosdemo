<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApiIps
 * Do not add any new functions to this class.
 */

class CApiIps extends CBaseApiIps {

	public static function fetchApiIpByIpAddressByCid( $strIpAddress, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						api_ips
					WHERE
						ip_address::INET >>= \'' . addslashes( trim( $strIpAddress ) ) . '\'::INET
						AND ( cid = ' . ( int ) $intCid . ' OR cid = ' . ( int ) CClients::$c_intNullCid . ' )
					LIMIT 1';

		return self::fetchApiIp( $strSql, $objDatabase );
	}
}
?>