<?php

use Psi\Eos\Entrata\CMaintenanceRequestNotes;
use Psi\Eos\Entrata\CMaintenanceStatuses;
use Psi\Eos\Entrata\CPropertyEmailRules;
use Psi\Eos\Entrata\CMaintenanceProblems;
use Psi\Eos\Entrata\CMaintenanceRequests;
use Psi\Eos\Entrata\CPropertyMaintenancePriorities;
use Psi\Eos\Entrata\CFileAssociations;
use Psi\Eos\Entrata\CPropertyUnits;
use Psi\Eos\Entrata\CCompanyEmployees;
use Psi\Eos\Entrata\CUnitSpaces;
use Psi\Eos\Entrata\CProperties;
use Psi\Eos\Entrata\CPropertyPreferences;
use Psi\Core\Facilities\CMaintenanceSmsLibrary;

class CMaintenanceRequest extends CBaseMaintenanceRequest {

	const PERMISSION_TO_ENTER_MENDATORY        = 1;
	const PERMISSION_TO_ENTER_HIDE             = 2;
	const PERMISSION_TO_ENTER_REQUIRE_RESPONSE = 3;
	const VALIDATE_WEBSERVICE_UPDATE           = 'update_webservice';
	const SMS_LENGTH                           = 160;
	const TERMS_AND_REPLY_STOP_MESSAGE         = ' Reply STOP FIX to cancel msgs. Reply HELP for help or https://www.entrata.com/help';
	const REPLACE_SPECIAL_CHARACTERS_PATTERN   = '/[~`\^\*\+=\{\}\[\]\|;"\<\>\\\]/'; // Same as getSanitizedFormField() with Special Characters, just exclude the ' (quote) from it

	const MAINTENANCE_FILTER_SEARCH_TYPE_UNIT      = 'unit';
	const MAINTENANCE_FILTER_SEARCH_TYPE_RESIDENT  = 'resident';
	const MAINTENANCE_FILTER_SEARCH_TYPE_REFERENCE = 'reference';
	const MAINTENANCE_FILTER_DATE_TYPE_CREATED     = 'created';
	const MAINTENANCE_FILTER_DATE_TYPE_SCHEDULED   = 'scheduled';
	const MAINTENANCE_FILTER_DATE_TYPE_DUE         = 'due';
	const MAINTENANCE_FILTER_DATE_TYPE_COMPLETED   = 'completed';
	const MAINTENANCE_FILTER_DATE_TYPE_RESPOND     = 'respond';

	const MAINTENANCE_FILTER_DATE_INTERVAL_ANYTIME      = 1;
	const MAINTENANCE_FILTER_DATE_INTERVAL_TODAY        = 2;
	const MAINTENANCE_FILTER_DATE_INTERVAL_YESTERDAY    = 3;
	const MAINTENANCE_FILTER_DATE_INTERVAL_SEVEN_DAYS   = 4;
	const MAINTENANCE_FILTER_DATE_INTERVAL_THIRTY_DAYS  = 5;
	const MAINTENANCE_FILTER_DATE_INTERVAL_NINTY_DAYS   = 6;
	const MAINTENANCE_FILTER_DATE_INTERVAL_CUSTOM_RANGE = 7;

	const NO_SURVEY                  = 0;
	const OLD_CUSTOM_SURVEY          = 1;
	const SIMPLE_SATISFACTION_SURVEY = 2;
	const THIRD_PARTY_URL            = 3;

	// Below two constants are not related to maintenance request statuses specified in database. These are customized and used only for view purpose.
	// Also for this class, Do not use the function similar to getTypeNameByTypeId() for getting constant name by Id.
	const MAINTENANCE_REQUEST_STATUS_INCOMPLETE = 1;
	const MAINTENANCE_REQUEST_STATUS_COMPLETE   = 2;

	protected $m_boolIsFloatingMaintenanceRequest;
	protected $m_boolIsShowIntegrationError;
	protected $m_boolIsMultipleMaintenanceRequest;
	protected $m_boolIsPropertyBuilding;
	protected $m_boolIsValidateCallId;
	protected $m_boolIsNotesVisibleToResident;
	protected $m_boolIsPropertyManager = false;
	protected $m_boolCheckExistingMaintenanceStatus = false;
	protected $m_boolIsNewMaintenanceRequest = false;
	protected $m_boolExcludeWeekendsHolidays = false;
	protected $m_boolIsEditMaintenanceRequest;
	protected $m_intIsVisibleToResident;
	protected $m_boolIsClientProblem;
	protected $m_boolIsMakeReadyExcludeClosedDays;
	protected $m_boolIsScheduledFromEntrataCalendar = false;
	protected $m_boolIsPublishedProblemAction;
	protected $m_boolIsCommercialProperty = false;
	protected $m_boolIsValidateScheduledEndDate = true;
	protected $m_intIsPrintedOn;

	protected $m_intMessageOperatorId;
	protected $m_intCallId;
	protected $m_intMaintenanceStatusTypeId;
	protected $m_intMaintenanceCategoryId;
	protected $m_intUnitSpaceStatusTypeId;
	protected $m_intDaysRemaining;
	protected $m_intDaysToComplete;
	protected $m_intIncludeInOther;
	protected $m_intUserDepartmentId;
	protected $m_intInspectionId;
	protected $m_intMaintenancePriorityTypeId;
	protected $m_intLeaseCustomerId;
	protected $m_intCalendarEventId;
	protected $m_intEventId;
	protected $m_intMaintenanceRequestResponseTypeId;
	protected $m_intMakeReadyDaysToComplete;
	protected $m_intIsOrganization;
	protected $m_intOpenSubTaskCount;

	protected $m_strUnitNumber;
	protected $m_strSpaceNumber;
	protected $m_strBuildingName;
	protected $m_strMobilePhoneNumber;
	protected $m_strCompanyUserNameFirst;
	protected $m_strCompanyUserNameLast;
	protected $m_strCompanyUserUsername;
	protected $m_strRequestDateTime;
	protected $m_strMaintenanceLocationName;
	protected $m_strNote;
	protected $m_strBuildingNumber;
	protected $m_strRequestedTime;
	protected $m_strRequestedDay;
	protected $m_strMaintenancePriorityName;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_strWorkOrderCreatedBy;
	protected $m_strUnitSpaceStatusTypeName;
	protected $m_strPropertyFloorPlanName;
	protected $m_strAlarmCode;
	protected $m_strMaintenanceLocationType;
	protected $m_strUserName;
	protected $m_strResidentStatusTypeName;
	protected $m_strMakeReadyMaintenanceRequestStatus;
	protected $m_strMaintenanceRequestType;
	protected $m_strMaintenanceStatusName;
	protected $m_strPropertyName;
	protected $m_strCustomSuccessMessage;
	protected $m_strPetsDescription;
	protected $m_strOrigin;
	protected $m_strMaintenanceCategoryName;
	protected $m_strCompanyEmployeeFullName;
	protected $m_strMaintenanceProblemName;
	protected $m_strMaintenanceProblemActionName;
	protected $m_strChargeCodeName;
	protected $m_strApPayeeCompanyName;
	protected $m_strMaintenanceStatusTypeName;
	protected $m_strLeaseAction;
	protected $m_strDueTime;
	protected $m_strCalendarEventStartDatetime;
	protected $m_strCalendarEventEndDatetime;
	protected $m_strParentActualStartDatetime;
	protected $m_strResponseText;
	protected $m_strEntryNotes;
	protected $m_strClosingNote;
	protected $m_strResidentSMS;
	protected $m_strCustomerFullName;
	protected $m_strResponseDateTime;
	/**
	 * @var CCustomer $m_objCustomer
	 */
	protected $m_objCustomer;
	/**
	 * @var CLease $m_objLease
	 */
	protected $m_objLease;
	/**
	 * @var \CProperty $m_objProperty
	 */
	protected $m_objProperty;
	protected $m_objUnitSpace;
	protected $m_objEmailDatabase;
	protected $m_objLabor;
	protected $m_objMaterial;
	protected $m_objAttachment;
	protected $m_objMaintenanceStatus;
	protected $m_objMileage;
	protected $m_objOldMaintenanceRequest;
	protected $m_objMaintenanceTemplate;

	protected $m_arrobjMaintenanceRequests;
	protected $m_arrobjSubMaintenanceRequest;
	protected $m_arrobjMaintenanceRequestPublishedNotes;
	protected $m_arrobjPropertyPreferences;
	/** @var  \CFile[] */
	protected $m_arrobjFiles;

	protected $m_arrmixChargeAmounts;
	protected $m_arrmixUpdateMaintenanceStatus;

	protected $m_arrstrMaintenanceRequestStatuses;
	protected $m_strMaintenanceExceptionType;
	protected $m_strJobName;

	public function __construct() {
		parent::__construct();
		$this->m_objMaintenanceStatus = NULL;

		return;
	}

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_floating_maintenance_request'] ) ) {
			$this->setIsFloatingMaintenanceRequest( $arrmixValues['is_floating_maintenance_request'] );
		}
		if( true == isset( $arrmixValues['unit_number'] ) ) {
			$this->setUnitNumber( $arrmixValues['unit_number'] );
		}
		if( true == isset( $arrmixValues['space_number'] ) ) {
			$this->setSpaceNumber( $arrmixValues['space_number'] );
		}
		if( true == isset( $arrmixValues['unit_number_cache'] ) ) {
			$this->setUnitNumber( $arrmixValues['unit_number_cache'] );
		}
		if( true == isset( $arrmixValues['building_name'] ) ) {
			$this->setBuildingName( $arrmixValues['building_name'] );
		}
		if( true == isset( $arrmixValues['mobile_phone_number'] ) ) {
			$this->setMobilePhoneNumber( $arrmixValues['mobile_phone_number'] );
		}
		if( true == isset( $arrmixValues['message_operator_id'] ) ) {
			$this->setMessageOperatorId( $arrmixValues['message_operator_id'] );
		}
		if( true == isset( $arrmixValues['maintenance_location_name'] ) ) {
			$this->setMaintenanceLocationName( $arrmixValues['maintenance_location_name'] );
		}
		if( true == isset( $arrmixValues['requested_day'] ) ) {
			$this->setRequestedDay( $arrmixValues['requested_day'] );
		}
		if( true == isset( $arrmixValues['requested_time'] ) ) {
			$this->setRequestedTime( $arrmixValues['requested_time'] );
		}
		if( true == isset( $arrmixValues['maintenance_priority_name'] ) ) {
			$this->setMaintenancePriorityName( $arrmixValues['maintenance_priority_name'] );
		}
		if( true == isset( $arrmixValues['move_in_date'] ) ) {
			$this->setMoveInDate( $arrmixValues['move_in_date'] );
		}
		if( true == isset( $arrmixValues['move_out_date'] ) ) {
			$this->setMoveOutDate( $arrmixValues['move_out_date'] );
		}
		if( true == isset( $arrmixValues['maintenance_status_type_id'] ) ) {
			$this->setMaintenanceStatusTypeId( $arrmixValues['maintenance_status_type_id'] );
		}
		if( true == isset( $arrmixValues['work_order_created_by'] ) ) {
			$this->setWorkOrderCreatedBy( $arrmixValues['work_order_created_by'] );
		}
		if( true == isset( $arrmixValues['pet'] ) ) {
			$this->setPet( $arrmixValues['pet'] );
		}
		if( true == isset( $arrmixValues['maintenance_category_id'] ) ) {
			$this->setMaintenanceCategoryId( $arrmixValues['maintenance_category_id'] );
		}
		if( true == isset( $arrmixValues['maintenance_category_name'] ) ) {
			$this->setMaintenanceCategoryName( $arrmixValues['maintenance_category_name'] );
		}
		if( true == isset( $arrmixValues['days_remaining'] ) ) {
			$this->setDaysRemaining( $arrmixValues['days_remaining'] );
		}
		if( true == isset( $arrmixValues['unit_space_status_type_name'] ) ) {
			$this->setUnitSpaceStatusTypeName( $arrmixValues['unit_space_status_type_name'] );
		}
		if( true == isset( $arrmixValues['unit_space_status_type_id'] ) ) {
			$this->setUnitSpaceStatusTypeId( $arrmixValues['unit_space_status_type_id'] );
		}
		if( true == isset( $arrmixValues['property_floorplan_name'] ) ) {
			$this->setPropertyFloorPlanName( $arrmixValues['property_floorplan_name'] );
		}
		if( true == isset( $arrmixValues['maintenance_location_type'] ) ) {
			$this->setMaintenanceLocationType( $arrmixValues['maintenance_location_type'] );
		}
		if( true == isset( $arrmixValues['username'] ) ) {
			$this->setCompanyUserUsername( $arrmixValues['username'] );
		}
		if( true == isset( $arrmixValues['name_first'] ) ) {
			$this->setCompanyUserNameFirst( $arrmixValues['name_first'] );
		}
		if( true == isset( $arrmixValues['name_last'] ) ) {
			$this->setCompanyUserNameLast( $arrmixValues['name_last'] );
		}
		if( true == isset( $arrmixValues['resident_status_type_name'] ) ) {
			$this->setResidentStatusTypeName( $arrmixValues['resident_status_type_name'] );
		}
		if( true == isset( $arrmixValues['maintenance_problem_name'] ) ) {
			$this->setMaintenanceProblemName( $arrmixValues['maintenance_problem_name'] );
		}
		if( true == isset( $arrmixValues['maintenance_problem_action_name'] ) ) {
			$this->setMaintenanceProblemActionName( $arrmixValues['maintenance_problem_action_name'] );
		}

		if( true == isset( $arrmixValues['is_published_problem_action'] ) ) {
			$this->setIsPublishedProblemAction( $arrmixValues['is_published_problem_action'] );
		}

		if( true == isset( $arrmixValues['inspection_id'] ) ) {
			$this->setInspectionId( $arrmixValues['inspection_id'] );
		}
		if( true == isset( $arrmixValues['origin'] ) ) {
			$this->setOrigin( $arrmixValues['origin'] );
		}
		if( true == isset( $arrmixValues['maintenance_request_type'] ) ) {
			$this->setMaintenanceRequestType( $arrmixValues['maintenance_request_type'] );
		}
		if( true == isset( $arrmixValues['maintenance_status_name'] ) ) {
			$this->setMaintenanceStatusName( $arrmixValues['maintenance_status_name'] );
		}
		if( true == isset( $arrmixValues['maintenance_priority_type_id'] ) ) {
			$this->setMaintenancePriorityTypeId( $arrmixValues['maintenance_priority_type_id'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['alarm_code'] ) ) {
			$this->setAlarmCode( $arrmixValues['alarm_code'] );
		}
		if( true == isset( $arrmixValues['company_employee_fullname'] ) ) {
			$this->setCompanyEmployeeFullName( $arrmixValues['company_employee_fullname'] );
		}
		if( true == isset( $arrmixValues['lease_customer_id'] ) ) {
			$this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
		}
		if( true == isset( $arrmixValues['calendar_event_id'] ) ) {
			$this->setCalendarEventId( $arrmixValues['calendar_event_id'] );
		}
		if( true == isset( $arrmixValues['ap_payee_company_name'] ) ) {
			$this->setApPayeeCompanyName( $arrmixValues['ap_payee_company_name'] );
		}
		if( true == isset( $arrmixValues['maintenance_status_type_name'] ) ) {
			$this->setMaintenanceStatusTypeName( $arrmixValues['maintenance_status_type_name'] );
		}
		if( true == isset( $arrmixValues['calendar_event_start_date_time'] ) ) {
			$this->setCalendarEventStartDateTime( $arrmixValues['calendar_event_start_date_time'] );
		}
		if( true == isset( $arrmixValues['calendar_event_end_date_time'] ) ) {
			$this->setCalendarEventEndDateTime( $arrmixValues['calendar_event_end_date_time'] );
		}
		if( true == isset( $arrmixValues['is_client_problem'] ) ) {
			$this->setIsClientProblem( $arrmixValues['is_client_problem'] );
		}
		if( true == isset( $arrmixValues['make_ready_days_to_complete'] ) ) {
			$this->setMakeReadyDaysToComplete( $arrmixValues['make_ready_days_to_complete'] );
		}
		if( true == isset( $arrmixValues['property_unit_location_id'] ) ) {
			$this->setPropertyUnitMaintenanceLocationId( $arrmixValues['property_unit_location_id'] );
		}
		if( true == isset( $arrmixValues['property_unit_location_id_sub_task'] ) ) {
			$this->setPropertyUnitMaintenanceLocationId( $arrmixValues['property_unit_location_id_sub_task'] );
		}
		if( true == isset( $arrmixValues['parent_actual_start_datetime'] ) ) {
			$this->setParentActualStartDatetime( $arrmixValues['parent_actual_start_datetime'] );
		}
		if( true == isset( $arrmixValues['make_ready_exclude_closed_days'] ) ) {
			$this->setMakeReadyExcludeClosedDays( $arrmixValues['make_ready_exclude_closed_days'] );
		}
		if( true == isset( $arrmixValues['entry_notes'] ) ) {
			$this->setEntryNotes( $arrmixValues['entry_notes'] );
		}
		if( true == isset( $arrmixValues['is_visible_to_resident'] ) ) {
			$this->setIsVisibleToResident( $arrmixValues['is_visible_to_resident'] );
		}
		if( true == isset( $arrmixValues['customer_full_name'] ) ) {
			$this->setCustomerFullName( $arrmixValues['customer_full_name'] );
		}
		if( true == isset( $arrmixValues['job_name'] ) ) {
			$this->setJobName( $arrmixValues['job_name'] );
		}
		if( true == isset( $arrmixValues['is_organization'] ) ) {
			$this->setIsOrganization( $arrmixValues['is_organization'] );
		}
		if( true == isset( $arrmixValues['open_sub_task_count'] ) ) {
			$this->setOpenSubTaskCount( $arrmixValues['open_sub_task_count'] );
		}
		if( true == isset( $arrmixValues['is_printed_on'] ) ) {
			$this->setIsPrintedOn( $arrmixValues['is_printed_on'] );
		}
	}

	/**
	 * Setters
	 *
	 */

	public function setMaintenanceStatusType( $objMaintenanceStatus ) {
		$this->m_objMaintenanceStatus = $objMaintenanceStatus;
	}

	public function setNote( $strNote ) {
		$this->m_strNote = $strNote;
	}

	public function setIsScheduledFromEntrataCalendar( $boolIsScheduledFromEntrataCalendar = false ) {
		$this->m_boolIsScheduledFromEntrataCalendar = $boolIsScheduledFromEntrataCalendar;
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setIsShowIntegrationError( $boolIsShowIntegrationError = false ) {
		$this->m_boolIsShowIntegrationError = $boolIsShowIntegrationError;
	}

	public function setMaintenanceRequests( $arrobjMaintenanceRequests ) {
		$this->m_arrobjMaintenanceRequests = $arrobjMaintenanceRequests;
	}

	public function setIsMultipleMaintenanceRequest( $boolIsMultipleMaintenanceRequest = false ) {
		$this->m_boolIsMultipleMaintenanceRequest = $boolIsMultipleMaintenanceRequest;
	}

	public function setIsPropertyBuilding( $boolIsPropertyBuilding = false ) {
		$this->m_boolIsPropertyBuilding = $boolIsPropertyBuilding;
	}

	public function setIsValidateCallId( $intIsValidateCallId ) {
		$this->m_boolIsValidateCallId = $intIsValidateCallId;
	}

	public function setIsNotesVisibleToResident( $boolIsNotesVisibleToResident ) {
		$this->m_boolIsNotesVisibleToResident = $boolIsNotesVisibleToResident;
	}

	public function setCallId( $intCallId ) {
		$this->m_intCallId = $intCallId;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setRequestedTime( $strRequestedTime ) {
		$this->m_strRequestedTime = $strRequestedTime;
	}

	public function setRequestedDay( $strRequestedDay ) {
		$this->m_strRequestedDay = $strRequestedDay;
	}

	public function setMaintenancePriorityName( $strMaintenancePriorityName ) {
		$this->m_strMaintenancePriorityName = $strMaintenancePriorityName;
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->m_strMoveInDate = $strMoveInDate;
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->m_strMoveOutDate = $strMoveOutDate;
	}

	public function setMaintenanceStatusTypeId( $intMaintenanceStatusTypeId ) {
		$this->m_intMaintenanceStatusTypeId = CStrings::strToIntDef( $intMaintenanceStatusTypeId, NULL, false );
	}

	public function setMaintenanceCategoryId( $intMaintenanceCategoryId ) {
		$this->m_intMaintenanceCategoryId = $intMaintenanceCategoryId;
	}

	public function setMaintenanceCategoryName( $strMaintenanceCategoryName ) {
		$this->m_strMaintenanceCategoryName = $strMaintenanceCategoryName;
	}

	public function setWorkOrderCreatedBy( $strWorkOrderCreatedBy ) {
		$this->m_strWorkOrderCreatedBy = $strWorkOrderCreatedBy;
	}

	public function setDaysRemaining( $intDaysRemaining ) {
		$this->m_intDaysRemaining = $intDaysRemaining;
	}

	public function setUnitSpaceStatusTypeName( $strUnitSpaceStatusTypeName ) {
		$this->m_strUnitSpaceStatusTypeName = $strUnitSpaceStatusTypeName;
	}

	public function setUnitSpaceStatusTypeId( $intUnitSpaceStatusTypeId ) {
		$this->m_intUnitSpaceStatusTypeId = $intUnitSpaceStatusTypeId;
	}

	public function setPropertyFloorPlanName( $strPropertyFloorPlanName ) {
		$this->m_strPropertyFloorPlanName = $strPropertyFloorPlanName;
	}

	public function setMaintenanceLocationType( $strMaintenanceLocationTypeId ) {
		$this->m_strMaintenanceLocationType = $strMaintenanceLocationTypeId;
	}

	public function setBuildingNumber( $intBuildingNumber ) {
		$this->m_strBuildingNumber = $intBuildingNumber;
	}

	public function setCompanyUserNameFirst( $strNameFirst ) {
		$this->m_strCompanyUserNameFirst = $strNameFirst;
	}

	public function setCompanyUserNameLast( $strNameLast ) {
		$this->m_strCompanyUserNameLast = $strNameLast;
	}

	public function setCompanyUserUsername( $strUsername ) {
		$this->m_strCompanyUserUsername = $strUsername;
	}

	public function setResidentStatusTypeName( $strResidentStatusTypeName ) {
		$this->m_strResidentStatusTypeName = $strResidentStatusTypeName;
	}

	public function setMaintenanceChargeAmount( $arrmixChargeAmounts ) {
		$this->m_arrmixChargeAmounts = $arrmixChargeAmounts;
	}

	public function setMaintenanceProblemName( $strMaintenanceProblemName ) {
		$this->m_strMaintenanceProblemName = $strMaintenanceProblemName;
	}

	public function setMaintenanceProblemActionName( $strMaintenanceProblemActionName ) {
		$this->m_strMaintenanceProblemActionName = $strMaintenanceProblemActionName;
	}

	public function setIsPublishedProblemAction( $boolIsPublishedProblemAction ) {
		$this->m_boolIsPublishedProblemAction = CStrings::strToBool( $boolIsPublishedProblemAction );
	}

	public function setIsCommercialProperty( $boolIsCommercialProperty ) {
		$this->m_boolIsCommercialProperty = CStrings::strToBool( $boolIsCommercialProperty );
	}

	public function setIsValidateScheduledEndDate( $boolIsValidateScheduledEndDate ) {
		$this->m_boolIsValidateScheduledEndDate = CStrings::strToBool( $boolIsValidateScheduledEndDate );
	}

	public function setMakeReadyMaintenanceRequestStatus( $strMakeReadyMaintenanceRequestStatus ) {
		return $this->m_strMakeReadyMaintenanceRequestStatus = $strMakeReadyMaintenanceRequestStatus;
	}

	public function setMaintenanceLabor( $objLabor ) {
		$this->m_objLabor = $objLabor;
	}

	public function setMaintenanceMaterial( $objMaterial ) {
		$this->m_objMaterial = $objMaterial;
	}

	public function setMaintenanceAttachment( $objAttachment ) {
		$this->m_objAttachment = $objAttachment;
	}

	public function setCustomerNameFirstEncrypted( $strCustomerNameFirstEncrypted ) {
		$this->m_strCustomerNameFirstEncrypted = $strCustomerNameFirstEncrypted;
	}

	public function setCustomerNameLastEncrypted( $strCustomerNameLastEncrypted ) {
		$this->m_strCustomerNameLastEncrypted = $strCustomerNameLastEncrypted;
	}

	public function setMainPhoneNumberEncrypted( $strMainPhoneNumberEncrypted ) {
		$this->m_strMainPhoneNumberEncrypted = $strMainPhoneNumberEncrypted;
	}

	public function setAltPhoneNumberEncrypted( $strAltPhoneNumberEncrypted ) {
		$this->m_strAltPhoneNumberEncrypted = $strAltPhoneNumberEncrypted;
	}

	public function setCustomerNameFirst( $strCustomerNameFirst ) {
		$this->m_strCustomerNameFirst = CStrings::strTrimDef( $strCustomerNameFirst, 50, NULL, true );
		if( true == valStr( $this->m_strCustomerNameFirst ) ) {
			$this->setCustomerNameFirstEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $this->m_strCustomerNameFirst, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setCustomerNameLast( $strCustomerNameLast ) {
		$this->m_strCustomerNameLast = CStrings::strTrimDef( $strCustomerNameLast, 50, NULL, true );
		if( true == valStr( $this->m_strCustomerNameLast ) ) {
			$this->setCustomerNameLastEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $this->m_strCustomerNameLast, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}

	}

	public function setMainPhoneNumber( $strMainPhoneNumber ) {
		$this->m_strMainPhoneNumber = CStrings::strTrimDef( $strMainPhoneNumber, 30, NULL, true );
		if( true == valStr( $this->m_strMainPhoneNumber ) ) {
			$this->setMainPhoneNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $this->m_strMainPhoneNumber, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}

	}

	public function setAltPhoneNumber( $strAltPhoneNumber ) {
		$this->m_strAltPhoneNumber = CStrings::strTrimDef( $strAltPhoneNumber, 30, NULL, true );
		if( true == valStr( $this->m_strAltPhoneNumber ) ) {
			$this->setAltPhoneNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( CStrings::strTrimDef( $this->m_strAltPhoneNumber, 240, NULL, true ), CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setOrigin( $strOrigin ) {
		$this->m_strOrigin = $strOrigin;
	}

	public function setMaintenanceRequestType( $strMaintenanceRequestType ) {
		$this->m_strMaintenanceRequestType = $strMaintenanceRequestType;
	}

	public function setMaintenanceRequestTypeName( $intMaintenanceRequestTypeId ) {
		if( CMaintenanceRequestType::SERVICE_REQUEST == $intMaintenanceRequestTypeId ) {
			$this->m_strMaintenanceRequestType = 'Service Request';
		} else {
			if( CMaintenanceRequestType::MAKE_READY == $intMaintenanceRequestTypeId ) {
				$this->m_strMaintenanceRequestType = 'Make Ready';
			} else {
				if( CMaintenanceRequestType::RECURRING == $intMaintenanceRequestTypeId ) {
					$this->m_strMaintenanceRequestType = 'Recurring';
				} else {
					if( CMaintenanceRequestType::FEATURE_UPGRADE == $intMaintenanceRequestTypeId ) {
						$this->m_strMaintenanceRequestType = 'Feature Upgrade';
					}
				}
			}
		}
	}

	public function setMaintenanceStatusName( $strMaintenanceStatusName ) {
		$this->m_strMaintenanceStatusName = $strMaintenanceStatusName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setCustomSuccessMessage( $strCustomSuccessMessage ) {
		$this->m_strCustomSuccessMessage = $strCustomSuccessMessage;
	}

	public function setMaintenancePriorityTypeId( $strMaintenancePriorityTypeId ) {
		$this->m_intMaintenancePriorityTypeId = $strMaintenancePriorityTypeId;
	}

	public function setAlarmCode( $strAlarmCode ) {
		$this->m_strAlarmCode = CStrings::strTrimDef( $strAlarmCode, 20, NULL, true );
	}

	public function setMaintenanceLocationName( $strMaintenanceLocationName ) {
		$this->m_strMaintenanceLocationName = $strMaintenanceLocationName;
	}

	public function setIsFloatingMaintenanceRequest( $boolIsFloatingMaintenance ) {
		$this->m_boolIsFloatingMaintenanceRequest = $boolIsFloatingMaintenance;
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setUnitSpace( $objUnitSpace ) {
		$this->m_objUnitSpace = $objUnitSpace;
	}

	public function setMaintenanceTemplate( $objMaintenanceTemplate ) {
		$this->m_objMaintenanceTemplate = $objMaintenanceTemplate;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = CStrings::strTrimDef( $strUnitNumber, 50, NULL, true );
	}

	public function setSpaceNumber( $strSpaceNumber ) {
		$this->m_strSpaceNumber = $strSpaceNumber;
	}

	public function setMobilePhoneNumber( $strMobilePhoneNumber ) {
		$this->m_strMobilePhoneNumber = $strMobilePhoneNumber;
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->m_intMessageOperatorId = $intMessageOperatorId;
	}

	public function setRequestDateTime( $strRequestDateTime ) {
		$this->m_strRequestDateTime = $strRequestDateTime;
	}

	public function setParentMaintenanceRequestId( $intParentMaintenanceRequestId ) {
		$this->m_intParentMaintenanceRequestId = $intParentMaintenanceRequestId;
	}

	public function setCheckExistingMaintenanceStatus( $boolCheckExistingMaintenanceStatus ) {
		$this->m_boolCheckExistingMaintenanceStatus = $boolCheckExistingMaintenanceStatus;
	}

	public function setIsEditMaintenanceRequest( $boolIsEditMaintenanceRequest ) {
		$this->m_boolIsEditMaintenanceRequest = $boolIsEditMaintenanceRequest;
	}

	public function setUserDepartmentId( $intUserDepartmentId ) {
		$this->m_intUserDepartmentId = $intUserDepartmentId;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setInspectionId( $intInspectionId ) {
		$this->m_intInspectionId = $intInspectionId;
	}

	public function setMaintenanceMileage( $objMileage ) {
		$this->m_objMileage = $objMileage;
	}

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = $strCompanyEmployeeFullName;
	}

	public function setCalendarEventId( $intCalendarEventId ) {
		$this->m_intCalendarEventId = $intCalendarEventId;
	}

	public function setChargeCodeName( $strChargeCodeName ) {
		$this->m_strChargeCodeName = $strChargeCodeName;
	}

	public function setApPayeeCompanyName( $strApPayeeCompanyName ) {
		$this->m_strApPayeeCompanyName = $strApPayeeCompanyName;
	}

	public function setMaintenanceStatusTypeName( $strMaintenanceStatusTypeName ) {
		$this->m_strMaintenanceStatusTypeName = $strMaintenanceStatusTypeName;
	}

	public function setIsVisibleToResident( $intIsResidentVisible ) {
		$this->m_intIsVisibleToResident = CStrings::strToIntDef( $intIsResidentVisible, NULL, false );
	}

	public function setAsset( $objAsset ) {
		$this->m_objAsset = $objAsset;
	}

	public function setLeaseAction( $strLeaseAction ) {
		$this->m_strLeaseAction = $strLeaseAction;
	}

	public function setMaintenanceRequestResponseTypeId( $intMaintenanceRequestResponseTypeId ) {
		$this->m_intMaintenanceRequestResponseTypeId = $intMaintenanceRequestResponseTypeId;
	}

	public function setResponseText( $strResponseText ) {
		$this->m_strResponseText = $strResponseText;
	}

	public function setResponseDatetime( $strResponseDatetime ) {
		$this->m_strResponseDatetime = $strResponseDatetime;
	}

	public function setCalendarEventStartDateTime( $strCalendarEventStartDatetime ) {
		$this->m_strCalendarEventStartDatetime = $strCalendarEventStartDatetime;
	}

	public function setCalendarEventEndDateTime( $strCalendarEventEndDatetime ) {
		$this->m_strCalendarEventEndDatetime = $strCalendarEventEndDatetime;
	}

	public function setIsClientProblem( $boolIsClientProblem ) {
		$this->m_boolIsClientProblem = $boolIsClientProblem;
	}

	public function setMakeReadyDaysToComplete( $intMakeReadyDaysToComplete ) {
		$this->m_intMakeReadyDaysToComplete = $intMakeReadyDaysToComplete;
	}

	public function setParentActualStartDatetime( $strParentActualStartDatetime ) {
		$this->m_strParentActualStartDatetime = $strParentActualStartDatetime;
	}

	public function setEntryNotes( $strEntryNotes ) {
		$this->set( 'm_strEntryNotes', CStrings::strTrimDef( $strEntryNotes, 240, NULL, true ) );
	}

	public function setClosingNote( $strClosingNote ) {
		$this->set( 'm_strClosingNote', CStrings::strTrimDef( $strClosingNote, 2000, NULL, true ) );
	}

	public function setResidentSMS( $strResidentSMS ) {
		$this->set( 'm_strResidentSMS', CStrings::strTrimDef( $strResidentSMS, 2000, NULL, true ) );
	}

	public function setMakeReadyExcludeClosedDays( $boolIsMakeReadyExcludeClosedDays ) {
		$this->m_boolIsMakeReadyExcludeClosedDays = $boolIsMakeReadyExcludeClosedDays;
	}

	public function setMaintenanceExceptionType( $strMaintenanceExceptionType ) {
		$this->m_strMaintenanceExceptionType = $strMaintenanceExceptionType;
	}

	public function setEventId( $intEventId ) {
		$this->m_intEventId = $intEventId;
	}

	public function setCustomerFullName( $strCustomerFullName ) {
		$this->m_strCustomerFullName = $strCustomerFullName;
	}

	public function setJobName( $strJobName ) {
		$this->m_strJobName = $strJobName;
	}

	public function setIsOrganization( $intIsOrganization ) {
		$this->m_intIsOrganization = $intIsOrganization;
	}

	public function setOpenSubTaskCount( $intOpenSubTaskCount ) {
		$this->m_intOpenSubTaskCount = $intOpenSubTaskCount;
	}

	public function setCreatedCustomerId( $intCreatedCustomerId ) {
		$this->setDetailsField( 'created_customer_id', $intCreatedCustomerId );
	}

	public function setIsPrintedOn( $intIsPrintedOn ) {
		$this->m_intIsPrintedOn = $intIsPrintedOn;
	}

	/**
	 * Getters
	 *
	 */

	public function getMaintenanceStatusType() {
		return $this->m_objMaintenanceStatus;
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function getIsScheduledFromEntrataCalendar() {
		return $this->m_boolIsScheduledFromEntrataCalendar;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getIsShowIntegrationError() {
		return $this->m_boolIsShowIntegrationError;
	}

	public function getMaintenanceRequests() {
		return $this->m_arrobjMaintenanceRequests;
	}

	public function getIsMultipleMaintenanceRequest() {
		return $this->m_boolIsMultipleMaintenanceRequest;
	}

	public function getIsPropertyBuilding() {
		return $this->m_boolIsPropertyBuilding;
	}

	public function getIsValidateCallId() {
		return $this->m_boolIsValidateCallId;
	}

	public function getIsNotesVisibleToResident() {
		return $this->m_boolIsNotesVisibleToResident;
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getRequestedTime() {
		return $this->m_strRequestedTime;
	}

	public function getRequestedDay() {
		return $this->m_strRequestedDay;
	}

	public function getMaintenancePriorityName() {
		return $this->m_strMaintenancePriorityName;
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getDaysToComplete() {
		return $this->m_intDaysToComplete;
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function setDaysToComplete( $intDaysToComplete ) {
		$this->m_intDaysToComplete = $intDaysToComplete;
	}

	public function getMaintenanceStatusTypeId() {
		return $this->m_intMaintenanceStatusTypeId;
	}

	public function getMaintenanceCategoryId() {
		return $this->m_intMaintenanceCategoryId;
	}

	public function getMaintenanceCategoryName() {
		return $this->m_strMaintenanceCategoryName;
	}

	public function getWorkOrderCreatedBy() {
		return $this->m_strWorkOrderCreatedBy;
	}

	public function getDaysRemaining() {
		return $this->m_intDaysRemaining;
	}

	public function getUnitSpaceStatusTypeName() {
		return $this->m_strUnitSpaceStatusTypeName;
	}

	public function getUnitSpaceStatusTypeId() {
		return $this->m_intUnitSpaceStatusTypeId;
	}

	public function getPropertyFloorPlanName() {
		return $this->m_strPropertyFloorPlanName;
	}

	public function getMaintenanceLocationType() {
		return $this->m_strMaintenanceLocationType;
	}

	public function getBuildingNumber() {
		return $this->m_strBuildingNumber;
	}

	public function getCompanyUserNameFirst() {
		return $this->m_strCompanyUserNameFirst;
	}

	public function getCompanyUserNameLast() {
		return $this->m_strCompanyUserNameLast;
	}

	public function getResidentStatusTypeName() {
		return $this->m_strResidentStatusTypeName;
	}

	public function getMaintenanceChargeAmount() {
		return $this->m_arrmixChargeAmounts;
	}

	public function getCompanyUserUsername() {
		return $this->m_strCompanyUserUsername;
	}

	public function getMaintenanceProblemName() {
		return $this->m_strMaintenanceProblemName;
	}

	public function getMaintenanceProblemActionName() {
		return $this->m_strMaintenanceProblemActionName;
	}

	public function getIsPublishedProblemAction() {
		return $this->m_boolIsPublishedProblemAction;
	}

	public function getIsCommercialProperty() {
		return $this->m_boolIsCommercialProperty;
	}

	public function getIsValidateScheduledEndDate() {
		return $this->m_boolIsValidateScheduledEndDate;
	}

	public function getMakeReadyMaintenanceRequestStatus() {
		return $this->m_strMakeReadyMaintenanceRequestStatus;
	}

	public function getMaintenanceLabor() {
		return $this->m_objLabor;
	}

	public function getMaintenanceMaterial() {
		return $this->m_objMaterial;
	}

	public function getMaintenanceAttachment() {
		return $this->m_objAttachment;
	}

	public function getOrigin() {
		return $this->m_strOrigin;
	}

	public function getMaintenanceRequestType() {
		return $this->m_strMaintenanceRequestType;
	}

	public function getMaintenanceStatusName() {
		return $this->m_strMaintenanceStatusName;
	}

	public function getMaintenanceStatusTypeName() {
		return $this->m_strMaintenanceStatusTypeName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getCustomSuccessMessage() {
		return $this->m_strCustomSuccessMessage;
	}

	public function getMaintenancePriorityTypeId() {
		return $this->m_intMaintenancePriorityTypeId;
	}

	public function getAlarmCode() {
		return $this->m_strAlarmCode;
	}

	public function getMaintenanceLocationName() {
		return $this->m_strMaintenanceLocationName;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getLease() {
		return $this->m_objLease;
	}

	public function getMaintenanceTemplate() {
		return $this->m_objMaintenanceTemplate;
	}

	public function getUserDepartmentId() {
		return $this->m_intUserDepartmentId;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getInspectionId() {
		return $this->m_intInspectionId;
	}

	public function getIsFloatingMaintenanceRequest() {
		return $this->m_boolIsFloatingMaintenanceRequest;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getSpaceNumber() {
		return $this->m_strSpaceNumber;
	}

	public function getMobilePhoneNumber() {
		return $this->m_strMobilePhoneNumber;
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function getRequestDateTime() {
		return $this->m_strRequestDateTime;
	}

	public function getParentMaintenanceRequestId() {
		return $this->m_intParentMaintenanceRequestId;
	}

	public function getCheckExistingMaintenanceStatus() {
		return $this->m_boolCheckExistingMaintenanceStatus;
	}

	public function getIsEditMaintenanceRequest() {
		return $this->m_boolIsEditMaintenanceRequest;
	}

	public function getMaintenanceMileage() {
		return $this->m_objMileage;
	}

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	public function getCalendarEventId() {
		return $this->m_intCalendarEventId;
	}

	public function getChargeCodeName() {
		return $this->m_strChargeCodeName;
	}

	public function getApPayeeCompanyName() {
		return $this->m_strApPayeeCompanyName;
	}

	public function getOrFetchProperty( $objDatabase ) {

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		}

		if( 0 < ( int ) $this->getPropertyId() ) {
			$this->m_objProperty = $this->fetchProperty( $objDatabase );

			return $this->m_objProperty;
		}

		return NULL;
	}

	public function getOrFetchLease( $objDatabase ) {

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {
			return $this->m_objLease;
		}

		if( 0 < ( int ) $this->m_intLeaseId ) {
			$this->m_objLease = $this->fetchLease( $objDatabase );

			return $this->m_objLease;
		}

		return NULL;
	}

	public function getOrFetchCustomer( $objDatabase ) {

		if( true == valObj( $this->m_objCustomer, 'CCustomer' ) ) {
			return $this->m_objCustomer;
		}

		if( 0 < ( int ) $this->m_intCustomerId ) {
			$this->m_objCustomer = $this->fetchCustomer( $objDatabase );

			return $this->m_objCustomer;
		}

		return NULL;
	}

	public function getOrFetchMaintenanceStatus( $objDatabase ) {
		if( false == valObj( $this->m_objMaintenanceStatus, 'CMaintenanceStatus' ) ) {
			$this->m_objMaintenanceStatus = CMaintenanceStatuses::createService()->fetchMaintenanceStatusByIdByCid( $this->getMaintenanceStatusId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objMaintenanceStatus;
	}

	public function getOrFetchUnitSpace( $objDatabase ) {

		if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) {
			return $this->m_objUnitSpace;
		}

		if( 0 < ( int ) $this->m_intUnitSpaceId ) {
			$this->m_objUnitSpace = $this->fetchUnitSpace( $objDatabase );

			return $this->m_objUnitSpace;
		}

		return NULL;
	}

	public function getOrFetchMaintenanceTemplate( $objDatabase ) {

		if( true == valObj( $this->m_objMaintenanceTemplate, 'CMaintenanceTemplate' ) ) {
			return $this->m_objMaintenanceTemplate;
		}

		if( 0 < ( int ) $this->m_intMaintenanceTemplateId ) {
			$this->m_objMaintenanceTemplate = $this->fetchMaintenanceTemplate( $objDatabase );

			return $this->m_objMaintenanceTemplate;
		}

		return NULL;
	}


	public function getIsVisibleToResident() {
		return $this->m_intIsVisibleToResident;
	}

	public function getAsset() {
		return $this->m_objAsset;
	}

	public function getLeaseAction() {
		return $this->m_strLeaseAction;
	}

	public function getMaintenanceRequestResponseTypeId() {
		return $this->m_intMaintenanceRequestResponseTypeId;
	}

	public function getResponseText() {
		return $this->m_strResponseText;
	}

	public function getResponseDatetime() {
		return $this->m_strResponseDatetime;
	}

	public function getCalendarEventStartDateTime() {
		return $this->m_strCalendarEventStartDatetime;
	}

	public function getCalendarEventEndDateTime() {
		return $this->m_strCalendarEventEndDatetime;
	}

	public function getIsClientProblem() {
		return $this->m_boolIsClientProblem;
	}

	public function getMakeReadyDaysToComplete() {
		return $this->m_intMakeReadyDaysToComplete;
	}

	public function getFiles() {
		return $this->m_arrobjFiles;
	}

	public function getParentActualStartDatetime() {
		return $this->m_strParentActualStartDatetime;
	}

	public function getEntryNotes() {
		return $this->m_strEntryNotes;
	}

	public function getClosingNote() {
		return $this->m_strClosingNote;
	}

	public function getResidentSMS() {
		return $this->m_strResidentSMS;
	}

	public function getMakeReadyExcludeClosedDays() {
		return $this->m_boolIsMakeReadyExcludeClosedDays;
	}

	public function getMaintenanceExceptionType() {
		return $this->m_strMaintenanceExceptionType;
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function getCustomerFullName() {
		return $this->m_strCustomerFullName;
	}

	public function getJobName() {
		return $this->m_strJobName;
	}

	public function getIsOrganization() {
		return $this->m_intIsOrganization;
	}

	public function getOpenSubTaskCount() {
		return $this->m_intOpenSubTaskCount;
	}

	public function getCreatedCustomerId() {
		return $this->getDetailsField( 'created_customer_id' );
	}

	public function getIsPrintedOn() {
		return $this->m_intIsPrintedOn;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchOutstandingInspectionsByPropertyUnitIdOrLeaseIdByCid( $objDatabase, $intLimit = NULL ) {
		return \Psi\Eos\Entrata\CInspections::createService()->fetchOutstandingInspectionsByPropertyUnitIdOrLeaseIdByCid( $this->getPropertyUnitId(), $this->getLeaseId(), $this->getCid(), $objDatabase, $intLimit, $this->getUnitSpaceId() );
	}

	public function fetchCompanyEmployee( $objDatabase ) {

		if( false != valId( $this->getCompanyEmployeeId() ) ) {

			return CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
		}

		return NULL;
	}

	public function fetchCustomer( $objDatabase ) {

		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->m_intCustomerId, $this->m_intCid, $objDatabase );
	}

	public function fetchMaintenanceProblem( $objDatabase ) {

		return CMaintenanceProblems::createService()->fetchMaintenanceProblemByIdByCid( $this->m_intMaintenanceProblemId, $this->getCid(), $objDatabase );
	}

	public function fetchSubMaintenanceProblem( $objDatabase ) {

		return CMaintenanceProblems::createService()->fetchMaintenanceProblemByIdByCid( $this->m_intSubMaintenanceProblemId, $this->getCid(), $objDatabase );
	}

	public function fetchMaintenancePriority( $objDatabase ) {

		if( false != valId( $this->m_intMaintenancePriorityId ) ) {

			return \Psi\Eos\Entrata\CMaintenancePriorities::createService()->fetchMaintenancePriorityByIdByCid( $this->m_intMaintenancePriorityId, $this->getCid(), $objDatabase );
		}

		return NULL;
	}

	public function fetchMaintenanceLocation( $objDatabase ) {

		if( false != valId( $this->m_intMaintenanceLocationId ) ) {

			return \Psi\Eos\Entrata\CMaintenanceLocations::createService()->fetchMaintenanceLocationByIdByCid( $this->m_intMaintenanceLocationId, $this->getCid(), $objDatabase );
		}

		return NULL;
	}

	public function fetchMaintenanceRequestsByLocations( $objDatabase ) {

		return CMaintenanceRequests::createService()->fetchMaintenanceRequestsByCompanyLocationIdByCid( $this->m_intMaintenanceLocationId, $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceRequestsByProblems( $objDatabase ) {

		if( false != valId( $this->m_intMaintenanceProblemId ) ) {

			return CMaintenanceRequests::createService()->fetchMaintenanceRequestsByCompanyProblemIdByCid( $this->m_intMaintenanceProblemId, $this->getCid(), $objDatabase );
		}

		return NULL;
	}

	public function fetchMaintenanceStatus( $objDatabase ) {

		if( false != valId( $this->m_intMaintenanceStatusId ) ) {

			return CMaintenanceStatuses::createService()->fetchMaintenanceStatusByIdByCid( $this->m_intMaintenanceStatusId, $this->m_intCid, $objDatabase );
		}

		return NULL;
	}

	public function fetchClient( $objDatabase ) {

		return CClients::fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		return CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchLease( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchNonDeletedLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
	}

	public function fetchCurrentLease( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnit( $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceRequestMaterialsWithApCodes( $objDatabase, $boolIsPosted = NULL ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestMaterials::createService()->fetchCustomMaintenanceRequestMaterialsWithApCodesByMaintenanceRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIsPosted );
	}

	public function fetchMaintenanceRequestMaterialByMaterialId( $intMaterialId, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestMaterials::createService()->fetchMaintenanceRequestMaterialByIdByCid( $intMaterialId, $this->getCid(), $objDatabase );
	}

	public function fetchUnitSpace( $objDatabase ) {
		return CUnitSpaces::createService()->fetchUnitSpaceByIdByCid( $this->getUnitSpaceId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceTemplate( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidById( $this->getCid(), $this->getMaintenanceTemplateId(), $objDatabase );
	}

	public function fetchMaintenanceRequestAttachments( $objDatabase, $boolIsFromChargesTab = false, $boolIsShowInPortal = false ) {
		return CFileAssociations::createService()->fetchMaintenanceRequestFileAssociationsByCidByMaintenanceRequestId( $this->getId(), $this->getCid(), $objDatabase, $boolIsFromChargesTab, $boolIsShowInPortal );
	}

	public function fetchSubMaintenanceRequestIds( $objDatabase, $boolIncludeDeletedSubMaintenanceRequests = false ) {
		return CMaintenanceRequests::createService()->fetchSubMaintenanceRequestIdsByMaintenanceRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIncludeDeletedSubMaintenanceRequests );
	}

	public function fetchScheduledChargesByArTriggerIdsByMaintenanceRequestIds( $arrintArTriggerIds, $arrintMaintenanceRequestIds, $arrintPropertyIds, $objDatabase, $boolGetCount = NULL, $boolFetchOpenScheduledCharges = false ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchMaintenanceRequestScheduledChargesByArTriggerIdsByMaintenanceRequestIdsByCid( $arrintArTriggerIds, $arrintMaintenanceRequestIds, $arrintPropertyIds, $this->getCid(), $objDatabase, $boolGetCount, $boolFetchOpenScheduledCharges );
	}

	public function fetchFileAssociationsCount( $objDatabase ) {
		return CFileAssociations::createService()->fetchMaintenanceRequestFileAssociationsCountByMaintenanceRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceRequestLaborsByMaintenanceRequestId( $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestLabors::createService()->fetchMaintenanceRequestLaborsByMaintenanceRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchMaintenanceRequestMaterialsByMaterialIds( $arrintMaterialIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMaintenanceRequestMaterials::createService()->fetchMaintenanceRequestMaterialsByIdsByMaintenanceRequestIdByCid( $arrintMaterialIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			trigger_error( __( 'A Client Id is required - {%s, 0}', [ 'CMaintenanceRequest' ] ), E_USER_ERROR );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valApPayeeId() {
		$boolIsValid = true;
		if( false == valId( $this->m_intApPayeeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', __( 'Vendor is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_intCompanyEmployeeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', __( 'Company employee is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == valId( $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'A Property is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intLeaseId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'A Lease is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		if( false == valId( $this->m_intCustomerId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Resident is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMaintenanceRequest() {
		$boolIsValid = true;
		if( true == valId( $this->m_intCustomerId ) && false == valId( $this->m_intLeaseId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'A Lease is required.' ) ) );
			$boolIsValid &= false;
		}

		if( false == valId( $this->m_intCustomerId ) && true == valId( $this->m_intLeaseId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Resident is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valCallCenterMaintenanceRequest() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCustomerId ) && false == valId( $this->m_intLeaseId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Resident is required.' ) ) );
			$boolIsValid &= false;
		} elseif( false == valId( $this->m_intLeaseId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'A Lease is required.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valMaintenancePriorityId( $objProperty = NULL, $objDatabase = NULL, $boolIsMaintenancePriority = NULL ) {
		$boolIsValid = true;
		if( true == valObj( $objProperty, 'CProperty' ) && false == is_null( $objDatabase ) ) {

			$arrobjPropertyPreferences = $objProperty->getOrFetchPropertyPreferences( $objDatabase );

			if( ( true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'HIDE_MAINTENANCE_PRIORITY', $arrobjPropertyPreferences ) ) ) || false == $boolIsMaintenancePriority ) {
				return $boolIsValid;
			} elseif( false == isset( $this->m_intMaintenancePriorityId ) && true == $boolIsMaintenancePriority ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Priority is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaintenanceLocationId( $objProperty = NULL, $objDatabase = NULL, $boolUseCategoryProblemAction = false ) {
		$boolIsValid = true;
		if( true == valObj( $objProperty, 'CProperty' ) && false == is_null( $objDatabase ) && true == $boolUseCategoryProblemAction ) {

			$objPropertyPreference = $objProperty->fetchPropertyPreferenceByKey( 'USE_CATEGORY_PROBLEM_ACTION', $objDatabase );

			if( ( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) && 1 == $objPropertyPreference->getValue() ) {
				return $boolIsValid;
			} elseif( false == isset( $this->m_intMaintenanceLocationId ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Location of problem is required.' ) ) );
			}
		} elseif( true == valObj( $objProperty, 'CProperty' ) && false == is_null( $objDatabase ) && false == $boolUseCategoryProblemAction ) {
			$objPropertyPreference = $objProperty->fetchPropertyPreferenceByKey( 'HIDE_LOCATIONS', $objDatabase );

			if( ( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) && 1 == $objPropertyPreference->getValue() ) {
				return $boolIsValid;
			} elseif( false == isset( $this->m_intMaintenanceLocationId ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Location of problem is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaintenanceProblemId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intMaintenanceProblemId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Problem is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceLocationCategoryProblemId( $objProperty = NULL, $objDatabase = NULL, $boolUseCategoryProblemAction = false ) {
		$objPropertyPreference     = NULL;
		$arrobjPropertyPreferences = [];

		$boolIsValid         = true;
		$boolIsLocationValid = true;
		$boolIsCategoryValid = true;

		if( true == valObj( $objProperty, 'CProperty' ) ) {

			$boolIsLocationValid = $this->valMaintenanceLocationId( $objProperty, $objDatabase, $boolUseCategoryProblemAction );
			$boolIsValid         &= $boolIsLocationValid;

			$arrobjPropertyPreferences = $objProperty->getOrFetchPropertyPreferences( $objDatabase );

			if( true == valArr( $arrobjPropertyPreferences ) ) {
				$objPropertyPreference = getArrayElementByKey( 'USE_CATEGORY_PROBLEM_ACTION', $arrobjPropertyPreferences );
			}
		}

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && $boolUseCategoryProblemAction == true ) {

			if( false == is_numeric( $this->getMaintenanceCategoryId() ) ) {
				$boolIsCategoryValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_category_id', __( 'Please select a category for the list of problems.' ) ) );
			} elseif( false == valId( $this->m_intMaintenanceProblemId ) ) {
				$boolIsValid &= $this->valMaintenanceProblemId();
			}

		} elseif( false == valId( $this->m_intMaintenanceProblemId ) && true == $boolIsLocationValid ) {

			$boolIsValid &= $this->valMaintenanceProblemId();
		}

		$boolIsValid &= $boolIsCategoryValid;

		return $boolIsValid;
	}

	public function valUnitNumber() {
		$boolIsValid = true;
		if( false == valStr( $this->m_strUnitNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', __( 'Unit number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCallCenterUnitNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strUnitNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', __( 'Unit number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyBuildingId() {
		$boolIsValid = true;

		if( true == $this->getIsPropertyBuilding() && false == valId( $this->getPropertyBuildingId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'building_id', __( 'Building is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledStartDatetime() {
		$boolIsValid = true;

		if( true == isset( $this->m_strScheduledStartDatetime ) && true == valStr( $this->m_strScheduledStartDatetime ) && 1 !== CValidation::checkDateTime( $this->m_strScheduledStartDatetime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Start date must be in correct date-time form.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDate( $strDateTime, $strDateType ) {
		$boolIsValid    = true;
		$arrintDateTime = explode( ' ', $strDateTime );
		$arrintDate     = explode( '-', $arrintDateTime[0] );

		if( true == isset( $strDateTime ) && true == valStr( $strDateTime ) && 1 != checkdate( $arrintDate[1], $arrintDate[2], $arrintDate[0] ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Invalid {%s, 0}.', [ $strDateType ] ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledEndDatetime() {
		$boolIsValid = true;

		if( true == isset( $this->m_strScheduledEndDatetime ) && true == valStr( $this->m_strScheduledEndDatetime ) && 1 !== CValidation::checkDateTime( $this->m_strScheduledEndDatetime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', __( 'End date must be in correct date-time form.' ) ) );
		}

		if( false == is_null( $this->getScheduledStartDatetime() ) && false == is_null( $this->getScheduledEndDatetime() ) ) {
			if( ( strtotime( $this->getScheduledStartDatetime() ) ) > ( strtotime( $this->getScheduledEndDatetime() ) ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_datetime', __( 'End date cannot be earlier than Start date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valOverviewScheduledEndDatetime() {
		$boolIsValid = true;
		if( true == valId( $this->getCalendarEventId() ) ) {
			return $boolIsValid;
		}
		if( true == isset( $this->m_strScheduledEndDatetime ) && true == valStr( $this->m_strScheduledEndDatetime ) && 1 !== CValidation::checkDateTime( $this->m_strScheduledEndDatetime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_datetime', __( 'End date must be in correct date-time form.' ) ) );

			return $boolIsValid;
		}

		if( true == valStr( $this->getScheduledStartDatetime() ) && true == valStr( $this->getScheduledEndDatetime() ) ) {
			if( ( strtotime( $this->getScheduledStartDatetime() ) ) > ( strtotime( $this->getScheduledEndDatetime() ) ) ) {
				$boolIsValid &= false;
				$this->setScheduledEndDatetime( NULL );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_datetime', __( 'Scheduled end date must be after scheduled start date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valOverviewStartEndDateWithSubTaskEndDate( $objDatabase ) {
		$boolIsValid                     = true;
		$strSubTaskHighestEndDate        = NULL;
		$strSubTaskLowestEndDate         = NULL;
		$strParentScheduledStartDatetime = NULL;
		$strParentScheduledEndDatetime   = NULL;

		if( true == valStr( $this->getScheduledStartDatetime() ) ) {
			$strParentScheduledStartDatetime = date( 'Y-m-d', strtotime( $this->getScheduledStartDatetime() ) );
		}

		if( true == valStr( $this->getScheduledEndDatetime() ) ) {
			$strParentScheduledEndDatetime = date( 'Y-m-d', strtotime( $this->getScheduledEndDatetime() ) );
		}

		if( true == valId( $this->getCalendarEventId() ) ) {
			return $boolIsValid;
		}

		// get highest sub task end date
		$this->m_arrobjSubMaintenanceRequest = CMaintenanceRequests::createService()->fetchSubMaintenanceRequestHighestOrLowestEndDateByTypeByMaintenanceRequestIdByCid( true, $this->getId(), $this->getCid(), $objDatabase );
		if( false == is_null( $this->m_arrobjSubMaintenanceRequest ) ) {
			foreach( $this->m_arrobjSubMaintenanceRequest as $objSubMaintenanceRequest ) {
				$strSubTaskHighestEndDate = date( 'Y-m-d', strtotime( $objSubMaintenanceRequest->getScheduledEndDatetime() ) );
			}
		}

		$this->m_arrobjSubMaintenanceRequest = CMaintenanceRequests::createService()->fetchSubMaintenanceRequestHighestOrLowestEndDateByTypeByMaintenanceRequestIdByCid( false, $this->getId(), $this->getCid(), $objDatabase );
		if( false == is_null( $this->m_arrobjSubMaintenanceRequest ) ) {
			foreach( $this->m_arrobjSubMaintenanceRequest as $objSubMaintenanceRequest ) {
				$strSubTaskLowestEndDate = date( 'Y-m-d', strtotime( $objSubMaintenanceRequest->getScheduledEndDatetime() ) );
			}
		}

		if( true == valStr( $strSubTaskLowestEndDate ) && true == valStr( $strParentScheduledStartDatetime ) ) {
			if( strtotime( $strParentScheduledStartDatetime ) > strtotime( $strSubTaskLowestEndDate ) ) {
				$boolIsValid &= false;
				$this->setScheduledEndDatetime( NULL );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_datetime', __( 'Scheduled start date must be less than or equal to lowest sub task end date!' ) ) );
			}
		}

		if( true == valStr( $strSubTaskHighestEndDate ) && true == valStr( $strParentScheduledEndDatetime ) ) {
			if( ( strtotime( $strParentScheduledEndDatetime ) ) < ( strtotime( $strSubTaskHighestEndDate ) ) ) {
				$boolIsValid &= false;
				$this->setScheduledEndDatetime( NULL );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_datetime', __( 'Scheduled end date must be greater than or equal to highest sub task end date!' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valStartEndDateWithSubTaskEndDate( $strParentScheduledEndDatetime = NULL, $strParentScheduledStartDatetime = NULL, $arrmixSubTask = [] ) {
		$boolIsValid              = true;
		$strSubTaskHighestEndDate = NULL;
		$strSubTaskLowestEndDate  = NULL;

		if( true == isset( $arrmixSubTask ) && false == empty( $arrmixSubTask ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrmixSubTask ) ) {
			// get temp sub task lowest and highest end dates
			$arrstrTempSubTaskEndDates = [];
			foreach( $arrmixSubTask as $arrobjTempSubTask ) {
				foreach( $arrobjTempSubTask as $objTempSubTask ) {
					$strTempSubTaskEndDate = $objTempSubTask->getScheduledEndDateTime();
					if( true == valStr( $strTempSubTaskEndDate ) && false == empty( $strTempSubTaskEndDate ) ) {
						if( false == in_array( $objTempSubTask->getScheduledEndDateTime(), $arrstrTempSubTaskEndDates ) ) {
							$arrstrTempSubTaskEndDates[] = $objTempSubTask->getScheduledEndDateTime();
						}
					}
				}
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrTempSubTaskEndDates ) ) {
				$strSubTaskLowestEndDate  = min( $arrstrTempSubTaskEndDates );
				$strSubTaskHighestEndDate = max( $arrstrTempSubTaskEndDates );

				if( true == valStr( $strSubTaskLowestEndDate ) && true == valStr( $strParentScheduledStartDatetime ) ) {
					if( strtotime( $strParentScheduledStartDatetime ) > strtotime( $strSubTaskLowestEndDate ) ) {
						$boolIsValid &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_datetime', __( 'Scheduled Start date must be less than or equal to lowest sub task end date.' ) ) );
					}
				}
				if( true == valStr( $strSubTaskHighestEndDate ) && true == valStr( $strParentScheduledEndDatetime ) ) {
					if( ( strtotime( $strParentScheduledEndDatetime ) ) < ( strtotime( $strSubTaskHighestEndDate ) ) ) {
						$boolIsValid &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_datetime', __( 'Scheduled End date must be greater than or equal to highest sub task end date.' ) ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valSubTaskScheduledEndDatetime( $strParentScheduledEndDatetime = NULL, $strParentScheduledStartDatetime = NULL ) {
		$boolIsValid = true;

		if( true == valStr( $this->m_strScheduledEndDatetime ) ) {
			$this->m_strScheduledEndDatetime = date( 'm/d/Y', strtotime( $this->m_strScheduledEndDatetime ) );
			if( true == valStr( $strParentScheduledStartDatetime ) ) {
				$strParentScheduledStartDatetime = date( 'm/d/Y', strtotime( $strParentScheduledStartDatetime ) );
			}
		}

		if( true == isset( $this->m_strScheduledEndDatetime ) && true == valStr( $this->m_strScheduledEndDatetime ) && 1 !== CValidation::checkDate( $this->m_strScheduledEndDatetime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_datetime', __( 'End date must be in correct date form.' ) ) );

			return $boolIsValid;
		}

		if( true == isset( $this->m_strScheduledEndDatetime ) && true == valStr( $this->m_strScheduledEndDatetime ) && true == valStr( $strParentScheduledStartDatetime ) ) {
			if( ( strtotime( $this->m_strScheduledEndDatetime ) ) < ( strtotime( $strParentScheduledStartDatetime ) ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_datetime', __( 'Subtask scheduled end date must be greater than or equal to the parent scheduled start date.' ) ) );
			}
		}
		if( true == isset( $this->m_strScheduledEndDatetime ) && true == valStr( $this->m_strScheduledEndDatetime ) && true == valStr( $strParentScheduledEndDatetime ) ) {
			if( ( strtotime( $this->m_strScheduledEndDatetime ) ) > ( strtotime( $strParentScheduledEndDatetime ) ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_datetime', __( 'Subtask scheduled end date must be smaller than or equal to the parent scheduled end date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valCompletedDatetime() {

		$boolIsValid = true;

		if( true == isset( $this->m_strCompletedDatetime ) && 0 < strlen( $this->m_strCompletedDatetime ) && 1 !== CValidation::checkDateTime( $this->m_strCompletedDatetime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'completed_date', __( 'Completed date must be in correct date-time form.' ) ) );
		}

		if( true == isset( $this->m_strCompletedDatetime ) && true == isset( $this->m_strActualStartDatetime ) && strtotime( $this->m_strActualStartDatetime ) > strtotime( $this->m_strCompletedDatetime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'completed_date', __( 'Completed date must be greater than creation date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress( $boolIsRequired = false ) {
		$boolIsValid = true;

		if( false == $boolIsRequired ) {
			if( true == valStr( $this->getEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_email_address', __( 'Email address is not correct.' ) ) );
			}
		} else {
			if( false == valStr( $this->getEmailAddress() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_email_address', __( 'Email address is required.' ) ) );
			} else {
				if( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_email_address', __( 'Email address is not correct.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valCustomerNameFirst() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strCustomerNameFirst ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_name_first', __( 'Resident first name is required.' ), 502 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCustomerNameLast() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strCustomerNameLast ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_name_last', __( 'Resident last name is required.' ), 503 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMainPhoneNumber( $boolIsRequired = true, $boolFullValidation = true ) {

		$boolIsValid = true;

		$objPhoneNumber = $this->createPhoneNumber( $this->m_strMainPhoneNumber );

		if( false == $boolIsRequired ) {
			if( true == $boolFullValidation && 0 < strlen( trim( $this->getMainPhoneNumber() ) ) && false == $objPhoneNumber->isValid() ) {
				$boolIsValid = false;
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'main_phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ) ) );
			}

			return $boolIsValid;
		}

		if( $this->m_strMainPhoneNumber == NULL ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'main_phone_number', __( 'Primary phone is required.' ), 504 ) );
			$boolIsValid = false;

		} elseif( CPhoneNumberType::MOBILE == $this->getMainPhoneNumberTypeId() ) {
			if( 7 > \Psi\CStringService::singleton()->strlen( $this->getMainPhoneNumber() ) || 12 < \Psi\CStringService::singleton()->strlen( $this->getMainPhoneNumber() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'main_phone_number', __( 'Primary phone number must be between 7 and 12 characters.' ) ) );
			}
		} elseif( true == $boolFullValidation && false == $objPhoneNumber->isValid() ) {
			// It has to be > 0 long
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'main_phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valAltPhoneNumber( $boolIsRequired = true, $boolIsFullValidation = true ) {

		$boolIsValid = true;

		$objPhoneNumber = $this->createPhoneNumber( $this->m_strAltPhoneNumber );

		if( false == $boolIsRequired ) {
			if( true == $boolIsFullValidation && 0 < strlen( trim( $this->m_strAltPhoneNumber ) ) && false == $objPhoneNumber->isValid() ) {
				$boolIsValid = false;
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'alt_phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ) ) );
			}

			return $boolIsValid;
		}

		if( false == valStr( $this->m_strAltPhoneNumber ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alt_phone_number', __( 'Alternate phone is required.' ), 504 ) );
			$boolIsValid = false;

		} elseif( CPhoneNumberType::MOBILE == $this->getAltPhoneNumberTypeId() ) {
			if( 7 > \Psi\CStringService::singleton()->strlen( $this->m_strAltPhoneNumber ) || 12 < \Psi\CStringService::singleton()->strlen( $this->m_strAltPhoneNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alt_phone_number', __( 'Alternate phone number must be between 7 and 12 characters.' ) ) );

			}
		} else {
			if( true == $boolIsFullValidation && false == $objPhoneNumber->isValid() ) {
				// It has to be > 0 long
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'alt_phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valProblemDescription() {

		$boolIsValid = true;
		if( $this->getParentMaintenanceRequestId() == NULL ) {
			if( false == valStr( $this->getProblemDescription() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'problem_description', __( 'Problem description is required.' ), 501 ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valActualStartDateTime() {
		$boolIsValid = true;

		if( true == valStr( $this->m_strActualStartDatetime ) ) {
			if( strtotime( $this->m_strActualStartDatetime ) > strtotime( 'now' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_datetime', __( 'Created on date time should be less than or equal to current date time.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valEntryNote() {
		$boolIsValid = true;

		if( false == is_null( $this->m_intPermissionToEnter ) && false == $this->m_intPermissionToEnter && true == is_null( $this->m_strEntryNotes ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entry_note', __( 'Entry note is required.' ), 507 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valAssetId( $objDatabase ) {

		$boolIsValid = true;

		if( false == is_null( $objDatabase ) && true == valId( $this->getAssetId() ) && 0 >= \Psi\Eos\Entrata\CAssetDetails::createService()->fetchIsRetiredAssetDetailCount( $this->getAssetId(), $this->getCid(), $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'asset_id', __( 'Selected asset has been retired.' ), 501 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPermissionToEnter( $objProperty = NULL, $objDatabase = NULL, $boolIsReferPropertyPreference = true ) {
		$boolIsValid = true;
		if( true == $boolIsReferPropertyPreference && true == valObj( $objProperty, 'CProperty' ) && false == is_null( $objDatabase ) ) {
			$objPropertyPreference = $objProperty->fetchPropertyPreferenceByKey( 'PERMISSION_TO_ENTER', $objDatabase );
			if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && ( ( self::PERMISSION_TO_ENTER_MENDATORY == $objPropertyPreference->getValue() && true == empty( $this->m_intPermissionToEnter ) ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'permission_to_enter', __( 'Permission to enter is required.' ), 507 ) );
				$boolIsValid = false;
			} else {
				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && ( ( self::PERMISSION_TO_ENTER_REQUIRE_RESPONSE == $objPropertyPreference->getValue() && true == is_null( $this->m_intPermissionToEnter ) ) ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'permission_to_enter', __( 'Please select if you agree to allow the property staff to enter your unit.' ), 507 ) );
					$boolIsValid = false;
				}
			}
		} elseif( false == $boolIsReferPropertyPreference && ( true == is_null( $this->m_intPermissionToEnter ) || false == valStr( $this->m_intPermissionToEnter ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'permission_to_enter', __( 'Permission to enter is required.' ), 507 ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceStatusId() {

		$boolIsValid = true;

		if( false == valId( $this->getMaintenanceStatusId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_id', __( 'Request status is required.' ) ) );
		} else {
			if( true == valId( $this->getMaintenanceStatusTypeId() ) && 0 == $this->getMaintenanceStatusTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_id', __( 'Request status is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valCallId() {

		$boolIsValid = true;

		if( true == $this->getIsValidateCallId() && 0 == strlen( $this->getCallId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_id', __( 'Call ID Number is required.' ) ) );
		} elseif( 0 < strlen( $this->getCallId() ) && false == is_numeric( $this->getCallId() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_id', __( 'Call ID Number should be numeric.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSubTaskStatus( $objDatabase ) {
		$boolIsValid = true;

		$objMaintenanceStatus = CMaintenanceStatuses::createService()->fetchMaintenanceStatusByIdByCid( $this->m_intMaintenanceStatusId, $this->getCid(), $objDatabase );

		if( true == valObj( $objMaintenanceStatus, 'CMaintenanceStatus' ) && true == in_array( $objMaintenanceStatus->getMaintenanceStatusTypeId(), CMaintenanceStatusType::$c_arrintClosedStatusTypes ) ) {
			$this->m_arrobjSubMaintenanceRequest = CMaintenanceRequests::createService()->fetchSubMaintenanceRequestsByMaintenanceRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase, true );
			if( true == valArr( $this->m_arrobjSubMaintenanceRequest ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_id', __( 'Maintenance request : {%d, 0, nots} All subtasks must be completed before you can close the main work order.', [ $this->getId() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valRequestedDatetime( $strRequestedDate = NULL, $strRequestedTime = NULL ) {
		$boolIsValid = true;

		if( true == valStr( $strRequestedDate ) && false == valStr( $strRequestedTime ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_datetime', __( 'Requested Time is required' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMakeReadyMaintenanceRequests( $strParentScheduledEndDatetime, $strMRMoveOutDate, $objDatabase ) {
		if( true == $this->getCustomerId() ) {
			$arrmixMakeReadyMaintenanceRequest = CMaintenanceRequests::createService()->fetchMakeReadyWorkOrderMaintenanceRequestByLeaseIdByCustomerIdByPropertyIdByCid( $this->getLeaseId(), $this->getCustomerId(), $this->getPropertyId(), $this->getCid(), $objDatabase, true );
			if( true == valArr( $arrmixMakeReadyMaintenanceRequest ) ) {
				$strMessage = '<a href="javascript:void(0)" onclick="loadMakeReadyMaintenanceRequest( ' . $arrmixMakeReadyMaintenanceRequest['id'] . ' ); return false;" class="action"> ' . $arrmixMakeReadyMaintenanceRequest['id'] . ' </a>';
				if( true == $this->getIsCommercialProperty() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', __( '{%s, 0} is already open for this tenant.', [ $strMessage ] ) ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', __( '{%s, 0} is already open for this resident.', [ $strMessage ] ) ) );
				}

				return false;
			}
		}
		$arrmixMakeReadyMaintenanceRequest = CMaintenanceRequests::createService()->fetchMakeReadyWorkOrderMaintenanceRequestByUnitIdByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->getUnitSpaceId(), $objDatabase, true );
		if( true == valArr( $arrmixMakeReadyMaintenanceRequest ) ) {
			$strMessage = '<a href="javascript:void(0)" onclick="loadMakeReadyMaintenanceRequest( ' . $arrmixMakeReadyMaintenanceRequest['id'] . ' ); return false;" class="action"> ' . $arrmixMakeReadyMaintenanceRequest['id'] . ' </a>';
			if( true == $this->getIsCommercialProperty() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', __( '{%s, 0} is already open for this suite.', [ $strMessage ] ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', __( '{%s, 0} is already open for this unit.', [ $strMessage ] ) ) );
			}

			return false;
		} else {
			$arrmixMakeReadyMaintenanceRequest = CMaintenanceRequests::createService()->fetchMakeReadyWorkOrderMaintenanceRequestByLeaseIdByCustomerIdByPropertyIdByCid( $this->getLeaseId(), NULL, $this->getPropertyId(), $this->getCid(), $objDatabase, true );
			if( true == valArr( $arrmixMakeReadyMaintenanceRequest ) ) {
				$strMessage = '<a href="javascript:void(0)" onclick="loadMakeReadyMaintenanceRequest( ' . $arrmixMakeReadyMaintenanceRequest['id'] . ' ); return false;" class="action"> ' . $arrmixMakeReadyMaintenanceRequest['id'] . ' </a>';
				if( true == $this->getIsCommercialProperty() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', __( '{%s, 0} is already open for this suite.', [ $strMessage ] ) ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', __( '{%s, 0} is already open for this unit.', [ $strMessage ] ) ) );
				}

				return false;
			}
		}

		if( false == is_null( $strParentScheduledEndDatetime ) && false == is_null( $strMRMoveOutDate ) ) {
			$arrstrParentScheduledEndDatetime = explode( ' ', $strParentScheduledEndDatetime );
			$arrstrParentScheduledEndDatetime = explode( '-', $arrstrParentScheduledEndDatetime[0] );
			$strParentScheduledEndDatetime    = date( 'm/d/Y', mktime( 0, 0, 0, $arrstrParentScheduledEndDatetime[1], $arrstrParentScheduledEndDatetime[2], $arrstrParentScheduledEndDatetime[0] ) );

			if( strtotime( $strParentScheduledEndDatetime ) < strtotime( $strMRMoveOutDate ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_template_id', __( 'Scheduled End date should be greater than Move-out Date!' ) ) );

				return false;
			}
		}

		return true;
	}

	public function valDueDate() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strDueDate ) ) {
			$this->m_strDueDate = date( 'Y-m-d H:i:s', strtotime( $this->m_strDueDate ) );
		}

		if( true == isset( $this->m_strDueDate ) && 0 < strlen( $this->m_strDueDate ) && 1 !== CValidation::checkDateTime( $this->m_strDueDate ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', __( 'Due date must be in correct date form.' ) ) );
			$boolIsValid = false;
		} else {
			if( 0 == strlen( $this->m_strDueDate ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', __( 'Due Date is required.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valAssignedTo() {
		$boolIsValid = true;

		if( false == is_null( $this->getCompanyEmployeeId() ) && false == valId( $this->getCompanyEmployeeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'assigned_to', __( 'Invalid Employee Id.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valAction( $objDatabase ) {
		$boolIsValid = true;

		$arrintMaintenanceProblemTypes = [ CMaintenanceProblemType::INSPECTION_ACTION_REPAIR, CMaintenanceProblemType::INSPECTION_ACTION_REPLACE, CMaintenanceProblemType::INSPECTION_ACTION_CLEAN ];
		$arrmixData                    = CMaintenanceProblems::createService()->fetchMaintenanceProblemsByMaintenanceProblemIdByProblemTypesByPropertyIdByCid( $this->m_intMaintenanceProblemId, $arrintMaintenanceProblemTypes, $this->getPropertyId(), $this->getCid(), $objDatabase, true, true, true );

		if( true == valArr( $arrmixData ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'action', __( 'Action is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSubTaskActualStartDatetime() {
		$boolIsValid = true;

		if( true == isset( $this->m_strActualStartDatetime ) && true == valStr( $this->m_strActualStartDatetime ) && true == valStr( $this->m_strParentActualStartDatetime ) ) {
			if( strtotime( $this->m_strActualStartDatetime ) < strtotime( $this->m_strParentActualStartDatetime ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_start_datetime', __( 'Subtask created date should be greater than parent created date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaintenanceExceptionId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction, $objProperty = NULL, $objDatabase = NULL, $strParentScheduledEndDatetime = NULL, $strParentScheduledStartDatetime = NULL, $boolIsMakeReadyMaintenanceRequest = NULL, $strRequestedDate = NULL, $strRequestedTime = NULL, $boolIsProblemRequired = true, $strMRMoveOutDate = NULL, $boolIsValidateSubTask = true, $boolIsClientProblem = false, $arrmixSubTask = [], $boolIsDueDateRequired = false, $boolIsCompanyEmployeeRequired = false, $boolIsActionRequired = false, $boolIsMobileWorkorderWorkflow = false, $boolIsUserPermittedToCreateBackDateWorkOrder = false ) {

		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();

				if( false == $this->getIsFloatingMaintenanceRequest() ) {
					$boolIsValid &= $this->valMaintenanceRequest();
					$boolIsValid &= $this->valCustomerId();
				} else {
					$boolIsValid &= $this->valMainPhoneNumber( false, false );
					if( CPsProduct::API_SERVICES != $this->getPsProductId() ) {
						$boolIsValid &= $this->valAltPhoneNumber( false, false );
					}

					$boolIsValid &= $this->valEmailAddress();
				}

				$boolIsValid &= $this->valScheduledStartDatetime();
				$boolIsValid &= $this->valScheduledEndDatetime();
				$boolIsValid &= $this->valCompletedDatetime();
				$boolIsValid &= $this->valMaintenancePriorityId();
				$boolIsValid &= $this->valMaintenanceStatusId();

				if( false == $boolIsMakeReadyMaintenanceRequest ) {
					if( true == $boolIsClientProblem && true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
						$boolIsValid &= $this->valMaintenanceProblemId();
					} elseif( true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceLocationCategoryProblemId( $objProperty, $objDatabase );
					} else {
						$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
					}
				}

				$boolIsValid &= $this->valAssetId( $objDatabase );
				$boolIsValid &= $this->valProblemDescription();
				$boolIsValid &= $this->valActualStartDateTime();
				$boolIsValid &= $this->valStartEndDateWithSubTaskEndDate( $strParentScheduledEndDatetime, $strParentScheduledStartDatetime, $arrmixSubTask );
				$boolIsValid &= $this->valRequestedDatetime( $strRequestedDate, $strRequestedTime );
				$boolIsValid &= $this->valAssignedTo();

				if( true == $boolIsMakeReadyMaintenanceRequest ) {
					$boolIsValid &= $this->valMakeReadyMaintenanceRequests( $strParentScheduledEndDatetime, $strMRMoveOutDate, $objDatabase );
				}

				if( true == $boolIsDueDateRequired ) {
					$boolIsValid &= $this->valDueDate();
				}

				if( true == $boolIsActionRequired ) {
					$boolIsValid &= $this->valAction( $objDatabase );
				}
				break;

			case 'call_center_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();

				if( true == $boolIsClientProblem ) {
					$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase, true );
					$boolIsValid &= $this->valMaintenanceProblemId();
				} else {
					$boolIsValid &= $this->valMaintenanceLocationCategoryProblemId( $objProperty, $objDatabase, true );
				}

				$boolIsValid &= $this->valMaintenancePriorityId();
				$boolIsValid &= $this->valMaintenanceStatusId();
				$boolIsValid &= $this->valProblemDescription();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valScheduledStartDatetime();
				if( true == $this->getIsValidateScheduledEndDate() ) {
					$boolIsValid &= $this->valScheduledEndDatetime();
				}
				$boolIsValid &= $this->valDate( $this->m_strCompletedDatetime, 'completed date' );
				$boolIsValid &= $this->valCompletedDatetime();
				$boolIsValid &= $this->valMaintenancePriorityId();
				$boolIsValid &= $this->valMaintenanceStatusId();

				if( false == $boolIsMakeReadyMaintenanceRequest ) {

					if( false == $boolIsValidateSubTask ) {
						$boolIsValid &= $this->valPermissionToEnter( $objProperty, $objDatabase );
					}

					if( true == $boolIsClientProblem && true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
						$boolIsValid &= $this->valMaintenanceProblemId();
					} elseif( true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceLocationCategoryProblemId( $objProperty, $objDatabase );
					} else {
						$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
					}
				}

				$boolIsValid &= $this->valAssetId( $objDatabase );
				$boolIsValid &= $this->valProblemDescription();
				if( true == $boolIsValidateSubTask && false == $boolIsMobileWorkorderWorkflow ) {
					$boolIsValid &= $this->valSubTaskStatus( $objDatabase, $boolIsMakeReadyMaintenanceRequest );
				}
				$boolIsValid &= $this->valRequestedDatetime( $strRequestedDate, $strRequestedTime );

				if( true == $boolIsDueDateRequired ) {
					$boolIsValid &= $this->valDueDate();
				}
				break;

			case self::VALIDATE_WEBSERVICE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();

				if( false == $this->getIsFloatingMaintenanceRequest() ) {
					$boolIsValid &= $this->valMaintenanceRequest();
				} else {
					$boolIsValid &= $this->valCustomerNameFirst();
					$boolIsValid &= $this->valCustomerNameLast();
				}

				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valMaintenancePriorityId();
				$boolIsValid &= $this->valMaintenanceStatusId();
				if( false == $boolIsMakeReadyMaintenanceRequest ) {
					if( true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceProblemId();
					} else {
						$boolIsValid &= $this->valMaintenanceLocationCategoryProblemId( $objProperty, $objDatabase, true );
					}
				}
				$boolIsValid &= $this->valProblemDescription();
				break;

			case 'validate_update_status_information':
				$boolIsValid &= $this->valMaintenancePriorityId();
				$boolIsValid &= $this->valCompletedDatetime();
				$boolIsValid &= $this->valOverviewScheduledEndDatetime();
				$boolIsValid &= $this->valRequestedDatetime( $strRequestedDate, $strRequestedTime );
				$boolIsValid &= $this->valSubTaskScheduledEndDatetime( $strParentScheduledEndDatetime, $strParentScheduledStartDatetime );

				if( true == $boolIsDueDateRequired ) {
					$boolIsValid &= $this->valDueDate();
				}
				break;

			case 'validate_update_request_details':
				if( false == $boolIsMakeReadyMaintenanceRequest ) {
					if( true == $boolIsClientProblem && true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
						$boolIsValid &= $this->valMaintenanceProblemId();
					} elseif( true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceLocationCategoryProblemId( $objProperty, $objDatabase );
					} else {
						$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
					}
				}

				if( true == $boolIsDueDateRequired ) {
					$boolIsValid &= $this->valDueDate();
				}

				$boolIsValid &= $this->valAssetId( $objDatabase );
				$boolIsValid &= $this->valProblemDescription();
				if( true == $boolIsActionRequired ) {
					$boolIsValid &= $this->valAction( $objDatabase );
				}
				break;

			case 'validate_update_close':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case 'validate_dashboard_update_status':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valMaintenanceStatusId();
				if( true == $boolIsCompanyEmployeeRequired ) {
					$boolIsValid &= $this->valCompanyEmployeeId();
				}
				$boolIsValid &= $this->valSubTaskStatus( $objDatabase, $boolIsMakeReadyMaintenanceRequest );
				break;

			case 'validate_insert_resident':
				if( true == $this->getIsFloatingMaintenanceRequest() ) {
					$boolIsValid &= $this->valMainPhoneNumber( false );
					$boolIsValid &= $this->valAltPhoneNumber( false );
					$boolIsValid &= $this->valEmailAddress();
				} else {
					$boolIsValid &= $this->valMaintenanceRequest();
					$boolIsValid &= $this->valCustomerId();
				}
				break;

			case 'validate_insert_other_detail':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPermissionToEnter( $objProperty, $objDatabase );
				$boolIsValid &= $this->valMaintenancePriorityId();
				$boolIsValid &= $this->valMaintenanceStatusId();

				if( false == $boolIsMakeReadyMaintenanceRequest ) {
					if( true == $boolIsClientProblem && true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
						$boolIsValid &= $this->valMaintenanceProblemId();
					} elseif( true == $boolIsProblemRequired ) {
						$boolIsValid &= $this->valMaintenanceLocationCategoryProblemId( $objProperty, $objDatabase );
					} else {
						$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
					}
				}

				$boolIsValid &= $this->valProblemDescription();
				$boolIsValid &= $this->valDate( $this->m_strScheduledStartDatetime, 'start date' );
				$boolIsValid &= $this->valDate( $this->m_strScheduledEndDatetime, 'end date' );
				$boolIsValid &= $this->valDate( $this->m_strCompletedDatetime, 'completed date' );
				$boolIsValid &= $this->valScheduledEndDatetime();
				$boolIsValid &= $this->valRequestedDatetime( $strRequestedDate, $strRequestedTime );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_sub_task':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valMaintenanceStatusId();
				$boolIsValid &= $this->valSubTaskScheduledEndDatetime( $strParentScheduledEndDatetime, $strParentScheduledStartDatetime );
				$boolIsValid &= $this->valCompletedDatetime();

				if( true == $boolIsClientProblem ) {
					$boolIsValid &= $this->valMaintenanceLocationId( $objProperty, $objDatabase );
					$boolIsValid &= $this->valMaintenanceProblemId();
				} else {
					if( false == $boolIsClientProblem ) {
						$boolIsValid &= $this->valMaintenanceLocationCategoryProblemId( $objProperty, $objDatabase );
					}
				}

				if( true == $boolIsActionRequired ) {
					$boolIsValid &= $this->valAction( $objDatabase );
				}

				if( true == in_array( $this->getMaintenanceRequestTypeId(), [ CMaintenanceRequestType::SERVICE_REQUEST, CMaintenanceRequestType::RECURRING, CMaintenanceRequestType::MAKE_READY ] ) && true == $boolIsUserPermittedToCreateBackDateWorkOrder ) {
					$boolIsValid &= $this->valSubTaskActualStartDatetime();
				}
				break;

			case 'validate_bulk_edit_sub_tasks':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valSubTaskScheduledEndDatetime( $strParentScheduledEndDatetime, $strParentScheduledStartDatetime );
				break;

			case 'bulk_edit_make_ready_maintenenace_requests':
				$boolIsValid &= $this->valMaintenanceProblemId();
				break;

			case 'validate_leasing_center_resident':
				$boolIsValid &= $this->valCustomerNameFirst();
				$boolIsValid &= $this->valCustomerNameLast();

				if( false == $this->getIsFloatingMaintenanceRequest() && true == $boolIsValid ) {
					$boolIsValid &= $this->valCallCenterMaintenanceRequest();
				}

				$boolIsValid &= $this->valCallCenterUnitNumber();
				$boolIsValid &= $this->valPropertyBuildingId();
				$boolIsValid &= $this->valMainPhoneNumber( true, false );
				$boolIsValid &= $this->valAltPhoneNumber( false, false );
				$boolIsValid &= $this->valEmailAddress();
				break;

			case 'validate_leasing_center_additional_info':
				$boolIsValid &= $this->valPermissionToEnter( NULL, NULL, false );
				$boolIsValid &= $this->valEntryNote();
				$boolIsValid &= $this->valCallId();
				$boolIsValid &= $this->valRequestedDatetime( $strRequestedDate, $strRequestedTime );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	// This function creates an sms message object.
	/**
	 * Commenting the code since we need to make changes in mobile number.Currently script is unpublished so at that time we need to take care for phone number changes.
	 */

	/*
	 public function createMessages( $objClientDatabase, $objWebsite = NULL ) {

		$objMessage = new CMessage();
		$objMessage->setDefaults();

		$objMessage->setCid( $this->getCid() );
		$objMessage->setMaintenanceRequestId( $this->getId() );
		$objMessage->setMessageTypeId( CMessageType::MAINTENANCE_NOTIFICATION );
		$objMessage->setMessageOperatorId( $this->getMessageOperatorId() );
		$objMessage->setPhoneNumber( $this->getMobilePhoneNumber() );
		$objMessage->setCustomerId( $this->getCustomerId() );
		$objMessage->setPropertyId( $this->getPropertyId() );

		$strAptDescription = ( false == is_null( $this->getUnitNumber() ) ) ? ' for apt #' . $this->getUnitNumber() : NULL;

		$objProperty = $this->fetchProperty( $objClientDatabase );

		if( true == valObj( $objProperty, 'CProperty' ) ) {

			if( true == valObj( $objWebsite, 'CWebsite' ) ) {
				$strRp40SubDomain = $objProperty->getRp40SubDomain( $objClientDatabase );
				if( true == valStr( $strRp40SubDomain ) ) {
					$strUrl = $strBaseName = 'https://' . $strRp40SubDomain . '.residentportal.com/app/apartment/maintenance';
				} else {
					$strUrl = $strBaseName = 'https://' . $objWebsite->getSubDomain() . '.residentportal.com/resident_portal/?module=maintenance_requests';
				}

				if( 'production' === \Psi\Libraries\UtilConfig\CConfig::createService()->get( 'environment' ) ) {
					$objBitly = new \Psi\Libraries\ExternalBitly\CBitly( \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( \Psi\Libraries\UtilConfig\CConfig::createService()->get( 'BITLY_USERNAME' ), \Psi\Libraries\UtilConfig\CConfig::createService()->get( 'SODIUM_KEY_LOGIN_USERNAME') ), \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( \Psi\Libraries\UtilConfig\CConfig::createService()->get( 'BITLY_API_KEY' ), \Psi\Libraries\UtilConfig\CConfig::createService()->get( 'SODIUM_KEY_LOGIN_USERNAME' ) ) );
					$strUrl   = $objBitly->shorten( $strBaseName );
				}
			}
		}
		$strMessage = '';
		( false == is_null( $strUrl ) ) ? $strMessage .= '.  Log in for detail. ' . $strUrl : $strMessage .= NULL;

		if( CMaintenanceStatusType::COMPLETED == $this->getMaintenanceStatusTypeId() ) {
			$strMaintenanceRequestStatus = ' has been completed';
		} elseif( CMaintenanceStatusType::CANCELLED == $this->getMaintenanceStatusTypeId() ) {
			$strMaintenanceRequestStatus = ' has been cancelled';
		}

		$strMessage = 'Maintenance request #' . $this->getId() . $strMaintenanceRequestStatus . $strAptDescription . $strMessage;

		$arrobjMessages = [];

		if( ( self::SMS_LENGTH - 5 ) < \Psi\CStringService::singleton()->strlen( $strMessage ) ) {

			$strFirstMessage  = \Psi\CStringService::singleton()->substr( $strMessage, 0, 150 );
			$strSecondMessage = \Psi\CStringService::singleton()->substr( $strMessage, 150 );

			$objMessageSecond = clone $objMessage;
			$objMessageThird  = clone $objMessage;

			$objMessage->setMessage( '(1/3)' . $strFirstMessage );
			$arrobjMessages[] = $objMessage;

			$objMessageSecond->setMessage( '(2/3)' . $strSecondMessage );
			$arrobjMessages[] = $objMessageSecond;

			$objMessageThird->setMessage( '(3/3)' . self::TERMS_AND_REPLY_STOP_MESSAGE );
			$arrobjMessages[] = $objMessageThird;
		} else {
			$objMessageSecond = clone $objMessage;

			$objMessage->setMessage( '(1/2)' . $strMessage );
			$arrobjMessages[] = $objMessage;

			$objMessageSecond->setMessage( '(2/2)' . self::TERMS_AND_REPLY_STOP_MESSAGE );
			$arrobjMessages[] = $objMessageSecond;
		}
		return $arrobjMessages;
	 }
	 */

	public function createMaintenanceRequestLabor() {

		$objMaintenanceRequestLabor = new CMaintenanceRequestLabor();

		$objMaintenanceRequestLabor->setCid( $this->getCid() );
		$objMaintenanceRequestLabor->setMaintenanceRequestId( $this->getId() );

		return $objMaintenanceRequestLabor;
	}

	public function createMaintenanceRequestMaterial() {

		$objMaintenanceRequestMaterial = new CMaintenanceRequestMaterial();

		$objMaintenanceRequestMaterial->setCid( $this->getCid() );
		$objMaintenanceRequestMaterial->setMaintenanceRequestId( $this->getId() );

		return $objMaintenanceRequestMaterial;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setDefaults();
		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setPropertyId( $this->getPropertyId() );
		$objFileAssociation->setMaintenanceRequestId( $this->getId() );

		return $objFileAssociation;
	}

	public function createWorkOrderEvent( $intEventSubTypeId, $objCompanyUser = NULL, $objOldMaintenanceRequest = NULL, $objMaintenanceRequestNote = NULL, $objDatabase, $boolIsNewDashboard = false, $boolReturnSqlOnly = false ) {

		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		if( true == valObj( $this->getCustomer(), 'CCustomer' ) ) {
			$objEventLibraryDataObject->setCustomer( $this->getCustomer() );
		}

		$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::WORK_ORDER_UPDATED, $intEventSubTypeId );
		$objEvent->setCid( $this->getCid() );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objEvent->setCompanyUser( $objCompanyUser );
			$intUserId = $objCompanyUser->getId();

			if( false == is_null( $objCompanyUser->getCompanyEmployeeId() ) ) {
				$objCompanyEmployee = CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $objCompanyUser->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
				if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
					$objEvent->setCompanyEmployee( $objCompanyEmployee );
				}
			}
		} else {
			if( true == valId( $objCompanyUser ) ) {
				// true == valId( $objCompanyUser ) someTimes we are sending only $intCompanyUserId in $objCompanyUser variable thats why valId()
				$intUserId = $objCompanyUser;
				$objEvent->setCompanyUser( NULL );
			} else {
				$intUserId = SYSTEM_USER_ID;
				$objEvent->setCompanyUser( NULL );
			}
		}

		$objEvent->setEventDatetime( 'NOW()' );

		if( false == is_null( $this->getCustomerId() ) ) {
			$objEvent->setCustomerId( $this->getCustomerId() );
			$objEvent->setLeaseId( $this->getLeaseId() );
		} else {
			$objEvent->setCustomerId( NULL );
			$objEvent->setLeaseId( NULL );
		}
		$objEvent->setPsProductId( $this->getUpdatedPsProductId() );
		$objEvent->setDataReferenceId( $this->getId() );

		if( CEventSubType::WORKORDER_NOTE_ADDED == $intEventSubTypeId || CEventSubType::WORKORDER_CLOSE_NOTE == $intEventSubTypeId || CEventSubType::WORKORDER_ENTRY_NOTE == $intEventSubTypeId || CEventSubType::WORKORDER_PROBLEM_DESCRIPTION_UPDATED == $intEventSubTypeId || CEventSubType::WORKORDER_OPENED == $intEventSubTypeId ) {
			if( true == valObj( $objMaintenanceRequestNote, 'CMaintenanceRequestNote' ) && false == is_null( $objMaintenanceRequestNote->getNote() ) && 0 < strlen( $objMaintenanceRequestNote->getNote() ) ) {
				$objEvent->setNotes( $objMaintenanceRequestNote->getNote() );

				// set is Visible to Resident
				if( true == $objMaintenanceRequestNote->getIsResidentVisible() ) {
					$this->setIsNotesVisibleToResident( $objMaintenanceRequestNote->getIsResidentVisible() );
				} else {
					$this->setIsNotesVisibleToResident( false );
				}
			} else {
				$objEvent->setNotes( $this->getProblemDescription() );
			}
		} else {
			$objEvent->setNotes( NULL );
		}

		$arrintEventSubTypeIds = array_flip( array_merge( CEventSubType::$c_arrintMaintenanceEventSubTypeIds, [ CEventSubType::WORKORDER_CLOSE_NOTE, CEventSubType::WORKORDER_ENTRY_NOTE ] ) );

		unset( $arrintEventSubTypeIds[CEventSubType::WORKORDER_OPENED], $arrintEventSubTypeIds[CEventSubType::WORKORDER_CLOSED], $arrintEventSubTypeIds[CEventSubType::WORKORDER_OUTGOING_EMAIL], $arrintEventSubTypeIds[CEventSubType::WORKORDER_SATISFACTION_OUTGOING_EMAIL] );
		if( true == array_key_exists( $intEventSubTypeId, $arrintEventSubTypeIds ) ) {
			$objEvent->setOldReferenceData( $objOldMaintenanceRequest );
		} else {
			$objEvent->setOldReferenceData( NULL );
		}

		$objEvent->setReference( $this );

		$objEventLibrary->buildEventDescription( $this, $boolIsNewDashboard );

		if( true == $boolReturnSqlOnly ) {
			return $objEventLibrary->insertEvent( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$intEventId = $objEventLibrary->insertEvent( $intUserId, $objDatabase );
		if( false == valId( $intEventId ) ) {
			$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
			$objDatabase->rollback();

			return false;
		} else {
			if( CEventType::WORK_ORDER_UPDATED == $objEvent->getEventTypeId() && CEventSubType::WORKORDER_SATISFACTION_OUTGOING_EMAIL == $objEvent->getEventSubTypeId() ) {
				return $intEventId;
			}
			if( CEventType::WORK_ORDER_UPDATED == $objEvent->getEventTypeId() && true == in_array( $objEvent->getEventSubTypeId(), [ CEventSubType::WORKORDER_OPENED, CEventSubType::WORKORDER_STATUS_UPDATED, CEventSubType::WORKORDER_CLOSED, CEventSubType::WORKORDER_CLOSED_AND_WORK_COMPLETED, CEventSubType::WORKORDER_COMPLETED, CEventSubType::WORKORDER_NOTE_ADDED, CEventSubType::WORKORDER_CLOSE_NOTE, CEventSubType::WORKORDER_CREATE_SURVEY, CEventSubType::WORKORDER_CREATE_SURVEY_RESPONSE ] ) ) {
				return $this->m_intEventId = $intEventId;
			}
		}

		return true;
	}

	public function createScheduledCharge() {

		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );

		return $objScheduledCharge;
	}

	public function createMaintenanceEmailLibrary( $objDatabase, $objEmailDatabase = NULL, $objObjectStorageGateway = NULL, $objClient = NULL, $arrobjPropertyPreferences = NULL, $objCompanyUser = NULL ) {
		$objMaintenanceEmailLibrary = new CMaintenanceEmailLibrary();
		$objMaintenanceEmailLibrary->setMaintenanceRequest( $this );
		$objMaintenanceEmailLibrary->setDatabase( $objDatabase );
		$objMaintenanceEmailLibrary->setEmailDatabase( $objEmailDatabase );
		$objMaintenanceEmailLibrary->setObjectStorageGateway( $objObjectStorageGateway );
		$objMaintenanceEmailLibrary->setClient( $objClient );
		$objMaintenanceEmailLibrary->setPropertyPreferences( $arrobjPropertyPreferences );
		$objMaintenanceEmailLibrary->setCompanyUser( $objCompanyUser );
		return $objMaintenanceEmailLibrary;
	}

	public function createMaintenanceSmsLibrary( $objClientDatabase, $objCompanyUser = NULL ) {
		$objMaintenanceSmsLibrary = new CMaintenanceSmsLibrary();
		$objMaintenanceSmsLibrary->setDatabase( $objClientDatabase );
		$objMaintenanceSmsLibrary->setMaintenanceRequest( $this );
		$objMaintenanceSmsLibrary->setCompanyUser( $objCompanyUser );
		return $objMaintenanceSmsLibrary;
	}

	/**
	 * Other Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == checkEntrataModuleWritePermission() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'You have not been granted write access to this module.' ) );

			return false;
		}

		$this->checkSpecialCharacters();

		// Removed the seconds from Actual start date time as it causing the issue for labor log entries in calculating the response time
		if( true == is_null( $this->getActualStartDatetime() ) ) {
			$this->setActualStartDatetime( date( 'm/d/Y H:i' ) );
		} else {
			$this->setActualStartDatetime( date( 'Y-m-d H:i', strtotime( $this->getActualStartDatetime() ) ) );
		}

		$boolIgnoreResponseTimes = $this->getIgnoreResponseTimes();
		if( false != empty( $boolIgnoreResponseTimes ) ) {
			$this->setIgnoreResponseTimes( false );
		}

		$intMaintenancePriorityId = $this->getMaintenancePriorityId();
		if( false != empty( $intMaintenancePriorityId ) ) {

			$arrintMaintenancePriority = CPropertyMaintenancePriorities::createService()->fetchSimpleMaintenancePriorityByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( false != isset ( $arrintMaintenancePriority[0]['new_maintenance_priority_id'] ) ) {
				$this->setMaintenancePriorityId( $arrintMaintenancePriority[0]['new_maintenance_priority_id'] );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Priority is required.' ) );

				return false;
			}

		}
		$objDataset = $objDatabase->createDataset();
		$intId      = $this->getId();

		// Set the alarm code on lease table
		if( false == is_null( $this->getAlarmCode() ) ) {
			$objLeaseDetail = \Psi\Eos\Entrata\CLeaseDetails::createService()->fetchLeaseDetailByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

			if( false != valObj( $objLeaseDetail, 'CLeaseDetail' ) ) {
				$objLeaseDetail->setAlarmKey( $this->getAlarmCode() );

				if( false == $objLeaseDetail->update( $intCurrentUserId, $objDatabase ) ) {
					$objDatabase->rollback();

					return false;
				}
			}
		}
		if( true == valId( $this->getCustomerId() ) && true == valId( $this->getLeaseId() ) ) {
			$this->setIsResidentVisible( 1 );
		}

		$strSql = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSql = true );

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {
			// Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert maintenance request record. The following error was reported.' ) ) );

			if( true == \Psi\CStringService::singleton()->strpos( $objDatabase->errorMsg(), 'valid_lease_id' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Lease is required' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );
			}

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert maintenance request record. The following error was reported.' ) ) );
			// Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrstrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrstrValues['type'], $arrstrValues['field'], $arrstrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$this->processCachedRates( $intCurrentUserId, $objDatabase );

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSoftDelete = true ) {

		if( $boolSoftDelete == true ) {

			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( ' NOW() ' );
			$this->setUpdatedBy( $intUserId );
			$this->setUpdatedOn( ' NOW() ' );
			$this->setMaintenanceTemplateId( NULL );
			if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
				return true;
			}

		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolUpdateUnitAvailableOnDate = false, $boolIsFloating = false, $strAvailableOnDate = '' ) {

		if( false == checkEntrataModuleWritePermission() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'You have not been granted write access to this module.' ) ) );

			return false;
		}

		$this->checkSpecialCharacters();

		$boolIgnoreResponseTimes = $this->getIgnoreResponseTimes();
		if( false != empty( $boolIgnoreResponseTimes ) ) {
			$this->setIgnoreResponseTimes( false );
		}

		$intMaintenancePriorityId = $this->getMaintenancePriorityId();
		if( false != empty( $intMaintenancePriorityId ) ) {

			$arrintMaintenancePriority = CPropertyMaintenancePriorities::createService()->fetchSimpleMaintenancePriorityByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( false != isset ( $arrintMaintenancePriority[0]['new_maintenance_priority_id'] ) ) {
				$this->setMaintenancePriorityId( $arrintMaintenancePriority[0]['new_maintenance_priority_id'] );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'Priority is required.' ) ) );

				return false;
			}

		}

		// Set the alarm code on lease table
		if( false == is_null( $this->getAlarmCode() ) ) {
			$objLeaseDetail = \Psi\Eos\Entrata\CLeaseDetails::createService()->fetchLeaseDetailByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

			if( false != valObj( $objLeaseDetail, 'CLeaseDetail' ) ) {
				$objLeaseDetail->setAlarmKey( $this->getAlarmCode() );

				if( false == $objLeaseDetail->update( $intCurrentUserId, $objDatabase ) ) {
					$objDatabase->rollback();

					return false;
				}
			}
		}

		$mixResult = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolReturnSqlOnly || false == $mixResult || true == $boolIsFloating ) {
			return $mixResult;
		}

		if( true == is_null( $this->getParentMaintenanceRequestId() ) ) {

			if( true == in_array( $this->getMaintenanceRequestTypeId(), [ CMaintenanceRequestType::MAKE_READY, CMaintenanceRequestType::RENOVATION ] ) ) {

				$this->getOrFetchUnitSpace( $objDatabase );
				$this->getOrFetchLease( $objDatabase );
				$this->getOrFetchMaintenanceStatus( $objDatabase );

				if( false == is_null( $this->getCustomerId() ) && false == valObj( $this->m_objLease, 'CLease' ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to load the lease/unit space associated with this maintenance request.' ) ) );

					return false;
				}

				if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) {

					$strCalculatedAvailableOnDate = NULL;
					$this->m_objUnitSpace->setSerializedOriginalValues( serialize( $this->m_objUnitSpace->toArray() ) );
					if( true == valStr( $this->getScheduledEndDatetime() ) ) {
						$this->m_objUnitSpace->setMakeReadyDate( date( 'm/d/Y', strtotime( $this->getScheduledEndDatetime() ) ) );
						CMakeReadyLibrary::createService()->setDatabase( $objDatabase );
						CMakeReadyLibrary::createService()->setUnitSpace( $this->m_objUnitSpace );
						$strCalculatedAvailableOnDate = CMakeReadyLibrary::createService()->calculateUnitAvailableOnDate( $this, $this->m_objLease, NULL, NULL, $this->getOrFetchMaintenanceTemplate( $objDatabase ) );
					}

					if( true == valStr( $strAvailableOnDate ) ) {
						if( CMaintenanceStatusType::COMPLETED != $this->getMaintenanceStatusTypeId() && true == valStr( $strCalculatedAvailableOnDate ) && strtotime( $strAvailableOnDate ) < strtotime( $this->getScheduledEndDatetime() ) ) {
							$strAvailableOnDate = $strCalculatedAvailableOnDate;
						}
						$this->m_objUnitSpace->setAvailableOn( $strAvailableOnDate );
					} else if( true == valStr( $strCalculatedAvailableOnDate ) ) {
						$strAvailableOnDate = $strCalculatedAvailableOnDate;
						if( true == $boolUpdateUnitAvailableOnDate ) {
							$this->m_objUnitSpace->setAvailableOn( $strAvailableOnDate );
						}
					}

					if( true == valStr( $this->getCompletedDatetime() ) ) {
						$this->m_objUnitSpace->setMakeReadyDate( date( 'm/d/Y', strtotime( $this->getCompletedDatetime() ) ) );
					}

					if( true == valStr( $this->getDeletedOn() ) ) {
						$this->m_objUnitSpace->setMakeReadyDate( NULL );
					}

					// Add the code to set the Unit Space Log type as Make Ready Status
					$intOldUnitSpaceLogTypeId      = $this->m_objUnitSpace->getUnitSpaceLogTypeId();
					$boolIsAllowDifferentialUpdate = $this->m_objUnitSpace->getAllowDifferentialUpdate();
					$this->m_objUnitSpace->setUnitSpaceLogTypeId( CUnitSpaceLogType::MAKE_READY_STATUS );
					$this->m_objUnitSpace->setAllowDifferentialUpdate( true );

					if( false == $this->m_objUnitSpace->update( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsgs( $this->m_objUnitSpace->getErrorMsgs() );
						$this->m_objUnitSpace->setUnitSpaceLogTypeId( $intOldUnitSpaceLogTypeId );
						$this->m_objUnitSpace->setAllowDifferentialUpdate( $boolIsAllowDifferentialUpdate );

						return false;
					}
					$this->m_objUnitSpace->setUnitSpaceLogTypeId( $intOldUnitSpaceLogTypeId );
					$this->m_objUnitSpace->setAllowDifferentialUpdate( $boolIsAllowDifferentialUpdate );

					if( true == valObj( $this->m_objMaintenanceStatus, 'CMaintenancestatus' ) && false == in_array( $this->m_objMaintenanceStatus->getMaintenanceStatusTypeId(), CMaintenanceStatusType::$c_arrintClosedStatusTypes ) ) {
						$this->setCompletedDatetime( NULL );
					}
				}
			}

			$this->processCachedRates( $intCurrentUserId, $objDatabase );
		}

		return $mixResult;
	}

	public function simpleUpdate( $intCurrentUserId, $objDatabase ) {
		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
		          'FROM maintenance_requests_update( row_to_json ( ROW ( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlParentMaintenanceRequestId() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPropertyBuildingId() . ', ' .
		          $this->sqlPropertyUnitId() . ', ' .
		          $this->sqlUnitSpaceId() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlJobProjectId() . ', ' .
		          $this->sqlMaintenancePriorityId() . ', ' .
		          $this->sqlMaintenanceStatusId() . ', ' .
		          $this->sqlMaintenanceLocationId() . ', ' .
		          $this->sqlPropertyUnitMaintenanceLocationId() . ', ' .
		          $this->sqlMaintenanceProblemId() . ', ' .
		          $this->sqlSubMaintenanceProblemId() . ', ' .
		          $this->sqlRecurringMaintenanceRequestId() . ', ' .
		          $this->sqlCompanyEmployeeId() . ', ' .
		          $this->sqlApPayeeId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlMaintenanceTemplateId() . ', ' .
		          $this->sqlMaintenanceRequestTypeId() . ', ' .
		          $this->sqlMainPhoneNumberTypeId() . ', ' .
		          $this->sqlAltPhoneNumberTypeId() . ', ' .
		          $this->sqlAssetId() . ', ' .
		          $this->sqlAddOnId() . ', ' .
		          $this->sqlMaintenanceGroupId() . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlDueDate() . ', ' .
		          $this->sqlProblemDescription() . ', ' .
		          $this->sqlAdditionalInfo() . ', ' .
		          $this->sqlLocationSpecifics() . ', ' .
		          $this->sqlPermissionToEnter() . ', ' .
		          $this->sqlRequestedDatetime() . ', ' .
		          $this->sqlScheduledStartDatetime() . ', ' .
		          $this->sqlScheduledEndDatetime() . ', ' .
		          $this->sqlActualStartDatetime() . ', ' .
		          $this->sqlCompletedDatetime() . ', ' .
		          $this->sqlIgnoreResponseTimes() . ', ' .
		          $this->sqlIgnoreResponseReason() . ', ' .
		          $this->sqlCustomerNameFirst() . ', ' .
		          $this->sqlCustomerNameFirstEncrypted() . ', ' .
		          $this->sqlCustomerNameLast() . ', ' .
		          $this->sqlCustomerNameLastEncrypted() . ', ' .
		          $this->sqlUnitNumber() . ', ' .
		          $this->sqlMainPhoneNumber() . ', ' .
		          $this->sqlMainPhoneNumberEncrypted() . ', ' .
		          $this->sqlAltPhoneNumber() . ', ' .
		          $this->sqlAltPhoneNumberEncrypted() . ', ' .
		          $this->sqlEmailAddress() . ', ' .
		          $this->sqlRequiredResponseDatetime() . ', ' .
		          $this->sqlResponseDuration() . ', ' .
		          $this->sqlSmsConfirmedOn() . ', ' .
		          $this->sqlIsPet() . ', ' .
		          $this->sqlIsResidentVisible() . ', ' .
		          $this->sqlImportedOn() . ', ' .
		          $this->sqlExportedOn() . ', ' .
		          $this->sqlPrintedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlMaintenanceExceptionId() . ', ' .
		          $this->sqlUpdatedPsProductId() . ', ' .
		          $this->sqlIsRequestedAm() . ', ' .
		          $this->sqlRequestedDate() . ', ' .
		          ( int ) $intCurrentUserId . ' ) ) ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update maintenance request record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update maintenance request record. The following error was reported.' ) );

			while( false == $objDataset->eof() ) {
				$arrstrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrstrValues['type'], $arrstrValues['field'], $arrstrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public static function createMaintenanceBulkGroup( $arrintScheduledTaskIds, $boolIsMaintenance, $objDatabase ) {
		$strSql = 'SELECT * ' .
		          'FROM create_maintenance_groups( ARRAY[' . sqlIntImplode( $arrintScheduledTaskIds ) . ']::INT[],
				current_date ,' .
		          $boolIsMaintenance . ' ) AS result;';

		return fetchData( $strSql, $objDatabase );
	}

	// Re-cache unit space if make ready work order

	public function processCachedRates( $intCompanyUserId, $objDatabase ) {
		if( false == valId( $this->getUnitSpaceId() ) || CMaintenanceRequestType::MAKE_READY != $this->getMaintenanceRequestTypeId() ) {
			return true;
		}

		$arrintUnitSpaceIds = [];

		if( 0 < $this->getUnitSpaceId() ) {
			$arrintUnitSpaceIds[$this->getUnitSpaceId()] = $this->getUnitSpaceId();
		}

		return \Psi\Eos\Entrata\CCachedRates::createService()->processCachedRatesByCidByPropertydByUnitSpaceIds( $this->getCid(), $this->getPropertyId(), $arrintUnitSpaceIds, $intCompanyUserId, $objDatabase );
	}

	public function exportMaintenanceRequest( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $objLease = NULL, $objCustomer = NULL, $intIntegrationSyncTypeId = NULL ) {

		if( false == is_null( $this->getRemotePrimaryKey() ) && 0 < strlen( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		// Setting closing note on maintenance request object from maintenance request note
		$objMaintenanceRequestClosingNote = CMaintenanceRequestNotes::createService()->fetchMaintenanceRequestClosingNoteByMaintenanceRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objMaintenanceRequestClosingNote, 'CMaintenanceRequestNote' ) ) {
			$this->setClosingNote( $objMaintenanceRequestClosingNote->getNote() );
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->m_intPropertyId, $this->m_intCid, CIntegrationService::SEND_MAINTENANCE_REQUEST, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setMaintenanceRequest( $this );
		$objGenericWorker->setProperty( $this->fetchProperty( $objDatabase ) );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( true == valObj( $objLease, 'CLease' ) ) {
			$objGenericWorker->setLease( $objLease );
		}

		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$objGenericWorker->setCustomer( $objCustomer );
		}

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		$boolIsValid = $objGenericWorker->process();

		if( true == $boolIsValid ) {
			$this->exportMaintenanceRequestAttachments( $intCompanyUserId, $objDatabase );
		}
		if( false == $boolIsValid && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportMaintenanceRequestAttachments( $intCompanyUserId, $objDatabase, $intIntegrationSyncTypeId = NULL, $boolQueueSyncOverride = false ) {

		if( false == valStr( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->m_intPropertyId, $this->m_intCid, CIntegrationService::UPLOAD_MAINTENANCE_REQUEST_ATTACHMENTS, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setMaintenanceRequest( $this );
		$objGenericWorker->setProperty( $this->fetchProperty( $objDatabase ) );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_COHESITY_S3 );
		$objObjectStorageGateway->initialize();
		$objGenericWorker->setObjectStorageGateway( $objObjectStorageGateway );

		$boolIsValid = $objGenericWorker->process();

		return $boolIsValid;
	}

	public function exportUpdatedMaintenanceRequest( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $objLease = NULL, $objCustomer = NULL ) {

		if( true == is_null( $this->getRemotePrimaryKey() ) || 0 == strlen( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		// Setting closing note on maintenance request object from maintenance request note
		$objMaintenanceRequestClosingNote = CMaintenanceRequestNotes::createService()->fetchMaintenanceRequestClosingNoteByMaintenanceRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objMaintenanceRequestClosingNote, 'CMaintenanceRequestNote' ) ) {
			$this->setClosingNote( $objMaintenanceRequestClosingNote->getNote() );
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::UPDATE_MAINTENANCE_REQUEST, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setMaintenanceRequest( $this );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( true == valObj( $objLease, 'CLease' ) ) {
			$objGenericWorker->setLease( $objLease );
		}

		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$objGenericWorker->setCustomer( $objCustomer );
		}

		if( true == valObj( $this->getProperty(), CProperty::class ) ) {
			$objGenericWorker->setProperty( $this->getProperty() );
		}

		return $objGenericWorker->process();
	}

	public function exportClosedMaintenanceRequest( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false ) {

		if( true == is_null( $this->getRemotePrimaryKey() ) || 0 == strlen( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		// Setting closing note on maintenance request object from maintenance request note
		$objMaintenanceRequestClosingNote = CMaintenanceRequestNotes::createService()->fetchMaintenanceRequestClosingNoteByMaintenanceRequestIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objMaintenanceRequestClosingNote, 'CMaintenanceRequestNote' ) ) {
			$this->setClosingNote( $objMaintenanceRequestClosingNote->getNote() );
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::CLOSE_MAINTENANCE_REQUEST, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setMaintenanceRequest( $this );
		$objGenericWorker->setDatabase( $objDatabase );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( true == valObj( $this->getProperty(), CProperty::class ) ) {
			$objGenericWorker->setProperty( $this->getProperty() );
		}

		return $objGenericWorker->process();
	}

	public function smsSubmitMaintenanceRequest( $objProperty, $intCompanyUserId, $objDatabase, $objSmsDatabase, $objDocumentManager = NULL ) {

		$arrintMigrationModeOnPropertyIds = CProperties::createService()->fetchCustomMigrationModeOnPropertiesByPropertyIdsByCid( [ $objProperty->getId() ], $this->getCid(), $objDatabase );
		if( true == valArr( $arrintMigrationModeOnPropertyIds ) ) {
			return true;
		}

		$strMobilePhoneNumber       = NULL;
		$objAssignedCompanyEmployee = NULL;

		if( true == valObj( $objProperty, 'CProperty' ) ) {

			$objPropertyMaintenanceProblem  = $objProperty->fetchPropertyMaintenanceProblemByMaintenanceProblemId( $this->getMaintenanceProblemId(), $objDatabase );
			$objPropertyMaintenancePriority = $objProperty->fetchPropertyMaintenancePriorityByMaintenancePriorityId( $this->getMaintenancePriorityId(), $objDatabase );
			$objClient                      = $objProperty->fetchClient( $objDatabase );
		}

		if( true == isset( $objPropertyMaintenanceProblem ) && 0 < $objPropertyMaintenanceProblem->getEmployeeId() && ( 1 == $objPropertyMaintenanceProblem->getSendSms() || ( true == isset( $objPropertyMaintenancePriority ) && $objPropertyMaintenanceProblem->getForcedMaintenancePriorityId() == $objPropertyMaintenancePriority->getMaintenancePriorityId() && 1 == $objPropertyMaintenancePriority->getSendSms() ) ) ) {
			$objAssignedCompanyEmployee = $objClient->fetchCompanyEmployeeByCompanyEmployeeId( $objPropertyMaintenanceProblem->getEmployeeId(), $objDatabase );
		}

		if( true == valObj( $objAssignedCompanyEmployee, 'CCompanyEmployee' ) ) {
			$strMobilePhoneNumber = $objAssignedCompanyEmployee->getPhoneNumberByPhoneNumberTypeId( CPhoneNumberType::MOBILE );
		}

		if( false == is_null( $strMobilePhoneNumber ) ) {

			// Preparing the sms that needs to be sent.
			$strMessage = 'Maintenance requested for ' . $objProperty->getPropertyName();

			// If the message length doesn't go beyond 160 then appending maintenance problem text to it.

			if( self::SMS_LENGTH > \Psi\CStringService::singleton()->strlen( $strMessage ) ) {
				$objMaintenanceProblem = CMaintenanceProblems::createService()->fetchMaintenanceProblemByIdByCid( $this->getMaintenanceProblemId(), $this->getCid(), $objDatabase );

				if( true == valObj( $objMaintenanceProblem, 'CMaintenanceProblem' ) ) {
					$strMaintenanceProblem = '. Problem: ' . $objMaintenanceProblem->getName();

					if( self::SMS_LENGTH > ( \Psi\CStringService::singleton()->strlen( $strMessage ) + \Psi\CStringService::singleton()->strlen( $strMaintenanceProblem ) ) ) {
						$strMessage .= $strMaintenanceProblem;
					}
				}
			}

			// If the message length doesn't go beyond 160 then appending maintenance priority text to it.
			if( self::SMS_LENGTH > \Psi\CStringService::singleton()->strlen( $strMessage ) ) {
				$objMaintenancePriority = \Psi\Eos\Entrata\CMaintenancePriorities::createService()->fetchMaintenancePriorityByIdByCid( $this->getMaintenancePriorityId(), $this->getCid(), $objDatabase );

				if( true == valObj( $objMaintenancePriority, 'CMaintenancePriority' ) ) {
					$strMaintenancePriority = '. Priority: ' . $objMaintenancePriority->getName();

					if( self::SMS_LENGTH > ( \Psi\CStringService::singleton()->strlen( $strMessage ) + \Psi\CStringService::singleton()->strlen( $strMaintenancePriority ) ) ) {
						$strMessage .= $strMaintenancePriority;
					}
				}
			}

			// Creating a new message which will be inserted into messages table in sms database.
			$objMessage = new CMessage();
			$objMessage->setDefaults();
			$objMessage->setCid( $objClient->getId() );
			$objMessage->setMessageTypeId( CMessageType::MAINTENANCE_NOTIFICATION );
			$objMessage->setMessageStatusTypeId( CMessageStatusType::PENDING_SEND );
			$objMessage->setPropertyId( $objProperty->getId() );
			$objMessage->setCompanyEmployeeId( $objAssignedCompanyEmployee->getId() );
			$objMessage->setMessage( $strMessage );
			$objMessage->setPhoneNumber( $strMobilePhoneNumber );
			$objMessage->setMessageDatetime( date( 'Y-m-d H:i:s' ) );
			$objMessage->setIsInbound( 0 );

			if( true == valId( $this->getCustomerId() ) && true == valId( $this->getLeaseId() ) ) {
				$objMessage->setIsResidentSms( true );
				$objMessage->setCustomerId( $this->getCustomerId() );
				$objMessage->setLeaseId( $this->getLeaseId() );
				$objMessage->setPropertyId( $objProperty->getId() );
				$objMessage->setFileTypeSystemCode( CFileType::SYSTEM_CODE_WORK_ORDER );
				$objMessage->setDocumentManager( $objDocumentManager );
				$objMessage->setClientDatabase( $objDatabase );
			}

			if( false == $objMessage->insert( $intCompanyUserId, $objSmsDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function checkSpecialCharacters() {

		$strPattern = self::REPLACE_SPECIAL_CHARACTERS_PATTERN;
		if( false == is_null( $this->getProblemDescription() ) ) {
			$this->setProblemDescription( preg_replace( $strPattern, ' ', strip_tags( $this->getProblemDescription() ) ) );
		}

		if( false == is_null( $this->getEntryNotes() ) ) {
			$this->setEntryNotes( replaceSpecialCharacters( $this->getEntryNotes() ) );
		}

		if( false == is_null( $this->getLocationSpecifics() ) ) {
			$this->setLocationSpecifics( replaceSpecialCharacters( $this->getLocationSpecifics() ) );
		}
	}

	public function disassociateData( $intCompanyUserId, $objDatabase ) {

		// update objects that have maintance location id on them and set them to null
		$arrobjMaintenanceRequestsByLocations = $this->fetchMaintenanceRequestsByLocations( $objDatabase );
		$arrobjMaintenanceRequestsByProblems  = $this->fetchMaintenanceRequestsByProblems( $objDatabase );

		switch( NULL ) {
			default:

				$boolIsValid = true;

				if( true == valArr( $arrobjMaintenanceRequestsByLocations ) ) {
					foreach( $arrobjMaintenanceRequestsByLocations as $objMaintenanceRequest ) {
						$objMaintenanceRequest->setMaintenanceLocationId( NULL );
						$objMaintenanceRequest->validate( VALIDATE_UPDATE );
					}
				}

				if( true == valArr( $arrobjMaintenanceRequestsByProblems ) ) {
					foreach( $arrobjMaintenanceRequestsByProblems as $objMaintenanceRequestByProblem ) {
						$objMaintenanceRequestByProblem->setMaintenanceProblemId( NULL );
						$objMaintenanceRequestByProblem->setSubMaintenanceProblemId( NULL );
						$objMaintenanceRequestByProblem->validate( VALIDATE_UPDATE );
					}
				}

				if( true == valArr( $arrobjMaintenanceRequestsByLocations ) ) {

					foreach( $arrobjMaintenanceRequestsByLocations as $objMaintenanceRequest ) {
						if( false == $objMaintenanceRequest->simpleUpdate( $intCompanyUserId, $objDatabase ) ) {
							return false;
						}
					}
				}

				if( true == valArr( $arrobjMaintenanceRequestsByProblems ) ) {

					foreach( $arrobjMaintenanceRequestsByProblems as $objMaintenanceRequestByProblem ) {
						if( false == $objMaintenanceRequestByProblem->simpleUpdate( $intCompanyUserId, $objDatabase ) ) {
							return false;
						}
					}
				}
		}

		return true;
	}

	// Adding customize field

	public function toArray() {
		$arrmixMaintenanceRequestToArray = parent::toArray();

		$arrmixMaintenanceRequestToArray['maintenance_priority_type_id']    = $this->getMaintenancePriorityTypeId();
		$arrmixMaintenanceRequestToArray['maintenance_status_type_id']      = $this->getMaintenanceStatusTypeId();
		$arrmixMaintenanceRequestToArray['name_first']                      = $this->getCompanyUserNameFirst();
		$arrmixMaintenanceRequestToArray['name_last']                       = $this->getCompanyUserNameLast();
		$arrmixMaintenanceRequestToArray['username']                        = $this->getCompanyUserUsername();
		$arrmixMaintenanceRequestToArray['maintenance_location_name']       = $this->getMaintenanceLocationName();
		$arrmixMaintenanceRequestToArray['building_name']                   = $this->getBuildingName();
		$arrmixMaintenanceRequestToArray['maintenance_category_id']         = $this->getMaintenanceCategoryId();
		$arrmixMaintenanceRequestToArray['maintenance_category_name']       = $this->getMaintenanceCategoryName();
		$arrmixMaintenanceRequestToArray['maintenance_location_type']       = $this->getMaintenanceLocationType();
		$arrmixMaintenanceRequestToArray['is_floating_maintenance_request'] = $this->getIsFloatingMaintenanceRequest();
		$arrmixMaintenanceRequestToArray['maintenance_problem_name']        = $this->getMaintenanceProblemName();
		$arrmixMaintenanceRequestToArray['is_published_problem_action']     = $this->getIsPublishedProblemAction();

		return $arrmixMaintenanceRequestToArray;
	}

	public function getEndDateExcludingWeekendsHolidays( $strStartDateTime, $intInterval, $objDatabase ) {
		// The function is used only for the Make Readies with the templates on and Exclude Closed days is set on those templates

		if( false == valStr( $strStartDateTime ) ) {
			return $strEndDateTime = NULL;
		}

		if( false == valId( $intInterval ) ) {
			return $strStartDateTime;
		}

		$objTimeZone     = CTimeZones::fetchTimeZoneByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		$strTimeZoneName = ( true == valObj( $objTimeZone, 'CTimeZone' ) ) ? $objTimeZone->getTimeZoneName() : 'America/Denver';

		if( true == valStr( $strStartDateTime ) ) {
			$strStartDateTime = getConvertedDateTime( $strStartDateTime, 'm/d/Y H:i:s', 'America/Denver', $strTimeZoneName );
		}

		$intCountWorkingDaysAdded       = 0;
		$boolIsHolidayOnEndDate         = false;
		$boolIsEndDateOnAppointmentDate = false;

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getPropertyId(), 'USE_CUSTOM_MAINTENANCE_AVAILABILITY', $this->getCid(), $objDatabase );

		// If 'Use Custom Maintenance Availability' is set to YES: display property maintenance hours
		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == $objPropertyPreference->getValue() ) {
			$arrobjPropertyHours = CPropertyHours::fetchMaintenancePropertyHoursByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		} else {
			// If 'Use Custom Maintenance Availability' is set to NO: display Property office hours
			$arrobjPropertyHours = CPropertyHours::fetchCustomPropertyOfficeHoursByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		if( true == valId( $intInterval ) && true == valArr( $arrobjPropertyHours ) ) {
			$arrstrDateHolidayClosed = []; // non-working holidays array
			$arrstrDateHolidayOpened = []; // working holidays array

			// get property holiday list
			$arrstrPropertyHolidays = CPropertyHolidays::fetchPropertyHolidayAvailabilityByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
			if( true == valArr( $arrstrPropertyHolidays ) ) {
				foreach( $arrstrPropertyHolidays as $arrstrPropertyHoliday ) {

					if( false == valStr( $arrstrPropertyHoliday['open_time'] ) && false == valStr( $arrstrPropertyHoliday['close_time'] ) ) {
						$arrstrDateHolidayClosed[$arrstrPropertyHoliday['date']] = $arrstrPropertyHoliday;
					} else {
						$arrstrDateHolidayOpened[$arrstrPropertyHoliday['date']] = $arrstrPropertyHoliday;
					}

				}
			}
			$arrobjPropertyHours = rekeyObjects( 'day', $arrobjPropertyHours );

			$boolIsFoundStartDate = false;

			while( false == $boolIsFoundStartDate ) {
				// check if the make ready is created on holiday or closed day. If it is created on holiday or closed day then increment the date by 1 day
				$intCurrentDay = date_format( new DateTime( $strStartDateTime ), 'w' );
				if( 0 == $intCurrentDay ) {
					$intCurrentDay = 7;
				}

				if( ( ( true == array_key_exists( $intCurrentDay, $arrobjPropertyHours ) && false == is_null( $arrobjPropertyHours[$intCurrentDay]->getCloseTime() ) && false == is_null( $arrobjPropertyHours[$intCurrentDay]->getOpenTime() ) ) || true == array_key_exists( date_format( new DateTime( $strStartDateTime ), 'm/d/Y' ), $arrstrDateHolidayOpened ) ) && false == array_key_exists( date_format( new DateTime( $strStartDateTime ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {
					$boolIsFoundStartDate = true;
				} else {
					$strStartDateTime = date( 'Y-m-d H:i:s', strtotime( $strStartDateTime . '+1 days' ) );
				}
			}

			$this->setScheduledStartDatetime( date( 'Y-m-d H:i:s', strtotime( $strStartDateTime ) ) );

			// add the days to the start date
			$strEndDateTime = date( 'Y-m-d H:i:s', strtotime( $strStartDateTime . ' + ' . ( int ) $intInterval . ' days' ) );

			$intEndDay = date_format( new DateTime( $strEndDateTime ), 'w' );

			// check if the End Date is a Holiday
			if( true == array_key_exists( date_format( new DateTime( $strEndDateTime ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {
				$boolIsHolidayOnEndDate = true;
			}

			// check if the end date is on appointment day
			if( true == array_key_exists( $intEndDay, $arrobjPropertyHours ) && true == is_null( $arrobjPropertyHours[$intEndDay]->getCloseTime() ) && true == is_null( $arrobjPropertyHours[$intEndDay]->getOpenTime() ) ) {
				$boolIsEndDateOnAppointmentDate = true;
			}

			if( false == array_key_exists( $intEndDay, $arrobjPropertyHours ) || true == $boolIsEndDateOnAppointmentDate || true == $boolIsHolidayOnEndDate ) {
				$strEndDateTime = date( 'Y-m-d H:i:s', strtotime( $strEndDateTime . ' + 1 days' ) );
			}

			// Increment the dates if holiday or closed day falls in between start date and end date
			While( date( 'Y-m-d', strtotime( $strStartDateTime ) ) < date( 'Y-m-d', strtotime( $strEndDateTime ) ) ) {
				$intDay = date_format( new DateTime( $strStartDateTime ), 'w' );

				if( 0 == $intDay ) {
					$intDay = 7;
				}

				if( ( ( true == array_key_exists( $intDay, $arrobjPropertyHours ) && false == is_null( $arrobjPropertyHours[$intDay]->getCloseTime() ) && false == is_null( $arrobjPropertyHours[$intDay]->getOpenTime() ) ) || true == array_key_exists( date_format( new DateTime( $strStartDateTime ), 'm/d/Y' ), $arrstrDateHolidayOpened ) ) && false == array_key_exists( date_format( new DateTime( $strStartDateTime ), 'm/d/Y' ), $arrstrDateHolidayClosed ) ) {

					if( $intCountWorkingDaysAdded == $intInterval ) {
						$strEndDateTime = date_format( new DateTime( $strStartDateTime ), 'Y-m-d H:i:s' );
						break;
					}

					$intCountWorkingDaysAdded += 1;
				}

				$strStartDateTime = date( 'Y-m-d H:i:s', strtotime( $strStartDateTime . ' + 1 days' ) );
				$strEndDateTime   = date( 'Y-m-d H:i:s', strtotime( $strEndDateTime . ' + 1 days' ) );
			}
		} else {
			$strEndDateTime = date( 'Y-m-d H:i:s', strtotime( $strStartDateTime . ' + ' . ( int ) $intInterval . ' days' ) );
		}

		return $strEndDateTime;

	}

	// function to get prefix for work order based on type of request

	public function getPrefix( $boolIsTranslate = true ) {
		if( true == $boolIsTranslate ) {
			return ( true == valId( $this->getParentMaintenanceRequestId() ) ? __( 'ST' ) : CMaintenanceRequestType::createService()->getMaintenanceRequestPrefix()[$this->getMaintenanceRequestTypeId()]['prefix'] );
		} else {
			return ( true == valId( $this->getParentMaintenanceRequestId() ) ? 'ST' : CMaintenanceRequestType::createService()->getMaintenanceRequestPrefix()[$this->getMaintenanceRequestTypeId()]['prefix_en'] );
		}
	}

	public function addFiles( $objFile ) {
		$this->m_arrobjFiles[] = $objFile;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	// This function is customized and used only for view purpose.

	public function getMaintenanceRequestStatuses() {

		if( true == valArr( $this->m_arrstrMaintenanceRequestStatuses ) ) {
			return $this->m_arrstrMaintenanceRequestStatuses;
		}

		$this->m_arrstrMaintenanceRequestStatuses = [
			self::MAINTENANCE_REQUEST_STATUS_INCOMPLETE => __( 'Incomplete' ),
			self::MAINTENANCE_REQUEST_STATUS_COMPLETE   => __( 'Complete' ),
		];

		return $this->m_arrstrMaintenanceRequestStatuses;
	}

	private function createPhoneNumber( $strPhoneNumber ) {
		require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';
		$arrmixOptions              = [];
		$arrmixOptions['extension'] = '';
		$objPhoneNumber             = new \i18n\CPhoneNumber( $strPhoneNumber, $arrmixOptions );

		return $objPhoneNumber;
	}

	public function sendSMS( $objCompanyUser, $objDatabase, $arrmixDetails = [], $objDocumentManager = NULL ) {

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$intCompanyUserId = $objCompanyUser->getId();
		} else {
			// From Resident Portal App we are not getting companyUser Obj and passing custom app user_id
			$intCompanyUserId = $objCompanyUser;
		}

		if( true == valArr( $arrmixDetails ) ) {
			$boolHasPsProductMessageCenter = true;

			if( true == valId( $this->getParentMaintenanceRequestId() ) || CMaintenanceRequestType::MAKE_READY == $this->getMaintenanceRequestTypeId() ) {
				return false;
			}

			$arrintMigrationModeOnPropertyIds = CProperties::createService()->fetchCustomMigrationModeOnPropertiesByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objDatabase );
			if( true == valArr( $arrintMigrationModeOnPropertyIds ) ) {
				return false;
			}

			if( true == isset( $arrmixDetails['is_work_order_note_sms'] ) && true == $arrmixDetails['is_work_order_note_sms'] ) {

				$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getPropertyId(), 'SEND_RESIDENT_VISIBLE_NOTES_VIA_SMS', $this->getCid(), $objDatabase );
				if( false == isset( $objPropertyPreference ) || '0' == $objPropertyPreference->getValue() ) {
					return false;
				}
			} else {

				$arrintSMSBlockedPropertyIds = ( array ) CProperties::createService()->fetchPropertyIdsByPropertyPreferenceKeyByCids( 'SMS_MAINTENANCE_REQUESTS', [ $this->getCid() ], $objDatabase );
				if( true == array_key_exists( $this->getPropertyId(), $arrintSMSBlockedPropertyIds ) ) {
					return false;
				}
			}
			$arrintLeaseStatusTypeIds = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE, CLeaseStatusType::FUTURE ];

			if( false == is_null( $this->getCustomerId() ) && true == valObj( $this->getCustomer(), 'CCustomer' ) && true == in_array( $this->getCustomer()->getLeaseStatusTypeId(), $arrintLeaseStatusTypeIds ) ) {
				$objCustomer = $this->getCustomer();
			} elseif( false == is_null( $this->getCustomerId() ) ) {
				$objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByLeaseIdByLeaseStatusTypeIdsByCid( $this->getCustomerId(), $this->getLeaseId(), $arrintLeaseStatusTypeIds, $this->getCid(), $objDatabase );
			}

			if( true == valId( $this->getCustomerId() ) ) {
				$arrmixCustomerPhoneNumber = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomMobileNumberByCustomerIdsByCid( [ $this->getCustomerId() ], $this->getCid(), $objDatabase );
			}

			$strMobileNumber = '';
			if( true == valArr( $arrmixCustomerPhoneNumber ) && true == valStr( $arrmixCustomerPhoneNumber[$this->getCustomerId()]['mobile_phone_number'] ) ) {
				$strMobileNumber = $arrmixCustomerPhoneNumber[$this->getCustomerId()]['mobile_phone_number'];
			}

			if( false == valObj( $objCustomer, 'CCustomer' ) || false == valStr( $strMobileNumber ) ) {
				return false;
			}

			$objSmsEnrollment = $objCustomer->fetchSmsEnrollmentByMessageTypeId( CMessageType::MAINTENANCE_NOTIFICATION, $objDatabase );
			if( false == valObj( $objSmsEnrollment, 'CSmsEnrollment' ) || ( true == valObj( $objSmsEnrollment, 'CSmsEnrollment' ) && false == is_null( $objSmsEnrollment->getDisabledOn() ) ) ) {
				return false;
			}

			$strMobilePhoneNumber = str_replace( '-', '', $strMobileNumber );

			if( true == isset( $arrmixDetails['is_work_order_status_update'] ) && true == $arrmixDetails['is_work_order_status_update'] && true == isset( $arrmixDetails['old_work_order'] ) && true == valObj( $arrmixDetails['old_work_order'], 'CMaintenanceRequest' ) ) {
				if( $arrmixDetails['old_work_order']->getMaintenanceStatusTypeId() == $this->getMaintenanceStatusTypeId() ) {
					return false;
				}
			}

			if( true == isset( $arrmixDetails['is_work_order_scheduled_sms'] ) && true == $arrmixDetails['is_work_order_scheduled_sms'] ) {

				$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getPropertyId(), 'HIDE_SCHEDULED_DATE', $this->getCid(), $objDatabase );
				if( true == isset( $objPropertyPreference ) && '1' == $objPropertyPreference->getValue() ) {
					return false;
				}

			}

			if( true == isset( $arrmixDetails['is_work_order_status_update'] ) && true == $arrmixDetails['is_work_order_status_update'] ) {
				$objOldMaintenanceRequest = $arrmixDetails['old_work_order'];
				if( true == in_array( $objOldMaintenanceRequest->getMaintenanceStatusTypeId(), CMaintenanceStatusType::$c_arrintClosedStatusTypes )
				    && true == in_array( $this->getMaintenanceStatusTypeId(), CMaintenanceStatusType::$c_arrintClosedStatusTypes ) ) {
					return false;
				}
			}

			$strMessage            = '';
			$intEventSubTypeId     = NULL;
			$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByPropertyIdByKeyByCid( $this->getPropertyId(), 'HIDE_MAINTENANCE_STATUS', $this->getCid(), $objDatabase );

			if( true == isset( $arrmixDetails['is_new_work_order'] ) && true == $arrmixDetails['is_new_work_order'] ) {
				$strMessage        = __( 'Work Order {%d, 0, nots} has been created.', [ $this->getId() ] );
				$intEventSubTypeId = CEventSubType::WORKORDER_OPENED_SMS;
			} elseif( true == isset( $arrmixDetails['is_work_order_status_update'] ) && true == $arrmixDetails['is_work_order_status_update'] ) {
				$objOldMaintenanceRequest = $arrmixDetails['old_work_order'];
				if( true == in_array( $this->getMaintenanceStatusTypeId(), CMaintenanceStatusType::$c_arrintClosedStatusTypes ) && true == isset( $objPropertyPreference ) && '2' == $objPropertyPreference->getValue() ) {
					$intEventSubTypeId = CEventSubType::WORKORDER_CLOSE_STATUS_UPDATE_SMS;
					if( $this->getMaintenanceStatusTypeId() == CMaintenanceStatusType::COMPLETED ) {
						$strMessage = __( 'Work Order {%d, 0, nots} has been completed.', [ $this->getId() ] );
					} else {
						if( $this->getMaintenanceStatusTypeId() == CMaintenanceStatusType::CANCELLED ) {
							$strMessage = __( 'Work Order {%d, 0, nots} has been cancelled.', [ $this->getId() ] );
						}
					}
				} else {

					$intEventSubTypeId = CEventSubType::WORKORDER_STATUS_UPDATE_SMS;

					$arrstrOldStatusName = CMaintenanceStatuses::createService()->fetchCustomMaintenanceStatusInfoByCidById( $this->getCid(), $objOldMaintenanceRequest->getMaintenanceStatusId(), $objDatabase );
					$arrstrNewStatusName = CMaintenanceStatuses::createService()->fetchCustomMaintenanceStatusInfoByCidById( $this->getCid(), $this->getMaintenanceStatusId(), $objDatabase );
					if( true == isset( $objPropertyPreference ) && '1' == $objPropertyPreference->getValue() ) {
						$strMessage = __( 'Work Order {%d, 0, nots} has been updated from {%s, 1, nots} to {%s, 2, nots}.', [ $this->getId(), $arrstrOldStatusName['status_type_name'], $arrstrNewStatusName['status_type_name'] ] );
					} elseif( true == isset( $objPropertyPreference ) && '0' == $objPropertyPreference->getValue() ) {
						$strMessage = __( 'Work Order {%d, 0, nots} has been updated from {%s, 1, nots} to {%s, 2, nots}.', [ $this->getId(), $arrstrOldStatusName['name'], $arrstrNewStatusName['name'] ] );
					} else {
						$strMessage = __( 'Work Order {%d, 0, nots} has been updated', [ $this->getId() ] );
					}
				}
			} elseif( true == isset( $arrmixDetails['is_work_order_note_sms'] ) && true == $arrmixDetails['is_work_order_note_sms'] ) {
				$intEventSubTypeId = CEventSubType::WORKORDER_NOTE_SMS;
				$strMessage        = __( 'Work Order {%d, 0, nots} note added: {%s, 1, nots}', [ $this->getId(), $arrmixDetails['note'] ] );
			} elseif( true == isset( $arrmixDetails['is_work_order_scheduled_sms'] ) && true == $arrmixDetails['is_work_order_scheduled_sms'] ) {
				$intEventSubTypeId   = CEventSubType::WORKORDER_SCHEDULED_SMS;
				$arrstrTimeZoneNames = CTimeZones::fetchSimpleTimeZoneNamesByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objDatabase );
				if( false == $arrmixDetails['is_schedule_date_update'] ) {
					$strMessage = __( 'Work Order {%d, 0, nots} scheduled for {%t, 1, DATE_NUMERIC_STANDARD} at {%t, 1, TIME_SHORT}', [ $this->getId(), CInternationalDateTime::create( $arrmixDetails['scheduled_start_datetime'], $arrstrTimeZoneNames[0]['time_zone_name'] ) ] );
				} else {
					$strMessage = __( 'Work Order {%d, 0, nots} is now scheduled for {%t, 1, DATE_NUMERIC_STANDARD} at {%t, 1, TIME_SHORT}', [ $this->getId(), CInternationalDateTime::create( $arrmixDetails['scheduled_start_datetime'], $arrstrTimeZoneNames[0]['time_zone_name'] ) ] );
				}

			} elseif( true == isset( $arrmixDetails['is_work_order_assigned_sms'] ) && true == $arrmixDetails['is_work_order_assigned_sms'] ) {
				$intEventSubTypeId = CEventSubType::WORKORDER_ASSIGNED_SMS;
				$strMessage        = __( 'Work Order {%d, 0, nots} assigned to {%s, 1, nots}', [ $this->getId(), $arrmixDetails['namefull'] ] );
			}

			// Creating a new message which will be inserted into messages table in sms database.
			$objMessage = new CMessage();
			$objMessage->setDefaults();
			$objMessage->setCid( $this->getCid() );
			$objMessage->setMessageTypeId( CMessageType::MAINTENANCE_NOTIFICATION );
			$objMessage->setMessageStatusTypeId( CMessageStatusType::PENDING_SEND );
			$objMessage->setPropertyId( $this->getPropertyId() );
			$objMessage->setCompanyEmployeeId( $this->getCompanyEmployeeId() );
			$objMessage->setPhoneNumber( $strMobilePhoneNumber );
			$objMessage->setMessageDatetime( date( 'Y-m-d H:i:s' ) );
			$objMessage->setIsInbound( 0 );

			$objMessage->setIsResidentSms( true );
			$objMessage->setCustomerId( $this->getCustomerId() );
			$objMessage->setLeaseId( $this->getLeaseId() );
			$objMessage->setFileTypeSystemCode( CFileType::SYSTEM_CODE_WORK_ORDER );
			$objMessage->setDocumentManager( $objDocumentManager );
			$objMessage->setClientDatabase( $objDatabase );

			if( self::SMS_LENGTH < \Psi\CStringService::singleton()->strlen( $strMessage ) ) {

				$strMessage = \Psi\CStringService::singleton()->str_ireplace( '’', '\'', $strMessage );

				$strFirstMessage  = \Psi\CStringService::singleton()->substr( $strMessage, 0, 150 );
				$strSecondMessage = \Psi\CStringService::singleton()->substr( $strMessage, 150 );

				$objMessageSecond = clone $objMessage;
				$objMessageThird  = clone $objMessage;

				$objMessage->setMessage( '(1/2)' . $strFirstMessage );
				$arrobjMessages[] = $objMessage;

				$objMessageSecond->setMessage( '(2/2)' . $strSecondMessage );
				$arrobjMessages[] = $objMessageSecond;

			} else {
				$objMessageSecond = clone $objMessage;

				$objMessage->setMessage( $strMessage );
				$arrobjMessages[] = $objMessage;

			}

			foreach( $arrobjMessages as $objMessage ) {
				if( false == $objMessage->insert( $intCompanyUserId, NULL, false, false ) ) {
					return false;
				}
			}

			$this->setResidentSMS( $strMessage );
			$this->createWorkOrderEvent( $intEventSubTypeId, $objCompanyUser, NULL, NULL, $objDatabase, false );

			return true;
		}

	}

	public function setDefaultMaintenancePriority( $objDatabase ) {

		if( false == valId( $this->getMaintenancePriorityId() ) ) {

			if( true == valId( $this->getMaintenanceProblemId() ) ) {
				// Setting problem default priority from property level
				$objPropertyMaintenanceProblem = \Psi\Eos\Entrata\CPropertyMaintenanceProblems::createService()->fetchPropertyMaintenanceProblemByPropertyIdByMaintenanceProblemIdByCid( $this->getPropertyId(), $this->getMaintenanceProblemId(), $this->getCid(), $objDatabase );
				if( true == valObj( $objPropertyMaintenanceProblem, 'CPropertyMaintenanceProblem' ) && true == valId( $objPropertyMaintenanceProblem->getForcedMaintenancePriorityId() ) ) {
					$this->setMaintenancePriorityId( $objPropertyMaintenanceProblem->getForcedMaintenancePriorityId() );
					return true;
				} else {
					// Setting problem default priority from company level
					$objMaintenanceProblem = CMaintenanceProblems::createService()->fetchMaintenanceProblemByIdByCid( $this->getMaintenanceProblemId(), $this->getCid(), $objDatabase );
					if( true == valObj( $objMaintenanceProblem, 'CMaintenanceProblem' ) && true == valId( $objMaintenanceProblem->getMaintenancePriorityId() ) ) {
						$this->setMaintenancePriorityId( $objMaintenanceProblem->getMaintenancePriorityId() );
						return true;
					}
				}
			}

			// Setting default maintenance priority from property as medium > low > high
			$objDefaultPropertyMaintenancePriority = CPropertyMaintenancePriorities::createService()->fetchDefaultPropertyMaintenancePriorityByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
			if( true == valObj( $objDefaultPropertyMaintenancePriority, 'CPropertyMaintenancePriority' ) ) {
				$this->setMaintenancePriorityId( $objDefaultPropertyMaintenancePriority->getMaintenancePriorityId() );
				return true;
			} else {
				// Setting default maintenance MEDIUM priority from Company
				$objCompanyMaintenanceDefaultPriority = \Psi\Eos\Entrata\CMaintenancePriorities::createService()->fetchMaintenancePriorityByCidByCode( $this->getCid(), CMaintenancePriority::CODE_MAINTENANCE_PRIORITY_MEDIUM, $objDatabase );
				if( true == valObj( $objCompanyMaintenanceDefaultPriority, 'CMaintenancePriority' ) ) {
					$this->setMaintenancePriorityId( $objCompanyMaintenanceDefaultPriority->getId() );
					return true;
				}
			}
		}

		return true;
	}

	public function getDefaultMaintenanceStatus( $objDatabase ) {

		$objDefaultMaintenanceStatus = CMaintenanceStatuses::createService()->fetchDefaultMaintenanceStatusByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objDefaultMaintenanceStatus, 'CMaintenanceStatus' ) ) {
			$objMaintenanceStatus = $objDefaultMaintenanceStatus;
		} else {
			$arrobjOpenTypeMaintenanceStatuses = CMaintenanceStatuses::createService()->fetchPublishedMaintenanceStatusesByPropertyIdsByMaintenanceStatusTypeIdsByCid( [ $this->getPropertyId() ], [ CMaintenanceStatusType::OPEN ], $this->getCid(), $objDatabase );
			if( true == valArr( $arrobjOpenTypeMaintenanceStatuses ) ) {
				$arrobjMaintenanceStatuses = array_change_key_case( rekeyObjects( 'name', $arrobjOpenTypeMaintenanceStatuses ) );
				if( true == array_key_exists( 'open', $arrobjMaintenanceStatuses ) ) {
					$objMaintenanceStatus = $arrobjMaintenanceStatuses['open'];
				} else {
					if( true == array_key_exists( 'new', $arrobjMaintenanceStatuses ) ) {
						$objMaintenanceStatus = $arrobjMaintenanceStatuses['new'];
					} else {
						$objMaintenanceStatus = current( $arrobjOpenTypeMaintenanceStatuses );
					}
				}
			}
		}
		if( false === valObj( $objMaintenanceStatus, 'CMaintenanceStatus' ) ) {
			// Fetch OPEN maintenance status type id if exists set it
			$objMaintenanceStatus = CMaintenanceStatuses::createService()->fetchMaintenanceStatusByCidByCode( $this->getCid(), CMaintenanceStatusType::CODE_MAINTENANCE_STATUS_TYPE_OPEN, $objDatabase );
		}

		return $objMaintenanceStatus;
	}

	public function getAttachmentsPath( $arrobjFileAssociations, $objObjectStorageGateway = NULL, $objDatabase = NULL ) {

		$intTotalAttachmentInEmailCount = 1;
		$intMaxAttachmentInEmail        = 5;
		$arrmixAttachmentFilePath       = [];

		usort( $arrobjFileAssociations, function( $objFirst, $objSecond ) {
			return ( $objFirst->getShowInPortal() < $objSecond->getShowInPortal() );
		} );

		if( false == is_null( $objObjectStorageGateway ) ) {
			foreach( $arrobjFileAssociations as $objFileAssociation ) {
				if( $intTotalAttachmentInEmailCount <= $intMaxAttachmentInEmail ) {

					$arrmixFileOptions = [
						'cid'        => $this->getCid(),
						'objectId'   => $objFileAssociation->getFileId(),
						'outputFile' => 'temp'
					];

					$objMaintenanceFilesLibrary      = new CMaintenanceFilesLibrary();
					$objObjectStorageGatewayResponse = $objMaintenanceFilesLibrary->getFile( $objObjectStorageGateway, $arrmixFileOptions, $objDatabase );
					if( false == $objObjectStorageGatewayResponse->hasErrors() ) {
						$arrmixAttachmentFilePath[$intTotalAttachmentInEmailCount]['file_path']         = $objObjectStorageGatewayResponse['outputFile'];
						$arrmixAttachmentFilePath[$intTotalAttachmentInEmailCount]['show_in_portal']    = ( int ) $objFileAssociation->getShowInPortal();
						$intTotalAttachmentInEmailCount++;
					}
				} else {
					break;
				}
			}
		}

		return $arrmixAttachmentFilePath;
	}

	public function setLocaleCode( $objProperty, $objDatabase, $objCustomerOrCompanyEmployee = NULL ) {
		$strPreferredLocaleCode = CLanguage::ENGLISH;
		if( true == valObj( $objProperty, CProperty::class ) && false == valObj( $this->getProperty(), CProperty::class ) ) {
			$this->setProperty( $objProperty );
		}
		if( ( true == valObj( $objCustomerOrCompanyEmployee, 'CCustomer' ) || true == valObj( $objCustomerOrCompanyEmployee, 'CCompanyEmployee' ) ) && true == valStr( $objCustomerOrCompanyEmployee->getPreferredLocaleCode() ) ) {
			$strPreferredLocaleCode = $objCustomerOrCompanyEmployee->getPreferredLocaleCode();
		} elseif( true == valObj( $objProperty, 'CProperty' ) && true == valStr( $objProperty->getLocaleCode() ) ) {
			$strPreferredLocaleCode = $objProperty->getLocaleCode();
		}
		CLocaleContainer::createService()->setPreferredLocaleCode( $strPreferredLocaleCode, $objDatabase );
	}

	public function calculateScheduledStartDate( $objMaintenanceTemplate, $strMoveOutDate, $objDatabase = NULL ) {

		if( false == valObj( $objMaintenanceTemplate, 'CMaintenanceTemplate' ) ) {
			$objMaintenanceTemplate = \Psi\Eos\Entrata\CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidById( $this->getCid(), $this->getMaintenanceTemplateId(), $objDatabase );
		}

		if( true == valObj( $objMaintenanceTemplate, 'CMaintenanceTemplate' ) && true == $objMaintenanceTemplate->getIsMoveOutDateAsStartDate() ) {
			return strtotime( $strMoveOutDate );
		} else {
			return strtotime( ' + 1 days ', strtotime( $strMoveOutDate ) );
		}
	}

}

?>
