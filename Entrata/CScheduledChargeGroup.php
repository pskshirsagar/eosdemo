<?php

class CScheduledChargeGroup extends CBaseScheduledChargeGroup {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStraightLineAmountMonthly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostStraightLineAdjustments() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updateStraightLineMonthlyAmount( $boolProrationEnabled ) {
		$fltStraightLineMonthlyAmount = bcdiv( $this->getTotalValue(), $this->getDuration( $this->getStartDate(), $this->getEndDate(), $boolProrationEnabled ), 10 );
		$this->setStraightLineAmountMonthly( round( $fltStraightLineMonthlyAmount, 2 ) );
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( is_null( $this->getLeaseIntervalId() ) ) {
			throw new \Exception( __( 'Lease Interval Id cannot be null.' ) );
		}
		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( is_null( $this->getLeaseIntervalId() ) ) {
			throw new \Exception( __( 'Lease Interval Id cannot be null.' ) );
		}
		$boolIsProrationEnabled = \CStrings::strToBool( \Psi\Eos\Entrata\CPropertyChargeSettings::create()->fetchStraightLineProrateChargesByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase ) );
		$this->updateStraightLineMonthlyAmount( $boolIsProrationEnabled );
		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function getDuration( $strStartDate, $strEndDate, $boolProrationEnabled = true ) {

		$strStartYear = date( 'Y', strtotime( $strStartDate ) );
		$strEndYear = date( 'Y', strtotime( $strEndDate ) );

		$strStartMonth = date( 'm', strtotime( $strStartDate ) );
		$strEndMonth = date( 'm', strtotime( $strEndDate ) );

		if( !$boolProrationEnabled ) {
			return ( ( $strEndYear - $strStartYear ) * 12 ) + ( $strEndMonth - $strStartMonth ) + 1;
		}

		$fltLenOfFirstMonth = $this->getLengthOfMonth( $strStartDate );
		$fltLenOfLastMonth = round( bcdiv( ( int ) date( 'j', strtotime( $strEndDate ) ), ( int ) date( 't', strtotime( $strEndDate ) ), 12 ), 10 );
		$intMiddleMonths = ( ( $strEndYear - $strStartYear ) * 12 ) + ( $strEndMonth - $strStartMonth ) - 1;
		$intDuration = $fltLenOfFirstMonth + $intMiddleMonths + $fltLenOfLastMonth;

		return $intDuration;
	}

	public function getLengthOfMonth( $strDate ) {
		$intDaysInMonth = ( int ) date( 't', strtotime( $strDate ) );
		$intDayOfDate = ( int ) date( 'j', strtotime( $strDate ) );
		$intDays = $intDaysInMonth - $intDayOfDate + 1;
		return round( bcdiv( $intDays, $intDaysInMonth, 12 ), 10 );
	}

}
?>