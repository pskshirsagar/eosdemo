<?php

class CMaintenanceAvailabilityException extends CBaseMaintenanceAvailabilityException {

    protected $m_strCompanyHolidayName;

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, true, $boolDirectSet );

    	if( true == isset( $arrValues['company_holiday_name'] ) ) $this->setCompanyHolidayName( $arrValues['company_holiday_name'] );

    	return;
    }

    public function setCompanyHolidayName( $strCompanyHolidayName ) {
    	$this->m_strCompanyHolidayName = $strCompanyHolidayName;
    }

    /**
     * Get Functions
     */

    public function getCompanyHolidayName() {
    	return $this->m_strCompanyHolidayName;
    }

    /**
     * validate Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyHolidayId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsAvailableAm() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsAvailablePm() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDate() {

    	$boolIsValid = true;

		if( false == isset( $this->m_strDate ) || false == CValidation::validateDate( $this->m_strDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', __( 'A valid date is required.' ) ) );
		}

		return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valDate();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>