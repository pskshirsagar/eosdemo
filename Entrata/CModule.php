<?php

class CModule extends CBaseModule {

	const PLACEHOLDER_MODULE_THRESHOLD = 1000000; // Module id below this ID is a system module and above this ID is a placeholder modules. Custom sequence custom_modules_id_seq is used for this?

	const PROPERTY_SYSTEM		 						= 1;
	const PROPERTY_SYSTEM_MEDIA_TAB 					= 3;
	const WEBSITE_SYSTEM			 					= 4;
	const CUSTOMERS_SYSTEM_RESIDENT						= 15;
	const PROSPECT_SYSTEM_LEAD 							= 17;
	const APPLICANT_SYSTEM 								= 1905;
	const ALL_LEADS 									= 36;
	const ALL_APPLICANTS 								= 1906;
	const REPORT_SYSTEM 								= 20;
	const REPORT_SYSTEM_NEW								= 3265;
	const FAVORITE_REPORT_TAB 							= 2047;
	const REPORT_INBOX_TAB 								= 2046;
	const SAVED_FILTERS_ID 								= 2051;
	const REPORT_QUEUE_ID 								= 2782;
	const MANAGE_REPORTS_ID 							= 3005;
	const REPORT_VERIFICATION_MODULE_ID 				= 3221;
	const RESIDENTS										= 21;
	const ACCOUNTING_SYSTEM								= 22;
	const GLOBAL_SETTINGS_SYSTEM						= 24;
	const RESIDENT_SYSTEM_LEDGER						= 26;
	const RESIDENT_SYSTEM_LEASE							= 27;
	const RESIDENT_SYSTEM_SCHEDULED_PAYMENTS_TAB		= 28;
	const ILS_PORTAL_SYSTEM								= 115;
	const ILS_PORTAL_CRAIGSLIST_TAB						= 122;
	const RENEWALS										= 138;
	const MESSAGE_CENTER								= 152;
	const PACKAGES										= 157;
	const HELP_SYSTEM									= 158;
	const MERCHANT_CHANGE_REQUEST						= 167;
	const GENERAL_DASHBOARD								= 178;
	const GENERAL_MY_DASHBOARD							= 181;
	const LEASING_CENTER								= 182;
	const WORK_ORDERS									= 192;
	const ACCOUNTING_JOURNAL_ENTRY						= 199;
	const NOTICE_LEASES									= 211;
	const LEASES										= 218;
	const RESIDENT_LEASES_PETS							= 227;
	const TRAINING 										= 231;
	const APPROVE_LEASES_ACTION							= 244;
	const APPROVE_APPLICATIONS_ACTION					= 246;
	const ACCOUNTING_SYSTEM_PURCHASE_ORDER				= 270;
	const RENEWALS_RENEW_RESIDENTS						= 275;
	const RENEWALS_CREATE_OFFERS						= 276;
	const APPROVE_PENDING_RENEWALS						= 277;
	const DENIED_RENEWALS								= 278;
	const APPROVE_DENY_OFFER_ACTION						= 279;
	const ACCOUNTING_SYSTEM_INVOICE						= 284;
	const ACCOUNTING_SYSTEM_PAYMENT						= 286;
	const PENDING_INVOICES								= 305;
	const ADD_EDIT_TICKETS_AND_TASKS_ACTION				= 308;
	const RELEASE_NOTES_FOR_ALL_PRODUCTS_ACTION			= 313;
	const UPCOMING_FEATURES_FOR_ALL_PRODUCTS_ACTION		= 314;
	const VIEW_APPROVED_APPLICATIONS					= 319;
	const VIEW_LEASE_COMPLETED_APPLICATIONS				= 320;
	const INVOICES_POST_MONTH 							= 341;
	const CHECKS_POST_MONTH 							= 342;
	const GENERAL_JOURNAL_POST_MONTH					= 343;
	const REPORT_SYSTEM_COMMUNICATIONS_TAB				= 349;
	const MESSAGE_CENTER_RESIDENT						= 353;
	const MESSAGE_CENTER_PROSPECT						= 354;
	const MESSAGE_CENTER_EMPLOYEE						= 355;
	const MESSAGE_CENTER_VENDOR							= 3067;
	const RESIDENT_SYSTEM_REVERSE_CHARGES				= 362;
	const APPS											= 363;
	const AR_POST_MONTH									= 374;
	const COLLECTIONS									= 376;
	const ABILITY_TO_BACKDATE_RENEWAL_OFFER				= 390;
	const TRAINING_DASHBOARD							= 393;
	const ALLOW_MOVE_OUT_AND_TRANSFER_ROOMATE			= 396;
	const INSPECTION_MANAGER							= 439;
	const EDIT_CHARGES_ACTION							= 445;
	const ALLOW_RESPONSIBLE_OCCUPANTS_UNDER_AGE			= 448;
	const VIEW_PRINT_EMAIL_QUOTES						= 449;
	const PERFORM_FINANCIAL_MOVE_OUT					= 453;
	const EDIT_MOVE_IN_DATE								= 462;
	const EDIT_MOVE_IN_DATE_AND_DONT_UPDATE_LEDGER		= 463;
	const APPROVE_APPLICATION_WITHOUT_SCREENING			= 469;
	const HELP 											= 472;
	const VIEW_COUNTER_SIGN_REQUIRED_FILES				= 481;
	const COMBINE_LEADS									= 484;
	const RESIDENT_SYSTEM_EDIT_MONTH_TO_MONTH_AMOUNT	= 493;
	const LEAD_EDIT_MONTH_TO_MONTH_AMOUNT				= 3734;
	const PERFORM_REVERSE_MOVE_IN						= 494;
	const FINANCIAL										= 509;
	const EDIT_RATES									= 527;
	const PRICING_TOOLS									= 528;
	const WEBSITES										= 542;
	const ONLINE_APPLICATION							= 543;
	const APPLICATION_FIELD_OPTIONS						= 544;
	const CANCEL_LEADS									= 553;
	const ARCHIVE_LEADS									= 4028;
	const COMPANY_SETUP									= 554;
	const SCREENING_SETUP								= 601;
	const MERCHANT_ACCOUNT								= 604;
	const LEADS											= 610;
	const MOVE_IN_MOVE_OUT								= 614;
	const LEAD_SOURCES									= 611;
	const COMPANY_MAINTENANCE							= 615;
	const COMPANY_MAINTENANCE_PRIORITIES				= 616;
	const COMPANY_MAINTENANCE_STATUSES					= 617;
	const COMPANY_MAINTENANCE_LOCATIONS					= 618;
	const COMPANY_MAINTENANCE_PROBLEM_CATEGORIES		= 619;
	const COMPANY_MAINTENANCE_PROBLEMS					= 620;
	const BILLING_ACCOUNTS_AGING_REPORT					= 712;
	const CL_ADS										= 3060;
	const MESSAGE_CENTER_ONCE_EMAIL						= 969;
	const MESSAGE_CENTER_RECURRING_EMAIL				= 970;
	const MESSAGE_CENTER_AUTOMATED_EMAIL				= 971;
	const MESSAGE_CENTER_EMAIL_TEMPLATE_LIBRARY			= 3325;
	const BILLING_ACCOUNTS_INVOICES_REPORT				= 998;
	const TRANSFERS										= 1418;
	const CONTACT_METHODS								= 1505;
	const MEDIA 										= 1508;
	const PROPERTY_FLOORPLANS							= 1510;
	const UNIT_TYPES									= 1511;
	const PROPERTY_UNITS								= 1512;
	const PROPERTY_FPU_SITEPLAN_BUILDINGS_AND_FLOORS	= 1514;
	const PROPERTY_DETAILS_COMPETITORS					= 1524;
	const PROPERTY_BUILDINGS							= 2484;
	const PROPERTY_AFFORDABLE							= 2505;
	const PROPERTY_AFFORDABLE_HUD						= 2506;
	const PROPERTY_AFFORDABLE_TAX_CREDITS				= 2507;
	const PROPERTY_AFFORDABLE_INCOME_LIMITS				= 2508;
	const TAX_CREDIT_COMPLIANCE							= 2995;
	const PROPERTY_LEASING_LEASE_TERMS					= 2322;
	const PROPERTY_LEASING_LEASE_EXPIRATION_LIMITS		= 1552;
	const PROPERTY_LEASING_LEASE_CLAUSES_EXCLUSIONS		= 3022;
	const MESSAGE_CENTER_EMAIL_ADMIN 					= 997;
	const DOCUMENT_MANAGEMENT			 				= 2054;
	const RESIDENT_VERIFY_VIEW_SCREENING_PACKAGES 		= 3348;
	const CALL_TRACKER_LOGSXXX						 	= 1983;
	const TENANTXXX										= 2781;
	const TENANT_FINANCIALXXX							= 3504;
	const TENANT_DOCUMENTXXX							= 3507;
	const TENANT_FINANCIALXXX_RENT_STEPS				= 3508;
	const TENANT_LEASE_TERM_RENT_AND_FEESXXX			= 3738;

	const SCHEDULE_CHARGESXXX							= 1148;
	const CUSTOMER_DOCUMENTSXXX							= 154;
	// Property Tab
	const PROPERTY_FLOORPLAN_UNITS_SPACE_OPTIONS		= 2303;

	// Marketing Tab
	const PROPERTY_MARKETING_PP_GENERAL					= 1527;
	const PROPERTY_MARKETING_PP_MEDIA					= 1529;
	const PROPERTY_MARKETING_PP_CORPORATE_SEARCH		= 1531;
	const PROPERTY_MARKETING_PP_PROPERTY_WRAPPER		= 1533;
	const PROPERTY_MARKETING_DESCRIPTIONS				= 1534;
	const PROPERTY_MARKETING_LEAD_SOURCES				= 1535;
	const PROPERTY_MARKETING_PP_LIMITED_DISPLAY			= 1532;
	const PROPERTY_MARKETING_PP_TOUR            		= 3838;

	// Leasing Tab
	const PROPERTY_LEASING_GOALS						= 1559;
	const PROPERTY_LEASING_EVENT_RESULTS_EVENT_RESULTS	= 1564;
	const PROPERTY_LEASING_OCCUPANT_TYPES				= 2272;
	const PROPERTY_LEASING_EVENT_RESULTS				= 2282;
	const PROPERTY_LEASING_LEASING_CENTER				= 2562;
	const PROPERTY_LEASING_APPLICATION_QUOTES			= 2140;
	const PROPERTY_LEASING_LEADS_WAITLIST				= 1542;

	// Resident Tab
	const PROPERTY_RESIDENT_BLACKOUT_DAYS				= 2475;
	const PROPERTY_RESIDENTS_RENEWALS_UPSELLS			= 1579;
	const PROPERTY_RESIDENTS_MAINTENANCE_EXPENSE        = 2228;
	const PROPERTY_RESIDENTS_TRANSFERS_GENERAL			= 2981;
	const PROPERTY_RESIDENTS_TRANSFERS_REASONS			= 2984;
	const PROPERTY_RESIDENTS_MOVEIN_MOVEIN				= 1567;
	const PROPERTY_RESIDENTS_MOVEOUT_MOVEOUT			= 1571;
	const PROPERTY_RESIDENTS_MOVEOUT_COLLECTIONS		= 1576;
	const PROPERTY_RESIDENTS_PARCEL_ALERT				= 1590;
	const PROPERTY_RESIDENT_TRANSFERS_QUOTES			= 2983;
	const PROPERTY_RESIDENT_MAINTENANCE_MAKE_READIES	= 2620;
	const PROPERTY_RESIDENT_RP_RESIDENT_OPTIONS			= 3028;
	const PROPERTY_RESIDENTS_MOVEIN_MOVEIN_SCHEDULER    = 3379;

	// Financial Tab
	const PROPERTY_FINANCIAL_BANKING					= 1612;
	const PROPERTY_FINANCIAL_BANK_ACCOUNTS				= 1613;
	const PROPERTY_FINANCIAL_DELINGQUENCY				= 3131;
	const PROPERTY_FINANCIAL_CAMS						= 2769;
	const PROPERTY_FINANCIAL_NOTICES					= 2177;
	const PROPERTY_FINANCIAL_CHARGES_DEPOSITE_INTERESTS	= 2243;
	const PROPERTY_FINANCIAL_CHARGES_ALLOCATIONS		= 3732;

	// Communication Tab
	const PROPERTY_COMMUNICATION_GENERAL_MISC			= 1621;
	const PROPERTY_COMMUNICATION_LEAD_SCHEDULING_NEW_LEADS	= 1623;

	// Financial move outs tab
	const DASHBOARD_PERFORM_FINANCIAL_MOVE_OUTS			= 3869;
	const DASHBOARD_REVISE_FINANCIAL_MOVE_OUTS			= 3870;
	const DASHBOARD_FINALIZE_FINANCIAL_MOVE_OUTS		= 3871;

	const PROPERTY_COMMERCIAL_UNIT_BUILDING_AND_FLOORS	= 4904;
	const PROPERTY_COMMERCIAL_UNIT_SUITES 				= 4901;

	const PROPERTY_LEASING_APPLICATION_STUDENT_PREFERENCES	= 1549;

	const NEW_DASHBOARD 								= 1673;

	const DASHBOARD_APPROVALS							= 1674;
	const DASHBOARD_APPROVALS_APPLICATIONS				= 1686;
	const DASHBOARD_APPROVALS_ESIGN_DOCS				= 1691;
	const DASHBOARD_APPROVALS_DOCUMENT_VERIFICATION     = 3568;
	const DASHBOARD_APPROVALS_LEASE_ESIGN_DOCS			= 2942;
	const DASHBOARD_APPROVALS_INDIVIDUAL_ESIGN_DOCS		= 2943;
	const DASHBOARD_APPROVALS_PAYMENTS					= 1693;
	const DASHBOARD_APPROVALS_AWAITING_PAYMENTS			= 2944;
	const DASHBOARD_APPROVALS_REVERSALS_PAYMENTS		= 2945;
	const DASHBOARD_APPROVALS_INSPECTIONS				= 1697;
	const DASHBOARD_APPROVALS_RENEWALS					= 1698;
	const DASHBOARD_APPROVALS_TRANSFERS					= 3023;
	const DASHBOARD_APPROVALS_POS						= 1700;
	const DASHBOARD_APPROVALS_INVOICES					= 1701;
	const DASHBOARD_APPROVALS_CHECKS					= 1702;
	const DASHBOARD_APPROVALS_UTILITIES					= 2061;
	const DASHBOARD_APPROVALS_PRICING					= 2488;
	const DASHBOARD_APPROVALS_FMO						= 2597;
	const DASHBOARD_APPROVALS_SCREENING					= 3034;
	const DASHBOARD_APPROVALS_JOBS					    = 3181;
	const DASHBOARD_APPROVALS_TRANSACTIONS				= 3290;
	const DASHBOARD_APPROVALS_BUDGET_WORKSHEETS         = 3841;
	const DASHBOARD_APPROVALS_SERVICES                  = 3909;

	const PURCHASE_ORDER_SYSTEMS_PO_ENTRY               = 292;

	const DASHBOARD_LEADS								= 1675;
	const DASHBOARD_LEADS_ANSWERED_CALLS				= 1714;
	const DASHBOARD_LEADS_ASSIGN_AGENTS					= 1710;
	const DASHBOARD_LEADS_FOLLOW_UP						= 1712;
	const DASHBOARD_LEADS_NEVER_CONTACTED				= 1711;
	const DASHBOARD_LEADS_CONTACT_NEEDED        		= 3000;
	const DASHBOARD_LEADS_NOT_PROGRESSING		        = 3001;
	const DASHBOARD_LEADS_TOURS		                    = 3850;

	const DASHBOARD_LEADS_REFERRALS						= 1713;
	const DASHBOARD_LEADS_UNKNOWN_EMAIL_LEADS			= 2526;
	const DASHBOARD_LEADS_POTENTIAL_LEADS				= 2998;

	const DASHBOARD_LEADS_MARKETING_ERRORS				= 3910;

	const DASHBOARD_APPLICANTS							= 1676;
	const DASHBOARD_APPLICANTS_ASSIGN_UNITS				= 2369;
	const DASHBOARD_APPLICANTS_FOLLOW_UP				= 1699;
	const DASHBOARD_APPLICANTS_GENERATE_LEASES			= 1703;
	const DASHBOARD_APPLICANTS_MISSING_INSURANCE		= 1704;
	const DASHBOARD_APPLICANTS_CONTACT_NEEDED        	= 3002;
	const DASHBOARD_APPLICANTS_NOT_PROGRESSING		    = 3003;
	const DASHBOARD_APPLICANTS_NEVER_CONTACTED		    = 3017;
	const DASHBOARD_APPLICANTS_PROCESS_SCREENING		= 3035;

	const DASHBOARD_RESIDENTS							= 1677;
	const DASHBOARD_RESIDENTS_FOLLOW_UP					= 1715;
	const DASHBOARD_RESIDENTS_MOVEIN_REVIEWS			= 1716;
	const DASHBOARD_RESIDENTS_NOT_PROGRESSING			= 3012;
	const DASHBOARD_RESIDENTS_MOVEINS					= 1717;
	const DASHBOARD_RESIDENTS_EVICTIONS					= 3233;
	const DASHBOARD_RESIDENTS_RENEWALS					= 1718;
	const DASHBOARD_RESIDENTS_TRANSFERS					= 1719;
	const DASHBOARD_RESIDENTS_RENTABLE_ITEMS			= 1720;
	const DASHBOARD_RESIDENTS_INSURANCE					= 1721;
	const DASHBOARD_RESIDENTS_MOVEOUTS					= 1947;
	const DASHBOARD_RESIDENTS_REVIEWS					= 2027;
	// The module id is available as "2980" in 'modules' table.
	const DASHBOARD_RESIDENTS_RECERTIFICATION_NOTICES	= 2980;
	const DASHBOARD_RESIDENTS_SOCIAL_MEDIA				= 2993;
	const DASHBOARD_RESIDENTS_POSTING_TOOL				= 3222;
	const DASHBOARD_RESIDENTS_GOOGLE_QA				    = 3770;
	const DASHBOARD_RESIDENTS_VEHICLES					= 3241;
	const DASHBOARD_RESIDENTS_MEGAREVIEWS				= 3322;
	const DASHBOARD_RESIDENTS_CORPORATE_COMPLAINTS		= 3344;

	const DASHBOARD_RESIDENT_PORTAL						 = 1678;
	const DASHBOARD_RESIDENT_PORTAL_AMENITY_RESERVATIONS = 1705;
	const DASHBOARD_RESIDENT_PORTAL_CLASSIFIEDS			 = 1706;
	const DASHBOARD_RESIDENT_PORTAL_CLUBS				 = 1707;
	const DASHBOARD_RESIDENT_PORTAL_CONCIERGE			 = 1708;
	const DASHBOARD_RESIDENT_PORTAL_EVENTS				 = 1709;

	const DASHBOARD_MAINTENANCE						 	= 1679;
	const DASHBOARD_MAINTENANCE_ASSIGN_WORK_ORDERS		= 1694;
	const DASHBOARD_MAINTENANCE_INSPECTIONS				= 1696;
	const DASHBOARD_MAINTENANCE_WORK_ORDERS				= 1695;
	const DASHBOARD_MAINTENANCE_RECURRING_WORK_ORDERS	= 2743;
	const DASHBOARD_MAINTENANCE_MAKE_READIES			= 2744;
	// The module id is available as "3024" in 'modules' table.
	const DASHBOARD_MAINTENANCE_MANUAL_CONTACTS			= 3024;

	const DASHBOARD_FINANCIAL							= 1680;
	const DASHBOARD_FINANCIAL_PAYMENTS					= 1684;
	const DASHBOARD_FINANCIAL_CHECK21					= 1685;
	const DASHBOARD_FINANCIAL_DEPOSITS					= 1687;
	const DASHBOARD_FINANCIAL_NOTICES					= 1688;
	const DASHBOARD_FINANCIAL_NSF_NOTICES			    = 2963;
	const DASHBOARD_FINANCIAL_DELINQENCIES_NOTICES		= 2962;
	const DASHBOARD_FINANCIAL_REPAYMNET_NOTICES			= 2964;

	const DASHBOARD_FINANCIAL_COLLECTIONS				= 1689;
	const DASHBOARD_FINANCIAL_DELIVERY_ERRORS			= 1690;
	const DASHBOARD_FINANCIAL_MOVEOUTS					= 1692;
	const DASHBOARD_FINANCIAL_ACCELERATED_RENT_CREDITS  = 2977;
	const DASHBOARD_FINANCIAL_VENDORACCESS              = 3184;
	const DASHBOARD_FINANCIAL_SEPA_FILES				= 3655;

	const DASHBOARD_SUPPORT 							= 1681;
	const DASHBOARD_SUPPORT_TRAINING 					= 1682;
	const DASHBOARD_SUPPORT_TICKETS						= 1683;

	const CANCEL_RENEWAL								= 1990;
	const START_NEW_INSPECTION							= 536;
	const CREATE_NEW_INSPECTIONS						= 2109;
	const OUTSTANDING_INSPECTION_NEW					= 2110;
	const REVIEW_INSPECTION_NEW							= 2111;
	const DELETE_INSPECTION_NEW							= 2112;
	const COMPLETE_RESIDENT_INSPECTIONS			        = 3462;
	const LEASE_DOCUMENTS_SETUP							= 649;
	const OTHER_LEASE_DOCUMENTS_SETUP					= 651;
	const CUSTOMER_PACKAGES								= 2074;
	const TOOLS											= 2380;
	const CALENDAR										= 2287;

	// Reputation Adviser & Posting tool
	const TOOLS_REPUTATION_MODULE						= 1019;
	const TOOLS_REPUTATION_REVIEWS_MODULE				= 3294;
	const TOOLS_REPUTATION_POSTING_TOOL_MODULE			= 3167;
	const MANAGE_SOCIAL_MEDIA_ACCOUNTS	                = 3173;
	const TOOLS_REPUTATION_GOOGLE_QA	                = 3765;
	const ADD_EDIT_AND_DELETE_ANSWERES	                = 3769;

	// Occurances found in this same file.
	const RESIDENT_SYSTEM_FINANCIAL						= 220;
	const VIEW_UNINSURED_LEASE_COMPLETED_APPLICATIONS	= 337;
	const RESIDENT_VERIFY_OVERRIDE_PERMISSIONS			= 350;
	const RESIDENT_VERIFY_RESCREEN_PERMISSION			= 372;
	const RESIDENT_VERIFY_VIEW_REPORT_PERMISSION		= 432;

	const SETUP_PROPERTIES_BETA							= 1900;
	const SETUP_PROPERTIES_BETA_SETUP					= 2293;
	const SETUP_PROPERTIES_BETA_SETTINGS_TEMPLATES		= 1729;
	const SETUP_PROPERTIES_BETA_ADD_EDIT_PROPERTY		= 1211;

	const SETUP_WEBSITES_BETA		                    = 2398;
	const ALLOW_PAYMENT_WARNING_OVERRIDE				= 2267;
	const ID_IMPLEMENTATION_PORTAL_COMPANY_SETUP		= 2292;
	const OVERRIDE_CERTIFIED_PAYMENTS   				= 2134;

	const RESIDENT_SYSTEM_ADD_SCHEDULED_CHARGES			= 2464;
	const RESIDENT_SYSTEM_EDIT_SCHEDULED_CHARGES		= 2465;
	const RESIDENT_SYSTEM_DELETE_SCHEDULED_CHARGES		= 2466;
	const RESIDENT_SYSTEM_ADD_SCHEDULED_CREDITS			= 2612;
	const RESIDENT_SYSTEM_EDIT_SCHEDULED_CREDITS		= 2613;
	const RESIDENT_SYSTEM_ALLOW_CUSTOM_CHARGE_DAYS		= 2629;

	const RESIDENT_SYSTEM_ADD_CHARGES					= 2161;
	const RESIDENT_SYSTEM_EDIT_CHARGES					= 2162;
	const RESIDENT_SYSTEM_ADD_CREDITS					= 2163;
	const RESIDENT_SYSTEM_EDIT_CREDITS					= 2164;
	const ADJUST_LEDGER_AFTER_FINANCIAL_MOVE_OUT		= 1363;
	const VIEW_ONLY_ACCESS_IN_RESIDENT_PORTAL			= 2978;

	const WORK_ORDER_REOPEN_CLOSED_WORK_ORDERS			= 1135;
	const MAKE_READY_BOARD_NEW							= 2028;
	const INSPECTIONS_NEWXXX							= 2029;
	const CUSTOMER_WORK_ORDER_CHARGES_NEW				= 2031;
	const WORK_ORDER_CLOSE_WORK_ORDERS_NEW				= 2107;
	const WORK_ORDER_CLOSE_MAKE_READIES                 = 3053;
	const COMPLETE_PAST_DUE_INSPECTIONS			        = 2598;
	const WORK_ORDER_EDIT_WORK_ORDER					= 1133;
	const WORK_ORDER_CREATE_WORK_ORDER_NEW				= 2104;
	const WORK_ORDER_EDIT_WORK_ORDER_NEW				= 2105;
	const WORK_ORDER_CUSTOMER_WORK_ORDER_NEW			= 2004;
	const WORK_ORDER_EDIT_CLOSED_WORK_ORDERS_NEW		= 2108;
	const WORK_ORDER_EDIT_PERMISSION_TO_ENTER			= 2669;
	const WORK_ORDER_CUSTOMER_WORK_ORDERS				= 2003;
	const WORK_ORDER_DELETE_WORK_ORDERS_NEW				= 2106;
	const WORK_ORDER_VIEW_EDIT_LABOR_PER_HOUR_RATE		= 2364;
	const WORK_ORDER_EDIT_BACK_DATE_LABOR_ENTRIES       = 3544;
	const WORK_ORDER_LOG_OVERTIME_HOLIDAY_LABOR			= 2439;
	const WORK_ORDER_EDIT_PRIORITY                      = 2991;
	const STANDARD_INVOICE								= 2510;
	const MANAGE_EXCEPTIONS                             = 3018;
	const WORK_ORDER_CREATE_BACKDATE_WORK_ORDER         = 3336;
	const WORK_ORDER_DOOR_CODE_OPERATION                = 3337;
	const WORK_ORDER_VIEW_DOOR_CODE                     = 3377;
	const WORK_ORDER_GENERATE_DOOR_CODE                 = 3378;
	const WORK_ORDER_EDIT_DUE_DATE                      = 3531;
	const WORK_ORDER_CLOSE_BACKDATE_WORK_ORDER          = 3588;
	const WORK_ORDER_CLOSE_BACKDATE_MAKE_READY          = 3589;
	const COPY_LOCATIONS_AND_PROBLEMS                   = 3621;
	const WORK_ORDER_CLOSE_RENOVATIONS                  = 3664;
	const WORK_ORDER_EDIT_MAKE_READY_COMPLETED_DATE     = 3933;

	const MY_PROFILE									= 1208;
	const LOCATION										= 1504;
	const PROPERTY_FINANCIAL_CHARGES_LEDGER_TYPES		= 1604;
	const PROPERTY_FINANCIAL_CHARGES_CHARGE_CODES		= 1605;
	const PROPERTY_FINANCIAL_PAYMENTS_INSTALLMENT_PLANS	= 1609;
	const PROPERTY_FINANCIAL_PAYMENTS_MAACCOUNTS		= 1614;
	const PROPERTY_FINANCIAL_BANKING_BANK_ACCOUNTS		= 1613;
	const PROPERTY_FINANCIAL_GENERAL_LEDGER_EXPORT		= 2265;

	const PROPERTY_RESIDENTS_MAINTENANCE_LOCATIONS_PROBLEMS		= 2327;
	const CUSTOM_TEXT									= 1596;
	const ANNOUNCEMENTS									= 1597;

	const TEMPLATES										= 1587;
	const PROPERTY_PRICING_RENT							= 2183;
	const PROPERTY_PRICING_RENT_PROPERTY				= 2184;
	const PROPERTY_PRICING_RENT_UNIT_TYPE				= 2185;
	const PROPERTY_PRICING_RENT_UNIT_SPACE				= 2186;
	const PROPERTY_PRICING_RENT_HISTORICAL_RENT			= 2188;
	const PROPERTY_PRICING_RENT_BUDGETED_RENT			= 2255;
	const PROPERTY_PRICING_DEPOSITES					= 2189;
	const PROPERTY_PRICING_DEPOSITES_PROPERTY			= 2190;
	const PROPERTY_PRICING_DEPOSITES_UNIT_TYPE			= 2191;
	const PROPERTY_PRICING_DEPOSITES_UNIT_SPACE			= 2192;
	const PROPERTY_PRICING_OTHER_FEES					= 2193;
	const PROPERTY_PRICING_OTHER_FEES_PROPERTY			= 2194;
	const PROPERTY_PRICING_OTHER_FEES_UNIT_TYPE			= 2195;
	const PROPERTY_PRICING_OTHER_FEES_UNIT_SPACE		= 2196;
	const PROPERTY_PRICING_AMENITIES					= 2197;
	const PROPERTY_PRICING_AMENITIES_PROPERTY			= 2198;
	const PROPERTY_PRICING_AMENITIES_FLOOR_PLAN			= 2225;
	const PROPERTY_PRICING_AMENITIES_UNIT_SPACE			= 2200;
	const PROPERTY_PRICING_ADD_ONS						= 2201;
	const PROPERTY_PRICING_ADD_ONS_RENTABLE_ITEMS		= 2205;
	const PROPERTY_PRICING_ADD_ONS_ASSIGNABLE_ITEMS		= 2202;
	const PROPERTY_PRICING_ADD_ONS_SERVICES				= 2203;
	const PROPERTY_PRICING_PETS							= 2206;
	const PROPERTY_PRICING_SPECIALS						= 2207;
	const PROPERTY_PRICING_SPECIALS_PROPERTY			= 2208;
	const PROPERTY_PRICING_SPECIALS_FLOOR_PLAN			= 2735;
	const PROPERTY_PRICING_SPECIALS_UNIT_TYPE			= 2209;
	const PROPERTY_PRICING_SPECIALS_UNIT_SPACE			= 2210;
	const PROPERTY_PRICING_TIERED						= 2211;
	const PROPERTY_PRICING_TIERED_DISCOUNTS_PROPERTY	= 2212;
	const PROPERTY_PRICING_TIERED_DISCOUNTS_UNIT_TYPE	= 2213;
	const PROPERTY_PRICING_LATE_FEES					= 1606;
	const PROPERTY_PRICING_UTILITIES					= 2215;
	const PROPERTY_PRICING_UTILITIES_PROPERTY			= 2216;
	const PROPERTY_PRICING_UTILITIES_UNIT_TYPE			= 2217;
	const PROPERTY_PRICING_UTILITIES_UNIT_SPACE			= 2218;
	const PROPERTY_PRICING_TAXES						= 2219;
	const PROPERTY_PRICING_PREFERENCES_TIERED_PRICING	= 2223;
	const PRICING_RISK_PREMIUMS							= 3308;

	const PROPERTY_COMMUNICATION_CALL_MENU				= 2573;
	const PROPERTY_COMMUNICATION_GENERAL_BRANDING		= 1618;
	const PROPERTY_COMMUNICATION_RESIDENT_SHEDULING		= 1628;
	const COMMUNICATION_GENERAL_QUICK_RESPONSES			= 1619;
	const COMMUNICATION_GENERAL_SMS						= 1620;
	const COMMUNICATION_NOTIFICATION_RECEPIENTS			= 1640;
	const COMMUNICATION_LOBBY_DISPLAY					= 2143;
	const COMMUNICATION_SYSTEM_DOCUMENTS				= 2473;
	const COMMUNICATION_SYSTEM_DOCUMENTS_INVOICE_MESSAGES	= 2474;
	const COMMUNICATION_SYSTEM_DOCUMENTS_DOCUMENT_TEMPLATE	= 2774;
	const COMMUNICATION_SYSTEM_DOCUMENTS_MISC				= 1639;
	const COMMUNICATION_FROM_ADDRESSES						= 3011;
	const PROPERTY_COMMUNICATION_RESIDENT_SHEDULING_RENEWALS 			= 1629;
	const PROPERTY_COMMUNICATION_RESIDENT_SHEDULING_PAYMENT 			= 1631;
	const PROPERTY_COMMUNICATION_RESIDENT_SHEDULING_OTHER				= 1632;
	const PROPERTY_COMMUNICATION_RESIDENT_SHEDULING_LEASE_MODIFICATION	= 1632;

	const PROPERTY_LEASING_LEASE_ONLINE_LEASE_SIGNING		= 1553;

	const PROPERTY_LEASING_EVENT_RESULTS_CANCELLATION_REASONS 	= 2281;
	const PROPERTY_RESIDENTS_REPUTATION_REVIEW_SITES 			= 2665;

	const PROPERTY_COMMERCIAL_UNIT_SETUP					= 2745;
	const PROPERTY_COMMERCIAL_UNIT_SETUP_BUILDINGS_FLOORS	= 2746;
	const PROPERTY_COMMERCIAL_UNIT_SETUP_SUITES				= 2747;

	const DASHBOARD_APPROVALS_UTILITIES_PREBILL_APPROVAL		= 2765;
	const UTILITY_ACCOUNT_CAPS_AND_SUBSIDIES                    = 1009;
	const UTILITY_ACCOUNT_INVOICE_MESSAGES                      = 1010;
	const UTILITY_ACCOUNT_INVOICE                               = 1011;
	const UTILITY_CUSTOMERS_MOVE_OUT                            = 1012;
	const PROPERTY_UTILITY_INVOICE_RESIDENT_MESSAGE             = 1013;
	const PROPERTY_UTILITY_INVOICE_MESSAGE                      = 2474;
	const UTILITY_NOTIFICATIONS                                 = 3368;
	const DASHBOARD_APPLICANTS_RV_NOT_PROGRESSING               = 3877;

	const PROPERTY_DATA_MANAGEMENT								= 1644;
	const PROPERTY_DATA_MANAGEMENT_GENERAL						= 1645;
	const PROPERTY_DATA_MANAGEMENT_VENDORS						= 1646;
	const PROPERTY_DATA_MANAGEMENT_FLOOR_PLANS_UNITS			= 1647;
	const PROPERTY_DATA_MANAGEMENT_LEASING						= 1648;
	const PROPERTY_DATA_MANAGEMENT_LEASING_LEADS 				= 1649;
	const PROPERTY_DATA_MANAGEMENT_LEASING_APPLICATION 			= 1650;
	const PROPERTY_DATA_MANAGEMENT_LEASING_LEASE 				= 1651;
	const PROPERTY_DATA_MANAGEMENT_LEASING_UNIT_AVAILABILITY 	= 1652;
	const PROPERTY_DATA_MANAGEMENT_LEASING_SCREENING 			= 1653;
	const PROPERTY_DATA_MANAGEMENT_LEASING_REVENUE_MANAGEMENT 	= 1654;
	const PROPERTY_DATA_MANAGEMENT_RESIDENTS					= 1655;
	const PROPERTY_DATA_MANAGEMENT_RESIDENTS_GENERAL			= 1656;
	const PROPERTY_DATA_MANAGEMENT_RESIDENTS_PAYMENT			= 1657;
	const PROPERTY_DATA_MANAGEMENT_RESIDENTS_MAINTENANCE		= 1658;
	const PROPERTY_DATA_MANAGEMENT_FINANCIAL					= 1659;
	const PROPERTY_DATA_MANAGEMENT_MIGRATIONS					= 1903;
	const LEASING_CHECKLISTS									= 2826;
	const ALLOW_VIEWING_UNMASKED_SSN_ON_REPORTS					= 2975;
	const PROPERTY_COMPETITORS									= 2788;
	const REPORT_PACKET_MODULE									= 2048;
	const SCHEDULE_REPORT_MODULE								= 2052;
	const AP_PAYEES												= 150;
	const MANAGE_CUSTOM_REPORTS									= 3114;
	const PROPERTY_AFFORDABLE_HOME								= 3318;
	const PROPERTY_GENERAL_WEBSITES_ADS							= 3051;
	const PROPERTY_GENERAL_BRANDING								= 3340;

	// Job costing variables
	const JOB_COSTING_JOB_BUDGET								= 2899;
	const JOB_COSTING_CHANGE_ORDER								= 3032;
	const ADJUSTMENTS											= 2976;

	const MESSAGE_CENTER_USE_RESIDENT_LIST						= 3155;
	const MESSAGE_CENTER_USE_PROSPECT_LIST						= 3156;
	const MESSAGE_CENTER_USE_EMPLOYEE_LIST						= 3157;
	const MESSAGE_CENTER_USE_VENDOR_LIST						= 3158;
	const MESSAGE_CENTER_SHARE_EMAILS							= 2602;

	// All Residents Communication setting
	const RESIDENT_COMMUNICATION								= 3193;
	const SEND_EMAIL_FROM_RESIDENT_ACTIVITY_LOG_AND_DASHBOARD	= 3194;
	const SEND_SMS_FROM_RESIDENT_ACTIVITY_LOG_AND_DASHBOARD		= 3195;

	const FINANCIAL_PAYMENT_ACCOUNT_VERIFICATION 				= 3191;
	const COMPANY_ALERTS										= 160;

	// Residents >> Tenants >> Lease Terms >> Reimbursable Expenses
	const REIMBURSABLE_EXPENSES									= 3741;

	// Residents >> Tenants >> Lease Terms >> Percent Rent
	const PERCENT_RENT									        = 3739;

	// Setup>> Properties>> Property>> Marketing>> Reputation
	const PROPERTY_MARKETING_REPUTATION                 = 2063;
	const PROPERTY_MARKETING_REPUTATION_REVIEWS         = 2064;
	const PROPERTY_MARKETING_REPUTATION_ATTRIBUTES      = 2065;
	const PROPERTY_MARKETING_REPUTATION_REVIEW_SITES    = 2665;
	const PROPERTY_MARKETING_REPUTATION_SOCIAL_ACCOUNTS = 3058;

	// Setup>> Properties>> Property>> Floor Plans & Units>> Units
	const PROPERTY_UNIT_SYSTEM							= 'property_unit_systemxxx';

	// Tools >> Group >> Contracts >> Contract Details
	const GROUP_CONTRACTS								= 'group_contractsxxx';
	const ADD_GROUP_ACTIVITY                            = 'add_group_activity';

	// Setup > Properties > Financial >  Reimbursable Expenses
	const PROPERTY_FINANCIAL_REIMBURSABLE_EXP_CAM		= 3117;
	const PROPERTY_FINANCIAL_REIMBURSABLE_EXP_INSURANCE	= 3118;
	const PROPERTY_FINANCIAL_REIMBURSABLE_EXP_TAXES		= 3119;

	const PROPERTY_FINANCIAL_VENDORS					= 3235;
	const PROPERTY_FINANCIAL_VENDORS_CORPORATE_CONTACTS	= 3236;

	const CUSTOMER_SMART_DEVICES_ACTIVE_AND_SCHEDULED_CODES = 3364;
	const CUSTOMER_SMART_DEVICES_MANAGE_GUEST_CODES         = 3365;
	const CUSTOMER_SMART_DEVICES_UPDATE_ACCESS_CODES        = 3366;
	const CUSTOMER_SMART_DEVICES_MANAGE_ACCESS_CODES        = 3434;

	// Setup >> Properties >> Property >> Floor Plans & Units
	const FLOORPLAN_UNIT_SMART_DEVICES      = 3166;
	const UNIT_TYPE_MANAGE_SMART_DEVICE     = 3317;
	const PROPERTY_ENTRATAMATION_GENERAL    = 3115;
	const PROPERTY_ENTRATAMATION_RULE       = 3581;

	// Setup >> Properties >> Property >> Entratamation >> Site Tour
	const PROPERTY_ENTRATAMATION_SITE_TOUR = 3645;

	// Setup >> Properties >> Property >> Entratamation >> Smart Amenity
	const PROPERTY_ENTRATAMATION_SMART_AMENITY = 3897;

	// Setup >> Properties >> Property >> Entratamation >> Smart Common Area
	const PROPERTY_ENTRATAMATION_SMART_COMMON_AREA = 3966;

	// Violation
	const SETUP_COMPANY_RESIDENTS_VIOLATION = 3372;
	const RESIDENT_LEASE_VIOLATION          = 3373;
	const VIOLATION_CREATE_VIOLATION        = 3374;
	const DASHBOARD_RESIDENTS_VIOLATION     = 3375;

	const DASHBOARD_RESIDENTS_HAZARDS     = 3798;

	const SMART_DEVICES_BULK_MANAGE_DEVICES                 = 3162;
	const SMART_DEVICES_PROPERTY_UNIT_LIST                  = 3137;
	const SMART_DEVICES_GENERATE_ACCESS_CODES               = 3161;
	const SMART_DEVICES_OVERRIDE_AUTOMATION_RESET           = 3446;
	const SMART_DEVICES_EDIT_DEVICE_NAME_LOCATION           = 3448;

	const DASHBOARD_FINANCIAL_INCOMPLETE_INVOICES			= 3391;
	const DASHBOARD_APPROVALS_SCHEDULED_CHARGES			    = 3497;
	const DASHBOARD_APPROVALS_REASONABLE_ACCOMMODATIONS		= 3953;
	const DASHBOARD_RESIDENTS_REASONABLE_ACCOMMODATIONS		= 3954;

	// Privacy management
	const DASHBOARD_APPROVALS_PRIVACY_MANAGEMENT			= 3789;
	const DASHBOARD_APPROVALS_PRIVACY_INFO_MANAGEMENT		= 3790;
	const DASHBOARD_APPROVALS_PRIVACY_DELETION_MANAGEMENT	= 3791;

	const AP_PAYEE_COMPLIANCE_DOC_VERIFICATION			    = 2796;

	// My Profile ( Entrata )
	const MY_PROFILE_INFORMATION                             = 3556;

	const COMPANY_FINANCIAL_BANK_ACCOUNTS					= 170;

	const LEAD_ADD_SCHEDULED_CHARGES                        = 3631;
	const LEAD_EDIT_SCHEDULED_CHARGES                       = 3632;
	const LEAD_DELETE_SCHEDULED_CHARGES_CREDITS             = 3633;
	const LEAD_ALLOW_CUSTOM_CHARGE_DAYS                     = 3634;
	const LEAD_ADD_SCHEDULED_CREDITS                        = 3635;
	const LEAD_EDIT_SCHEDULED_CREDITS                       = 3636;

	const LEAD_REVERSE_CHARGES								= 3638;
	const LEAD_ADD_CHARGES									= 3639;
	const LEAD_EDIT_CHARGE_DATES							= 3640;
	const LEAD_EDIT_CHARGES									= 3641;
	const LEAD_EDIT_CREDIT_DATES							= 3642;
	const LEAD_ADD_CREDITS									= 3643;
	const LEAD_EDIT_CREDITS									= 3644;

	const RESIDENT_VERIFY_OVERRIDE_CRIMINAL_DECISION		= 3186;
	const SWITCH_TO_THIRD_PARTY_DEPOSIT                     = 3763;
	const DASHBOARD_REPORTS									= 3801;
	const EDIT_PROPERTY_ALLOCATION_PRIORITY                 = 3810;

	// Tools > Alt Text Editor
	const ALT_TEXT_EDITOR_WEBSITE_WIDGETS                   = 3804;
	const ALT_TEXT_EDITOR_WEBSITE_ADS                       = 3844;
	const ALT_TEXT_EDITOR_WEBSITE_TEMPLATE_SLOTS            = 3856;
	const ALT_TEXT_EDITOR_WEBSITE_JOURNEYS                  = 3912;

	const COMMERCIAL_SYSTEM                                 = 2751;
	const COMMERCIAL_PROPERTIES                             = 3834;
	const COMMERCIAL_TENANTS                                = 3835;

	const SCREENING_PACKAGES            = 3253;
	const SCREENING_CRITERIA_TEMPLATES  = 3255;
	const SCREENING_CONDITION_TEMPLATES = 3257;


	const DASHBOARD_SYSTEM								= 'dashboard_systemxxx';
	const AUTHENTICATION								= 'authenticationxxx';
	const AR_DEPOSITS 									= 'ar_depositsxxx';
	const VENDORS										= 'ap_payees_systemxxx';
	const GENERAL_JOURNAL								= 'general_journalxxx';
	const INVOICE										= 'invoicexxx';
	const CHECK_REGISTER								= 'check_registerxxx';
	// @Todo will remove it after Transaction Approval routing release.
	// const CHARGE_AR_TRANSACTION							= 'charge_ar_transactionsxxx';
	const CHARGE_AR_TRANSACTION							= 'charge_ar_transactionxxx';
	const CREDIT_AR_TRANSACTION							= 'credit_ar_transactionxxx';
	const AR_TRANSACTION_APPROVAL_REQUEST				= 'ar_transaction_approval_requestxxx';
	const CUSTOMER										= 'customerxxx';
	const CUSTOMER_DOCUMENT								= 'customer_documentsxxx';
	const APPLICATION_DOCUMENT							= 'application_documentsxxx';
	const CAN_CREATE_PAYMENT_WITHOUT_DEPOSIT			= 'create_payment_without_depositxxx';
	const SCHEDULED_CHARGE_APPROVAL_REQUEST				= 'scheduled_charge_approval_requestxxx';

	const CUSTOMERS_SYSTEM 								= 'customers_systemxxx';
	const CUSTOMERS_CHARGES 							= 'customers_chargesxxx';
	const ROLLBACK_RECURRING_CHARGES                    = 'rollback_recurring_chargesxxx';
	const ROLLBACK_LATE_FEES                            = 'rollback_late_feesxxx';
	const BULK_REVERSE_AR_TRANSACTIONS                  = 'bulk_reverse_ar_transactionsxxx';
	const BULK_UNIT_ASSIGNMENTS                         = 'bulk_unit_assignmentsxxx';
	const CORPORATE_SYSTEM 								= 'corporate_systemxxx';
	const CORPORATE_PAYMENTS 							= 'corporate_paymentsxxx';
	const CORPORATE_CHARGES 							= 'corporate_chargesxxx';
	const CORPORATE_DEPOSITS 							= 'corporate_depositsxxx';
	const CORPORATE_CUSTOMERS 							= 'corporate_customersxxx';
	const CORPORATE_JOBS								= 'corporate_jobsxxx';
	const JOB_BUDGET								    = 'job_budgetxxx';
	const CORPORATE_CONTRACTS							= 'corporate_contractsxxx';
	const AFFORDABLE									= 'affordablexxx';
	const RESIDENT_VERIFY								= 'resident_verifyxxx';
	const AP_CONTRACT									= 'ap_contractxxx';

	const RESIDENT_REPAYMENT_AGREEMENT					= 'repayment_agreementsxxx';
	const PAYMENTS										= 'update_ar_paymentxxx';
	const APPLICATION									= 'application_systemxxx';
	const APPLICATION_HISTORY							= 'application_historyxxx';
	const LEADS_DASHBOARD								= 'leads_dashboardxxx';
	const WORK_ORDER									= 'customer_work_order_newxxx';
	const PURCHASE_ORDER								= 'purchase_orderxxx';

	const REVERSE_FINANCIAL_MOVE_OUT					= 'reverse_financial_move_out';

	const ACCOUNTING_SYSTEM_AR_POST_MONTH				= 'accounting_system_ar_post_month';

	const REPORTS										= 'reportsxxx';
	const SAP_REPORTS									= 'sap_reportsxxx';
	const FAVORITE_REPORTS								= 'favorite_reportsxxx';
	const REPORT_INBOX									= 'report_inboxxxx';

	// Report packet module and actions.
	const REPORT_PACKETS								= 'report_packetsxxx';
	const CREATE_REPORT_PACKET 							= 'create_report_packetxxx';
	const EDIT_REPORT_PACKET 							= 'edit_report_packetxxx';
	const DELETE_REPORT_PACKET 							= 'delete_report_packetxxx';

	// Saved filters module and actions.
	const SAVED_FILTERS									= 'report_filtersxxx';
	const CREATE_REPORT_FILTER 							= 'create_report_filterxxx';

	// Report schedules module and actions.
	const SCHEDULED_REPORTS								= 'scheduled_reportsxxx';
	const CREATE_REPORT_SCHEDULE 						= 'create_report_schedulexxx';
	const EDIT_REPORT_SCHEDULE 							= 'edit_report_schedulexxx';
	const DELETE_REPORT_SCHEDULE 						= 'delete_report_schedulexxx';

	const REPORTS_NEW									= 'report_system_newxxx';
	const MY_REPORTS_NEW								= 'my_reports_newxxx';
	const REPORT_INBOX_NEW								= 'report_inbox_newxxx';
	const COMPANY_REPORTS_NEW							= 'company_reports_newxxx';
	const REPORT_HISTORY_NEW							= 'report_histories_newxxx';
	const REPORT_SCHEDULES_NEW							= 'report_schedules_newxxx';
	const CREATE_REPORT_NEW_SCHEDULES					= 'create_report_new_schedulexxx';
	const EDIT_REPORT_NEW_SCHEDULES						= 'edit_report_new_schedulexxx';
	const DELETE_REPORT_NEW_SCHEDULES					= 'delete_report_new_schedulexxx';
	const CREATE_EDIT_CUSTOM_REPORT						= 'create_edit_custom_reportxxx';
	const REPORT_PACKETS_NEW							= 'report_packets_newxxx';
	const REPORT_LIBRARY_NEW							= 'report_library_newxxx';
	const REPORT_GROUPS_NEW								= 'report_groups_newxxx';
	const REPORT_INSTANCES_NEW							= 'report_instances_newxxx';
	const REPORT_DOCUMENTATION_NEW						= 'report_documentation_newxxx';
	const SAP_REPORTS_NEW								= 'sap_reports_newxxx';
	const DASHBOARD_REPORTS_NEW							= 'dashboard_reports_newxxx';
	const OVERRIDE_LOCKED_FILTERS						= 'override_locked_filtersxxx';
	const MANAGE_REPORT_NEW_GROUP						= 'manage_report_new_groupxxx';
	const ADD_REPORT_NEW_INSTANCE						= 'add_report_new_instancexxx';
	const EDIT_REPORT_NEW_INSTANCE						= 'edit_report_new_instancexxx';
	const DELETE_REPORT_NEW_INSTANCE					= 'delete_report_new_instancexxx';
	const CREATE_DASHBOARD_NEW							= 'create_dashboard_newxxx';
	const EDIT_DASHBOARD_NEW							= 'edit_dashboard_newxxx';
	const EDIT_DASHBOARD_DETAILS_NEW					= 'edit_dashboard_details_newxxx';
	const DELETE_DASHBOARD_NEW							= 'delete_dashboard_newxxx';
	const CREATE_PUBLIC_REPORT_NEW_PACKET               = 'create_public_report_new_packetxxx';
	const CREATE_REPORT_NEW_PACKET                      = 'create_report_new_packetxxx';

	const ACCOUNTING_REPORT_TAB							= 'accounting_report_tabxxx';
	const RESIDENTS_REPORT_TAB							= 'residents_report_tabxxx';
	const LEADS_REPORT_TAB								= 'leads_report_tabxxx';
	const MARKETING_REPORT_TAB							= 'marketing_report_tabxxx';
	const MAINTENANCE_REPORT_TAB						= 'maintenance_report_tabxxx';
	const PRICING_REPORT_TAB							= 'pricing_report_tabxxx';
	const PAYMENTS_REPORT_TAB							= 'payments_report_tabxxx';
	const UTILITIES_REPORT_TAB							= 'utilities_report_tabxxx';
	const COMMUNICATIONS_REPORT_TAB						= 'communications_report_tabxxx';
	const SYSTEM_REPORT_TAB								= 'system_report_tabxxx';
	const TAX_CREDITS_REPORT_TAB						= 'tax_credits_report_tabxxx';
	const PROPERTIES_REPORT_TAB							= 'properties_report_tabxxx';
	const INTEGRATIONS_REPORT_TAB						= 'integrations_report_tabxxx';
	const WEBSITES_REPORT_TAB							= 'websites_report_tabxxx';
	const MIGRATIONS_REPORT_TAB							= 'migrations_report_tabxxx';
	const DOCUMENT_MANAGEMENT_REPORT_TAB				= 'document_management_report_tabxxx';
	const INVOICE_SYSTEM_POST_INVOICE					= 'invoice_system_post_invoice';
	const REPORT_QUEUE									= 'report_queuexxx';
	const REPORT_HISTORY								= 'report_history_newxxx';
    const ADD_REPORTS									= 'add_reportsxxx';
    const UPDATE_VERSIONS								= 'update_versionsxxx';
	const ADD_REPORT_GROUP								= 'add_report_groupxxx';
	const MANAGE_REPORT_OR_GROUP						= 'manage_report_or_groupxxx';
    const USER_AND_GROUPS								= 'users_and_groupsxxx';
	const MANAGE_REPORTS								= 'manage_reportsxxx';

	const PRICING_AVAILABLITY_TRENDS_REPORT				= 'pricing_availability_reports';
	const LEAD_DETAIL_REPORT							= 'lead_detail_reportxxx';
	const PRICING_COMPETITVE_TRENDS_REPORT				= 'pricing_competitive_trends';
	const DELINQUENCY_REPORT							= 'delinquency_reportxxx';
	const DAILY_LEASE_ACTIVITY_REPORT					= 'daily_lease_activity_reportxxx';
	const PROPERTY_METER_HEALTH_REPORT					= 'property_meter_health_reportxxx';
	const BATCH_PROCESSING_REPORT						= 'batch_processing_reportxxx';
	const GL_DETAILS_REPORT								= 'gl_details_reportxxx';
	const CALL_PERFORMANCE_BY_AGENT_REPORT				= 'call_performance_by_agents_reportxxx';
	const TRIAL_BALANCE_REPORT							= 'trial_balance_reportxxx';
	const GAMIFICATION_REPORT							= 'gamification_reportxxx';
	const GPR_REPORT									= 'gpr_consolidate_by_reportxxx';
	const RESIDENT_AGED_RECEIVABLES_REPORT				= 'resident_aged_receivables_reportxxx';
	const TRACKER_TAB_REPORT							= 'call_trackerxxx'; // tracker_tab is a report though its name is not correct in modules table
	const AVAILABILITY_REPORT							= 'availability_reportxxx';
	const SETTLEMENT_DISTRIBUTION_REPORT				= 'settlement_distributions_reportxxx';
	const LEADS_REPORT									= 'leads_by_day_of_week_report';
	const RECURRING_PAYMENTS_REPORT						= 'recurring_payments_reportxxx';
	const OCCUPANCY_REPORT								= 'vacancy_reportxxx';
	const APPLICATION_DETAIL_REPORT						= 'application_detail_reportxxx';
	const BILLING_ACCOUNTS_LEDGERS_REPORT				= 'billing_accounts_ledger_reportxxx';
	const REPORT_SYSTEM_MODULE_NAME						= 'report_systemxxx';
	const REPORT_SYSTEM_NEW_MODULE_NAME						= 'report_system_newxxx';
	const REPORT_VERIFICATION_MODULE_NAME				= 'report_verificationxxx';
	const RESIDENT_RECEIPTS_REPORT						= 'resident_receipts_reportxxx';
	const RESIDENT_CHARGES_REPORT						= 'resident_charges_reportxxx';
	const INSURANCE_POLICIES_BY_LEASING_AGENT_REPORT	= 'insurance_policies_by_leasing_agent_reportxxx';
	const PREPAYMENTS_AND_DEPOSIT_REFUNDS_REPORT		= 'prepayments_and_deposit_refunds_reportxxx';
	const RESIDENT_DEPOSIT_AUDIT_REPORT 				= 'resident_deposit_audit_reportxxx';
	const BUDGETS										= 'budgetsxxx';
	const GL_DETAILS_CHILD_TREE_REPORT					= 'gl_details_child_tree_reportxxx';
	const RESIDENT_PORTAL_TRAFFIC_REPORT				= 'resident_portal_traffic_reportxxx';
	const IMPLEMENTATION_PORTAL_SETUP					= 'implementation_portal_setupxxx';
	const IMPLEMENTATION_PORTAL_COMPANY_SETUP			= 'implementation_portal_company_setupxxx';
	const IMPLEMENTATION_PORTAL_PROPERTY_SETUP			= 'implementation_portal_property_setupxxx';
	const GL_DETAILS_CONSOLIDATED_REPORT				= 'gl_details_consolidated_reportxxx';
	const BUILDINGS_AND_UNITS_REPORT					= 'buildings_and_units_reportxxx';
	const UNIT_EXCLUSIONS_REPORT						= 'unit_exclusions_reportxxx';
	const LEAD_EVENTS_REPORT							= 'lead_events_reportxxx';
	const BOX_SCORE_REPORT								= 'box_score_reportxxx';
	const RENT_ROLL_REPORT								= 'cached_rent_roll_reportxxx';
	const LEASE_ACTIVITY_REPORT							= 'lease_activity_reportxxx';
	const TRAFFIC_SUMMARY_REPORT						= 'traffic_summary_reportxxx';
	const APPLICANT_DETAIL_REPORT						= 'applicant_detail_reportxxx';
	const RESIDENT_RETENTION_REPORT						= 'resident_retention_reportxxx';
	const LEASING_CENTER_CALLS_BY_RESULT_REPORT			= 'leasing_center_calls_by_result_reportxxx';
	const CALL_TRACKER_LOGS								= 'call_tracker_logsxxx';
	const LEASE_DATA_REPORT								= 'lease_data_reportxxx';
	const APPLICATION_DATA_REPORT						= 'application_data_reportxxx';
	const UNIT_SPACE_DATA_REPORT						= 'unit_space_data_reportxxx';
	const EXECUTIVE_SUMMARY_REPORT						= 'executive_summary_reportxxx';
	const OPERATIONS_REPORT								= 'operations_reportxxx';
	const TENANT_LEASE_REIMBURSABLE_EXPENSES			= 'tenant_lease_term_reimbursable_expensesxxx';

	const REVISE_FINANCIAL_MOVE_OUT						= 'revise_financial_move_out';
	const CALL_SYSTEM_HELATH_DASHBOARD					= 'call_system_health_dashboard';
	const PRE_LEASE_REPORT								= 'pre_lease_reportxxx';
	const GROUP_BOOKINGS_REPORT							= 'group_bookings_reportxxx';
	const CAN_OVERWRITE_DEFAULT_CHARGE_AMOUNT			= 'can_overwrite_default_charge_amount';
	const ADD_RESIDENT									= 'add_residentxxx';
	const CANCEL_LEASE									= 'cancel_leasexxx';
	const PROPERTY_SETTINGS_TEMPLATES					= 'property_settings_templatesxxx';
	const ADD_BULK_CHARGES_OR_CREDITS                   = 'add_bulk_charges_or_credits';
	const POST_RECURRING_CHARGES                        = 'post_recurring_charges';
	const VENDOR_INVOICE_DETAILS						= 'vendor_invoice_details_reportxxx';

	const NAA_CLIENT_USAGE_REPORT						= 'naa_client_usage_report';

	const PROPERTY_BILLING_REPORT						= 'property_billing_reportxxx';
	const TOTAL_COST_BY_UTILITY_REPORT					= 'total_cost_by_utility_reportxxx';
	const VCR_RECOVERY_REPORT							= 'vcr_recovery_reportxxx';
	const UTILITY_AUDIT_REPORT							= 'utility_audit_reportxxx';
	const RESIDENT_DATA_REPORT							= 'resident_data_reportxxx';
	const AP_AGING_REPORT								= 'ap_aging_reportxxx';

	const AP_PAYEE_SYSTEM								= 'ap_payee_systemxxx';
	const AP_PAYEE_LOCATIONS							= 'ap_payee_locationsxxx';

	const VIEW_INSPECTION								= 'inspection_newxxx';
	const CL_AD_NEW										= 'craigslistxxx';
	const APPLICATION_SCREENING_DECISION				= 'application_screening_decisionxxx';
	const COMPANY_USER									= 'company_userxxx';
	const COMPANY_GROUP									= 'company_groupxxx';
	const ASSETS_TRACKING								= 'assets_trackingxxx';
	const ISSUE_REFUND									= 'issue_refund';
	const CANCEL_REFUND									= 'cancel_refund';
	const ISSUE_REFUND_FOR_LEASE_WITH_BALANCE			= 'issue_refund_for_lease_with_balance';

    const ADD_DEPOSIT 									= 'add_deposit';
    const EDIT_DEPOSIT 								    = 'edit_deposit';
	const EDIT_AFFORDABLE_SCHEDULED_CHARGES             = 'edit_affordable_scheduled_charges';

	const PROPERTY_SETTING_VALIDATION					= 'property_settings_validationxxx';

	const REVIEWS_LISTING                               = 'reviews_listingxxx';
	const REVIEWS_DETAILS								= 'reviews_detailsxxx';
	const REPUTATION									= 'reputationxxx';
    const REVIEWS_TRAINING								= 'trainingxxx';

	// lease_actions MOVE IN
	const PEFROM_MOVE_IN								= 'perform_move_in';
	const ADJUST_MOVE_IN								= 'adjust_move_in';
	const BULK_MOVE_IN									= 'bulk_move_in';
	const REVERSE_MOVE_IN 								= 'reverse_move_in';

	// lease actions Place On Notice
	const PLACE_ON_NOTICE								= 'place_on_notice';
	const CANCEL_NOTICE									= 'cancel_notice';
	const CHANGE_MOVE_OUT_DATE							= 'change_move_out_date';

	// lease_actions MOVE OUT
	const PERFORM_MOVE_OUT 								= 'perform_move_out';
	const BULK_MOVE_OUT									= 'bulk_move_out';
	const REVERSE_MOVE_OUT 								= 'reverse_move_out';
	const BULK_UTILITY_MOVE_OUT							= 'lease_action_bulk_utility_move_outxxx';

	// lease action Financial Move Out
	const BULK_FINANCIAL_MOVE_OUT 						= 'bulk_financial_move_out';

	// Lease Tab
	const EDIT_LEASE_TERM								= 'edit_lease_term';
	const EDIT_CURRENT_LEASE_DATES						= 'edit_current_lease_dates';
	const EDIT_LEASE_MOVE_IN_DATE						= 'edit_lease_move_in_date';
	const BULK_EDIT_LEASE_DATES							= 'bulk_edit_lease_dates';
	const MANAGE_OTHER_LEASES							= 'manage_other_leases';

	// lease_actions Transfer
	const SCHEDULE_TRANSFER								= 'schedule_transfer';
	const CANCEL_TRANSFER								= 'cancel_transfer';
	const PROCESS_TRANSFER								= 'process_transfer';
	const INDIVIDUAL_TRANSFER							= 'individual_transferxxx';

	const ENTER_TERMINATION								= 'enter_termination';
	const CANCEL_TERMINATION							= 'cancel_termination';
	const CONVERT_TO_OTHER_INCOME						= 'convert_to_other_income';
	const ADD_OTHER_INCOME								= 'add_other_income';
	const ENABLE_LEASE									= 'enable_lease';
	const DISABLE_LEASE									= 'disable_lease';

	// Residents >> All Residents
	const ADD_ACTIVITY									= 'add_activity';
	const EDIT_QUICK_VIEW								= 'edit_quick_view';
	const EDIT_LEASE_SUMMARY							= 'edit_lease_summary';
	const EDIT_OCCUPANT_INFORMATION						= 'resident_edit_occupant_information';
    const EDIT_DEMOGRAPHIC_INFORMATION					= 'resident_edit_demographic_information';
	const EDIT_COMMUNICATION_SETTINGS					= 'resident_edit_communication_settings';
	const EDIT_RESIDENT_PORTAL_SETTINGS					= 'resident_edit_resident_portal_settings';
	const MANAGE_CHARGE_CREDIT_ATTACHMENTS				= 'manage_charge_credit_attachments';

	// Collections
	const SEND_LEASE_TO_COLLECTIONS						= 'send_lease_to_collections';
	const REMOVE_LEASE_FROM_COLLECTIONS					= 'remove_lease_from_collections';
	const GENERATE_PRE_COLLECTION_LETTER				= 'generate_pre_collection_letter';
	const EXCLUDE_FROM_COLLECTIONS						= 'exclude_from_collections';
	const RESUME_COLLECTIONS							= 'resume_collections';

	// Renewals
	const SEND_NON_RENEWAL_NOTICE						= 'send_non_renewal_notice';
	const CREATE_RENEWAL_OFFER							= 'create_renewal_offer';
	const RENEW_RESIDENT								= 'renew_resident';
	const PRODUCT_UPDATE    							= 'product_updatesxxx';
	const EXTENSION										= 'extensionxxx';

	// Setup > Company > Financial > Delinqency
	const DELINQUENCY_SETUP    							= 'delinquency_setupxxx';
	const DELINQUENCIES    								= 'delinquenciesxxx';

	const CUSTOMER_ACTIVITY 							= 'customer_activityxxx';
	const COLLECTIONS_STATUS_REPORT 					= 'collections_status_reportxxx';

	// Tools > Groups
	const GROUPS 										= 'groupsxxx';
	const BULK_ADD_PAYMENT								= 'group_bulk_paymentxxx';
	const GROUP_CONTRACT_UPLOAD_OCCUPANTS				= 'group_contract_upload_occupants';

	const SCHEDULED_EMAILS_REPORT						= 'scheduled_emails_reportxxx';
	const THIRD_PARTY_DEPOSIT							= 'third_party_depositxxx';
	const DELETE_MAC_MONTH								= 'delete_mac_month';

	// Residents >> Commercial
	const COMMERCIAL									= 'commercialxxx';

	// Leads >> Referrer
	const REFERRER_SYSTEM								= 'referrer_systemxxx';

	protected $m_intReportId;
	protected $m_intReportModuleId;
	protected $m_intReportInstanceId;
	protected $m_intReportGroupId;
	protected $m_intReportGroupModuleId;
	protected $m_intReportFilterId;

	protected $m_boolReportIsSchedulable;

	protected $m_strReportName;
	protected $m_strModuleName;

	protected $m_boolItemDescription;

	protected $m_arrobjModules;

	public static $c_arrstrSiteTabletReportModuleNames	= [ CModule::DAILY_LEASE_ACTIVITY_REPORT, CModule::PROPERTY_METER_HEALTH_REPORT, CModule::BATCH_PROCESSING_REPORT, CModule::GL_DETAILS_REPORT, CModule::CALL_PERFORMANCE_BY_AGENT_REPORT, CModule::DELINQUENCY_REPORT, CModule::TRIAL_BALANCE_REPORT, CModule::GAMIFICATION_REPORT, CModule::GPR_REPORT, CModule::TRACKER_TAB_REPORT ];
	public static $c_arrstrCorporateSystemModuleNames	= [ self::CORPORATE_SYSTEM, self::CORPORATE_PAYMENTS, self::CORPORATE_CHARGES, self::CORPORATE_DEPOSITS, self::CORPORATE_CUSTOMERS, self::CORPORATE_JOBS, self::CORPORATE_CONTRACTS ];

	public static $c_arrstrEntrataLockoutAllowedModules = [ 'accounting_systemxxx' => NULL, 'report_systemxxx' => NULL, 'apps_systemxxx' => NULL, 'help_system' => NULL ];

	public static $c_arrstrStrictPermissionedModules = [ 'dashboard_financial_check21xxx' ];

	public static $c_arrstrCommercialPropertyHiddenModules = [
		'property_details_general_policiesxxx',
		'property_details_general_website_adsxxx',
		'property_floorplansxxx',
		'unit_typesxxx',
		'property_unitsxxx',
		'property_buildingsxxx',
		'property_details_action_competitorsxxx',
		'property_details_entratamationxxx',
		'property_pricing_rent_propertyxxx',
		'property_pricing_rent_unit_typexxx',
		'property_pricing_rent_unit_spacexxx',
		'property_pricing_depositsxxx',
		'property_pricing_other_fees_unit_typexxx',
		'property_pricing_other_fees_unit_spacexxx',
		'property_pricing_petsxxx',
		'property_pricing_specialsxxx',
		'property_pricing_offer_templatesxxx',
		'property_pricing_utilitiesxxx',
		'property_pricing_risk_premiumsxxx',
		'property_details_pricingxxx',
		'property_pricing_preferences_tiered_pricingxxx',
		'property_marketingxxx',
		'property_leasing_leadsxxx',
		'property_leasing_application_systemxxx',
		'property_leasing_screeningxxx',
		'property_leasing_goals_systemxxx',
		'property_leasing_leasing_centerxxx',
		'property_leasing_site_tabletxxx',
		'property_leasing_occupant_typesxxx',
		'property_leasing_event_result_systemxxx',
		'property_leasing_asset_libraryxxx',
		'property_leasing_lease_termsxxx',
		'property_leasing_lease_expiration_limitsxxx',
		'property_leasing_lease_online_lease_signingxxx',
		'property_leasing_lease_leasing_capsxxx',
		'property_residents_movein_movein_schedulerxxx',
		'property_residents_renewalsxxx',
		'property_residents_transfersxxx',
		'property_residents_insurancexxx',
		'property_residents_blackout_daysxxx',
		'property_communication_contact_points_leadsxxx',
		'property_communication_contact_points_applicantsxxx',
		'property_communication_contact_points_renewals_and_lease_modificationsxxx',
		'property_residents_maintenance_make_readiesxxx',
		'property_pricing_amenities_unit_typexxx',
		'property_pricing_amenities_floor_planxxx'
	];

	/**
	* Get Functions
	*
	*/

	public function getModules() {
		return $this->m_arrobjModules;
	}

	public function getPublicModuleIds() {
		return $this->m_arrintPublicModuleIds;
	}

	public function getReportInstanceId() {
		return $this->m_intReportInstanceId;
	}

	public function getReportId() {
		return $this->m_intReportId;
	}

	public function getReportName() {
		return $this->m_strReportName;
	}

	public function getReportModuleId() {
		return $this->m_intReportModuleId;
	}

	public function getReportGroupId() {
		return $this->m_intReportGroupId;
	}

	public function getReportGroupModuleId() {
		return $this->m_intReportGroupModuleId;
	}

	public function getReportIsSchedulable() {
		return $this->m_boolReportIsSchedulable;
	}

	public function getItemDescription() {
		return $this->m_boolItemDescription;
	}

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['report_instance_id'] ) && $boolDirectSet ) {
			$this->m_intReportInstanceId = trim( $arrmixValues['report_instance_id'] );
		} elseif( isset( $arrmixValues['report_instance_id'] ) ) {
			$this->setReportInstanceId( $arrmixValues['report_instance_id'] );
		}

		if( true == isset( $arrmixValues['report_id'] ) )				$this->setReportId( $arrmixValues['report_id'] );
		if( true == isset( $arrmixValues['report_name'] ) )				$this->setReportName( $arrmixValues['report_name'] );
		if( true == isset( $arrmixValues['report_module_id'] ) )		$this->setReportModuleId( $arrmixValues['report_module_id'] );
		if( true == isset( $arrmixValues['report_group_id'] ) )			$this->setReportGroupId( $arrmixValues['report_group_id'] );
		if( true == isset( $arrmixValues['report_group_module_id'] ) )	$this->setReportGroupModuleId( $arrmixValues['report_group_module_id'] );
		if( true == isset( $arrmixValues['report_is_schedulable'] ) )	$this->setReportIsSchedulable( $arrmixValues['report_is_schedulable'] );
		if( true == isset( $arrmixValues['has_item_description'] ) )	$this->setItemDescription( $arrmixValues['has_item_description'] );
		if( true == isset( $arrmixValues['report_filter_id'] ) )		$this->setReportFilterId( $arrmixValues['report_filter_id'] );
		if( true == isset( $arrmixValues['module_name'] ) )				$this->setModuleName( $arrmixValues['module_name'] );
		return;
	}

	public function setReportInstanceId( $intReportInstanceId ) {
		$this->m_intReportInstanceId = $intReportInstanceId;
	}

	public function setReportId( $intReportId ) {
		$this->m_intReportId = $intReportId;
	}

	public function setReportName( $strReportName ) {
		$this->m_strReportName = $strReportName;
	}

	public function setReportModuleId( $intReportModuleId ) {
		$this->m_intReportModuleId = $intReportModuleId;
	}

	public function setReportGroupId( $intReportGroupId ) {
		$this->m_intReportGroupId = $intReportGroupId;
	}

	public function setReportGroupModuleId( $intReportGroupModuleId ) {
		$this->m_intReportGroupModuleId = $intReportGroupModuleId;
	}

	public function setReportIsSchedulable( $boolReportIsSchedulable ) {
		$this->m_boolReportIsSchedulable = $boolReportIsSchedulable;
	}

	public function setItemDescription( $boolHasDescription ) {
		$this->m_boolItemDescription = $boolHasDescription;
	}

	public function getReportFilterId() {
		return $this->m_intReportFilterId;
	}

	public function setReportFilterId( $intReportFilterId ) {
		$this->m_intReportFilterId = $intReportFilterId;
	}

	public function getModuleName() {
		return $this->m_strModuleName;
	}

	public function setModuleName( $strModuleName ) {
		$this->m_strModuleName = $strModuleName;
	}

   /**
	* Add Functions
	*
	*/

	public function addModule( $objModule ) {
		$this->m_arrobjModules[$objModule->getId()] = $objModule;
	}

	/**
	* Validate Functions
	*
	*/

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valParentModuleId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valName( $strAction, $objDatabase ) {
		$boolIsValid = true;
		$arrstrModules = [];

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Module name is required.' ) );
		} else {
			$intModuleId = ( VALIDATE_INSERT == $strAction ) ? NULL : $this->getId();

			$strActions  = \Psi\Eos\Entrata\CModules::createService()->fetchDefaultModuleByIdByNameByActions( $intModuleId, $this->getName(), $this->sqlActions(), $objDatabase );

			$strActions = str_replace( [ '{', '}' ], '', $strActions );
			if( true == valStr( $strActions ) ) {
				$strErrorMessage = ( '1' != $strActions ) ? ' Module with action(s) ( ' . $strActions . ' ) are already exists. ' : 'Module name already exists. ';
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strErrorMessage ) );
				$boolIsValid = false;
			}

			if( true == $this->getIsAction() || false == $this->getIsPublished() ) {
				return $boolIsValid;
			}

			$strApplicationPath = PATH_APPLICATION_ENTRATA;

			if( false == \Psi\CStringService::singleton()->stristr( $this->getName(), 'xxx' ) ) $strApplicationPath = PATH_APPLICATION_RESIDENT_WORKS;

			require( $strApplicationPath . 'Application/AssignApplicationModules.php' );

			if( true == array_key_exists( $this->getName(), $arrstrModules ) ) {
				return $boolIsValid;
			}

			require( PATH_LIBRARIES_PSI . 'Reports/AssignApplicationModules.php' );
			$strModuleName = str_replace( 'xxx', '', $this->getName() );

			if( false == array_key_exists( $strModuleName, $arrstrModules ) ) {

				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Module name not exists in AssignAplicationModules.php file.' ) );
				$boolIsValid = false;
			}

		}
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valUrl() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsAction() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsWebMethod() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublic() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowOnlyAdmin() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAllowOnlyPsiAdmin() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsHidden() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	/**
	 * Reports are inserted in modules. So custom validation.
	 * @param CDatabase $objDatabase
	 * @return boolean
	 */

	public function valCustomReportModuleName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Report name is required.' ) );
		} else {
			$objModule = \Psi\Eos\Entrata\CModules::createService()->fetchModuleByModuleNameByCid( $this->getName(), $this->getCid(), $objDatabase );

			if( true == valObj( $objModule, 'CModule' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Report already exists in this report group.' ) );
				$boolIsValid = false;
			}

		}
		return $boolIsValid;
	}

	public function valFullstorySettings() {

		$boolIsValid = true;

		if( false == isset( $this->getFullstorySettings()->enable_fullstory ) ) {
			$this->setFullstorySettings( NULL );
			return $boolIsValid;
		}

		$boolIsValid &= $this->validateFullstoryDates();
		$boolIsValid &= $this->validateEveryNthDates();
		$boolIsValid &= $this->validateEveryNthSession();

		return $boolIsValid;

	}

	public function validateFullstoryDates() {

		$boolIsValid = true;

		if( NULL == $this->getFullstorySettings()->fullstory_start_date ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Start date is required.' ) );
		}

		if( NULL == $this->getFullstorySettings()->fullstory_end_date ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'End date is required.' ) );
		}

		if( ( true == $boolIsValid ) && ( $this->getFullstorySettings()->fullstory_start_date > $this->getFullstorySettings()->fullstory_end_date ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Start date should not be greater than end date.' ) );
		}

		return $boolIsValid;
	}

	public function validateEveryNthDates() {

		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( NULL == $this->getFullstorySettings()->fullstory_every_nth_dates ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Every Nth Dates is required.' ) );
					break;
				}

				$arrintDateRange = range( 1, 31 );
				$arrintNthDates = explode( ',', $this->getFullstorySettings()->fullstory_every_nth_dates );

				if( ( '1-31' != $this->getFullstorySettings()->fullstory_every_nth_dates )
					&& ( ( false == ( min( $arrintNthDates ) >= min( $arrintDateRange ) && max( $arrintNthDates ) <= max( $arrintDateRange ) ) )
					|| ( false == ( \Psi\Libraries\UtilFunctions\count( $arrintNthDates ) == \Psi\Libraries\UtilFunctions\count( array_filter( $arrintNthDates, 'is_numeric' ) ) ) ) ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please provide proper Nth dates.' ) );
					break;
				}
		}

		return $boolIsValid;
	}

	public function validateEveryNthSession() {

		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( NULL == $this->getFullstorySettings()->fullstory_every_nth_session ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Every Nth Session is required.' ) );
					break;
				}

				if( false == ( ( 1 <= $this->getFullstorySettings()->fullstory_every_nth_session ) && ( valId( $this->getFullstorySettings()->fullstory_every_nth_session ) ) ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please provide proper Nth session.' ) );
					break;
				}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $strAction, $objDatabase );
				break;

			case 'VALIDATE_CUSTOM_REPORT_MODULE_NAME_INSERT':
			case 'VALIDATE_CUSTOM_REPORT_MODULE_NAME_UPDATE':
				$boolIsValid &= $this->valCustomReportModuleName( $objDatabase );
				break;

			case 'VALIDATE_FULLSTORY_SETTINGS':
				$boolIsValid &= $this->valFullstorySettings();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	public function determineNavigationDivId() {

		$strDivName = '';

		switch( $this->getName() ) {
			case 'dashboard_systemxxx':
				$strDivName = 'dashboard';
				break;

			case 'property_system':
				$strDivName = 'properties';
				break;

			case 'customers_systemxxx':
				$strDivName = 'residents';
				break;

			case 'website_system':
				$strDivName = 'websites';
				break;

			case 'payment_system':
				$strDivName = 'payments';
				break;

			case 'prospect_systemxxx':
				$strDivName = 'leads';
				break;

			case 'report_systemxxx':
				$strDivName = 'reports';
				break;

			case 'accounting_systemxxx':
				$strDivName = 'accounting';
				break;

			case 'apps_systemxxx':
				$strDivName = 'apps';
				break;

			case 'global_settings_system':
				$strDivName = 'settings';
				break;

			case 'more_system':
				$strDivName = 'more';
				break;

			default:
				$strDivName = '';
		}

		return $strDivName;
	}

	public static function moduleIdToName( $intModuleId ) {

		switch( $intModuleId ) {
			case self::LEASES:
				return 'leases';
				break;

			case self::APPROVE_LEASES_ACTION:
				return 'approve_leases_action';
				break;

			case self::VIEW_APPROVED_APPLICATIONS:
				return 'view_approved_applications';
				break;

			case self::VIEW_COUNTER_SIGN_REQUIRED_FILES:
				return 'view_counter_sign_required_files';
				break;

			case self::VIEW_LEASE_COMPLETED_APPLICATIONS:
				return 'view_lease_completed_applications';
				break;

			case self::VIEW_UNINSURED_LEASE_COMPLETED_APPLICATIONS:
				return 'view_uninsured_approved_applications';
				break;

			case self::EDIT_CHARGES_ACTION:
				return 'edit_charges_action';
				break;

			case self::APPROVE_APPLICATIONS_ACTION:
				return 'approve_deny_applications_action';
				break;

			case self::RESIDENT_VERIFY_OVERRIDE_PERMISSIONS:
				return 'resident_verify_override_permissions';
				break;

			case self::RESIDENT_VERIFY_RESCREEN_PERMISSION:
				return 'resident_verify_re_screen';
				break;

			case self::RESIDENT_VERIFY_VIEW_REPORT_PERMISSION:
				return 'resident_verify_view_report_permission';
				break;

			case self::APPROVE_DENY_OFFER_ACTION:
				return 'approve_deny_offer_action';
				break;

			case self::ABILITY_TO_BACKDATE_RENEWAL_OFFER:
				return 'approve_deny_backdate_renewal_offer';
				break;

			case self::RENEWALS_RENEW_RESIDENTS:
				return 'renewals_renew_residents';
				break;

			case self::RENEWALS_CREATE_OFFERS:
				return 'renewals_create_offers';
				break;

			case self::APPROVE_PENDING_RENEWALS:
				return 'approval_pending_renewals';
				break;

			case self::DENIED_RENEWALS:
				return 'denied_renewals';
				break;

			case self::ADD_EDIT_TICKETS_AND_TASKS_ACTION:
				return 'ticket_and_taskxxx';
				break;

			case self::RELEASE_NOTES_FOR_ALL_PRODUCTS_ACTION:
				return 'release_notes_for_all_products_action';
				break;

			case self::UPCOMING_FEATURES_FOR_ALL_PRODUCTS_ACTION:
				return 'upcoming_features_for_all_products_action';
				break;

			case self::VIEW_PRINT_EMAIL_QUOTES:
				return 'view_print_email_quotes';
				break;

			case self::APPROVE_APPLICATION_WITHOUT_SCREENING:
				return 'approve_application_without_screening';
				break;

			case self::COMBINE_LEADS:
				return 'combine_leads';
				break;

			case self::CANCEL_LEADS:
				return 'cancel_lead';
				break;

			case self::ARCHIVE_LEADS:
				return 'archive_lead';
				break;

			case self::MERCHANT_ACCOUNT:
				return 'merchant_accountsxxx';
				break;

			case self::FINANCIAL:
				return 'financial_setupxxx';
				break;

			case self::COMPANY_MAINTENANCE:
				return 'company_maintenancexxx';
				break;

			case self::INSPECTION_MANAGER:
				return 'inspection_managerxxx';
				break;

			case self::RENEWALS:
				return 'renewalsxxx';
				break;

			default:
				// default case
				break;
		}
		return NULL;
	}

	public function createModule( $intCurrentUserId, $intIsDisabled, $strValidationCallbackFunction, $intMigratePermissionsFromModule, $intIsInternal, $intIsPublishable, $intIsSchedulable, $intIsAutoAdded, $intDefaultReportId, $objClientDatabase ) {
		$objDataset = $objClientDatabase->createDataset();

			$strSql = 'SELECT
							*
						FROM
							func_modules_add( ' . ( int ) $this->m_intId . ','
												 . ( true == is_null( $this->m_intParentModuleId ) ? 'NULL' : ( int ) $this->m_intParentModuleId ) . ','
												 . $this->sqlName() . ','
												 . ( true == is_null( $this->sqlActions() ) ? 'NULL' : $this->sqlActions() ) . ','
												 . $this->sqlTitle() . ','
												 . $this->sqlDescription() . ','
												 . $this->sqlUrl() . ','
												 . $this->sqlHasWriteOperation() . ','
												 . ( int ) $this->m_intIsAction . ','
												 . ( int ) $this->m_intIsPublished . ','
												 . ( int ) $this->m_intIsPublic . ','
												 . ( int ) $this->m_intIsWebMethod . ','
												 . ( int ) $this->m_intIsHidden . ','
												 . ( int ) $this->m_intAllowOnlyAdmin . ','
												 . ( int ) $this->m_intAllowOnlyPsiAdmin . ','
												 . $this->sqlOptions() . ','
												 . ( int ) $this->m_intOrderNum . ','
												 . ( int ) $intCurrentUserId . ','
												 . ( int ) $intDefaultReportId . ','
												 . ( int ) $intIsDisabled . ','
												 . '\'' . $strValidationCallbackFunction . '\','
												 . ( int ) $intMigratePermissionsFromModule . ','
												 . ( int ) $intIsInternal . ','
												 . ( int ) $intIsPublishable . ','
												 . ( int ) $intIsSchedulable . ','
												 . ( int ) $intIsAutoAdded .
			                                ') ';

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to add modules. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}
		$objDataset->cleanup();

		return true;
	}

	public function createModuleTemp( $intCurrentUserId, $intIsDisabled, $strValidationCallbackFunction, $intMigratePermissionsFromModule, $intIsInternal, $intIsPublishable, $intIsSchedulable, $intIsAutoAdded, $intDefaultReportId, $intDefaultReportGroupId, $objAdminDatabase ) {
		$objDataset = $objAdminDatabase->createDataset();

		$strSql = 'SELECT
						*
					FROM
						func_modules_add_temp( ' . ( int ) $this->m_intId . ','
											. ( true == is_null( $this->m_intParentModuleId ) ? 'NULL' : ( int ) $this->m_intParentModuleId ) . ','
											. $this->sqlName() . ','
											. $this->sqlTitle() . ','
											. $this->sqlDescription() . ','
											. $this->sqlUrl() . ','
											. ( int ) $this->m_intIsAction . ','
											. ( int ) $this->m_intIsPublished . ','
											. ( int ) $this->m_intIsPublic . ','
											. ( int ) $this->m_intIsWebMethod . ','
											. ( int ) $this->m_intIsHidden . ','
											. ( int ) $this->m_intAllowOnlyAdmin . ','
											. ( int ) $this->m_intAllowOnlyPsiAdmin . ','
											. $this->sqlOptions() . ','
											. ( int ) $this->m_intOrderNum . ','
											. ( int ) $intCurrentUserId . ','
											. ( int ) $intDefaultReportId . ','
											. ( int ) $intIsDisabled . ','
											. '\'' . $strValidationCallbackFunction . '\','
											. ( int ) $intMigratePermissionsFromModule . ','
											. ( int ) $intIsInternal . ','
											. ( int ) $intIsPublishable . ','
											. ( int ) $intIsSchedulable . ','
											. ( int ) $intIsAutoAdded . ','
											. ( int ) $intDefaultReportGroupId . ')';

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to add modules. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objAdminDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function editModule( $intCurrentUserId, $intIsDisabled, $strValidationCallbackFunction, $intMigratePermissionsFromModule, $intIsInternal, $intIsPublishable, $intIsSchedulable, $intIsAutoAdded, $objClientDatabase, $intDefaultReportId = NULL, $boolReturnSqlOnly = false ) {

		$intCount = \Psi\Eos\Entrata\CModules::createService()->fetchModuleCount( 'WHERE id=' . $this->getId(), $objClientDatabase );

		if( 0 == $intCount ) {
			return $this->createModule( $intCurrentUserId, $intIsDisabled, $strValidationCallbackFunction, $intMigratePermissionsFromModule, $intIsInternal, $intIsPublishable, $intIsSchedulable, $intIsAutoAdded, $intDefaultReportId, $objClientDatabase );
		}

		$objDataset = $objClientDatabase->createDataset();

			$strSql = 'SELECT
						*
					FROM
						func_modules_edit( ' . ( int ) $this->m_intId . ','
											 . ( true == is_null( $this->m_intParentModuleId ) ? 'NULL' : ( int ) $this->m_intParentModuleId ) . ','
											 . $this->sqlName() . ','
											 . ( true == is_null( $this->sqlActions() ) ? 'NULL' : $this->sqlActions() ) . ','
											 . $this->sqlTitle() . ','
											 . $this->sqlDescription() . ','
											 . $this->sqlUrl() . ','
											 . $this->sqlHasWriteOperation() . ','
											 . ( int ) $this->m_intIsAction . ','
											 . ( int ) $this->m_intIsPublished . ','
											 . ( int ) $this->m_intIsPublic . ','
											 . ( int ) $this->m_intIsWebMethod . ','
											 . ( int ) $this->m_intIsHidden . ','
											 . ( int ) $this->m_intAllowOnlyAdmin . ','
											 . ( int ) $this->m_intAllowOnlyPsiAdmin . ','
											 . $this->sqlOptions() . ','
											 . ( int ) $this->m_intOrderNum . ','
											 . ( int ) $intCurrentUserId . ','
											 . ( int ) $intIsDisabled . ','
											 . '\'' . $strValidationCallbackFunction . '\','
											 . ( int ) $intMigratePermissionsFromModule . ','
											 . ( int ) $intIsInternal . ','
											 . ( int ) $intIsPublishable . ','
											 . ( int ) $intIsSchedulable . ','
											 . ( int ) $intIsAutoAdded .
			                            ') ';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert to edit modules. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objClientDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}
		$objDataset->cleanup();

		return true;
	}

}
?>
