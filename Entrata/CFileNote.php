<?php

class CFileNote extends CBaseFileNote {

	protected $m_strCreatedByName;

    /**
    * Set Functions
    *
    */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

    	if( true == isset( $arrmixValues['created_by_name'] ) )	$this->setCreatedByName( $arrmixValues['created_by_name'] );

    	return;
    }

    public function setCreatedByName( $strCreatedByName ) {
    	$this->m_strCreatedByName = $strCreatedByName;
    }

    /**
    * Get Functions
    *
    */

    public function getCreatedByName() {
    	return $this->m_strCreatedByName;
    }

    /**
    * Other Functions
    *
    */

    public function createFileNote() {
    	$objNote = new CFileNote();
    	return $objNote;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNote() {
        $boolIsValid = true;

        if( false == valStr( $this->getNote() ) ) {
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        		$boolIsValid = $this->valNote();
        		break;

        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>