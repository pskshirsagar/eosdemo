<?php

class CEventResultType extends CBaseEventResultType {

	const FOLLOWUP_TOUR_APPOINTMENT		= 1;
	const VOICEMAIL						= 2;
	const INFO_REQUESTED				= 3;
	const LEAD_OUTREACH					= 4;
	const RENEWAL_OUTREACH				= 5;
	const COLLECTIONS_RESPONSE			= 6;
	const UNKNOWN						= 7;
	const OTHER						    = 8;
	const SPECIAL						= 9;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>