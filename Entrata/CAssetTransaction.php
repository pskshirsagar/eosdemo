<?php

class CAssetTransaction extends CBaseAssetTransaction {

	const RETIRED           = 'Retired ';
	const RETURNED          = 'Returned';
	const UNRETIRED         = 'Unretired ';
	const PURCHASED         = 'Purchased';
	const WORK_ORDER        = 'Work Order';
	const TRANSFERRED       = 'Transferred';
	const PLACED_IN_SERVICE = 'Placed in Service';
	const ASSET_EDITED      = 'Asset Edited';

	protected $m_boolIsSpecificUnit;
	protected $m_boolIsCurrentPostMonth;
	protected $m_boolIsValidatePropertyDepreciationSetting = true;

	protected $m_intEventId;
	protected $m_intOrderNum;
	protected $m_intApHeaderId;
	protected $m_intAssetTypeId;
	protected $m_intMaintenanceRequestId;
	protected $m_intApCodesUnitOfMeasureId;
	protected $m_intUnitOfMeasureTypeId;

	protected $m_fltReceivedQuantity;
	protected $m_fltReturnedQuantity;
	protected $m_fltRemainingQuantity;
	protected $m_fltConsumptionUnitCost;
	protected $m_fltRemainingPurchaseQuantity;
	protected $m_fltFixedAssetAccumulatedDepreciationAmount;
	protected $m_fltFixedAssetNetBookValue;

	protected $m_strAction;
	protected $m_strItemName;
	protected $m_strUnitNumber;
	protected $m_strApCodeName;
	protected $m_strActionDate;
	protected $m_strPropertyName;
	protected $m_strReturnedDate;
	protected $m_strReceivedDate;
	protected $m_strHeaderNumber;
	protected $m_strActionUserName;
	protected $m_strTransactionType;
	protected $m_strUnitOfMeasureName;
	protected $m_strCompanyEmployeeFullName;
	protected $m_strMaintenanceLocationName;
	protected $m_strAssetMaintenanceLocationName;
	protected $m_strAssetLocationName;

	protected $m_objAssetAllocation;

	/**
	 * Create Functions
	 */

	public function createAssetAllocation() {

		$objAssetAllocation = new CAssetAllocation();
		$objAssetAllocation->setCid( $this->getCid() );
		$objAssetAllocation->setPropertyId( $this->getPropertyId() );
		$objAssetAllocation->setPostDate( $this->getPostDate() );
		$objAssetAllocation->setReportingPostDate( $this->getPostDate() );
		$objAssetAllocation->setPostMonth( $this->getPostMonth() );
		$objAssetAllocation->setGlTransactionTypeId( CGlTransactionType::ASSET_CONSUMPTION );

		return $objAssetAllocation;
	}

	public function createConsumeInventoryAssetAllocation() {

		$objConsumeAssetTransaction = clone $this;

		$objConsumeAssetTransaction->setId( NULL );
		$objConsumeAssetTransaction->setUnitSpaceId( NULL );
		$objConsumeAssetTransaction->setMaintenanceLocationId( NULL );
		$objConsumeAssetTransaction->setUpdatedBy( NULL );
		$objConsumeAssetTransaction->setUpdatedOn( NULL );
		$objConsumeAssetTransaction->setCreatedBy( NULL );
		$objConsumeAssetTransaction->setCreatedOn( NULL );
		$objConsumeAssetTransaction->setUnitOfMeasureId( $this->getApCodesUnitOfMeasureId() );
		$objConsumeAssetTransaction->setAssetTransactionTypeId( CAssetTransactionType::CONSUME_INVENTORY );
		$objConsumeAssetTransaction->setMemo( __( 'Material used on work order' ) );
		$objConsumeAssetTransaction->setUnitCost( $objConsumeAssetTransaction->getConsumptionUnitCost() );
		$objConsumeAssetTransaction->setTransactionAmount( $objConsumeAssetTransaction->getConsumptionUnitCost() * $objConsumeAssetTransaction->getQuantity() );
		$objConsumeAssetTransaction->setCachedRemaining( 0 );

		return $objConsumeAssetTransaction;
	}

	/**
	 * GET Functions
	 *
	 */

	public function getRemainingQuantity() {
		return $this->m_fltRemainingQuantity;
	}

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	public function getItemName() {
		return $this->m_strItemName;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getReceivedQuantity() {
		return $this->m_fltReceivedQuantity;
	}

	public function getReturnedQuantity() {
		return $this->m_fltReturnedQuantity;
	}

	public function getReturnedDate() {
		return $this->m_strReturnedDate;
	}

	public function getReceivedDate() {
		return $this->m_strReceivedDate;
	}

	public function getApCodeName() {
		return $this->m_strApCodeName;
	}

	public function getAssetTypeId() {
		return $this->m_intAssetTypeId;
	}

	public function getUnitOfMeasureName() {
		return $this->m_strUnitOfMeasureName;
	}

	public function getTransactionType() {
		return $this->m_strTransactionType;
	}

	public function getApHeaderId() {
		return $this->m_intApHeaderId;
	}

	public function getHeaderNumber() {
		return $this->m_strHeaderNumber;
	}

	public function getIsSpecificUnit() {
		return $this->m_boolIsSpecificUnit;
	}

	public function getAssetMaintenanceLocationName() {
		return $this->m_strAssetMaintenanceLocationName;
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function getActionUserName() {
		return $this->m_strActionUserName;
	}

	public function getActionDate() {
		return $this->m_strActionDate;
	}

	public function getMaintenanceLocationName() {
		return $this->m_strMaintenanceLocationName;
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function getAssetAllocation() {
		return $this->m_objAssetAllocation;
	}

	public function getApCodesUnitOfMeasureId() {
		return $this->m_intApCodesUnitOfMeasureId;
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function getConsumptionUnitCost() {
		return $this->m_fltConsumptionUnitCost;
	}

	public function getRemainingPurchaseQuantity() {
		return $this->m_fltRemainingPurchaseQuantity;
	}

	public function getAssetLocationName() {
		return $this->m_strAssetLocationName;
	}

	public function getUnitOFMeasureTypeId() {
		return $this->m_intUnitOfMeasureTypeId;
	}

	public function getFixedAssetAccumulatedDepreciationAmount() {
		return $this->m_fltFixedAssetAccumulatedDepreciationAmount;
	}

	public function getFixedAssetNetBookValue() {
		return $this->m_fltFixedAssetNetBookValue;
	}

	public function getIsCurrentPostMonth() {
		return $this->m_boolIsCurrentPostMonth;
	}

	public function getIsValidatePropertyDepreciationSetting() {
		return $this->m_boolIsValidatePropertyDepreciationSetting;
	}

	/**
	 * SET Functions
	 *
	 */

	public function setRemainingQuantity( $fltRemainingQuantity ) {
		$this->m_fltRemainingQuantity = CStrings::strToFloatDef( $fltRemainingQuantity, NULL, false, 3 );
	}

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = CStrings::strTrimDef( $strCompanyEmployeeFullName, 100, NULL, true );
	}

	public function setItemName( $strItemName ) {
		$this->m_strItemName = CStrings::strTrimDef( $strItemName, 50, NULL, true );
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = CStrings::strTrimDef( $strUnitNumber, 50, NULL, true );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setReceivedQuantity( $fltReceivedQuantity ) {
		$this->m_fltReceivedQuantity = $fltReceivedQuantity;
	}

	public function setReturnedQuantity( $fltReturnedQuantity ) {
		$this->m_fltReturnedQuantity = $fltReturnedQuantity;
	}

	public function setReturnedDate( $strReturnedDate ) {
		$this->m_strReturnedDate = $strReturnedDate;
	}

	public function setReceivedDate( $strReceivedDate ) {
		$this->m_strReceivedDate = $strReceivedDate;
	}

	public function setApCodeName( $strApCode ) {
		$this->m_strApCodeName = CStrings::strTrimDef( $strApCode, 100, NULL, true );
	}

	public function setAssetTypeId( $intAssetTypeId ) {
		$this->m_intAssetTypeId = CStrings::strToIntDef( $intAssetTypeId, NULL, false );
	}

	public function setUnitOfMeasureName( $strUnitOfMeasureName ) {
		$this->m_strUnitOfMeasureName = CStrings::strTrimDef( $strUnitOfMeasureName, 100, NULL, true );
	}

	public function setIsSpecificUnit( $boolIsSpecificUnit ) {
		$this->m_boolIsSpecificUnit = $boolIsSpecificUnit;
	}

	public function setAssetMaintenanceLocationName( $strAssetMaintenanceLocationName ) {
		$this->m_strAssetMaintenanceLocationName = $strAssetMaintenanceLocationName;
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->m_intMaintenanceRequestId = $intMaintenanceRequestId;
	}

	public function setPostMonth( $strPostMonth ) {

		if( true == valStr( $strPostMonth ) ) {

			$arrstrPostMonth = explode( '/', $strPostMonth );

			if( true == valArr( $arrstrPostMonth ) ) {

				if( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
					$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[1];
				} elseif( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrPostMonth ) ) {
					$strPostMonth = $arrstrPostMonth[0] . '/01/' . $arrstrPostMonth[2];
				}
			}
		}

		$this->m_strPostMonth = $strPostMonth;
	}

	public function setTransactionType( $strTransactionType ) {
		$this->m_strTransactionType = CStrings::strTrimDef( $strTransactionType, 100, NULL, true );
	}

	public function setApHeaderId( $intApHeaderId ) {
		$this->m_intApHeaderId = CStrings::strToIntDef( $intApHeaderId, NULL, false );
	}

	public function setHeaderNumber( $strHeaderNumber ) {
		$this->m_strHeaderNumber = $strHeaderNumber;
	}

	public function setAction( $strAction ) {
		$this->m_strAction = $strAction;
	}

	public function setActionUserName( $strActionUserName ) {
		$this->m_strActionUserName = $strActionUserName;
	}

	public function setActionDate( $strActionDate ) {
		$this->m_strActionDate = $strActionDate;
	}

	public function setMaintenanceLocationName( $strMaintenanceLocationName ) {
		$this->m_strMaintenanceLocationName = $strMaintenanceLocationName;
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = $intOrderNum;
	}

	public function setAssetAllocation( $objAssetAllocation ) {
		$this->m_objAssetAllocation = $objAssetAllocation;
	}

	public function setApCodesUnitOfMeasureId( $intApCodesUnitOfMeasureId ) {
		$this->m_intApCodesUnitOfMeasureId = $intApCodesUnitOfMeasureId;
	}

	public function setEventId( $intEventId ) {
		$this->m_intEventId = ( int ) $intEventId;
	}

	public function setConsumptionUnitCost( $fltConsumptionUnitCost ) {
		$this->m_fltConsumptionUnitCost = CStrings::strToFloatDef( $fltConsumptionUnitCost, NULL, false, 2 );
	}

	public function setRemainingPurchaseQuantity( $fltRemainingPurchaseQuantity ) {
		$this->m_fltRemainingPurchaseQuantity = CStrings::strToFloatDef( $fltRemainingPurchaseQuantity, NULL, false, 2 );
	}

	public function setAssetLocationName( $strAssetLocationName ) {
		$this->m_strAssetLocationName = $strAssetLocationName;
	}

	public function setUnitOfMeasureTypeId( $intUnitOfMeasureTypeId ) {
		$this->m_intUnitOfMeasureTypeId = CStrings::strToIntDef( $intUnitOfMeasureTypeId, NULL, false );
	}

	public function setFixedAssetAccumulatedDepreciationAmount( $fltFixedAssetAccumulatedDepreciationAmount ) {
		$this->set( 'm_fltFixedAssetAccumulatedDepreciationAmount', CStrings::strToFloatDef( $fltFixedAssetAccumulatedDepreciationAmount, NULL, false, 2 ) );
	}

	public function setFixedAssetNetBookValue( $fltFixedAssetNetBookValue ) {
		$this->set( 'm_fltFixedAssetNetBookValue', CStrings::strToFloatDef( $fltFixedAssetNetBookValue, NULL, false, 2 ) );
	}

	public function setIsCurrentPostMonth( $boolIsCurrentPostMonth ) {
		$this->m_boolIsCurrentPostMonth = \Psi\Libraries\UtilStrings\CStrings::createService()->strToBool( $boolIsCurrentPostMonth );
	}

	public function setIsValidatePropertyDepreciationSetting( $boolIsValidatePropertyDepreciationSetting ) {
		$this->m_boolIsValidatePropertyDepreciationSetting  = CStrings::strToBool( $boolIsValidatePropertyDepreciationSetting );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['remaining_quantity'] ) ) {
			$this->setRemainingQuantity( $arrmixValues['remaining_quantity'] );
		}
		if( true == isset( $arrmixValues['company_employee_full_name'] ) ) {
			$this->setCompanyEmployeeFullName( $arrmixValues['company_employee_full_name'] );
		}
		if( true == isset( $arrmixValues['item_name'] ) ) {
			$this->setItemName( $arrmixValues['item_name'] );
		}
		if( true == isset( $arrmixValues['unit_number'] ) ) {
			$this->setUnitNumber( $arrmixValues['unit_number'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['asset_type_id'] ) ) {
			$this->setAssetTypeId( $arrmixValues['asset_type_id'] );
		}
		if( true == isset( $arrmixValues['ap_code_name'] ) ) {
			$this->setApCodeName( $arrmixValues['ap_code_name'] );
		}
		if( true == isset( $arrmixValues['unit_of_measure_name'] ) ) {
			$this->setUnitOfMeasureName( $arrmixValues['unit_of_measure_name'] );
		}
		if( true == isset( $arrmixValues['transaction_type'] ) ) {
			$this->setTransactionType( $arrmixValues['transaction_type'] );
		}
		if( true == isset( $arrmixValues['header_number'] ) ) {
			$this->setHeaderNumber( $arrmixValues['header_number'] );
		}
		if( true == isset( $arrmixValues['ap_header_id'] ) ) {
			$this->setApHeaderId( $arrmixValues['ap_header_id'] );
		}
		if( true == isset( $arrmixValues['is_specific_unit'] ) ) {
			$this->setIsSpecificUnit( $arrmixValues['is_specific_unit'] );
		}
		if( true == isset( $arrmixValues['maintenance_location_name'] ) ) {
			$this->setAssetMaintenanceLocationName( $arrmixValues['maintenance_location_name'] );
		}
		if( true == isset( $arrmixValues['maintenance_request_id'] ) ) {
			$this->setMaintenanceRequestId( $arrmixValues['maintenance_request_id'] );
		}
		if( true == isset( $arrmixValues['action'] ) ) {
			$this->setAction( $arrmixValues['action'] );
		}
		if( true == isset( $arrmixValues['action_user_name'] ) ) {
			$this->setActionUserName( $arrmixValues['action_user_name'] );
		}
		if( true == isset( $arrmixValues['action_date'] ) ) {
			$this->setActionDate( $arrmixValues['action_date'] );
		}
		if( true == isset( $arrmixValues['maintenance_request_name'] ) ) {
			$this->setMaintenanceRequestName( $arrmixValues['maintenance_request_name'] );
		}
		if( true == isset( $arrmixValues['order_num'] ) ) {
			$this->setOrderNum( $arrmixValues['order_num'] );
		}
		if( true == isset( $arrmixValues['ap_codes_unit_of_measure_id'] ) ) {
			$this->setApCodesUnitOfMeasureId( $arrmixValues['ap_codes_unit_of_measure_id'] );
		}
		if( true == isset( $arrmixValues['event_id'] ) ) {
			$this->setEventId( $arrmixValues['event_id'] );
		}
		if( true == isset( $arrmixValues['consumption_unit_cost'] ) ) {
			$this->setConsumptionUnitCost( $arrmixValues['consumption_unit_cost'] );
		}
		if( true == isset( $arrmixValues['remaining_purchase_quantity'] ) ) {
			$this->setRemainingPurchaseQuantity( $arrmixValues['remaining_purchase_quantity'] );
		}
		if( true == isset( $arrmixValues['asset_location_name'] ) ) {
			$this->setAssetLocationName( $arrmixValues['asset_location_name'] );
		}
		if( true == isset( $arrmixValues['unit_of_measure_type_id'] ) ) {
			$this->setUnitOfMeasureTypeId( $arrmixValues['unit_of_measure_type_id'] );
		}
		if( true == isset( $arrmixValues['fixed_asset_accumulated_depreciation_amount'] ) ) {
			$this->setFixedAssetAccumulatedDepreciationAmount( $arrmixValues['fixed_asset_accumulated_depreciation_amount'] );
		}
		if( true == isset( $arrmixValues['fixed_asset_net_book_value'] ) ) {
			$this->setFixedAssetNetBookValue( $arrmixValues['fixed_asset_net_book_value'] );
		}

		return;
	}

	public static function assignSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'RETIRED', self::RETIRED );
		$objSmarty->assign( 'RETURNED', self::RETURNED );
		$objSmarty->assign( 'UNRETIRED', self::UNRETIRED );
		$objSmarty->assign( 'PURCHASED', self::PURCHASED );
		$objSmarty->assign( 'WORK_ORDER', self::WORK_ORDER );
		$objSmarty->assign( 'TRANSFERRED', self::TRANSFERRED );
		$objSmarty->assign( 'PLACED_IN_SERVICE', self::PLACED_IN_SERVICE );
		$objSmarty->assign( 'ASSET_EDITED', self::ASSET_EDITED );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valPropertyId( $objCompanyUser = NULL, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valId( $this->getPropertyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		} elseif( true == valObj( $objCompanyUser, 'CCompanyUser' ) && true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$strSql = \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $this->getCid(), $objCompanyUser->getId(), $objCompanyUser->getIsAdministrator(), true, true );

			$strSql .= ' AND p.id IN (
								SELECT
									property_id
								FROM
									property_preferences
								WHERE
									cid = ' . ( int ) $this->getCid() . '
									AND key = \'' . CPropertyPreference::TRACK_INVENTORY_QUANTITIES . '\'
									AND value = \'1\'
							)';

			$arrobjProperties = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchProperties( $strSql, $objClientDatabase );

			if( false == array_key_exists( $this->getPropertyId(), $arrobjProperties ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Please select valid property.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPostDepreciationPropertyId( $objCompanyUser = NULL, $objClientDatabase = NULL ) {

		$boolIsValid = true;

		if( false == valId( $this->getPropertyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Property is required.' ) ) );
		} elseif( true == $this->getIsValidatePropertyDepreciationSetting()
		          && true == valObj( $objCompanyUser, CCompanyUser::class )
		          && true == valObj( $objClientDatabase, CDatabase::class ) ) {

			$strSql = \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $this->getCid(), $objCompanyUser->getId(), $objCompanyUser->getIsAdministrator(), true, true );

			$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferenceByCidByPropertyIdByKey( $this->m_intCid, $this->getPropertyId(), CPropertyPreference::IS_ASSET_POST_DEPRECIATION, $objClientDatabase );

			if( false == valObj( $objPropertyPreference, CPropertyPreference::class ) ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Post depreciation is not active for this property.' ) ) );
				return false;
			}

			$strSql .= ' AND p.id = ' . ( int ) $objPropertyPreference->getPropertyId();

			$arrobjProperties = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchProperties( $strSql, $objClientDatabase );

			if( false == array_key_exists( $this->getPropertyId(), $arrobjProperties ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Please select valid property.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valApCodeId( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valId( $this->getApCodeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', 'Catalog item is required.' ) );

		} elseif( true == valObj( $objClientDatabase, 'CDatabase' ) ) {

			$arrobjApCodes = ( array ) \Psi\Eos\Entrata\CApCodes::createService()->fetchAllApCodesByAssetTypeIdsByCid( CAssetType::$c_arrintAssetTypes, $this->getCid(), $objClientDatabase );

			if( false == array_key_exists( $this->getApCodeId(), $arrobjApCodes ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', 'Please select valid catalog item.' ) );
			}

			$arrobjGlAccountProperties = ( array ) rekeyObjects( 'PropertyId', CGlAccountProperties::fetchGlAccountPropertiesByApCodeIdByCid( $this->getApCodeId(), $this->getCid(), $objClientDatabase ) );

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjGlAccountProperties ) && false == array_key_exists( $this->getPropertyId(), $arrobjGlAccountProperties ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_code_id', 'The gl account associated to this item is restricted and not available for use by the selected property. Please update the gl account setup, or select a different catalog item or property.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valQuantity( $boolIsRequestFromInventory = false, $boolIsCancelledItems = false ) {

		$boolIsValid = true;

		if( true == $boolIsRequestFromInventory && false == valId( $this->getQuantity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', 'Quantity is required.' ) );

		} elseif( false == $boolIsRequestFromInventory ) {

			if( 0 >= ( float ) $this->getDisplayQuantity() ) {

				$boolIsValid = false;
				if( false == $boolIsCancelledItems ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_quantity', 'Received quantity should be greater than zero.' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_quantity', 'Cancelled quantity should be greater than zero.' ) );
				}

			} elseif( ( float ) $this->getRemainingQuantity() < ( float ) $this->getDisplayQuantity() ) {

				$boolIsValid = false;
				if( false == $boolIsCancelledItems ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_quantity', 'Received quantity should not be greater than remaining quantity.' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_quantity', 'Cancelled quantity should not be greater than remaining quantity.' ) );
				}

			}
		}

		return $boolIsValid;
	}

	public function valCancelQuantity() {
		$boolIsValid = true;

		if( CAssetType::FIXED_ASSET_CAPITALIZED_AND_TRACKED == $this->getAssetTypeId() || CAssetType::FIXED_ASSET_EXPENSED_AND_TRACKED == $this->getAssetTypeId() || CUnitOfMeasureType::EACH == $this->getUnitOfMeasureTypeId() ) {

			$arrintDecimalVal = explode( '.', $this->getQuantity() );
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintDecimalVal ) && 0 < $arrintDecimalVal[1] ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_quantity', __( 'Cancel Quantity should not be in decimals for type Fixed Asset,Fixed Asset Non Depreciable OR Unit of measure type Each.' ) ) );

				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valUnitCost( $boolIsAllowNegativeAmount = false, $objApHeader = NULL ) {

		$boolIsValid = true;

		if( true == valObj( $objApHeader, 'CApHeader' ) && true == valId( $this->getPoApDetailId() ) ) {
			$objApDetail = $objApHeader->getApDetails()[$this->getPoApDetailId()];
		}

		if( false == valStr( $this->getUnitCost() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_cost', 'Unit cost is required.' ) );

		} elseif( 0 >= $this->getUnitCost() && false == $boolIsAllowNegativeAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_cost', 'Unit cost should be greater than zero.' ) );

		} elseif( ( true == valObj( $objApDetail, 'CApDetail' ) ) && ( false == ( $objApDetail->getTransactionAmount() >= 0 ? ( $this->getUnitCost() <= 0 ? false : true ) : ( $this->getUnitCost() >= 0 ? false : true ) ) && false != $boolIsAllowNegativeAmount ) ) {
			$boolIsValid = false;
			$strErrorMsg = ( $objApDetail->getTransactionAmount() >= 0 ) ? 'The Received Unit Cost for this item must be positive because it has a positive unit cost on the Purchase Order.' : 'The Received Unit Cost for this item must be negative because it has a negative unit cost on the Purchase Order';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_cost', $strErrorMsg ) );
		}

		return $boolIsValid;
	}

	public function valTransactionAmount( $boolIsRequired = false ) {

		$boolIsValid = true;

		// used in case of add fixed assets.
		if( true == $boolIsRequired && 0 >= $this->getTransactionAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'Purchase price is required.' ) );
		} elseif( false == is_null( $this->getTransactionAmount() ) && 9999999999999.99 < $this->getTransactionAmount() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'QTY x Unit Cost should not be greater than $9,999,999,999,999.99.' ) );
		}

		return $boolIsValid;
	}

	public function valPostDepreciationTransactionAmount( $objClientDatabase ) {

		$boolIsValid = true;
		if( 0 >= - $this->getTransactionAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Accumulated depreciation value should be greater than {%d, 0} and less than purchase price.', [ 0 ] ) ) );
		}

		$objInitialAssetTransaction = \Psi\Eos\Entrata\CAssetTransactions::createService()->fetchAssetTransactionByAssetIdByAssetTransactionTypeIdsByCid( $this->getAssetId(),  array_keys( CAssetTransactionType::createService()->getReceivedAssetTransactionTypes() ), $this->getCid(), $objClientDatabase );
		if( true == valObj( $objInitialAssetTransaction, CAssetTransaction::class ) && - $this->getTransactionAmount() > $objInitialAssetTransaction->getTransactionAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Transaction amount must be less that purchase price.' ) ) );
		}

		$arrintAssetTransactionTypeIds = CAssetTransactionType::createService()->getAssetTransactionTypesForNetBookValue();
		$arrmixAssetTransactions = ( array ) \Psi\Eos\Entrata\CAssetTransactions::createService()->fetchNetBookValueOfAssetTransactionByAssetIdsAssetTransactionTypeIdsByCid( [ $this->getAssetId() ], $arrintAssetTransactionTypeIds, $this->getCid(), $objClientDatabase );
		$arrmixRekeyedAssetTransactions = rekeyArray( 'asset_id', $arrmixAssetTransactions );
		$arrmixRekeyedAssetTransaction = getArrayElementByKey( $this->getAssetId(), $arrmixRekeyedAssetTransactions );

		if( - $this->getTransactionAmount() > $arrmixRekeyedAssetTransaction['net_book_value'] ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Transaction amount must be less than net book value.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPostDate( $boolIsRequestFromInventory = false, $boolIsRequestFromInventoryTransfer = false ) {

		$boolIsValid = true;
		$strPostDate = $this->getPostDate();

		if( ( true == $boolIsRequestFromInventory && true == $boolIsRequestFromInventoryTransfer && false == valStr( $strPostDate ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', 'Effective date is required.' ) );

		} elseif( ( true == $boolIsRequestFromInventory && false == valStr( $strPostDate ) ) || false == valStr( $strPostDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', 'Post date is required.' ) );

		} elseif( false == $boolIsRequestFromInventory && false == valStr( $strPostDate ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', 'Received date is required.' ) );

		} elseif( 1 != CValidation::checkDate( $strPostDate, false ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_date', CValidation::checkDate( $this->getPostDate(), false ) ) );

		}

		return $boolIsValid;
	}

	public function valReturnedQuantity() {

		$boolIsValid = true;

		if( date( 'm/Y' ) != date( 'm/Y', strtotime( $this->getPostMonth() ) )
		    && date( 'm/d/Y' ) != date( 'm/d/Y', strtotime( $this->getPostDate() ) )
		    && 0 >= ( float ) $this->getReturnedQuantity() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'returned_quantity', 'Return quantity should be greater than zero.' ) );

		} elseif( ( float ) $this->getReceivedQuantity() < ( float ) $this->getReturnedQuantity() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'returned_quantity', 'Return quantity should not be greater than received quantity.' ) );
		}

		return $boolIsValid;
	}

	public function valPostMonth( $boolIsRequestFromInventory = false, $objClientDatabase = NULL, $objApHeader = NULL ) {

		$boolIsValid       = true;
		$strPostMonth      = $this->getPostMonth();
		$objPostMonth      = new DateTime( $strPostMonth );
		$objBeginPostMonth = new DateTime( '01/01/1970' );
		$objEndPostMonth   = new DateTime( '12/01/2099' );

		if( false == valStr( $strPostMonth ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month is required.' ) );

		} elseif( false == CValidation::validateDate( $strPostMonth ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Invalid post month.' ) );

		} elseif( ( $objPostMonth->format( 'Y-m-d' ) < $objBeginPostMonth->format( 'Y-m-d' ) ) || ( $objPostMonth->format( 'Y-m-d' ) > $objEndPostMonth->format( 'Y-m-d' ) ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', ' Entrata does not support transactions outside the date year range of 1969 to 2099.' ) );

		} elseif( true == valObj( $objClientDatabase, 'CDatabase' )
		          && ( true == $boolIsRequestFromInventory || true == in_array( $this->getAssetTypeId(), CAssetType::$c_arrintInventoryAssetTypeIds ) ) ) {

			$objPropertyGlSetting = CPropertyGlSettings::fetchPropertyGlSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

			if( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' )
			    && ( strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getGlLockMonth() )
			         || strtotime( $strPostMonth ) <= strtotime( $objPropertyGlSetting->getApLockMonth() ) ) ) {

				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'The selected post month is locked for AP and GL.' ) );

			} elseif( false == is_null( $objApHeader ) && date( 'Y-m-d', strtotime( $this->getPostMonth() ) ) < date( 'Y-m-d', strtotime( $objApHeader->getPostMonth() ) ) ) {

				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Post month should not be less than the post month of the current purchase order.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valReturnedDate() {

		$boolIsValid = true;

		$strReturnedDate = $this->getReturnedDate();

		if( false == valStr( $strReturnedDate ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'returned_date', 'Return date is required.' ) );

		} elseif( 1 != CValidation::checkDate( $strReturnedDate, false ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'returned_date', CValidation::checkDate( $strReturnedDate, false ) ) );
		} elseif( strtotime( $strReturnedDate ) < strtotime( $this->getReceivedDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'returned_date', 'Return date should not be less than received date.' ) );
		}

		return $boolIsValid;
	}

	public function valUnitOfMeasureId() {

		$boolIsValid = true;

		if( false == valId( $this->getUnitOfMeasureId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_of_measure_id', 'Unit of measure is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDisplayUnitOfMeasureId() {

		$boolIsValid = true;

		if( false == valId( $this->getDisplayUnitOfMeasureId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Display unit of measure is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAssetLocationId( $objClientDatabase = NULL ) {

		$boolIsValid       = true;
		$arrintLocationIds = [];

		if( false == valId( $this->getAssetLocationId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Location is required.' ) ) );
		} elseif( true == valObj( $objClientDatabase, CDatabase::class ) ) {

			$arrmixAssetLocations = ( array ) \Psi\Eos\Entrata\CAssetLocations::createService()->fetchActiveAssetLocationsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase, 0 );

			foreach( $arrmixAssetLocations as $arrmixAssetLocation ) {
				$arrintLocationIds[$arrmixAssetLocation['id']] = $arrmixAssetLocation['id'];
			}

			if( false == array_key_exists( $this->getAssetLocationId(), $arrintLocationIds ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Please select valid location.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;

		if( false == valStr( $this->getMemo() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'memo', 'Memo is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCachedRemaining() {
		$boolIsValid = true;

		if( 0 > $this->getCachedRemaining() ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cached_remaining', 'Consumed item can not be returned.' ) );
		}

		return $boolIsValid;
	}

	public function valDebitGlAccountId( $objClientDatabase = NULL ) {

		$boolIsValid = true;

		if( false == valId( $this->getDebitGlAccountId() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_gl_account_id', __( 'Debit GL account is required.' ) ) );

			return false;
		} elseif( true == valObj( $objClientDatabase, CDatabase::class ) ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND id = ' . ( int ) $this->getDebitGlAccountId() . '
							AND disabled_by IS NOT NULL
							AND disabled_on IS NOT NULL';

			if( 0 != \Psi\Eos\Entrata\CGlAccounts::createService()->fetchGlAccountCount( $strWhere, $objClientDatabase ) ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Debit GL account has be disabled.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function valCreditGlAccountId( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valId( $this->getCreditGlAccountId() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_gl_account_id', __( 'Credit GL account is required.' ) ) );

			return false;
		} elseif( true == valObj( $objClientDatabase, CDatabase::class ) ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND id = ' . ( int ) $this->getCreditGlAccountId() . '
							AND disabled_by IS NOT NULL
							AND disabled_on IS NOT NULL';

			if( 0 != \Psi\Eos\Entrata\CGlAccounts::createService()->fetchGlAccountCount( $strWhere, $objClientDatabase ) ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( NULL, '', __( 'Credit GL account has be disabled.' ) ) );
				$boolIsValid &= false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objCompanyUser = NULL, $objClientDatabase = NULL, $boolIsPlacedInService = true, $objApHeader = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valQuantity();
				$boolIsValid &= $this->valUnitCost();
				$boolIsValid &= $this->valPostDate();
				break;

			case 'return_items':
				$boolIsValid &= $this->valReturnedQuantity();
				$boolIsValid &= $this->valPostMonth( $boolIsPlacedInService, $objClientDatabase, $objApHeader );
				$boolIsValid &= $this->valReturnedDate();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_inventory_adjustment':
				$boolIsValid &= $this->valPropertyId( $objCompanyUser, $objClientDatabase );
				$boolIsValid &= $this->valApCodeId( $objClientDatabase );
				$boolIsValid &= $this->valAssetLocationId( $objClientDatabase );
				$boolIsValid &= $this->valPostDate( true );
				$boolIsValid &= $this->valPostMonth( true, $objClientDatabase );
				$boolIsValid &= $this->valQuantity( true );
				$boolIsValid &= $this->valUnitOfMeasureId();
				$boolIsValid &= $this->valUnitCost();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valMemo();
				break;

			case 'validate_inventory_bulk_transaction':
				$boolIsValid &= $this->valPropertyId( $objCompanyUser, $objClientDatabase );
				$boolIsValid &= $this->valAssetLocationId( $objClientDatabase );
				$boolIsValid &= $this->valPostDate( true );
				$boolIsValid &= $this->valPostMonth( true, $objClientDatabase );
				$boolIsValid &= $this->valQuantity( true );
				$boolIsValid &= $this->valUnitOfMeasureId();
				$boolIsValid &= $this->valUnitCost();
				$boolIsValid &= $this->valTransactionAmount();
				$boolIsValid &= $this->valMemo();
				break;

			case 'validate_fixed_assets':
				if( false == $boolIsPlacedInService ) {
					$boolIsValid &= $this->valAssetLocationId();
				}

				$boolIsValid &= $this->valPostMonth( $boolIsRequestFromInventory = true, $objClientDatabase );
				$boolIsValid &= $this->valTransactionAmount( $boolIsRequired = true );
				break;

			case 'retire_item':
				$boolIsValid &= $this->valPostMonth( true, $objClientDatabase );
				$boolIsValid &= $this->valPostDate( true );
				break;

			case 'receive_item':
				$boolIsValid &= $this->valCachedRemaining();
				break;

			case 'update_items':
				$boolIsValid &= $this->valQuantity();
				$boolIsValid &= $this->valUnitCost( true, $objApHeader );
				$boolIsValid &= $this->valPostMonth( true, $objClientDatabase, $objApHeader );
				$boolIsValid &= $this->valPostDate();
				break;

			case 'cancel_items':
				$boolIsValid &= $this->valCancelQuantity();
				$boolIsValid &= $this->valQuantity( false, true );
				break;

			case 'validate_inventory_transfer':
				$boolIsValid &= $this->valPropertyId( $objCompanyUser, $objClientDatabase );
				$boolIsValid &= $this->valPostDate( true, true );
				$boolIsValid &= $this->valQuantity( true );
				$boolIsValid &= $this->valAssetLocationId( $objClientDatabase );
				break;

			case 'validate_fixed_asset_post_depreciation':
				$boolIsValid &= $this->valPostDepreciationPropertyId( $objCompanyUser, $objClientDatabase );
				$boolIsValid &= $this->valApCodeId( $objClientDatabase );
				$boolIsValid &= $this->valUnitOfMeasureId();
				$boolIsValid &= $this->valDisplayUnitOfMeasureId();
				$boolIsValid &= $this->valPostDate( false, false );
				$boolIsValid &= $this->valCreditGlAccountId( $objClientDatabase );
				$boolIsValid &= $this->valDebitGlAccountId( $objClientDatabase );
				$boolIsValid &= $this->valPostDepreciationTransactionAmount( $objClientDatabase );
				$boolIsValid &= $this->valAssetLocationId();
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function createDepreciationGlHeader() : \CGlHeader {
		$objGlHeader = new CGlHeader();
		$objGlHeader->setCid( $this->getCid() );
		$objGlHeader->setGlHeaderTypeId( CGlHeaderType::STANDARD );
		$objGlHeader->setGlTransactionTypeId( CGlTransactionType::ASSET_DEPRECIATION );
		$objGlHeader->setGlHeaderStatusTypeId( CGlHeaderStatusType::POSTED );
		$objGlHeader->setReferenceId( $this->getId() );
		$objGlHeader->setPropertyId( $this->getPropertyId() );
		$objGlHeader->setTransactionDatetime( date( 'm/d/Y' ) );
		$objGlHeader->setPostDate( $this->getPostDate() );
		$objGlHeader->setPostMonth( $this->getPostMonth() );
		$objGlHeader->setMemo( 'Asset Depreciation ' . $objGlHeader->getFormattedPostMonth() );

		return $objGlHeader;
	}

}

?>