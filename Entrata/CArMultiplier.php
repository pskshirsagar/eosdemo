<?php

class CArMultiplier extends CBaseArMultiplier {

	const NO_MULTIPLIER                   = 1;
	const TOTAL_RESPONSIBLE_APPLICANTS    = 2;
	const TOTAL_RESPONSIBLE_OCCUPANTS     = 3;
	const TOTAL_NON_RESPONSIBLE_OCCUPANTS = 4;
	const TOTAL_OCCUPANTS                 = 5;
	const TOTAL_VEHICLES                  = 6;
	const TOTAL_PETS                      = 7;
	const INSUFFICIENT_NOTICE             = 8;
	const EARLY_TERMINATION_DAYS          = 9;
	const TOTAL_RESERVATION_DAYS          = 10;
	const TOTAL_RESERVATION_HOURS         = 11;

	public static $c_arrintPerItemFormulaReferences = [
		self::TOTAL_RESPONSIBLE_APPLICANTS,
		self::TOTAL_RESPONSIBLE_OCCUPANTS,
		self::TOTAL_NON_RESPONSIBLE_OCCUPANTS,
		self::TOTAL_OCCUPANTS,
		self::TOTAL_VEHICLES,
		self::TOTAL_PETS
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>