<?php

class COnSiteContactEmailAddress extends CBaseOnSiteContactEmailAddress {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intCid ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intPropertyId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyOnSiteContactId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intPropertyOnSiteContactId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_on_site_contact_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEmailAddressTypeId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intEmailAddressTypeId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valMarketingName() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_strMarketingName ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'marketing_name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_strEmailAddress ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intIsPublished ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intOrderNum ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>