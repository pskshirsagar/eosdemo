<?php

class CMoveInScheduleAssociation extends CBaseMoveInScheduleAssociation {

	protected $m_boolUseAppointments;

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInScheduleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyBuildingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyFloorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyFloorplanId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCapSize() {
		$boolIsValid = true;
		if( false == valId( $this->getCapSize() ) && $this->getUseAppointments() != 1 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please enter valid Cap Size.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $intMoveInScheduleTypeId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( CMoveInScheduleType::LEASE_APPROVAL_DATE == $intMoveInScheduleTypeId ) {
					$boolIsValid &= $this->valCapSize();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setUseAppointments( $boolUseAppointments ) {
		$this->m_boolUseAppointments = $boolUseAppointments;
	}

	public function getUseAppointments() {
		return $this->m_boolUseAppointments;
	}

}
?>