<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyBankAccountLogs
 * Do not add any new functions to this class.
 */

class CPropertyBankAccountLogs extends CBasePropertyBankAccountLogs {

	public static function fetchPropertyBankAccountsLogByBankAccountLogIdByCid( $intBankAccountId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pbal.cid,
						pbal.property_id,
						pbal.bank_account_log_id,
						ac.item_gl_account_id AS gl_account_id,
						pbal.is_reimbursed_property,
						p.property_name,
						gat.name AS gl_account_name,
						gat.formatted_account_number AS gl_account_number
					FROM
						property_bank_account_logs pbal
						JOIN ap_codes ac ON ( pbal.cid = ac.cid AND pbal.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN properties p ON ( pbal.cid = p.cid AND pbal.property_id = p.id )
						LEFT JOIN gl_account_trees gat ON ( gat.cid = ac.cid AND ac.item_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						pbal.cid = ' . ( int ) $intCid . '
						AND pbal.bank_account_log_id = ' . ( int ) $intBankAccountId . '
					ORDER BY
						pbal.is_reimbursed_property';

		return parent::fetchPropertyBankAccountLogs( $strSql, $objDatabase );
	}
}
?>