<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobUnits
 * Do not add any new functions to this class.
 */

class CJobUnits extends CBaseJobUnits {

	public static function fetchJobUnitsByJobIdByUnitTypeIdsByCid( $intJobId, $arrintUnitTypeId, $intCid, $objDatabase ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) || false == valIntArr( $arrintUnitTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					*
					FROM
						job_units
					WHERE
						cid = ' . ( int ) $intCid . '
						AND unit_type_id IN ( ' . implode( ',', $arrintUnitTypeId ) . ' )
						AND job_id = ' . ( int ) $intJobId;

		return self::fetchJobUnits( $strSql, $objDatabase );
	}

}
?>