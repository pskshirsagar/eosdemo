<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyMergeFieldValues
 * Do not add any new functions to this class.
 */

class CCompanyMergeFieldValues extends CBaseCompanyMergeFieldValues {

	public static function fetchCompanyMergeFieldValuesByCompanyMergeFieldIdsByCid( $arrintCompanyMergeFieldIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyMergeFieldIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_merge_field_values cmfv
					WHERE
						cmfv.company_merge_field_id IN ( ' . implode( ',', $arrintCompanyMergeFieldIds ) . ' )
						AND cmfv.deleted_by IS NULL
						AND cmfv.deleted_on IS NULL
						AND cmfv.cid = ' . ( int ) $intCid . '
					ORDER BY cmfv.order_num ASC';

		return parent::fetchCompanyMergeFieldValues( $strSql, $objDatabase );
	}

	public static function fetchActiveCompanyMergeFieldValuesByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {
		if( false == valId( $intCompanyMergeFieldId ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						company_merge_field_values cmfv
					WHERE
						cmfv.company_merge_field_id = ' . ( int ) $intCompanyMergeFieldId . '
						AND cmfv.deleted_by IS NULL
						AND cmfv.deleted_on IS NULL
						AND cmfv.cid = ' . ( int ) $intCid . '
					ORDER BY cmfv.order_num ASC';

		return parent::fetchCompanyMergeFieldValues( $strSql, $objDatabase );

	}

	public static function fetchActiveCompanyMergeFieldValueByCompanyMergeFieldIdByCid( $intCompanyMergeFieldId, $intCid, $objDatabase ) {
		if( false == valId( $intCompanyMergeFieldId ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						company_merge_field_values cmfv
					WHERE
						cmfv.company_merge_field_id = ' . ( int ) $intCompanyMergeFieldId . '
						AND cmfv.deleted_by IS NULL
						AND cmfv.deleted_on IS NULL
						AND cmfv.cid = ' . ( int ) $intCid . '
					ORDER BY cmfv.order_num ASC';

		return self::fetchCompanyMergeFieldValue( $strSql, $objDatabase );
	}

}
?>