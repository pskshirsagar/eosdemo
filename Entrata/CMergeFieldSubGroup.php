<?php

class CMergeFieldSubGroup extends CBaseMergeFieldSubGroup {

	const DEFAULT_MERGE_FIELD_SUB_GROUP = 224;

	protected $m_arrobjDocumentMergeFields;
	protected $m_arrobjBlockDocumentMergeFields;

	/**
	 * Get Functions
	 */

	public function getDocumentMergeFields() {
		return $this->m_arrobjDocumentMergeFields;
	}

	public function getBlockDocumentMergeFields() {
		return $this->m_arrobjBlockDocumentMergeFields;
	}

	/**
	 * Set Functions
	 */

	public function setDocumentMergeFields( $arrobjDocumentMergeFields ) {
		$this->m_arrobjDocumentMergeFields = $arrobjDocumentMergeFields;
	}

	/**
	 * Add Functions
	 */

	public function addDocumentMergeField( $objDocumentMergeField ) {

	    if( true == valArr( $this->m_arrobjDocumentMergeFields ) && true == in_array( $objDocumentMergeField->getField(), getObjectFieldValuesByFieldName( 'Field', $this->m_arrobjDocumentMergeFields ) ) ) {
	        return;
        }

		if( true == valId( $objDocumentMergeField->getOrderNum() ) && ( false == valArr( $this->m_arrobjDocumentMergeFields ) || false == array_key_exists( $objDocumentMergeField->getOrderNum(), $this->m_arrobjDocumentMergeFields ) ) ) {
			$strKey = $objDocumentMergeField->getOrderNum();
		} else {
			$strKey = $objDocumentMergeField->getId();
		}

		$this->m_arrobjDocumentMergeFields[$strKey] = $objDocumentMergeField;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valMergeFieldGroupId() {
		$boolIsValid = true;
		if( true == is_null( $this->getMergeFieldGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merge_field_group_id', 'Merge Field Group is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );

		} else {

				if( false == is_null( $this->getMergeFieldGroupId() ) && 0 < CMergeFieldSubGroups::fetchMergeFieldSubGroupCountByNameByGroupId( $this->getName(), $this->getMergeFieldGroupId(), $this->getId(), $objDatabase ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duplicate record.', 'This Subgroup already exists.' ) );
				}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		if( false == ( is_numeric( $this->getOrderNum() ) && \Psi\CStringService::singleton()->preg_match( '/^\d{1,4}$/', $this->getOrderNum() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Please enter valid order number in the range 0-9999. ' ) );
		}

		return $boolIsValid;
	}

	public function validateDocumentMergeFields( $arrstrMappedMergeFields ) {
		if( false == $this->getDocumentMergeFields() && false == $this->getBlockDocumentMergeFields() ) return true;

		$this->m_intDocumentMergeFieldErrorMsgsCount = 0;
		$boolIsValid = true;

		if( true == valArr( $this->getDocumentMergeFields() ) ) {
			$arrobjDocumentMergeFields = $this->getDocumentMergeFields();

			foreach( $arrobjDocumentMergeFields as $objDocumentMergeField ) {
				if( false == CLeaseExecutionMergeFieldValidator::validate( $objDocumentMergeField, $arrstrMappedMergeFields ) ) {
					$this->m_intInvalidDocumentMergeFieldsCount += 1;
				}
			}
		}
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valMergeFieldGroupId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>