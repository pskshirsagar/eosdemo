<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CStudentLeasingCaps
 * Do not add any new functions to this class.
 */

class CStudentLeasingCaps extends CBaseStudentLeasingCaps {

	public static function fetchCustomStudentLeasingCapsByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCidBySpaceOptionSetting( $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $boolIsPriceBySpaceConfiguration, $objDatabase ) {

		$strSpaceConfigurationCondition = '';
		if( false == $boolIsPriceBySpaceConfiguration ) {
			$strSpaceConfigurationCondition .= ' AND slc.space_option_id IS NULL';
		} else {
			$strSpaceConfigurationCondition .= ' AND slc.space_option_id = r.space_configuration_id';
		}

		$strSql = ' SELECT 
						pf.id as property_floorplan_id,
						pf.floorplan_name,
						r.space_configuration_id as space_option_id,
						sc.name as space_option_name,
						count(DISTINCT us.id) as total_spaces_count,
						slc.cap_amount,
						slc.is_active,
						slc.id as student_leasing_cap_id
					FROM 
						property_floorplans pf
						JOIN unit_types ut ON ( pf.cid = ut.cid AND pf.property_id = ut.property_id AND pf.id = ut.property_floorplan_id )
						LEFT JOIN unit_spaces us ON ( pf.cid = us.cid AND pf.property_id = us.property_id AND us.property_floorplan_id = pf.id AND ut.id = us.unit_type_id AND us.unit_exclusion_reason_type_id = ' . CUnitExclusionReasonType::NOT_EXCLUDED . ' AND us.deleted_by IS NULL )
						JOIN rates r ON ( r.cid = pf.cid AND r.property_id = pf.property_id AND r.unit_type_id = ut.id AND pf.id = r.property_floorplan_id )
						LEFT JOIN student_leasing_caps slc ON ( slc.cid = pf.cid AND slc.property_id = pf.property_id AND slc.property_floorplan_id = pf.id AND slc.deleted_by IS NULL AND slc.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . ' AND slc.lease_term_id = ' . ( int ) $intLeaseTermId . $strSpaceConfigurationCondition . ' )
						LEFT JOIN space_configurations sc ON ( r.cid = sc.cid AND r.space_configuration_id = sc.id )
					WHERE
						pf.cid = ' . ( int ) $intCid . '
						AND pf.property_id = ' . ( int ) $intPropertyId . '
						AND r.ar_origin_id = ' . ( int ) CArOrigin::BASE . '
						AND r.ar_code_type_id = ' . ( int ) CArCodeType::RENT . '
						AND r.ar_cascade_id = ' . ( int ) CArCascade::UNIT_TYPE . '
						AND r.is_allowed = true
						AND r.is_renewal = false
						AND r.deactivation_date >= NOW()
						AND pf.deleted_by IS NULL
						AND ut.deleted_by IS NULL
					GROUP BY
						pf.id,
						pf.floorplan_name,
						r.space_configuration_id,
						sc.name,
						slc.cap_amount,
						slc.is_active,
						slc.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExistingStudentLeasingCapsByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						slc.*
					FROM
						student_leasing_caps slc
					WHERE
						slc.cid = ' . ( int ) $intCid . '
						AND slc.property_id = ' . ( int ) $intPropertyId . '
						AND slc.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND slc.lease_start_window_id  = ' . ( int ) $intLeaseStartWindowId . '
						AND slc.deleted_by IS NULL
						AND slc.deleted_on IS NULL';

		return self::fetchStudentLeasingCaps( $strSql, $objDatabase );
	}

	public static function fetchStudentLeasingCapsByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $intCid, $intPropertyId, $objDatabase, $intLeaseTermId = 0, $intLeaseStartWindowId = 0 ) {

		$strWhereConditionsInLeftJoin = '';
		if( true == valId( $intLeaseTermId ) && true == valId( $intLeaseStartWindowId ) ) {
			$strWhereConditionsInLeftJoin = ' AND slc.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . ' AND slc.lease_term_id = ' . ( int ) $intLeaseTermId;
		}

		$strSql = ' SELECT 
						pf.id as property_floorplan_id,
						pf.floorplan_name,
						CASE WHEN r.space_configuration_id IS NOT NULL THEN r.space_configuration_id ELSE NULL END as space_configuration_id,
						CASE WHEN r.space_configuration_id IS NOT NULL THEN sc.name ELSE NULL END as space_configuration_name,
						count( DISTINCT us.id ) as total_spaces_count,
						slc.id as student_leasing_cap_id,
						CASE WHEN slc.cap_amount IS NOT NULL THEN slc.cap_amount ELSE NULL END as cap_amount,
						CASE WHEN slc.is_active IS NOT NULL THEN slc.is_active ELSE false END as is_active
					FROM 
						property_floorplans pf
						JOIN unit_types ut ON ( pf.cid = ut.cid AND pf.property_id = ut.property_id AND pf.id = ut.property_floorplan_id )
						JOIN unit_spaces us ON ( pf.cid = us.cid AND pf.property_id = us.property_id AND ut.id = us.unit_type_id )
						JOIN rates r ON ( r.cid = pf.cid AND r.property_id = pf.property_id AND r.unit_type_id = us.unit_type_id AND pf.id = r.property_floorplan_id )
						LEFT JOIN student_leasing_caps slc ON ( slc.cid = pf.cid AND slc.property_id = pf.property_id AND slc.property_floorplan_id = pf.id ' . $strWhereConditionsInLeftJoin . ' )
						LEFT JOIN space_configurations sc ON ( r.cid = sc.cid AND r.space_configuration_id = sc.id )
					WHERE 
						pf.cid = ' . ( int ) $intCid . '
						AND pf.property_id = ' . ( int ) $intPropertyId . '
						AND r.ar_origin_id = ' . ( int ) CArOrigin::BASE . ' 
						AND r.ar_code_type_id = ' . ( int ) CArCodeType::RENT . '
						AND r.is_allowed = true 
						AND r.is_renewal = false 
						AND r.deactivation_date >= NOW()
						AND us.unit_exclusion_reason_type_id = ' . CUnitExclusionReasonType::NOT_EXCLUDED . '
						AND pf.deleted_by IS NULL
						AND ut.deleted_by IS NULL
						AND us.deleted_by IS NULL
						AND slc.deleted_by IS NULL
					GROUP BY
						pf.id,
						pf.floorplan_name,
						slc.cap_amount,
						slc.is_active,
						slc.id,
						CASE WHEN r.space_configuration_id IS NOT NULL THEN r.space_configuration_id END,
						CASE WHEN r.space_configuration_id IS NOT NULL THEN sc.name END;';

		return fetchData( $strSql, $objDatabase );
	}

}
?>