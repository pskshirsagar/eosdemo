<?php

class CPropertyApplicationPreference extends CBasePropertyApplicationPreference {

	const MULTIPLE_OPTIONS  = 'multiple_options';

	public static $c_arrstrAutoGenerateLeaseKeys = [ 'PRIMARY_APPLICANT_DEFAULT_LEASE_AGREEMENT', 'PRIMARY_APPLICANT_DEFAULT_LEASE_RENEWAL_AGREEMENT', 'COAPPLICANT_DEFAULT_LEASE_RENEWAL_AGREEMENT', 'GUARANTOR_DEFAULT_LEASE_RENEWAL_AGREEMENT' ];

	protected $m_intPropertySettingKeyId;

    public function setPropertySettingKeyId( $intPropertySettingKeyId ) {
    	$this->m_intPropertySettingKeyId = CStrings::strToIntDef( $intPropertySettingKeyId, NULL, false );
    }

    public function getPropertySettingKeyId() {
    	return $this->m_intPropertySettingKeyId;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function bindDetails( $arrmixDetails ) {
		$arrmixDetails = ( object ) $arrmixDetails;
		$this->setDetails( $arrmixDetails );
	}

}
?>