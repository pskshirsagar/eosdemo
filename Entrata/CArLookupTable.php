<?php
class CArLookupTable extends CBaseArLookupTable {

	protected $m_strArMultiplierName;

	protected $m_arrintArTriggerIds;

	/** @var \CArLookupLevel[] $m_arrobjArLookupLevels */
	protected $m_arrobjArLookupLevels;

	/**
	 * @return \CArLookupLevel[]
	 */
	public function getArLookupLevels() : array {
		return $this->m_arrobjArLookupLevels;
	}

	/**
	 * @param \CArLookupLevel[] $arrobjArLookupLevels
	 * @return CArLookupTable
	 */
	public function setArLookupLevels( array $arrobjArLookupLevels ) : CArLookupTable {
		$this->m_arrobjArLookupLevels = $arrobjArLookupLevels;

		return $this;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_multiplier_name'] ) ) $this->setArMultiplierName( $arrmixValues['ar_multiplier_name'] );
		if( true == isset( $arrmixValues['ar_trigger_ids'] ) ) $this->setArTriggerIds( $arrmixValues['ar_trigger_ids'] );

		return;
	}

	/**
	* Get Functions
	*/

	public function getArMultiplierName() {
		return $this->m_strArMultiplierName;
	}

	public function getArTriggerIds() {
		return $this->m_arrintArTriggerIds;
	}

	/**
	* Set Functions
	*/

	public function setArMultiplierName( $strArMultiplierName ) {
		$this->m_strArMultiplierName = $strArMultiplierName;
	}

	public function setArTriggerIds( $arrintArTriggerIds ) {
		$this->m_arrintArTriggerIds = CStrings::strToArrIntDef( $arrintArTriggerIds, NULL );
	}

	/**
	* Validate Functions
	*/

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) || 0 >= ( int ) $this->getId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'id', __( 'Valid id is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'cid', __( 'Valid client id is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) || 0 >= ( int ) $this->getPropertyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'property_id', __( 'Valid property id is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valArMultiplierId() {
		$boolIsValid = true;

		if( true == is_null( $this->getArMultiplierId() ) || 0 >= ( int ) $this->getArMultiplierId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'ar_multiplier_id', __( 'Charge per is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Name is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseRanges() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;

		if( true == is_null( $this->getDeletedBy() ) || 0 >= ( int ) $this->getDeletedBy() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'deleted_by', __( 'Deleted by is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getDeletedOn() ) || 0 >= ( int ) $this->getDeletedOn() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'deleted_by', __( 'Deleted on is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valArFormulaId() {
		$boolIsValid = true;

		if( false == valId( $this->getArFormulaId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'ar_formula_id', __( 'Formula is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valArFormulaReferenceId() {
		$boolIsValid = true;
		$strErrorMsg = NULL;

		switch( $this->getArFormulaId() ) {
			case CArFormula::PERCENT_OF_CHARGE_CODE:
				if( false == valId( $this->getArFormulaReferenceId() ) ) {
					$strErrorMsg = __( 'Charge Code is required.' );
				}
				break;

			case CArFormula::PERCENT_OF_CHARGE_CODE_GROUP:
				if( false == valId( $this->getArFormulaReferenceId() ) ) {
					$strErrorMsg = __( 'Charge Code Group is required.' );
				}
				break;

			case CArFormula::PERCENT_OF_CHARGE_CODE_TYPE:
				if( false == valId( $this->getArFormulaReferenceId() ) ) {
					$strErrorMsg = __( 'Charge Code Type is required.' );
				}
				break;

			default:
		}

		if( true == valStr( $strErrorMsg ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'ar_formula_reference_id', $strErrorMsg, NULL ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valArMultiplierId();
				$boolIsValid &= $this->valArFormulaId();
				$boolIsValid &= $this->valArFormulaReferenceId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		if( false == $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		return true;
	}

}
?>