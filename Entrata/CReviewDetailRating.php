<?php

class CReviewDetailRating extends CBaseReviewDetailRating {

	protected $m_strName;

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function getName() {
		return $this->m_strName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewRatingTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRating() {
		$boolIsValid = true;
		if( true == is_null( $this->getRating() ) || 1 > $this->getRating() || 5 < $this->getRating() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rating', sprintf( CDisplayMessages::create()->getMessage( 'RATING_ON_SCALE_1_TO_5' ), $this->getName() ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valRating();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function buildReviewsDatetimeQuery( $objReputationFilter ) {
		$strSql = ' ( ( rd.review_datetime AT TIME ZONE tz.time_zone_name )::DATE BETWEEN \'' . $objReputationFilter->getFromDate() . ' \' AND \'' . $objReputationFilter->getToDate() . ' \' ) ';
		return $strSql;
	}

}
?>