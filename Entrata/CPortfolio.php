<?php

class CPortfolio extends CBasePortfolio {

    public function valId() {
        $boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Id is required' ) );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client Id is required.' ) );
		}

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required' ) );
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valName();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valName();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valCid();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>