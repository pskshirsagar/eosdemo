<?php
class CRenewalOfferSpecials extends CBaseRenewalOfferSpecials {

	public static function fetchRenewalOfferSpecials( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRenewalOfferSpecial', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRenewalOfferSpecial( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRenewalOfferSpecial', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}
}
?>