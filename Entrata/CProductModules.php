<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProductModules
 * Do not add any new functions to this class.
 */

class CProductModules extends CBaseProductModules {

    public static function fetchProductModulesByModuleId( $intModuleId, $objDatabase ) {
		return parent::fetchProductModulesByModuleId( $intModuleId, $objDatabase );
    }

    public static function fetchPsProductIdsByModuleId( $intModuleId, $objDatabase ) {

		$strSql = 'SELECT 
						ps_product_id,
						ps_product_option_id
					FROM 
						product_modules 
					WHERE 
						module_id = ' . ( int ) $intModuleId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteProductModulesByModuleId( $intModuleId, $objDatabase, $boolIsReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM product_modules WHERE module_id = ' . ( int ) $intModuleId . ';';

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return false;
		} else {
			$objDataset->cleanup();
			return true;
		}
	}

}
?>