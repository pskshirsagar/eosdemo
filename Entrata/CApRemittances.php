<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApRemittances
 * Do not add any new functions to this class.
 */

class CApRemittances extends CBaseApRemittances {

	public static function fetchApRemittancesDetailByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						apr.id,
						apt.id as remittance_type_id,
						apt.name as remittance_type_name,
						apr.name as remittance_name,
						apr.vendor_remittance_id,
						apr.check_account_type_id,
						apr.check_name_on_account,
						apr.check_bank_name,
						apr.check_routing_number,
						apr.check_account_number_encrypted,
						apr.street_line1,
						apr.street_line2,
						apr.street_line3,
						apr.city,
						apr.state_code,
						apr.postal_code,
						apr.country_code,
						apr.is_verified,
						apr.verified_on,
						apr.giact_verification_response,
						ap.default_ap_remittance_id AS is_default
					FROM
						ap_remittances apr
						LEFT JOIN ap_payment_types apt ON ( apr.ap_payment_type_id = apt.id )
						LEFT JOIN ap_payees ap ON ( apr.cid = ap.cid AND apr.id = ap.default_ap_remittance_id )
					WHERE
						apr.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apr.cid = ' . ( int ) $intCid . '
						AND apr.is_published = true ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesDetailByIdsByApPayeeIdByCid( $arrintApRemittanceIds, $intApPayeeId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApRemittanceIds ) ) return NULL;

		$strSql = 'SELECT
					apr.*,
						apt.name AS payment_type_name,
						cat.name AS check_account_type_name
					FROM
						ap_remittances apr
						LEFT JOIN ap_payment_types apt ON ( apr.ap_payment_type_id = apt.id )
						LEFT JOIN check_account_types cat ON ( apr.check_account_type_id = cat.id )
					WHERE
						apr.id IN ( ' . implode( ',', $arrintApRemittanceIds ) . ' )
						AND apr.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apr.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittanceDetailsByIdByApPayeeIdByCid( $intApRemittanceId, $intApPayeeId, $intCid, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) || false == valId( $intApRemittanceId ) ) return NULL;

		$strSql = 'SELECT
						apr.*,
						apt.name AS payment_type_name,
						cat.name AS check_account_type_name
					FROM
						ap_remittances apr
						LEFT JOIN ap_payment_types apt ON ( apr.ap_payment_type_id = apt.id )
						LEFT JOIN check_account_types cat ON ( apr.check_account_type_id = cat.id )
					WHERE
						apr.id = ' . ( int ) $intApRemittanceId . '
						AND apr.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apr.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase, $boolIsDefaultRemittance = true, $boolIsFromBulkEdit = false, $boolIsPublished = false ) {
		$strWhere = '';
		if( false == valIntArr( $arrintApPayeeIds ) && false == $boolIsFromBulkEdit ) {
			return NULL;
		} elseif( true == $boolIsFromBulkEdit && true == valIntArr( $arrintApPayeeIds ) ) {
			$strWhere = ' AND apr.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' ) ';
		} elseif( false == $boolIsFromBulkEdit ) {
			$strWhere = ' AND apr.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' ) ';
		}

		if( true == $boolIsPublished && true == valStr( $strWhere ) ) {
			$strWhere .= ' AND apr.is_published = true ';
		}

		$strJoin    = '';
		$strColumns = '';

		if( true == $boolIsDefaultRemittance ) {
			$strColumns = ', apl.id AS ap_payee_location_id,';
			$strColumns .= 'apl.is_primary ';
			$strColumns .= ', apl.location_name, ap.company_name as company_name';
			$strJoin = ' JOIN ap_payee_locations apl ON ( apr.cid = apl.cid AND apr.ap_payee_id = apl.ap_payee_id AND apr.ap_payee_location_id = apl.id ) ';
			$strJoin .= ' JOIN ap_payees ap ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id ) ';
		}

		$strSql = 'SELECT
						apr.*
						' . $strColumns . '
					FROM
						ap_remittances apr
						' . $strJoin . '
						
					WHERE
						apr.cid = ' . ( int ) $intCid . $strWhere;

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittanceByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase, $boolIsVendorRemittance = false ) {
		if( false == valId( $intApPayeeId ) ) return NULL;

		$strWhere = ( true == $boolIsVendorRemittance ) ? ' AND vendor_remittance_id IS NOT NULL LIMIT 1' : '';

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId .
						$strWhere;

		return self::fetchApRemittance( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApRemittancesByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase, $intApPayeeLocationId = NULL ) {
		if( false == valId( $intApPayeeId ) ) return NULL;

		$strCondition = ( true == valId( $intApPayeeLocationId ) ) ? ' AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '' : '';

		$strSql = 'SELECT
						apr.*
					FROM
						ap_remittances apr
						JOIN company_ap_payment_type_associations capta ON ( capta.cid = ' . ( int ) $intCid . ' AND apr.ap_payment_type_id = capta.ap_payment_type_id )
					WHERE
						apr.cid = ' . ( int ) $intCid . ' 
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND capta.is_enabled = TRUE
						AND is_published = true' . $strCondition;

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	/**
	 * @TODO - Rename function to fetchApRemittancesByApPaymentTypeIdsByApPayeeIdsByCid( $arrintApPayeeTypeIds, $arrintApPayeeIds, $intCid, $objClientDatabase ) and change query accordingly
	 */
	public static function fetchApRemittancesWithApPaymentTypeByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND ap_payment_type_id = ' . CApPaymentType::CHECK;

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesByApPayeeIdsByIsPublishedByCid( $arrintApPayeeIds, $boolIsPublished = false, $intCid, $objClientDatabase, $boolIsDefault = true ) {
		if( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strCondition	= NULL;

		if( true == $boolIsPublished ) {
			$strCondition .= ' AND apr.is_published = true';
		}
		if( true == $boolIsDefault ) {
			$strCondition .= ' AND apr.is_default = true';
		}

		$strSql = 'SELECT
						apr.id,
						apl.id AS ap_payee_location_id,
						apr.check_account_number_encrypted,
						apr.street_line1,
						apr.street_line2,
						apr.street_line3,
						apr.state_code,
						apr.postal_code,
						apr.country_code,
						apr.city,
						apr.ap_payee_id,
						apl.is_primary,
						apr.is_published,
						apr.ap_payment_type_id,
						apr.is_default
					FROM
						ap_remittances apr
						JOIN ap_payee_locations apl ON ( apr.cid = apl.cid AND apr.ap_payee_id = apl.ap_payee_id AND apr.ap_payee_location_id = apl.id )
					WHERE
						apr.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND apl.deleted_by IS NULL
						AND apr.cid = ' . ( int ) $intCid . $strCondition;

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchAllPublishedApRemittancesDetailByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						apr.*,
						ap.default_ap_remittance_id,
						ap.customer_id
					FROM
						ap_remittances apr
						JOIN ap_payees ap ON ( apr.cid = ap.cid AND apr.ap_payee_id = ap.id )
					WHERE
						apr.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND ap.cid = ' . ( int ) $intCid . '
						AND apr.is_published = true';

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesByIdsByApPayeeIdByCid( $arrintApRemittanceIds, $intApPayeeId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApRemittanceIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						id IN ( ' . implode( ',', $arrintApRemittanceIds ) . ' )
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesByVendorRemittanceIdsByApPayeeIdByCid( $arrintVendorRemittanceIds, $intApPayeeId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintVendorRemittanceIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						vendor_remittance_id IN ( ' . implode( ',', $arrintVendorRemittanceIds ) . ' )
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchAllPublishedApRemittancesByApCustomerIdsByApPayeeTypeIdsByCid( $arrintCustomerIds, $arrintApPayeeTypeIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						apr.*,
						ap.default_ap_remittance_id,
						ap.customer_id
					FROM
						ap_remittances apr
						JOIN ap_payees ap ON ( apr.cid = ap.cid AND apr.ap_payee_id = ap.id )
					WHERE
						ap.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND ap.ap_payee_type_id IN( ' . implode( ', ', $arrintApPayeeTypeIds ) . ' )
						AND ap.cid = ' . ( int ) $intCid . '
						AND apr.is_published = true ';

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchSyncedApRemittancesByStoreIdByCIds( $intStoreId, $arrintCIds, $objClientDatabase ) {
		if( false == valIntArr( $arrintCIds ) ) return NULL;

		$strSql = 'SELECT
						ar.*
					FROM
						ap_remittances ar
						JOIN ap_payee_locations apl ON ( ar.ap_payee_location_id = apl.id AND ar.cid = apl.cid )
					WHERE
						ar.vendor_remittance_id IS NOT NULL
						AND apl.store_id = ' . ( int ) $intStoreId . '
						AND ar.cid IN ( ' . sqlIntImplode( $arrintCIds ) . ' ) ';

		return self::fetchApRemittances( $strSql, $objClientDatabase, false );
	}

	public static function fetchApRemittancesDetailByIdByApPayeeIdByCid( $intId, $intApPayeeId, $intCid, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						apr.id,
						apt.id as remittance_type_id,
						apt.name as remittance_type_name,
						apr.name as remittance_name,
						apr.vendor_remittance_id,
						apr.check_account_type_id,
						apr.check_name_on_account,
						apr.check_bank_name,
						apr.check_routing_number,
						apr.check_account_number_encrypted,
						apr.street_line1,
						apr.street_line2,
						apr.street_line3,
						apr.city,
						apr.state_code,
						apr.postal_code,
						apr.country_code,
						apr.is_verified,
						ap.default_ap_remittance_id AS is_default
					FROM
						ap_remittances apr
						LEFT JOIN ap_payment_types apt ON ( apr.ap_payment_type_id = apt.id )
						LEFT JOIN ap_payees ap ON ( apr.cid = ap.cid AND apr.id = ap.default_ap_remittance_id )
					WHERE
						apr.id = ' . ( int ) $intId . '
						AND apr.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apr.cid = ' . ( int ) $intCid . '
						AND apr.is_published = true ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function updateApRemittancesByVendorRemittanceIdsByApPayeeLocationIdsByApPayeeIdByCId( $arrintVendorRemittanceIds, $arrintLocationsIds, $intApPayeeId, $intCompanyUserId, $intCId ) {

		if( false == valArr( $arrintVendorRemittanceIds = getIntValuesFromArr( $arrintVendorRemittanceIds ) ) || false == valArr( $arrintLocationsIds = getIntValuesFromArr( $arrintLocationsIds ) ) ) {
			return NULL;
		}

		$strSql = 'UPDATE
						ap_remittances
					SET 
						vendor_remittance_id = NULL,
						updated_on = NOW(),
						updated_by = ' . ( int ) $intCompanyUserId . '
					WHERE
						vendor_remittance_id IN( ' . sqlIntImplode( $arrintVendorRemittanceIds ) . ' )
						AND ap_payee_location_id IN ( ' . sqlIntImplode( $arrintLocationsIds ) . ' )
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCId . ';';

		return $strSql;
	}

	public static function fetchActiveApRemittancesByApPayeeIdByApPaymentTypeIdByCid( $intApPayeeId, $intApPaymentTypeId, $intCid, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) || false == valId( $intApPaymentTypeId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_payment_type_id = ' . $intApPaymentTypeId . '
						AND is_published = true ';

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesByIdsByCid( $arrintApRemittanceIds, $boolIsPublished, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApRemittanceIds ) ) return NULL;

		$strCondition = ( true == $boolIsPublished ) ? ' AND is_published = true' : '';

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						id IN ( ' . implode( ',', $arrintApRemittanceIds ) . ' )
						AND cid = ' . ( int ) $intCid . $strCondition;

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchPublishedApRemittancesByIdsOrByApPayeeIdsByApPaymentTypeIdsByCid( $arrintApRemittanceIds, $arrintApPayeeIds, $arrintApPaymentTypeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeIds ) || false == valIntArr( $arrintApRemittanceIds ) || false == valIntArr( $arrintApPaymentTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payment_type_id IN ( ' . implode( ', ', $arrintApPaymentTypeIds ) . ' )
						AND (
								ap_payee_id IN ( ' . implode( ', ', $arrintApPayeeIds ) . ' )
								OR id IN ( ' . implode( ', ', $arrintApRemittanceIds ) . ' )
							)
						AND is_published = true';

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesByApPayeeIdByApPayeeLocationIdByCid( $intApPayeeId, $intApPayeeLocationId, $intCid, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) || false == valId( $intApPayeeLocationId ) ) return NULL;

		$strSql = 'SELECT
						apr.id,
						apt.id as remittance_type_id,
						apt.name as remittance_type_name,
						apr.name as remittance_name,
						apr.vendor_remittance_id,
						apr.check_account_type_id,
						apr.check_name_on_account,
						apr.check_bank_name,
						apr.check_routing_number,
						apr.check_account_number_encrypted,
						apr.street_line1,
						apr.street_line2,
						apr.street_line3,
						apr.city,
						apr.state_code,
						apr.postal_code,
						apr.country_code,
						apr.is_verified,
						apr.verified_on,
						apr.giact_verification_response,
						apr.is_default,
						apr.is_published,
						apa.id as ap_payee_account_id
					FROM
						ap_remittances apr
						LEFT JOIN ap_payment_types apt ON ( apr.ap_payment_type_id = apt.id )
						LEFT JOIN ap_payee_accounts apa ON ( apa.cid = apr.cid AND apa.default_ap_remittance_id = apr.id )
					WHERE
						apr.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apr.cid = ' . ( int ) $intCid . '
						AND apr.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND apr.is_published';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApRemittanceByApPayeeIdByApPayeeLocationIdByIdByCid( $intApPayeeId, $intApPayeeLocationId, $intId, $intCid, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) || false == valId( $intApPayeeLocationId ) || false == valId( $intId ) ) return NULL;

		$strSql = 'SELECT
						apr.id,
						apt.id as remittance_type_id,
						apt.name as remittance_type_name,
						apr.name as remittance_name,
						apr.vendor_remittance_id,
						apr.check_account_type_id,
						apr.check_name_on_account,
						apr.check_bank_name,
						apr.check_routing_number,
						apr.check_account_number_encrypted,
						apr.street_line1,
						apr.street_line2,
						apr.street_line3,
						apr.city,
						apr.state_code,
						apr.postal_code,
						apr.country_code,
						apr.is_verified,
						apr.verified_on,
						apr.giact_verification_response,
						apr.is_default,
						apr.is_published
					FROM
						ap_remittances apr
						JOIN ap_payment_types apt ON ( apr.ap_payment_type_id = apt.id )
					WHERE
						apr.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apr.id = ' . ( int ) $intId . '
						AND apr.cid = ' . ( int ) $intCid . '
						AND apr.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesByApPayeeIdsByApPayeeLocationIdsByCid( $arrintApPayeeIds, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND is_published = true';

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultApRemittanceByApPayeeLocationIdByCid( $intApPayeeLocationId, $intCid, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intApPayeeLocationId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND is_default = true LIMIT 1';

		return self::fetchApRemittance( $strSql, $objClientDatabase );
	}

	public static function fetchApRemittancesByApPayeeLocationIdsByApPayeeIdByCid( $arrintApPayeeLocationIds, $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ap_payee_location_id,
						array_to_string( array_agg( DISTINCT id ), \',\' ) AS ap_remittance_id
					FROM
						ap_remittances
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_payee_location_id IN ( ' . implode( ', ', $arrintApPayeeLocationIds ) . ' )
					GROUP BY
						ap_payee_location_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultApRemittanceByApPayeeIdsByApPayeeLocationIdsByCid( $arrintApPayeeIds, $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_remittances
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id IN ( ' . sqlIntImplode( $arrintApPayeeIds ) . ' )
						AND ap_payee_location_id IN ( ' . sqlIntImplode( $arrintApPayeeLocationIds ) . ' )
						AND is_default = TRUE
						AND is_published = TRUE';

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultApRemittancesByApPayeeLocationIdsByCids( $arrintApPayeeLocationIds, $arrintCids, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						id as ap_remittence_id,
						cid,
						ap_payee_location_id
					FROM
						ap_remittances
					WHERE
						cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND ap_payee_location_id IN ( ' . implode( ', ', $arrintApPayeeLocationIds ) . ' )
						AND is_default = TRUE';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSyncedApRemittanceByVendorRemittanceIdByCid( $intVendorRemittanceId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						id
					FROM
						ap_remittances
					WHERE
						vendor_remittance_id = ' . ( int ) $intVendorRemittanceId . '
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPublishedCheckApRemittancesByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						apr.id,
						apl.id AS ap_payee_location_id,
						apr.check_account_number_encrypted,
						apr.street_line1,
						apr.street_line2,
						apr.street_line3,
						apr.state_code,
						apr.postal_code,
						apr.country_code,
						apr.city,
						apr.ap_payee_id,
						apl.is_primary,
						apr.is_published,
						apr.ap_payment_type_id,
						apr.is_default
					FROM
						ap_remittances apr
						JOIN ap_payee_locations apl ON ( apr.cid = apl.cid AND apr.ap_payee_id = apl.ap_payee_id AND apr.ap_payee_location_id = apl.id )
					WHERE
						apr.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND apr.ap_payment_type_id = ' . CApPaymentType::CHECK . '
						AND apr.is_published
						AND apr.cid = ' . ( int ) $intCid;

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApRemittanceAddressesByApPayeeLocationIdsByApRemittanceIdsByLeaseCustomerIdsByCid( $arrintApPayeeLocationIds, $arrintApRemittanceIds, $arrintLeaseCustomerIds, $intCid, $objDatabase, $boolIsPublished = true ) {

		if( false == valArr( $arrintLeaseCustomerIds ) && false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = '';

		$strAndCondition = ( true == $boolIsPublished ) ? ' AND ar.is_published ' : '';

		if( true == valArr( $arrintLeaseCustomerIds ) ) {

			$strSql = ' 
						SELECT 
							NULL AS ap_payee_id,
							NULL AS remittance_id,
							NULL AS ap_payee_location_id,
							NULL AS location_name,
							NULL AS loaded_ap_remittance_id,
							NULL as remittance_name,
							NULL as remittance_type,
							ca.id AS customer_address_id,
							lc.id AS lease_customer_id,
							ca.street_line1,
							ca.street_line2,
							ca.street_line3,
							ca.state_code,
							ca.postal_code,
							ca.city,
							ca.country_code,
							NULL AS check_routing_number,
							NULL AS check_account_number_encrypted,
							NULL AS check_account_type_id
						FROM
							lease_customers lc
							JOIN lease_processes lp ON ( lp.cid = lc.cid AND lp.lease_id = lc.lease_id AND lp.customer_id IS NULL )
							JOIN lease_customers lc1 ON ( lc.cid = lc1.cid AND lc.lease_id = lc1.lease_id AND ( CASE
																													WHEN lp.refund_method_id = ' . CRefundMethod::SINGLE_REFUND_PRIMARY_RESIDENT . ' OR lp.refund_method_id = ' . CRefundMethod::SINGLE_REFUND_MULTIPLE_RESIDENTS . ' THEN
																														lc1.customer_type_id = ' . CCustomerType::PRIMARY . '
																													ELSE
																														lc1.customer_id = lc.customer_id
																												END ) )
							JOIN customer_addresses ca ON ( ca.cid = lc1.cid AND ca.customer_id = lc1.customer_id AND ca.address_type_id = ' . CAddressType::FORWARDING . ' )
						WHERE
							lc.cid = ' . ( int ) $intCid . '
							AND lc.id IN ( ' . implode( ',', $arrintLeaseCustomerIds ) . ' )';
		}
		if( true == valArr( $arrintApPayeeLocationIds ) ) {

			$strSqlUnionCondition = ( true == valArr( $arrintLeaseCustomerIds ) && true == valArr( $arrintApPayeeLocationIds ) ) ? $strSql .= ' UNION ALL ' : '';

			$strSql = $strSqlUnionCondition . '
						SELECT
							apl.ap_payee_id,
							ar.id as remittance_id,
							apl.id AS ap_payee_location_id,
							apl.location_name,
							ar.id AS loaded_ap_remittance_id,
							ar.name as remittance_name,
							apt.name as remittance_type,
							NULL  AS customer_address_id,
							NULL AS lease_customer_id,
							CASE WHEN 0 < length( concat( ar.street_line1, ar.street_line2, ar.street_line3, ar.city, ar.postal_code, ar.state_code )::varchar ) THEN ar.street_line1 ELSE apl.street_line1 END AS street_line1,
							CASE WHEN 0 < length( concat( ar.street_line1, ar.street_line2, ar.street_line3, ar.city, ar.postal_code, ar.state_code )::varchar ) THEN ar.street_line2 ELSE apl.street_line2 END AS street_line2,
							CASE WHEN 0 < length( concat( ar.street_line1, ar.street_line2, ar.street_line3, ar.city, ar.postal_code, ar.state_code )::varchar ) THEN ar.street_line3 ELSE apl.street_line3 END AS street_line3,
							CASE WHEN 0 < length( concat( ar.street_line1, ar.street_line2, ar.street_line3, ar.city, ar.postal_code, ar.state_code )::varchar ) THEN ar.state_code ELSE apl.state_code END AS state_code,
							CASE WHEN 0 < length( concat( ar.street_line1, ar.street_line2, ar.street_line3, ar.city, ar.postal_code, ar.state_code )::varchar ) THEN ar.postal_code ELSE apl.postal_code END AS postal_code,
							CASE WHEN 0 < length( concat( ar.street_line1, ar.street_line2, ar.street_line3, ar.city, ar.postal_code, ar.state_code )::varchar ) THEN ar.city ELSE apl.city END AS city,
							CASE WHEN 0 < length( concat( ar.street_line1, ar.street_line2, ar.street_line3, ar.city, ar.postal_code, ar.state_code )::varchar ) THEN ar.country_code ELSE apl.country_code END AS country_code,
							CASE WHEN ar.check_routing_number IS NOT NULL THEN ar.check_routing_number ELSE NULL END AS check_routing_number,
							CASE WHEN ar.check_account_number_encrypted IS NOT NULL THEN ar.check_account_number_encrypted ELSE NULL END AS check_account_number_encrypted,
							CASE WHEN ar.check_account_type_id IS NOT NULL THEN ar.check_account_type_id ELSE NULL END AS check_account_type_id
						FROM
							ap_payee_locations apl
							LEFT JOIN ap_remittances ar ON ( ar.cid = apl.cid
											AND ar.id IN ( ' . implode( ',', $arrintApRemittanceIds ) . ' )
											AND ar.ap_payment_type_id IN ( ' . CApPaymentType::CHECK . ', ' . CApPaymentType::AVID_PAY . ' , ' . CApPaymentType::WIRE_TRANSFER . ' , ' . CApPaymentType::DEBIT_CARD . ', ' . CApPaymentType::CREDIT_CARD . ' , ' . CApPaymentType::CASH . ', ' . CApPaymentType::WRITTEN_CHECK . ', ' . CApPaymentType::ACH . ' , ' . CApPaymentType::VIRTUAL_CARD . ' )
											 ' . $strAndCondition . '
											AND ar.ap_payee_location_id = apl.id
											 )
							LEFT JOIN ap_payment_types apt ON ( apt.id = ar.ap_payment_type_id )
						WHERE
							apl.cid = ' . ( int ) $intCid . '
							AND apl.id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultApRemittancesByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						id,
						ap_payee_location_id,
						is_default
					FROM
						ap_remittances
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_location_id IN ( ' . implode( ', ', $arrintApPayeeLocationIds ) . ' )
						AND is_default = TRUE';

		return self::fetchApRemittances( $strSql, $objClientDatabase );
	}

}
?>