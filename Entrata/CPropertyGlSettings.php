<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyGlSettings
 * Do not add any new functions to this class.
 */

class CPropertyGlSettings extends CBasePropertyGlSettings {

	public static function fetchPropertyGlSettingsByGlTreeByCid( $intGlTreeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						pgs.*,
						util_get_translated( \'property_name\', p.property_name, p.details ) AS property_name,
						util_get_translated( \'name\', gt.name, gt.details ) AS gl_tree_name
					FROM
						property_gl_settings pgs
						JOIN properties p ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						JOIN gl_trees gt ON ( gt.cid = pgs.cid AND gt.id = pgs.gl_tree_id )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.gl_tree_id = ' . ( int ) $intGlTreeId . '
					ORDER BY p.property_name ASC';

		return parent::fetchPropertyGlSettings( $strSql, $objDatabase );
	}

	public static function fetchActivePropertyGlSettingsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						pgs.*,
						util_get_translated( \'property_name\', p.property_name, p.details ) AS property_name,
						util_get_translated( \'name\', gt.name, gt.details ) AS gl_tree_name
					FROM
						property_gl_settings pgs
						JOIN properties p ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND p.is_disabled = 0 )
						LEFT JOIN gl_trees gt ON ( gt.cid = pgs.cid AND gt.id = pgs.gl_tree_id )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
					ORDER BY p.property_name ASC';

		return parent::fetchPropertyGlSettings( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyGlSettingsByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pgs.*,
						p.property_name
					FROM
						property_gl_settings pgs
						JOIN properties p ON( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						pgs.property_id = ' . ( int ) $intPropertyId . '
						AND pgs.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchPropertyGlSetting( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertyGlSettingDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pgs.property_id,
						pgs.ar_post_month,
						pgs.ap_post_month,
						pgs.gl_post_month,
						pgs.ar_lock_month,
						pgs.ap_lock_month,
						CASE 
							WHEN pgs.activate_standard_posting = \'t\' THEN 1 
						END as activate_standard_posting
					FROM
						property_gl_settings pgs
						JOIN properties p ON( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						pgs.property_id IN(' . implode( ',', $arrintPropertyIds ) . ')
						AND pgs.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertyGlSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pgs.*,
						p.property_name
					FROM
						property_gl_settings pgs
						JOIN properties p ON( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						pgs.property_id IN(' . implode( ',', $arrintPropertyIds ) . ')
						AND pgs.cid = ' . ( int ) $intCid;

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertyGlSettingsByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pgs.*,
						p.property_name
					FROM
						property_gl_settings pgs
						JOIN properties p ON( p.cid = pgs.cid AND p.id = pgs.property_id )
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ], ARRAY[' . CPsProduct::ENTRATA . '] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = pgs.property_id )
					WHERE
						pgs.cid = ' . ( int ) $intCid;

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyGlSettingByLeaseIdByCid( $intLeaseId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pgl.*
					FROM
						property_gl_settings pgl,
						leases l
					WHERE
						pgl.property_id = l.property_id
						AND pgl.cid = l.cid
						AND l.id = ' . ( int ) $intLeaseId . '
						AND l.cid = ' . ( int ) $intCid;

		return self::fetchPropertyGlSetting( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyGlSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM property_gl_settings WHERE property_id IN (  ' . implode( ',', array_filter( $arrintPropertyIds ) ) . ' ) AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyGlSettingsByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						property_gl_settings
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase, false );
	}

	public static function fetchPropertyGlSettingsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		if( true == is_null( $intBankAccountId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						property_gl_settings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND	(	ap_bank_account_id = ' . ( int ) $intBankAccountId . '
								OR rent_bank_account_id = ' . ( int ) $intBankAccountId . '
								OR deposit_bank_account_id = ' . ( int ) $intBankAccountId . '
								OR refund_bank_account_id = ' . ( int ) $intBankAccountId . '
							)';

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	public static function fetchMigratedPropertyGlSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM property_gl_settings WHERE is_ar_migration_mode = true AND property_id IN (' . implode( ',', $arrintPropertyIds ) . ') AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	public static function fetchMigratedPropertyGlSettingByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {

		if( true == is_null( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM property_gl_settings WHERE is_ar_migration_mode = true AND property_id =' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyGlSetting( $strSql, $objClientDatabase );
	}

	public static function fetchNonMigratedPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT property_id FROM property_gl_settings WHERE is_ar_migration_mode = false AND property_id IN (' . implode( ',', $arrintPropertyIds ) . ') AND cid = ' . ( int ) $intCid;
		$arrintNonMigratedPropertyIds = fetchData( $strSql, $objClientDatabase );
		$arrintNonMigratedPropertyIds = array_keys( ( array ) rekeyArray( 'property_id', $arrintNonMigratedPropertyIds ) );
		return $arrintNonMigratedPropertyIds;
	}

	public static function fetchPropertyGlSettingsByCids( $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id
					FROM
						property_gl_settings
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	/**
	 * Other Functions
	 */

	public static function retrieveNonInitialImportTransactionsFlagForMigratedPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}

		$strSql = 'SELECT
					    COUNT ( 1 ) AS count
					FROM
					    property_gl_settings pgs
					    JOIN LATERAL
					    (
					      SELECT
					          1
					      FROM
					          ar_transactions at
					      WHERE
					          at.cid = pgs.cid
					          AND at.property_id = pgs.property_id
					          AND NOT at.is_initial_import
					          AND NOT at.is_deleted
					          AND at.cid = ' . ( int ) $intCid . '
					          AND at.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
					      LIMIT
					          1
					    ) sub ON ( TRUE )
					WHERE
					    pgs.cid =  ' . ( int ) $intCid . '
					    AND pgs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
					    AND pgs.is_ar_migration_mode
					    AND pgs.activate_standard_posting';

			$arrmixResult = fetchData( $strSql, $objClientDatabase );
			return ( true == isset( $arrmixResult[0]['count'] ) && 0 < $arrmixResult[0]['count'] ) ? false : true;
	}

	public static function fetchPropertyGlSettingsByPropertyIdsByGlAccountIdByCid( $arrintPropertyIds, $intGlAccountId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intGlAccountId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT pgs.*,
						gat.gl_account_id
					FROM
						property_gl_settings pgs
						JOIN gl_account_trees gat ON ( pgs.cid = gat.cid )
						JOIN properties p ON ( pgs.cid = p.cid AND pgs.property_id = p.id )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.property_id IN(  ' . implode( ',', $arrintPropertyIds ) . ' )
						AND gat.gl_account_id = ' . ( int ) $intGlAccountId . '
						AND (
								pgs.ap_gl_account_id	= ' . ( int ) $intGlAccountId . '
								OR pgs.retained_earnings_gl_account_id = ' . ( int ) $intGlAccountId . '
								OR pgs.market_rent_gl_account_id = ' . ( int ) $intGlAccountId . '
								OR pgs.gain_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
								OR pgs.loss_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
								OR pgs.vacancy_loss_gl_account_id = ' . ( int ) $intGlAccountId . '
						)';

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	public static function fetchCoreEntrataPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $boolIsGetCountOnly = false ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		if( true == $boolIsGetCountOnly ) {
			$strSql = 'SELECT
							COUNT ( DISTINCT pgs.property_id )
						FROM
							property_gl_settings pgs
							JOIN property_products pp ON pgs.cid = pp.cid
						WHERE
							pgs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
							AND pgs.activate_standard_posting = true
							AND pp.ps_product_id = ' . CPsProduct::ENTRATA . '
							AND pgs.cid = ' . ( int ) $intCid;

			$arrintCoreEntrataPropertiesCount = fetchData( $strSql, $objClientDatabase );

			return $arrintCoreEntrataPropertiesCount[0]['count'];
		} else {
			$strSql = 'SELECT
							DISTINCT pgs.property_id,
							pgs.activate_standard_posting,
							pp.ps_product_id
						FROM
							property_gl_settings pgs
							JOIN property_products pp ON pgs.cid = pp.cid
						WHERE
							pgs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
							AND pgs.activate_standard_posting = true
							AND pp.ps_product_id = ' . CPsProduct::ENTRATA . '
							AND pgs.cid = ' . ( int ) $intCid;

			return fetchData( $strSql, $objClientDatabase );
		}
	}

	public static function fetchCurrentPostMonthsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pgs.ar_post_month AS post_month
					FROM
						property_gl_settings pgs
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND pgs.activate_standard_posting = true

					UNION

					SELECT
						pgs.ap_post_month AS post_month
					FROM
						property_gl_settings pgs
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND pgs.activate_standard_posting = true

					UNION

					SELECT
						pgs.gl_post_month AS post_month
					FROM
						property_gl_settings pgs
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND pgs.activate_standard_posting = true
					ORDER BY
						post_month';

		$arrstrPostMonths	= [];
		$arrstrData			= fetchData( $strSql, $objClientDatabase );

		foreach( $arrstrData as $arrstrDatum ) {
			$arrstrPostMonths[] = $arrstrDatum['post_month'];
		}

		return $arrstrPostMonths;
	}

	public static function fetchPropertyGlSettingsLockMonthsByPropertyIdsByPeriodClosingsFilterByCid( $arrintPropertyIds, $objPeriodClosingsFilter, $intCid, $objClientDatabase ) {

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
			|| ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() )
			|| false == valArr( $arrintPropertyIds ) ) {

			return NULL;
		}

		$strColumnName	= NULL;
		$strCondition	= NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strColumnName	= 'ar_lock_month';
			$strCondition	= $strCondition . '
								AND p.ar_locked_on IS NOT NULL
								AND p.post_month = pgs.ar_lock_month';
		}

		if( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strColumnName	= 'ap_lock_month';
			$strCondition	= $strCondition . '
								AND p.ap_locked_on IS NOT NULL
								AND p.post_month = pgs.ap_lock_month';
		}

		if( 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strColumnName	= 'gl_lock_month';
			$strCondition	= $strCondition . '
								AND p.gl_locked_on IS NOT NULL
								AND p.post_month = pgs.gl_lock_month';
		}

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {

			$strCondition = $strCondition . '
							AND ar_lock_month = ap_lock_month
							AND ap_lock_month = gl_lock_month';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {

			$strCondition = $strCondition . '
							AND ar_lock_month = ap_lock_month';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {

			$strCondition = $strCondition . '
							AND ap_lock_month = gl_lock_month';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {

			$strCondition = $strCondition . '
							AND ar_lock_month = gl_lock_month';
		}

		$strSql = 'SELECT
						DISTINCT DATE( TO_CHAR( ' . $strColumnName . ', \'mm/01/YYYY\' ) ) AS ' . $strColumnName . '
					FROM
						property_gl_settings pgs
						JOIN periods p ON ( p.cid = pgs.cid AND p.property_id = pgs.property_id )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ') ' .
						$strCondition . '
					ORDER BY ' .
						$strColumnName;

		$arrstrLockMonths = fetchData( $strSql, $objClientDatabase );

		if( false == valArr( $arrstrLockMonths ) ) return NULL;

		foreach( $arrstrLockMonths as $arrstrLockMonth ) {
			$arrstrResult[] = $arrstrLockMonth[$strColumnName];
		}

		return $arrstrResult;
	}

	public static function fetchDependentPropertyNamesForGlAccountUsageTypeValidationByGlAccountIdByCid( $intGlAccountId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intGlAccountId ) ) return NULL;

		$strJoinFields = '	pgs.ap_gl_account_id,
							pgs.retained_earnings_gl_account_id,
							pgs.market_rent_gl_account_id,
							pgs.gain_to_lease_gl_account_id,
							pgs.loss_to_lease_gl_account_id,
							pgs.vacancy_loss_gl_account_id';

		$strCondition = '	(
								pgs.ap_gl_account_id = ' . ( int ) $intGlAccountId . '
								AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::ACCOUNTS_PAYABLE . '
							) OR (
								pgs.retained_earnings_gl_account_id = ' . ( int ) $intGlAccountId . '
								AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::RETAINED_EARNINGS . '
							) OR (
								pgs.market_rent_gl_account_id = ' . ( int ) $intGlAccountId . '
								AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::RENT . '
							) OR (
								pgs.gain_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
								AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::GAIN_LOSS_ON_LEAVE . '
							) OR (
								pgs.loss_to_lease_gl_account_id = ' . ( int ) $intGlAccountId . '
								AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::GAIN_LOSS_ON_LEAVE . '
							) OR (
								pgs.vacancy_loss_gl_account_id = ' . ( int ) $intGlAccountId . '
								AND ga.gl_account_usage_type_id = ' . ( int ) CGlAccountUsageType::VACANCY_LOSS . '
							)';

		$strSql = 'SELECT
						DISTINCT p.property_name
					FROM
						property_gl_settings pgs
						JOIN gl_accounts ga ON ( pgs.cid = ga.cid AND ga.id IN ( ' . $strJoinFields . ' ) )
						JOIN properties p ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND ( ' . $strCondition . ' )
					ORDER BY
						p.property_name';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyGlSettingsForAutoRollPostMonthScriptByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pgs.*
					FROM
						property_gl_settings pgs
						JOIN properties p ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND p.is_disabled = 0 )
						JOIN time_zones tz ON ( tz.id = p.time_zone_id )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.activate_standard_posting = true
						AND ( NOW() AT TIME ZONE tz.time_zone_name::text ) > CURRENT_DATE';

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	/**
	 * @param $intCid
	 * @param $objClientDatabase
	 * @return \CPropertyGlSetting[]
	 * this could be used from script to quickly determine which properties need to roll
	 */
	public static function fetchCustomPropertyGlSettingsByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = '
			SELECT
				pgs.*,
				p.property_name
			FROM
				property_gl_settings pgs
				JOIN properties p ON( p.cid = pgs.cid AND p.id = pgs.property_id )
			WHERE
				pgs.property_id IN(  ' . implode( ',', $arrintPropertyIds ) . ' )
				AND pgs.cid IN(  ' . implode( ',', $arrintCids ) . ' )';

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function syncAllocationPriorityFromGlSettingByCid( $intCid, $objDatabase, $arrintPropertyIds = [] ) {

		$strCondition = ( true == valArr( $arrintPropertyIds ) ) ? ' AND pgs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '';

		$strSql = 'UPDATE
						property_gl_settings pgs
					SET
						allocate_rent_first = gs.allocate_rent_first
					FROM
						gl_settings gs
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						' . $strCondition . '
						AND pgs.use_custom_ar_codes = 0
						AND gs.cid = pgs.cid
						AND pgs.allocate_rent_first <> gs.allocate_rent_first';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyGlSettingsByArPaymentIdByCid( $intArPaymentId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    pgs.user_validation_mode
					FROM
					    property_gl_settings pgs
					    JOIN ar_payment_splits aps ON ( aps.cid = pgs.cid AND aps.property_id = pgs.property_id )
					WHERE
					    aps.cid = ' . ( int ) $intCid . '
					    AND aps.ar_payment_id = ' . ( int ) $intArPaymentId . '
					    AND aps.deleted_by IS NULL
					    AND aps.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPropertyGlSettingsIsMigrationModeCountByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {

		$strWhere = 'WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND is_ar_migration_mode = true';

		return self::fetchPropertyGlSettingCount( $strWhere, $objClientDatabase );
	}

	public static function fetchPropertyGlSettingByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM property_gl_settings WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchPropertyGlSetting( $strSql, $objClientDatabase );
	}

	public static function fetchCoreEntrataPropertiesCountByPropertyGroupIdsByCid( $arrintPropertyGroupIds, $intCid, $objClientDatabase, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						CASE
							WHEN
								COUNT( DISTINCT pgs.property_id ) = COUNT( DISTINCT lp.property_id )
							THEN
								1
							ELSE
								0
						END as entrata_properties
					FROM
						load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $arrintPropertyGroupIds, ',' ) . '] ) lp
						LEFT JOIN property_gl_settings pgs ON ( pgs.cid = ' . ( int ) $intCid . '
																AND lp.property_id = pgs.property_id
																' . ( ( false == $boolShowDisabledData ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
																AND pgs.activate_standard_posting = true
														)';

		$arrintPropertiesCount = fetchData( $strSql, $objClientDatabase );

		return $arrintPropertiesCount[0]['entrata_properties'];
	}

	public static function fetchLeaseExpirationStructurePropertiesByLeaseExpirationStructureIdAndByCid( $intLeaseExpirationStructureId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT(pgs.property_id) as property_id
					FROM
						property_gl_settings pgs
						JOIN lease_expiration_structures les ON ( les.cid = pgs.cid )
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.lease_expiration_structure_id = ' . ( int ) $intLeaseExpirationStructureId;

		$arrintResponses = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintResponses ) ) return [];

		foreach( $arrintResponses as $arrintResponse ) {
			$arrintLeaseExpirationStructurePropertyIds[$arrintResponse['property_id']] = $arrintResponse['property_id'];
		}
		return $arrintLeaseExpirationStructurePropertyIds;
	}

	public static function fetchGlSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT pgs.property_id,
						gt.name as name
					FROM
						gl_trees gt
						LEFT JOIN property_gl_settings pgs ON ( gt.cid = pgs.cid AND gt.id = pgs.gl_tree_id )
						LEFT JOIN properties p ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND p.is_disabled = 0 AND p.property_type_id <> 99 )
					WHERE
						gt.cid = ' . ( int ) $intCid . '
						AND pgs.property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND gt.deleted_by IS NULL
						AND gt.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyGlSettingsDataByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pgs.property_id,
						CASE 
							WHEN pgs.activate_standard_posting = \'t\' THEN 1 
						END as activate_standard_posting
					FROM
						property_gl_settings pgs
						JOIN properties p ON( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						pgs.property_id IN(' . implode( ',', $arrintPropertyIds ) . ')
						AND pgs.cid = ' . ( int ) $intCid;

		$arrintResponses = fetchData( $strSql, $objClientDatabase );

		if( false == valArr( $arrintResponses ) ) return [];

		foreach( $arrintResponses as $arrintResponse ) {
			$arrintActivateStandardPostingPropertyIdsStatus[$arrintResponse['property_id']] = $arrintResponse['activate_standard_posting'];
		}
		return $arrintActivateStandardPostingPropertyIdsStatus;

	}

	public static function fetchDailyGprPropertyGlSettingsByCid( $intCid, CDatabase $objClientDatabase ) {

		if( false == is_int( $intCid ) ) {
			return NULL;
		}

		$strSql = '
			SELECT
				pgs.*
			FROM
				property_gl_settings pgs
				JOIN properties p ON p.cid = pgs.cid AND p.id = pgs.property_id AND p.is_disabled = 0
			WHERE
				pgs.cid = ' . ( int ) $intCid . '
				AND pgs.auto_post_gpr_adjustment = ' . CPropertyGlSetting::AUTO_POST_GPR_ADJUSTMENT_DAILY . '
				AND pgs.activate_standard_posting = true
				AND pgs.is_ap_migration_mode = false
				AND pgs.is_ar_migration_mode = false;';

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase, $boolIsReturnKeyedArray = false );

	}

	public static function fetchEntrataEnabledPropertiesCountByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT ( DISTINCT property_id )
					FROM
						property_gl_settings
					WHERE
						activate_standard_posting = true
						AND cid = ' . ( int ) $intCid;

		$arrintCoreEntrataPropertiesCount = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintCoreEntrataPropertiesCount[0]['count'] ) ) ? $arrintCoreEntrataPropertiesCount[0]['count'] : 0;
	}

	public static function fetchActivateStandardPostingByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						pgs.property_id,						
						CASE 
							WHEN pgs.activate_standard_posting = \'t\' THEN 1 
						END as activate_standard_posting
					FROM
						property_gl_settings pgs
						JOIN properties p ON( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						pgs.property_id = ' . ( int ) $intPropertyId . '
						AND pgs.cid = ' . ( int ) $intCid;

		$arrintResponses = fetchData( $strSql, $objClientDatabase );
		if( true == valArr( $arrintResponses ) && 1 == $arrintResponses[0]['activate_standard_posting'] ) {
			return true;
		}

		return false;

	}

	public static function fetchPropertyGlSettingsByApPayeeLocationIdByApPayeeAccountIdByCid( $intApPayeeLocationId, $intApPayeeAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT pgs.*
					FROM
						property_gl_settings pgs 
						JOIN ap_details ad ON ( ad.cid = pgs.cid AND ad.property_id = pgs.property_id )
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id ) 
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '  
						AND ah.ap_payee_account_id = ' . ( int ) $intApPayeeAccountId;

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedCurrentPostMonthPropertiesByPeriodClosingsFilterByCid( $arrmixPeriodClosingsFilterFieldValues, $intCid, $objClientDatabase, $boolIsReturnCount = false, $intOffset = 0, $intPageLimit = 0, $strSortBy = NULL, $boolIsHistoricalAccessMode = false ) {

		if( false == valArr( $arrmixPeriodClosingsFilterFieldValues ) ) {
			return NULL;
		}

		$strOrderBy  = $strOffset = $strJoinCondition = '';
		$intIsDisabled          = ( int ) $boolIsHistoricalAccessMode;
		$strWhereCondition      = ' WHERE pgs.activate_standard_posting = true AND p.is_disabled = ' . $intIsDisabled;
		$arrstrPostMonthRange   = [];

		if( $boolIsHistoricalAccessMode ) {
			$strJoinCondition = ' JOIN property_products pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.ps_product_id = ' . \CPsProduct::HISTORICAL_ACCESS . ' )';
		}

		if( true == valArr( $arrmixPeriodClosingsFilterFieldValues['post_month'] ) ) {
			if( 'Locked' == $arrmixPeriodClosingsFilterFieldValues['status'] ) {
				$strCondition = 'AND ( pe.ar_locked_on IS NOT NULL OR pe.ap_locked_on IS NOT NULL OR pe.gl_locked_on IS NOT NULL )';
				if( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] ) && true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] ) ) {
					$arrstrPostMonthRange[] = ' ( pe.post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\' AND pe.post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\' ' . $strCondition . ' ) ';
				} elseif( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] ) ) {
					$arrstrPostMonthRange[] = ' pe.post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\' ' . $strCondition;
				} elseif( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] ) ) {
					$arrstrPostMonthRange[] = ' pe.post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\' ' . $strCondition;
				}
			} elseif( 'Open' == $arrmixPeriodClosingsFilterFieldValues['status'] ) {
				$strCondition = 'AND ( pe.ar_locked_on IS NULL OR pe.ap_locked_on IS NULL OR pe.gl_locked_on IS NULL )';
				if( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] ) && true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] ) ) {
					$arrstrPostMonthRange[] = ' ( pe.post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\' AND pe.post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\' ' . $strCondition . ' ) ';
				} elseif( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] ) ) {
					$arrstrPostMonthRange[] = ' pe.post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\' ' . $strCondition;
				} elseif( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] ) ) {
					$arrstrPostMonthRange[] = ' pe.post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\' ' . $strCondition;
				}
			} else {
				if( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] ) && true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] ) ) {
					$arrstrPostMonthRange[] = ' ( pgs.ar_post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\' AND pgs.ar_post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\') ';
					$arrstrPostMonthRange[] = ' ( pgs.ap_post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\' AND pgs.ap_post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\') ';
					$arrstrPostMonthRange[] = ' ( pgs.gl_post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\' AND pgs.gl_post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\') ';
				} elseif( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] ) ) {
					$arrstrPostMonthRange[] = ' pgs.ar_post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\'';
					$arrstrPostMonthRange[] = ' pgs.ap_post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\'';
					$arrstrPostMonthRange[] = ' pgs.gl_post_month >= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_start'] . '\'';
				} elseif( true == valStr( $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] ) ) {
					$arrstrPostMonthRange[] = ' pgs.ar_post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\'';
					$arrstrPostMonthRange[] = ' pgs.ap_post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\'';
					$arrstrPostMonthRange[] = ' pgs.gl_post_month <= \'' . $arrmixPeriodClosingsFilterFieldValues['post_month']['post_month_end'] . '\'';
				}
			}

			if( valArr( $arrstrPostMonthRange ) ) {
				$strWhereCondition .= ' AND ( ' . implode( ' OR ', $arrstrPostMonthRange ) . ' ) ';
			}
		}

		if( true == $boolIsReturnCount ) {
			$strSql = 'SELECT COUNT( DISTINCT pgs.property_id )';
		} else {
			$strSql = 'SELECT
							DISTINCT pgs.property_id,
							p.property_name,
							pgs.ar_post_month,
							pgs.ap_post_month,
							pgs.gl_post_month,
							pgs.ar_lock_month,
							pgs.ap_lock_month,
							pgs.gl_lock_month';
			$strOrderBy = ' ORDER BY ' . ( ( true == valStr( $strSortBy ) ) ? $strSortBy . ', property_name ASC ' : 'property_name ASC' );
			$strOffset = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageLimit;
		}

		$strSql .= ' FROM
						property_gl_settings pgs
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrmixPeriodClosingsFilterFieldValues['property_group_ids'] ) . ' ] ) lp ON ( pgs.cid = lp.cid AND lp.property_id = pgs.property_id )
						JOIN properties p ON ( lp.cid = p.cid AND lp.property_id = p.id )
						JOIN periods pe ON ( p.cid = pe.cid AND p.id = pe.property_id ) ' . $strJoinCondition . $strWhereCondition . $strOrderBy . $strOffset;

		if( true == $boolIsReturnCount ) {
			return self::fetchColumn( $strSql, 'count', $objClientDatabase );
		}

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyGlSettingsLockMonthsByPropertyIdByPeriodClosingsFilterByCid( $intPropertyId, $objPeriodClosingsFilter, $intCid, $objClientDatabase ) {

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
			|| ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() )
			|| false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strColumnName = NULL;
		$strCondition  = NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strColumnName = 'LEAST( pgs.ar_lock_month, pgs.ap_lock_month, pgs.gl_lock_month )';
			$strCondition  = 'AND ( p.ar_locked_on IS NOT NULL OR p.ap_locked_on IS NOT NULL OR p.gl_locked_on IS NOT NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strColumnName = 'LEAST( pgs.ar_lock_month, pgs.ap_lock_month )';
			$strCondition  = 'AND ( p.ar_locked_on IS NOT NULL OR p.ap_locked_on IS NOT NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strColumnName = 'LEAST( pgs.ap_lock_month, pgs.gl_lock_month )';
			$strCondition  = 'AND ( p.ap_locked_on IS NOT NULL OR p.gl_locked_on IS NOT NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strColumnName = 'LEAST( pgs.ar_lock_month, pgs.gl_lock_month )';
			$strCondition  = 'AND ( p.ar_locked_on IS NOT NULL OR p.gl_locked_on IS NOT NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strColumnName = 'pgs.ar_lock_month';
			$strCondition  = 'AND p.ar_locked_on IS NOT NULL';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strColumnName = 'pgs.ap_lock_month';
			$strCondition  = 'AND p.ap_locked_on IS NOT NULL';
		} else {
			$strColumnName = 'pgs.gl_lock_month';
			$strCondition  = 'AND p.gl_locked_on IS NOT NULL';
		}

		$strSubSql = 'SELECT
						' . $strColumnName . ' - INTERVAL \'3 years\' AS lock_month
					FROM
						property_gl_settings pgs
					WHERE
						pgs.cid = ' . ( int ) $intCid . '
						AND pgs.property_id = ' . ( int ) $intPropertyId;

		$strSql = 'SELECT
						p.post_month AS post_months
					FROM
						periods p
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id = ' . ( int ) $intPropertyId . '
						AND p.post_month >= ( ' . $strSubSql . ' )
						' . $strCondition . '
					ORDER BY 
						p.post_month DESC';

		$arrstrLockMonths = fetchData( $strSql, $objClientDatabase );

		if( false == valArr( $arrstrLockMonths ) ) {
			return NULL;
		}

		foreach( $arrstrLockMonths as $arrstrLockMonth ) {
			$arrstrResult[] = $arrstrLockMonth['post_months'];
		}

		return $arrstrResult;
	}

	public static function fetchPropertyGlSettingsOpenMonthsByPropertyIdByPeriodClosingsFilterByCid( $intPropertyId, $objPeriodClosingsFilter, $intCid, $objClientDatabase ) {

		if( false == valObj( $objPeriodClosingsFilter, 'CPeriodClosingsFilter' )
		    || ( 0 == $objPeriodClosingsFilter->getAccountsReceivable() && 0 == $objPeriodClosingsFilter->getAccountsPayable() && 0 == $objPeriodClosingsFilter->getGeneralLedger() )
		    || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strLockPostMonth = $strCurrentPostMonth = $strCondition  = NULL;

		if( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strLockPostMonth = 'LEAST( pgs.ar_lock_month, pgs.ap_lock_month, pgs.gl_lock_month )';
			$strCurrentPostMonth = 'GREATEST( pgs.ar_post_month, pgs.ap_post_month, pgs.gl_post_month )';
			$strCondition  = 'AND ( p.ar_locked_on IS NULL OR p.ap_locked_on IS NULL OR p.gl_locked_on IS NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strLockPostMonth = 'LEAST( pgs.ar_lock_month, pgs.ap_lock_month )';
			$strCurrentPostMonth = 'GREATEST( pgs.ar_post_month, pgs.ap_post_month )';
			$strCondition  = 'AND ( p.ar_locked_on IS NULL OR p.ap_locked_on IS NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strLockPostMonth = 'LEAST( pgs.ap_lock_month, pgs.gl_lock_month )';
			$strCurrentPostMonth = 'GREATEST( pgs.ap_post_month, pgs.gl_post_month )';
			$strCondition  = 'AND ( p.ap_locked_on IS NULL OR p.gl_locked_on IS NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() && 1 == $objPeriodClosingsFilter->getGeneralLedger() ) {
			$strLockPostMonth = 'LEAST( pgs.ar_lock_month, pgs.gl_lock_month )';
			$strCurrentPostMonth = 'GREATEST( pgs.ar_post_month, pgs.gl_post_month )';
			$strCondition  = 'AND ( p.ar_locked_on IS NULL OR p.gl_locked_on IS NULL )';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsReceivable() ) {
			$strLockPostMonth = 'pgs.ar_lock_month';
			$strCurrentPostMonth = 'pgs.ar_post_month';
			$strCondition  = 'AND p.ar_locked_on IS NULL';
		} elseif( 1 == $objPeriodClosingsFilter->getAccountsPayable() ) {
			$strLockPostMonth = 'pgs.ap_lock_month';
			$strCurrentPostMonth = 'pgs.ap_post_month';
			$strCondition  = 'AND p.ap_locked_on IS NULL';
		} else {
			$strLockPostMonth = 'pgs.gl_lock_month';
			$strCurrentPostMonth = 'pgs.gl_post_month';
			$strCondition  = 'AND p.gl_locked_on IS NULL';
		}

		$strLockPostMonth .= ' + INTERVAL \'1 MONTH\'';
		$strCurrentPostMonth .= ' - INTERVAL \'1 MONTH\'';

		$strSql = 'SELECT 
						p.post_month AS post_months 
					FROM
						periods p
						JOIN property_gl_settings pgs ON( p.cid = pgs.cid AND p.property_id = pgs.property_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id = ' . ( int ) $intPropertyId . '
						AND p.post_month BETWEEN ' . $strLockPostMonth . ' AND ' . $strCurrentPostMonth . '
						' . $strCondition . '
					ORDER BY
						p.post_month ASC';

		$arrstrMonths = fetchData( $strSql, $objClientDatabase );

		if( false == valArr( $arrstrMonths ) ) {
			return NULL;
		}

		foreach( $arrstrMonths as $arrstrMonth ) {
			$arrstrResult[] = $arrstrMonth['post_months'];
		}

		return $arrstrResult;
	}

	public static function fetchPropertyGlSettingsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT pgs.*
					FROM
						property_gl_settings pgs 
						JOIN ap_details ad ON ( ad.cid = pgs.cid AND ad.property_id = pgs.property_id )
						JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id ) 
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payee_id = ' . ( int ) $intApPayeeId;

		return self::fetchPropertyGlSettings( $strSql, $objClientDatabase );
	}

}