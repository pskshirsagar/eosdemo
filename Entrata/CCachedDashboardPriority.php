<?php

class CCachedDashboardPriority extends CBaseCachedDashboardPriority {

	protected $m_strPriority;
	protected $m_strPropertyName;
	protected $m_strTimeZoneName;

	/**
	 * Get Functions
	 */

	public function getPriority() {
		return $this->m_strPriority;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getTimeZoneName() {
		return $this->m_strTimeZoneName;
	}

	/**
	 * Set Functions
	 */

	public function setPriority( $strPriority ) {
		$this->m_strPriority = $strPriority;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setTimeZoneName( $strTimeZoneName ) {
		$this->m_strTimeZoneName = $strTimeZoneName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['priority'] ) ) 				$this->setPriority( $arrmixValues['priority'] );
		if( true == isset( $arrmixValues['property_name'] ) ) 			$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['time_zone_name'] ) ) 			$this->setTimeZoneName( $arrmixValues['time_zone_name'] );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModuleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>