<?php

class CBankAccountType extends CBaseBankAccountType {

	const SINGLE_PROPERTY		= 1;
	const SHARED				= 2;
	const INTER_COMPANY			= 3;

	public static function loadBankAccountTypes() {

		$arrstrBankAccountTypes = [
            self::SINGLE_PROPERTY		=> 'Single Property',
            self::SHARED				=> 'Shared',
            self::INTER_COMPANY		=> 'Inter-Company'
		];

		return $arrstrBankAccountTypes;
	}

}
?>