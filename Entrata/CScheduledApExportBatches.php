<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledApExportBatches
 * Do not add any new functions to this class.
 */

class CScheduledApExportBatches extends CBaseScheduledApExportBatches {

	public static function fetchPaginatedScheduledApExportBatchesByCid( $intCid, $objPagination, $objClientDatabase, $objApExportBatchesFilter = NULL, $arrintApExportBatchTypeIds = NULL ) {

		$strOrderBy					= 'saeb.id DESC';
		$arrstrAndSearchParameters	= [];

		if( true == valObj( $objApExportBatchesFilter, 'CApExportBatchesFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchSearchCriteria( $objApExportBatchesFilter, $intCid );
		}

		if( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSelectColumns = 'COUNT(1)';
		} else {
			$strSelectColumns = 'saeb.id,
								saeb.ap_export_batch_type_id,
								saeb.frequency_id,
								saeb.scheduled_start_on,
								saeb.scheduled_end_on,
								saeb.next_post_date,
								saeb.disabled_on,
						CASE
							WHEN MAX ( paeb.property_id ) <> MIN ( paeb.property_id ) THEN \'Multiple\'
							WHEN MAX ( paeb.property_id ) IS NULL THEN \'-\'
							ELSE MAX ( p.property_name )
						END AS property_name ';
		}

		$strSql = 'SELECT 
						' . $strSelectColumns . '
						FROM
						scheduled_ap_export_batches saeb
						LEFT JOIN property_ap_export_batches paeb ON ( saeb.cid = paeb.cid AND saeb.id = paeb.scheduled_ap_export_batch_id )
						LEFT JOIN properties p ON ( paeb.cid = p.cid AND p.id = paeb.property_id )
					WHERE
						saeb.cid = ' . ( int ) $intCid . '
						AND saeb.deleted_by IS NULL
						AND saeb.deleted_on IS NULL';

		if( true == valArr( $arrstrAndSearchParameters ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );
		}

		if( true == valArr( $arrintApExportBatchTypeIds ) ) {
			$strSql .= ' AND saeb.ap_export_batch_type_id IN ( ' . implode( ',', $arrintApExportBatchTypeIds ) . ' ) ';
		}

		$strSql .= ' GROUP BY
						saeb.id';
		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ', saeb.ap_export_batch_type_id,
						saeb.frequency_id,
						saeb.scheduled_start_on,
						saeb.scheduled_end_on,
						saeb.next_post_date,
						saeb.disabled_on
					ORDER BY ' . $strOrderBy .
			' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return ( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) ? parent::fetchColumn( 'SELECT count(1) FROM ( ' . $strSql . ' ) as count', 'count', $objClientDatabase ) : self::fetchScheduledApExportBatches( $strSql, $objClientDatabase );
	}

	public static function fetchScheduledApExportBatchesCountByCid( $intCid, $objClientDatabase, $objApExportBatchesFilter = NULL, $arrintApExportBatchTypeIds = NULL ) {

		$arrstrAndSearchParameters = [];

		if( true == valObj( $objApExportBatchesFilter, 'CApExportBatchesFilter' ) ) {
			$arrstrAndSearchParameters = self::fetchSearchCriteria( $objApExportBatchesFilter, $intCid );
		}

		$strSql = 'SELECT
						COUNT( DISTINCT saeb.id )
					FROM
						scheduled_ap_export_batches saeb
						JOIN property_ap_export_batches paeb ON ( saeb.cid = paeb.cid AND saeb.id = paeb.scheduled_ap_export_batch_id )
						LEFT JOIN properties p ON ( paeb.cid = p.cid AND p.id = paeb.property_id )
					WHERE
						saeb.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrstrAndSearchParameters ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );
		}

		if( true == valArr( $arrintApExportBatchTypeIds ) ) {
			$strSql .= ' AND saeb.ap_export_batch_type_id IN ( ' . implode( ',', $arrintApExportBatchTypeIds ) . ' ) ';
		}

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );

	}

	public static function fetchSearchCriteria( $objApExportBatchesFilter, $intCid ) {

		$arrstrAndSearchParameters = [];

		if( true == valArr( $objApExportBatchesFilter->getPropertyGroupIds() ) ) {
			$arrstrAndSearchParameters[] = ' paeb.property_id IN ( SELECT
								                                        pga.property_id
								                                      FROM
								                                        properties AS p
								                                        JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
								                                        JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
								                                      WHERE
								                                        p.cid = ' . ( int ) $intCid . '
								                                        AND p.disabled_on IS NULL
								                                        AND pg.id IN ( ' . implode( ',', $objApExportBatchesFilter->getPropertyGroupIds() ) . ' )
								                                        AND pg.deleted_by IS NULL
								                                        AND pg.deleted_on IS NULL
                                                                ) ';
		}

		if( true == valStr( $objApExportBatchesFilter->getStatus() ) && 'active' == $objApExportBatchesFilter->getStatus() ) {
			$arrstrAndSearchParameters[] = ' saeb.disabled_by IS NULL AND saeb.disabled_on IS NULL';
		} elseif( true == valStr( $objApExportBatchesFilter->getStatus() ) && 'disabled' == $objApExportBatchesFilter->getStatus() ) {
			$arrstrAndSearchParameters[] = ' saeb.disabled_by IS NOT NULL AND saeb.disabled_on IS NOT NULL';
		}

		if( true == valStr( $objApExportBatchesFilter->getStartDate() ) ) {
			$arrstrAndSearchParameters[] = ' saeb.scheduled_start_on >= DATE(\'' . $objApExportBatchesFilter->getStartDate() . '\')';
		}

		if( true == valStr( $objApExportBatchesFilter->getEndDate() ) ) {
			$arrstrAndSearchParameters[] = ' saeb.scheduled_start_on <= DATE(\'' . $objApExportBatchesFilter->getEndDate() . '\')';
		}

		if( true == valStr( $objApExportBatchesFilter->getPostMonth() ) ) {
			$arrstrAndSearchParameters[] = ' ad.post_month = \'' . $objApExportBatchesFilter->getPostMonth() . '\'';
		}

		return $arrstrAndSearchParameters;
	}

	public static function fetchCustomActiveScheduledApExportBatchesByNextPostDate( $strNextPostDate, $objClientDatabase ) {

		if( false == valStr( $strNextPostDate ) ) return NULL;

		$strSql = 'SELECT
						saeb.*,
						c.company_name
					FROM
						scheduled_ap_export_batches saeb
						JOIN clients c ON ( c.id = saeb.cid )
					WHERE
						saeb.next_post_date = \'' . $strNextPostDate . '\'
						AND saeb.disabled_by IS NULL
						AND saeb.disabled_on IS NULL
					ORDER BY
						saeb.id ASC';

		return self::fetchScheduledApExportBatches( $strSql, $objClientDatabase );
	}

	public static function fetchScheduledApExportBatchesByIdsByCid( $arrintScheduledApExportBatchIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintScheduledApExportBatchIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
                        saeb.*
                    FROM
                        scheduled_ap_export_batches saeb
                    WHERE
                         saeb.id IN ( ' . implode( ',', $arrintScheduledApExportBatchIds ) . ' )
                         AND saeb.cid =  ' . ( int ) $intCid . '
                         AND saeb.disabled_by IS NULL 
                         AND saeb.disabled_on IS NULL';

		return self::fetchScheduledApExportBatches( $strSql, $objDatabase );
	}

}
?>