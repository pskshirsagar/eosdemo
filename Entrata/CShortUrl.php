<?php

class CShortUrl extends CBaseShortUrl {

	protected $m_strShortUrl;

    public function getShortUrl() {
    	return $this->m_strShortUrl;
    }

    public function setShortUrl( $strShortUrl ) {
    	$this->m_strShortUrl = $strShortUrl;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSlug( $objDatabase ) {
        $boolIsValid = true;

        $objShortUrl = CShortUrls::fetchShortUrlBySlugByCid( $this->getSlug(), $this->getCid(), $objDatabase );

        if( true == valObj( $objShortUrl, 'CShortUrl' ) ) {
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valFullUrl() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function genrateUniqueSlug( $objDatabase ) {

    	if( true == is_null( $this->getCid() ) ) {
    		return;
    	}

    	$this->setSlug( generateRandomString( 6 ) );

    	if( false == $this->valSlug( $objDatabase ) ) {
    		$this->genrateUniqueSlug( $objDatabase );
    	}

    	return $this->getSlug();
    }

}
?>