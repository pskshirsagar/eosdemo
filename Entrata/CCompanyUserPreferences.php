<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserPreferences
 * Do not add any new functions to this class.
 */

class CCompanyUserPreferences extends CBaseCompanyUserPreferences {

	public static function fetchOrderedUserPreferencesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $strOrderBy = '' ) {
		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cid = ' . ( int ) $intCid;

		$strSql .= ( 0 < strlen( $strOrderBy ) ) ? ' ORDER BY ' . addslashes( $strOrderBy ) : '';

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPreferenceByCompanyUserIdByKeyByCid( $intCompanyUserId, $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND key = \'' . $strKey . '\'';

		return self::fetchCompanyUserPreference( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPreferenceByKeyByCid( $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						key = \'' . $strKey . '\'
						AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPreferenceCountByCompanyUserIdByKeyByCid( $intCompanyUserId, $strKey, $intCid, $objDatabase ) {
		$strWhere = ' WHERE
						company_user_id = ' . ( int ) $intCompanyUserId . '
						AND key = \'' . $strKey . '\'
						AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyUserPreferenceCount( $strWhere, $objDatabase );
	}

	// FIXME: Duplicate function - fetchCompanyUserPreferenceByKeyByCid()

	public static function fetchCompanyUserPreferencesByKeyByCid( $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND key = \'' . $strKey . '\' ';

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPreferencesByCompanyUserIdByKeysByCid( $intCompanyUserId, $arrstrKeys, $intCid, $objDatabase, $boolIsCheckValue ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cid = ' . ( int ) $intCid . '
						AND key IN ( \'' . implode( '\',\'',  $arrstrKeys ) . '\')';

		$strSql .= ( true == $boolIsCheckValue ) ? ' AND value IS NOT NULL' : '';

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );
	}

	public static function fetchUserPreferencesByCompanyUserIdByUserPreferenceKeysByCid( $intCompanyUserId, $arrstrUserPreferenceKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrUserPreferenceKeys ) ) return NULL;

		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND key IN ( \'' . implode( '\',\'',  $arrstrUserPreferenceKeys ) . '\')';

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPreferencesByCompanyUserIdsByKeysByCids( $arrintCompanyUserIds, $arrstrCompanyUserPreferenceKeys, $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						company_user_id IN (' . implode( ',', $arrintCompanyUserIds ) . ')
						AND key IN ( \'' . implode( '\',\'',  $arrstrCompanyUserPreferenceKeys ) . '\')
						AND cid IN (' . implode( ',', $arrintCids ) . ')';

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchCompanyUserPreferencesByCidsByReportType( $arrintCids, $strReportType, $objClientDatabase ) {
		$strSql = 'SELECT
						distinct on ( cu.id )
   						c.id AS cid,
    					cu.id AS company_user_id,
    					ce.id employee_id,
					    ce.email_address,
					    cupp.key AS cupkey,
					    cupp.value AS cupvalue,
					    p.id as property_id
				   FROM
					    clients c
					    JOIN company_users cu ON ( c.id = cu.cid AND cu.is_disabled = 0 AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					    JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id AND ce.employee_status_type_id = 1 )
					    JOIN company_user_preferences cupp ON ( cupp.cid = cu.cid AND cupp.company_user_id = cu.id AND cupp.key = \'INSURANCE_STATS_REPORT\' AND cupp.value IS NOT NULL )
					    LEFT JOIN company_preferences cp ON ( c.id = cp.cid AND ( cp.key = \'BLOCK_RESIDENT_INSURE_STATISTICS_EMAILS\' OR cp.key = \'DISABLE_WEBSITE_STATISTICS_TRACKING\' ) AND cp.value IS NOT NULL )
					    LEFT JOIN properties p on ( p.cid = cu.cid AND p.is_disabled = 0 )
					    LEFT JOIN view_company_user_properties cup on ( cu.is_administrator = 0 and cu.id = cup.company_user_id AND cu.cid = cup.cid )
				   WHERE
					    cu.username <> \'PS Admin\'
						AND ce.id IS NOT NULL
					    AND cp.id IS NULL
					    AND ce.email_address IS NOT NULL
					    AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
					    AND cupp.key = \'INSURANCE_STATS_REPORT\'
					    AND ( cupp.value = \'' . $strReportType . '\' OR cupp.value = \'WEEKLYMONTHLY\' )
					    AND ( cup.property_id = p.id OR cu.is_administrator = 1 )
					   	AND c.id IN ( ' . implode( ',', $arrintCids ) . ' ) ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserPreferenceByCompanyUserIdsByKeyByCid( $arrintCompanyUserIds, $strKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						company_user_id IN (' . implode( ',', $arrintCompanyUserIds ) . ')
						AND key = \'' . $strKey . '\'
						AND cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPerferenceByCompanyPrefenceKeyByCompanyUserIdByKeysByCid( $strCompanyPrefenceKey, $intCompanyUserId, $arrstrKeys, $intCid, $objDatabase ) {
		$strSql = 'SELECT *
					FROM
						company_user_preferences cup
						JOIN company_preferences cp ON ( cp.cid = cup.cid AND cp.key = \'' . $strCompanyPrefenceKey . '\' AND cp.value = \'1\' )
					WHERE
						cup.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cup.key IN ( \'' . implode( '\',\'',  $arrstrKeys ) . '\')
						AND cup.cid = ' . ( int ) $intCid;
		$arrmixCompanyUserPreference = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixCompanyUserPreference ) ? true : false );
	}

	public static function fetchCompanyUserPreferencesByKeyByValueByCid( $strKey, $strValue, $intCid, $objDatabase ) {
		if( false == valStr( $strKey ) || false == valStr( $strValue ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_user_preferences
					WHERE
						key = \'' . $strKey . '\'
						AND value = \'' . $strValue . '\'
						AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPreferencesByCompanyUserIdByCidByModuleIdByKeys( $intCompanyUserId, $intCid, $intModuleId, $arrstrKeys, $objDatabase ) {

		$strSql = 'WITH user_permission AS (
						SELECT
							cup.is_allowed,
							cup.is_inherited,
							cu.cid,
							cu.id AS company_user_id
						FROM
							company_users cu
							LEFT JOIN company_user_permissions cup ON ( cup.cid = cu.cid AND cup.company_user_id = cu.id AND cup.module_id = ' . ( int ) $intModuleId . ' )
						WHERE
							cu.id = ' . ( int ) $intCompanyUserId . '
							AND cu.cid = ' . ( int ) $intCid . '
							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
							AND cu.is_administrator = 0
				  	)
		
				  	SELECT
						cupr.key,
						cupr.value
				 	FROM
						user_permission up
						JOIN company_user_preferences cupr ON ( cupr.cid = up.cid AND cupr.company_user_id = up.company_user_id )
				 	WHERE
						up.is_allowed = 1
						AND up.cid = ' . ( int ) $intCid . '
						AND up.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cupr.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
				 	UNION
				  	SELECT
						cgpr.key,
						max( cgpr.value ) as value
				  	FROM
						user_permission up
						JOIN company_user_groups cug ON( cug.cid = up.cid AND cug.company_user_id = up.company_user_id )
						JOIN company_group_permissions cgp ON ( cgp.cid = up.cid AND cgp.company_group_id = cug.company_group_id )
						JOIN company_group_preferences cgpr ON ( cgpr.cid = up.cid AND cgpr.company_group_id = cug.company_group_id )
				  	WHERE
						( up.is_inherited = 1 OR up.is_inherited IS NULL )
						AND up.cid = ' . ( int ) $intCid . '
						AND up.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cgp.module_id = ' . ( int ) $intModuleId . '
						AND cgp.is_allowed = 1
						AND cgpr.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
				  	GROUP BY key';

		$arrstrResult = fetchData( $strSql, $objDatabase );
		$arrstrCompanyUserPreferences = [];

		if( true == valArr( $arrstrResult ) ) {
			foreach( $arrstrResult as $arrstrCompanyUserPreference ) {
				$arrstrCompanyUserPreferences[$arrstrCompanyUserPreference['key']] = $arrstrCompanyUserPreference['value'];
			}
		}

		return $arrstrCompanyUserPreferences;
	}

	public static function fetchCompanyUserPreferencesByKey( $strKey, $objDatabase ) {
		if( false == valStr( $strKey ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_user_preferences
					WHERE
						key = \'' . $strKey . '\'';

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPreferencesByCompanyUserIdsByKeyByKeyValuesByCid( $arrintCompanyUserIds, $strKey, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						company_user_preferences
					WHERE
						company_user_id IN (' . implode( ',', $arrintCompanyUserIds ) . ')
						AND key = \'' . $strKey . '\'
						AND value IN( \'NEVER\', \'DAILY\', \'WEEKLY\' )
						AND cid = ' . ( int ) $intCid;
		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );

	}

	/**
	 * @param            $intCid
	 * @param            $strKey
	 * @param array      $arrintCompanyUserIds
	 * @param \CDatabase $objDatabase
	 * @return array
	 */
	public static function fetchCompanyUserPreferencesByCidByKeyByCompanyUserIds( $intCid, $strKey, array $arrintCompanyUserIds, \CDatabase $objDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) ) {
			return [];
		}

		$strSql = 'SELECT 
						cid, 
						company_user_id,
						key,
						value
					FROM
						company_user_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND key = \'' . $strKey . '\'
						AND company_user_id IN( ' . implode( ',', $arrintCompanyUserIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPreferencesByCompanyUserIdsByCidByModuleIdByKeys( $arrintCompanyUserId, $intCid, $intModuleId, $arrstrKeys, $objDatabase ) {

		$strSql = 'WITH user_permission AS (
						SELECT
							cup.is_allowed,
							cup.is_inherited,
							cu.id AS company_user_id,
							cu.cid
						FROM
							company_users cu
							LEFT JOIN company_user_permissions cup ON ( cup.cid = cu.cid AND cup.company_user_id = cu.id AND cup.module_id = ' . ( int ) $intModuleId . ' )
						WHERE
							cu.id IN( ' . implode( ',', $arrintCompanyUserId ) . ' )
							AND cu.cid = ' . ( int ) $intCid . '
							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
							AND cu.is_administrator = 0
					)

					SELECT
						cupr.key,
						cupr.value,
						cupr.company_user_id
				 	FROM
						user_permission up
						JOIN company_user_preferences cupr ON ( cupr.cid = up.cid AND cupr.company_user_id = up.company_user_id )
				 	WHERE
						up.is_allowed = 1
						AND up.cid = ' . ( int ) $intCid . '
						AND up.company_user_id IN( ' . implode( ',', $arrintCompanyUserId ) . ' )
						AND cupr.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
					UNION
					SELECT
						cgpr.key,
						max( cgpr.value ) as value,
						cug.company_user_id
					FROM
						user_permission up
						JOIN company_user_groups cug ON( cug.cid = up.cid AND cug.company_user_id = up.company_user_id )
						JOIN company_group_permissions cgp ON ( cgp.cid = up.cid AND cgp.company_group_id = cug.company_group_id )
						JOIN company_group_preferences cgpr ON ( cgpr.cid = up.cid AND cgpr.company_group_id = cug.company_group_id )
					WHERE
						( up.is_inherited = 1 OR up.is_inherited IS NULL )
						AND up.cid = ' . ( int ) $intCid . '
						AND up.company_user_id IN( ' . implode( ',', $arrintCompanyUserId ) . ' )
						AND cgp.module_id = ' . ( int ) $intModuleId . '
						AND cgp.is_allowed = 1
						AND cgpr.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
					GROUP BY cug.company_user_id,key';

		$arrstrResult = fetchData( $strSql, $objDatabase );
		$arrstrCompanyUserPreferences = [];

		if( true == valArr( $arrstrResult ) ) {
			foreach( $arrstrResult as $arrstrCompanyUserPreference ) {
				$arrstrCompanyUserPreferences[$arrstrCompanyUserPreference['company_user_id']][$arrstrCompanyUserPreference['key']] = $arrstrCompanyUserPreference['value'];
			}
		}

		return $arrstrCompanyUserPreferences;
	}

	public static function fetchCompanyUserPreferencesByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						cup.*,
						ce.name_first,
						ce.name_last,
						CONCAT(ce.name_first, \' \', ce.name_last) AS user_names
					FROM
						company_user_preferences cup
						JOIN company_users cu ON ( cu.id = cup.company_user_id AND cup.cid = cu.cid )
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cup.key IN ( \'' . implode( '\',\'',  $arrstrKeys ) . '\')
						AND cup.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCompanyUserPreferencesByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						cup.*,
						ce.name_first,
						ce.name_last,
						CONCAT(ce.name_first, \' \', ce.name_last) AS name_full,
						cu.is_administrator
					FROM
						company_user_preferences cup
						JOIN company_users cu ON ( cu.id = cup.company_user_id AND cu.cid = ' . ( int ) $intCid . ' )
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND ce.cid = ' . ( int ) $intCid . ' )
					WHERE
						cup.key IN ( ' . sqlStrImplode( $arrstrKeys ) . ' )
						AND cup.cid = ' . ( int ) $intCid;

		return self::fetchCompanyUserPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyUsersByPreferenceKeyByCid( $strKey, $intCid, $objDatabase ) {

		// fetch active property count
		$intActivePropertiesCount = \Psi\Eos\Entrata\CProperties::createService()->fetchAllActivePropertiesCountByCid( $intCid, $objDatabase );

		$strSql = 'WITH user_property_details AS (
						SELECT
							cup.company_user_id,
							CONCAT(ce.name_first, \' \', ce.name_last) AS user_names,
							count(p.id) as property_count,
							cu.is_administrator
						FROM
							company_user_preferences cup
							LEFT JOIN company_users cu ON ( cu.id = cup.company_user_id )
							LEFT JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
							LEFT JOIN company_user_property_groups cupg ON cupg.cid = cu.cid AND cupg.company_user_id = cu.id
							LEFT JOIN property_groups pg ON ( pg.cid = cu.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
							LEFT JOIN property_group_associations pga ON pga.cid = pg.cid AND pga.property_group_id = pg.id
							LEFT JOIN properties p ON p.cid = pga.cid AND p.id = pga.property_id AND p.is_disabled = 0 AND p.termination_date IS NULL
						WHERE
							cup.key = \'' . $strKey . '\'
							AND cup.cid = ' . ( int ) $intCid . '
						GROUP BY
							cup.company_user_id,
							cu.is_administrator,
							user_names 
					)
					SELECT 
						upd.company_user_id,
						upd.user_names
					FROM
						user_property_details upd
					WHERE
						upd.is_administrator = 1 OR upd.property_count = ' . ( int ) $intActivePropertiesCount . '
					ORDER BY upd.user_names';

		return fetchData( $strSql, $objDatabase );
	}

}
?>