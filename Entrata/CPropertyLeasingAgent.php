<?php

class CPropertyLeasingAgent extends CBasePropertyLeasingAgent {

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strUserName;
	protected $m_strFullName;
	protected $m_intCompanyDepartmentId;

	protected $m_boolIsAdministrator;

	/**
	 * Get Functions
	 */

	public function getNameFirst() {
		 return $this->m_strNameFirst;
	}

	public function getNameLast() {
		 return $this->m_strNameLast;
	}

	public function getUserName() {
		 return $this->m_strUserName;
	}

	public function getFullName() {
		return $this->m_strFullName = $this->m_strNameFirst . ' ' . $this->m_strNameLast;
	}

	public function getIsAdministrator() {
		return $this->m_boolIsAdministrator;
	}

	public function getCompanyDepartmentId() {
		return $this->m_intCompanyDepartmentId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) ) 			$this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name_first'] ) : $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) 			$this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name_last'] ) : $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['username'] ) ) 			$this->setUserName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['username'] ) : $arrmixValues['username'] );
		if( true == isset( $arrmixValues['full_name'] ) ) 			$this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['full_name'] ) : $arrmixValues['full_name'] );
		if( true == isset( $arrmixValues['is_administrator'] ) ) 	$this->setIsAdministrator( $arrmixValues['is_administrator'] );
		if( true == isset( $arrmixValues['company_department_id'] ) ) 	$this->setCompanyDepartmentId( $arrmixValues['company_department_id'] );
		return;
	}

	public function setNameFirst( $strNameFirst ) {
		 $this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		 $this->m_strNameLast = $strNameLast;
	}

	public function setUserName( $strUserName ) {
		 $this->m_strUserName = $strUserName;
	}

	public function setFullName( $strFullName ) {
		$this->m_strFullName = $strFullName;
	}

	public function setIsAdministrator( $boolIsAdministrator ) {
		$this->m_boolIsAdministrator = $boolIsAdministrator;
	}

	public function setCompanyDepartmentId( $intCompanyDepartmentId ) {
		$this->m_intCompanyDepartmentId = $intCompanyDepartmentId;
	}

	/**
	 * Validate Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is empty.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', __( 'Company employee id is empty.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id is empty.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsDefault() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCompanyEmployeeId();
				$boolIsValid &= $this->valPropertyId();
				break;

			case 'rwx_insert_user':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCompanyEmployee( $objDatabase ) {

		return \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->m_intCompanyEmployeeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserPropertyGroup( $objDatabase ) {

		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchCompanyUserPropertyGroupByCidByPropertyIdByCompanyEmployeeId( $this->m_intCid, $this->m_intPropertyId, $this->m_intCompanyEmployeeId, $objDatabase );
	}

}
?>