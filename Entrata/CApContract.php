<?php

class CApContract extends CBaseApContract {

	protected $m_intPropertyId;
	protected $m_intJobStatusId;

	protected $m_boolUseRetention;
	protected $m_boolHasContractItems;

	protected $m_fltBudgetSummary;
	protected $m_fltContractTotal;

	protected $m_strVendor;
	protected $m_strJobName;
	protected $m_strJobOriginalEndDate;
	protected $m_strJobOriginalStartDate;
	protected $m_strJobActualStartDate;

	public function valApPayeeId() {
		$boolIsValid = true;
		if( false == valId( $this->getApPayeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', ' Vendor is required.' ) );
		}
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;

		if( false == valId( $this->getJobId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_id', 'Job is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOriginalStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getOriginalStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_start_date', 'Planned start date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getOriginalStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_start_date', 'Planned start date should be in mm/dd/yyyy format.' ) );
		}

		return $boolIsValid;
	}

	public function valOriginalEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getOriginalEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', 'Planned completion date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getOriginalEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', 'Planned completion date should be in mm/dd/yyyy format.' ) );
		}

		if( false == is_null( $this->getOriginalStartDate() ) && false == is_null( $this->getOriginalEndDate() ) && ( strtotime( $this->getOriginalStartDate() ) > strtotime( $this->getOriginalEndDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'original_end_date', 'Planned completion date cannot be earlier than Planned start date.' ) );
		}

		return $boolIsValid;
	}

	public function valIsContractExists( $objDatabase ) {
		$boolIsValid = true;

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND job_id = ' . ( int ) $this->getJobId() . '
						AND name = \'' . addslashes( $this->getName() ) . '\'
						AND id <> ' . ( int ) $this->getId();

		if( 0 < \Psi\Eos\Entrata\CApContracts::createService()->fetchApContractCount( $strWhere, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_contract_exists', 'Contract Name already exists' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valJobId();
				$boolIsValid &= $this->valOriginalStartDate();
				$boolIsValid &= $this->valOriginalEndDate();
				$boolIsValid &= $this->valApPayeeId();
				$boolIsValid &= $this->valIsContractExists( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getHasContractItems() {
		return $this->m_boolHasContractItems;
	}

	public function getBudgetSummary() {
		return $this->m_fltBudgetSummary;
	}

	public function getVendor() {
		return $this->m_strVendor;
	}

	public function getContractTotal() {
		return $this->m_fltContractTotal;
	}

	public function getJobStatusId() {
		return $this->m_intJobStatusId;
	}

	public function getJobName() {
		return $this->m_strJobName;
	}

	public function getJobOriginalEndDate() {
		return $this->m_strJobOriginalEndDate;
	}

	public function getJobOriginalStartDate() {
		return $this->m_strJobOriginalStartDate;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getUseRetention() {
		return $this->m_boolUseRetention;
	}

	public function getJobActualStartDate() {
		return $this->m_strJobActualStartDate;
	}

	/**
	 * Set Functions
	 */

	public function setHasContractItems( $boolHasContractItems ) {
		$this->m_boolHasContractItems = CStrings::strToBool( $boolHasContractItems );
	}

	public function setBudgetSummary( $fltBudgetSummary ) {
		$this->m_fltBudgetSummary = CStrings::strToFloatDef( $fltBudgetSummary, NULL, false, 2 );
	}

	public function setVendor( $strVendor ) {
		$this->m_strVendor = CStrings::strTrimDef( $strVendor, 50, NULL, true );
	}

	public function setContractTotal( $fltContractTotal ) {
		$this->m_fltContractTotal = CStrings::strToFloatDef( $fltContractTotal, NULL, false, 2 );
	}

	public function setJobStatusId( $intJobStatusId ) {
		$this->set( 'm_intJobStatusId', CStrings::strToIntDef( $intJobStatusId, NULL, false ) );
	}

	public function setJobName( $strJobName ) {
		 $this->m_strJobName = CStrings::strTrimDef( $strJobName, 50, NULL, true );
	}

	public function setJobOriginalEndDate( $strJobOriginalEndDate ) {
		$this->m_strJobOriginalEndDate = $strJobOriginalEndDate;
	}

	public function setJobOriginalStartDate( $strJobOriginalStartDate ) {
		$this->m_strJobOriginalStartDate = $strJobOriginalStartDate;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setUseRetention( $boolUseRetention ) {
		$this->m_boolUseRetention = $boolUseRetention;
	}

	public function setJobActualStartDate( $strJobActualStartDate ) {
		$this->m_strJobActualStartDate = $strJobActualStartDate;
	}

	/*
	 * Create functions
	 */

	public function createApHeader( $objDatabase ) {

		$objApHeader = new CApHeader();
		$objApHeader->setId( $objApHeader->fetchNextId( $objDatabase ) );
		$objApHeader->setCid( $this->getCid() );
		$objApHeader->setApContractId( $this->getId() );
		$objApHeader->setGlTransactionTypeId( CGlTransactionType::AP_CHARGE );
		$objApHeader->setTransactionDatetime( 'NOW()' );
		$objApHeader->setApHeaderTypeId( CApHeaderType::CONTRACT );
		$objApHeader->setPostDate( date( 'm/d/Y' ) );
		$objApHeader->setPostMonth( date( 'm/Y', strtotime( $this->getOriginalStartDate() ) ) );

		return $objApHeader;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchEventsByEventSubTypeIds( $arrintEventTypeIds, $objDatabase, $objPagination = NULL ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchPaginatedEventsByReferenceIdByReferenceTypeIdByEventSubTypeIdsByCid( $this->getId(), CEventReferenceType::CONTRACT, $arrintEventTypeIds, $this->getCid(), $objDatabase, $objPagination );
	}

	public function fetchEventsCountByEventSubTypeIds( $arrintEventTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsCountByReferenceIdByReferenceTypeIdByEventSubTypeIdsByCid( $this->getId(), CEventReferenceType::CONTRACT, $arrintEventTypeIds, $this->getCid(), $objDatabase );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['vendor'] ) && $boolDirectSet ) {
			$this->m_strVendor = trim( $arrmixValues['vendor'] );
		} else {
			if( isset( $arrmixValues['vendor'] ) ) {
				$this->setVendor( $arrmixValues['vendor'] );
			}
		}
		if( isset( $arrmixValues['contract_total'] ) && $boolDirectSet ) {
			$this->m_fltContractTotal = trim( $arrmixValues['contract_total'] );
		} else {
			if( isset( $arrmixValues['contract_total'] ) ) {
				$this->setContractTotal( $arrmixValues['contract_total'] );
			}
		}

		if( isset( $arrmixValues['job_status_id'] ) && $boolDirectSet ) {
			$this->set( 'm_intJobStatusId', trim( $arrmixValues['job_status_id'] ) );
		} elseif( isset( $arrmixValues['job_status_id'] ) ) {
			$this->setJobStatusId( $arrmixValues['job_status_id'] );
		}

		if( isset( $arrmixValues['job_name'] ) && $boolDirectSet ) {
			$this->set( $arrmixValues['job_name'] );
		} elseif( isset( $arrmixValues['job_name'] ) ) {
			$this->setJobName( $arrmixValues['job_name'] );
		}

		if( isset( $arrmixValues['job_original_start_date'] ) && $boolDirectSet ) {
			$this->set( $arrmixValues['job_original_start_date'] );
		} elseif( isset( $arrmixValues['job_original_start_date'] ) ) {
			$this->setJobOriginalStartDate( $arrmixValues['job_original_start_date'] );
		}

		if( isset( $arrmixValues['job_original_end_date'] ) && $boolDirectSet ) {
			$this->set( $arrmixValues['job_original_end_date'] );
		} elseif( isset( $arrmixValues['job_original_end_date'] ) ) {
			$this->setJobOriginalEndDate( $arrmixValues['job_original_end_date'] );
		}

		if( isset( $arrmixValues['property_id'] ) && $boolDirectSet ) {
			$this->set( $arrmixValues['property_id'] );
		} elseif( isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}

		if( isset( $arrmixValues['use_retention'] ) && $boolDirectSet ) {
			$this->set( $arrmixValues['use_retention'] );
		} elseif( isset( $arrmixValues['use_retention'] ) ) {
			$this->setUseRetention( $arrmixValues['use_retention'] );
		}

		if( isset( $arrmixValues['job_actual_start_date'] ) && $boolDirectSet ) {
			$this->set( $arrmixValues['job_actual_start_date'] );
		} elseif( isset( $arrmixValues['job_actual_start_date'] ) ) {
			$this->setJobActualStartDate( $arrmixValues['job_actual_start_date'] );
		}
	}

}
?>