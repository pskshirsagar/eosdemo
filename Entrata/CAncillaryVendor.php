<?php

use Psi\Libraries\UtilConfig\CConfig;
use Psi\Libraries\Cryptography\CCrypto;
use function Psi\Libraries\UtilFunctions\valStr;

class CAncillaryVendor extends CBaseAncillaryVendor {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAncillaryTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUsernameEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPasswordEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPromotionalDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLearnMoreLink() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEflLink() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTerm() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getUsername() {
		if( false == valStr( $this->getUsernameEncrypted() ) ) {
			return NULL;
		}
		return CCrypto::createService()->decrypt( $this->getUsernameEncrypted(), CConfig::createService()->get( 'SODIUM_KEY_RESIDENT_PORTAL_ANCILLARY' ), [ 'legacy_secret_key' => CConfig::createService()->get( 'KEY_RESIDENT_PORTAL_ANCILLARY' ) ] );
	}

	public function getPassword() {
		if( false == valStr( $this->getPasswordEncrypted() ) ) {
			return NULL;
		}
		return CCrypto::createService()->decrypt( $this->getPasswordEncrypted(), CConfig::createService()->get( 'SODIUM_KEY_RESIDENT_PORTAL_ANCILLARY' ), [ 'legacy_secret_key' => CConfig::createService()->get( 'KEY_RESIDENT_PORTAL_ANCILLARY' ) ] );
	}
}
?>