<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPayments
 * Do not add any new functions to this class.
 */

class CScheduledPayments extends CBaseScheduledPayments {

	public static function fetchUnpostedScheduledPaymentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSubSql = '
			SELECT
				id
			FROM
				scheduled_payments sp
			WHERE
				sp.lease_id IN (
						SELECT
							vsp1.lease_id
						FROM
							view_scheduled_payments vsp1
							JOIN lease_customers lc1 ON ( vsp1.customer_id = lc1.customer_id AND vsp1.cid = lc1.cid AND lc1.customer_type_id IN( ' . CCustomerType::PRIMARY . ',' . CCustomerType::RESPONSIBLE . ' ) )
						WHERE
							vsp1.property_id = ' . ( int ) $intPropertyId . '
							AND vsp1.cid = ' . ( int ) $intCid . '
							AND vsp1.payment_amount_rate IS NOT NULL
							AND ( vsp1.approved_on IS NULL OR vsp1.terms_accepted_on IS NULL )
							AND vsp1.deleted_by IS NULL
							AND vsp1.deleted_on IS NULL
							AND vsp1.paused_by IS NULL
							AND vsp1.paused_on IS NULL )
							AND sp.cid = ' . ( int ) $intCid;

		$strSql = 'SELECT
						DISTINCT ON ( vsp.id )
						vsp.*,
						CASE WHEN pp.id IS NULL THEN 1 ELSE 0 END as is_paused,
						cust.preferred_locale_code AS customer_preferred_locale_code
					FROM
						view_scheduled_payments vsp
						JOIN clients c ON ( vsp.cid = c.id )
						JOIN properties cp ON ( cp.id = vsp.property_id AND cp.cid = vsp.cid )
						LEFT JOIN leases l ON ( vsp.lease_id = l.id AND vsp.cid = l.cid )
						LEFT JOIN lease_customers lc ON ( l.id = lc.lease_id AND vsp.customer_id = lc.customer_id AND l.cid = lc.cid AND vsp.cid = lc.cid )
						LEFT JOIN customers cust ON( vsp.customer_id = cust.id AND vsp.cid = cust.cid )
						LEFT JOIN property_products pp ON( pp.cid = vsp.cid AND pp.ps_product_id = ' . CPsProduct::RESIDENT_PAY . ' AND ( pp.property_id IS NULL OR pp.property_id = vsp.property_id ) )
					WHERE
						c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND cp.is_disabled <> 1
						AND vsp.property_id = ' . ( int ) $intPropertyId . '
						AND vsp.cid = ' . ( int ) $intCid . '
						AND vsp.deleted_by IS NULL
						AND vsp.deleted_on IS NULL
						AND vsp.paused_by IS NULL
						AND vsp.paused_on IS NULL
						AND vsp.terms_accepted_on IS NOT NULL
						AND CASE
								WHEN vsp.payment_type_id IN (' . CPaymentType::ACH . ', ' . CPaymentType::PAD . ' )
								THEN vsp.check_account_type_id IN(' . implode( ',', CCheckAccountType::$c_arrintPublishedCheckAccountTypes ) . ' )
								ELSE 1=1
							END
						AND vsp.payment_start_date <= date_trunc(\'day\', NOW())
						AND ( ( vsp.is_variable_amount = 1 AND vsp.force_reprocess > 0 )
						OR vsp.last_posted_on IS NULL
						OR
							CASE
								WHEN EXTRACT(DAY FROM now()) = ( DATE_PART(\'days\', DATE_TRUNC(\'month\', NOW())  + INTERVAL \'1 MONTH\' - INTERVAL \'1 DAY\' ) )  AND vsp.process_day = ' . CScheduledPayment::LAST_DAY_OF_THE_MONTH . '
							        THEN vsp.last_posted_on <= ( DATE_TRUNC( \'month\', NOW() - INTERVAL \'1 month\' ) + INTERVAL \'1 MONTH\' - INTERVAL \'1 DAY\' )
								WHEN EXTRACT(DAY FROM NOW()) = ( DATE_PART(\'days\', DATE_TRUNC(\'month\', NOW())  + INTERVAL \'1 MONTH\' - INTERVAL \'2 DAY\' ) ) AND vsp.process_day = ' . CScheduledPayment::SECOND_LAST_DAY_OF_THE_MONTH . '
									THEN vsp.last_posted_on <= ( DATE_TRUNC( \'month\', NOW() - INTERVAL \'1 month\' ) + INTERVAL \'1 MONTH\' - INTERVAL \'2 DAY\' )  
								WHEN EXTRACT(DAY FROM NOW()) = ( DATE_PART(\'days\', DATE_TRUNC(\'month\', NOW())  + INTERVAL \'1 MONTH\' - INTERVAL \'3 DAY\' ) ) AND vsp.process_day = ' . CScheduledPayment::THIRD_LAST_DAY_OF_THE_MONTH . '
									THEN vsp.last_posted_on <= ( DATE_TRUNC( \'month\', NOW() - INTERVAL \'1 month\' ) + INTERVAL \'1 MONTH\' - INTERVAL \'3 DAY\' )  
								ELSE vsp.last_posted_on <= ( date_trunc(\'day\', NOW()) - INTERVAL \'1 month\' )
							END )
						AND ( 
								CASE
									WHEN EXTRACT(DAY FROM NOW()) IN ( 
										DATE_PART(\'days\', DATE_TRUNC(\'month\', NOW())  + INTERVAL \'1 MONTH\' - INTERVAL \'1 DAY\' ), 
										DATE_PART(\'days\', DATE_TRUNC(\'month\', NOW())  + INTERVAL \'1 MONTH\' - INTERVAL \'2 DAY\' ),
										DATE_PART(\'days\', DATE_TRUNC(\'month\', NOW())  + INTERVAL \'1 MONTH\' - INTERVAL \'3 DAY\' ) ) 
										AND vsp.process_day IN ( ' . CScheduledPayment::LAST_DAY_OF_THE_MONTH . ',' . CScheduledPayment::SECOND_LAST_DAY_OF_THE_MONTH . ',' . CScheduledPayment::THIRD_LAST_DAY_OF_THE_MONTH . ' )
									THEN
										vsp.payment_start_date + INTERVAL \'1 months\' * 
										(SELECT (DATE_PART(\'year\', CASE WHEN CURRENT_DATE < COALESCE( vsp.payment_end_date, CURRENT_DATE) THEN CURRENT_DATE ELSE COALESCE( vsp.payment_end_date, CURRENT_DATE ) END ) - DATE_PART(\'year\', vsp.payment_start_date)) * 12 +
										(DATE_PART(\'month\', CASE WHEN CURRENT_DATE < COALESCE( vsp.payment_end_date, CURRENT_DATE ) THEN CURRENT_DATE ELSE COALESCE( vsp.payment_end_date, CURRENT_DATE ) END ) - DATE_PART(\'month\', vsp.payment_start_date))
										) + INTERVAL \'5 days\' >= CURRENT_DATE
									ELSE 
									    vsp.payment_start_date + INTERVAL \'1 months\' * ( SELECT EXTRACT(YEAR FROM age) * 12 + EXTRACT( MONTH FROM age ) FROM age( CASE WHEN CURRENT_DATE < COALESCE( vsp.payment_end_date, CURRENT_DATE ) THEN CURRENT_DATE ELSE COALESCE( vsp.payment_end_date, CURRENT_DATE ) END, vsp.payment_start_date ) ) + INTERVAL \'5 days\' >= CURRENT_DATE
								END  
							)
						AND (
								vsp.payment_amount_rate IS NULL OR vsp.id NOT IN ( ' . $strSubSql . ' )
							)';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchViewScheduledPaymentByIdsByCid( $arrintIds, $intCid, $objDatabase, $boolInActive = false ) {
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		if( false == $boolInActive ) {
			$strWhereCondition = ' AND deleted_by IS NULL
									AND deleted_on IS NULL ';
		}

		$strSql = 'SELECT 
					* 
					FROM 
						view_scheduled_payments 
					WHERE 
						id IN (' . implode( ', ', $arrintIds ) . ')
						AND cid = ' . ( int ) $intCid . $strWhereCondition;

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchAllScheduledPaymentsByPaymentTypesByPropertyIdsByCid( $arrintEligiblePaymentTypes, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintEligiblePaymentTypes ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strEligiblePaymentTypes	= implode( ',', $arrintEligiblePaymentTypes );
		$strPropertyIds				= implode( ',', $arrintPropertyIds );

		$strSql			= 'SELECT 
								* 
							FROM 
								active_scheduled_payments( ARRAY[ ' . $strEligiblePaymentTypes . '], ARRAY[ ' . $strPropertyIds . '], ' . ( int ) $intCid . ' ) as result';
		$arrmixResult	= fetchData( $strSql, $objDatabase );
		$arrmixResult	= rekeyArray( 'id', $arrmixResult );

		return $arrmixResult;
	}

	// This function gets all of the scheduled payments that should post in two days from now.

	public static function fetchWarningScheduledPaymentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSubSql = '
					SELECT
						id
					FROM
						scheduled_payments sp
					WHERE
						sp.lease_id IN (
								SELECT
									vsp1.lease_id
								FROM
									view_scheduled_payments vsp1
									JOIN lease_customers lc1 ON ( vsp1.customer_id = lc1.customer_id AND vsp1.cid = lc1.cid AND lc1.customer_type_id IN( ' . CCustomerType::PRIMARY . ',' . CCustomerType::RESPONSIBLE . ' ) )
								WHERE
									vsp1.property_id = ' . ( int ) $intPropertyId . '
									AND vsp1.cid = ' . ( int ) $intCid . '
									AND vsp1.payment_amount_rate IS NOT NULL
									AND vsp1.approved_on IS NULL
									AND vsp1.deleted_by IS NULL
									AND vsp1.deleted_on IS NULL
									AND vsp1.paused_by IS NULL
									AND vsp1.paused_on IS NULL )
									AND sp.cid = ' . ( int ) $intCid;

		$strSql = 'SELECT
						DISTINCT ON ( vsp.id )
						vsp.*
					FROM
						view_scheduled_payments vsp
						JOIN clients c ON ( vsp.cid = c.id )
						LEFT JOIN lease_customers clc ON ( clc.lease_id = vsp.lease_id AND clc.customer_id = vsp.customer_id AND clc.cid = vsp.cid )
						LEFT JOIN lease_processes lp ON ( vsp.cid = lp.cid AND vsp.lease_id = lp.lease_id AND lp.customer_id IS NULL )
					WHERE
						( clc.lease_status_type_id IS NULL OR ( clc.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ' ) AND lp.termination_start_date IS NULL ) )
						AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						AND vsp.property_id = ' . ( int ) $intPropertyId . '
						AND vsp.cid = ' . ( int ) $intCid . '
						AND vsp.deleted_by IS NULL
						AND vsp.deleted_on IS NULL
						AND vsp.payment_start_date <= ( date_trunc(\'day\', NOW()) + INTERVAL \'1 day\' )
						AND ( vsp.payment_end_date IS NULL OR vsp.payment_end_date >= ( date_trunc(\'day\', NOW()) + INTERVAL \'1 day\' ))
						AND ( vsp.last_posted_on IS NULL OR vsp.last_posted_on <= ( date_trunc(\'day\', NOW()) + INTERVAL \'1 day\' - INTERVAL \'1 month\' ) )
						AND ( vsp.payment_end_date IS NULL OR ( COALESCE( vsp.last_posted_on, ( date_trunc(\'day\', NOW()) - INTERVAL \'1 month\' + INTERVAL \'1 day\' )) + INTERVAL \'1 month\' ) <= vsp.payment_end_date )
						AND ( vsp.last_warned_on IS NULL OR vsp.last_warned_on <= ( date_trunc(\'day\', NOW()) + INTERVAL \'1 day\' - INTERVAL \'1 month\' - INTERVAL \'1 day\' ))
						AND CASE
								WHEN date_part( \'day\', vsp.payment_start_date ) > date_part(\'day\', CURRENT_DATE )
								THEN date_trunc(\'day\', NOW()) = ( date( date_part(\'month\', CURRENT_DATE ) || \'/\' || date_part(\'day\', vsp.payment_start_date ) ||\'/\' || date_part(\'year\', CURRENT_DATE ))- INTERVAL \'1 day\' )
								ELSE date_trunc(\'day\', NOW()) = ( date( date_part(\'month\', CURRENT_DATE ) || \'/\' || date_part(\'day\', vsp.payment_start_date ) ||\'/\' || date_part(\'year\', CURRENT_DATE ))  + INTERVAL \'1 month\' - INTERVAL \'1 day\' )
							END
						AND vsp.id NOT IN (

							SELECT
									DISTINCT ON ( vsp.id )
									vsp.id
								FROM
									view_scheduled_payments vsp
									JOIN clients c ON ( vsp.cid = c.id )
									LEFT JOIN lease_customers clc ON ( clc.lease_id = vsp.lease_id AND clc.customer_id = vsp.customer_id AND clc.cid = vsp.cid )
									LEFT JOIN lease_processes lp ON ( vsp.cid = lp.cid AND vsp.lease_id = lp.lease_id AND lp.customer_id IS NULL )
								WHERE
									( clc.lease_status_type_id IS NULL OR ( clc.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ' ) AND lp.termination_start_date IS NULL ) )
									AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . '
									AND vsp.property_id = ' . ( int ) $intPropertyId . '
									AND vsp.cid = ' . ( int ) $intCid . '
									AND vsp.deleted_by IS NULL
									AND vsp.deleted_on IS NULL
									AND vsp.payment_start_date <= date_trunc(\'day\', NOW())
									AND ( vsp.payment_end_date IS NULL OR vsp.payment_end_date >= date_trunc(\'day\', NOW()) )
									AND ( vsp.last_posted_on IS NULL OR vsp.last_posted_on <= ( date_trunc(\'day\', NOW()) - INTERVAL \'1 month\' ) )
									AND ( vsp.payment_end_date IS NULL OR ( COALESCE( vsp.last_posted_on, ( date_trunc(\'day\', NOW()) - INTERVAL \'1 month\' )) + INTERVAL \'1 month\' ) <= vsp.payment_end_date )

							) AND (
									vsp.payment_amount_rate IS NULL OR vsp.id NOT IN ( ' . $strSubSql . ' )
								)';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchCustomScheduledPaymentsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM view_scheduled_payments WHERE customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid . ' AND deleted_on IS NULL';
		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchSimpleScheduledPaymentsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM scheduled_payments WHERE customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid . ' AND deleted_on IS NULL';
		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentByIdByCustomerIdByCid( $intScheduledPaymentId, $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM scheduled_payments WHERE id = ' . ( int ) $intScheduledPaymentId . ' AND customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid . ' AND deleted_on IS NULL';
		return self::fetchScheduledPayment( $strSql, $objDatabase );
	}

	public static function fetchViewScheduledPaymentByIdByCustomerIdByCid( $intId, $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM view_scheduled_payments WHERE id = ' . ( int ) $intId . ' AND customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid . ' AND deleted_by IS NULL AND deleted_on IS NULL';
		return self::fetchScheduledPayment( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentsByCustomerIdsByPropertyIdsByCid( $arrintCustomerIds, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) && false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT * FROM view_scheduled_payments WHERE customer_id in (' . implode( ', ', $arrintCustomerIds ) . ') AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cid = ' . ( int ) $intCid . ' AND deleted_by IS NULL AND deleted_on IS NULL';
		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentsAssociatedToNonIntegratedLeasesPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						sp.*
					FROM
						scheduled_payments sp,
						leases cl
					WHERE
						sp.lease_id = cl.id
						AND sp.cid = cl.cid
						AND cl.remote_primary_key IS NULL
						AND cl.cid::integer = ' . ( int ) $intCid . '::integer
						AND cl.property_id::integer = ' . ( int ) $intPropertyId . '::integer ';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchUncancelledScheduledPaymentsAssociatedToNonIntegratedLeasesPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || true == empty( $arrintPropertyIds ) ) return NULL;

		$strSql = ' SELECT
							sp.*
						FROM
							view_scheduled_payments sp,
							leases cl
						WHERE
							sp.lease_id = cl.id
							AND sp.cid = cl.cid
							AND deleted_by IS NULL
							AND deleted_on IS NULL
							AND cl.remote_primary_key IS NULL
							AND cl.cid::integer = ' . ( int ) $intCid . '::integer
							AND cl.property_id IN( ' . implode( ', ', $arrintPropertyIds ) . ' )';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchUnPausedScheduledPaymentsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						view_scheduled_payments
					WHERE
						paused_by IS NULL
						AND paused_on IS NULL
						AND cid::integer = ' . ( int ) $intCid . '
						AND property_id IN( ' . implode( ', ', $arrintPropertyIds ) . ' )';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentByCompetingScheduledPayment( $objCompetingScheduledPayment, $objDatabase ) {
		if( true == is_null( $objCompetingScheduledPayment->getCustomerId() ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						scheduled_payments
					WHERE
						customer_id = ' . ( int ) $objCompetingScheduledPayment->getCustomerId() . '
						AND cid = ' . ( int ) $objCompetingScheduledPayment->getCid() . '
						AND lease_id = ' . ( int ) $objCompetingScheduledPayment->getLeaseId() . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND id <> ' . ( int ) $objCompetingScheduledPayment->getId() . '
						AND ( payment_end_date IS NULL OR payment_end_date > NOW() ) LIMIT ';

		return self::fetchScheduledPayment( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentsByIdByLeaseIdCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( 'SELECT * FROM view_scheduled_payments WHERE cid = ' . ( int ) $intCid . ' AND lease_id = ' . ( int ) $intLeaseId . ' AND deleted_on IS NULL', $objDatabase );
	}

	public static function fetchAllScheduledPaymentsByIdByLeaseIdCid( $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchScheduledPayments( 'SELECT * FROM view_scheduled_payments WHERE cid = ' . ( int ) $intCid . ' AND lease_id = ' . ( int ) $intLeaseId, $objDatabase );
	}

	public static function fetchCustomReviveableScheduledPaymentsByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'WITH fltsp AS ( 
						        SELECT COUNT(lc2.lease_id) as active_lease_count,
						               sp.id,
						               MAX(lc2.lease_id) AS saving_lease_id,
						               CASE
						                 WHEN MAX(lc.lease_status_type_id) IN (' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . ') THEN MAX(lc.id)
						               END AS lease_customer_id
						        FROM view_scheduled_payments sp
						             JOIN lease_customers lc ON (lc.cid = sp.cid AND lc.customer_id =
						               sp.customer_id AND lc.lease_id = sp.lease_id AND
						               lc.lease_status_type_id IN (' . CLeaseStatusType::CANCELLED . ', ' . CLeaseStatusType::PAST . '))
						             JOIN lease_customers lc2 ON (lc2.cid = sp.cid AND lc2.customer_id =
						               sp.customer_id AND lc2.id <> lc.id AND lc2.lease_id <>
						               sp.lease_id AND lc2.lease_status_type_id = ' . CLeaseStatusType::CURRENT . ')
						        WHERE EXISTS (
						                       SELECT 1
						                       FROM clients
						                       WHERE id = sp.cid AND
						                             company_status_type_id = ' . CCompanyStatusType::CLIENT . '
						              ) AND
						              EXISTS (
						                       SELECT 1
						                       FROM properties
						                       WHERE id = sp.property_id AND
						                             cid = sp.cid AND
						                             is_disabled = 0
						              ) AND
									  sp.cid IN( ' . implode( ',', $arrintCids ) . ' ) AND
						              sp.deleted_on IS NULL AND
						              sp.deleted_by IS NULL AND
						              (sp.last_posted_on IS NULL OR
						              sp.last_posted_on >(Date_trunc(\'day\', Now()) - interval \'2 months\'
						                )) AND
						              (sp.last_posted_on IS NULL OR
						              sp.last_posted_on <=(date_trunc(\'day\', now()) - interval \'1 month\'
						                )) AND
						              sp.payment_start_date <= date_trunc(\'day\', now()) AND
						              (sp.payment_end_date IS NULL OR
						              (COALESCE(sp.last_posted_on, (date_trunc(\'day\', now()) - interval
						                \'1 month\')) + interval \'1 month\') <= sp.payment_end_date) AND
						              (sp.payment_end_date IS NULL OR
						              sp.payment_end_date >= date_trunc(\'day\', now()))
						        GROUP BY sp.id
						        HAVING SUM(CASE
						                     WHEN sp.lease_id = lc2.lease_id AND sp.customer_id =
						                       lc2.customer_id AND sp.cid = lc2.cid AND sp.deleted_on IS
						                       NULL AND sp.deleted_by IS NULL AND (sp.payment_end_date
						                       IS NULL OR sp.payment_end_date >= date_trunc(\'day\', now()
						                       )) THEN 1
						                     ELSE 0
						                   END) = 0 AND COUNT(lc2.lease_id) = 1)
						        SELECT vsp.*,
						               fltsp.lease_customer_id,
						               fltsp.saving_lease_id,
						               fltsp.active_lease_count
						        FROM fltsp
						             JOIN view_scheduled_payments vsp ON vsp.id = fltsp.id';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchOldScheduledPaymentsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						sp.*
					FROM
						scheduled_payments sp
					WHERE
						sp.cid = ' . ( int ) $intCid . '
						AND sp.payment_end_date < ( NOW() - INTERVAL \'1 month\' )
						AND sp.deleted_on IS NULL
					UNION ALL
					SELECT sp_old.*
					FROM scheduled_payments sp_old
					JOIN lease_customers lc ON lc.cid = sp_old.cid AND lc.lease_id = sp_old.lease_id AND lc.customer_id = sp_old.customer_id
					WHERE lc.lease_status_type_id IN( ' . CLeaseStatusType::PAST . ',' . CLeaseStatusType::CANCELLED . ')
					AND sp_old.cid = ' . ( int ) $intCid . '
					AND sp_old.deleted_on is null
					AND COALESCE(sp_old.last_posted_on, sp_old.payment_start_date) <(NOW() - INTERVAL \'65 days\')';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchVariableRecurringPaymentByLeaseIdByDateByCid( $intLeaseId, $strDate, $intCid, $objDatabase ) {

		if( false == valStr( $strDate ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						scheduled_payments
					WHERE
						date_part( \'day\', payment_start_date ) = date_part( \'day\', \'' . $strDate . '\'::date )
					AND( payment_end_date is null or payment_end_date >= \'' . $strDate . '\' )
					AND lease_id = ' . ( int ) $intLeaseId . '
					AND cid = ' . ( int ) $intCid . '
					AND is_variable_amount = 1
					AND deleted_on IS NULL LIMIT 1 ';

		return self::fetchScheduledPayment( $strSql, $objDatabase );
	}

	public static function fetchVariableRecurringPaymentCountByLeaseIdByDateByCid( $intLeaseId, $strDate, $intCid, $objDatabase, $boolIsSplitPayment = false ) {
		if( false == valStr( $strDate ) ) return NULL;
		$strWhere = ' WHERE
					date_part( \'day\', payment_start_date ) = date_part( \'day\', \'' . $strDate . '\'::date )
					AND( payment_end_date is null or payment_end_date >= \'' . $strDate . '\' )
					AND lease_id = ' . ( int ) $intLeaseId . '
					AND cid = ' . ( int ) $intCid . '
					AND is_variable_amount = 1
					AND deleted_on IS NULL';

		if( true == $boolIsSplitPayment ) {
			$strWhere .= ' AND payment_amount_rate IS NULL';
		}

		return self::fetchScheduledPaymentCount( $strWhere, $objDatabase );
	}

	public static function fetchScheduledPaymentByCustomerIdByLeaseIdByPropertyIdByPaymentAmountByCid( $intCustomerId, $intLeaseId, $intPropertyId, $fltPaymentAmount, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						scheduled_payments
					WHERE
						customer_id	= ' . ( int ) $intCustomerId . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND property_id	= ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND payment_amount = ' . ( float ) $fltPaymentAmount . '
						AND payment_start_date	< NOW()
						AND ( payment_end_date IS NULL OR payment_end_date > NOW() )
						AND deleted_on IS NULL';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchSplitPaymentAmountInfoByCustomerIdLeaseIdCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {

		if( true == is_null( $intCustomerId ) || true == is_null( $intLeaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						is_variable_amount,
						payment_amount,
						payment_amount_rate
					FROM
						scheduled_payments
					WHERE
						customer_id	= ' . ( int ) $intCustomerId . '
						AND lease_id = ' . ( int ) $intLeaseId . '
						AND cid = ' . ( int ) $intCid . '
						AND payment_amount_rate IS NOT NULL
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomScheduledPaymentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql	= 'SELECT
						sp.*
					 FROM
						scheduled_payments sp
						LEFT JOIN properties cp ON ( sp.property_id = cp.id AND sp.cid = cp.cid )
					WHERE
						sp.cid = ' . ( int ) $intCid . '
						AND sp.property_id =' . ( int ) $intPropertyId . '
						AND sp.deleted_on IS NULL
					ORDER BY sp.id ';
		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchActiveScheduledPaymentsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql	= 'SELECT
						vsp.*
					 FROM
						view_scheduled_payments vsp
						JOIN properties cp ON ( vsp.property_id = cp.id AND vsp.cid = cp.cid )
						JOIN lease_customers lc ON ( vsp.customer_id = lc.customer_id AND vsp.lease_id = lc.lease_id AND vsp.cid = lc.cid  AND lc.lease_status_type_id not in ( ' . CLeaseStatusType::CANCELLED . ',' . CLeaseStatusType::PAST . ' ) )

					WHERE
						vsp.cid = ' . ( int ) $intCid . '
						AND vsp.property_id =' . ( int ) $intPropertyId . '
						AND vsp.deleted_by IS NULL
						AND vsp.deleted_on IS NULL
						AND ( vsp.payment_end_date IS NULL OR vsp.payment_end_date >= NOW() )';
		return self::fetchScheduledPayments( $strSql, $objDatabase );

	}

	public static function fetchPendingScheduledPaymentsCountByLeaseIdsByCid( $intCId, $arrintLeaseIds, $objClientDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
							count(sp.id), sp.lease_id
						FROM
							scheduled_payments sp
						WHERE
							sp.lease_id IN( \'' . implode( "','", $arrintLeaseIds ) . '\' )
							AND sp.cid = ' . ( int ) $intCId . '
							AND sp.approved_on IS NULL
							AND sp.payment_amount_rate IS NOT NULL
							AND sp.deleted_on IS NULL
						GROUP BY
							sp.lease_id';

		$arrintResponse = fetchData( $strSql, $objClientDatabase );
		$arrintResponse = rekeyArray( 'lease_id', $arrintResponse );
		return $arrintResponse;
	}

	public static function fetchActiveScheduledPaymentsByCharityIdByPropertyIdsByCid( $intCompanyCharityId, $intCid, $objDatabase, $arrintPropertyIds = NULL ) {

		if( true == is_null( $intCid ) || true == is_null( $intCompanyCharityId ) ) {
			return NULL;
		}

		$strSql	= 'SELECT
						sp.*
					FROM
						scheduled_payments sp
						JOIN properties p ON ( sp.property_id = p.id AND sp.cid = p.cid )
						JOIN lease_customers lc ON ( sp.customer_id = lc.customer_id AND sp.lease_id = lc.lease_id AND sp.cid = lc.cid  AND lc.lease_status_type_id not in ( ' . CLeaseStatusType::CANCELLED . ',' . CLeaseStatusType::PAST . ' ) )

					WHERE
						sp.company_charity_id =' . ( int ) $intCompanyCharityId . '
						AND	sp.cid = ' . ( int ) $intCid;
						if( true == valArr( $arrintPropertyIds ) ) {
							$strSql .= ' AND sp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
						}

						$strSql .= ' AND sp.deleted_by IS NULL AND sp.deleted_on IS NULL AND ( sp.payment_end_date IS NULL OR sp.payment_end_date >= NOW() )';

		return self::fetchScheduledPayments( $strSql, $objDatabase );

	}

	public static function fetchScheduledPaymentsByLeaseIdsByCustomerIdByCid( $arrintLeaseIds, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) || true == is_null( $intCustomerId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						scheduled_payments sp
						JOIN clients c ON ( c.id = sp.cid AND sp.cid = ' . ( int ) $intCid . ' )
						JOIN scheduled_payment_details spd ON ( sp.id = spd.scheduled_payment_id AND sp.cid = spd.cid )
					WHERE
						sp.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND sp.customer_id = ' . ( int ) $intCustomerId . '
						AND sp.deleted_on IS NULL
						AND sp.payment_start_date <= NOW() + INTERVAL \'30 days\'
						AND
							CASE
								WHEN sp.payment_end_date IS NOT NULL
								THEN  sp.payment_end_date > NOW()
								ELSE 1=1
							END
					ORDER BY
						sp.payment_start_date;';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentsCountByLeaseIdByCustomerIdByApprovedOnByCid( $intLeaseId, $intCustomerId, $intCid, $objClientDatabase, $boolIsEditRoomate ) {

		$strSqlWhere = '
			WHERE
				lease_id = ' . ( int ) $intLeaseId . '
				AND cid = ' . ( int ) $intCid . '
				AND customer_id = ' . ( int ) $intCustomerId . '
				AND deleted_on IS NULL';

		if( true == $boolIsEditRoomate ) {
			$strSqlWhere .= ' AND approved_on IS NOT NULL';
		}

		return self::fetchScheduledPaymentCount( $strSqlWhere, $objClientDatabase );
	}

	public static function fetchSplitScheduledPaymentsByLeaseIdsByCid( $intCid, $arrintLeaseIds, $objClientDatabase, $boolIsSplitPayment = false ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						sp.*
					FROM
						view_scheduled_payments sp
						JOIN lease_customers lc ON( lc.cid = sp.cid AND lc.lease_id = sp.lease_id AND lc.customer_id = sp.customer_id )
					WHERE
						lc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND sp.scheduled_payment_type_id = ' . CScheduledPaymentType::SPLIT . '
						AND lc.cid = ' . ( int ) $intCid . '
						AND sp.deleted_on IS NULL';

		if( false == $boolIsSplitPayment ) {
			$strSql .= ' AND lc.customer_type_id IN ( ' . CCustomerType::PRIMARY . ', ' . CCustomerType::RESPONSIBLE . ' )';
		}
		return self::fetchScheduledPayments( $strSql, $objClientDatabase, true );
	}

	public static function fetchBiMonthlyScheduledPaymentsByLeaseIdByCid( $intCid, $intLeaseId, $objClientDatabase ) {
		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						scheduled_payments sp
					WHERE
						sp.lease_id = ' . ( int ) $intLeaseId . '
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.scheduled_payment_type_id = ' . CScheduledPaymentType::BIMONTHLY . '
						AND sp.deleted_on IS NULL';

		return self::fetchScheduledPayments( $strSql, $objClientDatabase, true );
	}

	public static function fetchScheduledPaymentsByScheduledPaymentTypeIdByLeaseIdByCid( $intScheduledPaymentTypeId, $intLeaseId, $intCid, $objClientDatabase ) {
		if( false == valId( $intLeaseId ) || false == valId( $intScheduledPaymentTypeId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						scheduled_payments sp
					WHERE
						sp.lease_id = ' . ( int ) $intLeaseId . '
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.scheduled_payment_type_id = ' . $intScheduledPaymentTypeId . '
						AND sp.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchBimonthlyScheduledPaymentsCountByLeaseIdsByCid( $intCid, $intLeaseId, $objClientDatabase ) {
		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						count(id)
					FROM
						scheduled_payments sp
					WHERE
						sp.lease_id = ' . ( int ) $intLeaseId . '
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.scheduled_payment_type_id = ' . CScheduledPaymentType::BIMONTHLY . '
						AND sp.deleted_on IS NULL';

		$arrintResponse = fetchData( $strSql, $objClientDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchViewScheduledPaymentByIdsByCustomerIdByCid( $arrintIds, $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM view_scheduled_payments WHERE id IN (' . implode( ', ', $arrintIds ) . ') AND customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid . ' AND deleted_by IS NULL AND deleted_on IS NULL';
		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchOverlappingRecurringPaymentCountByLeaseIdByDateByCid( $intLeaseId, $strDate, $intCid, $objDatabase ) {

		if( false == valStr( $strDate ) ) return NULL;

		$strWhere = ' WHERE
					payment_start_date <= \'' . $strDate . '\'
					AND( payment_end_date is null or payment_end_date >= \'' . $strDate . '\' )
					AND lease_id = ' . ( int ) $intLeaseId . '
					AND cid = ' . ( int ) $intCid . '
					AND deleted_on IS NULL';

		return self::fetchScheduledPaymentCount( $strWhere, $objDatabase );
	}

	public static function fetchSimpleOverlappingRecurringPaymentDetailsByLeaseIdByDateByCid( $intLeaseId, $strDate, $intCid, $objDatabase ) {

		if( false == valStr( $strDate ) ) return NULL;

		$strSql = 'SELECT
						sp.id,
						c.name_first,
						c.name_last
					FROM
						scheduled_payments sp
						JOIN customers c ON ( sp.customer_id = c.id AND sp.cid = c.cid )
					WHERE
						sp.payment_start_date <= \'' . $strDate . '\'
						AND( sp.payment_end_date is null or sp.payment_end_date >= \'' . $strDate . '\' )
						AND sp.lease_id = ' . ( int ) $intLeaseId . '
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResponsibleScheduledPaymentCreatedByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $strCreatedOn = NULL ) {

		$strCondition = ( $strCreatedOn != NULL ) ? 'AND to_char(sp.created_on, \'MM/DD/YYYY hh24:mi\')::timestamp = to_timestamp(\'' . $strCreatedOn . '\', \'MM/DD/YYYY hh24:mi\')::timestamp ' : '';

		$strSql = 'SELECT
						sp.id,
						sp.customer_id
					FROM
						scheduled_payments sp
					WHERE
						sp.lease_id = ' . ( int ) $intLeaseId . '
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.deleted_by IS NULL
						AND sp.deleted_on IS NULL
						AND sp.approved_on IS NOT NULL ' . $strCondition . '

					ORDER BY
						sp.approved_on
					LIMIT 1';

		$arrintResponse = fetchData( $strSql, $objDatabase );
		return ( ( true == isset( $arrintResponse[0] ) ) ? $arrintResponse[0] : 0 );
	}

	public static function fetchSimpleViewScheduledPaymentsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $intLeaseId = NULL, $arrintPropertyId = NULL ) {

		$strSql = 'SELECT
						*
					FROM
						view_scheduled_payments vsp
					WHERE
						vsp.customer_id = ' . ( int ) $intCustomerId;

		if( true == valId( $intLeaseId ) ) {
			$strSql .= ' AND vsp.lease_id = ' . ( int ) $intLeaseId;
		}
		if( true == valArr( $arrintPropertyId ) ) {
			$strSql .= ' AND vsp.property_id IN ( ' . implode( ',', $arrintPropertyId ) . ' )';
		}
		$strSql .= ' AND vsp.cid = ' . ( int ) $intCid . '
					 AND vsp.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchScheduledPaymentDetailsByIdByCid( $intScheduledPaymentId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM view_scheduled_payments vsp WHERE vsp.id = ' . ( int ) $intScheduledPaymentId . ' AND vsp.cid = ' . ( int ) $intCid . ' AND vsp.deleted_on IS NULL';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveScheduledPaymentsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolCheckFinMoveout = false ) {

		$strSql	= 'SELECT
						vsp.*
					 FROM
						view_scheduled_payments vsp
						JOIN properties cp ON ( vsp.property_id = cp.id AND vsp.cid = cp.cid )
						JOIN lease_customers lc ON ( vsp.customer_id = lc.customer_id AND vsp.lease_id = lc.lease_id AND vsp.cid = lc.cid  AND lc.lease_status_type_id NOT IN ( ' . CLeaseStatusType::CANCELLED . ',' . CLeaseStatusType::PAST . ' ) )
					WHERE
						vsp.cid = ' . ( int ) $intCid . '
						AND vsp.lease_id =' . ( int ) $intLeaseId . '
						AND vsp.deleted_by IS NULL
						AND vsp.deleted_on IS NULL';

		if( true == $boolCheckFinMoveout ) {
			$strSql .= ' AND vsp.payment_end_date IS NULL AND vsp.use_lease_end = false';
		} else {
			$strSql .= ' AND ( vsp.payment_end_date IS NULL OR vsp.payment_end_date >= NOW() )';
		}


		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchVariableCeilingRecurringPaymentOfRoomateCountByCustomerIdByLeaseIdByDateByCid( $intCustomerId, $intLeaseId, $strDate, $intCid, $objDatabase ) {
		if( false == valStr( $strDate ) ) return NULL;
		$strWhere = ' WHERE
					date_part( \'day\', payment_start_date ) = date_part( \'day\', \'' . $strDate . '\'::date )
					AND( payment_end_date is null or payment_end_date >= \'' . $strDate . '\' )
					AND customer_id = ' . ( int ) $intCustomerId . '
					AND lease_id = ' . ( int ) $intLeaseId . '
					AND cid = ' . ( int ) $intCid . '
					AND is_variable_amount = 1
					AND deleted_on IS NULL';

		return self::fetchScheduledPaymentCount( $strWhere, $objDatabase );
	}

	public static function fetchVariableRecurringPaymentOfRoomateCountByCustomerIdByLeaseIdByDateByCid( $intCustomerId, $intLeaseId, $strDate, $intCid, $objDatabase ) {
		if( false == valStr( $strDate ) ) return NULL;
		$strWhere = ' WHERE
					date_part( \'day\', payment_start_date ) = date_part( \'day\', \'' . $strDate . '\'::date )
					AND( payment_end_date is null or payment_end_date >= \'' . $strDate . '\' )
					AND customer_id != ' . ( int ) $intCustomerId . '
					AND ( payment_ceiling_amount IS NULL OR 0 = payment_ceiling_amount )
					AND lease_id = ' . ( int ) $intLeaseId . '
					AND cid = ' . ( int ) $intCid . '
					AND is_variable_amount = 1
					AND deleted_on IS NULL';

		return self::fetchScheduledPaymentCount( $strWhere, $objDatabase );
	}

	/**
	 * @param $intCid
	 * @param $intLeaseId
	 * @param $objDatabase
	 */
	public static function fetchCustomRoommateScheduledPaymentsByLeaseIdByCid( $intCid, $intLeaseId, $objClientDatabase ) {
		$strSql = 'SELECT
						vsp.*
					FROM
						view_scheduled_payments vsp
					WHERE
						vsp.cid = ' . ( int ) $intCid . '
						AND vsp.lease_id = ' . ( int ) $intLeaseId . '
						AND vsp.payment_amount_rate IS NOT NULL
						AND vsp.deleted_on IS NULL';
		return self::fetchScheduledPayments( $strSql, $objClientDatabase );
	}

	public static function fetchBiMonthlyScheduledPaymentsByLeaseIdsByCid( $intCid, $arrintLeaseIds, $objClientDatabase ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						view_scheduled_payments sp
					WHERE
						sp.lease_id IN ( ' . implode( ', ', $arrintLeaseIds ) . ' )
						AND sp.cid = ' . ( int ) $intCid . '
						AND sp.scheduled_payment_type_id = ' . CScheduledPaymentType::BIMONTHLY . '
						AND sp.deleted_on IS NULL';

		return self::fetchScheduledPayments( $strSql, $objClientDatabase, true );
	}

	public static function fetchActiveScheduledPaymentsByLeaseIdsByCustomerIdsByCid( $arrintLeaseIds, $arrintCustomerIds, $intCid, $objDatabase ) {
		$strSql	= 'SELECT
						vsp.*
					 FROM
						view_scheduled_payments vsp
						JOIN properties cp ON ( vsp.property_id = cp.id AND vsp.cid = cp.cid )
						JOIN lease_customers lc ON ( vsp.customer_id = lc.customer_id AND vsp.lease_id = lc.lease_id AND vsp.cid = lc.cid  AND lc.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . ' )
					WHERE
						vsp.cid = ' . ( int ) $intCid . '
						AND vsp.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND vsp.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND vsp.deleted_by IS NULL
						AND vsp.deleted_on IS NULL
						AND ( vsp.payment_end_date IS NULL OR vsp.payment_end_date >= NOW() )';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchPausedScheduledPaymentsByIdByLeaseIdCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {
		$strSql	= 'SELECT 
						* 
				   FROM 
				        view_scheduled_payments 
				   WHERE cid = ' . ( int ) $intCid . '
				   AND customer_id = ' . ( int ) $intCustomerId . '
				   AND lease_id = ' . ( int ) $intLeaseId . '
				   AND deleted_on IS NULL 
				   AND terms_accepted_on IS NULL';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public static function fetchScheduledPaymentsbyCidByPropertyId( $intCid, $intPropertyId, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql	= 'SELECT
						sp.id
					FROM
						scheduled_payments sp
						JOIN scheduled_payment_details spd ON spd.cid = sp.cid AND spd.scheduled_payment_id = sp.id
						JOIN payment_types pt ON pt.id = sp.payment_type_id
						JOIN cached_leases l ON l.cid = sp.cid AND l.id = sp.lease_id
						LEFT JOIN lease_status_types lst ON lst.id = l.lease_status_type_id
						LEFT JOIN company_users cu_d ON cu_d.cid = sp.cid AND cu_d.id = sp.deleted_by
						LEFT JOIN company_users cu_c ON cu_c.cid = sp.cid AND cu_c.id = sp.created_by
						LEFT JOIN LATERAL 
						(
							SELECT
								COUNT ( spt.scheduled_payment_id ) AS scheduled_payments_count
							FROM
								scheduled_payment_transactions spt
							WHERE
								spt.cid = sp.cid
								AND spt.scheduled_payment_id = sp.id
						) spt ON TRUE
					WHERE
						sp.cid = ' . ( int ) $intCid . '
						AND l.occupancy_type_id NOT IN ( ' . implode( ',', COccupancyType::$c_arrintExcludedOccupancyTypes ) . ' )
						AND sp.deleted_on IS NULL
						AND sp.property_id = ' . ( int ) $intPropertyId;

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

	public function fetchCountBiMonthlyScheduledPaymentsByLeaseIdByCid( $intLeaseId, $intCid, $objClientDatabase ) {

		if( false == valId( $intLeaseId ) ) return NULL;

		$strSqlWhere = ' WHERE
		                    lease_id = ' . ( int ) $intLeaseId . '
							AND cid = ' . ( int ) $intCid . '
							AND scheduled_payment_type_id = ' . CScheduledPaymentType::BIMONTHLY . '
							AND deleted_on IS NULL';

		return self::fetchScheduledPaymentCount( $strSqlWhere, $objClientDatabase );
	}

	public static function fetchScheduledPaymentsByCustomerIdByCustomerPaymentAccountIdByCid( $intCustomerId, $intCustomerPaymentAccountId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						scheduled_payments
					WHERE
						customer_id	= ' . ( int ) $intCustomerId . '
						AND customer_payment_account_id = ' . ( int ) $intCustomerPaymentAccountId . '
						AND cid = ' . ( int ) $intCid . '
						AND ( payment_end_date IS NULL OR payment_end_date > NOW() )
						AND deleted_on IS NULL';

		return self::fetchScheduledPayments( $strSql, $objDatabase );
	}

}
?>
