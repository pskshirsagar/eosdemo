<?php

class CAppointmentPreference extends CBaseAppointmentPreference {

	const MONDAY 		= 1;
	const TUESDAY 		= 2;
	const WEDNESDAY 	= 3;
	const THURSDAY		= 4;
	const FRIDAY 		= 5;
	const SATURDAY 		= 6;
	const SUNDAY 		= 7;

	protected $m_boolByAppointmentOnly;
	protected $m_strLunchStartTime;
	protected $m_strLunchEndTime;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDayOfWeek() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvailableStart() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvailableEnd() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxSimultaneousAppointments() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAppointmentPreferenceType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Get Functions
	*
	*/

	public function getByAppointmentOnly() {
		return $this->m_boolByAppointmentOnly;
	}

	public function getLunchStartTime() {
		return $this->m_strLunchStartTime;
	}

	public function getLunchEndTime() {
		return $this->m_strLunchEndTime;
	}

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['by_appointment_only'] ) ) $this->setByAppointmentOnly( CStrings::strToBool( $arrmixValues['by_appointment_only'] ) );
		if( true == isset( $arrmixValues['lunch_start_time'] ) ) $this->setLunchStartTime( $arrmixValues['lunch_start_time'] );
		if( true == isset( $arrmixValues['lunch_end_time'] ) ) $this->setLunchEndTime( $arrmixValues['lunch_end_time'] );

		return;
	}

	public function setByAppointmentOnly( $boolByAppointmentOnly ) {
		$this->m_boolByAppointmentOnly = $boolByAppointmentOnly;
	}

	public function setLunchStartTime( $strLunchStartTime ) {
		$this->m_strLunchStartTime = $strLunchStartTime;
	}

	public function setLunchEndTime( $strLunchEndTime ) {
		$this->m_strLunchEndTime = $strLunchEndTime;
	}

}
?>