<?php

class CRevenueUnitTypeSetting extends CBaseRevenueUnitTypeSetting {

    public function valId() {
        $boolIsValid = true;
        if( true == is_null( $this->getId() ) || false == is_int( $this->getId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Invalid Id' ) );
        }
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        if( true == is_null( $this->getCid() ) || false == is_int( $this->getCid() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Invalid Client Id' ) );
        }
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        if( true == is_null( $this->getPropertyId() ) || false == is_int( $this->getPropertyId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Invalid Property Id' ) );
        }
        return $boolIsValid;
    }

    public function valUnitTypeId() {
        $boolIsValid = true;
        if( true == is_null( $this->getUnitTypeId() ) || false == is_int( $this->getUnitTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_type_id', 'Invalid Unit Type Id' ) );
        }
        return $boolIsValid;
    }

    public function valMinOptimalPrice() {
        $boolIsValid = true;
        if( true == is_nan( $this->getMinOptimalPrice() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_optimal_price', 'Min Optimal Price should be a valid number' ) );
        }
        if( 0 > $this->getMinOptimalPrice() && 99999 < $this->getMinOptimalPrice() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_optimal_price', 'Min Optimal Price should be in between 0 to 99999' ) );
        }
        return $boolIsValid;
    }

    public function valMaxOptimalPrice() {
        $boolIsValid = true;
        if( true == is_nan( $this->getMaxOptimalPrice() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_optimal_price', 'Max Optimal Price should be a valid number' ) );
        }
        if( 0 > $this->getMaxOptimalPrice() && 99999 < $this->getMaxOptimalPrice() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_optimal_price', 'Max Optimal Price should be in between 0 to 99999' ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valUnitTypeId();
            	$boolIsValid &= $this->valMinOptimalPrice();
            	$boolIsValid &= $this->valMaxOptimalPrice();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valUnitTypeId();
            	$boolIsValid &= $this->valMinOptimalPrice();
            	$boolIsValid &= $this->valMaxOptimalPrice();
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>