<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportStatusTypes
 * Do not add any new functions to this class.
 */

class CImportStatusTypes extends CBaseImportStatusTypes {

	public static function fetchImportStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CImportStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchImportStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CImportStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedImportStatusTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						import_status_types
					WHERE
						is_published = 1
					ORDER BY
						name';

		return self::fetchImportStatusTypes( $strSql, $objDatabase );
	}
}
?>