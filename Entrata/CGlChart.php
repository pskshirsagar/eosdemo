<?php

class CGlChart extends CBaseGlChart {

	const PROPERTY_DEFAULT_SYSTEM_CODE 	= 'PROP';
	const COMPANY_DEFAULT_SYSTEM_CODE 	= 'COMPANY';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

    	if( true == is_null( $this->getName() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Gl chart name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPrimary() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsManagerial() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
	       		break;

           	case VALIDATE_DELETE:
            	break;

           	default:
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }
}
?>