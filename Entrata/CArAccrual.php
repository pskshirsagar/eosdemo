<?php

class CArAccrual extends CBaseArAccrual {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArAccrualBatchId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArTransactionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAccruedArTransactionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStartDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEndDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAccrualDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAccrualAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWaivedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWaivedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>