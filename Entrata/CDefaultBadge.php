<?php

class CDefaultBadge extends CBaseDefaultBadge {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultActivityPointId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileHandle() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBaseDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valProDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBaseCriteria() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valProCriteria() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPenalty() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDisplayToAll() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDisplayToCustomer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>