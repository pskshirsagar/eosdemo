<?php

class CRouteRuleType extends CBaseRouteRuleType {

	const OVER_BUDGET                                                = 1;
	const UNDER_BUDGET                                               = 2;
	const TOTAL_GREATER_THAN                                         = 3;
	const TOTAL_LESS_THAN                                            = 4;
	const TOTAL_BETWEEN                                              = 5;
	const SPECIFIC_GL_ACCOUNTS                                       = 6;
	const SPECIFIC_COMPANY_USERS                                     = 7;
	const INVOICE_LINKED_TO_APPROVED_PO                              = 8;
	const INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_GREATER_THAN_AMOUNT  = 9;
	const OVER_BUDGET_BY_PERCENT                                     = 10;
	const UNDER_BUDGET_BY_PERCENT                                    = 11;
	const INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_LESS_THAN_AMOUNT     = 12;
	const SPECIFIC_VENDORS                                           = 13;
	const SPECIFIC_BANK_ACCOUNTS                                     = 14;
	const VENDOR_ACCESS_STATUS                                       = 15;
	const JOURNAL_ENTRIES_POST_MONTH_DIFFER_FROM_CURRENT_MONTH       = 18;
	const FINANCIAL_MOVE_OUT_BALANCE_GREATER_THAN                    = 19;
	const FINANCIAL_MOVE_OUT_BALANCE_LESS_THAN                       = 20;
	const FINANCIAL_MOVE_OUT_BALANCE_BETWEEN                         = 21;
	const FINANCIAL_MOVE_OUT_REFUND_PAYABLE_GREATER_THAN             = 22;
	const FINANCIAL_MOVE_OUT_REFUND_PAYABLE_LESS_THAN                = 23;
	const FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN                  = 24;
	const SPECIFIC_ROUTING_TAGS                                      = 25;
	const SPECIFIC_JOB_CATEGORY                                      = 26;
	const CHANGE_ORDER_TYPE_IS                                       = 27;
	const JOB_CHANGE_ORDER                                           = 28;
	const CONTRACT_CHANGE_ORDER                                      = 29;
	const INCREASE_BUDGET_BY                                         = 30;
	const INCREASE_THE_BUDGET_BY_PERCENT                             = 31;
	const SPECIFIC_JOB_COST_CODE                                     = 32;
	const FINANCIAL_MOVE_OUT_REFUND_IS_ALLOCATED_TO                  = 33;
	const FINANCIAL_MOVE_OUT_STARTED_DAYS_AFTER_MOVE_OUT_DATE        = 34;
	const FINANCIAL_MOVE_OUT_IS                                      = 35;
	const INVOICE_LINKED_TO_APPROVED_PO_NO                           = 36;
	const INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_GREATER_THAN_PERCENT = 37;
	const INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_LESS_THAN_PERCENT    = 38;
	const TOTAL_GREATER_THAN_PERCENT                                 = 39;
	const TOTAL_LESS_THAN_PERCENT                                    = 40;
	const TOTAL_BETWEEN_PERCENT                                      = 41;
	const TOTAL_AMOUNT_GREATER_THAN_PERCENT                          = 42;
	const TOTAL_AMOUNT_LESS_THAN_PERCENT                             = 43;
	const TOTAL_AMOUNT_BETWEEN_PERCENT                               = 44;
	const TOTAL_AMOUNT_GREATER_THAN                                  = 45;
	const TOTAL_AMOUNT_LESS_THAN                                     = 46;
	const TOTAL_AMOUNT_BETWEEN                                       = 47;
	const INCREASE_TOTAL_COST_BY                                     = 48;
	const INCREASE_TOTAL_COST_BY_PERCENT                             = 49;
	const AMOUNT_GREATER_THAN                                        = 50;
	const AMOUNT_LESS_THAN                                           = 51;
	const TRANSACTION_AMOUNT_BETWEEN                                 = 52;
	const TRANSACTION_CHARGE_CODE_IS                                 = 53;
	const INVOICE_PO_TYPE_IS                                         = 56;
	const OVER_JOB_BUDGET_BY                                         = 57;
	const OVER_JOB_BUDGET_BY_PERCENT                                 = 58;
	const OVER_CONTRACT_COST_BY                                      = 59;
	const OVER_CONTRACT_COST_BY_PERCENT                              = 60;
	const OVER_JOB_PHASE_BUDGET_BY                                   = 61;
	const OVER_JOB_PHASE_BUDGET_BY_PERCENT                           = 62;
	const OVER_CONTRACT_PHASE_COST_BY                                = 63;
	const OVER_CONTRACT_PHASE_COST_BY_PERCENT                        = 64;
	const INVOICE_TOTAL_IS_EQUAL_TO_PO_TOTAL						 = 65;
	const REFUND_INVOICE_IS_LINKED_TO_AN_FMO_THAT_WAS_APPROVED_THROUGH_FMO_APPROVAL_ROUTING = 66;
	const REFUND_PAYMENT_WAS_APPROVED_THROUGH_FMO_APPROVAL_ROUTING	 = 67;
	const SPECIFIC_COMPANY_GROUP									 = 68;
	const INCREASES_MONTHLY_BILLING_BY_MORE_THAN_PERCENT			 = 69;
	const DECREASES_MONTHLY_BILLING_BY_MORE_THAN_PERCENT			 = 70;
	const INCREASES_MONTHLY_BILLING_BY_MORE_THAN_AMOUNT				 = 71;
	const DECREASES_MONTHLY_BILLING_BY_MORE_THAN_AMOUNT				 = 72;
	const INCREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_PERCENT		 = 73;
	const DECREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_PERCENT		 = 74;
	const INCREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_AMOUNT		 = 75;
	const DECREASES_TOTAL_CONTRACT_VALUE_BY_MORE_THAN_AMOUNT		 = 76;
	const NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_GREATER_THAN			 = 77;
	const NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_LESS_THAN				 = 78;
	const NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_BETWEEN				 = 79;
	const NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_GREATER_THAN	= 80;
	const NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_LESS_THAN	= 81;
	const TRANSACTION_REQUEST_IS_FOR            	                    = 82;
	const ACCOMMODATION_IS												= 83;

	public static $c_arrintRouteRuleTypeIdsWithoutDetails = [
		CRouteRuleType::OVER_BUDGET,
		CRouteRuleType::UNDER_BUDGET,
		CRouteRuleType::OVER_BUDGET_BY_PERCENT,
		CRouteRuleType::UNDER_BUDGET_BY_PERCENT,
		CRouteRuleType::TOTAL_GREATER_THAN,
		CRouteRuleType::TOTAL_LESS_THAN,
		CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_GREATER_THAN_AMOUNT,
		CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_GREATER_THAN_PERCENT,
		CRouteRuleType::INVOICE_LINKED_TO_APPROVED_PO,
		CRouteRuleType::INVOICE_LINKED_TO_APPROVED_PO_NO,
		CRouteRuleType::TOTAL_BETWEEN,
		CRouteRuleType::JOURNAL_ENTRIES_POST_MONTH_DIFFER_FROM_CURRENT_MONTH,
		CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_GREATER_THAN,
		CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_LESS_THAN,
		CRouteRuleType::FINANCIAL_MOVE_OUT_BALANCE_BETWEEN,
		CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_GREATER_THAN,
		CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_LESS_THAN,
		CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_PAYABLE_BETWEEN,
		CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_IS_ALLOCATED_TO,
		CRouteRuleType::FINANCIAL_MOVE_OUT_STARTED_DAYS_AFTER_MOVE_OUT_DATE,
		CRouteRuleType::INCREASE_BUDGET_BY,
		CRouteRuleType::TOTAL_GREATER_THAN_PERCENT,
		CRouteRuleType::TOTAL_LESS_THAN_PERCENT,
		CRouteRuleType::TOTAL_BETWEEN_PERCENT,
		CRouteRuleType::INCREASE_THE_BUDGET_BY_PERCENT,
		CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_LESS_THAN_AMOUNT,
		CRouteRuleType::INVOICE_TOTAL_TO_PO_TOTAL_VARIANCE_IS_LESS_THAN_PERCENT,
		CRouteRuleType::CHANGE_ORDER_TYPE_IS,
		CRouteRuleType::TOTAL_AMOUNT_GREATER_THAN,
		CRouteRuleType::TOTAL_AMOUNT_LESS_THAN,
		CRouteRuleType::TOTAL_AMOUNT_BETWEEN,
		CRouteRuleType::TOTAL_AMOUNT_GREATER_THAN_PERCENT,
		CRouteRuleType::TOTAL_AMOUNT_LESS_THAN_PERCENT,
		CRouteRuleType::TOTAL_AMOUNT_BETWEEN_PERCENT,
		CRouteRuleType::INCREASE_TOTAL_COST_BY,
		CRouteRuleType::INCREASE_TOTAL_COST_BY_PERCENT,
		CRouteRuleType::JOB_CHANGE_ORDER,
		CRouteRuleType::CONTRACT_CHANGE_ORDER,
		CRouteRuleType::AMOUNT_GREATER_THAN,
		CRouteRuleType::AMOUNT_LESS_THAN,
		CRouteRuleType::TRANSACTION_AMOUNT_BETWEEN,
		CRouteRuleType::OVER_JOB_BUDGET_BY,
		CRouteRuleType::OVER_JOB_BUDGET_BY_PERCENT,
		CRouteRuleType::OVER_CONTRACT_COST_BY,
		CRouteRuleType::OVER_CONTRACT_COST_BY_PERCENT,
		CRouteRuleType::OVER_JOB_PHASE_BUDGET_BY,
		CRouteRuleType::OVER_JOB_PHASE_BUDGET_BY_PERCENT,
		CRouteRuleType::OVER_CONTRACT_PHASE_COST_BY,
		CRouteRuleType::OVER_CONTRACT_PHASE_COST_BY_PERCENT,
		CRouteRuleType::INVOICE_TOTAL_IS_EQUAL_TO_PO_TOTAL,
		CRouteRuleType::REFUND_INVOICE_IS_LINKED_TO_AN_FMO_THAT_WAS_APPROVED_THROUGH_FMO_APPROVAL_ROUTING,
		CRouteRuleType::REFUND_PAYMENT_WAS_APPROVED_THROUGH_FMO_APPROVAL_ROUTING,
		CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_GREATER_THAN,
		CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_LESS_THAN,
		CRouteRuleType::NET_AMOUNT_FOR_CHARGE_CODE_IN_MONTH_BETWEEN,
		CRouteRuleType::NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_GREATER_THAN,
		CRouteRuleType::NUMBER_OF_TRANSACTIONS_FOR_CHARGE_CODES_IN_MONTH_LESS_THAN,
	];

	public static $c_arrintRouteRuleTypeIdsWithDetails = [
		CRouteRuleType::SPECIFIC_GL_ACCOUNTS,
		CRouteRuleType::SPECIFIC_COMPANY_USERS,
		CRouteRuleType::SPECIFIC_COMPANY_GROUP,
		CRouteRuleType::SPECIFIC_VENDORS,
		CRouteRuleType::SPECIFIC_BANK_ACCOUNTS,
		CRouteRuleType::VENDOR_ACCESS_STATUS,
		CRouteRuleType::SPECIFIC_ROUTING_TAGS,
		CRouteRuleType::SPECIFIC_JOB_CATEGORY,
		CRouteRuleType::SPECIFIC_JOB_COST_CODE,
		CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_IS_ALLOCATED_TO,
		CRouteRuleType::FINANCIAL_MOVE_OUT_IS,
		CRouteRuleType::TRANSACTION_CHARGE_CODE_IS,
		CRouteRuleType::INVOICE_PO_TYPE_IS,
		CRouteRuleType::TRANSACTION_REQUEST_IS_FOR,
		CRouteRuleType::ACCOMMODATION_IS
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>