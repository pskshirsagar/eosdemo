<?php
use Psi\Eos\Entrata\CCompanyMediaFiles;
use Psi\Eos\Entrata\CWebsites;
use Psi\Eos\Entrata\CUnitTypes;
use Psi\Eos\Entrata\CSpecials;
use Psi\Eos\Entrata\CPropertyEmailRules;
use Psi\Eos\Entrata\CQuickResponses;
use Psi\Eos\Entrata\CFiles;
use Psi\Eos\Entrata\CFileAssociations;
use Psi\Eos\Entrata\CPropertyFloorplans;
use Psi\Eos\Entrata\CPropertyBuildings;
use Psi\Eos\Entrata\CPropertyUnits;
use Psi\Eos\Entrata\CUnitSpaces;
use Psi\Eos\Entrata\CPropertyPreferences;

use Psi\Libraries\ExternalPrinceWrapper\CPrinceFactory;
use Psi\Libraries\UtilDates\CDates;

require_once( PATH_APPLICATION_ENTRATA . 'Library/' . 'EventScheduleTask/CUpdateEventScheduleTaskExecutionLibrary.class.php' );

/**
 * @method array getPreferredUnitSpaceDetails()
 * @method void setPreferredUnitSpaceDetails( array $arrmixPreferredUnitSpaceDetails )
 */
class CApplication extends CBaseApplication {

	private $m_arrintIntentToApplyWithinMonthsIntervals;

	protected $m_objLease;
	protected $m_objEvent;
	protected $m_objWebsite;
	protected $m_objProperty;
	protected $m_objApplicant;
	protected $m_objUnitSpace;
	protected $m_objLeadSource;
	protected $m_objPropertyUnit;
	protected $m_objInsurancePolicy;
	protected $m_objDocumentManager;
	protected $m_objObjectStorageGateway;
	protected $m_objMoneyGramAccount;
	protected $m_objPrimaryApplicant;
	protected $m_objClient;
	protected $m_objPropertyFloorplan;
	protected $m_objApplicationDuplicate;
	protected $m_objApplicantApplication;
	protected $m_objPrimaryApplicantApplication;
	protected $m_objLeaseInterval;
	protected $m_objPropertyBuilding;
    protected $m_objLeaseTerm;

	protected $m_arrobjFiles;
	protected $m_arrobjApplicants;
	protected $m_arrobjRenewalOffers;
	protected $m_arrobjLeaseDocuments;
	protected $m_arrobjArTransactions;
	protected $m_arrobjFileMergeValues;
	protected $m_arrobjScheduledCharges;
	protected $m_arrobjApplicationSteps;
	protected $m_arrobjApplicantApplications;
	protected $m_arrobjCurrentApplicationSteps;
	protected $m_arrobjAddOnLeaseAssociations;
	protected $m_arrobjCurrentRwxApplicationSteps;
	protected $m_arrobjPropertyApplicationPreferences;

	protected $m_arrintCustomerTypeIds;
	protected $m_arrintDeletedApplicantIds;
	protected $m_arrintCustomerRelationshipIds;
	protected $m_arrmixRequiredParametes;
	protected $m_arrmixCustomerPhoneNUmbers;

	protected $m_intLeadSourceId;
	protected $m_intPolicyId;
	protected $m_intLeaseTerm;
	protected $m_intIsSendText;
	protected $m_intUnitNumber;
	protected $m_intEventTypeId;
	protected $m_intApplicantId;
	protected $m_intIsTextViewed;
	protected $m_intEventResultId;
	protected $m_intOldPropertyId;
	protected $m_intLastContactDays;
	protected $m_intLeaseDocumentId;
	protected $m_intCrmDayDifference;
	protected $m_intApplicationFormId;
	protected $m_intApplicationStepId;
	protected $m_intCurrentApplicationStepId;
	protected $m_intParentLeadSourceId;
	protected $m_intPrimaryLeadSourceId;
	protected $m_intCustomerTypeId;
	protected $m_intFileMergeValueCount;
	protected $m_intPropertyFloorplanId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intLateFeeFormulaId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intLeaseIntervalTypeId;
	protected $m_intTermMonth;
	protected $m_intShowButtonForPayment;
	protected $m_intApplicationDocumentId;
	protected $m_intApplicantApplicationId;
	protected $m_intApplicationDuplicateId;
	protected $m_intOldCompanyApplicationId;
	protected $m_intMainApplicationComplete;
	protected $m_intPrimaryPhoneNumberTypeId;
	protected $m_intOldApplicationStageId;
	protected $m_intOldApplicationStatusId;
	protected $m_intTransmissionResponseTypeId;
	protected $m_intAvailabilityAlertRequestsId;
	protected $m_intAppliedQuoteId;
	protected $m_intUnavailableUnitSearchNumber;
	protected $m_boolIsIndividualLeaseProgression;
	protected $m_intPropertyTransmissionVendorId;
	protected $m_intShowButtonToCompleteApplication;
	protected $m_intIsSyncApplicationAndLease;
	protected $m_intTotalSigned;
	protected $m_intTotalSigning;
	protected $m_intPaymentCaptured;
	protected $m_intApplicationStageStatusId;
	protected $m_intLockQuotedRent;
	protected $m_intPreviousQuoteId;
	protected $m_intChatId;
	protected $m_intCustomerId;
	protected $m_intPhoneNumberTypeId;

	protected $m_strAddedOn;
	protected $m_strNameLast;
	protected $m_strNameFirst;
	protected $m_strEventNote;
	protected $m_strOfferSentOn;
	protected $m_strPhoneNumber;
	protected $m_strPropertyName;
	protected $m_strEmailAddress;
	protected $m_strLeaseExpireOn;
	protected $m_strEventDatetime;
	protected $m_strTransferredOn;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strAdditionalInfo;
	protected $m_strVerificationKey;
	protected $m_strContactDatetime;
	protected $m_strLeaseGeneratedOn;
	protected $m_strApplicationStatus;
	protected $m_strOldRenewalEndDate;
	protected $m_strPrimaryPhoneNumber;
	protected $m_strApplicationStepName;
	protected $m_strLeasingAgentLastName;
	protected $m_strLeasingAgentFirstName;
	protected $m_strLeaseDocumentDeactivateOn;
	protected $m_strAvailabilityAlertRequestsReminderEmail;
	protected $m_strAvailabilityAlertRequestsReminderCellNumber;
	protected $m_strFloorplanName;
	protected $m_strBuildingName;
	protected $m_strUnitNumberCache;
	protected $m_strApplicationNotes;
	protected $m_strPrimaryApplicantName;
	protected $m_strEventSource;
	protected $m_strMoveInDate;
	protected $m_strPreferredLocaleCode;
	protected $m_strNameLastMatronymic;
	protected $m_strEsignSignatureRefusalReason;

	protected $m_strScreeningDecisionReasonTypes;

	protected $m_fltDesiredMaxRentAmount;

	protected $m_boolInsertLeaseRecords;
	protected $m_boolEditLease;
	protected $m_boolCancelLease;
	protected $m_boolUpdateLease;
	protected $m_boolIsResponsive;
	protected $m_boolIsExportEvent;
	protected $m_boolIsReleaseUnit;
	protected $m_boolIsLeaseEsigned;
	protected $m_boolHasValidPayment;
	protected $m_boolIsActiveRenewal;
	protected $m_boolIsContactTypeSms;
	protected $m_boolIsValidateCallId;
	protected $m_boolIsContactTypeChat;
	protected $m_boolIsRentRateRequired;
	protected $m_boolIsGuestCardDeduped;
	protected $m_boolInsertMessageQueue;
	protected $m_boolIsTransferringIn;
	protected $m_boolLeasePartiallyEsigned;
	protected $m_boolIsShowIntegrationError;
	protected $m_boolIsForcedGuestCardIntegration;
	protected $m_boolLeaseTermAndDatesAlreadyApplied;
	protected $m_boolIsQuoteGenerated;
	protected $m_boolIsImmediateMoveIn;
	protected $m_boolStudentPropertyApplication;
	protected $m_boolHasPets;
	protected $m_boolIsInsurancePolicyExists;
	protected $m_boolIsDefaultLeaseTermSet;
	protected $m_boolIsAutoLeaseApprove;

	protected $m_arrstrMergeFields;

	protected $m_intLeadScore;

	protected $m_fltOptimalMonthlyRentBase;
	protected $m_fltOptimalMonthlyRentTotal;
	protected $m_fltAdvertisedMonthlyRentBase;
	protected $m_fltAdvertisedMonthlyRentTotal;
	protected $m_fltExecutedMonthlyRentBase;
	protected $m_fltExecutedMonthlyRentTotal;

	const OLD_APPLICATION 						= 1;   // old application layout
	const NEW_APPLICATION 						= 2;   // New application layout
	const HISTORICAL_APPLICATION_DISPLAY_DAYS	= 180; // by deafult 180 days for the Application historical days
	const LOCK_RENT_AMOUNT 						= 1;   // For setting lock on rent
	const LOCK_QUOTES_RENT						= 2;   // For locking rent on Apply quotes
	const MAX_ALLOWED_INFANTS					= 6;
	const MAX_ALLOWED_MONTHS_FOR_OCCUPANCY_AGE_LIMIT = 215;   // For Non Responsible Occupants under Occupancy Age limit setting

	// This constants is used for application intent to apply column
	const PAST_MONTH = 0;
	const WITHIN_1_MONTH = 1;
	const WITHIN_1_3_MONTH = 3;
	const WITHIN_4_12_MONTH = 12;
	const WITHIN_ANY_TIME_PERIODS = 9999;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getIntentToApplyWithinMonthsIntervals() {
		if( isset( $this->m_arrintIntentToApplyWithinMonthsIntervals ) )
			return $this->m_arrintIntentToApplyWithinMonthsIntervals;

		$this->m_arrintIntentToApplyWithinMonthsIntervals = [
			self::PAST_MONTH    => __( 'Past' ),
			self::WITHIN_1_MONTH    => __( 'Within 1 mo.' ),
			self::WITHIN_1_3_MONTH  => __( '1-3 mos' ),
			self::WITHIN_4_12_MONTH => __( '4-12 mos' ),
			self::WITHIN_ANY_TIME_PERIODS => __( '1 year +' )
		];

		return $this->m_arrintIntentToApplyWithinMonthsIntervals;
	}

	public function getOccupierTypes() {
		$arrstrOccupiers = [
			'Single'                 => __( 'Single' ),
			'Couple'                 => __( 'Couple' ),
			'Family (with children)' => __( 'Family (with children)' ),
			'Sharers'                => __( 'Sharers' ),
			'Other Relations'        => __( 'Other Relations' ),
		];

		return $arrstrOccupiers;
	}

	public function __construct() {
		parent::__construct();

		// Setting force guest card integration flag as false by default.
		$this->m_boolIsForcedGuestCardIntegration 	= false;
		$this->m_boolIsShowIntegrationError			= false;
		$this->m_boolIsExportEvent					= true;
		$this->m_boolIsResponsive					= false;
		$this->m_intIsSyncApplicationAndLease		= true;
		$this->m_boolInsertLeaseRecords				= false;
		$this->m_boolIsInsurancePolicyExists        = false;

		return;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getPhoneNumberTypeId() {
		return $this->m_intPhoneNumberTypeId;
	}

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParametes;
	}

	public function getRemotePrimaryKey() {

		if( ( CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() && CApplicationStage::PRE_APPLICATION == $this->m_intApplicationStageId ) || true == $this->getIsForcedGuestCardIntegration() ) {
			return $this->m_strGuestRemotePrimaryKey;
		} elseif( CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() ) {
			return $this->m_strAppRemotePrimaryKey;
		} else {
			return NULL;
		}
	}

	public function getIsSyncApplicationAndLease() {
		return $this->m_intIsSyncApplicationAndLease;
	}

	public function getIsActiveRenewal() {
		return $this->m_boolIsActiveRenewal;
	}

	public function getLeadSourceId() {
		if( false == is_null( $this->m_intLeadSourceId ) ) return $this->m_intLeadSourceId;

		return $this->m_intConvertingLeadSourceId;
	}

	public function getParentLeadSourceId() {
		return $this->m_intParentLeadSourceId;
	}

	public function getPrimaryLeadSourceId() {
		return $this->m_intPrimaryLeadSourceId;
	}

	public function getArTransactions() {
		return $this->m_arrobjArTransactions;
	}

	public function getPropertyApplicationPreferences() {
		return $this->m_arrobjPropertyApplicationPreferences;
	}

	public function getApplicationSteps() {
		return $this->m_arrobjApplicationSteps;
	}

	public function getCurrentApplicationSteps() {
		return $this->m_arrobjCurrentApplicationSteps;
	}

	public function getCurrentRwxApplicationSteps() {
		return $this->m_arrobjCurrentRwxApplicationSteps;
	}

	public function getPropertyFloorplan() {
		return $this->m_objPropertyFloorplan;
	}

	public function getPropertyUnit() {
		return $this->m_objPropertyUnit;
	}

	public function getUnitSpace() {
		return $this->m_objUnitSpace;
	}

	public function getApplicantApplication() {
		return $this->m_objApplicantApplication;
	}

	public function getPrimaryApplicantApplication() {
		return $this->m_objPrimaryApplicantApplication;
	}

	public function getProperty() {
		return $this->m_objProperty;
	}

	public function getNameFirst( $objDatabase = NULL ) {
		if( true == isset( $this->m_strNameFirst ) ) return $this->m_strNameFirst;
		if( false == isset( $this->m_objPrimaryApplicant ) ) {
			$this->m_objPrimaryApplicant = $this->getOrFetchPrimaryApplicant( $objDatabase );
		}
		if( true == isset( $this->m_objPrimaryApplicant ) ) {
		    $this->m_strNameFirst = $this->m_objPrimaryApplicant->getNameFirst();
        }
        return $this->m_strNameFirst;
	}

	public function getNameLast( $objDatabase = NULL ) {
		if( true == isset( $this->m_strNameLast ) ) return $this->m_strNameLast;
		if( false == isset( $this->m_objPrimaryApplicant ) ) {
			$this->m_objPrimaryApplicant = $this->getOrFetchPrimaryApplicant( $objDatabase );
		}
        if( true == isset( $this->m_objPrimaryApplicant ) ) {
            $this->m_strNameLast = $this->m_objPrimaryApplicant->getNameLast();
        }
        return $this->m_strNameLast;
	}

	public function getApplicationStepName() {
		return $this->m_strApplicationStepName;
	}

	public function getApplicationStatus() {
		return $this->m_strApplicationStatus;
	}

	public function getApplicationStepId() {
		return $this->m_intApplicationStepId;
	}

	public function getCurrentApplicationStepId() {
		return $this->m_intCurrentApplicationStepId;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getEmailAddress( $objDatabase = NULL ) {
		if( true == isset( $this->m_strEmailAddress ) ) return $this->m_strEmailAddress;
		if( false == isset( $this->m_objPrimaryApplicant ) ) {
			$this->m_objPrimaryApplicant = $this->getOrFetchPrimaryApplicant( $objDatabase );
		}
        if( true == isset( $this->m_objPrimaryApplicant ) ) {
            $this->m_strEmailAddress = $this->m_objPrimaryApplicant->getEmailAddress();
        }
        return $this->m_strEmailAddress;
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function getUnitNumber() {
		return $this->m_intUnitNumber;
	}

	public function getApplicationDuplicateId() {
		return $this->m_intApplicationDuplicateId;
	}

	public function getOldApplicationStageId() {
		return $this->m_intOldApplicationStageId;
	}

	public function getOldApplicationStatusId() {
		return $this->m_intOldApplicationStatusId;
	}

	public function getContactDatetime() {
		return $this->m_strContactDatetime;
	}

	public function getLastContactDays() {
		return $this->m_intLastContactDays;
	}

	public function getIsTextViewed() {
		return $this->m_intIsTextViewed;
	}

	public function getIsSendText() {
		return $this->m_intIsSendText;
	}

	public function getFileMergeValueCount() {
		return $this->m_intFileMergeValueCount;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function getAddedOn() {
		return $this->m_strAddedOn;
	}

	public function getEventDatetime() {
		return $this->m_strEventDatetime;
	}

	public function getFloorplanName() {
		return $this->m_strFloorplanName;
	}

	public function getBuildingName( $objDatabase = NULL ) {
		if( true == isset( $this->m_strBuildingName ) ) return $this->m_strBuildingName;
		if( false == isset( $this->m_objPropertyBuilding ) ) {
			$this->m_objPropertyBuilding = $this->getOrFetchPropertyBuilding( $objDatabase );
		}
        if( true == isset( $this->m_objPropertyBuilding ) ) {
            $this->m_strBuildingName = $this->m_objPropertyBuilding->getBuildingName();
        }
        return $this->m_strBuildingName;
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function getPropertyTransmissionVendorId() {
		return $this->m_intPropertyTransmissionVendorId;
	}

	public function getApplicationDocumentId() {
		return $this->m_intApplicationDocumentId;
	}

	public function getApplicationFormId() {
		return $this->m_intApplicationFormId;
	}

	public function getTotalSigned() {
		return $this->m_intTotalSigned;
	}

	public function getTotalSigning() {
		return $this->m_intTotalSigning;
	}

	public function getAdditionalInfo() {
		return $this->m_strAdditionalInfo;
	}

	public function getLease() {
		return $this->m_objLease;
	}

	public function getInsurancePolicy() {
		return $this->m_objInsurancePolicy;
	}

	public function getHasValidPayment() {
		return $this->m_boolHasValidPayment;
	}

	public function getShowButtonToCompleteApplication() {
		return $this->m_intShowButtonToCompleteApplication;
	}

	public function getShowButtonForPayment() {
		return $this->m_intShowButtonForPayment;
	}

	public function getLeadSource() {
		return $this->m_objLeadSource;
	}

	public function getVerificationKey() {
		return $this->m_strVerificationKey;
	}

	public function getApplicant() {
		return $this->m_objApplicant;
	}

	public function getApplicationDuplicate() {
		return $this->m_objApplicationDuplicate;
	}

	public function getEvent() {
		return $this->m_objEvent;
	}

	public function getDesiredMaxRentAmount() {
		return $this->m_fltDesiredMaxRentAmount;
	}

	public function getIsRentRateRequired() {
		return $this->m_boolIsRentRateRequired;
	}

	public function getIsGuestCardDeduped() {
		return $this->m_boolIsGuestCardDeduped;
	}

	public function getIsContactTypeSms() {
		return $this->m_boolIsContactTypeSms;
	}

	public function getIsContactTypeChat() {
		return $this->m_boolIsContactTypeChat;
	}

	public function getInsertLeaseRecords() {
		return $this->m_boolInsertLeaseRecords;
	}

	public function getAvailabilityAlertRequestsReminderEmail() {
		return $this->m_strAvailabilityAlertRequestsReminderEmail;
	}

	public function getAvailabilityAlertRequestsId() {
		return $this->m_intAvailabilityAlertRequestsId;
	}

	public function getAppliedQuoteId() {
		return $this->m_intAppliedQuoteId;
	}

	public function getAvailabilityAlertRequestsReminderCellNumber() {
		return $this->m_strAvailabilityAlertRequestsReminderCellNumber;
	}

	public function getUnavailableUnitSearchNumber() {
		return $this->m_intUnavailableUnitSearchNumber;
	}

	public function getIsExportEvent() {
		return $this->m_boolIsExportEvent;
	}

	public function getEditLease() {
		return $this->m_boolEditLease;
	}

	public function getUpdateLease() {
		return $this->m_boolUpdateLease;
	}

	public function getCancelLease() {
		return $this->m_boolCancelLease;
	}

	public function getIsResponsive() {
		return $this->m_boolIsResponsive;
	}

	public function getIsTransferredIn() {
		return $this->m_boolIsTransferringIn;
	}

	public function getTransferredOn() {
		return $this->m_strTransferredOn;
	}

	public function getIsIndividualLeaseProgression() {
		return $this->m_boolIsIndividualLeaseProgression;
	}

	public function getLeaseTerm( $objDatabase = NULL ) {
        return $this->getTermMonth( $objDatabase );
	}

	public function getIsImmediateMoveIn() {
		return $this->m_boolIsImmediateMoveIn;
	}

	public function getLeaseStartDate( $objDatabase = NULL ) {
		if( true == isset( $this->m_strLeaseStartDate ) ) return $this->m_strLeaseStartDate;
		if( false == isset( $this->m_objLeaseInterval ) ) {
			$this->m_objLeaseInterval = $this->getOrFetchLeaseInterval( $objDatabase );
		}
        if( true == isset( $this->m_objLeaseInterval ) ) {
            $this->m_strLeaseStartDate = $this->m_objLeaseInterval->getLeaseStartDate();
        }
        return $this->m_strLeaseStartDate;
	}

	public function getLeaseEndDate( $objDatabase = NULL ) {
		if( true == isset( $this->m_strLeaseEndDate ) ) return $this->m_strLeaseEndDate;
		if( false == isset( $this->m_objLeaseInterval ) ) {
			$this->m_objLeaseInterval = $this->getOrFetchLeaseInterval( $objDatabase );
		}
        if( true == isset( $this->m_objLeaseInterval ) ) {
            $this->m_strLeaseEndDate = $this->m_objLeaseInterval->getLeaseEndDate();
        }
        return $this->m_strLeaseEndDate;
	}

	public function getLeaseIntervalTypeId( $objDatabase = NULL ) {
		if( true == isset( $this->m_intLeaseIntervalTypeId ) ) return $this->m_intLeaseIntervalTypeId;
		if( false == isset( $this->m_objLeaseInterval ) ) {
			$this->m_objLeaseInterval = $this->getOrFetchLeaseInterval( $objDatabase );
		}
        if( true == isset( $this->m_objLeaseInterval ) ) {
            $this->m_intLeaseIntervalTypeId = $this->m_objLeaseInterval->getLeaseIntervalTypeId();
        }
        return $this->m_intLeaseIntervalTypeId;
	}

	public function getLeaseTermId( $objDatabase = NULL ) {
		if( true == isset( $this->m_intLeaseTermId ) ) return $this->m_intLeaseTermId;
		if( false == isset( $this->m_objLeaseInterval ) ) {
			$this->m_objLeaseInterval = $this->getOrFetchLeaseInterval( $objDatabase );
		}
        if( true == isset( $this->m_objLeaseInterval ) ) {
            $this->m_intLeaseTermId = $this->m_objLeaseInterval->getLeaseTermId();
        }
        return $this->m_intLeaseTermId;
	}

	public function getLeaseStartWindowId( $objDatabase = NULL ) {
		if( true == isset( $this->m_intLeaseStartWindowId ) ) return $this->m_intLeaseStartWindowId;
		if( false == isset( $this->m_objLeaseInterval ) ) {
			$this->m_objLeaseInterval = $this->getOrFetchLeaseInterval( $objDatabase );
		}
        if( true == isset( $this->m_objLeaseInterval ) ) {
            $this->m_intLeaseStartWindowId = $this->m_objLeaseInterval->getLeaseStartWindowId();
        }
        return $this->m_intLeaseStartWindowId;
	}

	public function getTermMonth( $objDatabase = NULL ) {
        if( true == isset( $this->m_intTermMonth ) ) return $this->m_intTermMonth;
        if( false == isset( $this->m_objLeaseTerm ) ) {
            $this->m_objLeaseTerm = $this->getOrFetchLeaseTerm( $objDatabase );
        }
        if( true == isset( $this->m_objLeaseTerm ) ) {
            $this->m_intTermMonth = $this->m_objLeaseTerm->getTermMonth();
        }
        return $this->m_intTermMonth;
	}

	public function getLateFeeFormulaId() {
		return $this->m_intLateFeeFormulaId;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getOfferSentOn() {
		return $this->m_strOfferSentOn;
	}

	public function getOldRenewalEndDate() {
		return $this->m_strOldRenewalEndDate;
	}

	public function getOldCompanyApplicationId() {
		return $this->m_intOldCompanyApplicationId;
	}

	public function getApplicantApplications() {
		return $this->m_arrobjApplicantApplications;
	}

	public function getAddOnLeaseAssociations() {
		return $this->m_arrobjAddOnLeaseAssociations;
	}

	public function getApplicants() {
		return $this->m_arrobjApplicants;
	}

	public function getFiles() {
		return $this->m_arrobjFiles;
	}

	public function getLeaseDocuments() {
		return $this->m_arrobjLeaseDocuments;
	}

	public function getLeaseDocumentId() {
		return $this->m_intLeaseDocumentId;
	}

	public function getIsLeaseEsigned() {
		return $this->m_boolIsLeaseEsigned;
	}

	public function getIsLeasePartiallyEsigned() {
		return $this->m_boolLeasePartiallyEsigned;
	}

	public function getLeaseDocumentDeactivateOn() {
		return $this->m_strLeaseDocumentDeactivateOn;
	}

	public function getLeaseExpireOn() {
		return $this->m_strLeaseExpireOn;
	}

	public function getPhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId, $strMobileNumber, $strWorkNumber ) {


		$strPhoneNumber = NULL;

		switch( $intPhoneNumberTypeId ) {
			case CPhoneNumberType::HOME:
				$strPhoneNumber = $this->m_strPhoneNumber;
				break;

			case CPhoneNumberType::OFFICE:
				$strPhoneNumber = $strWorkNumber;
				break;

			case CPhoneNumberType::MOBILE:
				$strPhoneNumber = $strMobileNumber;
				break;

			default:
				$strPhoneNumber = ( 0 < strlen( trim( $strMobileNumber ) ) ) ? $strMobileNumber : $strWorkNumber;
				break;
		}

		return $strPhoneNumber;
	}

	public function getLockQuotedRent() {
		return $this->m_intLockQuotedRent;
	}

	public function getPrimaryPhoneNumber( $objDatabase = NULL ) {
		if( true == isset( $this->m_strPrimaryPhoneNumber ) ) return $this->m_strPrimaryPhoneNumber;
		if( false == isset( $this->m_objPrimaryApplicant ) ) {
			$this->m_objPrimaryApplicant = $this->getOrFetchPrimaryApplicant( $objDatabase );
		}
        if( true == isset( $this->m_objPrimaryApplicant ) ) {
			$objCustomerPhoneNumber = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomerPhoneNumberByCustomerIdByCid( $this->m_objPrimaryApplicant->getCustomerId(), $this->getCid(), $this->m_objDatabase, $boolIsPrimary = true );
			if( true == valObj( $objCustomerPhoneNumber, CCustomerPhoneNumber::class ) ) {
				$this->m_strPrimaryPhoneNumber = $objCustomerPhoneNumber->getPhoneNumber();
				$this->m_intPrimaryPhoneNumberTypeId = $objCustomerPhoneNumber->getPhoneNumberTypeId();
			}
        }
        return $this->m_strPrimaryPhoneNumber;
	}

	public function getPropertyName( $objDatabase = NULL ) {
		if( true == isset( $this->m_strPropertyName ) ) return $this->m_strPropertyName;
		if( false == isset( $this->m_objProperty ) ) {
			$this->m_objProperty = $this->getOrFetchProperty( $objDatabase );
		}
        if( true == isset( $this->m_objProperty ) ) {
            $this->m_strPropertyName = $this->m_objProperty->getPropertyName();
        }
        return $this->m_strPropertyName;
	}

	public function getPrimaryPhoneNumberTypeId() {
		return $this->m_intPrimaryPhoneNumberTypeId;
	}

	public function getPrimaryApplicant() {
		return $this->m_objPrimaryApplicant;
	}

	public function getScheduledCharges() {
		return $this->m_arrobjScheduledCharges;
	}

	public function getRenewalOffers() {
		return $this->m_arrobjRenewalOffers;
	}

	public function getWebsite() {
		return $this->m_objWebsite;
	}

	public function getIsReleaseUnit() {
		return $this->m_boolIsReleaseUnit;
	}

	public function getIsForcedGuestCardIntegration() {
		return $this->m_boolIsForcedGuestCardIntegration;
	}

	public function getIsShowIntegrationError() {
		return $this->m_boolIsShowIntegrationError;
	}

	public function getEventNote() {
		return $this->m_strEventNote;
	}

	public function getPolicyId() {
		return $this->m_intPolicyId;
	}

	public function getInsertMessageQueue() {
		return $this->m_boolInsertMessageQueue;
	}

	public function getIsTransmissionConditionsSatisfied( $arrobjTransmissionConditions ) {
		if( false == valArr( $arrobjTransmissionConditions ) )
			return false;

		$boolIsSatisfied 					= false;
		$boolIsGroup1Satisfied 				= false;
		$boolIsGroup2Satisfied 				= false;
		$intConditionGroup1Count 			= 0;
		$intSatisfiedConditionGroup1Count 	= 0;
		$intConditionGroup2Count 			= 0;
		$intSatisfiedConditionGroup2Count 	= 0;

		// Condition Group 0 => All, 1 => Group1, 2 => Group2( Satisfying Conditions )
		$arrobjTransmissionConditions = rekeyObjects( 'TransmissionConditionTypeId', $arrobjTransmissionConditions );

		// Group 1 Conditions( Verify Identity and Certified Funds )
		if( true == array_key_exists( CTransmissionConditionType::VERIFY_IDENTITY, $arrobjTransmissionConditions ) ) {
			$intConditionGroup1Count ++;

			$objTransmissionCondition = getArrayElementByKey( CTransmissionConditionType::VERIFY_IDENTITY, $arrobjTransmissionConditions );

			if( true == valObj( $objTransmissionCondition, 'CTransmissionCondition', 'SatisfiedBy' ) ) {
				$intSatisfiedConditionGroup1Count ++;
			}
		}

		if( true == array_key_exists( CTransmissionConditionType::REQUIRE_CERTIFIED_FUNDS, $arrobjTransmissionConditions ) ) {
			$intConditionGroup1Count ++;

			$objTransmissionCondition = getArrayElementByKey( CTransmissionConditionType::REQUIRE_CERTIFIED_FUNDS, $arrobjTransmissionConditions );

			if( true == valObj( $objTransmissionCondition, 'CTransmissionCondition', 'SatisfiedBy' ) ) {
				$intSatisfiedConditionGroup1Count ++;
			}
		}

		$boolIsGroup1Satisfied = ( $intConditionGroup1Count == $intSatisfiedConditionGroup1Count ) ? true : false;

		// for group 2 one of the condition should be satisfied
		if( true == array_key_exists( CTransmissionConditionType::INCREASE_DEPOSIT, $arrobjTransmissionConditions ) ) {
			$intConditionGroup2Count ++;

			$objTransmissionCondition = getArrayElementByKey( CTransmissionConditionType::INCREASE_DEPOSIT, $arrobjTransmissionConditions );

			if( true == valObj( $objTransmissionCondition, 'CTransmissionCondition', 'SatisfiedBy' ) ) {
				$intSatisfiedConditionGroup2Count ++;
			}
		}

		if( true == array_key_exists( CTransmissionConditionType::INCREASE_RENT, $arrobjTransmissionConditions ) ) {
			$intConditionGroup2Count ++;

			$objTransmissionCondition = getArrayElementByKey( CTransmissionConditionType::INCREASE_RENT, $arrobjTransmissionConditions );

			if( true == valObj( $objTransmissionCondition, 'CTransmissionCondition', 'SatisfiedBy' ) ) {
				$intSatisfiedConditionGroup2Count ++;
			}
		}

		if( true == array_key_exists( CTransmissionConditionType::INCREASE_RENT_2, $arrobjTransmissionConditions ) ) {
			$intConditionGroup2Count ++;

			$objTransmissionCondition = getArrayElementByKey( CTransmissionConditionType::INCREASE_RENT_2, $arrobjTransmissionConditions );

			if( true == valObj( $objTransmissionCondition, 'CTransmissionCondition', 'SatisfiedBy' ) ) {
				$intSatisfiedConditionGroup2Count ++;
			}
		}

		if( true == array_key_exists( CTransmissionConditionType::INCREASE_DEPOSIT_2, $arrobjTransmissionConditions ) ) {
			$intConditionGroup2Count ++;

			$objTransmissionCondition = getArrayElementByKey( CTransmissionConditionType::INCREASE_DEPOSIT_2, $arrobjTransmissionConditions );

			if( true == valObj( $objTransmissionCondition, 'CTransmissionCondition', 'SatisfiedBy' ) ) {
				$intSatisfiedConditionGroup2Count ++;
			}
		}

		if( true == array_key_exists( CTransmissionConditionType::CUSTOM_SCREENING_CONDITION, $arrobjTransmissionConditions ) ) {
			$intConditionGroup2Count ++;

			$objTransmissionCondition = getArrayElementByKey( CTransmissionConditionType::CUSTOM_SCREENING_CONDITION, $arrobjTransmissionConditions );

			if( true == valObj( $objTransmissionCondition, 'CTransmissionCondition', 'SatisfiedBy' ) ) {
				$intSatisfiedConditionGroup2Count ++;
			}
		}

		if( true == array_key_exists( CTransmissionConditionType::REQUIRE_GUARANTOR, $arrobjTransmissionConditions ) ) {
			$intConditionGroup2Count ++;

			$objTransmissionCondition = getArrayElementByKey( CTransmissionConditionType::REQUIRE_GUARANTOR, $arrobjTransmissionConditions );

			if( true == valObj( $objTransmissionCondition, 'CTransmissionCondition', 'SatisfiedBy' ) ) {
				$intSatisfiedConditionGroup2Count ++;
			}
		}

		$boolIsGroup2Satisfied = ( $intConditionGroup2Count == 0 || 1 == $intSatisfiedConditionGroup2Count ) ? true : false;

		return $boolIsSatisfied = $boolIsGroup1Satisfied & $boolIsGroup2Satisfied;
	}

	public function getIsValidateCallId() {
		return $this->m_boolIsValidateCallId;
	}

	public function getFullLeasingAgentName() {

		$strFullAgentName = $this->getLeasingAgentFirstName();
		$strFullAgentName .= ( true == valStr( $this->getLeasingAgentLastName() ) ) ? ' ' . $this->getLeasingAgentLastName() : '';

		return $strFullAgentName;
	}

	public function getLeasingAgentFirstName() {
		return $this->m_strLeasingAgentFirstName;
	}

	public function getLeasingAgentLastName() {
		return $this->m_strLeasingAgentLastName;
	}

	public function getEventResultId() {
		return $this->m_intEventResultId;
	}

	public function getTransmissionResponseTypeId() {
		return $this->m_intTransmissionResponseTypeId;
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function getCustomerTypeIds() {
		return $this->m_arrintCustomerTypeIds;
	}

	public function getDeletedApplicantIds() {
		return $this->m_arrintDeletedApplicantIds;
	}

	public function getCustomerRelationshipIds() {
		return $this->m_arrintCustomerRelationshipIds;
	}

	public function getCrmDayDifference() {
		return $this->m_intCrmDayDifference;
	}

	public function getUnitNumberCache( $objDatabase = NULL ) {
		if( true == isset( $this->m_strUnitNumberCache ) ) return $this->m_strUnitNumberCache;
		if( false == isset( $this->m_objUnitSpace ) ) {
			$this->m_objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );
		}
        if( true == isset( $this->m_objUnitSpace ) ) {
            $this->m_strUnitNumberCache = $this->m_objUnitSpace->getUnitNumberCache();
        }
        return $this->m_strUnitNumberCache;
	}

	public function getApplicationNotes( $objDatabase = NULL ) {

		$arrobjEvents = [];

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			$objDatabase = $this->m_objDatabase;
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$arrobjEvents = ( array ) \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByPropertyIdByLeaseIntervalIdByEventTypeIdsByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), [ CEventType::NOTES ], $this->getCid(), $objDatabase );
		}

		$objEvent = ( true == valArr( $arrobjEvents ) ) ? current( $arrobjEvents ) : NULL;

		if( true == valObj( $objEvent, 'CEvent' ) ) {
			$this->m_strApplicationNotes = trim( $objEvent->getNotes() );
		}

		return $this->m_strApplicationNotes;
	}

	public function getLeaseTermAndDatesAlreadyApplied() {
		return $this->m_boolLeaseTermAndDatesAlreadyApplied;
	}

	public function getStudentPropertyApplication() {
		return $this->m_boolStudentPropertyApplication;
	}

	public function getMergeFields() {
		return $this->m_arrstrMergeFields;
	}

	public function getPaymentCaptured() {
		return $this->m_intPaymentCaptured;
	}

	public function getApplicationStageStatusId() {
		return $this->m_intApplicationStageStatusId;
	}

	public function getScreeningDecisionReasonType() {
		return $this->m_strScreeningDecisionReasonTypes;
	}

	public function getScreeningConditionUrl() {
		return $this->m_strScreeningConditionUrl;
	}

	public function getMergeFieldByKey( $strKey ) {
		if( true == array_key_exists( $strKey, $this->m_arrstrMergeFields ) ) {
			return $this->m_arrstrMergeFields[$strKey];
		}

		return NULL;
	}

	public function getPrimaryApplicantName() {
		return $this->m_strPrimaryApplicantName;
	}

	public function getIsInsurancePolicyExists() {
		return $this->m_boolIsInsurancePolicyExists;
	}

	public function loadDocumentManager( $objDatabase, $intCompanyUserId ) {

		if( true == valObj( $this->m_objDocumentManager, 'IDocumentManager' ) ) {
			return true;
		}

		if( true == is_null( $this->getCid() ) || false == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to initialize document manager. Invalid client or database object.' ), NULL ) );
			return false;
		}

		$this->m_objDocumentManager = CDocumentManagerFactory::createDocumentManager( CDocumentManagerFactory::DOCUMENT_MANAGER_SECURE_FILE_SYSTEM, [ $this->getCid(), $intCompanyUserId, NULL, $objDatabase, NULL ] );

		return true;
	}

	public function loadObjectStorageGateway() {

		if( true == valObj( $this->m_objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			return true;
		}

		$this->m_objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );

		return true;
	}

	public function getHasPets() {
		return $this->m_boolHasPets;
	}

	public function getOccupancyTypeId() {
		return ( true == isset( $this->m_intOccupancyTypeId ) ) ? $this->m_intOccupancyTypeId : COccupancyType::CONVENTIONAL;
	}

	public function getLeadScore() {
		return $this->m_intLeadScore;
	}

	public function getPreviousQuoteId() {
		return $this->m_intPreviousQuoteId;
	}

	public function getOptimalMonthlyRentBase() {
		return $this->m_fltOptimalMonthlyRentBase;
	}

	public function getOptimalMonthlyRentTotal() {
		return $this->m_fltOptimalMonthlyRentTotal;
	}

	public function getAdvertisedMonthlyRentBase() {
		return $this->m_fltAdvertisedMonthlyRentBase;
	}

	public function getAdvertisedMonthlyRentTotal() {
		return $this->m_fltAdvertisedMonthlyRentTotal;
	}

	public function getExecutedMonthlyRentBase() {
		return $this->m_fltExecutedMonthlyRentBase;
	}

	public function getExecutedMonthlyRentTotal() {
		return $this->m_fltExecutedMonthlyRentTotal;
	}

	public function getEventSource() {
		return $this->m_strEventSource;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getPreferredLocaleCode() {
		return $this->m_strPreferredLocaleCode;
	}

	public function getIsAutoLeaseApprove() {
		return $this->m_boolIsAutoLeaseApprove;
	}

	public function getLeaseInterval() {
		return $this->m_objLeaseInterval;
	}

	public function getChatId() {
		return $this->m_intChatId;
	}

	public function getCustomerPhoneNumbers() {
		return $this->m_arrmixCustomerPhoneNUmbers;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setPhoneNumberTypeId( $intPhoneNumberTypeId ) {
		$this->m_intPhoneNumberTypeId = $intPhoneNumberTypeId;
	}

	public function setRequiredParameters( $arrmixParameters ) {
		$this->m_arrmixRequiredParametes = $arrmixParameters;
	}

	public function setIsActiveRenewal( $boolIsActiveRenewal ) {
		$this->m_boolIsActiveRenewal = $boolIsActiveRenewal;
	}

	public function setLockQuotedRent( $intLockQuotedRent ) {
		$this->m_intLockQuotedRent = $intLockQuotedRent;
	}

	public function setIsSyncApplicationAndLease( $intIsSyncApplicationAndLease ) {
		$this->m_intIsSyncApplicationAndLease = $intIsSyncApplicationAndLease;
	}

	// This function is used to set lease start date, end date and lease term
	// with or without proration settings

	public function setLeadSourceId( $intLeadSourceId ) {

		$this->setConvertingLeadSourceId( $intLeadSourceId );
		if( true == is_null( $this->m_intOriginatingLeadSourceId ) ) {
			$this->setOriginatingLeadSourceId( $intLeadSourceId );
		}
	}

	public function setParentLeadSourceId( $intParentLeadSourceId ) {
		$this->m_intParentLeadSourceId = $intParentLeadSourceId;
	}

	public function setPrimaryLeadSourceId( $intPrimaryLeadSourceId ) {
 		$this->m_intPrimaryLeadSourceId = CStrings::strToIntDef( $intPrimaryLeadSourceId, NULL, true );
	}

	public function setStudentLeaseTermMonth() {
		if( true == valId( $this->getLeaseStartWindowId() ) ) {
			$objLeaseTerm = new CLeaseTerm();
			$objLeaseTerm->setStartDate( $this->getLeaseStartDate() );
			$objLeaseTerm->setEndDate( $this->getLeaseEndDate() );
			$this->setTermMonth( round( $objLeaseTerm->calculateTermMonth() ) );
		}
	}

	public function setLeaseTermAndDates( $arrobjPropertyPreferences, $objDatabase = NULL, $boolIsForOTR = NULL,
	                                      $boolIsFromApplyQuotes = false, $boolSetLeaseTerm = true,
	                                      $boolEditOccupant = false, $boolIsFromUpdateGuestCard = false ) {

		if( true == $this->m_boolLeaseTermAndDatesAlreadyApplied && $boolIsFromApplyQuotes == false ) return;

		if( COccupancyType::HOSPITALITY == $this->getOccupancyTypeId() ) {
			$this->setFlexibleLeaseTermAndDates( $objDatabase );
			return;
		}
		if( true == $this->isGroupContractApplication() ) {
			$this->m_boolLeaseTermAndDatesAlreadyApplied = true;
			return;
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->getOrFetchProperty( $objDatabase );
		}

		if( false == valArr( $arrobjPropertyPreferences ) )
			$arrobjPropertyPreferences = [];

		$strMoveInDate = ( false == is_null( $this->getLeaseStartDate() ) ) ? $this->getLeaseStartDate() : date( 'm/d/Y' );

		if( CLeaseIntervalType::RENEWAL != $this->getLeaseIntervalTypeId() && false == is_null( $boolIsForOTR ) ) {
			if( false == array_key_exists( 'PRORATE_OTR_RENT', $arrobjPropertyPreferences ) ) {

				if( true == array_key_exists( 'LEASE_START_DATE', $arrobjPropertyPreferences ) && 'MID' == $arrobjPropertyPreferences['LEASE_START_DATE']->getValue() ) {
					$strMoveInDate = ( false == is_null( $this->getLeaseStartDate() ) ) ? $this->getLeaseStartDate() : date( 'm/d/Y' );
				} elseif( true == array_key_exists( 'LEASE_START_DATE', $arrobjPropertyPreferences ) && 'FOM' == $arrobjPropertyPreferences['LEASE_START_DATE']->getValue() ) {
					$strMoveInDate = date( 'm/1/Y', strtotime( $strMoveInDate ) );
				}
			}
		}

		$this->m_boolIsDefaultLeaseTermSet = true;
		if( 0 < $this->getTermMonth() ) {
			$intLeaseTerm = $this->getTermMonth();
			$this->m_boolIsDefaultLeaseTermSet = false;
		} elseif( true == valObj( $this->m_objProperty, 'CProperty' ) && true == valObj( $objDatabase, 'CDatabase' ) ) {
			if( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId() ) {
				$intLeaseTerm = $this->m_objProperty->getDefaultLeaseTerm( $objDatabase, false, true );
			} else {
				$intLeaseTerm = $this->m_objProperty->getDefaultLeaseTerm( $objDatabase, true, false, $this->getOccupancyTypeId() );
			}
		} else {
			$intLeaseTerm = 12;
		}

		$boolCalculateLeaseEndDate = false == $boolIsFromUpdateGuestCard || ( true == $boolIsFromUpdateGuestCard && true == ( strtotime( $this->getLeaseEndDate() ) > strtotime( $this->getLeaseStartDate() ) ) );

		if( CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() && true == valStr( $this->getApplicationStartedOn() ) ) {
			$boolCalculateLeaseEndDate = false;
		}

		if( ( 0 == ( int ) $this->getTermMonth() && false == is_null( $this->getLeaseEndDate() ) && true == $boolCalculateLeaseEndDate ) || ( CLeaseIntervalType::TRANSFER == $this->getLeaseIntervalTypeId() && true == $boolEditOccupant ) ) {
			$strLeaseEndDate = $this->getLeaseEndDate();
		} elseif( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId()
					&& true == valObj( $objDatabase, 'CDatabase' )
					&& true == $this->isStudentTierRenewal( $objDatabase )
					&& 0 != ( int ) $this->getTermMonth()
					&& false == is_null( $this->getLeaseEndDate() ) ) {
			$strLeaseEndDate = $this->getLeaseEndDate();
		} else {

			$strMoveInDate 	= date( 'm/d/Y', strtotime( $strMoveInDate ) );
			$objMoveInDate 	= new DateTime( $strMoveInDate );
			$intYear 		= $objMoveInDate->format( 'Y' );
			$intMonth 		= $objMoveInDate->format( 'n' );
			$intDay 		= $objMoveInDate->format( 'd' );
			$intYear 		+= floor( $intLeaseTerm / 12 );
			$intMonth 		+= ( $intLeaseTerm % 12 );

			if( $intMonth > 12 ) {
				$intYear ++;
				$intMonth = ( 0 != ( $intMonth % 12 ) ) ? ( $intMonth % 12 ) : 12;
			}
			if( false == checkdate( $intMonth, $intDay, $intYear ) ) {
				$objLeaseEndDate = DateTime::createFromFormat( 'Y-n-d', $intYear . '-' . $intMonth . '-1' );
				$objLeaseEndDate->modify( 'last day of' );
				$objLeaseEndDate = $objLeaseEndDate->format( 'm/d/Y' );
				$strLeaseEndDate = date( 'm/d/Y', strtotime( $objLeaseEndDate ) );
			} else {
				$objLeaseEndDate = DateTime::createFromFormat( 'Y-n-d', $intYear . '-' . $intMonth . '-' . $intDay );
				$objLeaseEndDate = $objLeaseEndDate->format( 'm/d/Y' );
				$strLeaseEndDate = date( 'm/d/Y', strtotime( '-1 day', strtotime( $objLeaseEndDate ) ) );
			}
		}

		$objPropertyPreference = getArrayElementByKey( 'ROUNDING_LEASE_TERM', $arrobjPropertyPreferences );
		$boolAllowRoundingPreferences = true;
		if( CLeaseIntervalType::TRANSFER == $this->getLeaseIntervalTypeId() ) {
			$objOldLease            = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByTransferLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
			$arrobjPropertyGroups   = ( array ) \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchAllCrossPropertyTransferPropertyGroupsByPropertyIdByCid( $this->m_objProperty->getId(), $this->getCid(), $objDatabase );

			if( false == array_key_exists( 'CREATE_TRANSFER_WITH_ORIGINAL_LEASE_END_DATE', $arrobjPropertyPreferences ) && true === valObj( $objOldLease, 'CLease' ) && CLeaseIntervalType::MONTH_TO_MONTH != $objOldLease->getLeaseIntervalTypeId()
				&& true === valObj( $this->m_objProperty, 'CProperty' ) && ( true === $this->m_objProperty->getHasOccupancyType( COccupancyType::CONVENTIONAL ) ) && false == valArr( $arrobjPropertyGroups ) ) {
				$boolAllowRoundingPreferences = false;
			}
		}

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == $boolAllowRoundingPreferences ) {
			switch( $objPropertyPreference->getValue() ) {

				case 'EOM':
					$objRoundToEOMPropertyPreference 	= getArrayElementByKey( 'ROUND_TO_PREV_EOM_IF_DATE_ON', $arrobjPropertyPreferences );
					$intMonthValue						= 1;

					if( true == valObj( $objRoundToEOMPropertyPreference, 'CPropertyPreference' ) && 0 < $objRoundToEOMPropertyPreference->getValue() && ( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId() || CLeaseIntervalType::TRANSFER == $this->getLeaseIntervalTypeId() || true == array_key_exists( 'PRORATE_OTR_RENT', $arrobjPropertyPreferences ) ) && date( 'd', strtotime( $strMoveInDate ) ) <= $objRoundToEOMPropertyPreference->getValue() ) {

						$strLastDayOfMonth = date( 'm/d/Y', strtotime( 'last day of 0 month', strtotime( $strLeaseEndDate ) ) );
						if( $strLeaseEndDate != $strLastDayOfMonth ) {
							$intMonthValue = -1;
						}
					}

					$strLeaseEndDate = CDates::createService()->getLastDayOfMonth( date( 'm/d/Y', strtotime( $strLeaseEndDate ) ), $intMonthValue, 'm/d/Y' );
					break;

				case 'Prev EOM':
					$strLastDayOfMonth = date( 'm/d/Y', strtotime( 'last day of 0 month', strtotime( $strLeaseEndDate ) ) );
					if( $strLeaseEndDate != $strLastDayOfMonth ) {
						$strLeaseEndDate = date( 'm/d/Y', strtotime( 'last day of -1 month', strtotime( $strLeaseEndDate ) ) );
					}
					break;

				default:

			}
		}

		$this->setLeaseStartDate( $strMoveInDate );
		$this->setLeaseEndDate( $strLeaseEndDate );

		if( true === valObj( $objDatabase, 'CDatabase' ) && true === valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == $boolAllowRoundingPreferences ) {
			CApplicationUtils::resetLeaseEndDateForBlackoutDates( $this, $objDatabase, $arrobjPropertyPreferences );
		}

		$objLeaseTerm = ( true == valObj( $objDatabase, 'CDatabase' ) ) ? CLeaseTerms::fetchLeaseTermByIdByCid( $this->getLeaseTermId(), $this->getCid(), $objDatabase ) : NULL;
		if( true == valObj( $objLeaseTerm, 'CLeaseTerm' ) && 0 < $objLeaseTerm->getTermMonth() ) {
			$this->setLeaseTerm( $objLeaseTerm->getTermMonth() );
		} elseif( true == $boolSetLeaseTerm ) {
			$this->setLeaseTerm( $intLeaseTerm );
		}

		$this->m_boolLeaseTermAndDatesAlreadyApplied = true;
	}

	public function setFlexibleLeaseTermAndDates( $objDatabase = NULL ) {

		if( true == $this->m_boolLeaseTermAndDatesAlreadyApplied ) {
			return;
		}

		$strToday          = date( 'm/d/Y' );
		$strLeaseStartDate = ( false == valStr( $this->getLeaseStartDate() ) || strtotime( $this->getLeaseStartDate() ) < strtotime( $strToday ) ) ? date( 'm/d/Y' ) : $this->getLeaseStartDate();
		$this->setLeaseStartDate( NULL );
		$strLeaseEndDate = ( true == valStr( $this->getLeaseEndDate() ) ) ? $this->getLeaseEndDate() : NULL;
		$this->setLeaseEndDate( NULL );

		$objAvailableFlexibleWindow = CLeaseStartWindows::fetchActiveFlexibleLeaseStartWindowsByPropertyIdByCidByLeaseStartDate( $this->getPropertyId(), $this->getCid(), $strLeaseStartDate, $objDatabase );
		if( true == valObj( $objAvailableFlexibleWindow, 'CLeaseStartWindow' ) ) {
			$this->setLeaseStartDate( $strLeaseStartDate );
			if( true == valStr( $strLeaseEndDate ) ) {
				$objDate1          = new DateTime( $strLeaseStartDate );
				$objDate2          = new DateTime( $strLeaseEndDate );
				$intDaysDifference = $objDate2->diff( $objDate1 )->format( '%a' );
				if( $intDaysDifference >= $objAvailableFlexibleWindow->getMinDays() && $intDaysDifference <= $objAvailableFlexibleWindow->getMaxDays() ) {
					$this->setLeaseEndDate( $strLeaseEndDate );
				} else {
					$objDate         = new DateTime( $strLeaseStartDate );
					$strLeaseEndDate = $objDate->add( new DateInterval( 'P' . $objAvailableFlexibleWindow->getMinDays() . 'D' ) )->format( 'm/d/Y' );
					$this->setLeaseEndDate( $strLeaseEndDate );
				}
			} else {
				$objDate         = new DateTime( $strLeaseStartDate );
				$strLeaseEndDate = $objDate->add( new DateInterval( 'P' . $objAvailableFlexibleWindow->getMinDays() . 'D' ) )->format( 'm/d/Y' );
				$this->setLeaseEndDate( $strLeaseEndDate );
			}
		} else {
			$arrobjPropertyFlexibleWindows = ( array ) CLeaseStartWindows::fetchActiveFlexibleLeaseStartWindowsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
			foreach( $arrobjPropertyFlexibleWindows as $intKey => $objPropertyFlexibleWindow ) {
				$arrstrFlexibleWindows[$intKey]['start_date'] = $objPropertyFlexibleWindow->getStartDate();
				$arrstrFlexibleWindows[$intKey]['min_days']   = $objPropertyFlexibleWindow->getMinDays();
			}
			if( true == valArr( $arrstrFlexibleWindows ) ) {
				foreach( $arrstrFlexibleWindows as $strFlexibleWindow ) {
					if( strtotime( $strFlexibleWindow['start_date'] ) > strtotime( $strLeaseStartDate ) ) {
						$this->setLeaseStartDate( $strFlexibleWindow['start_date'] );
						$objDate         = new DateTime( $strFlexibleWindow['start_date'] );
						$strLeaseEndDate = $objDate->add( new DateInterval( 'P' . $strFlexibleWindow['min_days'] . 'D' ) )->format( 'm/d/Y' );
						$this->setLeaseEndDate( $strLeaseEndDate );
						break;
					} else {
						$this->setLeaseStartDate( $strToday );
						$this->setLeaseEndDate( NULL );
					}
				}
			} else {
				$this->setLeaseStartDate( $strToday );
				$this->setLeaseEndDate( NULL );
			}
		}
		CApplicationUtils::setFlexibleLeaseTermData( $this, $objDatabase );
		$this->m_boolLeaseTermAndDatesAlreadyApplied = true;
	}

	public function setLeaseTerm( $intLeaseTerm ) {
		$this->m_intLeaseTerm = CStrings::strToIntDef( $intLeaseTerm, NULL, false );
        $this->setTermMonth( $this->m_intLeaseTerm );
	}

	public function setIsImmediateMoveIn( $boolIsImmediateMoveIn ) {
		$this->m_boolIsImmediateMoveIn = CStrings::strToBool( $boolIsImmediateMoveIn );
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->m_strLeaseStartDate = CStrings::strTrimDef( $strLeaseStartDate, - 1, NULL, true );
	}

	public function setOfferSentOn( $strOfferSentOn ) {
		$this->m_strOfferSentOn = $strOfferSentOn;
	}

	public function setOldRenewalEndDate( $strOldRenewalEndDate ) {
		$this->m_strOldRenewalEndDate = CStrings::strTrimDef( $strOldRenewalEndDate, - 1, NULL, true );
	}

	public function loadApplicantApplicationsGroupByLeaseGeneratedOn( $objDatabase, $boolIncludeDeletedAA = false ) {
		$arrobjApplicantApplications = ( array ) \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchSimpleApplicantApplicationsByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase, $boolIncludeDeletedAA );

		if( false == valArr( $arrobjApplicantApplications ) ) return NULL;

		$arrobjApplicantApplicationsGroupedByLeaseGeneratedOn = NULL;

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( true == is_null( $objApplicantApplication->getLeaseGeneratedOn() ) ) continue;

			$arrobjApplicantApplicationsGroupedByLeaseGeneratedOn[strtotime( date( 'm/d/Y H:i:s', strtotime( $objApplicantApplication->getLeaseGeneratedOn() ) ) )][$objApplicantApplication->getId()] = $objApplicantApplication;
		}

		return $arrobjApplicantApplicationsGroupedByLeaseGeneratedOn;
	}

	public function setArTransactions( $arrobjArTransactions ) {
		$this->m_arrobjArTransactions = $arrobjArTransactions;
	}

	public function setLeadSource( $objLeadSource ) {
		$this->m_objLeadSource = $objLeadSource;
	}

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setInsurancePolicy( $objInsurancePolicy ) {
		$this->m_objInsurancePolicy = $objInsurancePolicy;
	}

	public function setHasValidPayment( $boolHasValidPayment ) {
		$this->m_boolHasValidPayment = $boolHasValidPayment;
	}

	public function setShowButtonToCompleteApplication( $intShowButtonToCompleteApplication ) {
		$this->m_intShowButtonToCompleteApplication = $intShowButtonToCompleteApplication;
	}

	public function setShowButtonForPayment( $intShowButtonForPayment ) {
		$this->m_intShowButtonForPayment = $intShowButtonForPayment;
	}

	public function setPropertyApplicationPreferences( $arrobjPropertyApplicationPreferences ) {
		$this->m_arrobjPropertyApplicationPreferences = $arrobjPropertyApplicationPreferences;
	}

	public function setApplicationSteps( $arrobjApplicationSteps ) {
		$this->m_arrobjApplicationSteps = $arrobjApplicationSteps;
	}

	public function setCurrentApplicationSteps( $arrobjCurrentApplicationSteps ) {
		$this->m_arrobjCurrentApplicationSteps = $arrobjCurrentApplicationSteps;
	}

	public function setPropertyFloorplan( $objPropertyFloorplan ) {
		$this->m_objPropertyFloorplan = $objPropertyFloorplan;
	}

	public function setPropertyUnit( $objPropertyUnit ) {
		$this->m_objPropertyUnit = $objPropertyUnit;
	}

	public function setUnitSpace( $objUnitSpace ) {
		$this->m_objUnitSpace = $objUnitSpace;
	}

	public function setApplicantApplication( $objApplicantApplication ) {
		$this->m_objApplicantApplication = $objApplicantApplication;
	}

	public function setPrimaryApplicantApplication( $objPrimaryApplicantApplication ) {
		$this->m_objPrimaryApplicantApplication = $objPrimaryApplicantApplication;
	}

	public function setProperty( $objProperty ) {
		$this->m_objProperty = $objProperty;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setApplicationStepName( $strApplicationStepName ) {
		$this->m_strApplicationStepName = $strApplicationStepName;
	}

	public function setApplicationStatus( $strApplicationStatus ) {
		$this->m_strApplicationStatus = $strApplicationStatus;
	}

	public function setCurrentApplicationStepId( $intCurrentApplicationStepId ) {
		$this->m_intCurrentApplicationStepId = $intCurrentApplicationStepId;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setUnitNumber( $intUnitNumber ) {
		$this->m_intUnitNumber = $intUnitNumber;
	}

	public function setApplicationDuplicateId( $intApplicationDuplicateId ) {
		$this->m_intApplicationDuplicateId = $intApplicationDuplicateId;
	}

	public function setOldApplicationStageId( $intOldApplicationStageId ) {
		$this->m_intOldApplicationStageId = $intOldApplicationStageId;
	}

	public function setOldApplicationStatusId( $intOldApplicationStatusId ) {
		$this->m_intOldApplicationStatusId = $intOldApplicationStatusId;
	}

	public function setContactDatetime( $strContactDatetime ) {
		$this->m_strContactDatetime = $strContactDatetime;
	}

	public function setLastContactDays( $intLastContactDays ) {
		$this->m_intLastContactDays = $intLastContactDays;
	}

	public function setIsTextViewed( $intIsTextViewed ) {
		$this->m_intIsTextViewed = $intIsTextViewed;
	}

	public function setIsSendText( $intIsSendText ) {
		$this->m_intIsSendText = $intIsSendText;
	}

	public function setFileMergeValueCount( $intFileMergeValueCount ) {
		$this->m_intFileMergeValueCount = $intFileMergeValueCount;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->m_intApplicantApplicationId = $intApplicantApplicationId;
	}

	public function setAddedOn( $strAddedOn ) {
		$this->m_strAddedOn = $strAddedOn;
	}

	public function setEventDatetime( $strEventDatetime ) {
		$this->m_strEventDatetime = $strEventDatetime;
	}

	public function setFloorplanName( $strFloorplanName ) {
		$this->m_strFloorplanName = $strFloorplanName;
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->m_intEventTypeId = $intEventTypeId;
	}

	public function setPropertyTransmissionVendorId( $intPropertyTransmissionVendorId ) {
		$this->m_intPropertyTransmissionVendorId = $intPropertyTransmissionVendorId;
	}

	public function setApplicationDocumentId( $intApplicationDocumentId ) {
		$this->m_intApplicationDocumentId = $intApplicationDocumentId;
	}

	public function setApplicationFormId( $intApplicationFormId ) {
		$this->m_intApplicationFormId = $intApplicationFormId;
	}

	public function setTotalSigned( $intTotalSigned ) {
		$this->m_intTotalSigned = $intTotalSigned;
	}

	public function setTotalSigning( $intTotalSigning ) {
		$this->m_intTotalSigning = $intTotalSigning;
	}

	public function setAdditionalInfo( $strAdditionalInfo ) {
		$this->m_strAdditionalInfo = \Psi\CStringService::singleton()->ucfirst( $strAdditionalInfo );
	}

	public function setVerificationKey( $strVerificationKey ) {
		$this->m_strVerificationKey = $strVerificationKey;
	}

	public function setDesiredRentMin( $fltDesiredRentMin ) {
		parent::setDesiredRentMin( round( $fltDesiredRentMin ) );
	}

	public function setDesiredRentMax( $fltDesiredRentMax ) {
		parent::setDesiredRentMax( round( $fltDesiredRentMax ) );
	}

	public function setIsRentRateRequired( $boolIsRentRateRequired ) {
		$this->m_boolIsRentRateRequired = $boolIsRentRateRequired;
	}

	public function setIsGuestCardDeduped( $boolIsGuestCardDeduped ) {
		$this->m_boolIsGuestCardDeduped = $boolIsGuestCardDeduped;
	}

	public function setIsContactTypeSms( $boolIsContactTypeSms ) {
		$this->m_boolIsContactTypeSms = $boolIsContactTypeSms;
	}

	public function setIsContactTypeChat( $boolIsContactTypeChat ) {
		$this->m_boolIsContactTypeChat = $boolIsContactTypeChat;
	}

	public function setInsertLeaseRecords( $boolInsertLeaseRecords ) {
		$this->m_boolInsertLeaseRecords = $boolInsertLeaseRecords;
	}

	public function setApplicant( $objApplicant ) {
		$this->m_objApplicant = $objApplicant;
	}

	public function setApplicationDuplicate( $objApplicationDuplicate ) {
		$this->m_objApplicationDuplicate = $objApplicationDuplicate;
	}

	public function setEvent( $objEvent ) {
		$this->m_objEvent = $objEvent;
	}

	public function setAvailabilityAlertRequestsReminderEmail( $strAvailabilityAlertRequestsReminderEmail ) {
		$this->m_strAvailabilityAlertRequestsReminderEmail = $strAvailabilityAlertRequestsReminderEmail;
	}

	public function setAvailabilityAlertRequestsId( $intAvailabilityAlertRequestsId ) {
		$this->m_intAvailabilityAlertRequestsId = $intAvailabilityAlertRequestsId;
	}

	public function setAppliedQuoteId( $intAppliedQuoteId ) {
		$this->m_intAppliedQuoteId = $intAppliedQuoteId;
	}

	public function setAvailabilityAlertRequestsReminderCellNumber( $strAvailabilityAlertRequestsReminderCellNumber ) {
		$this->m_strAvailabilityAlertRequestsReminderCellNumber = $strAvailabilityAlertRequestsReminderCellNumber;
	}

	public function setUnavailableUnitSearchNumber( $intUnavailableUnitSearchNumber ) {
		$this->m_intUnavailableUnitSearchNumber = $intUnavailableUnitSearchNumber;
	}

	public function setIsExportEvent( $boolIsExportEvent ) {
		$this->m_boolIsExportEvent = $boolIsExportEvent;
	}

	public function setEditLease( $boolEditLease ) {
		$this->m_boolEditLease = $boolEditLease;
	}

	public function setCancelLease( $boolCancelLease ) {
		$this->m_boolCancelLease = $boolCancelLease;
	}

	public function setUpdateLease( $boolUpdateLease ) {
		$this->m_boolUpdateLease = $boolUpdateLease;
	}

	public function setIsResponsive( $boolIsResponsive ) {
		$this->m_boolIsResponsive = $boolIsResponsive;
	}

	public function setIsIndividualLeaseProgression( $boolIsIndividualLeaseProgression ) {
		$this->m_boolIsIndividualLeaseProgression = $boolIsIndividualLeaseProgression;
	}

	public function setApplicantApplications( $arrobjApplicantApplications ) {
		$this->m_arrobjApplicantApplications = $arrobjApplicantApplications;
	}

	public function setAddOnLeaseAssociations( $arrobjAddOnLeaseAssociations ) {
		$this->m_arrobjAddOnLeaseAssociations = $arrobjAddOnLeaseAssociations;
	}

	public function setApplicants( $arrobjApplicants ) {
		$this->m_arrobjApplicants = $arrobjApplicants;
	}

	public function setLeaseDocuments( $arrobjLeaseDocuments ) {
		$this->m_arrobjLeaseDocuments = $arrobjLeaseDocuments;
	}

	public function setLeaseDocumentId( $intLeaseDocumentId ) {
		$this->m_intLeaseDocumentId = $intLeaseDocumentId;
	}

	public function setIsLeaseEsigned( $boolIsLesaeEsigned ) {
		$this->m_boolIsLeaseEsigned = $boolIsLesaeEsigned;
	}

	public function setIsLeasePartiallyEsigned( $boolLeasePartiallyEsigned ) {
		$this->m_boolLeasePartiallyEsigned = $boolLeasePartiallyEsigned;
	}

	public function setLeaseGeneratedOn( $strLeaseGeneratedOn ) {
		$this->set( 'm_strLeaseGeneratedOn', $strLeaseGeneratedOn );
	}

	public function setLeaseDocumentDeactivateOn( $strLeaseDocumentDeactivateOn ) {
		$this->m_strLeaseDocumentDeactivateOn = $strLeaseDocumentDeactivateOn;
	}

	public function setLeaseExpireOn( $strLeaseExpireOn ) {
		$this->m_strLeaseExpireOn = $strLeaseExpireOn;
	}

	public function setPrimaryPhoneNumber( $strPrimaryPhoneNumber ) {
		$this->m_strPrimaryPhoneNumber = $strPrimaryPhoneNumber;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPrimaryPhoneNumberTypeId( $intPrimaryPhoneNumberTypeId ) {
		$this->m_intPrimaryPhoneNumberTypeId = $intPrimaryPhoneNumberTypeId;
	}

	public function setPrimaryApplicant( $objPrimaryApplicant ) {
		$this->m_objPrimaryApplicant = $objPrimaryApplicant;
	}

	public function setScheduledCharges( $arrobjScheduledCharges ) {
		$this->m_arrobjScheduledCharges = $arrobjScheduledCharges;
	}

	public function setRenewalOffers( $arrobjRenewalOffers ) {
		$this->m_arrobjRenewalOffers = $arrobjRenewalOffers;
	}

	public function setWebsite( $objWebsite ) {
		$this->m_objWebsite = $objWebsite;
	}

	public function setReleaseUnit( $boolIsReleaseUnit ) {
		$this->m_boolIsReleaseUnit = $boolIsReleaseUnit;
	}

	public function setIsForcedGuestCardIntegration( $boolIsForcedGuestCardIntegration ) {
		$this->m_boolIsForcedGuestCardIntegration = $boolIsForcedGuestCardIntegration;
	}

	public function setIsShowIntegrationError( $boolIsShowIntegrationError = false ) {
		$this->m_boolIsShowIntegrationError = $boolIsShowIntegrationError;
	}

	public function setEventNote( $strEventNote ) {
		$this->m_strEventNote = $strEventNote;
	}

	public function setPolicyId( $intPolicyId ) {
		$this->m_intPolicyId = $intPolicyId;
	}

	public function setInsertMessageQueue( $boolInsertMessageQueue ) {
		$this->m_boolInsertMessageQueue = $boolInsertMessageQueue;
	}

	public function setIsValidateCallId( $boolIsValidateCallId ) {
		$this->m_boolIsValidateCallId = $boolIsValidateCallId;
	}

	public function setLeasingAgentFirstName( $strAgentFirstName ) {
		$this->m_strLeasingAgentFirstName = ( 0 == strlen( $strAgentFirstName ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( $strAgentFirstName ) ) );
	}

	public function setLeasingAgentLastName( $strAgentLastName ) {
		$this->m_strLeasingAgentLastName = ( 0 == strlen( $strAgentLastName ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( $strAgentLastName ) ) );
	}

	public function setEventResultId( $intEventResultId ) {
		$this->m_intEventResultId = $intEventResultId;
	}

	public function setTransmissionResponseTypeId( $intTransmissionResponseTypeId ) {
		$this->m_intTransmissionResponseTypeId = $intTransmissionResponseTypeId;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = CStrings::strToIntDef( $intLeaseStatusTypeId, NULL, false );
	}

	public function setLateFeeFormulaId( $intLateFeeFormulaId ) {
		$this->m_intLateFeeFormulaId = CStrings::strToIntDef( $intLateFeeFormulaId, NULL, false );
	}

	public function setLeaseIntervalTypeId( $intLeaseIntervalTypeId ) {
		$this->m_intLeaseIntervalTypeId = CStrings::strToIntDef( $intLeaseIntervalTypeId, NULL, false );
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = CStrings::strToIntDef( $intLeaseTermId, NULL, false );
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->m_intLeaseStartWindowId = CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false );
	}

	public function setTermMonth( $intTermMonth ) {
		$this->m_intTermMonth = CStrings::strToIntDef( $intTermMonth, NULL, false );
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->m_strLeaseEndDate = CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true );
	}

	public function setUnitNumberCache( $strUnitNumberCache ) {
		$this->m_strUnitNumberCache = CStrings::strTrimDef( $strUnitNumberCache, 50, NULL, true );
	}

	public function setScreeningDecisionReasonType( $strScreeningDecisionReasonType ) {
		$this->m_strScreeningDecisionReasonTypes = $strScreeningDecisionReasonType;
	}

	public function setScreeningConditionUrl( $strScreeningConditionUrl ) {
		$this->m_strScreeningConditionUrl = $strScreeningConditionUrl;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) )
			$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) )
			$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['applicant_id'] ) )
			$this->setApplicantId( $arrmixValues['applicant_id'] );
		if( true == isset( $arrmixValues['applicant_application_id'] ) )
			$this->setApplicantApplicationId( $arrmixValues['applicant_application_id'] );
		if( true == isset( $arrmixValues['additional_info'] ) )
			$this->setAdditionalInfo( $arrmixValues['additional_info'] );
		if( true == isset( $arrmixValues['application_step_id'] ) )
			$this->setApplicationStepId( $arrmixValues['application_step_id'] );
		if( true == isset( $arrmixValues['application_step_name'] ) )
			$this->setApplicationStepName( $arrmixValues['application_step_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )
			$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['email_address'] ) )
			$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['unit_number'] ) )
			$this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['contact_id'] ) )
			$this->setApplicationDuplicateId( $arrmixValues['contact_id'] );
		if( true == isset( $arrmixValues['contact_datetime'] ) )
			$this->setContactDatetime( $arrmixValues['contact_datetime'] );
		if( true == isset( $arrmixValues['lease_end_date'] ) )
			$this->setLeaseEndDate( $arrmixValues['lease_end_date'] );
		if( true == isset( $arrmixValues['last_contact_days'] ) )
			$this->setLastContactDays( $arrmixValues['last_contact_days'] );
		if( true == isset( $arrmixValues['is_text_viewed'] ) )
			$this->setIsTextViewed( $arrmixValues['is_text_viewed'] );
		if( true == isset( $arrmixValues['is_send_text'] ) )
			$this->setIsSendText( $arrmixValues['is_send_text'] );
		if( true == isset( $arrmixValues['application_document_id'] ) )
			$this->setApplicationDocumentId( $arrmixValues['application_document_id'] );
		if( true == isset( $arrmixValues['application_form_id'] ) )
			$this->setApplicationFormId( $arrmixValues['application_form_id'] );
		if( true == isset( $arrmixValues['availability_alert_requests_reminder_email'] ) )
			$this->setAvailabilityAlertRequestsReminderEmail( $arrmixValues['availability_alert_requests_reminder_email'] );
		if( true == isset( $arrmixValues['availability_alert_requests_reminder_cell_number'] ) )
			$this->setAvailabilityAlertRequestsReminderCellNumber( $arrmixValues['availability_alert_requests_reminder_cell_number'] );
		if( true == isset( $arrmixValues['availability_alert_requests_id'] ) )
			$this->setAvailabilityAlertRequestsId( $arrmixValues['availability_alert_requests_id'] );
		if( true == isset( $arrmixValues['lease_document_id'] ) )
			$this->setLeaseDocumentId( $arrmixValues['lease_document_id'] );
		if( true == isset( $arrmixValues['added_on'] ) )
			$this->setAddedOn( $arrmixValues['added_on'] );
		if( true == isset( $arrmixValues['event_datetime'] ) )
			$this->setEventDatetime( $arrmixValues['event_datetime'] );
		if( true == isset( $arrmixValues['floorplan_name'] ) )
			$this->setFloorplanName( $arrmixValues['floorplan_name'] );
		if( true == isset( $arrmixValues['building_name'] ) )
			$this->setBuildingName( $arrmixValues['building_name'] );
		if( true == isset( $arrmixValues['event_type_id'] ) )
			$this->setEventTypeId( $arrmixValues['event_type_id'] );
		if( true == isset( $arrmixValues['lease_generated_on'] ) )
			$this->setLeaseGeneratedOn( $arrmixValues['lease_generated_on'] );
		if( true == isset( $arrmixValues['lease_expire_on'] ) )
			$this->setLeaseExpireOn( $arrmixValues['lease_expire_on'] );
		if( true == isset( $arrmixValues['lease_document_deactivate_on'] ) )
			$this->setLeaseDocumentDeactivateOn( $arrmixValues['lease_document_deactivate_on'] );
		if( true == isset( $arrmixValues['application_status'] ) )
			$this->setApplicationStatus( $arrmixValues['application_status'] );
		if( true == isset( $arrmixValues['old_application_stage_id'] ) )
			$this->setOldApplicationStageId( $arrmixValues['old_application_stage_id'] );
		if( true == isset( $arrmixValues['old_application_status_id'] ) )
			$this->setOldApplicationStatusId( $arrmixValues['old_application_status_id'] );
		if( true == isset( $arrmixValues['policy_id'] ) )
			$this->setPolicyId( $arrmixValues['policy_id'] );
		if( true == isset( $arrmixValues['offer_sent_on'] ) )
			$this->setOfferSentOn( $arrmixValues['offer_sent_on'] );
		if( true == isset( $arrmixValues['lead_source_id'] ) )
			$this->setLeadSourceId( $arrmixValues['lead_source_id'] );
		if( true == isset( $arrmixValues['parent_lead_source_id'] ) )
			$this->setParentLeadSourceId( $arrmixValues['parent_lead_source_id'] );
		if( true == isset( $arrmixValues['primary_lead_source_id'] ) )
			$this->setPrimaryLeadSourceId( $arrmixValues['primary_lead_source_id'] );
		if( true == isset( $arrmixValues['leasing_agent_first_name'] ) )
			$this->setLeasingAgentFirstName( $arrmixValues['leasing_agent_first_name'] );
		if( true == isset( $arrmixValues['leasing_agent_last_name'] ) )
			$this->setLeasingAgentLastName( $arrmixValues['leasing_agent_last_name'] );
		if( true == isset( $arrmixValues['primary_applicant_name'] ) )
			$this->setPrimaryApplicantName( $arrmixValues['primary_applicant_name'] );
		if( true == isset( $arrmixValues['transmission_response_type_id'] ) )
			$this->setTransmissionResponseTypeId( $arrmixValues['transmission_response_type_id'] );
		if( true == isset( $arrmixValues['crm_day_difference'] ) )
			$this->setCrmDayDifference( $arrmixValues['crm_day_difference'] );
		if( isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( isset( $arrmixValues['late_fee_formula_id'] ) ) $this->setLateFeeFormulaId( $arrmixValues['late_fee_formula_id'] );
		if( isset( $arrmixValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrmixValues['lease_term_id'] );
		if( isset( $arrmixValues['is_immediate_move_in'] ) ) $this->setIsImmediateMoveIn( $arrmixValues['is_immediate_move_in'] );
		if( isset( $arrmixValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrmixValues['lease_start_window_id'] );
		if( isset( $arrmixValues['term_month'] ) ) $this->setTermMonth( $arrmixValues['term_month'] );
		if( isset( $arrmixValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrmixValues['lease_start_date'] );
		if( isset( $arrmixValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrmixValues['lease_end_date'] );
		if( isset( $arrmixValues['lease_interval_type_id'] ) ) $this->setLeaseIntervalTypeId( $arrmixValues['lease_interval_type_id'] );
		if( isset( $arrmixValues['unit_number_cache'] ) ) $this->setUnitNumberCache( $arrmixValues['unit_number_cache'] );
		if( isset( $arrmixValues['total_signed'] ) ) $this->setTotalSigned( $arrmixValues['total_signed'] );
		if( isset( $arrmixValues['total_signing'] ) ) $this->setTotalSigned( $arrmixValues['total_signing'] );
		if( isset( $arrmixValues['is_transferring_in'] ) ) $this->setIsTransferringIn( $arrmixValues['is_transferring_in'] );
		if( isset( $arrmixValues['transferred_on'] ) ) $this->setTransferredOn( $arrmixValues['transferred_on'] );
		if( isset( $arrmixValues['lead_score'] ) ) $this->setLeadScore( $arrmixValues['lead_score'] );
		if( isset( $arrmixValues['previous_quote_id'] ) ) $this->setPreviousQuoteId( $arrmixValues['previous_quote_id'] );
		if( isset( $arrmixValues['move_in_date'] ) ) $this->setMoveInDate( $arrmixValues['move_in_date'] );
		if( isset( $arrmixValues['preferred_locale_code'] ) ) $this->setPreferredLocaleCode( $arrmixValues['preferred_locale_code'] );

		if( isset( $arrmixValues['optimal_monthly_rent_base'] ) ) $this->setOptimalMonthlyRentBase( $arrmixValues['optimal_monthly_rent_base'] );
		if( isset( $arrmixValues['optimal_monthly_rent_total'] ) ) $this->setOptimalMonthlyRentTotal( $arrmixValues['optimal_monthly_rent_total'] );
		if( isset( $arrmixValues['advertised_monthly_rent_base'] ) ) $this->setAdvertisedMonthlyRentBase( $arrmixValues['advertised_monthly_rent_base'] );
		if( isset( $arrmixValues['advertised_monthly_rent_total'] ) ) $this->setAdvertisedMonthlyRentTotal( $arrmixValues['advertised_monthly_rent_total'] );
		if( isset( $arrmixValues['executed_monthly_rent_base'] ) ) $this->setExecutedMonthlyRentBase( $arrmixValues['executed_monthly_rent_base'] );
		if( isset( $arrmixValues['executed_monthly_rent_total'] ) ) $this->setExecutedMonthlyRentTotal( $arrmixValues['executed_monthly_rent_total'] );
		if( isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );

		if( isset( $arrmixValues['phone_number'] ) ) $this->setPhoneNumber( $arrmixValues['phone_number'] );
		if( isset( $arrmixValues['phone_number_type_id'] ) ) $this->setPhoneNumberTypeId( $arrmixValues['phone_number_type_id'] );
		$this->setAllowDifferentialUpdate( true );
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->m_intCustomerTypeId = $intCustomerTypeId;
	}

	public function setCustomerTypeIds( $arrintCustomerTypeIds ) {
		$this->m_arrintCustomerTypeIds = $arrintCustomerTypeIds;
	}

	public function setDeletedApplicantIds( $arrintDeletedApplicantIds ) {
		$this->m_arrintDeletedApplicantIds = $arrintDeletedApplicantIds;
	}

	public function setCustomerRelationshipIds( $arrintCustomerRelationshipIds ) {
		$this->m_arrintCustomerRelationshipIds = $arrintCustomerRelationshipIds;
	}

	public function setCrmDayDifference( $intCrmDayDifference ) {
		$this->m_intCrmDayDifference = $intCrmDayDifference;
	}

	public function setDocumentManager( $objDocumentManager ) {
		$this->m_objDocumentManager = $objDocumentManager;
	}

	public function setObjectStorageGateway( $objObjectStorageGateway ) {
		$this->m_objObjectStorageGateway = $objObjectStorageGateway;
	}

	public function setLeaseTermAndDatesAlreadyApplied( $boolLeaseTermAndDatesAlreadyApplied ) {
		$this->m_boolLeaseTermAndDatesAlreadyApplied = $boolLeaseTermAndDatesAlreadyApplied;
	}

	public function setStudentPropertyApplication( $boolStudentPropertyApplication ) {
		$this->m_boolStudentPropertyApplication = $boolStudentPropertyApplication;
	}

	public function setMergeFields( $arrstrMergeFields ) {
		foreach( $arrstrMergeFields as $strKey => $strValue ) {
			$this->setMergeFieldByKey( $strKey, $strValue );
		}
	}

	public function setMergeFieldByKey( $strKey, $strValue ) {
		$this->m_arrstrMergeFields[$strKey] = $strValue;
	}

	public function setPaymentCaptured( $intPaymentCaptured ) {
		$this->m_intPaymentCaptured = $intPaymentCaptured;
	}

	public function setApplicationStageStatusId( $intApplicationStageStatusId ) {
		$this->m_intApplicationStageStatusId = $intApplicationStageStatusId;
	}

	public function setIsTransferringIn( $boolIsTransferringIn ) {
		$this->m_boolIsTransferringIn = $boolIsTransferringIn;
	}

	public function setTransferredOn( $strTransferredOn ) {
		$this->m_strTransferredOn = $strTransferredOn;
	}

	public function setPrimaryApplicantName( $strPrimaryApplicantName ) {
		$this->m_strPrimaryApplicantName = $strPrimaryApplicantName;
	}

	public function setHasPets( $boolHasPets ) {
		$this->m_boolHasPets = $boolHasPets;
	}

	public function setIsInsurancePolicyExists( $boolIsInsurancePolicyExists ) {
		$this->m_boolIsInsurancePolicyExists = $boolIsInsurancePolicyExists;
	}

	public function setLeadScore( $intLeadScore ) {
		$this->m_intLeadScore = $intLeadScore;
	}

	public function setApplicationNotes( $strApplicationNotes ) {
		$this->m_strApplicationNotes = trim( $strApplicationNotes );
	}

	public function setPreviousQuoteId( $intPreviousQuoteId ) {
		$this->m_intPreviousQuoteId = $intPreviousQuoteId;
	}

	public function setOptimalMonthlyRentBase( $fltOptimalMonthlyRentBase ) {
		$this->set( 'm_fltOptimalMonthlyRentBase', CStrings::strToFloatDef( $fltOptimalMonthlyRentBase, NULL, false, 2 ) );
	}

	public function setOptimalMonthlyRentTotal( $fltOptimalMonthlyRentTotal ) {
		$this->set( 'm_fltOptimalMonthlyRentTotal', CStrings::strToFloatDef( $fltOptimalMonthlyRentTotal, NULL, false, 2 ) );
	}

	public function setAdvertisedMonthlyRentBase( $fltAdvertisedMonthlyRentBase ) {
		$this->set( 'm_fltAdvertisedMonthlyRentBase', CStrings::strToFloatDef( $fltAdvertisedMonthlyRentBase, NULL, false, 2 ) );
	}

	public function setAdvertisedMonthlyRentTotal( $fltAdvertisedMonthlyRentTotal ) {
		$this->set( 'm_fltAdvertisedMonthlyRentTotal', CStrings::strToFloatDef( $fltAdvertisedMonthlyRentTotal, NULL, false, 2 ) );
	}

	public function setExecutedMonthlyRentBase( $fltExecutedMonthlyRentBase ) {
		$this->set( 'm_fltExecutedMonthlyRentBase', CStrings::strToFloatDef( $fltExecutedMonthlyRentBase, NULL, false, 2 ) );
	}

	public function setExecutedMonthlyRentTotal( $fltExecutedMonthlyRentTotal ) {
		$this->set( 'm_fltExecutedMonthlyRentTotal', CStrings::strToFloatDef( $fltExecutedMonthlyRentTotal, NULL, false, 2 ) );
	}

	public function setEventSource( $strEventSource ) {
		$this->m_strEventSource = $strEventSource;
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->m_strMoveInDate = CStrings::strTrimDef( $strMoveInDate, - 1, NULL, true );
	}

	public function setPreferredLocaleCode( $strPreferredLocaleCode ) {
		$this->m_strPreferredLocaleCode = CStrings::strTrimDef( $strPreferredLocaleCode, - 1, NULL, true );
	}

	public function setIsAutoLeaseApprove( $boolIsAutoLeaseApprove ) {
		$this->m_boolIsAutoLeaseApprove = $boolIsAutoLeaseApprove;
	}

	public function setLeaseInterval( $objLeaseInterval ) {
		$this->m_objLeaseInterval = $objLeaseInterval;
	}

	public function setChatId( $intChatId ) {
		$this->m_intChatId = $intChatId;
	}

	public function setCustomerPhoneNumbers( $arrmixPhoneNumbers ) {
		$this->m_arrmixCustomerPhoneNUmbers = $arrmixPhoneNumbers;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addApplicantApplication( $objApplicantApplication ) {
		$this->m_arrobjApplicantApplications[$objApplicantApplication->getId()] = $objApplicantApplication;
	}

	public function addAddOnLeaseAssociations( $objAddOnLeaseAssociation ) {
		$this->m_arrobjAddOnLeaseAssociations[$objAddOnLeaseAssociation->getId()] = $objAddOnLeaseAssociation;
	}

	public function addApplicant( $objApplicant ) {
		$this->m_arrobjApplicants[$objApplicant->getId()] = $objApplicant;
	}

	public function addFile( $objFile ) {
		$this->m_arrobjFiles[$objFile->getId()] = $objFile;
	}

	public function addRenewalOffer( $objRenewalOffer ) {
		$this->m_arrobjRenewalOffers[$objRenewalOffer->getId()] = $objRenewalOffer;
	}

	public function addScheduledCharge( $objScheduledCharge ) {
		$this->m_arrobjScheduledCharges[$objScheduledCharge->getId()] = $objScheduledCharge;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createApplicantApplication() {
		$objApplicantApplication = new CApplicantApplication();
		$objApplicantApplication->setApplicationId( $this->getId() );
		$objApplicantApplication->setCid( $this->getCid() );
		$objApplicantApplication->setStartedOn( 'NOW()' );

		return $objApplicantApplication;
	}

	public function createLease( $objDatabase ) {
		$objLease = new CLease();

		$objLease->setCid( $this->getCid() );
		$objLease->setPropertyId( $this->getPropertyId() );
		$objLease->setPropertyUnitId( $this->getPropertyUnitId() );
		$objLease->setRemotePrimaryKey( $this->getAppRemotePrimaryKey() );
		$objLease->setUnitSpaceId( $this->getUnitSpaceId() );
		$objLease->setLateFeeFormulaId( $this->getLateFeeFormulaId() );
		$objLease->setLeaseStatusTypeId( CLeaseStatusType::APPLICANT );
		$objLease->setPaymentAllowanceTypeId( CPaymentAllowanceType::ALLOW_ALL );

		if( CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() ) {
			$objLease->setLeaseIntervalTypeId( CLeaseIntervalType::APPLICATION );
		} elseif( CLeaseIntervalType::TRANSFER == $this->getLeaseIntervalTypeId() ) {
			$objLease->setLeaseIntervalTypeId( CLeaseIntervalType::TRANSFER );
		}

		if( false == is_null( $this->getOccupancyTypeId() ) )
			$objLease->setOccupancyTypeId( $this->getOccupancyTypeId() );

		$objLease->setActiveLeaseIntervalId( $this->getLeaseIntervalId() );

		if( true == CValidation::validateDate( $this->getLeaseStartDate() ) ) {
			$objLease->setLeaseStartDate( $this->getLeaseStartDate() );
			$objLease->setMoveInDate( $this->getLeaseStartDate() );
		}

		$objLease->setLeaseTermId( $this->getLeaseTermId() );
		if( true == valStr( $this->getLeaseStartWindowId( $objDatabase ) ) ) {
			$objLease->setLeaseStartWindowId( $this->getLeaseStartWindowId( $objDatabase ) );
		}
		$objLease->setApplicationId( $this->getId() );
		$objLease->setLeaseEndDate( $this->getLeaseEndDate() );
		$objLease->setLeaseTypeId( $this->getLeaseTypeId() );

		return $objLease;
	}

	public function createPetLeaseAssociation() {

		$objPetLeaseAssociation = new CPetLeaseAssociation();
		$objPetLeaseAssociation->setCid( $this->getCid() );
		$objPetLeaseAssociation->setPropertyId( $this->getPropertyId() );
		$objPetLeaseAssociation->setLeaseId( $this->getLeaseId() );
		$objPetLeaseAssociation->setLeaseIntervalId( $this->getLeaseIntervalId() );
		$objPetLeaseAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objPetLeaseAssociation->setArCascadeReferenceId( $this->getPropertyId() );
		$objPetLeaseAssociation->setArOriginId( CArOrigin::PET );
		$objPetLeaseAssociation->setUnitSpaceId( $this->getUnitSpaceId() );
		$objPetLeaseAssociation->setPropertyFloorplanId( $this->getPropertyFloorplanId() );
		$objPetLeaseAssociation->setUnitTypeId( $this->getUnitTypeId() );

		return $objPetLeaseAssociation;
	}

	public function createApplicationDuplicate( $objDatabase ) {
		$this->getOrFetchClient( $objDatabase );

		$objApplicationDuplicate = new CApplicationDuplicate();
		$objApplicationDuplicate->setCid( $this->getCid() );
		$objApplicationDuplicate->setPropertyId( $this->getPropertyId() );
		$objApplicationDuplicate->setLeadSourceId( $this->getLeadSourceId() );
		$objApplicationDuplicate->setPropertyFloorplanId( $this->getPropertyFloorplanId() );
		$objApplicationDuplicate->setPropertyUnitId( $this->getPropertyUnitId() );
		$objApplicationDuplicate->setUnitSpaceId( $this->getUnitSpaceId() );
		$objApplicationDuplicate->setLeaseIntervalTypeId( $this->getLeaseIntervalTypeId() );
		$objApplicationDuplicate->setDesiredRentMin( $this->getDesiredRentMin() );
		$objApplicationDuplicate->setDesiredRentMax( $this->getDesiredRentMax() );
		$objApplicationDuplicate->setTermMonth( $this->getTermMonth() );
		$objApplicationDuplicate->setLeaseStartDate( $this->getLeaseStartDate() );
		$objApplicationDuplicate->setDesiredBedrooms( $this->getDesiredBedrooms() );
		$objApplicationDuplicate->setDesiredBathrooms( $this->getDesiredBathrooms() );
		$objApplicationDuplicate->setContactDatetime( 'now()' );
		$objApplicationDuplicate->setSemSourceId( $this->getSemSourceId() );
		$objApplicationDuplicate->setSemAdGroupId( $this->getSemAdGroupId() );
		$objApplicationDuplicate->setSemKeywordId( $this->getSemKeywordId() );

		return $objApplicationDuplicate;
	}

	public function createExportRecord() {
		$objExportRecord = new CExportRecord();
		$objExportRecord->setCid( $this->getCid() );
		$objExportRecord->setTableKey( 'applications' );
		$objExportRecord->setExportTypeKey( $this->getInternetListingServiceId() );
		$objExportRecord->setReferenceNumber( $this->getId() );
		$objExportRecord->setExportedOn( date( 'Y-m-d H:i:s' ) );

		return $objExportRecord;
	}

	public function createTransmission() {
		$objTransmission = new CTransmission();

		$objTransmission->setDefaults();
		$objTransmission->setCid( $this->m_intCid );
		$objTransmission->setApplicationId( $this->m_intId );
		$objTransmission->setPropertyId( $this->getPropertyId() );

		return $objTransmission;
	}

	public function createScheduledCharge() {
		$objScheduledCharge = new CScheduledCharge();
		$objScheduledCharge->setCid( $this->getCid() );
		$objScheduledCharge->setLeaseId( $this->getLeaseId() );
		$objScheduledCharge->setLeaseIntervalId( $this->getLeaseIntervalId() );
		$objScheduledCharge->setPropertyId( $this->getPropertyId() );

		return $objScheduledCharge;
	}

	public function createInsurancePolicy() {
		$objInsurancePolicy = new CInsurancePolicy();
		$objInsurancePolicy->setCid( $this->getCid() );
		$objInsurancePolicy->setPropertyId( $this->getPropertyId() );
		$objInsurancePolicy->setPropertyUnitId( $this->getPropertyUnitId() );
		$objInsurancePolicy->setUnitSpaceId( $this->getUnitSpaceId() );
		$objInsurancePolicy->setLeaseId( $this->getLeaseId() );
		$objInsurancePolicy->setApplicationId( $this->getId() );

		return $objInsurancePolicy;
	}

	public function createResidentInsurancePolicy() {
		$objResidentInsurancePolicy = new CResidentInsurancePolicy();
		$objResidentInsurancePolicy->setCid( $this->getCid() );
		$objResidentInsurancePolicy->setPropertyId( $this->getPropertyId() );
		$objResidentInsurancePolicy->setPropertyUnitId( $this->getPropertyUnitId() );
		$objResidentInsurancePolicy->setUnitSpaceId( $this->getUnitSpaceId() );

		return $objResidentInsurancePolicy;
	}

	public function createAmenityLeaseAssociation() {
		$objAmenityLeaseAssociation = new CAmenityLeaseAssociation();

		$objAmenityLeaseAssociation->setCid( $this->getCid() );
		$objAmenityLeaseAssociation->setPropertyId( $this->getPropertyId() );
		$objAmenityLeaseAssociation->setLeaseId( $this->getLeaseId() );
		$objAmenityLeaseAssociation->setLeaseIntervalId( $this->getLeaseIntervalId() );
		$objAmenityLeaseAssociation->setArOriginId( CArOrigin::AMENITY );

		return $objAmenityLeaseAssociation;
	}

	public function createUnitSpaceOccupancyHistory() {
		$objUnitSpaceOccupancyHistory = new CUnitSpaceOccupancyHistory();
		$objUnitSpaceOccupancyHistory->setCid( $this->getCid() );
		$objUnitSpaceOccupancyHistory->setPropertyId( $this->getPropertyId() );
		$objUnitSpaceOccupancyHistory->setApplicationId( $this->getId() );
		$objUnitSpaceOccupancyHistory->setPropertyUnitId( $this->getPropertyUnitId() );
		$objUnitSpaceOccupancyHistory->setUnitSpaceId( $this->getUnitSpaceId() );
		$objUnitSpaceOccupancyHistory->setLeaseId( $this->getLeaseId() );

		return $objUnitSpaceOccupancyHistory;
	}

	public function createFileMergeValue() {
		$objFileMergeValue = new CFileMergeValue();
		$objFileMergeValue->setCid( $this->m_intCid );
		$objFileMergeValue->setApplicationId( $this->m_intId );

		return $objFileMergeValue;
	}

	public function createScreeningApplicationRequest( $intResidentVerifyScreeningVersion, $intCompanyUserId, $objDatabase ) {

		$objScreeningApplicationRequest = new CScreeningApplicationRequest();

		$objScreeningApplicationRequest->setApplicationId( $this->getId() );
		$objScreeningApplicationRequest->setCid( $this->getCid() );
		$objScreeningApplicationRequest->setScreeningVendorId( $intResidentVerifyScreeningVersion );
		$objScreeningApplicationRequest->setRequestStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
		$objScreeningApplicationRequest->setApplicationTypeId( CScreeningApplicationRequest::CONVENTIONAL_APPLICATION_TYPE_ID );

		$objDatabase->begin();

		if( false == $objScreeningApplicationRequest->insert( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Application [ID: ' ) . $this->getId() . __( '] : Failed to create screening application request.' ), NULL ) );
			$objDatabase->rollback();
			return;
		}

		$objDatabase->commit();

		return $objScreeningApplicationRequest;
	}

	public function createRoommate() {
		$objRoommate = new CRoommate();
		$objRoommate->setCid( $this->getCid() );
		$objRoommate->setParentLeaseId( $this->getLeaseId() );

		return $objRoommate;
	}

	public function createRoommateGroup() {
		$objRoommateGroup = new CRoommateGroup();
		$objRoommateGroup->setCid( $this->getCid() );
		$objRoommateGroup->setApplicationId( $this->getId() );
		$objRoommateGroup->setLeaseId( $this->getLeaseId() );

		return $objRoommateGroup;
	}

	public function createApplicationInterest() {
		$objApplicationInterest = new CApplicationInterest();
		$objApplicationInterest->setId( $objApplicationInterest->fetchNextId( 'public.application_interests_id_seq', $this->getDatabase() ) );
		$objApplicationInterest->setCid( $this->getCid() );
		$objApplicationInterest->setPropertyId( $this->getPropertyId() );
		$objApplicationInterest->setPropertyFloorplanId( $this->getPropertyFloorplanId() );
		$objApplicationInterest->setApplicationId( $this->getId() );
		$objApplicationInterest->setApplication( $this );
		$objApplicationInterest->setDatabase( $this->getDatabase() );

		return $objApplicationInterest;
	}

	public function createApplicationInterestSkip() {
		$objApplicationInterestSkip = new CApplicationInterestSkip();
		$objApplicationInterestSkip->setDefaults();
		$objApplicationInterestSkip->setCid( $this->getCid() );
		$objApplicationInterestSkip->setPropertyId( $this->getPropertyId() );

		return $objApplicationInterestSkip;
	}

	public function createApplicationFloorplan() {

		$objApplicationFloorplan = new CApplicationFloorplan();
		$objApplicationFloorplan->setCid( $this->getCid() );
		$objApplicationFloorplan->setPropertyId( $this->getPropertyId() );
		$objApplicationFloorplan->setApplicationId( $this->getId() );

		return $objApplicationFloorplan;
	}

	public function createChat() {
		$objChat = new CChat();
		$objChat->setPsProductId( $this->getPsProductId() );
		$objChat->setCid( $this->getCid() );
		$objChat->setPropertyId( $this->getPropertyId() );

		return $objChat;
	}

	public function createQuote() {
		$objQuote = new CQuote();

		$objQuote->setCid( $this->getCid() );
		$objQuote->setApplicationId( $this->getId() );

		return $objQuote;
	}

	public function createApplicationSubsidyDetail() {
		$objApplicationSubsidyDetail = new CApplicationSubsidyDetail();
		$objApplicationSubsidyDetail->setCid( $this->getCid() );
		$objApplicationSubsidyDetail->setApplicationId( $this->getId() );
		$objApplicationSubsidyDetail->setPrimaryApplicantId( $this->getPrimaryApplicantId() );
		return $objApplicationSubsidyDetail;
	}

	public function createSubsidyCertification() {
		$objSubsidyCertification = new CSubsidyCertification();
		$objSubsidyCertification->setCid( $this->getCid() );
		$objSubsidyCertification->setApplicationId( $this->getId() );
		$objSubsidyCertification->setPropertyId( $this->getPropertyId() );
		$objSubsidyCertification->setSubsidyCertificationStatusTypeId( CSubsidyCertificationStatusType::OPEN );
		return $objSubsidyCertification;
	}

	/**
	 * Email Functions
	 *
	 */

	public function sendApplicationScreeningEmails( $objApplicationTransmission, $strSecureBaseName, $objDatabase, $objEmailDatabase = NULL ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$this->m_arrobjPropertyApplicationPreferences = $this->getOrFetchPropertyApplicationPreferences( $objDatabase );

		if( true == valArr( $this->m_arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'DISABLE_APPLICATION_SCREENING_CONFIRMATION_PAGE_EMAIL_TEXT', $this->m_arrobjPropertyApplicationPreferences ) ) return;

		$boolIsTransmissionAvailable	= false;

		if( true == valObj( $objApplicationTransmission, 'CTransmission' ) ) {
			$objTransmission 	= $objApplicationTransmission;
			$boolIsTransmissionAvailable = true;

		} elseif( true == valObj( $objApplicationTransmission, 'CApplicantApplicationTransmission' ) ) {
			$objTransmission             = $objApplicationTransmission->fetchTransmission( $objDatabase );
			$boolIsTransmissionAvailable = true;
		}

			if( false == valObj( $objTransmission, 'CTransmission' ) ) return;

		$arrobjApplicants = ( array ) $this->fetchApplicantsByCustomerTypeIds( CCustomerType::$c_arrintResponsibleCustomerTypeIds, $objDatabase );

		if( false == valArr( $arrobjApplicants ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Applicants could not be found in database. Email not sent.' ), NULL ) );
			return;
		}

		$arrobjApplicantApplications 				= ( array ) $this->fetchApplicantApplicationsByApplicantIds( array_keys( $arrobjApplicants ), $objDatabase );
		$arrobjApplicantApplicationTransmissions 	= ( array ) $this->fetchApplicantApplicationTransmissionsByApplicantApplicationIds( array_keys( $arrobjApplicantApplications ), $objDatabase );
		$arrobjApplicantApplicationTransmissions 	= rekeyObjects( 'ApplicantApplicationId', $arrobjApplicantApplicationTransmissions );

		$objClient 									= $this->getOrFetchClient( $objDatabase );
		$objProperty 								= $this->getOrFetchProperty( $objDatabase );
		$objOfficePhoneNumber 						= $objProperty->getOrFetchOfficePhoneNumber( $objDatabase );
		$objMarketingMediaAssociation				= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		$objLogoMarketingMediaAssociation 		    = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		$arrobjPropertyPreferences 					= ( array ) $objProperty->fetchPropertyPreferences( $objDatabase );
		$arrobjTransmissionResponseTypes 			= ( array ) CTransmissionResponseTypes::fetchAllTransmissionResponseTypes( $objDatabase );
		$objPropertyApplicationPreferences 			= ( array ) $this->fetchPropertyApplicationPreferences( $objDatabase );
		$objPropertyEmailRule						= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::PROSPECT_EMAILER, $objProperty->getId(), $objDatabase );

		$objPrimaryApplicantApplicationTransmission = NULL;

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( CCustomerType::PRIMARY == $objApplicantApplication->getCustomerTypeId() ) {
				$objPrimaryApplicantApplicationTransmission = getArrayElementByKey( $objApplicantApplication->getId(), $arrobjApplicantApplicationTransmissions );
				break;
			}
		}

		if( false == valObj( $objPrimaryApplicantApplicationTransmission, 'CApplicantApplicationTransmission' ) ) return;

		$intNoNInProgressRecommendation	= 0;
		$boolSendMailManager 			= false;
		$objEvent 						= NULL;

		foreach( $arrobjApplicantApplicationTransmissions as $objApplicantApplicationTransmission ) {
			if( true == $boolIsTransmissionAvailable && CTransmissionVendor::RESIDENT_VERIFY == $objApplicantApplicationTransmission->getTransmissionVendorId() && true == in_array( $objApplicantApplicationTransmission->getTransmissionResponseTypeId(), [ CTransmissionResponseType::APPROVED, CTransmissionResponseType::CONDITIONALLY_APPROVED, CTransmissionResponseType::DENIED ] ) ) {
				$intNoNInProgressRecommendation++;
			}
		}

		$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByPropertyIdByLeaseIntervalIdByEventTypeIdsByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), [ CEventType::SCREENING_COMPLETE_EMAIL_TO_MANAGER ], $this->getCid(), $objDatabase );

		if( false == valArr( $objEvent, 'CEvent' ) && \Psi\Libraries\UtilFunctions\count( $arrobjApplicantApplicationTransmissions ) == $intNoNInProgressRecommendation ) {

			$objEventLibrarySendMailToManager	= new CEventLibrary();
			$objEventLibraryDataObject			= $objEventLibrarySendMailToManager->getEventLibraryDataObject();

			$objEventLibraryDataObject->setDatabase( $objDatabase );

			$objEvent = $objEventLibrarySendMailToManager->createEvent( [ $this ], CEventType::SCREENING_COMPLETE_EMAIL_TO_MANAGER );

			$objEvent->setCid( $this->getCid() );
			$objEvent->setEventDatetime( 'NOW()' );

			$objEventLibrarySendMailToManager->buildEventDescription( $this );

			$objDatabase->begin();

			$objEventLibrarySendMailToManager->setIsExportEvent( true );

			if( false == $objEventLibrarySendMailToManager->insertEvent( SYSTEM_USER_ID, $objDatabase ) ) {
				$this->addErrorMsgs( $objEventLibrarySendMailToManager->getErrorMsgs() );
				$objDatabase->rollback();
			}

			$objDatabase->commit();

			$boolSendMailManager = true;
		}

		// smarty assignment
		$objSmarty = new CPsSmarty( PATH_INTERFACES_PROSPECT_PORTAL, false );
		CCustomerType::assignSmartyConstants( $objSmarty );
		CTransmissionVendor::assignSmartyConstants( $objSmarty );

		$objSmarty->assign( 'application',									$this );
		$objSmarty->assign( 'transmission',									$objTransmission );
		$objSmarty->assign( 'property',										$objProperty );
		$objSmarty->assign( 'office_phone_number',							$objOfficePhoneNumber );
		$objSmarty->assign( 'marketing_media_association',					$objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file',					$objLogoMarketingMediaAssociation );
		$objSmarty->assign( 'primary_applicant_application_transmission',	$objPrimaryApplicantApplicationTransmission );
		$objSmarty->assign( 'applicant_application_transmissions',			$arrobjApplicantApplicationTransmissions );

		$objSmarty->assign( 'property_application_preferences',				rekeyObjects( 'Key', $objPropertyApplicationPreferences ) );

		$objSmarty->assign( 'applicants',									$arrobjApplicants );
		$objSmarty->assign( 'applicant_applications',						rekeyObjects( 'ApplicantId', $arrobjApplicantApplications ) );
		$objSmarty->assign( 'transmission_response_types',					$arrobjTransmissionResponseTypes );
		$objSmarty->assign( 'customer_types',								CCustomerTypes::fetchAllCustomerTypes( $objDatabase ) );

		$objSmarty->assign( 'media_library_uri',							CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'terms_and_condition_path',						$strSecureBaseName . '?module=miscellaneous_items&action=view_terms_and_conditions&application[id]=' . $this->getId() . '&company_application_id=' . $this->getCompanyApplicationId() );
		$objSmarty->assign( 'base_uri',										CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );

		// response types
		$objSmarty->assign( 'SCREENING_RESPONSE_TYPE_APPROVED',				CTransmissionResponseType::APPROVED );
		$objSmarty->assign( 'SCREENING_RESPONSE_TYPE_IN_PROGRESS',			CTransmissionResponseType::IN_PROGRESS );
		$objSmarty->assign( 'SCREENING_RESPONSE_TYPE_DENIED',				CTransmissionResponseType::DENIED );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',						CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	                                CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$arrobjSystemEmails = [];
		$objSystemEmail		= new CSystemEmail();

		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::PROSPECT_EMAILER );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::NORMAL );
		$objSystemEmail->setSubject( 'Application Screening Report.' );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		// From email address for applicants
		if( true == array_key_exists( 'FROM_APPLICATION_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() ) ) {
			$objSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() );
		}

		$objSmarty->assign( 'is_manager_email', false );

		if( true == $boolIsTransmissionAvailable && true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'BLOCK_SCREENING_EMAILS_TO_APPLICANTS', $arrobjPropertyPreferences ) && CTransmissionVendor::RENTAL_HISTORY_REPORTS != $objTransmission->getTransmissionVendorId() ) {
			if( true == valArr( $arrobjApplicants ) ) {
				$arrmixCustomers = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomerPreferredLocaleCodeByIdsByCid( array_keys( rekeyObjects( 'CustomerId', $arrobjApplicants ) ), $objClient->getId(), $objDatabase );
				foreach( $arrobjApplicants as $objApplicant ) {
					if( CCustomerType::GUARANTOR == $objApplicant->getCustomerTypeId() ) {
						continue;
					}

					$objSmarty->assign( 'current_applicant', $objApplicant );

					try {
						if( true == array_key_exists( $objApplicant->getCustomerId(), $arrmixCustomers ) ) {
							CLocaleContainer::createService()->setPreferredLocaleCode( $arrmixCustomers[$objApplicant->getCustomerId()], $objDatabase, $objProperty );
						}
						$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/application_screening_email.tpl' );
						CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
					} catch( Exception $objException ) {
						CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
					}
					$objCurrentSystemEmail = clone $objSystemEmail;
					$objCurrentSystemEmail->setToEmailAddress( $objApplicant->getUsername() );
					$objCurrentSystemEmail->setApplicantId( $objApplicant->getId() );

					if( 0 < strlen( $strHtmlEmailOutput ) ) {
						$objCurrentSystemEmail->setHtmlContent( $strHtmlEmailOutput );
					}

					$arrobjSystemEmails[] = $objCurrentSystemEmail;
				}
			}
		}

		$arrstrPropertyEmailAddresses = ( array ) $objProperty->fetchApplicationNotificationEmails( $objDatabase );

		$objSmarty->assign( 'is_manager_email', 		true );

		$objSmarty->assign( 'send_mail_manager', $boolSendMailManager );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL',	CONFIG_COMPANY_NAME_FULL );

		foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {
			$objSystemEmail->setToEmailAddress( $strEmailAddress );

			$strManagerHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/application_screening_email.tpl' );

			if( 0 < strlen( $strManagerHtmlEmailContent ) ) {
				$objSystemEmail->setHtmlContent( $strManagerHtmlEmailContent );
			}

			$objCurrentSystemEmail = clone $objSystemEmail;

			// From email address for manager
			if( true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) {
				$objCurrentSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() );
			} else {
				$objCurrentSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
			}

			$objCurrentSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

			$arrobjSystemEmails[] = $objCurrentSystemEmail;
		}

		$arrobjSystemEmails = ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjSystemEmails, $objPropertyEmailRule );

		if( true == valArr( $arrobjSystemEmails ) ) {
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				$strToEmailAddress = trim( $objSystemEmail->getToEmailAddress() );
				if( true == empty( $strToEmailAddress ) ) {
					continue;
				}

				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
					continue;
				}
			}
		}
	}

	public function sendSignInEmail( $objPrimaryApplicant, $strSecureBaseName, $objSecondaryApplicant = NULL, $strEmailType = '', $intCompanyUserId= NULL, $objDatabase, $objEmailDatabase = NULL, $boolSendInvitation = false, $boolIsShowApplicantInviteOnApplication = false ) {

		$arrobjPropertyApplicationPreferences	= $this->getOrFetchPropertyApplicationPreferences( $objDatabase );
		$objCompanyUser							= NULL;
		$objCompanyEmployee						= NULL;

		if( NULL != $intCompanyUserId ) {
			$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $intCompanyUserId, $this->getCid(), $objDatabase );
		}

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );
		}

		// If no secondary applicant is sent, we are sending this email to all applicants.
		if( true == valObj( $objSecondaryApplicant, 'CApplicant' ) ) {
			$arrobjApplicants = [
					$objSecondaryApplicant
			];
		} else {
			if( 'lease_agreement' == $strEmailType ) {
				$arrobjApplicants = ( array ) $this->fetchPendingLeaseApplicantsByCustomerTypeIds( CCustomerType::$c_arrintResponsibleCustomerTypeIds, $intRequiredApplicantAge = NULL, $objDatabase );
			} else {
				$arrobjApplicants = ( array ) $this->fetchPendingApplicantsByCustomerTypeIds( [ CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT, CCustomerType::GUARANTOR ], $intRequiredApplicantAge = NULL, $objDatabase );
			}
		}

		if( true == valArr( $arrobjApplicants ) ) {
			$arrobjApplicants	= rekeyObjects( 'CustomerId', $arrobjApplicants );
			$arrobjCustomers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByIdsByCid( array_keys( $arrobjApplicants ), $this->getCid(), $objDatabase );

			foreach( $arrobjCustomers as $intCustomerId => $objCustomer ) {
				$arrobjApplicants[$intCustomerId]->setCustomer( $objCustomer );
			}

			$arrobjApplicants	= rekeyObjects( 'Id', $arrobjApplicants );
		}

		if( true == is_null( $strEmailType ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No email type sent to sendSignInEmail.' ), NULL ) );
			exit();
		}

		$objProperty = $this->fetchProperty( $objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to load property object from database. Email not sent.' ), NULL ) );
			return false;
		}

		$strSecureBaseName = $this->generateWebsiteUrl( $objDatabase, $strSecureBaseName, $boolSendInvitation, $boolPreferDefaultWebsite = true );

		if( true == is_null( $strSecureBaseName ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'The property associated to this application could not be loaded. Email not sent.' ), NULL ) );
			exit();
		}

		$arrobjSystemEmails = [];
		$arrobjEventLibraries = [];

		$objPropertyEmailRule = CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::PROSPECT_EMAILER, $objProperty->getId(), $objDatabase );

		if( ( true == valObj( $objPropertyEmailRule, 'CPropertyEmailRule' ) ) && ( false == $objPropertyEmailRule->getSendsToProspect() || true == $objPropertyEmailRule->getProspectsDisabled() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( '"Send to Prospect" setting is disabled.' ), NULL ) );
			return false;
		}

		if( true == valArr( $arrobjApplicants ) ) {
			foreach( $arrobjApplicants as $objApplicant ) {
				if( true == is_null( $objApplicant->getUsername() ) || 0 == strlen( $objApplicant->getUsername() ) ) {
					if( true == isset( $objSecondaryApplicant ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Invitation email cannot be sent to selected applicants.' ), NULL ) );
						return false;
					} else {
						continue;
					}
				}
				try {
					if( true == valObj( $objApplicant->getCustomer(), 'CCustomer' ) ) {
						CLocaleContainer::createService()->setPreferredLocaleCode( $objApplicant->getCustomer()->getPreferredLocaleCode(), $objDatabase, $objProperty );
					}
					if( 'application_invitation' == $strEmailType ) {
						$this->setApplicantId( $objApplicant->getId() );
						$this->setApplicantApplicationId( $objApplicant->getApplicantApplicationId() );
						$this->setDatabase( $objDatabase );

						if( false == $this->sendApplicationInvitationEmail( 'RENTAL_APPLICATION_FOR_PROPERTY_NAME', CSystemEmailType::PROSPECT_EMAILER, $objCompanyUser ) ) {
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Can not send invitation to occupant' ), NULL ) );
							return false;
						}
					} else {
						$objSystemEmail = $objApplicant->createSignInEmail( $strEmailType, $strSecureBaseName, $objPrimaryApplicant, $objProperty, $arrobjPropertyApplicationPreferences, $this, $objCompanyUser, $objDatabase, $objPropertyEmailRule );
					}
					CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
				} catch( Exception $objException ) {
					CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
				}

				if( true == valObj( $objSystemEmail, 'CSystemEmail' ) ) {
					$objSystemEmail->setId( $objSystemEmail->fetchNextId( $objEmailDatabase ) );
					$objEmailDatabase->close();
					array_push( $arrobjSystemEmails, $objSystemEmail );
				}

				if( true == in_array( $strEmailType, [ 'revoke_account', 'send_reminder', 'pre_screening_invitation' ] ) ) {

					// create event for primary applicant as outgoing Emails
					$objEventLibrary = new CEventLibrary();

					$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
					$objEventLibraryDataObject->setDatabase( $objDatabase );
					$objEventLibraryDataObject->setCustomer( $objApplicant->getCustomer() );

					$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::INVITATION_EMAIL );
					$objEvent->setCid( $this->getCid() );
					$objEvent->setCustomerId( $objApplicant->getCustomerId() );
					if( $strEmailType == 'application_invitation' && true == $boolIsShowApplicantInviteOnApplication ) {
						$arrmixData = [ 'is_application_invitation' => true ];
						$arrmixData['applicant_user_name'] = $objApplicant->getUsername();
						$objEventLibraryDataObject->setApplicant( $objApplicant );
						$objEventLibraryDataObject->setData( $arrmixData );
						$objEventLibrary->buildEventDescription( $this );
					}

					if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
						$objEvent->setCompanyUser( $objCompanyUser );
					}

					if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
						$objEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
						$objEvent->setCompanyEmployee( $objCompanyEmployee );
					}
					$objEvent->setEventDatetime( CEvent::getCurrentUtcTime() );
					$arrmixData = [ 'is_application_invitation' => true ];
					$objEventLibraryDataObject->setData( $arrmixData );
					$objEventLibrary->buildEventSystemNotes( $this );
					$objEvent->setDataReferenceId( $objSystemEmail->getId() );
					$objEventLibrary->getEventLibraryDataObject()->setEvent( $objEvent );
					$arrobjEventLibraries[] = $objEventLibrary;
				}
			}
		}

		// Make sure there is a lease document associated to this applicant
		if( 'lease_agreement' == $strEmailType && true == valObj( $objSecondaryApplicant, 'CApplicant' ) ) {
			$objApplicantApplication = $this->fetchApplicantApplicationByApplicantId( $objSecondaryApplicant->getId(), $objDatabase );

			if( true == is_null( $objApplicantApplication->getLeaseDocumentId() ) || false == is_numeric( $objApplicantApplication->getLeaseDocumentId() ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'You must associate a lease agreement to this applicant before emailing. Email not sent.' ), NULL ) );
				return false;
			}

			if( CApplicationStage::LEASE != $this->getApplicationStageId() && CApplicationStatus::STARTED != $this->getApplicationStatusId() ) {
				$this->setApplicationStageId( CApplicationStage::LEASE );
				$this->setApplicationStatusId( CApplicationStatus::STARTED );
				if( false == $this->update( SYSTEM_USER_ID, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to update application. Email not sent.' ), NULL ) );
					return false;
				}
			}
		}

		if( true == valArr( $arrobjApplicants ) ) {
			foreach( $arrobjApplicants as $objApplicant ) {
				if( false == $objApplicant->update( SYSTEM_USER_ID, $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'applicant record failed to update.' ), NULL ) );
					return false;
				}
			}
		}

		$arrobjSystemEmails = ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjSystemEmails, $objPropertyEmailRule );

		if( true == valArr( $arrobjSystemEmails ) ) {
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				if( false == is_null( $objSystemEmail ) ) {
					if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'System email failed to insert.' ), NULL ) );
						return false;
					}
				}
			}
		}

		if( true == valArr( $arrobjEventLibraries ) ) {
			foreach( $arrobjEventLibraries as $objEventLibrary ) {
				if( true == is_null( $objEventLibrary->getEventLibraryDataObject()->getEvent()->getEventHandle() ) ) {
					$objEventLibrary->buildEventDescription( $objEventLibrary->getEventLibraryDataObject()->getCustomer() );
				}

				$objEventLibrary->setIsExportEvent( $this->getIsExportEvent() );

				if( false == $objEventLibrary->insertEvent( SYSTEM_USER_ID, $objDatabase ) ) {
					$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
					return false;
				}
			}
		}

		if( true == $boolIsShowApplicantInviteOnApplication ) {
			$this->exportEventsStack( SYSTEM_USER_ID, $objDatabase );
		}

		if( 'lease_agreement' == $strEmailType && true == valObj( $objSecondaryApplicant, 'CApplicant' ) && true == valObj( $objApplicantApplication, 'CApplicantApplication' ) ) {
			$objApplicantApplication->setLeaseInvitationSent( 1 );
			if( false == $objApplicantApplication->update( SYSTEM_USER_ID, $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function sendApplicationIntegrationFalureEmails( $strSecureBaseName, $objDatabase, $objEmailDatabase = NULL ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objClient = $this->getOrFetchClient( $objDatabase );
		$objProperty = $this->getOrFetchProperty( $objDatabase );
		$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		$objMarketingMediaAssociation				= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );

		$objPropertyEmailRule = CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::PROSPECT_EMAILER, $objProperty->getId(), $objDatabase );

		// smarty assignment
		$objSmarty = new CPsSmarty( PATH_INTERFACES_PROSPECT_PORTAL, false );

		$objSmarty->assign( 'application',					$this );
		$objSmarty->assign( 'property',						$objProperty );
		$objSmarty->assign( 'client',			$objClient );
		$objSmarty->assign( 'marketing_media_association',	$objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file',	$objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'property_email_rule',			$objPropertyEmailRule );
		$objSmarty->assign( 'media_library_uri',			CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri',						CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'terms_and_condition_path',		$strSecureBaseName . '?module=miscellaneous_items&action=view_terms_and_conditions&application[id]=' . $this->getId() . '&company_application_id=' . $this->getCompanyApplicationId() );
		$objSmarty->assign( 'image_url',	                CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$arrobjSystemEmails = [];
		$objSystemEmail		= new CSystemEmail();

		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::PROSPECT_EMAILER );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::NORMAL );
		$objSystemEmail->setSubject( 'Application Integration Failure Email.' );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		$arrstrPropertyEmailAddresses = ( array ) $objProperty->fetchApplicationNotificationEmails( $objDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrPropertyEmailAddresses ) ) {

			foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {
				$objSystemEmail->setToEmailAddress( $strEmailAddress );

				$strManagerHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/application_integration_failure_email.tpl' );

				if( 0 < strlen( $strManagerHtmlEmailContent ) ) {
					$objSystemEmail->setHtmlContent( $strManagerHtmlEmailContent );
				}

				$objCurrentSystemEmail 	= clone $objSystemEmail;

				$objCurrentSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

				$arrobjSystemEmails[] 	= $objCurrentSystemEmail;
			}
		}

		$arrobjSystemEmails = ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjSystemEmails, $objPropertyEmailRule );

		if( true == valArr( $arrobjSystemEmails ) ) {
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				$strToEmailAddress = trim( $objSystemEmail->getToEmailAddress() );
				if( true == empty( $strToEmailAddress ) ) {
					continue;
				}

				if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
					continue;
				}
			}
		}
	}

	public function resendAdverseActionScreeningEmails( $intCompanyUserId, $arrobjApplicants, $strAdverseActionHtmlContent, $arrobjApplicantApplications, $objDatabase, $objEmailDatabase = NULL ) {

		$objClient 					= $this->getOrFetchClient( $objDatabase );
		$objProperty 				= $this->getOrFetchProperty( $objDatabase );
		$objPropertyEmailRule		= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::PROSPECT_EMAILER, $objProperty->getId(), $objDatabase );

		$arrobjPropertyPreferences 		= ( array ) $objProperty->fetchPropertyPreferences( $objDatabase );
		$arrobjApplicantSystemEmails	= [];
		$arrobjManagerSystemEmails		= [];

		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::PROSPECT_EMAILER );
		$objSystemEmail->setSubject( 'Application Screening Decision' );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		$arrobjLatestApplicantApplicationTransmissions		= $this->fetchApplicantApplicationTransmissionsByApplicantApplicationIds( array_keys( $arrobjApplicantApplications ), $objDatabase );

		if( false == valArr( $arrobjLatestApplicantApplicationTransmissions ) ) return;

		foreach( $arrobjApplicants as $objApplicant ) {

			if( false == valStr( $objApplicant->getUsername() ) ) continue;

			$objCurrentSystemEmail = clone $objSystemEmail;

			$objCurrentSystemEmail->setToEmailAddress( $objApplicant->getUsername() );
			$objCurrentSystemEmail->setHtmlContent( $strAdverseActionHtmlContent );
			$objCurrentSystemEmail->setApplicantId( $objApplicant->getId() );
			$arrobjApplicantSystemEmails[] = $objCurrentSystemEmail;
		}

		$arrstrPropertyEmailAddresses = ( array ) $objProperty->fetchApplicationNotificationEmails( $objDatabase );

		// Mail to manager
		if( true == valArr( $arrstrPropertyEmailAddresses ) ) {

			if( true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) {
				$objSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() );
			} else {
				$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
			}

			foreach( $arrobjApplicants as $objApplicant ) {
				foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {
					if( false == valStr( $strEmailAddress ) )
						continue;

					$objManagerSystemEmail = clone $objSystemEmail;

					$objManagerSystemEmail->setToEmailAddress( $strEmailAddress );
					$objManagerSystemEmail->setHtmlContent( $strAdverseActionHtmlContent );
					$objManagerSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

					$arrobjManagerSystemEmails[] = $objManagerSystemEmail;
				}
			}
		}

		return $this->logAndSendAdverseActionScreeningEmails( $intCompanyUserId, $objDatabase, $arrobjApplicantSystemEmails, $arrobjLatestApplicantApplicationTransmissions, $arrobjManagerSystemEmails, $objPropertyEmailRule, $objEmailDatabase );
	}

	public function logAndSendAdverseActionScreeningEmails( $intCompanyUserId, $objDatabase, $arrobjApplicantSystemEmails, $arrobjLatestApplicantApplicationTransmissions, $arrobjManagerSystemEmails, $objPropertyEmailRule, $objEmailDatabase, $boolAttachAdverseActionLetter = false, $arrobjScreeningApplicantResults = NULL, $arrobjEventLibrary = NULL ) {

		switch( NULL ) {
			default:
				$objDatabase->begin();

				$intEmailSuccessCount = 0;

				$arrobjApplicantSystemEmails	= ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjApplicantSystemEmails, $objPropertyEmailRule );
				$arrobjManagerSystemEmails		= ( array ) CSystemEmailLibrary::processSystemEmailsByPropertyEmailRules( $arrobjManagerSystemEmails, $objPropertyEmailRule );

				if( false == valArr( $arrobjApplicantSystemEmails ) ) {
					break;
				}

				$arrintApplicantSystemEmailIds = [];
				foreach( $arrobjApplicantSystemEmails as $objSystemEmail ) {
					$objSystemEmail->setId( $objSystemEmail->fetchNextId( $objEmailDatabase ) );
					$objEmailDatabase->close();

					if( true == $boolAttachAdverseActionLetter ) {
						$objEmailAttachment = $this->createEmailAttachment( $intCompanyUserId, $objSystemEmail->getApplicantId(), $objDatabase, $objEmailDatabase );
						if( valObj( $objEmailAttachment, CEmailAttachment::class ) ) {
							$objSystemEmail->addEmailAttachment( $objEmailAttachment );
						}
					}

					if( false == $objSystemEmail->insert( $intCompanyUserId, $objEmailDatabase ) ) {
						$objDatabase->rollback();
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'System Email record failed to insert.' ), NULL ) );
						break;
					}

					if( valObj( $objEmailAttachment, CEmailAttachment::class ) ) {
						unlink( $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName() );
					}

					$intEmailSuccessCount++;
					$arrintApplicantSystemEmailIds[$objSystemEmail->getApplicantId()] = $objSystemEmail->getId();

				}

				if( true == valArr( $arrobjScreeningApplicantResults ) ) {
					$strSql = '';

					foreach( $arrobjScreeningApplicantResults as $objScreeningApplicantResult ) {
						$objScreeningApplicantResult->setAdverseActionLetterSentOn( 'NOW()' );

						$strSql .= $objScreeningApplicantResult->update( $intCompanyUserId, $objDatabase, true );
					}

					if( true == valStr( $strSql ) && false == valArr( executeSql( $strSql, $objDatabase ) ) ) {
						$objDatabase->rollback();
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to update screening applicant result.' ), NULL ) );
					}
				}

				if( true == valArr( $arrobjManagerSystemEmails ) ) {
					foreach( $arrobjManagerSystemEmails as $objSystemEmail ) {

						if( false == $objSystemEmail->insert( $intCompanyUserId, $objEmailDatabase ) ) {
							$objEmailDatabase->rollback();
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'System Email record failed to insert.' ), NULL ) );
							break;
						}
					}
				}

				foreach( $arrobjLatestApplicantApplicationTransmissions as $objApplicantApplicationTransmission ) {

					$objApplicantApplicationTransmission->setAdverseEmailedOn( 'NOW()' );

					if( false == $objApplicantApplicationTransmission->update( $intCompanyUserId, $objDatabase ) ) {
						$objDatabase->rollback();
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'applicant record failed to update.' ), NULL ) );
						break;
					}
				}

				if( true == valArr( $arrobjEventLibrary ) && true == valArr( $arrintApplicantSystemEmailIds ) ) {
					foreach( $arrobjEventLibrary as $intApplicantId => $objEventLibrary ) {

						$objEventLibrary->getEventLibraryDataObject()->getEvent()->setDataReferenceId( $arrintApplicantSystemEmailIds[$intApplicantId] );
						$objEventLibrary->buildEventDescription( $objEventLibrary->getEventLibraryDataObject()->getApplication() );
						$objEventLibrary->setIsExportEvent( true );

						if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
							$objDatabase->rollback();
							$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to insert activity log.' ), NULL ) );
							break;
						}
					}
				}

				$objDatabase->commit();
		}

		return $intEmailSuccessCount;
	}

	public function sendAdverseActionScreeningEmails( $intCompanyUserId, $arrmixTransmissionConditionDetails, $arrstrCreditScoreDetails, $arrobjApplicants, $arrobjApplicantApplications, $objDatabase, $objEmailDatabase = NULL, $boolIncludeDeletedAAL = false ) {
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objClient 					= $this->getOrFetchClient( $objDatabase );
		$objProperty 				= $this->getOrFetchProperty( $objDatabase );
		$objMarketingMediaAssociation	= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		$objPropertyInfo 			= $this->fetchPropertyAndLoadDisplayInfo( $objDatabase );
		$strSecureBaseName 			= $this->generateWebsiteUrl( $objDatabase );
		$objPropertyEmailRule		= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::PROSPECT_EMAILER, $objProperty->getId(), $objDatabase );

		// Loading admin database to fetch company info
		$objAdminDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::ADMIN, CDatabaseUserType::ONLINEAPPLICATION );

		if( true == valObj( $objAdminDatabase, 'CDatabase' ) ) {
			$arrmixCompanyAddress = \Psi\Eos\Admin\CAccounts::createService()->fetchCompanyBillingAddressByCidByAccountTypeId( $objClient->getId(), CAccountType::PRIMARY, $objAdminDatabase );
			$objAdminDatabase->close();
		} else {
            $arrmixCompanyAddress = NULL;
		}

		// check if application is screened using package setting
		$boolFetchSatisfiedCOnditionOnly = ( true == $arrmixTransmissionConditionDetails['offer_conditions_to_applicant'] ) ? false : true;
       $arrobjScreeningApplicationConditionSets   = $this->getScreeningApplicationConditions( $objDatabase, $boolFetchSatisfiedCOnditionOnly );
        $arrstrScreeningPackageConditionNames     = [];

		// loading screening application request to determine screening decision
		$objScreeningApplicationRequest = $this->fetchScreeningApplicationRequest( $objDatabase );
		$intRemoteScreeningId			= ( true == valObj( $objScreeningApplicationRequest, 'CScreeningApplicationRequest' ) ) ? $objScreeningApplicationRequest->getRemoteScreeningId() : NULL;
		$intScreeningDecisionTypeId 	= $arrmixTransmissionConditionDetails['screening_decision_type_id'];

       if( true == valArr( $arrobjScreeningApplicationConditionSets ) && CScreeningDecisionType::APPROVE_WITH_CONDITIONS == $intScreeningDecisionTypeId ) {
            $intConditionCounter = 0;
            foreach( $arrobjScreeningApplicationConditionSets as $objScreeningApplicationConditionSet ) {
                $arrstrScreeningPackageConditionNames[$intConditionCounter] = $objScreeningApplicationConditionSet->getConditionName();

                $intConditionCounter++;
            }
       }

		$arrobjTransmissionConditions 						= ( CScreeningDecisionType::APPROVE_WITH_CONDITIONS == $intScreeningDecisionTypeId ) ? ( array ) $this->fetchTransmissionConditions( $objDatabase ) : [];
		$arrobjPropertyPreferences 							= ( array ) $objProperty->fetchPropertyPreferences( $objDatabase );
		$arrobjTransmissionResponseTypes 					= ( array ) CTransmissionResponseTypes::fetchAllTransmissionResponseTypes( $objDatabase );

		$boolIsShowCompanyInfoOnAaLetter					= array_key_exists( 'SHOW_COMPANY_INFO_ON_AA_LETTER', $arrobjPropertyPreferences ) ? ( bool ) $arrobjPropertyPreferences['SHOW_COMPANY_INFO_ON_AA_LETTER']->getValue() : false;

		$arrobjLatestApplicantApplicationTransmissions		= $this->fetchApplicantApplicationTransmissionsByApplicantApplicationIds( array_keys( $arrobjApplicantApplications ), $objDatabase );

		if( false == valArr( $arrobjLatestApplicantApplicationTransmissions ) ) return;

		foreach( $arrobjTransmissionConditions as $objTransmissionCondition ) {
			$objTransmissionCondition->loadTransmisssionConditionName( $arrobjPropertyPreferences );
		}

		// Handle resident verify credit score details.
		$arrstrCreditScoreDetails 						= ( true == valArr( $arrstrCreditScoreDetails ) ) ? $arrstrCreditScoreDetails : [];
		$arrobjLatestApplicantApplicationTransmissions	= rekeyObjects( 'ApplicantApplicationId', $arrobjLatestApplicantApplicationTransmissions );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_PROSPECT_PORTAL, false );

		CScreeningDecisionType::assignSmartyConstants( $objSmarty );

		$objSmarty->assign( 'application',						$this );
		$objSmarty->assign( 'property',							$objProperty );
		$objSmarty->assign( 'property_details',					$objPropertyInfo->getPrimaryPropertyAddress() );
		$objSmarty->assign( 'screening_id',						$intRemoteScreeningId );

		$objSmarty->assign( 'marketing_media_association',		$objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file',		$objLogoMarketingMediaAssociation );
		$objSmarty->assign( 'client',							$objClient );

		$objSmarty->assign( 'applicants',						$arrobjApplicants );
		$objSmarty->assign( 'applicant_applications',			rekeyObjects( 'ApplicantId', $arrobjApplicantApplications ) );
		$objSmarty->assign( 'transmission_conditions',			$arrobjTransmissionConditions );
		$objSmarty->assign( 'transmission_response_types', 		$arrobjTransmissionResponseTypes );

		$objSmarty->assign( 'transmission_condition_details', 	$arrmixTransmissionConditionDetails );

        $objSmarty->assign( 'screening_package_condition_names', $arrstrScreeningPackageConditionNames );

		$objSmarty->assign( 'show_company_info_on_aa_letter', 	$boolIsShowCompanyInfoOnAaLetter );

		$objSmarty->assign( 'company_details', 					$arrmixCompanyAddress );

		$objSmarty->assign( 'property_email_rule',				$objPropertyEmailRule );
		$objSmarty->assign( 'media_library_uri',				CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri',							CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'terms_and_condition_path',			$strSecureBaseName . '?module=miscellaneous_items&action=view_terms_and_conditions&application[id]=' . $this->getId() . '&company_application_id=' . $this->getCompanyApplicationId() );
		$objSmarty->assign( 'image_url',	                    CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		CApplicationStage::assignSmartyConstants( $objSmarty );
		CApplicationStatus::assignSmartyConstants( $objSmarty );
		CCustomerType::assignSmartyConstants( $objSmarty );

		$arrobjApplicantSystemEmails	= [];
		$arrobjManagerSystemEmails		= [];

		$strEmailSubject    = 'Application Screening Decision';
		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::PROSPECT_EMAILER );
		$objSystemEmail->setSubject( $strEmailSubject );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		// Mail to applicants
		$strHtmlEmailOutput = [];
		$arrmixCustomers = [];
		if( true == valArr( $arrobjApplicants ) ) {
			$arrmixCustomers = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomCustomerPreferredLocaleCodeByIdsByCid( array_keys( rekeyObjects( 'CustomerId', $arrobjApplicants ) ), $objClient->getId(), $objDatabase );
		}
		foreach( $arrobjApplicants as $objApplicant ) {
			$objSmarty->assign( 'current_applicant', $objApplicant );
			// change made for Resident Screening only

			if( true == valArr( $arrobjLatestApplicantApplicationTransmissions ) && array_key_exists( $objApplicant->getApplicantApplicationId(), $arrobjLatestApplicantApplicationTransmissions ) ) {
				$objSmarty->assign( 'latest_applicant_application_transmission', $arrobjLatestApplicantApplicationTransmissions[$objApplicant->getApplicantApplicationId()] );
			}

			$intCreditScore = ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['creditScore'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['creditScore'] : ( ( isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['creditScore'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getId()]['creditScore'] : NULL );
			$boolIsNoCreditRecord = ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['NoCreditRecord'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['NoCreditRecord'] : false;
			$boolIsNoCreditScore = ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['NoCreditScore'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['NoCreditScore'] : false;
			$strCreditScoreMsg = '';

			if( true == $boolIsNoCreditRecord ) {
				$strCreditScoreMsg = 'No Credit Record';
			} elseif( true == $boolIsNoCreditScore ) {
				$strCreditScoreMsg = 'No Credit Score';
			}

			$objSmarty->assign( 'credit_score',					$intCreditScore );
			$objSmarty->assign( 'credit_score_msg',				$strCreditScoreMsg );
			$objSmarty->assign( 'primary_key_factor_points',	( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['keyFactorPoints'] ) ) ? explode( '~', $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['keyFactorPoints'] ) : ( ( isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['keyFactorPoints'] ) ) ? explode( '~', $arrstrCreditScoreDetails[$objApplicant->getId()]['keyFactorPoints'] ) : '-' ) );

			$strCreditScoreModelType = ( true == isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['creditScoreModelType'] ) ) ? \Psi\CStringService::singleton()->strtolower( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['creditScoreModelType'] ) : ( ( true == isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['creditScoreModelType'] ) ) ? \Psi\CStringService::singleton()->strtolower( $arrstrCreditScoreDetails[$objApplicant->getId()]['creditScoreModelType'] ) : '' );

			$arrstrCreditModelTypeDescription = [ 'vantage score v3', 'experianvantagescorev3', 'v3' ];
			$arrstrEquifaxCreditModelTypes    = [ 'beacon', 'beacon 3.0', 'beacon 4.0' ];
			$arrstrTazworkCreditModelTypes    = [ 'experianfairisaac' ];

			$strCreditScoreRange = '-';

			// use credit range on the base of model type
			if( true == valStr( $strCreditScoreModelType ) ) {
				if( true == in_array( $strCreditScoreModelType, $arrstrCreditModelTypeDescription ) ) {
					$strCreditScoreRange = '300 - 850';
				} elseif( in_array( strtolower( $strCreditScoreModelType ), $arrstrEquifaxCreditModelTypes ) ) {
					$strCreditScoreRange = '300 - 900';
				} elseif( in_array( strtolower( $strCreditScoreModelType ), $arrstrTazworkCreditModelTypes ) ) {
					$strCreditScoreRange = '501 - 990';
				}
			}
			$intScreeningId = ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['screeningId'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['screeningId'] : ( ( isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['screeningId'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getId()]['screeningId'] : '-' );
			$intScreeningApplicantId = ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['screeningApplicantId'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['screeningApplicantId'] : ( ( isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['screeningApplicantId'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getId()]['screeningApplicantId'] : '-' );

			$intRvIndexScore = ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['rvIndexScore'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['rvIndexScore'] : ( ( isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['rvIndexScore'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getId()]['rvIndexScore'] : NULL );
			$strRvIndexScoreMsg = '';

			if( false == valId( $intRvIndexScore ) ) {
				$strRvIndexScoreMsg = 'No RV Index Score';
			}

			$objSmarty->assign( 'credit_score_range',               $strCreditScoreRange );
			$objSmarty->assign( 'rv_index_score',				    $intRvIndexScore );
			$objSmarty->assign( 'rv_index_score_msg',			    $strRvIndexScoreMsg );
			$objSmarty->assign( 'score_model_name',                 ucfirst( $strCreditScoreModelType ) );
			$objSmarty->assign( 'screening_id',				  	    $intScreeningId );
			$objSmarty->assign( 'screening_applicant_id',	  	    $intScreeningApplicantId );
			$objSmarty->assign( 'is_use_for_rv_index_score', 	    ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['isUseForRvIndexScore'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['isUseForRvIndexScore'] : ( ( isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['isUseForRvIndexScore'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getId()]['isUseForRvIndexScore'] : '-' ) );
			$objSmarty->assign( 'transmission_response_date_time',  ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['creditScreeningCompletedOn'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['creditScreeningCompletedOn'] : ( ( isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['creditScreeningCompletedOn'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getId()]['creditScreeningCompletedOn'] : '-' ) );
			$objSmarty->assign( 'is_credit_search', 	            ( isset( $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['isCreditSearch'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getApplicantApplicationId()]['isCreditSearch'] : ( ( isset( $arrstrCreditScoreDetails[$objApplicant->getId()]['isCreditSearch'] ) ) ? $arrstrCreditScoreDetails[$objApplicant->getId()]['isCreditSearch'] : false ) );

			if( 'CA' == $objProperty->getCountryCode() ) {
				$strTemplate = 'system_emails/applications/ca/adverse_action_letter_email.tpl';
			} else {
				$strTemplate = 'system_emails/applications/adverse_action_letter_email.tpl';
			}

			try {
				if( true == array_key_exists( $objApplicant->getCustomerId(), $arrmixCustomers ) ) {
					CLocaleContainer::createService()->setPreferredLocaleCode( $arrmixCustomers[$objApplicant->getCustomerId()], $objDatabase, $objProperty );
				}
				$strHtmlEmailOutput[$objApplicant->getId()] = $objSmarty->fetch( PATH_INTERFACES_COMMON . $strTemplate );
				CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
			} catch( Exception $objException ) {
				CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
			}

			if( false == valStr( $objApplicant->getUsername() ) ) continue;

			$objCurrentSystemEmail = clone $objSystemEmail;

			$objCurrentSystemEmail->setToEmailAddress( $objApplicant->getUsername() );
			$objCurrentSystemEmail->setHtmlContent( $strHtmlEmailOutput[$objApplicant->getId()] );
			$objCurrentSystemEmail->setApplicantId( $objApplicant->getId() );
			$arrobjApplicantSystemEmails[] = $objCurrentSystemEmail;
		}

		$arrstrPropertyEmailAddresses = $objProperty->fetchApplicationNotificationEmails( $objDatabase );

		// Mail to manager
		if( true == valArr( $arrstrPropertyEmailAddresses ) ) {

			if( true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) {
				$objSystemEmail->setFromEmailAddress( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() );
			} else {
				$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
			}

			foreach( $arrobjApplicants as $objApplicant ) {
				foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {
					if( false == valStr( $strEmailAddress ) )
						continue;

					$objManagerSystemEmail = clone $objSystemEmail;

					$objManagerSystemEmail->setToEmailAddress( $strEmailAddress );
					$objManagerSystemEmail->setHtmlContent( $strHtmlEmailOutput[$objApplicant->getId()] );
					$objManagerSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

					$arrobjManagerSystemEmails[] = $objManagerSystemEmail;
				}
			}
		}

		// Create application adverse action file
		// gerenerte adverse action letter file

		$arrintApplicantApplications = $this->fetchApplicantApplicationsByApplicantIds( array_keys( $arrobjApplicants ), $objDatabase, $boolIncludeDeletedAAL );
		$this->loadDocumentManager( $objDatabase, $intCompanyUserId );

		if( true == valArr( $arrintApplicantApplications ) ) {
			foreach( $arrintApplicantApplications as $objApplicantApplication ) {
				if( true == valStr( $strHtmlEmailOutput[$objApplicantApplication->getApplicantId()] ) && false == $objApplicantApplication->generateDocumentFile( $strHtmlEmailOutput[$objApplicantApplication->getApplicantId()], $this, $objDatabase, $objApplicantApplication->getApplicantId() ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to generate the adverse action file.' ), NULL ) );
				}
			}
		}

		return $this->logAndSendAdverseActionScreeningEmails( $intCompanyUserId, $objDatabase, $arrobjApplicantSystemEmails, $arrobjLatestApplicantApplicationTransmissions, $arrobjManagerSystemEmails, $objPropertyEmailRule, $objEmailDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Company property id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompanyApplicationId( $objSession ) {
		$boolIsValid = true;

		$intCompanyApplication = valObj( $objSession, 'CSession' ) ? $objSession->getValue( 'company_application' ) : NULL;

		if( true == isset( $intCompanyApplication ) ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->getCompanyApplicationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Application type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDocumentId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valLeadSourceId( $objDatabase ) {
		if( true == is_numeric( $this->getLeadSourceId() ) && 0 < $this->getLeadSourceId() ) {
			return true;
		}

		$boolIsValid = true;
		$intLeadSourcesCount = 0;

		if( true == is_null( $this->getCurrentApplicationStepId() ) ) {
			$intCurrentApplicationStepId = CApplicationStep::UNIT_INFO;
		} else {
			$intCurrentApplicationStepId = CApplicationStep::BASIC_INFO;
		}

		if( true == valObj( $this->getProperty(), 'CProperty' ) ) {
			$intLeadSourcesCount = $this->getProperty()->getOrFetchPropertyLeadSourcesCount( $objDatabase );
		} elseif( true == is_numeric( $this->getPropertyId() ) && 0 < $this->getPropertyId() ) {
			$intLeadSourcesCount = \Psi\Eos\Entrata\CLeadSources::createService()->fetchPublishedLeadSourcesCountByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		if( 0 < $intLeadSourcesCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'originating_lead_source_id', __( 'Lead source is required.' ), '', [ 'label' => __( 'Preferences' ), 'step_id' => $intCurrentApplicationStepId ] ) );
		}

		return $boolIsValid;
	}

	public function valLeasingAgentId( $objDatabase ) {
		if( true == is_numeric( $this->getLeasingAgentId() ) && 0 < $this->getLeasingAgentId() ) {
			return true;
		}

		$boolIsValid = true;

		$intLeasingAgentsCount = \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchActivePropertyLeasingAgentsCountByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( 0 < $intLeasingAgentsCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'leasing_agent_id', __( 'Leasing agent is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDesiredRentMin() {
		$boolIsValid = true;

		if( true == is_null( $this->getDesiredRentMin() ) || 0 == $this->getDesiredRentMin() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'desired_rent_min', __( 'Desired minimum rent is required.' ), '', [ 'label' => __( 'Preferences' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valDesiredRentMax( $boolCanBeNull = false ) {
		$boolIsValid = true;

		if( false == $boolCanBeNull ) {
			if( true == is_null( $this->getDesiredRentMax() ) || 0 == $this->getDesiredRentMax() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'desired_rent_max', __( 'Desired maximum rent is required.' ), '', [ 'label' => __( 'Preferences' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}
		}

		if( false == is_null( $this->getDesiredRentMax() ) && ( 0 < $this->getDesiredRentMax() || 0 == $this->getDesiredRentMax() ) && $this->getDesiredRentMax() < $this->getDesiredRentMin() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'desired_rent_max', __( 'Desired maximum rent can\'t be less than the desired minimum rent.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTermMonth( $objDatabase = NULL, $boolIsUnitInfo = false, $boolIsValidateRequired = true ) {
		$boolIsValid = true;
		$arrobjLeaseTerms = [];

		if( true == is_null( $this->getCurrentApplicationStepId() ) ) {
			$intCurrentApplicationStepId = CApplicationStep::UNIT_INFO;
		} else {
			$intCurrentApplicationStepId = CApplicationStep::BASIC_INFO;
		}

		if( ( true == is_null( $this->getTermMonth() ) || 0 == $this->getTermMonth() ) ) {
			if( true == valObj( $objDatabase, 'CDatabase' ) ) {
				$arrobjLeaseTerms = $this->getOrFetchProperty( $objDatabase )->fetchLeaseTerms( $objDatabase );
			}
			if( false == valArr( $arrobjLeaseTerms ) && false == empty( $arrobjLeaseTerms ) ) {
				$boolIsValid = true;
			} else {
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$this->addErrorMsg( ( true == $boolIsUnitInfo ) ? new CErrorMsg( ERROR_TYPE_VALIDATION, 'term_month', __( 'Lease Term is required.' ) ) : new CErrorMsg( ERROR_TYPE_VALIDATION, 'term_month_basic', __( 'Desired lease length is required.' ), '', [ 'label' => __( 'Preferences' ), 'step_id' => $intCurrentApplicationStepId ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valLeaseStartDate( $boolCanBeNull = false, $boolValidateMoveInDateForFuture = true, $arrobjPropertyPreferences = NULL, $objDatabase = NULL, $boolIsBlackOutDateOverride = false, $boolIsValidateRequired = true ) {

		$boolIsValid = true;

		$intLength = strlen( trim( $this->getLeaseStartDate() ) );

		if( true == is_null( $this->getCurrentApplicationStepId() ) ) {
			$intCurrentApplicationStepId = CApplicationStep::UNIT_INFO;
		} else {
			$intCurrentApplicationStepId = CApplicationStep::BASIC_INFO;
		}

		$arrobjPropertyPreferences = ( false == valArr( $arrobjPropertyPreferences ) ) ? [] : $arrobjPropertyPreferences;

		if( false == $boolCanBeNull && false == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
			if( ( true == is_null( $this->getLeaseStartDate() ) || 0 == $intLength ) ) {
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date is required.' ), '', [ 'label' => __( 'Lease Start Date' ), 'step_id' => $intCurrentApplicationStepId ] ) );
			}
		}
		// @FIXME: For affordable we need to decide lease start validation, once logic in place we will remove if condition.
		if( false == in_array( $this->getOccupancyTypeId(), [ COccupancyType::AFFORDABLE, COccupancyType::HOSPITALITY ] ) ) {
			if( 0 < $intLength && false == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
				if( false == CValidation::validateDate( $this->getLeaseStartDate() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must be in mm/dd/yyyy form.' ), '', [ 'label' => __( 'Lease Start Date' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
				} elseif( strtotime( $this->getLeaseStartDate() ) < strtotime( date( 'm/d/Y' ) ) && true == $boolValidateMoveInDateForFuture ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must not be in the past.' ), '', [ 'label' => __( 'Lease Start Date' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
				} elseif( false == is_null( $arrobjPropertyPreferences ) && true == array_key_exists( 'FUTURE_MOVE_IN_DAY_LIMIT', $arrobjPropertyPreferences ) && 0 < ( int ) $arrobjPropertyPreferences['FUTURE_MOVE_IN_DAY_LIMIT']->getValue() ) {

					$mixMaxFutureMoveInDateLimit = $arrobjPropertyPreferences['FUTURE_MOVE_IN_DAY_LIMIT']->getValue();

					if( ( true === is_numeric( $mixMaxFutureMoveInDateLimit ) && ( floor( abs( ( strtotime( $this->getLeaseStartDate() ) - time() ) / ( 24 * 60 * 60 ) ) ) >= $mixMaxFutureMoveInDateLimit ) ) || ( true !== is_numeric( $mixMaxFutureMoveInDateLimit ) && ( strtotime( $this->getLeaseStartDate() ) > strtotime( $mixMaxFutureMoveInDateLimit ) ) ) ) {
						$boolIsValid       = false;
						$strMaxDateAllowed = ( true === is_numeric( $mixMaxFutureMoveInDateLimit ) ) ? strtotime( '+' . $mixMaxFutureMoveInDateLimit . ' days' ) : strtotime( $mixMaxFutureMoveInDateLimit );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Max. desired move-in date allowed is' ) . ' ' . ( date( 'm/d/Y', $strMaxDateAllowed ) ), '', [ 'label' => __( 'Lease Start Date' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
					}
				} elseif( false == $boolIsBlackOutDateOverride ) {
					if( false == is_null( $objDatabase ) && true == valObj( $objDatabase, 'CDatabase' ) && CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() && false == CApplicationUtils::resetLeaseStartDateForBlackoutDates( $this, $objDatabase, $arrobjPropertyPreferences, $boolIsResetDate = false ) ) {

						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Selected desired move-in date is blacked out.' ), '', [ 'label' => __( 'Lease Start Date' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
					}
				}
			}
		} else {
			if( false == CValidation::validateDate( $this->getLeaseStartDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Desired move-in date must be in mm/dd/yyyy form.' ), '', [ 'label' => __( 'Lease Start Date' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valRequestedScheduleTourDate( $strDate, $boolIsValidateRequired = true ) {
		$boolIsValid	= true;
		$intLength		= strlen( trim( $strDate ) );

		if( true == is_null( $strDate ) || 0 == $intLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_date', __( 'Requested date is required.' ) ) );
		} elseif( true == $boolIsValidateRequired && false == CValidation::validateDate( trim( $strDate ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_date', __( 'Requested date must be in mm/dd/yyyy form.' ) ) );
		} elseif( true == $boolIsValidateRequired && strtotime( trim( $strDate ) ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_date', __( 'Requested date should not be past date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseTermId( $objDatabase, $boolIsValidateRequired = true ) {

		$boolIsValid = true;

		if( false == is_null( $this->getLeaseTermId() ) ) {
			$intUnsetLeaseTermId = CLeaseTerms::fetchUnsetLeaseTermIdByCid( $this->getCid(), $objDatabase, true, true );
		}
		if( ( true == is_null( $this->getLeaseTermId() ) || false == is_numeric( $this->getLeaseTermId() ) || ( false == is_null( $intUnsetLeaseTermId ) && $this->getLeaseTermId() == $intUnsetLeaseTermId ) ) && true == $boolIsValidateRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_term_id', __( 'Lease term is required.' ), '', [ 'label' => __( 'Preferences' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valCallId() {
		$boolIsValid = true;

		if( true == $this->getIsValidateCallId() && 0 == strlen( $this->getCallId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_id', __( 'Call ID Number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valLeaseEndDate() {
		$boolIsValid = true;

		if( 0 != strlen( $this->getLeaseEndDate() ) && false == CValidation::validateDate( $this->getLeaseEndDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_end_date', __( 'Lease end date must be in mm/dd/yyyy form.' ) ) );

		} elseif( true == $boolIsValid && 0 != strlen( $this->getLeaseStartDate() ) && 0 != strlen( $this->getLeaseEndDate() ) ) {

			if( ( strtotime( $this->getLeaseEndDate() ) ) <= ( strtotime( $this->getLeaseStartDate() ) ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_start_date', __( 'Move-in date must be less than lease end date.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDesiredBedrooms() {
		$boolIsValid = true;

		if( true == is_null( $this->getDesiredBedrooms() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'desired_bedrooms', __( 'Desired bedrooms is required.' ), '', [ 'label' => __( 'Preferences' ), 'step_id' => $intCurrentApplicationStepId ] ) );
		}

		return $boolIsValid;
	}

	public function valDesiredBathrooms() {
		$boolIsValid = true;

		if( false == is_null( $this->m_fltDesiredBathrooms ) ) {
			if( true == is_numeric( $this->m_fltDesiredBathrooms ) ) {
				if( 0 > $this->m_fltDesiredBathrooms ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms must be blank or greater than zero' ) ) );

				} elseif( false == in_array( fmod( $this->m_fltDesiredBathrooms, 1 ), [ 0, 0.25, 0.5, 0.75 ] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
				}
			} else {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDesiredFloorplans() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyFloorplanId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan_id', __( 'Desired floorplans is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDesiredSpaceConfigurationId() {
		$boolIsValid = true;

		$strWhereSql = 'WHERE
								CID = ' . ( int ) $this->getCid() . '
								AND property_id = ' . ( int ) $this->getPropertyId() . '
								AND ar_origin_id = ' . ( int ) CArOrigin::BASE . '
								AND use_space_configurations_for_rent = true';

		$intPropertyArOriginRuleCount = ( int ) \Psi\Eos\Entrata\CPropertyArOriginRules::createService()->fetchRowCount( $strWhereSql, 'property_ar_origin_rules', $this->m_objDatabase );

		if( $intPropertyArOriginRuleCount > 0 && true == is_null( $this->getSpaceConfigurationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'space_configuration_id', __( 'Space Configuration is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valUnitSpaceId( $arrobjPropertyPreferences = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitSpaceId() ) && true == array_key_exists( 'FORCE_UNIT_SPACE_SELECTION', $arrobjPropertyPreferences ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', __( 'Apartment is required.' ), '', [ 'label' => __( 'Unit Space' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valPropertyFloorplanId( $arrobjPropertyPreferences = NULL, $boolIsStudentProperty = false, $objDatabase = NULL, $boolIsValidateRequired = false ) {
		$boolIsValid = true;
		if( true == is_null( $this->getPropertyFloorplanId() ) && ( ( true == $boolIsStudentProperty && CApplicationStage::PRE_APPLICATION != $this->getApplicationStageId() ) || ( false == $boolIsStudentProperty && true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'FORCE_FLOORPLAN_SELECTION', $arrobjPropertyPreferences ) ) ) ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan_id', __( 'Floor plan is required.' ), '', [ 'label' => __( 'Property Floorplan' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
		}

		if( true == $boolIsStudentProperty && false == is_null( $this->getPropertyFloorplanId() ) && true == valObj( $objDatabase, 'CDatabase' ) && true == $boolIsValidateRequired ) {
			// Validating conventional space configuration only if use space configuration for rent is on
			$strWhereSql = 'WHERE
								CID = ' . ( int ) $this->getCid() . '
								AND property_id = ' . ( int ) $this->getPropertyId() . '
								AND ar_origin_id = ' . ( int ) CArOrigin::BASE . '
								AND use_space_configurations_for_rent = true';

			$intPropertyArOriginRuleCount = ( int ) \Psi\Eos\Entrata\CPropertyArOriginRules::createService()->fetchRowCount( $strWhereSql, 'property_ar_origin_rules', $objDatabase );

			if( $intPropertyArOriginRuleCount > 0 && ( CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL == $this->getSpaceConfigurationId() || CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL == $this->getDesiredSpaceConfigurationId() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan_id', __( 'Floorplan is required.' ), '', [ 'label' => __( 'Property Floorplan' ), 'step_id' => CApplicationStep::UNIT_INFO ] ) );
				trigger_error( __( 'Convential space configuration is associated to student property.' ), E_USER_WARNING );
			}

		}

		return $boolIsValid;
	}

	public function valVerificationKey( $boolIsBypassCaptcha = false, $objSession = NULL ) {
		$boolIsValid = true;
		if( CCloud::REFERENCE_ID_CHINA === CConfig::get( 'cloud_reference_id' ) ) {
			return $boolIsValid;
		}

		if( true == $boolIsBypassCaptcha && 'yyy' == $this->m_strVerificationKey ) {
			return $boolIsValid;
		}

		if( ( true == isset( $GLOBALS['is_internal_request'] ) && 1 == $GLOBALS['is_internal_request'] && isset( $_COOKIE['SKIP_CAPTURE_IMAGE'] ) && 1 == $_COOKIE['SKIP_CAPTURE_IMAGE'] ) || true == in_array( $this->getCid(), CClients::$c_arrintMacroScriptCids ) ) {
			return $boolIsValid;
		}

		if( true == empty( $this->m_strVerificationKey ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'verification_key', __( 'Typing the verification letters/numbers from the image below is required.' ), E_USER_ERROR ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && ( true == valObj( $objSession, 'CSession' ) || true == valObj( $objSession, 'CRedisSession' ) ) && 0 != \Psi\CStringService::singleton()->strcasecmp( $objSession->getValue( 'verification_key' ), $this->m_strVerificationKey ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'verification_key', __( 'The letters/numbers from the verification image were not entered correctly.' ), E_USER_ERROR ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valRequiredProspectPortalPeopleApplicationFields( $arrobjPropertyApplicationPreferences, $objGuaranator ) {
		$boolIsValid = true;

		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			return $boolIsValid;
		}

		if( true == array_key_exists( 'REQUIRE_PEOPLE_GUARANATOR', $arrobjPropertyApplicationPreferences ) && ( false == valObj( $objGuaranator, 'CApplicant' ) ) ) {

			if( false == valArr( $this->m_arrobjPropertyApplicationPreferences ) || false == array_key_exists( 'REQUIRED_PEOPLE_MINIMUM_GUARANTORS', $this->m_arrobjPropertyApplicationPreferences ) || false == is_numeric( $this->m_arrobjPropertyApplicationPreferences['REQUIRED_PEOPLE_MINIMUM_GUARANTORS']->getValue() ) || $this->m_arrobjPropertyApplicationPreferences['REQUIRED_PEOPLE_MINIMUM_GUARANTORS']->getValue() < 0 ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'guaranator', __( 'At least one guarantor is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valRequiredApplicationFields( $arrobjPropertyApplicationPreferences, $arrobjPropertyPreferences = NULL, $objDatabase = NULL, $boolIsRequired = true, $boolValidateMoveInDateForFuture = false ) {

		if( true === in_array( $this->getLeaseIntervalTypeId(), [ CLeaseIntervalType::LEASE_MODIFICATION, CLeaseIntervalType::RENEWAL, CLeaseIntervalType::TRANSFER ] ) ) return true;

		$boolIsValid = true;
		$boolCanBeNull = ( false == $boolIsRequired ) ? true : false;

		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valLeaseStartDate( true, $boolValidateMoveInDateForFuture );
			$boolIsValid &= $this->valDesiredRentMax( true );

		} elseif( false == array_key_exists( 'HIDE_LEASE_PREFERENCES', $arrobjPropertyApplicationPreferences ) ) {

			if( true == $boolIsRequired ) {
				if( true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_LEASE_START_DATE', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valLeaseStartDate( $boolCanBeNull, $boolValidateMoveInDateForFuture );
				} elseif( true == valArr( $arrobjPropertyPreferences ) && ( ( true == array_key_exists( 'DAYS_AFTER_AVAILABLE_ON_AVAILABLE_UNIT', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_AVAILABLE_UNIT']->getValue() )
						|| ( true == array_key_exists( 'DAYS_AFTER_AVAILABLE_ON_NOTICE_UNIT', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['DAYS_AFTER_AVAILABLE_ON_NOTICE_UNIT']->getValue() ) || ( true == array_key_exists( 'DAYS_BEFORE_AVAILABLE_ON', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['DAYS_BEFORE_AVAILABLE_ON']->getValue() ) ) ) {
					$boolIsValid &= $this->valLeaseStartDate( $boolCanBeNull, $boolValidateMoveInDateForFuture );
				} elseif( 0 > ( int ) $this->getPropertyTransmissionVendorId() ) {
					$boolIsValid &= $this->valLeaseStartDate( $boolCanBeNull, $boolValidateMoveInDateForFuture );
				} else {
					$boolIsValid &= $this->valLeaseStartDate( true, $boolValidateMoveInDateForFuture );
				}
			} else {
				$boolIsValid &= $this->valLeaseStartDate( $boolCanBeNull, $boolValidateMoveInDateForFuture );
			}
		}

		if( true == $boolIsRequired && true == valArr( $arrobjPropertyApplicationPreferences ) ) {

			if( true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_LEAD_SOURCE_ID', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valLeadSourceId( $objDatabase );
			}

			if( 0 > ( int ) $this->getPropertyTransmissionVendorId() || true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_TERM_MONTH', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valTermMonth( $objDatabase, false );
			}

			if( true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_DESIRED_RENT_MIN', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valDesiredRentMin();
			}

			if( true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_DESIRED_RENT_MAX', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valDesiredRentMax( '' );
			} else {
				$boolIsValid &= $this->valDesiredRentMax( true );
			}

			if( true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_DESIRED_BEDROOMS', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valDesiredBedrooms();
			}

			if( true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_DESIRED_BATHROOMS', $arrobjPropertyApplicationPreferences ) ) {
				if( false == is_null( $this->m_fltDesiredBathrooms ) ) {
					if( true == is_numeric( $this->m_fltDesiredBathrooms ) ) {
						if( false == in_array( fmod( $this->m_fltDesiredBathrooms, 1 ), [ 0, 0.25, 0.5, 0.75 ] ) ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
						}
					} else {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
					}
				} else {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'desired_bathrooms', __( 'Desired bathrooms is required.' ), '', [ 'label' => __( 'Preferences' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valInsertRenewalOffer() {
		$boolIsValid = true;

		$arrintApplicationStageStatusIds = [
			CApplicationStage::PRE_APPLICATION 	=> [
				CApplicationStatus::CANCELLED,
				CApplicationStatus::COMPLETED
			],
			CApplicationStage::APPLICATION 		=> [
				CApplicationStatus::STARTED,
				CApplicationStatus::PARTIALLY_COMPLETED
			]
		];

		if( false == $this->hasApplicationStageStatusIn( $arrintApplicationStageStatusIds ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'insert_renewal_offer', __( 'Failed to add/edit offers as offer has been already accepted.' ), E_USER_ERROR ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valRequiredResidentWorksAddLeadFields( $arrobjPropertyPreferences, $objDatabase, $boolIsStudentProperty = false, $boolIsForStartApplication = false ) {
		$boolIsValid = true;

		if( false == $boolIsStudentProperty ) {
			$boolIsValid &= $this->valDesiredRentMax( true );
		}

		if( true == valArr( $arrobjPropertyPreferences ) ) {
			if( true == array_key_exists( 'GC_REQUIRE_SOURCE_OF_LEAD', $arrobjPropertyPreferences ) ) {
				$boolIsValid &= $this->valLeadSourceId( $objDatabase );
			}

			if( true == array_key_exists( 'GC_PREFERENCES', $arrobjPropertyPreferences ) ) {
				if( false == $boolIsStudentProperty && true == array_key_exists( 'SHOW_AND_REQUIRE_MOVE_IN_DATE', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valLeaseStartDate( $boolCanBeNull = false, $boolValidateMoveInDateForFuture = true, NULL, $objDatabase );
				} else {
					// Code to validate the blackout Move-in Date
					$boolIsValid &= $this->valLeaseStartDate( $boolCanBeNull = true, $boolValidateMoveInDateForFuture = false, NULL, $objDatabase );
				}
			}

			if( true == array_key_exists( 'GC_REQUIRE_AGENT_FOR_NEW_LEAD', $arrobjPropertyPreferences ) ) {
				$boolIsValid &= $this->valLeasingAgentId( $objDatabase );
			}

			if( true == is_null( $this->getPropertyFloorplanId() ) && ( ( true == $boolIsStudentProperty && true == $boolIsForStartApplication ) || true == array_key_exists( 'REQUIRE_FLOOR_PLAN', $arrobjPropertyPreferences ) ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_floorplan_id', __( 'Floor plan is required.' ) ) );
			}

			if( false == $boolIsStudentProperty ) {
				if( ( false == array_key_exists( 'GC_PREFERENCES', $arrobjPropertyPreferences )
					  && true == array_key_exists( 'GC_BEDROOMS', $arrobjPropertyPreferences ) )
				  && true == array_key_exists( 'REQUIRE_DESIRED_BEDROOMS', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valDesiredBedrooms();
				}

				if( ( false == array_key_exists( 'GC_PREFERENCES', $arrobjPropertyPreferences )
					  && true == array_key_exists( 'GC_BATHROOMS', $arrobjPropertyPreferences ) )
				   && true == array_key_exists( 'REQUIRE_DESIRED_BATHROOMS', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valDesiredBathrooms();
				}
			}
		}
		return $boolIsValid;
	}

	public function valApplicationCallId( $objDatabase ) {
		$boolIsValid = true;

		assertObj( $objDatabase, 'CDatabase' );

		if( true == valObj( $this->fetchApplicationByCallId( $objDatabase ), 'CApplication' ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_id', __( 'This call has already been associated to a lead.' ) ) );
		}

		return $boolIsValid;
	}

	public function validateEntrataAddLead( $objDatabase, $arrobjPropertyPreferences, $boolIsStudentProperty = false, $boolIsValidateRequired ) {

		$boolIsValid = true;

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'GC_REQUIRE_SOURCE_OF_LEAD', $arrobjPropertyPreferences ) ) {
			$boolIsValid &= $this->valLeadSourceId( $objDatabase );
		}

		if( true == $boolIsStudentProperty ) {
			$boolIsValid &= $this->valPropertyFloorplanId( $arrobjPropertyPreferences, $boolIsStudentProperty, NULL, $boolIsValidateRequired );
		}

		$boolIsValid &= $this->valDesiredRentMax( true );

		return $boolIsValid;
	}

	public function valCancellationListItemId() {

		$boolIsValid = true;

		if( false == valId( $this->getCancellationListItemId() ) ) {
			$boolIsValid 		= false;
			$strListTypeName 	= 'Cancellation';
			if( CListType::LEAD_ARCHIVE_REASONS == $this->getCancellationListTypeId() ) {
				$strListTypeName = 'Archive';
			} elseif( CListType::LEAD_DENIAL_REASONS == $this->getCancellationListTypeId() ) {
				$strListTypeName = 'Denial';
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cancellation_list_item_id', $strListTypeName . ' ' . __( 'reason is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valHasPets( $arrobjPropertyApplicationPreferences ) {
		$boolIsValid = true;
		if( true == array_key_exists( 'REQUIRE_OPTION_PET', $arrobjPropertyApplicationPreferences ) && NULL === $this->getDesiredPets() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Pets question answer is required.' ), '', [ 'label' => __( 'Pet question' ), 'step_id' => ( true == array_key_exists( 'SHOW_PETS_ON_ADDNL_INFO', $arrobjPropertyApplicationPreferences ) ) ? CApplicationStep::ADDITIONAL_INFO : CApplicationStep::OPTIONS_AND_FEES ] ) );
		}

		return $boolIsValid;
	}

	public function valFurnishingPreference() {
		$boolIsValid = true;
		if( true == is_null( $this->getDetails()->furnishing_option ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'furnishing_option', __( 'Furnishing preference is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrobjPropertyApplicationPreferences = NULL, $arrobjPropertyPreferences = NULL, $objDatabase = NULL, $objGuaranator = NULL, $boolIsFromLightWindow = false, $boolIsRentComExample = false, $boolIsLiveChat = false, $boolIsAvailabilityAlert = false, $boolIsStudentProperty = false, $boolIsForStartApplication = false, $boolIsBlackOutDateOverride = false, $boolIsValidateGuestCardFields = false, $objSession = NULL, $boolIsValidateRequired = true, $boolIsValidateFlooplanId = false ) {

		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDesiredRentMax( true );
				$boolIsValid &= $this->valRequiredApplicationFields( $arrobjPropertyApplicationPreferences );
				break;

			case VALIDATE_DELETE:
			case 'resident_works_update_guest_card':
				break;

			case 'prospect_portal_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case 'resident_portal_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCompanyApplicationId( $objSession );
				break;

			case 'create_account':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCompanyApplicationId( $objSession );

				if( true == valArr( $arrobjPropertyApplicationPreferences ) && false == array_key_exists( 'HIDE_LEASE_PREFERENCES', $arrobjPropertyApplicationPreferences ) ) {
					if( true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_LEAD_SOURCE_ID', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsValid &= $this->valLeadSourceId( $objDatabase );
					}
				}
				break;

			case 'basic_info':
				$boolIsValid &= $this->valRequiredApplicationFields( $arrobjPropertyApplicationPreferences, $arrobjPropertyPreferences, $objDatabase );
				break;

			case 'basic_info_resident_works':
				$objApplicantApplication = $this->getApplicantApplication();
				if( false == valObj( $objApplicantApplication, 'CApplicantApplication' ) || CCustomerType::PRIMARY == $objApplicantApplication->getCustomerTypeId() ) {
					$boolIsValid &= $this->valRequiredApplicationFields( $arrobjPropertyApplicationPreferences, $arrobjPropertyPreferences, $objDatabase, true, $boolValidateMoveInDateForFuture = false );
				}
				break;

			case 'unit_info':
				$boolCanBeNull 	= ( true == valArr( $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_LEASE_START_DATE', $arrobjPropertyApplicationPreferences ) ) ? false : true;
				$boolIsValid 	&= $this->valLeaseStartDate( $boolCanBeNull, $boolValidateMoveInDateForFuture = false, $arrobjPropertyPreferences, $objDatabase, $boolIsBlackOutDateOverride, $boolIsValidateRequired );
				$this->getOrFetchProperty( $objDatabase );
				$strHideDisplayRequireLeaseTerm = $this->m_objProperty->fetchPropertyPreferenceKeysValuesByKeys( [ 'HIDE_DISPLAY_REQUIRE_LEASE_TERM' ], $this->m_objDatabase );

				if( true == valArr( $strHideDisplayRequireLeaseTerm ) && true == array_key_exists( 'HIDE_DISPLAY_REQUIRE_LEASE_TERM', $strHideDisplayRequireLeaseTerm ) && 'REQUIRE' == $strHideDisplayRequireLeaseTerm['HIDE_DISPLAY_REQUIRE_LEASE_TERM']['value'] ) {
					$boolSkipLeaseTermValidation = ( false == isset( $arrobjPropertyPreferences['ALLOW_UNIT_SPACE_SELECTION'] ) && ( CApplicationStep::PRE_SCREENING == $this->getApplicationStepId() || false == isset( $arrobjPropertyApplicationPreferences['REQUIRE_LEASE_PREFERENCES_TERM_MONTH'] ) ) ) ? true : false;

					if( false == $boolSkipLeaseTermValidation && false == in_array( $this->getOccupancyTypeId(), [ COccupancyType::HOSPITALITY, COccupancyType::AFFORDABLE ] ) ) {
						$boolIsValid &= $this->valTermMonth( $objDatabase, true, $boolIsValidateRequired );
					}
				}

				$boolIsValid &= $this->valPropertyFloorplanId( $arrobjPropertyPreferences, NULL, NULL, $boolIsValidateRequired );
				$boolIsValid &= $this->valUnitSpaceId( $arrobjPropertyPreferences, $boolIsValidateRequired );
				break;

			case 'unit_info_for_student':
				if( CLeaseIntervalType::TRANSFER != $this->getLeaseIntervalTypeId() && CLeaseIntervalType::RENEWAL != $this->getLeaseIntervalTypeId() ) {
					if( CLeaseIntervalType::LEASE_MODIFICATION == $this->getLeaseIntervalTypeId() && CLeaseIntervalType::MONTH_TO_MONTH == $this->m_objLease->getLeaseIntervalTypeId() ) {
						$boolIsValid &= true;
					} else {
						$boolIsValid &= $this->valLeaseTermId( $objDatabase, $boolIsValidateRequired );
					}
				}

				if( true == $boolIsValid && false == in_array( $this->getLeaseIntervalTypeId(), [ CLeaseIntervalType::TRANSFER, CLeaseIntervalType::RENEWAL, CLeaseIntervalType::LEASE_MODIFICATION ] ) ) {
					$boolIsValid &= ( true == $boolIsValidateFlooplanId ) ?
						$boolIsValid &= $this->valPropertyFloorplanId( $arrobjPropertyPreferences, true, $objDatabase, $boolIsValidateRequired )
						: $boolIsValid &= $this->valPropertyFloorplanId( NULL, true, $objDatabase, $boolIsValidateRequired );

				}

				$boolIsValid &= $this->valDesiredBathrooms();
				break;

			case 'unit_info_for_waitlist_floorplans':
			case 'update_move_in_date':
				$boolIsValid &= $this->valLeaseStartDate( $boolCanBeNull = false, $boolValidateMoveInDateForFuture = true, $arrobjPropertyPreferences, $objDatabase, NULL, $boolIsValidateRequired );
				break;

			case 'insert_guest_card':
				$objProperty = $this->getOrFetchProperty( $objDatabase );

				if( false == $boolIsAvailabilityAlert ) {

					$boolIsValidateLeadSource = ( false == $boolIsLiveChat ) ? true : false;
					$boolIsValidateLeadSource &= ( ( false == valArr( $arrobjPropertyPreferences ) || ( false == array_key_exists( 'HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) && false == array_key_exists( 'DO_NOT_REQUIRE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ) ? true : false;
					$boolIsValidateLeadSource &= ( ( false == valArr( $arrobjPropertyPreferences ) || false == array_key_exists( 'HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ? true : false;

					$boolIsValid &= ( true == $boolIsValidateLeadSource ) ? $this->valLeadSourceId( $objDatabase ) : true;

					if( false == $boolIsLiveChat ) {
						if( ( true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'SHOW_TERM_MONTH', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRE_TERM_MONTH', $arrobjPropertyPreferences ) ) ) ) {
							if( true == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
								$boolIsValid &= $this->valLeaseTermId( $objDatabase, $boolIsValidateRequired = true );
							} else {
								$boolIsValid &= $this->valTermMonth();
							}
						}

						if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRE_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() ) ) {
							$boolIsValid &= $this->valDesiredRentMin();
							$boolIsValid &= $this->valDesiredRentMax( false );
						} else {
							$boolIsValid &= $this->valDesiredRentMax( true );
						}

						$boolIsValid &= ( false == $boolIsFromLightWindow ) ? $this->valPropertyFloorplanId( $arrobjPropertyPreferences ) : true;

						if( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
							$boolIsValid &= ( false == array_key_exists( 'HIDE_DESIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) && false == array_key_exists( 'DO_NOT_REQUIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() ) ) ? $this->valLeaseStartDate( false ) : $this->valLeaseStartDate( true );
						}

						if( ( true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'SHOW_DESIRED_BEDROOMS', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRED_DESIRED_BEDROOMS', $arrobjPropertyPreferences ) ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
							$boolIsValid &= $this->valDesiredBedrooms();
						}

						if( ( true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'SHOW_DESIRED_BATHROOMS', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRED_DESIRED_BATHROOMS', $arrobjPropertyPreferences ) ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
							$boolIsValid &= $this->valDesiredBathrooms();
						} else {
							if( false == is_null( $this->m_fltDesiredBathrooms ) ) {
								if( true == is_numeric( $this->m_fltDesiredBathrooms ) ) {
									if( false == in_array( fmod( $this->m_fltDesiredBathrooms, 1 ), [ 0, 0.25, 0.5, 0.75 ] ) ) {
										$boolIsValid = false;
										$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
									}
								} else {
									$boolIsValid = false;
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
								}
							}
						}
					}

					if( false == $boolIsRentComExample ) {
						$boolIsValid &= $this->valVerificationKey( false, $objSession );
					}
				} else {
					// Validate Availability Alert
					$boolIsValidateLeadSource = ( false == $boolIsLiveChat ) ? true : false;
					$boolIsValidateLeadSource &= ( ( false == valArr( $arrobjPropertyPreferences ) || ( false == array_key_exists( 'HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) && false == array_key_exists( 'DO_NOT_REQUIRE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ) ? true : false;
					$boolIsValidateLeadSource &= ( ( false == valArr( $arrobjPropertyPreferences ) || false == array_key_exists( 'HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ? true : false;

					$boolIsValid &= ( true == $boolIsValidateLeadSource ) ? $this->valLeadSourceId( $objDatabase ) : true;

					if( false == $boolIsLiveChat ) {
						if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRE_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() ) ) {
							$boolIsValid &= $this->valDesiredRentMin();
							$boolIsValid &= $this->valDesiredRentMax( false );
						} else {
							$boolIsValid &= $this->valDesiredRentMax( true );
						}

						$boolIsValid &= ( false == $boolIsFromLightWindow ) ? $this->valPropertyFloorplanId( $arrobjPropertyPreferences ) : true;
						$boolIsValid &= ( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_DESIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) && true == array_key_exists( 'DEFAULT_MOVE_IN_DATE_TO_BLANK', $arrobjPropertyPreferences ) && false == array_key_exists( 'DO_NOT_REQUIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) ) ? $this->valLeaseStartDate( false ) : $this->valLeaseStartDate( true );
					}

					if( false == $boolIsRentComExample ) {
						$boolIsValid &= $this->valVerificationKey( false, $objSession );
					}
				}
				break;

			case 'insert_premium_guest_card':
				$boolIsValidateLeadSource = ( false == $boolIsLiveChat ) ? true : false;
				$boolIsValidateLeadSource &= ( true == valArr( $arrobjPropertyPreferences ) && ( false == array_key_exists( 'HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) && false == array_key_exists( 'DO_NOT_REQUIRE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ? true : false;
				$boolIsValidateLeadSource &= ( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ? true : false;

				$boolIsValid &= ( true == $boolIsValidateLeadSource ) ? $this->valLeadSourceId( $objDatabase ) : true;

				if( false == $boolIsStudentProperty && false == $boolIsLiveChat && ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_TERM_MONTH', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRE_TERM_MONTH', $arrobjPropertyPreferences ) ) ) {
					if( true == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
						$boolIsValid &= $this->valLeaseTermId( $objDatabase, $boolIsValidateRequired = true );
					} elseif( true == $boolIsValidateGuestCardFields ) {
						$boolIsValid &= $this->valTermMonth();
					}
				}

				if( false == $boolIsLiveChat && true == $boolIsValidateGuestCardFields ) {
					$objProperty = $this->getOrFetchProperty( $objDatabase );

					if( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_FLOORPLAN_RENT_RANGE_WHEN_NO_AVAILABLE_UNITS', $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRE_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valDesiredRentMin();
						$boolIsValid &= $this->valDesiredRentMax( false );
					} else {
						$boolIsValid &= $this->valDesiredRentMax( true );
					}

					$boolIsValid &= ( false == $boolIsFromLightWindow ) ? $this->valPropertyFloorplanId( $arrobjPropertyPreferences ) : true;

					if( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
						$boolIsValid &= ( false == array_key_exists( 'HIDE_DESIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) && false == array_key_exists( 'DO_NOT_REQUIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() ) ) ? $this->valLeaseStartDate( false ) : $this->valLeaseStartDate( true );
					}

					if( ( true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'SHOW_DESIRED_BEDROOMS', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRED_DESIRED_BEDROOMS', $arrobjPropertyPreferences ) ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valDesiredBedrooms();
					}

					if( ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'ALLOW_FLOORPLAN_SELECTION', $arrobjPropertyPreferences ) && ( true == array_key_exists( 'SHOW_DESIRED_FLOORPLANS', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRED_DESIRED_FLOORPLANS', $arrobjPropertyPreferences ) ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valDesiredFloorplans();
					}

					if( ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'ALLOW_FLOORPLAN_SELECTION', $arrobjPropertyPreferences ) && ( true == array_key_exists( 'SHOW_DESIRED_FLOORPLANS', $arrobjPropertyPreferences ) ) ) && ( true == valId( $this->getPropertyFloorplanId() ) || true == array_key_exists( 'REQUIRED_DESIRED_FLOORPLANS', $arrobjPropertyPreferences ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valDesiredSpaceConfigurationId();
					}

					if( ( true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'SHOW_DESIRED_BATHROOMS', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRED_DESIRED_BATHROOMS', $arrobjPropertyPreferences ) ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
						if( true == is_null( $this->m_fltDesiredBathrooms ) ) {
							$boolIsValid = false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'desired_bathrooms', __( 'Desired bathrooms is required.' ) ) );
						} else {
							$boolIsValid &= $this->valDesiredBathrooms();
						}

					} else {
						if( false == is_null( $this->m_fltDesiredBathrooms ) ) {
							if( true == is_numeric( $this->m_fltDesiredBathrooms ) ) {
								if( false == in_array( fmod( $this->m_fltDesiredBathrooms, 1 ), [ 0, 0.25, 0.5, 0.75 ] ) ) {
									$boolIsValid = false;
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
								}
							} else {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
							}
						}
					}

					if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_FURNISHING_PREFERENCE', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRED_FURNISHING_PREFERENCE', $arrobjPropertyPreferences ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valFurnishingPreference();
					}
				}
				break;

			case 'multi_contact_insert_premium_guest_card':
				$boolIsValidateLeadSource = ( false == $boolIsLiveChat ) ? true : false;
				$boolIsValidateLeadSource &= ( ( false == valArr( $arrobjPropertyPreferences ) || ( false == array_key_exists( 'MULTI_CONTACT_HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) && false == array_key_exists( 'MULTI_CONTACT_DO_NOT_REQUIRE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ) ? true : false;
				$boolIsValidateLeadSource &= ( ( false == valArr( $arrobjPropertyPreferences ) || false == array_key_exists( 'MULTI_CONTACT_HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ? true : false;

				$boolIsValid &= ( true == $boolIsValidateLeadSource ) ? $this->valLeadSourceId( $objDatabase ) : true;

				if( false == $boolIsStudentProperty && false == $boolIsLiveChat && ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'MULTI_CONTACT_SHOW_TERM_MONTH', $arrobjPropertyPreferences ) && true == array_key_exists( 'MULTI_CONTACT_REQUIRE_TERM_MONTH', $arrobjPropertyPreferences ) ) ) {
					if( true == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
						$boolIsValid &= $this->valLeaseTermId( $objDatabase, $boolIsValidateRequired = true );
					} else {
						if( true == $boolIsValidateGuestCardFields ) {
							$boolIsValid &= $this->valTermMonth();
						}
					}
				}

				if( false == $boolIsLiveChat && true == $boolIsValidateGuestCardFields ) {
					$objProperty = $this->getOrFetchProperty( $objDatabase );

					if( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_FLOORPLAN_RENT_RANGE_WHEN_NO_AVAILABLE_UNITS', $arrobjPropertyPreferences ) && true == array_key_exists( 'MULTI_CONTACT_SHOW_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && true == array_key_exists( 'MULTI_CONTACT_REQUIRE_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valDesiredRentMin();
						$boolIsValid &= $this->valDesiredRentMax( false );
					} else {
						$boolIsValid &= $this->valDesiredRentMax( true );
					}

					$boolIsValid &= ( false == $boolIsFromLightWindow ) ? $this->valPropertyFloorplanId( $arrobjPropertyPreferences ) : true;

					if( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) ) {
						$boolIsValid &= ( false == array_key_exists( 'MULTI_CONTACT_HIDE_DESIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) && true == array_key_exists( 'DEFAULT_MOVE_IN_DATE_TO_BLANK', $arrobjPropertyPreferences ) && false == array_key_exists( 'MULTI_CONTACT_DO_NOT_REQUIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() ) ) ? $this->valLeaseStartDate( false ) : $this->valLeaseStartDate( true );
					}

					if( ( true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'MULTI_CONTACT_SHOW_DESIRED_BEDROOMS', $arrobjPropertyPreferences ) && true == array_key_exists( 'MULTI_CONTACT_REQUIRED_DESIRED_BEDROOMS', $arrobjPropertyPreferences ) ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valDesiredBedrooms();
					}

					if( ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'ALLOW_FLOORPLAN_SELECTION', $arrobjPropertyPreferences ) && ( true == array_key_exists( 'MULTI_CONTACT_SHOW_DESIRED_FLOORPLANS', $arrobjPropertyPreferences ) && true == array_key_exists( 'MULTI_CONTACT_REQUIRED_DESIRED_FLOORPLANS', $arrobjPropertyPreferences ) ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valDesiredFloorplans();
					}

					if( ( true == valArr( $arrobjPropertyPreferences ) && ( true == array_key_exists( 'MULTI_CONTACT_SHOW_DESIRED_BATHROOMS', $arrobjPropertyPreferences ) && true == array_key_exists( 'MULTI_CONTACT_REQUIRED_DESIRED_BATHROOMS', $arrobjPropertyPreferences ) ) ) && ( CPropertyType::SINGLE_FAMILY != $objProperty->getPropertyTypeId() && CPropertyType::HOME_OWNERS_ASSOCIATION != $objProperty->getPropertyTypeId() && CPropertyType::COMMERCIAL != $objProperty->getPropertyTypeId() ) ) {
						$boolIsValid &= $this->valDesiredBathrooms();
					} else {
						if( false == is_null( $this->m_fltDesiredBathrooms ) ) {
							if( true == is_numeric( $this->m_fltDesiredBathrooms ) ) {
								if( false == in_array( fmod( $this->m_fltDesiredBathrooms, 1 ), [ 0, 0.25, 0.5, 0.75 ] ) ) {
									$boolIsValid = false;
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
								}
							} else {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
							}
						}
					}
				}
				break;

			case 'insert_call_center_guest_card':
				$boolIsValid &= $this->valLeaseStartDate( true );
				$boolIsValid &= $this->valCallId();
				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'LEASING_CENTER_APPLY_ENTRATA_GUEST_CARD_AND_APPLICATION_SETTINGS', $arrobjPropertyPreferences ) && true == $arrobjPropertyPreferences['LEASING_CENTER_APPLY_ENTRATA_GUEST_CARD_AND_APPLICATION_SETTINGS']->getValue() ) {
					if( true == isset( $arrobjPropertyPreferences['GC_REQUIRE_SOURCE_OF_LEAD'] ) ) {
						$boolIsValid &= $this->valLeadSourceId( $objDatabase );
					}
				}
				break;

			case 'insert_tablet_guest_card':
			case 'insert_wait_list_floorplans':
				break;

			case 'insert_bulk_guest_card':
				$boolIsValid &= $this->valLeaseStartDate( true );
				break;

			case 'insert_mobile_guest_card':
				$boolIsValidateLeadSource = ( false == $boolIsLiveChat ) ? true : false;
				$boolIsValidateLeadSource &= ( ( false == valArr( $arrobjPropertyPreferences ) || ( false == array_key_exists( 'HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) && false == array_key_exists( 'DO_NOT_REQUIRE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ) ? true : false;
				$boolIsValidateLeadSource &= ( ( false == valArr( $arrobjPropertyPreferences ) || false == array_key_exists( 'HIDE_LEAD_SOURCE', $arrobjPropertyPreferences ) ) ) ? true : false;

				$boolIsValid &= ( true == $boolIsValidateLeadSource ) ? $this->valLeadSourceId( $objDatabase ) : true;

				if( true == $boolIsValidateGuestCardFields ) {
					if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) && true == array_key_exists( 'REQUIRE_DESIRED_MONTH_RENT_RANGE', $arrobjPropertyPreferences ) ) {
						$boolIsValid &= $this->valDesiredRentMin();
						$boolIsValid &= $this->valDesiredRentMax( false );
					} else {
						$boolIsValid &= $this->valDesiredRentMax( true );
					}

					$boolIsValid &= ( ( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_DESIRED_MOVEIN_DATE', $arrobjPropertyPreferences ) && true == array_key_exists( 'DEFAULT_MOVE_IN_DATE_TO_BLANK', $arrobjPropertyPreferences ) ) ) ? $this->valLeaseStartDate( false ) : $this->valLeaseStartDate( true );
				}
				break;

			case 'insert_sms_guest_card':
				break;

			case 'insert_customer_referral_guest_card':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case 'prospect_portal_people':
				$boolIsValid &= $this->valRequiredProspectPortalPeopleApplicationFields( $arrobjPropertyApplicationPreferences, $objGuaranator );
				break;

			case 'close_application':
				break;

			case 'resident_works_lease_renewal_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case 'resident_works_application_quick_view':
				$boolCanBeNull = ( true == valArr( $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_LEASE_START_DATE', $arrobjPropertyApplicationPreferences ) )  ? false : true;
				$boolIsValid &= $this->valLeaseStartDate( $boolCanBeNull, $boolValidateMoveInDateForFuture = false, NULL, $objDatabase, $boolIsBlackOutDateOverride );

				if( $this->getLeaseIntervalTypeId() != CLeaseIntervalType::RENEWAL && true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'GC_REQUIRE_AGENT_FOR_NEW_LEAD', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valLeasingAgentId( $objDatabase );
				}

				if( $this->getLeaseIntervalTypeId() != CLeaseIntervalType::RENEWAL ) {
					$boolIsValid &= $this->valLeaseEndDate();
				}
				break;

			case 'resident_works_application_preferences':
				$boolIsValid &= $this->valDesiredRentMax( true );
				$boolIsValid &= $this->valDesiredBathrooms();
				break;

			case 'validate_insert_renewal_offer':
			case 'validate_update_renewal_offer':
			case 'validate_send_renewal_offers':
				$boolIsValid &= $this->valInsertRenewalOffer();
				break;

			case 'validate_number_of_bathroom':
				$boolIsValid &= $this->valDesiredBathrooms();
				break;

			case 'resident_work_add_lead':
				$boolIsValid &= $this->valRequiredResidentWorksAddLeadFields( $arrobjPropertyPreferences, $objDatabase, $boolIsStudentProperty, $boolIsForStartApplication );
				break;

			case 'entrata_add_lead':
				$boolIsValid &= $this->validateEntrataAddLead( $objDatabase, $arrobjPropertyPreferences, $boolIsStudentProperty, $boolIsValidateRequired );
				break;

			case 'validate_verification_key':
				$boolIsValid &= $this->valVerificationKey( false, $objSession );
				break;

			case 'validate_financial_move_out':
				$boolIsValid &= $this->valCancellationListItemId();
				break;

			case 'validate_pets':
				$boolIsValid &= $this->valHasPets( $arrobjPropertyApplicationPreferences );
				break;

			default:
		}

		return $boolIsValid;
	}

	/**
	 * Get or Fetch Functions
	 *
	 */

	public function getOrFetchLeadSource( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( false == valObj( $this->m_objLeadSource, 'CLeadSource' ) ) {
			$this->m_objLeadSource = $this->fetchLeadSource( $objDatabase );
		}

		return $this->m_objLeadSource;
	}

	public function getOrFetchLease( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( false == valObj( $this->m_objLease, 'CLease' ) ) {
			$this->m_objLease = $this->fetchLease( $objDatabase );
		}

		if( true == valObj( $this->m_objLease, 'CLease' ) && false == valObj( $this->m_objLease->getDatabase(), 'CDatabase' ) ) {
			$this->m_objLease->setDatabase( $objDatabase );
		}

		return $this->m_objLease;
	}

	public function getOrFetchMoneygramAccount( $intCustomerId, $intLeaseId, $objDatabase = NULL, $objPaymentDatabase ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( false == valObj( $this->m_objMoneyGramAccount, 'CMoneyGramAccount' ) ) {
			$this->m_objMoneyGramAccount = $this->fetchMoneygramAccount( $intCustomerId, $intLeaseId, $objDatabase, $objPaymentDatabase );
		}

		return $this->m_objMoneyGramAccount;
	}

	public function getOrFetchProperty( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == valObj( $this->m_objProperty, 'CProperty' ) ) {
			return $this->m_objProperty;
		} else {
			return $this->fetchProperty( $objDatabase );
		}
	}

	public function getOrFetchClient( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient;
		} else {
			return $this->fetchClient( $objDatabase );
		}
	}

	public function getOrFetchApplicantApplicationByApplicantId( $intApplicantId, $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == valObj( $this->m_objApplicantApplication, 'CApplicantApplication' ) && $this->m_objApplicantApplication->getApplicantId() == $intApplicantId ) {
			return $this->m_objApplicantApplication;
		} else {
			return $this->fetchApplicantApplicationByApplicantId( $intApplicantId, $objDatabase );
		}
	}

	public function getOrFetchPrimaryApplicantApplication( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == valObj( $this->m_objPrimaryApplicantApplication, 'CApplicantApplication' ) ) {
			return $this->m_objPrimaryApplicantApplication;
		}

		if( true == valArr( $this->m_arrobjApplicantApplications ) ) {
			foreach( $this->m_arrobjApplicantApplications as $objApplicantApplication ) {
				if( CCustomerType::PRIMARY == $objApplicantApplication->getCustomerTypeId() ) {
					$this->m_objPrimaryApplicantApplication = $objApplicantApplication;
				}
			}
		}

		if( false == valObj( $this->m_objPrimaryApplicantApplication, 'CApplicantApplication' ) ) {
			$this->m_objPrimaryApplicantApplication = $this->fetchApplicantApplicationByCustomerTypeId( CCustomerType::PRIMARY, $objDatabase );
		}

		return $this->m_objPrimaryApplicantApplication;
	}

	public function getOrFetchPropertyFloorplan( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == is_null( $this->getPropertyFloorplanId() ) || false == is_numeric( $this->getPropertyFloorplanId() ) )
			return NULL;

		if( true == valObj( $this->m_objPropertyFloorplan, 'CPropertyFloorplan' ) ) {
			return $this->m_objPropertyFloorplan;
		} else {
			return $this->fetchPropertyFloorplan( $objDatabase );
		}
	}

	public function getOrFetchPropertyUnit( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == is_null( $this->getPropertyUnitId() ) || false == is_numeric( $this->getPropertyUnitId() ) )
			return NULL;

		if( true == valObj( $this->m_objPropertyUnit, 'CPropertyUnit' ) ) {
			return $this->m_objPropertyUnit;
		} else {
			return $this->fetchPropertyUnit( $objDatabase );
		}
	}

	public function getOrFetchUnitSpace( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) )
			return NULL;

		if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) {
			return $this->m_objUnitSpace;
		} else {
			return $this->fetchUnitSpace( $objDatabase );
		}
	}

	public function getOrFetchPropertyApplicationPreferences( $objDatabase = NULL, $intPropertyId = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == valArr( $this->m_arrobjPropertyApplicationPreferences ) ) {
			return $this->m_arrobjPropertyApplicationPreferences;
		} else {
			return $this->fetchPropertyApplicationPreferences( $objDatabase, $intPropertyId );
		}
	}

	public function getOrFetchApplicationSteps( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == valArr( $this->m_arrobjApplicationSteps ) ) {
			return $this->m_arrobjApplicationSteps;
		} else {
			return $this->fetchApplicationSteps( $objDatabase );
		}
	}

	public function getOrFetchApplicant( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( false == valObj( $this->m_objApplicant, 'CApplicant' ) ) {
			$this->m_objApplicant = $this->fetchApplicant( $objDatabase );
		}

		return $this->m_objApplicant;
	}

	public function getOrFetchApplicantApplications( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( false == valArr( $this->m_arrobjApplicantApplications ) ) {
			$this->m_arrobjApplicantApplications = $this->fetchApplicantApplications( $objDatabase );
		}

		return $this->m_arrobjApplicantApplications;
	}

	public function getOrFetchPrimaryApplicant( $objDatabase = NULL, $boolLoadFromView = true ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( false == valObj( $this->m_objPrimaryApplicant, 'CApplicant' ) ) {
			$this->m_objPrimaryApplicant = $this->fetchPrimaryApplicant( $objDatabase, $boolLoadFromView );
		}

		return $this->m_objPrimaryApplicant;
	}

	public function getOrFetchFileMergeValueCount( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == is_null( $this->m_intFileMergeValueCount ) || false == is_numeric( $this->m_intFileMergeValueCount ) ) {
			$this->m_intFileMergeValueCount = $this->fetchFileMergeValueCount( $objDatabase );
		}

		return $this->m_intFileMergeValueCount;
	}

	public function getOrFetchFileMergeValues( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( false == valArr( $this->m_arrobjFileMergeValues ) ) {
			$this->m_arrobjFileMergeValues = $this->fetchFileMergeValues( $objDatabase );
		}

		return $this->m_arrobjFileMergeValues;
	}

	public function getOrFetchScheduledCharges( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( false == valArr( $this->m_arrobjScheduledCharges ) ) {
			$this->m_arrobjScheduledCharges = $this->fetchScheduledCharges( $objDatabase );
		}

		return $this->m_arrobjScheduledCharges;
	}

	public function getOrFetchWebsite( $objDatabase = NULL, $boolSendInvitation = false ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == $boolSendInvitation ) {
			$this->m_objWebsite = CWebsites::createService()->fetchDefaultNonLobbyTypeWebsiteByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
			if( true == valObj( $this->m_objWebsite, 'CWebsite' ) ) return $this->m_objWebsite;
		}

		if( false == is_null( $this->getWebsiteId() ) ) {
			if( true == valObj( $this->m_objWebsite, 'CWebsite' ) && $this->m_objWebsite->getId() == $this->getWebsiteId() ) {
				return $this->m_objWebsite;
			} else {
				$this->m_objWebsite = CWebsites::createService()->fetchWebsiteByIdByCid( $this->getWebsiteId(), $this->getCid(), $objDatabase );
				return $this->m_objWebsite;
			}
		}

		return NULL;
	}

	public function getOrFetchLateFeeFormulaId( $objDatabase = NULL ) {

		if( false == is_null( $this->m_intLateFeeFormulaId ) ) {
			return $this->m_intLateFeeFormulaId;
		}

        if( true == is_null( $objDatabase ) ) {
            $objDatabase = $this->m_objDatabase;
            if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
        }

		if( false == valObj( $this->m_objLeaseInterval, 'CLeaseInterval' ) ) {
			$this->getOrFetchLeaseInterval( $objDatabase );
		}

		if( true == valObj( $this->m_objLeaseInterval, 'CLeaseInterval' ) && false == is_null( $this->m_objLeaseInterval->getLateFeeFormulaId() ) ) {
            $this->setLateFeeFormulaId( $this->m_objLeaseInterval->getLateFeeFormulaId() );
		    return $this->getLateFeeFormulaId();
		}

		$objLateFeeFormula = \Psi\Eos\Entrata\CLateFeeFormulas::createService()->fetchDefaultLateFeeFormulaByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objLateFeeFormula, 'CLateFeeFormula' ) ) {
			$this->setLateFeeFormulaId( $objLateFeeFormula->getId() );
		}

		return $this->getLateFeeFormulaId();
	}

	public function getOrFetchLeaseInterval( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == is_null( $this->m_objLeaseInterval ) && 0 < $this->getLeaseIntervalId() ) {
			$objLeaseInterval = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {
				$this->m_objLeaseInterval = $objLeaseInterval;
			}
		}

		return $this->m_objLeaseInterval;
	}

	public function getOrFetchPropertyBuilding( $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
		}

		if( true == is_null( $this->m_objPropertyBuilding ) ) {
			$objPropertyBuilding = CPropertyBuildings::createService()->fetchPropertyBuildingByIdByCid( $this->getPropertyBuildingId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objPropertyBuilding, 'CPropertyBuilding' ) ) {
				$this->m_objPropertyBuilding = $objPropertyBuilding;
			}
		}

		return $this->m_objPropertyBuilding;
	}

    public function getOrFetchLeaseTerm( $objDatabase = NULL ) {
        if( true == is_null( $objDatabase ) ) {
            $objDatabase = $this->m_objDatabase;
            if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
        }

        if( true == is_null( $this->m_objLeaseTerm ) ) {
            if( true == is_null( $this->m_objLeaseInterval ) ) {
                $this->getOrFetchLeaseInterval( $objDatabase );
                if( false == valObj( $this->m_objLeaseInterval, 'CLeaseInterval' ) ) return NULL;
            }
            $objLeaseTerm = CLeaseTerms::fetchLeaseTermByIdByCid( $this->m_objLeaseInterval->getLeaseTermId(), $this->getCid(), $objDatabase );

            if( true == valObj( $objLeaseTerm, 'CLeaseTerm' ) ) {
                $this->m_objLeaseTerm = $objLeaseTerm;
            }
        }

        return $this->m_objLeaseTerm;
    }

	public function getDocumentManager() {
		return $this->m_objDocumentManager;
	}

	public function getObjectStorageGateway() {
		return $this->m_objObjectStorageGateway;
	}

	public function fetchLeaseTerm( $objProspectPortalDatabase ) {
		return CLeaseTerms::fetchLeaseTermByPropertyIdByIdByCid( $this->getPropertyId(), $this->getLeaseTermId(), $this->getCid(), $objProspectPortalDatabase );
	}

	public function fetchPropertyIdByApplicationIdByCid( $objProspectPortalDatabase ) {
		return CApplications::fetchPropertyIdByApplicationIdByCid( $this->getId(), $this->getCid(), $objProspectPortalDatabase );
	}

	public function fetchLeaseInterval( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );
	}

	public function fetchLatestFileMergeValues( $objDatabase ) {
		return CFileMergeValues::fetchLatestFileMergeValuesByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSubsidyCertification( $objDatabase, $boolExcludeDeleted = true, $arrintExcludeSubsidyCertificationTypeIds = NULL ) {
		return \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchSubsidyCertificationByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolExcludeDeleted, $arrintExcludeSubsidyCertificationTypeIds );
	}

	public function fetchApplicationSubsidyDetail( $objDatabase, $boolExcludeDeleted = true ) {
		return \Psi\Eos\Entrata\CApplicationSubsidyDetails::createService()->fetchApplicationSubsidyDetailByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolExcludeDeleted );
	}

	/**
	 * Other Functions
	 *
	 */

	public function fetchLeaseUnitSpaceIds( $objDatabase ) {
		return CLeaseUnitSpaces::fetchActiveUnitSpaceIdsByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
	}

	public function determineIsIntegrated() {
		if( CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() && ( ( CApplicationStage::PRE_APPLICATION == $this->getApplicationStageId() && true == valStr( $this->getGuestRemotePrimaryKey() ) ) || true == valStr( $this->getAppRemotePrimaryKey() ) ) ) {
			return true;
		}

		return false;
	}

	public function determineAllApplicantApplicationsCompleted( $objDatabase ) {

		$arrobjApplicantApplications = ( array ) $this->getOrFetchApplicantApplications( $objDatabase );

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( false == in_array( $objApplicantApplication->getCustomerTypeId(), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds ) ) continue;

			if( true == is_null( $objApplicantApplication->getCompletedOn() ) ) {
				return false;
			}
		}

		return true;
	}

	public function determineAllApplicantApplicationsApproved( $objDatabase ) {

		$arrobjApplicantApplications = $this->getOrFetchApplicantApplications( $objDatabase );

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( false == in_array( $objApplicantApplication->getCustomerTypeId(), CCustomerType::$c_arrintResponsibleCustomerTypeIds ) ) continue;

			if( true == is_null( $objApplicantApplication->getApprovedOn() ) || true == is_null( $objApplicantApplication->getApprovedBy() ) ) {
				return false;
			}
		}

		return true;
	}

	public function determineSkipLeaseDocuments( $arrobjPropertyPreferences, $objDatabase ) {

		if( true == isset( $arrobjPropertyPreferences ) && true == valArr( $arrobjPropertyPreferences )
			&& true == array_key_exists( 'ALLOW_COMPLETING_LEASES_WITHOUT_GUARANTOR_LEASES_DOCUMENTS', $arrobjPropertyPreferences )
			&& true == $arrobjPropertyPreferences['ALLOW_COMPLETING_LEASES_WITHOUT_GUARANTOR_LEASES_DOCUMENTS']->getValue() ) {

			$arrobjApplicantApplications = $this->getOrFetchApplicantApplications( $objDatabase );

			if( false == valArr( $arrobjApplicantApplications ) ) return false;

			foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
				if( CCustomerType::GUARANTOR != $objApplicantApplication->getCustomerTypeId() || false == is_null( $objApplicantApplication->getLeaseSkippedOn() ) ) continue;

				if( false == is_null( $objApplicantApplication->getApprovedOn() ) && true == is_null( $objApplicantApplication->getLeaseGeneratedOn() ) && true == is_null( $objApplicantApplication->getLeaseSkippedOn() ) ) {
					return true;
				}
			}
		}

		return false;
	}

	public function determineAllApplicantApplicationsLeaseGenerated( $objDatabase ) {

		$arrobjApplicantApplications = $this->getOrFetchApplicantApplications( $objDatabase );

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( false == in_array( $objApplicantApplication->getCustomerTypeId(), CCustomerType::$c_arrintResponsibleCustomerTypeIds ) || false == is_null( $objApplicantApplication->getLeaseSkippedOn() ) ) continue;

			if( CLeaseStatusType::NOTICE == $objApplicantApplication->getLeaseStatusTypeId() ) continue;

			if( true == is_null( $objApplicantApplication->getLeaseGeneratedOn() ) ) {
				return false;
			}
		}

		return true;
	}

	public function determineAllApplicantApplicationsLeaseCompleted( $objDatabase ) {

		$arrobjApplicantApplications = ( array ) $this->getOrFetchApplicantApplications( $objDatabase );

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( false == in_array( $objApplicantApplication->getCustomerTypeId(), CCustomerType::$c_arrintResponsibleCustomerTypeIds ) || false == is_null( $objApplicantApplication->getLeaseSkippedOn() ) ) continue;

			if( true == is_null( $objApplicantApplication->getLeaseSignedOn() ) ) {
				return false;
			}
		}

		return true;
	}

	public function determineAllApplicantApplicationsLeaseGeneratedAtOnce( $objDatabase ) {

		$arrobjApplicantApplications	= $this->getOrFetchApplicantApplications( $objDatabase );
		$intLeaseGeneratedOn 			= NULL;

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( false == in_array( $objApplicantApplication->getCustomerTypeId(), CCustomerType::$c_arrintResponsibleCustomerTypeIds ) ) continue;

			if( true == is_null( $objApplicantApplication->getLeaseGeneratedOn() ) ) {
				return false;
			}

			if( true == is_null( $intLeaseGeneratedOn ) ) {
				$intLeaseGeneratedOn = strtotime( date( 'm/d/Y H:i:s', strtotime( $objApplicantApplication->getLeaseGeneratedOn() ) ) );
			}

			if( $intLeaseGeneratedOn != strtotime( date( 'm/d/Y H:i:s', strtotime( $objApplicantApplication->getLeaseGeneratedOn() ) ) ) ) {
				return false;
			}
		}

		return true;
	}

	public function boolAllowIndependentLeaseProgression( $arrobjPropertyPreferences ) {

		if( false == valArr( $arrobjPropertyPreferences ) ) return false;

		if( false == array_key_exists( 'ALLOW_INDEPENDENT_APPLICANT_LEASE_PROGRESSION', $arrobjPropertyPreferences ) ) return false;

		if( 1 != $arrobjPropertyPreferences['ALLOW_INDEPENDENT_APPLICANT_LEASE_PROGRESSION']->getValue() ) return false;

		 return true;
	}

	public function checkIsIndividualLeaseProgression( $objDatabase ) {

		$boolIsIndividualLeaseProgression = false;

		$objPropertyPreferences = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'ALLOW_INDEPENDENT_APPLICANT_LEASE_PROGRESSION', $this->getPropertyId(), $this->getCid(), $objDatabase );
		if( true == valObj( $objPropertyPreferences, 'CPropertyPreference' ) && 1 == $objPropertyPreferences->getValue() ) {
			$boolIsIndividualLeaseProgression = true;
		}

		if( true == $boolIsIndividualLeaseProgression ) {
			return $boolIsIndividualLeaseProgression;
		}

		$arrobjApplicantApplications			= ( array ) $this->getOrFetchApplicantApplications( $objDatabase );

		$intLeaseGeneratedOn 					= NULL;
		$intLeaseGeneratedApplicantsCount 		= 0;
		$intApplicationApprovedApplicantsCount 	= 0;

		if( false == valArr( $arrobjApplicantApplications ) ) {
			return $boolIsIndividualLeaseProgression;
		}

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {

			if( false == in_array( $objApplicantApplication->getCustomerTypeId(), CCustomerType::$c_arrintResponsibleCustomerTypeIds ) ) {
				unset( $arrobjApplicantApplications[$objApplicantApplication->getId()] );
				continue;
			}

			if( false == is_null( $objApplicantApplication->getApprovedOn() ) ) {
				$intApplicationApprovedApplicantsCount++;
			}

			if( true == is_null( $objApplicantApplication->getLeaseGeneratedOn() ) )  continue;

			$intLeaseGeneratedApplicantsCount++;

			if( true == is_null( $intLeaseGeneratedOn ) ) {
				$intLeaseGeneratedOn = strtotime( date( 'm/d/Y H:i:s', strtotime( $objApplicantApplication->getLeaseGeneratedOn() ) ) );
			}

			if( $intLeaseGeneratedOn != strtotime( date( 'm/d/Y H:i:s', strtotime( $objApplicantApplication->getLeaseGeneratedOn() ) ) ) ) {
				$boolIsIndividualLeaseProgression = true;
				break;
			}
		}

		if( 1 != $boolIsIndividualLeaseProgression ) {
			if( 0 != $intLeaseGeneratedApplicantsCount && $intLeaseGeneratedApplicantsCount != \Psi\Libraries\UtilFunctions\count( $arrobjApplicantApplications ) ) {
				$boolIsIndividualLeaseProgression = true;
			}

			if( 0 != $intApplicationApprovedApplicantsCount && $intApplicationApprovedApplicantsCount != \Psi\Libraries\UtilFunctions\count( $arrobjApplicantApplications ) && CApplicationStage::APPLICATION == $this->getApplicationStageId() && CApplicationStatus::APPROVED == $this->getApplicationStatusId() ) {
				$boolIsIndividualLeaseProgression = true;
			}
		}

		return $boolIsIndividualLeaseProgression;
	}

	public function checkEmailPermissionByPsProductIds( $arrintPsProductIds ) {
		// allow email if Lead Management, Message Center or Entrata Core product is enabled
		if( true == array_key_exists( CPsProduct::ENTRATA, $arrintPsProductIds ) ) {
			if( true == valArr( $arrintPsProductIds[CPsProduct::ENTRATA] ) ) {
				if( true == in_array( $this->getPropertyId(), $arrintPsProductIds[CPsProduct::ENTRATA]['property_ids'] ) ) {
					return true;
				}
			}
		}

		if( true == array_key_exists( CPsProduct::LEAD_MANAGEMENT, $arrintPsProductIds ) ) {
			if( true == valArr( $arrintPsProductIds[CPsProduct::LEAD_MANAGEMENT] ) ) {
				if( true == in_array( $this->getPropertyId(), $arrintPsProductIds[CPsProduct::LEAD_MANAGEMENT]['property_ids'] ) ) {
					return true;
				}
			}
		}

		if( true == array_key_exists( CPsProduct::MESSAGE_CENTER, $arrintPsProductIds ) ) {
			if( true == valArr( $arrintPsProductIds[CPsProduct::MESSAGE_CENTER] ) ) {
				if( true == in_array( $this->getPropertyId(), $arrintPsProductIds[CPsProduct::MESSAGE_CENTER]['property_ids'] ) ) {
					return true;
				}
			}
		}
		return false;
	}

	public function checkSmsPermissionByPsProductIds( $arrintPsProductIds ) {
		// allow sms if Lead Management, Message Center+Sms Option or Entrata Core product is enabled
		if( true == array_key_exists( CPsProduct::ENTRATA, $arrintPsProductIds ) ) {
			if( true == valArr( $arrintPsProductIds[CPsProduct::ENTRATA] ) ) {
				if( true == in_array( $this->getPropertyId(), $arrintPsProductIds[CPsProduct::ENTRATA]['property_ids'] ) ) {
					return true;
				}
			}
		}

		if( true == array_key_exists( CPsProduct::LEAD_MANAGEMENT, $arrintPsProductIds ) ) {
			if( true == valArr( $arrintPsProductIds[CPsProduct::LEAD_MANAGEMENT] ) ) {
				if( true == in_array( $this->getPropertyId(), $arrintPsProductIds[CPsProduct::LEAD_MANAGEMENT]['property_ids'] ) ) {
					return true;
				}
			}
		}

		if( true == array_key_exists( CPsProduct::MESSAGE_CENTER, $arrintPsProductIds ) ) {
			if( true == valArr( $arrintPsProductIds[CPsProduct::MESSAGE_CENTER] ) ) {
				if( true == array_key_exists( 'ps_product_option_ids', $arrintPsProductIds[CPsProduct::MESSAGE_CENTER] ) && true == array_key_exists( CPsProductOption::SMS, $arrintPsProductIds[CPsProduct::MESSAGE_CENTER]['ps_product_option_ids'] ) ) {
					if( true == valArr( $arrintPsProductIds[CPsProduct::MESSAGE_CENTER]['ps_product_option_ids'][CPsProductOption::SMS] ) && true == in_array( $this->getPropertyId(), $arrintPsProductIds[CPsProduct::MESSAGE_CENTER]['ps_product_option_ids'][CPsProductOption::SMS] ) ) {
						return true;
					}
				}

			}
		}
		return false;
	}

	public function loadApplicationSteps( $objClientDatabase, $intCustomerTypeId, $arrstrExitTags = [], $boolIsStudentProperty = false, $boolSecondaryApplicant = false, $objApplicantApplication = NULL, $boolResponsiveWebsiteTemplate = false ) {
		$this->getOrFetchPropertyApplicationPreferences( $objClientDatabase );
		$this->getOrFetchApplicationSteps( $objClientDatabase );
		$this->getOrFetchProperty( $objClientDatabase );

		if( false == valArr( $this->m_arrobjApplicationSteps ) ) return [];

		$arrobjPropertyPreferences = $this->m_objProperty->getOrFetchPropertyPreferences( $objClientDatabase );

		// Rekey the preferences array based off of the key of each object
		$this->m_arrobjPropertyApplicationPreferences = ( array ) rekeyObjects( 'Key', $this->m_arrobjPropertyApplicationPreferences );

		$this->m_arrobjCurrentApplicationSteps = [];

		$intStepCount = 1;
		$this->getOrFetchClient( $objClientDatabase );

		// DETERMINE MILITARY STEP
		if( COccupancyType::MILITARY == $this->getOccupancyTypeId() && CCustomerType::NON_LEASING_OCCUPANT != $intCustomerTypeId && true == $boolResponsiveWebsiteTemplate ) {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::MILITARY];
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_military'] );
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Military' ) );
			$intStepCount ++;
		}

		// DETERMINE UNIT INFO STEP

		if( ( true == isset( $arrobjPropertyPreferences['ALLOW_FLOORPLAN_SELECTION'] ) || true == isset( $arrobjPropertyPreferences['ALLOW_UNIT_SPACE_SELECTION'] ) ) && CCustomerType::PRIMARY == $intCustomerTypeId && false == in_array( $this->getLeaseIntervalTypeId(), [ CLeaseIntervalType::LEASE_MODIFICATION, CLeaseIntervalType::RENEWAL, CLeaseIntervalType::TRANSFER ] ) ) {

			$strStepName = ( false == isset( $arrobjPropertyPreferences['ALLOW_UNIT_SPACE_SELECTION'] ) ) ? __( 'Location' ) : __( 'Unit Info' );

			$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::UNIT_INFO];
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_unit_info'] );
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $strStepName );

			$intStepCount ++;
		}

		// DETERMINE OPTIONS & FEES STEP

		if( CLeaseIntervalType::RENEWAL != $this->getLeaseIntervalTypeId() && false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_OPTION'] ) && CCustomerType::PRIMARY == $intCustomerTypeId ) {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::OPTIONS_AND_FEES];
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_options'] );

			if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_OPTIONS'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_OPTIONS']->getValue() ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_OPTIONS']->getValue() );
			} else {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Options' ) );
			}

			$intStepCount ++;
		}

		// BASIC INFO STEP

		$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::BASIC_INFO];
		$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_basic_info'] );

		if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO']->getValue() ) ) {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO']->getValue() );
		} else {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Basic Info' ) );
		}

		$intStepCount ++;

		// DETERMINE ADDITIONAL INFO STEP

		$boolPersonalInformationHidden			= ( ( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_PERSONAL_INFO', $this->m_arrobjPropertyApplicationPreferences ) ) ? true : false );
		$boolCurrentAddressInformationHidden	= ( ( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS', $this->m_arrobjPropertyApplicationPreferences ) ) ? true : false );
		$boolPreviousAddressInformationHidden	= ( ( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $this->m_arrobjPropertyApplicationPreferences ) ) ? true : false );
		$boolVehicleInformationHidden			= ( ( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_VEHICLES', $this->m_arrobjPropertyApplicationPreferences ) ) ? true : false );
		$boolQuestionarioHidden					= ( ( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS', $this->m_arrobjPropertyApplicationPreferences ) ) ? true : false );
		$boolDisplayAdditionalInfoStep 			= true;

		if( true == $boolPersonalInformationHidden && true == $boolCurrentAddressInformationHidden && true == $boolPreviousAddressInformationHidden && true == $boolVehicleInformationHidden && true == $boolQuestionarioHidden ) {
			$boolDisplayAdditionalInfoStep = false;
		}

		if( CCustomerType::GUARANTOR == $intCustomerTypeId && true == $boolPersonalInformationHidden && true == $boolCurrentAddressInformationHidden && true == $boolQuestionarioHidden ) {
			$boolDisplayAdditionalInfoStep = false;
		}

		if( false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_ADDITIONAL_INFO'] ) && true == $boolDisplayAdditionalInfoStep ) {

			$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::ADDITIONAL_INFO];

			if( true == array_key_exists( 'application_additional_info', $arrstrExitTags ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_additional_info'] );
			}

			if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO']->getValue() ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO']->getValue() );
			} else {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Additional Info' ) );
			}

			$intStepCount ++;
		}

		// DETERMINE ROOMMATES STEP

		if( true == $boolIsStudentProperty && false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_ROOMMATE'] ) && CCustomerType::PRIMARY == $intCustomerTypeId ) {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::ROOMMATES];

			if( false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_APPLICATION_APPLICANT_GROUPING'] ) && true == array_key_exists( 'application_roommates', $arrstrExitTags ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_roommates'] );
			} elseif( true == array_key_exists( 'edit_roommates', $arrstrExitTags ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['edit_roommates'] );
			}

			if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_ROOMMATES'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_ROOMMATES']->getValue() ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_ROOMMATES']->getValue() );
			} else {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Roommates' ) );
			}

			$intStepCount ++;
		}

		// DETERMINE FINANCIAL STEP

		if( false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_FINANCIAL'] ) ) {

			if( ( ( false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_INCOME'] ) || false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_ASSET'] ) || true == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_DEPOSIT_PREFERENCE'] ) ) || ( ( CCustomerType::PRIMARY == $intCustomerTypeId || ( true == valObj( $objApplicantApplication, 'CApplicantApplication' ) && false == $objApplicantApplication->getPrimaryIsResponsible() ) ) ) && ( true == isset( $this->m_arrobjPropertyApplicationPreferences['ALLOW_FILE_UPLOAD_CONTROL_FINANCIAL'] ) ) && true == $boolResponsiveWebsiteTemplate ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::FINANCIAL];

				if( true == array_key_exists( 'application_financial', $arrstrExitTags ) ) {
					$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_financial'] );
				}

				if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_FINANCIAL'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_FINANCIAL']->getValue() ) ) {
					$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_FINANCIAL']->getValue() );
				} else {
					$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Financial' ) );
				}

				$intStepCount ++;
			}
		}

		// DETERMINE PEOPLE STEP

		if( false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_PEOPLE'] ) && CCustomerType::PRIMARY == $intCustomerTypeId && false == in_array( $this->getLeaseIntervalTypeId(), [ CLeaseIntervalType::LEASE_MODIFICATION, CLeaseIntervalType::TRANSFER, CLeaseIntervalType::RENEWAL ] ) ) {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::PEOPLE];
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_people'] );

			if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_PEOPLE'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_PEOPLE']->getValue() ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_PEOPLE']->getValue() );
			} else {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'People' ) );
			}

			$intStepCount ++;
		}

		// DETERMINE CONTACTS STEP

		if( false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_CONTACTS'] ) ) {

			$boolShowGuarantorPersonalReference1	= false;
			$boolShowGuarantorPersonalReference2	= false;
			$boolHideContactsEmergencyContact		= false;
			$boolHideContactsPersonalReference1		= false;
			$boolHideContactsPersonalReference2		= false;

			if( true == valArr( $this->m_arrobjPropertyApplicationPreferences ) ) {
				if( true == array_key_exists( 'SHOW_GUARANTOR_PERSONAL_REFERENCE1', $this->m_arrobjPropertyApplicationPreferences ) && 1 == $this->m_arrobjPropertyApplicationPreferences['SHOW_GUARANTOR_PERSONAL_REFERENCE1']->getValue() ) {
					$boolShowGuarantorPersonalReference1 = true;
				}

				if( true == array_key_exists( 'SHOW_GUARANTOR_PERSONAL_REFERENCE2', $this->m_arrobjPropertyApplicationPreferences ) && 1 == $this->m_arrobjPropertyApplicationPreferences['SHOW_GUARANTOR_PERSONAL_REFERENCE2']->getValue() ) {
					$boolShowGuarantorPersonalReference2 = true;
				}

				if( true == array_key_exists( 'HIDE_CONTACTS_EMERGENCY_CONTACT', $this->m_arrobjPropertyApplicationPreferences ) && 1 == $this->m_arrobjPropertyApplicationPreferences['HIDE_CONTACTS_EMERGENCY_CONTACT']->getValue() ) {
					$boolHideContactsEmergencyContact = true;
				}

				if( true == array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_1', $this->m_arrobjPropertyApplicationPreferences ) && 1 == $this->m_arrobjPropertyApplicationPreferences['HIDE_CONTACTS_PERSONAL_REFERENCE_1']->getValue() ) {
					$boolHideContactsPersonalReference1 = true;
				}

				if( true == array_key_exists( 'HIDE_CONTACTS_PERSONAL_REFERENCE_2', $this->m_arrobjPropertyApplicationPreferences ) && 1 == $this->m_arrobjPropertyApplicationPreferences['HIDE_CONTACTS_PERSONAL_REFERENCE_2']->getValue() ) {
					$boolHideContactsPersonalReference2 = true;
				}
			}

			if( ( false == $boolHideContactsEmergencyContact && CCustomerType::GUARANTOR != $intCustomerTypeId )
				|| ( false == $boolHideContactsPersonalReference1 && ( ( CCustomerType::GUARANTOR == $intCustomerTypeId && true == $boolShowGuarantorPersonalReference1 ) || ( CCustomerType::GUARANTOR != $intCustomerTypeId ) ) )
				|| ( false == $boolHideContactsPersonalReference2 && ( ( CCustomerType::GUARANTOR == $intCustomerTypeId && true == $boolShowGuarantorPersonalReference2 ) || ( CCustomerType::GUARANTOR != $intCustomerTypeId ) ) ) ) {

				$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::CONTACTS];

				if( true == array_key_exists( 'application_contacts', $arrstrExitTags ) ) {
					$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_contacts'] );
				}

				if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_CONTACTS'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_CONTACTS']->getValue() ) ) {
					$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_CONTACTS']->getValue() );
				} else {
					$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Contacts' ) );
				}

				$intStepCount ++;
			}
		}

		// DETERMINE CUSTOM FORM STEP

		if( false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_APPLICATION_FORM'] ) ) {

			// If there is no application document then we don't need to load
			// this step - NRW
			$objCompanyApplication = $this->fetchCompanyApplication( $objClientDatabase );

			$intCustomerTypeId = ( CCustomerType::NON_LEASING_OCCUPANT == $intCustomerTypeId ) ? CCustomerType::RESPONSIBLE : $intCustomerTypeId;
			if( true == valObj( $objCompanyApplication, 'CCompanyApplication' ) && false == is_null( $objCompanyApplication->getApplicationDocumentIdByLeaseCustomerId( $intCustomerTypeId ) ) ) {

				$intDocumentComponentCount = CDocumentComponents::fetchCustomDocumentComponentCountByDocumentIdByCid( $objCompanyApplication->getApplicationDocumentIdByLeaseCustomerId( $intCustomerTypeId ), $this->getCid(), $objClientDatabase );

				if( 0 < $intDocumentComponentCount || ( true == valObj( $objApplicantApplication, 'CApplicantApplication' ) && false == is_null( $objApplicantApplication->getApplicationFormId() ) ) ) {

					$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::CUSTOM_FORM];

					if( true == array_key_exists( 'application_custom_data', $arrstrExitTags ) ) {
						$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_custom_data'] );
					}
					if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_APPLICATION'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_APPLICATION']->getValue() ) ) {
						$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_APPLICATION']->getValue() );
					} else {
						$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Questionnaire' ) );
					}
					$intStepCount ++;
				}
			}
		}

		// SUMMARY FORM STEP

		$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::SUMMARY];
		$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_summary'] );

		if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_SUMMARY'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_SUMMARY']->getValue() ) ) {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_SUMMARY']->getValue() );
		} else {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Summary' ) );
		}

		$intStepCount ++;

		// DETERMINE PAYMENT STEP

		$arrobjPropertyPaymentTypes = ( array ) CPropertyPaymentTypes::fetchPropertyPaymentTypesByAllowInEntrataByAllowInResidentPortalByAllowInProspectPortalByIsCertifiedFundsByPropertyIdByCid( $boolAllowInEntrata = NULL, $boolAllowInResidentPortal = NULL, $boolAllowInProspectPortal = true, $boolRequireCertified = NULL, $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		$intMerchantAccountCount = $this->m_objProperty->fetchActiveMerchantAccountCount( $objClientDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjPropertyPaymentTypes ) && ( CCustomerType::PRIMARY == $intCustomerTypeId || false == $boolSecondaryApplicant ) && false == isset( $this->m_arrobjPropertyApplicationPreferences['HIDE_PAYMENT'] ) && 0 < $intMerchantAccountCount ) {
			$this->m_arrobjCurrentApplicationSteps[$intStepCount] = $this->m_arrobjApplicationSteps[CApplicationStep::PAYMENT];
			$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setExitUri( $arrstrExitTags['application_payments'] );

			if( true == isset( $this->m_arrobjPropertyApplicationPreferences['RENAME_PAYMENT'] ) && 0 < strlen( $this->m_arrobjPropertyApplicationPreferences['RENAME_PAYMENT']->getValue() ) ) {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( $this->m_arrobjPropertyApplicationPreferences['RENAME_PAYMENT']->getValue() );
			} else {
				$this->m_arrobjCurrentApplicationSteps[$intStepCount]->setApplicationStepName( __( 'Payment' ) );
			}
		}

		ksort( $this->m_arrobjCurrentApplicationSteps );

		return $this->m_arrobjCurrentApplicationSteps;
	}

	public function loadCustomApplicationStepswithstepIds( $arrintApplicationstepIds ) {
		$arrobjApplicationSteps = rekeyObjects( 'Id', $this->m_arrobjCurrentApplicationSteps );
		$arrobjNewApplicationsteps = [];
		$intNewStepCount = 1;
		foreach( $arrintApplicationstepIds as $intApplicationStepId ) {
			if( true == array_key_exists( $intApplicationStepId, $arrobjApplicationSteps ) ) {
				$arrobjNewApplicationsteps[$intNewStepCount] = $arrobjApplicationSteps[$intApplicationStepId];
				$intNewStepCount ++;
			}
		}

		return $arrobjNewApplicationsteps;
	}

	public function loadDefaultLeasingAgentId( $objDatabase ) {
		if( false == isset( $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Database object was not loaded.' ), NULL ) );
			exit();
		}

		$objDefaultPropertyLeasingAgent = \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchDefaultPropertyLeasingAgentByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objDefaultPropertyLeasingAgent, 'CPropertyLeasingAgent' ) && true == is_null( $this->getLeasingAgentId() ) ) {
			$this->setLeasingAgentId( $objDefaultPropertyLeasingAgent->getCompanyEmployeeId() );
		}

		return true;
	}

	public function loadDefaultLeadSourceId( $objDatabase ) {
		if( true == is_null( $this->getPropertyId() ) )
			return;

		if( false == valObj( $this->getProperty(), 'CProperty' ) ) {
			$this->getOrFetchProperty( $objDatabase );
		}

		$objDefaultLeadSource = $this->getProperty()->fetchPublishedPropertyLeadSourceByCidByInternetListingServiceId( CInternetListingService::PROSPECT_PORTAL, $objDatabase );

		if( true == valObj( $objDefaultLeadSource, 'CPropertyLeadSource' ) ) {
			$this->setLeadSourceId( $objDefaultLeadSource->getLeadSourceId() );
		}

		return true;
	}

	public function loadDefaultSitetabletLeadSourceId( $objDatabase ) {
		if( true == is_null( $this->getPropertyId() ) )
			return;

		if( false == valObj( $this->getProperty(), 'CProperty' ) ) {
			$this->getOrFetchProperty( $objDatabase );
		}

		$objDefaultLeadSource = $this->getProperty()->fetchPublishedPropertyLeadSourceByCidByInternetListingServiceId( CInternetListingService::SITETABLET, $objDatabase );

		if( true == valObj( $objDefaultLeadSource, 'CPropertyLeadSource' ) ) {
			$this->setLeadSourceId( $objDefaultLeadSource->getLeadSourceId() );
		}

		return true;
	}

	public function loadPropertyId( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) )
			return;

		$this->fetchUnitSpace( $objDatabase );

		// in case of child property we need to set child property on
		// application - NRW
		if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) && $this->m_objUnitSpace->getPropertyId() != $this->getPropertyId() ) {
			$this->setPropertyId( $this->m_objUnitSpace->getPropertyId() );
			return true;
		}

		return false;
	}

	public function export( $intCompanyUserId, $objDatabase, $objPaymentDatabase = NULL, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {
		$boolIsValid 								= true;
		$boolExportPreQualifyApplicationAsGuestCard = false;
		$objProperty 								= $this->getOrFetchProperty( $objDatabase );

		$this->refreshFields( [ 'details', 'first_event_id', 'first_event_medium_id', 'guest_remote_primary_key', 'app_remote_primary_key' ], 'applications', $objDatabase );

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) || true == valStr( $this->getRemotePrimaryKey() ) || CApplicationStatus::CANCELLED == $this->getApplicationStatusId() ) {
			return true;
		}

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) return true;

		if( $objProperty->getId() != $this->getPropertyId() ) {
			$objProperty = $this->fetchProperty( $objDatabase );
		}

		$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );

		// Don't export the application if assigned unit does not belongs to property associated.
		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) && false == is_null( $objUnitSpace->getPropertyId() ) && false == is_null( $this->getPropertyId() ) && ( $objUnitSpace->getPropertyId() != $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Assigned unit does not belongs to property associated. Remove and re-asscociate unit to clone application.' ), NULL ) );
			return false;
		}

		if( CApplicationStage::PRE_QUALIFICATION == $this->getApplicationStageId() && CApplicationStatus::COMPLETED == $this->getApplicationStatusId()
			&& true == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::MRI, CIntegrationClientType::YARDI ] ) ) {

			$arrobjPropertyPreferences	= $objProperty->getOrFetchPropertyPreferences( $objDatabase );
			$objPropertyPreference		= getArrayElementByKey( 'EXPORT_PRE_QUALIFY_APPLICATION_AS_GUEST_CARD', $arrobjPropertyPreferences );
			if( true == valObj( $objPropertyPreference, 'CPropertyPreference', 'Value', 1 ) ) {
				$boolExportPreQualifyApplicationAsGuestCard = true;
			}
		}

		if( true == $this->isGuestCard() || true == $this->getIsForcedGuestCardIntegration() || true == $boolExportPreQualifyApplicationAsGuestCard ) {
			$intIntegrationServiceId = CIntegrationService::SEND_GUEST_CARD;
		} elseif( CLeaseIntervalType::APPLICATION == $this->m_intLeaseIntervalTypeId ) {

			$objPrimaryApplicantApplication = $this->fetchApplicantApplicationByCustomerTypeId( CCustomerType::PRIMARY, $objDatabase );

			$arrintApplicationStageStatusIds = [
				CApplicationStage::PRE_QUALIFICATION => [
					CApplicationStatus::STARTED,
					CApplicationStatus::COMPLETED,
					CApplicationStatus::APPROVED
				]
			];

			if( false == valObj( $objPrimaryApplicantApplication, 'CApplicantApplication' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Missing primary applicant::Cid:' ) . $this->getCid() . ' ' . __( 'Application Id:' ) . $this->getId(), NULL ) );
				return false;
			}

			if( true == valStr( $objPrimaryApplicantApplication->getCompletedOn() ) || true == $this->hasApplicationStageStatusIn( $arrintApplicationStageStatusIds ) ) {
				$intIntegrationServiceId = CIntegrationService::SEND_APPLICATION;
			} else {
				return true;
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No lease interval type set when export attempt was made.' ), NULL ) );
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, $intIntegrationServiceId, $intCompanyUserId, $objDatabase, $objPaymentDatabase );

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $objProperty );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		$boolIsValid = $objGenericWorker->process();

		if( true == $boolIsValid ) {

			if( true == valObj( $objPaymentDatabase, 'CDatabase' ) ) {

				$this->exportCharges( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolQueueSyncOverride );
				$this->exportArPayments( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolQueueSyncOverride, CIntegrationSyncType::QUEUE );
			}

			if( true == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::YARDI, CIntegrationClientType::MRI ] ) ) {
				$this->exportScreening( $intCompanyUserId, $objDatabase );
			}
		}

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) && true == $this->getIsShowIntegrationError() ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportUpdateMoveIn( $intCompanyUserId, $objProperty, $objDatabase ) {
		$boolIsValid                 = true;

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$objIntegrationDatabase = $objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) return true;

		if( $objIntegrationDatabase->getIntegrationClientTypeId() != CIntegrationClientType::EMH ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::UPDATE_MOVE_IN_DATE, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $objProperty );

		$boolIsValid = $objGenericWorker->process();

		if( true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportUpdate( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL, $arrstrTransportRules = [] ) {
		$this->getOrFetchProperty( $objDatabase );

		// Throw an error if Company Property is not integrated or the
		// application has already been exported.
		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) return true;

		if( true == is_null( $this->getGuestRemotePrimaryKey() ) && true == is_null( $this->getAppRemotePrimaryKey() ) ) return true;

		$objIntegrationDatabase = \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchIntegrationDatabaseByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );

		// Do not trigger updateApplication to Resident for Yardi SIPP
		if( CApplicationStage::LEASE == $this->getApplicationStageId() && true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase', 'IntegrationClientTypeId', CIntegrationClientType::YARDI_RPORTAL ) ) {
			return true;
		}

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || CIntegrationClientType::REAL_PAGE == $objIntegrationDatabase->getIntegrationClientTypeId() ) return true;

		if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && CIntegrationClientType::EMH == $objIntegrationDatabase->getIntegrationClientTypeId() && CApplicationStatus::CANCELLED == $this->getApplicationStatusId() ) {
			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::SEND_GUEST_CARD_CANCELLED, $intCompanyUserId, $objDatabase );
		} else {
			$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::UPDATE_APPLICATION, $intCompanyUserId, $objDatabase );
		}

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		$this->loadObjectStorageGateway( $objDatabase, $intCompanyUserId );

		$objGenericWorker->setObjectStorageGateway( $this->m_objObjectStorageGateway );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		if( true == valArr( $arrstrTransportRules ) ) {
			$objGenericWorker->setTransportRules( $arrstrTransportRules );
		}
		$boolIsValid = $objGenericWorker->process();

		if( true == $this->getIsShowIntegrationError() && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		$objIntegrationClientType = $this->getProperty()->fetchIntegrationClientType( $objDatabase );

		if( false == valObj( $objGenericWorker->getIntegrationClient(), 'CIntegrationClient' ) && true == valObj( $objIntegrationClientType, 'CIntegrationClientType' ) && CIntegrationClientType::JENARK != $objIntegrationClientType->getId() ) {
			$this->addErrorMsgs( [ new CErrorMsg( NULL, NULL, __( 'Application cannot integrate - updateApplication service is not added.' ), NULL ) ] );
			$boolIsValid = false;
		} elseif( true == valObj( $objGenericWorker->getIntegrationClient(), 'CIntegrationClient' ) && CIntegrationClientStatusType::DISABLED == $objGenericWorker->getIntegrationClient()->getIntegrationClientStatusTypeId() ) {
			$this->addErrorMsgs( [ new CErrorMsg( NULL, NULL, __( 'Application cannot integrate - updateApplication is disabled.' ), NULL ) ] );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function exportQuote( $objProperty, $objClient, $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false ) {

		// Throw an error if Company Property is not integrated or the

		if( false == valObj( $objProperty, 'CProperty', 'RemotePrimaryKey' ) ) return true;

		if( true == is_null( $this->getGuestRemotePrimaryKey() ) && true == is_null( $this->getAppRemotePrimaryKey() ) ) return true;

		$objGenericClientWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::SEND_QUOTE, $intCompanyUserId, $objDatabase );

		$objGenericClientWorker->setApplication( $this );
		$objGenericClientWorker->setProperty( $objProperty );
		$objGenericClientWorker->setDatabase( $objDatabase );
		$objGenericClientWorker->setClient( $objClient );
		$objGenericClientWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$boolIsValid = $objGenericClientWorker->process();

		return $boolIsValid;
	}

	public function exportLease( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {

		$this->getOrFetchProperty( $objDatabase );

		// Throw an error if Company Property is not integrated or the
		// application has already been exported.
		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$objIntegrationDatabase = $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );

		// Block exportLease call if lease is not generated.
		$intLeaseFileAssociationsCount = CFileAssociations::createService()->fetchFileAssociationCountByApplicationIdBySystemCodesByCid( $this->getId(), [ CFileType::SYSTEM_CODE_LEASE_ADDENDUM ], $this->getCid(), $objDatabase );
		if( 0 == $intLeaseFileAssociationsCount && true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) && false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::MRI, CIntegrationClientType::YARDI ] ) ) {
			return true;
		}

		if( ! valStr( $this->getRemotePrimaryKey() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Client ID[' ) . $this->getCid() . __( '] Application ID[' ) . $this->getId() . __( '] is not yet Integrated.' ), NULL ) );
			return false;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::EXECUTE_LEASE, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		$this->loadObjectStorageGateway( $objDatabase, $intCompanyUserId );

		$objGenericWorker->setObjectStorageGateway( $this->m_objObjectStorageGateway );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		return $objGenericWorker->process();
	}

	public function exportUpdateRenewal( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL, $arrstrTransportRules = [], $objLease ) {
		$this->getOrFetchProperty( $objDatabase );

		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) return true;

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->getCid(), CIntegrationService::UPDATE_RENEWAL_APPLICATION, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$objGenericWorker->setLease( $objLease );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		if( true == isset( $arrstrTransportRules ) ) {
			$objGenericWorker->setTransportRules( $arrstrTransportRules );
		}

		$boolIsValid = $objGenericWorker->process();

		if( true == $this->getIsShowIntegrationError() && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		if( false == valObj( $objGenericWorker->getIntegrationClient(), 'CIntegrationClient' ) ) {
			$this->addErrorMsgs( [ new CErrorMsg( NULL, NULL, __( 'Application cannot integrate - updateRenewalApplication service is not added.' ), NULL ) ] );
			$boolIsValid = false;
		} elseif( CIntegrationClientStatusType::DISABLED == $objGenericWorker->getIntegrationClient()->getIntegrationClientStatusTypeId() ) {
			$this->addErrorMsgs( [ new CErrorMsg( NULL, NULL, __( 'Application cannot integrate - updateRenewalApplication is disabled.' ), NULL ) ] );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function exportLeaseRenewal( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {
		$this->getOrFetchProperty( $objDatabase );

		// application has already been exported.
		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		// Block exportLease call if lease is not generated.
		$intLeaseFileAssociationsCount = CFileAssociations::createService()->fetchFileAssociationCountByApplicationIdBySystemCodesByCid( $this->getId(), [ CFileType::SYSTEM_CODE_LEASE_ADDENDUM ], $this->getCid(), $objDatabase );
		if( 0 == $intLeaseFileAssociationsCount ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::EXECUTE_LEASE_RENEWAL, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		$this->loadObjectStorageGateway( $objDatabase, $intCompanyUserId );

		$objGenericWorker->setObjectStorageGateway( $this->m_objObjectStorageGateway );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		return $objGenericWorker->process();
	}

	public function exportScreening( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false ) {

		// Fetch the property and check if its integrated or not.
		$this->getOrFetchProperty( $objDatabase );

		// If its not an application OR primary applicant is not completed OR no
		// property OR non-integrated property, then return true.
		if( CLeaseIntervalType::APPLICATION != $this->getLeaseIntervalTypeId() || false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) || ( false == valStr( $this->getAppRemotePrimaryKey() ) && false == valStr( $this->getGuestRemotePrimaryKey() ) ) ) {
			return true;
		}

		$objPrimaryApplicantApplication = $this->fetchApplicantApplicationByCustomerTypeId( CCustomerType::PRIMARY, $objDatabase );

		$arrintApplicationStageStatusIds = [ CApplicationStage::PRE_QUALIFICATION => [ CApplicationStatus::STARTED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ] ];

		if( false == valObj( $objPrimaryApplicantApplication, 'CApplicantApplication' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Application missing primary applicant:' ) . $this->getId(), NULL ) );
			return false;
		}

		if( false == valStr( $objPrimaryApplicantApplication->getCompletedOn() ) && false == $this->hasApplicationStageStatusIn( $arrintApplicationStageStatusIds ) ) {
			return true;
		}

		$objIntegrationDatabase = $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );

		$arrintAllowdScreeningIntegrationClietTypeIds = [ CIntegrationClientType::YARDI, CIntegrationClientType::MRI ];

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), $arrintAllowdScreeningIntegrationClietTypeIds ) ) {
			return true;
		}

		$objTransmission 							= $this->fetchLatestTransmission( $objDatabase );
		$objApplicantApplicationTransmission 		= $this->fetchLatestApplicantApplicationTransmission( $objDatabase );
		$objPrimaryApplicantApplicationTransmission = $this->fetchLatestPrimaryApplicantApplicationTransmission( $objDatabase );

		if( false == valObj( $objPrimaryApplicantApplicationTransmission, 'CApplicantApplicationTransmission' ) || false == valObj( $objTransmission, 'CTransmission' ) || false == valObj( $objApplicantApplicationTransmission, 'CApplicantApplicationTransmission' ) || true == valObj( $objApplicantApplicationTransmission, 'CApplicantApplicationTransmission', 'RemotePrimaryKey' ) ) {
			return true;
		}

		$boolIsResidentVerifyEnabled = $this->isResidentVerifyEnabled( $objDatabase );

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::SEND_SCREENING_INFO, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->m_objProperty );
		$objGenericWorker->setTransmission( $objTransmission );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$objGenericWorker->setIsResidentVerifyEnabled( $boolIsResidentVerifyEnabled );

		$boolIsValid = $objGenericWorker->process();

		if( true == $this->getIsShowIntegrationError() && true == valArr( $objGenericWorker->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objGenericWorker->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function exportCharges( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, $intArTriggerId = NULL ) {
		$this->getOrFetchProperty( $objDatabase );

		if( true == is_null( $this->m_objProperty ) || true == is_null( $this->m_objProperty->getRemotePrimaryKey() ) || true == is_null( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		$arrobjArTransactions = $this->fetchNonIntegratedApplicationCharges( $objDatabase, $intArTriggerId );

		if( false == valArr( $arrobjArTransactions ) )
			return true;

		$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchNonDeletedLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase, false );

		$objGenericWorker = CIntegrationFactory::createWorker( $this->m_objProperty->getId(), $this->m_intCid, CIntegrationService::SEND_AR_TRANSACTIONS, $intCompanyUserId, $objDatabase, $objPaymentDatabase );

		$objGenericWorker->setArTransactions( $arrobjArTransactions );
		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setLease( $objLease );
		$objGenericWorker->setProperty( $this->m_objProperty );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		return $objGenericWorker->process();
	}

	public function exportArPayments( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {
		$boolIsValid = true;
		$arrobjViewArPayments = [];

		$this->getOrFetchProperty( $objDatabase );

		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) )
			return true;

		$arrobjArPayments = $this->fetchNonIntegratedArPayments( $objDatabase );

		if( false == valArr( $arrobjArPayments ) ) {
			$arrobjArPayments = CArPayments::fetchUnexportedArPaymentsByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
		}

		if( true == valArr( $arrobjArPayments ) ) {

			$this->m_objClient = $this->getOrFetchClient( $objDatabase );

			if( false == valObj( $this->m_objClient, 'CClient' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load client' ), NULL ) );
				return false;
			}

			$arrobjViewArPayments = $this->m_objClient->fetchArPaymentsByIds( array_keys( $arrobjArPayments ), $objPaymentDatabase );
		}

		$objPropertyPreference = $this->m_objProperty->fetchPropertyPreferenceByKey( 'DONT_EXPORT_APPLICATION_PAYMENTS', $objDatabase );

		if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == valArr( $arrobjViewArPayments ) ) {
			foreach( $arrobjViewArPayments as $objViewArPayment ) {
				$boolIsValid &= $objViewArPayment->exportArPayment( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $intIntegrationSyncTypeId, $boolQueueSyncOverride );
			}
		}

		return $boolIsValid;
	}

	public function exportReverseCharges( $intCompanyUserId, $objDatabase, $objPaymentDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {

		$this->getOrFetchProperty( $objDatabase );

		if( true == is_null( $this->m_objProperty ) || true == is_null( $this->m_objProperty->getRemotePrimaryKey() ) || true == is_null( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->m_objProperty->getId(), $this->m_intCid, CIntegrationService::REVERSE_APPLICATION_CHARGES, $intCompanyUserId, $objDatabase, $objPaymentDatabase );

		$objGenericWorker->setArTransactions( NULL );
		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setLease( $this->fetchLease( $objDatabase ) );
		$objGenericWorker->setProperty( $this->m_objProperty );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		return $objGenericWorker->process();
	}

	public function exportTransmissions( $intCurrentUserId, $objDatabase, $arrintTransmissionVendorIds = NULL, $boolIsDuplicate = false ) {

		if( false == valArr( $arrintTransmissionVendorIds ) )
			$arrintTransmissionVendorIds = [
				CTransmissionVendor::LEAD_TO_LEASE,
				CTransmissionVendor::LEAD_TRACKING_SOLUTIONS,
				CTransmissionVendor::BLUE_MOON
			];

		$objProperty 						= $this->getOrFetchProperty( $objDatabase );
		$arrobjCompanyTransmissionVendors 	= $objProperty->fetchCompanyTransmissionVendorByTransmissionVendorIds( $arrintTransmissionVendorIds, $objDatabase );
		$objIntegrationDatabase 			= $objProperty->fetchIntegrationDatabase( $objDatabase );
		$arrintApplicantIds             	= CApplicants::fetchApplicantIdsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase, false );
		$this->m_arrobjApplicants			= ( array ) CApplicants::fetchApplicantsByIdsByApplicationIdByCid( $arrintApplicantIds, $this->getId(), $this->getCid(), $objDatabase );// this change as per task id :141921
		$arrobjPropertyPreferences 			= $this->m_objProperty->getOrFetchPropertyPreferences( $objDatabase );

		if( false == valArr( $arrobjCompanyTransmissionVendors ) )
			return true;

		foreach( $arrobjCompanyTransmissionVendors as $objCompanyTransmissionVendor ) {

			if( false == valObj( $objCompanyTransmissionVendor, 'CCompanyTransmissionVendor', 'RequestUrl' ) ) {
				continue;
			}

			if( CTransmissionVendor::BLUE_MOON == $objCompanyTransmissionVendor->getTransmissionVendorId()
				&& ( false == valObj( $objCompanyTransmissionVendor, 'CCompanyTransmissionVendor', 'Key' ) || false == $this->hasApplicationStageStatusIn( CApplicationStageStatus::$c_arrintCompletedApplicationStatusIds ) ) ) {
				continue;
			}

			if( true == $this->isGuestCard() && CTransmissionVendor::BLUE_MOON == $objCompanyTransmissionVendor->getTransmissionVendorId() ) {
				continue;
			}

			$objCompanyTransmissionVendor->setProperty( $objProperty );
			$objCompanyTransmissionVendor->setDatabase( $objDatabase );
			$objCompanyTransmissionVendor->setCompanyUserId( $intCurrentUserId );
			$objCompanyTransmissionVendor->setApplication( $this );
			$objCompanyTransmissionVendor->setClient( $this->getOrFetchClient( $objDatabase ) );

			$strProspectCode 	 = '';
			$strRemotePrimaryKey = NULL;

			if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) {
				$strRemotePrimaryKey = ( CIntegrationClientType::YARDI == $objIntegrationDatabase->getIntegrationClientTypeId() ) ? \Psi\CStringService::singleton()->substr( $this->getRemotePrimaryKey(), 1 ) : $this->getRemotePrimaryKey();
				$strProspectCode 	 = '';
			}

			$objCompanyTransmissionVendor->setKeyValueData( [
				'ProspectRemotePrimaryKey' 	=> $strRemotePrimaryKey,
				'ProspectCode' 				=> $strProspectCode,
				'Applicant' 				=> $this->getOrFetchPrimaryApplicant( $objDatabase ),
				'Applicants'				=> $this->m_arrobjApplicants,
				'PropertyPreferences'		=> $arrobjPropertyPreferences,
				'IsDuplicateLead'			=> $boolIsDuplicate
			] );
			$objCompanyTransmissionVendor->process();
		}
	}

	public function exportDocuments( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {

		$this->getOrFetchProperty( $objDatabase );

		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) return true;

		if( $this->hasGreaterApplicationStageStatusProgression( CApplicationStage::APPLICATION, CApplicationStatus::COMPLETED ) || false == valStr( $this->getAppRemotePrimaryKey() ) ) return true;

		$objIntegrationDatabase = $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );
		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::YARDI ] ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::UPLOAD_APPLICATION_DOCUMENTS, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		$this->loadObjectStorageGateway( $objDatabase, $intCompanyUserId );

		$objGenericWorker->setObjectStorageGateway( $this->m_objObjectStorageGateway );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		return $objGenericWorker->process();
	}

	public function exportLeaseDocuments( $intCompanyUserId, $intIntegrationServiceId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {

		$arrobjFiles = [];
		$this->getOrFetchProperty( $objDatabase );
		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {

			return true;
		}

		if( false == $this->hasApplicationStageStatus( CApplicationStage::LEASE, CApplicationStatus::APPROVED ) ) {

			return true;
		}

		$objIntegrationDatabase = $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );
		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::AMSI, CIntegrationClientType::YARDI_RPORTAL ] ) ) {

			return true;
		}

		// we dont want to add new integration client for uploadRenewalLeaseDocument or  uploadLeaseDocuments service
		if( CIntegrationClientType::YARDI_RPORTAL != $objIntegrationDatabase->getIntegrationClientTypeId() ) {
			if( CIntegrationService::UPLOAD_RENEWAL_LEASE_DOCUMENTS == $intIntegrationServiceId ) {
				$intIntegrationServiceId = CIntegrationService::EXECUTE_LEASE_RENEWAL;
			} else {
				$intIntegrationServiceId = CIntegrationService::EXECUTE_LEASE;
			}
		}

		$arrobjFiles      = CFiles::createService()->fetchUnExportedFilesByApplicationIdByFileTypeSystemCodesByCid( $this->getId(), [ CFileType::SYSTEM_CODE_LEASE_ADDENDUM ], $this->getCid(), $objDatabase );
		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, $intIntegrationServiceId, $intCompanyUserId, $objDatabase );
		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setFiles( $arrobjFiles );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );
		$this->loadObjectStorageGateway( $objDatabase, $intCompanyUserId );

		$objGenericWorker->setObjectStorageGateway( $this->m_objObjectStorageGateway );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		return $objGenericWorker->process();
	}

	public function exportGuestCardReceipt( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {

		$this->getOrFetchProperty( $objDatabase );

		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		if( false == valStr( $this->getGuestRemotePrimaryKey() ) ) {
			return false;
		}
		$objIntegrationDatabase = $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::EMH ] ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::SEND_GUEST_CARD_RECEIPT, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		return $objGenericWorker->process();
	}

	public function exportCancelledGuestCardReceipt( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false, $intIntegrationSyncTypeId = NULL ) {
		$this->getOrFetchProperty( $objDatabase );

		if( false == valObj( $this->m_objProperty, 'CProperty', 'RemotePrimaryKey' ) ) {
			return true;
		}

		if( CApplicationStatus::CANCELLED != $this->getApplicationStatusId() || false == valStr( $this->getGuestRemotePrimaryKey() ) ) {
			return false;
		}

		$objIntegrationDatabase = $this->m_objProperty->fetchIntegrationDatabase( $objDatabase );

		if( false == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) || false == in_array( $objIntegrationDatabase->getIntegrationClientTypeId(), [ CIntegrationClientType::EMH ] ) ) {
			return true;
		}

		$objGenericWorker = CIntegrationFactory::createWorker( $this->getPropertyId(), $this->m_intCid, CIntegrationService::SEND_CANCELLED_GUEST_CARD_RECEIPT, $intCompanyUserId, $objDatabase );

		$objGenericWorker->setApplication( $this );
		$objGenericWorker->setProperty( $this->getProperty() );
		$objGenericWorker->setQueueSyncOverride( $boolQueueSyncOverride );

		if( NULL != $intIntegrationSyncTypeId ) {
			$objGenericWorker->setIntegrationSyncTypeId( $intIntegrationSyncTypeId );
		}

		return $objGenericWorker->process();
	}

	public function calculateTermMonth() {
		if( true == is_null( $this->getLeaseStartDate() ) || true == is_null( $this->getTermMonth() ) )
			return NULL;

		$strLeaseEndDate = strtotime( date( 'Y-m-d', strtotime( $this->getLeaseStartDate() ) ) . ' +' . $this->getTermMonth() . ' month' );
		$arrstrLeaseEndDate = getdate( $strLeaseEndDate );
		$intLeaseMonth = $arrstrLeaseEndDate['mday'] - 1;
		$strLeaseTerm = $this->getLeaseStartDate() . ' - ' . $arrstrLeaseEndDate['mon'] . '/' . $intLeaseMonth . '/' . $arrstrLeaseEndDate['year'];

		return $strLeaseTerm;
	}

	// To update latest data to the previous guest card we need this. - NRW

	public function loadAdditionalData( $objExistingGuestCard ) {
		$boolIsUpdateRequired = false;

		if( false == valObj( $objExistingGuestCard, 'CApplication' ) || CLeaseIntervalType::APPLICATION == $objExistingGuestCard->getLeaseIntervalTypeId() )
			return false;

		if( false == is_null( $objExistingGuestCard->getPropertyFloorplanId() ) ) {
			$boolIsUpdateRequired = true;
			$this->setPropertyFloorplanId( $objExistingGuestCard->getPropertyFloorplanId() );
		}

		if( false == is_null( $objExistingGuestCard->getPropertyUnitId() ) ) {
			$boolIsUpdateRequired = true;
			$this->setPropertyUnitId( $objExistingGuestCard->getPropertyUnitId() );
		}

		if( false == is_null( $objExistingGuestCard->getUnitSpaceId() ) ) {
			$boolIsUpdateRequired = true;
			$this->setUnitSpaceId( $objExistingGuestCard->getUnitSpaceId() );
		}

		if( false == is_null( $objExistingGuestCard->getLeadSourceId() ) ) {
			$boolIsUpdateRequired = true;
			$this->setLeadSourceId( $objExistingGuestCard->getLeadSourceId() );
		}

		if( false == is_null( $objExistingGuestCard->getTermMonth() ) ) {
			$boolIsUpdateRequired = true;
			$this->setTermMonth( $objExistingGuestCard->getTermMonth() );
		}

		if( false == is_null( $objExistingGuestCard->getLeaseStartDate() ) ) {
			$boolIsUpdateRequired = true;
			$this->setLeaseStartDate( $objExistingGuestCard->getLeaseStartDate() );
		}

		if( false == is_null( $objExistingGuestCard->getDesiredRentMax() ) && 0 < ( int ) $objExistingGuestCard->getDesiredRentMax() ) {
			$boolIsUpdateRequired = true;
			$this->setDesiredRentMax( $objExistingGuestCard->getDesiredRentMax() );
		}

		if( false == is_null( $objExistingGuestCard->getDesiredRentMin() ) && 0 < ( int ) $objExistingGuestCard->getDesiredRentMin() ) {
			$boolIsUpdateRequired = true;
			$this->setDesiredRentMin( $objExistingGuestCard->getDesiredRentMin() );
		}

		if( false == is_null( $objExistingGuestCard->getDesiredBedrooms() ) ) {
			$boolIsUpdateRequired = true;
			$this->setDesiredBedrooms( $objExistingGuestCard->getDesiredBedrooms() );
		}

		if( false == is_null( $objExistingGuestCard->getDesiredBathrooms() ) ) {
			$boolIsUpdateRequired = true;
			$this->setDesiredBathrooms( $objExistingGuestCard->getDesiredBathrooms() );
		}

		if( false == is_null( $objExistingGuestCard->getInternetListingServiceId() ) ) {
			$boolIsUpdateRequired = true;
			$this->setInternetListingServiceId( $objExistingGuestCard->getInternetListingServiceId() );
		}

		return $boolIsUpdateRequired;
	}

	public function mapContactData( $objApplicationDuplicate ) {
		$this->setApplicationDuplicateId( $objApplicationDuplicate->getId() );
		$this->setContactDatetime( $objApplicationDuplicate->getContactDatetime() );
		$this->setIsTextViewed( $objApplicationDuplicate->getIsTextViewed() );
		$this->setIsSendText( $objApplicationDuplicate->getIsSendText() );
		$this->setLastContactDays( $objApplicationDuplicate->getLastContactDays() );
	}

	public function refreshApplicationDatetimeByCid( $objDatabase ) {
		$strSql = 'SELECT application_datetime FROM applications WHERE id = ' . $this->getId() . ' AND cid = ' . $this->getCid();
		$arrstrApplicationDatetime = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrstrApplicationDatetime[0]['application_datetime'] ) ) {
			$this->setApplicationDatetime( $arrstrApplicationDatetime[0]['application_datetime'] );
		}
	}

	public function checkStampSignatureOnPolicyDocuments( $objDatabase, $arrobjPropertyApplicationPreferences, $intApplicantId = NULL ) {
		$arrobjPolicyFiles = $this->fetchFilesByFileTypeSystemCodes( [ CFileType::SYSTEM_CODE_POLICY ], $objDatabase );

		if( false == valArr( $arrobjPolicyFiles ) ) {
			return false;
		}

		foreach( $arrobjPolicyFiles as $objPolicyFile ) {
			if( CFileExtension::APPLICATION_PDF != $objPolicyFile->getFileExtensionId() ) {
				return false;
			}
		}

		$intSignedFileAssociationsCount = CFileAssociations::createService()->fetchSignedFileAssociationCountByApplicationIdByFileIdsByCid( $this->getId(), array_keys( $arrobjPolicyFiles ), $this->getCid(), $objDatabase, $intApplicantId );

		if( 0 < $intSignedFileAssociationsCount ) {
			return true;
		}

		if( true == valArr( $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'STAMP_SIGNATURE_ON_POLICY_DOCUMENT', $arrobjPropertyApplicationPreferences ) && false == is_null( $arrobjPropertyApplicationPreferences['STAMP_SIGNATURE_ON_POLICY_DOCUMENT']->getValue() ) ) {
			return true;
		}

		return false;
	}

	public function logActivityForApplication( $arrmixData, $objReference, $intEventType, $objCompanyUser, $objDatabase, $intSkippedApplicationId = NULL ) {

		$objEventLibrary = new CEventLibrary();

		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setLease( $this->m_objLease );
		$objEventLibraryDataObject->setApplication( $this );
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objSelectedEvent = $objEventLibrary->createEvent( [ $objReference ], $intEventType );
		$objSelectedEvent->setCid( $this->getCid() );
		$objSelectedEvent->setLeaseId( $this->getLeaseId() );
		$objSelectedEvent->setLeaseIntervalId( $this->getLeaseIntervalId() );
		$objSelectedEvent->setCompanyUser( $objCompanyUser );

		if( true == valArr( $arrmixData ) && true == valId( $arrmixData['customer_id'] ) ) {
			$objSelectedEvent->setCustomerId( $arrmixData['customer_id'] );
		}

		$objSelectedEvent->setEventDatetime( 'NOW()' );
		$objEventLibraryDataObject->setData( $arrmixData );

		if( CEventType::RENEWAL_OFFER_REVIEWED == $intEventType ) {
			$objEventLibraryDataObject->setCustomer( $this->m_objCustomer );
			$objSelectedEvent->setNewStatusId( $objReference->getApplicationStatusId() );
			$objSelectedEvent->setNewStageId( $objReference->getApplicationStageId() );
		}

		$objEventLibrary->buildEventDescription( $this );
		 return $objEventLibrary;
	}

	// Parent form setting

	/**
	 * @description:
	 *      validate Process Screening on Application Completed
	 *      Validate on Process Screening Callback with validation message
	 *      Validate to show Process Screening Block
	 *      Validate age
	 *      validate on process screening applicants window with error message
	 *      validate to show resend button on edit application and activity log
	 * @param      $objDatabase
	 * @param bool $boolCheckIsFormedSigned
	 * @return bool
	 */
	public function checkParentalConsentDetailsRequired( $objDatabase, $boolCheckIsFormSigned = true, $objApplicant = NULL ) {

		$boolIsMinorApplicant              = false;
		$boolIsParentalConsentFormRequired = false;
		$objProperty                       = $this->getOrFetchProperty( $objDatabase );

		if( true == CScreeningUtils::checkPCFRequiredSettings( $objProperty, $objDatabase, $this ) ) {

			if( true == valObj( $objApplicant, 'CApplicant' ) ) {
				$boolIsMinorApplicant = $this->validateParentConsentRequiredApplicantAge( $objApplicant, $objDatabase );
			} else {
				$boolIsMinorApplicant = $this->validateParentConsentRequiredApplicantsAge( $objDatabase );
			}

			if( true == $boolIsMinorApplicant ) {

				//  if it is not required to check whether PCF document is signed, then return true
				if( false == $boolCheckIsFormSigned ) {
					return true;
				}

				// validate if minor applicant has form signed or not
				if( true == $this->checkHasAllApplicantsParentConsentFormSigned( $objDatabase ) ) {
					$boolIsParentalConsentFormRequired = true;
				}
			} else {
                $boolIsParentalConsentFormRequired = false;
            }
		}

		return $boolIsParentalConsentFormRequired;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchScreeningApplicationRequest( $objDatabase, $boolIncludeCancelledScreeningApplicationRequest = false ) {
		return CScreeningApplicationRequests::fetchScreeningApplicationRequestByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolIncludeCancelledScreeningApplicationRequest );
	}

	public function fetchScreeningApplicantApplicationsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase, $boolIncludeDeletedAA = false, $boolIncludeCancelledScreeningApplicant = false ) {
		if( false == valArr( $arrintCustomerTypeIds ) ) return NULL;
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchScreeningApplicantApplicationsByApplicationIdByCustomerTypeIdsByCid( $this->getId(), $arrintCustomerTypeIds, false, $this->getCid(), $objDatabase, $boolIncludeDeletedAA, $boolIncludeCancelledScreeningApplicant );
	}

	public function fetchScreeningApplicationRequestByRequestTypeIds( $arrintScreeningRequestTypeIds, $objDatabase ) {
		return CScreeningApplicationRequests::fetchScreeningApplicationRequestByRequestTypeIdsByApplicationIdByCid( $arrintScreeningRequestTypeIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fechCompanyMediaFiles( $objDatabase, $intCompanyMediaFileId = NULL ) {
		return CCompanyMediaFiles::createService()->fetchCompanyMediaFilesByApplicationIdByMediaTypeIdByCid( $this->getId(), CMediaType::APPLICATION, $this->getCid(), $objDatabase, $intCompanyMediaFileId );
	}

	public function fetchCustomCompanyMediaFiles( $objDatabase, $intCompanyMediaFileId = NULL ) {
		return CCompanyMediaFiles::createService()->fetchCustomCompanyMediaFilesByApplicationIdByMediaTypeIdByCid( $this->getId(), CMediaType::APPLICATION, $this->getCid(), $objDatabase, $intCompanyMediaFileId );
	}

	// This function is used to fetch the rent company rate.

	public function fetchRateByChargeCodeId( $intArCodeId, $objDatabase ) {
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $this->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $this->getPropertyId() ] );
		$objRateLogsFilter->setArCodeIds( [ $intArCodeId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE ] );

		if( true == valObj( $this->getOrFetchUnitSpace( $objDatabase ), 'CUnitSpace' ) ) {
			$objRateLogsFilter->setUnitSpaceIds( [ $this->getOrFetchUnitSpace( $objDatabase )->getId() ] );
		}

		return \Psi\Eos\Entrata\CRateLogs::createService()->fetchRateLogsByRateLogsFilter( $objRateLogsFilter, $objDatabase );
	}

	public function fetchApplicantApplicationById( $intApplicantApplicationId, $objDatabase, $boolIncludeDeletedAA = false ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByIdByApplicationIdByCid( $intApplicantApplicationId, $this->getId(), $this->getCid(), $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchApplicantIdsByApplicantApplicationIds( $arrintApplicantApplicationIds, $objDatabase ) {
		return CApplicants::fetchApplicantIdsByApplicantApplicationIdsByApplicationIdByCid( $arrintApplicantApplicationIds, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPrimaryApplicant( $objDatabase, $boolLoadFromView = true ) {
		return CApplicants::fetchApplicantByApplicationIdByCustomerTypeIdByCid( $this->m_intId, CCustomerType::PRIMARY, $this->getCid(), $objDatabase, $boolLoadFromView, false );
	}

	public function fetchApplicants( $objDatabase, $boolLoadFromView = true, $boolIncludeDeletedAA = false ) {
		return CApplicants::fetchApplicantsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolLoadFromView, $boolIncludeDeletedAA );
	}

	public function fetchScreenedApplicants( $objDatabase ) {
		return CApplicants::fetchScreenedApplicantsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationByCustomerTypeId( $intCustomerTypeId, $objDatabase, $boolIncludeDeletedAA = false ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByApplicationIdByCustomerTypeIdByCid( $this->m_intId, $intCustomerTypeId, $this->m_intCid, $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchFirstLeaseGeneratedApplicantApplication( $objDatabase, $boolConsiderUploadedLease = false, $boolIncludeDeletedAA = false ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchFirstLeaseGeneratedApplicantApplicationByApplicationIdByCid( $this->m_intId, $this->m_intCid, $objDatabase, $boolConsiderUploadedLease, $boolIncludeDeletedAA );
	}

	public function fetchLatestLeaseGeneratedApplicantApplication( $objDatabase, $boolConsiderUploadedLease = false, $boolIncludeDeletedAA = false ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchLatestLeaseGeneratedApplicantApplicationByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase, $boolConsiderUploadedLease, $boolIncludeDeletedAA );
	}

	public function fetchMoneygramAccount( $intCustomerId, $intLeaseId, $objDatabase, $objPaymentDatabase ) {
		$this->m_objMoneyGramAccount = \Psi\Eos\Payment\CMoneyGramAccounts::createService()->fetchOrCreateMoneyGramAccountByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $this->getCid(), $objDatabase, $objPaymentDatabase );
		return $this->m_objMoneyGramAccount;
	}

	public function fetchLeadSource( $objDatabase ) {
		$this->m_objLeadSource = \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourceByIdByCid( $this->getLeadSourceId(), $this->getCid(), $objDatabase );
		return $this->m_objLeadSource;
	}

	public function fetchFileAssociationsByFileTypeSystemCode( $strSystemCode, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicationIdByFileTypeSystemCodeByCid( $this->getId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchAllApplicationAndApplicantApplicationFileAssociationsByFileTypeSystemCode( $strSystemCode, $objDatabase ) {
		return CFileAssociations::createService()->fetchAllApplicationAndApplicantApplicationFileAssociationsByApplicationIdByFileTypeSystemCodeByCid( $this->getId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsCountByFileTypeSystemCode( $strSystemCode, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsCountByApplicationIdByFileTypeSystemCodeByCid( $this->getId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsBySystemCodes( $arrstrSystemCodes, $objDatabase ) {
		if( false == valArr( $arrstrSystemCodes ) )
			return NULL;

		return CFileAssociations::createService()->fetchFileAssociationsByApplicationIdBySystemCodesByCid( $this->getId(), $arrstrSystemCodes, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationCountBySystemCodes( $arrstrSystemCodes, $objDatabase ) {
		if( false == valArr( $arrstrSystemCodes ) ) return NULL;

		return CFileAssociations::createService()->fetchFileAssociationCountByApplicationIdBySystemCodesByCid( $this->getId(), $arrstrSystemCodes, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsByApplicantIdsBySystemCodes( $arrintApplicantIds, $arrstrSystemCodes, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicantIdsByApplicationIdByDocumentIdBySystemCodesByCid( $arrintApplicantIds, $this->getId(), $this->getLeaseDocumentId(), $arrstrSystemCodes, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociations( $objDatabase, $boolFetchAll = false ) {
		return CFileAssociations::createService()->fetchCustomFileAssociationsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolFetchAll );
	}

	public function fetchPropertyLeadSource( $objDatabase ) {
		if( true == is_null( $this->getLeadSourceId() ) )
			return NULL;

		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourceByLeadSourceIdByPropertyIdByCid( $this->getLeadSourceId(), $this->m_intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeasingAgent( $objDatabase ) {
		if( true == is_null( $this->m_intLeasingAgentId ) )
			return NULL;

		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentByCompanyEmployeeIdByPropertyIdByCid( $this->m_intLeasingAgentId, $this->m_intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicationLeasingAgent( $objDatabase ) {
		if( true == is_null( $this->m_intLeasingAgentId ) )
			return NULL;

		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentByCompanyEmployeeByCid( $this->m_intLeasingAgentId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyEmployee( $objDatabase ) {
		if( true == is_null( $this->getLeasingAgentId() ) )
			return NULL;

		return \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->getLeasingAgentId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyAndLoadDisplayInfo( $objDatabase ) {
		if( false == is_numeric( $this->getPropertyId() ) )
			return NULL;

		$this->getOrFetchProperty( $objDatabase );
		$this->getProperty()->fetchOverviewMarketingMediaAssociation( $objDatabase );
		$this->getProperty()->getOrFetchOfficePhoneNumber( $objDatabase );
		$this->getProperty()->getOrFetchPrimaryPropertyAddress( $objDatabase );

		return $this->getProperty();
	}

	public function fetchPropertyFloorplanAndPropertyUnitAndUnitSpaceWithDisplayInfo( $objDatabase ) {
		if( true == is_numeric( $this->getPropertyId() ) ) {
			$this->getOrFetchPropertyFloorplan( $objDatabase );
			$this->getOrFetchPropertyUnit( $objDatabase );
			$this->getOrFetchUnitSpace( $objDatabase );

			if( true == isset( $this->m_objPropertyFloorplan ) && true == valObj( $this->m_objPropertyFloorplan, 'CPropertyFloorplan' ) ) {
				$this->m_objPropertyFloorplan->fetchActiveMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::FLOORPLAN_2D, $objDatabase );
			}

			if( true == valObj( $this->m_objPropertyUnit, 'CPropertyUnit' ) ) {
				$this->m_objPropertyUnit->fetchPropertyBuilding( $objDatabase );
			}
		}
	}

	public function fetchAmenityLeaseAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAmenityLeaseAssociations::createService()->fetchAmenityLeaseAssociationsByLeaseIdByLeaseIntervalIdByCid( $this->getLeaseId(),  $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicationSteps( $objDatabase ) {
		$this->m_arrobjApplicationSteps = CApplicationSteps::fetchAllApplicationSteps( $objDatabase );
		return $this->m_arrobjApplicationSteps;
	}

	public function fetchApplicantApplicationByApplicantId( $intApplicantId, $objDatabase, $boolIncludeDeletedAA = false ) {
		$this->m_objApplicantApplication = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByApplicationIdByApplicantIdByCid( $this->getId(), $intApplicantId, $this->getCid(), $objDatabase, $boolIncludeDeletedAA );
		return $this->m_objApplicantApplication;
	}

	public function fetchPropertyApplicationPreferences( $objDatabase, $intPropertyId = NULL ) {
		if( true == is_null( $this->getCompanyApplicationId() ) )
			return;
		if( true == is_null( $this->getPropertyId() ) )
			return;

		if( true == is_null( $intPropertyId ) ) {
			$this->m_arrobjPropertyApplicationPreferences = CPropertyApplicationPreferences::fetchPropertyApplicationPreferencesByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $this->getCompanyApplicationId(), $this->getPropertyId(), CApplicationPreferenceType::TYPE_GLOBAL, $this->getCid(), $objDatabase );
		} else {
			$this->m_arrobjPropertyApplicationPreferences = CPropertyApplicationPreferences::fetchPropertyApplicationPreferencesByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $this->getCompanyApplicationId(), $intPropertyId, CApplicationPreferenceType::TYPE_GLOBAL, $this->getCid(), $objDatabase );
		}

		$this->m_arrobjPropertyApplicationPreferences = ( array ) rekeyObjects( 'Key', $this->m_arrobjPropertyApplicationPreferences );

		return $this->m_arrobjPropertyApplicationPreferences;
	}

	public function fetchPropertyApplicationPreferenceByKey( $strKey, $objDatabase ) {
		if( true == is_null( $this->getCompanyApplicationId() ) )
			return;
		if( true == is_null( $this->getPropertyId() ) )
			return;
		if( true == is_null( $strKey ) || 0 == strlen( $strKey ) )
			return;

		$intApplicationPreferenceTypeId = CApplicationPreferenceType::TYPE_GLOBAL;

		return CPropertyApplicationPreferences::fetchPropertyApplicationPreferenceByKeyByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $strKey, $this->getCompanyApplicationId(), $this->getPropertyId(), $intApplicationPreferenceTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyApplicationPreferencesByKeys( $arrstrKeys, $objDatabase ) {
		if( true == is_null( $this->getCompanyApplicationId() ) )
			return;
		if( true == is_null( $this->getPropertyId() ) )
			return;
		if( false == valArr( $arrstrKeys ) )
			return;

		return CPropertyApplicationPreferences::fetchPropertyApplicationPreferencesByPropertyIdByCompanyApplicationIdByKeysByApplicationPreferenceTypeIdByCid( $this->getPropertyId(), $this->getCompanyApplicationId(), $arrstrKeys, CApplicationPreferenceType::TYPE_GLOBAL, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyApplication( $objDatabase ) {
		if( true == is_null( $this->getCompanyApplicationId() ) )
			return NULL;

		return CCompanyApplications::fetchCompanyApplicationByIdByCid( $this->getCompanyApplicationId(), $this->getCid(), $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		return $this->m_objProperty;
	}

	public function fetchClient( $objDatabase ) {
		$this->m_objClient = CClients::fetchClientById( $this->getCid(), $objDatabase );
		return $this->m_objClient;
	}

	public function fetchPropertyFloorplan( $objDatabase ) {
		if( true == is_null( $this->getPropertyFloorplanId() ) || false == is_numeric( $this->getPropertyFloorplanId() ) )
			return NULL;

		$this->m_objPropertyFloorplan = CPropertyFloorplans::createService()->fetchPropertyFloorplanByIdByCid( $this->getPropertyFloorplanId(), $this->getCid(), $objDatabase );
		return $this->m_objPropertyFloorplan;
	}

	public function fetchPropertyUnit( $objDatabase ) {
		if( true == is_null( $this->getPropertyUnitId() ) || false == is_numeric( $this->getPropertyUnitId() ) )
			return NULL;

		$this->m_objPropertyUnit = CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objDatabase );
		return $this->m_objPropertyUnit;
	}

	public function fetchUnitSpace( $objDatabase ) {
		if( true == is_null( $this->getUnitSpaceId() ) || false == is_numeric( $this->getUnitSpaceId() ) )
			return NULL;

		$this->m_objUnitSpace = CUnitSpaces::createService()->fetchCustomUnitSpaceByIdByCid( $this->getUnitSpaceId(), $this->getCid(), $objDatabase );
		return $this->m_objUnitSpace;
	}

	public function fetchDocumentAddendas( $objDatabase ) {
		return CDocumentAddendas::fetchDocumentAddendasByCompanyApplicationIdByCid( $this->m_intCompanyApplicationId, $this->getCid(), $objDatabase );
	}

	public function fetchEvents( $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByPropertyIdByLeaseIntervalIdByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );
	}

	public function fetchEventById( $intEventId, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventByIdByCid( $intEventId, $this->getCid(), $objDatabase );
	}

	public function fetchEventsByEventTypeIds( $arrintEventTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByPropertyIdByLeaseIntervalIdByEventTypeIdsByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), $arrintEventTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchTotalEventsCountByEventTypeIds( $arrintEventTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchTotalEventsCountByPropertyIdByLeaseIntervalIdByEventTypeIdsByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), $arrintEventTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchEventByEventTypeIdByNewStatusId( $intEventTypeId, $intNewStageId, $intNewStatusId, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventByPropertyIdByLeaseIntervalIdByEventTypeIdByNewStatusIdByNewStageIdByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), $intEventTypeId, $intNewStageId, $intNewStatusId, $this->getCid(), $objDatabase );
	}

	public function fetchLastEventByEventTypeIds( $arrintEventTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchLastEventByLeaseIntervalIdByEventTypeIdsByCid( $this->getLeaseIntervalId(), $arrintEventTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPolicyDocumentsByIdsByCustomerTypeIdByCid( $arrintDocumentAddendaIds, $intCustomerTypeId, $objDatabase ) {
		if( false == valArr( $arrintDocumentAddendaIds ) )
			return NULL;

		$strSql = 'SELECT
						d.*,
						da.company_application_id,
						da.id as document_addenda_id,
						da.attach_to_email,
						da.is_for_primary_applicant,
						da.is_for_co_applicant,
						da.is_for_co_signer,
						da.require_confirmation
					 FROM
						documents d JOIN document_addendas da ON( d.id = da.document_id AND d.cid = da.cid AND d.id IN(  ' . implode( ',', $arrintDocumentAddendaIds ) . ') AND d.cid = ' . $this->getCid() . ' )
					 WHERE
						da.deleted_by IS NULL ';

		switch( ( int ) $intCustomerTypeId ) {

			case CCustomerType::PRIMARY:
				$strSql .= ' AND is_for_primary_applicant = 1';
				break;

			case CCustomerType::RESPONSIBLE:
				$strSql .= ' AND is_for_co_applicant = 1';
				break;

			case CCustomerType::GUARANTOR:
				$strSql .= ' AND is_for_co_signer = 1';
				break;

			default:
		}

		return CDocuments::fetchDocuments( $strSql, $objDatabase );
	}

	public function fetchApplicantApplications( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchSimpleApplicantApplicationsByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationsByApplicantIds( $arrintApplicantIds, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintApplicantIds ) )
			return NULL;

		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicantIdsByApplicationIdByCid( $arrintApplicantIds, $this->m_intId, $this->getCid(), $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase, $boolLoadFromView = true, $boolLoadOnlyResponsibleApplicant = false, $boolIncludeDeletedAA = false, $intNonResponsibleLimit = NULL, $boolIsUnderAgeLimit = false, $boolLoadNewApplicantsOnly = false ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		return CApplicants::fetchApplicantsByApplicationIdByCustomerTypeIdsByCid( $this->m_intId, $arrintCustomerTypeIds, $this->getCid(), $objDatabase, $boolLoadFromView, $boolLoadOnlyResponsibleApplicant, $boolIncludeDeletedAA, $intNonResponsibleLimit, $boolIsUnderAgeLimit, $boolLoadNewApplicantsOnly );
	}

	public function fetchInactiveApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		return CApplicants::fetchInactiveApplicantsByApplicationIdByCustomerTypeIdsByCid( $this->m_intId, $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerRelationshipsByOccupancyTypeIds( $arrintOccupancyTypeIds, $objDatabase ) {
		return CCustomerRelationships::fetchCustomerRelationshipsByOccupancyTypeIdsByPropertyIdByCid( $arrintOccupancyTypeIds, $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnApprovedApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		return CApplicants::fetchUnApprovedApplicantsByApplicationIdByCustomerTypeIdsByCid( $this->m_intId, $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchPendingApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $intRequiredApplicantAge, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		return CApplicants::fetchPendingApplicantsByApplicationIdByCustomerTypeIdsByCid( $this->m_intId, $arrintCustomerTypeIds, $intRequiredApplicantAge, $this->getCid(), $objDatabase, $boolIncludeDeletedAA = false, $this->getLeaseIntervalId() );
	}

	public function fetchPendingLeaseApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $intRequiredApplicantAge, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		return CApplicants::fetchPendingLeaseApplicantsByApplicationIdByCustomerTypeIdsByCid( $this->m_intId, $arrintCustomerTypeIds, $intRequiredApplicantAge, $this->getCid(), $objDatabase );
	}

	public function fetchUsApplicantsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase, $boolIncludeAllApplicants = false ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		return CApplicants::fetchUsApplicantsByApplicationIdByCustomerTypeIdsByCid( $this->m_intId, $arrintCustomerTypeIds, $this->getCid(), $objDatabase, false, $boolIncludeAllApplicants );
	}

	public function fetchApplicantById( $intApplicantId, $objDatabase, $boolIncludeDeletedAA = false ) {
		return CApplicants::fetchApplicantByIdByApplicationIdByCid( $intApplicantId, $this->getId(), $this->getCid(), $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchPrimaryApplicantInfo( $objDatabase ) {
		return CApplicants::fetchPrimaryApplicantByIdByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantByCustomerId( $intCustomerId, $objDatabase ) {
		return CApplicants::fetchApplicantByApplicationIdByCustomerIdByCid( $this->getId(), $intCustomerId, $this->getCid(), $objDatabase );
	}

	public function fetchLease( $objDatabase ) {
		if( true == is_null( $this->getLeaseId() ) )
			return NULL;

		$this->m_objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchNonDeletedLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

		return $this->m_objLease;
	}

	public function fetchPropertyUnitNumberByPropertyUnitId( $intPropertyUnitId, $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitNumberByPropertyUnitIdByApplicationIdByCid( $intPropertyUnitId, $this->getId(), $this->getCid(), $objDatabase );
	}

	// TRANSMISSION FUNCTIONS STARTS HERE :-

	public function fetchLatestTransmissionByTransmissionTypeId( $intTransmissionTypeId, $objDatabase ) {
		return CTransmissions::fetchTransmissionByApplicationIdByTransmissionTypeIdByLimitOneByCid( $this->getId(), $intTransmissionTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchTransmissionsByTransmissionTypeIdByTransmissionVendorId( $intTransmissionTypeId, $intTransmissionVendorId, $objDatabase ) {
		return CTransmissions::fetchTransmissionsByApplicationIdByTransmissionTypeIdByTransmissionVendorIdByCid( $this->getId(), $intTransmissionTypeId, $intTransmissionVendorId, $this->getCid(), $objDatabase );
	}

	public function fetchTransmissions( $objDatabase ) {
		return CTransmissions::fetchCustomTransmissionsByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchTransmissionById( $intTransmissionId, $objDatabase ) {
		return CTransmissions::fetchTransmissionByIdByCid( $intTransmissionId, $this->getCid(), $objDatabase );
	}

	public function fetchLatestTransmissionsByApplicantApplicationIds( $arrintApplicantApplicationIds, $objDatabase ) {
		return CTransmissions::fetchLatestTransmissionsByApplicantApplicationIdsByCid( $this->m_intId, $arrintApplicantApplicationIds, $this->getCid(), $objDatabase );
	}

	public function fetchLatestTransmission( $objDatabase ) {
		return CTransmissions::fetchLatestTransmissionByApplicationIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchLatestPrimaryApplicantApplicationTransmission( $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchLatestPrimaryApplicantApplicationTransmissionByApplicationIdByCid( $this->getId(), $this->m_intCid, $objDatabase );
	}

	public function fetchApplicantApplicationTransmissionsByApplicantApplicationIds( $arrintApplicantApplicationIds, $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchApplicantApplicationTransmissionsByApplicationIdByApplicantApplicationIdsByCid( $this->getId(), $arrintApplicantApplicationIds, $this->m_intCid, $objDatabase );
	}

	public function fetchApplicantApplicationTransmissions( $objDatabase, $boolIncludeDeletedAA = false ) {
		return CApplicantApplicationTransmissions::fetchApplicantApplicationTransmissionsByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchApplicantApplicationTransmissionsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase, $boolIncludeDeletedAA = false ) {
		return CApplicantApplicationTransmissions::fetchApplicantApplicationTransmissionsByApplicationIdByCustomerTypeIdsByCid( $this->m_intId, $arrintCustomerTypeIds, $this->m_intCid, $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchApplicantApplicationsByApplicationId( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchLatestApplicantApplicationTransmission( $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchLatestApplicantApplicationTransmissionByApplicationIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchLatestGuarantorApplicantApplicationTransmission( $objDatabase, $boolIncludeDeletedAA = false ) {
		return CApplicantApplicationTransmissions::fetchLatestGuarantorApplicantApplicationTransmissionByApplicationIdByCid( $this->m_intId, $this->m_intCid, $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchLatestApplicantApplicationTransmissionsByApplicantApplicationIds( $arrintApplicantApplicationIds, $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchLatestApplicantApplicationTransmissionsByApplicantApplicationIdsByCid( $arrintApplicantApplicationIds, $this->getCid(), $objDatabase );
	}

	public function fetchLatestApplicantApplicationTransmissionsByApplicationIdByApplicantApplicationIds( $arrintApplicantApplicationIds, $intApplicationId, $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchLatestApplicantApplicationTransmissionsByApplicationIdByApplicantApplicationIdsByCid( $intApplicationId, $arrintApplicantApplicationIds, $this->getCid(), $objDatabase );
	}

	public function fetchLatestApplicantApplicationTransmissionByApplicationIdByApplicantApplicationId( $intApplicantApplicationId, $intApplicationId, $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchLatestApplicantApplicationTransmissionByApplicationIdByApplicantApplicationIdByCid( $intApplicationId, $intApplicantApplicationId, $this->getCid(), $objDatabase );
	}

	public function fetchTotalLatestApplicantApplicationTransmissionsCountForTransmissionApprovedByApplicantApplicationIdsByCustomerTypeIds( $arrintApplicantApplicationIds, $arrintCustomerTypeIds, $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchTotalLatestApplicantApplicationTransmissionsForTransmissionApprovedByApplicantApplicationIdsByCustomerTypeIdsByCid( $arrintApplicantApplicationIds, $arrintCustomerTypeIds, $this->m_intCid, $objDatabase );
	}

	public function fetchLatestApplicantApplicationTransmissionsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchLatestApplicantApplicationTransmissionsByApplicationIdByCustomerTypeIdsByCid( $this->getId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchTotalLatestApplicantApplicationTransmissionsCountByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchTotalLatestApplicantApplicationTransmissionsCountByApplicationIdByCustomerTypeIdsByCid( $this->getId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchApplicant( $objDatabase ) {
		$this->m_objApplicant = CApplicants::fetchApplicantByIdByCid( $this->getApplicantId(), $this->getCid(), $objDatabase );
		return $this->m_objApplicant;
	}

	public function fetchTotalApplicantsCountByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase ) {
		return CApplicants::fetchTotalApplicantsCountByApplicationIdByCustomerTypeIdsByCid( $this->getId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchTransmissionConditions( $objDatabase ) {
		return CTransmissionConditions::fetchTransmissionConditionsByCidByApplicationId( $this->getCid(), $this->getId(), $objDatabase );
	}

	public function fetchTransmissionConditionById( $intTransmissionConditionId, $objDatabase ) {
		return CTransmissionConditions::fetchTransmissionConditionsByCidByApplicationIdById( $this->getCid(), $this->getId(), $intTransmissionConditionId, $objDatabase );
	}

	public function fetchTransmissionConditionByConditionTypeIds( $arrintTransmissionConditionTypeIds, $objDatabase ) {
		return CTransmissionConditions::fetchTransmissionConditionsByCidByApplicationIdByTransmissionConditionTypeIds( $this->getCid(), $this->getId(), $arrintTransmissionConditionTypeIds, $objDatabase );
	}

	public function fetchApplicationDuplicate( $objDatabase ) {
		$this->m_objApplicationDuplicate = CApplicationDuplicates::fetchApplicationDuplicateByApplicationIdByApplicantIdByCid( $this->getId(), $this->getApplicantId(), $this->getCid(), $objDatabase );
		return $this->m_objApplicationDuplicate;
	}

	public function fetchScreeningApplicants( $objDatabase ) {
		return CApplicants::fetchScreeningApplicantsByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchNonIntegratedChargesByApplicantId( $intApplicantId, $objDatabase ) {
		return $this->m_arrobjArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchNonIntegratedApplicationChargesByLeaseIdByApplicantIdByApplicationIdByCid( $this->m_intLeaseId, $intApplicantId, $this->m_intId, $this->m_intCid, $objDatabase );
	}

	public function fetchNonIntegratedApplicationCharges( $objDatabase, $intArTriggerId = NULL ) {
		if( true == is_null( $this->getLeaseId() ) )
			return NULL;

		return $this->m_arrobjArTransactions = \Psi\Eos\Entrata\CArTransactions::createService()->fetchApplicationChargesByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase, true, $intArTriggerId );
	}

	public function fetchNonIntegratedArPayments( $objDatabase ) {
		return CArPayments::fetchNonIntegratedArPaymentsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantsByCustomerTypeIdsByApplicationStepId( $arrintCustomerTypeIds, $intApplicationStepId, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return NULL;

		return CApplicants::fetchApplicantsByApplicationIdByCustomerTypeIdsByApplicationStepIdByCid( $this->m_intId, $arrintCustomerTypeIds, $intApplicationStepId, $this->getCid(), $objDatabase );
	}

	// MEGARATES_COMMENTS: Removed function fetchApplicantApplicationPetByApplicantApplicationIds, no call for this function

	// MEGARATES_COMMENTS:removed function fetchApplicantApplicationPetsByApplicantApplicationIds, only one call was there now direct call on CCustomerPets using lease id

	// MEGARATES_COMMENTS:Removed function fetchSimpleApplicantApplicationPetsByCidByApplicantApplicationIds, no call for this function

	public function fetchContactsByContactTypeId( $intContactTypeId, $objDatabase ) {
		return CApplicantContacts::fetchApplicantContactsByContactTypeIdByApplicationIdByCid( $intContactTypeId, $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerIncomes( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerIncomesWithoutNullAmount( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByApplicationIdWithoutNullAmountByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAssets( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAssets::createService()->fetchCustomerAssetsByApplicationIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchAppliedSpecialsWithPromoCode( $strPromoCode, $objDatabase ) {
		$objPropertyUnit	= $this->getOrFetchPropertyUnit( $objDatabase );
		$intUnitTypeId		= ( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) ? $objPropertyUnit->getUnitTypeId() : NULL;

		$objUnitSpace		= $this->getOrFetchUnitSpace( $objDatabase );
		$intUnitSpaceId		= ( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) ? $objUnitSpace->getId() : NULL;

		return CSpecials::createService()->fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermBySpecialPromoCodeByCid( $this->getPropertyId(), $intUnitSpaceId, $intUnitTypeId, $this->getTermMonth(), $strPromoCode, $this->getCid(), $objDatabase );
	}

	public function fetchSpecialsWithPromoCode( $boolReturnCount, $strCouponCode, $objDatabase, $arrintCustomerIds ) {
		return CSpecials::createService()->fetchCouponCodeSpecialsByLeaseIntervalIdByPropertyIdByUnitTypeIdByUnitSpaceIdByMoveInDateByCid( $this->getLeaseIntervalId(), $this->getPropertyId(), $this->getUnitTypeId(), $this->getUnitSpaceId(), $this->getLeaseStartDate(), $this->getCid(), $objDatabase, $boolReturnCount, $strCouponCode, $arrintCustomerIds, $this->getPropertyFloorplanId() );
	}

	public function fetchLastEventResultByEventTypeIds( $arrintEventTypeIds, $objDatabase ) {
		$objEvent = $this->fetchLastEventByEventTypeIds( $arrintEventTypeIds, $objDatabase );

		if( false == valObj( $objEvent, 'CEvent' ) || false == is_numeric( $objEvent->getEventResultId() ) )
			return NULL;
		return \Psi\Eos\Entrata\CEventResults::createService()->fetchEventResultByIdByCid( $objEvent->getEventResultId(), $this->getCid(), $objDatabase );

		return NULL;
	}

	public function fetchAppliedSpecials( $objDatabase ) {
		$arrobjSpecials = NULL;

		// Fetch Unit Space Level
		if( false == is_null( $this->getUnitSpaceId() ) ) {
			$arrobjSpecials = CSpecials::createService()->fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermByCid( $this->getPropertyId(), $this->getUnitSpaceId(), NULL, $this->getTermMonth(), true, $this->getCid(), $objDatabase, true );
		}

		if( false == valArr( $arrobjSpecials ) ) {
			// Fetch UnitType Level
			$objPropertyUnit = $this->getOrFetchPropertyUnit( $objDatabase );

			if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) && 0 < $objPropertyUnit->getUnitTypeId() ) {
				$arrobjSpecials = CSpecials::createService()->fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermByCid( $this->getPropertyId(), NULL, $objPropertyUnit->getUnitTypeId(), $this->getTermMonth(), true, $this->getCid(), $objDatabase, true );

			} elseif( false == valObj( $objPropertyUnit, 'CPropertyUnit' ) && false == is_null( $this->getPropertyFloorplanId() ) ) {
				$arrobjUnitTypes = ( array ) CUnitTypes::createService()->fetchUnitTypesByPropertyFloorPlanIdByPropertyIdByCid( $this->getPropertyFloorplanId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

				if( true == valArr( $arrobjUnitTypes ) ) {
					$arrobjSpecials	= CSpecials::createService()->fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermByCid( $this->getPropertyId(), NULL, NULL, $this->getTermMonth(), true, $this->getCid(), $objDatabase, true, array_keys( $arrobjUnitTypes ) );
				}
			}
		}

		// Fetch Property Level
		if( false == valArr( $arrobjSpecials ) ) {
			$arrobjSpecials = CSpecials::createService()->fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermByCid( $this->getPropertyId(), NULL, NULL, $this->getTermMonth(), true,  $this->getCid(), $objDatabase, true );
		}

		return $arrobjSpecials;
	}

	public function fetchAppliedRates( $objDatabase ) {
		$arrobjRateLogs = [];
		$arrobjPetRateAssociations = [];

		if( true == is_null( $this->getId() ) ) {
			return $arrobjRateLogs;
		}

		$this->m_objUnitSpace 				= $this->getOrFetchUnitSpace( $objDatabase );
		$this->m_objPropertyUnit 			= $this->getOrFetchPropertyUnit( $objDatabase );
		$this->m_objPropertyFloorplan 		= $this->getOrFetchPropertyFloorplan( $objDatabase );
		$this->m_objProperty 				= $this->getOrFetchProperty( $objDatabase );
		$this->m_objApplicant 				= $this->fetchPrimaryApplicant( $objDatabase );
		$this->m_objApplicantApplication 	= $this->getOrFetchApplicantApplicationByApplicantId( $this->m_objApplicant->getId(), $objDatabase );

		$arrobjAmenityLeaseAssociations = $this->fetchAmenityLeaseAssociations( $objDatabase );
		$arrobjPropertyPreferences 		= $this->m_objProperty->getOrFetchPropertyPreferences( $objDatabase );

		$boolIsSpecific = true;

		$objRateLogsFilter				= new CRateLogsFilter();

		$objRateLogsFilter->setCids( [ $this->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $this->getPropertyId() ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE ] );
		$objRateLogsFilter->setMoveInDate( $this->getLeaseStartDate() );

		if( true == valObj( $this->m_objUnitSpace, 'CUnitSpace' ) ) {
			$objRateLogsFilter->setUnitSpaceIds( [ $this->m_objUnitSpace->getId() ] );

		} elseif( true == valObj( $this->m_objPropertyFloorplan, 'CPropertyFloorplan' ) ) {
			$objRateLogsFilter->setPropertyFloorplanIds( [ $this->m_objPropertyFloorplan->getId() ] );
			$objRateLogsFilter->setUnitTypeIds( array_keys( ( array ) $this->m_objPropertyFloorplan->getOrFetchUnitTypes( $objDatabase ) ) );
		}

		$arrobjTempRateLogs = [];

		$arrobjRateLogs = ( array ) \Psi\Eos\Entrata\CRateLogs::createService()->fetchRateLogsByRateLogsFilter( $objRateLogsFilter, $objDatabase, $boolReturnArray = false, $this->getTermMonth() );

		foreach( $arrobjRateLogs as $objRateLog ) {
			$arrobjTempRateLogs[] = clone $objRateLog;
		}

		$arrobjTempRateLogs = rekeyObjects( 'ArTriggerId', $arrobjTempRateLogs, $boolHasMultipleObjectsWithSameKey = true );

		if( false == array_key_exists( 'ENABLE_APPLICATION_PRE_SCREENING', $arrobjPropertyPreferences ) ) {
			$arrintApplicationArTriggerIds[] = CArTrigger::PRE_QUALIFICATION;
		}

		$arrintApplicationArTriggerIds[] = CArTrigger::APPLICATION_COMPLETED;

		if( true == valArr( $arrobjRateLogs ) ) {
			foreach( $arrobjRateLogs as $intKey => $objRateLog ) {
				if( true == in_array( $objRateLog->getArTriggerId(), $arrintApplicationArTriggerIds ) ) {
					unset( $arrobjRateLogs[$intKey] );
				}
			}
		}

		$arrobjSecondaryApplicants = ( array ) $this->fetchApplicantsByCustomerTypeIds( [ CCustomerType::RESPONSIBLE, CCustomerType::GUARANTOR, CCustomerType::NOT_RESPONSIBLE ], $objDatabase );

		if( true == valArr( $arrobjSecondaryApplicants ) && ( true == valArr( $arrobjTempRateLogs ) && true == array_key_exists( CArTrigger::APPLICATION_COMPLETED, $arrobjTempRateLogs ) ) ) {
			foreach( $arrobjSecondaryApplicants as $objSecondaryApplicant ) {

				if( false == $objSecondaryApplicant->isAboveEighteenYearsOld() && true == is_null( $objSecondaryApplicant->getCustomerRelationshipId() ) ) {
					continue;
				}

				$arrobjRateLogsTemp	= ( array ) $arrobjTempRateLogs[CArTrigger::APPLICATION_COMPLETED];

				foreach( $arrobjRateLogsTemp as $objRateLog ) {
					if( $objSecondaryApplicant->getCustomerRelationshipId() == $objRateLog->getCustomerRelationshipId() ) {
						$arrobjRateLogs[] = $objRateLog;
					}
				}
			}
		}

		if( true == valArr( $arrobjAmenityLeaseAssociations ) ) {

			$arrintCommunityAmenityIds = [];
			$arrintApartmentAmenityIds = [];

			foreach( $arrobjAmenityLeaseAssociations as $objAmenityLeaseAssociation ) {

				if( CArCascade::SPACE == $objAmenityLeaseAssociation->getArCascadeId() ) {
					$arrintApartmentAmenityIds[$objAmenityLeaseAssociation->getArOriginReferenceId()] = $objAmenityLeaseAssociation->getArOriginReferenceId();
				} elseif( CArCascade::PROPERTY == $objAmenityLeaseAssociation->getArCascadeId() ) {
					$arrintCommunityAmenityIds[$objAmenityLeaseAssociation->getArOriginReferenceId()] = $objAmenityLeaseAssociation->getArOriginReferenceId();
				}
			}

			$objRateLogsFilter = new CRateLogsFilter();
			$objRateLogsFilter->setCids( [ $this->getCid() ] );
			$objRateLogsFilter->setPropertyGroupIds( [ $this->getPropertyId() ] );
			$objRateLogsFilter->setArOriginIds( [ CArOrigin::AMENITY ] );
			$objRateLogsFilter->setArOriginReferenceIds( $arrintCommunityAmenityIds );

			\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );
			$arrobjCommunityAmenityRates = \Psi\Eos\Entrata\CRateLogs::createService()->fetchTempProspectRates( $objDatabase );

			$objRateLogsFilter->setArOriginReferenceIds( $arrintApartmentAmenityIds );
			\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

			$arrobjApartmentAmenityRates = \Psi\Eos\Entrata\CRateLogs::createService()->fetchTempProspectRates( $objDatabase );

			if( true == valArr( $arrobjCommunityAmenityRates ) ) {
				$arrobjRateLogs = array_merge( $arrobjRateLogs, $arrobjCommunityAmenityRates );
			}

			if( true == valArr( $arrobjApartmentAmenityRates ) ) {
				$arrobjRateLogs = array_merge( $arrobjRateLogs, $arrobjApartmentAmenityRates );
			}
		}

		// MEGARATES_COMMENTS: make sure $this->m_objApplicant must be set
		if( true == valObj( $this->m_objApplicant, 'CApplicant' ) ) {

			if( true == is_numeric( $this->getUnitSpaceId() ) ) {
				$arrobjPetRateAssociations = $this->m_objProperty->fetchPropertyPetRateAssociationsByUnitSpaceIdWithPetTypeName( $this->getUnitSpaceId(), $objDatabase );
			}

			if( false == valArr( $arrobjPetRateAssociations ) ) {
				$arrobjPetRateAssociations = $this->m_objProperty->fetchPetRateAssociationsWithPetTypeName( $objDatabase );
			}

			$arrobjCustomerPets = \Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetsByLeaseIdByLeaseIntervalIdByCustomerIdWithPetTypeName( $this->getLeaseId(), $this->getLeaseIntervalId(), $this->m_objApplicant->getCustomerId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrobjCustomerPets ) ) {

				$arrobjPetRateAssociations = rekeyObjects( 'ArOriginReferenceId', $arrobjPetRateAssociations );

				foreach( $arrobjCustomerPets as $objCustomerPet ) {

					$objPetRateAssociation = getArrayElementByKey( $objCustomerPet->getPetTypeId(), $arrobjPetRateAssociations );

					if( true == valObj( $objPetRateAssociation, 'CPetRateAssociation' ) ) {

						$arrobjPetTypeRates = $objPetRateAssociation->fetchPetRates( $objDatabase );

						if( true == valArr( $arrobjPetTypeRates ) ) {
							$arrobjRateLogs = array_merge( $arrobjRateLogs, $arrobjPetTypeRates );
						}
					}
				}
			}
		}

		// If application fee type exists then we need to update only primary
		// applicant rates.

		return $arrobjRateLogs;
	}

	public function fetchScheduledCharges( $objDatabase, $arrintArTriggerIds = NULL ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase, $arrintArTriggerIds );
	}

	public function fetchUnpostedScheduledCharges( $objDatabase, $arrintArTriggerIds = NULL ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchUnpostedScheduledChargesByLeaseIntervalIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase, $arrintArTriggerIds );
	}

	public function fetchPostedScheduledCharges( $objDatabase, $arrintArTriggerIds = NULL ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchPostedSchedledChargesByLeaseIntervalIdByCid( $this->getLeaseIntervalId(), $this->getCid(), $objDatabase, $arrintArTriggerIds );
	}

	public function fetchRentScheduledCharges( $objDatabase, $boolIsFromOtr = true ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsWithRentCodeByCid( $this->getLeaseIntervalId(), CArTrigger::$c_arrintRecurringArTriggers, $this->getCid(), $objDatabase, $boolIsFromOtr );
	}

	public function fetchScheduledChargesWithPromoCode( $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByPropertyIdWithPromoCodeByCid( $this->getLeaseIntervalId(), $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	public function fetchScheduledChargesByArTriggerIds( $arrintArTriggerIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $this->getLeaseIntervalId(), $arrintArTriggerIds, $this->getCid(), $objDatabase );
	}

	public function fetchScheduledChargeById( $intScheduledChargeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargeByIdByCid( $intScheduledChargeId, $this->getCid(), $objDatabase );
	}

	public function fetchFileSignaturesByFileAssociationIds( $arrintFileAssociationIds, $objDatabase, $boolIncludeSoftDeleted = false ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesByFileAssociationIdsByCid( $arrintFileAssociationIds, $this->getCid(), $objDatabase, $boolIncludeSoftDeleted );
	}

	public function fetchSignedFileSignaturesByFileIdByPageNumber( $intFileId, $intPageNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchSignedFileSignaturesByApplicationIdByFileIdByPageNumberByCid( $this->getId(), $intFileId, $intPageNumber, $this->getCid(), $objDatabase );
	}

	public function fetchSignedFileSignaturesBySystemCode( $strSystemCode, $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchSignedFileSignaturesByApplicationIdBySystemCodeByCid( $this->getId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFilesByFileTypeSystemCodes( $arrstrSystemCodes, $objDatabase, $intFileExtensionId = NULL ) {
		return CFiles::createService()->fetchFilesByApplicationIdByFileTypeSystemCodesByCid( $this->getId(), $arrstrSystemCodes, $this->getCid(), $objDatabase, false, $intFileExtensionId );
	}

	public function fetchFileByFileTypeSystemCode( $strSystemCode, $objDatabase ) {
		return CFiles::createService()->fetchFileByApplicationIdByFileTypeSystemCodeByCid( $this->getId(), $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchFilesByApplicantIdsByFileTypeSystemCodes( $arrintApplicantIds, $arrstrFileTypeSystemCodes, $objDatabase, $boolIsIncludeDeletedFileAssociation = false ) {
		if( false == valArr( $arrintApplicantIds ) ) return NULL;
		if( false == valArr( $arrstrFileTypeSystemCodes ) ) return NULL;

		return CFiles::createService()->fetchFilesByApplicationIdByApplicantIdsByFileTypeSystemCodesByCid( $this->getId(), $arrintApplicantIds, $arrstrFileTypeSystemCodes, $this->getCid(), $objDatabase, $boolIsIncludeDeletedFileAssociation );
	}

	public function fetchFileById( $intId, $objDatabase ) {
		return CFiles::createService()->fetchFileByApplicationIdByIdByCid( $this->getId(), $this->getCid(), $intId, $objDatabase );
	}

	public function fetchFilesByIds( $arrintIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) )
			return false;

		return CFiles::createService()->fetchFilesByApplicationIdByIdsByCid( $this->getId(), $arrintIds, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantsByIds( $arrintApplicantIds, $objDatabase, $boolIncludeDeletedAA = false ) {
		return CApplicants::fetchApplicantsByIdsByApplicationIdByCid( $arrintApplicantIds, $this->getId(), $this->getCid(), $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchFileAssociationsByFileIds( $arrintFileIds, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicationIdByFileIdsByCid( $this->getId(), $arrintFileIds, $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociationsByFileIdBySystemCode( $intFileId, $strSystemCode, $objDatabase ) {
		return CFileAssociations::createService()->fetchFileAssociationsByApplicationIdByFileIdBySystemCodeByCid( $this->getId(), $intFileId, $strSystemCode, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationsByIdsByCustomerTypeIds( $arrintIds, $arrintCustomerTypeIds, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByIdsByApplicationIdByCustomerTypeIdsByCid( $arrintIds, $this->getId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationsByCustomerTypeIds( $arrintCustomerTypeIds, $objDatabase, $boolIncludeDeletedAA = false ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return false;

		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicationIdByCustomerTypeIdsByCid( $this->getId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase, $boolIncludeDeletedAA );
	}

	public function fetchApplicantApplicationsByCustomerTypeIdsHavingPrimaryIsResponsible( $arrintCustomerTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCustomerTypeIds ) )
			return false;

		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicationIdByCustomerTypeIdsHavingPrimaryIsResponsibleByCid( $this->getId(), $arrintCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationsByExcludingCustomerTypeIds( $arrintExludingCustomerTypeIds, $objDatabase ) {
		if( false == valArr( $arrintExludingCustomerTypeIds ) )
			return NULL;

		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicationIdByExcludingCustomerTypeIdsByCid( $this->getId(), $arrintExludingCustomerTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseSignedApplicantApplicationsByFileIdByPageNumber( $intFileId, $intPageNumber, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchLeaseSignedApplicantApplicationsByApplicationIdByFileIdByPageNumberByCid( $this->getId(), $intFileId, $intPageNumber, $this->getCid(), $objDatabase );
	}

	public function fetchLeaseESignedApplicantApplications( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchLeaseESignedApplicantApplicationsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLeaseUnsignedApplicantApplications( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchLeaseUnsignedApplicantApplicationsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFormAnswersByTags( $arrstrTags, $objDatabase ) {
		if( false == valArr( $arrstrTags ) )
			return NULL;

		return CFormAnswers::fetchFormAnswersByApplicationIdByTagsByCid( $this->getId(), $arrstrTags, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationChanges( $objDatabase ) {
		return CApplicantApplicationChanges::fetchSimpleApplicantApplicationChangesByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchEventsCountByEventTypeIdHavingUnitSpaceId( $intEventTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsCountByPropertyIdByLeaseIntervalIdByEventTypeIdHavingUnitSpaceIdByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), $intEventTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchFileMergeValues( $objDatabase ) {
		return CFileMergeValues::fetchFileMergeValuesByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFileMergeValueCount( $objDatabase ) {
		return CFileMergeValues::fetchFileMergeValueCountByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCall( $objVoipDatabase ) {
		return CCalls::fetchCallByCidByPropertyIdById( $this->getCid(), $this->getPropertyId(), $this->getCallId(), $objVoipDatabase );
	}

	public function fetchOriginalApplication( $objDatabase ) {
		return CApplications::fetchCustomApplicationByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicationByCallId( $objDatabase ) {
		return CApplications::fetchApplicationByCallIdByCid( $this->getCallId(), $this->getCid(), $objDatabase );
	}

	public function fetchWaitListDataByOccupancyTypeIdByPropertyGroupIdsByCid( $objDatabase ) {
		$arrintPropertyIds = ( true == valObj( $this->m_objProperty, 'CProperty' ) ) ? $this->m_objProperty->getParentChildPropertyIds() : [ $this->getPropertyId() ];
		return CWaitLists::fetchWaitListDataByOccupancyTypeIdByPropertyGroupIdsByCid( $this->getOccupancyTypeId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public function checkLeaseEsigned( $objDatabase, $arrobjApplicantApplications = NULL ) {
		if( false == valArr( $arrobjApplicantApplications ) ) {
			$arrobjApplicantApplications = $this->fetchApplicantApplicationsByCustomerTypeIds( CCustomerType::$c_arrintResponsibleCustomerTypeIds, $objDatabase );
		}

		if( false == valArr( $arrobjApplicantApplications ) )
			return false;

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {

			if( false == is_null( $objApplicantApplication->getLeaseSkippedOn() ) ) continue;

			if( true == is_null( $objApplicantApplication->getLeaseSignedOn() ) || true == is_null( $objApplicantApplication->getLeaseSignature() ) || true == is_null( $objApplicantApplication->getLeaseIpAddress() ) ) {
				$this->m_boolIsLeaseEsigned = false;
				return $this->m_boolIsLeaseEsigned;
			}
		}

		$this->m_boolIsLeaseEsigned = true;

		return $this->m_boolIsLeaseEsigned;
	}

	public function checkLeasePartiallyEsigned( $objDatabase, $arrobjApplicantApplications = NULL ) {

		$this->m_boolLeasePartiallyEsigned = false;

		if( false == valArr( $arrobjApplicantApplications ) ) {
			$arrobjApplicantApplications = $this->getOrFetchApplicantApplications( $objDatabase );
		}

		if( false == valArr( $arrobjApplicantApplications ) ) {
			return $this->m_boolLeasePartiallyEsigned;
		}

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( false == in_array( $objApplicantApplication->getCustomerTypeId(), CCustomerType::$c_arrintResponsibleCustomerTypeIds ) || false == is_null( $objApplicantApplication->getLeaseSkippedOn() ) ) continue;

			if( false == is_null( $objApplicantApplication->getLeaseSignedOn() ) && false == is_null( $objApplicantApplication->getLeaseSignature() ) && false == is_null( $objApplicantApplication->getLeaseIpAddress() ) ) {
				$this->m_boolLeasePartiallyEsigned = true;
				break;
			}
		}

		return $this->m_boolLeasePartiallyEsigned;
	}

	public function createFileAssociation() {
		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setApplicationId( $this->getId() );
		$objFileAssociation->setPropertyId( $this->getPropertyId() );

		return $objFileAssociation;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolInsertEvent = true, $boolDoNotExport = false, $boolRemoveFileAssociations = false, $boolReturnSqlOnly = false, $boolRebuildFileAssociations = true ) {
		if( false == valId( $this->getId() ) || false == valStr( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolInsertEvent, $boolDoNotExport, $boolRemoveFileAssociations, $boolReturnSqlOnly, $boolRebuildFileAssociations );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolInsertEvent, $boolDoNotExport, $boolRemoveFileAssociations, $boolReturnSqlOnly, $boolIsFromApplyQuote = false, $intPsProductId = NULL, $boolRebuildFileAssociations );
		}
	}

	public function updateQuotedRentLockedUntil( $objDatabase ) {
		if( true == $this->allowLockRentAmount() ) {
			$arrobjPropertyPreferences = ( array ) CPropertyPreferences::createService()->fetchPropertyPreferencesByKeysByPropertyIdByCid( [ 'DAYS_TO_LOCK_RENT_AMOUNT', 'APPLICATION_QUOTE_IS_VALID_FOR' ], $this->getPropertyId(), $this->getCid(), $objDatabase );
			$arrobjPropertyPreferences = rekeyObjects( 'Key', $arrobjPropertyPreferences );

			if( true == isset( $arrobjPropertyPreferences['DAYS_TO_LOCK_RENT_AMOUNT'] ) && CApplication::LOCK_RENT_AMOUNT == $this->getLockQuotedRent() ) {
				$strLockedQuotedRentUntil = date( 'Y-m-d 11:59:59', strtotime( '+' . ( int ) ( $arrobjPropertyPreferences['DAYS_TO_LOCK_RENT_AMOUNT']->getValue() ) . ' day' ) );
				$this->setQuotedRentLockedUntil( $strLockedQuotedRentUntil );
			} elseif( CApplication::LOCK_QUOTES_RENT == $this->getLockQuotedRent() ) {

				$objQuote = CQuotes::fetchQuoteByIdByCid( $this->getQuoteId(), $this->getCid(), $objDatabase );

				if( false == valObj( $objQuote, 'CQuote' ) ) return true;

				if( 0 < ( true == isset( $arrobjPropertyPreferences['APPLICATION_QUOTE_IS_VALID_FOR'] ) && true == valObj( $arrobjPropertyPreferences['APPLICATION_QUOTE_IS_VALID_FOR'], 'CPropertyPreference' ) ) ? intval( $arrobjPropertyPreferences['APPLICATION_QUOTE_IS_VALID_FOR']->getValue() ) : 0 ) {
					$strLockedQuotedRentUntil = ( true == valObj( $arrobjPropertyPreferences['DAYS_TO_LOCK_RENT_AMOUNT'], 'CPropertyPreference' ) ) ? intval( $arrobjPropertyPreferences['DAYS_TO_LOCK_RENT_AMOUNT']->getValue() ) : 0;
					$strLockedQuotedRentUntil = ( 0 < $strLockedQuotedRentUntil ) ? date( 'Y-m-d', strtotime( '+' . $strLockedQuotedRentUntil . ' days ', strtotime( $objQuote->getExpiresOn() ) ) ) : $objQuote->getExpiresOn();
				} else {
					$strLockedQuotedRentUntil = ( true == valObj( $arrobjPropertyPreferences['DAYS_TO_LOCK_RENT_AMOUNT'], 'CPropertyPreference' ) ) ? intval( $arrobjPropertyPreferences['DAYS_TO_LOCK_RENT_AMOUNT']->getValue() ) : 0;
					$strLockedQuotedRentUntil = ( 0 < $strLockedQuotedRentUntil ) ? strtotime( '+' . $strLockedQuotedRentUntil . ' days', strtotime( date( 'Y-m-d' ) ) ) : strtotime( '+2 days', strtotime( date( 'Y-m-d 11:59:59' ) ) );
					$strLockedQuotedRentUntil = $strLockedQuotedRentUntil = date( 'Y-m-d', $strLockedQuotedRentUntil );
				}
				$this->setQuotedRentLockedUntil( $strLockedQuotedRentUntil );
			}
		}
		return true;
	}

	public function allowLockRentAmount() {

		if( $this->getApplicationStageId() == CApplicationStage::PRE_APPLICATION
			|| ( $this->getApplicationStageId() == CApplicationStage::PRE_QUALIFICATION && ( true == in_array( $this->getApplicationStatusId(), [ CApplicationStatus::STARTED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ] ) ) )
			|| ( $this->getApplicationStageId() == CApplicationStage::APPLICATION && $this->getApplicationStatusId() == CApplicationStatus::STARTED ) ) {
				return true;
		}

		return false;

	}

	public function insert( $intCurrentUserId, $objDatabase, $boolInsertEvent = true, $boolDoNotExport = false, $boolRemoveFileAssociations = false, $boolReturnSqlOnly = false, $boolRebuildFileAssociations = true ) {

		$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );

		// TODO: Remove code after debugging.
		if( CPsProduct::LEASING_CENTER == $this->getPsProductId() ) {
			$this->setApplicationInsertionUrl( ( true == isset( $_SERVER['REQUEST_URI'] ) ) ? $_SERVER['REQUEST_URI'] : NULL );
		}

		// Based on discussion with Kedar, checking min. / max. rent during insert process as well
		if( false == is_null( $this->getDesiredRentMax() ) && false == is_null( $this->getDesiredRentMin() ) && $this->getDesiredRentMax() < $this->getDesiredRentMin() ) {
			$intDesiredRentMax = $this->getDesiredRentMin();
			$this->setDesiredRentMin( $this->getDesiredRentMax() );
			$this->setDesiredRentMax( $intDesiredRentMax );
		}

		CApplicationUtils::setDefaultApplicationData( $this, $objDatabase );

		if( false == is_null( $this->getLockQuotedRent() ) ) {
			$this->updateQuotedRentLockedUntil( $objDatabase );
		}

		if( true == $boolReturnSqlOnly ) {
			$strSql = '';

			$strSql .= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			if( true == $boolInsertEvent ) $strSql .= $this->insertEvent( $intCurrentUserId, $objDatabase, NULL, $boolDoNotExport, $boolReturnSqlOnly );

			return $strSql;
		} else {

			$boolIsValid = true;

			if( true == $this->isGuestCard() && false == valStr( $this->getLeaseIntervalId() ) ) {

				$objLeaseInterval = new CLeaseInterval();

				$objLeaseInterval->setCid( $this->getCid() );
				$objLeaseInterval->setIntervalDatetime( 'NOW()' );

				$objLeaseInterval->setId( $objLeaseInterval->fetchNextId( $objDatabase ) );
				$objLeaseInterval->setPropertyId( $this->getPropertyId() );
				$objLeaseInterval->setLeaseStartDate( ( NULL == $this->getLeaseStartDate() )? date( 'm/d/Y' ):$this->getLeaseStartDate() );
				$objLeaseInterval->setLeaseEndDate( $this->getLeaseEndDate() );
				$objLeaseInterval->setLeaseIntervalTypeId( CLeaseIntervalType::APPLICATION );
				$objLeaseInterval->setLeaseStatusTypeId( CLeaseStatusType::APPLICANT );

				if( NULL == $this->getLeaseTermId() ) {
					$objLeaseTerm = $this->m_objProperty->fetchLeaseTermByTermMonth( 12, $objDatabase );

					if( true == valObj( $objLeaseTerm, 'CLeaseTerm' ) ) {
						$objLeaseInterval->setLeaseTermId( $objLeaseTerm->getId() );
					}
				} else {
					$objLeaseInterval->setLeaseTermId( $this->getLeaseTermId() );
				}
				$objLeaseInterval->setIsImmediateMoveIn( $this->getIsImmediateMoveIn() );
				if( true == valStr( $this->getLeaseStartWindowId() ) ) {
					$objLeaseInterval->setLeaseStartWindowId( $this->getLeaseStartWindowId() );
				}

				$this->setLeaseIntervalId( $objLeaseInterval->getId() );

				if( false == $objLeaseInterval->validate( 'validate_guest_card', $objDatabase ) ) {
					$this->addErrorMsgs( $objLeaseInterval->getErrorMsgs() );
					return false;
				}

				if( false == $objLeaseInterval->insert( $intCurrentUserId, $objDatabase ) ) {
					$this->addErrorMsgs( $objLeaseInterval->getErrorMsgs() );
					return false;
				}
			}

			if( CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() && true == is_null( $this->getLeaseId() ) && true == $this->getIsSyncApplicationAndLease() && ( false == $this->isGuestCard() || 0 < $this->getQuotesCount() ) ) {
				if( true == is_null( $this->getId() ) ) {
					$this->setId( $this->fetchNextId( $objDatabase ) );
				}

				$boolIsValid &= CApplicationUtils::insertLease( $this, $intCurrentUserId, $objDatabase );

				if( false == $boolIsValid ) return false;
			}

			$arrobjPropertyPreferences = ( array ) CPropertyPreferences::createService()->fetchPropertyPreferencesByKeysByPropertyIdByCid( [ 'AUTO_ASSOCIATE_IF_ONLY_ONE_INSTALLMENT', 'ENABLE_SEMESTER_SELECTION', 'LOCK_DOWN_APPLICATION_AT' ], $this->getPropertyId(), $this->getCid(), $objDatabase );
			$arrobjPropertyPreferences = rekeyObjects( 'key', $arrobjPropertyPreferences );

			if( COccupancyType::AFFORDABLE != $this->getOccupancyTypeId() && true == array_key_exists( 'AUTO_ASSOCIATE_IF_ONLY_ONE_INSTALLMENT', $arrobjPropertyPreferences ) ) {
				// for updating installment plan on application in case of student semester selection enabled and auto associate installment plan
				$objLeaseInterval = $this->getOrFetchLeaseInterval( $objDatabase );

				if( false != valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {

					if( false == CApplicationUtils::autoAssociateInstallmentPlan( $this, $objDatabase, $arrobjPropertyPreferences, $objLeaseInterval, $intCurrentUserId ) ) {

						return false;
					} else {

						if( false == $objLeaseInterval->update( $intCurrentUserId, $objDatabase ) ) {
							$this->addErrorMsgs( $objLeaseInterval->getErrorMsgs() );
							return false;
						}
					}
				}
			}

			if( true == array_key_exists( 'LOCK_DOWN_APPLICATION_AT', $arrobjPropertyPreferences ) && true == valObj( $arrobjPropertyPreferences['LOCK_DOWN_APPLICATION_AT'], 'CPropertyPreference' ) ) {
				$objApplicationLockDownStageStatus = CApplicationStageStatuses::fetchApplicationStageStatusById( $arrobjPropertyPreferences['LOCK_DOWN_APPLICATION_AT']->getValue(), $objDatabase );

				if( true == valObj( $objApplicationLockDownStageStatus, 'CApplicationStageStatuse' )
					&& true == $this->hasApplicationStageStatus( $objApplicationLockDownStageStatus->getApplicationStageId(), $objApplicationLockDownStageStatus->getApplicationStatusId() ) ) {

					$this->setLockedOn( 'NOW()' );
					$this->setLockedBy( $intCurrentUserId );
				}
			}

			$boolIsValid &= parent::insert( $intCurrentUserId, $objDatabase );

			if( false == $boolIsValid ) return false;

			if( true == $boolInsertEvent )
				$boolIsValid &= $this->insertEvent( $intCurrentUserId, $objDatabase, NULL, $boolDoNotExport );

			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$this->processAddOnLeaseAssociaitonsAttachedToUnit( $objUnitSpace, $intCurrentUserId, $objDatabase );
			}

			return $boolIsValid;
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolInsertEvent = true, $boolDoNotExportEvent = false, $boolRemoveFileAssociations = false, $boolReturnSqlOnly = false, $boolIsFromApplyQuote = false, $intPsProductId = NULL, $boolRebuildFileAssociations = true ) {

		if( true == is_null( $this->getId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Application: Failed to load application object.' ), NULL ) );
			exit();
		}

		// TODO: Remove code after debugging.
		if( CPsProduct::LEASING_CENTER == $this->getPsProductId() ) {
			$this->setApplicationInsertionUrl( ( true == isset( $_SERVER['REQUEST_URI'] ) ) ? $_SERVER['REQUEST_URI'] : NULL );
		}

		$objOriginalUnitSpace	= NULL;
		$boolUnitSpaceChanged	= false;
		$objOriginalApplication = CApplications::fetchApplicationWithLeaseIntervalDataByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$objOriginalUnitSpace	= ( true == valObj( $objOriginalApplication, 'CApplication' ) ) ? $objOriginalApplication->getOrFetchUnitSpace( $objDatabase ) : NULL;

		$objUnitSpace = $this->getOrFetchUnitSpace( $objDatabase );

		if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
			$this->setUnitTypeId( $objUnitSpace->getUnitTypeId() );
		}

		$objLeaseInterval = $this->getOrFetchLeaseInterval( $objDatabase );

		if( true == valObj( $objLeaseInterval, 'CLeaseInterval' ) && NULL != $this->getLateFeeFormulaId() && $objLeaseInterval->getLateFeeFormulaId() != $this->getLateFeeFormulaId() ) {
			$this->setLateFeeFormulaId( $this->getLateFeeFormulaId() );
		} elseif( true == valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {
			$this->setLateFeeFormulaId( $objLeaseInterval->getLateFeeFormulaId() );
		}

		if( true == $this->getIsSyncApplicationAndLease() && CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() && true == in_array( $this->m_intOldApplicationStatusId, [ CApplicationStatus::CANCELLED, CApplicationStatus::ON_HOLD ] ) && true == $this->hasApplicationStageStatusIn( CApplicationStageStatus::$c_arrintValidPostApplicationCompletedStatusIds ) ) {

			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$objProperty                                = $this->getOrFetchProperty( $this->m_objDatabase );
				if( $this->getLeaseId() != $objUnitSpace->getHoldLeaseId() && ( false == in_array( $objUnitSpace->getUnitSpaceStatusTypeId(), CUnitSpaceStatusType::$c_arrintAvailableUnitSpaceStatusTypes ) || ( strtotime( $objUnitSpace->getReserveUntil() ) > time() && true == $objProperty->isIntegrated( $this->m_objDatabase ) ) ) ) {

					$this->insertEventByEventTypeId( $intCurrentUserId, $objDatabase, CEventType::UNIT_UNAVAILABLE, true );

					$this->setUnitSpaceId( NULL );
					$this->setUnitTypeId( NULL );
					$this->setPropertyUnitId( NULL );
					$this->setUnitNumberCache( NULL );
				}
			}
		}

		// Set applicatinos.unit_number_cache
		if( false == is_null( $this->getUnitSpaceId() ) || true == is_numeric( $this->getUnitSpaceId() ) ) {
			if( true == valObj( $objOriginalApplication, 'CApplication' ) && $this->getUnitSpaceId() != $objOriginalApplication->getUnitSpaceId() && true == valId( $this->getUnitSpaceId() ) ) {
				$boolUnitSpaceChanged 	= true;
			}

		} else {

			if( true == valObj( $objOriginalApplication, 'CApplication' ) && false == is_null( $objOriginalApplication->getUnitSpaceId() ) ) {
				$boolUnitSpaceChanged 	= true;

				if( false == is_null( $this->getQuoteId() ) ) {
					CApplicationUtils::unsetApplicationQuote( $this, $intCurrentUserId, $objDatabase );
					$this->setQuotedRentLockedUntil( NULL );
				}
			}
		}

		if( false == is_null( $this->getDesiredRentMax() ) && false == is_null( $this->getDesiredRentMin() ) && $this->getDesiredRentMax() < $this->getDesiredRentMin() ) {
			$intDesiredRentMax = $this->getDesiredRentMin();
			$this->setDesiredRentMin( $this->getDesiredRentMax() );
			$this->setDesiredRentMax( $intDesiredRentMax );
		}

		CApplicationUtils::setDefaultApplicationData( $this, $objDatabase );

		$this->m_intOldCompanyApplicationId 	= NULL;
		$this->m_intOldApplicationStageId 		= NULL;
		$this->m_intOldApplicationStatusId 		= NULL;
		$this->m_intOldPropertyId 				= NULL;
		$intOldLeaseTermId                      = NULL;
		$strOldLeaseStartDate                   = NULL;
		$intOldQuoteId                          = NULL;

		$arrintResponse = fetchData( 'SELECT * FROM cached_applications WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );

		if( true == isset( $arrintResponse[0]['company_application_id'] ) ) $this->m_intOldCompanyApplicationId = $arrintResponse[0]['company_application_id'];
		if( true == isset( $arrintResponse[0]['application_stage_id'] ) ) $this->m_intOldApplicationStageId = $arrintResponse[0]['application_stage_id'];
		if( true == isset( $arrintResponse[0]['application_status_id'] ) ) $this->m_intOldApplicationStatusId = $arrintResponse[0]['application_status_id'];
		if( true == isset( $arrintResponse[0]['property_id'] ) ) $this->m_intOldPropertyId = $arrintResponse[0]['property_id'];
		if( true == isset( $arrintResponse[0]['lease_term_id'] ) ) $intOldLeaseTermId = $arrintResponse[0]['lease_term_id'];
		if( true == isset( $arrintResponse[0]['lease_start_date'] ) ) $strOldLeaseStartDate = $arrintResponse[0]['lease_start_date'];
		if( true == isset( $arrintResponse[0]['quote_id'] ) ) $intOldQuoteId = $arrintResponse[0]['quote_id'];

		if( \CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() && true == valId( $intOldQuoteId ) && $intOldQuoteId == $this->getQuoteId() && ( ( $intOldLeaseTermId != $this->getLeaseTermId() ) || strtotime( $strOldLeaseStartDate ) != strtotime( $this->getLeaseStartDate() ) ) ) {
			CApplicationUtils::unsetApplicationQuote( $this, $intCurrentUserId, $objDatabase );
		}

		if( true == valStr( $arrintResponse[0]['lease_start_date'] ) && $arrintResponse[0]['lease_start_date'] != $this->getLeaseStartDate() ) $this->setMoveInDate( $this->getLeaseStartDate() );

		if( true == valObj( $objOriginalApplication, 'CApplication' ) && true == valStr( $objOriginalApplication->getMoveInDate() ) && true == valStr( $this->getMoveInDate() ) ) $arrintResponse[0]['move_in_date'] = $objOriginalApplication->getMoveInDate();

		if( true == isset( $arrintResponse[0]['term_month'] ) && ( true == $this->getStudentPropertyApplication() || true == $this->m_boolIsDefaultLeaseTermSet ) ) {
			$arrintResponse[0]['term_month'] = $this->getTermMonth();
		}

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		if( true == $boolReturnSqlOnly ) {

			$strSql = '';

			$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_UPDATE, $arrintResponse[0], $intPsProductId );

			$strSql .= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			if( true == $boolInsertEvent && $this->getPropertyId() != $this->m_intOldPropertyId ) {
				$strSql .= $this->insertEventByEventTypeId( $intCurrentUserId, $objDatabase, CEventType::PROPERTY_CHANGE, true, $boolReturnSqlOnly );
			}

			if( true == $boolInsertEvent && $this->getApplicationStageId() != $this->m_intOldApplicationStageId && $this->getApplicationStatusId() != $this->m_intOldApplicationStatusId ) {
				$strSql .= $this->insertEvent( $intCurrentUserId, $objDatabase, $this->m_intOldApplicationStageId, $this->m_intOldApplicationStatusId, true, $boolReturnSqlOnly );
			}

			$this->getOrFetchProperty( $objDatabase );

			return $strSql;
		} else {
			$boolIsValid = true;

			if( true == $this->getIsSyncApplicationAndLease() ) {
				// Converted guest card to application - Need to insert data for resident section.
				if( true == is_null( $this->getLeaseId() ) && false == $this->isGuestCard() ) {

					// 1. Insert Customers
					$boolIsValid &= CApplicationUtils::insertCustomers( $this, $intCurrentUserId, $objDatabase );

					// 2. Insert lease stuff for an application.
					if( true == $boolIsValid ) {
						$boolIsValid &= CApplicationUtils::insertLease( $this, $intCurrentUserId, $objDatabase );
					}
				}

				if( true == valObj( $objOriginalApplication, 'CApplication' ) && true == $boolIsValid ) {
					$boolIsValid &= CApplicationUtils::syncApplicationToLease( $this, $objOriginalApplication, $objDatabase, $intCurrentUserId );
				}

				if( false == $boolIsValid ) return false;

			}

			// For making rent lock null if lease term,move in date or unit changes and updating rent lock

			if( true == valObj( $objOriginalApplication, 'CApplication' ) && ( false == is_null( $objOriginalApplication->getLeaseTermId() ) && $objOriginalApplication->getLeaseTermId() != $this->getLeaseTermId()
				|| $objOriginalApplication->getLeaseStartDate() != $this->getLeaseStartDate()
				|| $objOriginalApplication->getPropertyFloorplanId() != $this->getPropertyFloorplanId() && false == is_null( $objOriginalApplication->getPropertyFloorplanId() )
				|| $objOriginalApplication->getUnitTypeId() != $this->getUnitTypeId() && false == is_null( $objOriginalApplication->getUnitTypeId() )
				|| $objOriginalApplication->getUnitSpaceId() != $this->getUnitSpaceId() && false == is_null( $objOriginalApplication->getUnitSpaceId() ) ) ) {
					$this->setQuotedRentLockedUntil( NULL );
			}

			if( false == is_null( $this->getLockQuotedRent() ) ) {
				$this->updateQuotedRentLockedUntil( $objDatabase );
			}

			$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'LOCK_DOWN_APPLICATION_AT', $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == valObj( $objOriginalApplication, 'CApplication' ) ) {
				$objApplicationLockDownStageStatus = CApplicationStageStatuses::fetchApplicationStageStatusById( $objPropertyPreference->getValue(), $objDatabase );

				if( true == valObj( $objApplicationLockDownStageStatus, 'CApplicationStageStatus' ) ) {
					if( true == $objOriginalApplication->hasLessApplicationStageStatusProgression( $objApplicationLockDownStageStatus->getApplicationStageId(), $objApplicationLockDownStageStatus->getApplicationStatusId() )
						&& true == $this->hasApplicationStageStatus( $objApplicationLockDownStageStatus->getApplicationStageId(), $objApplicationLockDownStageStatus->getApplicationStatusId() ) ) {

						$this->setLockedOn( 'NOW()' );
						$this->setLockedBy( $intCurrentUserId );
					} elseif( true == $this->hasLessApplicationStageStatusProgression( $objApplicationLockDownStageStatus->getApplicationStageId(), $objApplicationLockDownStageStatus->getApplicationStatusId() ) ) {
						$this->setLockedOn( NULL );
						$this->setLockedBy( NULL );
					}
				}
			}

			$objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_UPDATE, $arrintResponse[0], $intPsProductId );

			// 2. Update application.

			$boolIsValid &= parent::update( $intCurrentUserId, $objDatabase );

			// We are adding logs to trace the issue task id : 1024591 [KD][CKS]
			if( CApplicationStage::LEASE == $this->getApplicationStageId() && CApplicationStatus::APPROVED == $this->getApplicationStatusId() && true == valObj( $objOriginalApplication, 'CApplication' ) && strtotime( $objOriginalApplication->getLeaseApprovedOn() ) > strtotime( $this->getLeaseApprovedOn() ) ) {
				CDebugLogger::writeBackTraceToNewRelic( [
					'eventType' => 'updateApplication',
					'cid' => $this->getCid()
				] );
			}

			// 3. Insert lease customers
			if( true == $this->getIsSyncApplicationAndLease() && false == $this->isGuestCard() && true == $boolIsValid ) {
				$boolIsValid &= CApplicationUtils::insertLeaseCustomers( $this, $intCurrentUserId, $objDatabase );
			}

			if( false == $boolIsValid ) return false;

			$boolIsQuoteRemoved = false;
			// if unit space is assign/changed in a application OR lease interval changed from Guest card to Application type
			if( true == valObj( $objOriginalApplication, 'CApplication' ) && ( ( $this->getUnitSpaceId() != $objOriginalApplication->getUnitSpaceId() && true == valId( $this->getUnitSpaceId() ) ) || ( true == $objOriginalApplication->isGuestCard() && CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() ) ) ) {
				$objUnitSpace = $this->fetchUnitSpace( $objDatabase );
				$boolUnitSpaceChanged = true;
				$objOriginalUnitSpace = ( true == $objOriginalApplication->isGuestCard() && CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() ) ? NULL : $objOriginalUnitSpace;

			} elseif( true == valObj( $objOriginalApplication, 'CApplication' ) && false == is_null( $objOriginalApplication->getUnitSpaceId() ) && true == is_null( $this->getUnitSpaceId() ) ) {
				// if unit space is removed from application
				$objUnitSpace = NULL;
				$boolUnitSpaceChanged = true;
			} elseif( $intOldQuoteId != $this->getQuoteId() && true == valObj( $objOriginalApplication, 'CApplication' ) && ( $this->getLeaseStartDate() != $objOriginalApplication->getLeaseStartDate() || $this->getLeaseEndDate() != $objOriginalApplication->getLeaseEndDate() ) ) {

				$boolIsQuoteRemoved = true;
			}

			if( true == $boolIsValid && ( true == $boolUnitSpaceChanged || true == $boolIsQuoteRemoved ) && false == $boolIsFromApplyQuote ) {
				$this->processAddOnLeaseAssociaitonsAttachedToUnit( $objUnitSpace, $intCurrentUserId, $objDatabase, $objOriginalUnitSpace );
			}

			if( true == $boolIsValid && true == $boolInsertEvent && $this->getPropertyId() != $this->m_intOldPropertyId ) {
				$boolIsValid &= $this->insertEventByEventTypeId( $intCurrentUserId, $objDatabase, CEventType::PROPERTY_CHANGE, true );
			}

			if( true == $boolIsValid && true == $boolInsertEvent && ( $this->getApplicationStageId() != $this->m_intOldApplicationStageId || $this->getApplicationStatusId() != $this->m_intOldApplicationStatusId ) ) {
				$boolIsValid &= $this->insertEvent( $intCurrentUserId, $objDatabase, $this->m_intOldApplicationStageId, $this->m_intOldApplicationStatusId, $boolDoNotExportEvent );
			}

			if( false == $boolIsValid ) {
				return false;
			}

			if( false == $this->updateCancelledLease( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}

			if( true == $this->getIsSyncApplicationAndLease() ) {
				$boolIsValid &= $this->syncApplicantsToCustomers( $intCurrentUserId, $objDatabase );
			}

			if( true == valObj( $objOriginalApplication, 'CApplication' ) && $objOriginalApplication->getApplicationStatusId() != $this->getApplicationStatusId() && CApplicationStage::LEASE == $this->getApplicationStageId() && CApplicationStatus::COMPLETED == $this->getApplicationStatusId() ) {
				$arrmixLeaseMessageData['cid']                    = $this->getCid();
				$arrmixLeaseMessageData['property_id']            = $this->getPropertyId();
				$arrmixLeaseMessageData['lease_id']               = $this->getLeaseId();
				$arrmixLeaseMessageData['company_user_id']        = $intCurrentUserId;
				$arrmixLeaseMessageData['lease_interval_type_id'] = $this->getLeaseIntervalTypeId();

				CResidentInsureLibrary::sendInsuranceInvitationEmail( $arrmixLeaseMessageData );
			}

			return $boolIsValid;
		}
	}

	public function syncApplicantsToCustomers( $intCurrentUserId, $objDatabase ) {

		if( CLeaseIntervalType::APPLICATION != $this->getLeaseIntervalTypeId()
			|| false == $this->hasApplicationStageStatusIn( [ CApplicationStage::APPLICATION => [ CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ] ] ) ) return true;
		return CApplicationUtils::syncApplicantsToCustomers( $this, $intCurrentUserId, $objDatabase );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$intApplicationStageId = $this->getApplicationStageId();
		$intApplicationStatusId = $this->getApplicationStatusId();
		$this->setApplicationStageId( ( CApplicationStage::PRE_APPLICATION == $intApplicationStageId ) ? CApplicationStage::PRE_APPLICATION : CApplicationStage::APPLICATION );
		$this->setApplicationStatusId( CApplicationStatus::CANCELLED );
		$this->setCancelledOn( 'NOW()' );
		$boolIsValid = false;

		if( true == $boolIsValid ) $this->exportUpdate( $intCurrentUserId, $objDatabase );

		$this->setApplicationStageId( $intApplicationStageId );
		$this->setApplicationStatusId( $intApplicationStatusId );

		return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	// CKT : added $boolRemoveFileAssociations parameter so that file associations are deleted when a property is changed from application quick view
	// even when the application status is "Application Completed"

	public function rebuildFileAssociations( $intCurrentUserId, $objDatabase, $boolRemoveFileAssociations = false, $boolReturnSqlOnly = false ) {
		$boolIsRebuildFileAssociationsInClone = ( true == valObj( $this, 'CApplication' ) && true == $this->getFromCloneApplication() ) ? true : false;

		$strSql = '';
	if( false == $boolIsRebuildFileAssociationsInClone ) {

		if( CApplicationStage::PRE_APPLICATION == $this->getApplicationStageId() || false == in_array( $this->getLeaseIntervalTypeId(), CLeaseIntervalType::$c_arrintLeaseSigningLeaseIntervalTypes ) || true == in_array( $this->getApplicationStatusId(), [ CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ] ) ) {
			return( true == $boolReturnSqlOnly ) ? $strSql : true;
		}

		if( true == is_null( $this->getId() ) )
			return( true == $boolReturnSqlOnly ) ? $strSql : true;

		if( false == $boolRemoveFileAssociations && ( $this->hasGreaterApplicationStageStatusProgression( CApplicationStage::LEASE, CApplicationStatus::APPROVED )
			|| true == is_null( $this->getCompanyApplicationId() ) ) ) {
				return( true == $boolReturnSqlOnly ) ? $strSql : true;
		}
    }

		// That means company application id is changed on the application in this case we should delete existing file associations
		if( true == is_numeric( $this->m_intOldCompanyApplicationId ) && $this->m_intOldCompanyApplicationId != $this->getCompanyApplicationId() ) {
			$arrobjFileAssociationsToDelete = ( array ) $this->fetchAllApplicationAndApplicantApplicationFileAssociationsByFileTypeSystemCode( CFileType::SYSTEM_CODE_POLICY, $objDatabase );
			$this->deleteFileAssociationsAndFileSignatures( $arrobjFileAssociationsToDelete, $intCurrentUserId, $objDatabase );
		}

		$this->loadDocumentManager( $objDatabase, $intCurrentUserId );
		$this->loadObjectStorageGateway();

		$objApplicationDataObject = new CApplicationDataObject();
		$objApplicationDataObject->m_objApplication				= $this;
		$objApplicationDataObject->m_objDatabase 				= $objDatabase;
		$objApplicationDataObject->m_objClient 				    = $this->getOrFetchClient( $objDatabase );
		$objApplicationDataObject->m_intCompanyUserId			= $intCurrentUserId;
		$objApplicationDataObject->m_boolReturnSqlOnly			= $boolReturnSqlOnly;

		$objApplicationSystemLibrary = new CApplicationSystemLibrary( $objApplicationDataObject );
		$objApplicationSystemLibrary->setDocumentManager( $this->m_objDocumentManager );
		$objApplicationSystemLibrary->setObjectStorageGateway( $this->getObjectStorageGateway() );

		if( false == $objApplicationSystemLibrary->generatePolicyDocument() ) {
			$this->addErrorMsgs( $objApplicationSystemLibrary->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function insertEvent( $intCurrentUserId, $objDatabase, $intOldApplicationStageId = NULL, $intOldApplicationStatusId = NULL,  $boolDoNotExport = false, $boolReturnSqlOnly = false, $intDataReferenceId = NULL ) {

		$boolIsValid 	= true;
		$strSql			= '';

		$arrintApplicationStageStatusIds = [
			CApplicationStage::PRE_APPLICATION 		=> [ CApplicationStatus::COMPLETED, CApplicationStatus::CANCELLED, CApplicationStatus::ON_HOLD ],
			CApplicationStage::PRE_QUALIFICATION 	=> [ CApplicationStatus::STARTED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ],
			CApplicationStage::APPLICATION 			=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED, CApplicationStatus::ON_HOLD, CApplicationStatus::CANCELLED ],
			CApplicationStage::LEASE 				=> [ CApplicationStatus::STARTED, CApplicationStatus::PARTIALLY_COMPLETED, CApplicationStatus::COMPLETED, CApplicationStatus::APPROVED ]
		];

		if( true == $this->hasApplicationStageStatusIn( $arrintApplicationStageStatusIds ) ) {

			$objEventLibrary = new CEventLibrary();

			$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
			$objEventLibraryDataObject->setDatabase( $objDatabase );

			if( true == valObj( $this->getApplicant(), 'CApplicant' ) ) {
				$objEventLibraryDataObject->setApplicant( $this->getApplicant() );
			}

			switch( true ) {

				case CApplicationStage::PRE_APPLICATION == $this->m_intApplicationStageId && CApplicationStatus::COMPLETED == $this->m_intApplicationStatusId:
					if( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId() ) {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::RENEWAL_PROGRESS );
					} else {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::ONLINE_GUEST_CARD );
					}
					break;

				case CApplicationStage::PRE_QUALIFICATION == $this->m_intApplicationStageId && CApplicationStatus::STARTED == $this->m_intApplicationStatusId:
				case CApplicationStage::PRE_QUALIFICATION == $this->m_intApplicationStageId && CApplicationStatus::COMPLETED == $this->m_intApplicationStatusId:
				case CApplicationStage::PRE_QUALIFICATION == $this->m_intApplicationStageId && CApplicationStatus::APPROVED == $this->m_intApplicationStatusId:

					$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::PRE_SCREENING );
					break;

				case CApplicationStage::APPLICATION == $this->m_intApplicationStageId && CApplicationStatus::STARTED == $this->m_intApplicationStatusId:
				case CApplicationStage::APPLICATION == $this->m_intApplicationStageId && CApplicationStatus::PARTIALLY_COMPLETED == $this->m_intApplicationStatusId:
				case CApplicationStage::APPLICATION == $this->m_intApplicationStageId && CApplicationStatus::COMPLETED == $this->m_intApplicationStatusId:
				case CApplicationStage::APPLICATION == $this->m_intApplicationStageId && CApplicationStatus::APPROVED == $this->m_intApplicationStatusId:
				case CApplicationStage::APPLICATION == $this->m_intApplicationStageId && CApplicationStatus::CANCELLED == $this->m_intApplicationStatusId:

					if( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId() ) {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::RENEWAL_PROGRESS );
					} elseif( CLeaseIntervalType::EXTENSION == $this->getLeaseIntervalTypeId() ) {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::EXTENSION_LEASE_PROGRESS );
					} else {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::APPLICATION_PROGRESS );
					}
					break;

				case CApplicationStage::PRE_APPLICATION == $this->m_intApplicationStageId && CApplicationStatus::CANCELLED == $this->m_intApplicationStatusId:
					if( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId() ) {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::RENEWAL_PROGRESS );
					} else {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::APPLICATION_PROGRESS );
					}
					break;

				case ( CApplicationStage::PRE_APPLICATION == $this->m_intApplicationStageId || CApplicationStage::APPLICATION == $this->m_intApplicationStageId ) && CApplicationStatus::ON_HOLD == $this->m_intApplicationStatusId:
					if( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId() ) {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::RENEWAL_PROGRESS );
					} else {
						$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::APPLICATION_PROGRESS );
					}
					$objEvent->setDoNotExport( true ); // there is no need to export the event of hold
					break;

				case CApplicationStage::LEASE == $this->m_intApplicationStageId && CApplicationStatus::STARTED == $this->m_intApplicationStatusId:
				case CApplicationStage::LEASE == $this->m_intApplicationStageId && CApplicationStatus::PARTIALLY_COMPLETED == $this->m_intApplicationStatusId:
				case CApplicationStage::LEASE == $this->m_intApplicationStageId && CApplicationStatus::COMPLETED == $this->m_intApplicationStatusId:
				case CApplicationStage::LEASE == $this->m_intApplicationStageId && CApplicationStatus::APPROVED == $this->m_intApplicationStatusId:

					$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::LEASE_PROGRESS );
					$objEvent->setDoNotExport( true );
					break;

				default:

			}

			$objEvent->setCid( $this->getCid() );
			$objEvent->setLeaseId( $this->getLeaseId() );
			$objEvent->setLeaseIntervalId( $this->getLeaseIntervalId() );
			$objEvent->setNewStatusId( $this->getApplicationStatusId() );
			$objEvent->setOldStatusId( $intOldApplicationStatusId );
			$objEvent->setNewStageId( $this->getApplicationStageId() );
			$objEvent->setOldStageId( $intOldApplicationStageId );

			( CEventType::ONLINE_GUEST_CARD == $objEvent->getEventTypeId()
				&& true == is_null( $objEvent->getOldStageId() )
				&& true == is_null( $objEvent->getOldStatusId() ) )
				? $objEvent->setEventDatetime( $this->getApplicationDatetime() )
				: $objEvent->setEventDatetime( 'NOW()' );

			$objEvent->setNotes( $this->getEventNote() );
			$objEvent->setEventResultId( $this->getEventResultId() );
			$objEvent->setCreatedBy( $intCurrentUserId );

			if( true == valStr( $this->getEventSource() ) ) {
				$arrmixEventDetails                     = ( true == valObj( $objEvent->getDetails(), 'stdClass' ) ) ? ( array ) $objEvent->getDetails() : [];
				$arrmixEventDetails['event_source']     = $this->getEventSource();
				$objEvent->bindDetails( $arrmixEventDetails );
			}

			if( CCompanyUser::PROSPECT_PORTAL == $intCurrentUserId || ( isset( $_COOKIE['login_from_entrata_company_user_id'] ) && true == valId( $_COOKIE['login_from_entrata_company_user_id'] ) ) ) {
				$objEvent->setPsProductId( CPsProduct::PROSPECT_PORTAL );
			} elseif( CEventType::APPLICATION_PROGRESS == $objEvent->getEventTypeId() ) {
				$objEvent->setPsProductId( CPsProduct::ENTRATA_PAAS );
			} else {
				$objEvent->setPsProductId( $this->getPsProductId() );
			}

			if( ( true == isset( $this->m_intCancellationListItemId ) && true == isset( $this->m_intCancellationListTypeId ) ) && ( false == is_null( $this->m_intCancellationListItemId ) && false == is_null( $this->m_intCancellationListTypeId ) ) ) {
				$objEvent->setListTypeId( $this->m_intCancellationListTypeId );
				$objEvent->setListItemId( $this->m_intCancellationListItemId );
			}

			if( false == is_null( $intCurrentUserId ) ) {
				$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $intCurrentUserId, $this->getCid(), $objDatabase );
				if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
					$objEvent->setCompanyUser( $objCompanyUser );

					$objCompanyEmployee = $objCompanyUser->getOrFetchCompanyEmployee( $objDatabase );

					if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
						$objEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
					}
				}
			}

			if( false == is_null( $intDataReferenceId ) ) {
				$objEvent->setDataReferenceId( $intDataReferenceId );
			}

			// if application appRemotePrimary key is not null and application
			// status type is waiting for co-applicant then we should not integrate event.
			if( false == is_null( $intOldApplicationStageId ) && false == is_null( $intOldApplicationStatusId )
				&& ( ( CApplicationStage::APPLICATION == $intOldApplicationStageId && CApplicationStatus::ON_HOLD == $intOldApplicationStatusId )
					 || ( CApplicationStage::APPLICATION == $intOldApplicationStageId && CApplicationStatus::PARTIALLY_COMPLETED == $this->getApplicationStatusId()
				&& 0 < strlen( $this->getAppRemotePrimaryKey() ) && CApplicationStatus::CANCELLED != $intOldApplicationStatusId ) ) ) {
					$objEvent->setDoNotExport( true );
			}

			// this is check to see if there is already a event of application
			// completed, if found then dont export that event.
			if( CApplicationStage::APPLICATION == $this->getApplicationStageId() && CApplicationStatus::COMPLETED == $this->getApplicationStatusId()
				&& $intOldApplicationStageId != $this->getApplicationStageId() && $intOldApplicationStatusId != $this->getApplicationStatusId()
				&& CApplicationStatus::CANCELLED != $intOldApplicationStatusId ) {

				$objCompletedEvent = $this->fetchEventByEventTypeIdByNewStatusId( $objEvent->getEventTypeId(), CApplicationStage::APPLICATION, CApplicationStatus::PARTIALLY_COMPLETED, $objDatabase );

				if( true == valObj( $objCompletedEvent, 'CEvent' ) ) {
					$objEvent->setDoNotExport( true );
				}
			}

			if( CApplicationStatus::CANCELLED == $intOldApplicationStatusId && true == $boolDoNotExport ) {
				$objEvent->setDoNotExport( true );
			}

			$objEventLibrary->setIsExportEvent( $this->getIsExportEvent() );

			$objEventLibrary->buildEventDescription( $this );

			$strReturnValue = $objEventLibrary->insertEvent( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			if( false == $boolReturnSqlOnly && false == $strReturnValue ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to insert event.' ), NULL ) );
				$boolIsValid = false;
			}

			// Don't trigger Email events for Lease: Approved when property in Migration Mode
			if( false == $boolReturnSqlOnly && CApplicationStage::LEASE == $objEvent->getNewStageId() && CApplicationStatus::APPROVED == $objEvent->getNewStatusId() ) {
				$objPropertyGlSettings = \Psi\Eos\Entrata\CPropertyGlSettings::createService()->fetchPropertyGlSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
				if( true == valObj( $objPropertyGlSettings, 'CPropertyGlSetting' ) && true == $objPropertyGlSettings->getIsArMigrationMode() ) {
					$objEventScheduleTaskLibrary = new CUpdateEventScheduleTaskExecutionLibrary( $this->getCid(), $strReturnValue, $this->getPropertyId(), $objDatabase );
					try {
						$objEventScheduleTaskLibrary->insertEventScheduleTaskDetails( $intCurrentUserId );
					} catch( CEventScheduleTaskException $objEventScheduleTaskException ) {
						$this->addErrorMsg( $objEventScheduleTaskException->getMessage() );
						$boolIsValid = false;
					}
				}
			}

			if( true == $boolReturnSqlOnly ) {
				$strSql .= $strReturnValue;
			}
		}

		return( true == $boolReturnSqlOnly ) ? $strSql : $boolIsValid;
	}

	public function insertEventByEventTypeId( $intCurrentUserId, $objDatabase, $intEventTypeId, $boolDoNotExport = false, $boolReturnSqlOnly = false, $boolIsClonedApplication = false, $intOldPropertyId = NULL ) {

		$boolAddEvent 		= true;
		$boolIsValid 		= true;
		$objEventLibrary 	= new CEventLibrary();
		$strSql				= '';

		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		switch( $intEventTypeId ) {
			case CEventType::PROPERTY_CHANGE:
				$objEventLibraryDataObject->setOldPropertyId( $this->m_intOldPropertyId );
				$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::PROPERTY_CHANGE );
				$objEventLibrary->buildEventDescription( $this );
				break;

			case CEventType::CLONED:
				$objEventLibraryDataObject->setOldPropertyId( $intOldPropertyId );
				$objEventLibraryDataObject->setCloneApplicationId( $this->getApplicationDuplicateId() );
				$objEventLibraryDataObject->setIsClonedApplication( $boolIsClonedApplication );

				if( true == valId( $intOldPropertyId ) && ( $intOldPropertyId == $this->getPropertyId() ) ) {
					$objEventLibraryDataObject->setIsCompanyApplicationChanged( true );
				}

				$objEvent = $objEventLibrary->createEvent( [
						$this
				], CEventType::CLONED );

				$objEventLibrary->buildEventDescription( $this );
				break;

			case CEventType::UNIT_UNAVAILABLE:
				$objEventLibraryDataObject->setUnitNumber( $this->getUnitNumberCache() );

				$objEvent = $objEventLibrary->createEvent( [
						$this
				], CEventType::UNIT_UNAVAILABLE );
				$objEventLibrary->buildEventDescription( $this );
				break;

			default:
				$boolAddEvent = false;

		}

		if( true == $boolAddEvent ) {
			$objEvent->setCid( $this->getCid() );
			$objEvent->setEventDatetime( 'NOW()' );
			$objEvent->setDoNotExport( ( int ) $boolDoNotExport );
			$objEvent->setNotes( $this->getEventNote() );

			$objEventLibrary->setIsExportEvent( $this->getIsExportEvent() );

			$strReturnValue = $objEventLibrary->insertEvent( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			if( false == $boolReturnSqlOnly && false == $strReturnValue ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to insert event by event type.' ), NULL ) );
				$boolIsValid = false;
			}

			if( true == $boolReturnSqlOnly ) $strSql .= $strReturnValue;
		}

		return( true == $boolReturnSqlOnly ) ? $strSql : $boolIsValid;
	}

	public function deleteFileAssociationsAndFileSignatures( $arrobjFileAssociationsToDelete, $intCurrentUserId, $objDatabase ) {
		if( false == valArr( $arrobjFileAssociationsToDelete ) )
			return true;

		$arrintFileAssociationIds = array_keys( $arrobjFileAssociationsToDelete );
		$arrobjFileSignaturesToDelete = ( array ) $this->fetchFileSignaturesByFileAssociationIds( $arrintFileAssociationIds, $objDatabase );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				foreach( $arrobjFileSignaturesToDelete as $objFileSignatureToDelete ) {
					$boolIsValid &= $objFileSignatureToDelete->validate( VALIDATE_DELETE );
				}

				foreach( $arrobjFileAssociationsToDelete as $objFileAssociationToDelete ) {
					$boolIsValid &= $objFileAssociationToDelete->validate( VALIDATE_DELETE );
				}

				if( false == $boolIsValid ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Files failed to pass validation.' ), NULL ) );
					$boolIsValid = false;
					break;
				}

				foreach( $arrobjFileSignaturesToDelete as $objFileSignatureToDelete ) {
					if( false == $objFileSignatureToDelete->delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File signature failed to delete.' ), NULL ) );
						$boolIsValid &= false;
						break 2;
					}
				}

				foreach( $arrobjFileAssociationsToDelete as $objFileAssociationToDelete ) {
					if( false == $objFileAssociationToDelete->delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File failed to delete.' ), NULL ) );
						$boolIsValid &= false;
						break 2;
					}
				}
		}
	}

	public function exportEventsStack( $intCompanyUserId, $objDatabase, $boolQueueSyncOverride = false ) {
		$boolIsValid = true;

		if( false == $this->getIsExportEvent() )
			return true;

		$arrobjEvents = \Psi\Eos\Entrata\CEvents::createService()->fetchNonExportedEventsByPropertyIdByLeaseIntervalIdByCid( $this->getPropertyId(), $this->getLeaseIntervalId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrobjEvents ) )
			return false;

		ksort( $arrobjEvents );
		$objEvent = current( $arrobjEvents );

		if( false == valObj( $objEvent, 'CEvent' ) )
			return false;

		if( false == $objEvent->exportStack( $intCompanyUserId, $objDatabase, $arrobjEvents, $boolQueueSyncOverride ) ) {
			$boolIsValid = false;
		}

		if( true == $this->getIsShowIntegrationError() && true == valArr( $objEvent->getErrorMsgs() ) ) {
			$this->addErrorMsgs( $objEvent->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function getOrFetchRwxApplicationStepsByIds( $arrintIds, $objDatabase ) {

		if( false == isset( $this->m_arrobjRwxApplicationSteps ) || false == valArr( $this->m_arrobjRwxApplicationSteps ) ) {
			if( false == valObj( $objDatabase, 'CDatabase' ) ) return NULL;
			$this->m_arrobjRwxApplicationSteps = CApplicationSteps::fetchApplicationStepsByIds( $arrintIds, $objDatabase );
		}

		return $this->m_arrobjRwxApplicationSteps;
	}

	public function buildFilePath( $strSystemCode ) {
		$strPath = '';

		switch( $strSystemCode ) {
			case CFileType::SYSTEM_CODE_LEASE_DOCUMENT:
				$strPath = $this->getId() . '/lease_documents/';
				break;

			case CFileType::SYSTEM_CODE_APPLICATION:
				$strPath = $this->getCid() . '/applications/application/' . $this->getPropertyId() . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getId() . '/';
				break;

			default:
				$strPath = $this->getId() . '/';
		}

		return $strPath;
	}

	public function buildFileName( $objFile, $strExtension = 'docx' ) {
		$strFileName = '';

		if( false == is_null( $objFile->getFileTypeSystemCode() ) ) {
			$strFileName = $objFile->getId() . '_' . \Psi\CStringService::singleton()->strtolower( preg_replace( '/\s+/', '_', $objFile->getFileTypeName() ) ) . '_' . date( 'Ymdhis' ) . '.' . $strExtension;
		} else {
			$strFileName = $objFile->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;
		}

		return $strFileName;
	}

	public function buildFileData( $objFile, $strInputTypeFileName = 'lease_file_name', $objDatabase, $boolRequiresignature = false ) {

		if( true == empty( $_FILES ) && empty( $_POST ) && isset( $_SERVER['REQUEST_METHOD'] ) && 'post' == \Psi\CStringService::singleton()->strtolower( $_SERVER['REQUEST_METHOD'] ) ) {
			$strPostMaxSize = ini_get( 'post_max_size' );
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload. File size should not exceed more than' ) . ' ' . $strPostMaxSize . 'B.' ) );
			return false;
		}

		if( true == is_null( $_FILES[$strInputTypeFileName]['name'] ) || 0 == strlen( trim( $_FILES[$strInputTypeFileName]['name'] ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select a file to upload.' ) ) );
			return false;
		}

		if( true == is_null( $objFile->getId() ) || false == is_numeric( $objFile->getId() ) ) {
			$objFile->setId( $objFile->fetchNextId( $objDatabase ) );
		}

		$objFile->setTitle( $_FILES[$strInputTypeFileName]['name'] );

		$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		$strExtension = ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$strFileName = $this->buildFileName( $objFile, $strExtension );

		$strMimeType = '';
		$objFileExtension = \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		if( true == isset( $_FILES[$strInputTypeFileName]['tmp_name'] ) )
			$objFile->setTempFileName( $_FILES[$strInputTypeFileName]['tmp_name'] );
		if( true == isset( $_FILES[$strInputTypeFileName]['error'] ) )
			$objFile->setFileError( $_FILES[$strInputTypeFileName]['error'] );
		if( true == isset( $strFileName ) )
			$objFile->setFileName( $strFileName );
		if( true == isset( $strMimeType ) )
			$objFile->setFileType( $strMimeType );

		$objFile->setFileExtensionId( $objFileExtension->getId() );

		$objFile->setFilePath( $this->buildFilePath( $objFile->getFileTypeSystemCode() ) );

		if( true == $boolRequiresignature ) {
			if( false == $objFile->validate( 'valid_upload_sign_document' ) ) {
				$this->addErrorMsgs( $objFile->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	public function deleteOfferToRentFiles( $intCompanyUserId, $objDatabase ) {
		$arrobjDeletingFiles = ( array ) $this->fetchFilesByFileTypeSystemCodes( [
				CFileType::SYSTEM_CODE_OFFER_TO_RENT
		], $objDatabase );

		if( false == valArr( $arrobjDeletingFiles ) ) {
			return true;
		}

		$boolIsValid = true;

		$arrobjFileAssociations = ( array ) $this->fetchFileAssociationsByFileIds( array_keys( $arrobjDeletingFiles ), $objDatabase );

		foreach( $arrobjFileAssociations as $objFileAssociation ) {
			if( false == $objFileAssociation->delete( $intCompanyUserId, $objDatabase, false ) ) {
				$boolIsValid &= false;
				break;
			}
		}

		if( true == $boolIsValid ) {
			foreach( $arrobjDeletingFiles as $objDeletingFile ) {

				$objDeletingFile->deleteFile();

				if( false == $objDeletingFile->delete( $intCompanyUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function resetPropertyUnitInfo( $intUserId, $objDatabase ) {
		$this->setPropertyFloorplanId( NULL );
		$this->setPropertyFloorplan( NULL );

		$this->setPropertyUnitId( NULL );
		$this->setPropertyUnit( NULL );

		$this->setUnitSpaceId( NULL );
		$this->setUnitSpace( NULL );

		$objDatabase->begin();

		if( false == $this->update( $intUserId, $objDatabase ) ) {
			$objDatabase->rollback();
		}

		$objDatabase->commit();
	}

	// TO DO: Need to refactor/deprecate this function to make it independent of application object. Created new function in CWebsites:generatePropertyWebsiteUrl
	public function generateWebsiteUrl( $objDatabase, $strSecureBaseName = NULL, $boolSendInvitation = false, $boolPreferDefaultWebsite = false, $boolIsFromRI = false ) {

		$arrmixRequiredParameters = $this->getRequiredParameters();
		$boolIsUseLink = isset( $arrmixRequiredParameters['ar_payment_id'] );

		$strProspectPortalSuffix = '.prospectportal.com';
		$boolCheckForResidentPortal = false;

		if( true == in_array( $this->getLeaseIntervalTypeId(), [ CLeaseIntervalType::RENEWAL, CLeaseIntervalType::LEASE_MODIFICATION, CLeaseIntervalType::TRANSFER ] ) ) {
			$boolCheckForResidentPortal = true;
		}

		if( CLeaseIntervalType::TRANSFER === ( int ) $this->getLeaseIntervalTypeId() && false == $boolIsFromRI) {
			$strProspectPortalSuffix = '.residentportal.com';
		}

		if( true == defined( CONFIG_PROSPECT_PORTAL_SUFFIX ) ) {
			$strProspectPortalSuffix = CONFIG_PROSPECT_PORTAL_SUFFIX;
		} elseif( false == is_null( CConfig::get( 'prospect_portal_suffix' ) ) ) {
			$strProspectPortalSuffix = CConfig::get( 'prospect_portal_suffix' );
		}

		if( CLeaseIntervalType::TRANSFER === ( int ) $this->getLeaseIntervalTypeId() && false == $boolIsFromRI ) {
			if( true == defined( CONFIG_RESIDENT_PORTAL_SUFFIX ) ) {
				$strProspectPortalSuffix = CONFIG_RESIDENT_PORTAL_SUFFIX;
			} elseif( false == is_null( CConfig::get( 'resident_portal_suffix' ) ) && false == $boolSendInvitation ) {
				$strProspectPortalSuffix = CConfig::get( 'resident_portal_suffix' );
			}
		}

		if( true == $boolPreferDefaultWebsite ) {
			$objDefaultWebsitePropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'RESIDENT_PORTAL_AUTO_LOGIN_DOMAIN', $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		if( true == valObj( $objDefaultWebsitePropertyPreference, CPropertyPreference::class ) && true == valStr( $objDefaultWebsitePropertyPreference->getValue() ) ) {
			$objResidentPortalDefaultWebsite = CWebsites::createService()->fetchCustomWebsiteBySubDomain( $objDefaultWebsitePropertyPreference->getValue(), $objDatabase );
		}

		if( false == is_null( $this->getWebsiteId() ) || true == valObj( $objResidentPortalDefaultWebsite, CWebsite::class ) ) {

			$objWebsite = ( true == valObj( $objResidentPortalDefaultWebsite, CWebsite::class ) ) ? $objResidentPortalDefaultWebsite : $this->getOrFetchWebsite( $objDatabase, $boolSendInvitation );

			if( true == valObj( $objWebsite, 'CWebsite' ) && true == is_null( $objWebsite->getDeletedOn() ) && 1 != $objWebsite->getIsDisabled() ) {
				if( '7890' == $this->getCid() ) {
					$arrobjProperties = ( array ) $objWebsite->fetchParentChildProperties( $objDatabase );
				} else {
					$arrobjProperties = ( array ) $objWebsite->fetchProperties( $objDatabase );
				}
				$arrobjRekeyProperties = rekeyObjects( 'PropertyId', $arrobjProperties );

				if( true == valArr( $arrobjProperties ) && ( true == array_key_exists( $this->getPropertyId(), $arrobjProperties ) || true == array_key_exists( $this->getPropertyId(), $arrobjRekeyProperties ) ) ) {

					if( 'production' == CConfig::get( 'environment' ) ) {
						$objWebsiteDomain = $objWebsite->fetchPrimaryWebsiteDomain( $objDatabase );

						if( true == valObj( $objWebsiteDomain, 'CWebsiteDomain' ) && true == $objWebsiteDomain->getIsSecure() ) {
							$arrstrHostTmp 	= explode( '.', str_replace( 'www.', '', $objWebsiteDomain->getWebsiteDomain() ) );
							if( true == $boolIsUseLink ) {
								$strSecureBaseName = ( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrHostTmp ) ? 'www.' : '' ) . $objWebsiteDomain->getWebsiteDomain();
							} else {
								$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . ( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrHostTmp ) ? 'www.' : '' ) . $objWebsiteDomain->getWebsiteDomain() . '/';
							}
							return $strSecureBaseName;
						}
					}

					if( true == $boolIsUseLink ) {
						$strSecureBaseName = $objWebsite->getSubDomain() . $strProspectPortalSuffix;
					} else {
						$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . $objWebsite->getSubDomain() . $strProspectPortalSuffix . '/';
					}
					return $strSecureBaseName;
				}
			}
		}

		$objProperty = $this->getOrFetchProperty( $objDatabase );

		 // CKT : Below code added to fetch website url of parent property when
		 // the application is holding child property id( i.e. child property unit is selected )

		if( true == is_null( $strSecureBaseName ) && false == is_null( $objProperty->getPropertyId() ) ) {
			$objWebsite  = NULL;
			$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objProperty->getPropertyId(), $objProperty->getCid(), $objDatabase );

			if( true == valObj( $objProperty, 'CProperty' ) ) {

				if( false == is_null( $this->getWebsiteId() ) ) {
					$objWebsite = $this->getOrFetchWebsite( $objDatabase );
				}

				// CKT : First verify whether the parent property exists within
				// the website from where the application was generated
				if( true == valObj( $objWebsite, 'CWebsite' ) && true == is_null( $objWebsite->getDeletedOn() ) && 1 != $objWebsite->getIsDisabled() ) {
					$arrobjProperties = $objWebsite->fetchProperties( $objDatabase );
					$arrobjRekeyProperties = rekeyObjects( 'PropertyId', $arrobjProperties );

					if( true == valArr( $arrobjProperties ) && true == array_key_exists( $objProperty->getId(), $arrobjProperties ) ) {
						if( true == $boolIsUseLink ) {
							$strSecureBaseName = $objWebsite->getSubDomain() . $strProspectPortalSuffix;
						} else {
							$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . $objWebsite->getSubDomain() . $strProspectPortalSuffix . '/';
						}
					}
				}

				// CKT : If the property is not longer associated to the website
				// used for creating application then fetch the default website
				if( true == is_null( $strSecureBaseName ) ) {
					$objWebsite = $objProperty->fetchDefaultNonLobbyTypeWebsite( $objDatabase, $boolCheckForResidentPortal );

					if( true == valObj( $objWebsite, 'CWebsite' ) ) {
						if( true == $boolIsUseLink ) {
							$strSecureBaseName = $objWebsite->getSubDomain() . $strProspectPortalSuffix;
						} else {
							$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . $objWebsite->getSubDomain() . $strProspectPortalSuffix . '/';
						}
					}
				}
			}
		}

		if( true == is_null( $strSecureBaseName ) && true == valObj( $objProperty, 'CProperty' ) ) {
			$objWebsite = $objProperty->fetchDefaultNonLobbyTypeWebsite( $objDatabase, $boolCheckForResidentPortal );

			if( true == valObj( $objWebsite, 'CWebsite' ) ) {
				if( true == $boolIsUseLink ) {
					$strSecureBaseName = $objWebsite->getSubDomain() . $strProspectPortalSuffix;
				} else {
					$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . $objWebsite->getSubDomain() . $strProspectPortalSuffix . '/';
				}
			}
		}

		return $strSecureBaseName;
	}

	public function associateLeasingAgent( $objDatabase, $objCompanyUser ) {
		if( true == is_null( $this->getLeasingAgentId() ) ) {

			$intLeasingAgentId = NULL;
			$objClient = $this->getOrFetchClient( $objDatabase );

			if( true == valObj( $objCompanyUser, 'CCompanyUser' ) && true == valObj( $objClient, 'CClient' ) ) {

				$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase, true );

				if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
					$objCompanyEmployeeDepartment = \Psi\Eos\Entrata\CCompanyEmployeeDepartments::createService()->fetchCompanyEmployeeDepartmentByCidByCompanyEmployeeIdByDefaultCompanyDepartmentId( $objClient->getId(), $objCompanyEmployee->getId(), CCompanyDepartment::DEFAULT_COMPANY_DEPARTMENT_LEASING_ID, $objDatabase );
					if( true == valObj( $objCompanyEmployeeDepartment, 'CCompanyEmployeeDepartment' ) ) {
						$intLeasingAgentId = $objCompanyEmployee->getId();

						if( false == is_null( $intLeasingAgentId ) ) {
							$this->setLeasingAgentId( $intLeasingAgentId );
							return true;
						}
					}
				}

			}
		}

		return false;
	}

	public function associateIsPetPolicyFlag( $arrobjCustomerPets, $arrobjPetRateAssociations ) {
		$arrmixPetTypeCounts = [];

		foreach( $arrobjCustomerPets as $objCustomerPet ) {
			$intPetTypeCount = 1;

			if( true == valArr( $arrmixPetTypeCounts ) && true == array_key_exists( $objCustomerPet->getPetTypeId(), $arrmixPetTypeCounts ) ) {

				$intPetTypeCount = $arrmixPetTypeCounts[$objCustomerPet->getPetTypeId()]['pet_count'] + 1;
				$arrmixPetTypeCounts[$objCustomerPet->getPetTypeId()]['pet_count'] = $intPetTypeCount;
				if( $arrmixPetTypeCounts[$objCustomerPet->getPetTypeId()]['weight_limit'] < $objCustomerPet->getWeight() ) {
					$arrmixPetTypeCounts[$objCustomerPet->getPetTypeId()]['weight_limit'] = $objCustomerPet->getWeight();
				}
				continue;
			}

			$arrmixPetTypeCounts[$objCustomerPet->getPetTypeId()]['pet_count'] = $intPetTypeCount;
			$arrmixPetTypeCounts[$objCustomerPet->getPetTypeId()]['weight_limit'] = $objCustomerPet->getWeight();
		}

		if( true == valArr( $arrmixPetTypeCounts ) && true == valArr( $arrobjPetRateAssociations ) ) {
			foreach( $arrobjPetRateAssociations as $objPetRateAssociation ) {
				$strMaxAllowed = $objPetRateAssociation->getPetMaxAllowed();
				if( true == array_key_exists( $objPetRateAssociation->getArOriginReferenceId(), $arrmixPetTypeCounts ) ) {
					$strWeightLimit = $objPetRateAssociation->getPetWeightLimit();
					if( ( $objPetRateAssociation->getPetMaxAllowed() < $arrmixPetTypeCounts[$objPetRateAssociation->getArOriginReferenceId()]['pet_count'] && false == empty( $strMaxAllowed ) && 0 < $objPetRateAssociation->getPetMaxAllowed() ) || ( $objPetRateAssociation->getPetWeightLimit() < $arrmixPetTypeCounts[$objPetRateAssociation->getArOriginReferenceId()]['weight_limit'] && false == empty( $strWeightLimit ) ) ) {
						$this->setIsPetPolicyConflicted( true );
						break;
					}
				}
			}
		}
	}

	public function insertResidentVerifyPolicyDocuments( $intCurrentUserId, $arrmixScreeningSystemCodes, $objDatabase ) {

		if( true == is_null( $this->getCompanyApplicationId() ) ) return;

		$objClient 	= $this->getOrFetchClient( $objDatabase );
		$objCompanyApplication 	= $objClient->fetchCompanyApplicationById( $this->getCompanyApplicationId(), $objDatabase );

		if( false == valObj( $objCompanyApplication, 'CCompanyApplication' ) ) return false;

		// Need to handle ScreeningAuthroziation and ConsumerAuthroziation

        if( true == in_array( CFileType::SYSTEM_CODE_SCREENING_AUTHORIZATION_AND_RIGHTS, $arrmixScreeningSystemCodes ) ) {
            $objDocument 		= $objCompanyApplication->createPolicyDocument();
            $strMountsFilePath 	= PATH_MOUNTS . '/documents/policy_documents/resident_verify/Screening Authorization and Rights.pdf';

            $objDocument->setName( CFileType::SCREENING_AUTHORIZATION_AND_RIGHTS_NAME );

            if( false == $this->insertAuthorizationDocument( $objDocument, $strMountsFilePath, $intCurrentUserId, $objDatabase ) ) {
                return false;
            }
        }

        if( true == in_array( CFileType::SYSTEM_CODE_CONSUMER_AUTHORIZATION_AND_ELECTRONIC_TRANSACTION, $arrmixScreeningSystemCodes ) ) {
            $objDocument = $objCompanyApplication->createPolicyDocument();

            $strMountsFilePath 	= PATH_MOUNTS . '/documents/policy_documents/resident_verify/Agreement to Conduct an Electronic Transaction.pdf';
            $objDocument->setName( CFileType::CONSUMER_AUTHORIZATION_AND_ELECTRONIC_TRANSACTION_NAME );

            if( false == $this->insertAuthorizationDocument( $objDocument, $strMountsFilePath, $intCurrentUserId, $objDatabase ) ) {
                return false;
            }
        }

		if( true == in_array( CFileType::SYSTEM_CODE_CONSUMER_AUTHORIZATION_CANADA, $arrmixScreeningSystemCodes ) ) {
			$objDocument = $objCompanyApplication->createPolicyDocument();

			$strMountsFilePath 	= PATH_MOUNTS . '/documents/policy_documents/resident_verify/Consumer Authorization Canada.pdf';
			$objDocument->setName( CFileType::CONSUMER_AUTHORIZATION_CANADA_NAME );

			if( false == $this->insertAuthorizationDocument( $objDocument, $strMountsFilePath, $intCurrentUserId, $objDatabase ) ) {
				return false;
			}
		}

		$objDatabase->commit();

		return true;
	}

	public function insertAuthorizationDocument( $objDocument, $strMountsFilePath, $intCurrentUserId, $objDatabase ) {

		$objDocument->setDescription( 'Resident Verify Policy Document' );
		$objDocument->setDocumentSubTypeId( CDocumentSubType::RESIDENT_VERIFY_SCREENING_POLICY_DOCUMENT );
        $objDocument->setSystemCode( CFileType::SYSTEM_CODE_POLICY );

		$objDocumentAddenda = $objDocument->createDocumentAddenda( CDocumentAddendaType::POLICY_DOCUMENT );
		$objDocumentAddenda->mapDocumentData( $objDocument );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				$boolIsValid &= $objDocument->validate( 'insert_update_policy_document' );
				$boolIsValid &= $objDocumentAddenda->validate( 'insert_update_policy_document' );

				if( false == $boolIsValid ) return false;

				$objDocument->setId( $objDocument->fetchNextId( $objDatabase ) );

				$intCurrentYear 				= date( 'Y' );
				$intCurrentMonth 				= date( 'm' );
				$intCurrentDay   				= date( 'd' );

                $strDocFilePath                 = $objDocument->buildDocumentFilePath();
				$strFileName					= $intCurrentYear . $intCurrentMonth . $intCurrentDay . '_' . $this->getCid() . '_' . $objDocument->getId() . '.pdf';

				$objDocument->setFilePath( $strDocFilePath );
				$objDocument->setFileName( $strFileName );
                $this->loadObjectStorageGateway();

				$arrmixPutGatewayRequest	= $objDocument->createPutGatewayRequest( [ 'inputFile' => $strMountsFilePath ] );
				$objPutGatewayResponse		= $this->getObjectStorageGateway()->putObject( $arrmixPutGatewayRequest );

				if( true == $objPutGatewayResponse->hasErrors() ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create policy file' ), NULL ) );
					$objDatabase->rollback();
					return false;
				}

				$objDocument->setObjectStorageGatewayResponse( $objPutGatewayResponse );

				if( false == $objDocument->insert( $intCurrentUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					return false;
				}

				$objDocumentAddenda->setDocumentId( $objDocument->getId() );

				if( false == $objDocumentAddenda->insert( $intCurrentUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					return false;
				}

				return true;
		}
	}

	public function isResidentVerifyEnabled( $objDatabase ) {
		$arrobjPropertyProducts  		= \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByTransmissionVendorIdsByPsProductIdsByCid( [ CTransmissionVendor::RESIDENT_VERIFY ], [ CPsProduct::RESIDENT_VERIFY ], $this->getCid(), $objDatabase );
		$boolIsResidentVerifyEnabled 	= ( true == valArr( $arrobjPropertyProducts ) ) ? in_array( $this->getPropertyId(), array_keys( $arrobjPropertyProducts ) ) : false;

		return $boolIsResidentVerifyEnabled;
	}

	public function isStudentTierRenewal( $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT sg.special_group_type_id
					FROM
						applications a
						JOIN quotes q ON (q.cid = a.cid AND q.application_id = a.id)
						JOIN specials s ON (s.cid = q.cid AND s.id = q.leasing_tier_special_id)
						JOIN special_groups sg ON (sg.cid = s.cid AND sg.id = s.special_group_id)
					WHERE
						sg.deleted_on IS NULL
						AND sg.deleted_by IS NULL
						AND s.deleted_on IS NULL
						AND s.is_active = \'true\'::boolean
						AND a.id = ' . ( int ) $this->getId() . '
						AND a.cid = ' . ( int ) $this->getCid();

		$arrintSpecialGroupTypeId = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintSpecialGroupTypeId ) && 1 != $arrintSpecialGroupTypeId[0]['special_group_type_id'] ) {
			return true;
		}

		return false;
	}

	public function isPartiallyGeneratedLease( $objDatabase ) {
		$intNonGeneratedFileCount = CFiles::createService()->fetchNonGeneratedFileCountByApplicationIdBySystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_LEASE_ADDENDUM, $this->getCid(), $objDatabase );
		if( 0 == $intNonGeneratedFileCount ) return false;
		return true;
	}

	public function getSatisfiedTransmissionCondition( $objDatabase ) {

		$objSatisfiedTransmissionCondition 		= NULL;
		$arrintTransmissionType					= [ CTransmissionConditionType::VERIFY_IDENTITY, CTransmissionConditionType::REQUIRE_CERTIFIED_FUNDS, CTransmissionConditionType::REQUIRE_CRIMINAL_CONFIRMATION, CTransmissionConditionType::CUSTOM_SCREENING_CONDITION ];

		$arrobjTransmissionConditions 			= $this->fetchTransmissionConditions( $objDatabase );
		$arrintTransmissionType 				= [ CTransmissionConditionType::VERIFY_IDENTITY, CTransmissionConditionType::REQUIRE_CERTIFIED_FUNDS, CTransmissionConditionType::REQUIRE_CRIMINAL_CONFIRMATION, CTransmissionConditionType::CUSTOM_SCREENING_CONDITION ];

		if( true == valArr( $arrobjTransmissionConditions ) ) {
			foreach( $arrobjTransmissionConditions as $objTransmissionCondition ) {
				if( false == in_array( $objTransmissionCondition->getTransmissionConditionTypeId(), $arrintTransmissionType ) ) {
					if( false == is_null( $objTransmissionCondition->getSatisfiedBy() ) && false == is_null( $objTransmissionCondition->getSatisfiedOn() ) ) {
						$objSatisfiedTransmissionCondition = ( false == is_null( $objTransmissionCondition->getDepositArTransactionId() ) || false == is_null( $objTransmissionCondition->getRentScheduledChargeId() ) || ( CTransmissionConditionType::REQUIRE_GUARANTOR == $objTransmissionCondition->getTransmissionConditionTypeId() ) ) ? $objTransmissionCondition  : [];
					}
				}
			}
		}

		return $objSatisfiedTransmissionCondition;
	}

	// Calcuate Application Rent amount

	public function calculateApplicationRentAmount( $objDatabase ) {

		$fltSpecialsApplicationRecurringChargeAmount 	= 0;
		$fltApplicationRecurringChargeAmount  			= 0;
		$fltTotalRentAmount								= 0;

		$arrintApplicantionRentAmounts 					= [];
		$arrintLeaseIntervalIds 						= [ $this->getLeaseIntervalId() ];

		if( CLeaseIntervalType::LEASE_MODIFICATION == $this->getLeaseIntervalTypeId() ) {
			$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
			array_push( $arrintLeaseIntervalIds, $objLease->getActiveLeaseIntervalId() );
		}

		$arrobjApplicationRecurringCharges 	= ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchActiveScheduledChargesByLeaseIdByLeaseIntervalIdsByArTriggerIdsByCid( $arrintLeaseIntervalIds, CArTrigger::$c_arrintRecurringArTriggers, $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjApplicationRecurringCharges ) ) {

			foreach( $arrobjApplicationRecurringCharges as $objApplicationRecurringCharge ) {

				if( true == valObj( $objApplicationRecurringCharge, 'CScheduledCharge' ) ) {

					if( CArOrigin::SPECIAL == $objApplicationRecurringCharge->getArOriginId() ) {
						if( CArCodeType::RENT == $objApplicationRecurringCharge->getArCodeTypeId() || ( CArCodeType::OTHER_INCOME == $objApplicationRecurringCharge->getArCodeTypeId() && CArTrigger::MONTHLY == $objApplicationRecurringCharge->getArTriggerId() ) ) {
							// Special charges
							$fltSpecialsApplicationRecurringChargeAmount += ( float ) $objApplicationRecurringCharge->getChargeAmount();
						}
					} else {
						// Other Recurring charges
						$fltApplicationRecurringChargeAmount += $objApplicationRecurringCharge->getChargeAmount();
					}
				}
			}

			// Calculating total rent amount.
			$fltTotalRentAmount = $fltApplicationRecurringChargeAmount + $fltSpecialsApplicationRecurringChargeAmount;

			array_push( $arrintApplicantionRentAmounts, $fltTotalRentAmount, $fltSpecialsApplicationRecurringChargeAmount );
		} else {
			array_push( $arrintApplicantionRentAmounts, $fltTotalRentAmount );
		}

		return $arrintApplicantionRentAmounts;
	}

	public function calculateApplicationDepositAmount( $objDatabase ) {

		$fltApplicationDepositChargeAmount  = 0;
		$arrintLeaseIntervalIds 			= [ $this->getLeaseIntervalId() ];

		if( CLeaseIntervalType::LEASE_MODIFICATION == $this->getLeaseIntervalTypeId() ) {
			$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
			array_push( $arrintLeaseIntervalIds, $objLease->getActiveLeaseIntervalId() );
		}

		$arrobjApplicationDepositCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIdByLeaseIntervalIdsByArCodeTypeIdsByCid( $this->getLeaseId(), $arrintLeaseIntervalIds, [ CArCodeType::DEPOSIT ], $this->getCid(), $objDatabase );

		foreach( $arrobjApplicationDepositCharges as $objApplicationDepositCharge ) {

			if( false == valObj( $objApplicationDepositCharge, 'CScheduledCharge' ) ) continue;

			$fltApplicationDepositChargeAmount += ( float ) $objApplicationDepositCharge->getChargeAmount();
		}

		return $fltApplicationDepositChargeAmount;
	}

	public function fetchScreeningApplicationConditionValuesByScreeningApplicationConditionSetIdByConditionTypeIds( $arrintConditionTypeIds, $objDatabase, $boolFetchAllRecord = false ) {
		return CScreeningApplicationConditionValues::fetchScreeningApplicationConditionValuesByCidByScreeningApplicationConditionSetIdByConditionTypeIds( $this->getCid(), $this->getId(), $arrintConditionTypeIds, $objDatabase, $boolFetchAllRecord );
	}

	public function processAddOnLeaseAssociaitonsAttachedToUnit( $objUnitSpace, $intCurrentUserId, $objDatabase, $objOldUnitSpace = NULL ) {

		if( true == $this->hasApplicationStageStatusIn( CApplicationStageStatus::$c_arrintCombineLeadApplicationStageStatusIds ) && false == in_array( $this->getLeaseIntervalTypeId(), [ CLeaseIntervalType::LEASE_MODIFICATION, CLeaseIntervalType::TRANSFER, CLeaseIntervalType::RENEWAL ] ) && false == $this->isGuestCard() ) {

			$objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

			$objApplicant = $this->getOrFetchPrimaryApplicant( $objDatabase );

			$objAddOnLeaseAssociationsLibrary = new CRentableItemReservationsLibrary();
			$objAddOnLeaseAssociationsLibrary->m_objApplication			= $this;
			$objAddOnLeaseAssociationsLibrary->m_objClient 				= $this->getOrFetchClient( $objDatabase );
			$objAddOnLeaseAssociationsLibrary->m_objProperty 			= $this->getOrFetchProperty( $objDatabase );
			$objAddOnLeaseAssociationsLibrary->m_intCompanyUserId 		= $intCurrentUserId;
			$objAddOnLeaseAssociationsLibrary->m_objDatabase 			= $objDatabase;
			$objAddOnLeaseAssociationsLibrary->m_objLease 		 		= $objLease;
			$objAddOnLeaseAssociationsLibrary->m_intLeaseIntervalId 	= $this->getLeaseIntervalId();
			$objAddOnLeaseAssociationsLibrary->m_objLeaseInterval		= $this->fetchLeaseInterval( $objDatabase );

			if( true == valObj( $objApplicant, 'CApplicant' ) ) {
				$objAddOnLeaseAssociationsLibrary->m_intApplicantId	= $objApplicant->getId();
			}

			if( false == is_null( $this->getLeaseId() ) ) {
				$objAddOnLeaseAssociationsLibrary->m_intLeaseId = $this->getLeaseId();
			}

			if( true == valObj( $objOldUnitSpace, 'CUnitSpace' ) ) {
				$intOldUnitSpaceAddOnCounts = \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnCountByUnitSpaceIdsByPropertyIdByCid( [ $objOldUnitSpace->getId() ], $this->getPropertyId(), $this->getCid(), $objDatabase );
			}

			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$intAddOnCounts = \Psi\Eos\Entrata\CAddOns::createService()->fetchAddOnCountByUnitSpaceIdsByPropertyIdByCid( [ $objUnitSpace->getId() ], $this->getPropertyId(), $this->getCid(), $objDatabase );
				$objAddOnLeaseAssociationsLibrary->m_objUnitSpace = $objUnitSpace;
				if( false == $objAddOnLeaseAssociationsLibrary->addDefaultAddOnServicesLeaseAssociation() ) {
					$this->addErrorMsg( $objAddOnLeaseAssociationsLibrary->getErrorMsgs() );
					return false;
				}
			}

			if( true == valObj( $objOldUnitSpace, 'CUnitSpace' ) && 0 < $intOldUnitSpaceAddOnCounts ) {

				$objAddOnLeaseAssociationsLibrary->m_objUnitSpace = $objOldUnitSpace;

				if( false == $objAddOnLeaseAssociationsLibrary->detachAddOnLeaseAssociationsFromUnit() ) {
					$this->addErrorMsg( $objAddOnLeaseAssociationsLibrary->getErrorMsgs() );
					return false;
				}
			}

			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) && 0 < $intAddOnCounts ) {
				$objAddOnLeaseAssociationsLibrary->m_objUnitSpace = $objUnitSpace;

				if( true == is_null( $this->getLeaseEndDate() ) ) {
					$arrobjPropertyPreferences = [];
					if( true == valObj( $objAddOnLeaseAssociationsLibrary->m_objProperty, 'CProperty' ) ) {
						$arrobjPropertyPreferences = $objAddOnLeaseAssociationsLibrary->m_objProperty->getOrFetchPropertyPreferences( $objDatabase );
					}
					$this->setLeaseTermAndDates( $arrobjPropertyPreferences, $objDatabase );
				}

				if( false == $objAddOnLeaseAssociationsLibrary->processAddOnsAttachedToUnit() ) {
					$this->addErrorMsg( $objAddOnLeaseAssociationsLibrary->getErrorMsgs() );
					return false;
				}
			}

		}
		return true;
	}

	public function getPropertyGroupIds( $objDatabase ) {
		$this->m_arrintPropertyGroupIds = ( array ) \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchSimplePropertyGroupIdsByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $objDatabase );
		return $this->m_arrintPropertyGroupIds;
	}

	public function postScheduledCharges( $intUserId, $objDatabase, $boolIsPMSoftwareEnabled = false, $arrintArTriggerIds = [], $strRewindDate = NULL, $arrintScheduledChargeIds = [] ) {

		// setup list of charge types to post
		if( false == valArr( $arrintArTriggerIds ) ) {

			switch( true ) {
				case CApplicationStage::APPLICATION == $this->getApplicationStageId() && CApplicationStatus::STARTED == $this->getApplicationStatusId():
				case CApplicationStage::APPLICATION == $this->getApplicationStageId() && CApplicationStatus::PARTIALLY_COMPLETED == $this->getApplicationStatusId():
				case CApplicationStage::APPLICATION == $this->getApplicationStageId() && CApplicationStatus::COMPLETED == $this->getApplicationStatusId():
					$arrintArTriggerIds = [ CArTrigger::APPLICATION_COMPLETED, CArTrigger::PRE_QUALIFICATION, CArTrigger::ONE_TIME ];
					break;

				case CApplicationStage::APPLICATION == $this->getApplicationStageId() && CApplicationStatus::APPROVED == $this->getApplicationStatusId():
					$arrintArTriggerIds = [ CArTrigger::APPLICATION_APPROVAL ];
					break;

				case CApplicationStage::LEASE == $this->getApplicationStageId() && CApplicationStatus::COMPLETED == $this->getApplicationStatusId():
				case CApplicationStage::LEASE == $this->getApplicationStageId() && CApplicationStatus::PARTIALLY_COMPLETED == $this->getApplicationStatusId():
					$arrintArTriggerIds = [ CArTrigger::LEASE_COMPLETED ];
					break;

				case CApplicationStage::LEASE == $this->getApplicationStageId() && CApplicationStatus::APPROVED == $this->getApplicationStatusId():
					$arrintArTriggerIds = [ CArTrigger::LEASE_APPROVAL ];

					if( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId() ) {
						$arrintArTriggerIds = array_merge( $arrintArTriggerIds, [ CArTrigger::MONTHLY ] );
					}
					break;

				default:
					// default do nothing
					break;
			}
		}

		$objPostScheduledChargesLibrary 	= CPostScheduledChargeFactory::createObject( CScheduledChargePostingMode::APPLICATION_SYSTEM, $objDatabase );
		$objPostScheduledChargesCriteria	= $objPostScheduledChargesLibrary->loadPostScheduledChargesCriteria( $this->getCid(), [ $this->getPropertyId() ], $intUserId );

		$objPostScheduledChargesCriteria->setScheduledChargeIds( $arrintScheduledChargeIds )
										->setScheduledChargeRewindDateReference( $strRewindDate )
										->setLeaseIds( [ $this->getLeaseId() ] )
										->setLeaseIntervalIds( [ $this->getLeaseIntervalId() ] )
										->setArTriggerIds( $arrintArTriggerIds )
										->setIsDryRun( false );

		if( ( true == $boolIsPMSoftwareEnabled && false == $objPostScheduledChargesLibrary->validate( $objPostScheduledChargesCriteria ) ) || false == $objPostScheduledChargesLibrary->postScheduledCharges( $objPostScheduledChargesCriteria ) ) {
			$this->addErrorMsgs( $objPostScheduledChargesLibrary->getErrorMsgs() );
			return false;

		} elseif( true == in_array( CArTrigger::APPLICATION_APPROVAL, $arrintArTriggerIds ) || true == in_array( CArTrigger::LEASE_COMPLETED, $arrintArTriggerIds ) ) {

			$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'AUTO_POST_APPLICATION_FEE_SCHEDULED_CHARGES', $this->getPropertyId(), $this->getCid(), $objDatabase );

			if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) || 1 != $objPropertyPreference->getValue() ) return true;

			if( true == in_array( CArTrigger::APPLICATION_COMPLETED, $arrintArTriggerIds ) ) {
				unset( $arrintArTriggerIds[array_search( CArTrigger::APPLICATION_COMPLETED, $arrintArTriggerIds )] );
			}

			$objPostScheduledChargesCriteria->setCacheableArTriggerIds( $arrintArTriggerIds );

			if( false == $boolIsPMSoftwareEnabled && ( false == \Psi\Eos\Entrata\CArTransactions::createService()->createBufferTableForScheduledChargePosting( $objDatabase ) || ( false == $objPostScheduledChargesLibrary->postCacheableScheduledCharges( $objPostScheduledChargesCriteria ) ) ) ) {
				$this->addErrorMsgs( $objPostScheduledChargesLibrary->getErrorMsgs() );
				return false;
			}
		}

		return true;
	}

	public function updateCancelledLease( $intCurrentUserId, $objDatabase ) {

		if( CLeaseIntervalType::RENEWAL == $this->getLeaseIntervalTypeId() ) return true;

		if( CApplicationStatus::CANCELLED != $this->m_intOldApplicationStatusId ) return true;

		if( true == in_array( $this->getApplicationStatusId(), [ CApplicationStatus::CANCELLED, CApplicationStatus::ON_HOLD ] ) ) return true;

		if( true == is_null( $this->getLeaseId() ) ) return true;

		$objActiveLeaseInterval = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchActiveLeaseIntervalByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase, $boolIncludeCancelledLeaseInterval = true );

		if( true == valObj( $objActiveLeaseInterval, 'CLeaseInterval' ) && CLeaseStatusType::CANCELLED == $objActiveLeaseInterval->getLeaseStatusTypeId() ) {
			$objActiveLeaseInterval->setLeaseStatusTypeId( CLeaseStatusType::APPLICANT );

			if( false == $objActiveLeaseInterval->update( $intCurrentUserId, $objDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to update active lease interval' ) ) );
				return false;
			}
		}

		// @TODO Test it thoroughly in second phase refactoring
		$objPrimaryLeaseCustomer = current( ( array ) \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchPrimaryLeaseCustomersByLeaseIdsByCid( [ $this->getLeaseId() ], $this->getCid(), $objDatabase ) );

		if( false == valObj( $objPrimaryLeaseCustomer, 'CLeaseCustomer' ) || CLeaseStatusType::CANCELLED != $objPrimaryLeaseCustomer->getLeaseStatusTypeId() ) return true;

		$objPrimaryLeaseCustomer->setLeaseStatusTypeId( CLeaseStatusType::APPLICANT );

		// While reopening the cancelled lead we don't need to update the unit status.
		if( false == $objPrimaryLeaseCustomer->update( $intCurrentUserId, $objDatabase, false, false, false, true ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to update primary lease customer' ) ) );
			return false;
		}

		return true;
	}

	public function checkLeasePartiallySigned() {

		if( CApplicationStage::LEASE == $this->getApplicationStageId()
			&& CApplicationStatus::STARTED == $this->getApplicationStatusId()
			&& false == is_null( $this->getTotalSigned() )
			&& false == is_null( $this->getTotalSigning() )
			&& $this->getTotalSigning() != $this->getTotalSigned() ) {
			return true;
		}

		return false;
	}

	public function hasApplicationStageStatus( $intApplicationStageId, $intApplicationStatusId ) {
		if( $intApplicationStageId == $this->getApplicationStageId() && $intApplicationStatusId == $this->getApplicationStatusId() ) {
			return true;
		}

		return false;
	}

	public function hasApplicationStageStatusIn( $arrintApplicationStatusIdsKeyedByStageId ) {
		if( true == multiInArray( $arrintApplicationStatusIdsKeyedByStageId, $this->getApplicationStageId(), $this->getApplicationStatusId() ) ) {
			return true;
		}

		return false;
	}

	public function hasGreaterApplicationStageStatusProgression( $intApplicationStageId, $intApplicationStatusId ) {
		if( $this->getApplicationStageId() > $intApplicationStageId || ( $this->getApplicationStageId() == $intApplicationStageId && $this->getApplicationStatusId() > $intApplicationStatusId ) ) return true;

		return false;
	}

	public function hasGreaterOrEqualApplicationStageStatusProgression( $intApplicationStageId, $intApplicationStatusId ) {
		if( $this->getApplicationStageId() > $intApplicationStageId || ( $this->getApplicationStageId() == $intApplicationStageId && $this->getApplicationStatusId() >= $intApplicationStatusId ) ) return true;

		return false;
	}

	public function fetchWaitlistApplicationFloorPlanCount( $objClientDatabase ) {
		$strSql = 'Where application_id = ' . $this->getId() . ' AND cid = ' . $this->getCid();
		return  CApplicationFloorplans::fetchApplicationFloorplanCount( $strSql, $objClientDatabase );
	}

	public function hasLessApplicationStageStatusProgression( $intApplicationStageId, $intApplicationStatusId ) {
		if( $this->getApplicationStageId() < $intApplicationStageId || ( $this->getApplicationStageId() == $intApplicationStageId && $this->getApplicationStatusId() < $intApplicationStatusId ) ) return true;
		return false;
	}

	public function hasLessOrEqualApplicationStageStatusProgression( $intApplicationStageId, $intApplicationStatusId ) {
		if( $this->getApplicationStageId() < $intApplicationStageId || ( $this->getApplicationStageId() == $intApplicationStageId && $this->getApplicationStatusId() <= $intApplicationStatusId ) ) return true;
		return false;
	}

	public function getCustomApplicationStatus( $objDatabase = NULL ) {
		$intOccupancyTypeId = $this->getOccupancyTypeId();
		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$intSubsidyRecertificationTypeId = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchSubsidyReCertificationTypeIdByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( \CSubsidyRecertificationType::DO_NOT_PERFORM_ANNUAL_CERTIFICATIONS == $intSubsidyRecertificationTypeId ) {
				$intOccupancyTypeId = COccupancyType::CONVENTIONAL;
			}
		}
		return CApplicationStageStatus::createService()->getCustomApplicationStageStatus( $this->getLeaseIntervalTypeId(), $this->getApplicationStageId(), $this->getApplicationStatusId(), $intOccupancyTypeId, $this->getId(), $this->getCid(), $objDatabase );
	}

	/** below function was added to separate logic of showing right corner status for affordable like Certification: certification status */
	public function getCustomCertificationStatus( $objDatabase = NULL ) {
		return CApplicationStageStatus::createService()->getCustomCertificationStageStatus( $this->getApplicationStageId(), $this->getApplicationStatusId(), $this->getOccupancyTypeId(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function getCustomApplicationStage() {
		return CApplicationStageStatus::createService()->getCustomApplicationStage( $this->getLeaseIntervalTypeId(), $this->getApplicationStageId(), $this->getOccupancyTypeId() );
	}

	public function isGuestCard() {
		return ( ( CLeaseIntervalType::APPLICATION == $this->getLeaseIntervalTypeId() || NULL == $this->getLeaseIntervalTypeId() ) && CApplicationStage::PRE_APPLICATION == $this->getApplicationStageId() ) ? true : false;
	}

	public function getIsQuoteGenerated( $objDatabase ) {

		if( false == is_null( $this->m_boolIsQuoteGenerated ) ) return $this->m_boolIsQuoteGenerated;

		$strSql = 'Where application_id = ' . $this->getId() . ' AND cid = ' . $this->getCid() . ' AND expires_on >= NOW()';

		$intCount = CQuotes::fetchQuoteCount( $strSql, $objDatabase );

		$this->m_boolIsQuoteGenerated = ( 0 < $intCount ) ? true : false;

		return $this->m_boolIsQuoteGenerated;
	}

	public function sendScreeningDecisionEmails( $intCurrentUserId, $arrmixScreeningDecisionDetails, $arrmixEncodedApplicantsLetters, $arrobjApplicants, $arrobjApplicantApplication, $objDatabase, $objEmailDatabase, $boolResendEmailOnly = false, $boolSendGuarantorFailEmail = false,  $boolOfferConditions = false ) {
		if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
			trigger_error( __( 'Failed to load email database' ), E_USER_ERROR );
		}

		if( false == $boolResendEmailOnly && false == valArr( $arrmixScreeningDecisionDetails ) ) {
			trigger_error( __( 'Failed to generate screening decision emails as required screening decision details not loaded' ), E_USER_ERROR );
		}

		$objScreeningApplicationRequest = $this->fetchScreeningApplicationRequest( $objDatabase );

		if( false == valObj( $objScreeningApplicationRequest, 'CScreeningApplicationRequest' ) ) {
			trigger_error( __( 'Failed to load screening application requests for application id : ' ) . $this->getId() . ' ' . __( 'And cid : ' ) . $this->getCid(), E_USER_ERROR );
		}

		$intScreeningDecisionTypeId = $arrmixScreeningDecisionDetails['screening_decision_type_id'];
		$objProperty 				= $this->getOrFetchProperty( $objDatabase );
		$arrobjPropertyPreferences 	= ( true == valObj( $objProperty, 'CProperty' ) ) ? ( array ) $objProperty->getOrFetchPropertyPreferences( $objDatabase ) : NULL;
		$objPropertyEmailRule		= CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::PROSPECT_EMAILER, $objProperty->getId(), $objDatabase );

		$arrstrPropertyEmailAddresses 				   = $objProperty->fetchApplicationNotificationEmails( $objDatabase );
		$arrobjLatestApplicantApplicationTransmissions = $this->fetchLatestApplicantApplicationTransmissionsByCustomerTypeIds( CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds, $objDatabase );
		$arrobjScreeningApplicantResults               = $this->fetchScreeningApplicantResultByApplicantIds( array_keys( $arrobjApplicants ), $objDatabase );
		$objCompanyPreferenceExcludeConditionsAALetter = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'EXCLUDE_CONDITIONS_AA_LETTER', $this->getCid(), $objDatabase );

		$boolExcludeConditionsAALetter = false;
		$boolAttachAALetter = true;
        $boolExcludeNotificationHeader = false;
        $boolExcludeNotificationFooter = false;
		if( true == valObj( $objCompanyPreferenceExcludeConditionsAALetter, 'CCompanyPreference' ) && true == $objCompanyPreferenceExcludeConditionsAALetter->getValue() ) {
			$boolExcludeConditionsAALetter = true;
            $boolExcludeNotificationHeader = true;
            $boolExcludeNotificationFooter = true;
		}

		$boolAttachAALetter = ( CScreeningDecisionType::APPROVE_WITH_CONDITIONS == $arrmixScreeningDecisionDetails['screening_decision_type_id'] && true == $boolExcludeConditionsAALetter ) ? false : true;

		$strCompanyPreferenceName = '';
		if( true == $boolSendGuarantorFailEmail ) {
			$strCompanyPreferenceName = CScreeningApplicationRequest::SCREENING_DECISION_GUARANTOR_FAILED;
		} else {
			$strCompanyPreferenceName = CScreeningApplicationRequest::$c_arrmixQuickResponseCompanyPreferences[$intScreeningDecisionTypeId];
		}

		$objCompanyPreference = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( $strCompanyPreferenceName, $this->getCid(), $objDatabase );

		$objMergeFieldLibrary = new CMergeFieldLibrary( $this->getCid(), NULL, $objDatabase );

		if( true == valObj( $objCompanyPreference, 'CCompanyPreference' ) ) {
			$objQuickResponse = CQuickResponses::createService()->fetchActiveQuickResponseByNameByCid( $objCompanyPreference->getValue(), $this->getCid(), $objDatabase );
			if( false == valObj( $objQuickResponse, 'CQuickResponse' ) ) {
				trigger_error( __( 'Failed to load quick response template for decision type id : ' ) . ( int ) $intScreeningDecisionTypeId . ' ' . __( 'for application id : ' ) . $this->getId() . ' ' . __( 'And cid : ' ) . $this->getCid(), E_USER_ERROR );
				return;
			}

			$strEmailSubject = str_replace( '*PROPERTY_NAME*', $this->getPropertyName(), $objQuickResponse->getSubject() );
		} elseif( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'USE_CUSTOMIZED_SCREENING_NOTIFICATIONS', $arrobjPropertyPreferences ) && 1 == $arrobjPropertyPreferences['USE_CUSTOMIZED_SCREENING_NOTIFICATIONS']->getValue() ) {
		    $intScreeningNotificationTypeId = CScreeningNotificationType::$c_arrintScreeningDecisionNotificationType[$intScreeningDecisionTypeId];
            $objScreeningNotification = CScreeningNotifications::fetchActiveScreeningNotificationByCidByPropertyIdByScreeningNotificationTypeId( $this->getCid(), $this->getPropertyId(), $intScreeningNotificationTypeId, $objDatabase );
            $intQuickResponseId = ( true == valObj( $objScreeningNotification, 'CScreeningNotification' ) ) ? $objScreeningNotification->getQuickResponseId() : NULL;
            $objQuickResponse = ( true == isset( $intQuickResponseId ) ) ? CQuickResponses::createService()->fetchActiveQuickResponseByIdByCid( $intQuickResponseId, $this->getCid(), $objDatabase ) : NULL;
            if( false == valObj( $objQuickResponse, 'CQuickResponse' ) ) {
                $strScreeningDecisionName = CScreeningApplicationRequest::$c_arrmixDefaultQuickResponseCompanyPreferences[$intScreeningDecisionTypeId];
                $objSmarty        = new CPsSmarty( PATH_INTERFACES_COMMON, false );
                $objQuickResponse = new CQuickResponse();
                $objQuickResponse->setMessage( $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/screening_notification_' . str_replace( ' ', '_', \Psi\CStringService::singleton()->strtolower( $strScreeningDecisionName ) ) . '.tpl' ) );
                $objQuickResponse->setSubject( '*PROPERTY_NAME* : ' . ' ' . $strScreeningDecisionName );
            }

            $strEmailSubject = str_replace( '*PROPERTY_NAME*', $this->getPropertyName(), $objQuickResponse->getSubject() );
        }

		$this->loadDocumentManager( $objDatabase, $intCurrentUserId );

		$arrstrHtmlEmailOutput = [];

		if( false == $boolResendEmailOnly && true == $boolAttachAALetter ) {
			foreach( $arrobjApplicantApplication as $objApplicantApplication ) {
				$objApplicant = getArrayElementByKey( $objApplicantApplication->getApplicantId(), $arrobjApplicants );

				if( false == array_key_exists( $objApplicantApplication->getApplicantId(), $arrmixEncodedApplicantsLetters ) ) {
					trigger_error( __( 'Failed to load letter template for applicant id : ' ) . $objApplicantApplication->getApplicantId() . ' ' . __( 'And cid : ' ) . $objApplicantApplication->getCid(), E_USER_WARNING );
				} else {

					$strLetterTemplate = getArrayElementByKey( $objApplicantApplication->getApplicantId(), $arrmixEncodedApplicantsLetters );
					$strLetterTemplate = base64_decode( $strLetterTemplate );

					$objApplicantApplication->generateDocumentFile( $strLetterTemplate, $this, $objDatabase, $objApplicantApplication->getApplicantId() );
				}
			}
		}

		$strReasons = '';
		$arrstrScreeningConditionNames = [];

		foreach( $arrmixScreeningDecisionDetails['transmission_conditions_reason'] as $intScreeningDecisionReasonId ) {
			$strReasons .= CScreeningDecisionReasonType::getScreeningDecisionReasonNameById( $intScreeningDecisionReasonId ) . ',';
		}

		$strReasons = rtrim( $strReasons, ',' );

		$arrmixScreeningConditionDetails = [];

		if( CScreeningDecisionType::APPROVE_WITH_CONDITIONS == $intScreeningDecisionTypeId ) {
			$boolFetchSatisfiedCOnditionOnly = ( true == $boolOfferConditions ) ? false : true;
			$arrmixScreeningConditionDetails = $this->getScreeningApplicationConditionDetails( $objDatabase, $boolFetchSatisfiedCOnditionOnly );
		}

		$objSmarty = new CPsSmarty( PATH_INTERFACES_PROSPECT_PORTAL, false );

		CScreeningDecisionType::assignSmartyConstants( $objSmarty );

		$objSmarty->assign( 'conditional_reasons',				( $strReasons == '' )?'':explode( ',', $strReasons ) );
		$objSmarty->assign( 'media_library_uri',				CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri',							CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );

		$objSmarty->assign( 'property_email_signature',			( true == array_key_exists( 'PROPERTY_EMAIL_SIGNATURE_CUSTOM_TEXT', $arrobjPropertyPreferences ) ) ? getArrayElementByKey( 'PROPERTY_EMAIL_SIGNATURE_CUSTOM_TEXT', $arrobjPropertyPreferences )->getValue() : NULL );

		$objSmarty->assign( 'transmission_condition_details', 	$arrmixTransmissionConditionDetails );
		$objSmarty->assign( 'screening_package_conditions',	    $arrmixScreeningConditionDetails );

		$objSmarty->assign( 'screening_decision_type_id',	    $arrmixScreeningDecisionDetails['screening_decision_type_id'] );
		$objSmarty->assign( 'image_url',	                    CONFIG_COMMON_PATH );

		$arrobjApplicantSystemEmails	= [];
		$arrobjManagerSystemEmails		= [];
		$arrobjEventLibrary             = [];

		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setPropertyId( $objProperty->getId() );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::SCREENING_EMAILS );
		$objSystemEmail->setSubject( $strEmailSubject );

		$strFromEmailAddress = ( true == array_key_exists( 'FROM_MANAGER_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() ) ) ? $arrobjPropertyPreferences['FROM_MANAGER_NOTIFICATION_EMAIL']->getValue() : CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;

		$objSystemEmail->setFromEmailAddress( $strFromEmailAddress );

		$strWebsiteUrl = $this->generateWebsiteUrl( $objDatabase );

		$boolShowConditionsInAALetter = ( false == $boolExcludeConditionsAALetter && true == CScreeningUtils::determineShowConditionSection( $this, $objDatabase ) ) ? true : false;

		foreach( $arrobjApplicants as $objApplicant ) {
			$this->setApplicantId( $objApplicant->getId() );
			$objMergeFieldLibrary->loadApplicationsMergeFields( [ $this ], $objQuickResponse->getMessage() );

			$strEmailBody = CScreeningUtils::replaceQuickResponseMedia( $objQuickResponse->getMessage(), $this, $this->getOrFetchClient( $objDatabase ), $objDatabase );
			$strEmailBody = $objMergeFieldLibrary->getProcessedContent( $this->getMergeFields(), $strEmailBody );

			$strDownloadAALetterUrl   = $strWebsiteUrl . 'Apartments/module/application_screening/action/download_aa_letter/application[id]/' . $this->getId() . '/applicant[id]/' . $objApplicant->getId() . '/client[id]/' . $objApplicant->getCid();
			$strQueryString 		  = '/customer[id]/' . $objApplicant->getCustomerId() . '/screening_request[id]/' . $objScreeningApplicationRequest->getId();
			$strScreeningConditionUrl = $strWebsiteUrl . 'Apartments/module/application_screening/action/view_screening_conditions/property[id]/' . $this->getPropertyId() . $strQueryString;

			$objSmarty->assign( 'applicant_name',	        $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() );

			$objSmarty->assign( 'email_body',               $strEmailBody );

			$objSmarty->assign( 'download_aa_letter_url',	$strDownloadAALetterUrl );
			$objSmarty->assign( 'apply_condition_page_url',	$strScreeningConditionUrl );
			$objSmarty->assign( 'show_conditions',	        $boolShowConditionsInAALetter );
            $objSmarty->assign( 'exclude_header',           $boolExcludeNotificationHeader );
            $objSmarty->assign( 'exclude_footer',           $boolExcludeNotificationFooter );

			$objSmarty->assign( 'bool_condition_offerred', ( true == valObj( $objScreeningApplicationRequest->getScreeningConditionOffer( $objDatabase ), 'CScreeningConditionOffer' ) ) ? true : false );

			$objSmarty->assign( 'is_guarantor',	            ( CCustomerType::GUARANTOR == $objApplicant->getCustomerTypeId() ) ? true : false );

			$arrstrHtmlEmailOutput[$objApplicant->getId()] = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/offered_adverse_action_letter_email.tpl' );

			$objCurrentSystemEmail = clone $objSystemEmail;

			$objCurrentSystemEmail->setToEmailAddress( $objApplicant->getUsername() );
			$objCurrentSystemEmail->setHtmlContent( $arrstrHtmlEmailOutput[$objApplicant->getId()] );
			$objCurrentSystemEmail->setApplicantId( $objApplicant->getId() );

			$arrobjApplicantSystemEmails[] = $objCurrentSystemEmail;

			if( true == array_key_exists( 'OFFER_CONDITIONS_TO_APPLICANTS', $arrobjPropertyPreferences ) && 1 == getArrayElementByKey( 'OFFER_CONDITIONS_TO_APPLICANTS', $arrobjPropertyPreferences )->getValue() ) {
				$arrobjEventLibrary[$objApplicant->getId()] = $objApplicant->createEmailOutgoingEvent( $intCurrentUserId, $strEmailSubject, $this, $objDatabase );
			}

			if( true == valArr( $arrstrPropertyEmailAddresses ) ) {
				foreach( $arrstrPropertyEmailAddresses as $strEmailAddress ) {
					if( false == valStr( $strEmailAddress ) ) continue;

					$objManagerSystemEmail = clone $objSystemEmail;

					$objManagerSystemEmail->setToEmailAddress( $strEmailAddress );
					$objManagerSystemEmail->setHtmlContent( $arrstrHtmlEmailOutput[$objApplicant->getId()] );
					$objManagerSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

					$arrobjManagerSystemEmails[] = $objManagerSystemEmail;
				}
			}
		}

		$this->logAndSendAdverseActionScreeningEmails( $intCurrentUserId, $objDatabase, $arrobjApplicantSystemEmails, $arrobjLatestApplicantApplicationTransmissions, $arrobjManagerSystemEmails, $objPropertyEmailRule, $objEmailDatabase, $boolAttachAALetter, $arrobjScreeningApplicantResults, $arrobjEventLibrary );
	}

	public function areApplicationScreeningConditionSatisfied( $objDatabase ) {
		return CScreeningApplicationConditionSets::areApplicationScreeningConditionsSatisfied( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function getScreeningApplicationConditionDetails( $objDatabase, $boolFetchSatisfiedConditionsOnly = false ) {
		$boolIncludeGuarantorCondition = false;

		if( true == $boolFetchSatisfiedConditionsOnly && false == $this->areApplicationScreeningConditionSatisfied( $objDatabase ) ) {
			$boolIncludeGuarantorCondition = true;
		}
		$arrobjScreeningApplicationConditionSets 	= CScreeningApplicationConditionSets::fetchAppliedConditionsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolFetchSatisfiedConditionsOnly, $boolIncludeGuarantorCondition );

		if( false == valArr( $arrobjScreeningApplicationConditionSets ) ) return;

		$arrmixScreeningConditionDetails = [];

		foreach( $arrobjScreeningApplicationConditionSets as $objScreeningApplicationConditionSet ) {
			if( $objScreeningApplicationConditionSet->getConditionSubsetId() == $objScreeningApplicationConditionSet->getConditionSubsetOneId() ) {
				$intRequiredItems = $objScreeningApplicationConditionSet->getRequiredItemsSubsetOne();
			} else {
				$intRequiredItems = $objScreeningApplicationConditionSet->getRequiredItemsSubsetTwo();
			}

			$arrmixScreeningConditionDetails[$objScreeningApplicationConditionSet->getId()][$objScreeningApplicationConditionSet->getConditionSubsetId()]['required_items'] = $intRequiredItems;
			$arrmixScreeningConditionDetails[$objScreeningApplicationConditionSet->getId()][$objScreeningApplicationConditionSet->getConditionSubsetId()][] = $objScreeningApplicationConditionSet->getConditionName();
			$arrmixScreeningConditionDetails[$objScreeningApplicationConditionSet->getId()]['screening_condition_operand_type_id'] = $objScreeningApplicationConditionSet->getScreeningConditionOperandTypeId();

		}

		return $arrmixScreeningConditionDetails;
	}

	public function getScreeningApplicationConditions( $objDatabase, $boolFetchSatisfiedConditionsOnly = false ) {
		$boolIncludeGuarantorCondition = false;

		if( true == $boolFetchSatisfiedConditionsOnly && false == $this->areApplicationScreeningConditionSatisfied( $objDatabase ) ) {
			$boolIncludeGuarantorCondition = true;
		}

		return CScreeningApplicationConditionSets::fetchAppliedConditionsByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolFetchSatisfiedConditionsOnly, $boolIncludeGuarantorCondition );
	}

	private function createEmailAttachment( $intCurrentUserId, $intApplicantId, $objDatabase, $objEmailDatabase ) {
		$objFileType = CFileTypes::fetchFileTypeBySystemCodeByCid( CFileType::SYSTEM_CODE_ADVERSE_ACTION_LETTER, $this->getCid(), $objDatabase );
		$arrobjFiles = CFiles::createService()->fetchFilesByApplicationIdByApplicantIdsByFileTypeIdByCid( $this->getId(), [ $intApplicantId ], $objFileType->getId(), $this->getCid(), $objDatabase );
		$objFile 	 = ( true == valArr( $arrobjFiles ) ) ? current( $arrobjFiles ) : NULL;

		if( false == valObj( $objFile, 'CFile' ) ) {
			trigger_error( __( 'Failed to load file object for applicant id : ' ) . ( int ) $intApplicantId . ' ' . __( 'application id : ' ) . $this->getId() . ' ' . __( 'And cid : ' ) . $this->getCid(), E_USER_ERROR );
		}

		$this->loadObjectStorageGateway();

		$objStoredObject                    = $objFile->fetchStoredObject( $this->m_objDatabase );
		$arrmixRequest                      = $objStoredObject->createGatewayRequest( [ 'checkExists' => true ] );
		$arrmixObjectStorageGatewayResponse = $this->getObjectStorageGateway()->getObject( $arrmixRequest );

		if( false == $arrmixObjectStorageGatewayResponse['isExists'] ) {
			trigger_error( __( 'Failed to load file for applicant id : ' ) . ( int ) $intApplicantId . ' ' . __( 'application id : ' ) . $this->getId() . ' ' . __( 'And cid : ' ) . $this->getCid(), E_USER_ERROR );
		}

		$arrmixRequest               = $objStoredObject->createGatewayRequest();
		$arrmixObjectStorageResponse = $this->getObjectStorageGateway()->getObject( $arrmixRequest );

		if( true == $arrmixObjectStorageResponse->hasErrors() ) {
			trigger_error( __( 'Failed to load file for applicant id : ' ) . ( int ) $intApplicantId . ' ' . __( 'application id : ' ) . $this->getId() . ' ' . __( 'And cid : ' ) . $this->getCid(), E_USER_ERROR );
		}

		$objPrince = CPrinceFactory::createPrince();

		if( false == valObj( $objPrince, 'Prince' ) ) {
			trigger_error( __( 'Failed to load prince library object' ), E_USER_ERROR );
		}

		$strFilePath = PATH_MOUNTS_FILES . $intApplicantId . '/' . time() . '/';
		$strTempPath = CDocumentManagerUtils::getDocumentManagementNonBackupMountsPath( $strFilePath, $this->getCid() );
		CFileIo::recursiveMakeDir( $strTempPath );
		$strLocalFile = $strTempPath . 'AdverseActionLetter.pdf';

		$objPrince->setBaseURL( PATH_COMMON );
		$objPrince->setHTML( 1 );
		$objPrince->convert_string_to_file( $arrmixObjectStorageResponse['data'], $strLocalFile );

		$objEmailAttachment = new CEmailAttachment();

		$objEmailAttachment->setFilePath( dirname( $strLocalFile ) );
		$objEmailAttachment->setFileName( basename( $strLocalFile ) );

		return $objEmailAttachment;
	}

	public function fetchScreeningApplicantResultByApplicantIds( $arrintApplicantIds, $objDatabase ) {
		return CScreeningApplicantResults::fetchScreeningApplicantResultsByApplicationIdByApplicantIdsByCid( $this->getId(), $arrintApplicantIds, $this->getCid(), $objDatabase );
	}

	public function getIsApplicationDenied() {
		return ( bool ) ( CApplicationStatus::CANCELLED == $this->getApplicationStatusId() && CListType::LEAD_DENIAL_REASONS == $this->getCancellationListTypeId() );
	}

	public function calculateAndSetUnitTypeId( $objDatabase, $intPropertyId = NULL ) {
		$intUnitTypeId = NULL;
		$arrintPropertyIds = [];

		if( true == is_null( $this->m_objProperty ) ) {
			$this->m_objProperty = $this->getOrFetchProperty( $objDatabase );
		}

		if( true == valId( $intPropertyId ) ) {
			$arrintPropertyIds = [ $intPropertyId ];
		} else {
			$arrintPropertyIds = ( true == valObj( $this->m_objProperty, 'CProperty' ) ) ? $this->m_objProperty->getParentChildPropertyIds( $objDatabase ) : [ $this->getPropertyId() ];
		}

		$intUnitTypeId = CUnitTypes::createService()->fetchMaxRentUnitTypeIdByPropertyFloorplanIdByPropertyIdsByCid( $this->getPropertyFloorplanId(), $arrintPropertyIds, $this->getCid(), $objDatabase );

		if( false === valId( $intUnitTypeId ) ) {
			$arrintPropertyIds = [ $this->getPropertyId() ];
		}

		$objUnitType    = CUnitTypes::createService()->fetchUnitTypeByPropertyFloorPlanIdByIdPropertyIdsByCidByUnitTypeId( $this->getPropertyFloorplanId(), $arrintPropertyIds, $this->getCid(), $objDatabase, $intUnitTypeId );

		if( false == valId( $intUnitTypeId ) ) {
			$intUnitTypeId  = ( true == valObj( $objUnitType, 'CUnitType' ) ) ? $objUnitType->getId() : $intUnitTypeId;
		}

		if( false == valId( $intPropertyId ) && false == valId( $this->getUnitSpaceId() ) && true == valObj( $objUnitType, 'CUnitType' ) && $this->getUnitTypeId() != $objUnitType->getId() && $this->getPropertyId() != $objUnitType->getPropertyId() ) {
			$this->setPropertyId( $objUnitType->getPropertyId() );
		}

		$this->setUnitTypeId( $intUnitTypeId );
	}

	public function getApplicationScreeningDecision( $objDatabase ) {
		$arrmixScreeningApplicationRequestData = CScreeningApplicationRequests::fetchCustomScreeningApplicationRequestDataByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrmixScreeningApplicationRequestData ) ) return NULL;

		$arrmixScreeningApplicationRequestData = current( $arrmixScreeningApplicationRequestData );
		return $arrmixScreeningApplicationRequestData['screening_decision_type_id'];
	}

	public function bindDetails( $arrmixDetails ) {
		$arrmixDetails = ( object ) $arrmixDetails;
		$this->setDetails( $arrmixDetails );
	}

	public function isApplicationScreened( $objDatabase ) {
		if( true == valObj( $this->fetchScreeningApplicationRequest( $objDatabase ), 'CScreeningApplicationRequest' ) ) {
			return true;
		} else {
			$strWhere = 'WHERE application_id = ' . $this->getId() . ' AND cid = ' . $this->getCid();
			$intApplicantApplicationTransmissionCount = CApplicantApplicationTransmissions::fetchApplicantApplicationTransmissionCount( $strWhere, $objDatabase );

			if( 0 < $intApplicantApplicationTransmissionCount ) return true;
		}

		return false;
	}

	public static function fetchScreeningApprovedApplicantApplicationIdsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
                       DISTINCT( aa.id )
	                FROM
	                    applications a
		               JOIN applicant_applications aa ON ( aa.application_id = a.id AND aa.cid = a.cid AND aa.deleted_on IS NULL )
		               JOIN screening_application_requests sar ON ( a.cid = sar.cid AND a.id = sar.application_id AND sar.screening_decision_type_id IN ( ' . implode( ',', [ CScreeningDecisionType::APPROVE, CScreeningDecisionType::APPROVE_WITH_CONDITIONS ] ) . ' ) )
		               JOIN screening_applicant_results sapr ON ( a.cid = sar.cid AND a.id = sar.application_id AND sapr.applicant_id = aa.applicant_id )
		               JOIN screening_application_condition_sets  sacs ON ( a.cid = sacs.cid AND a.id = sacs.application_id  )
		               JOIN property_preferences pp ON ( pp.cid = a.cid AND pp.property_id = a.property_id AND pp.key = \'SCREEN_AFTER_LEASE_COMPLETED\' AND pp.value IS NOT NULL )
		            WHERE
		               a.cid = ' . ( int ) $intCid . '
		               AND CASE WHEN sar.screening_decision_type_id = ' . CScreeningDecisionType::APPROVE_WITH_CONDITIONS . ' THEN
		                    sacs.satisfied_by IS NULL
		                    AND sacs.satisfied_on IS NULL
					        AND sacs.is_active = 1
					       ELSE
					        TRUE
					      END
		               AND a.id = ' . ( int ) $intApplicationId;

		$arrintApplicantApplicationData = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintApplicantApplicationData ) ) return [];
		$arrintApplicantApplicationIds = [];
		foreach( $arrintApplicantApplicationData as $arrintApplicantApplicationId ) {
			$arrintApplicantApplicationIds[$arrintApplicantApplicationId['id']] = $arrintApplicantApplicationId['id'];
		}

		return $arrintApplicantApplicationIds;
	}

	public function getQuoteEmailContent() {
		$strApplicationUrl				= '';
		$strResidentPortalUrl			= '';
		$strPropertyOfficePhoneNumber	= '';
		$intQuoteId						= NULL;
		$strUniqueKey 					= '';
		$arrmixRequiredParameters		= $this->getRequiredParameters();
		$strEmailContent 				= '';

		if( false == valArr( $arrmixRequiredParameters ) ) {
			return $strEmailContent;
		}

		if( true == array_key_exists( 'quote_unique_key', $arrmixRequiredParameters ) ) {
			$strUniqueKey = urlencode( $arrmixRequiredParameters['quote_unique_key'] );
		}

		if( true == array_key_exists( 'quote_ids', $arrmixRequiredParameters ) ) {
			$arrintQuotes = explode( ',', $arrmixRequiredParameters['quote_ids'] );
			$intQuoteId = current( $arrintQuotes );
		}

		$objPropertyPreferences = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'HIDE_QUOTE_EMAIL_LINK', $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		$objProperty = $this->fetchPropertyAndLoadDisplayInfo( $this->m_objDatabase );
		$objPropertyOfficePhoneNumber = $objProperty->getOrFetchOfficePhoneNumber( $this->m_objDatabase );
		$strPropertyOfficePhoneNumber = ( true == valObj( $objPropertyOfficePhoneNumber, 'CPropertyPhoneNumber' ) ) ? $objPropertyOfficePhoneNumber->getPhoneNumber() : '';

		$objPrimaryApplicant = $this->fetchPrimaryApplicant( $this->m_objDatabase );

		if( false == valObj( $objPrimaryApplicant, 'CApplicant' ) ) {
			return NULL;
		}

		$strEmailContent = '<div style="font:normal normal normal 12px arial;line-height: 20px;color:#808080;">' . __( 'Hi <name_first>{%s, 0}</name_first>,', [ $objPrimaryApplicant->getNameFirst() ] ) . '</div>';

		if( CLeaseIntervalType::APPLICATION === $this->getLeaseIntervalTypeId() && true == valId( $intQuoteId )
			&& ( false == valObj( $objPropertyPreferences, 'CPropertyPreference' ) || 1 != ( int ) $objPropertyPreferences->getValue() ) ) {

			$strApplicationUrl = $this->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );

			if( 'production' == CConfig::get( 'environment' ) && false !== \Psi\CStringService::singleton()->stripos( $strApplicationUrl, 'prospectportal' ) ) {
				$arrstrHttpHost = explode( '.', str_replace( 'https://', '', $strApplicationUrl ) );

				$objWebsiteDomain = \Psi\Eos\Entrata\CWebsiteDomains::createService()->fetchCustomPrimaryWebsiteDomainByHttpHostByCid( $arrstrHttpHost[0], $this->getCid(), $this->m_objDatabase );
				if( false === is_null( $objWebsiteDomain ) && true == valObj( $objWebsiteDomain, 'CWebsiteDomain' ) && true == $objWebsiteDomain->getIsSecure() ) {
					$arrstrHostDomain  = explode( '.', str_replace( 'www.', '', $objWebsiteDomain->getWebsiteDomain() ) );
					$strApplicationUrl = CONFIG_SECURE_HOST_PREFIX . ( 2 == \Psi\Libraries\UtilFunctions\count( $arrstrHostDomain ) ? 'www.' : '' ) . $objWebsiteDomain->getWebsiteDomain() . '/';
				}
			}

			$strApplicationUrl .= ( true == valStr( $strApplicationUrl ) ) ? 'Apartments/module/application_authentication/action/login_from_quote_email/property[id]/' . $this->getPropertyId() . '/applicant[key]/' . $strUniqueKey . '/applicant[id]/' . $this->getPrimaryApplicantId() . '/application[id]/' . $this->getId() . '/quote[id]/' . $intQuoteId . '/login_from_one_time_link/1/login_from_quote_email/1/is_primary_applicant/1/' : '';

			$strThankyouText = __( 'Thank you for expressing interest in our community. You will find your requested quotes attached.' );
			$strPhoneNumberText = __( 'Please contact our leasing office <phone_number>{%h, 0}</phone_number> with any questions.', [ $strPropertyOfficePhoneNumber ] );
			$strUrlText = __( 'Please click here to view your quote(s) and start an application.' );

			$strEmailContent .= '<div class="redirectUrl">' . $strThankyouText . '<br> </div>
								<div style="font:normal normal normal 12px arial;line-height: 20px;color:#808080;">' . $strPhoneNumberText . '<br></div>';

			$strEmailContent .= ( true == valStr( $strApplicationUrl ) ) ? '<div style="font:normal normal normal 12px arial;line-height: 20px;color:#808080;"><a href="' . $strApplicationUrl . '" target="_blank">' . $strUrlText . '</a></div>' : '';

		} elseif( CLeaseIntervalType::TRANSFER === $this->getLeaseIntervalTypeId() ) {
			$objWebsite 			 = CWebsites::createService()->fetchResidentPortalDefaultWebsiteByPropertyIdsByCid( [ $this->getPropertyId() ], $this->getCid(), $this->m_objDatabase );

			if( true === valObj( $objWebsite, 'CWebsite' ) ) {
				$strResidentPortalPrefix 	= ( true == valStr( CConfig::get( 'SECURE_HOST_PREFIX' ) ) ) ? CConfig::get( 'SECURE_HOST_PREFIX' ) : 'https://';
				$strResidentPortalSuffix 	= ( true == valStr( CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) ) ) ? CConfig::get( 'RESIDENT_PORTAL_SUFFIX' ) : '.residentportal.com';
				$strResidentPortalUrl		= $strResidentPortalPrefix . $objWebsite->getSubDomain() . $strResidentPortalSuffix . '/';
			}

			$strPhoneNumberText = __( 'You will find your requested quotes attached. Please contact our leasing office <phone_number>{%h, 0}</phone_number> with any questions.', [ [ $strPropertyOfficePhoneNumber ] ] );
			$strViewQuoteLinkText = __( 'Please click here to log in to Resident Portal and view your quotes.' );

			if( true == valStr( $strResidentPortalUrl ) ) {
				$strEmailContent .= '<div style="font:normal normal normal 12px arial;line-height: 20px;color:#808080;"> ' . $strPhoneNumberText . '<br></div>
								<div class="redirectUrl"><a href="' . $strResidentPortalUrl . '" target="_blank">' . $strViewQuoteLinkText . '</a></div>';
			}
		} else {
			$strEmailContent .= __( '<div style="font:normal normal normal 12px arial;line-height: 20px;color:#808080;">Please contact our leasing office <phone_number>{%h, 0}</phone_number> with any questions.<br></div>', [ $strPropertyOfficePhoneNumber ] );
		}

		return $strEmailContent;
	}

	public function getAvailableUnitInfo() {

		$strContent = '';

		$strLeaseStartDate 		= date( 'm/d/Y' );

		$objProperty = $this->fetchProperty( $this->m_objDatabase );

		$objPropertyFloorplan 	= $this->fetchPropertyFloorplan( $this->m_objDatabase );

		if( true == is_null( $objPropertyFloorplan ) ) {
			return;
		}

		$objFloorplanAvailabilityFilter	= new CFloorplanAvailabilityFilter();

		if( false == is_null( $this->getLeaseStartDate() ) ) {
			$objFloorplanAvailabilityFilter->setMoveInDate( $this->getLeaseStartDate() );
		} else {
			$objFloorplanAvailabilityFilter->setMoveInDate( $strLeaseStartDate );
		}

		$objFloorplanAvailabilityFilter->setShowOnWebsite( true );
		$arrobjPropertyPreferences	= ( array ) $objProperty->getPropertyPreferences();
		$arrobjUnitSpaces 			= ( array ) $objPropertyFloorplan->fetchAvailableUnitSpacesByFloorplanAvailabilityFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $arrobjPropertyPreferences, $this->m_objDatabase );

		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrobjUnitSpaces ) ) {
			return;
		}

		$arrobjPropertiesBuildings		= ( array ) CPropertyBuildings::createService()->fetchPropertyBuildingsByPropertyIdByCid( $objProperty->getId(), $this->getCid(), $this->m_objDatabase );
		$arrobjPropertyUnits = ( array ) $objProperty->fetchPropertyUnitsByIds( array_keys( rekeyObjects( 'PropertyUnitId', $arrobjUnitSpaces ) ), $this->m_objDatabase );

		$boolShowDisplayDeposit = false;
		$boolShowRentAmount     = false;
		$boolShowSqFeet         = false;
		$boolIsBuildingsExist   = false;

		foreach( $arrobjUnitSpaces as $intKey => $objUnitSpace ) {

			// Fetch and Set Building Id to display Building in Application Step
			$objPropertyUnit = getArrayElementByKey( $objUnitSpace->getPropertyUnitId(), $arrobjPropertyUnits );

			if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {
				// if available unit is of child property then we need to load the buildings of child property unit
				$objPropertyBuilding = getArrayElementByKey( $objPropertyUnit->getPropertyBuildingId(), $arrobjPropertiesBuildings );

				if( true == valObj( $objPropertyBuilding, 'CPropertyBuilding' ) ) {
					$objProperty->addPropertyBuilding( $objPropertyBuilding );
				}

				$objUnitSpace->setPropertyBuildingId( $objPropertyUnit->getPropertyBuildingId() );
				$objUnitSpace->setPropertyUnit( $objPropertyUnit );
			}

			$arrobjUnitSpaces[$intKey] = $objUnitSpace;

			if( 0 != $objUnitSpace->getMinDeposit() ) {
				$boolShowDisplayDeposit = true;
			} else if( 0 != $objPropertyFloorplan->getMinDeposit() ) {
				$boolShowDisplayDeposit = true;
			}

			if( 0 != $objUnitSpace->getMinRent() ) {
				$boolShowRentAmount = true;
			}

			if( 0 != $objPropertyUnit->getSquareFeet() || 0 != $objPropertyFloorplan->getMinSquareFeet() ) {
				$boolShowSqFeet = true;
			}

			if( false == is_null( $objUnitSpace->getPropertyBuildingId() ) ) {
				$boolIsBuildingsExist = true;
			}
		}

		$strContent .= '<div style="margin-top:15px; margin-bottom:5px; font:normal normal 12px/15px arial;font-weight:bold; border-bottom:1px dashed #000000;color:#000000;width:580px; text-align:left;">' . __( 'Available Units For Floor Plan \'{%s, 0}\'', [ $objPropertyFloorplan->getFloorplanName() ] ) . '</div>';
		$strContent .= '<table id="unitSpaces" cellpadding="0" cellspacing="0" border="0" style="width:580px;" class="floorplans">';
		$strContent .= '<tr class="description"><td style="width:50px;padding-left:5px;font:normal normal normal 11px/18px arial;font-weight:bold;color:#808080;">Apt#</td>';
		if( true == $boolIsBuildingsExist ) {
			$strContent .= '<td style="text-align:left;font:normal normal normal 11px/18px arial; color:#808080;font-weight:bold;">' . __( 'Building' ) . '</td>';
		}
		if( true == $boolShowRentAmount ) {
			$strContent .= '<td style="text-align:left;font:normal normal normal 11px/18px arial; color:#808080;font-weight:bold;">' . __( 'Price' ) . '</td>';
		}
		if( true == $boolShowDisplayDeposit ) {
			$strContent .= '<td style="text-align:left;font:normal normal normal 11px/18px arial; color:#808080;font-weight:bold;">' . __( 'Deposit' ) . '</td>';
		}
		if( true == $boolShowSqFeet ) {
			$strContent .= '<td style="text-align:left;font:normal normal normal 11px/18px arial; color:#808080;font-weight:bold;">' . __( 'Sq Ft' ) . '</td>';
		}

		foreach( $arrobjUnitSpaces as $objUnitSpace ) {
			$objPropertyUnit = $objUnitSpace->getPropertyUnit();
			$strContent .= '<tr><td style="text-align:left; padding-left:5px;font:normal normal normal 11px/18px arial; color:#808080;">' . $objUnitSpace->getUnitNumber() . '</td>';
			if( true == $boolIsBuildingsExist ) {
				$strBuildingName = '';
				if( false == is_null( $objUnitSpace->getPropertyBuildingId() ) ) {
					$intPropertyBuildingId = $objUnitSpace->getPropertyBuildingId();
					if( true == isset( $arrobjPropertiesBuildings[$intPropertyBuildingId] ) ) {
						$strBuildingName = $arrobjPropertiesBuildings[$intPropertyBuildingId]->getBuildingName();
					}
				}
				$strContent .= '<td style="text-align:left;font:normal normal normal 11px/18px arial; color:#808080;">' . $strBuildingName . '</td>';
			}

			$intMinRentAmount = $objUnitSpace->getMinRent();
			$intMaxRentAmount = $objUnitSpace->getMaxRent();
			$intMinDepositAmount = $objUnitSpace->getMinDeposit();
			$intMinFloorplanDepositAmount = $objPropertyFloorplan->getMinDeposit();
			$intMaxFloorplanDepositAmount = $objPropertyFloorplan->getMaxDeposit();
			if( true == $boolShowRentAmount ) {
				$strContent .= '<td style="text-align:left;font:normal normal normal 11px/18px arial; color:#808080;">';
				if( 0 < $intMinRentAmount || 0 < $intMaxRentAmount ) {
					if( $intMinRentAmount != $intMaxRentAmount ) {
						if( true == isset( $intMinRentAmount ) && 0 < $intMinRentAmount ) {
							$strContent .= __( '{%f, 0, p:2}', [ $intMinRentAmount ] );
						}
						if( true == isset( $intMaxRentAmount ) && 0 < $intMaxRentAmount ) {
							$strContent .= __( '-{%f, 0, p:2}', [ $intMaxRentAmount ] );
						}
					} else {
						$strContent .= __( '{%f, 0, p:2}', [ $intMinRentAmount ] );
					}
				} else {
					$strContent .= __( 'Call for Pricing' );
				}
				$strContent .= '</td>';
			}

			if( true == $boolShowDisplayDeposit ) {
				$strContent .= '<td style="text-align:left;font:normal normal normal 11px/18px arial; color:#808080;">';
				if( true == isset( $intMinDepositAmount ) && 0 < $intMinDepositAmount ) {
					$strContent .= __( '{%f, 0, p:2}', [ $intMinDepositAmount ] );
				} else {
					if( ( false == is_null( $intMinFloorplanDepositAmount ) && 0 < $intMinFloorplanDepositAmount ) || ( false == is_null( $intMaxFloorplanDepositAmount ) && 0 < $intMaxFloorplanDepositAmount ) ) {
						$strContent .= number_format( $intMinFloorplanDepositAmount, 2, '.', '' );
						if( 0 < $intMaxFloorplanDepositAmount ) {
							$strContent .= __( '-{%f, 0, p:2}', [ $intMaxFloorplanDepositAmount ] );
						}
					} else {
						$strContent .= '-';
					}
				}
				$strContent .= '</td>';
			}

			if( true == $boolShowSqFeet ) {
				$strContent .= '<td style="text-align:left;font:normal normal normal 11px/18px arial; color:#808080;">';
				if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {
					if( false == is_null( $objPropertyUnit->getSquareFeet() ) ) {
						$strContent .= $objPropertyUnit->getSquareFeet();
					} else {
						if( false == is_null( $objPropertyFloorplan->getMinSquareFeet() ) ) {
							$strContent .= $objPropertyFloorplan->getMinSquareFeet();
						} else {
							$strContent .= '-';
						}
					}
				} else {
					$strContent .= '-';
				}
			}
		}

		$strContent .= '</table>';

		return $strContent;
	}

	public function getUnsubscribeAvailabilityAlertReminderUrl() {

		$objProperty = $this->fetchProperty( $this->m_objDatabase );

		$objWebsite = $objProperty->fetchSingularWebsite( $this->m_objDatabase, $boolIsTemplateTypeRequired = true );

		if( false == valObj( $objWebsite, 'CWebsite' ) ) {
			$objWebsite = $objProperty->fetchEnabledDefaultWebsite( $this->m_objDatabase, $boolIsTemplateTypeRequired = true );
		}

		if( false == valObj( $objWebsite, 'CWebsite' ) ) {
			return;
		}

		$strUnsubscribeAvailabilityAlertReminderEmailUrl = '';

		$objAvailabilityAlertRequest = \Psi\Eos\Entrata\CAvailabilityAlertRequests::createService()->fetchAvailabilityAlertRequestByApplicationIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objAvailabilityAlertRequest, 'CAvailabilityAlertRequest' ) ) {
			if( false == is_null( $objAvailabilityAlertRequest->getId() ) && false == is_null( $objAvailabilityAlertRequest->getEmailAddress() ) && false == is_null( $this->getPropertyFloorplanId() ) ) {
				$strAvailabilityAlertReminderEmailAndId          = $objAvailabilityAlertRequest->getId() . '~' . $objAvailabilityAlertRequest->getEmailAddress() . '~' . $this->getPropertyFloorplanId();
				$strEncryptedAvailabilityAlertReminderEmailAndId = \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strAvailabilityAlertReminderEmailAndId, CONFIG_SODIUM_KEY_EMAIL_ADDRESS, [ 'legacy_secret_key' => CONFIG_KEY_EMAIL_ADDRESS ] );

				$strUnsubscribeAvailabilityAlertReminderEmailUrl = 'https://' . $objWebsite->getSubDomain() . '.prospectportal.com/Apartments/module/guest_card/action/unsubscribe_availability_alert_email/property[id]/' . $objProperty->getId() . '/unsubscribe[key]/' . str_replace( '/', '~', $strEncryptedAvailabilityAlertReminderEmailAndId );
			}
		}

		return $strUnsubscribeAvailabilityAlertReminderEmailUrl;
	}

	public function getApplicationCompletionUrl() {

		return $this->generateWebsiteUrl( $this->m_objDatabase ) . 'Apartments/module/application_authentication/action/view_login/property[id]/' . $this->getPropertyId();
	}

	/**
	 * @description: Validate if all required applicant signed the parent consent form or not
	 * - if found any record return true;
	 * @param $objDatabase
	 * @return bool
	 */
	public function checkHasAllApplicantsParentConsentFormSigned( $objDatabase ) {

		$boolAllApplicantsParentConsentFormSigned = false;
		/**
		 * below block will be used to display screening block or not
		 */
		// find out the applicants which having details about parent consent form and email address
		$arrobjApplicants = CApplicants::fetchApplicantsByCustomerTypeIdsByApplicationIdByCid( CCustomerType::$c_arrintParentConsentRequiredCustomerTypeIds, $this->getId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrobjApplicants ) ) {
			return $boolAllApplicantsParentConsentFormSigned;
		}

		$intRequiredApplicantAge	= CScreeningUtils::getPCFRequiredApplicantAge( $this, $objDatabase );
		$arrintApplicantIds			= [];

		foreach( $arrobjApplicants as $objApplicant ) {
			if( true == $objApplicant->getIsMinor( $intRequiredApplicantAge ) ) {
				$arrintApplicantIds[] = $objApplicant->getId();
			}
		}

		if( false == valArr( $arrintApplicantIds ) ) {
			return $boolAllApplicantsParentConsentFormSigned;
		}

		// Check if file generated and signed  - by application id by file type id
		$intSignedFileCount = CFileAssociations::createService()->fetchSignedFileAssociationsCountByApplicationIdApplicantIdsBySystemCodeByCid( $this->getId(), $arrintApplicantIds, CFileType::SYSTEM_CODE_PARENTAL_CONSENT_FORM, $this->getCid(), $objDatabase );

		if( \Psi\Libraries\UtilFunctions\count( $arrintApplicantIds ) != $intSignedFileCount ) {
			$boolAllApplicantsParentConsentFormSigned = true;
		}

		return $boolAllApplicantsParentConsentFormSigned;
	}

	/**
	 * @Description : Validate the age of each Active applicants
	 *              Used in checkParentalConsentDetailsRequired
	 *              Used in process screening
	 * @param $objDatabase
	 * @return bool
	 */
	public function validateParentConsentRequiredApplicantsAge( $objDatabase ) {
		$boolIsValidAge = false;
		$intRequiredApplicantAge = CScreeningUtils::getPCFRequiredApplicantAge( $this, $objDatabase );

		$arrobjApplicants = CApplicants::fetchApplicantsByCustomerTypeIdsByApplicationIdByCid( CCustomerType::$c_arrintParentConsentRequiredCustomerTypeIds, $this->getId(), $this->getCid(), $objDatabase );

		if( false == valArr( $arrobjApplicants ) ) {
			return false;
		}

		foreach( $arrobjApplicants as $objApplicant ) {
			if( true == $objApplicant->getIsMinor( $intRequiredApplicantAge ) ) {
				$boolIsValidAge = true;
			}
		}

		return $boolIsValidAge;
	}

	public function validateParentConsentRequiredApplicantAge( $objApplicant, $objDatabase ) {
		$boolIsValidAge = false;
		$intRequiredApplicantAge = CScreeningUtils::getPCFRequiredApplicantAge( $this, $objDatabase );

		if( true == valObj( $objApplicant, 'CApplicant' ) && true == $objApplicant->getIsMinor( $intRequiredApplicantAge ) ) {
			$boolIsValidAge = true;
		}

		return $boolIsValidAge;
	}

	public function getConventionalWaitlistEmailSubject() {
		$arrmixParameters = $this->getRequiredParameters();

		$boolIsAddOn = false;
		$strAddOnName = '';
		$strUnitNumberDetails = '';
		$intUnitSpaceId = NULL;

		if( true == valArr( $arrmixParameters ) ) {
			if( true == array_key_exists( 'add_on_name', $arrmixParameters ) && true == valStr( $arrmixParameters['add_on_name'] ) ) {
				$boolIsAddOn = true;
				$strAddOnName = $arrmixParameters['add_on_name'];
			}

			if( true == array_key_exists( 'unit_space_id', $arrmixParameters ) ) {
				$intUnitSpaceId = $arrmixParameters['unit_space_id'];
			}
		}

		if( false == is_null( $intUnitSpaceId ) ) {
			$objUnitSpace = CUnitSpaces::createService()->fetchUnitSpaceByIdByCid( $intUnitSpaceId, $this->getCid(), $this->m_objDatabase );

			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				$objProperty = $this->getOrFetchProperty( $this->m_objDatabase );
				if( true == valObj( $objProperty, CProperty::class ) && true == $objProperty->getHasOccupancyType( COccupancyType::MILITARY ) ) {
					$strUnitNumberDetails = $objUnitSpace->getUnitNumber();
				} else {
					$strUnitNumberDetails = $objUnitSpace->getUnitNumberCache();
				}
			}
		}

		if( true == $boolIsAddOn ) {
			$strConventionalWaitlistEmailSubject = 'Add On ' . $strAddOnName . ' available at ' . $this->getPropertyName( $this->m_objDatabase );
		} else {
			$strConventionalWaitlistEmailSubject = 'Unit ' . $strUnitNumberDetails . ' available at ' . $this->getPropertyName( $this->m_objDatabase );
		}

		return $strConventionalWaitlistEmailSubject;
	}

	public function getConventionalWaitlistEmailContent() {
		$strEmailContent = '';
		$arrmixParameters = $this->getRequiredParameters();

		if( false == valArr( $arrmixParameters ) ) {
			return $strEmailContent;
		}

		$boolIsAddOn = false;
		$strApplicantFirstName = '';
		$strApplicantLastName = '';
		$strApplicantMaternalLastName = '';
		$strUnitNumberDetails = '';
		$strFloorPlanName = '';
		$intUnitSpaceId = NULL;
		$strAddOnName = '';
		$strPropertyOfficePhoneNumber = '';

		if( true == array_key_exists( 'add_on_name', $arrmixParameters ) && true == valStr( $arrmixParameters['add_on_name'] ) ) {
			$boolIsAddOn = true;
			$strAddOnName = $arrmixParameters['add_on_name'];
		}

		if( true == array_key_exists( 'unit_space_id', $arrmixParameters ) ) {
			$intUnitSpaceId = $arrmixParameters['unit_space_id'];
		}

		$objProperty = $this->fetchProperty( $this->m_objDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			return NULL;
		}

		$strWebsiteUrl = $this->generateWebsiteUrl( $this->m_objDatabase );

		$objPropertyOfficePhoneNumber = $objProperty->fetchOfficePhoneNumber( $this->m_objDatabase );

		if( true == valObj( $objPropertyOfficePhoneNumber, 'CPropertyPhoneNumber' ) ) {
			$strPropertyOfficePhoneNumber = valStr( $objPropertyOfficePhoneNumber->getPhoneNumber() ) ? $objPropertyOfficePhoneNumber->getPhoneNumber() : '';
		}

		$strPropertyName = $objProperty->getPropertyName();

		$arrobjApplicantApplications   = ( array ) $this->getOrFetchApplicantApplications( $this->m_objDatabase );

		if( 0 == \Psi\Libraries\UtilFunctions\count( $arrobjApplicantApplications ) ) {
			return NULL;
		}

		$intPrimaryApplicantApplicationId = NULL;

		foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
			if( CCustomerType::PRIMARY == $objApplicantApplication->getCustomerTypeId() ) {
				$intPrimaryApplicantApplicationId = $objApplicantApplication->getId();
				break;
			}
		}
		$objApplicant = CApplicants::fetchApplicantByApplicantApplicationIdByCid( $intPrimaryApplicantApplicationId, $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objApplicant, 'CApplicant' ) ) {
			$strApplicantFirstName = $objApplicant->getNameFirst();
			$strApplicantLastName = $objApplicant->getNameLast();
			$strApplicantMaternalLastName = $objApplicant->getNameLastMatronymic();
		}

		if( false == is_null( $intUnitSpaceId ) ) {
			$objUnitSpace = CUnitSpaces::createService()->fetchUnitSpaceByIdByCid( $intUnitSpaceId, $this->getCid(), $this->m_objDatabase );

			if( true == valObj( $objUnitSpace, 'CUnitSpace' ) ) {
				if( true == $objProperty->getHasOccupancyType( COccupancyType::MILITARY ) ) {
					$strUnitNumberDetails = $objUnitSpace->getUnitNumber();
				} else {
					$strUnitNumberDetails = $objUnitSpace->getUnitNumberCache();
				}

				$objPropertyFloorPlan = $objUnitSpace->fetchPublishedPropertyFloorplan( $this->m_objDatabase );
				if( true == valObj( $objPropertyFloorPlan, 'CPropertyFloorplan' ) ) {
					$strFloorPlanName = $objPropertyFloorPlan->getFloorplanName();
				}
			}
		}

		if( CLeaseIntervalType::TRANSFER === ( int ) $this->getLeaseIntervalTypeId() ) {
			if( true == $boolIsAddOn ) {
				$strSubContent = __( 'You previously completed an application at {%s, 0}. Add On {%s, 1}  is now available and matches the preferences you listed.', [ $strPropertyName, $strAddOnName ] );
			} else {
				$strSubContent = __( 'You previously expressed interest in transferring units at {%s, 0}. Unit {%s, 1} is now available with Floor Plan {%s, 2} and matches the preferences you listed in your application.', [ $strPropertyName, $strUnitNumberDetails, $strFloorPlanName ] );
			}
		} else {
			$strSubContent = __( 'You previously completed an application at {%s, 0}.', [ $strPropertyName ] );
			if( true == valStr( $strWebsiteUrl ) ) {
				$strWebsiteUrl   = $strWebsiteUrl . 'Apartments/module/application_authentication/returning_applicant/1/property[id]/' . $objProperty->getId() . '/';
			}

			if( true == $boolIsAddOn ) {
				$strSubContent .= __( ' Add On {%s, 0} is now available and matches the preferences you listed in your application.', [ $strAddOnName ] );
			} else {
				$strSubContent .= __( ' Unit {%s, 0} is now available with Floor Plan {%s, 1}  and matches the preferences you listed in your application.', [ $strUnitNumberDetails, $strFloorPlanName ] );
			}
		}

		$strEmailContent = __( '<span class="property_logo_text" style="font-size: 12px">
                Dear {%s, 0} {%s, 1} {%s, 2},
                <br><br> {%s, 3} <br><br>
                Please&nbsp;<a href="{%s, 4}" target="_blank"><u>click here</u></a> to log in for more details.<br><br>
                Sincerely,<br>{%s, 5}<br>{%s, 6}<br>
              </span>', [ $strApplicantFirstName, $strApplicantLastName, $strApplicantMaternalLastName, $strSubContent, $strWebsiteUrl, $strPropertyName, $strPropertyOfficePhoneNumber ] );

		return $strEmailContent;
	}

	public function getWebsiteUrlForWaitlist() {
		$strWebsiteUrl = $this->generateWebsiteUrl( $this->m_objDatabase );

		if( false == valStr( $strWebsiteUrl ) ) {
			return NULL;
		}

		$objProperty = $this->fetchProperty( $this->m_objDatabase );

		if( CLeaseIntervalType::TRANSFER != $this->getLeaseIntervalTypeId() && true == valObj( $objProperty, 'CProperty' ) ) {
			$strWebsiteUrl = $strWebsiteUrl . 'Apartments/module/application_authentication/returning_applicant/1/property[id]/' . $objProperty->getId() . '/';
		}

		return $strWebsiteUrl;
	}

	public function getRenewalOfferSelectedContent() {
		$strTitle = __( 'Renewal Offer Selected.' );
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( false == valArr( $arrmixRequiredParameters ) ) {
			return '';
		}

		$strEmailContent = '<table cellpadding="0" cellspacing="0" border="0" style="width:570px;"><tr>
					<td style="font:normal normal bold 15px/18px arial; color:#000000; padding-top:30px; padding-bottom:10px;" align="left">
						<strong>' . $strTitle . '</strong>
					</td></tr>';

		$objLeaseTerm    = NULL;

		if( true == array_key_exists( 'lease_term_id', $arrmixRequiredParameters ) && true == valId( $arrmixRequiredParameters['lease_term_id'] ) ) {
			$objLeaseTerm = \Psi\Eos\Entrata\CLeaseTerms::createService()->fetchLeaseTermByIdByCid( $arrmixRequiredParameters['lease_term_id'], $this->getCid(), $this->m_objDatabase );
		}

		$arrobjScheduledCharges = [];
		if( true == array_key_exists( 'scheduled_charges_ids', $arrmixRequiredParameters ) && true == valStr( $arrmixRequiredParameters['scheduled_charges_ids'] ) ) {
			$arrintScheduledChargesIds = explode( ',',  $arrmixRequiredParameters['scheduled_charges_ids'] );
			$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByIdsByPropertyIdByCid( $arrintScheduledChargesIds, $this->getPropertyId(), $this->getCid(), $this->m_objDatabase, false, true );
		}

		$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'RENEWAL_OFFER_ACCEPTED_NOTIFICATION_TO_APPLICANT', $this->getPropertyId(), $this->getCid(), $this->m_objDatabase, false, true );
		$intTotalCharges = 0;
		$strDesiredLeaseTermName  = '';

		if( true == valObj( $objLeaseTerm, 'CLeaseTerm' ) ) {
			$strDesiredLeaseTermName = $objLeaseTerm->getName();
		}

		if( true == array_key_exists( 'lease_start_window_id', $arrmixRequiredParameters ) && true == valId( $arrmixRequiredParameters['lease_start_window_id'] ) ) {
			$objLeaseStartWindow = CLeaseStartWindows::fetchLeaseStartWindowByIdByCid( $arrmixRequiredParameters['lease_start_window_id'], $this->getCid(), $this->m_objDatabase );
			if( true == valObj( $objLeaseStartWindow, 'CLeaseStartWindow' ) ) {
				if( true === CValidation::validateDate( $objLeaseStartWindow->getRenewalStartDate() ) ) {
					$strDesiredLeaseTermName = $objLeaseTerm->getName() . ' ( ' . __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $objLeaseStartWindow->getRenewalStartDate() ] ) . ' - ' . __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $objLeaseStartWindow->getEndDate() ] ) . ' )';
				} else {
					$strDesiredLeaseTermName = $objLeaseTerm->getName() . ' ( ' . __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $objLeaseStartWindow->getStartDate() ] ) . ' - ' . __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $objLeaseStartWindow->getEndDate() ] ) . ' )';
				}
			}
		}

		foreach( $arrobjScheduledCharges as $objScheduleCharge ) {
			if( CArTrigger::MONTHLY == $objScheduleCharge->getArTriggerId() ) {
				$intTotalCharges += $objScheduleCharge->getChargeAmount();
			}
		}

		$strTotalChargesAmount = __( '{%m, 0, p:2; notes}', [ $intTotalCharges ] );

		$strEmailContent .= '<tr><td  style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left" class="break-word">' . __( 'Thank you for selecting your renewal offer. For your records, the term you selected was {%s, 0} and the total rent amount including all other charges is {%s, 1}', [ $strDesiredLeaseTermName, $strTotalChargesAmount ] ) . '</td></tr>';

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == valStr( $objPropertyPreference->getValue() ) ) {
			$strEmailContent .= '<tr><td>&nbsp;</td>
				</tr><tr> <td style="font: normal 11px/18px Arial, Helvetica, sans-serif; color: #000000;" align="left" class="break-word">' . $objPropertyPreference->getValue() . '</td></tr>';
		}

		$strFooterText = __( 'Please retain for your records.' );

		$strEmailContent .= '<tr><td align="left" style="padding-top:20px;">
				<div Style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" style="margin-top:20px;" align="left">
					<strong>' . $strFooterText . '<br/></strong>
				</div></td></tr></table>';

		return $strEmailContent;
	}

	public function getRenewalOfferAcceptedContentForProperty() {
		$strTitle = __( 'Renewal Offer Accepted.' );
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( false == valArr( $arrmixRequiredParameters ) ) {
			return '';
		}

		$strEmailContent = '<table cellpadding="0" cellspacing="0" border="0" style="width:570px;"><tr>
						<td style="font:normal normal bold 15px/18px arial; color:#000000; padding-top:30px; padding-bottom:10px;" align="left">
						<strong>' . $strTitle . '</strong></td></tr>';

		$objProperty         = $this->getOrFetchProperty( $this->m_objDatabase );
		$objPrimaryApplicant = CApplicants::fetchApplicantByIdByCid( $this->getPrimaryApplicantId(), $this->getCid(), $this->m_objDatabase );
		if( false == valObj( $objProperty, 'CProperty' ) || false == valObj( $objPrimaryApplicant, 'CApplicant' ) ) {
			return NULL;
		}

		$objLeaseTerm     = NULL;
		if( true == array_key_exists( 'lease_term_id', $arrmixRequiredParameters ) && true == valId( $arrmixRequiredParameters['lease_term_id'] ) ) {
			$objLeaseTerm = \Psi\Eos\Entrata\CLeaseTerms::createService()->fetchLeaseTermByIdByCid( $arrmixRequiredParameters['lease_term_id'], $this->getCid(), $this->m_objDatabase );
		}

		$arrobjScheduledCharges = [];
		if( true == array_key_exists( 'scheduled_charges_ids', $arrmixRequiredParameters ) && true == valStr( $arrmixRequiredParameters['scheduled_charges_ids'] ) ) {
			$arrintScheduledChargesIds = explode( ',',  $arrmixRequiredParameters['scheduled_charges_ids'] );
			$arrobjScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByIdsByPropertyIdByCid( $arrintScheduledChargesIds, $this->getPropertyId(), $this->getCid(), $this->m_objDatabase, false, true );
		}

		$objPropertyPreference                 = $objProperty->fetchPropertyPreferenceByKey( 'RENEWAL_OFFER_ACCEPTED_NOTIFICATION_TO_APPLICANT', $this->m_objDatabase );
		$intTotalCharges                       = 0;
		$strDesiredLeaseTermName               = '';
		$strUnitNumber                         = 0;
		$objPropertyUnit                       = CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $this->m_objDatabase );
		$strRenewalOfferAcceptingApplicantName = $objPrimaryApplicant->getNameFirst() . ' ' . $objPrimaryApplicant->getNameLast() . ' ' . $objPrimaryApplicant->getNameLastMatronymic();

		if( true == valObj( $objLeaseTerm, 'CLeaseTerm' ) ) {
			$strDesiredLeaseTermName = $objLeaseTerm->getName();
		}
		if( true == valObj( $objPropertyUnit, 'CPropertyUnit' ) ) {
			$strUnitNumber = $objPropertyUnit->getUnitNumber();
		}

		if( true == array_key_exists( 'lease_start_window_id', $arrmixRequiredParameters ) && true == valId( $arrmixRequiredParameters['lease_start_window_id'] ) ) {
			$objLeaseStartWindow = CLeaseStartWindows::fetchLeaseStartWindowByIdByCid( $arrmixRequiredParameters['lease_start_window_id'], $this->getCid(), $this->m_objDatabase );
			if( true == valObj( $objLeaseStartWindow, 'CLeaseStartWindow' ) ) {
				if( true === CValidation::validateDate( $objLeaseStartWindow->getRenewalStartDate() ) ) {
					$strDesiredLeaseTermName = $objLeaseTerm->getName() . ' ( ' . __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $objLeaseStartWindow->getRenewalStartDate() ] ) . ' - ' . __( '{%t,0,DATE_NUMERIC_STANDARD}', [ $objLeaseStartWindow->getEndDate() ] ) . ' )';
				} else {
					$strDesiredLeaseTermName = $objLeaseTerm->getName() . ' ( ' . __( '{%t ,0 ,DATE_NUMERIC_STANDARD}', [ $objLeaseStartWindow->getStartDate() ] ) . ' - ' . __( '{%t ,0 ,DATE_NUMERIC_STANDARD}', [ $objLeaseStartWindow->getEndDate() ] ) . ' )';
				}
			}
		}

		foreach( $arrobjScheduledCharges as $objScheduleCharge ) {
			if( CArTrigger::MONTHLY == $objScheduleCharge->getArTriggerId() ) {
				$intTotalCharges += $objScheduleCharge->getChargeAmount();
			}
		}

		$strTotalChargesAmount = __( '{%m, 0, p:2; nots}', [ $intTotalCharges ] );

		$strEmailContent .= __( '<tr><td  style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" align="left" class="break-word">
								A resident from {%s, 0} has selected a renewal offer. The resident is {%s, 1} from apartment {%s, 2}. The term of the offer is {%s, 3} and the total rent amount including all other charges is {%s, 4}. </td></tr>', [ $objProperty->getPropertyName(), $strRenewalOfferAcceptingApplicantName, $strUnitNumber, $strDesiredLeaseTermName, $strTotalChargesAmount ] );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == valStr( $objPropertyPreference->getValue() ) ) {
			$strEmailContent .= '<tr><td>&nbsp;</td></tr>
								<tr> <td style="font: normal 11px/18px Arial, Helvetica, sans-serif; color: #000000;" align="left" class="break-word">' . $objPropertyPreference->getValue() . '</td></tr>';
		}

		$strFooterText = __( 'Please retain for your records.' );

		$strEmailContent .= '<tr><td align="left" style="padding-top:20px;">
					<div Style="font: normal 11px/18px Arial, Helvetica, sans-serif;color:#000000;" style="margin-top:20px;" align="left">
						<strong>' . $strFooterText . '<br/></strong>
					</div></td></tr></table>';

		return $strEmailContent;
	}

	public function getLeaseSignedApplicantDetails() {
		$intApplicationId   = ( true == valId( $this->getLeaseId() ) ) ? $this->getLeaseId() : $this->getId();
		$strApplicants      = '';
		$strGaurantors      = '';
		$strDocuments       = '';
		$arrintCustomerIds  = [];
		$arrintApplicantIds = [];
		$arrmixRequiredParameters = $this->getRequiredParameters();

		$strTdCss = '<td style="font:normal normal 13px/18px arial; color:#000000; padding-top:15px; padding-bottom:10px;" align="left">';

		$strApplicationIdCaption = __( 'Application ID:' );

		$strEmailContent = '<table cellpadding="0" cellspacing="0" border="0" style="width:570px;"><tr> ' . $strTdCss . '
						<strong> ' . $strApplicationIdCaption . ( int ) $intApplicationId . '</strong><br/> </td></tr>';

		if( true == valArr( $arrmixRequiredParameters ) && true == array_key_exists( 'file_id', $arrmixRequiredParameters ) && true == valId( $arrmixRequiredParameters['file_id'] ) ) {
			$intFileId = $arrmixRequiredParameters['file_id'];
			$arrobjFileAssociations = ( array ) CFileAssociations::createService()->fetchFileAssociationsByFileIdByCid( $intFileId, $this->getCid(), $this->m_objDatabase );
			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjFileAssociations ) ) {
 				$arrintDocumentIds  = ( array ) extractUniqueFieldValuesFromObjects( 'getDocumentId', $arrobjFileAssociations );
				$arrintCustomerIds  = ( array ) extractUniqueFieldValuesFromObjects( 'getCustomerId', $arrobjFileAssociations );
				$arrintApplicantIds = ( array ) extractUniqueFieldValuesFromObjects( 'getApplicantId', $arrobjFileAssociations );
				$arrobjDocuments    = ( array ) CDocuments::fetchCustomLeaseDocumentsByIdsByCid( $arrintDocumentIds, $this->getCid(), $this->m_objDatabase );

				if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjDocuments ) ) {
					foreach( $arrobjDocuments as $objDocument ) {
						if( 1 == $objDocument->getIsForPrimaryApplicant() || 1 == $objDocument->getIsForCoApplicant() || 1 == $objDocument->getIsForCoSigner() ) {
							$strDocuments .= '<li>' . $objDocument->getName() . ' ' . $objDocument->getLeaseDocumentAppliedToLable() . '</li>';
						}
					}
				}
			}
		}

		if( true == valId( $this->getLeaseId() ) && true == valArr( $arrintCustomerIds ) ) {
			$arrobjCustomers   = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerInformationByIdsByCid( $arrintCustomerIds, $this->getCid(), $this->m_objDatabase );

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjCustomers ) ) {
				foreach( $arrobjCustomers as $objCustomer ) {
					if( CCustomerType::GUARANTOR != $objCustomer->getCustomerTypeId() ) {
						$strApplicants .= '<li>' . $objCustomer->getNameFirst() . ' ' . $objCustomer->getNameLast() . '</li>';
					} else {
						$strGaurantors .= '<li>' . $objCustomer->getNameFirst() . ' ' . $objCustomer->getNameLast() . '</li>';
					}
				}
			}
		} else {

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintApplicantIds ) ) {
				$arrobjApplicants = ( array ) CApplicants::fetchApplicantsByIdsByApplicationIdByCid( $arrintApplicantIds, $this->getId(), $this->getCid(),  $this->m_objDatabase );
			} else {
				$arrobjApplicants = ( array ) \Psi\Eos\Entrata\CApplicants::createService()->fetchSimpleApplicantsByCustomerTypeIdsByApplicationIdByCid( CCustomerType::$c_arrintResponsibleCustomerTypeIds, $this->getId(), $this->getCid(), $this->m_objDatabase );
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjApplicants ) ) {
				foreach( $arrobjApplicants as $objApplicant ) {
					if( CCustomerType::GUARANTOR != $objApplicant->getCustomerTypeId() ) {
						$strApplicants .= '<li> ' . $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() . '</li>';
					} else {
						$strGaurantors .= '<li> ' . $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() . '</li>';
					}
				}
			}
		}

		$strApplicatText = __( 'Applicant(s):' );
		$strGautantorText = __( 'Guarantor(s):' );
		$strLeaseDocumentText = __( 'Lease Document(s):' );

		$strEmailContent .= '<tr>' . $strTdCss . '<strong>' . $strApplicatText . '</strong>
				<ul>' . $strApplicants . '</ul></td></tr>';

		if( '' != $strGaurantors ) {
			$strEmailContent .= '<tr>' . $strTdCss . '<strong>' . $strGautantorText . '</strong>
					<ul>' . $strGaurantors . '</ul></td></tr>';
		}

		if( '' != $strDocuments ) {
			$strEmailContent .= '<tr>' . $strTdCss . '
					<strong>' . $strLeaseDocumentText . '</strong>
					<ul>' . $strDocuments . '</ul></td></tr>';
		}

		$strFooterContent = __( 'Please retain for your records.' );
		$strEmailContent .= '<tr> ' . $strTdCss . '<strong>' . $strFooterContent . '<br/></strong>
					</td></tr></table>';

		return $strEmailContent;
	}

	public function getLeasePartiallyCompletedApplicantDetails() {
		$intApplicationId   = ( true == valId( $this->getLeaseId() ) ) ? $this->getLeaseId() : $this->getId();
		$strApplicantsSignedDocument    = '';
		$strApplicantsNotSignedDocument = '';

		$strTdCss = '<td style="font:normal normal 13px/18px arial; color:#000000; padding-top:15px; padding-bottom:10px;" align="left">';

		$strApplicationIdText = __( 'Application ID: ' );

		$strEmailContent = '<table cellpadding="0" cellspacing="0" border="0" style="width:570px;"><tr>' . $strTdCss . '
						<strong>' . $strApplicationIdText . ( int ) $intApplicationId . '</strong><br/> </td></tr>';

		$arrobjApplicants = ( array ) \Psi\Eos\Entrata\CApplicants::createService()->fetchSimpleApplicantsByCustomerTypeIdsByApplicationIdByCid( CCustomerType::$c_arrintResponsibleCustomerTypeIds, $this->getId(), $this->getCid(), $this->m_objDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjApplicants ) ) {
			$arrobjApplicantApplications = $this->fetchApplicantApplicationsByApplicantIds( array_keys( $arrobjApplicants ), $this->m_objDatabase );

			if( false == valArr( $arrobjApplicantApplications ) ) {
				return NULL;
			}
			foreach( $arrobjApplicantApplications as $objApplicantApplication ) {
				$objApplicant = $arrobjApplicants[$objApplicantApplication->getApplicantId()];
				if( true == valObj( $objApplicant, 'CApplicant' ) && $objApplicantApplication->getId() == $objApplicant->getApplicantApplicationId() && '' != $objApplicantApplication->getLeaseSignedOn() ) {
					$strApplicantsSignedDocument .= '<li> ' . $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() . '</li>';
				} else if( true == valObj( $objApplicant, 'CApplicant' ) && $objApplicantApplication->getId() == $objApplicant->getApplicantApplicationId() && '' == $objApplicantApplication->getLeaseSignedOn() ) {
					$strApplicantsNotSignedDocument .= '<li> ' . $objApplicant->getNameFirst() . ' ' . $objApplicant->getNameLast() . '</li>';
				}
			}
		}

		$strApplicantSignedText = __( 'Applicant(s)/Guarantor(s) that have signed their document(s):' );
		$strApplicantNotSignedText = __( 'Applicant(s)/Guarantor(s) that have not yet signed their document(s):' );

		$strEmailContent .= '<tr>' . $strTdCss . '<strong>' . $strApplicantSignedText . '</strong>
				<ul>' . $strApplicantsSignedDocument . '</ul></td></tr>';

		$strEmailContent .= '<tr>' . $strTdCss . '<strong>' . $strApplicantNotSignedText . '</strong>
					<ul>' . $strApplicantsNotSignedDocument . '</ul></td></tr>';

		$strFooterText = __( 'Please retain for your records.' );
		$strEmailContent .= '<tr>' . $strTdCss . '
						<strong>' . $strFooterText . '<br/></strong></td></tr></table>';

		return $strEmailContent;
	}

	public function refreshFields( $arrstrFields, $strTableName, $objDatabase ) {

		$arrstrData = fetchData( sprintf( 'SELECT %s FROM %s WHERE id = %d AND cid = %d', implode( ',', $arrstrFields ), $strTableName, $this->getId(), $this->getCid() ), $objDatabase );

		foreach( $arrstrFields as $strFieldName ) {
			$strMethodName = 'set' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strFieldName ) ) );

			if( true == isset( $arrstrData[0][$strFieldName] ) && true == method_exists( $this, $strMethodName ) ) {
				$this->{$strMethodName}( $arrstrData[0][$strFieldName] );
			}
		}

	}

	public function checkStampSignatureOnGCICConsentForm( $objDatabase, $intApplicantId = NULL ) {

		$objFileAssociation = CFileAssociations::createService()->fetchFileAssociationByApplicantIdByApplicationIdBySystemCodeByCid( $intApplicantId, $this->getId(), CFileType::SYSTEM_CODE_GCIC, $this->getCid(), $objDatabase );
		if( false == valObj( $objFileAssociation, 'CFileAssociation' ) ) {
			return false;
		}

		if( true == valStr( $objFileAssociation->getFileSignedOn() ) ) {
			return false;
		}

		return true;
	}

	public function isGCICEmailAlreadySent( $intCustomerId, $objDatabase ) {
		$strEventDatetime = \Psi\Eos\Entrata\CEvents::createService()->fetchLastEventDatetimeByEventTypeIdByLeaseIdByLeaseIntervalIdByCustomerIdByCid( CEventType::GCIC_CONSENT_FORM_EMAIL_SENT, $this->getLeaseId(), $this->getLeaseIntervalId(), $intCustomerId, $this->getCid(), $objDatabase );
		return ( true == valStr( $strEventDatetime ) ) ? true : false;
	}

	public function logScreenningAuthorizationEvent( $intEventTypeId, $arrmixApplicantDetails, $intCompanyUserId, $objDatabase ) {

		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$this->m_objEvent          = $objEventLibrary->createEvent( [ $this ], $intEventTypeId );

		$arrmixLogData = [ 'applicant_name' => $arrmixApplicantDetails['name_first'] . ' ' . $arrmixApplicantDetails['name_last'] ];

		$this->m_objEvent->setDataReferenceId( NULL );
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setLease( $this->getOrFetchLease( $objDatabase ) );
		$objEventLibraryDataObject->setData( $arrmixLogData );

		$this->m_objEvent->setCid( $this->getCid() );
		$this->m_objEvent->setOldStatusId( $this->getApplicationStatusId() );
		$this->m_objEvent->setOldStageId( $this->getApplicationStageId() );
		$this->m_objEvent->setNewStageId( $this->getApplicationStageId() );
		$this->m_objEvent->setNewStatusId( $this->getApplicationStatusId() );
		$this->m_objEvent->setNewStatusId( $this->getApplicationStatusId() );
		$this->m_objEvent->setCustomerId( $arrmixApplicantDetails['customer_id'] );
		$this->m_objEvent->setEventDatetime( 'NOW()' );
		$this->m_objEvent->setDoNotExport( true );

		$objEventLibrary->buildEventDescription( $this );
		$objEventLibrary->setIsExportEvent( false );

		if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function createEventForGCICDocument( $intEventTypeId, $objDatabase ) {
		$objEventLibrary 			= new CEventLibrary();
		$objEventLibraryDataObject 	= $objEventLibrary->getEventLibraryDataObject();
		$this->m_objEvent 			= $objEventLibrary->createEvent( [ $this ], $intEventTypeId );

		$this->m_objEvent->setDataReferenceId( NULL );
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setLease( $this->getOrFetchLease( $objDatabase ) );

		$this->m_objEvent->setCid( $this->getCid() );
		$this->m_objEvent->setOldStageId( $this->getApplicationStageId() );
		$this->m_objEvent->setNewStageId( $this->getApplicationStageId() );
		$this->m_objEvent->setOldStatusId( $this->getApplicationStatusId() );
		$this->m_objEvent->setNewStatusId( $this->getApplicationStatusId() );
		$this->m_objEvent->setEventDatetime( 'NOW()' );
		$this->m_objEvent->setDoNotExport( true );

		return $objEventLibrary;
	}

	public function getApplicationPreferenceWebsiteUrl() {
		return $this->generateWebsiteUrl( $this->m_objDatabase, NULL, false, true );
	}

	public function createEventForMethodOfContact( $intCompanyUserId, $intEventTypeId, $objDatabase ) {
		$objEventLibrary = new CEventLibrary();

		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		if( true == valObj( $this->getApplicant(), 'CApplicant' ) ) {
			$objEventLibraryDataObject->setApplicant( $this->getApplicant() );
		}
		$objEventLibraryDataObject->setApplication( $this );

		$objEvent = $objEventLibrary->createEvent( [ $this ], $intEventTypeId );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setPropertyId( $this->getPropertyId() );
		$objEvent->setNewStatusId( $this->getApplicationStatusId() );
		$objEvent->setNewStageId( $this->getApplicationStageId() );
		$objEvent->setEventDatetime( $this->getApplicationDatetime() );
		$objEvent->setEventSubTypeId( NULL );
		$objEvent->setIpAddress( getRemoteIpAddress() );
		$objEvent->setLeaseIntervalId( $this->getLeaseIntervalId() );
		$objEvent->setLeaseId( $this->getLeaseId() );
		$objEvent->setPsProductId( $this->getPsProductId() );
		$objEvent->setDataReferenceId( $this->getId() );
		$objEvent->setReference( $this );
		if( CEventType::EXTERNAL == $intEventTypeId ) {
			$objEvent->setDoNotExport( true );
		}

		$objEventResult = \Psi\Eos\Entrata\CEventResults::createService()->fetchEventResultByEventTypeIdByDefaultEventResultIdByCid( $intEventTypeId, CDefaultEventResult::COMPLETED,  $this->getCid(), $objDatabase );
		if( true == valObj( $objEventResult, CEventResult::class ) ) {
			$objEvent->setEventResultId( $objEventResult->getId() );
		}

		if( CEventType::EXTERNAL != $intEventTypeId ) {
			$objEventLibrary->buildEventDescription( $this );
		}

		if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );
			return false;
		}

		return true;
	}

	public function getProspectPortalTermsAndConditionsLink() {
		$strProspectPortalTermsAndConditionsUrl = $this->generateWebsiteUrl( $this->m_objDatabase ) . '?module=miscellaneous_items&action=view_terms_and_conditions';

		$strProspectPortalTermsAndConditionsLink = __( '<a href="{%s, 0}"  target="_blank"><u>Terms and Conditions.</u></a>', [ $strProspectPortalTermsAndConditionsUrl ] );
		return $strProspectPortalTermsAndConditionsLink;
	}

	public function isGroupContractApplication() {
		if( true == valId( $this->getUnitSpaceId() )
		    && true == valObj( $objLease = $this->getOrFetchLease(), 'CLease' )
		    && true == valId( $objLease->getOrganizationContractId() )
		    && 0 < \Psi\Eos\Entrata\COrganizationContractUnitSpaces::createService()->fetchCountActiveOrganizationContractUnitSpacesByUnitSpaceIdByCid( $this->getUnitSpaceId(), $this->getCid(), $this->m_objDatabase ) ) {
			return true;
		} else {
			return false;
		}
	}

	public function getTransferRequestNotes() {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		return $arrmixRequiredParameters['notes'] ?? '';
	}

	public function getStudentRentRangeMaxAmenities() {
		return $this->getPropertyFloorPlanRentRange( true );
	}

	public function getStudentRentRangeOptedAmenities() {
		return $this->getPropertyFloorPlanRentRange( false, true );
	}

	public function getStudentLeaseTotalWAmenities() {
		return $this->getPropertyFloorPlanRentRange( true, false, true );
	}

	public function setNameLastMatronymic( $strNameLastMatronymic ) {
		$this->m_strNameLastMatronymic = $strNameLastMatronymic;
	}

	public function getNameLastMatronymic( $objDatabase = NULL ) {
		if( true == isset( $this->m_strNameLastMatronymic ) ) return $this->m_strNameLastMatronymic;
		if( false == isset( $this->m_objPrimaryApplicant ) ) {
			$this->m_objPrimaryApplicant = $this->getOrFetchPrimaryApplicant( $objDatabase );
		}
		if( true == isset( $this->m_objPrimaryApplicant ) ) {
			$this->m_strNameLastMatronymic = $this->m_objPrimaryApplicant->getNameLastMatronymic();
		}
		return $this->m_strNameLastMatronymic;
	}

	public function getPropertyFloorPlanRentRange( $boolIsIncludeMaxSpaceAmenities = false, $boolIsIncludeSelectedSpaceAmenities = false, $boolIsLeaseTotalRentRange = false ) {
		$fltRentMin = $fltRentMax = 0;
		$arrstrPropertyPreferenceKeys = [ 'ENABLE_SEMESTER_SELECTION', 'TURN_ON_ROUNDING_WITHIN_LEASE', 'GENERATE_FLOORPLAN_RENT_USING_VISIBLE_UNITS', 'SPECIFIC_UNITS_OVERRIDE_AVAILABLE_UNITS', 'FLOORPLAN_UNIT_DISPLAY_LIMIT', 'UNITS_DISPLAY_ORDER', 'ENABLE_IMMEDIATE_MOVE_IN', 'HIDE_UNITS_WITH_NO_RENT' ];
		$arrobjPropertyPreferences = ( array ) CPropertyPreferences::createService()->fetchCustomPropertyPreferencesByKeysByPropertyIdByCid( $arrstrPropertyPreferenceKeys, $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		$arrintOccupancyTypeIds = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyOccupancyTypeIdsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		if( true == valArrKeyExists( $arrobjPropertyPreferences, 'ENABLE_SEMESTER_SELECTION' ) && true == in_array( COccupancyType::STUDENT, $arrintOccupancyTypeIds ) ) {
			$objFloorplanAvailabilityFilter = new CFloorplanAvailabilityFilter();
			$objFloorplanAvailabilityFilter->setIsForStudentProperty( true );
			$objFloorplanAvailabilityFilter->setCid( $this->getCid() );
			$objFloorplanAvailabilityFilter->setIsFetchAllUnits( true );
			$objFloorplanAvailabilityFilter->setPropertyIds( [ $this->getPropertyId() ] );

			$objFloorplanAvailabilityFilter->setApplicationId( $this->getId() );
			$objFloorplanAvailabilityFilter->setIsFromProspectPortal( false );
			$objFloorplanAvailabilityFilter->setLeaseTermIds( [ $this->getLeaseTermId() ] );
			$objFloorplanAvailabilityFilter->setLeaseStartWindowId( $this->getLeaseStartWindowId() );
			$objFloorplanAvailabilityFilter->setPropertyFloorplanIds( ( array ) $this->getPropertyFloorplanId() );

			$arrmixPropertyArOriginRule = ( array ) \Psi\Eos\Entrata\CPropertyArOriginRules::createService()->fetchUnitSpaceAndLeaseTermConfigurationsByPropertyIdByArOriginIdByCid( $this->getPropertyId(), CArOrigin::BASE, $this->getCid(), $this->m_objDatabase );
			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixPropertyArOriginRule ) ) {
				if( true == $arrmixPropertyArOriginRule['use_space_configurations_for_rent'] || true == $arrmixPropertyArOriginRule['use_space_configurations_for_deposits'] ) {
					$objFloorplanAvailabilityFilter->setIsUseSpaceConfigurations( true );
				}

				if( true == $arrmixPropertyArOriginRule['use_lease_terms_for_rent'] || true == $arrmixPropertyArOriginRule['use_lease_terms_for_deposits'] ) {
					$objFloorplanAvailabilityFilter->setIsFetchLeaseTerms( true );
				}
			}

			$arrobjPropertyFloorplans = ( array ) CPropertyFloorplans::createService()->fetchCalculatedPropertyFloorplansByFloorplanFilterByPropertyPreferences( $objFloorplanAvailabilityFilter, $this->m_objDatabase, $arrobjPropertyPreferences );

			if( true == valArr( $arrobjPropertyFloorplans ) ) {
				if( true == valArrKeyExists( $arrobjPropertyPreferences, 'ENABLE_SEMESTER_SELECTION' ) ) {
					if( true == $objFloorplanAvailabilityFilter->getIsUseSpaceConfigurations() ) {
						$arrobjTempPropertyFloorplans = rekeyObjects( 'SpaceConfigurationId', $arrobjPropertyFloorplans );
						if( true == valArr( $arrobjTempPropertyFloorplans ) && true == array_key_exists( $this->getSpaceConfigurationId(), $arrobjTempPropertyFloorplans ) ) {
							unset( $arrobjPropertyFloorplans );
							$arrobjPropertyFloorplans[$arrobjTempPropertyFloorplans[$this->getSpaceConfigurationId()]->getId()] = $arrobjTempPropertyFloorplans[$this->getSpaceConfigurationId()];
						}
					} else {
						$arrobjPropertyFloorplans = rekeyObjects( 'Id', $arrobjPropertyFloorplans );
					}
				}

				$intPropertyFloorplanId = $this->getPropertyFloorplanId();
				foreach( $arrobjPropertyFloorplans as $intIndex => $objPropertyFloorplan ) {
					if( ( false == is_null( $intPropertyFloorplanId ) ) && $intPropertyFloorplanId == $intIndex ) {
						$fltRentMin = ( float ) $objPropertyFloorplan->getMinRent();
						$fltRentMax = ( float ) $objPropertyFloorplan->getMaxRent();
						break;
					}
				}
			}
		}
		if( 0 == $fltRentMin && 0 == $fltRentMax ) {
			return NULL;
		}

		$fltMaxUnitSpaceAmenityRentCharges = 0.0;
		if( true == $boolIsIncludeMaxSpaceAmenities ) {
			$fltMaxUnitSpaceAmenityRentCharges = $this->getStudentUnitSpaceAmenityCharges();
		}

		if( true == $boolIsIncludeSelectedSpaceAmenities && true == valArr( ( array ) $this->getPreferredAmenityIds() ) ) {
			$fltMaxUnitSpaceAmenityRentCharges = $this->getStudentUnitSpaceAmenityCharges( $boolIsIncludeSelectedSpaceAmenities );
		}

		if( true == $boolIsLeaseTotalRentRange && true == valObj( $this->m_objLeaseInterval, CLeaseInterval::class ) ) {
			$objPropertyChargeSetting = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

			$boolIsImmediateMoveIn = false;
			if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'ENABLE_IMMEDIATE_MOVE_IN', $arrobjPropertyPreferences ) ) {
				$boolIsImmediateMoveIn = true;
			}

			$boolIsProrateCharges = true;
			if( true == valObj( $objPropertyChargeSetting, CPropertyChargeSetting::class ) && 0 == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, NULL, $boolIsImmediateMoveIn ) ) {
				$boolIsProrateCharges = false;
			}

			$objLeaseTerm = \Psi\Eos\Entrata\CLeaseTerms::createService()->fetchDateBasedLeaseTermByLeaseStartWindowIdByCid( $this->m_objLeaseInterval->getLeaseStartWindowId(), $this->getCid(), $this->m_objDatabase );
			if( true == valObj( $objLeaseTerm, CLeaseterm::class ) ) {
				$objLeaseTerm->setStartDate( $this->m_objLeaseInterval->getLeaseStartDate() );
				$objLeaseTerm->setEndDate( $this->m_objLeaseInterval->getLeaseEndDate() );
				$arrintTermMonth = explode( '.', $objLeaseTerm->calculateTermMonth( $boolIsProrateCharges ) );

				if( true == valArr( $arrintTermMonth ) ) {
					$intNumberOfDays = date( 't', strtotime( $this->m_objLeaseInterval->getLeaseEndDate() ) );

					if( true == valObj( $objPropertyChargeSetting, CPropertyChargeSetting::class ) ) {
						$intNumberOfDays = $objPropertyChargeSetting->getFinancialDaysInMonth( $intNumberOfDays );
					}

					$fltRentMinTotal                        = $fltRentMin * $arrintTermMonth[0];
					$fltRentMaxTotal                        = $fltRentMax * $arrintTermMonth[0];
					$fltMaxUnitSpaceAmenityRentChargesTotal = $fltMaxUnitSpaceAmenityRentCharges * $arrintTermMonth[0];

					if( 1 < \Psi\Libraries\UtilFunctions\count( $arrintTermMonth ) && 0 < $arrintTermMonth[1] ) {
						$fltRentMinTotal                        += ( $fltRentMin / $intNumberOfDays ) * $arrintTermMonth[1];
						$fltRentMaxTotal                        += ( $fltRentMax / $intNumberOfDays ) * $arrintTermMonth[1];
						$fltMaxUnitSpaceAmenityRentChargesTotal += ( $fltMaxUnitSpaceAmenityRentCharges / $intNumberOfDays ) * $arrintTermMonth[1];
					}

					$fltRentMin                        = $fltRentMinTotal;
					$fltRentMax                        = $fltRentMaxTotal;
					$fltMaxUnitSpaceAmenityRentCharges = $fltMaxUnitSpaceAmenityRentChargesTotal;
				}
			}
		}

		return $this->getRentRange( $fltRentMin, bcadd( $fltRentMax, $fltMaxUnitSpaceAmenityRentCharges ), $arrobjPropertyPreferences );
	}

	private function getStudentUnitSpaceAmenityCharges( $boolIsForSelectedSpaceAmenities = false ) {

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ ( int ) $this->getCid() ] );
		$objRateLogsFilter->setPropertyGroupIds( [ ( int ) $this->getPropertyId() ] );
		$objRateLogsFilter->setPropertyFloorplanIds( [ ( int ) $this->getPropertyFloorplanId() ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::AMENITY ] );
		$objRateLogsFilter->setArCodeTypeIds( [ CArCodeType::RENT ] );
		$objRateLogsFilter->setMaxArCascadeId( [ CArCascade::SPACE ] );
		$objRateLogsFilter->setPricingTypeId( [ CCompanyPricingType::RESIDENT_LEASE ] );
		$objRateLogsFilter->setLoadAllSpaceConfigurations( true );
		$objRateLogsFilter->setIsEntrata( true );
		if( true == $boolIsForSelectedSpaceAmenities ) {
			$objRateLogsFilter->setArOriginReferenceIds( ( array ) $this->getPreferredAmenityIds() );
		}

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $this->m_objDatabase );

		$strSql = 'SELECT
                        max ( sub.total_rate_amount ) AS max_space_amenity_rent
					FROM
                        (
							SELECT
								sum( rate_amount ) AS total_rate_amount
							FROM
								' . \Psi\Eos\Entrata\CRateLogs::TEMP_TABLE_PROSPECT_RATES . '
							WHERE
								ar_cascade_id = ' . CArCascade::SPACE . '
							GROUP BY
                                ar_cascade_reference_id
                        ) AS sub';

		$fltMaxUnitSpaceAmenityRentCharges = \Psi\Eos\Entrata\CRateLogs::createService()->fetchColumn( $strSql, 'max_space_amenity_rent', $this->m_objDatabase );

		return ( true == valStr( $fltMaxUnitSpaceAmenityRentCharges ) ) ? $fltMaxUnitSpaceAmenityRentCharges : 0.0;
	}

	private function getRentRange( $fltMinRent, $fltMaxRent, $arrobjPropertyPreferences ) {
		if( true == valArrKeyExists( $arrobjPropertyPreferences, 'TURN_ON_ROUNDING_WITHIN_LEASE' ) && in_array( ( int ) $arrobjPropertyPreferences['TURN_ON_ROUNDING_WITHIN_LEASE']->getValue(), [ 1, 2 ] ) ) {
			$fltMinRent = __( '{%f, 0, p:0}', [ $fltMinRent ] );
			$fltMaxRent = __( '{%f, 0, p:0}', [ $fltMaxRent ] );
		} else {
			$fltMinRent = __( '{%f, 0, p:2}', [ $fltMinRent ] );
			$fltMaxRent = __( '{%f, 0, p:2}', [ $fltMaxRent ] );
		}

		if( $fltMinRent == $fltMaxRent || 0 == $fltMinRent ) {
			$fltFloorPlanRentRange = $fltMaxRent;
		} elseif( 0 == $fltMaxRent ) {
			$fltFloorPlanRentRange = $fltMinRent;
		} else {
			$fltFloorPlanRentRange = $fltMinRent . ' - ' . $fltMaxRent;
		}

		return $fltFloorPlanRentRange;
	}

	public function getTermsAndConditionsPath() {
		$strSecureBaseName = $this->generateWebsiteUrl( $this->m_objDatabase );
		if( false == valStr( $strSecureBaseName ) ) {
			return '';
		}

		return $strSecureBaseName . '?module=miscellaneous_items&action=view_terms_and_conditions&application[id]=' . $this->getId() . '&company_application_id=' . $this->getCompanyApplicationId();
	}

	public function genrateResidentVerifyPortalUrl() {
		$strResidentVerifyPortalSuffix = 'residentverify.com';

		if( true == defined( 'CONFIG_RESIDENT_VERIFY_PORTAL_SUFFIX' ) ) {
			$strResidentVerifyPortalSuffix = CONFIG_RESIDENT_VERIFY_PORTAL_SUFFIX;
		} elseif( false == is_null( CConfig::get( 'resident_verify_portal_suffix' ) ) ) {
			$strResidentVerifyPortalSuffix = CConfig::get( 'resident_verify_portal_suffix' );
		}

		return CONFIG_SECURE_HOST_PREFIX . $strResidentVerifyPortalSuffix;
	}

	public function getOneTimeUrlLink() {
		$arrmixParameters = $this->getRequiredParameters();
		if( false == valArrKeyExists( $arrmixParameters, 'unique_id' ) && false == valArrKeyExists( $arrmixParameters, 'applicant_id' ) ) {
			return '';
		}

		$strSecureBaseName = $this->genrateResidentVerifyPortalUrl();
		if( false == valStr( $strSecureBaseName ) ) {
			return '';
		}

		$strAction = '/parentalConsentForms/?module=parental_consent_form&action=view_parental_consent_form';

		$strParameters = '&clientId=' . $this->getCid() . '&propertyId=' . $this->getPropertyId() . '&applicationId=' . $this->getId() . '&applicantId=' . $arrmixParameters['applicant_id'] . '&applicantKey=' . rawurlencode( $arrmixParameters['unique_id'] );
		$strParameters = '&keys=' . rawurlencode( CEncryption::encrypt( $strParameters, CONFIG_KEY_ID ) );

		return $strSecureBaseName . $strAction . $strParameters;
	}

	public static function getLeaseEndDateAfterApplyingRoundingPreferences( $arrobjPropertyPreferences, $strLeaseEndDate ) {

		$intMonthValue = 0;
		$strLastDateOfMonth = CDates::createService()->getLastDayOfMonth( date( 'm/d/Y', strtotime( $strLeaseEndDate ) ), $intMonthValue, 'm/d/Y' );

		if( strtotime( $strLastDateOfMonth ) == strtotime( $strLeaseEndDate ) ) return $strLeaseEndDate;

		$objPropertyPreference 	= getArrayElementByKey( 'ROUNDING_LEASE_TERM', $arrobjPropertyPreferences );

		if( false == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) return $strLeaseEndDate;

		if( 'None' == $objPropertyPreference->getValue() ) return $strLeaseEndDate;

		if( 'EOM' == $objPropertyPreference->getValue()
			&& true == valObj( $objRoundToEOMPropertyPreference = getArrayElementByKey( 'ROUND_TO_PREV_EOM_IF_DATE_ON', $arrobjPropertyPreferences ), 'CPropertyPreference' )
			&& 0 < $objRoundToEOMPropertyPreference->getValue()
			&& ( int ) date( 'd', strtotime( $strLeaseEndDate ) ) <= ( int ) $objRoundToEOMPropertyPreference->getValue() ) {

			$intMonthValue = -1;
		} elseif( 'Prev EOM' == $objPropertyPreference->getValue() ) {
			$intMonthValue = -1;
		}

		$strLeaseEndDate = CDates::createService()->getLastDayOfMonth( date( 'm/d/Y', strtotime( $strLeaseEndDate ) ), $intMonthValue, 'm/d/Y' );

		return $strLeaseEndDate;

	}

	public function getTourEventDetails() {
		$intContactBtnVal = $this->getRequiredParameters()['is_contact_btn'] ?? 0;
		$strPropertyTimeZoneName = NULL;
		$objPropertyTimeZone     = \Psi\Eos\Entrata\CTimeZones::createService()->fetchTimeZoneByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		if( true == valObj( $objPropertyTimeZone, CTimeZone::class ) ) {
			$strPropertyTimeZoneName = $objPropertyTimeZone->getTimeZoneName();
		}

		$objTourOnsiteVisitEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchCustomTourEventByLeaseIntervalIdByEventTypeIdsByCid( $this->getLeaseIntervalId(), [ CEventType::TOUR, CEventType::ONSITE_VISIT, CEventType::SCHEDULED_APPOINTMENT ], [], $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objTourOnsiteVisitEvent, CEvent::class ) ) {
			return '';
		}

		if( 1 != $intContactBtnVal ) {

			$arrmixUnitSpacesNumbers = [];

			$arrintUnitSpaceIds    = [];
			$arrobjEventReferences = ( array ) \Psi\Eos\Entrata\CEventReferences::createService()->fetchEventReferencesByEventIdByEventReferenceTypeIdByCid( $objTourOnsiteVisitEvent->getId(), CEventReferenceType::UNIT_SPACE, $this->getCid(), $this->m_objDatabase );
			$arrintUnitSpaceIds    = array_keys( rekeyObjects( 'ReferenceId', $arrobjEventReferences ) );

			if( false == valArr( $arrintUnitSpaceIds ) && true == valId( $this->getUnitSpaceId() ) ) {
				$arrintUnitSpaceIds = [ $this->getUnitSpaceId() ];
			}

			$arrobjUnitSpaces = ( array ) CUnitSpaces::createService()->fetchUnitSpacesByIdsByCid( $arrintUnitSpaceIds, $this->getCid(), $this->m_objDatabase );

			$strText = __( '( Scheduled Date/time : {%t, 0, DATETIME_ALPHA_SHORT} ) ', [ CInternationalDateTime::create( $objTourOnsiteVisitEvent->getScheduledDatetime(), $strPropertyTimeZoneName ) ] );

			if( false == is_null( $objTourOnsiteVisitEvent->getUnitSpaceId() ) ) {
				if( true == valArr( $arrobjUnitSpaces ) ) {
					foreach( $arrobjUnitSpaces as $objUnitSpace ) {
						$arrmixUnitSpacesNumbers[] = $objUnitSpace->getUnitNumberCache() ?? $objUnitSpace->getUnitNumber();
					}

					$strText .= __( '( Unit :{%s,0} )', [ implode( $arrmixUnitSpacesNumbers ) ] );
				}
			}
		} else {
			$strText = __( '( Scheduled Date/time : {%t, 0, DATETIME_ALPHA_SHORT_NOTZ} - {%t, 1, TIME_SHORT} )', [ CInternationalDateTime::create( $objTourOnsiteVisitEvent ->getScheduledDatetime(), $strPropertyTimeZoneName ), CInternationalDateTime::create( $objTourOnsiteVisitEvent->getScheduledEndDatetime(), $strPropertyTimeZoneName ) ] );

		}

		return __( '
							<p class="deets" style="margin: 15px 0; font-size: 16px;">
								<span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Tour Taken</span>
								<br>
								<span style="display: inline-block;">Yes {%s,0}</span>
							</p>
						', [ $strText ] );
	}

	public function getApplicationTourDetails() {
		$strOutput = '';

		$strRentLabel = __( 'rent' );
		$arrobjPropertyPreferences = ( array ) CPropertyPreferences::createService()->fetchPropertyPreferencesByKeysByPropertyIdByCid( [ 'RENAME_RENT_LABEL', 'HIDE_BUILDING_UNIT_FLOOR_PLAN_BED_INFO' ], $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		$arrobjPropertyPreferences = rekeyObjects( 'Key', $arrobjPropertyPreferences );
		if( true == array_key_exists( 'RENAME_RENT_LABEL', $arrobjPropertyPreferences ) ) {
			$strRentLabel = $arrobjPropertyPreferences['RENAME_RENT_LABEL']->getValue();
		}
		$strRentLabel = ucfirst( $strRentLabel );

		$arrmixLeadSources = rekeyArray( 'id', ( array ) \Psi\Eos\Entrata\CLeadSources::createService()->fetchLeadSourcesByCid( $this->getCid(), $this->m_objDatabase ) );

		$arrmixRequestData = $this->getRequiredParameters();
		if( false == is_null( $this->getDesiredRentMin() ) || false == is_null( $this->getDesiredRentMax() ) ) {
			$strText = '';
			if( 0 == $this->getDesiredRentMin() ) {
				$strText = __( 'Not Specified' );
			} else {
				$strText = __( '{%f, min_rent, p:2}', $this->getDesiredRentMin() );
			}

			if( false == is_null( $this->getDesiredRentMin() ) && false == is_null( $this->getDesiredRentMax() ) && 0 < $this->getDesiredRentMax() ) {
				$strText .= ' - ';
			}

			if( 0 < $this->getDesiredRentMax() ) {
				$strText .= __( '{%f, max_rent, p:2}', [ 'max_rent' => $this->getDesiredRentMax() ] );
			}

			$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Desired Monthly {%s, 0}</span><br><span style="display: inline-block;">{%s,1}</span></p>', [ $strRentLabel, $strText ] );
		}

		if( false == is_null( $this->getLeadSourceId() ) ) {
			$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Lead Source</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $arrmixLeadSources[$this->getLeadSourceId()]['name'] ] );
		}

		if( false == is_null( $this->getTermMonth() ) ) {
			$strOutput .= __( '<p class="deets" style="margin: 0 0 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Lease Term</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $this->getTermMonth() ] );
		}

		if( false == is_null( $this->getFloorplanName() ) ) {
			$strOutput .= __( '<p class="deets" style="margin: 0 0 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Desired Floorplan</span><br><span style="display: inline-block;"></span></p>', [ $this->getFloorplanName() ] );
		}

		if( true == isset( $arrmixRequestData['schedule_tour'] ) && 1 == $arrmixRequestData['schedule_tour'] ) {
			if( false == array_key_exists( 'HIDE_BUILDING_UNIT_FLOOR_PLAN_BED_INFO', $arrobjPropertyPreferences ) ) {
				$strText = $arrmixRequestData['floor_name'] ?? '-';
				$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Floor Name</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $strText ] );
			}

			if( true == isset( $arrmixRequestData['occupants'] ) && '' != $arrmixRequestData['occupants'] && 0 != $arrmixRequestData['occupants'] ) {
				$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Occupants</span><br><span style="display: inline-block;">{%d,0}</span></p>', [ $arrmixRequestData['occupants'] ] );
			}

			if( true == $arrmixRequestData['lease_term'] && '' != $arrmixRequestData['lease_term'] ) {
				$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Lease Term</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $arrmixRequestData['lease_term'] ] );
			}

			if( true == $arrmixRequestData['floor_unit_rent'] && '' != $arrmixRequestData['floor_unit_rent'] ) {
				$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Unit {%s,0}</span><br><span style="display: inline-block;">{%s,1}</span></p>', [ $strRentLabel, $arrmixRequestData['floor_unit_rent'] ] );
			}

			if( true == $arrmixRequestData['floor_details'] && '' != $arrmixRequestData['floor_details'] ) {
				$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Location</span><br><span style="display: inline-block;">{%s,0}</span></p>', [ $arrmixRequestData['floor_details'] ] );
			}

			if( true == $arrmixRequestData['schedule_tour_date'] && true == $arrmixRequestData['requested_time'] ) {
				$strOutput .= __( '<p class="deets" style="margin: 15px 0; font-size: 16px;"><span style="color: #999; display: inline-block; font-size: 14px; font-style: italic; font-weight: 200;">Day & Time</span><br><span style="display: inline-block;">{%s,0}, {%s,1}</span></p>', [ $arrmixRequestData['schedule_tour_date'], $arrmixRequestData['requested_time'] ] );
			}
		}

		return $strOutput;
	}

	public function getApplicantNotes() {
		return $this->getRequiredParameters()['notes'] ?? '-';
	}

	public function getSenderIpAddress() {
		return $this->getRequiredParameters()['sender_ip_address'] ?? '-';
	}

	public function getRequestedTourDateAndTime() {
		$strPropertyTimeZoneName = NULL;

		$objPropertyTimeZone     = \Psi\Eos\Entrata\CTimeZones::createService()->fetchTimeZoneByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		if( true == valObj( $objPropertyTimeZone, CTimeZone::class ) ) {
			$strPropertyTimeZoneName = $objPropertyTimeZone->getTimeZoneName();
		}

		$objTourOnsiteVisitEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchCustomTourEventByLeaseIntervalIdByEventTypeIdsByCid( $this->getLeaseIntervalId(), [ CEventType::TOUR, CEventType::ONSITE_VISIT, CEventType::SCHEDULED_APPOINTMENT ], [], $this->getCid(), $this->m_objDatabase );

		if( false == valObj( $objTourOnsiteVisitEvent, CEvent::class ) ) {
			return '';
		}

		$strRequestedTime = $this->getRequiredParameters()['requested_time'];
		return __( '{%t, 0, DATE_ALPHA_MEDIUM} {%s,1}', [ CInternationalDateTime::create( $objTourOnsiteVisitEvent->getScheduledDatetime(), $strPropertyTimeZoneName ), $strRequestedTime ] );
	}

	public function getCallOrChatId() {
		$intCallOrChatId = $this->getRequiredParameters()['call_id'] ?? $this->getRequiredParameters()['chat_id'];

		if( true == valId( $intCallOrChatId ) ) {
			return __( 'Call Reference ID: {%s,0}', $intCallOrChatId );
		}

		return NULL;
	}

	public function getLCDiscussedPQRequirements() {

		$arrstrQualificationRequirementLabels = [
			'CALL_CENTER_LEAD_INCOME_REQUIREMENT'	=> __( 'Income Requirements' ),
			'CALL_CENTER_LEAD_SECTION_EIGHT' 		=> __( 'Section 8' ),
			'CALL_CENTER_LEAD_GENERAL_PET_POLICY'	=> __( 'Pet Policy' ),
			'CALL_CENTER_LEAD_SMOKING_POLICY'		=> __( 'Smoking Policy' ),
			'CALL_CENTER_LEAD_OCCUPANCY_LIMIT'		=> __( 'Occupancy Limits' ),
			'CALL_CENTER_LEAD_FEES_REQUIRED'		=> __( 'Fees Required with Application' ),
			'CALL_CENTER_LEAD_CREDIT_HISTORY'		=> __( 'Credit History' ),
			'CALL_CENTER_LEAD_CRIMINAL_HISTORY'		=> __( 'Criminal History' ),
			'CALL_CENTER_LEAD_RENTAL_HISTORY'		=> __( 'Rental History' ),
			'CALL_CENTER_LEAD_MORE_INFORMATION'		=> __( 'More Info' ),
			'CALL_CENTER_LEAD_MORE_REQUIREMENT'		=> __( 'More Req' )
		];

		$arrmixCustomerVotes = ( array ) \Psi\Eos\Entrata\CCustomerVotes::createService()->fetchCustomPropertyNotificationsOfCustomerVotesByCustomerIdByPropertyIdByCustomerVoteTypeIdByCid( $this->getRequiredParameters()['customer_id'], $this->getPropertyId(), CCustomerVoteType::QUALIFICATION,  $this->getCid(), $this->m_objDatabase );

		if( false == valArr( $arrmixCustomerVotes ) ) {
			return NULL;
		}

		$intReviewCount = $intFlaggedCount = 1;
		$arrstrContents = [ 0 => NULL, 1 => NULL, -1 => NULL ];
		foreach( $arrmixCustomerVotes as $arrmixCustomerVote ) {
			$strFirstText = NULL;
			$strComment = $arrmixCustomerVote['comment'] ?? '-';
			if( 0 == $arrmixCustomerVote['vote'] && 1 == $intReviewCount++ ) {
				$strFirstText = __( 'Review' );
			}

			if( 1 == $arrmixCustomerVote['vote'] && 1 == $intFlaggedCount++ ) {
				$strFirstText = __( 'Flagged' );
			}

			$strContent = '
							<tr style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
								<td width="20%" style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">' . $strFirstText . '</td>
								<td width="30%" style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">' . $arrstrQualificationRequirementLabels[$arrmixCustomerVote['key']] . '</td>
								<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">' . $strComment . '</td>
							</tr>
							';

			$arrstrContents[$arrmixCustomerVote['vote']] = $arrstrContents[$arrmixCustomerVote['vote']] . $strContent;

		}

		return $arrstrContents[0] . $arrstrContents[1];
	}

	public function getLCDiscussedAmenity() {

		$arrmixCustomerVotes = ( array ) \Psi\Eos\Entrata\CCustomerVotes::createService()->fetchComparableAssociationsOfCustomerVotesByCustomerIdByPropertyIdByCustomerVoteTypeIdByCid( $this->getRequiredParameters()['customer_id'], $this->getPropertyId(), CCustomerVoteType::AMENITY, $this->getCid(), $this->m_objDatabase );

		if( false == valArr( $arrmixCustomerVotes ) ) {
			return NULL;
		}
		$intCommentCount = $intLikeCount = $intDislikeCount = 1;
		$arrstrContents = [ 0 => NULL, 1 => NULL, -1 => NULL ];
		foreach( $arrmixCustomerVotes as $arrmixCustomerVote ) {
			$strFirstText = NULL;
			$strComment = $arrmixCustomerVote['comment'] ?? '-';
			if( 0 == $arrmixCustomerVote['vote'] && 1 == $intCommentCount++ ) {
				$strFirstText = __( 'Comments' );
			}

			if( 1 == $arrmixCustomerVote['vote'] && 1 == $intLikeCount++ ) {
				$strFirstText = __( 'Liked' );
			}

			if( -1 == $arrmixCustomerVote['vote'] && 1 == $intDislikeCount++ ) {
				$strFirstText = __( 'Disliked' );
			}

			$strContent = '
							<tr style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
								<td width="20%" style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">' . $strFirstText . '</td>
								<td width="30%%" style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">' . $arrmixCustomerVote['title'] . '</td>
								<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">' . $strComment . '</td>
							</tr>
							';

			$arrstrContents[$arrmixCustomerVote['vote']] = $arrstrContents[$arrmixCustomerVote['vote']] . $strContent;

		}

		return $arrstrContents[1] . $arrstrContents[-1] . $arrstrContents[0];
	}

	public function getLCCustomerDataAppointmentDetails() {
		$strContent = NULL;

		$objCustomerExplanation = \Psi\Eos\Entrata\CCustomerExplanations::createService()->fetchCustomerExplanationByCustomerIdByPropertyIdByCustomerDataTypeIdByCid( $this->getRequiredParameters()['customer_id'], $this->getpropertyId(), CCustomerDataType::APPOINTMENT, $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $objCustomerExplanation, CCustomerExplanation::class ) ) {
			$strContent .= __( '<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">No Appointment:</td><td colspan="2" style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s,0}</td>', [ $objCustomerExplanation->getExplanation() ] );
		} elseif( true == isset( $this->getRequiredParameters()['event_date_time'] ) ) {
			$strContent .= __( '<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">Appointment</td><td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s,0}</td><td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;font-weight: 500;">{%s,1}</td>', [ $this->getRequiredParameters()['event_date_time'], $this->getRequiredParameters()['event_note'] ] );
		}

		return $strContent;
	}

	public function getMoveInDateRange() {
		return $this->getRequiredParameters()['move_in_date_range'] ?? '';
	}

	public function getFloorPlanRentRange() {
		return $this->getRequiredParameters()['rent_range'] ?? '';
	}

	public function getUnitBuilding() {
		return $this->getRequiredParameters()['unit_building'] ?? '';
	}

	public function getSellingPointsDiscussed() {
		$intCustomerId = $this->getRequiredParameters()['customer_id'] ?? NULL;

		if( false == valId( $intCustomerId ) ) {
			return NULL;
		}

		$arrobjCustomerVotes = ( array ) \Psi\Eos\Entrata\CCustomerVotes::createService()->fetchCustomerVotesWithPropertySellingPointTitleByCustomerIdByPropertyIdByCid( $intCustomerId, $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );
		if( false == valArr( $arrobjCustomerVotes ) ) {
			return NULL;
		}

		$strSellingPointsDiscussed = '<table style="margin: 0;padding: 0;border: 0;font-size: 12px;font: inherit;vertical-align: baseline;border-collapse: collapse;border-spacing: 0;width: 100%;">';
		$strSellingPointsLiked = NULL;
		$strSellingPointsDisLiked = NULL;
		$strSellingPointsComment = NULL;
		$strLiked = __( 'Liked' );
		$strDisliked = __( 'Disliked' );
		$strComment = __( 'Comments' );

		$intLikedCount = 0;
		$intDisLikedCount = 0;
		$intCommentCount = 0;

		foreach( $arrobjCustomerVotes as $objCustomerVote ) {
			if( CCustomerVote::LIKE == $objCustomerVote->getVote() ) {
				++$intLikedCount;
				$strSellingPointsLiked .= __( '<tr style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
														<td width="20%" style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
														<td width="30%%" style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 1}: </td>
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
											        </tr>', [ $intLikedCount == 1 ? $strLiked : '', $objCustomerVote->getTitle(), $objCustomerVote->getComment() ?? '-' ] );
			} else if( CCustomerVote::DISLIKE == $objCustomerVote->getVote() ) {
				++$intDisLikedCount;
				$strSellingPointsDisLiked .= __( '<tr style="background: #F6F6F6;margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
														<td style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 1}</td>
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
											        </tr>', [ $intDisLikedCount == 1 ? $strDisliked : '', $objCustomerVote->getTitle(), $objCustomerVote->getComment() ?? '-' ] );
			} else if( CCustomerVote::COMMENT == $objCustomerVote->getVote() ) {
				++$intCommentCount;
				$strSellingPointsComment .= __( '<tr style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
														<td style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 1}</td>
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
													</tr>', [ $intCommentCount == 1 ? $strComment : '', $objCustomerVote->getTitle(), $objCustomerVote->getComment() ] );
			}
		}

		return $strSellingPointsDiscussed . $strSellingPointsLiked . $strSellingPointsDisLiked . $strSellingPointsComment . '</table>';
	}

	public function getFloorplansAndUnitsDiscussed() {
		$intCustomerId = $this->getRequiredParameters()['customer_id'] ?? NULL;

		if( false == valId( $intCustomerId ) ) {
			return NULL;
		}

		$strFloorplansAndUnitsDiscussed = '<table style="margin: 0;padding: 0;border: 0;font-size: 12px;font: inherit;vertical-align: baseline;border-collapse: collapse;border-spacing: 0;width: 100%;">';

		$strLiked = __( 'Liked' );
		$strDisliked = __( 'Disliked' );
		$strComment = __( 'Comments' );

		$strFloorplanLiked = NULL;
		$strFloorplanDisLiked = NULL;
		$strFloorplanComment = NULL;
		$strUnitLiked = NULL;
		$strUnitDisLiked = NULL;
		$strUnitComment = NULL;

		$arrobjCustomerFloorplanVotes = ( array ) \Psi\Eos\Entrata\CCustomerVotes::createService()->fetchCustomerVotesWithPropertyFloorplanNameByCustomerIdByPropertyIdByCid( $intCustomerId, $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		if( true == valArr( $arrobjCustomerFloorplanVotes ) ) {
			$intLikedCount = 0;
			$intDisLikedCount = 0;
			$intCommentCount = 0;

			foreach( $arrobjCustomerFloorplanVotes as $objCustomerVote ) {
				if( CCustomerVote::LIKE == $objCustomerVote->getVote() ) {
					++$intLikedCount;
					$strFloorplanLiked .= __( '<tr style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
													<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
													<td style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 1} </td>
													<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
												</tr>', [ $intLikedCount == 1 ? $strLiked : '', $objCustomerVote->getFloorplanName(), $objCustomerVote->getComment() ?? '-' ] );
				} else if( CCustomerVote::DISLIKE == $objCustomerVote->getVote() ) {
					++$intDisLikedCount;
					$strFloorplanDisLiked .= __( '<tr style="background: #F6F6F6;margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
														<td style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 1}</td>
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
											        </tr>', [ $intDisLikedCount == 1 ? $strDisliked : '', $objCustomerVote->getFloorplanName(), $objCustomerVote->getComment() ?? '-' ] );
				} else if( CCustomerVote::COMMENT == $objCustomerVote->getVote() ) {
					++$intCommentCount;
					$strFloorplanComment .= __( '<tr style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
														<td width="20%" style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
														<td width="30%%" style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 1}</td>
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
													</tr>', [ $intCommentCount == 1 ? $strComment : '', $objCustomerVote->getFloorplanName(), $objCustomerVote->getComment() ] );
				}
			}
		}

		$arrobjCustomerUnitVotes = ( array ) \Psi\Eos\Entrata\CCustomerVotes::createService()->fetchCustomerVotesWithUnitNumberByCustomerIdByPropertyIdByCid( $intCustomerId, $this->getPropertyId(), $this->getCid(), $this->m_objDatabase );

		if( true == valArr( $arrobjCustomerUnitVotes ) ) {
			$intLikedCount = 0;
			$intDisLikedCount = 0;
			$intCommentCount = 0;

			foreach( $arrobjCustomerUnitVotes as $objCustomerVote ) {
				if( CCustomerVote::LIKE == $objCustomerVote->getVote() ) {
					++$intLikedCount;
					$strUnitLiked .= __( '<tr style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
													<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
													<td style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">Unit: {%s, 1} </td>
													<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
												</tr>', [ ( false == valStr( $strFloorplanLiked ) && $intLikedCount == 1 ) ? $strLiked : '', $objCustomerVote->getUnitNumber(), $objCustomerVote->getComment() ?? '-' ] );
				} else if( CCustomerVote::DISLIKE == $objCustomerVote->getVote() ) {
					++$intDisLikedCount;
					$strUnitDisLiked .= __( '<tr style="background: #F6F6F6;margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;border-bottom: 1px solid #E6E7E8;">
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
														<td style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">Unit: {%s, 1}</td>
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
											        </tr>', [ ( false == valStr( $strFloorplanDisLiked ) && $intDisLikedCount == 1 ) ? $strDisliked : '', $objCustomerVote->getUnitNumber(), $objCustomerVote->getComment() ?? '-' ] );
				} else if( CCustomerVote::COMMENT == $objCustomerVote->getVote() ) {
					++$intCommentCount;
					$strUnitComment .= __( '<tr style="margin: 0;padding: 0;border: 0;font-size: 100%;font: inherit;vertical-align: baseline;border-bottom: 1px solid #E6E7E8;">
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #414042;font-weight: 500;">{%s, 0}</td>
														<td style="font-weight: bold;margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">Unit: {%s, 1}</td>
														<td style="margin: 0;padding: 8px 6px;border: 0;font-size: 10px;font: inherit;vertical-align: baseline;text-align: left;color: #7a7a7a;">{%s, 2}</td>
													</tr>', [ ( false == valStr( $strFloorplanComment ) && $intCommentCount == 1 ) ? $strComment : '', $objCustomerVote->getUnitNumber(), $objCustomerVote->getComment() ] );
				}
			}
		}

		return $strFloorplansAndUnitsDiscussed . $strFloorplanLiked . $strUnitLiked . $strFloorplanDisLiked . $strUnitDisLiked . $strFloorplanComment . $strUnitComment . '</table>';
	}

	public function getIsIdentificationDocumentsVerified() {

		$arrintCustomerIds = [];
		$arrobjCustomers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $this->m_objDatabase, false );

		if( true == valArr( $arrobjCustomers ) ) {
			$arrintCustomerIds = array_map( function ( $objCustomer ) {
				if( true == valObj( $objCustomer, 'CCustomer' ) ) {
					return $objCustomer->getId();
				}
			}, $arrobjCustomers );
		}

		$arrmixPendingFiles = ( array ) CFiles::createService()->fetchPendingIdentificationFilesDetailsByCustomerIdsByCid( $arrintCustomerIds, $this->getCid(), $this->m_objDatabase );

		if( true == valArr( $arrmixPendingFiles ) ) {
			return false;
		}

		return true;
	}

	public function getEsignSignatureRefusalReason() {
		return $this->m_strEsignSignatureRefusalReason;
	}

	public function setEsignSignatureRefusalReason( $strEsignSignatureRefusalReason ) {
		$this->m_strEsignSignatureRefusalReason = $strEsignSignatureRefusalReason;
	}

	public function getContactlessTourAuthenticationCode() {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		$strContent 		  = '';

		if( false == valArrKeyExists( $arrmixRequiredParameters, 'event_id' ) ) {
			return $strContent;
		}

		try {
			$objEntrataMationService = new \Psi\Libraries\Entratamation\CEntratamationServiceLibrary( $this );
			$objEntrataMationService->setCid( $this->getCid() );
			$objEntrataMationService->addAdditional( 'self_guided_tour_event_id', $arrmixRequiredParameters['event_id'] );
			$objEntrataMationService->setMethod( 'getVoiceAuthenticationCodes' );
			$objEntrataMationService->sendRequest();
			$arrmixResult = $objEntrataMationService->getResult();

			if( true == valArrKeyExists( $arrmixResult, 'response' ) && 200 == $arrmixResult['response']['code'] && true == array_key_exists( 'result', $arrmixResult['response'] ) && true === $arrmixResult['response']['result']['status'] && true == valArr( $arrmixResult['response']['result']['voice_authentication_codes'] ) ) {
				$arrmixVoiceAuthCodes = $arrmixResult['response']['result']['voice_authentication_codes'];
				if( true == valArrKeyExists( $arrmixRequiredParameters, 'from_sms' ) ) {
					foreach( $arrmixVoiceAuthCodes as $strUnit => $arrmixVoiceAuthCode ) {
						$strContent   .= __( "#Unit:{%s, 0} \n Voice Code:{%s, 1}", [ $strUnit, $arrmixVoiceAuthCode['voice_authentication_code'] ] );
						if( true == array_key_exists( 'door_access_code', $arrmixVoiceAuthCode ) && true == valId( $arrmixVoiceAuthCode['door_access_code'] ) ) {
							$strContent   .= __( "\n Door Code:{%s, 0}", $arrmixVoiceAuthCode['door_access_code'] );
						}
						$strContent .= "\n \n";
					}
					return $strContent;
				}

				$strContent = __( '<table class="margin40-top" border = "1" width="100%" cellspacing="0" cellpadding="0"><tr><td>#Unit:</td><td>Voice Code:</td><td>Door Code:</td></tr>' );
				foreach( $arrmixVoiceAuthCodes as $strUnit => $arrmixVoiceAuthCode ) {
					$strContent .= __( '<tr><td>{%s, 0} </td> <td> {%s, 1} </td><td>', [ $strUnit, $arrmixVoiceAuthCode['voice_authentication_code'] ] );
					if( true == array_key_exists( 'door_access_code', $arrmixVoiceAuthCode ) && true == valId( $arrmixVoiceAuthCode['door_access_code'] ) ) {
						$strContent .= __( ' {%s, 0}', $arrmixVoiceAuthCode['door_access_code'] );
					}
					$strContent .= '</td></tr>';
				}
				$strContent .= '</table>';
			}
		} catch( Exception $objException ) {
			$strContent = '';
		}

		return $strContent;
	}

	public function getIsIdentificationDocumentsRejected() {
		$arrintCustomerIds = [];
		$arrobjCustomers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $this->m_objDatabase );
		if( true == valArr( $arrobjCustomers ) ) {
			$arrintCustomerIds = array_map( function ( $objCustomer ) {
				if( true == valObj( $objCustomer, 'CCustomer' ) ) {
					return $objCustomer->getId();
				}
			}, $arrobjCustomers );

		}
		$arrmixRejectedFiles = ( array ) CFiles::createService()->fetchRejectedIdentificationFilesDetailsByCustomerIdsByCid( $arrintCustomerIds, $this->getCid(), $this->m_objDatabase );
			if( true == valArr( $arrmixRejectedFiles ) ) {
				return false;
			}
        return true;
	}

	public function generateRVWebsiteUrl( $objDatabase ) {
		$arrmixRequiredParameters = $this->getRequiredParameters();
		$boolIsUseLink = isset( $arrmixRequiredParameters['ar_payment_id'] );

		$strSecureBaseName       = NULL;
		$strProspectPortalSuffix = '.prospectportal.com';

		if( true == defined( CONFIG_PROSPECT_PORTAL_SUFFIX ) ) {
			$strProspectPortalSuffix = CONFIG_PROSPECT_PORTAL_SUFFIX;
		} elseif( false == is_null( CConfig::get( 'prospect_portal_suffix' ) ) ) {
			$strProspectPortalSuffix = CConfig::get( 'prospect_portal_suffix' );
		}

		if( false == is_null( $this->getWebsiteId() ) ) {
			$objWebsite = $this->getOrFetchWebsite( $objDatabase, false );

			if( true == valObj( $objWebsite, 'CWebsite' ) && true == is_null( $objWebsite->getDeletedOn() ) && 1 != $objWebsite->getIsDisabled() ) {
				$arrobjProperties = ( array ) $objWebsite->fetchProperties( $objDatabase );
				$arrobjRekeyProperties = rekeyObjects( 'PropertyId', $arrobjProperties );

				if( true == valArr( $arrobjProperties ) && ( true == array_key_exists( $this->getPropertyId(), $arrobjProperties ) || true == array_key_exists( $this->getPropertyId(), $arrobjRekeyProperties ) ) ) {

					if( 'production' == CConfig::get( 'environment' ) ) {
						$objWebsiteDomain = $objWebsite->fetchPrimaryWebsiteDomain( $objDatabase );

						if( true == valObj( $objWebsiteDomain, 'CWebsiteDomain' ) && true == $objWebsiteDomain->getIsSecure() ) {
							$arrstrHostTmp 	= explode( '.', str_replace( 'www.', '', $objWebsiteDomain->getWebsiteDomain() ) );
							if( true == $boolIsUseLink ) {
								$strSecureBaseName = ( 2 == count( $arrstrHostTmp ) ? 'www.' : '' ) . $objWebsiteDomain->getWebsiteDomain();
							} else {
								$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . ( 2 == count( $arrstrHostTmp ) ? 'www.' : '' ) . $objWebsiteDomain->getWebsiteDomain() . '/';
							}
							return $strSecureBaseName;
						}
					}

					if( true == $boolIsUseLink ) {
						$strSecureBaseName = $objWebsite->getSubDomain() . $strProspectPortalSuffix;
					} else {
						$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . $objWebsite->getSubDomain() . $strProspectPortalSuffix . '/';
					}
					return $strSecureBaseName;
				}
			}
		}

		$objProperty = $this->getOrFetchProperty( $objDatabase );

		// CKT : Below code added to fetch website url of parent property when
		// the application is holding child property id( i.e. child property unit is selected )

		if( true == is_null( $strSecureBaseName ) && false == is_null( $objProperty->getPropertyId() ) ) {
			$objWebsite  = NULL;
			$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objProperty->getPropertyId(), $objProperty->getCid(), $objDatabase );

			if( true == valObj( $objProperty, 'CProperty' ) ) {

				if( false == is_null( $this->getWebsiteId() ) ) {
					$objWebsite = $this->getOrFetchWebsite( $objDatabase );
				}

				// CKT : First verify whether the parent property exists within
				// the website from where the application was generated
				if( true == valObj( $objWebsite, 'CWebsite' ) && true == is_null( $objWebsite->getDeletedOn() ) && 1 != $objWebsite->getIsDisabled() ) {
					$arrobjProperties = $objWebsite->fetchProperties( $objDatabase );
					$arrobjRekeyProperties = rekeyObjects( 'PropertyId', $arrobjProperties );

					if( true == valArr( $arrobjProperties ) && true == array_key_exists( $objProperty->getId(), $arrobjProperties ) ) {
						if( true == $boolIsUseLink ) {
							$strSecureBaseName = $objWebsite->getSubDomain() . $strProspectPortalSuffix;
						} else {
							$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . $objWebsite->getSubDomain() . $strProspectPortalSuffix . '/';
						}
					}
				}

				// CKT : If the property is not longer associated to the website
				// used for creating application then fetch the default website
				if( true == is_null( $strSecureBaseName ) ) {
					$objWebsite = $objProperty->fetchDefaultNonLobbyTypeWebsite( $objDatabase, true );

					if( true == valObj( $objWebsite, 'CWebsite' ) ) {
						if( true == $boolIsUseLink ) {
							$strSecureBaseName = $objWebsite->getSubDomain() . $strProspectPortalSuffix;
						} else {
							$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . $objWebsite->getSubDomain() . $strProspectPortalSuffix . '/';
						}
					}
				}
			}
		}

		if( true == is_null( $strSecureBaseName ) && true == valObj( $objProperty, 'CProperty' ) ) {
			$objWebsite = $objProperty->fetchDefaultNonLobbyTypeWebsite( $objDatabase, true );

			if( true == valObj( $objWebsite, 'CWebsite' ) ) {
				if( true == $boolIsUseLink ) {
					$strSecureBaseName = $objWebsite->getSubDomain() . $strProspectPortalSuffix;
				} else {
					$strSecureBaseName = CONFIG_SECURE_HOST_PREFIX . $objWebsite->getSubDomain() . $strProspectPortalSuffix . '/';
				}
			}
		}

		return $strSecureBaseName;
	}

	public function sendApplicationInvitationEmail( $strSystemMessageKey, $intSystemEmailTypeId, $objCompanyUser ) {
		$objClient 	 = $this->getOrFetchClient( $this->m_objDatabase );
		$objProperty = $this->getOrFetchProperty( $this->m_objDatabase );
		$intCompanyUserId = SYSTEM_USER_ID;

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$intCompanyUserId = $objCompanyUser->getId();
		}

		$objSystemMessageLibrary = new CSystemMessageLibrary();
		$objSystemMessageLibrary->setClient( $objClient );
		$objSystemMessageLibrary->setDatabase( $this->m_objDatabase );
		$objSystemMessageLibrary->setPropertyId( $objProperty->getId() );

		$objSystemMessageLibrary->setObjectStorageGateway( $this->getObjectStorageGateway() );

		$objSystemMessageLibrary->setSystemEmailTypeId( $intSystemEmailTypeId );

		$objSystemMessageLibrary->setCompanyUserId( $intCompanyUserId );
		$objSystemMessageLibrary->setSystemMessageKey( $strSystemMessageKey );
		$objSystemMessageLibrary->setSystemMessageAudienceIds( [ \CSystemMessageAudience::LEAD ] );

		$objSystemMessageLibrary->setApplication( $this );
		$objSystemMessageLibrary->setProperty( $objProperty );
		$objSystemMessageLibrary->setIsLeadDirectSendToConsumer( true );

		return $objSystemMessageLibrary->execute();
	}

	public function createSendConsumerReportEvent( $intApplicantId, $objDatabase ) {
		$objStdScreeningEventData 	                    = new stdClass();
		$objStdScreeningEventData->intEventTypeId       = CEventType::CONSUMER_REPORT_WI_CA;
		$objStdScreeningEventData->objDatabase			= $objDatabase;
		$objStdScreeningEventData->objApplication 		= $this;
		$objStdScreeningEventData->intCurrentUserId     = CCompanyUser::PROSPECT_PORTAL;
		$objStdScreeningEventData->intApplicantId       = $intApplicantId;

		$objResidentVerifyLog                           = new CResidentVerifyActivityLogHandler();
		$objResidentVerifyLog->intialize( $objStdScreeningEventData );
		$objResidentVerifyLog->logApplicationActivity();
	}

	public function getParentPropertyApplicationPreferencesByKey( $strKey, $objDatabase ) {
		$objProperty = $this->getOrFetchProperty();
		if( ( false == valObj( $objProperty, 'CProperty' ) ) || ( false == valId( $objProperty->getPropertyId() ) ) || ( false == valId( $this->getCompanyApplicationId() ) ) ) {
			return;
		}

		return CPropertyApplicationPreferences::fetchPropertyApplicationPreferenceByKeyByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $strKey, $this->getCompanyApplicationId(), $objProperty->getPropertyId(), CApplicationPreferenceType::TYPE_GLOBAL, $this->getCid(), $objDatabase );
	}
}
?>