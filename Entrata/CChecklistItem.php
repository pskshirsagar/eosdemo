<?php

class CChecklistItem extends CBaseChecklistItem {

	use TEosStoredObject;

	protected $m_intCompletedBy;
	protected $m_intChecklistTriggerId;
	protected $m_intChecklistId;
	protected $m_intLeaseChecklistId;
	protected $m_intAjaxUploadedChecklistItemId;

	protected $m_strCompletedOn;
	protected $m_strAction;
	protected $m_strUsername;
	protected $m_strTitle;
	protected $m_strConvertedVideoFileName;

	protected $m_arrmixGatewayRequest;

	const CHARGE_SECOND_MONTH_RENT_IF_MOVE_IN_AFTER = 'charge_next_month_rent_if_move_in_after';

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['completed_by'] ) && $boolDirectSet ) $this->m_intcompletedBy = trim( $arrmixValues['completed_by'] );
		elseif( isset( $arrmixValues['completed_by'] ) ) $this->setCompletedBy( $arrmixValues['completed_by'] );
		if( isset( $arrmixValues['completed_on'] ) && $boolDirectSet ) $this->m_strCompletedOn = trim( $arrmixValues['completed_on'] );
		elseif( isset( $arrmixValues['completed_on'] ) ) $this->setCompletedOn( $arrmixValues['completed_on'] );
		if( isset( $arrmixValues['action'] ) && $boolDirectSet ) $this->m_strAction = trim( $arrmixValues['action'] );
		elseif( isset( $arrmixValues['action'] ) ) $this->setAction( $arrmixValues['action'] );
		if( isset( $arrmixValues['checklist_trigger_id'] ) && $boolDirectSet ) $this->m_intChecklistTriggerId = trim( $arrmixValues['checklist_trigger_id'] );
		elseif( isset( $arrmixValues['checklist_trigger_id'] ) ) $this->setChecklistTriggerId( $arrmixValues['checklist_trigger_id'] );
		if( isset( $arrmixValues['checklist_id'] ) && $boolDirectSet ) $this->m_intChecklistId = trim( $arrmixValues['checklist_id'] );
		elseif( isset( $arrmixValues['checklist_id'] ) ) $this->setChecklistId( $arrmixValues['checklist_id'] );
		if( isset( $arrmixValues['username'] ) && $boolDirectSet ) $this->m_strUsername = trim( $arrmixValues['username'] );
		elseif( isset( $arrmixValues['username'] ) ) $this->setUsername( $arrmixValues['username'] );
		if( isset( $arrmixValues['lease_checklist_id'] ) && $boolDirectSet ) $this->m_intLeaseChecklistId = trim( $arrmixValues['lease_checklist_id'] );
		elseif( isset( $arrmixValues['lease_checklist_id'] ) ) $this->setLeaseChecklistId( $arrmixValues['lease_checklist_id'] );
		if( isset( $arrmixValues['is_system'] ) && $boolDirectSet ) $this->m_boolIsSystem = trim( stripcslashes( $arrmixValues['is_system'] ) );
		elseif( isset( $arrmixValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_system'] ) : $arrmixValues['is_system'] );
	}

	public function setIsSystem( $boolIsSystem ) {
		$this->m_boolIsSystem = CStrings::strToBool( $boolIsSystem );
	}

	public function getIsSystem() {
		return $this->m_boolIsSystem;
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->m_strCompletedOn = CStrings::strTrimDef( $strCompletedOn, -1, NULL, true );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->m_intCompletedBy = CStrings::strToIntDef( $intCompletedBy, NULL, false );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function setAction( $strAction ) {
		$this->m_strAction = CStrings::strTrimDef( $strAction, -1, NULL, true );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function setChecklistTriggerId( $intChecklistTriggerId ) {
		$this->m_intChecklistTriggerId = CStrings::strToIntDef( $intChecklistTriggerId, NULL, false );
	}

	public function getChecklistTriggerId() {
		return $this->m_intChecklistTriggerId;
	}

	public function setChecklistId( $intChecklistId ) {
		$this->m_intChecklistId = CStrings::strToIntDef( $intChecklistId, NULL, false );
	}

	public function getChecklistId() {
		return $this->m_intChecklistId;
	}

	public function setLeaseChecklistId( $intLeaseChecklistId ) {
		$this->m_intLeaseChecklistId = CStrings::strToIntDef( $intLeaseChecklistId, NULL, false );
	}

	public function getLeaseChecklistId() {
		return $this->m_intLeaseChecklistId;
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = CStrings::strTrimDef( $strUsername, -1, NULL, true );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function setAjaxUploadedChecklistItemId( $intAjaxUploadedChecklistItemId ) {
		$this->m_intAjaxUploadedChecklistItemId = $intAjaxUploadedChecklistItemId;
	}

	public function getAjaxUploadedChecklistItemId() {
		return $this->m_intAjaxUploadedChecklistItemId;
	}

	public function getGatewayRequest() {
		return $this->m_arrmixGatewayRequest;
	}

	public function setConvertedVideoFileName( $strConvertedVideoFileName ) {
		$this->m_strConvertedVideoFileName = $strConvertedVideoFileName;
	}

	public function getConvertedVideoFileName() {
		return $this->m_strConvertedVideoFileName;
	}

	/*
	 * Fetch Functions
	 */

	public function fetchLeaseChecklistItemByLeaseChecklistIdByCid( $intLeaseChecklistId, $objDatabase ) {
		return CLeaseChecklistItems::fetchLeaseChecklistItemByLeaseChecklistIdByChecklistItemIdByCid( $intLeaseChecklistId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function valChecklistItemTypeId( $arrmixChecklistItemTypes = NULL ) {
		$boolIsValid = true;
		if( false == valId( $this->getChecklistItemTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_action', __( 'Checklist task action is required. ' ) ) );

		} elseif( true == is_null( $this->getDeletedBy() ) && true == is_null( $this->getDeletedOn() ) && true == valArr( $arrmixChecklistItemTypes ) && true == array_key_exists( $this->getChecklistItemTypeId(), $arrmixChecklistItemTypes ) ) {

			$strChecklistTypeTriggerIds = \Psi\CStringService::singleton()->substr( $arrmixChecklistItemTypes[$this->getChecklistItemTypeId()]['checklist_trigger_ids'], 1, -1 );
			$arrintChecklistTypeValidTriggerIds = ( array ) ( true == valStr( $strChecklistTypeTriggerIds ) ) ? explode( ',', $strChecklistTypeTriggerIds ) : [];

			if( true == valArr( $arrintChecklistTypeValidTriggerIds ) && false == in_array( $this->getChecklistTriggerId(), $arrintChecklistTypeValidTriggerIds ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'checklist_item_type_id', __( 'You are trying to associate wrong checklist item type for this checklist. ' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valChecklistItemTypeOptionId( $arrmixChecklistItemTypeOptions = NULL ) {
		$boolIsValid = true;
		if( false == valId( $this->getChecklistItemTypeOptionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'item_type_option_id', __( 'Checklist Task sub option is required. ' ) ) );
		} elseif( true == is_null( $this->getDeletedBy() ) && true == is_null( $this->getDeletedOn() ) && true == valArr( $arrmixChecklistItemTypeOptions ) ) {

			$arrmixSelectedChecklistItemTypesOptions = rekeyArray( 'id', $arrmixChecklistItemTypeOptions[$this->getChecklistItemTypeId()] );
			if( true == valArr( $arrmixSelectedChecklistItemTypesOptions ) && true == array_key_exists( $this->getChecklistItemTypeOptionId(), $arrmixSelectedChecklistItemTypesOptions ) ) {
				$strChecklistTypeOptionsTriggerIds         = \Psi\CStringService::singleton()->substr( $arrmixSelectedChecklistItemTypesOptions[$this->getChecklistItemTypeOptionId()]['checklist_trigger_ids'], 1, -1 );
				$arrintChecklistTypeOptionsValidTriggerIds = ( true == valStr( $strChecklistTypeOptionsTriggerIds ) ) ? explode( ',', $strChecklistTypeOptionsTriggerIds ) : [];
			}

			if( true == valArr( $arrintChecklistTypeOptionsValidTriggerIds ) && false == in_array( $this->getChecklistTriggerId(), $arrintChecklistTypeOptionsValidTriggerIds ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'item_type_option_id', __( 'You are trying to associate wrong checklist item type for this checklist. ' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valDocumentId() {
		$boolIsValid = true;
		if( true == is_null( $this->getDocumentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_id', __( 'Please select atleast one Document option. ' ) ) );
		}
		return $boolIsValid;
	}

	public function valItemTitle() {

		$boolIsValid = true;
		if( true == is_null( $this->getItemTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_title', __( 'Checklist task title is required. ' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixChecklistItemTypes = NULL, $arrmixChecklistItemTypeOptions = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valItemTitle();
				$boolIsValid &= $this->valChecklistItemTypeId( $arrmixChecklistItemTypes );

				if( true == in_array( $this->getChecklistItemTypeId(), [ CChecklistItemType::ACCEPT_PAYMENT, CChecklistItemType::INFORMATION_COLLECTED, CChecklistItemType::MARK_COMPLETE ] ) ) {
					$boolIsValid &= $this->valChecklistItemTypeOptionId( $arrmixChecklistItemTypeOptions );
				}

				if( CChecklistItemType::GENERATE_MERGE_FIELD_DOCUMENT == $this->getChecklistItemTypeId() ) {
					$boolIsValid &= $this->valDocumentId();
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valItemTitle();
				$boolIsValid &= $this->valChecklistItemTypeId();

				if( true == in_array( $this->getChecklistItemTypeId(), [ CChecklistItemType::ACCEPT_PAYMENT, CChecklistItemType::INFORMATION_COLLECTED, CChecklistItemType::MARK_COMPLETE ] ) ) {
					$boolIsValid &= $this->valChecklistItemTypeOptionId();
				}
				if( CChecklistItemType::GENERATE_MERGE_FIELD_DOCUMENT == $this->getChecklistItemTypeId() ) {
					$boolIsValid &= $this->valDocumentId();
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Other Functions
	 */

    public function createLeaseChecklistItem() {

        $objLeaseChecklistItem = new CLeaseChecklistItem();
        $objLeaseChecklistItem->setCid( $this->getCid() );
        $objLeaseChecklistItem->setChecklistItemId( $this->getId() );
        return $objLeaseChecklistItem;

    }

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsHardDelete = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	/*
	 * Video checklist Item related functions
	 */

	public function validateFileData() {

		// If file input tag is not available
		if( false == isset( $_FILES['checklist_video_file'] ) ) return true;

		if( true == empty( $_FILES ) && empty( $_POST ) && isset( $_SERVER['REQUEST_METHOD'] ) && 'post' == \Psi\CStringService::singleton()->strtolower( $_SERVER['REQUEST_METHOD'] ) ) {
			$strPostMaxSize = ini_get( 'post_max_size' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'video_size_error', __( 'File size should not exceed more than {%d, 0} B.', [ $strPostMaxSize ] ) ) );
			return false;
		}

		$arrstrExtensions = [ 'mov', 'mp4', 'avi', 'wmv' ];

		$arrstrFileInfo   = pathinfo( $_FILES['checklist_video_file']['name'] );
		$strExtension 	  = ( isset( $arrstrFileInfo['extension'] ) ) ? \Psi\CStringService::singleton()->strtolower( $arrstrFileInfo['extension'] ) : NULL;

		if( false == valStr( $strExtension ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'video_missing_error', __( ' Video file must be uploaded. ' ) ) );
			return false;
		}

		if( false == in_array( $strExtension, $arrstrExtensions ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'video_extension_error', __( ' Video file must be one of the supported formats: {%s, 0}', [ \Psi\CStringService::singleton()->strtoupper( implode( ', ', $arrstrExtensions ) ) ] ) ) );
			return false;
		}

		return true;
	}

	public function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		return 'checklist_docs' . '/' . $this->getCid() . '/' . date( 'Y/m/d' ) . '/' . $this->getId() . '/' . ( true == valStr( $this->getConvertedVideoFileName() ) ? $this->getConvertedVideoFileName() : $_FILES['checklist_video_file']['name'] );
	}

	protected function calcStorageContainer() {

		return CONFIG_AWS_CLIENTS_BUCKET_NAME;

	}

	public function saveFile( $intCompanyUserId, $objStorageGateway, CDatabase $objDatabase ) {
		$arrmixRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $_FILES['checklist_video_file']['tmp_name'] ) ] );
		$objResponse = $objStorageGateway->putObject( $arrmixRequest );
		if( false == $objResponse->hasErrors() ) {
			$this->setObjectStorageGatewayResponse( $objResponse )->insertOrUpdateStoredObject( $intCompanyUserId, $objDatabase );
		}

		return $objResponse;
	}

	public function deleteObject( $objObjectStorageGateway, $intCompanyUserId, $objDatabase ) {

		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			return false;
		}

		$objStoredObject = $this->fetchStoredObject( $objDatabase );

		if( false == valId( $objStoredObject->getId() ) ) {
			return true;
		}

		$this->m_arrmixGatewayRequest = $objStoredObject->createGatewayRequest();

		if( false == $this->deleteStoredObject( $intCompanyUserId, $objDatabase, $objStoredObject ) ) {
			return false;
		}

		return true;
	}

	public function deleteVideoFile( $objObjectStorageGateway ) {
		if( false == valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			return true;
		}

		$objDeleteObjectResponse = $objObjectStorageGateway->deleteObject( $this->m_arrmixGatewayRequest );

		if( true == $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Failed to delete actual video file for checklist item id {%d, 0}, due to the error: {%s, 1}', [ $this->getId(), $objDeleteObjectResponse->getMessages()[0] ] ) ) );
			return false;
		}

		return true;
	}

	public function setTitle( $objDatabase ) {
		$objChecklistItem = clone $this;
		if( true == valId( $this->getAjaxUploadedChecklistItemId() ) ) {
			$objChecklistItem->setId( $this->getAjaxUploadedChecklistItemId() );
		}

		$objStoredObject = $objChecklistItem->fetchStoredObject( $objDatabase );

		if( true == valId( $objStoredObject->getId() ) ) {

			/**
			 * To Do: We will have to modify this function to use the stored_objects.title once the Lease Execution team start setting it.
			 * Till time we are setting this title by getting the name of file from the stored_objects.key
			 */

			$arrmixStoredObjectKeyData = explode( '/', $objStoredObject->getKey() );
			$this->m_strTitle = ( true == valArr( $arrmixStoredObjectKeyData ) ) ? end( $arrmixStoredObjectKeyData ) : '';
		}
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

}
?>