<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetGlAccountMonths
 * Do not add any new functions to this class.
 */

class CBudgetGlAccountMonths extends CBaseBudgetGlAccountMonths {

	public static function fetchBudgetGlAccountMonthsByCidByBudgetId( $intCid, $intBudgetId, $objBudgetFilter, $objClientDatabase, $intPropertyId = NULL, $boolIsViewCurrent = false ) {

		$strCondition		  = '';
		$strGlDetailCondition = '';

		if( true == valObj( $objBudgetFilter, 'CBudgetsFilter' ) ) {

			if( true == valStr( $objBudgetFilter->getStartDate() ) && true == valStr( $objBudgetFilter->getEndDate() ) ) {

				$strStartDate	= $objBudgetFilter->getStartDate();
				$strEndDate 	= $objBudgetFilter->getEndDate();

				$strCondition .= ' AND bgam.post_month >= \'' . $strStartDate . '\' AND bgam.post_month <= \'' . $strEndDate . '\' ';
			}

			if( true == valStr( $objBudgetFilter->getMinAccountNumber() ) && true == valStr( $objBudgetFilter->getMaxAccountNumber() ) ) {
				$strAccountNumberCondition = " AND ( NULLIF ( regexp_replace ( gat.account_number, E'\\\\D', '', 'g' ), '' )::NUMERIC BETWEEN " . sqlStrImplode( [ \Psi\CStringService::singleton()->strtolower( $objBudgetFilter->getMinAccountNumber() ) ] ) . '::NUMERIC AND ' . sqlStrImplode( [ \Psi\CStringService::singleton()->strtolower( $objBudgetFilter->getMaxAccountNumber() ) ] ) . '::NUMERIC ) ';
				$strCondition              .= $strAccountNumberCondition;
				$strGlDetailCondition      = $strAccountNumberCondition;
			}
		}

		if( true == valId( $intPropertyId ) ) {
			$strGlDetailCondition .= ' AND gd.property_id = ' . ( int ) $intPropertyId;
		}

		$strSql = 'SELECT
						gat.gl_account_type_id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						bgam.id,
						bgam.cid,
						bgam.budget_id,
						bgam.property_id,
						bgam.gl_account_id,
						bgam.gl_dimension_id,
						gld.name AS gl_dimension_name,
						bgam.post_month,
						bgam.amount
					FROM
						budget_gl_account_months bgam
						JOIN gl_account_trees gat ON ( gat.cid = bgam.cid AND gat.gl_account_id = bgam.gl_account_id AND gat.is_default = 1 )
						LEFT JOIN gl_dimensions gld ON ( gld.cid = bgam.cid AND gld.id = bgam.gl_dimension_id AND gld.is_published = 1 )
					WHERE
						bgam.cid = ' . ( int ) $intCid . '
						AND bgam.budget_id = ' . ( int ) $intBudgetId .
						$strCondition;

		if( true == $boolIsViewCurrent ) {

			$strDefaultBudgetSql = '
				SELECT
					DISTINCT ON ( gat.gl_account_type_id, gat.formatted_account_number, gat.name ) gat.gl_account_type_id,
					gat.formatted_account_number AS account_number,
					gat.name AS account_name,
					gd.id,
					gd.cid,
					NULL AS budget_id,
					gd.property_id,
					( CASE
						WHEN pgs.is_cash_basis <> 0 THEN gd.cash_gl_account_id 
						ELSE gd.accrual_gl_account_id 
					 END ) AS gl_account_id,
					 gd.gl_dimension_id,
					 gld.name AS gl_dimension_name,
					NULL AS post_month,
					0 AS amount
				FROM
					gl_details gd
					JOIN property_gl_settings pgs ON ( pgs.cid = gd.cid AND pgs.property_id = gd.property_id )
					JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gat.is_default = 1
													AND gat.gl_account_id = COALESCE( ( CASE
																							WHEN pgs.is_cash_basis <> 0 THEN gd.cash_gl_account_id 
																							ELSE gd.accrual_gl_account_id 
																							END ), 0 )
																							
												)
					LEFT JOIN gl_dimensions gld ON ( gld.cid = gd.cid AND gld.id = gd.gl_dimension_id AND gld.is_published = 1 )
				WHERE
					gd.cid = ' . ( int ) $intCid .
					$strGlDetailCondition;

			$strSql = 'SELECT * FROM
						(
							' . $strSql . '
							UNION ALL
							' . $strDefaultBudgetSql . '
						) inner_query';
		}

		$strSql .= ' ORDER BY
							account_number,
							id';

		return self::fetchBudgetGlAccountMonths( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByCidByBudgetIdByPropertyGroupIds( $intCid, $intBudgetId, $arrintPropertyGroupIds, $objClientDatabase, $objBudgetsFilter ) {

		if( false == is_numeric( $intBudgetId ) && false == valArr( $arrintPropertyGroupIds ) ) {
			return NULL;
		}

		$strCondition = '';

		if( true == valObj( $objBudgetsFilter, 'CBudgetsFilter' ) && true == valId( $objBudgetsFilter->getGlAccountTypeId() ) ) {

			$strCondition .= ' AND gat.gl_account_type_id = ' . ( int ) $objBudgetsFilter->getGlAccountTypeId();

			if( true == valObj( $objBudgetsFilter, 'CBudgetsFilter' ) && true == valStr( $objBudgetsFilter->getMinAccountNumber() ) && true == valStr( $objBudgetsFilter->getMaxAccountNumber() ) ) {

				$strCondition .= " AND ( NULLIF ( regexp_replace ( gat.account_number, E'\\\\D', '', 'g' ), '' )::NUMERIC BETWEEN " . sqlStrImplode( [ \Psi\CStringService::singleton()->strtolower( $objBudgetsFilter->getMinAccountNumber() ) ] ) . '::NUMERIC AND ' . sqlStrImplode( [ \Psi\CStringService::singleton()->strtolower( $objBudgetsFilter->getMaxAccountNumber() ) ] ) . '::NUMERIC ) ';
			}
		}

		$strSql = 'SELECT
						gat.gl_account_type_id,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						gld.name AS gl_dimension_name,
						bgam.*
					FROM
						budget_gl_account_months bgam
						JOIN gl_account_trees gat ON ( bgam.gl_account_id = gat.gl_account_id AND bgam.cid = gat.cid AND gat.is_default = 1 )
						LEFT JOIN gl_account_properties gap ON ( gat.gl_account_id = gap.gl_account_id AND gat.cid = gap.cid )
						LEFT JOIN gl_dimensions gld ON ( gld.cid = bgam.cid AND gld.id = bgam.gl_dimension_id )
					WHERE
						bgam.cid = ' . ( int ) $intCid . '
						AND	bgam.budget_id = ' . ( int ) $intBudgetId . '
						AND ( gap.gl_account_id IS NULL OR gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ) )
						AND gat.disabled_by IS NULL
						' . $strCondition . '
					ORDER BY
						bgam.id';

		return self::fetchBudgetGlAccountMonths( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByCidByBudgetIdByGlAccountIdsByPostMonths( $intCid, $intBudgetId, $arrintGlAccountIds, $arrintGlAccountMonths, $objClientDatabase ) {

		$arrintDimensionIds = array_filter( array_keys( $arrintGlAccountIds ), 'valId' );

		if( true == valArr( $arrintDimensionIds ) ) {
			$strCondition = 'AND bgam.gl_dimension_id IN ( \'' . implode( '\',\'', $arrintDimensionIds ) . '\' )';
		} else {
			$strCondition = 'AND bgam.gl_dimension_id IS NULL ';
		}

		$strSql = 'SELECT
						*
					FROM
						budget_gl_account_months bgam
					WHERE
						bgam.cid = ' . ( int ) $intCid . '
						AND	bgam.budget_id = ' . ( int ) $intBudgetId . '
						AND bgam.gl_account_id IN ( \'' . implode( '\',\'', $arrintGlAccountIds ) . '\' )
						' . $strCondition . ' 
						AND bgam.post_month IN ( \'' . implode( '\',\'', $arrintGlAccountMonths ) . '\' )';

		return self::fetchBudgetGlAccountMonths( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetGlAccountMonthByGlAccountIdByPropertyIdByPostMonthByCid( $intGlAccountId, $intPropertyId, $strPostMonth, $intCid, $objClientDatabase ) {

				$strSql = 'SELECT
						SUM( bgam.amount ) AS budget_amount
						FROM
						budget_gl_account_months bgam,budgets b
						WHERE
						bgam.budget_id = b.id
						AND bgam.cid = b.cid
						AND b.is_default = 1
						AND bgam.cid = ' . ( int ) $intCid . '
						AND bgam.property_id = ' . ( int ) $intPropertyId . '
						AND bgam.gl_account_id = ' . ( int ) $intGlAccountId . '
						AND to_char( bgam.post_month,\'YYYY-MM\') = \'' . date( 'Y-m', strtotime( $strPostMonth ) ) . '\'';

				 return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByCidByBudgetIdByPostMonths( $intCid, $intBudgetId, $arrstrPostMonths, $objClientDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						budget_gl_account_months bgam
					WHERE
						bgam.cid = ' . ( int ) $intCid . '
						AND	bgam.budget_id = ' . ( int ) $intBudgetId . '
						AND bgam.post_month IN ( \'' . implode( '\',\'', $arrstrPostMonths ) . '\' )';

		return self::fetchBudgetGlAccountMonths( $strSql, $objClientDatabase );
	}

	public static function fetchBudgetGlAccountMonthsByPropertyIdsByGlAccountIdByBudgetStatusTypeIdByCid( $arrintPropertyIds, $intGlAccountId, $intBudgetStatusTypeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bgam.*
					FROM
						budget_gl_account_months bgam
						JOIN budgets b ON ( bgam.budget_id = b.id AND bgam.cid = b.cid )
					WHERE
						bgam.cid = ' . ( int ) $intCid . '
						AND bgam.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND bgam.gl_account_id = ' . ( int ) $intGlAccountId . '
						AND b.budget_status_type_id = ' . ( int ) $intBudgetStatusTypeId;

		return self::fetchBudgetGlAccountMonths( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountGroupTotalsByCidByBudgetId( $intCid, $intBudgetId, $objClientDatabase ) {

		if( false == valId( $intBudgetId ) ) return NULL;

		$arrintResults = [];

		$strSql = 'SELECT
						SUM( bgam.amount ),
						ga.gl_account_type_id
					FROM
						budget_gl_account_months bgam
						JOIN gl_accounts ga ON ( bgam.cid = ga.cid AND bgam.gl_account_id = ga.id )
						JOIN gl_account_types glat ON ( ga.gl_account_type_id = glat.id )
					WHERE
						bgam.cid = ' . ( int ) $intCid . '
						AND bgam.budget_id = ' . ( int ) $intBudgetId . '
					GROUP BY
						ga.gl_account_type_id
					ORDER BY
						ga.gl_account_type_id';

		$arrintData = fetchData( $strSql, $objClientDatabase );

		foreach( $arrintData as $arrintDatum ) {
			$arrintResults[$arrintDatum['gl_account_type_id']] = $arrintDatum['sum'];
		}

		return $arrintResults;
	}

}
?>