<?php
use Psi\Eos\Entrata\CAcceleratedRentTypes;

class CPropertyChargeSetting extends CBasePropertyChargeSetting {

	/**
	 * Get Functions
	 */
	protected $m_strPostWindowEndDate;

	/**
	 * @deprecated
	 * @param bool $boolIsImmediateMoveIn
	 * @return mixed
	 */
	public function getUse30DayMonth( $boolIsImmediateMoveIn = false ) {
		if( true == $boolIsImmediateMoveIn ) {
			return $this->m_boolImmediateMoveInUse30DayMonth;
		} else {
			return $this->m_boolUse30DayMonth;
		}
	}

	/**
	* Fetch Functions
	 */

	public function fetchRoundType( $objDatabase ) {
		return \Psi\Eos\Entrata\CRoundTypes::createService()->fetchRoundTypeById( $this->getRoundTypeId(), $objDatabase );
	}

	public function fetchAcceleratedRentType( $objDatabase ) {
		return CAcceleratedRentTypes::createService()->fetchAcceleratedRentTypeById( $this->getAcceleratedRentTypeId(), $objDatabase );
	}

	public function fetchPostWindowEndDate( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPostWindowEndDateByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	/**
	 *  Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAcceleratedRentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRoundTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequireFullMonthCharges() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequireDetachment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUse30DayMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeMoveInDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeMoveOutDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWriteOffAcceleratedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAutoPostScheduledCharges() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledChargeAutoPostDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledChargeAutoPostThrough() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostThroughNextMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequireInstallments() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $intLeaseTermStructureId = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsValid &= $this->valMonthToMonthRentArCodeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMonthToMonthRentArCodeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getMonthToMonthRentArCodeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month_to_month_rent_ar_code_id', __( 'Month to Month Rent Charge Code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrintResponse = fetchData( 'SELECT * FROM property_charge_settings WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );

		$this->setOriginalValues( $arrintResponse[0] );
		$this->setSerializedOriginalValues( serialize( $arrintResponse[0] ) );

		if( true == $boolReturnSqlOnly ) {

			$strSql = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			$strSql .= PHP_EOL . $this->insertTableLogs( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			return $strSql;
		}

		$boolIsValid = true;
		$boolIsValid &= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false == $this->insertTableLogs( $intCurrentUserId, $objDatabase ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'table_logs', __( 'Failed to insert log.' ) ) );
		}

		return $boolIsValid;
	}

	public function insertTableLogs( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strOldValuePair	= NULL;
		$strNewValuePair	= NULL;
		$strSql				= '';
		$strReturnValue		= true;

		if( $this->getLeaseTermStructureId() != $this->getOriginalValueByFieldName( 'lease_term_structure_id' ) ) {
			$strOldValuePair .= 'lease_term_structure_id::' . $this->getOriginalValueByFieldName( 'lease_term_structure_id' ) . '~^~';
			$strNewValuePair .= 'lease_term_structure_id::' . $this->getLeaseTermStructureId() . '~^~';
		}

		if( $this->getLeaseStartStructureId() != $this->getOriginalValueByFieldName( 'lease_start_structure_id' ) ) {
			$strOldValuePair .= 'lease_start_structure_id::' . $this->getOriginalValueByFieldName( 'lease_start_structure_id' ) . '~^~';
			$strNewValuePair .= 'lease_start_structure_id::' . $this->getLeaseStartStructureId() . '~^~';
		}

		if( $this->getAcceleratedRentTypeId() != $this->getOriginalValueByFieldName( 'accelerated_rent_type_id' ) ) {
			$strOldValuePair .= 'accelerated_rent_type_id::' . $this->getOriginalValueByFieldName( 'accelerated_rent_type_id' ) . '~^~';
			$strNewValuePair .= 'accelerated_rent_type_id::' . $this->getAcceleratedRentTypeId() . '~^~';
		}

		if( $this->getBaseRentRenewalOptionId() != $this->getOriginalValueByFieldName( 'base_rent_renewal_option_id' ) ) {
			$strOldValuePair .= 'base_rent_renewal_option_id::' . $this->getOriginalValueByFieldName( 'base_rent_renewal_option_id' ) . '~^~';
			$strNewValuePair .= 'base_rent_renewal_option_id::' . $this->getBaseRentRenewalOptionId() . '~^~';
		}

		if( $this->getOtherFeesRenewalOptionId() != $this->getOriginalValueByFieldName( 'other_fees_renewal_option_id' ) ) {
			$strOldValuePair .= 'other_fees_renewal_option_id::' . $this->getOriginalValueByFieldName( 'other_fees_renewal_option_id' ) . '~^~';
			$strNewValuePair .= 'other_fees_renewal_option_id::' . $this->getOtherFeesRenewalOptionId() . '~^~';
		}

		if( $this->getLeaseEndBillingTypeId() != $this->getOriginalValueByFieldName( 'lease_end_billing_type_id' ) ) {
			$strOldValuePair .= 'lease_end_billing_type_id::' . $this->getOriginalValueByFieldName( 'lease_end_billing_type_id' ) . '~^~';
			$strNewValuePair .= 'lease_end_billing_type_id::' . $this->getLeaseEndBillingTypeId() . '~^~';
		}

		if( $this->getRoundTypeId() != $this->getOriginalValueByFieldName( 'round_type_id' ) ) {
			$strOldValuePair .= 'round_type_id::' . $this->getOriginalValueByFieldName( 'round_type_id' ) . '~^~';
			$strNewValuePair .= 'round_type_id::' . $this->getRoundTypeId() . '~^~';
		}

		if( $this->getMonthToMonthTypeId() != $this->getOriginalValueByFieldName( 'month_to_month_type_id' ) ) {
			$strOldValuePair .= 'month_to_month_type_id::' . $this->getOriginalValueByFieldName( 'month_to_month_type_id' ) . '~^~';
			$strNewValuePair .= 'month_to_month_type_id::' . $this->getMonthToMonthTypeId() . '~^~';
		}

		if( $this->getMonthToMonthRentArCodeId() != $this->getOriginalValueByFieldName( 'month_to_month_rent_ar_code_id' ) ) {
			$strOldValuePair .= 'month_to_month_rent_ar_code_id::' . $this->getOriginalValueByFieldName( 'month_to_month_rent_ar_code_id' ) . '~^~';
			$strNewValuePair .= 'month_to_month_rent_ar_code_id::' . $this->getMonthToMonthRentArCodeId() . '~^~';
		}

		if( $this->convertBoolToString( $this->getEnforceUnitSpaceUnions() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'enforce_unit_space_unions' ) ) ) {
			$strOldValuePair .= 'enforce_unit_space_unions::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'enforce_unit_space_unions' ) ) . '~^~';
			$strNewValuePair .= 'enforce_unit_space_unions::' . $this->convertBoolToString( $this->getEnforceUnitSpaceUnions() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getProrateCharges() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'prorate_charges' ) ) ) {
			$strOldValuePair .= 'prorate_charges::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'prorate_charges' ) ) . '~^~';
			$strNewValuePair .= 'prorate_charges::' . $this->convertBoolToString( $this->getProrateCharges() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getIgnoreMonthToMonthIntervals() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'ignore_month_to_month_intervals' ) ) ) {
			$strOldValuePair .= 'ignore_month_to_month_intervals::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'ignore_month_to_month_intervals' ) ) . '~^~';
			$strNewValuePair .= 'ignore_month_to_month_intervals::' . $this->convertBoolToString( $this->getIgnoreMonthToMonthIntervals() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getTemporaryChargeGlPostDay() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'temporary_charge_gl_post_day' ) ) ) {
			$strOldValuePair .= 'temporary_charge_gl_post_day::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'temporary_charge_gl_post_day' ) ) . '~^~';
			$strNewValuePair .= 'temporary_charge_gl_post_day::' . $this->convertBoolToString( $this->getTemporaryChargeGlPostDay() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getChargeMoveInDay() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'charge_move_in_day' ) ) ) {
			$strOldValuePair .= 'charge_move_in_day::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'charge_move_in_day' ) ) . '~^~';
			$strNewValuePair .= 'charge_move_in_day::' . $this->convertBoolToString( $this->getChargeMoveInDay() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getChargeMoveOutDay() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'charge_move_out_day' ) ) ) {
			$strOldValuePair .= 'charge_move_out_day::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'charge_move_out_day' ) ) . '~^~';
			$strNewValuePair .= 'charge_move_out_day::' . $this->convertBoolToString( $this->getChargeMoveOutDay() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getImmediateMoveInProrateCharges() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'immediate_move_in_prorate_charges' ) ) ) {
			$strOldValuePair .= 'immediate_move_in_prorate_charges::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'immediate_move_in_prorate_charges' ) ) . '~^~';
			$strNewValuePair .= 'immediate_move_in_prorate_charges::' . $this->convertBoolToString( $this->getImmediateMoveInProrateCharges() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getWriteOffAcceleratedRent() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'write_off_accelerated_rent' ) ) ) {
			$strOldValuePair .= 'write_off_accelerated_rent::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'write_off_accelerated_rent' ) ) . '~^~';
			$strNewValuePair .= 'write_off_accelerated_rent::' . $this->convertBoolToString( $this->getWriteOffAcceleratedRent() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAutoPostScheduledCharges() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'auto_post_scheduled_charges' ) ) ) {
			$strOldValuePair .= 'auto_post_scheduled_charges::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'auto_post_scheduled_charges' ) ) . '~^~';
			$strNewValuePair .= 'auto_post_scheduled_charges::' . $this->convertBoolToString( $this->getAutoPostScheduledCharges() ) . '~^~';
		}

		if( $this->getBaseRentRenewalFactor() != $this->getOriginalValueByFieldName( 'base_rent_renewal_factor' ) ) {
			$strOldValuePair .= 'base_rent_renewal_factor::' . $this->getOriginalValueByFieldName( 'base_rent_renewal_factor' ) . '~^~';
			$strNewValuePair .= 'base_rent_renewal_factor::' . $this->getBaseRentRenewalFactor() . '~^~';
		}

		if( $this->getOtherFeesRenewalFactor() != $this->getOriginalValueByFieldName( 'other_fees_renewal_factor' ) ) {
			$strOldValuePair .= 'other_fees_renewal_factor::' . $this->getOriginalValueByFieldName( 'other_fees_renewal_factor' ) . '~^~';
			$strNewValuePair .= 'other_fees_renewal_factor::' . $this->getOtherFeesRenewalFactor() . '~^~';
		}

		if( $this->getScheduledChargeAutoPostDay() != $this->getOriginalValueByFieldName( 'scheduled_charge_auto_post_day' ) ) {
			$strOldValuePair .= 'scheduled_charge_auto_post_day::' . $this->getOriginalValueByFieldName( 'scheduled_charge_auto_post_day' ) . '~^~';
			$strNewValuePair .= 'scheduled_charge_auto_post_day::' . $this->getScheduledChargeAutoPostDay() . '~^~';
		}

		if( $this->getScheduledChargeAutoPostThrough() != $this->getOriginalValueByFieldName( 'scheduled_charge_auto_post_through' ) ) {
			$strOldValuePair .= 'scheduled_charge_auto_post_through::' . $this->getOriginalValueByFieldName( 'scheduled_charge_auto_post_through' ) . '~^~';
			$strNewValuePair .= 'scheduled_charge_auto_post_through::' . $this->getScheduledChargeAutoPostThrough() . '~^~';
		}

		if( $this->convertBoolToString( $this->getPostThroughNextMonth() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'post_through_next_month' ) ) ) {
			$strOldValuePair .= 'post_through_next_month::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'post_through_next_month' ) ) . '~^~';
			$strNewValuePair .= 'post_through_next_month::' . $this->convertBoolToString( $this->getPostThroughNextMonth() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getShowRentAmountOnQuickView() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'show_rent_amount_on_quick_view' ) ) ) {
			$strOldValuePair .= 'show_rent_amount_on_quick_view::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'show_rent_amount_on_quick_view' ) ) . '~^~';
			$strNewValuePair .= 'show_rent_amount_on_quick_view::' . $this->convertBoolToString( $this->getShowRentAmountOnQuickView() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getStartRenewalChargesOnFirst() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'start_renewal_charges_on_first' ) ) ) {
			$strOldValuePair .= 'start_renewal_charges_on_first::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'start_renewal_charges_on_first' ) ) . '~^~';
			$strNewValuePair .= 'start_renewal_charges_on_first::' . $this->convertBoolToString( $this->getStartRenewalChargesOnFirst() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getRequireInstallments() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'require_installments' ) ) ) {
			$strOldValuePair .= 'require_installments::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'require_installments' ) ) . '~^~';
			$strNewValuePair .= 'require_installments::' . $this->convertBoolToString( $this->getRequireInstallments() ) . '~^~';
		}

		if( $this->getAvailableSpaceLimitDays() != $this->getOriginalValueByFieldName( 'available_space_limit_days' ) ) {
			$strOldValuePair .= 'available_space_limit_days::' . $this->getOriginalValueByFieldName( 'available_space_limit_days' ) . '~^~';
			$strNewValuePair .= 'available_space_limit_days::' . $this->getAvailableSpaceLimitDays() . '~^~';
		}

		if( $this->getOnNoticeSpaceLimitDays() != $this->getOriginalValueByFieldName( 'on_notice_space_limit_days' ) ) {
			$strOldValuePair .= 'on_notice_space_limit_days::' . $this->getOriginalValueByFieldName( 'on_notice_space_limit_days' ) . '~^~';
			$strNewValuePair .= 'on_notice_space_limit_days::' . $this->getOnNoticeSpaceLimitDays() . '~^~';
		}

		if( $this->getEarlyDisplayAllowanceDays() != $this->getOriginalValueByFieldName( 'early_display_allowance_days' ) ) {
			$strOldValuePair .= 'early_display_allowance_days::' . $this->getOriginalValueByFieldName( 'early_display_allowance_days' ) . '~^~';
			$strNewValuePair .= 'early_display_allowance_days::' . $this->getEarlyDisplayAllowanceDays() . '~^~';
		}

		if( $this->convertBoolToString( $this->getShowAvailableNotReadySpaces() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'show_available_not_ready_spaces' ) ) ) {
			$strOldValuePair .= 'show_available_not_ready_spaces::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'show_available_not_ready_spaces' ) ) . '~^~';
			$strNewValuePair .= 'show_available_not_ready_spaces::' . $this->convertBoolToString( $this->getShowAvailableNotReadySpaces() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getShowNoticeAvailableSpaces() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'show_notice_available_spaces' ) ) ) {
			$strOldValuePair .= 'show_notice_available_spaces::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'show_notice_available_spaces' ) ) . '~^~';
			$strNewValuePair .= 'show_notice_available_spaces::' . $this->convertBoolToString( $this->getShowNoticeAvailableSpaces() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowRateOffsets() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_rate_offsets' ) ) ) {
			$strOldValuePair .= 'allow_rate_offsets::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_rate_offsets' ) ) . '~^~';
			$strNewValuePair .= 'allow_rate_offsets::' . $this->convertBoolToString( $this->getAllowRateOffsets() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowLateFeeOverrides() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_late_fee_overrides' ) ) ) {
			$strOldValuePair .= 'allow_late_fee_overrides::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_late_fee_overrides' ) ) . '~^~';
			$strNewValuePair .= 'allow_late_fee_overrides::' . $this->convertBoolToString( $this->getAllowLateFeeOverrides() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowAssetRates() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_asset_rates' ) ) ) {
			$strOldValuePair .= 'allow_asset_rates::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_asset_rates' ) ) . '~^~';
			$strNewValuePair .= 'allow_asset_rates::' . $this->convertBoolToString( $this->getAllowAssetRates() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowEquityRates() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_equity_rates' ) ) ) {
			$strOldValuePair .= 'allow_equity_rates::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_equity_rates' ) ) . '~^~';
			$strNewValuePair .= 'allow_equity_rates::' . $this->convertBoolToString( $this->getAllowEquityRates() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowExpenseRates() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_expense_rates' ) ) ) {
			$strOldValuePair .= 'allow_expense_rates::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_expense_rates' ) ) . '~^~';
			$strNewValuePair .= 'allow_expense_rates::' . $this->convertBoolToString( $this->getAllowExpenseRates() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowLiabilityRates() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_liability_rates' ) ) ) {
			$strOldValuePair .= 'allow_liability_rates::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_liability_rates' ) ) . '~^~';
			$strNewValuePair .= 'allow_liability_rates::' . $this->convertBoolToString( $this->getAllowLiabilityRates() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowPreQualificationTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_pre_qualification_trigger' ) ) ) {
			$strOldValuePair .= 'allow_pre_qualification_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_pre_qualification_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_pre_qualification_trigger::' . $this->convertBoolToString( $this->getAllowPreQualificationTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowApplicationApprovalTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_application_approval_trigger' ) ) ) {
			$strOldValuePair .= 'allow_application_approval_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_application_approval_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_application_approval_trigger::' . $this->convertBoolToString( $this->getAllowApplicationApprovalTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowLeaseApprovalTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_lease_approval_trigger' ) ) ) {
			$strOldValuePair .= 'allow_lease_approval_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_lease_approval_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_lease_approval_trigger::' . $this->convertBoolToString( $this->getAllowLeaseApprovalTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowLastLeaseMonthTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_last_lease_month_trigger' ) ) ) {
			$strOldValuePair .= 'allow_last_lease_month_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_last_lease_month_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_last_lease_month_trigger::' . $this->convertBoolToString( $this->getAllowLastLeaseMonthTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowNoticeTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_notice_trigger' ) ) ) {
			$strOldValuePair .= 'allow_notice_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_notice_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_notice_trigger::' . $this->convertBoolToString( $this->getAllowNoticeTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowAnniversaryOfMoveInTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_anniversary_of_move_in_trigger' ) ) ) {
			$strOldValuePair .= 'allow_anniversary_of_move_in_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_anniversary_of_move_in_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_anniversary_of_move_in_trigger::' . $this->convertBoolToString( $this->getAllowAnniversaryOfMoveInTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowEndOfYearTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_end_of_year_trigger' ) ) ) {
			$strOldValuePair .= 'allow_end_of_year_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_end_of_year_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_end_of_year_trigger::' . $this->convertBoolToString( $this->getAllowEndOfYearTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowHourlyTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_hourly_trigger' ) ) ) {
			$strOldValuePair .= 'allow_hourly_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_hourly_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_hourly_trigger::' . $this->convertBoolToString( $this->getAllowHourlyTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowDailyTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_daily_trigger' ) ) ) {
			$strOldValuePair .= 'allow_daily_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_daily_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_daily_trigger::' . $this->convertBoolToString( $this->getAllowDailyTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowWeeklyTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_weekly_trigger' ) ) ) {
			$strOldValuePair .= 'allow_weekly_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_weekly_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_weekly_trigger::' . $this->convertBoolToString( $this->getAllowWeeklyTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowEveryTwoWeeksTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_every_two_weeks_trigger' ) ) ) {
			$strOldValuePair .= 'allow_every_two_weeks_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_every_two_weeks_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_every_two_weeks_trigger::' . $this->convertBoolToString( $this->getAllowEveryTwoWeeksTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowTwicePerMonthTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_twice_per_month_trigger' ) ) ) {
			$strOldValuePair .= 'allow_twice_per_month_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_twice_per_month_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_twice_per_month_trigger::' . $this->convertBoolToString( $this->getAllowTwicePerMonthTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowSpecificMonthsTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_specific_months_trigger' ) ) ) {
			$strOldValuePair .= 'allow_specific_months_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_specific_months_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_specific_months_trigger::' . $this->convertBoolToString( $this->getAllowSpecificMonthsTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowQuarterlyTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_quarterly_trigger' ) ) ) {
			$strOldValuePair .= 'allow_quarterly_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_quarterly_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_quarterly_trigger::' . $this->convertBoolToString( $this->getAllowQuarterlyTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowTwicePerYearTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_twice_per_year_trigger' ) ) ) {
			$strOldValuePair .= 'allow_twice_per_year_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_twice_per_year_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_twice_per_year_trigger::' . $this->convertBoolToString( $this->getAllowTwicePerYearTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowYearlyTrigger() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_yearly_trigger' ) ) ) {
			$strOldValuePair .= 'allow_yearly_trigger::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_yearly_trigger' ) ) . '~^~';
			$strNewValuePair .= 'allow_yearly_trigger::' . $this->convertBoolToString( $this->getAllowYearlyTrigger() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getPostAccelRentInCurrentPostMonth() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'post_accel_rent_in_current_post_month' ) ) ) {
			$strOldValuePair .= 'post_accel_rent_in_current_post_month::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'post_accel_rent_in_current_post_month' ) ) . '~^~';
			$strNewValuePair .= 'post_accel_rent_in_current_post_month::' . $this->convertBoolToString( $this->getPostAccelRentInCurrentPostMonth() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowRepaymentAgreements() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_repayment_agreements' ) ) ) {
			$strOldValuePair .= 'allow_repayment_agreements::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_repayment_agreements' ) ) . '~^~';
			$strNewValuePair .= 'allow_repayment_agreements::' . $this->convertBoolToString( $this->getAllowRepaymentAgreements() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowRepaymentsForActiveLeases() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_repayments_for_active_leases' ) ) ) {
			$strOldValuePair .= 'allow_repayments_for_active_leases::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_repayments_for_active_leases' ) ) . '~^~';
			$strNewValuePair .= 'allow_repayments_for_active_leases::' . $this->convertBoolToString( $this->getAllowRepaymentsForActiveLeases() ) . '~^~';
		}

		if( $this->getRepaymentMaximumAllowedMonths() != $this->getOriginalValueByFieldName( 'repayment_maximum_allowed_months' ) ) {
			$strOldValuePair .= 'repayment_maximum_allowed_months::' . $this->getOriginalValueByFieldName( 'repayment_maximum_allowed_months' ) . '~^~';
			$strNewValuePair .= 'repayment_maximum_allowed_months::' . $this->getRepaymentMaximumAllowedMonths() . '~^~';
		}

		if( ( int ) $this->getRepaymentAgreementArCodeId() != ( int ) $this->getOriginalValueByFieldName( 'repayment_agreement_ar_code_id' ) ) {
			$strOldValuePair .= 'repayment_agreement_ar_code_id::' . $this->getOriginalValueByFieldName( 'repayment_agreement_ar_code_id' ) . '~^~';
			$strNewValuePair .= 'repayment_agreement_ar_code_id::' . $this->getRepaymentAgreementArCodeId() . '~^~';
		}

		if( $this->getRepaymentAllocationOrder() != $this->getOriginalValueByFieldName( 'repayment_allocation_order' ) ) {
			$strOldValuePair .= 'repayment_allocation_order::' . $this->getOriginalValueByFieldName( 'repayment_allocation_order' ) . '~^~';
			$strNewValuePair .= 'repayment_allocation_order::' . $this->getRepaymentAllocationOrder() . '~^~';
		}

		if( $this->convertBoolToString( $this->getRepaymentChargeLateFeesRetroactively() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'repayment_charge_late_fees_retroactively' ) ) ) {
			$strOldValuePair .= 'repayment_charge_late_fees_retroactively::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'repayment_charge_late_fees_retroactively' ) ) . '~^~';
			$strNewValuePair .= 'repayment_charge_late_fees_retroactively::' . $this->convertBoolToString( $this->getRepaymentChargeLateFeesRetroactively() ) . '~^~';
		}

		if( ( int ) $this->getDefaultMtmMultiplierLeaseTermId() != ( int ) $this->getOriginalValueByFieldName( 'default_mtm_multiplier_lease_term_id' ) ) {
			$strOldValuePair .= 'default_mtm_multiplier_lease_term_id::' . $this->getOriginalValueByFieldName( 'default_mtm_multiplier_lease_term_id' ) . '~^~';
			$strNewValuePair .= 'default_mtm_multiplier_lease_term_id::' . $this->getDefaultMtmMultiplierLeaseTermId() . '~^~';
		}

		if( $this->getDefaultMtmMultiplier() != $this->getOriginalValueByFieldName( 'default_mtm_multiplier' ) ) {
			$strOldValuePair .= 'default_mtm_multiplier::' . $this->getOriginalValueByFieldName( 'default_mtm_multiplier' ) . '~^~';
			$strNewValuePair .= 'default_mtm_multiplier::' . $this->getDefaultMtmMultiplier() . '~^~';
		}

		if( $this->getProrationMethodTypeId() != $this->getOriginalValueByFieldName( 'proration_method_type_id' ) ) {
			$strOldValuePair .= 'proration_method_type_id::' . $this->getOriginalValueByFieldName( 'proration_method_type_id' ) . '~^~';
			$strNewValuePair .= 'proration_method_type_id::' . $this->getProrationMethodTypeId() . '~^~';
		}

		if( false == is_null( $strNewValuePair ) || false == is_null( $strOldValuePair ) ) {

			$strDescription = __( 'Property charge settings has been modified' );

			$objTableLog = new CTableLog();
			$strReturnValue = $objTableLog->insert( $intCurrentUserId, $objDatabase, 'property_charge_settings', $this->getPropertyId(), 'UPDATE', $this->getCid(), $strOldValuePair, $strNewValuePair, $strDescription, $boolReturnSqlOnly );

			if( true == $boolReturnSqlOnly ) {
				$strSql .= $strReturnValue;
			}
		}

		return( true == $boolReturnSqlOnly ) ? $strSql : $strReturnValue;
	}

	private function convertBoolToString( $mixInputValue ) {

		$strReturnValue = '';

		if( 'boolean' === gettype( $mixInputValue ) ) {
			$strReturnValue = ( false != $mixInputValue ) ? 'Yes' : 'No';
		} elseif( 'string' === gettype( $mixInputValue ) ) {
			$strReturnValue = ( 'f' != $mixInputValue ) ? 'Yes' : 'No';
		}

		settype( $strReturnValue, 'string' );

		return $strReturnValue;
	}

	public function getCalculateBillingEndDate( $strEventDate ) {

		if( false == valStr( $strEventDate ) ) {
			return;
		}

		$strPostingMonth			= NULL;
		$strPostThroughIncrement	= NULL;

		if( true == $this->getPostThroughNextMonth() && ( ( strtotime( $strEventDate ) == strtotime( ( date( 'Y-m-t', strtotime( $strEventDate ) ) ) ) ) || $this->getScheduledChargeAutoPostDay() <= date( 'j', strtotime( $strEventDate ) ) ) ) {
			$strPostingMonth = date( 'Y-m-d', strtotime( '+1 month', strtotime( date( 'Y-m-01', strtotime( $strEventDate ) ) ) ) );
		} else {
			$strPostingMonth = date( 'Y-m-01', strtotime( $strEventDate ) );
		}

		$strPostThroughIncrement = $this->getScheduledChargeAutoPostThrough() - 1 . ' days';
		if( 0 == $this->getScheduledChargeAutoPostThrough() ) {
			$strPostThroughIncrement = '0 days';
		} elseif( true == in_array( $this->getScheduledChargeAutoPostThrough(), [ 29, 30, 31 ] ) ) {
			$strPostThroughIncrement = '1 month - 1 day';
		}

		return date( 'm/d/Y', strtotime( $strPostingMonth . $strPostThroughIncrement ) );
	}

	public function getOrFetchPostWindowEndDate( $objDatabase ) {

		if( false == valStr( $this->m_strPostWindowEndDate ) ) {
			$this->m_strPostWindowEndDate = $this->fetchPostWindowEndDate( $objDatabase );
		}
		return $this->m_strPostWindowEndDate;
	}

	public function	getFinancialDaysInMonth( $intDaysInMonth ) {

		if ( $this->m_intProrationMethodTypeId == \CProrationMethodType::PRORATION_METHOD_TYPE_CALENDAR_DAY_MAX_OF_30_DAYS && $intDaysInMonth > \CProrationMethodType::DAYS_IN_30_DAY_MONTH )
			$intDays = $intDaysInMonth - 1;
		else if ( $this->m_intProrationMethodTypeId == \CProrationMethodType::PRORATION_METHOD_TYPE_30_DAY_MONTH )
			$intDays = \CProrationMethodType::DAYS_IN_30_DAY_MONTH;
		else
			$intDays = $intDaysInMonth;

		return $intDays;
	}

}
?>