<?php

class CSubsidyCertificationStatusType extends CBaseSubsidyCertificationStatusType {

	const OPEN					= 1;
	const COMPLETED				= 2;
	const PROCESSED				= 3;
	const FINALIZED				= 4;
	const TRANSMITTED			= 5;
	const UNHANDLED_EXCEPTIONS	= 6;
	const ACKNOWLEDGED			= 7;

	public static $c_arrintAllSubsidyCertificationStatusTypeIds = [
		self::OPEN,
		self::COMPLETED,
		self::PROCESSED,
		self::FINALIZED,
		self::TRANSMITTED,
		self::UNHANDLED_EXCEPTIONS,
		self::ACKNOWLEDGED
	];

	public static $c_arrintFinalSubsidyCertificationStatusTypeIds = [
		self::PROCESSED,
		self::FINALIZED,
		self::TRANSMITTED,
		self::UNHANDLED_EXCEPTIONS,
		self::ACKNOWLEDGED
	];

	public static $c_arrintCurrentSubsidyCertificationStatusTypeIds = [
		self::FINALIZED,
		self::TRANSMITTED,
		self::UNHANDLED_EXCEPTIONS,
		self::ACKNOWLEDGED
	];

	public static $c_arrintHudCorrectionSubsidyCertificationStatusTypeIds = [
		self::TRANSMITTED,
		self::UNHANDLED_EXCEPTIONS,
		self::ACKNOWLEDGED
	];

	public static $c_arrintResetCertificationAllowedStatusTypeIds = [
		self::OPEN,
		self::COMPLETED
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getSubsidyCertificationStatusTypeIdsToStrArray() {
		return [
			self::OPEN => __( 'Open' ),
			self::COMPLETED => __( 'Completed' ),
			self::PROCESSED => __( 'Processed' ),
			self::FINALIZED => __( 'Finalized' ),
			self::TRANSMITTED => __( 'Transmitted' ),
			self::UNHANDLED_EXCEPTIONS => __( 'Unhandled Exceptions' ),
			self::ACKNOWLEDGED => __( 'Acknowledged' )
		];
	}

}
?>