<?php

class CBlogPost extends CBaseBlogPost {

	protected $m_intCommentCount;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strUsername;
	protected $m_boolIsAdministrator;

	public function valTitle( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
			return $boolIsValid;
		} elseif( false == is_null( $this->getTitle() ) ) {
			$objWebsiteBlogPost = \Psi\Eos\Entrata\CBlogPosts::createService()->fetchBlogPostByWebsiteIdByCid( $this->getTitle(), $this->m_intWebsiteId, $this->m_intCid, $objDatabase );
			if( true == valObj( $objWebsiteBlogPost, \CBlogPost::class ) && $objWebsiteBlogPost->getId() != $this->getId() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Title already exists.' ) ) );

				return false;
			}
		}
		return $boolIsValid;
	}

	public function valScheduledOn( $strTimeZone ) {
		$boolIsValid = true;
		$strScheduledOn = $this->getScheduledOn();
		date_default_timezone_set( $strTimeZone );
		$strTime = date( 'm/d/Y H:i:s' );

		if( true == empty( $strScheduledOn ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on', __( 'Schedule date/time is required.' ) ) );
			$boolIsValid = false;
		} elseif( strtotime( $strScheduledOn ) <= strtotime( $strTime ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on', __( 'Please enter future Schedule date/time.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $strTimeZone = CTimeZone::DEFAULT_TIME_ZONE_NAME ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_scheduled':
				$boolIsValid &= $this->valScheduledOn( $strTimeZone );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setUrl( $strUrl ) {
		$this->m_strUrl = CStrings::strTrimDef( $strUrl, 256, NULL, true );
		$this->m_strUrl = \Psi\CStringService::singleton()->preg_replace( '/(\s|-|\s-|-\s)+/m', '-', \Psi\CStringService::singleton()->preg_replace( '/(^-|`|\^|\\\|\[|\]|_)+|[^0-9a-zA-z_\s\-]+/m', '', \Psi\CStringService::singleton()->strtolower( $this->m_strUrl ) ) );
	}

	public function setUrlCustomExtension( $strUrlCustomExtension ) {
		if( true == valStr( $strUrlCustomExtension ) ) {
			$this->m_strUrlCustomExtension = CStrings::strTrimDef( $strUrlCustomExtension, 128, NULL, true );
			$this->m_strUrlCustomExtension = \Psi\CStringService::singleton()->preg_replace( '/(\s|-|\s-|-\s)+/m', '-', \Psi\CStringService::singleton()->preg_replace( '/(^-|`|\^|\\\|\[|\]|_)+|[^0-9a-zA-z_\s\-]+/m', '', \Psi\CStringService::singleton()->strtolower( $this->m_strUrlCustomExtension ) ) );
		} else {
			$this->m_strUrlCustomExtension = NULL;
		}
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes = true );

		if( isset( $arrstrValues['comment_count'] ) && $boolDirectSet ) {
			$this->m_intCommentCount = trim( $arrstrValues['comment_count'] );
		} elseif( isset( $arrstrValues['comment_count'] ) ) {
			$this->setCommentCount( $arrstrValues['comment_count'] );
		}
		if( isset( $arrstrValues['name_first'] ) ) {
			$this->setNameFirst( $arrstrValues['name_first'] );
		}
		if( isset( $arrstrValues['name_last'] ) ) {
			$this->setNameLast( $arrstrValues['name_last'] );
		}
		if( isset( $arrstrValues['username'] ) ) {
			$this->setUsername( $arrstrValues['username'] );
		}
		if( isset( $arrstrValues['is_administrator'] ) ) {
			$this->setIsAdministrator( $arrstrValues['is_administrator'] );
		}
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = CStrings::strTrimDef( $strNameFirst, 256, NULL, true );
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = CStrings::strTrimDef( $strNameLast, 256, NULL, true );
	}

	public function setUsername( $strUsername ) {
		$this->m_strUsername = CStrings::strTrimDef( $strUsername, 256, NULL, true );
	}

	public function setIsAdministrator( $boolIsAdministrator ) {
		$this->m_boolIsAdministrator = $boolIsAdministrator;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function getIsAdministrator() {
		return $this->m_boolIsAdministrator;
	}

	public function setCommentCount( $intCommentCount ) {
		return $this->m_intCommentCount = CStrings::strToIntDef( $intCommentCount, NULL, false );
	}

	public function setAuthorName( $strAuthorName ) {
		$arrstrPatterns = [ '/[^a-zA-Z\s]+/i', '/\s{2,}/i' ];
		$arrstrReplacements = [ '', ' ' ];
		$strAuthorName = \Psi\CStringService::singleton()->preg_replace( $arrstrPatterns, $arrstrReplacements, trim( $strAuthorName ) );
		$this->set( 'm_strAuthorName', CStrings::strTrimDef( $strAuthorName, 50, NULL, true ) );
	}

	public function getCommentCount() {
		return $this->m_intCommentCount;
	}

	public function fetchBlogPostsByPostId( $objDatabase ) {
		return \Psi\Eos\Entrata\CBlogPosts::createService()->fetchBlogPostsByPostId( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlogCommentsByWebsiteIdByCid( $intWebsiteId, $intCid, $objDatabase ) {
		return \Psi\Eos\Entrata\CBlogComments::createService()->fetchBlogCommentsByPostIdByWebsiteIdByCid( $this->getId(), $intWebsiteId, $intCid, $objDatabase );
	}

	public function fetchBlogPostMediaIds( $objDatabase ) {
		return \Psi\Eos\Entrata\CBlogPostMedias::createService()->fetchBlogPostMediaIdsByClientIdByWebsiteIdByPostId( $this->getCid(), $this->getWebsiteId(), $this->getId(), $objDatabase );
	}

	public function fetchBlogPostAssociatedMedias( $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::BLOG_MEDIA, CMarketingMediaSubType::BLOG_POST_MEDIA, $this->getCid(), $objDatabase );
	}

}
?>