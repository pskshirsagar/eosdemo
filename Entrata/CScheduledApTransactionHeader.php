<?php

class CScheduledApTransactionHeader extends CBaseScheduledApTransactionHeader {

	protected $m_boolIsAutoPostCurrentInvoice;

	protected $m_intCurrentTime;
	protected $m_intApRemittanceId;
	protected $m_intApLegalEntityId;
	protected $m_intApPayeeAccountId;
	protected $m_intNumberOfOccurrences;

	protected $m_fltControlTotal;
	protected $m_fltTransactionAmount;

	protected $m_strEndMethod;
	protected $m_strPostMonth;
	protected $m_strBankAccountName;

	protected $m_boolReceives1099;

	protected $m_objApPayee;
	protected $m_objApPayeeLocation;

	protected $m_arrintCompanyUserPropertyIds;
	protected $m_arrintAssociatedBankPropertyIds;
	protected $m_arrintScheduledApTransactionDetailsPropertyIds;

	protected $m_arrobjProperties;
	protected $m_arrobjFrequencies;
	protected $m_arrobjApPayeeProperties;
	protected $m_arrobjPropertyGlSettings;
	protected $m_arrobjGlAccountProperties;
	protected $m_arrobjAssociatedGlAccounts;
	protected $m_arrobjScheduledApTransactionDetails;

	/**
	 * Get Functions
	 */

	const SCHEDULED_AP_TRANSACTION_STATUS_PENDING		= 1;
	const SCHEDULED_AP_TRANSACTION_STATUS_DISABLED		= 2;
	const SCHEDULED_AP_TRANSACTION_STATUS_APPROVED		= 3;
	const SCHEDULED_AP_TRANSACTION_STATUS_COMPLETED		= 4;

	public static $c_arrintScheduleApTransactionStausTypes = [
		CScheduledApTransactionHeader::SCHEDULED_AP_TRANSACTION_STATUS_PENDING => 'Pending',
		CScheduledApTransactionHeader::SCHEDULED_AP_TRANSACTION_STATUS_DISABLED => 'Disabled',
		CScheduledApTransactionHeader::SCHEDULED_AP_TRANSACTION_STATUS_APPROVED => 'Approved',
		CScheduledApTransactionHeader::SCHEDULED_AP_TRANSACTION_STATUS_COMPLETED => 'Completed',
	];

	public function getApPayeeProperties() {
		return $this->m_arrobjApPayeeProperties;
	}

	public function getApPayee() {
		return $this->m_objApPayee;
	}

	public function getApPayeeLocation() {
		return $this->m_objApPayeeLocation;
	}

	public function getScheduledApTransactionDetailsPropertyIds() {
		return $this->m_arrintScheduledApTransactionDetailsPropertyIds;
	}

	public function getProperties() {
		return $this->m_arrobjProperties;
	}

	public function getAssociatedGlAccounts() {
		return $this->m_arrobjAssociatedGlAccounts;
	}

	public function getGlAccountProperties() {
		return $this->m_arrobjGlAccountProperties;
	}

	public function getScheduledApTransactionDetails() {
		return $this->m_arrobjScheduledApTransactionDetails;
	}

	public function getControlTotal() {
		return $this->m_fltControlTotal;
	}

	public function getPropertyGlSettings() {
		return $this->m_arrobjPropertyGlSettings;
	}

	public function getAssociatedBankPropertyIds() {
		return $this->m_arrintAssociatedBankPropertyIds;
	}

	public function getCurrentTime() {
		return $this->m_intCurrentTime;
	}

	public function getEndMethod() {
		return $this->m_strEndMethod;
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function getNumberOfOccurrences() {
		return $this->m_intNumberOfOccurrences;
	}

	public function getIsAutoPostCurrentInvoice() {
		return $this->m_boolIsAutoPostCurrentInvoice;
	}

	public function getBankAccountName() {
		return $this->m_strBankAccountName;
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function getCompanyUserPropertyIds() {
		return $this->m_arrintCompanyUserPropertyIds;
	}

	public function getApRemittenceId() {
		return $this->m_intApRemittanceId;
	}

	public function getApPayeeAccountId() {
		return $this->m_intApPayeeAccountId;
	}

	public function getApLegalEntityId() {
		return $this->m_intApLegalEntityId;
	}

	public function getReceives1099() {
		return $this->m_boolReceives1099;
	}

	/**
	 * Set Functions
	 */

	public function setApPayeeProperties( $arrintAssociatedApPayeeProperties ) {
		$this->m_arrobjApPayeeProperties = $arrintAssociatedApPayeeProperties;
	}

	public function setApPayee( $objApPayee ) {
		$this->m_objApPayee = $objApPayee;
	}

	public function setApPayeeLocation( $objApPayeeLocation ) {
		$this->m_objApPayeeLocation = $objApPayeeLocation;
	}

	public function setScheduledApTransactionDetailsPropertyIds( $arrintScheduledApTransactionDetailsPropertyIds ) {
		$this->m_arrintScheduledApTransactionDetailsPropertyIds = $arrintScheduledApTransactionDetailsPropertyIds;
	}

	public function setProperties( $arrobjProperties ) {
		$this->m_arrobjProperties = $arrobjProperties;
	}

	public function setAssociatedGlAccounts( $arrobjAssociatedGlAccounts ) {
		$this->m_arrobjAssociatedGlAccounts = $arrobjAssociatedGlAccounts;
	}

	public function setGlAccountProperties( $arrobjAssociatedGlAccountProperties ) {
		$this->m_arrobjGlAccountProperties = $arrobjAssociatedGlAccountProperties;
	}

	public function setScheduledApTransactionDetails( $arrobjScheduledApTransactionDetails ) {
		$this->m_arrobjScheduledApTransactionDetails = $arrobjScheduledApTransactionDetails;
	}

	public function setControlTotal( $fltControlTotal ) {
		$this->m_fltControlTotal = CStrings::strToFloatDef( $fltControlTotal, NULL, false, 3 );
	}

	public function setPropertyGlSettings( $arrobjPropertyGlSettings ) {
		$this->m_arrobjPropertyGlSettings = $arrobjPropertyGlSettings;
	}

	public function setCurrentTime( $intCurrentTime ) {
		$this->m_intCurrentTime = $intCurrentTime;
	}

	public function setAssociatedBankPropertyIds( $arrintAssociatedBankPropertyIds ) {
		$this->m_arrintAssociatedBankPropertyIds = $arrintAssociatedBankPropertyIds;
	}

	public function setEndMethod( $strEndMethod ) {
		$this->m_strEndMethod = $strEndMethod;
	}

	public function setPostMonth( $strPostMonth ) {
		$this->m_strPostMonth = $strPostMonth;
	}

	public function setNumberOfOccurrences( $intNumberOfOccurrences ) {
		$this->m_intNumberOfOccurrences = CStrings::strToIntDef( $intNumberOfOccurrences, NULL, false );
	}

	public function setIsAutoPostCurrentInvoice( $boolIsAutoPostCurrentInvoice ) {
		$this->m_boolIsAutoPostCurrentInvoice = $boolIsAutoPostCurrentInvoice;
	}

	public function setBankAccountName( $strBankAccountName ) {
		$this->m_strBankAccountName = $strBankAccountName;
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->m_fltTransactionAmount = CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 );
	}

	public function setCompanyUserPropertyIds( $arrintCompanyUserPropertyIds ) {
		$this->m_arrintCompanyUserPropertyIds = $arrintCompanyUserPropertyIds;
	}

	public function setApRemittenceId( $intApRemittenceId ) {
		$this->m_intApRemittanceId = $intApRemittenceId;
	}

	public function setApPayeeAccountId( $intApPayeeAccountId ) {
		$this->m_intApPayeeAccountId = $intApPayeeAccountId;
	}

	public function setApLegalEntityId( $intApLegalEntityId ) {
		$this->m_intApLegalEntityId = $intApLegalEntityId;
	}

	public function setReceives1099( $boolReceives1099 ) {
		$this->m_boolReceives1099 = $boolReceives1099;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['end_method'] ) ) $this->setEndMethod( $arrmixValues['end_method'] );
		if( true == isset( $arrmixValues['number_of_occurrences'] ) ) $this->setNumberOfOccurrences( $arrmixValues['number_of_occurrences'] );
		if( true == isset( $arrmixValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrmixValues['transaction_amount'] );
		if( true == isset( $arrmixValues['ap_payee_account_id'] ) ) $this->setApPayeeAccountId( $arrmixValues['ap_payee_account_id'] );
		if( true == isset( $arrmixValues['ap_remittance_id'] ) ) $this->setApRemittenceId( $arrmixValues['ap_remittance_id'] );
		if( true == isset( $arrmixValues['ap_legal_entity_id'] ) ) $this->setApLegalEntityId( $arrmixValues['ap_legal_entity_id'] );
		if( true == isset( $arrmixValues['receives_1099'] ) ) $this->setReceives1099( $arrmixValues['receives_1099'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsValidScheduler() {
		$boolIsValid = true;

		// Makes sure invoice or po is created through scheduler because a scheduler which neither create invoice nor po is of no use.
		if( 0 == $this->getAutoCreatePo() && 1 != $this->getAutoCreateInvoice() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auto_create_po', 'Scheduler should either create invoice or purchase order.' ) );
		}
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Error adding recurring transaction.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valApPayee() {
		$boolIsValid	= true;
		$objApPayee		= $this->getApPayee();

		if( false == valObj( $objApPayee, 'CApPayee' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Error adding recurring transaction. Missing vendor ID.' ) );
			return $boolIsValid;
		}

		if( CApPayeeStatusType::ACTIVE != $objApPayee->getApPayeeStatusTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Cannot add/edit/approve a recurring transaction to an inactive/on hold vendor.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valScheduledPoTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getScheduledPoTypeId() ) && 1 == $this->getAutoCreatePo() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_po_type_id', 'Error adding recurring transaction. Missing PO Type ID' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPoStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApPayeeLocationId() {
		$boolIsValid = true;

		if( false == valObj( $this->m_objApPayeeLocation, 'CApPayeeLocation' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', 'Location must be selected for vendor. ' ) );
			return $boolIsValid;
		}

		if( false == is_null( $this->m_objApPayeeLocation->getDisabledOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', 'The selected vendor location is disabled. ' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valBankAccountId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', 'A frequency is required to save a recurring transaction.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valApPayeeTermId() {
		$boolIsValid = true;

		if( 1 == $this->getAutoApproveInvoice() && true == is_null( $this->getApPayeeTermId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_term_id', 'Terms are required when automatically approving invoice for payment' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyInterval() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyInterval() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', 'Recurrance interval is required.' ) );
		} elseif( false == is_numeric( $this->getFrequencyInterval() ) || 0 >= $this->getFrequencyInterval() || 365 < $this->getFrequencyInterval() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', 'Recurrance interval is invalid.' ) );
		}

		return $boolIsValid;
	}

	public function valStartDate( $objClientDatabase = NULL, $objScheduledApTransactionHeader = NULL, $arrintOriginalScheduledApTransactionHeader = NULL ) {

		$boolIsValid          = true;
		if( true == valObj( $objScheduledApTransactionHeader, 'CScheduledApTransactionHeader' ) ) {
			$intFrequencyId       = $objScheduledApTransactionHeader->getFrequencyId();
			$intFrequencyInterval = $objScheduledApTransactionHeader->getFrequencyInterval();
			$strNextPostdate      = $objScheduledApTransactionHeader->getNextPostDate();
			$strLastPostedDate    = $objScheduledApTransactionHeader->getLastPostedDate();
		}

		if( 10 > strtotime( $this->getStartDate() ) ) {
			$this->setStartDate( '' );
		}

		if( true == is_null( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Recurring transaction start date is required.' ) );
			return $boolIsValid;
		} elseif( true == valStr( $this->getStartDate() ) && false == CValidation::validateDate( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Recurring transaction start date is invalid or start date should be in mm/dd/yyyy format.' ) );
			return $boolIsValid;
		}

		if( true == valObj( $objScheduledApTransactionHeader, 'CScheduledApTransactionHeader' )
		    && true == $boolIsValid && true == $objScheduledApTransactionHeader->getApprovedOn()
		    && true == $objScheduledApTransactionHeader->getApprovedBy()
		    && true == valStr( $this->getLastPostedDate() ) ) {

			$this->m_arrobjFrequencies	= ( array ) \Psi\Eos\Entrata\CFrequencies::createService()->fetchAllFrequenciesByIds( CFrequency::$c_arrintScheduledApTransactionFrequencies, $objClientDatabase );
			$objFrequency			    = $this->m_arrobjFrequencies[$intFrequencyId];

			if( false == valstr( $strNextPostdate ) ) {
				$this->setNextPostDate( $this->calculateNextPostDate( $objScheduledApTransactionHeader, $objClientDatabase, $objFrequency ) );

			} elseif( true == valArr( $arrintOriginalScheduledApTransactionHeader ) && ( $arrintOriginalScheduledApTransactionHeader['frequency_id'] != $intFrequencyId
			           || $arrintOriginalScheduledApTransactionHeader['frequency_interval'] != $intFrequencyInterval ) ) {

				$strUpdateNextPostDate        = date( 'm/d/Y', strtotime( $strLastPostedDate . ' +' . $intFrequencyInterval . ' ' . $objFrequency->getIntervalFrequencyLabelPlural() ) );
				$this->setNextPostDate( $strUpdateNextPostDate );

			} else {
				$this->setNextPostDate( $strNextPostdate );
			}

			$this->setLastPostedDate( $strLastPostedDate );
		} elseif( true == $boolIsValid ) {
			$this->setNextPostDate( $this->getStartDate() );
		}

		return $boolIsValid;
	}

	public function valEndDate() {

		$boolIsValid = true;

		if( true == valStr( $this->getEndMethod() ) && 'number' == $this->getEndMethod() ) {
			return true;
		}

		if( 10 > strtotime( $this->getEndDate() ) ) {
			$this->setEndDate( '' );
		}

		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Recurring transaction end date is required.' ) );
			return $boolIsValid;
		} elseif( true == valStr( $this->getEndDate() ) && false == CValidation::validateDate( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Recurring transaction end date is invalid or end date should be in mm/dd/yyyy format.' ) );
			return $boolIsValid;
		}

		if( strtotime( $this->getEndDate() ) < strtotime( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Recurring transaction end date must be on or after the start date.' ) );
			return $boolIsValid;
		}

		if( strtotime( $this->getEndDate() ) < strtotime( $this->getNextPostDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End Date must be Today\'s date or a future date.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valLastPostedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHeaderNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAutoApproveInvoice() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAutoApprovePo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAutoCreateInvoice() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAutoCreatePo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOnHold() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumberOfOccurrences() {
		$boolIsValid = true;

		if( true == valStr( $this->getEndMethod() ) && 'number' == $this->getEndMethod() ) {
			if( true == is_null( $this->getNumberOfOccurrences() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', 'Number of occurrences is required.' ) );
			} elseif( false == is_numeric( $this->getNumberOfOccurrences() ) || 0 == $this->getNumberOfOccurrences() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', 'Invalid number of occurrences.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valScheduledApTransactionDetails( $objClientDatabase, $boolIsScript ) {

		$boolIsValid									= true;
		$arrstrProperties 								= [];
		$arrintPropertyIds 								= [];
		$arrintUnassociatedPropertyIds					= [];
		$arrstrDisabledProperties						= [];
		$arrstrDisabledGlAccounts						= [];
		$arrobjGlAccountPropertiesGroupedByGlAccount	= [];
		$fltScheduledApTransactionDetailsTotal			= 0.00;
		$arrobjScheduledApTransactionsByGlAccountIds	= rekeyObjects( 'GlAccountId', $this->m_arrobjScheduledApTransactionDetails );

		// property and vendor location association
		foreach( $this->m_arrintScheduledApTransactionDetailsPropertyIds as $intKey => $intPropertyId ) {

			if( true == valArr( $this->getCompanyUserPropertyIds() ) && false == in_array( $intPropertyId, $this->getCompanyUserPropertyIds() ) ) {
				unset( $this->m_arrintScheduledApTransactionDetailsPropertyIds[$intKey] );
				continue;
			}

			if( false == array_key_exists( $intPropertyId, $this->m_arrobjApPayeeProperties )
				|| ( true == array_key_exists( $intPropertyId, $this->m_arrobjApPayeeProperties )
					 && $this->getApPayeeLocationId() != $this->m_arrobjApPayeeProperties[$intPropertyId]->getApPayeeLocationId() ) ) {

				if( true == array_key_exists( $intPropertyId, $this->m_arrobjProperties ) ) {
					$arrstrProperties[] = $this->m_arrobjProperties[$intPropertyId]->getPropertyName();
				}
			}
		}

		if( true == valArr( $arrstrProperties ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The property \'' . implode( ', ', $arrstrProperties ) . '\' is not associated with selected vendor.' ) );
			return $boolIsValid;
		}

		if( false == valArr( $this->m_arrobjScheduledApTransactionDetails ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'At least one detail line item is required.' ) );
			return $boolIsValid;
		}

		if( true == valArr( $this->m_arrobjAssociatedGlAccounts ) ) {
			foreach( $this->m_arrobjAssociatedGlAccounts as $objAssociatedGlAccount ) {
				if( false == is_null( $objAssociatedGlAccount->getDisabledBy() ) && true == isset( $arrobjScheduledApTransactionsByGlAccountIds[$objAssociatedGlAccount->getId()] ) ) {
					$arrstrDisabledGlAccounts[$objAssociatedGlAccount->getId()] = $objAssociatedGlAccount->getAccountNumber() . ' : ' . $objAssociatedGlAccount->getAccountName();
				}
			}

			if( true == valArr( $arrstrDisabledGlAccounts ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The Gl Account(s) \'' . implode( ', ', $arrstrDisabledGlAccounts ) . '\' are disabled.' ) );
				return $boolIsValid;
			}
		}

		foreach( $this->m_arrintScheduledApTransactionDetailsPropertyIds as $intScheduledApTransactionDetailPropertyId ) {

			if( true == array_key_exists( $intScheduledApTransactionDetailPropertyId, $this->m_arrobjProperties ) ) {
				if( 0 < $this->m_arrobjProperties[$intScheduledApTransactionDetailPropertyId]->getIsDisabled() ) {
					$arrstrDisabledProperties[] = $this->m_arrobjProperties[$intScheduledApTransactionDetailPropertyId]->getPropertyName();
				}
			} else {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'This recurring transaction references a disabled property.' ) );
				return $boolIsValid;
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrDisabledProperties ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The property \'' . implode( ', ', $arrstrDisabledProperties ) . '\' is disabled.' ) );
			return $boolIsValid;
		}

		$arrintBankAccountIds = [];
		$arrobjPropertyBankAccountsGroupedByBankAccountIds = [];

		if( true == is_null( $this->getBankAccountId() ) && 1 == $this->getAutoCreateInvoice() ) {
			foreach( $this->m_arrobjPropertyGlSettings as $objPropertyGlSetting ) {

				if( true == is_null( $objPropertyGlSetting->getApBankAccountId() ) ) {
					$arrintPropertyIds[] = $objPropertyGlSetting->getPropertyId();
				} else {
					$arrintBankAccountIds[$objPropertyGlSetting->getPropertyId()] = $objPropertyGlSetting->getApBankAccountId();
				}
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {

				foreach( $arrintPropertyIds as $intKey => $intPropertyId ) {
					$arrstrProperties[] = $this->m_arrobjProperties[$intPropertyId]->getPropertyName();
				}

				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The property \'' . implode( ', ', array_unique( $arrstrProperties ) ) . '\' does not have an assigned default bank account.' ) );
			}

		} else {
			// Get List Of all Properties which is not associated with selected bank if bank account is selected other than default

			$arrintBankAccountIds[] = $this->getBankAccountId();
			$arrintUnassociatedBankPropertyIds 	= array_diff( $this->m_arrintScheduledApTransactionDetailsPropertyIds, $this->m_arrintAssociatedBankPropertyIds );

			$arrintUnassociatedPropertyIds = array_unique( array_merge( $arrintUnassociatedBankPropertyIds, $arrintPropertyIds ) );
		}

		if( true == valArr( $arrintUnassociatedPropertyIds ) && 1 == $this->m_intAutoCreateInvoice ) {

			$arrstrProperties = [];

			foreach( $arrintUnassociatedPropertyIds as $intKey => $intUnassociatedPropertyId ) {
				$arrstrProperties[] = $this->m_arrobjProperties[$intUnassociatedPropertyId]->getPropertyName();
			}
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The property \'' . implode( ', ', $arrstrProperties ) . '\' is not associated with selected or default bank account.' ) );
		}

		$arrobjPropertyBankAccounts = ( array ) CPropertyBankAccounts::fetchPropertyBankAccountsByBankAccountIdsByCid( array_filter( $arrintBankAccountIds ), $this->getCid(), $objClientDatabase, true );

		foreach( $arrobjPropertyBankAccounts as $intBankAccountId => $objPropertyBankAccount ) {
				$arrobjPropertyBankAccountsGroupedByBankAccountIds[$objPropertyBankAccount->getBankAccountId()][$objPropertyBankAccount->getPropertyId()] = $objPropertyBankAccount;
		}

		$arrobjPropertyGlSettings = rekeyObjects( 'PropertyId', $this->m_arrobjPropertyGlSettings );

		if( true == valArr( $this->m_arrobjGlAccountProperties ) ) {
			foreach( $this->m_arrobjGlAccountProperties as $objGlAccountProperty ) {
				$arrobjGlAccountPropertiesGroupedByGlAccount[$objGlAccountProperty->getGlAccountId()][$objGlAccountProperty->getPropertyId()] = $objGlAccountProperty;
			}
		}

		foreach( $this->m_arrobjScheduledApTransactionDetails as $objScheduledApTransactionDetail ) {

			$fltScheduledApTransactionDetailsTotal = bcadd( $fltScheduledApTransactionDetailsTotal, $objScheduledApTransactionDetail->getTransactionAmount(), 2 );

			if( true == valArr( $this->getCompanyUserPropertyIds() ) && false == in_array( $objScheduledApTransactionDetail->getPropertyId(), $this->getCompanyUserPropertyIds() ) ) {
				continue;
			}

			if( true == $this->getAutoCreateInvoice() ) {

				// Prevent invoice from using AP's bucket gl account and bank's cash gl account

				if( true == array_key_exists( $objScheduledApTransactionDetail->getPropertyId(), $arrobjPropertyGlSettings )
					&& $objScheduledApTransactionDetail->getGlAccountId() == $arrobjPropertyGlSettings[$objScheduledApTransactionDetail->getPropertyId()]->getApGlAccountId() ) {

					if( true == $boolIsScript ) {
						$this->setIsAutoPostCurrentInvoice( false );
					} else {
						$boolIsValid		&= false;
						$objScheduledApTransactionDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', 'Invalid gl account used, "' . $objScheduledApTransactionDetail->getGlAccountName() . '" may not be used with property "' . $objScheduledApTransactionDetail->getPropertyName() . '".' ) );
					}
				}

				$intBankAccountId = $this->getBankAccountId();

				if( true == array_key_exists( $objScheduledApTransactionDetail->getPropertyId(), $arrintBankAccountIds ) ) {
					$intBankAccountId = $arrintBankAccountIds[$objScheduledApTransactionDetail->getPropertyId()];
				}

				if( true == array_key_exists( $intBankAccountId, $arrobjPropertyBankAccountsGroupedByBankAccountIds )
					&& true == array_key_exists( $objScheduledApTransactionDetail->getPropertyId(), $arrobjPropertyBankAccountsGroupedByBankAccountIds[$intBankAccountId] )
					&& $arrobjPropertyBankAccountsGroupedByBankAccountIds[$intBankAccountId][$objScheduledApTransactionDetail->getPropertyId()]->getBankGlAccountId() == $objScheduledApTransactionDetail->getGlAccountId() ) {

					if( true == $boolIsScript ) {
						$this->setIsAutoPostCurrentInvoice( false );
					} else {
						$boolIsValid &= false;
						$objScheduledApTransactionDetail->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', 'Invalid gl account used, "' . $objScheduledApTransactionDetail->getGlAccountName() . '" may not be used with property "' . $objScheduledApTransactionDetail->getPropertyName() . '".' ) );
					}
				}
			}

			if( true == is_numeric( $objScheduledApTransactionDetail->getGlAccountId() )
				&& true == is_numeric( $objScheduledApTransactionDetail->getPropertyId() )
				&& true == array_key_exists( $objScheduledApTransactionDetail->getGlAccountId(), $arrobjGlAccountPropertiesGroupedByGlAccount )
				&& false == array_key_exists( $objScheduledApTransactionDetail->getPropertyId(), $arrobjGlAccountPropertiesGroupedByGlAccount[$objScheduledApTransactionDetail->getGlAccountId()] ) ) {

				$strGlAccountNumber	= $this->m_arrobjAssociatedGlAccounts[$objScheduledApTransactionDetail->getGlAccountId()]->getAccountNumber();
				$strGlAccountName	= $this->m_arrobjAssociatedGlAccounts[$objScheduledApTransactionDetail->getGlAccountId()]->getAccountName();

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' GL account \'' . $strGlAccountNumber . ' : ' . $strGlAccountName . '\' is not associated with \'' . $this->m_arrobjProperties[$objScheduledApTransactionDetail->getPropertyId()]->getPropertyName() . '\' property.' ) );
				$boolIsValid &= false;
			}
		}

		if( 0 == bccomp( 0, $fltScheduledApTransactionDetailsTotal, 2 )
			|| 1 == bccomp( 0, $fltScheduledApTransactionDetailsTotal, 2 )
			|| 0 != bccomp( $fltScheduledApTransactionDetailsTotal, $this->getControlTotal(), 2 ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Please enter valid total amount.' ) );
		}

		if( true == is_numeric( $this->m_fltControlTotal ) && number_format( $this->m_fltControlTotal, 3 ) != number_format( $fltScheduledApTransactionDetailsTotal, 3 ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'The total must match the sum of the line items.' ) );
		}

		return $boolIsValid;
	}

	public function valGlPosting() {

		$boolIsValid 					= true;
		$arrstrGlLockedProperties		= [];
		$arrstrGlInactiveProperties		= [];

		$arrobjPropertyGlSettings = rekeyObjects( 'PropertyId', $this->m_arrobjPropertyGlSettings );

		foreach( $arrobjPropertyGlSettings as $objPropertyGlSetting ) {

			if( strtotime( $objPropertyGlSetting->getApLockMonth() ) >= strtotime( $this->getPostMonth() )
				|| strtotime( $objPropertyGlSetting->getGlLockMonth() ) >= strtotime( $this->getPostMonth() ) ) {
				$arrstrGlLockedProperties[] = getArrayElementByKey( $objPropertyGlSetting->getPropertyId(), $this->m_arrobjProperties )->getPropertyName();
			}

			if( false == $objPropertyGlSetting->getActivateStandardPosting() ) {
				$arrstrGlInactiveProperties[] = getArrayElementByKey( $objPropertyGlSetting->getPropertyId(), $this->m_arrobjProperties )->getPropertyName();
			}
		}

		if( true == valArr( $arrstrGlInactiveProperties ) && 1 == $this->getAutoCreateInvoice() ) {
			$arrstrGlInactiveProperties = array_unique( $arrstrGlInactiveProperties );
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Gl posting is not activated for property(s) \'' . implode( ', ', $arrstrGlInactiveProperties ) . '\'' ) );
		}

		if( true == valArr( $arrstrGlLockedProperties ) && 1 == $this->getAutoCreateInvoice() ) {
			$arrstrGlLockedProperties = array_unique( $arrstrGlLockedProperties );
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Post month is in locked period for property(s) \'' . implode( ', ', $arrstrGlLockedProperties ) . '\'' ) );
		}

		return $boolIsValid;
	}

	public function valUserAssignedProperties( $objClientDatabase ) {

		$boolIsValid = true;

		if( false == valArr( $this->getScheduledApTransactionDetails() ) ) {
			$this->setScheduledApTransactionDetails( $this->fetchScheduledApTransactionDetails( $objClientDatabase ) );
		}

		if( true == valArr( $this->getCompanyUserPropertyIds() ) && true == valArr( $this->getScheduledApTransactionDetails() ) ) {

			$arrintScheduledApTransactionDetailPropertyIds	= ( array ) array_filter( array_keys( rekeyObjects( 'PropertyId', $this->getScheduledApTransactionDetails() ) ) );

			if( false == valArr( $this->getProperties() ) ) {
				$arrobjProperties = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIdsByCid( $arrintScheduledApTransactionDetailPropertyIds, $this->getCid(), $objClientDatabase );
			} else {
				$arrobjProperties = ( array ) $this->getProperties();
			}

			$arrstrUnAssociatedCompanyUserProperties = [];

			foreach( $arrintScheduledApTransactionDetailPropertyIds as $intPropertyId ) {

				if( false == in_array( $intPropertyId, $this->getCompanyUserPropertyIds() ) ) {
					$arrstrUnAssociatedCompanyUserProperties[$intPropertyId] = $arrobjProperties[$intPropertyId]->getPropertyName();
				}
			}

			if( true == valArr( $arrstrUnAssociatedCompanyUserProperties ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Property(s) \'' . implode( ', ', $arrstrUnAssociatedCompanyUserProperties ) . '\' is/are not assigned to this user.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valDisable() {

		$boolIsValid = true;

		if( 1 == $this->getIsOnHold() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Scheduled transaction is already disabled.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valEnable() {

		$boolIsValid = true;

		if( 0 == $this->getIsOnHold() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Scheduled transaction is already enabled.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolIsScript = false, $objScheduledApTransactionHeader = NULL, $arrintOriginalScheduledApTransactionHeader = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valIsValidScheduler();
				$boolIsValid &= $this->valStartDate( $objClientDatabase, $objScheduledApTransactionHeader, $arrintOriginalScheduledApTransactionHeader );
				$boolIsValid &= $this->valScheduledApTransactionDetails( $objClientDatabase, $boolIsScript );
				$boolIsValid &= $this->valFrequencyInterval();
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valNumberOfOccurrences();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApPayee();
				$boolIsValid &= $this->valScheduledPoTypeId();
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valApPayeeTermId();
				$boolIsValid &= $this->valApPayeeLocationId();
				$boolIsValid &= $this->valUserAssignedProperties( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'approve':
				$boolIsValid &= $this->valStartDate( $objClientDatabase, $objScheduledApTransactionHeader );
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valScheduledApTransactionDetails( $objClientDatabase, $boolIsScript );
				$boolIsValid &= $this->valApPayeeLocationId();
				$boolIsValid &= $this->valApPayee();
				$boolIsValid &= $this->valUserAssignedProperties( $objClientDatabase );
				break;

			case 'process':
				$boolIsValid &= $this->valScheduledApTransactionDetails( $objClientDatabase, $boolIsScript );
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApPayee();
				$boolIsValid &= $this->valBankAccountId();
				$boolIsValid &= $this->valApPayeeTermId();
				$boolIsValid &= $this->valApPayeeLocationId();
				$boolIsValid &= $this->valGlPosting();
				break;

			case 'disable':
				$boolIsValid &= $this->valDisable();
				$boolIsValid &= $this->valUserAssignedProperties( $objClientDatabase );
				break;

			case 'enable':
				$boolIsValid &= $this->valEnable();
				$boolIsValid &= $this->valUserAssignedProperties( $objClientDatabase );
				break;

			case 'user_assigned_properties':
				$boolIsValid &= $this->valUserAssignedProperties( $objClientDatabase );
				break;

			default:
		}

		return $boolIsValid;
	}

	public function createScheduledApTransactionDetail() {

		$objScheduledApTransactionDetail = new CScheduledApTransactionDetail();

		$objScheduledApTransactionDetail->setScheduledApTransactionHeaderId( $this->getId() );
		$objScheduledApTransactionDetail->setCid( $this->getCid() );

		return $objScheduledApTransactionDetail;
	}

	public function createApHeader() {

		$objApHeader = new CApHeader();

		$objApHeader->setCid( $this->getCid() );
		$objApHeader->setScheduledApHeaderId( $this->getId() );
		$objApHeader->setApPayeeId( $this->getApPayeeId() );
		$objApHeader->setApPayeeLocationId( $this->getApPayeeLocationId() );

		return $objApHeader;
	}

	public function fetchScheduledApTransactionDetails( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CScheduledApTransactionDetails::createService()->fetchAllActiveScheduledApTransactionDetailsByScheduledApTransactionHeaderIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function calculateNextPostDate( $objScheduledApTransactionHeader = NULL, $objClientDatabase = NULL, $objFrequency = NULL ) {

		$strNextPostDate        = NULL;
		$strPostDate			= $objScheduledApTransactionHeader->getLastPostedDate();
		$strTodayDate           = date( 'm/d/Y', strtotime( CClients::fetchColumn( 'SELECT NOW() as time', 'time', $objClientDatabase ) ) );

		if( strtotime( $strPostDate ) < strtotime( $strTodayDate ) ) {
			while( strtotime( $strPostDate ) < strtotime( $strTodayDate ) ) {
			$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' +' . $objScheduledApTransactionHeader->getFrequencyInterval() . ' ' . $objFrequency->getIntervalFrequencyLabelPlural() ) );
			$strPostDate     = $strNextPostDate;
			}
		} elseif( strtotime( $strPostDate ) == strtotime( $strTodayDate ) ) {
			$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' +' . $objScheduledApTransactionHeader->getFrequencyInterval() . ' ' . $objFrequency->getIntervalFrequencyLabelPlural() ) );
		}

		if( strtotime( $strNextPostDate ) <= strtotime( $objScheduledApTransactionHeader->getEndDate() ) ) {
			return $strNextPostDate;
		} else {
			$this->setEndDate( $objScheduledApTransactionHeader->getEndDate() );
			return $strNextPostDate;
		}
	}

}
?>