<?php

class CSettingsTemplateProperty extends CBaseSettingsTemplateProperty {

	protected $m_intSettingsTemplateTypeId;
	protected $m_intUniqueSettingsCount;

	protected $m_strPropertyName;
	protected $m_strTemplateName;
	protected $m_intIsDefault;

    /**
     * Get Functions
     */

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

	public function getSettingTemplateTypeId() {
		return $this->m_intSettingsTemplateTypeId;
	}

	public function getUniqueSettingsCount() {
		return $this->m_intUniqueSettingsCount;
	}

	public function getTemplateName() {
        return $this->m_strTemplateName;
    }

    public function getIsDefault() {
    	return $this->m_intIsDefault;
    }

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
    	if( true == isset( $arrValues['setting_template_type_id'] ) ) $this->setSettingTemplateTypeId( $arrValues['setting_template_type_id'] );
    	if( true == isset( $arrValues['unique_settings_count'] ) ) $this->setUniqueSettingsCount( $arrValues['unique_settings_count'] );
    	if( true == isset( $arrValues['template_name'] ) ) $this->setTemplateName( $arrValues['template_name'] );
    	if( true == isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );

    }

    public function setPropertyName( $strPropertyName ) {
    	$this->m_strPropertyName = $strPropertyName;
    }

    public function setSettingTemplateTypeId( $intSettingsTemplateTypeId ) {
    	$this->m_intSettingsTemplateTypeId = $intSettingsTemplateTypeId;
    }

    public function setUniqueSettingsCount( $intUniqueSettingsCount ) {
    	$this->m_intUniqueSettingsCount = $intUniqueSettingsCount;
    }

    public function setTemplateName( $strTemplateName ) {
    	return $this->m_strTemplateName = $strTemplateName;
    }

    public function setIsDefault( $intIsDefault ) {
    	$this->m_intIsDefault = CStrings::strToIntDef( $intIsDefault, NULL, false );
    }

    /**
     * Validate Functions
     */

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'cid', 'client id is required.', NULL ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intPropertyId ) || 0 >= ( int ) $this->m_intPropertyId ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'property_id', 'Property id is required.', NULL ) );
        }

        return $boolIsValid;
    }

    public function valSettingsTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
    }

    public function valDuplicateSettingsTemplateProperty( $objDatabase ) {
    	$boolIsValid = true;
		$objSettingsTemplateProperty = CSettingsTemplateProperties::fetchSettingsTemplatePropertyBySettingTemplateTypeIdByPropertyIdByCid( $this->getSettingTemplateTypeId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objSettingsTemplateProperty, 'CSettingsTemplateProperty' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Property "' . $objSettingsTemplateProperty->getPropertyName() . '" is already associated to the same settings template type.' ) );
		}

    	return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valPropertyId();
        		$boolIsValid &= $this->valDuplicateSettingsTemplateProperty( $objDatabase );
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>