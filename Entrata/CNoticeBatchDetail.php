<?php

class CNoticeBatchDetail extends CBaseNoticeBatchDetail {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNoticeBatchId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemEmailId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerEmailAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailEventTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemEmailResponse() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsArchived() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsHandDelivered() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>