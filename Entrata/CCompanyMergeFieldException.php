<?php

class CCompanyMergeFieldException extends CBaseCompanyMergeFieldException {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyMergeFieldId() {
		$boolIsValid = true;

		$strCompanyMergeFieldId = trim( $this->getCompanyMergeFieldId() );

		if( true == is_null( $strCompanyMergeFieldId ) || 0 == strlen( $strCompanyMergeFieldId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Company Merge Field is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyGroupIds() {
		$boolIsValid = true;

		$arrintPropertyIds = ( array ) $this->getPropertyGroupIds();

		if( false == valArr( $arrintPropertyIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_ids', 'Select atleast one property.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valExceptionName() {
		$boolIsValid = true;

		$strExceptionName = trim( $this->getExceptionName() );

		$boolIsValid = true;

		if( true == is_null( $this->getExceptionName() ) || 0 == strlen( $strExceptionName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Exception Name is required.' ) );
			return $boolIsValid;
		}

		if( false == is_null( $objDatabase ) ) {
			$objExistingCompanyMergeFieldException = CCompanyMergeFieldException::fetchCCompanyMergeFieldExceptionByExceptionNameByCid( $strExceptionName, $this->getCid(), $objDatabase );
			if( true == valObj( $objExistingCompanyMergeFieldException, 'CCompanyMergeFieldException' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Exception name is already in use.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;

	}

	public function valDefaultValue() {
		$boolIsValid = true;

		$strDefaultValue = trim( $this->getDefaultValue() );

		if( true == is_null( $this->getDefaultValue() ) || 0 == strlen( $strDefaultValue ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_value', 'Default Value is required.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valCompanyMergeFieldId( $objDatabase );
			$boolIsValid &= $this->valExceptionName( $objDatabase );
			$boolIsValid &= $this->valDefaultValue();
			$boolIsValid &= $this->valPropertyGroupIds( $objDatabase );

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>