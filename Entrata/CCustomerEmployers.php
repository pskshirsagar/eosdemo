<?php

class CCustomerEmployers extends CBaseCustomerEmployers {

	public static function fetchTotalMonthlyAmountDataByCidByCustomerIdsByIncomeTypeIds( $intCid, $arrintCustomerIds, $arrintIncomeTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;
		if( false == valArr( $arrintIncomeTypeIds ) ) return NULL;

		 $strSql = 'SELECT
						ce.customer_id, sum( ce.gross_salary ) as total_monthly_amount
					FROM
						customer_employers as ce
					WHERE
						ce.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ')
						AND ce.cid = ' . ( int ) $intCid . '
						AND ce.income_type_id IN ( ' . implode( ',', $arrintIncomeTypeIds ) . ' )
					GROUP BY
						ce.customer_id';

		$arrmixData = fetchData( $strSql, $objDatabase );

		$arrintApplicantTotalMonthlyAmounts = [];
		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $arrstrData ) {
				$arrintApplicantTotalMonthlyAmounts[$arrstrData['customer_id']] = [ 'customer_id'          => $arrstrData['customer_id'],
                    'total_monthly_amount' => $arrstrData['total_monthly_amount'] ];
			}
		}

		return $arrintApplicantTotalMonthlyAmounts;
	}

}
?>