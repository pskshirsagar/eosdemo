<?php

class CQuoteLeaseTerm extends CBaseQuoteLeaseTerm {

    public $m_strName;
    public $m_strTermMonth;
    public $m_strSpecialNames;
    public $m_objRentScheduledCharge;
	protected $m_strLeaseTermExpiryDate;

    public $m_arrobjSpecials;

    public $m_arrstrStandardSpecialNames;
    public $m_arrstrIncentiveSpecialNames;

    public $m_intLeaseId;
    public $m_intRentArCodeId;
    public $m_intLeaseExpiringCount;
    public $m_intBaseRentScheduledChargeId;

    protected $m_fltNewRentAmount;
    protected $m_fltAddOnRentAmount;
    protected $m_fltTotalOfferAmount;
    protected $m_fltAmenityRentAmount;
    protected $m_fltCurrentRentAmount;
    protected $m_fltRentChangePercent;
    protected $m_fltRentDiscountAmount;
    protected $m_fltCalculatedRentAmount;
    protected $m_fltTotalOfferCurrentAmount;

    protected $m_boolExceedsExpirationLimit;

    /**
     * Set Functions
     */

    public function setLeaseId( $intLeaseId ) {
        $this->m_intLeaseId = $intLeaseId;
    }

    public function setSpecialNames( $strSpecialNames ) {
		$this->m_strSpecialNames = $strSpecialNames;
    }

    public function setName( $strName ) {
    	$this->m_strName = $strName;
    }

    public function setTermMonth( $strTermMonth ) {
    	$this->m_strTermMonth = $strTermMonth;
    }

    public function setSpecials( $arrobjSpecials ) {
    	$this->m_arrobjSpecials = $arrobjSpecials;
    }

    public function setRentScheduledCharge( $objRentScheduledCharge ) {
		$this->m_objRentScheduledCharge = $objRentScheduledCharge;
    }

    public function setStandardSpecialNames( $arrstrSpecialNames ) {
		$this->m_arrstrStandardSpecialNames = $arrstrSpecialNames;
    }

    public function setIncentiveSpecialNames( $arrstrSpecialNames ) {
    	$this->m_arrstrIncentiveSpecialNames = $arrstrSpecialNames;
    }

    public function setCurrentRentAmount( $fltCurrentRentAmount ) {
    	$this->m_fltCurrentRentAmount = $fltCurrentRentAmount;
    }

    public function setNewRentAmount( $fltNewRentAmount ) {
    	$this->m_fltNewRentAmount = $fltNewRentAmount;
    }

    public function setTotalOfferAmount( $intTotalOfferAmount ) {
    	$this->m_fltTotalOfferAmount = $intTotalOfferAmount;
    }

    public function setTotalOfferCurrentAmount( $intTotalOfferCurrentAmount ) {
    	$this->m_fltTotalOfferCurrentAmount = $intTotalOfferCurrentAmount;
    }

    public function setRentChangePercent( $fltRentChangePercent ) {
    	$this->m_fltRentChangePercent = $fltRentChangePercent;
    }

    public function setRentDiscountAmount( $fltRentDiscountAmount ) {
    	$this->m_fltRentDiscountAmount = $fltRentDiscountAmount;
    }

    public function setAddOnRentAmount( $fltAddOnRentAmount ) {
    	$this->m_fltAddOnRentAmount = $fltAddOnRentAmount;
    }

    public function setAmenityRentAmount( $fltAmenityRentAmount ) {
    	$this->m_fltAmenityRentAmount = $fltAmenityRentAmount;
    }

    public function setExceedsExpirationLimit( $boolExceedsExpirationLimit ) {
    	$this->m_boolExceedsExpirationLimit = $boolExceedsExpirationLimit;
    }

    public function setLeaseExpiringCount( $intLeaseExpiringCount ) {
    	$this->m_intLeaseExpiringCount = $intLeaseExpiringCount;
    }

    public function setBaseRentScheduledChargeId( $intBaseRentScheduledChargeId ) {
    	$this->m_intBaseRentScheduledChargeId = $intBaseRentScheduledChargeId;
    }

	public function setIsBaseRentSelected( $boolIsBaseRentSelected ) {
		$this->m_boolIsBaseRentSelected = $boolIsBaseRentSelected;
	}

	public function setCalculatedRentAmount( $fltCalculatedRentAmount ) {
    	$this->m_fltCalculatedRentAmount = $fltCalculatedRentAmount;
	}

	public function setRentArCodeId( $intRentArCodeId ) {
    	$this->m_intRentArCodeId = $intRentArCodeId;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['name'] ) )
    		$this->setName( $arrmixValues['name'] );
    	if( true == isset( $arrmixValues['term_month'] ) )
    		$this->setTermMonth( $arrmixValues['term_month'] );
    	if( true == isset( $arrmixValues['special_names'] ) )
    		$this->setSpecialNames( $arrmixValues['special_names'] );
    	if( true == isset( $arrmixValues['exceeds_expiration_limit'] ) )
    		$this->setExceedsExpirationLimit( $arrmixValues['exceeds_expiration_limit'] );
	    if( true == isset( $arrmixValues['lease_expiring_count'] ) )
		    $this->setLeaseExpiringCount( $arrmixValues['lease_expiring_count'] );
	    if( true == isset( $arrmixValues['base_rent_scheduled_charge_id'] ) )
		    $this->setBaseRentScheduledChargeId( $arrmixValues['base_rent_scheduled_charge_id'] );
	    if( true == isset( $arrmixValues['new_rent_amount'] ) )
		    $this->setNewRentAmount( $arrmixValues['new_rent_amount'] );
	    if( true == isset( $arrmixValues['is_base_rent_selected'] ) )
		    $this->setIsBaseRentSelected( $arrmixValues['is_base_rent_selected'] );
	    if( true == isset( $arrmixValues['amenity_rent_amount'] ) )
		    $this->setAmenityRentAmount( $arrmixValues['amenity_rent_amount'] );
	    if( true == isset( $arrmixValues['calculated_rent_amount'] ) )
		    $this->setCalculatedRentAmount( $arrmixValues['calculated_rent_amount'] );
	    if( true == isset( $arrmixValues['rent_ar_code_id'] ) )
		    $this->setRentArCodeId( $arrmixValues['rent_ar_code_id'] );
	    if( true == isset( $arrmixValues['lease_id'] ) )
		    $this->setLeaseId( $arrmixValues['lease_id'] );
	    if( true == isset( $arrmixValues['lease_term_expiry_date'] ) )
			$this->setLeaseTermExpiryDate( $arrmixValues['lease_term_expiry_date'] );
		if( true == isset( $arrmixValues['end_date'] ) )
			$this->setEndDate( $arrmixValues['end_date'] );
		if( true == isset( $arrmixValues['renewal_start_date'] ) )
			$this->setRenewalStartDate( $arrmixValues['renewal_start_date'] );
	}

	/**
	 * @param array $strLeaseTermExpiryDate
	 */
	public function setLeaseTermExpiryDate( $strLeaseTermExpiryDate ) {
		$this->m_strLeaseTermExpiryDate = $strLeaseTermExpiryDate;
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function setRenewalStartDate( $strRenewalStartDate ) {
		$this->set( 'm_strRenewalStartDate', CStrings::strTrimDef( $strRenewalStartDate, -1, NULL, true ) );
	}

    /**
     * get Functions
     */

    public function getLeaseId() {
    	return $this->m_intLeaseId;
    }

    public function getSpecialNames() {
		return $this->m_strSpecialNames;
    }

    public function getName() {
    	return $this->m_strName;
    }

    public function getTermMonth() {
    	return $this->m_strTermMonth;
    }

    public function getSpecials() {
    	return $this->m_arrobjSpecials;
    }

    public function getRentScheduledCharge() {
    	return $this->m_objRentScheduledCharge;
    }

    public function getIncentiveSpecialNames() {
    	return $this->m_arrstrIncentiveSpecialNames;
    }

    public function getStandardSpecialNames() {
    	return $this->m_arrstrStandardSpecialNames;
    }

    public function getCurrentRentAmount() {
    	return $this->m_fltCurrentRentAmount;
    }

    public function getNewRentAmount() {
    	return $this->m_fltNewRentAmount;
    }

    public function getTotalOfferAmount() {
    	return $this->m_fltTotalOfferAmount;
    }

    public function getTotalOfferCurrentAmount() {
    	return $this->m_fltTotalOfferCurrentAmount;
    }

    public function getRentChangePercent() {
    	return $this->m_fltRentChangePercent;
    }

    public function getRentDiscountAmount() {
    	return $this->m_fltRentDiscountAmount;
    }

    public function getAddOnRentAmount() {
    	return $this->m_fltAddOnRentAmount;
    }

    public function getAmenityRentAmount() {
    	return $this->m_fltAmenityRentAmount;
    }

    public function getExceedsExpirationLimit() {
    	return $this->m_boolExceedsExpirationLimit;
    }

    public function getLeaseExpiringCount() {
		return $this->m_intLeaseExpiringCount;
    }

	public function getBaseRentScheduledChargeId() {
		return $this->m_intBaseRentScheduledChargeId;
	}

	public function getIsBaseRentSelected() {
		return $this->m_boolIsBaseRentSelected;
	}

	public function getCalculatedRentAmount() {
		return $this->m_fltCalculatedRentAmount;
	}

	public function getRentArCodeId() {
		return $this->m_intRentArCodeId;
	}

	/**
	 * @return string
	 */
	public function getLeaseTermExpiryDate() {
		return $this->m_strLeaseTermExpiryDate;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function getRenewalStartDate() {
		return $this->m_strRenewalStartDate;
	}

	/**
     * Add Functions
     */

    public function addSpecial( $objSpecial ) {
    	$this->m_arrobjSpecials[$objSpecial->getId()] = $objSpecial;
    }

    public function addIncentiveSpecialName( $strSpecialName ) {

    	$this->m_arrstrIncentiveSpecialNames[] = $strSpecialName;
    }

    public function addStandardSpecialName( $strSpecialName ) {
    	$this->m_arrstrStandardSpecialNames[] = $strSpecialName;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valQuoteId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseTermId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == valId( $this->getId() ) || false == valId( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function delete( $intCompanyUserId, $objDatabase, $boolIsSoftDelete = false, $boolIsDeleteSchedueledCharges = false ) {

		if( true == $boolIsDeleteSchedueledCharges && false == $this->deleteScheduledCharges( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCompanyUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCompanyUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCompanyUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( $this->update( $intCompanyUserId, $objDatabase ) ) {
			return true;
		}
	}

	public function deleteScheduledCharges( $intCompanyUserId, $objDatabase ) {
		$arrobjScheudledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByQuoteLeaseTermIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$boolIsValid = true;

		foreach( $arrobjScheudledCharges as $objScheduledCharge ) {
			$boolIsValid &= $objScheduledCharge->delete( $intCompanyUserId, $objDatabase );
		}

		return $boolIsValid;
	}

}
?>