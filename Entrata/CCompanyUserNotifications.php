<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserNotifications
 * Do not add any new functions to this class.
 */

class CCompanyUserNotifications extends CBaseCompanyUserNotifications {

	public static function fetchCompanyUserNotificationsByCidByCompanyUserId( $intCid, $intCompanyUserId, $objDatabase, $arrstrTags = [] ) {

		$strWhere = '';
		if( true == valArr( $arrstrTags ) ) {
			$strWhere = ' AND cun.tags && \'{' . implode( ',', $arrstrTags ) . '}\'';
		}

		$strSql = 'SELECT
						cun.id,
						cun.cid,
						cun.message,
						cun.tags,
						cun.sent_on,
						cun.read_on,
						cun.created_on,
						cun.expires_on::timestamp,
						lc.lease_id,
						lc.customer_id
					FROM
						company_user_notifications cun
						LEFT JOIN lease_customers lc ON cun.cid = lc.cid AND util_to_int( cun.message->\'description\'->>\'lease_id\' ) = lc.lease_id AND util_to_int( cun.message->\'description\'->>\'customer_id\' ) = lc.customer_id
					WHERE
						cun.cid = ' . ( int ) $intCid . '
						AND cun.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND ( cun.expires_on IS NULL OR cun.expires_on > now() )
						' . $strWhere . '
					ORDER BY
						id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserNotificationsDetailsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cun.id,
						cun.cid,
						cun.company_user_id,
						cun.tags,
						cun.message,
						array_agg( cunm.module_id::text ) as module_ids,
						cun.expires_on,
						cun.snooze_until,
						cun.sent_on
					FROM
						company_user_notifications cun
						LEFT JOIN company_user_notification_modules cunm ON cun.id = cunm.company_user_notification_id AND cun.cid = cunm.cid
					WHERE
						cun.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND cun.cid = ' . ( int ) $intCid . '
					GROUP BY
						cun.id,
						cun.cid,
						cun.company_user_id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserNotificationsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $boolIsSmsFromCampaign = false ) {

		$strFieldName = ( true == $boolIsSmsFromCampaign ) ? '\'campaign_target_id\'' : '\'application_id\'';

		$strWhereSql1 = '';
		$strWhereSql2 = ' OR ( \'SMS_CHAT\' = ANY ( cun.tags ) AND cun.read_on IS NULL ) ';

		if( true == valArr( $arrintApplicationIds ) ) {
			$strWhereSql1 .= ' AND CASE WHEN 
								( \'CHAT\' = ANY ( cun.tags ) OR ( \'SMS_CHAT\' = ANY ( cun.tags ) ) ) THEN
								cun.message -> \'description\' ->> ' . $strFieldName . ' <> \'null\' AND (  cun.message -> \'description\' ->> ' . $strFieldName . ' )::INTEGER IN (' . implode( ',', $arrintApplicationIds ) . ') ELSE FALSE END';
		} else {
			$strWhereSql2 = ' OR ( \'SMS_CHAT\' = ANY ( cun.tags ) AND cun.read_on IS NOT NULL ) ';
		}

		$strSql = 'SELECT
						*
					FROM
						company_user_notifications cun
					WHERE
						cun.cid = ' . ( int ) $intCid . '
						AND ( \'CHAT\' = ANY ( cun.tags ) ' . $strWhereSql2 . ' ) AND ( cun.expires_on IS NULL OR cun.expires_on > now() ) ' . $strWhereSql1;

		return ( array ) self::fetchCompanyUserNotifications( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserNotificationByCompanyUserIdsByApplicationIdByCid( $arrintCompanyUserIds, $intCid, $intApplicationId, $objDatabase, $boolIsFromCampaign = false ) {

		if( false == valArr( $arrintCompanyUserIds ) ) {
			return [];
		}
		$strMessageField = "'application_id'";
		if( true == $boolIsFromCampaign ) {
			$strMessageField = "'campaign_target_id'";
		}

		$strSql = 'SELECT
						DISTINCT company_user_id
					FROM
						company_user_notifications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
						AND read_on IS NOT NULL
						AND expires_on > now()
						AND ( \'SMS_CHAT\' = ANY ( tags )
							AND ( ( ( message ->> \'description\' )::json ) ->>' . $strMessageField . ' )::INTEGER = ' . ( int ) $intApplicationId . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSmsChatCompanyUserNotificationsByCompanyUserIdByApplicationIdByCid( $intCompanyUserId, $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_user_notifications
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND ( read_on IS NULL OR expires_on > now() )
						AND ( \'SMS_CHAT\' = ANY ( tags )
							AND ( ( ( message ->> \'description\' )::json ) ->> \'application_id\' )::INTEGER = ' . ( int ) $intApplicationId . ' )';

		return self::fetchCompanyUserNotifications( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserNotificationsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_user_notifications cun
					WHERE
						cun.cid = ' . ( int ) $intCid . '
						AND read_on IS NOT NULL
						AND ( expires_on IS NULL OR expires_on > now() )
						AND \'SMS_CHAT\' = ANY ( tags )
						AND ( ( ( message ->> \'description\' )::json ) ->> \'application_id\' )::INTEGER = ' . ( int ) $intApplicationId . '
					LIMIT 1';

		return self::fetchCompanyUserNotification( $strSql, $objDatabase );
	}

	public static function fetchEntrataCallNotificationsByCallIdByCid( $intCallId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_user_notifications cun
					WHERE
						cun.cid = ' . ( int ) $intCid . '
						AND \'CALL_INCOMING\' = ANY ( tags )
						AND ( ( ( message ->> \'description\' )::json ) ->> \'call_id\' )::INTEGER = ' . ( int ) $intCallId;

		return ( array ) self::fetchCompanyUserNotifications( $strSql, $objDatabase );
	}

	public static function encodeUtfCharacters( $strString ) {
		if( false == valStr( $strString ) ) {
			return $strString;
		}
		return mb_convert_encoding( $strString, 'UTF-7', 'auto' );
	}

	public static function decodeUtfCharacters( $strString ) {
		if( false == valStr( $strString ) ) {
			return $strString;
		}
		return mb_convert_encoding( $strString, 'UTF-8', 'UTF-7' );
	}

	public static function fetchCompanyUserNotificationsByCampaignTargetIdByCid( $intCampaignTargetId, $intCid, $objDatabase, $boolIsAllActive = false ) {
		if( false == valId( $intCampaignTargetId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strIsNotificationRead = ( false == $boolIsAllActive ) ? ' AND read_on IS NOT NULL' : '';
		$strLimitCondition = ( false == $boolIsAllActive ) ? ' LIMIT 1' : '';

		$strSql = 'SELECT
						*
					FROM
						company_user_notifications cun
					WHERE
						cun.cid = ' . ( int ) $intCid . '
						' . $strIsNotificationRead . '
						AND ( expires_on IS NULL OR expires_on > now() )
						AND \'SMS_CHAT\' = ANY ( tags )
						AND ( message::json->\'description\'->>\'campaign_target_id\')::INTEGER = ' . ( int ) $intCampaignTargetId . $strLimitCondition;

		return ( false == $boolIsAllActive )? self::fetchCompanyUserNotification( $strSql, $objDatabase ) : self::fetchCompanyUserNotifications( $strSql, $objDatabase );
	}

}
?>