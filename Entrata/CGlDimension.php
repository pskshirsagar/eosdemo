<?php

class CGlDimension extends CBaseGlDimension {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlDimensionGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultGlDimensionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Tag name is required.' ) ) );
		}

		if( true == $boolIsValid ) {

			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							AND id <> ' . ( int ) $this->getId() . '
							AND deleted_by IS NULL';

			if( 0 < \Psi\Eos\Entrata\CGlDimensions::createService()->fetchGlDimensionCount( $strWhere, $objDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Tag name \'{%s,0}\' already exists.', [ $this->getName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode( $boolIsCodeExists ) {
		$boolIsValid = true;

		if( true === $boolIsCodeExists ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Custom Tag Code must be a unique value.', [ $this->getCode() ] ) ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $boolIsCodeExists = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valCode( $boolIsCodeExists );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>