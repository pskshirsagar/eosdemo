<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CNotificationProperties
 * Do not add any new functions to this class.
 */

class CNotificationProperties extends CBaseNotificationProperties {

    public static function fetchNotificationProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return self::fetchCachedObjects( $strSql, 'CNotificationProperty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false, $boolIsReturnKeyedArray );
    }

    public static function fetchNotificationProperty( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CNotificationProperty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchNotificationPropertyDetailsByNotificationIdByCid( $intNotificationId, $intCid, $objDatabase ) {

    	$strSql = 'SELECT
    					pg.id, pg.name as property_name
    				FROM
    					notification_properties np
    					JOIN property_groups pg ON ( pg.cid = np.cid AND pg.id = np.property_group_id )
    				WHERE
						np.cid = ' . ( int ) $intCid . '
						AND np.notification_id = ' . ( int ) $intNotificationId . '
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
    				ORDER BY
						lower ( pg.name )';
    	$arrstrData = fetchData( $strSql, $objDatabase );

    	$arrstrFinalData = [];

    	if( true == valArr( $arrstrData ) ) {
    		foreach( $arrstrData as $arrstrChunk ) {

    			$arrstrFinalData[] = [ 'id' => $arrstrChunk['id'], 'property_name' => $arrstrChunk['property_name'] ];
    		}
    	}

    	return $arrstrFinalData;
   	}

}
?>