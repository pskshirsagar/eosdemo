<?php

class CFileBatchEmail extends CBaseFileBatchEmail {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileBatchId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valToEmailAddress() {
        $boolIsValid = true;

        if( false == valStr( $this->getToEmailAddress() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter send to email addresses.' ) ) );
        	return $boolIsValid;
        }

        if( false == CValidation::validateEmailAddresses( $this->getToEmailAddress() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter valid send to email addresses.' ) ) );
        	return $boolIsValid;
        }

        return $boolIsValid;
    }

    public function valFromEmailDisplay() {
        $boolIsValid = true;

        if( false == valStr( $this->getFromEmailDisplay() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter from email address.' ) ) );
        	return $boolIsValid;
        }

        if( false == CValidation::validateEmailAddresses( $this->getFromEmailDisplay() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter valid from email address.' ) ) );

        	return $boolIsValid;
        }

        if( 1 < \Psi\Libraries\UtilFunctions\count( array_filter( explode( ';', $this->getFromEmailDisplay() ) ) ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter valid from email address.' ) ) );

        	return $boolIsValid;
        }

        return $boolIsValid;
    }

    public function valReplyToEmailAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailMessage() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailSubject() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileExportStatusId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExpiresOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valFromEmailDisplay();
        		$boolIsValid &= $this->valToEmailAddress();
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function getEmailTemplateContent( $strRecipientEmailAddress, $objCompanyLogo, $strArchiveFileName = '', $objClientDatabase ) {
    	$strUserName 	  = '';

    	$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $this->getCreatedBy(), $this->getCid(), $objClientDatabase );

    	if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
    		if( true == valStr( $objCompanyEmployee->getNameFirst() ) && true == valStr( $objCompanyEmployee->getNameLast() ) ) {
    			$strUserName = $objCompanyEmployee->getNameFirst() . ' ' . $objCompanyEmployee->getNameLast();
    		} else {
    			$strUserName = $objCompanyEmployee->getUserName();
    		}
    	}

    	$strEncryptedDocAccUrl = $this->getEncryptedDocAccUrl( $strRecipientEmailAddress );
    	$strEncryptedDocAccUrl = ( true == valStr( $strEncryptedDocAccUrl ) ? $strEncryptedDocAccUrl  : '' );

    	require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );
    	$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

    	$objSmarty->assign_by_ref( 'company_logo',	$objCompanyLogo );
    	$objSmarty->assign( 'custom_message',		$this->getEmailMessage() );
    	$objSmarty->assign( 'expire_on',			$this->getExpiresOn() );
    	$objSmarty->assign( 'media_library_uri',	CONFIG_MEDIA_LIBRARY_PATH );
    	$objSmarty->assign( 'encrypted_docacc_uri',	$strEncryptedDocAccUrl );
    	$objSmarty->assign( 'user_name',			$strUserName );
    	$objSmarty->assign( 'archive_file_name',	$strArchiveFileName );

    	if( 'production' == CONFIG_ENVIRONMENT ) {
    		$objSmarty->assign( 'domain_postfix',		CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN );
    	} else {
    		$objSmarty->assign( 'domain_postfix',		'http://' . CConfig::get( entrata_domain ) );
    	}

    	$strHtmlContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/document_management/email_invitation.tpl' );
    	return $strHtmlContent;
    }

    public function getEncryptedDocAccUrl( $strRecipientEmailAddress ) {
    	$strUrlParam = 'doc_access[cid]=' . $this->getCid();
    	$strUrlParam .= '&doc_access[recipient_email_address]=' . trim( $strRecipientEmailAddress );
    	$strUrlParam .= '&doc_access[file_batch_id]=' . $this->getFileBatchId();

    	$strEncryptedDocAccUrl  = urlencode( base64_encode( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( ( $strUrlParam ), CONFIG_SODIUM_KEY_MESSAGE_CENTER_EMAIL ) ) );
    	$strModuleLink 			= CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/docaccess?key=';
    	$strEncryptedDocAccUrl	= $strModuleLink . $strEncryptedDocAccUrl;

    	return $strEncryptedDocAccUrl;
    }

}
?>