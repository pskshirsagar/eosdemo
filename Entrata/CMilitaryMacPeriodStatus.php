<?php

class CMilitaryMacPeriodStatus extends CBaseMilitaryMacPeriodStatus {

	const NEW_MAC_PERIOD				= 1;
	const MIMO_FILE_GENERATED			= 2;
	const MIMO_FILE_ACCEPTED			= 3;
	const PROMO_DEMO_FILE_UPLOADED		= 4;
	const PROMO_DEMO_FILE_PROCESSES		= 5;
	const PAYMENTS_UPLOADED				= 6;
	const PAYMENTS_POSTED				= 7;

	public static $c_arrintCurrentMilitaryMacPeriodStatuses = [
		self::NEW_MAC_PERIOD,
		self::MIMO_FILE_GENERATED,
		self::MIMO_FILE_ACCEPTED,
		self::PROMO_DEMO_FILE_UPLOADED,
		self::PROMO_DEMO_FILE_PROCESSES,
		self::PAYMENTS_UPLOADED
	];

	public static $c_arrintMilitaryMacPeriodStatuses = [
		self::NEW_MAC_PERIOD,
		self::MIMO_FILE_GENERATED,
		self::MIMO_FILE_ACCEPTED,
		self::PROMO_DEMO_FILE_UPLOADED,
		self::PROMO_DEMO_FILE_PROCESSES,
		self::PAYMENTS_UPLOADED,
		self::PAYMENTS_POSTED
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>