<?php

class CJobUnitTypePhase extends CBaseJobUnitTypePhase {

	protected $m_strJobPhaseName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;

		if( true == is_null( $this->getJobId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_id', 'Job Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_type_id', 'Unit Type Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valJobPhaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitCount() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitCount() ) && 0 > $this->getUnitCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_type_id', 'Invalid unit count.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valJobId();
				$boolIsValid &= $this->valUnitTypeId();
				$boolIsValid &= $this->valUnitCount();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setJobPhaseName( $strJobPhaseName ) {
		$this->set( 'm_strJobPhaseName', CStrings::strTrimDef( $strJobPhaseName, 50, NULL, true ) );
	}

	public function getJobPhaseName() {
		return $this->m_strJobPhaseName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['job_phase_name'] ) ) $this->setJobPhaseName( $arrmixValues['job_phase_name'] );
	}

}
?>