<?php

class CCompanyUserQuiz extends CBaseCompanyUserQuiz {

	const PASSING_SCORE	= 80;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserAssessmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpQuizId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>