<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApplicationAddOnCategories
 * Do not add any new functions to this class.
 */

class CCompanyApplicationAddOnCategories extends CBaseCompanyApplicationAddOnCategories {

	public static function fetchCompanyApplicationAddOnCategoryByCompanyApplicationIdByPropertyIdByCid( $intCompanyApplicationId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_application_add_on_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_application_id = ' . ( int ) $intCompanyApplicationId . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchCompanyApplicationAddOnCategory( $strSql, $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoriesByCompanyApplicationIdsByPropertyIdByCid( $arrintCompanyApplicationIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_application_add_on_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND company_application_id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' ) ';

		return self::fetchCompanyApplicationAddOnCategories( $strSql, $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoriesByCompanyApplicationIdsByAddOnGroupIdByPropertyIdByCid( $arrintCompanyApplicationIds, $intAddOnGroupId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_application_add_on_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_application_id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId . '
						AND add_on_group_id = ' . ( int ) $intAddOnGroupId;

		return self::fetchCompanyApplicationAddOnCategories( $strSql, $objDatabase );
	}

	public static function fetchCompanyApplicationAddOnCategoriesByCompanyApplicationIdsByAddOnCategoryIdByPropertyIdByCid( $arrintCompanyApplicationIds, $intAddOnCategoryId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						company_application_add_on_categories
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_application_id IN ( ' . implode( ',', $arrintCompanyApplicationIds ) . ' )
						AND property_id = ' . ( int ) $intPropertyId . '
						AND add_on_category_id = ' . ( int ) $intAddOnCategoryId;

		return self::fetchCompanyApplicationAddOnCategories( $strSql, $objDatabase );
	}

}
?>
