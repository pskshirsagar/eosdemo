<?php

class CJobStatus extends CBaseJobStatus {

	const BUDGETING     = 1;
	const PENDING       = 2;
	const APPROVED      = 3;
	const IN_PROGRESS   = 4;
	const COMPLETED     = 5;
	const CLOSED	    = 6;
	const ON_HOLD	    = 7;
	const CANCELLED	    = 8;

	public static $c_arrintActiveJobStatuses = [
		self::BUDGETING  => 'Budgeting',
		self::PENDING	=> 'Pending',
		self::APPROVED	=> 'Approved',
		self::IN_PROGRESS	=> 'In Progress',
		self::COMPLETED	=> 'Completed',
		self::CLOSED	=> 'Closed',
		self::ON_HOLD	=> 'On Hold',
		self::CANCELLED	=> 'Cancelled'
	];

	public static $c_arrintOpenJobStatusIds = [
		self::BUDGETING,
		self::PENDING,
		self::APPROVED,
		self::IN_PROGRESS,
		self::ON_HOLD
	];

	public static $c_arrintAllowedFinancialJobStatusIds = [
		self::BUDGETING,
		self::PENDING,
		self::APPROVED,
		self::IN_PROGRESS,
		self::COMPLETED,
		self::ON_HOLD
	];

	public static $c_arrintAllowedApContractJobStatusIds = [
		self::BUDGETING,
		self::PENDING,
		self::APPROVED,
		self::IN_PROGRESS,
		self::ON_HOLD
	];

	public static $c_arrintTemplateJobStatusIds = [
		self::APPROVED	=> 'Approved',
		self::IN_PROGRESS	=> 'In Progress',
		self::COMPLETED	=> 'Completed',
		self::CLOSED	=> 'Closed',
		self::ON_HOLD	=> 'On Hold',
		self::CANCELLED	=> 'Cancelled'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
