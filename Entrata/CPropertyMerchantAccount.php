<?php

class CPropertyMerchantAccount extends CBasePropertyMerchantAccount {

	const DEFAULT_MAX_VISA_PAYMENT_AMOUNT = 500;

	protected $m_intCommercialMerchantAccountId;
	protected $m_intMerchantProcessingTypeId;

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['commercial_merchant_account_id'] ) && $boolDirectSet ) {
			$this->m_intCommercialMerchantAccountId = trim( $arrmixValues['commercial_merchant_account_id'] );
		} elseif( isset( $arrmixValues['commercial_merchant_account_id'] ) ) {
			$this->setCommercialMerchantAccountId( $arrmixValues['commercial_merchant_account_id'] );
		}

		if( isset( $arrmixValues['merchant_processing_type_id'] ) && $boolDirectSet ) {
			$this->m_intMerchantProcessingTypeId = trim( $arrmixValues['merchant_processing_type_id'] );
		} elseif( isset( $arrmixValues['merchant_processing_type_id'] ) ) {
			$this->setMerchantProcessingTypeId( $arrmixValues['merchant_processing_type_id'] );
		}
	}

	public function setCommercialMerchantAccountId( $intCommercialMerchantAccountId ) {
		$this->m_intCommercialMerchantAccountId = $intCommercialMerchantAccountId;
	}

	public function getCommercialMerchantAccountId() {
		return $this->m_intCommercialMerchantAccountId;
	}

	public function setMerchantProcessingTypeId( $intMerchantProcessingTypeId ) {
		$this->m_intMerchantProcessingTypeId = $intMerchantProcessingTypeId;
	}

	public function getMerchantProcessingTypeId() {
		return $this->m_intMerchantProcessingTypeId;
	}

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArCodeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyMerchantAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDisabledBy() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function valDisabledOn() {
        $boolIsValid = true;

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>