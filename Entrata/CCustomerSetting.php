<?php

class CCustomerSetting extends CBaseCustomerSetting {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( NULL, 'Id', 'Invalid Customer Setting Request:  Id required', NULL ) );
		}

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'Cid', 'Invalid Customer Setting Request:  Management id required', NULL ) );
		}

        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'Customer Id', 'Invalid Customer Setting Request:  Customer Id required', NULL ) );
        }

        return $boolIsValid;
    }

    public function valKey() {
        $boolIsValid = true;

        if( false == isset( $this->m_strKey ) || 0 == strlen( $this->m_strKey ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'Key', 'Invalid Customer Setting Request:  Key required', NULL ) );
        }

        return $boolIsValid;
    }

    public function valValue() {
        $boolIsValid = true;

        // Validation example
        // if( false == isset( $this->m_strValue ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valKey();
				break;

            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valKey();
				break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchCustomerById( $intCustomerId, $objClientDatabase ) {
    	return $objCustomers = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $intCustomerId, $this->getCid(), $objClientDatabase );
    }

}
?>