<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultChoreTypes
 * Do not add any new functions to this class.
 */

class CDefaultChoreTypes extends CBaseDefaultChoreTypes {

	public static function fetchDefaultChoreTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultChoreType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultChoreType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultChoreType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}
}
?>