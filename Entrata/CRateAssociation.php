<?php

class CRateAssociation extends CBaseRateAssociation {

	protected $m_boolIsRequirePropertyApproval;

	protected $m_intFileId;
	protected $m_intSpecialId;
	protected $m_intIsReservedByDay;
	protected $m_intFileExtensionId;
	protected $m_intMaxReservationTimeLimit;
	protected $m_intSpecialQuantityRemaining;
	protected $m_intReservationFeeOccurrenceTypeId;

	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strAmenityName;
	protected $m_strAmenityRate;
	protected $m_strSpecialName;
	protected $m_strRpDescription;
	protected $m_strRateLeaseTermIds;
	protected $m_strAmenityAvailability;
	protected $m_strBlackoutHolidays;
	protected $m_boolIsFutureAllowed;
	protected $m_intFutureDaysAllowed;
	protected $m_strFullSizeUri;

	/**
	 * Get Functions
	 */

	public function getRequirePropertyApproval() {
		return $this->m_boolIsRequirePropertyApproval;
	}

	public function getIsReservedByDay() {
		return $this->m_intIsReservedByDay;
	}

	public function getReservationFeeOccurrenceTypeId() {
		return $this->m_intReservationFeeOccurrenceTypeId;
	}

	public function getMaxReservationTimeLimit() {
		return $this->m_intMaxReservationTimeLimit;
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function getAmenityName() {
		return $this->m_strAmenityName;
	}

	public function getRpDescription() {
		return $this->m_strRpDescription;
	}

	public function getAmenityRate() {
		return $this->m_strAmenityRate;
	}

	public function getSpecialId() {
		return $this->m_intSpecialId;
	}

	public function getSpecialQuantityRemaining() {
		return $this->m_intSpecialQuantityRemaining;
	}

	public function getSpecialName() {
		return $this->m_strSpecialName;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getRateLeaseTermIds() {
		return $this->m_strRateLeaseTermIds;
	}

	public function getAmenityAvailability() {
		return $this->m_strAmenityAvailability;
	}

	public function getBlackoutHolidays() {
		return $this->m_strBlackoutHolidays;
	}

	public function getIsFutureAllowed() {
		return $this->m_boolIsFutureAllowed;
	}

	public function getFutureDaysAllowed() {
		return $this->m_intFutureDaysAllowed;
	}

	public function getFullSizeUri() {
		return $this->m_strFullSizeUri;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['require_property_approval'] ) ) $this->setRequirePropertyApproval( $arrmixValues['require_property_approval'] );
		if( true == isset( $arrmixValues['is_reserved_by_day'] ) ) $this->setIsReservedByDay( $arrmixValues['is_reserved_by_day'] );
		if( true == isset( $arrmixValues['reservation_fee_occurrence_type_id'] ) ) $this->setReservationFeeOccurrenceTypeId( $arrmixValues['reservation_fee_occurrence_type_id'] );
		if( true == isset( $arrmixValues['max_reservation_time_limit'] ) ) $this->setMaxReservationTimeLimit( $arrmixValues['max_reservation_time_limit'] );
		if( true == isset( $arrmixValues['file_id'] ) ) $this->setFileId( $arrmixValues['file_id'] );
		if( true == isset( $arrmixValues['file_extension_id'] ) ) $this->setFileExtensionId( $arrmixValues['file_extension_id'] );
		if( true == isset( $arrmixValues['amenity_name'] ) ) $this->setAmenityName( $arrmixValues['amenity_name'] );
		if( true == isset( $arrmixValues['rp_description'] ) ) $this->setRpDescription( $arrmixValues['rp_description'] );
		if( true == isset( $arrmixValues['amenity_rate'] ) ) $this->setAmenityRate( $arrmixValues['amenity_rate'] );
		if( true == isset( $arrmixValues['special_id'] ) ) $this->setSpecialId( $arrmixValues['special_id'] );
		if( true == isset( $arrmixValues['special_name'] ) ) $this->setSpecialName( $arrmixValues['special_name'] );
		if( true == isset( $arrmixValues['special_quantity_remaining'] ) ) $this->setSpecialQuantityRemaining( $arrmixValues['special_quantity_remaining'] );
		if( true == isset( $arrmixValues['file_name'] ) )	$this->setFileName( $arrmixValues['file_name'] );
		if( true == isset( $arrmixValues['file_path'] ) )	$this->setFilePath( $arrmixValues['file_path'] );
		if( true == isset( $arrmixValues['lease_term_ids'] ) )	$this->setRateLeaseTermIds( $arrmixValues['lease_term_ids'] );
		if( true == isset( $arrmixValues['amenity_availability'] ) )	$this->setAmenityAvailability( $arrmixValues['amenity_availability'] );
		if( true == isset( $arrmixValues['blackout_holidays'] ) )	$this->setBlackoutHolidays( $arrmixValues['blackout_holidays'] );
		if( true == isset( $arrmixValues['is_future_allowed'] ) )	$this->setIsFutureAllowed( $arrmixValues['is_future_allowed'] );
		if( true == isset( $arrmixValues['reservable_within_days'] ) )	$this->setFutureDaysAllowed( $arrmixValues['reservable_within_days'] );
		if( true == isset( $arrmixValues['fullsize_uri'] ) )	$this->setFullSizeUri( $arrmixValues['fullsize_uri'] );
	}

	public function setRequirePropertyApproval( $boolIsRequirePropertyApproval ) {
		$this->m_boolIsRequirePropertyApproval = $boolIsRequirePropertyApproval;
	}

	public function setIsReservedByDay( $intIsReservedByDay ) {
		$this->m_intIsReservedByDay = CStrings::strToIntDef( $intIsReservedByDay, NULL, false );
	}

	public function setReservationFeeOccurrenceTypeId( $intReservationFeeOccurrenceTypeId ) {
		$this->m_intReservationFeeOccurrenceTypeId = CStrings::strToIntDef( $intReservationFeeOccurrenceTypeId, NULL, false );
	}

	public function setMaxReservationTimeLimit( $intMaxReservationTimeLimit ) {
		$this->m_intMaxReservationTimeLimit = CStrings::strToIntDef( $intMaxReservationTimeLimit, NULL, false );
	}

	public function setFileId( $intFileId ) {
		$this->m_intFileId = $intFileId;
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->m_intFileExtensionId = $intFileExtensionId;
	}

	public function setAmenityName( $strAmenityName ) {
		$this->m_strAmenityName = $strAmenityName;
	}

	public function setRpDescription( $strRpDescription ) {
		$this->m_strRpDescription = $strRpDescription;
	}

	public function setAmenityRate( $strAmenityRate ) {
		$this->m_strAmenityRate = $strAmenityRate;
	}

	public function setSpecialId( $intSpecialId ) {
		$this->m_intSpecialId = $intSpecialId;
	}

	public function setSpecialQuantityRemaining( $intSpecialQuantityRemaining ) {
		$this->m_intSpecialQuantityRemaining = $intSpecialQuantityRemaining;
	}

	public function setSpecialName( $strSpecialName ) {
		$this->m_strSpecialName = $strSpecialName;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setRateLeaseTermIds( $strRateLeaseTermIds ) {
		$this->m_strRateLeaseTermIds = $strRateLeaseTermIds;
	}

	public function setAmenityAvailability( $strAmenityAvailability ) {
		$this->m_strAmenityAvailability = $strAmenityAvailability;
	}

	public function setBlackoutHolidays( $strBlackoutHolidays ) {
		$this->m_strBlackoutHolidays = $strBlackoutHolidays;
	}

	public function setIsFutureAllowed( $boolIsFutureAllowed ) {
		$this->m_boolIsFutureAllowed = $boolIsFutureAllowed;
	}

	public function setFutureDaysAllowed( $intFutureDaysAllowed ) {
		$this->m_intFutureDaysAllowed = $intFutureDaysAllowed;
	}

	public function setFullSizeUri( $strFullSizeUri ) {
		$this->m_strFullSizeUri = $strFullSizeUri;
	}

	/**
	 * Create Functions
	 */

	public static function createConcreteRateAssociation( $arrmixValues ) {

		if( false == valArr( $arrmixValues ) || false == array_key_exists( 'ar_origin_id', $arrmixValues ) ) {
			return new CRateAssociation();
		}

		switch( $arrmixValues['ar_origin_id'] ) {
			case CArOrigin::AMENITY:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::AMENITY ) . 'RateAssociation';
				break;

			case CArOrigin::PET:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::PET ) . 'RateAssociation';
				break;

			case CArOrigin::ADD_ONS:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::ADD_ONS ) . 'RateAssociation';
				break;

			case CArOrigin::RISK_PREMIUM:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::RISK_PREMIUM ) . 'RateAssociation';
				break;

			case CArOrigin::SPECIAL:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::SPECIAL ) . 'RateAssociation';
				break;

			case CArOrigin::MAINTENANCE:
				$strClassName = 'C' . CArOrigin::getIdToCamelizeName( CArOrigin::MAINTENANCE ) . 'RateAssociation';
				break;

			default:
				$strClassName = 'CRateAssociation';
				break;
		}

		return new $strClassName();
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * DELETE RATE ASSOCIATION - DELETE THE RATES
	 */

	public function deleteRateAssociation( $intCurrentUserId, $objDatabase, $intSpecialGroupId = NULL, $intSpecialRecipientId = NULL ) {
		if( false == valId( $this->getPropertyId() ) || false == valId( $this->getArCascadeId() ) || false == valId( $this->getArOriginId() ) || false == valId( $this->getArOriginReferenceId() ) ) return false;

		$objRatesCriteria	= new CRatesCriteria();

		$objRatesCriteria->setCid( $this->getCid() );
		$objRatesCriteria->setPropertyId( $this->getPropertyId() );
		$objRatesCriteria->setArCascadeId( $this->getArCascadeId() );
		$objRatesCriteria->setArCascadeReferenceId( $this->getArCascadeReferenceId() );

		$objRatesCriteria->setArOriginId( $this->getArOriginId() );
		$objRatesCriteria->setArOriginReferenceId( $this->getArOriginReferenceId() );

		if( true == valId( $intSpecialGroupId ) ) {
			$objRatesCriteria->setRequireSpecialGroups( true );
			$objRatesCriteria->setSpecialGroupIds( [ $intSpecialGroupId ] );
		}

		if( true == valId( $intSpecialRecipientId ) && true == in_array( $intSpecialRecipientId, [ CSpecialRecipient::RENEWALS, CSpecialRecipient::RESIDENTS ] ) ) {
			$objRatesCriteria->setIsRenewal( true );
		} else {
			$objRatesCriteria->setIsRenewal( false );
		}

		$objRatesLibrary = new CRatesLibrary( $objRatesCriteria );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( false == $objRatesLibrary->deleteRates( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}

				if( false == parent::delete( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}
		}

		return $boolIsValid;
	}

}
?>