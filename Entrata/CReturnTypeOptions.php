<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReturnTypeOptions
 * Do not add any new functions to this class.
 */

class CReturnTypeOptions extends CBaseReturnTypeOptions {

	public static function fetchCustomReturnTypeOptionsByCid( $intCid, $objDatabase ) {

        $strSql = 'SELECT
						rto.*,
						CASE
							WHEN rt.id < 100 THEN \'ACH/eCheck\'
							WHEN ( rt.id > 200 AND rt.id < 207 ) THEN \'Credit Card\'
							ELSE \'Check21\'
						END as payment_type,
						rt.name as return_type_name,
						rt.description as return_type_description,
						rt.occurrence_percent as return_type_occurrence_percent
					FROM
						return_type_options rto
						JOIN return_types rt ON ( rto.return_type_id = rt.id )
					WHERE
						rto.cid = ' . ( int ) $intCid . '
		    		ORDER BY
						payment_type ASC, rt.name ASC';

        return self::fetchReturnTypeOptions( $strSql, $objDatabase );
    }

    public static function fetchReturnTypeOptionByReturnTypeIdByCid( $intReturnTypeId, $intCid, $objDatabase ) {
        $strSql = 'SELECT * FROM return_type_options WHERE cid=' . ( int ) $intCid . ' AND return_type_id = ' . ( int ) $intReturnTypeId . ' LIMIT 1';
        return self::fetchReturnTypeOption( $strSql, $objDatabase );
    }

    public static function fetchCustomReturnTypeOptionsByIdByCid( $arrintReturnTypeOptionIds, $intCid, $objDatabase ) {

        $strSql = 'SELECT
						rto.*,
						CASE
							WHEN rt.id < 100 THEN \'ACH/eCheck\'
							WHEN ( rt.id > 200 AND rt.id < 207 ) THEN \'Card\'
							ELSE \'Check21\'
						END as payment_type,
						rt.name as return_type_name,
						rt.description as return_type_description
					FROM
						return_type_options rto
						JOIN return_types rt ON rto.return_type_id=rt.id
					WHERE
						cid =' . ( int ) $intCid . '
						AND rto.id IN (' . implode( ',', $arrintReturnTypeOptionIds ) . ')';

        return self::fetchReturnTypeOptions( $strSql, $objDatabase );
    }

}
?>