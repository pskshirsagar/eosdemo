<?php

class CJobPhaseUnit extends CBaseJobPhaseUnit {

	protected $m_strUnitNumber;
	protected $m_intUpgradedUnitTypeId;
	protected $m_intMaintenanceTemplateId;
	protected $m_intRenovationDaysToComplete;
	protected $m_strJobName;

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function getUpgradedUnitTypeId() {
		return $this->m_intUpgradedUnitTypeId;
	}

	public function setUpgradedUnitTypeId( $intUpgradedUnitTypeId ) {
		$this->m_intUpgradedUnitTypeId = $intUpgradedUnitTypeId;
	}

	public function getMaintenanceTemplateId() {
		return $this->m_intMaintenanceTemplateId;
	}

	public function setMaintenanceTemplateId( $intMaintenanceTemplateId ) {
		$this->m_intMaintenanceTemplateId = $intMaintenanceTemplateId;
	}

	public function getRenovationDaysToComplete() {
		return $this->m_intRenovationDaysToComplete;
	}

	public function setRenovationDaysToComplete( $intRenovationDaysToComplete ) {
		$this->m_intRenovationDaysToComplete = $intRenovationDaysToComplete;
	}

	public function getJobName() {
		return $this->m_strJobName;
	}

	public function setJobName( $strJobName ) {
		$this->m_strJobName = $strJobName;
	}

	public function valJobId() {
		$boolIsValid = true;

		if( true == is_null( $this->getJobId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_id', 'Job Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_type_id', 'Unit Type Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId( $objJob, $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyUnitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Property unit Id is required.' ) );
		} elseif( CJobType::JOB_TYPE_ROLLING_RENOVATION == $objJob->getJobTypeId() ) {
			$arrmixJobPhaseDetails = ( array ) Psi\Eos\Entrata\CJobPhaseUnits::createService()->fetchActiveRenovationJobPhaseByPropertyUnitIdByExcludeJobPhaseIdByCid( $this->getPropertyUnitId(), $this->getJobPhaseId(), $this->getCid(), $objDatabase );

			if( true == valArr( $arrmixJobPhaseDetails ) ) {
				$boolIsValid = false;
				$arrmixJobPhase = current( $arrmixJobPhaseDetails );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Unit ' . $arrmixJobPhase['unit_number'] . ' is already part of an active renovation (' . $arrmixJobPhase['job_name'] . ' - ' . $arrmixJobPhase['job_phase_name'] . ').' ) );
			}
		}

		return $boolIsValid;
	}

	public function valJobStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objJob = NULL, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valJobId();
				$boolIsValid &= $this->valUnitTypeId();
				$boolIsValid &= $this->valPropertyUnitId( $objJob, $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( $boolDirectSet && isset( $arrmixValues['unit_number'] ) ) {
			$this->set( 'm_strUnitNumber', trim( $arrmixValues['unit_number'] ) );
		} elseif( isset( $arrmixValues['unit_number'] ) ) {
			$this->setUnitNumber( trim( $arrmixValues['unit_number'] ) );
		}

		if( $boolDirectSet && isset( $arrmixValues['upgraded_unit_type_id'] ) ) {
			$this->set( 'm_intUpgradedUnitTypeId', trim( $arrmixValues['upgraded_unit_type_id'] ) );
		} elseif( isset( $arrmixValues['upgraded_unit_type_id'] ) ) {
			$this->setUpgradedUnitTypeId( trim( $arrmixValues['upgraded_unit_type_id'] ) );
		}

		if( $boolDirectSet && isset( $arrmixValues['maintenance_template_id'] ) ) {
			$this->set( 'm_intMaintenanceTemplateId', trim( $arrmixValues['maintenance_template_id'] ) );
		} elseif( isset( $arrmixValues['maintenance_template_id'] ) ) {
			$this->setMaintenanceTemplateId( trim( $arrmixValues['maintenance_template_id'] ) );
		}

		if( $boolDirectSet && isset( $arrmixValues['renovation_days_to_complete'] ) ) {
			$this->set( 'm_intRenovationDaysToComplete', trim( $arrmixValues['renovation_days_to_complete'] ) );
		} elseif( isset( $arrmixValues['renovation_days_to_complete'] ) ) {
			$this->setRenovationDaysToComplete( trim( $arrmixValues['renovation_days_to_complete'] ) );
		}

		if( $boolDirectSet && isset( $arrmixValues['job_name'] ) ) {
			$this->set( 'm_strJobName', trim( $arrmixValues['job_name'] ) );
		} elseif( isset( $arrmixValues['job_name'] ) ) {
			$this->setJobName( trim( $arrmixValues['job_name'] ) );
		}

	}

	public function createJobPhaseUnitComment() : \CJobPhaseUnitComment {

		$objJobPhaseUnitComment = new CJobPhaseUnitComment();
		$objJobPhaseUnitComment->setCid( $this->getCid() );
		$objJobPhaseUnitComment->setJobPhaseId( $this->getJobPhaseId() );
		$objJobPhaseUnitComment->setJobPhaseUnitId( $this->getId() );

		return $objJobPhaseUnitComment;
	}

}
?>