<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COccupations
 * Do not add any new functions to this class.
 */

class COccupations extends CBaseOccupations {

	public static function fetchAssociatedOccupationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						op.*
				   FROM occupations as op
						INNER JOIN property_occupations as po ON( op.id = po.occupation_id AND op.cid = po.cid )
				   WHERE
				   		op.cid = ' . ( int ) $intCid . '
				   		AND po.property_id = ' . ( int ) $intPropertyId . '
				   		AND op.is_published = 1';

		return self::fetchOccupations( $strSql, $objDatabase );
	}

	public static function fetchUnassociatedOccupationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						op.*
					FROM occupations as op
						LEFT OUTER JOIN property_occupations as po ON ( op.id = po.occupation_id AND op.cid = po.cid  AND po.property_id = ' . ( int ) $intPropertyId . ')
					WHERE
						po.id IS NULL
						AND	op.cid = ' . ( int ) $intCid . '
						AND op.is_published = 1';

		return self::fetchOccupations( $strSql, $objDatabase );
	}

	public static function fetchDuplicateOccupationByCidByName( $intCid, $strName, $intOccupationId, $objDatabase ) {

		$strSql 	= 'SELECT * FROM occupations WHERE	cid = ' . ( int ) $intCid;

		if( 0 < $intOccupationId ) {
			$strSql .= ' AND id != ' . ( int ) $intOccupationId;
		}

		$strSql 	.= ' AND lower(name)=\'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $strName ) ) ) . '\' LIMIT 1';

		return self::fetchOccupation( $strSql, $objDatabase );
	}

	public static function fetchOccupationNameByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						name
					FROM
						occupations
				   	WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId . '
						LIMIT 1';

		return parent::fetchColumn( $strSql, 'name', $objDatabase );
	}

	public static function fetchActiveOccupationsByNamesByCid( $arrstrOccupations, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrOccupations ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						occupations
					WHERE
						LOWER( name ) IN (\'' . \Psi\CStringService::singleton()->strtolower( implode( '\',\'', $arrstrOccupations ) ) . '\')
						AND cid = ' . ( int ) $intCid . '
						AND is_published = 1';

		return self::fetchOccupations( $strSql, $objDatabase );
	}

}
?>