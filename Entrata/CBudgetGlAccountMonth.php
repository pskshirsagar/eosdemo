<?php

class CBudgetGlAccountMonth extends CBaseBudgetGlAccountMonth {

	protected $m_intGlAccountTypeId;
	protected $m_strAccountNumber;
	protected $m_strAccountName;
	protected $m_strGlDimensionName;
	protected $m_arrintGlDimensionIds;
	protected $m_objGlDimension;
	protected $m_strStartDate;
	protected $m_strEndDate;

	// Get Functions

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getGlDimensionName() {
		return $this->m_strGlDimensionName;
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	// Set Functions

	// Name field from gl_group_sub_types

	public function setGlAccountTypeId( $intId ) {
		$this->m_intGlAccountTypeId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = CStrings::strTrimDef( $strAccountNumber, 15, NULL, true );
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = CStrings::strTrimDef( $strAccountName, 50, NULL, true );
	}

	public function setGlDimensionName( $strGlDimensionName ) {
		$this->m_strGlDimensionName = CStrings::strTrimDef( $strGlDimensionName, 50, NULL, true );
	}

	public function setStartDate( $strStartDate ) {
		$this->m_strStartDate = CStrings::strTrimDef( $strStartDate, 15, NULL, true );
	}

	public function setEndDate( $strEndDate ) {
		$this->m_strEndDate = CStrings::strTrimDef( $strEndDate, 15, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['gl_account_type_id'] ) && $boolDirectSet )
			$this->m_intGlAccountTypeId = trim( $arrmixValues['gl_account_type_id'] );
		elseif ( isset( $arrmixValues['gl_account_type_id'] ) )
			$this->setGlAccountTypeId( $arrmixValues['gl_account_type_id'] );
		if( isset( $arrmixValues['account_number'] ) && $boolDirectSet )
			$this->m_strAccountNumber = trim( stripcslashes( $arrmixValues['account_number'] ) );
		elseif ( isset( $arrmixValues['account_number'] ) )
			$this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_number'] ) : $arrmixValues['account_number'] );
		if( isset( $arrmixValues['account_name'] ) && $boolDirectSet )
			$this->m_strAccountName = trim( stripcslashes( $arrmixValues['account_name'] ) );
		elseif ( isset( $arrmixValues['account_name'] ) )
			$this->setAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['account_name'] ) : $arrmixValues['account_name'] );
		if( isset( $arrmixValues['gl_dimension_name'] ) && $boolDirectSet )
			$this->m_strGlDimensionName = trim( stripcslashes( $arrmixValues['gl_dimension_name'] ) );
		elseif ( isset( $arrmixValues['gl_dimension_name'] ) )
			$this->setGlDimensionName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['gl_dimension_name'] ) : $arrmixValues['gl_dimension_name'] );

		if( isset( $arrmixValues['start_date'] ) && $boolDirectSet ) {
			$this->m_strStartDate = trim( stripcslashes( $arrmixValues['start_date'] ) );
		} elseif( isset( $arrmixValues['start_date'] ) ) {
			$this->setStartDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['start_date'] ) : $arrmixValues['start_date'] );
		}

		if( isset( $arrmixValues['end_date'] ) && $boolDirectSet ) {
			$this->m_strEndDate = trim( stripcslashes( $arrmixValues['end_date'] ) );
		} elseif( isset( $arrmixValues['end_date'] ) ) {
			$this->setEndDate( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['end_date'] ) : $arrmixValues['end_date'] );
		}
	}

	// Validate

	public function valBudgetId() {
		$boolIsValid = true;

		if( true == is_null( $this->getBudgetId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'budget_id', 'Budget Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getGlAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', 'GL Account Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;

		if( true == is_null( $this->getPostMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', 'Month is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', 'Amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valBudgetId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId();
				$boolIsValid &= $this->valPostMonth();
				$boolIsValid &= $this->valAmount();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>