<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWaitListPoints
 * Do not add any new functions to this class.
 */

class CWaitListPoints extends CBaseWaitListPoints {

	public static function fetchWaitListPointsByPropertyGroupIdByOccupancyTypeIdByCid( $intPropertyGroupId, $intOccupancyTypeId, $intCid, $objDatabase, $strOrder = 'DESC' ) {

		if( false == valId( $intPropertyGroupId ) || false == valId( $intOccupancyTypeId ) ) {
			return false;
		}

		$strJoinAskSQL = '';
		$strSelectAskSql = '';
		if( COccupancyType::MILITARY != $intOccupancyTypeId ) {
			$strJoinAskSQL = 'JOIN application_setting_keys ask ON ( wlp.application_setting_key_id = ask.id )';
			$strSelectAskSql = ',ask.id as key_id, ask.label as criteria';
		}

		$strSql = 'SELECT
                        wlp.id,
                        wlp.name,
                        wlp.value,
                        wlp.details
                        ' . $strSelectAskSql . '
				   FROM 
				        wait_list_points wlp
				        JOIN wait_lists wl ON ( wlp.cid = wl.cid AND wlp.wait_list_id = wl.id ) 
				        ' . $strJoinAskSQL . ' 
				   WHERE 
				        wl.cid = ' . ( int ) $intCid . '
						AND wl.property_group_id = ' . ( int ) $intPropertyGroupId . '
						AND wl.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND wlp.deleted_by IS NULL
				   ORDER BY
						wlp.value ' . $strOrder;

		return fetchData( $strSql,  $objDatabase );
	}

	public static function fetchWaitListPointAndSettingKeyByIdByCid( $intId, $intOccupancyTypeId, $intCid, $objDatabase ) {
		if( true == valId( $intOccupancyTypeId ) && COccupancyType::MILITARY != $intOccupancyTypeId ) {
			$strJoinAskSQL = 'JOIN application_setting_keys ask ON ( wlp.application_setting_key_id = ask.id )';
			$strSelectAskSql = ',ask.key as application_setting_key, ask.application_setting_group_id as application_setting_group_id';
		}
		$strSql = ' SELECT 
                        wlp.*
                        ' . $strSelectAskSql . '
                    FROM 
                        wait_list_points wlp
                        ' . $strJoinAskSQL . '
                    WHERE 
                        wlp.id = ' . ( int ) $intId . '
                        AND cid =' . ( int ) $intCid;

		return self::fetchWaitListPoint( $strSql, $objDatabase );
	}

	public static function updateWaitlistPointsOnApplications( $intWaitListId, $intPropertyId, $intOccupancyTypeId, $intCid, $intCompanyUserId, $objDatabase ) {

		$strSql = 'SELECT * FROM update_wait_list_points_on_cached_applications( ' . ( int ) $intCid . ', ' . ( int ) $intWaitListId . ', ' . ( int ) $intPropertyId . ', ' . ( int ) $intOccupancyTypeId . ', ' . ( int ) $intCompanyUserId . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchWaitListPointsByWaitListIdByCidByValue( $intWaitListId, $intCid, $intValue, $objDatabase ) {

		$strSql = '	SELECT 
		                id,
		                value,
		                name
		            FROM 
		                wait_list_points wlp
		            WHERE 
		                wlp.wait_list_id = ' . ( int ) $intWaitListId . ' 
		                AND wlp.cid = ' . ( int ) $intCid . ' 
		                AND wlp.value > ' . ( int ) $intValue . '
						AND wlp.deleted_on IS NULL';

		return self::fetchWaitListPoints( $strSql, $objDatabase );
	}

	public static function fetchWaitListPointByPropertyGroupIdByOccupancyTypeIdByApplicationSettingKeyIdByCid( $intPropertyGroupId, $intOccupancyTypeId, $intApplicationSettingKeyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyGroupId ) || false == valId( $intOccupancyTypeId ) || false == valId( $intApplicationSettingKeyId ) ) return false;

		$strSql = 'SELECT
						wlp.*
					FROM 
						wait_list_points wlp
						JOIN wait_lists wl ON ( wlp.cid = wl.cid AND wlp.wait_list_id = wl.id ) 
						JOIN application_setting_keys ask ON ( wlp.application_setting_key_id = ask.id ) 
					WHERE 
						wl.cid = ' . ( int ) $intCid . '
						AND wl.property_group_id = ' . ( int ) $intPropertyGroupId . '
						AND wl.occupancy_type_id = ' . ( int ) $intOccupancyTypeId . '
						AND wlp.application_setting_key_id = ' . ( int ) $intApplicationSettingKeyId . '
						AND wlp.deleted_by IS NULL';

		return self::fetchWaitListPoint( $strSql, $objDatabase );
	}

	public static function fetchAllWaitListPointsByWaitListIdByCid( $intWaitListId, $intCid, $objDatabase, $boolIncludeDeletedPoints = false, $strOrder = 'DESC' ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedPoints ) ? ' AND deleted_on IS NULL' : 'AND deleted_on IS NOT NULL';
			$strSql = '	SELECT 
		               *
		                FROM 
		                    wait_list_points
			            WHERE 
	    	                wait_list_id = ' . ( int ) $intWaitListId . ' 
		                    AND cid = ' . ( int ) $intCid . ' 
						' . $strCheckDeletedUnitsSql . '
						ORDER BY 
							value ' . $strOrder;

				return self::fetchWaitListPoints( $strSql, $objDatabase );
	}

	public static function fetchWaitListPointsNameByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = '	SELECT 
		                wlp.name
		            FROM 
		                wait_list_applications wla
		                JOIN wait_list_points wlp ON ( wla.cid = wlp.cid AND wla.wait_list_point_id = wlp.id )
		            WHERE
		                wla.cid = ' . ( int ) $intCid . '
		                AND wla.application_id = ' . ( int ) $intApplicationId . ' 
		                AND wla.deleted_on IS NULL';

		return self::fetchWaitListPoints( $strSql, $objDatabase );
	}

}

?>