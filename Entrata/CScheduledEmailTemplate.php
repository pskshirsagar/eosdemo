<?php

class CScheduledEmailTemplate extends CBaseScheduledEmailTemplate {

    public function valId() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getId())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ));
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getCid())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ));
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getName())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ));
        // }

        return $boolIsValid;
    }

    public function valPath() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPath())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', '' ));
        // }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getDescription())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ));
        // }

        return $boolIsValid;
    }

    public function valPreviewImageUri() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getPreviewImageUri())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'preview_image_uri', '' ));
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getIsPublished())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ));
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

        // Validation example
        // if( true == is_null( $this->getOrderNum())) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ));
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>