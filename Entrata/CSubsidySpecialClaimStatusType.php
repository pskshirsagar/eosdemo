<?php

class CSubsidySpecialClaimStatusType extends CBaseSubsidySpecialClaimStatusType {

	const OPEN				= 1;
	const CANCELLED			= 2;
	const SUBMITTED			= 3;
	const APPROVED			= 4;
	const DENIED			= 5;
	const CLAIMED_ON_HAP	= 6;

	public static $c_arrintAllowedSubsidySpecialClaimStatusTypesForFileInsertion = [ self::OPEN, self::SUBMITTED ];
	public static $c_arrintAllowedSubsidySpecialClaimStatusTypesForPdfGeneration = [ self::APPROVED, self::DENIED ];

	public static function getSubsidySpecialClaimStatusTypes() : array {
		return [
			self::OPEN           => __( 'Open' ),
			self::SUBMITTED      => __( 'Submitted' ),
			self::APPROVED       => __( 'Approved' ),
			self::DENIED         => __( 'Denied' ),
			self::CLAIMED_ON_HAP => __( 'Claimed on HAP' )
		];
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
