<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyUtilities
 * Do not add any new functions to this class.
 */

class CPropertyUtilities extends CBasePropertyUtilities {

	public static function fetchPropertyUtilitiesByCidByPropertyId( $intCid, $intPropertyId, $objDatabase, $strLevel = 'property', $boolResidentResponsible = false, $boolSkipCustomUtilities = false ) {

		$strSelectStatment = 'u.name as utility_name,
							a.name as amenity_name,
							ap.company_name|| \', \' ||apl.location_name as vendor_name,
							uf.name as utility_formula_name,
							';

		if( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ) {
			$strSelectStatment = 'util_get_translated( \'name\', u.name, u.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) as utility_name,
									ap.company_name|| \', \' || util_get_translated( \'location_name\', apl.location_name, apl.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) as vendor_name,
									util_get_translated( \'name\', a.name, a.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) as amenity_name,
									util_get_system_translated( \'name\', uf.name, uf.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) as utility_formula_name,';
		}

		$strSql = 'SELECT
						pu.*,
						u.utility_type_id,
						' . $strSelectStatment . '
						u.is_published as utility_is_published
					FROM
						property_utilities pu
						JOIN utilities u ON ( u.id = pu.utility_id AND u.cid = pu.cid )
						JOIN utility_formulas uf ON ( uf.id = pu.utility_formula_id )
						LEFT JOIN ap_payee_locations apl ON ( apl.id = pu.ap_payee_location_id AND apl.cid = pu.cid )
						LEFT JOIN ap_payees ap ON ( ap.id = apl.ap_payee_id AND ap.cid = apl.cid )
						LEFT JOIN amenities a ON ( a.id = pu.amenity_id AND a.cid = pu.cid )
					WHERE
					 	pu.property_id = ' . ( int ) $intPropertyId . '
					 	AND pu.cid = ' . ( int ) $intCid . '
						AND u.is_published = true';

		$strSql .= ( 'property' == $strLevel ) ? ' AND pu.ar_cascade_id = ' . CArCascade::PROPERTY  : ( ( 'unit_type' == $strLevel ) ? ' AND pu.ar_cascade_id = ' . CArCascade::UNIT_TYPE : ( ( 'unit_space' == $strLevel ) ? ' AND pu.ar_cascade_id = ' . CArCascade::SPACE : ( ( 'floor_plan' == $strLevel ) ? ' AND pu.ar_cascade_id = ' . CArCascade::FLOOR_PLAN : '' ) ) );

		$strSql .= ( true == $boolResidentResponsible ) ? ' AND pu.is_responsible = true' : '';

		$strSql .= ( true == $boolSkipCustomUtilities ) ? ' AND u.utility_type_id <> ' . CUtilityType::LEASING : '';

		return self::fetchPropertyUtilities( $strSql, $objDatabase );
	}

	public static function fetchPropertyUtilitiesByIdsByCid( $arrintPropertyUtilitiesIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyUtilitiesIds ) ) return NULL;

		$strSql = 'SELECT * FROM property_utilities WHERE id IN ( ' . implode( ',', $arrintPropertyUtilitiesIds ) . ' ) AND cid = ' . ( int ) $intCid;

		return self::fetchPropertyUtilities( $strSql, $objDatabase );
	}

	public static function fetchPropertyUtilitiesByIntegrationDatabaseIdByCid( $intCid, $intIntegrationDatabaseId, $objDatabase ) {

		$strSql = 'SELECT
						pu.*
					FROM
						property_utilities pu
						JOIN property_integration_databases pid ON ( pid.property_id = pu.property_id AND pid.cid = pu.cid )
					WHERE
						pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND pid.cid = ' . ( int ) $intCid;

		return self::fetchPropertyUtilities( $strSql, $objDatabase );
	}

	public static function fetchAllPublishedPropertyUtilitiesByArCascadeReferenceIdsByCid( $arrintArCascadeReferenceIds, $intCid, $objDatabase, $boolIsResponsible = true ) {

		$strIsResponsibleSql = ( true == $boolIsResponsible ) ? 'AND pu.is_responsible = true'	: '';

		if( false == valArr( $arrintArCascadeReferenceIds ) ) return NULL;

		$strSql = 'SELECT
						pu.*,
						u.name as utility_name
					FROM
						property_utilities pu
						JOIN utilities u ON ( u.id = pu.utility_id AND u.cid = pu.cid )
					WHERE
						pu.ar_cascade_reference_id IN ( ' . implode( ', ', $arrintArCascadeReferenceIds ) . ' )
						AND pu.cid = ' . ( int ) $intCid . '
						AND u.is_published = true
						' . $strIsResponsibleSql . '
					ORDER BY
						ar_cascade_id';

		return self::fetchPropertyUtilities( $strSql, $objDatabase );
	}

	public static function fetchPropertyUtilitiesByUtilityTypeIdsByArCascadeIdByPropertyIdByCid( $arrintUtilityTypeIds, $intArCascadeId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintUtilityTypeIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valId( $intArCascadeId ) ) return NULL;

		$strSql = 'SELECT
						pu.*,
						u.name as utility_name
					FROM
						property_utilities pu
						JOIN utilities u ON ( u.id = pu.utility_id AND u.cid = pu.cid )
					WHERE
						pu.property_id = ' . ( int ) $intPropertyId . '
						AND pu.cid = ' . ( int ) $intCid . '
						AND u.utility_type_id IN (' . implode( ',', $arrintUtilityTypeIds ) . ')
						AND pu.ar_cascade_id = ' . ( int ) $intArCascadeId;

		return self::fetchPropertyUtilities( $strSql, $objDatabase );
	}

	public static function fetchPropertyUtilitiesByUtilityFormulaIdsByPropertyIdByCid( $arrintUtilityFormulaIds, $intPropertyId, $intCid, $objDatabase ) {
		if( true == valArr( $arrintUtilityFormulaIds ) ) return NULL;

		$strSql = 'SELECT
					    pu.*
					FROM
					    property_utilities pu
    					LEFT JOIN utility_formulas uf ON ( pu.utility_formula_id = uf.id )
					WHERE
					    pu.cid = ' . ( int ) $intCid . '
					    AND pu.property_id = ' . ( int ) $intPropertyId . '
					    AND pu.utility_formula_id IN (' . implode( ',', $arrintUtilityFormulaIds ) . ')';

		return self::fetchPropertyUtilities( $strSql, $objDatabase );
	}

}
?>