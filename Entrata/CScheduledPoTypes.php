<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPoTypes
 * Do not add any new functions to this class.
 */

class CScheduledPoTypes extends CBaseScheduledPoTypes {

    public static function fetchScheduledPoTypes( $strSql, $objClientDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CScheduledPoType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchScheduledPoType( $strSql, $objClientDatabase ) {
        return self::fetchCachedObject( $strSql, 'CScheduledPoType', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }
}
?>