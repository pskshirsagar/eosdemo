<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTrainingDatabases
 * Do not add any new functions to this class.
 */

class CTrainingDatabases extends CBaseTrainingDatabases {

	public static function fetchTrainingDatabasesDetailsByCid( $intCid, $objDatabase ) {
		$strSql = ' SELECT
						td.*,
						cu.username,
						ce.name_first,
                        ce.name_last,
                        ce.name_last || \'\' || ce.name_first as name_full
					FROM 
						training_databases td
						JOIN company_users cu ON ( td.cid = cu.cid AND td.created_by = cu.id )
						LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
					WHERE
						td.cid = ' . ( int ) $intCid . '
						AND td.end_date >= CURRENT_DATE
					ORDER BY
						id DESC';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) ) {
			return rekeyArray( 'id', $arrmixData );
		} else {
			return [];
		}
	}

	public static function fetchTrainingDatabasesByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $boolIsCountOnly = false, $boolGetAllStatus = false ) {

		$strSelect = ' td.id,
 						td.name,
 						tdu.id as training_database_user_id,
 						tdu.company_user_id';

		if( true == $boolIsCountOnly ) {
			$strSelect = ' count( td.id ) ';
		}

		$strSql = ' SELECT
 						' . $strSelect . '
					FROM
						training_databases td
						JOIN training_database_users tdu ON ( td.cid = tdu.cid AND td.id = tdu.training_database_id )
					WHERE
						td.cid = ' . ( int ) $intCid . '
						AND tdu.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND td.end_date >= CURRENT_DATE
						AND tdu.deleted_on IS NULL
						AND tdu.deleted_by IS NULL';

		$strSql .= ( true == $boolGetAllStatus ) ? ';' : ' AND td.start_date <= CURRENT_DATE;';

		if( true == $boolIsCountOnly ) {
			$arrintCount = fetchData( $strSql, $objDatabase );
			return $arrintCount[0]['count'];
		}

		return rekeyArray( 'id', fetchData( $strSql, $objDatabase ) );
	}

}
?>
