<?php
class CQuote extends CBaseQuote {

	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intUnitSpaceStatusTypeId;
	protected $m_intNumberOfBedrooms;
	protected $m_intNumberOfBathrooms;
	protected $m_intMaxSquareFeet;
	protected $m_intMinSquareFeet;
	protected $m_intPropertyUnitId;
	protected $m_intRenewalTierRank;
	protected $m_intQuantityRemaining;
	protected $m_intApplicationStatusId;
	protected $m_intPropertyFloorPlanId;
	protected $m_intPropertyBuildingId;
	protected $m_intSelectedQuoteId;
	protected $m_intMaxPets;

	protected $m_strUnitNumber;
	protected $m_strBuildingName;
	protected $m_strFloorplanName;
	protected $m_strFloorPlanId;
	protected $m_strRenewalTierName;
	protected $m_strFloorplanDescription;


	protected $m_objUnitSpace;
	protected $m_objPropertyUnit;
	protected $m_objPropertyFloorplan;
	protected $m_objLeasingTierSpecial;

	protected $m_boolShowCap;
	protected $m_boolShowIncentive;
	protected $m_boolShowTierDiscount;
	protected $m_boolIsBaseRentSelected;

	protected $m_fltMonthToMonthRent;

	protected $m_arrobjUnitSpaceCachedRates;
	protected $m_arrobjPetLeaseAssociations;
	protected $m_arrobjAddOnLeaseAssociations;
	protected $m_arrobjQuoteLeaseTerms;
	protected $m_arrobjRentScheduledCharges;

	protected $m_arrmixSpecials;
	protected $m_arrmixInstallmentPlans;
	protected $m_arrmixRentScheduledCharges;
	protected $m_arrmixMoveInScheduledCharges;
	protected $m_arrmixOneTimeScheduledCharges;
	protected $m_arrmixSpecialScheduledCharges;
	protected $m_arrmixTotalMonthlyScheduledCharges;
	protected $m_arrmixSumarizedRentScheduledCharges;
	protected $m_arrmixOtherRecurringScheduledCharges;
	protected $m_arrmixApplicationFeeScheduledCharges;
	protected $m_arrmixLeaseAssociations;

	const LEASE_LENGTH_SHORTEST			= 'LEASE_LENGTH_SHORTEST';
	const LEASE_LENGTH_LONGEST			= 'LEASE_LENGTH_LONGEST';
	const RENEWAL_RENT_SHORTEST			= 'RENEWAL_RENT_SHORTEST';
	const RENEWAL_RENT_LONGEST			= 'RENEWAL_RENT_LONGEST';

    public function __construct() {
        parent::__construct();
        $this->m_boolShowCap 			= false;
        $this->m_boolShowIncentive 		= false;
        $this->m_boolShowTierDiscount 	= false;
        return;
    }

	/**
	* Set Functions
	*/

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setUnitSpaceStatusTypeId( $intUnitSpaceStatusTypeId ) {
		$this->m_intUnitSpaceStatusTypeId = $intUnitSpaceStatusTypeId;
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->m_intNumberOfBedrooms = $intNumberOfBedrooms;
	}

	public function setNumberOfBathrooms( $intNumberOfBathrooms ) {
		$this->m_intNumberOfBathrooms = $intNumberOfBathrooms;
	}

	public function setMaxSquareFeet( $intMaxSquareFeet ) {
		$this->m_intMaxSquareFeet = $intMaxSquareFeet;
	}

	public function setMinSquareFeet( $intMinSquareFeet ) {
		$this->m_intMinSquareFeet = $intMinSquareFeet;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setMaxPets( $intMaxPets ) {
		$this->m_intMaxPets = $intMaxPets;
	}

	public function setQuantityRemaining( $intQuantityRemaining ) {
		$this->m_intQuantityRemaining = $intQuantityRemaining;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function setFloorplanName( $strFloorplanName ) {
		$this->m_strFloorplanName = $strFloorplanName;
	}

	public function setFloorplanId( $strFloorplanId ) {
		$this->m_strFloorplanId = $strFloorplanId;
	}

	public function setFloorplanDescription( $strFloorplanDescription ) {
		$this->m_strFloorplanDescription = $strFloorplanDescription;
	}

	public function setRenewalTierName( $strRenewalTierName ) {
		$this->m_strRenewalTierName = $strRenewalTierName;
	}

	public function setUnitSpace( $objUnitSpace ) {
		$this->m_objUnitSpace = $objUnitSpace;
	}

	public function setPropertyUnit( $objPropertyUnit ) {
		$this->m_objPropertyUnit = $objPropertyUnit;
	}

	public function setPropertyFloorPlan( $objPropertyFloorplan ) {
		$this->m_objPropertyFloorplan = $objPropertyFloorplan;
	}

	public function setUnitSpaceCachedRates( $arrobjUnitSpaceCachedRates ) {
		$this->m_arrobjUnitSpaceCachedRates = $arrobjUnitSpaceCachedRates;
	}

	public function setQuoteLeaseTerms( $arrobjQuoteLeaseTerms ) {
		$this->m_arrobjQuoteLeaseTerms = $arrobjQuoteLeaseTerms;
	}

	public function setLeasingTierSpecial( $objLeasingTierSpecial ) {
		$this->m_objLeasingTierSpecial = $objLeasingTierSpecial;
	}

    public function setShowCap( $boolShowCap ) {
		$this->m_boolShowCap = $boolShowCap;
    }

	public function setShowIncentive( $boolShowIncentive ) {
		$this->m_boolShowIncentive = $boolShowIncentive;
    }

    public function setShowTierDiscount( $boolShowTierDiscount ) {
    	$this->m_boolShowTierDiscount = $boolShowTierDiscount;
    }

    public function setRenewalTierRank( $intRenewalTierRank ) {
    	$this->m_intRenewalTierRank = $intRenewalTierRank;
    }

    public function setPropertyFloorPlanId( $intPropertyFloorPlanId ) {
    	$this->m_intPropertyFloorPlanId = $intPropertyFloorPlanId;
    }

    public function setPropertyBuildingId( $intPropertyBuildingId ) {
    	$this->m_intPropertyBuildingId = $intPropertyBuildingId;
    }

    public function setSelectedQuoteId( $intSelectedQuoteId ) {
    	$this->m_intSelectedQuoteId = $intSelectedQuoteId;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['renewal_tier_rank'] ) )
			$this->setRenewalTierRank( $arrmixValues['renewal_tier_rank'] );
		if( true == isset( $arrmixValues['property_id'] ) )
    		$this->setPropertyId( $arrmixValues['property_id'] );
    	if( true == isset( $arrmixValues['unit_space_status_type_id'] ) )
    		$this->setUnitSpaceStatusTypeId( $arrmixValues['unit_space_status_type_id'] );
    	if( true == isset( $arrmixValues['number_of_bedrooms'] ) )
    		$this->setNumberOfBedrooms( $arrmixValues['number_of_bedrooms'] );
    	if( true == isset( $arrmixValues['number_of_bathrooms'] ) )
    		$this->setNumberOfBathrooms( $arrmixValues['number_of_bathrooms'] );
    	if( true == isset( $arrmixValues['max_square_feet'] ) )
			$this->setMaxSquareFeet( $arrmixValues['max_square_feet'] );
		if( true == isset( $arrmixValues['min_square_feet'] ) )
			$this->setMinSquareFeet( $arrmixValues['min_square_feet'] );
    	if( true == isset( $arrmixValues['unit_number'] ) )
    		$this->setUnitNumber( $arrmixValues['unit_number'] );
    	if( true == isset( $arrmixValues['building_name'] ) )
    		$this->setBuildingName( $arrmixValues['building_name'] );
    	if( true == isset( $arrmixValues['floorplan_name'] ) )
    		$this->setFloorplanName( $arrmixValues['floorplan_name'] );
	    if( true == isset( $arrmixValues['floorplan_id'] ) )
		    $this->setFloorplanId( $arrmixValues['floorplan_id'] );
    	if( true == isset( $arrmixValues['max_pets'] ) )
    		$this->setMaxPets( $arrmixValues['max_pets'] );
	    if( true == isset( $arrmixValues['tier_name'] ) )
		    $this->setRenewalTierName( $arrmixValues['tier_name'] );
	    if( true == isset( $arrmixValues['lease_id'] ) )
		    $this->setLeaseId( $arrmixValues['lease_id'] );
	    if( true == isset( $arrmixValues['lease_expiring_count'] ) )
		    $this->setQuantityRemaining( $arrmixValues['lease_expiring_count'] );
	    if( true == isset( $arrmixValues['application_status_id'] ) )
		    $this->setApplicationStatusId( $arrmixValues['application_status_id'] );
	    if( true == isset( $arrmixValues['property_floorplan_id'] ) )
		    $this->setPropertyFloorPlanId( $arrmixValues['property_floorplan_id'] );
	    if( true == isset( $arrmixValues['property_building_id'] ) )
		    $this->setPropertyBuildingId( $arrmixValues['property_building_id'] );
	    if( true == isset( $arrmixValues['selected_quote_id'] ) )
		    $this->setSelectedQuoteId( $arrmixValues['selected_quote_id'] );

    }

	public function setRentScheduledCharges( $arrmixRentScheduledCharges ) {
		$this->m_arrmixRentScheduledCharges = $arrmixRentScheduledCharges;
	}

	public function setMoveInScheduledCharges( $arrmixMoveInScheduledCharges ) {
		$this->m_arrmixMoveInScheduledCharges = $arrmixMoveInScheduledCharges;
	}

	public function setSpecialScheduledCharges( $arrmixSpecialScheduledCharges ) {
		$this->m_arrmixSpecialScheduledCharges = $arrmixSpecialScheduledCharges;
	}

	public function setTotalMonthlyScheduledCharges( $arrmixTotalMonthlyScheduledCharges ) {
		$this->m_arrmixTotalMonthlyScheduledCharges = $arrmixTotalMonthlyScheduledCharges;
	}

	public function setSumarizedRentScheduledCharges( $arrmixSumarizedRentScheduledCharges ) {
		$this->m_arrmixSumarizedRentScheduledCharges = $arrmixSumarizedRentScheduledCharges;
	}

	public function setOtherRecurringScheduledCharges( $arrmixOtherRecurringScheduledCharges ) {
		$this->m_arrmixOtherRecurringScheduledCharges = $arrmixOtherRecurringScheduledCharges;
	}

	public function setApplicationFeeScheduledCharges( $arrmixApplicationFeeScheduledCharges ) {
		$this->m_arrmixApplicationFeeScheduledCharges = $arrmixApplicationFeeScheduledCharges;
	}

	public function setOneTimeScheduledCharges( $arrmixOneTimeScheduledCharges ) {
		$this->m_arrmixOneTimeScheduledCharges = $arrmixOneTimeScheduledCharges;
	}

	public function setInstallmentPlans( $arrmixInstallmentPlans ) {
		$this->m_arrmixInstallmentPlans = $arrmixInstallmentPlans;
	}

	public function setSpecials( $arrmixSpecial ) {
		$this->m_arrmixSpecials = $arrmixSpecial;
	}

	public function setMonthToMonthRent( $fltMonthToMonthRent ) {
		$this->m_fltMonthToMonthRent = $fltMonthToMonthRent;
	}

	public function setLeaseAssociations( $arrmixLeaseAssociations ) {
		$this->m_arrmixLeaseAssociations = $arrmixLeaseAssociations;
	}

	public function setApplicationStatusId( $intApplicationStatusId ) {
		$this->m_intApplicationStatusId = $intApplicationStatusId;
	}

	public function setQuoteNote( $strNote ) {
		$this->setDetailsField( 'note', $strNote );
	}

	/**
	* Get Functions
	*/
	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getUnitSpaceStatusTypeId() {
		return $this->m_intUnitSpaceStatusTypeId;
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function getNumberOfBathrooms() {
		return $this->m_intNumberOfBathrooms;
	}

	public function getMaxSquareFeet() {
		return $this->m_intMaxSquareFeet;
	}

	public function getMinSquareFeet() {
		return $this->m_intMinSquareFeet;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getMaxPets() {
		return $this->m_intMaxPets;
	}

	public function getQuantityRemaining() {
		return $this->m_intQuantityRemaining;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function getFloorplanName() {
		return $this->m_strFloorplanName;
	}

	public function getFloorplanId() {
		return $this->m_strFloorplanId;
	}

	public function getRenewalTierName() {
		return $this->m_strRenewalTierName;
	}

	public function getFloorplanDescription() {
		return $this->m_strFloorplanDescription;
	}

	public function getUnitSpace() {
		return $this->m_objUnitSpace;
	}

	public function getPropertyUnit() {
		return $this->m_objPropertyUnit;
	}

	public function getPropertyFloorPlan() {
		return $this->m_objPropertyFloorplan;
	}

	public function getUnitSpaceCachedRates() {
		return $this->m_arrobjUnitSpaceCachedRates;
	}

	public function getQuoteLeaseTerms() {
		return $this->m_arrobjQuoteLeaseTerms;
	}

	public function getLeasingTierSpecial() {
		return $this->m_objLeasingTierSpecial;
	}

    public function getShowCap() {
    	return $this->m_boolShowCap;
    }

    public function getShowIncentive() {
    	return $this->m_boolShowIncentive;
    }

    public function getShowTierDiscount() {
    	return $this->m_boolShowTierDiscount;
    }

    public function getRenewalTierRank() {
    	return $this->m_intRenewalTierRank;
    }

	public function getRentScheduledCharges() {
		return $this->m_arrmixRentScheduledCharges;
	}

	public function getMoveInScheduledCharges() {
		return $this->m_arrmixMoveInScheduledCharges;
	}

	public function getSpecialScheduledCharges() {
		return $this->m_arrmixSpecialScheduledCharges;
	}

	public function getTotalMonthlyScheduledCharges() {
		return $this->m_arrmixTotalMonthlyScheduledCharges;
	}

	public function getSumarizedRentScheduledCharges() {
		return $this->m_arrmixSumarizedRentScheduledCharges;
	}

	public function getOtherRecurringScheduledCharges() {
		return $this->m_arrmixOtherRecurringScheduledCharges;
	}

	public function getApplicationFeeScheduledCharges() {
		return $this->m_arrmixApplicationFeeScheduledCharges;
	}

	public function getOneTimeScheduledCharges() {
		return $this->m_arrmixOneTimeScheduledCharges;
	}

	public function getInstallmentPlans() {
		return $this->m_arrmixInstallmentPlans;
	}

	public function getSpecials() {
		return $this->m_arrmixSpecials;
	}

	public function getMonthToMonthRent() {
		return $this->m_fltMonthToMonthRent;
	}

	public function getLeaseAssociations() {
		return $this->m_arrmixLeaseAssociations;
	}

	public function getApplicationStatusId() {
		return $this->m_intApplicationStatusId;
	}

	public function getPropertyFloorPlanId() {
		return $this->m_intPropertyFloorPlanId;
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function getSelectedQuoteId() {
		return $this->m_intSelectedQuoteId;
	}

	public function getQuoteNote( $strKey ) {
		return $this->getDetailsField( $strKey );
	}

	/**
	* Add Functions
	*/

	public function addUnitSpaceCachedRate( $objUnitSpaceCachedRate ) {
		$this->m_arrobjUnitSpaceCachedRates[$objUnitSpaceCachedRate->getId()] = $objUnitSpaceCachedRate;
	}

	public function addPetLeaseAssociation( $objPetLeaseAssociation ) {
		$this->m_arrobjPetLeaseAssociations[$objPetLeaseAssociation->getId()] = $objPetLeaseAssociation;
	}

	public function addAddOnLeaseAssociation( $objAddOnLeaseAssociation ) {
		$this->m_arrobjAddOnLeaseAssociations[$objAddOnLeaseAssociation->getId()] = $objAddOnLeaseAssociation;
	}

	public function addQuoteLeaseTerm( $objQuoteLeaseTerm ) {
		$this->m_arrobjQuoteLeaseTerms[$objQuoteLeaseTerm->getId()] = $objQuoteLeaseTerm;
	}

	/**
	* Create Functions
	*/

	public function createQuoteLeaseTerm() {
		$objQuoteLeaseTerm = new CQuoteLeaseTerm();
		$objQuoteLeaseTerm->setCid( $this->getCid() );
		$objQuoteLeaseTerm->setQuoteId( $this->getId() );

		return $objQuoteLeaseTerm;
	}

	public function createQuoteSpecial() {
		$objQuoteSpecial = new CQuoteSpecial();
		$objQuoteSpecial->setCid( $this->getCid() );
		$objQuoteSpecial->setQuoteId( $this->getId() );

		return $objQuoteSpecial;
	}

	public function createPetLeaseAssociation() {
		$objPetLeaseAssociation = new CLeaseAssociation();
		$objPetLeaseAssociation->setCid( $this->getCid() );
		$objPetLeaseAssociation->setArOriginId( CArOrigin::PET );
		$objPetLeaseAssociation->setArCascadeId( CArCascade::PROPERTY );
		$objPetLeaseAssociation->setQuoteId( $this->getId() );
		$objPetLeaseAssociation->setIsSelectedQuote( false );

		return $objPetLeaseAssociation;
	}

	public function createSpecialLeaseAssociation() {
		$objSpecialLeaseAssociation = new CSpecialLeaseAssociation();
		$objSpecialLeaseAssociation->setCid( $this->getCid() );
		$objSpecialLeaseAssociation->setPropertyId( $this->getPropertyId() );
		$objSpecialLeaseAssociation->setQuoteId( $this->getId() );
		$objSpecialLeaseAssociation->setArOriginId( CArOrigin::SPECIAL );
		$objSpecialLeaseAssociation->setArCascadeId( CArCascade::SPACE );
		$objSpecialLeaseAssociation->setIsSelectedQuote( false );
		return $objSpecialLeaseAssociation;
	}

	/**
	* Validate functions
	*/

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valApplicationId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valApplicationId() {

		$boolIsValid = true;

		if( true == is_null( $this->getApplicationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Application id is not set for quote' ) ) );
		}

		return $boolIsValid;

	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == valId( $this->getId() ) || false == valStr( $this->getCreatedBy() ) || false == valStr( $this->getCreatedOn() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	/**
	* Fetch functions
	*/

	public function fetchPetsLeaseAssociations( $objDatabase ) {
	// MEGARATES CHANGES fetchQuotePets( $objDatabase )
		return \Psi\Eos\Entrata\Custom\CPetLeaseAssociations::createService()->fetchPetLeaseAssociationsByQuoteIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchCountPetsLeaseAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CPetLeaseAssociations::createService()->fetchCountPetLeaseAssociationsByQuoteIdByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
	}

	public function fetchAddOnLeaseAssociationsByAddOnTypeIds( $arrintAddOnTypeIds, $objDatabase, $boolIsCheckDeleted = false, $boolExcludeAddOnsAttachedToUnit = false ) {
		return \Psi\Eos\Entrata\Custom\CAddOnLeaseAssociations::createService()->fetchAddOnLeaseAssociationsByPropertyIdByQuoteIdByAddOnTypeIdsByCid( $this->getPropertyId(), $this->getId(), $arrintAddOnTypeIds, $this->getCid(), $objDatabase, $boolIsCheckDeleted, $boolExcludeAddOnsAttachedToUnit );
	}

	public function createOptionActivityLog( $objCompanyUser, $intEventSubTypeId, $objDatabase, $intPropertyId, $intLeaseId, $strQuoteName, $arrmixFieldChanges = [] ) {
		if( CEventSubType::QUOTE_UPDATED == $intEventSubTypeId && false === valArr( $arrmixFieldChanges ) ) {
			return true;
		}

		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $this ], CEventType::QUOTE, $intEventSubTypeId );
		$objEvent->setCid( $this->getCid() );
		$objEvent->setPropertyId( $intPropertyId );
		$objEvent->setLeaseId( $intLeaseId );
		$objEvent->setEventDatetime( CEvent::getCurrentUtcTime() );
		$objEvent->setCompanyUser( $objCompanyUser );
		$objEvent->setUpdatedBy( $objCompanyUser->getId() );
		$objEvent->setUpdatedOn( 'NOW()' );
		$objEvent->setCreatedBy( $objCompanyUser->getId() );
		$objEvent->setCreatedOn( 'NOW()' );
		$objEvent->setQuoteName( $strQuoteName );
		$objEvent->setDataReferenceId( $this->getId() );

		$objEventLibraryDataObject->setData( $arrmixFieldChanges );

		$objEventLibrary->buildEventDescription( $this );

		if( false === $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
			$this->addErrorMsgs( $objEventLibrary->getErrorMsgs() );

			return false;
		}

		return true;
	}

}
?>