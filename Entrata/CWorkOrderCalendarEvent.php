<?php

class CWorkOrderCalendarEvent extends CCalendarEvent {

	protected $m_strEndDatetime;
	protected $m_boolIsAllDay;

	public function getEventTypeLabel() {
		// Logic for determining if the event should be grouped in "Requested AM" or "Requested PM"
		if( $this->m_boolIsAllDay ) {

			$intHours		= date( 'G', strtotime( $this->m_strEndDatetime ) );
			$strEventLabel	= 'AM';
			if( $intHours >= 12 ) {
				$strEventLabel	= 'PM';
			}

			return 'Requested ' . $strEventLabel;
		} else {

			return $this->m_strEventType;
		}
	}

}
?>