<?php

class CDocumentLibrary extends CBaseDocumentLibrary {

	protected $m_arrobjDocumentPlugins;

    public function __construct() {
        parent::__construct();

        $this->m_arrobjDocumentPlugins = [];

        return;
    }

    public function getDocumentPlugins() {
    	return $this->m_arrobjDocumentPlugins;
    }

    public function addDocumentPlugin( $objDocumentPlugin ) {
    	$this->m_arrobjDocumentPlugins[$objDocumentPlugin->getId()] = $objDocumentPlugin;
    }

}
?>