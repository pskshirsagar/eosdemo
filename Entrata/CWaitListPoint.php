<?php

class CWaitListPoint extends CBaseWaitListPoint {

	const DEFAULT_MERGE_FIELD_ID_PERCENT_OF_MEDIAN_INCOME = 2832;

	protected $m_strApplicationSettingKey;
	protected $m_intApplicationSettingGroupId;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['application_setting_key'] ) && $boolDirectSet ) {
			$this->m_strApplicationSettingKey = trim( $arrmixValues['application_setting_key'] );
		} elseif( isset( $arrmixValues['application_setting_key'] ) ) {
			$this->setApplicationSettingKey( $arrmixValues['application_setting_key'] );
		}

		if( isset( $arrmixValues['application_setting_group_id'] ) && $boolDirectSet ) {
			$this->m_intApplicationSettingGroupId = trim( $arrmixValues['application_setting_group_id'] );
		} elseif( isset( $arrmixValues['application_setting_group_id'] ) ) {
			$this->setApplicationSettingGroupId( $arrmixValues['application_setting_group_id'] );
		}
	}

	// Set functions

	public function setApplicationSettingGroupId( $intApplicationSettingGroupId ) {
		$this->m_intApplicationSettingGroupId = $intApplicationSettingGroupId;
	}

	public function setApplicationSettingKey( $strApplicationSettingKey ) {
		$this->m_strApplicationSettingKey = $strApplicationSettingKey;
	}

	// Get functions

	public function getApplicationSettingGroupId() {
		return $this->m_intApplicationSettingGroupId;
	}

	public function getApplicationSettingKey() {
		return $this->m_strApplicationSettingKey;
	}

	public function getIncomeValueFromDetails() {
		return $this->getIncome();
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaitListId() {
		$boolIsValid = true;

		if( false == valId( $this->getWaitListId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Wait_list_id', __( 'Wait List Id is not valid' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valApplicationSettingKeyId( $arrmixRequestData ) {
		$boolIsValid = true;
		$boolIsValid &= valId( $this->getApplicationSettingKeyId() );
		if( false == $boolIsValid ) {
			$arrstrMessages = [ 'PHA' => 'Previous Housing Arrangement', 'DS'  => 'Displacement Status', '' => 'Certification Question' ];
			$strMessge = isset( $arrstrMessages[$arrmixRequestData['question']] ) ? $arrstrMessages[$arrmixRequestData['question']] : 'Certification Question';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_setting_key_id', __( 'Please select {%s,0}', [ $strMessge ] ) ) );
		}
		return $boolIsValid;
	}

	public function valMergeFieldId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $intOccupancyTypeId ) {
		$boolIsValid = true;
		$boolIsValid &= valStr( $this->getName() );

		$strStrategyName = __( 'Ranking' );
		if( COccupancyType::MILITARY == $intOccupancyTypeId ) {
			$strStrategyName = __( 'Priority' );
		}
		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Name', __( '{%s,0} Name is required', [ $strStrategyName ] ) ) );
		}
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		if( false == valStr( $this->getValue() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Rating is required' ) ) );
			$boolIsValid &= false;
		}

		if( $boolIsValid && ( $this->getValue() < 1 || $this->getValue() > 100 ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', __( 'Rating should be in 1-100' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixRequestData = [], $intOccupancyTypeId ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $intOccupancyTypeId );
				if( COccupancyType::MILITARY != $intOccupancyTypeId ) {
					$boolIsValid &= $this->valValue();
					$boolIsValid &= $this->valWaitListId();
					$boolIsValid &= $this->valApplicationSettingKeyId( $arrmixRequestData );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == valId( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>
