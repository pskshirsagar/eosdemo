<?php

class CPropertyTransferStatusType extends CBasePropertyTransferStatusType {

	const INITIALIZED 		= 1;
	const IN_PROGRESS 		= 2;
	const COMPLETED 		= 3;
	const FAILED 			= 4;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }
}
?>