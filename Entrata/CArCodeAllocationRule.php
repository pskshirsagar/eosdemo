<?php

class CArCodeAllocationRule extends CBaseArCodeAllocationRule {

	protected $m_intPropertyId;

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) {
			$this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) );
		} elseif( isset( $arrValues['property_id'] ) ) {
			$this->setPropertyId( $arrValues['property_id'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowedArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriority() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setPropertyId( $intPropertyCid ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyCid, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

}
?>