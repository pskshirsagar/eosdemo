<?php

class CCompanyHoliday extends CBaseCompanyHoliday {

    /**
     * Create Functions
     */

    public function createPropertyHoliday() {

    	$objPropertyHoliday = new CPropertyHoliday();

    	$objPropertyHoliday->setCid( $this->getCid() );
    	$objPropertyHoliday->setCompanyHolidayId( $this->getId() );
    	$objPropertyHoliday->setOpenTime( NULL );
    	$objPropertyHoliday->setCloseTime( NULL );

    	return $objPropertyHoliday;
    }

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intCid ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intPropertyId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

        if( false == isset( $this->m_strName ) || ( 1 > \Psi\CStringService::singleton()->strlen( $this->m_strName ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'The company Holiday name must have at least {%d, 0} character.', [ 1 ] ) ) );
        }

        return $boolIsValid;
    }

    public function valDate() {
        $boolIsValid = true;

			if( 1 !== CValidation::checkDate( $this->m_strDate ) ) {
				$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', __( 'Date must be in mm/dd/yyyy form.' ) ) );
			}

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

      /**
       * Validation example
       */

        // if( false == isset( $this->m_strDescription ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

      /**
       * Validation example
       */

        // if( false == isset( $this->m_intIsPublished ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valDate();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>