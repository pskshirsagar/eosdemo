<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaderExportBatches
 * Do not add any new functions to this class.
 */

class CApHeaderExportBatches extends CBaseApHeaderExportBatches {

	public static function fetchPaginatedApHeaderExportBatchesByCid( $intCid, $objDatabase, $intPageNo = NULL, $intPageSize = NULL ) {

		$intOffset	= 0;
		$intLimit	= 0;

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;
		}

		$strSql = 'SELECT
 						aheb.*,
 						cu.username as user_name
 					FROM
 						ap_header_export_batches aheb
 						JOIN company_users cu ON ( aheb.cid = cu.cid AND aheb.created_by = cu.id )
 					WHERE
 						aheb.cid = ' . ( int ) $intCid . '
 					AND aheb.deleted_by IS NULL
 					AND aheb.deleted_on IS NULL';

		$strSql .= ' ORDER BY aheb.id DESC';

		if( 0 < $intOffset ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( false == is_null( $intLimit ) && false == is_null( $intPageSize ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchApHeaderExportBatches( $strSql, $objDatabase );
	}

	public static function fetchApHeaderExportBatchByScheduledApHeaderExportBatchIdByCid( $intScheduledApheaderExportBatchId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						aheb.*,
						ctv.client_number
					FROM 
						ap_header_export_batches aheb
                        JOIN scheduled_ap_header_export_batches saheb ON ( saheb.cid = aheb.cid  AND saheb.id = aheb.scheduled_ap_header_export_batch_id )
                        LEFT JOIN company_transmission_vendors ctv ON ( ctv.cid = saheb.cid AND ctv.id = saheb.company_transmission_vendor_id )
					WHERE
						aheb.scheduled_ap_header_export_batch_id = ' . ( int ) $intScheduledApheaderExportBatchId . '
						AND aheb.cid = ' . ( int ) $intCid . '
					ORDER BY aheb.id DESC 
					LIMIT 1';
		return self::fetchApHeaderExportBatch( $strSql, $objDatabase );
	}

	public static function fetchFailedApHeaderExportBatches( $objDatabase ) {
		$strSql = ' SELECT
						aheb.*,
						ctv.client_number
					FROM
						ap_header_export_batches aheb
						JOIN scheduled_ap_header_export_batches saheb ON ( saheb.cid = aheb.cid  AND saheb.id = aheb.scheduled_ap_header_export_batch_id )
                        LEFT JOIN company_transmission_vendors ctv ON ( ctv.cid = saheb.cid AND ctv.id = saheb.company_transmission_vendor_id )
					WHERE
						aheb.is_success IS FALSE
						AND aheb.reattempt_count < ' . CApHeaderExportBatch::MAX_REATTEMPT_LIMIT . '
						AND aheb.deleted_by IS NULL
						AND aheb.scheduled_ap_header_export_batch_id IS NOT NULL
						AND aheb.batch_datetime > ( CURRENT_DATE - INTERVAL \'15 days\' )';
		return self::fetchApHeaderExportBatches( $strSql, $objDatabase );
	}

}
?>