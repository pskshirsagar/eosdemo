<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAssetLocations
 * Do not add any new functions to this class.
 */

class CAssetLocations extends CBaseAssetLocations {

	public static function fetchActiveAssetLocationsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						asset_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchAssetLocations( $strSql, $objClientDatabase );
	}

	public static function fetchAssetLocationByIdByCid( $intAssetLocationId, $intCid, $objClientDatabase ) {

		if( false == valId( $intAssetLocationId ) ) return false;

		$strSql = 'SELECT
						al.*,
						pg.name as property_name
					FROM
						asset_locations al
						LEFT JOIN property_groups pg ON ( al.cid = pg.cid AND al.property_id = pg.id )
					WHERE
						al.cid = ' . ( int ) $intCid . '
						AND al.id =' . ( int ) $intAssetLocationId . '
						AND al.deleted_by IS NULL
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
					ORDER BY
						name';

		return self::fetchAssetLocation( $strSql, $objClientDatabase );
	}

	public static function fetchAssetLocationByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {

		if( false == valId( $intPropertyId ) ) return false;

		$strSql = 'SELECT
						al.*
					FROM
						asset_locations al
					WHERE
						al.cid = ' . ( int ) $intCid . '
						AND al.property_id =' . ( int ) $intPropertyId;

		return self::fetchAssetLocation( $strSql, $objClientDatabase );
	}

	public static function fetchActiveAssetLocationsByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase, $intLoadAllProperties = 1, $arrmixRequestData = [], $intExcludeAssetLocationId = 0 ) {

		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}
		$strJoin		= '';
		$strCondition	= '';
		$arrstrAdvancedSearchParameters = [];

		$arrmixFilteredExplodedSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $arrmixRequestData['q'] );

		if( 0 == $intLoadAllProperties ) {

			$strJoin		= ' JOIN property_preferences pp ON ( p.cid = pp.cid AND pp.property_id = p.id )';
			$strCondition	= ' AND pp.key = \'' . CPropertyPreference::TRACK_INVENTORY_QUANTITIES . '\'
								AND pp.value = \'1\'';
		}

		if( true == valId( $intExcludeAssetLocationId ) ) {
			$strCondition .= ' AND pgal.asset_location_id <>' . ( int ) $intExcludeAssetLocationId;
		}

		$strSql = 'SELECT
						DISTINCT al.*,
						CASE WHEN ppr.value IS NOT NULL THEN 1 ELSE 0 END AS is_default_asset_location
					FROM
						asset_locations al
						JOIN property_group_asset_locations pgal ON ( al.cid = pgal.cid AND al.id = pgal.asset_location_id )
						JOIN property_groups pg ON ( pg.cid = pgal.cid AND pgal.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pgal.cid = pga.cid AND pg.id = pga.property_group_id )
						JOIN properties p ON ( p.cid = pgal.cid AND p.id = pga.property_id AND p.is_disabled = 0 ) '
						. $strJoin . '
						LEFT JOIN property_preferences ppr ON ( p.cid = ppr.cid AND ppr.property_id = p.id AND ppr.key = \'' . CPropertyPreference::KEY_DEFAULT_ASSET_LOCATION_ID . '\' AND ppr.value::INTEGER = al.id )
					WHERE
						al.cid = ' . ( int ) $intCid . '
						AND al.deleted_by IS NULL
						AND al.deleted_on IS NULL
						AND pga.property_id = ' . ( int ) $intPropertyId
						. $strCondition;

		if( true == valArr( $arrmixFilteredExplodedSearch ) ) {
			foreach( $arrmixFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'al.name 	ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAssetLocationsByNamesByCid( $arrstrNames, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrstrNames ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						asset_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND LOWER ( name ) IN ( ' . sqlStrImplode( array_map( 'addslashes', $arrstrNames ) ) . ' )';

		return self::fetchAssetLocations( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultAssetLocationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						al.id,
						pr.property_id
					FROM
						asset_locations al
						JOIN properties p ON ( p.cid = al.cid AND p.is_disabled = 0 )
						JOIN property_preferences pr ON ( p.cid = pr.cid AND pr.property_id = p.id )
					WHERE
						al.cid = ' . ( int ) $intCid . '
						AND al.deleted_by IS NULL
						AND al.deleted_on IS NULL
						AND p.id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ')
						AND pr.key = \'' . CPropertyPreference::KEY_DEFAULT_ASSET_LOCATION_ID . '\' AND pr.value::INTEGER = al.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchNonDeletableAssetLocationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						asset_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ')';

		return self::fetchAssetLocations( $strSql, $objClientDatabase );
	}

	public static function fetchAssetLocationPropertiesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name
					FROM
						properties p
						JOIN property_group_associations pga ON( pga.property_group_id = p.id AND pga.cid = p.cid )
						JOIN property_groups pg ON( pg.id = pga.property_group_id AND pga.cid = pg.cid )
						JOIN property_group_asset_locations pgal ON( pgal.property_group_id = pg.id AND pg.cid = pgal.cid )
						JOIN asset_locations al ON( al.id = pgal.asset_location_id AND al.cid = pgal.cid )
					WHERE
						pg.cid = ' . ( int ) $intCid . ' 
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL
						and al.deleted_by IS NULL
						AND p.is_disabled = 0
					GROUP BY
						p.id,
						p.property_name
					HAVING
						count( p.id ) > 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssetLocationsByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase, $intExcludeAssetLocationId = NULL ) {

		$strCondition = '';

		if( true == valId( $intExcludeAssetLocationId ) ) {
			$strCondition .= ' AND pgal.asset_location_id <>' . ( int ) $intExcludeAssetLocationId;
		}

		$strSql = 'SELECT
						al.id,
						al.name,
						pga.property_id
					FROM
						asset_locations al
						JOIN property_group_asset_locations pgal ON ( al.cid = pgal.cid AND al.id = pgal.asset_location_id )
						JOIN property_groups pg ON ( pg.cid = pgal.cid AND pgal.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( pga.cid = pgal.cid AND pga.property_group_id = pg.id )
					WHERE
						al.cid = ' . ( int ) $intCid . '
						AND al.deleted_by IS NULL
						AND pgal.property_group_id = ' . ( int ) $intPropertyId . $strCondition;

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>