<?php

class CFeeTemplateRule extends CBaseFeeTemplateRule {

	protected $m_boolIsCashBasis;
	protected $m_boolIsManualRule;
	protected $m_boolIsCashReceipts;

	protected $m_intPropertyId;
	protected $m_intGlAccountTypeId;
	protected $m_intIsFixedAmount;
	protected $m_fltFeeTemplateRuleAmount;

	protected $m_strArCodeSign;
	protected $m_strArCodeName;
	protected $m_strGlGroupSign;
	protected $m_strGlGroupName;
	protected $m_strGlAccountSign;
	protected $m_strGlAccountName;
	protected $m_strFixedAmountSign;
	protected $m_strFeeRuleTypeName;

	/**
	 * Get Functions
	 */

	public function getIsCashReceipts() {
		return $this->m_boolIsCashReceipts;
	}

	public function getIsManualRule() {
		return $this->m_boolIsManualRule;
	}

	public function getIsCashBasis() {
		return $this->m_boolIsCashBasis;
	}

	public function getIsFixedAmount() {
		return $this->m_intIsFixedAmount;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function getFeeTemplateRuleAmount() {
		return $this->m_fltFeeTemplateRuleAmount;
	}

	public function getGlGroupSign() {
		return $this->m_strGlGroupSign;
	}

	public function getArCodeSign() {
		return $this->m_strArCodeSign;
	}

	public function getGlAccountSign() {
		return $this->m_strGlAccountSign;
	}

	public function getFixedAmountSign() {
		return $this->m_strFixedAmountSign;
	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getGlGroupName() {
		return $this->m_strGlGroupName;
	}

	public function getArCodeName() {
		return $this->m_strArCodeName;
	}

	public function getFeeRuleTypeName() {
		return $this->m_strFeeRuleTypeName;
	}

	/**
	 * Set Functions
	 */

	public function setIsCashReceipts( $intIsCashReceipts ) {
		$this->m_boolIsCashReceipts = $intIsCashReceipts;
	}

	public function setIsManualRule( $boolIsManualRule ) {
		$this->m_boolIsManualRule = $boolIsManualRule;
	}

	public function setIsCashBasis( $boolIsCashBasis ) {
		$this->m_boolIsCashBasis = $boolIsCashBasis;
	}

	public function setIsFixedAmount( $intIsFixedAmount ) {
		$this->m_intIsFixedAmount = CStrings::strToIntDef( $intIsFixedAmount, NULL, false );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->m_intGlAccountTypeId = CStrings::strToIntDef( $intGlAccountTypeId, NULL, false );
	}

	public function setFeeTemplateRuleAmount( $fltFeeTemplateRuleAmount ) {
		$this->m_fltFeeTemplateRuleAmount = $fltFeeTemplateRuleAmount;
	}

	public function setGlGroupSign( $strGlGroupSign ) {
		$this->m_strGlGroupSign = CStrings::strTrimDef( $strGlGroupSign, 1, NULL, true );
	}

	public function setArCodeSign( $strArCodeSign ) {
		$this->m_strArCodeSign = CStrings::strTrimDef( $strArCodeSign, 1, NULL, true );
	}

	public function setGlAccountSign( $strGlAccountSign ) {
		$this->m_strGlAccountSign = CStrings::strTrimDef( $strGlAccountSign, 1, NULL, true );
	}

	public function setFixedAmountSign( $strFixedAmountSign ) {
		$this->m_strFixedAmountSign = CStrings::strTrimDef( $strFixedAmountSign, 1, NULL, true );
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = $strGlAccountName;
	}

	public function setGlGroupName( $strGlGroupName ) {
		$this->m_strGlGroupName = $strGlGroupName;
	}

	public function setArCodeName( $strArCodeName ) {
		$this->m_strArCodeName = $strArCodeName;
	}

	public function setFeeRuleTypeName( $strFeeRuleTypeName ) {
		$this->m_strFeeRuleTypeName = $strFeeRuleTypeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_cash_receipts'] ) ) $this->setIsCashReceipts( $arrmixValues['is_cash_receipts'] );
		if( true == isset( $arrmixValues['is_fixed_amount'] ) ) $this->setIsFixedAmount( $arrmixValues['is_fixed_amount'] );
		if( true == isset( $arrmixValues['fee_rule_type_name'] ) ) $this->setFeeRuleTypeName( $arrmixValues['fee_rule_type_name'] );
		if( true == isset( $arrmixValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrmixValues['gl_account_type_id'] );
		return;
	}

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFeeTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFeeRuleTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFeeRuleTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fee_rule_type_id', __( 'Adjust using is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGlGroupId( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getGlGroupId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_group_id', __( 'GL account group is required.' ) ) );
		} else {

			$strWhere = 'WHERE id = ' . ( int ) $this->getGlGroupId() . ' AND cid = ' . ( int ) $this->getCid() . ' AND disabled_on IS NOT NULL';

			if( 0 < CGlGroups::fetchGlGroupCount( $strWhere, $objClientDatabase ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account group is disabled.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valGlAccountId( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getGlAccountId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account is required.' ) ) );
		} else {

			$strWhere = 'WHERE id = ' . $this->getGlAccountId() . ' AND cid = ' . $this->getCid() . ' AND disabled_on IS NOT NULL';

			if( 0 < CGlAccounts::fetchGlAccountCount( $strWhere, $objClientDatabase ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account is disabled.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valArCodeId( $objClientDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getArCodeId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Ar code is required.' ) ) );
		} else {

			$strWhere = 'WHERE id = ' . $this->getArCodeId() . ' AND cid = ' . $this->getCid() . ' AND is_disabled = true';

			if( 0 < \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeCount( $strWhere, $objClientDatabase ) ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Ar code is disabled.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFixedAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getFixedAmount() ) || 0 == $this->getFixedAmount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fixed_amount', __( 'Fixed amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPercent() {
		$boolIsValid = true;

		if( true == is_null( $this->getPercent() ) || 0 == $this->getPercent() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent', __( 'Percent is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsAdjustsRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPercentRange() {
		$boolIsValid = true;

		if( ( -100 > $this->getPercent() || 100 < $this->getPercent() ) && false == is_null( $this->getPercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent', __( 'Percentage should be less than or equal to {%d, 0}.', [ 100 ] ) ) );
		}

		return $boolIsValid;
	}

	public function valFeeRuleType( $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $this->getFeeRuleTypeId() ) {
			case CFeeRuleType::GL_ACCOUNT_GROUP:
				$boolIsValid &= $this->valGlGroupId( $objClientDatabase );
				$boolIsValid &= $this->valPercent();
				$boolIsValid &= $this->valPercentRange();
				break;

			case CFeeRuleType::GL_ACCOUNT:
				$boolIsValid &= $this->valGlAccountId( $objClientDatabase );
				$boolIsValid &= $this->valPercent();
				$boolIsValid &= $this->valPercentRange();
				break;

			case CFeeRuleType::FIXED_AMOUNT:
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valFixedAmount();
				break;

			case CFeeRuleType::CHARGE_CODE:
				$boolIsValid &= $this->valArCodeId( $objClientDatabase );
				$boolIsValid &= $this->valPercent();
				$boolIsValid &= $this->valPercentRange();
				break;

			default:
				$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFeeRuleTypeId();
				$boolIsValid &= $this->valFeeRuleType( $objClientDatabase );
				break;

			case VALIDATE_DELETE:
			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>