<?php

class CDataCleanupType extends CBaseDataCleanupType {

	const DATA_CLEAN_UP_TYPE_UNIT_TYPES 		= 1;
	const DATA_CLEAN_UP_TYPE_UNIT 				= 2;
	const DATA_CLEAN_UP_TYPE_TRANSACTION		= 3;
	const DATA_CLEAN_UP_TYPE_LEASE				= 4;
	const DATA_CLEAN_UP_TYPE_GL_HISTORY			= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>