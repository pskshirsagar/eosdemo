<?php

class CSubsidyProgram extends CBaseSubsidyProgram {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyProgramTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyProgramSubTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProgramName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProgramNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPlacedInServiceDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFederal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function loadOrCreateSubsidyProgramIncomeLevel( CDatabase $objDatabase ) : CSubsidyProgramIncomeLevel {
		if( true == valId( $this->getId() ) ) {
			$objSubsidyProgramIncomeLevel = current( \Psi\Eos\Entrata\CSubsidyProjectIncomeLevels::createService()->fetchSubsidyProjectIncomeLevelsBySubsidyProgramIdByCid( $this->getId(), $this->getCid(), $objDatabase ) );
		} else {
			$objSubsidyProgramIncomeLevel = new CSubsidyProgramIncomeLevel();
			$objSubsidyProgramIncomeLevel->setCid( $this->getCid() );
			$objSubsidyProgramIncomeLevel->setPropertyId( $this->getPropertyId() );
			$objSubsidyProgramIncomeLevel->setSubsidyProgramId( $this->getId() );
		}
		return $objSubsidyProgramIncomeLevel;
	}

	public function createSubsidyProjectPropertyBuildings() : CSubsidyProjectPropertyBuilding {
		$objSubsidyProgramBuilding = new CSubsidyProjectPropertyBuilding();
		$objSubsidyProgramBuilding->setCid( $this->getCid() );
		$objSubsidyProgramBuilding->setPropertyId( $this->getPropertyId() );
		$objSubsidyProgramBuilding->setSubsidyProgramId( $this->getId() );

		return $objSubsidyProgramBuilding;
	}

	public function createSubsidyProjectUnitSpace() : CSubsidyProjectUnitSpace {
		$objSubsidyProjectUnitSpace = new CSubsidyProjectUnitSpace();
		$objSubsidyProjectUnitSpace->setCid( $this->getCid() );
		$objSubsidyProjectUnitSpace->setPropertyId( $this->getPropertyId() );
		$objSubsidyProjectUnitSpace->setSubsidyProgramId( $this->getId() );

		return $objSubsidyProjectUnitSpace;
	}

}
?>