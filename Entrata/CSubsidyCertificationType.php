<?php

class CSubsidyCertificationType extends CBaseSubsidyCertificationType {

	const INITIAL_CERTIFICATION		= 1;
	const MOVE_IN					= 2;
	const INTERIM_RECERTIFICATION	= 3;
	const UNIT_TRANSFER				= 4; // Partial
	const GROSS_RENT_CHANGE			= 5; // Partial
	const ANNUAL_RECERTIFICATION	= 6;
	const TERMINATION				= 7; // Partial
	const MOVE_OUT					= 8; // Partial

	public static $c_arrintPartialSubsidyCertificationTypeIds = [
		self::UNIT_TRANSFER,
		self::GROSS_RENT_CHANGE,
		self::TERMINATION,
		self::MOVE_OUT
	];

	public static $c_arrintFullSubsidyCertificationTypeIds = [
		self::INITIAL_CERTIFICATION,
		self::MOVE_IN,
		self::INTERIM_RECERTIFICATION,
		self::ANNUAL_RECERTIFICATION,
	];

	public static $c_arrintNotRapEffectiveDateSubsidyCertificationTypeIds = [
	    self::INITIAL_CERTIFICATION,
	    self::INTERIM_RECERTIFICATION,
	    self::GROSS_RENT_CHANGE,
	    self::ANNUAL_RECERTIFICATION
	];

	public static $c_arrintRapEffectiveDateSubsidyCertificationTypeIds = [
	    self::INITIAL_CERTIFICATION,
	    self::MOVE_IN,
	    self::INTERIM_RECERTIFICATION,
	    self::UNIT_TRANSFER,
	    self::GROSS_RENT_CHANGE,
	    self::ANNUAL_RECERTIFICATION
	];

	public static $c_arrintNotRapEffectiveDateIncrementByOneSubsidyCertificationTypeIds = [
	    self::MOVE_IN,
	    self::UNIT_TRANSFER,
	    self::TERMINATION,
	    self::MOVE_OUT
	];

	public static $c_arrintRapEffectiveDateIncrementByOneSubsidyCertificationTypeIds = [
	    self::INITIAL_CERTIFICATION,
	    self::MOVE_IN,
	    self::INTERIM_RECERTIFICATION,
	    self::UNIT_TRANSFER,
	    self::GROSS_RENT_CHANGE,
	    self::TERMINATION,
	    self::MOVE_OUT
	];

	public static $c_arrintNotRapEffectiveDateIncrementByTwoSubsidyCertificationTypeIds = [
	    self::INITIAL_CERTIFICATION,
	    self::MOVE_IN,
	    self::INTERIM_RECERTIFICATION,
	    self::UNIT_TRANSFER,
	    self::TERMINATION,
	    self::MOVE_OUT
	];

	public static $c_arrintHAPArTransactionSubsidyCertificationTypeIds = [
	    self::TERMINATION,
	    self::MOVE_OUT
	];

	public static $c_arrintRecertificationTypeIds = [
	    self::INTERIM_RECERTIFICATION,
	    self::ANNUAL_RECERTIFICATION
	];

	public static $c_arrstrSubsidyCertificationTypeNames = [
		self::INITIAL_CERTIFICATION   => 'Initial',
		self::MOVE_IN                 => 'Move-In',
	    self::INTERIM_RECERTIFICATION => 'Interim Recertification',
		self::UNIT_TRANSFER           => 'Unit Transfer',
	    self::GROSS_RENT_CHANGE       => 'Gross Rent Change',
	    self::ANNUAL_RECERTIFICATION  => 'Annual Recertification',
	    self::TERMINATION             => 'Termination',
	    self::MOVE_OUT                => 'Move-Out'
	];

	public static $c_arrstrSubsidyCertificationTypeCodes = [
		self::INITIAL_CERTIFICATION   => 'IC',
		self::MOVE_IN                 => 'MI',
		self::INTERIM_RECERTIFICATION => 'IR',
		self::UNIT_TRANSFER           => 'UT',
		self::GROSS_RENT_CHANGE       => 'GR',
		self::ANNUAL_RECERTIFICATION  => 'AR',
		self::TERMINATION             => 'TM',
		self::MOVE_OUT                => 'MO'
	];

	public static $c_arrstrSubsidyCertificationTypeNamesAsPerConstant = [
		self::INITIAL_CERTIFICATION   => 'INITIAL_CERTIFICATION',
		self::MOVE_IN                 => 'MOVE_IN',
		self::INTERIM_RECERTIFICATION => 'INTERIM_RECERTIFICATION',
		self::UNIT_TRANSFER           => 'UNIT_TRANSFER',
		self::GROSS_RENT_CHANGE       => 'GROSS_RENT_CHANGE',
		self::ANNUAL_RECERTIFICATION  => 'ANNUAL_RECERTIFICATION',
		self::TERMINATION             => 'TERMINATION',
		self::MOVE_OUT                => 'MOVE_OUT'
	];

	public static $c_arrintLayeredSitesCertificationTypeIds = [
	    self::INITIAL_CERTIFICATION,
		self::MOVE_IN,
		self::UNIT_TRANSFER,
		self::ANNUAL_RECERTIFICATION,
		self::INTERIM_RECERTIFICATION
	];

	public static $c_arrintTracsVersion203ASubsidyCertificationTypeIds = [
		self::UNIT_TRANSFER,
		self::GROSS_RENT_CHANGE
	];

	public static $c_arrintAllowedReverseSubsidyCertificationTypeIds = [
		self::MOVE_IN,
		self::INITIAL_CERTIFICATION,
		self::ANNUAL_RECERTIFICATION
	];
	public static $c_arrintSameDateMoveOutAllowedSubsidyCertificationTypeIds = [
		self::INTERIM_RECERTIFICATION,
		self::GROSS_RENT_CHANGE,
		self::ANNUAL_RECERTIFICATION
	];
	public static $c_arrintSameDateMoveOutRestrictedSubsidyCertificationTypeIds = [
		self::MOVE_IN,
		self::INITIAL_CERTIFICATION,
		self::UNIT_TRANSFER,
		self::TERMINATION
	];

	public static $c_arrstrSubsidyCertificationTypesRekeyedByName = [
		'initial'					=> self::INITIAL_CERTIFICATION,
		'interim recertification'	=> self::INTERIM_RECERTIFICATION,
		'annual recertification'	=> self::ANNUAL_RECERTIFICATION,
		'recertification'			=> self::ANNUAL_RECERTIFICATION,
		'moveout'					=> self::MOVE_OUT,
		'movein'					=> self::MOVE_IN
	];

	public static $c_arrstrMissouriExhibitTicGenerationSubsidyCertificationIds = [
		self::MOVE_IN,
		self::INITIAL_CERTIFICATION
	];

	public static $c_arrstrAllowedSecurityDepositOnPreviousSubsidyCertificationIds = [
		self::MOVE_IN,
		self::INITIAL_CERTIFICATION,
		self::UNIT_TRANSFER
	];

	public static $c_arrstrAllowedSubsidyCertificationIdsForSecurityDeposit = [
		self::ANNUAL_RECERTIFICATION,
		self::INTERIM_RECERTIFICATION,
		self::GROSS_RENT_CHANGE
	];

	public static function getNameById( $intSubsidyCertificationTypeId, $boolAppendSuffix = false ) {
		$strSubsidyCertificationTypeName = NULL;

		if( true == isset( self::$c_arrstrSubsidyCertificationTypeNames[$intSubsidyCertificationTypeId] ) ) {
			$strSubsidyCertificationTypeName = self::$c_arrstrSubsidyCertificationTypeNames[$intSubsidyCertificationTypeId];

			if( true == $boolAppendSuffix && false == in_array( $intSubsidyCertificationTypeId, self::$c_arrintRecertificationTypeIds ) ) {
				$strSubsidyCertificationTypeName .= ' Certification';
			}
		}

		return $strSubsidyCertificationTypeName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPartial() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
