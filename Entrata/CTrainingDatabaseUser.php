<?php

class CTrainingDatabaseUser extends CBaseTrainingDatabaseUser {

	private $m_objCompanyUser;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTrainingDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubdomain() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getOrFetchCompanyUser() {
		if( true == is_null( $this->m_objCompanyUser ) ) {
			$this->m_objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $this->getCompanyUserId(), $this->getCid(), $this->m_objDatabase );
		}

		return $this->m_objCompanyUser;
	}

	public function generateJwtToken( $intCompanyUserId = NULL ) {

		if( true == empty( $intCompanyUserId ) ) $intCompanyUserId = $this->getTrainingCompanyUserId();

		try {
			// create jwt token
			$objJwtToken = new \Psi\Libraries\UtilJwt\CJsonWebToken();

			// Cid of training client. This will always remain same.
			$intCid = 13;

			// setting payload data encrypted
			$objJwtToken->setPayloadClaim( 'details', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $intCid . '-' . $intCompanyUserId, CConfig::get( 'SODIUM_KEY_ENTRATA_JWT_TOKEN' ) ) );
			return $objJwtToken->generateToken( $objJwtToken::ALGO_HS256, CConfig::get( 'SODIUM_KEY_ENTRATA_JWT_TOKEN' ), 600 );
		} catch( \Psi\Libraries\UtilJwt\CJsonWebTokenException $objJsonWebToken ) {
			return NULL;
		} catch( \Psi\Libraries\Cryptography\Exceptions\CCryptoInvalidException $objCryptoException ) {
			return NULL;
		}
	}

	public function getEntrataUrl() {
		$strLoginUrl = 'https://' . $this->getSubdomain() . '.entrata.training';
		return $strLoginUrl;
	}

	public function getNameFull() {
		return $this->getOrFetchCompanyUser()->getOrFetchCompanyEmployee( $this->m_objDatabase )->getNameFull();
	}

}
?>