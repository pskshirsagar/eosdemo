<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRanks
 * Do not add any new functions to this class.
 */

class CRanks extends CBaseRanks {

	public static function fetchRanks( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRank', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchRank( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRank', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>