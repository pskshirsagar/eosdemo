<?php

class CSubsidyHapRequest extends CBaseSubsidyHapRequest {

	protected $m_strPropertyName;
	protected $m_intSubsidyContractTypeId;
	protected $m_intSubsidyTracsVersionId;
	protected $m_strSubsidyContractTypeName;
	private   $m_strSubsidyContractNumber;

	protected $m_objAssistancePaymentSummaryRecord;
	protected $m_objAssistancePaymentDetailRecord;
	protected $m_objAdjustmentPaymentDetailRecord;
	protected $m_objApprovedSpecialClaim;
	protected $m_objMiscellaneousAccountingRequest;
	protected $m_objRepaymentAgreement;
	protected $m_intAssistancePaymentBillingUnitCount;
	protected $m_intAdjustmentAssistancePaymentBillingUnitCount;

	/**
	 * Setters
	 *
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setSubsidyContractTypeId( $intSubsidyContractTypeId ) {
		$this->m_intSubsidyContractTypeId = $intSubsidyContractTypeId;
	}

	public function setSubsidyContractTypeName( $strSubsidyContractTypeName ) {
		$this->m_strSubsidyContractTypeName = $strSubsidyContractTypeName;
	}

	public function setAssistancePaymentSummaryRecord( $objAssistancePaymentSummaryRecord ) {
		$this->m_objAssistancePaymentSummaryRecord = $objAssistancePaymentSummaryRecord;
	}

	public function setAssistancePaymentDetailRecord( $objAssistancePaymentDetailRecord ) {
		$this->m_objAssistancePaymentDetailRecord = $objAssistancePaymentDetailRecord;
	}

	public function setAdjustmentPaymentDetailRecord( $objAdjustmentPaymentDetailRecord ) {
		$this->m_objAdjustmentPaymentDetailRecord = $objAdjustmentPaymentDetailRecord;
	}

	public function setApprovedSpecialClaim( $objApprovedSpecialClaim ) {
		$this->m_objApprovedSpecialClaim = $objApprovedSpecialClaim;
	}

	public function setMiscellaneousAccountingRequest( $objMiscellaneousAccountingRequest ) {
		$this->m_objMiscellaneousAccountingRequest = $objMiscellaneousAccountingRequest;
	}

	public function setRepaymentAgreement( $objRepaymentAgreement ) {
		$this->m_objRepaymentAgreement = $objRepaymentAgreement;
	}

	public function setSubsidyContractNumber( $strSubsidyContractNumber ) {
		$this->m_strSubsidyContractNumber = $strSubsidyContractNumber;
	}

	public function setSubsidyTracsVersionId( $intSubsidyTracsVersionId ) {
		$this->m_intSubsidyTracsVersionId = $intSubsidyTracsVersionId;
	}

	public function setAssistancePaymentBillingUnitCount( $intAssistancePaymentBillingUnitCount ) {
		$this->m_intAssistancePaymentBillingUnitCount = $intAssistancePaymentBillingUnitCount;
	}

	public function setAdjustmentAssistancePaymentBillingUnitCount( $intAdjustmentAssistancePaymentBillingUnitCount ) {
		$this->m_intAdjustmentAssistancePaymentBillingUnitCount = $intAdjustmentAssistancePaymentBillingUnitCount;
	}

	/**
	 * Getters
	 *
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getSubsidyContractTypeId() {
		return $this->m_intSubsidyContractTypeId;
	}

	public function getSubsidyContractTypeName() {
		return $this->m_strSubsidyContractTypeName;
	}

	public function getAssistancePaymentSummaryRecord() {
		return $this->m_objAssistancePaymentSummaryRecord;
	}

	public function getAssistancePaymentDetailRecord() {
		return $this->m_objAssistancePaymentDetailRecord;
	}

	public function getAdjustmentPaymentDetailRecord() {
		return $this->m_objAdjustmentPaymentDetailRecord;
	}

	public function getApprovedSpecialClaim() {
		return $this->m_objApprovedSpecialClaim;
	}

	public function getMiscellaneousAccountingRequest() {
		return $this->m_objMiscellaneousAccountingRequest;
	}

	public function getRepaymentAgreement() {
		return $this->m_objRepaymentAgreement;
	}

	public function getSubsidyContractNumber() {
		return $this->m_strSubsidyContractNumber;
	}

	public function getSubsidyTracsVersionId() {
		return $this->m_intSubsidyTracsVersionId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intPropertyId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSubsidyHapRequestStatusTypeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valSubsidyContractId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSubsidyContractId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_contract_id', 'Contract is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPreviousSubsidyHapRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVoucherDate() {

		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIncludeAdjustmentsThroughDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssistancePaymentAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdjustmentPaymentAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRepaymentAgreementAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMiscellaneousRequestAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialClaimsAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function getAssistancePaymentBillingUnitCount() {
		return $this->m_intAssistancePaymentBillingUnitCount;
	}

	public function getAdjustmentAssistancePaymentBillingUnitCount() {
		return $this->m_intAdjustmentAssistancePaymentBillingUnitCount;
	}

	/**
	 * Other Functions
	 * @param      $strVoucherDate
	 * @param      $arrintSubsidyHapRequestStatusIds
	 * @param      $objDatabase
	 * @param bool $boolReturnArray
	 * @return int|null
	 */

	public function loadSubsidyHapRequestCount( $strVoucherDate, $arrintSubsidyHapRequestStatusIds, $boolReturnArray = false, $objDatabase ) {

		if( false == valStr( $strVoucherDate ) || false == valArr( $arrintSubsidyHapRequestStatusIds ) ) return NULL;

		$arrmixSubsidyHapRequest = \Psi\Eos\Entrata\CSubsidyHapRequests::createService()->fetchSubsidyHapRequestCountByPropertyIdBySubsidyContractIdByVoucherDateBySubsidyHapRequestStatusIdsByCid( $this->getPropertyId(), $this->getSubsidyContractId(), $strVoucherDate, $arrintSubsidyHapRequestStatusIds, $this->getCid(), $objDatabase, $this->getId() );

		return ( true == $boolReturnArray ) ? $arrmixSubsidyHapRequest[0]: \Psi\Libraries\UtilFunctions\count( $arrmixSubsidyHapRequest );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['subsidy_contract_type_id'] ) ) $this->setSubsidyContractTypeId( $arrmixValues['subsidy_contract_type_id'] );
		if( true == isset( $arrmixValues['subsidy_contract_type_name'] ) ) $this->setSubsidyContractTypeName( $arrmixValues['subsidy_contract_type_name'] );
		if( true == isset( $arrmixValues['assistance_payment_summary_record'] ) ) 			$this->setAssistancePaymentSummaryRecord( $arrmixValues['assistance_payment_summary_record'] );
		if( true == isset( $arrmixValues['assistance_payment_detail_record'] ) )		    $this->setAssistancePaymentDetailRecord( $arrmixValues['assistance_payment_detail_record'] );
		if( true == isset( $arrmixValues['adjustment_payment_detail_record'] ) ) 			$this->setAdjustmentPaymentDetailRecord( $arrmixValues['adjustment_payment_detail_record'] );
		if( true == isset( $arrmixValues['approved_special_claim'] ) ) 			            $this->setApprovedSpecialClaim( $arrmixValues['approved_special_claim'] );
		if( true == isset( $arrmixValues['miscellaneous_accounting_request'] ) ) 			$this->setMiscellaneousAccountingRequest( $arrmixValues['miscellaneous_accounting_request'] );
		if( true == isset( $arrmixValues['repayment_agreement'] ) ) 						$this->setRepaymentAgreement( $arrmixValues['repayment_agreement'] );
		if( true == isset( $arrmixValues['contract_number'] ) ) 						    $this->setSubsidyContractNumber( $arrmixValues['contract_number'] );
		if( true == isset( $arrmixValues['subsidy_tracs_version_id'] ) ) 					$this->setSubsidyTracsVersionId( $arrmixValues['subsidy_tracs_version_id'] );

	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valSubsidyContractId();
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function canViewForm( $strSystemCode ) {
		switch( $strSystemCode ) {
			case CFileType::SYSTEM_CODE_HUD52670:
			case CFileType::SYSTEM_CODE_HUD52670A_PART1:
			case CFileType::SYSTEM_CODE_HUD52670A_PART3:
			case CFileType::SYSTEM_CODE_HUD52670A_PART4:
			case CFileType::SYSTEM_CODE_HUD52670A_PART5:
			case CFileType::SYSTEM_CODE_HUD52670A_PART6:
				$boolIsValid = ( CSubsidyHapRequestStatusType::OPEN == $this->getSubsidyHapRequestStatusTypeId() );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>