<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportGroups
 * Do not add any new functions to this class.
 */

class CImportGroups extends CBaseImportGroups {

	public static function fetchImportGroups( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CImportGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchImportGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CImportGroup', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedImportGroups( $objDatabase ) {
		$strSql = 'select * FROM import_groups WHERE is_published = 1;';
		return parent::fetchImportGroups( $strSql, $objDatabase );
	}
}
?>