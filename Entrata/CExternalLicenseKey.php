<?php

class CExternalLicenseKey extends CBaseExternalLicenseKey {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVendorName() {
		$boolIsValid = true;

		if( true == is_null( $this->getVendorName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vendor_name', 'Vendor Name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valInterfaceEntityName() {
		$boolIsValid = true;

		if( true == is_null( $this->getInterfaceEntityName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interface_entity_name', 'Interface Entity Name is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valInterfaceEntityLicenseKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getInterfaceEntityLicenseKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interface_entity_license_key', 'Interface Entity License Key is required. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valVendorName();
				$boolIsValid &= $this->valInterfaceEntityName();
				$boolIsValid &= $this->valInterfaceEntityLicenseKey();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valVendorName();
				$boolIsValid &= $this->valInterfaceEntityName();
				$boolIsValid &= $this->valInterfaceEntityLicenseKey();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>