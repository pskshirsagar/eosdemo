<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COwners
 * Do not add any new functions to this class.
 */

class COwners extends CBaseOwners {

	/**
	 * Fetch single record.
	 *
	 */
	public static function fetchOwnerByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						owners
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId;

		return self::fetchOwner( $strSql, $objClientDatabase );
	}

	/**
	 * Fetch muliple records in array format.
	 *
	 */

	public static function fetchPaginatedOwnersWithOwnershipByOwnerFilterByCid( $objOwnerFilter, $intCid, $objClientDatabase, $intOffset = NULL, $intPageLimit = NULL ) {

		$strSqlCondition	= '';
		$arrstrSqlCondition	= self::fetchSearchCriteria( $objOwnerFilter, $intCid );

		if( true == valArr( $arrstrSqlCondition ) ) {
			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
		}

		$strSql = 'SELECT
						DISTINCT ON ( o.id, o.owner_name )
						o.id,
						o.owner_name,
						o.phone_number,
						o.street_line1,
						o.ap_payee_id,
						o.disabled_by,
						o.disabled_on,
						apc.name_first,
						apc.name_last,
						apc.phone_number AS primary_phone_number,
						COUNT( oa.id ) AS ownership_count,
						apl.vendor_code
					FROM
						owners o
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = o.cid AND apc.ap_payee_id = o.ap_payee_id AND ( apc.name_first IS NOT NULL OR apc.name_last IS NOT NULL OR apc.phone_number IS NOT NULL ) )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = apc.cid AND apl.ap_payee_id = apc.ap_payee_id AND apl.is_primary )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id AND apcl.ap_payee_location_id = apl.id AND apcl.is_primary )
						LEFT JOIN owner_associations oa ON ( oa.cid = o.cid AND oa.parent_owner_id = o.id )
					WHERE
						o.cid = ' . ( int ) $intCid . '
						' . $strSqlCondition . '
					GROUP BY
						o.id,
						o.owner_name,
						o.ap_payee_id,
						o.disabled_by,
						o.disabled_on,
						o.phone_number,
						o.street_line1,
						apc.name_first,
						apc.name_last,
						primary_phone_number,
						apl.vendor_code
					ORDER BY
						o.owner_name';

		if( true === is_int( $intOffset ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
		}

		if( true === is_int( $intPageLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intPageLimit;
		}

		return self::fetchOwners( $strSql, $objClientDatabase );
	}

	public static function fetchActiveOwnersByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						o.*
					FROM
						owners o
						JOIN owner_types ot ON(  ot.cid = o.cid AND ot.id = o.owner_type_id )
					WHERE
						o.cid = ' . ( int ) $intCid . '
						AND o.disabled_by IS NULL
						AND o.disabled_on IS NULL
						AND ot.is_enabled = true
					ORDER BY
						o.owner_name';

		return self::fetchOwners( $strSql, $objClientDatabase );
	}

	public static function fetchCountDuplicateOwnersByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						o.owner_name
					FROM
						owners o
						JOIN owner_types ot ON ( ot.cid = o.cid AND ot.id = o.owner_type_id )
					WHERE
						o.cid = ' . ( int ) $intCid . '
						AND o.disabled_by IS NULL
						AND o.disabled_on IS NULL
						AND ot.is_enabled = TRUE
					GROUP BY
						o.owner_name
					HAVING
						count ( o.owner_name ) > 1
					ORDER BY
						o.owner_name';

		$arrmixOwners = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrstrOwners = [];
		foreach( $arrmixOwners as $arrmixOwnerData ) {
			$arrstrOwners[] = $arrmixOwnerData['owner_name'];
		}

		return $arrstrOwners;
	}

	public static function fetchOwnerWithPropertyIdsByIdByCid( $intId, $intCid, $objClientDatabase ) {

		if( false == valId( $intId ) || false == valId( $intCid ) ) return;

		$strSql = 'SELECT
						DISTINCT o.id,
						o.owner_name,
						array_to_string ( array_agg ( p.id ), \', \' ) AS property_ids,
						o.street_line1,
						o.street_line2,
						o.street_line3,
						o.city,
						o.province,
						o.country_code,
						o.state_code,
						o.postal_code,
						o.tax_number_encrypted,
						o.phone_number
					FROM
						owners o
						JOIN properties p ON ( o.cid = p.cid AND o.id = p.owner_id AND p.is_disabled = 0 )
					WHERE
						o.cid = ' . ( int ) $intCid . '
						AND o.id = ' . ( int ) $intId . '
					GROUP BY
						o.id,
						o.owner_name,
						o.street_line1,
						o.street_line2,
						o.street_line3,
						o.city,
						o.province,
						o.country_code,
						o.state_code,
						o.postal_code,
						o.tax_number_encrypted,
						o.phone_number';
		return self::fetchOwner( $strSql, $objClientDatabase );
	}

	public static function fetchOwnersCountWithOwnershipByOwnerFilterByCid( $objOwnerFilter, $intCid, $objClientDatabase ) {

		$strSqlCondition	= '';
		$arrstrSqlCondition	= self::fetchSearchCriteria( $objOwnerFilter );

		if( true == valArr( $arrstrSqlCondition ) ) {
			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
		}

		$strSql = 'SELECT
						COUNT( DISTINCT o.id )
					FROM
						owners o
						LEFT JOIN properties p ON ( o.cid = p.cid AND o.id = p.owner_id )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = o.cid AND apc.ap_payee_id = o.ap_payee_id )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = apc.cid AND apl.ap_payee_id = apc.ap_payee_id AND apl.is_primary )
						LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.cid = apc.cid AND apcl.ap_payee_contact_id = apc.id AND apcl.ap_payee_location_id = apl.id AND apcl.is_primary )
						LEFT JOIN owner_associations oa ON ( oa.cid = o.cid AND oa.parent_owner_id = o.id )
					WHERE
						o.cid = ' . ( int ) $intCid . '
						' . $strSqlCondition;

		return self::fetchOwners( $strSql, $objClientDatabase );
	}

	public static function fetchSearchCriteria( $objOwnerFilter, $intCid = NULL ) {

		if( false === valObj( $objOwnerFilter, 'CFormFilter' ) ) {
			return NULL;
		}

		$arrstrSqlCondition = [];
		$arrmixOwnerFilter  = $objOwnerFilter->getFilterFieldValues();

		if( true == valArr( $arrmixOwnerFilter['property_group_ids'] ?? NULL ) ) {
			$arrstrSqlCondition[] = 'EXISTS ( SELECT 1 FROM properties p WHERE o.cid = p.cid AND o.id = p.owner_id AND p.id IN(  
												 SELECT 
													pga.property_id 
												FROM 
													properties AS p
													JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
													JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
												WHERE 
													p.cid = ' . ( int ) $intCid . '
													AND pg.id IN ( ' . implode( ',', $arrmixOwnerFilter['property_group_ids'] ) . ' )
													AND pg.deleted_by IS NULL
													AND pg.deleted_on IS NULL
											) )';
		}

		if( NULL !== $objOwnerFilter->getFilterConfigs()['owner_ids']['submitted_value'] && true === valArr( $arrmixOwnerFilter['owner_ids'] ) ) {
			$arrstrSqlCondition[] = 'o.id IN( ' . implode( ',', $arrmixOwnerFilter['owner_ids'] ) . ' )';
		}

		if( true == valArr( $arrmixOwnerFilter['ap_payee_contact_ids'] ?? NULL ) ) {
			$arrstrSqlCondition[] = 'apc.id IN( ' . implode( ',', $arrmixOwnerFilter['ap_payee_contact_ids'] ) . ' )';
		}

		if( true == valStr( $arrmixOwnerFilter['vendor_code'] ?? NULL ) ) {
			$arrstrSqlCondition[] = 'apl.vendor_code ILIKE \'%' . addslashes( $arrmixOwnerFilter['vendor_code'] ) . '%\' ';
		}

		if( 0 == $arrmixOwnerFilter['disabled_by'] ) {
			$arrstrSqlCondition[] = ' o.disabled_by IS NULL ';
			$arrstrSqlCondition[] = ' o.disabled_on IS NULL ';
		}

		return $arrstrSqlCondition;
	}

	public static function fetchOwnersByIdsByCid( $arrintOwnerIds, $intCid, $objClientDatabase ) {

		if ( false == valIntArr( $arrintOwnerIds ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						owners o
					WHERE
						o.cid = ' . ( int ) $intCid . '
						AND o.id IN ( ' . implode( ',', $arrintOwnerIds ) . ' )
						AND o.disabled_by IS NULL
						AND o.disabled_on IS NULL';
		return self::fetchOwners( $strSql, $objClientDatabase );
	}

	public static function fetchOwnersByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if ( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						owners
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND disabled_by IS NULL
						AND disabled_on IS NULL';

		return self::fetchOwners( $strSql, $objClientDatabase );
	}

	public static function fetchActiveOwnersDataByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						o.id,
						o.owner_name
					FROM
						owners o
						JOIN owner_types ot ON( ot.cid = o.cid AND ot.id = o.owner_type_id )
					WHERE
						o.cid = ' . ( int ) $intCid . '
						AND o.disabled_by IS NULL
						AND o.disabled_on IS NULL
						AND ot.is_enabled = true
					ORDER BY
						o.owner_name';

		$arrmixOwners = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrstrOwners = [ '' => 'Choose One' ];
		foreach( $arrmixOwners as $arrmixOwnerData ) {
			$arrstrOwners[$arrmixOwnerData['id']] = $arrmixOwnerData['owner_name'];
		}

		return $arrstrOwners;
	}

	public static function fetchOwnersByPropertyIdsByCid( array $arrintPropertyIds, int $intCid, CDatabase $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = '
			SELECT
				o.*
			FROM
				owners o
				JOIN properties p ON p.cid = o.cid AND p.owner_id = o.id AND p.id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
			WHERE
				o.cid = ' . ( int ) $intCid . '
			ORDER BY
				o.owner_name';

		return self::fetchOwners( $strSql, $objClientDatabase );
	}

	public static function fetchOwnersByCidByBySearchKeywords( $intCid, $arrmixRequestData, $objDatabase ) {

		$strAndCondition		= NULL;
		$arrstrAndConditions	= [];
		$arrintSpippedOwnerIds	= array_filter( explode( ',', $arrmixRequestData['fast_lookup_filter']['skipped_owner_ids'] ) );

		if( true == valArr( $arrintSpippedOwnerIds ) ) {
			$arrstrAndConditions[] = ' id NOT IN ( ' . implode( ',', $arrintSpippedOwnerIds ) . ' )';
		}

		$arrstrAndConditions[] = ' o.owner_name ILIKE E\'%' . addslashes( trim( $arrmixRequestData['q'] ) ) . '%\'';

		if( true == valArr( $arrstrAndConditions ) ) {
			$strAndCondition = ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		if( COwner::LOOKUP_TYPE_OWNERS_WITH_PROPERTIES == $arrmixRequestData['fast_lookup_filter']['lookup_type'] ) {

			$strSql = 'SELECT
							DISTINCT o.id,
							o.owner_name,
							array_to_string ( array_agg ( p.property_name ), \' <br>\' ) AS proeprty_names,
							string_agg ( DISTINCT p.id::TEXT, \',\' ) AS property_ids
						FROM
							owners o
							JOIN properties p ON ( o.cid = p.cid AND o.id = p.owner_id AND p.is_disabled = 0 )
						WHERE
							o.cid = ' . ( int ) $intCid . '
							' . $strAndCondition;
			if( false != isset( $arrmixRequestData['owner_ids'] ) && false != valArr( $arrmixRequestData['owner_ids'] ) ) {
				$strSql .= ' AND o.id IN ( ' . implode( ',', $arrmixRequestData['owner_ids']  )  . ' )';
			}
			$strSql .= 'GROUP BY
							o.id,
							o.owner_name
						ORDER BY
							o.owner_name';
		} else {

			$strSql = 'SELECT
							o.id,
							o.owner_name
						FROM
							owners o
						WHERE
							o.cid = ' . ( int ) $intCid . '
							AND disabled_by IS NULL
							AND disabled_on IS NULL' .
									$strAndCondition . '
						ORDER BY
							o.owner_name';
		}

		$arrstrSearchResults = ( array ) fetchData( $strSql, $objDatabase );

		foreach( $arrstrSearchResults as $intKey => $arrmixSearchResult ) {
			$arrstrSearchResults[$intKey]['owner_name'] = preg_replace( '/[^(\x20-\x7F)]*/', '', $arrmixSearchResult['owner_name'] );
		}

		return $arrstrSearchResults;
	}

}
?>