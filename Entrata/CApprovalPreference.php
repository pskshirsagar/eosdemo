<?php

class CApprovalPreference extends CBaseApprovalPreference {

	protected $m_intRouteTypeOrderNum;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCompanyGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSendNotificationEmails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSendApprovalEmailsToGroup() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResetRouteOnEdit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEnabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrintResponse = fetchData( 'SELECT * FROM approval_preferences WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );

		$this->setOriginalValues( $arrintResponse[0] );
		$this->setSerializedOriginalValues( serialize( $arrintResponse[0] ) );

		if( true == $boolReturnSqlOnly ) {

			$strSql = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			$strSql .= PHP_EOL . $this->insertTableLogs( $intCurrentUserId, $objDatabase, 'UPDATE', $boolReturnSqlOnly );

			return $strSql;
		}

		$boolIsValid = true;
		$boolIsValid &= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( false == $this->insertTableLogs( $intCurrentUserId, $objDatabase ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'table_logs', __( 'Failed to insert log.' ) ) );
		}

		return $boolIsValid;
	}

	public function insertTableLogs( $intCurrentUserId, $objDatabase, $strAction = 'UPDATE', $boolReturnSqlOnly = false ) {
		$strOldValuePair	= NULL;
		$strNewValuePair	= NULL;
		$strSql				= '';
		$boolReturnValue 	= true;

		if( $this->getRouteTypeId() != $this->getOriginalValueByFieldName( 'route_type_id' ) ) {
			$strOldValuePair .= 'transaction_type::0' . CRouteType::getTypeNameByTypeId( $this->getOriginalValueByFieldName( 'route_type_id' ) ) . '~^~';
			$strNewValuePair .= 'transaction_type::' . CRouteType::getTypeNameByTypeId( $this->getRouteTypeId() ) . '~^~';
		}

		if( $this->getDefaultCompanyGroupId() != $this->getOriginalValueByFieldName( 'default_company_group_id' ) ) {
			$strOldValuePair .= 'default_company_group_id::' . $this->getOriginalValueByFieldName( 'default_company_group_id' ) . '~^~';
			$strNewValuePair .= 'default_company_group_id::' . $this->getDefaultCompanyGroupId() . '~^~';
		}

		if( $this->getDefaultCompanyUserId() != $this->getOriginalValueByFieldName( 'default_company_user_id' ) ) {
			$strOldValuePair .= 'default_company_user_id::' . $this->getOriginalValueByFieldName( 'default_company_user_id' ) . '~^~';
			$strNewValuePair .= 'default_company_user_id::' . $this->getDefaultCompanyUserId() . '~^~';
		}

		if( $this->convertBoolToString( $this->getSendNotificationEmails() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'send_notification_emails' ) ) ) {
			$strOldValuePair .= 'send_notification_emails::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'send_notification_emails' ) ) . '~^~';
			$strNewValuePair .= 'send_notification_emails::' . $this->convertBoolToString( $this->getSendNotificationEmails() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getResetRouteOnEdit() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'reset_route_on_edit' ) ) ) {
			$strOldValuePair .= 'reset_route_on_edit::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'reset_route_on_edit' ) ) . '~^~';
			$strNewValuePair .= 'reset_route_on_edit::' . $this->convertBoolToString( $this->getResetRouteOnEdit() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getIsEnabled() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'is_enabled' ) ) ) {
			$strOldValuePair .= 'advanced_routing::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'is_enabled' ) ) . '~^~';
			$strNewValuePair .= 'advanced_routing::' . $this->convertBoolToString( $this->getIsEnabled() ) . '~^~';
		}

		if( false == is_null( $strNewValuePair ) || false == is_null( $strOldValuePair ) ) {

			$strDescription = 'Approval preferences has been modified';

			$objTableLog = new CTableLog();
			$boolReturnValue = $objTableLog->insert( $intCurrentUserId, $objDatabase, 'approval_preferences', $this->getId(), $strAction, $this->getCid(), $strOldValuePair, $strNewValuePair, $strDescription, $boolReturnSqlOnly );

			if( true == $boolReturnSqlOnly ) {
				$strSql .= $boolReturnValue;
			}
		}

		return( true == $boolReturnSqlOnly ) ? $strSql : $boolReturnValue;
	}

	private function convertBoolToString( $mixInputValue ) {

		$strReturnValue = '';

		if( 'boolean' === gettype( $mixInputValue ) ) {
			$strReturnValue = ( false != $mixInputValue ) ? 'Yes' : 'No';
		} elseif( 'string' === gettype( $mixInputValue ) ) {
			$strReturnValue = ( 'f' != $mixInputValue ) ? 'Yes' : 'No';
		}

		settype( $strReturnValue, 'string' );

		return $strReturnValue;
	}

	/**
	 * Get Functions
	 */

	public function getRouteTypeOrderNum() {
		return $this->m_intRouteTypeOrderNum;
	}

	/**
	 * Set Functions
	 */

	public function setRouteTypeOrderNum( $intRouteTypeOrderNum ) {
		$this->m_intRouteTypeOrderNum = $intRouteTypeOrderNum;
	}

	/**
	 * Other Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['order_num'] ) ) $this->setRouteTypeOrderNum( $arrmixValues['order_num'] );
	}

}
?>