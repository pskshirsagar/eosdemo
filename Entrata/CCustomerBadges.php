<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerBadges
 * Do not add any new functions to this class.
 */

class CCustomerBadges extends CBaseCustomerBadges {

	public static function fetchCustomerBadgeByCompanyBadgeIdByCustomerIdByCid( $intCompanyBadgeId, $intCustomerId, $intCid, $objDatabase ) {
		if( false == valId( $intCompanyBadgeId ) && false == valId( $intCustomerId ) )  return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( cb.company_badge_id )
						cb.*
					FROM
						customer_badges cb
					WHERE
						cb.company_badge_id =  ' . ( int ) $intCompanyBadgeId . '
						AND cb.customer_id = ' . ( int ) $intCustomerId . '
						AND cb.cid = ' . ( int ) $intCid;

		return self::fetchCustomerBadge( $strSql, $objDatabase );
	}

	public static function fetchUnawardedBaseCustomerBadgesByCid( $intCid, $objDatabase ) {
		$strSql = ' SELECT
						cap.cid AS cid,
						cap.customer_id AS customer_id,
						cob.id AS company_badge_id,
						NOW() AS earned_datetime,
						0 AS is_pro_award,
						1 AS created_by,
						NOW() AS created_on
					FROM
						customer_activity_points cap
						JOIN company_activity_points coap ON ( coap.cid = cap.cid AND coap.id = cap.company_activity_point_id )
						JOIN company_badges cob ON ( cob.cid = coap.cid AND cob.company_activity_point_id = coap.id )
						LEFT JOIN customer_badges cb ON ( cb.cid = cap.cid AND cb.cid = cob.cid AND cb.company_badge_id = cob.id  AND cb.customer_id = cap.customer_id )
					WHERE
						cap.cid = ' . ( int ) $intCid . '
					GROUP BY
						cap.cid,
						cap.customer_id,
						cap.company_activity_point_id,
						cob.id,
						cob.base_criteria,
						cb.id
					HAVING
						0 < cob.base_criteria
						AND COUNT(*) >= cob.base_criteria
						AND cb.id IS NULL';

		return parent::fetchCustomerBadges( $strSql, $objDatabase );
	}

	public static function fetchUnawardedProCustomerBadgesByCid( $intCid, $objDatabase ) {
		$strSql = ' SELECT
						cb.*
					FROM
						customer_activity_points cap
						JOIN company_activity_points coap ON ( coap.cid = cap.cid AND coap.id = cap.company_activity_point_id )
						JOIN company_badges cob ON ( cob.cid = coap.cid AND cob.company_activity_point_id = coap.id )
						LEFT JOIN customer_badges cb ON ( cb.cid = cap.cid AND cb.cid = cob.cid AND cb.company_badge_id = cob.id AND cb.customer_id = cap.customer_id )
					WHERE
						cap.cid = ' . ( int ) $intCid . '
					GROUP BY
						cap.cid,
						cap.customer_id,
						cap.company_activity_point_id,
						cob.id,
						cob.pro_criteria,
						cb.id,
						cb.cid,
						cb.customer_id,
						cb.company_badge_id,
						cb.earned_datetime,
						cb.is_pro_award,
						cb.created_by,
						cb.created_on
					HAVING
						0 < cob.pro_criteria
						AND COUNT(*) >= cob.pro_criteria
						AND cb.id IS NOT NULL
						AND cb.is_pro_award = 0';

		return parent::fetchCustomerBadges( $strSql, $objDatabase );
	}

	public static function fetchCustomerBadgesByPropertyIdsByCid( $arrintPropetyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropetyIds ) ) return NULL;

			$strSql = ' SELECT
							DISTINCT ON ( cub.id )
							cub.*,
							cob.file_handle,
							cob.name,
							lc.lease_id as lease_id
						FROM
							customer_badges cub
							JOIN company_badges cob ON ( cob.cid = cub.cid AND cob.id = cub.company_badge_id )
							JOIN lease_customers lc ON ( lc.cid = cub.cid AND lc.customer_id = cub.customer_id )
							JOIN leases l ON ( l.cid = lc.cid AND l.id = lc.lease_id AND lc.customer_id = l.primary_customer_id )
						WHERE
							cub.cid = ' . ( int ) $intCid . '
							AND l.property_id IN ( ' . implode( ',',  $arrintPropetyIds ) . ' )';

		return parent::fetchCustomerBadges( $strSql, $objDatabase );
	}
}
?>