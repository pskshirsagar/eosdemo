<?php

class CPlaidRequestLog extends CBasePlaidRequestLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerPlaidItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPlaidRequestTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPlaidRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLinkSessionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHttpResponseCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRawRequest() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRawResponse() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>