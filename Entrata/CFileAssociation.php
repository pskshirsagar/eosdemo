<?php

use Psi\Eos\Entrata\CFileSignatures;

class CFileAssociation extends CBaseFileAssociation {

	protected $m_intFileId;
	protected $m_intIsLeaseAgreement;
	protected $m_intIsSigned;
	protected $m_intPageCount;
	protected $m_intFileOrderNum;
	protected $m_intLeasingAgentId;
	protected $m_intPropertyId;
	protected $m_intPacketFileId;
	protected $m_intFileExtensionId;

	protected $m_strDocumentName;
	protected $m_strFileTypeSystemCode;
	protected $m_strFileTypeName;

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strEmailAddress;
	protected $m_strUnitNumber;
	protected $m_strPropertyName;
	protected $m_strFileNote;

	protected $m_boolRequireConfirmation;
	protected $m_boolAttacheToEmail;
	protected $m_boolIsDocumentEsigned;
	protected $m_boolShowInPortal;
	protected $m_boolIsShowAttachment;

	protected $m_strTempFileName;
	protected $m_strFileError;
	protected $m_strFileType;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strFileTitle;
	protected $m_strFileUploadDate;
    protected $m_strMarketingName;

	protected $m_strUploadDocumentPreferenceKey;
	protected $m_strFileNoteDetails;

	protected $m_intUnsignedFileSignaturesCount;

	protected $m_arrobjFileSignatures;

	protected $m_strCountersignedOn;
	protected $m_intCountersignedBy;

	protected $m_boolIsOptionalPolicyAgreement;
	/**
	 * Create Functions
	 */

    public function createFileSignature() {

    	$objFileSignature = new CFileSignature();

    	$objFileSignature->setCid( $this->getCid() );
    	$objFileSignature->setFileAssociationId( $this->getId() );

    	return $objFileSignature;
    }

	public function createFileSignatures( $intPageCount, $intApplicantApplicationId = NULL ) {

		if( 0 >= ( int ) $intPageCount ) return NULL;

		$arrobjFileSignatures = [];
		for( $intIndex = 1; $intIndex <= $intPageCount; $intIndex++ ) {
			$objFileSignature = $this->createFileSignature();
			$objFileSignature->setApplicantApplicationId( $intApplicantApplicationId );
			$objFileSignature->setPageNumber( $intIndex );

			$arrobjFileSignatures[] = $objFileSignature;
		}

		return $arrobjFileSignatures;
	}

	public function createPdfMergeFieldFileSignatures( $arrmixSignatureAndIntialMergefields, $intApplicantApplicationId = NULL ) {

	    if( false == valArr( $arrmixSignatureAndIntialMergefields ) ) return [];

	    $arrobjFileSignatures = [];

	    $objFileSignature = $this->createFileSignature();
	    $objFileSignature->setApplicantApplicationId( $intApplicantApplicationId );

	    foreach( $arrmixSignatureAndIntialMergefields as $arrmixSignatureAndIntialMergefield ) {

	        $objClonedFileSignature = clone $objFileSignature;

	        $objClonedFileSignature->setPageNumber( $arrmixSignatureAndIntialMergefield['page_number'] );
	        $objClonedFileSignature->setUniqueLayoutKey( $arrmixSignatureAndIntialMergefield['unique_layout_key'] );

	        $arrobjFileSignatures[] = $objClonedFileSignature;
	    }

	    return $arrobjFileSignatures;
	}

	/**
	 * Add Functions
	 */

	public function addFileSignature( $objFileSignature ) {
		$this->m_arrobjFileSignatures[$objFileSignature->getId()] = $objFileSignature;
	}

	/**
	 * Set Functions
	 */

	public function setDocumentName( $strDocumentName ) {
		$this->m_strDocumentName = $strDocumentName;
	}

	public function setIsLeaseAgreement( $intIsLeaseAgreement ) {
		$this->m_intIsLeaseAgreement = $intIsLeaseAgreement;
	}

	public function setIsSigned( $intIsSigned ) {
		$this->m_intIsSigned = $intIsSigned;
	}

	public function setFileSignatures( $arrobjFileSignatures ) {
		$this->m_arrobjFileSignatures = $arrobjFileSignatures;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function setFileType( $strFileType ) {
		$this->m_strFileType = $strFileType;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setFileTitle( $strFileTitle ) {
		$this->m_strFileTitle = $strFileTitle;
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->m_intFileExtensionId = $intFileExtensionId;
	}

	public function setFileTypeSystemCode( $strFileTypeSystemCode ) {
		$this->m_strFileTypeSystemCode = $strFileTypeSystemCode;
	}

	public function setFileNote( $strFileNote ) {
		$this->m_strFileNote = $strFileNote;
	}

	public function setUnsignedFileSignaturesCount( $intUnsignedFileSignaturesCount ) {
		$this->m_intUnsignedFileSignaturesCount = $intUnsignedFileSignaturesCount;
	}

	public function setRequireConfirmation( $boolRequireConfirmation ) {
		$this->m_boolRequireConfirmation = $boolRequireConfirmation;
	}

	public function setAttacheToEmail( $boolAttacheToEmail ) {
		$this->m_boolAttacheToEmail = $boolAttacheToEmail;
	}

	public function setPageCount( $intPageCount ) {
		$this->m_intPageCount = $intPageCount;
	}

	public function setLeasingAgentId( $intLeasingAgentId ) {
		$this->m_intLeasingAgentId = $intLeasingAgentId;
	}

	public function setPropertyName( $strPropertyName ) {
		 $this->m_strPropertyName = $strPropertyName;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setFileTypeName( $strFileTypeName ) {
		$this->m_strFileTypeName = $strFileTypeName;
	}

	public function setFileUploadDate( $strFileUploadDate ) {
		$this->m_strFileUploadDate = $strFileUploadDate;
	}

	public function setFileOrderNum( $intFileOrderNum ) {
		$this->m_intFileOrderNum = $intFileOrderNum;
	}

	public function setPacketFileId( $intPacketFileId ) {
		$this->m_intPacketFileId = $intPacketFileId;
	}

	public function setUploadDocumentPreferenceKey( $strUploadDocumentPreferenceKey ) {
		$this->m_strUploadDocumentPreferenceKey = $strUploadDocumentPreferenceKey;
	}

    public function setMarketingName( $strMarketingName ) {
        $this->m_strMarketingName = $strMarketingName;
    }

	public function setFileNoteDetails( $strFileNoteDetails ) {
		$this->m_strFileNoteDetails = $strFileNoteDetails;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
	 	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

	 	if( true == isset( $arrmixValues['file_id'] ) ) 						$this->setFileId( $arrmixValues['file_id'] );
		if( true == isset( $arrmixValues['document_name'] ) ) 					$this->setDocumentName( $arrmixValues['document_name'] );
		if( true == isset( $arrmixValues['is_lease_agreement'] ) ) 			$this->setIsLeaseAgreement( $arrmixValues['is_lease_agreement'] );
		if( true == isset( $arrmixValues['unsigned_file_signatures_count'] ) ) $this->setUnsignedFileSignaturesCount( $arrmixValues['unsigned_file_signatures_count'] );
		if( true == isset( $arrmixValues['file_name'] ) ) 						$this->setFileName( $arrmixValues['file_name'] );
		if( true == isset( $arrmixValues['file_path'] ) ) 						$this->setFilePath( $arrmixValues['file_path'] );
		if( true == isset( $arrmixValues['title'] ) ) 							$this->setFileTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['file_extension_id'] ) ) 				$this->setFileExtensionId( $arrmixValues['file_extension_id'] );
		if( true == isset( $arrmixValues['file_type_system_code'] ) ) 			$this->setFileTypeSystemCode( $arrmixValues['file_type_system_code'] );
		if( true == isset( $arrmixValues['file_type_name'] ) ) 				$this->setFileTypeName( $arrmixValues['file_type_name'] );
		if( true == isset( $arrmixValues['require_confirmation'] ) ) 			$this->setRequireConfirmation( $arrmixValues['require_confirmation'] );
		if( true == isset( $arrmixValues['attach_to_email'] ) ) 				$this->setAttacheToEmail( $arrmixValues['attach_to_email'] );

		if( true == isset( $arrmixValues['name_first'] ) ) 					$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) 						$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['email_address'] ) ) 					$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['unit_number'] ) ) 					$this->setUnitNumber( $arrmixValues['unit_number'] );
		if( true == isset( $arrmixValues['property_name'] ) ) 					$this->setPropertyName( $arrmixValues['property_name'] );

		if( true == isset( $arrmixValues['leasing_agent_id'] ) ) 				$this->setLeasingAgentId( $arrmixValues['leasing_agent_id'] );
		if( true == isset( $arrmixValues['property_id'] ) ) 					$this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['file_upload_date'] ) ) 				$this->setFileUploadDate( $arrmixValues['file_upload_date'] );

		if( true == isset( $arrmixValues['note'] ) ) 							$this->setFileNote( $arrmixValues['note'] );
		if( true == isset( $arrmixValues['file_order_num'] ) ) 				$this->setFileOrderNum( $arrmixValues['file_order_num'] );
		if( true == isset( $arrmixValues['packet_file_id'] ) ) 				$this->setPacketFileId( $arrmixValues['packet_file_id'] );

		if( true == isset( $arrmixValues['upload_document_preference_key'] ) ) 				$this->setUploadDocumentPreferenceKey( $arrmixValues['upload_document_preference_key'] );
        if( true == isset( $arrmixValues['marketing_name'] ) )		                    $this->setMarketingName( $arrmixValues['marketing_name'] );
		if( true == isset( $arrmixValues['countersigned_by'] ) )		                    $this->setCountersignedBy( $arrmixValues['countersigned_by'] );
		if( true == isset( $arrmixValues['countersigned_on'] ) )		                    $this->setCountersignedOn( $arrmixValues['countersigned_on'] );
		if( true == isset( $arrmixValues['show_in_portal'] ) )		                    $this->setShowInPortal( $arrmixValues['show_in_portal'] );
		if( true == isset( $arrmixValues['file_note_details'] ) )		                    $this->setFileNoteDetails( $arrmixValues['file_note_details'] );
		if( true == isset( $arrmixValues['is_optional_policy_agreement'] ) )       $this->setIsOptionalPolicyAgreement( $arrmixValues['is_optional_policy_agreement'] );
	}

	public function setFileData( $arrstrFileData ) {
		if( true == isset( $arrstrFileData['tmp_name'] ) ) 	$this->setTempFileName( $arrstrFileData['tmp_name'] );
		if( true == isset( $arrstrFileData['error'] ) ) 	$this->setFileError( $arrstrFileData['error'] );
		if( true == isset( $arrstrFileData['name'] ) ) 		$this->setFileName( $arrstrFileData['name'] );
		if( true == isset( $arrstrFileData['path'] ) ) 		$this->setFilePath( $arrstrFileData['path'] );
		if( true == isset( $arrstrFileData['type'] ) ) 		$this->setFileType( $arrstrFileData['type'] );

		if( true == is_null( $this->getFilePath() ) ) {
			$this->setFilePath( $this->buildFileAssociationFilePath() );
		}
    }

    public function setCountersignedOn( $strCountersignedOn ) {
		$this->m_strCountersignedOn = $strCountersignedOn;
    }

	public function setCountersignedBy( $intCountersignedBy ) {
		$this->m_intCountersignedBy = $intCountersignedBy;
	}

	public function setShowInPortal( $boolShowInPortal ) {
		$this->m_boolShowInPortal = $boolShowInPortal;
	}

	public function setIsShowAttachment( $boolIsShowAttachment ) {
		$this->m_boolIsShowAttachment = $boolIsShowAttachment;
	}

	public function setIsOptionalPolicyAgreement( $boolIsOptionalPolicyAgreement ) {
		$this->m_boolIsOptionalPolicyAgreement = $boolIsOptionalPolicyAgreement;
	}

    /**
     * Get Functions
     */

    public function getFileUploadDate() {
    	return $this->m_strFileUploadDate;
    }

    public function getDocumentName() {
    	return $this->m_strDocumentName;
    }

    public function getIsLeaseAgreement() {
    	return $this->m_intIsLeaseAgreement;
    }

    public function getIsSigned() {
    	return $this->m_intIsSigned;
    }

    public function getFileSignatures() {
    	return $this->m_arrobjFileSignatures;
    }

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function getFileType() {
		return $this->m_strFileType;
	}

	public function getFileNote() {
		return $this->m_strFileNote;
	}

	public function getUnsignedFileSignaturesCount() {
		return $this->m_intUnsignedFileSignaturesCount;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getFileTitle() {
		return $this->m_strFileTitle;
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function getFileOrderNum() {
		return $this->m_intFileOrderNum;
	}

	public function getPacketFileId() {
		return $this->m_intPacketFileId;
	}

	public function getFullFilePath( $strFolderName = PATH_MOUNTS_FILES ) {

		switch( $this->getFileTypeSystemCode() ) {
			case CFileType::SYSTEM_CODE_PRE_SIGNED:
			case CFileType::SYSTEM_CODE_PARTIALLY_SIGNED:
			case CFileType::SYSTEM_CODE_SIGNED:
			case CFileType::SYSTEM_CODE_LEASE_ADDENDUM:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE:
			case CFileType::SYSTEM_CODE_LEASE_ESA:
				$strFolderName = PATH_MOUNTS_DOCUMENTS;
				break;

			case CFileType::SYSTEM_CODE_POLICY:
				$strFolderName = PATH_MOUNTS_FILES;
				break;

			default:
				// default  case
				break;
		}

		return getMountsPath( $this->getCid(), $strFolderName ) . $this->getFilePath();
	}

	public function getFileParentFolderName() {
		switch( $this->getFileTypeSystemCode() ) {
			case CFileType::SYSTEM_CODE_PRE_SIGNED:
			case CFileType::SYSTEM_CODE_PARTIALLY_SIGNED:
			case CFileType::SYSTEM_CODE_SIGNED:
			case CFileType::SYSTEM_CODE_LEASE_ADDENDUM:
			case CFileType::SYSTEM_CODE_OTHER_ESIGNED_LEASE:
			case CFileType::SYSTEM_CODE_LEASE_ESA:
			case CFileType::SYSTEM_CODE_LEASE_PACKET:
				return PATH_MOUNTS_DOCUMENTS;

			default:
				return PATH_MOUNTS_FILES;
		}
	}

	public function getFileTypeSystemCode() {
		return $this->m_strFileTypeSystemCode;
	}

	public function getRequireConfirmation() {
		return $this->m_boolRequireConfirmation;
	}

	public function getAttachToEmail() {
		return $this->m_boolAttacheToEmail;
	}

	public function getPageCount() {
		return $this->m_intPageCount;
	}

	public function getLeasingAgentId() {
		return $this->m_intLeasingAgentId;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return 	$this->m_strNameLast;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getFileTypeName() {
		return $this->m_strFileTypeName;
	}

	public function getUploadDocumentPreferenceKey() {
    	return $this->m_strUploadDocumentPreferenceKey;
	}

    public function getMarketingName() {
        return $this->m_strMarketingName;
    }

	public function getCountersignedOn() {
		return $this->m_strCountersignedOn;
	}

	public function getCountersignedBy() {
		return $this->m_intCountersignedBy;
	}

	public function getShowInPortal() {
		return $this->m_boolShowInPortal;
	}

	public function getIsShowAttachment() {
		return $this->m_boolIsShowAttachment;
	}

	public function getFileNoteDetails() {
		return $this->m_strFileNoteDetails;
	}

	public function getIsOptionalPolicyAgreement() {
		return $this->m_boolIsOptionalPolicyAgreement;
	}

	/**
     * Fetch Functions
     */

    public function fetchFileSignatures( $objDatabase ) {
    	return CFileSignatures::createService()->fetchFileSignaturesByCidByFileAssociationId( $this->getCid(), $this->getId(), $objDatabase );
    }

    public function fetchSignedFileSignatures( $objDatabase ) {
    	return CFileSignatures::createService()->fetchSignedFileSignaturesByFileAssociationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

    public function fetchFileSignature( $objDatabase ) {
    	return CFileSignatures::createService()->fetchFileSignatureByFileAssociationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

	public function fetchApplication( $objDatabase ) {
    	return CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
    }

    /**
     * Validte Functions
     */

	public function valUploadSignedLeaseAgreement() {

		$boolIsValid = true;

		$arrstrAllowedFileTypes = [ 'application/msword', 'application/pdf' ];

		if( UPLOAD_ERR_NO_FILE != $this->getFileError() && false == in_array( $this->getFileType(), $arrstrAllowedFileTypes ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unrecognized file type. Please choose .doc files only.' ) );
			return $boolIsValid;
    	}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to upload properly. Files cannot be larger than ' . PHP_MAX_POST . 'B.' ) );
			return $boolIsValid;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded , Missing a temporary folder.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valOtherLeaseDocument() {

		$boolIsValid = true;

		$arrstrAllowedFileTypes = [ 'application/msword', 'image/bmp', 'image/jpeg', 'application/pdf', 'image/png', 'image/tiff', 'image/pjpeg' ];

		if( UPLOAD_ERR_NO_FILE != $this->getFileError() && false == in_array( $this->getFileType(), $arrstrAllowedFileTypes ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unrecognized file type. Please choose .tif, .png, .doc, .docx, .jpg, .jpeg, .bmp and .pdf files only.' ) );
			return $boolIsValid;
    	}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to upload properly. Files cannot be larger than ' . PHP_MAX_POST . 'B.' ) );
			return $boolIsValid;
		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded , Missing a temporary folder.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valOtherEsignDocument() {

		$boolIsValid = true;

		if( true == is_null( $this->getApplicationId() ) && true == is_null( $this->getLeaseId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Application Id or Lease Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDocumentId() {
        $boolIsValid = true;

        if( true == is_null( $this->getDocumentId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_id', 'Main document is required.' ) );
        }

        return $boolIsValid;
    }

	public function valApplicationId() {
        $boolIsValid = true;

        if( true == is_null( $this->getApplicationId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_id', 'Application id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valScheduledEmailId() {
        $boolIsValid = true;

        if( true == is_null( $this->getScheduledEmailId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_id', 'Scheduled email id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valBankAccountId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getBankAccountId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bank_account_id', 'Bank account is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valFileId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getFileId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_id', 'File id is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valApPayeeLocationId() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getApPayeeLocationId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_location_id', 'Location is required.' ) );
    	}

    	return $boolIsValid;
    }

	public function valApPayeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApPayeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payee_id', 'Vendor is required.' ) );
		}

		return $boolIsValid;
	}

	public function valApPaymentId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApPaymentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_payment_id', 'Payment is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLeaseAccommodationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valApplicationId();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valApplicationId();
            	break;

            case VALIDATE_DELETE:
            	break;

	        case 'upload_document':
		        $boolIsValid &= $this->valCid();
		        break;

            case 'upload_ap_payee_document':
            	$boolIsValid &= $this->valApPayeeLocationId();
            	break;

            case 'upload_signed_lease_agreement':
            	$boolIsValid &= $this->valUploadSignedLeaseAgreement();
            	break;

            case 'other_lease_document':
           		$boolIsValid &= $this->valOtherLeaseDocument();
            	break;

            case 'validate_lease_file':
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valOtherEsignDocument();
            	break;

            case 'message_center_document':
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valScheduledEmailId();
            	break;

           	case 'bank_account_signature_file':
             	// $boolIsValid &= $this->valCid();
             	$boolIsValid &= $this->valBankAccountId();
             	$boolIsValid &= $this->valFileId();
           		break;

	        case 'validate_esa':
		        $boolIsValid &= $this->valCid();
		        break;

	        case 'lien_waiver_file':
		        $boolIsValid &= $this->valApPayeeId();
		        $boolIsValid &= $this->valApPaymentId();
		        break;

			case 'upload_mac':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valFileId();
				break;

	        case 'upload_owner_document':
		        $boolIsValid &= $this->valCid();
		        break;

	        default:
           		$boolIsValid = true;
           		break;
        }
        return $boolIsValid;
    }

	/**
	 * Other Functions
	 */

    public function checkDocumentESigned() {

		$arrobjFileSignatures = $this->getFileSignatures();

		if( false == valArr( $arrobjFileSignatures ) ) return false;

		foreach( $arrobjFileSignatures as $objFileSignature ) {
			if( true == is_null( $objFileSignature->getInitials() ) && true == is_null( $objFileSignature->getAgreementDatetime() ) && true == is_null( $objFileSignature->getIpAddress() ) ) {
				$this->m_boolIsDocumentEsigned = false;
				return $this->m_boolIsDocumentEsigned;
			}
		}

		$this->m_boolIsDocumentEsigned = true;

		return $this->m_boolIsDocumentEsigned;
    }

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = true, $boolReturnSqlOnly = false ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( ' NOW() ' );
		$this->setAllowDifferentialUpdate( true );

		if( $this->update( $intUserId, $objDatabase ) ) {
			return true;
		}
	}

    public function deleteFile() {
	    // TODO: This function is going to be deprecated, Please use deleteObject() with IObjectStorageGateway.

	    if( false == is_null( $this->getFilePath() ) && false == is_null( $this->getFileName() ) ) {

    		$arrstrFileExtensionsToDelete = [ '.tif', '.png', '.doc', '.docx', '.jpg', '.jpeg', '.bmp', '.pdf' ];

    		$arrstrFileNameExtension = explode( '.', $this->getFileName() );

    		foreach( $arrstrFileExtensionsToDelete as $strFileExtension ) {
    			if( file_exists( getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->getFilePath() . $arrstrFileNameExtension[0] . $strFileExtension ) ) {
    				unlink( getMountsPath( $this->getCid(), PATH_MOUNTS_DOCUMENTS ) . $this->getFilePath() . $arrstrFileNameExtension[0] . $strFileExtension );
    			} elseif( file_exists( getMountsPath( $this->getCid(), PATH_MOUNTS_FILES ) . $this->getFilePath() . $arrstrFileNameExtension[0] . $strFileExtension ) ) {
    				unlink( getMountsPath( $this->getCid(), PATH_MOUNTS_FILES ) . $this->getFilePath() . $arrstrFileNameExtension[0] . $strFileExtension );
    			}
    		}
    	}
    }

	public function deleteObject( $objObjectStorageGateway ) {
		if( false == is_null( $this->getFilePath() ) && false == is_null( $this->getFileName() ) ) {

			$arrstrFileExtensionsToDelete = [ '.tif', '.png', '.doc', '.docx', '.jpg', '.jpeg', '.bmp', '.pdf' ];

			$arrstrFileNameExtension = explode( '.', $this->getFileName() );

			foreach( $arrstrFileExtensionsToDelete as $strFileExtension ) {
				$arrobjResponse = $objObjectStorageGateway->getObject( [
					'cid'         => $this->getCid(),
					'objectId'    => -1,
					'container'   => PATH_MOUNTS_DOCUMENTS,
					'key'         => $this->getFilePath() . $arrstrFileNameExtension[0] . $strFileExtension,
					'checkExists' => true
				] );

				if( $arrobjResponse['isExists'] ) {
					$arrobjResponse = $objObjectStorageGateway->deleteObject( [
						'cid'             => $this->getCid(),
						'objectId'        => -1,
						'container'       => PATH_MOUNTS_DOCUMENTS,
						'key'             => $this->getFilePath() . $arrstrFileNameExtension[0] . $strFileExtension
					] );
				} else {

					$arrobjResponse = $objObjectStorageGateway->getObject( [
						'cid'         => $this->getCid(),
						'objectId'    => -1,
						'container'   => PATH_MOUNTS_FILES,
						'key'         => $this->getFilePath() . $arrstrFileNameExtension[0] . $strFileExtension,
						'checkExists' => true
					] );

					if( $arrobjResponse['isExists'] ) {
						$arrobjResponse = $objObjectStorageGateway->deleteObject( [
							'cid'             => $this->getCid(),
							'objectId'        => -1,
							'container'       => PATH_MOUNTS_FILES,
							'key'             => $this->getFilePath() . $arrstrFileNameExtension[0] . $strFileExtension
						] );
					}

				}
			}
		}
	}

    public function buildFileData( $objDatabase, $strInputTypeFileName = 'lease_file_name' ) {

    	if( true == is_null( $this->getId() ) || false == is_numeric( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
    	}

    	$arrstrFileInfo = pathinfo( $_FILES[$strInputTypeFileName]['name'] );
		$strExtension 	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;
		$strFileName 	= date( 'Y' ) . date( 'm' ) . date( 'd' ) . '_' . $this->getCid() . '_' . $this->getId() . '.' . $strExtension;

		// Getting the correct mime-type
		$strMimeType 	= '';
		$objFileExtension 	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		$arrstrFileData = [ 'tmp_name' => $_FILES[$strInputTypeFileName]['tmp_name'], 'error' => $_FILES[$strInputTypeFileName]['error'], 'name' => $strFileName, 'type' => $strMimeType ];
		$this->setFileData( $arrstrFileData );
    }

	public function buildFileAssociationFilePath() {
		return 'leases/' . $this->getApplicationId() . '/';
	}

	public function makeCopyAndArchive( $objDatabase ) {
   		$objSignedLeaseDocumentAssociation = clone $this;

   		$objSignedLeaseDocumentAssociation->setId( $objSignedLeaseDocumentAssociation->fetchNextId( $objDatabase ) );

   		$this->setIsArchived( 1 );

   		return $objSignedLeaseDocumentAssociation;
   	}

	public function buildFilePath( $strSystemCode ) {

		$strPath = '';

		switch( $strSystemCode ) {

			case CFileType::SYSTEM_CODE_POLICY:
					$strPath = 'policy_documents/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getApplicationId() . '/';
				break;

					$strPath = date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_APPLICATION:
				$strPath = $this->getCid() . '/applications/application/' . $this->getPropertyId() . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_OFFER_TO_RENT:
					$strPath = $this->getCid() . '/applications/offer_to_rent/' . $this->getPropertyId() . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_GUEST_CARD:
					$strPath = $this->getCid() . '/guestcards/' . $this->getPropertyId() . '/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_QUOTE:
				$strPath = $this->getCid() . '/applications/quote/' . $this->getPropertyId() . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_WAITLIST:
			case CFileType::SYSTEM_CODE_WORK_ORDER:
				$strPath = date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_VERIFICATION_DOCUMENT:
			case CFileType::SYSTEM_CODE_DOB_VERIFICATION:
			case CFileType::SYSTEM_CODE_CITIZENSHIP_VERIFICATION:
			case CFileType::SYSTEM_CODE_SSN_VERIFICATION:
					$strPath = 'verification_documents/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_GCIC:
				$strPath = 'gcic_consent_form/' . $this->getApplicationId() . '/' . $this->getApplicantId() . '/';
				break;

			case CFileType::SYSTEM_CODE_SIGNATURE:
				$strPath = 'signature/' . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/' . $this->getApplicationId() . '/';
				break;

			case CFileType::SYSTEM_CODE_VIOLATION_MANAGER:
				$strPath = 'leases/' . $this->getDocumentId() . '/';
				break;

			default:
				$strPath = 'leases/' . $this->getApplicationId() . '/';

		}

		return $strPath;
	}

	public function buildFileName( $strExtension = 'docx' ) {
		return  $this->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;
	}

	public function loadPageCount( $objObjectStorageGateway ) {
		$this->m_intPageCount = NULL;

		if( CFileExtension::APPLICATION_PDF != $this->getFileExtensionId() ) {
			return NULL;
		}

        $objFile = \Psi\Eos\Entrata\CFiles::createService()->fetchFileByIdByCid( $this->getFileId(), $this->getCid(), $this->m_objDatabase );
		if( is_null( $objFile->getFileTypeSystemCode() ) ) {
			$objFileType = \Psi\Eos\Entrata\CFileTypes::createService()->fetchFileTypeByIdByCid( $objFile->getFileTypeId(), $this->getCid(), $this->m_objDatabase );
			$objFile->setFileTypeSystemCode( $objFileType->getSystemCode() );
		}
		$arrmixGatewayRequest = $objFile->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile'  => 'temp' ] );
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( $arrmixGatewayRequest );

		$strTempPdfFilePath = $objObjectStorageGatewayResponse['outputFile'];

		$objApplicationSystemLibrary = new CApplicationSystemLibrary( $this->m_objApplicationDataObject );
		$strTempPdfFilePath = $objApplicationSystemLibrary->checkAndChangePdfVersion( $strTempPdfFilePath, $this->getCid() );

		if( is_file( $strTempPdfFilePath ) ) {
			$objZendPdf = Zend_Pdf::load( $strTempPdfFilePath );

			$this->m_intPageCount = \Psi\Libraries\UtilFunctions\count( $objZendPdf->pages );
		}
		return $this->m_intPageCount;
	}

	public function updateFileSignatureIsViewed( $intCurrentUserId, $objDatabase ) {
		$objFileSignature = $this->fetchFileSignature( $objDatabase );

		if( false == valObj( $objFileSignature, 'CFileSignature' ) ) {
			$objFileSignature = $this->createFileSignature();
		}

		$objFileSignature->setIsViewed( 1 );

		if( false == $objFileSignature->insertOrUpdate( $intCurrentUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objFileSignature->getErrorMsgs() );
			return NULL;
		}
		return true;
	}

}
?>