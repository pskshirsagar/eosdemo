<?php

class CMerchantMethod extends CBaseMerchantMethod {

	use TMerchantMethod;

	private $m_intTier;

	// region Additional Accessors

	// region Tier

	public function getTier() {
		return $this->m_intTier;
	}

	public function setTier( $intTier ) {
		$this->m_intTier = $intTier;
		return $this;
	}

	// endregion

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrmixValues['tier'] ) ) {
			$this->setTier( $arrmixValues['tier'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	// endregion

	// region Validation

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProcessingBankAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMerchantGatewayId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentMediumId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPin() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGatewayUsernameEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGatewayPasswordEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginUsernameEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginPasswordEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxPaymentAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUndelayedSettlementCeiling() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxMonthlyProcessAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinimumConvenienceFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGatewayTransactionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentTransactionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReverseTransactionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaivedTransactionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyReturnFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAuthSuccessFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAuthFailureFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDowngradeTransactionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInternationalTransactionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentReturnFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyDebitTransactionFee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProviderDiscountRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResidentDiscountRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyDiscountRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaivedDiscountRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDowngradeDiscountRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInternationalDiscountRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyDebitDiscountRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDistributionDelayDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReversalDelayDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCutOffTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCalcFeesOnMaxPayment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneAuthRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsControlledDistribution() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEnabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	// endregion

}
