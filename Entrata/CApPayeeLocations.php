<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeLocations
 * Do not add any new functions to this class.
 */

class CApPayeeLocations extends CBaseApPayeeLocations {

	public static function fetchApPayeeLocationsByApPayeeIdOrderByLocationNameByCid( $intApPayeeId, $intCid, $objClientDatabase ) {
		return self::fetchApPayeeLocations( sprintf( 'SELECT * FROM ap_payee_locations WHERE ap_payee_id = ' . ( int ) $intApPayeeId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY is_primary DESC, LOWER( location_name ) ASC' ), $objClientDatabase );
	}

	public static function fetchApPayeeLocationsByApPayeeIdByPropertyIdsByCid( $intApPayeeId, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						apl.*
					FROM
						ap_payee_locations apl
						LEFT JOIN ap_payee_property_groups appg ON ( apl.cid = appg.cid AND apl.id = appg.ap_payee_location_id )
						LEFT JOIN property_groups pg ON ( pg.cid = apl.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pga.property_group_id = pg.id )
					WHERE
						apl.cid =  ' . ( int ) $intCid . '
						AND apl.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
					ORDER BY
						LOWER( apl.location_name ) ASC';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationByIdByApPayeeIdByCid( $intId, $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apl.*,
						ap.company_name AS company_name,
						ale.receives_1099
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						JOIN ap_legal_entities ale ON ( apl.cid = ale.cid AND apl.ap_legal_entity_id = ale.id )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.id = ' . ( int ) $intId . ' 
						AND ale.deleted_by IS NULL 
						AND ale.deleted_on IS NULL 
						AND apl.ap_payee_id = ' . ( int ) $intApPayeeId;

		return self::fetchApPayeeLocation( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase, $boolIsSoftDelete = false ) {

		if( false == valArr( $arrintApPayeeIds ) ) return NULL;
		$strCondition = '';
		if( true == $boolIsSoftDelete ) {
			$strCondition = 'AND deleted_by IS NULL
							AND deleted_on IS NULL';
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' ) ' . $strCondition;

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchEnabledApPayeeLocationsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND disabled_on IS NULL
						AND deleted_on IS NULL';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationsByIdsByCid( $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeLocationIds ) ) return NULL;
		return self::fetchApPayeeLocations( 'SELECT * FROM ap_payee_locations WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )', $objClientDatabase );
	}

	public static function fetchSimpleApPayeeLocationsByCid( $intCid, $objClientDatabase ) {
		return self::fetchApPayeeLocations( 'SELECT * FROM ap_payee_locations WHERE cid = ' . ( int ) $intCid . ' ORDER BY is_primary DESC', $objClientDatabase );
	}

	public static function fetchApPayeeLocationsByIdsByCids( $arrintApPayeeLocationIds, $arrintCids, $objClientDatabase ) {
		if( false == valIntArr( $arrintApPayeeLocationIds ) || false == valIntArr( $arrintCids ) ) return NULL;
		return self::fetchApPayeeLocations( 'SELECT * FROM ap_payee_locations WHERE cid IN ( ' . implode( ',', $arrintCids ) . ' ) AND id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )', $objClientDatabase, false );
	}

	public static function fetchSyncedApPayeeLocationsByIdsByCids( $arrintApPayeeLocationIds, $arrintCids, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeLocationIds ) || false == valIntArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						store_id IS NOT NULL
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase, false );
	}

	public static function fetchApPayeeLocationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT apl.id,
						apl.cid,
						apl.ap_payee_id,
						apl.store_id,
						apl.payee_name,
						apl.vendor_code,
						apl.location_name,
						apl.location_datetime,
						apl.notes,
						apl.postal_code,
						apl.is_primary,
						apl.disabled_by,
						apl.disabled_on,
						apl.ap_legal_entity_id
					FROM
						ap_payee_locations apl
						JOIN ap_payee_property_groups appg ON ( apl.cid = appg.cid AND apl.id = appg.ap_payee_location_id )
						JOIN property_groups pg ON ( pg.cid = apl.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND appg.property_group_id IN (
														SELECT
															DISTINCT property_group_id
														FROM
															property_group_associations pga
															JOIN property_groups pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
														WHERE
															pg.cid = ' . ( int ) $intCid . '
															AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
														)
					ORDER BY
						apl.is_primary DESC';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationsByApPayeeIdByIsPrimaryByCid( $intApPayeeId, $boolIsPrimary, $intCid, $objClientDatabase ) {

		$strCondition = 'apl.is_primary = true';

		if( true != $boolIsPrimary ) {
			$strCondition = 'apl.is_primary = false';
		}

		$strSql = 'SELECT
						apl.*
					FROM
						ap_payee_locations apl
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ' . $strCondition . '
					ORDER BY
						apl.location_name DESC';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchPrimaryApPayeeLocationByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT * FROM ap_payee_locations WHERE cid = ' . ( int ) $intCid . ' AND ap_payee_id = ' . ( int ) $intApPayeeId . ' AND is_primary = TRUE LIMIT 1;';
		return self::fetchApPayeeLocation( $strSql, $objClientDatabase );
	}

	public static function fetchEnabledApPayeeLocationsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND disabled_on IS NULL
						AND deleted_by IS NULL
						AND location_name IS NOT NULL
					ORDER BY
						LOWER( location_name ) ASC';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationsByIdsApPayeeIdsByCid( $arrintIds, $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintIds ) || false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						apl.*,
						ap.company_name AS ap_payee_name
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND apl.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationsByApPayeeIdsByIsPrimaryByCid( $arrintApPayeeIds, $intIsPrimary, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeIds ) ) return NULL;

		$strCondition = 'apl.is_primary = false';
		if( 1 == $intIsPrimary ) {
			$strCondition = 'apl.is_primary = true';
		}

		$strSql = 'SELECT
						apl.*,
						lower(ap.company_name) as company_name
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.ap_payee_id IN ( ' . implode( ', ', $arrintApPayeeIds ) . ' )
						AND ' . $strCondition;

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchSyncApPayeeLocationsByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						vp_location_id
					FROM
						ap_payee_locations
					WHERE
						ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCid . '
						AND vp_location_id IS NOT NULL';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchCustomApPayeeLocationsByCid( $intCid, $objDatabase, $boolIsImportInvoiceCsv = false ) {

		$strSql = 'SELECT
						apl.*,
						ap.company_name AS ap_payee_name
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
					WHERE
						apl.cid = ' . ( int ) $intCid;

		if( true == $boolIsImportInvoiceCsv ) {
			$strSql .= ' AND apl.vendor_code IS NOT NULL';
		}

		return self::fetchApPayeeLocations( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApPayeeLocationsDetailByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase, $objPagination = NULL, $boolIsUpdatedListing = false ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						apl.id,
						apl.location_name,
						apl.vendor_code,
						apl.is_primary,
						apl.store_id,
						ale.entity_name,
						ale.tax_number_encrypted,
						apl.street_line1,
						apl.street_line2,
						apl.street_line3,
						apl.city,
						apl.state_code,
						apl.postal_code,
						apl.country_code,
						COUNT ( DISTINCT apa.id ) AS accounts,
						array_to_string( array_agg( DISTINCT apr.id ), \',\' ) AS remittance_info
					FROM
						ap_payee_locations apl
						LEFT JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						LEFT JOIN ap_legal_entities ale ON ( ap.cid = ale.cid AND apl.ap_legal_entity_id = ale.id )
						LEFT JOIN ap_payee_accounts apa ON ( ale.cid = apa.cid AND apl.id = apa.ap_payee_location_id AND apa.account_number IS NOT NULL )
						LEFT JOIN ap_remittances apr ON ( ale.cid = apr.cid AND apl.id = apr.ap_payee_location_id )
					WHERE
						ap.id = ' . ( int ) $intApPayeeId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND apl.location_name IS NOT NULL
						AND apr.is_default = true
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
					GROUP BY
						apl.id,
						apl.location_name,
						apl.is_primary,
						apl.vendor_code,
						apl.store_id,
						ale.entity_name,
						ale.tax_number_encrypted,
						apl.street_line1,
						apl.street_line2,
						apl.street_line3,
						apl.city,
						apl.state_code,
						apl.postal_code,
						apl.country_code,
						apl.updated_on';

		if( true == $boolIsUpdatedListing ) {
			$strSql .= ' ORDER BY
						apl.updated_on DESC';
		} else {
			$strSql .= ' ORDER BY
						apl.location_name';
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) ( $objPagination->getOffset() ) . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsDetailByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						apl.id,
						apl.location_name,
						apl.is_primary,
						ale.entity_name,
						ale.tax_number_encrypted,
						COUNT ( DISTINCT apa.id ) AS accounts,
						array_to_string( array_agg( DISTINCT apr.id ), \',\' ) AS remittance_info
					FROM
						ap_payee_locations apl
						LEFT JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						LEFT JOIN ap_legal_entities ale ON ( ap.cid = ale.cid AND apl.ap_legal_entity_id = ale.id )
						LEFT JOIN ap_payee_accounts apa ON ( ale.cid = apa.cid AND apl.id = apa.ap_payee_location_id AND apa.account_number IS NOT NULL )
						LEFT JOIN ap_remittances apr ON ( ale.cid = apr.cid AND apl.id = apr.ap_payee_location_id )
					WHERE
						ap.id = ' . ( int ) $intApPayeeId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND apl.location_name IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND ale.deleted_on IS NULL
						AND ale.deleted_by IS NULL
					GROUP BY
						apl.id,
						apl.location_name,
						apl.is_primary,
						ale.entity_name,
						ale.tax_number_encrypted,
						apl.updated_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsDetailObjectsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						apl.id,
						apl.location_name,
						apl.is_primary,
						ale.entity_name,
						ale.tax_number_encrypted,
						COUNT ( DISTINCT apa.id ) AS accounts,
						array_to_string( array_agg( DISTINCT apr.id ), \',\' ) AS remittance_info
					FROM
						ap_payee_locations apl
						LEFT JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						LEFT JOIN ap_legal_entities ale ON ( ap.cid = ale.cid AND apl.ap_legal_entity_id = ale.id )
						LEFT JOIN ap_payee_accounts apa ON ( ale.cid = apa.cid AND apl.id = apa.ap_payee_location_id AND apa.account_number IS NOT NULL )
						LEFT JOIN ap_remittances apr ON ( apl.cid = apr.cid AND apl.id = apr.ap_payee_location_id AND apr.is_default IS TRUE )
					WHERE
						ap.id = ' . ( int ) $intApPayeeId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND apl.location_name IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
					GROUP BY
						apl.id,
						apl.location_name,
						apl.is_primary,
						ale.entity_name,
						ale.tax_number_encrypted,
						apl.updated_on';

		return self::fetchApPayeeLocations( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsCountByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) return NULL;

		$strSql = 'SELECT
						COUNT ( apl.id ) AS COUNT
					FROM
						ap_payee_locations apl
						LEFT JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
					WHERE
						ap.id = ' . ( int ) $intApPayeeId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND apl.location_name IS NOT NULL
						AND apl.disabled_by IS NULL
						AND apl.disabled_on IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchApPayeeLocationDetailsByApPayeeLocationIdByApPayeeIdByCid( $intApPayeeLocationId, $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) || false == valId( $intApPayeeLocationId ) ) return NULL;

		$strSql = 'SELECT
					    apl.id,
					    apl.location_name,
					    apl.store_id,
						apl.vendor_code,
					    apl.street_line1,
					    apl.street_line2,
					    apl.street_line3,
					    apl.city,
					    apl.state_code,
					    apl.postal_code,
					    apl.country_code,
					    apl.notes,
						apl.duns_number,
					    apl.is_primary,
					    apl.disabled_by,
					    apl.disabled_on,
					    ale.id AS entity_id,
					    ale.entity_name,
					    ale.vendor_entity_id,
					    apr.id AS remittance_id,
					    apr.name AS remittance_name,
					    apr.ap_payment_type_id,
						aprt.id AS ap_routing_tag_id,
						aprt.name AS ap_routing_tag_name
					FROM
					    ap_payee_locations apl
					    LEFT JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
					    LEFT JOIN ap_legal_entities ale ON ( ap.cid = ale.cid AND apl.ap_legal_entity_id = ale.id )
					    LEFT JOIN ap_remittances apr ON ( ale.cid = apr.cid AND apl.id = apr.ap_payee_location_id )
						LEFT JOIN ap_routing_tags aprt ON ( aprt.cid = apl.cid AND aprt.id = apl.default_ap_routing_tag_id )
					WHERE
						ap.id = ' . ( int ) $intApPayeeId . '
						AND ap.cid = ' . ( int ) $intCid . '
						AND apl.id = ' . ( int ) $intApPayeeLocationId . '
						AND apr.is_default = true
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
					GROUP BY
					    apl.id,
					    apl.location_name,
					    apl.store_id,
						apl.vendor_code,
					    apl.street_line1,
					    apl.street_line2,
					    apl.street_line3,
					    apl.city,
					    apl.state_code,
					    apl.postal_code,
					    apl.country_code,
					    apl.notes,
						apl.duns_number,
					    apl.is_primary,
					    apl.disabled_by,
					    apl.disabled_on,
						aprt.id,
						aprt.name,
					    ale.id,
					    ale.entity_name,
					    ale.vendor_entity_id,
					    apr.id,
					    apr.name,
					    apr.ap_payment_type_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsByStoreIdsByCid( $arrintStoreIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valIntArr( $arrintStoreIds ) ) return NULL;

		$strSql = 'SELECT * FROM ap_payee_locations WHERE store_id IN ( ' . implode( ',', $arrintStoreIds ) . ' ) AND cid = ' . ( int ) $intCid;

		return self::fetchApPayeeLocations( $strSql, $objDatabase );
	}

	public static function fetchActiveApPayeeLocationsByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						LOWER( location_name )';

		return self::fetchApPayeeLocations( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationDetailsByIdByApPayeeIdByCid( $intApPayeeId, $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
							apl.*,
							apc.email_address
						FROM
							ap_payee_locations apl
							JOIN ap_payees ap ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
							JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid AND apc.ap_payee_id = ap.id )
						WHERE
							apl.cid = ' . ( int ) $intCid . '
							AND apl.id = ' . ( int ) $intId . '
							AND apl.ap_payee_id = ' . ( int ) $intApPayeeId;

		return self::fetchApPayeeLocation( $strSql, $objClientDatabase );

	}

	public static function fetchPrimaryApPayeeContactsByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		// @FIXME : Does query need 'AND apl.is_primary = TRUE' condition? confirm with developer

		$strSql = 'SELECT
						apl.*,
						apc.email_address
					FROM
						ap_payee_locations apl
						LEFT JOIN ap_payee_contact_locations apcl ON ( apl.cid = apcl.cid AND apl.id = apcl.ap_payee_location_id AND apcl.is_primary = TRUE )
						LEFT JOIN ap_payee_contacts apc ON ( apc.cid = apl.cid AND apc.id = apcl.ap_payee_contact_id )
					WHERE
						apc.cid = ' . ( int ) $intCid . '
						AND apcl.ap_payee_location_id IN  ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND apc.disabled_by IS NULL
						AND apc.disabled_on IS NULL';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		if( true == valArr( $arrstrData ) ) {
			return $arrstrData;
		} else {
			return NULL;
		}
	}

	public static function fetchApPayeeLocationByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id =  ' . ( int ) $intApPayeeId . '
					limit 1';

		return self::fetchApPayeeLocation( $strSql, $objClientDatabase );
	}

	public static function fetchVendorLocationsByPropertyIdsByCidForRpc( $arrintPropertyIds, $intCid, $objClientDatabase, $arrmixFilterParameters = [] ) {

		$strCondition = ( ( true == valArr( $arrintPropertyIds ) ) ? ' AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' : '' );

		$strSql = 'SELECT
						DISTINCT apl.id AS location_id,
						apl.location_name,
						apl.ap_payee_id,
						ARRAY_TO_STRING(ARRAY_AGG(DISTINCT (pga.property_id)), \',\' ) AS property_ids,
						ap.company_name,
						ap.remote_primary_key,
						ap.website_url,
						apst.name AS ap_payee_status_type,
						apt.name AS ap_payee_term,
						ale.id as ap_legal_entity_id,
						ale.entity_name as ap_legal_entity_name,
						apl.vendor_code,
						ap.secondary_number
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id )
						JOIN ap_payee_property_groups appg ON ( ap.id = appg.ap_payee_id AND ap.cid = appg.cid AND appg.ap_payee_location_id = apl.id )
						JOIN property_groups pg ON ( pg.cid = apl.cid AND appg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( appg.cid = pga.cid AND pga.property_group_id = pg.id )
				 		JOIN properties p ON ( pga.cid = p.cid AND pga.property_id = p.id AND p.is_disabled <> 1 )
						JOIN ap_payee_terms apt ON ( ap.cid = apt.cid AND ap.ap_payee_term_id = apt.id )
						JOIN ap_payee_status_types apst ON ( ap.ap_payee_status_type_id = apst.id )
						JOIN ap_legal_entities ale ON ( ale.cid = apl.cid and ale.id = apl.ap_legal_entity_id )';
		if( false != isset( $arrmixFilterParameters['lastModifiedOn'] ) ) {
			$strSql .= ' JOIN ap_payee_contacts apc ON ( apc.cid = ap.cid and apc.ap_payee_id = ap.id )
						 JOIN ap_remittances ar ON ( ar.cid = apl.cid and ar.ap_payee_location_id = apl.id AND ar.is_default IS TRUE )';
		}

		$strSql .= ' WHERE
						ap.cid = ' . ( int ) $intCid . '
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
						AND ap.ap_payee_type_id IN ( ' . implode( ',', $arrmixFilterParameters['apPayeeTypeIds'] ) . ' )';

		if( false != isset( $arrmixFilterParameters['showAllVendors'] ) && false == $arrmixFilterParameters['showAllVendors'] ) {
			$strSql .= ' AND apst.id = ' . CApPayeeStatusType::ACTIVE;
		}

		if( false != isset( $arrmixFilterParameters['vendorId'] ) ) {
			$strSql .= ' AND ap.id = ' . $arrmixFilterParameters['vendorId'];
		}
		if( false != isset( $arrmixFilterParameters['vendorName'] ) ) {
			$strSql .= ' AND LOWER( ap.company_name ) = ' . '\'' . addslashes( \Psi\CStringService::singleton()->strtolower( $arrmixFilterParameters['vendorName'] ) ) . '\'';
		}
		if( false != isset( $arrmixFilterParameters['vendorCode'] ) ) {
			$strSql .= ' AND apl.vendor_code = ' . '\'' . $arrmixFilterParameters['vendorCode'] . '\'';
		}
		if( false != isset( $arrmixFilterParameters['lastModifiedOn'] ) ) {
			$strSql .= ' AND ( 
								ap.updated_on >= ' . '\'' . $arrmixFilterParameters['lastModifiedOn'] . '\' OR
								ale.updated_on >= ' . '\'' . $arrmixFilterParameters['lastModifiedOn'] . '\' OR
								ar.updated_on >= ' . '\'' . $arrmixFilterParameters['lastModifiedOn'] . '\' OR
								apl.updated_on >= ' . '\'' . $arrmixFilterParameters['lastModifiedOn'] . '\' OR
								apc.updated_on >= ' . '\'' . $arrmixFilterParameters['lastModifiedOn'] . '\'
								 )';
		}
		$strSql .= $strCondition;
		$strSql .= 'GROUP BY apl.id,
						apl.location_name,
						apl.ap_payee_id,
						ap.company_name,
						ap.remote_primary_key,
						ap.website_url,
						apst.name,
						apt.name,
						ale.id,
						ale.entity_name,
						apl.vendor_code,
						ap.secondary_number';
		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchVendorLocationsContactsByLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {

			if( false == valId( $intCid ) || false == valIntArr( $arrintApPayeeLocationIds ) ) return NULL;

			$strSql = '( SELECT apl.id AS location_id,
							apc.name_first,
							apc.name_middle,
							apc.name_last,
							apl.street_line1,
							apl.street_line2,
							apl.street_line3,
							apl.city,
							apl.state_code,
	 						apl.postal_code,
							apc.phone_number,
							apc.fax_number,
							apc.email_address,
							\'Vendor\' AS contact_type
						FROM ap_payee_locations apl
							LEFT JOIN ap_payee_contacts apc ON ( apl.cid = apc.cid AND apl.ap_payee_id = apc.ap_payee_id )
							LEFT JOIN ap_payee_contact_locations apcl ON ( apc.cid = apcl.cid AND apl.cid = apcl.cid AND apl.id = apcl.ap_payee_location_id AND apc.id = apcl.ap_payee_contact_id )
						WHERE
							apl.id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
							AND apl.cid = ' . ( int ) $intCid . ')

						UNION
					( SELECT apl.id AS location_id,
							ar.name AS name_first,
							NULL,
							NULL,
							ar.street_line1,
							ar.street_line2,
							ar.street_line3,
							ar.city,
							ar.state_code,
							ar.postal_code,
							NULL,
							NULL,
							NULL,
							\'Vendor_Remittence\' AS contact_type
					FROM ap_payee_locations apl
						LEFT JOIN ap_remittances ar ON ( apl.cid = ar.cid AND apl.id = ar.ap_payee_location_id AND ar.is_default IS TRUE )
					WHERE
						apl.id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND ar.cid = ' . ( int ) $intCid . ' )';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationByApLegalEntityIdByCid( $intApLegalEntityId, $intCid, $objDatabase, $boolIsSoftDelete = false ) {

		$strWhere = '';

		if( true == $boolIsSoftDelete ) {
			$strWhere = ' AND deleted_by IS NULL AND deleted_on IS NULL';
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						ap_legal_entity_id =' . ( int ) $intApLegalEntityId . '
						AND cid = ' . ( int ) $intCid . $strWhere . '
						ORDER BY id DESC  LIMIT 1';

		return self::fetchApPayeeLocation( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsByApLegalEntityIdByCid( $intApLegalEntityId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						ap_legal_entity_id =' . ( int ) $intApLegalEntityId . '
						AND cid = ' . ( int ) $intCid . '
						AND disabled_by IS NULL
						AND deleted_by IS NULL';

		return self::fetchApPayeeLocations( $strSql, $objDatabase );
	}

	public static function fetchUnsyncApPayeeLocationsByApLegalEntityIdsByCid( $arrintApLegalEntityIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApLegalEntityIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						ap_legal_entity_id IN (' . implode( ',', $arrintApLegalEntityIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND store_id IS NULL
						AND disabled_by IS NULL
						AND deleted_by IS NULL';

		return self::fetchApPayeeLocations( $strSql, $objDatabase );
	}

	public static function fetchSyncApPayeeLocationsByApLegalEntityIdsByCid( $arrintApLegalEntityIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApLegalEntityIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						ap_legal_entity_id IN (' . implode( ',', $arrintApLegalEntityIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND store_id IS NOT NULL
						AND disabled_by IS NULL
						AND deleted_by IS NULL';

		return self::fetchApPayeeLocations( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsByApLegalEntityIdsByCid( $arrintApLegalEntityIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApLegalEntityIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						ap_legal_entity_id IN (' . implode( ',', $arrintApLegalEntityIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND disabled_by IS NULL
						AND deleted_by IS NULL';

		return self::fetchApPayeeLocations( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApPayeeLocationsDetailsByCid( $intCid, $objPagination, $objDatabase ) {

		$strPaginationSql = '';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strPaginationSql = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		$strSql = 'SELECT
						ale.id AS ap_legal_entity_id,
						ale.ap_payee_id,
						ale.entity_name,
						app.company_name,
						apl.location_name,
						apl.street_line1,
						apl.street_line2,
						apl.street_line3,
						apl.country_code,
						apl.city,
						apl.state_code,
						apl.postal_code
					FROM
						ap_payee_locations apl
						JOIN ap_legal_entities ale ON ( ale.cid = apl.cid AND ale.id = apl.ap_legal_entity_id )
						JOIN ap_payees app ON ( ale.cid = app.cid AND ale.ap_payee_id = app.id )
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND ale.vendor_entity_id IS NOT NULL
						AND ale.tax_number_encrypted IS NOT NULL
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND app.ap_payee_status_type_id IN ( ' . CApPayeeStatusType::ACTIVE . ', ' . CApPayeeStatusType::LOCKED . ' )
						AND apl.store_id IS NULL
						AND apl.deleted_by IS NULL
                        AND apl.deleted_on IS NULL
                        AND ale.deleted_by IS NULL
                        AND ale.deleted_on IS NULL
					GROUP BY
						ale.id,
						app.id,
						ale.ap_payee_id,
						ale.entity_name,
						app.company_name,
						apl.location_name,
						apl.street_line1,
						apl.street_line2,
						apl.street_line3,
						apl.country_code,
						apl.city,
						apl.state_code,
						apl.postal_code
					ORDER BY 
						app.company_name,
						ale.id'
						. $strPaginationSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsCountByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count( apl.id )
					FROM
						ap_payee_locations apl
						JOIN ap_legal_entities ale ON ( ale.cid = apl.cid AND ale.id = apl.ap_legal_entity_id )
						JOIN ap_payees app ON ( ale.cid = app.cid AND ale.ap_payee_id = app.id )
					WHERE
						ale.cid = ' . ( int ) $intCid . '
						AND ale.vendor_entity_id IS NOT NULL
						AND ale.tax_number_encrypted IS NOT NULL
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND app.ap_payee_status_type_id IN ( ' . CApPayeeStatusType::ACTIVE . ', ' . CApPayeeStatusType::LOCKED . ' )
						AND apl.store_id IS NULL
						AND apl.deleted_by IS NULL
						AND apl.deleted_on IS NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		$arrintCount = fetchData( $strSql, $objDatabase );
		return $arrintCount[0]['count'];
	}

	public static function fetchApPayeeLocationsDetailsByIdsByCid( $arrintApPayeeLocationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) ) return NULL;

		$strSql = 'SELECT
						apl.id,
						ale.tax_number_encrypted,
						apl.location_name,
						ap.company_name
					FROM
						ap_payee_locations apl
						JOIN ap_legal_entities ale ON ( apl.cid = ale.cid AND apl.ap_legal_entity_id = ale.id )
						JOIN ap_payees ap ON ( ap.cid = ale.cid AND apl.ap_payee_id = ap.id )
					WHERE
						apl.id IN ( ' . sqlIntImplode( $arrintApPayeeLocationIds ) . ')
						AND apl.cid = ' . ( int ) $intCid . ' 
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApPayeeLocationsByIdsApPayeeIdsByCid( $arrintApPayeeLocationIds, $arrintApPayeeIds, $intCid, $intCompanyUserId, $boolIsAdministrator, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeIds ) || false == valIntArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apl.*,
						ap.company_name AS ap_payee_name,
						apa.id AS ap_payee_account_id,
						apa.account_number AS ap_payee_account_number
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						JOIN ap_payee_accounts AS apa ON ( ap.cid = apa.cid AND ap.id = apa.ap_payee_id AND apl.id = apa.ap_payee_location_id )
						JOIN ap_headers ah ON ( ap.id = ah.ap_payee_id AND ap.cid = ah.cid AND ah.ap_payee_account_id = apa.id AND ah.ap_payee_location_id = apl.id )
						
						JOIN ap_details ad ON ( ah.id = ad.ap_header_id AND ad.cid = ah.cid )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND apl.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND ah.ap_payment_id IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND ah.is_posted = true
						AND ah.is_on_hold = false
						AND ad.id NOT IN (
											SELECT
												DISTINCT ad.id
											FROM
												ap_details ad
												LEFT JOIN ( ' . \Psi\Eos\Entrata\CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS vcup ON ( vcup.cid = ad.cid AND vcup.id = ad.property_id AND vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' )
											WHERE
												ad.cid = ' . ( int ) $intCid . '
												AND ( ( vcup.id IS NULL )
														OR
														( vcup.is_disabled = 1 AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL ) )
										)
						AND NOT ah.is_template
						AND ad.transaction_amount_due < 0
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationByIdByApLegalEntityIdByApPayeeIdByCid( $intId, $intApLegalEntityId, $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						id = ' . ( int ) $intId . '
						AND ap_legal_entity_id = ' . ( int ) $intApLegalEntityId . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSyncedApPayeeLocationsByApLegalEntityIdByApPayeeIdByCid( $intApLegalEntityId, $intApPayeeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id,
						store_id
						FROM
							ap_payee_locations
						WHERE
							store_id IS NOT NULL
							AND ap_legal_entity_id = ' . ( int ) $intApLegalEntityId . '
							AND ap_payee_id = ' . ( int ) $intApPayeeId . '
							AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSyncedContactApPayeeLocationsByApLegalEntityIdByApPayeeIdByCid( $intApLegalEntityId, $intApPayeeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							apl.*
						FROM
							ap_payee_locations apl
							LEFT JOIN ap_payee_contact_locations apcl ON ( apcl.ap_payee_location_id = apl.id AND apl.cid = apcl.cid )
							LEFT JOIN ap_payee_contacts apc ON ( apc.id = apcl.ap_payee_contact_id AND apc.cid = apcl.cid )
						WHERE
							apc.worker_id IS NOT NULL
							AND apl.ap_legal_entity_id = ' . ( int ) $intApLegalEntityId . '
							AND apl.ap_payee_id = ' . ( int ) $intApPayeeId . '
							AND apl.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSyncedApPayeeLocationsByStoreIdsByCids( $arrintStoreIds, $arrintCids, $objClientDatabase ) {

		if( false == ( $arrintStoreIds = getIntValuesFromArr( $arrintStoreIds ) ) || false == ( $arrintCids = getIntValuesFromArr( $arrintCids ) ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND store_id IN ( ' . implode( ',', $arrintStoreIds ) . ' )
						AND store_id IS NOT NULL
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationsByStoreIdByCIds( $intStoreId, $arrintCIds, $objDatabase ) {

		if( false == valArr( $arrintCIds ) || false == valId( $intStoreId ) ) return NULL;

		$strSql = 'SELECT * FROM ap_payee_locations WHERE store_id = ' . ( int ) $intStoreId . ' AND cid IN ( ' . sqlIntImplode( $arrintCIds ) . ' ) ';

		return self::fetchApPayeeLocations( $strSql, $objDatabase, false );
	}

	public static function fetchApPayeeLocationsByApLegalEntityIdsByCids( $arrintApLegalEntityId, $arrintCId, $objDatabase ) {

		if( false == ( $arrintCId = getIntValuesFromArr( $arrintCId ) ) || false == ( $arrintApLegalEntityId = getIntValuesFromArr( $arrintApLegalEntityId ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id,
						cid,
						ap_legal_entity_id
					FROM
						ap_payee_locations
					WHERE
						ap_legal_entity_id IN ( ' . sqlIntImplode( $arrintApLegalEntityId ) . ' )
						AND cid IN (' . sqlIntImplode( $arrintCId ) . ' )
						AND disabled_by IS NULL
						AND deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsCountsByCids( $arrintCIds, $objDatabase ) {

		if( false == valArr( $arrintCIds ) ) return NULL;

		$strSql = 'SELECT 
						COUNT(apl.id) as total,
						COUNT( CASE WHEN apl.store_id IS NOT NULL THEN apl.id END ) as synced,
						apl.cid
					FROM
						ap_payee_locations apl
						JOIN ap_payees app ON( apl.cid = app.cid AND apl.ap_payee_id = app.id )
					WHERE
						apl.cid IN (' . sqlIntImplode( $arrintCIds ) . ' )
						AND app.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND app.ap_payee_type_id IN ( ' . CApPayeeType::STANDARD . ', ' . CApPayeeType::INTERCOMPANY . ' )
						AND apl.location_name IS NOT NULL
					GROUP BY 
						apl.cid	';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSyncedApPayeeLocationByIdByApPayeeIdByCid( $intId, $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						apl.*
					FROM
						ap_payee_locations apl
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.id = ' . ( int ) $intId . '
						AND apl.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND apl.store_id IS NOT NULL';

		return self::fetchApPayeeLocation( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationsWithPropertyByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeIds ) ) return NULL;
		$strSql = 'SELECT 
						DISTINCT ON (apl.id)
						apl.*,
						STRING_AGG(pga.property_id::text, \',\') OVER(partition by apl.id) as location_property_ids,
						CASE
							WHEN pg.system_code = \'ALL\' THEN 1
							ELSE 0
						END as is_all_properties
					FROM 
						ap_payee_locations apl
						JOIN ap_payee_property_groups appg ON (apl.cid = appg.cid AND appg.ap_payee_location_id = apl.id )
						JOIN property_group_associations pga ON ( pga.cid = appg.cid AND pga.property_group_id = appg.property_group_id )
						JOIN property_groups pg ON ( pg.cid = appg.cid AND pg.id = appg.property_group_id)
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )
						AND apl.disabled_on IS NULL
						AND apl.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomApPayeeLocationsByIdsByApPayeeIdsByCids( $arrintApPayeeLocationIds, $arrintApPayeeIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) || false == valArr( $arrintApPayeeIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						apl.id,
						apl.cid,
						apl.ap_legal_entity_id,
						apl.ap_payee_id,
						ap.ap_payee_status_type_id,
						apl.default_ap_routing_tag_id
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( ap.id = apl.ap_payee_id AND ap.cid = apl.cid )
					WHERE
						apl.id in ( ' . sqlIntImplode( array_filter( $arrintApPayeeLocationIds ) ) . ' ) 
						AND apl.cid in ( ' . sqlIntImplode( array_filter( $arrintCids ) ) . ' ) 
						AND apl.ap_payee_id in ( ' . sqlIntImplode( array_filter( $arrintApPayeeIds ) ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsByApPayeeIdByIdByCid( $intApPayeeId, $intLocationId, $intCid, $objDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valId( $intLocationId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT apl.id,
						apl.location_name,
						apa.id AS account_id,
						apa.account_number,
						apa.default_ap_remittance_id
					FROM
						ap_payee_locations apl
						JOIN ap_payee_accounts apa ON ( apa.cid = apl.cid AND apa.ap_payee_id = apl.ap_payee_id AND apa.ap_payee_location_id = apl.id)
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						AND apl.ap_payee_id = ' . ( int ) $intApPayeeId . '
					GROUP BY
						apl.id,
						apl.cid,
						apa.id,
						apa.account_number,
						apa.default_ap_remittance_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeLocationsByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strSql = 'SELECT
						ah.id AS ap_header_id,
						apl.*,
						ap.company_name,
						ale.receives_1099
					FROM
						ap_payee_locations apl
						JOIN ap_headers ah ON ( ah.cid = apl.cid AND ah.ap_payee_location_id = apl.id )
						JOIN ap_payees ap ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						JOIN ap_legal_entities ale ON ( apl.cid = ale.cid AND apl.ap_legal_entity_id = ale.id )
					WHERE
						ah.id IN ( ' . sqlIntImplode( $arrintApHeaderIds ) . ')
						AND apl.cid = ' . ( int ) $intCid . '
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL';

		return self::fetchApPayeeLocations( $strSql, $objDatabase, false );
	}

	public static function updateApPayeeLocationsByIdsByApPayeeIdByCId( $arrintIds, $intApPayeeId, $intCId, $intCompanyUserId ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) {
			return NULL;
		}

		$strSql = 'UPDATE
						ap_payee_locations
					SET 
						store_id = NULL,
						updated_on = NOW(),
						updated_by = ' . ( int ) $intCompanyUserId . '
					WHERE
						id IN ( ' . sqlIntImplode( $arrintIds ) . ' ) 
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCId . ';';

		return $strSql;
	}

	public static function fetchNotSyncedApPayeeLocationsCountByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT ( id )
					FROM
						ap_payee_locations
					WHERE
						ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND cid = ' . ( int ) $intCid . '
						AND store_id IS NULL
						AND disabled_by IS NULL
						AND disabled_on IS NULL
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchSyncedApPayeeLocationByStoreIdByCid( $intStoreId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id
					FROM
						ap_payee_locations
					WHERE
						store_id = ' . ( int ) $intStoreId . '
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApPayeeIdsByVendorCodesByCid( $arrstrVendorCodes, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrVendorCodes ) ) {
			return NULL;
		}

		$strSql = '	SELECT 
						ap_payee_id 
					FROM 
						ap_payee_locations 
					WHERE 
						cid = ' . ( int ) $intCid . ' 
						AND lower( vendor_code ) IN ( ' . strtolower( sqlStrImplode( $arrstrVendorCodes ) ) . ')
						AND deleted_on IS NULL
						AND deleted_by IS NULL
					GROUP BY ap_payee_id;';

		$arrmixData = fetchData( $strSql, $objDatabase );
		$arrintApPayeeIds = [];
		if( false != valArr( $arrmixData ) ) {
			foreach( $arrmixData as $arrintTempApPayeeIds ) {
				$arrintApPayeeIds[$arrintTempApPayeeIds['ap_payee_id']] = $arrintTempApPayeeIds['ap_payee_id'];
			}
		}

		return $arrintApPayeeIds;
	}

	public static function fetchActiveApPayeeLocationsMixDataByCid( $intCid, $objClientDatabase ) {

		$strSql = '	SELECT
						ap.id AS ap_payee_id,
						ap.company_name AS vendor_name,
						apl.location_name,
						apl.vendor_code as vendor_code,
						ar.name as remittance_name,
						ar.id as remittance_id,
						ar.is_default,
						ar.ap_payment_type_id as remittance_type,
						apl.id as location_id
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( ap.cid = apl.cid AND apl.ap_payee_id = ap.id )
						JOIN ap_remittances ar ON ( ar.cid = apl.cid AND ar.ap_payee_location_id = apl.id )
					WHERE apl.cid = ' . ( int ) $intCid . ' AND
						ar.is_published is true';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApPayeeLocationsByPropertyGroupIdsByApHeaderTypeIdByCid( $arrintPropertyGroupIds, $intApHeaderTypeId, $intCid, $objClientDatabase, $strApPayeeName = NULL, $arrintSelectedRecords = [], $boolIsFromInvoiceFilter = false ) {
		if( false == valArr( $arrintPropertyGroupIds ) || false == valId( $intCid ) ) return NULL;

		if( CApHeaderType::PURCHASE_ORDER == $intApHeaderTypeId ) {
			$arrintApPayeeTypeIds = [ CApPayeeType::STANDARD, CApPayeeType::INTERCOMPANY, CApPayeeType::LENDER, CApPayeeType::OWNER ];
		} else {
			// CApHeaderType::INVOICE
			$arrintApPayeeTypeIds = [ CApPayeeType::STANDARD, CApPayeeType::OWNER, CApPayeeType::INTERCOMPANY, CApPayeeType::PROPERTY_SOLUTIONS, CApPayeeType::RESIDENT, CApPayeeType::LENDER ];
		}

		$strSqlApPayeeName = $strSqlLimit = $strApDetailsJoin = $strApHeadersCondition = $strApPaymentsCondition = NULL;

		if( true == valStr( $strApPayeeName ) ) {
			$strSqlLimit		= ' LIMIT 50 ';

			$strSqlApPayeeName	= ' CASE
										WHEN ah.lease_customer_id IS NOT NULL
										THEN lower( ah.header_memo ) ILIKE \'%' . strtolower( $strApPayeeName ) . '%\'
										ELSE (
												lower( ap.company_name ) ILIKE \'%' . strtolower( $strApPayeeName ) . '%\'
												OR
												lower( apl.vendor_code ) ILIKE \'%' . strtolower( $strApPayeeName ) . '%\' 
											)
									END ';

			if( true == valIntArr( $arrintSelectedRecords ) ) {
				$strSqlApPayeeName = ' AND ( apl.id NOT IN ( ' . sqlIntImplode( $arrintSelectedRecords ) . ' ) OR ah.lease_customer_id NOT IN ( ' . sqlIntImplode( $arrintSelectedRecords ) . ' ) OR ' . $strSqlApPayeeName . ' ) ';
			} else {
				$strSqlApPayeeName = ' AND ( ' . $strSqlApPayeeName . ' ) ';
			}
		}

		if( false == valStr( $strApPayeeName ) && true == valIntArr( $arrintSelectedRecords ) ) {
			$strSqlLimit		= ' LIMIT 50 ';
			$strSqlApPayeeName	= ' AND ( apl.id IN ( ' . sqlIntImplode( $arrintSelectedRecords ) . ' ) OR ah.lease_customer_id IN ( ' . sqlIntImplode( $arrintSelectedRecords ) . ' ) )';
		}

		if( true == $boolIsFromInvoiceFilter ) {
			$strApDetailsJoin		= 'JOIN ap_details ad ON ( ad.cid = pga.cid AND ad.property_id = pga.property_id AND ad.deleted_on IS NULL )';
			$strApHeadersCondition	= 'AND ad.ap_header_id = ah.id';
			$strApPaymentsCondition	= 'AND ah.ap_payment_id IS NULL';
		}

		$strSql = 'SELECT
						DISTINCT
						CASE
							WHEN ah.lease_customer_id IS NOT NULL THEN ah.header_memo
							ELSE ap.company_name
						END AS vendor_name,
						CASE
							WHEN ah.lease_customer_id IS NULL THEN COALESCE( apl.vendor_code, apl.location_name )
							ELSE NULL
						END AS vendor_code,
						apl.id,
						CASE
							WHEN ah.lease_customer_id IS NOT NULL THEN ah.lease_customer_id
							ELSE NULL
						END AS lease_customer_id,
						COUNT ( apl.id ) OVER ( PARTITION BY apl.ap_payee_id ) AS locations_count
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id )
						JOIN ap_headers ah ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id AND ah.ap_payee_location_id = apl.id AND ah.ap_header_type_id IN ( ' . CApHeaderType::PURCHASE_ORDER . ', ' . CApHeaderType::INVOICE . ' ) AND ah.deleted_on IS NULL )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						' . $strSqlApPayeeName . '
						AND EXISTS (
									SELECT
										NULL
									FROM
										property_groups pg
										JOIN property_group_associations pga ON ( pga.cid = pg.cid AND pga.property_group_id = pg.id )
										' . $strApDetailsJoin . '
									WHERE
										pg.cid = ' . ( int ) $intCid . '
										AND pg.id IN ( ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' )
										AND pg.deleted_on IS NULL
										' . $strApHeadersCondition . '
						)      
						AND apl.deleted_on IS NULL
						AND apl.disabled_on IS NULL
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND ap.ap_payee_type_id IN ( ' . sqlIntImplode( $arrintApPayeeTypeIds ) . ' )
						AND ap.company_name IS NOT NULL
						' . $strApPaymentsCondition . '
					ORDER BY
						1
					' . $strSqlLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchEnabledApPayeeLocationsByIdsByCid( $arrintApPayeeLocationIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_payee_locations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND disabled_on IS NULL
						AND deleted_by IS NULL
						AND location_name IS NOT NULL
					ORDER BY
						LOWER( location_name ) ASC';

		return self::fetchApPayeeLocations( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApPayeeIdsOrCodesBySearchKeywordByCid( $strApPayeeIdOrCode, $intCid, $objClientDatabase ) {

		$strSqlApPayeeIdOrCode	= '';
		$strSqlLimit			= '';

		if( true == valStr( $strApPayeeIdOrCode ) ) {
			$strSqlLimit		= ' LIMIT 15 ';

			$strSqlApPayeeIdOrCode	= 'AND ( lower( apl.vendor_code ) ILIKE \'%' . strtolower( $strApPayeeIdOrCode ) . '%\'
									OR
									apl.ap_payee_id::varchar ILIKE \'%' . $strApPayeeIdOrCode . '%\' ) ';
		}

		$strSql = 'SELECT 
						ap.id,
						CASE
							WHEN apl.vendor_code IS NULL THEN ap.id::varchar
							ELSE apl.vendor_code
						END AS ids_or_codes
					FROM 
						ap_payees ap
						JOIN ap_payee_locations apl on ( ap.cid = apl.cid AND ap.id = apl.ap_payee_id AND apl.deleted_by IS NULL )
					WHERE
						apl.cid = ' . ( int ) $intCid . ' AND
						ap_payee_type_id NOT IN ( ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::CORPORATE . ' )
						' . $strSqlApPayeeIdOrCode . '
					ORDER BY ids_or_codes'
						. $strSqlLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeLocationsBySearchKeywordByCid( $strApPayeeLocation, $intCid, $objClientDatabase ) {

		$strSqlApPayeeLocation	= '';
		$strSqlLimit			= '';

		if( true == valStr( $strApPayeeLocation ) ) {
			$strSqlLimit		= ' LIMIT 15 ';

			$strSqlApPayeeLocation	= 'AND lower( apl.location_name ) ILIKE \'%' . strtolower( $strApPayeeLocation ) . '%\'';
		}

		$strSql = 'SELECT 
					DISTINCT apl.location_name
					FROM
						ap_payee_locations apl
						JOIN ap_payees ap ON ( apl.cid = ap.cid AND apl.ap_payee_id = ap.id AND apl.deleted_by IS NULL )
					WHERE
						apl.cid = ' . ( int ) $intCid . '
						' . $strSqlApPayeeLocation . '
						AND ap.ap_payee_type_id NOT IN ( ' . CApPayeeType::EMPLOYEE . ', ' . CApPayeeType::CORPORATE . ' )
						' . $strSqlLimit . ' ';

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchApPayeeLocationNamesByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		$arrstrWhere			= [];
		if( true == valIntArr( $arrintApPayeeIds ) ) {
			$arrstrWhere[] = ' AND apl.ap_payee_id IN ( ' . sqlIntImplode( $arrintApPayeeIds ) . ' ) ';
		}

		$strSql = 'SELECT
						apl.location_name,
						apl.id,
						apl.ap_payee_id
					FROM
						ap_payee_locations apl
					WHERE
						cid = ' . ( int ) $intCid . '
						AND disabled_on IS NULL
						AND deleted_by IS NULL
						AND location_name IS NOT NULL
						' . implode( PHP_EOL . ' ', $arrstrWhere ) . ' ';
		return fetchData( $strSql, $objClientDatabase );
	}

}
?>