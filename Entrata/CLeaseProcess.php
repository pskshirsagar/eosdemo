<?php
use Psi\Eos\Entrata\CUnitSpaces;

class CLeaseProcess extends CBaseLeaseProcess {

	protected $m_strCustomerNameFirst;
	protected $m_strCustomerNameLast;
	protected $m_intLeaseStatusTypeId;
	protected $m_intLeaseCustomerId;

	public function getCustomerNameFirst() {
        return $this->m_strCustomerNameFirst;
    }

	public function getCustomerNameLast() {
        return $this->m_strCustomerNameLast;
    }

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['customer_name_first'] ) ) $this->setCustomerNameFirst( $arrmixValues['customer_name_first'] );
    	if( true == isset( $arrmixValues['customer_name_last'] ) ) $this->setCustomerNameLast( $arrmixValues['customer_name_last'] );
	    if( true == isset( $arrmixValues['lease_status_type_id'] ) ) $this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
	    if( true == isset( $arrmixValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
    }

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

    public function setCustomerNameFirst( $strCustomerNameFirst ) {
        $this->m_strCustomerNameFirst = $strCustomerNameFirst;
    }

	public function setCustomerNameLast( $strCustomerNameLast ) {
        $this->m_strCustomerNameLast = $strCustomerNameLast;
    }

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function addToDetails( $arrmixData ) {

		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $strKey => $strValue ) {
				$this->setDetailsField( $strKey, $strValue );
			}
		}
	}

	public function getDetail( $strKey ) {
		return $this->getDetailsField( $strKey );
	}

	public function removeDetail( $strKey ) {
		$arrmixDetails = ( array ) $this->getDetails();
		if( true == isset( $arrmixDetails[$strKey] ) ) {
			unset( $arrmixDetails[$strKey] );
			if( true == valArr( $arrmixDetails ) ) {
				$arrmixDetails = ( object ) $arrmixDetails;
			} else {
				$arrmixDetails = '';
			}
			$this->setDetails( $arrmixDetails );
		}
	}

    public function valId() {
        $boolIsValid = true;

		if( true == is_null( $this->getId() ) || 0 >= ( int ) $this->getId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Id is required.' ) ) );
		}

		return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
		}

		return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;

    	if( true == is_null( $this->getLeaseId() ) || 0 >= ( int ) $this->getLeaseId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'Lease id is required.' ) ) );
		}

		return $boolIsValid;
    }

    public function valTransferLeaseId() {
    	$boolIsValid = true;

    	if( false == valId( $this->getTransferLeaseId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transfer_lease_id', __( 'Transfer Lease id is required.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valForwardingCustomerAddressId() {
        $boolIsValid = true;
		return $boolIsValid;
    }

    public function valMoveOutReasonListItemId( $boolIsRequired = false, $arrmixDataToValidate = NULL ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && ( true == is_null( $this->getMoveOutReasonListItemId() ) || 0 >= ( int ) $this->getMoveOutReasonListItemId() ) ) {
			$boolIsValid = false;
			if( true == getArrayElementByKey( 'is_application_or_non_past_lease_fmo', $arrmixDataToValidate ) ) {
				$strValidationFor = __( 'cancellation' );
			} elseif( true == in_array( getArrayElementByKey( 'list_type_id', $arrmixDataToValidate ), [ CListType::EVICTION, CListType::SKIP ] ) ) {
				$strValidationFor = \Psi\CStringService::singleton()->strtolower( CListType::createService()->getListTypeByListTypeId( getArrayElementByKey( 'list_type_id', $arrmixDataToValidate ) ) );
			} else {
				$strValidationFor = __( 'move-out' );
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_reason_list_item_id', __( 'Please select {%s, 0} reason.', [ $strValidationFor ] ) ) );
		}

		return $boolIsValid;
    }

    public function valCustomerPaymentAccountId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApPaymentId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPaymentTypeId( $boolIsRequired = false ) {
        $boolIsValid = true;

		if( true == $boolIsRequired && ( true == is_null( $this->getRefundPaymentTypeId() ) || 0 >= ( int ) $this->getRefundPaymentTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_type_id', __( 'Payment type is required.' ) ) );
		}

		return $boolIsValid;
    }

    public function valNoticeDate( $boolIsRequired = false ) {
        $boolIsValid = true;

		if( true == $boolIsRequired && false == valStr( $this->getNoticeDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notice_date', __( 'Notice date is required.' ) ) );
		}

		if( true == valStr( $this->getNoticeDate() ) && 1 !== CValidation::checkISODate( $this->getNoticeDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notice_date', __( 'Notice date is not valid.' ) ) );
		}

		return $boolIsValid;
    }

    public function valCompareNoticeDate( $arrmixDataToValidate ) {
    	$boolIsValid = true;

		if( true == in_array( getArrayElementByKey( 'list_type_id', $arrmixDataToValidate ), [ CListType::MOVE_OUT, CListType::SKIP ] ) ) {

			if( true == $arrmixDataToValidate['is_compare_notice_or_move_out_date'] && strtotime( $this->getNoticeDate() ) < strtotime( $this->getMoveInDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notice_date', __( 'Notice date must be greater than or equal to move in date.' ) ) );

			}

			if( true == $arrmixDataToValidate['is_bulk_notice'] || true == $arrmixDataToValidate['is_from_bulk_move_out'] ) {
				if( strtotime( $this->getNoticeDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notice_date', __( 'Notice date cannot be future date.' ) ) );
				}
			} else {
				if( strtotime( $this->getNoticeDate() ) > strtotime( $arrmixDataToValidate['property_time_zone_date'] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notice_date', __( 'Notice date cannot be future date.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

    public function valMoveInDate( $boolIsRequired = false, $intApplicationId = NULL ) {
    	$boolIsValid = true;

    	if( true == valId( $intApplicationId ) ) {
		    $objSubsidyCertification = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchSubsidyCertificationByApplicantionIdByCid( $intApplicationId, $this->getCid(), $this->getDatabase() );
		    $objCachedLease = \Psi\Eos\Entrata\CCachedLeases::createService()->fetchCachedLeaseByIdByCid( $objSubsidyCertification->getLeaseId(), $objSubsidyCertification->getCid(), $this->getDatabase() );
	    }

    	if( true == $boolIsRequired && false == valStr( $this->getMoveInDate() ) ) {
    		$boolIsValid = false;
		    if( true == valObj( $objSubsidyCertification, 'CSubsidyCertification' ) && CSubsidyCertificationType::ANNUAL_RECERTIFICATION == $objSubsidyCertification->getSubsidyCertificationTypeId() ) {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Member move-in date is required.' ) ) );
		    } else {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date is required.' ) ) );
		    }
    	}

	    if( true == valStr( $this->getNoticeDate() ) && 1 !== CValidation::checkISODate( $this->getNoticeDate() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date is not valid' ) ) );
	    }

	    if( true == valStr( $this->getMoveInDate() ) && true == valObj( $objSubsidyCertification, 'CSubsidyCertification' ) && CSubsidyCertificationType::ANNUAL_RECERTIFICATION == $objSubsidyCertification->getSubsidyCertificationTypeId() && strtotime( $this->getMoveInDate() ) > strtotime( $objSubsidyCertification->getEffectiveThroughDate() ) ) {

	    	$boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Member Move-in date should not be greater than certification effective through date.' ) ) );
	    }

	    if( true == valStr( $this->getMoveInDate() ) && true == valObj( $objSubsidyCertification, 'CSubsidyCertification' ) && CSubsidyCertificationType::ANNUAL_RECERTIFICATION == $objSubsidyCertification->getSubsidyCertificationTypeId() && strtotime( $this->getMoveInDate() ) < strtotime( $objCachedLease->getMoveInDate() ) ) {

		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Member Move-in date should not be less than lease move-in date.' ) ) );
	    }

    	return $boolIsValid;
    }

	public function valMoveOutDate( $boolIsRequired = false, $arrmixDataToValidate = NULL, $objDatabase = NULL, $boolIsValidateMoveOutDate = false ) {
        $boolIsValid = true;

		if( CListType::EVICTION == getArrayElementByKey( 'list_type_id', $arrmixDataToValidate ) ) {
			$strValidationFor = __( 'Estimated eviction' );
		} else {
			$strValidationFor = __( 'Move-out' );
		}

        if( true == $boolIsRequired && false == valStr( $this->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( '{%s, 0} date is required.', [ $strValidationFor ] ) ) );
		}

		if( true == valStr( $this->getMoveOutDate() ) && 1 !== CValidation::checkISODate( $this->getMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( '{%s, 0} date is not valid.',  [ $strValidationFor ] ) ) );
		} else {
			$strMinimumDate = getArrayElementByKey( 'minimum_date_to_compare', $arrmixDataToValidate );
			if( true == valStr( $strMinimumDate ) && strtotime( $strMinimumDate ) > strtotime( $this->getMoveOutDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move Out date must be greater than or equal to lease start date or move in date which ever is later.' ) ) );
			}
		}
	    if( true == $boolIsValidateMoveOutDate ) {

		    $objSubsidyCertification = \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchLatestSubsidyCertificationBySubsidyCertificationStatusTypeByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase, CSubsidyCertificationStatusType::FINALIZED, false );
		    if( true == Valobj( $objSubsidyCertification, CSubsidyCertification::class ) && true == ValId( $this->getMoveOutReasonListItemId() ) && date( 'Y-m-d', strtotime( $objSubsidyCertification->getEffectiveDate() ) ) == date( 'Y-m-d', strtotime( $this->getMoveOutDate() ) ) ) {
			    if( true == in_array( $objSubsidyCertification->getSubsidyCertificationTypeId(), CSubsidyCertificationType::$c_arrintSameDateMoveOutRestrictedSubsidyCertificationTypeIds ) ) {
				    $boolIsValid = false;

				    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move Out cannot be done on the same day of move in.Please select future move out date' ) ) );

			    } elseif( true == in_array( $objSubsidyCertification->getSubsidyCertificationTypeId(), CSubsidyCertificationType::$c_arrintSameDateMoveOutAllowedSubsidyCertificationTypeIds ) && $objSubsidyCertification->getSubsidyCertificationStatusTypeId() == CSubsidyCertificationStatusType::TRANSMITTED ) {
				    $boolIsValid = false;

				    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move Out cannot be done as there is a certification on this lease which has been already submitted to tracs on the same date.Please select future move out date' ) ) );
			    }

		    }

	    }

	    return $boolIsValid;
    }

    public function valCompareMoveOutDate( $arrmixDataToValidate ) {
    	$boolIsValid = true;

    	if( true == $arrmixDataToValidate['is_compare_notice_or_move_out_date'] && strtotime( $this->getMoveOutDate() ) < strtotime( $arrmixDataToValidate['lease_start_date'] ) ) {
    		$boolIsValid = false;
		    if( CListType::EVICTION == $arrmixDataToValidate['list_type_id'] ) {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Estimated eviction date must be greater than Lease Start date.' ) ) );
		    } else {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move-out date must be greater than Lease Start date.' ) ) );
		    }
    	}

    	return $boolIsValid;
    }

	public function valRefundDate( $arrmixDataToValidate = NULL ) {
        $boolIsValid = true;

        if( false == valStr( $this->getRefundDate() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'refund_date', __( 'Refund date is required.' ) ) );
        	$boolIsValid = false;
        } else {
        	$mixIssueRefundOnDate = CValidation::checkISODateFormat( $this->getRefundDate(), $boolFormat = true );
        	if( false === $mixIssueRefundOnDate ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'refund_date', __( 'Refund date is not valid.' ) ) );
        		$boolIsValid = false;
        	} elseif( true == getArrayElementByKey( 'compare_refund_and_move_out_date', $arrmixDataToValidate ) && strtotime( $this->getRefundDate() ) < strtotime( $this->getMoveOutDate() ) ) {
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'refund_date', __( 'Refund date must be greater than or equal to move-out date.' ) ) );
        		$boolIsValid = false;
        	}
        }

        return $boolIsValid;
    }

    public function valTerminationStartDate( $boolIsRequired = false, $arrmixDataToValidate = NULL ) {

		$boolIsValid = true;

		if( CListType::EVICTION == getArrayElementByKey( 'list_type_id', $arrmixDataToValidate ) ) {

			if( true == $boolIsRequired && false == valStr( $this->getTerminationStartDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_start_date', __( 'Eviction start date is required.' ) ) );
			}

			if( true == valStr( $this->getTerminationStartDate() ) && 1 !== CValidation::checkISODate( $this->getTerminationStartDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_start_date', __( 'Eviction start date is not valid' ) ) );
			}

			if( true == valStr( $this->getMoveInDate() ) && strtotime( $this->getMoveInDate() ) > strtotime( $this->getTerminationStartDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_start_date', __( 'Eviction start date must be greater than Move-in date.' ) ) );
			}
 		}

		return $boolIsValid;
    }

 	public function valCollectionsStartDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTransferUnitSpaceId( $objDatabase ) {
        $boolIsValid = true;

        if( false == valId( $this->getTransferUnitSpaceId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transfer_unit_space_id', __( 'You must choose a unit to transfer this lease to.' ) ) );
        }

        // adding extra validation to make sure transferunitspaceid is different
        $objLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );
        if( true == valObj( $objLease, 'CLease' ) && $this->getTransferUnitSpaceId() == $objLease->getUnitSpaceId() ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_id', __( 'You cannot transfer to the same unit.' ) ) );
        }
        return $boolIsValid;
    }

    public function valIsTransfer( $objDatabase ) {

    	$boolIsValid = true;

	    $objTransferLease = $this->fetchTransferLease( $objDatabase );

	    if( false == valObj( $objTransferLease, 'CLease' ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transfer_lease_id', __( 'The new transferring lease could not be loaded.' ) ) );
	    }

	    $arrintApplicationStageStatusIds = [
	        CApplicationStage::APPLICATION  	=> [ CApplicationStatus::STARTED ]
	    ];

	    if( true == valObj( $objTransferLease, 'CLease' ) && CLeaseStatusType::APPLICANT == $objTransferLease->getLeaseStatusTypeId() ) {
		    $objApplication = CApplications::fetchApplicationByLeaseIdByLeaseIntervalTypeIdByApplicationStageStatusIdsByCid( $objTransferLease->getId(), CLeaseIntervalType::TRANSFER, $arrintApplicationStageStatusIds, $this->getCid(), $objDatabase );
	    }

	    if( false == valObj( $objApplication, 'CApplication' ) ) {

		    if( false == is_numeric( $this->getTransferUnitSpaceId() ) || false == is_numeric( $this->getTransferLeaseId() ) ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transfer_unit_space_id', __( 'You must choose a unit to transfer this lease to.' ) ) );
		    }

		    $objTransferUnitSpace = $this->fetchTransferUnitSpace( $objDatabase );

		    if( false == valObj( $objTransferUnitSpace, 'CUnitSpace' ) ) {
			    $boolIsValid = false;
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transfer_unit_space_id', __( 'The transfer unit could not be loaded.' ) ) );
		    }
	    }

    	if( true == valObj( $objTransferLease, 'CLease' ) && true == in_array( $objTransferLease->getLeaseStatusTypeId(), CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transfer_lease_id', __( 'The transfer cannot be cancelled because the transfer lease is now active.' ) ) );
    	}

    	if( false == is_null( $this->getTransferredOn() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transferred_on', __( 'This transfer has already been posted, and can no longer be cancelled.' ) ) );
    	}

    	return $boolIsValid;
    }

    public function valScheduledTransferDate( $objDatabase ) {

    	$boolIsValid = true;

    	$arrintLeaseStatusTypeIds = [ CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ];

    	$objPropertyLease = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->getLeaseId(), $this->getCid(), $objDatabase );

    	$intMaxCheckDate = strtotime( $this->getMoveInDate() );

    	if( true == valObj( $objPropertyLease, 'CLease' ) && strtotime( $this->getMoveInDate() ) < strtotime( $objPropertyLease->getLeaseStartDate() ) ) {
    		$intMaxCheckDate = strtotime( $objPropertyLease->getLeaseStartDate() );
    	}

    	if( strtotime( $this->getMoveOutDate() ) < $intMaxCheckDate ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move out date should be greater than move-in date and lease start date.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valCollectionsBlockedReason() {
    	$boolIsValid = true;
    	if( true == is_null( $this->getCollectionsBlockedReason() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'collections_blocked_reason', __( 'Reason for excluding resident from collections is required.' ) ) );
    	}
    	return $boolIsValid;
    }

    public function valFmoStartedOn() {
    	$boolIsValid = true;

    	if( false == valStr( $this->getFmoStartedOn() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fmo_started_on', __( 'Fmo started on date is required.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valFmoApprovedOn() {
    	$boolIsValid = true;

    	if( true == valStr( $this->getFmoStartedOn() ) && true == valStr( $this->getFmoApprovedOn() ) && strtotime( $this->getFmoApprovedOn() ) < strtotime( $this->getFmoStartedOn() ) ) {
       		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fmo_approved_on', __( 'Fmo approved on date must be greater than or equal to fmo started on date.' ) ) );
       		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

    public function valFmoProcessedOn() {
    	$boolIsValid = true;

    	if( true == valStr( $this->getFmoStartedOn() ) && true == valStr( $this->getFmoApprovedOn() ) && true == valStr( $this->getFmoProcessedOn() )
    		&& ( strtotime( $this->getFmoProcessedOn() ) < strtotime( $this->getFmoStartedOn() ) || strtotime( $this->getFmoProcessedOn() ) < strtotime( $this->getFmoApprovedOn() ) ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fmo_processe_on', __( 'Fmo processed on date must be greater than or equal to fmo started/processed on date.' ) ) );
    		$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

	public function valOverlappingDates( $arrmixDataToValidate ) {
		if( false == $this->valMoveOutDate( true ) ) {
			return false;
		}

		$strOldMoveOutDate                  = getArrayElementByKey( 'old_move_out_date', $arrmixDataToValidate );
		$boolIsAllowOverlappingDates        = getArrayElementByKey( 'allow_overlapping_dates', $arrmixDataToValidate );
		$strOccupantTransferringLeaseId     = getArrayElementByKey( 'occupant_transferring_lease_id', $arrmixDataToValidate );
		$strOccupantTransferringMoveInDate  = getArrayElementByKey( 'occupant_transferring_move_in_date', $arrmixDataToValidate );

		$strOldMoveOutDateToTime = ( true == valStr( $strOldMoveOutDate ) ? strtotime( $strOldMoveOutDate ) : '' );
		$strMoveOutDateToTime = ( true == valStr( $this->getMoveoutDate() ) ? strtotime( $this->getMoveoutDate() ) : '' );

		if( $strOldMoveOutDateToTime != $strMoveOutDateToTime ) {
			$strMoveInDate     = new DateTime( $strOccupantTransferringMoveInDate );
			$strMoveOutDate    = new DateTime( $this->getMoveOutDate() );
			$intDateDifference = $strMoveOutDate->diff( $strMoveInDate )->format( '%r%a' );

			if( 1 < $intDateDifference ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Gaps between Move-out and Transferring Lease {%s,0} Move-in date are not allowed.', [ $strOccupantTransferringLeaseId ] ) ) );
				return false;
			}

			if( false == $boolIsAllowOverlappingDates && 1 > $intDateDifference ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Overlapping of Move-out and Transfer Lease {%s,0} Move-in dates are not allowed.', [ $strOccupantTransferringLeaseId ] ) ) );
				return false;
			}
		}

		return true;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolSkipUnitSpaceIdValidation = false, $arrmixDataToValidate = NULL, $intApplicationId = NULL ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valNoticeDate();
				$boolIsValid &= $this->valMoveOutDate();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valNoticeDate();
				$boolIsValid &= $this->valMoveOutDate( false, $arrmixDataToValidate );
				$boolIsValid &= $this->valCompareNoticeDate( $arrmixDataToValidate );
				break;

			case 'validate_lease_move_out_notice':
				$boolIsValid &= $this->valNoticeDate( true, $arrmixDataToValidate );
				$boolIsValid &= $this->valCompareNoticeDate( $arrmixDataToValidate );
				$boolIsValid &= $this->valMoveOutDate( true, $arrmixDataToValidate, $objDatabase, true );
				$boolIsValid &= $this->valCompareMoveOutDate( $arrmixDataToValidate );
				break;

			case 'validate_termination_process':
				$boolIsValid &= $this->valTerminationStartDate( $boolIsRequired = true, $arrmixDataToValidate );
				$boolIsValid &= $this->valMoveOutReasonListItemId( true, $arrmixDataToValidate );
				break;

			case 'mid_lease_move_out':
				$boolIsValid &= $this->valNoticeDate( $boolIsRequired = true );
				$boolIsValid &= $this->valMoveOutDate( $boolIsRequired = true );
				break;

			case 'begin_transfer':
				$boolIsValid &= $this->valTransferLeaseId();
				$boolIsValid &= $this->valMoveOutDate( $boolIsRequired = true );
				$boolIsValid &= $this->valNoticeDate( $boolIsRequired = true );
				if( false == $boolSkipUnitSpaceIdValidation ) {
					$boolIsValid &= $this->valTransferUnitSpaceId( $objDatabase );
				}
				if( true == $boolIsValid ) $boolIsValid &= $this->valScheduledTransferDate( $objDatabase );
				break;

			case 'cancel_transfer':
				$boolIsValid &= $this->valIsTransfer( $objDatabase );
				break;

			case 'exclude_from_collections':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valCollectionsBlockedReason();
				break;

			case 'validate_move_in':
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valMoveInDate( $boolIsRequired = true, $intApplicationId );
				break;

			case 'validate_occupancy_date':
				$boolIsValid &= $this->valMoveInDate( $boolIsRequired = true );
				$boolIsValid &= $this->valMoveOutDate();
				break;

			case 'validate_financial_move_out':
				$boolIsValid &= $this->valMoveOutDate( getArrayElementByKey( 'require_move_out_date', $arrmixDataToValidate ) );
				$boolIsValid &= $this->valRefundDate( $arrmixDataToValidate );
				$boolIsValid &= $this->valMoveOutReasonListItemId( getArrayElementByKey( 'require_move_out_reason', $arrmixDataToValidate ), $arrmixDataToValidate );
				$boolIsValid &= $this->valFmoStartedOn();
				$boolIsValid &= $this->valFmoApprovedOn();
				$boolIsValid &= $this->valFmoProcessedOn();
				break;

			case 'validate_financial_move_out_dates':
				$boolIsValid &= $this->valFmoStartedOn();
				$boolIsValid &= $this->valFmoApprovedOn();
				$boolIsValid &= $this->valFmoProcessedOn();
				break;

			case 'validate_only_move_out_date':
			case 'validate_calculate_move_out_fees':
				$boolIsValid &= $this->valMoveOutDate( getArrayElementByKey( 'require_move_out_date', $arrmixDataToValidate ), $arrmixDataToValidate );
				break;

			case 'validate_occupant_transfer_move_out_dates':
				$boolIsValid &= $this->valOverlappingDates( $arrmixDataToValidate );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function fetchTransferUnitSpace( $objDatabase ) {
		return CUnitSpaces::createService()->fetchCustomUnitSpaceByIdByCid( $this->m_intTransferUnitSpaceId, $this->getCid(), $objDatabase );
	}

	public function fetchTransferLease( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeases::createService()->fetchCustomLeaseByIdByCid( $this->getTransferLeaseId(), $this->getCid(), $objDatabase );
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>