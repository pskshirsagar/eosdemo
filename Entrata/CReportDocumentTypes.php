<?php

class CReportDocumentTypes extends CBaseReportDocumentTypes {

	public static function fetchReportDocumentTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReportDocumentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReportDocumentType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReportDocumentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>