<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRankGroups
 * Do not add any new functions to this class.
 */

class CRankGroups extends CBaseRankGroups {

	public static function fetchRankGroup( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRankGroup', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>