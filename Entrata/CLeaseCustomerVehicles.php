<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseCustomerVehicles
 * Do not add any new functions to this class.
 */

class CLeaseCustomerVehicles extends CBaseLeaseCustomerVehicles {

	public static function fetchLeaseCustomerVehiclesByIntegrationDatabaseIdByPropertyIdsByCid( $intIntegrationDatabaseId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( lcv.id )
						lcv.*
					FROM
						lease_customer_vehicles lcv
						JOIN lease_customers lc ON ( lc.cid = lcv.cid AND lc.customer_id = lcv.customer_id )
						JOIN leases l ON ( l.cid = lc.cid AND l.id = lc.lease_id )
					WHERE
						lcv.cid = ' . ( int ) $intCid . '
						AND lc.lease_status_type_id != ' . CLeaseStatusType::CANCELLED . '
						AND lc.remote_primary_key IS NOT NULL
						AND l.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
					    AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
 						AND lcv.deleted_on IS NULL';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehicleByCustomerIdByLeaseCustomerVehicleIdByCid( $intCustomerId, $intLeaseCustomerVehicleId, $intCid, $objDatabase ) {
		return self::fetchLeaseCustomerVehicle( sprintf( 'SELECT * FROM lease_customer_vehicles WHERE customer_id = %d AND id = %d AND cid = %d LIMIT 1', ( int ) $intCustomerId, ( int ) $intLeaseCustomerVehicleId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchLeaseCustomLeaseCustomerVehiclesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $intCustomerVehicleTypeId = NULL ) {
		$strSql = 'SELECT * FROM
						lease_customer_vehicles
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND deleted_on IS NULL';

		if( false == is_null( $intCustomerVehicleTypeId ) ) {
			$strSql .= ' AND customer_vehicle_type_id = ' . ( int ) $intCustomerVehicleTypeId;
		} else {
			$strSql .= ' ORDER BY customer_vehicle_type_id ASC ';
		}
		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByCustomerVehicleTypesByCustomerIdByCid( $arrintCustomerVehicleTypeIds, $intCustomerId, $intCid, $objDatabase, $intLeaseId = NULL ) {
		if( false == valArr( $arrintCustomerVehicleTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						lease_customer_vehicles
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND customer_vehicle_type_id IN ( ' . implode( ',', $arrintCustomerVehicleTypeIds ) . ' ) 
						AND deleted_on IS NULL';

		if( true == valId( $intLeaseId ) ) {
			 $strSql .= ' AND lease_id = ' . $intLeaseId;
		}

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase, $boolIsFetchCustomerPhoneNumber = true ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;
		$strSelect = '';
		if( false != $boolIsFetchCustomerPhoneNumber ) {
			$strSelect = ' ,c.phone_number AS customer_phone_number';
		}
		$strSql = 'SELECT
						lcv.*,
						( c.name_last || \' \' || c.name_first ) AS customer_name_full';
		$strSql .= $strSelect;

		$strSql .= ' FROM
						lease_customer_vehicles lcv
						JOIN customers c ON ( lcv.customer_id = c.id AND lcv.cid = c.cid )
					WHERE
						lcv.cid = ' . ( int ) $intCid . '
						AND lcv.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND lcv.deleted_on IS NULL';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseCustomerVehiclesByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( true == is_null( $intLeaseId ) ) return NULL;

		$strSql	= 'SELECT
						lcv.*,
						lc.lease_id,
						lc.customer_id,
						func_format_customer_name( c.name_first, c.name_last, c.company_name ) AS customer_name_full
					FROM
						lease_customer_vehicles lcv
						JOIN lease_customers lc ON ( lcv.cid = lc.cid AND lcv.customer_id = lc.customer_id AND lcv.lease_id = lc.lease_id )
						JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
					WHERE
						lcv.lease_id = ' . ( int ) $intLeaseId . '
						AND lcv.cid = ' . ( int ) $intCid . '
						AND lcv.deleted_on IS NULL
					ORDER BY
 						id ASC';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public function fetchActiveLeaseCustomerVehiclesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		if( true == is_null( $intCustomerId ) ) return NULL;

		$strSql	= 'SELECT
						*
					FROM
						lease_customer_vehicles
					WHERE
						customer_id = ' . ( int ) $intCustomerId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
					ORDER BY
 						id ASC';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {
		if( true == is_null( $arrintLeaseIds ) ) return NULL;

		$strSql	= 'SELECT
						lcv.*,
						( c.name_last || \' \' || c.name_first ) AS customer_name_full
					FROM
						lease_customer_vehicles lcv
						JOIN lease_customers lc ON ( lcv.customer_id = lc.customer_id AND lcv.cid = lc.cid )
						JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
					WHERE
						lc.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND lcv.cid = ' . ( int ) $intCid . '
						AND lcv.make IS NOT NULL
						AND lcv.deleted_on IS NULL
					ORDER BY
						id ASC';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehicleByIdByLeaseIdByCid( $intId, $intLeaseId, $intCid, $objDatabase ) {
		return self::fetchLeaseCustomerVehicle( sprintf( 'SELECT * FROM lease_customer_vehicles WHERE id = %d AND lease_id = %d AND cid = %d AND deleted_on IS NULL AND deleted_by IS NULL', ( int ) $intId, ( int ) $intLeaseId,  ( int ) $intCid ), $objDatabase );
	}

	public static function deleteLeaseCustomerVehiclesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		if( true == is_null( $intCustomerId ) ) return NULL;
		$strSql = 'DELETE FROM lease_customer_vehicles WHERE customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid;
		return false == is_null( $objDatabase->execute( $strSql ) );
	}

	public static function fetchLatestLeaseCustomerVehicleByCustomerIdByLeaseIdByCid( $intCustomerId, $intLeaseId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM
						lease_customer_vehicles
					WHERE
						customer_id     = ' . ( int ) $intCustomerId . '
						AND cid         = ' . ( int ) $intCid . '
						AND lease_id    = ' . ( int ) $intLeaseId . '
						AND deleted_on IS NULL
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchLeaseCustomerVehicle( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByPropertyIdByCid( $intPropertyId, $intClientId, $objDatabase ) {

		$strSql = '
				 SELECT
						DISTINCT (lcv.id),
						lcv.customer_id,
						lcv.customer_vehicle_type_id as vehicle_type,
						l.property_id,
						l.cid,
						lc.customer_type_id as customer_type
				  FROM
						lease_customer_vehicles lcv
						JOIN customers c ON ( c.id = lcv.customer_id AND c.cid = lcv.cid )
						JOIN lease_customers lc ON ( lc.customer_id = lcv.customer_id AND lc.cid = lcv.cid )
						JOIN leases l ON ( l.id = lc.lease_id AND l.cid = lcv.cid )
						JOIN lease_intervals li ON ( li.lease_id = l.id AND li.cid = lcv.cid )
				  WHERE
						li.lease_status_type_id <> 2
						AND lc.customer_type_id IN ( ' . CCustomerType::PRIMARY . ',' . CCustomerType::NOT_RESPONSIBLE . ',' . CCustomerType::GUARANTOR . ' )
						AND lcv.cid = ' . ( int ) $intClientId . '
				 		AND l.property_id = ' . ( int ) $intPropertyId . '
						AND lcv.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByLeaseIdByCustomerVehicleTypeIdByCid( $intLeaseId, $intCustomerVehicleTypeId, $intCid, $objDatabase ) {
		if( true == is_null( $intLeaseId ) ) return NULL;

		$strSql	= 'SELECT
						lcv.*,
						lc.lease_id,
						lc.customer_id,
						( c.name_last || \' \' || c.name_first ) AS customer_name_full
					FROM lease_customer_vehicles lcv
						JOIN lease_customers lc ON ( lcv.customer_id = lc.customer_id AND lcv.cid = lc.cid )
						JOIN customers c ON ( lc.customer_id = c.id AND lc.cid = c.cid )
					WHERE
						lc.lease_id = ' . ( int ) $intLeaseId . '
						AND lcv.cid = ' . ( int ) $intCid . '
						AND lcv.customer_vehicle_type_id = ' . ( int ) $intCustomerVehicleTypeId . '
						AND lcv.deleted_on IS NULL';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchIntegratedCustomerVehiclesNotInCustomerVehicleIdsByCid( $arrintCustomerVehicleIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerVehicleIds ) ) return NULL;

		$strSql	= 'SELECT
						lcv.*
					FROM lease_customer_vehicles lcv
					JOIN lease_customers lc ON ( lc.cid = lcv.cid AND lc.lease_id = lcv.lease_id AND lc.customer_id = lcv.customer_id )
					WHERE lcv.id NOT IN ( ' . implode( ',', array_filter( array_keys( $arrintCustomerVehicleIds ) ) ) . ' )
						AND lc.property_id = ' . ( int ) $intPropertyId . '
 						AND lcv.cid = ' . ( int ) $intCid . '
 						AND lcv.deleted_by IS NULL
						AND lcv.deleted_on IS NULL';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByCustomerIdsByLeaseIdByCid( $arrintCustomerIds, $intLeaseId, $intCid, $objDatabase, $boolIncludeDeleted = true ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
    					lcv.*,
    					( c.name_last || \' \' || c.name_first ) AS customer_name_full,
    	                cpn.phone_number AS customer_phone_number
    				FROM
    					lease_customer_vehicles lcv
    					JOIN customers c ON ( lcv.customer_id = c.id AND lcv.cid = c.cid )
    					LEFT JOIN customer_phone_numbers cpn ON ( cpn.cid = c.cid AND cpn.customer_id = c.id AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
    				WHERE
    					lcv.cid = ' . ( int ) $intCid . '
    					AND lcv.lease_id = ' . ( int ) $intLeaseId . '
    					AND lcv.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';
		if( false == $boolIncludeDeleted ) {
			$strSql .= ' AND lcv.deleted_by IS NULL
    					AND lcv.deleted_on IS NULL';
		}

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByLeaseIdByCustomerIdByCid( $intLeaseId, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valId( $intCustomerId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
    					lease_customer_vehicles
    				WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
    					AND customer_id = ' . ( int ) $intCustomerId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY id ASC';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByLeaseIdByCustomerVehicleTypesByCustomerIdByCid( $intLeaseId, $arrintCustomerVehicleTypeIds, $intCustomerId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerVehicleTypeIds ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM
                        lease_customer_vehicles
                    WHERE
                        cid = ' . ( int ) $intCid . '
                        AND lease_id = ' . ( int ) $intLeaseId . '
                        AND customer_id = ' . ( int ) $intCustomerId . '
                        AND customer_vehicle_type_id IN ( ' . implode( ',', $arrintCustomerVehicleTypeIds ) . ' ) 
                        AND deleted_by IS NULL
                        AND deleted_on IS NULL
                    ORDER BY
                        id';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseCustomerVehiclesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $intCustomerVehicleTypeId = NULL ) {
		$strSql = 'SELECT * FROM
    					lease_customer_vehicles
    				WHERE
						cid = ' . ( int ) $intCid . '
    					AND customer_id = ' . ( int ) $intCustomerId;

		if( false == is_null( $intCustomerVehicleTypeId ) ) {
			$strSql .= ' AND customer_vehicle_type_id = ' . ( int ) $intCustomerVehicleTypeId;
		} else {
			$strSql .= ' ORDER BY customer_vehicle_type_id ASC ';
		}
		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomCustomerVehiclesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $intCustomerVehicleTypeId = NULL ) {
		$strSql = 'SELECT * FROM
    					lease_customer_vehicles
    				WHERE
						cid = ' . ( int ) $intCid . '
    					AND customer_id = ' . ( int ) $intCustomerId;

		if( false == is_null( $intCustomerVehicleTypeId ) ) {
			$strSql .= ' AND customer_vehicle_type_id = ' . ( int ) $intCustomerVehicleTypeId;
		} else {
			$strSql .= ' ORDER BY customer_vehicle_type_id ASC ';
		}
		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByCustomerIdsByLeaseIdsByCid( $arrintCustomerIds, $arrintLeaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						lcv.*,
						( c.name_last || \' \' || c.name_first ) AS customer_name_full,
						cpn.phone_number AS customer_phone_number
					FROM
						lease_customer_vehicles lcv
						JOIN customers c ON ( lcv.customer_id = c.id AND lcv.cid = c.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( c.id = cpn.customer_id AND c.cid = cpn.cid AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						lcv.cid = ' . ( int ) $intCid . '
						AND lcv.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND lcv.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND lcv.deleted_on IS NULL';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesByLeaseStatusTypeIdsByCustomerIdsByCid( $arrintLeaseStatusTypeIds, $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						lcv.*,
						( c.name_last || \' \' || c.name_first ) AS customer_name_full,
						cpn.phone_number AS customer_phone_number
					FROM
						lease_customer_vehicles lcv
						JOIN customers c ON ( lcv.customer_id = c.id AND lcv.cid = c.cid )
						JOIN cached_leases cl ON ( lcv.lease_id = cl.id AND lcv.cid = cl.cid )
						LEFT JOIN customer_phone_numbers cpn ON ( c.id = cpn.customer_id AND c.cid = cpn.cid AND cpn.is_primary IS TRUE AND cpn.deleted_by IS NULL )
					WHERE
						lcv.cid = ' . ( int ) $intCid . '
						AND cl.lease_status_type_id IN ( ' . implode( ',', $arrintLeaseStatusTypeIds ) . ' )
						AND lcv.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND lcv.deleted_on IS NULL';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseCustomerVehiclesByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase, $strDateFilter = NULL ) {
		if( false == valArr( $arrintLeaseIds ) ) return NULL;

		$strSql = 'SELECT
						lcv.*,
						( c.name_last || \' \' || c.name_first ) AS customer_name_full
					FROM
						lease_customer_vehicles lcv
						JOIN customers c ON ( lcv.customer_id = c.id AND lcv.cid = c.cid )
					WHERE
						lcv.lease_id IN( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND lcv.cid = ' . ( int ) $intCid . ' 
						AND lcv.deleted_by IS NULL
						AND lcv.deleted_on IS NULL' . $strDateFilter . ' 
					ORDER BY
						id ASC;';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public function fetchLeaseCustomerVehicleByCustomerIdByCustomerVehicleIdByCid( $intCustomerId, $intCustomerVehicleId, $intCid, $objDatabase ) {
		return self::fetchLeaseCustomerVehicle( sprintf( 'SELECT * FROM lease_customer_vehicles WHERE customer_id = %d AND id = %d AND cid = %d LIMIT 1', ( int ) $intCustomerId, ( int ) $intCustomerVehicleId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomLeaseCustomerVehicleByLeaseIdByCustomerIdByCid( $strMake, $strModel, $strColor, $strStateCode, $strLicensePlateNumber, $intLeaseId, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valId( $intCustomerId ) ) {
			return NULL;
		}

		$strSql = 'SELECT *
		            FROM lease_customer_vehicles
				    WHERE cid = ' . ( int ) $intCid . ' 
				          AND lease_id = ' . ( int ) $intLeaseId . '
				          AND customer_id = ' . ( int ) $intCustomerId . '
				          AND lower( make ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strMake ) ) . '\'
				          AND lower( model ) =\'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strModel ) ) . '\'
				          AND lower( color ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strColor ) ) . '\'
				          AND lower( state_code ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strStateCode ) ) . '\'
				          AND lower( license_plate_number ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strLicensePlateNumber ) ) . '\'
				          AND deleted_by IS NULL
				          AND deleted_on IS NULL
					LIMIT 1';

		return self::fetchLeaseCustomerVehicle( $strSql, $objDatabase );
	}

	public static function fetchLeaseCustomerVehiclesCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsSummarizeByPropertyAndPriority = false ) {
		// please reach to reports team while making any changes to these functions because same we are using in report
		if( !valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}
		$strSelectSql = 'COUNT( lcv.id ) AS customer_vehicles_count ';
		$strGroupBySql = '';
		$strPropertyJoinSql = '';
		if( $boolIsSummarizeByPropertyAndPriority ) {
			$strSelectSql = 'COUNT( lcv.id ) AS residents_vehicles,
							 1 AS priority,
							 e.property_id,
							 p.property_name ';
			$strGroupBySql  = ' GROUP BY
							priority,
							e.property_id,
							p.property_name';
			$strPropertyJoinSql = ' JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ' ] ) AS lp ON ( lp.cid = e.cid AND lp.is_disabled = 0 AND lp.property_id = e.property_id )
					JOIN properties AS p ON ( p.cid = lp.cid AND p.id = lp.property_id )';
		}

		$strSql = 'SELECT ' . $strSelectSql . ' 
			FROM
				events e
				JOIN lease_customer_vehicles lcv ON lcv.cid = e.cid AND lcv.lease_id = e.lease_id AND lcv.id = e.data_reference_id AND lcv.deleted_on IS NULL'
				. $strPropertyJoinSql .
				' WHERE
					e.cid = ' . ( int ) $intCid . '
					AND e.event_type_id = ' . CEventtype::ADD_VEHICLE . '
					AND e.created_by = ' . CUser::ID_RESIDENT_PORTAL . '
					AND e.property_id IN( SELECT property_id FROM load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . '], NULL, true ) )
					AND e.is_deleted = FALSE
					AND e.event_result_id IS NULL' . $strGroupBySql;
		$arrmixData = fetchData( $strSql, $objDatabase );
		if( !$boolIsSummarizeByPropertyAndPriority ) {
			if( valArr( $arrmixData ) ) {
				return $arrmixData[0];
			}
			return [ 'customer_vehicles_count' => 0 ];
		}
		return  $arrmixData;

	}

	public static function fetchPaginatedLeaseCustomerVehiclesByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase ) {
		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						lcv.id,
						lcv.cid,
						( c.name_last || \' \' || c.name_first ) AS customer_name_full,
						c.phone_number AS customer_phone_number,
						c.email_address,
						lcv.created_on,
						cl.property_name,
						e.id AS event_id,
						cl.id AS lease_id,
						c.id AS customer_id,
						lp.property_id AS property_id
					FROM
						events e  
						JOIN lease_customer_vehicles lcv ON( e.cid = lcv.cid AND e.lease_id = lcv.lease_id AND e.data_reference_id = lcv.id )
					    JOIN lease_customers lc ON ( lc.cid = lcv.cid AND lc.customer_id = lcv.customer_id AND lcv.lease_id = lc.lease_id )
						JOIN cached_leases cl ON ( cl.id = lc.lease_id AND cl.cid = lcv.cid AND lc.property_id = cl.property_id )
						JOIN customers c ON ( lcv.customer_id = c.id AND lcv.cid = c.cid )
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . '], NULL, ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'true' : 'false' ) . '  ) lp ON lp.property_id = cl.property_id
					WHERE
						lcv.cid = ' . ( int ) $intCid . '
					    AND lcv.deleted_on IS NULL
						AND e.event_type_id = ' . CEventtype::ADD_VEHICLE . '
						AND e.created_by = ' . CUser::ID_RESIDENT_PORTAL . '
						AND e.is_deleted = FALSE
						AND lcv.deleted_on IS NULL
						AND e.event_result_id IS NULL
					ORDER BY
						' . $strSortBy . ', e.id, lcv.id DESC
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllLeaseCustomerVehiclesByLeaseIdByCustomerIdByCid( $intLeaseId, $intCustomerId, $intCid, $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valId( $intCustomerId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
    					lease_customer_vehicles
    				WHERE
						cid = ' . ( int ) $intCid . '
						AND lease_id = ' . ( int ) $intLeaseId . '
    					AND customer_id = ' . ( int ) $intCustomerId . '
					ORDER BY id ASC';
		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

	public static function fetchCustomLeaseCustomerVehiclesByLeaseCustomerIdsByCid( $arrintLeaseCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintLeaseCustomerIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						lcv.*
					FROM
						lease_customer_vehicles lcv
						JOIN lease_customers lc ON ( lc.cid = lcv.cid AND lc.customer_id = lcv.customer_id AND lc.lease_id = lcv.lease_id )
						JOIN customers c ON ( c.cid = lcv.cid AND c.id = lcv.customer_id )
					WHERE
						lcv.cid = ' . ( int ) $intCid . '
						AND lc.id IN ( ' . sqlIntImplode( $arrintLeaseCustomerIds ) . ' )
						AND lcv.deleted_on IS NULL';

		return self::fetchLeaseCustomerVehicles( $strSql, $objDatabase );
	}

}
?>
