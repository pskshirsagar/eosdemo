<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COneTimeLinks
 * Do not add any new functions to this class.
 */

class COneTimeLinks extends CBaseOneTimeLinks {

	public static function fetchOneTimeLinksByApplicantIdByKeyEncryptedByManagementId( $intApplicantId, $strEncryptedKey, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						one_time_links
					WHERE
						applicant_id = ' . ( int ) $intApplicantId . '
						AND key_encrypted LIKE \'' . $strEncryptedKey . '\'
						AND deleted_on is NULL
						AND cid = ' . ( int ) $intCid;

		return self::fetchOneTimeLink( $strSql, $objDatabase );
	}

	public static function fetchOneTimeLinkByApplicantIdByApplicationIdByCidByKeyEncrypted( $intApplicantId, $intApplicationId, $intCid, $strEncryptedKey, $objDatabase ) {
		if( false == valId( $intApplicantId ) || false == valId( $intApplicationId ) || false == valId( $intCid ) || false == valStr( $strEncryptedKey ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						otl.*,
						cps.username,
						cps.password_encrypted as password
					FROM
						one_time_links otl
						JOIN clients c ON ( otl.cid = c.id AND c.id = ' . ( int ) $intCid . ' )
						JOIN view_applicants va ON ( c.id = va.cid AND c.id = ' . ( int ) $intCid . ' AND va.id = otl.applicant_id AND va.id = ' . ( int ) $intApplicantId . ' )
						JOIN applicant_applications aa ON ( aa.applicant_id = va.id AND aa.cid = va.cid AND aa.application_id = ' . ( int ) $intApplicationId . ' )
						JOIN customer_portal_settings cps ON ( va.customer_id = cps.customer_id AND va.cid = cps.cid )
					WHERE
						otl.deleted_on is NULL
						AND key_encrypted LIKE \'' . $strEncryptedKey . '\'
						AND NOW() < otl.created_on + INTERVAL \'' . COneTimeLink::EXPIRY_DAYS . ' days\'
						AND cps.username IS NOT NULL
						AND cps.password_encrypted IS NOT NULL
					LIMIT 1';

		return self::fetchOneTimeLink( $strSql, $objDatabase );
	}

}
?>