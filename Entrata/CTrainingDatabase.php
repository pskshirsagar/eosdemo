<?php

class CTrainingDatabase extends CBaseTrainingDatabase {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) || true == empty( $this->getName() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Training Name is required.' ), NULL ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valStartDate( $strAction = VALIDATE_INSERT, $boolIsTrainingDatabaseEditable ) {
		$boolIsValid = true;

		if( true == empty( $this->getStartDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Valid Start Date is required.' ), NULL ) );
			$boolIsValid = false;
		}

		$strCurrentDateByTimeZone = date( 'Y-m-d' );
		if( true == $boolIsValid && $strAction == VALIDATE_INSERT && strtotime( $this->getStartDate() ) < strtotime( $strCurrentDateByTimeZone ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Please select Start Date other than past date.' ) ), NULL );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && $strAction == VALIDATE_UPDATE && true == $boolIsTrainingDatabaseEditable && strtotime( $this->getStartDate() ) < strtotime( $strCurrentDateByTimeZone ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', __( 'Please select Start Date other than past date.' ) ), NULL );
			$boolIsValid &= false;
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valExpiresOn() {
		$boolIsValid = true;

		if( true == empty( $this->getEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expires_on', __( 'Valid Expiration Date is required.' ), NULL ) );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && strtotime( $this->getStartDate() ) >= strtotime( $this->getEndDate() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expires_on', __( 'Please select Expiration Date greater than Start Date.' ) ), NULL );
			$boolIsValid &= false;
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsTrainingDatabaseEditable = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valStartDate( $strAction, $boolIsTrainingDatabaseEditable );
				$boolIsValid &= $this->valExpiresOn();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>