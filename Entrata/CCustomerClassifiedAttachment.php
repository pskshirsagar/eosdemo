<?php

class CCustomerClassifiedAttachment extends CBaseCustomerClassifiedAttachment {
	use Psi\Libraries\EosFoundation\TEosStoredObject;
	use TEosDetails;
	use TEosTranslated;

	protected $m_strTitle;

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strName', $strLocaleCode );
	}

	public function deleteCustomerClassifiedAttachment( $objStorageGateway = NULL, \CDatabase $objDataBase = NULL ) {
		if( false == is_null( $this->getFilePath() ) && false == is_null( $this->getFileName() ) ) {

			if( true === \Psi\Libraries\UtilFunctions\valObj( $objStorageGateway, \CProxyObjectStorageGateway::class ) ) {
				$objFetchStoredObject = $this->fetchStoredObject( $objDataBase );
				$arrmixRequest = $objFetchStoredObject->createGatewayRequest( [ 'checkExists' => true ] );
				$objObjectStorageGatewayResponse = $objStorageGateway->getObject( $arrmixRequest );

				if( false == $objObjectStorageGatewayResponse->hasErrors() && true == $objObjectStorageGatewayResponse['isExists'] ) {
					$arrmixRequest = $this->fetchStoredObject( $objDataBase )->createGatewayRequest();
					$objObjectStorageGatewayResponse = $objStorageGateway->deleteObject( $arrmixRequest );
					if( false == $objObjectStorageGatewayResponse->hasErrors() ) {
						$this->deleteStoredObject( $this->getCid(), $objDataBase );
					}
				} else if( true == file_exists( getMountsPath( $this->getCid(), PATH_MOUNTS_FILES ) . $this->getFilePath() . $this->getFileName() ) ) {
					unlink( getMountsPath( $this->getCid(), PATH_MOUNTS_FILES ) . $this->getFilePath() . $this->getFileName() );
				}
			} else if( true == file_exists( getMountsPath( $this->getCid(), PATH_MOUNTS_FILES ) . $this->getFilePath() . $this->getFileName() ) ) {
				unlink( getMountsPath( $this->getCid(), PATH_MOUNTS_FILES ) . $this->getFilePath() . $this->getFileName() );
			}
		}
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return $this->getFilePath() . $this->getFileName();

			default:
				return 'classified_attachments' . '/' . $this->getCid() . '/' . date( 'Y/m/d' ) . '/' . $this->getId() . '/' . $this->getFileName();
		}
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return PATH_MOUNTS_FILES;

			default:
				return CONFIG_OSG_BUCKET_DOCUMENTS;
		}
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}
}