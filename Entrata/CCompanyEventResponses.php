<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEventResponses
 * Do not add any new functions to this class.
 */

class CCompanyEventResponses extends CBaseCompanyEventResponses {

	public static function fetchCompanyEventResponsesByCompanyEventIdByPropertyIdByCid( $intCompanyEventId, $intPropertyId, $intCid, $objDatabase, $intApproved = 0 ) {

		$strSql = 'SELECT cer.*
					 FROM
						company_event_responses cer
					 JOIN
						property_events pe ON ( cer.property_event_id = pe.id AND cer.cid = pe.cid ';

		if( 1 == $intApproved ) {
			$strSql .= ' AND cer.approved_on IS NOT NULL ';
		}

		$strSql .= ' ) WHERE
					 		pe.company_event_id = ' . ( int ) $intCompanyEventId . '
					 		AND cer.cid = ' . ( int ) $intCid . '
						    AND cer.property_id = ' . ( int ) $intPropertyId . '
					  ORDER BY
							cer.response_datetime DESC';

		return self::fetchCompanyEventResponses( $strSql, $objDatabase );
	}

	public static function fetchCompanyEventResponseByIdByCompanyEventIdByPropertyIdByCid( $intCompanyEventResponseId, $intCompanyEventId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT cer.*
					 FROM company_event_responses cer
					 JOIN property_events pe ON ( cer.property_event_id = pe.id AND cer.cid = pe.cid AND pe.company_event_id = ' . ( int ) $intCompanyEventId . '
					  AND cer.cid = ' . ( int ) $intCid . '
					  AND cer.property_id = ' . ( int ) $intPropertyId . '
 					  AND cer.id = ' . ( int ) $intCompanyEventResponseId . ' )';

		return self::fetchCompanyEventResponse( $strSql, $objDatabase );
	}

}
?>