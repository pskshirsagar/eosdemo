<?php

class CMaintenanceTemplateType extends CBaseMaintenanceTemplateType {

	const MAKE_READY = 1;
	const MANUAL     = 2;
	const SCHEDULED  = 3;

	  public function valId() {
	        $boolIsValid = true;
	        return $boolIsValid;
	  }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function getMaintenanceTemplateTypeNameById( $intId ) {

		$strTemplateTypeName = NULL;

		switch( $intId ) {

			case CMaintenanceTemplateType::MAKE_READY:
				$strTemplateTypeName = 'Make Ready';
				break;

			case CMaintenanceTemplateType::MANUAL:
				$strTemplateTypeName = 'Manual';
				break;

			case CMaintenanceTemplateType::SCHEDULED:
				$strTemplateTypeName = 'Scheduled';
				break;

			default:
				// default case
				break;
		}

		return $strTemplateTypeName;
	}

}

?>