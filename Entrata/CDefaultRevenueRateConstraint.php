<?php

class CDefaultRevenueRateConstraint extends CBaseDefaultRevenueRateConstraint {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRevenueRateConstraintTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRuleChangeAmount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsFixed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>