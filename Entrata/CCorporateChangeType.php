<?php

class CCorporateChangeType extends CBaseCorporateChangeType {

	const BUSINESS_NAME         = 1;
	const EMAIL_ADDRESS         = 2;
	const PHONE_NUMBER          = 3;
	const STREET                = 4;
	const CITY                  = 5;
	const STATE_CODE            = 6;
	const ZIP_CODE              = 7;
	const TAX_NUMBER            = 8;
	const BIN                   = 9;
	const PRIMARY_CONTACT_NAME  = 10;
	const PRIMARY_CONTACT_PHONE = 11;
	const PRIMARY_CONTACT_EMAIL = 12;

}