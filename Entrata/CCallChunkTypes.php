<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCallChunkTypes
 * Do not add any new functions to this class.
 */

class CCallChunkTypes extends CBaseCallChunkTypes {

	public static function fetchCallChunkTypes( $strSql, $objDatabase ) {

		return self::fetchCachedObjects( $strSql, 'CCallChunkType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallChunkType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallChunkType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>