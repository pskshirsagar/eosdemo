<?php

class CFileMdrImport extends CBaseFileMdrImport {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>