<?php

class COrganizationContractChecklist extends CBaseOrganizationContractChecklist {

	private $m_intChecklistTriggerId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrganizationContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChecklistId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateChecklistId( $intChecklistTriggerId, $objDatabase ) {
		if( false == valId( $this->getChecklistId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( '{%s,0} checklist required as \'Use {%s,0} Checklist\' is enabled.', [ ( CChecklistTrigger::GROUP_MOVE_IN == $intChecklistTriggerId ) ? 'Move-In' : 'Move-Out' ] ) ) );
			return false;
		}

		if( false == valArr( \Psi\Eos\Entrata\CPropertyChecklists::createService()->fetchPropertyChecklistIdByChecklistIdByChecklistTypeIdByChecklistTriggerIdByPropertyIdByCid( $this->getChecklistId(), CChecklistType::GROUP, $intChecklistTriggerId, $this->getPropertyId(), $this->getCid(), $objDatabase ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Invalid checklist id selected for {%s,0}.', [ ( CChecklistTrigger::GROUP_MOVE_IN == $intChecklistTriggerId ) ? 'Move-In' : 'Move-Out' ] ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objDatabase, $intChecklistTriggerId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid = $this->validateChecklistId( $intChecklistTriggerId, $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['checklist_trigger_id'] ) ) $this->setChecklistTriggerId( $arrmixValues['checklist_trigger_id'] );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );
		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	/**
	 * Getter Functions
	 *
	 */

	public function getChecklistTriggerId() {
		return $this->m_intChecklistTriggerId;
	}

	/**
	 * setter Functions
	 *
	 */

	public function setChecklistTriggerId( $intChecklistTriggerId ) {
		$this->m_intChecklistTriggerId = $intChecklistTriggerId;
	}

}
?>