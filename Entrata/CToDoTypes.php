<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CToDoTypes
 * Do not add any new functions to this class.
 */

class CToDoTypes extends CBaseToDoTypes {

	public static function fetchToDoTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CToDoType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchToDoType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CToDoType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}
}
?>