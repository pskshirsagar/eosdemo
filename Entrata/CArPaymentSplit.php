<?php

class CArPaymentSplit extends CBaseArPaymentSplit {

	protected $m_objLease;
	protected $m_objCustomer;
	protected $m_objPropertyGlSetting;

	protected $m_boolIsFloating;
	protected $m_boolAutoAllocations;
	protected $m_boolUseTransactionDateForAutoAllocation;

	protected $m_fltLeaseBalance;

	protected $m_intArCodeId;
	protected $m_intRepaymentId;
	protected $m_intCheckIsMoneyOrder;

	protected $m_strTransactionMemo;
	protected $m_strTransactionInternalMemo;

	protected $m_arrmixArAllocations;

	public function __construct() {
		parent::__construct();

		$this->m_boolAutoAllocations = true;

		return;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createPaymentArTransaction( $objArPayment, $boolIsTemporary, $objClientDatabase ) {

		if( false == is_numeric( $this->getId() ) ) {
			$this->fetchNextId( $objClientDatabase );
		}

		$objArTransaction = new CArTransaction();
		$objArTransaction->setCid( $this->getCid() );
		$objArTransaction->setPropertyId( $this->getPropertyId() );
		$objArTransaction->setLeaseId( $this->getLeaseId() );
		$objArTransaction->setArPaymentSplitId( $this->getId() );
		$objArTransaction->setArCodeTypeId( CArCodeType::PAYMENT );
		$objArTransaction->setArOriginId( CArOrigin::BASE );
		$objArTransaction->setArTriggerId( CArTrigger::UNKNOWN );
		$objArTransaction->setRepaymentId( $this->getRepaymentId() );

		if( true == valId( $this->getArCodeId() ) ) {
			$objArTransaction->setArCodeId( $this->getArCodeId() );
		} elseif( true == is_null( $objArPayment->getPaymentArCodeId() ) ) {
			$objClient = $objArPayment->getOrFetchClient( $objClientDatabase );
			if( true == $objArPayment->getIsGroupPayment() ) {
				$intLedgerFilterId   = reset( array_keys( ( array ) \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFiltersByDefaultLedgerFilterIdsByCid( [ \CDefaultLedgerFilter::GROUP ], $objClient->getId(), $objClientDatabase ) ) );
				$objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByLedgerFilterIdByArCodeTypeIdByCid( $intLedgerFilterId, \CArCodeType::PAYMENT, $objClient->getId(), $objClientDatabase );
			} else {
				$objArCode = $objClient->fetchArCodeByDefaultArCodeId( \CDefaultArCode::PAYMENT, $objClientDatabase );
			}
			if( true == valObj( $objArCode, 'CArCode' ) ) {
				$objArTransaction->setArCodeId( $objArCode->getId() );
			}
		} else {
			$objArTransaction->setArCodeId( $objArPayment->getPaymentArCodeId() );
		}
		if( true == $boolIsTemporary && \CPaymentStatusType::CAPTURED == $objArPayment->getPaymentStatusTypeId() ) {
			trigger_error( 'createPaymentArTransaction split module. Ar Payment Id : ' . $objArPayment->getId(), E_USER_WARNING );
		}
		$objArTransaction->setIsTemporary( $boolIsTemporary );
		$this->m_objPropertyGlSetting = $this->getOrFetchPropertyGlSetting( $objClientDatabase );
		if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			$objArTransaction->setPostMonth( $this->m_objPropertyGlSetting->getArPostMonth() );
		}

		$objArTransaction->setUseTransactionDateForAutoAllocation( $this->getUseTransactionDateForAutoAllocation() );

		$objArTransaction->setArPaymentId( $this->getArPaymentId() );
		$objArTransaction->setPostDate( ( ( false == is_null( $objArPayment->getTransactionDatetime() ) ) ? $objArPayment->getTransactionDatetime() : $objArPayment->getReceiptDatetime() ) );
		$objArTransaction->setPaymentAmount( $this->getPaymentAmount() );
		$objArTransaction->setTransactionAmountDue( -1 * $this->getPaymentAmount() );
		$objArTransaction->setMemo( true == valStr( $this->getTransactionMemo() ) ? $this->getTransactionMemo() : ( ( true == valStr( $objArPayment->getPostMemo() ) ) ? $objArPayment->getPostMemo() : $objArPayment->getPaymentMemo() ) );
		$objArTransaction->setInternalMemo( $this->getTransactionInternalMemo() );

		return $objArTransaction;
	}

	/**
	 * Get Or Fetch Functions
	 *
	 */

	public function getOrFetchLease( $objDatabase ) {

		if( true == valObj( $this->m_objLease, 'CLease' ) ) {
			return $this->m_objLease;
		} else {
			$this->m_objLease = $this->fetchLease( $objDatabase );
			return $this->m_objLease;
		}
	}

	public function getOrFetchPropertyGlSetting( $objClientDatabase ) {
		if( true == valObj( $this->m_objPropertyGlSetting, 'CPropertyGlSetting' ) ) {
			return $this->m_objPropertyGlSetting;
		} else {
			return $this->fetchPropertyGlSetting( $objClientDatabase );
		}
	}

	/**
	 * Get Functions
	 *
	 */

	public function getLease() {
		return $this->m_objLease;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getAutoAllocation() {
		return $this->m_boolAutoAllocations;
	}

	public function getIsFloating() {
		return $this->m_boolIsFloating;
	}

	public function getLeaseBalance() {
		return $this->m_fltLeaseBalance;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	public function getCheckIsMoneyOrder() {
		return $this->m_intCheckIsMoneyOrder;
	}

	public function getTransactionMemo() {
		return $this->m_strTransactionMemo;
	}

	public function getTransactionInternalMemo() {
		return $this->m_strTransactionInternalMemo;
	}

	public function getArCodeId() {
		return $this->m_intArCodeId;
	}

	public function getRepaymentId() {
		return $this->m_intRepaymentId;
	}

	public function getArAllocations() {
		return $this->m_arrmixArAllocations;
	}

	public function getUseTransactionDateForAutoAllocation() {
		return $this->m_boolUseTransactionDateForAutoAllocation;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setLease( $objLease ) {
		$this->m_objLease = $objLease;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setIsFloating( $intIsFloating ) {
		$this->m_boolIsFloating = $intIsFloating;
	}

	public function setLeaseBalance( $fltLeaseBalance ) {
		$this->m_fltLeaseBalance = $fltLeaseBalance;
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setAutoAllocation( $boolAllocations ) {
		$this->m_boolAutoAllocations = $boolAllocations;
	}

	public function setCheckIsMoneyOrder( $intCheckIsMoneyOrder ) {
		$this->m_intCheckIsMoneyOrder = CStrings::strToIntDef( $intCheckIsMoneyOrder, NULL, false );
	}

	public function setTransactionMemo( $strTransactionMemo ) {
		$this->m_strTransactionMemo = $strTransactionMemo;
	}

	public function setTransactionInternalMemo( $strTransactionInternalMemo ) {
		$this->m_strTransactionInternalMemo = CStrings::strTrimDef( $strTransactionInternalMemo, 2000, NULL, true );
	}

	public function setArCodeId( $intArCodeId ) {
		$this->m_intArCodeId = $intArCodeId;
	}

	public function setRepaymentId( $intRepaymentId ) {
		$this->m_intRepaymentId = $intRepaymentId;
	}

	public function setArAllocations( $arrmixArAllocations ) {
		$this->m_arrmixArAllocations = $arrmixArAllocations;
	}

	public function setUseTransactionDateForAutoAllocation( $boolUseTransactionDateForAutoAllocation ) {
		$this->m_boolUseTransactionDateForAutoAllocation = $boolUseTransactionDateForAutoAllocation;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['repayment_id'] ) ) {
			$this->setRepaymentId( $arrmixValues['repayment_id'] );
		}
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArPaymentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( false == is_null( $this->m_intCustomerId ) && 0 >= ( int ) $this->m_intCustomerId && 1 != $this->getIsFloating() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_medium_id', __( 'A resident is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intLeaseId ) && 0 >= ( int ) $this->m_intLeaseId && true != $this->m_boolIsFloating ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'A lease is required for split payment.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPaymentAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltPaymentAmount ) || 0 == $this->m_fltPaymentAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', __( 'Payment amount is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnExportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnBatchedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolIsAllowPaymentWarningOverride= true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valPaymentAmount();
				if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
					$boolIsValid &= $this->valCustomerPayStatus( $objClientDatabase, $boolIsAllowPaymentWarningOverride );
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valPaymentAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valCustomerPayStatus( $objClientDatabase, $boolIsAllowPaymentWarningOverride = true ) {

		$boolIsValid = true;

		$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );
		$objLease 	 = $this->getOrFetchLease( $objClientDatabase );

		if( false == valObj( $objCustomer, 'CCustomer' ) || false == valObj( $objLease, 'CLease' ) ) return false;

		$arrobjPropertyPreferences = CPropertyPreferences::fetchPropertyPreferencesByKeysByPropertyIdByCid( [ 'ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION', 'PREVENT_PAST_RESIDENT_OVERPAYMENT' ], $this->getPropertyId(), $this->m_intCid, $objClientDatabase );
		$arrobjPropertyPreferences = rekeyObjects( 'Key', $arrobjPropertyPreferences );

		if( CPaymentAllowanceType::BLOCK_ALL == $objCustomer->getPaymentAllowanceTypeId()
			|| CPaymentAllowanceType::BLOCK_ALL == $objLease->getPaymentAllowanceTypeId() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( '{%s, 0} is blocked from making any payments on this lease.', [ $objCustomer->getNameFull() ] ) ) );
		} elseif( ( CPaymentAllowanceType::CASH_EQUIVALENT == $objCustomer->getPaymentAllowanceTypeId()
						|| CPaymentAllowanceType::CASH_EQUIVALENT == $objLease->getPaymentAllowanceTypeId() )
					&& 1 != $this->getCheckIsMoneyOrder() && false == $boolIsAllowPaymentWarningOverride ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( '{%s, 0} is restricted to make payments by certified funds only on this lease.', [ $objCustomer->getNameFull() ] ) ) );

		} elseif( false == $objLease->getIsPaymentAllowedForEvicting() && true == isset( $arrobjPropertyPreferences['BLOCK_ALL_PAYMENTS_FROM_EVICTING'] ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Evicting status resident {%s, 0} is not permitted to make a payment.', [ $objCustomer->getNameFull() ] ) ) );

		} elseif( true == $objLease->checkLeaseInCollection()
					&& false == isset( $arrobjPropertyPreferences['ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION'] ) && false == $boolIsAllowPaymentWarningOverride ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'In Collection status split resident {%s, 0} is not permitted to make a payment.', [ $objCustomer->getNameFull() ] ) ) );
		}

		return $boolIsValid;
	}

	public function getCustomerPayStatus( $objClientDatabase ) {

		$intCheckEquivalent 				= 0;
		$intBlockAll 						= 0;
		$intLeaseIsInCollection				= 0;

		$boolLeaseIsInCollection			= false;
		$boolPreventPastResidentOverPayment = false;

		$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );
		$objLease 	 = $this->getOrFetchLease( $objClientDatabase );

		if( false == valObj( $objCustomer, 'CCustomer' ) || false == valObj( $objLease, 'CLease' ) ) return false;

		if( CPaymentAllowanceType::BLOCK_ALL == $objCustomer->getPaymentAllowanceTypeId()
			|| CPaymentAllowanceType::BLOCK_ALL == $objLease->getPaymentAllowanceTypeId() ) {

			$intCheckEquivalent = 0;
			$intBlockAll 		= 1;
		} elseif( ( CPaymentAllowanceType::CASH_EQUIVALENT == $objCustomer->getPaymentAllowanceTypeId()
					|| CPaymentAllowanceType::CASH_EQUIVALENT == $objLease->getPaymentAllowanceTypeId() ) && 1 != $this->getCheckIsMoneyOrder() ) {

			$intCheckEquivalent = 1;
			$intBlockAll 		= 0;
		}

		$arrobjPropertyPreferences = CPropertyPreferences::fetchPropertyPreferencesByKeysByPropertyIdByCid( [ 'ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION', 'PREVENT_PAST_RESIDENT_OVERPAYMENT', 'BLOCK_ALL_PAYMENTS_FROM_EVICTING' ], $this->getPropertyId(), $this->m_intCid, $objClientDatabase );
		$arrobjPropertyPreferences = rekeyObjects( 'Key', $arrobjPropertyPreferences );

		if( true == $objLease->checkLeaseInCollection() ) {
			$intLeaseIsInCollection = 1;
		}

		$intBlockAllPaymentsFromEvicting = 0;
		if( false == $objLease->getIsPaymentAllowedForEvicting() && true == isset( $arrobjPropertyPreferences['BLOCK_ALL_PAYMENTS_FROM_EVICTING'] ) ) {
			$intBlockAllPaymentsFromEvicting = 1;
		}

		$intBlockAllPaymentsFromInCollection = 0;
		if( 1 == $intLeaseIsInCollection && false == isset( $arrobjPropertyPreferences['ALLOW_PAYMENTS_FOR_RESIDENTS_IN_COLLECTION'] ) ) {
			$intBlockAllPaymentsFromInCollection = 1;
		}

		if( 0 == $intBlockAllPaymentsFromInCollection && 1 == $intLeaseIsInCollection ) {
			$boolLeaseIsInCollection = true;
		}

		if( true == isset( $arrobjPropertyPreferences['PREVENT_PAST_RESIDENT_OVERPAYMENT'] )
			&& CLeaseStatusType::PAST == $objLease->getLeaseStatusTypeId() ) {

			$boolPreventPastResidentOverPayment = true;
		}

		return [ 'cash_or_check_equivalent' => $intCheckEquivalent, 'block_all' => $intBlockAll, 'lease_in_collection' => $boolLeaseIsInCollection, 'prevent_past_resident_overpayment' => $boolPreventPastResidentOverPayment, 'block_all_payments_from_evicting' => $intBlockAllPaymentsFromEvicting, 'block_all_payments_from_in_collection' => $intBlockAllPaymentsFromInCollection ];
	}

	/**
	 * Fetch Functions
	 */

	public function fetchLease( $objDatabase ) {
		if( false == is_int( $this->m_intCid ) ) return NULL;
		if( false == is_int( $this->m_intLeaseId ) ) return NULL;

		return \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCidBySchema( $this->m_intLeaseId, $this->m_intCid, $objDatabase );
	}

	public function getOrFetchCustomer( $objDatabase ) {
		if( true == valObj( $this->m_objCustomer, 'CCustomer' ) ) {
			return $this->m_objCustomer;
		} else {
			$this->m_objCustomer = $this->fetchCustomer( $objDatabase );
			return $this->m_objCustomer;
		}
	}

	public function fetchCustomer( $objDatabase ) {
		if( false == valId( $this->m_intCid ) || false == valId( $this->m_intCustomerId ) ) return NULL;
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->m_intCustomerId, $this->m_intCid, $objDatabase );
	}

	public function fetchPropertyGlSetting( $objDatabase ) {
		return CPropertyGlSettings::fetchCustomPropertyGlSettingsByPropertyIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intCurrentUserId, $objDatabase, $intUserValidationMode = CPropertyGlSetting::VALIDATION_MODE_STRICT ) {

		if( CPropertyGlSetting::VALIDATION_MODE_STRICT != $intUserValidationMode ) return parent::delete( $intCurrentUserId, $objDatabase );

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

}
?>