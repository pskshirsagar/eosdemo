<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFieldGroups
 * Do not add any new functions to this class.
 */

class CFieldGroups extends CBaseFieldGroups {

	public static function fetchFieldGroupsByFieldTypeIdByCid( $intFieldTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						field_groups
					WHERE
						field_type_id = ' . $intFieldTypeId . '
						AND cid = ' . $intCid;

		return self::fetchFieldGroups( $strSql, $objDatabase );

	}
}
?>