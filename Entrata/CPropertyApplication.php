<?php

class CPropertyApplication extends CBasePropertyApplication {

	protected $m_strTitle;
	protected $m_boolWebVisible;
	protected $m_intOccupancyTypeId;

	protected $m_arrobjPropertyApplicationPreferences;

    /**
     * Get Functions
     */

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getWebVisible() {
		return $this->m_boolWebVisible;
	}

	public function getPropertyApplicationPreferences() {
		return $this->m_arrobjPropertyApplicationPreferences;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	/**
	 * Set Functions
	 */

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setWebVisible( $boolWebVisible ) {
		$this->m_boolWebVisible = $boolWebVisible;
	}

	public function setPropertyApplicationPreferences( $arrobjPropertyApplicationPreferences ) {
		$this->m_arrobjPropertyApplicationPreferences = $arrobjPropertyApplicationPreferences;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = CStrings::strToIntDef( $intOccupancyTypeId, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

        if( true == isset( $arrmixValues['title'] ) ) $this->setTitle( stripslashes( $arrmixValues['title'] ) );
        if( true == isset( $arrmixValues['web_visible'] ) ) $this->setWebVisible( stripslashes( $arrmixValues['web_visible'] ) );
        if( true == isset( $arrmixValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );

        return;
    }

    /**
     * Fetch Functions
     */

    public function createPropertyApplicationPreference() {

		$objPropertyApplicationPreference = new CPropertyApplicationPreference();
		$objPropertyApplicationPreference->setCid( $this->getCid() );
		$objPropertyApplicationPreference->setPropertyId( $this->getPropertyId() );
		$objPropertyApplicationPreference->setCompanyApplicationId( $this->getCompanyApplicationId() );
		$objPropertyApplicationPreference->setApplicationPreferenceTypeId( CApplicationPreferenceType::TYPE_GLOBAL );

		return $objPropertyApplicationPreference;
    }

    public function loadApplicationPreferenceGroups() {

    	$arrstrApplicationPreferenceGroups = [];

    	if( false == valId( $this->getOccupancyTypeId() ) )
    		return $arrstrApplicationPreferenceGroups;

    	switch( $this->getOccupancyTypeId() ) {
    		case COccupancyType::AFFORDABLE:
    			$arrstrApplicationPreferenceGroups = [
	    			'member'		=> __( 'Member' ),
	    			'unit_info'		=> __( 'Unit Info' ),
	    			'addl_info'		=> __( 'Addl. Info' ),
	    			'options'		=> __( 'Options' ),
	    			'contacts'		=> __( 'Contacts' ),
	    			'questionnaire'	=> __( 'Questionnaire' ),
	    			'general'		=> __( 'General' )
			    ];
    			break;

		    case COccupancyType::MILITARY:
			    $arrstrApplicationPreferenceGroups = [
				    'unit_info'     => __( 'Unit Info' ),
				    'options'       => __( 'Options' ),
				    'basic_info'    => __( 'Basic Info' ),
				    'addl_info'     => __( 'Addl. Info' ),
				    'roommates'     => __( 'Roommates' ),
				    'financial'     => __( 'Financial' ),
				    'people'        => __( 'People' ),
				    'contacts'      => __( 'Contacts' ),
				    'questionnaire' => __( 'Questionnaire' ),
				    'summary'       => __( 'Summary' ),
				    'payment'       => __( 'Payment' ),
				    'military_info' => __( 'Military' ),
				    'general'       => __( 'General' )
			    ];
			    break;

    		case COccupancyType::CONVENTIONAL:
            default:
                $arrstrApplicationPreferenceGroups = [
                    'unit_info'     => __( 'Unit Info' ),
                    'options'       => __( 'Options' ),
                    'basic_info'    => __( 'Basic Info' ),
                    'addl_info'     => __( 'Addl. Info' ),
                    'roommates'     => __( 'Roommates' ),
                    'financial'     => __( 'Financial' ),
                    'people'        => __( 'People' ),
                    'contacts'      => __( 'Contacts' ),
                    'questionnaire' => __( 'Questionnaire' ),
                    'summary'       => __( 'Summary' ),
                    'payment'       => __( 'Payment' ),
                    'general'       => __( 'General' )
                ];
                break;
    	}

    	return $arrstrApplicationPreferenceGroups;
    }

    /**
     * Fetch Functions
     */

    public function fetchPropertyApplicationPreferences( $objDatabase ) {
		if ( true == is_null( $this->getCompanyApplicationId() ) ) return NULL;
		if ( true == is_null( $this->getPropertyId() ) ) return NULL;

		$this->m_arrobjPropertyApplicationPreferences = CPropertyApplicationPreferences::fetchPropertyApplicationPreferencesByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $this->getCompanyApplicationId(), $this->getPropertyId(), CApplicationPreferenceType::TYPE_GLOBAL, $this->getCid(), $objDatabase );

		return $this->m_arrobjPropertyApplicationPreferences;
	}

	public function fetchPropertyApplicationPreferencesByKeys( $arrstrKeys, $objDatabase ) {
		return  CPropertyApplicationPreferences::fetchPropertyApplicationPreferencesByPropertyIdByCompanyApplicationIdByKeysByApplicationPreferenceTypeIdByCid( $this->getPropertyId(), $this->getCompanyApplicationId(), $arrstrKeys,  CApplicationPreferenceType::TYPE_GLOBAL, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyApplicationPreferencesByApplicationPreferenceTypeIdByKey( $intApplicationPreferenceTypeId, $strKey, $objDatabase ) {
		return  CPropertyApplicationPreferences::fetchPropertyApplicationPreferenceByKeyByCompanyApplicationIdByPropertyIdByApplicationPreferenceTypeIdByCid( $strKey, $this->getCompanyApplicationId(), $this->getPropertyId(), $intApplicationPreferenceTypeId, $this->getCid(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getPropertyId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyApplicationId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCompanyApplicationId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_application_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getOrderNum() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validateOccupancyTypeId( $arrmixProperties, $intCid, $objDatabase ) {

        $boolIsValid = true;

	    $arrstrNonAffordableProperties = NULL;

	    foreach( $arrmixProperties as $arrmixProperty ) {
		    if( CPropertyType::SUBSIDIZED != $arrmixProperty['property_type_id'] ) {
			    $arrstrNonAffordableProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
			    $boolIsValid &= false;
		    }
	    }

	    if( false == $boolIsValid ) {
		    $this->addValidationErrorMessage( $arrstrNonAffordableProperties, 'property type subsidized.' );
		    return $boolIsValid;
	    }

	    // check if affordable product exists
	    $arrobjAffordableProperties = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchAffordablePropertiesByCid( $intCid, $objDatabase, array_keys( $arrmixProperties ) );

	    foreach( $arrmixProperties as $arrmixProperty ) {
		    if( false == array_key_exists( $arrmixProperty['id'], $arrobjAffordableProperties ) ) {
			    $arrstrNonAffordableProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
			    $boolIsValid &= false;
		    }
	    }

	    if( false == $boolIsValid ) {
		    $this->addValidationErrorMessage( $arrstrNonAffordableProperties, 'affordable product associated or is not configured for affordable.' );
		    return $boolIsValid;
	    }

	    // check if HMFA code exists
	    $arrobjPropertySubsidyDetails   = rekeyObjects( 'PropertyId', \Psi\Eos\Entrata\CPropertySubsidyDetails::createService()->fetchPropertySubsidyDetailsByPropertyIdsByCid( array_keys( $arrmixProperties ), $intCid, $objDatabase ) );
	    foreach( $arrmixProperties as $arrmixProperty ) {
		    if( false == valArr( $arrobjPropertySubsidyDetails ) || false == array_key_exists( $arrmixProperty['id'], $arrobjPropertySubsidyDetails ) ) {
			    $arrstrNonAffordableProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
			    $boolIsValid &= false;
		    }
	    }

	    if( false == $boolIsValid ) {
		    $this->addValidationErrorMessage( $arrstrNonAffordableProperties, 'metro area associated.' );
		    return $boolIsValid;
	    }

	    return $boolIsValid;
    }

    public function validateMilitaryProperty( $arrmixProperties, $intCid, $objDatabase ) {
    	$boolIsValid = true;

	    // check if military product exists
	    $arrobjMilitaryProperties = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchMilitaryPropertiesByCid( $intCid, $objDatabase, array_keys( $arrmixProperties ) );

	    foreach( $arrmixProperties as $arrmixProperty ) {
		    if( false == array_key_exists( $arrmixProperty['id'], $arrobjMilitaryProperties ) ) {
			    $arrstrNonMilitaryProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
			    $boolIsValid &= false;
		    }
	    }

	    if( false == $boolIsValid ) {
		    $this->addValidationErrorMessage( $arrstrNonMilitaryProperties, ' the Military product enabled.' );
		    return $boolIsValid;
	    }

	    return $boolIsValid;
    }

	public function validateStudentProperty( $arrmixProperties, $intCid, $objDatabase ) {
		$boolIsValid = true;

		$arrobjPmEnabledProperties = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchActivePmEnabledPropertiesByCid( $intCid, $objDatabase, array_keys( $arrmixProperties ) );

		$arrstrNonPmEnabledProperties = [];
		foreach( $arrmixProperties as $arrmixProperty ) {
			if( false == array_key_exists( $arrmixProperty['id'], $arrobjPmEnabledProperties ) ) {
				$arrstrNonPmEnabledProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
				$boolIsValid &= false;
			}
		}

		if( false == $boolIsValid ) {
			$this->addValidationErrorMessage( $arrstrNonPmEnabledProperties, ' Entrata core product enabled.' );
			return $boolIsValid;
		}

		$arrstrInvalidOccupancyProperties = [];
		foreach( $arrmixProperties as $arrmixProperty ) {
			if( false == in_array( COccupancyType::STUDENT, $arrobjPmEnabledProperties[$arrmixProperty['id']]->getOccupancyTypeIds() ) ) {
				$arrstrInvalidOccupancyProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
				$boolIsValid &= false;
			}
		}

		if( false == $boolIsValid ) {
			$this->addValidationErrorMessage( $arrstrInvalidOccupancyProperties, ' Occupancy type Student.' );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validateHospitalityProperty( $arrmixProperties, $intCid, $objDatabase ) {
		$boolIsValid = true;

		$arrobjHospitalityProperties = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchActivePmEnabledPropertiesByCid( $intCid, $objDatabase, array_keys( $arrmixProperties ) );
		$arrstrInvalidHospitalityProperties = [];
		foreach( $arrmixProperties as $arrmixProperty ) {
			if( false == array_key_exists( $arrmixProperty['id'], $arrobjHospitalityProperties ) ) {
				$arrstrInvalidHospitalityProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
				$boolIsValid &= false;
			}
		}

		if( false == $boolIsValid ) {
			$this->addValidationErrorMessage( $arrstrInvalidHospitalityProperties, ' the Hospitality product enabled.' );
			return $boolIsValid;
		}

		$arrstrInvalidOccupancyProperties = [];
		foreach( $arrmixProperties as $arrmixProperty ) {
			if( false == in_array( COccupancyType::HOSPITALITY, $arrobjHospitalityProperties[$arrmixProperty['id']]->getOccupancyTypeIds() ) ) {
				$arrstrInvalidOccupancyProperties[$arrmixProperty['id']] = $arrmixProperty['property_name'];
				$boolIsValid &= false;
			}
		}

		if( false == $boolIsValid ) {
			$this->addValidationErrorMessage( $arrstrInvalidOccupancyProperties, ' Occupancy type hospitality.' );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

    public function addValidationErrorMessage( $arrmixProperties, $strMessageItem ) {
	    if( 5 < \Psi\Libraries\UtilFunctions\count( $arrmixProperties ) ) {
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Unable to associate Application, there are more than 5 properties that do not have the {%s,0}', [ $strMessageItem ] ) ) );
	    } elseif( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixProperties ) ) {
		    $strMessage = 'Unable to associate Application, ' . implode( ', ', $arrmixProperties ) . ' does not have the ' . $strMessageItem;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( '{%s,0}', [ $strMessage ] ) ) );
	    }
    }

	public function fetchPropertyCountByIdByPsProductIdByPsProductOptionIdsByCid( $intPropertyId, $intPsProductId, $arrintPsProductOptionIds, $intCid, $objDatabase, $intPropertyTypeId = NULL ) {
		$strSql = 'SELECT
						COUNT( DISTINCT( p.id ) ) AS property_count
					FROM
						properties p
						JOIN property_products pp ON ( pp.cid = p.cid AND ( pp.property_id = p.id OR pp.property_id IS NULL ) )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND	p.is_disabled = 0
						AND p.termination_date IS NULL
						AND p.id = ' . ( int ) $intPropertyId . '
						AND pp.ps_product_id = ' . ( int ) $intPsProductId . '
						AND pp.ps_product_option_id IN ( ' . implode( ',', $arrintPsProductOptionIds ) . ' ) ' .
		 ( ( true == valId( $intPropertyTypeId ) ) ? ' AND p.property_type_id = ' . ( int ) $intPropertyTypeId : '' );

		$arrstrTempPropertyProductsData = fetchData( $strSql, $objDatabase );

		return ( 0 < $arrstrTempPropertyProductsData[0]['property_count'] ) ? true : false;
	}

    public function validate( $strAction, $arrmixProperties = [], $intCid = NULL, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            case 'validate_subsidized_property':
                $boolIsValid = $this->validateOccupancyTypeId( $arrmixProperties, $intCid, $objDatabase );
                break;

	        case 'validate_military_property':
		        $boolIsValid = $this->validateMilitaryProperty( $arrmixProperties, $intCid, $objDatabase );
		        break;

	        case 'validate_student_property':
		        $boolIsValid = $this->validateStudentProperty( $arrmixProperties, $intCid, $objDatabase );
		        break;

	        case 'validate_hospitality_property':
		        $boolIsValid = $this->validateHospitalityProperty( $arrmixProperties, $intCid, $objDatabase );
		        break;

            default:
            	$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

}
?>