<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSplitBillingTypes
 * Do not add any new functions to this class.
 */

class CSplitBillingTypes extends CBaseSplitBillingTypes {

    public static function fetchSplitBillingTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CSplitBillingType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchSplitBillingType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CSplitBillingType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>