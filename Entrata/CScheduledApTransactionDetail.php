<?php

class CScheduledApTransactionDetail extends CBaseScheduledApTransactionDetail {

	protected $m_intApPayeeId;
	protected $m_intIsAllocation;
	protected $m_intGlAccountTypeId;
	protected $m_intIsDisabledProperty;

	protected $m_strUnitNumber;
	protected $m_strPropertyName;
	protected $m_strGlAccountName;
	protected $m_strGlAccountNumber;
	protected $m_strInvoiceAllocationName;

	protected $m_arrobjProperties;
	protected $m_arrobjPropertyUnits;
	protected $m_arrintAssociatedApPayeePropertyIds;

	/**
	 * Get Functions
	 */

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getGlAccountNumber() {
		return $this->m_strGlAccountNumber;
	}

	public function getIsDisabledProperty() {
		return $this->m_intIsDisabledProperty;
	}

	public function getIsAllocation() {
		return $this->m_intIsAllocation;
	}

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function getInvoiceAllocationName() {
		return $this->m_strInvoiceAllocationName;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getPropertyUnits() {
		return $this->m_arrobjPropertyUnits;
	}

	/**
	 * Set Functions
	 */

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = $strGlAccountName;
	}

	public function setGlAccountNumber( $strGlAccountNumber ) {
		$this->m_strGlAccountNumber = $strGlAccountNumber;
	}

	public function setIsDisabledProperty( $intIsDisabledProperty ) {
		$this->m_intIsDisabledProperty = $intIsDisabledProperty;
	}

	public function setIsAllocation( $intIsAllocation ) {
		$this->m_intIsAllocation = $intIsAllocation;
	}

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->m_intGlAccountTypeId = $intGlAccountTypeId;
	}

	public function setInvoiceAllocationName( $strInvoiceAllocationName ) {
		$this->m_strInvoiceAllocationName = $strInvoiceAllocationName;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = CStrings::strTrimDef( $strUnitNumber, 50, NULL, true );
	}

	public function setPropertyUnits( $arrobjPropertyUnits ) {
		$this->m_arrobjPropertyUnits = $arrobjPropertyUnits;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( $arrValues['unit_number'] );
		if( true == isset( $arrValues['is_allocation'] ) ) $this->setIsAllocation( $arrValues['is_allocation'] );
		if( true == isset( $arrValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrValues['gl_account_type_id'] );
		if( true == isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( true == isset( $arrValues['gl_account_name'] ) ) $this->setGlAccountName( $arrValues['gl_account_name'] );
		if( true == isset( $arrValues['gl_account_number'] ) ) $this->setGlAccountNumber( $arrValues['gl_account_number'] );
		if( true == isset( $arrValues['is_disabled_property'] ) ) $this->setIsDisabledProperty( $arrValues['is_disabled_property'] );
		if( true == isset( $arrValues['invoice_allocation_name'] ) ) $this->setInvoiceAllocationName( $arrValues['invoice_allocation_name'] );

		return;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valGlAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getGlAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', 'GL account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valQuantity() {
		$boolIsValid = true;

		if( true == is_null( $this->getQuantity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', 'Quantity is required.' ) );
		} elseif( 0 == $this->getQuantity() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quantity', 'Quantity should be greater than or equal to one.' ) );
		}

		return $boolIsValid;
	}

	public function valRate() {
		$boolIsValid = true;

		if( true == is_null( $this->getRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Rate is required.' ) );
		} elseif( 0 == $this->getRate() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rate', 'Rate cannot be zero.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId() {

		$boolIsValid = true;

		if( false == is_null( $this->getPropertyUnitId() ) && true == valArr( $this->getPropertyUnits() ) && false == array_key_exists( $this->getPropertyUnitId(), $this->getPropertyUnits() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Unit "' . $this->getUnitNumber() . '" is not associated with property "' . $this->getPropertyName() . '".' ) );

			// setting prpoerty_unit_id and unit_number to NULL after validating the deleted property_unit.
			$this->setUnitNumber( NULL );
			$this->setPropertyUnitId( NULL );
		}

		return $boolIsValid;
	}

	public function valTransactionAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'Transaction amount is required.' ) );
		} elseif( 1000000000 < abs( $this->getTransactionAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transaction_amount', 'Line item amount must be between -$1,000,000,000 and $1,000,000,000.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valGlAccountId();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valQuantity();
				$boolIsValid &= $this->valRate();
				$boolIsValid &= $this->valTransactionAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>