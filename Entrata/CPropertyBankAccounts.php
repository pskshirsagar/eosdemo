<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyBankAccounts
 * Do not add any new functions to this class.
 */

class CPropertyBankAccounts extends CBasePropertyBankAccounts {

	public static function fetchPropertyIdsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase, $boolIsPrimary = false ) {

		$strWhereCondition = '';

		if( true == $boolIsPrimary ) {
			$strWhereCondition .= ' AND is_primary = true';
		}

		$strSql = 'SELECT
						property_id
					FROM
						property_bank_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bank_account_id = ' . ( int ) $intBankAccountId . $strWhereCondition;

		$arrmixData = ( array ) fetchData( $strSql, $objClientDatabase );
		$arrintPropertyIds = NULL;

		foreach( $arrmixData as $arrintPropertyId ) {
			$arrintPropertyIds[$arrintPropertyId['property_id']] = $arrintPropertyId['property_id'];
		}

		return $arrintPropertyIds;
	}

	public static function fetchActivePropertyBankAccountsByBankAccountIdsByCid( $arrintBankAccountIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintBankAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pba.*,
						ac.item_gl_account_id AS gl_account_id
					FROM
						bank_accounts ba
						JOIN property_bank_accounts pba ON ( ba.cid = pba.cid AND ba.id = pba.bank_account_id )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.bank_account_id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' )
						AND ba.is_disabled = 0
						AND pba.is_primary = true';

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchCountActivePropertyBankAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						COUNT( ba.id )
					FROM
						bank_accounts ba,
						property_bank_accounts pba
					WHERE
						ba.id = pba.bank_account_id
						AND ba.cid = pba.cid
						AND pba.cid = ' . ( int ) $intCid . '
						AND ba.is_disabled = 0
						AND pba.is_primary = true';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchCountActivePropertyBankAccountsByGlAccountIdByCid( $intGlAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						COUNT( ba.id )
					FROM
						bank_accounts ba
						JOIN property_bank_accounts pba ON ( ba.cid = pba.cid AND ba.id = pba.bank_account_id )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND ac.item_gl_account_id = ' . ( int ) $intGlAccountId . '
						AND ba.is_disabled = 0';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchCountDisabledPropertyBankAccountsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						COUNT( ga.id )
					FROM
						gl_accounts ga
						LEFT JOIN ap_codes ac ON ( ga.cid = ac.cid AND ga.id = ac.item_gl_account_id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN property_bank_accounts pba ON ( ac.cid = pba.cid AND ac.id = pba.bank_ap_code_id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.bank_account_id = ' . ( int ) $intBankAccountId . '
						AND ga.disabled_by IS NOT NULL';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchActivePropertyBankAccountsByCid( $intCid, $objClientDatabase, $boolCheckIsPrimary = true ) {

		$strCondition = ( false == $boolCheckIsPrimary ) ? '' : ' AND pba.is_primary = true';

		$strSql = 'SELECT
						ba.*,
    					pba.*,
						ac.item_gl_account_id AS gl_account_id
					FROM
						bank_accounts ba
						JOIN property_bank_accounts pba ON ( pba.cid = ba.cid AND ba.id = pba.bank_account_id )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						ba.id = pba.bank_account_id
						AND pba.cid = ' . ( int ) $intCid . '
						AND ba.is_disabled = 0
						' . $strCondition;

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyBankAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pba.*,
						CASE
							WHEN ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . ' THEN
								( SELECT
										ac1.item_gl_account_id
									FROM
										property_bank_accounts pba1
										LEFT JOIN ap_codes ac1 ON ( pba1.cid = ac1.cid AND pba1.bank_ap_code_id = ac1.id AND ac1.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
									WHERE
										pba1.cid = ' . ( int ) $intCid . '
										AND pba1.bank_account_id = pba.bank_account_id
										AND pba1.cid = pba.cid
										AND	pba1.is_reimbursed_property = true
										AND pba1.property_id = ba.reimbursed_property_id
								)
							ELSE
								ac.item_gl_account_id
						END AS bank_gl_account_id
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN bank_accounts ba ON ( ba.cid = pba.cid AND pba.bank_account_id = ba.id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.is_primary = true';

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchActivePropertyBankAccountByPropertyIdByBankAccountIdByCid( $intPropertyId, $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pba.*,
						ac.item_gl_account_id AS gl_account_id
					FROM
						bank_accounts ba
						JOIN property_bank_accounts pba ON ( ba.cid = pba.cid AND ba.id = pba.bank_account_id )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.property_id = ' . ( int ) $intPropertyId . '
						AND pba.bank_account_id = ' . ( int ) $intBankAccountId . '
						AND ( CASE
								WHEN ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . ' THEN
									pba.is_reimbursed_property = true
								ELSE
									pba.is_reimbursed_property = false
								END
							)
						AND pba.is_primary = true
					LIMIT 1';

		return self::fetchPropertyBankAccount( $strSql, $objClientDatabase );
	}

	public static function fetchActivePropertyBankAccountByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pba.*,
						ac.item_gl_account_id AS gl_account_id
					FROM
						property_bank_accounts pba
						JOIN bank_accounts ba ON ( ba.cid = pba.cid AND pba.bank_account_id = ba.id )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.property_id = ' . ( int ) $intPropertyId . '
						AND pba.is_primary = true
					ORDER BY
						( CASE
							WHEN ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . ' THEN
								pba.is_reimbursed_property = true
							ELSE
								pba.is_reimbursed_property = false
							END
						)
					LIMIT 1';

		return self::fetchPropertyBankAccount( $strSql, $objClientDatabase );
	}

	public static function fetchCashGlAccountsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase, $boolIsCreditCardAccount = false ) {

		$strCondition = ' AND ga.gl_account_type_id IN ( ' . CGlAccountType::ASSETS . ' )
						AND ga.gl_account_usage_type_id IN ( ' . CGlAccountUsageType::BANK_ACCOUNT . ' )';

		if( true == $boolIsCreditCardAccount ) {
			$strCondition = ' AND ga.gl_account_type_id IN ( ' . CGlAccountType::LIABILITIES . ' ) 
								AND ga.gl_account_usage_type_id IN ( ' . CGlAccountUsageType::CREDIT_CARD . ' )';
		}

		$strSql = 'SELECT
						ac.item_gl_account_id AS gl_account_id,
						pba.property_id
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN gl_accounts ga ON ( ac.cid = ga.cid AND ac.item_gl_account_id = ga.id )
						JOIN bank_accounts ba ON ( ba.cid = pba.cid AND pba.bank_account_id = ba.id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.bank_account_id = ' . ( int ) $intBankAccountId . '
						AND ( CASE
								WHEN ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . ' THEN
									pba.is_reimbursed_property = true
								ELSE
									pba.is_reimbursed_property = false
								END
							)' . $strCondition;

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyBankAccountsByBankAccountIdsByCid( $arrintBankAccountIds, $intCid, $objClientDatabase, $boolIsPrimary = false, $boolIsReimbursedProperty = false ) {

		// This query will return property_bank_accounts records along with bank_gl_account_id(Cash GL Account) for each bank account and its property.

		$strWhereCondition = '';

		if( false == valIntArr( $arrintBankAccountIds ) ) {
			return NULL;
		}

		if( true == $boolIsPrimary ) {
			$strWhereCondition .= ' AND pba.is_primary = true';
		}

		if( true == $boolIsReimbursedProperty ) {
			$strWhereCondition .= ' AND pba.is_reimbursed_property = true AND pba.property_id = ba.reimbursed_property_id';
		}

		$strSql = 'SELECT
						pba.*,
						CASE
							WHEN
								ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . '
							THEN
								( SELECT
										ac1.item_gl_account_id
									FROM
										property_bank_accounts pba1
										LEFT JOIN ap_codes ac1 ON ( pba1.cid = ac1.cid AND pba1.bank_ap_code_id = ac1.id AND ac1.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
									WHERE
										pba1.bank_account_id = pba.bank_account_id
										AND pba1.cid = pba.cid
										AND pba1.is_reimbursed_property = true
										AND pba1.property_id = ba.reimbursed_property_id
										AND pba1.cid = ' . ( int ) $intCid . '
								)
							ELSE
								ac.item_gl_account_id
						END AS bank_gl_account_id,
						ba.bank_account_type_id,
						ba.is_use_subledgers,
						CASE
							WHEN
								ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . '
							THEN
								(
									SELECT 
										pba2.property_id
									FROM
										bank_accounts ba1
										JOIN property_bank_accounts pba2 ON( ba1.cid = pba2.cid AND ba1.id = pba2.bank_account_id )
									WHERE
										pba2.cid = ' . ( int ) $intCid . '
										AND ba1.cid = ba.cid
										AND ba1.id = ba.id
										AND pba2.property_id = ba.reimbursed_property_id
										AND pba2.is_reimbursed_property = true
									)
							ELSE
								NULL
						END AS inter_co_property_id
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN bank_accounts ba ON ( ba.cid = pba.cid AND pba.bank_account_id = ba.id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.bank_account_id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' ) ' . $strWhereCondition;

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyBankAccountsByPropertyIdsByGlAccountIdByCid( $arrintPropertyIds, $intGlAccountId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pba.*,
						ac.item_gl_account_id AS gl_account_id,
						ba.account_name AS bank_account_name
					FROM
						property_bank_accounts pba
						JOIN bank_accounts ba ON ( pba.cid = ba.cid AND pba.bank_account_id = ba.id )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND ac.item_gl_account_id = ' . ( int ) $intGlAccountId . '
						AND pba.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pba.is_primary = true';

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyBankAccountsByPropertyIdsAndGlAccountIdsByCid( $arrmixPropertyIdsAndGlAccountIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrmixPropertyIdsAndGlAccountIds ) ) return NULL;

		$strSql = 'SELECT
						pba.*,
						ac.item_gl_account_id AS gl_account_id,
						ba.account_name AS bank_account_name
					FROM
						property_bank_accounts pba
						JOIN bank_accounts ba ON ( pba.cid = ba.cid AND pba.bank_account_id = ba.id )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND ( pba.property_id, ac.item_gl_account_id ) IN ( ' . implode( ', ', $arrmixPropertyIdsAndGlAccountIds ) . ' )';

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyBankAccountsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $boolIsPrimary = false ) {

		$strWhereCondition = '';

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( true == $boolIsPrimary ) {
			$strWhereCondition .= ' AND pba.is_primary = true';
		}

		$strSql = 'SELECT
						pba.*,
						ac.item_gl_account_id AS gl_account_id
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strWhereCondition;

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchActivePropertyBankAccountsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $boolIsSetupReport = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pba.*,
						ba.account_name AS bank_account_name,
						ba.bank_account_type_id,
						ba.is_credit_card_account as is_credit_card_account,
						ac.item_gl_account_id AS gl_account_id
					FROM
						bank_accounts ba
						JOIN property_bank_accounts pba ON ( ba.cid = pba.cid AND ba.id = pba.bank_account_id )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						ba.cid = ' . ( int ) $intCid . '
						AND ba.is_disabled = 0
						AND pba.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pba.is_primary = true';

		if( true == $boolIsSetupReport ) {
			return fetchData( $strSql, $objClientDatabase );
		} else {
			return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
		}
	}

	public static function fetchPropertyBankAccountsCountByGlAccountIdNotByGlAccountUsageTypeIdByCid( $intGlAccountId, $intGlAccountUsageTypeId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intGlAccountId ) || false == is_numeric( $intGlAccountUsageTypeId ) || false == is_numeric( $intCid ) ) {
			return 0;
		}

		$strSql = 'SELECT
						COUNT( pba.id )
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN gl_accounts ga ON ( pba.cid = ga.cid AND ac.item_gl_account_id = ga.id )
						JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND ac.item_gl_account_id = ' . ( int ) $intGlAccountId . '
						AND gaut.id <> ' . ( int ) $intGlAccountUsageTypeId;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchPropertyBankAccountsByOldAndNewGlAccountIdByCid( $intOldGlAccountId, $intNewGlAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pba1.*,
						ba.is_disabled,
						ac.item_gl_account_id AS gl_account_id
					FROM
						property_bank_accounts pba1
						JOIN bank_accounts ba ON ( pba1.cid = ba.cid AND pba1.bank_account_id = ba.id )
						LEFT JOIN ap_codes ac ON ( pba1.cid = ac.cid AND pba1.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba1.cid = ' . ( int ) $intCid . '
						AND ac.item_gl_account_id = ' . ( int ) $intNewGlAccountId . '
						AND EXISTS (
										SELECT
											1
										FROM
											property_bank_accounts pba2
											LEFT JOIN ap_codes ac1 ON ( pba2.cid = ac1.cid AND pba2.bank_ap_code_id = ac1.id AND ac1.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . '  )
										WHERE
											pba2.cid = ' . ( int ) $intCid . '
											AND ac1.item_gl_account_id = ' . ( int ) $intOldGlAccountId . '
											AND pba1.property_id = pba2.property_id
									)
						AND ba.is_disabled = 0
						AND pba1.is_primary = true';

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchEndingGlBalanceByPropertyIdsAndByGlAccountIdsByPostDateOrByPostMonthByCid( $arrmixPropertyIdAndGlAccountIds, $strPostDate, $boolIsBankRecOnPostMonth, $intCid, $objClientDatabase, $boolIsCreditCardReconciliation = false, $strBeginningDate = '', $boolIsCcBankRecFromConsumer = false, $intBankAccountId = NULL ) {

		if( false == valArr( $arrmixPropertyIdAndGlAccountIds ) ) return NULL;

		$strSelect = '';
		$strGroupBy = '';
		$arrintExcludedGlHeaderStatusTypeIds = CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes;

		$strCondition = 'AND gh.post_date <= \'' . $strPostDate . '\'';

		if( true == $boolIsBankRecOnPostMonth ) {
			$strCondition = 'AND gh.post_month <= \'' . $strPostDate . '\'';
		}

		if( true == $boolIsCreditCardReconciliation ) {
			$arrintExcludedGlHeaderStatusTypeIds[] = CGlHeaderStatusType::REVERSED;
			$strCondition .= ( true == valStr( $strBeginningDate ) && false == $boolIsBankRecOnPostMonth ) ? 'AND gh.post_date >= \'' . $strBeginningDate . '\'' : '';
			$strCondition .= ' AND ga.gl_account_type_id IN ( ' . CGlAccountType::LIABILITIES . ' )
								AND ga.gl_account_usage_type_id IN ( ' . CGlAccountUsageType::CREDIT_CARD . ' )';

			if( true == $boolIsCcBankRecFromConsumer ) {
				$strSelect = ', gd.property_id';
				$strGroupBy = 'GROUP BY
								gd.property_id';
			}
		} else {
			$strCondition .= ' AND ga.gl_account_type_id IN ( ' . CGlAccountType::ASSETS . ' )
								AND ga.gl_account_usage_type_id IN ( ' . CGlAccountUsageType::BANK_ACCOUNT . ' )';
		}

		if( true == valId( $intBankAccountId ) ) {
			$strCondition .= ' AND ba.id = ' . $intBankAccountId;
		}

		$strSql = 'SELECT
						SUM( gd.amount ) AS opening_balance
						' . $strSelect . '
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN bank_accounts ba ON ( pba.cid = ba.cid AND pba.bank_account_id = ba.id )
						JOIN gl_accounts ga ON ( ba.cid = ga.cid AND ac.item_gl_account_id = ga.id )
						JOIN gl_details gd ON ( ga.cid = gd.cid AND pba.property_id = gd.property_id )
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND ( pba.property_id, ac.item_gl_account_id ) IN ( ' . implode( ', ', $arrmixPropertyIdAndGlAccountIds ) . ' ) '
						. $strCondition . '
						AND gh.gl_header_status_type_id NOT IN ( ' . sqlIntImplode( $arrintExcludedGlHeaderStatusTypeIds ) . ' )
						AND ba.is_disabled = 0
						AND gh.is_template = false
						AND ( ( ba.is_cash_basis = 0 AND ac.item_gl_account_id = gd.accrual_gl_account_id )
								OR ( ba.is_cash_basis = 1 AND ac.item_gl_account_id = gd.cash_gl_account_id ) )
						AND
							(
								( ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . ' AND pba.is_reimbursed_property = true AND pba.property_id = ba.reimbursed_property_id )
								OR ( ba.bank_account_type_id IN ( ' . CBankAccountType::SINGLE_PROPERTY . ', ' . CBankAccountType::SHARED . ' ) AND pba.is_reimbursed_property = false )
							)
					' . $strGroupBy;

		$arrstrData = ( array ) fetchData( $strSql, $objClientDatabase );

		if( true == $boolIsCcBankRecFromConsumer ) {
			return ( true == valArr( $arrstrData ) ) ? $arrstrData: [];
		} else {
			return ( true == valArr( $arrstrData ) ) ? $arrstrData[0]['opening_balance'] : 0;
		}
	}

	public static function fetchSimplePropertyBankAccountsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pba.property_id,
						ac.item_gl_account_id AS gl_account_id
					FROM
						property_bank_accounts pba
						JOIN bank_accounts ba ON ( pba.cid = ba.cid AND pba.bank_account_id = ba.id AND ba.is_disabled = 0 )
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
					WHERE
						pba.bank_account_id  = ' . ( int ) $intBankAccountId . '
						AND
							(
								( ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . ' AND pba.is_reimbursed_property = true AND pba.property_id = ba.reimbursed_property_id )
								OR ( ba.bank_account_type_id IN ( ' . CBankAccountType::SINGLE_PROPERTY . ', ' . CBankAccountType::SHARED . ' ) AND pba.is_reimbursed_property = false )
							)
						AND pba.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCurrentGlBalanceByPropertyIdsAndByGlAccountIdsByCid( $arrmixPropertyIdAndGlAccountIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrmixPropertyIdAndGlAccountIds ) ) return NULL;

		$strSql = 'SELECT
						SUM( gd.amount ) AS current_balance,
						pba.bank_account_id
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN bank_accounts ba ON ( pba.cid = ba.cid AND pba.bank_account_id = ba.id )
						JOIN gl_accounts ga ON ( ba.cid = ga.cid AND ac.item_gl_account_id = ga.id )
						JOIN gl_details gd ON ( ga.cid = gd.cid AND pba.property_id = gd.property_id )
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_transaction_type_id = gh.gl_transaction_type_id AND gd.post_month = gh.post_month AND gd.gl_header_id = gh.id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND ga.gl_account_type_id = ' . CGlAccountType::ASSETS . '
						AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::BANK_ACCOUNT . '
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
						AND ba.is_disabled = 0
						AND ( pba.property_id, ac.item_gl_account_id ) IN ( ' . implode( ', ', $arrmixPropertyIdAndGlAccountIds ) . ' )
						AND ( ( ba.is_cash_basis = 0 AND ac.item_gl_account_id = gd.accrual_gl_account_id )
								OR ( ba.is_cash_basis = 1 AND ac.item_gl_account_id = gd.cash_gl_account_id ) )
						AND gh.is_template = false
						AND pba.is_primary = true
					GROUP BY
						pba.bank_account_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	/*
	 * @deprecated Use CBankAccounts::fetchBankAccountsWithCurrentGlBalanceByIdsByCid instead
	 */

	public static function fetchCurrentGlBalanceByBankAccountIdsByCid( $arrintBankAccountIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintBankAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT pba.bank_account_id,
						SUM ( gd.amount ) OVER ( PARTITION BY pba.bank_account_id ) AS current_balance
					FROM
						property_bank_accounts pba
						JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' AND ac.item_gl_account_id IS NOT NULL )
						JOIN bank_accounts ba ON ( pba.cid = ba.cid AND pba.bank_account_id = ba.id )
						JOIN gl_accounts ga ON ( ba.cid = ga.cid AND ac.item_gl_account_id = ga.id )
						JOIN gl_details gd ON ( ga.cid = gd.cid AND pba.property_id = gd.property_id )
						JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_transaction_type_id = gh.gl_transaction_type_id AND gd.post_month = gh.post_month AND gd.gl_header_id = gh.id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND ga.gl_account_type_id = ' . CGlAccountType::ASSETS . '
						AND ga.gl_account_usage_type_id = ' . CGlAccountUsageType::BANK_ACCOUNT . '
						AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
						AND ba.is_disabled = 0
						AND pba.bank_account_id IN ( ' . sqlIntImplode( $arrintBankAccountIds ) . ' )
						AND (
								( ba.is_cash_basis = 0 AND ac.item_gl_account_id = gd.accrual_gl_account_id )
								OR
								( ba.is_cash_basis = 1 AND ac.item_gl_account_id = gd.cash_gl_account_id )
							)
						AND NOT gh.is_template
						AND pba.is_primary';

		$arrmixBalance = ( array ) fetchData( $strSql, $objClientDatabase );

		return rekeyArray( 'bank_account_id', $arrmixBalance );
	}

	public static function fetchPropertyBankAccountsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase, $boolIsPrimary = false, $boolIsReimbursedProperty = false, $boolIsFromEditBankAccount = false ) {

		$strWhereCondition = '';
		$strJoinCondition = '';

		if( true == $boolIsFromEditBankAccount ) {
			$strJoinCondition = ' JOIN properties AS p on( pba.cid = p.cid AND pba.property_id = p.id AND p.is_disabled = 0)';
		}

		if( true == $boolIsPrimary ) {
			$strWhereCondition .= ' AND pba.is_primary = true';
		}

		if( true == $boolIsReimbursedProperty ) {
			$strWhereCondition .= ' AND pba.is_reimbursed_property = true AND pba.property_id = ba.reimbursed_property_id';
		}

		$strSql = 'SELECT
						pba.*,
						ba.bank_account_type_id,
						ac.item_gl_account_id AS gl_account_id
					FROM
						property_bank_accounts pba
						' . $strJoinCondition . '
						JOIN bank_accounts ba ON ba.cid = pba.cid AND ba.id = pba.bank_account_id
						LEFT JOIN ap_codes ac ON pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.bank_account_id = ' . ( int ) $intBankAccountId . $strWhereCondition;

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchCustomPropertyBankAccountsByBankAccountIdsByCid( $arrintBankAccountIds, $intCid, $objClientDatabase, $boolIsCreditCardAccount = false ) {

		if( false == valArr( $arrintBankAccountIds ) ) return NULL;
		if( false != $boolIsCreditCardAccount ) {
			$strGlAccountSql = 'ac.ap_gl_account_id AS gl_account_id';
		} else {
			$strGlAccountSql = 'ac.item_gl_account_id AS gl_account_id';
		}

		$strSql = 'SELECT
						pba.*, ' . $strGlAccountSql . '
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.is_primary = true
						AND pba.bank_account_id IN( ' . implode( ',', $arrintBankAccountIds ) . ' );';

		return self::fetchPropertyBankAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchCountActivePropertyBankAccountsByPropertyIdByCid( $intPropertyId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						COUNT( ba.id )
					FROM
						bank_accounts ba,
						property_bank_accounts pba
					WHERE
						ba.id = pba.bank_account_id
						AND ba.cid = pba.cid
						AND pba.cid = ' . ( int ) $intCid . '
						AND pba.property_id = ' . ( int ) $intPropertyId . '
						AND ba.is_disabled = 0';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchPropertyBankAccountsDetailsByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						pba.*,
						ac.item_gl_account_id AS gl_account_id,
						p.property_name
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id
						LEFT JOIN properties p ON pba.cid = p.cid AND  pba.property_id = p.id
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.bank_account_id = ' . ( int ) $intBankAccountId;

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchCashGlAccountsByBankAccountIdsByPropertyIdsByCid( $arrintBankAccountIds, $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintBankAccountIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = 'SELECT
						ac.item_gl_account_id AS gl_account_id,
						pba.property_id,
						gat.account_number as cash_account_number
					FROM
						property_bank_accounts pba
						JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id AND ac.ap_code_type_id = ' . CApCodeType::GL_ACCOUNT . ' )
						JOIN gl_accounts ga ON ( ac.cid = ga.cid AND ac.item_gl_account_id = ga.id )
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid and ga.id = gat.gl_account_id and gat.is_default = 1 )
						JOIN bank_accounts ba ON ( ba.cid = pba.cid AND pba.bank_account_id = ba.id )

					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.bank_account_id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' )
						AND pba.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( CASE
								WHEN ba.bank_account_type_id = ' . CBankAccountType::INTER_COMPANY . ' THEN
									pba.is_reimbursed_property = true
								ELSE
									pba.is_reimbursed_property = false
								END
							)
						AND ga.gl_account_type_id IN ( ' . CGlAccountType::ASSETS . ' )
						AND ga.gl_account_usage_type_id IN ( ' . CGlAccountUsageType::BANK_ACCOUNT . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyBankAccountDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pba.cid,
						pba.property_id,
						array_to_string( array_agg( pba.bank_account_id ), \',\' ) as bank_account_ids
					FROM
						property_bank_accounts pba
						JOIN bank_accounts ba ON( ba.cid = pba.cid AND ba.id = pba.bank_account_id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pba.is_primary = TRUE
						AND	ba.deleted_on IS NULL
						AND ba.is_disabled = 0
					GROUP BY
						pba.cid,
						pba.property_id';

		return ( array ) fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchRentBankAccountsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $boolIsMilitary = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strAllowedBankAccounts = ( true == $boolIsMilitary ) ? 'COALESCE( pgs.military_mac_bank_account_id, pgs.rent_bank_account_id )' : 'pgs.rent_bank_account_id';

		$strSql = 'SELECT
						pba.cid,
						pba.property_id,
						ba.id as bank_account_id,
						initcap(ba.account_name) as bank_account_name
					FROM
						property_gl_settings pgs
						JOIN property_bank_accounts pba ON( pgs.cid = pba.cid AND pgs.property_id = pba.property_id AND ' . $strAllowedBankAccounts . ' = pba.bank_account_id )
						JOIN bank_accounts ba ON( ba.cid = pba.cid AND ba.id = pba.bank_account_id )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pba.is_primary = TRUE
						AND	ba.deleted_on IS NULL
						AND ba.is_disabled = 0';

		return ( array ) fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchUnreconciledTransactionsByPropertyIdsByGlAccountIdsByCid( $arrintPropertyIds, $arrintGlAccountIds, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intCid ) || false == valArr( $arrintPropertyIds ) || false == valArr( $arrintGlAccountIds ) ) return NULL;

		$strSql = ' SELECT
					    p.id AS property_id,
					    p.property_name,
					    gat.gl_account_id,
					    CONCAT( gat.formatted_account_number, \' : \', gat.name ) AS formatted_gl_account
					FROM
						(
						SELECT
							gd.cid,
							gd.property_id,
							CASE WHEN gd.accrual_gl_account_id IN (  ' . implode( ',', $arrintGlAccountIds ) . ' )
								 THEN gd.accrual_gl_account_id
								 ELSE gd.cash_gl_account_id
							END AS gl_account_id
						FROM
							gl_headers gh
							JOIN gl_details gd ON gh.cid = gd.cid AND gh.id = gd.gl_header_id AND gh.gl_transaction_type_id = gd.gl_transaction_type_id AND gh.post_month = gd.post_month
							LEFT JOIN gl_reconciliations gr ON gd.cid = gr.cid AND gd.gl_reconciliation_id = gr.id AND gr.gl_reconciliation_status_type_id <> ' . CGlReconciliationStatusType::RECONCILED . '
						WHERE
							gh.cid = ' . ( int ) $intCid . '
							AND NOT gh.is_template
							AND gh.gl_transaction_type_id IN ( ' . CGlTransactionType::GL_GJ . ', ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ',  ' . CGlTransactionType::AR_DEPOSIT . ', ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' )
							AND gh.gl_header_status_type_id NOT IN ( ' . implode( ', ', CGlHeaderStatusType::$c_arrintExcludedGlHeaderStatusTypes ) . ' )
							AND gd.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND ( gd.accrual_gl_account_id IN (  ' . implode( ',', $arrintGlAccountIds ) . ') OR gd.cash_gl_account_id IN (  ' . implode( ',', $arrintGlAccountIds ) . ' ) )
							AND ( gd.gl_reconciliation_id IS NULL OR ( gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id <> ' . CGlReconciliationStatusType::RECONCILED . ' ) )
							AND CASE
									WHEN 1 = gh.gl_transaction_type_id THEN true
									WHEN gh.gl_transaction_type_id IN (  ' . CGlTransactionType::AR_DEPOSIT . ', ' . CGlTransactionType::AR_DEPOSIT_REVERSAL . ' ) AND
										EXISTS (
												SELECT NULL FROM
													ar_deposits ad
												WHERE
													ad.cid = gh.cid
													AND ad.id = gh.reference_id
													AND ad.deposit_amount != 0
													AND gh.gl_transaction_type_id = ad.gl_transaction_type_id 
												)
									THEN true
									WHEN gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' ) AND
										EXISTS (
												SELECT NULL FROM
													ap_payments ap
													JOIN ap_headers ah ON ap.cid = ah.cid AND ap.id = ah.ap_payment_id AND ah.deleted_on IS NULL
													JOIN ap_details ad ON ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month
													JOIN ap_allocations aa ON ad.cid = aa.cid AND ( ad.id = aa.credit_ap_detail_id OR ( ad.id = aa.charge_ap_detail_id AND ad.is_cross_allocation = true ) )
													JOIN ap_details ad_charge ON  ad_charge.cid = aa.cid AND ad_charge.id = aa.charge_ap_detail_id AND ad_charge.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
					                            WHERE
					                                aa.cid = gh.cid
					                                AND aa.id = gh.reference_id
					                                AND aa.gl_transaction_type_id = gh.gl_transaction_type_id
													AND ap.payment_status_type_id <> ' . CPaymentStatusType::VOIDED . '
													AND ah.reversal_ap_header_id IS NULL
													AND ad.reversal_ap_detail_id IS NULL
													AND NOT aa.is_deleted 
												)
									THEN true
									WHEN gh.gl_transaction_type_id IN ( ' . implode( ',', CGlTransactionType::$c_arrintApAllocationGlTransactionTypeIds ) . ' ) AND
										EXISTS (
												SELECT NULL FROM
													ap_payments ap
													JOIN ap_headers ah ON ap.cid = ah.cid AND ap.id = ah.ap_payment_id AND ah.deleted_on IS NULL
													JOIN ap_details ad ON ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month
													JOIN ap_allocations aa ON ad.cid = aa.cid AND ( ad.id = aa.credit_ap_detail_id OR ( ad.id = aa.charge_ap_detail_id AND ad.is_cross_allocation = true ) )
													JOIN ap_details ad_charge ON ad_charge.cid = aa.cid AND ad_charge.id = aa.charge_ap_detail_id AND ad_charge.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
					                            WHERE
					                                aa.cid = gh.cid
					                                AND aa.id = gh.reference_id
					                                AND aa.gl_transaction_type_id = gh.gl_transaction_type_id
					                                AND ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' 
					                            )
									THEN true
					            ELSE false
							END
						GROUP BY
							1, 2, 3
						) sub_query
						JOIN gl_account_trees gat ON ( gat.cid = sub_query.cid AND gat.gl_account_id = sub_query.gl_account_id AND gat.is_default = 1 )
						JOIN properties p ON ( p.cid = sub_query.cid AND p.id = sub_query.property_id )
					WHERE
					    p.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

}

?>