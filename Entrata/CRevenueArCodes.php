<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueArCodes
 * Do not add any new functions to this class.
 */
class CRevenueArCodes extends CBaseRevenueArCodes {

	public static function fetchRevenueArCodesByOldAndNewArCodeIdByCid( $intOldArCodeId, $intNewArCodeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						rac1.*
					FROM
						revenue_ar_codes rac1
					WHERE
						rac1.cid = ' . ( int ) $intCid . '
						AND rac1.ar_code_id = ' . ( int ) $intNewArCodeId . '
						AND EXISTS (
										SELECT
											1
										FROM
											revenue_ar_codes rac2
										WHERE
											rac2.cid = ' . ( int ) $intCid . '
											AND rac2.ar_code_id = ' . ( int ) $intOldArCodeId . '
											AND rac1.revenue_ar_code_type_id = rac2.revenue_ar_code_type_id
											AND rac1.revenue_scheduled_charge_type_id = rac2.revenue_scheduled_charge_type_id )';

		return self::fetchRevenueArCodes( $strSql, $objClientDatabase );
	}
}
?>