<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateClients
 * Do not add any new functions to this class.
 */

class CCorporateClients extends CBaseCorporateClients {

	public static function fetchCorporateClientByUsernamePasswordCid( $strUsername, $strPassword, $intCid, $objDatabase ) {
		$strSql = sprintf( "SELECT cc.* FROM %s cc WHERE cc.username = '%s' AND cc.password = '%s' AND cc.cid = %d", 'corporate_clients', addslashes( ( string ) $strUsername ), ( string ) $strPassword, ( int ) $intCid );
		return self::fetchCorporateClient( $strSql, $objDatabase );
	}

	public static function fetchCompetingUserCountByUserNameByIdByCid( $strUsername, $intId, $intCid, $objDatabase ) {
		if( true == is_numeric( $intId ) ) {
    		return parent::fetchCorporateClientCount( 'WHERE lower(username) = lower( \'' . addslashes( trim( $strUsername ) ) . '\' ) AND id!= ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid, $objDatabase );
		}

    	return parent::fetchCorporateClientCount( 'WHERE lower(username) = lower( \'' . addslashes( trim( $strUsername ) ) . '\' )', $objDatabase );
	}

}
?>