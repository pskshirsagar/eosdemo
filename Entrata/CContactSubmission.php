<?php

class CContactSubmission extends CBaseContactSubmission {

	protected $m_objCall;
	protected $m_objChat;
	protected $m_objCustomer;
	protected $m_intPropertyBuildingId;
	protected $m_intCallId;
	protected $m_intChatId;
	protected $m_intCallResultId;
	protected $m_intChatResultId;
	protected $m_intPrimaryPhoneTypeId;
	protected $m_intSecondaryPhoneTypeId;
	protected $m_strBusinessName;
	protected $m_strProduct;
	protected $m_strPropertyBuildingNumber;
	protected $m_strFormattedContactSubmissionDate;
	protected $m_strPrimaryPhoneNumber;
	protected $m_strSecondaryPhoneNumber;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strCreatedCustomerName;
	protected $m_strCreatedCustomerPhone;
	protected $m_boolIsResident;
	protected $m_boolIsPropertyBuildingId;
	protected $m_boolIsValidateCallId;
	protected $m_boolIsFromResidentPortal;
	protected $m_intResponsePhoneNumberTypeId;
	protected $m_intCreateCustomerId;
	protected $m_intOccupancyTypeId;

	protected $m_arrmixRequiredParameters;

	const RESPONSE_REQUEST_TYPE_EMAIL = 1;
	const RESPONSE_REQUEST_TYPE_PHONE = 2;
	const RESPONSE_PHONE_NUMBER_TYPE_OFFICE = 3;
	const RESPONSE_PHONE_NUMBER_TYPE_MOBILE = 4;

	/**
	 * Get Functions
	 */

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParameters;
	}

	public function getIsResident() {
		return $this->m_boolIsResident;
	}

	public function getIsPropertyBuildingId() {
		return $this->m_boolIsPropertyBuildingId;
	}

	public function getIsValidateCallId() {
		return $this->m_boolIsValidateCallId;
	}

	public function getIsFromResidentPortal() {
		return $this->m_boolIsFromResidentPortal;
	}

	public function getCall() {
		return $this->m_objCall;
	}

	public function getChat() {
		return $this->m_objChat;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function getChatId() {
		return $this->m_intChatId;
	}

	public function getCallResultId() {
		return $this->m_intCallResultId;
	}

	public function getChatResultId() {
		return $this->m_intChatResultId;
	}

	public function getPrimaryPhoneTypeId() {
		return $this->m_intPrimaryPhoneTypeId;
	}

	public function getSecondaryPhoneTypeId() {
		return $this->m_intSecondaryPhoneTypeId;
	}

	public function getBusinessName() {
		return $this->m_strBusinessName;
	}

	public function getProduct() {
		return $this->m_strProduct;
	}

	public function getPropertyBuildingNumber() {
		return $this->m_strPropertyBuildingNumber;
	}

	public function getFormattedContactSubmissionDate() {
		return $this->m_strFormattedContactSubmissionDate;
	}

	public function getPrimaryPhoneNumber() {
		return $this->m_strPrimaryPhoneNumber;
	}

	public function getSecondaryPhoneNumber() {
		return $this->m_strSecondaryPhoneNumber;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getResponsePhoneNumberTypeId() {
		return $this->m_intResponsePhoneNumberTypeId;
	}

	public function getCreatedCustomerId() {
		return $this->m_intCreateCustomerId;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function getCreatedCustomerName() {
		return $this->m_strCreatedCustomerName;
	}

	public function getCreatedCustomerPhone() {
		return $this->m_strCreatedCustomerPhone;
	}

	/**
	 * Set Functions
	 */

	public function setRequiredParameters( $arrmixCustomData ) {
		$this->m_arrmixRequiredParameters = $arrmixCustomData;
	}

	public function setCall( $objCall ) {
		$this->m_objCall = $objCall;
	}

	public function setChat( $objChat ) {
		$this->m_objChat = $objChat;
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->m_intPropertyBuildingId = $intPropertyBuildingId;
	}

	public function setCallId( $intCallId ) {
		$this->m_intCallId = $intCallId;
	}

	public function setChatId( $intChatId ) {
		$this->m_intChatId = $intChatId;
	}

	public function setCallResultId( $intCallResultId ) {
		$this->m_intCallResultId = $intCallResultId;
	}

	public function setChatResultId( $intChatResultId ) {
		$this->m_intChatResultId = $intChatResultId;
	}

	public function setPrimaryPhoneTypeId( $intPrimaryPhoneTypeId ) {
		$this->m_intPrimaryPhoneTypeId = $intPrimaryPhoneTypeId;
	}

	public function setSecondaryPhoneTypeId( $intSecondaryPhoneTypeId ) {
		$this->m_intSecondaryPhoneTypeId = $intSecondaryPhoneTypeId;
	}

	public function setBusinessName( $strBusinessName ) {
		$this->m_strBusinessName = $strBusinessName;
	}

	public function setProduct( $strProduct ) {
		$this->m_strProduct = $strProduct;
	}

	public function setPropertyBuildingNumber( $strPropertyBuildingNumber ) {
		$this->m_strPropertyBuildingNumber = $strPropertyBuildingNumber;
	}

	public function setFormattedContactSubmissionDate( $strFormattedContactSubmissionDate ) {
		$this->m_strFormattedContactSubmissionDate = $strFormattedContactSubmissionDate;
	}

	public function setPrimaryPhoneNumber( $strPrimaryPhoneNumber ) {
		$this->m_strPrimaryPhoneNumber = $strPrimaryPhoneNumber;
	}

	public function setSecondaryPhoneNumber( $strSecondaryPhoneNumber ) {
		$this->m_strSecondaryPhoneNumber = $strSecondaryPhoneNumber;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setIsResident( $boolIsResident ) {
		$this->m_boolIsResident = $boolIsResident;
	}

	public function setIsPropertyBuildingId( $boolIsPropertyBuildingId ) {
		$this->m_boolIsPropertyBuildingId = $boolIsPropertyBuildingId;
	}

	public function setIsValidateCallId( $boolIsValidateCallId ) {
		$this->m_boolIsValidateCallId = $boolIsValidateCallId;
	}

	public function setIsFromResidentPortal( $boolIsFromResidentPortal ) {
		$this->m_boolIsFromResidentPortal = $boolIsFromResidentPortal;
	}

	public function setResponsePhoneNumberTypeId( $intResponsePhoneNumberTypeId ) {
		$this->m_intResponsePhoneNumberTypeId = $intResponsePhoneNumberTypeId;
	}

	public function setCreatedCustomerId( $intCreateCustomerId ) {
		$this->m_intCreateCustomerId = $intCreateCustomerId;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

	public function setCreatedCustomerName( $strCreatedCustomerName ) {
		$this->m_strCreatedCustomerName = $strCreatedCustomerName;
	}

	public function setCreatedCustomerPhone( $strCreatedCustomerPhone ) {
		$this->m_strCreatedCustomerPhone = $strCreatedCustomerPhone;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['business_name'] ) && true == valStr( $arrmixValues['business_name'] ) ) $this->setBusinessName( $arrmixValues['business_name'] );
		if( true == isset( $arrmixValues['product'] ) && true == valStr( $arrmixValues['product'] ) ) $this->setProduct( $arrmixValues['product'] );
		if( true == isset( $arrmixValues['property_building_id'] ) && true == is_numeric( $arrmixValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrmixValues['property_building_id'] );
		if( true == isset( $arrmixValues['property_building_number'] ) && true == valStr( $arrmixValues['property_building_number'] ) ) $this->setPropertyBuildingNumber( $arrmixValues['property_building_number'] );
		if( true == isset( $arrmixValues['name_first'] ) ) $this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) $this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['phone_number_type_id'] ) ) $this->setResponsePhoneNumberTypeId( $arrmixValues['phone_number_type_id'] );
		if( true == isset( $arrmixValues['primary_phone_number'] ) ) $this->setPrimaryPhoneNumber( $arrmixValues['primary_phone_number'] );
		if( true == isset( $arrmixValues['primary_phone_type_id'] ) ) $this->setPrimaryPhoneTypeId( $arrmixValues['primary_phone_type_id'] );
		if( true == isset( $arrmixValues['secondary_phone_number'] ) ) $this->setSecondaryPhoneNumber( $arrmixValues['secondary_phone_number'] );
		if( true == isset( $arrmixValues['secondary_phone_type_id'] ) ) $this->setSecondaryPhoneTypeId( $arrmixValues['secondary_phone_type_id'] );
		if( true == isset( $arrmixValues['call_id'] ) ) $this->setCallId( $arrmixValues['call_id'] );
		if( true == isset( $arrmixValues['chat_id'] ) ) $this->setChatId( $arrmixValues['chat_id'] );
		if( true == isset( $arrmixValues['call_result_id'] ) ) $this->setCallResultId( $arrmixValues['call_result_id'] );
		if( true == isset( $arrmixValues['chat_result_id'] ) ) $this->setChatResultId( $arrmixValues['chat_result_id'] );
		if( true == isset( $arrmixValues['is_validate_call_id'] ) ) $this->setIsValidateCallId( $arrmixValues['is_validate_call_id'] );
		if( true == isset( $arrmixValues['created_customer_id'] ) ) $this->setCreatedCustomerId( $arrmixValues['created_customer_id'] );
		if( true == isset( $arrmixValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
		if( true == isset( $arrmixValues['created_customer_name'] ) ) $this->setCreatedCustomerName( $arrmixValues['created_customer_name'] );
		if( true == isset( $arrmixValues['created_customer_phone'] ) ) $this->setCreatedCustomerPhone( $arrmixValues['created_customer_phone'] );

		return true;
	}

	public function setPhoneNumberByPhoneNumberTypeId( $intPhoneNumberPhoneTypeId ) {
		$strPhoneNumber = ( $intPhoneNumberPhoneTypeId == $this->getPrimaryPhoneTypeId() ) ? $this->getPrimaryPhoneNumber() : $this->getSecondaryPhoneNumber();

		switch( $intPhoneNumberPhoneTypeId ) {
			case CPhoneNumberType::HOME:
				$this->setPhoneNumber( $strPhoneNumber );
				break;

			case CPhoneNumberType::MOBILE:
				$this->setMobileNumber( $strPhoneNumber );
				break;

			case CPhoneNumberType::OFFICE:
				$this->setOfficeNumber( $strPhoneNumber );
				break;

			default:

		}
		return true;
	}

	public function setCustomerDetails( $objCustomer ) {
		$arrmixCustomerPhoneNumbers = $objCustomer->getCustomerPhoneNumbers();
		$this->setCustomerId( $objCustomer->getId() );
		$this->setNameFirst( $objCustomer->getNameFirst() );
		$this->setNameLast( $objCustomer->getNameLast() );
		$this->setUnitNumber( $objCustomer->getUnitNumber() );
		$this->setEmailAddress( $objCustomer->getEmailAddress() );
		if( true == valArr( $arrmixCustomerPhoneNumbers[CPhoneNumberType::MOBILE] ) ) {
			$this->setMobileNumber( $arrmixCustomerPhoneNumbers[CPhoneNumberType::MOBILE]['phone_number'] );
		}
		if( true == valArr( $arrmixCustomerPhoneNumbers[CPhoneNumberType::OFFICE] ) ) {
			$this->setOfficeNumber( $arrmixCustomerPhoneNumbers[CPhoneNumberType::OFFICE]['phone_number'] );
		}

		return true;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCall( $objVoipDatabase ) {
		return CCalls::fetchCallByCidByPropertyIdById( $this->getCid(), $this->getPropertyId(), $this->getCallId(), $objVoipDatabase );
	}

	/**
	 * Create Functions
	 */

	public function createCorporateComplaint( $intCompanyUserId ) {
		$objCorporateComplaint = new CCorporateComplaint();
		$objCorporateComplaint->setCid( $this->getCid() );
		$objCorporateComplaint->setPropertyId( $this->getPropertyId() );
		$objCorporateComplaint->setCallId( $this->getCallId() );
		$objCorporateComplaint->setNotes( $this->getMessage() );
		$objCorporateComplaint->setAssignedBy( $intCompanyUserId );
		$objCorporateComplaint->setAssignedOn( 'NOW()' );

		return $objCorporateComplaint;
	}

	/**
	 * Contact Submission Email
	 */

	public function sendContactSubmissionEmail( $objClient, $objProperty, $objDatabase, $arrstrContactSubmission = NULL, $objEmailDatabase, $boolIsTenant = false ) {
		$arrstrResidentEmailAddresses	= [];

		if( true == valStr( $this->getEmailAddress() ) ) {
			$arrstrResidentEmailAddresses = [ $this->getEmailAddress() ];
		}

		if( CPsProduct::LEASING_CENTER != $this->getPsProductId() && true == $this->getIsResident() && false == valArr( $arrstrResidentEmailAddresses ) ) {
			return false;
		}

		$strSystemMessageKey = '';
		if( CPsProduct::LEASING_CENTER == $this->getPsProductId() ) {
			$strSystemMessageKey = 'CONTACT_SUBMISSION_EMAIL';
		} else {
			$strSystemMessageKey = 'CONTACT_SUBMISSION_EMAIL_FROM_LEASING_CENTER';
		}

		$objSystemMessageLibrary = new CSystemMessageLibrary();
		$objSystemMessageLibrary->setClient( $objClient );
		$objSystemMessageLibrary->setDatabase( $objDatabase );
		$objSystemMessageLibrary->setPropertyId( $objProperty->getId() );
		$objSystemMessageLibrary->setSystemEmailTypeId( CSystemEmailType::CONTACT_SUBMISSIONS );
		$objSystemMessageLibrary->setCompanyUserId( SYSTEM_USER_ID );
		$objSystemMessageLibrary->setIsResident( true );
		$objSystemMessageLibrary->setIsLoadDefaultMetaData( false );

		$arrmixExtraMetaData['contact_submission_id'] = $this->getId();
		$arrmixExtraMetaData['property_id'] = $objProperty->getId();
		$arrmixExtraMetaData['contact_submissions']['property_building_number'] = $this->getPropertyBuildingNumber();

		if( true == valArr( $arrstrContactSubmission ) ) {
			$arrmixExtraMetaData['contact_submissions']['contact_submission_reason'] = ( true == isset( $arrstrContactSubmission['reason'] ) ? $arrstrContactSubmission['reason'] : NULL );
			$arrmixExtraMetaData['contact_submissions']['contact_submission_move_out_reason'] = ( true == isset( $arrstrContactSubmission['move_out_reason'] ) ? $arrstrContactSubmission['move_out_reason'] : NULL );
			$arrmixExtraMetaData['contact_submissions']['contact_submission_move_out_date'] = ( true == isset( $arrstrContactSubmission['move_out_date'] ) ? $arrstrContactSubmission['move_out_date'] : NULL );
		}

		$objSystemMessageLibrary->setSystemMessageAudienceIds( [ CSystemMessageAudience::CONTACT_SUBMISSION, CSystemMessageAudience::PROPERTY ] );
		$objSystemMessageLibrary->setSystemMessageKey( 'CONTACT_SUBMISSION_EMAIL' );
		if( true == $boolIsTenant ) {
			$objSystemMessageLibrary->setSystemMessageKey( 'COMMERCIAL_CONTACT_SUBMISSION_EMAIL' );
		}

		if( CPsProduct::LEASING_CENTER == $this->getPsProductId() ) {
			$objSystemMessageLibrary->setSystemMessageAudienceIds( [ CSystemMessageAudience::PROPERTY ] );
			$objSystemMessageLibrary->setSystemMessageKey( 'CONTACT_SUBMISSION_EMAIL_FROM_LEASEING_CENTER' );
			if( true == valObj( $this->m_objCall, 'CCall' ) ) {
				$arrmixExtraMetaData['contact_submissions']['call_id'] = $this->m_objCall->getId();
				$arrmixExtraMetaData['contact_submissions']['call_result_id'] = $this->m_objCall->getCallResultId();
				$arrmixExtraMetaData['properties'] = [ 'call_id' => $this->m_objCall->getId() ];
			}
		}

		if( true == $this->getIsFromResidentPortal() ) {
			$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'RESIDENT_PORTAL_MANAGER_CONTACT_NOTIFICATION_EMAIL', $objProperty->getId(), $objClient->getId(), $objDatabase );
		} else {
			$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'MANAGER_CONTACT_NOTIFICATION_EMAIL', $objProperty->getId(), $objClient->getId(), $objDatabase );
		}

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {
			$objSystemMessageLibrary->setPropertyPreference( $objPropertyPreference );
		}

		$objSystemMessageLibrary->setExtraMetaData( $arrmixExtraMetaData );
		$objSystemMessageLibrary->execute();

		return true;
	}

	public function buildHtmlRequestEmailByContactSubmission( $objClient = NULL, $objProperty = NULL, $boolIsResident = NULL, $objDatabase = NULL, $arrstrContactSubmission = NULL, $objPropertyEmailRule = NULL, $boolIsTenant = false ) {
		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';
		$objMarketingMediaAssociation						= NULL;
		$objLogoMarketingMediaAssociation							= NULL;
		$objManagerContactCustomTextPropertyNotification	= NULL;
		$objPropertyTimeZone								= NULL;

		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$objMarketingMediaAssociation						= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
			$objManagerContactCustomTextPropertyNotification	= $objProperty->fetchPropertyNotificationByKey( 'MANAGER_CONTACT_CUSTOM_TEXT', $objDatabase );
			$objPropertyTimeZone								= CTimeZones::fetchTimeZoneByPropertyIdByCid( $objProperty->getId(), $objClient->getId(), $objDatabase );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		$objSmarty = new CPsSmarty( PATH_PHP_INTERFACES . 'Templates/Common/', false );
		$objSmarty->assign( 'contact_submission', $this );
		$objSmarty->assign( 'property', $objProperty );
		$objSmarty->assign( 'marketing_media_association', $objMarketingMediaAssociation );
		$objSmarty->assign( 'default_company_media_file', $objLogoMarketingMediaAssociation );
		$objSmarty->assign( 'manager_contact_custom_text_property_notification', $objManagerContactCustomTextPropertyNotification );
		$objSmarty->assign( 'call', $this->m_objCall );
		$objSmarty->assign( 'chat', $this->m_objChat );
		$objSmarty->assign( 'property_time_zone', $objPropertyTimeZone );
		$objSmarty->assign( 'property_email_rule', $objPropertyEmailRule );

		if( true == valId( $this->getPrimaryPhoneTypeId() ) ) {
			$objSmarty->assign( 'primary_phone_type', CPhoneNumberType::createService()->getPhoneNumberTypeNameByPhoneNumberTypeId( $this->getPrimaryPhoneTypeId() ) );
		}

		$objSmarty->assign( 'secondary_phone_type', CPhoneNumberType::createService()->getPhoneNumberTypeNameByPhoneNumberTypeId( $this->getSecondaryPhoneTypeId() ) );
		$objSmarty->assign( 'current_date', date( 'm/d/Y H:i:s' ) );
		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'media_library_uri', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'PS_PRODUCT_LEASING_CENTER', CPsProduct::LEASING_CENTER );

		if( true == isset( $boolIsResident ) ) {
			$objSmarty->assign( 'is_resident', $boolIsResident );
		}

		if( true == valArr( $arrstrContactSubmission ) ) {
			$objSmarty->assign( 'contact_submission_reason', ( true == isset( $arrstrContactSubmission['reason'] ) ? $arrstrContactSubmission['reason'] : NULL ) );
			$objSmarty->assign( 'contact_submission_move_out_reason', ( true == isset( $arrstrContactSubmission['move_out_reason'] ) ? $arrstrContactSubmission['move_out_reason'] : NULL ) );
			$objSmarty->assign( 'contact_submission_move_out_date', ( true == isset( $arrstrContactSubmission['move_out_date'] ) ? $arrstrContactSubmission['move_out_date'] : NULL ) );
		}
		$objSmarty->assign( 'is_tenant', $boolIsTenant );

		return $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/contact_submissions/contact_submissions_email.tpl' );
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == valId( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_datetime', __( 'Property is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valResponseRequestTypeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valContactDatetime() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first name', __( 'First name is required.' ) ) );
		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->getNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getNameFirst(), 'Content-type' ) ) {
			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first name', __( 'First names cannot contain @ signs or email headers.' ) ) );
		} else {
			if( 1 > \Psi\CStringService::singleton()->strlen( $this->getNameFirst() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first name', __( 'First name must have at least one letter.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Last name is required.' ) ) );
		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->getNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getNameLast(), 'Content-type' ) ) {
			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last name', __( 'Last name cannot contain @ signs or email headers.' ) ) );
		} else {
			if( 1 > \Psi\CStringService::singleton()->strlen( $this->getNameLast() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last name', __( 'Last name must have at least one letter.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valUnitNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_datetime', __( 'Appt number or address is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSearchUnitNumber( $boolIsValidateFutureCustomer = NULL ) {
		$boolIsValid = true;

		if( true == $boolIsValidateFutureCustomer ) {
			if( true == valObj( $this->getCustomer(), 'CCustomer' ) && CLeaseStatusType::FUTURE != $this->getCustomer()->getLeaseStatusTypeId() && true == is_null( $this->getUnitNumber() ) ) {
				$boolIsValid = false;
			}
		} elseif( true == is_null( $this->getUnitNumber() ) ) {
				$boolIsValid = false;
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_number', __( 'Unit number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyBuildingId() {
		$boolIsValid = true;

		if( true == $this->getIsPropertyBuildingId() && true == is_null( $this->getPropertyBuildingId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_building_id', __( 'Building Number is required.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber( $boolFullValidation = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPhoneNumber() ) || ( 1 !== CValidation::checkPhoneNumber( $this->getPhoneNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'A valid phone number is required.' ) );
		} elseif( true == $boolFullValidation && 1 != CValidation::checkPhoneNumberFullValidation( $this->getPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be in xxx-xxx-xxxx format.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMobileNumber( $boolFullValidation = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getMobileNumber() ) || ( 1 !== CValidation::checkPhoneNumber( $this->getMobileNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', 'A valid mobile number is required.' ) );
		} elseif( true == $boolFullValidation && 1 != CValidation::checkPhoneNumberFullValidation( $this->getMobileNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', 'Mobile number must be in xxx-xxx-xxxx format.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valOfficeNumber( $boolFullValidation = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getOfficeNumber() ) || ( 1 !== CValidation::checkPhoneNumber( $this->getOfficeNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_number', 'A valid office phone number is required.' ) );
		} elseif( true == $boolFullValidation && 1 != CValidation::checkPhoneNumberFullValidation( $this->getOfficeNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_number', 'Office phone number must be in xxx-xxx-xxxx format.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPrimaryPhoneNumber( $boolIsFullValidation = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPrimaryPhoneNumber() ) || ( 1 !== CValidation::checkPhoneNumber( $this->getPrimaryPhoneNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'A valid primary phone number is required.' ) ) );
		} elseif( true == $boolIsFullValidation && 1 != CValidation::checkPhoneNumberFullValidation( $this->getPrimaryPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Primary phone number must be in xxx-xxx-xxxx format.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmailAddress() ) || ( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'The email address does not appear to be valid.' ) );
		}

		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;

		$this->setSubject( getSanitizedFormField( $this->getSubject() ) );

		if( false == valStr( $this->getSubject() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', __( 'Subject is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMessage() {
		$boolIsValid = true;

		$this->setMessage( trim( getSanitizedFormField( $this->getMessage(), true ) ) );

		if( false == valStr( $this->getMessage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Message is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCallResultId() {
		$boolIsValid = true;

		if( true == is_null( getArrayElementByKey( $this->m_objCall->getCallResultId(), CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_result_id', __( 'Call Result is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valChatResultId() {
		$boolIsValid = true;

		if( true == is_null( getArrayElementByKey( $this->m_objChat->getChatResultId(), CChatResult::$c_arrintLeasingCenterOtherContactChatResults ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'chat_result_id', __( 'Chat Result is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valClosedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valClosedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valViewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutDate() {
		$boolIsValid = true;
		if( true == valStr( $this->getMoveOutDate() ) ) {
			if( 1 !== CValidation::checkDate( $this->getMoveOutDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', ' Move Out date must be in mm/dd/yyyy format.' ) );
			} else if( strtotime( date( 'm/d/Y' ) ) > strtotime( $this->getMoveOutDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move Out date must be greater than or equal to today.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valCallId( $objVoipDatabase ) {
		$boolIsValid = true;

		assertObj( $objVoipDatabase, 'CDatabase' );

		if( true == $this->getIsValidateCallId() && false == valId( $this->getCallId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_id', __( 'Call ID Number is required.' ) ) );
		} elseif( 0 < \Psi\CStringService::singleton()->strlen( $this->getCallId() ) && false == is_numeric( $this->getCallId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_id', __( 'Call ID Number should be numeric.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUnitNumber();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valMessage();
				break;

			case 'leasing_center_insert':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valSearchUnitNumber( true );
				$boolIsValid &= $this->valPrimaryPhoneNumber( false );
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= $this->valCallId( $objVoipDatabase );
				break;

			case 'call_center_insert':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valSearchUnitNumber();
				$boolIsValid &= $this->valPropertyBuildingId();
				$boolIsValid &= $this->valPhoneNumber( false );
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= $this->valCallId( $objVoipDatabase );
				break;

			case 'resident_portal_mobile_insert':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valUnitNumber();
				break;

			case 'call_center_other_contact_not_resident':
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= ( false == is_null( $this->getEmailAddress() ) ) ? $this->valEmailAddress()	: true;
				$boolIsValid &= ( false == is_null( $this->getPhoneNumber() ) ) ? $this->valPhoneNumber( false ) : true;
				$boolIsValid &= $this->valCallId( $objVoipDatabase );
				break;

			case 'call_center_other_contact_not_resident_with_CallResultId':
				$boolIsValid &= $this->valCallResultId();
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= ( false == is_null( $this->getEmailAddress() ) ) ? $this->valEmailAddress()	: true;
				$boolIsValid &= ( false == is_null( $this->getPhoneNumber() ) ) ? $this->valPhoneNumber( false ) : true;
				$boolIsValid &= $this->valCallId( $objVoipDatabase );
				break;

			case 'leasing_center_other_contact_submission':
				if( true == valObj( $this->getCall(), 'CCall' ) ) {
					$boolIsValid &= $this->valCallResultId();
				} elseif( true == valObj( $this->getChat(), 'CChat' ) ) {
					$boolIsValid &= $this->valChatResultId();
				}

				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= ( true == valStr( $this->getEmailAddress() ) ) ? $this->valEmailAddress() : true;
				$boolIsValid &= ( true == valStr( $this->getPhoneNumber() ) ) ? $this->valPhoneNumber( false ) : true;
				$boolIsValid &= $this->valCallId( $objVoipDatabase );
				break;

			case 'resident_portal_1_5_insert':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				if( self::RESPONSE_REQUEST_TYPE_EMAIL == $this->getResponseRequestTypeId() ) {
					$boolIsValid &= $this->valEmailAddress();
				}

				if( self::RESPONSE_REQUEST_TYPE_PHONE == $this->getResponseRequestTypeId() ) {
					if( self::RESPONSE_PHONE_NUMBER_TYPE_MOBILE == $this->getResponsePhoneNumberTypeId() ) {
						$boolIsValid &= $this->valMobileNumber();
					} elseif( self::RESPONSE_PHONE_NUMBER_TYPE_OFFICE == $this->getResponsePhoneNumberTypeId() ) {
						$boolIsValid &= $this->valOfficeNumber();
					} else {
						$boolIsValid &= $this->valPhoneNumber();
					}
				}

				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= $this->valMoveOutDate();
				break;

			case 'resident_portal_40_insert':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				if( self::RESPONSE_REQUEST_TYPE_EMAIL == $this->getResponseRequestTypeId() ) {
					$boolIsValid &= $this->valEmailAddress();
				}
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= $this->valMoveOutDate();
				break;

			case 'validate_resident_corporate_complaint':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();

				if( true == valObj( $this->getCustomer(), 'CCustomer' ) && CLeaseStatusType::FUTURE != $this->getCustomer()->getLeaseStatusTypeId() ) {
					$boolIsValid &= $this->valUnitNumber();
				}

				$boolIsValid &= ( true == valStr( $this->getEmailAddress() ) ) ? $this->valEmailAddress() : true;
				$boolIsValid &= $this->valMessage();
				break;

			case 'validate_other_corporate_complaint':
				$boolIsValid &= $this->valCallResultId();
				$boolIsValid &= $this->valMessage();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getCallResultName() {
		$intCallResultId = $this->getRequiredParameters()['call_result_id'] ?? NULL;
		$objVoipDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) || false == valId( $intCallResultId ) ) {
			return '';
		}

		$objVoipDatabase->open();
		$objCallResult = \Psi\Eos\Voip\CCallResults::createService()->fetchCallResultById( $intCallResultId, $objVoipDatabase );
		$objVoipDatabase->close();

		if( true == valObj( $objCallResult, 'CCallResult' ) ) {
			return $objCallResult->getName();
		}

		return '';
	}

	public function getCallReferenceId() {
		return $this->getRequiredParameters()['call_id'] ?? NULL;
	}

	public function getContactSubmissionReason() {
		return $this->getRequiredParameters()['contact_submission_reason'] ?? NULL;
	}

	public function getContactSubmissionMoveOutReason() {
		return $this->getRequiredParameters()['contact_submission_move_out_reason'] ?? NULL;
	}

	public function getContactSubmissionMoveOutDate() {
		return $this->getRequiredParameters()['contact_submission_move_out_date'] ?? NULL;
	}

	public function getLeasingCenterContactSubmissionTitle() {
		$strTitle = __( 'Online Contact Form Submission' );
		$intCallId = $this->getRequiredParameters()['call_id'] ?? NULL;

		$objVoipDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::VOIP, CDatabaseUserType::PS_VOIP, CDatabaseServerType::POSTGRES, false );

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) || false == valId( $intCallId ) ) {
			return $strTitle;
		}

		$objVoipDatabase->open();
		$objCall = \Psi\Eos\Voip\CCalls::createService()->fetchCallByCidById( $this->getCid(), $intCallId, $objVoipDatabase );

		if( true == valObj( $objCall, 'CCall' ) ) {
			$objCallResult = \Psi\Eos\Voip\CCallResults::createService()->fetchCallResultById( $objCall->getCallResultId(), $objVoipDatabase );

			if( true == valObj( $objCallResult, 'CCallResult' ) ) {
				$strTitle = __( 'Leasing Center Call {%s,0}', [ $objCallResult->getName() ] );
			} else {
				$strTitle = __( 'Leasing Center Call' );
			}
		}

		$objVoipDatabase->close();

		return $strTitle;
	}

	public function getContactSubmissionPropertyBuildingNumber() {
		return $this->getRequiredParameters()['property_building_number'] ?? NULL;
	}

}
?>