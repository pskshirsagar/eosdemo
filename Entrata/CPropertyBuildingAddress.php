<?php

class CPropertyBuildingAddress extends CBasePropertyBuildingAddress {

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		return true;
	}

	public function valPropertyBuildingId() {
		return true;
	}

	public function valAddressTypeId( $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_intAddressTypeId ) || ( 1 > $this->m_intAddressTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', __( 'Address type does not appear valid.' ) ) );

		} elseif( false == $this->valCheckDuplicationByAddressTypeId( $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', __( 'Address already exists with selected address type.' ) ) );
		}

		return $boolIsValid;
	}

 	public function valTimeZoneId() {
		$boolIsValid = true;

		if( false == is_null( $this->m_intTimeZoneId ) && ( 1 > $this->m_intTimeZoneId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_zone_id', __( 'Time zone does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine1( $boolIsDuplicatePropertyBuildingAddress = false, $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strStreetLine1 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Street line 1 is required.' ) ) );
		}

		if( true == $boolIsDuplicatePropertyBuildingAddress && false == $this->valCheckDuplicationByPropertyBuildingAddress( $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Address already exists.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStreetLine2() {
		return true;
	}

	public function valStreetLine3() {
		return true;
	}

	public function valCity() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCity ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCounty() {
		return true;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( false == isset( $this->m_strStateCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State code is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valProvince() {
		return true;
	}

	public function valPostalCode( $strCountryCode ) {
		$boolIsValid = true;
		$strFieldName = ( 'US' == $strCountryCode ) ? __( 'zip code' ) : __( 'postal code' );

		// Validation example
		if( false == isset( $this->m_strPostalCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s, 0} is required.', [ \Psi\CStringService::singleton()->ucfirst( $strFieldName ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valIsVerified() {
		return true;
	}

	public function valCountryCode() {
		return true;
	}

	public function valLongitude() {
		return true;
	}

	public function valLatitude() {
		return true;
	}

	public function valIsPublished() {
		return true;
	}

	public function valOrderNum() {
		return true;
	}

	public function valCheckDuplicationByPropertyBuildingAddress( $objDatabase = NULL ) {
		$strSqlWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND property_id = ' . ( int ) $this->getPropertyId() . '
							AND property_building_id = ' . ( int ) $this->getPropertyBuildingId() . '
							AND lower( street_line1 ) = \'' . trim( \Psi\CStringService::singleton()->strtolower( addslashes( $this->getStreetLine1() ) ) . '\' ' );

		$strSqlWhere .= ( 0 < $this->getId() ) ? ' AND id <> ' . ( int ) $this->getId() : '';

		$intPropertyBuildingAddressCount = CPropertyBuildingAddresses::fetchPropertyBuildingAddressCount( $strSqlWhere, $objDatabase );

		return !( 0 < $intPropertyBuildingAddressCount );
	}

	public function valCheckDuplicationByAddressTypeId( $objDatabase = NULL ) {
		$strSqlWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND property_id = ' . ( int ) $this->getPropertyId() . '
							AND address_type_id = ' . ( int ) $this->getAddressTypeId() . '
							AND property_building_id = ' . ( int ) $this->getPropertyBuildingId();

		$strSqlWhere .= ( 0 < $this->getId() ) ? ' AND id <> ' . ( int ) $this->getId() : '';

		$intPropertyBuildingAddressCount = CPropertyBuildingAddresses::fetchPropertyBuildingAddressCount( $strSqlWhere, $objDatabase );

		return !( 0 < $intPropertyBuildingAddressCount );
	}

	public function valDeleteAddress() {
		$boolIsValid = true;

		if( CAddressType::PRIMARY == $this->getAddressTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', __( 'Primary address can not be deleted.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $arrintOccupancyTypeIds = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAddressTypeId( $objDatabase );
				$boolIsValid &= $this->valStreetLine1( $boolIsDuplicatePropertyBuildingAddress = true, $objDatabase );
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode();
				if( true == valArr( $arrintOccupancyTypeIds ) && true == in_array( COccupancyType::COMMERCIAL, $arrintOccupancyTypeIds ) ) {
					$boolIsValid &= $this->valPostalCode( $this->getCountryCode() );
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeleteAddress();
				break;

			case 'misc_location':
				$boolIsValid &= $this->valTimeZoneId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function buildDisplayAddress( $boolMultiLine = false ) {
		$strSeparator = $boolMultiLine ? "\n" : ', ';

		$strDisplayAddress = $this->getStreetLine1() . $strSeparator;

		if( false == is_null( $this->getStreetLine2() ) ) {
			$strDisplayAddress .= $this->getStreetLine2() . $strSeparator;
		}

		if( false == is_null( $this->getStreetLine3() ) ) {
			$strDisplayAddress .= $this->getStreetLine3() . $strSeparator;
		}

		$strDisplayAddress .= $this->getCity() . ', ';

		if( true == in_array( $this->getCountryCode(), [ 'US', 'CA' ] ) || true == is_null( $this->getCountryCode() ) ) {
			$strDisplayAddress .= $this->getStateCode() . ' ';
		} elseif( false == is_null( $this->getProvince() ) ) {
			$strDisplayAddress .= $this->getProvince() . ' ';
		}

		$strDisplayAddress .= $this->getPostalCode();

		if( false == is_null( $this->getCountryCode() ) ) {
			$strDisplayAddress .= ' ' . $this->getCountryCode();
		}

		return $strDisplayAddress;
	}

}
?>