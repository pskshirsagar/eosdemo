<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyGroupPropertyGroups
 * Do not add any new functions to this class.
 */

class CCompanyGroupPropertyGroups extends CBaseCompanyGroupPropertyGroups {

	public static function fetchEnabledCompanyGroupPropertyGroupsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		$strSql = 'SELECT
						cgpg.*
					FROM
						company_group_property_groups cgpg
						JOIN property_groups pg ON pg.cid = cgpg.cid AND pg.id = cgpg.property_group_id
						JOIN property_group_types pgt ON pgt.cid = pg.cid AND pgt.id = pg.property_group_type_id AND pgt.deleted_by IS NULL AND pgt.deleted_on IS NULL
					 	LEFT JOIN property_group_associations pga ON pga.cid = pg.cid AND pga.property_group_id = pg.id AND pgt.system_code = \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\'
						LEFT JOIN properties p ON p.cid = pg.cid AND p.id = pga.property_id ' . ( ( false == $boolShowDisabledData ) ? 'AND p.is_disabled = 0' : '' ) . '
					WHERE
						cgpg.cid = ' . ( int ) $intCid . '
						AND cgpg.company_group_id = ' . ( int ) $intCompanyGroupId . '
						AND ( pgt.system_code IS NULL OR pgt.system_code != \'' . CPropertyGroupType::PROPERTY_LIST_SYSTEM_CODE . '\' OR p.id IS NOT NULL )
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL;';

		return parent::fetchCompanyGroupPropertyGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupPropertyGroupsByCompanyGroupIdsByCid( $arrintCompanyGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						cgpg.*
					FROM
						company_group_property_groups cgpg
						JOIN property_groups pg ON ( pg.cid = cgpg.cid AND cgpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cgpg.cid = ' . ( int ) $intCid . '
						AND cgpg.company_group_id IN ( ' . implode( ', ', $arrintCompanyGroupIds ) . ' )
					ORDER BY
						cgpg.property_group_id DESC';

		return parent::fetchCompanyGroupPropertyGroups( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupPropertyGroupsByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cgpg.*
					FROM
						company_group_property_groups cgpg
						JOIN property_groups pg ON ( pg.cid = cgpg.cid AND cgpg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
					WHERE
						cgpg.company_group_id = ' . ( int ) $intCompanyGroupId . '
						AND cgpg.cid = ' . ( int ) $intCid;

		return self::fetchCompanyGroupPropertyGroups( $strSql, $objDatabase );
	}

}
?>