<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCandidateStatusTypes
 * Do not add any new functions to this class.
 */

class CCandidateStatusTypes extends CBaseCandidateStatusTypes {

	public static function fetchCandidateStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCandidateStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCandidateStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCandidateStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>