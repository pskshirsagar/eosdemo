<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileBatches
 * Do not add any new functions to this class.
 */

class CFileBatches extends CBaseFileBatches {

	public static function fetchUnexportedFileBatchesByCidByCompanyUserId( $intCid, $intCompanyUserId, $objDatabase ) {
		$strSql = '	SELECT fb.*
					FROM file_batches fb
						LEFT JOIN file_exports fe ON (fe.file_batch_id = fb.id AND fb.cid = fe.cid)
					WHERE
							fb.cid = ' . ( int ) $intCid . '
						AND fb.created_by = ' . ( int ) $intCompanyUserId . '
						AND fb.file_batch_type_id <> ' . CFileBatchType::IMPORT . '
						AND fe.id IS NULL
					ORDER BY id desc
					LIMIT 1';

		return self::fetchFileBatch( $strSql, $objDatabase );
	}

	public static function fetchUnexportedFileBatchByIdByCidByCompanyUserId( $intId, $intCid, $intCompanyUserId, $objDatabase ) {
		$strSql = '	SELECT fb.*
					FROM file_batches fb
						LEFT JOIN file_exports fe ON (fe.file_batch_id = fb.id AND fb.cid = fe.cid)
					WHERE
							fb.id = ' . ( int ) $intId . '
						AND fb.cid = ' . ( int ) $intCid . '
						AND fb.created_by = ' . ( int ) $intCompanyUserId . '
						AND fe.id IS NULL';

		return self::fetchFileBatch( $strSql, $objDatabase );
	}

	public static function fetchCustomFileBatchByCidByIdByCompanyUserId( $intCid, $intId, $intCompanyUserId, $objDatabase ) {

		$strSql = ' SELECT
						fb.*
					FROM
						file_batches fb
						JOIN file_exports fe ON ( fb.id = fe.file_batch_id AND fe.cid = fb.cid )
					WHERE
						fe.id = ' . ( int ) $intId . '
						AND fe.cid = ' . ( int ) $intCid . '
						AND fe.expires_on IS NULL
						AND fe.is_exported = 1
						AND fe.created_by = ' . ( int ) $intCompanyUserId . '
						ORDER BY fe.created_on DESC';

		return parent::fetchFileBatch( $strSql, $objDatabase );
	}

	public static function fetchFileBatchesByIds( $intCid, $arrintFileBatchIds, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						file_batches 
					WHERE
						cid = ' . ( int ) $intCid . '
						AND  id IN( ' . implode( ',', $arrintFileBatchIds ) . ' )';
		return parent::fetchFileBatches( $strSql, $objDatabase );
	}

}
?>