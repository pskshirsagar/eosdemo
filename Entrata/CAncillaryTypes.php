<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAncillaryTypes
 * Do not add any new functions to this class.
 */

class CAncillaryTypes extends CBaseAncillaryTypes {

	public static function fetchAncillaryTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CAncillaryType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAncillaryType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CAncillaryType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>