<?php

class CDocumentType extends CBaseDocumentType {

	const WEB_PAGE					= 1;
	const FORM						= 2;
	const MERGE_DOCUMENT			= 3;
	const FILES						= 4;
	const PACKET					= 5;
}
?>