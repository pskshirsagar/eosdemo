<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFeeTemplateRules
 * Do not add any new functions to this class.
 */

class CFeeTemplateRules extends CBaseFeeTemplateRules {

	public static function fetchFeeTemplateRulesByFeeTemplateIdByFeeRuleTypeIdsByCid( $intFeeTemplateId, $arrintFeeRuleTypeIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintFeeRuleTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						fee_template_rules
					WHERE
						fee_template_id = ' . ( int ) $intFeeTemplateId . '
						AND cid = ' . ( int ) $intCid . '
						AND fee_rule_type_id IN ( ' . implode( ',', $arrintFeeRuleTypeIds ) . ' )
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						id';

		return self::fetchFeeTemplateRules( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplateRulesByFeeTemplateIdsByCid( $arrintFeeTemplateIds, $intCid, $objClientDatabase ) {
		if( false == valIntArr( $arrintFeeTemplateIds ) ) return NULL;

		$strSql = 'SELECT
						ftr.*,
						frt.name AS fee_rule_type_name
					FROM
						fee_template_rules ftr
						JOIN fee_rule_types frt ON ( ftr.fee_rule_type_id = frt.id )
					WHERE
						ftr.cid = ' . ( int ) $intCid . '
						AND ftr.fee_template_id IN ( ' . implode( ',', $arrintFeeTemplateIds ) . ' )
						AND ftr.deleted_by IS NULL
						AND ftr.deleted_on IS NULL
					ORDER BY
						ftr.id';

		return self::fetchFeeTemplateRules( $strSql, $objClientDatabase );
	}

	public static function fetchFeeTemplateRulesByFeeTemplateIdByCid( $intFeeTemplateId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						ftr.*,
						frt.name AS fee_rule_type_name
					FROM
						fee_template_rules ftr
						JOIN fee_rule_types frt ON ( ftr.fee_rule_type_id = frt.id )
					WHERE
						ftr.cid = ' . ( int ) $intCid . '
						AND ftr.fee_template_id = ' . ( int ) $intFeeTemplateId . '
					ORDER BY
						ftr.id';

		return self::fetchFeeTemplateRules( $strSql, $objClientDatabase );
	}

}
?>