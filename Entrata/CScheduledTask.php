<?php

class CScheduledTask extends CBaseScheduledTask {

	private $m_objScheduledTaskType;
	/** @var CScheduleTypeInterface */
	protected $m_objScheduleType;
	protected $m_objDependencyContainer;
	protected $m_objStorageGateway;

	protected $m_arrstrScheduleDetails;
	protected $m_arrstrTaskDetails;

	protected $m_strPropertyName;
	protected $m_arrmixSystemMessageMetaData;

	protected $m_boolExecutionFailed = false;
	protected $m_boolIsResidentsDisabled;
	protected $m_boolIsProspectsDisabled;
	protected $m_boolIsEmployeesDisabled;
	protected $m_boolIsSendEmailToProspect;
	protected $m_boolIsSendEmailToResident;
	protected $m_boolIsSendEmailToProperty;

	protected $m_boolSendsToEmployee;
	protected $m_boolSendsToResident;
	protected $m_boolSendsToProspect;
	protected $m_boolIsDefaultSystemMessage;
	protected $m_boolIsDefaultSmsSystemMessage;

	protected $m_intIsEnableSmsCount;
	protected $m_intSmsTextCount;
	protected $m_intMessageTypeId;

	protected $m_intSystemEmailTypeId;
	protected $m_intPropertyCount;
	protected $m_intIsSendEmailToProspectCount;
	protected $m_intIsSendEmailToResidentCount;
	protected $m_intIsSendEmailToPropertyCount;

	protected $m_strTextMessageContent;
	protected $m_strDefaultDetails;
	protected $m_jsonDefaultDetails;

	protected $m_intSystemMessageAudienceId;
	/**
	 * Get Functions
	 *
	 */

	public function getIsEnableSmsCount() {
		return $this->m_intIsEnableSmsCount;
	}

	public function getSmsTextCount() {
		return $this->m_intSmsTextCount;
	}

	public function getTextMessageContent() {
		return $this->m_strTextMessageContent;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getExecutionFailed() {
		return $this->m_boolExecutionFailed;
	}

	public function getScheduleType() {

		if( true == is_null( $this->m_objScheduleType ) ) {

			switch( $this->m_intScheduleTypeId ) {

				case CScheduleType::DAILY:
					$this->m_objScheduleType = new CDailyScheduleType();
					break;

				case CScheduleType::WEEKLY:
					$this->m_objScheduleType = new CWeeklyScheduleType();
					break;

				case CScheduleType::MONTHLY:
					$this->m_objScheduleType = new CMonthlyScheduleType();
					break;

				case CScheduleType::DAY_OF_THE_MONTH:
					$this->m_objScheduleType = new CDayOfTheMonthScheduleType();
					break;

				case CScheduleType::YEARLY:
					$this->m_objScheduleType = new CYearlyScheduleType();
					break;

				case CScheduleType::EVENT_TRIGGER:
					$this->m_objScheduleType = new CEventTriggerScheduleType();
					break;

				default:
					// default action will go here
			}

			if( false == is_null( $this->m_objScheduleType ) ) {
				$this->m_objScheduleType->parseDetails( $this->getScheduleDetails() );
			}
		}

		return $this->m_objScheduleType;
	}

	public function getScheduledTaskType() {

		if( true == is_null( $this->m_objScheduledTaskType ) ) {

			switch( $this->m_intScheduledTaskTypeId ) {

				case CScheduledTaskType::INSPECTION:
					$this->m_objScheduledTaskType = new CInspectionTaskType();
					break;

				case CScheduledTaskType::MAINTENANCE_REQUEST:
					$this->m_objScheduledTaskType = new CMaintenanceRequestTaskType();
					break;

				default:
					// default action will go here
			}

			if( false == is_null( $this->m_objScheduledTaskType ) ) {
				$this->m_objScheduledTaskType->parseDetails( $this->getTaskDetails() );
			}
		}

		return $this->m_objScheduledTaskType;
	}

	public function getDependencyContainer() {
		return $this->m_objDependencyContainer;
	}

	public function getIsResidentDisabled() {
		return $this->m_boolIsResidentsDisabled;
	}

	public function getIsProspectDisabled() {
		return $this->m_boolIsProspectsDisabled;
	}

	public function getIsEmployeeDisabled() {
		return $this->m_boolIsEmployeesDisabled;
	}

	public function getSendsToProspect() {
		return $this->m_boolSendsToProspect;
	}

	public function getSendsToResident() {
		return $this->m_boolSendsToResident;
	}

	public function getSendsToEmployee() {
		return $this->m_boolSendsToEmployee;
	}

	public function getIsDefaultSystemMessage() {
		return $this->m_boolIsDefaultSystemMessage;
	}

	public function getIsDefaultSmsSystemMessage() {
		return $this->m_boolIsDefaultSmsSystemMessage;
	}

	public function getSystemMessageMetaData() {
		return $this->m_arrmixSystemMessageMetaData;
	}

	public function getSystemEmailTypeId() {
		return $this->m_intSystemEmailTypeId;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getIsSendEmailToProspectCount() {
		return $this->m_intIsSendEmailToProspectCount;
	}

	public function getIsSendEmailToResidentCount() {
		return $this->m_intIsSendEmailToResidentCount;
	}

	public function getIsSendEmailToPropertyCount() {
		return $this->m_intIsSendEmailToPropertyCount;
	}

	public function getDefaultDetails() {
		if( true == isset( $this->m_strDefaultDetails ) ) {
			$this->m_jsonDefaultDetails = CStrings::strToJson( $this->m_strDefaultDetails );
		}
		return $this->m_jsonDefaultDetails;
	}

	public function getSystemMessageAudienceId() {
		return $this->m_intSystemMessageAudienceId;
	}

	public function getStorageGateway() {
		return $this->m_objStorageGateway;
	}

	public function getMessageTypeId() {
		return $this->m_intMessageTypeId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setStorageGateway( $objStorageGateway ) {
		$this->m_objStorageGateway = $objStorageGateway;
	}

	public function setIsEnableSmsCount( $intIsEnableSmsCount ) {
		$this->m_intIsEnableSmsCount = $intIsEnableSmsCount;
	}

	public function setSmsTextCount( $intSmsTextCount ) {
		$this->m_intSmsTextCount = $intSmsTextCount;
	}

	public function setTextMessageContent( $strTextMessageContent ) {
		$this->m_strTextMessageContent = $strTextMessageContent;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setExecutionFailed( $boolExecutionFailed ) {
		$this->m_boolExecutionFailed = $boolExecutionFailed;
	}

	public function setIsResidentDisabled( $boolIsResidentDisabled ) {
		$this->m_boolIsResidentsDisabled = CStrings::strToBool( $boolIsResidentDisabled );
	}

	public function setIsProspectDisabled( $boolIsProspectDisabled ) {
		$this->m_boolIsProspectsDisabled = CStrings::strToBool( $boolIsProspectDisabled );
	}

	public function setIsEmployeeDisabled( $boolIsEmployeeDisabled ) {
		$this->m_boolIsEmployeesDisabled = CStrings::strToBool( $boolIsEmployeeDisabled );
	}

	public function setSendsToProspect( $boolSendsToProspect ) {
		$this->m_boolSendsToProspect = CStrings::strToBool( $boolSendsToProspect );
	}

	public function setSendsToResident( $boolSendsToResident ) {
		$this->m_boolSendsToResident = CStrings::strToBool( $boolSendsToResident );
	}

	public function setSendsToEmployee( $boolSendsToEmployee ) {
		$this->m_boolSendsToEmployee = CStrings::strToBool( $boolSendsToEmployee );
	}

	public function setIsSendEmailToProspectCount( $intIsSendEmailToProspectCount ) {
		$this->m_intIsSendEmailToProspectCount = $intIsSendEmailToProspectCount;
	}

	public function setIsSendEmailToResidentCount( $intIsSendEmailToResidentCount ) {
		$this->m_intIsSendEmailToResidentCount = $intIsSendEmailToResidentCount;
	}

	public function setIsSendEmailToPropertyCount( $intIsSendEmailToPropertyCount ) {
		$this->m_intIsSendEmailToPropertyCount = $intIsSendEmailToPropertyCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['property_name'] ) && $boolDirectSet ) {
			$this->m_strPropertyName = trim( $arrmixValues['property_name'] );
		} elseif( isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}

		if( isset( $arrmixValues['residents_disabled'] ) && $boolDirectSet ) {
			$this->m_boolIsResidentsDisabled = trim( $arrmixValues['residents_disabled'] );
		} elseif( isset( $arrmixValues['residents_disabled'] ) ) {
			$this->setIsResidentDisabled( $arrmixValues['residents_disabled'] );
		}

		if( isset( $arrmixValues['prospects_disabled'] ) && $boolDirectSet ) {
			$this->m_boolIsProspectsDisabled = trim( $arrmixValues['prospects_disabled'] );
		} elseif( isset( $arrmixValues['prospects_disabled'] ) ) {
			$this->setIsProspectDisabled( $arrmixValues['prospects_disabled'] );
		}

		if( isset( $arrmixValues['employees_disabled'] ) && $boolDirectSet ) {
			$this->m_boolIsEmployeesDisabled = trim( $arrmixValues['employees_disabled'] );
		} elseif( isset( $arrmixValues['employees_disabled'] ) ) {
			$this->setIsEmployeeDisabled( $arrmixValues['employees_disabled'] );
		}

		if( isset( $arrmixValues['send_to_resident'] ) && $boolDirectSet ) {
			$this->m_boolIsEmployeesDisabled = trim( $arrmixValues['send_to_resident'] );
		} elseif( isset( $arrmixValues['send_to_resident'] ) ) {
			$this->setSendsToResident( $arrmixValues['send_to_resident'] );
		}

		if( isset( $arrmixValues['send_to_prospect'] ) && $boolDirectSet ) {
			$this->m_boolIsEmployeesDisabled = trim( $arrmixValues['send_to_prospect'] );
		} elseif( isset( $arrmixValues['send_to_prospect'] ) ) {
			$this->setSendsToProspect( $arrmixValues['send_to_prospect'] );
		}

		if( isset( $arrmixValues['send_to_employee'] ) && $boolDirectSet ) {
			$this->m_boolIsEmployeesDisabled = trim( $arrmixValues['send_to_employee'] );
		} elseif( isset( $arrmixValues['send_to_employee'] ) ) {
			$this->setSendsToEmployee( $arrmixValues['send_to_employee'] );
		}

		if( isset( $arrmixValues['property_count'] ) && $boolDirectSet ) {
			$this->m_intPropertyCount = trim( $arrmixValues['property_count'] );
		} elseif( isset( $arrmixValues['property_count'] ) ) {
			$this->setPropertyCount( $arrmixValues['property_count'] );
		}

		if( isset( $arrmixValues['sms_text_count'] ) && $boolDirectSet ) {
			$this->m_intSmsTextCount = trim( $arrmixValues['sms_text_count'] );
		} elseif( isset( $arrmixValues['sms_text_count'] ) ) {
			$this->setSmsTextCount( $arrmixValues['sms_text_count'] );
		}

		if( isset( $arrmixValues['is_enable_sms_count'] ) && $boolDirectSet ) {
			$this->m_intIsEnableSmsCount = trim( $arrmixValues['is_enable_sms_count'] );
		} elseif( isset( $arrmixValues['is_enable_sms_count'] ) ) {
			$this->setIsEnableSmsCount( $arrmixValues['is_enable_sms_count'] );
		}

		if( isset( $arrmixValues['is_send_email_to_prospect_count'] ) && $boolDirectSet ) {
			$this->m_intIsSendEmailToProspectCount = trim( $arrmixValues['is_send_email_to_prospect_count'] );
		} elseif( isset( $arrmixValues['is_send_email_to_prospect_count'] ) ) {
			$this->setIsSendEmailToProspectCount( $arrmixValues['is_send_email_to_prospect_count'] );
		}

		if( isset( $arrmixValues['is_send_email_to_resident_count'] ) && $boolDirectSet ) {
			$this->m_intIsSendEmailToResidentCount = trim( $arrmixValues['is_send_email_to_resident_count'] );
		} elseif( isset( $arrmixValues['is_send_email_to_resident_count'] ) ) {
			$this->setIsSendEmailToResidentCount( $arrmixValues['is_send_email_to_resident_count'] );
		}

		if( isset( $arrmixValues['is_send_email_to_property_count'] ) && $boolDirectSet ) {
			$this->m_intIsSendEmailToPropertyCount = trim( $arrmixValues['is_send_email_to_property_count'] );
		} elseif( isset( $arrmixValues['is_send_email_to_property_count'] ) ) {
			$this->setIsSendEmailToPropertyCount( $arrmixValues['is_send_email_to_property_count'] );
		}

		if( true == isset( $arrmixValues['is_send_email_to_prospect'] ) ) {
			$this->setIsSendEmailToProspect( $arrmixValues['is_send_email_to_prospect'] );
		}

		if( true == isset( $arrmixValues['is_send_email_to_resident'] ) ) {
			$this->setIsSendEmailToResident( $arrmixValues['is_send_email_to_resident'] );
		}

		if( true == isset( $arrmixValues['is_send_email_to_property'] ) ) {
			$this->setIsSendEmailToProperty( $arrmixValues['is_send_email_to_property'] );
		}

		if( true == isset( $arrmixValues['default_details'] ) ) {
			$this->setDefaultDetails( $arrmixValues['default_details'] );
		}

		if( true == isset( $arrmixValues['system_message_audience_id'] ) ) {
			$this->setSystemMessageAudienceId( $arrmixValues['system_message_audience_id'] );
		}
		return;
	}

	public function setScheduleType( $objScheduleType ) {
		$this->m_objScheduleType = $objScheduleType;
	}

	public function setDependencyContainer( $objDependencyContainer ) {
		$this->m_objDependencyContainer = $objDependencyContainer;
	}

	public function setIsDefaultSystemMessage( $boolIsDefaultSystemMessage ) {
		$this->m_boolIsDefaultSystemMessage = $boolIsDefaultSystemMessage;
	}

	public function setIsDefaultSmsSystemMessage( $boolIsDefaultSmsSystemMessage ) {
		$this->m_boolIsDefaultSmsSystemMessage = $boolIsDefaultSmsSystemMessage;
	}

	public function setSystemMessageMetaData( $arrmixSystemMessageMetaData ) {
		$this->m_arrmixSystemMessageMetaData = $arrmixSystemMessageMetaData;
	}

	public function setMessageTypeId( $intMessageTypeId ) {
		$this->m_intMessageTypeId = $intMessageTypeId;
	}

	public function setSystemEmailTypeId( $intSystemEmailTypeId ) {
		$this->m_intSystemEmailTypeId = $intSystemEmailTypeId;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setDefaultDetails( $strDefaultDetails ) {
		$this->m_strDefaultDetails = $strDefaultDetails;
	}

	public function setSystemMessageAudienceId( $intSystemMessageId ) {
		$this->m_intSystemMessageAudienceId = $intSystemMessageId;
	}

	/**
	 *  Create Functions
	 *
	 */

	public static function createConcreteScheduledTask( $arrmixValues ) {

    	if( false == valArr( $arrmixValues ) || false == array_key_exists( 'scheduled_task_type_id', $arrmixValues ) ) {
    		return new CScheduledTask();
    	}

    	$strClassName = 'C' . CScheduledTaskType::getIdToCamelizeName( $arrmixValues['scheduled_task_type_id'] ) . 'ScheduledTask';

    	if( false == valStr( $strClassName ) ) {
    		return new CScheduledTask();
    	}

    	return new $strClassName();
	}

	/**
	 *  Validate Function
	 *
	 */

	public function valTaskDetails() {
		$boolIsValid = true;

		if( false == valObj( $this->getTaskDetails(), 'stdClass' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_details', __( 'Task details is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStartOn() {
		$boolIsValid = true;

		if( false == isset( $this->m_strStartOn ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_on', __( 'Start On is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valNextRunOn() {
		$boolIsValid = true;

		if( false == isset( $this->m_strNextRunOn ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_run_on', __( 'Next Run On is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGroupCount() {
		$boolIsValid = true;

		if( false == valId( $this->getScheduledTaskType()->getGroupCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_count', __( 'Please enter valid number of units in a bulk group.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClient = NULL, $objDatabase = NULL, $boolValidateGroupCount = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valStartOn();
				$boolIsValid &= $this->valNextRunOn();

				if( true == $boolValidateGroupCount ) {
					$boolIsValid &= $this->valGroupCount();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTaskDetails();

				if( true == $boolValidateGroupCount ) {
					$boolIsValid &= $this->valGroupCount();
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->getScheduleType()->validate( $strAction );
				$this->addErrorMsgs( $this->getScheduleType()->getErrorMsgs() );

				$boolIsValid &= $this->getScheduledTaskType()->validate( $strAction );
				$this->addSuccessMsg( $this->getScheduledTaskType()->getErrorMsgs() );
				break;

			default:
				// default action will go here
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function compose() {
		$this->composeScheduleDetails();
		$this->composeTaskDetails();
	}

	public function composeScheduleDetails() {
		// Inherited class has to provide implementation. [KAL]
	}

	public function composeTaskDetails() {
		// Inherited class has to provide implementation. [KAL]
	}

	public function parse() {
		$this->parseScheduleDetails();
		$this->parseTaskDetails();
	}

	public function parseScheduleDetails() {
		$this->parseDetails( $this->getScheduleDetails() );
	}

	public function parseTaskDetails() {
		$this->parseDetails( $this->getTaskDetails() );
	}

	public function parseDetails( $strDetails ) {
		// Inherited class has to provide implementation. [KAL]
	}

	public function delete( $intUserId, $objDatabase, $boolIsEndOnUpdate = true ) {

		$this->setDeletedOn( 'NOW()' );
		$this->setDeletedBy( $intUserId );

		if( true == $boolIsEndOnUpdate ) {
			$this->setEndOn( 'NOW()' );
		}

		if( $this->update( $intUserId, $objDatabase ) ) {
			return true;
		}
	}

	public static function createScheduledTask( $intPropertyId, $objScheduledTask ) {
		$objScheduledTaskClone	= new CScheduledTask();
		$objScheduledTaskClone->setPropertyId( $intPropertyId );
		$objScheduledTaskClone->setCid( $objScheduledTask->getCId() );
		$objScheduledTaskClone->setScheduleTypeId( $objScheduledTask->getScheduleTypeId() );
		$objScheduledTaskClone->setEventSubTypeId( $objScheduledTask->getEventSubTypeId() );
		$objScheduledTaskClone->setScheduleDetails( $objScheduledTask->getScheduleDetails() );
		$objScheduledTaskClone->setScheduledTaskTypeId( $objScheduledTask->getScheduledTaskTypeId() );
		$objScheduledTaskClone->setEventTypeId( $objScheduledTask->getEventTypeId() );
		$objScheduledTaskClone->setSystemEmailTypeId( $objScheduledTask->getSystemEmailTypeId() );

		return $objScheduledTaskClone;
	}

	public function createFileAttachments() {
		$arrobjFileAttachments = [];
		$arrmixSystemMessageMetaData = ( array ) $this->getSystemMessageMetaData();

		if( true == array_key_exists( 'file_path', $arrmixSystemMessageMetaData ) ) {
			$arrstrSystemGeneratedAttachments = ( array ) $arrmixSystemMessageMetaData['file_path'];

			foreach( $arrstrSystemGeneratedAttachments as $strSystemGeneratedAttachment ) {
				$objEmailAttachment	= new CEmailAttachment();
				$arrstrFileInfo		= pathinfo( $strSystemGeneratedAttachment );
				$objFileExtension	= \Psi\Eos\Entrata\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileInfo['extension'], $this->m_objDatabase );

				if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
					continue;
				}

				$objEmailAttachment->setFileExtensionId( $objFileExtension->getId() );
				$objEmailAttachment->setTitle( $arrstrFileInfo['basename'] );
				$objEmailAttachment->setFileName( $arrstrFileInfo['basename'] );

				if( true == CFileIo::fileExists( $strSystemGeneratedAttachment ) ) {
					$objEmailAttachment->setFileSize( CFileIo::getFileSize( $strSystemGeneratedAttachment ) );
				}

				$objEmailAttachment->setFilePath( $arrstrFileInfo['dirname'] );

				$arrobjFileAttachments[] = $objEmailAttachment;
			}
		}

		return $arrobjFileAttachments;
	}

	public function removeFileAttachments( $intCompanyUserId = NULL ) {
		if( true == is_null( $this->getStorageGateway() ) ) {
			return;
		}

		$arrmixSystemMessageMetaData = ( array ) $this->getSystemMessageMetaData();

		if( false == array_key_exists( 'file_path', $arrmixSystemMessageMetaData ) ) {
			return false;
		}

		$arrstrSystemGeneratedAttachments = ( array ) $arrmixSystemMessageMetaData['file_path'];

		if( false == valArr( $arrstrSystemGeneratedAttachments ) ) return false;

		$arrintFileIds = ( array ) array_keys( $arrstrSystemGeneratedAttachments );

		$arrobjFiles = ( array ) CFiles::fetchTemporaryFilesByIdsByCid( $arrintFileIds, $this->getCid(), $this->m_objDatabase );

		if( true == valObj( $this->m_objCompanyUser, CCompanyUser::class ) ) {
			$intCompanyUserId = $this->m_objCompanyUser->getId();
		}

		if( false == valId( $intCompanyUserId ) ) {
			$intCompanyUserId = SYSTEM_USER_ID;
		}

		foreach( $arrstrSystemGeneratedAttachments as $intFileId => $strSystemGeneratedAttachment ) {
			if( false == array_key_exists( $intFileId, $arrobjFiles ) ) continue;

			$objFile = $arrobjFiles[$intFileId];
			$arrmixRequest = $objFile->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest();
			$this->getStorageGateway()->deleteObject( $arrmixRequest );
			$objFile->deleteStoredObject( $intCompanyUserId, $this->m_objDatabase );
		}

		if( false == CFiles::bulkDelete( $arrobjFiles, $this->m_objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function setJsonbFieldValue( $strMethodName, $strKey, $mixValue ) {
		$strGetMethodName = 'get' . $strMethodName;
		$strSetMethodName = 'set' . $strMethodName;

		$objStdClass = $this->$strGetMethodName();

		if( false == valObj( $objStdClass, 'stdClass' ) ) {
			$objStdClass = new stdClass();
		}

		$objStdClass->$strKey = $mixValue;

		$this->$strSetMethodName( $objStdClass );
	}

	public function getJsonbFieldValue( $strMethodName, $strKey ) {
		$strMethodName = 'get' . $strMethodName;

		return ( false == is_null( $this->$strMethodName() ) && true == property_exists( $this->$strMethodName(), $strKey ) ? $this->$strMethodName()->$strKey : NULL );
	}

}
?>