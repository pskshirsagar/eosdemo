<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenuePostRules
 * Do not add any new functions to this class.
 */

class CRevenuePostRules extends CBaseRevenuePostRules {

	const POST_RULE_TARGET_STANDARD = 'standard';
	const POST_RULE_TARGET_RENEWAL = 'renewal';
	const POST_RULE_CATEGORY = 'post';

	public static function fetchRevenuePostRulesByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase, $intIsRenewal = 0, $strPostRuleCategory = SELF::POST_RULE_CATEGORY ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		if( 1 == $intIsRenewal ) {
			$strSqlCondition = 'AND rps.post_rule_target = \'' . self::POST_RULE_TARGET_RENEWAL . '\'';
		} else {
			$strSqlCondition = 'AND rps.post_rule_target = \'' . self::POST_RULE_TARGET_STANDARD . '\'';
		}
		$strSqlCondition .= 'AND rps.post_rule_category = \'' . $strPostRuleCategory . '\' AND rps.post_rule_category is NOT NULL';
		$strSql = 'SELECT * FROM
						revenue_post_rules rps
					WHERE
						rps.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND rps.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) '
		          . $strSqlCondition;

		return self::fetchRevenuePostRules( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchRevenuePostRulesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						revenue_post_rules rps
					WHERE
						rps.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid =  ' . ( int ) $intCid . '';

		return self::fetchRevenuePostRules( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRulesByPropertyIdByCidByRuleTarget( $intPropertyId, $intCid, $strRuleTarget, $strPostRuleCategory = 'post', $objDatabase ) {
		$strCondition = ' AND post_rule_target = \'standard\'';
		if( 'renewal' == $strRuleTarget ) {
			$strCondition = ' AND post_rule_target = \'renewal\'';
		}

		$strSql = 'SELECT
						*
					FROM
						revenue_post_rules
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid =  ' . ( int ) $intCid . '
						AND post_rule_category = \'' . $strPostRuleCategory . '\'' . $strCondition;
		return self::fetchRevenuePostRules( $strSql, $objDatabase );
	}

	public static function fetchRevenuePostRulesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $strOrderByClause = NULL ) {
		return self::fetchRevenuePostRules( sprintf( 'SELECT * FROM revenue_post_rules WHERE property_id = %d AND cid = %d %s', ( int ) $intPropertyId, ( int ) $intCid, ( string ) $strOrderByClause ), $objDatabase );
	}

}
?>