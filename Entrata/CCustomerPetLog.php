<?php

class CCustomerPetLog extends CBaseCustomerPetLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerPetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorCustomerPetLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPetTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantApplicationPetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerAnimalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBreed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valColor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWeight() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAge() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGender() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLicenseCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLicenseNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateOfLastShots() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsHouseBroken() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPetSpayedOrNeutered() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAssistanceAnimal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPetSpayedOrNeuteredDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOwnersName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialProvisions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPetNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>