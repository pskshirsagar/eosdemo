<?php

class CLeaseCamPool extends CBaseLeaseCamPool {

	protected $m_strCommercialClauseReference;
	protected $m_strPropertyCamPoolName;
	protected $m_strCustomerNameFull;
	protected $m_strCamPoolComment;

	protected $m_intSuiteArea;

	protected $m_fltShareOfAnnualBudget;
	protected $m_fltProRataValue;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['commercial_clause_reference'] ) && $boolDirectSet ) {
			$this->m_strCommercialClauseReference = trim( $arrmixValues['commercial_clause_reference'] );
		} elseif( isset( $arrmixValues['commercial_clause_reference'] ) ) {
			$this->setCommercialClauseReference( $arrmixValues['commercial_clause_reference'] );
		}

		if( isset( $arrmixValues['property_cam_pool_name'] ) && $boolDirectSet ) {
			$this->m_strPropertyCamPoolName = trim( $arrmixValues['property_cam_pool_name'] );
		} elseif( isset( $arrmixValues['property_cam_pool_name'] ) ) {
			$this->setPropertyCamPoolName( $arrmixValues['property_cam_pool_name'] );
		}

		if( true == isset( $arrmixValues['customer_name_full'] ) ) $this->setCustomerNameFull( $arrmixValues['customer_name_full'] );
		if( true == isset( $arrmixValues['suite_area'] ) ) $this->setSuiteArea( $arrmixValues['suite_area'] );
		if( true == isset( $arrmixValues['cam_pool_comment'] ) ) $this->setCamPoolComment( $arrmixValues['cam_pool_comment'] );
	}

	public function valCid() {
		return true;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( !valId( $this->m_intPropertyId ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		if( !valId( $this->m_intLeaseId ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'Lease is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valLeaseIntervalId() {
		$boolIsValid = true;
		if( !valId( $this->m_intLeaseIntervalId ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_interval_id', __( 'Lease interval is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPropertyCamPoolId() {
		$boolIsValid = true;
		if( !valId( $this->m_intPropertyCamPoolId ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_cam_pool_id', __( 'Property CAM pool is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCapAmount() {
		$boolIsValid = true;
		if( isset( $this->m_intCapAmount ) && ( 0 > $this->m_intCapAmount || 100 < $this->m_intCapAmount ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cap_amount', __( 'Pool Reimbursement Limit should be between 0 to 100.' ) ) );
		}
		return $boolIsValid;
	}

	public function getCommercialClauseReference() {
		return $this->m_strCommercialClauseReference;
	}

	public function setCommercialClauseReference( string $strCommercialClauseReference ) {
		return $this->m_strCommercialClauseReference = $strCommercialClauseReference;
	}

	public function getPropertyCamPoolName() {
		return $this->m_strPropertyCamPoolName;
	}

	public function setPropertyCamPoolName( string $strPropertyCamPoolName ) {
		return $this->m_strPropertyCamPoolName = $strPropertyCamPoolName;
	}

	public function getCustomerNameFull() {
		return $this->m_strCustomerNameFull;
	}

	public function setCustomerNameFull( $strCustomerNameFull ) {
		$this->m_strCustomerNameFull = $strCustomerNameFull;
	}

	public function getSuiteArea() {
		return $this->m_intSuiteArea;
	}

	public function setSuiteArea( $intSuiteArea ) {
		$this->m_intSuiteArea = $intSuiteArea;
	}

	public function getShareOfAnnualBudget() {
		return $this->m_fltShareOfAnnualBudget;
	}

	public function getCamPoolComment() {
		return $this->m_strCamPoolComment;
	}

	public function getProRataValue() {
		return $this->m_fltProRataValue;
	}

	public function setShareOfAnnualBudget( $fltShareOfAnnualBudget ) {
		return $this->m_fltShareOfAnnualBudget = $fltShareOfAnnualBudget;
	}

	public function setProRataValue( $fltProRataValue ) {
		$this->m_fltProRataValue = $fltProRataValue;
	}

	public function setCamPoolComment( $strCamPoolComment ) {
		return $this->m_strCamPoolComment = $strCamPoolComment;
	}

	public function validate( string $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valLeaseIntervalId();
				$boolIsValid &= $this->valPropertyCamPoolId();
				$boolIsValid &= $this->valCapAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( false === valId( $intCurrentUserId ) || false === valObj( $objDatabase, 'CDatabase' ) ) return false;

		$this->createCamPoolsActivityLog( $this, $intCurrentUserId, CEventSubType::CAM_POOL_DELETED, $objDatabase );

		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->createCamPoolsActivityLog( $this, $intCurrentUserId, CEventSubType::CAM_POOL_ADDED, $objDatabase );

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objOriginalLeaseCamPool = $this->fetchOriginalLeaseCamPool( $objDatabase );
		$this->createCamPoolsActivityLog( $this, $intCurrentUserId, CEventSubType::CAM_POOL_UPDATED, $objDatabase, $objOriginalLeaseCamPool, $this );

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function createCamPoolsActivityLog( $objLeaseCamPool, $intCompanyUserId, $intEventSubTypeId, $objDatabase, $objOldReferenceData = NULL, $objNewReferenceData = NULL ) {

		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCompanyUserId, $objLeaseCamPool->getCid(), $objDatabase );

		$objPropertyCamPool = \Psi\Eos\Entrata\CPropertyCamPools::createService()->fetchPropertyCamPoolByIdByCid( $objLeaseCamPool->getPropertyCamPoolId(), $objLeaseCamPool->getCId(), $objDatabase );

		$arrstrLogFields = [ 'CapAmount', 'IsBaseYearFixed', 'PropertyCamPoolId', 'Details' ];
		$arrmixChanges   = CTenantLibrary::compareObject( $objOldReferenceData, $objNewReferenceData, $arrstrLogFields );

		if( CEventSubType::CAM_POOL_UPDATED == $intEventSubTypeId && false == $arrmixChanges['lease_cam_pool'] ) {
			return true;
		}
		$objEventLibrary           = new CEventLibrary();
		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );

		$objEvent = $objEventLibrary->createEvent( [ $objLeaseCamPool ], CEventType::CAM_POOL, $intEventSubTypeId );
		$objEvent->setCid( $objLeaseCamPool->getCid() );
		$objEvent->setPropertyId( $objLeaseCamPool->getPropertyId() );
		$objEvent->setLeaseId( $objLeaseCamPool->getLeaseId() );
		$objEvent->setEventDatetime( CEvent::getCurrentUtcTime() );
		$objEvent->setCompanyUser( $objCompanyUser );
		$objEvent->setUpdatedBy( $objCompanyUser->getId() );
		$objEvent->setUpdatedOn( 'NOW()' );
		$objEvent->setCreatedBy( $objCompanyUser->getId() );
		$objEvent->setCreatedOn( 'NOW()' );
		$objEvent->setPoolName( $objPropertyCamPool->getName() );
		$objEvent->setDataReferenceId( $this->getId() );

		$objEventLibraryDataObject->setData( $arrmixChanges );

		$objEventLibrary->buildEventDescription( $objLeaseCamPool );

		if( false === $objEventLibrary->insertEvent( $objCompanyUser->getId(), $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function fetchOriginalLeaseCamPool( $objDatabase ) {
		return \Psi\Eos\Entrata\CLeaseCamPools::createService()->fetchLeaseCamPoolByIdByCId( $this->getId(), $this->getCid(), $objDatabase );
	}

}
