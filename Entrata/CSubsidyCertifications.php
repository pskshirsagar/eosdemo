<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertifications
 * Do not add any new functions to this class.
 */

class CSubsidyCertifications extends CBaseSubsidyCertifications {

	public static function fetchSubsidyCertificationsByApplicantionIdByCid( $intApplicantionId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicantionId ) ) {
			return [];
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicantionId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY sc.id DESC';

		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

    /**
     * @todo : Renamed this to fetchSubsidyCertificationBySubsidyCertificationTypeIdLeaseIdBySubsidyCertificationStatusTypeIdByCid in new EOD layer
     */
	public static function fetchSubsidyCertificationsBySubsidyCertificationTypeIdLeaseIdBySubsidyCertificationStatusTypeIdByCid( $intSubsidyCertificationTypeId, $intLeaseId, $arrintSubsidyCertificationStatusTypeIds, $intCid, $objDatabase, $boolIsExcludeDeleted = true ) {
		if( false == valId( $intLeaseId ) ) {
			return NULL;
		}

		$strDeletedCondition = ( true == $boolIsExcludeDeleted ) ? ' AND sc.deleted_by IS NULL AND sc.deleted_on IS NULL ' : '';

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.subsidy_certification_type_id = ' . ( int ) $intSubsidyCertificationTypeId . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.subsidy_certification_status_type_id IN(' . implode( ',', $arrintSubsidyCertificationStatusTypeIds ) . ')
						' . $strDeletedCondition . '
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY 
						sc.effective_date desc,
						sc.id DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationByApplicantionIdByCid( $intApplicantionId, $intCid, $objDatabase, $arrintExludedSubsidyCertificationType = [] ) {
		if( false == valId( $intApplicantionId ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == valArr( $arrintExludedSubsidyCertificationType ) ) ? ' AND sc.subsidy_certification_type_id NOT IN ( ' . implode( ',', $arrintExludedSubsidyCertificationType ) . ' ) ' : '';

		$strSql = 'SELECT
						sc.*,
						scon.subsidy_contract_type_id
					FROM
						subsidy_certifications sc
						LEFT JOIN subsidy_contracts scon ON ( sc.cid = scon.cid AND sc.subsidy_contract_id = scon.id )
					WHERE
						sc.application_id = ' . ( int ) $intApplicantionId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						' . $strWhereCondition . '
					ORDER BY
						sc.id DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchActiveSubsidyCertificationByApplicantionIdByCid( $intApplicantionId, $intCid, $objDatabase, $arrintExcludeCertificationStatusIds = NULL, $boolIsEffectiveDateValidationRequired = true ) {
		if( false == valId( $intApplicantionId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicantionId . '
						AND sc.cid = ' . ( int ) $intCid .
						( ( true == $boolIsEffectiveDateValidationRequired ) ? ' AND sc.effective_date <= CURRENT_DATE ' : '' ) . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL ' .
						( ( true == valArr( $arrintExcludeCertificationStatusIds ) ) ? ' AND sc.subsidy_certification_status_type_id NOT IN ( ' . implode( ',', $arrintExcludeCertificationStatusIds ) . ' )' : '' ) . '
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY sc.effective_date DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchOpenSubsidyCertificationDataByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) ) {
			return false;
		}

		$strSql = 'SELECT
						count( id )
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.subsidy_certification_status_type_id != ' . ( int ) CSubsidyCertificationStatusType::ACKNOWLEDGED . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					LIMIT 1';

		$intOpenSubsidyCertification = self::fetchColumn( $strSql, 'count', $objDatabase );

		if( 0 < $intOpenSubsidyCertification ) {
			return true;
		}

		return false;
	}

	// @TODO: We need to modify all the occurances for this function to consider future subsidy certifications o/w it will not pull the (current) future certifications

	public static function fetchCurrentSubsidyCertificationDataByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $arrintSubsidyCertificationStatusTypeIds = [], $boolConsiderFutureCertifications = false, $boolHasBeenCorrected = true ) {
		if( false == valId( $intLeaseId ) ) {
			return NULL;
		}

		$strCheckHasBeenCorrectedCondition  = ( false == $boolHasBeenCorrected ) ? ' AND sc.has_been_corrected = false ' : '';

		$arrintSubsidyCertificationStatusTypeIds     = ( false == valArr( $arrintSubsidyCertificationStatusTypeIds ) ) ? CSubsidyCertificationStatusType::$c_arrintCurrentSubsidyCertificationStatusTypeIds : $arrintSubsidyCertificationStatusTypeIds;
		$strConsiderFutureCertificationsAndCondition = ( false == $boolConsiderFutureCertifications ) ? 'AND sc.effective_date <= CURRENT_DATE' : '';

		$strSql = 'SELECT
						sc.id,
						sc.application_id,
						sc.subsidy_contract_id,
						sc.set_aside_id,
						COALESCE ( scs.subsidy_contract_type_id, ' . CSubsidyContractType::TAX_CREDIT . ' ) AS subsidy_contract_type_id,
						scs.subsidy_contract_sub_type_id,
						sc.effective_through_date,
						sc.eligibility_check_not_required,
						sc.subsidy_certification_type_id,
						sc.subsidy_certification_status_type_id
					FROM
						subsidy_certifications sc
						LEFT JOIN subsidy_contracts scs ON( scs.cid = sc.cid AND scs.id= sc.subsidy_contract_id )
						LEFT JOIN set_asides sa ON ( sa.cid = sc.cid AND sa.id = sc.set_aside_id ) 
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.subsidy_certification_status_type_id IN ( ' . implode( ',', $arrintSubsidyCertificationStatusTypeIds ) . ' )
						AND sc.cid = ' . ( int ) $intCid . '
		                ' . $strConsiderFutureCertificationsAndCondition . '
		                ' . $strCheckHasBeenCorrectedCondition . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						AND CASE
								WHEN sc.subsidy_contract_id IS NOT NULL
								THEN scs.id IS NOT NULL
								WHEN sc.set_aside_id IS NOT NULL
								THEN sa.id IS NOT NULL
							END	
					ORDER BY
						sc.effective_date DESC
					LIMIT 1';

		$arrmixSubsidyCertifcationData = fetchData( $strSql, $objDatabase );

		return $arrmixSubsidyCertifcationData[0];
	}

	public static function fetchLatestSubsidyCertificationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolHasBeenCorrected = true ) {
		if( false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strCheckHasBeenCorrectedCondition = ( false == $boolHasBeenCorrected )? ' AND sc.has_been_corrected = false ': '';

		$strSql = 'SELECT
						sc.id, 
						sc.lease_id,
						sc.effective_through_date,
						sc.subsidy_contract_id,
						sc.set_aside_id,
						sc.subsidy_certification_type_id,
						sc.subsidy_certification_status_type_id,
						st.name AS subsidy_contract_program_type,
						ssat.name AS set_aside_type_name,
						scc.contract_number,
						sa.median_income_percent,
						sa.subsidy_set_aside_type_id
					FROM
						subsidy_certifications sc
						LEFT JOIN subsidy_contracts scc ON ( scc.cid = sc.cid AND scc.property_id = sc.property_id AND scc.id = sc.subsidy_contract_id )
						LEFT JOIN set_asides sa ON ( sa.cid = sc.cid AND sa.id = sc.set_aside_id )
						LEFT JOIN subsidy_contract_types st ON ( st.id = scc.subsidy_contract_type_id )
						LEFT JOIN subsidy_set_aside_types ssat ON ( ssat.id = sa.subsidy_set_aside_type_id )
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						 ' . $strCheckHasBeenCorrectedCondition . '
						AND sc.subsidy_certification_status_type_id IN ( ' . implode( ',', CSubsidyCertificationStatusType::$c_arrintCurrentSubsidyCertificationStatusTypeIds ) . ' )
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						AND  CASE
								WHEN sc.subsidy_contract_id IS NOT NULL THEN scc.id IS NOT NULL 
								WHEN sc.set_aside_id IS NOT NULL THEN sa.id IS NOT NULL                                
								ELSE FALSE
						END
					ORDER BY
						sc.effective_date DESC, sc.id DESC
					LIMIT 1';

		$arrmixSubsidyCertifcationData = fetchData( $strSql, $objDatabase );
		return $arrmixSubsidyCertifcationData[0];
	}

	public static function fetchSubsidyCertificationsByIdsByCid( $arrintSubsidyCertificationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSubsidyCertificationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.id IN (' . implode( ',', $arrintSubsidyCertificationIds ) . ')
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL';

		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsWithRecertificationDataByIdsByCid( $arrintSubsidyCertificationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSubsidyCertificationIds ) ) {
			return NULL;
		}
		$strRemainingDaysCondition = 'DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )';

		$strSql = 'SELECT
						*,
						( sc.effective_through_date + INTERVAL \'1 day \' ) AS recertification_date,
					    CASE
					        WHEN 100 < ' . $strRemainingDaysCondition . ' 
					        THEN \'120 Days\'
					        WHEN 70 < ' . $strRemainingDaysCondition . ' 
					        THEN \'90 Days\'
					        WHEN 70 > ' . $strRemainingDaysCondition . ' 
					        THEN
					            CASE
									WHEN sc.subsidy_contract_id IS NULL AND sc.set_aside_id IS NOT NULL AND 50 > ' . $strRemainingDaysCondition . '  
									THEN \'30 Days\'
								ELSE \'60 Days\'
								END 
					    END AS notice_type
					FROM
						subsidy_certifications sc
					WHERE
						sc.id IN (' . implode( ',', $arrintSubsidyCertificationIds ) . ')
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTenhrDataByCertificationIdsByCid( $arrintSubsidyCertificationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSubsidyCertificationIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						c.company_name AS sender_name,
						pa.street_line1 AS sender_street_address,
						pa.city AS sender_city_name,
						pa.state_code AS sender_state,
						pa.postal_code AS sender_zip_code,
						psd.hud_project_name AS hud_project_name,
						subcontract.subsidy_contract_type_id AS subsidy_type,
						psd.hud_project_number AS hud_project_number,
						subcontract.contract_number,
						CONVERT_FROM( DECODE( psd.hud_project_imax_id_encrypted, \'BASE64\' ), \'UTF-8\' ) AS project_imax_id,
						owner_apl.duns_number AS owner_duns_number,
						parent_company_apl.duns_number AS parent_company_duns_number,
						o.tax_number_encrypted AS owner_tin,
						ale.tax_number_encrypted AS parent_company_tin,
						stv.name AS subsidy_tracs_version_name
					FROM
						subsidy_certifications AS subcert
						JOIN clients AS c ON ( c.id = subcert.cid )
						JOIN subsidy_contracts AS subcontract ON ( subcontract.cid = subcert.cid AND subcontract.id = subcert.subsidy_contract_id )
						JOIN property_subsidy_details AS psd ON ( psd.cid = subcert.cid AND psd.property_id = subcert.property_id )
						JOIN subsidy_tracs_versions AS stv ON ( stv.id = psd.subsidy_tracs_version_id )
						JOIN property_addresses AS pa ON ( pa.cid = subcert.cid AND pa.property_id = subcert.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
						JOIN properties AS p ON ( p.cid = subcert.cid AND p.id = subcert.property_id )
						JOIN owners AS o ON ( o.cid = subcert.cid AND o.id = p.owner_id )
						JOIN ap_payees AS ap ON ( ap.cid = o.cid AND ap.id = o.ap_payee_id AND  ap.ap_payee_type_id = ' . CApPayeeType::OWNER . ' )
						JOIN ap_payee_locations AS owner_apl ON ( owner_apl.cid = ap.cid AND owner_apl.ap_payee_id = ap.id AND  owner_apl.is_primary = TRUE AND owner_apl.disabled_on IS NULL AND owner_apl.deleted_on IS NULL )
						JOIN property_group_associations AS pga ON ( pga.cid = subcert.cid AND pga.property_id = subcert.property_id )
						JOIN ap_payee_property_groups AS appg ON ( appg.cid = pga.cid AND appg.property_group_id = pga.property_group_id )
						LEFT JOIN ap_payees AS ap_client ON ( ap_client.cid = appg.cid AND ap_client.id = appg.ap_payee_id AND ap_client.is_system = true )
						LEFT JOIN ap_payee_locations AS parent_company_apl ON ( parent_company_apl.cid = ap_client.cid AND parent_company_apl.ap_payee_id = ap_client.id AND ap_client.ap_payee_type_id = ' . CApPayeeType::INTERCOMPANY . ' AND parent_company_apl.is_primary = TRUE AND parent_company_apl.disabled_on IS NULL AND parent_company_apl.deleted_on IS NULL )
						LEFT JOIN ap_legal_entities AS ale ON ( ale.cid = ap_client.cid AND ale.ap_payee_id = ap_client.id AND ap_client.ap_payee_type_id = ' . CApPayeeType::INTERCOMPANY . ' )
					WHERE
						subcert.id IN (' . implode( ',', $arrintSubsidyCertificationIds ) . ')
						AND subcert.cid = ' . ( int ) $intCid . '
						AND subcontract.deleted_by IS NULL
						AND subcontract.deleted_on IS NULL
						AND ale.deleted_by IS NULL
						AND ale.deleted_on IS NULL
					GROUP BY
						c.company_name,
						pa.street_line1,
						pa.city,
						pa.state_code,
						pa.postal_code,
						psd.hud_project_name,
						subcontract.subsidy_contract_type_id,
						psd.hud_project_number,
						subcontract.contract_number,
						psd.hud_project_imax_id_encrypted,
						o.tax_number_encrypted,
						ale.tax_number_encrypted,
						parent_company_apl.duns_number,
						owner_apl.duns_number,
						stv.name';

		$arrmixTenantHrRecordData = fetchData( $strSql, $objDatabase );

		return $arrmixTenantHrRecordData[0];
	}

	public static function fetchMoveInDateByApplicationIdByPropertyIdByCid( $intApplicantionId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicantionId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						lease_start_date as move_in_date
					FROM
						cached_applications ca
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.id = ' . ( int ) $intApplicantionId;

		return parent::fetchColumn( $strSql, 'move_in_date', $objDatabase );
	}

    /**
     * @Todo: Renamed to fetchPreviousSubsidyCertificationBySubsidyCertificationTypeIdsByLeaseIdByPropertyIdByCid in new EOS layer
     */
	public static function fetchPreviousSubsidyCertificationBySubsidyCertificationTypesIdByLeaseIdByPropertyIdByCid( $arrintSubsidyCertificationTypeIds, $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSubsidyCertificationTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.property_id = ' . ( int ) $intPropertyId . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.subsidy_certification_type_id IN ( ' . implode( ', ', $arrintSubsidyCertificationTypeIds ) . ' )
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY
						sc.effective_date DESC
					limit 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyCertificationTypeIdByApplicantionIdByLeaseIdByCid( $intSubsidyCertificationTypeId, $intApplicantionId, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intSubsidyCertificationTypeId ) || false == valId( $intApplicantionId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicantionId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.subsidy_certification_type_id = ' . ( int ) $intSubsidyCertificationTypeId . '
						-- And sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					order by sc.id desc
					limit 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchCertificationDataByCertificationIdsByCid( $intSubsidyCertificationId, $fltAnnualIncomeAmount, $intFamilySize, $intCid, $objDatabase ) {
		if( false == valId( $intSubsidyCertificationId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strEligibilityUniverseCodeEquation = '( CASE WHEN scont.start_date < \'1981-10-01\'::DATE THEN 1 ELSE 2 END )';
		$strCurrentIncomeStatusCode         = '( CASE WHEN ' . $fltAnnualIncomeAmount . ' > sil.very_low_income_limit_amount THEN 1 WHEN ' . $fltAnnualIncomeAmount . ' > sil.extremely_low_income_limit_amount THEN 2 ELSE 3 END )';
		$intYear                            = 1995;

		$strSql = 'SELECT
						sc.lease_id AS owner_generated_tenant_id_number,
						sc.effective_date AS subsidy_certification_effective_date,
						c.name_last AS head_last_name,
						c.name_first AS head_first_name,
						left(c.name_middle, 1) AS head_middle_initial,
						c.birth_date AS head_birth_date,
						COALESCE((sc.details::json->\'subsidy\'->>\'project_move_in_date\')::date,cl.move_in_date) AS project_move_in_date,
						sct.code AS transaction_type,
						CASE
							WHEN sc.subsidy_certification_correction_type_id IS NOT NULL THEN 1
							ELSE NULL
						END AS action_processed_code,
						scct.code AS correction_type_code,
						CASE
							WHEN sc.subsidy_certification_type_id = ' . CSubsidyCertificationType::INITIAL_CERTIFICATION . ' THEN prev_sct.code
							ELSE NULL
						END AS previous_subsidy,
						asd.previous_subsidy_contract_type_id AS previous_subsidy_type,
						CASE
							WHEN sc.is_result_of_eiv = TRUE THEN \'Y\'
							ELSE NULL
						END AS eiv_indicator,
						CASE
							WHEN sc.subsidy_certification_type_id = ' . CSubsidyCertificationType::MOVE_IN . ' THEN spht.code
							ELSE NULL
						END AS previous_housing_code,
						CASE
							WHEN sc.subsidy_certification_type_id = ' . CSubsidyCertificationType::MOVE_IN . ' THEN sdst.code
							ELSE NULL
						END AS displacement_status_code,
						spr.id AS subsidy_passbook_rate_id,
						CASE
							WHEN
								scont.subsidy_contract_type_id != ' . \CSubsidyContractType::HUD_BMIR . '
								THEN
									ROUND( spr.passbook_rate_percent, 2 )
							ELSE 0
						END AS reported_passbook_rate_percent,
						CASE
							WHEN (
								scont.subsidy_contract_type_id IN (
									' . \CSubsidyContractType::HUD_SECTION_8 . ',
									' . \CSubsidyContractType::HUD_RENT_SUPPLEMENT . ',
									' . \CSubsidyContractType::HUD_RAP . ',
									' . \CSubsidyContractType::HUD_SECTION_236 . ',
									' . \CSubsidyContractType::HUD_SECTION_202_162_PAC . '
								)
								OR (
									scont.subsidy_contract_type_id IN (
										' . \CSubsidyContractType::HUD_SECTION_202_PRAC . ',
										' . \CSubsidyContractType::HUD_SECTION_811_PRAC . '
									)
									AND ' . ( int ) $intYear . ' = extract( year from scont.start_date )
								)
							) THEN sil.low_income_limit_amount
							WHEN scont.subsidy_contract_type_id = ' . \CSubsidyContractType::HUD_BMIR . ' THEN sil.bmir_income_limit_amount
							ELSE NULL
						END AS low_income_limit_amount,
						CASE
							WHEN (
								scont.subsidy_contract_type_id IN (
									' . \CSubsidyContractType::HUD_SECTION_8 . ',
									' . \CSubsidyContractType::HUD_SECTION_202_162_PAC . '
								)
								OR (
									scont.subsidy_contract_type_id IN (
										' . \CSubsidyContractType::HUD_SECTION_202_PRAC . ',
										' . \CSubsidyContractType::HUD_SECTION_811_PRAC . '
									)
									AND ' . ( int ) $intYear . ' != extract( year from scont.start_date )
								)
							) THEN sil.very_low_income_limit_amount
							ELSE NULL
						END AS very_low_income_limit_amount,
						CASE
							WHEN scont.subsidy_contract_type_id IN (
								' . \CSubsidyContractType::HUD_SECTION_8 . ',
								' . \CSubsidyContractType::HUD_PRA_DEMO_811 . '
							) THEN sil.extremely_low_income_limit_amount
							ELSE NULL
						END AS extremely_low_income_limit_amount,
						CASE
							WHEN scont.subsidy_contract_type_id = ' . \CSubsidyContractType::HUD_SECTION_8 . ' THEN ' . $strEligibilityUniverseCodeEquation . '
							ELSE NULL
						END AS eligibility_universe_code,
						CASE
							WHEN scont.subsidy_contract_type_id = ' . \CSubsidyContractType::HUD_SECTION_8 . ' THEN ' . $strCurrentIncomeStatusCode . '
							ELSE NULL
						END AS current_income_status_code,
						CASE
							WHEN (
								scont.subsidy_contract_type_id = ' . \CSubsidyContractType::HUD_SECTION_8 . '
								AND ' . $strEligibilityUniverseCodeEquation . ' = 2
								AND ' . $strCurrentIncomeStatusCode . ' = 1
								) THEN CASE
									WHEN ( cl.move_in_date) >= \'1984-07-01\' THEN \'Y\'
									ELSE \'N\'
								END
							ELSE NULL
						END AS section_8_assistance_1984_indicator,
						siet.code AS income_exception_code,
						CASE
							WHEN
								scont.subsidy_contract_type_id != ' . \CSubsidyContractType::HUD_BMIR . '
								THEN ROUND( ' . $fltAnnualIncomeAmount . ' * 0.03 )
							ELSE 0
						END AS three_percent_of_income,
						CASE
							WHEN scont.subsidy_contract_type_id = ' . \CSubsidyContractType::HUD_SECTION_8 . ' THEN smret.code
							ELSE NULL
						END AS min_rent_hardship_exemption_code,
						CASE
							WHEN
								csd.welfare_rent_amount IS NULL
								THEN 0
							ELSE csd.welfare_rent_amount
						END AS welfare_rent,
						CASE
							WHEN csd.is_police_or_security_officer = TRUE THEN \'Y\'
							ELSE \'N\'
						END AS police_or_security_tenant,
						COALESCE( pu.number_of_bedrooms, ut.number_of_bedrooms ) As bedroom_count,
						ROUND( COALESCE( pu.number_of_bathrooms, ut.number_of_bathrooms ) ) As bathroom_count,
						ut.number_of_rooms As number_of_rooms,
						lt.name AS lease_term,
						func_format_unit_number( NULL, NULL, cl.building_name, cl.unit_number_cache ) AS unit_number,
						scet.code AS hh_citizenship_eligibility,
						asd.anticipated_addition_adoption AS family_addition_adoption,
						asd.anticipated_addition_pregnancy AS family_addition_pregnancy,
						asd.anticipated_addition_foster_child AS family_addition_foster_child,
						CASE
							WHEN tcl.id IS NOT NULL THEN \'Y\'
							ELSE NULL
						END AS unit_transfer_code,
						CASE
							WHEN csd.is_qualified_surviving_member = TRUE THEN \'Y\'
							ELSE NULL
						END AS survivor_indicator,
						swt.code AS waiver_type_code,
						CASE
							WHEN sc.is_historical = TRUE THEN \'Y\'
							ELSE NULL
						END AS baseline_certification_indicator,
						sapt.code as plan_of_action_indicator,
						CASE
							WHEN ot.id IS NOT NULL THEN \'Y\'
							ELSE NULL
						END AS hud_owned_indicator,
						func_format_unit_number( NULL, NULL, tcl.building_name, tcl.unit_number_cache ) AS previous_unit_number,
						sset.code AS extenuating_circumstances_code,
						CASE
							WHEN sc.eligibility_check_not_required = TRUE THEN \'Y\'
							ELSE NULL
						END AS eligibility_check_not_required,
						scont.subsidy_contract_type_id AS current_subsidy_type,
						sc.subsidy_contract_id AS subsidy_contract_id,
						psd.hud_project_number,
						scont.contract_number,
						sc.subsidy_certification_type_id,
						sc.anticipated_voucher_date,
						CASE
							WHEN sc.rent_override_amount > 0 THEN \'Y\'
							ELSE NULL
						END AS is_rent_override,
						CASE
							WHEN scont.start_date < \'1981-10-01\'::DATE THEN \'LI\'
							WHEN siet.id IN(' . implode( ',', CSubsidyIncomeExceptionType::$c_arrmixSubsidyIncomeExceptionTypesForHap ) . ' ) THEN siet.code
							ELSE NULL
						END AS hap_income_code
					FROM
						subsidy_certifications AS sc
						JOIN customers AS c ON ( c.cid = sc.cid AND c.id = sc.primary_customer_id )
						JOIN property_subsidy_details AS psd ON ( psd.cid = sc.cid AND psd.property_id = sc.property_id )
						JOIN subsidy_income_limit_versions AS silv ON ( sc.effective_date BETWEEN silv.effective_date AND silv.effective_through_date AND silv.is_published= TRUE )
						JOIN subsidy_passbook_rates AS spr ON ( sc.effective_date BETWEEN spr.effective_date AND spr.effective_through_date AND spr.is_published = TRUE )
						JOIN applications AS a ON ( a.cid = sc.cid AND a.id = sc.application_id )
						LEFT JOIN unit_types AS ut ON ( a.cid = ut.cid AND ut.id = a.unit_type_id )
						LEFT JOIN property_units AS pu ON ( pu.cid = a.cid AND pu.property_id = a.property_id AND a.property_unit_id = pu.id )
						JOIN application_subsidy_details AS asd ON ( asd.cid = a.cid AND asd.application_id = a.id )
						JOIN subsidy_previous_housing_types AS spht ON ( spht.id = asd.subsidy_previous_housing_type_id )
						JOIN subsidy_displacement_status_types AS sdst ON ( sdst.id = asd.subsidy_displacement_status_type_id )
						LEFT JOIN subsidy_min_rent_exemption_types smret ON ( smret.id = asd.subsidy_min_rent_exemption_type_id )
						JOIN subsidy_citizenship_eligibility_types scet ON ( scet.id = asd.subsidy_citizenship_eligibility_type_id )
						LEFT JOIN subsidy_income_exception_types AS siet ON ( siet.id = asd.subsidy_income_exception_type_id )
						LEFT JOIN subsidy_waiver_types AS swt ON ( siet.id = asd.subsidy_waiver_type_id )
						LEFT JOIN subsidy_contract_types AS prev_sct ON ( prev_sct.id = asd.previous_subsidy_contract_type_id )
						JOIN cached_leases AS cl ON ( cl.cid = sc.cid AND cl.id = sc.lease_id )
						LEFT JOIN lease_intervals li ON ( cl.active_lease_interval_id = li.id AND cl.cid = li.cid )
						LEFT JOIN lease_terms lt ON ( li.lease_term_id = lt.id AND li.cid = lt.cid )
						-- Unit Transfers
						LEFT JOIN cached_leases AS tcl ON ( ( sc.subsidy_certification_type_id = ' . CSubsidyCertificationType::UNIT_TRANSFER . ' OR sc.subsidy_sub_certification_type_id = ' . CSubsidyCertificationType::UNIT_TRANSFER . ' ) AND tcl.cid = cl.cid AND tcl.transfer_lease_id = cl.id )
						JOIN customer_subsidy_details AS csd ON ( csd.cid = sc.cid AND csd.customer_id = sc.primary_customer_id AND csd.deleted_on IS NULL )
						JOIN subsidy_certification_types sct ON ( sct.id = sc.subsidy_certification_type_id )
						LEFT JOIN subsidy_certification_correction_types AS scct ON (scct.id = sc.subsidy_certification_correction_type_id )
						JOIN subsidy_contracts AS scont ON ( scont.cid = sc.cid AND scont.id = sc.subsidy_contract_id )
						LEFT JOIN subsidy_signature_exception_types AS sset ON( sset.id = sc.subsidy_signature_exception_type_id )
						LEFT JOIN subsidy_action_plan_types AS sapt ON ( sapt.id = scont.subsidy_action_plan_type_id)
						JOIN properties AS p ON (p.cid = sc.cid AND p.id = sc.property_id )
						JOIN owners AS o ON ( o.cid = sc.cid AND o.id = p.owner_id )
						LEFT JOIN owner_types ot ON ( o.cid = ot.cid AND o.owner_type_id = ot.id AND ot.name LIKE \'%HUD%\' AND ot.is_system = true )
						JOIN (
							SELECT
								sc.cid,
								sc.id AS subsidy_certification_id,
								MAX(
									CASE WHEN sil.subsidy_income_level_type_id = ' . CSubsidyIncomeLevelType::EXTREMELY_LOW . ' THEN sil.income_limit END
								) AS extremely_low_income_limit_amount,
								MAX(
									CASE WHEN sil.subsidy_income_level_type_id = ' . CSubsidyIncomeLevelType::VERY_LOW . ' THEN sil.income_limit END
								) AS very_low_income_limit_amount,
								MAX(
									CASE WHEN sil.subsidy_income_level_type_id = ' . CSubsidyIncomeLevelType::LOW . ' THEN sil.income_limit END
								) AS low_income_limit_amount,
								MAX(
									CASE WHEN sil.subsidy_income_level_type_id = ' . CSubsidyIncomeLevelType::BMIR . ' THEN sil.income_limit END
								) AS bmir_income_limit_amount
							FROM
								subsidy_certifications AS sc
								JOIN subsidy_income_limit_areas AS sila ON (sila.subsidy_income_limit_version_id = sc.subsidy_income_limit_version_id AND sila.hmfa_code = sc.hmfa_code )
								JOIN subsidy_income_limits AS sil ON ( sil.subsidy_income_limit_version_id = sc.subsidy_income_limit_version_id AND sil.subsidy_income_limit_area_id = sila.id )
							WHERE
								sil.family_size = ' . ( int ) $intFamilySize . '
								AND sc.cid = ' . ( int ) $intCid . '
								AND sil.subsidy_income_level_type_id IN (' . CSubsidyIncomeLevelType::EXTREMELY_LOW . ', ' . CSubsidyIncomeLevelType::VERY_LOW . ', ' . CSubsidyIncomeLevelType::LOW . ', ' . CSubsidyIncomeLevelType::BMIR . ')
								AND sil.is_hera = FALSE
							GROUP BY
								sc.cid,
								sc.id
						) AS sil ON ( sil.cid = sc.cid AND sil.subsidy_certification_id = sc.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.id = ' . ( int ) $intSubsidyCertificationId;

		$arrmixCustomerBaseRecordData = fetchData( $strSql, $objDatabase );

		return $arrmixCustomerBaseRecordData[0];
	}

	public static function fetchTaxCreditCertificationDataByCertificationIdsByCid( $intSubsidyCertificationId, $intCid, $objDatabase ) {
		if( false == valId( $intSubsidyCertificationId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sc.effective_date AS subsidy_certification_effective_date,
						cl.move_in_date,
						func_format_unit_number( NULL, NULL, tcl.building_name, COALESCE( tpu.details::json->\'subsidy_details\'->>\'tax_credit_unit_number\', tpu.unit_number ) ) AS transfer_unit_number,
						tcl.move_in_date AS transfer_move_in_date,
						sc.subsidy_certification_type_id,
						cl.property_name,
						psd.tax_credit_project_name,
						pa.county,
						pa.state_code,
						sa.building_identification_number,
						psd.tax_credit_project_number,
						cl.unit_number_cache AS unit_number,
						COALESCE( pu.details::json->\'subsidy_details\'->>\'tax_credit_unit_number\', pu.unit_number ) AS tax_credit_unit_number,
						COALESCE( pu.number_of_bedrooms, ut.number_of_bedrooms ) As bedroom_count,
						ROUND( COALESCE( pu.number_of_bathrooms, ut.number_of_bathrooms ) ) As bathroom_count,
						ut.number_of_rooms As number_of_rooms,
						lt.name AS lease_term,
						pu.square_feet,
						cl.name_last AS head_last_name,
						cl.name_first AS head_first_name,
						left( cl.name_middle, 1 ) AS head_middle_initial
					FROM
						subsidy_certifications sc
						JOIN property_subsidy_details psd ON ( psd.cid = sc.cid AND psd.property_id = sc.property_id )
						JOIN applications a ON ( a.cid = sc.cid AND a.id = sc.application_id )
						LEFT JOIN unit_types ut ON ( a.cid = ut.cid AND ut.id = a.unit_type_id )
						LEFT JOIN property_units pu ON ( pu.cid = a.cid AND pu.property_id = a.property_id AND a.property_unit_id = pu.id )

						JOIN cached_leases cl ON ( cl.cid = sc.cid AND cl.id = sc.lease_id )
						LEFT JOIN property_addresses pa ON ( pa.cid = cl.cid AND pa.property_id = cl.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
						JOIN set_asides sa ON ( sa.cid = sc.cid AND sa.id = sc.set_aside_id )
						LEFT JOIN lease_intervals li ON ( cl.active_lease_interval_id = li.id AND cl.cid = li.cid )
						LEFT JOIN lease_terms lt ON ( li.lease_term_id = lt.id AND li.cid = lt.cid )
						
						-- Corrections
						LEFT JOIN subsidy_certifications csc ON ( sc.subsidy_certification_type_id IN ( ' . sqlIntImplode( CSubsidyCertificationType::$c_arrintFullSubsidyCertificationTypeIds ) . ' ) AND sc.subsidy_certification_correction_type_id IS NOT NULL AND csc.cid = sc.cid AND csc.id = sc.corrected_subsidy_certification_id )
						
						-- Unit Transfers
						LEFT JOIN cached_leases tcl ON ( ( sc.subsidy_certification_type_id = ' . CSubsidyCertificationType::UNIT_TRANSFER . ' OR sc.subsidy_sub_certification_type_id = ' . CSubsidyCertificationType::UNIT_TRANSFER . ' ) AND tcl.cid = cl.cid AND tcl.transfer_lease_id = cl.id )
						LEFT JOIN property_units tpu ON ( tpu.cid = tcl.cid AND tpu.property_id = tcl.property_id AND tcl.property_unit_id = tpu.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.id = ' . ( int ) $intSubsidyCertificationId;

		$arrmixCustomerBaseRecordData = fetchData( $strSql, $objDatabase );

		return $arrmixCustomerBaseRecordData[0];
	}

	public static function fetchSubsidyCertifactionDataByPropertyIdByCid( $intPropertyId, $intSubsidyContractId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sc.id,
						ca.name_first,
						ca.name_last,
						ca.lease_approved_on,
						sc.subsidy_certification_type_id
					FROM
						subsidy_certifications sc
						JOIN cached_applications ca ON( ca.id = sc.application_id AND ca.property_id = sc.property_id AND ca.cid = sc.cid )
					WHERE
						ca.property_id = ' . ( int ) $intPropertyId . '
						AND sc.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
						AND ca.cid = ' . ( int ) $intCid . '
						AND sc.mat_data IS NOT NULL
						AND sc.subsidy_certification_status_type_id = ' . ( int ) CSubsidyCertificationStatusType::FINALIZED;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAnticipatedVoucherDateBySubsidyCertificationIdByCid( $intId, $intCid, $objDatabase ) {
		if( false == valId( $intId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						CASE
							WHEN
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintOtherHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id IN ( ' . implode( ',', CSubsidyCertificationType::$c_arrintNotRapEffectiveDateSubsidyCertificationTypeIds ) . ' ) AND date_part( \'day\', sc.effective_date ) = 1 )
								OR
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id IN ( ' . implode( ',', CSubsidyCertificationType::$c_arrintRapEffectiveDateSubsidyCertificationTypeIds ) . ' ) AND date_part( \'day\', sc.effective_date ) = 1 )
							THEN
								date_trunc ( \'month\', sc.effective_date )
							WHEN
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintOtherHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id IN( ' . implode( ',', CSubsidyCertificationType::$c_arrintNotRapEffectiveDateIncrementByOneSubsidyCertificationTypeIds ) . ' ) AND date_part( \'day\', sc.effective_date ) = 1 )
								OR
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintOtherHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id = ' . CSubsidyCertificationType::GROSS_RENT_CHANGE . '	AND date_part( \'day\', sc.effective_date ) != 1 )
								 OR
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id IN ( ' . CSubsidyCertificationType::TERMINATION . ',' . CSubsidyCertificationType::MOVE_OUT . ' ) AND date_part( \'day\', sc.effective_date ) = 1 )
								OR
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id IN ( ' . implode( ',', CSubsidyCertificationType::$c_arrintRapEffectiveDateIncrementByOneSubsidyCertificationTypeIds ) . ' ) AND date_part( \'day\', sc.effective_date ) != 1 )
							THEN
								date_trunc ( \'month\', sc.effective_date ) + interval \'1\' month
							WHEN
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintOtherHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id IN(  ' . implode( ',', CSubsidyCertificationType::$c_arrintNotRapEffectiveDateIncrementByTwoSubsidyCertificationTypeIds ) . ' ) AND date_part( \'day\', sc.effective_date ) != 1 )
							THEN
								date_trunc ( \'month\', sc.effective_date ) + interval \'2\' month
							 WHEN
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintOtherHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id = ' . CSubsidyCertificationType::ANNUAL_RECERTIFICATION . ' AND date_part( \'day\', sc.effective_date ) != 1 )
								OR
								( scc.subsidy_contract_type_id IN ( ' . implode( ',', CSubsidyContractType::$c_arrintHAPSubsidyContractTypeIds ) . ' ) AND sc.subsidy_certification_type_id = ' . CSubsidyCertificationType::ANNUAL_RECERTIFICATION . ' AND date_part( \'day\', sc.effective_date ) != 1 )
							THEN
								NULL
						END as anticipated_voucher_date
					FROM
						subsidy_certifications sc
						JOIN subsidy_contracts scc ON ( scc.cid = sc.cid AND scc.id = sc.subsidy_contract_id )
					WHERE
						sc.cid = ' . $intCid . '
						AND sc.id = ' . $intId . '
						AND sc.deleted_on IS NULL
						AND sc.deleted_by IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL';

		return self::fetchColumn( $strSql, 'anticipated_voucher_date', $objDatabase );
	}

	public static function fetchPendingSubsidyCertificationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		// @FIXME: As of now Considering GRC to fetch pending certification, as Annual recertification banner should not display if GRC in processed status. Full certification type constant should be updated after discussion.
		$arrintSubsidyCertificationTypes = CSubsidyCertificationType::$c_arrintFullSubsidyCertificationTypeIds;
		array_push( $arrintSubsidyCertificationTypes, CSubsidyCertificationType::GROSS_RENT_CHANGE );

		$strSql = 'SELECT
						sc.*
					FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.subsidy_certification_type_id IN ( ' . implode( ',', $arrintSubsidyCertificationTypes ) . ' )
						AND sc.subsidy_certification_status_type_id NOT IN ( ' . implode( ',', CSubsidyCertificationStatusType::$c_arrintCurrentSubsidyCertificationStatusTypeIds ) . ' )
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.deleted_on IS NULL
						AND sc.deleted_by IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY
						created_on DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $boolShowCorrectedCertifications = true, $boolShowDisabledCertification = false ) {
		if( false == valId( $intLeaseId ) ) {
			return NULL;
		}
		$strShowCorrectedCertificationsSql = ( false == $boolShowCorrectedCertifications ) ? ' AND sc.has_been_corrected = false' : '';
		$strShowDisabledCertificationSql   = ( false == $boolShowDisabledCertification ) ? ' AND sc.subsidy_certification_disable_reason_id IS NULL ' : '';

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						' . $strShowCorrectedCertificationsSql . $strShowDisabledCertificationSql . '
					ORDER BY
						sc.effective_date DESC,
						sc.id DESC';

		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

	public static function fetchTotalIncomeExceptionBySubsidyCertificationIdsByCid( $arrintSubsidyCertificationIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSubsidyCertificationIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						SUM( project_based_exceptions_in_use ) AS project_based_exceptions_in_use,
						SUM( tenant_based_exceptions_in_use ) AS tenant_based_exceptions_in_use
					FROM
						(
						SELECT
								CASE
									WHEN
										sc.mat_data->\'Section2\'->>\'hap_income_code\' = \'EP\'
									THEN
										1
									ELSE
										0
								END AS project_based_exceptions_in_use,
								CASE
									WHEN
										sc.mat_data->\'Section2\'->>\'income_exception_code\' LIKE \'E%\'
										AND sc.mat_data->\'Section2\'->>\'income_exception_code\' LIKE \'%T\'
									THEN
										1
									ELSE
										0
								END AS tenant_based_exceptions_in_use
							FROM
								subsidy_certifications sc
							WHERE
								sc.cid = ' . ( int ) $intCid . '
								AND sc.id IN (' . implode( ',', $arrintSubsidyCertificationIds ) . ')
								AND sc.deleted_by IS NULL
								AND sc.deleted_on IS NULL
								AND sc.subsidy_certification_disable_reason_id IS NULL
					) AS total_income_exception';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( ( true == valArr( $arrmixData ) )? $arrmixData[0] : NULL );
	}

	public static function fetchPaginatedResidentsRecertificationNoticesByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intLimit, $strSortBy, $objDatabase, $arrintExcludeSubsidyCertificationIds ) {
		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}
// This sql has to be refactored in future to make it less complex to understand.

		$strExcludeSubsidyCertificationWhereClause      = ( true == valArr( $arrintExcludeSubsidyCertificationIds ) ) ? ' AND sc.id NOT IN ( ' . sqlIntImplode( $arrintExcludeSubsidyCertificationIds ) . '  )' : '';

		$strSql = 'SELECT
          DISTINCT *
      FROM
          (
            SELECT
				1 AS priority,
				sc.subsidy_certification_id,
				sc.set_aside_id,
				sc.subsidy_contract_id,
				sc.set_aside_type_name,
				sc.subsidy_contract_program_type,
				ft.system_code,
				cl.cid,
				cl.property_id,
				cl.id AS lease_id,
				cl.primary_customer_id AS customer_id,
				func_format_customer_name ( cl.name_first, cl.name_last ) AS name_full,
				DATE_PART ( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::TIMESTAMP - CURRENT_DATE ) AS notice_days,
				CASE
					WHEN ( sc.set_aside_id IS NULL AND sc.subsidy_contract_id IS NOT NULL ) OR ( sc.set_aside_id IS NOT NULL AND sc.subsidy_contract_id IS NOT NULL ) 
						THEN CASE
						WHEN \'RE120DN\'  = ft.system_code THEN \'120 Days\'
						WHEN \'RE90DN\'   = ft.system_code THEN \'90 Days\'
						WHEN \'RE60DN\'   = ft.system_code THEN \'60 Days\'
                        WHEN \'HUD120DN\' = ft.system_code THEN \'120 Days\'
                        WHEN \'HUD90DN\'  = ft.system_code THEN \'90 Days\'
						ELSE \'60 Days\'
					END
				 ELSE CASE
					WHEN \'TC120DN\' = ft.system_code THEN \'120 Days\'
					WHEN \'TC90DN\' = ft.system_code THEN \'90 Days\'
					WHEN \'TC60DN\' = ft.system_code THEN \'60 Days\'
						ELSE \'30 Days\'
				END
				END AS notice_type,
				( sc.effective_through_date + INTERVAL \'1 day \' ) AS recertification_date,
				func_format_unit_number ( NULL, NULL, cl.building_name, cl.unit_number_cache ) AS unit_number,
				cl.property_name,
				ft.id AS file_type_id,
				rank ( ) over( Partition BY sc.lease_id ORDER BY sc.effective_date DESC, sc.subsidy_certification_id DESC, sc.subsidy_contract_type_id, ft.is_system ),
				ft.is_system
			FROM
				sub_cert sc
				JOIN cached_leases cl ON ( cl.cid = sc.cid AND cl.property_id = sc.property_id AND cl.id = sc.lease_id AND cl.lease_status_type_id IN ( ' . sqlIntImplode( CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) . ' ) AND sc.rank = 1 )
				JOIN file_types ft ON ( ft.cid = sc.cid AND ft.system_code IN ( ' . sqlStrImplode( CFileType::$c_arrstrAffordableRecertificationNoticeFileTypes ) . ' ) )
				JOIN documents d ON ( d.cid = ft.cid AND d.file_type_id = ft.id AND d.archived_on IS NULL AND d.deleted_by IS NULL )
				JOIN property_group_documents pgd ON ( pgd.cid = d.cid AND pgd.document_id = d.id AND pgd.property_group_id = cl.property_id )
				LEFT JOIN LATERAL 
				(
				SELECT
					f.id,
					f.cid,
					fa.lease_id,
					fa.subsidy_certification_id,
					f.file_type_id
				FROM
					files f
					JOIN file_associations fa ON ( fa.cid = pgd.cid AND fa.property_id = pgd.property_group_id AND fa.file_id = f.id )
				WHERE
					f.cid = ' . ( int ) $intCid . '
					AND fa.deleted_on IS NULL
					AND f.file_type_id = ft.id
					AND fa.subsidy_certification_id = sc.subsidy_certification_id
					AND fa.lease_id = cl.id
					) AS f ON ( TRUE )
				LEFT JOIN LATERAL 
				(
					SELECT
						mbd.id AS message_batch_detail_id
					FROM
						message_batches mb
						JOIN message_batch_details mbd ON ( mbd.cid = mb.cid AND mbd.message_batch_id = mb.id AND mbd.property_id = sc.property_id AND mbd.lease_id = sc.lease_id AND mbd.message_status <> \'Failed\' )
					WHERE
						mb.cid = ' . ( int ) $intCid . '
						AND mb.deleted_on IS NULL
						AND mb.message_batch_type_id = ' . CMessageBatchType::RECERTIFICATION_NOTICE . '
						AND CASE
							WHEN ( CAST ( mbd.details::JSONB ->> \'notice_type\' as TEXT ) = \'120 Days\' )	THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE, CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE, CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE ] ) . ' )
							WHEN ( CAST ( mbd.details::JSONB ->> \'notice_type\' as TEXT ) = \'90 Days\' )	THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE, CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE, CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE ] ) . ' )
							WHEN ( CAST ( mbd.details::JSONB ->> \'notice_type\' as TEXT ) = \'60 Days\' )	THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE, CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE, CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE ] ) . ' )
							WHEN ( CAST ( mbd.details::JSONB ->> \'notice_type\' as TEXT ) = \'30 Days\' )	THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE ] ) . ' )
						END
				) AS mb ON ( TRUE )	
				WHERE
						146 > DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )
						AND CASE
								WHEN sc.lease_interval_type_id IN ( ' . sqlIntImplode( [ CLeaseIntervalType::APPLICATION, CLeaseIntervalType::MONTH_TO_MONTH, CLeaseIntervalType::RENEWAL ] ) . ' ) 
									AND sc.lease_status_type_id IN ( ' . sqlIntImplode( [ CLeaseStatusType::FUTURE, CLeaseStatusType::CURRENT, CLeaseStatusType::NOTICE ] ) . ' )
								THEN ( sc.effective_through_date < CURRENT_DATE OR ( CURRENT_DATE BETWEEN (sc.effective_through_date - INTERVAL \'145 days\') AND (sc.effective_through_date) ) )
								ELSE sc.effective_through_date >= CURRENT_DATE AND CURRENT_DATE BETWEEN ( sc.effective_through_date - INTERVAL \'145 days\' )AND ( sc.effective_through_date )
							END
						AND CASE
								WHEN ( sc.set_aside_id IS NULL AND sc.subsidy_contract_id IS NOT NULL ) OR ( sc.set_aside_id IS NOT NULL AND sc.subsidy_contract_id IS NOT NULL ) THEN
								CASE
									WHEN 100 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE ) 
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE, CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_120_DAYS_NOTICE ] ) . ' )
									WHEN 70 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE, CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_90_DAYS_NOTICE ] ) . ' )
									ELSE ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE, CFileType::SYSTEM_CODE_HUD_RECERTIFICATION_60_DAYS_NOTICE ] ) . ' )
									END
							ELSE
								CASE
									WHEN 100 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE ) 
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE ] ) . ' )
									WHEN 70 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE ] ) . ' )
									WHEN 50 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE ] ) . ' )
									ELSE ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE ] ) . ' )
							END
						END
				AND f.id IS NULL
				AND mb.message_batch_detail_id IS NULL -- skip records who are already in RabbitMQ queue
				AND ft.deleted_on IS NULL
				AND EXISTS (
							SELECT
								1
							FROM
							property_subsidy_types pst
							WHERE
								pst.cid = sc.cid
								AND pst.property_id = sc.property_id
								AND pst.subsidy_type_id IN ( ' . sqlIntImplode( [ CSubsidyType::HUD, CSubsidyType::TAX_CREDIT ] ) . ' )
				)
				) AS subq
				where subq.rank = 1';

		$strSql = 'WITH sub_cert AS (
					SELECT
						sc.id AS subsidy_certification_id,
						sc.cid,
						sc.property_id,
						sc.lease_id,
						sc.effective_date,
						sc.effective_through_date,
						sc.set_aside_id,
						sc.subsidy_contract_id,
						ssat.name AS set_aside_type_name,
						sct.id as subsidy_contract_type_id,
						sct.name AS subsidy_contract_program_type,
						li.lease_interval_type_id,
						li.lease_status_type_id,
						rank ( ) over ( Partition BY sc.lease_id ORDER BY sc.effective_date DESC, li.lease_start_date DESC, sc.id DESC )
					FROM 
						subsidy_certifications sc
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], array[ ' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . ' ] ) lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = sc.property_id )
						JOIN properties p ON ( p.cid = sc.cid AND p.id = sc.property_id AND p.property_type_id = 6 )
						JOIN lease_intervals li ON ( sc.cid = li.cid AND sc.property_id = li.property_id AND sc.lease_id = li.lease_id )
						LEFT JOIN set_asides sa ON ( sa.cid = sc.cid AND sa.property_id = sc.property_id AND sa.id = sc.set_aside_id )
						LEFT JOIN subsidy_contracts sco ON ( sco.cid = sc.cid AND sco.property_id = sc.property_id AND sco.id = sc.subsidy_contract_id )
						LEFT JOIN subsidy_set_aside_types ssat ON ( ssat.id = sa.subsidy_set_aside_type_id )
						LEFT JOIN subsidy_contract_types sct ON ( sco.subsidy_contract_type_id = sct.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						' . $strExcludeSubsidyCertificationWhereClause . '
						AND NOT sc.has_been_corrected
						AND sc.subsidy_certification_status_type_id > ' . CSubsidyCertificationStatusType::PROCESSED . '
						AND sc.effective_through_date IS NOT NULL
						AND li.lease_status_type_id NOT IN ( ' . sqlIntImplode( [ CLeaseStatusType::APPLICANT, CLeaseStatusType::CANCELLED, CLeaseStatusType::PAST ] ) . ' )
						AND li.lease_interval_type_id NOT IN ( ' . sqlIntImplode( [ CLeaseIntervalType::LEASE_MODIFICATION ] ) . ' )
						)
					SELECT * FROM
					( ' . $strSql . ' ) SubQ
					WHERE
						cid = ' . ( int ) $intCid . '
					ORDER BY
						' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentsRecertificationNoticesCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset = 0, $intLimit = 100, $objDatabase ) {
		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}
// This sql has to be refactored in future to make it less complex to understand.
		$strSql = 'SELECT DISTINCT * 
					FROM
					( SELECT
						1 AS priority,
						sc.subsidy_certification_id,
						cl.id AS lease_id,
						ft.id AS file_type_id,
						rank ( ) over( Partition BY sc.lease_id ORDER BY sc.effective_date DESC, sc.subsidy_certification_id DESC )
					FROM
						sub_cert sc 
						JOIN cached_leases cl ON ( cl.cid = sc.cid AND cl.property_id = sc.property_id AND cl.id = sc.lease_id AND cl.lease_status_type_id IN ( ' . sqlIntImplode( CLeaseStatusType::$c_arrintActiveLeaseStatusTypes ) . ' ) AND sc.rank = 1 )
						JOIN file_types ft ON ( ft.cid = sc.cid AND ft.system_code IN ( ' . sqlStrImplode( CFileType::$c_arrstrAffordableRecertificationNoticeFileTypes ) . ' ) )
						JOIN documents d on ( d.cid = ft.cid AND d.file_type_id = ft.id AND d.archived_on IS NULL AND d.deleted_by IS NULL )
						JOIN property_group_documents pgd ON( pgd.cid = d.cid AND pgd.document_id = d.id AND pgd.property_group_id = cl.property_id )
						LEFT JOIN LATERAL (
							SELECT
								f.id,
								f.cid,
								fa.lease_id,
								fa.subsidy_certification_id,
								f.file_type_id
							FROM
								files f
								JOIN file_associations fa ON ( fa.cid = pgd.cid AND fa.property_id = pgd.property_group_id AND fa.file_id = f.id )
							WHERE
								f.cid = ' . ( int ) $intCid . '
								AND fa.deleted_on IS NULL
								AND f.file_type_id = ft.id
								AND fa.subsidy_certification_id = sc.subsidy_certification_id
								AND fa.lease_id = cl.id
						) AS f ON ( TRUE )
					WHERE
						 sc.effective_date <= CURRENT_DATE
						AND sc.effective_through_date >= CURRENT_DATE
						AND CURRENT_DATE BETWEEN ( sc.effective_through_date - INTERVAL \'120 days\' )AND ( sc.effective_through_date )
						AND CASE
								WHEN ( sc.set_aside_id IS NULL AND sc.subsidy_contract_id IS NOT NULL ) OR ( sc.set_aside_id IS NOT NULL AND sc.subsidy_contract_id IS NOT NULL ) THEN
								CASE
									WHEN 100 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE ) 
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE ] ) . ' )
									WHEN 70 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE ] ) . ' )
									ELSE ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE ] ) . ' )
									END
							ELSE
								CASE
									WHEN 100 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE ) 
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE ] ) . ' )
									WHEN 70 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE ] ) . ' )
									WHEN 50 < DATE_PART( \'day\', ( sc.effective_through_date + INTERVAL \'1 day\' )::timestamp - CURRENT_DATE )
									THEN ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE ] ) . ' )
									ELSE ft.system_code IN ( ' . sqlStrImplode( [ CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE ] ) . ' )
								END
							END
						AND f.id IS NULL
						AND ft.deleted_on IS NULL ) AS subq
						WHERE
						subq.rank = 1';

		$strSql = 'WITH sub_cert AS (
							SELECT
						sc.id AS subsidy_certification_id,
						sc.cid,
						sc.property_id,
						sc.lease_id,
						sc.effective_date,
						sc.effective_through_date,
						sc.set_aside_id,
						sc.subsidy_contract_id,
						rank ( ) over ( Partition BY sc.lease_id ORDER BY sc.effective_date DESC, sc.id DESC )
					FROM 
						subsidy_certifications sc
						JOIN load_properties( ARRAY[ ' . ( int ) $intCid . ' ], array[ ' . implode( $objDashboardFilter->getPropertyGroupIds(), ',' ) . ' ] ) lp ON ( ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'lp.is_disabled = 0 AND' : '' ) . ' lp.property_id = sc.property_id  )
						JOIN properties p ON ( p.cid = sc.cid AND p.id = sc.property_id AND p.property_type_id = ' . CPropertyType::SUBSIDIZED . ' )

					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						AND NOT sc.has_been_corrected
						AND sc.subsidy_certification_status_type_id > ' . CSubsidyCertificationStatusType::PROCESSED . '
						AND sc.effective_through_date IS NOT NULL
						)SELECT
						max( priority ) AS priority,
						count( 1 )
					FROM
						( ' . $strSql . ' ) AS subQ

						ORDER BY
						priority DESC
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationByLeaseIdByCidById( $intLeaseId, $intCid, $intId, $objDatabase ) {

		$strSql = 'SELECT * FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.id = ' . ( int ) $intId;

		return parent::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchCurrentActiveSubsidyCertificationByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.property_id = ' . ( int ) $intPropertyId . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.subsidy_certification_status_type_id >= ' . CSubsidyCertificationStatusType::PROCESSED . '
						AND NOT sc.has_been_corrected
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY
						id DESC
					LIMIT 1';

		return parent::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchHouseholdCertificationsByEffectiveDateBySubsidyCertificationStatusTypeIdsBySubsidyContractIdByCid( $strEffectiveDate, $arrintSubsidyCertificationStatusTypeIds, $intSubsidyContractId, $intCid, $objDatabase, $arrintUnitTypeIds = NULL, $arrintLeaseIds = [] ) {

		if( false == valStr( $strEffectiveDate ) || false == valArr( $arrintSubsidyCertificationStatusTypeIds ) || false == valId( $intSubsidyContractId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strWhereClause = ( true == valArr( $arrintLeaseIds ) ) ? ' AND sc.lease_id IN ( ' . sqlIntImplode( $arrintLeaseIds ) . ' )' : '';

		$strSql = '
			WITH RECURSIVE cte_transfer_lease_associations(cid, lease_id, unit_space_id, current_lease_id, current_head_of_household_name) AS (
				SELECT
					cl.cid,
					cl.id AS lease_id,
					cl.unit_space_id,
					cl.id AS current_lease_id,
					CONCAT(cl.name_last, \', \', cl.name_first) AS current_head_of_household_name,
					cl.primary_customer_id
				FROM
					cached_leases AS cl
					JOIN subsidy_certifications AS sc ON cl.cid = sc.cid AND cl.property_id = sc.property_id AND cl.id = sc.lease_id
				WHERE
					cl.cid = ' . ( int ) $intCid . '
					AND cl.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
					AND cl.lease_status_type_id <> ' . ( int ) CLeaseStatusType::CANCELLED . '
					AND cl.transfer_lease_id IS NULL
					AND sc.effective_date <= \'' . $strEffectiveDate . '\'::DATE
				GROUP BY
					cl.CID,
					cl.id,
					cl.unit_space_id,
					cl.id,
					cl.name_last,
					cl.name_first,
					cl.primary_customer_id
				
			  UNION ALL
				SELECT
					cl.cid,
					cl.id AS lease_id,
					cl.unit_space_id,
					ctla.current_lease_id,
					ctla.current_head_of_household_name,
					ctla.primary_customer_id
				FROM
					cte_transfer_lease_associations AS ctla
					JOIN cached_leases AS cl ON ( cl.cid = ctla.cid AND cl.transfer_lease_id = ctla.lease_id )
				WHERE
					ctla.cid = ' . ( int ) $intCid . '
			)
			SELECT
				row_number ( ) OVER ( PARTITION BY ctla.cid, ctla.current_lease_id ORDER BY ctla.current_lease_id, sc.effective_date, sc.created_on ) AS sequence_position,
				CASE WHEN 1 = cume_dist ( ) OVER ( PARTITION BY ctla.cid, ctla.current_lease_id ORDER BY ctla.current_lease_id, sc.effective_date < CURRENT_DATE, sc.effective_date, sc.created_on ) THEN TRUE
					ELSE FALSE
				END AS is_last_certification,
				ctla.current_head_of_household_name AS head_of_household_name,
				ctla.primary_customer_id,
				ctla.current_lease_id as latest_lease_id,
				sct.name AS subsidy_certification_type_name,
				us.unit_type_id,
				cl.lease_status_type,
				spr.passbook_rate_percent,
				cl.unit_number_cache,
				sc.*
			FROM
				subsidy_certifications sc
				JOIN cached_leases cl ON ( cl.cid = sc.cid AND cl.id = sc.lease_id AND cl.lease_status_type_id NOT IN ( ' . CLeaseStatusType::PAST . ', ' . CLeaseStatusType::CANCELLED . ' ) )
				JOIN subsidy_passbook_rates AS spr ON ( sc.effective_date BETWEEN spr.effective_date AND spr.effective_through_date AND spr.is_published = TRUE )
				JOIN cte_transfer_lease_associations AS ctla ON ( ctla.cid = sc.cid AND ctla.lease_id = sc.lease_id )
				JOIN subsidy_certification_types AS sct ON ( sct.id = sc.subsidy_certification_type_id )
				JOIN unit_spaces AS us ON ( us.cid = ctla.cid AND us.id = ctla.unit_space_id
		';

		if( true == valArr( $arrintUnitTypeIds ) ) {
			$strSql .= '
					AND us.unit_type_id IN ( ' . sqlIntImplode( $arrintUnitTypeIds ) . ' )
			';
		}

		$strSql .= '
				)
			WHERE
				sc.cid = ' . ( int ) $intCid . ' 
				AND sc.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
				AND sc.subsidy_certification_status_type_id IN ( ' . sqlIntImplode( $arrintSubsidyCertificationStatusTypeIds ) . ' )
				AND (
					\'' . $strEffectiveDate . '\'::DATE BETWEEN sc.effective_date AND sc.effective_through_date
					OR sc.effective_date >= \'' . $strEffectiveDate . '\'::DATE
				)
				AND sc.deleted_by IS NULL
				AND sc.deleted_on IS NULL
				AND sc.subsidy_certification_disable_reason_id IS NULL
				' . $strWhereClause . '
				AND sc.has_been_corrected = false /* Exclude corrected as we do not want to correct the corrected certification during GRC */
			ORDER BY
				ctla.current_head_of_household_name ASC,
				ctla.current_lease_id ASC,
				sc.effective_date ASC,
				sc.created_on ASC
		';

		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

	public static function fetchDetailedSubsidyCertificationByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sc.*,
						scon.start_date as contract_start_date,
						scon.end_date as contract_end_date,
						sct.name subsidy_contract_name,
						scon.contract_number
					FROM
						subsidy_certifications sc
						LEFT JOIN subsidy_contracts scon ON ( sc.subsidy_contract_id = scon.id AND sc.cid = scon.cid )
						JOIN subsidy_contract_types sct ON ( scon.subsidy_contract_type_id = sct.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.id = ' . ( int ) $intId;

		$arrmixSubsidyCertificationDetails = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixSubsidyCertificationDetails ) ) ? $arrmixSubsidyCertificationDetails[0] : [];
	}

	public static function fetchCurrentSubsidyCertificationEffectiveThroughDateByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sc.effective_through_date
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND has_been_corrected = FALSE
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY sc.effective_date DESC
					LIMIT 1';

		return self::fetchColumn( $strSql, 'effective_through_date', $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyGrossRentChangeIdByCid( $intSubsidyGrossRentChangeId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				sc.*,
				sct.name AS subsidy_certification_type_name,
				CONCAT( cl.name_last, \', \', cl.name_first ) AS head_of_household_name,
				cl.lease_status_type,
			    cl.unit_number_cache
			FROM
				subsidy_certifications sc
				JOIN cached_leases cl ON ( sc.lease_id = cl.id AND sc.cid = cl.cid )
				JOIN subsidy_certification_types sct ON ( sc.subsidy_certification_type_id = sct.id )
			WHERE
				sc.cid = ' . ( int ) $intCid . '
				AND sc.subsidy_gross_rent_change_id = ' . ( int ) $intSubsidyGrossRentChangeId . '
				AND sc.deleted_by IS NULL
				AND sc.deleted_on IS NULL
				AND sc.subsidy_certification_disable_reason_id IS NULL
			ORDER BY
				sc.effective_date DESC,
				sc.id DESC';
		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsBySubsidyCertificationStatusTypeIdsBySubsidyGrossRentChangeIdByCid( $arrintSubsidyCertificationStatusTypeIds, $intSubsidyGrossRentChangeId, $intCid, $objDatabase ) {
//		if( false == valArr( $arrintSubsidyCertificationTypeIds ) ) return NULL;

		$strSql = '
			SELECT
				*
			FROM
				subsidy_certifications
			WHERE
				cid = ' . ( int ) $intCid . '
				AND date_agent_signed IS NULL
				AND subsidy_gross_rent_change_id = ' . ( int ) $intSubsidyGrossRentChangeId . '
				AND subsidy_certification_status_type_id IN ( ' . sqlIntImplode( $arrintSubsidyCertificationStatusTypeIds ) . ' ) ';

		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

	public static function fetchProcessedSubsidyCertificationCountByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						COUNT( id ) AS id
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						AND sc.subsidy_certification_status_type_id > ' . CSubsidyCertificationStatusType::COMPLETED;

		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public static function fetchContractIdByApplicantionIdByCid( $intApplicantionId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						sc.subsidy_contract_id
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicantionId . '
						AND sc.cid = ' . ( int ) $intCid;

		return parent::fetchColumn( $strSql, 'subsidy_contract_id', $objDatabase );
	}

	public static function fetchTotalHudOverpaidAmountByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						SUM(sc.hud_overpaid_amount) AS repayment_amount
					FROM
						subsidy_certifications sc
					WHERE
						sc.repayment_id IS NULL
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.property_id = ' . ( int ) $intPropertyId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL';

		return parent::fetchColumn( $strSql, 'repayment_amount', $objDatabase );
	}

	public static function fetchSubsidyCertificationsWithNoRepaymentByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.repayment_id IS NULL
						AND sc.hud_overpaid_amount IS NOT NULL
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.property_id = ' . ( int ) $intPropertyId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL';

		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

    /**
     * @Todo: Renamed to fetchSubsidyCertificationByApplicationIdByLeaseIdByCid in new EOS layer
     */
	public static function fetchSubsidyCertificationsByApplicantionIdByLeaseIdByCid( $intApplicantionId, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicantionId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicantionId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.subsidy_certification_type_id IN ( ' . implode( ', ', CSubsidyCertificationType::$c_arrintFullSubsidyCertificationTypeIds ) . ' )
						And sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY sc.id DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchLatestActiveSubsidyCertificationByLeaseIdByPropertyIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.has_been_corrected = false
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY
						id DESC
					LIMIT 1';

		return parent::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	/**
	 * TODO below function added on temporary basis to avoid adding of wrong sequence for subsidy certiifcate id by migration team
	 * This fun will get removed once we identify all impacted area for fetchLatestActiveSubsidyCertificationByLeaseIdByPropertyIdByCid function and get verified from QA
	 */
	public static function fetchLatestActiveSubsidyCertificationByEffectiveDateByLeaseIdByPropertyIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.has_been_corrected = false
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY
						sc.effective_date DESC
					LIMIT 1';

		return parent::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsByPropertyIdsByCid( $arrintPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT * FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_on IS NULL
						AND sc.property_id IN ( ' . implode( ',', $arrintPropertyId ) . ' )';

		return parent::fetchSubsidyCertifications( $strSql, $objDatabase );

	}

    /**
     * @Todo: Renamed to fetchSubsidyCertificationsBySubsidyCertificationTypeIdBySubsidyCertificationStatusTypeIdByApplicationIdByLeaseIdByCid in new EOS layer
     */
	public static function fetchSubsidyCertificationsBySubsidyCertificationTypeIdBySubsidyCertificationStatusTypeIdByApplicantionIdByLeaseIdByCid( $intSubsidyCertificationTypeId, $intSubsidyCertificationStatusTypeId, $intApplicantionId, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intSubsidyCertificationTypeId ) || false == valId( $intApplicantionId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicantionId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.subsidy_certification_type_id = ' . ( int ) $intSubsidyCertificationTypeId . '
						AND sc.subsidy_certification_status_type_id = ' . ( int ) $intSubsidyCertificationStatusTypeId . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsByApplicantionIdsByCid( $arrintApplicantionIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id IN ( ' . implode( ',', $arrintApplicantionIds ) . ' )
						AND sc.cid = ' . ( int ) $intCid;

		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

	public static function fetchLatestSubsidyCertificationBySubsidyCertificationStatusTypeByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $intSubsidyCertificationStatusTypeId = CSubsidyCertificationStatusType::ACKNOWLEDGED, $boolHasBeenCorrected = true ) {
		if( false == valId( $intLeaseId ) || false == valId( $intCid ) || false == valId( $intSubsidyCertificationStatusTypeId ) ) {
			return false;
		}

		$strCheckHasBeenCorrectedCondition = ( false == $boolHasBeenCorrected ) ? ' AND sc.has_been_corrected = false ' : '';

		$strSql = 'SELECT
						sc.*
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						' . $strCheckHasBeenCorrectedCondition . '
						AND sc.subsidy_certification_status_type_id >= ' . ( int ) $intSubsidyCertificationStatusTypeId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						ORDER BY
							sc.effective_date DESC, sc.created_on DESC, sc.id DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchTaxCreditIncomeLimitBySubsidyCertificationIdByCid( $intSubsidyCertificationId, $intCid, $objDatabase,$intFamilySize ) {

		$strSql = 'SELECT
						sil.income_limit
					FROM
						subsidy_certifications sc
						JOIN application_subsidy_details asd ON ( asd.application_id = sc.application_id AND asd.cid = sc.cid )
						JOIN subsidy_income_limit_areas sila ON ( sc.hmfa_code = sila.hmfa_code AND sc.subsidy_income_limit_version_id = sila.subsidy_income_limit_version_id )
						JOIN subsidy_income_limits sil ON ( sil.subsidy_income_limit_area_id = sila.id AND sil.subsidy_income_limit_version_id = sc.subsidy_income_limit_version_id AND asd.current_income_level_type_id = sil.subsidy_income_level_type_id ) 
						JOIN property_subsidy_details psd ON ( sc.property_id = psd.property_id AND sc.cid = psd.cid )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.id = ' . ( int ) $intSubsidyCertificationId . '
						AND sil.family_size = ' . ( int ) $intFamilySize . '
						AND sil.is_hera = psd.tax_credit_use_hera_limits';

		return self::fetchColumn( $strSql, 'income_limit', $objDatabase );

	}

	public static function fetchLatestActiveSubsidyCertificationSubsidyContractIdByLeaseIdByPropertyIdByCid( $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT subsidy_contract_id FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.has_been_corrected = false
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchColumn( $strSql, 'subsidy_contract_id', $objDatabase );
	}

	public static function fetchSimpleSubsidyCertificationsNotAssociatedToChargeCodeIdByLeaseIdByCid( $intChargeCodeId, $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						sc.*,
						sct.name
					FROM 
						subsidy_certifications sc
						JOIN subsidy_certification_types sct ON ( sc.subsidy_certification_type_id = sct.id )
					WHERE 
						  sc.id NOT IN (
							SELECT 
								scsc.subsidy_certification_id
							FROM 
								subsidy_certification_scheduled_charges scsc
								JOIN scheduled_charges sch ON ( sch.id = scsc.scheduled_charge_id AND sch.cid = scsc.cid )
							WHERE 
								scsc.lease_id = ' . ( int ) $intLeaseId . ' 
								AND scsc.cid = ' . ( int ) $intCid . ' 
								AND sch.ar_code_id = ' . ( int ) $intChargeCodeId . '
								AND sch.deleted_by IS NULL
								AND sch.deleted_on IS NULL
						)
						AND sc.lease_id = ' . ( int ) $intLeaseId . ' 
						AND sc.cid = ' . ( int ) $intCid . ' 
						AND sc.has_been_corrected IS FALSE
						AND sc.subsidy_certification_status_type_id >= ' . CSubsidyCertificationStatusType::FINALIZED . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY sc.effective_date DESC, sc.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationBySubsidyCertificationTypeIdsByLeaseIdByCid( $arrintSubsidyCertificationTypeIds, $intLeaseId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSubsidyCertificationTypeIds ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.subsidy_certification_type_id IN ( ' . implode( ',', $arrintSubsidyCertificationTypeIds ) . ' )
						AND sc.subsidy_certification_status_type_id  >= ' . CSubsidyCertificationStatusType::FINALIZED . '
						AND sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY sc.effective_date DESC, sc.id DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	// FixMe: Added temporary condition of set_aside_id, subsidy_contract_id & lease_id due to constrains cc_subsidy_certifications_contract_setaside_validation for data fix task#1656984

	public static function fetchAllSubsidyCertificationsByCid( $arrintPropertyIds, $intCid, $objDatabase, $arrintSubsidyCertificationIds = NULL, $strIncludeMatDataType = 'ALL_MAT_DATA' ) {

		if( false == valId( $intCid ) ) return NULL;

		switch( $strIncludeMatDataType ) {
			case 'NOT_NULL_MAT_DATA':
				$strWhereCondition = 'AND sc.mat_data IS NOT NULL';
				break;

			case 'NULL_MAT_DATA':
				$strWhereCondition = 'AND sc.mat_data IS NULL';
				break;

			case NULL:
			case 'ALL_MAT_DATA':
			default:
				$strWhereCondition = '';
				break;
		}

		$strWhereCondition .= true == valArr( $arrintSubsidyCertificationIds ) ? ' AND sc.id IN ( ' . implode( ',', $arrintSubsidyCertificationIds ) . ' )' : '';

		$strWhereCondition .= true == valArr( $arrintPropertyIds ) ? ' AND sc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )' : '';

		$strSql = 'SELECT
						sc.*
					FROM
						subsidy_certifications sc
					WHERE
						sc.cid = ' . ( int ) $intCid .
		                $strWhereCondition . '
		                AND ( sc.subsidy_contract_id IS NOT NULL OR  sc.set_aside_id IS NOT NULL )
		                AND sc.lease_id IS NOT NULL
					    AND sc.subsidy_certification_status_type_id >= ' . CSubsidyCertificationStatusType::PROCESSED . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL';

		return self::fetchSubsidyCertifications( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsDetailsByPropertyIdBySubsidyContractIdByCid( $intPropertyId, $intSubsidyContractId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intSubsidyContractId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT 
					    cl.id as lease_id,
                        cl.name_first,
                        cl.name_last,
                        cl.unit_number_cache AS unit_number,
                        cl.unit_space_id
					FROM
						cached_leases cl
						JOIN subsidy_certifications sc ON ( sc.lease_id = cl.id AND sc.subsidy_contract_id IS NOT NULL AND sc.cid = cl.cid )
					WHERE
					    cl.property_id = ' . $intPropertyId . '
					    AND cl.occupancy_type_id = ' . COccupancyType::AFFORDABLE . '
					    AND sc.subsidy_certification_status_type_id = ' . CSubsidyCertificationStatusType::FINALIZED . ' 
					    AND sc.subsidy_contract_id = ' . $intSubsidyContractId . '
					    AND cl.lease_status_type_id IN ( ' . CLeaseStatusType::CURRENT . ', ' . CLeaseStatusType::NOTICE . ' )
					    AND cl.transfer_lease_id IS NULL
					    AND cl.lease_interval_type_id != ' . CLeaseIntervalType::MONTH_TO_MONTH . '
					    AND cl.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLastSubsidyCertificationsByLeaseIdsByCid( $arrintLeaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintLeaseIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    sc.*
					FROM
					    (
					      SELECT
					          *,
					          rank() OVER( PARTITION BY lease_id ORDER BY id DESC ) rank
					      FROM
					          subsidy_certifications
					      WHERE
					          cid = ' . ( int ) $intCid . '
					          AND lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
					    ) AS sc
					WHERE
					    sc.rank = 1';

		return parent::fetchSubsidyCertifications( $strSql, $objDatabase );

	}

    /**
     * @Todo: Renamed to fetchContractIdsByApplicationIdByCid in EOS layer
     */
	public static function fetchContractIdsByApplicantionIdByCid( $intApplicantionId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						sc.subsidy_contract_id,
						sc.set_aside_id
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicantionId . '
						AND sc.cid = ' . ( int ) $intCid;

		$arrmixContracts = fetchData( $strSql, $objDatabase );

		return $arrmixContracts[0];

	}

	public static function fetchSubsidyCertificationByEffectiveDateByLeaseIdByCid( $strEffectiveDate, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valStr( $strEffectiveDate ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return false;
		}

		$strSql = 'SELECT
						sc.*
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.subsidy_certification_status_type_id >= ' . CSubsidyCertificationStatusType::FINALIZED . '
						AND \'' . $strEffectiveDate . '\' BETWEEN sc.effective_date AND sc.effective_through_date
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY sc.effective_date DESC, sc.id DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchCountOfSubsidyCertificationsByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intLeaseId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						count( DISTINCT sc.id ) AS total_count
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.subsidy_certification_type_id NOT IN( ' . CSubsidyCertificationType::MOVE_IN . ', ' . CSubsidyCertificationType::INITIAL_CERTIFICATION . ' )
						AND sc.has_been_corrected = FALSE
						AND sc.subsidy_certification_status_type_id >= ' . CSubsidyCertificationStatusType::FINALIZED . ' 
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL';

		return self::fetchColumn( $strSql, 'total_count', $objDatabase );
	}

	public static function fetchFirstSubsidyCertificationByLeaseIdByCid( $intLeaseId, $intCid, $objDatabase, $arrintSubsidyCertificationStatusTypeIds = NULL, $arrintSubsidyCertificationTypeIds = NULL ) {

		$strWhereClause = ( true == valArr( $arrintSubsidyCertificationStatusTypeIds ) ) ? ' AND sc.subsidy_certification_status_type_id IN ( ' . implode( ',', $arrintSubsidyCertificationStatusTypeIds ) . ' )' : '';
		$strWhereClause .= ( true == valArr( $arrintSubsidyCertificationTypeIds ) ) ? ' AND sc.subsidy_certification_type_id IN ( ' . implode( ',', $arrintSubsidyCertificationTypeIds ) . ' )' : '';

		$strSql = 'SELECT
						*
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . $strWhereClause . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY
						sc.effective_date ASC,
						sc.id ASC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchOverlappingCertificationsByLeaseIdByCid( $strEffectiveDate, $strEffectiveThroughDate, $intLeaseId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				sc.id AS overlapping_certification_id,
				sct.name AS subsidy_certification_type,
				sct.name || \' (\' || TO_CHAR ( sc.effective_date::DATE, \'MM/DD/YYYY\' ) || \' - \' || TO_CHAR ( sc.effective_through_date::DATE, \'MM/DD/YYYY\' ) || \')\' AS overlapping_certification_label
			FROM
				subsidy_certifications sc
				JOIN subsidy_certification_types sct ON sc.subsidy_certification_type_id = sct.id
			WHERE
				( sc.effective_date, sc.effective_through_date ) OVERLAPS ( \'' . $strEffectiveDate . '\'::DATE, \'' . $strEffectiveThroughDate . '\'::DATE )
				AND sc.lease_id = ' . ( int ) $intLeaseId . '
				AND sc.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCertificationsNotHavingDocumentsByLeaseIdsByCid( $arrintLeaseId, $intCid, $objDatabase, $intSubsidyContractId = NULL, $intSetAsideId = NULL ) {

		$strContractCondition = '';

		if( true == valId( $intSubsidyContractId ) ) {
			$strContractCondition .= ' AND sc.subsidy_contract_id = ' . $intSubsidyContractId;
		}

		if( true == valId( $intSetAsideId ) ) {
			$strContractCondition .= ' AND sc.set_aside_id = ' . $intSetAsideId;
		}

		$strSql = '
			SELECT
				sc.* 
			FROM
				subsidy_certifications sc
			WHERE 
				sc.lease_id IN ( ' . sqlIntImplode( $arrintLeaseId ) . ' )
				AND sc.cid =  ' . ( int ) $intCid . '
				' . $strContractCondition . '
				AND NOT exists(
	                SELECT
		                1
	                FROM
		                file_associations fa
	                WHERE fa.cid = sc.cid
		              AND fa.subsidy_certification_id = sc.id
	            );';
		return self::fetchSubsidyCertifications( $strSql, $objDatabase );

	}

	public static function fetchSubsidyCertificationByLeaseIntervalIdByLeaseIdByCid( $intLeaseIntervalId, $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    sc.id,
					    sc.lease_id,
					    sc.cid,
					    sc.effective_date,
					    sc.effective_through_date,
					    sc.set_aside_id,
					    sc.subsidy_contract_id 
					FROM
					    subsidy_certifications sc
					    JOIN applications a ON( a.cid = sc.cid AND a.id = sc.application_id )
					    JOIN lease_intervals li ON( li.cid = a.cid AND li.id = a.lease_interval_id )
					WHERE
					sc.cid = ' . ( int ) $intCid . '
					AND sc.lease_id = ' . ( int ) $intLeaseId . '
					AND li.id = ' . ( int ) $intLeaseIntervalId . '
					AND sc.deleted_by IS NULL
					AND sc.deleted_on IS NULL
					AND sc.subsidy_certification_disable_reason_id IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationsAndFilesDetailsByCid( $intMessageBatchId, $intCid, $objDatabase, $arrintPropertyIds ) {

		$strPropertyIdcondition = ( true == valArr( $arrintPropertyIds ) ) ? ' AND mbd.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ' : '';

		$arrstrRecertificationNoticeFileTypeSystemCodes = [ CFileType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE, CFileType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE, CFileType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE, CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE, CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE, CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE, CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_30_DAYS_NOTICE ];

		$strSql = 'SELECT
						 mbd.property_id,
					     mbd.lease_id,
					     cl.primary_customer_id as customer_id,
					     func_format_customer_name ( cl.name_first, cl.name_last ) AS name_full,
					     func_format_unit_number ( NULL, NULL, cl.building_name, cl.unit_number_cache ) AS unit_number,
					     CASE
					       WHEN \'' . CFileType::SYSTEM_CODE_RECERTIFICATION_120_DAYS_NOTICE . '\' || \'' . CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_120_DAYS_NOTICE . '\' = ft.system_code THEN \'120 Days\'
					       WHEN \'' . CFileType::SYSTEM_CODE_RECERTIFICATION_90_DAYS_NOTICE . '\' || \'' . CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_90_DAYS_NOTICE . '\' = ft.system_code THEN \'90 Days\'
					       WHEN \'' . CFileType::SYSTEM_CODE_RECERTIFICATION_60_DAYS_NOTICE . '\' || \'' . CFileType::SYSTEM_CODE_TAXCREDIT_RECERTIFICATION_60_DAYS_NOTICE . '\' = ft.system_code THEN \'60 Days\'
					       ELSE \'30 Days\'
					     END AS notice_type,
					     mbd.file_id AS file_id,
					     CAST ( mbd.details::JSONB ->> \'recertification_date\' as Date ) AS recertification_date,
						 CAST ( mbd.details::JSONB ->> \'notice_type\' as TEXT ) AS notice_type
					FROM
						message_batch_details mbd
					    JOIN cached_leases cl ON ( mbd.lease_id = cl.id AND mbd.cid = cl.cid )
					    LEFT JOIN files f ON ( f.id = mbd.file_id AND f.cid = mbd.cid )
					    LEFT JOIN file_types ft ON ( ft.cid = mbd.cid AND ft.id = f.file_type_id AND ft.system_code IN ( ' . sqlStrImplode( $arrstrRecertificationNoticeFileTypeSystemCodes ) . ' ) )
					WHERE
					    mbd.cid = ' . ( int ) $intCid . '
					    AND mbd.message_batch_id = ' . ( int ) $intMessageBatchId . '
					    ' . $strPropertyIdcondition . '
				    ORDER BY mbd.file_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationByApplicationIdByCidBySubsidyCertificationTypeIdsByStatusIds( $intApplicationId, $intCid, CDatabase $objDatabase, array $arrintSubsidyCertificationTypeIds, array $arrintSubsidyCertificationStatusTypeIds ) {
		if( false == valId( $intApplicationId ) || false == valId( $intCid ) ) return NULL;

		$strWhereCondition = ( true == valArr( $arrintSubsidyCertificationTypeIds ) ) ? ' AND sc.subsidy_certification_type_id IN ( ' . implode( ',', $arrintSubsidyCertificationTypeIds ) . ' ) ' : '';
		$strWhereCondition .= ( true == valArr( $arrintSubsidyCertificationStatusTypeIds ) ) ? ' AND sc.subsidy_certification_status_type_id IN ( ' . implode( ',', $arrintSubsidyCertificationStatusTypeIds ) . ' ) ' : '';

		$strSql = 'SELECT
						sc.*
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicationId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						' . $strWhereCondition . '
					ORDER BY sc.effective_date DESC, sc.id DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationByLeaseIdByCidBySubsidyCertificationStatusTypeIds( $intLeaseId, $intCid, $intSubsidyCertificationStatusTypeIds, CDatabase $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strWhereCondition = ( true == valArr( $intSubsidyCertificationStatusTypeIds ) ) ? ' AND sc.subsidy_certification_status_type_id IN ( ' . implode( ',', $intSubsidyCertificationStatusTypeIds ) . ' ) ' : '';

		$strSql = 'SELECT
						sc.*
					FROM
						subsidy_certifications sc
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
						' . $strWhereCondition . '
					ORDER BY sc.effective_date DESC, sc.id DESC
					LIMIT 1';

		return self::fetchSubsidyCertification( $strSql, $objDatabase );
	}

	public static function fetchSubsidyCertificationSetAsideDetailsByLeaseIdByCidBySubsidyCertificationStatusTypeId( $intLeaseId, $intCid, $intSubsidyCertificationStatusTypeId, CDatabase $objDatabase ) {

		if( false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sc.lease_id,
						sc.set_aside_id,
						sa.subsidy_set_aside_type_id
					FROM
						subsidy_certifications sc
						JOIN set_asides sa ON ( sa.cid = sc.cid AND sa.id = sc.set_aside_id )
						JOIN subsidy_set_aside_types ssat ON ( ssat.id = sa.subsidy_set_aside_type_id )
					WHERE
						sc.lease_id = ' . ( int ) $intLeaseId . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.subsidy_certification_status_type_id >= ' . ( int ) $intSubsidyCertificationStatusTypeId . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.subsidy_certification_disable_reason_id IS NULL
					ORDER BY sc.effective_date DESC, sc.id DESC
					LIMIT 1';

		$arrmixSetAsides = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'lease_id', $arrmixSetAsides );

	}

	public static function fetchSubsidyReCertificationTypeIdByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		if( false == valId( $intApplicationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						subsidy_recertification_type_id
					FROM
						subsidy_certifications sc
					WHERE
						sc.application_id = ' . ( int ) $intApplicationId . '
						AND sc.cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'subsidy_recertification_type_id', $objDatabase );
	}

}

?>
