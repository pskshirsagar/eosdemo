<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CProductDefaultReports
 * Do not add any new functions to this class.
 */

class CProductDefaultReports extends CBaseProductDefaultReports {

    public static function fetchProductDefaultReports( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CProductDefaultReport', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchProductDefaultReport( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CProductDefaultReport', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>