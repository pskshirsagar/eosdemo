<?php

class CCustomerVoteType extends CBaseCustomerVoteType {

	protected $m_arrstrLeasingCenterCustomerVoteTypes;

	const FLOORPLAN				= 1;
	const BENEFIT_SELLING		= 2;
	const QUALIFICATION			= 3;
	const AMENITY				= 4;
	const UTILITY				= 5;
	const UNIT					= 6;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getLeasingCenterCustomerVoteTypes() {
		if( true == isset( $this->m_arrstrLeasingCenterCustomerVoteTypes ) )
			return $this->m_arrstrLeasingCenterCustomerVoteTypes;

		$this->m_arrstrLeasingCenterCustomerVoteTypes = [
			self::FLOORPLAN				=> __( 'Floorplan' ),
			self::BENEFIT_SELLING		=> __( 'Benefit Selling' ),
			self::QUALIFICATION			=> __( 'Qualification' ),
			self::AMENITY				=> __( 'Amenity' ),
			self::UTILITY				=> __( 'Utility' ),
			self::UNIT					=> __( 'Unit' )
		];

		return $this->m_arrstrLeasingCenterCustomerVoteTypes;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>