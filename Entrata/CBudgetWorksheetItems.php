<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorksheetItems
 * Do not add any new functions to this class.
 */

class CBudgetWorksheetItems extends CBaseBudgetWorksheetItems {

	public static function fetchBudgetWorksheetItemsByBudgetWorksheetGlAccountIdByCid( $intBudgetWorksheetGlAccountId, $intCid, $objDatabase ) {
		if( false == valId( $intBudgetWorksheetGlAccountId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bwi.*
					FROM
						budget_worksheet_items bwi
						JOIN budget_worksheet_gl_accounts bwga ON ( bwga.cid = bwi.cid AND bwga.budget_worksheet_id = bwi.budget_worksheet_id AND bwga.gl_account_id = bwi.gl_account_id )
					WHERE
						bwi.cid = ' . ( int ) $intCid . '
						AND bwga.id = ' . ( int ) $intBudgetWorksheetGlAccountId . '
					ORDER BY
						bwi.id';

		return self::fetchBudgetWorksheetItems( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorksheetItemsByBudgetWorksheetGlAccountIdsByCid( $arrintBudgetWorksheetGlAccountIds, $intCid, $objDatabase, $boolFetchData = false ) {
		if( false == valIntArr( $arrintBudgetWorksheetGlAccountIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bwi.*
					FROM
						budget_worksheet_items bwi
						JOIN budget_worksheet_gl_accounts bwga ON ( bwga.cid = bwi.cid AND bwga.budget_worksheet_id = bwi.budget_worksheet_id AND bwga.gl_account_id = bwi.gl_account_id )
					WHERE
						bwi.cid = ' . ( int ) $intCid . '
						AND bwga.id IN ( ' . sqlIntImplode( $arrintBudgetWorksheetGlAccountIds ) . ' )
					ORDER BY
						bwi.id';

		if( true == $boolFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchBudgetWorksheetItems( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorksheetItemsByIdsByCid( $arrintBudgetWorksheetItemIds, $intCid, $objDatabase ) {
		if( false == valIntArr( $arrintBudgetWorksheetItemIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						budget_worksheet_items
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . sqlIntImplode( $arrintBudgetWorksheetItemIds ) . ' )';

		return self::fetchBudgetWorksheetItems( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorksheetItemsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		if( false == valId( $intBudgetWorkbookId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bwi.*
					FROM
						budget_workbook_assumptions bwa
						JOIN budget_worksheets bw ON ( bw.cid = bwa.cid AND bw.budget_workbook_id = bwa.budget_workbook_id )
						JOIN budget_worksheet_items bwi ON ( bwi.cid = bw.cid AND bwi.budget_worksheet_id = bw.id AND bwi.formula LIKE CONCAT( \'%(\', bwa.key, \')%\' ) )
					WHERE
						bwa.cid = ' . ( int ) $intCid . '
						AND bwa.budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . '
						AND bwa.budget_assumption_type_id  = ' . CBudgetAssumptionType::CUSTOM . '
						AND bwa.deleted_by IS NULL
						AND bws.deleted_by IS NULL
					GROUP BY
						bwi.id,
						bwi.cid';

		return self::fetchBudgetWorksheetItems( $strSql, $objDatabase );
	}

}
?>