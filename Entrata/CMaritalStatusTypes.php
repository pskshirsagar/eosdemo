<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMaritalStatusTypes
 * Do not add any new functions to this class.
 */

class CMaritalStatusTypes extends CBaseMaritalStatusTypes {

	public static function fetchMaritalStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMaritalStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMaritalStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMaritalStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllMaritalStatusTypes( $objDatabase ) {
		return self::fetchMaritalStatusTypes( 'SELECT * FROM marital_status_types ORDER BY order_num', $objDatabase );
	}

	public static function fetchMaritalStatusTypeNameById( $intId, $objDatabase ) {

		$strSql = 'SELECT
						name
					FROM
						marital_status_types
				   	WHERE
						id = ' . ( int ) $intId . '
						LIMIT 1';

		return parent::fetchColumn( $strSql, 'name', $objDatabase );
	}
}
?>