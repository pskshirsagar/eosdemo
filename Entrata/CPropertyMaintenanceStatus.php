<?php

class CPropertyMaintenanceStatus extends CBasePropertyMaintenanceStatus {

	protected $m_intMaintenanceStatusTypeId;
	protected $m_intMaintenanceStatusPropertyCount;

	protected $m_strName;
	protected $m_strMaintenanceStatusTypeName;
	protected $m_strIntegrationClientTypeName;

    public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

    	parent::applyRequestForm( $arrmixRequestForm, $arrmixFormFields );

    	if( true == valArr( $arrmixFormFields ) ) {
    		$arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
    	}

    	$this->setValues( $arrmixRequestForm, false );

    	return;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, true, $boolDirectSet );

    	if( true == isset( $arrmixValues['name'] ) ) $this->setName( $arrmixValues['name'] );
    	if( true == isset( $arrmixValues['maintenance_status_type_name'] ) ) $this->setMaintenanceStatusTypeName( $arrmixValues['maintenance_status_type_name'] );
    	if( true == isset( $arrmixValues['maintenance_status_type_id'] ) ) $this->setMaintenanceStatusTypeId( $arrmixValues['maintenance_status_type_id'] );
    	if( true == isset( $arrmixValues['integration_client_type_name'] ) ) $this->setIntegrationClientTypeName( $arrmixValues['integration_client_type_name'] );
    	if( true == isset( $arrmixValues['properties_count'] ) ) $this->m_intMaintenanceStatusPropertyCount = trim( $arrmixValues['properties_count'] );

    	return;
    }

    public function setName( $strName ) {
    	$this->m_strName = CStrings::strTrimDef( $strName, 240, NULL, true );
    }

    public function getName() {
    	return $this->m_strName;
    }

    public function setMaintenanceStatusTypeName( $strMaintenanceStatusTypeName ) {
    	$this->m_strMaintenanceStatusTypeName = $strMaintenanceStatusTypeName;
    }

    public function getMaintenanceStatusTypeName() {
    	return $this->m_strMaintenanceStatusTypeName;
    }

    public function setMaintenanceStatusTypeId( $intMaintenanceStatusTypeId ) {
    	$this->m_intMaintenanceStatusTypeId = $intMaintenanceStatusTypeId;
    }

    public function getMaintenanceStatusTypeId() {
    	return $this->m_intMaintenanceStatusTypeId;
    }

    public function setIntegrationClientTypeName( $strIntegrationClientTypeName ) {
    	$this->m_strIntegrationClientTypeName = $strIntegrationClientTypeName;
    }

    public function getIntegrationClientTypeName() {
    	return $this->m_strIntegrationClientTypeName;
    }

    public function setMaintenanceStatusPropertyCount( $intMaintenanceStatusPropertyCount ) {
    	$this->m_intMaintenanceStatusPropertyCount = CStrings::strToIntDef( $intMaintenanceStatusPropertyCount, NULL, false );
    }

    public function getMaintenanceStatusPropertyCount() {
    	return $this->m_intMaintenanceStatusPropertyCount;
    }

    /**
    * Validation Functions
    *
    */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceStatusId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDefault() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valPropertyMaintenanceStatusType( $objDatabase ) {

    	if( false == is_null( $this->getMaintenanceStatusId() ) ) {

    		$arrobjMaintenanceRequests = \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchMaintenanceRequestsByPropertyIdByMaintenanceStatusIdByCid( $this->getPropertyId(), $this->getMaintenanceStatusId(), $this->getCid(), $objDatabase );

    		if( true == $this->getIsDefault() ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_id', __( 'Maintenance Status set as Default at property level cannot be removed.' ) ) );
    			return false;
    		}

    		if( true == $this->getIsDefaultClosed() ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_id', __( 'Maintenance Status set as Default Closed at property level cannot be removed.' ) ) );
    			return false;
    		}

    		$objMaintenanceStatus = \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchMaintenanceStatusByIdByCid( $this->getMaintenanceStatusId(), $this->getCid(), $objDatabase );

    		if( CMaintenanceStatusType::COMPLETED == $objMaintenanceStatus->getMaintenanceStatusTypeId() ) {
    			$intCompletedStatusCount = $this->getCompletedPropertyMaintenanceStatusTypeCount( $objDatabase );

    			if( 1 == $intCompletedStatusCount && true == $this->getIsPublished() ) {
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_id', __( 'At least one Maintenance Status should have close status at property level.' ) ) );
    				return false;
    			}
    		}
    	}

    	return true;
    }

    public function valUpdatePropertyMaintenanceStatusTypes( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == $this->getIsPublished() ) {

    		if( true == $this->getIsDefault() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', __( 'Maintenance Status set as Default at property level cannot be hidden.' ) ) );
    		}

    		if( true == $this->getIsDefaultClosed() ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', __( 'Maintenance Status set as Default Closed at property level cannot be hidden.' ) ) );
    		}

    		$objMaintenanceStatus = \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchMaintenanceStatusByIdByCid( $this->getMaintenanceStatusId(), $this->getCid(), $objDatabase );

    		if( CMaintenanceStatusType::COMPLETED == $this->getMaintenanceStatusTypeId() ) {
    			$boolForUpdatePropertyMaintenance = true;
    			$intCompletedStatusId = $this->getCompletedPropertyMaintenanceStatusTypeCount( $objDatabase, $boolForUpdatePropertyMaintenance );

    			if( false == is_null( $this->getId() ) && $this->getId() == $intCompletedStatusId ) {
    				$boolIsValid = false;
    				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', __( 'At least one Maintenance Status should have close status at property level.' ) ) );
    			}
    		}
    	}

    	return $boolIsValid;
    }

    public function getCompletedPropertyMaintenanceStatusTypeCount( $objDatabase, $boolForUpdatePropertyMaintenance = false ) {

    	$arrobjCompletedPropertyMaintenanceStatuses = \Psi\Eos\Entrata\CPropertyMaintenanceStatuses::createService()->fetchCompletedPropertyMaintenanceStatusesByPropertyIdByCid( $this->getPropertyId(), $this->m_intCid, $objDatabase );

    	$intCompletedStatusCount = 0;

    	if( true == valArr( $arrobjCompletedPropertyMaintenanceStatuses ) ) {
	    	foreach( $arrobjCompletedPropertyMaintenanceStatuses as $objCompletedPropertyMaintenanceStatus ) {
    			if( CMaintenanceStatusType::COMPLETED == $objCompletedPropertyMaintenanceStatus->getMaintenanceStatusTypeId() && 1 == $objCompletedPropertyMaintenanceStatus->getIsPublished() ) {
    				$intCompletedStatusCount++;
    				$intPropertyMaintenanceStatusId = $objCompletedPropertyMaintenanceStatus->getId();
    			}
    		}
    	}

    	if( 1 == $intCompletedStatusCount ) {
    		if( true == $boolForUpdatePropertyMaintenance ) {
    			return $intPropertyMaintenanceStatusId;
    		}
    	}

    	return $intCompletedStatusCount;
    }

    public function valPropertyMaintenanceStatusTypeMaintenanceTemplates( $objDatabase ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getMaintenanceStatusId() ) ) {

    		$arrobjMaintenanceTemplates  = \Psi\Eos\Entrata\CPropertyMaintenanceTemplates::createService()->fetchPropertyMaintenanceTemplatesByMaintenanceStatusIdByPropertyIdByCid( $this->getMaintenanceStatusId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

    		if( true == valArr( $arrobjMaintenanceTemplates ) ) {
    			$boolIsValid = false;
   				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_id', __( 'Maintenance template(s) exists for the maintenance status type.' ) ) );
    		}
    	}

    	return $boolIsValid;
    }

	public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
				break;

        	case VALIDATE_DELETE:
        		$boolIsValid &= $this->valPropertyMaintenanceStatusType( $objDatabase );
            	$boolIsValid &= $this->valPropertyMaintenanceStatusTypeMaintenanceTemplates( $objDatabase );
            	break;

			case 'validate_update_new':
            	$boolIsValid &= $this->valUpdatePropertyMaintenanceStatusTypes( $objDatabase );
             	break;

            default:
            	$boolIsValid = true;
        }

        return $boolIsValid;
    }

}
?>