<?php

define( 'ERROR_CODE_INVALID_EMAIL', 506 );

class CCorporate extends CBaseCorporate {

	protected $m_intRemoteScreeningId;
	protected $m_intScreeningDecisionTypeId;
	protected $m_intScreeningApplicationRequestId;

	const CORPORATE_APPLICATION_TYPE_ID = 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBusinessName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strBusinessName ) || 0 == strlen( $this->m_strBusinessName ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_name', __( 'Business name is required.' ) ) );
		}
		/** Removed for i18n **/
//		elseif( false == preg_match( '/^[a-zA-Z0-9\,\&\-\.\']*$/', str_replace( ' ', '', $this->m_strBusinessName ) ) ) {
//
//			$boolIsValid = false;
//
//			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_name', __( 'Business name is not valid.' ) ) );
//		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBin() {
		$boolIsValid = true;
		if( true == is_null( $this->m_strBin ) ) {
			$boolIsValid = true;
		} elseif( false == is_numeric( $this->m_strBin ) || 9 != \Psi\CStringService::singleton()->strlen( $this->m_strBin ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bin', __( 'BIN is not valid. Enter 9 digit BIN' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTaxId() {
		$strTaxNumber = ( true == valStr( $this->m_strTaxNumberEncrypted ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] ) : NULL );

		$boolIsValid = true;
		if( true == is_null( $strTaxNumber ) ) {
			$boolIsValid = true;
		} elseif( false == is_numeric( $strTaxNumber ) || 9 != strlen( $strTaxNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bin', __( 'Tax ID is not valid. Enter 9 digit tax id' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber() {
		$boolIsValid = true;
		$strPhoneNumber   = $this->getPhoneNumber();
		$objPhoneNumber = $this->createPhoneNumber( $strPhoneNumber );

		$intPhoneLength	= strlen( $objPhoneNumber->getNumber() );

		if( 0 == $intPhoneLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is required.' ) ) );
		} elseif( true == valStr( $objPhoneNumber->getNumber() ) && 0 < $intPhoneLength && false == $objPhoneNumber->isValid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPhoneNumber->getFormattedErrors(), [ 'region' => $objPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPhoneNumber->getCountryCode() ] ), 504 ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		if( 0 == strlen( $this->m_strEmailAddress ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Business Email address is required.' ) ) );
		} elseif( false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Business Email address does not appear to be valid.' ), ERROR_CODE_INVALID_EMAIL ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryContact() {
		$boolIsValid = true;

		if( false == isset( $this->m_strPrimaryContact ) || 0 == strlen( $this->m_strPrimaryContact ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_contact', __( 'Primary contact is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryContactPhone() {
		$boolIsValid = true;
		$strPrimaryContactPhoneNumber   = $this->getPrimaryContactPhone();
		$objPrimaryContactPhoneNumber = $this->createPhoneNumber( $strPrimaryContactPhoneNumber );

		$intPrimaryContactPhoneLength	= strlen( $objPrimaryContactPhoneNumber->getNumber() );

		if( 0 == $intPrimaryContactPhoneLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_contact_phone', __( 'Contact phone number is required.' ) ) );
		} elseif( true == valStr( $objPrimaryContactPhoneNumber->getNumber() ) && 0 < $intPrimaryContactPhoneLength && false == $objPrimaryContactPhoneNumber->isValid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'primary_contact_phone', __( 'Contact phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPrimaryContactPhoneNumber->getFormattedErrors(), [ 'region' => $objPrimaryContactPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPrimaryContactPhoneNumber->getCountryCode() ] ), 504 ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryContactEmail() {
		$boolIsValid = true;
		if( 0 == strlen( $this->m_strPrimaryContactEmail ) ) {
			return $boolIsValid;
		} elseif( false == CValidation::validateEmailAddresses( $this->m_strPrimaryContactEmail ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_contact_email', __( 'Primary Email address does not appear to be valid.' ), ERROR_CODE_INVALID_EMAIL ) );
		}

		return $boolIsValid;
	}

	public function valUniqueBusiness( $objDatabase, $intCid ) {
		$boolIsValid = true;

		$arrobjCorporates = CCorporates::fetchCorporatesByCid( $intCid, $objDatabase );

		if( true == valArr( $arrobjCorporates ) ) {
			foreach( $arrobjCorporates as $objCorporate ) {
				if( $this->getId() == $objCorporate->getId() ) continue;

				if( $this->m_strBusinessName == $objCorporate->getBusinessName() && $this->m_strPhoneNumber == $objCorporate->getPhoneNumber() && $this->getTaxNumber() == $objCorporate->getTaxNumber() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'business_name', __( 'Business already exists.' ) ) );
					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $intCid ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'add_edit_corporate':
				$boolIsValid &= $this->valUniqueBusiness( $objDatabase, $intCid );
				$boolIsValid &= $this->valBusinessName();
				$boolIsValid &= $this->valPhoneNumber();
                $boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPrimaryContact();
				$boolIsValid &= $this->valPrimaryContactPhone();
				$boolIsValid &= $this->valPrimaryContactEmail();
				$boolIsValid &= $this->valBin();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valTaxId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			if( false == $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase ) ) {
				return false;
			}

			$objCorporateEventLibrary = new CCorporateEventLibrary();
			$objCorporateEventData = new stdClass();
			$objCorporateEventData->eventTypeId     = CEventType::CREATE_CORPORATE_LEADS;
			$objCorporateEventData->corporate       = $this;
			$objCorporateEventData->companyUserId   = $intCurrentUserId;

			if( false == $objCorporateEventLibrary->logCorporateEvent( $objCorporateEventData, $objDatabase ) ) {
				return false;
			}

			return true;

		} else {
			if( true == $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase ) ) {
				$objApplicationLogLibrary = new CApplicationLogLibrary();
				$objApplicationLogLibrary->insertCorporateChanges( $intCurrentUserId, $this, $objDatabase );
				return true;
			}
			return false;
		}
	}

	public function setTaxNumberLastFour( $strPlainTaxNumber ) {
		$strMaskedNumber = CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', $strPlainTaxNumber ) );
		$this->m_strTaxNumberLastFour = CStrings::strTrimDef( $strMaskedNumber, 240, NULL, true );
	}

	public function getCustomer( $objDatabase ) {
		if( false == valObj( $this->m_objCustomer, 'CCustomer' ) || 0 == strlen( $this->m_objCustomer->getId() ) ) {
			$this->m_objCustomer = \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objCustomer;
	}

	public function getScreeningApplicationRequest( $objDatabase ) {
		$objScreeningApplicationRequest = $this->fetchScreeningApplicationRequest( $objDatabase );

		if( false == valObj( $objScreeningApplicationRequest, 'CScreeningApplicationRequest' ) ) {
			$objScreeningApplicationRequest = $this->createScreeningApplicationRequest( $this->m_intCurrentUserId, $objDatabase );
		}

		return $objScreeningApplicationRequest;
	}

	public function createScreeningApplicationRequest( $intCompanyUserId, $objDatabase ) {

		$objScreeningApplicationRequest = new CScreeningApplicationRequest();

		$objScreeningApplicationRequest->setApplicationId( $this->getId() );
		$objScreeningApplicationRequest->setCid( $this->getCid() );
		$objScreeningApplicationRequest->setScreeningVendorId( CTransmissionVendor::RESIDENT_VERIFY );
		$objScreeningApplicationRequest->setRequestStatusTypeId( CScreeningStatusType::APPLICANT_RECORD_STATUS_OPEN );
		$objScreeningApplicationRequest->setApplicationTypeId( self::CORPORATE_APPLICATION_TYPE_ID );

		$objDatabase->begin();

		if( false == $objScreeningApplicationRequest->insert( $intCompanyUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Corporate [ID: {%s, 0}] : Failed to create screening application request.', [ $this->getId() ] ), NULL ) );
			$objDatabase->rollback();
			return;
		}

		$objDatabase->commit();

		return $objScreeningApplicationRequest;
	}

	public function fetchScreeningApplicationRequest( $objDatabase ) {
		// @TODO: Add CorporateType
		return CScreeningApplicationRequests::fetchScreeningApplicationRequestByApplicationIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchProperty( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->m_intPropertyId, $this->m_intCid, $objDatabase );
	}

	public function getTaxNumber() {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function setTaxNumber( $strPlainTaxNumber ) {
		if( true == valStr( $strPlainTaxNumber ) && false == \Psi\CStringService::singleton()->stristr( $strPlainTaxNumber, '*' ) ) {
			$this->setTaxNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
			$this->setTaxNumberLastFour( $strPlainTaxNumber );
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['tax_number'] ) ) $this->setTaxNumber( $arrmixValues['tax_number'] );
		if( true == isset( $arrmixValues['screening_decision_type_id'] ) ) $this->setScreeningDecisionTypeId( $arrmixValues['screening_decision_type_id'] );
		if( true == isset( $arrmixValues['screening_application_request_id'] ) ) $this->setScreeningApplicationRequestId( $arrmixValues['screening_application_request_id'] );
		if( true == isset( $arrmixValues['remote_screening_id'] ) ) $this->setRemoteScreeningId( $arrmixValues['remote_screening_id'] );

		return;
	}

	public function setScreeningDecisionTypeId( $intScreeningDecisionTypeId ) {
		$this->m_intScreeningDecisionTypeId = CStrings::strToIntDef( $intScreeningDecisionTypeId, NULL, false );
	}

	public function setScreeningApplicationRequestId( $intScreeningApplicationRequestId ) {
		$this->m_intScreeningApplicationRequestId = CStrings::strToIntDef( $intScreeningApplicationRequestId, NULL, false );
	}

	public function setRemoteScreeningId( $intRemoteScreeningId ) {
		$this->m_intRemoteScreeningId = CStrings::strToIntDef( $intRemoteScreeningId, NULL, false );
	}

	public function getScreeningDecisionTypeId() {
		return $this->m_intScreeningDecisionTypeId;
	}

	public function getScreeningApplicationRequestId() {
		return $this->m_intScreeningApplicationRequestId;
	}

	public function getRemoteScreeningId() {
		return $this->m_intRemoteScreeningId;
	}

	// Send Adverse action email letter for corporate screening

	public function sendBusinessAdverseActionLetter( $intCompanyUserId, $objCompanyAddress, $objClient, $objEmailDatabase, $objDatabase ) {

		if( false == valStr( $this->getEmailAddress() ) ) {
			return false;
		}

		$objApplicant = CApplicants::fetchApplicantByCustomerIdByCid( $this->getCustomerId(), $objClient->getId(), $objDatabase );

		if( false == valObj( $objApplicant, CApplicant::class ) ) {
			return false;
		}

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_INTERFACES_ENTRATA, false );
		$objSmarty->assign( 'corporate', $this );
		$objSmarty->assign( 'client', $objClient );
		$objSmarty->assign( 'company_details', $objCompanyAddress );
		$objSmarty->assign( 'image_url',	    CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strHtmlEmailOutput = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/applications/business_denial_email.tpl' );

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->fetchNextId( $objEmailDatabase );
		$objSystemEmail->setCid( $objClient->getId() );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setHtmlContent( $strHtmlEmailOutput );
		$objSystemEmail->setReplyToEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::URGENT );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::PROSPECT_EMAILER );
		$objSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );
		$objSystemEmail->setToEmailAddress( $this->getEmailAddress() );
		$objSystemEmail->setApplicantId( $objApplicant->getId() );
		$objSystemEmail->setSubject( 'Business Screening Decision' );

		if( true == $objSystemEmail->insert( $intCompanyUserId, $objEmailDatabase ) ) {

			$objCorporateEventLibrary = new CCorporateEventLibrary();
			$objCorporateEventData = new stdClass();
			$objCorporateEventData->eventTypeId     = CEventType::CORPORATE_EMAIL_OUTGOING;
			$objCorporateEventData->corporate       = $this;
			$objCorporateEventData->companyUserId   = $intCompanyUserId;
			$objCorporateEventData->dataReferenceId = $objSystemEmail->getId();
			$objCorporateEventData->notes           = 'Adverse action letter for business screening denial';

			if( false == $objCorporateEventLibrary->logCorporateEvent( $objCorporateEventData, $objDatabase ) ) {
				return false;
			}

			return true;
		}

		return false;
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

}
?>