<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileNotes
 * Do not add any new functions to this class.
 */

class CFileNotes extends CBaseFileNotes {

	public static function fetchFileNoteByFileIdByCid( $intFileId, $intCid, $objDatabase ) {
		return self::fetchFileNote( sprintf( 'SELECT * FROM file_notes WHERE file_id = %d AND cid = %d LIMIT 5', ( int ) $intFileId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchFileNoteByFileIdsByCid( $strFileIds, $intCid, $objDatabase ) {
		return self::fetchFileNotes( sprintf( 'SELECT * FROM file_notes WHERE file_id IN( %s ) AND cid = %d LIMIT 5', $strFileIds, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRecentFileNotesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {

		if( false == valId( $intFileId ) ) return NULL;
		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						fn.*,
						COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) as created_by_name
					FROM
						file_notes fn
						LEFT JOIN company_users cu ON ( fn.created_by = cu.id AND cu.cid = fn.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id and ce.cid = cu.cid )
					WHERE
						fn.file_id = ' . ( int ) $intFileId . '
						AND fn.cid = ' . ( int ) $intCid . '
					ORDER BY
						fn.id DESC
					 LIMIT 5';

		return self::fetchFileNotes( $strSql, $objDatabase );
	}

	public static function fetchAllFileNotesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {

		if( false == valId( $intFileId ) ) return NULL;
		if( false == valId( $intCid ) ) return NULL;

		 $strSql = 'SELECT
						fn.*,
						COALESCE( ce.name_first || \' \' || ce.name_last, cu.username ) as created_by_name
					FROM
						file_notes fn
						LEFT JOIN company_users cu ON ( fn.created_by = cu.id AND cu.cid = fn.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id and ce.cid = cu.cid )
					WHERE
						fn.file_id = ' . ( int ) $intFileId . '
						AND fn.cid = ' . ( int ) $intCid;

		return self::fetchFileNotes( $strSql, $objDatabase );
	}

	public static function fetchCountAllFileNotesByFileIdByCid( $intFileId, $intCid, $objDatabase ) {

		if( false == valId( $intFileId ) ) return NULL;
		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						count(1)
					FROM
						file_notes fn
						LEFT JOIN company_users cu ON ( fn.created_by = cu.id AND cu.cid = fn.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id and ce.cid = cu.cid )
					WHERE
						fn.file_id = ' . ( int ) $intFileId . '
						AND fn.cid = ' . ( int ) $intCid;

		$arrmixResponseData = fetchData( $strSql, $objDatabase );
		return ( false == is_null( $arrmixResponseData[0]['count'] ) ) ? $arrmixResponseData[0]['count'] : NULL;
	}

	public static function fetchFileNotesByFileIdsByCid( $arrintFileIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintFileIds ) ) return NULL;

		$strSql = 'SELECT
						fn.*
					FROM
						file_notes fn
					WHERE
						fn.file_id IN ( ' . implode( ',', $arrintFileIds ) . ' )
						AND fn.cid = ' . ( int ) $intCid;

		return self::fetchFileNotes( $strSql, $objDatabase );
	}

}
?>