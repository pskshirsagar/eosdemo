<?php

class CSubsidyGrossRentChange extends CBaseSubsidyGrossRentChange {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorrectedSubsidyGrossRentChangeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasBeenCorrected() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getSubsidyCertifications() {
		return \Psi\Eos\Entrata\CSubsidyCertifications::createService()->fetchSubsidyCertificationsBySubsidyGrossRentChangeIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );
	}

}
?>