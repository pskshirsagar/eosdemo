<?php

class CComplianceRuleset extends CBaseComplianceRuleset {

	const INVOICE_STATUS_ALLOWANCES         = 'invoice';
	const PAYMENT_STATUS_ALLOWANCES         = 'payment';
	const PURCHASE_ORDER_STATUS_ALLOWANCES  = 'purchase_order';

	protected $m_arrobjComplianceRules;

	protected $m_intApPayeeId;
	protected $m_intPropertyId;

	protected $m_strPropertyGroupIds;
	protected $m_strApPayeeIds;
	protected $m_strCategoryIds;

	protected $m_boolIsDataChange;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDataChange = false;

	}

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rule_set_name', __( 'Rule set name is required. ' ) ) );
		}
		return $boolIsValid;
	}

	public function valNotes() {
		return true;
	}

	public function valPriorityNum() {
		return true;
	}

	public function valIncludeAllProperties() {
		return true;
	}

	public function valIncludeAllCategories() {
		return true;
	}

	public function valIsOnsite() {
		return true;
	}

	public function valAllowDeniedPurchaseOrders() {
		return true;
	}

	public function valAllowUnenrolledPurchaseOrders() {
		return true;
	}

	public function valAllowPendingPurchaseOrders() {
		return true;
	}

	public function valAllowDeniedInvoices() {
		return true;
	}

	public function valAllowUnenrolledInvoices() {
		return true;
	}

	public function valAllowPendingInvoices() {
		return true;
	}

	public function valAllowDeniedPayments() {
		return true;
	}

	public function valAllowUnenrolledPayments() {
		return true;
	}

	public function valAllowPendingPayments() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function valRequireWaiverOfSubrogation() {
		$boolIsValid = true;
		if( true == $this->getRequireWaiverOfSubrogation() && false == valStr( $this->getRequireWaiverOfSubrogationDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'require_waiver_of_subrogation', __( 'Waiver of subrogation text is required. ' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valRequireWaiverOfSubrogation();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = $intApPayeeId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setApPayeeIds( $strApPayeeIds ) {
		$this->m_strApPayeeIds = $strApPayeeIds;
	}

	public function setPropertyGroupIds( $strPropertyGroupIds ) {
		$this->m_strPropertyGroupIds = $strPropertyGroupIds;
	}

	public function setCategoryIds( $strCategoryIds ) {
		$this->m_strCategoryIds = $strCategoryIds;
	}

	public function setOwnerTypeIds( $strOwnerTypeIds ) {
		$this->m_strOwnerTypeIds = $strOwnerTypeIds;
	}

	public function setIsDataChange( $boolIsDataChange ) {
		$this->m_boolIsDataChange = $boolIsDataChange;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_payee_id'] ) ) {
			$this->setApPayeeId( $arrmixValues['ap_payee_id'] );
		}

		if( true == isset( $arrmixValues['ap_payee_ids'] ) ) {
			$this->setApPayeeIds( $arrmixValues['ap_payee_ids'] );
		}

		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}

		if( true == isset( $arrmixValues['property_group_ids'] ) ) {
			$this->setPropertyGroupIds( $arrmixValues['property_group_ids'] );
		}

		if( true == isset( $arrmixValues['category_ids'] ) ) {
			$this->setCategoryIds( $arrmixValues['category_ids'] );
		}

		if( true == isset( $arrmixValues['owner_type_ids'] ) ) {
			$this->setOwnerTypeIds( $arrmixValues['owner_type_ids'] );
		}

		if( true == isset( $arrmixValues['allow_differential_update'] ) ) {
			$this->setAllowDifferentialUpdate( $arrmixValues['allow_differential_update'] );
		}
	}

	/**
	 * GET functions
	 */

	public function getComplianceRules() {
		return $this->m_arrobjComplianceRules;
	}

	public function addComplianceRule( $objComplianceRule ) {
		if( NULL == $objComplianceRule->getId() ) {
			$this->m_arrobjComplianceRules[] = $objComplianceRule;
		} else {
			$this->m_arrobjComplianceRules[$objComplianceRule->getId()] = $objComplianceRule;
		}
	}

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getApPayeeIds() {
		return $this->m_strApPayeeIds;
	}

	public function getPropertyGroupIds() {
		return $this->m_strPropertyGroupIds;
	}

	public function getCategoryIds() {
		return $this->m_strCategoryIds;
	}

	public function getOwnerTypeIds() {
		return $this->m_strOwnerTypeIds;
	}

	public function getIsDataChange() {
		return $this->m_boolIsDataChange;
	}

	/**
	 * Create Functions
	 */

	public function createComplianceRule() {
		$objComplianceRule = new CComplianceRule();
		$objComplianceRule->setCid( $this->getCid() );
		$objComplianceRule->setComplianceRulesetId( $this->getId() );
		return $objComplianceRule;
	}

	public function createComplianceJob() {
		$objComplianceJob = new CComplianceJob();
		$objComplianceJob->setCid( $this->getCid() );
		$objComplianceJob->setComplianceRulesetId( $this->getId() );
		return $objComplianceJob;
	}

	public function createComplianceRulesetVendor() {
		$objComplianceRulesetVendor = new CComplianceRulesetVendor();
		$objComplianceRulesetVendor->setCid( $this->getCid() );
		$objComplianceRulesetVendor->setComplianceRulesetId( $this->getId() );
		return $objComplianceRulesetVendor;
	}

	public function createComplianceRulesetPropertyGroup() {
		$objComplianceRulesetPropertyGroup = new CComplianceRulesetPropertyGroup();
		$objComplianceRulesetPropertyGroup->setCid( $this->getCid() );
		$objComplianceRulesetPropertyGroup->setComplianceRulesetId( $this->getId() );
		return $objComplianceRulesetPropertyGroup;
	}

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase );
		}

		return $this->update( $intUserId, $objDatabase );
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrmixOriginalValues		= [];

		if( true == $this->getAllowDifferentialUpdate() ) {

			$this->unSerializeAndSetOriginalValues();
			$arrmixOriginalValues		= $this->m_arrstrOriginalValues;

			foreach( $arrmixOriginalValues as $strKey => $strOriginalValue ) {

				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );
				$strFunctionName    = 'get' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

				if( $strOriginalValue == 'f' ) {
					$strOriginalValue = 'false';
				}

				if( $strOriginalValue == 't' ) {
					$strOriginalValue = 'true';
				}

				if( true == in_array( $strKey, [ 'ap_payee_ids', 'property_group_ids', 'category_ids' ] ) ) {

					$arrintIds			= array_unique( array_filter( explode( ',',  $this->$strFunctionName() ) ) );
					$arrintOriginalIds	= array_unique( array_filter( explode( ',',  $strOriginalValue ) ) );

					if( 0 != \Psi\Libraries\UtilFunctions\count( array_diff( $arrintIds, $arrintOriginalIds ) ) || 0 != \Psi\Libraries\UtilFunctions\count( array_diff( $arrintOriginalIds, $arrintIds ) ) ) {
						$this->setIsDataChange( true );
					}
				}

				if( false == in_array( $strKey, [ 'allow_differential_update', 'ap_payee_ids', 'property_group_ids', 'category_ids' ] ) && CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $strOriginalValue ) {
					$this->setIsDataChange( true );
				}
			}
		}

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	// @TODO: Temprory code, once done with migration in Entrata will remove this code.

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( CWorker::USER_ID_FLAG_TO_MIGRATE_COMPLIANCE_DATA != $intCurrentUserId ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		} else {
			$this->setDatabase( $objDatabase );

			$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

			$strSql = 'INSERT INTO
							' . static::TABLE_NAME . '( id, cid, name, notes, require_waiver_of_subrogation_description, priority_num, include_all_properties, include_all_categories, include_all_vendors, is_onsite, require_waiver_of_subrogation, allow_not_compliant_purchase_orders, allow_required_purchase_orders, allow_pending_purchase_orders, allow_not_compliant_invoices, allow_required_invoices, allow_pending_invoices, allow_not_compliant_payments, allow_required_payments, allow_pending_payments, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, is_include_all_owner_types )
						VALUES ( ' .
								$strId . ', ' .
								$this->sqlCid() . ', ' .
								$this->sqlName() . ', ' .
								$this->sqlNotes() . ', ' .
								$this->sqlRequireWaiverOfSubrogationDescription() . ', ' .
								$this->sqlPriorityNum() . ', ' .
								$this->sqlIncludeAllProperties() . ', ' .
								$this->sqlIncludeAllCategories() . ', ' .
								$this->sqlIncludeAllVendors() . ', ' .
								$this->sqlIsOnsite() . ', ' .
								$this->sqlRequireWaiverOfSubrogation() . ', ' .
								$this->sqlAllowNotCompliantPurchaseOrders() . ', ' .
								$this->sqlAllowRequiredPurchaseOrders() . ', ' .
								$this->sqlAllowPendingPurchaseOrders() . ', ' .
								$this->sqlAllowNotCompliantInvoices() . ', ' .
								$this->sqlAllowRequiredInvoices() . ', ' .
								$this->sqlAllowPendingInvoices() . ', ' .
								$this->sqlAllowNotCompliantPayments() . ', ' .
								$this->sqlAllowRequiredPayments() . ', ' .
								$this->sqlAllowPendingPayments() . ', ' .
								$this->sqlDeletedBy() . ', ' .
								$this->sqlDeletedOn() . ', ' .
								$this->sqlUpdatedBy() . ', ' .
								$this->sqlUpdatedOn() . ', ' .
								$this->sqlCreatedBy() . ', ' .
								$this->sqlCreatedOn() . ', ' .
								$this->sqlDetails() . ', ' .
								$this->sqlIsIncludeAllOwnerTypes() . ' ) ' . ' RETURNING id;';

			if( true == $boolReturnSqlOnly ) {
				return $strSql;
			} else {
				return $this->executeSql( $strSql, $this, $objDatabase );
			}
		}
	}

}
?>