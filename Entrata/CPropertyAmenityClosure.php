<?php

class CPropertyAmenityClosure extends CBasePropertyAmenityClosure {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyAmenityId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClosureStart() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClosureEnd() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }
}
?>