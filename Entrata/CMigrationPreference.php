<?php

class CMigrationPreference extends CBaseMigrationPreference {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valCid() {
		$boolIsValid = true;

		if( false == isset ( $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'You must provide an client id.' ) );
		}

		return $boolIsValid;
    }

	public function valImportBatchId() {
		$boolIsValid = true;

		if( false == isset ( $this->m_intImportBatchId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'import_batch_id', 'You must provide an import batch id.' ) );
		}
		return $boolIsValid;
	}

    public function valImportDataTypeId() {
		$boolIsValid = true;

		if( false == isset ( $this->m_intImportDataTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'import_data_type_id', 'You must provide an import data type id.' ) );
		}

		return $boolIsValid;
	}

    public function valSourceKey() {
		$boolIsValid = true;

		if( false == isset ( $this->m_strSourceKey ) && false == valStr( $this->m_strSourceKey ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'source_key', 'You must provide an source key id.' ) );
		}

		return $boolIsValid;
	}

    public function valKey() {
		$boolIsValid = true;

		if( false == isset ( $this->m_strKey ) && false == valStr( $this->m_strKey ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'You must provide an key.' ) );
		}

		return $boolIsValid;
	}

    public function valValue() {
		$boolIsValid = true;

		if( false == isset ( $this->m_strValue ) && false == valStr( $this->m_strValue ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'You must provide an migration database backup date.' ) );
			return $boolIsValid;
		}

		if( true == CValidation::checkDateFormat( $this->m_strValue ) && strtotime( $this->m_strValue ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Migration database backup date is not greater than today\'s date.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

    public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				// $boolIsValid &= $this->valImportBatchId();
				$boolIsValid &= $this->valImportDataTypeId();
				$boolIsValid &= $this->valSourceKey();
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valValue();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

            default:
            	$boolIsValid = true;
            	break;
		}

        return $boolIsValid;
    }
}
?>