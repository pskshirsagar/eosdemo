<?php

class CTransmissionType extends CBaseTransmissionType {

	const APPLICATION_FORMS_INTEGRATION 	= 1;
	const LEAD_MANAGEMENT					= 2;
	const LEASE_FORM_INTEGRATION			= 3;
	const REVENUE_MANAGEMENT				= 4;
	const SCREENING							= 5;
	const UTILITY_BILLING					= 6;
	const VAULTWARE							= 7;
	const FINANCIAL							= 8;
	const DOCUMENT_STORAGE					= 9;
	const REPUTATION_ADVISOR				= 10;
	const GL_POSITIVEPAY_EXPORT				= 11;
	const VENDOR_MANAGEMENT                 = 12;
	const UBSEXPORT                         = 13;
	const WELLS_FARGO_PAYMENT_MANAGER		= 14;
	const POSITIVE_PAY                      = 15;
	const GL_EXPORT                         = 16;
	const AP_EXPORT                         = 17;
	const CHARGE_PAYMENT_EXPORT             = 19;
	const ACH_PAYMENTS                      = 20;
	const RESIDENT_EXPORT                   = 21;
	const API_EXPORT                        = 22;
	const DATA_EXPORT                       = 23;
	const HOSPITALITY                       = 24;
	const INTEGRATED_PAYABLES               = 25;

	public static $c_arrintAccountingExportTransmissionTypes = [
		self::POSITIVE_PAY => self::POSITIVE_PAY,
		self::GL_EXPORT    => self::GL_EXPORT,
		self::AP_EXPORT    => self::AP_EXPORT,
		self::ACH_PAYMENTS	=> self::ACH_PAYMENTS,
		self::WELLS_FARGO_PAYMENT_MANAGER	=> self::WELLS_FARGO_PAYMENT_MANAGER,
		self::INTEGRATED_PAYABLES => self::INTEGRATED_PAYABLES
	];

	public static $c_arrintApiServiceProductTransmissionTypeNames = [
		self::AP_EXPORT    => 'AP Export',
		self::GL_EXPORT    => 'GL Export',
		self::POSITIVE_PAY => 'Positive Pay',
		self::INTEGRATED_PAYABLES => 'Integrated Payables'
	];

	public static $c_arrintExportTransmissionTypes = [
		self::POSITIVE_PAY					=> self::POSITIVE_PAY,
		self::GL_EXPORT						=> self::GL_EXPORT,
		self::AP_EXPORT						=> self::AP_EXPORT,
		self::WELLS_FARGO_PAYMENT_MANAGER	=> self::WELLS_FARGO_PAYMENT_MANAGER,
		self::CHARGE_PAYMENT_EXPORT			=> self::CHARGE_PAYMENT_EXPORT,
		self::ACH_PAYMENTS					=> self::ACH_PAYMENTS,
		self::DATA_EXPORT					=> self::DATA_EXPORT,
		self::INTEGRATED_PAYABLES           => self::INTEGRATED_PAYABLES
	];
}
?>