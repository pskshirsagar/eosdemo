<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyResidenceTypes
 * Do not add any new functions to this class.
 */

class CCompanyResidenceTypes extends CBaseCompanyResidenceTypes {

	public static function fetchCompanyResidenceTypeNameByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						name
					FROM
						company_residence_types
				   	WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId . '
						LIMIT 1';

		return parent::fetchColumn( $strSql, 'name', $objDatabase );
	}
}
?>