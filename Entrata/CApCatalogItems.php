<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApCatalogItems
 * Do not add any new functions to this class.
 */

class CApCatalogItems extends CBaseApCatalogItems {

	public static function fetchApCatalogItemsWithApPayeeNameByApCodeIdByCid( $intApCodeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApCodeId ) ) return NULL;

		$strSql = 'SELECT
						cs.id,
						cs.cid,
						cs.ap_code_id,
						cs.ap_payee_id,
						cs.vendor_sku,
						cs.vendor_unit_cost,
						cs.unit_of_measure_id,
						ap.company_name as ap_payee_name
					FROM
						ap_catalog_items cs
						JOIN ap_payees ap ON ( cs.cid = ap.cid AND cs.ap_payee_id = ap.id )
					WHERE
						cs.cid = ' . ( int ) $intCid . '
						AND cs.ap_code_id = ' . ( int ) $intApCodeId . '
					ORDER BY id ASC';

		return self::fetchApCatalogItems( $strSql, $objClientDatabase );
	}

	public static function fetchApCatalogItemsByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_catalog_items cs
					WHERE
						cs.cid = ' . ( int ) $intCid . '
						AND cs.id IN ( ' . sqlIntImplode( $arrintIds ) . ' )';

		return self::fetchApCatalogItems( $strSql, $objClientDatabase );
	}

	public static function fetchApCatalogItemsByApCodeIdsByCid( $arrintApCodeIds, $intCid, $objClientDatabase ) {
		if( false == ( $arrintApCodeIds = getIntValuesFromArr( $arrintApCodeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_catalog_items cs
					WHERE
						cs.cid = ' . ( int ) $intCid . '
						AND cs.ap_code_id IN ( ' . sqlIntImplode( $arrintApCodeIds ) . ' )';

		return self::fetchApCatalogItems( $strSql, $objClientDatabase );
	}

	public static function fetchApCatalogItemsWithPropertyGroupAssociationsByApCodeIdsByCid( $arrintApCodeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApCodeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT cs.ap_code_id,
						pga.property_id
					FROM
						ap_catalog_items cs
						JOIN catalog_sku_property_groups cspg ON ( cs.cid = cspg.cid AND cs.id = cspg.ap_catalog_item_id )
						JOIN property_group_associations pga ON ( cspg.cid = pga.cid AND cspg.property_group_id = pga.property_group_id )
					WHERE
						cs.cid = ' . ( int ) $intCid . '
						AND cs.ap_code_id IN ( ' . implode( ' ,', $arrintApCodeIds ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApCatalogItemsByApCodeIdsByCid( $arrintApCodeIds, $intCid, $objClientDatabase ) {
		if( false == ( $arrintApCodeIds = getIntValuesFromArr( $arrintApCodeIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_catalog_items
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_code_id IN ( ' . sqlIntImplode( $arrintApCodeIds ) . ' )';

		return self::fetchApCatalogItems( $strSql, $objClientDatabase );
	}

	public static function fetchApCatalogItemsByApPayeeIdByCidBySkus( $intApPayeeId, $intCid, $arrstrSkus, $objClientDatabase ) {
		if( false == valArr( $arrstrSkus = array_filter( $arrstrSkus ) ) ) {
			return NULL;
		}

		$arrstrSkus = array_map( 'addslashes', $arrstrSkus );

		$strSql = 'SELECT
						aci.*,
						ac.name AS ap_code_name,
						ac.item_gl_account_id,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name
					FROM
						ap_catalog_items AS aci
						JOIN ap_codes AS ac ON ( aci.cid = ac.cid AND aci.ap_code_id = ac.id )
						JOIN gl_accounts AS ga ON ( ac.cid = ga.cid AND ac.item_gl_account_id = ga.id )
						JOIN gl_account_trees gat ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						aci.cid = ' . ( int ) $intCid . '
						AND aci.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND aci.vendor_sku IN ( ' . sqlStrImplode( $arrstrSkus ) . ' )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApCatalogItemsByApPayeeIdsByCidBySkus( $arrintApPayeeIds, $intCid, $arrstrSkus, $objClientDatabase, $intApCodeId = NULL ) {

		if( false == ( $arrintApPayeeIds = getIntValuesFromArr( $arrintApPayeeIds ) ) || false == valArr( $arrstrSkus = array_filter( $arrstrSkus ) ) ) {
			return NULL;
		}

		$arrstrSkus = array_map( 'addslashes', $arrstrSkus );

		$strWhereClause = '';

		if( true == valId( $intApCodeId ) ) {
			$strWhereClause = ' AND aci.ap_code_id != ' . ( int ) $intApCodeId;
		}

		$strSql = 'SELECT
						aci.ap_payee_id,
						aci.vendor_sku,
						ac.name AS catalog_item_name
					FROM
						ap_catalog_items AS aci
						join ap_codes AS ac ON ( aci.cid = ac.cid AND aci.ap_code_id = ac.id )
					WHERE
						aci.cid = ' . ( int ) $intCid . '
						AND aci.ap_payee_id IN ( ' . sqlIntImplode( $arrintApPayeeIds ) . ' )
						AND aci.vendor_sku IN ( ' . sqlStrImplode( $arrstrSkus ) . ' )
						AND aci.deleted_by IS NULL
						AND aci.deleted_on IS NULL'
						. $strWhereClause;

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>