<?php

class CIlsMediaFileDownload extends CBaseIlsMediaFileDownload {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyFloorplanId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImageRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImageUrlToDownload() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCaption() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDownloaded() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSortOrder() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImageUrlAddedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImageDownloadedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>