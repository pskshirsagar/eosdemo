<?php

class CScheduledPaymentDetail extends CBaseScheduledPaymentDetail {

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['check_account_number'] ) ) $this->setCheckAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['check_account_number'] ) : $arrValues['check_account_number'] );
		if( true == isset( $arrValues['cc_card_number'] ) ) $this->setCcCardNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cc_card_number'] ) : $arrValues['cc_card_number'] );
	}

	public function setCcCardNumber( $strPlainCcNumber ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strPlainCcNumber, 'XXXX' ) ) return;
		$strPlainCcNumber = CStrings::strTrimDef( $strPlainCcNumber, 20, NULL, true );
		if( true == valStr( $strPlainCcNumber ) ) {
			$this->setCcCardNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPlainCcNumber, CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );
		}
	}

	public function setCheckAccountNumber( $strPlainCheckAccountNumber ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strPlainCheckAccountNumber, 'XXXX' ) ) return;
		$strPlainCheckAccountNumber = CStrings::strTrimDef( $strPlainCheckAccountNumber, 20, NULL, true );
		if( true == valStr( $strPlainCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strPlainCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	public function getCcCardNumberMasked() {
		$strCcCardNumber = $this->getCcCardNumber();
		$intStringLength = strlen( $strCcCardNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCcCardNumber, -4 );

		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCcCardNumber() {
		if( false == valStr( $this->getCcCardNumberEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getCcCardNumberEncrypted(), CONFIG_SODIUM_KEY_CC_CARD_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_CC_CARD_NUMBER ] );
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = substr( $strCheckAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getCheckAccountNumber() {
		if( false == valStr( $this->getCheckAccountNumberEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getCheckAccountNumberEncrypted(), CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

}
?>