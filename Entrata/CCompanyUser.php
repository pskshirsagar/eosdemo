<?php
use Psi\Eos\Entrata\CWebsites;
use Psi\Eos\Entrata\CCompanyEmployees;
use Psi\Eos\Entrata\CCompanyPreferences;
use Psi\Eos\Entrata\CPropertyProducts;
use Psi\Eos\Entrata\CPropertyProductLogs;
use Psi\Eos\Admin\CAccounts;

use Psi\Libraries\NewRelic\CNewRelic;
use Psi\Libraries\UtilHash\CHash;

class CCompanyUser extends CBaseCompanyUser {

	const REMOTE_FOREIGN_KEY_DELIMITER = '~..~';

	const TOTAL_LOGIN_ATTEMPTS			= 15;
	const LOGIN_ATTEMPT_WAITING_TIME	= 1800;
	const LOGIN_ATTEMPT_PERIOD			= 300;

	const RESIDENT_WORKS		= 10;
	const PROSPECT_PORTAL		= 11;
	const RESIDENT_PORTAL		= 12;
	const MIGRATION_IMPORT		= 18;
	const PROPERTY_SOLUTIONS	= 27;
	const RESIDENT_SYNC_APP		= 30;
	const INTEGRATION			= 61;
	const VENDOR_ACCESS			= 21;
	const ID_UEM_IP				= 48;
	const SYSTEM				= 1;
	const SYSTEM_RENEWAL_USER	= 20;
	const CLIENT_ADMIN = 7;
	const DML_DEPLOYMENT = 8;

	const PASSWORD_ROTATION_NUMBER								= 4;
	const DEFAULT_PASSWORD_LENGTH								= 8;

	// Please do not use constant MAX_PSI_ADMIN_USER_ID for validating PSI user, instead validate with company_user_type_id.
	const MAX_PSI_ADMIN_USER_ID 								= 10000;

	const PSI_ADMIN_DISPLAY_USERNAME							= 'System';
	const ENTRATA_ILS_API_USERNAME								= 'entrataIlsApiUser';
	const ENTRATA_ILS_API_USER_ID								= 66;
	const ENTRATA_RV_API_USERNAME								= 'entrataRvApiUser';
	const ENTRATA_RV_API_USER_ID								= 74;
	const COMPANY_USER_REVENUE_MANAGEMENT						= 65;
	const UPDATE_LEASE_STATUS_TYPE_AND_ACTIVE_LEASE_INTERVAL	= 8054;
	const ENTRATA_USER											= 'Entrata';

	protected $m_objClient;
	protected $m_objAdminDatabase;

	protected $m_arrobjCompanyUserWebsites;
	protected $m_arrobjCompanyUserGroups;
	protected $m_arrobjProperties;      // Actual Properties
	protected $m_arrobjWebsites;
	protected $m_arrobjCompanyApps;
	protected $m_arrintCompanyUserPropertyIds;
	protected $m_arrmixRequiredParameters;

	private $m_arrstrModulePermissions;

	protected $m_intAuthenticationLogId;

	protected $m_strDrowssap;
	protected $m_strOldUsername;
	protected $m_strPassword;
	protected $m_strSecondPassword;
	protected $m_strConfirmPassword;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strNameFull;
	protected $m_strRwxDomain;
	protected $m_strCurrentPassword;
	protected $m_strBluemoonPasswordEncrypted;
	protected $m_strPreferredLocaleCode;
	protected $m_strSsoToken;

	protected $m_objCompanyEmployee;
	protected $m_arrintReportIds;

	protected $m_boolIsLoggedIn;
	protected $m_boolIsPSIUser;
	protected $m_boolIsPasswordRotation;
	protected $m_boolIsEditTooltipAllowed;
	protected $m_boolIsRequireTwoFactorAuthentication;
	protected $m_boolIsTwoFactorAuthenticationValidated;
	protected $m_boolIsEntrataNotificationEnabledUser;
	protected $m_boolUseUsernameInsteadOfEmail;
	protected $m_boolIsMarketingUser;
	protected $m_boolIsPreviewMode;
	protected $m_boolIsSamlRestrictEntrataLogin;

	protected $m_strCompanyGroupName;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLoggedIn                         = false;
		$this->m_boolIsPSIUser                          = false;
		$this->m_boolIsPasswordRotation                 = false;
		$this->m_boolIsRequireTwoFactorAuthentication   = false;
		$this->m_boolIsTwoFactorAuthenticationValidated = false;
		$this->m_arrstrModulePermissions                = [];
		$this->m_boolIsMarketingUser                    = false;
		$this->m_boolIsSamlRestrictEntrataLogin         = false;
		$this->m_boolIsScimUser                         = false;
		return;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createMaintenanceRequestFilter() {

		$objMaintenanceRequestFilter = new CMaintenanceRequestFilter();
		$objMaintenanceRequestFilter->setCid( $this->m_intCid );
		$objMaintenanceRequestFilter->setCompanyUserId( $this->m_intId );

		return $objMaintenanceRequestFilter;
	}

	public function createCompanyUserPropertyGroup() {

		$objCompanyUserPropertyGroup = new CCompanyUserPropertyGroup();
		$objCompanyUserPropertyGroup->setCid( $this->getCid() );
		$objCompanyUserPropertyGroup->setCompanyUserId( $this->getId() );

		return $objCompanyUserPropertyGroup;
	}

	public function createCompanyUserWebsite() {

		$objCompanyUserWebsite = new CCompanyUserWebsite();
		$objCompanyUserWebsite->setCid( $this->getCid() );
		$objCompanyUserWebsite->setCompanyUserId( $this->getId() );

		return $objCompanyUserWebsite;
	}

	public function createCompanyUserService() {

		$objCompanyUserService = new CCompanyUserService();
		$objCompanyUserService->setCid( $this->getCid() );
		$objCompanyUserService->setCompanyUserId( $this->getId() );

		return $objCompanyUserService;
	}

	public function createCompanyUserGroup() {

		$objCompanyUserGroup = new CCompanyUserGroup();
		$objCompanyUserGroup->setCid( $this->getCid() );
		$objCompanyUserGroup->setCompanyUserId( $this->getId() );

		return $objCompanyUserGroup;
	}

	public function createCompanyUserPermission( $intModuleId ) {

		$objCompanyUserPermission = new CCompanyUserPermission();
		$objCompanyUserPermission->setCid( $this->getCid() );
		$objCompanyUserPermission->setCompanyUserId( $this->getId() );
		$objCompanyUserPermission->setModuleId( $intModuleId );

		return $objCompanyUserPermission;
	}

	public function createPropertyLeasingAgent() {

		$objCompanyLeasingAgent = new CPropertyLeasingAgent();
		$objCompanyLeasingAgent->setCid( $this->getCid() );
		$objCompanyLeasingAgent->setCompanyEmployeeId( $this->getCompanyEmployeeId() );

		return $objCompanyLeasingAgent;
	}

	public function createApplicationFilter() {

		$objApplicationFilter = new CApplicationFilter();
		$objApplicationFilter->setDefaults();
		$objApplicationFilter->setCid( $this->m_intCid );
		$objApplicationFilter->setCompanyUserId( $this->m_intId );

		return $objApplicationFilter;
	}

	public function createGlobalUser( $boolIsBlueMoonUser = false ) {

		$objGlobalUser = new CGlobalUser();
		$objGlobalUser->setWorksId( $this->getId() );
		$objGlobalUser->setCid( $this->getCid() );
		$objGlobalUser->setUsername( $this->getUsername() );
		$objGlobalUser->setPasswordEncrypted( $this->getPasswordEncrypted() );
		$objGlobalUser->setGlobalUserTypeId( $this->getCompanyUserTypeId() );

		if( true == $boolIsBlueMoonUser ) {
			$objGlobalUser->setBluemoonEmailAddress( $this->getUsername() );
			$objGlobalUser->setGlobalUserTypeId( CGlobalUserType::BLUEMOON_USER );
			$objGlobalUser->setBluemoonPassword( $this->m_strPassword );
		}

		return $objGlobalUser;
	}

	public function createCompanyUserPreference() {

		$objCompanyUserPreference = new CCompanyUserPreference();
		$objCompanyUserPreference->setCid( $this->getCid() );
		$objCompanyUserPreference->setCompanyUserId( $this->getId() );

		return $objCompanyUserPreference;
	}

	public function createCompanyUserDetail() {

		$objCompanyUserDetail = new CCompanyUserDetail();
		$objCompanyUserDetail->setCid( $this->getCid() );
		$objCompanyUserDetail->setCompanyUserId( $this->getId() );

		return $objCompanyUserDetail;
	}

	public function createAuthenticationLog( $strSessionId = NULL ) {

		$objAuthenticationLog = new CAuthenticationLog();
		$objAuthenticationLog->setCid( $this->getCid() );
		$objAuthenticationLog->setIpAddress( getRemoteIpAddress() );
		$objAuthenticationLog->setLoginDatetime( 'NOW()' );
		$objAuthenticationLog->setCompanyUserId( $this->getId() );
		$objAuthenticationLog->setSessionId( $strSessionId );

		return $objAuthenticationLog;
	}

	public function createScheduledEmailFilter() {

		$objScheduledEmailFilter = new CScheduledEmailFilter();
		$objScheduledEmailFilter->setDefaults();
		$objScheduledEmailFilter->setCid( $this->getCid() );
		$objScheduledEmailFilter->setCompanyUserId( $this->getId() );

		return $objScheduledEmailFilter;

	}

	public function createOrFetchCompanyUserToken( $objClientDatabase ) {

		$objCompanyUserToken = new CCompanyUserToken();
		$objCompanyUserToken->setCompanyUserId( $this->getId() );
		$objCompanyUserToken->setToken( md5( time() ) );
		$objCompanyUserToken->setCid( $this->getCid() );

		if( ( false == $objCompanyUserToken->validate( VALIDATE_INSERT ) ) || ( false == $objCompanyUserToken->insert( $this->getId(), $objClientDatabase ) ) ) {
			$this->addErrorMsg( __( 'Failed to insert company user token.' ) );

			return NULL;
		}

		return $objCompanyUserToken;
	}

	public function createMaintenanceRequestNote() {

		$objMaintenanceRequestNote = new CMaintenanceRequestNote();
		$objMaintenanceRequestNote->setCid( $this->getCid() );
		$objMaintenanceRequestNote->setCompanyUserId( $this->getId() );

		return $objMaintenanceRequestNote;
	}

	public function createInspectionFilter() {

		$objInspectionFilter = new CInspectionFilter();
		$objInspectionFilter->setDefaults();
		$objInspectionFilter->setCid( $this->m_intCid );
		$objInspectionFilter->setCompanyUserId( $this->m_intId );

		return $objInspectionFilter;
	}

	public function createModuleFilter() {

		$objModuleFilter = new CModuleFilter();
		$objModuleFilter->setCid( $this->m_intCid );
		$objModuleFilter->setCompanyUserId( $this->m_intId );

		return $objModuleFilter;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setRwxDomain( $strRwxDomain ) {
		$this->m_strRwxDomain = $strRwxDomain;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setAuthenticationLogId( $intAuthenticationLogId ) {
		$this->m_intAuthenticationLogId = CStrings::strToIntDef( $intAuthenticationLogId, NULL, true );
	}

	public function setPassword( $strPassword ) {
		$this->m_strPassword  = trim( $strPassword );
		if( true == valStr( $strPassword ) ) {
			$this->set( 'm_strPasswordEncrypted', ( \Psi\Libraries\Cryptography\CCrypto::create() )->hash( trim( $strPassword ) ) );
		}
	}

	public function setIsLoggedIn( $boolIsLoggedIn ) {
		$this->m_boolIsLoggedIn = $boolIsLoggedIn;
	}

	public function setIsPSIUser() {
		$this->m_boolIsPSIUser = ( true == in_array( $this->getCompanyUserTypeId(), CCompanyUserType::$c_arrintPsiUserTypeIds ) ) ? true : false;
	}

	public function setCompanyGroupName( $strCompanyGroupName ) {
		$this->m_strCompanyGroupName = $strCompanyGroupName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		if( false == $this->m_boolInitialized && true == isset( $arrmixValues['username'] ) ) {
			$this->setOldUsername( $arrmixValues['username'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['password'] ) ) {
			$this->setPassword( $arrmixValues['password'] );
		}
		if( true == isset( $arrmixValues['confirm_password'] ) ) {
			$this->setConfirmPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['confirm_password'] ) : $arrmixValues['confirm_password'] );
		}
		if( true == isset( $arrmixValues['email_address'] ) ) {
			$this->setEmailAddress( $arrmixValues['email_address'] );
		}
		if( true == isset( $arrmixValues['preferred_locale_code'] ) ) {
			$this->setPreferredLocaleCode( $arrmixValues['preferred_locale_code'] );
		}
		if( true == isset( $arrmixValues['phone_number'] ) ) {
			$this->setPhoneNumber( $arrmixValues['phone_number'] );
		}
		if( true == isset( $arrmixValues['name_first'] ) ) {
			$this->setNameFirst( $arrmixValues['name_first'] );
		}
		if( true == isset( $arrmixValues['name_last'] ) ) {
			$this->setNameLast( $arrmixValues['name_last'] );
		}
		if( true == isset( $arrmixValues['current_password'] ) ) {
			$this->setCurrentPassword( $arrmixValues['current_password'] );
		}
		if( true == isset( $arrmixValues['company_group_name'] ) ) $this->setCompanyGroupName( $arrmixValues['company_group_name'] );
		$this->setIsPSIUser();

		return;
	}

	public function setOldUsername( $strOldUsername ) {
		$this->m_strOldUsername = $strOldUsername;
	}

	public function setUsername( $strUsername, $boolDoNotStripSpecialCharacters = false ) {
		$boolDoNotStripSpecialCharacters ? trim( $strUsername ) : $strUsername = preg_replace( '/[^@a-zA-Z0-9._+\-\s]/', '', trim( $strUsername ) );
		if( false == is_null( $this->m_boolUseUsernameInsteadOfEmail ) ) {
			$strUsername = $this->getFormattedUsername( $strUsername );
		}
		parent::setUsername( $strUsername );
	}

	public function setCompanyUserWebsites( $arrobjCompanyUserWebsites ) {
		$this->m_arrobjCompanyUserWebsites = $arrobjCompanyUserWebsites;
	}

	public function setCompanyUserGroups( $arrobjCompanyUserGroups ) {
		$this->m_arrobjCompanyUserGroups = $arrobjCompanyUserGroups;
	}

	public function setCompanyUserPropertyIds( $arrintCompanyUserPropertyIds ) {
		$this->m_arrintCompanyUserPropertyIds = $arrintCompanyUserPropertyIds;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setCurrentPassword( $strCurrentPassword ) {
		$this->m_strCurrentPassword = $strCurrentPassword;
	}

	public function setIsPasswordRotation( $boolIsPasswordRotation ) {
		$this->m_boolIsPasswordRotation = ( bool ) $boolIsPasswordRotation;
	}

	public function setIsEditTooltipAllowed( $boolIsEditTooltipAllowed ) {
		$this->m_boolIsEditTooltipAllowed = $boolIsEditTooltipAllowed;
	}

	public function setSecondPassword( $strSecondPassword ) {
		$this->m_strSecondPassword = $strSecondPassword;
	}

	public function setBluemoonPasswordEncrypted( $strBluemoonPasswordEncrypted ) {
		$this->m_strBluemoonPasswordEncrypted = $strBluemoonPasswordEncrypted;
	}

	public function setUseUsernameInsteadOfEmail( $boolUseUsernameInsteadOfEmail ) {
		$this->m_boolUseUsernameInsteadOfEmail = $boolUseUsernameInsteadOfEmail;
	}

	public function setIsMarketingUser( $boolIsMarketingUser ) {
		$this->m_boolIsMarketingUser = $boolIsMarketingUser;
	}

	public function setPreferredLocaleCode( $strLoacleCode ) {
		$this->m_strPreferredLocaleCode = $strLoacleCode;
	}

	public function getIsMarketingUser() {
		return $this->m_boolIsMarketingUser;
	}

	public function setIsPreviewMode( $boolIsPreviewMode ) {
		$this->m_boolIsPreviewMode = ( bool ) $boolIsPreviewMode;
	}

	public function setIsSamlRestrictEntrataLogin( $boolIsSamlRestrictEntrataLogin ) {
		$this->m_boolIsSamlRestrictEntrataLogin = ( bool ) $boolIsSamlRestrictEntrataLogin;
	}

	public function setSsoToken( $strSsoToken ) {
		$this->m_strSsoToken = $strSsoToken;
	}

	public function setIsScimUser( $boolIsScimUser ) {
		$this->m_boolIsScimUser = $boolIsScimUser;
	}

	/**
	 * Get or Fetch Functions
	 *
	 */

	public function getOrFetchClient( $objDatabase ) {
		if( false == valObj( $this->m_objClient, 'CClient' ) ) {
			$this->m_objClient = $this->fetchClient( $objDatabase );
		}

		return $this->m_objClient;
	}

	public function getOrFetchProperties( $objDatabase ) {
		if( true == is_null( $this->m_arrobjProperties ) ) {
			$this->m_arrobjProperties = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCidByCompanyUserId( $this->getCid(), $this->getId(), $objDatabase, $this->getIsAdministrator() );
		}

		return $this->m_arrobjProperties;
	}

	public function getOrFetchCompanyEmployee( $objDatabase ) {
		if( true == is_null( $this->m_objCompanyEmployee ) ) {
			$this->m_objCompanyEmployee = CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $this->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objCompanyEmployee;
	}

	public function getOrFetchPMSoftwareEnabledProperties( $objDatabase, $intPageNo = NULL, $intPageSize = NULL ) {
		if( true == is_null( $this->m_arrobjProperties ) ) {
			$this->m_arrobjProperties = \Psi\Eos\Entrata\CProperties::createService()->fetchPMSoftwareEnabledPropertiesByCompanyUserIdByCid( $this->getId(), $this->getIsAdministrator(), $this->getCid(), $objDatabase, $intPageNo, $intPageSize );
		}

		return $this->m_arrobjProperties;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getUsername( $boolCheckSystemUsername = true ) {

		// self::VENDOR_ACCESS != $this->getId() this is to show vendor access in routing rules
		if( true == $boolCheckSystemUsername && true == valStr( $this->getId() ) && ( true == in_array( $this->getCompanyUserTypeId(), CCompanyUserType::$c_arrintPsiUserTypeIds ) ) && self::RESIDENT_PORTAL != $this->getId() && self::VENDOR_ACCESS != $this->getId() ) {
			return self::PSI_ADMIN_DISPLAY_USERNAME;
		}

		return $this->m_strUsername;
	}

	public function getOldUsername() {
		return $this->m_strOldUsername;
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function getRwxDomain() {
		return $this->m_strRwxDomain;
	}

	public function getUsernameWithoutRwxDomain() {
		return preg_replace( '/@' . $this->m_strRwxDomain . '$/', '', $this->m_strUsername );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getNameFull() {
		if( valStr( $this->m_strNameFirst ) || valStr( $this->m_strNameLast ) ) {
			return trim( $this->m_strNameFirst . ' ' . $this->m_strNameLast );
		}
	}

	public function getFullDetail() {
		if( true == valStr( $this->m_strEmailAddress ) ) {
			return $this->m_strNameFirst . ' ' . $this->m_strNameLast . ' - ' . $this->m_strEmailAddress;

		} else {
			return $this->m_strNameFirst . ' ' . $this->m_strNameLast;

		}
	}

	public function getAuthenticationLogId() {
		return $this->m_intAuthenticationLogId;
	}

	public function getIsLoggedIn() {
		return $this->m_boolIsLoggedIn;
	}

	public function getIsPSIUser() {
		return $this->m_boolIsPSIUser;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	public function getCid() {
		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient->getId();
		} else {
			return parent::getCid();
		}
	}

	public function getConfirmPassword() {
		return $this->m_strConfirmPassword;
	}

	public function getCompanyUserWebsites() {
		return $this->m_arrobjCompanyUserWebsites;
	}

	public function getCompanyUserGroups() {
		return $this->m_arrobjCompanyUserGroups;
	}

	public function getCompanyUserPropertyIds() {
		return $this->m_arrintCompanyUserPropertyIds;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getCurrentPassword() {
		return $this->m_strCurrentPassword;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getIsPasswordRotation() {
		return $this->m_boolIsPasswordRotation;
	}

	public function getIsEditTooltipAllowed() {
		return $this->m_boolIsEditTooltipAllowed;
	}

	public function getSecondPassword() {
		return $this->m_strSecondPassword;
	}

	public function getGoogleAuthenticatorSecretKey() {
		if( false == $this->getDatabase()->getIsOpen() ) {
			$this->getDatabase()->validateAndOpen();
		}

		$objCompanyUserPreference  = $this->fetchCompanyUserPreferenceByKey( 'GOOGLE_AUTHENTICATOR_KEY', $this->getDatabase() );

		if( true == valObj( $objCompanyUserPreference, 'CCompanyUserPreference' ) && true == valStr( $objCompanyUserPreference->getValue() ) ) {
			$strGoogleAuthenticatorKey = $objCompanyUserPreference->getValue();
		} else {
			$strGoogleAuthenticatorKey = $this->generateGoogleAuthenticatorSecretKey();
		}
		return $strGoogleAuthenticatorKey;
	}

	public function getBluemoonPasswordEncrypted() {
		return $this->m_strBluemoonPasswordEncrypted;
	}

	public function getIsRequireTwoFactorAuthentication() {
		return $this->m_boolIsRequireTwoFactorAuthentication;
	}

	public function getIsTwoFactorAuthenticationValidated() {
		return $this->m_boolIsTwoFactorAuthenticationValidated;
	}

	public function getEncryptedXmppPassword() {
		if( false == valId( $this->getId() ) ) {
			return NULL;
		}
		return md5( $this->getId() );
	}

	public function getXmppUsername() {
		return 'entrata_' . ( int ) $this->getCid() . '_' . ( int ) $this->getId();
	}

	public function getCompanyGroupName() {
		return $this->m_strCompanyGroupName;
	}

	public function getPreferredLocaleCode() {
		return $this->m_strPreferredLocaleCode;
	}

	public function getIsPreviewMode() {
		return $this->m_boolIsPreviewMode;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public static function getCompanyUsersFields() {

		return [
			'username'                      => __( 'Username' ),
			'is_administrator'              => __( 'Administrator' ),
			'is_active_directory_user'      => __( 'Active Directory User' ),
			'password'                      => __( 'Password' ),
			'employee_portal_only'          => __( 'Access EmployeePortal Only' ),
			'block_admin_login'             => __( 'Block Admin Login' ),
			'is_general_task_stakeholder'   => __( 'General Task Stakeholder' ),
			'is_disabled'                   => __( 'is_disabled' )

		];

	}

	public function getRequiredParameters() {
		return $this->m_arrmixRequiredParameters;
	}

	public function getIsSamlRestrictEntrataLogin() {
		return $this->m_boolIsSamlRestrictEntrataLogin;
	}

	public function getSsoToken() {
		return $this->m_strSsoToken;
	}

	public function getIsScimUser() {
		return $this->m_boolIsScimUser;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setClient( $objClient ) {
		if( false == valObj( $objClient, 'CClient' ) ) {
			trigger_error( 'Incompatible types: \'CClient\' and \'' . get_class( $objClient ) . '\' - CCompanyCharge::setClient()', E_USER_ERROR );

			return false;
		}

		$this->m_objClient = $objClient;

		return true;
	}

	public function setCid( $intCid ) {
		parent::setCid( $intCid );
		// If we are setting the client id directly we unbind the
		// client object to avoid any concurrency issues
		$this->m_objClient = NULL;
	}

	public function setConfirmPassword( $strConfirmPassword ) {
		$this->m_strConfirmPassword = CStrings::strTrimDef( $strConfirmPassword, 240, NULL, true );
	}

	public function setLoginAttemptData( $boolIsLoggedIn = false ) {

		if( true == $boolIsLoggedIn ) {
			$this->setLoginAttemptCount( 0 );

		} elseif( time() > ( strtotime( date( 'c', strtotime( $this->getLastLoginAttemptOn() ) ) ) + self::LOGIN_ATTEMPT_WAITING_TIME ) && 0 < $this->getLoginAttemptCount() ) {

			$this->setLoginAttemptCount( 1 );

		} else {
			$this->setLoginAttemptCount( $this->getLoginAttemptCount() + 1 );
		}

		$this->setLastLoginAttemptOn( date( 'c', time() ) );
	}

	public function setIsRequireTwoFactorAuthentication( $objDatabase ) {
		$this->m_boolIsRequireTwoFactorAuthentication = \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPerferenceByCompanyPrefenceKeyByCompanyUserIdByKeysByCid( 'ENABLED_TWO_FACTOR_AUTHENTICATION', $this->getId(), [ 'ENABLED_TWO_FACTOR_USER_AUTHENTICATION', 'ENABLED_TWO_FACTOR_EMAIL_AUTHENTICATION', 'ENABLED_TWO_FACTOR_PHONE_NUMBER_AUTHENTICATION' ], $this->getCid(), $objDatabase );
	}

	public function setIsTwoFactorAuthenticationValidated( $boolIsTwoFactorAuthenticationValidated ) {
		$this->m_boolIsTwoFactorAuthenticationValidated = $boolIsTwoFactorAuthenticationValidated;
	}

	public function setRequiredParameters( $arrmixParameters ) {
		$this->m_arrmixRequiredParameters = $arrmixParameters;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchCompanyUserProperties( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchCompanyUserPropertiesByCidByCompanyUserId( $this->getCid(), $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserPropertyGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchCompanyUserPropertyGroupsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchInheritedCompanyUserPropertyGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchInheritedCompanyUserPropertyGroupsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchEnabledCompanyUserPropertyGroups( $objDatabase, $boolShowDisabledData = false ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchEnabledCompanyUserPropertyGroupsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolShowDisabledData );
	}

	public function fetchSimplePropertyGroupIds( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchSimplePropertyGroupIdsByCidByCompanyUserId( $this->getCid(), $this->getId(), $objDatabase );
	}

	// This function is getting used all over Entrata - NRW

	public function fetchProperties( $objDatabase, $boolReturnArray = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCidByCompanyUserId( $this->getCid(), $this->getId(), $objDatabase, $this->getIsAdministrator(), $boolReturnArray );
	}

	public function fetchPropertiesWithTypesWithRegions( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCompanyUserIdWithTypeWithRegionByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserPermissions( $objDatabase, $boolAllowedOnly = NULL ) {
		return \Psi\Eos\Entrata\CCompanyUserPermissions::createService()->fetchCustomCompanyUserPermissionsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolAllowedOnly );
	}

	public function fetchGlobalUser( $objConnectDatabase ) {
		return CGlobalUsers::fetchGlobalUserByCidByWorksIdByGlobalUserTypeId( $this->getCid(), $this->getId(), $this->getCompanyUserTypeId(), $objConnectDatabase );
	}

	public function fetchCompanyEmployee( $objDatabase ) {
		return CCompanyEmployees::createService()->fetchSimpleCompanyEmployeeByIdByCid( $this->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
	}

	public function fetchOpenArPaymentTransmissions( $objDatabase ) {
		return CArPaymentTransmissions::fetchOpenArPaymentTransmissionsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchUnprocessedConfirmedArPaymentTransmissions( $objDatabase ) {
		$arrobjProperties = $this->fetchProperties( $objDatabase );

		if( true == valArr( $arrobjProperties ) ) {

			$arrintPropertyIds = array_keys( $arrobjProperties );

			return CArPaymentTransmissions::fetchUnprocessedConfirmedArPaymentTransmissionsByPropertyIdsByCid( $arrintPropertyIds, $this->m_intCid, $objDatabase );
		} else {
			return NULL;
		}
	}

	public function fetchWebsites( $objDatabase ) {

		if( 1 == $this->m_intIsAdministrator || true == $this->m_boolIsPSIUser ) {
			return CWebsites::createService()->fetchCustomWebsitesByCid( $this->m_intCid, $objDatabase );
		} else {
			return CWebsites::createService()->fetchWebsitesByCompanyUserIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
		}
	}

	public function fetchWebsiteIds( $objDatabase ) {

		if( 1 == $this->m_intIsAdministrator || true == $this->m_boolIsPSIUser ) {
			return CWebsites::createService()->fetchWebsiteIdsByCid( $this->m_intCid, $objDatabase );
		} else {
			return CWebsites::createService()->fetchWebsiteIdsByCompanyUserIdByCid( $this->m_intId, $this->m_intCid, $objDatabase );
		}
	}

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserGroups( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserGroupsByCompanyUserIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserGroupsCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserGroupsCountByCompanyUserIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserPermissionsByModuleIds( $arrintModuleIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPermissions::createService()->fetchCompanyUserPermissionsByCompanyUserIdByModuleIdsByCid( $this->m_intId, $arrintModuleIds, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyGroupPreferencesByKeys( $arrstrKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroupPreferences::createService()->fetchCompanyGroupPreferencesByCompanyGroupIdByKeysByCid( $this->getId(), $arrstrKeys, $this->getCid(), $objDatabase, $strOrderBy = 'id' );
	}

	public function fetchCompanyUserPreferences( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchOrderedUserPreferencesByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase, $strOrderBy = 'id' );
	}

	public function fetchCompanyUserPreferencesByKeys( $arrstrKeys, $objDatabase, $boolIsCheckValue = true ) {
		return \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferencesByCompanyUserIdByKeysByCid( $this->getId(), $arrstrKeys, $this->getCid(), $objDatabase, $boolIsCheckValue );
	}

	public function fetchPermissionedAccountsByAccountTypeId( $arrintAccountTypeIds, $objAdminDatabase, $objDatabase ) {
		$arrobjAccounts = ( array ) CAccounts::createService()->fetchAccountsByCidByAccountTypeIds( $this->getCid(), $arrintAccountTypeIds, $objAdminDatabase, false );

		if( false == $this->getIsAdministrator() ) {
			$arrintEnabledPropertyIds = $this->fetchSimpleEnabledPropertyIds( $objDatabase );

			$arrobjAccountProperties = ( array ) \Psi\Eos\Admin\CAccountProperties::createService()->fetchAccountPropertiesByAccountTypeIdsByPropertyIdsByCid( $arrintAccountTypeIds, $arrintEnabledPropertyIds, $this->getCid(), $objAdminDatabase );
			$arrmixAccountProperties = rekeyObjects( 'AccountId', $arrobjAccountProperties, true );

			$arrobjPropertyMerchantAccounts = ( array ) CMerchantAccounts::fetchMerchantAccountsByPropertyIdByCid( $this->getCid(), $objDatabase, NULL, $arrintEnabledPropertyIds );
			$arrmixPropertyMerchantAccounts = rekeyArray( 'billing_account_id', $arrobjPropertyMerchantAccounts );

			$arrobjAccounts = array_intersect_key( $arrobjAccounts, $arrmixAccountProperties + $arrmixPropertyMerchantAccounts );
		}

		return $arrobjAccounts;
	}

	public function fetchAccountsByAccountTypeId( $intAccountTypeId, $objDatabase ) {

		if( true == $this->getIsAdministrator() ) {
			return CAccounts::createService()->fetchActiveAccountsByCidByPaymentTypeIdByAccountTypeId( $this->getCid(), CPaymentType::ACH, $intAccountTypeId, $objDatabase );
		} else {
			return CAccounts::createService()->fetchActiveAccountsByCidByPaymentTypeIdByAccountTypeIdByPropertyIds( $this->getCid(), CPaymentType::ACH, $intAccountTypeId, $this->getCompanyUserPropertyIds(), $objDatabase );
		}
	}

	public function fetchPropertyLeasingAgents( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentsByEmployeeIdByCid( $this->getCompanyEmployeeId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserDetail( $objDatabase ) {
		return CCompanyUserDetails::fetchCompanyUserDetailByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserPreferenceByKey( $strCompanyUserPreferenceKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferenceByCompanyUserIdByKeyByCid( $this->getId(), $strCompanyUserPreferenceKey, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserPreferenceCountByKey( $strCompanyUserPreferenceKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchCompanyUserPreferenceCountByCompanyUserIdByKeyByCid( $this->getId(), $strCompanyUserPreferenceKey, $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedCompanyUserAuthenticationLogs( $intPageNo, $intCountPerPage, $boolIsSiteTablet, $objDatabase ) {
		return CAuthenticationLogs::fetchPaginatedCompanyUserAuthenticationLogsByCompanyUserIdByCid( $intPageNo, $intCountPerPage, $this->getId(), $this->getCid(), $boolIsSiteTablet, $objDatabase );
	}

	public function fetchCompanyUserAuthenticationLogsCount( $boolIsSiteTablet, $objDatabase ) {
		return CAuthenticationLogs::fetchCompanyUserAuthenticationLogsCountByCompanyUserIdByCid( $this->getId(), $this->getCid(), $boolIsSiteTablet, $objDatabase );
	}

	public function fetchChats( $objDatabase ) {
		return \Psi\Eos\Chats\CChats::createService()->fetchCurrentDayChatsByCompanyUserId( $this->getId(), $objDatabase );
	}

	public static function fetchChatById( $intChatId, $objDatabase ) {
		return \Psi\Eos\Chats\CChats::createService()->fetchChatById( $intChatId, $objDatabase );
	}

	public function fetchPermissionedModuleIds( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchPermissionedModuleIds( $this, $objDatabase );
	}

	public function fetchPsProductIds( $objDatabase ) {
		return CPropertyProducts::createService()->fetchPsProductIdsByCompanyUser( $this, $objDatabase );
	}

	public function fetchPsProductDetailsByPsProductIds( $arrintPsProductIds, $objDatabase, $boolFetchPsProductOptionProperties = false, $boolShowDisabledData = false ) {
		return CPropertyProducts::createService()->fetchPsProductDetailsByCompanyUserByPsProductIds( $this, $arrintPsProductIds, $objDatabase, $boolFetchPsProductOptionProperties, $boolShowDisabledData );
	}

	public function fetchSimplePropertiesWithWebsitePermissions( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesByCompanyUserIdWithWebsitePermissionsByCid( $this->getId(), $this->getCid(), $objDatabase, $this->getIsAdministrator() );
	}

	public function fetchCompanyGroupPermissions( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroupPermissions::createService()->fetchCompanyGroupPermissionsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPreviousAuthenticationLog( $objDatabase ) {
		return CAuthenticationLogs::fetchPreviousAuthenticationLogByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAllProperties( $objDatabase, $boolShowEnabled = NULL, $boolIsFeatured = NULL, $boolIsExcludeTemplateProperties = NULL ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchAllPropertiesByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase, $boolShowEnabled, $boolIsFeatured, $this->getIsAdministrator(), $boolIsExcludeTemplateProperties );
	}

	public function fetchAllServicesWithCompanyUserServiceId( $objSelectedUser, $objDatabase, $boolIsEnabledReportApi = true ) {
		return \Psi\Eos\Entrata\CServices::createService()->fetchAllServicesWithCompanyUserServiceIdByCompanyUserIdByCid( $objSelectedUser->getId(), $this->getCid(), $objDatabase, $boolIsEnabledReportApi );
	}

	public function fetchCompanyUserServiceByServiceId( $intServiceId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserServices::createService()->fetchCompanyUserServiceByServiceIdByCompanyUserIdByCid( $intServiceId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserPreferencesByUserPreferenceKeys( $arrstrUserPreferenceKeys, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->fetchUserPreferencesByCompanyUserIdByUserPreferenceKeysByCid( $this->getId(), $arrstrUserPreferenceKeys, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyUserPermissionByModuleId( $intModuleId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPermissions::createService()->fetchCompanyUserPermissionByCompanyUserIdByModuleIdByCid( $this->getId(), $intModuleId, $this->getCid(), $objDatabase );
	}

	public function fetchMaxCompanyGroupPreferencesByUserPreferenceKeysByModuleId( $arrstrUserPreferenceKeys, $intModuleId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyGroupPreferences::createService()->fetchMaxCompanyGroupPreferencesByCompanyUserIdByKeysByModuleIdByCid( $this->getId(), $arrstrUserPreferenceKeys, $intModuleId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertiesByPropertyIds( $arrintPropertyIds, $objDatabase, $boolShowEnabled = NULL, $boolIsFeatured = NULL, $boolSortParentPropertyFirst = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCompanyUserIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase, $boolShowEnabled, $boolIsFeatured, $boolSortParentPropertyFirst );
	}

	public function fetchPropertiesByIdsByFieldNames( $arrintPropertyIds, $arrstrFieldNames, $objDatabase, $boolReturnArray = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByIdsByFieldNamesByCompanyUserIdByCid( $arrintPropertyIds, $arrstrFieldNames, $this->getId(), $this->getCid(), $objDatabase, $boolReturnArray );
	}

	public function fetchPropertyByPropertyId( $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByCompanyUserIdByPropertyIdByCid( $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchServicesByServiceTypeIdByGroupName( $intServiceTypeId, $strServiceGroupName, $objDatabase ) {
		return \Psi\Eos\Entrata\CServices::createService()->fetchServicesByCompanyUserIdByServiceTypeIdByServiceGroupNameByCid( $this->getId(), $intServiceTypeId, $strServiceGroupName, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeasingAgentsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeasingAgents::createService()->fetchPropertyLeasingAgentsByCompanyUserIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyLeadSourcesByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchLeadSourcesByCompanyUserByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchMerchantAccountsByPropertyId( $intPropertyId, $objDatabase ) {
		return CMerchantAccounts::fetchMerchantAccountsByCidByCompanyUserIdByPropertyId( $this->getCid(), $this->getId(), $intPropertyId, $objDatabase );
	}

	public function fetchPropertyAppsByAppId( $intAppId, $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyApps::createService()->fetchPropertyAppsByCompanyUserIdByAppIdByCid( $this->getId(), $intAppId, $this->getCid(), $objDatabase );
	}

	public function getOrFetchCompanyApps( $objDatabase ) {
		if( true == empty( $this->m_arrobjCompanyApps ) ) {
			$this->m_arrobjCompanyApps = CCompanyApps::fetchCompanyAppsByCompanyUserIdByIsAdministratorByCid( $this->getId(), $this->getIsAdministrator(), $this->getCid(), $objDatabase );
		}

		return $this->m_arrobjCompanyApps;
	}

	public function fetchMerchantAccountsByCid( $intCid, $objDatabase, $intPropertyId = NULL ) {
		return CMerchantAccounts::fetchMerchantAccountsByCompanyUserIdByCid( $this->getId(), $intCid, $objDatabase, $intPropertyId );
	}

	public function fetchPropertiesByMerchantAccountId( $intMerchantAccountId, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertiesByCompanyUserIdByMerchantAccountIdByCid( $this->getId(), $intMerchantAccountId, $this->getCid(), $objDatabase );
	}

	public function fetchPermissionedReportCategories( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchPermissionedReportCategories( $this, $objDatabase );
	}

	public function fetchPaginatedCompanyUserChangeLogs( $intPageNo, $intCountPerPage, $objDatabase ) {
		return CTableLogs::fetchPaginatedCompanyUserChangeLogsByReferenceNumberByCid( $intPageNo, $intCountPerPage, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPaginatedCompanyUserChangeLogsCount( $objDatabase ) {
		return CTableLogs::fetchPaginatedCompanyUserChangeLogsCountByReferenceNumberByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPMSoftwareEnabledPropertiesCount( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPMSoftwareEnabledPropertiesCountByCompanyUserIdByCid( $this->getId(), $this->getIsAdministrator(), $this->getCid(), $objDatabase );
	}

	public function fetchAllowedMaintenanceRequestById( $intMaintenanceRequestId, $objDatabase, $boolShowDisabledData = false ) {
		return \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchCustomAllowedMaintenanceRequestByIdByCompanyUserIdByCid( $this->getId(), $intMaintenanceRequestId, $this->getCid(), $this->getIsAdministrator(), $objDatabase, false, $boolShowDisabledData );
	}

	public function fetchAllowedInspectionById( $intInspectionId, $objDatabase, $boolShowDisabledData = false ) {
		return \Psi\Eos\Entrata\CInspections::createService()->fetchCustomAllowedInspectionByIdByCompanyUserIdByCid( $intInspectionId, $this->getId(), $this->getCid(), $objDatabase, $boolShowDisabledData );
	}

	// code cleanup

	public function fetchInspectionFilterById( $intInspectionSearchId, $objDatabase ) {
		return \Psi\Eos\Entrata\CInspectionFilters::createService()->fetchInspectionFilterByCompanyUserIdByIdByCid( $this->getId(), $intInspectionSearchId, $this->getCid(), $objDatabase );
	}

	public function fetchPerson( $objDatabase ) {
		return \Psi\Eos\Admin\CPersons::createService()->fetchPersonByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valUsername( $objDatabase, $objConnectDatabase ) {

		$boolIsValid = true;
		$strUsername = preg_replace( '/@' . $this->m_strRwxDomain . '$/', '', $this->m_strUsername );

		if( false == isset( $strUsername ) || ( 3 > strlen( $strUsername ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Username must be at least 3 characters.' ) ) );

			return $boolIsValid;
		}

		if( false !== \Psi\CStringService::singleton()->strpos( $strUsername, '@' ) ) {
			if( false == CValidation::validateEmailAddresses( $strUsername ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Username should be a valid username without \'@\' OR should be a valid email address.' ) ) );

				return $boolIsValid;
			}
		}

		if( true == valObj( $objConnectDatabase, 'CDatabase' ) ) {

			$intCompetingGlobalUsersCount = CGlobalUsers::fetchCompetingGlobalUsersCountByUsernameByNonCompetingCompanyUserIdByGlobalUserTypeId( $this->m_strUsername, $this->m_intId, $this->getCompanyUserTypeId(), $objConnectDatabase );

			if( 0 < $intCompetingGlobalUsersCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Username already in use.' ) ) );

				return $boolIsValid;
			}
		}

		if( true == isset ( $objDatabase ) ) {

			$intCount = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompetingCompanyUserCountByUsernameByNonCompetingCompanyUserId( $this->m_strUsername, $this->m_intId, $this->m_intCid, $objDatabase, $this->getCompanyUserTypeId() );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Username already in use.' ) ) );

				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valUsernameAsEmail( $objDatabase, $objConnectDatabase ) {

		$boolIsValid = true;

		if( false == isset( $this->m_strUsername ) || 0 == strlen( trim( $this->m_strUsername ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address is required.' ) ) );

			return $boolIsValid;

		} else {
			if( false == CValidation::validateEmailAddresses( $this->m_strUsername ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email does not appear to be valid.' ) ) );

				return $boolIsValid;
			}
		}

		if( true == isset ( $objConnectDatabase ) ) {

			if( false == is_null( $this->m_intId ) ) {
				$objGlobalUser = $this->fetchGlobalUser( $objConnectDatabase );

				if( true == valObj( $objGlobalUser, 'CGlobalUser' ) ) {
					$intGlobalUserId = $objGlobalUser->getId();
				}
			}

			$intGlobalUserId = ( true == isset( $intGlobalUserId ) ) ? $intGlobalUserId : '';

			$intCompetingGlobalUserCount = CGlobalUsers::fetchGlobalUsersCountByUsernameByNonCompetingGlobalUserIdByGlobalUserTypeId( \Psi\CStringService::singleton()->strtolower( $this->m_strUsername ), $intGlobalUserId, $this->getCompanyUserTypeId(), $objConnectDatabase );
			if( 0 < $intCompetingGlobalUserCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'A user with that email address already exists.  Please enter another email address.' ) ) );

				return $boolIsValid;
			}
		}

		if( true == isset ( $objDatabase ) ) {

			$intCount = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompetingCompanyUserCountByUsernameByNonCompetingCompanyUserId( \Psi\CStringService::singleton()->strtolower( $this->m_strUsername ), $this->m_intId, $this->m_intCid, $objDatabase, $this->getCompanyUserTypeId() );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'A user with that email address already exists.  Please enter another email address.' ) ) );

				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valPassword( $arrobjPasswordPolicyCompanyPreferences = [] ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strPassword ) || 0 == strlen( $this->m_strPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password is required.' ) ) );
		} else {
			if( true == valArr( $arrobjPasswordPolicyCompanyPreferences ) && true == array_key_exists( 'PASSWORD_REQUIREMENTS', $arrobjPasswordPolicyCompanyPreferences ) && 'custom' == $arrobjPasswordPolicyCompanyPreferences['PASSWORD_REQUIREMENTS']->getValue() ) {

				$arrstrCriteriaMessage = [];

				if( $arrobjPasswordPolicyCompanyPreferences['MINIMUM_PASSWORD_LENGTH']->getValue() > strlen( $this->m_strPassword ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password cannot be less than {%d, password_min_length} characters.', [ 'password_min_length' => $arrobjPasswordPolicyCompanyPreferences['MINIMUM_PASSWORD_LENGTH']->getValue() ] ) ) );
				}

				if( true == array_key_exists( 'REQUIRE_AT_LEAST_ONE_NUMBER', $arrobjPasswordPolicyCompanyPreferences ) && '1' == $arrobjPasswordPolicyCompanyPreferences['REQUIRE_AT_LEAST_ONE_NUMBER']->getValue() ) {
					$boolIsValid &= ( false == preg_match( '/^(?=.*\d)\S*$/', $this->m_strPassword ) ) ? false : true;
					$arrstrCriteriaMessage[] = __( '1 number' );
				}

				if( true == array_key_exists( 'REQUIRE_AT_LEAST_ONE_LOWERCASE_LETTER', $arrobjPasswordPolicyCompanyPreferences ) && '1' == $arrobjPasswordPolicyCompanyPreferences['REQUIRE_AT_LEAST_ONE_LOWERCASE_LETTER']->getValue() ) {
					$boolIsValid &= ( false == preg_match( '/^(?=.*[a-z])\S*$/', $this->m_strPassword ) ) ? false : true;
					$arrstrCriteriaMessage[] = __( '1 lowercase letter' );
				}

				if( true == array_key_exists( 'REQUIRE_AT_LEAST_ONE_UPPERCASE_LETTER', $arrobjPasswordPolicyCompanyPreferences ) && '1' == $arrobjPasswordPolicyCompanyPreferences['REQUIRE_AT_LEAST_ONE_UPPERCASE_LETTER']->getValue() ) {
					$boolIsValid &= ( false == preg_match( '/^(?=.*[A-Z])\S*$/', $this->m_strPassword ) ) ? false : true;
					$arrstrCriteriaMessage[] = __( '1 uppercase letter' );
				}

				if( true == array_key_exists( 'REQUIRE_AT_LEAST_ONE_NON_ALPHANUMERIC_CHARACTER', $arrobjPasswordPolicyCompanyPreferences ) && '1' == $arrobjPasswordPolicyCompanyPreferences['REQUIRE_AT_LEAST_ONE_NON_ALPHANUMERIC_CHARACTER']->getValue() ) {
					if( true == array_key_exists( 'NON_ALPHANUMERIC_CHARACTERS', $arrobjPasswordPolicyCompanyPreferences ) && false == is_null( $arrobjPasswordPolicyCompanyPreferences['NON_ALPHANUMERIC_CHARACTERS']->getValue() ) ) {
						$strPattern = '/[' . preg_quote( $arrobjPasswordPolicyCompanyPreferences['NON_ALPHANUMERIC_CHARACTERS']->getValue(), '/' ) . ']/';
						$boolIsValid &= ( false == preg_match( $strPattern, $this->m_strPassword ) ) ? false : true;
						$arrstrCriteriaMessage[] = __( '1 special character from {%s, non_alphanumeric_character}', [ 'non_alphanumeric_character' => $arrobjPasswordPolicyCompanyPreferences['NON_ALPHANUMERIC_CHARACTERS']->getValue() ] );
					} else {
						$boolIsValid &= ( false == preg_match( '/\W/', $this->m_strPassword ) ) ? false : true;
						$arrstrCriteriaMessage[] = __( '1 special character' );
					}
				}

				if( false == $boolIsValid && true == valArr( $arrstrCriteriaMessage ) ) {
					$strErrorMsg = implode( ', ', $arrstrCriteriaMessage );
					if( 1 < \Psi\Libraries\UtilFunctions\count( $arrstrCriteriaMessage ) ) {
						$strErrorMsg = \Psi\CStringService::singleton()->substr( $strErrorMsg, 0, \Psi\CStringService::singleton()->strrpos( $strErrorMsg, ',' ) ) . ' And ' . \Psi\CStringService::singleton()->substr( $strErrorMsg, \Psi\CStringService::singleton()->strrpos( $strErrorMsg, ',' ) + 1 );
					}

					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password must contain at least {%s,0}', [ $strErrorMsg ] ) ) );
				}
			} else {

				if( true == valStr( $this->m_strPassword ) && 8 > strlen( $this->m_strPassword ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password cannot be less than 8 characters.' ) ) );

				} else {
					if( true == valStr( $this->m_strPassword ) && false == preg_match( '/(?=^.{8,}$)(?=.*[\d|!@#$%^&*]+)(?=.*[A-Z]).*$/', $this->m_strPassword ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password must contain at least 1 capital letter AND 1 number or special character.' ) ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valCurrentPassword( $objDatabase = NULL, $boolIsResidentWorksLogin = true ) {

		$boolIsValid = true;

		if( true == empty( $this->getCurrentPassword() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_password', __( 'Current password is required.' ) ) );
		} else {
			$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByUsernameByRwxDomainByCid( $this->m_strUsername, $this->m_strRwxDomain, $this->m_intCid, $objDatabase, $boolIsResidentWorksLogin );
			if( true == valObj( $objCompanyUser, CCompanyUser::class ) && false == ( \Psi\Libraries\Cryptography\CCrypto::create() )->verifyHash( $this->getCurrentPassword(), $objCompanyUser->getPasswordEncrypted() ) ) {
				$objCompanyUser = NULL;
			}

			if( false == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_password', __( 'Current password is not correct.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valConfirmPassword() {
		$boolIsValid = true;

		if( true == isset( $this->m_strPassword ) && false == isset( $this->m_strConfirmPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirm_password', __( 'Confirmation password is required.' ) ) );

			return $boolIsValid;
		}

		if( 0 != strcmp( $this->m_strPassword, $this->m_strConfirmPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Passwords do not match.' ) ) );
		}

		return $boolIsValid;
	}

	public function valResetPasswordEmailAddress() {

		$boolIsValid = true;

		if( false == isset( $this->m_strEmailAddress ) || 0 == strlen( trim( $this->m_strEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email address is required.' ) ) );

			return $boolIsValid;

		} else {
			if( false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Email does not appear to be valid.' ) ) );

				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valPasswordExistence( $objDatabase, $arrobjPasswordPolicyCompanyPreferences = [] ) {
		$boolIsValid                               = true;
		$boolIsPasswordExistenceValidationRequired = true;

		if( true == valArr( $arrobjPasswordPolicyCompanyPreferences ) && ( true == array_key_exists( 'PASSWORD_REQUIREMENTS', $arrobjPasswordPolicyCompanyPreferences ) && 'custom' == $arrobjPasswordPolicyCompanyPreferences['PASSWORD_REQUIREMENTS']->getValue() ) && ( false == array_key_exists( 'PREVENT_PASSWORD_REUSE_NUMBER', $arrobjPasswordPolicyCompanyPreferences ) || ( true == array_key_exists( 'PREVENT_PASSWORD_REUSE_NUMBER', $arrobjPasswordPolicyCompanyPreferences ) && true == is_null( $arrobjPasswordPolicyCompanyPreferences['PREVENT_PASSWORD_REUSE_NUMBER']->getValue() ) ) ) ) {
			$boolIsPasswordExistenceValidationRequired = false;
		}

		if( true == $boolIsPasswordExistenceValidationRequired ) {
			if( true == $this->checkPasswordsInAuthenticationLogs( $objDatabase, $arrobjPasswordPolicyCompanyPreferences ) ) {
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $objConnectDatabase = NULL, $boolIsValidatePassword = false ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

		$arrobjPasswordPolicyCompanyPreferences = ( false == is_null( $objDatabase ) ) ? rekeyObjects( 'key', CCompanyPreferences::createService()->fetchCompanyPreferencesByKeysByCid( [ 'PASSWORD_REQUIREMENTS', 'MINIMUM_PASSWORD_LENGTH', 'REQUIRE_AT_LEAST_ONE_UPPERCASE_LETTER', 'REQUIRE_AT_LEAST_ONE_LOWERCASE_LETTER', 'REQUIRE_AT_LEAST_ONE_NUMBER', 'REQUIRE_AT_LEAST_ONE_NON_ALPHANUMERIC_CHARACTER', 'NON_ALPHANUMERIC_CHARACTERS', 'PREVENT_PASSWORD_REUSE_NUMBER' ], $this->m_intCid, $objDatabase ) ) : [];

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valUsername( $objDatabase, $objConnectDatabase );

				if( true == $boolIsValidatePassword ) {
					$boolIsValid &= $this->valPassword( $arrobjPasswordPolicyCompanyPreferences );
					if( true == $boolIsValid ) {
						$boolIsValid &= $this->valConfirmPassword();
					}
				}
				break;

			case 'update_profile':
				$boolIsValid &= $this->valCurrentPassword( $objDatabase );

				if( true == $boolIsValidatePassword ) {
					$boolIsValid &= $this->valPassword( $arrobjPasswordPolicyCompanyPreferences );
					if( true == $boolIsValid ) {
						$boolIsValid &= $this->valConfirmPassword();
					}
				}
				break;

			case VALIDATE_UPDATE:
				if( true == $boolIsValidatePassword ) {
					$boolIsValid &= $this->valPassword( $arrobjPasswordPolicyCompanyPreferences );
					if( true == $boolIsValid ) {
						$boolIsValid &= $this->valConfirmPassword();
					}
				}
				$boolIsValid &= $this->valUsername( $objDatabase, $objConnectDatabase );
				break;

			case 'update_password':
				if( true == $boolIsValidatePassword ) {
					$boolIsValid &= $this->valPassword( $arrobjPasswordPolicyCompanyPreferences );
					if( true == $boolIsValid ) {
						$boolIsValid &= $this->valConfirmPassword();
					}
				}
				break;

			case 'password_existence':
				$boolIsValid &= $this->valPasswordExistence( $objDatabase, $arrobjPasswordPolicyCompanyPreferences );
				break;

			case 'update_user':
			case 'validate_username':
				$boolIsValid &= $this->valUsername( $objDatabase, $objConnectDatabase );
				break;

			case 'validate_insert_with_email':
				$boolIsValid &= $this->valUsernameAsEmail( $objDatabase, $objConnectDatabase );

				if( true == $boolIsValidatePassword ) {
					$boolIsValid &= $this->valPassword( $arrobjPasswordPolicyCompanyPreferences );
					if( true == $boolIsValid ) {
						$boolIsValid &= $this->valConfirmPassword();
					}
				}
				break;

			case 'validate_update_with_email':
				if( true == $boolIsValidatePassword ) {
					$boolIsValid &= $this->valPassword( $arrobjPasswordPolicyCompanyPreferences );
					if( true == $boolIsValid ) {
						$boolIsValid &= $this->valConfirmPassword();
					}
				}
				$boolIsValid &= $this->valUsernameAsEmail( $objDatabase, $objConnectDatabase );
				break;

			case 'validate_reset_password':
				$boolIsValid &= $this->valResetPasswordEmailAddress();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function createPropertyGroupChangeLog( $arrobjCompanyUserPropertyGroups, $arrstrNewCompanyUserPropertyGroupsName, $objDatabase ) {

		$strNewData  = '';
		$arrobjCompanyUsersPropertyGroups = rekeyObjects( 'IsInherited', $arrobjCompanyUserPropertyGroups, true );
		if( true == valArr( $arrstrNewCompanyUserPropertyGroupsName ) ) {
			$strNewData  = ' Assigned: ' . implode( ', ', $arrstrNewCompanyUserPropertyGroupsName );
		}
		if( true == isset( $arrobjCompanyUsersPropertyGroups[0] ) ) {
			$arrintPropertyGroupIds = array_keys( rekeyObjects( 'PropertyGroupId', $arrobjCompanyUsersPropertyGroups[0] ) );
			$arrintRemovedCompanyUserPropertyGroup      = ( array ) \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroupNamesByIdsByCid( $arrintPropertyGroupIds, $this->getCid(), $objDatabase );
			$arrintRemovedCompanyUserPropertyGroupNames = array_column( $arrintRemovedCompanyUserPropertyGroup, 'name' );
			if( true == valArr( $arrintRemovedCompanyUserPropertyGroupNames ) ) {
				if( true == valStr( $strNewData ) ) {
					$strNewData  .= ' || ';
				}
				$strNewData  .= 'Removed: ' . implode( ', ', $arrintRemovedCompanyUserPropertyGroupNames );
			}
		}
		if( true == valStr( $strNewData ) ) {
			$strNewData                                 = 'company_user_property_groups::' . $strNewData;
			$strOldData                                 = 'company_user_property_groups::';
			$strUserPropertyGroupsAssignmentDescription = 'A company user property groups assignment has been modified.';
			$objTableLog                                = new CTableLog();
			if( false == $objTableLog->insert( $this->getId(), $objDatabase, 'company_user_property_groups', $this->getId(), 'UPDATE', $this->getCid(), $strOldData, $strNewData, $strUserPropertyGroupsAssignmentDescription ) ) {
				return false;
			}
		}
		return true;
	}

	public function importIntoPersons( $intUserId, $objPsLead, $objClientDatabase, $objAdminDatabase ) {

		$objCompanyEmployee = $this->fetchCompanyEmployee( $objClientDatabase );

		if( false == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) || ( true == is_null( $objCompanyEmployee->getNameFirst() ) && true == is_null( $objCompanyEmployee->getNameLast() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'You cannot migrate a company user to the lead system unless the user has employee details entered (first name, last name, etc).' ) ) );

			return false;
		}

		$objCompanyEmployeeAddress = $objCompanyEmployee->fetchCompanyEmployeeAddressByAddressTypeId( CAddressType::PRIMARY, $objClientDatabase );
		$strPrimaryPhoneNumber     = $objCompanyEmployee->getPhoneNumberByPhoneNumberTypeId( CPhoneNumberType::PRIMARY );
		$strMobilePhoneNumber      = $objCompanyEmployee->getPhoneNumberByPhoneNumberTypeId( CPhoneNumberType::MOBILE );
		$objCompanyDepartment      = $objCompanyEmployee->fetchCompanyDepartment( $objClientDatabase );

		$objPerson = new CPerson();

		$objPerson->setNameFirst( $objCompanyEmployee->getNameFirst() );
		$objPerson->setNameLast( $objCompanyEmployee->getNameLast() );
		$objPerson->setEmailAddress( $objCompanyEmployee->getEmailAddress() );
		$objPerson->setPsLeadId( $objPsLead->getId() );
		$objPerson->setCompanyUserId( $this->getId() );

		$objPerson->setCompanyName( $objPsLead->getCompanyName() );
		$objPerson->setCid( $objPsLead->getCid() );

		if( false == is_null( $strMobilePhoneNumber ) ) {

			$objPerson->setPhoneNumber2( $strMobilePhoneNumber );
			$objPerson->setPhoneNumber2TypeId( CPhoneNumberType::MOBILE );
		}

		if( false == is_null( $strPrimaryPhoneNumber ) ) {

			$objPerson->setPhoneNumber1( $strPrimaryPhoneNumber );
			$objPerson->setPhoneNumber1TypeId( CPhoneNumberType::OFFICE );
		}

		if( true == valObj( $objCompanyEmployeeAddress, 'CCompanyEmployeeAddress' ) ) {
			$objPerson->setAddressLine1( $objCompanyEmployeeAddress->getStreetLine1() );
			$objPerson->setAddressLine2( $objCompanyEmployeeAddress->getStreetLine2() );
			$objPerson->setCity( $objCompanyEmployeeAddress->getCity() );
			$objPerson->setStateCode( $objCompanyEmployeeAddress->getStateCode() );
			$objPerson->setPostalCode( $objCompanyEmployeeAddress->getPostalCode() );
		}

		// Set PS Person Type ID
		$intPersonContactTypeId = NULL;

		if( true == valObj( $objCompanyDepartment, 'CCompanyDepartment' ) ) {

			switch( $objCompanyDepartment->getName() ) {

				case 'Marketing':
					$intPersonContactTypeId = CPersonContactType::MARKETING;
					break;

				case 'Accounting':
					$intPersonContactTypeId = CPersonContactType::ACCOUNTING;
					break;

				case 'Maintenance':
					$intPersonContactTypeId = CPersonContactType::MAINTENANCE;
					break;

				case 'Managerial':
					$intPersonContactTypeId = CPersonContactType::MANAGER;
					break;

				case 'Administrative':
					$intPersonContactTypeId = CPersonContactType::MANAGER;
					break;

				default:
					$intPersonContactTypeId = CPersonContactType::OTHER;
					break;
			}

		}

		$objAdminDatabase->begin();

		if( false == $objPerson->validate( VALIDATE_INSERT ) || false == $objPerson->insert( $intUserId, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Person failed to import.' ) ) );

			return false;
		}

		$objPersonContactPreference = new CPersonContactPreference();
		$objPersonContactPreference->setPersonContactTypeId( $intPersonContactTypeId );
		$objPersonContactPreference->setPersonId( $objPerson->getId() );

		if( false == $objPersonContactPreference->insert( $intUserId, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Person Contact Preference failed to import.' ) ) );

			return false;
		}

		$objAdminDatabase->commit();

		return true;
	}

	public function generateBlogPassword() {
		$strPassword = \Psi\CStringService::singleton()->substr( $this->getPasswordEncrypted(), 0, 8 );

		$strPassword = \Psi\CStringService::singleton()->substr( CHash::createService()->hashGeneric( $strPassword ), 0, 8 );

		return $strPassword;
	}

	public function determineExistsInWordPress( $resWordPressDatabase ) {

		$strSql = "SELECT * FROM wp_users WHERE lower( user_login ) = '" . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . "' LIMIT 1;";

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$intUserId = NULL;

		while( false !== ( $arrstrUsers = mysqli_fetch_array( $resResult, MYSQLI_ASSOC ) ) ) {
			$intUserId = $arrstrUsers['ID'];
			break;
		}

		if( true == is_numeric( $intUserId ) ) {
			return true;
		}

		return false;
	}

	public function resyncWordPressAuthentication( $resWordPressDatabase ) {

		$strSql = 'SELECT * FROM wp_users WHERE lower( user_login ) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . '\' LIMIT 1';

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$intUserId = NULL;

		while( false !== ( $arrstrUsers = mysqli_fetch_array( $resResult, MYSQLI_ASSOC ) ) ) {
			$intUserId = $arrstrUsers['ID'];
			break;
		}

		if( false == is_numeric( $intUserId ) ) {
			echo 'wp_user does not exist.';

			return false;
		}

		// Please Connect with Smishra01(2225) or Tbhatt for class-phpass.php
		// Hash the encrypted company user password, and truncate to 8 characters.  Then use that password to apply to the word press users table.
		require_once( PATH_ENTRATA_LIBRARY . 'WordPress/class-phpass.php' );
		$objWordPressHasher   = new PasswordHash( 8, true );
		$strWordPressPassword = $objWordPressHasher->HashPassword( $this->generateBlogPassword() );

		$strSql = 'UPDATE wp_users SET user_pass = \'' . trim( $strWordPressPassword ) . '\' WHERE ID = ' . ( int ) $intUserId;

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$strSql = 'DELETE FROM wp_usermeta WHERE user_id = ' . ( int ) $intUserId . ' AND meta_key IN ( \'wp_capabilities\', \'wp_user_level\' ) ';

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$strSql = '
		INSERT INTO `wp_usermeta` ( `user_id`, `meta_key`, `meta_value` ) VALUES
		  ( ' . ( int ) $intUserId . ', \'wp_capabilities\', \'a:1:{s:13:\"administrator\";b:1;}\' ),
		  ( ' . ( int ) $intUserId . ', \'wp_user_level\', \'10\' );';

		if( false == mysqli_query( $resWordPressDatabase, $strSql ) ) {
			return false;
		}

		return true;
	}

	public function insertWordPressUser( $strWebsiteUrl, $resWordPressDatabase, $objDatabase ) {

		// Hash the encrypted company user password, and truncate to 8 characters.  Then use that password to apply to the word press users table.
		require_once( PATH_ENTRATA_LIBRARY . 'WordPress/class-phpass.php' );
		$objWordPressHasher   = new PasswordHash( 8, true );
		$strWordPressPassword = $objWordPressHasher->HashPassword( $this->generateBlogPassword() );

		$objCompanyEmployee = $this->fetchCompanyEmployee( $objDatabase );
		$strEmailAddress    = ( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) ? $objCompanyEmployee->getEmailAddress() : 'bloguser_' . $this->getId() . '@' . CONFIG_COMPANY_NAME . '.com';

		$strSql = "INSERT INTO `wp_users` (`user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`)
					VALUES ('" . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . "','" . $strWordPressPassword . "','" . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . "	','" . $strEmailAddress . "','http://" . $strWebsiteUrl . "','" . date( 'Y-m-d' ) . "','',0,'" . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . "'); ";

		if( false == mysqli_query( $resWordPressDatabase, $strSql ) ) {
			echo 'wp_user failed to insert.';
			echo $strSql;
			exit;

			return false;
		}

		$strSql = "SELECT ID FROM wp_users WHERE user_login = '" . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . "'";

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			echo 'wp_user id failed to retrieve.';

			return false;
		}

		$arrstrResult = mysqli_fetch_array( $resResult, MYSQLI_ASSOC );

		$intUserId = ( int ) $arrstrResult['ID'];

		$strSql = 'INSERT INTO `wp_usermeta` ( `user_id`, `meta_key`, `meta_value` ) VALUES
		  ( ' . ( int ) $intUserId . ', \'wp_capabilities\', \'a:1:{s:13:\"administrator\";b:1;}\' ),
		  ( ' . ( int ) $intUserId . ', \'wp_user_level\', \'10\' );';

		if( false == mysqli_query( $resWordPressDatabase, $strSql ) ) {
			return false;
		}

		return true;
	}

	public function marketingLogin( $strEntrataDomain, $objDatabase, $arrstrOptions = [] ) {
		$intAuthenticationLogTypeId = getArrayElementByKey( 'authentication_log_type_id', $arrstrOptions );

		$objCompanyUser		= \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchMarketingTypeCompanyUserByUsernameByEntrataDomainByCid( $this->m_strUsername, $strEntrataDomain, $this->m_intCid, $objDatabase );
		if( true == valObj( $objCompanyUser, CCompanyUser::class ) && ( false == valStr( $this->m_strPassword ) || ( true == valStr( $this->m_strPassword ) && false == ( \Psi\Libraries\Cryptography\CCrypto::create() )->verifyHash( $this->m_strPassword, $objCompanyUser->getPasswordEncrypted() ) ) ) ) {
			$objCompanyUser = NULL;
		}

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$this->m_boolIsLoggedIn	= true;
			$this->m_intId			= $objCompanyUser->getId();

			// We need following condition for users who are logging in from client admin which dont have client set to them.
			if( CClients::$c_intNullCid !== $this->getCid() ) {
				$objCompanyUser->setCid( $this->getCid() );
			}

			$this->logAuthentication( $objDatabase, $intAuthenticationLogTypeId, getArrayElementByKey( 'session_id', $arrstrOptions ) );
			$objCompanyUser->setAuthenticationLogId( $this->getAuthenticationLogId() );
		} else {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', __( 'Invalid username and/or password' ), 304 ) );
			return false;
		}

		if( true == valStr( $this->m_strPassword ) && \Psi\Libraries\Cryptography\CCryptoProviderFactory::CRYPTO_PROVIDER_MCRYPT == \Psi\Libraries\Cryptography\CCrypto::create()->getProviderNameForHash( $objCompanyUser->getPasswordEncrypted() ) ) {
			$objCompanyUser->setPassword( $this->m_strPassword );
		}

		return $objCompanyUser;
	}

	public function login( $strRwxDomain, $objDatabase, $boolIsResidentWorksLogin = true, $arrstrSamlUserDetails = NULL, $arrstrOptions = [] ) {
		$intAuthenticationLogTypeId = getArrayElementByKey( 'authentication_log_type_id', $arrstrOptions );

		$arrobjCompanyPreferences            = rekeyObjects( 'key', CCompanyPreferences::createService()->fetchCompanyPreferencesByKeysByCid( [ 'IS_ACTIVE_DIRECTORY_ENABLED', 'ACTIVE_DIRECTORY_DNS', 'ACTIVE_DIRECTORY_SERVER_PORT', 'ACTIVE_DIRECTORY_DOMAIN', 'ACTIVE_DIRECTORY_ADMIN_USER', 'ACTIVE_DIRECTORY_ADMIN_PASSWORD', 'ALLOWED_ACTIVE_DIRECTORY_GROUPS', 'ACTIVE_DIRECTORY_UNIQUE_IDENTIFIER_ATTR', 'SYNC_PROPERTY_PERMISSION_WITH_AD', 'USE_DISPLAY_NAME_INSTEAD_OF_NAME_FIELD', 'IS_SAML_SSO_ENABLED', 'SAML_SSO_RESTRICT_ENTRATA_LOGIN', 'IS_OIDC_ENABLED', 'USE_USERNAME_INSTEAD_OF_EMAIL', 'IS_INTERNAL_SSO' ], $this->m_intCid, $objDatabase ) );
		$boolIsActiveDirectoryEnabledCompany = ( false != valArr( $arrobjCompanyPreferences ) && false != array_key_exists( 'IS_ACTIVE_DIRECTORY_ENABLED', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['IS_ACTIVE_DIRECTORY_ENABLED']->getValue() : false;
		$boolIsSsoEnabledCompany             = ( false != valArr( $arrobjCompanyPreferences ) && false != array_key_exists( 'IS_SAML_SSO_ENABLED', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['IS_SAML_SSO_ENABLED']->getValue() : false;
		$boolISamlSsoRestrictEntrataLogin    = ( false != valArr( $arrobjCompanyPreferences ) && false != array_key_exists( 'SAML_SSO_RESTRICT_ENTRATA_LOGIN', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['SAML_SSO_RESTRICT_ENTRATA_LOGIN']->getValue() : false;
		$boolIsOidcEanbledCompany            = ( true == valArr( $arrobjCompanyPreferences ) && ( true == array_key_exists( 'IS_OIDC_ENABLED', $arrobjCompanyPreferences ) && true == $boolIsSsoEnabledCompany ) ) ? $arrobjCompanyPreferences['IS_OIDC_ENABLED']->getValue() : false;
		$boolIsUsernameInsteadOfEmail        = ( true == valArr( $arrobjCompanyPreferences ) && ( true == array_key_exists( 'USE_USERNAME_INSTEAD_OF_EMAIL', $arrobjCompanyPreferences ) ) ) ? $arrobjCompanyPreferences['USE_USERNAME_INSTEAD_OF_EMAIL']->getValue() : false;
		$boolIsInternalSso                   = ( true == valArr( $arrobjCompanyPreferences ) && ( true == array_key_exists( 'IS_INTERNAL_SSO', $arrobjCompanyPreferences ) ) ) ? $arrobjCompanyPreferences['IS_INTERNAL_SSO']->getValue() : false;

		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByUsernameByRwxDomainByCid( $this->m_strUsername, $strRwxDomain, $this->m_intCid, $objDatabase, $boolIsResidentWorksLogin );

		if( true == valArr( $arrstrSamlUserDetails ) ) {
			$objCompanyUser             = $this->samlLogin( $strRwxDomain, $arrstrSamlUserDetails, $objDatabase, $boolIsResidentWorksLogin );
			$intAuthenticationLogTypeId = CAuthenticationLogType::ENTRATA_SAML;

			// converting array into string to store into logs
			if( true == isset( $arrstrSamlUserDetails['groups'] ) ) {
				$arrstrSamlUserDetails['groups'] = implode( ', ', ( array ) $arrstrSamlUserDetails['groups'] );
			}

			// Log the SAML login details
			if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
				$objCompanyUser->logUserDetails( 'saml_login', $arrstrSamlUserDetails );
			}

		} elseif( true == $boolIsOidcEanbledCompany && ( true == isset( $arrstrOptions['oidc_authentication'] ) && true == $arrstrOptions['oidc_authentication'] ) ) {

			$objCompanyUser = $this->oidcLogin( $strRwxDomain, $objDatabase );

		} elseif( true == valObj( $objCompanyUser, CCompanyUser::class ) && ( false == $boolIsSsoEnabledCompany && true == $objCompanyUser->getIsSsoUser() && true == $boolIsInternalSso && false == valArr( $arrstrSamlUserDetails ) && empty( $arrstrOptions['is_token_based'] ) ) && ( false == $boolIsActiveDirectoryEnabledCompany || ( true == $boolIsActiveDirectoryEnabledCompany && false == $objCompanyUser->getIsActiveDirectoryUser() ) ) ) {
            $strUsername = ( true == $boolIsUsernameInsteadOfEmail && ( false == \Psi\CStringService::singleton()->strpos( $this->m_strUsername, '@' ) ) ) ? $this->m_strUsername . '@' . trim( $strRwxDomain ) : $this->m_strUsername;
			try {
				require_once( PATH_ENTRATA_LIBRARY . 'Sso/CEntrataSso.class.php' );
				$objSsoUser = new CEntrataSso();
				$objSsoUser->setAppName( $objSsoUser::APP_ENTRATA );
				$objSsoUser->authenticateUser( $strUsername, $this->m_strPassword );
				$arrstrUserInfo = $objSsoUser->getLoggedInUserInfo();
				$objCompanyUser->setSsoToken( $arrstrUserInfo['oidc_token'] ?? NULL );
			} catch( \Psi\Libraries\Authentication\Oidc\COidcException $objOidcException ) {
				if( 0 == $objOidcException->getCode() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, 'username', $objOidcException->getMessage() ) );
					return false;
				}

				if( false == ( \Psi\Libraries\Cryptography\CCrypto::create() )->verifyHash( $this->m_strPassword, $objCompanyUser->getPasswordEncrypted() ) ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', __( 'Invalid username and/or password' ), 304 ) );
					return false;
				}
			}
		} elseif( false != $boolIsActiveDirectoryEnabledCompany && ( false == isset( $arrstrOptions['is_token_based'] ) || false == $arrstrOptions['is_token_based'] ) ) {
			$boolBlockLogin = false;

			$objCompanyUser = $this->activeDirectoryLogin( $strRwxDomain, $arrobjCompanyPreferences, $boolIsResidentWorksLogin, $objDatabase, $boolBlockLogin );
			if( true == valObj( $objCompanyUser, 'CCompanyUser' ) && true == $objCompanyUser->getIsActiveDirectoryUser() ) {
				$intAuthenticationLogTypeId = CAuthenticationLogType::ENTRATA_ACTIVE_DIRECTORY;
			}

			if( true == $boolBlockLogin ) {
				return false;
			}

		} else {

			$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByUsernameByRwxDomainByCid( $this->m_strUsername, $strRwxDomain, $this->m_intCid, $objDatabase, $boolIsResidentWorksLogin );
			if( false == $arrstrOptions['is_token_based'] && true == valObj( $objCompanyUser, CCompanyUser::class ) && ( false == valStr( $this->m_strPassword ) || ( true == valStr( $this->m_strPassword ) && false == ( \Psi\Libraries\Cryptography\CCrypto::create() )->verifyHash( $this->m_strPassword, $objCompanyUser->getPasswordEncrypted() ) ) ) ) {
				$objCompanyUser = NULL;
			}
			if( true == valObj( $objCompanyUser, 'CCompanyUser' ) && true == $boolIsSsoEnabledCompany && true == $boolISamlSsoRestrictEntrataLogin ) {
				$boolCheckRestrictEntrataLogin = $objCompanyUser->samlCheckRestrictEntrataLogin();
				if( true == $boolCheckRestrictEntrataLogin ) {
					$this->setIsSamlRestrictEntrataLogin( true );
					$this->addErrorMsgs( $objCompanyUser->getErrorMsgs() );
					return false;
				}
			}
		}

		if( false == $arrstrOptions['is_token_based'] && true == valStr( $this->m_strPassword ) && true == valObj( $objCompanyUser, CCompanyUser::class ) && \Psi\Libraries\Cryptography\CCryptoProviderFactory::CRYPTO_PROVIDER_MCRYPT == \Psi\Libraries\Cryptography\CCrypto::create()->getProviderNameForHash( $objCompanyUser->getPasswordEncrypted() ) ) {
			$objCompanyUser->setPassword( $this->m_strPassword );
		}

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$this->m_boolIsLoggedIn = true;
			$this->m_intId          = $objCompanyUser->getId();

			if( true == $objCompanyUser->getIsAdministrator() ) {
				$this->m_intIsAdministrator = $objCompanyUser->getIsAdministrator();
			}

			// We need following condition for users who are logging in from client admin which dont have client set to them.
			if( CClients::$c_intNullCid !== $this->getCid() ) {
				$objCompanyUser->setCid( $this->getCid() );
			}

			if( false == isset( $arrstrOptions['do_not_log_authentication'] ) || false == $arrstrOptions['do_not_log_authentication'] ) {
				$this->logAuthentication( $objDatabase, $intAuthenticationLogTypeId, getArrayElementByKey( 'session_id', $arrstrOptions ) );
			}

			$objCompanyUser->setAuthenticationLogId( $this->getAuthenticationLogId() );

			$this->syncNotificationLogs( $objCompanyUser, $objDatabase );
		} else {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', __( 'Invalid username and/or password' ), 304 ) );

			return false;
		}

		return $objCompanyUser;
	}

	public function activeDirectoryLogin( $strRwxDomain, $arrobjCompanyPreferences, $boolIsResidentWorksLogin, $objDatabase, &$boolBlockLogin = false ) {

		$strActiveDirectoryDNS            = ( false != array_key_exists( 'ACTIVE_DIRECTORY_DNS', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['ACTIVE_DIRECTORY_DNS']->getValue() : false;
		$intActiveDirectoryServerPort     = ( false != array_key_exists( 'ACTIVE_DIRECTORY_SERVER_PORT', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['ACTIVE_DIRECTORY_SERVER_PORT']->getValue() : false;
		$strActiveDirectoryDomain         = ( false != array_key_exists( 'ACTIVE_DIRECTORY_DOMAIN', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['ACTIVE_DIRECTORY_DOMAIN']->getValue() : false;
		$strActiveDirectoryUsername       = ( false !== \Psi\CStringService::singleton()->strpos( $this->m_strUsername, '@' . $strRwxDomain ) ) ? str_replace( $strRwxDomain, $strActiveDirectoryDomain, $this->m_strUsername ) : $this->m_strUsername . '@' . $strActiveDirectoryDomain;
		$strAllowedActiveDirectoryGroups  = ( false != array_key_exists( 'ALLOWED_ACTIVE_DIRECTORY_GROUPS', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['ALLOWED_ACTIVE_DIRECTORY_GROUPS']->getValue() : '';
		$strUniqueIdentifierAttr          = ( false != array_key_exists( 'ACTIVE_DIRECTORY_UNIQUE_IDENTIFIER_ATTR', $arrobjCompanyPreferences ) ) ? \Psi\CStringService::singleton()->strtolower( $arrobjCompanyPreferences['ACTIVE_DIRECTORY_UNIQUE_IDENTIFIER_ATTR']->getValue() ) : NULL;
		$boolSyncPropertyGroups           = ( false != array_key_exists( 'SYNC_PROPERTY_PERMISSION_WITH_AD', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['SYNC_PROPERTY_PERMISSION_WITH_AD']->getValue() : false;
		$intDisplayNameFormatVal          = ( false != array_key_exists( 'USE_DISPLAY_NAME_INSTEAD_OF_NAME_FIELD', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['USE_DISPLAY_NAME_INSTEAD_OF_NAME_FIELD']->getValue() : false;
		$boolIsSsoEnabledCompany          = ( false != valArr( $arrobjCompanyPreferences ) && false != array_key_exists( 'IS_SAML_SSO_ENABLED', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['IS_SAML_SSO_ENABLED']->getValue() : false;
		$boolISamlSsoRestrictEntrataLogin = ( false != valArr( $arrobjCompanyPreferences ) && false != array_key_exists( 'SAML_SSO_RESTRICT_ENTRATA_LOGIN', $arrobjCompanyPreferences ) ) ? $arrobjCompanyPreferences['SAML_SSO_RESTRICT_ENTRATA_LOGIN']->getValue() : false;

		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByUsernameByRwxDomainByCid( $this->m_strUsername, $strRwxDomain, $this->m_intCid, $objDatabase, $boolIsResidentWorksLogin );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) && true == $boolIsSsoEnabledCompany && true == $boolISamlSsoRestrictEntrataLogin ) {

			$boolCheckRestrictEntrataLogin = $objCompanyUser->samlCheckRestrictEntrataLogin();
			if( true == $boolCheckRestrictEntrataLogin ) {
				$boolBlockLogin = true;
				$this->addErrorMsgs( $objCompanyUser->getErrorMsgs() );
				return false;
			}
		}

		if( false != valObj( $objCompanyUser, 'CCompanyUser' ) ) {

			if( true == $objCompanyUser->getIsActiveDirectoryUser() ) {

				$objLdap = new CLdap( $strActiveDirectoryDNS, $intActiveDirectoryServerPort );

				if( false != $objLdap->authenticate( $strActiveDirectoryUsername, $this->m_strPassword, $strActiveDirectoryDomain ) ) {

					$objDuplicateRpkCompanyEmployee = NULL;

					if( false !== \Psi\CStringService::singleton()->strpos( $strActiveDirectoryUsername, '@' ) ) {
						$strActiveDirectoryUsername = \Psi\CStringService::singleton()->substr( $strActiveDirectoryUsername, 0, \Psi\CStringService::singleton()->strpos( $strActiveDirectoryUsername, '@' ) );
					}

					if( false != valStr( $strAllowedActiveDirectoryGroups ) && false == $objLdap->verifyUserWithAllowedGroups( $strAllowedActiveDirectoryGroups, $strActiveDirectoryUsername ) ) {
						return false;
					}

					$boolIsCompanyUserUpdateRequired = false;
					if( false == ( \Psi\Libraries\Cryptography\CCrypto::create() )->verifyHash( $this->m_strPassword, $objCompanyUser->getPasswordEncrypted() ) ) {
						$objCompanyUser->setPasswordEncrypted( $this->m_strPasswordEncrypted );
						$boolIsCompanyUserUpdateRequired = true;
					}

					$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );

					$arrmixUserDetails = ( array ) $objLdap->getUserDetails( $strActiveDirectoryUsername, [ $strUniqueIdentifierAttr ] );
					if( true == array_key_exists( $strUniqueIdentifierAttr, $arrmixUserDetails ) ) {
						$arrmixDuplicateRpkCompanyEmployees = CCompanyEmployees::createService()->fetchCompanyEmployeesByCidByIdByEmployeeCode( $objCompanyUser->getCid(), $objCompanyUser->getCompanyEmployeeId(), \Psi\CStringService::singleton()->strtolower( $arrmixUserDetails[$strUniqueIdentifierAttr] ), $objDatabase );

						if( true == valArr( $arrmixDuplicateRpkCompanyEmployees[0] ) ) {

							if( 1 == $arrmixDuplicateRpkCompanyEmployees[0]['is_disabled'] || 0 == $arrmixDuplicateRpkCompanyEmployees[0]['is_active_directory_user'] ) {
								$objDuplicateRpkCompanyEmployee = CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $arrmixDuplicateRpkCompanyEmployees[0]['id'], $this->m_intCid, $objDatabase );
								$objDuplicateRpkCompanyEmployee->setEmployeeCode( NULL );
								$arrmixUserDetails['employee_code'] = $arrmixUserDetails[$strUniqueIdentifierAttr];
							} else {
								trigger_error( 'LDAP Unique Identifier already associated with other company employee[RPK: ' . $arrmixUserDetails[$strUniqueIdentifierAttr] . '].', E_USER_WARNING );
							}
						}
						unset( $arrmixUserDetails[$strUniqueIdentifierAttr] );
					} else {
						$arrmixUserDetails['employee_code'] = NULL;
					}
					if( true == isset( $intDisplayNameFormatVal ) && true == valStr( $intDisplayNameFormatVal ) ) {
						$arrmixUserDetails['display_name_format'] = $intDisplayNameFormatVal;
					}

					if( true == isset( $arrmixUserDetails['active_directory_guid'] ) && true == valStr( $arrmixUserDetails['active_directory_guid'] ) && $arrmixUserDetails['active_directory_guid'] != $objCompanyUser->getRemotePrimaryKey() ) {
						$objCompanyUser->setRemotePrimaryKey( $arrmixUserDetails['active_directory_guid'] );
						$boolIsCompanyUserUpdateRequired = true;
					}

					$objCompanyEmployee = CCompanyUser::setCompanyEmployeeDetails( $arrmixUserDetails, $objCompanyEmployee );
					switch( NULL ) {

						default:

							$objDatabase->begin();

							if( true == $boolIsCompanyUserUpdateRequired ) {
								$objConnectDatabase = CDatabases::createConnectDatabase( false );
								$objConnectDatabase->begin();
								$objCompanyUser->setAllowDifferentialUpdate( true );
								if( false == $objCompanyUser->update( $objCompanyUser->getId(), $objDatabase, $objConnectDatabase ) ) {
									$objDatabase->rollback();
									$objConnectDatabase->rollback();
									break;
								}
							}

							if( true == valObj( $objDuplicateRpkCompanyEmployee, 'CCompanyEmployee' ) && false == $objDuplicateRpkCompanyEmployee->update( $objDuplicateRpkCompanyEmployee->getId(), $objDatabase ) ) {
								$objDatabase->rollback();
								if( true == $boolIsCompanyUserUpdateRequired ) {
									$objConnectDatabase->rollback();
								}
								break;
							}

							if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
								// Do not break the transaction of updating remote primary key( AD unique ID )
								if( false == $objCompanyEmployee->update( $objCompanyUser->getId(), $objDatabase ) ) {
									$objDatabase->rollback();
									if( true == $boolIsCompanyUserUpdateRequired ) {
										$objConnectDatabase->rollback();
									}
									break;
								}
							}

							$objDatabase->commit();
							if( true == $boolIsCompanyUserUpdateRequired ) {
								$objConnectDatabase->commit();
								$objConnectDatabase->close();
							}

					}
				} else {
					return false;
				}

			} else {
				// For non Active Directory User
				if( false == valStr( $this->m_strPassword ) || false == ( \Psi\Libraries\Cryptography\CCrypto::create() )->verifyHash( $this->m_strPassword, $objCompanyUser->getPasswordEncrypted() ) ) {
					return false;
				}
			}

		} else {

			$objLdap = new CLdap( $strActiveDirectoryDNS, $intActiveDirectoryServerPort );

			if( false != $objLdap->authenticate( $strActiveDirectoryUsername, $this->m_strPassword, $strActiveDirectoryDomain ) ) {

				$arrmixDuplicateRpkCompanyEmployees = NULL;
				if( false !== \Psi\CStringService::singleton()->strpos( $strActiveDirectoryUsername, '@' ) ) {
					$strActiveDirectoryUsername = \Psi\CStringService::singleton()->substr( $strActiveDirectoryUsername, 0, \Psi\CStringService::singleton()->strpos( $strActiveDirectoryUsername, '@' ) );
				}

				if( false != valStr( $strAllowedActiveDirectoryGroups ) && false == $objLdap->verifyUserWithAllowedGroups( $strAllowedActiveDirectoryGroups, $strActiveDirectoryUsername ) ) {
					return false;
				}

				$arrmixUserDetails = ( array ) $objLdap->getUserDetails( $strActiveDirectoryUsername, [ $strUniqueIdentifierAttr ] );
				$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByRemotePrimaryKeyByCid( $this->m_intCid, $arrmixUserDetails['active_directory_guid'], $objDatabase );

				if( false == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
					$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserWithAllStatusByUsernameByRwxDomainByCid( $this->m_strUsername, $strRwxDomain, $this->m_intCid, $objDatabase );
				}

				if( true == array_key_exists( $strUniqueIdentifierAttr, $arrmixUserDetails ) ) {

					$intCompanyEmployeeId = NULL;
					if( false != valObj( $objCompanyUser, 'CCompanyUser' ) ) {
						$intCompanyEmployeeId = $objCompanyUser->getCompanyEmployeeId();
					}

					$arrmixDuplicateRpkCompanyEmployees = CCompanyEmployees::createService()->fetchCompanyEmployeesByCidByIdByEmployeeCode( $this->m_intCid, $intCompanyEmployeeId, \Psi\CStringService::singleton()->strtolower( $arrmixUserDetails[$strUniqueIdentifierAttr] ), $objDatabase );

					if( true == valArr( $arrmixDuplicateRpkCompanyEmployees[0] ) ) {

						if( 1 == $arrmixDuplicateRpkCompanyEmployees[0]['is_disabled'] || 0 == $arrmixDuplicateRpkCompanyEmployees[0]['is_active_directory_user'] ) {
							$objDuplicateRpkCompanyEmployee = CCompanyEmployees::createService()->fetchCompanyEmployeeByIdByCid( $arrmixDuplicateRpkCompanyEmployees[0]['id'], $this->m_intCid, $objDatabase );
							$objDuplicateRpkCompanyEmployee->setEmployeeCode( NULL );
							if( false == $objDuplicateRpkCompanyEmployee->update( $objDuplicateRpkCompanyEmployee->getId(), $objDatabase ) ) {
								return false;
							}
							$arrmixUserDetails['employee_code'] = $arrmixUserDetails[$strUniqueIdentifierAttr];
						} else {
							trigger_error( 'LDAP Unique Identifier already associated with other company employee[RPK: ' . $arrmixUserDetails[$strUniqueIdentifierAttr] . '].', E_USER_WARNING );
						}
					}
					unset( $arrmixUserDetails[$strUniqueIdentifierAttr] );
				} else {
					$arrmixUserDetails['employee_code'] = NULL;
				}

				$strUsername = ( false !== \Psi\CStringService::singleton()->strpos( $strActiveDirectoryUsername, '@' . $strActiveDirectoryDomain ) ) ? str_replace( $strActiveDirectoryDomain, $strRwxDomain, $strActiveDirectoryUsername ) : $strActiveDirectoryUsername;
				$strUsername = ( false !== \Psi\CStringService::singleton()->strpos( $strUsername, '@' . $strRwxDomain ) ) ? $strUsername : $strUsername . '@' . $strRwxDomain;

				$objConnectDatabase = CDatabases::createConnectDatabase( false );
				if( false != valObj( $objCompanyUser, 'CCompanyUser' ) ) {

					$this->setIsActiveDirectoryUser( 1 );
					$objCompanyUser->setUsername( $strUsername );
					$objCompanyUser = $this->updateCompanyUser( $objCompanyUser, $arrmixUserDetails, $objDatabase, $objConnectDatabase );

				} else {
					$this->setIsActiveDirectoryUser( 1 );
					$this->m_strUsername = $strUsername; // override entered username as per system requirement
					$objCompanyUser = $this->createAndInsertCompanyUser( $arrmixUserDetails, $objDatabase, $objConnectDatabase );
				}
				$objConnectDatabase->close();
			}
		}

		if( false != valObj( $objCompanyUser, 'CCompanyUser' ) && true == $objCompanyUser->getIsActiveDirectoryUser() ) {
			$this->syncActiveDirectoryUserGroups( $objCompanyUser, $objLdap, $objDatabase );
			if( false != $boolSyncPropertyGroups ) {
				$objCompanyUser->syncActiveDirectoryProperties( $objLdap, $objDatabase );
			}
		}

		return $objCompanyUser;
	}

	public function syncActiveDirectoryProperties( $objLdap, $objDatabase ) {
		// sync property permission
		$strSystemCode           = NULL;
		$strAllPropertyGroupName = \Psi\CStringService::singleton()->strtolower( CPropertyGroup::ALL_PROPERTIES );

		$arrstrActiveDirectoryUserGroups = $objLdap->getUserGroupsByGuid( $this->getRemotePrimaryKey() );
		$arrstrPropertyGroups            = ( array ) $objLdap->fetchGroupMembersByGroupName( CPropertyGroup::LDAP_PARENT_PROPERTY_GROUP_NAME );

		if( true == in_array( $strAllPropertyGroupName, $arrstrActiveDirectoryUserGroups ) && true == in_array( $strAllPropertyGroupName, $arrstrPropertyGroups ) ) {
			$arrstrPropertyGroupNames[] = $strAllPropertyGroupName;
			$strSystemCode              = CPropertyGroup::SYSTEM_CODE_ALL;
		} else {
			$arrstrPropertyGroupNames = array_intersect( $arrstrActiveDirectoryUserGroups, $arrstrPropertyGroups );
		}
		$this->syncPropertyPermissions( $arrstrPropertyGroupNames, $strSystemCode, $objDatabase );
	}

	public function samlLogin( $strRwxDomain, &$arrstrSamlUserDetails, $objDatabase, $boolIsResidentWorksLogin ) {

		$arrmixOptions['saml_username_type'] = $arrstrSamlUserDetails['saml_username_type'];
		$arrmixOptions['is_saml_login'] = true;
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByUsernameByRwxDomainByCid( $this->m_strUsername, $strRwxDomain, $this->m_intCid, $objDatabase, $boolIsResidentWorksLogin, false, $arrmixOptions );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {

			$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );

			// Enable sso setting for logged in SAML user
			if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
				$objCompanyEmployee->setIsSsoUser( 1 );

				if( false == $objCompanyEmployee->update( $objCompanyUser->getId(), $objDatabase ) ) {
					return false;
				}
			}

			$objCompanyUser->setLastLogin( date( 'm/d/Y G:i:s' ) );
			return $objCompanyUser->syncSamlCompanyUserData( $arrstrSamlUserDetails, $objDatabase );
		}

		$objConnectDatabase = CDatabases::createConnectDatabase( false );
		$objCompanyUser     = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserWithAllStatusByUsernameByRwxDomainByCid( $this->m_strUsername, $strRwxDomain, $this->m_intCid, $objDatabase, $arrmixOptions );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			// Keep user's old password while updating user record
			$this->setPasswordEncrypted( $objCompanyUser->getPasswordEncrypted() );

			$arrstrSamlUserDetails['is_sso_user'] = 1;
			// If user is disabled in Entrata but not in Client's Active Directory or in the Identity provider system
			$objCompanyUser = $this->updateCompanyUser( $objCompanyUser, $arrstrSamlUserDetails, $objDatabase, $objConnectDatabase );
		} else {
			// If user not found in database, create new user with random password. Since we do not get the password in case of SAML login
			$this->setPassword( $this->generateRandomPassword( $objDatabase ) );
			$arrstrSamlUserDetails['is_sso_user'] = 1;
			$objCompanyUser = $this->createAndInsertCompanyUser( $arrstrSamlUserDetails, $objDatabase, $objConnectDatabase );
		}

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objCompanyUser->setLastLogin( date( 'm/d/Y G:i:s' ) );
			return $objCompanyUser->syncSamlCompanyUserData( $arrstrSamlUserDetails, $objDatabase );
		}
		$objConnectDatabase->close();
		return false;
	}

	public function samlCheckRestrictEntrataLogin( $boolPasswordCheck = false ) {
		// Allow login for Admin users
		if( true == $this->getIsAdministrator() ) {
			return false;
		}

		$strWhere = sprintf( 'WHERE company_user_id = %d AND cid = %d AND deleted_by IS NULL AND deleted_on IS NULL', ( int ) $this->getId(), ( int ) $this->getCid() );
		$intCount = \Psi\Eos\Entrata\CCompanyUserServices::createService()->fetchCompanyUserServiceCount( $strWhere, $this->getDatabase() );

		if( 0 < $intCount ) {
			return false;
		}

		if( false == $boolPasswordCheck ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please use SAML link to login.' ) ) );
		}
		return true;
	}

	public function syncSamlCompanyUserData( $arrstrSamlUserDetails, $objDatabase ) {

		// Fetch existing user_group assignments of the user
		$strCompanyGroups = \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupsByIdsByComapnyUserIdByCid( $arrstrCompanyGroupIds = NULL, $this->getId(), $this->getCid(), $objDatabase, $boolFetchGroupNameOnly = true );
		if( false == is_null( $strCompanyGroups ) ) {
			$arrstrSamlUserDetails['old_groups'] = $strCompanyGroups;
		}

		if( true == isset( $arrstrSamlUserDetails['groups'] ) && true == valArr( $arrstrSamlUserDetails['groups'] ) ) {
			$this->syncSamlUserGroups( $arrstrSamlUserDetails['groups'], $objDatabase );
		}

		if( true == isset( $arrstrSamlUserDetails['property_groups'] ) && true == valArr( $arrstrSamlUserDetails['property_groups'] ) ) {
			$arrobjCompanyPreference = CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'SAML_SYNC_PROPERTY_PERMISSIONS', $this->m_intCid, $objDatabase );
			if( true == valObj( $arrobjCompanyPreference, 'CCompanyPreference' ) && 1 == $arrobjCompanyPreference->getValue() ) {
				$this->syncSamlProperties( $arrstrSamlUserDetails['property_groups'], $objDatabase );
			}
		}
		return $this;
	}

	public function oidcLogin( $strRwxDomain, $objDatabase ) {

		$arrobjOidcCompanyPreferences = rekeyObjects( 'key', CCompanyPreferences::createService()->fetchCompanyPreferencesByKeysByCid( [ 'OIDC_PROVIDER', 'OIDC_ENDPOINT', 'OIDC_CLIENT_ID', 'OIDC_CLIENT_SECRET', 'SAML_USERNAME_TYPE' ], $this->m_intCid, $objDatabase ) );

		if( false == valArr( $arrobjOidcCompanyPreferences ) ) {
			return false;
		}

		try{
			$objOidcAuthentication = new \Psi\Libraries\Authentication\Oidc\COidcAuthenticationClient();

			$boolIsAuthenticated   = $objOidcAuthentication->setTokenEndPoint( $arrobjOidcCompanyPreferences['OIDC_ENDPOINT']->getValue() )
															->setClientId( $arrobjOidcCompanyPreferences['OIDC_CLIENT_ID']->getValue() )
															->setClientSecret( $arrobjOidcCompanyPreferences['OIDC_CLIENT_SECRET']->getValue() )
															->authenticateUser( $this->m_strUsername, $this->m_strPassword );

			if( false == $boolIsAuthenticated ) {
				CNewRelic::createService()->addCustomParameters( [ 'oidc-username' => 'failed' ] );
				return false;
			}

			$arrstrAuthenticatedUserInfo = $objOidcAuthentication->getUserInfo();
			$strUsername                 = $arrstrAuthenticatedUserInfo['user-name'];
			CNewRelic::createService()->addCustomParameters( [ 'oidc-username' => $strUsername ] );

			$intUserType = $arrobjOidcCompanyPreferences['SAML_USERNAME_TYPE']->getValue();
			$arrmixOptions['is_saml_login']      = true;
			$arrmixOptions['saml_username_type'] = $intUserType;

			$strUsername = $this->formatOidcUsername( $strUsername, $intUserType, $strRwxDomain );
			$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByUsernameByRwxDomainByCid( $strUsername, $strRwxDomain, $this->m_intCid, $objDatabase, true, false, $arrmixOptions );

			if( true == valObj( $objCompanyUser, CCompanyUser::class ) ) {
				return $objCompanyUser;
			}

			return false;
		} catch( \Psi\Libraries\Authentication\Oidc\COidcException $objOidcException ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, 'username', $objOidcException->getMessage() ) );
			return false;
		}
	}

	private function formatOidcUsername( $strUsername, $intUserType, $strRwxDomain ) {
		if( false !== \Psi\CStringService::singleton()->strpos( $strUsername, '@' ) ) {
			if( true == is_null( $intUserType ) || $intUserType == CCompanyPreference::SAML_USERNAME_TYPE_PLAIN_USERNAME ) {
				$strUsername = \Psi\CStringService::singleton()->substr( $strUsername, 0, \Psi\CStringService::singleton()->strpos( $strUsername, '@' ) ) . '@' . trim( $strRwxDomain );
			}
		} else {
			$strUsername = $strUsername . '@' . trim( $strRwxDomain );
		}

		return $strUsername;
	}

	public function createAndInsertCompanyUser( $arrmixUserDetails, $objDatabase, $objConnectDatabase ) {

		$this->getOrFetchClient( $objDatabase );

		$objCompanyUser = $this->m_objClient->createCompanyUser();
		$objCompanyUser->setRwxDomain( $this->m_objClient->getRwxDomain() );
		$objCompanyUser->setIsActiveDirectoryUser( $this->getIsActiveDirectoryUser() );

		if( true == isset( $arrmixUserDetails['active_directory_guid'] ) ) {
			$objCompanyUser->setRemotePrimaryKey( $arrmixUserDetails['active_directory_guid'] );
		}

		$strUserName = \Psi\CStringService::singleton()->strtolower( ( string ) $this->m_strUsername );
		if( false == CValidation::validateEmailAddress( $strUserName ) ) {
			$strUserName = \Psi\CStringService::singleton()->strtolower( ( string ) $this->getUsernameWithoutRwxDomain() ) . '@' . trim( $this->m_objClient->getRwxDomain() );
		}

		$objCompanyUser->setUsername( $strUserName );
		$objCompanyUser->setPasswordEncrypted( $this->m_strPasswordEncrypted );
		$objCompanyUser->setPassword( $this->m_strPassword );
		$objCompanyUser->setLastLogin( 'NOW()' );

		$objCompanyEmployee = $this->m_objClient->createCompanyEmployee();

		$objCompanyEmployee = CCompanyUser::setCompanyEmployeeDetails( $arrmixUserDetails, $objCompanyEmployee );

		// Ap Payees
		$objApPayee = $this->m_objClient->createApPayee();
		$objApPayee->setApPayeeTypeId( CApPayeeType::EMPLOYEE );
		$objApPayee->setCompanyName( $objCompanyEmployee->getNamefirst() . ' ' . $objCompanyEmployee->getNameLast() );

		$objApPayeeTerm = CApPayeeTerms::fetchApPayeeTermBySystemCodeByCid( 'Net', $this->m_objClient->getId(), $objDatabase );

		if( false == valObj( $objApPayeeTerm, 'CApPayeeTerm' ) ) {
			$objApPayeeTerm = CApPayeeTerms::fetchApPayeeTermByDaysByCid( '0', $this->m_objClient->getId(), $objDatabase );
		}

		$objApPayee->setApPayeeTermId( $objApPayeeTerm->getId() );

		$objApRemittance    = $objApPayee->createApRemittance();
		$objApLegalEntities = $objApPayee->createApLegalEntity();
		$objApPayeeLocation = $objApPayee->createApPayeeLocation();
		$objApPayeeLocation->setLocationName( 'Corporate' );
		$objApPayeeLocation->setIsPrimary( 1 );

		$objApPayeeAccount = $objApPayee->createApPayeeAccount();

		$objApPayeeContactPrimary = $objApPayee->createApPayeeContact();
		$objApPayeeContactPrimary->setNameFirst( $objCompanyEmployee->getNameFirst() );
		$objApPayeeContactPrimary->setNameLast( $objCompanyEmployee->getNameLast() );
		$objApPayeeContactPrimary->setEmailAddress( $objCompanyEmployee->getEmailAddress() );
		$objApPayeeContactPrimary->setPhoneNumber( $objCompanyEmployee->getPhoneNumber1() );

		$objApPayeeContactVendor = $objApPayee->createApPayeeLocation();

		switch( NULL ) {

			default:

				$objDatabase->begin();
				$objConnectDatabase->begin();

				if( false == $objCompanyUser->validate( VALIDATE_INSERT, $objDatabase, $objConnectDatabase ) ) {
					$this->addErrorMsgs( $objCompanyUser->getErrorMsgs() );
					break;
				}

				if( false == $objApPayee->insert( CUser::ID_SYSTEM, $objDatabase ) ) {
					$objDatabase->rollback();
					$objConnectDatabase->rollback();
					break;
				}

				$objApLegalEntities->setApPayeeId( $objApPayee->getId() );
				$objApLegalEntities->setEntityName( $objApPayee->getCompanyName() );
				if( false == $objApLegalEntities->insert( $objCompanyUser->getId(), $objDatabase ) ) {
					$this->addErrorMsgs( $objCompanyEmployee->getErrorMsgs() );
					break;
				}

				$objApPayeeLocation->setApPayeeId( $objApPayee->getId() );
				$objApPayeeLocation->setApLegalEntityId( $objApLegalEntities->getId() );

				if( false == $objApPayeeLocation->insert( CUser::ID_SYSTEM, $objDatabase ) ) {
					$objDatabase->rollback();
					$objConnectDatabase->rollback();
					break;
				}

				$objApRemittance->setApPayeeId( $objApPayee->getId() );
				$objApRemittance->setApPayeeLocationId( $objApPayeeLocation->getId() );
				$objApRemittance->setIsDefault( true );
				if( false == $objApRemittance->insert( $objCompanyUser->getId(), $objDatabase ) ) {
					$this->addErrorMsgs( $objCompanyEmployee->getErrorMsgs() );
					break;
				}

				$objApPayeeAccount->setApPayeeId( $objApPayee->getId() );
				$objApPayeeAccount->setApPayeeLocationId( $objApPayeeLocation->getId() );
				$objApPayeeAccount->setDefaultApRemittanceId( $objApRemittance->getId() );

				if( false == $objApPayeeAccount->insert( CUser::ID_SYSTEM, $objDatabase ) ) {
					$objDatabase->rollback();
					$objConnectDatabase->rollback();
					break;
				}

				$objApPayeeContactVendor->setApPayeeId( $objApPayee->getId() );
				$objApPayeeContactVendor->setApLegalEntityId( $objApLegalEntities->getId() );

				if( false == $objApPayeeContactVendor->insert( CUser::ID_SYSTEM, $objDatabase ) ) {
					$objDatabase->rollback();
					$objConnectDatabase->rollback();
					break;
				}

				$objApPayeeContactPrimary->setApPayeeId( $objApPayee->getId() );
				if( false == $objApPayeeContactPrimary->insert( CUser::ID_SYSTEM, $objDatabase ) ) {
					$objDatabase->rollback();
					$objConnectDatabase->rollback();
					break;
				}

				$objCompanyEmployee->setApPayeeId( $objApPayee->getId() );
				if( false == $objCompanyEmployee->insert( CUser::ID_SYSTEM, $objDatabase ) ) {
					$objDatabase->rollback();
					$objConnectDatabase->rollback();
					break;
				}

				$objCompanyUser->setCompanyEmployeeId( $objCompanyEmployee->getId() );

				if( false == $objCompanyUser->insert( CUser::ID_SYSTEM, $objDatabase, $objConnectDatabase ) ) {
					$objDatabase->rollback();
					$objConnectDatabase->rollback();
					unset( $objCompanyUser );
					break;
				}

				$objDatabase->commit();
				$objConnectDatabase->commit();

				return $objCompanyUser;
		}

		return false;
	}

	public function updateCompanyUser( $objCompanyUser, $arrmixUserDetails, $objDatabase, $objConnectDatabase ) {

		$objCompanyUser->setIsActiveDirectoryUser( $this->getIsActiveDirectoryUser() );
		$objCompanyUser->setIsDisabled( 0 );
		$objCompanyUser->setPasswordEncrypted( $this->m_strPasswordEncrypted );

		if( true == isset( $arrmixUserDetails['active_directory_guid'] ) ) {
			$objCompanyUser->setRemotePrimaryKey( $arrmixUserDetails['active_directory_guid'] );
		}

		$objCompanyEmployee = $objCompanyUser->fetchCompanyEmployee( $objDatabase );

		switch( NULL ) {

			default:

				$objDatabase->begin();
				$objConnectDatabase->begin();

				$objCompanyUser->setAllowDifferentialUpdate( true );
				if( false == $objCompanyUser->update( $objCompanyUser->getId(), $objDatabase, $objConnectDatabase ) ) {
					$objDatabase->rollback();
					$objConnectDatabase->rollback();
					break;
				}

				if( false != valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {

					$objCompanyEmployee = CCompanyUser::setCompanyEmployeeDetails( $arrmixUserDetails, $objCompanyEmployee );

					// Do not break the transaction of updating remote primary key( AD unique ID )
					if( false == $objCompanyEmployee->update( $objCompanyUser->getId(), $objDatabase ) ) {
						$objDatabase->rollback();
						$objConnectDatabase->rollback();
						break;
					}

				}

				$objDatabase->commit();
				$objConnectDatabase->commit();

				return $objCompanyUser;
		}

		return false;
	}

	public function syncActiveDirectoryUserGroups( $objCompanyUser, \CLdap $objLdap, $objDatabase ) {

		$arrstrAssignedRemovedGroups     = [];
		$arrstrActiveDirectoryUserGroups = $objLdap->getUserGroupsByGuid( $objCompanyUser->getRemotePrimaryKey() );

		$arrmixCompanyUserGroups = ( array ) \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchActiveDirectoryEnabledCompanyGroupsByCompanyUserIdByCid( $objCompanyUser->getId(), $objCompanyUser->getCid(), $objDatabase );

		$arrstrCompanyGroups     = [];
		$arrstrCompanyUserGroups = [];

		if( true == valArr( $arrmixCompanyUserGroups ) ) {
			foreach( $arrmixCompanyUserGroups as $arrmixCompanyUserGroup ) {

				$arrstrCompanyGroups[$arrmixCompanyUserGroup['company_group_id']] = $arrmixCompanyUserGroup['company_group_name'];

				if( $arrmixCompanyUserGroup['company_user_id'] > 0 ) {
					$arrstrCompanyUserGroups[$arrmixCompanyUserGroup['company_user_group_id']] = $arrmixCompanyUserGroup['company_group_name'];
				}
			}
		}

		$arrstrCommonGroups = array_intersect( $arrstrActiveDirectoryUserGroups, $arrstrCompanyUserGroups );

		if( \Psi\Libraries\UtilFunctions\count( $arrstrCommonGroups ) != \Psi\Libraries\UtilFunctions\count( $arrstrActiveDirectoryUserGroups ) || \Psi\Libraries\UtilFunctions\count( $arrstrCommonGroups ) != \Psi\Libraries\UtilFunctions\count( $arrstrCompanyUserGroups ) ) {

			$arrstrAssignGroupsToUser = array_intersect( $arrstrCompanyGroups, array_diff( $arrstrActiveDirectoryUserGroups, $arrstrCompanyUserGroups ) );

			if( true == valArr( $arrstrAssignGroupsToUser ) ) {
				$this->assignUserToGroups( array_keys( $arrstrAssignGroupsToUser ), $objCompanyUser, $objDatabase );
				$arrstrAssignedRemovedGroups['assigned'] = $arrstrAssignGroupsToUser;
			}

			$arrstrRemoveUserFromGroups = array_diff( $arrstrCompanyUserGroups, $arrstrActiveDirectoryUserGroups );

			if( true == valArr( $arrstrRemoveUserFromGroups ) ) {
				$this->removeUserFromGroups( array_keys( $arrstrRemoveUserFromGroups ), $objCompanyUser, $objDatabase );
				$arrstrAssignedRemovedGroups['removed'] = $arrstrRemoveUserFromGroups;
			}
		}

		return $arrstrAssignedRemovedGroups;

	}

	public function syncPropertyPermissions( $arrstrPropertyGroupNames, $strSystemCode, $objDatabase ) {

		$arrmixCompanyUserPropertyGroups = ( array ) \Psi\Eos\Entrata\CPropertyGroups::createService()->fetchPropertyGroupsDetailsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $arrstrPropertyGroupNames, $strSystemCode, $objDatabase );
		$arrobjCompanyUserPropertyGroups = ( array ) \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchCompanyUserPropertyGroupsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$objCompanyUserPropertyGroup = new CCompanyUserPropertyGroup();
		$objCompanyUserPropertyGroup->setCid( $this->getCid() );
		$objCompanyUserPropertyGroup->setCompanyUserId( $this->getId() );
		$arrobjNewCompanyUserPropertyGroups = $arrstrNewCompanyUserPropertyGroupsName = [];
		if( true == valArr( $arrmixCompanyUserPropertyGroups ) ) {
			foreach( $arrmixCompanyUserPropertyGroups as $arrmixCompanyUserPropertyGroup ) {
				$intCompanyUserPropertyGroupId = $arrmixCompanyUserPropertyGroup['company_user_property_group_id'];
				if( true == is_null( $arrmixCompanyUserPropertyGroup['company_user_id'] ) ) {
					$objTempCompanyUserPropertyGroup = clone $objCompanyUserPropertyGroup;
					$objTempCompanyUserPropertyGroup->setPropertyGroupId( $arrmixCompanyUserPropertyGroup['property_group_id'] );
					$arrobjNewCompanyUserPropertyGroups[] = $objTempCompanyUserPropertyGroup;
					$arrstrNewCompanyUserPropertyGroupsName[] = $arrmixCompanyUserPropertyGroup['property_group_name'];
				}

				if( true == in_array( \Psi\CStringService::singleton()->strtolower( $arrmixCompanyUserPropertyGroup['property_group_name'] ), $arrstrPropertyGroupNames ) ) {
					unset( $arrobjCompanyUserPropertyGroups[$intCompanyUserPropertyGroupId] );
				}
			}
		}
		switch( NULL ) {

			default:

				$objDatabase->begin();

				if( true == valArr( $arrobjCompanyUserPropertyGroups ) && false == \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->bulkDelete( $arrobjCompanyUserPropertyGroups, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjNewCompanyUserPropertyGroups ) && false == \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->bulkInsert( $arrobjNewCompanyUserPropertyGroups, $this->getId(), $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}
				if( false == $this->createPropertyGroupChangeLog( $arrobjCompanyUserPropertyGroups, $arrstrNewCompanyUserPropertyGroupsName, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}
				if( false == \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->rebuildPropertyGroupPermissionsByCidByCompanyUserIds( $this->getCid(), [ $this->getId() ], $this->getId(), $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}
				$objDatabase->commit();
				return true;
		}

		return false;
	}

	/**
	 * Sync user group assignment after login through SAML.
	 */

	public function syncSamlUserGroups( $arrstrSamlUserGroups, $objDatabase ) {

		$arrstrCompanyPreferences = CCompanyPreferences::createService()->fetchCompanyPreferencesValueByKeysByCid( [ 'SAML_ALLOWED_GROUPS', 'SAML_GROUP_IDENTIFIER' ], $this->getCid(), $objDatabase );

		$strGroupIds              = getArrayElementByKey( 'SAML_ALLOWED_GROUPS', $arrstrCompanyPreferences );
		$boolCustomIdentifier     = ( 'custom_identifier' === getArrayElementByKey( 'SAML_GROUP_IDENTIFIER', $arrstrCompanyPreferences ) );

		if( false == valArr( $arrstrCompanyPreferences ) || true == is_null( $strGroupIds ) || false == valArr( $arrstrSamlUserGroups ) ) {
			return;
		}

		$arrstrSamlUserGroups  = array_map( 'strtolower', $arrstrSamlUserGroups );
		$arrstrCompanyGroupIds = [];
		if( '-1' != $arrstrCompanyPreferences['SAML_ALLOWED_GROUPS'] ) {
			$arrstrCompanyGroupIds = array_filter( explode( ',', $arrstrCompanyPreferences['SAML_ALLOWED_GROUPS'] ) );
		}

		$arrmixCompanyGroups = ( array ) \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyGroupsByIdsByComapnyUserIdByCid( $arrstrCompanyGroupIds, $this->getId(), $this->getCid(), $objDatabase );

		$arrstrCompanyGroups     = [];
		$arrstrCompanyUserGroups = [];

		if( true == valArr( $arrmixCompanyGroups ) ) {
			foreach( $arrmixCompanyGroups as $arrmixCompanyGroup ) {

				$strGroupValue = ( false == $boolCustomIdentifier ) ? $arrmixCompanyGroup['company_group_name'] : \Psi\CStringService::singleton()->strtolower( $arrmixCompanyGroup['remote_primary_key'] );
				$arrstrCompanyGroups[$arrmixCompanyGroup['company_group_id']] = $strGroupValue;

				if( $arrmixCompanyGroup['company_user_id'] > 0 ) {
					$arrstrCompanyUserGroups[$arrmixCompanyGroup['company_user_group_id']] = $strGroupValue;
				}
			}
		}

		$arrstrCompanyGroups = array_filter( $arrstrCompanyGroups );
		// Assign the allowed groups to the user
		$arrstrAssignGroupsToUser = array_intersect( $arrstrCompanyGroups, array_diff( $arrstrSamlUserGroups, $arrstrCompanyUserGroups ) );
		if( true == valArr( $arrstrAssignGroupsToUser ) ) {
			$this->assignUserToGroups( array_keys( $arrstrAssignGroupsToUser ), $this, $objDatabase );
		}

		// Remove user group assignment, which are not allowed
		$arrstrRemoveUserFromGroups = array_diff( $arrstrCompanyUserGroups, $arrstrSamlUserGroups );
		if( true == valArr( $arrstrRemoveUserFromGroups ) ) {
			$this->removeUserFromGroups( array_keys( $arrstrRemoveUserFromGroups ), $this, $objDatabase );
		}

	}

	public function syncSamlProperties( $arrstrSamlPropertyGroups, $objDatabase ) {
		$strSystemCode           = NULL;
		$strAllPropertyGroupName = \Psi\CStringService::singleton()->strtolower( CPropertyGroup::ALL_PROPERTIES );

		if( true == in_array( $strAllPropertyGroupName, $arrstrSamlPropertyGroups ) ) {
			$arrstrPropertyGroupNames[] = $strAllPropertyGroupName;
			$strSystemCode              = CPropertyGroup::SYSTEM_CODE_ALL;
		} else {
			$arrstrPropertyGroupNames = $arrstrSamlPropertyGroups;
		}
		$this->syncPropertyPermissions( $arrstrPropertyGroupNames, $strSystemCode, $objDatabase );
	}

	public function assignUserToGroups( $arrintCompanyGroupIds, $objCompanyUser, $objDatabase ) {
		$arrobjCompanyUserGroups = [];

		foreach( $arrintCompanyGroupIds as $intCompanyGroupId ) {

			$objCompanyUserGroup = $objCompanyUser->createCompanyUserGroup();
			$objCompanyUserGroup->setCompanyGroupId( $intCompanyGroupId );
			$arrobjCompanyUserGroups[] = $objCompanyUserGroup;
		}

		switch( NULL ) {

			default:

				$objDatabase->begin();
				foreach( $arrobjCompanyUserGroups as $objCompanyUserGroup ) {
					if( false == $objCompanyUserGroup->insert( $objCompanyUser->getId(), $objDatabase ) ) {
						$objDatabase->rollback();
						break 2;
					}
				}
				if( false == \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->rebuildPropertyGroupPermissionsByCidByCompanyUserIds( $this->getCid(), [ $objCompanyUser->getId() ], $objCompanyUser->getId(), $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				$objDatabase->commit();
		}

	}

	public function removeUserFromGroups( $arrintCompanyUserGroupIds, $objCompanyUser, $objDatabase ) {

		$arrobjCompanyUserGroups = \Psi\Eos\Entrata\CCompanyUserGroups::createService()->fetchCompanyUserGroupsByIdsByCid( $arrintCompanyUserGroupIds, $objCompanyUser->getCid(), $objDatabase );

		switch( NULL ) {

			default:

				$objDatabase->begin();

				foreach( $arrobjCompanyUserGroups as $objCompanyUserGroup ) {
					if( false == $objCompanyUserGroup->delete( $objCompanyUser->getId(), $objDatabase ) ) {
						$objDatabase->rollback();
						break 2;
					}
				}
				if( false == \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->rebuildPropertyGroupPermissionsByCidByCompanyUserIds( $this->getCid(), [ $objCompanyUser->getId() ], $objCompanyUser->getId(), $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				$objDatabase->commit();
		}
	}

	public static function setCompanyEmployeeDetails( $arrmixValues, $objCompanyEmployee ) {

		$strPhoneNumber = '';

		if( true == isset( $arrmixValues['display_name_format'] ) && true == valStr( $arrmixValues['display_name_format'] ) ) {
			$intDisplayNameFormatVal    = $arrmixValues['display_name_format'];
			$arrstrDisplayFormat        = CCompanyPreference::createService()->getActiveDirectoryDisplayNameFormat();
			$strSeparator               = $arrstrDisplayFormat[$intDisplayNameFormatVal]['separator'];
			$strSequence                = $arrstrDisplayFormat[$intDisplayNameFormatVal]['sequence'];
			$arrstrDisplayName          = explode( $strSeparator, $arrmixValues['display_name'] );

			if( 'F' == $strSequence[0] && 'L' == $strSequence[1] ) {
				$arrmixValues['first_name']     = trim( $arrstrDisplayName[0] );
				$arrmixValues['last_name']      = trim( $arrstrDisplayName[1] );
			} else {
				$arrmixValues['first_name']     = trim( $arrstrDisplayName[1] );
				$arrmixValues['last_name']      = trim( $arrstrDisplayName[0] );
			}
		}

		if( true == valStr( $arrmixValues['last_name'] ) && $arrmixValues['last_name'] != $objCompanyEmployee->getNameLast() ) {
			$objCompanyEmployee->setNameLast( $arrmixValues['last_name'] );
		}

		if( true == valStr( $arrmixValues['first_name'] ) && $arrmixValues['first_name'] != $objCompanyEmployee->getNameFirst() ) {
			$objCompanyEmployee->setNamefirst( $arrmixValues['first_name'] );
		}

		if( true == valStr( $arrmixValues['email_address'] ) && $arrmixValues['email_address'] != $objCompanyEmployee->getEmailAddress() ) {
			$objCompanyEmployee->setEmailAddress( $arrmixValues['email_address'] );
		}

		if( true == valId( $arrmixValues['telephone_number'] ) ) {
			$strPhoneNumber       = $arrmixValues['telephone_number'];
			$intPhoneNumberTypeId = CPhoneNumberType::OFFICE;
		} else {
			if( true == valId( $arrmixValues['phone_number_home'] ) ) {
				$strPhoneNumber       = $arrmixValues['phone_number_home'];
				$intPhoneNumberTypeId = CPhoneNumberType::HOME;
			} else {
				if( true == valId( $arrmixValues['mobile_number'] ) ) {
					$strPhoneNumber       = $arrmixValues['mobile_number'];
					$intPhoneNumberTypeId = CPhoneNumberType::MOBILE;
				}
			}
		}

		$intPhoneNumberTypeId = ( true == isset( $intPhoneNumberTypeId ) ) ? $intPhoneNumberTypeId : '';

		if( true == valStr( $strPhoneNumber ) ) {
			$objCompanyEmployee->setPhoneNumber1( $strPhoneNumber );
			$objCompanyEmployee->setPhoneNumber1TypeId( $intPhoneNumberTypeId );
		}

		if( true == array_key_exists( 'employee_code', $arrmixValues ) ) {
			$objCompanyEmployee->setEmployeeCode( \Psi\CStringService::singleton()->strtolower( $arrmixValues['employee_code'] ) );
		}

		if( true == array_key_exists( 'is_sso_user', $arrmixValues ) ) {
			$objCompanyEmployee->setIsSsoUser( $arrmixValues['is_sso_user'] );
		}

		return $objCompanyEmployee;
	}

	public function logUserDetails( $strType, $arrstrAdditional = [] ) {
		$arrmixParams = [
			'cid'             => $this->getCid(),
			'user_id'         => $this->getId(),
			'track'           => CConfig::get( 'cluster_name' ),
			'url'             => ( true === CONFIG_IS_SECURE_URL ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . ( $_SERVER['SERVER_PORT'] != 80 && $_SERVER['SERVER_PORT'] != 443 ? ':' . $_SERVER['SERVER_PORT'] : '' ) . $_SERVER['REQUEST_URI'],
			'server_name'     => gethostname(),
			'timestamp'       => date( 'Y-m-d H:i:s T' ),
			'HTTP_REFERER'    => ( true == isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : '' ),
			'HTTP_USER_AGENT' => ( true == isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '' ),
			'REQUEST_METHOD'  => ( true == isset( $_SERVER['REQUEST_METHOD'] ) ? $_SERVER['REQUEST_METHOD'] : '' ),
			'SERVER_ADDR'     => ( true == isset( $_SERVER['SERVER_ADDR'] ) ? $_SERVER['SERVER_ADDR'] : '' ),
		];

		$arrmixParams = array_merge( $arrmixParams, ( array ) $arrstrAdditional );

		CPsLogger::logToLogstash( $strType, $arrmixParams );
	}

	public function logout() {

		$this->m_boolIsLoggedIn = false;

		return true;
	}

	public function resetPassword( $objClientDatabase, $objConnectDatabase, $objEmailDatabase, $boolIsFromVacancy = false ) {

		// Reset the password for the user and notify him via email
		$this->setPassword( $this->generateRandomPassword( $objClientDatabase ) );

		// Generate email contents
		$boolIsMailSent = true;

		$strEmailContents = $this->generateEmailContents( $this->m_objClient, $this->m_strPassword, $objClientDatabase, $boolIsFromVacancy );

		$boolIsMailSent &= $this->sendEmail( $this->getEmailAddress(), $strEmailContents, $objEmailDatabase );

		if( false == $boolIsMailSent ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'email_address', __( 'Failed to send email to {%s, email_address}', [ 'email_address' => $this->getEmailAddress() ] ) ) );

			return false;
		} elseif( false == $this->update( $this->getId(), $objClientDatabase, $objConnectDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'email_address', __( 'Failed to reset password for email {%s, email_address}', [ 'email_address' => $this->getEmailAddress() ] ) ) );

			return false;
		}

		return true;
	}

	public function logAuthentication( $objDatabase, $intAuthenticationLogTypeId = CAuthenticationLogType::ENTRATA, $strSessionId = NULL ) {

		if( true == valId( $this->getAuthenticationLogId() ) ) {

			$objAuthenticationLog = CAuthenticationLogs::fetchAuthenticationLogByCompanyUserIdByIdByCid( $this->getId(), $this->getAuthenticationLogId(), $this->getCid(), $objDatabase, $strSessionId );

			if( true == valObj( $objAuthenticationLog, 'CAuthenticationLog' ) ) {
				$objAuthenticationLog->setLogoutDatetime( 'NOW()' );
			}
		} else {
			$objAuthenticationLog = $this->createAuthenticationLog( $strSessionId );
			$objAuthenticationLog->setAuthenticationLogTypeId( $intAuthenticationLogTypeId );
		}

		if( false == is_null( $objAuthenticationLog ) ) {
			if( false == $objAuthenticationLog->insertOrUpdate( $this->getId(), $objDatabase ) ) {
				trigger_error( 'Failed to insert the authentication log for the Company User : ' . $this->m_intId, E_USER_WARNING );
			}
			$this->setAuthenticationLogId( $objAuthenticationLog->getId() );
		}

		return true;
	}

	public function sqlCid() {
		if( true == valObj( $this->m_objClient, 'CClient' ) ) {
			return $this->m_objClient->sqlId();
		} else {
			return parent::sqlCid();
		}
	}

	public static function generateEmailContentsOnCompanyUserInsert( $objClient, $strPassword, $objDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		// Email contents
		$strContents = '<div align="left">';
		$strContents .= __( 'Your entrata password has been set to : <b> {%s, password} </b><br>', [ 'password' => $strPassword ] );
		$strContents .= __( 'This is a one time process <br>' );
		$strContents .= __( 'Please note that your Username/Userid remains unchanged.<br>' );
		$strContents .= __( 'You would be asked to change password at your first login.<br>' );
		$strContents .= __( 'To login please click : <a href="{%s, url}/?module=authenticationxxx&action=view_login">here</a><br>', [ 'url' => 'https://' . $objClient->getRwxDomain() . '.' . CONFIG_ENTRATA_SUFFIX ] );
		$strContents .= '</div>';

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_WORKS, false );

		$objSmarty->assign_by_ref( 'contents', $strContents );
		$objSmarty->assign_by_ref( 'company_media_file', $objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'base_uri',          CONFIG_HOST_PREFIX . CONFIG_ENTRATA_SUFFIX . '/' );
		$objSmarty->assign( 'media_library_uri', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'image_url',	     CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strEmailContents = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/user_retrieve_password/user_retrieve_password_email.tpl' );

		return $strEmailContents;
	}

	public function generateEmailContents( $objClient, $strPassword, $objDatabase, $boolIsFromVacancy ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		if( true == valObj( $objClient, 'CClient' ) ) {
			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		if( false == $boolIsFromVacancy ) {
			$strUrl = '.' . CONFIG_ENTRATA_SUFFIX . '/?module=myprofilexxx';
		} else {
			$strUrl = '.' . CONFIG_VACANCY_SUFFIX . '/authentication';
		}

		// Email contents
		$strContents = '<div style="padding:30px;">';
		$strContents .= '<p style="margin:0 0 20px;">';
		$strContents .= __( 'Hi {%s, first_name},<br><br>There was recently a request to reset your {%s, company_name} Admin password.', [ 'first_name' => $this->getNameFirst(), 'company_name' => CONFIG_COMPANY_NAME ] );
		$strContents .= '</p>';
		$strContents .= '<div style="background-color:#e8e8e8;border-radius:7px;border:1px solid #d0d0d0;padding:10px 15px;color:#404040;font-size:16px;line-height:26px;">';
		$strContents .= __( 'Your temporary password is: <strong> {%s, password} </strong><br>', [ 'password' => $strPassword ] );
		$strContents .= __( 'Please change the password by going to : <a href="{%s, url}" style="text-decoration:none; color:#1a70a6;">My Profile</a> section after logging.', [ 'url' => 'https://' . $objClient->getRwxDomain() . $strUrl ] );
		$strContents .= '</div>';
		$strContents .= '<p style="margin:20px 0 40px;">';
		$strContents .= __( 'If you have received this mail in error, it is likely that another user entered your email address by mistake while trying to reset a password. If you did not initiate the request, you do not need to take any further action and can safely disregard this email.' );
		$strContents .= '</p>';
		$strContents .= '<p style="margin:0;">';
		$strContents .= __( 'Note: Please do not reply back to this mail. This is sent from an unattended mail box.' );
		$strContents .= '</p>';
		$strContents .= '</div>';

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_INTERFACES_RESIDENT_WORKS, false );

		$objSmarty->assign_by_ref( 'contents', $strContents );
		$objSmarty->assign_by_ref( 'company_media_file', $objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_SUFFIX . '/' );
		$objSmarty->assign( 'media_library_uri', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX', CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'image_url',	    CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strEmailContents = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/user_retrieve_password/user_retrieve_password_email.tpl' );

		return $strEmailContents;
	}

	public function sendEmail( $strToEmailAddress, $strEmailContents, $objDatabase ) {

		$objSystemEmail = new CSystemEmail();
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::ENTRATA_PASSWORD );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setToEmailAddress( $strToEmailAddress );
		$objSystemEmail->setSubject( __( 'Password Change Notification' ) );
		$objSystemEmail->setHtmlContent( $strEmailContents );
		$objSystemEmail->setIsSelfDestruct( 1 );
		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		if( false == $objSystemEmail->insert( $this->getId(), $objDatabase, false, false ) ) {
			return false;
		}

		return true;
	}

	public function generateGoogleAuthenticatorSecretKey() {
		return ( false == is_null( $this->m_strUsername ) ) ? \Psi\CStringService::singleton()->strtoupper( str_replace( [ 0, 1, 8, 9 ], [ 'A', 'D', 'Z', 'Q' ], md5( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $this->m_strUsername, CONFIG_SODIUM_KEY_GOOGLE_AUTHENTICATOR_SECRET ) ) ) ) : NULL;
	}

	public function forceIntegration( $objDatabase ) {

		if( false == valStr( $this->getRemotePrimaryKey() ) ) {
			return true;
		}

		$arrstrRemotePrimaryKeyParts = explode( self::REMOTE_FOREIGN_KEY_DELIMITER, $this->getRemotePrimaryKey() );
		$objIntegrationDatabase      = \Psi\Eos\Entrata\CIntegrationDatabases::createService()->fetchCustomIntegrationDatabaseByIdByCid( $arrstrRemotePrimaryKeyParts[0], $this->getCid(), $objDatabase );

		if( true == valObj( $objIntegrationDatabase, 'CIntegrationDatabase' ) ) {
			$objGenericWorker = CIntegrationFactory::createWorkerFromIntegrationDatabase( $objIntegrationDatabase, CIntegrationService::RETRIEVE_COMPANY_USER, $this->getId(), $objDatabase );
			$objGenericWorker->setCompanyUser( $this );
			$objGenericWorker->setDatabase( $objDatabase );
			$objGenericWorker->process();
		}
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objConnectDatabase = NULL, $boolIsFromClientAdmin = false, $boolIsInsertCompanyUserPreferences = true ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objConnectDatabase, $boolIsFromClientAdmin = false, $boolIsInsertCompanyUserPreferences );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objConnectDatabase );
		}
	}

	/**
	 * @return bool
	 * @throws Throwable
	 */
	public function syncToSsoViaQueue() : bool {
		try {
			$objSyncCompanyUsersToSsoPublisher = new CSyncCompanyUsersToSsoPublisher();
			$objSyncCompanyUsersToSsoPublisher->setCompanyUserId( $this->getId() );
			$objSyncCompanyUsersToSsoPublisher->setCid( $this->getCid() );
			$objSyncCompanyUsersToSsoPublisher->setPassword( $this->getPassword() );
			$objSyncCompanyUsersToSsoPublisher->setCluster( CConfig::get( 'cluster_name' ) );
			$objSyncCompanyUsersToSsoPublisher->setConnectionConfig( $this->loadAmqpConnectionConfig() );
			$objSyncCompanyUsersToSsoPublisher->syncToSso();
		} catch( Throwable $objSsoException ) {
			throw $objSsoException;
		}
        return true;
	}

	/**
	 * @return bool
	 * @throws Throwable
	 */
	public function syncToSso() : bool {

		if( false == defined( 'CONFIG_PS_PRODUCT_ID' ) || CPsProduct::ENTRATA != CONFIG_PS_PRODUCT_ID ) {
			return true;
		}

		$arrstrUserChangedColumns   = [ 'Username', 'PasswordEncrypted', 'IsDisabled' ];
		$boolUpdateRequiredUser     = ( true !== empty( array_intersect( array_keys( $this->getChangedColumns() ), $arrstrUserChangedColumns ) ) );

		// Update in Sso if it is a Sso User
		if( $boolUpdateRequiredUser && true == $this->getIsSsoUser() ) {
			try {
				$this->syncCompanyUserToSso();
			} catch( Throwable $objSsoException ) {
				throw $objSsoException;
			}
		}

		return true;
	}

	public function insert( $intCompanyUserId, $objDatabase, $objConnectDatabase = NULL, $boolIsFromClientAdmin = false, $boolIsInsertCompanyUserPreferences = true, $boolIsBlueMoonUser = false ) {

		if( false == parent::insert( $intCompanyUserId, $objDatabase ) ) {
			return false;
		}

		if( false == $boolIsFromClientAdmin && false == in_array( $this->getCompanyUserTypeId(), [ CCompanyUserType::BROKER_PORTAL, \CCompanyUserType::RESERVATION_HUB, \CCompanyUserType::TENANT_PORTAL, \CCompanyUserType::CUSTOMER ] ) ) {
			// We need to insert this user into global users table also: We are not inserting client admin users into global users
			$objGlobalUser = $this->createGlobalUser( $boolIsBlueMoonUser );
			if( false == $objGlobalUser->validate( 'validate_user_type_id_and_username' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Username already in use.' ) ) );
				return false;
			}
			if( false == $objGlobalUser->insert( $intCompanyUserId, $objConnectDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Global user failed to insert.' ) ) );
				return false;
			}
		}

		// Sync user to Sso using Queue. Consider CompanyUserType Entrata only. Skip ClientAdmin, Bluemoon & Training Db Users.
		$objCompanyPreference   = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'IS_INTERNAL_SSO', $this->getCid(), $objDatabase );
		$boolIsInternalSso      = ( true == valObj( $objCompanyPreference, CCompanyPreference::class ) ) ? $objCompanyPreference->getValue() : false;
		if( false == $this->getIsScimUser() && CCompanyUserType::ENTRATA == $this->getCompanyUserTypeId() && false == $boolIsFromClientAdmin && false == $boolIsBlueMoonUser && false == CConfig::get( 'training_mode' ) ) {
			CNewRelic::createService()->addCustomParameters( [ 'debug.insert_company_user' => true ] );

			if( true == $boolIsInternalSso ) {
				try {
					$this->syncToSsoViaQueue();
				} catch( Throwable $objSsoException ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $objSsoException->getMessage(), $objSsoException->getCode() ) );
					return false;
				}
			}
		}

		// Not inserting Preference for Admin User. esp ClientAdmin User
		if( CClients::$c_intNullCid == $this->getCid() ) {
			return true;
		}

		return true;
	}

	// We are overridding this function to correctly set client id for PS Admin Users

	public function update( $intCurrentUserId, $objClientDatabase, $objConnectDatabase = NULL, $boolIsBlueMoonUser = false ) {

		if( false == in_array( $this->getCompanyUserTypeId(), CCompanyUserType::$c_arrintPsiUserTypeIds ) ) {
			// $this->setCid( CClients::$c_intNullCid );
			// } else {
			// We need to insert this user into global users table also:

			if( true == $boolIsBlueMoonUser ) {
				$objGlobalUser = $this->fetchGlobalUserByGlobalUserTypeId( CGlobalUserType::BLUEMOON_USER, $objConnectDatabase );
			} else {
				$objGlobalUser = $this->fetchGlobalUser( $objConnectDatabase );
			}

			if( false == valObj( $objGlobalUser, 'CGlobalUser' ) ) {
				$objGlobalUser = $this->createGlobalUser( $boolIsBlueMoonUser );
			} else {
				$objGlobalUser->setWorksId( $this->getId() );
				$objGlobalUser->setCid( $this->getCid() );
				$objGlobalUser->setUsername( $this->getUsername() );
				$objGlobalUser->setPasswordEncrypted( $this->getPasswordEncrypted() );
				$objGlobalUser->setIsDisabled( $this->getIsDisabled() );

				if( true == $boolIsBlueMoonUser ) {
					$objGlobalUser->setBluemoonPassword( $this->m_strPassword );
				}
			}

			if( false == $objGlobalUser->insertOrUpdate( $intCurrentUserId, $objConnectDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Global user failed to update.' ) ) );
			}
		}

		$intSsoGuid  = $this->getSsoGuid();  //check user come from SCIM
		// Sync to Sso directly if Username / Password / IsDisbled changed.
		$objCompanyPreference   = \Psi\Eos\Entrata\CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'IS_INTERNAL_SSO', $this->getCid(), $objClientDatabase );
		$boolIsInternalSso      = ( true == valObj( $objCompanyPreference, CCompanyPreference::class ) ) ? $objCompanyPreference->getValue() : false;
		if( false == $this->getIsScimUser() && CCompanyUserType::ENTRATA == $this->getCompanyUserTypeId() && false == $boolIsBlueMoonUser && false == CConfig::get( 'training_mode' ) ) {
			CNewRelic::createService()->addCustomParameters( [ 'debug.update_company_user' => true ] );
			if( true == $boolIsInternalSso ) {
				try {
					$this->syncToSso();
				} catch( Throwable $objSsoException ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $objSsoException->getMessage(), $objSsoException->getCode() ) );
					return false;
				}
			}
		}

		return parent::update( $intCurrentUserId, $objClientDatabase );
	}

	public function checkForUpdateContactInfoRequest( $objClientDatabase ) {

		$arrstrCompanyPreferences = CCompanyPreferences::createService()->fetchCompanyPreferencesValueByKeysByCid( [ 'USE_USERNAME_INSTEAD_OF_EMAIL' ], $this->getCid(), $objClientDatabase );

		$intCompanyPreferenceUseUserName = ( true == valArr( $arrstrCompanyPreferences ) ) ? ( int ) $arrstrCompanyPreferences['USE_USERNAME_INSTEAD_OF_EMAIL'] : 1;

		$objCompanyEmployee = $this->fetchCompanyEmployee( $objClientDatabase );

		if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
			if( true == is_null( $objCompanyEmployee->getNameFirst() ) || true == is_null( $objCompanyEmployee->getNameLast() ) || true == is_null( $objCompanyEmployee->getPhoneNumber1() ) || ( 1 == $intCompanyPreferenceUseUserName && true == is_null( $objCompanyEmployee->getEmailAddress() ) ) ) {
				return false;
			}
		}

		return true;
	}

	public function getLatestPasswordRotationDays( $objDatabase ) {

		$arrmixRotationDays = CAuthenticationLogs::fetchLatestPasswordRotationDaysByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		return $arrmixRotationDays;

	}

	public function checkPasswordsInAuthenticationLogs( $objDatabase, $arrobjPasswordPolicyCompanyPreferences ) {

		$boolIsPasswordExists      = false;
		$intPasswordRotationNumber = self::PASSWORD_ROTATION_NUMBER;

		if( true == valArr( $arrobjPasswordPolicyCompanyPreferences ) && ( true == array_key_exists( 'PASSWORD_REQUIREMENTS', $arrobjPasswordPolicyCompanyPreferences ) && 'custom' == $arrobjPasswordPolicyCompanyPreferences['PASSWORD_REQUIREMENTS']->getValue() ) && ( true == array_key_exists( 'PREVENT_PASSWORD_REUSE_NUMBER', $arrobjPasswordPolicyCompanyPreferences ) && false == is_null( $arrobjPasswordPolicyCompanyPreferences['PREVENT_PASSWORD_REUSE_NUMBER']->getValue() ) ) ) {
			$intPasswordRotationNumber = $arrobjPasswordPolicyCompanyPreferences['PREVENT_PASSWORD_REUSE_NUMBER']->getValue();
		}

		$arrstrPreviousPasswords = CAuthenticationLogs::fetchPreviousPasswordsByCompanyUserIdByRotationNumberByCid( $this->getId(), $intPasswordRotationNumber, $this->getCid(), $objDatabase );

		if( true == valArr( $arrstrPreviousPasswords ) ) {
			foreach( $arrstrPreviousPasswords as $arrstrPreviousPassword ) {
				if( $this->m_strPasswordEncrypted == $arrstrPreviousPassword->getPreviousPassword() ) {
					$boolIsPasswordExists = true;
					break;
				}
			}
		}

		if( true == $boolIsPasswordExists ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_password', __( 'Password should be different from previously used {%d, 0} passwords.', [ $intPasswordRotationNumber ] ) ) );
		}

		return $boolIsPasswordExists;
	}

	public function fetchAssignedPropertyIds( $objDatabase, $boolIsDisabled = false ) {

		$arrintPropertyIds = \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertyIdsByCompanyUserIdBySessionDataByCid( $this->getId(), $boolIsDisabled, $this->getCid(), $objDatabase );
		if( true == valArr( $arrintPropertyIds ) ) {
			return $arrintPropertyIds;
		}

		return false;
	}

	public function fetchSimpleEnabledPropertyIds( $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimpleEnabledPropertyIdsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyIdAndNameByPropertyIds( $arrintPropertyIds, $objDatabase, $boolShowAllProperties = false, $boolFetchPropertyName = false, $boolIncludeDisabledData = false, $boolShowDisabledData = false ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchSimplePropertiesByPropertyIdsByCidByDisabledData( $arrintPropertyIds, $this->getCid(), $objDatabase, $boolShowAllProperties, $boolFetchPropertyName, $boolIncludeDisabledData, $boolShowDisabledData );
	}

	public function fetchPermissionsByModuleId( $intModuleId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchPermissionsByModuleId( $this, $intModuleId, $objDatabase );
	}

	public function generateRandomPassword( $objDatabase = NULL, $strCharacterSet = NULL, $boolShuffle = true ) {

		$arrobjPasswordPolicyCompanyPreferences = ( false == is_null( $objDatabase ) ) ? rekeyObjects( 'key', CCompanyPreferences::createService()->fetchCompanyPreferencesByKeysByCid( [ 'PASSWORD_REQUIREMENTS', 'MINIMUM_PASSWORD_LENGTH', 'REQUIRE_AT_LEAST_ONE_NON_ALPHANUMERIC_CHARACTER', 'NON_ALPHANUMERIC_CHARACTERS' ], $this->m_intCid, $objDatabase ) ) : [];
		$arrstrPasswordPolicies                 = [ 'ABCDEFGHJKMNPQRSTUVWXYZ', 'abcdefghijkmnopqrstuvwxyz', '23456789' ];
		$strCharacterSet                        = $strCharacterSet ?? 'ABCDEFGHJKMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789!@#$%^&*()_+-=[]{}|';
		$strPassword                            = NULL;

		if( true == valArr( $arrobjPasswordPolicyCompanyPreferences ) && true == array_key_exists( 'PASSWORD_REQUIREMENTS', $arrobjPasswordPolicyCompanyPreferences ) && 'custom' == $arrobjPasswordPolicyCompanyPreferences['PASSWORD_REQUIREMENTS']->getValue() ) {

			$intPasswordLength = $arrobjPasswordPolicyCompanyPreferences['MINIMUM_PASSWORD_LENGTH']->getValue();

			if( true == array_key_exists( 'REQUIRE_AT_LEAST_ONE_NON_ALPHANUMERIC_CHARACTER', $arrobjPasswordPolicyCompanyPreferences ) && '1' == $arrobjPasswordPolicyCompanyPreferences['REQUIRE_AT_LEAST_ONE_NON_ALPHANUMERIC_CHARACTER']->getValue() ) {
				if( true == array_key_exists( 'NON_ALPHANUMERIC_CHARACTERS', $arrobjPasswordPolicyCompanyPreferences ) && false == is_null( $arrobjPasswordPolicyCompanyPreferences['NON_ALPHANUMERIC_CHARACTERS']->getValue() ) ) {
					$arrstrPasswordPolicies[] = $arrobjPasswordPolicyCompanyPreferences['REQUIRE_AT_LEAST_ONE_NON_ALPHANUMERIC_CHARACTER']->getValue();
				} else {
					$arrstrPasswordPolicies[] = '!@#$%^&*()_+-=[]{}|';
				}
			}
		} else {
			$intPasswordLength = self::DEFAULT_PASSWORD_LENGTH;
		}

		foreach( $arrstrPasswordPolicies as $arrstrPasswordPolicy ) {
			$strPassword .= $arrstrPasswordPolicy[rand( 0, strlen( $arrstrPasswordPolicy ) - 1 )];
		}

		$strPassword .= \Psi\CStringService::singleton()->substr( str_shuffle( $strCharacterSet ), 0, $intPasswordLength - \Psi\Libraries\UtilFunctions\count( $arrstrPasswordPolicies ) );

		if( $boolShuffle ) $strPassword = str_shuffle( $strPassword );

		return $strPassword;
	}

	public function syncNotificationLogs( $objCompanyUser, $objDatabase ) {

		if( false == $objCompanyUser->getIsPSIUser() ) {

			$arrintNotifications = \Psi\Eos\Entrata\CNotifications::createService()->fetchUnloggedNotificationDetailsByCompanyUserByCid( $objCompanyUser, $this->getCid(), $objDatabase );

			if( true == valArr( $arrintNotifications ) ) {

				switch( NULL ) {
					default:

						foreach( $arrintNotifications as $intNotificationId ) {

							$objNotificationLog = new CNotificationLog();
							$objNotificationLog->setCid( $this->getCid() );
							$objNotificationLog->setCompanyUserId( $objCompanyUser->getId() );
							$objNotificationLog->setNotificationId( $intNotificationId );

							if( false == $objNotificationLog->validate( VALIDATE_INSERT, $objDatabase ) ) {
								break;
							}

							if( false == $objNotificationLog->insert( $objCompanyUser->getId(), $objDatabase ) ) {
								break;
							}
						}
				}
			}
		}
	}

	public function fetchGlobalUsersByGlobalUserTypeIds( $arrintGlobalUserTypeIds, $objConnectDatabase ) {
		return CGlobalUsers::fetchGlobalUsersByCidByWorksIdByGlobalUserTypeIds( $this->getCid(), $this->getId(), $arrintGlobalUserTypeIds, $objConnectDatabase );
	}

	public function fetchGlobalUserByGlobalUserTypeId( $intGlobalUserTypeId, $objConnectDatabase ) {
		return CGlobalUsers::fetchGlobalUserByCidByWorksIdByGlobalUserTypeId( $this->getCid(), $this->getId(), $intGlobalUserTypeId, $objConnectDatabase );
	}

	/**
	 * sendResetPasswordLink
	 *
	 * @param string $strDomain
	 * @param object $objClientDatabase
	 * @param object $objEmailDatabase
	 * @param array  $arrstrFilter e.g. $arrstrFilter = array( 'is_active_directory_enabled' => true/false,
	 *                             'set_password' => '1', 'link_validity' => '7 days' );
	 *
	 * @return true/false
	 */

	public function sendResetPasswordLink( $strDomain, $objClientDatabase, $objEmailDatabase, $arrstrFilter = [] ) {
		$boolIsValid = $this->validate( 'validate_reset_password' );
		$strMsg = ( true == getArrayElementByKey( 'reset_password', $arrstrFilter ) ) ? 'Reset' : 'Forgot';
		if( false == $boolIsValid ) {
			return false;
		}

		if( true == getArrayElementByKey( 'saml_sso_restrict_entrata_login', $arrstrFilter ) ) {
			$objCompanyEmployee = $this->fetchCompanyEmployee( $this->m_objDatabase );
			$boolIsSsoEnabledSpecificUser = false;
			if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
				$boolIsSsoEnabledSpecificUser = $objCompanyEmployee->getIsSsoUser();
			}
			$boolCheckRestrictEntrataLogin = $this->samlCheckRestrictEntrataLogin( true );
			if( true == $boolCheckRestrictEntrataLogin && true == $boolIsSsoEnabledSpecificUser ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', __( ' {%s, 0} password is disabled for single sign-on users. Please contact your system administrator.', [ $strMsg ] ) ) );

				return false;
			}
		}

		if( true == getArrayElementByKey( 'is_active_directory_enabled', $arrstrFilter ) && true == $this->getIsActiveDirectoryUser() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', __( ' {%s, 0} password is disabled for active directory users. Please contact your system administrator.', [ $strMsg ] ) ) );

			return false;
		}

		$objCompanyUserToken = $this->createOrFetchCompanyUserToken( $objClientDatabase );

		if( false == valObj( $objCompanyUserToken, 'CCompanyUserToken' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email', __( 'Unable to load a valid company user token.' ) ) );

			return false;
		}

		// Email contents
		if( true == valStr( $this->getPreferredLocaleCode() ) ) {
			CLocaleContainer::createService()->setPreferredLocaleCode( $this->getPreferredLocaleCode(), $objClientDatabase );
		}

		$strUrlParam     = '';
		$strLinkValidity = __( 'If you have received this mail in error, it is likely that another user entered your email address by mistake while trying to reset a password. If you did not initiate the request, you do not need to take any further action and can safely disregard this email. Above link will be expired in next 7 days' );
		if( true == getArrayElementByKey( 'forget_password', $arrstrFilter ) ) {
			$strUrlParam     = '&forget_password=1';
			$strLinkValidity = __( 'If you have received this mail in error, it is likely that another user entered your email address by mistake while trying to reset a password. If you did not initiate the request, you do not need to take any further action and can safely disregard this email. Above link will be expired in next 2 hours' );
		}

		$strUrl = CConfig::get( 'secure_host_prefix' ) . $strDomain . '.' . CConfig::get( 'entrata_suffix' ) . '/?module=authenticationxxx&action=reset_password&company_user_token=' . $objCompanyUserToken->getToken() . $strUrlParam;

		$strContents = '<div style="width: 545px;"><img  height="121" width="545" src="' . CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_ENTRATA_LOGO_PREFIX . 'psi_email_header.jpg" border="0" ></div>';
		$strContents .= '<div style="width: 545px;">';
		$strContents .= '<p style="margin:0 0 20px;">';
		$strContents .= __( 'Hi {%s, first_name}',  [ 'first_name' => $this->getNameFirst() ] );

		if( true == getArrayElementByKey( 'reset_password', $arrstrFilter ) || true == getArrayElementByKey( 'forget_password', $arrstrFilter ) ) {
			$strContents .= __( ',<br><br>There was recently a request to reset your {%s, 0} password. <br>', [ CONFIG_COMPANY_NAME ] );
			$strContents .= __( 'Username: {%s, username}', [ 'username' => $this->getUsername() ] );
			$strContents .= '</p>';
			$strContents .= '<div style="background-color:#e8e8e8;border-radius:7px;border:1px solid #d0d0d0;padding:10px 15px;color:#404040;font-size:16px;line-height:26px;word-wrap: break-word;">';
			$strContents .= __( '<strong>To reset the password, please <a href="{%s, url}" style="text-decoration:none; color:#1a70a6;">click here</a>.</strong>', [ 'url' => $strUrl ] );
		} else {
			$strContents .= __( ',<br><br>Your {%s, 0} account has been created. <br>', [ CONFIG_COMPANY_NAME ] );
			$strContents .= __( 'Username: {%s, username}',  [ 'username' => $this->getUsername() ] );
			$strContents .= '<p>';
			$strContents .= '<div style="background-color:#e8e8e8;border-radius:7px;border:1px solid #d0d0d0;padding:10px 15px;color:#404040;font-size:16px;line-height:26px;">';
			$strContents .= __( '<strong>To set the password, please <a href="{%s, url}" style="text-decoration:none; color:#1a70a6;">click here</a>.</strong>', [ 'url' => $strUrl ] );
		}

		$strContents .= __( '<br/>or<br/>Please copy and paste below URL into your web browser. {%s, url} <br/>', [ 'url' => $strUrl ] );
		$strContents .= '<br></div>';
		$strContents .= '<p style="margin:20px 0 40px;">';
		$strContents .= $strLinkValidity;
		$strContents .= '</p>';
		$strContents .= '<p style="margin:0;">';
		$strContents .= __( 'Note: Please do not reply back to this mail. This is sent from an unattended mail box.' );
		$strContents .= '</p>';
		$strContents .= '</div>';

		$strContents .= '<div style="width: 545px;"><img width="545" height="90" src="' . CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_ENTRATA_LOGO_PREFIX . 'psi_email_footer.jpg" border="0" usemap="#propertySolutionsLink">
						<map name="EntrataLink" id="propertySolutionsLink"><area shape="rect" coords="24,46,184,64" href="' . CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '"/></map></div>';

		$boolIsMailSent = $this->sendEmail( $this->getEmailAddress(), $strContents, $objEmailDatabase );

		CLocaleContainer::createService()->unsetPreferredLocaleCode( $objClientDatabase );

		if( false == $boolIsMailSent ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'email_address', __( 'Failed to send email to {%s, email_address}', [ 'email_address' => $this->getEmailAddress() ] ) ) );
		}

		return $boolIsMailSent;
	}

	public function updatePsiCompanyUsersByUserIdByCidByIsAdministrator( $intIsAdmininistrator, $objDatabase ) {

		$strSql = 'UPDATE
						company_users
					SET
						is_administrator = ' . ( int ) $intIsAdmininistrator . ',
						updated_by = ' . ( int ) $this->getId() . ',
						updated_on = NOW()
					WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND id = ' . ( int ) $this->getId() . '
						AND is_administrator != ' . ( int ) $intIsAdmininistrator;

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchPsProductOptionIds( $objDatabase ) {
		return CPropertyProducts::createService()->fetchPsProductOptionIdsByCompanyUser( $this, $objDatabase );
	}

	public function checkModulePermissionsByModuleIds( $arrintModuleIds, $objDatabase ) {

		if( true == $this->getIsAdministrator() ) {
			return array_fill_keys( $arrintModuleIds, 'w' );
		}

		$arrmixFilteredModules = array_diff_key( array_flip( $arrintModuleIds ), ( array ) $this->m_arrstrModulePermissions );
		if( false == valArr( $arrmixFilteredModules ) ) {
			return array_intersect_key( ( array ) $this->m_arrstrModulePermissions, array_flip( $arrintModuleIds ) );
		}

		$intCid = $this->getCid();
		$intCompanyUserId = $this->getId();
		$arrintPsProductPsProductOptionIds = $this->fetchPsProductOptionIds( $objDatabase );
		$arrintPsProductIds         = $arrintPsProductPsProductOptionIds['product_ids'];
		$arrintPsProductOptionIds   = $arrintPsProductPsProductOptionIds['options_ids'];

		if( true == valArr( $arrintPsProductOptionIds ) ) {
			$arrintTempPsProductId      = array_keys( $arrintPsProductOptionIds );
			$strProductOptionIdsCondition = ' OR ( pm.ps_product_id , pm.ps_product_option_id ) IN ( ' . sqlIntMultiImplode( $arrintPsProductOptionIds ) . ' )';
		}

		if( true == valArr( $arrintPsProductIds ) ) {

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintPsProductIds ) && true == in_array( CPsProduct::DOCUMENT_MANAGEMENT, $arrintPsProductIds ) && ( false == valArr( $arrintPsProductOptionIds ) || ( 1 == \Psi\Libraries\UtilFunctions\count( $arrintTempPsProductId ) && true == in_array( CPsProduct::DOCUMENT_MANAGEMENT, $arrintTempPsProductId ) ) ) ) {
				$strProductIdsCondition = '( pm.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) AND pm.ps_product_option_id IS NULL ) ' . $strProductOptionIdsCondition;
			} else {
				$strProductIdsCondition = 'pm.ps_product_id IS NULL OR ( pm.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )  AND pm.ps_product_option_id IS NULL ) ' . $strProductOptionIdsCondition;
			}
		} else {
			$strProductIdsCondition = 'pm.ps_product_id IS NULL ';
		}

		if( false == valId( $intCid ) || false == valId( $intCompanyUserId ) || false == valArr( $arrmixFilteredModules ) ) {
			return [];
		}

		$strSql = 'with recursive modules_list as(
						SELECT
							mc.id,
							mc.parent_module_id,
							mc.is_public,
							mc.is_hidden,
							mc.has_write_operation,
							NULL::integer AS child_module_id,
                            1 AS level
						FROM
							modules mc
							JOIN product_modules pm ON ( pm.module_id = mc.id )
						WHERE
							mc.id IN ( ' . implode( ',', array_keys( $arrmixFilteredModules ) ) . ' )
							AND mc.is_published = 1
							AND mc.allow_only_psi_admin = 0 
							AND mc.allow_only_admin = 0
							AND ( ' . $strProductIdsCondition . ' )
						UNION 
						SELECT
							mp.id,
							mp.parent_module_id,
							mp.is_public,
							mp.is_hidden,
							mp.has_write_operation,
							modules_list.id AS child_module_id,
                            ( modules_list.level + 1 ) AS level
						FROM
							modules mp
							JOIN product_modules pm ON ( pm.module_id = mp.id )
							JOIN modules_list ON mp.id = modules_list.parent_module_id AND modules_list.level < 8
						WHERE
							mp.is_published = 1
							AND mp.allow_only_psi_admin = 0
							AND mp.allow_only_admin = 0
							AND ( ' . $strProductIdsCondition . ' )
				)

				SELECT
					permissions.*,
					m.parent_module_id,
					m.is_hidden,
					m.is_public,
					m.has_write_operation,
					m.level
				FROM
					(
					SELECT
						cup.module_id,
						cup.is_read_only,
						cup.is_allowed
					FROM
						  company_users cu
						  JOIN company_user_permissions cup ON ( cup.cid = cu.cid AND cup.company_user_id = cu.id )
						  JOIN modules_list ml ON ( ml.id = cup.module_id )
					WHERE
						  cu.cid = ' . ( int ) $intCid . '
						  AND cu.id = ' . ( int ) $intCompanyUserId . '
						  AND cu.is_disabled = 0
						  AND cup.is_inherited = 0
						  AND ml.is_hidden = 0
                          AND ml.is_public = 0
					UNION
					SELECT
						  cgp.module_id,
						  MIN( cgp.is_read_only ) AS is_read_only,
						  1 AS is_allowed
					  FROM
						  company_users cu
						  JOIN company_user_groups cug ON cug.cid = cu.cid AND cu.id = cug.company_user_id
						  JOIN company_groups cg ON ( cg.cid = cu.cid AND cg.id = cug.company_group_id )
						  JOIN company_group_permissions cgp ON ( cgp.cid = cu.cid AND cgp.company_group_id = cug.company_group_id )
						  JOIN modules_list ml ON ( ml.id = cgp.module_id )
					  WHERE
						  cu.cid = ' . ( int ) $intCid . '
						  AND cu.id = ' . ( int ) $intCompanyUserId . '
						  AND cu.is_disabled = 0
						  AND cgp.is_allowed = 1
						  AND ml.is_hidden = 0
                          AND ml.is_public = 0
						  AND NOT EXISTS ( SELECT 1 FROM company_user_permissions cup1 WHERE cup1.cid = cu.cid AND cup1.module_id = cgp.module_id AND cup1.company_user_id = cu.id AND cup1.is_inherited = 0 )
					  GROUP BY
						  cu.id,
						  cgp.module_id
					  UNION
					  SELECT
						  id AS module_id,
						  0 AS is_read_only,
						  1 AS is_allowed
					  FROM
						  modules_list
					  WHERE
						  is_public = 1
						  OR is_hidden = 1
					) permissions
					JOIN modules_list m ON ( m.id = permissions.module_id )
				ORDER BY
					m.level DESC';

		$arrmixResults = ( array ) fetchData( $strSql, $objDatabase );
		$arrmixResults = rekeyArray( 'module_id', $arrmixResults );

		$arrmixUserPermissions = [];

		foreach( $arrmixResults as $intModuleId => $arrstrModule ) {

			$intParentModuleId = ( true == valId( $arrstrModule['parent_module_id'] ) ? $arrstrModule['parent_module_id'] : 0 );
			if( ( true == valId( $intParentModuleId ) && false == array_key_exists( $intParentModuleId, $arrmixResults ) ) || 0 == $arrstrModule['is_allowed'] ) {
				continue;
			}

			$strPermission               = ( 1 == $arrstrModule['is_read_only'] ? 'r' : 'w' );
			$strParentModulePermissons   = ( true == isset( $arrmixResults[$intParentModuleId] ) && 1 == $arrmixResults[$intParentModuleId]['is_read_only'] ? 'r' : 'w' );

			if( 1 == $arrstrModule['has_write_operation'] && ( 'r' == $strPermission || 'r' == $strParentModulePermissons || ( true == isset( $arrmixUserPermissions[$intParentModuleId] ) && 'r' == $arrmixUserPermissions[$intParentModuleId] ) ) ) {
				continue;
			}

			if( true == valStr( $strPermission ) ) {
				$arrmixUserPermissions[$intModuleId] = ( true == isset( $arrmixUserPermissions[$intParentModuleId] ) && 'r' == $arrmixUserPermissions[$intParentModuleId] && ( true == $arrstrModule['is_hidden'] || true == $arrstrModule['is_public'] ) ) ? 'r' : ( 'r' == $strParentModulePermissons && ( true == $arrstrModule['is_hidden'] || true == $arrstrModule['is_public'] ) ) ? 'r' : $strPermission;
			}
		}
		$this->m_arrstrModulePermissions = ( array ) $this->m_arrstrModulePermissions + array_intersect_key( $arrmixUserPermissions, $arrmixFilteredModules );
		$arrmixUserPermissions = array_intersect_key( $this->m_arrstrModulePermissions, array_flip( $arrintModuleIds ) );

		return $arrmixUserPermissions;
	}

	/**
	 * @param $strUsername
	 * @return mixed|string
	 */
	private function getFormattedUsername( $strUsername ) {

		if( false == valStr( $strUsername ) ) {
			return $strUsername;
		}

		if( true == $this->m_boolUseUsernameInsteadOfEmail ) {
			$strUsername = preg_replace( '/@' . $this->getRwxDomain() . '$/', '', $strUsername );
			if( false === \Psi\CStringService::singleton()->strpos( $strUsername, '@' ) ) {
				$strUsername .= '@' . $this->getRwxDomain();
			} else {
				$strUsername = \Psi\CStringService::singleton()->strtolower( $strUsername );
			}
		} else {
			$strUsername = \Psi\CStringService::singleton()->strtolower( $strUsername );
		}

		return $strUsername;
	}

	public function updateRemotePrimaryKeyForADUsers( $objDatabase ) {

		$strSql = 'UPDATE
						company_users
					SET
						remote_primary_key = NULL ,
						updated_by = ' . ( int ) $this->getId() . ',
						updated_on = NOW()
					WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND remote_primary_key IS NOT NULL
						AND is_active_directory_user = 0';

		return $this->executeSql( $strSql, $this, $objDatabase );
	}

	public function fetchPsProductIdsForHistoricalData( $objDatabase ) {
		return CPropertyProductLogs::createService()->fetchPsProductIdsForHistoricalDataByCompanyUser( $this->getIsAdministrator(), $this->getIsPSIUser(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPsProductDetailsForHistoricalDataByPsProductIds( $arrintPsProductIds, $objDatabase, $boolFetchPsProductOptionProperties = false ) {
		return CPropertyProductLogs::createService()->fetchPsProductDetailsForHistoricalDataByPsProductIdsByCompanyUserIdByCid( $arrintPsProductIds, $this->getId(), $this->getCid(), $this->getIsAdministrator(), $this->getIsPSIUser(), $objDatabase, $boolFetchPsProductOptionProperties );
	}

	public function fetchPsProductOptionIdsForHistoricalData( $objDatabase ) {
		return CPropertyProductLogs::createService()->fetchPsProductOptionIdsForHistoricalDataByCompanyUser( $this->getIsAdministrator(), $this->getIsPSIUser(), $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSimplePropertyIdsForHistoricalData( $objDatabase ) {
		return CPropertyProductLogs::createService()->fetchSimplePropertyIdsForHistoricalDataByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase, $this->getIsAdministrator() );
	}

	public function fetchSimplePropertyGroupIdsForHistoricalData( $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUserPropertyGroups::createService()->fetchSimplePropertyGroupIdsForHistoricalDataByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function checkHasRVFullPackageAccessPermision( $objClient, $objDatabase ) {

        $boolIsRvScreeningPackageFullAccessUser = false;
        // fetch record count with group id and user id
        $intValidUserCount = CEmployees::fetchEmployeeByGroupIdByUserId( CGroup::RESIDENT_VERIFY_SCREENING_PACKAGE_GROUP, $this->getId(), $objDatabase );

        if( true == $this->getIsPSIUser() ) {
            if( 0 < $intValidUserCount ) {
                $boolIsRvScreeningPackageFullAccessUser = true;
            } elseif( ( 0 == $intValidUserCount ) && ( CCompanyStatusType::TEST_DATABASE == $objClient->getCompanyStatusTypeId() ) ) {
                $boolIsRvScreeningPackageFullAccessUser = true;
            }
        }

	    return $boolIsRvScreeningPackageFullAccessUser;
    }

	public function checkIsMyProfileAccessable( $objDatabase ) {

		$boolIsMyProfileInformationAccessable = false;
		$boolIsMyProfileAccessable = false;
		$arrmixUserPermissions = $this->checkModulePermissionsByModuleIds( [ CModule::MY_PROFILE, CModule::MY_PROFILE_INFORMATION ], $objDatabase );

		if( true == valArr( $arrmixUserPermissions ) ) {
			if( true == array_key_exists( CModule::MY_PROFILE, $arrmixUserPermissions ) ) {
				$boolIsMyProfileAccessable = true;
			}
			if( true == array_key_exists( CModule::MY_PROFILE_INFORMATION, $arrmixUserPermissions ) && 'w' == $arrmixUserPermissions[CModule::MY_PROFILE_INFORMATION] ) {
				$boolIsMyProfileInformationAccessable = true;
			}
		}

		return [ 'is_my_profile_accessable' => $boolIsMyProfileAccessable, 'is_my_profile_info_accessable' => $boolIsMyProfileInformationAccessable, ];
	}

	public function checkForGroupHasEnabledTwoFactorAuthentication( $objClientDatabase ) {

		$arrmixCompanyUserGroups = \Psi\Eos\Entrata\CCompanyGroups::createService()->fetchCompanyUserGroupsByCompanyUserIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		$objCompanyPreferences = CCompanyPreferences::createService()->fetchCompanyPreferenceByKeyByCid( 'ENABLED_TWO_FACTOR_AUTHENTICATION', $this->getCid(), $this->m_objDatabase );
		if( true == valArr( $arrmixCompanyUserGroups ) ) {
			foreach( $arrmixCompanyUserGroups as $arrmixCompanyUserGroup ) {
				$arrintCompanyGroupIds[] = $arrmixCompanyUserGroup['group_id'];
			}
			$arrstrCompanyGroupPreferencesKey = array_keys( rekeyArray( 'key', \Psi\Eos\Entrata\CCompanyGroupPreferences::createService()->fetchCompanyGroupPreferencesByCompanyGroupIdsByCid( $arrintCompanyGroupIds, $this->getCid(), $this->m_objDatabase ) ) );
			if( true == valObj( $objCompanyPreferences, 'CCompanyPreference' ) && true == in_array( 'ENABLED_TWO_FACTOR_AUTHENTICATION', $arrstrCompanyGroupPreferencesKey ) ) {
				return true;
			}
		}
		return false;
	}

	public function getImplementationManagerFullName() {
		$objAdminDatabase = $this->loadAdminDatabase();
		if( false == valObj( $objAdminDatabase, CDatabase::class ) ) {
			return NULL;
		}
		$arrmixPsLeadDetails = \Psi\Eos\Admin\CPsLeadDetails::createService()->fetchPsLeadDetailsWithImplementationEmployeeDetailsByCid( $this->getCid(), $objAdminDatabase );
		$objAdminDatabase->close();
		if( false == valArr( $arrmixPsLeadDetails ) ) {
			return NULL;
		}
		return $arrmixPsLeadDetails[0]['implementation_manager'];
	}

	public function getUserDetails() {
		$objAdminDatabase = $this->loadAdminDatabase();
		if( false == valObj( $objAdminDatabase, CDatabase::class ) ) {
			return NULL;
		}
		$arrmixPsLeadDetails = \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeeAndDepartmentDetailsByUserId( $this->getId(), $objAdminDatabase );
		$objAdminDatabase->close();
		if( false == valArr( $arrmixPsLeadDetails ) ) {
			return NULL;
		}
		$arrmixPsLeadDetail = $arrmixPsLeadDetails[0];
		return $arrmixPsLeadDetail;

	}

	public function getUserNameFirst() {
		$arrmixPsLeadDetail = $this->getUserDetails();
		if( false == valStr( $arrmixPsLeadDetail['name_first'] ) ) {
			return NULL;
		}
		return $arrmixPsLeadDetail['name_first'];
	}

	public function getUserNameLast() {
		$arrmixPsLeadDetail = $this->getUserDetails();
		if( false == valStr( $arrmixPsLeadDetail['name_last'] ) ) {
			return NULL;
		}
		return $arrmixPsLeadDetail['name_last'];
	}

	public function getUserEmailAddress() {
		$arrmixPsLeadDetail = $this->getUserDetails();
		if( false == valStr( $arrmixPsLeadDetail['email_address'] ) ) {
			return NULL;
		}
		return $arrmixPsLeadDetail['email_address'];
	}

	public function getUserDepartmentName() {
		$arrmixPsLeadDetail = $this->getUserDetails();
		if( false == valStr( $arrmixPsLeadDetail['department_name'] ) ) {
			return NULL;
		}
		return $arrmixPsLeadDetail['department_name'];
	}

	public function getPropertyProductDetails() {
		$strDetailsTableStructure = '';
		$strTableBody = '';
		$arrmixRequiredParameters = $this->getRequiredParameters();
		$objAdminDatabase = $this->loadAdminDatabase();
		if( false == valObj( $objAdminDatabase, CDatabase::class ) || false == valArrKeyExists( $arrmixRequiredParameters, 'contract_property_ids' ) ) {
			return NULL;
		}

		$arrintContractPropertyIds = $arrmixRequiredParameters['contract_property_ids'];
		$arrobjContractProperties  = \Psi\Eos\Admin\CContractProperties::createService()->fetchCustomContractPropertiesWithPropertyProductDetailsByIds( $arrintContractPropertyIds, $objAdminDatabase );
		$objAdminDatabase->close();
		if( false == valArr( $arrobjContractProperties ) ) {
			return NULL;
		}
		$strDetailsTableStructure = __( '<table style="width:100%;border: 1px solid #999;border-collapse: collapse;margin-top: 12px;" cellspacing="0" cellpadding="5">
					<tr>
						<th style="border: 1px solid #999;font:normal 13px Arial; color:#636363;" width="50%">Property</th>
						<th style="border: 1px solid #999;font:normal 13px Arial; color:#636363;" width="40%">Product</th>
						<th style="border: 1px solid #999;font:normal 13px Arial; color:#636363;" width="10%">Impl. Date</th>
					</tr>
					<tablebody>
					</table>' );

		foreach( $arrobjContractProperties as $objContractProperty ) {
			$strTableBody .= __( '<tr>
									<td style="border: 1px solid #999;font:normal 13px Arial; color:#636363;" >{%s, 0} ( {%s, 1} ) </td>
									<td style="border: 1px solid #999;font:normal 13px Arial; color:#636363;" >{%s, 2}</td>
									<td style="border: 1px solid #999;font:normal 13px Arial; color:#636363;" >{%t, 3, DATE_NUMERIC_STANDARD}</td>
								</tr>',
				[ $objContractProperty->getPropertyName(), $objContractProperty->getPropertyId(), $objContractProperty->getProductName(), $objContractProperty->getImplementationPostDate() ] );
		}

		$strContentDetails = str_replace( '<tablebody>', $strTableBody, $strDetailsTableStructure );

		return $strContentDetails;
	}

	public function getContractId() {
		$arrmixRequiredParameters = $this->getRequiredParameters();

		if( false == valArrKeyExists( $arrmixRequiredParameters, 'contract_id' ) ) {
			return NULL;
		}
		return $arrmixRequiredParameters['contract_id'];
	}

	public function loadAdminDatabase() {

		if( true == getIsDbOnMaintenance( CDatabase::ADMIN ) ) {
			return NULL;
		}

		$objAdminDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::ADMIN, CDatabaseUserType::PS_DEVELOPER, $intDatabaseServerTypeId = 1, $boolOpenConnection = false, $intConnectionTimeout = 2 );

		if( true == is_null( $objAdminDatabase ) || false == valObj( $objAdminDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		return $objAdminDatabase;
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	public function syncCompanyUserToSso() : bool {
		try {
			require_once( PATH_APPLICATION_ENTRATA . 'Library/Sso/CEntrataSso.class.php' );
			$objSsoUser = new CEntrataSso();
			$objSsoUser
				->setUserId( $this->getId() )
				->setCid( $this->getCid() )
				->setNewUsername( $this->getUsername() ?? NULL )
				->setUsername( $this->getOldUsername() ?? $this->getUsername() )
				->setPlainPassword( $this->getPassword() ?? NULL )
				->setIsDisabled( $this->getIsDisabled() )
				->setAppName( $objSsoUser::APP_ENTRATA )
				->syncEntrataUserToSso();
		} catch( Throwable $objSsoException ) {
            throw $objSsoException;
		}

		return true;
	}

	private function loadAmqpConnectionConfig() : array {
		$strUser     = CONFIG_AMQP_USERNAME;
		$strPassword = CONFIG_AMQP_PASSWORD;
		$strUser     = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strUser, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME, 'mode' => MCRYPT_MODE_CBC ] );
		$strPassword = \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $strPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD, 'mode' => MCRYPT_MODE_CBC ] );

		return  [
			'provider'            => \Psi\Libraries\PubSub\Repository\CProvider::RABBIT_MQ,
			'host'                => CONFIG_AMQP_HOST,
			'port'                => CONFIG_AMQP_PORT,
			'user'                => $strUser,
			'password'            => $strPassword,
			'vhost'               => '/',
			'connection_time_out' => 3,
			'read_write_time_out' => 60,
			'context'             => NULL,
			'keepalive'           => true,
			'heartbeat'           => 30
		];
	}

}

?>