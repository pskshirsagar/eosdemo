<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerTypes
 * Do not add any new functions to this class.
 */

class CCustomerTypes extends CBaseCustomerTypes {

    public static function fetchCustomerTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CCustomerType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchCustomerType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CCustomerType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchAllCustomerTypes( $objDatabase ) {
    	$strSql = 'SELECT * FROM customer_types order by order_num';
    	return self::fetchCustomerTypes( $strSql, $objDatabase );
    }

	public static function fetchAllPublishedCustomerTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						customer_types
					WHERE 
						is_published = 1';

		return parent::fetchCustomerTypes( $strSql, $objDatabase );
	}

	public static function fetchPublishedNonPrimaryCustomerTypes( $objDatabase ) {

    	$strSql = 'SELECT
						*
					FROM
						customer_types
					WHERE
						is_published = 1
						AND id <> ' . CCustomerType::PRIMARY . '
	                ORDER BY 
						order_num';

    	return self::fetchCustomerTypes( $strSql, $objDatabase );
	}

}
?>