<?php

class CViolationTemplateNotice extends CBaseViolationTemplateNotice {

	protected $m_boolIsSetCharge;

	public function setIsSetCharge( $boolIsSetCharge = false ) {
		$this->m_boolIsSetCharge = $boolIsSetCharge;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['is_set_charge'] ) && false == is_null( $arrmixValues['is_set_charge'] ) ) {
			$this->setIsSetCharge( true );
		} else {
			$this->setIsSetCharge( false );
		}
	}

	public function getIsSetCharge() {
		return $this->m_boolIsSetCharge;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViolationTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViolationWarningTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;
		if( true == $this->getIsSetCharge() && true == is_null( $this->getArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ar_code_id', __( 'Charge code is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		if( true == $this->getIsSetCharge() && ( true == is_null( $this->getAmount() ) || 0 == $this->getAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'amount', __( 'Nonzero Amount is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valNextFollowUpDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsStartEviction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valArCodeId();
				$boolIsValid &= $this->valAmount();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>