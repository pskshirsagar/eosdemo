<?php

class CClAdImage extends CBaseClAdImage {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClAdId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImagePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImageName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>