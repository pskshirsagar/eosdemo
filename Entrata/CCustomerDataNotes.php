<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerDataNotes
 * Do not add any new functions to this class.
 */

class CCustomerDataNotes extends CBaseCustomerDataNotes {

	public static function fetchCustomerDataNotesByCustomerIdByCustomerDataTypeIdByCustomerDataReferenceIdByCid( $intCustomerId, $intCustomerDataTypeId, $intCustomerDataReferenceId, $intCid, $objDatabase ) {

	    if( false == valStr( $intCustomerId ) && false == valStr( $intCustomerDataTypeId ) && false == valStr( $intCustomerDataReferenceId ) ) return NULL;

		$strSql = ' SELECT
                        cdn.*,
						cu.username AS company_user_name
                    FROM
                        customer_data_notes cdn
				     	LEFT JOIN company_users  cu ON ( cu.cid = cdn.cid AND cu.id = cdn.created_by )
                    WHERE
						cdn.cid = ' . ( int ) $intCid . '
                        AND cdn.customer_id = ' . ( int ) $intCustomerId . '
                        AND cdn.customer_data_type_id = ' . ( int ) $intCustomerDataTypeId . '
                        AND cdn.customer_data_reference_id = ' . ( int ) $intCustomerDataReferenceId . '
					ORDER BY cdn.id DESC';

		return self::fetchCustomerDataNotes( $strSql, $objDatabase );
	}

	public static function fetchCustomerDataNotesCountByCustomerDataTypeIdByCustomerDataReferenceIdsByCid( $intCustomerDataTypeId, $arrintCustomerDataReferenceId, $intCid, $objDatabase ) {

	    if( false == valStr( $intCustomerDataTypeId ) || false == valArr( $arrintCustomerDataReferenceId ) ) return NULL;

	    $strSql = ' SELECT
                        count( cdn.id ),
	                    cdn.customer_data_reference_id
				    FROM
                        customer_data_notes cdn
                    WHERE
						cdn.cid = ' . ( int ) $intCid . '                        
                        AND cdn.customer_data_type_id = ' . ( int ) $intCustomerDataTypeId . '
                        AND cdn.customer_data_reference_id IN ( ' . implode( ',', $arrintCustomerDataReferenceId ) . ' )
					GROUP BY cdn.customer_data_reference_id';

	    return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerDataNotesByCustomerDataTypeIdByCustomerDataReferenceIdByCid( $intCustomerDataTypeId, $intCustomerDataReferenceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						cdn.*,
						cu.username AS company_user_name
					FROM 
						customer_data_notes cdn
						LEFT JOIN company_users cu ON ( cu.cid = cdn.cid AND cu.id = cdn.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					WHERE 
						cdn.cid = ' . ( int ) $intCid . '
						AND cdn.customer_data_type_id = ' . ( int ) $intCustomerDataTypeId . '
						AND cdn.customer_data_reference_id = ' . ( int ) $intCustomerDataReferenceId;

		return self::fetchCustomerDataNotes( $strSql, $objDatabase );

	}

}
?>