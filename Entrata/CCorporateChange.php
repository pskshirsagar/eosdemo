<?php

class CCorporateChange extends CBaseCorporateChange {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorporateEventId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorporateChangeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOldValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getMaskedOldValue() {
		return  CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', CEncryption::decryptText( $this->m_strOldValue, CONFIG_KEY_TAX_NUMBER ) ) );
	}

	public function getMaskedNewValue() {
		return  CEncryption::maskText( preg_replace( '/[^a-z0-9]/i', '', CEncryption::decryptText( $this->m_strNewValue, CONFIG_KEY_TAX_NUMBER ) ) );
	}

}
?>