<?php

class CCommercialFloorShare extends CBaseCommercialFloorShare {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommercialLeaseDetailsId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyFloorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSquareFootageShare() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResponsibleUtilities() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResponsibleJanitorial() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResponsibleSecurity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>