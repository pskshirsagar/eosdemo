<?php

class CRuleStopApproval extends CBaseRuleStopApproval {

	protected $m_intCompanyUserId;
	protected $m_intCompanyGroupId;

	protected $m_strApprovalNote;
	protected $m_strCompanyUserName;
	protected $m_strCompanyGroupName;
	protected $m_strCompanyUserNameFirst;

	/**
	 * Get Functions
	 *
	 */

	public function getOrderNumber() {
		return $this->m_intOrderNumber;
	}

	public function getRouteId() {
		return $this->m_intRouteId;
	}

	public function getRouteRuleId() {
		return $this->m_intRouteRuleId;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getCompanyGroupId() {
		return $this->m_intCompanyGroupId;
	}

	public function getCompanyUserName() {
		return $this->m_strCompanyUserName;
	}

	public function getCompanyGroupName() {
		return $this->m_strCompanyGroupName;
	}

	public function getCompanyUserNameFirst() {
		return $this->m_strCompanyUserNameFirst;
	}

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['order_num'] ) )			$this->setOrderNumber( $arrmixValues['order_num'] );
		if( true == isset( $arrmixValues['route_id'] ) )			$this->setRouteId( $arrmixValues['route_id'] );
		if( true == isset( $arrmixValues['route_rule_id'] ) )		$this->setRouteRuleId( $arrmixValues['route_rule_id'] );
		if( true == isset( $arrmixValues['company_user_id'] ) )		$this->setCompanyUserId( $arrmixValues['company_user_id'] );
		if( true == isset( $arrmixValues['company_group_id'] ) )	$this->setCompanyGroupId( $arrmixValues['company_group_id'] );
		if( true == isset( $arrmixValues['company_user_name'] ) )	$this->setCompanyUserName( $arrmixValues['company_user_name'] );
		if( true == isset( $arrmixValues['company_group_name'] ) )	$this->setCompanyGroupName( $arrmixValues['company_group_name'] );
		if( true == isset( $arrmixValues['name_first'] ) )			$this->setCompanyUserNameFirst( $arrmixValues['name_first'] );

	}

	/**
	 * Set Functions
	 *
	 */

	public function setOrderNumber( $intOrderNumber ) {
		$this->m_intOrderNumber = $intOrderNumber;
	}

	public function setRouteId( $intRouteId ) {
		$this->m_intRouteId = $intRouteId;
	}

	public function setRouteRuleId( $intRouteRuleId ) {
		$this->m_intRouteRuleId = $intRouteRuleId;
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
	}

	public function setCompanyGroupId( $intCompanyGroupId ) {
		$this->m_intCompanyGroupId = $intCompanyGroupId;
	}

	public function setCompanyUserName( $strCompanyUserName ) {
		$this->m_strCompanyUserName = $strCompanyUserName;
	}

	public function setCompanyGroupName( $strCompanyGroupName ) {
		$this->m_strCompanyGroupName = $strCompanyGroupName;
	}

	public function setCompanyUserNameFirst( $strCompanyUserNameFirst ) {
		$this->m_strCompanyUserNameFirst = $strCompanyUserNameFirst;
	}

	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRuleStopId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRouteRuleReferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
			 // Empty default case
				break;
		}

		return $boolIsValid;
	}
}
?>