<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeasingGoals
 * Do not add any new functions to this class.
 */

class CLeasingGoals extends CBaseLeasingGoals {

	public static function fetchLeasingGoalNamesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
						return NULL;
		}
		$strSql = 'SELECT
					    util_get_translated( \'name\', lg.name, lg.details ) As goal_name,
					    lg.id,
					    lg.frequency_id
					FROM
					    leasing_goals lg
					    JOIN leasing_goal_associations lga ON ( lga.cid = lg.cid AND lga.leasing_goal_id = lg.id )
					    JOIN lease_terms lt ON ( lt.cid = lga.cid AND lt.id = lga.lease_term_id )
					    JOIN lease_start_windows lsw ON ( lsw.cid = lga.cid AND lsw.lease_term_id = lga.lease_term_id AND lsw.property_id = lg.property_id AND lsw.is_active = TRUE )
					WHERE
					    lg.cid = ' . ( int ) $intCid . '
					    AND lg.property_id = ' . ( int ) $intPropertyId . '
					    AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
					    AND lt.occupancy_type_id = ' . COccupancyType::STUDENT . '
					    AND lt.is_disabled = FALSE
					    AND lsw.end_date > CURRENT_DATE
					    AND lsw.deleted_on IS NULL
					    AND lt.deleted_on IS NULL
					    AND lg.deleted_by IS NULL
					    AND lga.deleted_by IS NULL
					GROUP BY
					    lg.name,
					    lg.id,
					    lg.details,
					    lg.frequency_id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchLeasingGoalNamesForLeasingGoalsFilterByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strSql = 'SELECT
					    util_get_translated( \'name\', lg.name, lg.details ) As goal_name,
					    lg.id,
					    lg.frequency_id
					FROM
					    leasing_goals lg
					    JOIN leasing_goal_associations lga ON ( lga.cid = lg.cid AND lga.leasing_goal_id = lg.id )
					    JOIN lease_terms lt ON ( lt.cid = lga.cid AND lt.id = lga.lease_term_id )
					    JOIN lease_start_windows lsw ON ( lsw.cid = lga.cid AND lsw.lease_term_id = lga.lease_term_id AND lsw.id = lga.lease_start_window_id AND lsw.property_id = lg.property_id )
					WHERE
					    lg.cid = ' . ( int ) $intCid . '
					    AND lg.property_id = ' . ( int ) $intPropertyId . '
					    AND lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
					    AND lt.occupancy_type_id = ' . COccupancyType::STUDENT . '
					    AND lsw.end_date > CURRENT_DATE
					    AND lsw.deleted_on IS NULL
					    AND lt.deleted_on IS NULL
					    AND lg.deleted_by IS NULL
					    AND lga.deleted_by IS NULL
					GROUP BY
					    lg.name,
					    lg.id,
					    util_get_translated( \'name\', lg.name, lg.details ),
					    lg.frequency_id';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomLeasingGoalsByPropertyIdByLeasingGoalIdByCid( $intPropertyId, $intLeasingGoalId, $intCid, $objDatabase ) {
		$strSql = 'with leasing_goals_data as (
					SELECT sum_of_goals.term_month,
					       sum_of_goals.cid,
					       sum_of_goals.is_published,
					       sum_of_goals.leasing_goal_id,
					       SUM( sum_of_goals.new_goal ) AS sum_new_goal,
					       SUM( sum_of_goals.renewal_goal ) sum_renewal_goal
					FROM (
					       SELECT to_char( DATE ( lgd.goal_date ), \'Mon YYYY\' ) AS term_month,
					              lg.cid,
					              CASE
					                WHEN true = lgd.is_renewal then 0
					                ELSE lgd.goal_count
					              END AS new_goal,
					              CASE
					                WHEN false = lgd.is_renewal then 0
					                ELSE lgd.goal_count
					              END AS renewal_goal,
					              lgd.goal_count,
					              lgd.is_published,
					              lgd.is_renewal,
					              lg.id AS leasing_goal_id,
					              lgd.goal_date
					       FROM leasing_goals lg
					            JOIN leasing_goal_distributions lgd ON (lgd.cid = lg.cid AND
					              lgd.leasing_goal_id = lg.id)
					       WHERE lg.cid = ' . ( int ) $intCid . ' AND
					             lg.property_id = ' . ( int ) $intPropertyId . ' AND             
					             lg.id = ' . ( int ) $intLeasingGoalId . '
					       ORDER BY to_timestamp( to_char( DATE ( lgd.goal_date ), \'1 Mon YYYY\' ),
					         \'1 Mon YYYY\' ),
					                lgd.is_renewal
					     ) sum_of_goals
					GROUP BY sum_of_goals.term_month,
					         sum_of_goals.cid,
					         sum_of_goals.is_published,
					         sum_of_goals.leasing_goal_id ORDER BY to_date(sum_of_goals.term_month,\'Mon YYYY\'))
					         select leasing_goals_data.term_month,
					                leasing_goals_data.cid,
					                leasing_goals_data.is_published,
					                leasing_goals_data.leasing_goal_id,
					                leasing_goals_data.sum_new_goal,
					                leasing_goals_data.sum_renewal_goal,
					                SUM( sum_new_goal ) OVER( 
					         ORDER BY leasing_goals_data.cid, leasing_goals_data.leasing_goal_id asc rows between unbounded
					           preceding and current row ) AS total_new_goal,
					                  SUM( sum_renewal_goal ) OVER( 
					         ORDER BY leasing_goals_data.cid, leasing_goals_data.leasing_goal_id asc rows between unbounded
					           preceding and current row ) AS total_renewal_goal,
							 CASE 
							  SUM(sum_new_goal + sum_renewal_goal)
							  OVER(PARTITION BY leasing_goals_data.cid,
							  leasing_goals_data.leasing_goal_id) 
							  WHEN 0 THEN 0                                  
							  ELSE 
							  ROUND((((SUM(sum_new_goal) OVER(
							ORDER BY leasing_goals_data.cid,
							  leasing_goals_data.leasing_goal_id asc rows
							  between unbounded preceding and current row) +
							  SUM(sum_renewal_goal) OVER(
							ORDER BY leasing_goals_data.cid,
							  leasing_goals_data.leasing_goal_id asc rows
							  between unbounded preceding and current row))
							  * 100) / SUM(sum_new_goal + sum_renewal_goal)
							  OVER(PARTITION BY leasing_goals_data.cid,
							  leasing_goals_data.leasing_goal_id)), 2)
							  END AS percentage
					         from leasing_goals_data';

		$arrmixTempPropertyLeasingGoals = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrmixTempPropertyLeasingGoals ) ) {
			return [];
		}

		return $arrmixTempPropertyLeasingGoals;
	}

	public static function fetchGoalCountByGoalIdByIsRenewalByStartDateByEndDateByPropertyIdByCid( $intGoalId, $intIsRenewal, $strStartDate, $strEndDate, $intPropertyId, $intCid, $objDatabase, $intPropertyFloorPlanId = NULL ) {

		if( false == valId( $intGoalId ) ) return NULL;

		$strAndCondition = '';
		if( false == is_null( $intPropertyFloorPlanId ) ) {
			$strAndCondition = ' AND lgd.property_floorplan_id = ' . ( int ) $intPropertyFloorPlanId;
		}

		$strSql = 'SELECT
					    sum(lgd.goal_count) as count
					FROM
					    leasing_goals lg
					    JOIN leasing_goal_distributions lgd ON ( lgd.cid = lg.cid AND lgd.leasing_goal_id = lg.id )
					WHERE
					    lg.cid = ' . ( int ) $intCid . '
					    AND lg.property_id = ' . ( int ) $intPropertyId . '
					    AND lg.id = ' . ( int ) $intGoalId .
		                $strAndCondition . '
					    AND lgd.is_renewal = CAST ( ' . ( int ) $intIsRenewal . ' AS BOOLEAN )
					    AND goal_date BETWEEN to_date( \'' . $strStartDate . '\', \'DD/mm/YYYY\' )
					    AND to_date( \'' . $strEndDate . '\', \'DD/mm/YYYY\' )
					    AND lg.deleted_by IS NULL
						AND lg.deleted_on IS NULL';

		$arrintCount = fetchData( $strSql, $objDatabase );

		return $arrintCount[0]['count'];
	}

	public static function fetchLeasingGoalByIdByPropertyIdByCid( $intGoalId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intGoalId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						leasing_goals
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intGoalId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return parent::fetchLeasingGoal( $strSql, $objDatabase );
	}

	public static function fetchLeasingGoalByNameByPropertyIdByCid( $strName, $intPropertyId, $intCid, $objDatabase, $intLeasingGoalId = 0 ) {

		$strSql = 'SELECT
						count( id ) as count
					FROM
						leasing_goals
					WHERE
						cid = ' . ( int ) $intCid . '
						AND LOWER(name) = LOWER(\'' . $strName . '\')
						AND property_id = ' . ( int ) $intPropertyId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		if( true == valId( $intLeasingGoalId ) ) {
			$strSql .= ' AND id <> ' . ( int ) $intLeasingGoalId;
		}

		$arrintCount = fetchData( $strSql, $objDatabase );

		return $arrintCount[0]['count'];
	}

	public static function fetchPreviousYearActualLeaseTermsByLeasingGoalsFilterByCid( $arrmixLeasingGoalsFilter, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $arrmixLeasingGoalsFilter['property_id'] ) || false == valId( $arrmixLeasingGoalsFilter['leasing_goal_id'] ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT lg_lsw.lease_start_window_id,          	             	
     	                lg_lsw.lease_term_name,
     	                lg_lsw.start_date,
     	                lg_lsw.end_date
                    FROM
			            leasing_goals lg
			            JOIN leasing_goal_associations lga ON ( lga.cid = lg.cid AND lga.leasing_goal_id = lg.id )
			            JOIN LATERAL (
				          SELECT 
				                DISTINCT prev_lsw.lease_term_id,
				                util_get_translated( \'name\', prev_lt.name, prev_lt.details ) AS lease_term_name,
				                prev_lsw.id as lease_start_window_id,
				                CASE 
									WHEN prev_lt.is_renewal = true AND prev_lt.is_prospect = false THEN 
										prev_lsw.renewal_start_date
									ELSE
										prev_lsw.start_date
								END AS start_date,
				                prev_lsw.end_date,
				                lsw.cid,
				                lsw.property_id
				            FROM
				                lease_start_windows lsw
				                JOIN lease_start_windows prev_lsw ON ( prev_lsw.cid = lsw.cid AND lsw.property_id = prev_lsw.property_id
					                AND prev_lsw.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND prev_lsw.deleted_on IS NULL AND prev_lsw.occupancy_type_id = ' . COccupancyType::STUDENT . '
					                AND ( ( lsw.start_date - interval \'1 year\' ), ( lsw.end_date - interval \'1 year\' ) )
					                 OVERLAPS ( prev_lsw.start_date, prev_lsw.end_date ) )
					            JOIN lease_terms prev_lt ON ( prev_lsw.cid = prev_lt.cid AND prev_lsw.lease_term_id = prev_lt.id AND prev_lt.occupancy_type_id = ' . COccupancyType::STUDENT . ' AND prev_lt.deleted_by IS NULL )
				            WHERE
				              lsw.cid = lga.cid 
				              AND lsw.property_id = lg.property_id
				              AND prev_lsw.id NOT IN ( ' . implode( ',', $arrmixLeasingGoalsFilter['goal_window_ids'] ) . ' )
				              AND lsw.lease_term_id = lga.lease_term_id
				              AND lsw.id = lga.lease_start_window_id
				              AND lsw.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . '
				              AND lsw.occupancy_type_id = ' . COccupancyType::STUDENT . '
				              AND lsw.is_active = TRUE
				              AND lsw.end_date > CURRENT_DATE
				              AND lsw.deleted_on IS NULL
                        ) as lg_lsw ON TRUE
				    WHERE
							lg.cid = ' . ( int ) $intCid . '
							AND lg.property_id = ' . ( int ) $arrmixLeasingGoalsFilter['property_id'] . '
				            AND lg.id = ' . ( int ) $arrmixLeasingGoalsFilter['leasing_goal_id'];

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActualGoalDetailsByLeasingGoalsFilterByCid( $arrmixLeasingGoalsFilter, $intCid, $objDatabase ) {

		if( true == array_key_exists( 'lease_start_window_id', $arrmixLeasingGoalsFilter ) && true == valId( $arrmixLeasingGoalsFilter['lease_start_window_id'] ) ) {
			$strJoinConditionSql = ' AND ca.lease_start_window_id = ' . ( int ) $arrmixLeasingGoalsFilter['lease_start_window_id'];
		} else {
			$strJoinConditionSql = ' AND ca.lease_start_window_id IN ( ' . implode( ',', $arrmixLeasingGoalsFilter['goal_window_ids'] ) . ' )';
		}

		$strSql = 'SELECT 
						act.floorplan_name,
						us.total_unit_space_count,
						act.new_actual_count,
						act.renewal_actual_count,
						us.total_unit_space_count -(act.new_actual_count + act.renewal_actual_count) AS remaining_actual_count
					FROM (
						SELECT 
							pf.floorplan_name,
							pf.id AS property_floorplan_id,
							pf.cid,
							sub.*
						FROM 
							property_floorplans pf
							JOIN leasing_goals lg ON (lg.cid = pf.cid AND lg.property_id = pf.property_id AND pf.deleted_by IS NULL)
							LEFT JOIN LATERAL
								( SELECT 
									COALESCE(SUM(
										CASE
											WHEN sc.id is NOT NULL AND ca.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' AND ca.unit_space_id IS NULL THEN sc.unit_space_count
											WHEN ca.lease_status_type_id >= ' . CLeaseStatusType::FUTURE . ' AND ca.unit_space_id IS NOT NULL THEN lus.unit_space_count
										ELSE 1 END
									) filter( WHERE ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '), 0) AS new_actual_count, 
									COALESCE(SUM(
										CASE
											WHEN sc.id is NOT NULL AND ca.lease_status_type_id = ' . CLeaseStatusType::FUTURE . ' AND ca.unit_space_id IS NULL THEN sc.unit_space_count
											WHEN ca.lease_status_type_id >= ' . CLeaseStatusType::FUTURE . ' AND ca.unit_space_id IS NOT NULL THEN lus.unit_space_count
										ELSE 1 END
									) filter( WHERE ca.lease_interval_type_id = ' . CLeaseIntervalType::RENEWAL . '), 0) AS renewal_actual_count
								FROM 
									cached_applications ca
									LEFT JOIN space_configurations sc ON ( ca.cid = sc.cid AND ca.desired_space_configuration_id = sc.id )
									LEFT JOIN LATERAL (
											SELECT
												COUNT ( * ) AS unit_space_count
											FROM
												lease_unit_spaces lus
											WHERE
												lus.cid = ca.cid
												AND lus.property_id = ca.property_id
												AND lus.unit_space_id IS NOT NULL
												AND lus.lease_id = ca.lease_id
												AND lus.deleted_by IS NULL
												AND lus.deleted_on IS NULL
											GROUP BY
												lus.lease_id
									) lus ON TRUE
								WHERE 
										ca.cid = pf.cid AND
										ca.property_id = ' . ( int ) $arrmixLeasingGoalsFilter['property_id'] . ' AND
										ca.lease_approved_on IS NOT NULL AND
										ca.property_floorplan_id = pf.id AND
										ca.lease_status_type_id IN (' . implode( ',', CLeaseStatusType::$c_arrintFutureAndActiveLeaseStatusTypes ) . ') AND
										ca.lease_interval_type_id IN (' . CLeaseIntervalType::APPLICATION . ', ' . CLeaseIntervalType::RENEWAL . ') ' . $strJoinConditionSql . '
								) as sub ON TRUE
								WHERE 
									pf.property_id = ' . ( int ) $arrmixLeasingGoalsFilter['property_id'] . '  AND
									lg.id = ' . ( int ) $arrmixLeasingGoalsFilter['leasing_goal_id'] . ' AND
									pf.cid = ' . ( int ) $intCid . '
							) act
							JOIN LATERAL
                            (                               SELECT COUNT(us.id) as total_unit_space_count,
                                                                   us.cid,
                                                                   us.property_floorplan_id,
                                                                   us.deleted_by
                                                            FROM unit_spaces us  WHERE us.property_id = ' . ( int ) $arrmixLeasingGoalsFilter['property_id'] . ' 
                                                            AND us.cid = ' . ( int ) $intCid . ' 
                                                            AND act.property_floorplan_id = us.property_floorplan_id   
                                                            GROUP BY us.cid,
                                                                     us.property_floorplan_id,
                                                                     us.deleted_by
                                
                               )us ON ( us.cid = act.cid AND us.property_floorplan_id = act.property_floorplan_id AND us.deleted_by IS NULL ) 
							GROUP BY 
								act.cid,
								act.floorplan_name,
								act.new_actual_count,
								act.renewal_actual_count,
								us.total_unit_space_count';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		return $arrmixResult;
	}

}

?>