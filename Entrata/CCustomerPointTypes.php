<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerPointTypes
 * Do not add any new functions to this class.
 */

class CCustomerPointTypes extends CBaseCustomerPointTypes {

	public static function fetchCustomerPointTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCustomerPointType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomerPointType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCustomerPointType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>