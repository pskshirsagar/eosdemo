<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDocumentTypes
 * Do not add any new functions to this class.
 */

class CDocumentTypes extends CBaseDocumentTypes {

	public static function fetchDocumentTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDocumentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDocumentType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDocumentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDocumentTypesByIds( $arrintDocumentTypeIds, $objDatabase ) {
		if ( false == valArr( $arrintDocumentTypeIds ) ) return NULL;
		$strSql = 'SELECT * FROM document_types WHERE id IN ( ' . implode( ',', $arrintDocumentTypeIds ) . ' )';

		return self::fetchDocumentTypes( $strSql, $objDatabase );
	}

}
?>