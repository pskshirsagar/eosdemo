<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerVoteTypes
 * Do not add any new functions to this class.
 */

class CCustomerVoteTypes extends CBaseCustomerVoteTypes {

	public static function fetchCustomerVoteTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCustomerVoteType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCustomerVoteType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCustomerVoteType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>