<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyContactTypes
 * Do not add any new functions to this class.
 */

class CPropertyContactTypes extends CBasePropertyContactTypes {

	public static function fetchPropertyContactTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CPropertyContactType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPropertyContactType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CPropertyContactType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>