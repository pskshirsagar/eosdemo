<?php

class CLeadCommunicationSchedule extends CBaseLeadCommunicationSchedule {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

    	if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', ' client is required.' ) );
        }

        return $boolIsValid;
    }

    public function valFollowUpDelay() {
        $boolIsValid = true;
		$strFollowUpDelay = trim( $this->getFollowUpDelay() );
        if( ( false == empty( $strFollowUpDelay ) ) && ( false == is_numeric( $this->getFollowUpDelay() ) ) || ( 0 >= $this->getFollowUpDelay() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'follow_up_delay', 'Invalid entry, Please enter integer value greater than 0.' ) );
        }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valFollowUpDelay();
            	break;

            case VALIDATE_DELETE:
            	break;

           	default:
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }
}
?>