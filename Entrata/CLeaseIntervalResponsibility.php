<?php

class CLeaseIntervalResponsibility extends CBaseLeaseIntervalResponsibility {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseIntervalId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyResponsibilityId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valResidentIsResponsible() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>