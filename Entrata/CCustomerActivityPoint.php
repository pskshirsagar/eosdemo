<?php

class CCustomerActivityPoint extends CBaseCustomerActivityPoint {

    public function setDefaults() {

    	$this->m_strRewardDatetime = date( 'Y-m-d H:i:s' );

    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyActivityPointId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPointsEarned() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRewardDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valCustomerId();
            	$boolIsValid &= $this->valCompanyActivityPointId();
				break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }
}
?>