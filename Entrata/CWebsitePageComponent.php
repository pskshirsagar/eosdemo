<?php

class CWebsitePageComponent extends CBaseWebsitePageComponent {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsitePageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteComponentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVersion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateInputData( $objClient, $objDatabase ) {

		$boolIsValid = true;
		if( true == is_null( $this->getData() ) || false == isset( $this->getData()->text ) || false == valStr( $this->getData()->text ) ) {
			switch( $this->getWebsiteComponentId() ) {

				case CWebsiteComponent::CUSTOM_PAGE_HEADING:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Heading Content', __( 'Heading Content is required.' ) ) );
					$boolIsValid = false;
					break;

				case CWebsiteComponent::CUSTOM_PAGE_PARAGRAPH:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Paragraph text', __( 'Paragraph text content is required.' ) ) );
					$boolIsValid = false;
					break;

				case CWebsiteComponent::CUSTOM_PAGE_IMAGE:
					if( false == isset( $this->getDetails()->src ) || false == valStr( $this->getDetails()->src ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Image Src', __( 'Please select image to upload.' ) ) );
						$boolIsValid = false;
					} elseif( false == isset( $this->getDetails()->alt ) || false == valStr( $this->getDetails()->alt ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Image Alt Text', __( 'Image alt description is required.' ) ) );
						$boolIsValid = false;
					} elseif( false == isset( $this->getDetails()->marketing_media_association_id ) || false == valId( $this->getDetails()->marketing_media_association_id ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Image Src', __( 'Image is not uploaded.' ) ) );
						$boolIsValid = false;
					} else {
						$objMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationById( $this->getDetails()->marketing_media_association_id, $objDatabase );
						if( false == valObj( $objMarketingMediaAssociation, 'CMarketingMediaAssociation' ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Image Src', __( 'Media already deleted or not present.' ) ) );
							$boolIsValid = false;
						}
					}
					break;

				case CWebsiteComponent::CUSTOM_PAGE_HTML_CODE_BLOCK:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Code Block Content', __( 'Code Block Content is required.' ) ) );
					$boolIsValid = false;
					break;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClient = NULL, $objDatabase = NULL  ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->validateInputData( $objClient, $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function fetchMarketingMediaAssociationOrderNumByMediaSubTypeIds( $arrintMarketingMediaSubTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchMarketingMediaAssociationOrderNumByMediaTypeIdByMediaSubTypeIds( CMarketingMediaType::WEBSITE_CUSTOM_PAGE_MEDIA, $arrintMarketingMediaSubTypeIds, $objDatabase );
	}

}
?>