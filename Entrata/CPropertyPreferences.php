<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyPreferences
 * Do not add any new functions to this class.
 */

class CPropertyPreferences extends CBasePropertyPreferences {

	public static function fetchPropertyPreferencesByKeysByIntegrationDatabaseIdByCid( $arrstrKeys, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;
		$strSql = 'SELECT pp.*
					FROM property_preferences pp, property_integration_databases pid
					WHERE pp.cid = ' . ( int ) $intCid . '
					AND pp.property_id = pid.property_id AND pp.cid = pid.cid
					AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
					AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		return $arrobjPropertyPreferences = self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByPropertyIdsByKeysByIntegrationDatabaseIdByCid( $arrintPropertyIds, $arrstrKeys, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;
		$strSql = 'SELECT
						pp.id,
						pp.key,
						pp.value,
						pp.property_id
					FROM
						property_preferences pp,
						property_integration_databases pid
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.property_id = pid.property_id AND pp.cid = pid.cid
						AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
						AND pid.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByKeysValuesByIntegrationDatabaseIdByCid( $arrstrKeys, $intIntegrationDatabaseId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT pp.property_id,pp.key,pp.value
					FROM property_preferences pp, property_integration_databases pid
					WHERE pp.cid = ' . ( int ) $intCid . '
					AND pp.property_id = pid.property_id AND pp.cid = pid.cid
					AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . '
					AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		$arrmixPropertyPreferences = fetchData( $strSql, $objDatabase );

		$arrmixPropertyPreferencesKeysValues = [];
		$arrmixPropertyPreferencesLocal = $arrmixPropertyPreferences;
		foreach( $arrmixPropertyPreferences as $arrmixPropertyPreference ) {

			foreach( $arrmixPropertyPreferencesLocal as $intKey => $arrmixPropertyPreferenceLocal ) {
				$arrmixPropertyPreferencesKeysValues[$arrmixPropertyPreferenceLocal['property_id']][$arrmixPropertyPreferenceLocal['key']] = $arrmixPropertyPreferenceLocal;
				unset( $arrmixPropertyPreferencesLocal[$intKey] );
			}

			$arrmixPropertyPreferences = $arrmixPropertyPreferencesLocal;
		}

		return $arrmixPropertyPreferencesKeysValues;
	}

	public static function fetchCustomPropertyPreferencesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$arrobjPropertyPreferences = self::fetchPropertyPreferences( sprintf( 'SELECT * FROM %s WHERE property_id::integer = %d::integer AND cid::integer = %d::integer', 'property_preferences', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );

		$arrobjPropertyPreferencesKeyedByKey = [];

		if( true == valArr( $arrobjPropertyPreferences ) ) {
			foreach( $arrobjPropertyPreferences as $objPropertyPreference ) {
				$arrobjPropertyPreferencesKeyedByKey[$objPropertyPreference->getKey()] = $objPropertyPreference;
			}
		}

		return $arrobjPropertyPreferencesKeyedByKey;
	}

	public static function fetchCachedPropertyPreferencesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$arrmixData = CCache::fetchObject( 'Property_Preferences_' . $intCid . '_' . $intPropertyId );

		if( false === $arrmixData || true == is_null( $arrmixData ) ) {
			$strSql = sprintf( 'SELECT key, value FROM %s WHERE property_id::integer = %d::integer AND cid::integer = %d::integer', 'property_preferences', ( int ) $intPropertyId, ( int ) $intCid );

			$arrmixResults = ( array ) fetchData( $strSql, $objDatabase );
			foreach( $arrmixResults as $arrmixResult ) {
				$arrmixData[$arrmixResult['key']] = $arrmixResult['value'];
			}
			CCache::storeObject( 'Property_Preferences_' . $intCid . '_' . $intPropertyId, $arrmixData, $intSpecificLifetime = 150, [] );
		}

		return $arrmixData;
	}

	public static function fetchCachedPropertyPreferencesByKeyByPropertyIdByCid( $strKey, $intPropertyId, $intCid, $objDatabase ) {
		$objPropertyPreference = CCache::fetchObject( 'Property_Preferences_' . $intCid . '_' . $intPropertyId . '_' . $strKey );

		if( false === $objPropertyPreference || true == is_null( $objPropertyPreference ) ) {
			$objPropertyPreference = self::fetchPropertyPreferencesByKeyByPropertyIdByCid( $strKey, $intPropertyId, $intCid, $objDatabase, $boolReturnCountOnly = false );
			CCache::storeObject( 'Property_Preferences_' . $intCid . '_' . $intPropertyId . '_' . $strKey, $objPropertyPreference, $intSpecificLifetime = 120, [] );
		}

		return $objPropertyPreference;
	}

	public static function fetchPropertyPreferencesByKeyByPropertyIdByCid( $strKey, $intPropertyId, $intCid, $objDatabase, $boolReturnCountOnly = false ) {

		$strSql = 'SELECT * FROM property_preferences WHERE property_id::int = ' . ( int ) $intPropertyId . '::int
	    			AND cid::integer = ' . ( int ) $intCid . '::integer AND key = \'' . trim( $strKey ) . '\'';

		if( false == $boolReturnCountOnly ) {

	    	return self::fetchPropertyPreference( $strSql, $objDatabase );
		} else {

			$strWhereSql = ' WHERE property_id::int = ' . ( int ) $intPropertyId . '::int
			AND cid::integer = ' . ( int ) $intCid . '::integer AND key = \'' . trim( $strKey ) . '\'';

			$intDataCount = self::fetchPropertyPreferenceCount( $strWhereSql, $objDatabase );
			return ( 0 < $intDataCount ) ? 1 : 0;
		}
	}

	public static function fetchPropertyPreferencesByKeyByPropertyIdsByCid( $strKey, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT * FROM property_preferences WHERE property_id::int IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
					AND cid::integer = ' . ( int ) $intCid . ' AND key = \'' . trim( $strKey ) . '\'';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByKeysByPropertyIdByCid( $arrstrKeys, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM property_preferences WHERE property_id = ' . ( int ) $intPropertyId . '
	    			AND cid::integer = ' . ( int ) $intCid . ' AND key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

	    return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesKeysValuesByKeysByPropertyIdByCid( $arrstrKeys, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		 $strSql = 'SELECT
						key,
						value
					FROM
						property_preferences
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND CID::INTEGER = ' . ( int ) $intCid . '
						AND KEY IN ( \'' . implode( "','", $arrstrKeys ) . '\')';

		$arrmixPropertyPreferences = [];

		$arrmixPropertyPreferencesKeysValues = fetchData( $strSql, $objDatabase );

		foreach( $arrmixPropertyPreferencesKeysValues as $arrmixPropertyPreference ) {
			$arrmixPropertyPreferences[$arrmixPropertyPreference['key']] = $arrmixPropertyPreference;
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchCustomPropertyPreferencesByPropertyIdsByKeys( $arrintPropertyIds, $arrstrKeys, $objDatabase, $boolIsCheckValue = true ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM property_preferences
					WHERE property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
					AND key IN (\'' . implode( "','", $arrstrKeys ) . '\') ';

		$strSql .= ( true == $boolIsCheckValue )?'	AND value IS NOT NULL' : '';

		return self::fetchPropertyPreferences( $strSql, $objDatabase, false );
	}

	public static function fetchPropertyPreferencesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase, $boolIsCheckValue = true, $boolIsCheckZero = false, $boolUseFetchData = false, $arrmixMultipleOccupancyData = NULL ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		list( $intPsCategoryId, $arrintPsReferenceCategoryIds ) = $arrmixMultipleOccupancyData;

		$strSql = 'SELECT * FROM property_preferences
					WHERE property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
					AND cid::int = ' . ( int ) $intCid . '
					AND key IN (\'' . implode( "','", $arrstrKeys ) . '\')'
					. \Psi\Eos\Entrata\CPropertyPreferences::createService()->getPsCategoryWhereClauseSql( $intPsCategoryId, $arrintPsReferenceCategoryIds );

		$strSql .= ( true == $boolIsCheckValue )?'	AND value IS NOT NULL' : '';
		$strSql .= ( true == $boolIsCheckZero ) ? ' AND value <> \'0\'' : '';

		if( true == $boolUseFetchData ) {
			return fetchData( $strSql, $objDatabase );
		}
		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesKeysAndValuesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT 
						key, value 
					FROM 
						property_preferences
					WHERE 
						property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND cid::int = ' . ( int ) $intCid . '
						AND key IN (\'' . implode( "','", $arrstrKeys ) . '\') 
						AND value IS NOT NULL';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferenceIdByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT 
						id
					FROM 
						property_preferences
					WHERE 
						property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND cid::int = ' . ( int ) $intCid . '
						AND key IN (\'' . implode( "','", $arrstrKeys ) . '\') 
						AND value IS NOT NULL LIMIT 1';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesCountByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase, $boolIsCheckValue = true ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT count( * ) FROM property_preferences
					WHERE property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
					AND cid::int = ' . ( int ) $intCid . '
					AND key IN (\'' . implode( "','", $arrstrKeys ) . '\') ';

		$strSql .= ( true == $boolIsCheckValue )?'	AND value IS NOT NULL' : '';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintResponse ) ) return NULL;

		return $arrintResponse[0]['count'];
	}

	public static function fetchPropertyPreferencesByPropertyIdsByKeyByCid( $arrintPropertyIds, $strKey, $intCid, $objDatabase, $boolIsCheckValue = true, $boolIsGetCountOnly = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		if( true == $boolIsGetCountOnly ) {
			$strSql = 'SELECT
						COUNT( id )
					FROM
						property_preferences
					WHERE
						property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND cid::int = ' . ( int ) $intCid . '
						AND key = \'' . $strKey . '\'';

			$strSql .= ( true == $boolIsCheckValue )?'	AND value IS NOT NULL' : '';

			$arrintPropertyPreferencesCount = fetchData( $strSql, $objDatabase );

			return $arrintPropertyPreferencesCount[0]['count'];
		} else {
			$strSql = 'SELECT
						*
					FROM
						property_preferences
					WHERE
						property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND cid::int = ' . ( int ) $intCid . '
						AND key = \'' . $strKey . '\' ';

			$strSql .= ( true == $boolIsCheckValue )?'	AND value IS NOT NULL' : '';

			return self::fetchPropertyPreferences( $strSql, $objDatabase );
		}

	}

	public static function fetchPropertyPreferencesByKeyByValueByPropertyIdsByCid( $strKey, $strValue, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT * FROM property_preferences
					WHERE cid::int = ' . ( int ) $intCid . '
					AND property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
					AND key = \'' . $strKey . '\'
					AND value = \'' . $strValue . '\' ';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByPropertyIdsByKeyByValuesByCid( $arrintPropertyIds, $strKey, $arrintValues, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valStr( $strKey ) || false == valArr( $arrintValues ) || false == valId( $intCid ) ) return [];
		$strSql = 'SELECT * FROM property_preferences
					WHERE cid::int = ' . ( int ) $intCid . '
					AND property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
					AND key = \'' . $strKey . '\'
					AND value::int IN (' . implode( ',', $arrintValues ) . ')';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsCheckValue = true, $boolIsFetchKeyedByPropertyId = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
              pp.id,
              pp.cid,
              pp.key,
              pp.property_id,
              pp.property_preference_type_id,
              pp.property_setting_key_id,
              CASE
                WHEN psk.is_translated = \'t\' THEN util_get_translated(\'value\', pp.value, pp.details, p.locale_code )
                ELSE pp.value
              END AS value,
              pp.updated_by,
              pp.updated_on,
              pp.created_by,
              pp.created_on
          FROM
              property_preferences pp
              JOIN property_setting_keys psk ON( psk.id = pp.property_setting_key_id OR psk.key = pp.key )
              JOIN properties p ON( pp.cid = p.cid AND pp.property_id = p.id )
          WHERE
              pp.cid::int = ' . ( int ) $intCid . '
              AND pp.property_id::int IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ';

		$strSql .= ( true == $boolIsCheckValue ) ? ' AND value IS NOT NULL' : '';

		$arrobjPropertyPreferences = self::fetchPropertyPreferences( $strSql, $objDatabase );

		if( true == $boolIsFetchKeyedByPropertyId && true == valArr( $arrobjPropertyPreferences ) ) {
			$arrobjPropertyPreferencesTemp = [];

			foreach( $arrobjPropertyPreferences as $objPropertyPreference ) {
				$arrobjPropertyPreferencesTemp[$objPropertyPreference->getPropertyId()][$objPropertyPreference->getKey()] = $objPropertyPreference;
			}

			$arrobjPropertyPreferences = $arrobjPropertyPreferencesTemp;
		}

		return $arrobjPropertyPreferences;

	}

	public static function fetchPropertyPreferencesByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;
		$strSql = 'SELECT distinct(value) FROM property_preferences WHERE value IS NOT NULL AND cid::integer = ' . ( int ) $intCid . '::integer AND key in ( \'' . implode( "','", $arrstrKeys ) . '\') ';
	    return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByKeysByValueByCid( $arrstrKeys, $strValue, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM property_preferences WHERE cid = ' . ( int ) $intCid . ' AND key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' ) AND value = \'' . $strValue . '\'';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCustomIntegratedPropertyPreferencesByKeys( $arrstrKeys, $objDatabase ) {
    	if( false == valArr( $arrstrKeys ) ) return NULL;

    	$strSql = 'SELECT
    					pp.*
    				FROM
    					property_preferences pp,
    					properties p
    				WHERE
    					pp.property_id = p.id
    					AND pp.cid = p.cid
    					AND p.remote_primary_key IS NOT NULL
    					AND	pp.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
    					AND pp.value IS NOT NULL
    				ORDER BY
    					pp.property_id ';

    	return self::fetchPropertyPreferences( $strSql, $objDatabase );
    }

    public static function fetchCustomTransmissionPropertyPreferencesByKeys( $arrstrKeys, $objDatabase, $boolCheckIsDisabled = false ) {
    	if( false == valArr( $arrstrKeys ) ) return NULL;
    	$strIsDisabledCondition = ( true == $boolCheckIsDisabled ) ? ' AND p.is_disabled = 0' : '';

    	$strSql = 'SELECT
    					pp.*
    				FROM
    					property_preferences pp,
    					properties p
    				WHERE
    					pp.property_id = p.id AND pp.cid = p.cid
    					AND	pp.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
    					AND pp.value IS NOT NULL
    					' . $strIsDisabledCondition . '
    				ORDER BY
    					pp.property_id';

    	return self::fetchPropertyPreferences( $strSql, $objDatabase );
    }

	public static function fetchTransmissionPropertyPreferencesByKeysByCid( $arrstrKeys, $intCid,  $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;
		$strSql = 'SELECT
						pp.*
					FROM
						property_preferences pp,
						properties p
					WHERE
						pp.property_id=p.id AND pp.cid = p.cid
						AND pp.cid = ' . ( int ) $intCid . '
						AND	pp.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
						AND pp.value IS NOT NULL
					ORDER BY
						pp.property_id';
		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyCountWithCalendarByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT(id)
					FROM
						property_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND	property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND	key = \'CALL_CENTER_DASHBOARD_GCALENDAR_USERNAME\'
						AND value IS NOT NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintResponse ) ) return NULL;

		return $arrintResponse[0]['count'];
	}

	public static function fetchPropertyPreferencesByPropertyIdsByKeysByCids( $arrintPropertyIds, $arrstrKeys, $arrintCids, $objDatabase, $boolIsCheckValue = true ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_preferences
					WHERE
						property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND cid::int IN (' . implode( ',', $arrintCids ) . ')
						AND key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		$strSql .= ( true == $boolIsCheckValue )?'	AND value IS NOT NULL' : '';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferenceValuesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$arrmixPropertyPreferences = [];

		$strSql = 'SELECT
						pp.property_id,
						pp.key,
						pp.value
					FROM
						property_preferences pp
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrmixPropertyPreferences[$arrmixResponse['property_id']][$arrmixResponse['key']] = $arrmixResponse['value'];
			}
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchPropertyPreferencesByKeyByCid( $strKey, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						property_preferences
					WHERE
						cid = ' . ( int ) $intCid . ' AND key = \'' . trim( $strKey ) . '\' AND value = \'1\'';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyPreferencesByKeysByPropertyIdByCid( $arrstrKeys, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM property_preferences WHERE property_id = ' . ( int ) $intPropertyId . '
	    			AND cid::integer = ' . ( int ) $intCid . ' AND key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		$arrobjPropertyPreferences = self::fetchPropertyPreferences( sprintf( $strSql, 'property_preferences', ( int ) $intPropertyId, ( int ) $intCid, $arrstrKeys ), $objDatabase );

		$arrobjPropertyPreferencesKeyedByKey = [];

		if( true == valArr( $arrobjPropertyPreferences ) ) {
			foreach( $arrobjPropertyPreferences as $objPropertyPreference ) {
				$arrobjPropertyPreferencesKeyedByKey[$objPropertyPreference->getKey()] = $objPropertyPreference;
			}
		}

		return $arrobjPropertyPreferencesKeyedByKey;

	}

	public static function fetchPropertyPreferenceCountByKeyByValueByPropertyIdByCid( $strKey, $strValue, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count( id )
				   FROM
						property_preferences
				   WHERE
						key = \'' . trim( $strKey ) . '\'
						AND value = \'' . $strValue . '\'
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid =' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchPropertyPreferenceCountsByKeyByValueByPropertyGroupIdsByCid( $strKey, $strValue, $arrintPropertyGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) ) return false;

		$strSql = 'SELECT
						count( pp.id ),
                        lp.property_id,
						p.property_name
				   FROM
						property_preferences pp
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ], ARRAY[' . CPsProduct::ENTRATA . '] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = pp.property_id )
						JOIN properties p ON ( lp.cid = p.cid AND lp.property_id = p.id )
				   WHERE
						pp.key = \'' . trim( $strKey ) . '\'
						AND pp.value = \'' . $strValue . '\'
						AND pp.cid =' . ( int ) $intCid . '
					GROUP BY
						lp.property_id,
						p.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyPreferencesByPropertyIdByKeysByCid( $intPropertyId, $arrstrKeys, $intCid, $objDatabase, $boolIsCheckValue = true ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT id, property_id, key, value FROM property_preferences
					WHERE property_id::int = ' . ( int ) $intPropertyId . '
					AND cid::int = ' . ( int ) $intCid . '
					AND key IN (\'' . implode( "','", $arrstrKeys ) . '\') ';

		$strSql .= ( true == $boolIsCheckValue )?'	AND value IS NOT NULL' : '';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		$arrmixPropertyPreferences = [];
		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrmixPropertyPreferences[$arrmixResponse['key']] = $arrmixResponse['value'];
			}
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchSimplePropertyPreferencesByPropertyIdByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					pp.property_id,
					pp.key,
					pp.value
				FROM
					property_preferences pp
				WHERE
					property_id::int IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND cid::int = ' . ( int ) $intCid;

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		$arrmixPropertyPreferences = [];
		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrmixPropertyPreferences[$arrmixResponse['property_id']][$arrmixResponse['key']] = $arrmixResponse['value'];
			}
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchPropertyPreferenceValueByPropertyPreferenceKeyByPropertyIdByCid( $strKey, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						value
					FROM
					    property_preferences
					WHERE
						key = \'' . trim( $strKey ) . '\'
						AND property_id = ' . ( int ) $intPropertyId . '
					    AND cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'value', $objDatabase );
	}

	public static function fetchIsPropertyPreferenceSetForAllByPropertyGroupIdsByKeyByCid( $arrintPropertyGroupIds, $strKey, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						CASE
							WHEN
								COUNT( DISTINCT pp.property_id ) = COUNT( DISTINCT lp.property_id )
							THEN
								1
							ELSE
								0
						END as property_preference
					FROM
						load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $arrintPropertyGroupIds, ',' ) . '] ) lp
						LEFT JOIN property_preferences pp ON ( pp.cid= ' . ( int ) $intCid . '
						 									AND pp.property_id = lp.property_id
						                                    ' . ( ( false == $boolShowDisabledData ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
															AND pp.key = \'' . $strKey . '\'
															AND pp.value IS NOT NULL
														)';

		$arrintPropertyPreferenceCount = fetchData( $strSql, $objDatabase );

		return $arrintPropertyPreferenceCount[0]['property_preference'];
	}

	public static function fetchCustomPropertyPreferencesByKeys( $arrstrKeys, $objDatabase, $boolIsReturnKeyedArray = true ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						pp.*
					FROM
						property_preferences pp
						JOIN properties p ON(
							p.cid = pp.cid
							AND p.id = pp.property_id
						)
					WHERE
						pp.key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )
						AND pp.value IS NOT NULL
					ORDER BY
						pp.cid,
						pp.property_id';

		return self::fetchPropertyPreferences( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchPropertyPreferencesCountByKeyByWebsiteIdByCid( $strKey, $intWebsiteId, $intCid, $objDatabase ) {
		if( false == valStr( $strKey ) || false == valId( $intWebsiteId ) ) return false;

		$strSql = 'SELECT
						count(*)
					FROM
						properties AS p
						JOIN property_preferences pp ON (p.id = pp .property_id AND p.cid = pp.cid)
						JOIN website_properties AS wp ON (p.id = wp.property_id AND p.cid = wp.cid)
					WHERE
						pp.key = \'' . $strKey . '\'
						AND wp.website_id = ' . ( int ) $intWebsiteId . '
						AND p.cid = ' . ( int ) $intCid;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintResponse ) ) return NULL;

		return $arrintResponse[0]['count'];
	}

	public static function fetchCustomPropertyPreferencesByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = ' SELECT
						id, cid, property_id
					FROM
						property_preferences
					WHERE
						cid::integer = ' . ( int ) $intCid . '
						AND key IN ( \'' . implode( "','", $arrstrKeys ) . '\' ) ';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferenceByPropertyIdByKeysByCid( $intPropertyId, $arrstrPropertyPreferencesKeys, $intCid, $objDatabase, $strValueAlias = 'URL' ) {

		$strSql = ' SELECT
						id,
						key,
						value as ' . $strValueAlias . '
					FROM
						property_preferences
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
					  	AND key IN (\'' . implode( "','", $arrstrPropertyPreferencesKeys ) . '\')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByKeysByPropertyIdsByCids( $arrstrKeys, $arrintPropertyIds, $arrintCids, $objDatabase, $boolIsFetchData = false ) {
		if( false == valArr( $arrstrKeys ) || false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						property_preferences
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND key IN ( \'' . implode( "','", $arrstrKeys ) . '\' ) ';

		if( false == $boolIsFetchData ) {
			return self::fetchPropertyPreferences( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchPropertyPreferencesByKeysByPropertyGroupIdsByCid( $arrstrKeys, $arrintPropertyGroupIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) || false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = ' SELECT
						sub.*
					FROM
						(
						  SELECT
							  pp.key,
							  pp.value,
							  COUNT ( pp.key ) AS pp_count,
							  lp.count AS property_count
						  FROM
							  property_preferences pp
							  JOIN
							  (
								SELECT
									lp.property_id,
									lp.cid,
									lp.is_disabled,
									COUNT ( lp.property_id ) OVER ( partition BY lp.cid )
								FROM
									load_properties ( ARRAY[' . ( int ) $intCid . '], array[ ' . implode( ',', $arrintPropertyGroupIds ) . ' ], ARRAY[' . CPsProduct::ENTRATA . '] ) lp
							  ) AS lp ON ( lp.is_disabled = 0 AND lp.property_id = pp.property_id )
						  WHERE
							  pp.cid =' . ( int ) $intCid . '
							  AND KEY IN ( \'' . implode( "','", $arrstrKeys ) . '\' )
						  GROUP BY
							  pp.key,
							  pp.value,
							  lp.count
						) sub
					WHERE
						sub.pp_count = sub.property_count';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyPreferencesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase ) {
		if( false == valArr( array_filter( $arrintPropertyIds ) ) || false == valArr( $arrstrKeys ) ) return NULL;

		$arrmixPropertyPreferences = [];

		$strSql = 'SELECT * FROM property_preferences
					WHERE
						property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND key IN (\'' . implode( "','", $arrstrKeys ) . '\')
						AND cid = ' . ( int ) $intCid . '
						AND value IS NOT NULL';

		$arrintResponses = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintResponses ) ) return NULL;

		foreach( $arrintResponses as $arrintResponse ) {
			$arrmixPropertyPreferences[$arrintResponse['property_id']][$arrintResponse['key']] = $arrintResponse['value'];
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchPropertyPreferencesByKeyByValuesByPropertyIdsByCid( $strKey, $arrstrValues, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrValues ) ) return NULL;

		$strSql = 'SELECT
						value,
						count(property_id) as properties_count,
						array_to_string ( array_agg ( property_id ), \',\' ) AS property_ids
					FROM
						property_preferences
					WHERE
						cid::int = ' . ( int ) $intCid . '
						AND property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND key = \'' . $strKey . '\'
						AND value::int IN ( ' . implode( ',', $arrstrValues ) . ' )
					GROUP BY
						value';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimplePropertyPreferenceValuesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$arrmixPropertyPreferences = [];

		$strSql = 'SELECT
						pp.property_id,
						pp.key,
						pp.value
					FROM
						property_preferences pp
						JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) lp ON ( pp.cid = lp.cid AND pp.property_id = lp.property_id AND lp.is_disabled = ' . ( ( false == $boolShowDisabledData ) ? 0 : 1 ) . ' )
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrmixPropertyPreferences[$arrmixResponse['property_id']][$arrmixResponse['key']] = $arrmixResponse['value'];
			}
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchPropertyPreferencesCountAndValuesByPropertyGroupIdsByPropertyValueGroupsByKeyByCid( $arrintPropertyGroupIds, $strKey, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = '	SELECT
                        count ( lp.property_id ) AS property_preference_count,
                        pp.value
					FROM
                        load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . implode( $arrintPropertyGroupIds, ', ' ) . ' ], ARRAY [ ]::INT [ ], TRUE ) lp
                        LEFT JOIN property_preferences pp ON ( lp.property_id = pp.property_id AND pp.key = \'' . $strKey . '\' )
					GROUP BY 
						pp.value';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByPropertyGroupIdsByKeyByCid( $arrintPropertyGroupIds, $strKey, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						pp.id,
						pp.cid,
						pp.property_id,
						pp.property_setting_key_id,
						pp.key,
						pp.value
					FROM
						load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $arrintPropertyGroupIds, ',' ) . '] ) lp
						JOIN property_preferences pp ON ( pp.cid= ' . ( int ) $intCid . '
															AND pp.property_id = lp.property_id
															AND lp.is_disabled = ' . ( ( false == $boolShowDisabledData ) ? 0 : 1 ) . '
															AND pp.key = \'' . $strKey . '\'
															AND pp.value IS NOT NULL
														)';
		$arrmixPropertyPreference = fetchData( $strSql, $objDatabase );
		return $arrmixPropertyPreference = ( true == valArr( $arrmixPropertyPreference ) ) ? rekeyArray( 'property_id', $arrmixPropertyPreference ) : [];
	}

	public static function fetchDelinquentPropertiesCountByPropertyIdsByCid( $arrintPropertyIds, $arrintDelinquencyLevelTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintDelinquencyLevelTypeIds ) ) return NULL;

		$strSql = ' SELECT
						value As delinquency_type_id,
						count( id )
					FROM
						property_preferences
					WHERE
						cid =' . ( int ) $intCid . '
						AND key = \'' . CPropertyPreference::DELINQUENCY_LEVEL . '\'
						AND value IN ( \'' . implode( "','", $arrintDelinquencyLevelTypeIds ) . '\')
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						GROUP BY value';

		$arrmixPropertyPreferences = NULL;
		$arrmixResponse = fetchData( $strSql, $objDatabase );
		if( false == valArr( $arrmixResponse ) ) return NULL;
		foreach( $arrmixResponse as $arrmixPropertyPreference ) {
			$arrmixPropertyPreferences[$arrmixPropertyPreference['delinquency_type_id']] = $arrmixPropertyPreference['count'];
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchCustomApExportTypesInUseByCid( $intCid, $objDatabase, $boolIsAdministrator = true, $intCompanyUserId = 0 ) {
		$strSql = 'SELECT
						value AS batch_type_id,
						aebt.name
					FROM
						property_preferences pp
						JOIN property_gl_settings pgs ON ( pp.property_id = pgs.property_id AND pp.cid = pgs.cid )
						JOIN properties p ON ( pp.property_id = p.id AND pp.cid = p.cid )
						JOIN ap_export_batch_types aebt ON ( aebt.id = pp.value::INTEGER )';
		if( false == $boolIsAdministrator ) {
			$strSql .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = pp.property_id AND vcup.cid = pp.cid ) ';
		}
		$strSql .= ' WHERE
						KEY = \'AP_EXPORT_BATCH_TYPE_SELECT\'
						AND pgs.activate_standard_posting = true
						AND pp.cid = ' . ( int ) $intCid . ' 
						AND p.is_disabled = 0
					GROUP BY
						pp.value,
						aebt.id
					ORDER BY 
						aebt.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsByWeekendandHolidaySettingsByCid( $intCid, $objDatabase ) {

		$strSql = '
					SELECT
					    p.cid,
					    p.id AS property_id,
					    LEAST(CASE
					            WHEN holiday_setting.property_id IS NULL THEN TRUE::INTEGER
					            ELSE holiday_setting.send_late_notice
					          END, CASE
					                 WHEN pp_lw.id IS NOT NULL AND pp_lw.value = \'0\' THEN FALSE::INTEGER
					                 ELSE TRUE::INTEGER
					               END) AS send_late_notice,
					    LEAST(CASE
					            WHEN holiday_setting.property_id IS NULL THEN TRUE::INTEGER
					            ELSE holiday_setting.send_demand_notice
					          END, CASE
					                 WHEN pp_dw.id IS NOT NULL AND pp_dw.value = \'0\' THEN FALSE::INTEGER
					                 ELSE TRUE::INTEGER
					               END) AS send_demand_notice
					FROM
					    properties p
					    LEFT JOIN property_preferences pp_lw ON (pp_lw.cid = p.cid AND pp_lw.property_id = p.id AND (6 = DATE_PART(\'dow\', NOW()) OR 0 = DATE_PART(\'dow\', NOW())) AND pp_lw.key
					        = \'SEND_LATE_NOTICES_ON_WEEKENDS\')
					    LEFT JOIN property_preferences pp_dw ON (pp_dw.cid = p.cid AND pp_dw.property_id = p.id AND (6 = DATE_PART(\'dow\', NOW()) OR 0 = DATE_PART(\'dow\', NOW())) AND pp_dw.key
					        = \'SEND_DEMAND_NOTICES_ON_WEEKENDS\')
					    LEFT JOIN
					    (
					      SELECT
					          ph.property_id,
					          ph.cid,
					          CASE
					            WHEN pp_lh.id IS NOT NULL AND pp_lh.value = \'0\' THEN FALSE::INTEGER
					            ELSE TRUE::INTEGER
					          END send_late_notice,
					          CASE
					            WHEN pp_dh.id IS NOT NULL AND pp_dh.value = \'0\' THEN FALSE::INTEGER
					            ELSE TRUE::INTEGER
					          END send_demand_notice
					      FROM
					          property_holidays ph
					          JOIN company_holidays ch ON (ch.cid = ph.cid AND ch.id = ph.company_holiday_id)
					          LEFT JOIN property_preferences pp_lh ON (pp_lh.cid = ph.cid AND pp_lh.property_id = ph.property_id AND pp_lh.key = \'SEND_LATE_NOTICES_ON_HOLIDAYS\')
					          LEFT JOIN property_preferences pp_dh ON (pp_dh.cid = ph.cid AND pp_dh.property_id = ph.property_id AND pp_dh.key = \'SEND_DEMAND_NOTICES_ON_HOLIDAYS\')
					      WHERE
					          ch.date = CURRENT_DATE
					    ) AS holiday_setting ON (holiday_setting.cid = p.cid AND holiday_setting.property_id = p.id)
					WHERE
					    p.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

	public static function resetDelinquencyPropertyPreferences( $intCid, $intUserId, $objDatabase ) {

		$strUpdateSql = 'UPDATE property_preferences
							SET value = ' . CDelinquencyLevelType::LEVEL_ZERO . ',
							updated_by = ' . ( int ) $intUserId . ',
							updated_on = \'NOW()\'
						WHERE
							key = \'' . CPropertyPreference::DELINQUENCY_LEVEL . '\'
							AND cid = ' . ( int ) $intCid . '
							AND value::int <> ' . CDelinquencyLevelType::LEVEL_ZERO;

		if( false == $objDatabase->execute( $strUpdateSql ) ) {
			return false;
		}
		return true;
	}

	public static function fetchPropertyPreferenceByPropertyIdByKeyByCid( $intPropertyId, $strPropertyPreferencesKey, $intCid, $objDatabase, $boolCustomFields = false ) {
		$strSubSql = NULL;
		if( false == valId( $intPropertyId ) || false == valStr( $strPropertyPreferencesKey ) || false == valId( $intCid ) ) return NULL;

		if( true == $boolCustomFields ) {
			$strSubSql = ' cid, property_id, property_preference_type_id, ';
		}

		$strSql = ' SELECT
						id,
						' . $strSubSql . '
						key,
						value
					FROM
						property_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND key = \'' . $strPropertyPreferencesKey . '\' ';

		return self::fetchPropertyPreference( $strSql, $objDatabase );
	}

	public static function fetchGuestCardPropertyPreferencesCountByPropertyIdsByGroupIdsByCid( $arrintPropertyIds, $arrintPropertySettingGroupIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintPropertySettingGroupIds ) ) return NULL;

		$strSql = 'SELECT
				   		pp.key,
						COUNT( pp.property_id ) AS count_exists
					FROM property_preferences pp
						JOIN properties p ON ( p.cid = pp.cid AND p.id = pp.property_id AND p.is_disabled <> 1 )
						JOIN property_setting_keys psk ON ( pp.key = psk.key AND psk.id = pp.property_setting_key_id )
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND psk.property_setting_group_id IN( ' . implode( ',', $arrintPropertySettingGroupIds ) . ' )
					GROUP BY
						pp.key';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $intKey => $arrstrData ) {
				$arrmixCountData[$arrstrData['key']] = $arrstrData['count_exists'];
			}
		}

		return $arrmixCountData;
	}

	public static function fetchCustomPropertyPreferencesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolFetchPropertyPreferenceValue = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSelectSubSql = ( true == $boolFetchPropertyPreferenceValue ) ? ', value, property_id' : '';

		$strSql = 'SELECT
						id,
						key
					' . $strSelectSubSql . '
					FROM
						property_preferences
					WHERE
						cid::int = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		if( false == $boolFetchPropertyPreferenceValue ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			$arrmixPropertyPreferences = self::fetchPropertyPreferences( $strSql, $objDatabase );
			$arrmixPropertyPreferences = ( true == valArr( $arrmixPropertyPreferences ) ) ? rekeyObjects( 'PropertyId', $arrmixPropertyPreferences, $boolHasMultipleObjectsWithSameKey = true ) : [];

            $arrmixPropertyPreferencesByKey = [];

            if( true == valArr( $arrmixPropertyPreferences ) ) {
				foreach( $arrmixPropertyPreferences as $intPropertyId => $arrmixPropertyPreference ) {
					$arrmixPropertyPreferencesByKey[$intPropertyId] = rekeyObjects( 'Key', $arrmixPropertyPreferences[$intPropertyId] );
				}
			}

			return ( true == valArr( $arrmixPropertyPreferencesByKey ) ) ? $arrmixPropertyPreferencesByKey : [];
		}

	}

	public static function fetchProductPropertyPreferencesByKeyByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT 
						ppr.property_id,
						ppr.value,
						ppr.key
					 FROM
						property_preferences ppr
						JOIN property_products pp ON( ppr.cid = pp.cid AND ppr.property_id = pp.property_id )
					 WHERE
						ppr.property_id::int IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
						AND ppr.cid::integer = ' . ( int ) $intCid . '
						AND ppr.key IN ( \'' . implode( "','", $arrstrKeys ) . '\' )
						AND pp.ps_product_id = ' . ( int ) CPsProduct::LEASE_EXECUTION;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyPreferencesByKeyByPropertyIdsByCid( $strKey, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						ppr.property_id,
						ppr.value,
						p.property_type_id,
						CASE WHEN ( ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids) AND ppf.value = \'1\' ) THEN 1 ELSE 0 END AS is_student_semester_selection_enabled
					FROM
						properties p
						JOIN property_preferences ppr ON ( p.cid = ppr.cid AND p.id = ppr.property_id )
						LEFT JOIN property_preferences ppf ON ( p.cid = ppf.cid AND p.id = ppf.property_id AND ppf.key = \'ENABLE_SEMESTER_SELECTION \' )
					WHERE
						ppr.property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND ppr.cid::integer = ' . ( int ) $intCid . '
						AND ppr.key = \'' . trim( $strKey ) . '\'
						AND p.disabled_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIntegratedPropertyPreferencesByPropertyIdsByKeysByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pp.property_id,
						pp.value,
						pp.key
					FROM
						property_preferences pp
						LEFT JOIN properties p ON ( pp.property_id = p.id AND pp.cid = p.cid )
					WHERE
						p.remote_primary_key IS NOT NULL
						AND pp.cid = ' . ( int ) $intCid . '
						AND	pp.property_id IN (' . implode( ',', $arrintPropertyIds ) . ' )
						AND pp.key IN ( \'' . implode( "','", $arrstrKeys ) . '\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomKeyForMaintenancePropertyPreferencesByKeyByPropertyIdsByCid( $strKey, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT pp.property_id,
						(CASE WHEN pp.value IS NOT NULL  THEN 1 ELSE 0 END ) AS value
					FROM
						properties p 
						JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id )
						JOIN property_products AS ppd ON ( p.cid = ppd.cid AND p.id = ppd.property_id )
					WHERE
						pp.property_id::int IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
						AND pp.cid::integer = ' . ( int ) $intCid . ' AND pp.key = \'' . trim( $strKey ) . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomKeysPropertyPreferencesByKeysByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT pp.property_id,
						(CASE WHEN pp.id IS NOT NULL THEN 1 ELSE 0 END ) AS value,
						pp.key
					FROM
						properties p 
						JOIN property_preferences pp ON ( p.cid = pp.cid AND p.id = pp.property_id )
					WHERE 
						( pp.value IS NOT NULL AND pp.value != \'NONE\' )
						AND pp.property_id::int IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
						AND pp.cid::integer = ' . ( int ) $intCid . ' AND pp.key IN ( \'' . implode( "','", $arrstrKeys ) . '\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyPreferenceDetailsByKeyByPropertyIdsByCid( $strKey, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pp.id,
						pp.property_id,
						pp.key,
						pp.value
					FROM
						property_preferences pp
					WHERE
						pp.property_id::int IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
						AND pp.cid::integer = ' . ( int ) $intCid . ' AND pp.key = \'' . trim( $strKey ) . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchResidentInsurePropertyPreferenceDetailsByKeysByPropertyIdsByCid( $strOccupiedUnitsKey, $strFrequencyReminderKey, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						p.id AS property_id,
						pp.value AS frequency_reminder,
						pp1.value AS occupied_units_value
					FROM
						properties p
						LEFT JOIN property_preferences pp1 ON ( pp1.property_id = p.id AND pp1.cid = p.cid AND pp1.key = \'' . trim( $strOccupiedUnitsKey ) . '\' )
						LEFT JOIN property_preferences pp ON ( pp.property_id = p.id AND pp.cid = p.cid AND pp.key = \'' . trim( $strFrequencyReminderKey ) . '\' AND pp.value::int > 0 )
					WHERE
						p.id::int IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
						AND p.cid::integer = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPropertyPreferencesByKeysByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						pp.property_id,
						pp.value,
						pp.key,
						p.property_type_id,
						psk.label AS name
					FROM
						property_preferences pp
						LEFT JOIN properties p ON ( p.id = pp.property_id AND p.cid = pp.cid )
						LEFT JOIN property_setting_keys psk ON ( psk.id = pp.property_setting_key_id )
					WHERE
						pp.key IN ( \'' . implode( "','", $arrstrKeys ) . '\' )
						AND pp.cid = ' . ( int ) $intCid . '
						AND pp.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesForFloorSelectionByKeysByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
			          p.id as property_id,
			          p.property_name as name,
			          pp.ps_product_id,
			          ppr.key,
			          ppr.value,
			          CASE WHEN ( ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND ppf.value = \'1\' ) THEN 1 ELSE 0 END AS is_student_semester_selection_enabled
			      FROM
			          properties p
			          LEFT JOIN property_preferences ppr ON ( p.cid = ppr.cid AND p.id = ppr.property_id AND ppr.key IN ( \'' . implode( "','", $arrstrKeys ) . '\' ) )
			          LEFT JOIN property_preferences ppf ON ( p.cid = ppf.cid AND p.id = ppf.property_id AND ppf.key = \'ENABLE_SEMESTER_SELECTION\' )
			          LEFT JOIN property_products AS pp ON ( p.cid = pp.cid AND p.id = pp.property_id AND pp.ps_product_id = ' . CPsProduct::STUDENT_HOUSING . ' ) 
			      WHERE
			          p.id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
			          AND p.cid::integer = ' . ( int ) $intCid . '
			          AND p.disabled_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultSettingsByPropertyIdsByCidByPropertySettingGroupIds( $arrintPropertyIds, $intCid, $arrintPropertySettingGroups, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
					    pp.property_id,
					    psg.name
					FROM
					    property_preferences pp
					    JOIN property_setting_keys psk ON psk.key = pp.key
					    JOIN property_setting_groups psg ON psg.id = psk.property_setting_group_id 
					WHERE
					    pp.cid::integer = ' . ( int ) $intCid . '
					    AND pp.property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
					    AND psg.id IN (' . implode( ',', $arrintPropertySettingGroups ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCustomPropertyPreferencesByKeyByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase, $intProductId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strTableCond = ( true == valId( $intProductId ) ) ? ' JOIN property_products pp ON( ppr.property_id = pp.property_id AND ppr.cid = pp.cid )': '';
		$strCond = ( true == valId( $intProductId ) ) ? ' pp.ps_product_id = ' . ( int ) $intProductId . ' AND': '';

		$strSql = 'SELECT
						DISTINCT ppr.property_id,
						(CASE WHEN ppr.id IS NOT NULL THEN 1 ELSE 0 END ) AS value
					 FROM
						property_preferences ppr
						' . $strTableCond . '
					 WHERE
						' . $strCond . '
						( ppr.key IN  (\'' . implode( "','", $arrstrKeys ) . '\') )
						AND ppr.value != \'0\'
						AND ppr.property_id::int IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
						AND ppr.cid::integer = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomLeasingPropertyPreferencesByKeyByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ppr.property_id
					 FROM
						property_preferences ppr
						JOIN property_preferences ppr1 ON ( ppr.property_id = ppr1.property_id AND ppr.cid = ppr1.cid )
					 WHERE
						( ( ppr.key = \'SIGNING_PROCEDURE\' and ppr.value != \'one_page\' ) OR ( ppr1.key = \'TURN_ON_ROUNDING_WITHIN_LEASE\' and ppr1.value::int != 0 ) )
						AND ppr.property_id::int IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
						AND ppr.cid::integer = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMigrationSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						p.id as property_id
					FROM
						properties p
                        JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
					WHERE
						p.id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND p.cid::integer = ' . ( int ) $intCid . '
						AND ( pgs.is_ap_migration_mode = FALSE AND pgs.is_ar_migration_mode = FALSE AND pgs.is_asset_migration_mode = FALSE )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScreeningCustomPropertyPreferencesByKeyByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $intProductId = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						 DISTINCT ptv.property_id
					 FROM
						property_transmission_vendors ptv
						JOIN company_transmission_vendors ctv ON ( ctv.cid = ptv.cid AND ctv.id = ptv.company_transmission_vendor_id AND ctv.transmission_vendor_id = ' . CTransmissionVendor::RESIDENT_VERIFY . ' )
						JOIN property_products pp ON ( ptv.property_id = pp.property_id AND ptv.cid = pp.cid AND pp.ps_product_id = ' . ( int ) $intProductId . ' )
					 WHERE
						ptv.property_id IN (' . implode( ',', array_filter( $arrintPropertyIds ) ) . ')
						AND ptv.cid =' . ( int ) $intCid . '
						AND ctv.transmission_type_id = ' . CTransmissionType::SCREENING;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPropertyPreferencesByKeysByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						pp.property_id,
						pp.value
					FROM
						property_preferences pp
						LEFT JOIN properties p ON ( p.cid = pp.cid AND p.id = pp.property_id )
					WHERE
						pp.key IN ( \'' . implode( "','", $arrstrKeys ) . '\' )
						AND pp.cid = ' . ( int ) $intCid . '
						AND pp.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMoveInPropertyPreferencesByKeyByPropertyIdsByCid( $strKey, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						p.id as property_id,
					    p.occupancy_type_ids,
					    pp.value,
					    pp.key,
					    psk.label AS name
					FROM
						properties p
					    LEFT JOIN property_preferences pp ON ( p.id = pp.property_id AND p.cid = pp.cid AND pp.key = \'' . $strKey . '\' )
					    LEFT JOIN property_setting_keys psk ON ( psk.id = pp.property_setting_key_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDistinctPropertyPreferenceValuesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase, $boolShowDisabledData = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$arrmixPropertyPreferences = [];

		$strSql = '	SELECT 
     	                string_agg(subquery.value,\',\'ORDER BY subquery.key) as VALUE, 
     	                subquery.key 
     	            FROM( 
     	                    SELECT
								distinct( pp.value),
								pp.key
							FROM
								property_preferences pp
								JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY[ ' . implode( ',', $arrintPropertyIds ) . ' ] ) lp ON ( pp.cid = lp.cid AND pp.property_id = lp.property_id AND lp.is_disabled = ' . ( ( false == $boolShowDisabledData ) ? 0 : 1 ) . ' )
							WHERE
								pp.cid = ' . ( int ) $intCid . '
								AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\') 
						) subquery
					GROUP BY subquery.key ';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrmixPropertyPreferences[$arrmixResponse['key']] = $arrmixResponse['value'];
			}
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchPropertyPreferencesByPropertySettingGroupIdByPropertyIdByCid( $intPropertySettingGroupId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT 
						pp.*
					FROM	
					    property_preferences pp
                        JOIN property_setting_keys psk on ( pp.key = psk.key and pp.property_setting_key_id = psk.id )
					WHERE pp.cid = ' . ( int ) $intCid . '
                        AND pp.property_id = ' . ( int ) $intPropertyId . ' 
                        AND psk.property_setting_group_id = ' . ( int ) $intPropertySettingGroupId . '
                   ';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );

	}

	public static function fetchInsurancePropertyPreferencesByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM property_preferences WHERE cid::integer = ' . ( int ) $intCid . '::integer AND key in ( \'' . implode( "','", $arrstrKeys ) . '\') ';
		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByKeyByPropertyIdsByCids( $strKey, $arrintPropertyIds, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT pp.property_id,
						pp.cid,
						pp.value
					FROM
						property_preferences pp
						JOIN properties p ON ( p.id = pp.property_id AND p.cid = pp.cid )
						JOIN events e ON ( e.property_id = pp.property_id AND e.cid = pp.cid )
					WHERE
						pp.key = \'' . $strKey . '\' 
						AND pp.value IS NOT NULL
						AND pp.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND pp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND e.event_type_id IN( ' . CEventType::CALL_INCOMING . ', ' . CEventType::VOICE_MAIL . ' )
						AND e.event_result_id IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesCountByPropertyGroupIdsByKeyByValueByCid( $arrintPropertyGroupIds, $strKey, $strValue, $intCid, $objDatabase, $boolIsloadAllProperties = false ) {
		if( false == valArr( $arrintPropertyGroupIds ) || false == valStr( $strKey ) ) return NULL;
		$strAndSql = '';
		if( false == $boolIsloadAllProperties ) {
			$strAndSql = ' AND lp.is_disabled = 0';
		}

		$strSql = 'SELECT
						count(pp.id)
					FROM
						load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $arrintPropertyGroupIds, ',' ) . '] ) lp
						JOIN property_preferences pp ON ( pp.cid= ' . ( int ) $intCid . '
															AND pp.property_id = lp.property_id
															' . $strAndSql . '
															AND pp.key = \'' . $strKey . '\'
															AND pp.value = \'' . $strValue . '\'
														)';
		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( false == valArr( $arrintResponse ) ) return NULL;
		return $arrintResponse[0]['count'];
	}

	public static function fetchPropertyPreferencesDetailsByKeysByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pp.property_id
					FROM property_preferences pp
					WHERE 
						pp.key = \'' . $arrstrKeys[0] . '\' AND
						pp.value = \'1\' AND
						pp.cid = ' . ( int ) $intCid . ' AND
						pp.property_id IN ( 
											SELECT 
												pp.property_id
											FROM 
												property_preferences pp
											WHERE 
												pp.key = \'' . $arrstrKeys[1] . '\' AND
												pp.value = \'1\' AND
												pp.cid = ' . ( int ) $intCid . ' AND
												pp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPropertyPreferencesByKeysByPropertyIdsByCid( $arrstrKeys, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						pp.property_id,
						count( pp.id ) AS value
					FROM
						property_preferences pp
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')
						AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\')
						AND pp.value IS NOT NULL
					GROUP BY
						pp.property_id
					HAVING count( pp.id ) = ' . \Psi\Libraries\UtilFunctions\count( $arrstrKeys );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferenceValuesByKeysByCid( $arrstrPropertyPreferencesKeys, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						id,
						cid,
						property_id,
						key,
						value
					FROM
						property_preferences
					WHERE
						cid = ' . ( int ) $intCid . '
					  	AND key IN (\'' . implode( "','", $arrstrPropertyPreferencesKeys ) . '\')';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferenceValuesByKeysByCids( $arrstrPropertyPreferencesKeys, $arrintCids, $objDatabase ) {

		if( false == valArr( $arrstrPropertyPreferencesKeys ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						id,
						cid,
						property_id,
						key,
						value
					FROM
						property_preferences
					WHERE
						cid IN (' . implode( ',', $arrintCids ) . ')
					  	AND key IN (\'' . implode( "','", $arrstrPropertyPreferencesKeys ) . '\')';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesKeysAndValuesByPropertyIdByKeysByCid( $intPropertyId, $arrstrKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT 
						key, value 
					FROM 
						property_preferences
					WHERE 
						property_id::int = ' . ( int ) $intPropertyId . '
						AND cid::int = ' . ( int ) $intCid . '
						AND key IN (\'' . implode( "','", $arrstrKeys ) . '\') 
						AND value IS NOT NULL';

		$arrmixResults = ( array ) fetchData( $strSql, $objDatabase );
		$arrmixData = [];
		foreach( $arrmixResults as $arrmixResult ) {
			$arrmixData[$arrmixResult['key']] = $arrmixResult['value'];
		}

		return $arrmixData;
	}

	public static function fetchPropertyPreferencesKeysAndValuesByKeysByPropertyIdByCid( $arrstrKeys, $intPropertyId, $intCid, $objDatabase, $strDate = NULL ) {
		if( false == valArr( $arrstrKeys ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						psk.key,
						pp.value,
						psl.new_value as value_on_date,
						psl.new_text,
						psl.created_on::DATE
					FROM
						property_setting_keys psk
						LEFT JOIN property_preferences pp ON ( psk.id = pp.property_setting_key_id AND pp.cid = ' . ( int ) $intCid . ' AND pp.property_id = ' . ( int ) $intPropertyId . ' )
						LEFT JOIN
								(
									SELECT
										property_setting_key_id,
										new_value,
										new_text,
										created_on::DATE,
										rank ( ) OVER ( PARTITION BY property_setting_key_id, created_on::DATE ORDER BY id DESC )
									FROM
										property_setting_logs
									WHERE
										cid = ' . ( int ) $intCid . '
										AND property_id = ' . ( int ) $intPropertyId . '
								) psl ON ( psk.id = psl.property_setting_key_id AND psl.created_on::DATE = \'' . $strDate . '\' AND psl.rank = 1 )
					WHERE
						psk.key IN ( ' . sqlStrImplode( $arrstrKeys ) . ' )';

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferenceByCidByPropertyIdByKey( $intCid, $intPropertyId, $strKey, $objDatabase ) {

		if( false == valStr( $strKey ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						pp.*
					FROM
						property_preferences pp
					WHERE
						pp.cid = ' . ( int ) $intCid . '
						AND pp.property_id = ' . ( int ) $intPropertyId . '
						AND pp.key = \'' . $strKey . '\'';

		return self::fetchPropertyPreference( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesCompanyEmployeesByKeysByCidsByPropertyIds( $arrstrKeys, $arrintCids, $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) || false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						pp.property_id,
						pp.key,
						pp.value,
						ce.email_address,
						ce.name_first
					FROM
						property_preferences pp
						LEFT JOIN company_employees ce ON ( ce.email_address = ANY ( string_to_array ( regexp_replace( pp.value, \' \', \'\'), \',\' ) ) )
					WHERE
						property_id::int IN (' . sqlIntImplode( $arrintPropertyIds ) . ')
						AND pp.cid::int IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND key IN ( \'' . implode( '\', \'', $arrstrKeys ) . '\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferenceCountByKeysByCompanyStatusTypeId( $strKey, $objDatabase ) : array {
		if( false === valStr( $strKey ) ) {
			return [];
		}

		$strSql = 'SELECT 
						count( DISTINCT (pp.cid) ) as clients_count, 
						count( DISTINCT (pp.property_id) ) properties_count
					FROM 
						property_preferences pp
						JOIN clients c ON (c.id = pp.cid AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
						JOIN properties p ON (p.cid = pp.cid AND p.id = pp.property_id)
					WHERE 
						pp.key = \'' . $strKey . '\' 
						AND pp.value = \'1\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTransmissionVendorTypesInUseByTransmissionTypeIdByKeyByCid( $intTransmissionTypeId, $strKey, $intCid, $objDatabase, $boolIsAdministrator = true, $intCompanyUserId = NULL, $boolIsShowDisabled = false ) {

		$strAndCondition  = ' AND p.is_disabled = 0';
		if( false != $boolIsShowDisabled ) {
			$strAndCondition  = ' AND p.is_disabled <> 0
			AND pp1.ps_product_id = ' . \CPsProduct::HISTORICAL_ACCESS;
		}

		$strSql = 'SELECT
						DISTINCT tv.id AS batch_type_id,
						tv.name,
						util_get_system_translated( \'name\', tv.name, tv.details ) AS name
					FROM
						property_preferences pp
						JOIN property_gl_settings pgs ON ( pp.property_id = pgs.property_id AND pp.cid = pgs.cid )
						JOIN properties p ON ( pp.property_id = p.id AND pp.cid = p.cid )
						JOIN property_products pp1 ON ( pgs.cid = pp1.cid AND ( p.id = pp1.property_id OR pp1.property_id IS NULL ) )
						JOIN transmission_vendors tv ON (tv.id :: text = ANY ( string_to_array ( regexp_replace( regexp_replace(pp.value, \'[\]]\', \'\'),\'[\[]\', \'\'), \',\' ) ) )';
					if( false == $boolIsAdministrator ) {
						$strSql .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = pp.property_id AND vcup.cid = pp.cid ) ';
					}
				$strSql .= ' WHERE
								KEY = ' . pg_escape_literal( $objDatabase->getHandle(), $strKey ) . '
								AND pgs.activate_standard_posting = true
								AND tv.transmission_type_id = ' . ( int ) $intTransmissionTypeId . '
								AND pp.cid = ' . ( int ) $intCid . ' 
								' . $strAndCondition . '
								AND tv.is_published = 1
							GROUP BY
								pp.value,
								tv.id
							ORDER BY 
								tv.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertiesByKeyByValuesByCompanyUserIdByCid( $strKey, $arrstrValues, $intCompanyUserId, $intCid, $boolIsAdministrator, $objDatabase ) {
		$strJoin = '';

		if( false == $boolIsAdministrator ) {
			$strJoin .= 'JOIN view_company_user_properties vcup ON ( vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND vcup.property_id = p.id AND vcup.cid = p.cid ) ';
		}
		$strSql = 'SELECT
					DISTINCT p.id, lower( p.property_name )
					FROM
					property_preferences ppr
					JOIN properties p ON ( p.id = ppr.property_id AND p.cid = ppr.cid )
					JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id AND pgs.activate_standard_posting = true )
					JOIN property_products pp ON ( pgs.cid = pp.cid AND ( p.id = pp.property_id OR pp.property_id IS NULL ) )
					
					' . $strJoin . '
					WHERE
					p.cid = ' . ( int ) $intCid . '
					AND ppr.key = ' . pg_escape_literal( $strKey ) . ' 
					AND string_to_array ( trim( ppr.value, \'[]\'), \',\' ) <@ string_to_array( \'' . implode( ',', $arrstrValues ) . ' \', \',\' )
					AND p.is_disabled = 0 
					ORDER BY
					lower( p.property_name ) ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyPreferencesByCidsByKeys( $arrintCids, $arrstrKeys, $objDatabase, $boolIsCheckValue = true ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_preferences
					WHERE
						cid::int IN (' . implode( ',', $arrintCids ) . ')
						AND key IN (\'' . implode( "','", $arrstrKeys ) . '\')';

		$strSql .= ( true == $boolIsCheckValue )?'	AND ( value IS NOT NULL OR value <> \'0\' )' : '';

		return self::fetchPropertyPreferences( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdsByKeyByValuesByCid( $strKey, $arrstrValues, $intCid, $objDatabase ) {
		if( false == valStr( $strKey ) || false == valArr( $arrstrValues ) ) return NULL;

		$strSql = 'SELECT
						property_id
					FROM
						property_preferences
					WHERE
						cid::int = ' . ( int ) $intCid . '
						AND key = \'' . $strKey . '\'
						AND value IN ( ' . "'" . implode( "', '", $arrstrValues ) . "'" . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSettingUnitOfMeasureForAreaByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return ' ';
		}
		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						util_get_translated( \'value\', property_preferences.value, property_preferences.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS value  
					FROM 
						property_preferences 
					WHERE 
						property_id = ' . ( int ) $intPropertyId . '
		                AND cid = ' . ( int ) $intCid . '
		                AND key = \'UNIT_OF_MEASURE_FOR_AREA\'';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) {
			return $arrstrData[0]['value'];
		}

		return __( 'Square Feet' );
	}

	public static function fetchUnitOfMeasureForAreaByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return ' ';
		}

		$strSql = 'SELECT util_set_locale(\'' . CLocale::DEFAULT_LOCALE . '\', \'' . CLocale::DEFAULT_LOCALE . '\' );
					SELECT
						util_get_translated( \'value\', property_preferences.value, property_preferences.details, \'' . CLocale::DEFAULT_LOCALE . '\' ) AS value  
					FROM 
						property_preferences 
					WHERE 
						property_id = ' . ( int ) $intPropertyId . '
		                AND cid = ' . ( int ) $intCid . '
		                AND key = \'UNIT_OF_MEASURE_FOR_AREA\'';

		$objPropertyPreference = self::fetchPropertyPreference( $strSql, $objDatabase );

		if( true === valObj( $objPropertyPreference, 'CPropertyPreference' ) && true === valStr( $objPropertyPreference->getValue() ) ) {
			return $objPropertyPreference->getValue();
		}

		return __( 'Square Feet' );
	}

	public static function fetchPropertyPreferenceDetailByKeyByPropertyIdByCid( $strKey, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						value 
					FROM 
						property_preferences 
					WHERE 
						property_id::int = ' . ( int ) $intPropertyId . '::int
						AND cid::int = ' . ( int ) $intCid . '::int
						AND key = \'' . trim( $strKey ) . '\'';

		$arrmixResultData = fetchData( $strSql, $objDatabase );
		return $arrmixResultData[0]['value'] ?? NULL;
	}

	public static function fetchRecentlyChangedPropertyPreferenceValuesByPropertyIdsByKeysByCid( $arrintPropertyIds, $arrstrKeys, $intCid, $objDatabase, $intLastSyncOn = NULL ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrstrKeys ) ) return NULL;

		$strWhereSql = '';
		if( true == valId( $intLastSyncOn ) ) {
			$strWhereSql = ' AND pp.updated_on > \'' . date( 'Y-m-d H:i:s', $intLastSyncOn ) . '\' ';
		}

		$arrmixPropertyPreferences = [];

		$strSql = 'SELECT
						pp.property_id,
						pp.key,
						pp.value
					FROM
						property_preferences pp
					WHERE
						pp.cid = ' . ( int ) $intCid . ' 
						AND pp.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
						AND pp.key IN (\'' . implode( "','", $arrstrKeys ) . '\') ' . $strWhereSql;

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrmixResponse ) {
				$arrmixPropertyPreferences[$arrmixResponse['property_id']][$arrmixResponse['key']] = $arrmixResponse['value'];
			}
		}

		return $arrmixPropertyPreferences;
	}

	public static function fetchPropertyPreferenceByPropertyIdsByCidsByKey( $arrintPropertyIds, $intCid, $strKey, $objDatabase, $intProductId = NULL, $boolIsValueRequired = false ) {

		$strJoin = '';

		if( valId( $intProductId ) ) {
			$strJoin = 'JOIN property_products pp ON (pp.cid = p.cid AND (pp.property_id = p.id OR pp.property_id IS NULL) AND pp.ps_product_id = ' . $intProductId . ' )';
		}

		$strSql = 'SELECT
						p.id,
						ppr.value
					FROM
						properties p
						' . $strJoin . '
						LEFT JOIN property_preferences ppr ON ( ppr.cid = p.cid AND ppr.property_id = p.id AND ppr.key = \'' . $strKey . '\' )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.cid = ' . ( int ) $intCid;

		if( true == $boolIsValueRequired ) {
			$strSql .= 'AND ( ppr.value = \'1\' )';
		} else {
			$strSql .= ' AND ( ppr.value <> \'1\' OR ppr.value IS NULL )';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnitOfMeasureForAreaForAllPropertiesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						property_id,
						util_get_translated( \'value\', property_preferences.value, property_preferences.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS value  
					FROM 
						property_preferences 
					WHERE
		                cid = ' . ( int ) $intCid . '
		                AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) 
		                AND key = \'UNIT_OF_MEASURE_FOR_AREA\'';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
