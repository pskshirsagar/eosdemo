<?php

class CInterCompanyTypes extends CBaseInterCompanyTypes {

	public static function fetchInterCompanyTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CInterCompanyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchInterCompanyType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CInterCompanyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>