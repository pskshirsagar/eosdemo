<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistItemTypes
 * Do not add any new functions to this class.
 */

class CChecklistItemTypes extends CBaseChecklistItemTypes {

	public static function fetchChecklistItemTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CChecklistItemType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchChecklistItemType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CChecklistItemType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchChecklistItemTypesByChecklistTypeId( $intChecklistTypeId, $objClientDatabase ) {
		if( false == is_numeric( $intChecklistTypeId ) || false == valId( $intChecklistTypeId ) ) return NULL;

		$strSql = 'SELECT util_set_locale(\'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getdefaultLocaleCode() . '\');
					SELECT
						cit.id,
						cit.checklist_trigger_ids,
						util_get_system_translated( \'name\', cit.name, cit.details ) AS name,
						cit.description,
						cit.is_published,
						cit.order_num
					FROM
						checklist_item_types cit
						JOIN checklist_triggers ct ON ( ct.id = ANY ( cit.checklist_trigger_ids ) AND ct.checklist_type_id = ' . ( int ) $intChecklistTypeId . ' )
					WHERE
						cit.is_published = TRUE
					GROUP BY
						cit.id
					ORDER BY
						cit.order_num';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>