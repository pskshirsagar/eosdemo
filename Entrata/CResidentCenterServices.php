<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CResidentCenterServices
 * Do not add any new functions to this class.
 */

class CResidentCenterServices extends CBaseResidentCenterServices {

	public static function fetchResidentCenterServicesByCidAndPropertyId( $intCid, $intPropertyId, $objDatabase ) {

		return self::fetchResidentCenterServices( sprintf( 'SELECT rcs.* FROM %s rcs, %s prcs WHERE prcs.resident_center_service_id::integer = rcs.id::integer AND prcs.cid::integer = %d AND prcs.property_id::integer = %d', 'resident_center_services', ( string ) 'property_resident_center_services', ( int ) $intCid, ( int ) $intPropertyId ), $objDatabase );
	}

}
?>