<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeProperties
 * Do not add any new functions to this class.
 */

class CApPayeeProperties extends CBaseApPayeeProperties {

    public static function fetchApPayeeProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return self::fetchCachedObjects( $strSql, 'CApPayeeProperty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false, $boolIsReturnKeyedArray );
    }

    public static function fetchApPayeeProperty( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CApPayeeProperty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchApPayeePropertiesByApPayeeIdByCid( $intApPayeeId, $intCid, $objDatabase ) {

		if( false == is_numeric( $intApPayeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
    					app.*
					FROM
					    ap_payee_properties app
					WHERE
					    app.cid = ' . ( int ) $intCid . '
					    AND app.ap_payee_id = ' . ( int ) $intApPayeeId;

		return self::fetchApPayeeProperties( $strSql, $objDatabase );

	}

}
?>