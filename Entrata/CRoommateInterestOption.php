<?php

class CRoommateInterestOption extends CBaseRoommateInterestOption {

	const CO_ED 			= 'CO';
	const SAME_GENDER_ONLY 	= 'SG';

	protected $m_boolIsSelected;

    /**
     * Val Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRoommateInterestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valAnswer() {
        $boolIsValid = true;
        if( false == valStr( $this->getAnswer() ) ) {

        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer', __( 'Option is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validateRoommateInterestOptions() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valAnswer();
            	break;

            case VALIDATE_DELETE:
            case 'validate_roommate_interest_options':
            	$boolIsValid &= $this->validateRoommateInterestOptions();
            	break;

            default:
            	// default;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false );

    	if( isset( $arrValues['is_selected'] ) && $boolDirectSet ) {
    		$this->m_intIsSelected = trim( $arrValues['is_selected'] );
    	} elseif( isset( $arrValues['is_selected'] ) ) {
    		$this->setIsSelected( $arrValues['is_selected'] );
    	}
    }

    public function setIsSelected( $boolIsSelected ) {
    	$this->m_boolIsSelected = $boolIsSelected;
    }

    /**
     * Get Functions
     */

    public function getIsSelected() {
    	return  $this->m_boolIsSelected;
    }

}
?>