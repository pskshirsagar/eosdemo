<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledTaskTypes
 * Do not add any new functions to this class.
 */

class CScheduledTaskTypes extends CBaseScheduledTaskTypes {

	public static function fetchScheduledTaskTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CScheduledTaskType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchScheduledTaskType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CScheduledTaskType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}

?>