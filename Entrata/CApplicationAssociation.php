<?php

class CApplicationAssociation extends CBaseApplicationAssociation {

	protected $m_intAddressTypeId;
	protected $m_intCustomerAddressId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerDataTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerDataTypeReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getCustomerAddressId() {
		return $this->m_intCustomerAddressId;
	}

	public function getAddressTypeId() {
		return $this->m_intAddressTypeId;
	}

	public function setCustomerAddressId( $intCustomerAddressId ) {
		$this->m_intCustomerAddressId = $intCustomerAddressId;
	}

	public function setAddressTypeId( $intAddressTypeId ) {
		$this->m_intAddressTypeId = $intAddressTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['customer_address_id'] ) ) $this->setCustomerAddressId( $arrmixValues['customer_address_id'] );
		if( true == isset( $arrmixValues['address_type_id'] ) ) 	$this->setAddressTypeId( $arrmixValues['address_type_id'] );
	}

	public function setApplicationAssociationData( $intCustomerAddressLogId, $intCustomerId ) {
		$this->setCustomerDataTypeReferenceId( $intCustomerAddressLogId );
		$this->setCustomerDataTypeId( CCustomerDataType::CUSTOMER_ADDRESS );
		$this->setCustomerId( $intCustomerId );
	}

}
?>