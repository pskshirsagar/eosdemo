<?php

class CEventResult extends CBaseEventResult {

	protected $m_intEventResultTypeId;
	protected $m_intEventTypeId;
	protected $m_intDefaultEventResultId;

	protected $m_strDefaultEventResult;
	protected $m_boolIsDefaultEventResult;

	protected $m_arrstrEventResultNames;
	protected $m_arrintEventResultTypeIds;

    /**
     * Set Functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['event_result_type_id'] ) )		$this->setEventResultTypeId( $arrmixValues['event_result_type_id'] );
    	if( true == isset( $arrmixValues['event_type_id'] ) )				$this->setEventTypeId( $arrmixValues['event_type_id'] );
    	if( true == isset( $arrmixValues['default_event_result_name'] ) )	$this->setDefaultEventResult( $arrmixValues['default_event_result_name'] );
    	if( true == isset( $arrmixValues['event_result_type_ids'] ) )		$this->setEventResultTypeIds( $arrmixValues['event_result_type_ids'] );
    	if( true == isset( $arrmixValues['is_default_event_result'] ) )		$this->setIsDefaultEventResult( $arrmixValues['is_default_event_result'] );
    	if( true == isset( $arrmixValues['default_event_result_id'] ) )		$this->setDefaultEventResultId( $arrmixValues['default_event_result_id'] );
    	if( true == isset( $arrmixValues['event_result_names'] ) )			$this->setEventResultNames( $arrmixValues['event_result_names'] );
    	return;
    }

    public function setEventResultTypeId( $intEventResultTypeId ) {
    	$this->m_intEventResultTypeId = $intEventResultTypeId;
    }

    public function setEventTypeId( $intEventTypeId ) {
    	$this->m_intEventTypeId = $intEventTypeId;
    }

    public function setDefaultEventResult( $strDefaultEventResult ) {
    	$this->strDefaultEventResult = $strDefaultEventResult;
    }

    public function setEventResultTypeIds( $arrintEventResultTypeIds ) {
    	$this->m_arrintEventResultTypeIds = $arrintEventResultTypeIds;
    }

    public function setIsDefaultEventResult( $boolIsDefaultEventResult ) {
    	$this->m_boolIsDefaultEventResult = $boolIsDefaultEventResult;
    }

    public function setDefaultEventResultId( $intDefaultEventResultId ) {
    	$this->m_intDefaultEventResultId = $intDefaultEventResultId;
    }

    public function setEventResultNames( $arrstrEventResultNames ) {
    	$this->m_arrstrEventResultNames = $arrstrEventResultNames;
    }

    /**
     * Get Functions
     */

    public function getEventResultTypeId() {
    	return $this->m_intEventResultTypeId;
    }

    public function getEventTypeId() {
    	return $this->m_intEventTypeId;
    }

	public function getDefaultEventResult() {
    	return $this->strDefaultEventResult;
    }

    public function getEventResultTypeIds() {
    	return $this->m_arrintEventResultTypeIds;
    }

    public function getIsDefaultEventResult() {
    	return $this->m_boolIsDefaultEventResult;
    }

    public function getDefaultEventResultId() {
    	return $this->m_intDefaultEventResultId;
    }

    public function getEventResultNames() {
    	return $this->m_arrstrEventResultNames;
    }

    /**
    * Fetch Functions
    *
    */

	public function fetchPropertyEventResults( $objDatabase ) {

		return \Psi\Eos\Entrata\CPropertyEventResults::createService()->fetchPropertyEventResultsByEventResultIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valEventResultName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'You must enter event result.' ) ) );
		} elseif( true == ( 3 > \Psi\CStringService::singleton()->strlen( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Event result must be at least 3 characters in length.' ) ) );
		} elseif( true == ( 150 < \Psi\CStringService::singleton()->strlen( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Length of event result should not exceed 150 characters.' ) ) );
		}

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {

			$strWhereConditions = ' WHERE
										cid = ' . ( int ) $this->getCid() . '
										AND deleted_on IS NULL
										AND lower(name) = \'' . addslashes( trim( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) ) . '\'
										' . ( ( 0 < $this->getId() ) ? ' AND id <> ' . ( int ) $this->getId() : '' );

			$intEventResultsCount = \Psi\Eos\Entrata\CEventResults::createService()->fetchEventResultCount( $strWhereConditions, $objDatabase );

			if( 0 < $intEventResultsCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Event result {%s, 0} already exists.', [ $this->getName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valDeleteEventResult( $objDatabase ) {

		$boolIsValid                = true;
		$arrobjPropertyEventResults = $this->fetchPropertyEventResults( $objDatabase );

		if( true == valArr( $arrobjPropertyEventResults ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '{%s,0} cannot be deleted, it is being associated with property. You need to disassociate from from property to delete.', [ $this->getName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName();
				break;

			case 'non_integrated_company_insert':
			case 'non_integrated_company_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valEventResultName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeleteEventResult( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validateEventResult() {
		$boolIsValid = true;
		$boolIsValid &= $this->valCid();
		if( false == valStr( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Result Name should not be empty.' ), NULL ) );

			return $boolIsValid &= false;
		} elseif( 100 < \Psi\CStringService::singleton()->strlen( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Result Name should be upto 100 characters.' ), NULL ) );

			return $boolIsValid &= false;
		}

		if( true == valArr( $this->getEventResultTypeIds() ) ) {
			if( \Psi\Libraries\UtilFunctions\count( array_filter( $this->getEventResultTypeIds() ) ) !== \Psi\Libraries\UtilFunctions\count( array_unique( array_filter( $this->getEventResultTypeIds() ) ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Duplicate event type.' ), NULL ) );
				$boolIsValid = false;

				return $boolIsValid;
			}

			foreach( $this->getEventResultTypeIds() as $intEventResultTypeId ) {
				if( false == is_numeric( $intEventResultTypeId ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select Event Type.' ), NULL ) );
					$boolIsValid &= false;

					return $boolIsValid;
				}
			}
		}

		if( true == valArr( $this->getDefaultEventResultIds() ) ) {
			foreach( $this->getDefaultEventResultIds() as $intDefaultEventResultIds ) {
				if( false == is_numeric( $intDefaultEventResultIds ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select Most Similar To.' ), NULL ) );

					return $boolIsValid &= false;
				}
			}
		}

		if( true == in_array( $this->getName(), $this->getEventResultNames() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'This Event Result already exists, please Add/Update associations in existing Event Result.' ), NULL ) );

			return $boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validateSortCustomEventResult() {
		$boolIsValid = true;
		$boolIsValid &= $this->valCid();

		if( true == valArr( $this->getEventResultTypeIds() ) ) {
			if( \Psi\Libraries\UtilFunctions\count( array_filter( $this->getEventResultTypeIds() ) ) !== \Psi\Libraries\UtilFunctions\count( array_unique( array_filter( $this->getEventResultTypeIds() ) ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Duplicate event type.' ), NULL ) );
				$boolIsValid = false;

				return $boolIsValid;
			}

			foreach( $this->getEventResultTypeIds() as $intEventResultTypeId ) {
				if( false == is_numeric( $intEventResultTypeId ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select Event Type.' ), NULL ) );
					$boolIsValid &= false;

					return $boolIsValid;
				}
			}
		}

		if( true == valArr( $this->getDefaultEventResultIds() ) ) {
			foreach( $this->getDefaultEventResultIds() as $intKey => $intDefaultEventResultIds ) {
				if( false == is_numeric( $intDefaultEventResultIds ) && true == is_numeric( $this->getEventResultTypeIds()[$intKey] ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Please select Most Similar To.' ), NULL ) );

					return $boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intUserId, $objDatabase );
		}

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'NOW()' );

		return $this->update( $intUserId, $objDatabase );
	}

}

?>