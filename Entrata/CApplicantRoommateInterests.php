<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantRoommateInterests
 * Do not add any new functions to this class.
 */

class CApplicantRoommateInterests extends CBaseApplicantRoommateInterests {

	public static function fetchApplicantRoommateInterestByRoommateInterestIdByCid( $intRoommateInterestId, $intCid, $objDatabase ) {
		if( false == valId( $intRoommateInterestId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						*
				   FROM
						applicant_roommate_interests
				   WHERE
						cid = ' . ( int ) $intCid . '
						AND roommate_interest_id = ' . ( int ) $intRoommateInterestId . '
						AND roommate_interest_option_id IS NOT NULL ';
		return self::fetchApplicantRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchOrderedApplicantRoommateInterestsByApplicantIdByLeaseIdByCid( $intApplicantId, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicantId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
				   FROM
						applicant_roommate_interests
				   WHERE
						cid = ' . ( int ) $intCid . '
						AND applicant_id = ' . ( int ) $intApplicantId . '
						AND lease_id = ' . ( int ) $intLeaseId . '
				   ORDER BY id ASC';
		return self::fetchApplicantRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchApplicantRoommateInterestsByApplicantIdByLeaseIdByCid( $intApplicantId, $intLeaseId, $intCid, $objDatabase, $arrintRoommateInterestIds = [] ) {
		if( false == valId( $intApplicantId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strWhereClause  = ' ';
		if( true == valArr( $arrintRoommateInterestIds ) ) {
			$strRoommateInterestIds             = implode( ',', $arrintRoommateInterestIds );
			$strWhereClause = ' AND ari.roommate_interest_id IN ( ' . $strRoommateInterestIds . ' ) ';
		}
		$strSql = 'SELECT
        				ari.*
					FROM
        				applicant_roommate_interests ari
        				JOIN roommate_interests ri ON ( ri.cid = ari.cid AND ri.id = ari.roommate_interest_id )
    				WHERE
						ari.cid = ' . ( int ) $intCid . '
        				AND ari.applicant_id = ' . ( int ) $intApplicantId . '
						AND ari.lease_id = ' . ( int ) $intLeaseId . $strWhereClause;

		return self::fetchApplicantRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchApplicantRoommateInterestsByApplicantIdsByLeaseIdsByCid( $arrintApplicantIds, $arrintLeaseIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) || false == valArr( $arrintLeaseIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
        				ari.*
					FROM
        				applicant_roommate_interests ari
        				JOIN roommate_interests ri ON ( ri.cid = ari.cid AND ri.id = ari.roommate_interest_id )
    				WHERE
						ari.cid = ' . ( int ) $intCid . '
        				AND ari.applicant_id IN ( ' . implode( ',', $arrintApplicantIds ) . ' )
						AND ari.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' ) ';

		return self::fetchApplicantRoommateInterests( $strSql, $objDatabase );
	}

	public static function fetchApplicantRoommateInterestsBySelectParametersByApplicantIdByLeaseIdByCid( $strSelectClause, $intApplicantId, $intLeaseId, $intCid, $objDatabase ) {
		if( false == valId( $intApplicantId ) || false == valId( $intLeaseId ) || false == valId( $intCid ) || false == valStr( $strSelectClause ) ) {
			return NULL;
		}
		$strSql = 'SELECT ' .
						$strSelectClause . '
					FROM
        				applicant_roommate_interests ari
        			WHERE
						ari.cid = ' . ( int ) $intCid . '
						AND ari.roommate_interest_option_id IS NOT NULL
        				AND ari.applicant_id = ' . ( int ) $intApplicantId . '
						AND ari.lease_id = ' . ( int ) $intLeaseId;

		return fetchData( $strSql, $objDatabase );
	}

}
?>