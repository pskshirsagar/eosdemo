<?php

class CScreeningApplicantResult extends CBaseScreeningApplicantResult {

    protected $m_intApplicantId;
    protected $m_strApplicantFirstName;
    protected $m_strApplicantLastName;
    protected $m_intCustomerTypeId;

    public static function getScreeningRecommendationTypeName( $intScreeningRecommendationTypeId ) {

        $strScreeningRecommendationName = '';

        switch( $intScreeningRecommendationTypeId ) {

            case CScreeningRecommendationType::PENDING_UNKNOWN:
                $strScreeningRecommendationName = __( 'In Progress' );
                break;

            case CScreeningRecommendationType::FAIL:
                $strScreeningRecommendationName = __( 'Fail' );
                break;

            case CScreeningRecommendationType::PASS:
                $strScreeningRecommendationName = __( 'Pass' );
                break;

            case CScreeningRecommendationType::PASSWITHCONDITIONS:
                $strScreeningRecommendationName = __( 'Pass With Conditions' );
                break;

            case CScreeningRecommendationType::ERROR:
                $strScreeningRecommendationName = __( 'Error - contact support' );
                break;

            default:
                $strScreeningRecommendationName = __( 'Not Screened Yet' );
                break;
        }

        return $strScreeningRecommendationName;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningApplicationRequestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScreeningRecommendationTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function getApplicantId() {
        return $this->m_intApplicantId;
    }

    public function getApplicantFirstName() {
        return $this->m_strApplicantFirstName;
    }

    public function getApplicantLastName() {
        return $this->m_strApplicantLastName;
    }

    public function getCustomerTypeId() {
        return $this->m_intCustomerTypeId;
    }

    public function setApplicantId( $intApplicantId ) {
        $this->m_intApplicantId = $intApplicantId;
    }

    public function setApplicantFirstName( $strApplicantFirstName ) {
        $this->m_strApplicantFirstName = $strApplicantFirstName;
    }

    public function setApplicantLastName( $strApplicantLastName ) {
        $this->m_strApplicantLastName = $strApplicantLastName;
    }

    public function setCustomerTypeId( $intCustomerTypeId ) {
        $this->m_intCustomerTypeId = $intCustomerTypeId;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['applicant_id'] ) ) $this->setApplicantId( $arrmixValues['applicant_id'] );
        if( true == isset( $arrmixValues['first_name'] ) )  $this->setApplicantFirstName( $arrmixValues['first_name'] );
        if( true == isset( $arrmixValues['last_name'] ) ) $this->setApplicantLastName( $arrmixValues['last_name'] );
        if( true == isset( $arrmixValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrmixValues['customer_type_id'] );

        return;
    }

    public function createScreeningApplicantResultLog() {
        $objScreeningApplicantResultLog = new CScreeningApplicantResultLog();
        $objScreeningApplicantResultLog->setCid( $this->getCid() );
        $objScreeningApplicantResultLog->setScreeningApplicantResultId( $this->getId() );

        return $objScreeningApplicantResultLog;
    }

}
?>