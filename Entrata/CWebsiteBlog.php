<?php

class CWebsiteBlog extends CBaseWebsiteBlog {

	protected $m_objWebsiteDomain;

	public function setPassword( $strPassword ) {
		$this->setPasswordEncrypted( CEncryption::encryptText( $strPassword, CONFIG_KEY_LOGIN_PASSWORD ) );
	}

	public function getWebsiteDomain() {
		return $this->m_objWebsiteDomain;
	}

	public function getPassword() {
		return CEncryption::decryptText( $this->m_strPasswordEncrypted, CONFIG_KEY_LOGIN_PASSWORD );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function flagBlogMasterAsInitialized( $resBlogMasterDatabase ) {

		$strSql = 'UPDATE initializations SET initialized_on = \'' . date( 'Y-m-d' ) . '\' WHERE init_handle = \'' . $this->m_strInitHandle . '\';';

		if( false == mysqli_query( $resBlogMasterDatabase, $strSql ) ) {
			return false;
		}

		return true;
	}

	public function insertInitializationRecordIntoBlogMasterDatatabase( $objWebsite, $resBlogMasterDatabase ) {
		set_time_limit( 95 );

		// We need to validate the
		$strUrl	= $objWebsite->getSubDomain() . CConfig::get( 'prospectportal_domain_postfix' );

		$strSql	= 'INSERT INTO `initializations` (`company_website_id`, `init_handle`, `domain`, `username`, `password`, `pass_phrase`, `prepared_on`, `initialized_on`) VALUES
					(' . $this->getWebsiteId() . ',\'' . $this->getInitHandle() . '\',\'' . $strUrl . '\',\'' . $this->getUsername() . '\',\'' . $this->getPassword() . '\',\'' . $this->getPassPhrase() . '\',NULL,NULL);';

		if( false == mysqli_query( $resBlogMasterDatabase, $strSql ) ) {
			return false;
		}

		// Now we are going to wait 90 seconds to see if the script prepares the blog on the external server.
		$boolBlogPrepared		= false;
		$intTimeDelayTolerance	= 90;
		$intStartTime			= time();

		while( false == $boolBlogPrepared && time() <= ( $intStartTime + $intTimeDelayTolerance ) ) {
			$strSql	= "SELECT * FROM initializations WHERE init_handle = '" . $this->getInitHandle() . "'";

			if( false == ( $resResult = mysqli_query( $resBlogMasterDatabase, $strSql ) ) ) {
				return false;
			}

			while( NULL !== ( $arrstrWordPressDatabase = mysqli_fetch_array( $resResult, MYSQLI_ASSOC ) ) ) {
				if( false == is_null( trim( $arrstrWordPressDatabase['prepared_on'] ) ) && true == CValidation::validateDate( $arrstrWordPressDatabase['prepared_on'] ) ) {
					return true;
				}
			}

			sleep( 5 );
		}

		return false;
	}

}
?>