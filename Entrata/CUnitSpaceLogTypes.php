<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceLogTypes
 * Do not add any new functions to this class.
 */

class CUnitSpaceLogTypes extends CBaseUnitSpaceLogTypes {

    public static function fetchUnitSpaceLogTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CUnitSpaceLogType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchUnitSpaceLogType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CUnitSpaceLogType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>