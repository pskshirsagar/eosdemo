<?php
use Psi\Eos\Entrata\CUnitSpaces;

class CUnitPackage extends CBaseUnitPackage {

	protected $m_intUnitNumber;
	protected $m_intSpaceNumber;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strCustomerName;
	protected $m_strCustomerEmail;
	protected $m_strPackageType;
	protected $m_strBuildingName;
	protected $m_boolIsSendSms;
	protected $m_boolIsSendEmail;

	const VALIDATE_NOTIFY = 'notify';

    public function __construct() {
        parent::__construct();

		$this->m_boolIsSendSms			= NULL;
		$this->m_boolIsSendEmail		= NULL;
		$this->m_boolIsDisplayOnPortal	= NULL;

        return;
    }

    public function setDefaults() {

		$this->setPropertyUnitId( NULL );
		$this->setAnnouncementId( NULL );
		$this->setUnitSpaceId( NULL );
		$this->setCustomerId( NULL );
		$this->setPackageTypeId( NULL );
		$this->setMessageId( NULL );
		$this->setSystemEmailId( NULL );
		$this->setIsSendSms( NULL );
		$this->setIsSendEmail( NULL );
		$this->setIsDisplayOnPortal( NULL );

        return;
    }

    /**
     * Setter Functions
     */

    public function setUnitNumber( $intUnitNumber ) {
		$this->m_intUnitNumber = $intUnitNumber;
    }

    public function setSpaceNumber( $intSpaceNumber ) {
		$this->m_intSpaceNumber	= $intSpaceNumber;
    }

    public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
    }

    public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
    }

    public function setCustomerEmail( $strCustomerEmail ) {
		$this->m_strCustomerEmail = $strCustomerEmail;
    }

    public function setBuildingName( $strBuildingName ) {
    	$this->m_strBuildingName = $strBuildingName;
    }

    public function setPackageType( $strPackageType ) {
		$this->m_strPackageType = $strPackageType;
    }

    public function setIsSendSms( $boolIsSendSms ) {
    	$this->m_boolIsSendSms = ( bool ) $boolIsSendSms;
    }

    public function setIsSendEmail( $boolIsSendEmail ) {
    	$this->m_boolIsSendEmail = ( bool ) $boolIsSendEmail;
    }

    public function setIsDisplayOnPortal( $boolIsDisplayOnPortal ) {
    	$this->m_boolIsDisplayOnPortal = ( bool ) $boolIsDisplayOnPortal;
    }

    public function setCustomerName( $strCustomerName ) {
    	$this->m_strCustomerName = $strCustomerName;
    }

    /**
     * Getter Functions
     */

    public function getUnitNumber() {
		return $this->m_intUnitNumber;
    }

    public function getSpaceNumber() {
		return $this->m_intSpaceNumber;
    }

    public function getNameFirst() {
		return $this->m_strNameFirst;
    }

    public function getNameLast() {
		return $this->m_strNameLast;
    }

    public function getCustomerEmail() {
		return $this->m_strCustomerEmail;
    }

    public function getPackageType() {
		return $this->m_strPackageType;
    }

    public function getBuildingName() {
    	return $this->m_strBuildingName;
    }

    public function getIsSendEmail() {
    	return $this->m_boolIsSendEmail;
    }

    public function getIsSendSms() {
    	return $this->m_boolIsSendSms;
    }

    public function getIsDisplayOnPortal() {
    	return $this->m_boolIsDisplayOnPortal;
    }

    public function getCustomerName() {
    	return $this->m_strCustomerName;
    }

    /**
     * Override Methods
     */

    public function setIncrementNotificationCountByOne() {
    	$intSentEmailCount = $this->getSentEmailCount();

    	if( false == is_null( $intSentEmailCount ) && 0 < $intSentEmailCount ) {
    		$intSentEmailCount = $intSentEmailCount + 1;
    	} else {
    		$intSentEmailCount = 1;
    	}

		$this->setSentEmailCount( $intSentEmailCount );
    	return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes = true );

        if( true == isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( $arrValues['unit_number'] );
        if( true == isset( $arrValues['space_number'] ) ) $this->setSpaceNumber( $arrValues['space_number'] );
        if( true == isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
        if( true == isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( true == isset( $arrValues['name_first'] ) || true == isset( $arrValues['name_last'] ) ) $this->setCustomerName( $this->getNameFirst() . ' ' . $this->getNameLast() );
        if( true == isset( $arrValues['email_address'] ) ) $this->setCustomerEmail( $arrValues['email_address'] );
        if( true == isset( $arrValues['package_type'] ) ) $this->setPackageType( $arrValues['package_type'] );
        if( true == isset( $arrValues['building_name'] ) ) $this->setBuildingName( $arrValues['building_name'] );
        if( true == isset( $arrValues['is_send_sms'] ) ) $this->setIsSendSms( $arrValues['is_send_sms'] );
		if( true == isset( $arrValues['is_send_email'] ) ) $this->setIsSendEmail( $arrValues['is_send_email'] );
		if( true == isset( $arrValues['is_display_on_portal'] ) ) $this->setIsDisplayOnPortal( $arrValues['is_display_on_portal'] );

		return;
    }

    /**
     * Validation Functions
     */

    public function valMessageId() {
    	$boolIsValid = true;

    	if( true == $this->getIsSendSms() && true == is_null( $this->getMessageId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_id', 'SMS Sending has some issues.' ) );
    	}
    	return $boolIsValid;
    }

    public function valPropertyUnitId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyUnitId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', 'Property unit is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPackageTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPackageTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'package_type_id', 'Package type is required.' ) );
        }
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;

        if( true == is_null( $this->getCustomerId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', 'Customer is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPackageDatetime() {
        $boolIsValid = true;

        if( true == is_null( $this->getPackageDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'package_datetime', 'Package date is required.' ) );
        } elseif( strtotime( date( 'd-m-Y' ) ) < strtotime( $this->getPackageDatetime() ) ) {
			$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'package_datetime', 'Package date should not be greater than the current date.' ) );
        }

        return $boolIsValid;
    }

    public function valRetrievedOn() {
        $boolIsValid = true;

        if( false == is_null( $this->getRetrievedOn() ) ) {
        	if( strtotime( date( 'd-m-Y' ) ) < strtotime( $this->getRetrievedOn() ) ) {
       			$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retrieved_on', 'Retrieved date should not be greater than the current date.' ) );
        	} elseif( strtotime( $this->getRetrievedOn() ) < strtotime( $this->getPackageDatetime() ) ) {
        		$boolIsValid = false;
            	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'retrieved_on', 'Retrieved date should not be less than the package date.' ) );
        	}
        }

        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase, $boolIsNotifyOnly = false ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valPackageTypeId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPackageDatetime();
				$boolIsValid &= $this->valRetrievedOn();
				break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $boolIsValid;
				break;

            default:
            	$boolIsValid = true;
            	break;
		}

        return $boolIsValid;
    }

	/**
	 * SQL Functions
	 */

    public function fetchPropertyUnit( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyUnits::createService()->fetchPropertyUnitByIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objDatabase );
    }

    public function fetchUnitSpaces( $objDatabase ) {

		return CUnitSpaces::createService()->fetchCustomUnitSpacesByPropertyUnitIdByCid( $this->getPropertyUnitId(), $this->getCid(), $objDatabase );
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	$this->setDeletedBy( $intCurrentUserId );
    	$this->setDeletedOn( 'NOW()' );

    	return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    }

    /**
     * Other Functions
     */

    public function buildDefaultSignatureFilePath() {
    	return getMountsPath( $this->m_intCid, PATH_MOUNTS_DOCUMENTS ) . 'parcelalert/' . date( 'Y' ) . '/' . date( 'n' ) . '/' . date( 'j' ) . '/';
    }

}
?>