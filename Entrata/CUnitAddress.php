<?php

class CUnitAddress extends CBaseUnitAddress {

	/**
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Unit address id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyUnitId ) || ( 1 > $this->m_intPropertyUnitId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_unit_id', __( 'Property unit id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intAddressTypeId ) || ( 1 > $this->m_intAddressTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'address_type_id', __( 'Address type does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTimeZoneId() {
		$boolIsValid = true;

		if( $this->m_intTimeZoneId && ( 1 > $this->m_intTimeZoneId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Time zone does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMarketingName() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strMarketingName )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'marketing_name', '' ));
		// }

		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strStreetLine1 )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', '' ));
		// }

		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strStreetLine2 )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line2', '' ));
		// }

		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strStreetLine3 )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line3', '' ));
		// }

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strCity )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', '' ));
		// }

		return $boolIsValid;
	}

	public function valCounty() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strCounty )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'county', '' ));
		// }

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strStateCode )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', '' ));
		// }

		return $boolIsValid;
	}

	public function valProvince() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strProvince )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'province', '' ));
		// }

		return $boolIsValid;
	}

	public function valPostalCode( $strFieldName = 'postal code' ) {
		$boolIsValid = true;

		$strFieldName = ( false == empty( $strFieldName ) ) ? $strFieldName : __( 'postal code' );

		if( false == isset( $this->m_strPostalCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( '{%s, 0} is required.', [ \Psi\CStringService::singleton()->ucfirst( $strFieldName ) ] ) ) );
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_strCountryCode )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', '' ));
		// }

		return $boolIsValid;
	}

	public function valLongitude() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_fltLongitude )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'longitude', '' ));
		// }

		return $boolIsValid;
	}

	public function valLatitude() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_fltLatitude )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'latitude', '' ));
		// }

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_intIsPublished )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ));
		// }

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		// Validation example
		// if( false == isset( $this->m_intOrderNum )) {
		//    $boolIsValid = false;
		//    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ));
		// }

		return $boolIsValid;
	}

	public function valEmptyUnitAddress() {
		$boolIsValid = true;

		if( false == isset( $this->m_strStreetLine1 ) && false == isset( $this->m_strStreetLine2 )
			&& false == isset( $this->m_strStreetLine3 ) && false == isset( $this->m_strCity )
			&& false == isset( $this->m_strCounty ) && false == isset( $this->m_strStateCode )
			&& false == isset( $this->m_strProvince ) && false == isset( $this->m_strPostalCode )
			&& false == isset( $this->m_strCountryCode ) && false == isset( $this->m_fltLongitude )
			&& false == isset( $this->m_fltLatitude ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCustomerAddressCity() {
		$boolIsValid = true;

		if( ( true == is_null( $this->getCity() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City is required.' ) ) );
		}
		return $boolIsValid;
	}

	/**
	 * @return bool
	 */
	public function validatePortalUnitAddress() {
		$boolIsValid = true;

		$boolIsValid &= $this->valId();
		$boolIsValid &= $this->valCid();
		$boolIsValid &= $this->valPropertyUnitId();
		$boolIsValid &= $this->valAddressTypeId();
		$boolIsValid &= $this->valPostalCode();

		if( empty( $this->getStreetLine1() ) || !valStr( $this->getStreetLine1() ) ) {
			$boolIsValid &= false;
		}

		if( empty( $this->getCity() ) || !valStr( $this->getCity() ) ) {
			$boolIsValid &= false;
		}

		if( empty( $this->getCountryCode() ) || !valStr( $this->getCountryCode() ) ) {
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPropertyUnitId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valTimeZoneId();
				$boolIsValid &= $this->valPostalCode( 'postal code' );
				break;

			case 'rpc_service_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valTimeZoneId();
				break;

			case 'validate_existing_address':
				$boolIsValid &= $this->validatePortalUnitAddress();

				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'validate_empty_unit_address':
				$boolIsValid &= $this->valEmptyUnitAddress();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>