<?php

class CArchivedRateLog extends CBaseArchivedRateLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyFloorplanId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCascadeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCascadeReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArPhaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArTriggerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArFormulaId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArFormulaReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpaceConfigurationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerRelationshipId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOptimalRateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectivePeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportingPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermMonths() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowStartDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowEndDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWindowEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateIncreaseIncrement() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthToMonthMultiplier() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNormalizedAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNormalizedPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateIntervalStart() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateIntervalOccurances() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRateIntervalOffset() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveThroughDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeactivationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowOnWebsite() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowInEntrata() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsRenewal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOptional() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLocked() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostMonthIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsEffectiveDatetimeIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAllowed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeObjectId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>