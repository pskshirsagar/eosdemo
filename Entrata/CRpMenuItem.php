<?php

class CRpMenuItem extends CBaseRpMenuItem {

	const RP_COMMUNITY_MENU_ITEM_ID = 1;
	const RP_AMENITIES_MENU_ITEM_ID = 2;
	const RP_ANNOUNCEMENTS_MENU_ITEM_ID = 3;
	const RP_CLASSIFIEDS_MENU_ITEM_ID = 4;
	const RP_COMMUNITY_FEED_MENU_ITEM_ID = 5;
	const RP_EVENTS_MENU_ITEM_ID = 6;
	const RP_OFFICE_INFORMATION_MENU_ITEM_ID = 7;
	const RP_PROPERTY_REVIEWS_MENU_ITEM_ID = 8;
	const RP_PROPERTY_SERVICES_MENU_ITEM_ID = 9;
	const RP_REFERRALS_MENU_ITEM_ID = 10;
	const RP_CONTACT_PROPERTY_MENU_ITEM_ID = 11;
	const RP_DEVICES_MENU_ITEM_ID = 12;
	const RP_MESSAGES_AND_ALERTS_MENU_ITEM_ID = 13;
	const RP_MY_APARTMENT_MENU_ITEM_ID = 14;
	const RP_DOCUMENTS_MENU_ITEM_ID = 15;
	const RP_EMERGENCY_CONTACTS_MENU_ITEM_ID = 16;
	const RP_GUEST_LIST_MENU_ITEM_ID = 17;
	const RP_INSPECTIONS_MENU_ITEM_ID = 18;
	const RP_INSURANCE_MENU_ITEM_ID = 19;
	const RP_MAINTENANCE_MENU_ITEM_ID = 20;
	const RP_RENTABLE_ITEMS_MENU_ITEM_ID = 21;
	const RP_REQUEST_TRANSFER_MENU_ITEM_ID = 22;
	const RP_UTILITIES_MENU_ITEM_ID = 23;
	const RP_VEHICLES_MENU_ITEM_ID = 24;
	const RP_PAYMENTS_MENU_ITEM_ID = 25;
	const RP_QUICK_LINKS_DASHBOARD_MENU_ITEM_ID = 26;
	const RP_ANNOUNCEMENTS_MENU_ITEM_TYPE_TWO_ID = 27;
	const RP_EVENTS_MENU_ITEM_TYPE_TWO_ID = 28;
	const RP_CLASSIFIEDS_MENU_ITEM_TYPE_TWO_ID = 29;
	const RP_REQUEST_MAINTENANCE_MENU_ITEM_TYPE_THREE_ID = 30;
	const RP_UPDATE_INSURANCE_MENU_ITEM_TYPE_THREE_ID = 31;
	const RP_CONTACT_US_MENU_ITEM_TYPE_THREE_ID = 32;
	const RP_VIEW_DOCUMENTS_MENU_ITEM_TYPE_THREE_ID = 33;
	const RP_RESERVE_AMENITY_MENU_ITEM_TYPE_THREE_ID = 34;
	const RP_RENT_ITEMS_MENU_ITEM_TYPE_THREE_ID = 35;
	const RP_REFER_A_FRIEND_MENU_ITEM_TYPE_THREE_ID = 36;
	const RP_SUBMIT_REVIEW_MENU_ITEM_TYPE_THREE_ID = 37;
	const RP_RESIDENT_SERVICES_MENU_ITEM_TYPE_THREE_ID = 38;

	const RP_PAYMENTS_MENU_ITEM = 'Payments';
	const RP_SETTINGS_MENU = 'Settings';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertySettingKeyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMenuItemTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHoverText() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>