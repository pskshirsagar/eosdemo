<?php

class CGlAccountProperty extends CBaseGlAccountProperty {

	protected $m_strGlAccountNumber;
	protected $m_strGlAccountName;

	protected $m_intPropertyId;

	/**
	 * Get Functions
	 */

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getGLAccountNumber() {
		return $this->m_strGlAccountNumber;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	/**
	 * Set Functions
	 */

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = $strGlAccountName;
	}

	public function setGlAccountNumber( $strGlAccountNumber ) {
		$this->m_strGlAccountNumber = $strGlAccountNumber;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['gl_account_name'] ) ) $this->setGlAccountName( $arrValues['gl_account_name'] );
		if( true == isset( $arrValues['gl_account_number'] ) ) $this->setGlAccountNumber( $arrValues['gl_account_number'] );
		if( true == isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		return;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {

		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Select at least one property.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>