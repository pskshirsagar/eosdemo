<?php

class CGlGroup extends CBaseGlGroup {

	const GL_ACCOUNT_GROUP_CASH_SYSTEM_CODE					= 'Cash';
	const GL_ACCOUNT_GROUP_SECURITY_DEPOSIT_SYSTEM_CODE		= 'SecDep';

	const INCOME_STATEMENT_SYSTEM_CODE			= 'INCST';
	const BALANCE_SHEET_SYSTEM_CODE				= 'BALSH';
	const NON_OPERATING_EXPENSES_SYSTEM_CODE	= 'Non-opExp';

	// Newly created constants
	// First level GL Groups system code
	const EQUITY_SYSTEM_CODE			= 'EQUITY';
	const ASSETS_SYSTEM_CODE			= 'ASSETS';
	const INCOME_SYSTEM_CODE			= 'INCOME';
	const EXPENSES_SYSTEM_CODE			= 'EXPENSES';
	const LIABILITIES_SYSTEM_CODE		= 'LIABILITIES';

	// Second level GL Groups system code
	const CURASS_CURASS_SYSTEM_CODE	= 'CURASS';
	const CL_SYSTEM_CODE				= 'CL';
	const CAP_SYSTEM_CODE				= 'CAP';
	const REV_SYSTEM_CODE				= 'REV';
	const EXP_SYSTEM_CODE				= 'EXP';

	// Third level GL Groups system code
	const CASH_SYSTEM_CODE			= 'Cash';
	const OWNER_EQUITY_SYSTEM_CODE	= 'Equity';
	const REVENUE_SYSTEM_CODE		= 'Revenue';
	const AP_SYSTEM_CODE			= 'AP';
	const OPEXPENSE_SYSTEM_CODE		= 'OpExpense';

	protected $m_boolIsSameGroup;

	protected $m_intGlAccountTreeId;
	protected $m_intGlAccountId;
	protected $m_intGroupDepth;

	protected $m_strGlAccountName;
	protected $m_strGlAccountNumber;
	protected $m_strSiblingSummation;
	protected $m_strSystemName;

	protected $m_arrobjGlGroups;
	protected $m_arrobjGlAccountTrees;
	protected $m_arrobjSiblingGlGroups;

	/**
	 * Get Functions
	 */

	public function getGlAccountTreeId() {
		return $this->m_intGlAccountTreeId;
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function getGroupDepth() {
		return $this->m_intGroupDepth;
	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function getGlAccountNumber() {
		return $this->m_strGlAccountNumber;
	}

	public function getGlAccountTrees() {
		return $this->m_arrobjGlAccountTrees;
	}

	public function getGlGroups() {
		return $this->m_arrobjGlGroups;
	}

	public function getSiblingGlGroups() {
		return $this->m_arrobjSiblingGlGroups;
	}

	public function getSiblingSummation() {
		return $this->m_strSiblingSummation;
	}

	public function getSystemName() {
		return $this->m_strSystemName;
	}

	public function getIsSameGroup() {
		return $this->m_boolIsSameGroup;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['gl_account_tree_id'] ) ) $this->setGlAccountTreeId( $arrmixValues['gl_account_tree_id'] );
		if( true == isset( $arrmixValues['gl_account_id'] ) ) $this->setGlAccountId( $arrmixValues['gl_account_id'] );
		if( true == isset( $arrmixValues['gl_account_name'] ) ) $this->setGlAccountName( $arrmixValues['gl_account_name'] );
		if( true == isset( $arrmixValues['gl_account_number'] ) ) $this->setGlAccountNumber( $arrmixValues['gl_account_number'] );
		if( true == isset( $arrmixValues['sibling_summation'] ) ) $this->setSiblingSummation( $arrmixValues['sibling_summation'] );
		if( true == isset( $arrmixValues['system_name'] ) ) $this->setSystemName( $arrmixValues['system_name'] );

		return;
	}

	public function setGlAccountTreeId( $intGlAccountTreeId ) {
		$this->m_intGlAccountTreeId = $intGlAccountTreeId;
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->m_intGlAccountId = $intGlAccountId;
	}

	public function setGroupDepth( $intGroupDepth ) {
		$this->m_intGroupDepth = $intGroupDepth;
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = $strGlAccountName;
	}

	public function setGlAccountNumber( $strGlAccountNumber ) {
		$this->m_strGlAccountNumber = $strGlAccountNumber;
	}

	public function setGlAccountTrees( $arrobjGlAccountTrees ) {
		$this->m_arrobjGlAccountTrees = $arrobjGlAccountTrees;
	}

	public function setGlGroups( $arrobjGlGroups ) {
		$this->m_arrobjGlGroups = $arrobjGlGroups;
	}

	public function setSiblingGlGroups( $arrobjSiblingGlGroups ) {
		$this->m_arrobjSiblingGlGroups = $arrobjSiblingGlGroups;
	}

	public function setSiblingSummation( $strSiblingSummation ) {
		return $this->m_strSiblingSummation = $strSiblingSummation;
	}

	public function setSystemName( $strSystemName ) {
		$this->m_strSystemName = CStrings::strTrimDef( $strSystemName, 50, NULL, true );
	}

	public function setIsSameGroup( $boolIsSameGroup ) {
		$this->m_boolIsSameGroup = $boolIsSameGroup;
	}

	/**
	 * Create Functions
	 */

	public function createGlAccount() {

		$objGlAccount = new CGlAccount();
		$objGlAccount->setCid( $this->getCid() );
		$objGlAccount->setGlAccountTypeId( $this->getGlAccountTypeId() );

		return $objGlAccount;
	}

	public function createGlAccountTree() {

		$objGlAccountTree = new CGlAccountTree();
		$objGlAccountTree->setCid( $this->getCid() );
		$objGlAccountTree->setGlTreeId( $this->getGlTreeId() );

		return $objGlAccountTree;
	}

	public function createGlAccountMaskGroup() {

		$objGlAccountMaskGroup = new CGlAccountMaskGroup();
		$objGlAccountMaskGroup->setCid( $this->getCid() );
		$objGlAccountMaskGroup->setGlGroupId( $this->getId() );

		return $objGlAccountMaskGroup;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchParentGlGroup( $objClientDatabase ) {
		return CGlGroups::fetchGlGroupByIdByGlTreeIdByCid( $this->getParentGlGroupId(), $this->getGlTreeId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchGlGroupsByParentGlGroupId( $objClientDatabase ) {
		return CGlGroups::fetchGlGroupsByGlTreeIdByParentGlGroupIdByCid( $this->getGlTreeId(), $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchGlGroupsByOriginGlGroupId( $objClientDatabase ) {
		return CGlGroups::fetchCustomGlGroupsByOriginGlGroupIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchGlGroupsBySystemCode( $objClientDatabase ) {
		return CGlGroups::fetchGlGroupsBySystemCodeByCid( $this->getSystemCode(), $this->getCid(), $objClientDatabase );
	}

	public function fetchMaxOrderNum( $objClientDatabase ) {

		$arrintData = CGlGroups::fetchMaxOrderNumByParentGlGroupIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );

		if( true == valArr( $arrintData ) ) return $arrintData[0]['order_num'];
		return 0;
	}

	/**
	 * Validation Functions
	 */

	public function valName() {

		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Group name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSummaryRowName() {

		$boolIsValid = true;

		if( CGlGroupType::TOTAL_GROUP != $this->getGlGroupTypeId()
		    && true == $this->getShowSummaryRowName()
		    && false == valStr( $this->getSummaryRowName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Total group name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTotalName( $boolIsSibling ) {

		$boolIsValid = true;

		if( ( true == $boolIsSibling || CGlGroupType::TOTAL_GROUP == $this->getGlGroupTypeId() ) && false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Total name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSiblingGlGroups() {

		$boolIsValid = true;

		if( true == valArr( $this->getSiblingGlGroups() ) ) {

			$intCountTotalGroup			= 0;
			$arrobjSiblingGlGroups	= $this->getSiblingGlGroups();

			foreach( $arrobjSiblingGlGroups as $objSiblingGlGroup ) {
				if( CGlGroupType::TOTAL_GROUP == $objSiblingGlGroup->getGlGroupTypeId() ) {
					$intCountTotalGroup++;
				}
			}

			if( 1 <= $intCountTotalGroup && 1 < \Psi\Libraries\UtilFunctions\count( $arrobjSiblingGlGroups ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'More than One account group is not allowed.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valGlGroupDependencies( $objClientDatabase ) {

		$boolIsValid = true;

		$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . ' AND gl_tree_id = ' . ( int ) $this->getGlTreeId() . ' AND parent_gl_group_id = ' . ( int ) $this->getId();

		$intGlGroupCount = CGlGroups::fetchGlGroupCount( $strWhere, $objClientDatabase );

		if( 0 < $intGlGroupCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Gl Group can not be deleted, it is associated with masked account groups.' ) ) );
			$boolIsValid &= false;
		}

		$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . ' AND gl_group_id = ' . ( int ) $this->getId();

		$intGlAccountTreeCount = CGlAccountTrees::fetchGlAccountTreeCount( $strWhere, $objClientDatabase );

		if( 0 < $intGlAccountTreeCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Gl Group can not be deleted, it is associated with masked accounts.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valDisabledOn( $boolIsSibling, $objClientDatabase ) {

		if( true == $boolIsSibling ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Group cannot be disabled, if it is being grouped with other objects.' ) ) );
			return false;
		}

		$strWhere = 'WHERE cid = ' . ( int ) $this->getCid() . ' AND parent_gl_group_id = ' . ( int ) $this->getId() . ' AND disabled_by IS NULL ';

		$intGlGroupCount = CGlGroups::fetchGlGroupCount( $strWhere, $objClientDatabase );

		if( 0 < $intGlGroupCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Group can not be disabled, an enabled sub group(s) is attached.' ) ) );
			return false;
		}

		$strWhere = 'WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND gl_account_id IN (
												SELECT
													ga.id
												FROM
													gl_accounts ga
													JOIN gl_account_trees gat ON( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
												WHERE
													gat.cid = ' . ( int ) $this->getCid() . '
													AND gat.gl_tree_id = ' . ( int ) $this->getGlTreeId() . '
													AND gat.gl_group_id = ' . ( int ) $this->getId() . '
													AND ga.disabled_by IS NULL
											)';

		$intGlAccountTreeCount = CGlAccountTrees::fetchGlAccountTreeCount( $strWhere, $objClientDatabase );

		if( 0 < $intGlAccountTreeCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Group can not be disabled, an enabled GL account(s) is attached.' ) ) );
			return false;
		}

		$objParentGlGroup = $this->fetchParentGlGroup( $objClientDatabase );

		if( CGlGroupType::TOTAL_GROUP == $objParentGlGroup->getGlGroupTypeId() ) {

			$strWhere = 'WHERE cid = ' . ( int ) $objParentGlGroup->getCid() . ' AND parent_gl_group_id = ' . ( int ) $objParentGlGroup->getId();
			$intSiblingGlGroupCount = CGlGroups::fetchGlGroupCount( $strWhere, $objClientDatabase );

			if( 0 < $intSiblingGlGroupCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Group can not be disabled, it is associated with a total group.' ) ) );
				return false;
			}
		}

		return true;
	}

	public function valGlAccountTrees() {

		$boolIsValid = true;

		if( CGlGroupType::NET_CASH_FLOW_GROUP == $this->getGlGroupTypeId() && 0 == \Psi\Libraries\UtilFunctions\count( $this->getGlAccountTrees() ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Select at least one Gl Account.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClientDatabase = NULL, $boolIsSibling = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'disable_gl_group_update':
				$boolIsValid &= $this->valDisabledOn( $boolIsSibling, $objClientDatabase );
			case VALIDATE_UPDATE:
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				// $boolIsValid &= $this->valSummaryRowName();
				$boolIsValid &= $this->valSiblingGlGroups();
				$boolIsValid &= $this->valGlAccountTrees();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valGlGroupDependencies( $objClientDatabase );
				break;

			case 'validate_total_name':
				$boolIsValid &= $this->valTotalName( $boolIsSibling );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;

	}

}
?>