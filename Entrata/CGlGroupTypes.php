<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlGroupTypes
 * Do not add any new functions to this class.
 */

class CGlGroupTypes extends CBaseGlGroupTypes {

	public static function fetchGlGroupTypes( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlGroupType', $objClientDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchGlGroupType( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlGroupType', $objClientDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchActiveGlGroupTypes( $objClientDatabase ) {

		$strSql = 'SELECT * FROM gl_group_types WHERE is_published = 1 ORDER BY order_num';

		return self::fetchGlGroupTypes( $strSql, $objClientDatabase );
	}
}
?>