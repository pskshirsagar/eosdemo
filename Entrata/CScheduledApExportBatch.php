<?php

class CScheduledApExportBatch extends CBaseScheduledApExportBatch {

	protected $m_intPropertyId;
	protected $m_strEndOn;
	protected $m_strCompanyName;
	protected $m_strPropertyName;
	protected $m_strBeforeEditScheduledStartOn;
	protected $m_strBeforeEditNextPostDate;

	protected $m_boolIsEdit;
	protected $m_boolIsClientFtp;
	protected $m_boolIsEntrataFtp;
	protected $m_intConnectionType;
	protected $m_strHostName;
	protected $m_strUserName;
	protected $m_strPassword;
	protected $m_strFilePath;
	protected $m_intPort;

	protected $m_boolIsValidScheduledEndOn = true;

	const CONNECTION_TYPE_FTP = 1;
	const CONNECTION_TYPE_SFTP = 2;

	const FILE_EXTENSION_CSV = 'csv';
	const FILE_EXTENSION_XLS = 'xls';
	const FILE_EXTENSION_TXT = 'txt';
	const FILE_EXTENSION_IIF = 'iif';

    /**
     * Get Functions
     */

    public function getEndOn() {
    	return $this->m_strEndOn;
    }

    public function getPropertyId() {
    	return $this->m_intPropertyId;
    }

    public function getPropertyName() {
    	return $this->m_strPropertyName;
    }

    public function getCompanyName() {
    	return $this->m_strCompanyName;
    }

	public function getEntrataFtp() {
		return $this->m_boolIsEntrataFtp;
	}

	public function getClientFtp() {
		return $this->m_boolIsClientFtp;
	}

	public function getConnectionType() {
		return $this->m_intConnectionType;
	}

	public function getHostName() {
		return $this->m_strHostName;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getPort() {
		return $this->m_intPort;
	}

	public function getBeforeEditScheduledStartOn() {
    	return $this->m_strBeforeEditScheduledStartOn;
	}

	public function getBeforeEditNextPostDate() {
    	return $this->m_strBeforeEditNextPostDate;
	}

	public function getIsSchedulerUpdate() {
		return $this->m_boolIsEdit;
	}

    /**
     * Set Functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['end_on'] ) ) {
    		$this->setEndOn( $arrmixValues['end_on'] );
		}

    	if( true == isset( $arrmixValues['property_id'] ) ) {
    		$this->setPropertyId( $arrmixValues['property_id'] );
    	}

    	if( true == isset( $arrmixValues['property_name'] ) ) {
    		$this->setPropertyName( $arrmixValues['property_name'] );
	    }

    	if( true == isset( $arrmixValues['company_name'] ) ) {
    		$this->setCompanyName( $arrmixValues['company_name'] );
	    }

	    if( true == isset( $arrmixValues['connection_type'] ) ) {
		    $this->setConnectionType( $arrmixValues['connection_type'] );
	    }

	    if( true == isset( $arrmixValues['entrata_ftp'] ) ) {
		    if( self::CONNECTION_TYPE_SFTP == $this->getConnectionType() ) {
			    $this->setEntrataFtp( false );
			    $this->setClientFtp( true );
		    } else {
			    $this->setEntrataFtp( $arrmixValues['entrata_ftp'] );
			    $this->setClientFtp( false );
			    if( false == $this->getEntrataFtp() ) {
				    $this->setClientFtp( true );
			    }
		    }
	    }

	    if( true == isset( $arrmixValues['host_ip'] ) ) {
		    $this->setHostName( $arrmixValues['host_ip'] );
	    }

	    if( true == isset( $arrmixValues['user'] ) ) {
		    $this->setUserName( $arrmixValues['user'] );
	    }

	    if( true == isset( $arrmixValues['password'] ) ) {
		    $this->setPassword( $arrmixValues['password'] );
	    }

	    if( true == isset( $arrmixValues['file_path'] ) ) {
		    $this->setFilePath( $arrmixValues['file_path'] );
	    }

	    if( true == isset( $arrmixValues['port'] ) ) {
		    $this->setPort( $arrmixValues['port'] );
	    }

    	return;
    }

    public function setEndOn( $strEndOn ) {
    	$this->m_strEndOn = $strEndOn;
    }

    public function setPropertyId( $intPropertyId ) {
    	$this->m_intPropertyId = $intPropertyId;
    }

    public function setPropertyName( $strPropertyName ) {
    	$this->m_strPropertyName = $strPropertyName;
    }

    public function setCompanyName( $strCompanyName ) {
    	$this->m_strCompanyName = $strCompanyName;
    }

    public function createPropertyApExportBatch() {

    	$objPropertyApExportBatch = new CPropertyApExportBatch();

    	$objPropertyApExportBatch->setCid( $this->getCid() );
    	$objPropertyApExportBatch->setScheduledApExportBatchId( $this->getId() );

    	return $objPropertyApExportBatch;
    }

	public function setEntrataFtp( $boolIsEntrataFtp ) {
		$this->m_boolIsEntrataFtp = $boolIsEntrataFtp;
	}

	public function setClientFtp( $boolIsClientFtp ) {
		$this->m_boolIsClientFtp = $boolIsClientFtp;
	}

	public function setConnectionType( $intConnectionType ) {
		$this->m_intConnectionType = $intConnectionType;
	}

	public function setHostName( $strHostName ) {
		$this->m_strHostName = $strHostName;
		if( self::CONNECTION_TYPE_SFTP == $this->getConnectionType() ) {
			$this->m_strHostName = 'sftp://' . $strHostName;
		}
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setPassword( $strPassword ) {
		$this->m_strPassword = $strPassword;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setPort( $intPort ) {
		$this->m_intPort = $intPort;
	}

	public function setBeforeEditScheduledStartOn( $strBeforeEditScheduledStartOn ) {
    	$this->m_strBeforeEditScheduledStartOn = $strBeforeEditScheduledStartOn;
	}

	public function setBeforeEditNextPostDate( $strBeforeEditNextPostDate ) {
		$this->m_strBeforeEditNextPostDate = $strBeforeEditNextPostDate;
	}

	public function setIsSchedulerUpdate( $boolIsEdit ) {
    	$this->m_boolIsEdit = $boolIsEdit;
	}

	/**
	 * Validate Function
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valApExportBatchTypeId();
				$boolIsValid &= $this->valTransmissionVendorId();
				$boolIsValid &= $this->valHostName();
				$boolIsValid &= $this->valUserName();
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valFilePath();
				$boolIsValid &= $this->valPort();
				$boolIsValid &= $this->valFrequencyId();
				$boolIsValid &= $this->valFrequencyInterval();
				$boolIsValid &= $this->valWeekDays();
				$boolIsValid &= $this->valMonthDays();
				$boolIsValid &= $this->valScheduledStartOn();
				$boolIsValid &= $this->valScheduledEndOn();
				$boolIsValid &= $this->valNumberOfOccurrences();
				$boolIsValid &= $this->valNextPostDate();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id required.' ) );
		}

		return $boolIsValid;
	}

	public function valTransmissionVendorId() {
		$boolIsValid = true;

			if( false == is_null( $this->getCompanyTransmissionVendorId() ) && false == is_numeric( $this->getCompanyTransmissionVendorId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transmission_vendor_id', 'Company Transmission vendor id is invalid' ) );
			}

		return $boolIsValid;
	}

	public function valApExportBatchTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getApExportBatchTypeId() ) || 0 >= ( int ) $this->getApExportBatchTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ap_export_batch_type_id', 'AP export batch type id required.' ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyId() ) || 0 >= ( int ) $this->getFrequencyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_id', 'Frequency id required.' ) );
		}

		return $boolIsValid;
	}

	public function valFrequencyInterval() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrequencyInterval() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', 'Frequency interval is required.' ) );
		} elseif( false == is_numeric( $this->getFrequencyInterval() ) || true == is_null( $this->getFrequencyInterval() ) || 0 >= $this->getFrequencyInterval() || 365 < $this->getFrequencyInterval() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'frequency_interval', 'Frequency interval is invalid.' ) );
		}

		return $boolIsValid;
	}

	public function valWeekDays() {
		$boolIsValid = true;

		if( true == is_null( $this->getWeekDays() ) && ( CFrequency::WEEKLY == $this->getFrequencyId() || CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'week_days', 'Weekday is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMonthDays() {
		$boolIsValid = true;

		if( false == valStr( $this->getMonthDays() ) && CFrequency::MONTHLY == $this->getFrequencyId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month_days', 'Day(s) of month is required.' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledStartOn() {

		$boolIsValid = true;

		if( false == valStr( $this->getScheduledStartOn() ) ) {
			$boolIsValid &= false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', 'Scheduled start on date is required.' ) );
			return $boolIsValid;
		}

		if( false == preg_match( '#^(0?[1-9]|1[0-2])/(0?[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4}$#', $this->getScheduledStartOn() ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', 'Scheduled start on date is invalid.' ) );
			return $boolIsValid;
		}

		if( false == CValidation::checkDateFormat( $this->getScheduledStartOn(), true ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', 'Scheduled start on date is invalid.' ) );
		} elseif( false == valId( $this->getId() ) && true == $boolIsValid && strtotime( date( 'm/d/Y' ) ) >= strtotime( $this->getScheduledStartOn() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', 'Scheduled start on date can not be past date or current date.' ) );
		}

		if( false != $this->getIsSchedulerUpdate() && ( false == valStr( $this->getNextPostDate() ) || NULL != $this->getDisabledOn() ) ) {
			if( strtotime( date( 'm/d/Y' ) ) >= strtotime( $this->getScheduledStartOn() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_start_on', 'Scheduled start on date can not be past date or current date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valScheduledEndOn() {

		$boolIsValid = true;

		if( true == valStr( $this->getEndOn() ) && 'date' == $this->getEndOn() ) {
			if( false == valStr( $this->getScheduledEndOn() ) ) {
				$boolIsValid &= false;
				$this->m_boolIsValidScheduledEndOn = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_on', 'Scheduled end on date is required.' ) );
				return $boolIsValid;
			}

			if( false == preg_match( '#^(0?[1-9]|1[0-2])/(0?[1-9]|[1-2][0-9]|3[0-1])/[0-9]{4}$#', $this->getScheduledEndOn() ) ) {
				$boolIsValid &= false;
				$this->m_boolIsValidScheduledEndOn = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_on', 'Scheduled end on date is invalid.' ) );

				return $boolIsValid;
			}

			if( false == CValidation::checkDateFormat( $this->getScheduledEndOn(), true ) ) {
				$boolIsValid &= false;
				$this->m_boolIsValidScheduledEndOn = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_on', 'Scheduled end on date is invalid.' ) );
			} elseif( strtotime( $this->getScheduledStartOn() ) > strtotime( $this->getScheduledEndOn() ) ) {
				$boolIsValid &= false;
				$this->m_boolIsValidScheduledEndOn = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_end_on', 'Scheduled end on date should be greater than or equal to ' . $this->getScheduledStartOn() . '.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valNumberOfOccurrences() {
		$boolIsValid = true;

		if( true == valStr( $this->getEndOn() ) && 'occurrences' == $this->getEndOn() ) {
			if( true == is_null( $this->getNumberOfOccurrences() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', 'Number of occurrences is required.' ) );
			} elseif( false == is_numeric( $this->getNumberOfOccurrences() ) || 0 == $this->getNumberOfOccurrences() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_occurrences', 'Invalid number of occurrences.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valNextPostDate() {
		$boolIsValid = true;

		if( true == valStr( $this->getEndOn() ) && false == is_null( $this->getScheduledEndOn() ) && ( 'date' == $this->getEndOn() || 'occurrences' == $this->getEndOn() ) ) {
			if( true == valStr( $this->getNextPostDate() ) && true == valStr( $this->getScheduledEndOn() ) && false == CValidation::checkDateFormat( $this->getNextPostDate(), true ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_post_date', 'Next Post date is invalid.' ) );
			} elseif( true == valStr( $this->getScheduledEndOn() ) && strtotime( $this->getScheduledEndOn() ) < strtotime( $this->getNextPostDate() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'next_post_date', 'The end date ' . $this->getScheduledEndOn() . ' should be greater than or equal to the next occurrence date ' . $this->getNextPostDate() . ' .' ) );
			}
		}
		return $boolIsValid;
	}

	public function valHostName() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() ) {
			$strHostName = str_replace( 'sftp://', '', $this->getHostName() );
			if( false == valStr( $strHostName ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'host_ip', 'Host name is required.' ) );
			}

			if( false == filter_var( $strHostName, FILTER_VALIDATE_IP ) && false == filter_var( gethostbyname( $strHostName ), FILTER_VALIDATE_IP ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'host_ip', 'Host name is not valid.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valUserName() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() && false == valStr( $this->getUserName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user', 'User name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() && false == valStr( $this->getPassword() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPort() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() && self::CONNECTION_TYPE_SFTP == $this->getConnectionType() && false != is_null( $this->getPort() ) && false == is_numeric( $this->getPort() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', 'Port is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;

		if( true == $this->getClientFtp() && false != valStr( $this->getFilePath() ) && false == preg_match( '/^[\w\/\\\]*$/', $this->getFilePath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', 'File path cannot contain special characters except _ \ / ' ) );
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Function
	 */

	public function fetchPropertyIds( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyApExportBatches::createService()->fetchPropertyIdsByScheduledApExportBatchIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPropertyApExportBatches( $objDatabase ) {
		return \Psi\Eos\Entrata\CPropertyApExportBatches::createService()->fetchPropertyApExportBatchesByScheduledApExportBatchIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Other Function
	 */

	public function calculateScriptStartDate() {

		$boolTakeLastDayOfMonth		= false;
		$boolTakeNextDayFromMonth	= false;
		$strScriptStartDate			= NULL;
		$strPostDate				= $this->getScheduledStartOn();
		$intFrequencyInterval		= $this->getFrequencyInterval();
		$arrintDaysOfMonth			= ( true == valStr( $this->getMonthDays() ) ) ? explode( ',', $this->getMonthDays() ) : [];
		$arrmixDayNames				= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];
		$arrmixRelativeFormats		= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];

		if( false == is_numeric( $intFrequencyInterval ) || false == $this->m_boolIsValidScheduledEndOn ) {
			return NULL;
		}

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
			case CFrequency::YEARLY:
				$strScriptStartDate = $strPostDate;
				break;

			case CFrequency::WEEKLY:
				if( true == is_numeric( $this->getWeekDays() ) ) {
					$objStartDatetime	= new DateTime( $strPostDate );
					$objStartDatetime->modify( $arrmixDayNames[$this->getWeekDays()] );
					$strScriptStartDate	= $objStartDatetime->format( 'm/d/Y' );

					// if script start date is passed then calculate next day date
					if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
						$objStartDatetime->modify( 'next ' . $arrmixDayNames[$this->getWeekDays()] );
						$strScriptStartDate = $objStartDatetime->format( 'm/d/Y' );
					}
				}
				break;

			case CFrequency::MONTHLY:
			 	if( false == valArr( $arrintDaysOfMonth ) ) {
					return NULL;
				}

				$intNextPostDay	= $arrintDaysOfMonth[0];
				$intDayKey		= array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

				if( true == is_numeric( $intDayKey ) ) {

					if( true == array_key_exists( $intDayKey, $arrintDaysOfMonth ) ) {
						$intNextPostDay = $arrintDaysOfMonth[$intDayKey];
					}
					$strScriptStartDate 			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
					$strScriptStartDate 			= date( 'm/d/Y', strtotime( $strScriptStartDate ) );
					$boolTakeNextDayFromMonth		= true;
				} else {

					$arrstrPostDate = explode( '/', $strPostDate );

					$intDay = $arrstrPostDate[1];

					foreach( $arrintDaysOfMonth as $intDate ) {
						if( $intDate > $intDay ) {
							$intNextPostDay				= $intDate;
							$boolTakeNextDayFromMonth	= true;
							break;
						}
					}

					$strScriptStartDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

					if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
						$objNextPostMonthDatetime = new DateTime( $strScriptStartDate );
						$objNextPostMonthDatetime->modify( $intFrequencyInterval . ' month' );
						$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
					}
				}

				if( false == $boolTakeNextDayFromMonth && ( 0 == $intNextPostDay || ( false == $intDayKey && false != array_search( 0, $arrintDaysOfMonth ) ) ) ) {
					$boolTakeLastDayOfMonth = true;
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				if( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getWeekDays() ) ) {
					return NULL;
				}

				$strPostDate 		= date( $strPostDate );
				$strDayName 		= $arrmixDayNames[$this->getWeekDays()];
				$strRelativeNumber	= $arrmixRelativeFormats[$intFrequencyInterval];

				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of this month' );
				$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );

				if( strtotime( $strPostDate ) > strtotime( $strScriptStartDate ) ) {
					$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
					$strScriptStartDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				}
				break;

			default:
		}// switch

		$arrstrNextPostDate = explode( '/', $strScriptStartDate );
		if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
			$boolTakeLastDayOfMonth = true;
		}

		if( true == $boolTakeLastDayOfMonth ) {

			$objDateTimeCurrentPostDate = new DateTime( $strPostDate );
			$objDateTimeCurrentPostDate->modify( 'last day of this month' );
			$strScriptStartDate = $objDateTimeCurrentPostDate->format( 'm/d/Y' );
		}

		return date( 'm/d/Y', strtotime( $strScriptStartDate ) );
	}

	public function calculateEndDate( $objFrequency ) {

		if( false == is_numeric( $this->getNumberOfOccurrences() ) || 0 >= $this->getNumberOfOccurrences() ) {
			return NULL;
		}

		$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() ) );

		switch( $this->getFrequencyId() ) {
			case CFrequency::DAILY:
			case CFrequency::YEARLY:
			case CFrequency::WEEKLY:
				$intNumberOfIntervalUnits	= ( $this->getNumberOfOccurrences() - 1 ) * $this->getFrequencyInterval();
				$strIntervalLabel			= $objFrequency->getIntervalFrequencyLabelPlural();

				if( 0 < $intNumberOfIntervalUnits ) {
					$strEndDate	= date( 'm/d/Y', strtotime( $this->getNextPostDate() . ' +' . $intNumberOfIntervalUnits . ' ' . $strIntervalLabel ) );
				}
				break;

			case CFrequency::MONTHLY:
			case CFrequency::WEEKDAY_OF_MONTH:
				if( ( CFrequency::WEEKDAY_OF_MONTH == $this->getFrequencyId() && false == is_numeric( $this->getWeekDays() ) ) || ( CFrequency::MONTHLY == $this->getFrequencyId() && false == valStr( $this->getMonthDays() ) ) ) {
					return NULL;
				}

				$strScriptStartDate		= $this->getNextPostDate();
				$intNumberOfOccurrences	= $this->getNumberOfOccurrences();

				for( $intCounter = 2; $intCounter <= $intNumberOfOccurrences; $intCounter++ ) {

					$strEndDate = $this->calculateNextPostDate();
					$this->setNextPostDate( $strEndDate );
				}
				$this->setNextPostDate( $strScriptStartDate );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strEndDate ) );
	}

	public function calculateNextPostDate() {
		if( false == is_numeric( $this->getFrequencyInterval() ) ) {
			return NULL;
		}

		$intNextPostDay			= NULL;
		$intFrequencyInterval	= ( int ) $this->getFrequencyInterval();
		$arrintDaysOfMonth		= explode( ',', $this->getMonthDays() );
		$strPostDate			= $this->getNextPostDate();

		$arrmixRelativeFormats	= [ 1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth' ];
		$arrmixDayNames			= [ 0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday' ];

		switch( $this->getFrequencyId() ) {

			case CFrequency::DAILY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . $intFrequencyInterval . ' Days' ) );
				break;

			case CFrequency::WEEKLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . $intFrequencyInterval . ' Weeks' ) );
				break;

			case CFrequency::YEARLY:
				$strNextPostDate = date( 'm/d/Y', strtotime( $strPostDate . ' + ' . $intFrequencyInterval . ' Years' ) );

				if( date( 'm', strtotime( $strPostDate ) ) != date( 'm', strtotime( $strNextPostDate ) ) ) {

					$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
					$objDateTimeNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
				}
				break;

			case CFrequency::MONTHLY:
				if( true == valArr( $arrintDaysOfMonth ) ) {

					$intDayKey = array_search( date( 'j', strtotime( $strPostDate ) ), $arrintDaysOfMonth );

					if( true == is_numeric( $intDayKey ) ) {

						if( true == array_key_exists( $intDayKey + 1, $arrintDaysOfMonth ) ) {

							$intNextPostDay = $arrintDaysOfMonth[$intDayKey + 1];

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}
							} else {

								$strNextPostDate = date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );

								$arrstrNextPostDate = explode( '/', $strNextPostDate );

								if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {

									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/01/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( 'last day of this month' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}

								if( strtotime( $strPostDate ) == strtotime( $strNextPostDate ) ) {

									$intNextPostDay				= current( $arrintDaysOfMonth );
									$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
									$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
									$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
									$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
								}
							}
						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

						}
					} else {

						if( 1 == \Psi\Libraries\UtilFunctions\count( $arrintDaysOfMonth ) ) {

							$intNextPostDay = current( $arrintDaysOfMonth );

							if( 0 == $intNextPostDay ) {

								$objDateTimeNextPostDate	= new DateTime( $strPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$objDateTimeNextPostDate->modify( 'last day of this month' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );

							} else {

								$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
								$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
								$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
								$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
							}

						} else {

							$intNextPostDay				= current( $arrintDaysOfMonth );
							$strNextPostDate			= date( 'm', strtotime( $strPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strPostDate ) );
							$objDateTimeNextPostDate	= new DateTime( $strNextPostDate );
							$objDateTimeNextPostDate->modify( '+' . $intFrequencyInterval . ' months' );
							$strNextPostDate			= $objDateTimeNextPostDate->format( 'm/d/Y' );
						}
					}
				}

				$objPostDate = new DateTime( $strPostDate );
				$objPostDate->modify( 'first day of this month' );

				$objNextPostDate = new DateTime( $strNextPostDate );
				$objNextPostDate->modify( 'first day of this month' );

				$objInterval = date_diff( $objPostDate, $objNextPostDate );

				if( $objInterval->format( '%m' ) != $intFrequencyInterval && $objInterval->format( '%m' ) != 0 ) {

					$intNextPostDay		= current( $arrintDaysOfMonth );
					$objNextPostDate	= new DateTime( $strNextPostDate );
					$objNextPostDate->modify( 'last day of previous month' );
					$strNextPostDate	= $objNextPostDate->format( 'm/d/Y' );

					if( 0 != $intNextPostDay ) {

						$strNextPostDateCopy = $strNextPostDate;

						$strNextPostDate	= date( 'm', strtotime( $strNextPostDate ) ) . '/' . $intNextPostDay . '/' . date( 'Y', strtotime( $strNextPostDate ) );
						$arrstrNextPostDate	= explode( '/', $strNextPostDate );

						if( 3 == \Psi\Libraries\UtilFunctions\count( $arrstrNextPostDate ) && false == checkdate( $arrstrNextPostDate[0], $arrstrNextPostDate[1], $arrstrNextPostDate[2] ) ) {
							$strNextPostDate = $strNextPostDateCopy;
						}
					}
				}
				break;

			case CFrequency::WEEKDAY_OF_MONTH:
				$strDayName = $arrmixDayNames[$this->getWeekDays()];
				$strRelativeNumber = $arrmixRelativeFormats[$intFrequencyInterval];
				$objNextPostMonthDatetime = new DateTime( $strPostDate );
				$objNextPostMonthDatetime->modify( $strRelativeNumber . ' ' . $strDayName . ' of next month' );
				$strNextPostDate = $objNextPostMonthDatetime->format( 'm/d/Y' );
				break;

			default:
		}

		return date( 'm/d/Y', strtotime( $strNextPostDate ) );
	}

}
?>