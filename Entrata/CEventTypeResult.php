<?php

class CEventTypeResult extends CBaseEventTypeResult {

	protected $m_intCid;
	protected $m_intEventTypeId;
	protected $m_intEventResultId;
	protected $m_intDefaultEventResultId;

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['cid'] ) )	$this->setCid( $arrmixValues['cid'] );
		if( true == isset( $arrmixValues['event_type_id'] ) )	$this->setEventTypeId( $arrmixValues['event_type_id'] );
		if( true == isset( $arrmixValues['event_result_id'] ) )	$this->setEventResultId( $arrmixValues['event_result_id'] );
		if( true == isset( $arrmixValues['default_event_result_id'] ) )	$this->setDefaultEventResultId( $arrmixValues['default_event_result_id'] );
		return;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setEventTypeId( $intEventTypeId ) {
		$this->m_intEventTypeId = $intEventTypeId;
	}

	public function setEventResultId( $intEventResultId ) {
		$this->m_intEventResultId = $intEventResultId;
	}

	public function setDefaultEventResultId( $intDefaultEventResultId ) {
		$this->m_intDefaultEventResultId = $intDefaultEventResultId;
	}

	/**
	 * Get Functions
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getEventTypeId() {
		return $this->m_intEventTypeId;
	}

	public function getEventResultId() {
		return $this->m_intEventResultId;
	}

	public function getDefaultEventResultId() {
		return $this->m_intDefaultEventResultId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEventTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getEventTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Event Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEventResultId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valEventTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				$boolIsValid &= $this->valEventTypeId();
				break;
		}

		return $boolIsValid;
	}

}
?>