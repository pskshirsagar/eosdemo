<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultCalendarEventTypes
 * Do not add any new functions to this class.
 */

class CDefaultCalendarEventTypes extends CBaseDefaultCalendarEventTypes {
	//static variables
	const GENERAL 				= 1;
	const COMMUNITY 			= 2;
	const UNAVAILABLE 			= 3;
	const MOVE_IN				= 4;
	const MOVE_OUT				= 5;
	const TRANSFER				= 6;
	const RESIDENT_APPOINTMENT 	= 7;
	const TOUR					= 8;
	const WORK_ORDER			= 9;
	const LEASING_APPOINTMENT	= 10;
	const INSPECTION			= 11;


    public static function fetchDefaultCalendarEventTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CDefaultCalendarEventType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchDefaultCalendarEventType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CDefaultCalendarEventType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>