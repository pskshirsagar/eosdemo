<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAlertPriorities
 * Do not add any new functions to this class.
 */

class CAlertPriorities extends CBaseAlertPriorities {

	public static function fetchAlertPriority( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CAlertPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}
}
?>