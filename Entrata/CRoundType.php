<?php

class CRoundType extends CBaseRoundType {

	const DO_NOT_ROUND 		= 1;
	const STANDARD_ROUND 	= 2;
	const ROUND_UP 			= 3;
	const ROUND_DOWN 		= 4;

	protected $m_arrmixRoundTypes;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getAllRoundTypes() {

		if( isset( $this->m_arrmixRoundTypes ) ) {
			return $this->m_arrmixRoundTypes;
		}

		$this->m_arrmixRoundTypes	= [
			self::DO_NOT_ROUND => __( 'Do Not Round' ),
			self::ROUND_UP => __( 'Round Up To Nearest Dollar' ),
			self::ROUND_DOWN => __( 'Round Down To Nearest Dollar' ),
			self::STANDARD_ROUND => __( 'Standard Rounding' )
		];
		return $this->m_arrmixRoundTypes;
	}

}
?>