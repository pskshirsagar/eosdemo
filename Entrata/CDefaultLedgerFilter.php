<?php

class CDefaultLedgerFilter extends CBaseDefaultLedgerFilter {

	const   RESIDENT        = 1;
	const   SUBSIDY         = 2;
	const   MILITARY        = 3;
	const   GROUP           = 4;
    const   STRAIGHT_LINE   = 5;

	// IMPORTANT: used for associating default ledger and every new default ledger must be added here.
	public static $c_arrintOccupancyTypeIdByDefaultLedgerId = [
		self::SUBSIDY       => COccupancyType::AFFORDABLE,
		self::MILITARY      => COccupancyType::MILITARY
	];

	// IMPORTANT: used for associating default ledger and every new default ledger must be added here.
	public static $c_arrintPaymentTypeIdByDefaultLedgerId = [
		self::SUBSIDY       => CPaymentType::HAP,
		self::MILITARY      => CPaymentType::BAH
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>