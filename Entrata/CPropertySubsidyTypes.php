<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertySubsidyTypes
 * Do not add any new functions to this class.
 */

class CPropertySubsidyTypes extends CBasePropertySubsidyTypes {

	public static function fetchActivePropertySubsidyTypesByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase ) {
		if ( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_subsidy_types
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL';

		return self::fetchPropertySubsidyTypes( $strSql, $objDatabase );
	}

	public static function fetchPropertyIdByPropertyIdBySubsidyTypeId( $intPropertyId, $intSubsidyTypeId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intSubsidyTypeId ) ) return NULL;

		$strSql = 'SELECT
						property_id
					FROM
						property_subsidy_types
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND subsidy_type_id = ' . ( int ) $intSubsidyTypeId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'property_id', $objDatabase );
	}

	public static function fetchActivePropertySubsidyTypeCountByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase ) {
		if ( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						count( id )
					FROM
						property_subsidy_types
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchActivePropertySubsidyTypesBySubsidyTypeIdsByPropertyIdsByCId( $arrintSubsidyTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						pst.*
					FROM
						property_subsidy_types pst
					WHERE
						pst.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND pst.cid = ' . ( int ) $intCid . '
						AND pst.subsidy_type_id IN ( ' . implode( ',', $arrintSubsidyTypeIds ) . ' )
						AND pst.deleted_on IS NULL
						AND pst.deleted_by IS NULL';

		return self::fetchPropertySubsidyTypes( $strSql, $objDatabase );
	}

	public static function fetchActivePropertySubsidyTypeByPropertyIdByCId( $intPropertyId, $intCid, $objDatabase ) {
		if ( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						CASE
							WHEN count( distinct( pst.subsidy_type_id ) ) = 1 AND 1 = ANY ( array_agg( distinct( pst.subsidy_type_id ) ) ) THEN 1 -- HUD
							WHEN count( distinct( pst.subsidy_type_id ) ) = 1 AND 2 = ANY ( array_agg( distinct( pst.subsidy_type_id ) ) ) THEN 2 -- Tax Credit
							WHEN count( distinct( pst.subsidy_type_id ) ) = 2 THEN 3 -- Layered
						END as property_subsidy_type
					FROM
						property_subsidy_types pst
						LEFT JOIN subsidy_contracts sc ON ( pst.cid = sc.cid AND pst.property_id = sc.property_id AND CURRENT_DATE BETWEEN sc.start_date AND sc.end_date )
						LEFT JOIN set_asides sas ON ( pst.cid = sas.cid AND pst.property_id = sas.property_id AND CURRENT_DATE >= sas.placed_in_service_date )
					WHERE
						pst.cid = ' . ( int ) $intCid . '
						AND pst.property_id = ' . ( int ) $intPropertyId . '
						AND pst.deleted_by IS NULL
						AND sc.deleted_by IS NULL
						AND sas.deleted_by IS NULL';

		return self::fetchColumn( $strSql, 'property_subsidy_type', $objDatabase );

	}

	public static function fetchActivePropertySubsidyTypesByPropertyIdsByCId( $arrintPropertyId, $intCid, $objDatabase ) {
		if ( false == valArr( $arrintPropertyId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_subsidy_types
					WHERE
						property_id IN ( ' . implode( ',', $arrintPropertyId ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL';

		$arrobjPropertiesSubsidyTypes = self::fetchPropertySubsidyTypes( $strSql, $objDatabase );

		if( true == valArr( $arrobjPropertiesSubsidyTypes ) ) {
			return rekeyObjects( 'PropertyId', $arrobjPropertiesSubsidyTypes, true );
		}

		return NULL;
	}

	public static function fetchPropertySubsidyTypeIncomeLimitVersionByPropertyIdByCidBySubsidyTypeId( $intPropertyId, $intCid, $intSubsidyTypeId, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valId( $intSubsidyTypeId ) ) return NULL;

		$strSql = 'SELECT
					    pst.*,
					    silv.property_id as subsidy_income_limit_version_property_id
					FROM
					    property_subsidy_types pst
						JOIN subsidy_income_limit_versions silv ON silv.id = pst.subsidy_income_limit_version_id
					WHERE
					    pst.property_id = ' . ( int ) $intPropertyId . '
					    AND pst.cid = ' . ( int ) $intCid . '
					    AND pst.subsidy_type_id = ' . ( int ) $intSubsidyTypeId . '
					    AND pst.deleted_on IS NULL
					    AND pst.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

}
?>