<?php

class CReportHistory extends CBaseReportHistory {

	protected $m_intMajor;
	protected $m_intMinor;
	protected $m_intCompanyUserId;
	protected $m_strReportFilterName;
	protected $m_strFilename;
	protected $m_strFilePath;
	protected $m_intFileId;
	protected $m_strReportInstanceName;
	/** @var CReport */
	protected $m_objReport;
	protected $m_arrmixFilterDescriptions;
	protected $m_arrmixDownloadOptions;
	protected $m_arrmixRecipientDetails;
	/** @var DateInterval */
	protected $m_objTimeQueued;
	/** @var DateInterval */
	protected $m_objTimeProcessing;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportInstanceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportScheduleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportFilterId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportScheduleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilters() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDownloadOptions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRecipients() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( true == isset( $arrmixValues['errors'] ) ) {
			$this->setErrors( $arrmixValues['errors'] );
			unset( $arrmixValues['errors'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['major'] ) ) $this->setMajor( $arrmixValues['major'] );
		if( true == isset( $arrmixValues['minor'] ) ) $this->setMinor( $arrmixValues['minor'] );

		if( true == isset( $arrmixValues['report_filter_name'] ) && $boolDirectSet ) {
			$this->m_strReportFilterName = trim( stripcslashes( $arrmixValues['report_filter_name'] ) );
		} elseif( isset( $arrmixValues['report_filter_name'] ) ) {
			$this->setReportFilterName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['report_filter_name'] ) : $arrmixValues['report_filter_name'] );
		}

		if( true == isset( $arrmixValues['recipients'] ) ) {
			$this->m_arrmixRecipientDetails = json_decode( $arrmixValues['recipients'], true );
		}
		if( true == isset( $arrmixValues['instance_name'] ) ) $this->setReportInstanceName( $arrmixValues['instance_name'] );
		if( true == isset( $arrmixValues['filename'] ) ) $this->setFilename( $arrmixValues['filename'] );
		if( true == isset( $arrmixValues['file_id'] ) ) $this->setFileId( $arrmixValues['file_id'] );
		if( true == isset( $arrmixValues['filePath'] ) ) $this->setFilename( $arrmixValues['filePath'] );
		if( true == isset( $arrmixValues['download_options'] ) ) $this->setDownloadOptionData( json_decode( $arrmixValues['download_options'], true ) );
		if( true == isset( $arrmixValues['company_user_id'] ) ) $this->setCompanyUserId( $arrmixValues['company_user_id'] );
	}

	/*
	 * Utility Methods
	 */

	public function createCompanyUserReportHistory( $intCompanyUserId, array $arrintPropertyGroupIds ) {

		$objCompanyUserReportHistory = new CCompanyUserReportHistory();
		$objCompanyUserReportHistory->setCid( $this->getCid() );
		$objCompanyUserReportHistory->setCompanyUserId( $intCompanyUserId );
		$objCompanyUserReportHistory->setReportHistoryId( $this->getId() );
		$objCompanyUserReportHistory->setPropertyGroupIds( json_encode( $arrintPropertyGroupIds ) );
		$objCompanyUserReportHistory->setIsArchived( 0 );
		$objCompanyUserReportHistory->setIsTemporary( is_null( $this->getReportScheduleId() ) );

		return $objCompanyUserReportHistory;
	}

	public function toNotification( DateTime $objUserTime, CDatabase $objDatabase ) {
		// Load report object
		$objReport = $this->getOrFetchReport( $objDatabase );

		$strOutputType = key( array_filter( $this->getDownloadOptionData(), function( $strKey ) {
			return false == in_array( $strKey, CReportSchedule::DLOPT_ALL_FLAGS, true );
		}, ARRAY_FILTER_USE_KEY ) );

		// Compute timestamps and intervals
		// Use the user's timezone as reported by the browser to convert the queued on time to their local timezone
		$objQueuedDateTime = new DateTime( $this->getQueuedDatetime() );
		$objQueuedDateTime->setTimezone( $objUserTime->getTimezone() );
		$this->setQueuedDatetime( $objQueuedDateTime->format( 'n/j/Y g:i a T' ) );

		$objStartedDateTime = new DateTime( $this->getStartedDatetime() );
		$objStartedDateTime->setTimezone( $objUserTime->getTimezone() );

		$objCompletedDateTime = new DateTime( $this->getCompletedDatetime() );
		$objCompletedDateTime->setTimezone( $objUserTime->getTimezone() );

		$this->setTimeQueued( $objStartedDateTime->diff( $objQueuedDateTime ) );
		$this->setTimeProcessing( $objCompletedDateTime->diff( $objStartedDateTime ) );

		if( $this->getReportNewInstanceId() ) {
			$strInstanceName = $this->getReportInstanceName();
		} else {
			$strInstanceName = $objReport->getTitle();
		}

		// To set the created date for the report histories in Processing status and for the older report histories which were not flushed out.
		$strCreatedDateTime = $this->getFormattedTimestamp( $objQueuedDateTime );

		if( !is_null( $this->getUserTime() ) ) {
			$objUserDateTime		= CReportsLibrary::parseJavascriptDateTime( $this->getUserTime() ) ?: $objQueuedDateTime;
			$strCreatedDateTime		= $this->getFormattedTimestamp( $objUserDateTime );
		}

		return array_merge( $this->toArray(), [
			'title'					=> $objReport->getTitle(),
			'major'					=> $objReport->getMajor(),
			'minor'					=> $objReport->getMinor(),
			'url'					=> $objReport->getReportUrl(),
			'output_type'			=> $strOutputType,
			'created_on'			=> $this->getQueuedDatetime(),
			'created'				=> $strCreatedDateTime,
			'queued'				=> $this->getTimeQueued()->format( '%h:%I:%S' ),
			'processing'			=> $this->getTimeProcessing()->format( '%h:%I:%S' ),
			'filter_descriptions'	=> $this->getCachedFilterDescriptions(),
			'instance_name'			=> $strInstanceName,
			'company_user_id'		=> $this->getCompanyUserId()
		] );
	}

	private function getFormattedTimestamp( DateTime $objDateTime ) {
		return CInternationalDateTime::create( $objDateTime )->format( CInternationalDateFormatter::DATETIME_NUMERIC_SHORT );
	}

	public function generateReportHistoryName( CReportNewInstance $objReportInstance, $strPacketName = '', DateTime $objToday = NULL ) {
		if( is_null( $objToday ) ) {
			$objToday = new DateTime();
		}

		if( '' != $strPacketName ) {
			// For report packets, the name will be <packet name> - <report name> - <filter name> - <date>
			$this->setName( implode( ' - ', array_filter( [ $strPacketName, $objReportInstance->getReportName(), $objReportInstance->getName(), $objToday->format( 'm-d-Y' ) ] ) ) );
		} else {
			// For single reports, the name will be <report name> - <filter name> - <date>
			$this->setName( implode( ' - ', array_filter( [ $objReportInstance->getReportName(), $objReportInstance->getName(), $objToday->format( 'm-d-Y' ) ] ) ) );
		}
	}

	/*
	 * Getters
	 */

	public function getMajor() {
		return $this->m_intMajor;
	}

	public function getMinor() {
		return $this->m_intMinor;
	}

	public function getReportFilterName() {
		return $this->m_strReportFilterName;
	}

	public function getFilename() {
		return $this->m_strFilename;
	}

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getOrFetchReport( CDatabase $objDatabase ) {
		if( false != is_null( $this->m_objReport ) ) {
			$objReport = \Psi\Eos\Entrata\CReports::createService()->fetchReportByReportVersionIdByCid( $this->getReportVersionId(), $this->getCid(), $objDatabase );
			if( false == valObj( $objReport, 'CReport' ) ) {
				$strReportName = explode( ' - ', $this->getName() )[0];
				$objReport = CSandboxReportLoader::getSandboxReport( $strReportName );
			}
			if( true == valObj( $objReport, 'CReport' ) ) {
				$this->setReport( $objReport );
			} else {
				$strErrorMessage = 'This is not valid object for report history ID - ' . $this->getId() . ', Client ' . $this->getCid() . ', Report version - ' . $this->getReportVersionId() . ', Database Name - ' . $objDatabase->getDatabaseName() . ' Created At ' . date( 'm/d/Y H:i:s' ) . ' .';
				$this->addErrorMsg( $strErrorMessage );
				return NULL;
			}
		}
		return $this->m_objReport;
	}

	public function getFilterDescriptions() {
		return $this->m_arrmixFilterDescriptions;
	}

	public function getDownloadOptionData() {
		return $this->m_arrmixDownloadOptions;
	}

	public function getRecipientDetails() {
		return $this->m_arrmixRecipientDetails;
	}

	public function getTimeQueued() {
		return $this->m_objTimeQueued;
	}

	public function getTimeProcessing() {
		return $this->m_objTimeProcessing;
	}

	public function getReportInstanceName() {
		return $this->m_strReportInstanceName;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getCachedFilterDescriptions() {
		return $this->getDetailsField( [ 'cached_filter_descriptions' ] ) ?: NULL;
	}

	public function getUserTime() {
		return $this->getDetailsField( [ 'user_time' ] ) ?: NULL;
	}

	/*
	 * Setters
	 */

	private function setMajor( $intMajor ) {
		$this->m_intMajor = ( int ) $intMajor;
		return $this;
	}

	private function setMinor( $intMinor ) {
		$this->m_intMinor = ( int ) $intMinor;
		return $this;
	}

	public function setReportFilterName( $strReportFilterName ) {
		$this->m_strReportFilterName = $strReportFilterName;
		return $this;
	}

	public function setFilename( $strFilename ) {
		$this->m_strFilename = $strFilename;
		return $this;
	}

	public function setFileId( $intFileid ) {
		$this->m_intFileId = $intFileid;
		return $this;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
		return $this;
	}

	public function setReport( CReport $objReport ) {
		$this->m_objReport = $objReport;
		return $this;
	}

	public function setFilterDescriptions( $arrmixFilterDescriptions ) {
		$this->m_arrmixFilterDescriptions = $arrmixFilterDescriptions;
		return $this;
	}

	public function setDownloadOptionData( $arrmixDownloadOptionData ) {
		$this->m_arrmixDownloadOptions = $arrmixDownloadOptionData;
		return $this;
	}

	public function setTimeQueued( DateInterval $objTimeQueued ) {
		$this->m_objTimeQueued = $objTimeQueued;
	}

	public function setTimeProcessing( DateInterval $objTimeProcessing ) {
		$this->m_objTimeProcessing = $objTimeProcessing;
	}

	public function setReportInstanceName( $strReportInstanceName ) {
		$this->m_strReportInstanceName = $strReportInstanceName;
		return $this;
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = $intCompanyUserId;
		return $this;
	}

}