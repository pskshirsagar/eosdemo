<?php

class CRevenueStudentDemand extends CBaseRevenueStudentDemand {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModelProvider() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDistance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemand() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valModelDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valForecastModel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>