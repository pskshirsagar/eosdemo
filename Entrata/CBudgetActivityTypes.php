<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetActivityTypes
 * Do not add any new functions to this class.
 */

class CBudgetActivityTypes extends CBaseBudgetActivityTypes {

	public static function fetchBudgetActivityTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CBudgetActivityType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchBudgetActivityType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CBudgetActivityType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>