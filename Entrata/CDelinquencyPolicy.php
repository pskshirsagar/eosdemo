<?php

class CDelinquencyPolicy extends CBaseDelinquencyPolicy {

	protected $m_intActiveDocumentCount;
	protected $m_intActiveLeaseCount;
	protected $m_intPropertyId;

	protected $m_arrobjExceptionDelinquencyPolicies;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {

		$boolIsValid = true;
		if( true == is_null( $this->getOccupancyTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupancy_type_id', __( 'Occupancy type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDelinquencyThresholdTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL, $boolIsFromExceptionPolicy = false ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Policy Name is required.' ) ) );

			return $boolIsValid;
		}

		if( false == is_null( $objDatabase ) ) {
			$intConfilictingDelinquencyPolicyCount = \Psi\Eos\Entrata\CDelinquencyPolicies::createService()->fetchConflictingNameCount( $this, $objDatabase, $boolIsFromExceptionPolicy );
			if( 0 < $intConfilictingDelinquencyPolicyCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Policy Name is already in use.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valThresholdAmount( $arrobjDelinquencyPolicyDocuments ) {

		$boolIsValid = true;
		if( true == valArr( $arrobjDelinquencyPolicyDocuments ) ) {
			if( true == is_null( $this->getThresholdAmount() ) || $this->getThresholdAmount() <= 0 ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'threshold_amount', __( 'Threshold amount is required and must be greater than {%m, amount}.', [ 'amount' => 0.00 ] ) ) );
				return $boolIsValid;
			}
		} elseif( false == valArr( $arrobjDelinquencyPolicyDocuments ) ) {
			if( $this->getThresholdAmount() != 0 ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'threshold_amount', __( 'Threshold amount should be {%m, amount} if the notice schedule is not configured.', [ 'amount' => 0.00 ] ) ) );
				return $boolIsValid;
			}
		}
		return $boolIsValid;
	}

	public function valSmallBalanceAmount( $arrobjDelinquencyPolicyDocuments ) {

		$boolIsValid = true;
		if( true == is_null( $this->getSmallBalanceAmount() ) || $this->getSmallBalanceAmount() <= 0 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'small_balance_amount', __( 'Small balance threshold is required and should be greater than zero.' ) ) );
			return $boolIsValid;
		}

		if( true == valArr( $arrobjDelinquencyPolicyDocuments ) && CDelinquencyThresholdType::FLAT_AMOUNT == $this->getDelinquencyThresholdTypeId() && $this->getSmallBalanceAmount() >= $this->getThresholdAmount() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'small_balance_amount', __( 'Small Balance Threshold should be less than the threshold for Delinquency Notices.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valFileTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyGroupIds( $arrintPropertyGroupIds ) {

		$boolIsValid = true;

		if( false == valArr( $arrintPropertyGroupIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_group_ids', __( 'At least one property is required.' ) ) );

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valSendToCollectionsThreshold() {

		$boolIsValid = true;
		if( is_numeric( $this->getSendToCollectionsThreshold() ) && $this->getSendToCollectionsThreshold() != 0 && $this->getSendToCollectionsThreshold() < $this->getThresholdAmount() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_to_collections_threshold', __( 'Send to Collections Threshold must be greater than or equal to Balance Threshold.' ) ) );
				return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valParentDelinquencyPolicyId( $boolIsFromExceptionPolicy ) {

		$boolIsValid = true;
		if( true == $boolIsFromExceptionPolicy && ( true == is_null( $this->getParentDelinquencyPolicyId() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_delinquency_policy_id', __( 'Parent policy is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $arrobjDelinquencyPolicyDocuments = NULL, $objDatabase = NULL, $boolIsFromExceptionPolicy = false ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case 'delinquency_policy_insert':
			case 'delinquency_policy_update':
			case 'collections_policy_insert':
			case 'collections_policy_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase, $boolIsFromExceptionPolicy );
				$boolIsValid &= $this->valOccupancyTypeId();
				$boolIsValid &= $this->valThresholdAmount( $arrobjDelinquencyPolicyDocuments );
				$boolIsValid &= $this->valSendToCollectionsThreshold();
				$boolIsValid &= $this->valParentDelinquencyPolicyId( $boolIsFromExceptionPolicy );
				break;

			case 'delinquency_policy_insert_small_balance':
				$boolIsValid &= $this->valSmallBalanceAmount( $arrobjDelinquencyPolicyDocuments );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createDelinquencyPolicyDocument() {

		$objDelinquencyPolicyDocument = new CDelinquencyPolicyDocument();
		$objDelinquencyPolicyDocument->setCid( $this->getCid() );
		$objDelinquencyPolicyDocument->setDelinquencyPolicyId( $this->getId() );

		return $objDelinquencyPolicyDocument;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet ); // TODO: Change the autogenerated stub

		if( isset( $arrmixValues['active_document_count'] ) ) {
			$this->setActiveDocumentCount( $arrmixValues['active_document_count'] );
		}

		if( isset( $arrmixValues['active_lease_count'] ) ) {
			$this->setActiveLeaseCount( $arrmixValues['active_lease_count'] );
		}

		if( isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
	}

	public function getActiveDocumentCount() {
		return $this->m_intActiveDocumentCount;
	}

	public function getActiveLeaseCount() {
		return $this->m_intActiveLeaseCount;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function setActiveDocumentCount( $intActiveDocumentCount ) {
		$this->m_intActiveDocumentCount = ( int ) $intActiveDocumentCount;
	}

	public function setActiveLeaseCount( $intActiveLeaseCount ) {
		$this->m_intActiveLeaseCount = ( int ) $intActiveLeaseCount;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = ( int ) $intPropertyId;
	}

	public function getExceptionDelinquencyPolicies() {
		return $this->m_arrobjExceptionDelinquencyPolicies;
	}

	public function setExceptionDelinquencyPolicies( $arrobjExceptionDelinquencyPolicies ) {
		$this->m_arrobjExceptionDelinquencyPolicies = $arrobjExceptionDelinquencyPolicies;
	}

}
?>