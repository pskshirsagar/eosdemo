<?php

class CCustomerContact extends CBaseCustomerContact {

	private $m_strReference;
	private $m_intPhoneNumberType;
	private $m_intCommercialClauseId;

	protected $m_objI18nPhoneNumber;
	protected $m_objI18nMobileNumber;
	protected $m_objI18nRepresentativePhoneNumber;
	protected $m_objI18nOfficePhoneNumber;

	protected $m_intLeaseId;
	protected $m_intLeaseCustomerId;
	protected $m_intPropertyId;
	protected $m_strPreferredLocaleCode;

	public function getReference() {
		return $this->m_strReference;
	}

	public function getPhoneNumberType() {
		return $this->m_intPhoneNumberType;
	}

	public function getCommercialClauseId() {
		return $this->m_intCommercialClauseId;
	}

	public function getI18nPhoneNumber() {
		return $this->m_objI18nPhoneNumber;
	}

	public function getI18nMobileNumber() {
		return $this->m_objI18nMobileNumber;
	}

	public function getI18nRepresentativePhoneNumber() {
		return $this->m_objI18nRepresentativePhoneNumber;
	}

	public function getI18nOfficePhoneNumber() {
		return $this->m_objI18nOfficePhoneNumber;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPreferredLocaleCode() {
		return $this->m_strPreferredLocaleCode;
	}

	public function setReference( $strReference ) {
		$this->m_strReference = $strReference;
	}

	public function setPhoneNumberType( $intPhoneNumberType ) {
		$this->m_intPhoneNumberType = $intPhoneNumberType;
	}

	public function setCommercialClauseId( $intCommercialClauseId ) {
		$this->m_intCommercialClauseId = $intCommercialClauseId;
	}

	public function setI18nPhoneNumber( $objI18nPhoneNumber ) {
		$this->m_objI18nPhoneNumber = $objI18nPhoneNumber;
	}

	public function setI18nMobileNumber( $objI18nMobileNumber ) {
		$this->m_objI18nMobileNumber = $objI18nMobileNumber;
	}

	public function setI18nRepresentativePhoneNumber( $objI18nRepresentativePhoneNumber ) {
		$this->m_objI18nRepresentativePhoneNumber = $objI18nRepresentativePhoneNumber;
	}

	public function setI18nOfficePhoneNumber( $objI18nOfficePhoneNumber ) {
		$this->m_objI18nOfficePhoneNumber = $objI18nOfficePhoneNumber;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPreferredLocaleCode( $strPreferredLocaleCode ) {
		$this->m_strPreferredLocaleCode = $strPreferredLocaleCode;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['reference'] ) ) $this->setReference( $arrmixValues['reference'] );
		if( true == isset( $arrmixValues['commercial_clause_id'] ) ) $this->setCommercialClauseId( $arrmixValues['commercial_clause_id'] );
		if( true == isset( $arrmixValues['phone_number_type'] ) ) $this->setPhoneNumberType( $arrmixValues['phone_number_type'] );
		if( true == isset( $arrmixValues['lease_id'] ) ) {
			$this->setLeaseId( $arrmixValues['lease_id'] );
		}
		if( true == isset( $arrmixValues['lease_customer_id'] ) ) {
			$this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['preferred_locale_code'] ) ) {
			$this->setPreferredLocaleCode( $arrmixValues['preferred_locale_code'] );
		}
	}

	public function bindDetails( $arrmixDetails ) {
		$arrmixDetails = ( object ) $arrmixDetails;
		$this->setDetails( $arrmixDetails );
	}

    /**
     * Validation Functions
     */

	public function valId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intId ) || 0 >= ( int ) $this->m_intId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Contact Request:  Id required - CCustomerContact::valId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 0 >= ( int ) $this->m_intCid ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Contact Request:  Management Id required - CCustomerContact::valCid()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Contact Request:  Customer Id required - CCustomerContact::valCustomerId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valContactTypeId() {
		$boolIsValid = true;

		if( true == isset( $this->m_intContactTypeId ) && 0 >= $this->m_intContactTypeId ) {
			$boolIsValid = false;
			trigger_error( 'Invalid Customer Contact Request: Contact Type Id invalid - CCustomerContact::valContactTypeId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valNameFirst( $boolAddErrorMessage = true, $boolIsLeadRepresentative = false ) {

		$boolIsValid = true;

		$strErrorMessage = '';
		if( true == $boolIsLeadRepresentative ) {
			$strErrorMessage = __( 'Representative' );
		}
		if( true == is_null( $this->m_strNameFirst ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( '{%s, 0}  First name is required.', [ $strErrorMessage ] ) ) );
			}

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'Names cannot contain @ signs or email headers.' ) ) );

		} else {
			if( 1 > strlen( $this->m_strNameFirst ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( '{%s, 0}  First name must have at least one letter.', [ $strErrorMessage ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameLast( $boolAddErrorMessage = true, $boolIsLeadRepresentative = false ) {

		$boolIsValid = true;

		$strErrorMessage = '';
		if( true == $boolIsLeadRepresentative ) {
			$strErrorMessage = __( 'Representative' );
		}
		if( true == is_null( $this->m_strNameLast ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( '{%s, 0} Last name is required.', [ $strErrorMessage ] ) ) );
			}

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Names cannot contain @ signs or email headers.' ) ) );

		}

		return $boolIsValid;
	}

	public function valEmailAddress( $boolCanBeNull = false, $boolIsRp = false, $boolIsLeadRepresentative = false ) {

		$boolIsValid = true;
		$strErrorMessage = '';
		if( true == $boolIsLeadRepresentative ) {
			$strErrorMessage = __( 'Representative' );
		}
		if( true == $boolIsRp && true == is_null( $this->m_strEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( '{%s, 0} Email address is required.', [ $strErrorMessage ] ), 504 ) );
		}

		if( false == $boolCanBeNull ) {
			if( false == is_null( $this->m_strEmailAddress ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Please enter a valid {%s, 0} email address.', [ $strErrorMessage ] ) ) );
			}
		}
		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber( $boolIsRP = false, $boolIsLeadRepresentative = false, $boolIsPhoneNumReq = true ) {

		$objI18nPhoneNumber = $this->getI18nPhoneNumber();

		if( true == valObj( $objI18nPhoneNumber, 'i18n\CPhoneNumber' ) ) {
			if( false == $objI18nPhoneNumber->isValid( $boolIsPhoneNumReq ) ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nPhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nPhoneNumber->getFormattedErrors() ] ) ) );
				return false;
			}
		} else {
			$intPrimaryNumberLength	= \Psi\CStringService::singleton()->strlen( $this->m_strPhoneNumber );

			$strErrorMessage = __( 'Primary phone' );
			if( true == $boolIsLeadRepresentative ) {
				$strErrorMessage = __( 'Representative phone' );
			}
			if( true == $boolIsRP && 0 == $intPrimaryNumberLength ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( '{%s,0} number is required.', [ $strErrorMessage ] ), '' ) );
				return false;
			}

			if( true == valStr( $this->m_strPhoneNumber ) ) {
				if( 0 < $intPrimaryNumberLength && ( 10 > $intPrimaryNumberLength || 15 < $intPrimaryNumberLength ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( '{%s,0} number must be between 10 and 15 characters.', [ $strErrorMessage ] ), '' ) );
					return false;
				}
				if( 1 != $this->checkPhoneNumberFullValidation( $this->m_strPhoneNumber ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Enter valid {%s,0} number.', [ $strErrorMessage ] ), 504 ) );
					return false;
				}
			}
		}

		return true;
	}

	public function valMobileNumber( $boolIsRequired = false ) {

		$objI18nMobileNumber = $this->getI18nMobileNumber();
		if( true == valObj( $objI18nMobileNumber, 'i18n\CPhoneNumber' ) ) {

			if( false == $objI18nMobileNumber->isValid( $boolIsRequired ) ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, NULL, __( 'Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nMobileNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nMobileNumber->getCountryCode(), 'formatted_errors' => $objI18nMobileNumber->getFormattedErrors() ] ) ) );
				return false;
			}
		} else {
			$intSecondaryNumberLength = \Psi\CStringService::singleton()->strlen( $this->m_strMobileNumber );
			if( true == valStr( $this->m_strMobileNumber ) ) {
				if( 0 < $intSecondaryNumberLength && ( 10 > $intSecondaryNumberLength || 15 < $intSecondaryNumberLength ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Secondary phone number must be between 10 and 15 characters.' ), '' ) );
					return false;
				}
				if( 1 != $this->checkPhoneNumberFullValidation( $this->m_strMobileNumber ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Enter valid secondary phone number.' ), 504 ) );
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Such validations should be done using CPhoneNumber::isValid() and in controller / library / outside-EOS.
	 *            Such format validation shouldn't be the part of EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function checkPhoneNumberFullValidation( $strNumber ) {
		if( true == preg_match( '/\-{3,}/', $strNumber ) || 1 !== preg_match( '/^[\(]{0,1}(\d{1,3})[\)]?[\-)]?[\s]{0,}(\d{3})[\s]?[\-]?(\d{4})[\s]?[x]?[\s]?(\d*)$/', $strNumber ) ) {
			return 'Phone number not in valid format.';
		}

		return 1;
	}

	public function valEmailAddressOrPrimaryNumber( $arrobjPropertyApplicationPreferences = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( CCustomerContactType::LANDLORD_REFERENCE == $this->getCustomerContactTypeId() && true == valArr( $arrobjPropertyApplicationPreferences ) ) {

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->m_strPhoneNumber ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Primary number is required for current address.' ), 504, [ 'label' => 'Current Address', 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}

			}

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->m_strEmailAddress ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Email address is required for current address.' ), 504, [ 'label' => 'Current Address', 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
			}

		} elseif( CCustomerContactType::TYPE_2ND_LANDLORD_REFERENCE == $this->getCustomerContactTypeId() && true == valArr( $arrobjPropertyApplicationPreferences ) ) {

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->m_strPhoneNumber ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Primary number is required for previous address.' ), 504, [ 'label' => 'Previous Address', 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
			}

			if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->m_strEmailAddress ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Email address is required for previous address.' ), 504, [ 'label' => 'Previous Address', 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
			}

		} elseif( CCustomerContactType::LANDLORD_REFERENCE != $this->getCustomerContactTypeId() && CCustomerContactType::TYPE_2ND_LANDLORD_REFERENCE != $this->getCustomerContactTypeId() ) {
			if( true == is_null( $this->m_strEmailAddress ) && true == is_null( $this->m_strPhoneNumber ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Email address or Primary number is required.' ), 504, [ 'label' => 'Previous Address', 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
			}
		}
		return $boolIsValid;
	}

	public function valPostalCode( $boolIsEmergencyContact = false ) {

		$boolIsValid = true;

		if( true == is_null( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Zip code is required.' ), 504 ) );
		}

		if( false == $boolIsEmergencyContact && false == valId( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Zip Code is not valid.' ), 504 ) );
		}

		if( true == $boolIsValid && false == CValidation::validatePostalCode( $this->getPostalCode(), ( true == $boolIsEmergencyContact ) ? NULL : CCountry::CODE_USA ) ) {
			$boolIsValid = false;
			if( true == $boolIsEmergencyContact ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Zip code must contain numbers and letters only.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Zip code must be 5 digit long.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valStreetLine1( $boolIsRp = false ) {
		$boolIsValid = true;

		if( true == $boolIsRp && true == is_null( $this->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Address Line 1 is required.' ), 504 ) );
		}

		return $boolIsValid;
	}

	public function valStateCode( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State is required.' ), 504 ) );
		} elseif( false == is_null( $objDatabase ) && false == valObj( \Psi\Eos\Entrata\CStates::createService()->fetchStateByCode( $this->getStateCode(), $objDatabase ), 'CState' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'Please select valid state.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCity( $boolIsTenantContactCity = false ) {
		$boolIsValid = true;

		if( false == $boolIsTenantContactCity && true == is_null( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City is required.' ), 504 ) );
		}

//		 ToDo: Removing this because its not storing Hindi input.
//		if( false == is_null( $this->getCity() ) ) {
//			if( true == preg_match( '/[^a-zA-Z\s]/', $this->getCity() ) ) {
//				$boolIsValid = false;
//				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'City can only contain characters.' ) ) );
//			}
//		}

		return $boolIsValid;
	}

	public function valAge() {
		$boolIsValid = true;

		if( true == is_null( $this->getAge() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'age', __( 'Age is required.' ), 504 ) );
		}

		if( false == valId( $this->getAge() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'age', __( 'Age is not valid.' ), 504 ) );
		} else {
			if( false == preg_match( '/^[0-9]{1,2}$/', $this->getAge() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'age', __( 'Age must be between 1 to 2 digit.' ), 504 ) );
			}
		}

		return $boolIsValid;
	}

	public function valRelationship( $boolIsRp = false, $boolIsLeadRepresentative = false ) {
		$boolIsValid = true;

		$strErrorMessage = '';
		if( true == $boolIsLeadRepresentative ) {
			$strErrorMessage = __( 'Representative ' );
		}
		if( true == $boolIsRp && true == is_null( $this->m_strRelationship ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship', __( '{%s, 0} relationship is required.', [ $strErrorMessage ] ), 504 ) );
		}

		 return $boolIsValid;
	}

	public function valRepresentativeMobileNumber( $boolIsRP = false ) {

		$objI18nMobileNumber 	= $this->getI18nMobileNumber();
		if( true == valObj( $objI18nMobileNumber, 'i18n\CPhoneNumber' ) ) {
			if( 0 == strlen( $objI18nMobileNumber->getNumber() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Representative mobile number is required.' ), 504 ) );
				return false;
			}
			if( true == valStr( $objI18nMobileNumber->getNumber() ) && false == $objI18nMobileNumber->isValid() ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Representative Mobile number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nMobileNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nMobileNumber->getCountryCode(), 'formatted_errors' => $objI18nMobileNumber->getFormattedErrors() ] ) ) );
				return false;
			}
		} else {
			$intSecondaryNumberLength = \Psi\CStringService::singleton()->strlen( $this->m_strMobileNumber );
			if( 0 < $intSecondaryNumberLength && ( 10 > $intSecondaryNumberLength || 15 < $intSecondaryNumberLength ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Representative Mobile Number must be between 10 and 15 characters.' ), '' ) );
				return false;
			}
			if( true == $boolIsRP && false == valStr( $this->m_strMobileNumber ) ) {
				if( 1 != $this->checkPhoneNumberFullValidation( $this->m_strMobileNumber ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mobile_number', __( 'Representative mobile number is required.' ), 504 ) );
					return false;
				}
			}
		}
		return true;
	}

	public function valOfficePhoneNumber( $boolIsRP = false, $boolIsLeadRepresentative = false ) {

		$strErrorMessage = '';
		if( true == $boolIsLeadRepresentative ) {
			$strErrorMessage = __( 'Representative' );
		}

		$strOfficePhoneNumber       = $this->getOfficePhoneNumber();
		$objI18nOfficePhoneNumber   = $this->getI18nOfficePhoneNumber();

		if( true == valObj( $objI18nOfficePhoneNumber, 'i18n\CPhoneNumber' ) ) {
			if( 0 == strlen( $objI18nOfficePhoneNumber->getNumber() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_number', __( 'Representative office number is required.' ), 504 ) );
				return false;
			}
			if( true == valStr( $objI18nOfficePhoneNumber->getNumber() ) && false == $objI18nOfficePhoneNumber->isValid() ) {
				$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'office_number', __( 'Representative office number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nOfficePhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nOfficePhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nOfficePhoneNumber->getFormattedErrors() ] ) ) );
				return false;
			}
		} else {
			$intOfficePhoneNumberLength = \Psi\CStringService::singleton()->strlen( $strOfficePhoneNumber );
			if( 0 < $intOfficePhoneNumberLength && ( 10 > $intOfficePhoneNumberLength || 15 < $intOfficePhoneNumberLength ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_number', __( '{%s, 0} office number must be between 10 and 15 characters.', [ $strErrorMessage ] ), '' ) );

				return false;
			}
			if( true == $boolIsRP && false == valStr( $strOfficePhoneNumber ) ) {
				if( 1 != $this->checkPhoneNumberFullValidation( $strOfficePhoneNumber ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_number', __( '{%s, 0} office number is required.', [ $strErrorMessage ] ), 504 ) );

					return false;
				}
			}
		}
		return true;
	}

	public function validate( $strAction, $boolIsRP = false, $arrobjPropertyApplicationPreferences = NULL, $boolIsValidateRequired = true, $boolIsEmergencyContact = false, $objDatabase = NULL, $arrobjPropertyPreferences = NULL, $boolIsPhoneNumReq = true ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valContactTypeId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber( $boolIsRP, false, $boolIsPhoneNumReq );
				$boolIsValid &= $this->valMobileNumber();
				$boolIsValid &= $this->valRelationship( $boolIsRP );
				$boolIsValid &= $this->valEmailAddress( false, $boolIsRP );
				$boolIsValid &= $this->valStreetLine1( $boolIsRP );

				if( true == $boolIsRP ) {
					$boolIsValid &= $this->valAge();
					$boolIsValid &= $this->valPostalCode( $boolIsEmergencyContact );
					$boolIsValid &= $this->valStreetLine1();
					$boolIsValid &= $this->valStateCode( $objDatabase );
					$boolIsValid &= $this->valCity();
				} else {
					$boolIsValid &= $this->valEmailAddressOrPrimaryNumber( $arrobjPropertyApplicationPreferences, $boolIsValidateRequired );
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'validate_commercial_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valContactTypeId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valStreetLine1( $boolIsRP = false );
				$boolIsValid &= $this->valEmailAddress( false, $boolIsRP = false );
				$boolIsValid &= $this->valCity( $boolIsTenantContactCity = true );
				break;

			case 'validate_lead_representative':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valContactTypeId();
				$boolIsValid &= $this->valNameFirst( $boolAddErrorMessage = true, $boolIsLeadRepresentative = true );
				$boolIsValid &= $this->valNameLast( $boolAddErrorMessage = true, $boolIsLeadRepresentative = true );
				$boolIsValid &= $this->valRelationship( $boolIsRP, $boolIsLeadRepresentative = true );

				if( true == valObj( $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL'], 'CPropertyPreference' ) ) {
					$boolIsValid &= $this->valPhoneNumberAndEmail( $boolIsRP, $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue(), $boolIsLeadRepresentative = true );
				}
				break;

			case 'validate_lead_customer_contact':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valContactTypeId();
				$boolIsValid &= $this->valNameFirst( $boolAddErrorMessage = true, $boolIsLeadRepresentative = false );
				$boolIsValid &= $this->valNameLast( $boolAddErrorMessage = true, $boolIsLeadRepresentative = false );
				$boolIsValid &= $this->valRelationship( $boolIsRP, $boolIsLeadRepresentative = false );

				if( true == valObj( $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL'], 'CPropertyPreference' ) ) {
					$boolIsValid &= $this->valPhoneNumberAndEmail( $boolIsRP, $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue(), $boolIsLeadRepresentative = false );
				}
				break;

			case 'validate_senior_representative':
				$boolIsValid &= $this->valRelationship( $boolIsRP, $boolIsLeadRepresentative = true );
				$boolIsValid &= $this->valNameFirst( $boolAddErrorMessage = true, $boolIsLeadRepresentative = true );
				$boolIsValid &= $this->valNameLast( $boolAddErrorMessage = true, $boolIsLeadRepresentative = true );
				$boolIsValid &= $this->valEmailAddress( false, $boolAddErrorMessage = true, $boolIsLeadRepresentative = true );
				break;

			case 'validate_occupant_contact':
			case 'validate_group_contact':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valContactTypeId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber( false, false, false );
				$boolIsValid &= $this->valEmailAddress( false );
				break;

			case 'validate_commercial_contact';
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valContactTypeId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber( false, false, false );
				$boolIsValid &= $this->valEmailAddress( false, true );
				break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function getRepresentativePhoneNumber() {
		if( CPhoneNumberType::HOME == $this->getPhoneNumberType() ) {
			return $this->getPhoneNumber();
		} else if( CPhoneNumberType::MOBILE == $this->getPhoneNumberType() ) {
			return $this->getMobileNumber();
		} else if( CPhoneNumberType::OFFICE == $this->getPhoneNumberType() ) {
			if( false == is_null( $this->getOfficePhoneNumber() ) ) {
				return $this->getOfficePhoneNumber();
			}
			return NULL;
		}
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
    public function valPhoneNumberAndEmail( $boolIsRP, $strPropertySettingValue, $boolIsLeadRepresentative = false ) {
		$boolIsValid = true;
		if( false == valStr( $strPropertySettingValue ) ) {
			return $boolIsValid;
		}
		$strErrorMessage = '';
		$strRepresentative = '';
		if( true == $boolIsLeadRepresentative ) {
			$strRepresentative = __( 'Representative' );
		}

		if( CPhoneNumberType::MOBILE == $this->getPhoneNumberType() ) {
			$strErrorMessage = __( 'Mobile' );
		} else if( CPhoneNumberType::OFFICE == $this->getPhoneNumberType() ) {
			$strErrorMessage = __( 'Office' );
		} else {
			$strErrorMessage = __( 'Phone' );
		}

		$strErrorMessage = $strRepresentative . ' ' . $strErrorMessage;

		if( 'PHONE_ONLY' == $strPropertySettingValue ) {
			if( CPhoneNumberType::HOME == $this->getPhoneNumberType() ) {
				$boolIsValid &= $this->valPhoneNumber( $boolIsRP, $boolIsLeadRepresentative );
			} else if( CPhoneNumberType::MOBILE == $this->getPhoneNumberType() ) {
				$boolIsValid &= $this->valRepresentativeMobileNumber( $boolIsRP );
			} else if( CPhoneNumberType::OFFICE == $this->getPhoneNumberType() ) {
				$boolIsValid &= $this->valOfficePhoneNumber( $boolIsRP, $boolIsLeadRepresentative );
			}
		}

		$boolIsPhoneValid = $boolIsEmailValid = true;

		if( true == $boolIsPhoneValid && 'PHONE_ONLY' != $strPropertySettingValue ) {
			$objI18nRepresentativePhoneNumber 	= $this->getI18nRepresentativePhoneNumber();
			if( true == valObj( $objI18nRepresentativePhoneNumber, 'i18n\CPhoneNumber' ) ) {
				if( 0 == strlen( $objI18nRepresentativePhoneNumber->getNumber() ) ) {
					$boolIsPhoneValid = false;
				}
				if( true == valStr( $objI18nRepresentativePhoneNumber->getNumber() ) && false == $objI18nRepresentativePhoneNumber->isValid() ) {
					$this->addErrorMsg( \Psi\Libraries\UtilErrorMsg\CErrorMsg::create( \Psi\Libraries\UtilErrorMsg\CErrorMsg::ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Representative Phone number is not valid for {%s, region} (+{%s, country_code}). {%s, formatted_errors}', [ 'region' => $objI18nRepresentativePhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objI18nRepresentativePhoneNumber->getCountryCode(), 'formatted_errors' => $objI18nRepresentativePhoneNumber->getFormattedErrors() ] ) ) );
					return false;
				}
			} else {
				$strPhoneNumber 	= $this->getRepresentativePhoneNumber();
				if( 0 == strlen( trim( $strPhoneNumber ) ) ) {
					$boolIsPhoneValid = false;
				}
				if( 0 < strlen( $strPhoneNumber ) && ( 10 > \Psi\CStringService::singleton()->strlen( $strPhoneNumber ) || 15 < \Psi\CStringService::singleton()->strlen( $strPhoneNumber ) ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( '{%s, 0} number must be between 10 and 15 characters.', [ $strErrorMessage ] ) ) );

					return false;

				} else {
					if( true == isset( $strPhoneNumber ) && false == ( CValidation::validateFullPhoneNumber( $strPhoneNumber, $boolFormat = false ) ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Valid {%s, 0} number is required.', [ $strErrorMessage ] ) ) );

						return false;
					}
				}
			}
		}
		if( 0 == strlen( trim( $this->getEmailAddress() ) ) ) {
			$boolIsEmailValid = false;
		}
		if( true == $boolIsEmailValid && false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( '{%s, 0} Email address does not appear to be valid.', [ $strErrorMessage ] ) ) );
			return false;
		}
		if( 'PHONE_ONLY' == $strPropertySettingValue ) {
			return $boolIsValid;
		}
		if( ( ( true == $boolIsPhoneValid || true == $boolIsEmailValid ) && 'PHONE_OR_EMAIL' == $strPropertySettingValue ) ||
			( true == $boolIsPhoneValid && true == $boolIsEmailValid && 'PHONE_AND_EMAIL' == $strPropertySettingValue ) ) {
			return true;
		}
		$strValidationMessage = __( '{%s, 0} number and {%s, 1} email address are required.', [ $strErrorMessage, $strRepresentative ] );

		if( 'PHONE_OR_EMAIL' == $strPropertySettingValue ) {
			$strValidationMessage = __( 'Either of {%s, 0} number or {%s, 1} email address is required.', [ $strErrorMessage, $strRepresentative ] );
		}
		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_or_email_address', $strValidationMessage ) );
		return false;
	}

    /**
     * Other Functions
     *
     */

    public function mapApplicantContact( $objApplicantContact ) {
    	$this->setCustomerContactTypeId( $objApplicantContact->getCustomerContactTypeId() );
    	$this->setNameFirst( $objApplicantContact->getNameFirst() );
    	$this->setNameLast( $objApplicantContact->getNameLast() );
    	$this->setPhoneNumber( $objApplicantContact->getPhoneNumber() );
    	$this->setMobileNumber( $objApplicantContact->getMobileNumber() );
    	$this->setFaxNumber( $objApplicantContact->getFaxNumber() );
    	$this->setEmailAddress( $objApplicantContact->getEmailAddress() );
    	$this->setRelationship( $objApplicantContact->getRelationship() );
    	$this->setStreetLine1( $objApplicantContact->getStreetLine1() );
    	$this->setStreetLine2( $objApplicantContact->getStreetLine2() );
    	$this->setStreetLine3( $objApplicantContact->getStreetLine3() );
    	$this->setCity( $objApplicantContact->getCity() );
    	$this->setStateCode( $objApplicantContact->getStateCode() );
    	$this->setProvince( $objApplicantContact->getProvince() );
    	$this->setPostalCode( $objApplicantContact->getPostalCode() );
    	$this->setCountryCode( $objApplicantContact->getCountryCode() );
    	$this->setAge( $objApplicantContact->getAge() );
    	$this->setHasAccessToUnit( $objApplicantContact->getHasAccessToUnit() );

    	return true;
    }

    public function createApplicantContact( $objApplicant ) {

    	$objApplicantContact = new CApplicantContact();
    	$objApplicantContact->setCid( $this->getCid() );
    	$objApplicantContact->setApplicantId( $objApplicant->getId() );

    	return $objApplicantContact;
    }

}
?>