<?php

class CSubsidyHapRequestStatusType extends CBaseSubsidyHapRequestStatusType {

	const OPEN			        = 1;
	const FINALIZED		        = 2;
	const SUBMITTED		        = 3;
	const UNHANDLED_EXCEPTIONS	= 4;
	const ACKNOWLEDGED			= 5;
	const APPROVED		        = 6;
	const ALLOCATED		        = 7;

	public static $c_arrstrAllSubsidyHapRequestStatusTypes = [
		self::OPEN 		=> 'Open',
		self::FINALIZED => 'Finalized',
		self::SUBMITTED => 'Submitted',
		self::UNHANDLED_EXCEPTIONS => 'Unhandled Exceptions',
		self::ACKNOWLEDGED => 'Acknowledged',
		self::APPROVED 	=> 'Approved',
		self::ALLOCATED => 'Allocated'
	];

	public static function getSubsidyHapRequestStatusTypes() : array {
		return [
			self::OPEN                 => __( 'Open' ),
			self::FINALIZED            => __( 'Finalized' ),
			self::SUBMITTED            => __( 'Submitted' ),
			self::UNHANDLED_EXCEPTIONS => __( 'Unhandled Exception' ),
			self::ACKNOWLEDGED         => __( 'Acknowledged' ),
			self::APPROVED             => __( 'Approved' ),
			self::ALLOCATED            => __( 'Allocated' )
		];
	}

	public static $c_arrstrFilterSubsidyHapRequestStatusTypes = [
		self::OPEN,
		self::FINALIZED,
		self::SUBMITTED,
		self::UNHANDLED_EXCEPTIONS,
		self::ACKNOWLEDGED,
		self::APPROVED
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
