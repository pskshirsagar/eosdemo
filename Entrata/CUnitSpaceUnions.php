<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpaceUnions
 * Do not add any new functions to this class.
 */

class CUnitSpaceUnions extends CBaseUnitSpaceUnions {

	public static function fetchExistingUnitSpaceUnionsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintLeaseTermIds = [], $arrintLeaseStartWindowIds = [] ) {

		$strWhereSql = ( true == valArr( $arrintLeaseStartWindowIds ) ) ? ' AND lease_start_window_id IN ( ' . implode( ', ', $arrintLeaseStartWindowIds ) . ' ) ' : ' AND lease_start_window_id IS NULL ';
		$strWhereSql .= ( false == valArr( $arrintLeaseTermIds ) ) ? ' AND lease_term_id IS NULL ' : ' AND lease_term_id IN (  ' . implode( ', ', $arrintLeaseTermIds ) . ' )';

		$strSql = ' SELECT
						*
					FROM
						unit_space_unions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . $strWhereSql;

		return parent::fetchUnitSpaceUnions( $strSql, $objDatabase );
	}

	public static function fetchExistingUnitSpaceIdsByGroupUnitSpaceIdByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $strGroupUnitSpaceIds, $arrintLeaseTermIds, $intPropertyId, $intCid, $objDatabase, $intLeaseStartWindowId = NULL ) {

		if( false == valStr( $strGroupUnitSpaceIds ) || false == valArr( $arrintLeaseTermIds ) ) return NULL;

		$strSqlWhereCondition = ( ( true == valId( $intLeaseStartWindowId ) ) ? ' AND li.lease_start_window_id = ' . $intLeaseStartWindowId . ' ' : '' );

		$strSql = ' SELECT
						usu.group_unit_space_id,
						lus.lease_id,
						usu.unit_space_id
					FROM
						unit_space_unions usu
						JOIN lease_unit_spaces lus ON ( usu.cid = lus.cid AND usu.group_unit_space_id = lus.unit_space_id AND usu.property_id = lus.property_id )
                        JOIN lease_intervals li ON ( lus.lease_id = li.lease_id AND lus.cid = li.cid AND lus.property_id = li.property_id AND li.lease_term_id = usu.lease_term_id AND li.lease_start_window_id = usu.lease_start_window_id )
				 	WHERE
						usu.cid = ' . ( int ) $intCid . '
					 	AND usu.property_id = ' . ( int ) $intPropertyId . '
					 	AND usu.group_unit_space_id IN ( ' . $strGroupUnitSpaceIds . ' )
					 	AND li.lease_term_id IN ( ' . implode( ', ', $arrintLeaseTermIds ) . ' ) ' .
		                $strSqlWhereCondition . '
					 	AND lus.deleted_by IS NULL
					 	AND lus.deleted_on IS NULL
					 	AND li.lease_status_type_id IN ( ' . CLeaseStatusType::CURRENT . ', ' . CLeaseStatusType::NOTICE . ' )    
					 GROUP BY
					 	usu.group_unit_space_id,
					 	lus.lease_id,
						usu.unit_space_id ORDER BY lease_id DESC';

		$arrmixUnitSpaceUnionsDetails = fetchData( $strSql, $objDatabase );

		$arrmixUnitSpaceUnionsDetails = rekeyArray( 'lease_id', $arrmixUnitSpaceUnionsDetails, false, true );

		foreach( $arrmixUnitSpaceUnionsDetails as $intLeaseId => $arrmixTempUnitSpaceUnions ) {

			foreach( $arrmixTempUnitSpaceUnions as $arrmixUnitSpaceUnion ) {
				$arrintGroupUnitSpaceIds[$intLeaseId] = $arrmixUnitSpaceUnion['group_unit_space_id'];

			}

			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixUnitSpaceUnionsDetails[$intLeaseId] ) ) {
				return $arrintGroupUnitSpaceIds;
			}
		}
	}

	public static function fetchUnitSpaceUnionsByBulkUnitAssignmentFilterByCid( $objBulkUnitAssignmentFilter, $intCid, $objDatabase ) {

		if( false == valObj( $objBulkUnitAssignmentFilter, 'CBulkUnitAssignmentFilter' ) || false == valId( $objBulkUnitAssignmentFilter->getLeaseTermId() ) || false == valId( $objBulkUnitAssignmentFilter->getLeaseStartWindowId() ) ) return NULL;

		$strSql = ' SELECT
						usu.group_unit_space_id,
						usu.unit_space_id,
						us.space_number
					FROM
						unit_space_unions usu
						LEFT JOIN unit_spaces us ON( usu.cid = us.cid AND usu.property_id = us.property_id AND usu.unit_space_id = us.id )
					WHERE
						usu.cid = ' . ( int ) $intCid . '
						AND usu.property_id = ' . ( int ) $objBulkUnitAssignmentFilter->getPropertyId() . '
						AND usu.lease_term_id = ' . ( int ) $objBulkUnitAssignmentFilter->getLeaseTermId() . '
						AND usu.lease_start_window_id = ' . ( int ) $objBulkUnitAssignmentFilter->getLeaseStartWindowId() . '
						AND us.deleted_by IS NULL
						AND us.deleted_on IS NULL
					 ORDER BY
	 					usu.unit_space_id';

		$arrmixUnitSpaceUnionsData = fetchData( $strSql, $objDatabase );

		$arrmixData = [];

		if( true == valArr( $arrmixUnitSpaceUnionsData ) ) {

			foreach( $arrmixUnitSpaceUnionsData as $arrmixTempData ) {

				$arrmixData[$arrmixTempData['group_unit_space_id']][] = $arrmixTempData['unit_space_id'];
				if( true == valStr( $arrmixData[$arrmixTempData['group_unit_space_id']]['space_number'] ) ) {
					$arrmixData[$arrmixTempData['group_unit_space_id']]['space_number'] = $arrmixData[$arrmixTempData['group_unit_space_id']]['space_number'] . ',' . \Psi\CStringService::singleton()->strtoupper( $arrmixTempData['space_number'] );
				} else {
					$arrmixData[$arrmixTempData['group_unit_space_id']]['space_number'] = \Psi\CStringService::singleton()->strtoupper( $arrmixTempData['space_number'] );
				}
			}
		}

		return $arrmixData;
	}

	public static function fetchUnitSpaceIdsByUnitSpaceIdsByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $arrintUnitSpaceIds, $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintUnitSpaceIds ) ) return false;

		$strSql = ' SELECT
						CASE WHEN usu.unit_space_id IS NOT NULL THEN usu.unit_space_id ELSE us.id END as unit_space_id
					FROM
						unit_spaces us
						LEFT JOIN unit_space_unions usu ON( usu.cid = us.cid AND usu.property_id = us.property_id AND usu.group_unit_space_id = us.id AND lease_term_id = ' . ( int ) $intLeaseTermId . ' AND lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . ')
					WHERE
						us.cid = ' . ( int ) $intCid . '
						AND us.property_id = ' . ( int ) $intPropertyId . '
						AND us.id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
					ORDER BY
 	                    CASE WHEN usu.unit_space_id IS NOT NULL THEN usu.unit_space_id ELSE us.id END';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMergedUnitSpacesCountByGroupUnitSpaceIdsByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $arrintGroupUnitSpaceIds, $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintGroupUnitSpaceIds ) || false == valId( $intLeaseTermId ) || false == valId( $intLeaseStartWindowId ) ) return false;

		$strSql = 'SELECT
						count ( usu.unit_space_id ),
						usu.group_unit_space_id
					FROM
						unit_space_unions usu
					WHERE
						usu.cid = ' . ( int ) $intCid . '
						AND usu.property_id = ' . ( int ) $intPropertyId . '
						AND usu.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND usu.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
						AND usu.group_unit_space_id IN( ' . implode( ', ', $arrintGroupUnitSpaceIds ) . ' ) 
						AND usu.unit_space_id <> usu.group_unit_space_id
					GROUP BY
						usu.group_unit_space_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSpaceConfigurationIdByUnitSpaceIdByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $intUnitSpaceId, $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intUnitSpaceId ) || false == valId( $intLeaseTermId ) || false == valId( $intLeaseStartWindowId ) ) return NULL;

		$strSql = ' SELECT
						space_configuration_id
					FROM
						unit_space_unions usu
					WHERE
						usu.cid = ' . ( int ) $intCid . '
						AND usu.property_id = ' . ( int ) $intPropertyId . '
						AND usu.unit_space_id = ' . ( int ) $intUnitSpaceId . '
						AND usu.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND usu.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId;

		return self::fetchColumn( $strSql, 'space_configuration_id', $objDatabase );
	}

	public static function fetchUnitSpaceUnionsByGroupUnitSpaceIdsByLeaseTermIdsByLeaseStartWindowIdsByPropertyIdByCid( $strLeaseUnitSpaces, $arrintLeaseTermIds, $arrintLeaseStartWindowIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valStr( $strLeaseUnitSpaces ) || false == valArr( $arrintLeaseTermIds ) || false == valArr( $arrintLeaseStartWindowIds ) ) return NULL;

		$strSql = '	SELECT
						array_to_string( array_agg( usu.unit_space_id ), \',\' ) AS unit_space_ids,
						lease_term_id,
						lease_start_window_id,
						group_unit_space_id,
						space_configuration_id
					FROM
						unit_space_unions usu
					WHERE
						usu.cid = ' . ( int ) $intCid . '
						AND usu.property_id = ' . ( int ) $intPropertyId . '
						AND usu.group_unit_space_id IN ( ' . ( string ) $strLeaseUnitSpaces . ' )
						AND usu.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' )
						AND usu.lease_start_window_id IN ( ' . implode( ',', $arrintLeaseStartWindowIds ) . ' )
					GROUP BY
						group_unit_space_id,
						lease_term_id,
						lease_start_window_id,
						space_configuration_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSpaceConfigurationDataByPropertyUnitIdByLeaseTermIdByPropertyIdByCid( $intPropertyUnitId, $intLeaseTermId, $intPropertyId, $intCid, $objDatabase, $intLeaseStartWindowId = NULL, $intPropertyTypeId = NULL ) {

		if( false == valId( $intPropertyUnitId ) ) return NULL;
		$strLeaseStartWindowCondition = '';
		if( false == valId( $intPropertyTypeId ) || CPropertyType::MILITARY != $intPropertyTypeId ) {
			if( false == valId( $intLeaseStartWindowId ) || false == valId( $intLeaseTermId ) ) return NULL;
			$strLeaseStartWindowCondition = ' AND usu.lease_term_id = ' . ( int ) $intLeaseTermId . ' AND usu.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId;
		}
		$strSql = ' SELECT
						DISTINCT ( usu.space_configuration_id ),
						sc.name,
						sc.unit_space_count
					FROM
						unit_space_unions usu
						JOIN unit_spaces us ON ( us.id = usu.unit_space_id AND us.cid = usu.cid )
						LEFT JOIN space_configurations sc ON ( sc.cid = usu.cid AND sc.id = usu.space_configuration_id )
					WHERE
						usu.cid = ' . ( int ) $intCid . '
						AND usu.property_id = ' . ( int ) $intPropertyId . '
						AND us.property_unit_id = ' . ( int ) $intPropertyUnitId . $strLeaseStartWindowCondition;
		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchUnitSpaceUnionsByUnitSpaceIdByLeaseTermIdsByLeaseStartWindowIdsByPropertyIdByCid( $intUnitSpaceId, $arrintLeaseTermIds, $arrintLeaseStartWindowIds, $intPropertyId, $intCid, $objDatabase ) {

		if ( false == valId( $intUnitSpaceId ) || false == valArr( $arrintLeaseTermIds ) || false == valArr( $arrintLeaseStartWindowIds ) ) return NULL;

		$strSql = '	SELECT
						array_to_string( array_agg( DISTINCT ( usu.unit_space_id ) ), \',\' ) AS unit_space_ids,
						usu.group_unit_space_id,
						usu.lease_term_id
					FROM
						unit_space_unions usu
					WHERE
						usu.cid = ' . ( int ) $intCid . '
						AND usu.property_id = ' . ( int ) $intPropertyId . '
						AND ( usu.group_unit_space_id = ' . ( int ) $intUnitSpaceId . ' OR usu.unit_space_id = ' . ( int ) $intUnitSpaceId . ' )
						AND usu.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' )
						AND usu.lease_start_window_id IN ( ' . implode( ',', $arrintLeaseStartWindowIds ) . ' )
					GROUP BY
						usu.group_unit_space_id,
						usu.lease_term_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteUnitSpaceUnionsByGroupUnitSpaceIdsByPropertyIdByCid( $arrintGroupUnitSpaceIds, $intPropertyId, $intCid, $objDatabase, $strCondition = NULL ) {
		if( false == valArr( $arrintGroupUnitSpaceIds ) ) return true;

		$strSql = 'DELETE FROM unit_space_unions WHERE group_unit_space_id IN ( ' . implode( ',', $arrintGroupUnitSpaceIds ) . ' ) AND cid = ' . ( int ) $intCid . $strCondition . ' AND property_id = ' . ( int ) $intPropertyId;

		return $objDatabase->execute( $strSql );
	}

	public static function fetchUnitSpaceUnionsByGroupUnitSpaceIdsByLeaseTermIdByLeaseStartWindowIdByPropertyIdByCid( $arrintGroupUnitSpaceIds, $intLeaseTermId, $intLeaseStartWindowId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintGroupUnitSpaceIds ) || false == valId( $intLeaseTermId ) || false == valId( $intLeaseStartWindowId ) ) return false;

		$strSql = 'SELECT
						count ( usu.unit_space_id ) as merged_unit_space_count,
						usu.group_unit_space_id,
						usu.space_configuration_id
					FROM
						unit_space_unions usu
					WHERE
						usu.cid = ' . ( int ) $intCid . '
						AND usu.property_id = ' . ( int ) $intPropertyId . '
						AND usu.lease_term_id = ' . ( int ) $intLeaseTermId . '
						AND usu.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
						AND usu.group_unit_space_id IN( ' . implode( ', ', $arrintGroupUnitSpaceIds ) . ' )
					GROUP BY
						usu.group_unit_space_id,
						usu.space_configuration_id';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnitSpaceUnionsByPropertyIdByLeaseTermsIdsByUnitSpaceIds( $intPropertyId, $arrintLeaseTermIds, $arrintUnitSpaceIds, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valArr( $arrintLeaseTermIds ) ) {
			return NULL;
		}
		$strSql = '
					SELECT
					    sub.lease_term_id,
					    sub.unit_space_id,
					    sub.usu_lease_start_windows_count,
					    sub.cid,
					    array_to_json ( array_agg ( sub.space_configuration_id ) ) AS space_configuration_ids
					FROM
					    (
					      SELECT
					          us.cid,
					          usu.lease_term_id,
					          usu.space_configuration_id,
					          usu.lease_start_window_id,
					          us.id AS unit_space_id,
					          us.unit_number_cache,
					          COUNT ( usu.space_configuration_id ) OVER ( PARTITION BY us.cid, us.id, usu.lease_term_id ) AS usu_lease_start_windows_count
					      FROM
					          unit_space_unions usu
					          JOIN lease_start_windows lsw ON ( lsw.id = usu.lease_start_window_id AND lsw.property_id = usu.property_id AND lsw.cid = usu.cid AND lsw.lease_term_id = usu.lease_term_id AND lsw.is_active = TRUE AND lsw.end_date >= NOW ( )::DATE AND lsw.deleted_on IS NULL )
					          JOIN unit_spaces us ON ( us.cid = usu.cid AND us.property_id = usu.property_id AND us.id = usu.unit_space_id AND us.is_marketed = TRUE )
					      WHERE
								usu.cid = ' . ( int ) $intCid . '
								AND usu.property_id = ' . ( int ) $intPropertyId . '
								AND usu.lease_term_id IN (' . implode( ', ', $arrintLeaseTermIds ) . ')
								AND usu.unit_space_id IN (' . implode( ', ', $arrintUnitSpaceIds ) . ')
					            AND usu.lease_start_window_id IS NOT NULL
					      GROUP BY
					          us.id,
					          us.cid,
					          usu.lease_term_id,
					          usu.lease_start_window_id,
					          usu.space_configuration_id
					    ) AS sub

					GROUP BY
					    sub.lease_term_id,
					    sub.unit_space_id,
					    sub.usu_lease_start_windows_count,
					    sub.cid,
					    sub.space_configuration_id';
		return fetchData( $strSql, $objDatabase );
	}

}
?>