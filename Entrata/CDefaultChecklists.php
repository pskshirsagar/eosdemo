<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultChecklists
 * Do not add any new functions to this class.
 */

class CDefaultChecklists extends CBaseDefaultChecklists {

	public static function fetchDefaultChecklists( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultChecklist::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultChecklist( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultChecklist::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>