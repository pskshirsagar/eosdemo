<?php

class CGlAccountUsageType extends CBaseGlAccountUsageType {

	const BANK_ACCOUNT				= 101;
	const ACCOUNTS_RECEIVABLE		= 102;
	const UNDEPOSITED_FUNDS			= 103;
	const REIMBURSEMENT_RECEIVABLE	= 104;
	const ACCUMULATED_DEPRECIATION	= 105;
	const FIXED_ASSET				= 106;
	const INVENTORY					= 107;
	const PENDING_REIMBURSEMENTS	= 108;

	const ACCOUNTS_PAYABLE			= 201;
	const PRE_PAYMENTS				= 202;
	const SECURITY_DEPOSITS_HELD	= 203;
	const CREDIT_CARD				= 204;
	const REFUND_IN_TRANSIT         = 206;
	const REIMBURSEMENT_PAYABLE		= 208;

	const RETAINED_EARNINGS			= 301;
	const OWNER_CONTRIBUTIONS		= 302;
	const OWNER_DRAWS				= 303;

	const RENT						= 401;
	const APPLICATION_FEES			= 402;
	const AMENITY					= 403;
	const SPECIALS_CONSESSIONS      = 404;
	const LATE_FEES					= 405;
	const RETURN_FEES				= 406;
	const IMPROVEMENTS				= 407;
	const DAMAGES_OR_MAINTENANCE_INCOME	= 408;
	const ANCILLARY_INCOME			= 409;
	const EARLY_TERMINATION_FEES	= 410;
	const BAD_DBEBT                 = 411;
	const BEGINNING_BALANCE			= 412;
	const INTEREST_INCOME			= 413;
	const UTILITY_INCOME			= 414;
	const GAIN_LOSS_ON_LEAVE		= 415;
	const VACANCY_LOSS				= 416;
	const DELINQUENT_RENT           = 418;
	const DELINQUENT_OTHER          = 419;
	const PREPAID_INCOME            = 420;
	const PET_INCOME				= 421;
	const ADDON_INCOME				= 422;
	const RISK_PREMIUM_INCOME		= 423;

	const MANAGEMENT_FEES			= 502;
	const DEPRECIATION				= 510;
	const UNCLAIMED_PROPERTY 		= 600;
	const RETENTION_PAYABLE         = 211;
	const OTHER_ASSET               = 199;
	const OTHER_LIABILITY           = 299;
	const OTHER_EQUITY              = 399;
	const OTHER_INCOME              = 499;
	const OTHER_EXPENSE 			= 599;

	public static $c_arrintExcludedGlAccountUsageTypeIds = [
		self::BANK_ACCOUNT,
		self::PENDING_REIMBURSEMENTS,
		self::UNDEPOSITED_FUNDS,
		self::REIMBURSEMENT_RECEIVABLE,
		self::REIMBURSEMENT_PAYABLE,
		self::REFUND_IN_TRANSIT,
		self::SECURITY_DEPOSITS_HELD,
		self::UNCLAIMED_PROPERTY
	];

	public static $c_arrintGlAccountUsageTypes = [
		'BANK_ACCOUNT' => self::BANK_ACCOUNT,
		'FIXED_ASSET' => self::FIXED_ASSET,
		'INVENTORY' => self::INVENTORY
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function assignSmartyData( $objSmarty ) {
		$objSmarty->assign( 'GL_ACCOUNT_USAGE_TYPE_ACCOUNTS_PAYABLE',			self::ACCOUNTS_PAYABLE );
		$objSmarty->assign( 'GL_ACCOUNT_USAGE_TYPE_RETAINED_EARNINGS', 		self::RETAINED_EARNINGS );
		$objSmarty->assign( 'GL_ACCOUNT_USAGE_TYPE_RENT', 					self::RENT );
		$objSmarty->assign( 'GL_ACCOUNT_USAGE_TYPE_GAIN_LOSS_ON_LEAVE', 		self::GAIN_LOSS_ON_LEAVE );
		$objSmarty->assign( 'GL_ACCOUNT_USAGE_TYPE_VACANCY_LOSS', 			self::VACANCY_LOSS );
		$objSmarty->assign( 'GL_ACCOUNT_USAGE_TYPE_PRE_PAYMENTS', 			self::PRE_PAYMENTS );
		$objSmarty->assign( 'GL_ACCOUNT_USAGE_TYPE_PENDING_REIMBURSEMENTS',	self::PENDING_REIMBURSEMENTS );
	}

}
?>