<?php

class CMilitaryMacPeriod extends CBaseMilitaryMacPeriod {

	const MOVE_IN = 'MI';
	const M0VE_OUT = 'MO';

	const MIMO = 1;
	const PROMO_DEMO = 2;
	const PAYMENTS = 3;


	protected $m_strCompanyEmployeeName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMilitaryInstallationId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMilitaryInstallationId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'military_installation_id', __( 'Military installation is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valMilitaryMacPeriodStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPostMonth() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month is required.' ) ) );
		} elseif( false == preg_match( '#^(((0?[1-9])|(1[012]))(/0?1)/([12]\d)?\d\d)$#', $this->getPostMonth() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Post month of format mm/yyyy is required.' ) ) );
		} elseif( false == is_null( $this->getMilitaryInstallationId() ) && true == valObj( $objDatabase, 'CDatabase' ) ) {

			$strSqlSearchWhere						= ' AND mmp.military_installation_id = ' . ( int ) $this->getMilitaryInstallationId() . '  AND mmp.post_month = \'' . $this->getPostMonth() . '\'';
			if( true == valId( $this->getId() ) ) {
				$strSqlSearchWhere .= ' AND mmp.id != ' . $this->getId();
			}

			$intSearchMilitaryMacPeriodsCount	= CMilitaryMacPeriods::fetchMilitaryMacPeriodsCountBySearchFields( $this->getCid(), $strSqlSearchWhere, $objDatabase );

			if( true == valId( $intSearchMilitaryMacPeriodsCount ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_month', __( 'Only one MAC request is allowed per installation per month.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valNumMoveIns() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumMoveOuts() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumPromotions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNumDemotions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalBahAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	/*
	 * Get Functions
	 */

	public function getCompanyEmployeeName() {
		return $this->m_strCompanyEmployeeName;
	}

	/*
	 * Set Functions
	 */

	public function setCompanyEmployeeName( $strCompanyEmployeeName ) {
		$this->m_strCompanyEmployeeName = $strCompanyEmployeeName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrmixValues['company_employee_name'] ) ) {
			$this->setCompanyEmployeeName( $arrmixValues['company_employee_name'] );
		}
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valMilitaryInstallationId();
				$boolIsValid &= $this->valPostMonth( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
