<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReservationFeeOccurrenceTypes
 * Do not add any new functions to this class.
 */

class CReservationFeeOccurrenceTypes extends CBaseReservationFeeOccurrenceTypes {

    public static function fetchReservationFeeOccurrenceTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CReservationFeeOccurrenceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchReservationFeeOccurrenceType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CReservationFeeOccurrenceType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPublishedReservationFeeOccurrenceTypes( $objDatabase ) {
    	$strSql = 'SELECT * FROM reservation_fee_occurrence_types WHERE is_published = 1';
    	return self::fetchReservationFeeOccurrenceTypes( $strSql, $objDatabase );
    }
}
?>