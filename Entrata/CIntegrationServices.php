<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationServices
 * Do not add any new functions to this class.
 */

class CIntegrationServices extends CBaseIntegrationServices {

	public static function fetchAllIntegrationServices( $objDatabase ) {
		return parent::fetchIntegrationServices( 'SELECT * FROM integration_services ORDER BY name', $objDatabase );
	}

    public static function fetchIntegrationServicesByIds( $arrintIntegrationServiceIds, $objDatabase, $boolOrderBy = false ) {

		if( false == valArr( $arrintIntegrationServiceIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						integration_services
					WHERE
						id IN ( ' . implode( ',', $arrintIntegrationServiceIds ) . ' )'
						. ( ( true == $boolOrderBy ) ? ' ORDER BY name' : '' );

		return parent::fetchIntegrationServices( $strSql, $objDatabase );
	}

    public static function fetchIntegrationServices( $objDatabase, $boolReturnArray = false ) {
		$strSql = 'SELECT
							DISTINCT isr.*
						FROM
							integration_services isr ,
							integration_client_type_services icts
						WHERE
							isr.id = icts.integration_service_id OR isr.id IN( ' . CIntegrationService::MODIFY_EXTERNAL_REFERENCE . ',' . CIntegrationService::MODIFY_EXTERNAL_REFERENCES . ',' . CIntegrationService::RETRIEVE_DIFFERENTIAL_CUSTOMERS . ',' . CIntegrationService::VENDOR_PROP_RESTRICTIONS . ',' . CIntegrationService::OPEN_PAYMENT_BATCH . ',' . CIntegrationService::RETRIEVE_LEASE_OFFERS . ',' . CIntegrationService::RETRIEVE_RENEWAL_LEASE_CHARGES . ' )
						ORDER BY
					 		isr.name';

		if( true == $boolReturnArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return parent::fetchIntegrationServices( $strSql, $objDatabase );
	}

	public static function fetchIntegrationServcesByIntegrationClientTypeIds( $arrintIntegrationClientTypeIds, $objDatabase ) {
		if( false == valArr( $arrintIntegrationClientTypeIds ) ) return NULL;

		$strSql = 'SELECT
							*
						FROM
							integration_services
						WHERE
							id IN (
									SELECT
										DISTINCT ( integration_service_id )
									FROM
										integration_client_type_services
									WHERE
										integration_client_type_id IN ( ' . implode( ',', $arrintIntegrationClientTypeIds ) . ' )
									)
						ORDER BY
							name;';

		return parent::fetchIntegrationServices( $strSql, $objDatabase );
	}

}
?>