<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CModuleFilters
 * Do not add any new functions to this class.
 */

class CModuleFilters extends CBaseModuleFilters {

	public static function fetchModuleFiltersByFilterTypeByCompanyUserIdByCid( $strFilterType, $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						module_filters mf
					WHERE
						mf.cid = ' . ( int ) $intCid . '
						AND mf.filter_type LIKE \'%' . $strFilterType . '%\'
						AND mf.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND mf.deleted_on IS NULL
					ORDER BY
						mf.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchModuleFilterByNameByCompanyUserIdByCid( $strFilterName, $intCompanyUserId, $intCid, $objDatabase, $strFilterType = NULL ) {

			if( true == valStr( $strFilterType ) ) {
				$strWhereCondition = 'AND mf.filter_type LIKE \'' . $strFilterType . '\' ';
			}

		$strSql = 'SELECT
						*
					FROM
						module_filters mf
					WHERE
						mf.cid = ' . ( int ) $intCid . '
						AND mf.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND mf.name LIKE \'' . addslashes( $strFilterName ) . '\'
						' . $strWhereCondition . '
						AND mf.deleted_on IS NULL
					LIMIT 1';

		return self::fetchModuleFilter( $strSql, $objDatabase );
	}

}
?>