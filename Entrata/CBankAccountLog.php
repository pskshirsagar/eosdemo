<?php

class CBankAccountLog extends CBaseBankAccountLog {

	const ACTION_CREATED				= 'Created';
	const ACTION_EDITED					= 'Edited';
	const ACTION_DISABLED				= 'Disabled';
	const ACTION_ENABLED				= 'Enabled';
	const ACTION_PAYMENT_FILES_EDITED	= 'Payment Files Edited';
	const ACTION_NOTES					= 'Note';
	const ACTION_SFTP_EDITED			= 'SFTP Edited';

	protected $m_strActionNote;
	protected $m_strCompanyEmployeeFullName;
	protected $m_strCompanyDiscretionaryData;

	public function mapBankAccountLog( $objBankAccount ) {

		$this->setCid( $objBankAccount->getCid() );
		$this->setBankAccountId( $objBankAccount->getId() );
		$this->setBankAccountTypeId( $objBankAccount->getBankAccountTypeId() );
		$this->setBankLoginTypeId( $objBankAccount->getBankLoginTypeId() );
		$this->setCheckTemplateId( $objBankAccount->getCheckTemplateId() );
		$this->setCheckAccountTypeId( $objBankAccount->getCheckAccountTypeId() );
		$this->setAccountName( $objBankAccount->getAccountName() );
		$this->setCheckBankName( $objBankAccount->getCheckBankName() );
		$this->setBankStreetLine1( $objBankAccount->getBankStreetLine1() );
		$this->setBankStreetLine2( $objBankAccount->getBankStreetLine2() );
		$this->setBankStateCode( $objBankAccount->getBankStateCode() );
		$this->setBankPostalCode( $objBankAccount->getBankPostalCode() );
		$this->setBankCity( $objBankAccount->getBankCity() );
		$this->setCheckNameOnAccount( $objBankAccount->getCheckNameOnAccount() );
		$this->setPayerStreetLine1( $objBankAccount->getPayerStreetLine1() );
		$this->setPayerStreetLine2( $objBankAccount->getPayerStreetLine2() );
		$this->setPayerCity( $objBankAccount->getPayerCity() );
		$this->setPayerStateCode( $objBankAccount->getPayerStateCode() );
		$this->setPayerPostalCode( $objBankAccount->getPayerPostalCode() );
		$this->setPayerPhoneNumberEncrypted( $objBankAccount->getPayerPhoneNumberEncrypted() );
		$this->setCheckRoutingNumber( $objBankAccount->getCheckRoutingNumber() );
		$this->setCheckFractionalRoutingNumber( $objBankAccount->getCheckFractionalRoutingNumber() );
		$this->setCheckAccountNumberEncrypted( $objBankAccount->getCheckAccountNumberEncrypted() );
		$this->setNextCheckNumber( $objBankAccount->getNextCheckNumber() );
		$this->setPrintSignatureLimitAmount( $objBankAccount->getPrintSignatureLimitAmount() );
		$this->setSecondSignatureLimitAmount( $objBankAccount->getSecondSignatureLimitAmount() );
		$this->setReserveAmount( $objBankAccount->getReserveAmount() );
		$this->setPrintInvoiceDetails( $objBankAccount->getPrintInvoiceDetails() );
		$this->setIsCreditCardAccount( $objBankAccount->getIsCreditCardAccount() );
		$this->setIsStubStubCheckFormat( $objBankAccount->getIsStubStubCheckFormat() );
		$this->setIsDisabled( $objBankAccount->getIsDisabled() );
		$this->setIsBankRecOnPostMonth( $objBankAccount->getIsBankRecOnPostMonth() );
		$this->setIsCashBasis( $objBankAccount->getIsCashBasis() );
		$this->setImmediateDestinationName( $objBankAccount->getImmediateDestinationName() );
		$this->setImmediateDestinationId( $objBankAccount->getImmediateDestinationId() );
		$this->setImmediateOriginId( $objBankAccount->getImmediateOriginId() );
		$this->setImmediateOriginName( $objBankAccount->getImmediateOriginName() );
		$this->setCompanyName( $objBankAccount->getCompanyName() );
		$this->setCompanyId( $objBankAccount->getCompanyId() );
		$this->setOriginatingDfi( $objBankAccount->getOriginatingDfi() );
		$this->setIsAchEnable( $objBankAccount->getIsAchEnable() );
		$this->setIsBalancedAchFile( $objBankAccount->getIsBalancedAchFile() );
		$this->setIsShowVendorAccountNumber( $objBankAccount->getIsShowVendorAccountNumber() );
		$this->setIsCreateInvoiceAtPayment( $objBankAccount->getIsCreateInvoiceAtPayment() );
		$this->setDeletedBy( $objBankAccount->getDeletedBy() );
		$this->setDeletedOn( $objBankAccount->getDeletedOn() );
		$this->setUpdatedBy( $objBankAccount->getUpdatedBy() );
		$this->setUpdatedOn( $objBankAccount->getUpdatedOn() );
		$this->setCreatedBy( $objBankAccount->getCreatedBy() );
		$this->setCreatedOn( $objBankAccount->getCreatedOn() );
		$this->setWfpmCeoCompanyId( $objBankAccount->getWfpmCeoCompanyId() );
		$this->setCheckLayoutId( $objBankAccount->getCheckLayoutId() );
		$this->setOriginatingBankId( $objBankAccount->getOriginatingBankId() );
		$this->setAchCompanyId( $objBankAccount->getAchCompanyId() );
		$this->setAchSecCode( $objBankAccount->getAchSecCode() );
		$this->setIsWfpmEnable( $objBankAccount->getIsWfpmEnable() );
		$this->setCcExpDateMonth( $objBankAccount->getCcExpDateMonth() );
		$this->setCcExpDateYear( $objBankAccount->getCcExpDateYear() );
		$this->setCcFirstName( $objBankAccount->getCcFirstName() );
		$this->setCcLastName( $objBankAccount->getCcLastName() );
		$this->setApPayeeId( $objBankAccount->getApPayeeId() );
		$this->setApPayeeLocationId( $objBankAccount->getApPayeeLocationId() );
		$this->setApPayeeAccountId( $objBankAccount->getApPayeeAccountId() );
		$this->setIsUseSubledgers( $objBankAccount->getIsUseSubledgers() );
		$this->setIsAutoGenerateReimbursementInvoice( $objBankAccount->getIsAutoGenerateReimbursementInvoice() );
		$this->setSenderId( $objBankAccount->getSenderId() );
		$this->setClientId( $objBankAccount->getClientId() );
		$this->setDetails( json_encode( $objBankAccount->getDetails() ) );
	}

	/**
	 * Get Functions
	 */

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	public function getPayerPhoneNumber() {
		if( false == valStr( $this->m_strPayerPhoneNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPayerPhoneNumberEncrypted, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getCheckAccountNumber() {
		if( false == valStr( $this->m_strCheckAccountNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getCompanyDiscretionaryData() {
		return $this->m_strCompanyDiscretionaryData;
	}

	/**
	 * Set Functions
	 */

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = CStrings::strTrimDef( $strCompanyEmployeeFullName, 100, NULL, true );
	}

	public function setPayerPhoneNumber( $strPayerPhoneNumber ) {
		$strPayerPhoneNumber = CStrings::strTrimDef( $strPayerPhoneNumber, 15, NULL, true );
		if( true == valStr( $strPayerPhoneNumber ) ) {
			$this->setPayerPhoneNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPayerPhoneNumber, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		if ( true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumber, 'XXXX' ) ) return;
		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 25, NULL, true );
		if( true == valStr( $strCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	public function setCompanyDiscretionaryData( $strCompanyDiscretionaryData ) {
		$this->m_strCompanyDiscretionaryData = $strCompanyDiscretionaryData;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_employee_full_name'] ) ) $this->setCompanyEmployeeFullName( $arrmixValues['company_employee_full_name'] );
		return;
	}

	/**
	 * Fetch Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankLoginTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckBankName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankContactNameFirstEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankContactNameLastEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBankPhoneNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckNameOnAccount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayerStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayerStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayerCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayerStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayerPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayerPhoneNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckRoutingNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckFractionalRoutingNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckAccountNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckAccountNumberMasked() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckReorderReminderDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCheckReorderReminderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNextCheckNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinimumBalance() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrintSignatureLimitAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSecondSignatureLimitAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReserveAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrintInvoiceDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginPasswordEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCreditCardAccount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsStubStubCheckFormat() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsBankRecOnPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCashBasis() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public static function loadBankAccountLogActions() {
		$arrstrBankAccountLogActions = [];

		$arrstrBankAccountLogActions[self::ACTION_CREATED]					= self::ACTION_CREATED;
		$arrstrBankAccountLogActions[self::ACTION_EDITED]					= self::ACTION_EDITED;
		$arrstrBankAccountLogActions[self::ACTION_DISABLED]					= self::ACTION_DISABLED;
		$arrstrBankAccountLogActions[self::ACTION_ENABLED]					= self::ACTION_ENABLED;
		$arrstrBankAccountLogActions[self::ACTION_NOTES]					= self::ACTION_NOTES;
		$arrstrBankAccountLogActions[self::ACTION_PAYMENT_FILES_EDITED]		= self::ACTION_PAYMENT_FILES_EDITED;
		return $arrstrBankAccountLogActions;
	}

}
?>