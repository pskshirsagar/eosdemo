<?php

class CCustomerIncome extends CBaseCustomerIncome {

	const HOURLY			= 15;
	const WEEKLY			= 3;
	const BI_WEEKLY			= 11;
	const SEMI_MONTHLY		= 12;
	const MONTHLY			= 4;
	const ANNUALY			= 6;

	const FUTURE			= 1;
	const CURRENT			= 2;
	const PAST				= 3;

	public static $c_arrstrAllIncomeStatusTypes = [
		self::FUTURE	=> 'Future',
		self::CURRENT	=> 'Current',
		self::PAST		=> 'Past'
	];

	protected $m_boolIsDelete;
	protected $m_boolIncomeTypeSelected;
	protected $m_boolIsVerified;
	protected $m_boolMemberReceivesOvertimePay;
	protected $m_boolMemberReceivesTips;
	protected $m_boolMemberReceivesBonuses;
	protected $m_boolIsPureAffordableLease;

	protected $m_strTypeOfBusiness;
	protected $m_strBusinessTaxId;
	protected $m_strIncomeName;
	protected $m_strIncomeCode;
	protected $m_strRequiredQuestion;
	protected $m_strStatus;
	protected $m_strAction;

	protected $m_intIncomeTypeCount = NULL;
	protected $m_intApplicationId;
	protected $m_intApplicantId;
	protected $m_intCurrentApplicationStepId;
	protected $m_intFrequencyOfTips;
	protected $m_intHourPerWeek;
	protected $m_intWeekPerYear;
	protected $m_intOvertimeHoursPerWeek;
	protected $m_intOvertimeWeeksPerYear;
	protected $m_intFrequencyOfBonuses;
	protected $m_intMemberNumber;

	protected $m_fltHourlyWage;
	protected $m_fltAnnualIncomeAmount;
	protected $m_fltOvertimeWage;
	protected $m_fltBonusAmount;
	protected $m_fltTipAmount;

	protected $m_arrstrIncomeStatusTypes;

	protected $m_objParsedDetails;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIncomeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFrequencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInstitutionCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContactEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPositionOrTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSocialSecurityClaimNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateStarted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateEnded() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIncomeEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objIncomeType = NULL, $arrobjPropertyApplicationPreferences = NULL, $arrstrIncomeTypeCustomFields = NULL, $arrobjPropertyPreferences = NULL, $boolValidateRequiredFields = true, $boolValidateEndDate = true ) {

		$boolIsValid = true;

		$objCustomerIncomeValidator = new CCustomerIncomeValidator();
		$objCustomerIncomeValidator->setCustomerIncome( $this );
		$objCustomerIncomeValidator->setValidateRequiredFields( $boolValidateRequiredFields );

		$boolIsValid &= $objCustomerIncomeValidator->validate( $strAction, $objIncomeType, $arrobjPropertyApplicationPreferences, $arrstrIncomeTypeCustomFields, $arrobjPropertyPreferences, $boolValidateEndDate );

		return $boolIsValid;
	}

	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = $boolIsDelete;
	}

	public function setIncomeName( $strIncomeName ) {
		$this->m_strIncomeName = $strIncomeName;
	}

	public function setIncomeCode( $strIncomeCode ) {
		$this->m_strIncomeCode = $strIncomeCode;
	}

	public function setIncomeTypeSelected( $boolIncomeTypeSelected ) {
		$this->m_boolIncomeTypeSelected = $boolIncomeTypeSelected;
	}

	public function setIncomeTypeCount( $intIncomeTypeCount ) {
		$this->m_intIncomeTypeCount = $intIncomeTypeCount;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setHourlyWage( $fltHourlyWage ) {
		$this->m_fltHourlyWage = $fltHourlyWage;
	}

	public function setHourPerWeek( $intHourPerWeek ) {
		$this->m_intHourPerWeek = $intHourPerWeek;
	}

	public function setWeekPerYear( $intWeekPerYear ) {
		$this->m_intWeekPerYear = $intWeekPerYear;
	}

	public function setMemberReceivesOvertimePay( $boolMemberReceivesOvertimePay ) {
		$this->m_boolMemberReceivesOvertimePay = $boolMemberReceivesOvertimePay;
	}

	public function setOvertimeWage( $fltOvertimeWage ) {
		$this->m_fltOvertimeWage = $fltOvertimeWage;
	}

	public function setOvertimeHoursPerWeek( $intOvertimeHoursPerWeek ) {
		$this->m_intOvertimeHoursPerWeek = $intOvertimeHoursPerWeek;
	}

	public function setOvertimeWeeksPerYear( $intOvertimeWeeksPerYear ) {
		$this->m_intOvertimeWeeksPerYear = $intOvertimeWeeksPerYear;
	}

	public function setMemberReceivesBonuses( $boolMemberReceivesBonuses ) {
		$this->m_boolMemberReceivesBonuses = $boolMemberReceivesBonuses;
	}

	public function setFrequencyOfBonuses( $intFrequencyOfBonuses ) {
		$this->m_intFrequencyOfBonuses = $intFrequencyOfBonuses;
	}

	public function setBonusAmount( $fltBonusAmount ) {
		$this->m_fltBonusAmount = $fltBonusAmount;
	}

	public function setMemberReceivesTips( $boolMemberReceivesTips ) {
		$this->m_boolMemberReceivesTips = $boolMemberReceivesTips;
	}

	public function setFrequencyOfTips( $intFrequencyOfTips ) {
		$this->m_intFrequencyOfTips = $intFrequencyOfTips;
	}

	public function setTipAmount( $fltTipAmount ) {
		$this->m_fltTipAmount = $fltTipAmount;
	}

	public function setAnnualIncomeAmount( $fltAnnualIncomeAmount ) {
		$this->m_fltAnnualIncomeAmount = $fltAnnualIncomeAmount;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = ( int ) $intApplicantId;
	}

	public function setCurrentApplicationStepId( $intCurrentApplicationStepId ) {
		$this->m_intCurrentApplicationStepId = ( int ) $intCurrentApplicationStepId;
	}

	public function setSsnClaimNumber( $strSsnClaimNumber ) {
		$this->m_strSsnClaimNumber = $strSsnClaimNumber;
	}

	public function setMemberNumber( $intMemberNumber ) {
		$this->m_intMemberNumber = $intMemberNumber;
	}

	public function setStatus( $strStatus ) {
		$this->m_strStatus = $strStatus;
	}

	public function setSocialSecurityClaimNumber( $strSocialSecurityClaimNumber ) {

		if( true == valStr( $strSocialSecurityClaimNumber ) ) {
			$this->setSocialSecurityClaimNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( preg_replace( '/[^a-z0-9]/i', '', $strSocialSecurityClaimNumber ), CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
	}

	public function getIsPureAffordableLease() {
		return $this->m_boolIsPureAffordableLease;
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function getHourlyWage() {
		return $this->m_fltHourlyWage;
	}

	public function getHourPerWeek() {
		return $this->m_intHourPerWeek;
	}

	public function getWeekPerYear() {
		return $this->m_intWeekPerYear;
	}

	public function getMemberReceivesOvertimePay() {
		return $this->m_boolMemberReceivesOvertimePay;
	}

	public function getOvertimeWage() {
		return $this->m_fltOvertimeWage;
	}

	public function getOvertimeHoursPerWeek() {
		return $this->m_intOvertimeHoursPerWeek;
	}

	public function getOvertimeWeeksPerYear() {
		return $this->m_intOvertimeWeeksPerYear;
	}

	public function getMemberReceivesBonuses() {
		return $this->m_boolMemberReceivesBonuses;
	}

	public function getFrequencyOfBonuses() {
		return $this->m_intFrequencyOfBonuses;
	}

	public function getBonusAmount() {
		return $this->m_fltBonusAmount;
	}

	public function getMemberReceivesTips() {
		return $this->m_boolMemberReceivesTips;
	}

	public function getFrequencyOfTips() {
		return $this->m_intFrequencyOfTips;
	}

	public function getTipAmount() {
		return $this->m_fltTipAmount;
	}

	public function getAnnualIncomeAmount() {
		return $this->m_fltAnnualIncomeAmount;
	}

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	public function getIncomeName() {
		return $this->m_strIncomeName;
	}

	public function getIncomeCode() {
		return $this->m_strIncomeCode;
	}

	public function getIncomeTypeSelected() {
		return $this->m_boolIncomeTypeSelected;
	}

	public function getIncomeTypeCount() {
		return $this->m_intIncomeTypeCount;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getDefaultSectionName() {
		return 'Institution\'s';
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getCurrentApplicationStepId() {
		return $this->m_intCurrentApplicationStepId;
	}

	public function getSsnClaimNumber() {
		return $this->m_strSsnClaimNumber;
	}

	public function getMemberNumber() {
		return $this->m_intMemberNumber;
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function setIsPureAffordableLease( $boolIsPureAffordableLease ) {
		$this->m_boolIsPureAffordableLease = $boolIsPureAffordableLease;
	}

	public function setAction( $strAction ) {
		$this->m_strAction = $strAction;
	}

	public function getRecentLog( $objDatabase ) {
		$objCustomerIncomeLog = CCustomerIncomeLogs::fetchRecentCustomerIncomeLogByCustomerIdByIdByCid( $this->getCustomerId(), $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objCustomerIncomeLog, 'CCustomerIncomeLog' ) ) {
			return $objCustomerIncomeLog;
		}

		return NULL;
	}

	public function getMonthlyAmount() {

		$fltAmount = $this->getAmount();

		switch( $this->getFrequencyId() ) {
			case CFrequency::DAILY:
				$fltAmount *= 30;
				break;

			case CFrequency::WEEKLY:
				$fltAmount = ( $fltAmount * 30 ) / 7;
				break;

			case CFrequency::QUARTERLY:
				$fltAmount = $fltAmount / 3;
				break;

			case CFrequency::YEARLY:
				$fltAmount = $fltAmount / 12;
				break;

			default:
				// default case
				break;
		}

		return $fltAmount;
	}

	public function getRequiredQuestion( $boolIsSecondaryCustomer = false, $strCustomerFirstName = NULL, $strIncomeTypeLabel = NULL ) {
		if( true == valStr( $this->m_strRequiredQuestion ) ) return $this->m_strRequiredQuestion;

		switch( $this->getIncomeTypeId() ) {
			case CIncomeType::CURRENT_EMPLOYER:
				$this->m_strRequiredQuestion = ( 1 == $boolIsSecondaryCustomer ) ? __( 'Is {%s, customer_first_name} currently employed ?', [ 'customer_first_name' => $strCustomerFirstName ] ) : __( 'Are you currently employed ?' );
				break;

			case CIncomeType::SELF_EMPLOYER:
				$this->m_strRequiredQuestion = ( 1 == $boolIsSecondaryCustomer ) ? __( 'Is {%s, customer_first_name} self employed ?', [ 'customer_first_name' => $strCustomerFirstName ] ) : __( 'Are you self employed ?' );
				break;

			case CIncomeType::PREVIOUS_EMPLOYER:
				$this->m_strRequiredQuestion = ( 1 == $boolIsSecondaryCustomer ) ? __( 'Was {%s, customer_first_name} previously employed ?', [ 'customer_first_name' => $strCustomerFirstName ] ) : __( 'Were you previously employed ?' );
				break;

			case CIncomeType::VA_BENEFITS:
				$this->m_strRequiredQuestion = ( 1 == $boolIsSecondaryCustomer ) ? __( 'Is {%s, customer_first_name} receiving VA benefits ?', [ 'customer_first_name' => $strCustomerFirstName ] ) : __( 'Are you receiving VA benefits ?' );
				break;

			case CIncomeType::SSI:
				$this->m_strRequiredQuestion = ( 1 == $boolIsSecondaryCustomer ) ? __( 'Is {%s, customer_first_name} receiving SSI ?', [ 'customer_first_name' => $strCustomerFirstName ] ): __( 'Are you receiving SSI ?' );
				break;

			case CIncomeType::DHS:
				$this->m_strRequiredQuestion = ( 1 == $boolIsSecondaryCustomer ) ? __( 'Is {%s, customer_first_name} receiving DHS ( {%m, dhs_amount} ) ?', [ 'customer_first_name' => $strCustomerFirstName, 'dhs_amount' => 42 ] ): __( 'Are you receiving DHS ( {%m, dhs_amount} ) ?', [ 'dhs_amount' => 42 ] );
				break;

			default:
				$this->m_strRequiredQuestion = ( 1 == $boolIsSecondaryCustomer ) ? __( 'Is {%s, customer_first_name} receiving {%s, income_type_lable} ?', [ 'customer_first_name' => $strCustomerFirstName, 'income_type_lable' => $strIncomeTypeLabel ] ) : __( 'Are you receiving {%s, income_type_lable} ?', [ 'income_type_lable' => $strIncomeTypeLabel ] );
		}

		return $this->m_strRequiredQuestion;
	}

	public function getParsedDetails() {
		$this->m_objParsedDetails = $this->getDetailsField( [] );
		return $this->m_objParsedDetails;
	}

	public function getTypeOfBusiness() {

		if( false == is_object( $this->m_objParsedDetails ) )
			$this->m_objParsedDetails = $this->getParsedDetails();

		if( true == is_object( $this->m_objParsedDetails ) && true == property_exists( $this->m_objParsedDetails, 'type_of_business' ) )
			return $this->m_objParsedDetails->type_of_business;

		return NULL;
	}

	public function getBusinessTaxId( $boolIsMaskedBusinessTaxId = false ) {

		if( false == is_object( $this->m_objParsedDetails ) ) {
			$this->m_objParsedDetails = $this->getParsedDetails();
		}

		$strBusinessTaxId = NULL;

		if( true == is_object( $this->m_objParsedDetails ) && true == property_exists( $this->m_objParsedDetails, 'business_tax_id' ) && true == valStr( $this->m_objParsedDetails->business_tax_id ) ) {
			$strBusinessTaxId = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_objParsedDetails->business_tax_id, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
		}

		if( true == $boolIsMaskedBusinessTaxId && false == is_null( $strBusinessTaxId ) ) {
			$strBusinessTaxId = CEncryption::maskText( $strBusinessTaxId );
		}

		return $strBusinessTaxId;
	}

	public function getOldBusinessTaxId( $strDetails = NULL ) {
		if( false == valStr( $strDetails ) ) return NULL;

		$objParsedDetails = json_decode( $strDetails );

		$strOldBusinessTaxId = NULL;

		if( true == is_object( $objParsedDetails ) ) {
			$strOldBusinessTaxId = preg_replace( '/[^a-z0-9]/i', '', CEncryption::decryptText( $objParsedDetails->business_tax_id, CONFIG_KEY_TAX_NUMBER ) );
		}

		return $strOldBusinessTaxId;
	}

	public function getSocialSecurityClaimNumber() {

		if( false == valStr( $this->getSocialSecurityClaimNumberEncrypted() ) ) {
			return NULL;
		}

		return preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getSocialSecurityClaimNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER ) );

	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['income_name'] ) ) 	$this->setIncomeName( $arrmixValues['income_name'] );
		if( true == isset( $arrmixValues['member_number'] ) ) 	$this->setMemberNumber( $arrmixValues['member_number'] );
		if( true == isset( $arrmixValues['income_code'] ) ) 	$this->setIncomeCode( $arrmixValues['income_code'] );
		if( true == isset( $arrmixValues['status'] ) )          $this->setStatus( $arrmixValues['status'] );
		if( true == isset( $arrmixValues['social_security_claim_number'] ) ) $this->setSocialSecurityClaimNumber( $arrmixValues['social_security_claim_number'] );
		if( true == isset( $arrmixValues['ssn_claim_number'] ) ) {
			$this->setSsnClaimNumber( preg_replace( '/[^a-z0-9]/i', '', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $arrmixValues['ssn_claim_number'], CONFIG_SODIUM_KEY_TAX_NUMBER ) ) );
		}

		$arrmixDetailsKeys = [
			'hourly_wage',
			'hour_per_week',
			'week_per_year',
			'member_receives_overtime_pay',
			'overtime_wage',
			'overtime_hours_per_week',
			'overtime_weeks_per_year',
			'member_receives_bonuses',
			'frequency_of_bonuses',
			'bonus_amount',
			'member_receives_tips',
			'frequency_of_tips',
			'tip_amount',
			'annual_income_amount'
		];

		$arrmixTempDetail = [];
		foreach( $arrmixDetailsKeys as $intKeyDetail ) {
			if( true == isset( $arrmixValues[$intKeyDetail] ) ) {
				$arrmixTempDetail[$intKeyDetail] = $arrmixValues[$intKeyDetail];
			}
		}

		if( true == valStr( json_encode( $this->getDetailsField( [] ) ) ) && true == valStr( json_encode( $arrmixValues['details'] ) ) && false == valArr( $arrmixTempDetail ) ) {
			$arrmixValues = ( array ) ( $this->getDetailsField( [] ) );

		} elseif( true == valStr( $arrmixValues['frequency_id'] ) ) {
			$this->setDetails( json_encode( $arrmixTempDetail ) );
		}

		if( true === is_null( getArrayElementByKey( 'annual_income_amount', $arrmixValues ) ) ) {
			switch( $this->getFrequencyId() ) {

				case CFrequency::WEEKLY:
					$arrmixValues['annual_income_amount'] = $this->getAmount() * 52;
					break;

				case CFrequency::EVERY_TWO_WEEKS:
					$arrmixValues['annual_income_amount'] = $this->getAmount() * 26;
					break;

				case CFrequency::TWICE_PER_MONTH:
					$arrmixValues['annual_income_amount'] = $this->getAmount() * 24;
					break;

				case CFrequency::MONTHLY:
					$arrmixValues['annual_income_amount'] = $this->getAmount() * 12;
					break;

				case CFrequency::YEARLY:
					$arrmixValues['annual_income_amount'] = $this->getAmount() * 1;
					break;

				default:
					$arrmixValues['annual_income_amount'] = $this->getAmount() * 12;
					break;
			}

		}

		$this->setJsonData( $arrmixValues );
	}

	public function setJsonData( $arrmixValues ) {

		if( true == isset( $arrmixValues['hourly_wage'] ) )						$this->setHourlyWage( $arrmixValues['hourly_wage'] );
		if( true == isset( $arrmixValues['hour_per_week'] ) )					$this->setHourPerWeek( $arrmixValues['hour_per_week'] );
		if( true == isset( $arrmixValues['week_per_year'] ) )					$this->setWeekPerYear( $arrmixValues['week_per_year'] );
		if( true == isset( $arrmixValues['member_receives_overtime_pay'] ) )	$this->setMemberReceivesOvertimePay( $arrmixValues['member_receives_overtime_pay'] );
		if( true == isset( $arrmixValues['overtime_wage'] ) )					$this->setOvertimeWage( $arrmixValues['overtime_wage'] );
		if( true == isset( $arrmixValues['overtime_hours_per_week'] ) )			$this->setOvertimeHoursPerWeek( $arrmixValues['overtime_hours_per_week'] );
		if( true == isset( $arrmixValues['overtime_weeks_per_year'] ) )			$this->setOvertimeWeeksPerYear( $arrmixValues['overtime_weeks_per_year'] );
		if( true == isset( $arrmixValues['member_receives_bonuses'] ) )			$this->setMemberReceivesBonuses( $arrmixValues['member_receives_bonuses'] );
		if( true == isset( $arrmixValues['frequency_of_bonuses'] ) )			$this->setFrequencyOfBonuses( $arrmixValues['frequency_of_bonuses'] );
		if( true == isset( $arrmixValues['bonus_amount'] ) )					$this->setBonusAmount( $arrmixValues['bonus_amount'] );
		if( true == isset( $arrmixValues['member_receives_tips'] ) )			$this->setMemberReceivesTips( $arrmixValues['member_receives_tips'] );
		if( true == isset( $arrmixValues['frequency_of_tips'] ) )				$this->setFrequencyOfTips( $arrmixValues['frequency_of_tips'] );
		if( true == isset( $arrmixValues['tip_amount'] ) )						$this->setTipAmount( $arrmixValues['tip_amount'] );
		if( true == isset( $arrmixValues['annual_income_amount'] ) )			$this->setAnnualIncomeAmount( $arrmixValues['annual_income_amount'] );
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getIncomeStatusTypes() {

		if( true == valArr( $this->m_arrstrIncomeStatusTypes ) )
			return $this->m_arrstrIncomeStatusTypes;

		$this->m_arrstrIncomeStatusTypes = [
			self::FUTURE      => __( 'Future' ),
			self::CURRENT     => __( 'Current' ),
			self::PAST       => __( 'Past' )
		];

		return $this->m_arrstrIncomeStatusTypes;
	}

	public function makeBlank() {
		$this->setDescription( NULL );
		$this->setDetails( NULL );
		$this->setInstitutionName( NULL );
		$this->setInstitutionPhoneNumber( NULL );
		$this->setInstitutionStreetLine1( NULL );
		$this->setInstitutionStreetLine2( NULL );
		$this->setInstitutionStreetLine3( NULL );
		$this->setInstitutionCity( NULL );
		$this->setInstitutionStateCode( NULL );
		$this->setInstitutionProvince( NULL );
		$this->setInstitutionPostalCode( NULL );
		$this->setInstitutionCountryCode( NULL );
		$this->setContactName( NULL );
		$this->setContactPhoneNumber( NULL );
		$this->setContactFaxNumber( NULL );
		$this->setContactEmail( NULL );
		$this->setPositionOrTitle( NULL );
		$this->setAmount( NULL );
		$this->setIncomeEffectiveDate( NULL );
		$this->setDateStarted( NULL );
		$this->setDateEnded( NULL );
		$this->setIsInstitutionAddressVerified( false );
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		if( false == $boolReturnSqlOnly ) {
			$boolIsValid  = $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			$boolIsValid &= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_INSERT );
			$boolIsValid &= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			return $boolIsValid;
		}

		$strSql  = $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_INSERT );
		$strSql .= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return $strSql;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		$arrstrResponse = fetchData( 'SELECT * FROM customer_incomes WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );
		$this->setSerializedOriginalValues( serialize( $arrstrResponse[0] ) );

		if( true == valObj( $this, 'CCustomerIncome' ) && ( 0 == $this->getApplicantId() || NULL == $this->getApplicantId() ) ) {
			$arrintResponse = fetchData( 'SELECT id FROM applicants WHERE customer_id = ' . ( int ) $this->getCustomerId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );
			$this->setApplicantId( $arrintResponse[0]['id'] );
		}

		if( false == $boolReturnSqlOnly ) {
			$boolIsValid  = $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			$boolIsValid &= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly );
			$boolIsValid &= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			return $boolIsValid;
		}

		$strSql  = $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly );
		$strSql .= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		return $strSql;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSoftDelete = true ) {

		if( true == $boolSoftDelete ) {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		$strSql						= 'DELETE FROM public.customer_income_logs WHERE customer_income_id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';
		$strApplicantChangeLogSql	= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, true, VALIDATE_DELETE );

		if( true == $boolReturnSqlOnly ) {
			return $strSql . parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) . $strApplicantChangeLogSql;
		} else {
			$boolResult = $this->executeSql( $strSql, $this, $objDatabase );

			if( true == $boolResult ) {
				$boolResult &= $this->executeSql( $strApplicantChangeLogSql, $this, $objDatabase );
				$boolResult = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			}

			return $boolResult;
		}
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objPreviousCustomerIncome = NULL ) {

		$boolIsValid = true;
		if( true == $this->getIsPureAffordableLease() && true == valstr( $this->getAction() ) ) {
			$boolIsValid = $this->insertEventsAndApplicantApplicationChangesForAffordable( $intCurrentUserId, $objDatabase, $objPreviousCustomerIncome );
		}

		if( true == $boolIsValid ) {
			if( true == is_null( $this->getId() ) ) {
				return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			} else {
				return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			}
		}

		return $boolIsValid;
	}

	public function insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getApplicationId() ) ) {
			return ( false == $boolReturnSqlOnly ) ? true : NULL;
		}

		$this->unSerializeAndSetOriginalValues();

		$arrobjApplicantApplicationChanges = [];

		if( CIncomeType::CURRENT_EMPLOYER == $this->getIncomeTypeId() ) {

			if( $this->getAmount() != $this->getOriginalValueByFieldName( 'amount' ) || $this->getFrequencyId() != $this->getOriginalValueByFieldName( 'frequency_id' ) ) {
				$strOriginalValue = number_format( $this->getOriginalValueByFieldName( 'amount' ), 2, '.', ',' ) . ' ( ' . CFrequency::frequencyIdToStr( $this->getOriginalValueByFieldName( 'frequency_id' ) ) . ' ) ';
				$strNewValue = number_format( $this->getAmount(), 2, '.', ',' ) . ' ( ' . CFrequency::frequencyIdToStr( $this->getFrequencyId() ) . ' ) ';

				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::CURRENT_EMPLOYER_SALARY, $strOriginalValue, $strNewValue );
			}

			if( $this->getDateStarted() != $this->getOriginalValueByFieldName( 'date_started' ) ) {
				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::EMPLOYMENT_START_DATE, $this->getOriginalValueByFieldName( 'date_started' ), $this->getDateStarted() );
			}

			if( $this->getContactName() != $this->getOriginalValueByFieldName( 'contact_name' ) ) {
				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::EMPLOYER_CONTACT_NAME, $this->getOriginalValueByFieldName( 'contact_name' ), $this->getContactName() );
			}

			if( $this->getContactPhoneNumber() != $this->getOriginalValueByFieldName( 'contact_phone_number' ) ) {
				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::EMPLOYER_PHONE_NUMBER, $this->getOriginalValueByFieldName( 'contact_phone_number' ), $this->getContactPhoneNumber() );
			}
		}

		$boolIsValid = true;
		$strSql 	 = '';

		if( true == valArr( $arrobjApplicantApplicationChanges ) ) {
			return CApplicantApplicationChanges::bulkInsert( $arrobjApplicantApplicationChanges, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return ( false == $boolReturnSqlOnly ) ? $boolIsValid : $strSql;
	}

	public function createApplicantApplicationChange( $intApplicantApplicationChangeTypeId, $strOldValue, $strNewValue ) {

		$objApplicantApplicationChange = new CApplicantApplicationChange();
		$objApplicantApplicationChange->setCid( $this->getCid() );
		$objApplicantApplicationChange->setApplicationId( $this->getApplicationId() );
		$objApplicantApplicationChange->setApplicantId( $this->getApplicantId() );
		$objApplicantApplicationChange->setApplicantApplicationChangeTypeId( $intApplicantApplicationChangeTypeId );
		$objApplicantApplicationChange->setOldValue( $strOldValue );
		$objApplicantApplicationChange->setNewValue( $strNewValue );

		return $objApplicantApplicationChange;
	}

	public function insertEventsAndApplicantApplicationChangesForAffordable( $intCurrentUserId, $objDatabase, $objPreviousCustomerIncome = NULL ) {

		$arrobjEvents = [];
		$arrstrActions = $this->checkIncomeActionType( $objPreviousCustomerIncome );
		if( false == valArr( $arrstrActions ) ) {
			return true;
		}
		$objAffordableLibrary = $this->prepareAffordableLibrary( $objDatabase, $intCurrentUserId );
		foreach( $arrstrActions as $strAction ) {
			$arrobjEvents[] = $objAffordableLibrary->createEventAndApplicantApplicationChanges( CEventType::INCOME_CHANGE, $objPreviousCustomerIncome, $strAction );
		}
		return $objAffordableLibrary->insertEventsAndApplicantApplicationChanges( $arrobjEvents );
	}

	public function checkIncomeActionType( $objPreviousCustomerIncome = NULL ) {

		$arrstrActions = [];
		if( 'added' == $this->m_strAction ) {
			$this->m_strAction = ( false == valId( $this->getId() ) ) ? 'added' : 'edited';
		}
		if( true == in_array( $this->m_strAction, [ 'deleted', 'terminated', 'added' ] ) ) {
			$arrstrActions[] = $this->m_strAction . '_income';
		} else {
			if( true == valId( $this->getAmount() ) && $objPreviousCustomerIncome->getAnnualIncomeAmount() != $this->getAmount() ) {
				$arrstrActions[] = 'edit_income_amount';
			}
			if( $objPreviousCustomerIncome->getIncomeTypeId() != $this->getIncomeTypeId() ) {
				$arrstrActions[] = 'edit_income_type';
			}
			if( $objPreviousCustomerIncome->getFrequencyId() != $this->getFrequencyId() ) {
				$arrstrActions[] = 'edit_income_frequency';
			}
			if( $objPreviousCustomerIncome->getIncomeEffectiveDate() != $this->getIncomeEffectiveDate() ) {
				$arrstrActions[] = 'edit_income_effective_date';
			}
			if( 1 > \Psi\Libraries\UtilFunctions\count( $arrstrActions ) && false == in_array( $arrstrActions, [ 'edit_income_amount', 'edit_income_type', 'edit_income_frequency', 'edit_income_effective_date' ] ) ) {
				return false;
			}
		}
		return $arrstrActions;
	}

	public function prepareAffordableLibrary( $objDatabase, $intCurrentUserId ) {

		$objApplication = CApplications::fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intCurrentUserId, $this->getCid(), $objDatabase );
		$objApplicant   = CApplicants::fetchApplicantByLeaseIdByCustomerIdByCid( $objApplication->getLeaseId(), $this->getCustomerId(), $this->getCid(), $objDatabase );
		$objLease       = \Psi\Eos\Entrata\CLeases::createService()->fetchLeaseByIdByCid( $objApplication->getLeaseId(), $this->getCid(), $objDatabase );

		require_once( PATH_LIBRARY_AFFORDABLE . 'CAffordableLibrary.class.php' );
		$objAffordableLibrary = new CAffordableLibrary();
		$objAffordableLibrary->setDatabase( $objDatabase );
		$objAffordableLibrary->setApplicantId( $objApplicant->getId() );
		$objAffordableLibrary->setApplication( $objApplication );
		$objAffordableLibrary->setCompanyUser( $objCompanyUser );
		$objAffordableLibrary->setCustomerIncome( $this );
		$objAffordableLibrary->setLease( $objLease );

		return $objAffordableLibrary;
	}

}
?>
