<?php

class CConstraintCondition extends CBaseConstraintCondition {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRevenueConstraintId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEvent() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valBaseFormula() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventFormula() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valComparator() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>