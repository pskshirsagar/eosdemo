<?php

class CCustomerPet extends CBaseCustomerPet {

	protected $m_strPetTypeName;
	protected $m_strMoveInDate;
	protected $m_strMoveOutDate;
	protected $m_intDefaultPetTypeId;
	protected $m_intLeaseId;
	protected $m_intRateAmount;
	protected $m_intPetWeightLimit;
	protected $m_intQuoteId;
	protected $m_strCustomerName;
	protected $m_intPetImageId;
	protected $m_intLeaseStatusTypeId;
	protected $m_intNextCustomerPetId;
	protected $m_boolIsCountAgainstMaxPetAmount;
	protected $m_boolUseLeaseStartDate;
	protected $m_boolUseLeaseMoveOutDate;

	public function getPetTypeName() {
		return $this->m_strPetTypeName;
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getMoveOutDate() {
		return $this->m_strMoveOutDate;
	}

	public function getDefaultPetTypeId() {
		return $this->m_intDefaultPetTypeId;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getRateAmount() {
		return $this->m_intRateAmount;
	}

	public function getPetWeightLimit() {
		return $this->m_intPetWeightLimit;
	}

	public function getQuoteId() {
		return $this->m_intQuoteId;
	}

	public function getCustomerName() {
		return $this->m_strCustomerName;
	}

	public function getPetImageId() {
		return $this->m_intPetImageId;
	}

	public function getRecentLog( $objDatabase ) {
		$objCustomerPetLog = CCustomerPetLogs::fetchRecentCustomerPetLogByCustomerIdByCustomerPetIdByCid( $this->getCustomerId(), $this->getId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objCustomerPetLog, 'CCustomerPetLog' ) ) {
			return $objCustomerPetLog;
		}

		return NULL;
	}

	public function getNextCustomerPetId() {
		return $this->m_intNextCustomerPetId;
	}

	public function getLeaseStatusTypeId() {
		return $this->m_intLeaseStatusTypeId;
	}

	public function getIsCountAgainstMaxPetAmount() {
		return $this->m_boolIsCountAgainstMaxPetAmount;
	}

	public function getUseLeaseStartDate() {
		return $this->m_boolUseLeaseStartDate;
	}

	public function getUseLeaseMoveOutDate() {
		return $this->m_boolUseLeaseMoveOutDate;
	}

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['pet_type_name'] ) )			$this->setPetTypeName( $arrmixValues['pet_type_name'] );
		if( true == isset( $arrmixValues['default_pet_type_id'] ) )		$this->setDefaultPetTypeId( $arrmixValues['default_pet_type_id'] );
		if( true == isset( $arrmixValues['lease_id'] ) )				$this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['rate_amount'] ) )				$this->setRateAmount( $arrmixValues['rate_amount'] );
		if( true == isset( $arrmixValues['pet_weight_limit'] ) )		$this->setPetWeightLimit( $arrmixValues['pet_weight_limit'] );
		if( true == isset( $arrmixValues['customer_name'] ) )			$this->setCustomerName( $arrmixValues['customer_name'] );
		if( true == isset( $arrmixValues['pet_image_id'] ) )			$this->setPetImageId( $arrmixValues['pet_image_id'] );
		if( true == isset( $arrmixValues['move_in_date'] ) )			$this->setMoveInDate( $arrmixValues['move_in_date'] );
		if( true == isset( $arrmixValues['move_out_date'] ) )			$this->setMoveOutDate( $arrmixValues['move_out_date'] );
		if( true == isset( $arrmixValues['lease_status_type_id'] ) )	$this->setLeaseStatusTypeId( $arrmixValues['lease_status_type_id'] );
		if( true == isset( $arrmixValues['next_customer_pet_id'] ) )	$this->setNextCustomerPetId( $arrmixValues['next_customer_pet_id'] );
		if( true == isset( $arrmixValues['pet_max_allowed'] ) ) {
			( CPetRateAssociation::PETS_ALLOWED_AGAINST_COUNT == $arrmixValues['pet_max_allowed'] )? $this->setIsCountAgainstMaxPetAmount( false ) :  $this->setIsCountAgainstMaxPetAmount( true );
		}
		if( true == isset( $arrmixValues['use_lease_start_date'] ) )    $this->setUseLeaseStartDate( $arrmixValues['use_lease_start_date'] );
		if( true == isset( $arrmixValues['use_lease_move_out_date'] ) ) $this->setUseLeaseMoveOutDate( $arrmixValues['use_lease_move_out_date'] );
		if( true == isset( $arrmixValues['quote_id'] ) ) $this->setQuoteId( $arrmixValues['quote_id'] );
	}

	public function setPetTypeName( $strPetTypeName ) {
		$this->m_strPetTypeName = $strPetTypeName;
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->m_strMoveInDate = $strMoveInDate;
	}

	public function setMoveOutDate( $strMoveOutDate ) {
		$this->m_strMoveOutDate = $strMoveOutDate;
	}

	public function setDefaultPetTypeId( $intDefaultPetTypeId ) {
		$this->m_intDefaultPetTypeId = $intDefaultPetTypeId;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setRateAmount( $intRateAmount ) {
		$this->m_intRateAmount = $intRateAmount;
	}

	public function setPetWeightLimit( $intPetWeightLimit ) {
		$this->m_intPetWeightLimit = $intPetWeightLimit;
	}

	public function setQuoteId( $intQuoteId ) {
		$this->m_intQuoteId = $intQuoteId;
	}

	public function setCustomerName( $strCustomerName ) {
		$this->m_strCustomerName = $strCustomerName;
	}

	public function setPetImageId( $intPetImageId ) {
		$this->m_intPetImageId = $intPetImageId;
	}

	public function setLeaseStatusTypeId( $intLeaseStatusTypeId ) {
		$this->m_intLeaseStatusTypeId = $intLeaseStatusTypeId;
	}

	public function setNextCustomerPetId( $intNextCustomerPetId ) {
		$this->m_intNextCustomerPetId = $intNextCustomerPetId;
	}

	public function setIsCountAgainstMaxPetAmount( $boolIsCountAgainstMaxPetAmount ) {
		$this->m_boolIsCountAgainstMaxPetAmount = $boolIsCountAgainstMaxPetAmount;
	}

	public function setUseLeaseStartDate( $boolUseLeaseStartDate ) {
		$this->m_boolUseLeaseStartDate = CStrings::strToBool( $boolUseLeaseStartDate );
		return $this;
	}

	public function setUseLeaseMoveOutDate( $boolUseLeaseMoveOutDate ) {
		$this->m_boolUseLeaseMoveOutDate = CStrings::strToBool( $boolUseLeaseMoveOutDate );
		return $this;
	}

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCustomerId ) || 0 >= ( int ) $this->m_intCustomerId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Occupant is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPetTypeId( $intPetNumber = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPetTypeId() ) || 0 == $this->getPetTypeId() ) {
			$boolIsValid = false;
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet type is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet type is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_type_id', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		}

		return $boolIsValid;
	}

	public function valName( $boolIsRequired = false, $intPetNumber = NULL, $boolIsValidateRequired = true, $intStepId = CApplicationStep::OPTIONS_AND_FEES ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && true == is_null( $this->getName() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet name is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet name is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_name', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => $intStepId ] ) );
		}

		return $boolIsValid;
	}

	public function valBreed( $boolIsRequired = false, $intPetNumber = NULL, $boolIsValidateRequired = true, $intStepId = CApplicationStep::OPTIONS_AND_FEES ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && true == is_null( $this->getBreed() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet breed is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet breed is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_breed', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => $intStepId ] ) );
		}

		return $boolIsValid;
	}

	public function valColor( $boolIsRequired = false, $intPetNumber = NULL, $boolIsValidateRequired = true, $intStepId = CApplicationStep::OPTIONS_AND_FEES ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && true == is_null( $this->getColor() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet color is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet color is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_color', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => $intStepId ] ) );
		}

		return $boolIsValid;
	}

	public function valWeight( $intPetNumber = NULL, $arrobjPropertyApplicationPreferences, $boolIsValidateRequired = true, $intStepId = CApplicationStep::OPTIONS_AND_FEES, $boolIsValidatePetsWeight = true ) {

		$boolIsValid = true;

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_WEIGHT', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->getWeight() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet weight is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet weight is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_weight', $strErrorMessage, '', [ 'label' => __( 'Pet Weight' ), 'step_id' => $intStepId ] ) );
		}

		// MEGARATES_COMMENTS: fetch pet rate associations on unit space as per the migration
		if( false == array_key_exists( 'HIDE_OPTION_PET_WEIGHT', $arrobjPropertyApplicationPreferences ) && true == $boolIsValidatePetsWeight ) {
			if( false == is_null( $this->getPetTypeId() ) && '' != $this->getPetTypeId() ) {

				$intWeightLimit 		= $this->getPetWeightLimit();
				$intWeight 				= $this->getWeight();

				if( false == empty( $intWeightLimit ) && false == empty( $intWeight ) && $intWeight > $intWeightLimit ) {
					$boolIsValid = false;
					$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Allowed weight for pet #{%d, 0} is', [ $intPetNumber ] ) : __( 'Allowed weight for pet is' );

					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_weight', $strErrorMessage . ' ' . __( '{%d,0} lbs . ', [ $intWeightLimit ] ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valAge( $intPetNumber = NULL, $boolIsValidateRequired = true, $intStepId = CApplicationStep::OPTIONS_AND_FEES ) {
		$boolIsValid = true;

		if( true == is_null( $this->getAge() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet age is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet age is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_age', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => $intStepId ] ) );
		}

		return $boolIsValid;
	}

	public function valGender( $intPetNumber = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;
		if( true == is_null( $this->getGender() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet gender is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet gender is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_gender', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valLicenseCity( $intPetNumber = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getLicenseCity() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'License City is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'License City is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_license_city', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		}
		return $boolIsValid;
	}

	public function valLicenseNumber( $intPetNumber = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getLicenseNumber() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'License Number is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'License Number is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_license_number', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		}
		return $boolIsValid;
	}

	public function valDateOfLastShots( $boolIsRequired = false, $intPetNumber = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && true == is_null( $this->getDateOfLastShots() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet date of last shot is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet date of last shot is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_of_last_shots', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		} elseif( false == is_null( $this->getDateOfLastShots() ) && false == CValidation::validateISODate( $this->getDateOfLastShots() ) ) {
			$boolIsValid = false;
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Invalid date format in pet date of last shot for pet #{%d, 0}. Date must be in mm/dd/yyyy form. ', [ $intPetNumber ] ) : __( 'Invalid date format for pet date of last shot. Date must be in mm/dd/yyyy form.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date_of_last_shots', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valIsHouseBroken( $intPetNumber = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getIsHouseBroken() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'House Broken is required for pet #{%d, 0} ', [ $intPetNumber ] ) : '.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_house_broken', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		}
		return $boolIsValid;
	}

	public function valIsPetSpayedOrNeutered( $intPetNumber = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getIsPetSpayedOrNeutered() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet spayed or neutered is required for pet #{%d, 0} ', [ $intPetNumber ] ) : '.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_spayed_or_neutered', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		}
		return $boolIsValid;
	}

	public function valDateOfSpayedOrNeutered( $boolIsRequired = false, $intPetNumber = NULL, $boolIsValidateRequired = true ) {

		$boolIsValid = true;

		if( 1 == $this->getIsPetSpayedOrNeutered() ) {
			if( true == $boolIsRequired && true == is_null( $this->getPetSpayedOrNeuteredDate() ) ) {
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet date of spayed or neutered is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet date of spayed or neutered required.' );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_date_of_spayed_or_neutered', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
			} elseif( false == is_null( $this->getPetSpayedOrNeuteredDate() ) && false == CValidation::validateISODate( $this->getPetSpayedOrNeuteredDate() ) ) {
				$boolIsValid = false;
				$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Invalid date format in pet date of sprayed or neutered for pet #{%d, 0}. Date must be in mm/dd/yyyy form. ', [ $intPetNumber ] ) : __( 'Invalid date format for pet date of sprayed or neutered. Date must be in mm/dd/yyyy form.' );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_date_of_spayed_or_neutered', $strErrorMessage ) );
			}
		} elseif( true == $boolIsRequired && ( true == is_null( $this->getIsPetSpayedOrNeutered() ) || 0 != $this->getIsPetSpayedOrNeutered() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Pet date of spayed or neutered is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet date of spayed or neutered required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_date_of_spayed_or_neutered', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		}

		return $boolIsValid;
	}

	public function valOwnersName( $intPetNumber = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == is_null( $this->getOwnersName() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$strErrorMessage = ( 0 < $intPetNumber ) ? __( 'Owner Name is required for pet #{%d, 0} ', [ $intPetNumber ] ) : __( 'Pet gender required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pet_owner_name', $strErrorMessage, '', [ 'label' => __( 'Pets' ), 'step_id' => CApplicationStep::OPTIONS_AND_FEES ] ) );
		}

		return $boolIsValid;
	}

	public function valRequiredFields( $intPetNumber, $arrobjPropertyApplicationPreferences, $boolIsValidateRequired = true, $boolIsValidatePetsWeight = true ) {
		$boolIsValidTemp = true;

		$intStepId = CApplicationStep::OPTIONS_AND_FEES;

		if( true == valArr( $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'SHOW_PETS_ON_ADDNL_INFO', $arrobjPropertyApplicationPreferences ) ) {
			$intStepId = CApplicationStep::ADDITIONAL_INFO;
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_NAME', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valName( true, $intPetNumber, $boolIsValidateRequired, $intStepId );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_COLOR', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valColor( true, $intPetNumber, $boolIsValidateRequired, $intStepId );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_BREED', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valBreed( true, $intPetNumber, $boolIsValidateRequired, $intStepId );
		}

		$boolIsValidTemp &= $this->valWeight( $intPetNumber, $arrobjPropertyApplicationPreferences, $boolIsValidateRequired, $intStepId, $boolIsValidatePetsWeight );

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_AGE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valAge( $intPetNumber, $boolIsValidateRequired, $intStepId );
		}

		return $boolIsValidTemp;
	}

	public function valNewApplicationRequiredFields( $intPetNumber, $arrobjPropertyApplicationPreferences, $boolIsValidateRequired = true ) {
		$boolIsValidTemp = true;

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_GENDER', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valGender( $intPetNumber, $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_SPAYED', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valIsPetSpayedOrNeutered( $intPetNumber, $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_DATE_OF_SPAYED', $arrobjPropertyApplicationPreferences ) || true == array_key_exists( 'SHOW_OPTION_PET_DATE_OF_SPAYED', $arrobjPropertyApplicationPreferences ) || false == is_null( $this->getPetSpayedOrNeuteredDate() ) ) {
			$boolIsValidTemp &= $this->valDateOfSpayedOrNeutered( ( ( true == array_key_exists( 'REQUIRE_OPTION_PET_DATE_OF_SPAYED', $arrobjPropertyApplicationPreferences ) ) ? true : false ), $intPetNumber, $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_DATE_OF_LAST_RABIES_SHOT', $arrobjPropertyApplicationPreferences ) || true == array_key_exists( 'SHOW_OPTION_PET_DATE_OF_LAST_RABIES_SHOT', $arrobjPropertyApplicationPreferences ) || false == is_null( $this->getDateOfLastShots() ) ) {
			$boolIsValidTemp &= $this->valDateOfLastShots( ( ( true == array_key_exists( 'REQUIRE_OPTION_PET_DATE_OF_LAST_RABIES_SHOT', $arrobjPropertyApplicationPreferences ) ) ? true : false ), $intPetNumber, $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_LICENSE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valLicenseNumber( $intPetNumber, $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_LICENSE_CITY', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valLicenseCity( $intPetNumber, $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_OWNER_NAME', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valOwnersName( $intPetNumber, $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_OPTION_PET_HOUSEBROKEN', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValidTemp &= $this->valIsHouseBroken( $intPetNumber, $boolIsValidateRequired );
		}

		return $boolIsValidTemp;
	}

	public function valSpecialProvisions() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPetNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseDates( $objDatabase, $boolIsFromUpdate = NULL ) {

		$objLeaseProcesses	= \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseIdByCid( $this->getLeaseId(), $this->getCid(), $objDatabase );

		$strPetMoveInDate		= strtotime( $this->getMoveInDate() );
		$strPetMoveOutDate		= strtotime( $this->getMoveOutDate() );
		$strLeaseMoveInDate		= strtotime( $objLeaseProcesses->getMoveInDate() );
		$strLeaseMoveOutDate	= strtotime( $objLeaseProcesses->getMoveOutDate() );
		$strCurrentDate			= strtotime( date( 'm/d/Y' ) );

		if( false == $this->getUseLeaseStartDate() && false == valStr( $this->getMoveInDate() ) && false == $boolIsFromUpdate ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date is required' ) ) );
			return false;
		} else {

			if( false == in_array( $this->getLeaseStatusTypeId(), [ CLeaseStatusType::APPLICANT, CLeaseStatusType::FUTURE ] ) && false == $this->getUseLeaseStartDate() && true == valStr( $this->getMoveInDate() ) && $strPetMoveInDate > $strCurrentDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date must be on or before today.' ) ) );
				return false;
			}

			if( false == $this->getUseLeaseStartDate() && true == valStr( $this->getMoveInDate() ) && $strPetMoveInDate < $strLeaseMoveInDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date must be on or after the move-in date for the lease ( {%t, 0, DATE_NUMERIC_STANDARD} ) ', [ $objLeaseProcesses->getMoveInDate() ] ) ) );
				return false;
			}

			if( true == valStr( $objLeaseProcesses->getMoveOutDate() ) && true == valStr( $this->getMoveInDate() ) && $strPetMoveInDate > $strLeaseMoveOutDate ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date must be on or before the move-out date for the lease ( {%t, 0, DATE_NUMERIC_STANDARD} ) ', [ $objLeaseProcesses->getMoveOutDate() ] ) ) );
				return false;
			}

			if( true == valStr( $this->getMoveOutDate() ) ) {

				if( $strPetMoveOutDate > $strCurrentDate ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move-out date must be on or before today.' ) ) );
					return false;
				}

				if( true == valStr( $this->getMoveInDate() ) && $strPetMoveInDate > $strPetMoveOutDate ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_in_date', __( 'Move-in date must be before move-out date.' ) ) );
					return false;
				}

				if( $strLeaseMoveInDate > $strPetMoveOutDate ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move-out date must be on or after the move-in date for the lease ( {%t, 0, DATE_NUMERIC_STANDARD} ) ', [ $objLeaseProcesses->getMoveInDate() ] ) ) );
					return false;
				}

				if( true == valStr( $objLeaseProcesses->getMoveOutDate() ) && $strPetMoveOutDate > $strLeaseMoveOutDate ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'move_out_date', __( 'Move-out date should be on or before the move-out date for the lease ( {%t, 0, DATE_NUMERIC_STANDARD} ) ', [ $objLeaseProcesses->getMoveOutDate() ] ) ) );
					return false;
				}
			}
		}

		return true;
	}

	public function isPetAlreadyExists( $objDatabase ) {
		$boolIsValid = true;

		$objCustomerPet = \Psi\Eos\Entrata\CCustomerPets::createService()->fetchCustomerPetByNameByPetTypeIdByCustomerIdByCid( $this->getName(), $this->getPetTypeId(), $this->getCustomerId(), $this->getCid(), $objDatabase );

		if( true == valObj( $objCustomerPet, 'CCustomerPet' ) ) {

			if( $this->getId() != $objCustomerPet->getId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Pet is already added.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $intPetNumber = NULL, $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequired = true, $boolIsFromUpdate = NULL, $boolIsValidatePetsWeight = true ) {

		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPetTypeId( $intPetNumber );
				$boolIsValid &= $this->valDateOfLastShots();
				$boolIsValid &= $this->valWeight( $intPetNumber, $arrobjPropertyApplicationPreferences );
				$boolIsValid &= $this->valAge();
				$boolIsValid &= $this->valColor( true );
				$boolIsValid &= $this->valLeaseDates( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valPetTypeId( $intPetNumber );
				$boolIsValid &= $this->valDateOfLastShots();
				$boolIsValid &= $this->valWeight( $intPetNumber, $arrobjPropertyApplicationPreferences );
				$boolIsValid &= $this->valAge();
				$boolIsValid &= $this->valColor( true );
				$boolIsValid &= $this->valLeaseDates( $objDatabase, $boolIsFromUpdate );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'validate_insert_pets':
				$boolIsValid &= $this->valPetTypeId( $intPetNumber );
				$boolIsValid &= $this->valRequiredFields( $intPetNumber, $arrobjPropertyApplicationPreferences );
				$boolIsValid &= $this->isPetAlreadyExists( $objDatabase );
				break;

			case 'validate_application_pets':
				$boolIsValid &= $this->valPetTypeId( $intPetNumber );
				if( true == $boolIsValid && true == $this->getIsCountAgainstMaxPetAmount() ) {
					$boolIsValid &= $this->valRequiredFields( $intPetNumber, $arrobjPropertyApplicationPreferences, $boolIsValidateRequired, $boolIsValidatePetsWeight );
					$boolIsValid &= $this->valNewApplicationRequiredFields( $intPetNumber, $arrobjPropertyApplicationPreferences, $boolIsValidateRequired );
				}
				break;

			case 'validate_pre_qual_pets':
				$boolIsValid &= $this->valPetTypeId( $intPetNumber );
				if( true == array_key_exists( 'REQUIRE_OPTION_PET_BREED', $arrobjPropertyApplicationPreferences ) && true == $boolIsValid && true == $this->getIsCountAgainstMaxPetAmount() ) {
					$boolIsValid &= $this->valBreed( true, $intPetNumber );
				}
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valId( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>