<?php

class CReviewTransmissionVendor extends CBaseReviewTransmissionVendor {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransmissionVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemoteReviewKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePageKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>