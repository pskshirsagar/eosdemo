<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyAppointmentHours
 * Do not add any new functions to this class.
 */

class CPropertyAppointmentHours extends CBasePropertyAppointmentHours {

	public static function fetchCustomPropertyAppointmentHoursByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchPropertyAppointmentHours( 'SELECT * FROM property_appointment_hours WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY day', $objDatabase );
	}

	public static function fetchCustomPropertyAppointmentHoursByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM property_appointment_hours WHERE property_id IN (' . implode( ',', $arrintPropertyIds ) . ') AND cid = ' . ( int ) $intCid . ' ORDER BY day ';
		$arrobjPropertyAppointmentHours = self::fetchPropertyAppointmentHours( $strSql, $objDatabase );
		return $arrobjPropertyAppointmentHours;
	}
}
?>