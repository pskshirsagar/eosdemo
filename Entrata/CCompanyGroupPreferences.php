<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyGroupPreferences
 * Do not add any new functions to this class.
 */

class CCompanyGroupPreferences extends CBaseCompanyGroupPreferences {

	public static function fetchOrderedGroupPreferencesByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objDatabase, $strOrderBy = '' ) {

		$strSql = 'SELECT
					*
					FROM
						company_group_preferences
					WHERE
						company_group_id = ' . ( int ) $intCompanyGroupId . '
					AND
						cid = ' . ( int ) $intCid;

		if( 0 < strlen( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . $strOrderBy;
		}

		return self::fetchCompanyGroupPreferences( $strSql, $objDatabase );

	}

	public static function fetchCompanyGroupPreferencesByCompanyGroupIdByKeysByCid( $intCompanyGroupId, $arrstrKeys, $intCid, $objDatabase, $boolIsCheckValue ) {

		if ( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						company_group_preferences
					WHERE
						company_group_id = ' . ( int ) $intCompanyGroupId . '
						AND key IN ( \'' . implode( '\',\'',  $arrstrKeys ) . '\')
						AND cid = ' . ( int ) $intCid;

		$strSql .= ( true == $boolIsCheckValue ) ? ' AND value IS NOT NULL' : '';

		return self::fetchCompanyGroupPreferences( $strSql, $objDatabase );
	}

	public static function fetchMaxCompanyGroupPreferencesByCompanyUserIdByKeysByModuleIdByCid( $intCompanyUserId, $arrstrUserPreferenceKeys, $intModuleId, $intCid, $objDatabase ) {

		if ( false == valArr( $arrstrUserPreferenceKeys ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON (
										sub_query.key,
										sub_query.value
									) sub_query.*
					FROM (
							SELECT
								 cgp.*,
								\'1\' AS is_allowed,
								rank() OVER( PARTITION BY cgp.key ORDER BY ( CASE WHEN cgp.value < \'A\' THEN lpad( cgp.value, 1024, \'0\' ) ELSE cgp.value END ) DESC, cgp.cid ) as rank
							FROM
								company_group_preferences cgp
								JOIN company_user_groups cug ON ( cgp.company_group_id = cug.company_group_id AND cgp.cid = cug.cid )
								JOIN company_group_permissions cgpe ON ( cug.company_group_id = cgpe.company_group_id AND cgpe.is_allowed = 1 AND cug.cid = cgpe.cid )
							WHERE
								cug.company_user_id = ' . ( int ) $intCompanyUserId . '
								AND cgpe.cid = ' . ( int ) $intCid . '
								AND cgp.key IN ( \'' . implode( '\',\'',  $arrstrUserPreferenceKeys ) . '\')
								AND cgpe.module_id = ' . ( int ) $intModuleId . '
						) as sub_query
					WHERE
						sub_query.rank = 1 AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyGroupPreferences( $strSql, $objDatabase );
	}

	public static function fetchCompanyGroupPreferenceEntrataTimeoutByCompanyUserIdByCid( $intUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						MIN(cgp.value::INTEGER) as value
					FROM
						company_group_preferences cgp 
						JOIN company_user_groups cug ON ( cug.company_group_id = cgp.company_group_id AND cug.cid = cgp.cid )
					WHERE
						cgp.key = \'CUSTOM_ENTRATA_TIMEOUT\'
						AND cgp.cid = ' . ( int ) $intCid . '
						AND cgp.value IS NOT NULL
						AND cug.company_user_id = ' . ( int ) $intUserId;

		return parent::fetchColumn( $strSql, 'value', $objDatabase );
	}

	public static function fetchCompanyGroupPreferencesByCompanyGroupIdsByCid( $arrintCompanyGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cgp.key
					FROM
						company_group_preferences cgp
					WHERE
						cgp.company_group_id IN ( \'' . implode( '\',\'',  $arrintCompanyGroupId ) . '\')
						AND cgp.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

}
?>