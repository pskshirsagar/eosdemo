<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportHistories
 * Do not add any new functions to this class.
 */

class CReportHistories extends CBaseReportHistories {

	public static function fetchReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return self::fetchObjects( $strSql, 'CReportHistory', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchReportHistory( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CReportHistory', $objDatabase );
	}

	public static function fetchRecentReportHistoriesByReportIdByCid( $intReportId, $intCid, $objDatabase, $intLimit = NULL, $intOffset = NULL ) {
		$strSql = '
			SELECT
				rh.*,
				rf.name AS report_filter_name
			FROM
				report_histories rh
				JOIN report_filters rf ON rf.cid = rh.cid AND rf.id = rh.report_filter_id
			WHERE
				rh.cid = ' . ( int ) $intCid . '
				AND rf.report_id = ' . ( int ) $intReportId . '
			ORDER BY
				rh.created_on DESC
			' . ( false == is_null( $intLimit ) && false == is_null( $intOffset ) ? ' LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset : '' );

		return CBaseReportHistories::fetchReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );
	}

	/**
	 * @param $strCorrelationId
	 * @param $intReportFilterId
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReportHistory
	 */
	public static function fetchReportHistoryByCorrelationIdByReportFilterIdByCid( $strCorrelationId, $intReportFilterId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				rh.*,
				f.file_name AS filename
			FROM
				report_histories rh
				JOIN company_user_report_histories curh ON curh.cid = rh.cid AND curh.report_history_id = rh.id
				LEFT JOIN file_associations fa ON fa.cid = curh.cid AND fa.company_user_report_history_id = curh.id
				LEFT JOIN files f ON f.cid = fa.cid AND f.id = fa.file_id
				LEFT JOIN file_extensions fe on fe.id = f.file_extension_id
			WHERE
				rh.cid = ' . ( int ) $intCid . '
				AND rh.report_filter_id ' . ( is_null( $intReportFilterId ) ? 'IS NULL' : '= ' . ( int ) $intReportFilterId ) . '
				AND rh.correlation_id = \'' . $strCorrelationId . '\'';

		return self::fetchReportHistory( $strSql, $objDatabase ) ?: NULL;
	}

	/**
	 * @param $strCorrelationId
	 * @param $intCid
	 * @param $objDatabase
	 * @return array
	 */
	public static function fetchReportHistoriesByCorrelationIdByCid( $strCorrelationId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				rh.*,
				f.file_name AS filename,
				f.file_path AS filePath,
				f.id AS file_id
			FROM
				report_histories rh
				JOIN company_user_report_histories curh ON curh.cid = rh.cid AND curh.report_history_id = rh.id
				LEFT JOIN file_associations fa ON fa.cid = curh.cid AND fa.company_user_report_history_id = curh.id
				LEFT JOIN files f ON f.cid = fa.cid AND f.id = fa.file_id
				LEFT JOIN file_extensions fe on fe.id = f.file_extension_id
			WHERE
				rh.cid = ' . ( int ) $intCid . '
				AND rh.correlation_id = \'' . $strCorrelationId . '\'';

		return self::fetchReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) ?: [];
	}

	public static function fetchReportHistoriesByCorrelationIdsByCid( $arrstrCorrelationId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				rh.*
			FROM
				report_histories rh
			WHERE
				rh.cid = ' . ( int ) $intCid . '
				AND rh.correlation_id IN( \'' . str_replace( ',', '\',\'', str_replace( '\'', '', implode( ',', $arrstrCorrelationId ) ) ) . '\' )
		';

		return self::fetchReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) ?: [];
	}

	/**
	 * @param $intCid
	 * @param $intCompanyUserId
	 * @param $objDatabase
	 * @param null $strReportName
	 * @param array $arrstrStatus
	 * @param $arrstrDateRange
	 * @param int $intOffset
	 * @param int $intLimit
	 * @param string $strSortBy
	 * @param CReportHistory[] $arrobjTempReportHistories
	 * @return CReportHistory[]
	 */
	public static function fetchManualReportHistoriesByCidByCompanyUserId( $intCid, $intCompanyUserId, $objDatabase, $strReportName = NULL, $arrstrStatus = [], $arrstrDateRange, $intOffset = 0, $intLimit = 50, $strSortBy = '', $arrobjTempReportHistories ) {

		$arrstrConditions = [ 'TRUE' ];
		$strReportName = getSanitizedFormField( $strReportName, true );
		if( false == empty( trim( $strReportName ) ) ) {
			$strReportName = str_replace( ' ', '_', \Psi\CStringService::singleton()->strtolower( $strReportName ) );
			$arrstrConditions[] = 'rh.name ILIKE \'%' . trim( $strReportName ) . '%\'';
		}

		if( true == valArr( $arrstrStatus ) ) {
			$arrstrConditions[] = 'rh.status IN( \'' . implode( '\', \'', $arrstrStatus ) . '\' )';
		}

		if( true == isset( $arrstrDateRange['min-date'] ) && false == empty( $arrstrDateRange['min-date'] ) ) {
			$arrstrConditions[] = 'date_trunc( \'day\', rh.queued_datetime ) >= \'' . $arrstrDateRange['min-date'] . '\'';
		}

		if( true == isset( $arrstrDateRange['max-date'] ) && false == empty( $arrstrDateRange['max-date'] ) ) {
			$arrstrConditions[] = 'date_trunc( \'day\', rh.queued_datetime ) <= \'' . $arrstrDateRange['max-date'] . '\'';
		}

		if( false == empty( $strSortBy ) ) {
			$strSortBy .= ', rh.queued_datetime DESC';
		}

		$strAdditionalConditions = implode( ' AND ', $arrstrConditions );

		$strSql = ' DROP TABLE
					IF EXISTS pg_temp.temp_report_histories;
			CREATE TEMP TABLE pg_temp.temp_report_histories( LIKE report_histories );

			' . str_replace( 'public.report_histories(', 'pg_temp.temp_report_histories(', CReportHistories::bulkInsert( $arrobjTempReportHistories, $intCompanyUserId, $objDatabase, true ) ) . '

			ANALYZE pg_temp.temp_report_histories;

			SELECT
				rh.*,
				COALESCE( rv.major, 1 ) AS major,
				COALESCE( rv.minor, 0 ) AS minor
			FROM
				(
					SELECT
						rh.*,
						curh.id AS company_user_report_history_id
					FROM
						report_histories rh
						JOIN company_user_report_histories curh ON ( curh.cid = rh.cid AND curh.report_history_id = rh.id )
					WHERE
						rh.cid = ' . ( int ) $intCid . '
						AND COALESCE( rh.download_options->>\'self_destruct\', \'false\' )::BOOL = false
						AND curh.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND curh.is_temporary = true
						AND rh.report_schedule_type_id = ' . CReportScheduleType::MANUAL . '
						AND rh.report_new_instance_id IS NULL
						AND ( rh.details ? \'is_from_library\' ) = false

					UNION ALL

					SELECT
						rh.*,
						NULL AS company_user_report_history_id
					FROM
						pg_temp.temp_report_histories rh
						LEFT JOIN report_histories rh2 ON ( rh2.cid = rh.cid AND rh2.correlation_id = rh.correlation_id )
					WHERE
						COALESCE( rh.download_options->>\'self_destruct\', \'false\' )::BOOL = false
						AND rh2.id IS NULL
						AND rh.report_schedule_type_id = ' . CReportScheduleType::MANUAL . '
				) rh
				LEFT JOIN report_versions rv ON ( rv.cid = rh.cid AND rv.id = rh.report_version_id )
			WHERE
				' . $strAdditionalConditions . '
			ORDER BY
				' . $strSortBy . '
			LIMIT
				' . ( int ) $intLimit . '
			OFFSET
				' . ( int ) $intOffset . '
		';

		return CReportHistories::fetchReportHistories( $strSql, $objDatabase, false ) ?: [];
	}

	public static function fetchNewManualReportHistoriesByCidByCompanyUserId( $intCid, $intCompanyUserId, $objDatabase, $strReportName = NULL, $arrstrStatus = [], $arrstrDateRange, $intOffset = 0, $intLimit = 50, $strSortBy = '', $arrobjTempReportHistories ) {
		$arrstrConditions	= [ 'TRUE' ];
		$strSqlReportJoin	= '';
		$strReportName		= getSanitizedFormField( $strReportName, true );

		if( !empty( trim( $strReportName ) ) ) {
			$strSqlReportJoin		= ' LEFT JOIN reports r ON r.cid = rh.cid AND r.id = rh.report_id ';
			$strReportName			= str_replace( ' ', '_', \Psi\CStringService::singleton()->strtolower( $strReportName ) );
			$arrstrConditions[]		= '( rh.name ILIKE \'%' . trim( $strReportName ) . '%\' OR r.title ILIKE \'%' . trim( $strReportName ) . '%\' )';
		}

		if( valArr( $arrstrStatus ) ) {
			$arrstrConditions[] = 'rh.status IN( \'' . implode( '\', \'', $arrstrStatus ) . '\' )';
		}

		if( isset( $arrstrDateRange['min-date'] ) && !empty( $arrstrDateRange['min-date'] ) ) {
			$arrstrConditions[] = 'date_trunc( \'day\', rh.queued_datetime ) >= \'' . $arrstrDateRange['min-date'] . '\'';
		}

		if( isset( $arrstrDateRange['max-date'] ) && !empty( $arrstrDateRange['max-date'] ) ) {
			$arrstrConditions[] = 'date_trunc( \'day\', rh.queued_datetime ) <= \'' . $arrstrDateRange['max-date'] . '\'';
		}

		if( !empty( $strSortBy ) ) {
			$strSortBy .= ', rh.queued_datetime DESC';
		}

		$strAdditionalConditions = implode( ' AND ', $arrstrConditions );

		$strSql = ' DROP TABLE
					IF EXISTS pg_temp.temp_report_histories;
					CREATE TEMP TABLE pg_temp.temp_report_histories( LIKE report_histories );

					' . str_replace( 'public.report_histories(', 'pg_temp.temp_report_histories(', CReportHistories::bulkInsert( $arrobjTempReportHistories, $intCompanyUserId, $objDatabase, true ) ) . '

					ANALYZE pg_temp.temp_report_histories;
		
					SELECT
						rh.*,
						COALESCE( rv.major, 1 ) AS major,
						COALESCE( rv.minor, 0 ) AS minor,
						CASE
							WHEN rh.report_new_instance_id IS NOT NULL
							THEN util_get_translated( \'name\', ri.name, ri.details )
							ELSE left( rh.name, strpos( rh.name, \'-\' ) - 1 )
						END AS instance_name
					FROM
						(
							SELECT
								rh.*,
								curh.id AS company_user_report_history_id,
								curh.company_user_id
							FROM
								report_histories rh
								JOIN company_user_report_histories curh ON ( curh.cid = rh.cid AND curh.report_history_id = rh.id )
							WHERE
								rh.cid = ' . ( int ) $intCid . '
								AND COALESCE( rh.download_options->>\'self_destruct\', \'false\' )::BOOL = false
								AND curh.company_user_id = ' . ( int ) $intCompanyUserId . '
								AND curh.is_temporary = true
								AND rh.report_schedule_type_id = ' . CReportScheduleType::MANUAL . '
								AND ( rh.report_new_instance_id IS NOT NULL OR ( rh.details ? \'is_from_library\' ) = TRUE )
		
							UNION ALL
		
							SELECT
								rh.*,
								NULL AS company_user_report_history_id,
								NULL AS company_user_id
							FROM
								pg_temp.temp_report_histories rh
								LEFT JOIN report_histories rh2 ON rh2.cid = rh.cid AND rh2.correlation_id = rh.correlation_id
							WHERE
								COALESCE( rh.download_options->>\'self_destruct\', \'false\' )::BOOL = false
								AND rh2.id IS NULL
								AND rh.report_schedule_type_id = ' . CReportScheduleType::MANUAL . '
						) rh
						LEFT JOIN report_new_instances ri ON ri.cid = rh.cid AND ri.id = rh.report_new_instance_id
						LEFT JOIN report_versions rv ON rv.cid = rh.cid AND rv.id = rh.report_version_id
						' . $strSqlReportJoin . '
					WHERE
						' . $strAdditionalConditions . '
					ORDER BY
						' . $strSortBy . '
					LIMIT
						' . ( int ) $intLimit . '
					OFFSET
						' . ( int ) $intOffset . '
				';

		return CReportHistories::fetchReportHistories( $strSql, $objDatabase, false ) ?: [];
	}

	/**
	 * @param $strCorrelationId
	 * @param $objDatabase
	 * @return array
	 */
	public static function fetchReportHistoriesByCorrelationId( $strCorrelationId, $objDatabase ) {
		$strSql = '
			SELECT
				rh.*,
				f.file_path AS filename
			FROM
				report_histories rh
				JOIN company_user_report_histories curh ON curh.cid = rh.cid AND curh.report_history_id = rh.id
				LEFT JOIN file_associations fa ON fa.cid = curh.cid AND fa.company_user_report_history_id = curh.id
				LEFT JOIN files f ON f.cid = fa.cid AND f.id = fa.file_id
				LEFT JOIN file_extensions fe on fe.id = f.file_extension_id
			WHERE
				rh.correlation_id = \'' . $strCorrelationId . '\'';

		return self::fetchReportHistories( $strSql, $objDatabase ) ?: [];
	}

	public static function fetchOrphanedReportHistories( $objDatabase ) {
		$strSql = '
			SELECT
				rh.cid,
				rh.id AS report_history_id
			FROM
				report_histories rh
				LEFT JOIN company_user_report_histories curh ON rh.cid = curh.cid AND rh.id = curh.report_history_id
			WHERE
				curh.id IS NULL;
		';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUsersWithReportHistoriesByCid( $intCid, $objDatabase, $boolIsPSIUser ) {

		$strPSIUserConditionSql = ( $boolIsPSIUser == false ) ? ' AND cu.id > 10000 ' : '';
		$strSql = '
			SELECT
				cu.id, 
				CASE WHEN ce.id IS NOT NULL THEN ce.name_first || \' \' || ce.name_last
					ELSE cu.username
				END AS created_by
			FROM
				company_users AS cu
				LEFT JOIN company_employees AS ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
			WHERE
				cu.cid = ' . ( int ) $intCid . '
				' . $strPSIUserConditionSql . '
				AND cu.id IN( 
								SELECT 
									rh.created_by
								FROM 
									report_histories AS rh 
								WHERE 
									rh.cid = ' . ( int ) $intCid . ' 
									AND rh.status IS NOT NULL 
								GROUP BY 
									rh.created_by
							)
			ORDER BY
				created_by';

		$arrmixData = fetchData( $strSql, $objDatabase );

		$arrmixCompanyUsers = extractArrayKeyValuePairs( $arrmixData, 'id', 'created_by' );

		return $arrmixCompanyUsers;
	}

	public static function fetchReportHistoryByIdOrByCorrelationIdByCid( $intReportHistoryId, $strCorrelationId, $intCid, $objDatabase ) {

		$strHistoryIdCondition = '';
		if( valId( $intReportHistoryId ) ) {
			$strHistoryIdCondition = ' AND rh.id = ' . ( int ) $intReportHistoryId;
		}
		$strSql = '
			SELECT
				rh.*,
				f.file_name AS filename,
				f.file_path AS filePath,
				f.id AS file_id
			FROM
				report_histories AS rh
				JOIN company_user_report_histories AS curh ON ( curh.cid = rh.cid AND curh.report_history_id = rh.id )
				LEFT JOIN file_associations AS fa ON ( fa.cid = curh.cid AND fa.company_user_report_history_id = curh.id )
				LEFT JOIN files AS f ON ( f.cid = fa.cid AND f.id = fa.file_id )
				LEFT JOIN file_extensions AS fe ON ( fe.id = f.file_extension_id )
			WHERE
				rh.cid = ' . ( int ) $intCid . '
				' . $strHistoryIdCondition . '
				AND rh.correlation_id = \'' . $strCorrelationId . '\'';

		if( !valId( $intReportHistoryId ) ) {
			return self::fetchReportHistories( $strSql, $objDatabase ) ? reset( self::fetchReportHistories( $strSql, $objDatabase ) ) : NULL;
		} else {
			return self::fetchReportHistory( $strSql, $objDatabase ) ?: NULL;
		}

	}

	public static function fetchReportHistoryByCorrelationIdByCid( $strCorrelationId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				rh.*
			FROM
				report_histories AS rh
			WHERE
				rh.cid = ' . ( int ) $intCid . '
				AND rh.correlation_id = \'' . $strCorrelationId . '\'';

		return self::fetchReportHistory( $strSql, $objDatabase ) ?: [];
	}

	/**
	 * @param string $strCorrelationId
	 * @param int $intCompanyUserId
	 * @param int $intCid
	 * @param \CDatabase $objDatabase
	 * @return array
	 */
	public static function fetchReportHistoryCacheFileByCorrelationIdByCid( string $strCorrelationId, int $intCompanyUserId, int $intCid, \CDatabase $objDatabase ) {
		$strSql = '
			SELECT
				DISTINCT ON (f.id) rh.id,
				f.file_name,
				f.file_path,
				f.id AS file_id,
				fa.id AS file_association_id
			FROM
				report_histories AS rh
				JOIN company_user_report_histories AS curh ON curh.cid = rh.cid AND curh.report_history_id = rh.id AND curh.company_user_id = ' . ( int ) $intCompanyUserId . '
				LEFT JOIN file_associations AS fa ON fa.cid = curh.cid AND fa.company_user_report_history_id = curh.id
				LEFT JOIN files AS f ON f.cid = fa.cid AND f.id = fa.file_id
				LEFT JOIN file_extensions AS fe on fe.id = f.file_extension_id
			WHERE
				rh.cid = ' . ( int ) $intCid . '
				AND rh.correlation_id = \'' . $strCorrelationId . '\'
				AND COALESCE( f.details ->> \'is_cached_dataset_file\', \'false\' )::BOOL = TRUE';

		return fetchData( $strSql, $objDatabase ) ?: [];
	}

}