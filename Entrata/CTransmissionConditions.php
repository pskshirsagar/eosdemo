<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTransmissionConditions
 * Do not add any new functions to this class.
 */

class CTransmissionConditions extends CBaseTransmissionConditions {

	public static function fetchTransmissionConditionsByCidByApplicationId( $intCid, $intApplicationId, $objDatabase, $boolFetchAll = false ) {

		$strSql = 'SELECT
						tc.*,
						tct.name as transmission_condition_name
				   	FROM
						transmission_conditions as tc
				   		JOIN transmission_condition_types as tct ON ( tc.transmission_condition_type_id = tct.id )
				   	WHERE
						tc.cid = ' . ( int ) $intCid . '
				   		AND tc.application_id = ' . ( int ) $intApplicationId;

	   	if( false == $boolFetchAll ) {
	   		$strSql .= ' AND tc.deleted_by IS NULL
	   		  			 AND tc.deleted_on IS NULL ';
		}
		return self::fetchTransmissionConditions( $strSql, $objDatabase );
	}

	public static function fetchTransmissionConditionsByCidByApplicationIdById( $intCid, $intApplicationId, $intTransmissionConditionId, $objDatabase ) {

		$strSql = 'SELECT * FROM
						transmission_conditions
				   	WHERE
						cid = ' . ( int ) $intCid . '
				   		AND application_id = ' . ( int ) $intApplicationId . '
				   		AND	id = ' . ( int ) $intTransmissionConditionId . '
				   		AND deleted_by IS NULL
				   		AND deleted_on IS NULL';

		return self::fetchTransmissionCondition( $strSql, $objDatabase );
	}

	public static function fetchTransmissionConditionsByCidByApplicationIdByTransmissionConditionTypeIds( $intCid, $intApplicationId, $arrintTransmissionConditionTypeIds, $objDatabase ) {
		if( false == valArr( $arrintTransmissionConditionTypeIds ) ) return;

		$strSql = 'SELECT * FROM
						transmission_conditions
				   	WHERE
						cid = ' . ( int ) $intCid . '
				   		AND application_id = ' . ( int ) $intApplicationId . '
				   		AND transmission_condition_type_id IN ( ' . implode( ',', $arrintTransmissionConditionTypeIds ) . ' )
				   		AND deleted_by IS NULL
				   		AND deleted_on IS NULL
				   	ORDER BY
				   		transmission_condition_type_id';

		return self::fetchTransmissionConditions( $strSql, $objDatabase );
	}

	public static function fetchTransmissionConditionsByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $boolFetchAll = false ) {
		if( false == valArr( $arrintApplicationIds ) ) return NULL;

		$strSql = 'SELECT
						tc.*,
						tct.name as transmission_condition_name
				   	FROM
						transmission_conditions as tc
				   		JOIN transmission_condition_types as tct ON ( tc.transmission_condition_type_id = tct.id )
				   	WHERE
						tc.cid = ' . ( int ) $intCid . '
				   		AND tc.application_id IN( ' . implode( ',', $arrintApplicationIds ) . ')';

		if( false == $boolFetchAll ) {
			$strSql .= ' AND tc.deleted_by IS NULL
	   		  			 AND tc.deleted_on IS NULL ';
		}

		return self::fetchTransmissionConditions( $strSql, $objDatabase );
	}
}
?>