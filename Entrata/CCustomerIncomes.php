<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerIncomes
 * Do not add any new functions to this class.
 */

class CCustomerIncomes extends CBaseCustomerIncomes {

	public static function fetchCustomerIncome( $strSql, $objDatabase, $strClassName = 'CCustomerIncome' ) {
		return parent::fetchObject( $strSql, $strClassName, $objDatabase );
	}

	public static function fetchCustomerIncomes( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CCustomerIncome', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchCustomerIncomeByIncomeTypeIdByCustomerIdByCid( $intIncomeTypeId, $intCustomerId, $intCid, $objDatabase, $intApplicationId = NULL, $strApplicationLockedOn = NULL ) {
		if( false == valId( $intApplicationId ) && true == valStr( $strApplicationLockedOn ) ) {
			$strSql = CCustomerIncomeLogs::fetchApplicationCustomerIncomeLogsByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase, $intApplicationId, $boolReturnSqlOnly = true );

			return self::fetchCustomerIncome( $strSql, $objDatabase, [ 'CCustomerIncome', 'fromLogToIncome' ] );
		}

		return self::fetchCustomerIncome( sprintf( 'SELECT * FROM %s WHERE customer_id = %d AND income_type_id = %d AND cid = %d AND deleted_by IS NULL AND deleted_on IS NULL ORDER BY amount DESC LIMIT 1', 'customer_incomes', ( int ) $intCustomerId, ( int ) $intIncomeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeByIdByIncomeTypeIdByCid( $intId, $intIncomeTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomes( sprintf( 'SELECT * FROM %s WHERE id = %d AND income_type_id = %d AND cid = %d AND deleted_by IS NULL AND deleted_on IS NULL', 'customer_incomes', ( int ) $intId, ( int ) $intIncomeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomeByIdsByIncomeTypeIdByCid( $arrintIds, $intIncomeTypeId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomes( sprintf( 'SELECT * FROM %s WHERE id IN (' . implode( ',', $arrintIds ) . ') AND income_type_id = %d AND cid = %d AND deleted_by IS NULL AND deleted_on IS NULL', 'customer_incomes', ( int ) $intIncomeTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {
		return self::fetchCustomerIncomes( sprintf( 'SELECT 
																*,
																CASE
							                                        WHEN date_ended < CURRENT_DATE THEN \'' . __( 'Past' ) . '\'
							                                        WHEN ( income_effective_date IS NULL AND date_started > CURRENT_DATE ) OR ( income_effective_date > CURRENT_DATE ) THEN \'' . __( 'Future' ) . '\'
							                                        ELSE \'' . __( 'Current' ) . '\'
                                                                END as status 
															FROM %s WHERE customer_id = %d AND cid = %d AND deleted_by IS NULL AND deleted_on IS NULL', 'customer_incomes', ( int ) $intCustomerId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdByIncomeTypeIdsByCid( $intCustomerId, $arrintIncomeTypeIds, $intCid, $objDatabase, $boolIsShowCategorizedInfo = false ) {
		if( false == valArr( $arrintIncomeTypeIds ) ) return NULL;

		$strCaseStatement = '';
		$strOrderBy = ' ORDER BY ci.income_effective_date';
		if( true == $boolIsShowCategorizedInfo ) {
			$strOrderBy = ' ORDER BY subq.status, subq.income_effective_date';
			$strCaseStatement = ' ,CASE
							        WHEN ci.date_ended < CURRENT_DATE THEN \'' . __( 'Past' ) . '\'
							        WHEN ( ci.income_effective_date IS NULL AND ci.date_started > CURRENT_DATE ) OR ( ci.income_effective_date > CURRENT_DATE ) THEN \'' . __( 'Future' ) . '\'
							        ELSE \'' . __( 'Current' ) . '\'
                                END as status';
		}

		$strSql = 'SELECT
						it.name as income_name,
						ci.* ' . $strCaseStatement . '
					FROM
						customer_incomes ci
						JOIN income_types it ON( ci.income_type_id = it.id)
					WHERE
						ci.customer_id = ' . ( int ) $intCustomerId . '
						AND ci.cid = ' . ( int ) $intCid . '
						AND ci.income_type_id IN ( ' . implode( ',', $arrintIncomeTypeIds ) . ' )
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		$strSql = ( true == $boolIsShowCategorizedInfo ) ? 'SELECT * FROM ( ' . $strSql . ' ) as subq ' . $strOrderBy : $strSql . $strOrderBy;
		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdsExcludingIncomeTypeIdsByCid( $arrintCustomerIds, $arrintIncomeTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == valArr( $arrintIncomeTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM customer_incomes WHERE customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )  AND income_type_id NOT IN ( ' . implode( ',', $arrintIncomeTypeIds ) . ' ) AND cid = ' . ( int ) $intCid . ' AND deleted_by IS NULL AND deleted_on IS NULL ORDER BY amount DESC';
		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchTotalMonthlyAmountDataByCidByCustomerIdsByIncomeTypeIds( $intCid, $arrintCustomerIds, $arrintIncomeTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;
		if( false == valArr( $arrintIncomeTypeIds ) ) return NULL;

		 $strSql = 'SELECT
						ci.customer_id, sum( ci.amount ) as total_monthly_amount
					FROM
						customer_incomes as ci
					WHERE
						ci.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ')
						AND ci.cid = ' . ( int ) $intCid . '
						AND ci.income_type_id IN ( ' . implode( ',', $arrintIncomeTypeIds ) . ' )
						AND ci.deleted_by IS NULL
						AND ci.deleted_on IS NULL
					GROUP BY
						ci.customer_id';

		$arrmixData = fetchData( $strSql, $objDatabase );

		$arrintApplicantTotalMonthlyAmounts = [];
		if( true == valArr( $arrmixData ) ) {
			foreach( $arrmixData as $arrstrData ) {
				$arrintApplicantTotalMonthlyAmounts[$arrstrData['customer_id']] = [
					'customer_id' 			=> $arrstrData['customer_id'],
					'total_monthly_amount'  => $arrstrData['total_monthly_amount']
				];
			}
		}

		return $arrintApplicantTotalMonthlyAmounts;
	}

	public static function fetchCustomerIncomesByCidByIncomeTypeIdByCustomerIds( $intCid, $intIncomeTypeId, $arrintCustomerIds, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = '	SELECT
						*
					FROM
						customer_incomes
					WHERE customer_id IN (' . implode( ',', $arrintCustomerIds ) . ')
					AND income_type_id = ' . ( int ) $intIncomeTypeId . '
					AND deleted_by IS NULL
					AND deleted_on IS NULL
					AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCidByIncomeTypeIdsByCustomerIds( $intCid, $arrintIncomeTypeIds, $arrintCustomerIds, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) || false == valArr( $arrintIncomeTypeIds ) )		return [];

		$strSql = '	SELECT
						*
					FROM
						customer_incomes
					WHERE customer_id IN (' . implode( ',', $arrintCustomerIds ) . ')
					AND income_type_id IN ( ' . implode( ',', $arrintIncomeTypeIds ) . ' )
					AND deleted_by IS NULL
					AND deleted_on IS NULL
					AND cid = ' . ( int ) $intCid;

		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByPropertyIdByCustomerTypeIdsByCid( $intPropertyId, $arrintCustomerTypeIds,  $intCid, $objDatabase ) {

		$strWhereCutomerType = ( true == valArr( $arrintCustomerTypeIds ) ) ? ' AND lc.customer_type_id IN ( ' . implode( ',', $arrintCustomerTypeIds ) . ')' : '';
		$strSql = '   SELECT
							 DISTINCT ON (ci.id) ci.id,
							 ci.*,
							 ( c.name_first || \' \' || c.name_last ) as customer_name,
							 lc.customer_type_id as customer_type
		  			  FROM
						  leases l
						  JOIN lease_intervals li ON ( li.cid = l.cid AND li.id = l.active_lease_interval_id AND l.id = li.lease_id )
						  JOIN lease_customers lc ON ( lc.cid = l.cid AND l.id = lc.lease_id )
						  JOIN customers c ON ( lc.cid = c.cid AND lc.customer_id = c.id )
						  JOIN customer_types ct ON ( lc.customer_type_id = ct.id )
						  JOIN customer_portal_settings cps ON ( cps.cid = c.cid AND cps.customer_id = c.id )
						  JOIN customer_incomes ci ON ( ci.customer_id = c.id AND ci.cid = c.cid )
			  		  WHERE
							l.property_id =' . ( int ) $intPropertyId . ' AND
							l.cid =' . ( int ) $intCid . '
							AND li.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
							AND ci.deleted_by IS NULL
							AND ci.deleted_on IS NULL
							AND ci.institution_name IS NOT NULL ' . $strWhereCutomerType;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolIncludeDeletedAA = false, $strApplicationLockedOn = NULL ) {
		if( true == valStr( $strApplicationLockedOn ) ) {
			$strSql = CCustomerIncomeLogs::fetchCustomerIncomeLogsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase, $boolReturnSqlOnly = true );

			// @FIXME: Third parameter is wrong.
			return self::fetchCustomerIncomes( $strSql, $objDatabase, [ 'CCustomerIncome', 'fromLogToIncome' ] );
		}

		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND aa.deleted_on IS NULL' : '';

		$strSql = 'SELECT ci.*
					FROM customer_incomes ci
						 JOIN applicants a ON ( a.cid = ci.cid AND a.customer_id = ci.customer_id )
	 					 JOIN applicant_applications as aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid ' . $strCheckDeletedAASql . ' )
						 AND aa.application_id = ' . ( int ) $intApplicationId . ' AND aa.cid = ' . ( int ) $intCid . '
						 AND ci.deleted_by IS NULL
						 AND ci.deleted_on IS NULL
						 ORDER BY ci.customer_id';

		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdByIncomeTypeIdsWithoutNullAmountByCid( $intCustomerId, $arrintIncomeTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIncomeTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM customer_incomes WHERE customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid . ' AND income_type_id IN ( ' . implode( ',', $arrintIncomeTypeIds ) . ' ) AND amount IS NOT NULL AND deleted_by IS NULL AND deleted_on IS NULL ORDER BY income_type_id';
		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdsByIncomeTypeIdWithoutNullAmountByCid( $arrintCustomerIds, $intIncomeTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT * FROM customer_incomes WHERE customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND cid = ' . ( int ) $intCid . ' AND income_type_id = ' . ( int ) $intIncomeTypeId . ' AND amount IS NOT NULL AND deleted_by IS NULL AND deleted_on IS NULL';

		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdByIncomeTypeIdsWithoutZeroAmountByCid( $intCustomerId, $arrintIncomeTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIncomeTypeIds ) ) return NULL;

		$strSql = 'SELECT 
						*,
						CASE
							        WHEN date_ended < CURRENT_DATE THEN \'' . __( 'Past' ) . '\'
							        WHEN ( income_effective_date IS NULL AND date_started > CURRENT_DATE ) OR ( income_effective_date > CURRENT_DATE ) THEN \'' . __( 'Future' ) . '\'
							        ELSE \'' . __( 'Current' ) . '\'
                                END as status 
				   FROM customer_incomes 
				   WHERE 
				        customer_id = ' . ( int ) $intCustomerId . ' 
				        AND cid = ' . ( int ) $intCid . ' 
				        AND income_type_id IN ( ' . implode( ',', $arrintIncomeTypeIds ) . ' ) 
				        AND amount > 0 AND deleted_by IS NULL 
				        AND deleted_on IS NULL 
				   ORDER BY income_type_id';
		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdWithoutNullAmountByCid( $intCustomerId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM customer_incomes WHERE customer_id = ' . ( int ) $intCustomerId . ' AND cid = ' . ( int ) $intCid . ' AND amount IS NOT NULL AND deleted_by IS NULL AND deleted_on IS NULL';
		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdsWithoutNullAmountByCid( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT * FROM customer_incomes WHERE customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' ) AND cid = ' . ( int ) $intCid . ' AND amount IS NOT NULL AND deleted_by IS NULL AND deleted_on IS NULL';
		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdsWithoutNullAmountByEffectiveDateByCid( $arrintCustomerIds, $strEffectiveDate, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strStartDateCondition = ( true == valStr( $strEffectiveDate ) ) ? ' AND
			CASE
				WHEN date_ended IS NOT NULL
					THEN \'' . $strEffectiveDate . '\' BETWEEN income_effective_date AND date_ended
				WHEN date_ended IS NULL
					THEN income_effective_date <= \'' . $strEffectiveDate . '\'
			END' : '';

		$strSql = 'SELECT 
						id,
						amount,
						customer_id,
						income_type_id,
						frequency_id
					FROM 
						customer_incomes
					WHERE 
						customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' ) 
						AND cid = ' . ( int ) $intCid
						. $strStartDateCondition . '
						AND amount IS NOT NULL 
						AND deleted_by IS NULL 
						AND deleted_on IS NULL';

		return rekeyArray( 'id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchCustomCustomerIncomesByStatusByLeaseIdByCid( $intIncomeStatus, $intLeaseId, $intCid, $objDatabase ) {

		$strWhereCondition = '';

		if( $intIncomeStatus == CCustomerIncome::CURRENT || $intIncomeStatus == '' ) {
			$strWhereCondition = 'AND (
											(
												ci.date_started IS NULL 
												AND ci.income_effective_date IS NULL
												AND
												(
													ci.date_ended IS NULL 
													OR ci.date_ended >= current_date
												)
											)
											OR (
												ci.income_effective_date <= current_date
											  		AND ( ci.date_ended >= current_date OR ci.date_ended IS NULL )
												)
											OR
											( ci.income_effective_date  IS NULL
												AND ci.date_started <= current_date
												AND ( ci.date_ended >= current_date OR ci.date_ended IS NULL )
											)
									  )';
		}

		if( $intIncomeStatus == CCustomerIncome::PAST ) {
			$strWhereCondition = 'AND ( ci.date_ended < current_date )';
		}

		if( $intIncomeStatus == CCustomerIncome::FUTURE ) {
			$strWhereCondition = 'AND 
									( 
										( 
											ci.income_effective_date  IS NULL 
											AND ci.date_started > current_date 
										) 
										OR 
										( 
											ci.income_effective_date > current_date 
										) 
									)';
		}

		// In below SQL, We are performing intersection between JSON values and [empty string, NULL ]. '&&' Operator is the Intersection Operator and it will return false if no element in common.

		$strSql = 'SELECT
					ci.id,
					ci.income_type_id,
					it.name,
					ci.institution_name,
					ci.income_effective_date,
					ci.date_ended,
					ci.description,
					ci.frequency_id,
					round(ci.amount::numeric, 2) as amount,
					round( ( CASE
							WHEN ci.frequency_id = ' . CFrequency::WEEKLY . ' 			THEN ci.amount::numeric * 52
							WHEN ci.frequency_id = ' . CFrequency::EVERY_TWO_WEEKS . '	THEN ci.amount::numeric * 26
							WHEN ci.frequency_id = ' . CFrequency::TWICE_PER_MONTH . '	THEN ci.amount::numeric * 24
							WHEN ci.frequency_id = ' . CFrequency::MONTHLY . '			THEN ci.amount::numeric * 12
							WHEN ci.frequency_id = ' . CFrequency::HOURLY . ' AND
								 FALSE = ( ARRAY[ci.details::json ->> \'hourly_wage\',ci.details::json ->> \'hour_per_week\',ci.details::json ->> \'week_per_year\'] && ARRAY[ \'\' ,NULL] )
							THEN ( ( ci.details::json ->> \'hourly_wage\' )::float * (ci.details::json ->> \'hour_per_week\')::float *(ci.details::json ->> \'week_per_year\')::float  )
							ELSE ci.amount::numeric
						END + CASE
                                WHEN FALSE = ( ARRAY [ ci.details::json ->> \'overtime_wage\', ci.details::json ->> \'overtime_hours_per_week\', ci.details::json ->> \'overtime_weeks_per_year\' ] && ARRAY [ \'\', NULL ] ) THEN ( ( ci.details::json ->> \'overtime_wage\' )::float * ( ci.details::json ->> \'overtime_hours_per_week\' )::float * ( ci.details::json ->> \'overtime_weeks_per_year\' )::float )
                                ELSE 0
                                END )::NUMERIC, 2 ) AS annual_income,
					ci.customer_id,
					c.name_first,
					c.name_last
				FROM
					customer_incomes AS ci
					JOIN income_types AS it ON ci.income_type_id = it.id
					JOIN customers AS c ON ci.cid = c.cid AND ci.customer_id = c.id
					JOIN lease_customers AS lc ON lc.cid = c.cid AND lc.customer_id = c.id
				WHERE
					c.cid = ' . ( int ) $intCid . '
					AND lc.lease_id = ' . ( int ) $intLeaseId . '
					 ' . $strWhereCondition . '
					AND ci.deleted_by IS NULL
					AND ci.deleted_on IS NULL 
				ORDER BY
					ci.income_effective_date';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAnnualIncomeByIdsByCustomerIdByCid( $arrintCustomerIncomeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIncomeIds ) ) return NULL;

		$strSql = ' SELECT
						SUM( (ci.details::json ->> \'annual_income_amount\')::float ) as annual_income
					FROM
						customer_incomes ci
					WHERE
						ci.cid = ' . ( int ) $intCid . '
						AND ci.id IN ( ' . implode( ',', $arrintCustomerIncomeIds ) . ' )
						AND ci.deleted_on IS NULL
						AND ci.deleted_by IS NULL';

		$arrmixAnnualIncome = fetchData( $strSql, $objDatabase );

		return $arrmixAnnualIncome[0]['annual_income'];
	}

	public static function fetchActiveCustomerIncomesByCustomerIdsByEffectiveDateByCid( $arrintCustomerIds, $strEffectiveDate, $intCid, $objDatabase, $strEndDate = NULL, $boolIncludeDeleted = false ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strIncludeDeletedCondition = ( false == $boolIncludeDeleted ) ? ' AND ci.deleted_on IS NULL AND ci.deleted_by IS NULL ' : '';

		$strStartDateCondition = ( true == valStr( $strEffectiveDate ) ) ? ' AND
			CASE
				WHEN ci.date_ended IS NOT NULL
					THEN \'' . $strEffectiveDate . '\' BETWEEN ci.income_effective_date AND ci.date_ended
				WHEN ci.date_ended IS NULL
					THEN ci.income_effective_date <= \'' . $strEffectiveDate . '\'
			END' : '';

		$strEndDateCondition = ( true == valStr( $strEndDate ) ) ? ' AND
			CASE
				WHEN ci.date_ended IS NOT NULL
					THEN \'' . $strEndDate . '\' BETWEEN ci.income_effective_date AND ci.date_ended
				WHEN ci.date_ended IS NULL
					THEN ci.income_effective_date <= \'' . $strEndDate . '\'
			END' : '';

		$strSql = ' SELECT
						DISTINCT ci.id AS income_id,
						ci.customer_id,
						COALESCE ( ci.institution_name, it.name ) AS income_name
					FROM
						customer_incomes ci
						JOIN income_types it ON ( ci.income_type_id = it.id )
					WHERE
						ci.cid = ' . ( int ) $intCid . '
						AND ci.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						' . $strStartDateCondition .
						$strEndDateCondition . '
						AND ci.income_type_id IN ( ' . implode( ',', CIncomeType::$c_arrintEmploymentIncomeType ) . ' ) '
						. $strIncludeDeletedCondition;

		return fetchData( $strSql, $objDatabase );
	}

	// @FIXME: This function cannot be executed on array of application_id as effective date would be different. Refactor this function.

	public static function fetchCurrentCustomerIncomesByApplicationIdsByCid( $arrintApplicationIds, $intCid, $objDatabase, $strDesiredSubsidyCertificationEffectiveDate = 'NOW()', $strPossibleSubsidyCertificationEffectiveThroughDate = NULL ) {

		$strDesiredSubsidyCertificationEffectiveDate = ( false == valStr( $strDesiredSubsidyCertificationEffectiveDate ) ) ? 'NOW()' : $strDesiredSubsidyCertificationEffectiveDate;
		$strPossibleSubsidyCertificationEffectiveThroughDate = ( true == valStr( $strPossibleSubsidyCertificationEffectiveThroughDate ) ) ? '\'' . $strPossibleSubsidyCertificationEffectiveThroughDate . '\'::date' : '\'' . $strDesiredSubsidyCertificationEffectiveDate . '\'::date + INTERVAL \'1 YEAR -1 DAY\'';

		$strWhereCondition = 'AND (
									(
										DATE( to_char( ci.date_started, \'YYYY-MM-DD\' ) ) IS NULL
										AND
										DATE( to_char( ci.income_effective_date, \'YYYY-MM-DD\' ) ) IS NULL
									)
									OR
									(
										DATE( to_char( ci.income_effective_date, \'YYYY-MM-DD\' ) ) <= ' . $strPossibleSubsidyCertificationEffectiveThroughDate . '
										AND
										(
											DATE( to_char( ci.date_ended, \'YYYY-MM-DD\' ) ) >= \'' . $strDesiredSubsidyCertificationEffectiveDate . '\'::date
											OR
											DATE( to_char( ci.date_ended, \'YYYY-MM-DD\' ) ) IS NULL
										)
									)
									OR
									(
										DATE( to_char( ci.income_effective_date, \'YYYY-MM-DD\' ) )  IS NULL
										AND
										DATE( to_char( ci.date_started, \'YYYY-MM-DD\' ) ) <= ' . $strPossibleSubsidyCertificationEffectiveThroughDate . '
										AND
										(
											DATE( to_char( ci.date_ended, \'YYYY-MM-DD\' ) ) >= \'' . $strDesiredSubsidyCertificationEffectiveDate . '\'::date
											OR
											DATE( to_char( ci.date_ended, \'YYYY-MM-DD\' ) ) IS NULL
										)
									)
								)';

		$strSql = 'SELECT
						DISTINCT ci.id,
						ci.cid,
						ci.customer_id,
						ci.income_type_id,
						ci.frequency_id,
						ci.institution_name,
						ci.amount,
						ci.income_effective_date,
						ci.date_ended,
						( CASE
							WHEN ci.frequency_id = ' . CFrequency::WEEKLY . ' 			THEN ci.amount::numeric * 52
							WHEN ci.frequency_id = ' . CFrequency::EVERY_TWO_WEEKS . '	THEN ci.amount::numeric * 26
							WHEN ci.frequency_id = ' . CFrequency::TWICE_PER_MONTH . '	THEN ci.amount::numeric * 24
							WHEN ci.frequency_id = ' . CFrequency::MONTHLY . '			THEN ci.amount::numeric * 12
							WHEN ci.frequency_id = ' . CFrequency::HOURLY . ' AND
								 FALSE = ( ARRAY[ci.details::json ->> \'hourly_wage\',ci.details::json ->> \'hour_per_week\',ci.details::json ->> \'week_per_year\'] && ARRAY[ \'\' ,NULL] )
							THEN ( ( ci.details::json ->> \'hourly_wage\' )::float * (ci.details::json ->> \'hour_per_week\')::float *(ci.details::json ->> \'week_per_year\')::float  )
							ELSE ci.amount::numeric
						END 
						+ CASE
							WHEN ci.details IS NOT NULL AND FALSE = ( ARRAY [ ci.details::json ->> \'overtime_wage\', ci.details::json ->> \'overtime_hours_per_week\', ci.details::json ->> \'overtime_weeks_per_year\' ] && ARRAY [ \'\', NULL ] ) THEN ( ( ci.details::json ->> \'overtime_wage\' )::float * ( ci.details::json ->> \'overtime_hours_per_week\' )::float * ( ci.details::json ->> \'overtime_weeks_per_year\' )::float )
							ELSE 0
						END 
						+ CASE
							WHEN ci.details IS NOT NULL AND FALSE = ( ARRAY [ ci.details::json ->> \'member_receives_bonuses\', ci.details::json ->> \'frequency_of_bonuses\', ci.details::json ->> \'bonus_amount\' ] && ARRAY [ \'\', NULL ] ) THEN 
								CASE
									WHEN ( ci.details::json ->> \'frequency_of_bonuses\' )::numeric = ' . CFrequency::WEEKLY . '			THEN ( ci.details::json ->> \'bonus_amount\' )::float * 52
									WHEN ( ci.details::json ->> \'frequency_of_bonuses\' )::numeric = ' . CFrequency::EVERY_TWO_WEEKS . '	THEN ( ci.details::json ->> \'bonus_amount\' )::float * 26
									WHEN ( ci.details::json ->> \'frequency_of_bonuses\' )::numeric = ' . CFrequency::TWICE_PER_MONTH . '	THEN ( ci.details::json ->> \'bonus_amount\' )::float * 24
									WHEN ( ci.details::json ->> \'frequency_of_bonuses\' )::numeric = ' . CFrequency::MONTHLY . '			THEN ( ci.details::json ->> \'bonus_amount\' )::float * 12
									ELSE ( ci.details::json ->> \'bonus_amount\' )::float
								END
							ELSE 0
						END 
						+ CASE
							 WHEN ci.details IS NOT NULL AND FALSE = ( ARRAY [ ci.details::json ->> \'member_receives_tips\', ci.details::json ->> \'frequency_of_tips\', ci.details::json ->> \'tip_amount\' ] && ARRAY [ \'\', NULL ] ) THEN 
								CASE
									WHEN ( ci.details::json ->> \'frequency_of_tips\' )::numeric = ' . CFrequency::WEEKLY . '			THEN ( ci.details::json ->> \'tip_amount\' )::float * 52
									WHEN ( ci.details::json ->> \'frequency_of_tips\' )::numeric = ' . CFrequency::EVERY_TWO_WEEKS . '	THEN ( ci.details::json ->> \'tip_amount\' )::float * 26
									WHEN ( ci.details::json ->> \'frequency_of_tips\' )::numeric = ' . CFrequency::TWICE_PER_MONTH . '	THEN ( ci.details::json ->> \'tip_amount\' )::float * 24
									WHEN ( ci.details::json ->> \'frequency_of_tips\' )::numeric = ' . CFrequency::MONTHLY . '			THEN ( ci.details::json ->> \'tip_amount\' )::float * 12
									ELSE ( ci.details::json ->> \'tip_amount\' )::float
								END
							ELSE 0
						END ) AS annual_income,
						it.hud_code AS income_code,
						ci.social_security_claim_number_encrypted AS ssn_claim_number
					FROM
						customer_incomes ci
						JOIN income_types it ON ( ci.income_type_id = it.id )
						JOIN income_type_groups itg ON ( it.income_type_group_id = itg.id )
						JOIN customers c ON ( ci.customer_id = c.id AND ci.cid = c.cid )
						JOIN applicants a ON ( c.id = a.customer_id AND c.cid = a.cid )
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid )
						JOIN applications ap ON ( aa.application_id = ap.id AND aa.cid = ap.cid )
						JOIN lease_customers lc ON ( lc.customer_id = ci.customer_id AND lc.cid = ci.cid AND ap.lease_id = lc.lease_id )
					WHERE
						aa.application_id IN ( ' . sqlIntImplode( $arrintApplicationIds ) . ' )
						AND aa.cid =' . ( int ) $intCid . '
						AND itg.is_subsidy = true
						' . $strWhereCondition . '
						AND ci.deleted_by IS NULL
						AND ci.deleted_on IS NULL
						AND lc.lease_status_type_id <> ' . CLeaseStatusType::CANCELLED . '
					ORDER BY ci.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCidByPropertyIdByLeaseId( $intCid, $intPropertyId,  $objDatabase, $intLeaseId = NULL ) {

		if( false == valId( $intPropertyId ) ) return NULL;

		$strWhere = ( true == valId( $intLeaseId ) ) ? '  AND lc.lease_id = ' . $intLeaseId : '';

		$strSql	= 'SELECT 
						ci.*
					FROM
						 customer_incomes AS ci
					JOIN customers AS c ON ( c.cid = ci.cid AND ci.customer_id = c.id )
					JOIN lease_customers AS lc ON ( lc.cid = c.cid AND c.id = lc.customer_id )
					WHERE
					    ci.cid = ' . ( int ) $intCid . ' 
						AND lc.property_id = ' . $intPropertyId . ' ' . $strWhere;

		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchCustomerIncomesByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT * FROM customer_incomes WHERE cid = ' . ( int ) $intCid . ' AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND deleted_by IS NULL AND deleted_on IS NULL';

		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

	public static function fetchTotalCustomerIncomesByIncomeTypeGroupIdsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						itg.id as income_type_group_id,
						SUM( ci.amount ) as total_income
					FROM
						customer_incomes ci
						JOIN income_types it ON ( ci.income_type_id = it.id )
						JOIN income_type_groups itg ON ( it.income_type_group_id = itg.id )
						JOIN customers c ON ( ci.customer_id = c.id AND ci.cid = c.cid )
						JOIN applicants a ON ( c.id = a.customer_id AND c.cid = a.cid )
						JOIN applicant_applications aa ON ( a.id = aa.applicant_id AND a.cid = aa.cid AND aa.deleted_by IS NULL )
					WHERE
						aa.application_id =' . ( int ) $intApplicationId . '
						AND aa.cid =' . ( int ) $intCid . '
						AND itg.is_subsidy = true
						AND ci.deleted_by IS NULL
						AND ci.deleted_on IS NULL
					GROUP BY
						itg.id';

		return $arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['total_income'];

		return 0;
	}

	public static function fetchCustomerDetailsCustomerIncomeByIdByCid( $intCustomerIncomeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT 
						c.id,
						c.name_first,
					    c.name_last
					FROM 
						customer_incomes ci
						JOIN customers c ON (ci.cid = c.cid AND ci.customer_id = c.id)
					WHERE 
						ci.id = ' . ( int ) $intCustomerIncomeId . '
						AND ci.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomersIncomeByCustomerIdsByIncomeTypeIdsByCid( $arrintCustomerIds, $arrintIncomeTypeIds, $intCid, $objDatabase, $boolIsShowCategorizedInfo = false ) {
		if( false == valArr( $arrintCustomerIds ) || false == valArr( $arrintIncomeTypeIds ) ) return NULL;

		$strCaseStatement = '';
		$strOrderBy = ' ORDER BY ci.income_effective_date';
		if( true == $boolIsShowCategorizedInfo ) {
			$strOrderBy = ' ORDER BY subq.status, subq.income_effective_date';
			$strCaseStatement = ' ,CASE
							        WHEN ci.date_ended < CURRENT_DATE THEN \'' . __( 'Past' ) . '\'
							        WHEN ( ci.income_effective_date IS NULL AND ci.date_started > CURRENT_DATE ) OR ( ci.income_effective_date > CURRENT_DATE ) THEN \'' . __( 'Future' ) . '\'
							        ELSE \'' . __( 'Current' ) . '\'
                                END as status';
		}

		$strSql = 'SELECT
						it.name as income_name,
						ci.* ' . $strCaseStatement . '
					FROM
						customer_incomes ci
						JOIN income_types it ON( ci.income_type_id = it.id)
					WHERE
						ci.customer_id IN(' . sqlIntImplode( $arrintCustomerIds ) . ')
						AND ci.cid = ' . ( int ) $intCid . '
						AND ci.income_type_id IN ( ' . sqlIntImplode( $arrintIncomeTypeIds ) . ' )
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		$strSql = ( true == $boolIsShowCategorizedInfo ) ? 'SELECT * FROM ( ' . $strSql . ' ) as subq ' . $strOrderBy : $strSql . $strOrderBy;
		$arrobjCustomerIncome = self::fetchCustomerIncomes( $strSql, $objDatabase );
		if( true == valArr( $arrobjCustomerIncome ) ) {
			$arrobjCustomerIncome = rekeyObjects( 'CustomerId', $arrobjCustomerIncome, true );
		}

		return $arrobjCustomerIncome;
	}

	public static function fetchCustomerIncomesByCustomerIdsByIncomeTypeIdsByCid( $arrintCustomerIds, $arrintIncomeTypes, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						customer_incomes 
					WHERE 
						cid = ' . ( int ) $intCid . ' 
						AND customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' ) 
						AND income_type_id IN ( ' . sqlIntImplode( $arrintIncomeTypes ) . ' ) 
						AND deleted_by IS NULL 
						AND deleted_on IS NULL';

		return self::fetchCustomerIncomes( $strSql, $objDatabase );
	}

}
?>
