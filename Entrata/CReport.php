<?php

class CReport extends CBaseReport {

	// region Report Name Constants

	// Accounting Report.
	const AP_AGING_REPORT								= 'ap_aging';
	const AR_DEPOSITS_REPORT							= 'ar_deposits';
	const AR_BANK_DEPOSITS_REPORT 						= 'ar_bank_deposits';
	const BILLING_ACCOUNTS_LEDGERS_REPORT				= 'billing_accounts_ledger';
	const BILLING_ACCOUNTS_AGING_REPORT					= 'billing_accounts_aging';
	const BILLING_ACCOUNTS_INVOICES_REPORT				= 'billing_accounts_invoices';
	const SCREENING_BILLING_DETAILS_REPORT				= 'screening_billing_details';

	const GL_DETAILS_REPORT								= 'gl_details';
	const GL_DETAILS_CONSOLIDATED_REPORT				= 'gl_details_consolidated';
	const TRIAL_BALANCE_REPORT							= 'trial_balance';
	const BALANCE_SHEET_REPORT							= 'balance_sheet';
	const VENDOR_INVOICE_DETAILS_REPORT					= 'vendor_invoice_details';
	const INCOME_STATEMENT_BUDGET_VS_ACTUAL				= 'income_statement_budget_vs_actual';
	const PAYMENTS_IN_TRANSIT_REPORT					= 'payments_in_transit';
	const INCOME_STATEMENT								= 'income_statement';
	const GL_ACCOUNT_TOTALS_BY_PROPERTY_REPORT			= 'gl_account_totals_by_property';
	const GL_ACCOUNT_TOTALS_BY_PROPERTY_BALANCE_SHEET_REPORT		= 'gl_account_totals_by_property:_balance_sheet';
	const GL_ACCOUNT_TOTALS_BY_PROPERTY_INCOME_STATEMENT_REPORT		= 'gl_account_totals_by_property:_income_statement';

	// Operations Report.
	const APPLICATION_DATA_REPORT						= 'application_data';
	const AVAILABILITY_REPORT							= 'availability';
	const BOX_SCORE_REPORT								= 'box_score';
	const BUILDINGS_AND_UNITS_REPORT					= 'buildings_and_units';
	const COLLECTIONS_STATUS_REPORT						= 'collections_status';
	const DELINQUENCY_REPORT							= 'delinquency';
	const DAILY_AND_WEEKLY_OPERATIONS_REPORT			= 'daily_and_weekly_operations';
	const EXECUTIVE_SUMMARY_REPORT						= 'executive_summary';
	const GAMIFICATION_REPORT							= 'gamification';
	const GPR_REPORT									= 'gpr_consolidate_by';
	const GPR_CASH_BASIS								= 'gross_potential_rent_-_cash_basis';
	const INSURANCE_POLICY_DETAILS_REPORT				= 'insurance_policy_details';
	const INSURANCE_POLICIES_BY_LEASING_AGENT_REPORT	= 'insurance_policies_by_leasing_agent';
	const LEASE_ACTIVITY_REPORT							= 'lease_activity';
	const LEASE_DATA_REPORT								= 'lease_data';
	const MONTHLY_RECONCILIATION_REPORT					= 'monthly_reconciliation_new';
	const OPERATIONS_REPORT								= 'operations';
	const PRE_LEASE_REPORT								= 'pre_lease';
	const PREPAYMENTS_AND_DEPOSIT_REFUNDS_REPORT		= 'prepayments_and_deposit_refunds';
	const PROPERTY_LEADS								= 'property_leads';
	const PROFILE_DATA									= 'profile_data';
	const RENT_ROLL_REPORT								= 'cached_rent_roll';
	const RENTABLE_ITEMS_REPORT							= 'rentable_items';
	const RESIDENT_AGED_RECEIVABLES_REPORT				= 'resident_aged_receivables';
	const RESIDENT_CHARGES_REPORT						= 'resident_charges';
	const RESIDENT_DATA_REPORT							= 'resident_data';
	const RESIDENT_DEMOGRAPHICS_REPORT					= 'resident_demographics';
	const RESIDENT_DEPOSIT_AUDIT_REPORT					= 'resident_deposit_audit';
	const RESIDENT_PORTAL_TRAFFIC_REPORT				= 'resident_portal_traffic';
	const RESIDENT_RECEIPTS_REPORT						= 'resident_receipts';
	const RESIDENT_REFERRALS_REPORT						= 'resident_referrals';
	const RESIDENT_RETENTION_REPORT						= 'resident_retention';
	const UNIT_EXCLUSIONS_REPORT						= 'unit_exclusions';
	const UNIT_SPACE_DATA_REPORT						= 'unit_space_data';
	const REVIEW_ATTRIBUTES_REPORT						= 'review_attribute';
	const PROPERTY_ATTRIBUTES_REPORT					= 'property_attribute';
	const EXPIRING_LEASES_REPORT						= 'expiring_leases';
	const TRENDING_OCCUPANCY_REPORT						= 'trending_occupancy';
	const HISTORICAL_WAITLIST							= 'historical_waitlist';
	const MONUMENT_WEEKLY_REPORT						= 'monument_weekly';
	const SCION_PORTFOLIO_DELINQUENCY_AND_OCCUPANCY		= 'scion_portfolio_delinquency_and_occupancy';
	const SCHEDULED_CHARGES_BY_LEASE					= 'scheduled_charges_by_lease';
	const PROPERTY_PULSE_REPORT							= 'property_pulse';
	const GROUP_BOOKINGS_REPORT							= 'group_bookings';
	const RECEIPTS_BY_CHARGE_CODE_REPORT				= 'receipts_by_charge_code';
	const SITE_TABLET_USAGE_REPORT						= 'site_tablet_usage';
	const PACKAGES_REPORT								= 'packages';
	const LEASING_GOAL_REPORT							= 'leasing_goals';
	const PEAK_CAMPUS_BOX_SCORE_REPORT					= 'peak_campus_box_score';
	const PEAK_CAMPUS_UNIT_SPACE_DATA_REPORT			= 'peak_campus_unit_space_data_for_box_score';
	const PEAK_CAMPUS_APPLICATION_DATA_REPORT			= 'peak_campus_application_data_for_box_score';
	const PEAK_CAMPUS_LEASE_DATA_REPORT					= 'peak_campus_lease_data_for_box_score';
	const PEAK_CAMPUS_APPLICATION_STATUS_REPORT			= 'peak_campus_application_status_for_box_score';
	const PEAK_CAMPUS_TRAFFIC_AND_EVENT_REPORT			= 'peak_campus_traffic_and_events_for_box_score';

	// Leasing Report.
	const APPLICANT_DETAIL_REPORT						= 'applicant_detail';
	const APPLICATIONS_BY_LEAD_SOURCE_REPORT			= 'applications_by_lead_source';
	const APPLICATION_DETAIL_REPORT						= 'application_detail';
	const LEAD_DEMOGRAPHICS_REPORT						= 'lead_demographics';
	const LEAD_CONVERSION_REPORT						= 'lead_conversion';
	const WEIDNER_LEAD_CONVERSION_REPORT				= 'weidner_lead_conversion';
	const LEAD_DETAIL_REPORT							= 'lead_detail';
	const LEAD_SCORE_DETAILS_REPORT						= 'lead_score_details';
	const LEAD_EVENTS_REPORT							= 'lead_events';
	const LEADS_BY_LEAD_SOURCE_REPORT					= 'leads_by_lead_source';
	const LEADS_BY_PRODUCT_REPORT						= 'leads_by_product';
	const LEADS_TRENDS_BY_SOURCE_REPORT					= 'lead_trends_by_source';
	const LEASE_EXECUTION_SUMMARY_REPORT				= 'lease_execution_summary';
	const LEASE_EXECUTION_APPLICANT_REPORT				= 'lease_execution_(applicant)';
	const LEASE_EXECUTION_ADOPTION_REPORT				= 'lease_execution_adoption';
	const MIDDLEBURG_LEASING_AND_EXPIRATIONS_REPORT		= 'middleburg_leasing_and_expirations';
	const LEASING_PERFORMANCE_REPORT					= 'leasing_performance';
	const BONAVENTURE_LEASING_PERFORMANCE_REPORT		= 'bonaventure_leasing_performance';
	const ONLINE_APPLICATIONS_REPORT					= 'online_applications';
	const ONLINE_GUEST_CARDS_REPORT						= 'online_guest_cards';
	const ONLINE_APPLICATIONS_ABANDON_POINTS_REPORT		= 'online_applications_abandon_points';
	const PROPERTY_PERFORMANCE_REPORT					= 'property_performance';
	const BONAVENTURE_PROPERTY_PERFORMANCE_REPORT		= 'bonaventure_property_performance';
	const TOUR_CONVERSION_REPORT						= 'tour_conversion';
	const TRAFFIC_AND_EVENTS_REPORT						= 'traffic_and_events';
	const TRAFFIC_SUMMARY_REPORT						= 'traffic_summary';
	const COST_PER_LEAD_AND_LEASE						= 'cost_per_lead_and_lease';
	const MILLCREEK_LEASING_STATUS						= 'millcreek_leasing_status';
	const CLOSE_REASONS_REPORT							= 'close_reasons';
	const APPLICATION_STATUS_REPORT						= 'application_status';
	const SCREENING_DETAIL_REPORT						= 'screening_detail';
	const CONVERSION_PATHWAY_REPORT						= 'conversion_pathway';
	const CLS_LEASING_REPORT							= 'cls_leasing';
	const NEGLECTED_LEADS_REPORT						= 'neglected_leads';
	const LEAD_TREND_ANALYSIS_REPORT					= 'lead_trend_analysis';
	const LEAD_SOURCE_ATTRIBUTION						= 'lead_source_attribution';
	const LEASE_TERM_PROGRESS_SUMMARY_REPORT			= 'lease_term_progress_summary';
	const LEASE_TRADE_OUT								= 'lease_trade_out';

	// Maintenance Report.
	const WORK_ORDER_DETAILS_REPORT						= 'work_order_details';
	const WORK_ORDER_SUMMARY_REPORT						= 'work_order_summary';
	const WORK_ORDER_SATISFACTION_SURVEYS_REPORT		= 'work_order_satisfaction_surveys';

	// Payments Report.
	const BATCH_PROCESSING_REPORT						= 'batch_processing';
	const CHECK21_BATCHES_REPORT						= 'check21_batches';
	const RECURRING_PAYMENTS_REPORT						= 'recurring_payments';
	const SETTLEMENT_DISTRIBUTION_REPORT				= 'settlement_distributions';
	const PAYMENT_DETAILS_REPORT						= 'payment_details';

	// Utilities Report.
	const GOOGLE_PPC_CALL_TRACKER_REPORT				= 'google_ppc_call_tracker';
	const PROPERTY_BILLING_REPORT						= 'property_billing';
	const PROPERTY_METER_HEALTH_REPORT					= 'property_meter_health';
	const TOTAL_COST_BY_UTILITY_REPORT					= 'total_cost_by_utility';
	const TOTAL_EXPENSE_BY_UTILITY_REPORT				= 'total_expense_by_utility';
	const UTILITY_AUDIT_REPORT							= 'utility_audit';
	const VCR_RECOVERY_REPORT							= 'vcr_recovery';
	const ACCOUNT_UTILITY_TREND_REPORT					= 'account_utility_trend';

	// Communications Report.
	const CALL_PERFORMANCE_BY_AGENT_REPORT				= 'call_performance_by_agents';
	const CALLS_DISTRIBUTION_REPORT						= 'calls_distribution';
	const CALL_SETTING_CHANGES_REPORT					= 'call_setting_changes';
	const LEASING_CENTER_CALLS_BY_RESULT_REPORT			= 'leasing_center_calls_by_result';
	const LEASING_CENTER_ACTIVITY_SUMMARY_REPORT		= 'leasing_center_activity_summary';
	const LIVE_CHAT_BREAKDOWN_REPORT					= 'live_chat_breakdown';
	const PROPERTY_CALL_CONVERSION_REPORT				= 'property_call_conversion';
	const LEASING_CENTER_APPOINTMENT_ANALYSIS			= 'leasing_center_appointment_analysis';
	const LEASING_CENTER_CONVERSION_REPORT				= 'leasing_center_conversion';
	const DEMAND_REPORT									= 'demand';
	const CALL_ANALYSIS_AUDIT						    = 'callanalysis_audit';
	const CHATS_REPORT									= 'chats_report';

	// System Report.
	const BILLING_ACCOUNT_INVOICE_DETAIL_REPORT			= 'billing_account_invoice_detail';
	const OPEN_PERIOD_DETAILS_REPORT					= 'open_period_details';

	// Job Costing Report.
	const JOBS_OVERVIEW                                 = 'jobs_overview';

	// endregion

	protected $m_intReportGroupId;
	protected $m_intCompanyUserId;
	protected $m_intModuleId;
	protected $m_strReportGroupName;
	protected $m_strReportNewGroupName;
	protected $m_intPositionNumber;
	protected $m_intReportGroupTypeId;
	protected $m_boolIsInternal;
	protected $m_boolIsPublic;
	protected $m_strReportGroupType;
	protected $m_boolIsQuickLink = false;

	protected $m_intReportInstanceId;
	protected $m_intReportNewInstanceId;
	protected $m_strReportNewInstanceName;

	protected $m_intReportVersionId;
	protected $m_strDefinition;
	protected $m_intMajor				= 1;
	protected $m_intMinor				= 0;
	protected $m_strNewestVersion;
	protected $m_strExpiration;
	protected $m_strTitleAddendum;
	protected $m_boolIsLatest = true;
	protected $m_boolIsDefault = false;
	protected $m_boolIsExpired = false;
	protected $m_boolUpdateAvailable = false;

	protected $m_intReportFilterId;
	protected $m_strReportFilterName;

	protected $m_intReportScheduleId;
	protected $m_arrmixDownloadOptions;
	protected $m_arrmixScheduledData;
	protected $m_strReportScheduleType;

	protected $m_boolHasItemDescription = false;
	protected $m_strCategory;
	protected $m_arrmixDefinition;

	/*
	 * Get Functions
	 */

	public function getReportGroupId() {
		return $this->m_intReportGroupId;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getModuleId() {
		return $this->m_intModuleId;
	}

	public function getReportGroupName() {
		return $this->m_strReportGroupName;
	}

	public function getReportNewInstanceName() {
		return $this->m_strReportNewInstanceName;
	}

	public function getReportNewGroupName() {
		return $this->m_strReportNewGroupName;
	}

	public function getPositionNumber() {
		return $this->m_intPositionNumber;
	}

	public function getReportGroupTypeId() {
		return $this->m_intReportGroupTypeId;
	}

	public function getIsInternal() {
		return $this->m_boolIsInternal;
	}

	public function getIsPublic() {
		return $this->m_boolIsPublic;
	}

	public function getIsQuickLink() {
		return $this->m_boolIsQuickLink;
	}

	public function getReportGroupType() {
		return $this->m_strReportGroupType;
	}

	public function getReportInstanceId() {
		return $this->m_intReportInstanceId;
	}

	public function getReportNewInstanceId() {
		return $this->m_intReportNewInstanceId;
	}

	public function getReportVersionId() {
		return $this->m_intReportVersionId;
	}

	public function getDefinition() {
		return $this->m_strDefinition;
	}

	public function getMajor() {
		return $this->m_intMajor;
	}

	public function getMinor() {
		return $this->m_intMinor;
	}

	public function getNewestVersion() {
		return $this->m_strNewestVersion;
	}

	public function getExpiration() {
		return $this->m_strExpiration;
	}

	public function getTitleAddendum() {
		return $this->m_strTitleAddendum;
	}

	public function getIsLatest() {
		return $this->m_boolIsLatest;
	}

	public function getUpdateAvailable() {
		return $this->m_boolUpdateAvailable;
	}

	public function getIsExpired() {
		return $this->m_boolIsExpired;
	}

	public function getReportFilterId() {
		return $this->m_intReportFilterId;
	}

	public function getReportFilterName() {
		return $this->m_strReportFilterName;
	}

	public function getReportScheduleId() {
		return $this->m_intReportScheduleId;
	}

	public function getDownloadOptions() {
		return $this->m_arrmixDownloadOptions;
	}

	public function getReportScheduleType() {
		return $this->m_strReportScheduleType;
	}

	public function getHasItemDescription() {
		return $this->m_boolHasItemDescription;
	}

	public function getCategory() {
		return $this->m_strCategory;
	}

	public function getDefinitionData() {
		return $this->m_arrmixDefinition;
	}

	public function getDefinitionField( $strFieldName ) {
		if( false == isset( $this->m_arrmixDefinition ) ) {
			$this->decodeDefinition();
		}

		return ( true == isset( $this->m_arrmixDefinition[$strFieldName] ) ? $this->m_arrmixDefinition[$strFieldName] : NULL );
	}

	public function getClass() {
		return $this->getDefinitionField( 'class' );
	}

	public function getUrl() {
		return $this->getDefinitionField( 'url' );
	}

	public function getReportXml() {
		return $this->getDefinitionField( 'report_xml' );
	}

	public function getDocumentId() {
		return $this->getDefinitionField( 'document_id' );
	}

	public function getCuid() {
		return $this->getDefinitionField( 'cuid' );
	}

	public function getValidationCallbacks() {
		return $this->getDefinitionField( 'validation_callbacks' ) ?: [];
	}

	public function getAllowOnlyAdmin() {
		return $this->getDefinitionField( 'allow_only_admin' );
	}

	public function getAllowOnlyPsiAdmin() {
		return $this->getDefinitionField( 'allow_only_psi_admin' );
	}

	public function getIsLibraryReport() {
		return true == in_array( $this->getReportTypeId(), CReportType::$c_arrintLibraryReportTypes, true );
	}

	public function getReportUrl() {
		switch( $this->getReportTypeId() ) {
			case CReportType::SYSTEM:
			case CReportType::CUSTOM:
				return '?module=' . CModule::REPORTS . $this->getReportQueryString();

			case CReportType::CONTROLLER:
				return $this->getUrl();

			case CReportType::SAP:
				// Toggle these lines to run SAP reports within the Entrata report framework
				// return '?module=' . CModule::REPORTS . $this->getReportQueryString();
				return '?module=sap_reportsxxx&action=view_report' . $this->getReportQueryString();

			case CReportType::SISENSE:
				// Toggle these lines to run dashboard reports within the Entrata report framework
				// return '?module=' . CModule::REPORTS . $this->getReportQueryString();
				return '?module=dashboard_reports_newxxx&action=view_dashboard' . $this->getReportQueryString();

			default:
				trigger_error( 'Report type is not yet supported', E_USER_ERROR );
				return NULL;
		}
	}

	public function getReportQueryString() {
		$strQueryString = '&name=' . urlencode( $this->getName() );

		if( false == $this->getIsLatest() ) {
			$strQueryString .= '&version=' . $this->getMajor() . '.' . $this->getMinor();
		}

		if( CReportType::SYSTEM != $this->getReportTypeId() ) {
			$strQueryString .= '&type=' . $this->getReportTypeId();
		}

		return $strQueryString;
	}

	public function getIsDefault() {
		return $this->m_boolIsDefault;
	}

	public function getScheduledData() {
		return json_decode( $this->m_arrmixScheduledData, true );
	}

	/*
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		// We need to handle the definition before the parent method, so that escape sequences are not removed from the JSON and it is never direct set
		if( true == isset( $arrmixValues['definition'] ) ) {
			$this->setDefinition( $arrmixValues['definition'] );
			unset( $arrmixValues['definition'] );
		}

		if( true == isset( $arrmixValues['details'] ) && true == valArr( $arrmixValues['details'], 0 ) ) {
			// Base setValues tries to trim details, which fails for objects and arrays
			$arrmixValues['details'] = json_encode( $arrmixValues['details'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['report_group_id'] ) )			$this->setReportGroupId( $arrmixValues['report_group_id'] );
		if( true == isset( $arrmixValues['company_user_id'] ) )			$this->setCompanyUserId( $arrmixValues['company_user_id'] );
		if( true == isset( $arrmixValues['report_group_name'] ) )		$this->setReportGroupName( $arrmixValues['report_group_name'] );
		if( true == isset( $arrmixValues['report_new_instance_name'] ) )$this->setReportNewInstanceName( $arrmixValues['report_new_instance_name'] );
		if( true == isset( $arrmixValues['report_new_group_name'] ) )	$this->setReportNewGroupName( $arrmixValues['report_new_group_name'] );
		if( true == isset( $arrmixValues['position_number'] ) )			$this->setPositionNumber( $arrmixValues['position_number'] );
		if( true == isset( $arrmixValues['report_group_type_id'] ) )	$this->setReportGroupTypeId( $arrmixValues['report_group_type_id'] );
		if( true == isset( $arrmixValues['is_internal'] ) )				$this->setIsInternal( $arrmixValues['is_internal'] );
		if( true == isset( $arrmixValues['report_group_type'] ) )		$this->setReportGroupType( $arrmixValues['report_group_type'] );
		if( true == isset( $arrmixValues['report_instance_id'] ) )		$this->setReportInstanceId( $arrmixValues['report_instance_id'] );
		if( true == isset( $arrmixValues['report_new_instance_id'] ) )	$this->setReportNewInstanceId( $arrmixValues['report_new_instance_id'] );
		if( true == isset( $arrmixValues['report_version_id'] ) )		$this->setReportVersionId( $arrmixValues['report_version_id'] );
		if( true == isset( $arrmixValues['major'] ) )					$this->setMajor( $arrmixValues['major'] );
		if( true == isset( $arrmixValues['minor'] ) )					$this->setMinor( $arrmixValues['minor'] );
		if( true == isset( $arrmixValues['newest_version'] ) )			$this->setNewestVersion( $arrmixValues['newest_version'] );
		if( true == isset( $arrmixValues['expiration'] ) )				$this->setExpiration( $arrmixValues['expiration'] );
		if( true == isset( $arrmixValues['title_addendum'] ) )			$this->setTitleAddendum( $arrmixValues['title_addendum'] );
		if( true == isset( $arrmixValues['is_public'] ) )				$this->setIsPublic( $arrmixValues['is_public'] );
		if( true == isset( $arrmixValues['is_latest'] ) )				$this->setIsLatest( $arrmixValues['is_latest'] );
		if( true == isset( $arrmixValues['is_default'] ) )				$this->setIsDefault( $arrmixValues['is_default'] );
		if( true == isset( $arrmixValues['has_item_description'] ) )	$this->setHasItemDescription( $arrmixValues['has_item_description'] );
		if( true == isset( $arrmixValues['category'] ) )				$this->setCategory( $arrmixValues['category'] );
		if( true == isset( $arrmixValues['report_filter_id'] ) )		$this->setReportFilterId( $arrmixValues['report_filter_id'] );
		if( true == isset( $arrmixValues['report_filter_name'] ) )		$this->setReportFilterName( $arrmixValues['report_filter_name'] );
		if( true == isset( $arrmixValues['report_schedule_id'] ) )		$this->setReportScheduleId( $arrmixValues['report_schedule_id'] );
		if( true == isset( $arrmixValues['download_options'] ) )		$this->setDownloadOptions( json_decode( $arrmixValues['download_options'], true ) );
		if( true == isset( $arrmixValues['report_schedule_type'] ) )	$this->setReportScheduleType( $arrmixValues['report_schedule_type'] );
		if( true == isset( $arrmixValues['update_available'] ) )		$this->setUpdateAvailable( $arrmixValues['update_available'] );
		if( true == isset( $arrmixValues['is_expired'] ) )				$this->setIsExpired( $arrmixValues['is_expired'] );
		if( true == isset( $arrmixValues['module_id'] ) )				$this->setModuleId( $arrmixValues['module_id'] );
		if( true == isset( $arrmixValues['scheduled_data'] ) )			$this->setScheduledData( $arrmixValues['scheduled_data'] );
		if( true == isset( $arrmixValues['is_quick_link'] ) )			$this->setIsQuickLink( $arrmixValues['is_quick_link'] );
	}

	public function toArray() {
		return array_merge( parent::toArray(), [
			'definition'		=> $this->getDefinitionData(),
			'report_version_id'	=> $this->getReportVersionId(),
			'major'				=> $this->getMajor(),
			'minor'				=> $this->getMinor()
		] );
	}

	public function setReportGroupId( $intReportGroupId ) {
		$this->m_intReportGroupId = ( int ) $intReportGroupId;
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = ( int ) $intCompanyUserId;
	}

	public function setModuleId( $intModuleId ) {
		$this->m_intModuleId = ( int ) $intModuleId;
	}

	public function setReportGroupName( $strReportGroupName ) {
		$this->m_strReportGroupName = $strReportGroupName;
	}

	public function setReportNewInstanceName( $strReportNewInstanceName ) {
		$this->m_strReportNewInstanceName = $strReportNewInstanceName;
	}

	public function setReportNewGroupName( $strReportNewGroupName ) {
		$this->m_strReportNewGroupName = $strReportNewGroupName;
	}

	public function setPositionNumber( $intPositionNumber ) {
		$this->m_intPositionNumber = ( int ) $intPositionNumber;
	}

	public function setReportGroupTypeId( $intReportGroupTypeId ) {
		$this->m_intReportGroupTypeId = ( int ) $intReportGroupTypeId;
	}

	public function setIsInternal( $boolIsInternal ) {
		$this->m_boolIsInternal = CStrings::strToBool( $boolIsInternal );
	}

	public function setIsPublic( $boolIsPublic ) {
		$this->m_boolIsPublic = CStrings::strToBool( $boolIsPublic );
	}

	public function setIsQuickLink( $boolIsQuickLink ) {
		$this->set( 'm_boolIsQuickLink', CStrings::strToBool( $boolIsQuickLink ) );
	}

	public function setReportGroupType( $strReportGroupType ) {
		$this->m_strReportGroupType = $strReportGroupType;
	}

	public function setReportInstanceId( $intReportInstanceId ) {
		$this->m_intReportInstanceId = $intReportInstanceId;
	}

	public function setReportNewInstanceId( $intReportNewInstanceId ) {
		$this->m_intReportNewInstanceId = $intReportNewInstanceId;
	}

	public function setReportVersionId( $intReportVersionId ) {
		$this->m_intReportVersionId = ( int ) $intReportVersionId;
	}

	public function setDefinition( $strDefinition ) {
		if( false == valArr( $strDefinition ) ) {
			$this->m_strDefinition = $strDefinition;
			$this->decodeDefinition();
		} else {
			$this->m_arrmixDefinition = $strDefinition;
			$this->encodeDefinition();
		}
		return $this;
	}

	public function setMajor( $intMajor ) {
		$this->m_intMajor = ( int ) $intMajor;
	}

	public function setMinor( $intMinor ) {
		$this->m_intMinor = ( int ) $intMinor;
	}

	public function setNewestVersion( $intNewestVersion ) {
		$this->m_strNewestVersion = $intNewestVersion;
	}

	public function setExpiration( $strExpiration ) {
		$this->m_strExpiration = $strExpiration;
	}

	public function setTitleAddendum( $strTitleAddendum ) {
		$this->m_strTitleAddendum = $strTitleAddendum;
	}

	public function setIsLatest( $boolIsLatest ) {
		$this->m_boolIsLatest = CStrings::strToBool( $boolIsLatest );
	}

	public function setUpdateAvailable( $boolUpdateAvailable ) {
		$this->m_boolUpdateAvailable = CStrings::strToBool( $boolUpdateAvailable );
	}

	public function setIsExpired( $boolIsExpired ) {
		$this->m_boolIsExpired = CStrings::strToBool( $boolIsExpired );
	}

	public function setReportFilterId( $intReportFilterId ) {
		$this->m_intReportFilterId = ( int ) $intReportFilterId;
	}

	public function setReportFilterName( $strReportFilterName ) {
		$this->m_strReportFilterName = $strReportFilterName;
	}

	public function setReportScheduleId( $intReportScheduleId ) {
		$this->m_intReportScheduleId = ( int ) $intReportScheduleId;
	}

	public function setDownloadOptions( $arrmixDownloadOptions ) {
		$this->m_arrmixDownloadOptions = $arrmixDownloadOptions;
	}

	public function setReportScheduleType( $strReportScheduleType ) {
		$this->m_strReportScheduleType = $strReportScheduleType;
	}

	public function setHasItemDescription( $boolHasItemDescription ) {
		$this->m_boolHasItemDescription = CStrings::strToBool( $boolHasItemDescription );
	}

	public function setCategory( $strCategory ) {
		$this->m_strCategory = $strCategory;
	}

	protected function setDefinitionField( $strFieldName, $mixValue ) {
		$this->m_arrmixDefinition[$strFieldName] = $mixValue;
		$this->encodeDefinition();
		return $this;
	}

	public function setClass( $strClass ) {
		return $this->setDefinitionField( 'class', $strClass );
	}

	public function setUrl( $strUrl ) {
		return $this->setDefinitionField( 'url', $strUrl );
	}

	public function setReportXml( $strReportXml ) {
		return $this->setDefinitionField( 'report_xml', $strReportXml );
	}

	public function setAllowOnlyAdmin( $boolAllowOnlyAdmin ) {
		return $this->setDefinitionField( 'allow_only_admin', $boolAllowOnlyAdmin ? true : false );
	}

	public function setAllowOnlyPsiAdmin( $boolAllowOnlyPsiAdmin ) {
		return $this->setDefinitionField( 'allow_only_psi_admin', $boolAllowOnlyPsiAdmin ? true : false );
	}

	public function setValidationCallbacks( array $arrmixValidationCallbacks ) {
		return $this->setDefinitionField( 'validation_callbacks', ( array ) $arrmixValidationCallbacks );
	}

	public function addValidationCallback( $strFunctionName, $arrmixArguments ) {
		$this->m_arrmixDefinition['validation_callbacks'][$strFunctionName] = $arrmixArguments;
		$this->encodeDefinition();
		return $this;
	}

	public function clearValidationCallbacks() {
		return $this->setDefinitionField( 'validation_callbacks', [] );
	}

	public function setIsDefault( $boolIsDefault ) {
		$this->m_boolIsDefault = CStrings::strToBool( $boolIsDefault );
	}

	public function setScheduledData( $arrmixScheduledData ) {
		$this->m_arrmixScheduledData = $arrmixScheduledData;
	}

	/*
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportTypeId() {
		$boolIsValid = true;
		if( false == in_array( $this->getReportTypeId(), CReportType::$c_arrintAllReportTypes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_type_id', 'Invalid report type id' ) );
		}
		return $boolIsValid;
	}

	public function valDefaultReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitleAddendum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportXml() {
		if( false == valStr( $this->getReportXml() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'xml', 'Please contact ' . CONFIG_COMPANY_NAME . ' support and provide the error message: Report XML is missing.' ) );
		} else {
			$boolIsValid = $this->validateXml();
		}
		return $boolIsValid;
	}

	public function valValidationCallbackFunction() {

		$boolIsValid = true;

		$objReportsValidation = new CReportsValidation( '', '', '', '' );
		foreach( $this->getValidationCallbacks() as $strValidationCallbackFunction => $arrmixValidationCallbackArgs ) {
			if( false == method_exists( $objReportsValidation, $strValidationCallbackFunction ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'validation_callback_function', 'Validation callback function does not exist.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsInternal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAllowedForApi() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublishable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPublishedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPublishedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validateXml() {
		$boolIsValid = true;
		libxml_use_internal_errors( true );

		$objDOMDocument = new DOMDocument( '1.0', 'utf-8' );
		$objDOMDocument->loadXML( $this->getReportXml() );
		$arrstrErrors = libxml_get_errors();

		if( false == empty( $arrstrErrors ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'xml', 'Please contact ' . CONFIG_COMPANY_NAME . ' support and provide the error message: Invalid Report XML.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
			default:
				// default
				break;
		}
		return $boolIsValid;
	}

	/*
	 * Other Functions
	 */

	protected function decodeDefinition() {
		$this->m_arrmixDefinition = json_decode( $this->m_strDefinition, true );
	}

	protected function encodeDefinition() {
		$this->m_strDefinition = json_encode( $this->m_arrmixDefinition );
	}

}
