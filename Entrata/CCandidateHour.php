<?php

class CCandidateHour extends CBaseCandidateHour {

    public function valId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCandidateId() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCandidateId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'candidate_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDay() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getDay() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'day', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHours() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getHours() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hours', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valUpdatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getUpdatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'updated_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedBy() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedBy() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_by', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCreatedOn() {
        $boolIsValid = true;

            /**
             * Validation example
             */

        // if( true == is_null( $this->getCreatedOn() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }
}
?>