<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueUnitTypePreferences
 * Do not add any new functions to this class.
 */

class CRevenueUnitTypePreferences extends CBaseRevenueUnitTypePreferences {

	public static function fetchRevenueUnitTypePreferencesCountByUnitTypeIdByPropertyId( $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = ' SELECT
                            count(ID) 
                        FROM
							revenue_unit_type_preferences
						WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND unit_type_id = ' . ( int ) $intUnitTypeId;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesCountByUnitTypeIdsByPropertyId( $strUnitTypeIds, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = '  SELECT 
                            ut.id
					   FROM unit_types ut
					        LEFT JOIN revenue_unit_type_preferences rutp ON (ut.id =
					          rutp.unit_type_id AND ut.cid = rutp.cid AND ut.property_id = rutp.property_id)
					   WHERE 
					   ut.cid = ' . ( int ) $intCid . ' 
					   AND ut.property_id = ' . ( int ) $intPropertyId . '
					   AND ut.id IN ( ' . $strUnitTypeIds . ' )
					   Group by ut.id
					   having count(rutp.unit_type_id) = 0';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDefaultValueRevenueUnitTypePreferencesByKey( $arrstrPropertySettingKey, $objDatabase ) {
		$strSql = ' SELECT 
                        dpp.value, psk.key, psk.id
					FROM 
						default_property_preferences dpp
                    JOIN property_setting_keys psk ON dpp.key = psk.key
                    WHERE   
                        dpp.key IN ( \'' . implode( "', '", $arrstrPropertySettingKey ) . '\' ) 
                    Order By psk.id ';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesByKeyIdsByUnitTypeIdByPropertyIdByCid( $arrstrKeys, $intUnitTypeId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = ' SELECT 
                        * 
					FROM    
						revenue_unit_type_preferences 
					WHERE 
						property_id = ' . ( int ) $intPropertyId . '
					     AND unit_type_id = ' . ( int ) $intUnitTypeId . '
	    			     AND cid::integer = ' . ( int ) $intCid . ' 
	    			     AND property_setting_key_id IN (\'' . implode( "','", $arrstrKeys ) . '\') ';

		return self::fetchRevenueUnitTypePreferences( $strSql, $objDatabase );
	}

	public static function fetchRevenueUnitTypePreferencesByKeyIdsByUnitTypeIdsByPropertyIdByCid( $arrstrKeys, $strUnitTypeIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) {
			return NULL;
		}

		$strSql = ' SELECT 
                        * 
					FROM 
						revenue_unit_type_preferences 
					WHERE 
						property_id = ' . ( int ) $intPropertyId . '
					     AND unit_type_id IN ( ' . $strUnitTypeIds . ' )
	    			     AND cid::integer = ' . ( int ) $intCid . '
	    			     AND property_setting_key_id IN (\'' . implode( "','", $arrstrKeys ) . '\') ';

		$arrmixUnitTypePreferencesData = self::fetchRevenueUnitTypePreferences( $strSql, $objDatabase );

		$arrmixUnitTypeIdsData = [];

		if( true == valArr( $arrmixUnitTypePreferencesData ) ) {
			foreach( $arrmixUnitTypePreferencesData as $arrmixData ) {
				$arrmixUnitTypeIdsData[$arrmixData->getUnitTypeId()][] = $arrmixData;
			}
		}
		return $arrmixUnitTypeIdsData;
	}

	public static function fetchRevenueUnitTypePreferencesByKeysByUnitTypeIdsByPropertyIdsByCid( $arrstrKeys, $arrintUnitTypeIds, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) {
			return NULL;
		}
		$arrmixUnitTypeIdsData = [];

		$strSql = ' SELECT
						* 
					FROM 
						revenue_unit_type_preferences 
					WHERE 
						 property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')
						 AND unit_type_id::int IN (' . implode( ',', $arrintUnitTypeIds ) . ')
						 AND cid::integer = ' . ( int ) $intCid . '
						 AND key IN (\'' . implode( "','", $arrstrKeys ) . '\') ';

		$arrmixUnitTypePreferencesData = self::fetchRevenueUnitTypePreferences( $strSql, $objDatabase );

		if( false == valArr( $arrmixUnitTypePreferencesData ) ) {
			return NULL;
		}
		foreach( $arrmixUnitTypePreferencesData as $arrmixData ) {
			$arrmixUnitTypeIdsData[$arrmixData->getPropertyId()][$arrmixData->getUnitTypeId()][$arrmixData->getKey()] = $arrmixData->getValue();
		}
		return $arrmixUnitTypeIdsData;

	}

	public static function fetchRevenueBulkUnitTypePreferencesValueByKeyIdsByUnitTypeIdsByPropertyIdByCid( $strSettingKey, $strUnitTypeIds, $intPropertyId, $intCid, $objDatabase ) {
		$strJoin    = '';
		$strSelect  = '';
		if( 'PRICING_VACANCY_SUSPENSION_CHARGE_CODE' == $strSettingKey ) {
			$strJoin = 'JOIN ar_codes ar ON ar.id = rutp.value::integer';
			$strSelect = ', ar.name as charge_code_name ';
		}

		$strSql = ' SELECT     
                        Distinct rutp.id,
                        psk.key,
                        rutp.value,
                    CASE rutp.value
                        WHEN \'0\' THEN \'Off\'
                        WHEN \'0.5\' THEN \'Conservative\'
                        WHEN \'1\' THEN \'Moderate\'
                        WHEN \'1.5\' THEN \'Aggressive\'
                    END AS updated_value,
                    CASE psk.key
                       WHEN \'PRICING_ENABLE_VACANCY_SUSPENSION\'
				        THEN 
				            CASE rutp.value
				             WHEN \'0\' THEN \'No\'
                             WHEN \'1\' THEN \'Yes\'
				            END
				    END AS vss_log_value,
                    ut.name, psk.label, cu.username, rutp.updated_by ' . $strSelect . '
					FROM 
						revenue_unit_type_preferences rutp
					 JOIN unit_types ut on ut.id = rutp.unit_type_id
					 JOIN property_setting_keys psk on psk.id = rutp.property_setting_key_id
					 JOIN company_users cu ON cu.id = rutp.updated_by ' . $strJoin . ' 
					WHERE 
						rutp.property_id = ' . ( int ) $intPropertyId . '
					     AND rutp.unit_type_id IN ( ' . $strUnitTypeIds . ' )
	    			     AND rutp.cid::integer = ' . ( int ) $intCid . '
	    			     AND rutp.key = \'' . $strSettingKey . '\'';
		return fetchData( $strSql, $objDatabase );
	}

}

?>