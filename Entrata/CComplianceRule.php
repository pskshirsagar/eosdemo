<?php

class CComplianceRule extends CBaseComplianceRule {

	protected $m_boolIsDataChange;

	protected $m_strComplianceItemName;
	protected $m_strComplianceLevelName;

	protected $m_arrobjComplianceRuleItemLimits;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDataChange = false;
		$this->m_arrobjComplianceRuleItemLimits = [];

	}

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valComplianceRulesetId() {
		$boolIsValid = true;
		if( true == is_null( $this->getComplianceRulesetId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_ruleset_id', __( 'Invalid ruleset id.' ) ) );
		}
		return $boolIsValid;
	}

	public function valComplianceTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getComplianceTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_type_id', __( 'Compliance type not defined.' ) ) );
		}
		return $boolIsValid;
	}

	public function valComplianceLevelId() {
		$boolIsValid = true;
		if( true == is_null( $this->getComplianceLevelId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_level_id', __( '{%s, 0} is required . ', [ CComplianceType::getComplianceTypeName( $this->getComplianceTypeId() ) ] ) ) );
		}
		return $boolIsValid;
	}

	public function valComplianceItemId() {
		$boolIsValid = true;
		if( true == is_null( $this->getComplianceItemId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_item_id', __( '{%s, 0} type is required . ', [ CComplianceType::getComplianceTypeName( $this->getComplianceTypeId() ) ] ) ) );
		}
		return $boolIsValid;
	}

	public function valScreeningPackageId() {
		$boolIsValid = true;
		if( true == is_null( $this->getScreeningPackageId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_package_id', __( 'Please select screening package for {%s, 0}. ', [ CComplianceType::getComplianceTypeName( $this->getComplianceTypeId() ) ] ) ) );
		}
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '{%s, 0} name is required.', [ CComplianceType::getComplianceTypeName( $this->getComplianceTypeId() ) ] ) ) );
		}
		return $boolIsValid;
	}

	public function valScreeningFrequencyMonths() {
		$boolIsValid = true;
		if( true == is_null( $this->getScreeningFrequencyMonths() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'screening_frequencey_months', __( 'Please select screening package expiry months.' ) ) );
		}
		return $boolIsValid;
	}

	public function valValidateExpiration() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( false == in_array( $this->getComplianceTypeId(), [ CComplianceType::CREDIT, CComplianceType::CRIMINAL ] ) ) {
					$boolIsValid &= $this->valName();
				}
				$boolIsValid &= $this->valComplianceTypeId();
				$boolIsValid &= $this->valComplianceItemId();

				$boolIsValid &= $this->valComplianceLevelId();

				if( true == in_array( $this->getComplianceTypeId(), [ CComplianceType::CREDIT, CComplianceType::CRIMINAL ] ) ) {
					$boolIsValid &= $this->valScreeningPackageId();
					$boolIsValid &= $this->valScreeningFrequencyMonths();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase );
		}

		return $this->update( $intUserId, $objDatabase );
	}

	public function createComplianceJobItem() {
		$objComplianceJobItem = new CComplianceJobItem();
		$objComplianceJobItem->setCid( $this->getCid() );
		$objComplianceJobItem->setComplianceRuleId( $this->getId() );
		return $objComplianceJobItem;
	}

	public function createComplianceRuleItemLimit() {
		$objComplianceRuleItemLimit = new CComplianceRuleItemLimit();
		$objComplianceRuleItemLimit->setCid( $this->getCid() );
		$objComplianceRuleItemLimit->setComplianceRuleId( $this->getId() );
		$objComplianceRuleItemLimit->setComplianceItemId( $this->getComplianceItemId() );
		return $objComplianceRuleItemLimit;
	}

	// custom update setter and getter

	public function setIsDataChange( $boolIsDataChange ) {
		$this->m_boolIsDataChange = $boolIsDataChange;
	}

	public function getComplianceItemName() {
		return $this->m_strComplianceItemName;
	}

	public function getComplianceLevelName() {
		return $this->m_strComplianceLevelName;
	}

	public function setComplianceItemName( $strComplianceItemName ) {
		$this->m_strComplianceItemName = $strComplianceItemName;
	}

	public function setComplianceLevelName( $strComplianceLevelName ) {
		$this->m_strComplianceLevelName = $strComplianceLevelName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['allow_differential_update'] ) ) {
			$this->setAllowDifferentialUpdate( $arrmixValues['allow_differential_update'] );
		}

		if( true == isset( $arrmixValues['compliance_item_name'] ) ) {
			$this->setComplianceItemName( $arrmixValues['compliance_item_name'] );
		}

		if( true == isset( $arrmixValues['compliance_level_name'] ) ) {
			$this->setComplianceLevelName( $arrmixValues['compliance_level_name'] );
		}
	}

	public function getIsDataChange() {
		return $this->m_boolIsDataChange;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrmixOriginalValues		= [];

		if( true == $this->getAllowDifferentialUpdate() ) {

			$this->unSerializeAndSetOriginalValues();
			$arrmixOriginalValues		= $this->m_arrstrOriginalValues;

			foreach( $arrmixOriginalValues as $strKey => $strOriginalValue ) {

				if( $strOriginalValue == 'f' ) {
					$strOriginalValue = 'false';
				}

				if( $strOriginalValue == 't' ) {
					$strOriginalValue = 'true';
				}

				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

				if( false == in_array( $strKey, [ 'allow_differential_update', 'updated_by', 'updated_on', 'created_by', 'created_on' ] ) && CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $strOriginalValue ) {
					$this->setIsDataChange( true );
				}
			}
		}

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function addComplianceRuleItemLimit( $objComplianceRuleItemLimit ) {
		$this->m_arrobjComplianceRuleItemLimits[] = $objComplianceRuleItemLimit;
	}

	public function getComplianceRuleItemLimits() {
		return $this->m_arrobjComplianceRuleItemLimits;
	}

	public function setComplianceRuleItemLimitsDetails( $arrmixComplianceRuleItemLimit ) {
		$this->m_arrobjComplianceRuleItemLimits = $arrmixComplianceRuleItemLimit;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( CWorker::USER_ID_FLAG_TO_MIGRATE_COMPLIANCE_DATA != $intCurrentUserId ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		} else {
			$this->setDatabase( $objDatabase );

			$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

			$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, compliance_ruleset_id, compliance_type_id, compliance_level_id, compliance_item_id, screening_package_id, name, required_amount, screening_frequency_months, validate_expiration, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
								$strId . ', ' .
								$this->sqlCid() . ', ' .
								$this->sqlComplianceRulesetId() . ', ' .
								$this->sqlComplianceTypeId() . ', ' .
								$this->sqlComplianceLevelId() . ', ' .
								$this->sqlComplianceItemId() . ', ' .
								$this->sqlScreeningPackageId() . ', ' .
								$this->sqlName() . ', ' .
								$this->sqlRequiredAmount() . ', ' .
								$this->sqlScreeningFrequencyMonths() . ', ' .
								$this->sqlValidateExpiration() . ', ' .
								$this->sqlDeletedBy() . ', ' .
								$this->sqlDeletedOn() . ', ' .
								$this->sqlUpdatedBy() . ', ' .
								$this->sqlUpdatedOn() . ', ' .
								$this->sqlCreatedBy() . ', ' .
								$this->sqlCreatedOn() . ', ' .
								$this->sqlDetails() . ' ) ' . ' RETURNING id;';

			if( true == $boolReturnSqlOnly ) {
				return $strSql;
			} else {
				return $this->executeSql( $strSql, $this, $objDatabase );
			}
		}

	}

}
?>