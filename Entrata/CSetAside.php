<?php

class CSetAside extends CBaseSetAside {

	private $m_strBuildingName;
	protected $m_intPropertySubsidyTypeId;

	public function getPropertySubsidyTypeId() {
		return $this->m_intPropertySubsidyTypeId;
	}

	public function setPropertySubsidyTypeId( $intPropertySubsidyTypeId ) {
		$this->m_intPropertySubsidyTypeId = $intPropertySubsidyTypeId;
	}

	/**
	 * @return mixed
	 */
	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	/**
	 * @param mixed $strBuildingName
	 */
	public function setBuildingName( $strBuildingName ) {
		$this->m_strBuildingName = $strBuildingName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyBuildingId() {

		// @TODO: If property_subsidy_details.is_tax_credit_multiple_building_project is false then BuildingId won't be there
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyIncomeLimitAreaId( $boolIsHera = false ) {
		$boolIsValid = true;

		if( false == valId( $this->m_intSubsidyIncomeLimitAreaId ) ) {
			$boolIsValid = false;
			if( false == $boolIsHera ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_income_limit_area_id', 'HUD metro area is required.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_income_limit_area_id', 'HERA metro area is not available.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valSubsidySetAsideTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;
		return $boolIsValid;

	}

	public function valBuildingIdentificationNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strBuildingIdentificationNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'building_identification_number', 'Building Identification Number(BIN) is required.' ) );
		}

		// Building number must be 9 characters in length.
		if( 9 != strlen( $this->m_strBuildingIdentificationNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'building_identification_number', 'Building Identification Number (BIN) must be 9 characters in length.' ) );
		}

		return $boolIsValid;
	}

	public function valPlacedInServiceDate() {
		$boolIsValid = true;

		if( false == CValidation::valDate( $this->m_strPlacedInServiceDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'placed_in_service_date', 'Valid Placed In Service date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCreditStartDate() {
		$boolIsValid = true;
		if( false == CValidation::valDate( $this->m_strCreditStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_start_date', 'Valid Credit Start date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valComplianceStartDate() {
		$boolIsValid = true;

		if( false == CValidation::valDate( $this->m_strComplianceStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'compliance_start_date', 'Valid Compliance Start date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valExtendedUseStartDate() {
		$boolIsValid = true;

		if( false == CValidation::valDate( $this->m_strExtendedUseStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extended_use_start_date', 'Valid Extended Use Start date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMedianIncomePercent() {
		$boolIsValid = true;

		if( 1 < $this->m_fltMedianIncomePercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'median_income_percent', 'Median Income percentage should be less than or equal to 100%.' ) );
		}

		if( 0 > $this->m_fltMedianIncomePercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'median_income_percent', 'Median Income percentage should be greater than 0%.' ) );
		}

		return $boolIsValid;
	}

	public function valQualifiedUnitsPercent() {
		$boolIsValid = true;

		if( 1 < $this->m_fltQualifiedUnitsPercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qualified_units_percent', 'Qualified Units percentage should be less than or equal to 100%.' ) );
		}

		if( 0 > $this->m_fltQualifiedUnitsPercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qualified_units_percent', 'Qualified Units percentage should be greater than 0%.' ) );
		}

		return $boolIsValid;
	}

	public function valFirstYearLevelPercent() {
		$boolIsValid = true;

		if( 1 < $this->m_fltFirstYearLevelPercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_year_level_percent', 'First Year Level percentage should be less than or equal to 100%.' ) );
		}

		if( 0 > $this->m_fltFirstYearLevelPercent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_year_level_percent', 'First Year Level percentage should be greater than 0%.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsHera = false ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBuildingIdentificationNumber();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPlacedInServiceDate();
				$boolIsValid &= $this->valCreditStartDate();
				$boolIsValid &= $this->valComplianceStartDate();
				$boolIsValid &= $this->valExtendedUseStartDate();
				$boolIsValid &= $this->valMedianIncomePercent();
				$boolIsValid &= $this->valQualifiedUnitsPercent();
				$boolIsValid &= $this->valFirstYearLevelPercent();
				$boolIsValid &= $this->valSubsidyIncomeLimitAreaId( $boolIsHera );
				break;

			case 'property_level_set_aside':
				// We are soft deleting, so no need for validation
				if( NULL !== $this->m_strDeletedOn ) {
					break;
				}
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valPlacedInServiceDate();
				$boolIsValid &= $this->valCreditStartDate();
				$boolIsValid &= $this->valComplianceStartDate();
				$boolIsValid &= $this->valExtendedUseStartDate();
				$boolIsValid &= $this->valMedianIncomePercent();
				$boolIsValid &= $this->valQualifiedUnitsPercent();
				$boolIsValid &= $this->valFirstYearLevelPercent();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['building_name'] ) ) {
			$this->setBuildingName( $arrmixValues['building_name'] );
		}
		if( true == isset( $arrmixValues['property_subsidy_type_id'] ) ) $this->setPropertySubsidyTypeId( $arrmixValues['property_subsidy_type_id'] );
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>