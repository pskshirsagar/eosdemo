<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyWaiverTypes
 * Do not add any new functions to this class.
 */

class CSubsidyWaiverTypes extends CBaseSubsidyWaiverTypes {

	public static function fetchActiveSubsidyWaiverTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM subsidy_waiver_types WHERE is_published = TRUE ORDER BY order_num';
		return self::fetchSubsidyWaiverTypes( $strSql, $objDatabase );
	}

	public static function fetchAllSubsidyWaiverTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM subsidy_waiver_types ORDER BY order_num';
		return self::fetchSubsidyWaiverTypes( $strSql, $objDatabase );
	}

}
?>