<?php

class CRefundMethod extends CBaseRefundMethod {
	const STANDARD_INVOICE 					= 1;
	const SINGLE_REFUND_PRIMARY_RESIDENT 	= 2;
	const SINGLE_REFUND_MULTIPLE_RESIDENTS 	= 3;
	const MULTIPLE_REFUNDS 					= 4;
	const SINGLE_ACH_REFUND 				= 5;

	public static $c_arrintMultipleResidentRefundMethods = [ self::SINGLE_REFUND_MULTIPLE_RESIDENTS, self::MULTIPLE_REFUNDS, self::SINGLE_ACH_REFUND ];
	public static $c_arrstrRefundMethodConstantNames = [ self::SINGLE_REFUND_PRIMARY_RESIDENT => 'SINGLE_REFUND_PRIMARY_RESIDENT', self::SINGLE_REFUND_MULTIPLE_RESIDENTS => 'SINGLE_REFUND_MULTIPLE_RESIDENTS', self::MULTIPLE_REFUNDS => 'MULTIPLE_REFUNDS', self::SINGLE_ACH_REFUND => 'SINGLE_ACH_REFUND' ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>