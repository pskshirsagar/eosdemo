<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileMetadataKeys
 * Do not add any new functions to this class.
 */

class CFileMetadataKeys extends CBaseFileMetadataKeys {

    public static function fetchFileMetadataKeys( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CFileMetadataKey', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchFileMetadataKey( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CFileMetadataKey', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>