<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerSubsidyDetails
 * Do not add any new functions to this class.
 */

class CCustomerSubsidyDetails extends CBaseCustomerSubsidyDetails {

	public static function fetchCustomerSubsidyDetailByCustomerIdByCid( $intCustomerId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						customer_subsidy_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id = ' . ( int ) $intCustomerId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchCustomerSubsidyDetail( $strSql, $objDatabase );
	}

	public static function fetchCustomerSubsidyDetailByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						customer_subsidy_details
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchCustomerSubsidyDetails( $strSql, $objDatabase );
	}

	public static function fetchSpecialStatusCodesByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) ) return NULL;
		$arrmixSpecialStatusCodes = [];
		$strSql = '	SELECT
						csd.customer_id,
						array_to_string( array_agg( ssst.code ), \'\' ) AS special_status_code,
						sdt.code AS dependent_type
					FROM
						customer_subsidy_details csd
						LEFT JOIN subsidy_dependent_types sdt ON ( csd.subsidy_dependent_type_id = sdt.id )
						LEFT JOIN customer_subsidy_special_status_types cssst ON ( csd.cid = cssst.cid AND csd.customer_id = cssst.customer_id )
						LEFT JOIN subsidy_special_status_types ssst ON ( cssst.subsidy_special_status_type_id = ssst.id )
					WHERE
						csd.cid = ' . ( int ) $intCid . '
						AND csd.customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )
						AND csd.deleted_by IS NULL
                        AND csd.deleted_on IS NULL
					GROUP BY
						csd.customer_id, cssst.customer_id, sdt.code';

		$arrmixCustomerSubsidyDetails = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixCustomerSubsidyDetails ) ) {
			foreach( $arrmixCustomerSubsidyDetails as $arrmixCustomerSubsidyDetail ) {
				$strSpecialStatusCode = ( true == valStr( $arrmixCustomerSubsidyDetail['special_status_code'] ) ) ? $arrmixCustomerSubsidyDetail['special_status_code'] : '';
				$strDependentType     = ( true == valStr( $arrmixCustomerSubsidyDetail['dependent_type'] ) ) ? $arrmixCustomerSubsidyDetail['dependent_type'] : '';
				$arrmixSpecialStatusCodes[$arrmixCustomerSubsidyDetail['customer_id']] = $strSpecialStatusCode . $strDependentType;
			}
		}

		return $arrmixSpecialStatusCodes;

	}

	public static function fetchCustomerSubsidyDataByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCustomerIds ) || false == is_int( $intCid ) ) return NULL;

		$strSql = ' SELECT
						max ( CASE WHEN srt.code = \'I\' AND csrt.id IS NOT NULL THEN \'Y\'  END ) AS race_american,
						max ( CASE WHEN srt.code = \'A\' AND csrt.id IS NOT NULL THEN \'Y\' END ) AS race_asian,
						max ( CASE WHEN srt.code = \'B\' AND csrt.id IS NOT NULL THEN \'Y\'   END ) AS race_black,
						max ( CASE WHEN srt.code = \'H\' AND csrt.id IS NOT NULL THEN \'Y\' END ) AS race_native,
						max ( CASE WHEN srt.code = \'W\' AND csrt.id IS NOT NULL THEN \'Y\' END ) AS race_white,
						max ( CASE WHEN srt.code = \'O\' AND csrt.id IS NOT NULL THEN \'Y\' END ) AS race_other,
						max ( CASE WHEN ( srt.code = \'X\' AND csrt.id IS NOT NULL ) OR csrt.id IS NULL THEN \'Y\' END ) AS race_declined,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::ASIAN . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::ASIAN_INDIA . ' THEN \'Y\' END ) AS asian_india,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::ASIAN . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::JAPANESE . ' THEN \'Y\' END ) AS japanese,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::ASIAN . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::CHINESE . ' THEN \'Y\' END ) AS chinese,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::ASIAN . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::KOREN . ' THEN \'Y\' END ) AS korean,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::ASIAN . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::FILIPINO . ' THEN \'Y\' END ) AS filipino,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::ASIAN . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::VIETNAMESE . ' THEN \'Y\' END ) AS vietnamese,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::ASIAN . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::OTHER_ASIAN . ' THEN \'Y\' END ) AS other_asian,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::NATIVE_HAWAIIAN . ' THEN \'Y\' END ) AS native_hawaiian,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::SAMOAN . ' THEN \'Y\' END ) AS samoan,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::GUAMANIAN_CHAMORRO . ' THEN \'Y\' END ) AS guamanian,
						max ( CASE WHEN csrt.subsidy_race_type_id = ' . \CSubsidyRaceType::NATIVE_HAWAIIAN_OR_OTHER_PACIFIC_ISLANDER . ' AND csrt.subsidy_race_sub_type_id = ' . \CSubsidyRaceSubType::OTHER_PACIFIC_ISLANDER . ' THEN \'Y\' END ) AS other_pacific_islander,
						max ( CASE WHEN sets.code = \'1\' AND csd.subsidy_ethnicity_sub_type_id = ' . \CSubsidyEthnicitySubType::PUERTO_RICAN . ' THEN \'Y\' END ) AS puerto_rican,
						max ( CASE WHEN sets.code = \'1\' AND csd.subsidy_ethnicity_sub_type_id = ' . \CSubsidyEthnicitySubType::CUBAN . ' THEN \'Y\' END ) AS cuban,
						max ( CASE WHEN sets.code = \'1\' AND csd.subsidy_ethnicity_sub_type_id = ' . \CSubsidyEthnicitySubType::MEXICAN_AMERICAN_CHICANO . ' THEN \'Y\' END ) AS mexican,
						max ( CASE WHEN sets.code = \'1\' AND csd.subsidy_ethnicity_sub_type_id = ' . \CSubsidyEthnicitySubType::ANOTHER_HISPANIC_LATINO_OR_SPANISH_ORIGIN . ' THEN \'Y\' END ) AS another_hispanic,
						max ( CASE WHEN ( csd.is_part_time_student = TRUE AND cssst.id IS NOT NULL ) OR ssst.code = \'S\' THEN \'Y\' END ) AS student_status,
						csd.customer_id,
						sct.code As member_citizenship_code,
						csd.alien_registration_number As alien_registration_number,
						sets.code AS ethnicity,
						csd.subsidy_ssn_exception_type_id AS ssn_exception,
						array_to_string( array_agg(  csrt.subsidy_race_type_id  ), \',\' ) as subsidy_race_type_ids 
					FROM
						customer_subsidy_details csd
						LEFT JOIN customer_subsidy_race_types csrt ON ( csrt.cid = csd.cid AND csrt.customer_id = csd.customer_id )
						LEFT JOIN subsidy_race_types srt ON ( srt.id = csrt.subsidy_race_type_id  )
						LEFT JOIN customer_subsidy_special_status_types cssst ON ( cssst.customer_id = csd.customer_id AND cssst.cid = csd.cid )
						LEFT JOIN subsidy_special_status_types ssst ON ( ssst.id = cssst.subsidy_special_status_type_id )
						LEFT JOIN subsidy_citizenship_types sct ON ( sct.id = csd.subsidy_citizenship_type_id )
						LEFT JOIN subsidy_ethnicity_types sets ON ( sets.id = csd.subsidy_ethnicity_type_id )
						LEFT JOIN subsidy_ethnicity_sub_types sest ON ( sest.id = csd.subsidy_ethnicity_sub_type_id )
					WHERE
						csd.cid = ' . ( int ) $intCid . '
						AND csd.customer_id IN (' . implode( ',', $arrintCustomerIds ) . ')
						AND csd.deleted_by IS NULL
						AND csd.deleted_on IS NULL
					GROUP BY
						csd.customer_id,
						sct.code,
						csd.alien_registration_number,
						sets.code,
						csd.subsidy_ssn_exception_type_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>