<?php

class CMilitaryUnitCdmpStatusType extends CBaseMilitaryUnitCdmpStatusType {

	const EXISTING_UNIT 	    = 1;
	const RENOVATION_MINOR		= 2;
	const RENOVATION_MEDIUM		= 3;
	const RENOVATION_MAJOR	    = 4;
	const NEW_CONSTRUCTION	    = 5;
	const DEMOLITION	        = 6;

	public static function getMilitaryUnitCDMPStatusTypeNameByMilitaryUnitCDMPStatusTypeId( $intMilitaryUnitCDMPStatusTypeId ) {
		$strStatusTypeName = NULL;

		switch( $intMilitaryUnitCDMPStatusTypeId ) {

			case CMilitaryUnitCdmpStatusType::EXISTING_UNIT:
				$strStatusTypeName = 'Existing Unit';
				break;

			case CMilitaryUnitCdmpStatusType::RENOVATION_MINOR:
				$strStatusTypeName = 'Renovation - Minor';
				break;

			case CMilitaryUnitCdmpStatusType::RENOVATION_MEDIUM:
				$strStatusTypeName = 'Renovation - Medium';
				break;

			case CMilitaryUnitCdmpStatusType::RENOVATION_MAJOR:
				$strStatusTypeName = 'Renovation - Major';
				break;

			case CMilitaryUnitCdmpStatusType::NEW_CONSTRUCTION:
				$strStatusTypeName = 'New Construction';
				break;

			case CMilitaryUnitCdmpStatusType::DEMOLITION:
				$strStatusTypeName = 'Demolition';
				break;

			default:
				// default case
				break;
		}

		return $strStatusTypeName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>