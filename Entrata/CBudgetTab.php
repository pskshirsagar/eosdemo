<?php

use Psi\Eos\Entrata\CBudgetTabs;

class CBudgetTab extends CBaseBudgetTab {

	protected $m_intPropertyId;
	protected $m_strApprovalNote;

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBudgetId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBudgetWorksheetId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBudgetDataSourceTypeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsConfidential() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( $boolDirectSet && isset( $arrmixValues['property_id'] ) ) {
			$this->set( 'm_intPropertyId', trim( $arrmixValues['property_id'] ) );
		} elseif( isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( trim( $arrmixValues['property_id'] ) );
		}
	}

	/**
	 * set Functions
	 */

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	/**
	 * get Functions
	 */

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	/*
	 * other Function
	 */

	public function approveBudgetTab( $intCurrentUserId, $objClientDatabase ) {

		$boolIsApproveBudget = true;
		$this->setApprovedBy( $intCurrentUserId );
		$this->setApprovedOn( 'NOW()' );

		if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );

			return false;
		}

		$objBudget        = \Psi\Eos\Entrata\CBudgets::createService()->fetchBudgetByIdByCid( $this->getBudgetId(), $this->getCid(), $objClientDatabase );
		$arrobjBudgetTabs = ( array ) CBudgetTabs::createService()->fetchBudgetTabsByBudgetIdByCid( $this->getBudgetId(), $this->getCid(), $objClientDatabase );
		foreach( $arrobjBudgetTabs as $objBudgetTab ) {
			if( is_null( $objBudgetTab->getApprovedOn() ) ) {
				$boolIsApproveBudget = false;
				break;
			}
		}
		if( $boolIsApproveBudget ) {

			$objBudget->setApprovedBy( $intCurrentUserId );
			$objBudget->setApprovedOn( 'NOW()' );
			if( false == $objBudget->update( $intCurrentUserId, $objClientDatabase ) ) {
				$this->addErrorMsgs( $objBudget->getErrorMsgs() );

				return false;
			}
		}

		return true;
	}

	public function unapproveBudgetTab( $intCurrentUserId, $objClientDatabase ) {

		$objBudget = \Psi\Eos\Entrata\CBudgets::createService()->fetchBudgetByIdByCid( $this->getBudgetId(), $this->getCid(), $objClientDatabase );

		$objBudget->setApprovedBy( NULL );
		$objBudget->setApprovedOn( NULL );
		$this->setApprovedBy( NULL );
		$this->setApprovedOn( NULL );

		if( false == $this->update( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );

			return false;
		}
		if( false == $objBudget->update( $intCurrentUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $objBudget->getErrorMsgs() );

			return false;
		}

		return true;
	}

}

?>