<?php

class CGlAccountMaskAssociation extends CBaseGlAccountMaskAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlTreeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountMaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>