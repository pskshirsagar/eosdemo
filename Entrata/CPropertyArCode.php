<?php

use Psi\Core\AccountsReceivables\Collections\CPropertyArCodeAllocationRulesCollection;
use Psi\Eos\Entrata\CPropertyArCodeAllocationRules;

class CPropertyArCode extends CBasePropertyArCode {

	protected $m_objArCode;

	protected $m_strArCodeName;
	protected $m_strArCodeTypeName;
	protected $m_strDebitGlAccountName;
	protected $m_strCreditGlAccountName;
	protected $m_strAssociatedGlAccountName;

	protected $m_intArOriginId;
	protected $m_intArTriggerId;
	protected $m_intArCodeTypeId;
	protected $m_intDefaultAmount;
	protected $m_intGlAccountTypeId;

	protected $m_boolIsSystem;
	protected $m_boolIntegratedOnly;
	protected $m_boolIsPaymentInKind;
	protected $m_objPropertyArCodeAllocationRulesCollection;

	protected $m_intLedgerFilterId;

    /**
     * Get Functions
     */

    public function getArCodeName() {
    	return $this->m_strArCodeName;
    }

    public function getArCodeTypeName() {
    	return $this->m_strArCodeTypeName;
    }

    public function getAssociatedGlAccountName() {
    	return $this->m_strAssociatedGlAccountName;
    }

    public function getDefaultAmount() {
    	return $this->m_intDefaultAmount;
    }

    public function getDebitGlAccountName() {
    	return $this->m_strDebitGlAccountName;
    }

    public function getCreditGlAccountName() {
    	return $this->m_strCreditGlAccountName;
    }

    public function getArCodeTypeId() {
    	return $this->m_intArCodeTypeId;
    }

    public function getGlAccountTypeId() {
    	return $this->m_intGlAccountTypeId;
    }

    public function getIntegratedOnly() {
    	return $this->m_boolIntegratedOnly;
    }

    public function getArOriginId() {
    	return $this->m_intArOriginId;
    }

    public function getArTriggerId() {
    	return $this->m_intArTriggerId;
    }

    public function getIsPaymentInKind() {
    	return $this->m_boolIsPaymentInKind;
    }

    public function getIsSystem() {
	    return $this->m_boolIsSystem;
    }

	public function getLedgerFilterId() {
		return $this->m_intLedgerFilterId;
	}

	public function getPropertyArCodeAllocationRulesCollection() {
		return $this->m_objPropertyArCodeAllocationRulesCollection;
	}

	public function getArCode() {
    	return $this->m_objArCode;
	}

    /**
     * Set Functions
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['ar_code_name'] ) ) $this->setArCodeName( $arrmixValues['ar_code_name'] );
    	if( true == isset( $arrmixValues['ar_code_type_name'] ) ) $this->setArCodeTypeName( $arrmixValues['ar_code_type_name'] );
    	if( true == isset( $arrmixValues['associated_gl_account_name'] ) ) $this->setAssociatedGlAccountName( $arrmixValues['associated_gl_account_name'] );
    	if( true == isset( $arrmixValues['default_amount'] ) ) $this->setDefaultAmount( $arrmixValues['default_amount'] );
    	if( true == isset( $arrmixValues['debit_gl_account_name'] ) ) $this->setDebitGlAccountName( $arrmixValues['debit_gl_account_name'] );
    	if( true == isset( $arrmixValues['credit_gl_account_name'] ) ) $this->setCreditGlAccountName( $arrmixValues['credit_gl_account_name'] );
    	if( true == isset( $arrmixValues['ar_code_type_id'] ) ) $this->setArCodeTypeId( $arrmixValues['ar_code_type_id'] );
    	if( true == isset( $arrmixValues['gl_account_type_id'] ) ) $this->setGlAccountTypeId( $arrmixValues['gl_account_type_id'] );
    	if( true == isset( $arrmixValues['integrated_only'] ) ) $this->setIntegratedOnly( $arrmixValues['integrated_only'] );
    	if( true == isset( $arrmixValues['ar_origin_id'] ) ) $this->setArOriginId( $arrmixValues['ar_origin_id'] );
    	if( true == isset( $arrmixValues['ar_trigger_id'] ) ) $this->setArTriggerId( $arrmixValues['ar_trigger_id'] );
    	if( true == isset( $arrmixValues['is_payment_in_kind'] ) ) $this->setIsPaymentInKind( $arrmixValues['is_payment_in_kind'] );
	    if( true == isset( $arrmixValues['is_system'] ) ) $this->setIsSystem( $arrmixValues['is_system'] );
	    if( true == isset( $arrmixValues['ledger_filter_id'] ) ) $this->setLedgerFilterId( $arrmixValues['ledger_filter_id'] );
    	return;
    }

    public function setArCodeName( $strArCodeName ) {
    	$this->m_strArCodeName = ( string ) $strArCodeName;
    }

    public function setArCodeTypeName( $strArCodeTypeName ) {
    	$this->m_strArCodeTypeName = ( string ) $strArCodeTypeName;
    }

    public function setAssociatedGlAccountName( $strAssociatedGlAccountName ) {
    	$this->m_strAssociatedGlAccountName = ( string ) $strAssociatedGlAccountName;
    }

    public function setDefaultAmount( $intDefaultAmount ) {
    	$this->m_intDefaultAmount = ( int ) $intDefaultAmount;
    }

    public function setDebitGlAccountName( $strDebitGlAccountName ) {
    	$this->m_strDebitGlAccountName = ( string ) $strDebitGlAccountName;
    }

    public function setCreditGlAccountName( $strCreditGlAccountName ) {
    	$this->m_strCreditGlAccountName = ( string ) $strCreditGlAccountName;
    }

    public function setArCodeTypeId( $intArCodeTypeId ) {
    	$this->m_intArCodeTypeId = ( int ) $intArCodeTypeId;
    }

    public function setGlAccountTypeId( $intGlAccountTypeId ) {
    	$this->m_intGlAccountTypeId = CStrings::strToIntDef( $intGlAccountTypeId, NULL, false );
    }

    public function setIntegratedOnly( $boolIntegratedOnly ) {
    	$this->m_boolIntegratedOnly = $boolIntegratedOnly;
    }

    public function setArOriginId( $intArOriginId ) {
    	$this->m_intArOriginId = CStrings::strToIntDef( $intArOriginId, NULL, false );
    }

    public function setArTriggerId( $intArTriggerId ) {
    	$this->m_intArTriggerId = CStrings::strToIntDef( $intArTriggerId, NULL, false );
    }

    public function setIsPaymentInKind( $boolIsPaymentInKind ) {
    	$this->m_boolIsPaymentInKind = CStrings::strToBool( $boolIsPaymentInKind );
    }

	public function setIsSystem( $boolIsSystem ) {
		$this->m_boolIsSystem = CStrings::strToBool( $boolIsSystem );
	}

	public function setLedgerFilterId( $intLedgerFilterId ) {
		$this->m_intLedgerFilterId = CStrings::strToIntDef( $intLedgerFilterId, NULL, false );
	}

	public function setPropertyArCodeAllocationRulesCollection( CPropertyArCodeAllocationRulesCollection $objPropertyArCodeAllocationRulesCollection ) {
		$this->m_objPropertyArCodeAllocationRulesCollection = $objPropertyArCodeAllocationRulesCollection;
	}

	public function setArCode( $objArCode ) {
    	$this->m_objArCode = $objArCode;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreditGlAccountId( $objDatabase = NULL ) {
        $boolIsValid = true;

        if( false == valId( $this->getCreditGlAccountId() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'credit_gl_account_id', __( 'Credit GL account is required.' ), NULL ) );
        } elseif( true == valObj( $objDatabase, 'CDatabase' ) && false == $this->getIntegratedOnly() ) {

        	$arrobjGlAccount	= ( array ) CGlAccounts::fetchEligibleCreditGlAccountsByArCodeTypeIdByArOriginIdByArTriggerIdByCid( $this->getArCodeTypeId(), $this->getArOriginId(), $this->getArTriggerId(), $this->getCid(), $objDatabase, [ $this->getCreditGlAccountId() ] );
        	$objGlAccount		= array_pop( $arrobjGlAccount );

        	if( false == valObj( $objGlAccount, 'CGlAccount' ) ) {
        		$boolIsValid &= false;
        		$this->addErrorMsg( new CErrorMsg( NULL, 'credit_gl_account_id', __( 'The Gl account you have provided is not valid for this type of charge code as credit Gl account.' ), NULL ) );
        	}
        }

        return $boolIsValid;
    }

	public function valAssociatedChargeCode( $objDatabase ) : bool {
			$boolIsValid        = true;
			$arrintFetchArCodeIds = [];

			if( true == valId( $this->getWriteOffArCodeId() ) ) array_push( $arrintFetchArCodeIds, $this->getWriteOffArCodeId() );
			if( true == valId( $this->getRecoveryArCodeId() ) ) array_push( $arrintFetchArCodeIds, $this->getRecoveryArCodeId() );

			$objArCodesFilter = new CArCodesFilter( false, true );
			$objArCodesFilter->setArCodeIds( $arrintFetchArCodeIds );
			$arrobjArCodes    = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->getCid(), $objArCodesFilter, $objDatabase );

			if( true == valId( $this->getWriteOffArCodeId() ) && true == $arrobjArCodes[$this->getWriteOffArCodeId()]->getIntegratedOnly() ) {
				$strErrorMsg   = __( 'Entrata Core charge codes must be written off to an Entrata Core charge code.<br>' );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integrated_only', $strErrorMsg ) );
				$boolIsValid &= false;
			}

			if( true == valId( $this->getRecoveryArCodeId() ) && true == $arrobjArCodes[$this->getRecoveryArCodeId()]->getIntegratedOnly() ) {
				$strErrorMsg   = __( 'Entrata Core charge codes must be recovered to an Entrata Core charge code.' );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integrated_only', $strErrorMsg ) );
				$boolIsValid &= false;
			}
			return $boolIsValid;
	}

    public function valDebitGlAccountId( $objDatabase = NULL ) {
        $boolIsValid = true;

        if( false == valId( $this->getDebitGlAccountId() ) ) {
        	$boolIsValid &= false;
        	$this->addErrorMsg( new CErrorMsg( NULL, 'debit_gl_account_id', __( 'Debit GL account is required.' ), NULL ) );
        } elseif( true == valObj( $objDatabase, 'CDatabase' ) && false == $this->getIntegratedOnly() ) {
        	$arrobjGlAccount	= ( array ) CGlAccounts::fetchEligibleDebitGlAccountsByArCodeTypeIdByCid( $this->getArCodeTypeId(), $this->getCid(), $objDatabase, $this->getArCodeId(), [ $this->getDebitGlAccountId() ] );
        	$objGlAccount		= array_pop( $arrobjGlAccount );

        	if( false == valObj( $objGlAccount, 'CGlAccount' ) ) {
        		$boolIsValid &= false;
        		$this->addErrorMsg( new CErrorMsg( NULL, 'debit_gl_account_id', __( 'The Gl account you have provided is not valid for this type of charge code as debit Gl account.' ), NULL ) );
        	}
        }

        return $boolIsValid;
    }

    public function valArCodeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPriorityNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valWhiteListExists() : bool {
		$boolIsValid = true;

		if( valObj( $this->getArCode(), CArCode::class ) && !$this->getArCode()->isValidForAdvancedAllocation() && $this->hasWhitelist() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'white_list', __( 'Charge code should not have whitelist.' ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	private function getArCodeAutoAllocationTypeErrorMessage( $intArCodeAutoAllocationTypeId ) {
		$strErrorMessage = '';

		switch( $intArCodeAutoAllocationTypeId ) {
			case CArCodeAllocationType::RESTRICTED:
				$strErrorMessage = __( 'At least one charge code is required to enable restricted allocations.' );
				break;

			case CArCodeAllocationType::CUSTOM_PRIORITY:
				$strErrorMessage = __( 'At least one charge code is required to customize the allocation priority.' );
				break;

			default:
				// This is default case
				break;
		}

		return $strErrorMessage;
	}

	public function valWhiteListExistsForAutoAllocation() : bool {
		$boolIsValid = true;

		$arrintArCodeAutoAllocationTypes = [ CArCodeAllocationType::RESTRICTED, CArCodeAllocationType::CUSTOM_PRIORITY ];

		if( valObj( $this->getArCode(), CArCode::class ) && $this->getArCode()->isValidForAdvancedAllocation() && !$this->hasWhitelist() && in_array( $this->getArCodeAllocationTypeId(), $arrintArCodeAutoAllocationTypes ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'white_list', $this->getArCodeAutoAllocationTypeErrorMessage( $this->getArCodeAllocationTypeId() ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {

            case VALIDATE_INSERT:
	            $boolIsValid &= $this->valDebitGlAccountId( $objDatabase );
	            $boolIsValid &= $this->valCreditGlAccountId( $objDatabase );
	            break;

	        case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valDebitGlAccountId( $objDatabase );
            	$boolIsValid &= $this->valCreditGlAccountId( $objDatabase );
	            $boolIsValid &= $this->valAssociatedChargeCode( $objDatabase );
	            $boolIsValid &= $this->valWhiteListExists();
		        $boolIsValid &= $this->valWhiteListExistsForAutoAllocation();
            	break;

	        default:
	        	// default case
		        break;
        }

        return $boolIsValid;
    }

    public function mapKeyValuePair( $arrmixArCodeDetails ) {

    	$strKeyValuePair = '';

    	foreach( $arrmixArCodeDetails as $strKey => $strValue ) {
    		$strKeyValuePair .= \Psi\CStringService::singleton()->strtoupper( $strKey ) . '::' . $strValue . '~^~';
    	}

    	return $strKeyValuePair;
    }

    public function log( $objOldArCode, $objNewArCode, $arrmixArCodeDetails, $objCompanyUser, $objDatabase ) {

    	$objTableLog = new CTableLog();

    	$arrmixOldArCode 		= $objOldArCode->getPropertyArCodeDetails( $arrmixArCodeDetails );
    	$arrmixNewArCode		= $objNewArCode->getPropertyArCodeDetails( $arrmixArCodeDetails );
    	$strArCodeDescription	= __( 'Property Ar Code has been modified.' );

    	$strOldArCodeKeyValuePair = $objOldArCode->mapKeyValuePair( $arrmixOldArCode );
    	$strNewArCodeKeyValuePair = $objNewArCode->mapKeyValuePair( $arrmixNewArCode );

    	if( 0 != \Psi\CStringService::singleton()->strcasecmp( $strOldArCodeKeyValuePair, $strNewArCodeKeyValuePair ) ) {
    		$objTableLog->insert( $objCompanyUser->getId(), $objDatabase, 'property_ar_codes', $objNewArCode->getId(), 'UPDATE', $objNewArCode->getCid(), $strOldArCodeKeyValuePair, $strNewArCodeKeyValuePair, $strArCodeDescription );
    	}

    	return true;
    }

    public function getPropertyArCodeDetails( $arrmixArCodeDetails ) {

    	$arrmixArCode 	= [];

    	$arrmixArCode['credit_gl_account'] 			= ( true == array_key_exists( $this->getCreditGlAccountId(), $arrmixArCodeDetails['credit_gl_accounts'] ) ) ? $arrmixArCodeDetails['credit_gl_accounts'][$this->getCreditGlAccountId()]->getAccountName() : NULL;
    	$arrmixArCode['debit_gl_account']			= ( true == array_key_exists( $this->getDebitGlAccountId(), $arrmixArCodeDetails['debit_gl_accounts'] ) ) ? $arrmixArCodeDetails['debit_gl_accounts'][$this->getDebitGlAccountId()]->getAccountName() : NULL;
    	$arrmixArCode['modified_debit_gl_account']	= ( true == valId( $this->getModifiedDebitGlAccountId() ) && array_key_exists( $this->getModifiedDebitGlAccountId(), $arrmixArCodeDetails['modified_debit_gl_accounts'] ) ) ? $arrmixArCodeDetails['modified_debit_gl_accounts'][$this->getModifiedDebitGlAccountId()]->getAccountNumber() . ' - ' . $arrmixArCodeDetails['modified_debit_gl_accounts'][$this->getModifiedDebitGlAccountId()]->getAccountName() : NULL;

    	return $arrmixArCode;
    }

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrobjErrorMsgs = CPropertyArCodeAllocationRules::createService()->deletePropertyArCodeAllocationRulesByPropertyArCode( $this, $objDatabase );

    	if( true == valArr( $arrobjErrorMsgs ) || false == parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to delete property ar codes' ) ) );
			$this->addErrorMsg( new CErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) ) );
			$this->addErrorMsgs( $arrobjErrorMsgs );
			return false;
		}

		return true;
	}

	public function createPropertyArCodeAllocationRule( $intAllowedArCodeId ) : CPropertyArCodeAllocationRule {
		$objPropertyArCodeAllocationRule = new CPropertyArCodeAllocationRule();
		$objPropertyArCodeAllocationRule->setCid( $this->getCid() );
		$objPropertyArCodeAllocationRule->setArCodeId( $this->getArCodeId() );
		$objPropertyArCodeAllocationRule->setPropertyId( $this->getPropertyId() );
		$objPropertyArCodeAllocationRule->setAllowedArCodeId( $intAllowedArCodeId );

		return $objPropertyArCodeAllocationRule;
	}

	public function getOrFetchArCode( $objDatabase ) {
    	if( !valObj( $this->m_objArCode, CArCode::class ) ) {
			$this->m_objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $this->getArCodeId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objArCode;
	}

	public function hasPropertyArCodeAllocationRules() : bool {
		return valObj( $this->getPropertyArCodeAllocationRulesCollection(), CPropertyArCodeAllocationRulesCollection::class );
	}

	public function update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false, $boolUpdateWhiteList = false ) {

		$boolIsValid = true;

		if( false == parent::update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			$objDatabase->rollback();
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && true == $boolUpdateWhiteList && false == $this->deleteAndInsertPropertyArCodeAllocationRules( $objDatabase, $intCompanyUserId ) ) {
			$objDatabase->rollback();
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function deletePropertyArCodeAllocationRules( $objDatabase ) {
		$boolIsValid = true;
		if( valId( $this->getId() ) ) {
			if( true == valArr( CPropertyArCodeAllocationRules::createService()->deletePropertyArCodeAllocationRulesByPropertyArCode( $this, $objDatabase ) ) ) {
				$this->addErrorMsgs( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Property ArCode Allocation Rules failed to delete.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function insertPropertyArCodeAllocationRules( $intCurrentUserId, $objDatabase ) {
		$boolIsValid = true;

		if( $this->hasPropertyArCodeAllocationRules()
		    && $this->getPropertyArCodeAllocationRulesCollection()->count() > 0
			&& valId( $this->getArCodeId() ) ) {

			if( false == CPropertyArCodeAllocationRules::createService()->bulkInsert( $this->getPropertyArCodeAllocationRulesCollection()->getAll(), $intCurrentUserId, $objDatabase ) ) {
				$this->addErrorMsgs( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Property ArCode Allocation Rules failed to insert.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = true;
		if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			$objDatabase->rollback();
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid
		    && $this->hasPropertyArCodeAllocationRules()
		    && false == $this->insertPropertyArCodeAllocationRules( $intCurrentUserId, $objDatabase ) ) {

			$objDatabase->rollback();
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function loadPropertyArCodeAllocationRulesCollection( CDatabase $objDatabase ) {
		$arrobjPropertyArCodeAllocationRules = ( array ) CPropertyArCodeAllocationRules::createService()->fetchSortedPropertyArCodeAllocationRulesByArCodeIdByPropertyIdByCid( $this->getArCodeId(), $this->getPropertyId(), $this->getCid(), $objDatabase );

		$objPropertyArCodeAllocationRulesCollection = new CPropertyArCodeAllocationRulesCollection( $arrobjPropertyArCodeAllocationRules, 'AllowedArCodeId' );
		$this->setPropertyArCodeAllocationRulesCollection( $objPropertyArCodeAllocationRulesCollection );
	}

	public function useStandardAllocation() : bool {
		return !isset( $this->m_objPropertyArCodeAllocationRulesCollection )
		       || 0 == $this->m_objPropertyArCodeAllocationRulesCollection->count();
	}

	public function hasWhitelist() : bool {
		return !$this->useStandardAllocation();
	}

	/**
	 * @param      $objDatabase
	 * @param      $intCompanyUserId
	 * @return bool
	 */
	public function deleteAndInsertPropertyArCodeAllocationRules( $objDatabase, $intCompanyUserId ) : bool {
		$boolIsValid = true;

		if( false == $this->deletePropertyArCodeAllocationRules( $objDatabase ) ) {
			$boolIsValid &= false;
		}

		if( true == $boolIsValid
		    && $this->hasPropertyArCodeAllocationRules()
		    && false == $this->insertPropertyArCodeAllocationRules( $intCompanyUserId, $objDatabase ) ) {
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

}
?>