<?php

class CPropertyEmailSubscription extends CBasePropertyEmailSubscription {

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getCid() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valSystemEmailTypeId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getSystemEmailTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_email_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getPropertyId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCompanyEmployeeId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getCompanyEmployeeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>