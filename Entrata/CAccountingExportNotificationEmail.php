<?php

class CAccountingExportNotificationEmail extends CBaseAccountingExportNotificationEmail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountingExportTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSchedulerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddresses() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>