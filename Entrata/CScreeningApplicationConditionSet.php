<?php

use Psi\Eos\Entrata\CFileAssociations;

class CScreeningApplicationConditionSet extends CBaseScreeningApplicationConditionSet {

	protected $m_intConditionValueId;
	protected $m_strConditionName;
	protected $m_intConditionSubsetValueId;
	protected $m_intConditionTypeId;
	protected $m_intConditionAmountTypeId;
	protected $m_intBaseChargeCodeId;
	protected $m_intApplyToChargeCodeId;
	protected $m_intArTriggerId;
	protected $m_strConditionValue;
	protected $m_intConditionSubsetId;
	protected $m_intArTransactionId;
	protected $m_intScheduledChargeId;
	protected $m_intIsReverted;
	protected $m_intSubsetSatisfiedBy;
	protected $m_strSubsetSatisfiedOn;
	protected $m_intFinalValue;
	protected $m_intScreeningAvailableConditionId;

	protected $m_intRequiredItem;

	public function getConditionValueId() {
		return $this->m_intConditionValueId;
	}

	public function getConditionSubsetValueId() {
		return $this->m_intConditionSubsetValueId;
	}

	public function getConditionName() {
		return $this->m_strConditionName;
	}

	public function getConditionTypeId() {
		return $this->m_intConditionTypeId;
	}

	public function getConditionAmountTypeId() {
		return $this->m_intConditionAmountTypeId;
	}

	public function getBaseChargeCodeId() {
		return $this->m_intBaseChargeCodeId;
	}

	public function getApplyToChargeCodeId() {
		return $this->m_intApplyToChargeCodeId;
	}

	public function getArTriggerId() {
		 return $this->m_intArTriggerId;
	}

	public function getConditionValue() {
		return $this->m_strConditionValue;
	}

	public function getConditionSubsetId() {
		return $this->m_intConditionSubsetId;
	}

	public function getArTransactionId() {
		return $this->m_intArTransactionId;
	}

	public function getScheduledChargeId() {
		return $this->m_intScheduledChargeId;
	}

	public function getIsReverted() {
		return $this->m_intIsReverted;
	}

	public function getSubsetSatisfiedBy() {
		return $this->m_intSubsetSatisfiedBy;
	}

	public function getSubsetSatisfiedOn() {
		return $this->m_strSubsetSatisfiedOn;
	}

	public function getRequiredItem() {
		return $this->m_intRequiredItem;
	}

	public function getFinalValue() {
		return $this->m_intFinalValue;
	}

	public function setFinalValue( $intFinalValue ) {
		$this->m_intFinalValue = $intFinalValue;
	}

	public function setConditionValueId( $intConditionValueId ) {
		$this->m_intConditionValueId = $intConditionValueId;
	}

	public function setConditionSubsetValueId( $intConditionSubsetValueId ) {
		$this->m_intConditionSubsetValueId = $intConditionSubsetValueId;
	}

	public function setConditionName( $strConditionName ) {
		$this->m_strConditionName = $strConditionName;
	}

	public function setConditionTypeId( $intConditionTypeId ) {
		$this->m_intConditionTypeId = $intConditionTypeId;
	}

	public function setConditionAmountTypeId( $intConditionAmountTypeId ) {
		$this->m_intConditionAmountTypeId = $intConditionAmountTypeId;
	}

	public function setBaseChargeCodeId( $intBaseChargeCodeId ) {
		$this->m_intBaseChargeCodeId = $intBaseChargeCodeId;
	}

	public function setApplyToChargeCodeId( $intApplyToChargeCodeId ) {
		$this->m_intApplyToChargeCodeId = $intApplyToChargeCodeId;
	}

	public function setArTriggerId( $intArTriggerId ) {
		$this->m_intArTriggerId = $intArTriggerId;
	}

	public function setConditionValue( $strConditionValue ) {
		$this->m_strConditionValue = $strConditionValue;
	}

	public function setConditionSubsetId( $intConditionSubsetId ) {
		$this->m_intConditionSubsetId = $intConditionSubsetId;
	}

	public function setArTransactionId( $intArTransactionId ) {
		$this->m_intArTransactionId = $intArTransactionId;
	}

	public function setScheduledChargeId( $intScheduledChargeId ) {
		$this->m_intScheduledChargeId = $intScheduledChargeId;
	}

	public function setIsReverted( $intIsReverted ) {
		$this->m_intIsReverted = $intIsReverted;
	}

	public function setSubsetSatisfiedBy( $intSubsetSatisfiedBy ) {
		$this->m_intSubsetSatisfiedBy = $intSubsetSatisfiedBy;
	}

	public function setSubsetSatisfiedOn( $strSubsetSatisfiedOn ) {
		$this->m_strSubsetSatisfiedOn = $strSubsetSatisfiedOn;
	}

	public function setRequiredItem( $intRequiredItem ) {
		$this->m_intRequiredItem = $intRequiredItem;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['condition_value_id'] ) ) 			$this->setConditionValueId( $arrmixValues['condition_value_id'] );
		if( true == isset( $arrmixValues['condition_subset_value_id'] ) )   $this->setConditionSubsetValueId( $arrmixValues['condition_subset_value_id'] );
		if( true == isset( $arrmixValues['condition_name'] ) ) 				$this->setConditionName( $arrmixValues['condition_name'] );
		if( true == isset( $arrmixValues['condition_type_id'] ) ) 			$this->setConditionTypeId( $arrmixValues['condition_type_id'] );
		if( true == isset( $arrmixValues['condition_amount_type_id'] ) ) 	$this->setConditionAmountTypeId( $arrmixValues['condition_amount_type_id'] );
		if( true == isset( $arrmixValues['base_charge_code_id'] ) ) 		$this->setBaseChargeCodeId( $arrmixValues['base_charge_code_id'] );
		if( true == isset( $arrmixValues['ar_trigger_id'] ) ) 				$this->setArTriggerId( $arrmixValues['ar_trigger_id'] );
		if( true == isset( $arrmixValues['apply_to_charge_code_id'] ) ) 	$this->setApplyToChargeCodeId( $arrmixValues['apply_to_charge_code_id'] );
		if( true == isset( $arrmixValues['condition_value'] ) ) 			$this->setConditionValue( $arrmixValues['condition_value'] );
		if( true == isset( $arrmixValues['condition_subset_id'] ) ) 		$this->setConditionSubsetId( $arrmixValues['condition_subset_id'] );
		if( true == isset( $arrmixValues['ar_transaction_id'] ) ) 			$this->setArTransactionId( $arrmixValues['ar_transaction_id'] );
		if( true == isset( $arrmixValues['scheduled_charge_id'] ) ) 		$this->setScheduledChargeId( $arrmixValues['scheduled_charge_id'] );
		if( true == isset( $arrmixValues['is_reverted'] ) ) 				$this->setIsReverted( $arrmixValues['is_reverted'] );
		if( true == isset( $arrmixValues['subset_satisfied_by'] ) ) 		$this->setSubsetSatisfiedBy( $arrmixValues['subset_satisfied_by'] );
		if( true == isset( $arrmixValues['subset_satisfied_on'] ) ) 		$this->setSubsetSatisfiedOn( $arrmixValues['subset_satisfied_on'] );
		if( true == isset( $arrmixValues['screening_available_condition_id'] ) ) 		$this->setScreeningAvailableConditionId( $arrmixValues['screening_available_condition_id'] );

		if( true == isset( $arrmixValues['final_value'] ) ) 				$this->setFinalValue( $arrmixValues['final_value'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScreeningApplicationRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConditionSetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConditionSetName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiredItemsSubsetOne() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiredItemsSubsetTwo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSatisfiedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// This would eventually update the satisfied fields for the condition which are being selected by the user

	public function satisfyScreeningApplicationCondition( $objScreeningApplicationConditionValue, $intCompanyUserId, $objDatabase ) {

		$boolIsValid = false;

		if( false == valObj( $objScreeningApplicationConditionValue, 'CScreeningApplicationConditionValue' ) ) return $boolIsValid;

		$objScreeningApplicationConditionValue->setSatisfiedBy( $intCompanyUserId );
		$objScreeningApplicationConditionValue->setSatisfiedOn( 'NOW()' );

		switch( NULL ) {
			default:
				$objDatabase->begin();

				if( false == $objScreeningApplicationConditionValue->update( $intCompanyUserId, $objDatabase ) ) {
					trigger_error( 'Failed to update the screening application condition value object.', E_USER_ERROR );
					$objDatabase->rollback();
					break;
				}

				$objDatabase->commit();

				$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function revertScreeningApplicationConditions( $intApplicationId, $intCid, $intCompanyUserId, $objDatabase ) {

		$arrobjScreeningApplicationConditionSets = CScreeningApplicationConditionSets::fetchActiveScreeningApplicationConditionSetByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase );

		if( false == valArr( $arrobjScreeningApplicationConditionSets ) ) return false;

		$boolIsValid = true;

		// TODO - check for consistency .. wt if one of the condition set gets satisfied and other has error..
		$objScreeningApplicationConditionValue = new CScreeningApplicationConditionValue();
		foreach( $arrobjScreeningApplicationConditionSets as $objScreeningApplicationConditionSet ) {
			$boolIsValid &= $objScreeningApplicationConditionValue->revertScreeningApplicationConditionValues( [ $objScreeningApplicationConditionSet->getId() ], $objScreeningApplicationConditionSet->getCid(), $intCompanyUserId, $objDatabase );

			if( true == $boolIsValid ) {
				// Inactive screening condition set
				$objScreeningApplicationConditionSet->setIsActive( false );
				$objScreeningApplicationConditionSet->setSatisfiedOn( NULL );
				$objScreeningApplicationConditionSet->setSatisfiedBy( NULL );
				if( false == $objScreeningApplicationConditionSet->update( $intCompanyUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					return $boolIsValid;
				}
			}
		}

		return $boolIsValid;
	}

	public function generateScheduledChargeFromRates( $intArOriginId, $intArOriginReferenceId, $intScreeningAvailableConditionId, $intArTriggerId, $intApplicationId, $intCid, $objDatabase ) {

		$objApplication 	= CScreeningApplicationConditionSet::fetchApplication( $intApplicationId, $intCid, $objDatabase );
		$objProperty 		= CScreeningApplicationConditionSet::fetchProperty( $objApplication->getPropertyId(), $intCid, $objDatabase );

		$arrobjRate = \Psi\Eos\Entrata\CRates::createService()->fetchActiveRatesByArOriginReferenceIdByArOriginIdByPropertyIdByCid( $intScreeningAvailableConditionId, CArOrigin::RISK_PREMIUM, $objProperty->getId(), $intCid, $objDatabase );

		if( false == valArr( $arrobjRate ) ) {
			$arrobjRate = \Psi\Eos\Entrata\CRates::createService()->fetchActiveRatesByArOriginReferenceIdByArOriginIdByPropertyIdByCid( $intArOriginReferenceId, CArOrigin::RISK_PREMIUM, $objProperty->getId(), $intCid, $objDatabase );
		} else {
			$intArOriginReferenceId = $intScreeningAvailableConditionId;
		}

		if( true == valArr( $arrobjRate ) ) {
			$arrobjRate = array_values( $arrobjRate );

			$intArForumlaId = $arrobjRate[0]->getArFormulaId();
			$intArTriggerId = $arrobjRate[0]->getArTriggerId();
			if( CArFormula::FIXED_AMOUNT != $intArForumlaId ) {
				$intArTriggerId = 0;
			} else {
				$intArTriggerId = $arrobjRate[0]->getArTriggerId();
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'No rates are setup for the selected conditions with reference id: ' ) . ( int ) $intArOriginReferenceId ) );
			return false;
		}

		$objPropertyChargeSetting 	= \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingByPropertyIdByCid( $objApplication->getPropertyId(), $intCid, $objDatabase );
		$objLeaseProcesses			= \Psi\Eos\Entrata\CLeaseProcesses::createService()->fetchLeaseProcessByLeaseIdByCid( $objApplication->getLeaseId(), $intCid, $objDatabase );

		$arrobjPropertyPreferences = ( array ) CPropertyPreferences::fetchCustomPropertyPreferencesByKeysByPropertyIdByCid( [ 'ENABLE_SEMESTER_SELECTION', 'APPLY_ALL_RATES', 'RV_USE_SCHEDULED_CHARGE_FOR_RATES', 'EXCLUDE_ADD_ONS' ], $objProperty->getId(), $intCid, $objDatabase );
		$boolUseScheduledChargeForRates = ( ( false == array_key_exists( 'RV_USE_SCHEDULED_CHARGE_FOR_RATES', $arrobjPropertyPreferences ) ) || ( true == array_key_exists( 'RV_USE_SCHEDULED_CHARGE_FOR_RATES', $arrobjPropertyPreferences ) && 1 == $arrobjPropertyPreferences['RV_USE_SCHEDULED_CHARGE_FOR_RATES']->getValue() ) ) ? true : false;

		$objRate = NULL;

		if( true == $boolUseScheduledChargeForRates ) {
			$objRate = $arrobjRate[0];
			if( CArFormula::FIXED_AMOUNT == $arrobjRate[0]->getArFormulaId() ) {
				$fltRateAmount = $objRate->getRateAmount();
			} else {
				$fltRateAmount = CScreeningUtils::getRateFromScheduledCharges( $arrobjRate[0], $objApplication, $objDatabase );
			}

			if( $fltRateAmount <= 0 ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'Failed to load scheduled charge rate for reference id: {%d, 0}. Please contact support.', [ ( int ) $intArOriginReferenceId ] ) ) );
				return false;
			}

			$arrobjRateLogs = \Psi\Eos\Entrata\CRateLogs::createService()->fetchRateLogsByRateIdByPropertyIdByCid( $objRate->getId(), $objRate->getPropertyId(), $objRate->getCid(), $objDatabase );
			$arrobjRateLogs = array_values( $arrobjRateLogs );
			$objRateLog     = $arrobjRateLogs[0];

			if( true == valObj( $objRateLog, 'CRateLog' ) ) {
				$objRateLog->setRateAmount( $fltRateAmount );
				$objRateLog->setArOriginReferenceId( $intArOriginReferenceId );

				$objRate = $objRateLog;
			}

		} else {

			$objRateLogsFilter = new CRateLogsFilter();
			$objRateLogsFilter->setCids( [ $intCid ] );
			$objRateLogsFilter->setPropertyGroupIds( [ $objApplication->getPropertyId() ] );

			if( 0 < $intArTriggerId ) {
				$objRateLogsFilter->setArTriggerIds( [ $intArTriggerId ] );
				$objRateLogsFilter->setArOriginReferenceIds( [ $intArOriginReferenceId ] );
				$objRateLogsFilter->setArOriginIds( [ $intArOriginId ] );
			} else {
				$objRateLogsFilter->setArTriggerIds( [] );
				$objRateLogsFilter->setArOriginReferenceIds( [] );
				$objRateLogsFilter->setArOriginIds( [] );
			}

			$objRateLogsFilter->setLoadOptionalRates( true );

			// Condition For Student Property

			$arrintSpaceConfigurationIds = [];

			if( true == valObj( $objProperty, 'CProperty', 'PropertyTypeId' ) && true == in_array( COccupancyType::STUDENT, $objProperty->getOccupancyTypeIds() ) ) {
				$boolEnableSemesterSelection = ( true == array_key_exists( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) && 1 == $arrobjPropertyPreferences['ENABLE_SEMESTER_SELECTION']->getValue() ) ? true : false;

				if( true == $boolEnableSemesterSelection ) {
					$objRateLogsFilter->setIsStudentProperty( true );
					$objRateLogsFilter->setLeaseStartWindowId( $objApplication->getLeaseStartWindowId() );
					$objRateLogsFilter->getIsEntrata( true );

					$arrintSpaceConfigurationIds = ( true == valId( $objApplication->getSpaceConfigurationId() ) ) ? [ $objApplication->getSpaceConfigurationId() ] : [];
				}
			}

			if( false == is_null( $objApplication->getUnitSpaceId() ) ) {
				$objRateLogsFilter->setUnitSpaceIds( [ $objApplication->getUnitSpaceId() ] );
			} elseif( false == is_null( $objApplication->getUnitTypeId() ) ) {
				$objRateLogsFilter->setUnitTypeIds( [ $objApplication->getUnitTypeId() ] );
			}

			$boolUseAllRates = false;

			// WE HAVE TO CHECK IT IT MIGHT NOT BE WITH AVAIALBLE CONDTION THERE....
			if( true == array_key_exists( 'APPLY_ALL_RATES', $arrobjPropertyPreferences ) && true == $arrobjPropertyPreferences['APPLY_ALL_RATES']->getValue() ) {
				$arrobjLeaseAssociations = \Psi\Eos\Entrata\CLeaseAssociations::createService()->fetchLeaseAssociationByArOriginIdsByArOriginReferenceIdByLeaseIntervalIdByCid( [ CArOrigin::RISK_PREMIUM ], $intArOriginReferenceId, $objApplication->getLeaseIntervalId(), $objApplication->getCid(), $objDatabase );
				if( true == valArr( $arrobjLeaseAssociations ) ) {
					$boolUseAllRates = true;
					$objRateLogsFilter->setLeaseIntervalIds( [ $objApplication->getLeaseIntervalId() ] );
				}
			}

			$boolExcludeAddOns = ( true == array_key_exists( 'EXCLUDE_ADD_ONS', $arrobjPropertyPreferences ) && true == $arrobjPropertyPreferences['EXCLUDE_ADD_ONS']->getValue() ) ? true : false;

			// If the property setting is ON do not pass addons for rate log filter
			if( true == $boolUseAllRates && true == $boolExcludeAddOns ) {
				$objRateLogsFilter->setArOriginIds( CArOrigin::$c_arrintArOriginIdsWithoutAddOns );
			}

			if( false !== \Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase ) ) {
				$arrintLeaseTermIds = ( true == valId( $objApplication->getLeaseTermId() ) ) ? [ $objApplication->getLeaseTermId() ] : [];
				$arrobjRateLogs     = ( array ) \Psi\Eos\Entrata\CRateLogs::createService()->fetchTempProspectRates( $objDatabase, false, false, $arrintLeaseTermIds, NULL, NULL, NULL, $arrintSpaceConfigurationIds );

				if( true == valArr( $arrobjRateLogs ) ) {

					$objRate = NULL;
					$objLeaseTermRate = NULL;
					$fltRateAmount    = 0;
					$arrmixLeaseTermRateAmounts = [];

					if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjRateLogs ) ) {
						foreach( $arrobjRateLogs as $objRateLog ) {
							if( $intArOriginReferenceId == $objRateLog->getArOriginReferenceId() ) {
								if( true == is_null( $objRate ) ) {
									$objRate = $objRateLog;
								}

								if( $objRateLog->getRateAmount() > 0 ) {
									$arrmixLeaseTermRateAmounts[$objRateLog->getLeaseTermId()] = $objRateLog->getRateAmount();
								}

								if( true == valId( $objApplication->getLeaseTermId() ) && $objApplication->getLeaseTermId() == $objRateLog->getLeaseTermId() ) {
									$objLeaseTermRate = $objRateLog;

									if( false == $boolUseAllRates ) {
										break;
									}
								}
							}
						}

						if( true == $boolUseAllRates ) {
							$intLeaseTermCounts = \Psi\Libraries\UtilFunctions\count( $arrmixLeaseTermRateAmounts );
							foreach( $arrmixLeaseTermRateAmounts as $fltLeaseTermRateAmount ) {
								$fltRateAmount += $fltLeaseTermRateAmount;
							}

							if( $fltRateAmount <= 0 ) {
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, ' Selected condition sets have a $0 value and cannot be calculated. Lease Terms: ' . ( int ) $intLeaseTermCounts . ', Rate Counts : ' . ( int ) $intTotalRateLogs . ', Please review lease term setup' ) );
								return false;
							}
						}

					} else {
						$arrobjRateLogs = array_values( $arrobjRateLogs );
						$objRateLog     = $arrobjRateLogs[0];
						if( $intArOriginReferenceId == $objRateLog->getArOriginReferenceId() ) {
							$fltRateAmount = $objRateLog->getRateAmount();
							$objRate       = $objRateLog;
						}
					}

					if( true == valObj( $objLeaseTermRate, 'CRateLog' ) ) {
						$objRate = $objLeaseTermRate;
					}

					if( false == valObj( $objRate, 'CRateLog' ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Failed to load rates for reference id : ' . ( int ) $intArOriginReferenceId . ' And cid : ' . ( int ) $intCid ) );
						return false;
					}

					if( true == $boolUseAllRates ) {
						$objRate->setRateAmount( $fltRateAmount );
					}

				}
			}
		}

		if( true == valObj( $objRate, 'CRateLog' ) ) {

			$objScheduledCharge = $objRate->createScheduledCharge();
			$objScheduledCharge->setLeaseId( $objApplication->getLeaseId() );
			$objScheduledCharge->setLeaseIntervalId( $objApplication->getLeaseIntervalId() );
			$objScheduledCharge->setArCodeId( $objRate->getArCodeId() );
			$objScheduledCharge->setArCodeTypeId( \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeTypeIdByArCodeIdByCid( $objRate->getArCodeId(), $intCid, $objDatabase ) );
			$objScheduledCharge->setChargeStartDate( date( 'm/d/Y' ) );

			$objArCode = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeByIdByCid( $objRate->getArCodeId(), $intCid, $objDatabase );

			if( CArTrigger::MONTHLY == $objScheduledCharge->getArTriggerId() ) {
				$objScheduledCharge->setStartsWithLease( true );
				$boolIsImmediateMoveIn = false;

				$objLeaseInterval = \Psi\Eos\Entrata\CLeaseIntervals::createService()->fetchLeaseIntervalByIdByCid( $objApplication->getLeaseIntervalId(), $objApplication->getCid(), $objDatabase );

				if( true == valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {
					$boolIsImmediateMoveIn = true;
				}

				if( false == \Psi\Eos\Entrata\CScheduledCharges::createService()->determineProration( $objPropertyChargeSetting, $objArCode->getProrateCharges(), $boolIsImmediateMoveIn ) ) {
					$objScheduledCharge->setChargeStartDate( date( 'm/1/Y', strtotime( $objApplication->getLeaseStartDate() ) ) );
				} else {

					$objScheduledCharge->setChargeStartDate( $objApplication->getLeaseStartDate() );

					if( strtotime( $objLeaseProcesses->getMoveInDate() ) <= strtotime( $objApplication->getLeaseStartDate() ) ) {
						$objScheduledCharge->setChargeStartDate( $objLeaseProcesses->getMoveInDate() );

						if( false == $objPropertyChargeSetting->getChargeMoveInDay() ) {
							$objScheduledCharge->setChargeStartDate( date( 'm/d/Y', strtotime( '+1 day', strtotime( $objScheduledCharge->getChargeStartDate() ) ) ) );
						}
					}
				}
			}

			return $objScheduledCharge;
		}

		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, 'Failed to load calculated rates for reference id: ' . ( int ) $intArOriginReferenceId . '. Please contact support.' ) );
	}

	public function handleIncreaseScheduledCharges( $objScreeningApplicationConditionValue, $intArOriginReferenceId, $intScreeningAvailableConditionId, $intApplicationId, $intCompanyUserId, $objDatabase ) {

		if( false == valObj( $objScreeningApplicationConditionValue, 'CScreeningApplicationConditionValue' ) ) return NULL;

		$objApplication 		= CScreeningApplicationConditionSet::fetchApplication( $intApplicationId, $objScreeningApplicationConditionValue->getCid(), $objDatabase );

		if( CScreeningPackageConditionType::INCREASE_RENT == $objScreeningApplicationConditionValue->getConditionTypeId() ) {
			$intArTriggerId = CArTrigger::MONTHLY;
		} else {
			$intArTriggerId = CArTrigger::MOVE_IN;
		}

		$objScheduledCharge = $this->generateScheduledChargeFromRates( CArOrigin::RISK_PREMIUM, $intArOriginReferenceId, $intScreeningAvailableConditionId, $intArTriggerId, $intApplicationId, $objScreeningApplicationConditionValue->getCid(), $objDatabase );

		if( false == valObj( $objScheduledCharge, 'CScheduledCharge' ) )  return false;

		if( $objScheduledCharge->getChargeAmount() <= 0 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'Selected condition sets have a $0 value and cannot be calculated. Please review screening condition setup' ) ) );
			return false;
		}

		if( CScreeningPackageConditionType::REPLACE_SECURITY_DEPOSIT == $objScreeningApplicationConditionValue->getConditionTypeId() ) {
			$fltCurrentDepositAmount = 0;
			$arrobjDepositScheduledCharges = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchActiveScheduledChargesByLeaseIdByLeaseIntervalIdsByArTriggerIdsByArCodeGroupIdByArCodeTypeIdByCid( [ $objApplication->getLeaseIntervalId() ], $objApplication->getCid(), $objDatabase, [], NULL, NULL, $objScheduledCharge->getArCodeId() );

			if( true == valArr( $arrobjDepositScheduledCharges ) ) {
				foreach( $arrobjDepositScheduledCharges as $objDepositScheduledCharge ) {
					if( 1 == $objDepositScheduledCharge->getArOriginId() ) {
						// Consider only the base charge amounts
						$fltCurrentDepositAmount += $objDepositScheduledCharge->getChargeAmount();
					}
				}

				// Find the difference of New Value and Current Value for Replacement Deposit
				$fltChargeAmount = $objScheduledCharge->getChargeAmount() - $fltCurrentDepositAmount;

				if( 0 >= $fltChargeAmount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'Unable to replace Security Deposit as the calculated amount is less than the current Security Deposit.' ) ) );
					return false;
				}

				$objScheduledCharge->setChargeAmount( $fltChargeAmount );
			}
		}

		$objScheduledCharge->setArFormulaId( CArFormula::FIXED_AMOUNT );

		$objDatabase->begin();

		if( false == $objScheduledCharge->validate( VALIDATE_INSERT, $objDatabase ) || false == $objScheduledCharge->insert( $intCompanyUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		} else {
			$objScreeningApplicationConditionValue->setScheduledChargeId( $objScheduledCharge->getId() );
			$objScreeningApplicationConditionValue->setFinalValue( $objScheduledCharge->getChargeAmount() );

			if( false == $objScreeningApplicationConditionValue->update( $intCompanyUserId, $objDatabase ) ) {
				$objDatabase->rollback();
				return false;
			}

			$objDatabase->commit();
			return true;
		}
	}

	public function processScreeningApplicationConditions( $arrobjScreeningApplicationConditionValues, $intApplicationId, $strSecureBaseName, $intCompanyUserId, $objDatabase ) {
		$boolIsValid = false;

		if( false == valArr( $arrobjScreeningApplicationConditionValues ) ) return $boolIsValid;

		$arrintScreeningApplicationConditionSetIds = [];

		foreach( $arrobjScreeningApplicationConditionValues as $objScreeningApplicationConditionValue ) {
			$boolIsScheduledChargeProcessed = ( CScreeningPackageConditionValueType::AMOUNT == $objScreeningApplicationConditionValue->getConditionValueTypeId() && false == $objScreeningApplicationConditionValue->getProcessAutomatically() ) ? true : false;
			$boolIsProcessed = false;
			$intCid = $objScreeningApplicationConditionValue->getCid();

			if( false == is_null( $objScreeningApplicationConditionValue->getSatisfiedBy() ) || ( false == is_null( $objScreeningApplicationConditionValue->getSatisfiedOn() ) ) ) continue;

			switch( $objScreeningApplicationConditionValue->getConditionTypeId() ) {

				case CTransmissionConditionType::VERIFY_IDENTITY:
				case CTransmissionConditionType::REQUIRE_CERTIFIED_FUNDS:
				case CTransmissionConditionType::CUSTOM_SCREENING_CONDITION:
				case CTransmissionConditionType::APRROVED_WITH_QUALIFYING_ROOMMATE:
					$boolIsConditionSatisfied = $this->satisfyScreeningApplicationCondition( $objScreeningApplicationConditionValue, $intCompanyUserId, $objDatabase );
					break;

				case CTransmissionConditionType::INCREASE_DEPOSIT:
				case CTransmissionConditionType::REPLACE_DEPOSIT:
				case CTransmissionConditionType::INCREASE_RENT:
				case CTransmissionConditionType::OTHER_CHARGES:
					$objResidentVerifyLibrary = new CResidentVerifyLibrary();
					$intPackageSubsetValueId            = $objScreeningApplicationConditionValue->getConditionSubsetValueId();
					$intScreeningAvailableConditionId   = $objScreeningApplicationConditionValue->getScreeningAvailableConditionId();

					if( true == $boolIsScheduledChargeProcessed ) {
						$boolIsConditionSatisfied   = $this->satisfyScreeningApplicationCondition( $objScreeningApplicationConditionValue, $intCompanyUserId, $objDatabase );
					} else {
						$boolIsProcessed  = $this->handleIncreaseScheduledCharges( $objScreeningApplicationConditionValue, $intPackageSubsetValueId, $intScreeningAvailableConditionId, $intApplicationId, $intCompanyUserId, $objDatabase );

						if( false == $boolIsProcessed ) return false;

						$boolIsConditionSatisfied   = $this->satisfyScreeningApplicationCondition( $objScreeningApplicationConditionValue, $intCompanyUserId, $objDatabase );
					}
					break;

                case CTransmissionConditionType::UPFRONT_RENT:
                    $objApplication = $this->fetchApplication( $intApplicationId, $intCid, $objDatabase );
                    if( false == valObj( $objApplication, 'CApplication' ) ) return false;

                    // Get the default installment plan
                    $objDefaultInstallmentPlan = \Psi\Eos\Entrata\CInstallmentPlans::createService()->fetchScreeningDefaultPropertyLeaseTermInstallmentPlan( $objApplication, $objDatabase );

                    if( false == valObj( $objDefaultInstallmentPlan, 'CInstallmentPlan' ) ) {
                        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'No default installment plan is available for Upfront Rent' ) ) );
                        return false;
                    }

                    // As discussed with Stuident team , if we have multiple charges for multiple ar codes we are not allowing to apply the functionality
                    $intCountOfScheduledChargesWithDuplicateArCode = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchCountOfScheduledChargesWithDuplicateChargeCodeByLeaseIntervalIdByCid( $objApplication->getLeaseIntervalId(), $objApplication->getCid(), $objDatabase );
                    if( ( int ) $intCountOfScheduledChargesWithDuplicateArCode > 1 ) {
                        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, __( 'The instalment plan cannot be applied, this application has multiple charges of the same charge code.' ) ) );
                        return false;
                    }

                    // This function is use for storing the scheduled charges as per the selected installment plan
                    $boolIsProcessed  = $this->handleInstallmentPlanScheduledCharges( $objApplication, $objDefaultInstallmentPlan->getId(), $intCompanyUserId, $objDatabase );

                    if( false == $boolIsProcessed ) return false;
                    $boolIsConditionSatisfied   = $this->satisfyScreeningApplicationCondition( $objScreeningApplicationConditionValue, $intCompanyUserId, $objDatabase );
                    break;

				case CTransmissionConditionType::REQUIRE_GUARANTOR:
						$boolIsConditionSatisfied = $this->processGuarantorCondition( $objScreeningApplicationConditionValue, $intApplicationId, $intCompanyUserId, $objDatabase );
					break;

				case CTransmissionConditionType::VERIFY_RENTAL_COLLECTION_PROOF:
						$boolIsConditionSatisfied = $this->processRentalCollectionCondition( $objScreeningApplicationConditionValue, $intApplicationId, $intCompanyUserId, $objDatabase );
					break;

				default:

					// added default case
			}

			// need to check when multiple conditions are there and one of them gets satsified

			if( true == $boolIsConditionSatisfied ) {
				$boolIsValid = true;
			}

			$arrintScreeningApplicationConditionSetIds[] = $objScreeningApplicationConditionValue->getScreeningApplicationConditionSetId();
		}

		$arrintScreeningApplicationConditionSetIds = array_unique( $arrintScreeningApplicationConditionSetIds );

		$intSatisfiedConditionSetCounter = 0;

		foreach( $arrintScreeningApplicationConditionSetIds as $intScreeningApplicationConditionSetId ) {

			$objScreeningApplicationConditionSet = CScreeningApplicationConditionSets::fetchScreeningApplicationConditionSetByIdByCid( $intScreeningApplicationConditionSetId, $objScreeningApplicationConditionValue->getCid(), $objDatabase );

			if( false == valObj( $objScreeningApplicationConditionSet, 'CScreeningApplicationConditionSet' ) ) {
				trigger_error( 'Failed to load condition set', E_USER_ERROR );
				exit;
			}

			$arrobjScreeningApplicationConditionSets = CScreeningApplicationConditionSets::isConditionSubsetsSatisfied( $objScreeningApplicationConditionSet->getConditionSetId(), $objScreeningApplicationConditionSet->getApplicationId(), $objScreeningApplicationConditionSet->getCid(), $objDatabase );
			$arrobjScreeningApplicationConditionSets = rekeyObjects( 'ConditionSubsetId', $arrobjScreeningApplicationConditionSets, $boolHasMultipleObjectsWithSameKey = true );

			$boolIsValid					= true;
			$boolIsConditionSetSatisfied	= false;
			$intRequiredItemsSubsetOne		= $intRequiredItemsSubsetTwo = $intSubsetOneSatisfiedCounter = $intSubsetTwoSatisfiedCounter = 0;

			if( true == valArr( $arrobjScreeningApplicationConditionSets ) ) {
				$intSubsetCounter   = 1;

				foreach( $arrobjScreeningApplicationConditionSets as $arrobjScreeningApplicationConditionSubSets ) {
					foreach( $arrobjScreeningApplicationConditionSubSets as $objScreeningApplicationConditionSubSet ) {
						if( 1 == $intSubsetCounter ) {
							// need to add logic so that this would be exceuted only for the first iteration... may be add a counter..
							$intRequiredItemsSubsetOne = ( 0 == $objScreeningApplicationConditionSubSet->getRequiredItemsSubsetOne() ) ? \Psi\Libraries\UtilFunctions\count( $arrobjScreeningApplicationConditionSubSets ) : $objScreeningApplicationConditionSubSet->getRequiredItemsSubsetOne();
							if( false == is_null( $objScreeningApplicationConditionSubSet->getSubsetSatisfiedBy() ) && false == is_null( $objScreeningApplicationConditionSubSet->getSubsetSatisfiedOn() ) )
								$intSubsetOneSatisfiedCounter++;
						} else {
							// need to add logic so that this would be exceuted only for the first iteration...may be add a counter..
							$intRequiredItemsSubsetTwo = ( 0 == $objScreeningApplicationConditionSubSet->getRequiredItemsSubsetTwo() ) ? \Psi\Libraries\UtilFunctions\count( $arrobjScreeningApplicationConditionSubSets ) : $objScreeningApplicationConditionSubSet->getRequiredItemsSubsetTwo();
							if( false == is_null( $objScreeningApplicationConditionSubSet->getSubsetSatisfiedBy() ) && false == is_null( $objScreeningApplicationConditionSubSet->getSubsetSatisfiedOn() ) )
								$intSubsetTwoSatisfiedCounter++;
						}
					}

					$intScreeningConditionOperandTypeId = $objScreeningApplicationConditionSubSet->getScreeningConditionOperandTypeId();
					$intSubsetCounter++;
				}

				switch( $intScreeningConditionOperandTypeId ) {
					case NULL:
					case CScreeningPackageOperandType::SCREENING_PACKAGE_OPERAND_TYPE_AND:
						$boolIsConditionSetSatisfied = ( $intSubsetOneSatisfiedCounter >= $intRequiredItemsSubsetOne && $intSubsetTwoSatisfiedCounter >= $intRequiredItemsSubsetTwo ) ? true : false;
						break;

					case CScreeningPackageOperandType::SCREENING_PACKAGE_OPERAND_TYPE_OR:
						$boolIsConditionSetSatisfied = ( $intSubsetOneSatisfiedCounter >= $intRequiredItemsSubsetOne || $intSubsetTwoSatisfiedCounter >= $intRequiredItemsSubsetTwo ) ? true : false;
						break;

					default:
						// added default case
						break;
				}

				if( true == $boolIsConditionSetSatisfied ) {

					$objScreeningApplicationConditionSet->setSatisfiedBy( $intCompanyUserId );
					$objScreeningApplicationConditionSet->setSatisfiedOn( 'NOW()' );

                    $boolIsValid = true;

					switch( NULL ) {
						default:

							$objDatabase->begin();

							if( false == $objScreeningApplicationConditionSet->update( $intCompanyUserId, $objDatabase ) ) {
								$objDatabase->rollback();
								$boolIsValid &= false;
								trigger_error( 'Unable to update record for satisfying screening condition set!' );
								break;
							} else {
								$intSatisfiedConditionSetCounter = $intSatisfiedConditionSetCounter + 1;
							}

							$objDatabase->commit();
							if( true == valStr( $strSecureBaseName ) ) {
								echo "<script language='javascript'>
										ajaxRequest( 'application_status_actions', '" . $strSecureBaseName . '/?module=applicationxxx&action=view_application_status_actions&application[id]=' . ( int ) $intApplicationId . "');
									</script>";

								echo '<script language="javascript">
										psi.patterns.ajaxRequest( {
											url: "' . $strSecureBaseName . '/?module=application_leasexxx&action=view_application_lease&reload_preferences=1' . '",
											strElementSelector:"#application_lease_tab_content"

										} );
										</script>';
							}
					}
				} else {
					$objDatabase->begin();

					$objScreeningApplicationConditionSet->setSatisfiedBy( NULL );
					$objScreeningApplicationConditionSet->setSatisfiedOn( NULL );

					if( false == $objScreeningApplicationConditionSet->update( $intCompanyUserId, $objDatabase ) ) {
						$objDatabase->rollback();
						$boolIsValid &= false;
						trigger_error( 'Unable to update record for satisfying screening condition set!' );
						break;
					}

					$objDatabase->commit();
				}
			}
		}

		if( \Psi\Libraries\UtilFunctions\count( $arrintScreeningApplicationConditionSetIds ) == $intSatisfiedConditionSetCounter ) {
			$objResidentVerifyLibrary = new CResidentVerifyLibrary();
			$objResidentVerifyLibrary->autoApproveApplication( $intApplicationId, $intCid, $intCompanyUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function revertScheduledCharges( $objScreeningApplicationRevertCondition, $objProperty = NULL, $intLeaseIntervalId, $intCompanyUserId, $objDatabase ) {

		if( false == valObj( $objScreeningApplicationRevertCondition, 'CScreeningApplicationConditionSet' ) ) return false;

		$intArOriginReferenceId = ( false == is_null( $objScreeningApplicationRevertCondition->getScreeningAvailableConditionId() ) ) ? $objScreeningApplicationRevertCondition->getScreeningAvailableConditionId() : $objScreeningApplicationRevertCondition->getConditionSubsetValueId();

		if( CScreeningPackageConditionType::INCREASE_RENT == $objScreeningApplicationRevertCondition->getConditionTypeId() ) {
			$intArTriggerId = CArTrigger::MONTHLY;
			$boolRentCharge = true;
		} else {

			// For other charges it may be other than move-in

			$intArTriggerId = $objScreeningApplicationRevertCondition->getArTriggerId();
			if( true == is_null( $intArTriggerId ) ) {
				$intArTriggerId = CArTrigger::MOVE_IN;
			}
			$boolRentCharge = false;
		}

		$objScheduledCharge = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargeByIdByLeaseIntervalIdByArOriginIdByArOriginReferenceIdByCid( $objScreeningApplicationRevertCondition->getScheduledChargeId(), $intLeaseIntervalId, CArOrigin::RISK_PREMIUM, $intArOriginReferenceId, $objScreeningApplicationRevertCondition->getCid(), $objDatabase );
		$objDatabase->begin();

		if( false == valObj( $objScheduledCharge, 'CScheduledCharge' ) ) {

			// Pre Megarates records

			$intChargeCodeId		= $objScreeningApplicationRevertCondition->getBaseChargeCodeId();
			$objApplication 		= CScreeningApplicationConditionSet::fetchApplication( $objScreeningApplicationRevertCondition->getApplicationId(), $objScreeningApplicationRevertCondition->getCid(), $objDatabase );
			$arrobjOldScheduledCharges = ( array ) \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchScheduledChargesByLeaseIntervalIdByArTriggerIdsByCid( $objApplication->getLeaseIntervalId(), [ $intArTriggerId ], $objScreeningApplicationRevertCondition->getCid(), $objDatabase );

			if( false == $boolRentCharge ) {

				// Increase security deposit or Other charge

				$objArTransaction = \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionByIdByCid( $objScreeningApplicationRevertCondition->getArTransactionId(), $objScreeningApplicationRevertCondition->getCid(), $objDatabase );

				if( true == valObj( $objArTransaction, 'CArTransaction' ) && false == $objArTransaction->getIsDeleted() ) {
					$objArTransaction->getOrFetchPostMonthValidationData( $intCompanyUserId, $objDatabase );
					$objPropertyGlSetting 	= $objProperty->getOrFetchPropertyGlSetting( $objDatabase );
					$boolRapidDelete 		= ( true == valObj( $objPropertyGlSetting, 'CPropertyGlSetting' ) ) ? false : true;
					if( false == $objArTransaction->validate( VALIDATE_DELETE, $objDatabase ) || false == $objArTransaction->delete( $intCompanyUserId, $objDatabase, NULL, false, $boolRapidDelete ) ) {
						$objDatabase->rollback();
						return;
					}
				}

				$objDeleteScheduledCharge = NULL;

				foreach( $arrobjOldScheduledCharges as $objOldScheduledCharge ) {
					if( 0 < $intChargeCodeId && $intChargeCodeId == $objOldScheduledCharge->getArCodeId() ) {
						$objDeleteScheduledCharge = clone $objOldScheduledCharge;

						if( ( false == $objDeleteScheduledCharge->validate( VALIDATE_DELETE, $objDatabase ) || false == $objDeleteScheduledCharge->delete( $intCompanyUserId, $objDatabase ) ) ) {
							$objDatabase->rollback();
							return;
						}
					}
				}

			} else {
				$objRentScheduledCharge		= NULL;
				$fltDecreasedChargeAmount	= 0;

				foreach( $arrobjOldScheduledCharges as $objOldScheduledCharge ) {
					$objRentScheduledCharge = ( 0 < $intChargeCodeId && $intChargeCodeId == $objOldScheduledCharge->getArCodeId() ) ? $objOldScheduledCharge : $objRentScheduledCharge;
				}

				if( true == valObj( $objRentScheduledCharge, 'CScheduledCharge' ) && 0 < $objScreeningApplicationRevertCondition->getConditionValue() ) {

					if( true == is_numeric( $objScreeningApplicationRevertCondition->getConditionAmountTypeId() ) && CScreeningPackageChargeCodeAmountType::ABSOLUTE == ( int ) $objScreeningApplicationRevertCondition->getConditionAmountTypeId() ) {
						$fltDecreasedChargeAmount = $objRentScheduledCharge->getChargeAmount() - ( float ) $objScreeningApplicationRevertCondition->getConditionValue();
					}

					if( true == is_numeric( $objScreeningApplicationRevertCondition->getConditionAmountTypeId() ) && CScreeningPackageChargeCodeAmountType::PERCENTAGE == ( int ) $objScreeningApplicationRevertCondition->getConditionAmountTypeId() ) {
						$fltDecreasedChargeAmount = CScreeningApplicationConditionSet::revertScheduledCharge( $objRentScheduledCharge->getChargeAmount(), $objScreeningApplicationRevertCondition->getConditionValue() );
					}

					if( 0 < $fltDecreasedChargeAmount ) {

						$objRentScheduledCharge->setChargeAmount( $fltDecreasedChargeAmount );

						if( false == $objRentScheduledCharge->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) {
							$objDatabase->rollback();
							return;
						}
					} else {
						if( false == $objRentScheduledCharge->validate( VALIDATE_DELETE, $objDatabase ) || false == $objRentScheduledCharge->delete( $intCompanyUserId, $objDatabase ) ) {
							$objDatabase->rollback();
							return;
						}
					}
				}

			}

		} else {

			// Post Megarates records
			// Below is a code which will delete the scheduled charges associated with risk premium that were added while increasing rent

			if( false == $objScheduledCharge->validate( VALIDATE_DELETE, $objDatabase ) || false == $objScheduledCharge->delete( $intCompanyUserId, $objDatabase ) ) {
				$objDatabase->rollback();
				return;
			}
		}

		// update application status if required and update the screening conditions ...
		if( true == CScreeningApplicationConditionSet::revertScreeningApplicationConditions( $objScreeningApplicationRevertCondition->getApplicationId(), $objScreeningApplicationRevertCondition->getCid(), $intCompanyUserId, $objDatabase ) ) {
			$objDatabase->commit();
			return true;
		}

		return false;

	}

	public function handleRevertScreeningApplicationConditions( $intApplicationId, $objProperty, $intCid, $intCompanyUserId, $objDatabase ) {

		$boolIsValid = true;

		$boolIsValid = CScreeningApplicationConditionSet::revertScheduledChargeConditions( $intApplicationId, $objProperty, $intCid, $intCompanyUserId, $objDatabase );

		if( true == $boolIsValid ) {
			$objDatabase->begin();

			$boolIsValid = CScreeningApplicationConditionSet::revertScreeningApplicationConditions( $intApplicationId, $intCid, $intCompanyUserId, $objDatabase );

			if( false == $boolIsValid ) {
				$objDatabase->rollback();
			} else {
				$objDatabase->commit();
			}

			return true;
		}

		return false;
	}

	public function revertScheduledChargeConditions( $intApplicationId, $objProperty, $intCid, $intCompanyUserId, $objDatabase ) {
		$arrobjScreeningApplicationRevertingConditions = CScreeningApplicationConditionSets::fetchAppliedConditionsByApplicationIdByCid( $intApplicationId, $intCid, $objDatabase );
		$objApplication = CScreeningApplicationConditionSet::fetchApplication( $intApplicationId, $intCid, $objDatabase );

		if( false == valArr( $arrobjScreeningApplicationRevertingConditions ) ) return false;

		$boolIsValid = true;

		foreach( $arrobjScreeningApplicationRevertingConditions as $objScreeningApplicationRevertCondition ) {

			switch( $objScreeningApplicationRevertCondition->getConditionTypeId() ) {

				case CTransmissionConditionType::INCREASE_RENT:
					if( false == is_null( $objScreeningApplicationRevertCondition->getScheduledChargeId() ) )
						$boolIsValid &= CScreeningApplicationConditionSet::revertScheduledCharges( $objScreeningApplicationRevertCondition, NULL, $objApplication->getLeaseIntervalId(), $intCompanyUserId, $objDatabase );
					break;

				case CTransmissionConditionType::INCREASE_DEPOSIT:
				case CTransmissionConditionType::REPLACE_DEPOSIT:
					if( false == is_null( $objScreeningApplicationRevertCondition->getArTransactionId() ) || false == is_null( $objScreeningApplicationRevertCondition->getScheduledChargeId() ) )
						$boolIsValid &= CScreeningApplicationConditionSet::revertScheduledCharges( $objScreeningApplicationRevertCondition, $objProperty, $objApplication->getLeaseIntervalId(), $intCompanyUserId, $objDatabase );
					break;

				case CTransmissionConditionType::OTHER_CHARGES:
					if( false == is_null( $objScreeningApplicationRevertCondition->getArTransactionId() ) || false == is_null( $objScreeningApplicationRevertCondition->getScheduledChargeId() ) )
						$boolIsValid &= CScreeningApplicationConditionSet::revertScheduledCharges( $objScreeningApplicationRevertCondition, $objProperty, $objApplication->getLeaseIntervalId(), $intCompanyUserId, $objDatabase );
					break;

				default:
					// added default case
			}
		}

		return $boolIsValid;
	}

	public function processGuarantorCondition( $objScreeningApplicationConditionValue, $intApplicationId, $intCompanyUserId, $objDatabase ) {

		if( false == valObj( $objScreeningApplicationConditionValue, 'CScreeningApplicationConditionValue' ) ) return true;

		$objScreeningApplicationRequest = \Psi\Eos\Entrata\CScreeningApplicationRequests::createService()->fetchScreeningApplicationRequestByApplicationIdByCid( $intApplicationId, $objScreeningApplicationConditionValue->getCid(), $objDatabase );

		if( false == valObj( $objScreeningApplicationRequest, 'CScreeningApplicationRequest' ) ) return true;

		if( CTransmissionVendor::RESIDENT_VERIFY == $objScreeningApplicationRequest->getScreeningVendorId() ) {
			// RV Application
			$arrobjGuarantorScreeningApplicantResults = ( array ) CScreeningApplicantResults::fetchScreeningApplicantResultsByApplicationIdByCustomerTypeIdsByCid( $intApplicationId, [ CCustomerType::GUARANTOR ], $objScreeningApplicationConditionValue->getCid(), $objDatabase );
			$arrobjGuarantorScreeningApplicantResults = rekeyObjects( 'ScreeningRecommendationTypeId', $arrobjGuarantorScreeningApplicantResults );

			if( false == valArr( $arrobjGuarantorScreeningApplicantResults ) ) {
				return true;
			}

			if( true == array_key_exists( CScreeningRecommendationType::PASS, $arrobjGuarantorScreeningApplicantResults ) || true == array_key_exists( CScreeningRecommendationType::PASSWITHCONDITIONS, $arrobjGuarantorScreeningApplicantResults ) ) {
				$intConditionSatisfiedBy 	= $intCompanyUserId;
					$strSatisfiedOn          = 'NOW()';
			} else {
					$intConditionSatisfiedBy = NULL;
					$strSatisfiedOn          = NULL;
			}

		} elseif( true == in_array( $objScreeningApplicationRequest->getScreeningVendorId(), CTransmissionVendor::$c_arrintAllowRVPermissionToNonRvVendorIds ) ) {
			// NON RV Application
			$intPassedGuarantorCount = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchScreenedPassedGuarantorCountApplicantsByApplicationIdByCidByCustomerTypeId( $intApplicationId, $objScreeningApplicationConditionValue->getCid(), CCustomerType::GUARANTOR, $objDatabase );

			if( false == valId( $intPassedGuarantorCount ) ) {
				return true;
			}

			$intConditionSatisfiedBy = $intCompanyUserId;
			$strSatisfiedOn          = 'NOW()';
		} else {
			$intConditionSatisfiedBy = NULL;
			$strSatisfiedOn          = NULL;
		}

		$objScreeningApplicationConditionValue->setSatisfiedBy( $intConditionSatisfiedBy );
		$objScreeningApplicationConditionValue->setSatisfiedOn( $strSatisfiedOn );

		$objDatabase->begin();

		if( false == $objScreeningApplicationConditionValue->update( $intCompanyUserId, $objDatabase ) ) {
			return false;
			$objDatabase->rollback();
		}

		$objDatabase->commit();

		return true;

	}

	public function revertScheduledCharge( $fltApplicationChargeAmount, $intIncreaseRentBy ) {
		$fltOriginalScheduledCharge = ( 100 / ( 100 + $intIncreaseRentBy ) ) * $fltApplicationChargeAmount;
		return $fltOriginalScheduledCharge;
	}

	public function processRentalCollectionCondition( $objScreeningApplicationConditionValue, $intApplicationId, $intCompanyUserId, $objDatabase ) {

		$intCountApprovedFileAssociations     = CFileAssociations::createService()->fetchApprovedRentalCollectionFileAssociationsCountByApplicationIdByCid( $intApplicationId, $objScreeningApplicationConditionValue->getCid(), $objDatabase );
		$intCountAllFileAssociations          = CFileAssociations::createService()->fetchAllRentalCollectionFileAssociationsCountByApplicationIdByCid( $intApplicationId, $objScreeningApplicationConditionValue->getCid(), $objDatabase );

		if( ( $intCountApprovedFileAssociations != $intCountAllFileAssociations ) || ( 0 == $intCountAllFileAssociations ) ) {
			return false;
		}

		$objScreeningApplicationConditionValue->setSatisfiedBy( $intCompanyUserId );
		$objScreeningApplicationConditionValue->setSatisfiedOn( 'NOW()' );

		$objDatabase->begin();
		if( false == $objScreeningApplicationConditionValue->update( $intCompanyUserId, $objDatabase ) ) {
			$objDatabase->rollback();
			return false;
		}

		$objDatabase->commit();

		return true;

	}

	public function handleInstallmentPlanScheduledCharges( $objApplication, $intInstallmentPlanId, $intCompanyUserId, $objDatabase ) {

        switch( NULL ) {
            default:

                $boolForceReload = false;
                if( true == valId( $intInstallmentPlanId ) ) {
                    $objLeaseInterval   = $objApplication->fetchLeaseInterval( $objDatabase );
                    $objClient          = $objApplication->fetchClient( $objDatabase );
                    $objCompanyUser     = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $intCompanyUserId, $objApplication->getCid(), $objDatabase );

                    if( false == valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {
                        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Failed to load lease interval database object' ) ) );
                        break;
                    }

                    // get the existing installment plan to check whether the installment plan is already exist which will be use in rebuilt process
                    $arrobjOldLeaseIntervalInstallmentPlans = \Psi\Eos\Entrata\CLeaseIntervalInstallmentPlans::createService()->fetchLeaseIntervalInstallmentPlansByLeaseIntervalIdByInstallmentPlanIdsByCid( $objLeaseInterval->getId(), $intInstallmentPlanId, $objLeaseInterval->getCid(), $objDatabase );

                    // We require this for rebuilding process to insert or update the LeaseIntervalInstallmentPlans table
                    $objInstallmentPlansLibrary = new CInstallmentPlansLibrary();
                    $objInstallmentPlansLibrary->setClient( $objClient );
                    $objInstallmentPlansLibrary->setLeaseInterval( $objLeaseInterval );
                    $objInstallmentPlansLibrary->setCompanyUser( $objCompanyUser );
                    $objInstallmentPlansLibrary->setDatabase( $objDatabase );

                    // This function is use for Insert or update the Lease Interval Installment Plans
                    if( false == $objInstallmentPlansLibrary->insertUpdateLeaseIntervalInstallmentPlans( [ $intInstallmentPlanId ], $objApplication ) ) {
                        $this->addErrorMsgs( $objInstallmentPlansLibrary->getErrorMsgs() );
                        break;
                    }

                    // $boolForceReload parameter = true [ Delete all existing sceduled charges created by existing installment plan and create the new scheduled charges as per new installment plan ]
                    // $boolForceReload parameter = false [ It will generate scheduled charges as per new installment plan ]
                    if( false == valArr( $arrobjOldLeaseIntervalInstallmentPlans ) ) {
                        $boolForceReload       = true;
                    } else {
                        $arrintOldInstallmentPlanIds = [];
                        $arrintOldInstallmentPlanIds = extractUniqueFieldValuesFromObjects( 'getInstallmentPlanId', $arrobjOldLeaseIntervalInstallmentPlans );

                        $arrintDeleteInstallmentPlanIds = array_diff( $arrintOldInstallmentPlanIds, ( array ) $intInstallmentPlanId );
                        if( true == valArr( $arrintDeleteInstallmentPlanIds ) ) {
                            $boolForceReload = true;
                        }
                    }

                    // This will use to generate Application data Object which require for scheduling the charges and rebuilding
                    $objApplicationDataObject = $this->generateApplicationDataObject( $objApplication, $objCompanyUser, $intCompanyUserId, $objClient, $objDatabase );

                    $objApplicationScheduledChargesLibrary = new CApplicationScheduledChargesLibrary( $objApplicationDataObject );

                    if( true == valId( $objApplication->getQuoteId() ) && strtotime( $objApplication->getQuotedRentLockedUntil() ) > time() && true == valObj( $objLeaseInterval, 'CLeaseInterval' ) ) {
                        $arrobjNewLeaseIntervalInstallmentPlans = \Psi\Eos\Entrata\CLeaseIntervalInstallmentPlans::createService()->fetchLeaseIntervalInstallmentPlansByLeaseIntervalIdByInstallmentPlanIdsByCid( $objLeaseInterval->getId(), $intInstallmentPlanId, $objLeaseInterval->getCid(), $objDatabase );
                        $objApplicationScheduledChargesLibrary->setForceReloadChargesForQuotesAndInstallments( valArr( $arrobjNewLeaseIntervalInstallmentPlans ) );
                    }

                    // We are providing arrintArOriginIds = NULL as we are not considering PET RENT
                    // boolFromEntrata = NULL
                    if( false == $objApplicationScheduledChargesLibrary->rebuildApplicationScheduledCharges( NULL, $boolForceReload, true ) ) {
                        $objDatabase->rollback();
                        return false;
                    }

                    $objDatabase->commit();
                    return true;
                }
        }
    }

    public function generateApplicationDataObject( $objApplication, $objCompanyUser, $intCompanyUserId, $objClient, $objDatabase ) {
        $objApplicationDataObject = new CApplicationDataObject();

        $objProperty                            = $objApplication->fetchProperty( $objDatabase );
        $arrobjPropertyPreferences              = \Psi\Eos\Entrata\CpropertyPreferences::createService()->fetchPropertyPreferencesByPropertyIdByCid( $objProperty->getId(), $objClient->getId(), $objDatabase );
        $arrobjPropertyApplicationPreferences   = $objApplication->fetchPropertyApplicationPreferences( $objDatabase, $objProperty->getId() );

        $objPrimaryApplicant = $objApplication->getOrFetchPrimaryApplicant( $objDatabase );
        $objApplicantApplication = $objApplication->fetchApplicantApplicationByApplicantId( $objPrimaryApplicant->getId(), $objDatabase );

        $objApplicationDataObject->m_objApplication 	= $objApplication;
        $objApplicationDataObject->m_objApplicant		= $objPrimaryApplicant;

        if( true == valObj( $objPrimaryApplicant, 'CApplicant' ) ) {
            $objApplicationDataObject->m_objApplicantApplication 	= $objApplication->fetchApplicantApplicationByApplicantId( $objPrimaryApplicant->getId(), $objDatabase );
        }

        if( false == $objApplicationDataObject->m_strSecureBaseName ) {
            $objApplicationDataObject->m_strSecureBaseName = $this->m_strSecureBaseName;
        }

        $objApplicationDataObject->m_intCompanyUserId			          = $intCompanyUserId;
        $objApplicationDataObject->m_objCompanyUser				          = $objCompanyUser;
        $objApplicationDataObject->m_objClient 					          = $objClient;
        $objApplicationDataObject->m_objProperty				          = $objProperty;
        $objApplicationDataObject->m_arrobjPropertyPreferences	          = $arrobjPropertyPreferences;
        $objApplicationDataObject->m_objDatabase				          = $objDatabase;

        $objApplicationDataObject->m_intOldPropertyId 			          = $objProperty->getId();
        $objApplicationDataObject->m_objPrimaryApplicant 	              = $objPrimaryApplicant;

        $objApplicationDataObject->m_arrobjPropertyApplicationPreferences = ( array ) $arrobjPropertyApplicationPreferences;
        $objApplicationDataObject->m_objApplicant					      = $objPrimaryApplicant;
        $objApplicationDataObject->m_objApplicantApplication		      = $objApplicantApplication;
        $objApplicationDataObject->m_intPsProductId					      = CPsProduct::ENTRATA;

        $objApplicationDataObject->m_boolIsStudentProperty			      = false;
        if( CPropertyType::STUDENT == $objProperty->getPropertyTypeId() ) {
            $objApplicationDataObject->m_boolIsStudentProperty			  = true;
        }
        $objApplicationDataObject->m_boolIsResidentVerifyEnabled	      = true;
        $objApplicationDataObject->m_boolValidateRequiredFields		      = false;
        $objApplicationDataObject->m_arrobjPropertyApplicationCustomerDataTypes	= rekeyObjects( 'CustomerDataTypeId', ( array ) $this->m_arrobjPropertyApplicationCustomerDataTypes );

	    return $objApplicationDataObject;
    }

	/**
	 * Fetch Functions
	 */

	public function fetchApplication( $intApplicationId, $intCid, $objDatabase ) {
		return CApplications::fetchCustomApplicationByIdByCid( $intApplicationId, $intCid, $objDatabase );
	}

	public function fetchProperty( $intPropertyId, $intCid, $objDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $intCid, $objDatabase );
	}

	public function setScreeningAvailableConditionId( $intScreeningAvailableConditionId ) {
		$this->m_intScreeningAvailableConditionId = $intScreeningAvailableConditionId;
	}

	public function getScreeningAvailableConditionId() {
		return $this->m_intScreeningAvailableConditionId;
	}

}
?>