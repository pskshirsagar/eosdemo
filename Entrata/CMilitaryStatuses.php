<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMilitaryStatuses
 * Do not add any new functions to this class.
 */

class CMilitaryStatuses extends CBaseMilitaryStatuses {

	public static function fetchMilitaryStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMilitaryStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchMilitaryStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMilitaryStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedMilitaryStatuses( $objDatabase ) {

		$strSql = 'SELECT * FROM military_statuses WHERE is_published=true order by order_num';
		return self::fetchMilitaryStatuses( $strSql, $objDatabase );
	}

}
?>