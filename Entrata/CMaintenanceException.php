<?php

class CMaintenanceException extends CBaseMaintenanceException {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Exception name is required.' ) ) );

		} else {
			$objMaintenanceException = \Psi\Eos\Entrata\CMaintenanceExceptions::createService()->fetchMaintenanceExceptionByCidByName( $this->m_intCid, $this->getName(), $objDatabase );

			if( true == is_object( $objMaintenanceException ) && true == valObj( $objMaintenanceException, 'CMaintenanceException' ) && $objMaintenanceException->getId() != $this->getId() ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( ' Exception {%s, 0} already exists.', [ $objMaintenanceException->getName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>