<?php

class CBudgetAdjustment extends CBaseBudgetAdjustment {

	protected $m_strApprovalNote;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFromApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToApHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getApprovalNote() {
		return $this->m_strApprovalNote;
	}

	/**
	 * Set Functions
	 */

	public function setApprovalNote( $strApprovalNote ) {
		$this->m_strApprovalNote = $strApprovalNote;
	}

	public function fetchEventsByEventTypeIds( $arrintEventSubTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByReferenceIdByReferenceTypeIdByEventSubTypeIdsByCid( $this->getId(), CEventReferenceType::BUDGET_ADJUSTMENT, $arrintEventSubTypeIds, $this->getCid(), $objDatabase );
	}

}
?>