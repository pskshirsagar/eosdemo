<?php

class CAssetTransactionType extends CBaseAssetTransactionType {

	const RECEIVE_INVENTORY	= 1;
	const CONSUME_INVENTORY	= 2;
	const RETURN_INVENTORY	= 3;
	const MANUAL_ADJUSTMENT	= 4;
	const DEPRECIATION = 5;
	const ASSET_TRANSFER	= 6;
	const ASSET_RETIRE		= 7;
	const PLACED_IN_SERVICE	= 8;
	const CANCEL_ITEM   	= 9;

	private $m_arrstrReceivedAssetTransactionType;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getAssetTransactionTypesForNetBookValue() {
		return [ self::RECEIVE_INVENTORY, self::MANUAL_ADJUSTMENT, self::ASSET_TRANSFER ];
	}

	public function getReceivedAssetTransactionTypes() {
		if( true == valArr( $this->m_arrstrReceivedAssetTransactionType ) ) {
			return $this->m_arrstrReceivedAssetTransactionType;
		}

		$this->m_arrstrReceivedAssetTransactionType = [
			self::RECEIVE_INVENTORY	=> __( 'Receive Inventory' ),
			self::MANUAL_ADJUSTMENT	=> __( 'Manual Adjustment' ),
			self::ASSET_TRANSFER	=> __( 'Asset Transfer' )
		];

		return $this->m_arrstrReceivedAssetTransactionType;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Other functions
	 */

}
?>