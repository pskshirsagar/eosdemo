<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlHeaderLogs
 * Do not add any new functions to this class.
 */

class CGlHeaderLogs extends CBaseGlHeaderLogs {

	public static function fetchGlHeaderLogsByGlHeaderIdByActionsByCid( $intGlHeaderId, $intCid, $objClientDatabase, $arrstrActions = NULL ) {

		$strConditions = NULL;

		if( true == valArr( $arrstrActions ) ) {
			$strConditions = ' AND ghl.action IN( \'' . implode( '\'' . ', \'', $arrstrActions ) . '\' )';
		}

		$strSql = 'SELECT
						ghl.*,
						ARRAY_TO_STRING ( ARRAY[ ce.name_first, ce.name_last ], \' \' ) AS company_employee_full_name
					FROM
						gl_header_logs ghl
						JOIN company_users cu ON ( cu.id = ghl.updated_by AND cu.cid = ghl.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
					WHERE
						ghl.gl_header_id = ' . ( int ) $intGlHeaderId . '
						AND ghl.cid = ' . ( int ) $intCid .
						$strConditions . '
					ORDER BY
						ghl.id DESC';

		return self::fetchGlHeaderLogs( $strSql, $objClientDatabase );
	}

	public static function fetchPreviousGlHeaderLogByIdByGlHeaderIdByCid( $intId, $intGlHeaderId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ghl.*,
						p.property_name as bulk_property_name
					FROM
						gl_header_logs ghl
						LEFT JOIN properties p ON ( p.cid = ghl.cid AND ghl.bulk_property_id = p.id )
					WHERE
						ghl.gl_header_id = ' . ( int ) $intGlHeaderId . '
						AND ghl.cid = ' . ( int ) $intCid . '
						AND ghl.id < ' . ( int ) $intId . '
					ORDER BY
						ghl.id DESC
					LIMIT 1';

		return self::fetchGlHeaderLog( $strSql, $objClientDatabase );
	}

	public static function fetchLatestRoutingGlHeaderLogsByGlHeaderIdByCid( $intGlHeaderId, $intCid, $objClientDatabase ) {

		$arrstrActions = [
			CGlHeaderLog::ACTION_APPROVED,
			CGlHeaderLog::ACTION_UNAPPROVED,
			CGlHeaderLog::ACTION_RETURNED_TO_PREVIOUS,
			CGlHeaderLog::ACTION_RETURNED_TO_BEGINNING,
			CGlHeaderLog::ACTION_FINAL_APPROVAL
		];

		$strSql = 'SELECT
						ghl.*,
						ARRAY_TO_STRING ( ARRAY[ ce.name_first, ce.name_last ], \' \' ) AS company_employee_full_name
					FROM
						gl_header_logs ghl
						JOIN company_users cu ON ( cu.id = ghl.updated_by AND cu.cid = ghl.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND cu.cid = ce.cid )
					WHERE
						ghl.cid = ' . ( int ) $intCid . '
						AND ghl.gl_header_id = ' . ( int ) $intGlHeaderId . '
						AND ghl.action IN ( ' . sqlStrImplode( $arrstrActions ) . ' )
					ORDER BY
						ghl.id';

		return self::fetchGlHeaderLogs( $strSql, $objClientDatabase );
	}

}
?>