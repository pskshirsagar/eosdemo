<?php

class CDataExportSchedule extends CBaseDataExportSchedule {

	protected $m_strExportStatus;
	protected $m_strRequestUrl;
	protected $m_strUserName;
	protected $m_strPassword;

	protected $m_intPort;
	protected $m_intPropertyCount;
	protected $m_intDataExportStatusTypeId;
	protected $m_intCompanyStatusTypeId;
	protected $m_intDataExportFrequencyTypeId;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['export_status'] ) ) $this->setExportStatus( $arrmixValues['export_status'] );
		if( true == isset( $arrmixValues['data_export_status_type_id'] ) ) $this->setDataExportStatusTypeId( $arrmixValues['data_export_status_type_id'] );
		if( true == isset( $arrmixValues['property_count'] ) ) $this->setPropertyCount( $arrmixValues['property_count'] );
		if( true == isset( $arrmixValues['request_url'] ) ) $this->setRequestUrl( $arrmixValues['request_url'] );
		if( true == isset( $arrmixValues['username'] ) ) $this->setUserName( $arrmixValues['username'] );
		if( true == isset( $arrmixValues['password'] ) ) $this->setPassword( $arrmixValues['password'] );
		if( true == isset( $arrmixValues['port'] ) ) $this->setPort( $arrmixValues['port'] );
		if( true == isset( $arrmixValues['company_status_type_id'] ) ) $this->setCompanyStatusTypeId( $arrmixValues['company_status_type_id'] );
		if( true == isset( $arrmixValues['data_export_frequency_type_id'] ) ) $this->setDataExportFrequencyTypeId( $arrmixValues['data_export_frequency_type_id'] );
	}

    /**
     * Get Functions
     */

    public function getExportStatus() {
    	return $this->m_strExportStatus;
    }

    public function getDataExportStatusTypeId() {
    	return $this->m_intDataExportStatusTypeId;
    }

    public function getPropertyCount() {
    	return $this->m_intPropertyCount;
    }

	public function getRequestUrl() {
		return $this->m_strRequestUrl;
	}

	public function getUserName() {
		if( false == valStr( $this->m_strUserName ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strUserName, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getPassword() {
		if( false == valStr( $this->m_strPassword ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD, 'is_base64_encoded' => true, 'should_rtrim_nulls' => true ] );
	}

	public function getPort() {
		return $this->m_intPort;
	}

	public function getCompanyStatusTypeId() {
		return $this->m_intCompanyStatusTypeId;
	}

	public function getDataExportFrequencyTypeId() {
		return $this->m_intDataExportFrequencyTypeId;
	}

    /**
     * Set Functions
     */

	public function setExportStatus( $strExportStatus ) {
    	$this->m_strExportStatus = $strExportStatus;
    }

	public function setDataExportStatusTypeId( $strDataExportStatusTypeId ) {
    	$this->m_intDataExportStatusTypeId = $strDataExportStatusTypeId;
    }

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function setRequestUrl( $strRequestUrl ) {
		$this->m_strRequestUrl = $strRequestUrl;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setPassword( $strPassword ) {
		$this->m_strPassword = $strPassword;
	}

	public function setPort( $intPort ) {
		$this->m_intPort = $intPort;
	}

	public function setCompanyStatusTypeId( $intCompanyStatusTypeId ) {
		$this->m_intCompanyStatusTypeId = $intCompanyStatusTypeId;
	}

	public function setDataExportFrequencyTypeId( $intDataExportFrequencyTypeId ) {
		$this->m_intDataExportFrequencyTypeId = $intDataExportFrequencyTypeId;
	}

    /**
     * Create Functions
     */

    public function createPropertyDataExportSchedule() {

    	$objPropertyDataExportSchedule = new CPropertyDataExportSchedule();
    	$objPropertyDataExportSchedule->setCid( $this->getCid() );
    	$objPropertyDataExportSchedule->setDataExportScheduleId( $this->getId() );

    	return $objPropertyDataExportSchedule;
    }

    public function createDataExportScheduleTableGroup() {

    	$objDataExportScheduleTableGroup = new CDataExportScheduleTableGroup();
    	$objDataExportScheduleTableGroup->setCid( $this->getCid() );
    	$objDataExportScheduleTableGroup->setDataExportScheduleId( $this->getId() );

    	return $objDataExportScheduleTableGroup;
    }

    public function createDataExport() {

    	$objDataExport = new CDataExport();
    	$objDataExport->setCid( $this->getCid() );
    	$objDataExport->setDataExportScheduleId( $this->getId() );
    	$objDataExport->setDataExportStatusTypeId( CDataExportStatusType::UNUSED );

    	return $objDataExport;
    }

    public function createDataExportLog() {
    	$objDataExportLog = new CDataExportLog();
    	$objDataExportLog->setCid( $this->getCid() );
    	$objDataExportLog->setDataExportScheduleId( $this->getId() );
    	$objDataExportLog->setLogDatetime( 'NOW()' );

    	return $objDataExportLog;
    }

    public function createDataExportFrequency() {
    	$objDataExportFrequency = new CDataExportFrequency();
    	$objDataExportFrequency->setCid( $this->getCid() );

    	return $objDataExportFrequency;
    }

    /**
     * Validate Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataExportFrequencyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDataExportTypeId() {
        $boolIsValid = true;

        if( NULL == $this->getDataExportTypeId() ) {
        	$this->addErrorMsg( new CErrorMsg( NULL, 'export_name', 'Export type is required.' ) );
        	$boolIsValid = false;
        } elseif( 1 > strlen( $this->getDataExportTypeId() ) ) {
        	$this->addErrorMsg( new CErrorMsg( NULL, 'export_name', 'Export type is required.' ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function valDataExportEmailAlert() {
		$boolIsValid = true;

		if( NULL == $this->getNotificationOptions() && NULL != $this->getEmailAddress() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'export_alert', 'Notification Option is required.' ) );
			$boolIsValid = false;
		}

		if( NULL != $this->getNotificationOptions() && NULL == $this->getEmailAddress() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'email_address', 'Email Address is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDataExportDirectory() {

		if( 'production' != CONFIG_ENVIRONMENT ) return true;

		$strFTPServerUrl	= 'ftp.entrata.com';
		$strRemoteLocation	= '/' . $this->getCid() . '/';
		$objConnection		= ftp_connect( $strFTPServerUrl );

		if( false == ftp_login( $objConnection,
				\Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( CONFIG_FTP_USERNAME, CONFIG_SODIUM_KEY_FTP_MASTER_USER, [ 'legacy_secret_key' => CONFIG_KEY_FTP_MASTER_USER ] ),
				\Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( CONFIG_FTP_PASSWORD, CONFIG_SODIUM_KEY_FTP_MASTER_USER, [ 'legacy_secret_key' => CONFIG_KEY_FTP_MASTER_USER ] ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to login to remote FTP server.' ) );
			return false;
		}

		if( false == in_array( 'exported_data', ftp_nlist( $objConnection, $strRemoteLocation ) ) ) {

			// check if client folder is created on root.
			if( false == in_array( $strRemoteLocation, ftp_nlist( $objConnection, '/' ) ) && !ftp_mkdir( $objConnection, $strRemoteLocation ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to create directory "' . $strRemoteLocation . ' "' ) );
				return false;
			}

			if( false == ftp_mkdir( $objConnection, $strRemoteLocation . 'exported_data/' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to create directory "' . $strRemoteLocation . 'exported_data "' ) );
				return false;
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Directory "' . $strRemoteLocation . 'exported_data " created successfully' ) );
		}
		return true;
	}

    public function valExportName() {
         $boolIsValid = true;

    	if( NULL == $this->getExportName() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'export_name', 'Export name is required.' ) );
			$boolIsValid = false;
		} elseif( 1 > strlen( $this->getExportName() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'export_name', 'Export name is required.' ) );
			$boolIsValid = false;
		}

        return $boolIsValid;
    }

    public function valNextRunDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLastRunDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function validate( $strAction ) {
	$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valExportName();
				$boolIsValid &= $this->valDataExportTypeId();
				$boolIsValid &= $this->valDataExportEmailAlert();
				$boolIsValid &= $this->valDataExportDirectory();
				break;

			case VALIDATE_DELETE:
				break;

			default:
		}

		return $boolIsValid;
    }

    /**
     * Fetch Functions
     */

    public function fetchDataExportFrequency( $objDatabase ) {
    	return \Psi\Eos\Entrata\CDataExportFrequencies::createService()->fetchDataExportFrequencyByDataExportScheduleIdByCid( $this->getId(), $this->getCid(), $objDatabase );
    }

}
?>