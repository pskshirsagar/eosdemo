<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyMessageDetails
 * Do not add any new functions to this class.
 */

class CSubsidyMessageDetails extends CBaseSubsidyMessageDetails {

	public static function fetchSubsidyMessageDetailForCertificationBySubsidyMessageIdBySubsidyMessageTypeByCid( $intSubsidyMessageId, $intSubsidyMessageType, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					smd.id,
					smd.result_message,
					smd.num_errors,
					smd.field_errors,
					sc.subsidy_certification_type_id,
					smd.subsidy_message_sub_type_id,
					concat( apt.name_first, \' \', apt.name_last ) as head_of_household
				FROM
					subsidy_message_details smd
					LEFT JOIN subsidy_certifications sc ON ( sc.cid = smd.cid AND sc.id = smd.reference_id )
					JOIN applications ap ON ( ap.cid = smd.cid AND ap.id = sc.application_id )
					JOIN applicants apt ON ( apt.cid = smd.cid AND apt.id = ap.primary_applicant_id )

				WHERE
					smd.cid = ' . ( int ) $intCid . '
					AND smd.subsidy_message_sub_type_id = ' . ( int ) CSubsidyMessageSubType::CERTIFICATIONS;

		if( $intSubsidyMessageType == 0 ) {
			$strSql .= ' AND smd.submission_subsidy_message_id = ' . ( int ) $intSubsidyMessageId;
		} else {
			$strSql .= ' AND smd.result_subsidy_message_id = ' . ( int ) $intSubsidyMessageId;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyMessageDetailForHapBySubsidyMessageIdBySubsidyMessageTypeByCid( $intSubsidyMessageId, $intSubsidyMessageType, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					smd.id,
					smd.result_message,
					smd.num_errors,
					smd.field_errors,
					shr.voucher_date,
					shr.total_amount,
					smd.subsidy_message_sub_type_id
				FROM
					subsidy_message_details smd
					LEFT JOIN subsidy_hap_requests shr ON ( shr.cid = smd.cid AND shr.id = smd.reference_id )

				WHERE
					smd.cid = ' . ( int ) $intCid . '
					AND smd.subsidy_message_sub_type_id = ' . ( int ) CSubsidyMessageSubType::HAP_VOUCHERS;

		if( $intSubsidyMessageType == 0 ) {
			$strSql .= ' AND smd.submission_subsidy_message_id = ' . ( int ) $intSubsidyMessageId;
		} else {
			$strSql .= ' AND smd.result_subsidy_message_id = ' . ( int ) $intSubsidyMessageId;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyMessageDetailErrorCountByCid( $intCid, $objDatabase ) {

		$strSql = ' SELECT
						SUM ( scm.num_errors ) as error_count,
						scm.submission_subsidy_message_id
					FROM
						subsidy_message_details scm
					WHERE
						scm.cid = ' . ( int ) $intCid . '
						AND scm.result_subsidy_message_id IS NOT NULL
					GROUP BY
						scm.submission_subsidy_message_id ';

		$arrmixResultSet = fetchData( $strSql, $objDatabase );
		return rekeyArray( 'submission_subsidy_message_id', $arrmixResultSet );
	}

	public static function fetchSubsidyMessageDetaisIdBySubmissionSubsidyMessageIdByCid( $intSubmissionSubsidyMessageId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						subsidy_message_details smd
					WHERE
						smd.cid = ' . ( int ) $intCid . '
						AND smd.submission_subsidy_message_id = ' . ( int ) $intSubmissionSubsidyMessageId;

		return self::fetchSubsidyMessageDetails( $strSql, $objDatabase );
	}

}
?>