<?php

class CDefaultRoommateInterestOption extends CBaseDefaultRoommateInterestOption {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDefaultRoommateInterestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAnswer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

            default:
            	// default;
            	break;
        }

        return $boolIsValid;
    }
}
?>