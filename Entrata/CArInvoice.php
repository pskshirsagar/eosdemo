<?php

class CArInvoice extends CBaseArInvoice {

	const SINGLE				= 1;
	const MULTIPLE				= 2;
	const MULTIPLE_AND_REQUIRED	= 3;
	const REQUIRED				= 4;
	const NOT_REQUIRED			= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		if( false == valId( $this->m_intCid ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Invalid Ar Invoice Request: Client id required.' ) ) );
			return false;
		}

		return true;
	}

	public function valPropertyId() {
		if( false == valId( $this->m_intPropertyId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Invalid Ar Invoice Request: Property Id required.' ) ) );
			return false;
		}

		return true;
	}

	public function valLeaseId() {
		if( false == valId( $this->m_intLeaseId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_id', __( 'Invalid Ar Invoice Request: Lease Id required.' ) ) );
			return false;
		}

		return true;
	}

	public function valCustomerId() {
		if( false == valId( $this->m_intCustomerId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', __( 'Customer Id is required.' ) ) );
			return false;
		}

		return true;
	}

	public function valInvoiceNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceDatetime() {
		if( false == valStr( $this->m_strInvoiceDatetime ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoice_datetime', __( 'Invoice datetime required.' ) ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valInvoiceDatetime();
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valLeaseId();
				$boolIsValid &= $this->valCustomerId();
				$boolIsValid &= $this->valInvoiceDatetime();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>