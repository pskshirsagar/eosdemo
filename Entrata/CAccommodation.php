<?php

class CAccommodation extends CBaseAccommodation {

	protected $m_strUsername;

	public function validate( $strAction, $objDatabase = NULL, $objAccommodation = NULL ) {
		$boolIsValid = true;

		$objAccommodationValidator 	= \Psi\Eos\Entrata\Validate\Accommodation\CAccommodationValidator::createService();

		$boolIsValid &= $objAccommodationValidator->validate( $strAction, $objDatabase, $objAccommodation );

		return $boolIsValid;
	}

	public function setUsername( $strUsername ) {
		return $this->m_strUsername = $strUsername;
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['username'] ) ) $this->setUsername( $arrmixValues['username'] );

		$this->m_arrmixCustomVariables['user'] = ( isset( $arrmixValues['user'] ) ) ? trim( $arrmixValues['user'] ) : '';

		return;
	}

}
?>