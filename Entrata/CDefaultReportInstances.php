<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultReportInstances
 * Do not add any new functions to this class.
 */

class CDefaultReportInstances extends CBaseDefaultReportInstances {

	public static function fetchDefaultReportInstances( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDefaultReportInstance', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultReportInstance( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDefaultReportInstance', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDefaultReportInstanceMaxId( $objDatabase ) {
		$strSql = '
			SELECT
				MAX( id ) + 1 AS id
			FROM
				default_report_instances';

		return fetchData( $strSql, $objDatabase );
	}

}
?>