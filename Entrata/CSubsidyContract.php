<?php

class CSubsidyContract extends CBaseSubsidyContract {

	protected $m_strSubsidyContractTypeName;
	protected $m_strFirstHapVoucherMonth;
	private $m_boolIsSubsidyContractEditable = false;

	private $m_arrobjSubsidyContractUnitSpaces;

	/**
	 * Get Functions
	 *
	 */

	public function getSubsidyContractTypeName() {
		return $this->m_strSubsidyContractTypeName;
	}

	public function getFirstHapVoucherMonth() {
		return $this->m_strFirstHapVoucherMonth;
	}

	public function getIsSubsidyContractEditable() {
		return $this->m_boolIsSubsidyContractEditable;
	}

	public function setIsSubsidyContractEditable( $boolIsSubsidyContractEditable ) {
		$this->m_boolIsSubsidyContractEditable = $boolIsSubsidyContractEditable;
	}

	public function getFormattedSubsidyContractName() {
		if( true == is_null( $this->m_strSubsidyContractTypeName ) ) {
			return NULL;
		}

		return $this->m_strSubsidyContractTypeName . ( true == valStr( $this->m_strContractNumber ) ? ' (' . $this->m_strContractNumber . ')' : '' );
	}

	public function getSubsidyContractUnitSpaces() {
		return $this->m_arrobjSubsidyContractUnitSpaces;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['subsidy_contract_type_name'] ) ) {
			$this->setSubsidyContractTypeName( $arrmixValues['subsidy_contract_type_name'] );
		}
		if( true == isset( $arrmixValues['first_hap_voucher_month'] ) ) {
			$this->setFirstHapVoucherMonth( $arrmixValues['first_hap_voucher_month'] );
		}

		return;
	}

	public function setSubsidyContractTypeName( $strSubsidyContractTypeName ) {
		$this->m_strSubsidyContractTypeName = $strSubsidyContractTypeName;
	}

	public function setFirstHapVoucherMonth( $strFirstHapVoucherMonth ) {
		$this->m_strFirstHapVoucherMonth = $strFirstHapVoucherMonth;
	}

	public function setSubsidyContractUnitSpaces( $arrobjSubsidyContractUnitSpaces ) {
		$this->m_arrobjSubsidyContractUnitSpaces = ( array ) $arrobjSubsidyContractUnitSpaces;
	}

	/**
	 * Validation Functions
	 *
	 */

	private function valId() {
		$boolIsValid = true;

		if( false == valId( $this->getId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_contract', CSubsidyContractType::getSubsidyContractNameById( $this->getSubsidyContractTypeId() ) . ' subsidy contract is missing.' ) ] );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valSubsidyContractTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getSubsidyContractTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_contract_type_id', 'Project type is required.' ) ] );
		}

		return $boolIsValid;
	}

	public function valSubsidyContractSubTypeId() {
		$boolIsValid = true;
		if( 1 == $this->getSubsidyContractTypeId() && false == valStr( $this->getSubsidyContractSubTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_contract_sub_type_id', 'Section 8 contract type is required.' ) ] );
		}

		return $boolIsValid;
	}

	public function valSubsidyIncomeLimitAreaId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valSubsidyUnitAssignmentTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getAppliesToAllUnits() ) && false == valStr( $this->getSubsidyUnitAssignmentTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_unit_assignment_type_id', 'Unit are is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSubsidyActionPlanTypeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date is required.' ) );
		} elseif( false === CValidation::checkISODateFormat( $this->getStartDate(), $boolFormat = true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date must be in mm/dd/yyyy format.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Expiration date is required.' ) );
		} elseif( false === CValidation::checkISODateFormat( $this->getEndDate(), $boolFormat = true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Expiration date must be in mm/dd/yyyy format.' ) );
		} elseif( true == valStr( $this->getStartDate() ) && true == valStr( $this->getEndDate() ) ) {
			if( strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Expiration date must be greater than or equal to start date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valContractNumber() {
		$boolIsValid = true;

		if( true == in_array( $this->getSubsidyContractTypeId(), CSubsidyContractType::$c_arrintContractNumberSubsidyContractTypes ) && false == valStr( $this->getContractNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_number', 'Contract number is required.' ) ] );
		}

		if( true == valStr( $this->getContractNumber() ) ) {
			if( false == ctype_alnum( $this->getContractNumber() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_number', ' Contract number should be alphanumeric value.' ) );
			} elseif( 11 != strlen( trim( $this->getContractNumber() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_number', ' Contract number should be equal to 11 characters.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valAppliesToAllUnits() {
		$boolIsValid = true;
		if( true == is_null( $this->getAppliesToAllUnits() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applies_to_all_units', 'Contracted units is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUnitCount() {

		$boolIsValid = true;
		if( true == in_array( $this->getSubsidyContractTypeId(), CSubsidyContractType::$c_arrintContractUnitCountSubsidyContractTypes ) && 0 == $this->getAppliesToAllUnits() ) {

			if( true == is_null( $this->getUnitCount() ) ) {

				$boolIsValid = false;
				$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_count', 'Number of contracted units is required.' ) ] );

			} elseif( 0 >= $this->getUnitCount() ) {

				$boolIsValid = false;
				$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_count', 'Number of contracted units is not a valid number.' ) ] );

			}

		}

		return $boolIsValid;
	}

	public function valHapTitle() {
		$boolIsValid = true;

		if( false == valStr( $this->getHapTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'hap_title', 'HAP title is required.' ) ] );
		}

		return $boolIsValid;
	}

	public function valHapPersonName() {
		$boolIsValid = true;

		if( false == valStr( $this->getHapPersonName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'hap_person_name', 'HAP signer name is required.' ) ] );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsDuplicateContract( $intPropertyId, $intCid, $objDatabase, $arrmixSubsidyContractDetails ) {
		// function to check if selected project type is duplicate with same Project Type with overlapping effective dates & other inputs
		$boolIsValid                                              = false;
		$arrmixSubsidyContractDetails['subsidy_contract_id']      = $this->getId();
		$arrmixSubsidyContractDetails['subsidy_contract_type_id'] = $this->getSubsidyContractTypeId();
		$arrmixSubsidyContractDetails['start_date']               = $this->getStartDate();

		if( 0 < \Psi\Eos\Entrata\CSubsidyContracts::createService()->fetchDuplicateSubsidyContractsCount( $intPropertyId, $intCid, $objDatabase, $arrmixSubsidyContractDetails ) ) {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valFirstHapVoucherMonth() {
		$boolIsValid = true;

		if( false == valStr( $this->getFirstHapVoucherMonth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_hap_voucher_month', 'First hap voucher month is required.' ) );
		} elseif( false === CValidation::isValidPostMonth( $this->getFirstHapVoucherMonth(), $boolFormat = true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_hap_voucher_month', 'First hap voucher month must be in mm/yyyy format.' ) );
		} elseif( strtotime( $this->getFirstHapVoucherMonth() ) < strtotime( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_hap_voucher_month', 'First hap voucher month should be greater than or equal to start date.' ) );
		} elseif( strtotime( $this->getFirstHapVoucherMonth() ) > strtotime( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_hap_voucher_month', 'First hap voucher month should be less than or equal to expiration date.' ) );
		}

		return $boolIsValid;
	}

	public function valUnitsCount() {
		$boolIsValid = true;

		if( false == valId( $this->getUnitCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_count', CSubsidyContractType::getSubsidyContractNameById( $this->getSubsidyContractTypeId() ) . ' units count is required.' ) ] );
		}

		return $boolIsValid;
	}

	public function valUnitSpaces() {
		$boolIsValid = true;

		if( CSubsidyUnitAssignmentType::FIXED_UNITS == $this->getSubsidyUnitAssignmentTypeId() && false == valArr( $this->getSubsidyContractUnitSpaces()['unit_space_id'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_spaces', 'Please select units for association with ' . \Psi\CStringService::singleton()->strtolower( CSubsidyContractType::getSubsidyContractNameById( $this->getSubsidyContractTypeId() ) ) . ' subsidy contract.' ) ] );
		}

		return $boolIsValid;
	}

	public function valSelectedUnits() {
		$boolIsValid = true;

		if( CSubsidyUnitAssignmentType::FIXED_UNITS == $this->getSubsidyUnitAssignmentTypeId() && true == valArr( $this->getSubsidyContractUnitSpaces()['unit_space_id'] ) && true == valId( $this->getUnitCount() ) && $this->getUnitCount() != \Psi\Libraries\UtilFunctions\count( array_unique( $this->getSubsidyContractUnitSpaces()['unit_space_id'] ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsgs( [ new CErrorMsg( ERROR_TYPE_VALIDATION, 'selected_units', 'Selected ' . \Psi\CStringService::singleton()->strtolower( CSubsidyContractType::getSubsidyContractNameById( $this->getSubsidyContractTypeId() ) ) . ' units must be equal to number of units.' ) ] );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intSubsidyContractTypeId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valSubsidyContractTypeId();
				$boolIsValid &= $this->valSubsidyContractSubTypeId();
				$boolIsValid &= $this->valContractNumber();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valUnitCount();
				$boolIsValid &= $this->valAppliesToAllUnits();
				$boolIsValid &= $this->valSubsidyUnitAssignmentTypeId();
				if( true == in_array( $this->getSubsidyContractTypeId(), CSubsidyContractType::$c_arrintContractUnitCountSubsidyContractTypes ) ) {
					$boolIsValid &= $this->valHapTitle();
					$boolIsValid &= $this->valHapPersonName();
					$boolIsValid &= $this->valFirstHapVoucherMonth();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSubsidyContractTypeId();
				$boolIsValid &= $this->valSubsidyContractSubTypeId();
				$boolIsValid &= $this->valContractNumber();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				$boolIsValid &= $this->valUnitCount();
				$boolIsValid &= $this->valAppliesToAllUnits();
				$boolIsValid &= $this->valSubsidyUnitAssignmentTypeId();
				if( true == in_array( $this->getSubsidyContractTypeId(), CSubsidyContractType::$c_arrintContractUnitCountSubsidyContractTypes ) ) {
					$boolIsValid &= $this->valHapTitle();
					$boolIsValid &= $this->valHapPersonName();
					$boolIsValid &= $this->valFirstHapVoucherMonth();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_home_contract':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valUnitsCount();
				$boolIsValid &= $this->valUnitSpaces();
				$boolIsValid &= $this->valSelectedUnits();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>