<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCachedAvailabilities
 * Do not add any new functions to this class.
 */

class CCachedAvailabilities extends CBaseCachedAvailabilities {

	public static function fetchCachedAvailabilitiesByPropertyFloorplanIdsByPropertyIdsByCid( $objFloorplanAvailabilityFilter, $intCid, $objDatabase ) {
		if( false == valObj( $objFloorplanAvailabilityFilter, CFloorplanAvailabilityFilter::class ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$arrstrSqlWhereClause = [];

		$strSqlSelect = $strSqlJoins = $strSqlSelectRates = $strPropertyFloorPlanIds = $strUnitSpaceIds = '';
		if( true == valArr( $objFloorplanAvailabilityFilter->getLeaseTermIds() ) ) {
			if( false == $objFloorplanAvailabilityFilter->getIsFlexible() && strtotime( $objFloorplanAvailabilityFilter->getMoveInDate() ) > strtotime( '+89 days' ) ) {
				$objRateLogsFilter = new CRateLogsFilter( $objFloorplanAvailabilityFilter );
				\Psi\Eos\Entrata\CRateLogs::createService()->loadPivotedRates( $objRateLogsFilter, $objDatabase );
				$strSqlSelectRates = ', \'rates\', jsonb_build_object( \'base_amount\', cr.entrata_base_amount,
																	\'base_tax\', cr.entrata_base_tax,
																	\'base_rent\', cr.entrata_advertised_base_rent,
																	\'amenity_amount\', cr.entrata_amenity_amount,
																	\'amenity_tax\', cr.entrata_amenity_tax,
																	\'amenity_rent\', cr.entrata_advertised_amenity_rent,
																	\'special_amount\', cr.entrata_special_amount,
																	\'special_tax\', cr.entrata_special_tax,
																	\'special_rent\', cr.entrata_advertised_special_rent,
																	\'add_on_amount\', cr.entrata_add_on_amount,
																	\'add_on_tax\', cr.entrata_add_on_tax,
																	\'add_on_rent\', cr.entrata_advertised_add_on_rent,
																	\'market_rent\', cr.entrata_advertised_market_rent,
																	\'total_deposit\', cr.total_deposit,
																	\'ar_trigger_id\', cr.billing_frequency,
																	\'exceeds_expiration_limit\', plte.exceeds_expiration_limit)';

				$strSqlJoins .= ' LEFT JOIN detailed_pricing cr ON ( cr.cid =  ca.cid AND cr.unit_space_id = ca.unit_space_id AND cr.lease_term_id IN ( ' . implode( ',', $objFloorplanAvailabilityFilter->getLeaseTermIds() ) . ' ) )';

			} else {

				if( true == $objFloorplanAvailabilityFilter->getIsFlexible() && true == valArr( $objFloorplanAvailabilityFilter->getLeaseStartWindowIds() ) ) {
					$strSqlCachedRatesJoinCondition = ' AND cr.lease_start_window_id IN ( ' . implode( ',', $objFloorplanAvailabilityFilter->getLeaseStartWindowIds() ) . ' ) AND cr.effective_date = \'' . date( 'm/d/Y' ) . '\'::DATE';
				} else {
					$strSqlCachedRatesJoinCondition = ' AND cr.effective_date = ca.available_date';
				}

				$strSqlJoins       .= ' LEFT JOIN cached_rates cr ON ( cr.cid =  ca.cid AND cr.unit_space_id = ca.unit_space_id AND cr.lease_term_id IN ( ' . implode( ',', $objFloorplanAvailabilityFilter->getLeaseTermIds() ) . ' ) ' . $strSqlCachedRatesJoinCondition . ' )';
				$strSqlSelectRates = ', \'rates\', jsonb_build_object(
																	\'base_amount\', cr.base_amount,
																	\'base_tax\', cr.base_tax,
																	\'base_rent\', cr.base_rent,
																	\'amenity_amount\', cr.amenity_amount,
																	\'amenity_tax\', cr.amenity_tax,
																	\'amenity_rent\', cr.amenity_rent,
																	\'special_amount\', cr.special_amount,
																	\'special_tax\', cr.special_tax,
																	\'special_rent\', cr.special_rent,
																	\'add_on_amount\', cr.add_on_amount,
																	\'add_on_tax\', cr.add_on_tax,
																	\'add_on_rent\', cr.add_on_rent,
																	\'market_rent\', cr.market_rent,
																	\'total_deposit\', cr.total_deposit,
																	\'ar_trigger_id\', cr.ar_trigger_id,
																	\'exceeds_expiration_limit\', plte.exceeds_expiration_limit)';
			}

			if( true == is_numeric( $objFloorplanAvailabilityFilter->getMaxRent() ) && 0 < ( int ) $objFloorplanAvailabilityFilter->getMaxRent() ) {
				$arrstrSqlWhereClause[] = ' ( cr.market_rent <= ' . $objFloorplanAvailabilityFilter->getMaxRent() . ' ) ';
			}

			if( true == is_numeric( $objFloorplanAvailabilityFilter->getMinRent() ) && 0 < ( int ) $objFloorplanAvailabilityFilter->getMinRent() ) {
				$arrstrSqlWhereClause[] = ' ( cr.market_rent >= ' . $objFloorplanAvailabilityFilter->getMinRent() . ' ) ';
			}

			if( true == $objFloorplanAvailabilityFilter->getIsFlexible() && true == valStr( $objFloorplanAvailabilityFilter->getMoveInStartDate() ) && true == valStr( $objFloorplanAvailabilityFilter->getMoveInEndDate() ) ) {
				$strSqlSelect .= ', CASE WHEN SUM( CASE WHEN ( ca.available_date BETWEEN \'' . $objFloorplanAvailabilityFilter->getMoveInStartDate() . '\'::DATE AND \'' . $objFloorplanAvailabilityFilter->getMoveInEndDate() . '\'::DATE ) AND ( true = is_available AND false = has_applicant AND false = is_preleased AND false = is_reserved AND oc.contract_id IS NULL ) THEN 1 ELSE 0 END ) > ( \'' . $objFloorplanAvailabilityFilter->getMoveInEndDate() . '\'::DATE - \'' . $objFloorplanAvailabilityFilter->getMoveInStartDate() . '\'::DATE ) THEN TRUE ELSE FALSE END AS custom_is_available';
			}

			$strSqlJoins .= ' LEFT JOIN property_gl_settings pgs ON ( pgs.cid = ' . ( int ) $objFloorplanAvailabilityFilter->getCid() . ' AND cr.property_id = pgs.property_id )
				LEFT OUTER JOIN lease_expiration_structures les ON ( pgs.cid = les.cid AND les.id = pgs.lease_expiration_structure_id )
				LEFT OUTER JOIN property_lease_term_expirations plte ON (
				 plte.cid = les.cid
				 AND plte.property_id = cr.property_id
				 AND (EXTRACT( Month FROM DATE ( DATE ( date( \'' . $objFloorplanAvailabilityFilter->getMoveInDate() . '\' ) + ( cr.lease_term_months || \' month \' )::INTERVAL)- ( 1 || \' day \' )::INTERVAL )) = date_part( \'Month\', plte.month))
				 AND ( EXTRACT( Year FROM DATE ( DATE ( date( \'' . $objFloorplanAvailabilityFilter->getMoveInDate() . '\') + ( cr.lease_term_months || \'month\' )::INTERVAL )- ( 1 || \' day \' )::INTERVAL ) ) = date_part( \'YEAR\', plte.month ) ) ) ';

		} else if( '-1' == $objFloorplanAvailabilityFilter->getLeaseTermIds() && true == valStr( $objFloorplanAvailabilityFilter->getMoveInStartDate() ) && true == valStr( $objFloorplanAvailabilityFilter->getMoveInEndDate() ) ) {
			$strSqlSelect .= ', CASE WHEN SUM( CASE WHEN ( ca.available_date BETWEEN \'' . $objFloorplanAvailabilityFilter->getMoveInStartDate() . '\'::DATE AND \'' . $objFloorplanAvailabilityFilter->getMoveInEndDate() . '\'::DATE ) AND ( true = is_available AND false = has_applicant AND false = is_preleased AND false = is_reserved AND oc.contract_id IS NULL ) THEN 1 ELSE 0 END ) > ( \'' . $objFloorplanAvailabilityFilter->getMoveInEndDate() . '\'::DATE - \'' . $objFloorplanAvailabilityFilter->getMoveInStartDate() . '\'::DATE ) THEN TRUE ELSE FALSE END AS custom_is_available';
		}

		if( true == valArr( $objFloorplanAvailabilityFilter->getPropertyFloorplanIds() ) ) {
			$strPropertyFloorPlanIds = implode( ',', $objFloorplanAvailabilityFilter->getPropertyFloorplanIds() );
		}

		if( true == valArr( $objFloorplanAvailabilityFilter->getUnitSpaceIds() ) ) {
			$strUnitSpaceIds = implode( ',', $objFloorplanAvailabilityFilter->getUnitSpaceIds() );
		}

		$strSql = 'SELECT
					  cached_units.*,
					  us.building_name,
					  pf.floorplan_name,
					  pu.number_of_bathrooms,
					  pu.number_of_bedrooms,
					  pu.square_feet
					FROM
						(
							SELECT
								ca.cid,
								ca.property_id,
								ca.property_floorplan_id,
								ca.unit_type_id,
								ca.property_unit_id,
								ca.unit_space_id,
								CASE WHEN ca.has_pets IS TRUE THEN 1 ELSE 0 END as has_pets,
								CASE WHEN ca.has_specials IS TRUE THEN 1 ELSE 0 END as has_specials,
								CASE WHEN bool_and( CASE 
														WHEN true = is_available AND false = has_applicant AND false = is_preleased AND false = is_reserved AND oc.contract_id IS NULL THEN TRUE
														WHEN ca.available_date < \'' . $objFloorplanAvailabilityFilter->getMoveInDate() . '\'::DATE THEN TRUE
														ELSE FALSE
													END ) IS TRUE  THEN 1 ELSE 0 
								END as is_available,
								CASE WHEN ca.is_occupied IS TRUE THEN 1 ELSE 0 END as is_occupied,
								jsonb_agg( jsonb_build_object( ca.available_date, jsonb_build_object( \'leases\', jsonb_extract_path( ca.details, \'leases\' ), \'contract\', jsonb_extract_path( ca.details, \'contract\' ), \'make_ready\', jsonb_extract_path( ca.details, \'make_ready\' ), \'unit_exclusion_reason\', jsonb_extract_path( ca.details, \'unit_exclusion_reason\' ) ' . $strSqlSelectRates . ' ) )
								ORDER BY
								ca.available_date ) AS details
								' . $strSqlSelect . '
							FROM
								load_cached_availabilities ( ' . $intCid . ', ARRAY [ ' . implode( ',', $objFloorplanAvailabilityFilter->getPropertyIds() ) . ' ]::INT [ ], ARRAY [ ' . $strPropertyFloorPlanIds . ' ]::INT [ ], ARRAY [ ]::INT [ ], ARRAY [ ]::INT [ ], ARRAY [ ' . $strUnitSpaceIds . ' ]::INT [ ], \'' . date( 'm/d/Y' ) . '\'::DATE, \'' . date( 'm/d/Y', strtotime( ' + ' . CAdvanceUnitAvailabilitySelectorController::MATRIX_DAYS . ' days ' ) ) . '\'::DATE ) ca 
								LEFT  JOIN LATERAL (
							        SELECT
										oc.id AS contract_id         
							        FROM
										organization_contract_unit_spaces ocus
										JOIN organization_contracts oc ON ( ocus.cid = oc.cid AND ocus.organization_contract_id = oc.id AND oc.organization_contract_status_type_id IN ( 1, 2 ) )
										JOIN lease_start_windows lsw ON ( oc.cid = lsw.cid AND oc.lease_start_window_id = lsw.id AND lsw.deleted_on IS NULL AND lsw.is_active IS TRUE )
									WHERE
										ca.available_date::DATE BETWEEN lsw.start_date AND lsw.end_date
										AND  ocus.cid = ca.cid AND ocus.unit_space_id = ca.unit_space_id AND ocus.deleted_on IS NULL         
							    ) as oc ON true' . $strSqlJoins . '
								' . ( true == valArr( $arrstrSqlWhereClause ) ? ' WHERE ' : '' ) . '
								' . implode( ' AND', $arrstrSqlWhereClause ) . '
							GROUP BY
								ca.cid,
								ca.property_id,
								ca.property_floorplan_id,
								ca.unit_type_id,
								ca.property_unit_id,
								ca.unit_space_id,
								ca.has_pets,
								ca.has_specials,
								ca.is_occupied
							ORDER BY
								ca.cid,
								ca.property_id,
								ca.property_floorplan_id,
								ca.property_unit_id,
								ca.unit_space_id
						) AS cached_units
						JOIN property_floorplans pf ON ( pf.cid = cached_units.cid AND pf.property_id = cached_units.property_id AND pf.id = cached_units.property_floorplan_id )
						JOIN property_units pu ON ( pu.cid = cached_units.cid AND pu.property_id = cached_units.property_id AND pu.id = cached_units.property_unit_id )
						JOIN unit_spaces us ON ( us.cid = cached_units.cid AND us.property_id = cached_units.property_id AND us.id = cached_units.unit_space_id )
					ORDER BY
						available_on,
						' . $objDatabase->getCollateSort( 'us.building_name' ) . ' ASC NULLS FIRST,
						natural_sort( us.unit_number ) ASC NULLS FIRST';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCachedAvailableUnitIdsByFilterData( $arrmixFilter, $objDatabase ) {

		if( false == valId( $arrmixFilter['cid'] ) || false == valId( $arrmixFilter['property_id'] ) || false == valStr( $arrmixFilter['lease_start_date'] ) || false == valStr( $arrmixFilter['lease_end_date'] ) ) {
			return[];
		}

		$strPropertyFloorPlanId = '';

		if( true == valId( $arrmixFilter['property_floorplan_id'] ) ) {
			$strPropertyFloorPlanId = $arrmixFilter['property_floorplan_id'];
		}

		$strInnerSql = 'SELECT unit_space_id,
						       CASE
						         WHEN bool_and(CASE
						                         WHEN ca.is_available IS TRUE THEN ca.is_available
						                         ELSE FALSE
						                       END) IS TRUE THEN TRUE
						         ELSE FALSE
						       END AS is_available
						FROM load_cached_availabilities ( ' . $arrmixFilter['cid'] . ', ARRAY [ ' . $arrmixFilter['property_id'] . ' ]::INT [ ], ARRAY [ ' . $strPropertyFloorPlanId . ' ]::INT [ ], ARRAY [ ]::INT [ ], ARRAY [ ]::INT [ ], ARRAY [ ]::INT [ ], \'' . $arrmixFilter['lease_start_date'] . '\'::DATE, \'' . $arrmixFilter['lease_end_date'] . '\'::DATE ) ca
						GROUP BY unit_space_id';

		$strSql = ' SELECT unit_space_id FROM ( ' . $strInnerSql . ' ) as subquery WHERE subquery.is_available::BOOLEAN IS TRUE';

		return fetchData( $strSql, $objDatabase );
	}

}

?>