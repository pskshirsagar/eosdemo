<?php

class CEmailTemplateFolder extends CBaseEmailTemplateFolder {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailTemplateFolderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;
		$this->m_strName = trim( $this->m_strName );
		$strName = preg_replace( '/[^a-zA-Z0-9_\-\s]/', '', $this->m_strName );

		if( \Psi\CStringService::singleton()->strlen( $this->m_strName ) != \Psi\CStringService::singleton()->strlen( $strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Only alphanumeric characters [0 to 9, A to Z ,'-' , '_'and a-z ] are allowed in name." ) ) );
			return false;
		}

		if( 2 > \Psi\CStringService::singleton()->strlen( $strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Folder Name should contain at least 2 characters.' ) ) );
			return false;
		}

		$this->m_strName = preg_replace( '/(\s\s+)/', ' ', $this->m_strName );

		$strWhere = 'WHERE cid = ' . $this->m_intCid;
		$strWhere .= ' AND lower(name) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->m_strName ) ) . '\'';
		$strWhere .= ' AND deleted_on IS NULL AND deleted_by IS NULL';

		$intCount = \Psi\Eos\Entrata\CEmailTemplateFolders::createService()->fetchEmailTemplateFolderCount( $strWhere, $objDatabase );

		if( 0 < $intCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Folder Already Exists' ) ) );
			return false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function createEmailTemplateFolder() {

		$objEmailTemplateFolder = new CEmailTemplateFolder();
		$objEmailTemplateFolder->setCid( 1 );

		return $objEmailTemplateFolder;
	}

}
?>