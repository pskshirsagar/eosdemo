<?php

class CPaymentAllowanceType extends CBasePaymentAllowanceType {

	const ALLOW_ALL			= 1;
	const CASH_EQUIVALENT	= 2;
	const BLOCK_ALL			= 3;

	public static $c_arrintPaymentAllowanceTypes = [ self::CASH_EQUIVALENT, self::BLOCK_ALL ];

	public static function getPaymentAllowanceTypeNameByPaymentAllowanceTypeId( $intPaymentAllowanceTypeId ) {

		$strAllowanceTypeName = NULL;

		switch( $intPaymentAllowanceTypeId ) {
			case CPaymentAllowanceType::ALLOW_ALL:
				$strAllowanceTypeName = __( 'Allow All Payment Types' );
				break;

			case CPaymentAllowanceType::CASH_EQUIVALENT:
				$strAllowanceTypeName = __( 'Certified Funds Only' );
				break;

			case CPaymentAllowanceType::BLOCK_ALL:
				$strAllowanceTypeName = __( 'Don\'t Allow Payments' );
				break;

            default:
            	$boolIsValid = true;
            	break;
		}

		return $strAllowanceTypeName;
	}

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getPaymentAllowanceTypeIdToStr( $intPaymentAllowanceTypeId ) {

		$strAllowanceTypeName = NULL;

		switch( $intPaymentAllowanceTypeId ) {
			case CPaymentAllowanceType::ALLOW_ALL:
				$strAllowanceTypeName = __( 'Allow All Payment Types' );
				break;

			case CPaymentAllowanceType::CASH_EQUIVALENT:
				$strAllowanceTypeName = __( 'Certified Funds Only' );
				break;

			case CPaymentAllowanceType::BLOCK_ALL:
				$strAllowanceTypeName = __( 'Don\'t Allow Payments' );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $strAllowanceTypeName;
	}

}
?>