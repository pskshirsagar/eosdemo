<?php
// required eos files

class CCustomerReferral extends CBaseCustomerReferral {

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valArTransactionId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIncentiveAmount() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( $this->m_strNameFirst == NULL ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Referal first name is required.', 502 ) );
			$boolIsValid = false;
		} elseif( strlen( $this->m_strNameFirst ) <= 0 ) {
			// It has to be > 0 long
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Referal first name is required.', 502 ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( $this->m_strNameLast == NULL ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Referal last name is required.', 502 ) );
			$boolIsValid = false;
		} elseif( strlen( $this->m_strNameLast ) <= 0 ) {
			// It has to be > 0 long
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Referal last name is required.', 502 ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		if( true == is_null( $this->m_strEmailAddress ) || 0 == strlen( trim( $this->m_strEmailAddress ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email adress is required.', 502 ) );
			$boolIsValid = false;
		} elseif( false == is_null( $this->m_strEmailAddress ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Invalid email address.', 502 ) );
			$boolIsValid = false;
		}
	    return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumber() {
		$boolIsValid = true;
		if( false == is_null( $this->m_strPhoneNumber ) || true == valStr( $this->m_strPhoneNumber ) ) {
			if( true == preg_match( '/\-{3,}/', $this->m_strPhoneNumber ) || 1 !== preg_match( '/^([0-9]{3})[^0-9,-]*([0-9,-]{3})[^0-9,-]*([0-9,-]{4,9})$/', $this->m_strPhoneNumber ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number not in valid format.', 502 ) );
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

	public function valPersonalNote() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBrochureSentOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valConvertedToLeadOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIncentiveCreditedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIncentiveApprovedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIncentiveApprovedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valViewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valViewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsSkipPhoneValidation = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_insert_resident_portal20':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				if( false == $boolIsSkipPhoneValidation ) {
					$boolIsValid &= $this->valPhoneNumber();
				}
				$boolIsValid &= $this->valEmailAddress();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>