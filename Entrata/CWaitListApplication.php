<?php

class CWaitListApplication extends CBaseWaitListApplication {

	protected $m_intRejectionLimit;

	protected $m_objWaitlistPoint;
	protected $m_objWaitlistFloorPlanIds;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaitListId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectionCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsException() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaitListPointId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaitStartOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaitEndOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrmixValues['rejection_limit'] ) )
			$this->setRejectionLimit( $arrmixValues['rejection_limit'] );

	}

	public function getRejectionlimit() {
		return $this->m_intRejectionLimit;
	}

	public function setRejectionLimit( $intRejectionLimit ) {
		$this->m_intRejectionLimit = $intRejectionLimit;
	}

	public function getWaitlistPoint() {
		return $this->m_objWaitlistPoint;
	}

	public function setWaitlistPoint( $objWaitlistPoint ) {
		$this->m_objWaitlistPoint = $objWaitlistPoint;
	}

	public function fetchWaitlistPoint( $objDatabase ) {
		if( true == $this->valId( $this->getWaitListPointId() ) ) {
			$this->m_objWaitlistPoint = CWaitListPoints::fetchWaitListPointByIdByCid( $this->getWaitListPointId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objWaitlistPoint;
	}

	public function getOrFetchWaitlistPoint( $objDatabase ) {
		if( true == $this->valId( $this->getWaitListPointId() ) && false == valObj( $this->m_objWaitlistPoint, 'CWaitListPoint' ) ) {
			$this->m_objWaitlistPoint = CWaitListPoints::fetchWaitListPointByIdByCid( $this->getWaitListPointId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objWaitlistPoint;
	}

	public function fetchWaitlistFloorPlans( $objDatabase ) {
		$this->m_objWaitlistFloorPlans = CWaitListPoints::fetchWaitListPointByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
		return $this->m_objWaitlistFloorPlans;
	}

	public function getOrFetchWaitlistFloorPlans( $objDatabase ) {
		if( false == valObj( $this->m_objWaitlistFloorPlans, 'CApplicationFloorPlans' ) ) {
			$this->m_objWaitlistFloorPlans = CWaitListPoints::fetchWaitListPointByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
		}
	}

	public function getWaitlistPointName() {
		if( true == valObj( $this->m_objWaitlistPoint, 'CWaitListPoint' ) ) {
			return $this->m_objWaitlistPoint->getName();
		} else {
			return '';
		}
	}

}
?>