<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerReferrals
 * Do not add any new functions to this class.
 */

class CCustomerReferrals extends CBaseCustomerReferrals {

	public static function fetchPaginatedCustomerReferralsByPropertyIdsByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $intCid, $objDatabase, $boolIsShowDeleted = false ) {
		if( false == isset ( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$intOffset 		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit 		= ( int ) $intPageSize;
		$strFieldName 	= ( true === $boolIsShowDeleted ) ? 'updated_on' : 'created_on';

		$strSql = 'SELECT
				 		DISTINCT ON ( ' . $strFieldName . ', cr.id ) cr.*
				  	FROM
				 		customer_referrals cr
				  		JOIN customers cc ON ( cc.id = cr.customer_id AND cc.cid = cr.cid AND cc.cid = ' . ( int ) $intCid . ' )
					WHERE
				 		cr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' .
						( ( true === $boolIsShowDeleted ) ? ' AND cr.deleted_on IS NOT NULL ' : ' AND cr.deleted_on IS NULL ' ) . '
					ORDER BY
						' . $strFieldName . ' DESC, cr.id DESC
					OFFSET
    		   	   		' . ( int ) $intOffset . '
    			   	LIMIT
    			   		' . $intLimit;

		return self::fetchCustomerReferrals( $strSql, $objDatabase );
	}

	public static function fetchCustomerReferralsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIsRemovedCustomerReferrals = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strWhere = ' WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id  IN (' . implode( ',', $arrintPropertyIds ) . ')';

		$strWhere .= ( true == $boolIsRemovedCustomerReferrals ) ? ' AND deleted_on IS NOT NULL' : ' AND deleted_on IS NULL';

		return self::fetchCustomerReferralCount( $strWhere, $objDatabase );
	}

	public static function fetchPaginatedCustomerReferralsCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						count( DISTINCT  cr.id )
				  	FROM
				 		customer_referrals cr
				  		JOIN customers cc ON ( cc.id = cr.customer_id AND cc.cid = cr.cid AND cc.cid = ' . ( int ) $intCid . ' )
				  	WHERE
				 		cr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cr.deleted_on IS NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );

    	if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
	}

	public static function fetchCustomerReferralsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;
		$strSql = 'SELECT * FROM customer_referrals WHERE id IN (' . implode( ',', $arrintIds ) . ') AND cid = ' . ( int ) $intCid;
		return self::fetchCustomerReferrals( $strSql, $objDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchPaginatedCustomerReferralsByDashboardFilterByCid( $objDashboardFilter, $intCid, $intOffset, $intPageSize, $strSortBy, $objDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							DISTINCT ON( cr.id )
							CASE
								WHEN TRIM( dp.leads_resident_referrals->>\'urgent_referral_submitted_since\' ) != \'\' AND NOW() >= ( cr.created_on::timestamp + INTERVAL \'1 hour\' * ( dp.leads_resident_referrals->>\'urgent_referral_submitted_since\' )::int ) THEN
									3
								WHEN TRIM( dp.leads_resident_referrals->>\'important_referral_submitted_since\' ) != \'\' AND NOW() >= ( cr.created_on::timestamp + INTERVAL \'1 hour\' * ( dp.leads_resident_referrals->>\'important_referral_submitted_since\' )::int ) THEN
									2
								ELSE
									1
							END as priority,
							cr.id,
							cr.cid,
							func_format_customer_name( cr.name_first, cr.name_last ) as referral_name,
							cr.email_address,
							cr.phone_number,
							cr.created_on,
							func_format_customer_name( cl.name_first, cl.name_last ) as referred_by,
							COALESCE( cl.building_name || \' - \' || cl.unit_number_cache, cl.building_name, cl.unit_number_cache ) as unit_number,
							cl.property_name,
							cl.id AS lease_id
						FROM
							customer_referrals cr
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], array [' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON ( lp.cid = cr.cid AND lp.is_disabled = 0 AND lp.property_id = cr.property_id )
							JOIN cached_leases cl ON ( cl.cid = cr.cid AND cl.property_id = cr.property_id )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = cr.cid )
						WHERE
							cr.cid = ' . ( int ) $intCid . '
							AND cr.deleted_on IS NULL
							AND cl.id =
          					          (	SELECT
                        					max(lc.lease_id)
                         				FROM
                         					lease_customers lc
                         				WHERE
                         					lc.cid= cr.cid
                           					AND lc.property_id = cr.property_id
                            				AND lc.customer_id =cr.customer_id )
						ORDER BY
							cr.id,
							cl.id DESC
					) as referrals
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY
						' . $strSortBy . '
					OFFSET
    		   	   		' . ( int ) $intOffset . '
    			   	LIMIT
    			   		' . ( int ) $intPageSize;

		return fetchData( $strSql, $objDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchCustomerReferralsCountByDashboardFilterByCid( $objDashboardFilter, $intCid, $objDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) return NULL;

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							DISTINCT ON( cr.id )
							CASE
								WHEN TRIM( dp.leads_resident_referrals->>\'urgent_referral_submitted_since\' ) != \'\' AND NOW() >= ( cr.created_on::timestamp + INTERVAL \'1 hour\' * ( dp.leads_resident_referrals->>\'urgent_referral_submitted_since\' )::int ) THEN
									3
								WHEN TRIM( dp.leads_resident_referrals->>\'important_referral_submitted_since\' ) != \'\' AND NOW() >= ( cr.created_on::timestamp + INTERVAL \'1 hour\' * ( dp.leads_resident_referrals->>\'important_referral_submitted_since\' )::int ) THEN
									2
								ELSE
									1
							END as priority,
							cr.id,
							cr.cid,
							cl.property_id,
							p.property_name
						FROM
							customer_referrals cr
							JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], array [' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON ( lp.cid = cr.cid AND lp.is_disabled = 0 AND lp.property_id = cr.property_id )
							JOIN properties p ON ( p.cid = cr.cid AND p.id = cr.property_id )
							JOIN cached_leases cl ON ( cl.cid = cr.cid AND cl.property_id = cr.property_id )
							LEFT JOIN dashboard_priorities dp ON ( dp.cid = cr.cid )
						WHERE
							cr.cid = ' . ( int ) $intCid . '
							AND cr.deleted_on IS NULL
							AND cl.id =
          					          (	SELECT
                        					max(lc.lease_id)
                         				FROM
                         					lease_customers lc
                         				WHERE
                         					lc.cid= cr.cid
                           					AND lc.property_id = cr.property_id
                            				AND lc.customer_id =cr.customer_id )
					) as referrals
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY
						priority DESC' .
					( false == $boolIsGroupByProperties ?
						' OFFSET
							0
	    				LIMIT
	    					100'
					: '' );

		if( false == $boolIsGroupByProperties ) {
			$strSql = ( ( 0 != $intMaxExecutionTimeOut )? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '' ) . 'SELECT COUNT(1) || \'-\' || COALESCE( MAX( priority ), 1 ) AS count FROM ( ' . $strSql . ' ) as subQ';

			return parent::fetchColumn( $strSql, 'count', $objDatabase );
		} else {
			$strSql = 'SELECT COUNT(1) AS leads_referrals, priority, property_id, property_name FROM ( ' . $strSql . ' ) AS subQ GROUP BY priority, property_id, property_name';

			return fetchData( $strSql, $objDatabase );
		}

	}

	public static function fetchCustomerReferralsWithDetailsByIdByCid( $intCustomerReferralId, $intCid, $objDatabase ) {

		if( false == valId( $intCustomerReferralId ) ) return NULL;

		$strSql = 'SELECT
						cr.*,
						cdp.details
					FROM
						customer_referrals cr
						JOIN cached_dashboard_priorities cdp ON ( cr.cid = cdp.cid AND cr.id = cdp.reference_id )
					WHERE
						cr.id  = ' . $intCustomerReferralId . '
						AND cr.cid = ' . ( int ) $intCid . '
						AND cdp.details ->> \'lead_type\' = \'Resident Portal Referral\'';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrmixResponse[0] ) ) return $arrmixResponse[0];
	}

}
?>