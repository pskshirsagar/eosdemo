<?php

class CApplicantRoommateInterest extends CBaseApplicantRoommateInterest {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicantId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRoommateInterestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRoommateInterestOptionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPreferenceDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWrittenResponse() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valImportanceRating() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default;
            	break;
        }

        return $boolIsValid;
    }
}
?>