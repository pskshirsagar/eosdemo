<?php

class CPrivacyItemGroup extends CBasePrivacyItemGroup {

	const ESSENTIAL = 1;
	const PERFORMANCE_AND_FUNCTIONALITY = 2;
	const ANALYTICS_AND_CUSTOMIZATION = 3;
	const ADVERTISING = 4;
	const SOCIAL_NETWORKING = 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>