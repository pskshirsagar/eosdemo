<?php

class CPropertyStandardResponse extends CBasePropertyStandardResponse {

	protected $m_boolIsHtmlMessage;

    /**
     * Set Functions
     */

    public function setIsHtmlMessage( $boolIsHtmlMessage ) {
    	$this->m_boolIsHtmlMessage = $boolIsHtmlMessage;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['is_html_message'] ) ) $this->setIsHtmlMessage( $arrValues['is_html_message'] );
    }

    /**
     * Get Functions
     */

    public function getIsHtmlMessage() {
    	return $this->m_boolIsHtmlMessage;
    }

    /**
     * Validate Functions
     */

    public function valCid() {
    	$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || 1 > ( $this->m_intCid = ( int ) $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id does not appear to be valid.' ) );
		}

		return $boolIsValid;
    }

    public function valPropertyId() {
    	$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || 1 > ( $this->m_intPropertyId = ( int ) $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property id does not appear to be valid.' ) );
		}

		return $boolIsValid;
    }

    public function valName() {
    	$boolIsValid = true;

		if( false == isset( $this->m_strName ) || 3 > strlen( $this->m_strName = trim( $this->m_strName ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'A name must contain at least 3 characters.' ) );
			return false;
		}

		$strName = preg_replace( '/[^a-zA-Z0-9\s]/', '', trim( $this->m_strName ) );

		if( 0 == strlen( $strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Invalid Name.' ) );
			return false;
		}

		$this->m_strName = preg_replace( '/(\s\s+)/', ' ', $this->m_strName );

		return $boolIsValid;
    }

    public function valMessage() {
       	$boolIsValid = true;

		if( true == is_null( trim( $this->m_strMessage ) ) || 0 == strlen( trim( $this->m_strMessage ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', 'Message is required.' ) );
		}

		return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valMessage();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>