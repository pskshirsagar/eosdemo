<?php

class CWebVisit extends CBaseWebVisit {

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, true, $boolDirectSet );

    	if( true == isset( $arrValues['visitors_count'] ) )	$this->setVisitorCount( $arrValues['visitors_count'] );
    	if( true == isset( $arrValues['clicks_count'] ) ) 	$this->setClicksCount( $arrValues['clicks_count'] );

    	if( true == isset( $arrValues['page_clicks_count'] ) ) 	$this->setPageClicksCount( $arrValues['page_clicks_count'] );
    	if( true == isset( $arrValues['page_views_count'] ) ) 	$this->setPageViewsCount( $arrValues['page_views_count'] );
    	if( true == isset( $arrValues['unique_visitors_count'] ) ) 	$this->setUniqueVisitorsCount( $arrValues['unique_visitors_count'] );
    }

    public function setVisitorCount( $intVisitorCount ) {
    	$this->m_intVisitorCount = $intVisitorCount;
    }

    public function setClicksCount( $intClicksCount ) {
    	$this->m_intClicksCount = $intClicksCount;
    }

    public function setPageClicksCount( $intPageViewsCount ) {
    	$this->m_intPageViewsCount = $intPageViewsCount;
    }

    public function setPageViewsCount( $intPageViewsCount ) {
    	$this->m_intPageViewsCount = $intPageViewsCount;
    }

    public function setUniqueVisitorsCount( $intUniqueVisitorsCount ) {
    	$this->m_intUniqueVisitorsCount = $intUniqueVisitorsCount;
    }

    /**
     * Get Functions
     */

    public function getVisitorCount() {
    	return $this->m_intVisitorCount;
    }

    public function getClicksCount() {
    	return $this->m_intClicksCount;
    }

    public function getPageClicksCount() {
    	return $this->m_intPageViewsCount;
    }

    public function getPageViewsCount() {
    	return $this->m_intPageViewsCount;
    }

    public function getUniqueVisitorsCount() {
    	return $this->m_intUniqueVisitorsCount;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        if( true == is_null( $this->getCid() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'client id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valWebsiteId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInterentListingServiceId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCraigslistPostId() {
        $boolIsValid = true;
        if( true == is_null( $this->getCraigslistPostId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Craigslist post id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valClAdId() {
    	$boolIsValid = true;
    	if( true == is_null( $this->getClAdId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Cl Ad id is required.' ) );
    	}
    	return $boolIsValid;
    }

    public function valWebVisitTypeId() {
        $boolIsValid = true;
        if( true == is_null( $this->getWebVisitTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Web visit type id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valTrafficCookiesId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valReferrer() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valVisitDatetime() {
        $boolIsValid = true;
        if( true == is_null( $this->getVisitDatetime() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Visit date time is required.' ) );
        }
        return $boolIsValid;
    }

    public function valViewCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIfFirstVisit() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valWebVisitTypeId();
            	$boolIsValid &= $this->valVisitDatetime();
				break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>