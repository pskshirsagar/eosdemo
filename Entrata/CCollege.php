<?php

class CCollege extends CBaseCollege {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntityName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valZipcode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLatitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLongitutde() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>