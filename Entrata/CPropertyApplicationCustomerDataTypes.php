<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyApplicationCustomerDataTypes
 * Do not add any new functions to this class.
 */

class CPropertyApplicationCustomerDataTypes extends CBasePropertyApplicationCustomerDataTypes {

	public static function fetchPropertyApplicationCustomerDataTypesByCompanyApplicationIdByPropertyIdByCid( $intCompanyApplicationId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						pacdt.*
					FROM
						property_application_customer_data_types pacdt
					WHERE
						pacdt.cid = ' . ( int ) $intCid . '
						AND pacdt.property_id = ' . ( int ) $intPropertyId . '
						And	pacdt.company_application_id = ' . ( int ) $intCompanyApplicationId;

		return self::fetchPropertyApplicationCustomerDataTypes( $strSql, $objDatabase );
	}

	public static function fetchPropertyApplicationCustomerDataTypesByApplicationIdCustomerDataTypeIdsByCid( $intApplicationId, $arrintCustomerDataTypeIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						pacdt.customer_data_type_id, pacdt.verification_type_ids
					FROM
						applications a 
						JOIN property_application_customer_data_types pacdt ON ( a.cid = pacdt.cid AND a.property_id = pacdt.property_id AND a.company_application_id = pacdt.company_application_id )
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.id = ' . ( int ) $intApplicationId . '
						AND pacdt.customer_data_type_id IN ( ' . implode( ',', $arrintCustomerDataTypeIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

}
?>