<?php

class CCompanyCharity extends CBaseCompanyCharity {

	public function createPropertyCharity() {

		$this->m_objPropertyCharity = new CPropertyCharity();
		$this->m_objPropertyCharity->setCid( $this->getCid() );
		$this->m_objPropertyCharity->setCompanyCharityId( $this->getId() );

		return $this->m_objPropertyCharity;
	}

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client Id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSystemCode( $strAction, $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getSystemCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_code', __( 'System Code is required.' ) ) );
		}

		if( VALIDATE_UPDATE == $strAction ) {
			$strWhereSql = sprintf( 'WHERE cid = %d AND id != %d AND system_code like \'%s\'', ( int ) $this->getCid(), ( int ) $this->getId(), $this->getSystemCode() );
		} else {
			$strWhereSql = sprintf( 'WHERE cid = %d AND system_code like \'%s\'', ( int ) $this->getCid(), $this->getSystemCode() );
		}

		if( 0 < CCompanyCharities::fetchCompanyCharityCount( $strWhereSql, $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_code', __( 'System code is already in use.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $strAction, $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
		}

		if( VALIDATE_UPDATE == $strAction ) {
			$strWhereSql = sprintf( 'WHERE cid = %d AND id != %d AND name like \'%s\'', ( int ) $this->getCid(), ( int ) $this->getId(), $this->getName() );
		} else {
			$strWhereSql = sprintf( 'WHERE cid = %d AND name like \'%s\'', ( int ) $this->getCid(), $this->getName() );
		}

		if( 0 < CCompanyCharities::fetchCompanyCharityCount( $strWhereSql, $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is already in use.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		return true;
	}

	public function valUrl() {
		$boolIsValid = true;

		if( false == is_null( $this->getUrl() ) ) {

			if( false == CValidation::checkUrl( $this->getUrl() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'url', __( 'Url is invalid.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaxMatchPercentage() {
		return true;
	}

	public function valMaxMatchAmount() {
		return true;
	}

	public function valIsGlobal() {
		$boolIsValid = true;

		if( 1 == $this->getIsGlobal() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'This is system generated. So can not be deleted.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( VALIDATE_INSERT, $objDatabase );
				$boolIsValid &= $this->valUrl();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( VALIDATE_UPDATE, $objDatabase );
				$boolIsValid &= $this->valUrl();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valCid();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function fetchCompanyMediaFileByMediaTypeId( $intMediaTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFileByCharityIdByMediaTypeIdByCid( $this->m_intId, $intMediaTypeId, $this->getCid(), $objDatabase );
	}

}

?>