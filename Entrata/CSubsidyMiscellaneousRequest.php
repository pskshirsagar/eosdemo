<?php

class CSubsidyMiscellaneousRequest extends CBaseSubsidyMiscellaneousRequest {

	protected $m_strSubsidyMiscellaneousRequestTypeCode;

	/**
	 * Setters
	 *
	 */

	public function setSubsidyMiscellaneousRequestTypeCode( $strSubsidyMiscellaneousRequestTypeCode ) {
		$this->m_strSubsidyMiscellaneousRequestTypeCode = $strSubsidyMiscellaneousRequestTypeCode;
	}

	/**
	 * Getters
	 *
	 */

	public function getSubsidyMiscellaneousRequestTypeCode() {
		return $this->m_strSubsidyMiscellaneousRequestTypeCode;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyHapRequestId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSubsidyHapRequestId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_hap_request_id', 'SubsidyHapRequest is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSubsidyMiscellaneousRequestTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSubsidyMiscellaneousRequestTypeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subsidy_miscellaneous_request_type_id', 'Miscellaneous request type is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valComments() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->m_fltRequestAmount ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_amount', 'Requested amount is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSubsidyMiscellaneousRequestTypeId();
				$boolIsValid &= $this->valRequestAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['miscellaneous_request_type_code'] ) ) $this->setSubsidyMiscellaneousRequestTypeCode( $arrmixValues['miscellaneous_request_type_code'] );

	}
}
?>