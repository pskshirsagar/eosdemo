<?php
use Psi\Eos\Entrata\CMaintenanceTemplates;

class CMaintenanceTemplate extends CBaseMaintenanceTemplate {

	protected $m_arrobjMaintenanceTemplates;
	protected $m_strMaintenanceRequestTypeName;
	/**
	 * Create Functions
	 */

	public function createPropertyMaintenanceTemplate() {

		$objPropertyMaintenanceTemplate = new CPropertyMaintenanceTemplate();
		$objPropertyMaintenanceTemplate->setCid( $this->m_intCid );
		$objPropertyMaintenanceTemplate->setMaintenanceTemplateId( $this->m_intId );

		return $objPropertyMaintenanceTemplate;
	}

	public function createMaintenanceRequest() {
		$objMaintenanceRequest = new CMaintenanceRequest();
		$objMaintenanceRequest->setMaintenanceTemplateId( $this->getId() );
		$objMaintenanceRequest->setCid( $this->getCid() );
		$objMaintenanceRequest->setMaintenanceProblemId( $this->getMaintenanceProblemId() );
		$objMaintenanceRequest->setProblemDescription( $this->getDescription() );
		$objMaintenanceRequest->setMaintenancePriorityId( $this->getMaintenancePriorityId() );
		$objMaintenanceRequest->setMaintenanceRequestTypeId( $this->getMaintenanceRequestTypeId() );
		$objMaintenanceRequest->setMaintenanceLocationId( $this->getMaintenanceLocationId() );

		return $objMaintenanceRequest;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchMaintenanceTemplateById( $objClientDatabase ) {
		return CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidById( $this->getCid(), $this->getId(), $objClientDatabase );
	}

	public function fetchMaintenanceTemplatesByParentMaintenanceTemplateId( $objDatabase ) {
		return CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByParentMaintenanceTemplateIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	 * Set Functions
	 */

	public function setMaintenanceTemplates( $arrobjMaintenanceTemplates ) {
		$this->m_arrobjMaintenanceTemplates = $arrobjMaintenanceTemplates;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setMaintenanceRequestTypeName( $strMaintenanceRequestTypeName ) {
		$this->m_strMaintenanceRequestTypeName = $strMaintenanceRequestTypeName;
	}

	public function setMaintenanceTemplateAttachment( $objAttachment ) {
		$this->m_objAttachment = $objAttachment;
	}

	/**
	 * Get Functions
	 */

	public function getMaintenanceTemplates() {
		return $this->m_arrobjMaintenanceTemplates;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getMaintenanceRequestTypeName() {
		return $this->m_strMaintenanceRequestTypeName;
	}

	public function getMaintenanceTemplateAttachment() {
		return $this->m_objAttachment;
	}

	/**
	 * Validation Functions
	 */

	public function valId( $objDatabase ) {

		$boolIsValid = true;
		$strErrorMsg = '';

		$arrobjPropertyMaintenanceTemplates = \Psi\Eos\Entrata\CPropertyMaintenanceTemplates::createService()->fetchPropertyMaintenanceTemplateByMaintenanceTemplateIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$arrmixMaintenanceGroups = \Psi\Eos\Entrata\CMaintenanceGroups::createService()->fetchMaintenanceGroupsByMaintenanceTemplateIdByCid( $this->getId(), $this->getCid(), $objDatabase );

		$arrobjMaintenanceRequests   = \Psi\Eos\Entrata\CMaintenanceRequests::createService()->fetchMaintenanceRequestsByMaintenanceTemplateIdsByCid( [ $this->getId() ], $this->getCid(), $objDatabase );
		if( true == valArr( $arrobjPropertyMaintenanceTemplates ) ) {
			$strErrorMsg .= __( 'Template cannot be deleted because it is associated to one or more properties.' );
			$boolIsValid = false;
		}

		if( true == valArr( $arrmixMaintenanceGroups ) ) {
			$strErrorMsg .= __( 'Maintenance group(s) exists for the maintenance template.' );
			$boolIsValid = false;
		}

		if( true == valArr( $arrobjMaintenanceRequests ) ) {
			$strErrorMsg .= __( 'Maintenance request(s) exists for the maintenance template.' );
			$boolIsValid = false;
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strErrorMsg, NULL ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valParentMaintenanceTemplateId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valMaintenancePriorityId() {
		$boolIsValid = true;

		// Validation example
		if( true == is_null( $this->getMaintenancePriorityId() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_priority_id', __( 'Priority of template is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceLocationId() {
		$boolIsValid = true;

		// Validation example
		if( true == is_null( $this->getMaintenanceLocationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_location_id', __( 'Location of template is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceStatusId() {
		$boolIsValid = true;

		// Validation example
		if( true == is_null( $this->getMaintenanceStatusId() ) && CMaintenanceRequestType::RECURRING == $this->getMaintenanceRequestTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_status_id', __( 'Status of template is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceProblemId() {
		$boolIsValid = true;

		// Validation example
		if( true == is_null( $this->getMaintenanceProblemId() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maintenance_problem_id', __( 'Problem is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTitle( $objDatabase ) {
		$boolIsValid = true;

		// Validation example
		if( true == is_null( $this->getTitle() ) ) {
		    $boolIsValid = false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Name is required. ' ) ) );
		}

		if( false == is_null( $this->getTitle() ) && 50 < \Psi\CStringService::singleton()->strlen( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Name should not exceed {%d, 0, nots} characters. ', [ 50 ] ) ) );
		}

		$arrmixMaintenanceTemplates = CMaintenanceTemplates::createService()->fetchMaintenanceTemplatesByNameByMaintenanceRequestTypeIdByCid( $this->getTitle(), $this->getMaintenanceRequestTypeId(), $this->getId(), $this->getCid(), $objDatabase );

		if( true == valArr( $arrmixMaintenanceTemplates ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Name \' {%s, 0} \' already exists. ', [ $this->getTitle() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valMaintenanceRequestTypeId() {
		$boolIsValid = true;

		// Validation example
		if( true == is_null( $this->getMaintenanceRequestTypeId() ) || false == is_numeric( $this->getMaintenanceRequestTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_to_complete', __( 'Maintenance request type is required.' ) ) );
		}

 		return $boolIsValid;
	}

	public function valDaysToComplete( $boolIsMakeReadyTemplate = false, $boolIsFeatureUpgradeTemplate = false, $objDatabase ) {
		$boolIsValid = true;

		// Validation example
		if( true == $boolIsMakeReadyTemplate ) {
			if( true == is_null( $this->getDaysToComplete() ) && true == is_null( $this->getParentMaintenanceTemplateId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_to_complete', __( 'Days to complete is required.' ) ) );
			} elseif( false == valId( $this->getDaysToComplete() ) && false == is_numeric( $this->getDaysToComplete() ) && true == is_null( $this->getParentMaintenanceTemplateId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_to_complete', __( 'Days to complete is invalid. ' ) ) );
			}

			if( false == is_null( $this->getParentMaintenanceTemplateId() ) && true == is_numeric( $this->getParentMaintenanceTemplateId() ) ) {
				$objMaintenanceTemplate = CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidById( $this->getCid(), $this->getParentMaintenanceTemplateId(), $objDatabase );
				if( valObj( $objMaintenanceTemplate, 'CMaintenanceTemplate' ) ) {
					if( $objMaintenanceTemplate->getDaysToComplete() < $this->getDaysToComplete() ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_to_complete', __( 'Days to complete for subtask should be less than or equal to days to complete for parent task.' ) ) );
					}
				}
			}
		} elseif( true == $boolIsFeatureUpgradeTemplate ) {
			if( true == is_null( $this->getDaysToComplete() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_to_complete', __( 'Days to complete is required.' ) ) );
			} elseif( false == is_numeric( $this->getDaysToComplete() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_to_complete', __( 'Days to complete is invalid. ' ) ) );
			}

			if( false == is_null( $this->getParentMaintenanceTemplateId() ) && true == is_numeric( $this->getParentMaintenanceTemplateId() ) ) {
				$objMaintenanceTemplate = CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidById( $this->getCid(), $this->getParentMaintenanceTemplateId(), $objDatabase );
				if( valObj( $objMaintenanceTemplate, 'CMaintenanceTemplate' ) ) {
					if( $objMaintenanceTemplate->getDaysToComplete() < $this->getDaysToComplete() ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_to_complete', __( 'Days to complete for subtask should be less than or equal to days to complete for parent task.' ) ) );
					}
				}
			}
		} else {
			if( false == is_null( $this->getParentMaintenanceTemplateId() ) && true == is_numeric( $this->getParentMaintenanceTemplateId() ) ) {
				$objMaintenanceTemplate = CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidById( $this->getCid(), $this->getParentMaintenanceTemplateId(), $objDatabase );
				if( valObj( $objMaintenanceTemplate, 'CMaintenanceTemplate' ) ) {
					if( $objMaintenanceTemplate->getDaysToComplete() < $this->getDaysToComplete() ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'days_to_complete', __( 'Days to complete for subtask should be less than or equal to days to complete for parent task.' ) ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valDueInterval( $objDatabase ) {
		$boolIsValid = true;

		// validate if the due interval and due interval both are inserted
		if( ( true == valId( $this->getDueInterval() ) && false == valId( $this->getDueIntervalTypeId() ) ) || ( false == valId( $this->getDueInterval() ) && true == valId( $this->getDueIntervalTypeId() ) ) || ( 1000 <= $this->getDueInterval() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_interval', __( 'Due Time for subtask should be numeric, less than {%d, 0, nots} and interval should be selected.', [ 1000 ] ) ) );
			return $boolIsValid;
		}

		// Validate Due Interval and Due Interval type for sub-tasks
		if( false == is_null( $this->getParentMaintenanceTemplateId() ) && true == is_numeric( $this->getParentMaintenanceTemplateId() ) ) {
			$objMaintenanceTemplate = CMaintenanceTemplates::createService()->fetchMaintenanceTemplateByCidById( $this->getCid(), $this->getParentMaintenanceTemplateId(), $objDatabase );
			if( valObj( $objMaintenanceTemplate, 'CMaintenanceTemplate' ) ) {
				// get the due interval and due interval type from parent template, convert it into hours and compare with the sub task due interval and due interval type
				$intParentDueIntervalInHours = ( CIntervalType::DAYS == $objMaintenanceTemplate->getDueIntervalTypeId() ) ? ( $objMaintenanceTemplate->getDueInterval() * 24 ) : $objMaintenanceTemplate->getDueInterval();
				$intSubTaskDueIntervalInHours = ( CIntervalType::DAYS == $this->getDueIntervalTypeId() ) ? ( $this->getDueInterval() * 24 ) : $this->getDueInterval();

				if( $intParentDueIntervalInHours < $intSubTaskDueIntervalInHours ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_interval', __( 'Due Time for subtask should be less than or equal to Due Time for parent task.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valEstimatedHours() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valUpdatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valUpdatedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedBy() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCreatedOn() {
		$boolIsValid = true;
	}

	public function valIsMoveOutDateAsStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeIds() {
		$boolIsValid = true;
		if( true == is_null( $this->getOccupancyTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupancy_types', __( 'Please select at least one occupancy type.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $boolIsForSubTasks = false, $boolIsMakeReadyTemplate = false, $boolIsFeatureUpgradeTemplate = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( false == $boolIsForSubTasks ) {
					$boolIsValid &= $this->valMaintenanceRequestTypeId();
					$boolIsValid &= $this->valTitle( $objDatabase );
					$boolIsValid &= $this->valDescription();
					$boolIsValid &= $this->valMaintenanceStatusId();
					if( true == $boolIsMakeReadyTemplate ) {
						$boolIsValid &= $this->valOccupancyTypeIds();
						$boolIsValid &= $this->valDaysToComplete( $boolIsMakeReadyTemplate, $boolIsFeatureUpgradeTemplate, $objDatabase );
					} else {
						$boolIsValid &= $this->valMaintenanceLocationId();
					}
				} else {
					$boolIsValid &= $this->valMaintenanceProblemId();
					$boolIsValid &= $this->valMaintenanceLocationId();
					if( true == $boolIsMakeReadyTemplate ) {
						$boolIsValid &= $this->valDaysToComplete( $boolIsMakeReadyTemplate, $boolIsFeatureUpgradeTemplate, $objDatabase );
					}
				}
				break;

			case VALIDATE_UPDATE:
				if( false == $boolIsForSubTasks ) {
					$boolIsValid &= $this->valMaintenanceRequestTypeId();
					$boolIsValid &= $this->valTitle( $objDatabase );
					$boolIsValid &= $this->valDescription();

					if( false == $boolIsMakeReadyTemplate ) {
						$boolIsValid &= $this->valMaintenanceLocationId();
					} else {
						$boolIsValid &= $this->valOccupancyTypeIds();
					}

					$boolIsValid &= $this->valMaintenanceStatusId();
				} else {
					$boolIsValid &= $this->valMaintenanceProblemId();
					$boolIsValid &= $this->valMaintenanceLocationId();
					if( true == $boolIsMakeReadyTemplate ) {
						$boolIsValid &= $this->valDaysToComplete( $boolIsMakeReadyTemplate, $boolIsFeatureUpgradeTemplate, $objDatabase );
					}
				}

				if( true == $boolIsMakeReadyTemplate ) {
					$boolIsValid &= $this->valDaysToComplete( $boolIsMakeReadyTemplate, $boolIsFeatureUpgradeTemplate, $objDatabase );
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId( $objDatabase );
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['maintenance_request_type_name'] ) ) $this->setMaintenanceRequestTypeName( $arrmixValues['maintenance_request_type_name'] );

	}

	public function disassociateMaintenanceRequests( $intCompanyUserId, CDatabase $objDatabase ) {
		$strSql = 'UPDATE maintenance_requests SET maintenance_template_id = NULL, updated_by = ' . ( int ) $intCompanyUserId . ', updated_on = NOW() WHERE cid = ' . ( int ) $this->getCid() . ' AND maintenance_template_id = ' . ( int ) $this->getId() . ';';

		$objDataset = $objDatabase->createDataset();
		if( ( false == $objDataset->execute( $strSql ) ) ) {
			$objDataset->cleanup();

			return false;
		}
		$objDataset->cleanup();

		return true;
	}

}

?>