<?php

class CMaintenanceProblemType extends CBaseMaintenanceProblemType {

	const MAINTENANCE_PROBLEM			= 1;
	const PROBLEM_CATEGORY				= 2;
	const INSPECTION_ACTION_REPAIR		= 4;
	const INSPECTION_ACTION_REPLACE		= 5;
	const INSPECTION_ACTION_CLEAN		= 6;

	public static $c_arrintLeasingCenterMaintenanceProblemType = [
		self::MAINTENANCE_PROBLEM
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>