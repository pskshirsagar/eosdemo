<?php

class CApplicantApplicationTransmission extends CBaseApplicantApplicationTransmission {

	protected $m_intCustomerTypeId;
	protected $m_intCustomerId;
	protected $m_intLeaseCustomerId;
	protected $m_intFirstAdvantageRecommendationTypeId;

	protected $m_strResidentVerifyTransmissionStatusName;
	protected $m_strApplicantFirstName;
	protected $m_strApplicantLastName;
	protected $m_strFirstAdvantageApplicationDecision;
	protected $m_strTazWorkTransmissionStatusName;

	/**
	 * Get Functions
	 */

    public function getFirstAdvantageRecommendationTypeId() {
    	return $this->m_intFirstAdvantageRecommendationTypeId;
    }

    public function getFirstAdvantageApplicationDecision() {
    	return $this->m_strFirstAdvantageApplicationDecision;
    }

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function getResidentVerifyTransmissionStatusName() {
		return $this->m_strResidentVerifyTransmissionStatusName;
	}

	public function getTazWorkTransmissionStatusName() {
		return $this->m_strTazWorkTransmissionStatusName;
	}

	public function getApplicantFirstName() {
		return $this->m_strApplicantFirstName;
	}

	public function getApplicantLastName() {
		return $this->m_strApplicantLastName;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	/**
	 * Set Functions
	 */

	public function setFirstAdvantageRecommendationTypeId( $intFirstAdvantageRecommendationTypeId ) {
		$this->m_intFirstAdvantageRecommendationTypeId = $intFirstAdvantageRecommendationTypeId;
	}

	public function setFirstAdvantageApplicationDecision( $strApplicationDecision ) {
		$this->m_strFirstAdvantageApplicationDecision = $strApplicationDecision;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->m_intCustomerTypeId = $intCustomerTypeId;
	}

	public function setApplicantFirstName( $strApplicantFirstName ) {
		$this->m_strApplicantFirstName = $strApplicantFirstName;
	}

	public function setApplicantLastName( $strApplicantLastName ) {
		$this->m_strApplicantLastName = $strApplicantLastName;
	}

	public function setResidentVerifyDecisionStatusName( $intScreeningDecisionTypeId ) {

		switch( $intScreeningDecisionTypeId ) {
			case CScreeningDecisionType::APPROVE:
				$strScreeningDecisionName = 'Approved';
				break;

			case CScreeningDecisionType::DENIED:
				$strScreeningDecisionName = 'Denied';
				break;

			case CScreeningDecisionType::APPROVE_WITH_CONDITIONS:
				$strScreeningDecisionName = 'Approve With Conditions';
				break;

			default:
				$strScreeningDecisionName = '';
		}

		$this->setResidentVerifyTransmissionStatusName( $strScreeningDecisionName );

	}

	public function setResidentVerifyRecommendationStatusName( $intRecommendationTypeId, $boolIsRecommendationTypeId = false ) {

		if( true == $boolIsRecommendationTypeId ) {
			switch( $intRecommendationTypeId ) {

				case CScreeningRecommendationType::PENDING_UNKNOWN:
					$strPrimaryCoapplicantResponseStatus = __( 'In Progress' );
					break;

				case CScreeningRecommendationType::FAIL:
					$strPrimaryCoapplicantResponseStatus = __( 'Fail' );
					break;

				case CScreeningRecommendationType::PASS:
					$strPrimaryCoapplicantResponseStatus = __( 'Pass' );
					break;

				case CScreeningRecommendationType::PASSWITHCONDITIONS:
					$strPrimaryCoapplicantResponseStatus = __( 'Pass With Conditions' );
					break;

				case CScreeningRecommendationType::ERROR:
					$strPrimaryCoapplicantResponseStatus = __( 'Error - Contact Support' );
					break;

				default:
					$strPrimaryCoapplicantResponseStatus = '';
					break;
			}
		} else {
			switch( $intRecommendationTypeId ) {
				case CTransmissionResponseType::INSUFFICIENT_INFO:
					$strPrimaryCoapplicantResponseStatus = __( 'Error - Contact Support' );
					break;

				case CTransmissionResponseType::IN_PROGRESS:
					$strPrimaryCoapplicantResponseStatus = __( 'In Progress' );
					break;

				case CTransmissionResponseType::DENIED:
					$strPrimaryCoapplicantResponseStatus = __( 'Fail' );
					break;

				case CTransmissionResponseType::APPROVED:
					$strPrimaryCoapplicantResponseStatus = __( 'Pass' );
					break;

				case CTransmissionResponseType::CONDITIONALLY_APPROVED:
					$strPrimaryCoapplicantResponseStatus = __( 'Pass With Conditions' );
					break;

				default:
					$strPrimaryCoapplicantResponseStatus = '';
					break;
			}
		}

		$this->setResidentVerifyTransmissionStatusName( $strPrimaryCoapplicantResponseStatus );
	}

	public function setTazworkRecommendationStatusName( $intRecommendationTypeId ) {

			switch( $intRecommendationTypeId ) {
				case CTransmissionResponseType::INSUFFICIENT_INFO:
					$strPrimaryCoapplicantResponseStatus = __( 'Error' );
					break;

				case CTransmissionResponseType::FAILURE:
					$strPrimaryCoapplicantResponseStatus = __( 'Fail' );
					break;

				case CTransmissionResponseType::IN_PROGRESS:
					$strPrimaryCoapplicantResponseStatus = __( 'In Progress' );
					break;

				case CTransmissionResponseType::DENIED:
					$strPrimaryCoapplicantResponseStatus = __( 'Denied' );
					break;

				case CTransmissionResponseType::SUCCESSFUL:
					$strPrimaryCoapplicantResponseStatus = __( 'Pass' );
					break;

				case CTransmissionResponseType::CONDITIONALLY_APPROVED:
					$strPrimaryCoapplicantResponseStatus = __( 'Conditionally Approved' );
					break;

				default:
					$strPrimaryCoapplicantResponseStatus = '';
					break;
			}

		$this->setTazWorkTransmissionStatusName( $strPrimaryCoapplicantResponseStatus );
	}

	public function setTazWorkTransmissionStatusName( $strPrimaryCoapplicantResponseStatus ) {
		$this->m_strTazWorkTransmissionStatusName = $strPrimaryCoapplicantResponseStatus;
	}

	public function setResidentVerifyTransmissionStatusName( $strPrimaryCoapplicantResponseStatus ) {
		$this->m_strResidentVerifyTransmissionStatusName = $strPrimaryCoapplicantResponseStatus;
	}

	public function setFirstAdvantageApplicationDecisionName( $strApplicationDecisionResponse ) {

		$strApplicationDecisionResponse = explode( '~..~', $strApplicationDecisionResponse );
		$strApplicationDecision			= ( true == valArr( $strApplicationDecisionResponse ) && true == array_key_exists( 1, $strApplicationDecisionResponse ) ) ? $strApplicationDecisionResponse[1] : '';

		$strApplicationDecisionName = '';
		$intRecommendationTypeId	= NULL;

		switch( trim( $strApplicationDecision ) ) {
			case 'APPROVED':
			case 'Accepted':
				$strApplicationDecisionName = 'Approved';
				$intRecommendationTypeId 	= CTransmissionResponseType::APPROVED;
				break;

			case 'APPRVD W/COND':
			case 'APPROVED WITH CONDITIONS':
			case 'Conditional':
				$strApplicationDecisionName = 'Approved W/Cond';
				$intRecommendationTypeId 	= CTransmissionResponseType::CONDITIONALLY_APPROVED;
				break;

			case 'DECLINED':
			case 'Denied':
				$strApplicationDecisionName = 'Denied';
				$intRecommendationTypeId 	= CTransmissionResponseType::DENIED;
				break;

			case 'Pending':
				$strApplicationDecisionName = 'In Progress';
				$intRecommendationTypeId 	= CTransmissionResponseType::IN_PROGRESS;
				break;

			case 'Guarantor Required':
				$strApplicationDecisionName = 'Guarantor Required';
				$intRecommendationTypeId 	= CTransmissionResponseType::GUARANTOR_REQUIRED;
				break;

			case 'FURTHER REVIEW':
			case 'Further Review':
				$strApplicationDecisionName = 'Further Review';
				$intRecommendationTypeId 	= CTransmissionResponseType::FURTHER_REVIEW;
				break;

			default:
				$strApplicationDecisionName = 'Insufficient Info';
				$intRecommendationTypeId 	= CTransmissionResponseType::INSUFFICIENT_INFO;
				break;
		}

		$this->setFirstAdvantageApplicationDecision( $strApplicationDecisionName );
		$this->setFirstAdvantageRecommendationTypeId( $intRecommendationTypeId );

	}

    /**
     * Validate Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApplicantApplicationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTransmissionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTransmissionVendorId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyTransmissionVendorId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCreditTransmissionResponseTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCriminalTransmissionResponseTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIdentifyTransmissionResponseTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTransmissionResponseTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTransmissionRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTransmissionDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseScore() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valResponse() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHasCriminalFlag() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHasCreditAlert() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valHasDataError() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsFailed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCriminalResponseOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valAdversePrintedOn() {
        $boolIsValid = true;
        return $boolIsValid;
	}

	public function valAdverseEmailedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApprovedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApprovedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
        		break;

            default:
            	$boolIsValid = true;
            	break;
        }
        return $boolIsValid;
    }

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['customer_type_id'] ) ) $this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
		if( true == isset( $arrmixValues['transmission_response_type_id'] ) ) $this->setResidentVerifyRecommendationStatusName( $arrmixValues['transmission_response_type_id'] );
		if( true == isset( $arrmixValues['name_first'] ) )	$this->setApplicantFirstName( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) )	$this->setApplicantLastName( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
		if( true == isset( $arrmixValues['response'] ) ) $this->setFirstAdvantageApplicationDecisionName( $arrmixValues['response'] );
		if( true == isset( $arrmixValues['transmission_response_type_id'] ) ) $this->setTazworkRecommendationStatusName( $arrmixValues['transmission_response_type_id'] );
		return;
	}

    /**
     * Fetch Functions
     */

    public function fetchTransmission( $objDatabase ) {
    	return CTransmissions::fetchTransmissionByIdByCid( $this->getTransmissionId(), $this->getCid(), $objDatabase );
    }

    public function fetchApplicantApplicationTransmissions( $objDatabase ) {
		return CApplicantApplicationTransmissions::fetchSimpleApplicantApplicationTransmissionsByTransmissionIdByCid( $this->getTransmissionId(), $this->getCid(), $objDatabase );
    }

}
?>