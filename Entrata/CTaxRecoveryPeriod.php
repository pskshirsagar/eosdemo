<?php

class CTaxRecoveryPeriod extends CBaseTaxRecoveryPeriod {

	const YEARS_3_TAX_RECOVERY_PERIOD_ID	        = 1;
	const YEARS_5_TAX_RECOVERY_PERIOD_ID	        = 2;
	const YEARS_7_TAX_RECOVERY_PERIOD_ID	        = 3;
	const YEARS_10_TAX_RECOVERY_PERIOD_ID	        = 4;
	const YEARS_15_TAX_RECOVERY_PERIOD_ID	        = 5;
	const YEARS_20_TAX_RECOVERY_PERIOD_ID	        = 6;
	const YEARS_27_TAX_RECOVERY_PERIOD_ID	        = 7;
	const YEARS_39_TAX_RECOVERY_PERIOD_ID	        = 8;
	const NON_DEPRECIATION_TAX_RECOVERY_PERIOD_ID	= 9;

	public static function getTaxRecoveryPeriodName( $intTaxRecoveryPeriodId ) {

		$strTaxRecoveryPeriodIdName = '';

		switch( $intTaxRecoveryPeriodId ) {

			case self::YEARS_3_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = '3 Years';
				break;

			case self::YEARS_5_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = '5 Years';
				break;

			case self::YEARS_7_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = '7 Years';
				break;

			case self::YEARS_10_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = '10 Years';
				break;

			case self::YEARS_15_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = '15 Years';
				break;

			case self::YEARS_20_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = '20 Years';
				break;

			case self::YEARS_27_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = '27.5 Years';
				break;

			case self::YEARS_39_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = '39 Years';
				break;

			case self::NON_DEPRECIATION_TAX_RECOVERY_PERIOD_ID:
				$strTaxRecoveryPeriodIdName = 'Non-Depreciable';
				break;

			default:
		}
		return $strTaxRecoveryPeriodIdName;
	}

}
?>