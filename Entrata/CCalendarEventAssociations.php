<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCalendarEventAssociations
 * Do not add any new functions to this class.
 */

class CCalendarEventAssociations extends CBaseCalendarEventAssociations {

	public static function fetchCalendarEventAssociationsByCalendarEventIdsByCid( $arrintCalendarEventIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCalendarEventIds ) ) return false;

		$strChildSql = \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->getCompanyUserPreferenceSql( 'ENABLE_ENTRATA_CALENDAR', $intCid );

		$strSql = 'with temp_company_preference AS( ' . $strChildSql . ' )
														SELECT
															cea.*,
															CASE
																WHEN cu.id = cea.company_user_id THEN cem.name_first || \' \' || cem.name_last
																WHEN cea.property_id = p.id THEN p.property_name
															END AS name
														FROM
															calendar_event_associations cea
															LEFT JOIN company_users cu ON ( cu.id = cea.company_user_id AND cu.cid = cea.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
															LEFT JOIN company_employees cem ON ( cu.company_employee_id = cem.id AND cu.cid = cem.cid )
															LEFT JOIN properties p ON cea.cid = p.cid AND ( cea.property_id = p.id )
															LEFT JOIN temp_company_preference tcp ON tcp.cid = cea.cid AND tcp.company_user_id = cea.company_user_id AND tcp.value=\'1\'
														WHERE
															cea.calendar_event_id IN ( ' . implode( ', ', $arrintCalendarEventIds ) . ' )
															AND cea.cid = ' . ( int ) $intCid . '
															AND ( cea.company_user_id IS NULL OR tcp.company_user_id IS NOT NULL )';

		return self::fetchCalendarEventAssociations( $strSql, $objDatabase );
	}

	public static function fetchCalendarEventAssociationsByCalendarEventIds( $arrintCalendarEventIds, $objDatabase ) {

		if( false == valArr( $arrintCalendarEventIds ) ) return false;

		$strChildSql = \Psi\Eos\Entrata\CCompanyUserPreferences::createService()->getCompanyUserPreferenceSql( 'ENABLE_ENTRATA_CALENDAR', '' );

		$strSql = 'with temp_company_preference AS( ' . $strChildSql . ' )  
														SELECT
															cea.*,
															CASE
																WHEN cu.id = cea.company_user_id THEN cem.name_first || \' \' || cem.name_last
																WHEN cea.property_id = p.id THEN p.property_name
															END AS name
														FROM
															calendar_event_associations cea
															LEFT JOIN company_users cu ON ( cu.id = cea.company_user_id AND cu.cid = cea.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
															LEFT JOIN company_employees cem ON ( cu.company_employee_id = cem.id AND cu.cid = cem.cid )
															LEFT JOIN properties p ON cea.cid = p.cid AND ( cea.property_id = p.id )
															LEFT JOIN temp_company_preference tcp ON tcp.cid = cea.cid AND tcp.company_user_id = cea.company_user_id AND tcp.value=\'1\'
														WHERE
															cea.calendar_event_id IN ( ' . implode( ', ', $arrintCalendarEventIds ) . ' )
															AND ( cea.company_user_id IS NULL OR tcp.company_user_id IS NOT NULL )';

		return self::fetchCalendarEventAssociations( $strSql, $objDatabase );
	}

}
?>