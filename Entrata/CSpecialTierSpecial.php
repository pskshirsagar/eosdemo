<?php

class CSpecialTierSpecial extends CBaseSpecialTierSpecial {

	protected $m_strName;
	protected $m_intSpecialTypeId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTierSpecialId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			return $this->update( $intCurrentUserId, $objDatabase, false, false );
		} else {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}
	}

	/**
	 * Get Functions
	 *
	 */
	public function getName() {
		return $this->m_strName;
	}

	public function getSpecialTypeId() {
		return $this->m_intSpecialTypeId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setSpecialTypeId( $intSpecialTypeId ) {
		$this->m_intSpecialTypeId = $intSpecialTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name'] ) ) {
			$this->setName( $arrmixValues['name'] );
		}

		if( true == isset( $arrmixValues['special_type_id'] ) ) {
			$this->setSpecialTypeId( $arrmixValues['special_type_id'] );
		}
		return;
	}

}
?>