<?php

class CLeaseAction extends CBaseLeaseAction {

    const APPLICATION_RECEIVED 				= 1;
    const APPLICATION_ACCEPTED 				= 2;
    const LEASE_SIGN	 					= 3;
    const LEASE_START		 				= 4;
    const LEASE_END	 						= 5;
    const NOTICE	 						= 6;
    const MOVE_IN		 					= 7;
    const MOVE_OUT		 					= 8;
    const EVICTION		 					= 9;
    const TRANSFER		 					= 10;
    const RENEWAL			 				= 11;
    const FINANCIAL_MOVE_OUT				= 12;
	const REVERSE_FINANCIAL_MOVE_OUT		= 13;
	const REVERSE_MOVE_IN					= 14;
	const ISSUE_REFUND						= 15;

    public static function getActionId( $strEvent ) {
		$intActionId = NULL;

    	switch( $strEvent ) {

    		case 'application_received':
				$intActionId = CLeaseAction::APPLICATION_RECEIVED;
				break;

    		case 'application_accepted':
				$intActionId = CLeaseAction::APPLICATION_ACCEPTED;
				break;

    		case 'lease_sign':
				$intActionId = CLeaseAction::LEASE_SIGN;
				break;

			case 'lease_start':
				$intActionId = CLeaseAction::LEASE_START;
				break;

			case 'lease_end':
				$intActionId = CLeaseAction::LEASE_END;
				break;

			case 'notice':
				$intActionId = CLeaseAction::NOTICE;
				break;

			case 'move_in':
				$intActionId = CLeaseAction::MOVE_IN;
				break;

			case 'move_out':
				$intActionId = CLeaseAction::MOVE_OUT;
				break;

			case 'eviction':
				$intActionId = CLeaseAction::EVICTION;
				break;

			case 'transfer':
				$intActionId = CLeaseAction::TRANSFER;
				break;

		    case 'renewal':
			    $intActionId = CLeaseAction::RENEWAL;
			    break;

		    case 'issue_refund':
			    $intActionId = CLeaseAction::ISSUE_REFUND;
			    break;

			default:
				// default case
				break;
		}
		return $intActionId;
    }

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getName() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getDescription() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', '' ) );
        // }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getIsPublished() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', '' ) );
        // }

        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;

       // Validation example

        // if( true == is_null( $this->getOrderNum() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>