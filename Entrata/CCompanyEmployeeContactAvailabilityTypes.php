<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeContactAvailabilityTypes
 * Do not add any new functions to this class.
 */

class CCompanyEmployeeContactAvailabilityTypes extends CBaseCompanyEmployeeContactAvailabilityTypes {

	public static function fetchCompanyEmployeeContactAvailabilityTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCompanyEmployeeContactAvailabilityType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCompanyEmployeeContactAvailabilityType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCompanyEmployeeContactAvailabilityType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllCompanyEmployeeContactAvailabilityTypes( $objDatabase ) {
		$strSql = 'SELECT *	FROM company_employee_contact_availability_types WHERE is_published = true';
		return parent::fetchObjects( $strSql, 'CCompanyEmployeeContactAvailabilityType', $objDatabase );
	}
}
?>