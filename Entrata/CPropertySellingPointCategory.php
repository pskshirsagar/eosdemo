<?php

class CPropertySellingPointCategory extends CBasePropertySellingPointCategory {

	const FEATURED = 2;

	protected static $c_arrstrSellingPointCategoryIconClass = [
		1	=> 'location',
		2	=> 'featured',
		3	=> 'school',
		4	=> 'upgrade',
		5	=> 'selling-point'
	];

	public static function getSellingPointCategoryIconClass() {
		return self::$c_arrstrSellingPointCategoryIconClass;
	}

	public static function assignSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'selling_point_category_icon_class', self::$c_arrstrSellingPointCategoryIconClass );
	}

}
?>