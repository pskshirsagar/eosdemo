<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultLeaseTerms
 * Do not add any new functions to this class.
 */

class CDefaultLeaseTerms extends CBaseDefaultLeaseTerms {

	public static function fetchDefaultGroupLeaseTermByIdByPropertyIdByCid( $intId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intId ) ) return NULL;

		$strSql = 'SELECT
							dlt.*,
							pcs.lease_term_structure_id,
							pcs.lease_start_structure_id
						FROM
							default_lease_terms dlt, property_charge_settings pcs
						WHERE
							pcs.cid = ' . ( int ) $intCid . '
							AND pcs.property_id = ' . ( int ) $intPropertyId . '
							AND dlt.deleted_on IS NULL
							AND dlt.id = ' . ( int ) $intId;

		$arrmixData = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrmixData ) ? $arrmixData[0] : NULL );
	}

	public static function fetchActiveDefaultGroupLeaseTermById( $intId, $objDatabase ) {

		$strSql = 'SELECT
						dlt.*
					FROM
						default_lease_terms dlt
					WHERE
						dlt.id = ' . ( int ) $intId . '
						AND dlt.deleted_on IS NULL';

		return parent::fetchDefaultLeaseTerm( $strSql, $objDatabase );
	}

}
?>