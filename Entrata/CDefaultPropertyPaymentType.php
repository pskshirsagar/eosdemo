<?php

class CDefaultPropertyPaymentType extends CBaseDefaultPropertyPaymentType {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowInEntrata() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowInResidentPortal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowInProspectPortal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCertifiedFunds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAutoAdded() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>