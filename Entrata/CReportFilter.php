<?php

class CReportFilter extends CBaseReportFilter {

	protected $m_intReportId;
	protected $m_intReportGroupId;
	protected $m_intReportVersionId;
	protected $m_arrmixDownloadOptions;
	protected $m_strReportName;
	protected $m_arrmixFilterData;
	protected $m_intReportTypeId;

	/*
	 * Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intReportId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_id', 'Report id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objCompanyUserId, $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		if( 0 < \Psi\Eos\Entrata\CReportFilters::createService()->fetchRowCount( ' WHERE cid = ' . $this->getCid() . ' AND created_by =' . $objCompanyUserId . 'AND name=\'' . addslashes( $this->getName() ) . '\' AND deleted_by IS NULL AND report_id = ' . $this->getReportId() . ( 0 < $this->getId() ? ' AND id <> ' . $this->getId() : '' ), 'report_filters', $objDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_filters', 'Filter Name ' . addslashes( $this->getName() ) . ' already exists. ' ) );
		}

		return $boolIsValid;
	}

	public function valFilters() {
		$boolIsValid = true;

		if( false == isset( $this->m_strFilters ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filters', 'Filters are required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSharedReportFilter( $intDefaultFilterId, $intCid, $objDatabase ) {
		$boolIsValid = true;

		$intDefaultFilterCount = \Psi\Libraries\UtilFunctions\count( \Psi\Eos\Entrata\CReportInstances::createService()->fetchReportInstancesByDefaultFilterIdByCid( $intDefaultFilterId, $intCid, $objDatabase ) );

		if( 0 != $intDefaultFilterCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filters', 'Filter is currently being used as default filter for report instance. Please disassociate filter and then shared.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objCompanyUserId, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objCompanyUserId, $objDatabase );
				$boolIsValid &= $this->valFilters();
				$boolIsValid &= $this->valReportId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFilters();
				$boolIsValid &= $this->valReportId();
				break;

			case VALIDATE_SHARED_FILTER_UPDATE:
				$boolIsValid &= $this->valFilters();
				$boolIsValid &= $this->valReportId();
				$boolIsValid &= $this->valName( $objCompanyUserId, $objDatabase );
				$boolIsValid &= $this->valSharedReportFilter( $this->getId(), $this->getCid(), $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Getters
	 */

	public function getReportId() {
		return $this->m_intReportId;
	}

	public function getReportGroupId() {
		return $this->m_intReportGroupId;
	}

	public function getReportVersionId() {
		return $this->m_intReportVersionId;
	}

	public function getDownloadOptions() {
		return $this->m_arrmixDownloadOptions;
	}

	public function getReportName() {
		return $this->m_strReportName;
	}

	public function getFilterData() {
		return $this->m_arrmixFilterData;
	}

	public function getReportFilterId() {
		return $this->m_intReportFilterId;
	}

	public function getReportTypeId() {
		return $this->m_intReportTypeId;
	}

	/*
	 * Setters
	 */

	public function setReportId( $intReportId ) {
		$this->m_intReportId = ( int ) $intReportId;
	}

	public function setReportGroupId( $intReportGroupId ) {
		$this->m_intReportGroupId = ( int ) $intReportGroupId;
	}

	public function setReportVersionId( $intReportVersionId ) {
		$this->m_intReportVersionId = ( int ) $intReportVersionId;
	}

	public function setDownloadOptions( $arrmixDownloadOptions ) {
		$this->m_arrmixDownloadOptions = $arrmixDownloadOptions;
	}

	public function setReportName( $strReportName ) {
		$this->m_strReportName = $strReportName;
	}

	public function setFilterValue( $strFilterKey, $mixFilterValue ) {
		$this->m_arrmixFilterData['report_filter'][$strFilterKey] = $mixFilterValue;
		$this->setFilters( json_encode( $this->m_arrmixFilterData ) );
	}

	public function setFilters( $strFilters ) {
		parent::setFilters( $strFilters );

		// Parse filters into filter data
		$this->parseFilterData();
	}

	public function setReportTypeId( $intReportTypeId ) {
		$this->m_intReportTypeId = ( int ) $intReportTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		// We need to handle filters before the parent method, so that escape sequences are not removed from the JSON
		if( isset( $arrmixValues['filters'] ) ) {
			if( true == $boolDirectSet ) {
				$this->m_strFilters = trim( $arrmixValues['filters'] );
			} else {
				$this->setFilters( $arrmixValues['filters'] );
			}
			unset( $arrmixValues['filters'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['report_id'] ) )			$this->setReportId( $arrmixValues['report_id'] );
		if( true == isset( $arrmixValues['report_group_id'] ) )		$this->setReportGroupId( $arrmixValues['report_group_id'] );
		if( true == isset( $arrmixValues['report_version_id'] ) )	$this->setReportVersionId( $arrmixValues['report_version_id'] );
		if( true == isset( $arrmixValues['report_name'] ) )			$this->setReportName( $arrmixValues['report_name'] );
		if( true == isset( $arrmixValues['report_type_id'] ) )		$this->setReportTypeId( $arrmixValues['report_type_id'] );
	}

	/*
	 * Other Functions
	 */

	protected function parseFilterData() {
		$this->m_arrmixFilterData = json_decode( $this->getFilters(), true );
	}

	public function applyFilters( $strOutputType = NULL ) {
		// Set request data to match filters
		$_REQUEST['report_filter'] = $this->m_arrmixFilterData['report_filter'];

		if( true == isset( $this->m_arrmixFilterData['display_options'] ) ) {
			$_REQUEST['display_options'] = $this->m_arrmixFilterData['display_options'];
		}

		if( true == isset( $this->m_arrmixDownloadOptions[$strOutputType] ) ) {
			$_REQUEST['download_options'] = $this->m_arrmixDownloadOptions[$strOutputType];
		}
	}

	public function overrideFilterValues( $arrmixFilterOverrides ) {
		if( true == valArr( $arrmixFilterOverrides ) ) {
			foreach( $arrmixFilterOverrides as $strKey => $arrmixValues ) {
				$this->setFilterValue( $strKey, $arrmixValues );
			}
		}
	}

	public function createReportHistory( CReportSchedule $objReportSchedule, DateTime $objToday, CDatabase $objDatabase, CCompanyUser $objCompanyUser ) {

		$objReportGroup = $objReportSchedule->getOrFetchReportGroup( $objDatabase );
		$intReportGroupId = ( true == valObj( $objReportGroup, 'CReportGroup' ) ) ? $objReportGroup->getId() : NULL;

		$objReportHistory = new CReportHistory();
		$objReportHistory->setCid( $this->getCid() );
		$objReportHistory->setReportId( $this->getReportId() );
		$objReportHistory->setReportVersionId( $this->getReportVersionId() >= 0 ? $this->getReportVersionId() : NULL );
		$objReportHistory->setReportScheduleTypeId( $objReportSchedule->getReportScheduleTypeId() );
		$objReportHistory->setReportFilterId( $this->getId() >= 0 ? $this->getId() : NULL );
		$objReportHistory->setReportScheduleId( $objReportSchedule->getId() );
		$objReportHistory->setReportGroupId( $intReportGroupId );
		$objReportHistory->setFilters( $this->getFilters() );
		$objReportHistory->setDownloadOptions( json_encode( $this->getDownloadOptions() ) );
		$objReportHistory->setRecipients( $objReportSchedule->fetchRecipientCache( $objDatabase, $objCompanyUser ) ?: '{}' );
		$objReportHistory->generateReportHistoryName( $this, $objReportGroup->getName(), $objToday );
		$objReportHistory->setCreatedOn( $objToday->format( 'c' ) );

		return $objReportHistory;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( date( 'm/d/Y H:i:s' ) );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function toReportInstance() {
		$objReportInstance = new CReportNewInstance();

		$objReportInstance->setValues( [
			'id'				=> -abs( $this->getId() ),
			'cid'				=> $this->getCid(),
			'report_filter_id'	=> $this->getId(),
			'report_id'			=> $this->getReportId(),
			'report_name'		=> $this->getReportName(),
			'report_version_id'	=> $this->getReportVersionId(),
			'module_id'			=> NULL,
			'name'				=> $this->getName(),
			'description'		=> $this->getName(),
			'filters'			=> $this->getFilters(),
			'deleted_by'		=> $this->getDeletedBy(),
			'deleted_on'		=> $this->getDeletedOn(),
			'updated_by'		=> $this->getUpdatedBy(),
			'updated_on'		=> $this->getUpdatedOn(),
			'created_by'		=> $this->getCreatedBy(),
			'created_on'		=> $this->getCreatedOn(),
			'report_type_id'	=> $this->getReportTypeId()
		] );

		return $objReportInstance;
	}

}
