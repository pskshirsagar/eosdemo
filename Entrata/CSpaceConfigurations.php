<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSpaceConfigurations
 * Do not add any new functions to this class.
 */

class CSpaceConfigurations extends CBaseSpaceConfigurations {

	public static function fetchAllSpaceConfigurationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsSimple = true ) {

		$strSql = 'SELECT
						sc.id,
						sc.name,
						sc.unit_space_count,
						psc.order_num,
						sc.default_space_configuration_id,
						CASE WHEN ' . CDefaultSpaceConfiguration::SPACE_CONFIGURATION_ENTIRE_UNIT . ' = sc.default_space_configuration_id THEN \'Entire Unit\' ELSE sc.unit_space_count::text END AS count_or_entire_unit,
						sc.occupancy_type_ids
					FROM
						space_configurations sc
						JOIN property_space_configurations psc ON ( psc.cid = sc.cid AND psc.space_configuration_id = sc.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND psc.property_id = ' . ( int ) $intPropertyId . '
						AND sc.is_default = false
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						ORDER BY psc.order_num';

		if( false == $boolIsSimple ) {
			return self::fetchSpaceConfigurations( $strSql, $objDatabase );
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveSpaceConfigurationsByCid( $intCid, $objDatabase, $boolIsPropertyCountRequired = false, $boolFetchOccupancyTypeNames = true ) {

		$strSqlSelect  = '';
		$strSqlJoin    = '';
		$strSqlGroupBy = '';

		if( true == $boolIsPropertyCountRequired ) {
			$strSqlSelect  = ', count ( psc.id ) AS property_count';
			$strSqlJoin    = 'LEFT JOIN LATERAL
							(
							  SELECT DISTINCT
								pscs.cid,
								pscs.id,
								pscs.space_configuration_id
							  FROM
								property_space_configurations pscs
								JOIN properties p ON pscs.cid = p.cid AND pscs.property_id = p.id
								LEFT JOIN property_products pp ON ( pp.cid = p.cid AND pp.property_id = p.id AND pp.ps_product_id = ' . \CPsProduct::ENTRATA . ' )
							  WHERE
								( ( ' . COccupancyType::STUDENT . ' = ANY( p.occupancy_type_ids ) AND pp.id IS NOT NULL ) OR p.property_type_id = ' . CPropertyType::MILITARY . ' )
								AND p.occupancy_type_ids && sc.occupancy_type_ids
								AND pscs.cid = ' . ( int ) $intCid . '
							) psc ON sc.id = psc.space_configuration_id AND sc.cid = psc.cid';
			$strSqlGroupBy = ' GROUP BY
								psc.space_configuration_id,
								sc.id,
								dsc.name,
								sc.name,
								sc.is_student,
								sc.unit_space_count,
								sc.details
								' . ( ( true == $boolFetchOccupancyTypeNames ) ? ',occupancies.occupancy_type_names ' : '' ) . '
							  ORDER BY sc.id;';
		}

		// On Edit property space option screen, only occupancy type ids are required ( instead of occupancy type names ) to compare with property occupancy types
		if( true == $boolFetchOccupancyTypeNames ) {
			$strSqlSelect .= ' ,occupancies.occupancy_type_names';
			$strSqlJoin   .= ' LEFT JOIN LATERAL(
								SELECT
									array_to_string( array_agg( ot.name ), \',\' ) AS occupancy_type_names
								FROM
									occupancy_types ot
								WHERE
									ot.id = ANY( sc.occupancy_type_ids )
							) AS occupancies ON TRUE';
		} else {
			$strSqlSelect .= ' ,sc.occupancy_type_ids';
		}

		$strSql = 'SELECT
						dsc.name as default_space_configuration_name, 
						sc.id, 
						sc.name, 
						sc.is_student,
						sc.unit_space_count' . $strSqlSelect . ',
						sc.details
				   FROM
						space_configurations sc
						LEFT JOIN default_space_configurations dsc ON( sc.default_space_configuration_id = dsc.id )
						' . $strSqlJoin . '
				   WHERE
						sc.id <> ' . CSpaceConfiguration::SPACE_CONFIG_CONVENTIONAL . '
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						' . $strSqlGroupBy;

		return self::fetchSpaceConfigurations( $strSql, $objDatabase );
	}

	public static function fetchDefaultConventionalSpaceConfigurationByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						dsc.name as default_space_configuration_name, sc.id, sc.name, sc.unit_space_count
					FROM
						space_configurations sc
						JOIN default_space_configurations dsc ON( sc.default_space_configuration_id = dsc.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND dsc.id = ' . CDefaultSpaceConfiguration::SPACE_CONFIGURATION_CONVENTIONAL . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						AND sc.is_default = true
					ORDER BY sc.id
					LIMIT 1';

		return self::fetchSpaceConfiguration( $strSql, $objDatabase );
	}

	public static function fetchSpaceConfigurationNameBySpaceConfigurationIdByCid( $intSpaceConfigurationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sc.name
					FROM
						space_configurations sc
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.id = ' . ( int ) $intSpaceConfigurationId . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL ';

		return parent::fetchColumn( $strSql, 'name', $objDatabase );
	}

	public static function fetchSpaceConfigurationByIdBySelectClauseByCid( $intId, $strSelectClause, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						' . $strSelectClause . '
					FROM
						space_configurations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchSpaceConfiguration( $strSql, $objDatabase );
	}

	public static function fetchCustomSpaceConfigurationByIdByCid( $intId, $strSelectClause, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						' . $strSelectClause . ',
						dsc.name AS default_space_configuration_name
					FROM
						space_configurations sc
						JOIN default_space_configurations dsc ON (dsc.id = sc.default_space_configuration_id)
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND sc.id = ' . ( int ) $intId . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL';

		return self::fetchSpaceConfiguration( $strSql, $objDatabase );
	}

	public static function fetchSpaceConfigurationsBySpaceConfigurationIdsByCid( $arrintSpaceConfigurationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSpaceConfigurationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sc.id,
						sc.name,
						sc.unit_space_count
					FROM
						space_configurations sc
					WHERE
						sc.id IN ( ' . implode( ', ', $arrintSpaceConfigurationIds ) . ' )
						AND sc.cid = ' . ( int ) $intCid . '
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL ';

		return self::fetchSpaceConfigurations( $strSql, $objDatabase );
	}

	public static function fetchSpaceConfigurationsWithPricingByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $intFloorPlanId = NULL, $intLeaseStartWindowId = NULL, $intAlwaysFetchedSpaceConfigurationId = NULL, $intUnitTypeId = NULL, $intLeaseIntervalId = NULL, $intLeaseIntervalTypeId = NULL, $boolIsIgnorePricing = false, $arrintSkipDefaultSpaceConfigurationIds = [], $intLeaseOccupancyTypeId = NULL ) {

		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || ( CLeaseIntervalType::RENEWAL == $intLeaseIntervalTypeId && false == valId( $intLeaseIntervalId ) ) ) {
			return [];
		}

		$strCachedRatesJoinConditions = '';
		$strWhere                     = '';

		if( false == $boolIsIgnorePricing ) {
			if( CLeaseIntervalType::RENEWAL == $intLeaseIntervalTypeId ) {
				$boolIsRenewal = true;
				$strTableName  = 'renewal_rates';
			} else {
				$boolIsRenewal = false;
				$strTableName  = 'prospect_rates';
			}

			$objRateLogsFilter = new CRateLogsFilter();
			$objRateLogsFilter->setCids( ( array ) $intCid );
			$objRateLogsFilter->setArOriginIds( ( array ) CArOrigin::BASE );
			$objRateLogsFilter->setPropertyGroupIds( ( array ) $intPropertyId );
			$objRateLogsFilter->setPropertyFloorplanIds( ( array ) $intFloorPlanId );
			$objRateLogsFilter->setLeaseStartWindowId( $intLeaseStartWindowId );
			$objRateLogsFilter->setUnitTypeIds( ( array ) $intUnitTypeId );
			$objRateLogsFilter->setLeaseIntervalIds( ( array ) $intLeaseIntervalId );
			$objRateLogsFilter->setIsRenewal( $boolIsRenewal );
			$objRateLogsFilter->setIsStudentProperty( true );
			$objRateLogsFilter->setIsEntrata( true );
			$objRateLogsFilter->setPublishedUnitsOnly( false );

			\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

			$strCachedRatesJoinConditions = ' JOIN ' . $strTableName . ' temp ON ( psc.cid = temp.cid AND psc.property_id = temp.property_id AND psc.space_configuration_id = temp.space_configuration_id ) ';

			if( true == valId( $intAlwaysFetchedSpaceConfigurationId ) ) {
				$strCachedRatesJoinConditions = ' JOIN ' . $strTableName . ' temp ON ( ( psc.cid = temp.cid AND psc.property_id = temp.property_id AND psc.space_configuration_id = temp.space_configuration_id ) OR sc.id = ' . $intAlwaysFetchedSpaceConfigurationId . ' ) ';
			}
		}

		if( true == valArr( $arrintSkipDefaultSpaceConfigurationIds ) ) {
			$strWhere = ' AND sc.default_space_configuration_id NOT IN (' . implode( ',', $arrintSkipDefaultSpaceConfigurationIds ) . ' ) ';
		}

		if( true == valId( $intLeaseOccupancyTypeId ) ) {
			$strWhere .= ' AND ( sc.occupancy_type_ids && ARRAY[ ' . $intLeaseOccupancyTypeId . ' ] )';
		}

		$strSql = 'SELECT
                        sc.id,
                        sc.name,
                        sc.unit_space_count,
                        psc.order_num,
                        sc.default_space_configuration_id
                    FROM
                        space_configurations sc
                        JOIN property_space_configurations psc ON ( psc.cid = sc.cid AND psc.space_configuration_id = sc.id )
                        ' . $strCachedRatesJoinConditions . '
                    WHERE
                        sc.is_default = false
                        AND psc.cid = ' . ( int ) $intCid . '
                        AND psc.property_id = ' . ( int ) $intPropertyId . '
                        AND sc.deleted_by IS NULL
                        AND sc.deleted_on IS NULL
                        ' . $strWhere . '
                    GROUP BY
                        sc.id,
                        sc.name,
                        sc.unit_space_count,
                        psc.order_num,
                        sc.default_space_configuration_id
                    ORDER BY
                        psc.order_num';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSpaceConfigurationByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}
		$strSql = '	 SELECT
						sp.id,
						sp.name,
						psc.property_id
					 FROM
						space_configurations sp
						JOIN property_space_configurations psc ON ( sp.cid = psc.cid AND sp.id = psc.space_configuration_id )
					 WHERE
						psc.cid = ' . ( int ) $intCid . '
						AND psc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND psc.show_in_entrata IS TRUE
						AND psc.show_on_website IS TRUE ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSpaceConfigurationsByDefaultSpaceConfigurationIdByPropertyIdByCid( $intDefaultSpaceConfigurationId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sc.id,
						sc.name
						FROM
							space_configurations sc
						JOIN property_space_configurations psc ON (sc.cid = psc.cid AND sc.id = psc.space_configuration_id)
						WHERE
							sc.default_space_configuration_id = ' . ( int ) $intDefaultSpaceConfigurationId . ' AND
							sc.cid = ' . ( int ) $intCid . ' AND
							psc.property_id = ' . ( int ) $intPropertyId . ' AND
							sc.deleted_by IS NULL AND
							sc.deleted_on IS NULL AND
							psc.show_in_entrata = true AND
							psc.show_on_website = true ';

		return self::fetchSpaceConfigurations( $strSql, $objDatabase );
	}

	public static function fetchActiveSpaceConfigurationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsReturnSqlOnly = false ) {
		$strSql = sprintf( '
                                SELECT
									DISTINCT sc.id,
									sc.name
								FROM
									space_configurations sc
								JOIN cached_rates cr ON ( cr.cid = sc.cid
							        AND cr.effective_date = CURRENT_DATE
							        AND cr.space_configuration_id = sc.id )
								WHERE
									sc.cid = %d
									AND cr.property_id = %d
									', $intCid, $intPropertyId );

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		return self::fetchSpaceConfigurations( $strSql, $objDatabase );
	}

	public static function fetchRenewalSpaceConfigurationsByPropertyIdByUnitTypeIdByCid( $intPropertyId, $intUnitTypeId, $intCid, $objDatabase ) {

		$strSql = sprintf( '
            SELECT
                DISTINCT sc.id,
                sc.name
            FROM space_configurations sc
            JOIN
                renewal_rates rr ON(
                rr.cid = sc.cid
                AND rr.space_configuration_id = sc.id
                )
            WHERE
                rr.cid = %d
                AND rr.property_id = %d
                AND rr.unit_type_id = %d
            ', $intCid, $intPropertyId, $intUnitTypeId );

		return self::fetchSpaceConfigurations( $strSql, $objDatabase );

	}

	public static function fetchAllSpaceConfigurationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						sc.id,
						sc.name,
						sc.unit_space_count,
						psc.order_num
					FROM
						space_configurations sc
						JOIN property_space_configurations psc ON ( psc.cid = sc.cid AND psc.space_configuration_id = sc.id )
					WHERE
						sc.cid = ' . ( int ) $intCid . '
						AND psc.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND sc.is_default = false
						AND sc.deleted_by IS NULL
						AND sc.deleted_on IS NULL
						ORDER BY psc.order_num';

		return self::fetchSpaceConfigurations( $strSql, $objDatabase );

	}

}

?>