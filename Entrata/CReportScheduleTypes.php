<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportScheduleTypes
 * Do not add any new functions to this class.
 */

class CReportScheduleTypes extends CBaseReportScheduleTypes {

    public static function fetchReportScheduleTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CReportScheduleType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchReportScheduleType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CReportScheduleType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPublishedReportScheduleTypes( $objDatabase ) {
    	$strSql = ' SELECT
    					*
    				FROM
    					report_schedule_types
    				WHERE
    					is_published = 1';

    	return self::fetchReportScheduleTypes( $strSql, $objDatabase );
    }

}
?>