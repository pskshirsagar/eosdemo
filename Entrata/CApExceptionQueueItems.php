<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApExceptionQueueItems
 * Do not add any new functions to this class.
 */

class CApExceptionQueueItems extends CBaseApExceptionQueueItems {

	public static function fetchPaginatedIncompleteApExceptionQueueItemsByDashboardFilterByApHeaderSubTypeIdsByCid( $objDashboardFilter, $arrintApHeaderSubTypeIds, $arrintPropertyGroupIds, $intOffset, $intLimit, $strSortBy, $intCid, $objClientDatabase ) {
		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valArr( $arrintApHeaderSubTypeIds ) || false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'WITH user_properties AS (
						SELECT 
							ARRAY (
									SELECT
										lp.property_id
									FROM
										load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ], ARRAY [ ' . CPsProduct::ENTRATA . ' ], FALSE ) AS lp
									) as permissioned_property_ids,
						
							ARRAY 
									(
									SELECT
										lp.property_id
									FROM
										load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . sqlIntImplode( $objDashboardFilter->getPropertyGroupIds() ) . ' ], ARRAY [ ' . CPsProduct::ENTRATA . ' ], FALSE ) AS lp
									) as filtered_property_ids
						)
						
					SELECT
						DISTINCT 1 AS priority,
						aeqi.id,
						aeqi.cid,
						COALESCE ( ( aeqi.details -> \'ap_header\' ->> \'ap_header_sub_type_id\'::TEXT )::INTEGER, ' . CApHeaderSubType::STANDARD_INVOICE . '::INTEGER ) AS ap_header_sub_type_id,
						aeqi.details -> \'ap_header\' ->> \'header_number\'::TEXT AS header_number,
						ap.company_name || \' \' || apl.location_name AS ap_payee_name,
						CASE
							 WHEN array_length ( pids.ids, 1 ) > 1 THEN \'Multiple\'
							ELSE p.property_name
						END AS property_name,
						aeqi.missing_items,
						aeqi.details,
						aeqi.created_on,
						array_to_string ( pids.ids, \',\' ) AS property_ids
					FROM
						ap_exception_queue_items aeqi
						JOIN LATERAL 
						(
							SELECT
								array_agg ( DISTINCT ( ( d.value::json ) ->> \'property_id\' )::TEXT::INTEGER ) AS ids
							FROM
							(
								SELECT
									*
								FROM
									json_each ( ( aeqi.details::json ) -> \'ap_details\' )
							) AS d
						WHERE
							( ( d.value::json ) ->> \'property_id\' )::TEXT::INTEGER IS NOT NULL
						) pids ON TRUE
						LEFT JOIN dashboard_priorities dp ON ( dp.cid = aeqi.cid )
						LEFT JOIN ap_payees ap ON ( ap.cid = aeqi.cid AND ap.id = ( aeqi.details -> \'ap_header\' ->> \'ap_payee_id\' )::INTEGER )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = aeqi.cid AND apl.id = ( aeqi.details -> \'ap_header\' ->> \'ap_payee_location_id\' )::INTEGER )
						LEFT JOIN properties p ON ( p.cid = aeqi.cid AND p.id = ANY ( pids.ids ) )
					WHERE
						aeqi.resolved_on IS NULL
						AND aeqi.processed_on IS NULL
						AND aeqi.cancelled_on IS NULL
						AND aeqi.cid = ' . ( int ) $intCid . '
						AND COALESCE ( (aeqi.details -> \'ap_header\' ->> \'ap_header_sub_type_id\'::TEXT)::INTEGER, ' . CApHeaderSubType::STANDARD_INVOICE . '::INTEGER ) IN ( ' . sqlIntImplode( $arrintApHeaderSubTypeIds ) . ' )
						AND ( 
								p.id IS NULL
								OR
								( 
								/* All associated property check - all permissioned property groups */
								ARRAY( SELECT pids.ids ) <@ ( SELECT permissioned_property_ids FROM user_properties )
								AND 
								/* Filtered property check - filtered property group ids */            
								ARRAY( SELECT pids.ids ) && ( SELECT filtered_property_ids FROM user_properties )
							 )
						)
						AND aeqi.cancelled_by IS NULL
					ORDER BY
						' . $strSortBy . ', id DESC
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchIncompleteApExceptionQueueItemsCountByDashboardFilterByApHeaderSubTypeIdsByCid( $objDashboardFilter, $arrintApHeaderSubTypeIds, $arrintPropertyGroupIds, $intCid, $objDatabase, $intMaxExecutionTimeOut = 0, $boolIsGroupByProperties = false ) {
		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valArr( $arrintApHeaderSubTypeIds ) ) return NULL;

		$intOffset = 0;
		$intLimit  = 100;

		$strSql = 'WITH user_properties AS (
						SELECT
						     ARRAY (
						              SELECT
						                  lp.property_id
						              FROM
						                  load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ], ARRAY [ ' . CPsProduct::ENTRATA . ' ], FALSE ) AS lp
						            ) as permissioned_property_ids,
						
						     ARRAY
						          (
						              SELECT
						                  lp.property_id
						              FROM
						                  load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . sqlIntImplode( $objDashboardFilter->getPropertyGroupIds() ) . ' ], ARRAY [ ' . CPsProduct::ENTRATA . ' ], FALSE ) AS lp
						            ) as filtered_property_ids
						            
					)
					SELECT
						DISTINCT 1 as priority,
						aeqi.id,
						p.id AS property_id,
						p.property_name
					FROM
						ap_exception_queue_items aeqi
						JOIN LATERAL (
							SELECT
								array_agg( DISTINCT ( ( d.value::json )->> \'property_id\' )::TEXT::INTEGER ) as ids
							FROM
								(
									SELECT * FROM json_each ( ( aeqi.details ::json ) -> \'ap_details\' )
								) AS d
							WHERE
								( ( d.value::json )->> \'property_id\' )::TEXT::INTEGER IS NOT NULL
						) pids ON TRUE
						LEFT JOIN dashboard_priorities dp ON ( dp.cid = aeqi.cid )
						LEFT JOIN properties p ON ( p.cid = aeqi.cid AND p.id = ANY( pids.ids ) )
					WHERE
						aeqi.resolved_on IS NULL
						AND aeqi.processed_on IS NULL
						AND aeqi.cancelled_on IS NULL
						AND aeqi.cid = ' . ( int ) $intCid . '
						AND COALESCE ( (aeqi.details -> \'ap_header\' ->> \'ap_header_sub_type_id\'::TEXT)::INTEGER, ' . CApHeaderSubType::STANDARD_INVOICE . '::INTEGER ) IN ( ' . sqlIntImplode( $arrintApHeaderSubTypeIds ) . ' )
						AND (
								p.id IS NULL
								OR
								(
									/* All associated property check - all permissioned property groups */
									ARRAY( SELECT pids.ids ) <@ ( SELECT permissioned_property_ids FROM user_properties )
									AND
									/* Filtered property check - filtered property group ids */
									ARRAY( SELECT pids.ids ) && ( SELECT filtered_property_ids FROM user_properties )
								)
							)
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . ( int ) $intLimit;

		if( false == $boolIsGroupByProperties ) {
			$strSql     = ( ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '' ) . ' SELECT COUNT(1) || \'-\' || COALESCE( MAX( priority ), 1 ) as count FROM ( ' . $strSql . ' ) as subQ';
			$arrmixData = fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objDatabase );
			if( true == valArr( $arrmixData ) ) {
				return $arrmixData[0]['count'];
			} else {
				return '0-0';
			}
		} else {
			$strSql = 'SELECT COUNT(1) AS financial_incomplete_invoices, priority, property_name, property_id FROM ( ' . $strSql . ' ) as subQ GROUP BY property_name, property_id, priority';
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchApExceptionDetailByUtilityBillId( $intUtilityBillId, $intClientId, $objClientDatabase, $boolCheckNonCanceled = false ) {

		$strWhere = '';

		if( true == $boolCheckNonCanceled ) {
			$strWhere = ' AND cancelled_by IS NOT NULL AND cancelled_on IS NOT NULL';
		}

		$strSql = 'SELECT 
						id,
						cancelled_by,
						cancelled_on
					FROM 
						ap_exception_queue_items
					WHERE 
						cid = ' . ( int ) $intClientId . '   
 				        AND utility_bill_id = ' . ( int ) $intUtilityBillId . $strWhere;

		return getArrayElementByKey( 0, fetchData( $strSql, $objClientDatabase ) );
	}

	public static function fetchApExceptionQueueItemByIdByOrderHeaderIdByCid( $intId, $intOrderHeaderId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						*
					FROM
						ap_exception_queue_items
					WHERE
						id= ' . ( int ) $intId . '
						AND order_header_id = ' . ( int ) $intOrderHeaderId . ' 
						AND cid = ' . ( int ) $intCid;

		return self::fetchApExceptionQueueItem( $strSql, $objDatabase );
	}

	public static function fetchActiveApExceptionQueueItemByIdByOrderHeaderIdByCid( $intId, $intOrderHeaderId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						*
					FROM
						ap_exception_queue_items
					WHERE
						id= ' . ( int ) $intId . '
						AND processed_by IS NULL
						AND cancelled_by IS NULL
						AND order_header_id = ' . ( int ) $intOrderHeaderId . ' 
						AND cid = ' . ( int ) $intCid;

		return self::fetchApExceptionQueueItem( $strSql, $objDatabase );
	}

	public static function fetchActiveApExceptionQueueItemsByIdsByOrderHeaderIdsByCid( $arrintIds, $arrintOrderHeaderIds, $intCid, $objDatabase ) {
		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) || false == ( $arrintOrderHeaderIds = getIntValuesFromArr( $arrintOrderHeaderIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						*
					FROM
						ap_exception_queue_items
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . sqlIntImplode( $arrintIds ) . ' )
						AND order_header_id IN ( ' . sqlIntImplode( $arrintOrderHeaderIds ) . ' ) 
						AND processed_by IS NULL
						AND cancelled_by IS NULL';

		return self::fetchApExceptionQueueItems( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCatalogTypeIncompleteApExceptionQueueItemsByDashboardFilterByCid( $objDashboardFilter, $intCid, $arrintPropertyGroupIds, $objClientDatabase, $objPagination, $arrmixFilter = [] ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return [];
		}

		$strWhereClause = '';

		if( true == valStr( $strItemName = getArrayElementByKey( 'item_name', $arrmixFilter ) ) ) {
			$strWhereClause .= ' AND aeqi.details ->> \'description\'::TEXT ILIKE \'%' . addslashes( $strItemName ) . '%\'';
		}

		$strSql = 'WITH user_properties AS (
						SELECT
							ARRAY (
									SELECT
										lp.property_id
									FROM
										load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ], ARRAY [ ' . CPsProduct::ENTRATA . ' ], FALSE ) AS lp
									) as permissioned_property_ids,
						
							ARRAY
									(
									SELECT
										lp.property_id
									FROM
										load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . sqlIntImplode( $objDashboardFilter->getPropertyGroupIds() ) . ' ], ARRAY [ ' . CPsProduct::ENTRATA . ' ], FALSE ) AS lp
									) as filtered_property_ids
						)
						
					SELECT
						DISTINCT 1 AS priority,
						aeqi.id,
						aeqi.cid,
						aeqi.details ->> \'ap_exception_queue_item_type\'::TEXT AS ap_exception_queue_item_type,
						aeqi.details ->> \'vendor_sku\'::TEXT AS vendor_sku,
						aeqi.details ->> \'item_name\'::TEXT AS item_name,
						ap.company_name AS ap_payee_name,
						p.property_name,
						CASE
							 WHEN array_length ( aeqids.temp_order_ids, 1 ) > 1 THEN \'Multiple\'
							ELSE aeqids.temp_order_ids::text
						END AS temp_order_ids,
						aeqi.missing_items,
						aeqi.details,
						aeqi.created_on,
						array_to_string ( aeqids.temp_order_ids, \',\' ) AS order_ids
					FROM
						ap_exception_queue_items aeqi
						JOIN LATERAL
						(
							SELECT
								array_agg ( DISTINCT d.value::text ) AS temp_order_ids
							FROM
							(
								SELECT
									*
								FROM
									json_each ( ( (aeqi.details->\'po_ap_exception_queue_item_ids\')::json ) )
							) AS d
						WHERE
							 d.value::TEXT IS NOT NULL
						) aeqids ON TRUE
						LEFT JOIN dashboard_priorities dp ON ( dp.cid = aeqi.cid )
						LEFT JOIN ap_payees ap ON ( ap.cid = aeqi.cid AND ap.id = ( aeqi.details ->> \'ap_payee_id\' )::INTEGER )
						LEFT JOIN properties p ON ( p.cid = aeqi.cid AND p.id = (aeqi.details ->> \'property_id\')::INTEGER )
					WHERE
						aeqi.cid = ' . ( int ) $intCid . '
						AND aeqi.resolved_on IS NULL
						AND aeqi.processed_on IS NULL
						AND aeqi.cancelled_on IS NULL
						AND aeqi.cancelled_by IS NULL
						AND aeqi.details ->> \'ap_exception_queue_item_type\'::TEXT = \'catalog_items\'
						AND (
								p.id IS NULL
								OR
								(
									/* All associated property check - all permissioned property groups */
									ARRAY[ (aeqi.details ->> \'property_id\')::INTEGER ] <@ ( SELECT permissioned_property_ids FROM user_properties )
									AND
									/* Filtered property check - filtered property group ids */
									ARRAY[ (aeqi.details ->> \'property_id\')::INTEGER ] && ( SELECT filtered_property_ids FROM user_properties )
								 )
							)
						' . $strWhereClause . '
					ORDER BY
						' . $objPagination->getOrderByField() . ', id DESC
					OFFSET
						' . ( int ) $objPagination->getOffset() . '
					LIMIT
						' . ( int ) $objPagination->getPageSize();

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApExceptionQueueItemByIdByCidByApHeaderSubTypeIdsByPropertyGroupIds( $intId, $intCid, $arrintApHeaderSubTypeIds, $arrintPropertyGroupIds, $objDatabase ) {
		if( false == ( $arrintPropertyGroupIds = getIntValuesFromArr( $arrintPropertyGroupIds ) ) || false == ( $arrintApHeaderSubTypeIds = getIntValuesFromArr( $arrintApHeaderSubTypeIds ) ) ) {
			return [];
		}

		$strSql = 'WITH user_properties AS (
						SELECT 
							ARRAY (
									SELECT
										lp.property_id
									FROM
										load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . sqlIntImplode( $arrintPropertyGroupIds ) . ' ], ARRAY [ ' . CPsProduct::ENTRATA . ' ], FALSE ) AS lp
									) as permissioned_property_ids
					)
					SELECT 
						aeqi.*
					FROM
						ap_exception_queue_items aeqi
						JOIN LATERAL
						(
							SELECT
								array_agg ( DISTINCT CASE WHEN lower( d.value::text ) = \'null\' then NULL else d.value::text::INTEGER END ) AS ids
							FROM
							(
								SELECT
									*
								FROM
									json_each ( ( aeqi.details::json ) )
							) AS d
						WHERE
							 ((d.key)::text = \'property_id\') AND d.value::TEXT IS NOT NULL
						) pids ON TRUE
						LEFT JOIN properties p ON ( p.cid = aeqi.cid AND p.id = ANY( pids.ids ) )
					WHERE	
						aeqi.cid = ' . ( int ) $intCid . '
						AND aeqi.id = ' . ( int ) $intId . '
						AND COALESCE ( (aeqi.details -> \'ap_header\' ->> \'ap_header_sub_type_id\'::TEXT)::INTEGER ) IN ( ' . sqlIntImplode( $arrintApHeaderSubTypeIds ) . ' )
						AND (
								p.id IS NULL
								OR
								( 
									/* All associated property check - all permissioned property groups */
									ARRAY( SELECT pids.ids ) <@ ( SELECT permissioned_property_ids FROM user_properties )
								)
							)
						AND aeqi.resolved_on IS NULL
						AND aeqi.processed_on IS NULL
						AND aeqi.cancelled_on IS NULL';

		return self::fetchApExceptionQueueItem( $strSql, $objDatabase );
	}

	public static function fetchActiveApExceptionQueueItemByIdByCid( $intId, $intCid, $objDatabase, $boolIsApPayee = false ) {
		$strSelectApCompanyName = NULL;
		$strLeftJoinApPayees = NULL;
		if( false == valId( $intId ) ) {
			return NULL;
		}
		if( true == $boolIsApPayee ) {
			$strLeftJoinApPayees = 'LEFT JOIN ap_payees ap ON ( ap.cid = aeqi.cid AND ap.id = ( aeqi.details ->> \'ap_payee_id\' )::INTEGER )';
			$strSelectApCompanyName = ', ap.company_name AS ap_payee_name';
		}
		$strSql = 'SELECT
						aeqi.* ' . $strSelectApCompanyName . '
					FROM
						ap_exception_queue_items aeqi
						' . $strLeftJoinApPayees . '
					WHERE
						aeqi.cid= ' . ( int ) $intCid . '
						AND aeqi.id= ' . ( int ) $intId . '
						AND aeqi.processed_by IS NULL
						AND aeqi.cancelled_by IS NULL
						AND aeqi.resolved_by IS NULL';

		return self::fetchApExceptionQueueItem( $strSql, $objDatabase );
	}

	public static function fetchCatalogPOApExceptionQueueItemsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == ( $arrintIds = getIntValuesFromArr( $arrintIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_exception_queue_items
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . sqlIntImplode( $arrintIds ) . ' )
						AND processed_by IS NULL
						AND cancelled_by IS NULL
						AND resolved_by IS NULL ';

		return self::fetchApExceptionQueueItems( $strSql, $objDatabase );
	}

	public static function fetchApExceptionQueueItemsByApPayeeIdByVendorSkuByCid( $intApPayeeIds, $strVendorSku, $intCid, $objDatabase ) {
		if( false == valId( $intApPayeeIds ) && false == valStr( $strVendorSku ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						aeqi.*
					FROM
                    ap_exception_queue_items aeqi
                    WHERE
						aeqi.cid = ' . ( int ) $intCid . '
						AND aeqi.details ->> \'ap_exception_queue_item_type\'::TEXT = \'catalog_items\'
						AND (aeqi.details ->> \'ap_payee_id\'::text)::INTEGER = ' . ( int ) $intApPayeeIds . '
						AND aeqi.details ->> \'vendor_sku\'::TEXT = \'' . addslashes( $strVendorSku ) . '\'
						AND aeqi.resolved_on IS NULL
						AND aeqi.processed_on IS NULL
						AND aeqi.cancelled_on IS NULL
						AND aeqi.cancelled_by IS NULL';

		return self::fetchApExceptionQueueItems( $strSql, $objDatabase );
	}

}
?>