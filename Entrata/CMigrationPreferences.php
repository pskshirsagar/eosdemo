<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMigrationPreferences
 * Do not add any new functions to this class.
 */

class CMigrationPreferences extends CBaseMigrationPreferences {

	public static function fetchMigrationPreferencesByKeysByCid( $arrstrKeys, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrKeys ) ) return NULL;

		$strSql = 'SELECT * FROM migration_preferences WHERE value IS NOT NULL AND cid::integer = ' . ( int ) $intCid . '::integer AND key in ( \'' . implode( "','", $arrstrKeys ) . '\') ';
		return self::fetchMigrationPreferences( $strSql, $objDatabase );
	}

	public static function fetchMigrationPreferencesBySourceKeyByImportDataTypeIdByCId( $strSourceKey, $intImportDataTypeId, $intCId, $objDatabase ) {
		$strSql = 'SELECT * FROM migration_preferences WHERE value IS NOT NULL AND cid::integer = ' . ( int ) $intCId . '::integer AND source_key = \'' . $strSourceKey . '\' AND import_data_type_id::integer = ' . ( int ) $intImportDataTypeId . '::integer';
		return self::fetchMigrationPreferences( $strSql, $objDatabase );
	}

	public static function fetchMigrationPreferenceByKeyBySourceKeyByImportDataTypeIdByCId( $strKey, $strSourceKey, $intImportDataTypeId, $intCId, $objDatabase ) {
		$strSql = 'SELECT * FROM migration_preferences WHERE value IS NOT NULL AND cid::integer = ' . ( int ) $intCId . '::integer AND key = \'' . $strKey . '\' AND source_key = \'' . $strSourceKey . '\' AND import_data_type_id::integer = ' . ( int ) $intImportDataTypeId . '::integer';
		return self::fetchMigrationPreference( $strSql, $objDatabase );
	}

}
?>