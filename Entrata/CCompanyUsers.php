<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUsers
 * Do not add any new functions to this class.
 */

class CCompanyUsers extends CBaseCompanyUsers {

	public static function fetchCompanyUsersNotInIdsWithEmployeeInfoByCid( $arrintAlreadyAssociatedCompanyUserIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cu.*,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.is_disabled <> 1
						AND cu.username <> \'PS Admin\'
						AND ( ce.name_first IS NOT NULL OR ce.name_last IS NOT NULL )
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.deleted_on IS NULL ';

		if( true == valArr( $arrintAlreadyAssociatedCompanyUserIds ) ) {
			$strSql .= ' AND cu.id NOT IN ( ' . implode( ',', $arrintAlreadyAssociatedCompanyUserIds ) . ' )';
		}

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchActiveCompanyUsersByCid( $intCid, $objClientDatabase, $boolIsAdministrator = false, $boolOrderByLastName = false ) {

		$strWhereCondition  = '';
		$strOrderByLastName = '';
		if( false == valId( $intCid ) ) {
			return NULL;
		}

		if( true == $boolIsAdministrator ) {
			$strWhereCondition = ' AND cu.is_administrator = 1 ';
		}

		if( true == $boolOrderByLastName ) {
			$strOrderByLastName = ', ce.name_last ASC';
		}

		$strSql = 'SELECT
						cu.*,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.is_disabled <> 1
						' . $strWhereCondition . '
						AND cu.username NOT LIKE \'PS Admin%\'
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND ( ce.name_first IS NOT NULL OR ce.name_last IS NOT NULL )
						AND cu.deleted_on IS NULL
					ORDER BY ce.name_first ' . $strOrderByLastName;

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyAdminUsersByCid( $intCid, $objClientDatabase, $boolIsEnabled = false, $boolIncludePSIUsers = true ) {

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    CID = ' . ( int ) $intCid . '
					    AND default_company_user_id IS NULL
					    AND is_administrator = 1';
		if( true == $boolIsEnabled ) {
			$strSql .= ' AND is_disabled = 0';
		}

		if( false == $boolIncludePSIUsers ) {
			$strSql .= ' AND company_user_type_id NOT IN ( ' . implode( ', ', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )';
		} else {
			$strSql .= ' AND company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )';

		}
		$strSql .= ' ORDER BY username';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersByCompanyGroupIdByCid( $intCompanyGroupId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
					    u.*
					FROM
					    company_users u,
					    company_user_groups g
					WHERE
					    u.id = g.company_user_id
					    AND u.cid = g.cid
					    AND g.company_group_id = ' . ( int ) $intCompanyGroupId . '
					    AND u.cid = ' . ( int ) $intCid . '
					    AND u.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
                        AND u.default_company_user_id IS NULL';
		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchMarketingTypeCompanyUserByUsernameByEntrataDomainByCid( $strUsername, $strEntrataDomain, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cu.*
					FROM
						company_users cu
						LEFT JOIN clients mc ON ( cu.cid = mc.id )
					WHERE
						(
							cu.username = \'' . addslashes( ( string ) preg_replace( '/@' . $strEntrataDomain . '$/', '', $strUsername ) . '@' . trim( $strEntrataDomain ) ) . '\'
							OR
							cu.username = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'
						)
						AND cu.company_user_type_id = ' . CCompanyUserType::MARKETING . '
						AND cu.deleted_on IS NULL
						AND mc.id = ' . ( int ) $intCid . '
						AND cu.is_disabled <> 1';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserByUsernameByPasswordByRwxDomainByCid( $strUsername, $strPassword, $strRwxDomain, $intCid, $objClientDatabase, $boolIsResidentWorksLogin = true, $boolCheckWebServicePermissions = false ) {
		// when any PS user log in from CA into RW,
		// he will not have a MC set to him. So we have altered the authentication logic to bipass checking MC for the user.
		// PS user will have prefix "Admin_User_" to his username
		// Although this is not a secure solution, we have added a validation on Add Company User, to validate duplicate usernames.

		if( ( 'Entrata_' != \Psi\CStringService::singleton()->substr( $strUsername, 0, 8 ) ) ) {
			$intClientCondition = 1;
		} else {
			$intClientCondition = $intCid;
		}

		$strSql = 'SELECT
						cu.*
					FROM
						company_users cu
						LEFT JOIN clients mc ON ( cu.cid = mc.id )
					WHERE
						CASE
								WHEN ' . ( int ) $intClientCondition . '=1
								THEN
									(
									cu.username = \'' . addslashes( ( string ) preg_replace( '/@' . $strRwxDomain . '$/', '', $strUsername ) . '@' . trim( $strRwxDomain ) ) . '\'
									OR
									cu.username = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'
							)
								ELSE cu.username = \'' . addslashes( ( string ) $strUsername ) . '\'
							END
						AND cu.password_encrypted = \'' . ( string ) $strPassword . '\'
						AND CASE
								WHEN ' . ( int ) $intClientCondition . '=1
								THEN ( mc.id = ' . ( int ) $intCid . ' OR mc.rwx_domain = \'' . addslashes( $strRwxDomain ) . '\' )
								ELSE mc.id = ' . ( int ) $intCid . '
							END
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.deleted_on IS NULL';

		if( true == $boolIsResidentWorksLogin ) {
			$strSql .= ' AND cu.is_disabled <> 1
							AND cu.block_admin_login <> 1
							AND ( cu.is_administrator = 1 OR cu.employee_portal_only <> 1 )';
		}

		if( true == $boolCheckWebServicePermissions ) {
			$strSql = $strSql . ' AND cu.allow_web_services = 1';
		}

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserByUsernameByRwxDomainByCid( $strUsername, $strRwxDomain, $intCid, $objClientDatabase, $boolIsResidentWorksLogin = true, $boolCheckWebServicePermissions = false, $arrmixOptions = [] ) {
		// when any PS user log in from CA into RW,
		// he will not have a MC set to him. So we have altered the authentication logic to bipass checking MC for the user.
		// PS user will have prefix "Entrata_" to his username
		// Although this is not a secure solution, we have added a validation on Add Company User, to validate duplicate usernames.

		$strCheckUserNameType = '';
		if( ( 'Entrata_' != \Psi\CStringService::singleton()->substr( $strUsername, 0, 8 ) ) ) {
			$intClientCondition = 1;
		} else {
			$intClientCondition = $intCid;
		}

		if( true == $arrmixOptions['is_saml_login'] ?? false ) {
			$strCheckUserNameType .= ' OR lower( cu.username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'';
			if( CCompanyPreference::SAML_USERNAME_TYPE_MIXED_USERNAME == $arrmixOptions['saml_username_type'] ) {
				$strCheckUserNameType .= ' OR lower( cu.username ) = \'' . addslashes( ( string ) preg_replace( '/@.*$/', '', \Psi\CStringService::singleton()->strtolower( $strUsername ) ) . '@' . trim( $strRwxDomain ) ) . '\'';
			}
		}

		$strSql = 'SELECT
						cu.*
					FROM
						company_users cu
						LEFT JOIN clients mc ON ( cu.cid = mc.id )
					WHERE
						CASE
								WHEN ' . ( int ) $intClientCondition . '=1
								THEN
									(
									cu.username = \'' . addslashes( ( string ) preg_replace( '/@' . $strRwxDomain . '$/', '', $strUsername ) . '@' . trim( $strRwxDomain ) ) . '\'
									OR
									cu.username = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'' . $strCheckUserNameType . '
									OR
									(
											cu.is_active_directory_user = 1
											AND
											lower( cu.username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) preg_replace( '/@' . $strRwxDomain . '$/', '', $strUsername ) ) . '@' . trim( $strRwxDomain ) ) . '\'
									 )
							)
								ELSE cu.username = \'' . addslashes( ( string ) $strUsername ) . '\'
							END
						AND CASE
								WHEN ' . ( int ) $intClientCondition . '=1
								THEN ( mc.id = ' . ( int ) $intCid . ' OR mc.rwx_domain = \'' . addslashes( $strRwxDomain ) . '\' )
								ELSE mc.id = ' . ( int ) $intCid . '
							END
						AND cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.deleted_on IS NULL';

		if( true == $boolIsResidentWorksLogin ) {
			$strSql .= ' AND cu.is_disabled <> 1
							AND cu.block_admin_login <> 1
							AND ( cu.is_administrator = 1 OR cu.employee_portal_only <> 1 )';
		}

		if( true == $boolCheckWebServicePermissions ) {
			$strSql = $strSql . ' AND cu.allow_web_services = 1';
		}

		$strSql .= ' LIMIT 1';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchIntegratedCompanyUsersByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
						CID = ' . ( int ) $intCid . '
						AND default_company_user_id IS NULL
						AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND remote_primary_key IS NOT NULL';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCustomCompanyUserByIdByCid( $intCompanyUserId, $intCid, $objClientDatabase ) {
		if ( false == is_numeric( $intCompanyUserId ) ) return NULL;

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    CID = ' . ( int ) $intCid . '
					    AND id = ' . ( int ) $intCompanyUserId;

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersWithCompanyEmployeesDetailByIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase, $boolOrderByName = false ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return NULL;

		$strOrderByClause = ( true == $boolOrderByName ) ? ' ORDER BY name_first' : '';

		$strSql = 'SELECT
						cu.*,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						COALESCE( ce.email_address, NULL ) as email_address
					FROM
						company_users cu
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )' . $strOrderByClause;

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCustomCompanyUsersByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return NULL;

		$strSql = 'SELECT
						cu.id,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL )as name_last,
						COALESCE( cu.username, NULL ) as username,
						cu.company_user_type_id
					FROM
						company_users cu
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' ) ORDER By ce.name_first';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserByRemotePrimaryKeyByCid( $intCid, $strRemotePrimaryKey, $objClientDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    CID = ' . ( int ) $intCid . '
					    AND default_company_user_id IS NULL
					    AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND lower( remote_primary_key ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) trim( $strRemotePrimaryKey ) ) ) . '\'';
		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompetingCompanyUserCountByUsernameByNonCompetingCompanyUserId( $strUsername, $intNonCompetingCompanyUserId, $intCid, $objClientDatabase, $intCompanyUserTypeId = CCompanyUserType::ENTRATA ) {
		if ( false == is_numeric( $intCid ) ) return NULL;

		$strWhereSql = ' WHERE 
							default_company_user_id IS NULL 
							AND lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strUsername ) ) ) . '\'
							AND company_user_type_id = ' . ( int ) $intCompanyUserTypeId . '
							AND cid = ' . $intCid;
		$strWhereSql .= ( true == is_numeric( $intNonCompetingCompanyUserId ) ) ? ' AND id <> ' . ( int ) $intNonCompetingCompanyUserId : '';

		return self::fetchCompanyUserCount( $strWhereSql, $objClientDatabase );
	}

	public static function fetchCustomCompanyUsersByCid( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    UPPER ( username ) <> \'PS ADMIN\' 
					    AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND UPPER(username) <> \'ADMIN\' 
					    AND default_company_user_id IS NULL 
					    AND cid=' . ( int ) $intCid;
		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCustomCompanyUsersIsDisabledByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
					    id,
					    company_employee_id,
					    is_disabled
					FROM
					    company_users
					WHERE
					    UPPER ( username ) <> \'PS ADMIN\'
					    AND UPPER(username) <> \'ADMIN\'
					    AND default_company_user_id IS NULL
					    AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND cid=' . ( int ) $intCid;
		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersExceptPsAdminByCid( $intCid, $intShowDisabledUsers, $objClientDatabase ) {

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    CID = ' . ( int ) $intCid . '
					    AND default_company_user_id IS NULL
					    AND id != 1
					    AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND username != \'PS Admin\' AND deleted_on IS NULL';

		if( 1 != $intShowDisabledUsers ) {
			$strSql .= ' AND is_disabled <> 1 ';
		}

		$strSql .= ' ORDER BY LOWER( username )';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchNonPsiCompanyUsersByCid( $intCid, $objClientDatabase, $boolIsHideDisabledCompanyUsers = true ) {
		if( true == $boolIsHideDisabledCompanyUsers ) {
			$strHideDisabledCompanyUsersSql = ' AND is_disabled <> 1 ';
		}

		$strSql = 'SELECT
						cu.*,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						INNER JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.default_company_user_id IS NULL
						AND cu.deleted_on IS NULL
						AND cu.username NOT LIKE  \'PS Admin%\' ' . $strHideDisabledCompanyUsersSql . '
					ORDER BY
						ce.name_first,
						ce.name_last';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserByUsernameByCid( $strUsername, $intCid, $objClientDatabase, $arrintCompanyUserTypeIds = [ CCompanyUserType::ENTRATA, CCompanyUserType::SYSTEM, CCompanyUserType::ADMIN ] ) {

		if( true == in_array( CCompanyUserType::ADMIN, $arrintCompanyUserTypeIds ) && 1 == \Psi\Libraries\UtilFunctions\count( $arrintCompanyUserTypeIds ) ) {
			$strCondition = '';
		} else {
			$strCondition = ' AND default_company_user_id IS NULL';
		}
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    CID = ' . ( int ) $intCid . $strCondition . '
					    AND company_user_type_id IN ( ' . implode( ', ', $arrintCompanyUserTypeIds ) . ' )
					    AND LOWER ( username ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strUsername ) ) . '\' LIMIT 1 ';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersByIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase, $boolCompanyEmployeeName = false ) {

		if( false == valArr( $arrintCompanyUserIds ) ) {
			return NULL;
		}

		$strSelect	= ' cu.* ';
		$strJoin	= ' ';
		if( true == $boolCompanyEmployeeName ) {
			$strSelect	= ' cu.id,
							ce.name_first AS name_first,
							ce.name_last AS name_last ';
			$strJoin	= ' JOIN company_employees ce ON( cu.company_employee_id = ce.id ) ';
		}

		$strSql = 'SELECT
						' . $strSelect . ' 
					FROM
						company_users cu 
						' . $strJoin . '
					WHERE
						cu.id IN ( ' . implode( ', ', $arrintCompanyUserIds ) . ' )
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.default_company_user_id IS NULL';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
					    u.username,
					    u.id,
					    p.created_by
					FROM
					    company_users AS u
					    LEFT JOIN properties AS p ON ( u.id = p.created_by AND u.cid = p.cid )
					WHERE
					    u.id IN ( ' . implode( ', ', $arrintCompanyUserIds ) . ' )
					    AND u.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					    AND p.cid = ' . ( int ) $intCid;

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    CID = ' . ( int ) $intCid . '
					    AND default_company_user_id IS NULL
					    AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND company_employee_id = ' . ( int ) $intCompanyEmployeeId . '
					LIMIT
					    1';
		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserByEmailAddressByCid( $strEmailAddress, $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_employees ce,
						company_users cu
					WHERE
						ce.cid = ' . ( int ) $intCid . '
						AND ce.id = cu.company_employee_id
						AND ce.cid = cu.cid
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND LOWER( ce.email_address ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strEmailAddress ) ) . '\' LIMIT 1';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchNonAdminCompanyUsersByPropertyIdsByModuleIdsByManagementId( $arrintPropertyIds, $arrintModuleIds, $intCid, $boolHideInactiveUsers, $objClientDatabase, $boolIsReturnArray = false ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintModuleIds ) ) return NULL;

		$strSql = '
			SELECT
				cu.id,
				cu.cid,
				cu.company_employee_id,
				cu.username
			FROM
				company_users cu
			WHERE
				cu.cid = ' . ( int ) $intCid . '
				AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
				AND cu.employee_portal_only != 1';

		if( true == $boolHideInactiveUsers ) {
			$strSql .= ' AND cu.is_disabled = 0 ';
		}

		$strSql .= '
			AND cu.default_company_user_id IS NULL
			AND CASE WHEN cu.is_administrator = 1 THEN FALSE
			ELSE
				EXISTS (
					SELECT
						1
					FROM
						company_user_property_groups cupg
						JOIN property_groups pg ON pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL
						JOIN property_group_associations pga ON cupg.cid = pga.cid AND pg.id = pga.property_group_id
					WHERE
						cu.cid = cupg.cid AND cupg.company_user_id = cu.id
						AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				)
				AND EXISTS (
					SELECT
						1
					FROM
						company_user_permissions cup
					WHERE
						cup.cid = cu.cid AND cup.company_user_id = cu.id
						AND cup.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND cup.is_inherited = 0 AND cup.is_allowed = 1
					
					UNION ALL
					
					SELECT
						1
					FROM
						company_user_groups cug
						JOIN company_group_permissions cgp ON cgp.cid = cug.cid AND cgp.company_group_id = cug.company_group_id
						LEFT JOIN company_user_permissions cup ON cup.cid = cug.cid AND cup.company_user_id = cug.company_user_id AND cgp.module_id = cup.module_id
					WHERE
						cug.cid = cu.cid AND cug.company_user_id = cu.id
						AND cgp.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND COALESCE( cup.is_inherited, 1 ) = 1 AND cgp.is_allowed = 1
				)
				END
				ORDER BY cu.username
		';

		if( false != $boolIsReturnArray ) {
			return fetchData( $strSql, $objClientDatabase );
		}

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );

	}

	public static function fetchNonAdminCompanyUsersWithNameByPropertyIdsByModuleIdByCid( $arrintPropertyIds, $intModuleId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						cu.id,
						cu.username,
						COALESCE ( ce.name_first, NULL ) AS name_first,
						COALESCE ( ce.name_last, NULL ) AS name_last
					FROM
						company_users cu
						JOIN company_user_permissions cuper ON ( cuper.company_user_id = cu.id AND cuper.cid = cu.cid AND cuper.module_id = ' . ( int ) $intModuleId . ' )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
						LEFT JOIN (
									SELECT
										cug.cid,
										cug.company_user_id,
										COUNT( cug.company_group_id ) AS grp_count
									FROM
										company_user_groups cug
										JOIN company_group_permissions cgp ON ( cgp.cid = cug.cid AND cgp.company_group_id = cug.company_group_id )
									WHERE
										cug.cid = ' . ( int ) $intCid . '
										AND cgp.module_id = ' . ( int ) $intModuleId . '
										AND cgp.is_allowed = 1
									GROUP BY
										cug.cid,
										cug.company_user_id

						) cug1 ON ( cug1.cid = cu.cid AND cug1.company_user_id = cu.id )
					WHERE
						cu.is_administrator = 0
						AND cu.is_disabled = 0
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.default_company_user_id IS NULL

						AND ( cuper.is_allowed = 1 OR ( cuper.is_inherited = 1 AND 0 < cug1.grp_count ) )

						AND EXISTS (
								SELECT 1 FROM
									view_company_user_properties cup
								WHERE
									cup.cid = ' . ( int ) $intCid . '
									AND cup.company_user_id = cu.id
									AND cup.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						)
					GROUP BY
						cu.id,
						cu.username,
						ce.name_first,
						ce.name_last
					ORDER BY
						LOWER ( ce.name_first || ce.name_last ) ASC;';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCustomCompanyUsersByIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase, $intCompanyUserTypeId = CCompanyUserType::ENTRATA ) {
		if( false == valArr( $arrintCompanyUserIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    id IN ( ' . implode( ', ', $arrintCompanyUserIds ) . ' )
					    AND CID = ' . ( int ) $intCid . '
					    AND company_user_type_id = ' . ( int ) $intCompanyUserTypeId . '
					    AND default_company_user_id IS NULL ';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCustomizedCompanyUsersByIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return NULL;
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    id IN ( ' . implode( ', ', $arrintCompanyUserIds ) . ' )
					    AND company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					    AND CID = ' . ( int ) $intCid;
		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCustomCompanyUsersByIds( $arrintCompanyUserIds, $objClientDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return NULL;
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    id IN ( ' . implode( ', ', $arrintCompanyUserIds ) . ' )
					     AND company_user_type_id IN( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )';
		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchConflictingCompanyUserByUsernameByIdByCid( $strUsername, $intId, $intCid, $objClientDatabase ) {

		if( true == is_null( $strUsername ) || 0 == strlen( $strUsername ) ) return NULL;
		if( true == is_null( $intId ) || false == is_numeric( $intId ) ) return NULL;

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    lower( username ) = \'' . trim( \Psi\CStringService::singleton()->strtolower( ( string ) addslashes( $strUsername ) ) ) . '\' 
					    AND id::integer != ' . ( int ) $intId . ' ::integer 
					    AND cid::integer = ' . ( int ) $intCid . ' ::integer 
					    AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND default_company_user_id IS NULL';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersWithEmailAddressByIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return NULL;

		$strSql = 'SELECT
						cu.*,
						ce.email_address
					FROM
						company_users cu,
						company_employees ce
					WHERE
						cu.company_employee_id = ce.id
						AND cu.cid = ce.cid
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersWithNameByIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase, $arrintCompanyUserTypeIds = [ \CCompanyUserType::ENTRATA, \CCompanyUserType::SYSTEM, \CCompanyUserType::ADMIN ] ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return NULL;

		$strSql = 'SELECT
						cu.id,
						cu.cid,
						cu.company_employee_id,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						cu.username,
						cu.company_user_type_id
					FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
					WHERE
						cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ')
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', $arrintCompanyUserTypeIds ) . ' )
						AND cu.default_company_user_id IS NULL ';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCustomCompanyUsersWithNameByIdsByCids( $arrintCompanyUserIds, $arrintCids, $objClientDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return NULL;
		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						cu.id,
						cu.cid,
						cu.company_employee_id,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						cu.username,
						cu.company_user_type_id
					FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
					WHERE
						cu.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ')';

		$arrobjCompanyUsers = self::fetchCompanyUsers( $strSql, $objClientDatabase, false );

		$arrstrCustomCompanyUsers = [];

		if( true == valArr( $arrobjCompanyUsers ) ) {
			foreach( $arrobjCompanyUsers as $objCompanyUser ) {
				$intCid = $objCompanyUser->getCid();
				$intUserId = $objCompanyUser->getId();
				$arrstrCustomCompanyUsers[$intCid][$intUserId] = $objCompanyUser;
			}
		}

		return $arrstrCustomCompanyUsers;
	}

	public static function fetchCompanyUsersWithNameByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cu.id,
						cu.cid,
						cu.company_employee_id,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						cu.username,
						cu.is_administrator,
						cu.is_disabled
					FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.cid = cu.cid  AND ce.id = cu.company_employee_id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					ORDER BY LOWER( ce.name_first || ce.name_last ) ASC';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersWithNameWithEmailAddressByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cu.id,
						cu.cid,
						cu.company_employee_id,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						COALESCE( ce.email_address, NULL ) as email_address,
						cu.username,
						cu.is_administrator,
						cu.is_disabled
					FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
					WHERE
						cu.is_disabled = 0
						AND ce.email_address IS NOT NULL
						AND COALESCE( NULLIF( cu.is_administrator, 0 ), cu.is_general_task_stakeholder ) = 1
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.default_company_user_id IS NULL
					ORDER BY LOWER( ce.name_first || ce.name_last ) ASC';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchRelieverCompanyUsersWithNameWithEmailAddressByCid( $arrstrModulesName, $intCid, $objClientDatabase ) {

		$strModuleSql = 'SELECT
							m.id
						FROM
							modules m
						WHERE
							m.name = ANY( \' { ' . implode( ',', $arrstrModulesName ) . ' } \' )';

		$strSql = 'SELECT
						cu.id,
						cu.cid,
						cu.company_employee_id,
						ce.name_first,
						ce.name_last,
						ce.email_address,
						cu.username,
						cu.is_administrator,
						cu.is_disabled
					FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.is_disabled = 0
						AND ce.name_first IS NOT NULL
						AND cu.username NOT LIKE \'%PS Admin%\'
						AND cu.default_company_user_id IS NULL
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND CASE
								WHEN cu.is_administrator = 0 THEN(
																	cu.id IN(
																				SELECT
																					cup.company_user_id
																				FROM
																					company_user_permissions cup
																				WHERE
																					cup.cid = ' . ( int ) $intCid . '
																					AND cup.module_id IN( ' . $strModuleSql . ' )
																					AND cup.is_allowed = 1
																			UNION
																				SELECT
																					DISTINCT cug.company_user_id
																				FROM
																					company_user_groups cug
																					JOIN company_group_permissions cgp ON ( cgp.company_group_id = cug.company_group_id AND cgp.cid = cug.cid )
																				WHERE
																						cug.cid = ' . ( int ) $intCid . '
																						AND cgp.module_id IN( ' . $strModuleSql . ' )
																						AND cgp.is_allowed = 1
																						AND cgp.module_id NOT IN (
																													SELECT
																														cup.module_id
																													FROM
																														company_user_permissions cup
																													WHERE
																														cup.cid = cgp.cid
																														AND cup.module_id = cgp.module_id
																														AND cup.company_user_id = cug.company_user_id
																														AND cup.is_inherited = 0
																												)
																			)
																)
									WHEN cu.is_administrator = 1 THEN cu.id <> 0
							END
					ORDER BY LOWER( ce.name_first || ce.name_last ) ASC';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersCountByCompanyUserPreferenceKeysByPropertyIdByCid( $arrstrUserPreferenceKeys, $intPropertyId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrstrUserPreferenceKeys ) ) return 0;

		$strSql = 'SELECT
						count( cu.id ) AS count
					FROM
						company_users AS cu
						JOIN company_user_preferences AS cup ON cu.id = cup.company_user_id AND cu.is_disabled = 0 AND cu.cid = cup.cid
					WHERE
						(
							cu.is_administrator = 1
							OR 
							EXISTS (
								SELECT
								FROM
									view_company_user_properties AS cupr
								WHERE
									cupr.cid = cup.cid
									AND cupr.company_user_id = cup.company_user_id
									AND cupr.property_id = ' . ( int ) $intPropertyId . '
							)
						)
						AND cup.key IN (  \'' . implode( '\',\'',  $arrstrUserPreferenceKeys ) . '\' )
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.default_company_user_id IS NULL';

		$arrstrData = ( array ) fetchData( $strSql, $objClientDatabase );

		return $arrstrData[0]['count'];
	}

	public static function fetchCompanyUsersCountByCompanyUserPreferenceKeysByPropertyIdsByCid( $arrstrUserPreferenceKeys, $arrintPropertyIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrstrUserPreferenceKeys ) ) return;
		if ( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT
						count(cu.id) as company_user_count,
						cupr.property_id,
						max( cu.is_administrator ) as is_administrator
					FROM
						company_users cu
						JOIN company_user_preferences cup ON ( cu.id = cup.company_user_id AND cup.key IN ( \'' . implode( '\',\'',  $arrstrUserPreferenceKeys ) . '\' ) AND cu.is_disabled = 0 AND ( cu.cid = ' . ( int ) $intCid . ' AND cu.cid = cup.cid ) )
						LEFT JOIN view_company_user_properties cupr on ( cup.company_user_id = cupr.company_user_id AND cup.cid = cupr.cid )
					WHERE
						( cu.is_administrator = 1 OR cupr.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ) ' . '
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' 
					GROUP BY cupr.property_id';

		$arrstrData = fetchData( $strSql, $objClientDatabase );

		return $arrstrData;
	}

	public static function fetchEmployeeCompanyUserByIdByCidByIsDisabled( $intId, $intCid, $objClientDatabase, $boolIsCheckForEnabled = false, $arrintCompanyUserTypeIds = [ CCompanyUserType::ENTRATA, CCompanyUserType::SYSTEM, CCompanyUserType::ADMIN ] ) {
		$strCondition = ( true == $boolIsCheckForEnabled ) ? ' AND cu.is_disabled <> 1 ' : '';

		$strSql = 'SELECT
						cu.*,
						COALESCE( ce.name_first, NULL ) AS name_first,
						COALESCE( ce.name_last, NULL ) AS name_last,
						COALESCE( ce.email_address, NULL ) AS email_address
					FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.id = ' . ( int ) $intId . '
		                AND cu.company_user_type_id IN ( ' . implode( ', ', $arrintCompanyUserTypeIds ) . ' )' .
						$strCondition;

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchEmployeeCompanyUsersByCidByIsDisabled( $intCId, $objClientDatabase, $boolIsCheckForEnabled = false ) {

		$strCondition = ( true == $boolIsCheckForEnabled ) ? ' AND cu.is_disabled = 0' : '';

		$strSql = 'SELECT
						cu.*,
						COALESCE( ce.name_first, NULL ) AS name_first,
						COALESCE( ce.name_last, NULL ) AS name_last,
						COALESCE( ce.email_address, NULL ) AS email_address
					FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						cu.cid = ' . ( int ) $intCId . '
						AND cu.company_user_type_id IN ( ' . implode( ' ,', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) ' .
						$strCondition;

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCustomEmployeeCompanyUserByIdByIsDisabled( $intId, $objClientDatabase, $boolIsCheckForEnabled = false ) {

		$strCondition = ( true == $boolIsCheckForEnabled ) ? ' AND cu.is_disabled = 0' : '';

		$strSql = 'SELECT
						cu.*,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						COALESCE( ce.email_address, NULL ) as email_address
				   FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
				   WHERE
						cu.id =' . ( int ) $intId . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )' .
						$strCondition;

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchPermissionedModuleIds( $objCompanyUser, $objClientDatabase ) {

		if( 1 == $objCompanyUser->getIsAdministrator() ) {

			$strSql = ' SELECT
							id AS module_id,
							\'0\' AS permission
						FROM
							modules
						WHERE
							is_published = 1
							AND id < ' . CModule::PLACEHOLDER_MODULE_THRESHOLD . '

						/* UNION

						SELECT
							module_id,
							\'0\' AS permission
						FROM
							reports r
							JOIN modules m ON r.module_id = m.id AND m.is_published = 1
						WHERE
							r.cid = ' . ( int ) $objCompanyUser->getCid() . ' */

						UNION

						SELECT
							module_id,
							\'0\' AS permission
						FROM
							report_instances ri
							JOIN modules m ON ri.module_id = m.id AND m.is_published = 1
						WHERE
							ri.cid = ' . ( int ) $objCompanyUser->getCid() . '

						UNION

						SELECT
							module_id,
							\'0\' AS permission
						FROM
							report_groups rg
							JOIN modules m ON rg.module_id = m.id AND m.is_published = 1
						WHERE
							rg.cid = ' . ( int ) $objCompanyUser->getCid() . ';';

		} else {
			// user permissioned modules
			$strSql = 'SELECT
							cup.module_id,
							cup.is_read_only AS permission
						FROM
							company_user_permissions cup
							LEFT JOIN company_preferences cp ON ( cp.cid = cup.cid AND cp.key = \'LIMIT_PERMISSIONS_TO_GROUPS_ONLY\' AND cp.value = \'1\' )
						WHERE
							cup.is_allowed = 1
							AND cp.id IS NULL
							AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
							AND cup.cid = ' . ( int ) $objCompanyUser->getCid();

			// group permissioned modules
			$strSql .= ' UNION
							SELECT
								cgp.module_id,
								MIN( cgp.is_read_only ) AS permission
							FROM
								company_group_permissions cgp
							WHERE
								cgp.company_group_id IN (
															SELECT
																cug.company_group_id
															FROM
																company_user_groups cug
															WHERE
																cug.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
																AND cug.cid = ' . ( int ) $objCompanyUser->getCid() . '
								)
								AND cgp.module_id NOT IN (
															SELECT
																cup.module_id
															FROM
																company_user_permissions cup
																LEFT JOIN company_preferences cp ON ( cp.cid = cup.cid AND cp.key = \'LIMIT_PERMISSIONS_TO_GROUPS_ONLY\' AND cp.value = \'1\' )
															WHERE
																cup.is_inherited = 0
																AND cp.id IS NULL
																AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
																AND cup.cid = ' . ( int ) $objCompanyUser->getCid() . '
								)
								AND cgp.is_allowed = 1
								AND cgp.cid = ' . ( int ) $objCompanyUser->getCid() . '
							GROUP BY cgp.module_id';

			// always load public modules
			$strSql .= ' UNION
							SELECT
								id,
								\'0\' AS permission
							FROM
								modules
							WHERE
								( is_public = 1	OR is_hidden = 1 )
								AND is_published = 1
								AND id < ' . CModule::PLACEHOLDER_MODULE_THRESHOLD;
		}

		$arrintResultData = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrintModuleIds = [];

		if( true == valArr( $arrintResultData ) ) {
			foreach( $arrintResultData as $arrmixModuleIds ) {
				$arrintModuleIds[$arrmixModuleIds['module_id']] = $arrmixModuleIds['permission'];
			}
		}

		return $arrintModuleIds;
	}

	public static function fetchCompanyUserByCompanyUserToken( $objCompanyUserToken, $objClientDatabase, $intTokenValiditySeconds = 15 ) {

		$strSql = 'SELECT
						cu.*,
						cut.cid
					FROM
						company_user_tokens cut,
						company_users cu
					WHERE
						cut.company_user_id = cu.id
						AND cut.cid = cu.cid
						AND cu.deleted_on IS NULL
						AND cu.is_disabled <> 1
						AND cu.block_admin_login <> 1
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND	cut.cid = ' . ( int ) $objCompanyUserToken->getCid() . '
						AND	cut.company_user_id = ' . ( int ) $objCompanyUserToken->getCompanyUserId() . '
						AND cut.token = \'' . addslashes( $objCompanyUserToken->getToken() ) . '\'
						AND cut.updated_on >= NOW() - interval \'' . $intTokenValiditySeconds . ' second\'';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserByActiveCompanyUserToken( $objCompanyUserToken, $objClientDatabase, $intTokenValiditySeconds ) {

		$strSql = 'SELECT
					    cu.*
					FROM
					    company_users cu
					    JOIN company_user_tokens cut ON ( cut.cid = cu.cid AND cut.company_user_id = cu.id )
					WHERE
					    cu.cid = ' . ( int ) $objCompanyUserToken->getCid() . '
					    AND cu.id = ' . ( int ) $objCompanyUserToken->getCompanyUserId() . '
					    AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					    AND cut.token = \'' . addslashes( $objCompanyUserToken->getToken() ) . '\'
					    AND cu.is_disabled <> 1
					    AND cu.block_admin_login <> 1
					    AND cut.updated_on >= NOW ( ) - INTERVAL \'' . $intTokenValiditySeconds . ' second\'
					    AND cut.id = 
					    (
					      SELECT
					          id
					      FROM
					          company_user_tokens cut_temp
					      WHERE
					          cut_temp.cid = cut.cid
					          AND cut_temp.company_user_id = cut.company_user_id
					      ORDER BY
					          id DESC
					      LIMIT
					          1
					    )';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersWithPropertiesAndWebsitesByCid( $intCid, $intShowDisabledUsers, $objClientDatabase ) {

		$strSql = 'WITH company_user_property_names AS (
							SELECT
								cu.cid,
								cu.id,
								CASE
									WHEN cu.is_administrator = 1 THEN \'All Properties\'
									ELSE string_agg ( p.property_name, \', \' )
								END AS company_user_property_names
							FROM
								company_users cu
								LEFT JOIN company_user_property_groups cupg ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id )
								LEFT JOIN property_groups pg ON ( pg.cid = cu.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
								LEFT JOIN property_group_associations pga ON ( pga.cid = cupg.cid AND pga.property_group_id = pg.id  )
								LEFT JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id AND p.is_disabled = 0 )
							WHERE
								cu.cid = ' . ( int ) $intCid . '
								AND cu.default_company_user_id IS NULL
								AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
								' . ( 1 != $intShowDisabledUsers ? ' AND cu.is_disabled <> 1 ' : '' ) . '
							GROUP BY
								cu.cid,
								cu.id ),
						company_user_website_names AS (
							SELECT
								cu.cid,
								cu.id,
								CASE
									WHEN cu.is_administrator = 1 THEN \'All Websites\'
									ELSE array_to_string ( array_agg ( w.name ORDER BY w.name ), \', \' )
								END AS company_user_website_names
							FROM
								company_users cu
								LEFT JOIN company_user_websites cuw ON ( cuw.cid = cu.cid AND cuw.company_user_id = cu.id )
								LEFT JOIN websites w ON ( cuw.cid = w.cid AND cuw.website_id = w.id AND w.deleted_on IS NULL )
							WHERE
								cu.cid = ' . ( int ) $intCid . '
								AND cu.default_company_user_id IS NULL
								AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
								' . ( 1 != $intShowDisabledUsers ? ' AND cu.is_disabled <> 1 ' : '' ) . '
							GROUP BY
								cu.cid,
								cu.id ) ';

		$strSql .= 'SELECT
						cu.id,
						cu.username,
						COALESCE ( ce.name_first, NULL ) AS name_first,
						COALESCE ( ce.name_last, NULL ) AS name_last,
						COALESCE ( ce.email_address, NULL ) AS email_address,
						ce.birth_date,
						ce.tax_number_encrypted,
						cuwn.company_user_website_names,
						cupn.company_user_property_names,
						cu.last_login,
						cu.is_disabled,
						cu.is_administrator,
						cu.cid
					FROM
						company_users cu
						JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
						LEFT JOIN company_user_website_names cuwn ON ( cuwn.cid = cu.cid AND cuwn.id = cu.id )
						LEFT JOIN company_user_property_names cupn ON ( cupn.cid = cu.cid AND cupn.id = cu.id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.username NOT LIKE  \'PS Admin%\'
						AND cu.deleted_on IS NULL
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						' . ( 1 != $intShowDisabledUsers ? ' AND cu.is_disabled <> 1 ' : '' );

		return fetchData( $strSql, $objClientDatabase );
	}

	// @FIXME : This function is use in ols dashboard file so we can't modify this function.

	public static function fetchDisabledCompanyUsersByCid( $boolIsLeasingAgent = false, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cu.id,
						cu.company_employee_id,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )';

		if( true == $boolIsLeasingAgent ) {
			$strSql .= ' JOIN company_departments cd ON( ce.company_department_id = cd.id AND ce.cid = cd.cid AND lower( cd.name ) = \'' . \Psi\CStringService::singleton()->strtolower( CCompanyDepartment::LEASING ) . '\' )';
		} else {
			$strSql .= ' JOIN company_departments cd ON( ce.company_department_id = cd.id AND ce.cid = cd.cid )';
		}

		$strSql .= ' WHERE
						cu.is_disabled = 1
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.default_company_user_id IS NULL';

		$arrstrCompanyUsers = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrstrDisabledCompanyUsers = [];

		if( true == valArr( $arrstrCompanyUsers ) ) {
			foreach( $arrstrCompanyUsers as $arrstrCompanyUser ) {
				$arrstrDisabledCompanyUsers[$arrstrCompanyUser['id']] = $arrstrCompanyUser;
			}
		}

		return $arrstrDisabledCompanyUsers;
	}

	public static function fetchQuickSearchCompanyUsersByCidNotInIdsWithEmployeeInfo( $intCid, $arrintAlreadyAssociatedCompanyUserIds, $arrstrFilteredExplodedSearch, $objClientDatabase ) {

		if( true == empty( $intCid ) || false == is_numeric( $intCid ) ) return NULL;

		$strSearchCriteria = ' AND ';

		if( true == valArr( $arrstrFilteredExplodedSearch, 1 ) && true == isset( $arrstrFilteredExplodedSearch[0] ) && true == is_numeric( $arrstrFilteredExplodedSearch[0] ) ) {
			$strSearchCriteria .= ' ( cu.id = ' . ( int ) $arrstrFilteredExplodedSearch[0] . ' OR ce.id = ' . ( int ) $arrstrFilteredExplodedSearch[0] . ' ) ';
		} else {
			$strSearchCriteria .= ' (
										ce.name_first ILIKE \'%' . implode( '%\' AND ce.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
										OR ce.name_last ILIKE \'%' . implode( '%\' AND ce.name_last ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
										OR cu.username ILIKE \'%' . implode( '%\' AND cu.username ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
									 ) ';
		}

		$strSql = 'SELECT
						cu.*,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.username <> \'PS Admin\'
						AND cu.is_disabled <> 1
						AND ( ce.name_first IS NOT NULL OR ce.name_last IS NOT NULL )
						AND cu.deleted_on IS NULL ' . $strSearchCriteria;

		if( true == valArr( $arrintAlreadyAssociatedCompanyUserIds ) ) {
			$strSql .= ' AND cu.id NOT IN ( ' . implode( ',', $arrintAlreadyAssociatedCompanyUserIds ) . ' )';
		}

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchPermissionsByModuleId( $objCompanyUser, $intModuleId, $objClientDatabase ) {

		if( 1 == $objCompanyUser->getIsAdministrator() ) {
			$strSql = 'SELECT id AS module_id FROM modules WHERE is_published = 1 AND id = ' . ( int ) $intModuleId;
		} else {
			// user permissioned modules
			$strSql = 'SELECT
							cup.module_id
						FROM
							company_user_permissions cup
						WHERE
							cup.is_allowed = 1
							AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
							AND cup.cid = ' . ( int ) $objCompanyUser->getCid() . '
							AND cup.module_id = ' . ( int ) $intModuleId;
			// group permissioned modules
			$strSql .= 'UNION
							SELECT
								cgp.module_id
							FROM
								company_group_permissions cgp
							WHERE
								cgp.company_group_id IN (	SELECT cug.company_group_id	FROM company_user_groups cug WHERE cug.company_user_id = ' . ( int ) $objCompanyUser->getId() . ' AND cug.cid = ' . ( int ) $objCompanyUser->getCid() . ' )
								AND	cgp.module_id NOT IN ( SELECT cup.module_id FROM company_user_permissions cup WHERE cup.is_inherited = 0 AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . ' AND cup.cid = ' . ( int ) $objCompanyUser->getCid() . ' AND cup.module_id = ' . ( int ) $intModuleId . ' )
								AND cgp.is_allowed = 1
								AND cgp.cid = ' . ( int ) $objCompanyUser->getCid() . '
								AND cgp.module_id = ' . ( int ) $intModuleId;
			// always load public modules
			$strSql .= 'UNION
							SELECT id FROM modules WHERE is_public = 1 AND id = ' . ( int ) $intModuleId;
		}

		$arrintResultData = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrintModuleIds = [];

		if( true == valArr( $arrintResultData ) ) {
			foreach( $arrintResultData as $arrmixModuleIds ) {
				$arrintModuleIds[$arrmixModuleIds['module_id']] = $arrmixModuleIds['module_id'];
			}
		}

		return $arrintModuleIds;
	}

	// @TODO: Should this function be in modules??

	public static function fetchPermissionsByModuleNames( $objCompanyUser, $arrstrModuleNames, $objClientDatabase, $boolReturnPermission = false ) {

		if( 1 == $objCompanyUser->getIsAdministrator() ) {
			$strSql = '	SELECT
							m.name AS module_name,
							\'admin\' AS permission
						FROM
							modules m
						WHERE
							m.is_published = 1
							AND m.name IN ( \'' . ( implode( '\',\'', $arrstrModuleNames ) ) . '\')';
		} else {
			// user permissioned modules
			$strSql = 'SELECT
							m.name AS module_name,
							\'user\' AS permission
						FROM
							company_user_permissions cup
							JOIN modules m ON ( m.id = cup.module_id )
						WHERE
							cup.is_allowed = 1
							AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
							AND cup.cid = ' . ( int ) $objCompanyUser->getCid() . '
							AND m.name IN ( \'' . ( implode( '\',\'', $arrstrModuleNames ) ) . '\' )';
			// group permissioned modules
			$strSql .= 'UNION
							SELECT
								 m.name AS module_name,
								\'group\' AS permission
							FROM
								company_group_permissions cgp
								JOIN modules m ON ( m.id = cgp.module_id )
							WHERE
								cgp.company_group_id IN (
													SELECT
														cug.company_group_id
													FROM
														company_user_groups cug
													WHERE
														cug.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
														AND cug.cid = ' . ( int ) $objCompanyUser->getCid() . ' )
								AND	m.name NOT IN (
													SELECT
														m.name
													FROM
														company_user_permissions cup
														JOIN modules m ON ( m.id = cup.module_id )
													WHERE
														cup.is_inherited = 0
														AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
														AND cup.cid = ' . ( int ) $objCompanyUser->getCid() . '
														AND m.name IN ( \'' . ( implode( '\',\'', $arrstrModuleNames ) ) . '\' ) )
								AND cgp.is_allowed = 1
								AND cgp.cid = ' . ( int ) $objCompanyUser->getCid() . '
								AND m.name IN ( \'' . ( implode( '\',\'', $arrstrModuleNames ) ) . '\' ) ';
			// always load public modules
			$strSql .= 'UNION
							SELECT
								m.name AS module_name,
								\'public\' AS permission
							FROM
								modules m
							WHERE
								( m.is_public = 1 OR m.is_hidden = 1 )
								AND m.name IN ( \'' . ( implode( '\',\'', $arrstrModuleNames ) ) . '\' ) ';
		}

		$arrintResultData = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrstrPermissionedModuleNames = [];
		foreach( $arrintResultData as $arrstrModuleName ) {

			if( true == $boolReturnPermission ) {
				$arrstrPermissionedModuleNames[$arrstrModuleName['module_name']] = $arrstrModuleName['permission'];
			} else {
				$arrstrPermissionedModuleNames[] = $arrstrModuleName['module_name'];
			}
		}

		return $arrstrPermissionedModuleNames;
	}

	public static function fetchAllCompanyUsersByCid( $intCid, $objClientDatabase ) {

		$strSql = 'select * from company_users cu where cu.cid = ' . ( int ) $intCid . ' AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchNonAdminUsersByPropertyIdsByRouteIdByCid( $arrintPropertyIds, $intRouteId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}
		$strSql = 'SELECT
						DISTINCT pd.id,
						cu.id as company_user_id
					FROM
						rule_stops rs
						JOIN company_users cu ON ( cu.id = rs.company_user_id AND rs.cid = cu.cid AND rs.route_id = ' . ( int ) $intRouteId . ' AND rs.cid = ' . ( int ) $intCid . ' AND rs.deleted_by IS NULL AND cu.is_administrator !=1 ) AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						JOIN view_company_user_properties cup ON (cu.id = cup.company_user_id AND cup.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND cup.cid = cu.cid )
						JOIN properties p ON ( cup.property_id = p.id AND p.cid = cup.cid )
						JOIN property_details pd ON pd.property_id = p.id AND pd.cid = p.cid';

		return fetchData( $strSql, $objClientDatabase );
	}

	// this needs to be moved in plural file

	public static function fetchCompanyUserByIdByCid( $intId, $intCid, $objClientDatabase, $arrintCompanyUserTypeIds = [ CCompanyUserType::ENTRATA, CCompanyUserType::SYSTEM, CCompanyUserType::ADMIN ] ) {

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    id = ' . ( int ) $intId . '
					    AND cid = ' . ( int ) $intCid . '
					    AND company_user_type_id IN ( ' . implode( ' ,', $arrintCompanyUserTypeIds ) . ' )';

		 return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersByIdsByCids( $arrintIds, $arrintCids, $objClientDatabase, $boolIsDisabled = false, $boolIsReturnKeyedArray = true ) {
		if( false == valArr( $arrintIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    id IN ( ' . implode( ',', $arrintIds ) . ' )
					    AND cid IN ( ' . implode( ',', $arrintCids ) . ' )
					    AND company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) ';

		if( true == $boolIsDisabled ) {
			$strSql .= ' AND is_disabled = 1';
		}

		return self::fetchCompanyUsers( $strSql, $objClientDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchCompanyUsersByCid( $intCid, $objDatabase, $boolIsDisabled = false ) {
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    CID = ' . ( int ) $intCid . '
					    AND company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					    AND default_company_user_id IS NULL';
		if( true == $boolIsDisabled ) {
			$strSql .= ' AND is_disabled = 1';
		}
		return self::fetchCompanyUsers( sprintf( $strSql ), $objDatabase );
	}

	public static function fetchCompanyUsersByCompanyUserTypeIdByCid( $intCompanyUserTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    company_user_type_id = ' . ( int ) $intCompanyUserTypeId . '
					    AND CID = ' . ( int ) $intCid . '
					    AND default_company_user_id IS NULL';
		return self::fetchCompanyUsers( sprintf( $strSql ), $objDatabase );
	}

	public static function fetchCompanyUsersDetailsByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cu.id,
					    cu.username,
					    ce.name_first,
					    ce.name_last
					FROM
					    company_users cu
					    LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
					    cu.cid = ' . ( int ) $intCid . '
					    AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND cu.default_company_user_id IS NULL';

		return self::fetchCompanyUsers( $strSql, $objDatabase );
	}

	public static function fetchCompanyUsersByDefaultCompanyUserIdByCid( $intDefaultCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
						default_company_user_id = ' . ( int ) $intDefaultCompanyUserId . '
						AND company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND CID = ' . ( int ) $intCid;
		return self::fetchCompanyUsers( sprintf( $strSql ), $objDatabase );
	}

	public static function fetchCompanyUsersByCompanyEmployeeIdByCid( $intCompanyEmployeeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
						company_employee_id = ' . ( int ) $intCompanyEmployeeId . '
					    AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND CID = ' . ( int ) $intCid . '
						AND default_company_user_id IS NULL';
		return self::fetchCompanyUsers( sprintf( $strSql ), $objDatabase );
	}

	public static function fetchCompanyUsersByMainEulaIdByCid( $intMainEulaId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_users
					WHERE
						main_eula_id = ' . ( int ) $intMainEulaId . '
						AND CID = ' . ( int ) $intCid . '
						AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND default_company_user_id IS NULL';
		return self::fetchCompanyUsers( sprintf( $strSql ), $objDatabase );
	}

	public static function fetchCompanyUsersByCheck21EulaIdByCid( $intCheck21EulaId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_users
					WHERE
						check21_eula_id = ' . ( int ) $intCheck21EulaId . '
						AND CID = ' . ( int ) $intCid . '
						AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND default_company_user_id IS NULL';
		return self::fetchCompanyUsers( sprintf( $strSql ), $objDatabase );
	}

	public static function fetchCompanyUsersByCompanyUserScoreContactTypeIdByCid( $intCompanyUserScoreContactTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_users
					WHERE
						company_user_score_contact_type_id = ' . ( int ) $intCompanyUserScoreContactTypeId . '
						AND CID = ' . ( int ) $intCid . '
						AND company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND default_company_user_id IS NULL';
		return self::fetchCompanyUsers( sprintf( $strSql ), $objDatabase );
	}

	public static function fetchPermissionedReportCategories( $objCompanyUser, $objClientDatabase ) {

		if( 1 == $objCompanyUser->getIsAdministrator() ) {
			$strSql = 'SELECT
							id,
							title
						FROM
							modules
						WHERE
							is_published = 1
							AND parent_module_id = ' . CModule::REPORT_SYSTEM . '
							AND is_hidden = 0';
		} else {
			// user permissioned modules
			$strSql = 'SELECT
							cup.module_id,
							util_get_translated( \'title\', m.title, m.details ) AS title
						FROM
							company_user_permissions cup
							JOIN modules m ON ( cup.module_id = m.id )
						WHERE
							cup.cid = ' . ( int ) $objCompanyUser->getCid() . '
							AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . '
							AND cup.is_allowed = 1
							AND m.is_published = 1
							AND m.parent_module_id =  ' . CModule::REPORT_SYSTEM;

			// group permissioned modules
			$strSql .= 'UNION
							SELECT
								cgp.module_id,
								util_get_translated( \'title\', m.title, m.details ) AS title
							FROM
								company_group_permissions cgp
								JOIN modules m ON ( cgp.module_id = m.id )
							WHERE
								cgp.company_group_id IN (	SELECT cug.company_group_id	FROM company_user_groups cug WHERE cug.company_user_id = ' . ( int ) $objCompanyUser->getId() . ' AND cug.cid = ' . ( int ) $objCompanyUser->getCid() . ' )
								AND	cgp.module_id NOT IN ( SELECT cup.module_id FROM company_user_permissions cup WHERE cup.is_inherited = 0 AND cup.company_user_id = ' . ( int ) $objCompanyUser->getId() . ' AND cup.cid = ' . ( int ) $objCompanyUser->getCid() . ' )
								AND cgp.is_allowed = 1
								AND cgp.cid = ' . ( int ) $objCompanyUser->getCid() . '
								AND m.is_published = 1
								AND m.parent_module_id =  ' . CModule::REPORT_SYSTEM;
			// new modules
			// always load public modules
			$strSql .= 'UNION
							SELECT
								id,
								title
							FROM
								modules
							WHERE
								is_public = 1
								AND parent_module_id = ' . CModule::REPORT_SYSTEM;
		}

		$arrintResultData = fetchData( $strSql, $objClientDatabase );

		return $arrintResultData;
	}

	public static function fetchCompanyUserWithAllStatusByUsernameByRwxDomainByCid( $strUsername, $strRwxDomain, $intCid, $objClientDatabase, $arrmixOptions = [] ) {
		// when any PS user log in from CA into RW,
		// he will not have a MC set to him. So we have altered the authentication logic to bipass checking MC for the user.
		// PS user will have prefix "Entrata_" to his username
		// Although this is not a secure solution, we have added a validation on Add Company User, to validate duplicate usernames.

		$strCheckUserNameType = '';
		if( ( 'Entrata_' != \Psi\CStringService::singleton()->substr( $strUsername, 0, 8 ) ) ) {
			$intClientCondition = 1;
		} else {
			$intClientCondition = $intCid;
		}

		if( true == $arrmixOptions['is_saml_login'] ?? false ) {
			$strCheckUserNameType .= ' OR lower( cu.username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'';
			if( CCompanyPreference::SAML_USERNAME_TYPE_MIXED_USERNAME == $arrmixOptions['saml_username_type'] ) {
				$strCheckUserNameType .= ' OR lower( cu.username ) = \'' . addslashes( ( string ) preg_replace( '/@.*$/', '', \Psi\CStringService::singleton()->strtolower( $strUsername ) ) . '@' . trim( $strRwxDomain ) ) . '\'';
			}
		}

		$strSql = 'SELECT
						cu.*
					FROM
						company_users cu
						LEFT JOIN clients mc ON ( cu.cid = mc.id )
					WHERE
						CASE
								WHEN ' . ( int ) $intClientCondition . ' = 1
								THEN
									(
									cu.username = \'' . addslashes( ( string ) preg_replace( '/@' . $strRwxDomain . '$/', '', $strUsername ) . '@' . trim( $strRwxDomain ) ) . '\'
									OR
									cu.username = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'' . $strCheckUserNameType . '
									OR
									lower( cu.username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) preg_replace( '/@' . $strRwxDomain . '$/', '', $strUsername ) ) . '@' . trim( $strRwxDomain ) ) . '\'
							)
								ELSE cu.username = \'' . addslashes( ( string ) $strUsername ) . '\'
							END
						AND cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND CASE
								WHEN ' . ( int ) $intClientCondition . '=1
								THEN ( mc.id = ' . ( int ) $intCid . ' OR mc.rwx_domain = \'' . addslashes( $strRwxDomain ) . '\' )
								ELSE 1 = 1
							END
					LIMIT 1';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchUnmigratedCompanyUsersCountByCid( $intCid, $objClientDatabase ) {

		$strSql = '	SELECT
						count(cu.id)
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.id != 1
						AND cu.username NOT LIKE  \'PS Admin%\'
						AND cu.deleted_on IS NULL
						AND cu.is_disabled = 0	
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_active_directory_user = 0';

		$arrintCompanyUserCount = ( array ) fetchData( $strSql, $objClientDatabase );

		return $arrintCompanyUserCount[0]['count'];
	}

	public static function fetchPaginatedUnmigratedCompanyUsersByCid( $intPageNo, $intPageSize, $intCid, $objClientDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						ce.name_first,
						ce.name_last,
						cu.id,
						cu.username as username
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.id != 1
						AND cu.username NOT LIKE  \'PS Admin%\'
						AND cu.deleted_on IS NULL
						AND cu.is_disabled = 0
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_active_directory_user = 0
					ORDER BY
						LOWER(ce.name_first), LOWER(ce.name_last),LOWER(cu.username)
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intLimit;

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchActiveDirectoryMigratedCompanyUsersByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_users cu
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.username NOT LIKE  \'PS Admin%\'
						AND cu.default_company_user_id IS NULL
						AND cu.deleted_on IS NULL
						AND cu.is_disabled = 0
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_active_directory_user = 1
					ORDER BY
						LOWER(cu.username)';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchEmployeeCompanyUserByCompanyGroupIdByIsDisabledByCid( $intCompanyGroupId, $boolIsCheckForEnabled = false, $intCid, $objClientDatabase ) {

		$strCondition = ( true == $boolIsCheckForEnabled ) ? ' AND cu.is_disabled = 0' : '';

		$strSql = 'SELECT
						cu.*,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						COALESCE( ce.email_address, NULL ) as email_address
				   FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
				   WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.id IN ( SELECT g.company_user_id FROM company_user_groups g WHERE g.cid = ' . ( int ) $intCid . ' AND g.company_group_id = ' . ( int ) $intCompanyGroupId . ' ) ' . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA .
						$strCondition;

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchEmployeeCompanyUserByIdsByIsDisabledByCid( $arrintIds, $boolIsCheckForEnabled = false, $intCid, $objClientDatabase ) {

		$strCondition = ( true == $boolIsCheckForEnabled ) ? ' AND cu.is_disabled = 0' : '';

		$strSql = 'SELECT
						cu.*,
						ce.preferred_locale_code,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						COALESCE( ce.email_address, NULL ) as email_address
				   FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
				   WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.id IN( ' . implode( ',', $arrintIds ) . ' ) ' . '
						AND cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) ' .
						$strCondition;

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserByRwxDomainByUsernameByCid( $strRwxDomain, $strUsername, $intCid, $objClientDatabase ) {
		// when any PS user log in from CA into RW,
		// he will not have a MC set to him. So we have altered the authentication logic to bipass checking MC for the user.
		// PS user will have prefix "Entrata_" to his username
		// Although this is not a secure solution, we have added a validation on Add Company User, to validate duplicate usernames.

		if( ( 'Entrata_' != \Psi\CStringService::singleton()->substr( $strUsername, 0, 8 ) ) ) {
			$intClientCondition = 1;
		} else {
			$intClientCondition = $intCid;
		}

		$strSql = 'SELECT
						cu.*
					FROM
						company_users cu
						LEFT JOIN clients mc ON ( cu.cid = mc.id )
					WHERE
						CASE
							WHEN ' . ( int ) $intClientCondition . ' = 1
							THEN
								(
									cu.username = \'' . addslashes( ( string ) preg_replace( '/@' . $strRwxDomain . '$/', '', $strUsername ) . '@' . trim( $strRwxDomain ) ) . '\'
									OR
									cu.username = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'
								)
								ELSE cu.username = \'' . addslashes( ( string ) $strUsername ) . '\'
						END
						AND ( mc.id = ' . ( int ) $intCid . ' OR mc.rwx_domain = \'' . addslashes( $strRwxDomain ) . '\' )
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersByPropertyIdsByCompanyGroupIdsByCid( $arrintPropertyIds, $arrintCompanyGroupIds, $intCid, $objClientDatabase ) {

		$strWhere = '';

		if( true == valArr( $arrintCompanyGroupIds ) ) {
			$strWhere .= ' AND cug.company_group_id IN( ' . implode( ',', $arrintCompanyGroupIds ) . ' ) ';
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$strWhere .= ' AND ( cu.is_administrator = 1 OR ( cup.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) AND p.is_disabled <> 1 ) ) ';
		} else {
			$strWhere .= ' AND ( cu.is_administrator = 1 OR ( p.is_disabled <> 1 ) ) ';
		}

		$strSql = 'SELECT
						DISTINCT ( cu.id ),
						cu.cid,
						ce.preferred_locale_code,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						COALESCE( ce.email_address, NULL ) as email_address,
						cg.name as company_group_name
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
						JOIN company_user_groups cug ON ( cug.cid =  cu.cid AND cu.id = cug.company_user_id )
						JOIN company_groups cg ON ( cg.cid = cu.cid AND cug.company_group_id = cg.id )
						LEFT JOIN view_company_user_properties cup ON ( cup.cid = cu.cid AND cu.id = cup.company_user_id )
						LEFT JOIN properties p ON ( p.cid = cup.cid AND p.id = cup.property_id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						' . $strWhere . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_disabled <> 1';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );

	}

	public static function fetchCompanyUsersByPropertyIdsByCidByGroups( $arrintPropertyIds, $intCid, $objDatabse ) {
		$strWhere = '';

		if( true == valArr( $arrintPropertyIds ) ) {
			$strWhere .= ' AND ( cu.is_administrator = 1 OR ( cup.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' ) AND p.is_disabled <> 1 ) ) ';
		} else {
			$strWhere .= ' AND ( cu.is_administrator = 1 OR ( p.is_disabled <> 1 ) ) ';
		}

		$strSql = 'SELECT
						cu.id,
						cu.cid,
						COALESCE( ce.name_first, NULL ) as name_first,
						COALESCE( ce.name_last, NULL ) as name_last,
						COALESCE( ce.email_address, NULL ) as email_address,
						cg.name as company_group_name
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
						JOIN company_user_groups cug ON ( cug.cid =  cu.cid AND cu.id = cug.company_user_id )
						JOIN company_groups cg ON ( cg.cid = cu.cid AND cug.company_group_id = cg.id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_disabled <> 1';

		return self::fetchCompanyUsers( $strSql, $objDatabse, false );
	}

	public static function fetchCompanyUserPropertyNameByIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) ) return [];

		$strSql = 'SELECT
						cu.id as company_user_id,
						CASE
							WHEN cu.is_administrator = 1 THEN \'Multiple\'
							WHEN count( DISTINCT p.id ) > 1 THEN \'Multiple\'
							ELSE MAX( p.property_name )
						END as property_name
					FROM
						company_users cu
						LEFT JOIN company_user_property_groups cupg ON cupg.cid = cu.cid AND cupg.company_user_id = cu.id
						LEFT JOIN property_groups pg ON ( pg.cid = cu.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						LEFT JOIN property_group_associations pga ON pga.cid = pg.cid AND pga.property_group_id = pg.id
						LEFT JOIN properties p ON p.cid = pga.cid AND p.id = pga.property_id AND p.is_disabled = 0
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
					GROUP BY
						cu.id,
						cu.is_administrator';

		return fetchData( $strSql, $objClientDatabase );
	}

	// @FIXME : This function use in one place but there is we take count so do only join.

	public static function fetchCompanyUsersCountByPropertyIdsByHelpResourceIdByCid( $arrintPropertyIds, $intSelectedHelpResourceId, $intCid, $objClientDatabase, $intVisibleTo ) {

		if( false == valArr( $arrintPropertyIds ) && false == is_numeric( $intSelectedHelpResourceId ) && false == is_numeric( $intCid ) ) return false;

		if( CHelpResource::VISIBLE_TO_ADMINS == $intVisibleTo ) {
			$strCondition = ' AND cu.is_administrator = 1 ';
		}

		$strSql = ' SELECT
						count( company_user_details.* ) as company_users_count
					FROM
					( SELECT
						DISTINCT ON (cu.username) cu.username as username,
						ce.id as company_employee_id,
						cu.id as company_user_id,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON (cu.cid = ce.cid AND cu.company_employee_id = ce.id)
						JOIN company_user_property_groups cupg ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id )
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( cupg.cid = pga.cid AND pg.id = pga.property_group_id AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					WHERE
						cu.cid = ' . ( int ) $intCid . ' AND
						EXISTS (
									SELECT
										1
									FROM
										company_departments cd
										JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
									WHERE
										ced.cid = ' . ( int ) $intCid . '
										AND ced.company_employee_id = ce.id
						) AND
						cu.is_disabled = 0 AND
						cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' AND
						cu.id NOT IN (
										SELECT
											cua.company_user_id
										FROM
											company_user_assessments cua
										WHERE
											cua.completed_on IS NULL AND
											cua.cid = ' . ( int ) $intCid . ' AND
											cua.deleted_by IS NULL AND
											cua.deleted_on IS NULL AND
											cua.help_resource_id = ' . ( int ) $intSelectedHelpResourceId . '
									) ' . $strCondition . ' 
					) AS company_user_details';

		return self::fetchColumn( $strSql, 'company_users_count', $objClientDatabase );

	}

	public static function fetchPaginatedCompanyUsersByPropertyIdsByHelpResourceIdByCid( $intPageNo, $intPageSize, $arrintPropertyIds, $intSelectedHelpResourceId, $intCid, $objClientDatabase, $intVisibleTo ) {

		$strCondition = '';
		if( false == valArr( $arrintPropertyIds ) && false == is_numeric( $intSelectedHelpResourceId ) && false == is_numeric( $intCid ) ) return false;

		if( CHelpResource::VISIBLE_TO_ADMINS == $intVisibleTo ) {
			$strCondition = ' AND cu.is_administrator = 1 ';
		}

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT
						DISTINCT ON (cu.username) cu.username as username,
						ce.id as company_employee_id,
						cu.id as company_user_id,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON (cu.cid = ce.cid AND cu.company_employee_id = ce.id)
						JOIN company_user_property_groups cupg ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id )
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id AND pg.deleted_by IS NULL AND pg.deleted_on IS NULL )
						JOIN property_group_associations pga ON ( cupg.cid = pga.cid AND pg.id = pga.property_group_id AND pga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
					WHERE
						cu.cid = ' . ( int ) $intCid . ' AND
						EXISTS (
									SELECT
										1
									FROM
										company_departments cd
										JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
									WHERE
										ced.cid = ' . ( int ) $intCid . '
										AND ced.company_employee_id = ce.id
						) AND
						cu.is_disabled = 0 AND
						cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' AND
						cu.id NOT IN (
										SELECT
											cua.company_user_id
										FROM
											company_user_assessments cua
										WHERE
											cua.completed_on IS NULL AND
											cua.cid = ' . ( int ) $intCid . ' AND
											cua.deleted_by IS NULL AND
											cua.deleted_on IS NULL AND
											cua.help_resource_id = ' . ( int ) $intSelectedHelpResourceId . '
									) ' . $strCondition . ' 
					ORDER BY
						cu.username OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserDetailsByScheduledEmailIdByCid( $intScheduledEmailId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT (cu.id),
						cu.cid,
						cu.username,
						ce.name_first,
						ce.name_last
					FROM
						company_users AS cu
						JOIN scheduled_emails AS se ON (
								cu.id = se.email_owner_id
								AND cu.cid = se.cid
							)
						JOIN company_employees ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
					WHERE
						cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND se.id = ' . ( int ) $intScheduledEmailId . '
						AND se.deleted_on IS NULL
						AND se.cid = ' . ( int ) $intCid;

		$arrmixScheduledEmailCompanyUsers = ( array ) fetchData( $strSql, $objDatabase );

		$arrmixCompanyUserDetails = [];

		if( true == valArr( $arrmixScheduledEmailCompanyUsers ) ) {
			foreach( $arrmixScheduledEmailCompanyUsers as $arrmixScheduledEmailCompanyUser ) {
				$arrmixCompanyUserDetails['id'] = $arrmixScheduledEmailCompanyUser['id'];
				$arrmixCompanyUserDetails['cid'] = $arrmixScheduledEmailCompanyUser['cid'];
				$arrmixCompanyUserDetails['username'] = $arrmixScheduledEmailCompanyUser['username'];
				$arrmixCompanyUserDetails['name_first'] = $arrmixScheduledEmailCompanyUser['name_first'];
				$arrmixCompanyUserDetails['name_last'] = $arrmixScheduledEmailCompanyUser['name_last'];
			}
		}
		return $arrmixCompanyUserDetails;
	}

	public static function fetchCompanyUsersInfoByModuleIdByCid( $intModuleId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT (cu.id),
						cu.username
					FROM
						company_users AS cu
						LEFT JOIN company_user_permissions AS cup ON ( cu.id = cup.company_user_id AND cu.cid = cup.cid AND cu.is_administrator = 0 AND cup.module_id = ' . ( int ) $intModuleId . ' AND ( cup.is_allowed = 1 OR cup.is_inherited = 1 ) )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_disabled = 0
						AND cu.default_company_user_id IS NULL
						AND cu.id <> 1
						AND cu.deleted_on IS NULL
						AND ( cu.is_administrator = 1 OR ( cup.id IS NOT NULL ) )
					ORDER BY cu.username';

		$arrmixMessageCenterCompanyUsers = fetchData( $strSql, $objDatabase );

		return ( array ) $arrmixMessageCenterCompanyUsers;
	}

	public static function fetchActiveEmployeeNamesByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						cu.id,
						ce.name_first || \' \' || ce.name_last AS name_full
					FROM
						view_company_user_properties cup
						JOIN company_users cu ON ( cup.company_user_id = cu.id AND cup.cid = cu.cid AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' )
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cup.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
						AND cup.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_disabled = 0
						AND cu.deleted_on IS NULL
						AND ce.is_employee = 1
						AND ( ce.name_first IS NOT NULL OR ce.name_last IS NOT NULL )
					ORDER BY ce.name_first';

		$arrmixResultData = ( array ) fetchData( $strSql, $objClientDatabase );

		$arrmixUserData = [];

		if( true == valArr( $arrmixResultData ) ) {
			foreach( $arrmixResultData as $arrmixRecord ) {
				$arrmixUserData[$arrmixRecord['id']] = $arrmixRecord['name_full'];
			}
		}

		return $arrmixUserData;

	}

	public static function fetchNonAdminCompanyUsersFilterData( $arrmixFilterData, $objClientDatabase ) {

		$arrintPropertyIds 		= $arrmixFilterData['property_ids'];
		$arrintModuleIds 		= $arrmixFilterData['module_ids'];
		$arrintGroupIds			= $arrmixFilterData['group_ids'];
		$intCId 				= $arrmixFilterData['cid'];
		$boolHideInactiveUsers 	= $arrmixFilterData['is_active_user'];
		$boolSummarizeBygroup 	= $arrmixFilterData['summarize_by_group'];

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSqlHideInactiveUsers = '';
		$strSqlSummarizeBygroup = '';
		$strSqlGroupsJoin = '';
		$strSqlGroupsCondition = '';
		$strSqlGroupsColumns = '';
		$strSqlSummarizeByUserGroup = '';
		$strSqlSummarizeByCompanyGroup = '';

		if( false == $boolHideInactiveUsers && false == $boolSummarizeBygroup ) {
			$strSqlHideInactiveUsers = ' AND cu.is_disabled = 0 ';
		}

		if( false == $boolSummarizeBygroup ) {
			if( true == valArr( $arrintModuleIds ) ) {
				$strSqlSummarizeByUserGroup = ' AND cup.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' ) ';
				$strSqlSummarizeByCompanyGroup = ' AND cgp.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' ) ';
			}
		}

		if( true == valArr( $arrintGroupIds ) ) {
			$strSqlGroupsCondition = ' AND cug.company_group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )';
		}
		$strSqlGroupsColumns = 'cug.company_group_id';
		$strSqlGroupsJoin = ' LEFT JOIN company_user_groups cug ON ( cug.company_user_id = cu.id AND cug.cid = cu.cid ' . $strSqlGroupsCondition . ' ) ';

		$strSql	= ' SELECT
						DISTINCT ( username ),
						id,
						cid,
						group_id,
						company_employee_id
					FROM
						(
							SELECT
								id,
								cid,
								company_employee_id,
								username,
								module_id,
								group_id,
								parent_module_id,
								is_allowed
							FROM
							(
								SELECT
									cu.id,
									cu.cid,
									cu.company_employee_id,
									cu.username,
									cup.module_id,
									m.parent_module_id,
									' . $strSqlGroupsColumns . ' As group_id,
									cup.is_allowed
								FROM
									company_users cu
									JOIN company_user_permissions cup ON ( cup.cid = cu.cid AND cup.company_user_id = cu.id AND cup.module_id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' AND cup.is_allowed = 1  )
									JOIN modules m ON ( m.id = cup.module_id AND m.is_published = 1 AND m.id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ')
									JOIN view_company_user_properties vcup ON ( vcup.cid = cu.cid AND vcup.company_user_id = cu.id )
									' . $strSqlGroupsJoin . '
								WHERE
									cu.cid = ' . ( int ) $intCId . '
									AND m.id = cup.module_id
									AND cu.employee_portal_only != 1
									AND cu.default_company_user_id IS NULL
									AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
									AND vcup.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) '
												. $strSqlSummarizeByUserGroup . $strSqlHideInactiveUsers . '
							) AS sub
							WHERE
								1 = 1
								-- AND sub.parent_module_id IS NULL

					UNION
						SELECT
							id,
							cid,
							company_employee_id,
							username,
							module_id,
							group_id,
							parent_module_id,
							is_allowed
						FROM
							(
								SELECT
									cu.id,
									cu.cid,
									cu.company_employee_id,
									cu.username,
									cgp.module_id,
									m.parent_module_id,
									' . $strSqlGroupsColumns . '  As group_id,
									cgp.is_allowed
								FROM
									company_users cu
									JOIN view_company_user_properties vcup ON ( vcup.cid = cu.cid AND vcup.company_user_id = cu.id )
									JOIN company_user_groups cug ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
									JOIN company_group_permissions cgp ON ( cgp.cid = cug.cid AND cgp.company_group_id = cug.company_group_id AND cgp.module_id < ' . ( int ) CModule::PLACEHOLDER_MODULE_THRESHOLD . ' AND cgp.is_allowed = 1 )
									JOIN modules m ON ( m.id = cgp.module_id AND m.is_published = 1 )
								WHERE
									cu.cid = ' . ( int ) $intCId . '
									AND cu.employee_portal_only != 1
									' . $strSqlGroupsCondition . '
									AND cu.default_company_user_id IS NULL
									AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
									AND vcup.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
									' . $strSqlSummarizeByCompanyGroup . $strSqlHideInactiveUsers . '
						 ) AS sub1
						WHERE
							1 = 1
							-- AND sub1.parent_module_id IS NULL
					) TEMP
					ORDER BY
						username ';

		if( true == $boolSummarizeBygroup ) {
			return fetchData( $strSql, $objClientDatabase );
		} else {
			return self::fetchCompanyUsers( $strSql, $objClientDatabase );
		}
	}

	public static function fetchNonAdminCompanyUsersWithNameByPropertyIdsByRouteIdByModuleIdByCid( $arrintPropertyIds, $intRouteId, $intModuleId, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valId( $intModuleId ) ) return NULL;

		$strSql = 'SELECT
						cu.id,
						cu.username,
						COALESCE ( ce.name_first, NULL ) AS name_first,
						COALESCE ( ce.name_last, NULL ) AS name_last
					FROM
						company_users cu
						JOIN company_user_permissions cuper ON ( cuper.company_user_id = cu.id AND cuper.cid = cu.cid AND cuper.module_id = ' . ( int ) $intModuleId . ' )
						LEFT JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
						LEFT JOIN (
									SELECT
										cug.cid,
										cug.company_user_id,
										COUNT( cug.company_group_id ) AS grp_count
									FROM
										company_user_groups cug
										JOIN company_group_permissions cgp ON ( cgp.cid = cug.cid AND cgp.company_group_id = cug.company_group_id )
									WHERE
										cug.cid = ' . ( int ) $intCid . '
										AND cgp.module_id = ' . ( int ) $intModuleId . '
										AND cgp.is_allowed = 1
									GROUP BY
										cug.cid,
										cug.company_user_id

						) cug1 ON ( cug1.cid = cu.cid AND cug1.company_user_id = cu.id )
					WHERE
						cu.is_administrator = 0
						AND cu.is_disabled = 0
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL

						AND ( cuper.is_allowed = 1 OR ( cuper.is_inherited = 1 AND 0 < cug1.grp_count ) )

						AND EXISTS (
								SELECT 1 FROM
									view_company_user_properties cup
									JOIN property_routes pr ON ( pr.cid = cup.cid AND pr.property_id = cup.property_id )
									JOIN routes r ON ( r.cid = pr.cid AND r.id = pr.route_id AND r.is_published = 1 AND r.id = ' . ( int ) $intRouteId . ' )
								WHERE
									cup.cid = ' . ( int ) $intCid . '
									AND cup.company_user_id = cu.id
									AND cup.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						)
					GROUP BY
						cu.id,
						cu.username,
						ce.name_first,
						ce.name_last
					ORDER BY
						LOWER ( ce.name_first || ce.name_last ) ASC;';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchAllActiveNonPsiCompanyUsersByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cu.id,
						CONCAT(ce.name_first, \' \', ce.name_last) as name
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND ( ce.name_first IS NOT NULL OR ce.name_last IS NOT NULL )
						AND cu.deleted_on IS NULL
						AND cu.is_disabled = 0
					ORDER BY
						name';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCompanyUsersByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						cu.id,
						ce.email_address,
						ce.name_first,
						ce.name_last,
						cu.username,
						cu.company_user_type_id
					FROM
						company_users cu
						LEFT JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid)
					WHERE
						cu.id = ' . ( int ) $intId . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPSIUsersByCidByIsAdministrator( $intCid, $intIsAdmininstrator, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						company_users
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_administrator = ' . ( int ) $intIsAdmininstrator . '
						AND company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )';
		return self::fetchCompanyUsers( $strSql, $objDatabase );
	}

	public static function fetchAllCompanyUsersByNameByCid( $strCompanyUserName, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cu.*,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.default_company_user_id IS NULL
						AND cu.is_disabled <> 1
						AND cu.username <> \'PS Admin\'
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND ( ce.name_first IS NOT NULL OR ce.name_last IS NOT NULL )
						AND cu.deleted_on IS NULL
						AND ( name_first ILIKE \'%' . trim( addslashes( $strCompanyUserName ) ) . '%\'
								OR name_last ILIKE \'%' . trim( addslashes( $strCompanyUserName ) ) . '%\' )
					ORDER BY ce.name_first';

		return self::fetchCompanyUsers( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserNameByIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.id = ' . ( int ) $intCompanyUserId;

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		return $arrstrData[0]['name_first'] . ' ' . $arrstrData[0]['name_last'];
	}

	public static function updatePsiCompanyUsersIsAdministratorFlag( $arrintSuperUsers, $intCid, $intIsAdmininistrator, $objDatabase ) {
		$strSql = 'UPDATE
						company_users
					SET
						is_administrator = ' . ( int ) $intIsAdmininistrator . '
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' ) ';

		if( true == valArr( $arrintSuperUsers ) ) {
			$strSql .= ' AND id NOT IN ( ' . implode( ',', $arrintSuperUsers ) . ' )';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUsersByPropertyGroupIdByByCid( $intPropertyGroupId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cu.*
					FROM
						company_users cu
						JOIN company_user_property_groups cupg ON ( cupg.cid = cu.cid AND cupg.company_user_id = cu.id )
						JOIN property_groups pg ON ( pg.cid = cupg.cid AND cupg.property_group_id = pg.id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cupg.property_group_id = ' . ( int ) $intPropertyGroupId . '
						AND pg.deleted_by IS NULL
						AND pg.deleted_on IS NULL';

		return self::fetchCompanyUsers( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserDetailsByIdByCid( $intCompanyUserId, $intCid, $arrstrSelectFields, $objDatabase ) {

		$strSql = ' SELECT ' .
						implode( ', ', $arrstrSelectFields ) . '
					FROM
						company_users cu
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
						AND cu.id = ' . ( int ) $intCompanyUserId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUsersByCidBySearchKeywords( $intCid, $arrstrFilteredExplodedSearch, $objDatabase ) {

		$arrstrAdvancedSearchParameters	= [];

		$strSql = 'SELECT
						cu.id,
						cu.cid,
						cu.company_employee_id,
						COALESCE( ce.name_first, NULL ) AS name_first,
						COALESCE( ce.name_last, NULL ) AS name_last,
						cu.username,
						cu.is_administrator,
						cu.is_disabled
					FROM
						company_users cu
						LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ( ce.cid = cu.cid AND cu.default_company_user_id IS NULL ) )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.default_company_user_id IS NULL';

		foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
			if( '' != $strKeywordAdvancedSearch ) {
				array_push( $arrstrAdvancedSearchParameters, 'ce.name_first  ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				array_push( $arrstrAdvancedSearchParameters, 'ce.name_last ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
			}
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		$strSql .= ' ORDER BY LOWER( ce.name_first || ce.name_last ) ASC';

		$arrstrSearchResults = ( array ) fetchData( $strSql, $objDatabase );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUsersByCompanyEmployeeIdByCidByDefaultCompanyDepartmentId( $intCompanyUserId, $intCid, $intDefaultCompanyDepartmentId, $objDatabase ) {

		$strSql = 'SELECT
					    cu.*
					FROM
					    company_users cu
					    JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ( ce.cid = cu.cid AND cu.default_company_user_id IS NULL ) )
					WHERE
					    cu.id = ' . ( int ) $intCompanyUserId . '
					    AND cu.cid = ' . ( int ) $intCid . '
					    AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
					    AND cu.default_company_user_id IS NULL
					    AND EXISTS (
					                 SELECT
					                     1
					                 FROM
					                     company_departments cd
					                     JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
					                 WHERE
					                     ced.cid = ' . ( int ) $intCid . '
					                     AND ced.company_employee_id = ce.id
					                     AND cd.default_company_department_id = ' . ( int ) $intDefaultCompanyDepartmentId . '
					    )';

		$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

		return $arrstrData[0];
	}

	public static function fetchAllCompanyUsersWithApprovedSignerPreferenceKeyByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT *
					FROM
						company_users cu
						JOIN company_user_preferences cup ON ( cu.id = cup.company_user_id AND cu.is_disabled = 0 AND cu.cid = cup.cid )
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cup.key = \'' . CCompanyUserPreference::APPROVED_SIGNER . '\'
						AND cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.default_company_user_id IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultUserResidentFriendlyPreferenceByUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT COALESCE( bool_or(
			CASE
			    WHEN ( cup.value = \'1\') THEN TRUE
                WHEN (cup.value = \'2\') THEN 
                    cgp.value = \'1\'  -- RETURNS a boolean value
                ELSE FALSE
	        END
			), FALSE ) as final_default_value
			FROM 
				company_users cu
				LEFT JOIN company_user_preferences cup ON ( cup.cid = cu.cid AND cup.company_user_id = cu.id AND cup.key = \'DEFAULT_TO_RESIDENT_FRIENDLY\' )
				LEFT JOIN company_user_groups cug ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
				LEFT JOIN company_group_preferences cgp ON ( cgp.cid = cug.cid AND cgp.company_group_id = cug.company_group_id AND cgp.key = \'DEFAULT_TO_RESIDENT_FRIENDLY\' )
			WHERE cu.cid = ' . ( int ) $intCid . ' AND 
				cu.id = ' . ( int ) $intCompanyUserId;

		$arrstrTempResult = fetchData( $strSql, $objDatabase )[0];
		if( isset( $arrstrTempResult['final_default_value'] ) ) {
			return 't' == $arrstrTempResult['final_default_value'];
		} else {
			return false;
		}
	}

	/**
	 * @param array $arrintCompanyUserIds
	 * @param array $arrintPropertyIds
	 * @param       $strCompanyUserPreferenceByKey
	 * @param       $intCid
	 * @param       $objDatabase
	 * @param bool  $boolIsSmsChat
	 * @return array
	 */
	public static function fetchCompanyUsersByPropertyIdByCompanyUserPreferenceKeyByCompanyUserIdsByCid( array $arrintPropertyIds, $strCompanyUserPreferenceByKey, $intCid, $objDatabase, array $arrintCompanyUserIds = [], $boolIsSmsChat = false, $boolIsCallNotifications = false ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return [];
		}

		$strWhereSql = '';
		if( true == valArr( $arrintCompanyUserIds ) ) {
			$strWhereSql = ' AND cu.id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )';
		}

		$strSql = 'with temp_company_preference AS( 
						WITH tmp_user_group_preferences AS ( 
						 SELECT
							cu.id AS company_user_id,
							cu.cid,
							cgp.key,
							cgp.value AS value
						FROM
							company_users cu
							JOIN company_user_groups AS cug ON ( cu.cid = cug.cid AND cu.id = cug.company_user_id )
							LEFT JOIN company_group_preferences AS cgp ON ( cug.cid = cgp.cid AND cug.company_group_id = cgp.company_group_id )
						WHERE
							cu.cid = ' . ( int ) $intCid . '
							AND cgp.key = \'' . $strCompanyUserPreferenceByKey . '\'
							' . $strWhereSql . ' 
						GROUP BY
							cu.id,
							cu.cid,
							cgp.key,
							cgp.value), tmp_user_company_preferences AS (    
						SELECT
							cu.id AS company_user_id,
							cu.cid,
							cup.key,
							cup.value
						FROM
							company_users cu
							JOIN company_user_preferences cup ON ( cu.cid = cup.cid AND cu.id = cup.company_user_id )
						WHERE
							cu.cid = ' . ( int ) $intCid . '
							AND cup.key = \'' . $strCompanyUserPreferenceByKey . '\'
							' . $strWhereSql . ' )
						SELECT
							*
						FROM
							tmp_user_company_preferences
						UNION
						SELECT
							*
						FROM
							tmp_user_group_preferences
						WHERE
							company_user_id NOT IN (
												SELECT
														company_user_id
												FROM
														tmp_user_company_preferences
						))
														SELECT
															property_id,
															string_agg ( DISTINCT id::TEXT, \',\' ) AS company_user_ids
														FROM
														(
															SELECT
																vcup.property_id,
																cu.id
															FROM
																company_users cu
																LEFT JOIN temp_company_preference tcp ON tcp.cid = cu.cid AND tcp.company_user_id = cu.id
																JOIN view_company_user_properties vcup ON vcup.cid = cu.cid AND vcup.company_user_id = cu.id
															WHERE
																cu.cid = ' . ( int ) $intCid . '
																AND cu.is_disabled <> 1
																' . $strWhereSql . '
																AND tcp.key = \'' . $strCompanyUserPreferenceByKey . '\'
																AND tcp.value = \'1\'
																AND cu.is_administrator = 0
																AND vcup.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
															UNION
															SELECT
																p.property_id,
																cu.id
															FROM
																company_users cu
																LEFT JOIN temp_company_preference tcp ON tcp.cid = cu.cid AND tcp.company_user_id = cu.id
																JOIN 
																(
																	SELECT
																		unnest ( ARRAY [' . implode( ',', $arrintPropertyIds ) . '] ) AS property_id
																) p ON TRUE
															WHERE
																cu.cid = ' . ( int ) $intCid . '
																AND cu.is_disabled <> 1
																' . $strWhereSql . '
																AND tcp.key =  \'' . $strCompanyUserPreferenceByKey . '\'
																AND tcp.value = \'1\'
																AND cu.is_administrator = 1
															) AS TEMP
														GROUP BY
															property_id';

		return fetchData( $strSql, $objDatabase );
	}

	// use this function to fetch combined value set in user preference and group preference for that particular user, currently DEFAULT_TO_RESIDENT_FRIENDLY and DEFAULT_LEDGER_TO_SHOW_REVERSALS

	public static function fetchUserAndGroupPreferenceCombinedByPreferenceKeyByUserIdByCid( $strPreferenceKey, $intCompanyUserId, $intCid, $objDatabase ) {

		if( false == valStr( $strPreferenceKey ) || false == valId( $intCompanyUserId ) || false == valId( $intCid ) ) {
			return false;
		}

		$strSql = 'SELECT COALESCE( bool_or(
			CASE
			    WHEN ( cup.value = \'1\') THEN TRUE
                WHEN (cup.value = \'2\') THEN 
                    cgp.value = \'1\'  -- RETURNS a boolean value
                ELSE FALSE
	        END
			), FALSE ) as final_default_value
			FROM 
				company_users cu
				LEFT JOIN company_user_preferences cup ON ( cup.cid = cu.cid AND cup.company_user_id = cu.id AND cup.key = \'' . ( string ) $strPreferenceKey . '\' )
				LEFT JOIN company_user_groups cug ON ( cug.cid = cu.cid AND cug.company_user_id = cu.id )
				LEFT JOIN company_group_preferences cgp ON ( cgp.cid = cug.cid AND cgp.company_group_id = cug.company_group_id AND cgp.key = \'' . ( string ) $strPreferenceKey . '\' )
			WHERE cu.cid = ' . ( int ) $intCid . ' AND 
				cu.id = ' . ( int ) $intCompanyUserId;

		$arrstrTempResult = fetchData( $strSql, $objDatabase )[0];
		if( isset( $arrstrTempResult['final_default_value'] ) ) {
			return 't' == $arrstrTempResult['final_default_value'];
		} else {
			return false;
		}
	}

	public static function fetchCompanyUsersByCidByModuleId( $intCid, $intModuleId, $objDatabase ) {
		$intAllPropertyGroupId = ( $intCid + 100000000 );

		$strSql = 'WITH user_permissions AS (
						SELECT
							m.id AS module_id,
							m.parent_module_id,
							cup1.cid,
							cup2.company_user_id,
							cup1.is_allowed AS p_is_allowed,
							cup2.is_allowed,
							cup2.is_inherited
						FROM
							modules m
							JOIN company_user_permissions cup1 ON cup1.module_id = m.parent_module_id AND cup1.is_allowed = 1
							LEFT JOIN company_user_permissions cup2 ON cup2.module_id = m.id AND cup2.cid = cup1.cid AND cup2.company_user_id = cup1.company_user_id
						WHERE
							m.id = ' . ( int ) $intModuleId . '
							AND cup1.cid = ' . ( int ) $intCid . '
					),
					group_permissions AS (
					
						SELECT
							cug.CID,
							cug.company_user_id
						FROM
							(
							  SELECT
									cug.cid,
									cug.company_user_id,
									cgp2.module_id,
									max ( cgp2.is_allowed ) AS is_allowed,
									max ( cgp1.is_allowed ) AS p_is_allowed
							  FROM
									company_user_groups cug
									JOIN modules m ON m.id < 100000
									JOIN company_group_permissions cgp1 ON cgp1.module_id = m.parent_module_id AND cgp1.cid = cug.cid AND cgp1.company_group_id = cug.company_group_id AND cgp1.is_allowed = 1
									LEFT JOIN company_group_permissions cgp2 ON cgp2.module_id = m.id AND cgp2.cid = cgp1.cid AND cgp2.company_group_id = cug.company_group_id
							  WHERE
								  cug.cid = ' . ( int ) $intCid . '
								  AND cgp2.module_id = ' . ( int ) $intModuleId . '
								  AND cgp2.is_allowed = 1
							  GROUP BY
								  cug.cid,
								  cug.company_user_id,
								  cgp2.module_id
							) AS cug
							LEFT JOIN company_user_permissions cup ON cup.cid = cug.cid AND cup.company_user_id = cug.company_user_id AND cup.module_id = cug.module_id
						WHERE
							cug.cid = ' . ( int ) $intCid . '
							AND COALESCE ( cup.is_inherited, 1 ) = 1
					),
					prop_list AS (  
					  SELECT
						  lp.cid,
						  lp.property_id
					  FROM
						  product_modules pm
						  JOIN load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . ( int ) $intAllPropertyGroupId . ' ], ARRAY [ pm.ps_product_id ], TRUE ) lp ON TRUE
					  WHERE
						  lp.cid = ' . ( int ) $intCid . '
						  AND pm.module_id = ' . ( int ) $intModuleId . '
						  AND pm.ps_product_id IS NOT NULL
					),
					users_with_permissions AS (
						SELECT
							CID,
							company_user_id
						FROM
							user_permissions up
						WHERE
							up.cid = ' . ( int ) $intCid . '
							AND up.is_allowed = 1
						UNION
						SELECT
							cug.cid,
							cug.company_user_id
						FROM
							group_permissions cug
						WHERE
							cug.cid = ' . ( int ) $intCid . '
					)
					
					SELECT
						cu.id,
						cu.cid,
						cu.username,
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON ce.cid = cu.cid AND ce.id = cu.company_employee_id
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.company_user_type_id NOT IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' ) 
						AND cu.is_disabled = 0
						AND CASE
							  WHEN cu.is_administrator <> 1 THEN EXISTS (
																		  SELECT
																			  1
																		  FROM
																			  prop_list pl
																			  JOIN view_company_user_properties vcup ON pl.cid = vcup.cid AND pl.property_id = vcup.property_id
																		  WHERE
																			  vcup.company_user_id = cu.id
						)
						AND EXISTS (
									 SELECT
										 1
									 FROM
										 users_with_permissions up
									 WHERE
										 up.cid = cu.cid
										 AND up.company_user_id = cu.id
						)
							  ELSE EXISTS (
											SELECT
												1
											FROM
												prop_list pl
											WHERE
												pl.cid = cu.cid
											LIMIT
												1
						)
						END
					ORDER BY
						ce.name_first, 
						ce.name_last';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUsersByCompanyUserPreferenceByPropertyId( $intPropertyId, $strCompanyUserPreferenceKey, $intCid, $objDatabase, $intCompanyUserId = NULL ) {

		$strWhereSql = '';
		if( false == is_null( $intCompanyUserId ) ) {
			$strWhereSql = ' AND cu.id <> ' . ( int ) $intCompanyUserId;
		}

		$strSql = 'SELECT
						cu.id AS company_user_id
					FROM
						company_users cu
						JOIN company_user_preferences cup ON ( cu.cid = cup.cid AND cu.id = cup.company_user_id )
						JOIN view_company_user_properties vcup ON ( cu.cid = vcup.cid AND cu.id = vcup.company_user_id )
						JOIN property_preferences pp ON ( cu.cid = pp.cid AND pp.property_id = vcup.property_id )
					WHERE
						cu.cid = ' . ( int ) $intCid . '
						AND cu.is_disabled <> 1
						AND pp.property_id = ' . ( int ) $intPropertyId . '
						AND pp.value = \'1\'
						AND cup.key = \'' . $strCompanyUserPreferenceKey . '\'
						AND cup.value = \'1\'' . $strWhereSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertySecondaryCompanyUserByPropertyId( $intClientId, $intPropertyId, $objDatabase ) {

		if( false == valId( $intClientId ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						cu.id,
						ce.name_first,
						ce.name_last
					FROM
						ips_property_company_users AS ipcu
						JOIN company_users AS cu ON ( cu.id = ANY( ipcu.secondary_company_user_ids ) )
						JOIN company_employees AS ce ON ( cu.company_employee_id = ce.id )
					WHERE
						ipcu.cid = ' . ( int ) $intClientId . '
						AND ipcu.property_id = ' . ( int ) $intPropertyId;

		return self::fetchCompanyUsers( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserByIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase, $intCheckActiveDirectoryUser = 1 ) {

		$strSubSql = ' AND cu.is_active_directory_user = 1';

		$strSql = 'SELECT
						cu.*,
						ce.email_address, 
						ce.name_first,
						ce.name_last
					FROM
						company_users cu
						JOIN company_employees ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE
						cu.cid = ' . ( int ) $intCid . ( $intCheckActiveDirectoryUser ? $strSubSql : '' ) . '
					    AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA;

		if( true == valArr( $arrintCompanyUserIds ) ) {
			$strSql .= ' AND cu.id IN (' . implode( ',', $arrintCompanyUserIds ) . ')';
		}

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchDocVerifiedCompanyUsersByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objClientDatabase ) {
		if( false == ( $arrintCompanyUserIds = getIntValuesFromArr( $arrintCompanyUserIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						company_users
					WHERE
						id IN ( ' . sqlIntImplode( $arrintCompanyUserIds ) . ')
						AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserDataByNameFirstByNameLast( $strNameFirst, $strNameLast, $intCid, $objDatabase ) {
		$strSql = 'SELECT cu.*
						FROM
							company_users cu
							JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
						WHERE
							lower( ce.name_first ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameFirst ) ), 50, NULL, true ) . '\'
							AND lower( ce.name_last ) = \'' . CStrings::strTrimDef( \Psi\CStringService::singleton()->strtolower( addslashes( $strNameLast ) ), 50, NULL, true ) . '\'
							AND cu.cid = ' . ( int ) $intCid . '
							AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
							AND cu.default_company_user_id IS NULL
						LIMIT 1';

		return self::fetchCompanyUser( $strSql, $objDatabase );
	}

	public static function getCompanyUserDepartmentsTempTable( $intCid ) {

		$strSql = ' DROP TABLE IF EXISTS company_user_departments_temp;
						CREATE TEMP TABLE company_user_departments_temp as (  
						           SELECT
						               cd.cid,
						               ced.company_employee_id,
						               string_agg ( util_get_translated( \'name\', cd.name, cd.details ), \', \' ) AS departments
						           FROM
						               company_departments cd
						               JOIN company_employee_departments ced ON ( cd.cid = ced.cid AND cd.id = ced.company_department_id )
						               JOIN company_users cu ON ( cu.company_employee_id = ced.company_employee_id AND cu.cid = ced.cid )
						           WHERE
						               ced.cid = ' . ( int ) $intCid . '
						           GROUP BY
						               cd.cid,
						               ced.company_employee_id
						 );
						ANALYZE company_user_departments_temp;
						CREATE INDEX idx_cud_company_employee_id_cid ON company_user_departments_temp ( cid, company_employee_id ); ';
		return $strSql;
	}

	public static function fetchCompanyUserPropertiesByCompanyUserId( $intCid, $arrintCompanyUserIds, $objClientDatabase ) {

		if( false == valArr( $arrintCompanyUserIds ) ) {
			return [];
		}

		$strLabel = __( 'Multiple' );

		$strSql = 'SELECT
					    cupg.company_user_id,
					    CASE
					      WHEN count ( DISTINCT pga.id ) > 1 THEN \'' . $strLabel . '\'
					      ELSE 
					    (
					      SELECT
					          property_name
					      FROM
					          properties p
					      WHERE
					          p.id = max ( pga.property_id )
					    )
					    END AS property_name
					FROM
					    property_groups pg
					    JOIN company_user_property_groups cupg ON ( pg.deleted_on IS NULL AND pg.cid = cupg.cid AND cupg.property_group_id = pg.id )
					    JOIN property_group_associations pga ON ( cupg.cid = pga.cid AND cupg.property_group_id = pga.property_group_id )
					WHERE
					    pg.cid = ' . ( int ) $intCid . '
					    AND cupg.company_user_id IN (  ' . implode( ',', $arrintCompanyUserIds ) . ' )
					GROUP BY
					    cupg.company_user_id ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUserDetailsByCidByCompanyUserTypeId( $intCid, $intCompanyUserTypeId, $arrmixFilters, $arrmixPagination, $arrmixOtherDetails, $objClientDatabase ) {

		$strWhere = '';
		$strCondition = '';

		$boolIsSelectAllUsers = ( true == valArr( $arrmixOtherDetails ) ) ? $arrmixOtherDetails['is_select_all_users'] : false;
		$intOffset = ( 0 < $arrmixPagination['page_number'] ) ? $arrmixPagination['page_size'] * ( $arrmixPagination['page_number'] - 1 ) : 0;
		$intLimit  = ( int ) $arrmixPagination['page_size'];
		if( false == $boolIsSelectAllUsers ) {
			$strCondition .= ' OFFSET ' . ( int ) $intOffset;
			$strCondition .= ' LIMIT ' . ( int ) $intLimit;
		}
		$strOrderBy = 'LOWER(' . $objClientDatabase->getCollateSort( 'name_full' ) . ') asc';

		if( true == valStr( $arrmixPagination['sort_by'] ) ) {
			$strOrderByField = \Psi\CStringService::singleton()->strstr( $arrmixPagination['sort_by'], ' ', true );
			$strOrder = \Psi\CStringService::singleton()->strstr( $arrmixPagination['sort_by'], ' ' );
			$strOrderBy = $objClientDatabase->getCollateSort( $strOrderByField ) . $strOrder . ( false === \Psi\CStringService::singleton()->strpos( $arrmixPagination['sort_by'], 'name_full' ) ? ',' . $objClientDatabase->getCollateSort( 'name_full' ) : '' );
		}

		if( 0 == $arrmixFilters['is_disabled'] ) {
			$strWhere .= ' AND cu.is_disabled = 0';
		} elseif( 1 == $arrmixFilters['is_disabled'] ) {
			$strWhere .= ' AND cu.is_disabled = 1';
		}
		$arrintPropertyIds     = getArrayElementByKey( 'property_group_ids', $arrmixFilters );
		$objCompanyUser        = getArrayElementByKey( 'company_user', $arrmixFilters );
		$strUserName           = getArrayElementByKey( 'username', $arrmixFilters );
		$arrintCompanyGroupIds = getArrayElementByKey( 'company_group_ids', $arrmixFilters );

		if( true == valStr( $strUserName ) ) {
			$strWhere .= ' AND (
								lower( name_first || \' \' || name_last ) LIKE lower( \'%' . $strUserName . '%\' )
								OR lower( name_last || \' \' || name_first ) LIKE lower( \'%' . $strUserName . '%\' )
								OR lower( username ) LIKE lower( \'%' . $strUserName . '%\' )
							)';
		}
		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) && false == $objCompanyUser->getIsAdministrator() && false == $objCompanyUser->getIsPSIUser() ) {
			$strWhere .= ' AND cu.id != ' . ( int ) $objCompanyUser->getId() . ' AND cu.is_administrator = 0 ';
		}

		if( true == valArr( $arrintCompanyGroupIds ) ) {
			$strWhere .= ' AND ( EXISTS(
                                SELECT
                                    company_user_id
                                FROM
                                    company_user_groups cug
					            WHERE
							        cug.company_user_id = cu.id
							        AND cug.cid = cu.cid
							        AND cug.company_group_id IN( ' . implode( ',', $arrintCompanyGroupIds ) . ' )
                            ) )';
		}
	if( true == valArr( $arrintPropertyIds ) ) {
		$strWhere .= ' AND ( cu.is_administrator = 1
			                  OR ( EXISTS ( 
			                              SELECT
                                              1
                                          FROM
                                              load_properties ( ARRAY [ ' . ( int ) $intCid . ' ], ARRAY [ ' . implode( ',', $arrintPropertyIds ) . '  ], NULL, ' . ( ( false == getArrayElementByKey( 'show_disabled_data', $arrmixFilters ) ) ? 'TRUE' : 'FALSE' ) . ' ) lp
                                              JOIN view_company_user_properties vcup ON lp.cid = vcup.cid AND lp.property_id = vcup.property_id
                                          WHERE
                                              lp.cid = cu.cid
                                              AND vcup.company_user_id = cu.id
                                          LIMIT
                                              1
		                  ) ) ) ';
	}

		$arrstrEmployeeRoles = [ 'admin' => __( 'Admin' ), 'employee' => __( 'Employee' ), 'non_employee' => __( 'Non-Employee' ) ];
		$strSql  = '';
		$strSql .= self::getCompanyUserDepartmentsTempTable( $intCid );

		$strSql .= ' SELECT
					    u.*,
					    d.departments
					 FROM
					    (
					      SELECT
					          *
					      FROM
			                 (
						     SELECT
						         cu.id as company_user_id,
						         cu.cid,
						         ce.id as company_employee_id,
						         COALESCE(ce.name_first || \' \' || ce.name_last, ce.name_first, ce.name_last) AS name_full,
						         cu.username,
						         cu.is_administrator,
						         ce.is_employee,
						         cu.is_disabled,
						         ce.remote_primary_key as employee_remote_primary_key,
						         cu.is_active_directory_user,
						         CASE
						           WHEN cu.is_administrator = 1 THEN \'' . $arrstrEmployeeRoles['admin'] . '\'
						           WHEN ce.is_employee = 1 THEN \'' . $arrstrEmployeeRoles['employee'] . '\'
						           ELSE \'' . $arrstrEmployeeRoles['non_employee'] . '\'
						         END AS user_type
						     FROM
						         company_employees ce
						         JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
						     WHERE
						         cu.cid =  ' . ( int ) $intCid . $strWhere . '
						         AND company_user_type_id = ' . ( int ) $intCompanyUserTypeId . '
						         AND cu.default_company_user_id IS NULL
						         AND cu.company_user_type_id NOT IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						         AND cu.username NOT LIKE \'PS Admin%\'
						         AND cu.deleted_on IS NULL
						   ) u
							ORDER BY
								' . $strOrderBy . $strCondition . '
		                ) u
					    LEFT JOIN company_user_departments_temp d ON u.cid = d.cid AND u.company_employee_id = d.company_employee_id
					    WHERE
							u.cid = ' . ( int ) $intCid . '
						ORDER BY
						' . $strOrderBy;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchLoggedInCompanyUsersByCid( $intCid, $objClientDatabase ) {
		$strSql = 'SELECT
						id as company_user_id
					FROM
						company_users
					WHERE
						cid = ' . ( int ) $intCid . '
						AND last_login::date = CURRENT_DATE
						AND company_user_type_id NOT IN ( ' . implode( ' ,', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						AND is_disabled = 0';

		return ( array ) fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomCompanyUsersByCidByIds( $intCid, $arrintCompanyUserIds, $objClientDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					    id,company_user_type_id
					FROM
					    company_users
					WHERE
					    id IN ( ' . sqlIntImplode( $arrintCompanyUserIds ) . ' )
					    AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersByCompanyEmployeeIdsByCid( $arrintCompanyEmployeeIds, $intCid, $objDatabase ) : array {
		$arrintCompanyEmployeeIds = array_filter( $arrintCompanyEmployeeIds );
		if( false === valArr( $arrintCompanyEmployeeIds ) || false === valId( $intCid ) ) {
			return [];
		}

		$strSql = '	SELECT
						*
					FROM
						company_users
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_employee_id IN ( ' . implode( ',', $arrintCompanyEmployeeIds ) . ' )';

		return ( array ) self::fetchCompanyUsers( $strSql, $objDatabase );
	}

	public static function fetchActiveCompanyUsersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = ' SELECT 
						cu.id as company_user_id,
                        cu.cid,
                        ce.id as company_employee_id,
                        COALESCE(ce.name_first || \' \' || ce.name_last) AS name_full
					FROM 
						company_employees ce
                        JOIN company_users cu ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					WHERE 
						cu.cid = ' . ( int ) $intCid . '
                        AND cu.is_disabled = 0
						AND ( cu.is_administrator = 1 OR ( 
						EXISTS ( 
							SELECT 1 
							FROM
                                view_company_user_properties vcup
                            WHERE 
                                vcup.cid = cu.cid 
                                AND vcup.property_id = ' . ( int ) $intPropertyId . '
                                AND vcup.company_user_id = cu.id 
							LIMIT 1
                        )))
                        AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
                        AND cu.default_company_user_id IS NULL
                        AND cu.username NOT LIKE \'PS Admin%\'
                        AND cu.deleted_on IS NULL
					ORDER BY
						name_full ASC';

		$arrmixResults = fetchData( $strSql, $objDatabase );
		$arrmixResults = rekeyArray( 'company_user_id', $arrmixResults );
		return $arrmixResults;
	}

	public static function fetchUserPropertyDetailsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = ' SELECT 
						ce.name_first || \' \' || ce.name_last as name,
						cu.username,
						CASE 
							WHEN cu.is_disabled = 1 THEN \'YES\'
							ELSE \'NO\'
						END AS is_disabled,
						string_agg( p.property_name, \', \' order by p.property_name ) as properties
					FROM 
						company_user_property_groups cupg 
						JOIN company_users cu ON ( cupg.company_user_id = cu.id AND cupg.cid = cu.cid ) 
						JOIN properties p ON ( cupg.property_group_id = p.id and cupg.cid = p.cid ) 
						JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ce.cid = cu.cid )
					WHERE 
						cupg.cid = ' . ( int ) $intCid . '
						AND cupg.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND EXISTS (
						           SELECT
						               1
						           FROM
						               properties p
						           WHERE
						               p.id = cupg.property_group_id
						               AND p.cid = cupg.cid
						               AND p.is_disabled = 0
						)
						AND cu.company_user_type_id NOT IN ( ' . implode( ', ', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' ) 
					GROUP BY
						cu.username,
						ce.name_first,
						ce.name_last,
						cu.is_disabled
					ORDER BY
						cu.username,
						cu.is_disabled ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function dissociateUsersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $intCompanyUserId, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) || false == valId( $intCompanyUserId ) ) {
			return NULL;
		}
		$strSql = ' with deleted_data as ( 
						DELETE
						FROM 
							company_user_property_groups cupg
						WHERE 
							cupg.cid = ' . ( int ) $intCid . '
							AND cupg.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND EXISTS (
							           SELECT
							               1
							           FROM
							               properties p
							           WHERE
							               p.id = cupg.property_group_id
							               AND p.cid = cupg.cid
							               AND p.is_disabled = 0
							)  
						RETURNING 
							cid,
							company_user_id,
							property_group_id
					)	
					
					INSERT INTO table_logs
					SELECT
						nextval ( \'table_logs_id_seq\'::regclass ) AS id,
					    dd.cid,
					    \'company_user_property_groups\' AS table_name,
					    dd.company_user_id AS reference_number,
					    \'UPDATE\' AS action,
					    \'Deleted association for disabled properties by system\' AS description,
					    \'Property/Property Groups::\' AS old_data,
					    \'Property/Property Groups::Removed: \' || string_agg ( pg.name, \', \' ORDER BY pg.name ) AS new_data,
					    ' . ( int ) $intCompanyUserId . ' AS created_by,
					    NOW() AS created_on
				    FROM
					    deleted_data dd
				        JOIN property_groups pg ON pg.cid = dd.cid AND pg.id = dd.property_group_id
				        LEFT JOIN company_users cu ON cu.id = dd.company_user_id and cu.cid = dd.cid
				    WHERE
					    cu.company_user_type_id NOT IN ( ' . implode( ', ', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
			        GROUP BY
					    dd.cid,company_user_id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNonKeyedCompanyUsersByIdsByCids( $arrintIds, $arrintCids, $objClientDatabase ) {
		if( false == valArr( $arrintIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    id IN ( ' . implode( ',', $arrintIds ) . ' )
					    AND cid IN ( ' . implode( ',', $arrintCids ) . ' )
					    AND company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) ';

		return self::fetchCompanyUsers( $strSql, $objClientDatabase, false );
	}

	public static function fetchCompanyUserEmailByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						ce.email_address
					FROM 
						company_users cu, company_employees ce 
					WHERE 
						cu.company_employee_id = ce.id 
						AND cu.cid = ce.cid 
						AND cu.id = ' . ( int ) $intCompanyUserId . ' 
						AND cu.cid = ' . ( int ) $intCid . ' 
						AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . '
						AND cu.is_disabled = 0';

		$arrmixResult = fetchData( $strSql, $objDatabase );
		return $arrmixResult[0]['email_address'] ?? NULL;
	}

	public static function fetchCompanyUserByReferenceIdByCompanyUserTypeId( $intReferenceId, $intCid, $intCompanyUserTypeId, $objClientDatabase ) {
		if( false == valId( $intReferenceId ) || false == valId( $intCid ) || false == valId( $intCompanyUserTypeId ) ) {
			return NULL;
		}

		$strSql = sprintf( 'SELECT
					    *
					FROM
					    company_users
					WHERE
					    reference_id = %d
					    AND cid = %d
					    AND company_user_type_id = %d LIMIT 1', ( int ) $intReferenceId, ( int ) $intCid, ( int ) $intCompanyUserTypeId );

		return self::fetchCompanyUser( $strSql, $objClientDatabase );
	}

	public static function fetchCompanyUsersByUsernamesByCid( $arrstrUsername, $intCid, $objClientDatabase, $intCompanyUserTypeId = CCompanyUserType::ENTRATA ) {
		if( false == valArr( $arrstrUsername ) ) {
			return false;
		}
		$strSql = 'SELECT
					    id,
					    cid,
					    username,
					    company_user_type_id
					FROM
					    company_users
					WHERE
					    CID = ' . ( int ) $intCid . '
					    AND default_company_user_id IS NULL
					    AND company_user_type_id = ' . ( int ) $intCompanyUserTypeId . '
					    AND LOWER ( username )  IN ( \'' . implode( "','", $arrstrUsername ) . '\' )';
		return self::fetchCompanyUsers( $strSql, $objClientDatabase );
	}

}
?>