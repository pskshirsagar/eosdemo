<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountTrees
 * Do not add any new functions to this class.
 */

class CGlAccountTrees extends CBaseGlAccountTrees {

	public static function fetchCustomGlAccountTreesByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase, $objGlTreeFilter = NULL, $boolIsFromImport = false ) {

		$strOrderBy = 'gat.order_num';

		if( true == valObj( $objGlTreeFilter, 'CGlTreeFilter' ) && true == valStr( $objGlTreeFilter->getSortBy() ) ) {
			$strOrderBy = $objGlTreeFilter->getSortBy() . ' ' . $objGlTreeFilter->getSortDirection();
		}

		if( true == $boolIsFromImport ) {
			$strAccountConditions = 'REPLACE( gat.account_number, \' \', \'\' ) AS account_number,';
		} else {
			$strAccountConditions = 'gat.account_number AS account_number,';
		}

		$strSql = 'SELECT
						gat.id,
						gat.cid,
						gat.gl_tree_id,
						gat.gl_group_id,
						gat.gl_account_id,
						gat.gl_account_tree_id,
						gat.grouping_gl_account_id,
						gat.gl_account_type_id,
						gat.name,'
						. $strAccountConditions . '
						gat.formatted_account_number,
						gat.secondary_number,
						gat.description,
						gat.hide_if_zero,
						gat.is_system,
						gat.is_default,
						gat.hide_account_balance_from_reports,
						gat.order_num,
						gat.disabled_by,
						gat.disabled_on,
						gat.updated_by,
						gat.updated_on,
						gat.created_by,
						gat.created_on,
						ga.disabled_by,
						ga.gl_account_type_id,
						ga.gl_account_usage_type_id,
						ga.cash_flow_type_id,
						util_get_system_translated( \'name\', dgam.name, dgam.details ) AS system_name,
						dgam.account_number AS system_account_number,
						dgt.account_number_pattern AS system_account_number_pattern,
						dgt.account_number_delimiter AS system_account_number_delimiter,
						util_get_translated( \'name\', gg.name, gg.details ) AS group_location,
						util_get_system_translated( \'name\', glat.name, glat.details ) AS child_gl_type,
						gg.gl_account_type_id as gl_group_gl_account_type_id
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
						LEFT JOIN default_gl_account_masks dgam ON ( dgam.default_gl_account_id = ga.default_gl_account_id )
						LEFT JOIN default_gl_trees dgt ON ( dgt.id = dgam.default_gl_tree_id )
						LEFT JOIN gl_groups gg ON ( gg.cid = gat.cid AND gg.id = gat.gl_group_id )
						LEFT JOIN gl_account_types glat ON ( glat.id = gg.gl_account_type_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND ( gat.is_default = 1 OR gat.gl_account_tree_id IS NULL )
					ORDER BY ' . $strOrderBy;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlAccountTreesByGlAccountIdNotByIdByCid( $intGlAccountId, $intId, $intCid, $objClientDatabase, $boolShowParents = true ) {

		if( false == valId( $intGlAccountId ) || false == valId( $intId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gt.name AS gl_tree_name,
						gt.account_number_pattern,
						gt.account_number_delimiter,
						gat.cid,
						gat.gl_tree_id,
						gat.gl_account_type_id,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.disabled_by
							ELSE
								gat_parent.disabled_by
						END AS disabled_by,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.name
							ELSE
								gat_parent.name
						END AS name,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.account_number
							ELSE
								gat_parent.account_number
						END AS account_number,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.id
							ELSE
								gat_parent.id
						END AS id,
						NULL AS gl_account_tree_id,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.gl_group_id
							ELSE
								gat_parent.gl_group_id
						END AS gl_group_id,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.gl_account_id
							ELSE
								gat_parent.gl_account_id
						END AS gl_account_id,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.secondary_number
							ELSE
								gat_parent.secondary_number
						END AS secondary_number,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.description
							ELSE
								gat_parent.description
						END AS description,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.hide_if_zero
							ELSE
								gat_parent.hide_if_zero
						END AS hide_if_zero,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.is_system
							ELSE
								gat_parent.is_system
						END AS is_system,
						gat.is_default,
						gat.hide_account_balance_from_reports,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.order_num
							ELSE
								gat_parent.order_num
						END AS order_num,
						CASE
							WHEN gat.gl_account_tree_id IS NULL THEN
								gat.details
							ELSE
								gat_parent.details
						END AS details
					FROM
						gl_account_trees gat
						JOIN gl_trees gt ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
						LEFT JOIN gl_account_trees gat_parent ON ( gat.cid = gat_parent.cid AND gat.gl_account_tree_id = gat_parent.id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_account_id = ' . ( int ) $intGlAccountId . '
						AND gat.id <> ' . ( int ) $intId . '
						AND gt.deleted_by IS NULL
						AND gt.deleted_on IS NULL
					ORDER BY
						gat.id';

		if( false == $boolShowParents ) {

			$strSql = 'SELECT
					gt.name AS gl_tree_name,
					gt.account_number_pattern,
					gt.account_number_delimiter,
					gat.cid,
					gat.gl_tree_id,
					gat.gl_account_type_id,
					gat.grouping_gl_account_id,
					gat.disabled_by,
					gat.name,
					gat.account_number,
					gat.id,
					gl_account_tree_id,
					gat.gl_group_id,
					gat.gl_account_id,
					gat.secondary_number,
					gat.description,
					gat.hide_if_zero,
					gat.is_system,
					gat.is_default,
					gat.order_num,
					gat.hide_account_balance_from_reports,
					gat.details
				FROM
					gl_account_trees gat
					JOIN gl_trees gt ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
				WHERE
					gat.cid = ' . ( int ) $intCid . '
					AND gat.gl_account_id = ' . ( int ) $intGlAccountId . '
					AND gat.id <> ' . ( int ) $intId . '
					AND gt.deleted_by IS NULL
					AND gt.deleted_on IS NULL
				ORDER BY
					gat.id';
		}

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByGlAccountIdNotByIdByCid( $intGlAccountId, $intId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_account_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_account_id = ' . ( int ) $intGlAccountId . '
						AND id <> ' . ( int ) $intId;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreeByIdByCid( $intId, $intCid,  $objClientDatabase ) {

		$strSql = 'SELECT
						gt.name AS gl_tree_name,
						gt.account_number_pattern,
						gt.account_number_delimiter,
						gat.*,
						ga.gl_account_type_id,
						ga.cash_flow_type_id,
						ga.gl_account_usage_type_id
					FROM
						gl_account_trees gat
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id )
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
					WHERE
						gt.cid = ' . ( int ) $intCid . '
						AND gat.id = ' . ( int ) $intId . '
					ORDER BY
						gat.id';

		return self::fetchGlAccountTree( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByCidByGlTreeIdByGlGroupId( $intCid, $intGlTreeId, $intGlGroupId, $objClientDatabase, $intOrderNumber = NULL ) {

		$strCondition = '';

		if( false == is_null( $intOrderNumber ) && true == valId( $intOrderNumber ) ) {
			$strCondition = ' AND order_num >= ' . $intOrderNumber;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_account_trees
					WHERE
						cid =' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gl_group_id = ' . ( int ) $intGlGroupId . $strCondition . '
					ORDER BY
						LOWER( account_number )';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByCidByGlTreeIdByAccountNumber( $intCid, $intGlTreeId, $strAccountNumber, $objClientDatabase, $strFormattedAccountNumber = NULL ) {

		if( false == is_null( $strFormattedAccountNumber ) ) {
			$strConditions = " AND formatted_account_number LIKE '" . $strFormattedAccountNumber . "'";
		}

		$strSql = 'SELECT
						*
					FROM
						gl_account_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND REPLACE( account_number, \' \', \'\' ) LIKE REPLACE( \'' . $strAccountNumber . '\', \' \', \'\')
						AND gl_account_tree_id IS NULL' .
						$strConditions;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchCountActiveGlAccountTreeByCidByGlTreeIdByGlGroupId( $intCid, $intGlTreeId, $intGlGroupId, $objClientDatabase ) {

		$strSql = 'SELECT
						COUNT( gat.id )
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gat.gl_group_id = ' . ( int ) $intGlGroupId . '
						AND ga.disabled_by IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesCountRecursivelyByCidByGlTreeIdByGlGroupId( $intCid, $intGlTreeId, $intGlGroupId, $objClientDatabase, $objGlTreeFilter = NULL ) {

		$strConditions = NULL;

		if( true == valObj( $objGlTreeFilter, 'CGlTreeFilter' ) ) {

			if( true == valStr( $objGlTreeFilter->getGlAccountTypeId() ) ) {
				$strConditions = ' AND ga.gl_account_type_id = ' . ( int ) $objGlTreeFilter->getGlAccountTypeId();
			}
		}

		$strSql = 'WITH RECURSIVE
						recursive_gl_groups( id, cid, parent_gl_group_id, name, level )
						AS (
								SELECT
									gg1.id,
									gg1.cid,
									gg1.parent_gl_group_id,
									gg1.name,
									1 AS level
								FROM
									gl_groups gg1
								WHERE
									gg1.id = ' . ( int ) $intGlGroupId . '
									AND gg1.cid = ' . ( int ) $intCid . '
									AND gg1.gl_tree_id = ' . ( int ) $intGlTreeId . '

									UNION ALL

									SELECT
										gg2.id,
										gg2.cid,
										gg2.parent_gl_group_id,
										gg2.name,
										level + 1
									FROM
										gl_groups gg2,
										recursive_gl_groups rgg
									WHERE
										rgg.id = gg2.parent_gl_group_id
										AND rgg.cid = gg2.cid
										AND gg2.cid = ' . ( int ) $intCid . ' )

					SELECT
						COUNT( gat.id ) AS gl_accout_trees_count
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id )
					WHERE
						gat.gl_group_id IN ( SELECT id FROM recursive_gl_groups )
						AND gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_account_tree_id IS NULL
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId .
						$strConditions;

		$arrintData = ( array ) fetchData( $strSql, $objClientDatabase );

		return $arrintData[0]['gl_accout_trees_count'];
	}

	// This function returns all GlAccountTrees objects associated with all children of supplied GL group id
	// ( This functions fetches all children GL groups recurresively and then fetches all associated GL account trees )

	public static function fetchGlAccountTreesRecursivelyByCidByGlTreeIdByGlGroupId( $intCid, $intGlTreeId, $intGlGroupId, $objClientDatabase, $objGlTreeFilter = NULL, $objPagination = NULL, $arrintGlAccountIds = NULL, $boolIsBulkEdit = false ) {

		$strOrderBy				= 'LOWER( gat.name )';
		$strConditions			= NULL;
		$strPaginationCondition	= NULL;
		$strGlAccountsCondition	= NULL;

		if( true == $boolIsBulkEdit ) {
			$strOrderBy		= 'gat.formatted_account_number';
		}

		if( true == valObj( $objGlTreeFilter, 'CGlTreeFilter' ) ) {

			if( true == valStr( $objGlTreeFilter->getSortBy() ) ) {
				$strOrderBy = $objGlTreeFilter->getSortBy() . ' ' . $objGlTreeFilter->getSortDirection();
			}

			if( true == valStr( $objGlTreeFilter->getGlAccountTypeId() ) ) {
				$strConditions = ' AND ga.gl_account_type_id = ' . ( int ) $objGlTreeFilter->getGlAccountTypeId();
			}
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {

			$intOffset	= ( 0 < $objPagination->getPageNo() ) ? $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) : 0;
			$intLimit	= ( int ) $objPagination->getPageSize();

			$strPaginationCondition = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( true == valArr( $arrintGlAccountIds ) ) {
			$strGlAccountsCondition = ' AND ga.id IN ( ' . implode( ', ', $arrintGlAccountIds ) . ' )';
		}

		$strSql = 'WITH RECURSIVE
						recursive_gl_groups( id, cid, parent_gl_group_id, name, level )
						AS (
								SELECT
									gg1.id,
									gg1.cid,
									gg1.parent_gl_group_id,
									gg1.name,
									1 AS level
								FROM
									gl_groups gg1
								WHERE
									gg1.id = ' . ( int ) $intGlGroupId . '
									AND gg1.cid = ' . ( int ) $intCid . '
									AND gg1.gl_tree_id = ' . ( int ) $intGlTreeId . '

									UNION ALL

									SELECT
										gg2.id,
										gg2.cid,
										gg2.parent_gl_group_id,
										gg2.name,
										level + 1
									FROM
										gl_groups gg2,
										recursive_gl_groups rgg
									WHERE
										rgg.id = gg2.parent_gl_group_id
										AND rgg.cid = gg2.cid
										AND gg2.cid = ' . ( int ) $intCid . ' )

					SELECT
						gat.*,
						ga.disabled_by,
						ga.gl_account_type_id,
						ga.default_gl_account_id,
						ga.restrict_for_ap,
						ga.restrict_for_gl,
						ga.cash_flow_type_id,
						gt.account_number_pattern,
						gt.account_number_delimiter,
						ga.gl_account_usage_type_id
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id )
					WHERE
						gat.gl_group_id IN ( SELECT id FROM recursive_gl_groups )
						AND gat.cid = ' . ( int ) $intCid . $strConditions . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId .
						$strGlAccountsCondition . '
						AND gat.gl_account_tree_id IS NULL
					ORDER BY ' .
						$strOrderBy .
					$strPaginationCondition;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByIdsByCid( $arrintGlAccountTreeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintGlAccountTreeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gat.*,
						ga.gl_account_type_id,
						gt.account_number_pattern,
						gt.account_number_delimiter
					FROM
						gl_account_trees gat
						JOIN gl_trees gt ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
						LEFT JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
					WHERE
						gat.cid =' . ( int ) $intCid . '
						AND gat.id IN ( ' . implode( ',', $arrintGlAccountTreeIds ) . ' )';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByCidByGlTreeIdByGlGroupIds( $intCid, $intGlTreeId, $arrintGlGroupIds, $objClientDatabase ) {

		if( false == valArr( $arrintGlGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_account_trees
					WHERE
						cid =' . ( int ) $intCid . '
						AND gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gl_group_id IN ( ' . implode( ',', $arrintGlGroupIds ) . ' )';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesReportByCidByGlTreeId( $intCid, $intGlTreeId, $objClientDatabase ) {

		$strSql = 'SELECT
						gat.account_number,
						gat1.account_number AS master_account_number,
						gat.name AS account_name,
						gat.secondary_number,
						gat.description,
						gg.name AS group_location,
						glat.name AS gl_account_type,
						ga.disabled_by,
						ga.system_code
					FROM
						gl_account_trees gat
						JOIN gl_groups gg ON ( gg.cid = gat.cid AND gg.id = gat.gl_group_id )
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_types glat ON ( glat.id = gat.gl_account_type_id )
						LEFT JOIN gl_account_trees gat1 ON ( gat.cid = gat1.cid AND gat.gl_account_id = gat1.gl_account_id AND gat1.is_default = 1 )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gat.hide_account_balance_from_reports <> 1
					ORDER BY
						glat.id,
						LOWER( gat.account_number )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesReportByCidByGlTreeIdsByGlTreeTypeId( $intCid, $arrintGlTreeIds, $intGlTreeTypeId, $objClientDatabase, $intGlGroupId = NULL, $intGlAccountTypeId = NULL, $arrstrAccountNumbers = [] ) {

		if( false == valArr( $arrintGlTreeIds ) ) {
			return NULL;
		}

		$strWhere = NULL;
		if( true == valId( $intGlGroupId ) ) {
			$strWhere .= ' AND gg.id = ' . ( int ) $intGlGroupId;
		}
		if( true == valId( $intGlAccountTypeId ) ) {
			$strWhere .= ' AND glat.id = ' . ( int ) $intGlAccountTypeId;
		}

		if( true == valArr( $arrstrAccountNumbers ) ) {
			$strWhere .= ' AND gat.account_number IN( ' . sqlStrImplode( $arrstrAccountNumbers ) . ' ) ';
		}

		$strSql = 'SELECT
						gat.gl_tree_id,
						gat.gl_account_id,
						gat.account_number,
						gat.secondary_number,
						gt.account_number_pattern,
						gt.account_number_delimiter,
						gat.name AS account_name,
						gt.name AS gl_tree_name,
						gat.description,
						gat.gl_group_id,
						gg.name AS group_location,
						gg.gl_group_type_id,
						CASE WHEN gg.disabled_by IS NULL AND gg.disabled_on IS NULL THEN 1 ELSE 0 END as group_is_enabled,
						gg.show_group_name,
						gg.show_summary_row_name,
						gg.summary_row_name,
						gg.description AS group_description,
						glat.name AS gl_account_type,
						ga.disabled_by,
						gat.is_system
					FROM
						gl_account_trees gat
						JOIN gl_groups gg ON ( gg.cid = gat.cid AND gg.id = gat.gl_group_id )
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_types glat ON ( glat.id = gg.gl_account_type_id)
						JOIN gl_trees gt ON ( gat.gl_tree_id = gt.id AND gat.cid = gt.cid )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gt.gl_tree_type_id = ' . ( int ) $intGlTreeTypeId . '
						AND gt.id IN ( ' . implode( ',', $arrintGlTreeIds ) . ' )
						AND gt.deleted_by IS NULL
						AND gt.deleted_on IS NULL ' . $strWhere . '
					ORDER BY
						LOWER( glat.name ),
						LOWER( gat.account_number )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByIdOrByGlAccountTreeIdByCid( $intId, $intGlAccountTreeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_account_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id = ' . ( int ) $intId . '
						OR gl_account_tree_id = ' . ( int ) $intGlAccountTreeId;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByChildTreeIdAndAccountIdsByCid( $arrintChildTreeIdAccountIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintChildTreeIdAccountIds ) ) {
			return NULL;
		}

		$arrstrConditions = [];

		foreach( $arrintChildTreeIdAccountIds as $intAccountId => $intChildTreeId ) {
			$arrstrConditions[] = ' ( ' . ( int ) $intChildTreeId . ',' . ( int ) $intAccountId . ' ) ';
		}

		$strSql = 'SELECT
						gat.*,
						gt.account_number_pattern,
						gt.account_number_delimiter
					FROM
						gl_account_trees gat
						JOIN gl_trees gt ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND ( gat.gl_tree_id, gat.gl_account_id ) IN ( ' . implode( ',', $arrstrConditions ) . ' )';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByGlAccountTreeIdsByCid( $arrintGlAccountTreeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintGlAccountTreeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gat.*,
						ga.disabled_by,
						gt.account_number_pattern,
						gt.account_number_delimiter
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
						JOIN gl_trees gt ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
					WHERE
						gat.gl_account_tree_id IN ( ' . implode( ',', $arrintGlAccountTreeIds ) . ' )
						AND gat.cid = ' . ( int ) $intCid;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchEnabledGlAccountTreesByGlAccountTreeIdsByCid( $arrintGlAccountTreeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountTreeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gat.*,
						ga.disabled_by
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
					WHERE
						gat.gl_account_tree_id IN ( ' . implode( ',', $arrintGlAccountTreeIds ) . ' )
						AND gat.cid = ' . ( int ) $intCid . '
						AND ga.disabled_by IS NULL';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase, $objGlTreeFilter = NULL ) {

		$strOrderBy = 'gat.order_num';

		if( true == valObj( $objGlTreeFilter, 'CGlTreeFilter' ) && true == valStr( $objGlTreeFilter->getSortBy() ) ) {
			$strOrderBy = $objGlTreeFilter->getSortBy() . ' ' . $objGlTreeFilter->getSortDirection();
		}

		$strSql = 'SELECT
						gat.*,
						ga.disabled_by,
						ga.gl_account_type_id,
						ga.gl_account_usage_type_id,
						ga.cash_flow_type_id,
						dgam.name AS system_name,
						dgam.account_number AS system_account_number,
						dgt.account_number_pattern AS system_account_number_pattern,
						dgt.account_number_delimiter AS system_account_number_delimiter
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
						LEFT JOIN default_gl_account_masks dgam ON ( dgam.default_gl_account_id = ga.default_gl_account_id )
						LEFT JOIN default_gl_trees dgt ON ( dgt.id = dgam.default_gl_tree_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
					ORDER BY ' . $strOrderBy;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByGlTreeIdByGlAccountTypeIdsByCid( $intGlTreeId, $arrintGlAccountTypeIds, $intCid, $objClientDatabase ) {

		if( false == valId( $intGlTreeId ) || false == valArr( $arrintGlAccountTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gat.*,
						func_format_gl_account_number( gat.account_number, gt.account_number_pattern, gt.account_number_delimiter ) AS account_number
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
						JOIN gl_trees gt ON ( gat.cid = gt.cid AND gat.gl_tree_id = gt.id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND ga.gl_account_type_id IN ( ' . implode( ', ', $arrintGlAccountTypeIds ) . ' )
						AND gat.gl_account_tree_id IS NULL
					ORDER BY
						gat.account_number';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchNetCashFlowGlAccountTreesByGlGroupIdsByCid( $arrintGlGroupIds, $intCid, $objClientDatabase, $objGlTreeFilter = NULL ) {

		if( false == valArr( $arrintGlGroupIds ) ) {
			return NULL;
		}

		$strOrderBy = 'gamg.order_num,
					  gat.order_num';

		if( true == valObj( $objGlTreeFilter, 'CGlTreeFilter' ) && true == valStr( $objGlTreeFilter->getSortBy() ) ) {
			$strOrderBy = 'gat.' . $objGlTreeFilter->getSortBy() . ' ' . $objGlTreeFilter->getSortDirection();
		}

		$strSql = 'SELECT
						gat.*,
						gamg.gl_group_id
					FROM
						gl_account_trees gat
						JOIN gl_account_mask_groups gamg ON ( gat.cid = gamg.cid AND gat.id = gamg.gl_account_tree_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gamg.gl_group_id IN ( ' . implode( ', ', $arrintGlGroupIds ) . ' )
						AND gat.gl_account_tree_id IS NULL
					ORDER BY ' . $strOrderBy;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase, false );
	}

	public static function fetchGlAccountTreesByGlGroupIdsByCid( $arrintGlGroupIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_account_trees
					WHERE
						cid =' . ( int ) $intCid . '
						AND gl_group_id IN ( ' . implode( ',', $arrintGlGroupIds ) . ' )';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByGlGroupIdsNotByHideAccountBalanceFromReportsByCid( $arrintGlGroupIds, $intHideAccountBalanceFromReports, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_account_trees
					WHERE
						cid =' . ( int ) $intCid . '
						AND gl_group_id IN ( ' . implode( ',', $arrintGlGroupIds ) . ' )
						AND hide_account_balance_from_reports <> ' . ( int ) $intHideAccountBalanceFromReports;

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByGlAccountIdsNotByIdsByCid( $arrintGlAccountIds, $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountIds ) || false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_account_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_account_id IN ( ' . implode( ',', $arrintGlAccountIds ) . ' )
						AND id NOT IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleGlAccountTreesByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase,  $arrintGlAccountTypeIds = [] ) {

		$strGlAccountTypeIdsCondition	= ( true == valArr( $arrintGlAccountTypeIds ) ) ? ' AND gat.gl_account_type_id IN( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )' : NULL;

		$strSql = 'SELECT
						gat.grouping_gl_account_id,
						gat.formatted_account_number || \': \' || util_get_translated( \'name\', gat.name, gat.details ) AS display_name
					FROM
						gl_account_trees gat
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gat.hide_account_balance_from_reports = 0
						' . $strGlAccountTypeIdsCondition . '
					ORDER BY
						gat.account_number';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlAccountsByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gat.*
					FROM
						gl_account_trees gat
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.disabled_by IS NULL
					    AND gat.is_default = 1
					ORDER BY
						gat.account_number';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlAccountsByCidByGlChartSystemCode( $intCid, $strGlChartSystemCode, $objClientDatabase ) {

		$strSql = 'SELECT
						gat.*,
						glat.name AS gl_account_type_name
					FROM
						gl_account_trees gat
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
						JOIN gl_charts gc ON ( gat.gl_chart_id = gc.id AND gat.cid = gc.cid )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND	gc.system_code = \'' . addslashes( $strGlChartSystemCode ) . '\'
						AND	gat.disabled_by IS NULL
					ORDER BY gat.gl_account_type_id, gat.account_number, gat.gl_account_id';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchAllGlAccountsByGlAccountTypeIdsByCid( $arrintGlAccountTypeIds, $intCid, $objClientDatabase ) {

		if( false == valId( $intCid ) || false == valArr( array_filter( $arrintGlAccountTypeIds ) ) ) return NULL;

		$strSql = 'SELECT
						gat.*,
						ga.gl_account_type_id
					FROM
						gl_account_trees gat
						LEFT JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
					WHERE
						gat.cid =' . ( int ) $intCid . '
						AND gat.is_default = 1
						AND	gat.disabled_by IS NULL
						AND ga.gl_account_type_id IN ( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )
					ORDER BY
						gat.account_number';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchMaxOrderNumByGlGroupIdsByCid( $arrintGlGroupIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlGroupIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gl_group_id,
						MAX( order_num ) AS order_num
					FROM
						gl_account_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND gl_group_id IN ( ' . implode( ',', $arrintGlGroupIds ) . ' )
					GROUP BY
						gl_group_id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAssociatedGlAccountsWithApCodesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gat.gl_account_id as id,
						gat.cid,
						gat.formatted_account_number AS account_number,
						gat.name AS account_name,
						ac.id AS ap_code_id
					FROM
						gl_account_trees gat
						JOIN ap_codes ac ON ( ac.cid = gat.cid AND ac.item_gl_account_id = gat.gl_account_id AND ac.ap_code_type_id = ' . CApCodeType::CATALOG_ITEMS . ' )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.is_default = 1
						AND gat.disabled_by IS NULL';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase, false );
	}

	public static function fetchCustomGlAccountBulkTreesByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gat.*,
						gat1.account_number as master_account_number,
						ga.gl_account_type_id,
						ga.gl_account_usage_type_id,
						gg.name AS group_location,
						glat.name AS child_gl_type
					FROM
						gl_account_trees gat
						JOIN gl_groups gg ON ( gg.cid = gat.cid AND gg.id = gat.gl_group_id )
						JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						JOIN gl_account_types glat ON ( glat.id = ga.gl_account_type_id )
						LEFT JOIN gl_account_trees gat1 ON ( gat.cid = gat1.cid AND gat.gl_account_id = gat1.gl_account_id AND gat1.is_default = 1 )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gat.hide_account_balance_from_reports <> 1
					order by gat.order_num, 
						gat.account_number NULLS FIRST,
						gat.name DESC';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchDefaultGlAccountMasksByBankAccountIdByCid( $intBankAccountId, $intCid, $objClientDatabase, $boolIsShowDisableProperty = false ) {

		if( false == valId( $intBankAccountId ) ) return NULL;

		$strJoin = ' AND p.is_disabled = 0 ';
		if( true == $boolIsShowDisableProperty ) {
			$strJoin = '';
		}

		$strSql = 'SELECT
						p.property_name,
						glat.account_number,
						glat.name AS account_name,
						glat.gl_account_id
					FROM
						property_bank_accounts pba
						LEFT JOIN ap_codes ac ON ( pba.cid = ac.cid AND pba.bank_ap_code_id = ac.id )
						JOIN gl_account_trees glat ON ( glat.cid = ac.cid AND glat.gl_account_id = ac.item_gl_account_id )
						JOIN properties p ON ( p.cid = pba.cid AND p.id = pba.property_id ' . $strJoin . ' )
					WHERE
						pba.cid = ' . ( int ) $intCid . '
						AND pba.bank_account_id = ' . ( int ) $intBankAccountId . '
						AND glat.is_default = 1
						AND glat.disabled_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountsBySearchParameteresByCid( $arrmixSearchData, $intCid, $objDatabase ) {

		$intArCodeTypeId 		= NULL;
		$arrstrFilteredSearch 	= [];

		$arrstrFilteredSearch	= CDataBlobs::buildFilteredExplodedSearchBySearchedString( $arrmixSearchData['q'] );

		$intArCodeId 		= $arrmixSearchData['fast_lookup_filter']['ar_code_id'];
		$intArCodeTypeId 	= $arrmixSearchData['fast_lookup_filter']['ar_code_type_id'];
		$intArOriginId 	 	= $arrmixSearchData['fast_lookup_filter']['ar_origin_id'];
		$intArTriggerId 	= $arrmixSearchData['fast_lookup_filter']['ar_trigger_id'];

		if( false == valId( $intArCodeTypeId ) || false == in_array( $arrmixSearchData['fast_lookup_filter']['lookup_type'], [ CGlAccountTree::LOOKUP_TYPE_DEBIT_GL_ACCOUNTS, CGlAccountTree::LOOKUP_TYPE_CREDIT_GL_ACCOUNTS ] ) ) return;

		if( CGlAccountTree::LOOKUP_TYPE_DEBIT_GL_ACCOUNTS == $arrmixSearchData['fast_lookup_filter']['lookup_type'] ) {

			$strSql = 'SELECT
							gat.gl_account_id,
							util_get_translated( \'name\', gat.name, gat.details ) AS account_name,
							gat.formatted_account_number AS account_number,
							util_get_system_translated( \'name\', glat.name, glat.details ) AS gl_account_type_name
						FROM
							gl_account_trees gat
							JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
							JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
							JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id AND ' . ( int ) $intArCodeTypeId . ' = ANY( gaut.debit_ar_code_types ) )
						WHERE
							gat.cid = ' . ( int ) $intCid . '
							AND gat.is_default = 1
							AND ga.disabled_on IS NULL';

			if( true == valId( $intArCodeId ) ) {

				$strSql	= 'SELECT
								gat.gl_account_id,
								util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
								gat.formatted_account_number AS account_number,
								util_get_system_translated( \'name\', glat.name, glat.details ) AS gl_account_type_name
							FROM
								gl_account_trees gat
								JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
								JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
								JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id AND ' . ( int ) $intArCodeTypeId . ' = ANY( gaut.debit_ar_code_types ) )
								JOIN ar_codes ac ON( gat.cid = ac.cid AND ac.id = ' . ( int ) $intArCodeId . ' )
								JOIN gl_accounts ga_debit ON( ac.cid = ga_debit.cid AND ac.debit_gl_account_id = ga_debit.id )
							WHERE
								gat.cid = ' . ( int ) $intCid . '
								AND gat.is_default = 1
								AND gat.disabled_by IS NULL
								AND ( ga.gl_account_usage_type_id = ga_debit.gl_account_usage_type_id
										OR ( NOT EXISTS ( SELECT 1 FROM ar_transactions at WHERE at.cid = ac.cid AND at.ar_code_id = ac.id LIMIT 1 )
								 			 AND NOT EXISTS ( SELECT 1 FROM property_ar_codes pac WHERE pac.cid = ac.cid AND pac.ar_code_id = ac.id LIMIT 1 )
									) )';
			}
		}

		if( CGlAccountTree::LOOKUP_TYPE_CREDIT_GL_ACCOUNTS == $arrmixSearchData['fast_lookup_filter']['lookup_type'] ) {

			$strSql = 'SELECT
							gat.gl_account_id,
							util_get_translated( \'name\', gat.name, gat.details ) AS account_name,
							gat.formatted_account_number AS account_number,
							util_get_system_translated( \'name\', glat.name, glat.details ) AS gl_account_type_name
						FROM
							gl_account_trees gat
							JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
							JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id )
							JOIN gl_account_usage_types gaut ON ( ga.gl_account_usage_type_id = gaut.id AND ' . ( int ) $intArCodeTypeId . ' = ANY( gaut.credit_ar_code_types ) AND ' . ( int ) $intArOriginId . ' = ANY( gaut.credit_ar_origins ) )
						WHERE
							gat.cid = ' . ( int ) $intCid . '
							AND CASE
									WHEN gaut.allow_null_triggers = true
									THEN ( gaut.credit_ar_trigger_ids IS NULL OR ' . ( int ) $intArTriggerId . ' = ANY( gaut.credit_ar_trigger_ids ) )
									ELSE ' . ( int ) $intArTriggerId . ' = ANY( gaut.credit_ar_trigger_ids )
								END
							AND gat.is_default = 1
							AND gat.disabled_by IS NULL';
		}

		$arrstrAdvancedSearchParameters = [];

		foreach( $arrstrFilteredSearch as $strKeywordAdvancedSearch ) {
			if( false == empty( $strKeywordAdvancedSearch ) ) {
				array_push( $arrstrAdvancedSearchParameters, 'gat.name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
			}
		}

		if( true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		if( CGlAccountTree::LOOKUP_TYPE_CREDIT_GL_ACCOUNTS == $arrmixSearchData['fast_lookup_filter']['lookup_type'] || CGlAccountTree::LOOKUP_TYPE_DEBIT_GL_ACCOUNTS == $arrmixSearchData['fast_lookup_filter']['lookup_type'] ) {

			$strSql .= ' ORDER BY
							gat.gl_account_type_id,
							gat.formatted_account_number,
							account_name';
		}

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchCashEquivalentGlAccountTreesByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase, $arrintGlAccountTypeIds = [] ) {

		$strGlAccountTypeIdsCondition	= ( true == valArr( $arrintGlAccountTypeIds ) ) ? ' AND gat.gl_account_type_id IN( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )' : NULL;

		$strSql = 'SELECT
						gat.grouping_gl_account_id,
						gat.formatted_account_number || \': \' || gat.name AS display_name
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gat.hide_account_balance_from_reports = 0
						AND ga.cash_flow_type_id = ' . CCashFlowType::CASH_EQUIVALENT . '
						' . $strGlAccountTypeIdsCondition . '
					ORDER BY
						gat.account_number';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchBalanceSheetTypeGlAccountTreesByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase, $arrintGlAccountTypeIds = [] ) {

		$strGlAccountTypeIdsCondition = ( true == valArr( $arrintGlAccountTypeIds ) ) ? ' AND gat.gl_account_type_id IN( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )' : NULL;

		$strSql = 'SELECT
						gat.grouping_gl_account_id,
						gat.formatted_account_number || \': \' || gat.name AS display_name
					FROM
						gl_account_trees gat
						JOIN gl_account_types glat ON ( gat.gl_account_type_id = glat.id AND glat.is_income_type = 0 )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gat.hide_account_balance_from_reports = 0
						' . $strGlAccountTypeIdsCondition . '
					ORDER BY
						gat.account_number';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesForMasterGlTreeByGlAccountIdsByCid( $arrintGlAccountIds, $intCid, $objClientDatabase ) {

		if( false == ( $arrintGlAccountIds = getIntValuesFromArr( $arrintGlAccountIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
 						gat.gl_account_id,
						gat.formatted_account_number AS account_number,
						gat.name,
						gat.disabled_by
					FROM
						gl_account_trees gat
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_account_id IN ( ' . sqlIntImplode( $arrintGlAccountIds ) . ' )
						AND gat.is_default = 1
					ORDER BY
						gat.formatted_account_number';

		return self::fetchGlAccountTrees( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountTreesByGlTreeIdsByCid( $arrintGlTreeIds, $intCid, $objClientDatabase ) {

		if( !valArr( $arrintGlTreeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gat.id,
						gat.gl_tree_id,
						gat.gl_account_id,
						gat.formatted_account_number AS account_number,
						util_get_translated( \'name\', gat.name, gat.details ) AS account_name
					FROM
						gl_account_trees gat
						JOIN gl_accounts ga ON ( gat.cid = ga.cid AND gat.gl_account_id = ga.id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_account_tree_id IS NULL
						AND gat.gl_tree_id IN ( ' . implode( ', ', $arrintGlTreeIds ) . ' )
						AND ( ga.disabled_on IS NULL OR EXISTS (
								SELECT
									1
								FROM
									gl_account_trees gat_child
									JOIN gl_accounts ga_child ON ( gat_child.cid = ga_child.cid AND gat_child.gl_account_id = ga_child.id )
								WHERE
									gat_child.cid = gat.cid
									AND gat.id = gat_child.gl_account_tree_id
									AND ga_child.disabled_on IS NULL
							) )
					ORDER BY account_number';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>