<?php

class CMaintenanceRequestFilter extends CBaseMaintenanceRequestFilter {

	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intCompanyUserId;
	protected $m_intSearchAllProblemTypes;
	protected $m_intPageNumber;
	protected $m_intIsArchive;
	protected $m_intMaintenanceRequestId;
	protected $m_intPropertyId;
	protected $m_intCompanyEmployeeId;
	protected $m_intMaintenanceStatusTypeId;
	protected $m_intMaintenanceRequestTypeId;
	protected $m_intMaintenanceStatusId;
	protected $m_intOrderByType;
	protected $m_intEligibleMaintenancePrioritiesCount;
	protected $m_intEligibleMaintenanceStatusesCount;
	protected $m_intEligibleMaintenanceProblemsCount;
	protected $m_intEligibleMaintenanceLocationsCount;
	protected $m_intMaintenancePriorityTypeId;
	protected $m_intPropertyUnitId;
	protected $m_intUnitSpaceId;
	protected $m_intBuildingId;
	protected $m_strOrderByField;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_strLastUpdatedOnFromDate;
	protected $m_strLastUpdatedOnToDate;
	protected $m_strRemotePrimaryKey;
	protected $m_strMaintenanceStatus;
	protected $m_strCustomPropertyGroupIds;
	protected $m_strParentMaintenanceRequestIds;

	protected $m_arrintPsProductIds;
	protected $m_arrintEligiblePsProductIds;
	protected $m_arrintCompanyUserPropertyIds;
	protected $m_arrobjEligibleMaintenanceFilters;
	protected $m_arrobjEligiblePsProducts;
	protected $m_arrobjEligibleMaintenancePriorities;
	protected $m_arrobjEligibleMaintenanceStatuses;
	protected $m_arrobjEligibleMaintenanceProblems;
	protected $m_arrobjEligibleMaintenanceLocations;
	protected $m_arrobjAllMaintenancePriorities;
	protected $m_arrobjAllMaintenanceStatuses;
	protected $m_arrobjAllMaintenanceProblems;
	protected $m_arrobjAllMaintenanceLocations;
	protected $m_arrstrEligibleProperties;
	protected $m_arrintMaintenanceStatusTypeIds;
	protected $m_arrintMaintenanceCategoryIds;
	protected $m_boolIsAssignedToOthers;
	protected $m_boolIsNotAssigned;
	protected $m_boolIsSearch;

	const DEFAULT_PAGE_NO = 1;
	const DEFAULT_COUNT_PER_PAGE = 25;

	 /**
	  * Set Functions
	  */

	public function setDefaults() {

		$this->setPageNumber( self::DEFAULT_PAGE_NO );
		$this->setCountPerPage( self::DEFAULT_COUNT_PER_PAGE );

		return;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = CStrings::strToIntDef( $intCustomerId, NULL, false );
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = CStrings::strToIntDef( $intLeaseId, NULL, false );
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->m_intCompanyUserId = CStrings::strToIntDef( $intCompanyUserId, NULL, false );
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->m_intCompanyEmployeeId = CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false );
	}

	public function setOrderByField( $strOrderByField ) {
		$this->m_strOrderByField = $strOrderByField;
	}

	public function setOrderByType( $strOrderByType ) {
		$this->m_intOrderByType = $strOrderByType;
	}

	public function setSearchAllProblemTypes( $intSearchAllProblemTypes ) {
		$this->m_intSearchAllProblemTypes = $intSearchAllProblemTypes;
	}

	public function setMaintenanceRequestTypeIds( $arrintMaintenanceRequestTypeIds ) {

		if( true == valArr( $arrintMaintenanceRequestTypeIds ) ) {
			$this->m_strMaintenanceRequestTypeIds = $this->flattenIds( $arrintMaintenanceRequestTypeIds );
		} elseif( false == empty( $arrintMaintenanceRequestTypeIds ) ) {
			$this->m_strMaintenanceRequestTypeIds = $arrintMaintenanceRequestTypeIds;
		} else {
			$this->m_strMaintenanceRequestTypeIds = NULL;
		}
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->m_strStartDatetime = CStrings::strTrimDef( $strStartDatetime, -1, NULL, true );
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->m_strEndDatetime = CStrings::strTrimDef( $strEndDatetime, -1, NULL, true );
	}

	public function setLastUpdatedOnFromDate( $strLastUpdatedOnFromDate ) {
		$this->m_strLastUpdatedOnFromDate = CStrings::strTrimDef( $strLastUpdatedOnFromDate, -1, NULL, true );
	}

	public function setLastUpdatedOnToDate( $strLastUpdatedOnToDate ) {
		$this->m_strLastUpdatedOnToDate = CStrings::strTrimDef( $strLastUpdatedOnToDate, -1, NULL, true );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setBuildingId( $intBuildingId ) {
		$this->m_intBuildingId = CStrings::strToIntDef( $intBuildingId, NULL, false );
	}

	public function setPropertyGroupIds( $arrintPropertyGroupIds ) {

		if( true == valArr( $arrintPropertyGroupIds ) ) {
			$this->m_strPropertyGroupIds = $this->flattenIds( $arrintPropertyGroupIds );
		} elseif( false == empty( $arrintPropertyGroupIds ) ) {
			$this->m_strPropertyGroupIds = $arrintPropertyGroupIds;
		} else {
			$this->m_strPropertyGroupIds = NULL;
		}
	}

	public function setCustomPropertyGroupIds( $arrintCustomPropertyGroupIds ) {

		if( true == valArr( $arrintCustomPropertyGroupIds ) ) {
			$this->m_strCustomPropertyGroupIds = $this->flattenIds( $arrintCustomPropertyGroupIds );
		} elseif( false == empty( $arrintCustomPropertyGroupIds ) ) {
			$this->m_strCustomPropertyGroupIds = $arrintCustomPropertyGroupIds;
		} else {
			$this->m_strCustomPropertyGroupIds = NULL;
		}
	}

	public function setMaintenanceStatusIds( $arrintMaintenanceStatusIds ) {

		if( true == valArr( $arrintMaintenanceStatusIds ) ) {
			$this->m_strMaintenanceStatusIds = $this->flattenIds( $arrintMaintenanceStatusIds );
		} elseif( false == empty( $arrintMaintenanceStatusIds ) ) {
			$this->m_strMaintenanceStatusIds = $arrintMaintenanceStatusIds;
		} else {
			$this->m_strMaintenanceStatusIds = NULL;
		}
	}

	public function setCompanyEmployeeIds( $arrintCompanyEmployeeIds ) {

		if( true == valArr( $arrintCompanyEmployeeIds ) ) {
			$this->m_strCompanyEmployeeIds = $this->flattenIds( $arrintCompanyEmployeeIds );
		} elseif( false == empty( $arrintCompanyEmployeeIds ) ) {
			$this->m_strCompanyEmployeeIds = $arrintCompanyEmployeeIds;
		} else {
			$this->m_strCompanyEmployeeIds = NULL;
		}
	}

	public function setMaintenancePriorityIds( $arrintMaintenancePriorityIds ) {

		if( true == valArr( $arrintMaintenancePriorityIds ) ) {
			$this->m_strMaintenancePriorityIds = $this->flattenIds( $arrintMaintenancePriorityIds );
		} elseif( false == empty( $arrintMaintenancePriorityIds ) ) {
			$this->m_strMaintenancePriorityIds = $arrintMaintenancePriorityIds;
		} else {
			$this->m_strMaintenancePriorityIds = NULL;
		}
	}

	public function setMaintenanceLocations( $arrintMaintenanceLocationIds ) {

		if( true == valArr( $arrintMaintenanceLocationIds ) ) {
			$this->m_strMaintenanceLocationIds = $this->flattenIds( $arrintMaintenanceLocationIds );
		} elseif( false == empty( $arrintMaintenanceLocationIds ) ) {
			$this->m_strMaintenanceLocationIds = $arrintMaintenanceLocationIds;
		} else {
			$this->m_strMaintenanceLocationIds = NULL;
		}
	}

	public function setMaintenanceProblemIds( $arrintMaintenanceProblemIds ) {

		if( true == valArr( $arrintMaintenanceProblemIds ) ) {
			$this->m_strMaintenanceProblemIds = $this->flattenIds( $arrintMaintenanceProblemIds );
		} elseif( false == empty( $arrintMaintenanceProblemIds ) ) {
			$this->m_strMaintenanceProblemIds = $arrintMaintenanceProblemIds;
		} else {
			$this->m_strMaintenanceProblemIds = NULL;
		}
	}

	public function setApPayeeIds( $arrintApPayeeIds ) {

		if( true == valArr( $arrintApPayeeIds ) ) {
			$this->m_strApPayeeIds = $this->flattenIds( $arrintApPayeeIds );
		} elseif( false == empty( $arrintApPayeeIds ) ) {
			$this->m_strApPayeeIds = $arrintApPayeeIds;
		} else {
			$this->m_strApPayeeIds = NULL;
		}
	}

	public function setCompanyEmployeesIds( $arrintCompanyEmployeeIds ) {

		if( true == valArr( $arrintCompanyEmployeeIds ) ) {
			$this->m_strCompanyEmployeeIds = $this->flattenIds( $arrintCompanyEmployeeIds );
		} elseif( false == empty( $arrintCompanyEmployeeIds ) ) {
			$this->m_strCompanyEmployeeIds = $arrintCompanyEmployeeIds;
		} else {
			$this->m_strCompanyEmployeeIds = NULL;
		}
	}

	public function setPropertyBuildingIds( $arrintPropertyBuildingIds ) {

		if( true == valArr( $arrintPropertyBuildingIds ) ) {
			$this->m_strPropertyBuildingIds = $this->flattenIds( $arrintPropertyBuildingIds );
		} elseif( false == empty( $arrintPropertyBuildingIds ) ) {
			$this->m_strPropertyBuildingIds = $arrintPropertyBuildingIds;
		} else {
			$this->m_strPropertyBuildingIds = NULL;
		}
	}

	public function setUnitTypeIds( $arrintUnitTypeIds ) {

		if( true == valArr( $arrintUnitTypeIds ) ) {
			$this->m_strUnitTypeIds = $this->flattenIds( $arrintUnitTypeIds );
		} elseif( false == empty( $arrintUnitTypeIds ) ) {
			$this->m_strUnitTypeIds = $arrintUnitTypeIds;
		} else {
			$this->m_strUnitTypeIds = NULL;
		}
	}

	public function setPsProductIds( $arrintPsProductIds ) {

		if( true == valArr( $arrintPsProductIds ) ) {
			$this->m_strPsProductIds = $this->flattenIds( $arrintPsProductIds );
		} elseif( false == empty( $arrintPsProductIds ) ) {
			$this->m_strPsProductIds = $arrintPsProductIds;
		} else {
			$this->m_strPsProductIds = NULL;
		}
	}

	public function setParentMaintenanceRequestIds( $arrintParentMaintenanceRequestIds ) {

		if( true == valArr( $arrintParentMaintenanceRequestIds ) ) {
			$this->m_strParentMaintenanceRequestIds = $this->flattenIds( $arrintParentMaintenanceRequestIds );
		} elseif( false == empty( $arrintParentMaintenanceRequestIds ) ) {
			$this->m_strParentMaintenanceRequestIds = $arrintParentMaintenanceRequestIds;
		} else {
			$this->m_strParentMaintenanceRequestIds = NULL;
		}
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->m_strRemotePrimaryKey = CStrings::strTrimDef( $strRemotePrimaryKey, 50, NULL, true );
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->m_intMaintenanceRequestId = CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false );
	}

	public function setPageNumber( $intPageNumber ) {
		$this->m_intPageNumber = $intPageNumber;
	}

	public function setCountPerPage( $intCountPerPage ) {
		$this->m_intCountPerPage = $intCountPerPage;
	}

	public function setMaintenanceStatusTypeIds( $arrintMaintenanceStatusTypeIds ) {

		if( true == valArr( $arrintMaintenanceStatusTypeIds ) ) {
			$this->m_arrintMaintenanceStatusTypeIds = $this->flattenIds( $arrintMaintenanceStatusTypeIds );
		} elseif( false == empty( $arrintMaintenanceStatusTypeIds ) ) {
			$this->m_arrintMaintenanceStatusTypeIds = $arrintMaintenanceStatusTypeIds;
		} else {
			$this->m_arrintMaintenanceStatusTypeIds = NULL;
		}
	}

	public function setMaintenanceCategoryIds( $arrintMaintenanceCategoryIds ) {

		if( true == valArr( $arrintMaintenanceCategoryIds ) ) {
			$this->m_arrintMaintenanceCategoryIds = $this->flattenIds( $arrintMaintenanceCategoryIds );
		} elseif( false == empty( $arrintMaintenanceCategoryIds ) ) {
			$this->m_arrintMaintenanceCategoryIds = $arrintMaintenanceCategoryIds;
		} else {
			$this->m_arrintMaintenanceCategoryIds = NULL;
		}
	}

	public function setUnitSpaceStatusTypeIds( $arrintUnitSpaceStatusTypeIds ) {

		if( true == valArr( $arrintUnitSpaceStatusTypeIds ) ) {
			$this->m_strUnitSpaceStatusTypeIds = $this->flattenIds( $arrintUnitSpaceStatusTypeIds );
		} elseif( false == empty( $arrintUnitSpaceStatusTypeIds ) ) {
			$this->m_strUnitSpaceStatusTypeIds = $arrintUnitSpaceStatusTypeIds;
		} else {
			$this->m_strUnitSpaceStatusTypeIds = NULL;
		}
	}

	public function setMaintenanceStatusTypeId( $intMaintenanceStatusTypeId ) {
		$this->m_intMaintenanceStatusTypeId = $intMaintenanceStatusTypeId;
	}

	public function setMaintenanceStatusId( $intMaintenanceStatusId ) {
		$this->m_intMaintenanceStatusId = $intMaintenanceStatusId;
	}

	public function setMaintenanceRequestTypeId( $intMaintenanceRequestTypeId ) {
		$this->m_intMaintenanceRequestTypeId = $intMaintenanceRequestTypeId;
	}

	public function setMaintenanceStatus( $mixMaintenanceStatus ) {
		$this->m_strMaintenanceStatus = $mixMaintenanceStatus;
	}

	public function setMaintenancePriorityTypeId( $intMaintenancePriorityTypeId ) {
		$this->m_intMaintenancePriorityTypeId = $intMaintenancePriorityTypeId;
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->m_intPropertyUnitId = $intPropertyUnitId;
	}

	public function setUnitSpaceId( $intUnitSpaceId ) {
		$this->m_intUnitSpaceId = $intUnitSpaceId;
	}

	public function setCompanyUserPropertyIds( $arrintCompanyUserPropertyIds ) {
		$this->m_arrintCompanyUserPropertyIds = $arrintCompanyUserPropertyIds;
	}

	public function setIsNotAssigned( $boolIsNotAssigned ) {
		$this->m_boolIsNotAssigned = $boolIsNotAssigned;
	}

	public function setIsAssignedToOthers( $boolIsAssignedToOthers ) {
		$this->m_boolIsAssignedToOthers = $boolIsAssignedToOthers;
	}

	public function setIsSearch( $boolIsSearch ) {
		$this->m_boolIsSearch = $boolIsSearch;
	}

	 /**
	  * Get Functions
	  */

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getPageNumber() {
		return $this->m_intPageNumber;
	}

	public function getOrderByField() {
		return $this->m_strOrderByField;
	}

	public function getOrderByType() {
		return $this->m_intOrderByType;
	}

	public function getIsArchive() {
		return $this->m_intIsArchive;
	}

	public function getEligibleMaintenancePrioritiesCount() {
		return $this->m_intEligibleMaintenancePrioritiesCount;
	}

	public function getEligibleMaintenanceStatusesCount() {
		return $this->m_intEligibleMaintenanceStatusesCount;
	}

	public function getEligibleMaintenanceProblemsCount() {
		return $this->m_intEligibleMaintenanceProblemsCount;
	}

	public function getEligibleMaintenanceLocationsCount() {
		return $this->m_intEligibleMaintenanceLocationsCount;
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function getLastUpdatedOnFromDate() {
		return $this->m_strLastUpdatedOnFromDate;
	}

	public function getLastUpdatedOnToDate() {
		return $this->m_strLastUpdatedOnToDate;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getBuildingId() {
		return $this->m_intBuildingId;
	}

	public function getPropertyGroupIds() {

		if( true == valArr( $this->m_strPropertyGroupIds ) ) {
			return $this->m_strPropertyGroupIds;
		} elseif( false == empty( $this->m_strPropertyGroupIds ) ) {
			return $this->expandIds( $this->m_strPropertyGroupIds );
		} else {
			return [];
		}
	}

	public function getCustomPropertyGroupIds() {

		if( true == valArr( $this->m_strCustomPropertyGroupIds ) ) {
			return $this->m_strCustomPropertyGroupIds;
		} elseif( false == empty( $this->m_strCustomPropertyGroupIds ) ) {
			return $this->expandIds( $this->m_strCustomPropertyGroupIds );
		} else {
			return [];
		}
	}

	public function getMaintenanceStatusIds() {

		if( true == valArr( $this->m_strMaintenanceStatusIds ) ) {
			return $this->m_strMaintenanceStatusIds;
		} elseif( false == empty( $this->m_strMaintenanceStatusIds ) ) {
			return $this->expandIds( $this->m_strMaintenanceStatusIds );
		} else {
			return [];
		}
	}

	public function getMaintenanceRequestTypeIds() {

		if( true == valArr( $this->m_strMaintenanceRequestTypeIds ) ) {
			return $this->m_strMaintenanceRequestTypeIds;
		} elseif( false == empty( $this->m_strMaintenanceRequestTypeIds ) ) {
			return $this->expandIds( $this->m_strMaintenanceRequestTypeIds );
		} else {
			return [];
		}
	}

	public function getUnitSpaceStatusTypeIds() {

		if( true == valArr( $this->m_strUnitSpaceStatusTypeIds ) ) {
			return $this->m_strUnitSpaceStatusTypeIds;
		} elseif( false == empty( $this->m_strUnitSpaceStatusTypeIds ) ) {
			return $this->expandIds( $this->m_strUnitSpaceStatusTypeIds );
		} else {
			return [];
		}
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function getCompanyEmployeeIds() {

		if( true == valArr( $this->m_strCompanyEmployeeIds ) ) {
			return array_filter( $this->m_strCompanyEmployeeIds );
		} elseif( false == empty( $this->m_strCompanyEmployeeIds ) ) {
			return $this->expandIds( $this->m_strCompanyEmployeeIds );
		} else {
			return [];
		}
	}

	public function getMaintenancePriorityIds() {

		if( true == valArr( $this->m_strMaintenancePriorityIds ) ) {
			return $this->m_strMaintenancePriorityIds;
		} elseif( false == empty( $this->m_strMaintenancePriorityIds ) ) {
			return $this->expandIds( $this->m_strMaintenancePriorityIds );
		} else {
			return [];
		}
	}

	public function getMaintenanceLocationIds() {

		if( true == valArr( $this->m_strMaintenanceLocationIds ) ) {
			return $this->m_strMaintenanceLocationIds;
		} elseif( false == empty( $this->m_strMaintenanceLocationIds ) ) {
			return $this->expandIds( $this->m_strMaintenanceLocationIds );
		} else {
			return [];
		}
	}

	public function getApPayeeIds() {

		if( true == valArr( $this->m_strApPayeeIds ) ) {
			return $this->m_strApPayeeIds;
		} elseif( false == empty( $this->m_strApPayeeIds ) ) {
			return $this->expandIds( $this->m_strApPayeeIds );
		} else {
			return [];
		}
	}

	public function getPropertyBuildingIds() {

		if( true == valArr( $this->m_strPropertyBuildingIds ) ) {
			return $this->m_strPropertyBuildingIds;
		} elseif( false == empty( $this->m_strPropertyBuildingIds ) ) {
			return $this->expandIds( $this->m_strPropertyBuildingIds );
		} else {
			return [];
		}
	}

	public function getUnitTypeIds() {

		if( true == valArr( $this->m_strUnitTypeIds ) ) {
			return $this->m_strUnitTypeIds;
		} elseif( false == empty( $this->m_strUnitTypeIds ) ) {
			return $this->expandIds( $this->m_strUnitTypeIds );
		} else {
			return [];
		}
	}

	public function getSearchAllProblemTypes() {
		return $this->m_intSearchAllProblemTypes;
	}

	public function getMaintenanceProblemIds() {

		if( true == valArr( $this->m_strMaintenanceProblemIds ) ) {
			return $this->m_strMaintenanceProblemIds;
		} elseif( false == empty( $this->m_strMaintenanceProblemIds ) ) {
			return $this->expandIds( $this->m_strMaintenanceProblemIds );
		} else {
			return [];
		}
	}

	public function getMaintenanceCategoryIds() {

		if( true == valArr( $this->m_arrintMaintenanceCategoryIds ) ) {
			return $this->m_arrintMaintenanceCategoryIds;
		} elseif( false == empty( $this->m_arrintMaintenanceCategoryIds ) ) {
			return $this->expandIds( $this->m_arrintMaintenanceCategoryIds );
		} else {
			return [];
		}
	}

	public function getParentMaintenanceRequestIds() {

		if( true == valArr( $this->m_strParentMaintenanceRequestIds ) ) {
			return $this->m_strParentMaintenanceRequestIds;
		} elseif( false == empty( $this->m_strParentMaintenanceRequestIds ) ) {
			return $this->expandIds( $this->m_strParentMaintenanceRequestIds );
		} else {
			return [];
		}
	}

	public function getPsProductIds() {

		if( true == valArr( $this->m_strPsProductIds ) ) {
			return $this->m_strPsProductIds;
		} elseif( false == empty( $this->m_strPsProductIds ) ) {
			return $this->expandIds( $this->m_strPsProductIds );
		} else {
			return [];
		}
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function getEligiblePsProductIds() {
		return $this->m_arrobjEligiblePsProducts;
	}

	public function getEligibleProperties() {
		return $this->m_arrstrEligibleProperties;
	}

	public function getEligibleMaintenancePriorities() {
		return $this->m_arrobjEligibleMaintenancePriorities;
	}

	public function getEligibleMaintenanceStatuses() {
		return $this->m_arrobjEligibleMaintenanceStatuses;
	}

	public function getEligibleMaintenanceStatusTypes() {
		return $this->m_arrobjEligibleMaintenanceStatuses;
	}

	public function getEligibleMaintenanceProblems() {
		return $this->m_arrobjEligibleMaintenanceProblems;
	}

	public function getEligibleMaintenanceLocations() {
		return $this->m_arrobjEligibleMaintenanceLocations;
	}

	public function getAllMaintenancePriorities() {
		return $this->m_arrobjAllMaintenancePriorities;
	}

	public function getAllMaintenanceStatuses() {
		return $this->m_arrobjAllMaintenanceStatuses;
	}

	public function getAllMaintenanceProblems() {
		return $this->m_arrobjAllMaintenanceProblems;
	}

	public function getAllMaintenanceLocations() {
		return $this->m_arrobjAllMaintenanceLocations;
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function getMaintenanceStatusTypeIds() {

		if( true == valArr( $this->m_arrintMaintenanceStatusTypeIds ) ) {
			return $this->m_arrintMaintenanceStatusTypeIds;
		} elseif( false == empty( $this->m_arrintMaintenanceStatusTypeIds ) ) {
			return $this->expandIds( $this->m_arrintMaintenanceStatusTypeIds );
		} else {
			return [];
		}
	}

	public function getMaintenanceStatusTypeId() {
		return $this->m_intMaintenanceStatusTypeId;
	}

	public function getMaintenanceStatusId() {
		return $this->m_intMaintenanceStatusId;
	}

	public function getMaintenanceRequestTypeId() {
		return $this->m_intMaintenanceRequestTypeId;
	}

	public function getMaintenanceStatus() {
		return $this->m_strMaintenanceStatus;
	}

	public function getCompanyUserMaintenanceFilters() {
		return $this->m_arrobjEligibleMaintenanceFilters;
	}

	public function getMaintenancePriorityTypeId() {
		return $this->m_intMaintenancePriorityTypeId;
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function getUnitSpaceId() {
		return $this->m_intUnitSpaceId;
	}

	public function getCompanyUserPropertyIds() {
		return $this->m_arrintCompanyUserPropertyIds;
	}

	public function getIsNotAssigned() {
		return $this->m_boolIsNotAssigned;
	}

	public function getIsAssignedToOthers() {
		return $this->m_boolIsAssignedToOthers;
	}

	public function getIsSearch() {
		return $this->m_boolIsSearch;
	}

	 /**
	  * Validation Functions
	  */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		// It is required
		if( true == is_null( $this->getCid() ) ) {
			trigger_error( __( 'A Client Id is required - {%s, 0}', [ 'CMaintenanceFilter' ] ), E_USER_ERROR );
			$boolIsValid = false;
		} elseif( $this->getCid() < 1 ) {
			// Must be greater than zero
			trigger_error( __( 'A Client Id has to be greater than zero - {%s, 0}', [ 'CMaintenanceFilter' ] ), E_USER_ERROR );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valName( $intCompanyUserId, $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
			$boolIsValid = false;
		}

		$intCountMaintenanceRequestFilters = \Psi\Eos\Entrata\CMaintenanceRequestFilters::createService()->fetchMaintenanceRequestFilterCountByIdByFilterNameByCompanyUserIdByCid( $this->getId(), $this->getName(), $intCompanyUserId, $this->getCid(), $objDatabase );

		if( 0 < $intCountMaintenanceRequestFilters ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '{%s, 0} already exists. Please enter a different name.', [ $this->getName() ] ) ) );
		}

		return $boolIsValid;
	}

	public function valSearchKeyword() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyGroupIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenancePriorityIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceStatusTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceStatusIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceLocationIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceProblemIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaintenanceRequestTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreatedOnFrom() {
		$boolIsValid = true;
		$boolValidatedCreatedOnFrom = CValidation::checkDateFormat( $this->getCreatedOnFrom(), $boolFormat = true );

		if( true == valStr( $this->getCreatedOnFrom() ) && false === $boolValidatedCreatedOnFrom && 'Min' != $this->getCreatedOnFrom() ) {
			$boolIsValid = false;
			$this->setCreatedOnFrom( NULL );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on_from', __( 'Max date created must be in mm/dd/yyyy format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCreatedOnTo() {
		$boolIsValid = true;
		$boolValidatedCreatedOnFrom = CValidation::checkDateFormat( trim( $this->getCreatedOnFrom() ), $boolFormat = true );
		$boolValidatedCreatedOnTo = CValidation::checkDateFormat( trim( $this->getCreatedOnTo() ), $boolFormat = true );

		if( true == valStr( trim( $this->getCreatedOnTo() ) ) && false === $boolValidatedCreatedOnTo && 'Max' != $this->getCreatedOnTo() ) {
			$boolIsValid = false;
			$this->setCreatedOnTo( NULL );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on_to', __( 'Min date created must be in mm/dd/yyyy format.' ) ) );
		}

		if( true == valStr( $boolValidatedCreatedOnFrom ) && true == valStr( $boolValidatedCreatedOnTo ) && strtotime( $boolValidatedCreatedOnFrom ) >= strtotime( $boolValidatedCreatedOnTo ) ) {
   			$boolIsValid = false;
   			$this->setCreatedOnFrom( NULL );
   			$this->setCreatedOnTo( NULL );
   			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'created_on_to', __( 'Max date created must be greater than min date created.' ) ) );
		}

		return $boolIsValid;
	}

	public function valScheduledOnDate() {
	    $boolIsValid = true;
	    $strValidatedScheduledOnFrom = CValidation::checkDateFormat( trim( $this->getScheduledOnFrom() ), $boolFormat = true );
	    $strValidatedScheduledOnTo = CValidation::checkDateFormat( trim( $this->getScheduledOnTo() ), $boolFormat = true );

	    if( true == valStr( trim( $this->getScheduledOnTo() ) ) && false === $strValidatedScheduledOnTo && 'Max' != $this->getScheduledOnTo() ) {
	        $boolIsValid = false;
	        $this->setScheduledOnTo( NULL );
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on_to', __( 'Max scheduled date must be in mm/dd/yyyy format.' ) ) );
	    }

	    if( true == valStr( trim( $this->getScheduledOnFrom() ) ) && false === $strValidatedScheduledOnFrom && 'Max' != $this->getScheduledOnFrom() ) {
	        $boolIsValid = false;
	        $this->setScheduledOnFrom( NULL );
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on_from', __( 'Min scheduled date must be in mm/dd/yyyy format.' ) ) );
	    }

	    if( true == valStr( $strValidatedScheduledOnFrom ) && true == valStr( $strValidatedScheduledOnTo ) && strtotime( $strValidatedScheduledOnFrom ) >= strtotime( $strValidatedScheduledOnTo ) ) {
	        $boolIsValid = false;
	        $this->setScheduledOnFrom( NULL );
	        $this->setScheduledOnTo( NULL );
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_date', __( 'Max date scheduled must be greater than min date created.' ) ) );
	    }

	    return $boolIsValid;
	}

	public function valDueOnDate() {
	    $boolIsValid = true;
	    $boolValidatedDueOnFrom = CValidation::checkDateFormat( trim( $this->getDueOnFrom() ), $boolFormat = true );
	    $boolValidatedDueOnTo = CValidation::checkDateFormat( trim( $this->getDueOnTo() ), $boolFormat = true );

	    if( true == valStr( trim( $this->getDueOnTo() ) ) && false === $boolValidatedDueOnTo && 'Max' != $this->getDueOnTo() ) {
	        $boolIsValid = false;
	        $this->setDueOnTo( NULL );
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Due_on_to', __( 'Min date Due must be in mm/dd/yyyy format.' ) ) );
	    }

	    if( true == valStr( $boolValidatedDueOnFrom ) && true == valStr( $boolValidatedDueOnTo ) && strtotime( $boolValidatedDueOnFrom ) >= strtotime( $boolValidatedDueOnTo ) ) {
	        $boolIsValid = false;
	        $this->setDueOnFrom( NULL );
	        $this->setDueOnTo( NULL );
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Due_on_to', __( 'Max date Due must be greater than min date Due.' ) ) );
	    }

	    return $boolIsValid;
	}

	public function valCompletedOnFrom() {
		$boolIsValid = true;
		$boolValidatedCompletedOnFrom = CValidation::checkDateFormat( trim( $this->getCompletedOnFrom() ), $boolFormat = true );

		if( true == valStr( trim( $this->getCompletedOnFrom() ) ) && false === $boolValidatedCompletedOnFrom && 'Min' != $this->getCompletedOnFrom() ) {
			$boolIsValid = false;
			$this->setCompletedOnFrom( NULL );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'completed_on_from', __( 'Min date completed must be in mm/dd/yyyy format.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCompletedOnTo() {
		$boolIsValid = true;
		$boolValidatedCompletedOnFrom = CValidation::checkDateFormat( trim( $this->getCompletedOnFrom() ), $boolFormat = true );
		$boolValidatedCompletedOnTo = CValidation::checkDateFormat( trim( $this->getCompletedOnTo() ), $boolFormat = true );

		if( true == valStr( trim( $this->getCompletedOnTo() ) ) && false === $boolValidatedCompletedOnTo && 'Max' != $this->getCompletedOnTo() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'completed_on_to', __( 'Max date completed must be in mm/dd/yyyy format.' ) ) );
		}

		if( true == valStr( $boolValidatedCompletedOnFrom ) && true == valStr( $boolValidatedCompletedOnTo ) && strtotime( $boolValidatedCompletedOnFrom ) >= strtotime( $boolValidatedCompletedOnTo ) ) {
   			$boolIsValid = false;
   			$this->setCompletedOnFrom( NULL );
   			$this->setCompletedOnTo( NULL );
   			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'completed_on_to', __( 'Max date completed must be greater than min date completed.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPsProductIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderByField() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderByType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsShowSubTasks() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $intCompanyUserId, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCreatedOnFrom();
				$boolIsValid &= $this->valCreatedOnTo();
				$boolIsValid &= $this->valScheduledOnDate();
				$boolIsValid &= $this->valDueOnDate();
				$boolIsValid &= $this->valCompletedOnFrom();
				$boolIsValid &= $this->valCompletedOnTo();
				$boolIsValid &= $this->valName( $intCompanyUserId, $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCreatedOnFrom();
				$boolIsValid &= $this->valCreatedOnTo();
				$boolIsValid &= $this->valScheduledOnDate();
				$boolIsValid &= $this->valDueOnDate();
				$boolIsValid &= $this->valCompletedOnFrom();
				$boolIsValid &= $this->valCompletedOnTo();
				$boolIsValid &= $this->valName( $intCompanyUserId, $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	 /**
	  * Other Functions
	  */

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrmixRequestForm ) ) {
			$arrmixRequestForm = $this->setRequestData( $arrmixRequestForm );
		}

		if( true == valArr( $arrmixFormFields ) ) {
			$arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
		}

		$this->setValues( $arrmixRequestForm, false );
		return;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( 'Min' == $this->getScheduledOnFrom() ) {
		    $this->setScheduledOnFrom( NULL );
		}

		if( 'Max' == $this->getScheduledOnTo() ) {
		    $this->setScheduledOnTo( NULL );
		}

		if( 'Min' == $this->getDueOnFrom() ) {
		    $this->setDueOnFrom( NULL );
		}

		if( 'Max' == $this->getDueOnTo() ) {
		    $this->setDueOnTo( NULL );
		}

		if( true == isset( $arrmixValues['property_group_ids'] ) ) {
			$this->setPropertyGroupIds( $arrmixValues['property_group_ids'] );
		} elseif( false == is_null( $this->getPropertyGroupIds() ) ) {
			$this->setPropertyGroupIds( NULL );
		}

		if( true == isset( $arrmixValues['custom_property_group_ids'] ) ) {
			$this->setCustomPropertyGroupIds( $arrmixValues['custom_property_group_ids'] );
		} elseif( false == is_null( $this->getCustomPropertyGroupIds() ) ) {
			$this->setCustomPropertyGroupIds( NULL );
		}

		if( true == isset( $arrmixValues['maintenance_priority_ids'] ) ) {
			$this->setMaintenancePriorityIds( $arrmixValues['maintenance_priority_ids'] );
		} elseif( false == is_null( $this->getMaintenancePriorityIds() ) ) {
			$this->setMaintenancePriorityIds( NULL );
		}

		if( true == isset( $arrmixValues['maintenance_status_ids'] ) ) {
			if( true == valArr( $arrmixValues['maintenance_status_ids'] ) ) {
				$this->setMaintenanceStatusIds( implode( ',', $arrmixValues['maintenance_status_ids'] ) );
			} elseif( false == empty( $arrmixValues['maintenance_status_ids'] ) ) {
				$this->setMaintenanceStatusIds( $arrmixValues['maintenance_status_ids'] );
			} else {
				$this->setMaintenanceStatusIds( NULL );
			}
		}

		if( true == isset( $arrmixValues['maintenance_category_ids'] ) ) {
			if( true == valArr( $arrmixValues['maintenance_category_ids'] ) ) {
				$this->setMaintenanceCategoryIds( implode( ',', $arrmixValues['maintenance_category_ids'] ) );
			} elseif( false == empty( $arrmixValues['maintenance_category_ids'] ) ) {
				$this->setMaintenanceCategoryIds( $arrmixValues['maintenance_category_ids'] );
			} else {
				$this->setMaintenanceCategoryIds( NULL );
			}
		}

		if( true == isset( $arrmixValues['maintenance_location_ids'] ) ) {
			$this->setMaintenanceLocationIds( $arrmixValues['maintenance_location_ids'] );
		} elseif( false == is_null( $this->getMaintenanceLocationIds() ) ) {
			$this->setMaintenanceLocationIds( NULL );
		}

		if( true == isset( $arrmixValues['maintenance_problem_ids'] ) ) {
			$this->setMaintenanceProblemIds( $arrmixValues['maintenance_problem_ids'] );
		} elseif( false == is_null( $this->getMaintenanceProblemIds() ) ) {
			$this->setMaintenanceProblemIds( NULL );
		}

		if( true == isset( $arrmixValues['ps_product_ids'] ) ) {
			$this->setPsProductIds( $arrmixValues['ps_product_ids'] );
		} elseif( false == is_null( $this->getPsProductIds() ) ) {
			$this->setPsProductIds( NULL );
		}

		if( true == isset( $arrmixValues['company_employee_ids'] ) ) {
			$this->setCompanyEmployeeIds( $arrmixValues['company_employee_ids'] );
		} elseif( false == is_null( $this->getCompanyEmployeeIds() ) ) {
			$this->setCompanyEmployeeIds( NULL );
		}

		if( true == isset( $arrmixValues['ap_payee_ids'] ) ) {
			$this->setApPayeeIds( $arrmixValues['ap_payee_ids'] );
		} elseif( false == is_null( $this->getApPayeeIds() ) ) {
			$this->setApPayeeIds( NULL );
		}

		if( true == isset( $arrmixValues['property_building_ids'] ) ) {
			$this->setPropertyBuildingIds( $arrmixValues['property_building_ids'] );
		} elseif( false == is_null( $this->getPropertyBuildingIds() ) ) {
			$this->setPropertyBuildingIds( NULL );
		}

		if( true == isset( $arrmixValues['unit_type_ids'] ) ) {
			$this->setUnitTypeIds( $arrmixValues['unit_type_ids'] );
		} elseif( false == is_null( $this->getUnitTypeIds() ) ) {
			$this->setUnitTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['building_id'] ) ) {
			$this->setbuildingId( $arrmixValues['building_id'] );
		} elseif( false == is_null( $this->getbuildingId() ) ) {
			$this->setbuildingId( NULL );
		}

		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		} elseif( false == is_null( $this->getPropertyId() ) ) {
			$this->setPropertyId( NULL );
		}

		if( true == isset( $arrmixValues['is_not_assigned'] ) ) {
			$this->setIsNotAssigned( $arrmixValues['is_not_assigned'] );
		}

		if( true == isset( $arrmixValues['is_assigned_to_others'] ) ) {
			$this->setIsAssignedToOthers( $arrmixValues['is_assigned_to_others'] );
		}

		if( true == isset( $arrmixValues['is_search'] ) ) {
			$this->setIsSearch( $arrmixValues['is_search'] );
		}

		if( true == isset( $arrmixValues['last_updated_on_from_date'] ) ) {
			$this->setLastUpdatedOnFromDate( $arrmixValues['last_updated_on_from_date'] );
		}
		if( true == isset( $arrmixValues['last_updated_on_to_date'] ) ) {
			$this->setLastUpdatedOnToDate( $arrmixValues['last_updated_on_to_date'] );
		}
		return;
	}

	public function initializeFilter( $intCompanyUserId, $objDatabase, $boolIsFromResidentProfile = false ) {

	    if( CMaintenanceRequestType::MAKE_READY == $this->getMaintenanceRequestTypeId() ) {
	        $this->m_arrobjEligibleMaintenanceFilters 		= \Psi\Eos\Entrata\CMaintenanceRequestFilters::createService()->fetchMakeReadyFiltersByCompanyUserIdByCid( $intCompanyUserId, $this->getCid(), $objDatabase );
	    } else {
	        $this->m_arrobjEligibleMaintenanceFilters 		= \Psi\Eos\Entrata\CMaintenanceRequestFilters::createService()->fetchMaintenanceRequestFiltersByCompanyUserIdByCid( $intCompanyUserId, $this->getCid(), $objDatabase );
	    }

		$this->m_arrobjEligiblePsProducts = CPsProduct::loadPsProductsByIds( [ CPsProduct::ENTRATA, CPsProduct::RESIDENT_PORTAL, CPsProduct::PMS_INTEGRATION, CPsProduct::LEASING_CENTER, CPsProduct::INSPECTION_MANAGER, CPsProduct::SITE_TABLET ] );

		if( true != $boolIsFromResidentProfile ) {
			$this->m_arrobjAllMaintenancePriorities = \Psi\Eos\Entrata\CMaintenancePriorities::createService()->fetchMaintenancePrioritiesByPropertyIdsByCid( $this->getCompanyUserPropertyIds(), $this->m_intCid, $objDatabase );
			$this->m_arrobjAllMaintenanceStatuses 	= \Psi\Eos\Entrata\CMaintenanceStatuses::createService()->fetchMaintenanceStatusesByPropertyIdsByCid( $this->getCompanyUserPropertyIds(), $this->m_intCid, $objDatabase );
			$this->m_arrobjAllMaintenanceProblems  	= \Psi\Eos\Entrata\CMaintenanceProblems::createService()->fetchCustomMaintenanceProblemsByCid( $this->m_intCid, $objDatabase, CMaintenanceProblem::INCLUDE_IN_MAINTENANCE_REQUESTS );
			$this->m_arrobjAllMaintenanceLocations 	= \Psi\Eos\Entrata\CMaintenanceLocations::createService()->fetchCustomMaintenanceLocationsByCid( $this->m_intCid, $objDatabase );
		}

		$this->m_arrobjEligibleMaintenancePriorities  	= ( array ) $this->m_arrobjAllMaintenancePriorities;
		$this->m_arrobjEligibleMaintenanceStatuses 		= ( array ) $this->m_arrobjAllMaintenanceStatuses;
		$this->m_arrobjEligibleMaintenanceProblems  	= ( array ) $this->m_arrobjAllMaintenanceProblems;
		$this->m_arrobjEligibleMaintenanceLocations 	= ( array ) $this->m_arrobjAllMaintenanceLocations;

		$this->m_intEligibleMaintenancePrioritiesCount	= \Psi\Libraries\UtilFunctions\count( $this->m_arrobjEligibleMaintenancePriorities );
		$this->m_intEligibleMaintenanceStatusesCount	= \Psi\Libraries\UtilFunctions\count( $this->m_arrobjEligibleMaintenanceStatuses );
		$this->m_intEligibleMaintenanceProblemsCount	= \Psi\Libraries\UtilFunctions\count( $this->m_arrobjEligibleMaintenanceProblems );
		$this->m_intEligibleMaintenanceLocationsCount	= \Psi\Libraries\UtilFunctions\count( $this->m_arrobjEligibleMaintenanceLocations );

		// Set default priorities
		if( false == valArr( $this->getMaintenancePriorityIds() ) && true == valArr( $this->m_arrobjAllMaintenancePriorities ) ) {
			foreach( $this->m_arrobjAllMaintenancePriorities as $objMaintenancePriority ) {
				if( true == $objMaintenancePriority->getIsPublished() && false == preg_match( '/Disabled/', $objMaintenancePriority->getFilterPriority() ) ) {
					$arrintMaintenancePriorityIds[] = $objMaintenancePriority->getId();
				}
			}

			if( true == valArr( $arrintMaintenancePriorityIds ) ) {
				$this->m_strMaintenancePriorityIds = $this->flattenIds( $arrintMaintenancePriorityIds );
			}
		}
	}

	public function setRequestData( $arrmixRequestForm = [] ) {
		$arrmixRequestForm['property_group_ids']				 = ( true == isset( $arrmixRequestForm['property_group_ids'] ) && true == valStr( $arrmixRequestForm['property_group_ids'] ) ) ? explode( ',', $arrmixRequestForm['property_group_ids'] ) : '';
		$arrmixRequestForm['maintenance_status_ids']			 = ( true == isset( $arrmixRequestForm['maintenance_status_ids'] ) && true == valArr( $arrmixRequestForm['maintenance_status_ids'] ) )	? implode( ',', $arrmixRequestForm['maintenance_status_ids'] ) : '';
		$arrmixRequestForm['company_employee_ids']				 = ( true == isset( $arrmixRequestForm['company_employee_ids'] ) && true == valArr( $arrmixRequestForm['company_employee_ids'] ) )	? implode( ',', $arrmixRequestForm['company_employee_ids'] ) : '';
		$arrmixRequestForm['maintenance_priority_ids']			 = ( true == isset( $arrmixRequestForm['maintenance_priority_ids'] ) && true == valArr( $arrmixRequestForm['maintenance_priority_ids'] ) ) ? implode( ',', $arrmixRequestForm['maintenance_priority_ids'] ) : '';
		$arrmixRequestForm['maintenance_problem_ids']			 = ( true == isset( $arrmixRequestForm['maintenance_problem_ids'] ) && true == valArr( $arrmixRequestForm['maintenance_problem_ids'] ) ) ? implode( ',', $arrmixRequestForm['maintenance_problem_ids'] ) : '';
		$arrmixRequestForm['maintenance_location_ids']			 = ( true == isset( $arrmixRequestForm['maintenance_location_ids'] ) && true == valArr( $arrmixRequestForm['maintenance_location_ids'] ) ) ? implode( ',', $arrmixRequestForm['maintenance_location_ids'] ) : '';
		$arrmixRequestForm['property_building_ids']				 = ( true == isset( $arrmixRequestForm['property_building_ids'] ) && true == valArr( $arrmixRequestForm['property_building_ids'] ) ) ? implode( ',', $arrmixRequestForm['property_building_ids'] ) : '';
		$arrmixRequestForm['unit_type_ids']						 = ( true == isset( $arrmixRequestForm['unit_type_ids'] ) && true == valArr( $arrmixRequestForm['unit_type_ids'] ) ) ? implode( ',', $arrmixRequestForm['unit_type_ids'] ) : '';
		$arrmixRequestForm['ps_product_ids']					 = ( true == isset( $arrmixRequestForm['ps_product_ids'] ) && true == valArr( $arrmixRequestForm['ps_product_ids'] ) )	? implode( ',', $arrmixRequestForm['ps_product_ids'] ) : '';
		$arrmixRequestForm['maintenance_status_type_ids']		 = ( true == isset( $arrmixRequestForm['maintenance_status_type_ids'] ) && true == valArr( $arrmixRequestForm['maintenance_status_type_ids'] ) ) ? implode( ',', $arrmixRequestForm['maintenance_status_type_ids'] ) : '';
		$arrmixRequestForm['maintenance_request_type_ids']		 = ( true == isset( $arrmixRequestForm['maintenance_status_type_ids'] ) && true == valArr( $arrmixRequestForm['maintenance_status_type_ids'] ) ) ? implode( ',', $arrmixRequestForm['maintenance_status_type_ids'] ) : '';
		$arrmixRequestForm['is_not_assigned']					 = ( true == isset( $arrmixRequestForm['is_not_assigned'] ) && true == valArr( $arrmixRequestForm['is_not_assigned'] ) ) ? implode( ',', $arrmixRequestForm['is_not_assigned'] ) : false;
		$arrmixRequestForm['is_assigned_to_others']				 = ( true == isset( $arrmixRequestForm['is_assigned_to_others'] ) && true == valArr( $arrmixRequestForm['is_assigned_to_others'] ) ) ? implode( ',', $arrmixRequestForm['is_assigned_to_others'] ) : false;

		return $arrmixRequestForm;
	}

	public function flattenIds( $arrintIds ) {
	 	if ( false == valArr( $arrintIds ) ) return '';
		return implode( ',', $arrintIds );
	}

	public function expandIds( $strIds ) {
		return array_filter( explode( ',', $strIds ) );
	}

	public function getValues() {

		if( true == isset( $this->m_strPropertyGroupIds ) )					$arrmixValues['property_group_ids'] = $this->m_strPropertyGroupIds;
		if( true == isset( $this->m_strCustomPropertyGroupIds ) )			$arrmixValues['custom_property_group_ids'] = $this->m_strCustomPropertyGroupIds;
		if( true == isset( $this->m_strMaintenancePriorityIds ) )			$arrmixValues['maintenance_priority_ids'] = $this->m_strMaintenancePriorityIds;
		if( true == isset( $this->m_strMaintenanceStatusIds ) )				$arrmixValues['maintenance_status_ids'] = $this->m_strMaintenanceStatusIds;
		if( true == isset( $this->m_strMaintenanceLocationIds ) )			$arrmixValues['maintenance_location_ids'] = $this->m_strMaintenanceLocationIds;
		if( true == isset( $this->m_strMaintenanceProblemIds ) )			$arrmixValues['maintenance_problem_ids'] = $this->m_strMaintenanceProblemIds;
		if( true == isset( $this->m_strPsProductIds ) ) 					$arrmixValues['ps_product_ids'] = $this->m_strPsProductIds;
		if( true == isset( $this->m_strCompanyEmployeeIds ) )				$arrmixValues['company_employee_ids'] = $this->m_strCompanyEmployeeIds;
		if( true == isset( $this->m_strApPayeeIds ) )						$arrmixValues['ap_payee_ids'] = $this->m_strApPayeeIds;

		if( true == isset( $this->m_intId ) )								$arrmixValues['id'] = $this->m_intId;
		if( true == isset( $this->m_intCid ) )								$arrmixValues['cid'] = $this->m_intCid;
		if( true == isset( $this->m_strName ) )								$arrmixValues['name'] = $this->m_strName;
		if( true == isset( $this->m_strSearchKeyword ) )					$arrmixValues['search_keyword'] = $this->m_strSearchKeyword;
		if( true == isset( $this->m_strMaintenanceRequestTypeIds ) )		$arrmixValues['maintenance_request_type_ids'] = $this->m_strMaintenanceRequestTypeIds;
		if( true == isset( $this->m_strUnitSpaceStatusTypeIds ) )			$arrmixValues['unit_space_status_type_ids'] = $this->m_strUnitSpaceStatusTypeIds;
		if( true == isset( $this->m_strOrderByField ) )						$arrmixValues['order_by_field'] = $this->m_strOrderByField;
		if( true == isset( $this->m_intOrderByType ) )						$arrmixValues['order_by_type'] = $this->m_intOrderByType;
		if( true == isset( $this->m_strCreatedOnFrom ) )					$arrmixValues['created_on_from'] = $this->m_strCreatedOnFrom;
		if( true == isset( $this->m_strCreatedOnTo ) ) 						$arrmixValues['created_on_to'] = $this->m_strCreatedOnTo;
		if( true == isset( $this->m_strCompletedOnFrom ) )					$arrmixValues['completed_on_from'] = $this->m_strCompletedOnFrom;
		if( true == isset( $this->m_strCompletedOnTo ) )					$arrmixValues['completed_on_to'] = $this->m_strCompletedOnTo;
		if( true == isset( $this->m_intIsShowSubTasks ) )					$arrmixValues['is_show_sub_tasks'] = $this->m_intIsShowSubTasks;
		if( true == isset( $this->m_intDeletedBy ) )						$arrmixValues['deleted_by'] = $this->m_intDeletedBy;
		if( true == isset( $this->m_strDeletedOn ) )						$arrmixValues['deleted_on'] = $this->m_strDeletedOn;
		if( true == isset( $this->m_intUpdatedBy ) )						$arrmixValues['updated_by'] = $this->m_intUpdatedB;
		if( true == isset( $this->m_strUpdatedOn ) )						$arrmixValues['updated_on'] = $this->m_strUpdatedOn;
		if( true == isset( $this->m_intCreatedBy ) )						$arrmixValues['created_by'] = $this->m_intCreatedBy;
		if( true == isset( $this->m_strCreatedOn ) )						$arrmixValues['created_on'] = $this->m_strCreatedOn;
		if( true == isset( $this->m_intPropertyId ) )						$arrmixValues['property_id'] = $this->m_intPropertyId;
		if( true == isset( $this->m_intBuildingId ) )						$arrmixValues['building_id'] = $this->m_intBuildingId;

		if( true == isset( $this->m_strPropertyBuildingIds ) )				$arrmixValues['property_building_ids'] = $this->m_strPropertyBuildingIds;
		if( true == isset( $this->m_strUnitTypeIds ) )						$arrmixValues['unit_type_ids'] = $this->m_strUnitTypeIds;
		if( true == isset( $this->m_strScheduledEndFrom ) )					$arrmixValues['scheduled_end_from'] = $this->m_strScheduledEndFrom;
		if( true == isset( $this->m_strScheduledEndTo ) )					$arrmixValues['scheduled_end_to'] = $this->m_strScheduledEndTo;
		if( true == isset( $this->m_strMoveInFrom ) )						$arrmixValues['move_in_from'] = $this->m_strMoveInFrom;
		if( true == isset( $this->m_strMoveInTo ) )							$arrmixValues['move_in_to'] = $this->m_strMoveInTo;
		if( true == isset( $this->m_strMoveOutFrom ) )						$arrmixValues['move_out_from'] = $this->m_strMoveOutFrom;
		if( true == isset( $this->m_strMoveOutTo ) )						$arrmixValues['move_out_to'] = $this->m_strMoveOutTo;
		if( true == isset( $this->m_strSubtaskScheduledEndFrom ) )			$arrmixValues['subtask_scheduled_end_from'] = $this->m_strSubtaskScheduledEndFrom;
		if( true == isset( $this->m_strSubtaskScheduledEndTo ) )			$arrmixValues['subtask_scheduled_end_to'] = $this->m_strSubtaskScheduledEndTo;
		if( true == isset( $this->m_boolIsSearch ) )			$arrmixValues['is_search'] = $this->m_boolIsSearch;

		return $arrmixValues;
	}

}
?>