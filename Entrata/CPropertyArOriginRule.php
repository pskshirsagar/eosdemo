<?php

class CPropertyArOriginRule extends CBasePropertyArOriginRule {

	// This variable is used for passing pRestrictToAllowedRates to update_rates_allowed() function
	private $m_boolIsRestrictToAllowedRates = true;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valArOriginId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowPerItem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowLookups() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowPercentCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowPercentCodeGroup() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowPercentCodeType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRenewalSameAsProspect() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRenewalPercentageOfPrior() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseOneTimeLeaseTerms() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseRecurringLeaseTerms() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseOneTimeLeaseStartWindows() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseRecurringLeaseStartWindows() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseOneTimeSpaceConfigurations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUseRecurringSpaceConfigurations() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRateOffsets() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRateSchedulingForRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRateSchedulingForDeposits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRateSchedulingForOther() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowRateCaps() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowPreQualificationTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowApplicationApprovalTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowLeaseApprovalTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowLastLeaseMonthTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowAllowNoticeTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowAnniversaryOfMoveInTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowEndOfYearTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowFinalStatementTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowHourlyTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowDailyTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowWeeklyTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowEveryTwoWeeksTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowTwicePerMonthTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowSpecificMonthsTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowQuarterlyTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowTwicePerYearTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowYearlyTrigger() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsStudent = false, $boolIsPricing = false, $boolIsMilitary = false, $boolIsConventional = false, $boolIsHospitality = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'update_settings':
				$boolIsValid &= $this->valUseLeaseTermsForRent( $boolIsPricing );
				$boolIsValid &= $this->valUseLeaseTermsForDeposits();
				$boolIsValid &= $this->valUseLeaseTermsForOther();

				$boolIsValid &= $this->valUseLeaseStartWindowsForRent( $boolIsStudent, $boolIsPricing );
				$boolIsValid &= $this->valUseLeaseStartWindowsForDeposits();
				$boolIsValid &= $this->valUseLeaseStartWindowsForOther();

				$boolIsValid &= $this->valUseSpaceConfigurationsForRent( $boolIsStudent, $boolIsMilitary, $boolIsConventional, $boolIsHospitality );
				$boolIsValid &= $this->valUseSpaceConfigurationsForDeposits( $boolIsStudent, $boolIsMilitary, $boolIsConventional, $boolIsHospitality );
				$boolIsValid &= $this->valUseSpaceConfigurationsForOther( $boolIsStudent, $boolIsMilitary, $boolIsConventional, $boolIsHospitality );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valUseLeaseTermsForRent( $boolIsPricing ) {
		$boolIsValid = true;

		if( true == $this->getUseLeaseTermsForRent() && true == in_array( $this->getArOriginId(), [ CArOrigin::PET, CArOrigin::MAINTENANCE ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_terms_for_rent', __( 'Price By Lease Term for {%s,0} Rent must be No.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );

		} elseif( false == $this->getUseLeaseTermsForRent() && CArOrigin::BASE == $this->getArOriginId() && true == $boolIsPricing ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_terms_for_rent', __( 'Price By Lease Term for {%s,0} Rent must be Yes.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );

		}

		return $boolIsValid;
	}

	public function valUseLeaseTermsForDeposits() {
		$boolIsValid = true;

		if( true == $this->getUseLeaseTermsForDeposits() && true == in_array( $this->getArOriginId(), [ CArOrigin::PET, CArOrigin::MAINTENANCE ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_terms_for_deposit', __( 'Price By Lease Term for {%s,0} Deposit must be No.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );
		}

		return $boolIsValid;
	}

	public function valUseLeaseTermsForOther() {
		$boolIsValid = true;

		if( true == $this->getUseLeaseTermsForOther() && true == in_array( $this->getArOriginId(), [ CArOrigin::PET, CArOrigin::MAINTENANCE ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_terms_for_other', __( 'Price By Lease Term for {%s,0} Income must be No.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );
		}

		return $boolIsValid;
	}

	public function valUseLeaseStartWindowsForRent( $boolIsStudent, $boolIsPricing ) {
		$boolIsValid = true;

		if( false == $this->getUseLeaseStartWindowsForRent() && CArOrigin::BASE == $this->getArOriginId() && true == $boolIsPricing && false == $boolIsStudent ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_start_window_for_rent', __( 'Enable Revenue Management for {%s,0} Rent must be Yes.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );

		} elseif( true == $this->getUseLeaseStartWindowsForRent() && false == $boolIsPricing ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_start_window_for_rent', __( 'Enable Revenue Management for {%s,0} Rent must be No. ', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );

		} elseif( true == $this->getUseLeaseStartWindowsForRent() && true == $this->getAllowRateSchedulingForRent() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_start_window_for_rent', __( 'Enable Revenue Management for {%s,0} Rent cannot be enabled if rate scheduling is also enabled.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );
		}

		return $boolIsValid;
	}

	public function valUseLeaseStartWindowsForDeposits() {
		$boolIsValid = true;

		if( true == $this->getUseLeaseStartWindowsForDeposits() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_start_window_for_deposit', __( 'Enable Revenue Management for {%s,0} Deposit must be No.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );

		} elseif( true == $this->getUseLeaseStartWindowsForDeposits() && true == $this->getAllowRateSchedulingForDeposits() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_start_window_for_deposit', __( 'Enable Revenue Management for {%s,0} Deposit cannot be enabled if rate scheduling is also enabled.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );
		}

		return $boolIsValid;
	}

	public function valUseLeaseStartWindowsForOther() {
		$boolIsValid = true;

		if( true == $this->getUseLeaseStartWindowsForOther() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_start_window_for_other', __( 'Enable Revenue Management for {%s,0} Income must be No.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );

		} elseif( true == $this->getUseLeaseStartWindowsForOther() && true == $this->getAllowRateSchedulingForOther() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_lease_start_window_for_other', __( 'Enable Revenue Management for {%s,0} Income cannot be enabled if rate scheduling is also enabled.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );
		}

		return $boolIsValid;
	}

	public function valUseSpaceConfigurationsForRent( $boolIsStudent, $boolIsMilitary, $boolIsConventional, $boolIsHospitality ) {
		$boolIsValid = true;

		if( ( true == $boolIsStudent || true == $boolIsMilitary || true == $boolIsConventional || true == $boolIsHospitality ) && true == in_array( $this->getArOriginId(), [ CArOrigin::BASE, CArOrigin::SPECIAL ] ) ) return true;

		if( true == $this->getUseSpaceConfigurationsForRent() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_space_configurations_for_rent', __( 'Price By Space Configuration for {%s,0} Rent must be No.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );
		}

		return $boolIsValid;
	}

	public function valUseSpaceConfigurationsForDeposits( $boolIsStudent, $boolIsMilitary, $boolIsConventional, $boolIsHospitality ) {
		$boolIsValid = true;

		if( ( true == $boolIsStudent || true == $boolIsMilitary || true == $boolIsConventional || true == $boolIsHospitality ) && CArOrigin::BASE == $this->getArOriginId() ) return true;

		if( true == $this->getUseSpaceConfigurationsForDeposits() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_space_configurations_for_deposit', __( 'Price By Space Configuration for {%s,0} Deposit must be No.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );
		}

		return $boolIsValid;
	}

	public function valUseSpaceConfigurationsForOther( $boolIsStudent, $boolIsMilitary, $boolIsConventional, $boolIsHospitality ) {
		$boolIsValid = true;

		if( ( true == $boolIsStudent || true == $boolIsMilitary || true == $boolIsConventional || true == $boolIsHospitality ) && true == in_array( $this->getArOriginId(), [ CArOrigin::BASE, CArOrigin::SPECIAL ] ) ) return true;

		if( true == $this->getUseSpaceConfigurationsForOther() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'use_space_configurations_for_other', __( 'Price By Space Configuration for {%s,0} Income must be No.', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] ) ) );
		}

		return $boolIsValid;
	}

	public function validateLeaseStartWindows() {

		$boolIsValid = false;

		switch( NULL ) {
			default:
				$boolIsValid = $this->getUseLeaseStartWindowsForRent();
				if( true == $boolIsValid ) {
					break;
				}

				$boolIsValid = $this->getUseLeaseStartWindowsForDeposits();
				if( true == $boolIsValid ) {
					break;
				}

				$boolIsValid = $this->getUseLeaseStartWindowsForOther();
				if( true == $boolIsValid ) {
					break;
				}
				break;
		}

		return $boolIsValid;
	}

	public function validateSpaceConfigurations() {

		$boolIsValid = false;

		switch( NULL ) {
			default:
				$boolIsValid = $this->getUseSpaceConfigurationsForRent();
				if( true == $boolIsValid ) {
					break;
				}

				$boolIsValid = $this->getUseSpaceConfigurationsForDeposits();
				if( true == $boolIsValid ) {
					break;
				}

				$boolIsValid = $this->getUseSpaceConfigurationsForOther();
				if( true == $boolIsValid ) {
					break;
				}
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getArOriginName() {
		return $this->m_strArOriginName;
	}

	public function getIsRestrictToAllowedRates() {
		return $this->m_boolIsRestrictToAllowedRates;
	}

	/**
	 * Set Functions
	 */

	public function setArOriginName( $strArOriginName ) {
		$this->m_strArOriginName = $strArOriginName;
	}

	public function setIsRestrictToAllowedRates( $boolIsRestrictToAllowedRates ) {
		$this->m_boolIsRestrictToAllowedRates = $boolIsRestrictToAllowedRates;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_origin_name'] ) ) $this->setArOriginName( $arrmixValues['ar_origin_name'] );

		return;
	}

	/**
	 * Over-ridden function
	 * @see CBasePropertyArOriginRule::insert()
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolCallUpdateRatesAllowed = true, $boolIsPreview = false, $arrintPropertyFloorplanIds = [], $arrintUnitTypeIds = [], $arrintUnitSpaceIds = [], $arrintArOriginIds = [], $arrintArCodeTypeIds = [], $arrintArTriggerIds = [], $arrintArFormulaIds = [], $arrintRateIds = [] ) {

		if( true == $boolReturnSqlOnly ) {
			$strUpdateRatesAllowedSql = '';

			// Call the parent insert and then call the function update_rates_allowed() as its supposed to be after insert update trigger.
			$strSql = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			// Call updateRatesAllowed if required
			if( true == $boolCallUpdateRatesAllowed ) {
				$strUpdateRatesAllowedSql = \Psi\Eos\Entrata\CRates::createService()->updateRatesAllowed( $objDatabase, $this->getCid(), $this->getPropertyId(), $intCurrentUserId, $this->getIsRestrictToAllowedRates(), $boolReturnSqlOnly );
			}

			return $strSql . PHP_EOL . $strUpdateRatesAllowedSql;
		}

		$boolIsValid = true;
		$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		// Check if call to updateRatesAllowed if required
		if( true == $boolCallUpdateRatesAllowed ) {
			$boolIsValid &= \Psi\Eos\Entrata\CRates::createService()->updateRatesAllowed( $objDatabase, $this->getCid(), $this->getPropertyId(), $intCurrentUserId, $this->getIsRestrictToAllowedRates(), $boolReturnSqlOnly, $boolIsPreview, $arrintPropertyFloorplanIds, $arrintUnitTypeIds, $arrintUnitSpaceIds, $arrintArOriginIds, $arrintArCodeTypeIds, $arrintArTriggerIds, $arrintArFormulaIds, $arrintRateIds );
		}

		return $boolIsValid;
	}

	/**
	 * Over-ridden function
	 * @see CBasePropertyArOriginRule::update()
	 */

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolCallUpdateRatesAllowed = true, $boolIsPreview = false, $arrintPropertyFloorplanIds = [], $arrintUnitTypeIds = [], $arrintUnitSpaceIds = [], $arrintArOriginIds = [], $arrintArCodeTypeIds = [], $arrintArTriggerIds = [], $arrintArFormulaIds = [], $arrintRateIds = [] ) {

		$arrintResponse = fetchData( 'SELECT * FROM property_ar_origin_rules WHERE id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid(), $objDatabase );

		$this->setOriginalValues( $arrintResponse[0] );
		$this->setSerializedOriginalValues( serialize( $arrintResponse[0] ) );

		if( true == $boolReturnSqlOnly ) {
			$strUpdateRatesAllowedSql = '';

			// Call the parent update and then call the function update_rates_allowed() as its supposed to be after insert update trigger.
			$strSql = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			// Call updateRatesAllowed if required
			if( true == $boolCallUpdateRatesAllowed ) {
				$strUpdateRatesAllowedSql = \Psi\Eos\Entrata\CRates::createService()->updateRatesAllowed( $objDatabase, $this->getCid(), $this->getPropertyId(), $intCurrentUserId, $this->getIsRestrictToAllowedRates(), $boolReturnSqlOnly );
			}

			$strSql .= PHP_EOL . $this->insertTableLogs( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			return $strSql . PHP_EOL . $strUpdateRatesAllowedSql;
		}

		$boolIsValid = true;
		$boolIsValid = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		// Check if call to updateRatesAllowed if required
		if( true == $boolCallUpdateRatesAllowed ) {
			$boolIsValid &= \Psi\Eos\Entrata\CRates::createService()->updateRatesAllowed( $objDatabase, $this->getCid(), $this->getPropertyId(), $intCurrentUserId, $this->getIsRestrictToAllowedRates(), $boolReturnSqlOnly, $boolIsPreview, $arrintPropertyFloorplanIds, $arrintUnitTypeIds, $arrintUnitSpaceIds, $arrintArOriginIds, $arrintArCodeTypeIds, $arrintArTriggerIds, $arrintArFormulaIds, $arrintRateIds );
		}

		$boolIsValid &= $this->insertTableLogs( $intCurrentUserId, $objDatabase );

		return $boolIsValid;
	}

	public function insertTableLogs( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strOldValuePair	= NULL;
		$strNewValuePair	= NULL;
		$strSql				= '';

		if( $this->convertBoolToString( $this->getAllowPerItem() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_per_item' ) ) ) {
			$strOldValuePair .= 'allow_per_item::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_per_item' ) ) . '~^~';
			$strNewValuePair .= 'allow_per_item::' . $this->convertBoolToString( $this->getAllowPerItem() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowVolumeLookups() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_volume_lookups' ) ) ) {
			$strOldValuePair .= 'allow_volume_lookups::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_volume_lookups' ) ) . '~^~';
			$strNewValuePair .= 'allow_volume_lookups::' . $this->convertBoolToString( $this->getAllowVolumeLookups() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowTieredLookups() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_tiered_lookups' ) ) ) {
			$strOldValuePair .= 'allow_tiered_lookups::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_tiered_lookups' ) ) . '~^~';
			$strNewValuePair .= 'allow_tiered_lookups::' . $this->convertBoolToString( $this->getAllowTieredLookups() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowPercentCode() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_percent_code' ) ) ) {
			$strOldValuePair .= 'allow_percent_code::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_percent_code' ) ) . '~^~';
			$strNewValuePair .= 'allow_percent_code::' . $this->convertBoolToString( $this->getAllowPercentCode() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowPercentCodeGroup() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_percent_code_group' ) ) ) {
			$strOldValuePair .= 'allow_percent_code_group::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_percent_code_group' ) ) . '~^~';
			$strNewValuePair .= 'allow_percent_code_group::' . $this->convertBoolToString( $this->getAllowPercentCodeGroup() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowPercentCodeType() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_percent_code_type' ) ) ) {
			$strOldValuePair .= 'allow_percent_code_type::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_percent_code_type' ) ) . '~^~';
			$strNewValuePair .= 'allow_percent_code_type::' . $this->convertBoolToString( $this->getAllowPercentCodeType() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowRateSchedulingForRent() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_rate_scheduling_for_rent' ) ) ) {
			$strOldValuePair .= 'allow_rate_scheduling_for_rent::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_rate_scheduling_for_rent' ) ) . '~^~';
			$strNewValuePair .= 'allow_rate_scheduling_for_rent::' . $this->convertBoolToString( $this->getAllowRateSchedulingForRent() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowRateSchedulingForDeposits() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_rate_scheduling_for_deposits' ) ) ) {
			$strOldValuePair .= 'allow_rate_scheduling_for_deposits::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_rate_scheduling_for_deposits' ) ) . '~^~';
			$strNewValuePair .= 'allow_rate_scheduling_for_deposits::' . $this->convertBoolToString( $this->getAllowRateSchedulingForDeposits() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getAllowRateSchedulingForOther() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_rate_scheduling_for_other' ) ) ) {
			$strOldValuePair .= 'allow_rate_scheduling_for_other::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'allow_rate_scheduling_for_other' ) ) . '~^~';
			$strNewValuePair .= 'allow_rate_scheduling_for_other::' . $this->convertBoolToString( $this->getAllowRateSchedulingForOther() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseLeaseTermsForRent() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_terms_for_rent' ) ) ) {
			$strOldValuePair .= 'use_lease_terms_for_rent::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_terms_for_rent' ) ) . '~^~';
			$strNewValuePair .= 'use_lease_terms_for_rent::' . $this->convertBoolToString( $this->getUseLeaseTermsForRent() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseLeaseTermsForDeposits() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_terms_for_deposits' ) ) ) {
			$strOldValuePair .= 'use_lease_terms_for_deposits::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_terms_for_deposits' ) ) . '~^~';
			$strNewValuePair .= 'use_lease_terms_for_deposits::' . $this->convertBoolToString( $this->getUseLeaseTermsForDeposits() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseLeaseTermsForOther() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_terms_for_other' ) ) ) {
			$strOldValuePair .= 'use_lease_terms_for_other::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_terms_for_other' ) ) . '~^~';
			$strNewValuePair .= 'use_lease_terms_for_other::' . $this->convertBoolToString( $this->getUseLeaseTermsForOther() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseLeaseStartWindowsForRent() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_start_windows_for_rent' ) ) ) {
			$strOldValuePair .= 'use_lease_start_windows_for_rent::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_start_windows_for_rent' ) ) . '~^~';
			$strNewValuePair .= 'use_lease_start_windows_for_rent::' . $this->convertBoolToString( $this->getUseLeaseStartWindowsForRent() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseLeaseStartWindowsForDeposits() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_start_windows_for_deposits' ) ) ) {
			$strOldValuePair .= 'use_lease_start_windows_for_deposits::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_start_windows_for_deposits' ) ) . '~^~';
			$strNewValuePair .= 'use_lease_start_windows_for_deposits::' . $this->convertBoolToString( $this->getUseLeaseStartWindowsForDeposits() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseLeaseStartWindowsForOther() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_start_windows_for_other' ) ) ) {
			$strOldValuePair .= 'use_lease_start_windows_for_other::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_lease_start_windows_for_other' ) ) . '~^~';
			$strNewValuePair .= 'use_lease_start_windows_for_other::' . $this->convertBoolToString( $this->getUseLeaseStartWindowsForOther() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseSpaceConfigurationsForRent() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_space_configurations_for_rent' ) ) ) {
			$strOldValuePair .= 'use_space_configurations_for_rent::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_space_configurations_for_rent' ) ) . '~^~';
			$strNewValuePair .= 'use_space_configurations_for_rent::' . $this->convertBoolToString( $this->getUseSpaceConfigurationsForRent() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseSpaceConfigurationsForDeposits() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_space_configurations_for_deposits' ) ) ) {
			$strOldValuePair .= 'use_space_configurations_for_deposits::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_space_configurations_for_deposits' ) ) . '~^~';
			$strNewValuePair .= 'use_space_configurations_for_deposits::' . $this->convertBoolToString( $this->getUseSpaceConfigurationsForDeposits() ) . '~^~';
		}

		if( $this->convertBoolToString( $this->getUseSpaceConfigurationsForOther() ) != $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_space_configurations_for_other' ) ) ) {
			$strOldValuePair .= 'use_space_configurations_for_other::' . $this->convertBoolToString( $this->getOriginalValueByFieldName( 'use_space_configurations_for_other' ) ) . '~^~';
			$strNewValuePair .= 'use_space_configurations_for_other::' . $this->convertBoolToString( $this->getUseSpaceConfigurationsForOther() ) . '~^~';
		}

		if( false == is_null( $strNewValuePair ) || false == is_null( $strOldValuePair ) ) {

			$strOldValuePair = '0ar_origin_id::' . \Psi\CStringService::singleton()->strtolower( CArOrigin::getIdToCamelizeName( $this->getArOriginId() ) ) . '~^~' . $strOldValuePair;
			$strNewValuePair = '0ar_origin_id::' . \Psi\CStringService::singleton()->strtoupper( CArOrigin::getIdToCamelizeName( $this->getArOriginId() ) ) . '~^~' . $strNewValuePair;

			$strDescription = __( '{%s, 0} pricing settings has been modified', [ CArOrigin::createService()->getArOrignNames()[$this->getArOriginId()] ] );

			$objTableLog = new CTableLog();
			$strReturnValue = $objTableLog->insert( $intCurrentUserId, $objDatabase, 'property_ar_origin_rules', $this->getPropertyId(), 'UPDATE', $this->getCid(), $strOldValuePair, $strNewValuePair, $strDescription, $boolReturnSqlOnly );

			if( true == $boolReturnSqlOnly ) {
				$strSql .= $strReturnValue;
			}
		}

		return( true == $boolReturnSqlOnly ) ? $strSql : true;
	}

	private function convertBoolToString( $mixInputValue ) {

		$strReturnValue = '';

		if( 'boolean' === gettype( $mixInputValue ) ) {
			$strReturnValue = ( false != $mixInputValue ) ? __( 'Yes' ) : __( 'No' );
		} elseif( 'string' === gettype( $mixInputValue ) ) {
			$strReturnValue = ( 'f' != $mixInputValue ) ? __( 'Yes' ) : __( 'No' );
		}

		settype( $strReturnValue, 'string' );

		return $strReturnValue;
	}

}
?>