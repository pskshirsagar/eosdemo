<?php

class CCustomerRelationship extends CBaseCustomerRelationship {

	protected $m_intOccupancyTypeId;

	const CUSTOMER_RELATIONSHIP_PRIMARY				= 1;
	const CUSTOMER_CHILD							= 4;
	const CUSTOMER_RELATIONSHIP_HEAD_OF_HOUSEHOLD	= 7;
	const CUSTOMER_RELATIONSHIP_SPOUCE				= 8;
	const CUSTOMER_RELATIONSHIP_CO_HEAD				= 9;
	const CUSTOMER_RELATIONSHIP_DEPENDENT			= 10;
	const CUSTOMER_FOSTER_CHILD						= 12;

	public static $c_arrintDefaultCustomerRelationshipIds	= [ self::CUSTOMER_RELATIONSHIP_HEAD_OF_HOUSEHOLD, self::CUSTOMER_RELATIONSHIP_SPOUCE, CCustomerRelationship::CUSTOMER_RELATIONSHIP_CO_HEAD ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDefaultCustomerRelationshipId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerRelationshipGroupId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerRelationshipGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'customer relationship group is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_type_id', __( 'Please select system type.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please add occupant type name.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMarketingName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowInternally() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valShowExternally() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedApplications( $objDatabase ) {

		$boolIsValid = true;

		$arrobjApplicantApplications = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByCustomerRelationshipIdByCid( $this->m_intId, $this->getCid(), $objDatabase );

		if( true == valArr( $arrobjApplicantApplications ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s, name} cannot be deleted, it is being associated with application.', [ 'name' => $this->m_strName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valAssociatedRates( $objDatabase ) {

		$boolIsValid = true;

		$strWhereConditions = ' WHERE cid = ' . ( int ) $this->getCid() . ' AND customer_relationship_id = ' . ( int ) $this->getId();

		$intRateCount = \Psi\Eos\Entrata\CRates::createService()->fetchRateCount( $strWhereConditions, $objDatabase );

		if( 0 < $intRateCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s, name} can not be unpublished, it is being associated with rates.', [ 'name' => $this->m_strName ] ) ) );
		}

		return $boolIsValid;
	}

	public function valDeleteCustomerRelationships( $objDatabase, $boolIncludeDeletedAA = false ) {
		$strCheckDeletedAASql = ( false == $boolIncludeDeletedAA ) ? ' AND deleted_on IS NULL' : '';

		$boolIsValid = true;

		$strSql = 'SELECT COUNT(id) FROM applicant_applications WHERE cid = ' . ( int ) $this->getCid() . ' AND customer_relationship_id = ' . ( int ) $this->getId() . $strCheckDeletedAASql;
		$arrmixData = fetchData( $strSql, $objDatabase );

		$intApplicationsCount = ( true == isset( $arrmixData[0]['count'] ) ) ? ( int ) $arrmixData[0]['count'] : 0;

		$strSql = 'SELECT COUNT(id) FROM lease_customers WHERE cid = ' . ( int ) $this->getCid() . ' AND customer_relationship_id = ' . ( int ) $this->getId();
		$arrmixData = fetchData( $strSql, $objDatabase );

		$intLeasesCount = ( true == isset( $arrmixData[0]['count'] ) ) ? ( int ) $arrmixData[0]['count'] : 0;

		$strSql = 'SELECT COUNT(id) FROM rates WHERE cid = ' . ( int ) $this->getCid() . ' AND customer_relationship_id = ' . ( int ) $this->getId();
		$arrmixData = fetchData( $strSql, $objDatabase );

		$intRatesCount = ( true == isset( $arrmixData[0]['count'] ) ) ? ( int ) $arrmixData[0]['count'] : 0;

		if( 0 < ( $intApplicationsCount + $intLeasesCount + $intRatesCount ) ) {
			$boolIsValid = false;

			( 0 < $intApplicationsCount ) ? $arrstrAssociations[] 	= __( 'applications' ) : '';
			( 0 < $intLeasesCount ) ? $arrstrAssociations[] 		= __( 'leases' ) 		 : '';
			( 0 < $intRatesCount ) ? $arrstrAssociations[] 			= __( 'rates' ) 		 : '';

			$strDescription  = __( 'Occupant Type {%s, name} is associated to', [ 'name' => $this->getName() ] ) . ' ';
			$strDescription .= implode( ', ', $arrstrAssociations );
			$strDescription .= ' ' . __( 'and cannot be deleted.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strDescription ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( false == $boolIsValid ) {
					return $boolIsValid;
				}
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valCustomerRelationshipGroupId();
				$boolIsValid &= $this->valCustomerTypeId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valCustomerRelationshipGroupId();
				$boolIsValid &= $this->valCustomerTypeId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeleteCustomerRelationships( $objDatabase );
				break;

			case 'validate_delete_associated_customer_relationships':
				$boolIsValid &= $this->valDeleteCustomerRelationships( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getCustomerTypeName() {
		return CCustomerType::createService()->customerTypeIdToStr( $this->getCustomerTypeId() );
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['occupancy_type_id'] ) ) $this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
	}

}
?>