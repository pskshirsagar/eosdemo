<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomAdvertisements
 * Do not add any new functions to this class.
 */

class CCustomAdvertisements extends CBaseCustomAdvertisements {

	public static function fetchCustomAdvertisementByIdByCId( $intId, $intCid, $objDatabase ) {
		return self::fetchCustomAdvertisement( sprintf( 'SELECT * FROM custom_advertisements WHERE id = %d AND cid = %d', ( int ) $intId, ( int ) $intCid ), $objDatabase );

	}

	public static function fetchCustomAdvertisementsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomAdvertisement( sprintf( 'SELECT * FROM custom_advertisements WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );

	}

	public static function fetchPublishedCustomAdvertisementsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCustomAdvertisement( sprintf( 'SELECT * FROM custom_advertisements WHERE is_published = 1 AND property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid ), $objDatabase );

	}
}
?>