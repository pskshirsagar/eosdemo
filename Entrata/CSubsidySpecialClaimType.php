<?php

class CSubsidySpecialClaimType extends CBaseSubsidySpecialClaimType {

	const UNPAID_RENT_OR_DAMAGES	= 1;
	const RENT_UP_VACANCY			= 2;
	const REGULAR_VACANCY			= 3;
	const DEBT_SERVICE				= 4;

	public static function getSubsidySpecialClaimTypes() : array {
		return [
			self::UNPAID_RENT_OR_DAMAGES => __( 'Unpaid Rent/ Damages' ),
			self::RENT_UP_VACANCY        => __( 'Rent Up Vacancy' ),
			self::REGULAR_VACANCY        => __( 'Regular Vacancy' ),
			self::DEBT_SERVICE           => __( 'Debt Service' )
		];
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>