<?php

class CGlobalUserLease extends CBaseGlobalUserLease {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlobalUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStatusName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBuildingName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitIsSmart() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHaTermsAgreedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHaTermsAgreedVersion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLoginDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginDisabledOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>