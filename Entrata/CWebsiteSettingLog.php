<?php

class CWebsiteSettingLog extends CBaseWebsiteSettingLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteSettingKeyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUpdatedValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOptionLabel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>