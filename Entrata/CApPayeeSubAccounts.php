<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApPayeeSubAccounts
 * Do not add any new functions to this class.
 */

class CApPayeeSubAccounts extends CBaseApPayeeSubAccounts {

	public static function fetchApPayeeSubAccountsByApPayeeAccountIdsByCid( $arrintApPayeeAccountIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeAccountIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_sub_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_account_id IN ( ' . implode( ',', $arrintApPayeeAccountIds ) . ' ) ';

		return self::fetchApPayeeSubAccounts( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeAssociatedSubAccountsByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApPayeeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ap_payee_sub_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' ) ';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApPayeeSubAccountsByApPayeeIdByApPayeeLocationIdByApPayeeAccountIdByCid( $intApPayeeId, $intApPayeeLocationId, $intApPayeeAccountId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valId( $intApPayeeLocationId ) || false == valId( $intApPayeeAccountId ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						ap_payee_sub_accounts
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND ap_payee_account_id = ' . ( int ) $intApPayeeAccountId;

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>