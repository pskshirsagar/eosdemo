<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCustomerAppLoginTokens
 * Do not add any new functions to this class.
 */

class CCustomerAppLoginTokens extends CBaseCustomerAppLoginTokens {

	public static function fetchCustomerAppLoginTokenByCustomerIdByCidByToken( $intCustomerId, $intCid, $strRememberMetoken, $objDatabase ) {

		$strSql = ' SELECT
							*
					FROM
							customer_app_login_tokens
					WHERE
							customer_id =' . ( int ) $intCustomerId . '
							AND remember_me_token =\'' . $strRememberMetoken . '\'
							AND cid =' . ( int ) $intCid;

		return self::fetchCustomerAppLoginToken( $strSql, $objDatabase );
	}

}
?>