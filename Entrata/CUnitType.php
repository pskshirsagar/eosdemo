<?php
use Psi\Eos\Entrata\CPropertyFloorplans;
use Psi\Eos\Entrata\CPropertyUnits;
use Psi\Eos\Entrata\CUnitSpaces;

class CUnitType extends CBaseUnitType {

	const CATCH_ALL_UNIT_TYPE = 'Floorplan Catch All Unit Type';
	const IS_OCCUPIABLE_YES   = 1;

	private $m_intSpecialId;
	private $m_intUnitTypeRatesCount;
	private $m_intUnitTypeUnitsCount;
	private $m_intUnitTypeSpecialsCount;

	private $m_arrobjUnitSpaces;
	private $m_arrobjUnitTypeSpecials;
	private $m_arrobjPropertyFloorplans;
	private $m_intMilitaryOfficerStructureId;

	protected $m_arrobjLeaseTermIds;

	protected $m_strPropertyFloorplanName;

	/**
	* Get Functions
	*/

	public function getUnitTypeUnitsCount() {
		return $this->m_intUnitTypeUnitsCount;
	}

	public function getPropertyFloorPlanName() {
		return $this->m_strPropertyFloorplanName;
	}

	public function getUnitTypeRatesCount() {
		return $this->m_intUnitTypeRatesCount;
	}

	public function getUnitTypeSpecialsCount() {
		return $this->m_intUnitTypeSpecialsCount;
	}

	public function getSpecialId() {
		return $this->m_intSpecialId;
	}

	public function getUnitSpaces() {
		return $this->m_arrobjUnitSpaces;
	}

	public function getUnitTypeSpecials() {
		return $this->m_arrobjUnitTypeSpecials;
	}

	public function getPropertyFloorplans() {
		return $this->m_arrobjPropertyFloorplans;
	}

	public function getMilitaryOfficerStructureId() {
		return $this->m_intMilitaryOfficerStructureId;
	}

	public function getLeaseTermIds() {
		return $this->m_arrobjLeaseTermIds;
	}

	/**
	* Set Functions
	*/

	public function setUnitTypeUnitsCount( $intUnitTypeUnitsCount ) {
		$this->m_intUnitTypeUnitsCount = $intUnitTypeUnitsCount;
	}

	public function setUnitTypeRatesCount( $intUnitTypeRatesCount ) {
		$this->m_intUnitTypeRatesCount = $intUnitTypeRatesCount;
	}

	public function setPropertyFloorPlanName( $strPropertyFloorplanName ) {
		$this->m_strPropertyFloorplanName = $strPropertyFloorplanName;
	}

	public function setUnitTypeSpecialsCount( $intUnitTypeSpecialsCount ) {
		$this->m_intUnitTypeSpecialsCount = $intUnitTypeSpecialsCount;
	}

	public function setSpecialId( $intSpecialId ) {
		$this->m_intSpecialId = $intSpecialId;
	}

	public function setUnitSpaces( $arrobjUnitSpaces ) {
		$this->m_arrobjUnitSpaces = $arrobjUnitSpaces;
	}

	public function setUnitTypeSpecials( $arrobjUnitTypeSpecials ) {
		$this->m_arrobjUnitTypeSpecials = $arrobjUnitTypeSpecials;
	}

	public function setPropertyFloorplans( $arrobjPropertyFloorplans ) {
		$this->m_arrobjPropertyFloorplans = $arrobjPropertyFloorplans;
	}

	public function setMilitaryOfficerStructureId( $intMilitaryOfficerStructureId ) {
		$this->m_intMilitaryOfficerStructureId = $intMilitaryOfficerStructureId;
	}

	public function setLeaseTermIds( $arrobjLeaseTermIds ) {
		$this->m_arrobjLeaseTermIds = NULL;
		if( true == valArr( $arrobjLeaseTermIds ) ) {
			$this->m_arrobjLeaseTermIds = implode( ',', $arrobjLeaseTermIds );
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['unit_type_units_count'] ) ) {
			$this->setUnitTypeUnitsCount( $arrmixValues['unit_type_units_count'] );
		}
		if( true == isset( $arrmixValues['unit_type_rates_count'] ) ) {
			$this->setUnitTypeRatesCount( $arrmixValues['unit_type_rates_count'] );
		}
		if( true == isset( $arrmixValues['floorplan_name'] ) ) {
			$this->setPropertyFloorPlanName( $arrmixValues['floorplan_name'] );
		}
		if( true == isset( $arrmixValues['unit_type_specials_count'] ) ) {
			$this->setUnitTypeSpecialsCount( $arrmixValues['unit_type_specials_count'] );
		}
		if( true == isset( $arrmixValues['unit_spaces'] ) ) {
			$this->setUnitSpaces( $arrmixValues['unit_spaces'] );
		}
		if( true == isset( $arrmixValues['unit_type_specials'] ) ) {
			$this->setUnitTypeSpecials( $arrmixValues['unit_type_specials'] );
		}
		if( true == isset( $arrmixValues['special_id'] ) ) {
			$this->setSpecialId( $arrmixValues['special_id'] );
		}
	}

	/**
	* Fetch Functions
	*/

	public function fetchPropertyFloorplanById( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplanByUnitTypeIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyUnits( $objDatabase, $boolIncludeDeletedUnits = false ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsByUnitTypeIdByCid( $this->m_intId, $this->getCid(), $objDatabase, $boolIncludeDeletedUnits );
	}

	public function fetchPropertyUnitsCount( $objDatabase ) {
		return CPropertyUnits::createService()->fetchPropertyUnitsCountByUnitTypeIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValues( $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValuesByUnitTypeIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchIntegrationClientKeyValueByKey( $strKey, $objDatabase ) {
		return \Psi\Eos\Entrata\CIntegrationClientKeyValues::createService()->fetchIntegrationClientKeyValueByUnitTypeIdByKeyByCid( $this->m_intId, $strKey, $this->getCid(), $objDatabase );
	}

	public function fetchPropertyFloorplans( $objDatabase ) {
		return CPropertyFloorplans::createService()->fetchPropertyFloorplansByUnitTypeIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchSpecialRateAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CSpecialRateAssociations::createService()->fetchSpecialRateAssociationsByPropertyIdByArCascadeIdByArCascadeReferenceIdByCid( $this->getPropertyId(), CArCascade::UNIT_TYPE, $this->getId(), $this->getCid(), $objDatabase );
	}

	/**
	* Create Functions
	*/

	public function createRate() {
		$objRate = new CRate();

		$objRate->setCid( $this->m_intCid );
		$objRate->setPropertyId( $this->m_intPropertyId );
		$objRate->setUnitTypeId( $this->m_intId );
		$objRate->setArCascadeId( CArCascade::UNIT_TYPE );
		$objRate->setArCascadeReferenceId( $this->m_intId );

		return $objRate;
	}

	public function createRateAssociation() {
		$objRateAssociation = new CRateAssociation();
		$objRateAssociation->setCid( $this->m_intCid );
		$objRateAssociation->setPropertyId( $this->m_intPropertyId );
		$objRateAssociation->setUnitTypeId( $this->m_intId );
		$objRateAssociation->setArCascadeId( CArCascade::UNIT_TYPE );
		$objRateAssociation->setArCascadeReferenceId( $this->m_intId );

		return $objRateAssociation;
	}

	public function createPropertyUnit() {
		$objRate = new CRate();
		$objRate->setCid( $this->m_intCid );
		$objRate->setRateTypeId( CRateType::PROPERTY_UNIT );
		$objRate->setReferenceId( $this->getId() );

		return $objRate;
	}

	public function createIntegrationClientKeyValue() {
		$objIntegrationClientKeyValue = new CIntegrationClientKeyValue();
		$objIntegrationClientKeyValue->setCid( $this->m_intCid );
		$objIntegrationClientKeyValue->setPropertyId( $this->m_intPropertyId );
		$objIntegrationClientKeyValue->setUnitTypeId( $this->m_intId );
		$objIntegrationClientKeyValue->setKey( 'REMOTE_WAITING_LIST_UNIT_ID' );

		return $objIntegrationClientKeyValue;
	}

	public function createUnitTypeMilitaryOfficerStructure() {
		$objUnitTypeMilitaryOfficerStructure = new CUnitTypeMilitaryOfficerStructure();
		$objUnitTypeMilitaryOfficerStructure->setCid( $this->m_intCid );
		$objUnitTypeMilitaryOfficerStructure->setUnitTypeId( $this->m_intId );
		return $objUnitTypeMilitaryOfficerStructure;
	}

	/**
	* Validation Functions
	*/

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', ' Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strName ) || ( 1 > strlen( $this->m_strName ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'The Unit type name must have at least 1 character.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMinSquareFeet() {
		$boolIsValid = true;

		if( true == isset( $this->m_fltMinSquareFeet ) ) {
			if( false == is_numeric( $this->m_fltMinSquareFeet ) || 0 >= $this->m_fltMinSquareFeet ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_square_feet', __( 'Minimum square feet must be blank or greater than zero.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaxSquareFeet() {
		$boolIsValid = true;

		if( true == isset( $this->m_fltMaxSquareFeet ) ) {
			if( false == is_numeric( $this->m_fltMaxSquareFeet ) || 0 >= $this->m_fltMaxSquareFeet ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_square_feet', __( 'Maximum square feet must be blank or greater than zero.' ) ) );
			}

			if( true == $boolIsValid && true == isset( $this->m_fltMinSquareFeet ) && true == is_numeric( $this->m_fltMinSquareFeet ) && $this->m_fltMinSquareFeet > $this->m_fltMaxSquareFeet ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_square_feet', __( 'Maximum square feet must be greater than minimum square feet.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valNumberOfBedrooms() {
		$boolIsValid = true;

		if( true == isset( $this->m_intNumberOfBedrooms ) && ( false == is_numeric( $this->m_intNumberOfBedrooms ) || 0 > $this->m_intNumberOfBedrooms ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bedrooms', __( 'Number of bedrooms must be blank or greater than zero. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valNumberOfBathrooms() {
		$boolIsValid = true;

		if( false == is_null( $this->m_fltNumberOfBathrooms ) ) {

			if( true == is_numeric( $this->m_fltNumberOfBathrooms ) ) {

				if( 0 > $this->m_fltNumberOfBathrooms ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms must be blank or greater than zero' ) ) );

				} elseif( false == in_array( fmod( $this->m_fltNumberOfBathrooms, 1 ), [ 0, 0.25, 0.5, 0.75 ] ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
				}
			} else {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', __( 'Number of bathrooms should be increments of 0.25' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMaximumOccupants( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getMaximumOccupants() ) && ( false == is_numeric( $this->getMaximumOccupants() ) || 0 >= $this->getMaximumOccupants() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maximum_occupants', __( 'Maximum occupants must be blank or greater than zero.' ) ) );
		} else {
			$fltMaxPropertyUnitOccupants = CPropertyUnits::createService()->fetchMaxPropertyUnitOccupantsByUnitTypeIdByPropertyIdByCid( $this->m_intId, $this->getPropertyId(), $this->getCid(), $objDatabase );
			if( $fltMaxPropertyUnitOccupants > 0 && $this->getMaximumOccupants() < $fltMaxPropertyUnitOccupants ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'maximum_occupants', __( 'Unit Type Max Occupants Cannot be less than {%d,0} as set on the Property unit.', [ $fltMaxPropertyUnitOccupants ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valActiveUnitSpacesAssigned( $objDatabase ) {

		$boolIsValid = true;

		$strWhereCondition = 'WHERE cid = ' . ( int ) $this->getCid() . ' AND unit_type_id = ' . ( int ) $this->getId() . ' AND deleted_by IS NULL';
		$intActiveUnitSpaceCount = CUnitSpaces::createService()->fetchUnitSpaceCount( $strWhereCondition, $objDatabase );

		if( 0 < $intActiveUnitSpaceCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Active unit spaces associated to the unit type. Please disassociate those units before deleting the unit type.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valNumberOfBedrooms();
				$boolIsValid &= $this->valNumberOfBathrooms();
				$boolIsValid &= $this->valMinSquareFeet();
				$boolIsValid &= $this->valMaxSquareFeet();
				$boolIsValid &= $this->valMaximumOccupants( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valActiveUnitSpacesAssigned( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Other Functions
	*/

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true, $boolReturnSqlOnly = false ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( ' NOW() ' );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>
