<?php

class COwner extends CBaseOwner {

	use TPostalAddressHelper;

	const LOOKUP_TYPE_OWNERS_WITH_PROPERTIES = 'owners_with_properties';

	protected $m_boolIsOwnershipDistribution;

	protected $m_fltPercentageOwnership;

	protected $m_strPropertyIds;

	protected $m_arrstrExtraVars;

	public function __construct() {
		parent::__construct();

		$this->m_arrstrExtraVars = [];

		return;
	}

	/**
	 * Set Functions
	 */

	public function setIsOwnershipDistribution( $boolIsOwnershipDistribution ) {
		$this->m_boolIsOwnershipDistribution = CStrings::strToIntDef( $boolIsOwnershipDistribution, NULL, false );
	}

	public function setPercentageOwnership( $fltPercentageOwnership ) {
		$this->m_fltPercentageOwnership = CStrings::strToFloatDef( $fltPercentageOwnership, NULL, false, 6 );
	}

	public function setPropertyIds( $strPropertyIds ) {
		$this->m_strPropertyIds = $strPropertyIds;
	}

	public function setTaxNumber( $strPlainTaxNumber ) {
		if( true == valStr( $strPlainTaxNumber ) ) {
			$this->setTaxNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['percentage_ownership'] ) ) {
			$this->setPercentageOwnership( $arrmixValues['percentage_ownership'] );
		}
		if( true == isset( $arrmixValues['property_ids'] ) ) {
			$this->setPropertyIds( $arrmixValues['property_ids'] );
		}

		$this->m_arrstrExtraVars['primary_phone_number'] = ( isset( $arrmixValues['primary_phone_number'] ) ) ? trim( $arrmixValues['primary_phone_number'] ) : '';
		$this->m_arrstrExtraVars['name_first']           = ( isset( $arrmixValues['name_first'] ) ) ? trim( $arrmixValues['name_first'] ) : '';
		$this->m_arrstrExtraVars['name_last']            = ( isset( $arrmixValues['name_last'] ) ) ? trim( $arrmixValues['name_last'] ) : '';
		$this->m_arrstrExtraVars['ownership_count']      = ( isset( $arrmixValues['ownership_count'] ) ) ? trim( $arrmixValues['ownership_count'] ) : '';
		$this->m_arrstrExtraVars['vendor_code']          = ( isset( $arrmixValues['vendor_code'] ) ) ? trim( $arrmixValues['vendor_code'] ) : '';

		return;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getIsOwnershipDistribution() {
		return $this->m_boolIsOwnershipDistribution;
	}

	public function getPercentageOwnership() {
		return $this->m_fltPercentageOwnership;
	}

	public function getPropertyIds() {
		return $this->m_strPropertyIds;
	}

	public function getExtraVarByKey( $strKey ) {
		return ( true == array_key_exists( $strKey, $this->m_arrstrExtraVars ) ) ? $this->m_arrstrExtraVars[$strKey] : NULL;
	}

	/**
	 * Encryption, Decryption and Masking Functions
	 *
	 */

	public function getTaxNumber() {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}

		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function encryptTaxNumber() {
		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}
		$this->setTaxNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER ) );
	}

	public function getTaxNumberMasked() {

		$strLastFour     = \Psi\CStringService::singleton()->substr( $this->getTaxNumberDecrypted(), -4 );
		$intStringLength = strlen( $this->getTaxNumberDecrypted() );

		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function getTaxNumberDecrypted() {
		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return NULL;
		}

		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getTaxNumberEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	/**
	 * Validation Functions
	 */

	public function valOwnerTypeId() {

		$boolIsValid = true;

		if( false == valId( $this->getOwnerTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'owner_type_id', 'Owner Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOwnerName() {

		$boolIsValid = true;

		if( false == valStr( $this->getOwnerName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'owner_name', 'Owner Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {

		$boolIsValid = true;
		$intLength   = strlen( str_replace( '-', '', $this->getPostalCode() ) );

		if( CCountry::CODE_USA == $this->getCountryCode()
		    && false == is_null( $this->getPostalCode() )
		    && 0 < $intLength
		    && ( false == preg_match( '/^([[:alnum:]]){5,5}?$/', $this->getPostalCode() )
		         && false == preg_match( '/^([[:alnum:]]){5,5}-([[:alnum:]]){4,4}?$/', $this->getPostalCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 5 or 10 characters in XXXXX or XXXXX-XXXX format and should contain only alphanumeric characters.' ) );
		}

		if( CCountry::CODE_CANADA == $this->getCountryCode()
		    && false == is_null( $this->getPostalCode() )
		    && 0 < $intLength
		    && ( false == preg_match( '/^[a-zA-Z]{1}\d{1}[a-zA-Z]{1} \d{1}[a-zA-Z]{1}\d{1}$/', $this->getPostalCode() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code must be 6 characters in XXX XXX format and should contain only alphanumeric characters.' ) );
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted() {

		$boolIsValid = true;

		if( false == valStr( $this->getTaxNumberEncrypted() ) ) {
			return $boolIsValid;
		}

		if( false == preg_match( '/^([\d]{2}-[\d]{7})$|([\d]{3}-[\d]{2}-[\d]{4})$/', $this->getTaxNumberDecrypted() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'taxpayer_id_number_encrypted', 'Tax ID Number should be in XX-XXXXXXX or XXX-XX-XXXX and should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valOwnerDetailsFor1099EFile() {

		$boolIsValid = true;

		$arrstrRequiredFields[] = ( false == valStr( $this->getTaxNumberDecrypted() ) ) ? 'Tax ID Number' : '';
		$arrstrRequiredFields[] = ( false == valStr( $this->getOwnerName() ) ) ? 'Name' : '';
		$arrstrRequiredFields[] = ( false == valStr( $this->getStreetLine1() ) && false == valStr( $this->getStreetLine2() ) && false == valStr( $this->getStreetLine3() ) ) ? 'Address' : '';
		$arrstrRequiredFields[] = ( false == valStr( $this->getCity() ) ) ? 'City' : '';
		if( CCountry::CODE_USA == $this->getCountryCode() || CCountry::CODE_CANADA == $this->getCountryCode() ) {
			$arrstrRequiredFields[] = ( false == valStr( $this->getStateCode() ) ) ? 'State' : '';
		} else {
			$arrstrRequiredFields[] = ( false == valStr( $this->getProvince() ) ) ? 'State' : '';
		}
		$arrstrRequiredFields[] = ( false == valStr( $this->getPostalCode() ) ) ? 'Zip Code' : '';
		$arrstrRequiredFields   = array_filter( $arrstrRequiredFields );

		if( true == valArr( $arrstrRequiredFields ) ) {
			$boolIsValid       = false;

			$strValidationVerb = ( 1 == count( $arrstrRequiredFields ) ) ? 'is' : 'are all';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, implode( ', ', $arrstrRequiredFields ) . ' ' . $strValidationVerb . ' required for ' . $this->getOwnerName() . '. Please <a href=\'?module=ownersxxx\' target="_blank">click here</a> to review the owner\'s details. ' ) );
		}

		return $boolIsValid;
	}

	public function valPercentageOwnership() {

		$boolIsValid = true;

		if( 1 == $this->getIsOwnershipDistribution() && 100 != $this->getPercentageOwnership() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Ownership percentage total should be 100.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * @deprecated: This function doesn't take care of international phone number formats.
	 *            Keep only business validations (e.g.: Phone number is required for the product) in EOS and
	 *            remaining validations can be done using CPhoneNumber::isValid() in controller / library / outside-EOS.
	 *            If this function is not under i18n scope, please remove this deprecation DOC_BLOCK.
	 * @see       \i18n\CPhoneNumber::isValid() should be used.
	 */
	public function valPhoneNumberWithExtension() {

		$boolIsValid = true;

		if( true == is_null( $this->getPhoneNumber() ) ) {
			return $boolIsValid;
		}

		$arrstrPhoneNumberOrExtension = explode( ':', $this->getPhoneNumber() );
		$intPhoneNumber               = preg_replace( '/[^0-9]/', '', $arrstrPhoneNumberOrExtension[0] );

		if( false == valStr( $intPhoneNumber ) && true == valStr( $arrstrPhoneNumberOrExtension[1] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
		} elseif( true == valStr( $intPhoneNumber ) && 10 > strlen( $intPhoneNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be 10 digits.' ) );
		} elseif( false == preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $arrstrPhoneNumberOrExtension[0] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valFaxNumber() {

		$boolIsValid = true;

		if( true == is_null( $this->getFaxNumber() ) ) {
			return $boolIsValid;
		}

		$intLength = strlen( preg_replace( '/[^0-9]/', '', $this->getFaxNumber() ) );

		if( 0 < $intLength && 10 > $intLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Fax number must be 10 digits.' ) );
		} elseif( false == preg_match( '/^[0-9\-\(\)\+\.\s]+$/', $this->getFaxNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', 'Fax number is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valWebsiteUrl() {
		$boolIsValid = true;

		if( true == is_null( $this->getWebsiteUrl() ) ) {
			return $boolIsValid;
		}

		if( false == preg_match( '/^(http(s?):\/\/)?(www\.)+[a-zA-Z0-9\.\-\_]+(\.[a-zA-Z]{2,3})+(\/[a-zA-Z0-9\_\-\s\.\/\?\%\#\&\=]*)?$/', $this->getWebsiteUrl() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'website_url', 'Invalid website url format.' ) );
		}

		return $boolIsValid;
	}

	protected function valPostalAddress() {
		$boolIsValid = true;

		$objPostalAddressService = \Psi\Libraries\I18n\PostalAddress\CPostalAddressService::createService();
		if( false == $objPostalAddressService->isValid( $this->getPostalAddress(), false ) ) {
			$this->addPostalAddressErrorMsgs( $objPostalAddressService->getErrorMsgs(), $this );
			$boolIsValid = false;

		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valOwnerName();
				$boolIsValid &= $this->valOwnerTypeId();
				$boolIsValid &= $this->valTaxNumberEncrypted();
				$boolIsValid &= $this->valPhoneNumberWithExtension();
				$boolIsValid &= $this->valFaxNumber();
				$boolIsValid &= $this->valWebsiteUrl();
				$boolIsValid &= $this->valPostalAddress();
				$boolIsValid &= $this->valPercentageOwnership();
				break;

			case VALIDATE_DELETE:
				break;

			case 'generate_1099_eFile':
				$boolIsValid &= $this->valOwnerDetailsFor1099EFile();
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createOwnerAssociation() {

		$objOwnerAssociation = new COwnerAssociation();
		$objOwnerAssociation->setCid( $this->getCid() );
		$objOwnerAssociation->setParentOwnerId( $this->getId() );
		$objOwnerAssociation->setPercentageOwnership( NULL );

		return $objOwnerAssociation;
	}

	public function createApPayeeContact() {

		$objApPayeeContact = new CApPayeeContact();

		$objApPayeeContact->setCid( $this->getCid() );
		$objApPayeeContact->setNameFirst( $this->getOwnerName() );
		$objApPayeeContact->setStreetLine1( $this->getStreetLine1() );
		$objApPayeeContact->setStreetLine2( $this->getStreetLine2() );
		$objApPayeeContact->setCity( $this->getCity() );
		$objApPayeeContact->setStateCode( $this->getStateCode() );
		$objApPayeeContact->setPostalCode( $this->getPostalCode() );
		$objApPayeeContact->setPhoneNumber( $this->getPhoneNumber() );

		return $objApPayeeContact;
	}

	public function createApRemittance() {

		$objApRemittance = new CApRemittance();
		$objApRemittance->setCid( $this->getCid() );
		$objApRemittance->setApPayeeId( $this->getApPayeeId() );
		$objApRemittance->setApPaymentTypeId( CApPaymentType::ACH );

		return $objApRemittance;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();

		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setOwnerId( $this->getId() );

		return $objFileAssociation;
	}

	/**
	 * Fetch Functions.
	 */

	public function fetchOwnerAssociationsByParentOwnerId( $objClientDatabase ) {
		return \Psi\Eos\Entrata\COwnerAssociations::createService()->fetchOwnerAssociationsByParentOwnerIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchRecursiveOwnerIds( $objClientDatabase ) {
		return \Psi\Eos\Entrata\COwnerAssociations::createService()->fetchRecursiveOwnerIdsByOwnerIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public function createEvent( $intEventTypeId, $intEventSubTypeId = NULL, $strEventHandle = NULL, $strNotes = NULL, $objDatabase = NULL ) {
		if( false == valId( $intEventTypeId ) ) {
			return NULL;
		}

		$objEvent = new CEvent();
		$objEvent->setCid( $this->getCid() );
		$objEvent->setEventTypeId( $intEventTypeId );
		$objEvent->setEventSubTypeId( $intEventSubTypeId );
		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setEventHandle( $strEventHandle );
		$objEvent->setNotes( $strNotes );

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$arrintPropertyIds = ( array ) \Psi\Eos\Entrata\CProperties::createService()->fetchManagerialPropertyIdsByCid( $this->getCid(), $objDatabase );
			if( true == valArr( $arrintPropertyIds ) ) {
				$objEvent->setPropertyId( current( $arrintPropertyIds ) );
			}
		}

		$objEventReference = $objEvent->createEventReference();
		$objEventReference->setEventReferenceTypeId( CEventReferenceType::OWNER );
		$objEventReference->setReferenceId( $this->getId() );

		$objEvent->setEventReferences( [ $objEventReference ] );

		return $objEvent;
	}

	public function fetchEventsByEventTypeIds( $arrintEventTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CEvents::createService()->fetchEventsByReferenceIdByReferenceTypeIdByEventTypeIdsByCid( $this->getId(), CEventReferenceType::OWNER, $arrintEventTypeIds, $this->getCid(), $objDatabase, 'AND e.event_handle IS NOT NULL AND e.event_type_id NOT IN ( ' . CEventType::ADD_ON . ' )' );
	}

}

?>