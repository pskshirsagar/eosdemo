<?php

class COfferTemplate extends CBaseOfferTemplate {

	protected $m_arrobjUnitTypes;
	protected $m_arrobjLeaseStartWindows;
	protected $m_arrobjOffers;
	protected $m_arrobjUnitTypeDetails;
	protected $m_arrobjLeaseStartWindowDetails;
	Protected $m_arrobjPublishedUnitTypes;
	Protected $m_arrobjAssociatedUnitTypes;
	Protected $m_arrobjSpecialTypes;

	protected $m_objOffer;
	protected $m_objCompanyUser;

	protected $m_intArCascadeId;
	protected $m_intActiveRenewalOfferCount;
	protected $m_intOfferId;

	public function __construct( $arrmixOfferTemplate = NULL ) {
		parent::__construct();
		$this->setValues( $arrmixOfferTemplate );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['ar_cascade_id'] ) ) $this->setArCascadeId( $arrmixValues['ar_cascade_id'] );
		if( 'CONVENTIONAL TIER' == $arrmixValues['offer_template_type_id'] ) $this->setOfferTemplateTypeId( NULL );
		if( true == valArr( $arrmixValues ) && true == array_key_exists( 'prospects_qualify_at', $arrmixValues ) && true == valId( $arrmixValues['prospects_qualify_at'] ) ) $this->setApplicationStageStatusId( $arrmixValues['prospects_qualify_at'] );
		if( true == isset( $arrmixValues['id'] ) ) $this->setId( $arrmixValues['id'] );
		if( true == isset( $arrmixValues['offer_id'] ) ) $this->setOfferId( $arrmixValues['offer_id'] );
	}

	public function getUnitTypes() {
		return $this->m_arrobjUnitTypes;
	}

	public function getLeaseStartWindows() {
		return $this->m_arrobjLeaseStartWindows;
	}

	public function getOffers() {
		return ( array ) $this->m_arrobjOffers;
	}

	public function getUnitTypeDetails() {
		return $this->m_arrobjUnitTypeDetails;
	}

	public function getLeaseStartWindowDetails() {
		return $this->m_arrobjLeaseStartWindowDetails;
	}

	public function getArCascadeId() {
		return $this->m_intArCascadeId;
	}

	public function getPublishedUnitTypes() {
		return $this->m_arrobjPublishedUnitTypes;
	}

	public function getAssociatedUnitTypes() {
		return $this->m_arrobjAssociatedUnitTypes;
	}

	public function getActiveOfferCount() {
		return $this->m_intActiveRenewalOfferCount;
	}

	public function getSpecialTypes() {
		return $this->m_arrobjSpecialTypes;
	}

	public function getOfferId() {
		return $this->m_intOfferId;
	}

	public function setUnitTypes( $arrobjUnitTypes ) {
		$this->m_arrobjUnitTypes = $arrobjUnitTypes;
	}

	public function setLeaseStartWindows( $arrobjLeaseStartWindows ) {
		$this->m_arrobjLeaseStartWindows = $arrobjLeaseStartWindows;
	}

	public function setOffers( $arrobjOffers ) {
		$this->m_arrobjOffers = $arrobjOffers;
	}

	public function setUnitTypeDetails( $arrobjUnitTypeDetails ) {
		$this->m_arrobjUnitTypeDetails = $arrobjUnitTypeDetails;
	}

	public function setLeaseStartWindowDetails( $arrobjLeaseStartWindowDetails ) {
		$this->m_arrobjLeaseStartWindowDetails = $arrobjLeaseStartWindowDetails;
	}

	public function setPublishedUnitTypes( $arrobjPublishedUnitTypes ) {
		$this->m_arrobjPublishedUnitTypes = $arrobjPublishedUnitTypes;
	}

	public function setAssociatedUnitTypes( $arrobjAssociatedUnitTypes ) {
		$this->m_arrobjAssociatedUnitTypes = $arrobjAssociatedUnitTypes;
	}

	public function setActiveOfferCount( $intActiveRenewalOfferCount ) {
		$this->m_intActiveRenewalOfferCount = $intActiveRenewalOfferCount;
	}

	public function addOffer( $objOffer ) {
		$this->m_arrobjOffers[$objOffer->getId()] = $objOffer;
	}

	public function getCompanyUser() {
		return $this->m_objCompanyUser;
	}

	public function setCompanyUser( \CCompanyUser $objCompanyUser ) {
		$this->m_objCompanyUser = $objCompanyUser;
	}

	public function setArCascadeId( $intArCascadeId ) {
		$this->m_intArCascadeId = $intArCascadeId;
	}

	public function setSpecialTypes( $arrobjSpecialTypes ) {
		$this->m_arrobjSpecialTypes = $arrobjSpecialTypes;
	}

	public function setOfferId( $intOfferId ) {
		$this->m_intOfferId = $intOfferId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationStageStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOfferTemplateTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseIntervalTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		$strRegex = '/^[a-z A-Z0-9-_\\/\\\\.\'\"]+$/';

		if( '' == $this->getName() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required' ) );

		} elseif( false == preg_match( $strRegex, $this->getName() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required and should not contain special characters' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveOutEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpiryDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsStrictDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportOfferTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function addUnitTypes( $arrobjUnitType ) {
		$this->m_arrobjUnitTypes[] = $arrobjUnitType;
	}

	public function addLeaseStartWindows( $arrobjLeaseStartWindow ) {
		$this->m_arrobjLeaseStartWindows[] = $arrobjLeaseStartWindow;
	}

	public function addUnitTypeDetails( $arrobjUnitTypeDetail ) {
		$this->m_arrobjUnitTypeDetails[] = $arrobjUnitTypeDetail;
	}

	public function addLeaseStartWindowDetails( $arrobjLeaseStartWindowDetail ) {
		$this->m_arrobjLeaseStartWindowDetails[] = $arrobjLeaseStartWindowDetail;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolIsHardDelete = false, $boolReturnSqlOnly = false ) {
		if( true == $boolIsHardDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

}
?>
