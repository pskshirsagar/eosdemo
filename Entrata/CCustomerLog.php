<?php

class CCustomerLog extends CBaseCustomerLog {

	const IS_IGNORED_FALSE = 'false';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorCustomerLogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntegrationDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadSourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeasingAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessageOperatorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentAllowanceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaritalStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyIdentificationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryPhoneNumberTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSecondaryPhoneNumberTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSecondaryNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNamePrefix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameMiddle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameSuffix() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameMaiden() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameSpouse() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimaryIsVerified() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMobileNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWorkNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaxNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaxNumberMasked() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReturnedPaymentsCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBirthDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGender() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDlNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDlStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDlProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIdentificationValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIdentificationExpiration() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDontAllowLogin() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplyThroughPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPostDateIgnored() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsOpeningLog() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>