<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportEntityOptions
 * Do not add any new functions to this class.
 */

class CImportEntityOptions extends CBaseImportEntityOptions {

	public static function fetchImportEntityOptions( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CImportEntityOption', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchImportEntityOption( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CImportEntityOption', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>