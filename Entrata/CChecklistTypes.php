<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChecklistTypes
 * Do not add any new functions to this class.
 */

class CChecklistTypes extends CBaseChecklistTypes {

	public static function fetchChecklistTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CChecklistType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchChecklistType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CChecklistType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>