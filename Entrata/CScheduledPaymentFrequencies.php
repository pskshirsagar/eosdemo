<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScheduledPaymentFrequencies
 * Do not add any new functions to this class.
 */

class CScheduledPaymentFrequencies extends CBaseScheduledPaymentFrequencies {

	public static function fetchScheduledPaymentFrequency( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CScheduledPaymentFrequency', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>