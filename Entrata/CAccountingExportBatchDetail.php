<?php

class CAccountingExportBatchDetail extends CBaseAccountingExportBatchDetail {

	protected $m_intAccountNumber;
	protected $m_strAccountName;
	protected $m_strPropertyName;
	protected $m_strMriRef;
	protected $m_strMriEntityId;
	protected $m_strMriSource;
	protected $m_intLookupCode;
	protected $m_strSecondaryNumber;
	protected $m_intGlAccountId;
	protected $m_intJournalCode;
	protected $m_strAccountNumberDelimiter;
	protected $m_strDetailDescription;
	protected $m_strMriSiteId;
	protected $m_strIncludeOriginatingCurrency;
	protected $m_strJdeNumber;
	protected $m_strTransactionDatetimeDetail;
	protected $m_strGlAccountPrefix;
	protected $m_strGlHeaderReference;
	protected $m_intGlTransactionId;
	protected $m_strPropertyGlBu;
	protected $m_intRecordSequence;

	const GL_EXPORT_BOOK_TYPE_ACCRUAL = 1;
	const GL_EXPORT_BOOK_TYPE_CASH    = 2;
	const GL_EXPORT_BOOK_TYPE_BOTH    = 3;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountingExportBatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccrualGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCashGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getLookupCode() {
		return $this->m_intLookupCode;
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function getJournalCode() {
		return $this->m_intJournalCode;
	}

	public function getMriRef() {
		return $this->m_strMriRef;
	}

	public function getMriEntityId() {
		return $this->m_strMriEntityId;
	}

	public function getMriSource() {
		return $this->m_strMriSource;
	}

	public function getLocation() {
		return $this->m_strLocation;
	}

	public function getAccountNumberDelimiter() {
		return $this->m_strAccountNumberDelimiter;
	}

	public function getDetailDescription() {
		return $this->m_strDetailDescription;
	}

	public function getMriSiteId() {
		return $this->m_strMriSiteId;
	}

	public function getIncludeOriginatingCurrency() {
		return $this->m_strIncludeOriginatingCurrency;
	}

	public function getJdeNumber() {
		return $this->m_strJdeNumber;
	}

	public function getTransactionDatetimeDetail() {
		return $this->m_strTransactionDatetimeDetail;
	}

	public function getGlAccountPrefix() {
		return $this->m_strGlAccountPrefix;
	}

	public function getGlHeaderReference() {
		return $this->m_strGlHeaderReference;
	}

	public function getGlTransactionId() {
		return $this->m_intGlTransactionId;
	}

	public function getPropertyGlBu() {
		return $this->m_strPropertyGlBu;
	}

	public function getRecordSequence() {
		return $this->m_intRecordSequence;
	}

	public function getFrequencyId() {
		return $this->m_strFrequencyId;
	}

	/**
	 * Set Functions
	 */

	public function setAccountNumber( $intAccountNumber ) {
		$this->m_strAccountNumber = $intAccountNumber;
	}

	public function setAccountName( $strAccountName ) {
		return $this->m_strAccountName = $strAccountName;
	}

	public function setPropertyName( $strPropertyName ) {
		return $this->m_strPropertyName = $strPropertyName;
	}

	public function setLookupCode( $intLookupCode ) {
		return $this->m_intLookupCode = $intLookupCode;
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		return $this->m_strSecondaryNumber = $strSecondaryNumber;
	}

	public function setGlAccountId( $intGlAccountId ) {
		return $this->m_intGlAccountId = $intGlAccountId;
	}

	public function setJournalCode( $intJournalCode ) {
		return $this->m_intJournalCode = $intJournalCode;
	}

	public function setMriRef( $strMriRef ) {
		return $this->m_strMriRef = $strMriRef;
	}

	public function setMriEntityId( $strMriEntityId ) {
		return $this->m_strMriEntityId = $strMriEntityId;
	}

	public function setMriSource( $strMriSource ) {
		return $this->m_strMriSource = $strMriSource;
	}

	public function setLocation( $strLocation ) {
		return $this->m_strLocation = $strLocation;
	}

	public function setAccountNumberDelimiter( $strAccountNumberDelimiter ) {
		return $this->m_strAccountNumberDelimiter = $strAccountNumberDelimiter;
	}

	public function setDetailDescription( $strDetailDescription ) {
		return $this->m_strDetailDescription = $strDetailDescription;
	}

	public function setMriSiteId( $strMriSiteId ) {
		return $this->m_strMriSiteId = $strMriSiteId;
	}

	public function setIncludeOriginatingCurrency( $strIncludeOriginatingCurrency ) {
		return $this->m_strIncludeOriginatingCurrency = $strIncludeOriginatingCurrency;
	}

	public function setJdeNumber( $strJdeNumber ) {
		return $this->m_strJdeNumber = $strJdeNumber;
	}

	public function setTransactionDatetimeDetail( $strTransactionDatetimeDetail ) {
		return $this->m_strTransactionDatetimeDetail = $strTransactionDatetimeDetail;
	}

	public function setGlAccountPrefix( $strGlAccountPrefix ) {
		$this->m_strGlAccountPrefix = $strGlAccountPrefix;
	}

	public function setGlHeaderReference( $strGlHeaderReference ) {
		$this->m_strGlHeaderReference = $strGlHeaderReference;
	}

	public function setGlTransactionId( $intGlTransactionId ) {
		$this->m_intGlTransactionId = $intGlTransactionId;
	}

	public function setPropertyGlBu( $strPropertyGlBu ) {
		$this->m_strPropertyGlBu = $strPropertyGlBu;
	}

	public function setRecordSequence( $intRecordSequence ) {
		$this->m_intRecordSequence = $intRecordSequence;
	}

	public function setFrequencyId( $strFrequencyId ) {
		$this->m_strFrequencyId = $strFrequencyId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['account_number'] ) ) {
			$this->setAccountNumber( $arrmixValues['account_number'] );
		}
		if( true == isset( $arrmixValues['account_name'] ) ) {
			$this->setAccountName( $arrmixValues['account_name'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['lookup_code'] ) ) {
			$this->setLookupCode( $arrmixValues['lookup_code'] );
		}
		if( true == isset( $arrmixValues['secondary_number'] ) ) {
			$this->setSecondaryNumber( $arrmixValues['secondary_number'] );
		}
		if( true == isset( $arrmixValues['gl_account_id'] ) ) {
			$this->setGlAccountId( $arrmixValues['gl_account_id'] );
		}
		if( true == isset( $arrmixValues['value'] ) ) {
			$this->setJournalCode( $arrmixValues['value'] );
		}
		if( true == isset( $arrmixValues['ref'] ) ) {
			$this->setMriRef( $arrmixValues['ref'] );
		}
		if( true == isset( $arrmixValues['entity_id'] ) ) {
			$this->setMriEntityId( $arrmixValues['entity_id'] );
		}
		if( true == isset( $arrmixValues['mri_source'] ) ) {
			$this->setMriSource( $arrmixValues['mri_source'] );
		}
		if( true == isset( $arrmixValues['location_id'] ) ) {
			$this->setLocation( $arrmixValues['location_id'] );
		}
		if( true == isset( $arrmixValues['account_number_delimiter'] ) ) {
			$this->setAccountNumberDelimiter( $arrmixValues['account_number_delimiter'] );
		}
		if( true == isset( $arrmixValues['detail_description'] ) ) {
			$this->setDetailDescription( $arrmixValues['detail_description'] );
		}
		if( true == isset( $arrmixValues['site_id'] ) ) {
			$this->setMriSiteId( $arrmixValues['site_id'] );
		}
		if( true == isset( $arrmixValues['include_originating_currency'] ) ) {
			$this->setIncludeOriginatingCurrency( $arrmixValues['include_originating_currency'] );
		}
		if( true == isset( $arrmixValues['jde_number'] ) ) {
			$this->setJdeNumber( $arrmixValues['jde_number'] );
		}
		if( true == isset( $arrmixValues['datetime'] ) ) {
			$this->setTransactionDatetimeDetail( $arrmixValues['datetime'] );
		}
		if( true == isset( $arrmixValues['gl_account_prefix'] ) ) {
			$this->setGlAccountPrefix( $arrmixValues['gl_account_prefix'] );
		}
		if( true == isset( $arrmixValues['reference'] ) ) {
			$this->setGlHeaderReference( $arrmixValues['reference'] );
		}
		if( true == isset( $arrmixValues['transaction_id'] ) ) {
			$this->setGlTransactionId( $arrmixValues['transaction_id'] );
		}
		if( true == isset( $arrmixValues['property_gl_bu'] ) ) {
			$this->setPropertyGlBu( $arrmixValues['property_gl_bu'] );
		}
		if( true == isset( $arrmixValues['frequency_id'] ) ) {
			$this->setFrequencyId( $arrmixValues['frequency_id'] );
		}
		if( true == isset( $arrmixValues['record_sequence'] ) ) {
			$this->setRecordSequence( $arrmixValues['record_sequence'] );
		}
		return;
	}

}
?>