<?php

class CPropertyBankAccount extends CBasePropertyBankAccount {

	protected $m_intIsDisabled;			// Is bank account disabled?
	protected $m_intGlAccountId;
	protected $m_intBankGlAccountId;	// Cash Gl Account for bank account.
	protected $m_intBankAccountTypeId;
	protected $m_intIsCreditCardAccount;
	protected $m_intInterCoPropertyId;

	protected $m_strPropertyName;
	protected $m_strAccountName;
	protected $m_strAccountNumber;
	protected $m_strBankAccountName;

	protected $m_boolIsUseSubledgers;

	protected $m_arrintBankAccountIds;

	protected $m_arrmixGlAccountProperties;

	/**
	 * Get Functions
	 */

	public function getBankGlAccountId() {
		return $this->m_intBankGlAccountId;
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function getGlAccountProperties() {
		return $this->m_arrmixGlAccountProperties;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	// Get gl account name and gl account number.

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	// Get bank account name.

	public function getBankAccountName() {
		return $this->m_strBankAccountName;
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function getBankAccountTypeId() {
		return $this->m_intBankAccountTypeId;
	}

	public function getBankAccountIds() {
		return $this->m_arrintBankAccountIds;
	}

	public function getIscreditCardAccount() {
		return $this->m_intIsCreditCardAccount;
	}

	public function getIsUseSubledgers() {
		return $this->m_boolIsUseSubledgers;
	}

	public function getInterCoPropertyId() {
		return $this->m_intInterCoPropertyId;
	}

	/**
	 * Set Functions
	 */

	public function setBankGlAccountId( $intBankGlAccountId ) {
		$this->m_intBankGlAccountId = CStrings::strToIntDef( $intBankGlAccountId, NULL, false );
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->m_intGlAccountId = CStrings::strToIntDef( $intGlAccountId, NULL, false );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setGlAccountProperties( $arrmixGlAccountProperties ) {
		$this->m_arrmixGlAccountProperties = $arrmixGlAccountProperties;
	}

	// Set gl account name and gl account number.

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->m_strAccountNumber = $strAccountNumber;
	}

	// Set bank account name.

	public function setBankAccountName( $strBankAccountName ) {
		$this->m_strBankAccountName = $strBankAccountName;
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->m_intIsDisabled = CStrings::strToIntDef( $intIsDisabled, NULL, false );
	}

	public function setBankAccountTypeId( $intBankAccountTypeId ) {
		$this->m_intBankAccountTypeId = CStrings::strToIntDef( $intBankAccountTypeId, NULL, false );
	}

	public function setBankAccountIds( $arrintBankAccountIds ) {
		$this->m_arrintBankAccountIds = $arrintBankAccountIds;
	}

	public function setIscreditCardAccount( $intIscreditCardAccount ) {
		$this->m_intIsCreditCardAccount = CStrings::strToIntDef( $intIscreditCardAccount, NULL, false );
	}

	public function setIsUseSubledgers( $boolIsUseSubledgers ) {
		$this->m_boolIsUseSubledgers = CStrings::strToBool( $boolIsUseSubledgers );
	}

	public function setInterCoPropertyId( $intInterCoPropertyId ) {
		$this->m_intInterCoPropertyId = CStrings::strToIntDef( $intInterCoPropertyId, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['bank_account_name'] ) ) $this->setBankAccountName( $arrmixValues['bank_account_name'] );
		if( true == isset( $arrmixValues['bank_gl_account_id'] ) ) $this->setBankGlAccountId( $arrmixValues['bank_gl_account_id'] );
		if( true == isset( $arrmixValues['gl_account_id'] ) ) $this->setGlAccountId( $arrmixValues['gl_account_id'] );
		if( true == isset( $arrmixValues['is_disabled'] ) ) $this->setIsDisabled( $arrmixValues['is_disabled'] );
		if( true == isset( $arrmixValues['bank_account_ids'] ) ) $this->setBankAccountIds( $arrmixValues['bank_account_ids'] );
        if( true == isset( $arrmixValues['bank_account_type_id'] ) ) $this->setBankAccountTypeId( $arrmixValues['bank_account_type_id'] );
		if( true == isset( $arrmixValues['is_credit_card_account'] ) ) $this->setIscreditCardAccount( $arrmixValues['is_credit_card_account'] );
		if( true == isset( $arrmixValues['is_use_subledgers'] ) ) $this->setIsUseSubledgers( $arrmixValues['is_use_subledgers'] );
		if( true == isset( $arrmixValues['inter_co_property_id'] ) ) $this->setInterCoPropertyId( $arrmixValues['inter_co_property_id'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( ' Associated Properties are required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBankApCodeId() {
		$boolIsValid = true;

		if( CBankAccountType::INTER_COMPANY == $this->getBankAccountTypeId() ) {
			return $boolIsValid;
		}

		if( true == is_null( $this->getBankApCodeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'Gl account is required.' ) ) );
		}

		$arrmixGlAccountProperties = ( array ) $this->getGlAccountProperties();

		if( true == valArr( $arrmixGlAccountProperties ) && false == array_key_exists( $this->getGlAccountId(), $arrmixGlAccountProperties ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gl_account_id', __( 'GL account "{%s, 0} : {%s, 1}" is not associated with Property {%s, 2}', [ $this->getAccountNumber(), $this->getAccountName(), $this->getPropertyName() ] ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valBankAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDueToFromGlAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valBankApCodeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>