<?php

class CRevenueSupplyDemand extends CBaseRevenueSupplyDemand {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEffectiveDate() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSupplyCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDemandCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>