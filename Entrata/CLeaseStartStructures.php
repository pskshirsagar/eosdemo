<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CLeaseStartStructures
 * Do not add any new functions to this class.
 */

class CLeaseStartStructures extends CBaseLeaseStartStructures {

	public static function fetchConflictingLeaseStartStructureCountByCid( $objLeaseStartStructure, $intCid, $objClientDatabase ) {

		$strWhereSql = ' WHERE name = \'' . addslashes( $objLeaseStartStructure->getName() ) . '\'
				   		AND deleted_by IS NULL
				   		AND deleted_on IS NULL
						AND id <> ' . ( int ) $objLeaseStartStructure->getId() . '
				   		AND cid = ' . ( int ) $intCid . '
				   LIMIT 1';

		return self::fetchLeaseStartStructureCount( $strWhereSql, $objClientDatabase );
	}

	public static function fetchActiveDefaultLeaseStartWindowsByCid( $intCid, $objDatabase ) {

		$strOrderBySql  = ' ORDER BY lss.id ASC';

		$strSql = 'SELECT
						lss.id,
						lss.cid,
						lss.default_lease_start_structure_id,
						util_get_translated( \'name\', lss.name, lss.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) as name,
						util_get_translated( \'description\', lss.description, lss.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) as description,
						lss.is_default,
						lss.is_published,
						lss.is_system,
						lss.order_num
					FROM
						lease_start_structures AS lss
					WHERE
						lss.is_default = false
						AND lss.is_system = false
						AND lss.is_published = true
						AND lss.default_lease_start_structure_id IS NOT NULL
						AND lss.cid = ' . ( int ) $intCid .
						$strOrderBySql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedActiveLeaseStartWindowsByCid( $intCid, $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strOrderBySql  = ' ORDER BY lss.id ASC';

		$strSql = 'SELECT
						DISTINCT( lss.id ),
						lsw.is_active,
						util_get_translated( \'name\', lss.name, lss.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS lease_start_structure_name,
						util_get_translated( \'name\', lss.name, lss.details, \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' ) AS default_locale_lease_start_structure_name,
						lss.is_default,
						lss.default_lease_start_structure_id,
						COUNT(pcs.property_id) over (PARTITION BY lss.id, lsw.id ) as total_properties,
						lss.details
					FROM
						lease_start_structures lss
						LEFT JOIN lease_start_windows lsw ON ( lsw.cid = lss.cid AND lsw.lease_start_structure_id = lss.id AND lsw.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.lease_term_type_id IS NULL )
						LEFT JOIN property_charge_settings pcs ON ( pcs.cid = lss.cid AND pcs.lease_start_structure_id = lss.id )
					WHERE
						lss.cid = ' . ( int ) $intCid . '
						AND lss.is_published = true
						AND lss.is_default = false
						AND is_system = false
						AND lss.deleted_by IS NULL
						GROUP BY lss.id, lss.name, lsw.is_active, pcs.property_id, lss.id, lsw.id, lss.is_default, lss.default_lease_start_structure_id, lss.details' .
							$strOrderBySql .
							' OFFSET ' . ( int ) $intOffset .
							' LIMIT ' . ( int ) $intLimit;

		return self::fetchLeaseStartStructures( $strSql, $objDatabase );
	}

	public static function fetchActiveLeaseStartWindowsCountByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						COUNT( DISTINCT( lss.id ) ) AS lease_start_structure_count
    				FROM
    					lease_start_structures lss
					    LEFT JOIN lease_start_windows lsw ON ( lsw.cid = lss.cid AND lsw.lease_start_structure_id = lss.id AND lsw.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.lease_term_type_id IS NULL )
					    LEFT JOIN property_charge_settings pcs ON ( pcs.cid = lss.cid AND pcs.lease_start_structure_id = lss.id )
    				WHERE
    					lss.cid = ' . ( int ) $intCid . '
    					AND lss.is_published = true
    					AND lss.is_default = false
    					AND is_system = false
    					AND lss.deleted_by IS NULL';

		return self::fetchLeaseStartStructure( $strSql, $objDatabase );
	}

	public static function fetchLeaseStartStructureByNameByCid( $strName, $intCid, $objDatabase, $boolPublishedOnly = true ) {

		$strSqlPublish = NULL;

		if( true === $boolPublishedOnly ) {
			$strSqlPublish = ' AND lss.is_published = TRUE ';
		}

		$strSql = 'SELECT
						*
					FROM
						lease_start_structures lss
					WHERE
						lss.cid = ' . ( int ) $intCid . '
						AND lss.name = \'' . $strName . '\'
						AND lss.deleted_on IS NULL
						' . $strSqlPublish . '
					LIMIT 1';

		return self::fetchLeaseStartStructure( $strSql, $objDatabase );
	}

}
?>
