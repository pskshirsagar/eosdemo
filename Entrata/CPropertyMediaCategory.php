<?php

class CPropertyMediaCategory extends CBasePropertyMediaCategory {

	public function valName() {
		$boolIsValid = true;

		if( ( true == is_null( $this->getName() ) ) || ( 0 == strlen( trim( $this->getName() ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Category name must be at least 1 character.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) || 0 == strlen( trim( $this->getDescription() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Please enter description.' ) );
		}

		return $boolIsValid;
	}

	public function valMediaWidth() {
		$boolIsValid = true;

		if( true == is_null( $this->getMediaWidth() ) || ( false == is_numeric( $this->getMediaWidth() ) || 0 >= $this->getMediaWidth() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_width', 'Width has to be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valMediaHeight() {
		$boolIsValid = true;

		if( true == is_null( $this->getMediaHeight() ) || ( false == is_numeric( $this->getMediaHeight() ) || 0 >= $this->getMediaHeight() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_height', 'Height has to be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valMediaWidth();
				$boolIsValid &= $this->valMediaHeight();
				break;

			case VALIDATE_DELETE:
				break;

			case 'without_description':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valMediaWidth();
				$boolIsValid &= $this->valMediaHeight();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>