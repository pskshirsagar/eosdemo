<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CChoreCategories
 * Do not add any new functions to this class.
 */

class CChoreCategories extends CBaseChoreCategories {

	public static function fetchChoreCategories( $strSql, $objClientDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CChoreCategory', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchChoreCategory( $strSql, $objClientDatabase ) {
		return self::fetchCachedObject( $strSql, 'CChoreCategory', $objClientDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchChoreCategoriesByIds( $arrintChoreCategoryIds, $objClientDatabase ) {
		if( false == valArr( $arrintChoreCategoryIds ) ) return NULL;
		return self::fetchChoreCategories( 'SELECT * FROM chore_categories WHERE id IN ( ' . implode( ',', $arrintChoreCategoryIds ) . ' ) ORDER BY id', $objClientDatabase );
	}
}
?>