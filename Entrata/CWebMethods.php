<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CWebMethods
 * Do not add any new functions to this class.
 */

class CWebMethods extends CBaseWebMethods {

	public static function fetchWebMethods( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CWebMethod', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchWebMethod( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CWebMethod', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllWebMethods( $objDatabase ) {
		$strSql = 'SELECT * FROM web_methods ORDER BY name';
		return self::fetchWebMethods( $strSql, $objDatabase );
	}
}
?>