<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSpecials
 * Do not add any new functions to this class.
 */

class CSpecials extends CBaseSpecials {

	public static function fetchIntegratedSpecialsByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT ON( s.cid, s.id, ra.property_id )
						s.*,
						ra.property_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
					WHERE
						s.cid =' . ( int ) $intCid . '
						AND s.remote_primary_key IS NOT NULL';

		return parent::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchGiftIncentivesByCidByPropertyIdByPropertyFloorPlanIdByUnitTypeIdByUnitSpaceId( $intCid, $intPropertyId, $intPropertyFloorPlanId = NULL, $intUnitTypeId = NULL, $intUnitSpaceId = NULL, $strLeaseStartDate, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						ra.hide_description
					FROM
						specials s
						JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id =' . CArOrigin::SPECIAL . '
						AND ( ( ra.ar_cascade_id =' . CArCascade::PROPERTY . ' AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . ' )
							  OR ( ra.ar_cascade_id =' . CArCascade::UNIT_TYPE . ' AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitTypeId . ' )
							  OR ( ra.ar_cascade_id =' . CArCascade::SPACE . ' AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . ' )
							  OR ( ra.ar_cascade_id =' . CArCascade::FLOOR_PLAN . ' AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyFloorPlanId . ' )
							)
						AND s.deleted_on IS NULL
						AND s.special_type_id =' . CSpecialType:: GIFT_INCENTIVE . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND ( s.quantity_remaining IS NULL OR s.quantity_remaining > 0 )
						AND COALESCE( s.start_date, \'1970-01-01\'::DATE ) <= \'' . $strLeaseStartDate . '\'
						AND COALESCE( s.end_date, \'2099-12-31\'::DATE ) >= \'' . $strLeaseStartDate . '\'
						AND s.is_active::BOOLEAN = true
						AND s.show_on_website::BOOLEAN = true

					ORDER BY s.application_stage_status_id ASC, s.end_date ASC, s.quantity_remaining ASC, s.gift_value DESC';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialsBySpecialGroupIdsByCid( $arrintSpecialGroupIds,$intCid, $objDatabase, $boolIsFetchObject = false ) {
		if( false == valArr( $arrintSpecialGroupIds ) ) return NULL;

		$strSql = 'SELECT
					*,
					(
					SELECT
						array_to_string ( array_agg ( INITCAP(si.name ||\'(\'|| st.name || \')\' ) ORDER BY ' . $objDatabase->getCollateSort( 'INITCAP(si.name)' ) . '), \',\' )
					FROM
						specials si
						JOIN special_tier_specials sts ON ( sts.cid = si.cid AND sts.tier_special_id = s.id AND si.id = sts.special_id AND si.cid = ' . ( int ) $intCid . ' AND sts.deleted_by IS NULL )
						JOIN special_types st ON ( st.id = si.special_type_id )
						WHERE si.cid = s.cid
					) AS special_names
					FROM specials s
					WHERE
					cid =' . ( int ) $intCid . '
					AND s.deleted_by IS NULL
					AND special_group_id IN( ' . implode( ',', $arrintSpecialGroupIds ) . ' )
					ORDER BY s.id ASC,special_group_id,max_days_to_lease_start DESC, start_date ASC; ';

		if( true == $boolIsFetchObject ) {
			return self::fetchSpecials( $strSql, $objDatabase );
		} else {
			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchIntegratedSpecialsByIntegrationDatabaseIdByCid( $intIntegrationDatabaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						ra.hide_description
					FROM
						specials s
						LEFT OUTER JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
						property_integration_databases pid
					WHERE
						( ra.id IS NULL OR ( ra.property_id = pid.property_id AND ra.cid = pid.cid AND pid.integration_database_id = ' . ( int ) $intIntegrationDatabaseId . ' AND pid.cid = ' . ( int ) $intCid . ' ) )
						AND cp.cid =' . ( int ) $intCid . '
						AND cp.remote_primary_key IS NOT NULL';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchPublishedSpecialsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeUnitSpaceSpecial = false, $strMoveInDate = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strIncludeUnitSpaceSpecial = '';

		if( true == $boolIncludeUnitSpaceSpecial ) {
			$strIncludeUnitSpaceSpecial = ' OR ra.ar_cascade_id = ' . CArCascade::SPACE;
		}

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		$strSql = 'SELECT
					s.*,
					ra.hide_description,
					ra.property_id
				FROM
					specials s
					JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ( ra.ar_cascade_id = ' . CArCascade::PROPERTY . $strIncludeUnitSpaceSpecial . ' ) )
		WHERE
					s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
					AND ra.cid = ' . ( int ) $intCid . '
					AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND s.is_active::BOOLEAN = true
					AND s.show_on_website::BOOLEAN = true
					AND s.deleted_on IS NULL
					AND ( ( ' . $strMoveInDate . ' ::Date >= s.start_date AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( ' . $strMoveInDate . ' ::Date >= s.start_date AND s.end_date IS NULL ) OR
									( s.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( s.start_date IS NULL AND s.end_date IS NULL )
									)
					AND s.special_type_id <> ' . CSpecialType::FIXED_BAH_ADJUSTMENT . '
					AND s.special_type_id <> ' . CSpecialType::INSTALLMENT_PLAN;

		$strSql .= ' ORDER BY
					ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialByIdByPropertyIdByCid( $intSpecialId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						ra.hide_description
					FROM
						specials AS s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
					WHERE
						s.cid = ' . ( int ) $intCid . '
						AND s.deleted_on IS NULL
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
						AND s.id = ' . ( int ) $intSpecialId;

		return self::fetchSpecial( $strSql, $objDatabase );
	}

	public static function fetchCustomSpecialByIdCid( $intSpecialId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						o.max_offer_count,
						ra.hide_description
					FROM
						specials AS s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN offer_items oi ON( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id )
						LEFT JOIN offers o ON( oi.cid = o.cid AND o.id = oi.offer_id )
					WHERE
						s.cid = ' . ( int ) $intCid . '
						AND s.deleted_on IS NULL
						AND s.id = ' . ( int ) $intSpecialId;

		return self::fetchSpecial( $strSql, $objDatabase );
	}

	public static function fetchAssociatedSpecialsByLeaseIntervalIdByIdsByCid( $intSpecialIds, $intLeaseIntervalId, $intCid, $objDatabase ) {
		if( false == valArr( $intSpecialIds ) || false == valId( $intLeaseIntervalId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						DISTINCT ( oi.ar_origin_reference_id ) as id,
						o.days_offset AS max_days_to_lease_start,
						o.start_date,
						o.end_date,
						la.description as description,
						la.hide_description,
						s.name,
						s.special_type_id,
						s.gift_value
					FROM
						offer_items oi
						JOIN offers o ON (oi.cid = o.cid AND oi.offer_id = o.id AND o.deleted_by IS NULL)
						JOIN specials s ON (oi.cid = s.cid AND oi.ar_origin_reference_id = s.id)
						JOIN lease_associations la ON (la.cid = s.cid AND la.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND la.ar_origin_reference_id = s.id AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' )
					WHERE
						oi.cid = ' . ( int ) $intCid . ' AND
						oi.deleted_on IS NULL AND
						oi.ar_origin_reference_id in ( ' . sqlIntImplode( $intSpecialIds ) . ' )';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchCustomSpecialByIdsCid( $intSpecialIds, $intCid, $objDatabase ) {
		if( false == valArr( $intSpecialIds ) ) return NULL;

		$strSql = 'SELECT 
					s.id,
					o.cid,
					o.offer_template_id AS special_group_id,
					COALESCE(lag(o.days_offset, 1) OVER(
					ORDER BY o.days_offset), (
									SELECT o1.days_offset
									FROM offers o1
									WHERE o1.cid = o.cid AND
										o1.offer_template_id = o.offer_template_id AND
										o1.deleted_by IS NULL AND
										o1.days_offset < o.days_offset
									ORDER BY o1.days_offset DESC
									LIMIT 1
					) + 1, 0) AS min_days_to_lease_start,
					o.days_offset AS max_days_to_lease_start,
					o.max_offer_count AS quantity_remaining,
					o.is_active,
					o.start_date,
					o.end_date,
					s.special_type_id,
					s.name
				 FROM
					specials s
					LEFT JOIN offer_items oi ON( s.cid = oi.cid AND s.id = oi.ar_origin_reference_id AND oi.deleted_by IS NULL )
					LEFT JOIN offers o ON( o.cid =  oi.cid AND o.id = oi.offer_id AND o.deleted_by IS NULL )
					JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
					WHERE
					s.cid = ' . ( int ) $intCid . '
					AND s.deleted_by IS NULL
					AND s.id IN ( ' . sqlIntImplode( $intSpecialIds ) . ' )';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialsByPropertyIdByIdsByCid( $intPropertyId, $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						r.rate_interval_occurances AS rate_interval_occurances,
						r.ar_trigger_id AS ar_trigger_id,
						r.rate_amount
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						JOIN rates r ON ( r.cid = ra.cid AND r.ar_origin_reference_id = ra.ar_origin_reference_id AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id )
					WHERE
						ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_cascade_reference_id  = ' . ( int ) $intPropertyId . '
						AND s.id IN ( ' . implode( ',', $arrintIds ) . ' )
						AND s.deleted_by IS NULL';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchProspectSpecialsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						pr.ar_trigger_id AS ar_trigger_id,
						pr.rate_amount,
						pr.rate_increase_increment AS percentage
					FROM
						specials s
						JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN prospect_rates pr ON( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND s.deleted_on IS NULL
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND ra.property_id  = ' . ( int ) $intPropertyId . '
						AND ( s.end_date IS NULL or s.end_date > NOW() )';

		return self::fetchSpecials( $strSql, $objDatabase );

	}

	public static function fetchRenewalSpecialsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						r.ar_trigger_id AS ar_trigger_id
					FROM
						specials s
						LEFT JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN rates r ON( r.cid = ra.cid AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id AND r.ar_origin_reference_id = ra.ar_origin_reference_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND s.special_type_id IN ( ' . CSpecialType::GIFT_INCENTIVE . ', ' . CSpecialType::STANDARD . ')
						AND s.deleted_on IS NULL
						AND s.is_active = true
						AND s.special_recipient_id = ' . CSpecialRecipient::RENEWALS . '
						AND ra.ar_cascade_reference_id  = ' . ( int ) $intPropertyId . '
						AND ( s.end_date IS NULL or s.end_date > NOW() )';

		return self::fetchSpecials( $strSql, $objDatabase );

	}

	public static function fetchCurrentPublishedProspectSpecialsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $strMoveInDate = NULL, $arrintExcludeSpecialTypeIds = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setMaxArCascadeId( 1 );
		$objRateLogsFilter->setPricingTypeId( 3 );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		$strSql = 'SELECT
						DISTINCT ON ( s.id )
						s.*,
						ra.hide_description,
						pr.rate_amount,
						pr.rate_increase_increment AS percentage,
						ra.property_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN prospect_rates pr ON( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					 WHERE
						ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.deleted_on IS NULL
						AND s.is_active::BOOLEAN = true
						AND s.show_on_website::BOOLEAN = true
						AND ( ( ' . $strMoveInDate . ' ::Date >= s.start_date AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( ' . $strMoveInDate . ' ::Date >= s.start_date AND s.end_date IS NULL ) OR
									( s.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( s.start_date IS NULL AND s.end_date IS NULL )
									)';

		if( true == valArr( $arrintExcludeSpecialTypeIds ) ) {
			$strSql .= ' AND s.special_type_id NOT IN ( ' . implode( ',', $arrintExcludeSpecialTypeIds ) . ' )';
		}

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchRecurringPaymentSpecialsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						pr.rate_amount AS rate_amount,
						pr.rate_increase_increment AS percentage,
						pr.ar_code_id AS ar_code_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN prospect_rates pr ON( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND s.special_type_id = ' . CSpecialType::RECURRING_PAYMENT_SET_UP_INCENTIVE . '
						AND s.deleted_by IS NULL
						AND pr.ar_cascade_id = ' . CArCascade::PROPERTY . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
						AND ( s.start_date IS NULL OR s.start_date <= NOW() )
						AND ( s.end_date IS NULL OR s.end_date >= NOW() )
					ORDER BY ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchAllSpecialsByPropertyIdByPropertyUnitIdByCid( $intPropertyId, $intPropertyUnitId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						DISTINCT s.*,
						ra.hide_description,
						s.order_num,
						( SELECT COUNT(id) FROM rate_associations WHERE cid = s.cid AND ar_origin_reference_id=s.id AND ar_origin_id = ' . CArOrigin::SPECIAL . ' AND property_id =' . ( int ) $intPropertyId . ' AND ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND cid = ' . ( int ) $intCid . ' ) AS unit_type_count,
						( SELECT COUNT(id) FROM rate_associations WHERE cid = s.cid AND ar_origin_reference_id=s.id AND ar_origin_id = ' . CArOrigin::SPECIAL . ' AND property_id =' . ( int ) $intPropertyId . ' AND ar_cascade_id = ' . CArCascade::SPACE . ' AND cid = ' . ( int ) $intCid . ' ) AS unit_space_count
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						--JOIN unit_spaces us ON ( us.cid = ps.cid AND ps.unit_space_id = us.id ' . $strCheckDeletedUnitSpacesSql . ' )
					WHERE
						s.deleted_on IS NULL
						AND ( s.end_date IS NULL OR NOW() < s.end_date )
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyUnitId . '
						--AND ' . $strCheckDeletedUnitSpacesSql . '
					ORDER BY ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchAllSpecialsByPropertyIdByUnitSpaceIdByCid( $intPropertyId, $intUnitSpaceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT s.*,
						ra.hide_description,
						s.order_num,
						( SELECT COUNT(id) FROM rate_associations WHERE cid = s.cid AND ar_origin_reference_id=s.id AND ar_origin_id = ' . CArOrigin::SPECIAL . ' AND property_id =' . ( int ) $intPropertyId . ' AND ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND cid = ' . ( int ) $intCid . ' ) AS unit_type_count,
						( SELECT COUNT(id) FROM rate_associations WHERE cid = s.cid AND ar_origin_reference_id=s.id AND ar_origin_id = ' . CArOrigin::SPECIAL . ' AND property_id =' . ( int ) $intPropertyId . ' AND ar_cascade_id = ' . CArCascade::SPACE . ' AND cid = ' . ( int ) $intCid . ' ) AS unit_space_count
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						s.deleted_on IS NULL
						AND ( s.end_date IS NULL OR NOW() < s.end_date )
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . '
					ORDER BY ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchPublishedSpecialsCountByPropertyIdByUnitTypeIdsByCid( $intPropertyId, $arrintUnitTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintUnitTypeIds ) ) return 0;

		$strSql = 'SELECT
						COUNT( DISTINCT s.id )
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						s.cid = ' . ( int ) $intCid . '
						AND	s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.deleted_on IS NULL
						AND s.is_active::BOOLEAN = true
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitTypeIds ) . ' )
						AND NOW() >= COALESCE( s.start_date, NOW() )
						AND NOW() <= COALESCE( s.end_date, NOW() )';

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchPublishedSpecialsByPropertyIdByUnitTypeIdsByCid( $arrintPropertyIds, $arrintUnitTypeIds, $intCid, $objDatabase, $intLeaseStartWindowId = NULL, $strMoveInDate = NULL ) {
		if( false == valArr( $arrintUnitTypeIds ) ) return NULL;

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		$strSqlJoin = '';

		if( true == valId( $intLeaseStartWindowId ) ) {
			$strSqlJoin = ' LEFT JOIN installment_plans ip ON ( ip.cid = s.cid AND s.id = ip.special_id AND ip.deleted_by IS NULL )';
		}

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						ra.ar_cascade_reference_id AS unit_type_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )'
		 . $strSqlJoin .
		 ' WHERE
						s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitTypeIds ) . ' )
						AND s.deleted_on IS NULL
						AND s.is_active::BOOLEAN = true
						AND s.special_type_id <> ' . CSpecialType::FIXED_BAH_ADJUSTMENT . '
						AND s.show_on_website::BOOLEAN = true
						AND ( ( ' . $strMoveInDate . ' ::Date >= s.start_date AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( ' . $strMoveInDate . ' ::Date >= s.start_date AND s.end_date IS NULL ) OR
									( s.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( s.start_date IS NULL AND s.end_date IS NULL )
									)';
		if( true == valId( $intLeaseStartWindowId ) ) {
			$strSql .= ' AND CASE
							WHEN ( s.special_type_id = ' . CSpecialType::INSTALLMENT_PLAN . ' ) THEN ip.lease_start_window_id = ' . ( int ) $intLeaseStartWindowId . '
							ELSE TRUE
						END';
		}
		$strSql .= ' ORDER BY ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchPublishedSpecialsByPropertyIdByUnitSpaceIdsByCid( $arrintPropertyIds, $arrintUnitSpaceIds, $intCid, $objDatabase, $boolIsMarketedOnly = false, $strMoveInDate = NULL ) {
		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setUnitSpaceIds( $arrintUnitSpaceIds );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strIsMarketedOnlySql = '';
		if( true == $boolIsMarketedOnly ) {
			$strIsMarketedOnlySql = ' AND s.show_on_website::BOOLEAN = true';
		}

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						ra.unit_space_id,
						pr.rate_amount,
						pr.rate_increase_increment AS percentage
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						LEFT JOIN prospect_rates pr ON( ra.ar_origin_reference_id = pr.ar_origin_reference_id AND ra.cid = pr.cid AND ra.property_id = pr.property_id AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.deleted_on IS NULL
						AND s.is_active::BOOLEAN = true
						' . $strIsMarketedOnlySql . '
						AND ( ( ' . $strMoveInDate . ' ::Date >= s.start_date AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
							  ( ' . $strMoveInDate . ' ::Date >= s.start_date AND s.end_date IS NULL ) OR
							  ( s.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
							  ( s.start_date IS NULL AND s.end_date IS NULL )
							)
						AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
					ORDER BY ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchPublishedSpecialsCountByCidByPropertyIdByUnitSpaceIds( $intCid, $intPropertyId, $arrintUnitSpaceIds, $objDatabase ) {
		if( false == valArr( $arrintUnitSpaceIds ) ) return NULL;
		$strSql = 'SELECT
						COUNT( DISTINCT s.id )
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.is_active::BOOLEAN = true
						AND s.show_on_website::BOOLEAN = true
						AND s.deleted_on IS NULL
						AND NOW() >= COALESCE( s.start_date, NOW() )
						AND NOW() <= COALESCE( s.end_date, NOW() )
						AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid;

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchPublishedSpecialsByCidByPropertyIdByPropertyFloorplanIds( $intCid, $intPropertyId, $arrintPropertyFloorplanIds, $objDatabase, $strMoveInDate = NULL ) {

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		$strSql = 'SELECT
						ut.property_floorplan_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN unit_types ut ON ( ut.cid = s.cid AND ut.property_id = ' . ( int ) $intPropertyId . ' AND ut.property_floorplan_id IN ( ' . implode( ',', $arrintPropertyFloorplanIds ) . ' ) )
					WHERE
						s.cid = ' . ( int ) $intCid . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.is_active::BOOLEAN = true
						AND s.show_on_website::BOOLEAN = true
						AND s.deleted_on IS NULL
						AND ( ( ' . $strMoveInDate . ' ::Date >= s.start_date AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
							  ( ' . $strMoveInDate . ' ::Date >= s.start_date AND s.end_date IS NULL ) OR
							  ( s.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
							  ( s.start_date IS NULL AND s.end_date IS NULL )
							)
						AND ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . '
						AND ra.ar_cascade_reference_id = ut.id GROUP BY ut.property_floorplan_id';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponse ) ) {
			$arrintSpecialRateAssociationsPropertyFloorplanIds = [];

			foreach( $arrmixResponse as $arrintResponse ) {
				$arrintSpecialRateAssociationsPropertyFloorplanIds[] = $arrintResponse['property_floorplan_id'];
			}

			return $arrintSpecialRateAssociationsPropertyFloorplanIds;
		}

		return [ 0 ];
	}

	public static function fetchPublishedSpecialsByPropertyIdByPropertyUnitIdByCid( $intPropertyId, $intPropertyUnitId, $intCid, $objDatabase, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						s.*,
						ra.hide_description
					FROM
						specials s
						JOIN unit_spaces us ON ( us.cid = s.cid AND us.property_id = ' . ( int ) $intPropertyId . ' AND us.property_unit_id = ' . ( int ) $intPropertyUnitId . ' ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						s.cid = ' . ( int ) $intCid . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
						AND s.deleted_on IS NULL
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND (
								( NOW() >= s.start_date AND NOW() <= s.end_date ) OR
								( NOW() >= s.start_date AND s.end_date IS NULL ) OR
								( s.start_date IS NULL AND NOW() <= s.end_date ) OR
								( s.start_date IS NULL AND s.end_date IS NULL )
							)
						ORDER BY ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchPublishedSpecialsByPropertyIdByUnitSpaceIdByCid( $intPropertyId, $intUnitSpaceId, $intCid, $objDatabase, $strMoveInDate = NULL ) {

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setUnitSpaceIds( [ $intUnitSpaceId ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						pr.rate_amount,
						pr.rate_increase_increment AS percentage
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						LEFT JOIN prospect_rates pr ON( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.is_active::BOOLEAN = true
						AND s.show_on_website::BOOLEAN = true
						AND s.deleted_on IS NULL
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . '
						AND ( 	( ' . $strMoveInDate . ' ::Date >= s.start_date AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
								( ' . $strMoveInDate . ' ::Date >= s.start_date AND s.end_date IS NULL ) OR
								( s.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
								( s.start_date IS NULL AND s.end_date IS NULL )
							)
						ORDER BY ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchPublishedProspectPortalPopupSpecialsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						fv.value AS pop_up_special
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN field_values fv ON ( fv.cid = s.cid AND fv.reference_id = s.id )
						JOIN fields fd ON( fd.cid = fv.cid AND fd.id = fv.field_id )
					 WHERE
						s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND fd.field_name = \'Is Popup Special\'
						AND s.deleted_on IS NULL
						AND (
								( NOW() >= s.start_date AND NOW() <= s.end_date )OR
								( NOW() >= s.start_date AND s.end_date IS NULL ) OR
								( s.start_date IS NULL AND NOW() <= s.end_date ) OR
								( s.start_date IS NULL AND s.end_date IS NULL )
							)
						 ORDER BY s.updated_on DESC ';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialsByPropertyIdByUnitSpaceIdByCid( $intPropertyId, $intUnitSpaceId, $intCid, $arrintLeaseTermIds = NULL, $objDatabase, $boolIncludeDeletedUnits = false, $boolOnlyIncludeMarketed = false, $strMoveInDate = NULL ) {

		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		if( false == valArr( $arrintLeaseTermIds ) ) {
			$arrintLeaseTermIds = [ 0 ];
		}

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::BASE, CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setUnitSpaceIds( [ $intUnitSpaceId ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckMarketedSpecialsSql = ( true == $boolOnlyIncludeMarketed ) ? ' AND s.show_on_website = TRUE' : '';

		$strSql = 'SELECT
						DISTINCT ON (s.id, pr.lease_term_id)
						s.*,
						pr.lease_term_id,
						ra.hide_description,
						pr.rate_interval_occurances AS rate_interval_occurances,
						pr.rate_amount,
						pr.rate_increase_increment AS percentage
					FROM
						unit_spaces AS us
						JOIN rate_associations ra
						ON (
							( ra.ar_cascade_id =  ' . CArCascade::SPACE . ' AND ra.ar_cascade_reference_id = us.id )
								OR( ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND ra.ar_cascade_reference_id = us.unit_type_id AND ra.unit_space_id IS NULL )
								OR ( ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' AND ra.ar_cascade_reference_id = us.property_floorplan_id AND ra.unit_type_id IS NULL AND ra.unit_space_id IS NULL )
								OR ( ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND ra.ar_cascade_reference_id = us.property_id AND ra.unit_type_id IS NULL AND ra.unit_space_id IS NULL )
						)
						JOIN specials AS s
						ON (
								s.id = ra.ar_origin_reference_id
								AND s.cid = ra.cid
								AND s.cid = ' . ( int ) $intCid . '
								AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
								AND s.deleted_on IS NULL
								AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
								AND s.is_active::BOOLEAN = true
								' . $strCheckMarketedSpecialsSql . '
								AND ( ( ' . $strMoveInDate . ' ::Date >= s.start_date AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( ' . $strMoveInDate . ' ::Date >= s.start_date AND s.end_date IS NULL ) OR
									( s.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( s.start_date IS NULL AND s.end_date IS NULL )
									) )
						LEFT JOIN prospect_rates pr ON( pr.show_on_website::BOOLEAN = true AND s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						us.id = ' . ( int ) $intUnitSpaceId . '
						AND ( pr.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' ) OR pr.id IS NULL)
						AND us.property_id = ' . ( int ) $intPropertyId . '
						AND us.cid = ' . ( int ) $intCid . $strCheckDeletedUnitsSql;

		return self::fetchSpecials( $strSql, $objDatabase, false );
	}

	public static function fetchWebsiteSpecialsByPropertyIdByUnitTypeIdByFloorplanIdByUnitSpaceIdByCid( $intPropertyId, $intUnitTypeId, $intFloorPlanId, $intUnitSpaceId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						s.*
					FROM
						unit_spaces AS us
						JOIN rate_associations ra
						ON (
							( ra.ar_cascade_id =  ' . CArCascade::SPACE . ' AND ra.ar_cascade_reference_id = us.id )
								OR( ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND ra.ar_cascade_reference_id = us.unit_type_id AND ra.unit_space_id IS NULL )
								OR ( ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' AND ra.ar_cascade_reference_id = us.property_floorplan_id AND ra.unit_type_id IS NULL AND ra.unit_space_id IS NULL )
								OR ( ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND ra.ar_cascade_reference_id = us.property_id AND ra.unit_type_id IS NULL AND ra.unit_space_id IS NULL )
						)
						JOIN specials AS s
						ON (	s.id = ra.ar_origin_reference_id
								AND s.cid = ra.cid
								AND s.cid = ' . ( int ) $intCid . '
								AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
								AND s.deleted_on IS NULL
								AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
								AND s.is_active::BOOLEAN = true
								AND s.show_on_website::BOOLEAN = true
								AND COALESCE( s.start_date, \'1970-01-01\'::DATE ) <= NOW()
								AND COALESCE( s.end_date, \'2099-12-31\'::DATE ) >= NOW()
							)
					WHERE
						us.id = ' . ( int ) $intUnitSpaceId .
		 ( ( false == is_null( $intUnitTypeId ) && false == empty( $intUnitTypeId ) ) ? ' AND us.unit_type_id = ' . ( int ) $intUnitTypeId : '' ) .
		 ( ( false == is_null( $intFloorPlanId ) && false == empty( $intFloorPlanId ) ) ? ' AND us.property_floorplan_id = ' . ( int ) $intFloorPlanId : '' ) .
		 ' AND us.property_id = ' . ( int ) $intPropertyId . '
						AND us.cid = ' . ( int ) $intCid;

		return self::fetchSpecials( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchSpecialsWithLeaseTermsByPropertyIdByPropertyFloorPlanIdByUnitTypeIdByUnitSpaceIdByCid( $intPropertyId, $intFloorPlanId, $intUnitTypeId, $intUnitSpaceId, $intCid, $objDatabase, $boolOnlyIncludeMarketed = false ) {

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setIsEntrata( true );

		if( false == is_null( $intFloorPlanId ) && false == empty( $intFloorPlanId ) ) {
			$objRateLogsFilter->setPropertyFloorplanIds( [ $intFloorPlanId ] );
		}

		if( false == is_null( $intUnitTypeId ) && false == empty( $intUnitTypeId ) ) {
			$objRateLogsFilter->setUnitTypeIds( [ $intUnitTypeId ] );
		}

		$objRateLogsFilter->setUnitSpaceIds( [ $intUnitSpaceId ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strCheckMarketedSpecialsSql = ( true == $boolOnlyIncludeMarketed ) ? ' AND s.show_on_website = TRUE' : '';

		$strSql = 'SELECT
						s.*,
						pr.lease_term_id
					FROM
						unit_spaces AS us
						JOIN rate_associations ra
						ON (
							( ra.ar_cascade_id =  ' . CArCascade::SPACE . ' AND ra.ar_cascade_reference_id = us.id )
								OR( ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND ra.ar_cascade_reference_id = us.unit_type_id AND ra.unit_space_id IS NULL )
								OR ( ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' AND ra.ar_cascade_reference_id = us.property_floorplan_id AND ra.unit_type_id IS NULL AND ra.unit_space_id IS NULL )
								OR ( ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND ra.ar_cascade_reference_id = us.property_id AND ra.unit_type_id IS NULL AND ra.unit_space_id IS NULL )
						)
						JOIN specials AS s
						ON (
								s.id = ra.ar_origin_reference_id
								AND s.cid = ra.cid
								AND s.cid = ' . ( int ) $intCid . '
								AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
								AND s.deleted_on IS NULL
								AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
								AND s.is_active::BOOLEAN = true
								' . $strCheckMarketedSpecialsSql . '
								AND ( ( NOW() >= s.start_date AND NOW() <= s.end_date ) OR
									( NOW() >= s.start_date AND s.end_date IS NULL ) OR
									( s.start_date IS NULL AND NOW() <= s.end_date ) OR
									( s.start_date IS NULL AND s.end_date IS NULL )
									) )
						LEFT JOIN prospect_rates pr ON( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						us.id = ' . ( int ) $intUnitSpaceId .
		 ( ( false == is_null( $intUnitTypeId ) && false == empty( $intUnitTypeId ) ) ? ' AND us.unit_type_id = ' . ( int ) $intUnitTypeId : '' ) .
		 ( ( false == is_null( $intFloorPlanId ) && false == empty( $intFloorPlanId ) ) ? ' AND us.property_floorplan_id = ' . ( int ) $intFloorPlanId : '' ) .
		 ' AND CASE WHEN  pr.id IS NULL
								  THEN TRUE
								  ELSE pr.show_on_website::BOOLEAN = true
							  END
						AND us.property_id = ' . ( int ) $intPropertyId . '
						AND us.cid = ' . ( int ) $intCid;

		return self::fetchSpecials( $strSql, $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchAppliedSpecials( $objLease, $objDatabase ) {

		$arrobjSpecials = NULL;

		$objUnitSpace 		= $objLease->getOrFetchUnitSpace( $objDatabase );
		$objPropertyUnit	= $objLease->getOrFetchPropertyUnit( $objDatabase );
		$objProperty		= $objLease->getOrFetchProperty( $objDatabase );

		// Fetch Unit Space Level
		if( false == is_null( $objUnitSpace->getId() ) ) {
			$arrobjSpecials = self::fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermByCid( $objProperty->getId(), $objUnitSpace->getId(), NULL, NULL, true, $objLease->getCid(), $objDatabase );
		}

		// Fetch UnitType Level
		if( false == valArr( $arrobjSpecials ) && 0 == \Psi\Libraries\UtilFunctions\count( $arrobjSpecials ) && true == valObj( $objPropertyUnit, 'CPropertyUnit', 'UnitTypeId' ) ) {
			$arrobjSpecials = self::fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermByCid( $objProperty->getId(), NULL, $objPropertyUnit->getUnitTypeId(), NULL, true, $objLease->getCid(), $objDatabase );
		}

		// Fetch Property Level
		if( false == valArr( $arrobjSpecials ) ) {
			$arrobjSpecials = self::fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermByCid( $objProperty->getId(), NULL, NULL, NULL, true, $objLease->getCid(), $objDatabase );
		}

		return $arrobjSpecials;
	}

	public static function fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermByCid( $intPropertyId, $intUnitSpaceId, $intUnitTypeId, $intLeaseTerm, $intCid, $objDatabase, $arrintUnitTypeIds = NULL ) {

		if( false == is_null( $arrintUnitTypeIds ) && false == valArr( $arrintUnitTypeIds ) ) return NULL;

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setUnitSpaceIds( [ $intUnitSpaceId ] );
		$objRateLogsFilter->setUnitTypeIds( [ $intUnitTypeId ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		if( true == is_null( $intLeaseTerm ) ) {
			$intLeaseTerm = 1;
		}

		if( true == is_null( $intUnitSpaceId ) && true == is_null( $intUnitTypeId ) && true == is_null( $arrintUnitTypeIds ) ) {
			$strCondition = ' ra.unit_space_id IS NULL AND ra.unit_type_id IS NULL ';
		} else {
			$strCondition = ( true == is_null( $intUnitSpaceId ) ) ? ' ra.unit_space_id IS NULL ' : ' ra.unit_space_id = ' . ( int ) $intUnitSpaceId . ' AND ' . CArCascade::SPACE;
			$strCondition .= ' AND ';
			if( true == is_null( $arrintUnitTypeIds ) ) {
				$strCondition .= ( true == is_null( $intUnitTypeId ) ) ? ' ra.unit_type_id IS NULL ' : ' ra.unit_type_id = ' . ( int ) $intUnitTypeId . ' AND ' . CArCascade::UNIT_TYPE;
			} elseif( true == valArr( $arrintUnitTypeIds ) ) {
				$strCondition .= ' ra.unit_type_id IN ( ' . implode( ',', $arrintUnitTypeIds ) . ' ) AND ' . CArCascade::UNIT_TYPE;
			}
		}

		$strSql = 'SELECT
						DISTINCT s.*,
						ra.hide_description,
						pr.rate_amount AS rate_amount,
						pr.rate_increase_increment AS percentage,
						pr.rate_interval_start,
						pr.rate_interval_occurances,
						pr.ar_code_id AS ar_code_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN prospect_rates pr ON( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						--JOIN rates r ON( r.cid = ra.cid AND r.ar_origin_reference_id = ra.ar_origin_reference_id AND r.ar_origin_id = ra.ar_origin_id AND r.ar_cascade_id = ra.ar_cascade_id )
					WHERE
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ( ' . $strCondition . ' )
						AND s.deleted_on IS NULL
						AND (
								( NOW() >= s.start_date AND NOW() <= s.end_date )OR
								( NOW() >= s.start_date AND s.end_date IS NULL ) OR
								( s.start_date IS NULL AND NOW() <= s.end_date ) OR
								( s.start_date IS NULL AND s.end_date IS NULL )
							)
						AND pr.lease_term_months = ' . ( int ) $intLeaseTerm . '
						AND s.special_type_id <> ' . CSpecialType::RECURRING_PAYMENT_SET_UP_INCENTIVE . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS;

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialsByUnitSpaceIdsByPropertyIdsByLeaseTermIdsByCid( $arrintUnitSpaceIds, $arrintPropertyIds, $arrintLeaseTermIds, $intCid, $objDatabase, $boolIncludeDeletedUnits = false ) {
		if( false == valArr( $arrintUnitSpaceIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;
		if( false == valArr( $arrintLeaseTermIds ) ) return NULL;
		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( $arrintPropertyIds );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );
		$objRateLogsFilter->setUnitSpaceIds( $arrintUnitSpaceIds );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						*
					FROM
					(
						SELECT
							ra.unit_space_id,
							ra.ar_origin_reference_id,
							pr.lease_term_id,
							pr.lease_term_months,
							util_get_translated( \'name\', s.name, s.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS name,
							s.start_date,
							s.end_date,
							rank() OVER ( PARTITION BY ra.cid, ra.property_id, ra.unit_space_id ORDER BY ra.order_special )
						FROM
							(
								SELECT
									ra.id,
									ra.cid,
									ra.property_id,
									ra.ar_origin_id,
									ra.ar_origin_reference_id,
									us.id AS unit_space_id,
									CASE
										WHEN ra.unit_space_id IS NOT NULL THEN 1
										WHEN ra.unit_type_id IS NOT NULL THEN 2
										ELSE 3
									END AS order_special
								FROM
								(
									SELECT
										us.id,
										us.cid,
										us.property_id,
										pu.unit_type_id,
										pu.property_floorplan_id
									FROM
										unit_spaces us
										JOIN property_units pu ON ( us.cid = pu.cid AND us.property_id = pu.property_id AND us.property_unit_id = pu.id ' . $strCheckDeletedUnitsSql . ' )
									WHERE
										us.id IN ( ' . implode( ',', $arrintUnitSpaceIds ) . ' )
										AND us.cid = ' . ( int ) $intCid . '
								) AS us
								JOIN rate_associations ra ON( ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.cid = us.cid AND us.property_id = ra.property_id AND ( ( ra.unit_type_id IS NULL AND ra.unit_space_id IS NULL ) OR ( ( ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND ra.ar_cascade_reference_id = us.unit_type_id ) OR ( ra.ar_cascade_id = ' . CArCascade::SPACE . ' AND ra.ar_cascade_reference_id = us.id ) ) )	)
								WHERE ra.cid = ' . ( int ) $intCid . '
							) AS ra
							JOIN specials s ON ( s.cid = ra.cid AND ra.ar_origin_reference_id = s.id AND s.special_type_id IN ( ' . CSpecialType::STANDARD . ' ) AND s.coupon_code IS NULL AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . ' AND s.is_active = TRUE )
							JOIN prospect_rates pr ON ( pr.cid = s.cid AND pr.ar_origin_id = ra.ar_origin_id AND pr.lease_term_id IN ( ' . implode( ',', $arrintLeaseTermIds ) . ' ) )
						WHERE
							ra.cid = ' . ( int ) $intCid . '
							AND pr.ar_trigger_id IN( ' . CArTrigger::APPLICATION_COMPLETED . ', ' . CArTrigger::MONTHLY . ' )
							AND s.deleted_on IS NULL
					) AS sub_query
					WHERE
						sub_query.rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByLeaseTermBySpecialPromoCodeByCid( $intPropertyId, $intUnitSpaceId, $intUnitTypeId, $intLeaseTerm, $strCouponCode, $intCid, $objDatabase ) {

		if( true == is_null( $intLeaseTerm ) ) {
			$intLeaseTerm = 1;
		}

		if( 0 < $intUnitSpaceId ) {
			$arrstrCaseAndWhereCondition[3] = ' ( ra.ar_cascade_id = ' . CArCascade::SPACE . ' AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . ' AND ra.unit_type_id IS NULL ) ';
		}

		if( 0 < $intUnitTypeId ) {
			$arrstrCaseAndWhereCondition[2] = ' ( ra.unit_space_id IS NULL AND ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND ra.ar_cascade_reference_id  = ' . ( int ) $intUnitTypeId . ' ) ';
		}

		$arrstrCaseAndWhereCondition[1] 	= ' ( ra.unit_space_id IS NULL AND ra.unit_type_id IS NULL ) ';

		$strLeaseTermCondition	= ( 0 < $intLeaseTerm ) ? ' ( r.lease_term_months = ' . ( int ) $intLeaseTerm . ' ) ' : '  ';
		$strCaseStatement		= ' CASE ';
		$strWhereCondition		= NULL;

		foreach( $arrstrCaseAndWhereCondition as $intKey => $strCaseAndWhereCondition ) {
			$strCaseStatement 	.= ' WHEN ' . $strCaseAndWhereCondition . ' THEN ' . ( int ) $intKey;
			$strWhereCondition	.= ( ( 0 < strlen( $strWhereCondition ) ) ? ' OR ' : '' ) . $strCaseAndWhereCondition;
		}

		$strCaseStatement .= ' END ';

		$strCommonSql = ' FROM
							specials s
							INNER JOIN rate_associations ra ON( s.cid = ra.cid AND s.id = ra.ar_origin_reference_id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.property_id = ' . ( int ) $intPropertyId . ' AND ra.cid = ' . ( int ) $intCid . ' )
							LEFT OUTER JOIN rates r ON( r.cid = ra.cid AND r.ar_origin_reference_id = ra.ar_origin_reference_id AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id AND r.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ' . $strLeaseTermCondition . ' )
						WHERE
						s.deleted_on IS NULL
						AND lower( s.coupon_code ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strCouponCode ) ) . '\'
						AND (
								( NOW() >= s.start_date AND NOW() <= s.end_date ) OR ( NOW() >= s.start_date AND s.end_date IS NULL )
							 OR
								( s.start_date IS NULL AND NOW() <= s.end_date ) OR ( s.start_date IS NULL AND s.end_date IS NULL )
							)
						AND s.special_recipient_id <> ' . CSpecialRecipient::RESIDENTS . '
						AND ( ' . $strWhereCondition . ' )';

		$strSql = 'SELECT * FROM
						( SELECT DISTINCT s.*, ' . $strCaseStatement . ' AS special_type_id	' . $strCommonSql . ' ) as subq1,
						r.ar_trigger_id AS ar_trigger_id,
						r.rate_interval_occurances AS rate_interval_occurances
					WHERE
					 special_type_id = ( SELECT MAX ( ' . $strCaseStatement . ' ) as max_special_type_id ' . $strCommonSql . ' ) ';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchIntegratedSpecialsByPropertyIdByPropertyUnitIdByCid( $intPropertyId, $intPropertyUnitId, $intCid, $objDatabase ) {
		if( true == is_null( $intPropertyUnitId ) ) return NULL;

		$arrobjUnitSpaces = ( array ) \Psi\Eos\Entrata\CUnitSpaces::createService()->fetchUnitSpacesByPropertyUnitIdByCid( $intPropertyUnitId, $intCid, $objDatabase );

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );

		if( true == valArr( $arrobjUnitSpaces ) ) {
			$objRateLogsFilter->setUnitSpaceIds( array_keys( $arrobjUnitSpaces ) );
		}

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strSql = 'SELECT
						DISTINCT s.*,
						ra.hide_description,
						pr.rate_amount,
						pr.rate_increase_increment AS percentage,
						pr.rate_interval_start,
						pr.rate_interval_occurances
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN prospect_rates pr ON( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . CArCascade::SPACE . '
						AND ra.ar_cascade_reference_id IN ( ' . implode( ',', array_keys( $arrobjUnitSpaces ) ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						AND s.coupon_code IS NULL
						AND s.deleted_on IS NULL
						AND (
								( NOW() >= s.start_date AND NOW() <= s.end_date )OR
								( NOW() >= s.start_date AND s.end_date IS NULL ) OR
								( s.start_date IS NULL AND NOW() <= s.end_date ) OR
								( s.start_date IS NULL AND s.end_date IS NULL )
							)
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.remote_primary_key IS NOT NULL
						AND ra.remote_primary_key IS NOT NULL';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchIntegratedSpecialsByPropertyIdByUnitTypeIdByCid( $intPropertyId, $intUnitTypeId, $intLeaseTerm, $intCid, $objDatabase ) {
		if( true == is_null( $intUnitTypeId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT s.*,
						ra.hide_description,
						r.rate_interval_start AS rate_interval_start,
						r.rate_amount AS rate_amount,
						r.rate_interval_occurances AS rate_interval_occurances,
						r.ar_formula_id AS ar_formula_id,
						r.ar_formula_reference_id AS ar_formula_reference_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN rates r ON( r.cid = ra.cid AND r.ar_origin_reference_id = ra.ar_origin_reference_id AND r.ar_origin_id = ra.ar_origin_id AND r.ar_cascade_id = ra.ar_cascade_id )
					WHERE
						r.property_id = ' . ( int ) $intPropertyId . '
						AND r.ar_cascade_id = ' . CArCascade::UNIT_TYPE . '
						AND r.ar_cascade_reference_id = ' . ( int ) $intUnitTypeId . '
						AND r.cid = ' . ( int ) $intCid . '
						AND s.coupon_code IS NULL
						AND s.deleted_on IS NULL
						AND (
								( NOW() >= s.start_date AND NOW() <= s.end_date )OR
								( NOW() >= s.start_date AND s.end_date IS NULL ) OR
								( s.start_date IS NULL AND NOW() <= s.end_date ) OR
								( s.start_date IS NULL AND s.end_date IS NULL )
							)
						AND r.lease_term_months = ' . ( int ) $intLeaseTerm . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.remote_primary_key IS NOT NULL
						AND r.remote_primary_key IS NOT NULL';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchResidentSpecialsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $arrintSpecialTypeIds = NULL ) {

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						r.rate_amount AS rate_amount,
						r.ar_trigger_id AS ar_trigger_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN rates r ON( r.cid = ra.cid AND r.ar_origin_reference_id = ra.ar_origin_reference_id AND r.ar_origin_id = ra.ar_origin_id AND r.ar_cascade_id = ra.ar_cascade_id )
					WHERE
						s.cid = ' . ( int ) $intCid . '
						AND r.property_id = ' . ( int ) $intPropertyId . '
						AND s.special_recipient_id = ' . CSpecialRecipient::RESIDENTS . '
						AND ( s.start_date IS NULL OR s.end_date >= NOW() )
						AND ( s.start_date IS NULL OR s.start_date <= NOW() )';

		if( true == valArr( $arrintSpecialTypeIds ) ) {
			$strSql .= 'AND s.special_type_id IN ( ' . implode( ',', $arrintSpecialTypeIds ) . ' )
						AND r.ar_trigger_id = ' . CArTrigger:: MOVE_IN;
		}

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialByLeaseIntervalIdByCid( $intLeaseIntervalId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						ip.id,
						ip.name
					FROM
						lease_intervals li
						JOIN lease_interval_installment_plans liip ON (li.cid = liip.cid AND li.id = liip.lease_interval_id) 
						JOIN installment_plans ip ON ( ip.cid = liip.cid AND ip.id = liip.installment_plan_id )
					WHERE
						li.cid = ' . ( int ) $intCid . '
						AND li.id = ' . ( int ) $intLeaseIntervalId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSpecialsByIdsByCid( $arrintSpecialIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSpecialIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						specials s
					WHERE
						s.cid = ' . ( int ) $intCid . '
						AND s.id IN( ' . implode( ',', $arrintSpecialIds ) . ' )';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialsByLeaseIdByPropertyIdByCid( $intLeaseId, $intPropertyId, $intCid, $objDatabase, $intFloorPlanId = NULL, $intUnitTypeId = NULL, $intUnitSpaceId = NULL ) {

		if( false == valId( $intLeaseId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strRateAssociationJoinSql = '';

		if( true == valId( $intFloorPlanId ) ) {
			$strRateAssociationJoinSql .= ' WHEN ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' THEN ra.ar_cascade_reference_id = ' . $intFloorPlanId . '';
		}

		if( true == valId( $intUnitTypeId ) ) {
			$strRateAssociationJoinSql .= ' WHEN ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' THEN ra.ar_cascade_reference_id = ' . $intUnitTypeId . '';
		}

		if( true == valId( $intUnitSpaceId ) ) {
			$strRateAssociationJoinSql .= ' WHEN ra.ar_cascade_id = ' . CArCascade::SPACE . ' THEN ra.ar_cascade_reference_id = ' . $intUnitSpaceId . '';
		}

		$strSql = ' SELECT
						*
					FROM
						(
							SELECT
								s.id,
								s.name,
								util_get_system_translated( \'name\', st.name, st.details ) AS special_type_name,
								s.special_type_id,
								CASE
									WHEN s.special_type_id = ' . CSpecialType::GIFT_INCENTIVE . ' THEN s.gift_value
									ELSE sc.charge_amount
								END AS rate_amount,
								li.id AS lease_interval_id,
								li.lease_start_date as lease_interval_start_date,
								li.lease_end_date as lease_interval_end_date,
								li.lease_status_type_id as lease_status_type_id
							FROM
								specials s
								JOIN special_types AS st ON ( s.special_type_id = st.id )
								JOIN rate_associations AS ra ON ( s.cid = ra.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND s.id = ra.ar_origin_reference_id AND CASE WHEN ra.ar_cascade_id = 1 THEN ra.ar_cascade_reference_id = ' . $intPropertyId . ' ' . $strRateAssociationJoinSql . ' ELSE TRUE END )
								JOIN scheduled_charges AS sc ON ( sc.cid = s.cid AND sc.property_id = ra.property_id AND sc.ar_origin_id = ra.ar_origin_id AND sc.ar_origin_reference_id = s.id AND sc.ar_trigger_id IN( ' . \CArTrigger::APPLICATION_COMPLETED . ', ' . \CArTrigger::MONTHLY . ' ) AND sc.deleted_by IS NULL AND sc.deleted_on IS NULL )
								JOIN lease_intervals li ON ( li.cid = sc.cid AND li.property_id = sc.property_id AND li.lease_id = sc.lease_id AND li.id = sc.lease_interval_id )
							WHERE
								sc.lease_id = ' . $intLeaseId . ' AND
								sc.property_id = ' . $intPropertyId . ' AND
								sc.cid = ' . $intCid . ' 
							
							UNION
							
							SELECT
								s.id,
								s.name,
								util_get_system_translated( \'name\', st.name, st.details ) AS special_type_name,
								s.special_type_id,
								CASE
									WHEN s.special_type_id = ' . CSpecialType::GIFT_INCENTIVE . ' THEN s.gift_value
									ELSE sc.charge_amount
								END AS rate_amount,
								li.id AS lease_interval_id,
								li.lease_start_date as lease_interval_start_date,
								li.lease_end_date as lease_interval_end_date,
								li.lease_status_type_id as lease_status_type_id
							FROM
								specials s
								JOIN special_types AS st ON ( s.special_type_id = st.id )
								JOIN lease_associations AS la ON ( s.cid = la.cid AND la.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND s.id = la.ar_origin_reference_id AND la.deleted_by IS NULL AND la.deleted_on IS NULL )
								JOIN lease_intervals li ON ( li.cid = la.cid AND li.property_id = la.property_id AND li.lease_id = la.lease_id AND li.id = la.lease_interval_id )
								LEFT JOIN scheduled_charges AS sc ON ( sc.cid = la.cid AND sc.property_id = la.property_id AND sc.lease_id = la.lease_id AND sc.ar_origin_id = la.ar_origin_id AND sc.ar_origin_reference_id = la.ar_origin_reference_id AND sc.ar_trigger_id IN( ' . \CArTrigger::APPLICATION_COMPLETED . ', ' . \CArTrigger::MONTHLY . ' ) AND sc.deleted_by IS NULL AND sc.deleted_on IS NULL )
							WHERE
								la.lease_id = ' . $intLeaseId . ' AND
								la.property_id = ' . $intPropertyId . ' AND
								la.cid = ' . $intCid . '
						) as customer_specials
						ORDER BY
							customer_specials.lease_interval_id DESC,
							customer_specials.special_type_id';

		return self::fetchSpecials( $strSql, $objDatabase, false );

	}

	// TODO:Remaining

	public static function fetchSpecialsByPropertyIdByUnitSpaceIdByUnitTypeIdByPropertyFloorplanIdByLeaseTermByCid( $intPropertyId, $intUnitSpaceId, $intUnitTypeId, $intLeaseTerm, $intCid, $objDatabase, $arrintUnitTypeIds = NULL, $intPropertyFloorplanId = NULL ) {

		if( false == is_null( $arrintUnitTypeIds ) && false == valArr( $arrintUnitTypeIds ) ) return NULL;

		if( true == is_null( $intLeaseTerm ) ) {
			$intLeaseTerm = 1;
		}

		if( true == is_null( $intUnitSpaceId ) && true == is_null( $intUnitTypeId ) && true == is_null( $arrintUnitTypeIds ) && true == is_null( $intPropertyFloorplanId ) ) {
			$strCondition = ' ra.unit_space_id IS NULL AND ra.unit_type_id IS NULL AND ra.property_floorplan_id IS NULL';
		} else {
			$strCondition = ( true == is_null( $intUnitSpaceId ) ) ? ' ra.unit_space_id IS NULL ' : ' ra.unit_space_id = ' . ( int ) $intUnitSpaceId . ' AND ra.ar_cascade_id = ' . CArCascade::SPACE;
			$strCondition .= ' OR ';
			if( true == is_null( $arrintUnitTypeIds ) ) {
				$strCondition .= ( true == is_null( $intUnitTypeId ) ) ? ' ra.unit_type_id IS NULL ' : ' ra.unit_type_id = ' . ( int ) $intUnitTypeId . ' AND ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE;
			} elseif( true == valArr( $arrintUnitTypeIds ) ) {
				$strCondition .= ' ra.unit_type_id IN ( ' . implode( ',', $arrintUnitTypeIds ) . ' ) AND ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE;
			}

			if( false == is_null( $intPropertyFloorplanId ) ) {
				$strCondition .= ( true == is_null( $intPropertyFloorplanId ) ) ? ' AND ra.property_floorplan_id IS NULL ' : ' AND ra.property_floorplan_id = ' . ( int ) $intPropertyFloorplanId . ' AND ra.ar_cascade_id ' . CArCascade::FLOOR_PLAN;
			}
		}

		$strSql = 'SELECT
						DISTINCT s.*,
						ra.hide_description,
						r.rate_interval_start AS rate_interval_start
					FROM
						specials s
						JOIN rate_associations ra ON ( s.cid = ra.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN rates r ON ( r.cid = ra.cid AND r.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id AND r.ar_origin_reference_id = ra.ar_origin_reference_id )
					WHERE
						ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ( ' . $strCondition . ' )
						AND s.deleted_on IS NULL
						AND (
								( NOW() >= s.start_date AND NOW() <= s.end_date )OR
								( NOW() >= s.start_date AND s.end_date IS NULL ) OR
								( s.start_date IS NULL AND NOW() <= s.end_date ) OR
								( s.start_date IS NULL AND s.end_date IS NULL )
							)
						AND ( r.lease_term_months = 0 OR r.lease_term_months = ' . ( int ) $intLeaseTerm . ' )
						AND s.special_type_id <> ' . CSpecialType::RECURRING_PAYMENT_SET_UP_INCENTIVE . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS;

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSimpleSpecialsByArCascadeIdByPropetyIdByCid( $intArCascadeId, $intPropertyId, $intCid, $objDatabase, $intArCascadeReferenceId = NULL, $strUnitSpaceName = NULL, $arrintExcludeSpecialTypeIds = NULL ) {

		$strCondition = '';

		if( CArCascade::UNIT_TYPE == $intArCascadeId ) {
			$strJoin = 'JOIN unit_types ut ON ( ut.cid = ra.cid AND ut.property_id = ra.property_id AND ut.id = ra.ar_cascade_reference_id ) ';
			$strSelectFields = ' ,ut.id AS unit_type_id, ut.name AS unit_type_name ';
		} elseif( CArCascade::SPACE == $intArCascadeId ) {
			$strJoin = 'JOIN unit_spaces us ON ( us.cid = ra.cid AND us.property_id = ra.property_id AND us.id = ra.ar_cascade_reference_id ) ';
			$strSelectFields = ' ,us.id AS unit_space_id, us.unit_number_cache AS unit_space_name, us.property_building_id ';
		} elseif( CArCascade::FLOOR_PLAN == $intArCascadeId ) {
			$strJoin = 'JOIN property_floorplans pf ON ( pf.cid = ra.cid AND pf.property_id = ra.property_id AND pf.id = ra.ar_cascade_reference_id ) ';
			$strSelectFields = ' ,pf.id AS floor_plan_id, pf.floorplan_name AS floor_plan_name';
		}

		if( true == valId( $intArCascadeReferenceId ) ) {
			$strCondition .= ' AND ra.ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId;
		}

		if( true == valStr( $strUnitSpaceName ) ) {
			$strCondition .= ' AND lower( us.unit_number_cache ) LIKE lower( \'%' . $strUnitSpaceName . '%\' )';
		}

		if( true == valArr( $arrintExcludeSpecialTypeIds ) ) {
			$strCondition .= ' AND s.special_type_id NOT IN ( ' . implode( ', ', $arrintExcludeSpecialTypeIds ) . ')';
		}

		$strSql = 'SELECT
						s.id, s.is_active, s.show_on_website, s.start_date, s.end_date, s.remote_primary_key, s.special_recipient_id, s.quantity_remaining, s.quantity_budgeted,'
						. ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ? 'util_get_translated( \'name\', s.name, s.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ' : 's.name' ) . ' as name,
						' . ( false == empty( CLocaleContainer::createService()->getDefaultLocaleCode() ) ? 'util_get_translated( \'name\', s.name, s.details, \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' ) ' : 's.name' ) . ' as default_locale_name,
						' . ( false == empty( CLocaleContainer::createService()->getTargetLocaleCode() ) ? 'util_get_translated( \'description\', s.description, s.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ' : 's.description' ) . ' as description,
						' . ( false == empty( CLocaleContainer::createService()->getDefaultLocaleCode() ) ? 'util_get_translated( \'description\', s.description, s.details, \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' ) ' : 's.description' ) . ' as default_locale_description,
						util_get_system_translated( \'name\',  sr.name, sr.details )  as special_recipient_name,
						util_get_system_translated( \'name\', st.name, st.details ) as special_type_name,
						st.id as special_type_id,
						ra.is_published AS rate_association_is_published,
						ra.hide_description
						' . $strSelectFields . '
					FROM
						specials s
						JOIN special_recipients sr ON ( s.special_recipient_id = sr.id )
						JOIN special_types st ON ( st.id = s.special_type_id )
						JOIN rate_associations ra ON ( s.cid = ra.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
						' . $strJoin . '
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '' . $strCondition . '
						AND s.deleted_on IS NULL
						AND s.special_group_id IS NULL
					ORDER BY
						s.created_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSpecialsByArCascadeIdByPropertyIdByCid( $intArCascadeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						ra.hide_description
					FROM
						specials s
						LEFT JOIN offer_items oi ON ( s.cid = oi.cid AND oi.deleted_by IS NULL AND oi.ar_origin_reference_id = s.id )
						LEFT JOIN offers o ON ( oi.cid = o.cid AND oi.offer_id = o.id AND o.deleted_by IS NULL)
						LEFT JOIN offer_templates ot ON (o.cid = ot.cid AND o.offer_template_id = ot.id)
						JOIN rate_associations ra ON ( s.cid = ra.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND s.deleted_by IS NULL
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'ot.name' ) . ',
						' . $objDatabase->getCollateSort( 's.name' );

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchNonDeletedSpecialsBySpecialGroupIdByCid( $intSpecialGroupId, $intCid, $objDatabase ) {
		return self::fetchSpecials( sprintf( 'SELECT s.* FROM specials s WHERE s.deleted_by IS NULL AND s.deleted_on IS NULL AND s.special_group_id = %d AND s.cid = %d ORDER BY s.min_days_to_lease_start DESC,start_date ASC', ( int ) $intSpecialGroupId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRenewalTierSpecialsByIdsByCid( $arrintIds = [], $intCid, $objDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						oi.ar_origin_reference_id AS id,
						o.days_offset AS max_days_to_lease_start,
						o.start_date,
						o.end_date,
						o.max_offer_count,
						COALESCE ( lag ( o.days_offset, 1 ) OVER ( ORDER BY
						o.days_offset ), (
											SELECT
												o1.days_offset
											FROM
												offers o1
											WHERE
												o1.cid = o.cid
												AND o1.offer_template_id = o.offer_template_id
												AND o1.deleted_by IS NULL
												AND o1.days_offset < o.days_offset
											ORDER BY
												o1.days_offset DESC
											LIMIT 1
						) + 1, 0 ) AS min_days_to_lease_start,
						s.special_type_id,
						s.quantity_budgeted,
						COALESCE ( s.quantity_remaining, o.max_offer_count ) AS quantity_remaining,
						COALESCE ( s.quantity_budgeted, o.max_offer_count ) AS quantity_budgeted,
						COALESCE ( s.limit_quantity, true ) AS limit_quantity,
						oi.name,
						ot.offer_template_type_id AS special_group_type_id,
						ra.hide_description
					FROM
						offer_items oi
						JOIN offers o ON ( oi.cid = o.cid AND oi.offer_id = o.id AND o.deleted_by IS NULL )
						JOIN offer_templates ot ON ( ot.cid = o.cid AND ot.id = o.offer_template_id )
						JOIN specials s ON ( oi.cid = s.cid AND oi.ar_origin_reference_id = s.id )
						JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						oi.ar_origin_reference_id IN (' . implode( ', ', $arrintIds ) . ')
						AND oi.cid = ' . ( int ) $intCid . '
						AND oi.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND oi.deleted_by IS NULL ';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchRenewalSpecialsBySpecialGroupIdsByPropertyIdsByCid( $arrintSpecialGroupIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIsRenewalRecipients = true ) {

		if( false == valArr( $arrintSpecialGroupIds ) || false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSpecialRecipientCondition = ' s.special_recipient_id = ' . CSpecialRecipient::RENEWALS . '';
		if( false == $boolIsRenewalRecipients ) {
			$strSpecialRecipientCondition = ' s.special_recipient_id IN ( ' . sqlIntImplode( [ CSpecialRecipient::RENEWALS, CSpecialRecipient::PROSPECTS ] ) . ' ) ';
		}

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						ra.property_id
					FROM
						specials s
						JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						' . $strSpecialRecipientCondition . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						AND s.is_system = false
						AND s.special_group_id IN ( ' . implode( ',', $arrintSpecialGroupIds ) . ' )

						AND s.deleted_by IS NULL
						AND ( s.end_date IS NULL OR CURRENT_DATE < s.end_date )
					ORDER BY
						s.max_days_to_lease_start DESC, s.start_date DESC';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialsByPropertyIdByLeaseIntervalIdByCidWithCouponCode( $intPropertyId, $intLeaseIntervalId, $intCid, $objDatabase, $boolReturnCount = false ) {
		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						r.ar_trigger_id AS ar_trigger_id,
						r.rate_amount
					FROM specials s
						JOIN scheduled_charges sc ON ( sc.cid = s.cid AND sc.ar_origin_reference_id = s.id AND sc.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND sc.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN rates r ON ( r.cid = ra.cid AND ra.ar_origin_reference_id = ra.ar_origin_reference_id AND r.ar_origin_id = ra.ar_origin_id AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND sc.property_id = ' . ( int ) $intPropertyId . '
						AND sc.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
						AND sc.ar_origin_reference_id IS NOT NULL
						AND s.coupon_code IS NOT NULL
						AND s.deleted_by IS NULL';

		if( false == $boolReturnCount ) {
			return self::fetchSpecials( $strSql, $objDatabase );
		} else {
			$arrmixData = fetchData( $strSql, $objDatabase );
			return ( ( true == valArr( $arrmixData ) ) ? \Psi\Libraries\UtilFunctions\count( $arrmixData ) : 0 );
		}
	}

	public static function fetchCouponCodeSpecialsByLeaseIntervalIdByPropertyIdByUnitTypeIdByUnitSpaceIdByMoveInDateByCid( $intLeaseIntervalId, $intPropertyId, $intUnitTypeId, $intUnitSpaceId, $strMoveInDate, $intCid, $objDatabase, $boolReturnCount = false, $strCouponCode = NULL, $arrintCustomerIds, $intPropertyFloorplanId = NULL ) {

		if( true == is_null( $intPropertyId ) ) return NULL;

		$strCustomerIdsSql = '';
		if( true == valArr( $arrintCustomerIds ) ) {
			$strCustomerIdsSql = ' AND sc.customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';
		}

		$strSelectSql = ( true == $boolReturnCount ) ? ' COUNT( s.id ) ' : ' s.*, ra.hide_description ';
		$strCouponCodeSql = ( true == is_null( $strCouponCode ) ) ? ' AND s.coupon_code IS NOT NULL' : ' AND s.coupon_code = \'' . $strCouponCode . '\'';

		$strSql = ' SELECT
						' . $strSelectSql . '
					FROM
						specials s
						JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN scheduled_charges sc ON( sc.cid = s.cid
								AND sc.ar_origin_reference_id = s.id
								AND sc.ar_origin_id = ' . CArOrigin::SPECIAL . '
								AND sc.lease_interval_id = ' . ( int ) $intLeaseIntervalId . '
								' . $strCustomerIdsSql . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ( ( ra.ar_cascade_id = 1 AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyId . ' AND ra.unit_type_id IS NULL AND ra.unit_space_id IS NULL )
							OR ( ra.ar_cascade_id = 3 AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitTypeId . ' AND ra.unit_space_id IS NULL )
							OR ( ra.ar_cascade_id = 4 AND ra.ar_cascade_reference_id = ' . ( int ) $intUnitSpaceId . ' )
							OR ( ra.ar_cascade_id = 2 AND ra.ar_cascade_reference_id = ' . ( int ) $intPropertyFloorplanId . ' )
							)
						AND ( \'' . $strMoveInDate . '\' BETWEEN COALESCE( s.start_date, \'' . $strMoveInDate . '\' ) AND COALESCE( s.end_date, \'' . $strMoveInDate . '\' ) )
						AND s.coupon_code_required = true
						AND s.is_active = true
						' . $strCouponCodeSql . '
						AND s.deleted_by IS NULL
						AND s.is_active =  true
						AND ( ( sc.id IS NULL AND sc.deleted_by IS NULL ) OR ( sc.id IS NOT NULL AND sc.deleted_by IS NOT NULL ) )';
		if( false == $boolReturnCount ) {
			return self::fetchSpecials( $strSql, $objDatabase );
		} else {
			$arrmixData = fetchData( $strSql, $objDatabase );
			return ( ( true == valArr( $arrmixData ) ) ? $arrmixData[0][count] : 0 );
		}
	}

	public static function fetchSpecialsByIdsByLeaseStartDateByCid( $arrintSpecialIds, $strLeaseStartDate, $intCid, $objDatabase ) {

		if( false == valArr( array_filter( $arrintSpecialIds ) ) ) return;

		$strSql = 'SELECT
						s.*,
						ra.hide_description
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
					WHERE
						s.cid =' . ( int ) $intCid . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.id IN ( ' . sqlIntImplode( $arrintSpecialIds ) . ')
						AND COALESCE( s.start_date, \'1970-01-01\'::DATE ) <= \'' . $strLeaseStartDate . '\'
						AND COALESCE( s.end_date, \'2099-12-31\'::DATE ) >= \'' . $strLeaseStartDate . '\'
						AND s.is_active::BOOLEAN = TRUE
						AND s.deleted_by IS NULL';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialsBySpecialTypeIdsByPropertyIdByCid( $arrintSpecialTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSpecialTypeIds ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql = '
					SELECT
					DISTINCT( s.id ),
						s.*,
						ra.hide_description
					FROM
						specials s
						LEFT JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN rates r ON( r.cid = ra.cid AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id AND r.ar_origin_reference_id = ra.ar_origin_reference_id AND ra.property_id = r.property_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND s.deleted_by IS NULL
						AND s.special_type_id IN ( ' . sqlIntImplode( $arrintSpecialTypeIds ) . ' )
						AND s.special_recipient_id = ' . CSpecialRecipient::RENEWALS . '
						AND ra.ar_cascade_reference_id  = ' . ( int ) $intPropertyId . '
						AND ( s.end_date IS NULL or s.end_date > NOW() )';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchRenewalSpecialsByIdsByLeaseIdByLeaseIntervalIdByCid( $arrintGiftIncentiveIds, $intLeaseId, $intLeaseIntervalId, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintGiftIncentiveIds ) ) return [];

		$strSql = '
					SELECT
						DISTINCT s.id AS id,
						s.id AS gift_incentive_id,
						s.name,
						s.cid,
						s.special_type_id,
						s.quantity_remaining AS quantity_remaining,
						la.description,
						st.name AS special_type_name,
						q.leasing_tier_special_id As tier_special_id,
						q.id AS quote_id,
						la.id AS lease_association_id,
						la.lease_id,
						lt.id AS lease_term_id,
						sc.id AS scheduled_charge_id,
						sc.charge_amount AS rate_amount,
						sc.ar_trigger_id,
						la.ar_origin_id,
						sc.ar_code_id,
						sc.ar_code_type_id,
						la.ar_origin_reference_id,
						sc.ar_cascade_id,
						sc.ar_cascade_reference_id,
						sc.ar_formula_id,
						sc.ar_formula_reference_id,
						sc.rate_id,
						sc.id AS rate_log_id,
						sc.lease_start_window_id,
						CASE
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NULL
							THEN lt.name ||\' ( \'||to_char(lsw.start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\' )\'
							WHEN lt.lease_term_type_id = ' . CLeaseTermType::DATE_BASED . ' AND lsw.renewal_start_date IS NOT NULL
							THEN lt.name ||\' ( \'||to_char(lsw.renewal_start_date,\'MM/DD/YYYY\')||\' - \'||to_char(lsw.end_date,\'MM/DD/YYYY\')||\' )\'
							ELSE lt.name
						END AS lease_term_months,
						la.hide_description,
						lt.term_month,
						s.id 
					FROM 
						specials s
						JOIN special_types st ON (st.id = s.special_type_id)
						JOIN lease_associations la ON (la.cid = s.cid AND la.ar_origin_reference_id = s.id)
						JOIN applications a ON (a.cid = la.cid AND a.property_id = la.property_id AND a.lease_id = la.lease_id AND la.lease_interval_id = a.lease_interval_id)
						JOIN quotes q ON (q.cid = a.cid AND q.application_id = a.id AND la.quote_id = q.id)
						JOIN quote_lease_terms qlt ON (qlt.cid = q.cid AND qlt.quote_id = q.id AND la.quote_lease_term_id = qlt.id)
						JOIN lease_terms lt ON (qlt.cid = lt.cid AND qlt.lease_term_id = lt.id)
						LEFT JOIN lease_start_windows lsw ON(	lsw.cid = lt.cid
																AND lt.id = lsw.lease_term_id
																AND lsw.is_active IS TRUE
																AND lsw.deleted_by IS NULL
																AND lsw.deleted_on IS NULL
																AND lsw.property_id = a.property_id
															)
						LEFT JOIN scheduled_charges sc ON (sc.cid = la.cid AND sc.property_id = la.property_id AND sc.lease_id = la.lease_id AND sc.lease_interval_id = la.lease_interval_id AND
						  sc.ar_origin_id = la.ar_origin_id AND sc.ar_origin_reference_id = la.ar_origin_reference_id AND sc.lease_association_id = la.id AND sc.quote_id = q.id AND sc.lease_term_id =
						  qlt.lease_term_id)
						LEFT JOIN rate_associations ra ON (ra.cid = s.cid AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND ra.ar_cascade_reference_id = la.property_id AND ra.ar_origin_reference_id = s.id)
					WHERE
						s.cid = ' . ( int ) $intCid . ' AND
						s.id IN ( ' . sqlIntImplode( $arrintGiftIncentiveIds ) . ' ) AND
						la.lease_id = ' . ( int ) $intLeaseId . ' AND
						la.lease_interval_id = ' . ( int ) $intLeaseIntervalId . ' AND
						la.deleted_by IS NULL AND
						sc.deleted_by IS NULL AND
						s.special_type_id IN ( ' . CSpecialType::STANDARD . ', ' . CSpecialType::GIFT_INCENTIVE . ' )
					ORDER BY
						tier_special_id,
						' . $objDatabase->getCollateSort( 's.name' ) . ',
						lt.term_month,
						s.special_type_id';

		$arrmixGiftIncentives = fetchData( $strSql, $objDatabase );

		return $arrmixGiftIncentives;
	}

	public static function fetchIntegratedSpecialsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolIncludeDeletedSpecials = true ) {

		$strDeletedCondition = ( false == $boolIncludeDeletedSpecials ) ? ' AND deleted_on IS NULL': '';

		$strSql = 'SELECT
						DISTINCT ON( s.cid, s.id, ra.property_id )
						s.*,
						ra.property_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
					WHERE
						s.cid =' . ( int ) $intCid . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						' . $strDeletedCondition . '
						AND s.remote_primary_key IS NOT NULL';

		return parent::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchSpecialByRemotePrimaryKeyByCid( $strRemotePrimaryKey, $intCid, $objDatabase ) {
		$strSql = ' SELECT
						*
					FROM
						specials s
					WHERE
						lower( s.remote_primary_key ) = \'' . \Psi\CStringService::singleton()->strtolower( addslashes( trim( $strRemotePrimaryKey ) ) ) . '\'
						AND s.cid = ' . ( int ) $intCid;

		return parent::fetchSpecial( $strSql, $objDatabase );
	}

	public static function fetchCurrentPublishedProspectSpecialsByPropertyIdsByPropertyFloorplanIdsByCid( $arrintPropertyIds, $arrintPropertyFloorplanIds, $intCid, $objDatabase, $strMoveInDate = NULL ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintPropertyFloorplanIds ) ) {
			return NULL;
		}

 		if( false == is_null( $strMoveInDate ) ) {
			$strMoveInDate = '\'' . standardizeMoveInDate( $strMoveInDate ) . '\'';
		} else {
			$strMoveInDate = '\'' . date( 'm/d/Y' ) . '\'';
		}

		$strSql = 'SELECT
 						s.*,
 						ra.property_floorplan_id,
						ra.hide_description,
						ra.property_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
 					 WHERE
						ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . '
						AND ra.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ra.property_floorplan_id IN ( ' . implode( ',', $arrintPropertyFloorplanIds ) . ' )
						AND ra.cid = ' . ( int ) $intCid . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.deleted_on IS NULL
						AND s.is_active::BOOLEAN = true
						AND s.show_on_website::BOOLEAN = true
						AND ( ( ' . $strMoveInDate . ' ::Date >= s.start_date AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( ' . $strMoveInDate . ' ::Date >= s.start_date AND s.end_date IS NULL ) OR
									( s.start_date IS NULL AND ' . $strMoveInDate . ' ::Date <= s.end_date ) OR
									( s.start_date IS NULL AND s.end_date IS NULL )
									)';

 		return self::fetchSpecials( $strSql, $objDatabase, false );
	}

	public static function fetchCustomSpecialLogsBySpecialGroupIdByCid( $intSpecialGroupId, $intCid, $objDatabase ) {
		if( false == valId( $intSpecialGroupId ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						(
						 SELECT
							DATE ( sl.created_on ) as created_date,
							sl.created_by as user_id,
							COALESCE( initcap( ce.name_first || \' \' || ce.name_last ), cu.username ) as user_name,
							sl.*,
							rank ( ) OVER ( PARTITION BY DATE ( sl.created_on ),sl.created_by
						ORDER BY
							sl.created_on DESC, sl.id DESC ) AS rank
						FROM
							special_logs sl
							LEFT JOIN company_users cu ON ( cu.cid = sl.cid AND cu.id = sl.created_by )
							LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
						WHERE
							sl.cid = ' . ( int ) $intCid . '
							AND sl.special_group_id = ' . $intSpecialGroupId . '
						) AS sub
					ORDER BY
					sub.created_on DESC,sub.created_by';

		$arrmixSpecialLogs = fetchData( $strSql, $objDatabase );
		return $arrmixSpecialLogs;
	}

	public static function fetchPublishedWebsiteSpecialsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $objFloorplanAvailabilityFilter = NULL ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strWhere = NULL;
		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ $intCid ] );
		$objRateLogsFilter->setPropertyGroupIds( [ $intPropertyId ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		if( true == valObj( $objFloorplanAvailabilityFilter, 'CFloorplanAvailabilityFilter' ) ) {
			$strMoveInStartDate		= '\'' . standardizeMoveInDate( $objFloorplanAvailabilityFilter->getMoveInStartDate() ) . '\'';
			$strMoveInEndDate		= '\'' . standardizeMoveInDate( $objFloorplanAvailabilityFilter->getMoveInEndDate() ) . '\'';

			$strWhere = ' AND ( ( ' . $strMoveInStartDate . ' >= s.start_date AND ' . $strMoveInEndDate . ' <= s.end_date ) 
							OR ( s.start_date BETWEEN ' . $strMoveInStartDate . ' AND ' . $strMoveInEndDate . ')
							OR (  s.end_date BETWEEN ' . $strMoveInStartDate . ' AND ' . $strMoveInEndDate . ') 
							OR ( ' . $strMoveInStartDate . ' >= s.start_date AND s.end_date IS NULL ) 
							OR ( ' . $strMoveInEndDate . ' <= s.end_date AND s.start_date IS NULL )
							OR ( s.start_date IS NULL AND ' . $strMoveInEndDate . ' <= s.end_date ) 
							OR ( s.start_date IS NULL AND s.end_date IS NULL ) )';
		} else {
			$strWhere = ' AND ( ( \'' . date( 'Y-m-d' ) . '\' >= s.start_date AND \'' . date( 'Y-m-d' ) . '\' <= s.end_date )
							OR ( \'' . date( 'Y-m-d' ) . '\' >= s.start_date AND s.end_date IS NULL )
							OR ( s.start_date IS NULL AND \'' . date( 'Y-m-d' ) . '\' <= s.end_date )
							OR ( s.start_date IS NULL AND s.end_date IS NULL ) )';
		}

		$strSql = 'SELECT
						s.*,
						abs( pr.rate_amount ) AS rate_amount,
						pr.ar_code_id AS ar_code_id
					FROM
						specials s
						JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' )
						LEFT JOIN prospect_rates pr ON ( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND s.special_recipient_id = ' . CSpecialRecipient::PROSPECTS . '
						AND s.is_active::BOOLEAN = true
						AND s.show_on_website::BOOLEAN = true
						AND s.deleted_on IS NULL 
						' . $strWhere . '
						AND s.special_type_id <> ' . CSpecialType::INSTALLMENT_PLAN . '
					ORDER BY
						ra.order_num';

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchProspectSpecialsByStartDateByEndDateByPropertyIdByCid( $strStartDate, $strEndDate, $intPropertyId, $intCid, $objDatabase, $intSpecialGroupId = NULL, $intArCascadeId = NULL, $arrintUnitTypeIds = [], $arrintLeaseStartWindowIds = [] ) {

		$strJoinCondition = $strCondition = '';
		if( CArCascade::UNIT_TYPE == $intArCascadeId ) {
			$strJoinCondition .= ' JOIN unit_types ut ON ( ut.cid = ra.cid AND ut.id = ra.ar_cascade_reference_id )';
			if( true == valArr( $arrintUnitTypeIds ) ) {
				$strCondition .= ' AND ra.ar_cascade_reference_id = ANY( ARRAY[' . implode( ',', $arrintUnitTypeIds ) . ']::INTEGER[] )';
			}
		}

		if( true === valArr( $arrintLeaseStartWindowIds ) ) {
			$strCondition .= ' AND sgl.lease_start_window_id IN ( ' . sqlIntImplode( $arrintLeaseStartWindowIds ) . ' )';
		}

		$strSql = 'SELECT 
					* 
					FROM 
						specials s 
					JOIN 
                        special_groups sg ON ( s.special_group_id = sg.id AND s.cid = sg.cid )
                    JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . ' )
						' . $strJoinCondition . '
					LEFT JOIN special_group_lease_start_windows sgl ON ( sgl.cid = sg.cid AND sg.id = sgl.special_group_id AND sgl.deleted_on IS NULL  ) 
					WHERE 
						sg.property_id = ' . ( int ) $intPropertyId . '
						AND s.cid = ' . ( int ) $intCid . '
						AND sg.special_recipient_id  = ' . CSpecialRecipient::PROSPECTS . '
						AND ( s.start_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						OR s.end_date BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\' 
						OR  \'' . $strStartDate . '\' BETWEEN s.start_date AND s.end_date 
						OR  \'' . $strEndDate . '\' BETWEEN s.start_date AND s.end_date   
						)
						AND s.deleted_on IS NULL
						AND sg.deleted_by IS NULL
						' . ( true == valId( $intSpecialGroupId ) ? ' AND s.special_group_id <> ' . ( int ) $intSpecialGroupId : '' )
						. $strCondition;

		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public  static function fetchSimpleSpecialsByPropetyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '	SELECT
						s.*,
						ra.ar_cascade_id,
						sr.name AS special_recipient_name,
						st.name AS special_type_name,
						ra.is_published AS rate_association_is_published,
						ra.hide_description,
						ra.ar_cascade_reference_id,
						CASE
							WHEN ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' THEN pf.floorplan_name
							WHEN ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' THEN ut.name
							WHEN ra.ar_cascade_id = ' . CArCascade::SPACE . ' THEN us.unit_number_cache
						END AS ar_cascade_reference_name,
						us.building_name,
						us.property_unit_id,
						us.property_building_id,
						ra.ar_origin_id
					FROM
						specials s
						JOIN rate_associations ra ON ( s.cid = ra.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
						JOIN special_recipients sr ON ( s.special_recipient_id = sr.id )
						JOIN special_types st ON ( st.id = s.special_type_id )
						LEFT JOIN property_floorplans pf ON ( pf.cid = ra.cid AND ra.ar_cascade_id = ' . CArCascade::FLOOR_PLAN . ' AND pf.id = ra.ar_cascade_reference_id AND pf.deleted_on IS NULL )
						LEFT JOIN unit_types ut ON ( ut.cid = ra.cid AND ra.ar_cascade_id = ' . CArCascade::UNIT_TYPE . ' AND ut.id = ra.ar_cascade_reference_id )
						LEFT JOIN unit_spaces us ON ( us.cid = ra.cid AND ra.ar_cascade_id = ' . CArCascade::SPACE . ' AND us.id = ra.ar_cascade_reference_id AND us.deleted_on IS NULL )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND s.special_type_id NOT IN ( ' . CSpecialType::INSTALLMENT_PLAN . ' )
						AND s.deleted_on IS NULL
						AND s.is_active = TRUE
						AND s.special_group_id IS NULL
					ORDER BY
						ra.ar_cascade_id,
						s.created_on';
		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSpecialBySpecialTypeIdByCid( $intSpecialTypeId, $intCid, $objDatabase ) {
		return self::fetchSpecial( sprintf( 'SELECT * FROM specials WHERE special_type_id = %d AND cid = %d LIMIT 1', ( int ) $intSpecialTypeId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchRenewalSpecialsByPropertyIdByUnitTypeIdByUnitSpaceIdByCid( $intPropertyId, $intUnitTypeId, $intUniSpaceId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intUnitTypeId ) || false == valId( $intUniSpaceId ) )
			return NULL;

		$strSql = 'SELECT DISTINCT ON (s.*) s.*,
						ra.hide_description,
						r.ar_trigger_id AS ar_trigger_id,
						r.rate_amount
					FROM
						specials s
						LEFT JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN rates r ON( r.cid = ra.cid AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id AND r.ar_origin_reference_id = ra.ar_origin_reference_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND s.special_type_id IN ( ' . CSpecialType::GIFT_INCENTIVE . ', ' . CSpecialType::STANDARD . ')
						AND s.deleted_on IS NULL
						AND s.is_active::BOOLEAN = true
						AND s.special_recipient_id = ' . CSpecialRecipient::RENEWALS . '
						AND ra.ar_cascade_reference_id  IN ( ' . ( int ) $intPropertyId . ',' . ( int ) $intUnitTypeId . ',' . ( int ) $intUniSpaceId . ' )
						AND
						CASE 
							WHEN s.special_type_id = 1 THEN ( r.lease_term_months = ' . CLeaseTerm::LEASE_TERM_STANDARD . ' AND r.is_allowed::BOOLEAN = TRUE  AND r.rate_amount != 0 AND r.rate_amount IS NOT NULL )
					        ELSE ( s.gift_value IS NOT NULL AND s.gift_value != 0 ) 
					    END
						AND ( s.end_date IS NULL or s.end_date > NOW() )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSpecialBySpecialTypeIdByPropertyIdByCid( $intSpecialTypeId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intSpecialTypeId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					  s.id
					FROM
					  specials s
					  JOIN rate_associations ra ON ( s.cid = ra.cid AND ra.ar_cascade_id = ' . CArCascade::PROPERTY . ' AND ra.ar_cascade_reference_id = ' . $intPropertyId . ' AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . ' AND ra.ar_origin_reference_id = s.id )
					WHERE
					  s.special_type_id = ' . $intSpecialTypeId . ' AND
					  s.cid = ' . ( int ) $intCid . ' AND
					  s.is_system IS TRUE AND
					  s.is_active IS TRUE AND
					  s.is_default IS TRUE AND
					  s.show_on_website IS FALSE';

		return self::fetchSpecial( $strSql, $objDatabase );

	}

	public static function fetchPricingStudentSpecialsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIsReturnSqlOnly = false ) {

		$strSql = sprintf( 'SELECT
										s.name as special_name,
										ra.cid,
										ra.property_id,
										ra.unit_type_id,
										ra.ar_cascade_id,
										ra.ar_cascade_reference_id
									FROM
										specials s
										JOIN rate_associations ra ON ( s.cid = ra.cid AND ra.ar_origin_reference_id = s.id AND ra.ar_origin_id = %d )
										JOIN special_recipients sr ON ( s.special_recipient_id = sr.id )
										JOIN special_types st ON ( st.id = s.special_type_id )
										LEFT JOIN property_floorplans pf ON ( pf.cid = ra.cid AND ra.ar_cascade_id = %d AND pf.id = ra.ar_cascade_reference_id AND pf.deleted_on IS NULL )
										LEFT JOIN unit_types ut ON ( ut.cid = ra.cid AND ra.ar_cascade_id = %d )
									WHERE
										ra.cid = %d
										AND ra.property_id = %d
										AND s.special_type_id NOT IN ( %d )
										AND s.deleted_on IS NULL
										AND s.is_active = TRUE
										AND s.special_group_id IS NULL
										AND (
											( NOW() >= s.start_date AND NOW() <= s.end_date )OR
											( NOW() >= s.start_date AND s.end_date IS NULL ) OR
											( s.start_date IS NULL AND NOW() <= s.end_date ) OR
											( s.start_date IS NULL AND s.end_date IS NULL )
										)
									GROUP BY 
										s.name,
										ra.cid,
										ra.property_id,
										ra.unit_type_id,
										ra.ar_cascade_id,
ra.ar_cascade_reference_id',  CArOrigin::SPECIAL, CArCascade::FLOOR_PLAN, CArCascade::UNIT_TYPE, $intCid, $intPropertyId, CSpecialType::INSTALLMENT_PLAN );

		if( true == $boolIsReturnSqlOnly ) {
			return $strSql;
		}

		return fetchData( $strSql, $objDatabase );

	}

	// This function is having multiple associations, so we are keeping keyed array as false

	public static function fetchSpecialsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase, $boolCheckIsActive = false ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSubSql = ( true == $boolCheckIsActive ) ? ' AND s.is_active = true' : NULL;

		$strSql = 'SELECT
						s.*,
						ra.property_id,
						ra.unit_type_id,
						ra.unit_space_id,
						ra.property_floorplan_id,
						ra.ar_cascade_id,
						ra.ar_cascade_reference_id,
						r.ar_code_id,
						r.ar_code_type_id,
						r.ar_trigger_id,
						r.rate_amount,
						ass.application_stage_id,
						ass.application_status_id
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN application_stage_statuses ass ON ( ass.id = s.application_stage_status_id )
						LEFT JOIN rates r ON ( r.cid = ra.cid AND r.ar_origin_reference_id = ra.ar_origin_reference_id AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id )
					WHERE
						ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id   IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND s.deleted_by IS NULL' . $strSubSql;

		return self::fetchSpecials( $strSql, $objDatabase, false );
	}

	public static function fetchPublishedSpecialsByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objDatabase, $arrintArCascadeIds, $arrintSpecialRecipientIds, $arrintExcludeSpecialTypeIds = NULL ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return [];

		$objRateLogsFilter = new CRateLogsFilter();
		$objRateLogsFilter->setCids( [ implode( $arrintCids ) ] );
		$objRateLogsFilter->setPropertyGroupIds( [ implode( $arrintPropertyIds ) ] );
		$objRateLogsFilter->setArOriginIds( [ CArOrigin::SPECIAL ] );

		\Psi\Eos\Entrata\CRateLogs::createService()->loadRates( $objRateLogsFilter, $objDatabase );

		$strWhereClause = '';

		if( valArr( $arrintArCascadeIds ) ) {
			$strWhereClause .= ' AND ra.ar_cascade_id IN ( ' . implode( ',', $arrintArCascadeIds ) . ' )';
		}

		if( valArr( $arrintSpecialRecipientIds ) ) {
			$strWhereClause .= ' AND s.special_recipient_id IN ( ' . implode( ',', $arrintSpecialRecipientIds ) . ' )';
		}

		if( true == valArr( $arrintExcludeSpecialTypeIds ) ) {
			$strWhereClause .= ' AND s.special_type_id NOT IN ( ' . implode( ', ', $arrintExcludeSpecialTypeIds ) . ')';
		}

		$strSql = 'SELECT
						s.*,
						ra.hide_description,
						pr.ar_trigger_id AS ar_trigger_id,
						pr.rate_amount,
						pr.rate_increase_increment AS percentage
					FROM
						specials s
						JOIN rate_associations ra ON ( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN prospect_rates pr ON( s.id = pr.ar_origin_reference_id AND s.cid = pr.cid AND ra.property_id = pr.property_id	AND pr.ar_origin_id = ' . CArOrigin::SPECIAL . ' )
					WHERE
						ra.cid IN ( ' . implode( $arrintCids ) . ' )
						AND ra.property_id IN ( ' . implode( $arrintPropertyIds ) . ' )
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND s.deleted_on IS NULL
						AND ( s.end_date IS NULL or s.end_date > NOW() )' . $strWhereClause;
		unset( $strWhereClause );
		return self::fetchSpecials( $strSql, $objDatabase );
	}

	public static function fetchResidentSpecialsByPropertyIdByCidBySpecialTypeIdByArCascadeIdByArCascadeReferenceId( $intPropertyId, $intCid, $intSpecialTypeId, $intArCascadeId, $intArCascadeReferenceId, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						ra.ar_cascade_id,
						ra.ar_cascade_reference_id,
						r.ar_code_id,
						r.ar_code_type_id,
						r.ar_trigger_id,
						r.rate_amount
					FROM
						specials s
						JOIN rate_associations ra ON( ra.cid = s.cid AND ra.ar_origin_reference_id = s.id )
						LEFT JOIN rates r ON ( r.cid = ra.cid AND r.ar_origin_reference_id = ra.ar_origin_reference_id AND r.ar_cascade_id = ra.ar_cascade_id AND r.ar_cascade_reference_id = ra.ar_cascade_reference_id )
					WHERE
						ra.cid = ' . ( int ) $intCid . '
						AND ra.property_id = ' . ( int ) $intPropertyId . '
						AND ra.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND ra.ar_cascade_id = ' . ( int ) $intArCascadeId . '
						AND ra.ar_cascade_reference_id = ' . ( int ) $intArCascadeReferenceId . '
						AND s.special_recipient_id = ' . CSpecialRecipient::RESIDENTS . '
						AND s.special_type_id = ' . ( int ) $intSpecialTypeId . '
						AND ( s.start_date IS NULL OR s.start_date < NOW ( ) )
						AND ( s.end_date IS NULL OR s.end_date > NOW ( ) )
						AND ( r.effective_datetime IS NULL OR r.effective_datetime < NOW () )
						AND ( r.deactivation_date IS NULL OR r.deactivation_date > NOW() )
						AND ( s.quantity_remaining IS NULL OR s.quantity_remaining > 0 )
						AND s.is_active = true
						AND s.deleted_by IS NULL';

		return self::fetchSpecials( $strSql, $objDatabase, false );
	}

	public static function fetchResidentSpecialsByPropertyIdByCidByLeaseIdByLeaseIntervalId( $intPropertyId, $intCid, $intLeaseId, $intLeaseIntervalId, $objDatabase ) {

		$strSql = 'SELECT
						s.*,
						la.ar_cascade_id,
						la.ar_cascade_reference_id,
						sc.ar_code_id,
						sc.ar_code_type_id,
						sc.ar_trigger_id,
						sc.charge_amount AS rate_amount
					FROM
						specials s
						JOIN lease_associations la ON( la.cid = s.cid AND la.ar_origin_reference_id = s.id )
						LEFT JOIN scheduled_charges sc ON ( sc.cid = la.cid AND sc.ar_origin_reference_id = la.ar_origin_reference_id AND sc.ar_cascade_id = la.ar_cascade_id AND sc.ar_cascade_reference_id = la.ar_cascade_reference_id )
					WHERE
						la.cid = ' . ( int ) $intCid . '
						AND la.property_id = ' . ( int ) $intPropertyId . '
						AND la.ar_origin_id = ' . CArOrigin::SPECIAL . '
						AND la.lease_id = ' . ( int ) $intLeaseId . '
						AND la.lease_interval_id = ' . ( int ) $intLeaseIntervalId;
		return self::fetchSpecials( $strSql, $objDatabase, false );
	}

}
?>
