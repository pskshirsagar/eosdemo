<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CFileExportStatuses
 * Do not add any new functions to this class.
 */

class CFileExportStatuses extends CBaseFileExportStatuses {

    public static function fetchFileExportStatuses( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CFileExportStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchFileExportStatus( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CFileExportStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>