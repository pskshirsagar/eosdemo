<?php

class CDefaultPropertyPreference extends CBaseDefaultPropertyPreference {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_intId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valKey() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_strKey ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', '' ) );
        // }

        return $boolIsValid;
    }

    public function valValue() {
        $boolIsValid = true;

       // Validation example

        // if( false == isset( $this->m_strValue ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>