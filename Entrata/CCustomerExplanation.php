<?php

class CCustomerExplanation extends CBaseCustomerExplanation {

	protected $m_arrstrNoAppointmentReasons;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getNoAppointmentReasons() {
		if( true == isset( $this->m_arrstrNoAppointmentReasons ) )
			return $this->m_arrstrNoAppointmentReasons;

		$this->m_arrstrNoAppointmentReasons	= [
			__( 'Unable to commit to a specific time' ),
			__( 'Not enough interest in the property' ),
			__( 'Calling from outside the area' ),
			__( 'Only available to tour outside of office hours' ),
			__( 'Lack of available appointment times during office hours' ),
			__( 'Willing to start application without a tour' ),
			__( 'Soft Close' ),
			__( 'No phone number given' ),
			__( 'The onsite team already contacted lead' )
		];

		return $this->m_arrstrNoAppointmentReasons;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerDataTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExplanation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>