<?php

class CCompanyNumber extends CBaseCompanyNumber {

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intId ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intCid ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCustomerNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intCustomerNumber ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_number', '' ) );
        // }

        return $boolIsValid;
    }

    public function valLeaseNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intLeaseNumber ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'lease_number', '' ));
        // }

        return $boolIsValid;
    }

    public function valPropertyNumber() {
        $boolIsValid = true;

        /**
         * Validation example
         */

        // if( false == isset( $this->m_intPropertyNumber ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_number', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>