<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CScreeningNotifications
 * Do not add any new functions to this class.
 */

class CScreeningNotifications extends CBaseScreeningNotifications {

	public static function fetchScreeningNotificationsByScreeningNotificationTypeIdByPropertyIdByCid( $intScreeningNotificationTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT	
						snc.customer_type_id,
						sn.cid,
						sn.property_id,
						sn.screening_notification_type_id,
						sn.quick_response_id
					FROM
						screening_notifications sn
						JOIN screening_notification_customers snc ON snc.screening_notification_id = sn.id AND snc.cid = sn.cid
					WHERE
						sn.is_active = TRUE
						AND sn.screening_notification_type_id = ' . ( int ) $intScreeningNotificationTypeId . '
						AND sn.property_id = ' . ( int ) $intPropertyId . '
						AND sn.cid = ' . ( int ) $intCid;

		return parent::fetchScreeningNotifications( $strSql, $objDatabase );
	}

	public static function fetchScreeningNotificationsCountByScreeningEventTypeIdByPropertyIdByCid( $intScreeningEventTypeId, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						count( sn.id )
					FROM
						screening_notifications sn
						JOIN screening_notification_types snt ON snt.id = sn.screening_notification_type_id AND snt.screening_event_type_id = ' . ( int ) $intScreeningEventTypeId . '
					WHERE					
						sn.property_id = ' . ( int ) $intPropertyId . '
						AND sn.cid = ' . ( int ) $intCid;

		$arrmixResult = executeSql( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResult ) ) ? $arrmixResult['data'][0]['count'] : 0;
	}

	public static function fetchScreeningNotificationsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						sn.cid,
						sn.id,
						sn.property_id,
						sn.is_active,
						sn.screening_notification_type_id,
						sn.quick_response_id
					FROM
						screening_notifications sn
					WHERE
						is_active = TRUE 
						AND sn.property_id = ' . ( int ) $intPropertyId . '
						AND sn.cid = ' . ( int ) $intCid . '
						order by sn.id';

		return parent::fetchScreeningNotifications( $strSql, $objDatabase );
	}

	public static function archiveScreeningNotificationSettingsByCidByPropertyIdByIds( $intCid, $intPropertyId,$intUserId, $arrintNotificationIds, $objDatabase ) {
		if( false == valArr( $arrintNotificationIds ) ) return;

		$strSql = 'UPDATE
				       screening_notifications
				   SET
				       is_active = false,
				       updated_by = ' . ( int ) $intUserId . ',
				       updated_on = NOW ()
				   WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND id IN (' . implode( ',', $arrintNotificationIds ) . ')';

		return executeSql( $strSql, $objDatabase );
	}

	public static function archiveScreeningNotificationsByQuickResponseIdByCid( $intCurrentUserId, $intQuickResponseId, $intCid, $objDatabase, $arrintPropertyIds = [] ) {
		$strWhere = '';

		if( true == valArr( $arrintPropertyIds ) ) {
			$strWhere = ' AND property_id NOT IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}

		$strSql = 'UPDATE
						screening_notifications
					SET
						is_active = false,
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE
						cid = ' . ( int ) $intCid . '
						AND quick_response_id = ' . ( int ) $intQuickResponseId;

		$strSql .= $strWhere;

		executeSql( $strSql, $objDatabase );
	}

	public static function archiveScreeningNotificationsByQuickResponseIdsByCidByPropertyId( $intCurrentUserId, $arrintQuickResponseIds, $intCid, $intPropertyId, $objDatabase ) {

		$strSql = 'UPDATE
						screening_notifications
					SET
						is_active = false,
						updated_by = ' . ( int ) $intCurrentUserId . ',
						updated_on = NOW()
					WHERE
						cid = ' . ( int ) $intCid . '
						AND quick_response_id IN (' . implode( ',', $arrintQuickResponseIds ) . ')
						AND property_id =' . ( int ) $intPropertyId;

		executeSql( $strSql, $objDatabase );
	}

	public static function fetchActiveScreeningNotificationByCidByPropertyIdByScreeningNotificationTypeId( $intCid, $intPropertyId, $intScreeningNotificationTypeId,  $objDatabase ) {
        $strSql = 'SELECT
						*
					FROM
						screening_notifications
					WHERE
						  is_active = TRUE 
						  AND property_id = ' . ( int ) $intPropertyId . '
						  AND cid = ' . ( int ) $intCid . '
						  AND screening_notification_type_id = ' . ( int ) $intScreeningNotificationTypeId;

        return parent::fetchScreeningNotification( $strSql, $objDatabase );
    }

}
?>