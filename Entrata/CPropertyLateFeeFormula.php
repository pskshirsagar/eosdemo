<?php

class CPropertyLateFeeFormula extends CBasePropertyLateFeeFormula {

	public $m_strLateFeeFormulaName;
	protected $m_intPercentOfSalesArCodeId;

	/**
	 *Get Functions
	 */

	public function getPercentOfSalesArCodeId() {
		return $this->m_intPercentOfSalesArCodeId;
	}

	public function getLateFeeFormulaName() {
		return $this->m_strLateFeeFormulaName;
	}

	/**
	 * Set Functions
	 */

	public function setPercentOfSalesArCodeId( $intPercentOfSalesArCodeId ) {
		$this->m_intPercentOfSalesArCodeId = $intPercentOfSalesArCodeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['late_fee_formula_name'] ) && $boolDirectSet ) {
			$this->m_strLateFeeFormulaName = trim( $arrmixValues['late_fee_formula_name'] );
		} elseif( isset( $arrmixValues['late_fee_formula_name'] ) ) {
			$this->setLateFeeFormulaName( $arrmixValues['late_fee_formula_name'] );
		}
	}

	public function setLateFeeFormulaName( $strLateFeeFormulaName ) {
		$this->m_strLateFeeFormulaName = CStrings::strTrimDef( $strLateFeeFormulaName, 50, NULL, true );
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid	&= $this->valPropertyId();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid	&= $this->valPropertyId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function mapKeyValuePair( $objPropertyLateFeeFormula, $objClientDatabase ) {

		$strKeyValuePair = '';

		$objLateFeeFormula = \Psi\Eos\Entrata\CLateFeeFormulas::createService()->fetchLateFeeFormulaByIdByPropertyIdByCid( $objPropertyLateFeeFormula->getLateFeeFormulaId(), $this->m_intPropertyId, $this->m_intCid, $objClientDatabase );

		if( true == valObj( $objLateFeeFormula, 'CLateFeeFormula' ) ) {
			$strKeyValuePair = 'Is Default::' . $objLateFeeFormula->getName();
		}

		return $strKeyValuePair;
	}

	public function log( $objOldPropertyLateFeeFormula, $objNewPropertyLateFeeFormula, $intCompanyUserId, $objClientDatabase ) {

		$objOldPropertyLateFeeFormulaKeyValuePair = $this->mapKeyValuePair( $objOldPropertyLateFeeFormula, $objClientDatabase );
		$objNewPropertyLateFeeFormulaKeyValuePair = $this->mapKeyValuePair( $objNewPropertyLateFeeFormula, $objClientDatabase );

		$objTableLog = new CTableLog();

		return $objTableLog->insert( $intCompanyUserId, $objClientDatabase, 'property_late_fee_formulas', $objNewPropertyLateFeeFormula->getId(), 'UPDATE', $objNewPropertyLateFeeFormula->getCid(), $objOldPropertyLateFeeFormulaKeyValuePair, $objNewPropertyLateFeeFormulaKeyValuePair, 'Default Late Fee Formula is changed.' );

	}

	public function fetchLateFeeFormula( $objDatabase ) {
		return \Psi\Eos\Entrata\CLateFeeFormulas::createService()->fetchLateFeeFormulaByIdByCid( $this->getLateFeeFormulaId(), $this->getCid(), $objDatabase );
	}

}
?>