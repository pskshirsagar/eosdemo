<?php

class CLeaseType extends CBaseLeaseType {

	const STANDARD	= 1;
	const CORPORATE	= 2;
	const EMPLOYEE	= 3;
	const MILITARY	= 4;
	const COURTESY_OFFICER	= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) || 0 == strlen( trim( $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );
			return $boolIsValid;
		}

		if( false == is_null( $objDatabase ) ) {

			$objConfilictingLeaseType = CLeaseTypes::fetchConflictingNameLeaseTypeByCid( $this, $objDatabase );

			if( true == valObj( $objConfilictingLeaseType, 'CLeaseType' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Lease Type Name is already in use.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName( $objDatabase );
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>