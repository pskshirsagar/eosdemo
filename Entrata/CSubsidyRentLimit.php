<?php

class CSubsidyRentLimit extends CBaseSubsidyRentLimit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyRentLimitVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyAreaId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyContractTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBedroomCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRentLimit() {
		$boolIsValid = true;

		if( true == is_null( $this->getRentLimit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rent_limit', 'Home program rents are required.' ) );
		}

		if( $this->getRentLimit() === 0 ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rent_limit', 'Home program rents must be greater than zero.' ) );
		}

		if( 0 > $this->getRentLimit() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rent_limit', 'Home program rents must be positive.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRentLimit();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>