<?php

class CApCodeCategory extends CBaseApCodeCategory {

	/**
	 * Create Functions
	 */

	public function createApCode() {

		$objApCode = new CApCode();
		$objApCode->setCid( $this->getCid() );
		$objApCode->setApCodeTypeId( CApCodeType::CATALOG_ITEMS );

		return $objApCode;
	}

	/**
	 * Val Functions
	 */

	public function valName( $objClientDatabase, $arrobjApCodeCategories = NULL, $strInsert = NULL ) {
		$strCondtion = '';
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Category name is required.' ) ) );
		} elseif( false == preg_match( '/^[A-Za-z \d\:\-\.\_\@\$\!\(\)\[\}\^\&\]\{\/\*\#\%\=\+]+$/i', trim( $this->getName() ) ) || false == mb_check_encoding( $this->getName(), 'UTF-8' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Invalid Category name (It contains special symbol).' ) ) );
		} elseif( true == valArr( $arrobjApCodeCategories ) ) {

			foreach( $arrobjApCodeCategories as $objApCodeCategory ) {

				if( mb_strtolower( $this->getName() ) === mb_strtolower( $objApCodeCategory->getName() ) && $this->getId() <> $objApCodeCategory->getId() ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Category name '{%s, 0}' already exists.", [ $this->getName() ] ) ) );
				}
			}
		}

		if( true == $boolIsValid && false == valArr( $arrobjApCodeCategories ) ) {

			$strCondtion = ' AND ap_code_type_id = ' . $this->getApCodeTypeId();
			$strWhere = 'WHERE
							cid = ' . ( int ) $this->getCid() . '
							AND LOWER( name ) = LOWER( \'' . addslashes( $this->getName() ) . '\' )
							AND id  <> ' . ( int ) $this->getId() . '
							AND deleted_by IS NULL' . $strCondtion;

			if( 0 < \Psi\Eos\Entrata\CApCodeCategories::createService()->fetchApCodeCategoryCount( $strWhere, $objClientDatabase ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Category name '{%s, 0}' already exists.", [ $this->getName() ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $arrobjApCodeCategories = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase, NULL, VALIDATE_INSERT );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objDatabase, $arrobjApCodeCategories, VALIDATE_UPDATE );
				break;

			case 'validate_insert_update_job_group':
				$boolIsValid &= $this->valName( $objDatabase, $arrobjApCodeCategories, VALIDATE_UPDATE );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>