<?php

use Psi\Libraries\UtilHash\CHash;

define( 'ERROR_CODE_NAME_REQUIRE', 501 );
define( 'ERROR_CODE_INVALID_NAME', 502 );
define( 'ERROR_CODE_PHONE_NUMBER_REQUIRE', 503 );
define( 'ERROR_CODE_INVALID_PHONE_NUMBER', 504 );
define( 'ERROR_CODE_EMAIL_REQUIRE', 505 );
define( 'ERROR_CODE_INVALID_EMAIL', 506 );

class CApplicant extends CBaseApplicant {

	use TEosPostalAddresses;

	protected $m_objCustomer;
	protected $m_objCustomerIncome;
	protected $m_objProperty;
	protected $m_objCustomerAddress;

	protected $m_arrobjApplicationSteps;

	protected $m_arrmixCustomerPhoneNumbers;

	protected $m_intIsLead;
	protected $m_intHeightFeet;
	protected $m_intPropertyId;
	protected $m_intBirthMonths;
	protected $m_intHeightInches;
	protected $m_intRelationship;
	protected $m_intApplicationId;
	protected $m_intAssociationId;
	protected $m_intLeaseDocumentId;
	protected $m_intApplicationFormId;
	protected $m_intApplicationStepId;
	protected $m_intLeadSourceId;
	protected $m_intCurrentApplicationStepId;
	protected $m_intScreeningPackageId;
	protected $m_intTransmissionOrderId;
	protected $m_intAuthenticationLogId;
	protected $m_intCustomerTypeId;
	protected $m_intApplicationDocumentId;
	protected $m_intCurrentCustomerIncome;
	protected $m_intApplicantApplicationId;
	protected $m_intScreeningApplicantTypeId;
	protected $m_intCustomerRelationshipId;
	protected $m_intApplicantApplicationTransmissionId;
	protected $m_strSubsidyTracsVersionId;
	protected $m_intDefaultCustomerRelationshipId;
	protected $m_intOccupancyTypeId;
	protected $m_intLeaseId;
	protected $m_intLeaseIntervalId;

	protected $m_intCurrentCompanyResidenceTypeId;
	protected $m_intCurrentMoveOutReasonListItemId;
	protected $m_strCurrentCommunityName;
	protected $m_strCurrentReasonForLeaving;
	protected $m_strCurrentMoveInDate;
	protected $m_strCurrentMoveOutDate;
	protected $m_fltCurrentMonthlyPaymentAmount;
	protected $m_strCurrentPaymentRecipient;
	protected $m_strCurrentStreetLine1;
	protected $m_strCurrentStreetLine2;
	protected $m_strCurrentStreetLine3;
	protected $m_strCurrentCity;
	protected $m_strCurrentStateCode;
	protected $m_strCurrentPostalCode;
	protected $m_strCurrentCountryCode;
	protected $m_strCurrentAddressOwnerType;
	protected $m_boolCurrentIsVerified;
	protected $m_boolCurrentIsNonUsAddress;
	protected $m_strCurrentDependentLocality;
	protected $m_strCurrentSortingCode;
	protected $m_strWechatOpenId;

	protected $m_intPreviousCompanyResidenceTypeId;
	protected $m_intPreviousMoveOutReasonListItemId;
	protected $m_strPreviousCommunityName;
	protected $m_strPreviousReasonForLeaving;
	protected $m_strPreviousMoveInDate;
	protected $m_strPreviousMoveOutDate;
	protected $m_fltPreviousMonthlyPaymentAmount;
	protected $m_strPreviousPaymentRecipient;
	protected $m_strPreviousStreetLine1;
	protected $m_strPreviousStreetLine2;
	protected $m_strPreviousStreetLine3;
	protected $m_strPreviousCity;
	protected $m_strPreviousStateCode;
	protected $m_strPreviousPostalCode;
	protected $m_strPreviousCountryCode;
	protected $m_strPreviousAddressOwnerType;
	protected $m_boolPreviousIsVerified;
	protected $m_boolPreviousIsNonUsAddress;
	protected $m_strPreviousDependentLocality;
	protected $m_strPreviousSortingCode;

	protected $m_strRelation;
	protected $m_strPassword;
	protected $m_strCompletedOn;
	protected $m_strPropertyName;
	protected $m_strTypeOfOccupent;
	protected $m_strUsernameConfirm;
	protected $m_strPasswordConfirm;
	protected $m_strCustomerType;
	protected $m_strApplicantInstitutionName;
	protected $m_strApplicationStatus;
	protected $m_strApplicantApplicationDeletedOn;
	protected $m_strCustomerRelationshipName;
	protected $m_strMoveInDate;
	protected $m_strTaxNumberMasked;
	protected $m_strPreferredLocaleCode;
	protected $m_strPrimaryPhoneNumber;

	protected $m_boolIsDelete;
	protected $m_boolIsBlanked;
	protected $m_boolValFailed;
	protected $m_boolPrimaryIsResponsible;
	protected $m_boolIsMilitaryApplicaion;
	protected $m_boolLivingInThisApartment;
	protected $m_boolIsBlockMessageCenterSms;
	protected $m_boolIsBlockPropertyMarketingSms;
	protected $m_boolIsBlockMessageCenterEmail;
	protected $m_boolIsBlockContactPointsEmail;
	protected $m_boolIsBlockLeadCommunicationEmail;
	protected $m_boolIsBlockPropertyMarketingEmail;
	protected $m_boolSendConsumerReport;
	protected $m_boolSkipSyncWithCustomerContacts;
	protected $m_boolIsClientI18nEnabled;
	protected $m_boolIsApplicantDeduped = false;
	protected $m_boolIsOnlyAdditionalPhoneNumber = false;

	const DEFAULT_REQUIRED_APPLICANT_AGE					= 18;
	const TEXT_TO_REPLACE_FROM_DIGITAL_SIGNATURE_EXAMPLE 	= 'For e.g. John Smith';
	const IDENTIFICATION_NUMBER_MODE_MANUAL 				= 'manual';
	const IDENTIFICATION_NUMBER_MODE_SCANNED 				= 'scanned';

	public static $c_arrstrMilitaryEmailDomains 			= [ 'mil', 'gov' ];
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_intFelonyIsResolved              = '0';
		$this->m_intHasMiddleName                 = '1';
		$this->m_intIsAlien                       = NULL;
		$this->m_boolAllowDifferentialUpdate      = false;
		$this->m_intHasAltMiddleName              = 1;
		$this->m_intHasNameLastMatronymic         = 1;
		$this->m_boolSkipSyncWithCustomerContacts = false;
		$this->m_arrstrPostalAddressFields = [
			'default' => [
				'm_strCurrentStreetLine1' => 'addressLine1',
				'm_strCurrentStreetLine2' => 'addressLine2',
				'm_strCurrentStreetLine3' => 'addressLine3',
				'm_strCurrentCity' => 'locality',
				'm_strCurrentStateCode' => 'administrativeArea',
				'm_strCurrentPostalCode' => 'postalCode',
				'm_strCurrentCountryCode' => 'country'
			]
		];

		return;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getAuthenticationLogId() {
		return $this->m_intAuthenticationLogId;
	}

	public function setWechatOpenId( $strWeChatID ) {
		$this->m_strWechatOpenId = $strWeChatID;
	}

	public function getWechatOpenId() {
		return $this->m_strWechatOpenId;
	}

	public function getIsClientI18nEnabled() {
		return $this->m_boolIsClientI18nEnabled;
	}

	public function setIsClientI18nEnabled( $boolIsClientI18nEnabled ) {
		$this->m_boolIsClientI18nEnabled = $boolIsClientI18nEnabled;
	}

	public function getTaxNumber() {
		if( false == valStr( $this->m_strTaxNumberEncrypted ) ) {
			return NULL;
		}

		return $strTaxNumber = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strTaxNumberEncrypted, CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function getTaxNumberMasked() {
		if( 4 == \Psi\CStringService::singleton()->strlen( $this->getTaxNumber() ) ) {
			return $this->getTaxNumber();
		} else {
		   return CEncryption::maskTaxIdTypeIdText( $this->getTaxNumber(), $this->getTaxIdTypeId() );
		}
	}

	public function getApplicantTaxNumberMasked() {
		return $this->m_strTaxNumberMasked;
	}

	public function getHasTaxNumber() {
		return $this->getDetailsField( 'has_tax_number' );
	}

	public function getDlNumber() {
		if( false == valStr( $this->m_strDlNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strDlNumberEncrypted, CONFIG_SODIUM_KEY_DL_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_DL_NUMBER ] );
	}

	public function getIdentificationNumber( $boolIsMaskedIdentificationNumber = false ) {
		if( true == $boolIsMaskedIdentificationNumber ) {
			return CEncryption::maskText( $this->getIdentificationNumber(), -2 );
		}
		if( false == valStr( $this->m_strIdentificationValue ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strIdentificationValue, CONFIG_SODIUM_KEY_DL_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_DL_NUMBER ] );
	}

	public function getCustomerTypeId() {
		return $this->m_intCustomerTypeId;
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function getLivingInThisApartment() {
		return $this->m_boolLivingInThisApartment;
	}

	public function getTypeOfOccupent() {
		return $this->m_strTypeOfOccupent;
	}

	public function getApplicationFormId() {
		return $this->m_intApplicationFormId;
	}

	public function getApplicationDocumentId() {
		return $this->m_intApplicationDocumentId;
	}

	public function getLeaseFormId() {
		return $this->m_intLeaseFormId;
	}

	public function getLeaseDocumentId() {
		return $this->m_intLeaseDocumentId;
	}

	public function getCustomerType() {
		return $this->m_strCustomerType;
	}

	public function getApplicationStatus() {
		return $this->m_strApplicationStatus;
	}

	public function getUsernameConfirm() {
		return $this->m_strUsernameConfirm;
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function getPasswordConfirm() {
		return $this->m_strPasswordConfirm;
	}

	public function getApplicationStepId() {
		return $this->m_intApplicationStepId;
	}

	public function getCurrentApplicationStepId() {
		return $this->m_intCurrentApplicationStepId;
	}

	public function getValFailed() {
		return $this->m_boolValFailed;
	}

	public function getCustomer() {
		return $this->m_objCustomer;
	}

	public function getCustomerAddress() {
		return $this->m_objCustomerAddress;
	}

	public function getCustomerIncome() {
		return $this->m_objCustomerIncome;
	}

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	public function getIsBlanked() {
		return $this->m_boolIsBlanked;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getAssociationId() {
		return $this->m_intAssociationId;
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPrimaryIsResponsible() {
		return $this->m_boolPrimaryIsResponsible;
	}

	public function getSendConsumerReport() {
		return $this->m_boolSendConsumerReport;
	}

	public function getRelationship() {
		return $this->m_intRelationship;
	}

	public function getRelation() {
		return $this->m_strRelation;
	}

	public function getCustomerRelationshipId() {
		return $this->m_intCustomerRelationshipId;
	}

	public function getApplicantApplicationTransmissionId() {
		return $this->m_intApplicantApplicationTransmissionId;
	}

	public function getTransmissionOrderId() {
		return $this->m_intTransmissionOrderId;
	}

	public function getScreeningApplicantTypeId() {
		return $this->m_intScreeningApplicantTypeId;
	}

	public function getApplicantInstitutionName() {
		return $this->m_strApplicantInstitutionName;
	}

	public function getApplicantCurrentIncome() {
		return $this->m_intCurrentCustomerIncome;
	}

	public function getScreeningPackageId() {
		return $this->m_intScreeningPackageId;
	}

	public function getIsValidateRequired() {
		return $this->m_boolIsValidateRequired;
	}

	public function getSubsidyTracsVersionId() {
		return $this->m_strSubsidyTracsVersionId;
	}

	public function getDefaultCustomerRelationshipId() {
		return $this->m_intDefaultCustomerRelationshipId;
	}

	// This is not the efficient way to load relation[SG], this is temp.

	public function loadRelation( $objCustomerRelationshipChildOrSpouse ) {
		$this->m_strRelation = '';

		if( true == is_null( $this->getCustomerTypeId() ) ) {
			return;
		}

		switch( $this->getCustomerTypeId() ) {
			case CCustomerType::GUARANTOR:
				$this->m_strRelation = 'Gurantor';
				break;

			case CCustomerType::NOT_RESPONSIBLE:
				if( true == valObj( $objCustomerRelationshipChildOrSpouse, 'CCustomerRelationship' ) && $objCustomerRelationshipChildOrSpouse->getId() == $this->getCustomerRelationshipId() ) {
					$this->m_strRelation = 'Child';
				} else {
					$this->m_strRelation = 'Occupant';
				}
				break;

			case CCustomerType::RESPONSIBLE:
				if( true == valObj( $objCustomerRelationshipChildOrSpouse, 'CCustomerRelationship' ) && $objCustomerRelationshipChildOrSpouse->getId() == $this->getCustomerRelationshipId() ) {
					$this->m_strRelation = 'Spouse';
				} else {
					$this->m_strRelation = 'Co-App.';
				}

			case CCustomerType::NON_LEASING_OCCUPANT:
				$this->m_strRelation = 'Non-Leasing Co-app';
				break;

			default:
				// default case
				break;
		}

		return;
	}

	public function getGenderText() {
		$strGender = '';
		if( true == is_null( $this->getGender() ) ) {
			return $strGender;
		}

		switch( $this->getGender() ) {
			case 'M':
				$strGender = __( 'Male' );
				break;

			case 'F':
				$strGender = __( 'Female' );
				break;

			default:
				// deafult case
				break;
		}

		return $strGender;
	}

	public function getHeightFeet() {
		return $this->m_intHeightFeet;
	}

	public function getHeightInches() {
		return $this->m_intHeightInches;
	}

	public function getInitials() {
		$arrstrNameFirst          = \Psi\CStringService::singleton()->str_split( $this->getNameFirst() );
		$arrstrNameMiddle         = ( 0 < strlen( $this->getNameMiddle() ) ) ? \Psi\CStringService::singleton()->str_split( $this->getNameMiddle() ) : NULL;
		$arrstrNameLast           = ( 0 < strlen( $this->getNameLast() ) ) ? \Psi\CStringService::singleton()->str_split( $this->getNameLast() ) : NULL;
        $arrstrNameLastMatronymic = ( 0 < strlen( $this->getNameLastMatronymic() ) ) ? \Psi\CStringService::singleton()->str_split( $this->getNameLastMatronymic() ) : NULL;

		return $arrstrNameFirst[0] . ( ( false == is_null( $arrstrNameMiddle ) ) ? $arrstrNameMiddle[0] : '' ) . ( ( false == is_null( $arrstrNameLast ) ) ? $arrstrNameLast[0] : '' ) . ( ( false == is_null( $arrstrNameLastMatronymic ) ) ? $arrstrNameLastMatronymic[0] : '' );
	}

	public function getFirstNameAndLastInitial() {
		$arrstrNameLast = ( 0 < strlen( $this->getNameLast() ) ) ? $this->getNameLast() : NULL;

		return $this->getNameFirst() . ' ' . ( ( false == is_null( $arrstrNameLast ) ) ? $arrstrNameLast[0] : '' );
	}

	public function getFirstAndLastNameInitials() {
		$arrstrNameFirst = \Psi\CStringService::singleton()->str_split( $this->getNameFirst() );
		$arrstrNameLast  = ( 0 < strlen( $this->getNameLast() ) ) ? $this->getNameLast() : NULL;

		return $arrstrNameFirst[0] . ( ( false == is_null( $arrstrNameLast ) ) ? $arrstrNameLast[0] : '' );
	}

	public function getApplicationSteps() {
		return $this->m_arrobjApplicationSteps;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getIsBlockMessageCenterEmail() {
		return $this->m_boolIsBlockMessageCenterEmail;
	}

	public function getIsBlockContactPointsEmail() {
		return $this->m_boolIsBlockContactPointsEmail;
	}

	public function getIsBlockLeadCommunicationEmail() {
		return $this->m_boolIsBlockLeadCommunicationEmail;
	}

	public function getIsBlockPropertyMarketingEmail() {
		return $this->m_boolIsBlockPropertyMarketingEmail;
	}

	public function getIsBlockMessageCenterSms() {
		return $this->m_boolIsBlockMessageCenterSms;
	}

	public function getIsBlockPropertyMarketingSms() {
		return $this->m_boolIsBlockPropertyMarketingSms;
	}

	public function getApplicantApplicationDeletedOn() {
		return $this->m_strApplicantApplicationDeletedOn;
	}

	public function getIsLead() {
		return $this->m_intIsLead;
	}

	public function getBirthDate( $boolIsMaskedBirthDate = false ) {
		if( true == valStr( $this->m_strBirthDate ) && true == $boolIsMaskedBirthDate ) {
			return '**/**/****';
		}

		return $this->m_strBirthDate;
	}

	public function getBirthMonths() {
		return $this->m_intBirthMonths;
	}

	public function getCorporateLease() {
		return $this->m_boolIsCorporateLease;
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function getIsMilitaryApplicaion() {
		return $this->m_boolIsMilitaryApplicaion;
	}

	public function getOccupancyTypeId() {
		return $this->m_intOccupancyTypeId;
	}

	public function getCurrentCompanyResidenceTypeId() {
		return $this->m_intCurrentCompanyResidenceTypeId;
	}

	public function getCurrentMoveOutReasonListItemId() {
		return $this->m_intCurrentMoveOutReasonListItemId;
	}

	public function getCurrentCommunityName() {
		return $this->m_strCurrentCommunityName;
	}

	public function getCurrentReasonForLeaving() {
		return $this->m_strCurrentReasonForLeaving;
	}

	public function getCurrentMoveInDate() {
		return $this->m_strCurrentMoveInDate;
	}

	public function getCurrentMoveOutDate() {
		return $this->m_strCurrentMoveOutDate;
	}

	public function getCurrentMonthlyPaymentAmount() {
		return $this->m_fltCurrentMonthlyPaymentAmount;
	}

	public function getCurrentPaymentRecipient() {
		return $this->m_strCurrentPaymentRecipient;
	}

	public function getCurrentStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strCurrentStreetLine1' );
	}

	public function getCurrentStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strCurrentStreetLine2' );
	}

	public function getCurrentStreetLine3() {
		return $this->getPostalAddressField( 'addressLine3', 'default', 'm_strCurrentStreetLine3' );
	}

	public function getCurrentCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strCurrentCity' );
	}

	public function getCurrentStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strCurrentStateCode' );
	}

	public function getCurrentPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strCurrentPostalCode' );
	}

	public function getCurrentCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strCurrentCountryCode' );
	}

	public function getCurrentIsVerified() {
		return $this->m_boolCurrentIsVerified;
	}

	public function getCurrentAddressOwnerType() {
		return $this->m_strCurrentAddressOwnerType;
	}

	public function getCurrentIsNonUsAddress() {
		return $this->m_boolCurrentIsNonUsAddress;
	}

	public function getPreviousCompanyResidenceTypeId() {
		return $this->m_intPreviousCompanyResidenceTypeId;
	}

	public function getPreviousMoveOutReasonListItemId() {
		return $this->m_intPreviousMoveOutReasonListItemId;
	}

	public function getPreviousCommunityName() {
		return $this->m_strPreviousCommunityName;
	}

	public function getPreviousReasonForLeaving() {
		return $this->m_strPreviousReasonForLeaving;
	}

	public function getPreviousMoveInDate() {
		return $this->m_strPreviousMoveInDate;
	}

	public function getPreviousMoveOutDate() {
		return $this->m_strPreviousMoveOutDate;
	}

	public function getPreviousMonthlyPaymentAmount() {
		return $this->m_fltPreviousMonthlyPaymentAmount;
	}

	public function getPreviousPaymentRecipient() {
		return $this->m_strPreviousPaymentRecipient;
	}

	public function getPreviousStreetLine1() {
		return $this->m_strPreviousStreetLine1;
	}

	public function getPreviousStreetLine2() {
		return $this->m_strPreviousStreetLine2;
	}

	public function getPreviousStreetLine3() {
		return $this->m_strPreviousStreetLine3;
	}

	public function getPreviousCity() {
		return $this->m_strPreviousCity;
	}

	public function getPreviousStateCode() {
		return $this->m_strPreviousStateCode;
	}

	public function getPreviousPostalCode() {
		return $this->m_strPreviousPostalCode;
	}

	public function getPreviousCountryCode() {
		return $this->m_strPreviousCountryCode;
	}

	public function getPreviousIsVerified() {
		return $this->m_boolPreviousIsVerified;
	}

	public function getPreviousAddressOwnerType() {
		return $this->m_strPreviousAddressOwnerType;
	}

	public function getPreviousIsNonUsAddress() {
		return $this->m_boolPreviousIsNonUsAddress;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getLeaseIntervalId() {
		return $this->m_intLeaseIntervalId;
	}

	public function getPreferredLocaleCode() {
		return $this->m_strPreferredLocaleCode;
	}

	public function getPreviousDependentLocality() {
		return $this->m_strPreviousDependentLocality;
	}

	public function getCurrentDependentLocality() {
		return $this->m_strCurrentDependentLocality;
	}

	public function getPreviousSortingCode() {
		return $this->m_strPreviousSortingCode;
	}

	public function getCurrentSortingCode() {
		return $this->m_strCurrentSortingCode;
	}

	public function getIsApplicantDeduped() {
		return $this->m_boolIsApplicantDeduped;
	}

	public function getIsOnlyAdditionalPhoneNumber() {
		return $this->m_boolIsOnlyAdditionalPhoneNumber;
	}

	public function getCustomerPhoneNumbers() {
		return $this->m_arrmixCustomerPhoneNumbers;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setNameFirst( $strNameFirst ) {
		parent::setNameFirst( ucfirst( strip_tags( $strNameFirst ) ) );
	}

	public function setNameMiddle( $strNameMiddle ) {
		parent::setNameMiddle( ucfirst( strip_tags( $strNameMiddle ) ) );
	}

	public function setNameLast( $strNameLast ) {
		parent::setNameLast( ucfirst( strip_tags( $strNameLast ) ) );
	}

	public function setCurrentMoveOutReasonListItemId( $intCurrentMoveOutReasonListItemId ) {
		$this->m_intCurrentMoveOutReasonListItemId = $intCurrentMoveOutReasonListItemId;
	}

	public function setCurrentCompanyResidenceTypeId( $intCurrentCompanyResidenceTypeId ) {
		$this->m_intCurrentCompanyResidenceTypeId = $intCurrentCompanyResidenceTypeId;
	}

	public function setCurrentMoveInDate( $strCurrentMoveInDate ) {
		$this->m_strCurrentMoveInDate = $strCurrentMoveInDate;
	}

	public function setCurrentMoveOutDate( $strCurrentMoveOutDate ) {
		$this->m_strCurrentMoveOutDate = $strCurrentMoveOutDate;
	}

	public function setCurrentMonthlyPaymentAmount( $fltCurrentMonthlyPaymentAmount ) {
		$this->m_fltCurrentMonthlyPaymentAmount = $fltCurrentMonthlyPaymentAmount;
	}

	public function setCurrentPaymentRecipient( $strCurrentPaymentRecipient ) {
		$this->m_strCurrentPaymentRecipient = $strCurrentPaymentRecipient;
	}

	public function setCurrentStateCode( $strCurrentStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strCurrentStateCode, $strAddressKey = 'default', 'm_strCurrentStateCode' );
	}

	public function setCurrentPostalCode( $strCurrentPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strCurrentPostalCode, $strAddressKey = 'default', 'm_strCurrentPostalCode' );
	}

	public function setCurrentCountryCode( $strCurrentCountryCode ) {
		$this->setPostalAddressField( 'country', $strCurrentCountryCode, $strAddressKey = 'default', 'm_strCurrentCountryCode' );
	}

	public function setCurrentIsVerified( $boolCurrentIsVerified ) {
		$this->m_boolCurrentIsVerified = $boolCurrentIsVerified;
	}

	public function setCurrentAddressOwnerType( $strCurrentAddressOwnerType ) {
		$this->m_strCurrentAddressOwnerType = $strCurrentAddressOwnerType;
	}

	public function setCurrentIsNonUsAddress( $boolCurrentIsNonUsAddress ) {
		$this->m_boolCurrentIsNonUsAddress = $boolCurrentIsNonUsAddress;
	}

	public function setPreviousCompanyResidenceTypeId( $intPreviousCompanyResidenceTypeId ) {
		$this->m_intPreviousCompanyResidenceTypeId = $intPreviousCompanyResidenceTypeId;
	}

	public function setPreviousMoveOutReasonListItemId( $intPreviousMoveOutReasonListItemId ) {
		$this->m_intPreviousMoveOutReasonListItemId = $intPreviousMoveOutReasonListItemId;
	}

	public function setPreviousMoveInDate( $strPreviousMoveInDate ) {
		$this->m_strPreviousMoveInDate = $strPreviousMoveInDate;
	}

	public function setPreviousMoveOutDate( $strPreviousMoveOutDate ) {
		$this->m_strPreviousMoveOutDate = $strPreviousMoveOutDate;
	}

	public function setPreviousMonthlyPaymentAmount( $fltPreviousMonthlyPaymentAmount ) {
		$this->m_fltPreviousMonthlyPaymentAmount = $fltPreviousMonthlyPaymentAmount;
	}

	public function setPreviousPaymentRecipient( $strPreviousPaymentRecipient ) {
		$this->m_strPreviousPaymentRecipient = $strPreviousPaymentRecipient;
	}

	public function setPreviousStateCode( $strPreviousStateCode ) {
		$this->m_strPreviousStateCode = $strPreviousStateCode;
	}

	public function setPreviousPostalCode( $strPreviousPostalCode ) {
		$this->m_strPreviousPostalCode = $strPreviousPostalCode;
	}

	public function setPreviousCountryCode( $strPreviousCountryCode ) {
		$this->m_strPreviousCountryCode = $strPreviousCountryCode;
	}

	public function setPreviousIsVerified( $boolPreviousIsVerified ) {
		$this->m_boolPreviousIsVerified = $boolPreviousIsVerified;
	}

	public function setPreviousAddressOwnerType( $strPreviousAddressOwnerType ) {
		$this->m_strPreviousAddressOwnerType = $strPreviousAddressOwnerType;
	}

	public function setPreviousIsNonUsAddress( $boolPreviousIsNonUsAddress ) {
		$this->m_boolPreviousIsNonUsAddress = $boolPreviousIsNonUsAddress;
	}

	public function setIsValidateRequired( $boolIsValidateRequired ) {
		$this->m_boolIsValidateRequired = $boolIsValidateRequired;
	}

	public function setCorporateLease( $boolCorporateLease ) {
		$this->m_boolIsCorporateLease = $boolCorporateLease;
	}

	public function setMobileNumber( $strMobileNumber ) {
		if( true == valStr( $strMobileNumber ) ) {
			$strMobileNumber = preg_replace( '~[^0-9+ ]+~', '', $strMobileNumber );
		}

		parent::setMobileNumber( $strMobileNumber );
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		if( true == valStr( $strPhoneNumber ) ) {
			$strPhoneNumber = preg_replace( '~[^0-9+ ]+~', '', $strPhoneNumber );
		}

		parent::setPhoneNumber( $strPhoneNumber );
	}

	public function setWorkNumber( $strWorkNumber ) {
		if( true == valStr( $strWorkNumber ) ) {
			$strWorkNumber = preg_replace( '~[^0-9+ ]+~', '', $strWorkNumber );
		}

		parent::setWorkNumber( $strWorkNumber );
	}

	public function setCurrentStreetLine1( $strCurrentStreetLine1 ) {

		$strCurrentStreetLine1 = ( false == valStr( $strCurrentStreetLine1 ) ? '' : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCurrentStreetLine1, 100, NULL, true ) ) ) );
		$this->setPostalAddressField( 'addressLine1', $strCurrentStreetLine1, $strAddressKey = 'default', 'm_strCurrentStreetLine1' );
	}

	public function setCurrentStreetLine2( $strCurrentStreetLine2 ) {

		$strCurrentStreetLine2 = ( false == valStr( $strCurrentStreetLine2 ) ? '' : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCurrentStreetLine2, 100, NULL, true ) ) ) );
		$this->setPostalAddressField( 'addressLine2', $strCurrentStreetLine2, $strAddressKey = 'default', 'm_strCurrentStreetLine2' );
	}

	public function setCurrentStreetLine3( $strCurrentStreetLine3 ) {

		$strCurrentStreetLine3 = ( false == valStr( $strCurrentStreetLine3 ) ? '' : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCurrentStreetLine3, 100, NULL, true ) ) ) );
		$this->setPostalAddressField( 'addressLine3', $strCurrentStreetLine3, $strAddressKey = 'default', 'm_strCurrentStreetLine3' );
	}

	public function setCurrentCity( $strCurrentCity ) {

		$strCurrentCity = ( false == valStr( $strCurrentCity ) ? '' : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCurrentCity, 50, NULL, true ) ) ) );
		$this->setPostalAddressField( 'locality', $strCurrentCity, $strAddressKey = 'default', 'm_strCurrentCity' );
	}

	public function setCurrentCommunityName( $strCurrentCommunityName ) {
		$this->m_strCurrentCommunityName = ( 0 == strlen( $strCurrentCommunityName ) ? NULL : CStrings::strTrimDef( $strCurrentCommunityName, 50, NULL, true ) );
	}

	public function setCurrentReasonForLeaving( $strCurrentReasonForLeaving ) {
		$this->m_strCurrentReasonForLeaving = ( 0 == strlen( $strCurrentReasonForLeaving ) ? NULL : CStrings::strTrimDef( $strCurrentReasonForLeaving, 240, NULL, true ) );
	}

	public function setPreviousCommunityName( $strPreviousCommunityName ) {
		$this->m_strPreviousCommunityName = ( 0 == strlen( $strPreviousCommunityName ) ? NULL : CStrings::strTrimDef( $strPreviousCommunityName, 50, NULL, true ) );
	}

	public function setPreviousReasonForLeaving( $strPreviousReasonForLeaving ) {
		$this->m_strPreviousReasonForLeaving = ( 0 == strlen( $strPreviousReasonForLeaving ) ? NULL : CStrings::strTrimDef( $strPreviousReasonForLeaving, 240, NULL, true ) );
	}

	public function setPreviousStreetLine1( $strPreviousStreetLine1 ) {
		$this->m_strPreviousStreetLine1 = ( 0 == strlen( $strPreviousStreetLine1 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPreviousStreetLine1, 100, NULL, true ) ) ) );
	}

	public function setPreviousStreetLine2( $strPreviousStreetLine2 ) {
		$this->m_strPreviousStreetLine2 = ( 0 == strlen( $strPreviousStreetLine2 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPreviousStreetLine2, 100, NULL, true ) ) ) );
	}

	public function setPreviousStreetLine3( $strPreviousStreetLine3 ) {
		$this->m_strPreviousStreetLine3 = ( 0 == strlen( $strPreviousStreetLine3 ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPreviousStreetLine3, 100, NULL, true ) ) ) );
	}

	public function setPreviousCity( $strPreviousCity ) {
		$this->m_strPreviousCity = ( 0 == strlen( $strPreviousCity ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPreviousCity, 50, NULL, true ) ) ) );
	}

	public function setAuthenticationLogId( $intAuthenticationLogId ) {
		$this->m_intAuthenticationLogId = CStrings::strToIntDef( $intAuthenticationLogId, NULL, true );
	}

	public function setCustomer( $objCustomer ) {
		$this->m_objCustomer = $objCustomer;
	}

	public function setCustomerAddress( $objCustomerAddress ) {
		$this->m_objCustomerAddress = $objCustomerAddress;
	}

	public function setCustomerIncome( $objCustomerIncome ) {
		$this->m_objCustomerIncome = $objCustomerIncome;
	}

	public function setDlNumber( $strPlainDlNumber ) {
		if( false == valStr( $strPlainDlNumber ) || true == \Psi\CStringService::singleton()->stristr( $strPlainDlNumber, 'xxxx' ) ) {
			return;
		}
		$this->setDlNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainDlNumber, CONFIG_SODIUM_KEY_DL_NUMBER ) );
	}

	public function setIdentificationNumber( $strPlainDlNumber ) {
		if( false == preg_match( '/[a-zA-Z0-9]/', $strPlainDlNumber ) ) {
			return;
		}
		$strPlainDlNumber = CStrings::strTrimDef( $strPlainDlNumber, 240, NULL, true );
		$this->setIdentificationValue( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainDlNumber, CONFIG_SODIUM_KEY_DL_NUMBER ) );
	}

	public function setHasTaxNumber( $boolHasTaxNumber ) {
		$this->setDetailsField( 'has_tax_number', $boolHasTaxNumber );
	}

	public function setBirthDate( $strBirthDate ) {
		if( true == \Psi\CStringService::singleton()->stristr( $strBirthDate, '**' ) ) {
			return;
		}
		$this->m_strBirthDate = CStrings::strTrimDef( $strBirthDate, -1, NULL, true );
	}

	public function setBirthMonths( $intBirthMonths ) {
		$this->m_intBirthMonths = $intBirthMonths;
	}

	public function setTaxNumber( $strPlainTaxNumber ) {
		if( false == valStr( $strPlainTaxNumber ) || true == \Psi\CStringService::singleton()->stristr( $strPlainTaxNumber, '*' ) ) {
			return;
		}
		$strPlainTaxNumber = preg_replace( ( CTaxIdType::SSN != $this->getTaxIdTypeId() ? CTaxIdType::OTHER_TAX_TYPE_VALIDATION_STRING : CTaxIdType::SSN_TAX_TYPE_VALIDATION_STRING ), '', $strPlainTaxNumber );
		$this->setTaxNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainTaxNumber, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		$this->setTaxNumberMasked( CEncryption::maskTaxIdTypeIdText( $strPlainTaxNumber, $this->getTaxIdTypeId() ) );

	}

	public function setCustomerTypeId( $intCustomerTypeId ) {
		$this->m_intCustomerTypeId = $intCustomerTypeId;
	}

	public function setApplicationStatus( $strApplicationStatus ) {
		$this->m_strApplicationStatus = $strApplicationStatus;
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->m_intApplicantApplicationId = $intApplicantApplicationId;
	}

	public function setLivingInThisApartment( $boolLivingInThisApartment ) {
		$this->m_boolLivingInThisApartment = $boolLivingInThisApartment;
	}

	public function setTypeOfOccupent( $strTypeOfOccupent ) {
		$this->m_strTypeOfOccupent = $strTypeOfOccupent;
	}

	public function setApplicationFormId( $intApplicationFormId ) {
		$this->m_intApplicationFormId = $intApplicationFormId;
	}

	public function setApplicationDocumentId( $intApplicationDocumentId ) {
		$this->m_intApplicationDocumentId = $intApplicationDocumentId;
	}

	public function setLeaseFormId( $intLeaseFormId ) {
		$this->m_intLeaseFormId = $intLeaseFormId;
	}

	public function setLeaseDocumentId( $intLeaseDocumentId ) {
		$this->m_intLeaseDocumentId = $intLeaseDocumentId;
	}

	public function setCustomerType( $strCustomerType ) {
		$this->m_strCustomerType = $strCustomerType;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setUsernameConfirm( $strUsernameConfirm ) {
		$this->m_strUsernameConfirm = $strUsernameConfirm;
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', trim( $strPassword ) );
	}

	public function setPasswordConfirm( $strPasswordConfirm ) {
		$this->m_strPasswordConfirm = $strPasswordConfirm;
	}

	public function generatePassword() {
		// Generate password with password complexity policy
		$strPassword		= md5( uniqid( mt_rand(), true ) );
		$strPassword		= \Psi\CStringService::singleton()->substr( $strPassword, 0, 8 );
		$intPasswordCount	= \Psi\CStringService::singleton()->strlen( $strPassword );

		// @TODO: Please remove magic numbers
		for( $intCount = 0; $intCount < $intPasswordCount; $intCount++ ) {
			if( ( int ) ord( $strPassword[$intCount] ) <= 122 && ( int ) ord( $strPassword[$intCount] ) >= 97 ) {
				$strPassword[$intCount] = \Psi\CStringService::singleton()->ucwords( $strPassword[$intCount] );
				break;
			}
		}

		$strPasswordPattern = '/^(?!.*([a-z\d])\1{1})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d\S]{8,}$/';

		if( 1 != \Psi\CStringService::singleton()->preg_match( $strPasswordPattern, $strPassword ) ) {
			$strPassword = $this->generatePassword();
		}

		return $strPassword;
	}

	public function setApplicationStepId( $intApplicationStepId ) {
		$this->m_intApplicationStepId = $intApplicationStepId;
	}

	public function setCurrentApplicationStepId( $intCurrentApplicationStepId ) {
		$this->m_intCurrentApplicationStepId = $intCurrentApplicationStepId;
	}

	public function setValFailed( $boolValFailed ) {
		$this->m_boolValFailed = $boolValFailed;
	}

	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = $boolIsDelete;
	}

	public function setIsBlanked( $boolIsBlanked ) {
		$this->m_boolIsBlanked = $boolIsBlanked;
	}

	public function setAssociationId( $intAssociationId ) {
		$this->m_intAssociationId = $intAssociationId;
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->m_strCompletedOn = $strCompletedOn;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setPrimaryIsResponsible( $boolPrimaryIsResponsible ) {
		$this->m_boolPrimaryIsResponsible = $boolPrimaryIsResponsible;
	}

	public function setSendConsumerReport( $boolSendConsumerReport ) {
		$this->m_boolSendConsumerReport = $boolSendConsumerReport;
	}

	public function setRelationship( $intRelationship ) {
		$this->m_intRelationship = $intRelationship;
	}

	public function setRelation( $strRelation ) {
		$this->m_strRelation = $strRelation;
	}

	public function setCustomerRelationshipId( $intCustomerRelationshipId ) {
		$this->m_intCustomerRelationshipId = $intCustomerRelationshipId;
	}

	public function setHeightFeet( $intHeightFeet ) {
		$this->m_intHeightFeet = CStrings::strToIntDef( $intHeightFeet, NULL, false );
	}

	public function setApplicationSteps( $arrobjApplicationSteps ) {
		$this->m_arrobjApplicationSteps = $arrobjApplicationSteps;
	}

	public function setHeightInches( $intHeightInches ) {
		$this->m_intHeightInches = CStrings::strToIntDef( $intHeightInches, NULL, false );
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setIsBlockMessageCenterEmail( $boolIsBlockMessageCenterEmail ) {
		$this->m_boolIsBlockMessageCenterEmail = $boolIsBlockMessageCenterEmail;
	}

	public function setIsBlockContactPointsEmail( $boolIsBlockContactPointsEmail ) {
		$this->m_boolIsBlockContactPointsEmail = $boolIsBlockContactPointsEmail;
	}

	public function setIsBlockLeadCommunicationEmail( $boolIsBlockLeadCommunicationEmail ) {
		$this->m_boolIsBlockLeadCommunicationEmail = $boolIsBlockLeadCommunicationEmail;
	}

	public function setIsBlockPropertyMarketingEmail( $boolIsBlockPropertyMarketingEmail ) {
		$this->m_boolIsBlockPropertyMarketingEmail = $boolIsBlockPropertyMarketingEmail;
	}

	public function setIsBlockMessageCenterSms( $boolIsBlockMessageCenterSms ) {
		$this->m_boolIsBlockMessageCenterSms = $boolIsBlockMessageCenterSms;
	}

	public function setIsBlockPropertyMarketingSms( $boolIsBlockPropertyMarketingSms ) {
		$this->m_boolIsBlockPropertyMarketingSms = $boolIsBlockPropertyMarketingSms;
	}

	public function setApplicantApplicationTransmissionId( $intApplicantApplicationTransmissionId ) {
		$this->m_intApplicantApplicationTransmissionId = $intApplicantApplicationTransmissionId;
	}

	public function setTransmissionOrderId( $intTransmissionOrderId ) {
		$this->m_intTransmissionOrderId = CStrings::strTrimDef( $intTransmissionOrderId, 64, NULL, true );
	}

	public function setScreeningApplicantTypeId( $intScreeningApplicantTypeId ) {
		$this->m_intScreeningApplicantTypeId = $intScreeningApplicantTypeId;
	}

	public function setApplicantInstitutionName( $strApplicantInstitutionName ) {
		$this->m_strApplicantInstitutionName = $strApplicantInstitutionName;
	}

	public function setApplicantCurrentIncome( $intApplicantCurrentIncome ) {
		$this->m_intCurrentCustomerIncome = $intApplicantCurrentIncome;
	}

	public function setApplicantApplicationDeletedOn( $strApplicantApplicationDeletedOn ) {
		$this->m_strApplicantApplicationDeletedOn = $strApplicantApplicationDeletedOn;
	}

	public function setScreeningPackageId( $intScreeningPackageId ) {
		$this->m_intScreeningPackageId = $intScreeningPackageId;
	}

	public function setIsLead( $intIsLead ) {
		$this->m_intIsLead = $intIsLead;
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->m_intLeadSourceId = $intLeadSourceId;
	}

	public function setSubsidyTracsVersionId( $strTracsVersionId ) {
		$this->m_strSubsidyTracsVersionId = $strTracsVersionId;
	}

	public function setDefaultCustomerRelationshipId( $intDefaultCustomerRelationshipId ) {
		$this->m_intDefaultCustomerRelationshipId = $intDefaultCustomerRelationshipId;
	}

	public function setOccupancyTypeId( $intOccupancyTypeId ) {
		$this->m_intOccupancyTypeId = $intOccupancyTypeId;
	}

	public function setPrimaryPhoneNumber( $strPrimaryPhoneNumber ) {
		$this->m_strPrimaryPhoneNumber = $strPrimaryPhoneNumber;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['tax_number'] ) ) {
			$this->setTaxNumber( $arrmixValues['tax_number'] );
		}
		if( true == isset( $arrmixValues['dl_number'] ) ) {
			$this->setDlNumber( $arrmixValues['dl_number'] );
		}
//		if( true == isset( $arrmixValues['identification_number'] ) ) {
//			$this->setIdentificationNumber( $arrmixValues['identification_number'] );
//		}
		if( true == isset( $arrmixValues['customer_type_id'] ) ) {
			$this->setCustomerTypeId( $arrmixValues['customer_type_id'] );
		}
		if( true == isset( $arrmixValues['applicant_application_id'] ) ) {
			$this->setApplicantApplicationId( $arrmixValues['applicant_application_id'] );
		}
		if( true == isset( $arrmixValues['application_form_id'] ) ) {
			$this->setApplicationFormId( $arrmixValues['application_form_id'] );
		}
		if( true == isset( $arrmixValues['application_document_id'] ) ) {
			$this->setApplicationDocumentId( $arrmixValues['application_document_id'] );
		}
		if( true == isset( $arrmixValues['lease_form_id'] ) ) {
			$this->setLeaseFormId( $arrmixValues['lease_form_id'] );
		}
		if( true == isset( $arrmixValues['lease_document_id'] ) ) {
			$this->setLeaseDocumentId( $arrmixValues['lease_document_id'] );
		}
		if( true == isset( $arrmixValues['customer_type'] ) ) {
			$this->setCustomerType( $arrmixValues['customer_type'] );
		}
		if( true == isset( $arrmixValues['application_status'] ) ) {
			$this->setApplicationStatus( $arrmixValues['application_status'] );
		}
		if( true == isset( $arrmixValues['application_id'] ) ) {
			$this->setApplicationId( $arrmixValues['application_id'] );
		}
		if( true == isset( $arrmixValues['username_confirm'] ) ) {
			$this->setUsernameConfirm( $arrmixValues['username_confirm'] );
		}
		if( true == isset( $arrmixValues['password'] ) ) {
			$this->setPassword( $arrmixValues['password'] );
		}
		if( true == isset( $arrmixValues['password_confirm'] ) ) {
			$this->setPasswordConfirm( $arrmixValues['password_confirm'] );
		}
		if( true == isset( $arrmixValues['application_step_id'] ) ) {
			$this->setApplicationStepId( $arrmixValues['application_step_id'] );
		}
		if( true == isset( $arrmixValues['agrees_to_pay_fees'] ) ) {
			$this->setAgreesToPayFees( $arrmixValues['agrees_to_pay_fees'] );
		}
		if( true == isset( $arrmixValues['agrees_to_terms'] ) ) {
			$this->setAgreesToTerms( $arrmixValues['agrees_to_terms'] );
		}
		if( true == isset( $arrmixValues['completed_on'] ) ) {
			$this->setCompletedOn( $arrmixValues['completed_on'] );
		}
		if( true == isset( $arrmixValues['primary_is_responsible'] ) ) {
			$this->setPrimaryIsResponsible( $arrmixValues['primary_is_responsible'] );
		}
		if( true == isset( $arrmixValues['relationship'] ) ) {
			$this->setRelationship( $arrmixValues['relationship'] );
		}
		if( true == isset( $arrmixValues['customer_relationship_id'] ) ) {
			$this->setCustomerRelationshipId( $arrmixValues['customer_relationship_id'] );
		}
		if( true == isset( $arrmixValues['property_id'] ) ) {
			$this->setPropertyId( $arrmixValues['property_id'] );
		}
		if( true == isset( $arrmixValues['height_feet'] ) ) {
			$this->setHeightFeet( $arrmixValues['height_feet'] );
		}
		if( true == isset( $arrmixValues['height_inches'] ) ) {
			$this->setHeightInches( $arrmixValues['height_inches'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['is_blanked'] ) ) {
			$this->setIsBlanked( $arrmixValues['is_blanked'] );
		}
		if( true == isset( $arrmixValues['is_block_message_center_email'] ) ) {
			$this->setIsBlockMessageCenterEmail( $arrmixValues['is_block_message_center_email'] );
		}
		if( true == isset( $arrmixValues['is_block_message_center_sms'] ) ) {
			$this->setIsBlockMessageCenterSms( $arrmixValues['is_block_message_center_sms'] );
		}
		if( true == isset( $arrmixValues['is_block_property_marketing_sms'] ) ) {
			$this->setIsBlockPropertyMarketingSms( $arrmixValues['is_block_property_marketing_sms'] );
		}
		if( true == isset( $arrmixValues['is_block_contact_points_email'] ) ) {
			$this->setIsBlockContactPointsEmail( $arrmixValues['is_block_contact_points_email'] );
		}
		if( true == isset( $arrmixValues['is_block_lead_communication_email'] ) ) {
			$this->setIsBlockLeadCommunicationEmail( $arrmixValues['is_block_lead_communication_email'] );
		}
		if( true == isset( $arrmixValues['is_block_property_marketing_email'] ) ) {
			$this->setIsBlockPropertyMarketingEmail( $arrmixValues['is_block_property_marketing_email'] );
		}
		if( true == isset( $arrmixValues['applicant_application_deleted_on'] ) ) {
			$this->setApplicantApplicationDeletedOn( $arrmixValues['applicant_application_deleted_on'] );
		}
		if( true == isset( $arrmixValues['is_lead'] ) ) {
			$this->setIsLead( $arrmixValues['is_lead'] );
		}
		if( true == isset( $arrmixValues['send_consumer_report'] ) ) {
			$this->setSendConsumerReport( $arrmixValues['send_consumer_report'] );
		}
		if( true == isset( $arrmixValues['has_name_last_matronymic'] ) ) {
			$this->setHasNameLastMatronymic( $arrmixValues['has_name_last_matronymic'] );
		}

		$strNotes = ( ( true == isset( $arrmixValues['notes'] ) ) && true == valStr( $arrmixValues['notes'] ) ) ? $arrmixValues['notes'] : NULL;
		$this->setNotes( $strNotes );

		if( false == is_null( $this->getHeightFeet() ) || false == is_null( $this->getHeightInches() ) ) {
			$this->setHeight( ( $this->getHeightFeet() * 12 ) + $this->getHeightInches() );

		} else {
			if( 0 < $this->getHeight() ) {
				$this->setHeightFeet( $this->getHeight() / 12 );
				$this->setHeightInches( $this->getHeight() - ( $this->getHeightFeet() * 12 ) );
			}
		}

		if( true == isset( $arrmixValues['customer_relationship_name'] ) ) {
			$this->setCustomerRelationShipName( $arrmixValues['customer_relationship_name'] );
		}

		if( true == isset( $arrmixValues['move_in_date'] ) ) {
			$this->setMoveInDate( $arrmixValues['move_in_date'] );
		}

		if( true == isset( $arrmixValues['lead_source_id'] ) ) {
			$this->setLeadSourceId( $arrmixValues['lead_source_id'] );
		}

		if( true == isset( $arrmixValues['occupancy_type_id'] ) ) {
			$this->setOccupancyTypeId( $arrmixValues['occupancy_type_id'] );
		}

		if( true == isset( $arrmixValues['current_company_residence_type_id'] ) ) {
			$this->setCurrentCompanyResidenceTypeId( $arrmixValues['current_company_residence_type_id'] );
		}

		if( true == isset( $arrmixValues['current_move_out_reason_list_item_id'] ) ) {
			$this->setCurrentMoveOutReasonListItemId( $arrmixValues['current_move_out_reason_list_item_id'] );
		}

		if( true == isset( $arrmixValues['current_community_name'] ) ) {
			$this->setCurrentCommunityName( $arrmixValues['current_community_name'] );
		}

		if( true == isset( $arrmixValues['current_reason_for_leaving'] ) ) {
			$this->setCurrentReasonForLeaving( $arrmixValues['current_reason_for_leaving'] );
		}

		if( true == isset( $arrmixValues['current_move_in_date'] ) ) {
			$this->setCurrentMoveInDate( $arrmixValues['current_move_in_date'] );
		}

		if( true == isset( $arrmixValues['current_move_out_date'] ) ) {
			$this->setCurrentMoveOutDate( $arrmixValues['current_move_out_date'] );
		}

		if( true == isset( $arrmixValues['current_monthly_payment_amount'] ) ) {
			$this->setCurrentMonthlyPaymentAmount( $arrmixValues['current_monthly_payment_amount'] );
		}

		if( true == isset( $arrmixValues['current_payment_recipient'] ) ) {
			$this->setCurrentPaymentRecipient( $arrmixValues['current_payment_recipient'] );
		}

		if( true == isset( $arrmixValues['current_street_line1'] ) ) {
			$this->setCurrentStreetLine1( $arrmixValues['current_street_line1'] );
		}

		if( true == isset( $arrmixValues['current_street_line2'] ) ) {
			$this->setCurrentStreetLine2( $arrmixValues['current_street_line2'] );
		}

		if( true == isset( $arrmixValues['current_street_line3'] ) ) {
			$this->setCurrentStreetLine3( $arrmixValues['current_street_line3'] );
		}

		if( true == isset( $arrmixValues['current_city'] ) ) {
			$this->setCurrentCity( $arrmixValues['current_city'] );
		}

		if( true == isset( $arrmixValues['current_state_code'] ) ) {
			$this->setCurrentStateCode( $arrmixValues['current_state_code'] );
		}

		if( true == isset( $arrmixValues['current_postal_code'] ) ) {
			$this->setCurrentPostalCode( $arrmixValues['current_postal_code'] );
		}

		if( true == isset( $arrmixValues['current_country_code'] ) ) {
			$this->setCurrentCountryCode( $arrmixValues['current_country_code'] );
		}

		if( true == isset( $arrmixValues['current_is_verified'] ) ) {
            $this->set( 'm_boolCurrentIsVerified', CStrings::strToBool( $arrmixValues['current_is_verified'] ) );
		}

		if( true == isset( $arrmixValues['current_address_owner_type'] ) ) {
			$this->setCurrentAddressOwnerType( $arrmixValues['current_address_owner_type'] );
		}

		if( true == isset( $arrmixValues['current_is_non_us_address'] ) ) {
            $this->set( 'm_boolCurrentIsNonUsAddress', CStrings::strToBool( $arrmixValues['current_is_non_us_address'] ) );
		}

		if( true == isset( $arrmixValues['previous_company_residence_type_id'] ) ) {
			$this->setPreviousCompanyResidenceTypeId( $arrmixValues['previous_company_residence_type_id'] );
		}

		if( true == isset( $arrmixValues['previous_move_out_reason_list_item_id'] ) ) {
			$this->setPreviousMoveOutReasonListItemId( $arrmixValues['previous_move_out_reason_list_item_id'] );
		}

		if( true == isset( $arrmixValues['previous_community_name'] ) ) {
			$this->setPreviousCommunityName( $arrmixValues['previous_community_name'] );
		}

		if( true == isset( $arrmixValues['previous_reason_for_leaving'] ) ) {
			$this->setPreviousReasonForLeaving( $arrmixValues['previous_reason_for_leaving'] );
		}

		if( true == isset( $arrmixValues['previous_move_in_date'] ) ) {
			$this->setPreviousMoveInDate( $arrmixValues['previous_move_in_date'] );
		}

		if( true == isset( $arrmixValues['previous_move_out_date'] ) ) {
			$this->setPreviousMoveOutDate( $arrmixValues['previous_move_out_date'] );
		}

		if( true == isset( $arrmixValues['previous_monthly_payment_amount'] ) ) {
			$this->setPreviousMonthlyPaymentAmount( $arrmixValues['previous_monthly_payment_amount'] );
		}

		if( true == isset( $arrmixValues['previous_payment_recipient'] ) ) {
			$this->setPreviousPaymentRecipient( $arrmixValues['previous_payment_recipient'] );
		}

		if( true == isset( $arrmixValues['previous_street_line1'] ) ) {
			$this->setPreviousStreetLine1( $arrmixValues['previous_street_line1'] );
		}

		if( true == isset( $arrmixValues['previous_street_line2'] ) ) {
			$this->setPreviousStreetLine2( $arrmixValues['previous_street_line2'] );
		}

		if( true == isset( $arrmixValues['previous_street_line3'] ) ) {
			$this->setPreviousStreetLine3( $arrmixValues['previous_street_line3'] );
		}

		if( true == isset( $arrmixValues['previous_city'] ) ) {
			$this->setPreviousCity( $arrmixValues['previous_city'] );
		}

		if( true == isset( $arrmixValues['previous_state_code'] ) ) {
			$this->setPreviousStateCode( $arrmixValues['previous_state_code'] );
		}

		if( true == isset( $arrmixValues['previous_postal_code'] ) ) {
			$this->setPreviousPostalCode( $arrmixValues['previous_postal_code'] );
		}

		if( true == isset( $arrmixValues['previous_country_code'] ) ) {
			$this->setPreviousCountryCode( $arrmixValues['previous_country_code'] );
		}

		if( true == isset( $arrmixValues['previous_is_verified'] ) ) {
            $this->set( 'm_boolPreviousIsVerified', CStrings::strToBool( $arrmixValues['previous_is_verified'] ) );
		}

		if( true == isset( $arrmixValues['previous_address_owner_type'] ) ) {
			$this->setPreviousAddressOwnerType( $arrmixValues['previous_address_owner_type'] );
		}

		if( true == isset( $arrmixValues['previous_is_non_us_address'] ) ) {
            $this->set( 'm_boolPreviousIsNonUsAddress', CStrings::strToBool( $arrmixValues['previous_is_non_us_address'] ) );
		}

		if( true == isset( $arrmixValues['lease_id'] ) ) {
			$this->setLeaseId( $arrmixValues['lease_id'] );
		}

		if( true == isset( $arrmixValues['lease_interval_id'] ) ) {
			$this->setLeaseIntervalId( $arrmixValues['lease_interval_id'] );
		}

		if( true == isset( $arrmixValues['preferred_locale_code'] ) ) {
			$this->setPreferredLocaleCode( $arrmixValues['preferred_locale_code'] );
		}

		if( true == isset( $arrmixValues['current_dependent_locality'] ) ) {
			$this->setCurrentDependentLocality( $arrmixValues['current_dependent_locality'] );
		}

		if( true == isset( $arrmixValues['previous_dependent_locality'] ) ) {
			$this->setPreviousDependentLocality( $arrmixValues['previous_dependent_locality'] );
		}

		if( true == isset( $arrmixValues['current_sorting_code'] ) ) {
			$this->setCurrentSortingCode( $arrmixValues['current_sorting_code'] );
		}

		if( true == isset( $arrmixValues['previous_sorting_code'] ) ) {
			$this->setPreviousSortingCode( $arrmixValues['previous_sorting_code'] );
		}

		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrmixValues, $this->m_arrstrPostalAddressFields );
		}

		if( true == isset( $arrmixValues['primary_phone_number'] ) ) {
			$this->setPrimaryPhoneNumber( $arrmixValues['primary_phone_number'] );
		}

		return;
	}

	public function setIsMilitaryApplicaion( $boolIsMilitaryApplication = false ) {
		return $this->m_boolIsMilitaryApplicaion = $boolIsMilitaryApplication;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setLeaseIntervalId( $intLeaseIntervalId ) {
		$this->m_intLeaseIntervalId = $intLeaseIntervalId;
	}

	public function setPreferredLocaleCode( $strPreferredLocaleCode ) {
		$this->m_strPreferredLocaleCode = $strPreferredLocaleCode;
	}

	public function setNameLastMatronymic( $strNameLastMatronymic ) {
		$this->m_strNameLastMatronymic = \Psi\CStringService::singleton()->ucfirst( strip_tags( $strNameLastMatronymic ) );
	}

	public function setCurrentDependentLocality( $strCurrentDependentLocality ) {
		$this->m_strCurrentDependentLocality = ( 0 == strlen( $strCurrentDependentLocality ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCurrentDependentLocality, 50, NULL, true ) ) ) );
	}

	public function setPreviousDependentLocality( $strPreviousDependentLocality ) {
		$this->m_strPreviousDependentLocality = ( 0 == strlen( $strPreviousDependentLocality ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPreviousDependentLocality, 50, NULL, true ) ) ) );
	}

	public function setCurrentSortingCode( $strCurrentSortingCode ) {
		$this->m_strCurrentSortingCode = ( 0 == strlen( $strCurrentSortingCode ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strCurrentSortingCode, 50, NULL, true ) ) ) );
	}

	public function setPreviousSortingCode( $strPreviousSortingCode ) {
		$this->m_strPreviousSortingCode = ( 0 == strlen( $strPreviousSortingCode ) ? NULL : \Psi\CStringService::singleton()->ucwords( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strPreviousSortingCode, 50, NULL, true ) ) ) );
	}

	public function setIsApplicantDeduped( $boolIsApplicantDeduped ) {
		$this->m_boolIsApplicantDeduped = $boolIsApplicantDeduped;
	}

	public function setIsOnlyAdditionalPhoneNumber( $boolIsOnlyAdditionalPhoneNumber ) {
		return $this->m_boolIsOnlyAdditionalPhoneNumber = $boolIsOnlyAdditionalPhoneNumber;
	}

	public function setCustomerPhoneNumbers( $arrmixPhoneNumbers ) {
		$this->m_arrmixCustomerPhoneNumbers = $arrmixPhoneNumbers;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createApplicantContact() {

		$objApplicantContact = new CApplicantContact();

		$objApplicantContact->setCid( $this->m_intCid );
		$objApplicantContact->setApplicantId( $this->m_intId );

		return $objApplicantContact;
	}

	public function createCustomerIncome() {

		$objCustomerIncome = new CCustomerIncome();

		$objCustomerIncome->setCid( $this->getCid() );
		$objCustomerIncome->setCustomerId( $this->getCustomerId() );
		$objCustomerIncome->setApplicantId( $this->getId() );
		$objCustomerIncome->setFrequencyId( CFrequency::MONTHLY );
		$objCustomerIncome->setIsDelete( false );

		return $objCustomerIncome;
	}

	public function createApplicantAsset() {

		$objApplicantAsset = new CApplicantAsset();
		$objApplicantAsset->setCid( $this->m_intCid );
		$objApplicantAsset->setApplicantId( $this->m_intId );

		return $objApplicantAsset;
	}

	public function createCustomerAsset() {

		$objCustomerAsset = new CCustomerAsset();
		$objCustomerAsset->setCid( $this->m_intCid );
		$objCustomerAsset->setApplicantId( $this->m_intId );
		$objCustomerAsset->setCustomerId( $this->m_intCustomerId );

		return $objCustomerAsset;
	}

	public function createLeaseCustomerVehicle() {

		$objLeaseCustomerVehicle = new CLeaseCustomerVehicle();

		$objLeaseCustomerVehicle->setCid( $this->m_intCid );
		$objLeaseCustomerVehicle->setCustomerId( $this->m_intCustomerId );

		return $objLeaseCustomerVehicle;
	}

	public function createApplicantRoommateInterest() {
		$objApplicantRoommateInterest = new CApplicantRoommateInterest();

		$objApplicantRoommateInterest->setCid( $this->m_intCid );
		$objApplicantRoommateInterest->setApplicantId( $this->m_intId );

		return $objApplicantRoommateInterest;
	}

	public function createApplication() {

		$objApplication = new CApplication();
		$objApplication->setCid( $this->m_intCid );
		$objApplication->setApplicationStageId( CApplicationStage::APPLICATION );
		$objApplication->setApplicationStatusId( CApplicationStatus::STARTED );

		return $objApplication;
	}

	public function createApplicantApplication( $intApplicationId = NULL ) {

		$objApplicantApplication = new CApplicantApplication();
		$objApplicantApplication->setCid( $this->getCid() );
		$objApplicantApplication->setApplicantId( $this->getId() );
		$objApplicantApplication->setApplicationId( $intApplicationId );
		$objApplicantApplication->setStartedOn( 'NOW()' );
		$objApplicantApplication->setCustomerRelationshipId( $this->getCustomerRelationshipId() );
		$objApplicantApplication->setCustomerTypeId( $this->getCustomerTypeId() );

		return $objApplicantApplication;
	}

	public function createArPayment() {

		$objArPayment = new CArPayment();

		$objArPayment->setDefaults();
		$objArPayment->setIsApplicationPayment( true );
		$objArPayment->setBillingInfoIsStored( 0 );
		$objArPayment->setCid( $this->getCid() );
		$objArPayment->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
		$objArPayment->setBilltoNameFirst( $this->getNameFirst() );
		$objArPayment->setBilltoNameLast( $this->getNameLast() );
		$objArPayment->setBilltoEmailAddress( $this->getUsername() );
		$objArPayment->setBilltoPhoneNumber( $this->getPhoneNumber() );
		$objArPayment->setBilltoStreetLine1( $this->getCurrentStreetLine1() );
		$objArPayment->setBilltoStreetLine2( $this->getCurrentStreetLine2() );
		$objArPayment->setBilltoStreetLine3( $this->getCurrentStreetLine3() );
		$objArPayment->setBilltoCity( $this->getCurrentCity() );
		$objArPayment->setBilltoStateCode( $this->getCurrentStateCode() );
		$objArPayment->setBilltoPostalCode( $this->getCurrentPostalCode() );
		$objArPayment->setCustomerId( $this->getCustomerId() );

		$objArPayment->setPaymentDatetime( date( 'm/d/Y H:i s' ) );
		$objArPayment->setCcNameOnCard( $this->getNameFirst() . ' ' . $this->getNameLast() );
		$objArPayment->setCheckNameOnAccount( $this->getNameFirst() . ' ' . $this->getNameLast() );

		return $objArPayment;
	}

	public function createSignInEmail( $strEmailType, $strSecureBaseName, $objPrimaryApplicant, $objProperty, $arrobjPropertyApplicationPreferences, $objApplication, $objCompanyUser, $objDatabase, $objPropertyEmailRule = NULL ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objCustomer = $this->getOrFetchCustomer( $objDatabase );
		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$strPrimaryApplicantPreferredLocale = $objCustomer->getPreferredLocaleCode();
			CLocaleContainer::createService()->setPreferredLocaleCode( $strPrimaryApplicantPreferredLocale, $objDatabase );
		}
		// $arrobjProeprtyPreferences				=  $objProperty->fetchPropertyPreferences($objDatabase);
		$intCompanyApplicationId     = $objApplication->getCompanyApplicationId();
		$arrobjProeprtyNotifications = $objProperty->fetchPropertyNotifications( $objDatabase );
		$arrobjProeprtyNotifications = rekeyObjects( 'Key', $arrobjProeprtyNotifications );

		$objClient                        = $this->fetchClient( $objDatabase );
		$objMarketingMediaAssociation		= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		$objPropertyOfficePhoneNumber     = $objProperty->fetchOfficePhoneNumber( $objDatabase );
		$objPropertyAdditionalPhoneNumber = $objProperty->fetchSingleAdditionalPropertyPhoneNumber( $objDatabase );
		$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		$arrobjPropertyPreferences        = $objProperty->fetchPropertyPreferencesByKeys( [ 'FROM_APPLICATION_NOTIFICATION_EMAIL', 'FROM_LEASE_NOTIFICATION_EMAIL' ], $objDatabase );

		$arrobjPropertyPreferences = rekeyObjects( 'Key', $arrobjPropertyPreferences );

		$strForgotPwdLink = '';
		$strCreatePwdLink = '';
		$strSubject   = 'Rental Application for ' . $objProperty->getPropertyName();
		$strFromEmail = CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;

		$boolCreateNewPasswordFlag = false;

		if( true == valArr( $arrobjPropertyPreferences ) ) {
			if( true == array_key_exists( 'FROM_APPLICATION_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue() ) ) {
				$strFromEmail = $arrobjPropertyPreferences['FROM_APPLICATION_NOTIFICATION_EMAIL']->getValue();
			}
		}

		if( 'lease_agreement' == $strEmailType || 'lease_renewal_agreement' == $strEmailType ) {
			$strSubject = 'Lease Agreement For ' . $objProperty->getPropertyName();
			if( true == valArr( $arrobjPropertyPreferences ) ) {
				if( true == array_key_exists( 'FROM_LEASE_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && 0 < strlen( $arrobjPropertyPreferences['FROM_LEASE_NOTIFICATION_EMAIL']->getValue() ) ) {
					$strFromEmail = $arrobjPropertyPreferences['FROM_LEASE_NOTIFICATION_EMAIL']->getValue();
				}
			}

			if( true == is_null( $this->getPasswordEncrypted() ) || 0 == strlen( $this->getPasswordEncrypted() ) ) {
				$boolCreateNewPasswordFlag = true;
			} else {
				$strForgotPwdLink = $strSecureBaseName . 'Apartments/module/application_authentication/action/search_password/property[id]/' . $objProperty->getId();
			}

		} else {
			if( 'lease_approved' == $strEmailType || 'lease_renewal_approved' == $strEmailType ) {
				$strSubject = 'Lease Approved For ' . $objProperty->getPropertyName();

			} else {
				if( 'pre_screening_invitation' != $strEmailType ) {
					if( true == is_null( $this->getPasswordEncrypted() ) || 0 == strlen( $this->getPasswordEncrypted() ) ) {
						$boolCreateNewPasswordFlag = true;
					}
				}
			}
		}

		if( true == $boolCreateNewPasswordFlag ) {
			$strEncryptedUniqueKey = \Psi\CStringService::singleton()->substr( CEncryption::encrypt( uniqid( mt_rand(), true ), CONFIG_KEY_LOGIN_PASSWORD ), 0, 240 );

			$objDatabase->begin();

			$objOneTimeLink = new COneTimeLink();
			$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $this->getId(), 'key_encrypted' => $strEncryptedUniqueKey ] );

			if( false == $objOneTimeLink->insert( 1, $objDatabase ) ) {
				$boolIsValid = false;
				trigger_error( 'one time link failed to inserted.', E_USER_ERROR );
				$objDatabase->rollback();
				exit;
			}
			$objDatabase->commit();

			$strCreatePwdLink = $strSecureBaseName . 'Apartments/module/application_authentication/action/create_applicant_password/property[id]/' . ( ( true == is_null( $objProperty->getPropertyId() ) ) ? $objProperty->getId() : $objProperty->getPropertyId() ) . '/applicant[key]/' . urlencode( $strEncryptedUniqueKey ) . '/applicant[id]/' . $this->getId() . '/is_primary_applicant/' . ( ( $objPrimaryApplicant->getId() == $this->getId() ) ? 1 : 0 ) . '/application[id]/' . $objApplication->getId() . '/';
			if( 'revoke_account' == $strEmailType ) {
				$strCreatePwdLink = $strSecureBaseName . 'Apartments/module/application_authentication/property[id]/' . ( ( true == is_null( $objProperty->getPropertyId() ) ) ? $objProperty->getId() : $objProperty->getPropertyId() ) . '/';
			}
		}

		$arrintPsProductIds = $objClient->fetchPsProductPermissionsByPropertyIds( [ CPsProduct::LEAD_MANAGEMENT ], [ $objProperty->getId() ], $objDatabase );

		$boolIncludeDeletedAA = ( 'revoke_account' == $strEmailType ) ? true : false;
		$strFromEmailAddress = '';
		$strReplyToFromAddress = '';

		if( false == valStr( $strFromEmail ) && true == valArr( $arrintPsProductIds ) && ( 'application_invitation' == $strEmailType || 'send_reminder' == $strEmailType || 'revoke_account' == $strEmailType ) && true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objApplicantApplication = $objApplication->fetchApplicantApplicationByApplicantId( $this->getId(), $objDatabase, $boolIncludeDeletedAA );
			$strFromEmail            = preg_replace( '/[^a-zA-Z0-9 s]/', '', $objProperty->getPropertyName() ) . '<' . getFormattedEmailRelayFromEmailAddress( $objProperty, $objCompanyUser, $objApplicantApplication ) . '>';
		}

		if( true == valStr( $strFromEmailAddress ) ) {
			$strFromEmail = $strFromEmailAddress;
		}

		$intPropertyId = ( 0 < $objProperty->getPropertyId() ) ? $objProperty->getPropertyId() : $objProperty->getId();

		$strApplicationUrl = $strSecureBaseName . 'Apartments/module/application_authentication/action/view_login/returning_applicant/1/property[id]/' . $intPropertyId;

		if( false == is_null( $objApplication->getInternetListingServiceId() ) && CInternetListingService::APARTMENTGUIDE_APPLICATION == $objApplication->getInternetListingServiceId() ) {
			$strApplicationUrl .= '/ils/' . $objApplication->getInternetListingServiceId() . '/ils_template/' . $objApplication->getInternetListingServiceId() . '/';
		}

		$objSmarty = new CPsSmarty( PATH_INTERFACES_PROSPECT_PORTAL, false );
		CCustomerType::assignSmartyConstants( $objSmarty );

		$objSmarty->assign_by_ref( 'applicant', $this );
		$objSmarty->assign_by_ref( 'primary_applicant', $objPrimaryApplicant );
		$objSmarty->assign_by_ref( 'property', $objProperty );
		$objSmarty->assign_by_ref( 'marketing_media_association', $objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file', $objLogoMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'property_application_preferences', $arrobjPropertyApplicationPreferences );
		$objSmarty->assign_by_ref( 'forgot_password_link', $strForgotPwdLink );
		$objSmarty->assign_by_ref( 'create_password_link', $strCreatePwdLink );
		$objSmarty->assign_by_ref( 'email_type', $strEmailType );
		$objSmarty->assign_by_ref( 'property_notifactions', $arrobjProeprtyNotifications );

		$objSmarty->assign( 'property_email_rule', $objPropertyEmailRule );
		$objSmarty->assign( 'media_library_uri', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri', CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'application_url', $strApplicationUrl );
		$objSmarty->assign( 'terms_and_condition_path', $strSecureBaseName . '?module=miscellaneous_items&action=view_terms_and_conditions&application[id]=' . $this->getApplicationId() . '&company_application_id=' . ( int ) $intCompanyApplicationId );

		$objSmarty->assign( 'CUSTOMER_TYPE_GUARANTOR', CCustomerType::GUARANTOR );
		$objSmarty->assign( 'CUSTOMER_TYPE_RESPONSIBLE', CCustomerType::RESPONSIBLE );
		$objSmarty->assign( 'CUSTOMER_TYPE_PRIMARY', CCustomerType::PRIMARY );
		$objSmarty->assign( 'CUSTOMER_TYPE_NON_LEASING_OCCUPANT', CCustomerType::NON_LEASING_OCCUPANT );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	    CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		if( true == valObj( $objApplicantApplication, 'CApplicantApplication' ) ) {
			$objSmarty->assign( 'application_steps', $objApplicantApplication->getApplicationStepId() );
		}

		$objSmarty->assign_by_ref( 'property_office_phone_number', $objPropertyOfficePhoneNumber );
		$objSmarty->assign_by_ref( 'property_additional_phone_number', $objPropertyAdditionalPhoneNumber );

		if( 'send_reminder' == $strEmailType ) {
			$objSmarty->assign( 'is_send_reminder', true );
		}

		if( 'pre_screening_invitation' == $strEmailType ) {
			$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/applications/pre_screening_invitation_email.tpl' );
		} elseif( 'revoke_account' == $strEmailType ) {
			$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/applications/revoke_account.tpl' );
		} else {
			$strHtmlEmailOutput = $objSmarty->fetch( PATH_PHP_INTERFACES . 'Templates/Common/system_emails/applications/signin_email.tpl' );
		}

		CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
		$objSignInEmail = new CSystemEmail();
		$objSignInEmail->setPropertyId( $objProperty->getId() );
		$objSignInEmail->setCid( $objClient->getId() );
		$objSignInEmail->setSystemEmailTypeId( CSystemEmailType::PROSPECT_EMAILER );

		if( false == valStr( $strFromEmail ) && true == valArr( $arrintPsProductIds ) && ( 'application_invitation' == $strEmailType || 'send_reminder' == $strEmailType || 'revoke_account' == $strEmailType ) && true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$strFromEmail	= $objProperty->getEmailRelayAddress();
			if( false == valStr( $strFromEmail ) ) {
				$objAdminDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::ADMIN, CDatabaseUserType::JOBEXECUTOR, 1, true, 0 );

				if( true == valObj( $objAdminDatabase, CDatabase::class ) ) {
					$strFromEmail = $objProperty->prepareAndUpdateEmailRelayAddress( $objCompanyUser->getCompanyUserId(), $objDatabase, $objAdminDatabase );
					$objAdminDatabase->close();
				}
			}

			if( true == valStr( $strFromEmail ) ) {
				$strReplyToFromAddress = preg_replace( '/[^a-zA-Z0-9 s]/', '', $objProperty->getPropertyName() ) . '<' . getFormattedEmailRelayFromEmailAddress( $objProperty, $objCompanyUser, $objApplicantApplication ) . '>';
				$objSignInEmail->setReplyToEmailAddress( 'Message-ID: ' . $strReplyToFromAddress );
			}
		}

		$objSignInEmail->setFromEmailAddress( $strFromEmail );

		if( true == valStr( $strReplyToFromAddress ) ) {
			$objSignInEmail->setReplyToEmailAddress( 'Message-ID: ' . $strReplyToFromAddress );
		}

		$objSignInEmail->setToEmailAddress( $this->getUsername() );
		$objSignInEmail->setSubject( $strSubject );
		$objSignInEmail->setHtmlContent( $strHtmlEmailOutput );
		$objSignInEmail->setApplicantId( $this->getId() );

		return $objSignInEmail;
	}

	public function createCustomer( $objDatabase ) {

		$strUsername = ( 0 < strlen( $this->getUsername() ) ) ? $this->getUsername() : $this->getEmailAddress();

		$intCustomerPortalSettingCount = ( 0 < strlen( $strUsername ) ) ? CCustomerPortalSettings::fetchPreExistingCustomerCountByUsernameByCid( $strUsername, $this->getCid(), NULL, $objDatabase ) : $strUsername;

		$objCustomer = new CCustomer();
		$objCustomer->setCid( $this->getCid() );
		$objCustomer->setDontAllowLogin( 0 );
		$objCustomer->setPaymentAllowanceTypeId( CPaymentAllowanceType::ALLOW_ALL );
		$objCustomer->setBlockPosting( 0 );
		$objCustomer->setIsNetworkingEnrolled( 0 );

		$objCustomer->setCompanyName( $this->getCompanyName() );
		$objCustomer->setNamePrefix( $this->getNamePrefix() );
		$objCustomer->setNameFirst( $this->getNameFirst() );
		$objCustomer->setNameMiddle( $this->getNameMiddle() );
		$objCustomer->setNameLast( $this->getNameLast() );
		$objCustomer->setNameSuffix( $this->getNameSuffix() );
		$objCustomer->setNameMaiden( $this->getNameMaiden() );
		// $objCustomer->setNameSpouse( $this->getNameSpouse() );
		$objCustomer->setPrimaryStreetLine1( $this->getCurrentStreetLine1() );
		$objCustomer->setPrimaryStreetLine2( $this->getCurrentStreetLine2() );
		$objCustomer->setPrimaryStreetLine3( $this->getCurrentStreetLine3() );
		$objCustomer->setPrimaryCity( $this->getCurrentCity() );
		$objCustomer->setPrimaryStateCode( $this->getCurrentStateCode() );
		$objCustomer->setPrimaryPostalCode( $this->getCurrentPostalCode() );
		$objCustomer->setPrimaryCountryCode( $this->getCurrentCountryCode() );

		$objCustomer->setPreviousStreetLine1( $this->getPreviousStreetLine1() );
		$objCustomer->setPreviousStreetLine2( $this->getPreviousStreetLine2() );
		$objCustomer->setPreviousStreetLine3( $this->getPreviousStreetLine3() );
		$objCustomer->setPreviousCity( $this->getPreviousCity() );
		$objCustomer->setPreviousStateCode( $this->getPreviousStateCode() );
		$objCustomer->setPreviousPostalCode( $this->getPreviousPostalCode() );
		$objCustomer->setPreviousCountryCode( $this->getPreviousCountryCode() );

		/** $objCustomer->setPhoneNumber( $this->getPhoneNumber() );
		$objCustomer->setMobileNumber( $this->getMobileNumber() );
		$objCustomer->setWorkNumber( $this->getWorkNumber() ); */
		$objCustomer->setFaxNumber( $this->getFaxNumber() );
		/** $objCustomer->setPrimaryPhoneNumberTypeId( $this->getPrimaryPhoneNumberTypeId() );
		$objCustomer->setSecondaryPhoneNumberTypeId( $this->getSecondaryPhoneNumberTypeId() ); */

		$objCustomer->setEmailAddress( $this->getEmailAddress() );
		$objCustomer->setTaxIdTypeId( $this->getTaxIdTypeId() );
		$objCustomer->setTaxNumber( $this->getTaxNumber() );
		$objCustomer->setBirthDate( $this->getBirthDate() );
		$objCustomer->setGender( $this->getGender() );
		$objCustomer->setDlNumber( $this->getDlNumber() );
		$objCustomer->setDlStateCode( $this->getDlStateCode() );

		$objCustomer->setCompanyIdentificationTypeId( $this->m_intCompanyIdentificationTypeId );
		$objCustomer->setIdentificationNumber( $this->getIdentificationNumber() );
		$objCustomer->setIdentificationExpiration( $this->m_strIdentificationExpiration );
		$objCustomer->setStudentIdNumber( $this->m_strStudentIdNumber );
		$objCustomer->setPreferredLocaleCode( $this->getPreferredLocaleCode() );

		if( false == is_null( $this->getParentNameFirst() ) && false == is_null( $this->getParentEmailAddress() ) ) {
			$objCustomer->setParentNameFirst( $this->getParentNameFirst() );
			$objCustomer->setParentNameLast( $this->getParentNameLast() );
			$objCustomer->setParentEmailAddress( $this->getParentEmailAddress() );
		}

		$objCustomer->setNotes( trim( $this->getNotes() ) );
		if( false == is_null( $this->getLeadSourceId() ) ) {
			$objCustomer->setLeadSourceId( $this->getLeadSourceId() );
		}
		// if name is same then only set username, b'coz same username can be used by co-applicants - NRW
		if( 0 == $intCustomerPortalSettingCount && \Psi\CStringService::singleton()->strtolower( $this->getNameFirst() ) == \Psi\CStringService::singleton()->strtolower( $objCustomer->getNameFirst() ) && \Psi\CStringService::singleton()->strtolower( $this->getNameLast() ) == \Psi\CStringService::singleton()->strtolower( $objCustomer->getNameLast() ) ) {
			$objCustomer->setUsername( $this->getUsername() );
			$objCustomer->setPassword( $this->getPasswordEncrypted() );
		}

		// To integrate the Co-applicant -- NRW
		// $objCustomer->setRemotePrimaryKey( $this->getAppRemotePrimaryKey() );

		return $objCustomer;
	}

	public function createAuthenticationLog() {

		$objAuthenticationLog = new CAuthenticationLog();
		$objAuthenticationLog->setCid( $this->getCid() );
		$objAuthenticationLog->setIpAddress( getRemoteIpAddress() );
		$objAuthenticationLog->setLoginDatetime( 'NOW()' );
		$objAuthenticationLog->setApplicantId( $this->getId() );

		return $objAuthenticationLog;
	}

	public function createMessage() {

		$objMessage = new CMessage();
		$objMessage->setDefaults();
		$objMessage->setCid( $this->getCid() );
		$objMessage->setApplicantId( $this->getId() );

		return $objMessage;
	}

	public function createScheduledEmailAddressFilter() {

		$objScheduledEmailAddressFilter = new CScheduledEmailAddressFilter();
		$objScheduledEmailAddressFilter->setCid( $this->m_intCid );
		$objScheduledEmailAddressFilter->setApplicantId( $this->m_intId );
		$objScheduledEmailAddressFilter->setIsUnsubscribed( $this->m_boolIsBlockMessageCenterEmail );

		return $objScheduledEmailAddressFilter;
	}

	public function createApplicationSubsidyDetail() {

		$objApplicationSubsidyDetail = new CApplicationSubsidyDetail();
		$objApplicationSubsidyDetail->setCid( $this->getCid() );
		$objApplicationSubsidyDetail->setApplicationId( $this->getApplicationId() );
		$objApplicationSubsidyDetail->setPrimaryApplicantId( $this->getId() );

		return $objApplicationSubsidyDetail;
	}

	public function createCustomerMilitaryDetail() {

		$objCustomerMilitaryDetail = new CCustomerMilitaryDetail();
		$objCustomerMilitaryDetail->setCid( $this->getCid() );
		$objCustomerMilitaryDetail->setCustomerId( $this->getCustomerId() );

		return $objCustomerMilitaryDetail;
	}

	public function createCustomerDataVerification() {

		$objCustomerDataVerification = new CCustomerDataVerification();
		$objCustomerDataVerification->setCid( $this->getCid() );
		$objCustomerDataVerification->setApplicationId( $this->getApplicationId() );
		$objCustomerDataVerification->setCustomerId( $this->getCustomerId() );

		return $objCustomerDataVerification;
	}

	/**
	 * Get Or Fetch Functions
	 *
	 */

	public function getOrFetchCustomer( $objDatabase ) {
		if( false == valObj( $this->m_objCustomer, 'CCustomer' ) || 0 == strlen( $this->m_objCustomer->getId() ) ) {
			$this->m_objCustomer = $this->fetchCustomer( $objDatabase );
		}

		return $this->m_objCustomer;
	}

	public function getOrFetchProperty( $objDatabase ) {

		if( true == empty( $this->m_objProperty ) ) {
			$this->m_objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		}

		return $this->m_objProperty;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchCompanyIdentificationType( $objDatabase ) {
		return CCompanyIdentificationTypes::fetchCompanyIdentificationTypeByIdByCid( $this->m_intCompanyIdentificationTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCompanyIdentificationTypeByName( $strName, $objDatabase ) {
		return CCompanyIdentificationTypes::fetchCompanyIdentificationTypeByNameByCid( $strName, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantPhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId, $objDatabase ) {
		return CApplicantPhoneNumbers::fetchApplicantPhoneNumberByApplicantIdByPhoneNumberTypeIdByCid( $this->m_intId, $intPhoneNumberTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomer( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationByApplicationId( $intApplicationId, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationByApplicationIdByApplicantIdByCid( $intApplicationId, $this->getId(), $this->getCid(), $objDatabase );
	}

	// This function returns all the applications submitted after ( NOW() - $intHistoricalDays )

	public function fetchApplicationsByPropertyIdsByHistoricalDaysByApplicationStageStatusIdsByLeaseIntervalTypeIds( $arrintPropertyIds, $intHistoricalDays, $arrintApplicationStageStatusIds, $arrintLeaseIntervalTypeIds, $objDatabase, $boolIncludeDeletedAA = false, $arrintExcludeOccupancyTypes = NULL ) {
		return CApplications::fetchApplicationsByApplicantIdByPropertyIdsByHistoricalDaysByApplicationStageStatusIdsByLeaseIntervalTypeIdsByCid( $this->getId(), $arrintPropertyIds, $intHistoricalDays, $arrintApplicationStageStatusIds, $arrintLeaseIntervalTypeIds, $this->getCid(), $objDatabase, $boolIncludeDeletedAA, $arrintExcludeOccupancyTypes );
	}

	public function fetchApplicationByHistoricalDaysByLeaseIntervalTypeIds( $intHistoricalDays, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $objDatabase, $intPropertyId = NULL, $boolFetchDeniedApplications = false, $boolIncludeDeletedAA = false, $intOccupancyTypeId = NULL ) {
		return CApplications::fetchApplicationByApplicantIdByHistoricalDaysByLeaseIntervalTypeIdsByCid( $this->getId(), $intHistoricalDays, $arrintLeaseIntervalTypeIds, $arrintPropertyIds, $this->getCid(), $objDatabase, $intPropertyId, $boolFetchDeniedApplications, $boolIncludeDeletedAA = true, $intOccupancyTypeId );
	}

	public function fetchApplicationById( $intApplicationId, $objDatabase ) {
		return CApplications::fetchApplicationByIdByApplicantIdByCid( $intApplicationId, $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchAddressByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchApplicationCustomerAddressByCustomerIdByAddressTypeIdByCid( $this->getCustomerId(), $intAddressTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchAddressesByAddressTypeIds( $arrintAddressTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAddresses::createService()->fetchApplicationCustomerAddressesByCustomerIdsByAddressTypeIdsByCid( [ $this->getCustomerId() ], $arrintAddressTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchContactByContactTypeId( $intContactTypeId, $objDatabase ) {
		return CApplicantContacts::fetchApplicantContactByApplicantIdByContactTypeIdByCid( $this->m_intId, $intContactTypeId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantDetail( $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantDetails::createService()->fetchApplicantDetailByApplicantIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerIncomes( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCustomerIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerIncomesWithoutNullAmount( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCustomerIdWithoutNullAmountByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchOtherCustomerIncomes( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCustomerIdByIncomeTypeIdsByCid( $this->getCustomerId(), [ CIncomeType::OTHER_INCOME ], $this->getCid(), $objDatabase );
	}

	public function fetchAllOtherCustomerIncomes( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchAllCustomerIncomesByCustomerIdByExcludingIncomeTypeIdsByCid( $this->getCustomerId(), [ CIncomeType::CURRENT_EMPLOYER, CIncomeType::PREVIOUS_EMPLOYER ], $this->getCid(), $objDatabase );
	}

	public function fetchCustomerIncomeByIncomeTypeId( $intIncomeTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomeByIncomeTypeIdByCustomerIdByCid( $intIncomeTypeId, $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerIncomesByIncomeTypeIds( $arrintIncomeTypeIds, $objDatabase, $boolIsShowCategorizedInfo = false ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCustomerIdByIncomeTypeIdsByCid( $this->getCustomerId(), $arrintIncomeTypeIds, $this->getCid(), $objDatabase, $boolIsShowCategorizedInfo );
	}

	public function fetchCustomerIncomesByIncomeTypeIdsWithoutNullAmount( $arrintIncomeTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCustomerIdByIncomeTypeIdsWithoutNullAmountByCid( $this->getCustomerId(), $arrintIncomeTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerIncomesByIncomeTypeIdsWithoutZeroAmount( $arrintIncomeTypeIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCustomerIdByIncomeTypeIdsWithoutZeroAmountByCid( $this->getCustomerId(), $arrintIncomeTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCurrentCustomerIncome( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomeByIncomeTypeIdByCustomerIdByCid( CIncomeType::CURRENT_EMPLOYER, $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchCurrentCustomerIncomes( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomesByCustomerIdByIncomeTypeIdsByCid( $this->getCustomerId(), [ CIncomeType::CURRENT_EMPLOYER ], $this->getCid(), $objDatabase );
	}

	public function fetchApplicantPreviousEmployer( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerIncomes::createService()->fetchCustomerIncomeByIncomeTypeIdByCustomerIdByCid( CIncomeType::PREVIOUS_EMPLOYER, $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAssets( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAssets::createService()->fetchCustomerAssetsByCustomerIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAssetsWithoutZeroMarketValue( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAssets::createService()->fetchCustomerAssetsByCustomerIdWithoutZeroMarketValueByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAssetsWithoutNullMarketValue( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAssets::createService()->fetchCustomerAssetsByCustomerIdWithoutNullMarketValueByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAssetsWithNullMarketValue( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAssets::createService()->fetchCustomerAssetsByCustomerIdWithNullMarketValueByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchCustomerAssetsByCustomerAssetTypeIds( $arrintCustomerAssetTypeIds, $objDatabase, $boolIncludeDeletedCA = false, $boolIsShowCategorizedInfo = false ) {
		return \Psi\Eos\Entrata\CCustomerAssets::createService()->fetchCustomerAssetsByCustomerIdByCustomerAssetTypeIdsByCid( $this->getCustomerId(), $arrintCustomerAssetTypeIds, $this->getCid(), $objDatabase, $boolIncludeDeletedCA, $boolIsShowCategorizedInfo );
	}

// NEED FOR RENEWALS

	public function fetchApplicantAssets( $objDatabase ) {
		return CApplicantAssets::fetchApplicantAssetsByApplicantIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantAssetsWithoutNullMarketValue( $objDatabase ) {
		return CApplicantAssets::fetchApplicantAssetsByApplicantIdWithoutNullMarketValueByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchAuthenticationLogs( $objDatabase ) {
		return CAuthenticationLogs::fetchAuthenticationLogsByApplicantIdByCid( $this->m_intId, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantAssetsByApplicantAssetTypeIds( $arrintApplicantAssetTypeIds, $objDatabase ) {
		return CApplicantAssets::fetchApplicantAssetsByApplicantIdByApplicantAssetTypeIdsByCid( $this->m_intId, $arrintApplicantAssetTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchApplicantAssetsByApplicantAssetTypeIdsWithoutNullMarketValue( $arrintApplicantAssetTypeIds, $objDatabase ) {
		return CApplicantAssets::fetchApplicantAssetsByApplicantIdByApplicantAssetTypeIdsWithoutNullMarketValueByCid( $this->m_intId, $arrintApplicantAssetTypeIds, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerExpensesById( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerExpenses::createService()->fetchCustomerExpensesByCustomerIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	// This should be defined used in Applicant Application

	public function fetchForm( $objDatabase ) {
		return CForms::fetchFormByIdByCid( $this->getApplicationFormId(), $this->getCid(), $objDatabase );
	}

	// This should be defined used in Applicant Application

	public function fetchApplicationDocument( $objDatabase ) {
		return CDocuments::fetchApplicationDocumentByIdByCid( $this->getApplicationDocumentId(), $this->getCid(), $objDatabase );
	}

	public function fetchClient( $objDatabase ) {
		return CClients::fetchClientById( $this->m_intCid, $objDatabase );
	}

	public function fetchAuthenticatedApplicant( $objDatabase, $boolIsPasswordEncryptionRequired = true, $arrintPropertyIds = [] ) {
		if( true == $boolIsPasswordEncryptionRequired ) {
			$this->encryptPassword();
		}

		if( false == filter_var( $this->getUsername(), FILTER_VALIDATE_EMAIL ) ) {
			$intQuoteId = preg_replace( '/\D+/', '', $this->getUsername() );

			if( true == valId( $intQuoteId ) && 0 != $intQuoteId ) {
				$objApplicant = CApplicants::fetchApplicantByQuoteIdByPropertyIdsByPasswordByCid( $intQuoteId, $arrintPropertyIds, $this->getPasswordEncrypted(), $this->getCid(), $objDatabase );
			}

		} else {
			$objApplicant = CApplicants::fetchApplicantByUsernameByPasswordByCid( $this->getUsername(), $this->getPasswordEncrypted(), $this->getCid(), $objDatabase );
		}

		if( true == valObj( $objApplicant, 'CApplicant' ) ) {
			return $objApplicant;
		} else {
			return NULL;
		}
	}

	public function fetchApplicantApplicationsByApplicationIds( $arrintApplicationIds, $objDatabase ) {
		return \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantApplicationsByApplicantIdByApplicationIdsByCid( $this->getId(), $arrintApplicationIds, $this->getCid(), $objDatabase );
	}

	public function fetchLatestApplication( $objDatabase ) {
		return CApplications::fetchLatestApplicationByApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchLatestApplicationByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		return CApplications::fetchLatestApplicationByApplicantIdByPropertyIdsByCid( $this->getId(), $arrintPropertyIds, $this->getCid(), $objDatabase );
	}

	public function fetchCustomerPaymentAccounts( $objDatabase ) {
		return CCustomerPaymentAccounts::fetchCustomerPaymentAccountsByApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchElectronicCustomerPaymentAccounts( $objDatabase ) {
		return CCustomerPaymentAccounts::fetchElectronicCustomerPaymentAccountsByApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantPropertyIdentificationTypeByPropertyId( $intPropertyId, $objDatabase ) {
		return CPropertyIdentificationTypes::fetchPropertyIdentificationTypeByPropertyIdByCompanyIdentificationTypeIdByCid( $intPropertyId, $this->getCompanyIdentificationTypeId(), $this->getCid(), $objDatabase );
	}

	public function fetchCompanyIdentificationTypeById( $objDatabase ) {
		return CCompanyIdentificationTypes::fetchCompanyIdentificationTypeByIdByCid( $this->getCompanyIdentificationTypeId(), $this->getCid(), $objDatabase );
	}

	public function fetchApplicantApplicationChanges( $objDatabase ) {
		return CApplicantApplicationChanges::fetchApplicantApplicationChangesByApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFileSignatures( $objDatabase ) {
		return \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesByApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFilesBySystemCodes( $arrstrSystemCodes, $objDatabase ) {
		return CFiles::fetchFilesByApplicantIdBySystemCodesByCid( $this->getId(), $arrstrSystemCodes, $this->getCid(), $objDatabase );
	}

	public function fetchRequireSignFiles( $objDatabase ) {
		return CFiles::fetchRequireSignFilesByApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchBlockedApplicant( $objDatabase ) {
		return \Psi\Eos\Entrata\CScheduledEmailAddressFilters::createService()->fetchBlockedApplicantByApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchPreExistingApplicantCountByPropertyIdByHistoricaldays( $intPropertyId, $intHistoricalDays, $objDatabase ) {
		return CApplicants::fetchPreExistingApplicantCountByPropertyIdByUsernameByHistoricaldaysByCid( $this->getUsername(), $intPropertyId, $this->getId(), $intHistoricalDays, $this->getCid(), $objDatabase );
	}

	public function fetchPreExistingDeletedApplicantByApplicationId( $intApplicationId, $objDatabase ) {
		return CApplicants::fetchPreExistingDeletedApplicantByApplicationIdByUsernameByFirstAndlastNameByBirthDateByPhoneNumberByCid( $intApplicationId, $this->getUsername(), $this->getNameFirst(), $this->getNameLast(), $this->getBirthDate(), $this->getPrimaryPhoneNumber(), $this->getCid(), $objDatabase );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valNameFirst( $boolAddErrorMessage = true, $strSectionName = NULL ) {

		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'First' );

		if( false == isset( $this->m_strNameFirst ) || 0 == strlen( $this->m_strNameFirst ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( '{%s,0} name is required.', [ $strSectionName ] ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'First Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

		} else {
			if( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameFirst, 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name cannot contain @ signs or email headers.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'First Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

			} else {
				if( 1 > strlen( $this->m_strNameFirst ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', __( 'First name must have at least one letter.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'First Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valNameLast( $boolAddErrorMessage = true, $boolNameLastRequired = true ) {
		$boolIsValid = true;

		if( ( true == isset( $this->m_strNameLast ) && 0 < strlen( $this->m_strNameLast ) ) || true == $boolNameLastRequired ) {

			if( false == isset( $this->m_strNameLast ) || 0 == strlen( $this->m_strNameLast ) ) {
				$boolIsValid = false;

				if( true == $boolAddErrorMessage ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Last Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				}

			} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameLast, 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', __( 'Last name cannot contain @ signs or email headers.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'Last Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

			}
		}

		return $boolIsValid;
	}

	public function valMessage() {
		$boolIsValid = true;

		if( $this->m_strNotes != strip_tags( $this->m_strNotes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Message cannot contain HTML tag or JS code.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'Message' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valCustomerTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getCustomerTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_type_id', __( 'Occupant Type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCustomerRelationshipId() {
		$boolIsValid = true;

		if( false == valId( $this->getCustomerRelationshipId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_relationship_id', __( 'Relationship is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valParentNameFirst( $boolAddErrorMessage = true, $strSectionName = NULL ) {
		$boolIsValid = true;

		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Parent First' );

		if( false == isset( $this->m_strParentNameFirst ) || 0 == strlen( $this->m_strParentNameFirst ) ) {
			$boolIsValid = false;

			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_name_first', __( '{%s, 0} name is required.', [ $strSectionName ] ), ERROR_CODE_NAME_REQUIRE ) );
			}

		} else {
			if( true == \Psi\CStringService::singleton()->stristr( $this->m_strParentNameFirst, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strParentNameFirst, 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_name_first', __( 'Parent First name cannot contain @ signs or email headers.' ), ERROR_CODE_INVALID_NAME ) );

			} else {
				if( 1 > strlen( $this->m_strParentNameFirst ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_name_first', __( 'Parent First name must have at least one letter.' ), ERROR_CODE_INVALID_NAME ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valParentNameLast( $boolAddErrorMessage = true, $boolNameLastRequired = true ) {
		$boolIsValid = true;

		if( true == isset( $this->m_strParentNameLast ) && ( 0 < strlen( $this->m_strParentNameLast ) || true == $boolNameLastRequired ) ) {

			if( false == isset( $this->m_strParentNameLast ) || 0 == strlen( $this->m_strParentNameLast ) ) {
				$boolIsValid = false;

				if( true == $boolAddErrorMessage ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_name_last', __( 'Parent Last name is required.' ) ) );
				}

			} elseif( true == \Psi\CStringService::singleton()->stristr( $this->m_strParentNameLast, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strParentNameLast, 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_name_last', __( 'Parent Last name cannot contain @ signs or email headers.' ) ) );

			}
		}

		return $boolIsValid;
	}

	public function valParentEmail() {
		$boolIsValid = true;

		if( false == isset( $this->m_strParentEmailAddress ) || 0 == strlen( trim( $this->m_strParentEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_email_address', __( 'Parent Email address is required.' ), ERROR_CODE_EMAIL_REQUIRE ) );

			return $boolIsValid;
		} else {
			if( false == CValidation::validateEmailAddresses( $this->m_strParentEmailAddress ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_email_address', __( 'Parent Email address does not appear to be valid.' ), ERROR_CODE_INVALID_EMAIL ) );

				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		$this->setNotes( getSanitizedFormField( $this->getNotes() ) );

		return $boolIsValid;
	}

	public function valNameMaiden( $boolIsValidateRequired ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_strNameMaiden ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_maiden', __( 'Former last name is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} else {
			if( true == \Psi\CStringService::singleton()->stristr( $this->m_strNameMaiden, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strNameMaiden, 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_maiden', __( 'Former last name cannot contain @ signs or email headers.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

			} else {
				if( 2 > \Psi\CStringService::singleton()->strlen( $this->m_strNameMaiden ) && true == $boolIsValidateRequired ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_maiden', __( 'Former last name must have at least 2 letters.' ), ERROR_CODE_INVALID_NAME, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valRelationshipToApplicant( $boolRelationshipToApplicantCanBeNull = true ) {

		$boolIsValid = true;

		if( true == is_null( $this->m_strRelationshipToApplicant ) && false == $boolRelationshipToApplicantCanBeNull ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship_to_applicant', __( 'Relationship to applicant is required.' ) ) );

		} else {
			if( false == is_null( $this->m_strRelationshipToApplicant ) && ( true == \Psi\CStringService::singleton()->stristr( $this->m_strRelationshipToApplicant, '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->m_strRelationshipToApplicant, 'Content-type' ) ) ) {

				// This condition protects against email bots that are using forms to send spam.
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship_to_applicant', __( 'Relationship to applicant cannot contain @ signs or email headers.' ) ) );

			} else {
				if( false == is_null( $this->m_strRelationshipToApplicant ) && false == preg_match( '/^[a-z\s]+/i', $this->m_strRelationshipToApplicant ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship_to_applicant', __( 'Relationship to applicant cannot contain numerics or any special characters.' ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valHeight( $boolIsValidateRequired ) {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intHeight ) || true == is_null( $this->m_intHeight ) || 0 == $this->m_intHeight ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'height', __( 'Height is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valWeight( $boolIsValidateRequired ) {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intWeight ) || true == is_null( $this->m_intWeight ) || 0 == $this->m_intWeight ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weight', __( 'Weight is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valOccupation( $boolIsValidateRequired ) {
		$boolIsValid = true;

		if( ( false == isset( $this->m_intOccupationId ) || true == is_null( $this->m_intOccupationId ) || 0 == $this->m_intOccupationId ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occupation_id', __( 'Occupation is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valEyeColor( $boolIsValidateRequired ) {
		$boolIsValid = true;
		if( true == is_null( $this->m_strEyeColor ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'eye_color', __( 'Eye color is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valHairColor( $boolIsValidateRequired ) {
		$boolIsValid = true;
		if( true == is_null( $this->m_strHairColor ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hair_color', __( 'Hair color is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valMaritalStatusTypeId( $boolIsValidateRequired ) {
		$boolIsValid = true;
		if( true == is_null( $this->m_intMaritalStatusTypeId ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'marital_status_type_id', __( 'Marital Status is required.' ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( 'Identification/Personal Information' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valFaxNumber( $boolCanBeNull = false, $strSectionName = NULL ) {
		$boolIsValid = true;

		$intLength      = strlen( $this->m_strFaxNumber );
		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Fax' );

		if( false == $boolCanBeNull ) {
			if( true == is_null( $this->m_strFaxNumber ) || 0 == $intLength ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( '{%s, 0} number is required.', [ $strSectionName ] ) ) );
			}
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( '{%s, 0} number must be between 10 and 15 characters.', [ $strSectionName ] ), 504 ) );
			$boolIsValid = false;

		} elseif( true == isset( $this->m_strFaxNumber ) && false == ( CValidation::validateFullPhoneNumber( $this->m_strFaxNumber, false ) ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'fax_number', __( 'Valid {%s, 0} number is required.', [ $strSectionName ] ), 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCurrentFaxNumber( $boolCanBeNull = false, $strSectionName = NULL, $boolIsValidateRequired ) {
		$boolIsValid = true;

		if( false == $boolCanBeNull ) {
			$boolIsValid &= $this->valAdditionalInfoFaxNumber( false, $strSectionName, 'current_address_', $boolIsValidateRequired );
		} else {
			if( 0 < strlen( $this->getCurrentFaxNumber() ) ) {
				$boolIsValid &= $this->valAdditionalInfoFaxNumber( true, $strSectionName, 'current_address_', $boolIsValidateRequired );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousFaxNumber( $boolCanBeNull = false, $strSectionName = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( false == $boolCanBeNull ) {
			$boolIsValid &= $this->valAdditionalInfoFaxNumber( false, $strSectionName, 'previous_address_', $boolIsValidateRequired );
		} else {
			if( 0 < strlen( $this->getPreviousFaxNumber() ) ) {
				$boolIsValid &= $this->valAdditionalInfoFaxNumber( true, $strSectionName, 'previous_address_', $boolIsValidateRequired );
			}
		}

		return $boolIsValid;
	}

	public function valAdditionalInfoFaxNumber( $boolCanBeNull = false, $strSectionName = NULL, $strFaxNumberType = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid    = true;
		$strFaxValue    = ( 'current_address_' == $strFaxNumberType ) ? $this->getCurrentFaxNumber() : $this->getPreviousFaxNumber();
		$intLength      = \Psi\CStringService::singleton()->strlen( $strFaxValue );
		$strSectionName = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : ( 'previous_address_' == $strFaxNumberType ? __( 'Previous Address' ) : __( 'Current Address' ) );

		if( false == $boolCanBeNull && ( true == is_null( $strFaxValue ) || 0 == $intLength ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFaxNumberType . 'fax_number', __( '{%s, 0} fax number is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s, 0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFaxNumberType . 'fax_number', __( '{%s, 0} fax number must be between 10 and 15 characters.', [ $strSectionName ] ), 504 ) );

		} elseif( true == isset( $strFaxValue ) && false == ( CValidation::validateFullPhoneNumber( $strFaxValue, false ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFaxNumberType . 'fax_number', __( '{%s, 0} fax number is not valid.', [ $strSectionName ] ), 504 ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryPhoneNumber() {

		$objPrimaryPhoneNumber = $this->createPhoneNumber( $this->getPrimaryPhoneNumber() );
		$intLength             = \Psi\CStringService::singleton()->strlen( $objPrimaryPhoneNumber->getNumber() );
		$boolIsValid           = true;

		if( 0 == $intLength ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number',  __( 'Primary phone number is required.' ), ERROR_CODE_PHONE_NUMBER_REQUIRE, [ 'label' => __( 'Primary Phone' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			return false;
		}

		if( true == valStr( $objPrimaryPhoneNumber->getNumber() ) && false == $objPrimaryPhoneNumber->isValid() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Primary phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPrimaryPhoneNumber->getFormattedErrors(), [ 'region' => $objPrimaryPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPrimaryPhoneNumber->getCountryCode() ] ), ERROR_CODE_INVALID_PHONE_NUMBER, [ 'label' => __( 'Primary Phone' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valSecondaryPhoneNumber() {

		$strSecondaryPhoneNumber = $this->getSecondaryPhoneNumber();
		$intLength               = \Psi\CStringService::singleton()->strlen( $strSecondaryPhoneNumber );
		$boolIsValid             = true;
		$objSecondaryPhoneNumber = $this->createPhoneNumber( $strSecondaryPhoneNumber );

		if( true == is_null( $strSecondaryPhoneNumber ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Secondary phone number is required.' ), ERROR_CODE_PHONE_NUMBER_REQUIRE, [ 'label' => __( 'Secondary Phone' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			$boolIsValid &= false;
		}

		if( true == $objSecondaryPhoneNumber->getNumber() && false == $objSecondaryPhoneNumber->isValid() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_phone_number', __( 'Secondary phone number is not valid for {%s, region} (+{%s, country_code}).' . ' ' . $objSecondaryPhoneNumber->getFormattedErrors(), [ 'region' => $objSecondaryPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objSecondaryPhoneNumber->getCountryCode() ] ), 504, [ 'label' => __( 'Secondary Phone' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valBirthDate( $boolBirthDateIsRequired = false, $intRequiredApplicantAge = NULL, $intMaxApplicantAge = NULL, $strSectionName = 'birth_date' ) {

		$boolIsValid = true;

		if( true == isset( $this->m_strBirthDate ) && 0 < strlen( $this->m_strBirthDate ) && 1 !== CValidation::checkDate( $this->m_strBirthDate ) ) {
			$boolIsValid = false;
			$this->setBirthDate( NULL );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date must be in mm/dd/yyyy format.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

		} else {
			if( ( false == isset( $this->m_strBirthDate ) || true == is_null( $this->m_strBirthDate ) ) && true == $boolBirthDateIsRequired ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date is required.' ), ' ', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}
		}

		if( true == isset( $this->m_strBirthDate ) && 0 < strlen( $this->m_strBirthDate ) ) {
			if( CCustomerType::GUARANTOR == $this->getCustomerTypeId() && 1 == CValidation::checkDate( $this->getBirthDate() ) ) {

				$strBirthDate = $this->getBirthDate();
				if( true == isset( $strBirthDate ) && 0 < strlen( $strBirthDate ) ) {
					$arrstrBirthDate = explode( '/', $strBirthDate );
					$intYear         = $arrstrBirthDate[2];

					// Year should be four digit
					if( 4 != \Psi\CStringService::singleton()->strlen( $intYear ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth year should be full year (yyyy form).' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}
				}
			}

			if( true == $boolIsValid && false == is_null( $intRequiredApplicantAge ) && 0 != $intRequiredApplicantAge && false == $this->isFosterOrLiveInAttendant() && false == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Age should be above ' ) . ( int ) $intRequiredApplicantAge . ' ' . __( 'years.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

			if( true == $boolIsValid && false == is_null( $intMaxApplicantAge ) && 0 < $intMaxApplicantAge && false == $this->isFosterOrLiveInAttendant() && true == $this->checkApplicantAge( $intMaxApplicantAge ) && false == $this->isFosterAdult() && CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Age should be less than ' ) . ( int ) $intMaxApplicantAge . ' ' . __( 'years.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

			if( true == $boolIsValid && true == isset( $this->m_strBirthDate ) && 0 < strlen( $this->m_strBirthDate ) && strtotime( $this->m_strBirthDate ) > strtotime( date( 'm/d/Y' ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date cannot be in the future.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

			$intRequiredResponsibleApplicantAge = ( 0 != $intRequiredApplicantAge ) ? $intRequiredApplicantAge : ( ( true == valId( $intMaxApplicantAge ) ) ? $intMaxApplicantAge : self::DEFAULT_REQUIRED_APPLICANT_AGE );

			if( CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() ) {

				if( false == $this->isFosterOrLiveInAttendant() && false == $this->isFosterAdult() && true == $this->checkApplicantAge( $intRequiredResponsibleApplicantAge ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship', __( 'Applicant older than ' ) . ( int ) $intRequiredResponsibleApplicantAge . ' ' . __( 'can not be Non-Responsible.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					$boolIsValid = false;
				}

				if( true == $this->isFosterAdult() && false == $this->checkApplicantAge( $intRequiredResponsibleApplicantAge ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship', __( 'Age should be above ' ) . ( int ) $intRequiredResponsibleApplicantAge . ' ' . __( 'years.' ), '', [ 'label' => __( 'Birth Date' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					$boolIsValid = false;
				}
			}

		}

		return $boolIsValid;
	}

	public function valUsername( $objDatabase = NULL, $boolUsernameRequired = false, $boolAllowDuplicate = false, $boolUsernameConfirmRequired = false, $boolIsFromEntrata = false ) {
		$boolIsValid = true;

		if( false == CValidation::validateEmailAddresses( $this->m_strUsername ) && 0 < strlen( $this->m_strUsername ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address does not appear to be valid.' ), ERROR_CODE_INVALID_EMAIL, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

			return $boolIsValid;
		}

		if( ( false == isset( $this->m_strUsername ) && 0 < strlen( $this->m_strUsername ) ) || true == $boolUsernameRequired ) {
			if( false == isset( $this->m_strUsername ) || 0 == strlen( trim( $this->m_strUsername ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address is required.' ), ERROR_CODE_EMAIL_REQUIRE, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

				return $boolIsValid;

			} else {
				if( true == $boolUsernameConfirmRequired && true == isset( $this->m_strUsernameConfirm ) ) {
					if( 0 == strlen( trim( $this->m_strUsernameConfirm ) ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username_confirm', __( 'Confirm Email field is required.' ), ERROR_CODE_EMAIL_REQUIRE, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

						return $boolIsValid;
					}
				}
			}

			// Make sure it does not already exist in the client
			if( false == $boolAllowDuplicate && false == is_null( $objDatabase ) ) {

				$intPrexistingApplicantCount = CApplicants::fetchPreExistingApplicantCountWithPasswordByUsernameByCid( $this->getUsername(), $this->getId(), $this->getCid(), $objDatabase );

				if( 0 < $intPrexistingApplicantCount ) {
					$boolIsValid = false;
					if( false == $boolIsFromEntrata ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'We\'ve found an account using that email already. Please login with your email and password, or if you have forgotten your password you can click the "Forgot/Generate Password?" link to reset it.' ), 319, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					} else {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email is already being used.' ), 319, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}

				}
			}
		}

		if( true == $boolUsernameConfirmRequired && true == isset( $this->m_strUsernameConfirm ) && trim( $this->m_strUsername ) != trim( $this->m_strUsernameConfirm ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username_confirm', __( 'Email and confirmation email do not match.' ), '', [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validateMatchOtherApplicants( $intApplicationId, $strSSNSectionName, $objDatabase ) {
		$intExistingApplicantCount = 0;
		if( false == is_null( $this->getUsername() ) && 0 < strlen( trim( $this->getUsername() ) ) ) {
			$intExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByUsernameByCid( $intApplicationId, $this->getUsername(), $this->getCid(), $objDatabase, $this->getId(), $boolIncludeDeletedAA = false );

			if( 0 < $intExistingApplicantCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant with the same email address already exists on the current application.' ) ) );

				return false;
			}
		}

		if( false == is_null( $this->getTaxNumber() ) && 0 < strlen( trim( $this->getTaxNumber() ) ) ) {
			$boolIsTestClient = CClients::fetchIsTestClientsByClientIdByCompanyStatusTypeIds( $this->getCid(), [ CCompanyStatusType::TEST_DATABASE, CCompanyStatusType::SALES_DEMO ], $objDatabase );

			if( false == $boolIsTestClient ) {
				$intExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByTaxNumberByCid( $intApplicationId, $this->getTaxNumber(), $this->getCid(), $objDatabase, $this->getId(), $boolIncludeDeletedAA = false, $arrintExcludeOccupantTypes = [ CCustomerType::NOT_RESPONSIBLE ] );

				if( 0 < $intExistingApplicantCount ) {
					$this->setTaxNumberMasked( NULL );
					$this->setTaxNumberEncrypted( NULL );
					$this->setTaxNumber( NULL );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant already exists for current application with same ' . $strSSNSectionName . '.' ) ) );

					return false;
				}
			}
		}

		if( false == is_null( $this->getBirthDate() ) && 0 < strlen( trim( $this->getBirthDate() ) ) && 1 == CValidation::checkDate( $this->getBirthDate() ) ) {
			$intExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByFirstNameByLastNameByBirthDateByCid( $intApplicationId, $this->getNameFirst(), $this->getNameLast(), $this->getBirthDate(), $this->getCid(), $objDatabase, $this->getId(), $boolIncludeDeletedAA = false );

			if( 0 < $intExistingApplicantCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant already exists for current application with same first name, last name and birth date.' ) ) );

				return false;
			}
		}

		if( false == is_null( $this->getPrimaryPhoneNumber() ) && 0 < strlen( trim( $this->getPrimaryPhoneNumber() ) ) ) {
			$intExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByFirstNameByLastNameByPhoneNumberByCid( $intApplicationId, $this->getNameFirst(), $this->getNameLast(), $this->getPrimaryPhoneNumber(), $this->getCid(), $objDatabase, $this->getId(), $boolIncludeDeletedAA = false );

			if( 0 < $intExistingApplicantCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant already exists for current application with same first name, last name and phone number.' ) ) );

				return false;
			}
		}

		if( 0 == $intExistingApplicantCount ) {
			if( false == is_null( $this->getSecondaryPhoneNumber() ) && 0 < strlen( trim( $this->getSecondaryPhoneNumber() ) ) ) {
				$intSecondaryPhoneExistingApplicantCount = CApplicants::fetchPreExistingApplicantCountByApplicationIdByFirstNameByLastNameByPhoneNumberByCid( $intApplicationId, $this->getNameFirst(), $this->getNameLast(), $this->getSecondaryPhoneNumber(), $this->getCid(), $objDatabase, $this->getId(), $boolIncludeDeletedAA = false );
				if( 0 < $intSecondaryPhoneExistingApplicantCount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Another applicant already exists for current application with same first name, last name and phone number.' ) ) );

					return false;
				}
			}
		}

		return true;
	}

	public function valIlsGuestCardUsername() {
		$boolIsValid = true;

		if( false == isset( $this->m_strUsername ) || 0 == strlen( trim( $this->m_strUsername ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email is required.' ), ERROR_CODE_EMAIL_REQUIRE ) );

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPassword( $boolPasswordConfirmRequired = true, $boolPasswordRequired = true ) {
		$boolIsValid = true;

		if( 0 < strlen( $this->getPassword() ) || true == $boolPasswordRequired ) {

			if( true == is_null( $this->getPassword() ) || 0 == strlen( $this->getPassword() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password is required.' ) ) );
			}

			if( true == $boolIsValid && true == isset( $this->m_strPassword ) && 6 > strlen( $this->m_strPassword ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password must be at least 6 characters long.' ) ) );
			}

			if( true == $boolIsValid && true == $boolPasswordConfirmRequired ) {
				if( true == is_null( $this->m_strPasswordConfirm ) || 0 == strlen( $this->m_strPasswordConfirm ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Confirm Password is required.' ) ) );

				} else {
					if( trim( $this->m_strPassword ) != trim( $this->m_strPasswordConfirm ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', __( 'Password and confirmation password do not match.' ) ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valPropertyId() {

		$boolIsValid = true;

		if( false == isset( $this->m_intPropertyId ) || 1 > ( $this->m_intPropertyId = ( int ) $this->m_intPropertyId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCurrentStreetLine1() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCurrentStreetLine2() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCurrentStreetLine3() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCurrentCity() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCurrentStateCode() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCurrentPostalCode( $strSectionName = NULL ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->ucwords( $strSectionName ) : __( 'Current' );

		if( 0 == strlen( $this->getCurrentPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_postal_code', __( '{%s, 0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '' ) );
		} else {
			if( false == CValidation::validatePostalCode( $this->getCurrentPostalCode() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_postal_code', __( 'Invalid {%s, 0} zip/postal code.', [ $strSectionNameToDisplay ] ), '' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentCountryCode() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPreviousStreetLine1() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPreviousStreetLine2() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPreviousStreetLine3() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPreviousCity() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPreviousStateCode() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPreviousPostalCode( $strSectionName = NULL ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? \Psi\CStringService::singleton()->ucwords( $strSectionName ) : __( 'Previous' );

		if( 0 == strlen( $this->getPreviousPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( '{%s, 0} zip/postal code is required.', [ $strSectionNameToDisplay ] ), '' ) );

		} else {
			if( false == CValidation::validatePostalCode( $this->getPreviousPostalCode() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_postal_code', __( 'Invalid {%s, 0} zip/postal code.', [ $strSectionNameToDisplay ] ), '' ) );

			}
		}

		return $boolIsValid;
	}

	public function valPreviousCountryCode() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valPrimaryVehicleMake() {
		$boolIsValid = true;
		if( false == is_null( $this->getPrimaryVehicleModel() ) || false == is_null( $this->getPrimaryVehicleMake() ) || false == is_null( $this->getPrimaryVehicleYear() ) || false == is_null( $this->getPrimaryVehicleColor() ) || false == is_null( $this->getPrimaryVehicleLicensePlateNumber() ) || false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getPrimaryVehicleMake() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_make', __( 'Primary vehicle make is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPrimaryVehicleModel() {
		$boolIsValid = true;
		if( false == is_null( $this->getPrimaryVehicleModel() ) || false == is_null( $this->getPrimaryVehicleMake() ) || false == is_null( $this->getPrimaryVehicleYear() ) || false == is_null( $this->getPrimaryVehicleColor() ) || false == is_null( $this->getPrimaryVehicleLicensePlateNumber() ) || false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getPrimaryVehicleModel() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_model', __( 'Primary vehicle model is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPrimaryVehicleYear() {
		$boolIsValid = true;

		if( false == is_null( $this->getPrimaryVehicleModel() ) || false == is_null( $this->getPrimaryVehicleMake() ) || false == is_null( $this->getPrimaryVehicleYear() ) || false == is_null( $this->getPrimaryVehicleColor() ) || false == is_null( $this->getPrimaryVehicleLicensePlateNumber() ) || false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getPrimaryVehicleYear() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_year', __( 'Primary vehicle year is required.' ) ) );
			}
		}

		if( 0 < strlen( $this->getPrimaryVehicleYear() ) && ( 4 > \Psi\CStringService::singleton()->strlen( $this->getPrimaryVehicleYear() ) || 1900 > $this->getPrimaryVehicleYear() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_year', __( 'Primary vehicle year is not valid. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryVehicleColor() {
		$boolIsValid = true;
		if( false == is_null( $this->getPrimaryVehicleModel() ) || false == is_null( $this->getPrimaryVehicleMake() ) || false == is_null( $this->getPrimaryVehicleYear() ) || false == is_null( $this->getPrimaryVehicleColor() ) || false == is_null( $this->getPrimaryVehicleLicensePlateNumber() ) || false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getPrimaryVehicleColor() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_color', __( 'Primary vehicle color is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPrimaryVehicleLicensePlateNumber() {
		$boolIsValid = true;

		if( false == is_null( $this->getPrimaryVehicleModel() ) || false == is_null( $this->getPrimaryVehicleMake() ) || false == is_null( $this->getPrimaryVehicleYear() ) || false == is_null( $this->getPrimaryVehicleColor() ) || false == is_null( $this->getPrimaryVehicleLicensePlateNumber() ) || false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getPrimaryVehicleLicensePlateNumber() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_license_plate_number', __( 'Primary vehicle license plate number is required.' ) ) );
			}

		}

		return $boolIsValid;
	}

	public function valPrimaryVehicleStateCode() {
		$boolIsValid = true;

		if( false == is_null( $this->getPrimaryVehicleColor() ) || false == is_null( $this->getPrimaryVehicleModel() ) || false == is_null( $this->getPrimaryVehicleMake() ) || false == is_null( $this->getPrimaryVehicleYear() ) || false == is_null( $this->getPrimaryVehicleColor() ) || false == is_null( $this->getPrimaryVehicleLicensePlateNumber() ) || false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) || false == is_null( $this->getSecondaryVehicleStateCode() ) ) {
			if( true == is_null( $this->getPrimaryVehicleStateCode() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_state_code', __( 'Primary vehicle state/province code is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valPrimaryVehicleProvince() {
		$boolIsValid = true;

		if( true == is_null( $this->getPrimaryVehicleProvince() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_province', __( 'Primary vehicle state/province is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSecondaryVehicleMake() {
		$boolIsValid = true;
		if( false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getSecondaryVehicleMake() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_vehicle_make', __( 'Secondary vehicle make is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valSecondaryVehicleModel() {
		$boolIsValid = true;
		if( false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getSecondaryVehicleModel() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_vehicle_model', __( 'Secondary vehicle model is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valSecondaryVehicleYear() {
		$boolIsValid = true;

		if( false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getSecondaryVehicleYear() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_vehicle_year', __( 'Secondary vehicle year is required.' ) ) );
			}
		}

		if( 0 < strlen( $this->getSecondaryVehicleYear() ) && 4 > \Psi\CStringService::singleton()->strlen( $this->getSecondaryVehicleYear() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_vehicle_year', __( 'Secondary vehicle year is not valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSecondaryVehicleColor() {
		$boolIsValid = true;

		if( false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
			if( true == is_null( $this->getSecondaryVehicleColor() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_vehicle_color', __( 'Secondary vehicle make is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valSecondaryVehicleLicensePlateNumber() {
		$boolIsValid = true;

		if( false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {

			if( true == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_vehicle_license_plate_number', __( 'Secondary vehicle license plate number is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valSecondaryVehicleStateCode() {
		$boolIsValid = true;

		if( false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleModel() ) || false == is_null( $this->getSecondaryVehicleMake() ) || false == is_null( $this->getSecondaryVehicleYear() ) || false == is_null( $this->getSecondaryVehicleColor() ) || false == is_null( $this->getSecondaryVehicleLicensePlateNumber() ) || false == is_null( $this->getSecondaryVehicleStateCode() ) ) {
			if( true == is_null( $this->getSecondaryVehicleStateCode() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'secondary_vehicle_state_code', __( 'Secondary vehicle state/province code is required' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valFirstNameLastNameBirthDate( $objDatabase, $arrobjPropertyApplicationPreferences, $boolIsFromResidentWorks = false, $arrobjPropertyPreferences, $boolCheckBirthDate = true, $boolIsAllowDuplicateEmail = true, $boolIsFromEntrata = false ) {

		$boolIsValid = true;

		if( 0 < strlen( $this->getNameFirst() ) || 0 < strlen( $this->getNameLast() ) || 0 < strlen( $this->getBirthDate() ) || 0 < strlen( $this->getUsername() ) || true == $boolIsFromResidentWorks ) {
			$boolIsValid &= $this->valNameFirst();
			$boolIsValid &= $this->valNameLast();

			if( true == valArr( $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
				$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
			} else {
				$intRequiredApplicantAge = 18;
			}

			if( false == $boolIsFromResidentWorks && true == array_key_exists( 'REQUIRE_BASIC_INFO_MIDDLE_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNameMiddle();
			}

			$intCustomerType = ( false == is_null( $this->getCustomerTypeId() ) ) ? $this->getCustomerTypeId() : $this->getRelationship();

			// for the co-applicant Not checking Date
			if( true == $boolCheckBirthDate ) {
				if( CCustomerType::RESPONSIBLE == $intCustomerType || CCustomerType::NON_LEASING_OCCUPANT == $intCustomerType ) {
					$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge );
				} elseif( CCustomerType::NOT_RESPONSIBLE == $intCustomerType ) {
					$boolIsValid &= $this->valBirthDate( true, NULL, $intRequiredApplicantAge, $strSectionName = 'birth_date' );
				} elseif( CCustomerType::GUARANTOR == $intCustomerType ) {
					$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge, $strSectionName = 'birth_date' );
				}
			} else {
				$boolIsValid &= $this->valBirthDate( false, NULL, $intRequiredApplicantAge, $strSectionName = 'birth_date' );
			}

			if( ( true == $boolIsFromResidentWorks ) && ( CCustomerType::PRIMARY == $intCustomerType || ( true == is_null( $intCustomerType ) ) ) ) {
				if( ( true == isset( $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL'] ) && 'PHONE_AND_EMAIL' == $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue() )
				 || ( true == isset( $this->m_arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL'] )
				  && 'PHONE_OR_EMAIL' == $this->m_arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue() && true == is_null( $this->getPrimaryPhoneNumber() ) ) ) {
					$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail, true, $boolIsFromEntrata );
				} else {
					$boolIsValid &= $this->valUsername( $objDatabase, false, $boolIsAllowDuplicateEmail, true, $boolIsFromEntrata );
				}
			} else {
				if( 1 != $this->getIsDelete() ) {
					$boolUsernameRequired = false;

					if( ( true == $boolIsFromResidentWorks && true == isset( $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL'] )
					  && 'PHONE_AND_EMAIL' == $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue() && CCustomerType::NOT_RESPONSIBLE != $intCustomerType )
					 || ( true == isset( $this->m_arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL'] )
					  && 'PHONE_OR_EMAIL' == $this->m_arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue() && true == is_null( $this->getPrimaryPhoneNumber() ) ) || false == $boolIsFromResidentWorks ) {
						$boolUsernameRequired = true;
					}

					if( CCustomerType::GUARANTOR == $this->getCustomerTypeId() ) {
						$boolIsValid &= $this->valUsername( $objDatabase, $boolUsernameRequired, $boolIsAllowDuplicateEmail, true, $boolIsFromEntrata );
					} else {
						if( false == is_null( $this->getBirthDate() ) && true == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
							$boolIsValid &= $this->valUsername( $objDatabase, $boolUsernameRequired, $boolIsAllowDuplicateEmail, true, $boolIsFromEntrata );

						} else {
							if( false == is_null( $this->getBirthDate() ) ) {
								$boolIsValid &= $this->valUsername( $objDatabase, false, $boolIsAllowDuplicateEmail, true, $boolIsFromEntrata );
							} else {
								$boolIsValid &= $this->valUsername( $objDatabase, $boolUsernameRequired, $boolIsAllowDuplicateEmail, true, $boolIsFromEntrata );
							}
						}
					}
				}
			}

			$boolValidateRelationShip = true;

			if( true == in_array( $intCustomerType, [ CCustomerType::RESPONSIBLE, CCustomerType::NOT_RESPONSIBLE ] ) && true == valArr( $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_PEOPLE_CO_APPLICANT_RELATIONSHIP', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_RELATIONSHIP']->getValue() ) ) {
				$boolIsValid &= $this->valRelationship();
			}
		}

		if( 0 == strlen( $this->getNameFirst() ) && 0 == strlen( $this->getNameLast() ) && 0 == strlen( $this->getBirthDate() ) && 0 == strlen( $this->getUsername() ) ) {
			$this->setIsBlanked( true );
		} else {
			$this->setIsBlanked( false );
		}

		return $boolIsValid;
	}

	public function valFirstNameLastNameBirthDateUsername( $objDatabase, $intApplicationId, $arrobjPropertyApplicationPreferences ) {

		$boolIsValid = true;

		if( 0 < strlen( $this->getNameFirst() ) || 0 < strlen( $this->getNameLast() ) || 0 < strlen( $this->getBirthDate() ) || 0 < strlen( $this->getUsername() ) ) {

			$boolIsValid &= $this->valNameFirst();
			$boolIsValid &= $this->valNameLast();

			$intCustomerType = ( false == is_null( $this->getCustomerTypeId() ) ) ? $this->getCustomerTypeId() : $this->getRelationship();

			if( CCustomerType::RESPONSIBLE == $intCustomerType && true == array_key_exists( 'REQUIRE_PEOPLE_CO_APPLICANT_RELATIONSHIP', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_RELATIONSHIP']->getValue() ) ) {

				$objApplication               = CApplications::fetchApplicationByIdByCid( $intApplicationId, $this->getCid(), $objDatabase );
				$intCustomerRelationshipCount = CCustomerRelationships::fetchPublishedCustomerRelationshipsCountByPropertyIdByCustomerTypeIdsByCid( $objApplication->getPropertyId(), [ CCustomerType::RESPONSIBLE ], $this->getCid(), $objDatabase );

				if( 0 < $intCustomerRelationshipCount ) {
					$boolIsValid &= $this->valRelationship();
				}
			}

			if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
				$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
			} else {
				$intRequiredApplicantAge = 18;
			}

			if( CCustomerType::RESPONSIBLE == $intCustomerType ) {

				$boolIsValid &= $this->valBirthDate( true, 18 );

			} elseif( CCustomerType::NOT_RESPONSIBLE == $intCustomerType ) {

				$boolIsValid &= $this->valBirthDate( true, NULL, 18 );

			} elseif( CCustomerType::GUARANTOR == $intCustomerType ) {

				$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge );

			} else {
				$boolIsValid &= $this->valBirthDate( true );
			}

			if( 1 != $this->getIsDelete() ) {

				if( CCustomerType::GUARANTOR == $intCustomerType ) {
					$boolIsValid &= $this->valUsername( $objDatabase, true, true );

				} else {
					if( false == is_null( $this->getBirthDate() ) && true == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
						$boolIsValid &= $this->valUsername( $objDatabase, true, true );

					} else {
						if( false == is_null( $this->getBirthDate() ) ) {
							$boolIsValid &= $this->valUsername( $objDatabase, false, true );
						}
					}
				}
			}
		}

		if( 0 == strlen( $this->getNameFirst() ) && 0 == strlen( $this->getNameLast() ) && 0 == strlen( $this->getBirthDate() ) && 0 == strlen( $this->getUsername() ) ) {
			$this->setIsBlanked( true );
		} else {
			$this->setIsBlanked( false );
		}

		return $boolIsValid;
	}

	public function valFirstNameLastName( $objDatabase, $intApplicationId, $arrobjPropertyApplicationPreferences ) {

		$boolIsValid = true;

		if( 0 < strlen( $this->getNameFirst() ) || 0 < strlen( $this->getNameLast() ) ) {

			$boolIsValid &= $this->valNameFirst();
			$boolIsValid &= $this->valNameLast();

			$intCustomerType = ( false == is_null( $this->getCustomerTypeId() ) ) ? $this->getCustomerTypeId() : $this->getRelationship();

			if( CCustomerType::RESPONSIBLE == $intCustomerType && true == array_key_exists( 'REQUIRE_PEOPLE_CO_APPLICANT_RELATIONSHIP', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_RELATIONSHIP']->getValue() ) ) {

				$objApplication               = CApplications::fetchApplicationByIdByCid( $intApplicationId, $this->getCid(), $objDatabase );
				$intCustomerRelationshipCount = CCustomerRelationships::fetchPublishedCustomerRelationshipsCountByPropertyIdByCustomerTypeIdsByCid( $objApplication->getPropertyId(), [ CCustomerType::RESPONSIBLE ], $this->getCid(), $objDatabase );

				if( 0 < $intCustomerRelationshipCount ) {
					$boolIsValid &= $this->valRelationship();
				}
			}
		}

		if( 0 == strlen( $this->getNameFirst() ) && 0 == strlen( $this->getNameLast() ) && 0 == strlen( $this->getBirthDate() ) && 0 == strlen( $this->getUsername() ) ) {
			$this->setIsBlanked( true );
		} else {
			$this->setIsBlanked( false );
		}

		return $boolIsValid;
	}

	public function valCurrentAddressResidenceType( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCurrentCompanyResidenceTypeId() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_company_residence_type_id', __( '{%s, 0} residence type is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_company_residence_type_id', __( 'Current address residence type is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressReasonForLeaving( $intPropertyId, $objDatabase, $strSectionName = NULL ) {

		$boolIsValid            = true;
		$intMoveOutReasonsCount = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemsCountByPropertyIdByCIdByListTypeId( $intPropertyId, $this->getCid(), $objDatabase, CListType::MOVE_OUT );

		if( ( 0 < $intMoveOutReasonsCount && true == is_null( $this->getCurrentMoveOutReasonListItemId() ) ) || ( 0 == $intMoveOutReasonsCount && true == is_null( $this->getCurrentReasonForLeaving() ) ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_reason_for_leaving', __( '{%s, 0} reason for leaving is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_reason_for_leaving', __( 'Current address reason for leaving is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressAddressOwnerType( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( CCustomerAddress::ADDRESS_OWNER_TYPE_UNKNOWN == $this->getCurrentAddressOwnerType() ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_address_owner_type', $strSectionName . ' owned/rented is required.', '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_address_owner_type', 'Current address owned/rented is required..', '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressCommunityName( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCurrentCommunityName() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_community_name', __( '{%s, 0} community name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_community_name', __( 'Current address community name is required..' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressManagerNameFirst( $strSectionName = NULL, $boolIsValidateRequired = true, $boolIsRequired = true, $boolCheckLength = true ) {
		$boolIsValid = true;
		if( true == $boolIsRequired && 0 == strlen( $this->getCurrentManagerNameFirst() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_first', __( '{%s, 0} manager first name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_first', __( 'Current address manager first name is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		} else {
			if( true == \Psi\CStringService::singleton()->stristr( $this->getCurrentManagerNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getCurrentManagerNameFirst(), 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_first', __( 'Names cannot contain @ signs or email headers.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				if( true == $boolCheckLength && false == is_null( $this->getCurrentManagerNameFirst() ) && 2 > \Psi\CStringService::singleton()->strlen( $this->getCurrentManagerNameFirst() ) ) {
					if( true == $boolIsValidateRequired ) {
						$boolIsValid = false;
					}
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_first', __( 'Current address Manager first name must have at least 2 letters.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressManagerNameLast( $strSectionName = NULL, $boolIsValidateRequired = true, $boolIsRequired = true, $boolCheckLength = true ) {

		$boolIsValid = true;
		if( true == $boolIsRequired && 0 == strlen( $this->getCurrentManagerNameLast() ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_last', __( '{%s, 0} manager last name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_last', __( 'Current address manager last name is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		} else {
			if( true == \Psi\CStringService::singleton()->stristr( $this->getCurrentManagerNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getCurrentManagerNameLast(), 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_last', __( 'Names cannot contain @ signs or email headers.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

			} else {
				if( true == $boolCheckLength && false == is_null( $this->getCurrentManagerNameLast() ) && 2 > \Psi\CStringService::singleton()->strlen( $this->getCurrentManagerNameLast() ) ) {
					if( true == $boolIsValidateRequired ) {
						$boolIsValid = false;
					}
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_manager_name_last', __( 'Current address Manager last name must have at least 2 letters.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressPhoneNumber( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequired = true ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName :  'Current address';
		$objCurrentPhoneNumber = $this->createPhoneNumber( $this->getCurrentPhoneNumber() );

		if( true == $boolIsRequired && ( 0 == strlen( $this->getCurrentPhoneNumber() ) || false == valStr( $objCurrentPhoneNumber->getNumber() ) ) ) {
			if( true == $boolIsValidateRequired ) {
				$boolIsValid = false;
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_phone_number', __( '{%s, 0} phone number is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		} elseif( true == valStr( $objCurrentPhoneNumber->getNumber() && false == $objCurrentPhoneNumber->isValid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_phone_number', __( '{%s,sectionName} phone number is not valid for {%s, region} (+{%s, country_code}).' . ' ' . $objCurrentPhoneNumber->getFormattedErrors(), [ 'region' => $objCurrentPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objCurrentPhoneNumber->getCountryCode(), 'sectionName' => $strSectionNameToDisplay ] ), 504, [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCurrentAddressEmail( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequired ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->getCurrentEmailAddress() ) && true == $boolIsValidateRequired ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_email', __( '{%s, 0} email is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_email', __( 'Current address email is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getCurrentEmailAddress() ) && false == CValidation::validateEmailAddress( $this->getCurrentEmailAddress() ) ) {
			$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current address';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_email', __( '{%s, 0} valid email is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMoveInDate( $boolIsRequired = false, $strSectionName = NULL ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Current address' );

		if( true == $boolIsRequired && 0 == strlen( $this->getCurrentMoveInDate() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( 'Current address move-in date is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getCurrentMoveInDate() ) && 1 != CValidation::checkDate( $this->getCurrentMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', ( ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current Address' ) . ' move-in date must be in mm/dd/yyyy form.', '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( 0 < valStr( $this->getCurrentMoveInDate() ) && strtotime( $this->getCurrentMoveInDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', ( ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current Address' ) . ' move-in date cannot be greater than current date.', '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMoveOutDate( $boolIsRequired = false, $strSectionName = NULL ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->getCurrentMoveOutDate() ) ) {

			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', __( '{%s,0} move-out date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', __( 'Current address move-out date is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getCurrentMoveOutDate() ) && 1 != CValidation::checkDate( $this->getCurrentMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_out_date', ( ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Current Address' ) . ' move-out date must be in mm/dd/yyyy form.', '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( true == $boolIsValid && 0 != strlen( $this->getCurrentMoveInDate() && 0 != strlen( $this->getCurrentMoveOutDate() ) ) ) {
			if( ( strtotime( $this->getCurrentMoveOutDate() ) ) <= ( strtotime( $this->getCurrentMoveInDate() ) ) ) {
				$boolIsValid &= false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( '{%s,0} move-in date must be less than move-out date.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_move_in_date', __( 'Current address move-in date must be less than move-out date.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMonthlyPaymentAmount( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCurrentMonthlyPaymentAmount() ) || 0 > $this->getCurrentMonthlyPaymentAmount() ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_amount', __( '{%s,0} monthly payment valid amount is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_amount', __( 'Current address monthly payment valid amount is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddressMonthlyPaymentRecipient( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getCurrentPaymentRecipient() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_recipient', __( '{%s,0} monthly payment recipient is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_address_monthly_payment_recipient', __( 'Current address monthly payment recipient is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valCurrentAddress( $strSectionName = NULL, $strStepName = NULL ) {

		$boolIsValid = true;

		if( 'addl_info' == $strStepName ) {
			$boolIsNonUsAddress = ( bool ) $this->m_intCurrentIsNonUsAddress;
		} else {
			$boolIsNonUsAddress = ( bool ) $this->m_intIsAlien;
		}

		if( 0 == strlen( $this->m_strCurrentStreetLine1 ) && 0 == strlen( $this->m_strCurrentStreetLine2 ) ) {
			$boolIsValid = false;
			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line1', __( '{%s,0} street is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				if( 0 < $this->getCurrentIsNonUsAddress() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line1', __( 'Current address is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_street_line1', __( 'Current street is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		if( false == $boolIsNonUsAddress ) {

			if( 0 == strlen( $this->m_strCurrentCity ) ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_city', __( '{%s,0} city is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_city', __( 'Current city is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			$boolIsValid &= $this->valCurrentPostalCode( $strSectionName );

			if( 0 == strlen( $this->m_strCurrentStateCode ) ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_state_code', __( '{%s, 0} state/province code is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_state_code', __( 'Current state/province code is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressResidenceType( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPreviousCompanyResidenceTypeId() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_company_residence_type_id', __( '{%s,0} residence type is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_company_residence_type_id', __( 'Previous Address residence type is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressReasonForLeaving( $intPropertyId, $objDatabase, $strSectionName = NULL ) {

		$boolIsValid            = true;
		$intMoveOutReasonsCount = \Psi\Eos\Entrata\CListItems::createService()->fetchListItemsCountByPropertyIdByCIdByListTypeId( $intPropertyId, $this->getCid(), $objDatabase, CListType::MOVE_OUT );

		if( ( 0 < $intMoveOutReasonsCount && true == is_null( $this->getPreviousMoveOutReasonListItemId() ) ) || ( 0 == $intMoveOutReasonsCount && true == is_null( $this->getPreviousReasonForLeaving() ) ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_reason_for_leaving', __( '{%s,0} reason for leaving is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_reason_for_leaving', __( 'Previous address reason for leaving is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressAddressOwnerType( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( CCustomerAddress::ADDRESS_OWNER_TYPE_UNKNOWN == $this->getPreviousAddressOwnerType() ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_address_owner_type', $strSectionName . ' owned/rented is required.', '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_address_owner_type', 'Previous address owned/rented is required..', '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressCommunityName( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPreviousCommunityName() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_community_name', __( '{%s,0} community name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_community_name', __( 'Previous address community name is required..' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressManagerNameFirst( $strSectionName = NULL, $boolIsValidateRequired = true, $boolIsRequired = true, $boolCheckLength = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->getPreviousManagerNameFirst() ) && true == $boolIsValidateRequired ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_first', __( '{%s,0} manager first name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_first', __( 'Previous address manager first name is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		} else {
			if( true == \Psi\CStringService::singleton()->stristr( $this->getPreviousManagerNameFirst(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getPreviousManagerNameFirst(), 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_first', __( 'Names cannot contain @ signs or email headers.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

			} else {
				if( true == $boolCheckLength && false == is_null( $this->getPreviousManagerNameFirst() ) && 2 > strlen( $this->getPreviousManagerNameFirst() ) ) {
					if( true == $boolIsValidateRequired ) {
						$boolIsValid = false;
					}
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_first', __( 'Previous address manager first name must have at least 2 letters.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressManagerNameLast( $strSectionName = NULL, $boolIsValidateRequired = true, $boolIsRequired = true, $boolCheckLength = true ) {

		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->getPreviousManagerNameLast() ) && true == $boolIsValidateRequired ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_last', __( '{%s,0} manager last name is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_last', __( 'Previous address manager last name is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		} else {
			if( true == \Psi\CStringService::singleton()->stristr( $this->getPreviousManagerNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getPreviousManagerNameLast(), 'Content-type' ) ) {

				// This condition protects against email bots that are using forms to send spam.
				if( true == $boolIsValidateRequired ) {
					$boolIsValid = false;
				}
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_last', __( 'Names cannot contain @ signs or email headers.' ) ) );

			} else {
				if( true == $boolCheckLength && false == is_null( $this->getPreviousManagerNameLast() ) && 2 > \Psi\CStringService::singleton()->strlen( $this->getPreviousManagerNameLast() ) ) {
					if( true == $boolIsValidateRequired ) {
						$boolIsValid = false;
					}
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_manager_name_last', __( 'Previous address manager last name must have at least 2 letters.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressPhoneNumber( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequired ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous address';
		$objPreviousPhoneNumber = $this->createPhoneNumber( $this->getPreviousPhoneNumber() );

		if( true == $boolIsRequired && ( 0 == strlen( $this->getPreviousPhoneNumber() ) || false == valStr( $objPreviousPhoneNumber->getNumber() ) ) && true == $boolIsValidateRequired ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_phone_number', __( '{%s,0} phone number is required.', [ $strSectionNameToDisplay ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		} elseif( true == valStr( $objPreviousPhoneNumber->getNumber() && false == $objPreviousPhoneNumber->isValid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_phone_number', __( '{%s,sectionName} phone number is not valid for {%s, region} (+{%s, country_code}).' . ' ' . $objPreviousPhoneNumber->getFormattedErrors(), [ 'region' => $objPreviousPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPreviousPhoneNumber->getCountryCode(), 'sectionName' => $strSectionNameToDisplay ] ), 504, [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPreviousAddressEmail( $boolIsRequired = false, $strSectionName = NULL, $boolIsValidateRequired = true ) {
		$boolIsValid = true;

		if( true == $boolIsRequired && 0 == strlen( $this->getPreviousEmailAddress() ) && true == $boolIsValidateRequired ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_email', __( '{%s,0} email is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_email', __( 'Previous address email is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getPreviousEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->getPreviousEmailAddress() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_email', ( ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous Address' ) . ' valid email is required.', '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMoveInDate( $boolIsRequired = false, $strSectionName = NULL ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Previous address' );

		if( true == $boolIsRequired && 0 == strlen( $this->getPreviousMoveInDate() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( 'Previous address move-in date required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getPreviousMoveInDate() ) && 1 !== CValidation::checkDate( $this->getPreviousMoveInDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', ( ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous Address' ) . ' move-in date must be in mm/dd/yyyy form.', '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( 0 < valStr( $this->getPreviousMoveInDate() ) && strtotime( $this->getPreviousMoveInDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', ( ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous Address' ) . ' move-in date cannot be greater than current date.', '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMoveOutDate( $boolIsRequired = false, $strSectionName = NULL, $strCurrentSectionName = 'current' ) {

		$boolIsValid = true;
		$strSectionNameToDisplay = ( 0 < strlen( $strSectionName ) ) ? $strSectionName : __( 'Previous address' );

		if( true == $boolIsRequired && 0 == strlen( $this->getPreviousMoveOutDate() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( '{%s,0} move-out date is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( 'Previous address move-out date is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}

		} elseif( 0 < strlen( $this->getPreviousMoveOutDate() ) && 1 !== CValidation::checkDate( $this->getPreviousMoveOutDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', ( ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous Address' ) . ' move-out date must be in mm/dd/yyyy form.', '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );

		} elseif( strtotime( $this->getPreviousMoveOutDate() ) > strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', ( ( 0 < strlen( $strSectionName ) ) ? $strSectionName : 'Previous Address' ) . ' move-out date cannot be greater than current date.', '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( 0 != strlen( $this->getCurrentMoveInDate() ) && 0 != strlen( $this->getPreviousMoveOutDate() ) ) {
			if( ( strtotime( $this->getCurrentMoveInDate() ) ) < ( strtotime( $this->getPreviousMoveOutDate() ) ) ) {
				$boolIsValid &= false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( '{%s,0} move-out date must be less than {%s,1} move-in date.', [ $strSectionName, $strCurrentSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_out_date', __( 'Previous move-out date must be less than {%s,0} move-in date.', [ $strCurrentSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		if( true == $boolIsValid && 0 != strlen( $this->getPreviousMoveInDate() && 0 != strlen( $this->getPreviousMoveOutDate() ) ) ) {
			if( ( strtotime( $this->getPreviousMoveOutDate() ) ) <= ( strtotime( $this->getPreviousMoveInDate() ) ) ) {
				$boolIsValid &= false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( '{%s,0} move-in date must be less than move-out date.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_move_in_date', __( 'Previous move-in date must be less than move-out date.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMonthlyPaymentAmount( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPreviousMonthlyPaymentAmount() ) || 0 > $this->getPreviousMonthlyPaymentAmount() ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_amount', __( '{%s,0} monthly payment valid amount is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_amount', __( 'Previous address monthly payment valid amount is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressMonthlyPaymentRecipient( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPreviousPaymentRecipient() ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_recipient', __( '{%s,0} monthly payment recipient is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_address_monthly_payment_recipient', __( 'Previous address monthly payment recipient is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddress( $strSectionName = NULL, $strStepName = NULL ) {

		$boolIsValid = true;

		if( 'addl_info' == $strStepName ) {
			$boolIsNonUsAddress = ( bool ) $this->m_intPreviousIsNonUsAddress;
		} else {
			$boolIsNonUsAddress = ( bool ) $this->m_intIsAlien;
		}

		if( 0 == strlen( $this->m_strPreviousStreetLine1 ) && 0 == strlen( $this->m_strPreviousStreetLine2 ) ) {
			$boolIsValid = false;

			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( '{%s,0} street is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			} else {
				if( 0 < $this->getPreviousIsNonUsAddress() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( 'Previous address is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_street_line1', __( 'Previous street is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		if( false == $boolIsNonUsAddress ) {

			if( 0 == strlen( $this->m_strPreviousCity ) ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_city', __( '{%s,0} city is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_city', __( 'Previous city is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			$boolIsValid &= $this->valPreviousPostalCode( $strSectionName );

			if( 0 == strlen( $this->m_strPreviousStateCode ) ) {
				$boolIsValid = false;

				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_state_code', __( '{%s,0} state/province code is required.', [ $strSectionName ] ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_state_code', __( 'Previous state/province code is required.' ), '', [ 'label' => __( 'Previous Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valGender( $boolIsFromRoommateInterestTab = false ) {

		$boolIsValid = true;

		if( true == is_null( $this->getGender() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'gender', __( 'Gender is required.' ), '', [ 'label' => ( $boolIsFromRoommateInterestTab ) ? __( 'Interest-Gender' ) : __( 'Gender' ), 'step_id' => ( $boolIsFromRoommateInterestTab ) ? CApplicationStep::ROOMMATES : CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valNamePrefix() {
		$boolIsValid = true;

		if( true == is_null( $this->getNamePrefix() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_prefix', __( 'Title is required.' ), '', [ 'label' => __( 'Title' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valNameMiddle() {

		$boolIsValid = true;

		if( true == is_null( $this->getNameMiddle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_middle', __( 'Middle name is required.' ), '', [ 'label' => __( 'Middle Name' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valAltNameMiddle( $strSectionName ) {

		$boolIsValid = true;
		$strSectionName = __( '{%s,0} Middle Name', [ $strSectionName ] );
		if( true == is_null( $this->getAltNameMiddle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alt_name_middle', __( '{%s,0} is required.', [ $strSectionName ] ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valPreferredName( $strSectionName ) {

		$boolIsValid = true;
		if( true == is_null( $this->getPreferredName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'preferred_name', __( '{%s,0} is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valDlNumberEncrypted() {

		$boolIsValid = true;

		if( 0 == strlen( $this->getDlNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'drivers_licence', __( 'Driver\'s license is required.' ), '' ) );
		}

		return $boolIsValid;
	}

//	public function valIdentificationValue() {
//
//		$boolIsValid = true;
//
//		if( 0 == strlen( $this->getIdentificationNumber() ) ) {
//			$boolIsValid = false;
//			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identification_value', __( 'ID number is required.' ), '', [ 'label' => __( 'Id number' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
//		}
//
//		return $boolIsValid;
//	}

//	public function valIdentificationExpiration() {
//
//		$boolIsValid = true;
//
//		if( true == isset( $this->m_strIdentificationExpiration ) && 0 < strlen( $this->m_strIdentificationExpiration ) && 1 !== CValidation::checkDate( $this->m_strIdentificationExpiration ) ) {
//			$boolIsValid = false;
//			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'identification_expiration', __( 'ID expiration date must be in mm/dd/yyyy form.' ) ) );
//		}
//
//		return $boolIsValid;
//	}

	public function valIdentificationDate( $strDate ) {
		$arrstrDate = [];
		if( strpos( $strDate, '-' ) ) {
			$arrstrDate  = explode( '-', $strDate );
		} else {
			$arrstrDate  = explode( '/', $strDate );
		}

		if( \Psi\Libraries\UtilFunctions\count( $arrstrDate ) < 3 ) {
			return false;
		}
		if( false == valStr( $arrstrDate[0] ) || false == valStr( $arrstrDate[1] ) || false == valStr( $arrstrDate[2] ) ) {
			return false;
		}
		if( 4 != strlen( $arrstrDate[0] ) && 4 != strlen( $arrstrDate[1] ) && 4 != strlen( $arrstrDate[2] ) ) {
			return false;
		}

		$strIdDate = date_format( new DateTime( $strDate ), 'm/d/Y' );
		if( true == valStr( $strIdDate ) && 1 != CValidation::checkDate( $strIdDate ) ) {
			return false;
		}

		return true;
	}

	public function valCompanyIdentificationNumber() {

		$boolIsValid = true;

		if( false == isset( $this->m_intCompanyIdentificationTypeId ) || false == valId( $this->m_intCompanyIdentificationTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_identification_type_id', __( 'Identification Type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTaxNumber( $strSectionName, $arrobjPropertyApplicationPreferences = [], $intTaxIdTypeId, $objDatabase ) {

		$boolIsValid = true;

		if( false == valId( $intTaxIdTypeId ) ) {
			return true;
		}

		$objTaxIdType = \Psi\Eos\Entrata\CTaxIdTypes::createService()->fetchTaxIdTypeById( $intTaxIdTypeId, $objDatabase );

		if( false == valobj( $objTaxIdType, 'CTaxIdType' ) ) {
			return $boolIsValid;
		}

		$strTaxIdTypeAbbreviation = $objTaxIdType->getAbbreviation() ?? CTaxIdType::SSN;
		if( 0 == strlen( $strSectionName ) ) {
			$strSectionName = $strTaxIdTypeAbbreviation;
		}

		$intTaxNumberLength = \Psi\CStringService::singleton()->strlen( preg_replace( '/[-]/', '', $this->getTaxNumber() ) );
		if( 0 < $intTaxNumberLength ) {
			if( true == in_array( $intTaxIdTypeId, CTaxIdType::$c_arrstrMixTaxIdTypes ) ) {
				if( CTaxIdType::CIN == $intTaxIdTypeId && false == preg_match( CTaxIdType::CIN_TYPE_VALIDATION_STRING, $this->getTaxNumber() ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
				if( CTaxIdType::NINO == $intTaxIdTypeId && false == preg_match( CTaxIdType::NINO_TYPE_VALIDATION_STRING, $this->getTaxNumber() ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
				if( ( CTaxIdType::CIN != $intTaxIdTypeId && CTaxIdType::NINO != $intTaxIdTypeId ) && ( ( 9 != $intTaxNumberLength ) || ( false == is_numeric( preg_replace( '/[-]/', '', $this->getTaxNumber() ) ) ) || ( 0 == ( int ) $this->getTaxNumber() ) ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
			} elseif( true == in_array( $intTaxIdTypeId, CTaxIdType::$c_arrstrSpanishTaxIdTypes ) ) {
				// if DNI or NIE does not have a valid format it returns error
				if( ( 9 != $intTaxNumberLength ) || ( !preg_match( CTaxIdType::NIF_TYPE_VALIDATION_STRING, $this->getTaxNumber() ) ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid because it does not match the format of DNI, CIF, or NIE.', [ $strSectionName ] );
				} else {
					$boolIsValid &= $this->validateSpanishTaxIdTypes( $intTaxIdTypeId, $this->getTaxNumber() );
					if( false == $boolIsValid ) {
						$strErrorMessage = __( '{%s,0} is not valid because it does not match the format of DNI, CIF, or NIE.', [ $strSectionName ] );
					}
				}
			} else {
				if( ( 15 < $intTaxNumberLength )
				    || ( 0 >= $intTaxNumberLength )
				    || ( 0 != preg_match( CTaxIdType::OTHER_TAX_TYPE_VALIDATION_STRING, $this->getTaxNumber() ) ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
			}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, $strErrorMessage, '', [ 'label' => $strTaxIdTypeAbbreviation, 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valSecureInfo( $arrobjPropertyApplicationPreferences, $boolIsRequired = false, $boolIsFromEntrata = false, $objDatabase ) {

		$boolIsValid = true;
		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			return $boolIsValid;
		}

		$strLocaleCode = CLocale::DEFAULT_LOCALE;
		$intPropertyId      = current( $arrobjPropertyApplicationPreferences )->getPropertyId();
		$objProperty        = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $this->getCid(), $objDatabase );
		if( true == valObj( $objProperty, 'CProperty' ) ) {
			$strLocaleCode = $objProperty->getLocaleCode();
		}

		if( true == array_key_exists( 'HIDE_BASIC_INFO_SECURE_INFO', $arrobjPropertyApplicationPreferences ) && false == $arrobjPropertyApplicationPreferences['HIDE_BASIC_INFO_SECURE_INFO']->getValue( $strLocaleCode ) ) {

			$objLeaseCustomer = NULL;
			if( true == valId( $this->getApplicationId() ) && true == valId( $this->getCustomerId() ) ) {
				$objApplication = \Psi\Eos\Entrata\CApplications::createService()->fetchApplicationByIdByCid( $this->getApplicationId(), $this->getCid(), $objDatabase );
				$objLeaseCustomer = \Psi\Eos\Entrata\CLeaseCustomers::createService()->fetchLeaseCustomerByCustomerIdByLeaseIdByCid( $this->getCustomerId(), $objApplication->getLeaseId(), $this->getCid(), $objDatabase );
			}
			$objSecureInfoHelper = new CApplicantSecureInfoHelper( $arrobjPropertyApplicationPreferences, $objDatabase, NULL, NULL, $objLeaseCustomer );

			$arrobjCustomerIdentificationValues = ( array ) rekeyObjects( 'CompanyIdentificationTypeId', \Psi\Eos\Entrata\CCustomerIdentificationValues::createService()->fetchCustomerIdentificationValuesByCustomerIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase ) );
			$arrobjCustomerIdentificationFiles = ( array ) \Psi\Eos\Entrata\CFiles::createService()->fetchFilesByCustomerIdsByFileTypeSystemCodeByCid( [ $this->getCustomerId() ], CFileType::SYSTEM_CODE_APPLICANT_IDENTIFICATION, $this->getCid(), $objDatabase );

			if( true == array_key_exists( 'BASIC_INFO_ASSIGN_TYPE', $arrobjPropertyApplicationPreferences ) && 'customer' == $arrobjPropertyApplicationPreferences['BASIC_INFO_ASSIGN_TYPE']->getValue( $strLocaleCode ) ) {
				if( true == array_key_exists( 'BASIC_INFO_IDENTIFICATION_TYPE', $arrobjPropertyApplicationPreferences ) && 'tax_id' == $arrobjPropertyApplicationPreferences['BASIC_INFO_IDENTIFICATION_TYPE']->getValue( $strLocaleCode ) ) {

					if( true == is_null( $this->getIsAlien() ) ) {

						if( true == $objSecureInfoHelper->showCitizenshipQuestion() ) {

							if( false == valStr( $this->getTaxNumber() ) ) {

								$boolIsValid &= false;
								$strErrorMessage = __( 'Tax number is required' );

								if( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN_REQUIRED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN_REQUIRED']->getValue( $strLocaleCode ) ) {
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_citizen', $strErrorMessage, '', [ 'label' => __( 'Tax Number Citizen' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
								}

								if( true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED']->getValue( $strLocaleCode ) ) {
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_non_citizen', $strErrorMessage, '', [ 'label' => __( 'Tax Number Non Citizen' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
								}

							}

							$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getCitizenSecureInfo(), $arrobjCustomerIdentificationValues, 'Citizen', $arrobjCustomerIdentificationFiles );
							$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getNonCitizenSecureInfoWithTaxId(), $arrobjCustomerIdentificationValues, 'Non Citizen', $arrobjCustomerIdentificationFiles );
							$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getNonCitizenSecureInfoWithoutTaxId(), $arrobjCustomerIdentificationValues, 'Non Citizen', $arrobjCustomerIdentificationFiles );
						} else {
							if( true == is_null( $this->getHasTaxNumber() ) ) {
								if( false == valStr( $this->getTaxNumber() ) ) {
									$boolIsValid &= false;
									$strErrorMessage = __( 'Tax number is required' );
									if( true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED']->getValue( $strLocaleCode ) ) {
										$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_non_citizen', $strErrorMessage, '', [ 'label' => __( 'Tax Number' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
									}
								}

								$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getNonCitizenSecureInfoWithTaxId(), $arrobjCustomerIdentificationValues, '', $arrobjCustomerIdentificationFiles );
								$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getNonCitizenSecureInfoWithoutTaxId(), $arrobjCustomerIdentificationValues, '', $arrobjCustomerIdentificationFiles );

							} else {
								if( true == $this->getHasTaxNumber() ) {
									if( true == valStr( $this->getTaxNumber() ) ) {
										$intTaxIdTypeId = $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue( $strLocaleCode );
										$boolIsValid &= $this->valTaxNumber( 'tax_number_non_citizen', $arrobjPropertyApplicationPreferences, $intTaxIdTypeId, $objDatabase );
									} else {
										$boolIsValid &= false;
										$strErrorMessage = __( 'Tax number is required' );
										if( true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED']->getValue( $strLocaleCode ) ) {
											$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_non_citizen', $strErrorMessage, '', [ 'label' => __( 'Tax Number' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
										}

									}
									$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getNonCitizenSecureInfoWithTaxId(), $arrobjCustomerIdentificationValues, '', $arrobjCustomerIdentificationFiles );

								} else {
									$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getNonCitizenSecureInfoWithoutTaxId(), $arrobjCustomerIdentificationValues, '', $arrobjCustomerIdentificationFiles );
								}

							}
						}

					} else {

						if( false == $this->getIsAlien() ) {

							if( true == valStr( $this->getTaxNumber() ) ) {
								$intTaxIdTypeId = $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue( $strLocaleCode );
								$boolIsValid &= $this->valTaxNumber( 'tax_number_citizen', $arrobjPropertyApplicationPreferences, $intTaxIdTypeId, $objDatabase );
							} else {
								if( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN_REQUIRED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN_REQUIRED']->getValue( $strLocaleCode ) ) {
									$boolIsValid &= false;
									$strErrorMessage = __( 'Tax number is required' );
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_citizen', $strErrorMessage, '', [ 'label' => __( 'Tax Number Citizen' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
								}
							}

							$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getCitizenSecureInfo(), $arrobjCustomerIdentificationValues, 'Citizen', $arrobjCustomerIdentificationFiles );
						}

						if( true == $this->getIsAlien() ) {

							if( false != $this->getHasTaxNumber() && false == valStr( $this->getTaxNumber() ) ) {
								$boolIsValid &= false;
								$strErrorMessage = __( 'Tax number is required' );
								if( true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN_REQUIRED']->getValue( $strLocaleCode ) ) {
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number_non_citizen', $strErrorMessage, '', [ 'label' => __( 'Tax Number' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
								}
							}

							if( true == valStr( $this->getTaxNumber() ) ) {
								$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getNonCitizenSecureInfoWithTaxId(), $arrobjCustomerIdentificationValues, 'Non Citizen', $arrobjCustomerIdentificationFiles );

								$intTaxIdTypeId = $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue( $strLocaleCode );
								$boolIsValid &= $this->valTaxNumber( 'tax_number_non_citizen', $arrobjPropertyApplicationPreferences, $intTaxIdTypeId, $objDatabase );

							} else {
								$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getNonCitizenSecureInfoWithoutTaxId(), $arrobjCustomerIdentificationValues, 'Non Citizen', $arrobjCustomerIdentificationFiles );
							}
						}
					}

				} else {
					$boolIsValid &= $this->valIdentificationTypes( $objSecureInfoHelper->getAlternateSecureInfo(), $arrobjCustomerIdentificationValues, 'Alternate', $arrobjCustomerIdentificationFiles );
					return $boolIsValid;
				}
			} else {

				if( true == is_null( $this->getIsAlien() ) ) {
					if( true == array_key_exists( 'BASIC_INFO_ENTITY_QUESTION_REQUIRED', $arrobjPropertyApplicationPreferences )
				    	&& true == $arrobjPropertyApplicationPreferences['BASIC_INFO_ENTITY_QUESTION_REQUIRED']->getValue( $strLocaleCode ) ) {
						if( false == valStr( $this->getTaxNumber() ) ) {
							$boolIsValid &= false;
							$strErrorMessage = __( 'Tax number is required.' );
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_tax_number', $strErrorMessage, '', [ 'label' => __( 'Tax Number' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
						}
					}
				} else {

					if( true == array_key_exists( 'BASIC_INFO_ENTITY_QUESTION_REQUIRED', $arrobjPropertyApplicationPreferences )
				    	&& true == $arrobjPropertyApplicationPreferences['BASIC_INFO_ENTITY_QUESTION_REQUIRED']->getValue( $strLocaleCode ) ) {
						if( false == $this->getIsAlien() ) {
							if( false == valStr( $this->getTaxNumber() ) ) {
								$boolIsValid &= false;
								$strErrorMessage = __( 'Tax number is required.' );
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_tax_number', $strErrorMessage, '', [ 'label' => __( 'Tax Number' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
							} else {

								if( true == valObj( $objProperty, 'CProperty' ) ) {
									$objTaxIdType = \Psi\Eos\Entrata\CTaxIdTypes::createService()->fetchTaxIdTypeByPropertyCountryCodeByIsOrganization( $objProperty->getCountryCode(), true, $objDatabase );
									if( true == valObj( $objTaxIdType, 'CTaxIdType' ) ) {
										$boolIsValid &= $this->valTaxNumber( 'entity_tax_number', $arrobjPropertyApplicationPreferences, $objTaxIdType->getId(), $objDatabase );
									}
								}
							}
						}

					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valIdentificationTypes( $arrmixIdentificationTypesSetUp, $arrobjCustomerIdentificationValues, $strLabel, $arrobjCustomerIdentificationFiles ) {
		$boolIsValid = true;
		$boolValidateAll = true;
		$strErrorLabel = strtolower( str_replace( ' ', '_', ( $strLabel ) ) );

		if( false == valArr( $arrmixIdentificationTypesSetUp ) || false == array_key_exists( 'identification_types', $arrmixIdentificationTypesSetUp ) ) return $boolIsValid;

		if( true == $arrmixIdentificationTypesSetUp['is_multiple_options'] ) {
			if( true == valArr( $arrobjCustomerIdentificationValues ) ) {
				foreach( $arrobjCustomerIdentificationValues as $objCustomerIdentificationValue ) {
					if( true == valStr( $objCustomerIdentificationValue->getIdentificationNumber() ) || true == valStr( $objCustomerIdentificationValue->getIdExpirationDate() ) ) {
						$boolValidateAll = false;
					}
				}
			}

		}

		foreach( $arrmixIdentificationTypesSetUp['identification_types'] as $intIdentificationTypeId => $arrmixIdentificationType ) {
			if( true == $arrmixIdentificationType['is_required'] ) {

				if( true == $arrmixIdentificationTypesSetUp['is_multiple_options']
				    && false == $boolValidateAll
				    && ( false == valObj( $arrobjCustomerIdentificationValues[$intIdentificationTypeId], 'CCustomerIdentificationValue' )
				         || ( true == valObj( $arrobjCustomerIdentificationValues[$intIdentificationTypeId], 'CCustomerIdentificationValue' )
				             && false == valStr( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getIdentificationNumber() )
				             && false == valStr( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getIdExpirationDate() ) ) ) ) {
					continue;
				}

				if( false == array_key_exists( $intIdentificationTypeId, $arrobjCustomerIdentificationValues ) ) {

					$boolIsValid = false;

					$strErrorMessage = __( 'Identification Number is required' );
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_id_number_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification Number', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

					if( true == $arrmixIdentificationType['include_document_upload'] ) {
						$strErrorMessage = __( 'Document is required' );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_file_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification File Upload', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}

					if( true == $arrmixIdentificationType['basic_sub_questions']['issue_date'] ) {
						$strErrorMessage = __( 'Issue date is required' );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_issue_date_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification Issue Date', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}

					if( true == $arrmixIdentificationType['basic_sub_questions']['expiration_date'] ) {
						$strErrorMessage = __( 'Expiration date is required' );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_expiration_date_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification Expiration Date', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}

					if( true == $arrmixIdentificationType['basic_sub_questions']['licensing_authority_country'] ) {
						$strErrorMessage = __( 'Country is required' );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_licensing_authority_country_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification Country', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}

					if( true == $arrmixIdentificationType['basic_sub_questions']['licensing_authority_administrative_area'] ) {
						$strErrorMessage = __( 'State is required' );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_licensing_authority_administrative_area_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification State', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}

				} else {

					if( false == valStr( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getIdentificationNumber() ) ) {
						$boolIsValid = false;
						$strErrorMessage = __( 'Identification Number is required' );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_id_number_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification Number', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}

					if( true == $arrmixIdentificationType['include_document_upload'] && false == valId( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getFileId() ) ) {
						$boolIsValid = false;
						$strErrorMessage = __( 'Document is required' );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_file_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification File Upload', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}

					if( true == $arrmixIdentificationType['basic_sub_questions']['issue_date'] ) {
						if( false == valStr( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getIdIssueDate() ) ) {
							$boolIsValid     = false;
							$strErrorMessage = __( 'Issue date is required' );
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_issue_date_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification Issue Date', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
						}
					}

					if( true == $arrmixIdentificationType['basic_sub_questions']['expiration_date'] ) {
						if( false == valStr( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getIdExpirationDate() ) ) {
							$boolIsValid     = false;
							$strErrorMessage = __( 'Expiration date is required' );
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_expiration_date_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification Expiration Date', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
						}
					}

					if( true == $arrmixIdentificationType['basic_sub_questions']['licensing_authority_country'] ) {
						if( false == valStr( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getDetailsField( 'country' ) ) ) {
							$boolIsValid     = false;
							$strErrorMessage = __( 'Country is required' );
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $intIdentificationTypeId . '_licensing_authority_country_' . $strErrorLabel, $strErrorMessage, '', [ 'label' => __( '{%s,0} Identification Country', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
						}
						if( true == $arrmixIdentificationType['basic_sub_questions']['licensing_authority_administrative_area'] ) {
							$objCustomerAddress = new CCustomerAddress();
							$objPostalAddress   = $objCustomerAddress->getPostalAddress();
							$objPostalAddress->setCountry( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getDetailsField( 'country' ) );
							$objPostalAddress->setAdministrativeArea( $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->getDetailsField( 'administrative_area' ) );
							$objErrorMsg = $arrobjCustomerIdentificationValues[$intIdentificationTypeId]->valIdentificationState( $strLabel, $objPostalAddress, $intIdentificationTypeId );
							if( true == valObj( $objErrorMsg, 'CErrorMsg' ) ) {
								$boolIsValid     = false;
								$this->addErrorMsg( $objErrorMsg );
							}
						}

					}

				}

			}

			if( false == $boolValidateAll ) break;

		}

		if( true == valarr( $arrobjCustomerIdentificationValues ) && true == valarr( $arrobjCustomerIdentificationFiles ) && true == $arrmixIdentificationType['include_document_upload'] ) {
			foreach( $arrobjCustomerIdentificationValues as $objCustomerIdentificationValues ) {
				foreach( $arrobjCustomerIdentificationFiles as $objFile ) {
					$arrmixFileVerification = ( array ) $objFile->getFileVerification( [ 'current_status' ] );
					if( $objCustomerIdentificationValues->getFileId() == $objFile->getId() &&
					    \CFileEvent::FILE_VERIFICATION_STATUS_REJECTED == $arrmixFileVerification['current_status'] ) {
						$boolIsValid = false;
						$strErrorMessage = __( 'Document is rejected' );
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $objCustomerIdentificationValues->getCompanyIdentificationTypeId() . '_file_rejected', $strErrorMessage, '', [ 'label' => __( '{%s,0} File Upload', [ $strLabel ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valTaxNumberEncrypted( $strSectionName = NULL, $boolRequired = false, $arrobjPropertyApplicationPreferences, $objDatabase ) {

		$boolIsValid = true;
		if( false == valId( $this->getTaxIdTypeId() ) ) {
			return true;
		}

		$intTaxIdTypeId = $this->getTaxIdTypeId();
		$objTaxIdType = \Psi\Eos\Entrata\CTaxIdTypes::createService()->fetchTaxIdTypeById( $intTaxIdTypeId, $objDatabase );

		if( false == valobj( $objTaxIdType, 'CTaxIdType' ) ) {
			return $boolIsValid;
		}

		$strTaxIdTypeAbbreviation = $objTaxIdType->getAbbreviation() ?? CTaxIdType::SSN;
		if( CTaxIdType::SSN != $intTaxIdTypeId || 0 == strlen( $strSectionName ) ) {
			$strSectionName = $strTaxIdTypeAbbreviation;
		}

		$intTaxNumberLength = \Psi\CStringService::singleton()->strlen( preg_replace( '/[-]/', '', $this->getTaxNumber() ) );
		if( 0 < $intTaxNumberLength ) {
			if( true == in_array( $intTaxIdTypeId, CTaxIdType::$c_arrstrMixTaxIdTypes ) ) {
				if( CTaxIdType::CIN == $intTaxIdTypeId && false == preg_match( CTaxIdType::CIN_TYPE_VALIDATION_STRING, $this->getTaxNumber() ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
				if( CTaxIdType::NINO == $intTaxIdTypeId && false == preg_match( CTaxIdType::NINO_TYPE_VALIDATION_STRING, $this->getTaxNumber() ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
				if( ( CTaxIdType::CIN != $intTaxIdTypeId && CTaxIdType::NINO != $intTaxIdTypeId ) && ( ( 9 != $intTaxNumberLength ) || ( false == is_numeric( preg_replace( '/[-]/', '', $this->getTaxNumber() ) ) ) || ( 0 == ( int ) $this->getTaxNumber() ) ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
			} elseif( true == in_array( $intTaxIdTypeId, CTaxIdType::$c_arrstrSpanishTaxIdTypes ) ) {
				// if DNI or NIE does not have a valid format it returns error
				if( ( 9 != $intTaxNumberLength ) || ( !preg_match( CTaxIdType::NIF_TYPE_VALIDATION_STRING, $this->getTaxNumber() ) ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid because it does not match the format of DNI, CIF, or NIE.', [ $strSectionName ] );
				} else {
					$boolIsValid &= $this->validateSpanishTaxIdTypes( $intTaxIdTypeId, $this->getTaxNumber() );
					if( false == $boolIsValid ) {
						$strErrorMessage = __( '{%s,0} is not valid because it does not match the format of DNI, CIF, or NIE.', [ $strSectionName ] );
					}
				}
			} else {
				if( ( 15 < $intTaxNumberLength )
				    || ( 0 >= $intTaxNumberLength )
				    || ( 0 != preg_match( CTaxIdType::OTHER_TAX_TYPE_VALIDATION_STRING, $this->getTaxNumber() ) ) ) {
					$boolIsValid     &= false;
					$strErrorMessage = __( '{%s,0} is not valid.', [ $strSectionName ] );
				}
			}
		}

		if( false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', $strErrorMessage, '', [ 'label' => $strTaxIdTypeAbbreviation, 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}

    return $boolIsValid;
	}

	public function valTaxNumberEncryptedForMoveIn( $strSectionName = NULL ) {

		$boolIsValid = true;

		if( 0 < strlen( $this->getTaxNumber() ) && ( 9 > \Psi\CStringService::singleton()->strlen( $this->getTaxNumber() ) || false == is_numeric( $this->getTaxNumber() ) ) ) {
			$boolIsValid = false;
			if( 0 < strlen( $strSectionName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( '{%s,0} is not valid.', [ $strSectionName ] ), '' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( 'SSN is not valid.' ), '' ) );
			}
		}

		return $boolIsValid;
	}

	public function valVehiclePlateNumbers() {

		$boolIsValid = true;

		if( 0 == strlen( $this->getPrimaryVehicleLicensePlateNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_vehicle_license_plate_number', __( 'Primary plate number is required.' ), '', [ 'label' => __( 'Vehicles' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valAffordableTaxNumberEncrypted( $strSectionName = NULL, $boolRequired = false ) {

		$boolIsValid = true;

		if( 0 == strlen( $this->getTaxNumber() ) ) {
			if( true == $boolRequired ) {
				if( false == $this->getIsAlien() ) {
					$boolIsValid &= false;

					if( 0 < strlen( $strSectionName ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( '{%s,0} is required.', [ $strSectionName ] ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					} else {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( 'SSN is required.' ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
					}
				}
			}
		} else {
			if( ( 9 != \Psi\CStringService::singleton()->strlen( $this->getTaxNumber() ) ) || ( false == is_numeric( $this->getTaxNumber() ) ) || ( 0 == ( int ) $this->getTaxNumber() ) ) {

				$boolIsValid &= false;
				if( 0 < strlen( $strSectionName ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( '{%s,0} is not valid.', [ $strSectionName ] ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'tax_number', __( 'SSN is not valid.' ), '', [ 'label' => __( 'SSN' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPublicJudgmentYear() {

		$boolIsValid = true;

		if( 0 < strlen( $this->getPublicJudgmentYear() ) ) {
			if( false == is_numeric( $this->getPublicJudgmentYear() ) || 4 > \Psi\CStringService::singleton()->strlen( $this->getPublicJudgmentYear() ) || 0 == $this->getPublicJudgmentYear() || 1900 > $this->getPublicJudgmentYear() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'public_judgment_year', __( 'Public judgment year must be in a valid format.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valBankruptcyYear() {

		$boolIsValid = true;

		if( 0 < strlen( $this->getBankruptcyYear() ) ) {
			if( false == is_numeric( $this->getBankruptcyYear() ) || 4 > \Psi\CStringService::singleton()->strlen( $this->getBankruptcyYear() ) || 0 == $this->getBankruptcyYear() || 1900 > $this->getBankruptcyYear() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_year', __( 'Bankruptcy year must be in a valid format.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function valRequiredApplicantName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strNameFirst ) && false == isset( $this->m_strNameLast ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_name', __( 'Please Enter First Name or Last Name' ), 502 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valRelationship() {
		$boolIsValid     = true;
		$strRelationship = $this->getRelationship();
		if( true == empty( $strRelationship ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relationship', __( 'Relationship is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valBlockNonUsResident( $intApplicationStepId = NULL ) {

		$boolIsValid = true;

		if( false == is_null( $intApplicationStepId ) && CApplicationStep::BASIC_INFO == $intApplicationStepId ) {
			if( true == $this->getIsAlien() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_alien', __( 'U.S. citizen is required.' ), '' ) );
			}
		} else {
			if( false == is_null( $intApplicationStepId ) && CApplicationStep::ADDITIONAL_INFO == $intApplicationStepId ) {
				if( true == $this->getCurrentIsNonUsAddress() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_is_non_us_address', __( 'A current U.S. address is required. Please contact the property staff for assistance with your application.' ), '' ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valRequiredBasicInfoFields( $objDatabase, $arrobjPropertyApplicationPreferences, $boolIsRequired = true, $boolIsFromEntrata = false ) {
		$boolIsValid   = true;
		$boolCanBeNull = ( false == $boolIsRequired ) ? true : false;
		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			// CKT : Added code to validate the birth date when property application preference is not set else error could be returned during DML statement execution due to invalid date format / incorrect data.
			$boolIsValid &= $this->valBirthDate( true, 18 );
//			$boolIsValid &= $this->valIdentificationExpiration();

			return $boolIsValid;
		}

		if( true == $boolIsRequired ) {

			if( true == array_key_exists( 'REQUIRE_BASIC_INFO_GENDER', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'HIDE_BASIC_INFO_TITLE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valGender();
			}

			if( true == array_key_exists( 'REQUIRE_BASIC_INFO_MIDDLE_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valNameMiddle();
			}

			if( true == array_key_exists( 'REQUIRE_BASIC_INFO_MIDDLE_NAME', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_BASIC_INFO_ALTERNATE_NAME', $arrobjPropertyApplicationPreferences ) ) {
				$strSectionName = ( true == array_key_exists( 'RENAME_BASIC_INFO_ALTERNATE_NAME', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_ALTERNATE_NAME']->getValue() ) ? $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_ALTERNATE_NAME']->getValue() : __( 'Alternate' );

				$boolIsValid &= $this->valAltNameMiddle( $strSectionName );
			}
		}

		if( ( true == array_key_exists( 'SHOW_GUARANTOR_RELATIONSHIP_TO_APPLICANT', $arrobjPropertyApplicationPreferences ) ) && $this->getCustomerTypeId() == CCustomerType::GUARANTOR ) {
			$boolRelationshipToApplicantCanBeNull = ( false == array_key_exists( 'REQUIRE_GUARANTOR_RELATIONSHIP_TO_APPLICANT', $arrobjPropertyApplicationPreferences ) ) ? true : false;
			$boolIsValid &= $this->valRelationshipToApplicant( $boolRelationshipToApplicantCanBeNull );
		}

		// validate secondary phone number
		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_SECONDARY_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valSecondaryPhoneNumber();
		}

		$boolIsValid &= $this->valSecureInfo( $arrobjPropertyApplicationPreferences, $boolIsRequired, $boolIsFromEntrata, $objDatabase );
		$intRequiredApplicantAge = 0;

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && ( $this->getCustomerTypeId() == CCustomerType::PRIMARY || $this->getCustomerTypeId() == CCustomerType::GUARANTOR ) ) {
			$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();

		} else {
			if( true == in_array( $this->getCustomerTypeId(), [ CCustomerType::GUARANTOR, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT ] ) ) {
				$intRequiredApplicantAge = 18;
			} else {
				if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
					$intRequiredApplicantAge = 18;
					if( false == array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsRequired = false;
					}
				}
			}
		}

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valBirthDate( $boolIsRequired, $intRequiredApplicantAge );

		} elseif( 0 < $intRequiredApplicantAge ) {
			$boolIsValid &= $this->valBirthDate( $boolIsRequired, $intRequiredApplicantAge );

		} else {
			$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge );
		}

		return $boolIsValid;
	}

	public function valRequiredNewBasicInfoFields( $objDatabase, $arrobjPropertyApplicationPreferences, $boolIsRequired = true, $boolValidateUsername = false, $boolIsAllowDuplicateEmail = false, $arrobjPropertyPreferences = NULL, $boolIsFromEntrata = false ) {
		$boolIsValid   = true;
		$boolCanBeNull = ( false == $boolIsRequired ) ? true : false;

		if( false == valArr( $arrobjPropertyApplicationPreferences ) ) {
			return $boolIsValid;
		}

		if( true == $boolValidateUsername ) {
			if( CCustomerType::GUARANTOR == $this->getCustomerTypeId() ) {
				$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail );
			} else {
				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
					$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
				} else {
					$intRequiredApplicantAge = 18;
				}

				if( false == is_null( $this->getBirthDate() ) && true == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
					$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail );
				} else {
					if( false == is_null( $this->getBirthDate() ) ) {
						$boolIsValid &= $this->valUsername( $objDatabase, false, $boolIsAllowDuplicateEmail );
					} else {
						$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail );
					}
				}
			}
		}

		// validate Gender
		if( ( false == array_key_exists( 'ALLOW_COED_ROOMMATES', $arrobjPropertyPreferences ) || false == getPropertyPreferenceValueByKey( 'ALLOW_COED_ROOMMATES', $arrobjPropertyPreferences ) || CCustomerType::PRIMARY != $this->getCustomerTypeId() ) &&
		    ( ( true == array_key_exists( 'REQUIRE_BASIC_INFO_GENDER', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'HIDE_BASIC_INFO_TITLE', $arrobjPropertyApplicationPreferences ) ) || ( false == array_key_exists( 'HIDE_BASIC_INFO_GENDER', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_BASIC_INFO_GENDER', $arrobjPropertyApplicationPreferences ) ) ) ) {
			$boolIsValid &= $this->valGender();
		}

		// validate Name Title
		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_TITLE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'HIDE_BASIC_INFO_GENDER', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valNamePrefix();
		}

		// validate Middle name
		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_MIDDLE_NAME', $arrobjPropertyApplicationPreferences ) && 0 != $this->getHasMiddleName() ) {
			$boolIsValid &= $this->valNameMiddle();
		}

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_MIDDLE_NAME', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_BASIC_INFO_ALTERNATE_NAME', $arrobjPropertyApplicationPreferences ) && 0 != $this->getHasAltMiddleName() ) {
			$strSectionName = ( true == array_key_exists( 'RENAME_BASIC_INFO_ALTERNATE_NAME', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_ALTERNATE_NAME']->getValue() ) ? $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_ALTERNATE_NAME']->getValue() : __( 'Alternate' );
			$boolIsValid &= $this->valAltNameMiddle( $strSectionName );
		}

		// validate Preferred name
		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_PREFERRED_NAME', $arrobjPropertyApplicationPreferences ) ) {
			$strSectionName = ( true == array_key_exists( 'RENAME_BASIC_INFO_PREFERRED_NAME', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_PREFERRED_NAME']->getValue() ) ? $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_PREFERRED_NAME']->getValue() : __( 'Preferred Name' );
			$boolIsValid &= $this->valPreferredName( $strSectionName );
		}

		// validate Maternal Last Name
		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_MATERNAL_LAST_NAME', $arrobjPropertyPreferences ) && 0 != $this->getHasNameLastMatronymic() && true == $arrobjPropertyPreferences['REQUIRE_BASIC_INFO_MATERNAL_LAST_NAME']->getValue() ) {
			$strSectionName = ( true == array_key_exists( 'RENAME_BASIC_INFO_MATERNAL_LAST_NAME', $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['RENAME_BASIC_INFO_MATERNAL_LAST_NAME']->getValue() ) ) ? $arrobjPropertyPreferences['RENAME_BASIC_INFO_MATERNAL_LAST_NAME']->getValue() : __( '​Mother\'s Last Name' );
			$boolIsValid &= $this->valNameLastMatronymic( $strSectionName );
		}

		if( ( true == array_key_exists( 'SHOW_GUARANTOR_RELATIONSHIP_TO_APPLICANT', $arrobjPropertyApplicationPreferences ) ) && $this->getCustomerTypeId() == CCustomerType::GUARANTOR ) {
			$boolRelationshipToApplicantCanBeNull = ( false == array_key_exists( 'REQUIRE_GUARANTOR_RELATIONSHIP_TO_APPLICANT', $arrobjPropertyApplicationPreferences ) ) ? true : false;
			$boolIsValid &= $this->valRelationshipToApplicant( $boolRelationshipToApplicantCanBeNull );
		}

		$boolIsValid &= $this->valSecureInfo( $arrobjPropertyApplicationPreferences, $boolIsRequired, $boolIsFromEntrata, $objDatabase );
		$intRequiredApplicantAge = 0;

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && ( true == in_array( $this->getCustomerTypeId(), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds ) ) ) {
			$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
		} else {
			if( true == in_array( $this->getCustomerTypeId(), [ CCustomerType::GUARANTOR, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT ] ) ) {
				$intRequiredApplicantAge = 18;
			} else {
				if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
					$intRequiredApplicantAge = 18;
					if( false == array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) ) {
						$boolIsRequired = false;
					}
				}
			}
		}

		if( true == array_key_exists( 'HIDE_BASIC_INFO_SECURE_INFO', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) ) {
			if( 1 == getPropertyPreferenceValueByKey( 'ENABLE_PARENTAL_CONSENT_FORM', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['ENABLE_PARENTAL_CONSENT_FORM']->getValue() && true == $this->getIsMinor( $intRequiredApplicantAge ) ) {
				if( true == in_array( $this->getCustomerTypeId(), CCustomerType::$c_arrintParentConsentRequiredCustomerTypeIds ) ) {
					$boolIsValid = $this->valStudentBirthDate( $arrobjPropertyApplicationPreferences );
				} else {
					$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge );
				}
			} else {
				$boolIsValid &= $this->valBirthDate( $boolIsRequired, $intRequiredApplicantAge );
			}

		} else {
			if( 1 == getPropertyPreferenceValueByKey( 'ENABLE_PARENTAL_CONSENT_FORM', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['ENABLE_PARENTAL_CONSENT_FORM']->getValue() && true == $this->getIsMinor( $intRequiredApplicantAge ) ) {
				if( true == in_array( $this->getCustomerTypeId(), CCustomerType::$c_arrintParentConsentRequiredCustomerTypeIds ) ) {
					$boolIsValid = $this->valStudentBirthDate( $arrobjPropertyApplicationPreferences );
				} else {
					$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge );
				}
			} else {
				$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge );
			}
		}

		if( true == array_key_exists( 'SHOW_BASIC_INFO_ALTERNATE_NAME', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_BASIC_INFO_ALTERNATE_NAME', $arrobjPropertyApplicationPreferences ) ) {

			$strSectionName = ( true == array_key_exists( 'RENAME_BASIC_INFO_ALTERNATE_NAME', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_ALTERNATE_NAME']->getValue() ) ? $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_ALTERNATE_NAME']->getValue() : __( 'Alternate' );

			$boolIsValid &= $this->valAltNameFirst( false, true, $strSectionName );
			$boolIsValid &= $this->valAltNameLast( false, true, $strSectionName );
		}

		return $boolIsValid;
	}

	public function valRequiredApplicantPreScreeningFields( $objDatabase, $arrobjPropertyApplicationPreferences, $arrobjPropertyPreferences ) {
		$boolIsValid = true;

		if( true == valArr( $arrobjPropertyPreferences )
		 && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRED_GENDER', $arrobjPropertyPreferences )
		 && 0 < strlen( $arrobjPropertyPreferences['APPLICATION_PRE_SCREENING_REQUIRED_GENDER']->getValue() ) ) {

			$boolIsValid &= $this->valGender();
		}

		$boolIsValid &= $this->valNameFirst();
		$boolIsValid &= $this->valNameLast();
		$boolIsValid &= $this->valCurrentAddress();
		$boolIsValid &= $this->valUsername( $objDatabase, true, false );

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
			$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
		} else {
			$intRequiredApplicantAge = 18;
		}

		$intCustomerType = ( false == is_null( $this->getCustomerTypeId() ) ) ? $this->getCustomerTypeId() : $this->getRelationship();

		if( CCustomerType::RESPONSIBLE == $intCustomerType ) {
			$boolIsValid &= $this->valBirthDate( true, 18 );
		} elseif( CCustomerType::NOT_RESPONSIBLE == $intCustomerType && ( false == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) || ( true == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && ( true == is_null( $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) || true == $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ) ) ) {
			$boolIsValid &= $this->valBirthDate( true, NULL, 18 );
		} elseif( CCustomerType::GUARANTOR == $intCustomerType ) {
			$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge );
		} else {
			$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge );
		}

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_ADDITIONAL_IDENTIFICATION_TYPE', $arrobjPropertyPreferences ) && false == $this->getIsAlien() ) {
			$boolIsValid &= $this->valDlNumberEncrypted();
//			$boolIsValid &= $this->valDlStateCode();
		}

		if( true == valArr( $arrobjPropertyPreferences )
		 && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRED_CURRENT_RENT_RANGE_MORTGAGE', $arrobjPropertyPreferences ) ) {

			$boolIsValid &= $this->valCurrentMonthlyPaymentAmount();
		}

		if( true == is_null( $this->getIsAlien() ) || 1 <> $this->getIsAlien() ) {

			$boolIsValid &= $this->valTaxNumberEncrypted( $strSectionName = NULL, false, $arrobjPropertyApplicationPreferences, $objDatabase );
		}

		return $boolIsValid;
	}

	public function valRequiredNewApplicantPreScreeningFields( $objDatabase, $arrobjPropertyApplicationPreferences, $arrobjPropertyPreferences, $objProperty = NULL ) {

		$boolIsValid    = true;
		$strSectionName = '';

		if( true == valArr( $arrobjPropertyPreferences ) && false == array_key_exists( 'APPLICATION_PRE_SCREENING_HIDE_GENDER', $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRED_GENDER', $arrobjPropertyPreferences ) && 1 == $arrobjPropertyPreferences['APPLICATION_PRE_SCREENING_REQUIRED_GENDER']->getValue() ) {
			$boolIsValid &= $this->valGender();
		}

		// validate Name Title
		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_TITLE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_HIDE_GENDER', $arrobjPropertyPreferences ) ) {
			$boolIsValid &= $this->valNamePrefix();
		}

		$boolIsValid &= $this->valNameFirst();

		if( true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_MIDDLE_NAME', $arrobjPropertyPreferences ) && 0 != $this->getHasMiddleName() ) {
			$boolIsValid &= $this->valNameMiddle();
		}

		$boolIsValid &= $this->valNameLast();

		if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
			$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
		} else {
			$intRequiredApplicantAge = 18;
		}

		$intCustomerType                    = ( false == is_null( $this->getCustomerTypeId() ) ) ? $this->getCustomerTypeId() : $this->getRelationship();
		$boolNonResponsiveRequiredBirthDate = ( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT', $arrobjPropertyPreferences ) && 0 != $arrobjPropertyPreferences['NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT']->getValue() && false == in_array( $intCustomerType, [ CCustomerType::GUARANTOR, CCustomerType::PRIMARY ] ) ) ? true : false;

		if( false == array_key_exists( 'HIDE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) || true == $boolNonResponsiveRequiredBirthDate ) {
			$boolRequiredBirthDate = ( true == $boolNonResponsiveRequiredBirthDate ) ? $boolNonResponsiveRequiredBirthDate : array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences );

			if( CCustomerType::RESPONSIBLE == $intCustomerType ) {
				$boolIsValid &= $this->valBirthDate( $boolRequiredBirthDate, $intRequiredApplicantAge );
			} elseif( CCustomerType::NOT_RESPONSIBLE == $intCustomerType && ( false == array_key_exists( 'REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) || ( true == array_key_exists( 'REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && ( true == is_null( $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) || true == $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ) ) ) {
				$boolIsValid &= $this->valBirthDate( $boolRequiredBirthDate, NULL, $intRequiredApplicantAge );
			} elseif( CCustomerType::GUARANTOR == $intCustomerType ) {
				$boolIsValid &= $this->valBirthDate( $boolRequiredBirthDate, $intRequiredApplicantAge );
			} elseif( false == array_key_exists( 'HIDE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) ) {
				$boolIsValid &= $this->valBirthDate( $boolRequiredBirthDate, $intRequiredApplicantAge );
			}
		}

		if( false == is_null( $this->getBirthDate() ) && true == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
			$boolIsValid &= $this->valUsername( $objDatabase, true, true, true );
		} else {
			if( false == is_null( $this->getBirthDate() ) && false == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
				$boolIsValid &= $this->valUsername( $objDatabase, false, true, true );
			} else {
				if( true == is_null( $this->getBirthDate() ) ) {
					$boolIsValid &= $this->valUsername( $objDatabase, true, false, true );
				}
			}
		}

		if( false == in_array( $this->getCustomerTypeId(), CCustomerType::$c_arrintSecondaryCustomerTypeIds ) && true == valArr( $arrobjPropertyPreferences ) ) {
			if( true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_CURRENT_MOVE_IN_DATE', $arrobjPropertyPreferences ) ) {
				$boolIsValid &= $this->valCurrentAddressMoveInDate( true, $strSectionName );
			} else {
				if( false == is_null( $this->getCurrentMoveInDate() ) ) {
					$boolIsValid &= $this->valCurrentAddressMoveInDate( false, $strSectionName );
				}
			}
			if( true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_CURRENT_MOVE_OUT_DATE', $arrobjPropertyPreferences ) ) {
				$boolIsValid &= $this->valCurrentAddressMoveOutDate( true, $strSectionName );
			} else {
				if( false == is_null( $this->getCurrentMoveOutDate() ) ) {
					$boolIsValid &= $this->valCurrentAddressMoveOutDate( false, $strSectionName );
				}
			}
		}

		if( CCustomerType::NOT_RESPONSIBLE != $this->getCustomerTypeId() ) {

			$intCompanyIdentificationTypeCount = CCompanyIdentificationTypes::fetchAssociatedCompanyIdentificationTypesByPropertyIdByCid( $objProperty->getId(), $objProperty->getCid(), $objDatabase, $boolReturnCount = true );

//			if( true == valArr( $arrobjPropertyPreferences ) && 0 < $intCompanyIdentificationTypeCount && ( ( true == array_key_exists( 'REQUIRE_BASIC_INFO_TAX_NUMBER_OR_ID_NUMBER', $arrobjPropertyApplicationPreferences ) && ( true == $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_TAX_NUMBER_OR_ID_NUMBER']->getValue() ) && true == $this->getIsAlien() ) || true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_ADDITIONAL_IDENTIFICATION_TYPE', $arrobjPropertyPreferences ) ) ) {
//				$boolIsValid &= $this->valIdentificationValue();
//				$objCompanyIdentificationType = CCompanyIdentificationTypes::fetchCompanyIdentificationTypeByIdByCid( $this->getCompanyIdentificationTypeId(), $this->getCid(), $objDatabase );
//
//				if( false == is_null( $this->getIdentificationNumber() ) && true == valObj( $objCompanyIdentificationType, 'CCompanyIdentificationType' ) && 'DR LIC' == $objCompanyIdentificationType->getSystemCode() ) {
//					$boolIsValid &= $this->valDlStateCode();
//				}
//
//				$boolIsValid &= $this->valIdentificationExpiration();
//			}

			if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRED_CURRENT_RENT_RANGE_MORTGAGE', $arrobjPropertyPreferences ) && 1 == $arrobjPropertyPreferences['APPLICATION_PRE_SCREENING_REQUIRED_CURRENT_RENT_RANGE_MORTGAGE']->getValue() ) {
				$boolIsValid &= $this->valCurrentMonthlyPaymentAmount( true );
			} else {
				$boolIsValid &= $this->valCurrentMonthlyPaymentAmount();
			}

			$boolTaxIdKeyExists = ( bool ) ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) && true == valId( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() ) || true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN', $arrobjPropertyApplicationPreferences ) && true == valId( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() ) );

			if( ( true == is_null( $this->getIsAlien() ) || 1 <> $this->getIsAlien() ) && false == $boolTaxIdKeyExists ) {

				$strSectionName = CTaxIdType::createService()->getAbbreviations()[( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) ) ? ( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() ) : ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) ? $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() : '' )];

				$boolIsValid &= $this->valTaxNumberEncrypted( $strSectionName, false, $arrobjPropertyApplicationPreferences, $objDatabase );
			}
		}

		return $boolIsValid;
	}

	public function valRequiredAdditionalInfoFields( $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequired = true ) {

		$boolIsValid    = true;
		$strSectionName = '';
		$boolIsValid &= $this->valPersonalInformation( $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequired );
		$boolIsValid &= $this->valCurrentAddressInformation( $arrobjPropertyApplicationPreferences, $boolIsValidateRequired );

		if( ( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && CCustomerType::GUARANTOR != $this->getCustomerTypeId() ) ) {
			$boolIsValid &= $this->valPreviousAddressInformation( $arrobjPropertyApplicationPreferences, $boolIsValidateRequired );
		}

		return $boolIsValid;
	}

	public function valPersonalInformation( $arrobjPropertyApplicationPreferences, $objDatabase, $boolIsValidateRequired ) {
		$boolIsValid    = true;
		$strSectionName = '';

		if( true == array_key_exists( 'HIDE_ADDITIONAL_INFO_PERSONAL_INFO', $arrobjPropertyApplicationPreferences ) ) {
			return true;
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_NAME_MAIDEN', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valNameMaiden( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_HEIGHT', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valHeight( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_WEIGHT', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valWeight( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_OCCUPATION', $arrobjPropertyApplicationPreferences ) ) {

			// If Occupations not associated with the property, it will not validate
			$intPropertyId = $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_OCCUPATION']->getPropertyId();

			$intPropertyOccupationCount = CPropertyOccupations::fetchPublishedPropertyOccupationsByPropertyIdByCid( $intPropertyId, $this->getCid(), $objDatabase, $boolReturnCount = true );

			if( 0 < $intPropertyOccupationCount ) {
				$boolIsValid &= $this->valOccupation( $boolIsValidateRequired );
			}
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_MARITAL_STATUS', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valMaritalStatusTypeId( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_EYE_COLOR', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valEyeColor( $boolIsValidateRequired );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_HAIR_COLOR', $arrobjPropertyApplicationPreferences ) ) {
			$boolIsValid &= $this->valHairColor( $strSectionName, 'addl_info' );
		}

		if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_CITIZENSHIP', $arrobjPropertyApplicationPreferences ) && true == valId( $arrobjPropertyApplicationPreferences['REQUIRE_ADDITIONAL_INFO_PERSONAL_INFO_CITIZENSHIP']->getValue() ) ) {
			$boolIsValid &= $this->valCitizenshipCountryCode();
		}

		return $boolIsValid;
	}

	public function valCurrentAddressInformation( $arrobjPropertyApplicationPreferences, $boolIsValidateRequired ) {

		$boolIsValid    = true;
		$strSectionName = '';

		if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {

			if( true == array_key_exists( 'RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue() ) {
				$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_CURRENT_ADDRESS']->getValue();
			}

			if( CCustomerAddress::ADDRESS_OWNER_TYPE_OWNER != $this->getCurrentAddressOwnerType() ) {

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MANAGER_NAME_FIRST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressManagerNameFirst( $strSectionName, $boolIsValidateRequired, $boolIsRequired = true, $boolCheckLength = false );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MANAGER_NAME_FIRST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressManagerNameFirst( $strSectionName, $boolIsValidateRequired, $boolIsRequired = false, $boolCheckLength = false );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_MANAGER_NAME_LAST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressManagerNameLast( $strSectionName, $boolIsValidateRequired, $boolIsRequired = true, $boolCheckLength = false );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_MANAGER_NAME_LAST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressManagerNameLast( $strSectionName, $boolIsValidateRequired, $boolIsRequired = false, $boolCheckLength = false );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressPhoneNumber( true, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressPhoneNumber( false, $strSectionName, $boolIsValidateRequired );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressEmail( true, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentAddressEmail( false, $strSectionName, $boolIsValidateRequired );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_CURRENT_ADDRESS_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentFaxNumber( false, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_CURRENT_ADDRESS_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valCurrentFaxNumber( true, $strSectionName, $boolIsValidateRequired );
				}
			}
		}

		return $boolIsValid;
	}

	public function valPreviousAddressInformation( $arrobjPropertyApplicationPreferences, $boolIsValidateRequired = true ) {
		$boolIsValid    = true;
		$strSectionName = '';
		if( true == array_key_exists( 'RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue() ) {
			$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_ADDITIONAL_INFO_PREVIOUS_ADDRESS']->getValue();
		}

		if( ( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) && CCustomerType::GUARANTOR != $this->getCustomerTypeId() )
		 || ( CCustomerType::GUARANTOR == $this->getCustomerTypeId() && true == array_key_exists( 'SHOW_GUARANTOR_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ) {

            if( CCustomerAddress::ADDRESS_OWNER_TYPE_OWNER != $this->getPreviousAddressOwnerType() ) {

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MANAGER_NAME_FIRST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressManagerNameFirst( $strSectionName, $boolIsValidateRequired, $boolIsRequired = true, $boolCheckLength = false );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MANAGER_NAME_FIRST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressManagerNameFirst( $strSectionName, $boolIsValidateRequired, $boolIsRequired = false, $boolCheckLength = false );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MANAGER_NAME_LAST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressManagerNameLast( $strSectionName, $boolIsValidateRequired, $boolIsRequired = true, $boolCheckLength = false );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_MANAGER_NAME_LAST', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressManagerNameLast( $strSectionName, $boolIsValidateRequired, $boolIsRequired = false, $boolCheckLength = false );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressPhoneNumber( true, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_PHONE_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressPhoneNumber( false, $strSectionName, $boolIsValidateRequired );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressEmail( true, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_EMAIL_ADDRESS', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousAddressEmail( false, $strSectionName, $boolIsValidateRequired );
				}

				if( true == array_key_exists( 'REQUIRE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_FAX_NUMBER', $arrobjPropertyApplicationPreferences )
				 && !( $this->getCustomerTypeId() == CCustomerType::GUARANTOR && false == array_key_exists( 'SHOW_GUARANTOR_PREVIOUS_ADDRESS', $arrobjPropertyApplicationPreferences ) ) ) {
					$boolIsValid &= $this->valPreviousFaxNumber( false, $strSectionName, $boolIsValidateRequired );
				} elseif( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_PREVIOUS_ADDRESS_FAX_NUMBER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valPreviousFaxNumber( true, $strSectionName, $boolIsValidateRequired );
				}

			}

		}

		return $boolIsValid;
	}

	public function valGeneralQuestions( $arrobjPropertyApplicationPreferences ) {
		$boolIsValid = true;
		if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS', $arrobjPropertyApplicationPreferences ) ) {
			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SUED_FOR_RENT', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getWasSuedForRent() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_sued_for_rent', '', '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == $this->getWasSuedForRent() && true == is_null( $this->getSuedForRentComments() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sued_for_rent_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SUED_FOR_DAMAGES', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getWasSuedForDamages() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_sued_for_damages', '', '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_EVICTED', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasBeenEvicted() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_been_evicted' ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_DEFAULTED_ON_LEASE', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasBrokenLease() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_broken_lease', '', '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( CCustomerType::GUARANTOR != $this->getCustomerTypeId() ) {
				if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_FELONY', $arrobjPropertyApplicationPreferences ) ) {
					if( true == is_null( $this->getHasFelonyConviction() ) ) {
						$boolIsValid &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_felony_conviction' ) );
					}
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_PUBLIC_SUITS', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasPublicJudgment() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_public_judgment' ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_BANKRUPTCY', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasBankruptcy() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_bankruptcy' ) );
				}
			}

			if( CCustomerType::GUARANTOR != $this->getCustomerTypeId() ) {
				if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_RENTERS_INSURANCE', $arrobjPropertyApplicationPreferences ) ) {
					if( true == is_null( $this->getHasRentersInsurance() ) ) {
						$boolIsValid &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_renters_insurance' ) );
					}
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SMOKING', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getSmokes() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'smokes' ) );
				}
			}

			if( true == array_key_exists( 'SHOW_ADDITIONAL_INFO_QUESTIONS_GAA_SCREENING', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasPreviousRentOwed() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_previous_rent_owed' ) );
				}

				if( true == is_null( $this->getIsReturningResident() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_returning_resident' ) );
				}

				if( true == is_null( $this->getIsUnemployed() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_unemployed' ) );
				}
			}

			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'general_questions', __( 'Please select Yes/No for given question(s)' ) ) );
			}

		}

		return $boolIsValid;
	}

	public function valRequiredGAAQuestions() {

		$boolIsValid = true;

		if( false == is_null( $this->getGaaScreeningInitials() ) ) {
			$strGaaInitials = $this->getGaaScreeningInitials();

			if( true == in_array( \Psi\CStringService::singleton()->strlen( $strGaaInitials ), [ 3, 4 ] ) && \Psi\CStringService::singleton()->strtoupper( $strGaaInitials ) == \Psi\CStringService::singleton()->strtoupper( $this->getInitials() ) ) {
				$arrstrGaaInitials = \Psi\CStringService::singleton()->str_split( $strGaaInitials );
				$strGaaInitials    = $arrstrGaaInitials[0] . $arrstrGaaInitials[2];
			}

			if( \Psi\CStringService::singleton()->strtoupper( $strGaaInitials ) != \Psi\CStringService::singleton()->strtoupper( $this->getFirstAndLastNameInitials() ) ) {
				$boolIsValid = false;
				if( 0 == strlen( trim( $strGaaInitials ) ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, 'GAA_SCREENING_INITIALS', __( 'Please enter your Initials.' ), E_USER_ERROR ) );
				} else {
					$strFirstAndLastNameInitials = \Psi\CStringService::singleton()->strtoupper( $this->getFirstAndLastNameInitials() );
					$this->addErrorMsg( new CErrorMsg( NULL, 'GAA_SCREENING_INITIALS', __( 'Please enter your correct initials. Should be: {%s,0}.', [ $strFirstAndLastNameInitials ] ), E_USER_ERROR, [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}
		}

		if( true == $this->getHasBeenEvicted() && true == is_null( $this->getEvictedComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'HAS_BEEN_EVICTED', __( 'Answer to eviction action is required.' ), E_USER_ERROR ) );
		}

		if( true == $this->getHasPreviousRentOwed() && true == is_null( $this->getPreviousRentOwedComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'HAS_PREVIOUS_RENT_OWED', __( 'Answer to previous money pending is required.' ), E_USER_ERROR ) );
		}

		if( true == $this->getHasBankruptcy() && true == is_null( $this->getBankruptcyComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'HAS_BANKRUPTCY', __( 'Answer to bankruptcy is required.' ), E_USER_ERROR ) );
		}

		if( true == $this->getHasFelonyConviction() && true == is_null( $this->getFelonyConvictionComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'HAS_FELONY_CONVICTION', __( 'Answer to any crime is required.' ), E_USER_ERROR ) );
		}

		if( true == $this->getHasBrokenLease() && true == is_null( $this->getBrokenLeaseComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'HAS_BROKEN_LEASE', __( 'Answer to lease violation is required.' ), E_USER_ERROR ) );
		}

		if( true == $this->getIsReturningResident() && true == is_null( $this->getReturningResidentComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'IS_RETURNING_RESIDENT', __( 'Answer to apartment community you lived before is required.' ), E_USER_ERROR ) );
		}

		if( true == $this->getIsUnemployed() && true == is_null( $this->getUnemployedComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'IS_UNEMPLOYED', __( 'Answer to un-employment is required.' ), E_USER_ERROR ) );
		}

		if( 0 == $this->getIsLegalUsResident() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'IS_LEGAL_US_RESIDENT', __( 'Please select legal rights to be in U.S.' ), E_USER_ERROR, [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		if( true == $this->getIsLegalUsResident() && CApplicantDetail::LEGAL_US_RESIDENT_YES_WITH_USCIS == $this->getIsLegalUsResident() && true == is_null( $this->getReasonInUsComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'REASON_IN_US_COMMENTS', __( 'Answer to reason in U.S. is required.' ), E_USER_ERROR ) );
		}

		if( true == is_null( $this->getGaaScreeningInitials() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'GAA_SCREENING_INITIALS', __( 'Initials required.' ), E_USER_ERROR, [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function valAdditionalInfoQuestions( $arrobjPropertyApplicationPreferences ) {

		$boolIsValid = true;
		if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS', $arrobjPropertyApplicationPreferences ) ) {
			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SUED_FOR_RENT', $arrobjPropertyApplicationPreferences ) ) {

				if( true == is_null( $this->getWasSuedForRent() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_sued_for_rent', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
				if( true == $this->getWasSuedForRent() && true == is_null( $this->getSuedForRentComments() ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_HAS_BEEN_SUED_FOR_RENT', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sued_for_rent_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SUED_FOR_DAMAGES', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getWasSuedForDamages() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_sued_for_damages', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
				if( true == $this->getWasSuedForDamages() && true == is_null( $this->getSuedForDamageComments() ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_PROPERTY_DAMAGES', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sued_for_damage_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_EVICTED', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasBeenEvicted() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_been_evicted', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_DEFAULTED_ON_LEASE', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasBrokenLease() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_broken_lease', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == $this->getHasBrokenLease() && true == is_null( $this->getBrokenLeaseComments() ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_DEFAULTED_ON_LEASE', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'broken_lease_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

			}

			if( CCustomerType::GUARANTOR != $this->getCustomerTypeId() || true == array_key_exists( 'SHOW_ADDITIONAL_INFO_QUESTIONS_GAA_SCREENING', $arrobjPropertyApplicationPreferences ) ) {
				if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_FELONY', $arrobjPropertyApplicationPreferences ) ) {
					if( true == is_null( $this->getHasFelonyConviction() ) ) {
						$boolIsValid &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_felony_conviction', __( 'Please select Yes/No for the given question.' . CCustomerType::GUARANTOR . '!=' . $this->getCustomerTypeId() ) ) );
					}
				}

				if( true == $this->getHasFelonyConviction() && true == is_null( $this->getFelonyConvictionComments() ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_FELONY', $arrobjPropertyApplicationPreferences ) ) {
					if( true == $this->m_boolValidateRequired ) {
						$boolIsValid &= false;
					}
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'felony_conviction_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questionsdd' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_PUBLIC_SUITS', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasPublicJudgment() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_public_judgment', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
				if( true == $this->getHasPublicJudgment() && true == is_null( $this->getPublicJudgmentYear() ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_PUBLIC_SUITS', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'public_judgment_year', __( 'Year is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_BANKRUPTCY', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasBankruptcy() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_bankruptcy', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
				if( true == $this->getHasBankruptcy() && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_BANKRUPTCY', $arrobjPropertyApplicationPreferences ) ) {
					if( false == array_key_exists( 'SHOW_ADDITIONAL_INFO_QUESTIONS_GAA_SCREENING', $arrobjPropertyApplicationPreferences ) ) {
						if( true == is_null( $this->getBankruptcyYear() ) ) {
							$boolIsValid &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_year', __( 'Year is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
						if( true == is_null( $this->getBankruptcyCounty() ) ) {
							$boolIsValid &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_county', __( 'County is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
						if( true == is_null( $this->getBankruptcyStateCode() ) ) {
							$boolIsValid &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bankruptcy_state', __( 'State/Province is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
					}
				}
			}

			if( CCustomerType::GUARANTOR != $this->getCustomerTypeId() ) {
				if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_RENTERS_INSURANCE', $arrobjPropertyApplicationPreferences ) ) {
					if( true == is_null( $this->getHasRentersInsurance() ) ) {
						$boolIsValid &= false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_renters_insurance', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
					}
					if( true == $this->getHasRentersInsurance() ) {
						if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_RENTERS_POLICY_NAME', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->getInsuranceProviderName() ) ) {
							$boolIsValid &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renter_policy_name', __( 'Provider name is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
						if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_RENTERS_POLICY_NUMBER', $arrobjPropertyApplicationPreferences ) && true == is_null( $this->getInsurancePolicyNumber() ) ) {
							$boolIsValid &= false;
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'renter_policy_number', __( 'Policy number is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
						}
					}
				}
			}

			if( false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_SMOKING', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getSmokes() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'smokes', __( 'Please select Yes/No for the given question.' ) ) );
				}
			}

			if( false == array_key_exists( 'SHOW_ADDITIONAL_INFO_QUESTIONS_GAA_SCREENING', $arrobjPropertyApplicationPreferences ) ) {
				if( true == $this->getHasBeenEvicted() && true == is_null( $this->getEvictedComments() ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_EVICTED', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'evicted_comments', __( 'Explaination is required.' ) ) );
				}

				if( true == $this->getHasFelonyConviction() && true == is_null( $this->getFelonyConvictionComments() ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_FELONY', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'felony_conviction_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == $this->getHasBrokenLease() && true == is_null( $this->getBrokenLeaseComments() ) && false == array_key_exists( 'HIDE_ADDITIONAL_INFO_QUESTIONS_COMMENT_DEFAULTED_ON_LEASE', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'broken_lease_comments', __( 'Explaination is required.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}
			}

			if( true == array_key_exists( 'SHOW_ADDITIONAL_INFO_QUESTIONS_GAA_SCREENING', $arrobjPropertyApplicationPreferences ) ) {
				if( true == is_null( $this->getHasPreviousRentOwed() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_previous_rent_owed', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == is_null( $this->getIsReturningResident() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_returning_resident', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				if( true == is_null( $this->getIsUnemployed() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_unemployed', __( 'Please select Yes/No for the given question.' ), '', [ 'label' => __( 'General Questions' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
				}

				$boolIsValid &= $this->validate( 'gaa_screening_update' );
			}
		}

		return $boolIsValid;
	}

	public function valInitials( $strEnteredInitials ) {
		$boolIsValid = false;

		if( 0 == strlen( $strEnteredInitials ) ) {
			return false;
		}

		if( ( \Psi\CStringService::singleton()->strtolower( $strEnteredInitials ) == \Psi\CStringService::singleton()->strtolower( $this->getInitials() ) ) || ( \Psi\CStringService::singleton()->strtolower( $strEnteredInitials ) == \Psi\CStringService::singleton()->strtolower( $this->getFirstAndLastNameInitials() ) ) ) {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valDigitalSignature( $strEnteredDigitalSignature ) {
		$boolIsValid = false;
		$strEnteredDigitalSignature = \Psi\CStringService::singleton()->strtolower( $strEnteredDigitalSignature );
		if( 0 == strlen( $strEnteredDigitalSignature ) ) {
			return false;
		}
		$strExactSignatureMatch1 = \Psi\CStringService::singleton()->strtolower( $this->getNameFirst() . ' ' . $this->getNameLast() );
		$strExactSignatureMatch2 = \Psi\CStringService::singleton()->strtolower( $this->getNameFirst() . ' ' . $this->getNameMiddle() . ' ' . $this->getNameLast() );

		if( true == $this->getHasNameLastMatronymic() && false == empty( trim( $this->getNameLastMatronymic() ) ) ) {
			$strExactSignatureMatch1 = \Psi\CStringService::singleton()->strtolower( $this->getNameFirst() . ' ' . $this->getNameLast() . ' ' . $this->getNameLastMatronymic() );
			$strExactSignatureMatch2 = \Psi\CStringService::singleton()->strtolower( $this->getNameFirst() . ' ' . $this->getNameMiddle() . ' ' . $this->getNameLast() . ' ' . $this->getNameLastMatronymic() );
		}

		if( $strExactSignatureMatch1 == $strEnteredDigitalSignature || $strExactSignatureMatch2 == $strEnteredDigitalSignature ) {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valRequiredInsertGuestCardFields( $arrobjPropertyPreferences, $objDatabase, $boolIsLiveChat, $boolIsValidateGuestCardFields ) {

		$boolIsValid = true;

		$boolIsValid &= $this->valNameFirst();
		$boolIsValid &= $this->valNameLast();
		$boolIsValid &= $this->valUsername( $objDatabase, true, true );

		if( true == $boolIsValidateGuestCardFields && false == $boolIsLiveChat && true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_AND_REQUIRE_ZIP_CODE_OPTION', $arrobjPropertyPreferences ) ) {
			$boolIsValid &= $this->valCurrentPostalCode();
		}

		return $boolIsValid;
	}

	public function valRequiredInsertMobileScheduleTourFields( $arrobjPropertyPreferences, $boolIsLiveChat, $objDatabase ) {

		$boolIsValid = true;

		$boolIsValid &= $this->valNameFirst();
		$boolIsValid &= $this->valNameLast();
		$boolIsValid &= $this->valUsername( $objDatabase, true, true );

		return $boolIsValid;
	}

	public function valBlockMessageCenterEmail() {
		$boolIsValid = true;

		if( false == $this->getIsBlockMessageCenterEmail() && false == valStr( $this->getUsername() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'block_email', __( 'No email address is provided for this applicant.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBlockContactPointsEmail() {
		$boolIsValid = true;

		if( false == $this->getIsBlockContactPointsEmail() && false == valStr( $this->getUsername() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'block_email', __( 'No email address is provided for this applicant.' ) ) );
		}

		return $boolIsValid;
	}

	public function valBlockMessageCenterSms( $objDatabase ) {

		$boolIsValid = true;
		$objCustomerPhoneNumber = \Psi\Eos\Entrata\CCustomerPhoneNumbers::createService()->fetchCustomerPhoneNumberByCustomerIdByPhoneNumberTypeIdByCid( $this->getCustomerId(), CPhoneNumberType::MOBILE, $this->getCid(), $objDatabase );

		if( false == $this->getIsBlockMessageCenterSms() && false == valObj( $objCustomerPhoneNumber, CCustomerPhoneNumber::class ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'block_sms', __( 'Mobile number is not provided by this applicant, you can not subscribe it.' ) ) );

		}

		return $boolIsValid;
	}

	public function valPhoneNumberOrUsername( $objDatabase, $boolIsAllowDuplicateEmail = false ) {
		$boolIsValid = true;
		if( false == valArr( $this->getCustomerPhoneNumbers() ) && false != is_null( $this->getUsername() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number_or_email', __( 'At least one phone number or email is required for the applicant.' ) ) );

			return $boolIsValid;
		}

		if( false == is_null( $this->getFaxNumber() ) ) {
			$boolIsValid &= $this->valFaxNumber( true );
		}
		if( false == is_null( $this->getUsername() ) ) {
			$boolIsValid &= $this->valUsername( $objDatabase, false, $boolIsAllowDuplicateEmail );
		}

		return $boolIsValid;
	}

	public function valPhoneNumberAndEmail( $strPropertySettingValue ) {
		$boolIsValid = true;

		if( false == valStr( $strPropertySettingValue ) ) {
			return $boolIsValid;
		}

		$strPrimaryPhoneNumber = $this->getPrimaryPhoneNumber();
		$objPrimaryPhoneNumber = $this->createPhoneNumber( $strPrimaryPhoneNumber );
		$intLength             = \Psi\CStringService::singleton()->strlen( $objPrimaryPhoneNumber->getNumber() );

		$strEmailAddress  = $this->getUsername();
		$boolIsPhoneValid = true;
		$boolIsEmailValid = true;

		if( CCustomerType::NOT_RESPONSIBLE != $this->getCustomerTypeId() && ( false == isset( $strPrimaryPhoneNumber ) || true == empty( $strPrimaryPhoneNumber ) ) ) {
			$boolIsPhoneValid = false;
		}

		if( true == $boolIsPhoneValid && 'PHONE_ONLY' != $strPropertySettingValue ) {

			if( true == valStr( $objPrimaryPhoneNumber->getNumber() ) && false == $objPrimaryPhoneNumber->isValid() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_phone_number', __( 'Primary phone number is not valid for {%s, region} (+{%s, country_code}).' . $objPrimaryPhoneNumber->getFormattedErrors(), [ 'region' => $objPrimaryPhoneNumber->getIsoRegionCodeForCountryCode(), 'country_code' => $objPrimaryPhoneNumber->getCountryCode() ] ), ERROR_CODE_INVALID_PHONE_NUMBER, [ 'label' => __( 'Primary Phone' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

				return false;
			}

		}

		if( false == isset( $strEmailAddress ) && 0 == strlen( trim( $strEmailAddress ) ) ) {
			$boolIsEmailValid = false;
		}

		if( true == $boolIsEmailValid ) {
			if( false == CValidation::validateEmailAddresses( $strEmailAddress ) && 0 < strlen( $strEmailAddress ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Email address does not appear to be valid.' ), ERROR_CODE_INVALID_EMAIL, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

				return false;
			}
		}

		if( true == $boolIsEmailValid && true == $this->getIsMilitaryApplicaion() ) {
			$boolIsEmailDomainValid = ( true == in_array( end( explode( '.', $strEmailAddress ) ), self::$c_arrstrMilitaryEmailDomains ) ) ? false : true;
			if( false === $boolIsEmailDomainValid && 0 < strlen( $strEmailAddress ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', __( 'Primary Email address cannot use a domain ending in ".mil" or ".gov".' ), ERROR_CODE_INVALID_EMAIL, [ 'label' => __( 'Email Address' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

				return false;
			}
		}

		if( 'PHONE_ONLY' == $strPropertySettingValue ) {
			return $boolIsValid;
		}

		if( ( ( true == $boolIsPhoneValid || true == $boolIsEmailValid ) && 'PHONE_OR_EMAIL' == $strPropertySettingValue ) ||
		 ( true == $boolIsPhoneValid && true == $boolIsEmailValid && 'PHONE_AND_EMAIL' == $strPropertySettingValue ) ) {
			return true;
		}

		$strValidationMessage = __( 'Primary phone number and email address are required.' );

		if( 'PHONE_OR_EMAIL' == $strPropertySettingValue ) {
			$strValidationMessage = __( 'Either of Primary phone number or email address is required.' );
		}

		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, __( 'phone_number_or_email_address' ), $strValidationMessage, ERROR_CODE_PHONE_NUMBER_REQUIRE, [ 'label' => __( 'Primary Phone' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );

		return false;
	}

	public function valIsAlien() {
		$boolIsValid = true;
		if( true === is_null( $this->getIsAlien() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_alien', __( 'US Citizen is required.' ) ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function validateEntrataAddLead( $arrobjPropertyPreferences ) {

		$boolIsValid                        = true;
		$boolBirthDateIsRequired            = false;
		$boolIsValid &= $this->valNameFirst( true, __( 'Legal First' ) );
		$boolIsValid &= $this->valNameLast();

		$strPhoneEmailPropertyPreferenceValue = NULL;

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'GC_REQUIRE_PHONE_NUMBER_AND_EMAIL', $arrobjPropertyPreferences ) ) {
			$strPhoneEmailPropertyPreferenceValue = $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue();
		}

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'GC_REQUIRE_US_CITIZEN', $arrobjPropertyPreferences ) ) {
			$boolIsValid &= $this->valIsAlien();
		}

		if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'GC_ADDITIONAL_INFO_PERSONAL_INFO', $arrobjPropertyPreferences ) ) {

			if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'GC_REQUIRE_BASIC_INFO_GENDER', $arrobjPropertyPreferences ) ) {
				$boolIsValid &= $this->valGender();
			}
			if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'GC_REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyPreferences ) ) {
				$boolBirthDateIsRequired = true;
			}
			$boolIsValid &= $this->valBirthDate( $boolBirthDateIsRequired );
		}

//		$boolIsValid &= $this->valIdentificationExpiration();

		return $boolIsValid;
	}

	public function valMonthIntentToApply( $strMonthToIntent ) {
		$boolIsValid = true;
		if( '' == $strMonthToIntent ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'application_intent_to_apply', __( 'Month you intend to apply is required.' ) ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valReferrerFirstName( $strFirstName ) {
		$boolIsValid = true;
		if( '' == $strFirstName ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referrer-first-name', __( 'Referrer first name is required.' ) ) );
			$boolIsValid &= false;
		}
		if( '' != $strFirstName && \Psi\CStringService::singleton()->strlen( trim( $strFirstName ) ) < 2 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referrer-first-name', __( 'Referrer first name should contain more that 1 character.' ) ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valReferrerLastName( $strLastName ) {
		$boolIsValid = true;
		if( '' == $strLastName ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referrer-last-name', __( 'Referrer last names is required.' ) ) );
			$boolIsValid &= false;
		}
		if( '' != $strLastName && \Psi\CStringService::singleton()->strlen( trim( $strLastName ) ) < 2 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referrer-first-name', __( 'Referrer last name should contain more that 1 character.' ) ) );
			$boolIsValid &= false;
		}
		return $boolIsValid;
	}

	public function valPreferredLocaleCode( $arrobjPropertyApplicationPreferences, $objProperty, $objDatabase, $strSectionName = 'preferred_language' ) {
		$boolIsValid = true;
		$arrmixPropertyLocales = [];
		if( true == valObj( $objProperty, 'CProperty' ) && true == valObj( $objDatabase, 'CDatabase' ) ) {
			$arrmixPropertyLocales = ( array ) CPropertyLocales::fetchPropertyLocalesByCidByPropertyId( $objProperty->getCid(), $objProperty->getId(), $objDatabase );
		}

		if( true == valArr( $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_LEASE_PREFERENCES_PREFERRED_LANGUAGE', $arrobjPropertyApplicationPreferences ) && true == valId( $arrobjPropertyApplicationPreferences['REQUIRE_LEASE_PREFERENCES_PREFERRED_LANGUAGE']->getValue() ) && true == valArr( $arrmixPropertyLocales ) && false == valStr( $this->getPreferredLocaleCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Preferred language is required.' ), '', [ 'label' => __( 'Preferred Language' ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}
		return $boolIsValid;
	}

	public function valNameLastMatronymic( $strSectionName ) {
		$boolIsValid = true;
		if( false == valStr( $this->getNameLastMatronymic() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last_matronymic', __( '{%s,0} is required.', [ $strSectionName ] ), '', [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
		}
		return $boolIsValid;
	}

	public function valCitizenshipCountryCode() {
		$boolIsValid = true;
		if( false == valStr( $this->getCitizenshipCountryCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'citizenship_country_code', __( 'Citizenship is required.' ), '', [ 'label' => __( 'Citizenship' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $intApplicationId = NULL, $arrobjPropertyApplicationPreferences = NULL, $boolIsAllowDuplicateEmail = false, $arrobjPropertyPreferences = NULL, $boolIsLiveChat = false, $objProperty = NULL, $boolIsValidateGuestCardFields = false, $boolIsCorporateLease = false, $boolIsMaskedPhoneNumber = false, $boolIsFromEntrata = false, $boolIsFromGenerateQuote = false, $boolIsValidatePhone = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valPassword();
				break;

			case 'resident_works_update':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valBirthDate( true );
				break;

			case 'resident_works_online_application_create':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, true );
				$boolIsValid &= $this->valPassword();
				break;

			case 'online_application_create':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valPassword();
				break;

			case 'online_new_application_create':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail, true );
				$boolIsValid &= $this->valPassword();
				if( false == $boolIsFromGenerateQuote ) {
					$boolIsValid &= $this->valPreferredLocaleCode( $arrobjPropertyApplicationPreferences, $objProperty, $objDatabase );
				}

				if( false == $boolIsFromGenerateQuote && true == array_key_exists( 'REQUIRE_BASIC_INFO_MATERNAL_LAST_NAME', $arrobjPropertyPreferences ) && 0 != $this->getHasNameLastMatronymic() && true == $arrobjPropertyPreferences['REQUIRE_BASIC_INFO_MATERNAL_LAST_NAME']->getValue() ) {
					$strSectionName = ( true == array_key_exists( 'RENAME_BASIC_INFO_MATERNAL_LAST_NAME', $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['RENAME_BASIC_INFO_MATERNAL_LAST_NAME']->getValue() ) ) ? $arrobjPropertyPreferences['RENAME_BASIC_INFO_MATERNAL_LAST_NAME']->getValue() : __( '​Mother\'s Last Name' );
					$boolIsValid &= $this->valNameLastMatronymic( $strSectionName );
				}
				break;

			case 'applicant_pre_screening':
				$boolIsValid &= $this->valRequiredApplicantPreScreeningFields( $objDatabase, $arrobjPropertyApplicationPreferences, $arrobjPropertyPreferences );
				break;

			case 'new_applicant_pre_screening':
				$boolIsValid &= $this->valRequiredNewApplicantPreScreeningFields( $objDatabase, $arrobjPropertyApplicationPreferences, $arrobjPropertyPreferences, $objProperty );
				break;

			case 'new_occupant_pre_screening':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();

				if( true == array_key_exists( 'APPLICATION_PRE_SCREENING_REQUIRE_MIDDLE_NAME', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valNameMiddle();
				}

				$boolBirthDateIsRequired = ( ( false == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) || ( true == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && ( true == is_null( $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) || true == $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ) ) && true == valArr( $arrobjPropertyPreferences ) && true == in_array( 'NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT', array_keys( $arrobjPropertyPreferences ) ) && 0 != $arrobjPropertyPreferences['NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT']->getValue() && CCustomerType::GUARANTOR != $this->getCustomerTypeId() ) ? true : ( ( true == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() && ( false == $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() || false == $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ) ? 0 : ( ( CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() && true == array_key_exists( 'REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ? true : ( array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) ) ) );

				$boolIsValid &= $this->valBirthDate( $boolBirthDateIsRequired, $intRequiredApplicantAge = NULL, $intMaxApplicantAge = ( ( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) ) ? $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() : 18 );
				break;

			case VALIDATE_DELETE:
				break;

			case 'applicant_info_update':
				break;

			case 'resident_works_add_applicant':
				$boolBirthDateIsRequired = ( ( false == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) || ( true == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && ( true == is_null( $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) || true == $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ) ) && true == valArr( $arrobjPropertyPreferences ) && true == in_array( 'NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT', array_keys( $arrobjPropertyPreferences ) ) && 0 != $arrobjPropertyPreferences['NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT']->getValue() && CCustomerType::GUARANTOR != $this->getCustomerTypeId() ) ? true : false;

				if( false == $boolBirthDateIsRequired ) {
					$boolBirthDateIsRequired = ( true == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() && ( false == $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() || false == $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ) ? 0 : ( ( CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() && true == array_key_exists( 'REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ? true : ( ( CCustomerType::NOT_RESPONSIBLE != $this->getCustomerTypeId() ) ? array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) : false ) );
				}

				$boolIsValid &= $this->valFirstNameLastNameBirthDate( $objDatabase, $arrobjPropertyApplicationPreferences, $boolIsFromResidentWorks = 1, $arrobjPropertyPreferences, $boolBirthDateIsRequired, $boolIsAllowDuplicateEmail, $boolIsFromEntrata );

				if( CCustomerType::NOT_RESPONSIBLE != $this->getCustomerTypeId() ) {
					$boolIsValid &= $this->valPreferredLocaleCode( $arrobjPropertyApplicationPreferences, $objProperty, $objDatabase );
				}
				break;

			case 'add_business_applicant':
				$boolIsValid &= $this->valNameFirst( true, __( 'Business' ) );
				$boolIsValid &= $this->valUsername( $objDatabase, false, false, false );
				$boolIsValid &= $this->valCurrentAddress( __( 'Corporate Headquarter' ) );
				$boolIsValid &= $this->valTaxNumberEncrypted( __( 'Tax Id' ), $boolRequired = true, $arrobjPropertyApplicationPreferences, $objDatabase );
				break;

			case 'new_resident_works_add_applicant':
				$boolIsValid &= $this->valFirstNameLastNameBirthDate( $objDatabase, $arrobjPropertyApplicationPreferences, $boolIsFromResidentWorks = true, $arrobjPropertyPreferences, $boolCheckBirthDate = true, $boolIsAllowDuplicateEmail, $boolIsFromEntrata );
				if( true === valStr( $this->getCurrentPostalCode() ) ) {
					$boolIsValid &= $this->valCurrentPostalCode();
				}
				break;

			case 'new_resident_works_add_applicant_from_matched_applicants':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valBirthDate();
				break;

			case 'new_resident_works_add_applicant_from_new_add_lead':
				$boolIsValid &= $this->validateEntrataAddLead( $arrobjPropertyPreferences );
				break;

			case 'prospect_portal_secondary_people':
				$boolIsValid &= $this->valFirstNameLastNameBirthDateUsername( $objDatabase, $intApplicationId, $arrobjPropertyApplicationPreferences );
				break;

			case 'resident_works_lease_renewal_insert':
				$boolIsValid &= $this->valFirstNameLastNameBirthDateUsername( $objDatabase, $intApplicationId, $arrobjPropertyApplicationPreferences );
				break;

			case 'resident_works_lease_retrieve_renewal_insert':
				$boolIsValid &= $this->valFirstNameLastName( $objDatabase, $intApplicationId, $arrobjPropertyApplicationPreferences );
				break;

			case 'prospect_portal_secondary_people_secure_info':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valBirthDate();
				$boolIsValid &= $this->valUsername();
				if( true == is_null( $this->getIsAlien() ) || 1 <> $this->getIsAlien() ) {
					$boolIsValid &= $this->valTaxNumberEncrypted( NULL, false, $arrobjPropertyApplicationPreferences, $objDatabase );
				}
				break;

			case 'secondary_people_secure_info_new_application':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valBirthDate();
				break;

			case 'validate_forget_password':
			case 'validate_single_applicant_forgot_password':
				$boolIsValid &= $this->valUsername( $objDatabase, true, true );
				break;

			case 'validate_multiple_applicants_forgot_password':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, true );
				break;

			case 'validate_applicant_name':
				$boolIsValid &= $this->valRequiredApplicantName();
				break;

			case 'resident_works_applicant_basic_info_update':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast( true, false );
				$boolIsValid &= $this->valUsername( $objDatabase, false, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valPassword( false, false );
				$boolIsValid &= $this->valRequiredBasicInfoFields( $objDatabase, $arrobjPropertyApplicationPreferences, false, $boolIsFromEntrata );
				$boolIsValid &= $this->valTaxNumberEncryptedForMoveIn();
				break;

			case 'validate_member_step':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast( true, false );
				$boolIsValid &= $this->valUsername( $objDatabase, false, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valPassword( false, false );
				$boolIsValid &= $this->valRequiredBasicInfoFields( $objDatabase, $arrobjPropertyApplicationPreferences, false, $boolIsFromEntrata );

				$strSectionName = '';
				if( true == array_key_exists( 'RENAME_BASIC_INFO_SSN_OR_TAX_NUMBER', $arrobjPropertyApplicationPreferences ) && '' != $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_SSN_OR_TAX_NUMBER']->getValue() ) {
					$strSectionName = $arrobjPropertyApplicationPreferences['RENAME_BASIC_INFO_SSN_OR_TAX_NUMBER']->getValue();
				}

				if( false == array_key_exists( 'HIDE_BASIC_INFO_TAX_NUMBER', $arrobjPropertyApplicationPreferences ) && false == $this->getIsAlien() ) {
					$boolIsValid &= $this->valAffordableTaxNumberEncrypted( $strSectionName, $boolRequired = array_key_exists( 'HIDE_BASIC_INFO_NOT_US_RESIDENT', $arrobjPropertyApplicationPreferences ) );
				}
				break;

			case 'resident_works_applicant_new_basic_info_update':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, false, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valPassword( false, false );
//				$boolIsValid &= $this->valIdentificationExpiration();
//				if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
//					$boolIsValid &= $this->valStudentIdNumber( $arrobjPropertyApplicationPreferences, false );
//				}

				$intRequiredApplicantAge = 0;

				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && ( true == in_array( $this->getCustomerTypeId(), CCustomerType::$c_arrintApplicationRequiredCustomerTypeIds ) ) ) {
					$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();

				} else {
					if( true == in_array( $this->getCustomerTypeId(), [ CCustomerType::GUARANTOR, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT ] ) ) {
						$intRequiredApplicantAge = 18;

					} else {
						if( CCustomerType::PRIMARY == $this->getCustomerTypeId() ) {
							$intRequiredApplicantAge = 18;
						}
					}
				}

				// validate Name Title
				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_TITLE', $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'HIDE_BASIC_INFO_GENDER', $arrobjPropertyApplicationPreferences ) ) {
					$boolIsValid &= $this->valNamePrefix();
				}

				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_BIRTH_DATE', $arrobjPropertyApplicationPreferences ) ) {
					if( true == in_array( COccupancyType::STUDENT, $this->getAdo()->m_objProperty->getOccupancyTypeIds() ) && 1 == getPropertyPreferenceValueByKey( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) && 1 == getPropertyPreferenceValueByKey( 'ENABLE_PARENTAL_CONSENT_FORM', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['ENABLE_PARENTAL_CONSENT_FORM']->getValue() && true == $this->getIsMinor( $intRequiredApplicantAge ) ) {
						if( true == in_array( $this->m_objApplicant->getCustomerTypeId(), CCustomerType::$c_arrintParentConsentRequiredCustomerTypeIds ) ) {
							$boolIsValid = $this->valStudentBirthDate( $arrobjPropertyApplicationPreferences );
						} else {
							$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge );
						}
					} else {
						$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge );
					}

				} else {
					if( true == in_array( COccupancyType::STUDENT, $this->getAdo()->m_objProperty->getOccupancyTypeIds() ) && 1 == getPropertyPreferenceValueByKey( 'ENABLE_SEMESTER_SELECTION', $arrobjPropertyPreferences ) && 1 == getPropertyPreferenceValueByKey( 'ENABLE_PARENTAL_CONSENT_FORM', $arrobjPropertyPreferences ) && 0 < $arrobjPropertyPreferences['ENABLE_PARENTAL_CONSENT_FORM']->getValue() && true == $this->getIsMinor( $intRequiredApplicantAge ) ) {
						if( true == in_array( $this->m_objApplicant->getCustomerTypeId(), CCustomerType::$c_arrintParentConsentRequiredCustomerTypeIds ) ) {
							$boolIsValid = $this->valStudentBirthDate( $arrobjPropertyApplicationPreferences );
						} else {
							$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge );
						}
					} else {
						$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge );
					}
				}

				$strSectionName = CTaxIdType::createService()->getAbbreviations()[( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) ) ? ( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() ) : ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) ? $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() : '' )];

				$boolTaxIdKeyExists = true == array_key_exists( 'BASIC_INFO_IDENTIFICATION_TYPE', $arrobjPropertyApplicationPreferences ) && \CProperty::TAX_ID == $arrobjPropertyApplicationPreferences['BASIC_INFO_IDENTIFICATION_TYPE'] && ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) && true == valId( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() ) || true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN', $arrobjPropertyApplicationPreferences ) && true == valId( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() ) );
				if( false == $boolTaxIdKeyExists && false == $this->getIsAlien() ) {
					$boolRequired = ( bool ) ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) && true == valId( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() ) ?? true == array_key_exists( 'BASIC_INFO_TAX_ID_NONCITIZEN', $arrobjPropertyApplicationPreferences ) && true == valId( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() ) );
					$boolIsValid  &= $this->valTaxNumberEncrypted( $strSectionName, $boolRequired, $arrobjPropertyApplicationPreferences, $objDatabase );
				}
				break;

			case 'resident_works_applicant_additional_info_update':
				$boolIsValid &= $this->valRequiredAdditionalInfoFields( $arrobjPropertyApplicationPreferences, $objDatabase, $this->getIsValidateRequired() );
				$boolIsValid &= $this->valGeneralQuestions( $arrobjPropertyApplicationPreferences );
				$boolIsValid &= $this->valPublicJudgmentYear();
				$boolIsValid &= $this->valBankruptcyYear();
				break;

			case 'gaa_screening_update':
				$boolIsValid &= $this->valRequiredGAAQuestions();
				break;

			case 'applicant_basic_info_update':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valRequiredBasicInfoFields( $objDatabase, $arrobjPropertyApplicationPreferences, false, $boolIsFromEntrata );
				break;

			case 'applicant_new_basic_info_update':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valRequiredNewBasicInfoFields( $objDatabase, $arrobjPropertyApplicationPreferences, true, true, true, $arrobjPropertyPreferences, $boolIsFromEntrata );

				if( CCustomerType::NOT_RESPONSIBLE != $this->getCustomerTypeId() ) {
					$boolIsValid &= $this->valPreferredLocaleCode( $arrobjPropertyApplicationPreferences, $objProperty, $objDatabase );
				}
				break;

			case 'applicant_additional_info_update':
				$boolIsValid &= $this->valRequiredAdditionalInfoFields( $arrobjPropertyApplicationPreferences, $objDatabase, $this->getIsValidateRequired() );
				$boolIsValid &= $this->valGeneralQuestions( $arrobjPropertyApplicationPreferences );
				break;

			case 'additional_info_questions_new':
				$boolIsValid &= $this->valAdditionalInfoQuestions( $arrobjPropertyApplicationPreferences );
				$boolIsValid &= $this->valRequiredAdditionalInfoFields( $arrobjPropertyApplicationPreferences, $objDatabase, $this->getIsValidateRequired() );
				break;

			case 'application_basic_info_update':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valRequiredBasicInfoFields( $objDatabase, $arrobjPropertyApplicationPreferences, false, $boolIsFromEntrata );
				break;

			case 'application_additional_info_update':
				$boolIsValid &= $this->valRequiredAdditionalInfoFields( $arrobjPropertyApplicationPreferences, $objDatabase );
				break;

			case 'insert_guest_card':
				$boolIsValid &= $this->valRequiredInsertGuestCardFields( $arrobjPropertyPreferences, $objDatabase, $boolIsLiveChat, $boolIsValidateGuestCardFields );
				break;

			case 'insert_guest_card_prospect_portal':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, true );

				if( false == $boolIsLiveChat && true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_AND_REQUIRE_ZIP_CODE_OPTION', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valCurrentPostalCode();
				}
				$boolIsValid &= $this->valNotes();
				break;

			case 'insert_premium_guest_card_prospect_portal':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valMessage();

				if( false == $boolIsMaskedPhoneNumber )
					$boolIsValid &= $this->valUsername( $objDatabase, true, true );

				if( false == $boolIsLiveChat && true == $boolIsMaskedPhoneNumber && true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'ALLOW_EMAIL_OR_PHONE_NUMBER_AS_REQUIRED', $arrobjPropertyPreferences ) ) {
					if( false == valArr( $this->getCustomerPhoneNumbers() ) && true == is_null( $this->m_strUsername ) ) {
						$boolIsValid &= $this->valEmailOrPhoneNumber();
					} elseif( false == is_null( $this->m_strUsername ) ) {
						$boolIsValid &= $this->valUsername( $objDatabase, true, true );
					}
				} elseif( true == $boolIsMaskedPhoneNumber ) {
					$boolIsValid &= $this->valUsername( $objDatabase, true, true );
				}

				if( true == $boolIsValidatePhone ) {
					if( true == $boolIsMaskedPhoneNumber && false == $boolIsLiveChat && true == valArr( $this->getCustomerPhoneNumbers() ) ) {
						if( true == $boolIsFromGenerateQuote || ( false == valArr( $arrobjPropertyPreferences ) || ( false == array_key_exists( 'DO_NOT_REQUIRE_PHONE_NUMBER', $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_PHONE_NUMBER', $arrobjPropertyPreferences ) && false == array_key_exists( 'ALLOW_EMAIL_OR_PHONE_NUMBER_AS_REQUIRED', $arrobjPropertyPreferences ) ) ) ) {
								$boolIsValid &= $this->valCustomerPhoneNumbers( true );
						}
					} elseif( true == $boolIsFromGenerateQuote || true == $boolIsLiveChat || ( false == valArr( $arrobjPropertyPreferences ) || ( false == array_key_exists( 'DO_NOT_REQUIRE_PHONE_NUMBER', $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_PHONE_NUMBER', $arrobjPropertyPreferences ) ) ) ) {
							$boolIsValid &= $this->valCustomerPhoneNumbers( true );
					}
				}

				if( false == $boolIsFromGenerateQuote && true == valArr( $this->getCustomerPhoneNumbers() ) ) {
					$boolIsValid &= $this->valCustomerPhoneNumbers( false );
				}

				if( true == $boolIsValidateGuestCardFields && false == $boolIsLiveChat && true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'SHOW_AND_REQUIRE_ZIP_CODE_OPTION', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valCurrentPostalCode();
				}
				break;

			case 'multi_contact_insert_premium_guest_card_prospect_portal':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();

				if( false == $boolIsMaskedPhoneNumber ) {
					$boolIsValid &= $this->valUsername( $objDatabase, true, true );
				}

				if( false == $boolIsLiveChat && true == $boolIsMaskedPhoneNumber && true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'ALLOW_EMAIL_OR_PHONE_NUMBER_AS_REQUIRED', $arrobjPropertyPreferences ) ) {
					if( false == valArr( $this->getCustomerPhoneNumbers() ) && true == is_null( $this->m_strUsername ) ) {
						$boolIsValid &= $this->valEmailOrPhoneNumber();
					} elseif( false == is_null( $this->m_strUsername ) ) {
						$boolIsValid &= $this->valUsername( $objDatabase, true, true );
					}
				} elseif( true == $boolIsMaskedPhoneNumber ) {
					$boolIsValid &= $this->valUsername( $objDatabase, true, true );
				}

				if( true == $boolIsMaskedPhoneNumber && false == $boolIsLiveChat && true == valArr( $this->getCustomerPhoneNumbers() ) ) {
					if( ( false == valArr( $arrobjPropertyPreferences ) || ( false == array_key_exists( 'DO_NOT_REQUIRE_PHONE_NUMBER', $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_PHONE_NUMBER', $arrobjPropertyPreferences ) && false == array_key_exists( 'ALLOW_EMAIL_OR_PHONE_NUMBER_AS_REQUIRED', $arrobjPropertyPreferences ) ) ) ) {
						$boolIsValid &= $this->valCustomerPhoneNumbers( true );
					}
				} elseif( true == $boolIsLiveChat || ( false == valArr( $arrobjPropertyPreferences ) || ( false == array_key_exists( 'DO_NOT_REQUIRE_PHONE_NUMBER', $arrobjPropertyPreferences ) && false == array_key_exists( 'HIDE_PHONE_NUMBER', $arrobjPropertyPreferences ) ) ) ) {
					$boolIsValid &= $this->valCustomerPhoneNumbers( true );
				}

				if( true == $boolIsValidateGuestCardFields && false == $boolIsLiveChat && true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'MULTI_CONTACT_SHOW_AND_REQUIRE_ZIP_CODE_OPTION', $arrobjPropertyPreferences ) ) {
					$boolIsValid &= $this->valCurrentPostalCode();
				}
				break;

			case 'multi_contact_insert_premium_text_chat_prospect_portal':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, true );

				if( true == valArr( $this->getCustomerPhoneNumbers() ) ) {
					$boolIsValid &= $this->valCustomerPhoneNumbers( true );
				}
				break;

			case 'insert_mobile_schedule_tour':
				$boolIsValid &= $this->valRequiredInsertMobileScheduleTourFields( $arrobjPropertyPreferences, $boolIsLiveChat, $objDatabase );
				break;

			case 'insert_call_center_guest_card':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();

				if( false == is_null( $this->getPrimaryPhoneNumber() ) ) {
					$boolIsValid &= $this->valPrimaryPhoneNumber();
				}
				if( false == is_null( $this->getSecondaryPhoneNumber() ) ) {
					$boolIsValid &= $this->valSecondaryPhoneNumber();
				}

				if( true == valArr( $arrobjPropertyPreferences ) && true == array_key_exists( 'LEASING_CENTER_APPLY_ENTRATA_GUEST_CARD_AND_APPLICATION_SETTINGS', $arrobjPropertyPreferences ) && true == $arrobjPropertyPreferences['LEASING_CENTER_APPLY_ENTRATA_GUEST_CARD_AND_APPLICATION_SETTINGS']->getValue() ) {
					if( ( true == isset( $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL'] ) && 'PHONE_AND_EMAIL' == $arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue() )
					 || ( true == isset( $this->m_arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL'] )
					 && 'PHONE_OR_EMAIL' == $this->m_arrobjPropertyPreferences['GC_REQUIRE_PHONE_NUMBER_AND_EMAIL']->getValue() && true == is_null( $this->getPrimaryPhoneNumber() ) ) ) {
						$boolIsValid &= $this->valUsername( $objDatabase, true, true, true );
					} else {
						$boolIsValid &= $this->valUsername( $objDatabase, false, true );
					}
				} else {
					$boolIsValid &= $this->valUsername( $objDatabase, false, true );
				}
				break;

			case 'insert_sms_guest_card':
				$boolIsValid &= $this->valNameFirst();
				break;

			case 'insert_confirmation':
			case 'password_confirmation':
				$boolIsValid &= $this->valPassword();
				break;

			case 'resident_portal_insert':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valPassword();
				break;

			case 'insert_move_in':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valBirthDate( $boolBirthDateIsRequired = true, $boolIsCheckAbove18 = false );
				$boolIsValid &= $this->valUsername( $objDatabase, false, $boolIsAllowDuplicateEmail );
				$boolIsValid &= $this->valTaxNumberEncryptedForMoveIn();
				break;

			case 'validate_ils_guest_card':
				// Allow guest cards with missing last name to be inserted so that they can be further processed			*

				$boolIsValid &= $this->valNameFirst();
				// check that either valid phone number or valid email is present
				if( false == is_null( $this->getUsername() ) ) {
					$boolIsValid &= $this->valIlsGuestCardUsername();
				} else {
					$boolIsValid &= ( true == valArr( $this->getCustomerPhoneNumbers() ) ) && ( new CCustomerPhoneNumber() )->validateCustomerPhoneNumbers( ( new CCustomerPhoneNumber() )->loadCustomerPhoneNumbers( ( array ) $this->getCustomerPhoneNumbers(), NULL, $this->getCid() ), true );
				}
				break;

			case 'resident_works_application_quick_view':
				$boolIsValid &= $this->valUsername( $objDatabase, false, true );
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				break;

			case 'resident_works_application_preferences':
				break;

			case 'communication_settings':
				$boolIsValid &= $this->valBlockMessageCenterEmail();
				$boolIsValid &= $this->valBlockMessageCenterSms( $objDatabase );
				$boolIsValid &= $this->valBlockContactPointsEmail();
				break;

			case 'rpc_send_mits_leads':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumberOrUsername( $objDatabase, $boolIsAllowDuplicateEmail );
				break;

			case 'validate_phone_numbers':
				return true;
				break;

			case 'validate_phone_number':
				return true;
				break;

			case 'validate_name_first':
				$boolIsValid &= $this->valNameFirst();
				break;

			case 'validate_name_last':
				$boolIsValid &= $this->valNameLast();
				break;

			case 'validate_parent_name_first':
				$boolIsValid &= $this->valParentNameFirst();
				break;

			case 'validate_parent_name_last':
				$boolIsValid &= $this->valParentNameLast();
				break;

			case 'validate_parent_email':
				$boolIsValid &= $this->valParentEmail();
				break;

//			case 'validate_identification_expiration':
//				$boolIsValid &= $this->valIdentificationExpiration();
//				break;

			case 'import_data_leads':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valBirthDate();
				$boolIsValid &= $this->valUsername( $objDatabase, false );
				break;

			case 'match_other_applicants':
				$strSSNSectionName = 'SSN';

				$strSSNSectionName = CTaxIdType::createService()->getAbbreviations()[( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) ) ? ( $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_CITIZEN']->getValue() ) : ( true == array_key_exists( 'BASIC_INFO_TAX_ID_CITIZEN', $arrobjPropertyApplicationPreferences ) ? $arrobjPropertyApplicationPreferences['BASIC_INFO_TAX_ID_NONCITIZEN']->getValue() : '' )];
				$strSSNSectionName = valStr( $strSSNSectionName ) ? $strSSNSectionName : 'SSN';

				$boolIsValid &= $this->validateMatchOtherApplicants( $intApplicationId, $strSSNSectionName, $objDatabase );
				$boolIsValid &= $this->valNameFirst();

				if( false == $boolIsCorporateLease ) {
					$boolIsValid &= $this->valNameLast();
				}
				if( CCustomerType::NOT_RESPONSIBLE != $this->getCustomerTypeId() ) {
					$boolIsValid &= $this->valPreferredLocaleCode( $arrobjPropertyApplicationPreferences, $objProperty, $objDatabase );
				}

				if( true == valArr( $arrobjPropertyPreferences ) && ( ( true == in_array( 'NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT', array_keys( $arrobjPropertyPreferences ) ) && 0 != $arrobjPropertyPreferences['NON_RESPONSIBLE_OCCUPANT_AGE_LIMIT']->getValue() ) || ( true == array_key_exists( 'REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && true == $arrobjPropertyApplicationPreferences['REQUIRE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() && CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() ) ) && CCustomerType::GUARANTOR != $this->getCustomerTypeId() && ( false == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) || ( true == array_key_exists( 'HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE', $arrobjPropertyApplicationPreferences ) && ( true == is_null( $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) || true == $arrobjPropertyApplicationPreferences['HIDE_PEOPLE_CO_APPLICANT_NON_RESPONSIBLE_BIRTHDATE']->getValue() ) ) ) ) {
					$boolIsValid &= $this->valBirthDate( true );
				}
				break;

			case 'review_and_send_notices_email_update':
				$boolIsValid &= $this->valUsername( $objDatabase, true, false, false );
				break;

			case 'lease_modification':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valCustomerTypeId();
				$boolIsValid &= $this->valCustomerRelationshipId();

				$boolIsValid &= $this->valUsername( $objDatabase, false );
				if( false == is_null( $this->getSecondaryPhoneNumber() ) ) {
					$boolIsValid &= $this->valSecondaryPhoneNumber();
				}

				$intRequiredApplicantAge = NULL;
				$intMaxApplicantAge      = NULL;

				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
					$intApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
				}

				if( true == in_array( $this->getCustomerTypeId(), [ CCustomerType::PRIMARY, CCustomerType::GUARANTOR, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT ] ) ) {
					$intRequiredApplicantAge = ( true == isset( $intApplicantAge ) ) ? $intApplicantAge : 18;

				} else {
					if( CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() ) {
						$intMaxApplicantAge = ( true == isset( $intApplicantAge ) ) ? $intApplicantAge : 18;
					}
				}

				if( true == valStr( $this->getBirthDate() ) ) {
					$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge, $intMaxApplicantAge );
				}
				break;

				// @fixme : need to remove this case after the changes of i18n phone numbers in current file
			case 'mid_lease_modification':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valCustomerTypeId();
				$boolIsValid &= $this->valCustomerRelationshipId();

				$boolIsValid &= $this->valUsername( $objDatabase, false );

				$intRequiredApplicantAge = NULL;
				$intMaxApplicantAge      = NULL;

				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
					$intApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
				}

				if( true == in_array( $this->getCustomerTypeId(), [ CCustomerType::PRIMARY, CCustomerType::GUARANTOR, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT ] ) ) {
					$intRequiredApplicantAge = ( true == isset( $intApplicantAge ) ) ? $intApplicantAge : 18;

				} else {
					if( CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() ) {
						$intMaxApplicantAge = ( true == isset( $intApplicantAge ) ) ? $intApplicantAge : 18;
					}
				}

				if( true == valStr( $this->getBirthDate() ) ) {
					$boolIsValid &= $this->valBirthDate( true, $intRequiredApplicantAge, $intMaxApplicantAge );
				}
				break;

			case 'schedule_transfer_applicant':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valBirthDate( true );

				$boolIsValid &= $this->valUsername( $objDatabase, false );
				if( false == is_null( $this->getSecondaryPhoneNumber() ) ) {
					$boolIsValid &= $this->valSecondaryPhoneNumber();
				}

				$intRequiredApplicantAge = NULL;
				$intMaxApplicantAge      = NULL;

				if( true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
					$intApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
				}

				if( true == in_array( $this->getCustomerTypeId(), [ CCustomerType::PRIMARY, CCustomerType::GUARANTOR, CCustomerType::RESPONSIBLE, CCustomerType::NON_LEASING_OCCUPANT ] ) ) {
					$intRequiredApplicantAge = ( true == isset( $intApplicantAge ) ) ? $intApplicantAge : 18;

				} else {
					if( CCustomerType::NOT_RESPONSIBLE == $this->getCustomerTypeId() ) {
						$intMaxApplicantAge = ( true == isset( $intApplicantAge ) ) ? $intApplicantAge : 18;
					}
				}

				if( true == valStr( $this->getBirthDate() ) ) {
					$boolIsValid &= $this->valBirthDate( false, $intRequiredApplicantAge, $intMaxApplicantAge );
				}
				break;

			case 'review_and_send_notices_email_update':
				$boolIsValid &= $this->valUsername( $objDatabase, true, false, false );
				break;

			case 'validate_username':
				$boolIsValid &= $this->valUsername( $objDatabase, true, $boolIsAllowDuplicateEmail );
				break;

			case 'validate_gender_for_roommate_interest':
				$boolIsValid &= $this->valGender( true );
				break;

			case 'validate_edit_roommate_from_application':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valUsername();
				break;

			default:
				// default break
				break;
		}

		return $boolIsValid;
	}

	public function validateSpanishTaxIdTypes( $intTaxIdtype, $strTaxNumber ) {
		$strTaxNumber = strtoupper( $strTaxNumber );

		for( $intLength = 0; $intLength < 9;  $intLength++ ) {
			$arrintNum[$intLength] = substr( $strTaxNumber, $intLength, 1 );
		} // if it does not have a valid format it returns error
		if( !preg_match( '((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)', $strTaxNumber ) ) {
			return false;
		}
			if( preg_match( '/(^[0-9]{8}[A-Z]{1}$)/', $strTaxNumber ) ) {
				if( $arrintNum[8] == substr( 'TRWAGMYFPDXBNJZSQVHLCKE', substr( $strTaxNumber, 0, 8 ) % 23, 1 ) ) {
					return true;
				} else {
					return false;
				}
			}
		// algorithm for checking code type CIF

			$intSum = $arrintNum[2] + $arrintNum[4] + $arrintNum[6];
			for( $intLengthCif = 1; $intLengthCif < 8; $intLengthCif += 2 ) {
				$intSum += substr( ( 2 * $arrintNum[$intLengthCif] ), 0, 1 ) + substr( ( 2 * $arrintNum[$intLengthCif] ), 1, 1 );
			}
			$intCif = 10 - substr( $intSum, strlen( $intSum ) - 1, 1 ); // verification of special NIFs (calculated as CIFs)
			if( preg_match( '/^[KLM]{1}/', $strTaxNumber ) ) {
				if( $arrintNum[8] == chr( 64 + $intCif ) ) {
					return true;
				} else {
					return false;
				}
			} // CIF check

			if( preg_match( '/^[ABCDEFGHJNPQRSUVW]{1}/', $strTaxNumber ) ) {
				if( $arrintNum[8] == chr( 64 + $intCif ) || $arrintNum[8] == substr( $intCif, strlen( $intCif ) - 1, 1 ) ) {
					return true;
				} else {
					return false;
				}
			} // NIEs check

			if( preg_match( '/^[T]{1}/', $strTaxNumber ) ) {
				if( $arrintNum[8] == preg_match( '/^[T]{1}[A-Z0-9]{8}$/', $strTaxNumber ) ) {
					return true;
				} else {
					return false;
				}
			}
			if( preg_match( '/^[XYZ]{1}/', $strTaxNumber ) ) {
				if( $arrintNum[8] == substr( 'TRWAGMYFPDXBNJZSQVHLCKE', substr( str_replace( [ 'X', 'Y', 'Z' ], [ '0', '1', '2' ], $strTaxNumber ), 0, 8 ) % 23, 1 ) ) {
					return true;
				} else {
					return false;
				}
			}
		return false;
	}

/**
	 * Other Functions
	 *
	 */

	public function encryptPassword() {

		if( 0 == strlen( $this->getPassword() ) ) {
			return true;
		}

		return $this->setPasswordEncrypted( CHash::createService()->hashCustomer( $this->getPassword() ) );
	}

	public function generateEmailContents( $objClient, $objProperty, $objDatabase, $strSecureBaseName = NULL, $intUniqueId = NULL, $objPropertyEmailRule = NULL ) {

		$objCustomer = $this->getOrFetchCustomer( $objDatabase );
		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$strPrimaryApplicantPreferredLocale = $objCustomer->getPreferredLocaleCode();
			CLocaleContainer::createService()->setPreferredLocaleCode( $strPrimaryApplicantPreferredLocale, $objDatabase );
		}
		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		if( true == valObj( $objProperty, 'CProperty' ) ) {

			$objMarketingMediaAssociation		= $objProperty->fetchActiveMarketingMediaAssociationsByMediaSubTypeId( CMarketingMediaSubType::PROPERTY_LOGO, $objDatabase, true );
		}

		if( true == valObj( $objClient, 'CClient' ) ) {

			$objLogoMarketingMediaAssociation = $objClient->fetchMarketingMediaAssociationByMediaSubTypeId( CMarketingMediaSubType::COMPANY_LOGO, $objDatabase );
		}

		// Create link for new password creation
		$strCreateApplicantPasswordLink = $strSecureBaseName . '/action/create_applicant_password/property[id]/' . ( ( true == is_null( $objProperty->getPropertyId() ) ) ? $objProperty->getId() : $objProperty->getPropertyId() ) . '/applicant[key]/' . urlencode( $intUniqueId ) . '/applicant[id]/' . $this->getId() . '/is_primary_applicant/0/';

		// Email contents
		$strContents = $strCreateApplicantPasswordLink;

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_INTERFACES_PROSPECT_PORTAL, false );

		$objSmarty->assign_by_ref( 'contents', $strContents );
		$objSmarty->assign_by_ref( 'applicant', $this );
		$objSmarty->assign_by_ref( 'marketing_media_association', $objMarketingMediaAssociation );
		$objSmarty->assign_by_ref( 'default_company_media_file', $objLogoMarketingMediaAssociation );

		$objSmarty->assign( 'property_email_rule', $objPropertyEmailRule );
		$objSmarty->assign( 'media_library_uri', CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'CONFIG_COMPANY_NAME_FULL', CONFIG_COMPANY_NAME_FULL );
		$objSmarty->assign( 'image_url',	    CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES );

		$strEmailContents = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'system_emails/applicant_retrieve_password/retrieve_password_email.tpl' );

		return $strEmailContents;
	}

	public function sendEmail( $arrobjPropertyPreferences, $strToEmailAddress, $strEmailContents, $objDatabase, $objPropertyEmailRule = NULL, $objClientDatabase = NULL ) {

		$objCustomer = $this->getOrFetchCustomer( $objClientDatabase );
		if( true == valObj( $objCustomer, 'CCustomer' ) ) {
			$strPrimaryApplicantPreferredLocale = $objCustomer->getPreferredLocaleCode();
			CLocaleContainer::createService()->setPreferredLocaleCode( $strPrimaryApplicantPreferredLocale, $objClientDatabase );
		}
		if( true == array_key_exists( 'FROM_FORGOT_PASSWORD_EMAIL', $arrobjPropertyPreferences ) && false == is_null( $arrobjPropertyPreferences['FROM_FORGOT_PASSWORD_EMAIL']->getValue() ) ) {

			$strFromEmailAddress = $arrobjPropertyPreferences['FROM_FORGOT_PASSWORD_EMAIL']->getValue();

		} else {

			$strFromEmailAddress = CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS;
		}

		$strSubject = 'Password Change Notification';

		$boolIsValid = true;

		switch( NULL ) {
			default:

				$objRetrievePasswordEmail = new CSystemEmail();
				$objRetrievePasswordEmail->setCid( $this->getCid() );
				$objRetrievePasswordEmail->setPropertyId( $this->getPropertyId() );
				$objRetrievePasswordEmail->setSystemEmailTypeId( CSystemEmailType::ENTRATA_PASSWORD );
				$objRetrievePasswordEmail->setFromEmailAddress( $strFromEmailAddress );
				$objRetrievePasswordEmail->setToEmailAddress( $strToEmailAddress );
				$objRetrievePasswordEmail->setSubject( $strSubject );
				$objRetrievePasswordEmail->setHtmlContent( $strEmailContents );
				$objRetrievePasswordEmail->setApplicantId( $this->getId() );
				$objRetrievePasswordEmail->setIsSelfDestruct( 1 );

				$objRetrievePasswordEmail = CSystemEmailLibrary::processSystemEmailByPropertyEmailRules( $objRetrievePasswordEmail, $objPropertyEmailRule );
				if( false == valObj( $objRetrievePasswordEmail, 'CSystemEmail' ) ) {
					return false;
				}
				CLocaleContainer::createService()->unsetPreferredLocaleCode( $objDatabase );
				$objDatabase->begin();

				if( false == $objRetrievePasswordEmail->insert( 1, $objDatabase ) ) {
					$objDatabase->rollback();
					trigger_error( 'Application Error: Failed to send forget password email to ' . $this->getUsername(), E_USER_WARNING );
					$boolIsValid = false;
					break;
				}

				$objDatabase->commit();
		}

		return $boolIsValid;
	}

	public function checkApplicantAge( $intRequiredApplicantAge ) {

		if( true == is_null( $this->getBirthDate() ) ) return NULL;

		$boolIsValid = true;

		if( 0 == $intRequiredApplicantAge ) {
			return $boolIsValid;
		}

		$objBirthDate       = new DateTime( $this->getBirthDate() );
		$objCurrentDate     = new DateTime( 'today' );

		$intAgeDiffYear     = $objBirthDate->diff( $objCurrentDate )->y;
		$intAgeDiffMonth    = $objBirthDate->diff( $objCurrentDate )->m;
		$intAgeDiffDay      = $objBirthDate->diff( $objCurrentDate )->d;

		if( $intAgeDiffYear < $intRequiredApplicantAge || ( $intAgeDiffYear == $intRequiredApplicantAge && ( $intAgeDiffMonth < 0 || ( $intAgeDiffMonth == 0 && $intAgeDiffDay < 0 ) ) ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function getIsMinor( $intRequiredApplicantAge ) {
		$boolIsValid = false;

		$arrstrBirthDate = explode( '/', $this->getBirthDate() );

		$strYear  = ( true == isset( $arrstrBirthDate[2] ) ? ( int ) date( 'Y' ) - ( int ) $arrstrBirthDate[2] : ( int ) date( 'Y' ) );
		$strMonth = ( int ) date( 'm', time() ) - ( int ) date( 'm', strtotime( $this->m_strBirthDate ) );
		$strDay   = ( int ) date( 'd', time() ) - ( int ) date( 'd', strtotime( $this->m_strBirthDate ) );

		if( $strMonth < 0 || ( $strMonth === 0 && ( int ) date( 'd', time() ) < ( int ) date( 'd', strtotime( $this->m_strBirthDate ) ) ) ) {
			$strYear--;
		}

		if( $strYear < 18 && $strYear >= $intRequiredApplicantAge ) {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function valStudentBirthDate( $arrobjPropertyApplicationPreferences = NULL ) {
		$boolIsValid             = true;
		$strSectionName          = 'birth_date';
		$intRequiredApplicantAge = 0;

		if( true == valArr( $arrobjPropertyApplicationPreferences ) && true == array_key_exists( 'REQUIRE_BASIC_INFO_AGE_VARIFICATION', $arrobjPropertyApplicationPreferences ) && 0 < strlen( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) && true == is_numeric( $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue() ) ) {
			$intRequiredApplicantAge = $arrobjPropertyApplicationPreferences['REQUIRE_BASIC_INFO_AGE_VARIFICATION']->getValue();
		}

		if( ( false == isset( $this->m_strBirthDate ) || true == is_null( $this->m_strBirthDate ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date is required.' ) ) );
		} else {
			if( 1 !== CValidation::checkDate( $this->m_strBirthDate ) ) {
				$boolIsValid = false;
				$this->setBirthDate( NULL );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Birth date must be in mm/dd/yyyy format.' ) ) );
			} else {
				if( 0 != $intRequiredApplicantAge && false == $this->checkApplicantAge( $intRequiredApplicantAge ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strSectionName, __( 'Age should be above {%d,0} years.', [ $intRequiredApplicantAge ] ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valEmailOrPhoneNumber() {
		$boolIsValid = true;

		if( false == valArr( $this->getCustomerPhoneNumbers() ) && true == is_null( $this->m_strUsername ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Either email address or phone number is required.' ) ) );
		}

		return $boolIsValid;
	}

	/**
	 * @param $boolIsRequired
	 * @return boolean
	 */
	public function valCustomerPhoneNumbers( $boolIsRequired = false ) {
		$boolIsValid = true;

		$arrobjCustomerPhoneNumbers = ( new CCustomerPhoneNumber() )->loadCustomerPhoneNumbers( ( array ) $this->getCustomerPhoneNumbers(), NULL, $this->getId() );

		if( false == valArr( $arrobjCustomerPhoneNumbers ) && true == $boolIsRequired ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is required.' ) ) );
		}

		$objCustomerPhoneNumber = new CCustomerPhoneNumber();
		if( false == $objCustomerPhoneNumber->validateCustomerPhoneNumbers( $arrobjCustomerPhoneNumbers, $boolIsRequired ) ) {
			$boolIsValid &= false;
			$this->addErrorMsgs( $objCustomerPhoneNumber->getErrorMsgs() );
		}

		return $boolIsValid;
	}

	public function isAboveEighteenYearsOld() {

		$boolIsValid = true;

		if( false == isset( $this->m_strBirthDate ) || 0 == strlen( $this->m_strBirthDate ) || 1 !== CValidation::checkDate( $this->m_strBirthDate ) ) {
			return false;
		}

		$arrstrBirthDate = explode( '/', $this->m_strBirthDate );
		$strYear         = ( int ) date( 'Y' ) - ( int ) $arrstrBirthDate[2];
		$strMonth        = ( int ) date( 'm', time() ) - ( int ) date( 'm', strtotime( $this->m_strBirthDate ) );
		$strDay          = ( int ) date( 'd', time() ) - ( int ) date( 'd', strtotime( $this->m_strBirthDate ) );

		if( ( $strYear < 18 || ( $strYear == 18 && ( $strMonth < 0 || ( $strMonth == 0 && $strDay < 0 ) ) ) ) ) {

			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function mapCorrespondingCustomerData( $objCustomer, $boolReformatTaxNumber = true ) {

		$this->setCid( $objCustomer->getCid() );
		$this->setMaritalStatusTypeId( $objCustomer->getMaritalStatusTypeId() );
		$this->setCustomerId( $objCustomer->getId() );
		$this->setOriginatingCustomerId( $objCustomer->getId() );
		$this->setCompanyName( $objCustomer->getCompanyName() );
		$this->setNamePrefix( $objCustomer->getNamePrefix() );
		$this->setNameFirst( $objCustomer->getNameFirst() );
		$this->setNameLast( $objCustomer->getNameLast() );
		$this->setNameMiddle( $objCustomer->getNameMiddle() );
		$this->setNameLastMatronymic( $objCustomer->getNameLastMatronymic() );
		$this->setNameSuffix( $objCustomer->getNameSuffix() );
		$this->setNameMaiden( $objCustomer->getNameMaiden() );
		$this->setFaxNumber( $objCustomer->getFaxNumber() );
		$this->setEmailAddress( $objCustomer->getEmailAddress() );
		$this->setTaxNumber( $objCustomer->getTaxNumber( $boolReformatTaxNumber ) );
		$this->setBirthDate( $objCustomer->getBirthDate() );
		$this->setGender( $objCustomer->getGender() );
		$this->setDlNumber( $objCustomer->getDlNumber() );

		$this->setAltNameFirst( $objCustomer->getAltNameFirst() );
		$this->setAltNameLast( $objCustomer->getAltNameLast() );
		$this->setAltNameMiddle( $objCustomer->getAltNameMiddle() );
		$this->setPreferredName( $objCustomer->getPreferredName() );

		$this->setDlStateCode( \Psi\CStringService::singleton()->strtoupper( $objCustomer->getDlStateCode() ) );
		$this->setUsername( $objCustomer->getUsername() );
		$this->setNotes( trim( $objCustomer->getNotes() ) );
		$this->setPasswordEncrypted( $objCustomer->getPassword() );
		$this->setPassword( $this->getPasswordEncrypted() );
		$this->setPasswordConfirm( $this->getPasswordEncrypted() );

		$this->setCurrentStreetLine1( $objCustomer->getPrimaryStreetLine1() );
		$this->setCurrentStreetLine2( $objCustomer->getPrimaryStreetLine2() );
		$this->setCurrentStreetLine3( $objCustomer->getPrimaryStreetLine3() );
		$this->setCurrentCity( $objCustomer->getPrimaryCity() );
		$this->setCurrentStateCode( \Psi\CStringService::singleton()->strtoupper( $objCustomer->getPrimaryStateCode() ) );
		$this->setCurrentPostalCode( $objCustomer->getPrimaryPostalCode() );
		$this->setCurrentCountryCode( $objCustomer->getPrimaryCountryCode() );

		$this->setPreviousStreetLine1( $objCustomer->getPreviousStreetLine1() );
		$this->setPreviousStreetLine2( $objCustomer->getPreviousStreetLine2() );
		$this->setPreviousStreetLine3( $objCustomer->getPreviousStreetLine3() );
		$this->setPreviousCity( $objCustomer->getPreviousCity() );
		$this->setPreviousStateCode( \Psi\CStringService::singleton()->strtoupper( $objCustomer->getPreviousStateCode() ) );
		$this->setPreviousPostalCode( $objCustomer->getPreviousPostalCode() );
		$this->setPreviousCountryCode( $objCustomer->getPreviousCountryCode() );
		$this->setTaxIdTypeId( $objCustomer->getTaxIdTypeId() );

		if( true == valId( $objCustomer->getCompanyIdentificationTypeId() ) ) {
			$this->setCompanyIdentificationTypeId( $objCustomer->getCompanyIdentificationTypeId() );
		}

		if( true == valStr( $objCustomer->getIdentificationNumber() ) ) {
			$this->setIdentificationNumber( $objCustomer->getIdentificationNumber() );
		}
		if( true == valStr( $objCustomer->getStudentIdNumber() ) ) {
			$this->setStudentIdNumber( $objCustomer->getStudentIdNumber() );
		}

		if( true == valStr( $objCustomer->getIdentificationExpiration() ) ) {
			$this->setIdentificationExpiration( $objCustomer->getIdentificationExpiration() );
		}

		return true;
	}

	public function buildApplicationLoginForm( $strBounceUrl, $boolIsPopup = false ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$strEncryptedUserName = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $this->getUsername(), CONFIG_SODIUM_KEY_LOGIN_USERNAME );
		$strEncryptedPassword = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $this->getPasswordEncrypted(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD );

		$objApplicantSsoManager = new \Psi\Core\Applicant\Sso\CApplicantSsoManager( $this );
		$arrmixJwtTokenParams = $objApplicantSsoManager->generateJwtToken();

		// Create Smarty Object
		$objSmarty = new CPsSmarty( PATH_INTERFACES_PROSPECT_PORTAL, false );

		$objSmarty->assign( 'bounce_url', $strBounceUrl );
		$objSmarty->assign( 'username_encrypted', $strEncryptedUserName );
		$objSmarty->assign( 'password_encrypted', $strEncryptedPassword );
		$objSmarty->assign( 'jwt_token', $arrmixJwtTokenParams['token'] );
		$objSmarty->assign( 'is_popup', $boolIsPopup );

		$strFormContents = $objSmarty->nestedFetch( PATH_INTERFACES_COMMON . 'layouts/login_form.tpl' );

		return $strFormContents;
	}

	public function logAuthentication( $intCompanyUserId, $objDatabase, $boolIsLoginFromEntrata = false, $intEntrataUserId = NULL, $intCid = NULL ) {

		if( false == is_null( $this->getAuthenticationLogId() ) && true == is_numeric( $this->getAuthenticationLogId() ) ) {

			$objAuthenticationLog = CAuthenticationLogs::fetchAuthenticationLogByApplicantIdByIdByCid( $this->getId(), $this->getAuthenticationLogId(), $this->getCid(), $objDatabase );

			if( true == valObj( $objAuthenticationLog, 'CAuthenticationLog' ) ) {
				$objAuthenticationLog->setLogoutDatetime( 'NOW()' );
			}
		} else {
			$objAuthenticationLog = $this->createAuthenticationLog();
			$objAuthenticationLog->setAuthenticationLogTypeId( CAuthenticationLogType::PROSPECT_PORTAL );
		}

		if( false == is_null( $objAuthenticationLog ) ) {

			if( false == $objAuthenticationLog->insertOrUpdate( $intCompanyUserId, $objDatabase ) ) {
				trigger_error( 'Failed to insert the authentication log for the Applicant : ' . $this->m_intId, E_USER_WARNING );
			} else {
				if( true == $boolIsLoginFromEntrata ) {

					$objApplication = $this->fetchApplicationById( $this->getApplicationId(), $objDatabase );

					if( false == valObj( $objApplication, 'CApplication' ) ) {
						return false;
					}

					$objEventLibrary = new CEventLibrary();

					$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
					$objEventLibraryDataObject->setDatabase( $objDatabase );

					$intEventType = CEventType::PROSPECT_LOGIN_BY_MANAGER;

					$objEvent = $objEventLibrary->createEvent( [ $objApplication ], $intEventType );
					$objEvent->setCid( $this->getCid() );
					$objEvent->setPropertyId( $objApplication->getPropertyId() );
					$objEvent->setEventDatetime( 'NOW()' );

					if( false == is_null( $intEntrataUserId ) && false == is_null( $intCid ) ) {

						$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCompanyUserByIdByCid( $intEntrataUserId, $intCid, $objDatabase );
						if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
							$objEvent->setReference( $objCompanyUser );
						}
					}

					$objEventLibraryDataObject->setApplication( $objApplication );

					$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $intCompanyUserId, $this->getCid(), $objDatabase );

					if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
						$objEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
						$objEvent->setCompanyEmployee( $objCompanyEmployee );
					}

					$objEventLibrary->buildEventDescription( $this );

					if( false == $objEventLibrary->insertEvent( $intCompanyUserId, $objDatabase ) ) {
						trigger_error( 'Failed to insert event for the Customer : ' . $this->m_intId, E_USER_WARNING );
					}
				}
			}
			$this->setAuthenticationLogId( $objAuthenticationLog->getId() );
		}

		return true;
	}

	// While deleting the applicant we need to delete the related information as well. - NRW

	public function deleteApplicantDetails( $intApplicationId, $intCompanyUserId, $objDatabase ) {

		if( false == $this->getIsDelete() ) {
			return true;
		}

		$intApplicantApplicationCount = \Psi\Eos\Entrata\CApplicantApplications::createService()->fetchApplicantCountByApplicantIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$objApplicantApplication      = $this->fetchApplicantApplicationByApplicationId( $intApplicationId, $objDatabase );
		$arrobjFileSignatures         = ( array ) \Psi\Eos\Entrata\CFileSignatures::createService()->fetchFileSignaturesByApplicationIdByApplicantIdByCid( $intApplicationId, $this->getId(), $this->getCid(), $objDatabase );

		$boolIsValid = true;

		if( true == valObj( $objApplicantApplication, 'CApplicantApplication' ) ) {
			$boolIsValid &= $objApplicantApplication->delete( $intCompanyUserId, $objDatabase );
		}

		if( 1 < $intApplicantApplicationCount ) {
			foreach( $arrobjFileSignatures as $objFileSignature ) {
				$boolIsValid &= $objFileSignature->delete( $intCompanyUserId, $objDatabase );
			}

			return $boolIsValid;
		}

		$arrobjPeopleIncomes      = ( array ) $this->fetchCustomerIncomes( $objDatabase );
		$arrobjPeopleAssets       = ( array ) $this->fetchApplicantAssets( $objDatabase );
		$arrobjAuthenticationLogs = ( array ) $this->fetchAuthenticationLogs( $objDatabase );
		$arrobjAAChanges          = ( array ) $this->fetchApplicantApplicationChanges( $objDatabase );
		$arrobjFileSignatures     = ( array ) $this->fetchFileSignatures( $objDatabase );

		foreach( $arrobjPeopleIncomes as $objPeopleIncome ) {
			$boolIsValid &= $objPeopleIncome->delete( $intCompanyUserId, $objDatabase );
		}

		foreach( $arrobjPeopleAssets as $objPeopleAsset ) {
			$boolIsValid &= $objPeopleAsset->delete( $intCompanyUserId, $objDatabase );
		}

		foreach( $arrobjAuthenticationLogs as $objAuthenticationLog ) {
			$boolIsValid &= $objAuthenticationLog->delete( $intCompanyUserId, $objDatabase );
		}

		foreach( $arrobjAAChanges as $objAAChange ) {
			$boolIsValid &= $objAAChange->delete( $intCompanyUserId, $objDatabase );
		}

		foreach( $arrobjFileSignatures as $objFileSignature ) {
			$boolIsValid &= $objFileSignature->delete( $intCompanyUserId, $objDatabase );
		}

		if( true == $boolIsValid ) {
			$boolIsValid &= $this->delete( $intCompanyUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function setPhoneNumberByPhoneNumberTypeId( $strPhoneNumber, $intPhoneNumberTypeId ) {

		switch( $intPhoneNumberTypeId ) {

			case CPhoneNumberType::FAX:
				$this->setFaxNumber( $strPhoneNumber );
				break;

			default:
				// deafult break
				break;
		}
	}

	public function setPhoneNumberTypes() {

		$boolIsUpdateRequired = false;

		if( false == is_null( $this->getPrimaryPhoneNumberTypeId() ) && false == is_null( $this->getSecondaryPhoneNumberTypeId() ) && $this->getPrimaryPhoneNumberTypeId() == $this->getSecondaryPhoneNumberTypeId() ) {
			$boolIsUpdateRequired = true;
			$this->setSecondaryPhoneNumberTypeId( NULL );
		}

		// First to check if type is already set and according to that data is set or not, else set the type id
		// also, we are not showing the fax number in UI, so any of the type should not be set as Fax - NRW
		if( true == is_null( $this->getPrimaryPhoneNumber() ) ) {

			if( false == is_null( $this->getPhoneNumber() ) ) {
				$boolIsUpdateRequired = true;
				$this->setPrimaryPhoneNumberTypeId( CPhoneNumberType::HOME );

			} elseif( false == is_null( $this->getWorkNumber() ) ) {
				$boolIsUpdateRequired = true;
				$this->setPrimaryPhoneNumberTypeId( CPhoneNumberType::OFFICE );

			} elseif( false == is_null( $this->getMobileNumber() ) ) {
				$boolIsUpdateRequired = true;
				$this->setPrimaryPhoneNumberTypeId( CPhoneNumberType::MOBILE );
			}
		}

		return $boolIsUpdateRequired;
	}

	public function setMoveInDate( $strMoveInDate ) {
		$this->m_strMoveInDate = $strMoveInDate;
	}

	public function setCustomerRelationShipName( $strCustomerRelationShipName ) {
		$this->m_strCustomerRelationshipName = $strCustomerRelationShipName;
	}

	public function getPrimaryPhoneNumber() {
		return $this->m_strPrimaryPhoneNumber;
	}

	public function getMoveInDate() {
		return $this->m_strMoveInDate;
	}

	public function getCustomerRelationShipName() {
		return $this->m_strCustomerRelationshipName;
	}

	public function getLeadPrimaryApplicantPhoneNumber() {
		$strPhoneNumber = $this->getPhoneNumber();
		if( false == valStr( $strPhoneNumber ) ) {
			return 'Not Available';
		}

		return $strPhoneNumber;
	}

	public function getLeadPrimaryApplicantEmailAddress() {
		$strEmailAddress = $this->getEmailAddress();
		if( false == valStr( $strEmailAddress ) ) {
			return 'Not Available';
		}

		return $strEmailAddress;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intPsProductId = NULL ) {

		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = 'SELECT nextval( \'applicants_id_seq\'::text ) AS id';

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert applicant record. The following error was reported.' ) ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();

				return false;
			}

			$arrmixValues = $objDataset->fetchArray();
			$this->setId( $arrmixValues['id'] );

			$objDataset->cleanup();
		}

		// Insert customer
		$strInsertCustomerResult = NULL;

		if( true == is_null( $this->getCustomerId() ) ) {

			if( false == is_null( $this->getAppRemotePrimaryKey() ) && true == is_null( $this->getPropertyId() ) ) {
				trigger_error( 'Property id must be set for integrated applicant.', E_USER_WARNING );
			}

			$objCustomer = CApplicationUtils::loadOrCreateCustomer( $this, $this->getPropertyId(), $objDatabase );

			if( false == valObj( $objCustomer, 'CCustomer' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load customer object.' ) ) );

				return false;
			}

			if( true == $this->getIsClientI18nEnabled() ) {
				$objCustomer->setDetailsField( 'wechat_open_id', $this->getWechatOpenId() );
			}

			if( true == is_null( $objCustomer->getId() ) ) {
				$objCustomer->setId( $objCustomer->fetchNextId( $objDatabase ) );
				$objCustomer->setApplicantId( $this->getId() );
				$objCustomer->setPropertyId( $this->getPropertyId() );

				$strInsertCustomerResult = $objCustomer->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

				if( false == $boolReturnSqlOnly && false == $strInsertCustomerResult ) {
					return false;
				}
			}

			$this->setCustomerId( $objCustomer->getId() );
		}

		// CKT : Below code is temporary code to identify areas within Entrata and Prospect Portal from where duplicate applicant records are getting inserted
		if( CCompanyUser::INTEGRATION != $intCurrentUserId ) {
			$arrintCount = ( array ) fetchData( 'SELECT count( 1 ) AS customer_count FROM applicants WHERE cid = ' . ( int ) $this->getCid() . ' AND customer_id = ' . ( int ) $this->getCustomerId(), $objDatabase );
			if( true == isset( $arrintCount[0]['customer_count'] ) && $arrintCount[0]['customer_count'] > 0 ) {
				$strMessage = sprintf( 'Duplicate applicant record getting inserted for same Customer id %s for cid %s. Request Data : First Name: %s, Last Name: %s, Username: %s, Email Address: %s, DOB: %s, SSN: %s ', $this->getCustomerId(), $this->getCid(), $this->getNameFirst(), $this->getNameLast(), ( false == is_null( $this->getUsername() ) ? $this->getUsername() : 'NULL' ), ( false == is_null( $this->getEmailAddress() ) ? $this->getEmailAddress() : 'NULL' ), ( false == is_null( $this->getBirthDate() ) ? $this->getBirthDate() : 'NULL' ), ( false == is_null( $this->getTaxNumberMasked() ) ? $this->getTaxNumberMasked() : 'NULL' ) );
				if( CPsProduct::API_SERVICES == $intPsProductId ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $strMessage ) );
					return false;
				}
				trigger_error( $strMessage, E_USER_WARNING );
			}
		}

		// call applicants_insert.sql function
		$strSql = parent::insert( $intCurrentUserId, $objDatabase, true );

		if( true == $boolReturnSqlOnly ) {
			return $strInsertCustomerResult . $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {
			// Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert applicant record. The following error was reported.' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to insert applicant record. The following error was reported.' ) ) );
			// Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		if( false == $this->insertOrUpdateCustomerPhoneNumbers( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $intPsProductId = NULL, $boolReformatTaxNumber = true ) {

		$objDataset = $objDatabase->createDataset();

		// Insert customer
		$strInsertCustomerResult = NULL;
		if( true == is_null( $this->getCustomerId() ) ) {
			if( false == is_null( $this->getAppRemotePrimaryKey() ) && true == is_null( $this->getPropertyId() ) ) {
				trigger_error( 'Property id must be set for integrated applicant.', E_USER_WARNING );
			}

			$objCustomer = CApplicationUtils::loadOrCreateCustomer( $this, $this->getPropertyId(), $objDatabase );
			if( false == valObj( $objCustomer, 'CCustomer' ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to load customer object.' ) ) );

				return false;
			}

			if( true == $this->getIsClientI18nEnabled() ) {
				$objCustomer->setDetailsField( 'wechat_open_id', $this->getWechatOpenId() );
			}

			if( true == is_null( $objCustomer->getId() ) ) {
				$objCustomer->setId( $objCustomer->fetchNextId( $objDatabase ) );
				$objCustomer->setApplicantId( $this->getId() );
				$strInsertCustomerResult = $objCustomer->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

				if( false == $boolReturnSqlOnly && false == $strInsertCustomerResult ) {
					return false;
				}
			}

			$this->setCustomerId( $objCustomer->getId() );
		}

		$arrintCount = ( array ) fetchData( 'SELECT customer_id FROM applicants WHERE cid = ' . ( int ) $this->getCid() . ' AND id = ' . ( int ) $this->getId(), $objDatabase );
		if( true == isset( $arrintCount[0]['customer_id'] ) && $arrintCount[0]['customer_id'] != $this->getCustomerId() ) {
			trigger_error( 'Customer id ' . ( int ) $this->getCustomerId() . ' getting updated on applicant ( id : ' . ( int ) $this->getId() . ', cid : ' . ( int ) $this->getCid() . ' )', E_USER_WARNING );
		}

		$arrstrResponse	= ( array ) \Psi\Eos\Entrata\CApplicants::createService()->fetchOriginalApplicantByIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		$this->setOriginalValues( $arrstrResponse );
		$this->setSerializedOriginalValues( serialize( $arrstrResponse ) );

		// call applicants_update.sql function
		$strSql = parent::update( $intCurrentUserId, $objDatabase, true );

		if( true == is_null( $this->getGuestRemotePrimaryKey() ) && true == is_null( $this->getAppRemotePrimaryKey() ) && COccupancyType::COMMERCIAL <> $this->getOccupancyTypeId() && false == $this->getIsFromRenewalOffer() ) {

			CApplicationUtils::syncApplicantToCustomer( NULL, $intCurrentUserId, $objDatabase, $this, false, $boolReformatTaxNumber );
		}

		$objApplicationLogLibrary = new CApplicationLogLibrary();

		if( true == $boolReturnSqlOnly ) {
			$strSql = $strInsertCustomerResult . $strSql;
			$strSql .= $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
			$strSql .= $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_UPDATE, NULL, $intPsProductId );

			return $strSql;
		}

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update applicant record. The following error was reported.' ) ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, __( 'Failed to update applicant record. The following error was reported.' ) ) );
			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrmixValues['type'], $arrmixValues['field'], $arrmixValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		if( false == $this->insertOrUpdateCustomerPhoneNumbers( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		$objDataset->cleanup();

		if( false == $this->insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		if( true == is_null( $this->getApplicationId() ) ) {
			return true;
		}

		return $objApplicationLogLibrary->insertApplicationChanges( $intCurrentUserId, $this, $objDatabase, $boolReturnSqlOnly, VALIDATE_UPDATE, NULL, $intPsProductId );
	}

	public function insertApplicantApplicationChanges( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getApplicationId() ) ) {
			return ( false == $boolReturnSqlOnly ) ? true : NULL;
		}

		$objApplication = $this->fetchApplicationById( $this->getApplicationId(), $objDatabase );

		if( true == valObj( $objApplication, 'CApplication' ) ) {
			if( CLeaseIntervalType::LEASE_MODIFICATION == $objApplication->getLeaseIntervalTypeId() ) {
				return ( false == $boolReturnSqlOnly ) ? true : NULL;
			}
		}

		$this->unSerializeAndSetOriginalValues();

		$arrobjApplicantApplicationChanges = [];

		if( $this->getTaxNumberEncrypted() != $this->getOriginalValueByFieldName( 'tax_number_encrypted' ) ) {
			$strOldValue = $this->getOriginalValueByFieldName( 'tax_number_masked' );
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::SSN, $strOldValue, $this->getTaxNumberMasked() );
		}

		if( $this->getNameFirst() != $this->getOriginalValueByFieldName( 'name_first' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::NAME_FIRST, $this->getOriginalValueByFieldName( 'name_first' ), $this->getNameFirst() );
		}

		if( $this->getNameLast() != $this->getOriginalValueByFieldName( 'name_last' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::NAME_LAST, $this->getOriginalValueByFieldName( 'name_last' ), $this->getNameLast() );
		}

		if( $this->getUsername() != $this->getOriginalValueByFieldName( 'username' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::EMAIL_ADDRESS, $this->getOriginalValueByFieldName( 'username' ), $this->getUsername() );
		}

		if( true == $this->getIsClientI18nEnabled() ) {
			$objApplicantDetails = json_decode( $this->getOriginalValueByFieldName( 'details' ) );
			if( true == valObj( $objApplicantDetails, 'stdClass' ) && true == property_exists( $objApplicantDetails, 'wechat_open_id' ) ) {
				if( $this->getWechatOpenId() != $objApplicantDetails->wechat_open_id ) {
					$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::WECHAT_OPEN_ID, $objApplicantDetails->wechat_open_id, $this->getWechatOpenId() );
				}
			}
		}

		if( $this->getCurrentStreetLine1() != $this->getOriginalValueByFieldName( 'current_street_line1' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::STREET_LINE_1, $this->getOriginalValueByFieldName( 'current_street_line1' ), $this->getCurrentStreetLine1() );
		}

		if( $this->getCurrentStreetLine2() != $this->getOriginalValueByFieldName( 'current_street_line2' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::STREET_LINE_2, $this->getOriginalValueByFieldName( 'current_street_line2' ), $this->getCurrentStreetLine2() );
		}

		if( $this->getCurrentStreetLine3() != $this->getOriginalValueByFieldName( 'current_street_line3' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::STREET_LINE_3, $this->getOriginalValueByFieldName( 'current_street_line3' ), $this->getCurrentStreetLine3() );
		}

		if( $this->getCurrentCity() != $this->getOriginalValueByFieldName( 'current_city' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::CITY, $this->getOriginalValueByFieldName( 'current_city' ), $this->getCurrentCity() );
		}

		if( $this->getCurrentStateCode() != $this->getOriginalValueByFieldName( 'current_state_code' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::STATE, $this->getOriginalValueByFieldName( 'current_state_code' ), $this->getCurrentStateCode() );
		}

		if( $this->getCurrentPostalCode() != $this->getOriginalValueByFieldName( 'current_postal_code' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::ZIP_CODE, $this->getOriginalValueByFieldName( 'current_postal_code' ), $this->getCurrentPostalCode() );
		}

		if( $this->getBirthDate() != $this->getOriginalValueByFieldName( 'birth_date' ) ) {
			$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::DOB, $this->getOriginalValueByFieldName( 'birth_date' ), $this->getBirthDate() );
		}

//		if( $this->getIdentificationValue() != $this->getOriginalValueByFieldName( 'identification_value' ) ) {
//			$strNewIdentificationValue = $this->getIdentificationValue();
//			$this->setIdentificationValue( $this->getOriginalValueByFieldName( 'identification_value' ) );
//			$strOldValue = $this->getIdentificationNumber();
//			$this->setIdentificationValue( $strNewIdentificationValue );
//
//			if( $strOldValue != $this->getIdentificationNumber() ) {
//				$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::ID_NUMBER, $strOldValue, $this->getIdentificationNumber() );
//			}
//		}

		if( $this->getPrimaryPhoneNumberTypeId() != $this->getOriginalValueByFieldName( 'primary_phone_number_type_id' ) || $this->getPrimaryPhoneNumber() != $this->getPhoneNumberByPhoneNumberTypeId( $this->getOriginalValueByFieldName( 'primary_phone_number_type_id' ), $boolIsLoadOriginalValue = true ) ) {
			$arrmixPhoneNumbers = $this->getCustomerPhoneNumbers();
			if( true == valArr( $arrmixPhoneNumbers ) ) {
				foreach( $arrmixPhoneNumbers as $arrmixPhoneNumber ) {
					if( true == isset( $arrmixPhoneNumber['is_primary'] ) && $this->getOriginalValueByFieldName( 'primary_phone_number_type_id' ) == $arrmixPhoneNumber['phone_number_type'] ) {
						$strNewPhoneNumber = $arrmixPhoneNumber['phone_number'];
					}
				}

				if( true == valStr( $strNewPhoneNumber ) ) {
					$strOriginalPhoneNumber = $this->getOriginalValueByFieldName( 'primary_phone_number' ) . ' - ' . CPhoneNumberType::createService()->getPhoneNumberTypeNameByPhoneNumberTypeId( $this->getOriginalValueByFieldName( 'primary_phone_number_type_id' ) );
					$strNewPhoneNumber      .= ' - ' . CPhoneNumberType::createService()->getPhoneNumberTypeNameByPhoneNumberTypeId( $this->getPrimaryPhoneNumberTypeId() );
					if( $strOriginalPhoneNumber != $strNewPhoneNumber ) {
						$arrobjApplicantApplicationChanges[] = $this->createApplicantApplicationChange( CApplicantApplicationChangeType::PRIMARY_PHONE_NUMBER, $strOriginalPhoneNumber, $strNewPhoneNumber );
					}
				}
			}
		}

		$boolIsValid = true;
		$strSql      = '';

		$arrobjApplicantApplicationChanges = array_reverse( $arrobjApplicantApplicationChanges );

		CApplicationUtils::formatDateTypeApplicantApplicationChages( $arrobjApplicantApplicationChanges );

		if( true == valArr( $arrobjApplicantApplicationChanges ) ) {
			return CApplicantApplicationChanges::bulkInsert( $arrobjApplicantApplicationChanges, $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return ( false == $boolReturnSqlOnly ) ? $boolIsValid : $strSql;
	}

	public function createApplicantApplicationChange( $intApplicantApplicationChangeTypeId, $strOldValue, $strNewValue ) {

		$objApplicantApplicationChange = new CApplicantApplicationChange();
		$objApplicantApplicationChange->setCid( $this->getCid() );
		$objApplicantApplicationChange->setApplicationId( $this->getApplicationId() );
		$objApplicantApplicationChange->setApplicantId( $this->getId() );
		$objApplicantApplicationChange->setApplicantApplicationChangeTypeId( $intApplicantApplicationChangeTypeId );
		$objApplicantApplicationChange->setOldValue( $strOldValue );
		$objApplicantApplicationChange->setNewValue( $strNewValue );

		return $objApplicantApplicationChange;
	}

	public function setRequestData( $arrstrApplicantRequestData, $arrmixFormFields = NULL, $boolOverrideNullValues = false ) {
		if( true == valArr( $arrstrApplicantRequestData ) ) {
			$arrstrApplicantRequestData = array_filter( $arrstrApplicantRequestData );
			$arrmixRequestForm          = mergeIntersectArray( $arrstrApplicantRequestData, $arrmixFormFields );
		}

		if( false == $boolOverrideNullValues ) {
			foreach( $arrmixRequestForm as $strMixkey => $strRequestForm ) {
				if( true == is_null( $strRequestForm ) ) {
					unset( $arrmixRequestForm[$strMixkey] );
				}
			}
		}
		$this->setValues( $arrmixRequestForm, $boolStripSlashes = false );
	}

	public function unsetPhoneNumbers( $arrintNonExistingPhoneNumberTypes ) {

		if( false == valArr( $arrintNonExistingPhoneNumberTypes ) ) {
			return;
		}

	}

	public function createEmailOutgoingEvent( $intCompanyUserId, $strSubject, $objApplication, $objDatabase ) {

		$objEventLibrary = new CEventLibrary();

		$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
		$objEventLibraryDataObject->setDatabase( $objDatabase );
		$objEventLibraryDataObject->setApplicant( $this );

		$objEvent = $objEventLibrary->createEvent( [ $objApplication ], CEventType::EMAIL_OUTGOING );
		$objEvent->setCid( $this->getCid() );

		$objCompanyUser = \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $intCompanyUserId, $this->getCid(), $objDatabase );

		if( true == valObj( $objCompanyUser, 'CCompanyUser' ) ) {
			$objEvent->setCompanyUser( $objCompanyUser );
		}

		$objEvent->setEventDatetime( 'NOW()' );
		$objEvent->setCustomerId( $this->getCustomerId() );
		$objEvent->setNotes( $strSubject );

		$objCompanyEmployee = \Psi\Eos\Entrata\CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $intCompanyUserId, $this->getCid(), $objDatabase );

		if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) ) {
			$objEvent->setCompanyEmployeeId( $objCompanyEmployee->getId() );
			$objEvent->setCompanyEmployee( $objCompanyEmployee );
		}

		return $objEventLibrary;
	}

	public function addMilitaryDetails( $arrobjPeople, $intCurrentUserId, $objDatabase, $intPrimaryCustomerId, $objApplication ) {

		if( false == valArr( $arrobjPeople ) ) return;

		$arrintCustomerIds = [];
		$boolIsPrimaryApplicantUpdated = true;
		$boolIsZipcodeFilled = false;

		foreach( $arrobjPeople as $objPeople ) {
			if( true == valObj( $objPeople, 'CApplicant' ) && CCustomerType::RESPONSIBLE == $objPeople->getCustomerTypeId() ) {
				$arrintCustomerIds[] = $objPeople->getCustomerId();
			}
		}

		if( true == valArr( $arrintCustomerIds ) ) {
			$arrintExistingMilitaryCustomerIds = CCustomerMilitaryDetails::fetchExistingCustomerMilitaryDetailByCustomerIdsByCid( $arrintCustomerIds, $this->getCid(), $objDatabase );

			$arrintTemp = [];

			foreach( $arrintExistingMilitaryCustomerIds as $arrintCustomerId ) {
				$arrintTemp[] = $arrintCustomerId['customer_id'];
			}

			$arrintExistingMilitaryCustomerIds = $arrintTemp;

			$arrintNewCustomerIds = array_diff( $arrintCustomerIds, $arrintExistingMilitaryCustomerIds );

			$arrobjCustomerMilitaryDetails = [];

			foreach( $arrintNewCustomerIds as $intCustomerId ) {
				$objCustomerMilitaryDetail = new CCustomerMilitaryDetail();
				$objCustomerMilitaryDetail->setCid( $this->getCid() );
				$objCustomerMilitaryDetail->setMilitaryStatusId( CMilitaryStatus::NON_MILITARY );
				$objCustomerMilitaryDetail->setCustomerId( $intCustomerId );
				$arrobjCustomerMilitaryDetails[] = $objCustomerMilitaryDetail;
			}
		}

		$objPrimaryCustomerMilitaryDetail  = CCustomerMilitaryDetails::fetchCustomerMilitaryDetailByCustomerIdByCid( $intPrimaryCustomerId, $this->getCid(), $objDatabase );
		if( true == valObj( $objPrimaryCustomerMilitaryDetail, 'CCustomerMilitaryDetail' ) && $objPrimaryCustomerMilitaryDetail->getMilitaryDependentStatusTypeId() != CMilitaryDependentStatusType::WITH_DEPENDENT ) {
			$boolIsPrimaryApplicantUpdated = false;
			$objPrimaryCustomerMilitaryDetail->setMilitaryDependentStatusTypeId( CMilitaryDependentStatusType::WITH_DEPENDENT );

			$boolIsZipcodeFilled = ( false == is_null( $objPrimaryCustomerMilitaryDetail->getDutyAddressPostalCode() ) ) ? true : false;
		}

		switch( NULL ) {
			default:

				$objDatabase->begin();

				if( true == valArr( $arrobjCustomerMilitaryDetails ) && false == CCustomerMilitaryDetails::bulkInsert( $arrobjCustomerMilitaryDetails, $intCurrentUserId, $objDatabase ) ) {
					$objDatabase->rollback();
					break;
				}

				if( false == $boolIsPrimaryApplicantUpdated ) {
					if( false == $objPrimaryCustomerMilitaryDetail->Update( $intCurrentUserId, $objDatabase ) ) {
						$objDatabase->rollback();
						break;
					}

					if( true == $boolIsZipcodeFilled ) {

						$objApplicationDataObject = new CApplicationDataObject();
						$objApplicationDataObject->m_objApplication				= $objApplication;
						$objApplicationDataObject->m_objApplicantApplication	= $objApplication->fetchApplicantApplicationByApplicantId( $this->getId(), $objDatabase );
						$objApplicationDataObject->m_objDatabase 				= $objDatabase;
						$objApplicationDataObject->m_intCompanyUserId			= $intCurrentUserId;
						$objApplicationDataObject->m_objProperty                = $this->getOrFetchProperty( $objDatabase );
						$objApplicationDataObject->m_objCustomerMilitaryDetail  = $objPrimaryCustomerMilitaryDetail;
						$objApplicationDataObject->m_objClient                  = $this->fetchClient( $objDatabase );

						$objApplicationScheduledChargesLibrary = new CApplicationScheduledChargesLibrary( $objApplicationDataObject );

						if( false == $objApplicationScheduledChargesLibrary->rebuildApplicationScheduledCharges( $arrintArOriginIds = NULL, true, $boolIsFromEntrata = true ) ) {
							$objDatabase->rollback();
							break;
						}

					}

				}

				$objDatabase->commit();
		}

	}

	public function fetchPublishedCustomerAssets( $objDatabase ) {
		return \Psi\Eos\Entrata\CCustomerAssets::createService()->fetchPublishedCustomerAssetsByCustomerIdByCid( $this->getCustomerId(), $this->getCid(), $objDatabase );
	}

	public function fetchSendConsumerReportStatus( $intApplicationId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					CASE
		                WHEN send_consumer_report IS TRUE THEN 1
		                ELSE 0
		              END AS  send_consumer_report
				 	 FROM
						applicants a
						JOIN applicant_applications aa ON ( aa.applicant_id = a.id AND aa.cid = a.cid and aa.application_id = ' . ( int ) $intApplicationId . ' )
					 WHERE
						a.cid = ' . ( int ) $intCid . '
						AND aa.applicant_id = ' . ( int ) $this->getId();

		return fetchData( $strSql, $objDatabase );
	}

	public function isFosterOrLiveInAttendant() {

		if( COccupancyType::AFFORDABLE != $this->getOccupancyTypeId() ) return false;
		if( ( CDefaultCustomerRelationship::HUD_FOSTER_CHILD == $this->getDefaultCustomerRelationshipId() && \CSubsidyTracsVersion::TRACS_2_0_3A == $this->getSubsidyTracsVersionId() ) || CDefaultCustomerRelationship::HUD_LIVE_IN_AID == $this->getDefaultCustomerRelationshipId() ) {
			return true;
		}
		return false;
	}

	public function isFosterAdult() {

		if( COccupancyType::AFFORDABLE != $this->getOccupancyTypeId() ) return false;

		return ( CDefaultCustomerRelationship::HUD_FOSTER_ADULT == $this->getDefaultCustomerRelationshipId() );
	}

	public function isDependent() {

		if( COccupancyType::AFFORDABLE != $this->getOccupancyTypeId() ) return false;

		return ( CDefaultCustomerRelationship::HUD_DEPENDENT == $this->getDefaultCustomerRelationshipId() );
	}

	public function valAltNameFirst( $boolAltNameFirstRequired, $boolAddErrorMessage = true, $strSectionName = NULL ) {

		$boolIsValid = true;

		$strSectionName = __( '{%s,0} First Name', [ $strSectionName ] );

		if( ( true == is_null( $this->getAltNameFirst() ) || 0 == strlen( $this->getAltNameFirst() ) ) ) {
			if( true == $boolAltNameFirstRequired ) {
				$boolIsValid = false;
			}
			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alt_name_first', __( '{%s,0} is required.', [ $strSectionName ] ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}

		}

		return $boolIsValid;
	}

	public function valAltNameLast( $boolAltNameLastRequired, $boolAddErrorMessage = true, $strSectionName = NULL ) {
		$boolIsValid = true;

		$strSectionName = __( '{%s,0} Last Name', [ $strSectionName ] );
		if( true == is_null( $this->getAltNameLast() ) || 0 == strlen( $this->getAltNameLast() ) ) {
			if( true == $boolAltNameLastRequired ) {
				$boolIsValid = false;
			}
			if( true == $boolAddErrorMessage ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'alt_name_last', __( '{%s,0} is required.', [ $strSectionName ] ), ERROR_CODE_NAME_REQUIRE, [ 'label' => __( '{%s,0}', [ $strSectionName ] ), 'step_id' => CApplicationStep::BASIC_INFO ] ) );
			}
		}

		return $boolIsValid;
	}

	public function createPhoneNumber( $strPhoneNumber ) {
		$objPhoneNumber = new \i18n\CPhoneNumber( $strPhoneNumber );
		return $objPhoneNumber;
	}

	public function setIsFromRenewalOffer( $boolSkipSyncWithCustomerContacts ) {
		$this->m_boolSkipSyncWithCustomerContacts = $boolSkipSyncWithCustomerContacts;
	}

	public function getIsFromRenewalOffer() : bool {
		return $this->m_boolSkipSyncWithCustomerContacts;
	}

	public function fetchOneTimeLinkByEncryptedKey( $strEncryptedKey, $objProspectPortalDatabase ) {
        return COneTimeLinks::fetchOneTimeLinksByApplicantIdByKeyEncryptedByManagementId( $this->getId(), $strEncryptedKey, $this->getCid(), $objProspectPortalDatabase );
    }

	public function valCurrentMonthlyPaymentAmount( $boolRequired = false ) {

		$boolIsValid = true;

		if( ( true == $boolRequired || 0 < strlen( $this->getCurrentMonthlyPaymentAmount() ) ) && ( 0 > $this->getCurrentMonthlyPaymentAmount() || false == is_numeric( $this->getCurrentMonthlyPaymentAmount() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_monthly_payment_amount', __( 'Current rent or mortgage is required.' ), '', [ 'label' => __( 'Current Address' ), 'step_id' => CApplicationStep::ADDITIONAL_INFO ] ) );
		}

		return $boolIsValid;
	}

	public function isCurrentCustomerPIIChanged() {

		$arrstrPIIChangedColumns = [ 'NameFirst', 'NameMiddle', 'NameLast', 'TaxNumberMasked' ];

		$strOldDOB = valStr( $this->getOriginalValueByFieldName( 'birth_date' ) ) ? date_format( new DateTime( $this->getOriginalValueByFieldName( 'birth_date' ) ), 'Y-m-d' ) : '';
		$strCurrentDOB = valStr( $this->getBirthDate() ) ? date_format( new DateTime( $this->getBirthDate() ), 'Y-m-d' ) : '';

		if( $strOldDOB != $strCurrentDOB || true == valArr( array_intersect_key( array_flip( $arrstrPIIChangedColumns ), $this->getChangedColumns() ) ) ) {
			return true;
		}

		return false;
	}

	public function insertOrUpdateCustomerPhoneNumbers( $intCurrentUserId, $objDatabase ) {

		$boolIsValid = true;
		$boolIsDelete = true;

		if( true == $this->getIsApplicantDeduped() ) {
			$boolIsDelete = false;
		}

		$arrobjCustomerPhoneNumbers = ( new CCustomerPhoneNumber() )->loadCustomerPhoneNumbers( ( array ) $this->getCustomerPhoneNumbers(), $this->getCustomerId(), $this->getCid(), $objDatabase );
		if( true == valArr( $arrobjCustomerPhoneNumbers ) && false == ( new CCustomerPhoneNumber() )->insertOrUpdateCustomerPhoneNumber( $arrobjCustomerPhoneNumbers, $intCurrentUserId, $this->getCid(), $objDatabase, $boolIsDelete, $this->getIsOnlyAdditionalPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, __( 'Failed to insert or update customer phone number.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function createSystemEmailBlock( $intEmailBlockTypeId ) {
		$strCustomerEmailAddress = ( false == valStr( $this->m_strUsername ) ) ? $this->m_strEmailAddress : $this->m_strUsername;
		$objSystemEmailBlock = new CSystemEmailBlock();
		$objSystemEmailBlock->setCid( $this->getCid() );
		$objSystemEmailBlock->setEmailBlockTypeId( $intEmailBlockTypeId );
		$objSystemEmailBlock->setEmailAddress( $strCustomerEmailAddress );
		$objSystemEmailBlock->setCustomerId( $this->getCustomerId() );
		$objSystemEmailBlock->setIsPublished( 1 );

		return $objSystemEmailBlock;
	}

	public function createSmsEnrollment( $intMessageBlockTypeId = NULL ) {

		$objSmsEnrollment = new CSmsEnrollment();
		$objSmsEnrollment->setCid( $this->m_intCid );
		$objSmsEnrollment->setMessageTypeId( $intMessageBlockTypeId );
		$objSmsEnrollment->setCustomerId( $this->getCustomerId() );
		$objSmsEnrollment->setEnrollmentDatetime( date( 'Y-m-d H:i:s' ) );

		return $objSmsEnrollment;
	}

	public function fetchSmsEnrollmentByMessageTypeId( $intMessageTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CSmsEnrollments::createService()->fetchSmsEnrollmentByCidByCustomerIdByMessageTypeId( $this->m_intCid, $this->getCustomerId(), $intMessageTypeId, $objDatabase );
	}

    public function updateApplicantData( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        if( false == $this->getAllowDifferentialUpdate() ) {
            $boolUpdate = true;
        } else {
            $boolUpdate = false;
        }

        $strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marital_status_type_id = ' . $this->sqlMaritalStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'MaritalStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' marital_status_type_id = ' . $this->sqlMaritalStatusTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originating_customer_id = ' . $this->sqlOriginatingCustomerId(). ',' ; } elseif( true == array_key_exists( 'OriginatingCustomerId', $this->getChangedColumns() ) ) { $strSql .= ' originating_customer_id = ' . $this->sqlOriginatingCustomerId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_remote_primary_key = ' . $this->sqlGuestRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'GuestRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' guest_remote_primary_key = ' . $this->sqlGuestRemotePrimaryKey() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' app_remote_primary_key = ' . $this->sqlAppRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'AppRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' app_remote_primary_key = ' . $this->sqlAppRemotePrimaryKey() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_remote_primary_key = ' . $this->sqlRenewalRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RenewalRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' renewal_remote_primary_key = ' . $this->sqlRenewalRemotePrimaryKey() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix(). ',' ; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix(). ',' ; } elseif( true == array_key_exists( 'NameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden(). ',' ; } elseif( true == array_key_exists( 'NameMaiden', $this->getChangedColumns() ) ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber(). ',' ; } elseif( true == array_key_exists( 'MobileNumber', $this->getChangedColumns() ) ) { $strSql .= ' mobile_number = ' . $this->sqlMobileNumber() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_number = ' . $this->sqlWorkNumber(). ',' ; } elseif( true == array_key_exists( 'WorkNumber', $this->getChangedColumns() ) ) { $strSql .= ' work_number = ' . $this->sqlWorkNumber() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber(). ',' ; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_masked = ' . $this->sqlTaxNumberMasked(). ',' ; } elseif( true == array_key_exists( 'TaxNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_masked = ' . $this->sqlTaxNumberMasked() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate(). ',' ; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender(). ',' ; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' height = ' . $this->sqlHeight(). ',' ; } elseif( true == array_key_exists( 'Height', $this->getChangedColumns() ) ) { $strSql .= ' height = ' . $this->sqlHeight() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weight = ' . $this->sqlWeight(). ',' ; } elseif( true == array_key_exists( 'Weight', $this->getChangedColumns() ) ) { $strSql .= ' weight = ' . $this->sqlWeight() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupation_id = ' . $this->sqlOccupationId(). ',' ; } elseif( true == array_key_exists( 'OccupationId', $this->getChangedColumns() ) ) { $strSql .= ' occupation_id = ' . $this->sqlOccupationId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eye_color = ' . $this->sqlEyeColor(). ',' ; } elseif( true == array_key_exists( 'EyeColor', $this->getChangedColumns() ) ) { $strSql .= ' eye_color = ' . $this->sqlEyeColor() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hair_color = ' . $this->sqlHairColor(). ',' ; } elseif( true == array_key_exists( 'HairColor', $this->getChangedColumns() ) ) { $strSql .= ' hair_color = ' . $this->sqlHairColor() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_number_encrypted = ' . $this->sqlDlNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'DlNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' dl_number_encrypted = ' . $this->sqlDlNumberEncrypted() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dl_state_code = ' . $this->sqlDlStateCode(). ',' ; } elseif( true == array_key_exists( 'DlStateCode', $this->getChangedColumns() ) ) { $strSql .= ' dl_state_code = ' . $this->sqlDlStateCode() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_identification_type_id = ' . $this->sqlCompanyIdentificationTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyIdentificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_identification_type_id = ' . $this->sqlCompanyIdentificationTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_value = ' . $this->sqlIdentificationValue(). ',' ; } elseif( true == array_key_exists( 'IdentificationValue', $this->getChangedColumns() ) ) { $strSql .= ' identification_value = ' . $this->sqlIdentificationValue() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' identification_expiration = ' . $this->sqlIdentificationExpiration(). ',' ; } elseif( true == array_key_exists( 'IdentificationExpiration', $this->getChangedColumns() ) ) { $strSql .= ' identification_expiration = ' . $this->sqlIdentificationExpiration() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername(). ',' ; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' facebook_user = ' . $this->sqlFacebookUser(). ',' ; } elseif( true == array_key_exists( 'FacebookUser', $this->getChangedColumns() ) ) { $strSql .= ' facebook_user = ' . $this->sqlFacebookUser() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_access_on = ' . $this->sqlLastAccessOn(). ',' ; } elseif( true == array_key_exists( 'LastAccessOn', $this->getChangedColumns() ) ) { $strSql .= ' last_access_on = ' . $this->sqlLastAccessOn() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_login_attempts = ' . $this->sqlMaxLoginAttempts(). ',' ; } elseif( true == array_key_exists( 'MaxLoginAttempts', $this->getChangedColumns() ) ) { $strSql .= ' max_login_attempts = ' . $this->sqlMaxLoginAttempts() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_error_count = ' . $this->sqlLoginErrorCount(). ',' ; } elseif( true == array_key_exists( 'LoginErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' login_error_count = ' . $this->sqlLoginErrorCount() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_password_change = ' . $this->sqlRequiresPasswordChange(). ',' ; } elseif( true == array_key_exists( 'RequiresPasswordChange', $this->getChangedColumns() ) ) { $strSql .= ' requires_password_change = ' . $this->sqlRequiresPasswordChange() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt_name_first = ' . $this->sqlAltNameFirst(). ',' ; } elseif( true == array_key_exists( 'AltNameFirst', $this->getChangedColumns() ) ) { $strSql .= ' alt_name_first = ' . $this->sqlAltNameFirst() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt_name_middle = ' . $this->sqlAltNameMiddle(). ',' ; } elseif( true == array_key_exists( 'AltNameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' alt_name_middle = ' . $this->sqlAltNameMiddle() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alt_name_last = ' . $this->sqlAltNameLast(). ',' ; } elseif( true == array_key_exists( 'AltNameLast', $this->getChangedColumns() ) ) { $strSql .= ' alt_name_last = ' . $this->sqlAltNameLast() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName(). ',' ; } elseif( true == array_key_exists( 'PreferredName', $this->getChangedColumns() ) ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last_matronymic = ' . $this->sqlNameLastMatronymic(). ',' ; } elseif( true == array_key_exists( 'NameLastMatronymic', $this->getChangedColumns() ) ) { $strSql .= ' name_last_matronymic = ' . $this->sqlNameLastMatronymic() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_id_type_id = ' . $this->sqlTaxIdTypeId(). ',' ; } elseif( true == array_key_exists( 'TaxIdTypeId', $this->getChangedColumns() ) ) { $strSql .= ' tax_id_type_id = ' . $this->sqlTaxIdTypeId() . ','; $boolUpdate = true; }
        if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
        $strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
        $strSql .= ' updated_on = \'NOW()\' ';

        $strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

        if( true == $boolReturnSqlOnly ) {
            return ( true == $boolUpdate ) ? $strSql : false;
        } else {
            if( true == $boolUpdate ) {
                if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
                    if( true == $this->getAllowDifferentialUpdate() ) {
                        $this->resetChangedColumns();
                        return true;
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
    }

}

?>