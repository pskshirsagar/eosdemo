<?php

class CCompanyEmployeeContactType extends CBaseCompanyEmployeeContactType {

	const MAINTENANCE			= 1;
	const GENERAL				= 2;
	const CORPORATE				= 3;
	const PARKING_AND_TOWING	= 4;
	const SECURITY				= 5;
	const CUSTOM				= 6;

	public static $c_arrmixCompanyEmployeeContactTypeName = [
		self::MAINTENANCE			=> 'Maintenance',
		self::GENERAL				=> 'Leasing Office',
		self::PARKING_AND_TOWING	=> 'Parking & Towing',
		self::SECURITY				=> 'Security'
	];

	public static $c_arrmixAllCompanyEmployeeContactTypeName = [
		self::MAINTENANCE			=> 'Maintenance',
		self::GENERAL				=> 'Leasing Office',
		self::PARKING_AND_TOWING	=> 'Parking & Towing',
		self::SECURITY				=> 'Security',
		self::CUSTOM				=> 'Custom'
	];

	public static $c_arrmixCompanyEmployeeContactTypeIds = [
		self::MAINTENANCE,
		self::GENERAL,
		self::PARKING_AND_TOWING,
		self::SECURITY,
		self::CUSTOM
	];

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>
