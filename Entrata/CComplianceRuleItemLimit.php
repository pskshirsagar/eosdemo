<?php

class CComplianceRuleItemLimit extends CBaseComplianceRuleItemLimit {

	protected $m_boolIsDataChange;

	protected $m_strComplianceItemName;

	protected $m_intComplianceJobItemId;

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valComplianceRuleId() {
		return true;
	}

	public function valComplianceItemId() {
		return true;
	}

	public function valRequiredAmount() {
		return true;
	}

	public function valDeletedBy() {
		return true;
	}

	public function valDeletedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setComplianceItemName( $strComplianceItemName ) {
		$this->m_strComplianceItemName = $strComplianceItemName;
	}

	public function setIsDataChange( $boolIsDataChange ) {
		$this->m_boolIsDataChange = $boolIsDataChange;
	}

	public function setComplianceJobItemId( $intComplianceJobItemId ) {
		$this->set( 'm_intComplianceJobItemId', CStrings::strToIntDef( $intComplianceJobItemId, NULL, false ) );
	}

	/**
	 * Get Functions
	 *
	 */

	public function getComplianceItemName() {
		return $this->m_strComplianceItemName;
	}

	public function getIsDataChange() {
		return $this->m_boolIsDataChange;
	}

	public function getComplianceJobItemId() {
		return $this->m_intComplianceJobItemId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['compliance_item_name'] ) && $boolDirectSet ) {
			$this->m_strComplianceItemName = trim( $arrmixValues['compliance_item_name'] );
		} elseif( isset( $arrmixValues['compliance_item_name'] ) ) {
			$this->setComplianceItemName( $arrmixValues['compliance_item_name'] );
		}

		if( true == isset( $arrmixValues['allow_differential_update'] ) ) {
			$this->setAllowDifferentialUpdate( $arrmixValues['allow_differential_update'] );
		}

		if( isset( $arrmixValues['compliance_job_item_id'] ) && $boolDirectSet ) {
			$this->m_intComplianceJobItemId = trim( $arrmixValues['compliance_job_item_id'] );
		} elseif( isset( $arrmixValues['compliance_job_item_id'] ) ) {
			$this->setComplianceJobItemId( $arrmixValues['compliance_job_item_id'] );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$arrmixOriginalValues		= [];

		if( true == $this->getAllowDifferentialUpdate() ) {

			$this->unSerializeAndSetOriginalValues();
			$arrmixOriginalValues		= $this->m_arrstrOriginalValues;

			foreach( $arrmixOriginalValues as $strKey => $strOriginalValue ) {

				if( $strOriginalValue == 'f' ) {
					$strOriginalValue = 'false';
				}

				if( $strOriginalValue == 't' ) {
					$strOriginalValue = 'true';
				}

				$strSqlFunctionName = 'sql' . str_replace( ' ', '', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $strKey ) ) );

				if( false == in_array( $strKey, [ 'allow_differential_update', 'updated_by', 'updated_on', 'created_by', 'created_on' ] ) && CStrings::reverseSqlFormat( $this->$strSqlFunctionName() ) != $strOriginalValue ) {
					$this->setIsDataChange( true );
				}
			}
		}

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

	// @TODO: Temprory code, once done with migration in Entrata will remove this code.

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( CWorker::USER_ID_FLAG_TO_MIGRATE_COMPLIANCE_DATA != $intCurrentUserId ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );
		} else {
			$this->setDatabase( $objDatabase );

			$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

			$strSql = 'INSERT INTO
					' . static::TABLE_NAME . '( id, cid, compliance_rule_id, compliance_item_id, required_amount, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
			VALUES ( ' .
				$strId . ', ' .
				$this->sqlCid() . ', ' .
				$this->sqlComplianceRuleId() . ', ' .
				$this->sqlComplianceItemId() . ', ' .
				$this->sqlRequiredAmount() . ', ' .
				$this->sqlDeletedBy() . ', ' .
				$this->sqlDeletedOn() . ', ' .
				$this->sqlUpdatedBy() . ', ' .
				$this->sqlUpdatedOn() . ', ' .
				$this->sqlCreatedBy() . ', ' .
				$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

			if( true == $boolReturnSqlOnly ) {
				return $strSql;
			} else {
				return $this->executeSql( $strSql, $this, $objDatabase );
			}
		}
	}

}
?>