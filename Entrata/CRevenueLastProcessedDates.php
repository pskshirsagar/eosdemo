<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueLastProcessedDates
 * Do not add any new functions to this class.
 */

class CRevenueLastProcessedDates extends CBaseRevenueLastProcessedDates {

    public static function fetchRevenueLastProcessedDateByProcessNameByCid( $strProcessName, $intCid, $objDatabase ) {
    	$strSql = 'SELECT * FROM revenue_last_processed_dates WHERE cid = ' . ( int ) $intCid . ' AND process_name = \'' . $strProcessName . '\' LIMIT 1';
    	return parent::fetchRevenueLastProcessedDate( $strSql, $objDatabase );
    }

}
?>