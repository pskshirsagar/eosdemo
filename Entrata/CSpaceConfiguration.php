<?php

class CSpaceConfiguration extends CBaseSpaceConfiguration {

	const SPACE_CONFIG_CONVENTIONAL     = 1;
	const SPACE_CONFIG_PRIVATE          = 2;
	const SPACE_CONFIG_SHARED           = 3;
	const SPACE_CONFIG_DEFAULT          = 0;
	const ENTIRE_UNIT_SPACE_COUNT       = 0;

	protected $m_strDefaultSpaceConfigurationName;
	protected $m_intPropertyCount;
	protected $m_intCustomUnitSpaceCount;

	protected $m_arrstrOccupancyTypeNames;

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Custom Space Option Title is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valUnitSpaceCount() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitSpaceCount() ) || CSpaceConfiguration::ENTIRE_UNIT_SPACE_COUNT > $this->getUnitSpaceCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_space_count', __( 'Space Count is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objDatabase ) {

		$boolIsValid = true;

		$intPropertySpaceConfigurationCount = CPropertySpaceConfigurations::fetchActivePropertySpaceConfigurationCountBySpaceConfigurationIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( 0 < $intPropertySpaceConfigurationCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( '{%s, 0} is currently associated to one or more properties. This space option must first be removed from all properties before it can be deleted.', [ $this->getName() ] ) ) );
			}

		return $boolIsValid;
	}

	public function valOccupancyTypeIds( $arrintPropertyIds, $objDatabase ) {
		$boolIsValid = true;

		if( true == valArr( $arrintPropertyIds ) ) {
			if( false == valArr( $this->getOccupancyTypeIds() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Please select at least one occupancy type.' ) ) );
			} elseif( true == valStr( $strInvalidPropertyName = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyIdMissingGivenOccupancyTypesByIdsByOccupancyTypeIdsByCid( $arrintPropertyIds, $this->getOccupancyTypeIds(), $this->getCid(), $objDatabase ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Property {%s, 0} is not associated with any selected occupancy type.', [ $strInvalidPropertyName ] ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $arrintPropertyIds = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valUnitSpaceCount( $objDatabase );
				$boolIsValid &= $this->valOccupancyTypeIds( $arrintPropertyIds, $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependantInformation( $objDatabase );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intCurrentUserId, $objDatabase );
		}

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( 'NOW()' );

		return $this->update( $intCurrentUserId, $objDatabase );
	}

	public function getDefaultSpaceConfigurationName() {
		return $this->m_strDefaultSpaceConfigurationName;
	}

	public function setDefaultSpaceConfigurationName( $strDefaultSpaceConfigurationName ) {
		$this->m_strDefaultSpaceConfigurationName = $strDefaultSpaceConfigurationName;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->m_intPropertyCount = $intPropertyCount;
	}

	public function getNameWithCount() {
		return $this->getName() . ( ( $this->getDefaultSpaceConfigurationId() == CDefaultSpaceConfiguration::SPACE_CONFIGURATION_ENTIRE_UNIT ) ? '(' . __( 'Entire Unit' ) . ')' : '(' . $this->getUnitSpaceCount() . ')' );
	}

	public function setOccupancyTypeNames( $arrstrOccupancyTypeNames ) {
		$this->m_arrstrOccupancyTypeNames = $arrstrOccupancyTypeNames;
	}

	public function getOccupancyTypeNames() {
		return $this->m_arrstrOccupancyTypeNames;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['default_space_configuration_name'] ) ) $this->setDefaultSpaceConfigurationName( $arrmixValues['default_space_configuration_name'] );

		if( true == isset( $arrmixValues['property_count'] ) ) $this->setPropertyCount( $arrmixValues['property_count'] );

		if( true == isset( $arrmixValues['occupancy_type_names'] ) && true == valStr( $arrmixValues['occupancy_type_names'] ) ) $this->setOccupancyTypeNames( explode( ',', $arrmixValues['occupancy_type_names'] ) );
	}

	public function getCustomUnitSpaceCount() {
		if( $this->getUnitSpaceCount() == CSpaceConfiguration::ENTIRE_UNIT_SPACE_COUNT ) {
			$this->m_intCustomUnitSpaceCount = __( 'Entire Unit' );
		} else {
			$this->m_intCustomUnitSpaceCount = __( '{%d, unit_space_count}', [ 'unit_space_count' => $this->getUnitSpaceCount() ] );
		}
		return $this->m_intCustomUnitSpaceCount;
	}

}
?>
