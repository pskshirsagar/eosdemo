<?php

class CDocumentAddendaType extends CBaseDocumentAddendaType {

	const POLICY_DOCUMENT				= 1;
	const LEASE_DOCUMENT				= 2;
	const MASTER_GUARANTOR				= 3;
	const LEASE_ADDENDUM				= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>