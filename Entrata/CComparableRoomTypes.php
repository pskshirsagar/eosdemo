<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CComparableRoomTypes
 * Do not add any new functions to this class.
 */

class CComparableRoomTypes extends CBaseComparableRoomTypes {

	public static function fetchComparableRoomTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CComparableRoomType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchComparableRoomType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CComparableRoomType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllComparableRoomTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM comparable_room_types WHERE is_published = true';
		return self::fetchCachedObjects( $strSql, 'CComparableRoomType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>