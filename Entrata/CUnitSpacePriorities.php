<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUnitSpacePriorities
 * Do not add any new functions to this class.
 */

class CUnitSpacePriorities extends CBaseUnitSpacePriorities {

	public static function fetchCustomUnitSpacePrioritiesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false, $boolIncludeDeletedFloorPlans = false ) {

		$strCheckDeletedUnitsSql 		 = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql 	 = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesAsSql  = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND deleted_on IS NULL' : '';
		$strCheckDeletedUnitFloorPlanSql = ( false == $boolIncludeDeletedFloorPlans ) ? ' AND pf.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						usp.*,
						pf.floorplan_name AS floorplan_name,
						pu.unit_number AS unit_number,
						ut.name AS unit_type_name,
						CASE
							WHEN ( 1 < ( SELECT COUNT( id ) FROM unit_spaces WHERE property_unit_id = us.property_unit_id AND cid = us.cid ' . $strCheckDeletedUnitSpacesAsSql . ' ) )
								THEN us.space_number
							ELSE NULL
						END AS space_number,
						CASE
							WHEN us.unit_space_status_type_id IN (' . implode( ',', CUnitSpaceStatusType::$c_arrintUnavailableUnitSpaceStatusTypes ) . ' )
								THEN 0
							ELSE 1
						END AS is_unit_available
					FROM
						unit_space_priorities usp
						JOIN unit_spaces us ON ( us.id = usp.unit_space_id AND us.cid = usp.cid AND us.property_id = ' . ( int ) $intPropertyId . ' AND us.cid = ' . ( int ) $intCid . ' ' . $strCheckDeletedUnitSpacesSql . ' )
						JOIN property_units pu ON ( pu.id = us.property_unit_id AND pu.cid = us.cid ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN property_floorplans pf ON ( pf.id = pu.property_floorplan_id AND pf.cid = pu.cid ' . $strCheckDeletedUnitFloorPlanSql . ' )
						LEFT JOIN unit_types ut ON ( ut.id = pu.unit_type_id AND ut.cid = pu.cid )
					WHERE
						usp.cid = ' . ( int ) $intCid . '
					ORDER BY
						usp.priority_order';

		return self::fetchUnitSpacePriorities( $strSql, $objDatabase );
	}

	public static function fetchMaxPriorityOrderByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						CASE
							WHEN MAX( priority_order ) IS NULL
								THEN 0
							ELSE MAX( priority_order )
						END AS max_priority_order
					FROM
						unit_space_priorities
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrmixResponse[0]['max_priority_order'] ) ) {
			return $arrmixResponse[0]['max_priority_order'];
		}

		return 0;
	}

	public static function fetchUnitSpacePriorityByUnitSpaceIdByCid( $intUnitSpaceId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM unit_space_priorities WHERE unit_space_id = ' . ( int ) $intUnitSpaceId . ' AND cid = ' . ( int ) $intCid . ' LIMIT 1';
		return self::fetchUnitSpacePriority( $strSql, $objDatabase );
	}

	public static function fetchUnitSpacePrioritiesByPropertyIdAbovePriorityOrderByCid( $intPropertyId, $intPriorityOrder, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM unit_space_priorities WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND priority_order > ' . ( int ) $intPriorityOrder . ' ORDER BY priority_order';
		return self::fetchUnitSpacePriorities( $strSql, $objDatabase );
	}
}
?>