<?php

class CJobStakeholder extends CBaseJobStakeholder {

	public function valCompanyUserId() {
		$boolIsValid = true;

		if( false == valId( $this->getCompanyUserId() ) ) {
			$strErrorMessage = __( 'Stakeholder Name is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_user_id', $strErrorMessage ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valRole() {
		$boolIsValid = true;

		if( false == valStr( $this->getRole() ) ) {
			$strErrorMessage = __( 'Role is required.' );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'role', $strErrorMessage ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCompanyUserId();
				$boolIsValid &= $this->valRole();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>