<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPropertyInstructions
 * Do not add any new functions to this class.
 */

class CPropertyInstructions extends CBasePropertyInstructions {

	public static function fetchPropertyInstructionsByCidByPropertyIdByPropertyInstructionId( $intCid, $intPropertyId, $intPropertyInstructionId, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intPropertyInstructionId ) ) return NULL;

		$strSql = 'SELECT
						*,
						CASE WHEN property_id IS NOT NULL THEN 0 ELSE 1 END AS is_client_level 
					FROM
						property_instructions 
					WHERE
						cid = ' . ( int ) $intCid . ' 
						AND ( property_id = ' . ( int ) $intPropertyId . ' OR property_id IS NULL )
						AND property_instruction_id = ' . ( int ) $intPropertyInstructionId . '
						AND deleted_by IS NULL
					ORDER BY
						property_id, id';

		return self::fetchPropertyInstructions( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyInstructionsByCidByPropertyIdByPropertyInstructionIds( $intCid, $intPropertyId, $arrintPropertyInstructionIds, $objClientDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valArr( $arrintPropertyInstructionIds ) ) return NULL;

		$strSql = 'SELECT
						*,
						CASE WHEN property_id IS NOT NULL THEN 0 ELSE 1 END AS is_client_level 
					FROM
						property_instructions 
					WHERE
						cid = ' . ( int ) $intCid . ' 
						AND ( property_id = ' . ( int ) $intPropertyId . ' OR property_id IS NULL )
						AND property_instruction_id IN ( ' . implode( ',', $arrintPropertyInstructionIds ) . ' )
						AND deleted_by IS NULL
					ORDER BY
						property_id, id';

		return self::fetchPropertyInstructions( $strSql, $objClientDatabase );
	}

	public static function fetchPropertyInstructionsByCidByPropertyInstructionIds( $intCid, $arrintPropertyInstructionIds, $objClientDatabase, $boolIsClientLevelOnly = true ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyInstructionIds ) ) return NULL;

		$strPropertyIdClauseSql = NULL;

		if( true == $boolIsClientLevelOnly ) {
			$strPropertyIdClauseSql = ' AND property_id IS NULL ';
		}

		$strSql = 'SELECT
						* 
					FROM
						property_instructions 
					WHERE
						cid = ' . ( int ) $intCid
						. $strPropertyIdClauseSql . '
						AND property_instruction_id IN ( ' . implode( ',', $arrintPropertyInstructionIds ) . ' )
						AND deleted_by IS NULL
					ORDER BY
						id';

		return self::fetchPropertyInstructions( $strSql, $objClientDatabase );
	}

}
?>