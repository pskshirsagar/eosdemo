<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlReconciliationItems
 * Do not add any new functions to this class.
 */

class CGlReconciliationItems extends CBaseGlReconciliationItems {

	public static function fetchGlReconciliationItemsByReconciliationIdOrByReversingGlReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase ) {
		if( true == is_null( $intGlReconciliationId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gri.id,
						gri.cid,
						gri.gl_reconciliation_id,
						gri.reversing_gl_reconciliation_id,
						gri.amount,
						gri.notes,
						gri.reference_number,
						gri.date,
						gri.is_initial_item,
						gri.is_bank_balance,
						gri.is_reconciled,
						gr.gl_reconciliation_status_type_id,
						ba.account_name,
						gr.statement_date,
						gr.beginning_date
					FROM
						gl_reconciliation_items gri
						JOIN gl_reconciliations gr ON ( gri.gl_reconciliation_id = gr.id AND gri.cid = gr.cid )
						JOIN bank_accounts ba ON ( gr.bank_account_id = ba.id AND gr.cid = ba.cid )
					WHERE
						gr.cid = ' . ( int ) $intCid . '
						AND (
								(	gri.is_initial_item = 1
									AND gri.is_reconciled = 1
									AND ( 	( gri.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
												AND gri.reversing_gl_reconciliation_id IS NULL
											)
											OR gri.reversing_gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
										)
								)
								OR ( gri.is_initial_item = 1
									 AND gri.is_reconciled = 0
									 AND gri.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
									)
								OR ( gri.is_initial_item = 0
									 AND ( gri.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
									 		OR gri.reversing_gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . '
									 	 )
									)
							)';

		return self::fetchGlReconciliationItems( $strSql, $objDatabase );
	}

	public static function fetchGlReconciliationItemsByGlReconciliationIdsOrByReversingGlReconciliationIdByCid( $arrintGlReconciliationIds, $intReversingGlReconciliationId, $intCid, $intStatementDate, $objClientDatabase, $arrintSelectedReconciliationItemIds = [] ) {
		if( false == valIntArr( $arrintGlReconciliationIds ) ) {
			return NULL;
		}

		$strCondition = ( true == valIntArr( $arrintSelectedReconciliationItemIds ) ) ? ' AND gri.id IN ( ' . implode( ',', $arrintSelectedReconciliationItemIds ) . ' ) ' : '';

		$strSql = 'SELECT
						gri.*
					FROM
						gl_reconciliation_items gri
						LEFT JOIN gl_reconciliations gr ON gri.cid = gr.cid AND gri.reversing_gl_reconciliation_id = gr.id
					WHERE
						gri.cid = ' . ( int ) $intCid . '
						AND gri.gl_reconciliation_id IN ( ' . implode( ',', $arrintGlReconciliationIds ) . ' ) '
						 . $strCondition . '
						AND gri.created_on >= \'2014-04-18\' -- This condition is added to hide historical added reconciliation items from Uncleared Transactions.
						AND gri.date <= \'' . $intStatementDate . '\'
						AND (
								CASE WHEN gr.id IS NOT NULL THEN
									gr.statement_date >= \'' . $intStatementDate . '\'
								ELSE
									1 = 1
								END
						)
						AND ( ( gri.reversing_gl_reconciliation_id IS NULL OR gri.reversing_gl_reconciliation_id = ' . ( int ) $intReversingGlReconciliationId . ' ) 
								OR (
										( gri.reversing_gl_reconciliation_id IS NOT NULL
										AND gri.reversing_gl_reconciliation_id <> ' . ( int ) $intReversingGlReconciliationId . ') 
									)
							)
						AND gri.is_initial_item = 0';

		return self::fetchGlReconciliationItems( $strSql, $objClientDatabase );
	}

	public static function fetchOpenGlReconciliationItemsByGlReconciliationIdsOrByReversingGlReconciliationIdByIsInitialItemByCid( $arrintGlReconciliationIds, $intReversingGlReconciliationId, $intIsInitial, $intCid, $intStatementDate, $objClientDatabase, $arrintUnCheckedReconcileItemIds = [] ) {
		if( false == valIntArr( $arrintGlReconciliationIds ) ) {
			return NULL;
		}

		$strCondition = ( true == valIntArr( $arrintUnCheckedReconcileItemIds ) ) ? ' AND gri.id IN ( ' . implode( ',', $arrintUnCheckedReconcileItemIds ) . ' )' : '';

		$strSql = 'SELECT
						gri.*
					FROM
						gl_reconciliation_items gri
						LEFT JOIN gl_reconciliations gr ON gri.cid = gr.cid AND gri.reversing_gl_reconciliation_id = gr.id
					WHERE
						gri.cid = ' . ( int ) $intCid . '
						AND gri.gl_reconciliation_id IN ( ' . implode( ',', $arrintGlReconciliationIds ) . ' )'
						 . $strCondition . '
						AND gri.created_on >= \'2014-04-18\' -- This condition is added to hide historical added reconciliation items from Uncleared Transactions.
						AND gri.date <= \'' . $intStatementDate . '\'
						AND (
								CASE WHEN gr.id IS NOT NULL THEN
									gr.statement_date >= \'' . $intStatementDate . '\'
								ELSE
									1 = 1
								END
							)
						AND	(
								(	gri.is_initial_item = ' . ( int ) $intIsInitial . '
									AND gri.is_reconciled = 0
									AND ( gri.reversing_gl_reconciliation_id IS NULL OR gri.reversing_gl_reconciliation_id = ' . ( int ) $intReversingGlReconciliationId . ' )
								)
								OR ( gri.is_initial_item = ' . ( int ) $intIsInitial . '
									 AND gri.is_reconciled = 0
									 AND gri.gl_reconciliation_id = ' . ( int ) $intReversingGlReconciliationId . ' 
									)
								OR ( gri.is_initial_item = ' . ( int ) $intIsInitial . '
									 AND gri.is_reconciled = 1
									 AND ( ( gri.gl_reconciliation_id = ' . ( int ) $intReversingGlReconciliationId . ' AND gri.reversing_gl_reconciliation_id IS NOT NULL )
											OR
											(
												( gri.reversing_gl_reconciliation_id IS NOT NULL
												AND gri.reversing_gl_reconciliation_id <> ' . ( int ) $intReversingGlReconciliationId . ') 
											)
											OR
											(
												( gri.gl_reconciliation_id  <> ' . ( int ) $intReversingGlReconciliationId . '
												AND gri.reversing_gl_reconciliation_id = ' . ( int ) $intReversingGlReconciliationId . ' ) 
											)
										)
									)
							)';

		return self::fetchGlReconciliationItems( $strSql, $objClientDatabase );
	}

	public static function fetchClearedGlReconciliationItemsByGlReconciliationIdsOrByReversingGlReconciliationIdByIsInitialItemByCid( $arrintGlReconciliationIds, $intReversingGlReconciliationId, $intIsInitial, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGlReconciliationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliation_items
					WHERE
						gl_reconciliation_id IN ( ' . implode( ',', $arrintGlReconciliationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND created_on >= \'2014-04-18\' -- This condition is added to hide historical added reconciliation items from Uncleared Transactions.
						AND ( reversing_gl_reconciliation_id IS NULL OR reversing_gl_reconciliation_id = ' . ( int ) $intReversingGlReconciliationId . ' )
						AND is_initial_item = ' . ( int ) $intIsInitial . '
						AND is_reconciled = 1';

		return self::fetchGlReconciliationItems( $strSql, $objClientDatabase );
	}

	public static function fetchGlReconciliationItemsByGlReconciliationItemIdsByCid( $arrintGlReconciliationItemIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintGlReconciliationItemIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_reconciliation_items
					WHERE
						id IN ( ' . implode( ',', $arrintGlReconciliationItemIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchGlReconciliationItems( $strSql, $objClientDatabase );
	}

	public static function fetchGlReconciliationItemsByReconciliationIdByCid( $intGlReconciliationId, $intCid, $objDatabase, $arrintUnCheckedReconcileItemIds = [] ) {

		if( true == is_null( $intGlReconciliationId ) ) {
			return NULL;
		}

		$strCondition = ( true == valIntArr( $arrintUnCheckedReconcileItemIds ) ) ? ' AND gri.id IN ( ' . implode( ',', $arrintUnCheckedReconcileItemIds ) . ' ) ' : '';

		$strSql = 'SELECT
						gri.id,
						gri.cid,
						gri.gl_reconciliation_id,
						gri.reversing_gl_reconciliation_id,
						gri.amount,
						gri.notes,
						gri.reference_number,
						gri.date,
						gri.is_initial_item,
						gri.is_bank_balance,
						gri.is_reconciled,
						gr.gl_reconciliation_status_type_id,
						ba.account_name,
						gr.statement_date,
						gr.beginning_date
					FROM
						gl_reconciliation_items gri
						JOIN gl_reconciliations gr ON ( gri.gl_reconciliation_id = gr.id AND gri.cid = gr.cid )
						JOIN bank_accounts ba ON ( gr.bank_account_id = ba.id AND gr.cid = ba.cid )
					WHERE
						gr.cid = ' . ( int ) $intCid
						 . $strCondition . '
						AND ( ( gri.is_initial_item = 0 AND gri.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . ' )
						OR ( gri.is_initial_item = 1 AND ( gri.gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . ' OR gri.reversing_gl_reconciliation_id = ' . ( int ) $intGlReconciliationId . ' ) )
						)';

		return self::fetchGlReconciliationItems( $strSql, $objDatabase );

	}

}
?>