<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMessageBatchTypes
 * Do not add any new functions to this class.
 */

class CMessageBatchTypes extends CBaseMessageBatchTypes {

	public static function fetchMessageBatchTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMessageBatchType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMessageBatchType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMessageBatchType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>