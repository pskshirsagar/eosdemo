<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicationAddOns
 * Do not add any new functions to this class.
 */

class CApplicationAddOns extends CBaseApplicationAddOns {

	public static function fetchApplicationAddOnsApplicantsByApplicantsNameByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $intSearchAddOnType = NULL, $strApplicantsName = NULL, $strAddOnIds = NULL, $strAddOnsGroupIds = NULL ) {

		$strApplicantNameItemTypeCaseCondition = ' 0 AS is_applicant_searched';
		if( 1 == $intSearchAddOnType ) {

			if( false == empty( $strApplicantsName ) ) {

				if( \Psi\CStringService::singleton()->strpos( $strApplicantsName, ' ' ) == true ) {
					$arrstrFullApplicantsName = explode( ' ', $strApplicantsName );
					$strApplicantNameItemTypeCaseCondition = ' CASE
															      WHEN ( apl.name_first ILIKE \'%' . $arrstrFullApplicantsName['0'] . '%\' AND apl.name_last ILIKE \'%' . $arrstrFullApplicantsName['1'] . '%\' ) THEN 1
															      ELSE 0
															    END AS is_applicant_searched';
				} else {
					$strApplicantNameItemTypeCaseCondition = ' CASE
															      WHEN ( apl.name_first ILIKE \'%' . $strApplicantsName . '%\' OR apl.name_last ILIKE \'%' . $strApplicantsName . '%\' ) THEN 1
															      ELSE 0
															    END AS is_applicant_searched';
				}
			}
		} elseif( 2 == $intSearchAddOnType ) {
			if( false == is_null( $strAddOnIds ) ) {
				$strAddOnIdsCondition = ' WHEN ao.id IN ( ' . $strAddOnIds . ' ) THEN 1 ';
			}
			if( false == is_null( $strAddOnsGroupIds ) ) {
				$strAddOnGroupIdsCondition = ' WHEN aog.id IN ( ' . $strAddOnsGroupIds . ' ) THEN 1 ';
			}
			if( true == is_null( $strAddOnIds ) && true == is_null( $strAddOnsGroupIds ) ) {
				$strApplicantNameItemTypeCaseCondition = ' 0 AS is_applicant_searched ';
			} else {
				$strApplicantNameItemTypeCaseCondition = ' CASE
															  ' . $strAddOnIdsCondition . $strAddOnGroupIdsCondition . '
														      ELSE 0
														    END AS is_applicant_searched ';
			}
		}

		$strSql = 'SELECT
						ca.cid,
						ca.id AS application_id,
						ca.lease_id,
						ca.property_id,
						ca.application_completed_on,
						ca.lease_start_date,
						apl.id AS applicant_id,
						apl.name_first || \' \' || COALESCE ( apl.name_middle, \'\' ) || apl.name_last AS applicant_name,
						COALESCE ( apl.phone_number, apl.mobile_number, apl.work_number, NULL ) AS contact_number,
						apl.name_first,
						apl.name_last,
						apl.username AS email_address,
						ai.offer_submitted_on,
						ao.id AS add_on_id,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						aog.id AS add_on_group_id,
						util_get_translated( \'name\', aog.name, aog.details, \'' . \CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_group_name,
						ao.hold_until_date,
						ais.skip_reason,
						ai.event_type_id,
						ai.id AS application_interest_id,
						' . $strApplicantNameItemTypeCaseCondition . '
					FROM
						application_add_ons aao
						JOIN cached_applications ca ON ( aao.cid = ca.cid AND aao.application_id = ca.id )
						JOIN applicant_applications aa ON ( aa.cid = aao.cid AND aa.application_id = aao.application_id AND ca.primary_applicant_id = aa.applicant_id )
						JOIN applicants apl ON ( aao.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN add_ons ao ON ( ao.cid = aao.cid AND ao.id = aao.add_on_id AND ao.show_on_website = true AND ao.deleted_by IS NULL )
						JOIN add_on_groups aog ON ( ao.cid = aog.cid AND ao.property_id = aog.property_id AND ao.add_on_group_id = aog.id AND aog.show_on_website = true AND aog.deleted_by IS NULL )
						LEFT JOIN application_interests ai ON ( ai.cid = aao.cid AND ai.property_id = aao.property_id AND ai.application_id = aao.application_id AND ai.add_on_id = aao.add_on_id AND ( ai.event_type_id IS NULL OR ai.event_type_id = ' . ( int ) CEventType::GENERATE_WAITLIST_OFFER . ' OR ai.event_type_id = ' . ( int ) CEventType::WAITLIST_APPLICATION_REMOVED_BY_PROPERTY . ' ) )
						LEFT JOIN application_interest_skips ais ON ( ais.cid = ai.cid AND ais.property_id = ai.property_id AND ais.application_id = ai.application_id AND ais.application_interest_id = ai.id )
					WHERE
				 		aao.cid = ' . ( int ) $intCid . '
				 		AND aao.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				 	ORDER BY
				 		ao.name,
				 		ca.application_completed_on';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApplicantsPreferedApplicationAddOnsByApplicationIdByPropertyIdByCid( $intApplicationId, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS name
					FROM
						application_add_ons aao
						JOIN cached_applications ca ON ( aao.cid = ca.cid AND aao.application_id = ca.id )
						JOIN applicant_applications aa ON ( aa.cid = aao.cid AND aa.application_id = aao.application_id AND ca.primary_applicant_id = aa.applicant_id )
						JOIN applicants apl ON ( aao.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN add_ons ao ON ( ao.cid = aao.cid AND ao.id = aao.add_on_id )
						LEFT JOIN application_interests ai ON ( ai.cid = aao.cid AND ai.property_id = aao.property_id AND ai.add_on_id = aao.add_on_id )
					WHERE
				 		aao.cid = ' . ( int ) $intCid . '
				 		AND aao.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				 		AND aao.application_id = ' . ( int ) $intApplicationId;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCustomApplicationAddOnsByAddOnIdByApplicationIdByPropertyIdByCid( $intAddOnId, $intApplicationId, $intPropertyId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
					    aao.*
					FROM
					    application_add_ons aao
					WHERE
					    aao.cid = ' . ( int ) $intCid . '
					    AND aao.property_id = ' . ( int ) $intPropertyId . '
					    AND aao.application_id = ' . ( int ) $intApplicationId . '
					    AND aao.add_on_id IN (
					                           SELECT
					                               ao.id
					                           FROM
					                               add_ons ao
					                           WHERE
					                               ao.add_on_group_id =
					                               (
					                                 SELECT
					                                     ao1.add_on_group_id
					                                 FROM
					                                     add_ons ao1
					                                 WHERE
					    		                         ao1.cid = ' . ( int ) $intCid . '
					    								 AND ao1.property_id = 	' . ( int ) $intPropertyId . '
					                                     AND ao1.id = ' . ( int ) $intAddOnId . '
					                                     AND ao1.deleted_on IS NULL
					                                     AND ao1.deleted_by IS NULL
					                               )
					                               AND ao.cid = ' . ( int ) $intCid . '
					    						   AND ao.property_id = ' . ( int ) $intPropertyId . '
					                               AND ao.deleted_on IS NULL
					                               AND ao.deleted_by IS NULL
					    )';

		return self::fetchApplicationAddOns( $strSql, $objClientDatabase );
	}

	public static function fetchCustomAllApplicationAddOnByAddOnGroupIdByApplicationIdByPropertyIdByCid( $intAddOnGroupId, $intApplicationId, $intPropertyId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						aao.*
					FROM
						application_add_ons aao
						JOIN cached_applications ca ON ( aao.cid = ca.cid AND aao.application_id = ca.id )
						JOIN add_ons ao ON ( ao.cid = aao.cid AND ao.id = aao.add_on_id )
					WHERE
				 		aao.cid = ' . ( int ) $intCid . '
				 		AND aao.property_id = ' . ( int ) $intPropertyId . '
				 		AND aao.application_id = ' . ( int ) $intApplicationId . '
						AND ao.add_on_group_id = ' . ( int ) $intAddOnGroupId;

		return self::fetchApplicationAddOns( $strSql, $objClientDatabase );
	}

	public static function fetchReservedApplicationAddOnsByApplicationIdByPropertyIdByCid( $intApplicationId, $intPropertyId, $intCid, $objClientDatabase ) {

		$strSql  = 'SELECT
						aoc.*,
						aog.id AS add_on_group_id
					FROM
						application_add_ons aao
					    JOIN cached_applications ca ON ( aao.cid = ca.cid AND aao.application_id = ca.id )
					    JOIN add_ons ao ON ( ao.cid = aao.cid AND ao.id = aao.add_on_id )
						JOIN add_on_groups aog ON ( aog.id = ao.add_on_group_id AND aog.cid = ao.cid )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
					WHERE
						aao.cid = ' . ( int ) $intCid . '
						AND aao.property_id=' . ( int ) $intPropertyId . '
						AND aao.application_id = ' . ( int ) $intApplicationId;

		return self::fetchApplicationAddOns( $strSql, $objClientDatabase );
	}

	public static function fetchApplicationAddOnsByApplicationIdByPropertyIdByCid( $intApplicationId, $intPropertyId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ca.cid,
						ai.event_type_id,
                        ao.hold_until_date,
						ai.offer_submitted_on,
						ca.id AS application_id,
						ca.lease_id,
						ca.property_id,
						apl.id AS applicant_id,
						ao.id AS add_on_id,
						util_get_translated( \'name\', ao.name, ao.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_name,
						aog.id AS add_on_group_id,
						util_get_translated( \'name\', aog.name, aog.details, \'' . \CLocaleContainer::createService()->getLocaleCode() . '\' ) AS add_on_group_name,
						aoc.id AS add_on_category_id,
						util_get_system_translated( \'name\', aoc.name, aoc.details ) AS add_on_category_name,
						aog.add_on_type_id
					FROM
						application_add_ons aao
						JOIN cached_applications ca ON ( aao.cid = ca.cid AND aao.application_id = ca.id )
						JOIN applicant_applications aa ON ( aa.cid = aao.cid AND aa.application_id = aao.application_id AND ca.primary_applicant_id = aa.applicant_id )
						JOIN applicants apl ON ( aao.cid = apl.cid AND apl.id = aa.applicant_id )
						JOIN add_ons ao ON ( ao.cid = aao.cid AND ao.id = aao.add_on_id AND ao.show_on_website = true AND ao.deleted_by IS NULL )
						JOIN add_on_groups aog ON ( ao.cid = aog.cid AND ao.property_id = aog.property_id AND ao.add_on_group_id = aog.id AND aog.show_on_website = true AND aog.deleted_by IS NULL )
						JOIN add_on_categories aoc ON ( aoc.id = aog.add_on_category_id AND aoc.cid = aog.cid AND ( aoc.property_id IS NULL OR aoc.property_id = aog.property_id ) )
						LEFT JOIN application_interests ai ON ( ai.cid = aao.cid AND ai.property_id = aao.property_id AND ai.application_id = aao.application_id AND ai.add_on_id = aao.add_on_id AND ( ai.event_type_id IS NULL OR ai.event_type_id = ' . ( int ) CEventType::GENERATE_WAITLIST_OFFER . ' OR ai.event_type_id = ' . ( int ) CEventType::WAITLIST_APPLICATION_REMOVED_BY_PROPERTY . ' ) AND ai.created_on >= aao.created_on )
						LEFT JOIN application_interest_skips ais ON ( ais.cid = ai.cid AND ais.property_id = ai.property_id AND ais.application_id = ai.application_id AND ais.application_interest_id = ai.id )
					WHERE
				 		aao.cid = ' . ( int ) $intCid . '
				 		AND aao.property_id = ' . ( int ) $intPropertyId . '
						AND aao.application_id = ' . ( int ) $intApplicationId . '
				 	ORDER BY
				 		ao.name,
				 		ca.application_completed_on';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>
