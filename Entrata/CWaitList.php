<?php

class CWaitList extends CBaseWaitList {

	const STRATEGY_RANKING     = 1;
	const STRATEGY_RATING      = 2;
	const STRATEGY_COMBINATION = 3;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getStrategyNames() {
		$arrstrStrategyNames = [
			self::STRATEGY_RANKING		=> __( 'Ranking' ),
			self::STRATEGY_RATING		=> __( 'Rating' ),
			self::STRATEGY_COMBINATION	=> __( 'Combination' )
		];

		return $arrstrStrategyNames;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['original_application_stage_status_id'] ) )
			$this->setOriginalApplicationStageStatusId( $arrmixValues['original_application_stage_status_id'] );
	}

	public function setOriginalApplicationStageStatusId( $intOriginalApplicationStageStatusId ) {
		$this->m_intOriginalApplicationStageStatusId = CStrings::strToIntDef( $intOriginalApplicationStageStatusId, NULL, false );
	}

	public function getOriginalApplicationStageStatusId() {
		return $this->m_intOriginalApplicationStageStatusId;
	}

	public function sqlOriginalApplicationStageStatusId() {
		return ( true == isset( $this->m_intOriginalApplicationStageStatusId ) ) ? ( string ) $this->m_intOriginalApplicationStageStatusId : 'NULL';
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client id is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationStageStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStrategy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpirationDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseDays() {
		$boolIsValid = true;

		if( false == is_null( $this->getResponseDays() ) && '' != $this->getResponseDays() && false == valId( $this->getResponseDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Invalid Response days value.' ) ) );
		}

		return $boolIsValid;
	}

	public function valOfferLimit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectionLimit() {
		$boolIsValid = true;

		if( false == is_null( $this->getRejectionLimit() ) && '' != $this->getRejectionLimit() && false == valId( $this->getRejectionLimit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Invalid Rejection limit value.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasTransferApplications() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valResponseDays();
				$boolIsValid &= $this->valRejectionLimit();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Other functions
	 */

	public function createWaitListApplication() {
		$objWaitListApplication = new CWaitListApplication();
		$objWaitListApplication->setCid( $this->getCid() );
		$objWaitListApplication->setWaitListId( $this->getId() );

		return $objWaitListApplication;
	}

	public function createWaitListPoint() {
		$objWaitListPoint = new CWaitListPoint();
		$objWaitListPoint->setCid( $this->getCid() );
		$objWaitListPoint->setWaitListId( $this->getId() );

		return $objWaitListPoint;
	}

}
?>