<?php

class CRoommateInterest extends CBaseRoommateInterest {

	const SYSTEM_CODE_GENDER_PREFERENCE = 'GN';

	protected $m_arrmixInterestOptions;
	protected $m_strWrittenResponse;

	public function __construct() {
		parent::__construct();
		$this->m_arrmixInterestOptions = [];
		return;
	}

    /**
     * Val Functions
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

	public function valInputTypeId() {
        $boolIsValid = true;
        if( true == is_null( $this->getInputTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'input_type_id', __( 'Answer format is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valSystemCode() {
        $boolIsValid = true;
        return $boolIsValid;
    }

 	public function valQuestion() {
        $boolIsValid = true;
        if( false == valStr( $this->getQuestion() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', __( 'Preference question title is required.' ) ) );
        }
        return $boolIsValid;
    }

    public function valMatchingWeight() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRoommateInterestCategoryId() {
    	$boolIsValid = true;
    	if( false == valStr( $this->getRoommateInterestCategoryId() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'category', __( 'Question type is required.' ) ) );
    	}
    	return $boolIsValid;
    }

    public function valIsRequired() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsSystem() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDate() {
    	$boolIsValid = true;
    	switch( $this->getInputTypeId() ) {
    		case CInputType::LONG_DATE:
    		case CInputType::SHORT_DATE:
    			if( true == valStr( $this->getWrittenResponse() ) ) {
				    $mixValidatedLongDate = CValidation::checkDateFormat( __( '{%t, 0, DATE_NUMERIC_MMDDYYYY}', [ $this->getWrittenResponse() ] ), $boolFormat = true );
    				if( false == $mixValidatedLongDate ) {
    					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getId(), __( 'Date must be in mm/dd/yyyy format.' ) ) );
    					$boolIsValid &= false;
    				}
				    if( false == CValidation::validateISODate( $this->getWrittenResponse() ) ) {
					    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Date is invalid.' ) ) );
					    $boolIsValid &= false;
				    }
    			}
    			break;

    		default:
    			// default;
    			break;
    	}
    	return $boolIsValid;
    }

	public function valRoommateInterestOptions() {

		$boolIsValid = true;

		switch( $this->getInputTypeId() ) {

			case CInputType::SINGLE_SELECT:
			case CInputType::MULTI_SELECT:
				$boolIsSelected = false;
				foreach( $this->getInterestOptions() as $objInterestOption ) {
					if( true == $objInterestOption->getIsSelected() ) {
						$boolIsSelected &= true;
						break 2;
					}
				}

				if( true == $this->getIsRequired() && false == $boolIsSelected ) {
					$boolIsValid &= $boolIsSelected;
					if( false == $boolIsValid ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getId(), __( 'Answer to the (*) question is required.' ), '', [ 'label' => __( 'Interests-General' ), 'step_id' => CApplicationStep::ROOMMATES ] ) );
					}
				}
				break;

			case CInputType::TEXT:
				if( false == valStr( $this->getWrittenResponse() ) && true == $this->getIsRequired() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getId(), __( 'Answer to the (*) question is required.' ), '', [ 'label' => __( 'Interests-General' ), 'step_id' => CApplicationStep::ROOMMATES ] ) );
					$boolIsValid &= false;
				}
				break;

			case CInputType::LONG_DATE:
			case CInputType::SHORT_DATE:
				if( false == valStr( $this->getWrittenResponse() ) && true == $this->getIsRequired() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getId(), __( 'Answer to the (*) question is required.' ), '', [ 'label' => __( 'Interests-General' ), 'step_id' => CApplicationStep::ROOMMATES ] ) );
					$boolIsValid &= false;
					break;
				}
				if( true == valStr( $this->getWrittenResponse() ) ) {
					$mixValidatedLongDate = CValidation::checkDateFormat( __( '{%t, 0, DATE_NUMERIC_MMDDYYYY}', [ $this->getWrittenResponse() ] ), $boolFormat = true );
					if( false == $mixValidatedLongDate ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getId(), __( 'Date must be in mm/dd/yyyy format.' ) ) );
						$boolIsValid &= false;
					}
					if( false == CValidation::validateISODate( $this->getWrittenResponse() ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Date is invalid.' ) ) );
						$boolIsValid &= false;
					}
				}
				break;

			default:
				// default;
				break;
		}

		return $boolIsValid;
	}

    public function validate( $strAction ) {

    	$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRoommateInterestOptions();
				break;

			case 'validate_insert_update_for_roommate_interest':
			case 'validate_insert_update_for_property_preferences':
				$boolIsValid &= $this->valRoommateInterestCategoryId();
				$boolIsValid &= $this->valQuestion();
				$boolIsValid &= $this->valInputTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_update_for_date':
				$boolIsValid &= $this->valDate();
				break;

			default:
				// Default;
				break;
		}

		return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public function delete( $intRoommateInterestId, $objDatabase, $boolIsSoftDelete = false ) {

    	if( true == $boolIsSoftDelete ) {
    		$this->setDeletedBy( $intRoommateInterestId );
    		$this->setDeletedOn( 'NOW()' );
    	} else {
    		return parent::delete( $intRoommateInterestId, $objDatabase );
    	}

    	return $this->update( $intRoommateInterestId, $objDatabase );
    }

    public function createRoommateInterestOption() {

    	$objRoommateInterestOption = new CRoommateInterestOption();
    	$objRoommateInterestOption->setDefaults();
    	$objRoommateInterestOption->setCid( $this->getCid() );

    	return $objRoommateInterestOption;
    }

    public function createPropertyRoommateInterest() {

    	$objPropertyRoommateInterest = new CPropertyRoommateInterest();
    	$objPropertyRoommateInterest->setDefaults();
    	$objPropertyRoommateInterest->setCid( $this->getCid() );
    	$objPropertyRoommateInterest->setRoommateInterestId( $this->getId() );
    	$objPropertyRoommateInterest->setIsPublished( $this->getIsPublished() );
    	$objPropertyRoommateInterest->setMatchingWeight( $this->getMatchingWeight() );

    	return $objPropertyRoommateInterest;
    }

    /**
     * Get Functions
     */

    public function getInterestOptions() {
    	return $this->m_arrmixInterestOptions;
    }

    public function getWrittenResponse() {
    	return $this->m_strWrittenResponse;
    }

    /**
     * Set Functions
     */

    public function setInterestOptions( $arrmixInterestOptions ) {
    	$this->m_arrmixInterestOptions = ( array ) $arrmixInterestOptions;
    }

    public function setWrittenResponse( $strWrittenResponse ) {
    	$this->m_strWrittenResponse = $strWrittenResponse;
    }

    public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

    	if( isset( $arrstrValues['written_response'] ) && $boolDirectSet ) {
    		$this->m_strWrittenResponse = trim( stripcslashes( $arrstrValues['written_response'] ) );
    	} elseif( isset( $arrstrValues['written_response'] ) ) {
    		$this->setWrittenResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['written_response'] ) : $arrstrValues['written_response'] );
    	}
    }

    /**
     * Fetch Functions
     */

    public function fetchRoommateInterestOptions( $objDatabase ) {
    	return CRoommateInterestOptions::fetchRoommateInterestOptionsByRoommateInterestIdByCid( $this->getId(),  $this->getCid(), $objDatabase );
    }

}
?>
