<?php

class CReportAdhocType extends CBaseReportAdhocType {

	const EXPRESS	= 1;
	const STANDARD	= 2;
	const CROSSTAB	= 3;

	public static $c_arrintAllReportAdhocTypes = [ self::EXPRESS, self::STANDARD, self::CROSSTAB ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAction() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExtension() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// no validation avaliable
				break;
		}

		return $boolIsValid;
	}
}
?>