<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateChangeTypes
 * Do not add any new functions to this class.
 */

class CCorporateChangeTypes extends CBaseCorporateChangeTypes {

	public static function fetchCorporateChangeTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CCorporateChangeType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCorporateChangeType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CCorporateChangeType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedCorporateChangeTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						corporate_change_types
					WHERE is_published = True; ';
		return parent::fetchCorporateChangeTypes( $strSql, $objDatabase );
	}

}
?>