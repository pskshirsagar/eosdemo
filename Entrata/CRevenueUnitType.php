<?php

class CRevenueUnitType extends CBaseRevenueUnitType {

	protected $m_intOptimalRent;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intOverrideRentAmount;
	protected $m_intRevenueOverrideRentsId;
	protected $m_intUnitSpaceConfigurationId;
	protected $m_intIsRenewal;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExpiryDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandThirtyDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandSixtyDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDemandNinetyDay() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDailyAvailabilityChangeRate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinExecutedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxExecutedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgExecutedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinCompetitiveRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxCompetitiveRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAvgCompetitiveRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getOptimalRent() {
		return $this->m_intOptimalRent;
	}

	public function setOptimalRent( $intOptimalRent ) {
		$this->m_intOptimalRent = CStrings::strToIntDef( $intOptimalRent, NULL, false );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->m_intLeaseTermId = CStrings::strToIntDef( $intLeaseTermId, NULL, false );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->m_intLeaseStartWindowId = CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false );
	}

	public function getOverrideRentAmount() {
		return $this->m_intOverrideRentAmount;
	}

	public function setOverrideRentAmount( $intOverrideRentAmount ) {
		$this->m_intOverrideRentAmount = CStrings::strToIntDef( $intOverrideRentAmount, NULL, false );
	}

	public function getRevenueOverrideRentsId() {
		return $this->m_intRevenueOverrideRentsId;
	}

	public function setRevenueOverrideRentsId( $intRevenueOverrideRentsId ) {
		$this->m_intRevenueOverrideRentsId = CStrings::strToIntDef( $intRevenueOverrideRentsId, NULL, false );
	}

	public function getUnitSpaceConfigurationId() {
		return $this->m_intUnitSpaceConfigurationId;
	}

	public function setUnitSpaceConfigurationId( $intUnitSpaceConfigurationId ) {
		$this->m_intUnitSpaceConfigurationId = CStrings::strToIntDef( $intUnitSpaceConfigurationId, NULL, false );
	}

	public function getIsRenewal() {
		return $this->m_intIsRenewal;
	}

	public function setIsRenewal( $intIsRenewal ) {
		$this->m_intIsRenewal = CStrings::strToIntDef( $intIsRenewal, NULL, false );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['optimal_rent'] ) ) {
			$this->m_intOptimalRent = trim( stripcslashes( $arrmixValues['optimal_rent'] ) );
		}
		if( isset( $arrmixValues['lease_term_id'] ) ) {
			$this->m_intLeaseTermId = trim( stripcslashes( $arrmixValues['lease_term_id'] ) );
		}
		if( isset( $arrmixValues['lease_start_window_id'] ) ) {
			$this->m_intLeaseStartWindowId = trim( stripcslashes( $arrmixValues['lease_start_window_id'] ) );
		}
		if( isset( $arrmixValues['override_rent_amount'] ) ) {
			$this->m_intOverrideRentAmount = trim( stripcslashes( $arrmixValues['override_rent_amount'] ) );
		}
		if( isset( $arrmixValues['revenue_override_rents_id'] ) ) {
			$this->m_intRevenueOverrideRentsId = trim( stripcslashes( $arrmixValues['revenue_override_rents_id'] ) );
		}
		if( isset( $arrmixValues['unit_space_configuration_id'] ) ) {
			$this->m_intUnitSpaceConfigurationId = trim( stripcslashes( $arrmixValues['unit_space_configuration_id'] ) );
		}
		if( isset( $arrmixValues['is_renewal'] ) ) {
			$this->m_intIsRenewal = trim( stripcslashes( $arrmixValues['is_renewal'] ) );
		}
	}

}
?>