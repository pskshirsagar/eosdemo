<?php

class CApplicantIncomes extends CBaseApplicantIncomes {

	public static function fetchApplicantIncomeByApplicantIdsByIncomeTypeIdByCid( $arrintApplicantIds, $intIncomeTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintApplicantIds ) ) return NULL;

		$strJoinClause 	= ( CIncomeType::RENT_VOUCHER == $intIncomeTypeId ) ? ' JOIN applicant_applications as aa ON ( ai.applicant_id = aa.applicant_id AND ai.cid = aa.cid AND aa.customer_type_id = ' . CCustomerType::PRIMARY . ' AND aa.deleted_on IS NULL ) ' : '';

		$strSql = 'SELECT
						ai.*
					FROM
						applicant_incomes as ai ' .
						$strJoinClause . '
					WHERE
						ai.applicant_id IN (' . implode( ',', $arrintApplicantIds ) . ')
						AND ai.cid = ' . ( int ) $intCid . '
						AND ai.income_type_id = ' . ( int ) $intIncomeTypeId . '
					 ORDER BY
						amount DESC';

		return self::fetchApplicantIncomes( $strSql, $objDatabase );
	}

}
?>