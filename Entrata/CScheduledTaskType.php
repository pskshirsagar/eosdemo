<?php

class CScheduledTaskType extends CBaseScheduledTaskType {

	const INSPECTION 					= 1;
	const MAINTENANCE_REQUEST 			= 2;
	const RESIDENT_EVENT 				= 3;
	const APPLICANT 					= 4;
	const REPORT 						= 5;
	const LEAD 							= 7;
	const RENEWALS_LEASE_MODIFICATION 	= 8;
	const MAINTENANCE_CONTACT_POINTS 	= 9;
	const COMMERCIAL 					= 10;
	const UNIT_EXCLUSION_REASON			= 11;
	const PRICING_NOTIFICATIONS			= 12;
	const PAYMENT 					    = 13;

	public static $c_arrintScheduledTaskTypes = [
		1 => 'Inspection',
		2 => 'Maintenance Request',
		3 => 'Residents',
		4 => 'Applicant',
		5 => 'Report',
		6 => 'Reputation',
		7 => 'Lead',
	 	8 => 'Renewals Lease Modification',
		9 => 'Maintenance Contact Points'
	];

	/**
	* Get Functions
	*
	*/

	public static function getIdToCamelizeName( $intScheduledTaskTypeId ) {
		switch( $intScheduledTaskTypeId ) {

			case self::LEAD:
			case self::APPLICANT:
				return 'Application';
				break;

			case self::MAINTENANCE_CONTACT_POINTS:
			case self::RESIDENT_EVENT:
			case self::PRICING_NOTIFICATIONS:
				return 'LeaseCustomer';
				break;

			case CScheduledTaskType::REPORT:
				return 'Report';
				break;

			case CScheduledTaskType::RENEWALS_LEASE_MODIFICATION:
				return 'LeaseCustomer';
				break;

			case CScheduledTaskType::PAYMENT:
				return 'Payment';
				break;

			case CScheduledTaskType::COMMERCIAL:
				return 'Commercial';

			case CScheduledTaskType::UNIT_EXCLUSION_REASON:
				return 'UnitExclusionReason';

			default:
				// do nothing
				break;
		}
	}

/**
* Validate Functions
*
*/

	public function validate( $strAction ) {
		$boolIsValid = true;

	    switch( $strAction ) {
	 		case VALIDATE_INSERT:
	 		case VALIDATE_UPDATE:
	 		case VALIDATE_DELETE:
	 			break;

	 		default:
	 			$boolIsValid = true;
	 			break;
	 	}
			return $boolIsValid;
	}

}
?>