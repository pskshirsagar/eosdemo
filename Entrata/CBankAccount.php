<?php

class CBankAccount extends CBaseBankAccount {

	const CASH_ACCOUNT_NUMBER = 20;
	const CREDIT_CARD_ACCOUNT_NUMBER = 16;

	protected $m_intUndepositedArTransactionCount;

	protected $m_fltCurrentGlBalance;

	protected $m_strCompanyDiscretionaryData;

	protected $m_arrintPropertyIds;

	protected $m_arrmixFiles;

	protected $m_arrstrProperties;
	protected $m_arrstrUnAssociatedPropertyNames;

	protected $m_arrobjApPayments;
	protected $m_arrobjGlAccountProperties;
	protected $m_arrobjPropertyBankAccounts;
	protected $m_arrobjBankAccountGlAccounts;
	protected $m_arrobjDaddyPropertyBankAccounts;
	protected $m_arrobjExistingPropertyBankAccounts;
	protected $m_arrobjInterCompanyGlAccounts;
	protected $m_arrobjBankAccountMerchantAccounts;

	public static $c_arrstrBankCodes = [
		1 => 'NYB',
		2 => 'CCT',
		7 => 'CMD',
		8 => 'CDC',
		11 => 'CNV',
		13 => 'CCA',
		14 => 'CIL',
		16 => 'FSB',
		30 => 'CNJ',
		40 => 'FNA',
		41 => 'CTX'
	];

	const CHECK_STOCK_LAYOUT_0 = 0;
	const CHECK_STOCK_LAYOUT_1 = 1;
	const CHECK_STOCK_LAYOUT_2 = 2;
	const CHECK_STOCK_LAYOUT_3 = 3;
	const CHECK_STOCK_LAYOUT_4 = 4;
	const CHECK_STOCK_LAYOUT_5 = 5;
	const CHECK_STOCK_LAYOUT_6 = 6;

	const CHECK_STOCK_LAYOUT_DETAIL_0 = 'Check | Stub | Stub';
	const CHECK_STOCK_LAYOUT_DETAIL_1 = 'Stub | Stub | Check';
	const CHECK_STOCK_LAYOUT_DETAIL_2 = 'Stub | Stub | Check (Nelco)';
	const CHECK_STOCK_LAYOUT_DETAIL_3 = 'Copy | Stub | Check | Stub';
	const CHECK_STOCK_LAYOUT_DETAIL_4 = 'Stub | Copy | Stub | Check';
	const CHECK_STOCK_LAYOUT_DETAIL_5 = 'Stub | Check | Stub | ';
	const CHECK_STOCK_LAYOUT_DETAIL_6 = 'Stub | Check | Address';

	public static $c_arrstrCheckStockLayouts = [ self::CHECK_STOCK_LAYOUT_0 => self::CHECK_STOCK_LAYOUT_DETAIL_0, self::CHECK_STOCK_LAYOUT_1 => self::CHECK_STOCK_LAYOUT_DETAIL_1, self::CHECK_STOCK_LAYOUT_2 => self::CHECK_STOCK_LAYOUT_DETAIL_2, self::CHECK_STOCK_LAYOUT_3 => self::CHECK_STOCK_LAYOUT_DETAIL_3, self::CHECK_STOCK_LAYOUT_4 => self::CHECK_STOCK_LAYOUT_DETAIL_4, self::CHECK_STOCK_LAYOUT_5 => self::CHECK_STOCK_LAYOUT_DETAIL_5, self::CHECK_STOCK_LAYOUT_6 => self::CHECK_STOCK_LAYOUT_DETAIL_6 ];

	public function __construct() {
		parent::__construct();
		$this->m_arrobjApPayments = [];
		return;
	}

	/**
	 * Create Functions
	 */

	/**
	 *
	 * @param CDatabase $objDatabase
	 * @return CArDeposit
	 */

	public function createArDeposit() {

		$objArDeposit = new CArDeposit();

		$objArDeposit->setCid( $this->getCid() );
		$objArDeposit->setArDepositTypeId( CArDepositType::MANUAL );
		$objArDeposit->setBankAccountId( $this->getId() );
		$objArDeposit->setPostDate( date( 'm/d/Y' ) );
		$objArDeposit->setTransactionDatetime( 'NOW()' );

		return $objArDeposit;
	}

	public function createPropertyBankAccount() {

		$objPropertyBankAccount = new CPropertyBankAccount();

		$objPropertyBankAccount->setBankAccountId( $this->getId() );
		$objPropertyBankAccount->setCid( $this->getCid() );

		return $objPropertyBankAccount;
	}

	public function createApPayment() {

		$objApPayment = new CApPayment();

		$objApPayment->setBankAccountId( $this->getId() );
		$objApPayment->setCid( $this->getCid() );
		$objApPayment->setApPaymentTypeId( CApPaymentType::CHECK );
		$objApPayment->setPaymentStatusTypeId( CPaymentStatusType::PENDING );
		$objApPayment->setPaymentMediumId( CPaymentMedium::WEB );
		$objApPayment->setPaymentDate( date( 'm/d/Y' ) );
		$objApPayment->setPaymentNumber( $this->getNextCheckNumber() );
		$objApPayment->setCheckBankName( $this->getAccountName() );
		$objApPayment->setCheckBankName( $this->getCheckBankName() );
		$objApPayment->setCheckNameOnAccount( $this->getCheckNameOnAccount() );
		$objApPayment->setCheckAccountTypeId( $this->getCheckAccountTypeId() );
		$objApPayment->setCheckRoutingNumber( $this->getCheckRoutingNumber() );
		$objApPayment->setCheckAccountNumber( $this->getCheckAccountNumber() );

		return $objApPayment;
	}

	public function createFileAssociation() {

		$objFileAssociation = new CFileAssociation();
		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setBankAccountId( $this->getId() );

		return $objFileAssociation;
	}

	public function createGlReconciliation() {

		$objGlReconciliation = new CGlReconciliation();

		$objGlReconciliation->setCid( $this->getCid() );
		$objGlReconciliation->setBankAccountId( $this->getId() );

		return $objGlReconciliation;
	}

	public function createBankAccountMerchantAccount() {

		$objBankAccountMerchantAccount = new CBankAccountMerchantAccount();

		$objBankAccountMerchantAccount->setDefaults();
		$objBankAccountMerchantAccount->setCid( $this->getCid() );
		$objBankAccountMerchantAccount->setBankAccountId( $this->getId() );

		return $objBankAccountMerchantAccount;
	}

	/**
	 * Get Functions
	 */

	public function getCheckAccountNumber() {
		if( false == valStr( $this->m_strCheckAccountNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] );
	}

	public function getCcCardNumberMasked() {
		if( false == valStr( $this->getCcCardNumberEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getCcCardNumberEncrypted(), CONFIG_SODIUM_KEY_CC_CARD_NUMBER );
	}

	public function getCheckAccountNumberWithoutSymbols() {
		$strCheckAccountNumber = ( true == valStr( $this->m_strCheckAccountNumberEncrypted ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] ) : NULL );
		return preg_replace( '/[^0-9\-]/', '', $strCheckAccountNumber );
	}

	public function getCheckAccountNumberEncryptedWithoutSymbols() {
		$strCheckAccountNumber = $this->getCheckAccountNumberWithoutSymbols();
		return ( true == valStr( $strCheckAccountNumber ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) : NULL );
	}

	public function getPayerPhoneNumber() {
		if( false == valStr( $this->m_strPayerPhoneNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPayerPhoneNumberEncrypted, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getBankContactNameFirst() {
		if( false == valStr( $this->m_strBankContactNameFirstEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strBankContactNameFirstEncrypted, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getBankContactNameLast() {
		if( false == valStr( $this->m_strBankContactNameLastEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strBankContactNameLastEncrypted, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getBankPhoneNumber() {
		if( false == valStr( $this->m_strBankPhoneNumberEncrypted ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strBankPhoneNumberEncrypted, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION, [ 'legacy_secret_key' => CONFIG_KEY_SENSITIVE_DATA_ENCRYPTION ] );
	}

	public function getUndepositedArTransactionCount() {
		return $this->m_intUndepositedArTransactionCount;
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber	= ( true == valStr( $this->m_strCheckAccountNumberEncrypted ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] ) : NULL );
		$intStringLength 		= strlen( $strCheckAccountNumber );
		$strLastFour 			= \Psi\CStringService::singleton()->substr( rtrim( \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, 0, -1 ) ), -4 ) . \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, strlen( rtrim( \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, 0, -1 ) ) ), -1 );

		// incase of credit card account we are using same check_account_number filed to store ccaccount number so in case of check account number we dont have MICR symbol 'C' at the end,
		// so we will consider last charcter as well.
		if( 1 == $this->getIsCreditCardAccount() ) {
			$strLastFour = \Psi\CStringService::singleton()->substr( rtrim( \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, 0, $intStringLength ) ), -4 ) . \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, strlen( rtrim( \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, 0, $intStringLength ) ) ), -1 );
		}

		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength - 1, '*', STR_PAD_LEFT );
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function getProperties() {
		return $this->m_arrstrProperties;
	}

	public function getUnAssociatedPropertyNames() {
		return $this->m_arrstrUnAssociatedPropertyNames;
	}

	public function getDaddyPropertyBankAccounts() {
		return $this->m_arrobjDaddyPropertyBankAccounts;
	}

	public function getPropertyBankAccounts() {
		return $this->m_arrobjPropertyBankAccounts;
	}

	public function getBankAccountMerchantAccounts() {
		return $this->m_arrobjBankAccountMerchantAccounts;
	}

	public function getInterCompanyGlAccounts() {
		return $this->m_arrobjInterCompanyGlAccounts;
	}

	public function getBankAccountGlAccounts() {
		return $this->m_arrobjBankAccountGlAccounts;
	}

	public function getExistingPropertyBankAccounts() {
		return $this->m_arrobjExistingPropertyBankAccounts;
	}

	public function getApPayments() {
		return $this->m_arrobjApPayments;
	}

	public function getGlAccountProperties() {
		return $this->m_arrobjGlAccountProperties;
	}

	public function getCurrentGlBalance() {
		return $this->m_fltCurrentGlBalance;
	}

	public function getFiles() {
		return $this->m_arrmixFiles;
	}

	public function getCompanyDiscretionaryData() {
		return $this->m_strCompanyDiscretionaryData;
	}

	/**
	 * Set Functions
	 */

	// Overridden to achieve validation from add/edit bank account screen

	public function setNextCheckNumber( $intNextCheckNumber ) {
		$this->m_intNextCheckNumber = CStrings::strTrimDef( $intNextCheckNumber, -1, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['undeposited_ar_transaction_count'] ) && $boolDirectSet ) {
			$this->m_intUndepositedArTransactionCount = trim( $arrmixValues['undeposited_ar_transaction_count'] );
		} elseif( isset( $arrmixValues['undeposited_ar_transaction_count'] ) ) {
			$this->setUndepostedArTransactionCount( $arrmixValues['undeposited_ar_transaction_count'] );
		}

		if( isset( $arrmixValues['current_gl_balance'] ) && $boolDirectSet ) {
			$this->m_fltCurrentGlBalance = trim( $arrmixValues['current_gl_balance'] );
		} elseif( isset( $arrmixValues['current_gl_balance'] ) ) {
			$this->setCurrentGlBalance( $arrmixValues['current_gl_balance'] );
		}

		return;
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		if ( true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumber, 'XXXX' ) ) return;
		// $strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 25, NULL, true );
		if( true == valStr( $strCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
			$this->setCheckAccountNumberMasked();
		}
	}

	public function setCcCardNumberMasked( $strCcCardNumberMasked ) {
		if( true == valStr( $strCcCardNumberMasked ) ) {
			$this->setCcCardNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strCcCardNumberMasked ), CONFIG_SODIUM_KEY_CC_CARD_NUMBER ) );
		} else {
			$this->setCcCardNumberEncrypted( NULL );
		}
	}

	public function setPayerPhoneNumber( $strPayeePhoneNumber ) {

		$strPayeePhoneNumber = CStrings::strTrimDef( $strPayeePhoneNumber, 20, NULL, true );
		if( true == valStr( $strPayeePhoneNumber ) ) {
			$this->setPayerPhoneNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPayeePhoneNumber, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setBankContactNameFirst( $strBankContactNameFirst ) {

		$strBankContactNameFirst = CStrings::strTrimDef( $strBankContactNameFirst, 50, NULL, true );
		if( true == valStr( $strBankContactNameFirst ) ) {
			$this->setBankContactNameFirstEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strBankContactNameFirst, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setBankContactNameLast( $strBankContactNameLast ) {

		$strBankContactNameLast = CStrings::strTrimDef( $strBankContactNameLast, 50, NULL, true );
		if( true == valStr( $strBankContactNameLast ) ) {
			$this->setBankContactNameLastEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strBankContactNameLast, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setBankPhoneNumber( $strBankPhoneNumber ) {

		$strBankPhoneNumber = CStrings::strTrimDef( $strBankPhoneNumber, 50, NULL, true );
		if( true == valStr( $strBankPhoneNumber ) ) {
			$this->setBankPhoneNumberEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strBankPhoneNumber, CONFIG_SODIUM_KEY_SENSITIVE_DATA_ENCRYPTION ) );
		}
	}

	public function setUndepostedArTransactionCount( $intUndepositedArTransactionCount ) {
		$this->m_intUndepositedArTransactionCount = $intUndepositedArTransactionCount;
	}

	public function setCheckAccountNumberMasked( $strCheckAccountNumberMasked = NULL ) {
		$this->m_strCheckAccountNumberMasked = ( true == valStr( $strCheckAccountNumberMasked ) ) ? $strCheckAccountNumberMasked : $this->getCheckAccountNumberMasked();
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->m_arrintPropertyIds = $arrintPropertyIds;
	}

	public function setProperties( $arrstrProperties ) {
		$this->m_arrstrProperties = $arrstrProperties;
	}

	public function setUnAssociatedPropertyNames( $arrstrUnAssociatedPropertyNames ) {
		$this->m_arrstrUnAssociatedPropertyNames = $arrstrUnAssociatedPropertyNames;
	}

	public function setDaddyPropertyBankAccounts( $arrobjDaddyPropertyBankAccounts ) {
		$this->m_arrobjDaddyPropertyBankAccounts = $arrobjDaddyPropertyBankAccounts;
	}

	public function setPropertyBankAccounts( $arrobjPropertyBankAccounts ) {
		$this->m_arrobjPropertyBankAccounts = $arrobjPropertyBankAccounts;
	}

	public function setBankAccountMerchantAccounts( $arrobjBankAccountMerchantAccounts ) {
		return $this->m_arrobjBankAccountMerchantAccounts = $arrobjBankAccountMerchantAccounts;
	}

	public function setInterCompanyGlAccounts( $arrobjInterCompanyGlAccounts ) {
		$this->m_arrobjInterCompanyGlAccounts = $arrobjInterCompanyGlAccounts;
	}

	public function setBankAccountGlAccounts( $arrobjBankAccountGlAccounts ) {
		$this->m_arrobjBankAccountGlAccounts = $arrobjBankAccountGlAccounts;
	}

	public function setExistingPropertyBankAccounts( $arrobjExistingPropertyBankAccounts ) {
		$this->m_arrobjExistingPropertyBankAccounts = $arrobjExistingPropertyBankAccounts;
	}

	public function setApPayments( $arrobjApPayments ) {
		$this->m_arrobjApPayments = $arrobjApPayments;
	}

	public function setGlAccountProperties( $arrobjGlAccountProperties ) {
		$this->m_arrobjGlAccountProperties = $arrobjGlAccountProperties;
	}

	// Overriding setCheckRoutingNumber : In base class we are triming routing number
	// Now we are storing each digit of a routing number in sepearate textbox, suppose if we keep first few textboxes empty then we enter value for routing number and
	// suppose we get any validation message then it will trim that routing number and it is showing number started from 1st textbox to hadle this kind of senario we are
	// overriding the following method.

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->m_strCheckRoutingNumber = $strCheckRoutingNumber;
	}

	public function setCurrentGlBalance( $fltCurrentGlBalance ) {
		$this->m_fltCurrentGlBalance = $fltCurrentGlBalance;
	}

	public function setFiles( $arrmixFiles ) {
		$this->m_arrmixFiles = $arrmixFiles;
	}

	public function setCompanyDiscretionaryData( $strCompanyDiscretionaryData ) {
		$this->m_strCompanyDiscretionaryData = $strCompanyDiscretionaryData;
	}

	/**
	 * Validate Function
	 */

	public function validate( $strAction, $objClientDatabase = NULL, $arrmixParameters = [] ) {
		$boolIsValid = true;

		$objBankAccountValidator = new CBankAccountValidator();
		$objBankAccountValidator->setBankAccount( $this );

		$boolIsValid &= $objBankAccountValidator->validate( $strAction, $objClientDatabase, $arrmixParameters );

		return $boolIsValid;
	}

	public function validateAchSetting( $arrintBankAccountIds, $objClientDatabase ) {

		$boolIsValid = true;

		if( false == valIntArr( $arrintBankAccountIds ) ) {
			return $boolIsValid;
		}

		$arrobjBankAccountIsAchDisabled = ( array ) CBankAccounts::fetchBankAccountsByIsAchDisabledByIdsByCid( $arrintBankAccountIds, $this->getCid(), $objClientDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjBankAccountIsAchDisabled ) ) {

			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_ach_enable', 'Bank account(s) ' . implode( ',', array_keys( rekeyObjects( 'CheckBankName', $arrobjBankAccountIsAchDisabled ) ) ) . ' must be enabled for ACH payments in order to create ACH payments. This can be configured at: Setup > Company > Financial > Bank Accounts > Edit Bank Account.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckBlackListedBankAccount( $objClientDatabase ) {
		$boolIsValid = true;

		$objPaymentBlacklistEntry = new CPaymentBlacklistEntry();
		$objBlacklistedEntry = $objPaymentBlacklistEntry->checkBlacklistedBankAccount( $objClientDatabase, $this->getCheckRoutingNumber(), $this->getCheckAccountNumberWithoutSymbols() );

		if( NULL == $objBlacklistedEntry ) {
			return true;
		}

		switch( $objBlacklistedEntry->getPaymentBlacklistTypeId() ) {
			case CPaymentBlacklistType::NONE:
				return true;

			case CPaymentBlacklistType::ROUTING_NUMBER:
				$this->addErrorMsg( new CErrorMsg( 'ERROR_TYPE_VALIDATION', 'blacklist_bank_account', $objBlacklistedEntry->getBlacklistReason() ) );
				$boolIsValid = false;
				break;

			default:
				$this->addErrorMsg( new CErrorMsg( 'ERROR_TYPE_VALIDATION', 'blacklist_bank_account', $objBlacklistedEntry->getBlacklistReason() ) );
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPropertyBankAccounts( $objClientDatabase, $boolIsFromEditBankAccount = false ) {
		return \Psi\Eos\Entrata\CPropertyBankAccounts::createService()->fetchPropertyBankAccountsByBankAccountIdByCid( $this->getId(), $this->getCid(), $objClientDatabase, false, false, $boolIsFromEditBankAccount );
	}

	public function fetchPropertyIds( $objDatabase ) {

		$strSql = 'SELECT property_id FROM property_bank_accounts WHERE bank_account_id = ' . ( int ) $this->getId() . ' AND cid = ' . ( int ) $this->getCid() . ' GROUP BY property_id';
		$arrstrPropertyIds = fetchData( $strSql, $objDatabase );

		$arrintFormattedPropertyIds = [];

		if( true == valArr( $arrstrPropertyIds ) ) {
			foreach( $arrstrPropertyIds as $arrintPropertyId ) {
				$arrintFormattedPropertyIds[$arrintPropertyId['property_id']] = $arrintPropertyId['property_id'];
			}
		}

		return $arrintFormattedPropertyIds;
	}

	public function fetchMaxPaymentNumber( $intApPaymentTypeId, $objClientDatabase ) {
		return CBankAccounts::fetchMaxPaymentNumberByBankAccountIdByApPaymentTypeIdByCid( $this->getId(), $intApPaymentTypeId, $this->getCid(), $objClientDatabase );
	}

	public function fetchEligibleMerchantAccounts( $objDatabase ) {
		return CMerchantAccounts::fetchEligibleMerchantAccountsByBankAccountIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	public function fetchFileAssociations( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CFileAssociations::createService()->fetchFileAssociationsByBankAccountIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchEndingBalance( $objClientDatabase ) {
		return CGlReconciliations::fetchEndingBalanceByBankAcccountIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchCashGlAccounts( $objClientDatabase ) {
		$intIsCreditCardAccount = ( 1 == $this->getIsCreditCardAccount() ) ? 1 : 0;
		return CPropertyBankAccounts::fetchCashGlAccountsByBankAccountIdByCid( $this->getId(), $this->getCid(), $objClientDatabase, $intIsCreditCardAccount );
	}

	public function fetchCreditCardGlAccounts( $objClientDatabase ) {
		return CPropertyBankAccounts::fetchCashGlAccountsByBankAccountIdByCid( $this->getId(), $this->getCid(), $objClientDatabase, $boolIsCreditCardAccount = true );
	}

	public function fetchFiles( $objClientDatabase ) {
		return CFiles::fetchFilesByBankAccountIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchBankAccountActiveMarketingMediaAssociation( $objDatabase ) {
		return \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationByReferenceIdByMediaTypeIdByMediaSubTypeIdByCid( $this->getId(), CMarketingMediaType::BANK_ACCOUNT_MEDIA, CMarketingMediaSubType::CHECK_LOGO, $this->getCid(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function addApPayment( $objApPayment ) {
		if( false == is_null( $objApPayment->getId() ) ) {
			$this->m_arrobjApPayments[$objApPayment->getId()] = $objApPayment;
		} else {
			$this->m_arrobjApPayments[] = $objApPayment;
		}
	}

	public function insert( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly = false ) {

		if( false == parent::insert( $intCurrentUserId, $objClientDatabase, $boolReturnSqlOnly ) ) {
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$objClientDatabase->rollback();
			return false;
		}

		// Log History
		if( false == $this->logHistory( CBankAccountLog::ACTION_CREATED, $intCurrentUserId, $objClientDatabase ) ) {
			$objClientDatabase->rollback();
			return false;
		}
		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolRemoveSchedulerBankAccount = false ) {

		$strSql = parent::update( $intCurrentUserId, $objDatabase, true );
		if( true == $boolRemoveSchedulerBankAccount ) {
			$strSql .= ' UPDATE 
						accounting_export_schedulers 
					SET 
						bank_account_ids = array_remove(bank_account_ids, ' . $this->getId() . ' )
					WHERE 
						cid =' . $this->getCid() . '
						AND ' . $this->getId() . ' = ANY( bank_account_ids );';
		}

		return ( true == $boolReturnSqlOnly ) ? $strSql : $this->executeSql( $strSql, $this, $objDatabase );
	}

	public function removeUnassociatedProperties( $objDatabase, $boolReturnSqlOnly = false, $arrintAccountingExportSchedulerIds, $intTransmissionVendorId ) {
		$strSql = ' DELETE 
					FROM 
						property_accounting_export_schedulers 
					WHERE
					 cid = ' . $this->getCid() . '
					 AND accounting_export_scheduler_id IN (  ' . implode( ',', array_unique( $arrintAccountingExportSchedulerIds ) ) . ' )
					 AND property_id NOT IN (
											SELECT
												pba.property_id
											FROM
												property_bank_accounts pba
												JOIN accounting_export_schedulers aes ON ( pba.cid = aes.cid AND pba.bank_account_id= ANY ( aes.bank_account_ids ) )
											WHERE
												pba.bank_account_id = ' . $this->getId() . '
												AND pba.cid = ' . $this->getCid() . '
												AND aes.transmission_vendor_id = ' . ( int ) $intTransmissionVendorId . ');';

		return ( true == $boolReturnSqlOnly ) ? $strSql : $this->executeSql( $strSql, $this, $objDatabase );
	}

	/**
	 * LOG Methods
	 */

	public function logHistory( $strAction, $intCompanyUserId, $objClientDatabase ) {

		$boolIsValid 							= true;
		$objBankAccountLog 						= $this->createBankAccountLog();
		$arrobjPropertyBankAccountLogs			= ( array ) $this->createPropertyBankAccountLogs( $objClientDatabase );
		$arrobjBankAccountMerchantAccountLogs	= ( array ) $this->createBankAccountMerchantAccountLogs( $objClientDatabase );

		$objBankAccountLog->setAction( $strAction );
		$objBankAccountLog->setUpdatedOn( 'NOW()' );
		if( false == $objBankAccountLog->insert( $intCompanyUserId, $objClientDatabase ) ) {
			$this->addErrorMsgs( $objBankAccountLog->getErrorMsgs() );
			return $boolIsValid;
		}

		foreach( $arrobjPropertyBankAccountLogs as $objPropertyBankAccountLog ) {

			$objPropertyBankAccountLog->setBankAccountLogId( $objBankAccountLog->getId() );
			if( false == $objPropertyBankAccountLog->insert( $intCompanyUserId, $objClientDatabase ) ) {
				$this->addErrorMsgs( $objPropertyBankAccountLog->getErrorMsgs() );
				$boolIsValid = false;
				break;
			}
		}

		foreach( $arrobjBankAccountMerchantAccountLogs as $objBankAccountMerchantAccountLog ) {

			$objBankAccountMerchantAccountLog->setBankAccountLogId( $objBankAccountLog->getId() );
			if( false == $objBankAccountMerchantAccountLog->insert( $intCompanyUserId, $objClientDatabase ) ) {
				$this->addErrorMsgs( $objPropertyBankAccountLog->getErrorMsgs() );
				$boolIsValid = false;
				break;
			}
		}

		return $boolIsValid;
	}

	public function createBankAccountLog() {

		$objBankAccountLog = new CBankAccountLog();
		$objBankAccountLog->mapBankAccountLog( $this );
		return $objBankAccountLog;
	}

	public function createPropertyBankAccountLogs( $objClientDatabase ) {

		$arrobjPropertyBankAccountLogs		= [];
		$arrobjPropertyBankAccounts			= ( array ) $this->getPropertyBankAccounts();
		$arrobjDaddyPropertyBankAccounts	= ( array ) $this->getDaddyPropertyBankAccounts();

		$arrobjPropertyBankAccounts 		= array_merge( $arrobjPropertyBankAccounts, $arrobjDaddyPropertyBankAccounts );

		if( false == valArr( $arrobjPropertyBankAccounts ) ) {
			$arrobjPropertyBankAccounts = ( array ) CPropertyBankAccounts::fetchActivePropertyBankAccountsByBankAccountIdsByCid( [ $this->getId() ], $this->getCid(), $objClientDatabase );
		}

		foreach( $arrobjPropertyBankAccounts as $objPropertyBankAccount ) {

			$objPropertyBankAccountLog = new CPropertyBankAccountLog();
			$objPropertyBankAccountLog->mapPropertyBankAccountLog( $objPropertyBankAccount );
			$arrobjPropertyBankAccountLogs[] = $objPropertyBankAccountLog;
		}

		return $arrobjPropertyBankAccountLogs;
	}

	public function createBankAccountMerchantAccountLogs( $objClientDatabase ) {

		$arrobjBankAccountMerchantAccountLogs	= [];
		$arrobjBankAccountMerchantAccounts		= ( array ) $this->getBankAccountMerchantAccounts();

		if( false == valArr( $arrobjBankAccountMerchantAccounts ) ) {
			$arrobjBankAccountMerchantAccounts = ( array ) CBankAccountMerchantAccounts::fetchBankAccountMerchantAccountsByBankAccountIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		}

		foreach( $arrobjBankAccountMerchantAccounts as $objBankAccountMerchantAccount ) {

			$objBankAccountMerchantAccountLog = new CBankAccountMerchantAccountLog();
			$objBankAccountMerchantAccountLog->mapBankAccountMerchantAccountLog( $objBankAccountMerchantAccount );
			$arrobjBankAccountMerchantAccountLogs[] = $objBankAccountMerchantAccountLog;
		}

		return $arrobjBankAccountMerchantAccountLogs;
	}

	public function verifyAccount( $objDatabase ) {

		$objGiactLibrary = new CGiactLibrary( CPsProduct::BILL_PAY, $this->getCid(), $objDatabase );

		$arrstrResponses = $objGiactLibrary->verifyAccount( $this->getCheckAccountNumberEncryptedWithoutSymbols(), $this->getCheckRoutingNumber(), $this->getCheckAccountTypeId() );

		if( false === $arrstrResponses ) {
			return false;
		}

		$this->setGiactVerificationResponse( $objGiactLibrary->getCombinedResponse() );

		if( true == $objGiactLibrary->isPass() ) {
			$this->setVerifiedOn( 'NOW()' );
			$this->setIsVerified( true );
			return true;
		}

		$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Account information has been ' . \Psi\CStringService::singleton()->strtolower( $objGiactLibrary->getVerificationResponse() ) ) );
		$this->addErrorMsgs( $objGiactLibrary->getErrorMsgs() );
		$this->setIsVerified( false );

		return false;
	}

}

?>