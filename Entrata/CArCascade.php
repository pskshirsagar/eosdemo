<?php

class CArCascade extends CBaseArCascade {

	const PROPERTY			= 1;
	const FLOOR_PLAN		= 2;
	const UNIT_TYPE			= 3;
	const SPACE				= 4;

	public static $c_arrintAllArCascade = [ self::PROPERTY, self::FLOOR_PLAN, self::UNIT_TYPE, self::SPACE ];

	public static $c_arrstrArCascadeNames = [
		self::PROPERTY   => 'Property',
		self::FLOOR_PLAN => 'Floor Plan',
		self::UNIT_TYPE  => 'Unit Type',
		self::SPACE      => 'Space'
	];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>