<?php

class CSubsidyProject extends CBaseSubsidyProject {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Client ID is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == valId( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Property ID is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valSubsidyTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getSubsidyTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Subsidy type ID is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valSubsidyProgramTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getSubsidyProgramTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Subsidy program type ID is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valSubsidyProgramSubTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFederal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valSubsidyTypeId();
				$boolIsValid &= $this->valSubsidyProgramTypeId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	// Getter setter methods for details field

	public function setPlacedInServiceDate( $strPlacedInServiceDate ) {
		$this->setDetailsField( [ 'tax_credit_details', 'placed_in_service_date' ], $strPlacedInServiceDate );
	}

	public function getPlacedInServiceDate() {
		return $this->getDetailsField( [ 'tax_credit_details', 'placed_in_service_date' ] );
	}

	public function setSubsidyActionPlanTypeId( $intSubsidyActionPlanTypeId ) {
		$this->setDetailsField( [ 'hud_details', 'subsidy_action_plan_type_id' ], $intSubsidyActionPlanTypeId );
	}

	public function getSubsidyActionPlanTypeId() {
		return $this->getDetailsField( [ 'hud_details', 'subsidy_action_plan_type_id' ] );
	}

	public function setSubsidyActionPlanType( $strSubsidyActionPlanType ) {
		$this->setDetailsField( [ 'hud_details', 'subsidy_action_plan_type' ], $strSubsidyActionPlanType );
	}

	public function getSubsidyActionPlanType() {
		return $this->getDetailsField( [ 'hud_details', 'subsidy_action_plan_type' ] );
	}

	public function setHapTitle( $strHapTitle ) {
		$this->setDetailsField( [ 'hud_details', 'hap_details', 'hap_title' ], $strHapTitle );
	}

	public function getHapTitle() {
		return $this->getDetailsField( [ 'hud_details', 'hap_details', 'hap_title' ] );
	}

	public function setHapPersonName( $strHapPersonName ) {
		$this->setDetailsField( [ 'hud_details', 'hap_details', 'hap_person_name' ], $strHapPersonName );
	}

	public function getHapPersonName() {
		return $this->getDetailsField( [ 'hud_details', 'hap_details', 'hap_person_name' ] );
	}

	public function setFirstHapVoucherMonth( $strFirstHapVoucherMonth ) {
		$this->setDetailsField( [ 'hud_details', 'hap_details', 'first_hap_voucher_month' ], $strFirstHapVoucherMonth );
	}

	public function getFirstHapVoucherMonth() {
		return $this->getDetailsField( [ 'hud_details', 'hap_details', 'first_hap_voucher_month' ] );
	}

	public function setHapPersonPhoneNumber( $strHapPersonPhoneNumber ) {
		$this->setDetailsField( [ 'hud_details', 'hap_details', 'hap_person_phone_number' ], $strHapPersonPhoneNumber );
	}

	public function getHapPersonPhoneNumber() {
		return $this->getDetailsField( [ 'hud_details', 'hap_details', 'hap_person_phone_number' ] );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['placed_in_service_date'] ) ) $this->setPlacedInServiceDate( $arrmixValues['placed_in_service_date'] );
		if( isset( $arrmixValues['subsidy_action_plan_type_id'] ) ) $this->setSubsidyActionPlanTypeId( $arrmixValues['subsidy_action_plan_type_id'] );
		if( isset( $arrmixValues['subsidy_action_plan_type'] ) ) $this->setSubsidyActionPlanType( $arrmixValues['subsidy_action_plan_type'] );
		if( isset( $arrmixValues['hap_title'] ) ) $this->setHapTitle( $arrmixValues['hap_title'] );
		if( isset( $arrmixValues['hap_person_name'] ) ) $this->setHapPersonName( $arrmixValues['hap_person_name'] );
		if( isset( $arrmixValues['first_hap_voucher_month'] ) ) $this->setFirstHapVoucherMonth( $arrmixValues['first_hap_voucher_month'] );
		if( isset( $arrmixValues['hap_person_phone_number'] ) ) $this->setHapPersonPhoneNumber( $arrmixValues['hap_person_phone_number'] );
	}

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
        $this->setDeletedBy( $intCurrentUserId );
        $this->setDeletedOn( 'NOW()' );
        return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    }

}
?>