<?php
class CChecklistItemType extends CBaseChecklistItemType {

	const MARK_COMPLETE                 = 1;
	const ACCEPT_PAYMENT                = 2;
	const TEXT_RESPONSE                 = 3;
	const GENERATE_MERGE_FIELD_DOCUMENT = 4;
	const PRINT_SEND_STATIC_DOCUMENT    = 5;
	const UPLOAD_DOCUMENT               = 6;
	const UPLOAD_RESIDENT_PHOTO         = 7;
	const SETUP_RESIDENT_SERVICES       = 8;
	const ASSIGNABLE_ITEMS              = 9;
	const RENTABLE_ITEMS                = 10;
	const INFORMATION_COLLECTED         = 11;
	const UNSIGNED_DOCS                 = 12;
	const TEXT_OPT_IN                   = 13;
	const WATCH_VIDEO                   = 14;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>