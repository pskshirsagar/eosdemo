<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCorporateChanges
 * Do not add any new functions to this class.
 */

class CCorporateChanges extends CBaseCorporateChanges {

	public static function fetchCorporateChangesByCorporateEventIdByCid( $intCorporateEventId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						cc.*
					FROM
						corporate_changes cc
					WHERE
						cc.cid = ' . ( int ) $intCid . '
						AND cc.corporate_event_id = ' . ( int ) $intCorporateEventId . '
					ORDER BY created_by DESC;';
		return self::fetchCorporateChanges( $strSql, $objDatabase );

	}

}
?>