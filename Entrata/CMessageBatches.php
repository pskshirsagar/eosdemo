<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CMessageBatches
 * Do not add any new functions to this class.
 */

class CMessageBatches extends CBaseMessageBatches {

	public static function fetchMessageBatchesOfCurrentMonthByMessageTypeIdByCid( $intMessageTypeId, $intCid, $objDatabase, $arrintPropertyIds ) {

		$strPropertyIdCondition = ( true == valArr( $arrintPropertyIds ) ) ? ' AND mbd.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ' : '';

		$strSql = 'SELECT
					    *
					FROM
					    (
					      SELECT
					          DISTINCT ( mb.created_on ),
					          mb.id,
					          count ( mbd.id ) AS message_batch_detail_count,
					          SUM ( CASE
					                  WHEN mbd.message_status = \'' . CMessageStatusEnum::PENDING . '\' THEN 1
					                  ELSE 0
					                END ) AS pending_count
					      FROM
					          message_batches mb
					          JOIN message_batch_details mbd ON ( mbd.cid = mb.cid AND mbd.message_batch_id = mb.id )
					      WHERE
					          mb.cid = ' . ( int ) $intCid . ' 
							  AND mb.message_batch_type_id IN ( ' . ( int ) $intMessageTypeId . ' )
					          AND date_trunc(\'month\', mb.created_on) = date_trunc(\'month\', now())
					          ' . $strPropertyIdCondition . '
					      GROUP BY
					          mb.created_on,
					          mb.id
					      ORDER BY
					          mb.created_on DESC
					    ) AS subq
					WHERE
					    subq.pending_count = 0';

		return fetchData( $strSql, $objDatabase );
	}

}
?>