<?php

class CCustomAdvertisement extends CBaseCustomAdvertisement {

	public function valCompanyMediaFileId() {
		return true;
	}

	public function valTitle() {
		$boolIsValid = true;
    	if( true == is_null( $this->m_strTitle ) && CCustomAdType::TEXT == $this->getCustomAdTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Title is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valBodyText() {
		$boolIsValid = true;
    	if( true == is_null( $this->m_strBodyText ) && CCustomAdType::TEXT == $this->getCustomAdTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Body Text is required.' ) ) );
			$boolIsValid = false;
		}
		if( false == is_null( $this->m_strBodyText ) && 500 < strlen( $this->m_strBodyText ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'name', __( 'Body Text should not be greater than {%d,0} characters.', [ 500 ] ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valBodyText();
				$boolIsValid &= $this->valCompanyMediaFileId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function fetchCompanyMediaFileByMediaType( $intMediaTypeId, $objDatabase ) {
		return \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFileByCustomAdIdByMediaTypeIdByCid( $this->getId(), $intMediaTypeId, $this->getCid(), $objDatabase );
	}

	public static function fetchCustomCustomAdvertisementByIdByCId( $intPropertyId, $intCid, $objDatabase, $boolIsPublished = false ) {

		$strWhereCondition = ( true == $boolIsPublished ) ? ' AND ca.is_published = 1' : NULL;

		$strSql = 'SELECT
						ca.*
					FROM
						custom_advertisements as ca
					WHERE
						ca.property_id = ' . ( int ) $intPropertyId . '
						AND ca.cid = ' . ( int ) $intCid . '
						' . $strWhereCondition . '
					ORDER BY ca.id DESC LIMIT 1';

		return CCustomAdvertisements::fetchCustomAdvertisement( $strSql, $objDatabase );
	}

}
?>