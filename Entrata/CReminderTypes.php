<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReminderTypes
 * Do not add any new functions to this class.
 */

class CReminderTypes extends CBaseReminderTypes {

    public static function fetchReminderTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CReminderType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchReminderType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CReminderType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>