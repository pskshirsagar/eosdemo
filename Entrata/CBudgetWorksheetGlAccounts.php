<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CBudgetWorksheetGlAccounts
 * Do not add any new functions to this class.
 */

class CBudgetWorksheetGlAccounts extends CBaseBudgetWorksheetGlAccounts {

	public static function fetchBudgetWorksheetGlAccountsByBudgetWorksheetIdsByCid( $arrintBudgetWorksheetIds, $intCid, $objDatabase ) {
		if( false == valIntArr( $arrintBudgetWorksheetIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bwgl.*
					FROM
						budget_worksheet_gl_accounts bwgl
					WHERE
						bwgl.cid = ' . ( int ) $intCid . '
						AND bwgl.budget_worksheet_id IN ( ' . sqlIntImplode( $arrintBudgetWorksheetIds ) . ' )';

		return self::fetchBudgetWorksheetGlAccounts( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountDetailsByBudgetWorksheetIdByCid( $intBudgetWorksheetId, $intCid, $objDatabase, $intGlAccountId = NULL ) {

		if( false == valId( $intBudgetWorksheetId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strCondition = ( true == valId( $intGlAccountId ) ) ? ' AND bwgl.gl_account_id = ' . ( int ) $intGlAccountId : '';

		$strSql = 'SELECT
						bwgl.*,
						gat.formatted_account_number AS gl_account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS gl_account_name
					FROM
						budget_worksheet_gl_accounts bwgl
						JOIN gl_account_trees gat ON ( gat.cid = bwgl.cid AND gat.gl_account_id = bwgl.gl_account_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
					WHERE
						bwgl.cid = ' . ( int ) $intCid . '
						AND bwgl.budget_worksheet_id = ' . ( int ) $intBudgetWorksheetId . $strCondition . '
					ORDER BY bwgl.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountsByIdsByCid( $arrintBudgetWorksheetGlAccountIds, $intCid, $objDatabase ) {
		if( false == valIntArr( $arrintBudgetWorksheetGlAccountIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bwgl.*,
						concat( gat.formatted_account_number , \' : \' , util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) ) AS gl_account_name
					FROM
						budget_worksheet_gl_accounts bwgl
						JOIN gl_account_trees gat ON ( gat.cid = bwgl.cid AND gat.gl_account_id = bwgl.gl_account_id )
						JOIN gl_trees gt ON ( gt.cid = gat.cid AND gt.id = gat.gl_tree_id AND gt.is_system = 1 AND gt.system_code = \'DEFAULT\' )
					WHERE
						bwgl.cid = ' . ( int ) $intCid . '
						AND bwgl.id IN ( ' . sqlIntImplode( $arrintBudgetWorksheetGlAccountIds ) . ' )';

		return self::fetchBudgetWorksheetGlAccounts( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountsByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		if( false == valId( $intBudgetWorkbookId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bwga.*
					FROM
						budget_workbook_assumptions bwa
						JOIN budget_worksheets bw ON ( bw.cid = bwa.cid AND bw.budget_workbook_id = bwa.budget_workbook_id )
						JOIN budget_worksheet_gl_accounts bwga ON ( bwga.cid = bw.cid AND bwga.budget_worksheet_id = bw.id AND bwga.formula LIKE CONCAT( \'%(\', bwa.key, \')%\' ) )
					WHERE
						bwa.cid = ' . ( int ) $intCid . '
						AND bwa.deleted_by IS NULL
						AND bwa.budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . '
						AND bwa.budget_assumption_type_id  = ' . CBudgetAssumptionType::CUSTOM . '
						AND bws.deleted_by IS NULL
					GROUP BY
						bwga.id,
						bwga.cid';

		return self::fetchBudgetWorksheetGlAccounts( $strSql, $objDatabase );
	}

	public static function fetchBudgetWorksheetGlAccountFormulasByBudgetWorkbookIdByCid( $intBudgetWorkbookId, $intCid, $objDatabase ) {
		if( false == valId( $intBudgetWorkbookId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						bwga.formula
					FROM
						budget_worksheet_gl_accounts bwga
						JOIN budget_worksheets bw ON ( bw.cid = bwga.cid AND bw.id = bwga.budget_worksheet_id AND bw.budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . ' )
					WHERE
						bwga.cid = ' . ( int ) $intCid . '
						AND bwga.budget_data_source_type_id = ' . ( int ) CBudgetDataSourceType::CUSTOM_FORMULAS . '
						AND bwga.formula IS NOT NULL
						AND bw.deleted_by IS NULL
					UNION
					SELECT
						bwi.formula
					FROM
						budget_worksheet_items bwi
						JOIN budget_worksheets bw ON ( bw.cid = bwi.cid AND bw.id = bwi.budget_worksheet_id AND bw.budget_workbook_id = ' . ( int ) $intBudgetWorkbookId . ' )
					WHERE
						bwi.cid = ' . ( int ) $intCid . '
						AND bwi.budget_data_source_type_id = ' . ( int ) CBudgetDataSourceType::CUSTOM_FORMULAS . '
						AND bwi.formula IS NOT NULL
						AND bw.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

}

?>