<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPolicyMatchTypes
 * Do not add any new functions to this class.
 */

class CPolicyMatchTypes extends CBasePolicyMatchTypes {

	public static function fetchPolicyMatchTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CPolicyMatchType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPolicyMatchType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CPolicyMatchType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>