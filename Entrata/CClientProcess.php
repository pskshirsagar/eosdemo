<?php

class CClientProcess extends CBaseClientProcess {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRefreshLeaseArchives() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRefreshArTransactions() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRefreshArBalances() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRefreshGlTotals() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRefreshInitialImports() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLeaseArchivesRefreshedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArTransactionsRefreshedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArBalancesRefreshedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valGlTotalsRefreshedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInitialImportsRefreshedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>