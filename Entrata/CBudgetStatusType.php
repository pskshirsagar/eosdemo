<?php

class CBudgetStatusType extends CBaseBudgetStatusType {

	const CURRENT 	= 1;
	const WORKING 	= 2;
	const ARCHIVED 	= 4;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public static function loadBudgetStatusTypesNameArray() {

    	$arrstrBudgetStatusTypesNameArray = [];

    	$arrstrBudgetStatusTypesNameArray[self::CURRENT]  = __( 'Current' );
    	$arrstrBudgetStatusTypesNameArray[self::WORKING]  = __( 'Working' );
    	$arrstrBudgetStatusTypesNameArray[self::ARCHIVED] = __( 'Archived' );

    	return $arrstrBudgetStatusTypesNameArray;
    }

}
?>