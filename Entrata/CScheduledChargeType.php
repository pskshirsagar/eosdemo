<?php

class CScheduledChargeType extends CBaseScheduledChargeType {

	const STANDARD 				= 1;
	const REPORTING_ONLY 		= 2;
	const RENEWAL 				= 3;
	const MONTH_TO_MONTH 		= 4;
	const MONTH_TO_MONTH_FEE 	= 5;
	const INSTALLMENT_SOURCE 	= 6;
	const ARCHIVED_LEASE_RATES 	= 7;
	const LEASE_ABSTRACT		= 8;
	const AFFORDABLE		    = 9;
	const ACCELERATED_RENT  	= 10;
	const EARLY_MOVE_IN			= 11;
	const EXTENSION             = 12;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>