<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CIntegrationContactTypes
 * Do not add any new functions to this class.
 */

class CIntegrationContactTypes extends CBaseIntegrationContactTypes {

	public static function fetchIntegrationContactTypesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM integration_contact_types where cid = ' . ( int ) $intCid;
		return self::fetchIntegrationContactTypes( $strSql, $objDatabase );
	}

	public static function fetchIntegrationContactTypeByIdByPropertyIdByCid( $intIntegrationContactTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						integration_contact_types
					where
						remote_primary_key IS NOT NULL
						AND id = ' . ( int ) $intIntegrationContactTypeId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;
		return self::fetchIntegrationContactType( $strSql, $objDatabase );
	}

	public static function fetchIntegrationContactTypeByPropertyListItemIdByIntegrationContactTypeIdByCid( $intPropertyListItemId, $intIntegrationContactTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ict.*
					FROM
						integration_contact_types ict
					JOIN
						property_list_items pli ON( pli.integration_contact_type_id = ict.id AND pli.cid = ict.cid AND pli.id = ' . ( int ) $intPropertyListItemId . ')
					where
						ict.remote_primary_key IS NOT NULL
						AND ict.id = ' . ( int ) $intIntegrationContactTypeId . '
						AND ict.cid = ' . ( int ) $intCid;
		return self::fetchIntegrationContactType( $strSql, $objDatabase );
	}

	public static function fetchDefaultIntegrationContactType( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						integration_contact_types
					WHERE
						is_denial <> true
						AND is_close <> true
						AND remote_primary_key IS NOT NULL
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY id DESC LIMIT 1';
		return self::fetchIntegrationContactType( $strSql, $objDatabase );
	}

	public static function fetchCancelIntegrationContactTypeByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						integration_contact_types
					where
						remote_primary_key IS NOT NULL
						AND is_close = true
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY id ASC LIMIT 1';
		return self::fetchIntegrationContactType( $strSql, $objDatabase );
	}

	public static function fetchDenialIntegrationContactTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						integration_contact_types
					where
						remote_primary_key IS NOT NULL
						AND is_denial = true
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY id ASC';

		return self::fetchIntegrationContactTypes( $strSql, $objDatabase );
	}

	public static function fetchCancelIntegrationContactTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id,
						name
					FROM
						integration_contact_types
					where
						remote_primary_key IS NOT NULL
						AND ( is_close = true OR is_denial = true )
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY name ASC';

		return self::fetchIntegrationContactTypes( $strSql, $objDatabase );
	}

	public static function fetchContactIntegrationContactTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						id,
						name
					FROM
						integration_contact_types
					where
						remote_primary_key IS NOT NULL
						AND is_contact = true
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid . '
					ORDER BY name ASC';

		return self::fetchIntegrationContactTypes( $strSql, $objDatabase );
	}

	public static function fetchIntegratedIntegrationContactTypesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return true;

		$strSql = 'SELECT
						*
					FROM
						integration_contact_types
					where
						remote_primary_key IS NOT NULL
						AND cid = ' . ( int ) $intCid . '
						AND id IN( ' . implode( ',', $arrintIds ) . ' )
					ORDER BY name ASC';

		return self::fetchIntegrationContactTypes( $strSql, $objDatabase );
	}

	public static function fetchIntegrationContactTypesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						ict.*
					FROM
						integration_contact_types ict
					WHERE
						ict.cid	= ' . ( int ) $intCid . '
						AND ict.property_id	= ' . ( int ) $intPropertyId;

		return self::fetchIntegrationContactTypes( $strSql, $objDatabase );
	}
}
?>