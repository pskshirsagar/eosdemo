<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportItemTypes
 * Do not add any new functions to this class.
 */

class CReportItemTypes extends CBaseReportItemTypes {

	public static function fetchReportItemTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReportItemType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReportItemType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReportItemType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>