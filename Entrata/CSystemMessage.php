<?php

use Psi\Eos\Entrata\CScheduledTasks;
use Psi\Eos\Entrata\CSystemMessageTemplateCustomTexts;
use Psi\Eos\Entrata\CSystemMessageTemplateSlotImages;
use Psi\Eos\Entrata\CSystemMessageTemplateSlots;
use Psi\Eos\Entrata\CFileAssociations;

class CSystemMessage extends CBaseSystemMessage {

	protected $m_strSystemMessageTypeName;
	protected $m_strSystemMessageCategoryName;
	protected $m_strSystemMessageAudienceName;
	protected $m_strFromEmailAddress;
	protected $m_strCcEmailAddresses;
	protected $m_strBccEmailAddresses;
	protected $m_strReplyTo;
	protected $m_strPropertyPreference;

	protected $m_intPropertyHeaderFooterOptionTypeId;

	protected $m_arrobjMergeFieldGroups;
	protected $m_arrobjFiles;

	const IS_ENABLED_FOR_CONTACT_POINTS = true;
	const SYSTEM_MESSAGE_MAX_QUEUE_PRIORITY                  = 3;
	const SYSTEM_MESSAGE_QUEUE_IMMEDIATE_PRIORITY            = 2;
	const SYSTEM_MESSAGE_QUEUE_DEFAULT_PRIORITY             = 1;
	const SYSTEM_MESSAGE_QUEUE_TEST_CLIENT_PRIORITY          = 0;

	protected static $c_arrstrSystemMessageTemplateSlotImages = array(
		'header_main_image' => [ 'company_media_file_id' => NULL, 'fullsize_uri' => '', 'link' => NULL, 'link_target' => NULL ],
		'left_image' 		=> [ 'company_media_file_id' => NULL, 'fullsize_uri' => '', 'link' => NULL, 'link_target' => NULL ],
		'center_image' 		=> [ 'company_media_file_id' => NULL, 'fullsize_uri' => '', 'link' => NULL, 'link_target' => NULL ],
		'right_image' 		=> [ 'company_media_file_id' => NULL, 'fullsize_uri' => '', 'link' => NULL, 'link_target' => NULL ],
	);

	protected static $c_arrstrEmailFileAttachments = array(
		'file1' => array( 'id' => NULL, 'title' => '' ),
		'file2' => array( 'id' => NULL, 'title' => '' ),
		'file3' => array( 'id' => NULL, 'title' => '' ),
	);

	public function setSystemMessageTypeName( $strSystemMessageTypeName ) {
		$this->m_strSystemMessageTypeName = $strSystemMessageTypeName;
	}

	public function setSystemMessageCategoryName( $strSystemMessageCategoryName ) {
		$this->m_strSystemMessageCategoryName = $strSystemMessageCategoryName;
	}

	public function setSystemMessageAudienceName( $strSystemMessageAudienceName ) {
		$this->m_strSystemMessageAudienceName = $strSystemMessageAudienceName;
	}

	public function setMergeFieldGroups( $arrobjMergeFieldGroups ) {
		$this->m_arrobjMergeFieldGroups = $arrobjMergeFieldGroups;
	}

	public function setFiles( $arrobjFiles ) {
		$this->m_arrobjFiles = $arrobjFiles;
	}

	public function setDefaultMergeFieldGroupIds( $arrintDefaultMergeFieldGroupIds ) {
		$this->m_arrintDefaultMergeFieldGroupIds = CStrings::strToArrIntDef( $arrintDefaultMergeFieldGroupIds, NULL );
	}

	public function getSystemMessageTypeName() {
		return $this->m_strSystemMessageTypeName;
	}

	public function getSystemMessageCategoryName() {
		return $this->m_strSystemMessageCategoryName;
	}

	public function getSystemMessageAudienceName() {
		return $this->m_strSystemMessageAudienceName;
	}

	public function getMergeFieldGroups() {
		return $this->m_arrobjMergeFieldGroups;
	}

	public function getSystemMessageTemplateCustomTexts( $objClientDatabase ) {
		$arrstrSystemMessageTemplateCustomTexts = ( array ) CSystemMessageTemplateCustomTexts::createService()->fetchSimpleSystemMessageTemplateCustomTextsBySystemMessageIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		return $arrstrSystemMessageTemplateCustomTexts;
	}

	public function getSystemMessageTemplateSlotImages( $objDatabase ) {
		if( false == valStr( $this->getId() ) ) return false;

		$arrstrSystemMessageTemplateSlotImages = array();

		$arrstrSystemMessageTemplateSlotName    = ( array ) CSystemMessageTemplateSlots::createService()->fetchSimpleSystemMessageTemplateSlots( $objDatabase );
		$arrmixSystemMessageTemplateSlotImages  = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSimpleSystemMessageTemplateSlotImagesBySystemMessageIdByCid( $this->getId(), $this->getCid(),  $objDatabase );

		foreach( $arrmixSystemMessageTemplateSlotImages as $arrmixSystemMessageTemplateSlotImage ) {
			if( true == array_key_exists( $arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id'], $arrstrSystemMessageTemplateSlotName ) ) {
				$strKey = $arrstrSystemMessageTemplateSlotName[$arrmixSystemMessageTemplateSlotImage['system_message_template_slot_id']];
				$strFullUri = '';

				if( true == valStr( $arrmixSystemMessageTemplateSlotImage['fullsize_uri'] ) ) {
					$strFullUri = CConfig::get( 'media_library_path' ) . $arrmixSystemMessageTemplateSlotImage['fullsize_uri'];
				}

				$arrstrSystemMessageTemplateSlotImages[$strKey] = array(
					'company_media_file_id' => $arrmixSystemMessageTemplateSlotImage['company_media_file_id'],
					'fullsize_uri'			=> $strFullUri,
					'link'					=> $arrmixSystemMessageTemplateSlotImage['link'],
					'link_target'			=> $arrmixSystemMessageTemplateSlotImage['link_target'],
				);
			}
		}

		$arrstrSystemMessageTemplateSlotImages = array_merge( self::$c_arrstrSystemMessageTemplateSlotImages, $arrstrSystemMessageTemplateSlotImages );

		return $arrstrSystemMessageTemplateSlotImages;
	}

	public function getEmailAttachments( $objClientDatabase ) {
		if( false == valStr( $this->getId() ) ) return false;
		$arrmixFileAttachments = array();

		$this->m_arrobjFiles = ( array ) CFiles::fetchFilesBySystemMessageIdByFileTypeSystemCodeByCid( $this->getId(), CFileType::SYSTEM_CODE_MESSAGE_CENTER, $this->getCid(), $objClientDatabase );

		$intCount = 1;
		foreach( $this->m_arrobjFiles as $objFile ) {
			$strKey = 'file' . $intCount;
			$arrmixFileAttachments[$strKey] = array( 'id' => $objFile->getId(), 'title' => $objFile->getTitle() );
			$intCount++;
		}

		$arrmixFileAttachments = array_merge( self::$c_arrstrEmailFileAttachments, $arrmixFileAttachments );
		return $arrmixFileAttachments;
	}

	public function getFiles() {
		return $this->m_arrobjFiles;
	}

	public function getDefaultMergeFieldGroupIds() {
		return $this->m_arrintDefaultMergeFieldGroupIds;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['system_message_type_name'] ) && $boolDirectSet ) {
			$this->m_strSystemMessageTypeName = trim( $arrmixValues['system_message_type_name'] );
		} elseif( isset( $arrmixValues['system_message_type_name'] ) ) {
			$this->setSystemMessageTypeName( $arrmixValues['system_message_type_name'] );
		}

		if( isset( $arrmixValues['system_message_audience_name'] ) && $boolDirectSet ) {
			$this->m_strSystemMessageAudienceName = trim( $arrmixValues['system_message_audience_name'] );
		} elseif( isset( $arrmixValues['system_message_audience_name'] ) ) {
			$this->setSystemMessageAudienceName( $arrmixValues['system_message_audience_name'] );
		}

		if( isset( $arrmixValues['system_message_category_name'] ) && $boolDirectSet ) {
			$this->m_strSystemMessageCategoryName = trim( $arrmixValues['system_message_category_name'] );
		} elseif( isset( $arrmixValues['system_message_category_name'] ) ) {
			$this->setSystemMessageCategoryName( $arrmixValues['system_message_category_name'] );
		}

		if( isset( $arrmixValues['from_email_address'] ) && $boolDirectSet ) {
			$this->m_strFromEmailAddress = trim( $arrmixValues['from_email_address'] );
		} elseif( isset( $arrmixValues['from_email_address'] ) ) {
			$this->setFromEmailAddress( $arrmixValues['from_email_address'] );
		}

		if( isset( $arrmixValues['cc_email_addresses'] ) && $boolDirectSet ) {
			$this->m_strCcEmailAddresses = trim( $arrmixValues['cc_email_addresses'] );
		} elseif( isset( $arrmixValues['cc_email_addresses'] ) ) {
			$this->setCcEmailAddresses( $arrmixValues['cc_email_addresses'] );
		}

		if( isset( $arrmixValues['bcc_email_addresses'] ) && $boolDirectSet ) {
			$this->m_strBccEmailAddresses = trim( $arrmixValues['bcc_email_addresses'] );
		} elseif( isset( $arrmixValues['bcc_email_addresses'] ) ) {
			$this->setBccEmailAddresses( $arrmixValues['bcc_email_addresses'] );
		}

		if( isset( $arrmixValues['property_header_footer_option_type_id'] ) && $boolDirectSet ) {
			$this->m_intPropertyHeaderFooterOptionTypeId = trim( $arrmixValues['property_header_footer_option_type_id'] );
		} elseif( isset( $arrmixValues['property_header_footer_option_type_id'] ) ) {
			$this->setPropertyHeaderFooterOptionTypeId( $arrmixValues['property_header_footer_option_type_id'] );
		}

		if( isset( $arrmixValues['reply_to'] ) && $boolDirectSet ) {
			$this->m_strReplyTo = trim( $arrmixValues['reply_to'] );
		} elseif( isset( $arrmixValues['reply_to'] ) ) {
			$this->setReplyTo( $arrmixValues['reply_to'] );
		}

		if( isset( $arrmixValues['default_merge_fields'] ) && $boolDirectSet ) {
			$this->m_arrintDefaultMergeFieldGroupIds = trim( $arrmixValues['default_merge_fields'] );
		} elseif( isset( $arrmixValues['default_merge_fields'] ) ) {
			$this->setDefaultMergeFieldGroupIds( $arrmixValues['default_merge_fields'] );
		}

		if( isset( $arrmixValues['property_preference'] ) ) {
			$this->setPropertyPreference( $arrmixValues['property_preference'] );
		}

		if( isset( $arrmixValues['preheader'] ) ) {
			$this->setPreheader( $arrmixValues['preheader'] );
		}

	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemMessageTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSystemMessageTypeId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_message_type_id', __( 'Please select system message type.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSystemMessageAudienceId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSystemMessageAudienceId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_message_audience_id', __( 'Please select system message audience.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSystemMessageCategoryId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSystemMessageCategoryId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_message_category_id', __( 'Please select system message category.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSystemMessageTemplateId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSystemMessageTemplateId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_message_template_id', __( 'Please select template.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		$this->m_strName	= trim( $this->m_strName );

		if( false == isset( $this->m_strName ) || 3 > strlen( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'A name must contain at least 3 characters.' ) ) );
			return false;
		}

		$strName = preg_replace( '/[^a-zA-Z0-9_\-\s]/', '', $this->m_strName );

		if( strlen( $this->m_strName ) != strlen( $strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( "Only alphanumeric characters [0 to 9, A to Z ,'-' , '_'and a-z ] are allowed in name." ) ) );
			return false;
		}

		$this->m_strName = preg_replace( '/(\s\s+)/', ' ', $this->m_strName );

		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMergeFieldGroupIds() {
		$arrintMergeFieldGroupIds = $this->getMergeFieldGroupIds();

		if( true == valArr( $this->m_arrobjMergeFieldGroups ) && true == valArr( $arrintMergeFieldGroupIds ) ) {

			foreach( $arrintMergeFieldGroupIds as $intMergeFieldGroupId ) {
				if( false == array_key_exists( $intMergeFieldGroupId, $this->m_arrobjMergeFieldGroups ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merge_field_group_ids', __( 'Please select valid Merge Field Groups.' ) ) );
					return false;
				}
			}
		}

		return true;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiredObjects() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valSystemMessageTypeId();
				$boolIsValid &= $this->valSystemMessageAudienceId();
				$boolIsValid &= $this->valSystemMessageCategoryId();
				$boolIsValid &= $this->valSystemMessageTemplateId();
				$boolIsValid &= $this->valMergeFieldGroupIds();
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valSubject();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function createSystemMessage() {
		$objSystemMessage = new CSystemMessage();
		$objSystemMessage->setSystemMessageTemplateId( 1 );
		$objSystemMessage->setCid( 1 );

		return $objSystemMessage;
	}

	public function createCustomSystemMessage( $intPropertyId, $intCid, $strLocaleCode = CLocale::DEFAULT_LOCALE ) {
		$objSystemMessage = new CSystemMessage();
		$objSystemMessage->setCid( $intCid );
		$objSystemMessage->setPropertyId( $intPropertyId );
		$objSystemMessage->setMergeFieldGroupIds( $this->getMergeFieldGroupIds() );
		$objSystemMessage->setSystemMessageTypeId( $this->getSystemMessageTypeId() );
		$objSystemMessage->setSystemMessageAudienceId( $this->getSystemMessageAudienceId() );
		$objSystemMessage->setSystemMessageCategoryId( $this->getSystemMessageCategoryId() );
		$objSystemMessage->setRequiredInputTables( CStrings::arrToStrIntDef( $this->getRequiredInputTables(), NULL ) );
		$objSystemMessage->setKey( $this->getKey() );
		$objSystemMessage->setName( $this->getName() );
		$objSystemMessage->setReplyTo( $this->getReplyTo() );
		$objSystemMessage->setCcEmailAddresses( $this->getCcEmailAddresses() );
		$objSystemMessage->setBccEmailAddresses( $this->getBccEmailAddresses() );
		$objSystemMessage->setLocaleCode( $strLocaleCode );

		$objSystemMessage->setPropertyPreference( $this->getPropertyPreference() );
		$objSystemMessage->setPropertyHeaderFooterOptionTypeId( $this->getPropertyHeaderFooterOptionTypeId() );

		$objSystemMessage->setIsPublished( true );

		return $objSystemMessage;
	}

	public function createCustomSmsSystemMessage( $intPropertyId, $intCid, $strLocaleCode = CLocale::DEFAULT_LOCALE ) {
		$objSystemMessage = new CSystemMessage();
		$objSystemMessage->setCid( $intCid );
		$objSystemMessage->setPropertyId( $intPropertyId );
		$objSystemMessage->setMergeFieldGroupIds( $this->getMergeFieldGroupIds() );
		$objSystemMessage->setSystemMessageTypeId( $this->getSystemMessageTypeId() );
		$objSystemMessage->setSystemMessageAudienceId( $this->getSystemMessageAudienceId() );
		$objSystemMessage->setSystemMessageCategoryId( $this->getSystemMessageCategoryId() );
		$objSystemMessage->setRequiredInputTables( CStrings::arrToStrIntDef( $this->getRequiredInputTables(), NULL ) );
		$objSystemMessage->setKey( $this->getKey() );
		$objSystemMessage->setName( $this->getName() );
		$objSystemMessage->setSystemMessageTemplateId( CSystemMessageTemplate::CUSTOM );
		$objSystemMessage->setPropertyPreference( $this->getPropertyPreference() );
		$objSystemMessage->setLocaleCode( $strLocaleCode );

		$objSystemMessage->setIsPublished( true );

		return $objSystemMessage;
	}

	public function syncCustomSystemMessages( $objDefaultSystemMessage ) {
		$this->setSystemMessageTypeId( $objDefaultSystemMessage->getSystemMessageTypeId() );
		$this->setSystemMessageAudienceId( $objDefaultSystemMessage->getSystemMessageAudienceId() );
		$this->setSystemMessageCategoryId( $objDefaultSystemMessage->getSystemMessageCategoryId() );
		$this->setKey( $objDefaultSystemMessage->getKey() );
		$this->setMergeFieldGroupIds( $objDefaultSystemMessage->getMergeFieldGroupIds() );
		$this->setName( $objDefaultSystemMessage->getName() );
		$this->setRequiredInputTables( stripslashes( CStrings::arrToStrIntDef( $objDefaultSystemMessage->getRequiredInputTables(), NULL ) ) );
		$this->setPropertyPreference( $objDefaultSystemMessage->getPropertyPreference() );
	}

	public function updateSystemMessageTemplateCustomText( $objScheduledTask, $arrstrCustomText, $objCompanyUser, $objClientDatabase ) {
		$strSavingEmailTemplatedCustomTextError = __( 'Error saving email templated custom text' );
		// fetch and delete old custom text
		if( false == valArr( $arrstrCustomText ) ) return false;

		$arrobjSystemMessageTemplateCustomTexts = ( array ) CSystemMessageTemplateCustomTexts::createService()->fetchSystemMessageTemplateCustomTextsBySystemMessageIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		$arrobjSystemMessageTemplateCustomTexts = rekeyObjects( 'key', $arrobjSystemMessageTemplateCustomTexts );
		$arrstrMergeFields		= [ '*NAME_FIRST*', '*NAME_LAST*', '*LEASE_EXPIRATION_DATE*', '*PRIMARY_APPLICANT_NAME*', '*LEASE_RENEWAL_DATE*', '*UNIT_NUMBER*', '*BUILDING_NAME*', '*POLICY_NUMBER*', '*PROPERTY_UNIT_ADDRESS*', '*STUDENT_ID*' ];

		foreach( $arrstrCustomText as $strKey => $strValue ) {

			if( true == array_key_exists( $strKey, $arrobjSystemMessageTemplateCustomTexts ) ) {
				$objSystemMessageTemplateCustomText = $arrobjSystemMessageTemplateCustomTexts[$strKey];
			} else {
				$objSystemMessageTemplateCustomText = new CSystemMessageTemplateCustomText();
				$objSystemMessageTemplateCustomText->setSystemMessageId( $this->getId() );
				$objSystemMessageTemplateCustomText->setCid( $this->getCid() );
				$objSystemMessageTemplateCustomText->setKey( $strKey );
			}
			switch( $this->getSystemMessageAudienceId() ) {

				case CSystemMessageAudience::LEAD:
					foreach( $arrstrMergeFields as $strMergeField ) {
						if( 0 == strcmp( $strMergeField, '*POLICY_NUMBER*' ) ) {
							$strValue = \Psi\CStringService::singleton()->str_ireplace( $strMergeField, '*LEAD_INSURANCE_' . ltrim( $strMergeField, '*' ), $strValue );
						} else {
							$strValue = \Psi\CStringService::singleton()->str_ireplace( $strMergeField, '*LEAD_' . ltrim( $strMergeField, '*' ), $strValue );
						}
					}
					break;

				case CSystemMessageAudience::RESIDENT:
					foreach( $arrstrMergeFields as $strMergeField ) {
						if( 0 == strcmp( $strMergeField, '*POLICY_NUMBER*' ) ) {
							$strValue = \Psi\CStringService::singleton()->str_ireplace( $strMergeField, '*RESIDENT_INSURANCE_' . ltrim( $strMergeField, '*' ), $strValue );
						} else {
							$strValue = \Psi\CStringService::singleton()->str_ireplace( $strMergeField, '*RESIDENT_' . ltrim( $strMergeField, '*' ), $strValue );
						}
					}
					break;

				default:
					$strValue = $strValue;
			}

			if( true == valObj( $objScheduledTask, 'CScheduledTask' ) && true == $this->valId( $objScheduledTask->getComposeEmailId() ) ) {
				$objSystemMessageTemplateCustomText->setScheduledTaskEmailId( $objScheduledTask->getComposeEmailId() );
			}

			$objSystemMessageTemplateCustomText->setSystemMessageTemplateId( $this->getSystemMessageTemplateId() );
			$objSystemMessageTemplateCustomText->setValue( $strValue );

			if( false == $objSystemMessageTemplateCustomText->insertOrUpdate( $objCompanyUser->getId(), $objClientDatabase ) ) {
				$this->addErrorMsg( $strSavingEmailTemplatedCustomTextError );
				return false;
			}
		}
		return true;
	}

	public function updateSystemMessageTemplateSlotImages( $objScheduledTask, $arrmixSlotImages, $objCompanyUser, $objClientDatabase ) {

		$arrmixSystemMessageTemplateSlots       = ( array ) CSystemMessageTemplateSlots::createService()->fetchSimpleSystemMessageTemplateSlotsBySystemMessageTemplateId( $this->getSystemMessageTemplateId(), $objClientDatabase );
		$arrobjSystemMessageTemplateSlotImages  = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSystemMessageTemplateSlotImagesBySystemMessageIdByCid( $this->getId(), $this->getCid(), $objClientDatabase );
		$strSavingContentError = __( 'Error while saving content' );
		$arrintOldCompanyMediaFileIds          = [];
		$arrintNewCompanyMediaFileIds          = [];

		foreach( $arrobjSystemMessageTemplateSlotImages as $objSystemMessageTemplateSlotImage ) {
			if( false == $objSystemMessageTemplateSlotImage->delete( $objCompanyUser->getId(), $objClientDatabase ) ) {
				$this->addErrorMsg( $strSavingContentError );
				return false;
			}
			$arrintOldCompanyMediaFileIds[] = $objSystemMessageTemplateSlotImage->getCompanyMediaFileId();
		}

		if( true == valArr( $arrmixSlotImages ) ) {
			foreach( $arrmixSlotImages as $strImageKey => $arrmixSlotImage ) {
				if( false == valArr( $arrmixSlotImage ) || false == valStr( $arrmixSlotImage['company_media_file_id'] ) ) continue;

				$intSystemMessageTemplateSlotId = array_search( $strImageKey, $arrmixSystemMessageTemplateSlots );

				if( false == is_numeric( $intSystemMessageTemplateSlotId ) ) continue;

				$objCompanyMediaFile = \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $arrmixSlotImage['company_media_file_id'], $this->getCid(), $objClientDatabase );
				if( false == valObj( $objCompanyMediaFile, 'CCompanyMediaFile' ) ) {
					continue;
				}

				$objSystemMessageTemplateSlotImage = new CSystemMessageTemplateSlotImage();
				if( true == valObj( $objScheduledTask, 'CScheduledTask' ) && true == $this->valId( $objScheduledTask->getComposeEmailId() ) ) {
					$objSystemMessageTemplateSlotImage->setScheduledTaskEmailId( $objScheduledTask->getComposeEmailId() );
				}
				$objSystemMessageTemplateSlotImage->setSystemMessageId( $this->getId() );
				$objSystemMessageTemplateSlotImage->setSystemMessageTemplateId( $this->getSystemMessageTemplateId() );
				$objSystemMessageTemplateSlotImage->setCid( $this->getCid() );

				$objSystemMessageTemplateSlotImage->setSystemMessageTemplateSlotId( $intSystemMessageTemplateSlotId );
				$objSystemMessageTemplateSlotImage->setCompanyMediaFileId( $arrmixSlotImage['company_media_file_id'] );
				$objSystemMessageTemplateSlotImage->setLink( $arrmixSlotImage['link'] );
				$objSystemMessageTemplateSlotImage->setLinkTarget( $arrmixSlotImage['link_target'] );

				if( false == $objSystemMessageTemplateSlotImage->insert( $objCompanyUser->getId(), $objClientDatabase ) ) {
					$this->addErrorMsg( $strSavingContentError );
					return false;
				}
				$arrintNewCompanyMediaFileIds[] = $arrmixSlotImage['company_media_file_id'];
			}
		}

		foreach( $arrintOldCompanyMediaFileIds as $intOldComapanyMediaFileId ) {
			if( false == in_array( $intOldComapanyMediaFileId, $arrintNewCompanyMediaFileIds ) ) {
				$objCompanyMediaFile = \Psi\Eos\Entrata\CCompanyMediaFiles::createService()->fetchCompanyMediaFileByIdByCid( $intOldComapanyMediaFileId, $this->getCid(), $objClientDatabase );
				$arrobjSystemMessageTemplateSlotImages = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSystemMessageTemplateSlotImagesByCompanyMediaFileIdsByCid( [ $intOldComapanyMediaFileId ], $this->getCid(), $objClientDatabase );

				if( true == valArr( $arrobjSystemMessageTemplateSlotImages ) ) {
					if( false == CSystemMessageTemplateSlotImages::createService()->bulkDelete( $arrobjSystemMessageTemplateSlotImages, $objClientDatabase ) ) {
						$this->addErrorMsg( $strSavingContentError );
						return false;
					}
				}

				if( true == valObj( $objCompanyMediaFile, 'CCompanyMediaFile' ) ) {
					if( false == $objCompanyMediaFile->delete( $objCompanyUser->getId(), $objClientDatabase ) ) {
						$this->addErrorMsg( $strSavingContentError );
						return false;
					}
				}

			}
		}

		return true;
	}

	public function updateFileAttachments( $objScheduledTask, $arrstrEmailFileAttachments, $objCompanyUser, $objClientDatabase ) {

		$strSavingEmailAttachmentError = __( 'Error while saving email attachments' );

		if( false == valArr( $arrstrEmailFileAttachments ) ) {
			$this->addErrorMsg( $strSavingEmailAttachmentError );
			return false;
		}

		foreach( $arrstrEmailFileAttachments as $arrmixFileAttachment ) {
			if( true == is_numeric( $arrmixFileAttachment['id'] ) ) {

				$objFile 			= CFiles::fetchFileByIdByCid( $arrmixFileAttachment['id'], $this->getCid(), $objClientDatabase );
				$objFileAssociation = CFileAssociations::createService()->fetchFileAssociationByFileIdBySystemMessageIdByCid( $objFile->getId(), $this->getId(), $this->getCid(), $objClientDatabase );

				if( false == valObj( $objFileAssociation, 'CFileAssociation' ) ) {
					$objFileAssociation = new CFileAssociation();
					$objFileAssociation->setCid( $this->getCid() );
					$objFileAssociation->setSystemMessageId( $this->getId() );
				}

				$objFileAssociation->setFileId( $objFile->getId() );

				if( false == $objFile->insertOrUpdate( $objCompanyUser->getId(), $objClientDatabase ) ) {
					$objFile->deleteFile();
					$this->m_objDatabase->rollback();
					$this->addErrorMsg( $strSavingEmailAttachmentError );
					return false;
				}
				if( true == valObj( $objScheduledTask, 'CScheduledTask' ) && true == $this->valId( $objScheduledTask->getComposeEmailId() ) ) {
					$objFileAssociation->setScheduledTaskEmailId( $objScheduledTask->getComposeEmailId() );
				}

				if( false == $objFileAssociation->insertOrUpdate( $objCompanyUser->getId(), $objClientDatabase ) ) {
					$this->m_objDatabase->rollback();
					$this->addErrorMsg( $strSavingEmailAttachmentError );
					return false;
				}
			}
		}

		return true;
	}

	public function revertToDefaultSystemMessage( $intScheduledTaskId, $intCompanyUserId, $objDatabase ) {
		$boolIsSuccess										= true;
		$arrobjTempSystemMessageTemplateCustomTexts			= [];
		$arrobjTempDefaultSystemMessageTemplateSlotImages	= [];
		$arrobjTempDefaultFileAssociations					= [];

		$objScheduledTask = CScheduledTasks::createService()->fetchScheduledTaskByIdByCid( $intScheduledTaskId, $this->getCid(), $objDatabase );

		$objDefaultSystemMessage = \Psi\Eos\Entrata\CSystemMessages::createService()->fetchDefaultSystemMessageByKeyBySystemMessageAudienceId( $this->getKey(), $this->getSystemMessageAudienceId(), $objDatabase );

		if( true == valObj( $objDefaultSystemMessage, 'CSystemMessage' ) ) {
			$arrobjDefaultSystemMessageTemplateCustomTexts	= ( array ) CSystemMessageTemplateCustomTexts::createService()->fetchSystemMessageTemplateCustomTextsBySystemMessageIdByCid( $objDefaultSystemMessage->getId(), $objDefaultSystemMessage->getCid(), $objDatabase );
			$arrobjDefaultSystemMessageTemplateSlotImages	= ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSystemMessageTemplateSlotImagesBySystemMessageIdByCid( $objDefaultSystemMessage->getId(), $objDefaultSystemMessage->getCid(), $objDatabase );
			$arrobjDefaultFileAssociations					= ( array ) CFileAssociations::createService()->fetchFileAssociationsBySystemMessageIdByCid( $objDefaultSystemMessage->getId(), $objDefaultSystemMessage->getCid(), $objDatabase );
		}

		$objDatabase->begin();

		switch( NULL ) {
			default:
				if( true == valObj( $this, 'CSystemMessage' ) && CClient::ID != $this->getCid() && true == valId( $this->getPropertyId() ) ) {
					$arrobjSystemMessageTemplateCustomText = ( array ) CSystemMessageTemplateCustomTexts::createService()->fetchSystemMessageTemplateCustomTextsBySystemMessageIdByCid( $this->getId(), $this->getCid(), $objDatabase );
					if( true == valArr( $arrobjSystemMessageTemplateCustomText ) ) {
						if( false == CSystemMessageTemplateCustomTexts::createService()->bulkDelete( $arrobjSystemMessageTemplateCustomText, $objDatabase ) ) {
							$boolIsSuccess = false;
							$objDatabase->rollback();
							break;
						}
					}

					$arrobjSystemMessageTemplateSlotImages  = ( array ) CSystemMessageTemplateSlotImages::createService()->fetchSystemMessageTemplateSlotImagesBySystemMessageIdByCid( $this->getId(), $this->getCid(), $objDatabase );
					if( true == valArr( $arrobjSystemMessageTemplateSlotImages ) ) {
						if( false == CSystemMessageTemplateSlotImages::createService()->bulkDelete( $arrobjSystemMessageTemplateSlotImages, $objDatabase ) ) {
							$boolIsSuccess = false;
							$objDatabase->rollback();
							break;
						}
					}

					$arrobjFileAssociation = ( array ) CFileAssociations::createService()->fetchFileAssociationsBySystemMessageIdByCid( $this->getId(), $this->getCid(), $objDatabase );
					if( true == valArr( $arrobjFileAssociation ) ) {
						if( false == CFileAssociations::createService()->bulkDelete( $arrobjFileAssociation, $objDatabase ) ) {
							$boolIsSuccess = false;
							$objDatabase->rollback();
							break;
						}
					}

					foreach( $arrobjDefaultSystemMessageTemplateCustomTexts as $objDefaultSystemMessageTemplateCustomText ) {
						$objInsertDefaultSystemMessageTemplateCustomText = clone $objDefaultSystemMessageTemplateCustomText;
						$objInsertDefaultSystemMessageTemplateCustomText->setSystemMessageId( $this->getId() );
						$objInsertDefaultSystemMessageTemplateCustomText->setCid( $this->getCid() );
						$objInsertDefaultSystemMessageTemplateCustomText->setId( NULL );

						$arrobjTempSystemMessageTemplateCustomTexts[] = $objInsertDefaultSystemMessageTemplateCustomText;
					}

					if( true == valArr( $arrobjTempSystemMessageTemplateCustomTexts ) ) {
						if( false == CSystemMessageTemplateCustomTexts::createService()->bulkInsert( $arrobjTempSystemMessageTemplateCustomTexts, $intCompanyUserId, $objDatabase ) ) {
							$boolIsSuccess = false;
							$objDatabase->rollback();
							break;
						}
					}

					foreach( $arrobjDefaultSystemMessageTemplateSlotImages as $objDefaultSystemMessageTemplateSlotImage ) {
						$objInsertDefaultSystemMessageTemplateSlotImage = clone $objDefaultSystemMessageTemplateSlotImage;
						$objInsertDefaultSystemMessageTemplateSlotImage->setSystemMessageId( $this->getId() );
						$objInsertDefaultSystemMessageTemplateSlotImage->setCid( $this->getCid() );
						$objInsertDefaultSystemMessageTemplateSlotImage->setId( NULL );

						$arrobjTempDefaultSystemMessageTemplateSlotImages[] = $objInsertDefaultSystemMessageTemplateSlotImage;
					}

					if( true == valArr( $arrobjTempDefaultSystemMessageTemplateSlotImages ) ) {
						if( false == CSystemMessageTemplateSlotImages::createService()->bulkInsert( $arrobjTempDefaultSystemMessageTemplateSlotImages, $intCompanyUserId, $objDatabase ) ) {
							$boolIsSuccess = false;
							$objDatabase->rollback();
							break;
						}
					}

					foreach( $arrobjDefaultFileAssociations as $objDefaultFileAssociation ) {
						$objInsertDefaultFileAssociation = clone $objDefaultFileAssociation;
						$objInsertDefaultFileAssociation->setSystemMessageId( $this->getId() );
						$objInsertDefaultFileAssociation->setCid( $this->getCid() );
						$objInsertDefaultFileAssociation->setId( NULL );

						$arrobjTempDefaultFileAssociations[] = $objInsertDefaultFileAssociation;
					}

					if( true == valArr( $arrobjTempDefaultFileAssociations ) ) {
						if( false == CFileAssociations::createService()->bulkInsert( $arrobjTempDefaultFileAssociations, $intCompanyUserId, $objDatabase ) ) {
							$boolIsSuccess = false;
							$objDatabase->rollback();
							break;
						}
					}

				}

				$this->setSubject( $objDefaultSystemMessage->getSubject() );
				$this->setDetails( $objDefaultSystemMessage->getDetails() );
				$this->setSystemMessageTemplateId( $objDefaultSystemMessage->getSystemMessageTemplateId() );

				if( false == $this->update( $intCompanyUserId, $objDatabase ) ) {
					$boolIsSuccess = false;
					$objDatabase->rollback();
				}

				if( true == valId( $intScheduledTaskId ) ) {
					$objScheduledTask = CScheduledTasks::createService()->fetchScheduledTaskByIdByCid( $intScheduledTaskId, $this->getCid(), $objDatabase );
				}
				if( true == valObj( $objScheduledTask, 'CScheduledTask' ) ) {
					$objScheduledTask->setIsEntrataDefault( true );
					if( false == $objScheduledTask->update( $intCompanyUserId, $objDatabase ) ) {
						$boolIsSuccess = false;
						$objDatabase->rollback();
					}
				}
		}

		if( true == $boolIsSuccess ) {
			$objDatabase->commit();
		}
	}

	public function getEmailContent( $objDatabase ) {
		$objSystemMessageContent = new CSystemMessageContentLibrary();
		$objSystemMessageContent->setSystemMessage( $this );
		$objSystemMessageContent->setClientDatabase( $objDatabase );
		$objSystemMessageContent->setSystemEmailTypeId( CSystemEmailType::EVENT_SCHEDULER_EMAIL );

		$strHtmlContent = $objSystemMessageContent->getEmailContent();

		$this->setFiles( $objSystemMessageContent->getFiles() );

		return $strHtmlContent;
	}

	public function createFileAssociation() {
		$objFileAssociation = new CFileAssociation();
		$objFileAssociation->setCid( $this->getCid() );
		$objFileAssociation->setSystemMessageId( $this->getId() );

		return $objFileAssociation;
	}

	public function getSystemMessageFromEmailAddress() {

		$objSystemMessage = \Psi\Eos\Entrata\CSystemMessages::createService()->fetchSystemMessageByIdByCid( $this->getId(), $this->getCid(), $this->m_objDatabase );

		$strFromEmailAddress = CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS; // If nothing is set then we will use system@entrata.com as from email address.
		$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( $objSystemMessage->getPropertyPreference(), $objSystemMessage->getPropertyId(), $objSystemMessage->getCid(), $this->m_objDatabase );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && true == CValidation::validateEmailAddresses( $objPropertyPreference->getValue() ) ) {
			$strFromEmailAddress = convertNonAsciiCharactersToAscii( $objPropertyPreference->getValue() );
		}

		$objPropertyPreference = \Psi\Eos\Entrata\CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'EVENT_SCHEDULER_EMAIL_RELAY', $objSystemMessage->getPropertyId(), $objSystemMessage->getCid(), $this->m_objDatabase );

		if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) && 1 == $objPropertyPreference->getValue() ) {
			$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $objSystemMessage->getPropertyId(), $objSystemMessage->getCid(), $this->m_objDatabase );
			$strFromEmailAddress	= \Psi\CStringService::singleton()->strtolower( getAlfaNumericValue( convertNonAsciiCharactersToAscii( $objProperty->getPropertyName() ) ) ) . '@' . CConfig::get( 'email_relay_domain' );
		}

		return $strFromEmailAddress;

	}

}
?>
