<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CImportEntityReports
 * Do not add any new functions to this class.
 */

class CImportEntityReports extends CBaseImportEntityReports {

	public static function fetchImportEntityReports( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CImportEntityReport', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchImportEntityReport( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CImportEntityReport', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedImportEntityReports( $objDatabase, $boolIsSkipDeprecatedReport = false ) {
		$strSubCondition = '';
		if( true == $boolIsSkipDeprecatedReport ) {
			$strSubCondition = ' AND default_report_id <> ' . CDefaultReport::LEAD_DETAILS . '';
		}
		return self::fetchImportEntityReports( 'SELECT * FROM import_entity_reports WHERE is_published = 1 ' . $strSubCondition, $objDatabase );
	}

}
?>