<?php

class CSubsidyCitizenshipEligibilityType extends CBaseSubsidyCitizenshipEligibilityType {

	const NOT_SUBJECT				= 1;
	const ALL_MEMBERS_ELIGIBLE		= 2;
	const CONTINUED_ASSISTANCE		= 3;
	const PRORATED_ASSISTANCE		= 4;
	const FULL_ASSISTANCE			= 5;
	const TEMPORARY_DEFFERRAL		= 6;

	public static function getIdByCode( $strSubsidyCitizenshipEligibilityTypeCode ) {
		$intSubsidyCitizenshipEligibilityTypeId = NULL;

		switch( $strSubsidyCitizenshipEligibilityTypeCode ) {
			case 'N':
				$intSubsidyCitizenshipEligibilityTypeId = self::NOT_SUBJECT;
				break;

			case 'E':
				$intSubsidyCitizenshipEligibilityTypeId = self::ALL_MEMBERS_ELIGIBLE;
				break;

			case 'C':
				$intSubsidyCitizenshipEligibilityTypeId = self::CONTINUED_ASSISTANCE;
				break;

			case 'P':
				$intSubsidyCitizenshipEligibilityTypeId = self::PRORATED_ASSISTANCE;
				break;

			case 'F':
				$intSubsidyCitizenshipEligibilityTypeId = self::FULL_ASSISTANCE;
				break;

			case 'T':
				$intSubsidyCitizenshipEligibilityTypeId = self::TEMPORARY_DEFFERRAL;
				break;

			default:
				// Invalid Code
				break;
		}

		return $intSubsidyCitizenshipEligibilityTypeId;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>