<?php

class CAmenity extends CBaseAmenity {

	protected $m_boolIsInserted;
	protected $m_intArCascadeReferenceId;
	protected $m_intPropertyFloorPlanId;
	protected $m_arrobjRateAssociations;

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['is_inserted'] ) ) {
			$this->setIsInserted( $arrmixValues['is_inserted'] );
		}
		if( true == isset( $arrmixValues['ar_cascade_reference_id'] ) ) {
			$this->setArCascadeReferenceId( $arrmixValues['ar_cascade_reference_id'] );
		}
	}

	// Get Functions

	public function getIsInserted() {
		return $this->m_boolIsInserted;
	}

	public function getArCascadeReferenceId() {
		return $this->m_intArCascadeReferenceId;
	}

	public function getPropertyFloorPlanId() {
		return $this->m_intPropertyFloorPlanId;
	}

	// Set Functions

	public function setIsInserted( $boolIsInserted ) {
		$this->m_boolIsInserted = $boolIsInserted;
	}

	public function setArCascadeReferenceId( $intArCascadeReferenceId ) {
		$this->m_intArCascadeReferenceId = $intArCascadeReferenceId;
	}

	public function setPropertyFloorPlanId( $intPropertyFloorPlanId ) {
		$this->m_intPropertyFloorPlanId = $intPropertyFloorPlanId;
	}

	public function createAmenityRateAssociation() {

		$objRateAssociation = new CAmenityRateAssociation();
		$objRateAssociation->setDefaults();
		$objRateAssociation->setCid( $this->m_intCid );
		$objRateAssociation->setArOriginId( CArOrigin::AMENITY );

		return $objRateAssociation;
	}

	public function addRateAssociation( $objRateAssociation ) {
		$this->m_arrobjRateAssociations[$objRateAssociation->getId()] = $objRateAssociation;
	}

	public function getRateAssociations() {
		return $this->m_arrobjRateAssociations;
	}

	public function fetchAmenityRateAssociationByPropertyId( $intPropertyId, $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationByArCascadeIdByArCascadeReferenceIdByArOriginReferenceIdByPropertyIdByCid( CArCascade::PROPERTY, $intPropertyId, $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );
	}

	public function fetchAmenityRateAssociations( $objDatabase ) {
		return \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsByArOriginReferenceIdByCid( $this->getId(), $this->getCid(), $objDatabase );
	}

	// Val Functions

	public function valId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Amenity id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client id does not appear valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valExternalVideoUrl() {
		$boolIsValid = true;

		if( true == valStr( $this->m_strExternalVideoUrl ) && false == CValidation::checkUrl( $this->m_strExternalVideoUrl, false, true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'external_video_url', CDisplayMessages::create()->getMessage( 'VALID_URL_REQUIRED' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'amenity name does not appear valid.' ) ) );
		}

		if( true == $boolIsValid && true == isset ( $objDatabase ) ) {
			$objAmenity = \Psi\Eos\Entrata\CAmenities::createService()->fetchAmenityByNameByAmenityTypeIdByCid( $this->m_strName, $this->m_intAmenityTypeId, $this->m_intCid, $objDatabase );

			if( true == valObj( $objAmenity, 'CAmenity' ) && $this->m_intId != $objAmenity->getId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'The name you chose is already being used.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsFromBulkAmenities = false ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $this->m_objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valName( $this->m_objDatabase );
				if( false == $boolIsFromBulkAmenities ) {
					$boolIsValid &= $this->valExternalVideoUrl();
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	// Other Functions

	public function deleteAmenity( $intArCascadeId, $intPropertyId, $intCurrentUserId, $objDatabase ) {
		if( 0 >= ( int ) $intArCascadeId || 0 >= ( int ) $intPropertyId ) {
			return false;
		}

		/**
		 *	Comment: DELETE AMENITY
		 *	1. Delete rates function
		 *	2. Hard delete rate associations
		 *	3. Soft delete marketing media associations
		 *	4. Hard delete amenity record per conditions
		 */

		// Rates Criteria
		$objRatesCriteria	= new CRatesCriteria();

		$objRatesCriteria->setCid( $this->getCid() );
		$objRatesCriteria->setPropertyId( $intPropertyId );
		$objRatesCriteria->setArCascadeId( $intArCascadeId );
		$objRatesCriteria->setArOriginId( CArOrigin::AMENITY );
		$objRatesCriteria->setArOriginReferenceId( $this->getId() );

		$objRatesLibrary	= new CRatesLibrary( $objRatesCriteria );

		// Rate Associations
		$arrobjAmenityRateAssociations = ( array ) \Psi\Eos\Entrata\CRateAssociations::createService()->fetchRateAssociationsByArCascadeIdByArOriginIdByArOriginReferenceIdByPropertyIdByCid( $intArCascadeId, CArOrigin::AMENITY, $this->getId(), $intPropertyId, $this->getCid(), $objDatabase );

		// Media Files
		if( true == valArr( $arrobjAmenityRateAssociations ) ) {
			$arrobjMarketingMediaAssociations	= \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->fetchActiveMarketingMediaAssociationsByIdsByCid( array_keys( rekeyObjects( 'MarketingMediaAssociationId', $arrobjAmenityRateAssociations, false, true ) ), $this->getCid(), $objDatabase );
		}

		if( true == valArr( $arrobjMarketingMediaAssociations ) ) {
			foreach( $arrobjMarketingMediaAssociations as $objMarketingAssociation ) {
				$objMarketingAssociation->setDeletedBy( $intCurrentUserId );
				$objMarketingAssociation->setDeletedOn( 'NOW()' );
			}
		}

		// Is Delete Amenity
		$boolIsDeleteAmenity		= \Psi\Eos\Entrata\Custom\CAmenityRateAssociations::createService()->fetchAmenityRateAssociationsDependencyByArOriginReferenceIdByExcludeRateAssociationIdsByCid( $this->getId(), array_keys( $arrobjAmenityRateAssociations ), $this->getCid(), $objDatabase );
		if( true == $boolIsDeleteAmenity && $intCurrentUserId == $this->getCreatedBy() ) {
			$boolIsDeleteAmenity = true;
		} else {
			$boolIsDeleteAmenity = false;
		}

		$arrintAmenityRateAssociationIds = array_keys( $arrobjAmenityRateAssociations );

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( false == $objRatesLibrary->deleteRates( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}

				$arrobjPropertySellingPoints    = ( array ) CPropertySellingPoints::fetchPropertySellingPointsByRateAssociationIdsByCidByPropertyId( $arrintAmenityRateAssociationIds, $this->getCid(), $intPropertyId, $this->m_objDatabase );
				$arrobjComparableAssociations   = ( array ) CComparableAssociations::fetchComparableAssociationByComparableReferenceIdsByCidByPropertyId( $arrintAmenityRateAssociationIds, $this->getCid(), $intPropertyId, $this->m_objDatabase );

				foreach( $arrobjPropertySellingPoints as $objPropertySellingPoint ) {
					if( false == $objPropertySellingPoint->delete( $intCurrentUserId, $this->m_objDatabase ) ) {
						$this->m_objDatabase->rollback();
						break 2;
					}
				}

				foreach( $arrobjComparableAssociations as $objComparableAssociation ) {
					if( false == $objComparableAssociation->delete( $intCurrentUserId, $this->m_objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}

				foreach( $arrobjAmenityRateAssociations as $objAmenityRateAssociation ) {
					if( false == $objAmenityRateAssociation->delete( $intCurrentUserId, $objDatabase ) ) {
						$boolIsValid &= false;
						break 2;
					}
				}

				if( true == valArr( $arrobjMarketingMediaAssociations ) && false == \Psi\Eos\Entrata\CMarketingMediaAssociations::createService()->bulkUpdate( $arrobjMarketingMediaAssociations, [ 'deleted_by', 'deleted_on' ], $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}

				if( true == $boolIsDeleteAmenity && false == parent::delete( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid &= false;
					break;
				}
		}
		return $boolIsValid;
	}

}
?>