<?php

class CArCodeSummaryType extends CBaseArCodeSummaryType {

	const NO_SUMMARY 				= 1;
	const DAILY_SUMMARIZATION 		= 2;
	const MONTHLY_SUMMARIZATION 	= 3;
	const COMPLETE_SUMMARIZATION 	= 4;

	protected $m_arrstrAllArCodeSummaryTypes;

	// We might need to remove this static array if not in use.
	public static $c_arrmixArCodeSummaryTypes = [ CArCodeSummaryType::NO_SUMMARY => 'Don\'t Summarize', CArCodeSummaryType::DAILY_SUMMARIZATION => 'Daily Summarization', CArCodeSummaryType::MONTHLY_SUMMARIZATION => 'Monthly Summarization', CArCodeSummaryType::COMPLETE_SUMMARIZATION => 'Complete Summarization' ];

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getAllArCodeSummaryTypes() {
		if( isset( $this->m_arrstrAllArCodeSummaryTypes ) )
			return $this->m_arrstrAllArCodeSummaryTypes;

		$this->m_arrstrAllArCodeSummaryTypes = [
			CArCodeSummaryType::NO_SUMMARY    => __( 'Don\'t Summarize' ),
			CArCodeSummaryType::DAILY_SUMMARIZATION    => __( 'Daily Summarization' ),
			CArCodeSummaryType::MONTHLY_SUMMARIZATION   => __( 'Monthly Summarization' ),
			CArCodeSummaryType::COMPLETE_SUMMARIZATION  => __( 'Complete Summarization' )
		];

		return $this->m_arrstrAllArCodeSummaryTypes;
	}

}
?>