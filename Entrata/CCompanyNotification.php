<?php

class CCompanyNotification extends CBaseCompanyNotification {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyNotificationGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;

		if( false == valStr( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Please select company notification type.' ) ) );
		}

		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;

		if( true == valStr( $this->getStartDatetime() ) && true == valStr( $this->getEndDatetime() ) && ( true == strtotime( $this->getStartDatetime() ) > strtotime( $this->getEndDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Company notification should not expires before it starts.' ) ) );
		}

		return $boolIsValid;
	}

	public function valEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsHighlighted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_leasing_center_company_notification':
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valStartDatetime();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>