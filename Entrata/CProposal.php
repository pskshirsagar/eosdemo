<?php

class CProposal extends CBaseProposal {

	protected $m_strJobName;
	protected $m_strPropertyName;
	protected $m_strContractName;

	/**
	 * Get Function
	 */
	public function getJobName() {
		return $this->m_strJobName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getContractName() {
		return $this->m_strContractName;
	}

	/**
	 * Set Function
	 */
	public function setJobName( $strJobName ) {
		$this->m_strJobName = $strJobName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setContractName( $strContractName ) {
		$this->m_strContractName = $strContractName;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == valId( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valJobId() {
		$boolIsValid = true;

		if( false == valId( $this->getJobId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'job_id', 'Job is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRequestName() {
		$boolIsValid = true;

		if( false == valStr( $this->getRequestName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_name', 'RFP Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEstimateAmount() {
		$boolIsValid = true;

		if( 100000000000 < $this->getEstimateAmount() || $this->getEstimateAmount() < 0 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimate_amount', 'Expected estimate should be greater than zero or less than or equal to $100000000000.00' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRequestName();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valJobId();
				$boolIsValid &= $this->valEstimateAmount();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['job_name'] ) ) $this->setJobName( $arrmixValues['job_name'] );
		if( true == isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['contract_name'] ) ) $this->setContractName( $arrmixValues['contract_name'] );
	}

	/**
	 * Create Functions
	 */

	public function createProposalApPayee() {
		$objProposalApPayee = new CProposalApPayee();
		$objProposalApPayee->setCid( $this->getCid() );

		return $objProposalApPayee;
	}

}
?>