<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserReportHistories
 * Do not add any new functions to this class.
 */

class CCompanyUserReportHistories extends CBaseCompanyUserReportHistories {

	public static function fetchCompanyUserReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return self::fetchCachedObjects( $strSql, 'CCompanyUserReportHistory', $objDatabase, DATA_CACHE_MEMORY, NULL, false, $boolIsReturnKeyedArray );
	}

	public static function fetchCompanyUserReportHistory( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCompanyUserReportHistory', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCompanyUserReportHistoriesByCompanyUserIdByIsArchivedByByCidByFromDateByToDate( $intCompanyUserId, $intIsArchived, $intCid, $objDatabase, $strFromDate = NULL, $strToDate = NULL ) {
		$strDateRangeFromSql	= ( false == is_null( $strFromDate ) ? ' AND curh.created_on >= \'' . $strFromDate . '\'' : '' );
		$strDateRangeToSql		= ( false == is_null( $strToDate ) ? ' AND curh.created_on <= \'' . $strToDate . '\'' : '' );

		$strSql = '
			SELECT
				curh.id,
				rh.id AS report_history_id,
				curh.created_on,
				rf.report_id,
				rh.report_filter_id,
				rh.report_group_id,
				rh.report_version_id,
				rs.scheduled_task_id,
				rg.id AS packet_id,
				rs.id AS report_schedule_id,
				rh.download_options,
				rst.id AS packet_type_id,
				COALESCE( util_get_translated( \'name\', ri.name, ri.details ), util_get_translated( \'title\', r.title, r.details ) ) AS report_name,
				rf.name AS report_filter_name,
				util_get_translated( \'name\', rg.name, rg.details ) AS packet_name,
				CASE
					WHEN
						cu.company_employee_id IS NOT NULL
					THEN
						ce.name_last || \', \' || ce.name_first
					ELSE cu.username
				END AS from_user,
				rh.correlation_id,
				rh.filters,
				fa.id AS file_association_id
			FROM
				company_user_report_histories curh
				LEFT JOIN file_associations AS fa ON ( fa.company_user_report_history_id = curh.id AND fa.cid = curh.cid )
				JOIN report_histories rh ON ( rh.cid = curh.cid AND rh.id = curh.report_history_id )
				JOIN reports r ON ( r.cid = rh.cid AND r.id = rh.report_id )
				JOIN report_versions rv ON ( rv.cid = rh.cid AND rv.id = rh.report_version_id )
				LEFT JOIN report_filters rf ON ( rh.cid = rf.cid AND rh.report_filter_id = rf.id AND rf.deleted_by IS NULL )
				LEFT JOIN report_schedules rs ON ( rh.cid = rs.cid AND rh.report_schedule_id = rs.id )
				LEFT JOIN report_schedule_types rst ON ( rs.report_schedule_type_id = rst.id )
				JOIN company_users cu ON ( cu.cid = rh.cid AND cu.id = rh.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
				LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
				LEFT JOIN report_groups rg ON ( rg.cid = rh.cid AND rg.id = rh.report_group_id )
				LEFT JOIN report_instances ri ON ( ri.cid = rg.cid AND ri.report_group_id = rg.id AND ri.report_version_id = rv.id AND ri.report_filter_id = rf.id AND ri.deleted_on IS NULL )
			WHERE
				curh.cid = ' . ( int ) $intCid . '
				AND curh.company_user_id =  ' . ( int ) $intCompanyUserId . '
				AND r.is_published = TRUE
				AND curh.is_temporary = FALSE
				AND rh.report_new_instance_id IS NULL
				AND rh.report_new_group_id IS NULL
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND curh.is_archived = ' . ( int ) $intIsArchived . '
				' . $strDateRangeFromSql . '
				' . $strDateRangeToSql . '
			 ORDER BY
				curh.created_on DESC, ri.id, COALESCE( ri.name, r.title )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNewCompanyUserReportHistoriesByCompanyUserIdByIsArchivedByCidByFromDateByToDate( $intCompanyUserId, $intIsArchived, $intCid, $objDatabase, $strFromDate = NULL, $strToDate = NULL ) {

		$strDateRangeFromSql	= ( false == is_null( $strFromDate ) ? ' AND curh.created_on >= \'' . $strFromDate . '\'' : '' );
		$strDateRangeToSql		= ( false == is_null( $strToDate ) ? ' AND curh.created_on <= \'' . $strToDate . '\'' : '' );

		$strSql = '
			SELECT
				curh.id,
				rh.id AS report_history_id,
				curh.created_on,
				rh.report_new_instance_id,
				rh.report_new_group_id,
				rh.report_version_id,
				rs.scheduled_task_id,
				rng.id AS packet_id,
				rs.id as report_schedule_id,
				rh.download_options,
				( CASE 
					WHEN ( rh.download_options ->> \'screen\' ) = \'[]\' OR strpos ( ( ( rh.download_options ->> \'screen\' )::json -> \'format\' )::TEXT, \'"json"\' )::INTEGER > 0 
					THEN 1
					ELSE 0
				END ) AS download_option_type,
				rst.id AS packet_type_id,
				COALESCE( util_get_translated( \'name\', rni.name, rni.details ), util_get_translated( \'title\', r.title, r.details ) ) AS report_name,
				rng.name AS packet_name,
				CASE
					WHEN
						cu.company_employee_id IS NOT NULL
					THEN
						ce.name_last || \', \' || ce.name_first
					ELSE cu.username
				END AS from_user,
				rh.correlation_id,
				rh.filters
			FROM
				company_user_report_histories curh
				JOIN file_associations AS fa ON ( fa.company_user_report_history_id = curh.id AND fa.cid = curh.cid )
				JOIN report_histories rh ON ( rh.cid = curh.cid AND rh.id = curh.report_history_id )
				JOIN reports r ON ( r.cid = rh.cid AND r.id = rh.report_id )
				JOIN report_versions rv ON ( rv.cid = rh.cid AND rv.id = rh.report_version_id )
				LEFT JOIN report_schedules rs ON ( rh.cid = rs.cid AND rh.report_schedule_id = rs.id )
				LEFT JOIN report_schedule_types rst ON ( rs.report_schedule_type_id = rst.id )
				JOIN company_users cu ON ( cu.cid = rh.cid AND cu.id = rh.created_by AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
				LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
				LEFT JOIN report_new_groups rng ON ( rng.cid = rh.cid AND rng.id = rh.report_new_group_id )
				LEFT JOIN report_new_instances rni ON ( rni.cid = rh.cid AND rni.id = rh.report_new_instance_id AND rni.report_version_id = rv.id AND rni.deleted_on IS NULL )
				LEFT JOIN report_new_group_instances rngi ON ( rngi.cid = rni.cid AND rni.id = rngi.report_new_instance_id AND rng.id = rngi.report_new_group_id AND rngi.deleted_on IS NULL )
			WHERE
				curh.cid = ' . ( int ) $intCid . '
				AND curh.company_user_id =  ' . ( int ) $intCompanyUserId . '
				AND r.is_published = TRUE
				AND curh.is_temporary = FALSE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND curh.is_archived = ' . ( int ) $intIsArchived . '
				AND ( rh.report_new_group_id IS NOT NULL OR rh.report_new_instance_id IS NOT NULL )
				' . $strDateRangeFromSql . '
				' . $strDateRangeToSql . '
			 ORDER BY
				curh.created_on DESC, rngi.id, COALESCE( rni.name, r.title )';
		// Adding order by rngi.id clause so that this listing should show instances in order in which user added those.

		return fetchData( $strSql, $objDatabase );
	}

	/**
	 * @param $arrintScheduledTaskIds
	 * @param $arrstrCreatedOnDates
	 * @param $intCompanyUserId
	 * @param $intCid
	 * @param $objDatabase
	 * @return CCompanyUserReportHistory[]
	 */
	public static function fetchCompanyUserReportHistoryByScheduledTaskIdsByCreatedOnByCompanyUserIdByCid( $arrintScheduledTaskIds, $arrstrCreatedOnDates, $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				curh.*
			FROM
				company_user_report_histories curh
				JOIN report_histories rh ON ( curh.cid = rh.cid AND curh.report_history_id = rh.id )
				JOIN report_schedules rs ON ( rh.cid = rs.cid AND rh.report_schedule_id = rs.id )
				JOIN scheduled_tasks st ON ( st.cid = rs.cid AND st.id = rs.scheduled_task_id )
				JOIN report_filters rf ON ( rh.cid = rf.cid AND rf.deleted_by IS NULL AND rh.report_filter_id = rf.id )
			WHERE
				curh.cid = ' . ( int ) $intCid . '
				AND st.id IN ( ' . implode( ',', $arrintScheduledTaskIds ) . ')
				AND curh.company_user_id =  ' . ( int ) $intCompanyUserId . '
				AND st.deleted_by IS NULL
				AND st.deleted_on IS NULL
				AND date_trunc( \'second\', curh.created_on ) IN ( ' . implode( ',', $arrstrCreatedOnDates ) . ')';

		return self::fetchCompanyUserReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchCompanyUserReportHistoryByReportHistoryIdByCompanyUserIdByCid( $intReportHistoryId, $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				curh.*
			FROM
				company_user_report_histories curh
			WHERE
				curh.cid = ' . ( int ) $intCid . '
				AND curh.company_user_id = ' . ( int ) $intCompanyUserId . '
				AND curh.report_history_id = ' . ( int ) $intReportHistoryId . '
			LIMIT 1';

		return self::fetchCompanyUserReportHistory( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserReportHistoriesForDeletion( $objDatabase, $intPerUserLimit = NULL, $intAgeLimit = NULL ) {
		$strPerUserLimitCondition	= ( ( int ) $intPerUserLimit ) > 0 ? 'ROW_NUMBER() OVER( PARTITION BY curh.cid, curh.company_user_id ORDER BY curh.created_on DESC ) > ' . ( int ) $intPerUserLimit : 'FALSE';
		$strAgeLimitCondition		= ( ( int ) $intAgeLimit ) > 0 ? '( now() - curh.created_on ) > \'' . ( int ) $intAgeLimit . ' days\'::INTERVAL' : 'FALSE';

		$strSql = '
			SELECT
				curh.cid,
				curh.company_user_report_history_id,
				curh.report_history_id,
				curh.stored_object_id,
				array_agg(curh.file_association_id) AS file_association_ids,
				array_agg(curh.file_id) AS file_ids,
				array_agg(curh.stored_object_id) AS stored_object_ids,
				array_agg(curh.file_path) AS file_paths,
				curh.to_delete,
				curh.delete_report_history,
				curh.include,
				curh.is_temporary
			FROM
				(
					SELECT
						curh.cid,
						curh.id AS company_user_report_history_id,
						curh.report_history_id,
						curh.is_temporary,
						fa.id AS file_association_id,
						f.id AS file_id,
						f.file_path,
						so.id AS stored_object_id,
						curh.to_delete,
						bool_and(curh.to_delete) OVER( PARTITION BY curh.cid, curh.report_history_id ) AS delete_report_history,
						bool_or(curh.to_delete) OVER( PARTITION BY curh.cid, curh.report_history_id ) AS include
					FROM
						(
							SELECT
								curh.*,
								(
									curh.is_temporary AND
									(
										' . $strPerUserLimitCondition . ' OR
										' . $strAgeLimitCondition . '
									)
								) AS to_delete
							FROM
								company_user_report_histories curh
								JOIN report_histories rh ON curh.cid = rh.cid AND curh.report_history_id = rh.id
							WHERE
								rh.report_schedule_type_id = ' . CReportScheduleType::MANUAL . '
						) AS curh
						LEFT JOIN file_associations fa ON fa.cid = curh.cid AND fa.company_user_report_history_id = curh.id
						LEFT JOIN files f ON f.cid = fa.cid AND f.id = fa.file_id
						LEFT JOIN stored_objects so ON so.cid = f.cid AND so.reference_id = f.id AND so.reference_table = \'' . CBaseFile::TABLE_NAME . '\' AND so.deleted_on IS NULL
					ORDER BY
						curh.cid,
						curh.report_history_id,
						curh.id
				) curh
			WHERE
				curh.to_delete = true
			GROUP BY
				curh.cid,
				curh.report_history_id,
				curh.stored_object_id,
				curh.company_user_report_history_id,
				curh.to_delete,
				curh.delete_report_history,
				curh.include,
				curh.is_temporary
			ORDER BY
				curh.cid,
				curh.report_history_id,
				curh.company_user_report_history_id
		';

		return fetchData( $strSql, $objDatabase ) ?: [];
	}

	/**
	 * @param $arrintReportHistoryIds
	 * @param $intCompanyUserId
	 * @param $intCid
	 * @param $objDatabase
	 * @return CCompanyUserReportHistory[]
	 */
	public static function fetchCompanyUserReportHistoryByReportHistoryIdsByCompanyUserIdByCid( $arrintReportHistoryIds, $arrstrCreatedOnDates,  $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				curh.*
			FROM
				company_user_report_histories curh
				JOIN report_histories rh ON ( curh.cid = rh.cid AND curh.report_history_id = rh.id )
			WHERE
				curh.cid = ' . ( int ) $intCid . '
				AND rh.id IN ( ' . implode( ',', $arrintReportHistoryIds ) . ')
				AND curh.company_user_id = ' . ( int ) $intCompanyUserId . '
				AND date_trunc( \'second\', curh.created_on )  IN ( ' . implode( ',', $arrstrCreatedOnDates ) . ')';

		return self::fetchCompanyUserReportHistories( $strSql, $objDatabase, $boolIsReturnKeyedArray = true );
	}

	public static function fetchUnreadInboxMessageCount( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				curh.*
			FROM
				company_user_report_histories curh
				JOIN file_associations AS fa ON ( fa.company_user_report_history_id = curh.id AND fa.cid = curh.cid )
				JOIN report_histories rh ON ( rh.cid = curh.cid AND rh.id = curh.report_history_id )
				JOIN reports r ON ( r.cid = rh.cid AND r.id = rh.report_id )
				JOIN report_versions rv ON ( rv.cid = rh.cid AND rv.id = rh.report_version_id )
			WHERE
				curh.cid = ' . ( int ) $intCid . '
				AND curh.company_user_id =  ' . ( int ) $intCompanyUserId . '
				AND curh.is_temporary = FALSE
				AND curh.is_archived = 0
				AND curh.viewed_on IS NULL
				AND r.is_published = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND ( rh.report_new_group_id IS NOT NULL OR rh.report_new_instance_id IS NOT NULL )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserReportHistoryByReportHistoryIdsByCid( $arrintReportHistoryIds, $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				curh.*
			FROM
				company_user_report_histories AS curh
			WHERE
				curh.cid = ' . ( int ) $intCid . '
				AND curh.company_user_id = ' . ( int ) $intCompanyUserId . '
				AND curh.report_history_id IN ( ' . implode( ',', $arrintReportHistoryIds ) . ')';

		return self::fetchCompanyUserReportHistories( $strSql, $objDatabase );
	}

}
