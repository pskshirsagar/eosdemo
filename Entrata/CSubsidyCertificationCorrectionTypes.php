<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyCertificationCorrectionTypes
 * Do not add any new functions to this class.
 */

class CSubsidyCertificationCorrectionTypes extends CBaseSubsidyCertificationCorrectionTypes {

	public static function fetchSubsidyCertificationCorrectionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSubsidyCertificationCorrectionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSubsidyCertificationCorrectionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSubsidyCertificationCorrectionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedSubsidyCertificationCorrectionTypes( $objDatabase, $boolShowInEntrata = true ) {
		$strSql = 'SELECT * FROM subsidy_certification_correction_types WHERE is_published = true';
		$strSql .= ( true == $boolShowInEntrata ) ? ' AND show_in_entrata = true' : '';
		return self::fetchSubsidyCertificationCorrectionTypes( $strSql, $objDatabase );
	}

}
?>