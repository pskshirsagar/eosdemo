<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDataCleanupRequests
 * Do not add any new functions to this class.
 */

class CDataCleanupRequests extends CBaseDataCleanupRequests {

	public static function fetchCustomAllDataCleanupRequests( $objDatabase ) {
		$strSql = 'SELECT * FROM data_cleanup_requests WHERE is_published = 1 AND completion_on IS NULL';
		return CDataCleanupRequests::fetchDataCleanupRequests( $strSql, $objDatabase );
	}

	public static function fetchCustomAllDataCleanupRequestsByDataCleanupTypeId( $intDataCleanupTypeId, $objDatabase ) {
		if( false == valId( $intDataCleanupTypeId ) ) return false;

		$strSql = 'SELECT * FROM data_cleanup_requests WHERE data_cleanup_type_id = ' . ( int ) $intDataCleanupTypeId . ' AND is_published = 1 AND completion_on IS NULL';
		return CDataCleanupRequests::fetchDataCleanupRequests( $strSql, $objDatabase );
	}

	public static function fetchCustomAllDataCleanupRequestsByDataCleanupTypeIdByCid( $intDataCleanupTypeId, $intCid, $objDatabase ) {
		if( false == valId( $intDataCleanupTypeId ) || false == valId( $intCid ) ) return false;

		$strSql = 'SELECT * FROM data_cleanup_requests WHERE cid = ' . ( int ) $intCid . ' AND data_cleanup_type_id = ' . ( int ) $intDataCleanupTypeId . ' AND is_published = 1 AND completion_on IS NULL';
		return CDataCleanupRequests::fetchDataCleanupRequests( $strSql, $objDatabase );
	}

	public static function fetchDataCleanupRequestsByRPKByDataCleanupTypeIdByPropertyIdByCid( $strSummary, $intDataCleanupTypeId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intDataCleanupTypeId ) || false == valId( $intCid ) || false == valStr( $strSummary ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						data_cleanup_requests
					WHERE
						CID = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND data_cleanup_type_id = ' . ( int ) $intDataCleanupTypeId . '
						AND CAST( summary as jsonb )->>\'rpk\' = \'' . $strSummary . '\'
						AND is_published = 1
						AND completion_on IS NULL';

		return CDataCleanupRequests::fetchDataCleanupRequest( $strSql, $objDatabase );
	}

	public static function fetchPendingDataCleanupRequestsCountBySourceByDataCleanupTypeByPropertyIdByCId( $strDataCleanupRequestRPK, $intDataCleanupTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strWhereSql = 'WHERE
							CID = ' . ( int ) $intCid . '
							AND property_id = ' . ( int ) $intPropertyId . '
							AND data_cleanup_type_id = ' . ( int ) $intDataCleanupTypeId . '
							AND CAST( summary as jsonb )->>\'source\' = \'' . $strDataCleanupRequestRPK . '\'
							AND ( completion_on IS NULL )';

		return parent::fetchRowCount( $strWhereSql, 'data_cleanup_requests', $objDatabase );
	}

	public static function fetchDataCleanupRequestsBySourceByDataCleanupTypeByPropertyIdByCId( $strDataCleanupRequestRPK, $intDataCleanupTypeId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
							*
					FROM
							data_cleanup_requests
					WHERE
							CID = ' . ( int ) $intCid . '
							AND property_id = ' . ( int ) $intPropertyId . '
							AND data_cleanup_type_id = ' . ( int ) $intDataCleanupTypeId . '
							AND CAST( summary as jsonb )->>\'source\' = \'' . $strDataCleanupRequestRPK . '\'
					ORDER BY id DESC limit 1';

		return CDataCleanupRequests::fetchDataCleanupRequest( $strSql, $objDatabase );
	}

}
?>