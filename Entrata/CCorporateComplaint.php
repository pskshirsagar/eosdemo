<?php

class CCorporateComplaint extends CBaseCorporateComplaint {

	protected $m_strCorporateComplaintTypeNames;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPhoneNumber;
	protected $m_strOfficeNumber;
	protected $m_strMobileNumber;
	protected $m_strEmailAddress;
	protected $m_strUnitNumber;
	protected $m_strContactDateTime;
	protected $m_strPropertyName;
	protected $m_intCustomerId;
	protected $m_intPriority;
	protected $m_intCorporateCareRequestFormType;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['corporate_complaint_type_names'] ) )	$this->setCorporateComplaintTypeNames( $arrmixValues['corporate_complaint_type_names'] );
		if( isset( $arrmixValues['name_first'] ) )						$this->setNameFirst( $arrmixValues['name_first'] );
		if( isset( $arrmixValues['name_last'] ) )						$this->setNameLast( $arrmixValues['name_last'] );
		if( isset( $arrmixValues['phone_number'] ) )					$this->setPhoneNumber( $arrmixValues['phone_number'] );
		if( isset( $arrmixValues['office_number'] ) )					$this->setOfficeNumber( $arrmixValues['office_number'] );
		if( isset( $arrmixValues['mobile_number'] ) )					$this->setMobileNumber( $arrmixValues['mobile_number'] );
		if( isset( $arrmixValues['email_address'] ) )					$this->setEmailAddress( $arrmixValues['email_address'] );
		if( isset( $arrmixValues['customer_id'] ) )						$this->setCustomerId( $arrmixValues['customer_id'] );
		if( isset( $arrmixValues['unit_number'] ) )						$this->setUnitNumber( $arrmixValues['unit_number'] );
		if( isset( $arrmixValues['contact_datetime'] ) )				$this->setContactDateTime( $arrmixValues['contact_datetime'] );
		if( isset( $arrmixValues['property_name'] ) )					$this->setPropertyName( $arrmixValues['property_name'] );
		if( isset( $arrmixValues['priority'] ) )						$this->setPriority( $arrmixValues['priority'] );
		return true;
	}

	public function setCorporateComplaintTypeNames( $strCorporateComplaintTypeNames ) {
		$this->m_strCorporateComplaintTypeNames = $strCorporateComplaintTypeNames;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setOfficeNumber( $strOfficeNumber ) {
		$this->m_strOfficeNumber = $strOfficeNumber;
	}

	public function setMobileNumber( $strMobileNumber ) {
		$this->m_strMobileNumber = $strMobileNumber;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->m_strUnitNumber = $strUnitNumber;
	}

	public function setContactDateTime( $strContactDateTime ) {
		$this->m_strContactDateTime = $strContactDateTime;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setPriority( $intPriority ) {
		$this->m_intPriority = $intPriority;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setCorporateCareRequestFormType( $intCorporateCareRequestFormType ) {
		$this->m_intCorporateCareRequestFormType = $intCorporateCareRequestFormType;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getCorporateComplaintTypeNames() {
		return $this->m_strCorporateComplaintTypeNames;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getOfficeNumber() {
		return $this->m_strOfficeNumber;
	}

	public function getMobileNumber() {
		return $this->m_strMobileNumber;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function getContactDateTime() {
		return $this->m_strContactDateTime;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getCorporateCareRequestFormType() {
		return $this->m_intCorporateCareRequestFormType;
	}

	public  function getPrimaryPhoneNumber() {
		if( true == valStr( $this->m_strPhoneNumber ) ) {
			return $this->m_strPhoneNumber;
		}

		if( true == valStr( $this->m_strOfficeNumber ) ) {
			return $this->m_strOfficeNumber;
		}

		return $this->m_strMobileNumber;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorporateComplaintTypeIds() {
		$boolIsValid = true;
		if( false == valArr( $this->getCorporateComplaintTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'corporate_complaint_type_ids', __( 'Corporate complaint types are required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valContactSubmissionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCorporateComplaintId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;
		if( false == valId( $this->getCompanyEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', __( 'Company employee is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valMaintenanceRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBusinessName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valServiceName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResolvedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResolvedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssignedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssignedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_corporate_complaint_insert':
				$boolIsValid &= $this->valCorporateComplaintTypeIds();
				break;

			case 'validate_corporate_complaint_update':
				$boolIsValid &= $this->valCompanyEmployeeId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>