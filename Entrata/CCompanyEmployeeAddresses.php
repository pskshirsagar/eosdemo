<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyEmployeeAddresses
 * Do not add any new functions to this class.
 */

class CCompanyEmployeeAddresses extends CBaseCompanyEmployeeAddresses {

	public static function fetchCompanyEmployeeAddressByCompanyEmployeeIdByAddressTypeIdByCid( $intCompanyEmployeeId, $intAddressTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM
						company_employee_addresses
					WHERE
						company_employee_id = ' . ( int ) $intCompanyEmployeeId . '
						AND cid = ' . ( int ) $intCid . '
						AND address_type_id = ' . ( int ) $intAddressTypeId . '
					LIMIT 1';

		return self::fetchCompanyEmployeeAddress( $strSql, $objDatabase );
	}

}

?>