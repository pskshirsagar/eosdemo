<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobStatuses
 * Do not add any new functions to this class.
 */

class CJobStatuses extends CBaseJobStatuses {

	public static function fetchAllJobStatuses( $objDatabase ) {

		$strSql = 'SELECT * FROM job_statuses WHERE is_published = 1 ORDER BY order_num';

		return self::fetchJobStatuses( $strSql, $objDatabase );
	}

	public static function fetchJobStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CJobStatus', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchJobStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CJobStatus', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>