<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyRentLimits
 * Do not add any new functions to this class.
 */

class CSubsidyRentLimits extends CBaseSubsidyRentLimits {

	public static function fetchSubsidyRentLimitsBySubsidyRentLimitVersionIdBySubsidyAreaIdBySubsidyContractTypeIdByPropertyIdByCid( $intSubsidyRentLimitVersionId, $intSubsidyAreaId, $arrintSubsidyContractTypeIds, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intSubsidyRentLimitVersionId ) || false == valId( $intSubsidyAreaId ) || false == valArr( $arrintSubsidyContractTypeIds ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						srl.*
					FROM
						subsidy_rent_limits srl
					WHERE
						srl.cid = ' . ( int ) $intCid . '
						AND srl.property_id = ' . ( int ) $intPropertyId . '
						AND srl.subsidy_rent_limit_version_id = ' . ( int ) $intSubsidyRentLimitVersionId . '
						AND srl.subsidy_area_id = ' . ( int ) $intSubsidyAreaId . '
						AND srl.subsidy_contract_type_id IN ( ' . implode( ',', $arrintSubsidyContractTypeIds ) . ' ) ';

		return self::fetchSubsidyRentLimits( $strSql, $objDatabase );
	}

	public static function fetchSubsidyRentLimitsBySubsidyContractTypeIdBySubsidyRentLimitVersionIdByBedroomCountByCidByPropertyId( $intSubsidyContractTypeId, $intSubsidyRentLimitVersionId, $intSubsidyRentAreaId, $intBedroomCount, $intCid, $intPropertyId, $objDatabase ) {

		if( false == valId( $intSubsidyContractTypeId ) || false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intSubsidyRentLimitVersionId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						srl.*
					FROM
						subsidy_rent_limits srl
					WHERE
						srl.cid = ' . ( int ) $intCid . '
						AND srl.property_id = ' . ( int ) $intPropertyId . '
						AND srl.subsidy_contract_type_id = ' . ( int ) $intSubsidyContractTypeId . '
						AND srl.subsidy_rent_limit_version_id = ' . ( int ) $intSubsidyRentLimitVersionId . '
						AND srl.subsidy_area_id = ' . ( int ) $intSubsidyRentAreaId . '
						AND srl.bedroom_count = ' . ( int ) $intBedroomCount . ' ';

		return self::fetchSubsidyRentLimit( $strSql, $objDatabase );

	}

}
?>