<?php

class CJobType extends CBaseJobType {

	const JOB_TYPE_TEMPLATE = 1;
	const JOB_TYPE_STANDARD_JOB = 2;
	const JOB_TYPE_ROLLING_RENOVATION = 3;

	protected $m_strGlAccountName;
	protected $m_strFormattedAccountNumber;

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues );

		if( isset( $arrValues['gl_account_name'] ) && $boolDirectSet )
			$this->m_strGlAccountName = trim( $arrValues['gl_account_name'] );
		elseif( isset( $arrValues['gl_account_name'] ) )
			$this->setGlAccountName( $arrValues['gl_account_name'] );

		if( isset( $arrValues['formatted_account_number'] ) && $boolDirectSet )
			$this->m_strFormattedAccountNumber = trim( $arrValues['formatted_account_number'] );
		elseif( isset( $arrValues['formatted_account_number'] ) )
			$this->setFormattedAccountNumber( $arrValues['formatted_account_number'] );

	}

	public function getGlAccountName() {
		return $this->m_strGlAccountName;
	}

	public function setGlAccountName( $strGlAccountName ) {
		$this->m_strGlAccountName = $strGlAccountName;
	}

	public function getFormattedAccountNumber() {
		return $this->m_strFormattedAccountNumber;
	}

	public function setFormattedAccountNumber( $strFormattedAccountNumber ) {
		$this->m_strFormattedAccountNumber = $strFormattedAccountNumber;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( '&nbsp;cid does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( '&nbsp;Name is required.' ) ) );
		} elseif( false == preg_match( '/^[A-Za-z \d\:\-\.\_\@\$\!\(\)\[\}\^\&\]\{\/\*\#\%\=\+]+$/i', trim( $this->getName() ) ) || false == mb_check_encoding( $this->getName(), 'UTF-8' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Invalid job type name (It contains special symbol).' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getJobTypes(){
		return [
			self::JOB_TYPE_STANDARD_JOB => __( 'Standard Job' ),
			self::JOB_TYPE_ROLLING_RENOVATION => __( 'Rolling Renovation' )
		];
	}

}
?>