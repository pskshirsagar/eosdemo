<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyHapRequestArTransactions
 * Do not add any new functions to this class.
 */
class CSubsidyHapRequestArTransactions extends CBaseSubsidyHapRequestArTransactions {

	public static function fetchSubsidyHapRequestArTransactionsBySubsidyHapRequestIdByPropertyIdByStatusTypeIdByCid( $intSubsidyHapRequestId, $intPropertyId, $intSubsidyHapRequestStatusTypeId, $intCid, $objDatabase ) {

		$arrintDefaultArCodeIds = [ CDefaultArCode::HUDOVERPMT, CDefaultArCode::SUBSIDY_RENT, CDefaultArCode::HUD_COLLECTION_FEES ];

		$strSql = 'SELECT
						sub.*,
						SUM( sub.hud_approved_amount ) OVER () as total_payment_amount
					FROM(
							SELECT
								shrat.cid,
								shrat.lease_id,
								l.primary_customer_id,
								SUM( shrat.hud_approved_amount ) as hud_approved_amount
							FROM
								subsidy_hap_request_ar_transactions shrat
								JOIN subsidy_hap_requests shr ON ( shrat.cid = shr.cid AND shrat.property_id = shr.property_id AND shrat.subsidy_hap_request_id = shr.id )
								JOIN leases l ON ( shrat.cid = l.cid AND shrat.lease_id = l.id AND l.occupancy_type_id = ' . COccupancyType::AFFORDABLE . ' )
								JOIN ar_transactions at ON ( shrat.cid = at.cid AND shrat.ar_transaction_id = at.id )
								JOIN ar_codes ac ON ( ac.cid = at.cid AND ac.id = at.ar_code_id AND ac.default_ar_code_id IN( ' . implode( ',', $arrintDefaultArCodeIds ) . ' ) )
							WHERE
								shrat.cid = ' . ( int ) $intCid . '
								AND shrat.subsidy_hap_request_id = ' . ( int ) $intSubsidyHapRequestId . '
								AND shrat.property_id = ' . ( int ) $intPropertyId . '
								AND shr.subsidy_hap_request_status_type_id = ' . ( int ) $intSubsidyHapRequestStatusTypeId . '
								AND shrat.hud_approved_amount <> 0
							GROUP BY
								shrat.cid,
								shrat.lease_id,
								l.primary_customer_id
						) sub';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMAT30Section3AssistancePaymentDetailRecordsBySubsidyHapRequestIdByPropertyIdByCid( $intSubsidyHapRequestId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = '
					WITH old_hap_transactions AS (
						SELECT
							at.id AS old_transaction_id,
							at.transaction_amount,
							at.lease_id,
							at.cid,
							at.property_id,
							at.ar_code_id,
							shrat.subsidy_hap_request_id
						FROM
							ar_transactions at
							JOIN subsidy_hap_request_ar_transactions shrat ON ( shrat.cid = at.cid AND shrat.property_id = at.property_id AND shrat.lease_id = at.lease_id AND shrat.ar_transaction_id = at.id)
						WHERE
							at.cid = ' . ( int ) $intCid . '
							AND at.property_id = ' . ( int ) $intPropertyId . '
							AND shrat.subsidy_hap_request_id IN ( SELECT shr.previous_subsidy_hap_request_id FROM subsidy_hap_requests shr WHERE shr.cid = ' . ( int ) $intCid . ' AND shr.id = ' . ( int ) $intSubsidyHapRequestId . ' )
							AND shrat.is_adjustment = FALSE
					)
					SELECT
						sc.id AS subsidy_certification_id,
						sc.subsidy_certification_type_id,
						shrat.lease_id AS lease_id,
						MAX ( at.transaction_amount ) AS subsidy_rent,
						shrat.ar_transaction_id AS transaction_subsidy_rent_id,
						cl.name_last AS last_name,
						cl.name_first AS first_name,
						cl.primary_customer_id AS customer_id,
						SUBSTRING ( cl.name_middle, 1, 1 ) AS middle_initial,
						cl.name_full AS head_of_household,
						pu.number_of_bedrooms || \' Bed(s)\' AS unit_size,
						( sc.effective_through_date + INTERVAL \'1 day \' ) AS recertification_date,
						sct.name || \'-\' || sc.effective_date AS turnover_info,
						CASE
							WHEN
								at.transaction_amount <> oht.transaction_amount
							THEN
								sct.name
							ELSE
								NULL
						END AS change_reason,
						sc.first_voucher_date,
						cl.unit_number_cache AS unit_number,
						COALESCE( pu.number_of_bedrooms, ut.number_of_bedrooms ) As bedroom_count,
						CASE 
							WHEN sct.is_partial IS TRUE
							THEN sc.mat_data ->> \'contract_rent\'
							ELSE sc.mat_data -> \'Section2\' ->> \'contract_rent\'
						END AS contract_rent,
						CASE 
							WHEN sct.is_partial IS TRUE
							THEN sc.mat_data ->> \'utility_allowance\'
							ELSE sc.mat_data -> \'Section2\' ->> \'utility_allowance\'
						END AS utility_allowance,
							CASE 
							WHEN sct.is_partial IS TRUE
							THEN sc.mat_data ->> \'gross_rent\'
							ELSE sc.mat_data -> \'Section2\' ->> \'gross_rent\'
						END AS gross_rent,
						sct.code AS certification_type,
						sc.effective_date AS cert_effective_date,
						CASE 
							WHEN sc.subsidy_certification_type_id IN ( ' . implode( ',', \CSubsidyCertificationType::$c_arrintPartialSubsidyCertificationTypeIds ) . ' )
								THEN 
									CASE 
										WHEN sc.mat_data ->>\'correction_type\' IS NOT NULL
											THEN \'Y\'
											ELSE NULL
										END
									ELSE
										CASE
											WHEN sc.mat_data -> \'Section2\' ->>\'correction_type_code\' IS NOT NULL
												THEN \'Y\'
												ELSE NULL
											END
									END AS correction_flag,
						MAX ( at.transaction_amount ) AS requested_amount,
						shrat.hud_approved_amount AS paid_amount,
						NULL AS oa_vendor_data
					FROM
						subsidy_hap_request_ar_transactions shrat
						JOIN subsidy_hap_requests shr ON ( shr.cid = shrat.cid AND shr.property_id = shrat.property_id AND shr.id = shrat.subsidy_hap_request_id )
						JOIN cached_leases cl ON ( cl.cid = shrat.cid AND cl.id = shrat.lease_id AND cl.property_id = shrat.property_id )
						JOIN subsidy_certifications sc ON( sc.cid = shrat.cid AND sc.id = shrat.subsidy_certification_id )
						JOIN subsidy_certification_types sct ON( sct.id = sc.subsidy_certification_type_id )
						JOIN unit_spaces AS us ON ( us.cid = cl.cid AND us.property_id = cl.property_id  AND us.id = cl.unit_space_id )
						JOIN unit_types AS ut ON ( ut.cid = cl.cid AND us.property_id = us.property_id AND ut.id = us.unit_type_id )
						LEFT JOIN property_units AS pu ON ( pu.cid = cl.cid AND pu.property_id = cl.property_id AND cl.property_unit_id = pu.id )
						JOIN ar_transactions at ON( at.cid = shrat.cid AND at.id = shrat.ar_transaction_id AND at.transaction_amount > 0 AND at.dependent_ar_transaction_id IS NULL )
						JOIN ar_codes ac ON ( ac.cid = at.cid AND ac.id = at.ar_code_id AND ac.is_disabled = FALSE )
						JOIN subsidy_contracts sc1 ON ( sc1.cid = sc.cid AND sc1.id = sc.subsidy_contract_id )
						LEFT JOIN old_hap_transactions oht ON ( oht.cid = at.cid AND oht.lease_id = at.lease_id AND oht.property_id = at.property_id AND oht.ar_code_id = at.ar_code_id )
					WHERE
						shrat.subsidy_hap_request_id = ' . ( int ) $intSubsidyHapRequestId . '
						AND shrat.cid = ' . ( int ) $intCid . '
						AND shrat.property_id = ' . ( int ) $intPropertyId . '
						AND shrat.is_adjustment = FALSE
						AND shrat.excluded_by IS NULL
					GROUP BY
						sc.id,
						sc.subsidy_certification_type_id,
						sc.mat_data,
						shrat.lease_id,
						shrat.ar_transaction_id ,
						cl.name_last,
						cl.name_first,
						cl.name_middle,
						cl.name_full,
						sct.name,
						sc.effective_date,
						sc.has_been_corrected,
						shrat.hud_approved_amount,
						cl.unit_number_cache,
						sc.effective_through_date,
						sc.first_voucher_date,
						sct.code,
						sct.is_partial,
						at.transaction_amount,
						oht.old_transaction_id,
						oht.transaction_amount,
						pu.number_of_bedrooms,
						ut.number_of_bedrooms,
						cl.display_number,
						cl.primary_customer_id
					ORDER BY
						cl.display_number';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchMAT30Section4AdjustmentHapArTransactionsBySubsidyHapRequestIdByPropertyIdByCid( $intSubsidyHapRequestId, $intPropertyId, $intCid, $objDatabase, $boolIsAdjustmentsSummarised = true ) {

		if( false == valId( $intSubsidyHapRequestId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSummarisedAdjustmentRows = ( true == $boolIsAdjustmentsSummarised )? '
						lease_id,
						array_to_string( array_agg( transaction_id ), \',\' ) AS transaction_ids,
						SUM( hud_approved_amount )AS hud_approved_amount,
						MAX( head_of_household_name ) AS head_of_household_name,
						MAX( last_name ) AS last_name,
						MAX( first_name ) AS first_name,
						MAX( middle_initial ) AS middle_initial,
						MAX( unit )AS unit,
						MAX( customer_id ) AS customer_id,
						MAX( adjustment_reason )AS adjustment_reason,
						MAX( certification_type )AS certification_type,
						subsidy_certification_id,
						MAX( effective_date ) AS effective_date,
						MAX( first_voucher_date ) AS first_voucher_date,
						is_prior_billing,
						CASE WHEN is_prior_billing IS FALSE THEN TRUE ELSE FALSE END AS is_new_cert,
						MIN( beginning_date ) AS beginning_date,
						CASE
							WHEN Max( end_date )::date IS NOT NULL THEN ROUND( sum( requested_amount ) /( ( Max( end_date )::date ) - MIN( beginning_date ) + 1 ), 2 )
							ELSE ROUND( sum( requested_amount ) /( Max( ending_date ) - MIN( beginning_date ) + 1 ), 2 )
						END AS daily_rate,
						CASE
							WHEN DATE_PART( \'day\', MIN( beginning_date ) ) <> 1 
							THEN CASE
									WHEN Max( end_date )::date IS NOT NULL THEN ROUND( sum( requested_amount ) /( ( Max( end_date )::date ) - MIN( beginning_date ) + 1 ), 2 )
									ELSE ROUND( sum( requested_amount ) /( Max( ending_date ) - MIN( beginning_date ) + 1 ), 2 )
								END
							ELSE NULL
						END AS daily_rate_pdf,
						MAX( CASE WHEN memo_ending_date IS NOT NULL THEN memo_ending_date::date
									ELSE ending_date
							END ) AS ending_date,
						CASE WHEN Max( end_date )::date  IS NOT NULL THEN Max( end_date )::date - MIN( start_date )::date + 1
							ELSE Max(ending_date) - MIN( beginning_date ) + 1
						END AS number_of_days,
						SUM( beginning_number_of_days )AS beginning_number_of_days,
						ROUND( (SUM( beginning_daily_rate ) / count(beginning_daily_rate) ),2 )AS beginning_daily_rate,
						CASE
							WHEN Max( end_date )::date  IS  NULL 
							THEN
								(DATE_PART(\'year\', Max(ending_date )+1) - DATE_PART(\'year\', MIN( beginning_date ) )) * 12 +(DATE_PART(\'month\', Max(ending_date )+1) - DATE_PART(\'month\', MIN( beginning_date )+2))
							ELSE 
								(DATE_PART(\'year\', Max( end_date )::date +1) - DATE_PART(\'year\', MIN( beginning_date )+2)) * 12 +(DATE_PART(\'month\', Max( end_date )::date +1) - DATE_PART(\'month\', MIN( beginning_date )+2))
							END AS number_of_months,
							avg( requested_amount )AS monthly_rate,
						SUM( CASE WHEN ending_date <> get_last_day_of_month( ending_date ) THEN ending_number_of_days ELSE NULL END ) AS ending_number_of_days,
						ROUND( ( CASE WHEN Max( ending_date ) <> Max( get_last_day_of_month( ending_date ) ) AND count(ending_daily_rate) <> 0 THEN SUM( ending_daily_rate ) / count(ending_daily_rate) ELSE NULL END ),2 )AS beginning_daily_rate,
						SUM( requested_amount ) AS assistance_payment,
						MAX( subsidy_rent )AS subsidy_rent,
						SUM( requested_amount )AS requested_amount
						FROM hap_adjustments
						GROUP BY
						lease_id,
						subsidy_certification_id,
						is_prior_billing
						' : ' * FROM hap_adjustments ';

		$strSql = 'WITH hap_adjustments AS( SELECT
						DISTINCT ON ( shrat.ar_transaction_id ) shrat.ar_transaction_id AS transaction_id,
						shrat.lease_id,
						shrat.hud_approved_amount,
						CASE
							WHEN
								cl.name_middle <> \'\'
							THEN
								CONCAT( cl.name_last, \', \', cl.name_first, \',\', SUBSTRING ( cl.name_middle, 1, 1 ) )
							ELSE
								CONCAT( cl.name_last, \', \', cl.name_first )
						END AS head_of_household_name,
						cl.name_last  AS last_name,
						cl.name_first AS first_name,
						SUBSTRING ( cl.name_middle, 1, 1 ) AS middle_initial,
						cl.unit_number_cache AS unit,
						cl.primary_customer_id AS customer_id,
						sct.name AS adjustment_reason,
						sct.code AS certification_type,
						sc.id AS subsidy_certification_id,
						sc.effective_date AS effective_date,
						sc.first_voucher_date,
						sc.has_been_corrected AS is_correction_cert,
						MIN( art.post_date ) AS beginning_date,
						art.is_reversal AS is_prior_billing,
						ROUND( ( art.transaction_amount / ( get_last_day_of_month( MAX( art.post_date ) ) - MIN( art.post_date ) + 1  ) ), 2 ) As daily_rate,
						get_last_day_of_month( MAX( art.post_date ) ) AS ending_date,
						CASE
							WHEN MAX( util.util_to_date( art.details->\'memo_data\'->>\'end_date\' ) ) IS NOT NULL THEN MAX( util.util_to_date( art.details->\'memo_data\'->>\'end_date\' ) )
							ELSE get_last_day_of_month( MAX( art.post_date ) )
						END AS memo_ending_date,
						get_last_day_of_month( MAX( art.post_date ) ) - MIN( art.post_date ) + 1 AS number_of_days,
						CASE
							WHEN
								DATE_PART( \'day\', MIN( art.post_date ) ) <> 1
								THEN ( DATE_TRUNC( \'month\', MIN( art.post_date ) ) + interval \'1 month\' ) - DATE_TRUNC( \'day\', MIN( art.post_date ) )
							ELSE NULL
						END AS beginning_number_of_days,
						CASE
							WHEN
								DATE_PART( \'day\', MIN( art.post_date ) ) <> 1
								THEN ROUND( ( scharge.charge_amount / get_days_in_month( MIN( art.post_date ) ) ), 2 )
							ELSE NULL
						END  AS beginning_daily_rate,
						CASE
							WHEN
								DATE_PART( \'day\', art.post_date ) = 1
								AND get_last_day_of_month( art.post_date ) <= scharge.charge_end_date
							THEN 1
						ELSE 0
						END	AS number_of_months,
							CASE 
								WHEN 
								art.scheduled_charge_id IS NOT NULL
								THEN
									scharge.charge_amount 
								ELSE
                                art.transaction_amount
                                END AS monthly_rate,
						CASE
							WHEN
								DATE_TRUNC( \'month\', MIN( art.post_date ) ) <> DATE_TRUNC( \'month\', scharge.charge_end_date )
								AND get_last_day_of_month( MAX( art.post_date ) ) > scharge.charge_end_date
							THEN
								DATE_PART( \'day\', scharge.charge_end_date )
							ELSE NULL
						END AS ending_number_of_days,
						art.details::json->>\'memo_data\' AS memo_data,
						MAX( util.util_to_date( art.details->\'memo_data\'->>\'end_date\' ) ) AS end_date,
						MIN( util.util_to_date( art.details->\'memo_data\'->>\'start_date\' ) ) AS start_date,
						CASE
							WHEN
								DATE_TRUNC( \'month\', MIN( art.post_date ) ) <> DATE_TRUNC( \'month\', scharge.charge_end_date )
								AND get_last_day_of_month( MAX( art.post_date ) ) > scharge.charge_end_date
                            THEN ROUND( ( art.transaction_amount / get_days_in_month( scharge.charge_end_date ) ), 2 )
                            ELSE NULL
						END AS ending_daily_rate,
						CASE WHEN sc.subsidy_certification_type_id IN ( ' . implode( ',', \CSubsidyCertificationType::$c_arrintFullSubsidyCertificationTypeIds ) . ' ) 
							THEN util.util_to_numeric( sc.mat_data->\'Section2\'->>\'assistance_payment_amount\' )
							ELSE art.transaction_amount
						END AS assistance_payment,
						CASE WHEN sc.subsidy_certification_type_id IN ( ' . implode( ',', \CSubsidyCertificationType::$c_arrintPartialSubsidyCertificationTypeIds ) . ' ) THEN util.util_to_numeric( sc.mat_data->>\'subsidy_rent\' )
							ELSE util.util_to_numeric( sc.mat_data->\'Section2\'->>\'subsidy_rent\' )
						END AS subsidy_rent,
						SUM( art.transaction_amount ) AS requested_amount
						FROM
							subsidy_hap_request_ar_transactions shrat
							JOIN ar_transactions art ON ( shrat.cid = art.cid AND shrat.lease_id = art.lease_id AND shrat.property_id = art.property_id AND shrat.ar_transaction_id = art.id )
							LEFT JOIN scheduled_charges AS scharge ON ( art.ar_code_id = scharge.ar_code_id AND scharge.id = art.scheduled_charge_id  )
							LEFT JOIN subsidy_certification_scheduled_charges AS scsc ON ( scsc.cid = scharge.cid AND scsc.lease_id = scharge.lease_id AND scsc.scheduled_charge_id = scharge.id)
							JOIN subsidy_certifications AS sc ON ((sc.cid = scsc.cid AND sc.lease_id = scsc.lease_id AND sc.first_voucher_date IS NOT NULL AND sc.id = scsc.subsidy_certification_id AND sc.deleted_by IS NULL AND sc.deleted_on IS NULL AND sc.subsidy_certification_status_type_id >= ' . CSubsidyCertificationStatusType::FINALIZED . ' AND scsc.id IS NOT NULL ) OR ( art.cid = sc.cid AND art.lease_id = sc.lease_id and art.post_date BETWEEN sc.effective_date AND sc.effective_through_date AND scsc.id IS NULL))
							JOIN subsidy_certification_types AS sct ON ( sct.id = sc.subsidy_certification_type_id )
							JOIN cached_leases cl ON ( cl.cid = shrat.cid AND cl.id = shrat.lease_id AND cl.property_id = shrat.property_id )
						WHERE
							shrat.cid = ' . ( int ) $intCid . '
							AND shrat.property_id =  ' . ( int ) $intPropertyId . '
							AND shrat.is_adjustment IS TRUE
							AND shrat.subsidy_hap_request_id = ' . ( int ) $intSubsidyHapRequestId . '
							AND shrat.excluded_by IS NULL
						GROUP BY
							shrat.lease_id,
							shrat.ar_transaction_id,
							shrat.hud_approved_amount,
							art.scheduled_charge_id,
							cl.name_middle,
							cl.name_last,
							cl.name_first,
							sct.name,
							sct.code,
							sc.mat_data,
							sc.has_been_corrected,
							sc.effective_date,
							sc.id,
							scharge.charge_end_date,
							scharge.charge_amount,
							art.post_date,
							art.is_reversal,
							sc.first_voucher_date,
							cl.unit_number_cache,
							cl.display_number,
							cl.primary_customer_id,
							art.transaction_amount,
							memo_data,
							scsc.id,
							art.scheduled_charge_id,
							sc.subsidy_certification_type_id
						ORDER BY
						shrat.ar_transaction_id,
						cl.display_number )
						
					SELECT
						' . $strSummarisedAdjustmentRows . '
					ORDER BY
					unit,
						is_prior_billing DESC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTotalSubsidyHapRequestUnitCountBySubsidyHapRequestIdByPropertyIdByCid( $intSubsidyHapRequestId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intSubsidyHapRequestId ) ) return NULL;

		$strSql = 'SELECT
						COUNT(
							DISTINCT CASE
								WHEN
									shrat.is_adjustment = TRUE
								THEN
									shrat.lease_id
							END
						) AS total_adjustment_count,
						COUNT(
							DISTINCT CASE
								WHEN
									shrat.is_adjustment = FALSE
								THEN
									shrat.lease_id
							END
						) AS total_assistance_count
						FROM
							subsidy_hap_request_ar_transactions shrat
						WHERE
							shrat.cid = ' . ( int ) $intCid . '
							AND shrat.property_id = ' . ( int ) $intPropertyId . '
							AND shrat.subsidy_hap_request_id = ' . ( int ) $intSubsidyHapRequestId . '
							AND shrat.excluded_by IS NULL ';

		$arrmixData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixData ) ) ? $arrmixData[0] : [];
	}

	public static function fetchMAT30Section7RepaymentAgreementsBySubsidyHapRequestIdByPropertyIdByCid( $intSubsidyHapRequestId, $intPropertyId, $intCid, $objDatabase, $boolIsTRACS = false ) {

		if( false == valId( $intSubsidyHapRequestId ) || false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSelect              = ( true == $boolIsTRACS ) ? 'shrat.ar_transaction_id,' : '';
		$strOuterSelect         = ( true == $boolIsTRACS ) ? ', CASE WHEN repayment.total_payment < 0 THEN repayment.total_payment * -1 ELSE repayment.total_payment END AS repayment_total_payment' : '';
		$strGroupBy             = ( true == $boolIsTRACS ) ? 'shrat.ar_transaction_id,' : '';
		$strRepaymentGroupBy    = ( true == $boolIsTRACS ) ? 'ar_transaction_id,' : '';

		$strSql = '
					SELECT
						repayment_agreement.*,
						COALESCE( repayment_agreement.total_payment, 0 ) - COALESCE( repayment_agreement.amount_retained, 0 ) AS amount_requested,
						CASE WHEN COALESCE( repayment_agreement.total_payment, 0 ) < 0 
							THEN ( COALESCE( repayment_agreement.total_payment * -1, 0 ) - COALESCE( repayment_agreement.amount_retained, 0 ) )
							ELSE COALESCE( repayment_agreement.total_payment, 0 ) - COALESCE( repayment_agreement.amount_retained, 0 )
						END AS pdf_amount_requested,
						0 AS pdf_agreement_change_amount
					FROM
						(
							SELECT
								repayment.*,
								CASE 
									WHEN repayment.repayment_agreement_change_amount < 0 THEN repayment.repayment_agreement_change_amount * -1 
									ELSE repayment.repayment_agreement_change_amount 
								END AS agreement_change_amount,
								FLOOR( CASE
											WHEN
												repayment.total_payment < 0
											THEN
												repayment.total_payment * 0.2 * -1
											ELSE
												repayment.total_payment * 0.2
										END ) AS amount_retained
								' . $strOuterSelect . '
							FROM
								(
									SELECT
										CASE
											WHEN
												cl.name_middle <> \'\'
											THEN
												CONCAT( cl.name_last, \', \', cl.name_first, \',\', SUBSTRING ( cl.name_middle, 1, 1 ) )
											ELSE
												CONCAT( cl.name_last, \', \', cl.name_first )
										END AS head_of_household,
										cl.name_last AS head_last_name,
										cl.name_first AS head_first_name,
										shrat.lease_id AS lease_id,
										' . $strSelect . '
										cl.unit_number_cache AS unit_number,
										COALESCE( r.external_id, r.id ) AS agreement_id,
										r.id as repayment_id,
										to_char( r.created_on, \'MM/DD/YY\') AS agreement_date,
										CASE
											WHEN
												r.is_resident = TRUE
											THEN
												\'T\'
											ELSE
												\'N\'
										END AS agreement_type,
										r.agreement_amount_total AS agreement_amount,
										r.current_balance AS ending_balance,
										sc.id AS subsidy_certification_id,
										shrat.hud_approved_amount,
										cl.primary_customer_id AS customer_id,
										SUM( at.transaction_amount ) AS repayment_agreement_change_amount,
										SUM( CASE WHEN ac.ar_code_type_id = ' . CArCodeType::OTHER_LIABILITY . ' 
												THEN CASE WHEN at.transaction_amount < 0 THEN at.transaction_amount * -1 ELSE at.transaction_amount END
												ELSE 0 
											END ) AS total_payment
									FROM
										subsidy_hap_request_ar_transactions shrat
										JOIN cached_leases cl ON ( cl.id = shrat.lease_id AND cl.cid = shrat.cid )
										JOIN repayments r ON ( r.cid = shrat.cid AND r.lease_id = shrat.lease_id AND r.property_id = shrat.property_id )
										JOIN repayment_ar_transactions rat ON ( rat.repayment_id = r.id AND rat.cid = r.cid AND r.property_id = rat.property_id )
										JOIN ar_codes ac ON ( ac.cid = shrat.cid AND ac.is_disabled = FALSE )
										JOIN ar_transactions at ON( at.cid = shrat.cid AND at.property_id = shrat.property_id AND at.lease_id = shrat.lease_id AND at.id = shrat.ar_transaction_id AND ac.id = at.ar_code_id AND at.dependent_ar_transaction_id = rat.ar_transaction_id )
										JOIN subsidy_certifications sc ON( sc.cid = shrat.cid AND sc.property_id = shrat.property_id AND sc.lease_id = shrat.lease_id AND sc.id = shrat.subsidy_certification_id AND sc.repayment_id = r.id )
									WHERE
										shrat.subsidy_hap_request_id = ' . ( int ) $intSubsidyHapRequestId . '
										AND shrat.cid = ' . ( int ) $intCid . '
										AND shrat.property_id = ' . ( int ) $intPropertyId . '
										AND shrat.is_adjustment = FALSE
									GROUP BY
										sc.mat_data,
										r.external_id,
										' . $strGroupBy . '
										r.id,
										r.is_resident,
										r.created_on,
										r.agreement_amount_total,
										r.current_balance,
										sc.id,
										shrat.lease_id,
										cl.name_middle,
										cl.name_last,
										cl.name_first,
										cl.name_middle,
										cl.unit_number_cache,
										cl.primary_customer_id,
										shrat.hud_approved_amount
									ORDER BY
										r.id,
										shrat.lease_id
							)AS repayment
								GROUP BY
									head_of_household,
									head_last_name,
									head_first_name,
									unit_number,
									agreement_type,
									lease_id,
									agreement_id,
									repayment_id,
									' . $strRepaymentGroupBy . '
									agreement_date,
									agreement_amount,
									ending_balance,
									subsidy_certification_id,
									hud_approved_amount,
									amount_retained,
									total_payment,
									repayment.repayment_agreement_change_amount,
									customer_id
								ORDER BY
									repayment_id,
									lease_id	
						)AS repayment_agreement	';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
