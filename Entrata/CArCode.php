<?php

class CArCode extends CBaseArCode {

	protected $m_intIsView;
	protected $m_intPropertyId;
	protected $m_intGlAccountTypeId;
	protected $m_intPropertyArCodeId;
	protected $m_intDefaultLedgerFilterId;
	protected $m_intCachedArCodePeriodeCid;
	protected $m_boolShowMergedName;
	protected $m_boolSetDefaultAmount;

	protected $m_strKeyValuePair;
	protected $m_strArTriggerName;
	protected $m_strTaxArCodeName;
	protected $m_strArCodeTypeName;
	protected $m_strLedgerFilterName;
	protected $m_strGlAccountTypeName;
	protected $m_strWriteOffCodeName;
	protected $m_strArCodeDescription;
	protected $m_strDebitGLAccountName;
	protected $m_strCreditGLAccountName;
	protected $m_strDebitGlAccountNumber;
	protected $m_strArCodeSummaryTypeName;
	protected $m_strCreditGlAccountNumber;

	protected $m_fltTaxPercent;

	/** @var \Psi\Core\AccountsReceivables\Collections\CArCodeAllocationRulesCollection $m_objArCodeAllocationRulesCollection*/
	protected $m_objArCodeAllocationRulesCollection;

	const USE_PROPERTY_AR_CODE_ID = 'use_property_ar_code_id';

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['gl_account_type_name'] ) ) $this->setGlAccountTypeName( $arrmixValues['gl_account_type_name'] );
		if( true == isset( $arrmixValues['property_ar_code_id'] ) ) $this->setPropertyArCodeId( $arrmixValues['property_ar_code_id'] );
		if( true == isset( $arrmixValues['ar_code_type_name'] ) ) $this->setArCodeTypeName( $arrmixValues['ar_code_type_name'] );
		if( true == isset( $arrmixValues['ledger_filter_name'] ) ) $this->setLedgerFilterName( $arrmixValues['ledger_filter_name'] );
		if( true == isset( $arrmixValues['credit_gl_account_name'] ) ) $this->setCreditGLAccountName( $arrmixValues['credit_gl_account_name'] );
		if( true == isset( $arrmixValues['debit_gl_account_name'] ) ) $this->setDebitGlAccountName( $arrmixValues['debit_gl_account_name'] );
		if( true == isset( $arrmixValues['write_off_code_name'] ) ) $this->setWriteOffCodeName( $arrmixValues['write_off_code_name'] );
		if( true == isset( $arrmixValues['tax_ar_code_name'] ) ) $this->setTaxArCodeName( $arrmixValues['tax_ar_code_name'] );
		if( true == isset( $arrmixValues['ar_code_summary_type_name'] ) ) $this->setArCodeSummaryTypeName( $arrmixValues['ar_code_summary_type_name'] );
		if( true == isset( $arrmixValues['property_id'] ) ) $this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['is_view'] ) ) $this->setIsView( $arrmixValues['is_view'] );
		if( true == isset( $arrmixValues['credit_gl_account_number'] ) ) $this->setCreditGlAccountNumber( $arrmixValues['credit_gl_account_number'] );
		if( true == isset( $arrmixValues['debit_gl_account_number'] ) ) $this->setDebitGlAccountNumber( $arrmixValues['debit_gl_account_number'] );
		if( true == isset( $arrmixValues['ar_trigger_name'] ) ) $this->setArTriggerName( $arrmixValues['ar_trigger_name'] );
		if( true == isset( $arrmixValues['cached_ar_code_cid'] ) ) $this->setCachedArCodePeriodCid( $arrmixValues['cached_ar_code_cid'] );
		if( true == isset( $arrmixValues['prorate_charges'] ) ) $this->setProrateCharges( $arrmixValues['prorate_charges'] );// This is for NULL value
		if( true == isset( $arrmixValues['default_ledger_filter_id'] ) ) $this->setDefaultLedgerFilterId( $arrmixValues['default_ledger_filter_id'] );
		if( true == isset( $arrmixValues['tax_percent'] ) ) $this->setTaxPercent( $arrmixValues['tax_percent'] );
		if( true == isset( $arrmixValues['show_merged_name'] ) ) $this->setShowMergedName( $arrmixValues['show_merged_name'] );
		if( true == isset( $arrmixValues['set_default_amount'] ) ) $this->setSetDefaultAmount( $arrmixValues['set_default_amount'] );

		return;
	}

	/**
	 * Get Functions
	 */

	public function getGlAccountTypeId() {
		return $this->m_intGlAccountTypeId;
	}

	public function getGlAccountTypeName() {
		return $this->m_strGlAccountTypeName;
	}

	public function getPropertyArCodeId() {
		return $this->m_intPropertyArCodeId;
	}

	public function getArCodeTypeId() {
		return $this->m_intArCodeTypeId;
	}

	public function getArCodeTypeName() {
		return $this->m_strArCodeTypeName;
	}

	public function getLedgerFilterName() {
		return $this->m_strLedgerFilterName;
	}

	public function getCreditGLAccountName() {
		return $this->m_strCreditGLAccountName;
	}

	public function getDebitGlAccountName() {
		return $this->m_strDebitGLAccountName;
	}

	public function getWriteOffCodeName() {
		return $this->m_strWriteOffCodeName;
	}

	public function getTaxArCodeName() {
		return $this->m_strTaxArCodeName;
	}

	public function getArCodeSummaryTypeName() {
		return $this->m_strArCodeSummaryTypeName;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getIsView() {
		return $this->m_intIsView;
	}

	public function getCreditGlAccountNumber() {
		return $this->m_strCreditGlAccountNumber;
	}

	public function getDebitGlAccountNumber() {
		return $this->m_strDebitGlAccountNumber;
	}

	public function getArTriggerName() {
		return $this->m_strArTriggerName;
	}

	public function getCachedArCodePeriodCid() {
		return $this->m_intCachedArCodePeriodeCid;
	}

	public function getDefaultLedgerFilterId() {
		return $this->m_intDefaultLedgerFilterId;
	}

	public function getShowMergedName() {
		return $this->m_boolShowMergedName;
	}

	public function getSetDefaultAmount() {
		return $this->m_boolSetDefaultAmount;
	}

	public function getName( $strLocaleCode = NULL ) {
		return ( true === $this->getShowMergedName() ) ? parent::getName( $strLocaleCode ) . ' [ ' . $this->getDescription( $strLocaleCode ) . ' ]' : parent::getName( $strLocaleCode );
	}

	public function getTaxPercent() {
		return ( float ) $this->m_fltTaxPercent;
	}

	public function getArCodeAllocationRulesCollection() {
		return $this->m_objArCodeAllocationRulesCollection;
	}

	public function useStandardAllocation() : bool {
		return !isset( $this->m_objArCodeAllocationRulesCollection )
			|| 0 == $this->m_objArCodeAllocationRulesCollection->count();
	}

	public function hasWhitelist() {
		return !$this->useStandardAllocation();
	}

	public function getWhitelistArCodeIds() {
		return 	$this->useStandardAllocation() ? [] : $this->getArCodeAllocationRulesCollection()->getAllowedArCodeIds();
	}

	/**
	 * Set Functions
	 */

	public function setGlAccountTypeId( $intGlAccountTypeId ) {
		$this->m_intGlAccountTypeId = $intGlAccountTypeId;
	}

	public function setGlAccountTypeName( $strGlAccountTypeName ) {
		$this->m_strGlAccountTypeName = $strGlAccountTypeName;
	}

	public function setPropertyArCodeId( $intPropertyArCodeId ) {
		$this->m_intPropertyArCodeId = ( int ) $intPropertyArCodeId;
	}

	public function setArCodeTypeName( $strArCodeTypeName ) {
		$this->m_strArCodeTypeName = ( string ) $strArCodeTypeName;
	}

	public function setLedgerFilterName( $strLedgerFilterName ) {
		$this->m_strLedgerFilterName = $strLedgerFilterName;
	}

	public function setCreditGLAccountName( $strCreditGLAccountName ) {
		$this->m_strCreditGLAccountName = $strCreditGLAccountName;
	}

	public function setDebitGlAccountName( $strDebitGLAccountName ) {
		$this->m_strDebitGLAccountName = $strDebitGLAccountName;
	}

	public function setWriteOffCodeName( $strWriteOffCodeName ) {
		$this->m_strWriteOffCodeName = ( string ) $strWriteOffCodeName;
	}

	public function setTaxArCodeName( $strTaxArCodeName ) {
		$this->m_strTaxArCodeName = ( string ) $strTaxArCodeName;
	}

	public function setArCodeSummaryTypeName( $strArCodeSummaryTypeName ) {
		$this->m_strArCodeSummaryTypeName = ( string ) $strArCodeSummaryTypeName;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	public function setIsView( $intIsView ) {
		$this->m_intIsView = $intIsView;
	}

	public function setCreditGlAccountNumber( $strCreditGlAccountNumber ) {
		$this->m_strCreditGlAccountNumber = CStrings::strTrimDef( $strCreditGlAccountNumber, 50, NULL, true );
	}

	public function setDebitGlAccountNumber( $strDebitGlAccountNumber ) {
		$this->m_strDebitGlAccountNumber = CStrings::strTrimDef( $strDebitGlAccountNumber, 50, NULL, true );
	}

	public function setArTriggerName( $strArTriggerName ) {
		$this->m_strArTriggerName = CStrings::strTrimDef( $strArTriggerName, 50, NULL, true );
	}

	public function setCachedArCodePeriodCid( $intCachedArCodePeriodCid ) {
		$this->m_intCachedArCodePeriodeCid = CStrings::strToIntDef( $intCachedArCodePeriodCid, NULL, false );
	}

	public function setProrateCharges( $boolProrateCharges ) {
		$this->m_boolProrateCharges = ( 0 < strlen( trim( $boolProrateCharges ) ) ) ? CStrings::strToBool( $boolProrateCharges ) : NULL;
	}

	public function setDefaultLedgerFilterId( $intDefaultLedgerFilterId ) {
		$this->m_intDefaultLedgerFilterId = $intDefaultLedgerFilterId;
	}

	public function setTaxPercent( $fltTaxPercent ) {
		$this->set( 'm_fltTaxPercent', CStrings::strToFloatDef( $fltTaxPercent, NULL, false, 6 ) );
	}

	public function setShowMergedName( $boolShowMergedName ) {
		$this->m_boolShowMergedName = CStrings::strToBool( $boolShowMergedName );
	}

	public function setArCodeAllocationRulesCollection( \Psi\Core\AccountsReceivables\Collections\CArCodeAllocationRulesCollection $objArCodeAllocationRulesCollection ) {
		$this->m_objArCodeAllocationRulesCollection = $objArCodeAllocationRulesCollection;
	}

	public function setSetDefaultAmount( $boolSetDefaultAmount ) {
		$this->m_boolSetDefaultAmount = CStrings::strToBool( $boolSetDefaultAmount );
	}

	/**
	 *  Validation Functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) || 0 >= ( int ) $this->getId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'id', __( 'Valid id is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) || 0 >= ( int ) $this->getCid() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'cid', __( 'Valid client id is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valGlAccountTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getGlAccountTypeId() ) || 0 >= ( int ) $this->getGlAccountTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'gl_account_type_id', __( 'Valid Gl Account Type is required.' ), NULL ) );
		}

		return $boolIsValid;
	}

	public function valCreditGlAccountId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valId( $this->getCreditGlAccountId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'credit_gl_account_id', __( 'Credit GL account is required.' ), NULL ) );
		} elseif( true == valObj( $objDatabase, 'CDatabase' ) && false == $this->getIntegratedOnly() ) {

			$arrobjGlAccounts = ( array ) CGlAccounts::fetchEligibleCreditGlAccountsByArCodeTypeIdByArOriginIdByArTriggerIdByCid( $this->getArCodeTypeId(), $this->getArOriginId(), $this->getArTriggerId(), $this->getCid(), $objDatabase, [ $this->getCreditGlAccountId() ] );

			if( false == valArr( $arrobjGlAccounts ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( NULL, 'credit_gl_account_id', __( 'The Gl account you have provided is not valid for this type of charge code as credit Gl account.' ), NULL ) );
			}
		}

		return $boolIsValid;
	}

	public function valDebitGlAccountId( $objDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valId( $this->getDebitGlAccountId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'debit_gl_account_id', __( 'Debit GL account is required.' ), NULL ) );
		} elseif( true == valObj( $objDatabase, 'CDatabase' ) && false == $this->getIntegratedOnly() ) {

			$arrobjGlAccounts = ( array ) CGlAccounts::fetchEligibleDebitGlAccountsByArCodeTypeIdByCid( $this->getArCodeTypeId(), $this->getCid(), $objDatabase, $this->getId(), [ $this->getDebitGlAccountId() ] );

			if( false == valArr( $arrobjGlAccounts ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( NULL, 'debit_gl_account_id', __( 'The Gl account you have provided is not valid for this type of charge code as debit Gl account.' ), NULL ) );
			}
		}

		return $boolIsValid;
	}

	public function valArCodeTypeId( $arrobjAllArCodes = NULL, $objDatabase = NULL ) {
		$boolIsValid = true;
		if( true == is_null( $this->getArCodeTypeId() ) || 0 >= ( int ) $this->getArCodeTypeId() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'ar_code_type_id', __( 'Ar Code Type is required.' ), NULL ) );
			$boolIsValid = false;
		}

		if( true == $boolIsValid && true == valArr( $arrobjAllArCodes ) && true == array_key_exists( $this->getId(), $arrobjAllArCodes ) && $arrobjAllArCodes[$this->getId()]->getArCodeTypeId() != $this->getArCodeTypeId() && false == $this->getIsDisabled() ) {
			$boolIsValid &= $this->valPropertyGlSettingDependencies( 'ar_code_type_id', $objDatabase );
		}
		return $boolIsValid;
	}

	public function valArCodeSummaryTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getArCodeSummaryTypeId() ) || 0 >= ( int ) $this->getArCodeSummaryTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'ar_code_summary_type_id', __( 'Ar Code Summary Type is required.' ), NULL ) );
		}
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		if( true == is_null( $this->getRemotePrimaryKey() ) || 0 >= ( int ) $this->getRemotePrimaryKey() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'remote_primary_key', __( 'Remote Primary Key is required.' ), NULL ) );
		}
		return $boolIsValid;
	}

	public function valName( $objDatabase = NULL ) {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Name is required.' ) ) );

		} elseif( true == valObj( $objDatabase, 'CDatabase' ) && false == $this->getIntegratedOnly() ) {
			$intDuplicateArCodeCount = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodeCountByIdByNameByCid( $this->getId(), $this->getName(), $this->getCid(), $objDatabase );

			if( 0 < $intDuplicateArCodeCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Duplicate charge code name.' ) ) );
			}
		}
		return $boolIsValid;
	}

	public function valLedgerFilterId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLedgerFilterId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ledger_filter_id', __( 'Associated ledger is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCaptureDelayDays() {
		$boolIsValid = true;

		if( false == is_null( $this->getArCodeTypeId() ) && false == is_null( $this->getCaptureDelayDays() ) && ( false == is_int( $this->getCaptureDelayDays() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'capture_delay_days', __( 'Capture delay days must be numeric.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;

		if( false == is_null( $this->getIsSystem() ) && 1 == $this->getIsSystem() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_system', __( 'System A/R code cannot be deleted.' ) ) );
		}

		return $boolIsValid;
	}

	public function valDefaultAmount() {
		$boolIsValid = true;

		if( ( false == $this->getDefaultAmountIsEditable() && 0 == $this->getDefaultAmount() ) || ( false == $this->getDefaultAmountIsEditable() && false == is_numeric( $this->getDefaultAmount() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_amount', __( 'If the default amount cannot be overwritten, the default amount cannot be set to $0.00' ) ) );
		} elseif( false == is_numeric( $this->getDefaultAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_amount', __( 'Please enter valid default amount.' ) ) );
		}
		return $boolIsValid;
	}

	public function valHideCharges() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHideCredits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWaiveLateFees() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDontExport() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostToCash() {
		$boolIsValid = true;

		if( false == valId( $this->getDebitGlAccountId() ) || false == valId( $this->getCreditGlAccountId() ) || true == $this->getPostToCash() ) return $boolIsValid;

		$arrintGlAccountTypeIds = [ CGlAccountType::INCOME, CGlAccountType::EXPENSES ];
		$arrintArCodeTypeIds  = [ CArCodeType::RENT, CArCodeType::OTHER_INCOME, CArCodeType::EXPENSE ];

		if( false == $this->getPostToCash() && ( false == in_array( $this->getGlAccountTypeId(), $arrintGlAccountTypeIds ) || false == in_array( $this->getArCodeTypeId(), $arrintArCodeTypeIds ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'post_to_cash', __( 'Post to cash can only be zero when ar code is of type charge and belongs to income or expense.' ) ) );
		}

		return $boolIsValid;
	}

	public function valAccelRentArCodeId() {
		$boolIsValid = true;
		if( false == is_null( $this->getAccelRentArCodeId() ) && false == valId( $this->getAccelRentArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'accel_rent_ar_code_id', __( 'Accelerated rent charge code is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valSkipAccelRentArCodeId() {
		$boolIsValid = true;
		if( false == is_null( $this->getSkipAccelRentArCodeId() ) && false == valId( $this->getSkipAccelRentArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'skip_accel_rent_ar_code_id', __( 'Skip accelerated rent charge code is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMonthToMonthRentArCodeId() {
		$boolIsValid = true;
		if( false == is_null( $this->getMonthToMonthRentArCodeId() ) && false == valId( $this->getMonthToMonthRentArCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'month_to_month_rent_ar_code_id', __( 'Month to month rent charge code is invalid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIsReserved() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriorityNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIntegratedOnly( $objDatabase, $arrintEntrataCorePropertyIds ) {
		$boolIsValid = true;
		// If charge code is not hidden from entrata core properties, then do not create database request.
		if( 0 == $this->getIntegratedOnly() || false == valArr( $arrintEntrataCorePropertyIds ) ) {
			return $boolIsValid;
		}
		// If charge code is disabled then do not create database request
		if( 1 == $this->getIsDisabled() ) {
			return $boolIsValid;
		}

		if( true == $this->getIsSystem() && true == $this->getIntegratedOnly() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integrated_only', __( 'System charge code can not be hidden from entrata core.' ) ) );
			return false;
		}

		$strErrorMessage = NULL;
		$strScJoin = ' JOIN cached_leases lp ON ( sc.lease_id = lp.id AND sc.cid = lp.cid AND sc.lease_interval_id = lp.active_lease_interval_id )';

		$strScAdditionalCondition = ' AND ( CASE WHEN lp.fmo_started_on IS NOT NULL THEN sc.posted_through_date IS NOT NULL
    									  ELSE sc.posted_through_date IS NULL
    									  END
    									  OR ( sc.charge_end_date > sc.posted_through_date ) OR
												CASE
            									WHEN
													sc.ar_trigger_id IN ( ' . implode( ',', CArTrigger::$c_arrintRecurringArTriggers ) . ' ) THEN sc.charge_end_date IS NULL
												END
											)
										  AND ( CASE WHEN lp.lease_status_type_id <> 1 THEN sc.is_unselected_quote = FALSE ELSE TRUE = TRUE END )
										  AND sc.deleted_on IS NULL';

		$arrstrSqls[] = 'SELECT
							\'Scheduled Charges\'    single_reference,
							\'' . __( 'Scheduled Charges ' ) . '\'   multiple_reference,
							\'' . __( 'scheduled_charges ' ) . '\'   table_name,
							sc.property_id,
							COUNT( DISTINCT sc.id ) row_count
						 FROM
							scheduled_charges sc'
						. $strScJoin . '
		                JOIN leases l ON( sc.lease_id = l.id AND sc.cid = l.cid )
						JOIN property_gl_settings pgs ON ( sc.cid = pgs.cid AND pgs.property_id = sc.property_id ) 
						WHERE
							sc.cid = ' . ( int ) $this->getCid() . '
							AND sc.ar_code_id = ' . ( int ) $this->getId() . '
							AND l.property_id IN ( ' . implode( ',', ( array ) $arrintEntrataCorePropertyIds ) . ' )
							AND pgs.activate_standard_posting = true '
							. $strScAdditionalCondition . '
						GROUP BY sc.property_id';

		$arrstrSqls[] = 'SELECT
							\'Scheduled Charges\'    single_reference,
							\'' . __( 'Scheduled Charges' ) . '\'       multiple_reference,
							\'' . __( 'scheduled_charges' ) . '\'       table_name,
							null AS property_id,
							count( sub.id ) AS row_count
						FROM
                            (
								SELECT
									aar.id,
									json_array_elements_text ( aar.request_details -> \'gross_details\' -> \'ar_codes\' ) AS ar_code
								FROM
									ar_approval_requests aar
								WHERE
									aar.cid = ' . ( int ) $this->getCid() . '
									AND aar.is_archived = false
									AND aar.route_type_id = ' . ( int ) CRouteType::SCHEDULED_CHARGES . '
							) AS sub
						WHERE
							sub.ar_code = ' . '\'' . ( int ) $this->getId() . '\'' . '';

		$arrstrSqls[] = ' SELECT
						\'Rates\'    single_reference,
						\'' . __( 'Rates' ) . '\'   multiple_reference,
						\'' . __( 'rates' ) . '\'   	table_name,
						r.property_id,
						count( r.id ) AS row_count
					FROM
						rates r
						JOIN property_gl_settings pgs on ( pgs.cid = r.cid AND pgs.property_id = r.property_id )
					WHERE
						r.cid = ' . ( int ) $this->getCid() . '
						AND ( r.rate_amount != 0
							OR r.rate_increase_increment != 0
							OR r.show_in_entrata = TRUE
							OR r.show_on_website = TRUE )
						AND r.is_allowed = TRUE
						AND r.ar_code_id = ' . ( int ) $this->getId() . '
						AND pgs.activate_standard_posting = TRUE
					GROUP BY r.property_id ';

		$arrstrSqls[] = 'SELECT
							\'Scheduled Charge Formulas\'    single_reference,
							\'' . __( 'Scheduled Charge Formulas' ) . '\'   multiple_reference,
							\'' . __( 'Scheduled Charge Formulas' ) . '\'   table_name,
							sc.property_id,
							COUNT( DISTINCT sc.id ) row_count
						 FROM
							scheduled_charges sc'
		 . $strScJoin . '
						 WHERE
							sc.cid = ' . ( int ) $this->getCid() . '
							AND sc.ar_formula_reference_id = ' . ( int ) $this->getId() .
		 ' AND sc.ar_formula_id = ' . CArFormula::PERCENT_OF_CHARGE_CODE
		 . $strScAdditionalCondition .
		 ' GROUP BY sc.property_id';

		$arrstrSqls[] = 'SELECT
							\'Rate Formulas\'    single_reference,
							\'' . __( 'Rate Formulas' ) . '\'   	multiple_reference,
							\'' . __( 'Rate Formulas' ) . '\'   	table_name,
							r.property_id,
							count( r.id ) 	row_count
						FROM
							rates r
						WHERE
							r.cid = ' . ( int ) $this->getCid() . '
							AND ( r.rate_amount != 0
								OR r.rate_increase_increment != 0
								OR r.show_in_entrata = TRUE
								OR r.show_on_website = TRUE )
							AND r.is_allowed = TRUE
							AND r.ar_formula_reference_id = ' . ( int ) $this->getId() .
		 ' AND r.ar_formula_id = ' . CArFormula::PERCENT_OF_CHARGE_CODE .
		 ' GROUP BY r.property_id';

		$arrstrSqls[] = 'SELECT
							\'Tiered Pricing Formulas\'    single_reference,
							\'' . __( 'Tiered Pricing Formulas' ) . '\'   	multiple_reference,
							\'' . __( 'Tiered Pricing Formulas' ) . '\'   	table_name,
							alt.property_id,
							count( alt.id ) 	row_count
						FROM
							ar_lookup_tables alt
						WHERE
							alt.cid = ' . ( int ) $this->getCid() . '
							AND alt.ar_formula_reference_id = ' . ( int ) $this->getId() .
		 ' AND alt.ar_formula_id = ' . CArFormula::PERCENT_OF_CHARGE_CODE .
		 ' GROUP BY alt.property_id';

		$arrstrSqls[] = 'SELECT
							\'Percent Sales Charge Code\'    single_reference,
							\'' . __( 'Percent Sales Charge Code' ) . '\'   multiple_reference,
							\'' . __( 'property_gl_settings' ) . '\'   table_name,
							pgs.property_id,
							count( pgs.id ) 		   row_count
						 FROM
							property_gl_settings pgs
						 WHERE
							pgs.cid = ' . ( int ) $this->getCid() . '
							AND pgs.percent_of_sales_ar_code_id = ' . ( int ) $this->getId() . '
						 GROUP BY pgs.property_id';

		$arrstrSqls[] = 'SELECT
							\'Rent Charge Code\'    single_reference,
							\'' . __( 'Rent Charge Code' ) . '\'   multiple_reference,
							\'' . __( 'property_gl_settings' ) . '\'   table_name,
							pgs.property_id,
							count( pgs.id ) 		   row_count
						 FROM
							property_gl_settings pgs
						 WHERE
							pgs.cid = ' . ( int ) $this->getCid() . '
							AND pgs.rent_ar_code_id = ' . ( int ) $this->getId() . '
						 GROUP BY pgs.property_id';

		$arrstrSqls[] = 'SELECT
							\'Deposit Charge Code\'    single_reference,
							\'' . __( 'Deposit Charge Code' ) . '\'   multiple_reference,
							\'' . __( 'property_gl_settings' ) . '\'   table_name,
							pgs.property_id,
							count( pgs.id ) 		   row_count
						 FROM
							property_gl_settings pgs
						 WHERE
							pgs.cid = ' . ( int ) $this->getCid() . '
							AND pgs.deposit_ar_code_id = ' . ( int ) $this->getId() . '
						 GROUP BY pgs.property_id';

		$arrstrSqls[] = 'SELECT
							\'Deposit Interest Formulas\'    single_reference,
							\'' . __( 'Deposit Interest Formulas' ) . '\'   multiple_reference,
							\'' . __( 'property_interest_formulas' ) . '\'   table_name,
							pif.property_id,
							count( pif.id ) 				 row_count
					 	 FROM
							property_interest_formulas pif
						 WHERE
							pif.cid = ' . ( int ) $this->getCid() . '
							AND pif.ar_code_id = ' . ( int ) $this->getId() . '
							AND pif.interest_formula_type_id = ' . CInterestFormulaType::DEPOSIT_INTEREST . '
							AND pif.deleted_by IS NULL
							GROUP BY pif.property_id';

		$arrstrSqls[] = 'SELECT
							\'Delinquency Interest Formulas\'    single_reference,
							\'' . __( 'Delinquency Interest Formulas' ) . '\'   multiple_reference,
							\'' . __( 'property_interest_formulas' ) . '\'   table_name,
							pif.property_id,
							count( pif.id ) 				 row_count
					 	 FROM
							property_interest_formulas pif
						 WHERE
							pif.cid = ' . ( int ) $this->getCid() . '
							AND pif.ar_code_id = ' . ( int ) $this->getId() . '
							AND pif.interest_formula_type_id = ' . CInterestFormulaType::DELINQUENCY_INTEREST . '
							AND pif.deleted_by IS NULL
		                GROUP BY pif.property_id';

		$arrstrSqls[] = 'SELECT
							\'Delinquency Interest Formula Charge Codes\'    single_reference,
							\'' . __( 'Delinquency Interest Formula Charge Codes' ) . '\'   multiple_reference,
							\'' . __( 'property_interest_formula_ar_codes' ) . '\'   table_name,
							pif.property_id,
							count( pifac.id ) 				 row_count
					 	 FROM
							property_interest_formula_ar_codes pifac
							JOIN property_interest_formulas pif ON ( pifac.cid = pif.cid AND pifac.property_interest_formula_id = pif.id )
						 WHERE
							pif.cid = ' . ( int ) $this->getCid() . '
							AND pifac.ar_code_id = ' . ( int ) $this->getId() . '
							AND pif.interest_formula_type_id = ' . CInterestFormulaType::DELINQUENCY_INTEREST . '
							AND pif.deleted_by IS NULL
						 GROUP BY pif.property_id';

		// Separating property_charge_settings table dropdown.
		$arrstrSqls[] = 'SELECT
							\'Month to Month Rent Charge Code\'    single_reference,
							\'' . __( 'Month to Month Rent Charge Code' ) . '\'   multiple_reference,
							\'' . __( 'Month_to_Month_Rent_Charge_Code' ) . '\'   table_name,
							pcs.property_id,
							count( pcs.id ) 			   row_count
						 FROM
							property_charge_settings pcs
						 WHERE
							pcs.cid = ' . ( int ) $this->getCid() . '
							AND pcs.month_to_month_rent_ar_code_id = ' . ( int ) $this->getId() . '
						 GROUP BY pcs.property_id';

		$arrstrSqls[] = 'SELECT
							\'Repayment Agreement Charge Code\'    single_reference,
							\'' . __( 'Repayment Agreement Charge Code' ) . '\'   multiple_reference,
  							\'' . __( 'repayment_agreement_charge_code' ) . '\'   table_name,
							pcs.property_id,
							count( pcs.id ) 			   row_count
						 FROM
							property_charge_settings pcs
						 WHERE
							pcs.cid = ' . ( int ) $this->getCid() . '
							AND pcs.repayment_agreement_ar_code_id = ' . ( int ) $this->getId() . '
						 GROUP BY pcs.property_id';

		$arrstrSqls[] = 'SELECT
							\'Property Write Off Charge Code\'    single_reference,
							\'' . __( 'Property Write Off Charge Code' ) . '\'   multiple_reference,
							\'' . __( 'property_ar_codes' ) . '\'    	table_name,
							pac.property_id,
							COUNT( pac.id ) 			row_count
						 FROM
							property_ar_codes pac
						 WHERE
							pac.cid = ' . ( int ) $this->getCid() . '
							AND pac.write_off_ar_code_id = ' . ( int ) $this->getId() . '
							AND pac.write_off_ar_code_id <> pac.ar_code_id
						GROUP BY pac.property_id';

		$arrstrSqls[] = 'SELECT
							\'Property Recovery Charge Code\'    single_reference,
							\'' . __( 'Property Recovery Charge Code' ) . '\'   multiple_reference,
							\'' . __( 'property_ar_codes' ) . '\'    	table_name,
							pac.property_id,
							COUNT( pac.id ) 			row_count
						 FROM
							property_ar_codes pac
						 WHERE
							pac.cid = ' . ( int ) $this->getCid() . '
							AND pac.recovery_ar_code_id = ' . ( int ) $this->getId() . '
							AND pac.recovery_ar_code_id <> pac.ar_code_id
						GROUP BY pac.property_id';

		$arrstrSqls[] = 'SELECT
							\'Transactions Pending Approvals\'    single_reference,
							\'' . __( 'Transactions Pending Approvals' ) . '\'   multiple_reference,
							\'' . __( 'Transactions Pending Approvals' ) . '\'   table_name,
							l.property_id,
							count( aar.id ) 			row_count
						FROM
							ar_approval_requests aar
							JOIN leases l ON ( l.cid = aar.cid AND l.id = aar.lease_id )
							JOIN properties p ON ( p.cid = l.cid AND p.id = l.property_id )
						WHERE
							aar.cid = ' . ( int ) $this->getCid() . '
							AND aar.is_archived = false
							AND aar.ar_transaction_id IS NULL
							AND aar.request_details ->>\'ar_code_id\' = ' . '\'' . ( int ) $this->getId() . '\'' . '
						GROUP BY l.property_id';

		$arrstrSqls[] = 'SELECT
							\'Accelerated Rent Charge Code\'    single_reference,
							\'' . __( 'Accelerated Rent Charge Code' ) . '\'   multiple_reference,
							\'' . __( 'property_gl_settings' ) . '\'   table_name,
							pgs.property_id,
							count( pgs.id ) 		   row_count
						 FROM
							property_gl_settings pgs
						 WHERE
							pgs.cid = ' . ( int ) $this->getCid() . '
							AND pgs.accel_rent_ar_code_id = ' . ( int ) $this->getId() . '
						 GROUP BY pgs.property_id';

		$arrstrSqls[] = 'SELECT
							\'Skip Accelerated Rent Charge Code\'    single_reference,
							\'' . __( 'Skip Accelerated Rent Charge Code' ) . '\'   multiple_reference,
							\'' . __( 'property_gl_settings' ) . '\'   table_name,
							pgs.property_id,
							count( pgs.id ) 		   row_count
						 FROM
							property_gl_settings pgs
						 WHERE
							pgs.cid = ' . ( int ) $this->getCid() . '
							AND pgs.skip_accel_rent_ar_code_id = ' . ( int ) $this->getId() . '
						 GROUP BY pgs.property_id';

		$arrstrSqls[] = 'SELECT
							\'Charge Code Accelerated Rent Charge Code\'    single_reference,
							\'' . __( 'Charge Code - Accelerated Rent Charge Code' ) . '\'    multiple_reference,
							\'' . __( 'ar_codes' ) . '\'    table_name,
							NULL as property_id,
							COUNT( ac.id )  row_count
						 FROM
							ar_codes ac
						 WHERE
							ac.cid = ' . ( int ) $this->getCid() . '
							AND ac.id <> ' . ( int ) $this->getId() . '
							AND ac.accel_rent_ar_code_id = ' . ( int ) $this->getId() .
		                   'GROUP BY ac.id';

		$arrstrSqls[] = 'SELECT
							\'Charge Code Skip Accelerated Rent Charge Code\'    single_reference,
							\'' . __( 'Charge Code - Skip Accelerated Rent Charge Code' ) . '\'    multiple_reference,
							\'' . __( 'ar_codes' ) . '\'    table_name,
							NULL as property_id,
							COUNT( ac.id )  row_count
						 FROM
							ar_codes ac
						 WHERE
							ac.cid = ' . ( int ) $this->getCid() . '
							AND ac.id <> ' . ( int ) $this->getId() . '
							AND ac.skip_accel_rent_ar_code_id = ' . ( int ) $this->getId() .
		                   'GROUP BY ac.id';

		$arrstrSqls[] = 'SELECT
							\'Charge Code Month to Month Rent Charge Code\'    single_reference,
							\'' . __( 'Charge Code - Month to Month Rent Charge Code' ) . '\'    multiple_reference,
							\'' . __( 'ar_codes' ) . '\'    table_name,
							NULL as property_id,
							COUNT( ac.id )  row_count
						 FROM
							ar_codes ac
						 WHERE
							ac.cid = ' . ( int ) $this->getCid() . '
							AND ac.id <> ' . ( int ) $this->getId() . '
							AND ac.month_to_month_rent_ar_code_id = ' . ( int ) $this->getId() .
		                   'GROUP BY ac.id';

		$arrstrSqls[] = 'SELECT
							\'Violation Templates\'    single_reference,
							\'' . __( 'Violation Templates' ) . '\'	multiple_reference,
							\'' . __( 'violation_template_notices' ) . '\' 	table_name,
							null as property_id,
							count( vtn.id ) 	row_count
						FROM
							violation_template_notices vtn
						WHERE
							vtn.cid = ' . ( int ) $this->getCid() . '
							AND vtn.ar_code_id =' . ( int ) $this->getId() . '
						GROUP BY vtn.ar_code_id';

		$arrstrSqls[] = 'SELECT
		              	    \'Late Fee Formulas\'    single_reference,
		              		\'' . __( 'Late Fee Formulas' ) . '\'   multiple_reference,
					        \'' . __( 'late_fee_formulas' ) . '\' 	table_name,
					        plff.property_id as property_id,
							count( lff.id ) 	row_count
				         FROM
				            late_fee_formulas lff
				            JOIN property_late_fee_formulas plff ON ( plff.cid = lff.cid AND plff.late_fee_formula_id = lff.id )
				         WHERE
				            lff.cid = ' . ( int ) $this->getCid() . '
				            AND lff.is_disabled = FALSE
				            AND ( lff.daily_ar_code_id = ' . ( int ) $this->getId() . '
				                OR lff.first_day_ar_code_id = ' . ( int ) $this->getId() . ' )
		                 GROUP BY 
		                    plff.property_id';

		$arrstrSqls[] = 'SELECT
							\'Late Fee Calculation Methods\'    single_reference,
							\'' . __( 'Late Fee Calculation Methods' ) . '\'	multiple_reference,
							\'' . __( 'late_fee_formulas' ) . '\' 	table_name,
							plff.property_id as property_id,
							count( lff.id ) 	row_count
						FROM
							late_fee_formulas lff
							JOIN property_late_fee_formulas plff ON ( plff.cid = lff.cid AND plff.late_fee_formula_id = lff.id )
						WHERE
							lff.cid = ' . ( int ) $this->getCid() . '
							AND ( ( lff.monthly_max_calculation_type_id = ' . CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE . ' AND lff.monthly_max_calculation_type_reference_id = ' . ( int ) $this->getId() . ' )
									OR ( lff.balance_threshold_calculation_type_id = ' . CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE . ' AND lff.balance_threshold_calculation_type_reference_id = ' . ( int ) $this->getId() . ' )
									OR ( lff.first_day_late_fee_calculation_method_id = ' . CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE . ' AND lff.first_day_late_fee_calculation_type_reference_id = ' . ( int ) $this->getId() . ' )
									OR ( lff.daily_late_fee_calculation_method_id = ' . CLateFeeCalculationType::PERCENT_OF_CHARGE_CODE . ' AND lff.daily_late_fee_calculation_type_reference_id = ' . ( int ) $this->getId() . ' ) )
						 GROUP BY plff.property_id';

		$arrstrSqls[] = 'SELECT
							\'Late Fee Charge Code Exceptions\'    single_reference,
							\'' . __( 'Late Fee Charge Code Exceptions' ) . '\'   multiple_reference,
							\'' . __( 'late_fee_formula_ar_codes' ) . '\'   table_name,
							plff.property_id as property_id,
							count( lffac.id ) 	row_count
						 FROM
							late_fee_formula_ar_codes lffac
							JOIN late_fee_formulas lff ON (lff.id = lffac.late_fee_formula_id AND lffac.cid = lff.cid )
							JOIN property_late_fee_formulas plff ON ( plff.cid = lff.cid AND plff.late_fee_formula_id = lff.id )
						 WHERE
							lffac.cid = ' . ( int ) $this->getCid() . '
							AND lffac.ar_code_id = ' . ( int ) $this->getId() . '
		                 GROUP BY plff.property_id';

		$arrstrSqls[] = 'SELECT
							\'Property Charge Code Allocation Rules\'    single_reference,
							\'' . __( 'Property Charge Code Allocation Rules' ) . '\'   multiple_reference,
							\'' . __( 'property_ar_code_allocation_rules' ) . '\'   table_name,
							pacar.ar_code_id,
							count( pacar.id ) 		  row_count
						 FROM
							property_ar_code_allocation_rules pacar
						 WHERE
							pacar.cid = ' . ( int ) $this->getCid() . '
							AND pacar.allowed_ar_code_id  = ' . ( int ) $this->getId() . '
						 GROUP BY pacar.ar_code_id';

		$arrstrSqls[] = 'SELECT
							\'Charge Code Allocation Rules\'    single_reference,
							\'' . __( 'Charge Code Allocation Rules' ) . '\'   multiple_reference,
							\'' . __( 'ar_code_allocation_rules' ) . '\'   table_name,
							acar.ar_code_id,
							count( acar.id ) 		  row_count
						 FROM
							ar_code_allocation_rules acar
						 WHERE
							acar.cid = ' . ( int ) $this->getCid() . '
							AND acar.allowed_ar_code_id  = ' . ( int ) $this->getId() . '
						 GROUP BY acar.ar_code_id';

		$strSql = implode( PHP_EOL . ' UNION ALL ' . PHP_EOL, $arrstrSqls );

		$arrmixArCodeDependencies = fetchData( $strSql, $objDatabase );

		foreach( $arrmixArCodeDependencies as $arrmixArCodeDependency ) {
			if( 1 == $arrmixArCodeDependency['row_count'] ) {
				$strErrorMessage .= ( NULL == $strErrorMessage ) ? __( 'Can not hide charge code from entrata core properties, it is in use' ) : ' ';
				$boolIsValid = false;

			} elseif( 1 < $arrmixArCodeDependency['row_count'] ) {
				$strErrorMessage .= ( NULL == $strErrorMessage ) ? __( 'Can not hide charge code from entrata core properties, it is in use' ) : ' ';
				$boolIsValid = false;

			}

		}

		if( false == $boolIsValid ) {
			$strErrorMessage .= '.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integrated_only', $strErrorMessage ) );
		}

		return [ $boolIsValid, $arrmixArCodeDependencies ];
	}

	public function valAssociatedChargeCode( $objDatabase ) : bool {
		$boolIsValid = true;

		if( ( false == valId( $this->getWriteOffArCodeId() ) && false == valId( $this->getRecoveryArCodeId() ) ) || true == $this->getIntegratedOnly() ) return $boolIsValid;

			$objArCodesFilter = new CArCodesFilter( false, true );
			$arrintFetchArCodeIds = [];

			if( true == valId( $this->getWriteOffArCodeId() ) ) array_push( $arrintFetchArCodeIds, $this->getWriteOffArCodeId() );
			if( true == valId( $this->getRecoveryArCodeId() ) ) array_push( $arrintFetchArCodeIds, $this->getRecoveryArCodeId() );

			$objArCodesFilter->setArCodeIds( $arrintFetchArCodeIds );
			$arrobjArCodes = \Psi\Eos\Entrata\CArCodes::createService()->fetchArCodesByCidByArCodesFilter( $this->getCid(), $objArCodesFilter, $objDatabase );

			if( true == valId( $this->getWriteOffArCodeId() ) && true == $arrobjArCodes[$this->getWriteOffArCodeId()]->getIntegratedOnly() ) {
				$strErrorMsg   = 'Entrata Core charge codes must be written off to an Entrata Core charge code.<br>';
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integrated_only', $strErrorMsg ) );
				$boolIsValid &= false;
			}

			if( true == valId( $this->getRecoveryArCodeId() ) && true == $arrobjArCodes[$this->getRecoveryArCodeId()]->getIntegratedOnly() ) {
				$strErrorMsg   = 'Entrata Core charge codes must be recovered to an Entrata Core charge code.';
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'integrated_only', $strErrorMsg ) );
				$boolIsValid &= false;
			}

		return $boolIsValid;
	}

	public function valPropertyGlSettingDependencies( $strField, $objDatabase ) {
		$boolIsValid = true;

		$strSql = 'SELECT
						pgs.*,
						p.property_name,
						pcs.month_to_month_rent_ar_code_id
					FROM
						property_gl_settings pgs
						JOIN properties p ON ( pgs.property_id = p.id AND pgs.cid = p.cid )
						JOIN property_charge_settings pcs ON ( pcs.cid = pgs.cid AND pcs.property_id = pgs.property_id )
					WHERE
						pgs.cid = ' . ( int ) $this->getCid() . '
						AND ( pgs.rent_ar_code_id = ' . ( int ) $this->getId() . '
						OR pcs.month_to_month_rent_ar_code_id = ' . ( int ) $this->getId() . '
						OR pgs.deposit_ar_code_id = ' . ( int ) $this->getId() . ')';

		$arrobjPropertyGlSettings = ( array ) CPropertyGlSettings::fetchPropertyGlSettings( $strSql, $objDatabase );

		$strErrorMessageRentArCode				= '';
		$strErrorMessageMtmRentArCode			= '';
		$strErrorMessageMtmFeeArCode			= '';
		$strErrorMessageSecurityDepositArCode	= '';

		foreach( $arrobjPropertyGlSettings as $objPropertyGlSetting ) {
			if( $objPropertyGlSetting->getRentArCodeId() == $this->getId() ) {
				$strErrorMessageRentArCode				.= ( false == valStr( $strErrorMessageRentArCode ) ) ? __( ' Charge Code currently in use in setting for rent charge code ' ): '';
				$boolIsValid = false;
			}
			if( $objPropertyGlSetting->getMonthToMonthRentArCodeId() == $this->getId() ) {
				$strErrorMessageMtmRentArCode			.= ( false == valStr( $strErrorMessageMtmRentArCode ) ) ? __( ' Charge Code currently in use in setting for month-to-month rent charge code ' ) : '';
				$boolIsValid = false;
			}
			if( $objPropertyGlSetting->getDepositArCodeId() == $this->getId() ) {
				$strErrorMessageSecurityDepositArCode	.= ( false == valStr( $strErrorMessageSecurityDepositArCode ) ) ? __( ' Charge Code currently in use in setting for security deposit charge code ' ) : '';
				$boolIsValid = false;
			}
		}

		if( false == $boolIsValid ) {
			$strAction			= ( 'ar_code_type_id' == $strField ) ? 'modify' : 'disable';
			$strErrorMessage	= '';
			$strErrorMessageRentArCode				.= ( true == valStr( $strErrorMessageRentArCode ) ) ? __( '. You must choose a different charge code for rent charge code before you can {%s, 0} this charge code.', [ $strAction ] ) : '';
			$strErrorMessageMtmRentArCode			.= ( true == valStr( $strErrorMessageMtmRentArCode ) ) ? __( '. You must choose a different charge code for month-to-month rent charge code before you can {%s, 0} this charge code.', [ $strAction ] ) : '';
			$strErrorMessageMtmFeeArCode			.= ( true == valStr( $strErrorMessageMtmFeeArCode ) ) ? __( '. You must choose a different charge code for month-to-month fee charge code before you can {%s, 0} this charge code.', [ $strAction ] ) : '';
			$strErrorMessageSecurityDepositArCode	.= ( true == valStr( $strErrorMessageSecurityDepositArCode ) ) ?__( '. You must choose a different charge code for security deposit charge code before you can {%s, 0} this charge code.', [ $strAction ] ) : '';

			$strErrorMessage = $strErrorMessageRentArCode . $strErrorMessageMtmRentArCode . $strErrorMessageMtmFeeArCode . $strErrorMessageSecurityDepositArCode;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strField, $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	public function valExistingTransaction( $objDatabase, $intOldLedgerFilterId, $boolExistingTransactionCountOnly = false ) {

		if( $intOldLedgerFilterId != $this->getLedgerFilterId() || true == $boolExistingTransactionCountOnly ) {

			$strWhereConditions  = ' WHERE
    									cid = ' . ( int ) $this->getCid() . '
    							  		AND ar_code_id = ' . ( int ) $this->getId();

			$intArTransactionCount	= \Psi\Eos\Entrata\CArTransactions::createService()->fetchArTransactionCount( $strWhereConditions, $objDatabase );

			if( true == $boolExistingTransactionCountOnly ) {
				return $intArTransactionCount;
				exit;
			}

			if( 0 < $intArTransactionCount && true == valId( $intOldLedgerFilterId ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'You cannot change ledger for this charge code, as there are some transactions associated with it.' ) ) );
				return false;
			}

			$intScheduledChargesCount = \Psi\Eos\Entrata\CScheduledCharges::createService()->fetchFutureScheduledChargesCountByArCodeIdByCid( $this->getId(), $this->getCid(), $objDatabase );

			if( 0 < $intScheduledChargesCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ledger_filter_id', __( 'You cannot change ledger for this charge code, as there are some scheduled charges associated with it.' ) ) );
				return false;
			}

			$intRateCount 	 = \Psi\Eos\Entrata\CRates::createService()->fetchRateCount( $strWhereConditions, $objDatabase );

			if( 0 < $intRateCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ledger_filter_id', __( 'You cannot change ledger for this charge code, as there are some rates associated with it.' ) ) );
				return false;
			}

			$strSql = 'SELECT
							count(*) as count
						From
							property_gl_settings pgs
							JOIN property_charge_settings pcs ON  pgs.cid=pcs.cid and pgs.property_id = pcs.property_id
						WHERE
							 pgs.cid = ' . ( int ) $this->getCid() . '
							 AND ( pgs.rent_ar_code_id = ' . ( int ) $this->getId() . ' OR pcs.month_to_month_rent_ar_code_id = ' . ( int ) $this->getId() . ' );';

			$intPropertyGlSettingCount = ( int ) array_pop( call_user_func_array( 'array_merge', fetchData( $strSql, $objDatabase ) ) );

			if( 0 < $intPropertyGlSettingCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ledger_filter_id', __( 'You cannot change ledger for this charge code, as there are scheduled charges settings associated with it.' ) ) );
				return false;
			}
		}
		return true;
	}

	// We might need to remove this function as it not in use.

	public function valRateDependencies( $objDatabase ) {
		$boolIsValid = true;

		$strSql = 'SELECT
						\'rate\'    single_reference,
						\'rates\'   multiple_reference,
  						\'rates\'   table_name,
						count( r.id ) row_count
					FROM
						rates r
						JOIN property_gl_settings pgs on (pgs.cid = r.cid AND pgs.property_id = r.property_id)
					WHERE
						r.cid = ' . ( int ) $this->getCid() . '
						AND ( r.rate_amount != 0
							OR r.rate_increase_increment != 0
							OR r.show_in_entrata = TRUE
							OR r.show_on_website = TRUE )
						AND r.is_allowed = TRUE
						AND r.ar_code_id = ' . ( int ) $this->getId() . '
						AND pgs.activate_standard_posting = TRUE';

		$arrmixRateDependencies = fetchData( $strSql, $objDatabase );

		foreach( $arrmixRateDependencies as $arrmixRateDependency ) {
			if( 1 == $arrmixRateDependency['row_count'] ) {
				$strErrorMessage .= ( NULL == $strErrorMessage ) ? __( 'Can not update charge code, it is being used in ' ) : ', ';
				$strErrorMessage .= $arrmixRateDependency['row_count'] . ' ' . $arrmixRateDependency['single_reference'];
				$boolIsValid = false;

			} elseif( 1 < $arrmixRateDependency['row_count'] ) {
				$strErrorMessage .= ( NULL == $strErrorMessage ) ? __( 'Can not update charge code, it is being used in ' ) : ', ';
				$strErrorMessage .= $arrmixRateDependency['row_count'] . ' ' . $arrmixRateDependency['multiple_reference'];
				$boolIsValid = false;
			}

		}

		if( false == $boolIsValid ) {
			$strErrorMessage .= '.';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'validate', $strErrorMessage ) );
		}

		return $boolIsValid;
	}

	private function getArCodeAutoAllocationTypeErrorMessage( $intArCodeAutoAllocationTypeId ) {
		$strErrorMessage = '';

		switch( $intArCodeAutoAllocationTypeId ) {
			case CArCodeAllocationType::RESTRICTED:
				$strErrorMessage = __( 'At least one charge code is required to enable restricted allocations.' );
				break;

			case CArCodeAllocationType::CUSTOM_PRIORITY:
				$strErrorMessage = __( 'At least one charge code is required to customize the allocation priority.' );
				break;

			default:
				// This is default case
				break;
		}

		return $strErrorMessage;
	}

	public function valWhiteListExists() : bool {
		$boolIsValid = true;

		if( !$this->isValidForAdvancedAllocation() && $this->hasWhitelist() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'white_list', __( 'Charge code should not have whitelist.' ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valWhiteListExistsForAutoAllocation() : bool {
		$boolIsValid = true;

		$arrintArCodeAutoAllocationTypes = [ CArCodeAllocationType::RESTRICTED, CArCodeAllocationType::CUSTOM_PRIORITY ];

		if( $this->isValidForAdvancedAllocation() && !$this->hasWhitelist() && in_array( $this->getArCodeAllocationTypeId(), $arrintArCodeAutoAllocationTypes ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'white_list', $this->getArCodeAutoAllocationTypeErrorMessage( $this->getArCodeAllocationTypeId() ), NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

    public function valArCodeTypeIdForStraightLineRentLedger( $objDatabase ) : bool {
        $boolIsValid = true;

        $objLedgerFilter = \Psi\Eos\Entrata\CLedgerFilters::create()->fetchLedgerFilterByIdByCid( $this->getLedgerFilterId(), $this->getCid(), $objDatabase );

        if( $objLedgerFilter->getDefaultLedgerFilterId() == CDefaultLedgerFilter::STRAIGHT_LINE && !$this->isStraightLineLedgerAppropriateArCodeTypeId() ) {
            $this->addErrorMsg( new CErrorMsg( NULL, 'ledger_filter_id', __( 'Charge Codes on the Straight Line ledger cannot be Payment, Deposit, or Inter-Company Loan type.' ) ) );
            $boolIsValid = false;
        }

        return $boolIsValid;
    }

	public function validate( $strAction, $arrintEntrataCorePropertyIds = NULL, $objDatabase = NULL, $intOldLedgerFilterId = NULL, $boolValidateAssociatedChargeCodes = false ) {
		$boolIsValid = true;

		$objArCodeDependencyValidator = new CArCodeDependencyValidator();

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valArCodeTypeId();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valLedgerFilterId();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valDebitGlAccountId( $objDatabase );
				$boolIsValid &= $this->valCreditGlAccountId( $objDatabase );
				$boolIsValid &= $this->valDefaultAmount();
				$boolIsValid &= $this->valCaptureDelayDays();
				$boolIsValid &= $this->valPostToCash();
				$boolIsValid &= ( true == $boolValidateAssociatedChargeCodes ) ? $this->valAssociatedChargeCode( $objDatabase ) : true;
				$boolIsValid &= $this->valWhiteListExistsForAutoAllocation();
                $boolIsValid &= $this->valArCodeTypeIdForStraightLineRentLedger( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				// As we are allowing to change the ar code type id commenting the below validation. REF task #938333
				// $boolIsValid &= $this->valArCodeTypeId( $arrobjAllArCodes, $objDatabase );
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valLedgerFilterId();
				$boolIsValid &= $this->valDescription();
				$arrmixArCodeDependencies = CArCodeDependencyValidator::checkArCodeDependencies( $objDatabase, $this );
				$boolIsValid &= $this->valDefaultAmount();
				$boolIsValid &= $this->valCaptureDelayDays();
				if( true == valArr( $arrmixArCodeDependencies ) ) {
					$boolIsValid &= $arrmixArCodeDependencies[0];
				} else {
					$boolIsValid &= $arrmixArCodeDependencies;
				}

				$arrmixIntegratedArCodeDependencies = $this->valIntegratedOnly( $objDatabase, $arrintEntrataCorePropertyIds );

				if( true == valArr( $arrmixIntegratedArCodeDependencies ) ) {
					$arrmixArCodeDependencies = $arrmixIntegratedArCodeDependencies;
					$boolIsValid &= $arrmixArCodeDependencies[0];
				} else {
					$boolIsValid &= $arrmixIntegratedArCodeDependencies;
				}

				if( false == $this->getIntegratedOnly() ) {
					$boolIsValid &= $this->valDebitGlAccountId( $objDatabase );
					$boolIsValid &= $this->valCreditGlAccountId( $objDatabase );
				}

				$boolIsValid &= $this->valExistingTransaction( $objDatabase, $intOldLedgerFilterId );
				$boolIsValid &= $this->valPostToCash();
				$boolIsValid &= ( true == $boolValidateAssociatedChargeCodes ) ? $this->valAssociatedChargeCode( $objDatabase ) : true;
				$boolIsValid &= $this->valWhiteListExists();
				$boolIsValid &= $this->valWhiteListExistsForAutoAllocation();
                $boolIsValid &= $this->valArCodeTypeIdForStraightLineRentLedger( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valIsSystem();
				$boolIsValid &= ( CArCodeDependencyValidator::checkArCodeDependencies( $objDatabase, $this, $objDataChange = NULL, $boolIsSqlOnly = false, $boolIsAffectedTablesOnly = false, CArCodeDependencyValidator::MODE_CLEAN_UP_CHARGE_CODE ) )[0];
				break;

			case 'VALIDATE_REORDER_UPDATE':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCaptureDelayDays();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valDebitGlAccountId( $objDatabase );
				$boolIsValid &= $this->valCreditGlAccountId( $objDatabase );
				$boolIsValid &= $this->valDefaultAmount();
				$boolIsValid &= $this->valPostToCash();
				break;

			case 'VALIDATE_INTEGRATION_UPDATE':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCaptureDelayDays();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valDebitGlAccountId( $objDatabase );
				$boolIsValid &= $this->valCreditGlAccountId( $objDatabase );
				$boolIsValid &= $this->valDefaultAmount();
				$boolIsValid &= $this->valPostToCash();
				break;

			case 'VALIDATE_UPDATE_BULK_CHARGE_CODES':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valArCodeTypeId();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDebitGlAccountId( $objDatabase );
				$boolIsValid &= $this->valCreditGlAccountId( $objDatabase );
				$boolIsValid &= $this->valDefaultAmount();
				$boolIsValid &= $this->valPostToCash();
				break;

			case 'validate_dependant_ar_codes':
				$boolIsValid &= $this->valAccelRentArCodeId();
				$boolIsValid &= $this->valSkipAccelRentArCodeId();
				$boolIsValid &= $this->valMonthToMonthRentArCodeId();
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return [ $boolIsValid, $arrmixArCodeDependencies ];
	}

	public function insert( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getLedgerFilterId() ) ) {
			$intLedgerFilterId = \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFilterIdByDefaultLedgerFilterIdByCid( CDefaultLedgerFilter::RESIDENT, $this->getCid(), $objDatabase );
			$this->setLedgerFilterId( $intLedgerFilterId );
		}

		$boolIsValid = true;
		if( false == parent::insert( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			$objDatabase->rollback();
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid
			&& $this->hasArCodeAllocationRules()
			&& false == $this->insertArCodeAllocationRules( $intCompanyUserId, $objDatabase ) ) {

			$objDatabase->rollback();
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly = false, $boolUpdateWhiteList = false ) {

		$boolIsValid = true;
		if( true == $this->getIsView() ) {
			trigger_error( __( 'You cannot update an ar_code record, that has been loaded from the view.' ), E_USER_ERROR );
			exit;
		}

		if( true == is_null( $this->getLedgerFilterId() ) ) {
			$intLedgerFilterId = \Psi\Eos\Entrata\CLedgerFilters::createService()->fetchLedgerFilterIdByDefaultLedgerFilterIdByCid( CDefaultLedgerFilter::RESIDENT, $this->getCid(), $objDatabase );
			$this->setLedgerFilterId( $intLedgerFilterId );
		}

		if( false == parent::update( $intCompanyUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			$objDatabase->rollback();
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$boolIsValid &= false;
		}

		if( true == $boolIsValid && true == $boolUpdateWhiteList && false == $this->deleteAndInsertArCodeAllocationRules( $objDatabase, $intCompanyUserId ) ) {
			$objDatabase->rollback();
			$this->addErrorMsgs( $this->getErrorMsgs() );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function fetchEligibleCreditGlAccountsByCid( $objDatabase ) {
		return CGlAccounts::fetchEligibleCreditGlAccountsByArCodeTypeIdByArOriginIdByArTriggerIdByCid( $this->getArCodeTypeId(), $this->getArOriginId(), $this->getArTriggerId(), $this->getCid(), $objDatabase );
	}

	public function fetchEligibleDebitGlAccountsByCid( $objDatabase ) {
		return CGlAccounts::fetchEligibleDebitGlAccountsByArCodeTypeIdByCid( $this->getArCodeTypeId(), $this->getCid(), $objDatabase, $this->getId() );
	}

	public function loadDefaultPostToCashValue() {

		$arrintGlAccountTypeIds = [ CGlAccountType::INCOME, CGlAccountType::EXPENSES ];
		$arrintArCodeTypeIds  = [ CArCodeType::RENT, CArCodeType::OTHER_INCOME, CArCodeType::EXPENSE ];

		$boolPostToCash = ( true == $this->getPostToCash() || false == in_array( $this->getGlAccountTypeId(), $arrintGlAccountTypeIds ) || false == in_array( $this->getArCodeTypeId(), $arrintArCodeTypeIds ) ) ? true : false;

		$this->setPostToCash( $boolPostToCash );

		return;
	}

	public function loadArCodeAllocationRulesCollection( CDatabase $objDatabase ) {
		$arrobjArCodeAllocationRules = [];
		if( valId( $this->getId() ) ) {
			$arrobjArCodeAllocationRules = ( array ) \Psi\Eos\Entrata\CArCodeAllocationRules::createService()
				->fetchSortedArCodeAllocationRulesByArCodeIdByCid( $this->getId(), $this->getCid(), $objDatabase );
		}

		$objArCodeAllocationRulesCollection = new \Psi\Core\AccountsReceivables\Collections\CArCodeAllocationRulesCollection( $arrobjArCodeAllocationRules, 'AllowedArCodeId' );
		$this->setArCodeAllocationRulesCollection( $objArCodeAllocationRulesCollection );
	}

	public function mapKeyValuePair( $arrmixArCodeDetails ) {

		$this->m_strKeyValuePair = '';

		foreach( $arrmixArCodeDetails as $strKey => $strValue ) {
			$this->m_strKeyValuePair .= \Psi\CStringService::singleton()->strtoupper( $strKey ) . '::' . $strValue . '~^~';
		}

		return $this->m_strKeyValuePair;
	}

	public function log( $objOldArCode, $objNewArCode, $arrmixArCodeDetails, $objCompanyUser, $objDatabase ) {

		$objTableLog = new CTableLog();

		$arrmixOldArCode                = $objOldArCode->getArCodeDetails( $arrmixArCodeDetails );
		$arrmixNewArCode                = $objNewArCode->getArCodeDetails( $arrmixArCodeDetails );
		$this->m_strArCodeDescription   = __( 'Ar Code has been modified.' );

		$strOldArCodeKeyValuePair = $objOldArCode->mapKeyValuePair( $arrmixOldArCode );
		$strNewArCodeKeyValuePair = $objNewArCode->mapKeyValuePair( $arrmixNewArCode );

		if( 0 != \Psi\CStringService::singleton()->strcasecmp( $strOldArCodeKeyValuePair, $strNewArCodeKeyValuePair ) ) {
			$objTableLog->insert( $objCompanyUser->getId(), $objDatabase, 'ar_codes', $objNewArCode->getId(), 'UPDATE', $objNewArCode->getCid(), $strOldArCodeKeyValuePair, $strNewArCodeKeyValuePair, $this->m_strArCodeDescription );
		}

		return true;
	}

	public function getArCodeDetails( $arrmixArCodeDetails ) {

		$arrmixArCode 	= [];

		$arrmixArCode['credit_gl_account'] 		= ( true == array_key_exists( $this->getCreditGlAccountId(), $arrmixArCodeDetails['credit_gl_accounts'] ) ) ? $arrmixArCodeDetails['credit_gl_accounts'][$this->getCreditGlAccountId()]->getAccountNumber() . ' - ' . $arrmixArCodeDetails['credit_gl_accounts'][$this->getCreditGlAccountId()]->getAccountName() : NULL;
		$arrmixArCode['debit_gl_account']		= ( true == array_key_exists( $this->getDebitGlAccountId(), $arrmixArCodeDetails['debit_gl_accounts'] ) ) ? $arrmixArCodeDetails['debit_gl_accounts'][$this->getDebitGlAccountId()]->getAccountNumber() . ' - ' . $arrmixArCodeDetails['debit_gl_accounts'][$this->getDebitGlAccountId()]->getAccountName() : NULL;
		$arrmixArCode['associated_ledger']		= ( true == valId( $this->getLedgerFilterId() ) && array_key_exists( $this->getLedgerFilterId(), $arrmixArCodeDetails['associated_ledgers'] ) )? $arrmixArCodeDetails['associated_ledgers'][$this->getLedgerFilterId()]->getName() : NULL;
		$arrmixArCode['write_off_code']			= ( true == valId( $this->getWriteOffArCodeId() ) && array_key_exists( $this->getWriteOffArCodeId(), $arrmixArCodeDetails['write_off_codes'] ) ) ? $arrmixArCodeDetails['write_off_codes'][$this->getWriteOffArCodeId()]->getName() : NULL;
		$arrmixArCode['ar_code_group']			= ( true == valId( $this->getArCodeGroupId() ) && array_key_exists( $this->getArCodeGroupId(), $arrmixArCodeDetails['ar_code_groups'] ) ) ? $arrmixArCodeDetails['ar_code_groups'][$this->getArCodeGroupId()]->getName() : NULL;
		$arrmixArCode['ar_code_summary_type']	= ( true == valId( $this->getArCodeSummaryTypeId() ) && array_key_exists( $this->getArCodeSummaryTypeId(), $arrmixArCodeDetails['ar_code_summary_types'] ) ) ? $arrmixArCodeDetails['ar_code_summary_types'][$this->getArCodeSummaryTypeId()]->getName() : NULL;
		$arrmixArCode['ar_code_type']			= ( true == valId( $this->getArCodeTypeId() ) && array_key_exists( $this->getArCodeTypeId(), $arrmixArCodeDetails['ar_code_types'] ) ) ? $arrmixArCodeDetails['ar_code_types'][$this->getArCodeTypeId()]->getName() : NULL;

		if( CArCodeType::PAYMENT == $this->getArCodeTypeId() ) {
			$arrmixArCode['modified_debit_gl_account']		= ( true == valId( $this->getModifiedDebitGlAccountId() ) && array_key_exists( $this->getModifiedDebitGlAccountId(), $arrmixArCodeDetails['modified_debit_gl_accounts'] ) ) ? $arrmixArCodeDetails['modified_debit_gl_accounts'][$this->getModifiedDebitGlAccountId()]->getAccountNumber() . ' - ' . $arrmixArCodeDetails['modified_debit_gl_accounts'][$this->getModifiedDebitGlAccountId()]->getAccountName() : NULL;
		}

		$arrmixArCode['name']								= $this->getName();
		$arrmixArCode['description']						= $this->getDescription();
		$arrmixArCode['default_amount'] 					= $this->getDefaultAmount();
		$arrmixArCode['capture_delay_days']    				= $this->getCaptureDelayDays();
		$arrmixArCode['hide_charges_from_residents']		= $this->getHideCharges();
		$arrmixArCode['hide_credits_from_residents'] 		= $this->getHideCredits();
		$arrmixArCode['waive_late_fees'] 					= $this->getWaiveLateFees();
		$arrmixArCode['post_to_cash']						= $this->getPostToCash();
		$arrmixArCode['show_move_out_reminder'] 			= $this->getShowMoveOutReminder();
		$arrmixArCode['hide_from_entrata_core_properties'] 	= $this->getIntegratedOnly();
		$arrmixArCode['dont_export']  						= $this->getDontExport();
		$arrmixArCode['prorate_charges'] 					= $this->getProrateCharges();
		$arrmixArCode['default_amount_is_editable'] 		= $this->getDefaultAmountIsEditable();
		$arrmixArCode['require_note'] 						= $this->getRequireNote();
		$arrmixArCode['disable_charge_code']				= $this->getIsDisabled();
		$arrmixArCode['capture_on_application_approval']	= $this->getCaptureOnApplicationApproval();

		return $arrmixArCode;
	}

	public function getAllowIsTaxable() :bool {
		return ( false == in_array( $this->getArCodeTypeId(), CArCodeType::$c_arrintHideArCodeTypeIds ) && false == in_array( $this->getArTriggerId(), CArTrigger::$c_arrintArInvoicingExcludedArTriggers ) );
	}

	public function isValidForAdvancedAllocation() : bool {
		$arrintExcludedArCodeTypes = [ CArCodeType::INTER_COMPANY_LOAN, CArCodeType::INTER_COMPANY_RECEIVABLE, CArCodeType::MANAGEMENT_FEES ];
		return ( !$this->getIsDisabled()
		         && !$this->getIntegratedOnly()
		         && !in_array( $this->getArCodeTypeId(), $arrintExcludedArCodeTypes )
		         && ( !$this->getIsReserved() || $this->getArCodeTypeId() == CArCodeType::PAYMENT ) );
	}

	public function createArCodeAllocationRule( int $intAllowedArCodeId ) : CArCodeAllocationRule {
		$objArCodeAllocationRule = new \CArCodeAllocationRule();
		$objArCodeAllocationRule->setCid( $this->getCid() );
		$objArCodeAllocationRule->setArCodeId( $this->getId() );
		$objArCodeAllocationRule->setAllowedArCodeId( $intAllowedArCodeId );

		return $objArCodeAllocationRule;
	}

	public function hasArCodeAllocationRules() : bool {
		return valObj( $this->getArCodeAllocationRulesCollection(), \Psi\Core\AccountsReceivables\Collections\CArCodeAllocationRulesCollection::class );
	}

	public function insertArCodeAllocationRules( $intCurrentUserId, $objDatabase ) {
		$boolIsValid = true;

		if( $this->hasArCodeAllocationRules()
			&& valId( $this->getId()
			&& valArr( $this->getArCodeAllocationRulesCollection()->getAll() ) ) ) {

			$this->getArCodeAllocationRulesCollection()->setArCodeId( $this->getId() );

			if( false == \Psi\Eos\Entrata\CArCodeAllocationRules::createService()->bulkInsert( $this->getArCodeAllocationRulesCollection()->getAll(), $intCurrentUserId, $objDatabase ) ) {
				$this->addErrorMsgs( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'ArCode Allocation Rules failed to insert.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function deleteArCodeAllocationRules( $objDatabase ) {
		$boolIsValid = true;
		if( valId( $this->getId() ) ) {

			if( false == \Psi\Eos\Entrata\CArCodeAllocationRules::createService()->deleteArCodeAllocationRulesByArCode( $this, $objDatabase ) ) {
				$this->addErrorMsgs( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'ArCode Allocation Rules failed to delete.' ) ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function getOrFetchNextId( CDatabase $objDatabase ) {
		if( !valId( $this->getId() ) ) {
			$this->fetchNextId( $objDatabase );
		}

		return $this->getId();
	}

	function isSkipAllocation() : bool {
		return \CArCodeAllocationType::DO_NOT_AUTO_ALLOCATE == $this->getArCodeAllocationTypeId();
	}

	/**
	 * @param      $objDatabase
	 * @param      $intCompanyUserId
	 * @return bool
	 */
	public function deleteAndInsertArCodeAllocationRules( $objDatabase, $intCompanyUserId ) : bool {
		$boolIsValid = true;

		if( false == $this->deleteArCodeAllocationRules( $objDatabase ) ) {

			$boolIsValid &= false;
		}

		if( true == $boolIsValid
		    && $this->hasArCodeAllocationRules()
		    && false == $this->insertArCodeAllocationRules( $intCompanyUserId, $objDatabase ) ) {

			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

    /**
     * @return bool
     */
    public function isStraightLineLedgerAppropriateArCodeTypeId(): bool {
        return !in_array( $this->getArCodeTypeId(), CArCodeType::$c_arrintExcludeFromStraightLineLedgerArCodeTypes );
    }

}
?>