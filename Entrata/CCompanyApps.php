<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyApps
 * Do not add any new functions to this class.
 */

class CCompanyApps extends CBaseCompanyApps {

	public static function fetchCompanyAppByAppClientIdByCid( $strClientId, $intCid, $objDatabase ) {
		// static management id = 5061 as per requirement, task id:255131
		$strSql = 'SELECT
						ca.*,
						oc.id as oauth_client_id
					FROM
						company_apps ca
				    	JOIN apps a ON ( a.id = ca.app_id )
					 	JOIN oauth_clients oc ON ( oc.id = a.oauth_client_id )
					WHERE
						ca.cid = ' . ( int ) $intCid . '
					  	AND ( a.approved_by IS NOT NULL OR ' . ( int ) $intCid . ' = 5061 )
					  	AND oc.client_id = \'' . $strClientId . '\'';

		return parent::fetchCompanyApp( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyAppByAppIdByCid( $intAppId, $intCid, $objDatabase ) {
		$strSql = 'SELECT ca.* FROM company_apps ca WHERE ca.cid = ' . ( int ) $intCid . ' AND ca.app_id = ' . ( int ) $intAppId;
		return parent::fetchCompanyApp( $strSql, $objDatabase );
	}

	public static function fetchCustomAssociatedCompanyAppsRatingCountByAppId( $intAppId, $objDatabase ) {
		$strSql = ' WHERE app_id = ' . ( int ) $intAppId;
		return parent::fetchCompanyAppCount( $strSql, $objDatabase );
	}

	public static function fetchPublishedCompanyAppByAppIdByCid( $intAppId, $intCid, $objDatabase ) {

		if( 0 >= $intAppId || 0 >= $intCid ) return NULL;

		// static management id = 5061 as per requirement, task id:255131
		$strSql	= 'SELECT
						mc.company_name,
						mc.rwx_domain,
						a.name as app_name,
						oc.auth_url as app_url,
						oc.auth_email,
						oc.client_id,
						oc.id as oauth_client_id,
						ca.*
					FROM
						company_apps as ca
						JOIN apps as a ON ca.app_id = a.id
						JOIN oauth_clients oc ON oc.id = a.oauth_client_id
						JOIN clients as mc ON mc.id = ca.cid
					WHERE
						ca.is_published = 1
						AND ( a.approved_by IS NOT NULL OR ' . ( int ) $intCid . ' = 5061 )
						AND ca.app_id = ' . ( int ) $intAppId . '
						AND ca.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return parent::fetchCompanyApp( $strSql, $objDatabase );
	}

	public static function fetchCompanyAppsByCompanyUserIdByIsAdministratorByCid( $intCompanyUserId, $boolIsAdministrator, $intCid, $objDatabase ) {
		$strWhere = '';
		// static management id = 5061 as per requirement, task id:255131
		$strSql = 'SELECT * FROM (
				   SELECT
						DISTINCT ON ( ca.id )
						ca.*,
						a.name as app_name,
						oc.client_id as client_id,
						oc.client_secret as client_secret
					FROM
						company_apps ca
						JOIN apps a ON ( a.id = ca.app_id )
						JOIN oauth_clients oc ON ( oc.id = a.oauth_client_id )
						LEFT JOIN property_apps pa ON ( pa.company_app_id = ca.id AND pa.cid = ca.cid ) ';
		if( 1 != $boolIsAdministrator ) {
			$strSql .= ' LEFT JOIN view_company_user_properties cup ON ( pa.property_id = cup.property_id AND pa.cid = cup.cid AND cup.company_user_id = ' . ( int ) $intCompanyUserId . ' AND cup.cid = ' . ( int ) $intCid . ' )
							WHERE
		     				( ca.allow_all_properties = 1 OR ( pa.id IS NOT NULL AND cup.property_id IS NOT NULL ) )
							AND ';
		} else {
			$strSql .= 'WHERE ';
		}

		$strSql .= ' ca.cid = ' . ( int ) $intCid . ' AND a.is_published = 1
					AND ( a.approved_by IS NOT NULL OR ca.cid = 5061 ) ) result
					ORDER BY result.app_name';

		return parent::fetchCompanyApps( $strSql, $objDatabase );
	}

	public static function fetchCompanyAppsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) return false;

		// static management id = 5061 as per requirement, task id:255131
		$strSql = 'SELECT
						ca.*,
						a.name as app_name,
						oc.auth_url as app_url,
						oc.auth_email,
						oc.client_id,
						oc.id as oauth_client_id
					FROM
						company_apps ca
                        JOIN apps a ON ( a.id = ca.app_id )
                        JOIN oauth_clients oc ON oc.id = a.oauth_client_id
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ( a.approved_by IS NOT NULL OR ' . ( int ) $intCid . ' = 5061 )
						AND ca.id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return parent::fetchCompanyApps( $strSql, $objDatabase );
	}

	public static function fetchCompanyAppsByAppIdsByCid( $arrintAppIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintAppIds ) ) return false;

		// static management id = 5061 as per requirement, task id:255131
		$strSql = 'SELECT
						ca.*,
						a.name as app_name,
						oc.auth_url as app_url,
						oc.auth_email,
						oc.client_id,
						oc.id as oauth_client_id
					FROM
						company_apps ca
						JOIN apps a ON ( a.id = ca.app_id )
						JOIN oauth_clients oc ON oc.id = a.oauth_client_id
					WHERE
						ca.cid = ' . ( int ) $intCid . '
						AND ( a.approved_by IS NOT NULL OR ' . ( int ) $intCid . ' = 5061 )
						AND ca.app_id IN ( ' . implode( ',', $arrintAppIds ) . ' )';

		return parent::fetchCompanyApps( $strSql, $objDatabase );
	}

}
?>