<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSplitQueueProperties
 * Do not add any new functions to this class.
 */

class CSplitQueueProperties extends CBaseSplitQueueProperties {

    public static function fetchSplitQueueProperties( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
        return self::fetchCachedObjects( $strSql, 'CSplitQueueProperty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false, $boolIsReturnKeyedArray );
    }

    public static function fetchSplitQueueProperty( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CSplitQueueProperty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>