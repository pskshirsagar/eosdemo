<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRuleStopResults
 * Do not add any new functions to this class.
 */

class CRuleStopResults extends CBaseRuleStopResults {

	public static function fetchAllApprovedRuleStopResultsByReferenceIdByRouteRuleIdByRouteTypeIdByCid( $intReferenceId, $intRouteRuleId, $intRouteTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT temp_rss.* FROM
						(
							SELECT
								DISTINCT ON ( rss.rule_stop_id )
								rss.*,
								rs.order_num
							FROM
								rule_stop_results rss
								JOIN rule_stops rs ON ( rs.cid = rss.cid AND rs.id = rss.rule_stop_id AND rs.deleted_on IS NULL )
							WHERE
								rss.cid = ' . ( int ) $intCid . '
								AND rss.route_type_id = ' . ( int ) $intRouteTypeId . '
								AND rss.reference_id = ' . ( int ) $intReferenceId . '
								AND rs.route_rule_id = ' . ( int ) $intRouteRuleId . '
								AND NOT rss.is_archived
							ORDER BY
								rss.rule_stop_id, rss.id DESC
						) as temp_rss
					WHERE
						temp_rss.rule_stop_status_type_id = ' . CRuleStopStatusType::APPROVED . '
					ORDER BY
						temp_rss.order_num';

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchOtherRuleStopResultsByCidById( $intCid, $intId, $objDatabase ) {

		$strSql = 'SELECT
						rss.*
					FROM
						rule_stop_results rss
						JOIN (
							SELECT
								cid,
								property_id,
								reference_id,
								rule_stop_id
							FROM
								rule_stop_results rss1
							WHERE
								rss1.cid = ' . ( int ) $intCid . '
								AND rss.id = ' . ( int ) $intId . '
								AND NOT rss1.is_archived

						) ref ON ( ref.cid = rss.cid AND ref.property_id = rss.property_id AND ref.reference_id = rss.reference_id AND ref.rule_stop_id = rss.rule_stop_id )
					WHERE
						rss.id <> ' . ( int ) $intId . '
						AND rss.cid = ' . ( int ) $intCid;

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchTotalPendingRuleStopResultsCountByReferenceIdByRouteTypeIdByCid( $intReferenceId, $intRouteTypeId, $intCid, $objDatabase ) {

		$strSql = 'WITH total_results AS (

						SELECT
							rsr.*,
							rs.order_num,
							rs.route_id,
							rs.route_rule_id,
							ROW_NUMBER() OVER ( PARTITION BY rsr.rule_stop_id, rsr.property_id, rsr.reference_id ORDER BY rsr.id DESC ) as row_num
						FROM
							rule_stop_results rsr
							JOIN rule_stops rs ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
						WHERE
							rsr.cid = ' . ( int ) $intCid . '
							AND NOT rsr.is_archived
							AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
							AND rsr.reference_id = ' . ( int ) $intReferenceId . '

					), total_rule_stops AS (

						SELECT
							tr.cid,
							tr.reference_id,
							COUNT(tr.id) AS rule_stop_count
						FROM
							total_results tr
						WHERE
							tr.cid = ' . ( int ) $intCid . '
							AND tr.row_num = 1
						GROUP BY
							tr.cid,
							tr.reference_id

					), actual_approved_stops AS (

						SELECT
							tr.cid,
							tr.reference_id,
							COUNT(tr.id) AS approved_count
						FROM
							total_results tr
						WHERE
							tr.cid = ' . ( int ) $intCid . '
							AND tr.row_num = 1
							AND tr.rule_stop_status_type_id = ' . CRuleStopStatusType::APPROVED . '
						GROUP BY
							tr.cid,
							tr.reference_id

					)

					SELECT
						( trs.rule_stop_count - COALESCE( aas.approved_count, 0 ) ) as pending_routes_count
					FROM
						total_rule_stops trs
						LEFT JOIN actual_approved_stops aas ON ( aas.cid = trs.cid AND aas.reference_id = trs.reference_id )
					WHERE
						trs.rule_stop_count <> COALESCE( aas.approved_count, 0 );';

		$intPendingRoutesCount = parent::fetchColumn( $strSql, 'pending_routes_count', $objDatabase );

		return $intPendingRoutesCount;
	}

	public static function fetchActiveRuleStopResultsByPropertyIdsByReferenceIdByRouteTypeIdByCid( $arrintPropertyIds, $intReferenceId, $intRouteTypeId, $intCid, $objDatabase, $intSelectedRuleStopOrderNum = NULL, $intCurrentRuleStopOrderNum = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return;
		}

		$strWhereCondition = ( false == is_null( $intSelectedRuleStopOrderNum ) ) ? ' AND rss.order_num >= ' . ( int ) $intSelectedRuleStopOrderNum . ' AND rss.order_num <= ' . ( int ) $intCurrentRuleStopOrderNum : '';

		$strSql = 'SELECT * FROM
						rule_stop_results rss
					WHERE
						rss.cid = ' . ( int ) $intCid . '
						AND rss.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rss.reference_id = ' . ( int ) $intReferenceId . '
						AND rss.property_id IN ( ' . sqlIntImplode( $arrintPropertyIds ) . ' )
						AND NOT rss.is_archived' . $strWhereCondition;

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchPendingRuleStopResultsByRouteTypeIdByCompanyUserIdsByPropertyIdsByCid( $intRouteTypeId, $arrintCompanyUserIds, $arrintPropertyIds, $intCid, $objDatabase, $boolIsRoutingMaster = false ) {
		if( false == $boolIsRoutingMaster && false == valArr( $arrintCompanyUserIds ) ) {
			return;
		}
		if( false == valArr( $arrintPropertyIds ) ) {
			return;
		}

		// Rule stop results with order num
		// Added join of routes to get the permission for adding a reject note
		$strSql = 'SELECT
						rs.route_id,
						rs.route_rule_id,
						rs.company_group_id,
						rs.company_user_id As rule_stop_company_user_id,
						cg.name AS company_group_name,
						ce.name_first || \' \' || ce.name_last AS company_user_name,
						p.property_name,
						r.require_notes_on_rejected_returned,
						rsr.*,
						ROW_NUMBER() OVER ( PARTITION BY rsr.reference_id, rsr.property_id, rs.order_num ORDER BY rsr.id DESC ) as row_num_1
					FROM
						rule_stop_results rsr
						JOIN rule_stops rs ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
						JOIN routes r ON ( r.cid = rs.cid AND r.id = rs.route_id )
						LEFT JOIN company_groups cg ON ( cg.cid = rs.cid AND cg.id = rs.company_group_id )
						LEFT JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.company_user_id )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
						JOIN properties p ON ( p.cid = rsr.cid AND p.id = rsr.property_id AND p.is_disabled = 0 )
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rsr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND NOT rsr.is_archived
					ORDER BY
						rsr.property_id, rs.order_num, rsr.order_num';

		$strSql = 'SELECT
						temp_rsr.*
					FROM
						(
							SELECT 
								*,
								ROW_NUMBER() OVER ( PARTITION BY trs.reference_id, trs.property_id ORDER BY trs.order_num ) AS row_num_2
							FROM	
								( ' . $strSql . ' ) AS trs
							WHERE
								trs.row_num_1 = 1
								AND trs.rule_stop_status_type_id = ' . CRuleStopStatusType::PENDING . '
						) AS temp_rsr
					WHERE
						temp_rsr.row_num_2 = 1';

		if( false == $boolIsRoutingMaster && true == valArr( $arrintCompanyUserIds ) ) {

			$strSql .= ' AND (
								temp_rsr.company_group_id IN ( SELECT
																	cug.company_group_id
																FROM
																	company_user_groups cug
																WHERE
																	cug.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
																	AND cug.cid = ' . ( int ) $intCid . ' )
								OR
								temp_rsr.rule_stop_company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
							) ';
		}

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchPendingRuleStopResultsByReferenceIdByPropertyIdsByRouteTypeIdByCid( $intRouteTypeId, array $arrintReferenceIds, $arrintPropertyIds, $intCid, $objDatabase, $strOrder = 'current', $arrintCompanyUserIds = [], $boolIsRoutingMaster = false, $boolShowPendingApproval = false, $boolIsFetchData = false, $arrmixRuleStopDetails = [] ) {

		// Fixme: Execution is returning Object of Class rule_stop_results By DEFAULT, so can't access non-member variables

		if( false == valIntArr( $arrintPropertyIds ) ) {
			return;
		}
		if( false == $boolShowPendingApproval && false == $boolIsRoutingMaster && false == valArr( $arrintCompanyUserIds ) ) {
			return;
		}

		$strWhereCaluse = 'AND rsr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )';

		if( true == valArr( $arrmixRuleStopDetails ) ) {
			$strWhereCaluse = 'AND rsr.property_id IN ( ' . sqlIntImplode( array_keys( $arrmixRuleStopDetails ) ) . ' )';
		}

		$arrmixWhereConditions = [];

		foreach( $arrmixRuleStopDetails as $intPropertyId => $intRuleStopOrderNum ) {
			$arrmixWhereConditions[] .= '( temp_rsr.property_id = ' . ( int ) $intPropertyId . ' AND temp_rsr.row_num_2 > ' . ( int ) $intRuleStopOrderNum . ' )';
		}

		$strOrderBy					= ( true == valArr( $arrmixRuleStopDetails ) ) ? ' ORDER BY trs.order_num ASC, trs.id ASC' : ' ORDER BY trs.order_num DESC, trs.id DESC';
		$strOuterWhereCondition		= ( true == valArr( $arrmixWhereConditions ) ) ? implode( ' OR ', $arrmixWhereConditions ) : ' temp_rsr.row_num_2 = 1';
		$strCompanyUserName         = ( true == $boolIsFetchData ) ? 'util_get_translated( \'name\', cg.name, cg.details )' : 'cg.name';
		$boolIsReturnKeyedArray = true;

		// Rule stop results with order num
		$strSql = 'SELECT
						rs.route_id,
						r.allow_notes_on_approval,
						rs.route_rule_id,
						rs.company_group_id,
						rs.company_user_id As rule_stop_company_user_id,
						' . $strCompanyUserName . ' AS company_group_name,
						ce.name_first || \' \' || ce.name_last AS company_user_name,
						p.property_name,
						rsr.*,';

		$strSql .= ( in_array( $intRouteTypeId, [ CRouteType::TRANSACTIONS, CRouteType::SCHEDULED_CHARGES ] ) ) ? ' apr.id as ar_approval_request_id, apr.request_details,' : '';

		$strSql .= '
						ROW_NUMBER() OVER ( PARTITION BY rsr.reference_id, rsr.property_id, rs.order_num ORDER BY rsr.id DESC ) as row_num_1
					FROM
						rule_stop_results rsr
						JOIN rule_stops rs ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
						JOIN routes r ON( r.cid = rs.cid AND r.id = rs.route_id )
						LEFT JOIN company_groups cg ON ( cg.cid = rs.cid AND cg.id = rs.company_group_id )
						';

		$strSql .= ( in_array( $intRouteTypeId, [ CRouteType::TRANSACTIONS, CRouteType::SCHEDULED_CHARGES ] ) ) ? ' JOIN ar_approval_requests apr ON ( apr.cid = rsr.cid AND apr.id = rsr.reference_id AND apr.route_type_id IN ( ' . CRouteType::TRANSACTIONS . ', ' . CRouteType::SCHEDULED_CHARGES . ' ) ) ' : '';

		$strSql .= '
						LEFT JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.company_user_id )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
						JOIN properties p ON ( p.cid = rsr.cid AND p.id = rsr.property_id AND p.is_disabled = 0 )
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rsr.reference_id IN ( ' . implode( ', ', $arrintReferenceIds ) . ' )
						' . $strWhereCaluse . '
						AND NOT rsr.is_archived
					ORDER BY
						rsr.property_id, rs.order_num, rsr.order_num';

		// Current
		if( 'current' == $strOrder ) {

			$strSql = 'SELECT
							temp_rsr.*
						FROM
							(
								SELECT 
									*,
									ROW_NUMBER() OVER ( PARTITION BY trs.reference_id, trs.property_id ORDER BY trs.order_num ) AS row_num_2
								FROM
									( ' . $strSql . ' ) AS trs
								WHERE
									trs.row_num_1 = 1
									AND trs.rule_stop_status_type_id = ' . CRuleStopStatusType::PENDING . '
							) AS temp_rsr
						WHERE
							temp_rsr.row_num_2 = 1';

			if( false == $boolIsRoutingMaster && true == valArr( $arrintCompanyUserIds ) ) {

				$strSql .= ' AND (
									temp_rsr.company_group_id IN ( SELECT
																		cug.company_group_id
																	FROM
																		company_user_groups cug
																	WHERE
																		cug.company_user_id IN ( ' . implode( ',', array_filter( $arrintCompanyUserIds ) ) . ' )
																		AND cug.cid = ' . ( int ) $intCid . ' )
									OR
									temp_rsr.rule_stop_company_user_id IN ( ' . implode( ',', array_filter( $arrintCompanyUserIds ) ) . ' )
								) ';
			}
		}

		// Next
		if( 'next' == $strOrder ) {

			$strSql = 'SELECT
							temp_rsr.*
						FROM
							(
								SELECT 
									*,
									ROW_NUMBER() OVER ( PARTITION BY trs.reference_id, trs.property_id ORDER BY trs.order_num ) AS row_num_2
								FROM
									( ' . $strSql . ' ) AS trs
								WHERE
									trs.row_num_1 = 1
									AND trs.rule_stop_status_type_id = ' . CRuleStopStatusType::PENDING . '
							) AS temp_rsr
						WHERE
							temp_rsr.row_num_2 = 2';
		}

		// last Approved - scenario - If all rule stops approved and now we have to go to last stop [Invoices]
		if( 'last' == $strOrder ) {

			$strSql = 'SELECT
							temp_rsr.*
						FROM
							(
								SELECT 
									*,
									ROW_NUMBER() OVER ( PARTITION BY trs.reference_id, trs.property_id ORDER BY trs.order_num DESC, trs.id DESC ) AS row_num_2
								FROM
									( ' . $strSql . ' ) AS trs
								WHERE
									trs.row_num_1 = 1
									AND trs.rule_stop_status_type_id = ' . CRuleStopStatusType::APPROVED . '
							) AS temp_rsr
						WHERE
							temp_rsr.row_num_2 = 1';

		}

		// Previous
		if( 'prev' == $strOrder ) {

			$boolIsReturnKeyedArray = false;

			$strSql = 'SELECT
							cid,
							property_id,
							reference_id,
							route_type_id,
							rule_stop_id,
							order_num
						FROM
							(
								SELECT
									*,
									ROW_NUMBER() OVER ( PARTITION BY trs.reference_id, trs.property_id ' . $strOrderBy . ' ) AS row_num_2
								FROM
									( ' . $strSql . ' ) AS trs
								WHERE
									trs.row_num_1 = 1
									AND trs.rule_stop_status_type_id = ' . CRuleStopStatusType::APPROVED . '
							) AS temp_rsr
						WHERE
							' . $strOuterWhereCondition;

			$strSql = 'SELECT
							DISTINCT ON ( rs1.id, t_rss.property_id )
							rs1.cid,
							t_rss.property_id,
							t_rss.rule_stop_id,
							' . CRuleStopStatusType::PENDING . ' AS rule_stop_status_type_id,
							t_rss.reference_id,
							t_rss.route_type_id,
							rs1.order_num
						FROM
							rule_stops rs1
							JOIN ( ' . $strSql . ' ) as t_rss ON ( t_rss.cid = rs1.cid AND t_rss.rule_stop_id = rs1.id )
						WHERE
							rs1.cid = ' . ( int ) $intCid;
		}

		// First rule-stop - scenario - If transaction is created by system user and it is rejected then to send mail to first company user from approval route
		if( 'first' == $strOrder ) {

			$strSql = 'SELECT
							temp_rsr.*
						FROM
							(
								SELECT
									*,
									ROW_NUMBER() OVER ( PARTITION BY trs.reference_id, trs.property_id ORDER BY trs.order_num, trs.id ) AS row_num_2
								FROM
									( ' . $strSql . ' ) AS trs
								WHERE
									NOT trs.is_archived
							) temp_rsr
						WHERE
							temp_rsr.row_num_2 = 1';

		}

		return ( true == $boolIsFetchData ) ? ( array ) fetchData( $strSql, $objDatabase ) : parent::fetchRuleStopResults( $strSql, $objDatabase, $boolIsReturnKeyedArray );

	}

	public static function fetchRuleStopResultsByRouteTypeIdByReferenceIdByCid( $intRouteTypeId, $intReferenceId, $intCid, $objDatabase ) {

		$strSql = 'SELECT temp_rsr.* FROM
						(
							SELECT
								rs.route_id,
								rs.route_rule_id,
								rs.company_user_id AS rule_stop_company_user_id,
								rs.company_group_id,
								cg.name AS company_group_name,
								ce.name_first || \' \' || ce.name_last AS company_user_name,
								ce1.name_first || \' \' || ce1.name_last AS actual_company_user_name,
								rs.company_user_id AS rule_stop_company_user_id,
								p.property_name,
								rsr.*,
								ROW_NUMBER() OVER ( PARTITION BY rsr.reference_id, rsr.property_id, rsr.rule_stop_id ORDER BY rsr.id DESC ) as row_num
							FROM
								rule_stop_results rsr
								JOIN rule_stops rs ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
								LEFT JOIN company_groups cg ON ( cg.cid = rs.cid AND cg.id = rs.company_group_id )
								LEFT JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.company_user_id )
								LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
								LEFT JOIN company_users cu1 ON ( cu1.cid = rsr.cid AND cu1.id = rsr.company_user_id )
								LEFT JOIN company_employees ce1 ON ( ce1.cid = cu1.cid AND ce1.id = cu1.company_employee_id )
								JOIN properties p ON ( p.cid = rsr.cid AND p.id = rsr.property_id AND p.is_disabled = 0 )
							WHERE
								rsr.cid = ' . ( int ) $intCid . '
								AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
								AND rsr.reference_id = ' . ( int ) $intReferenceId . '
								AND NOT rsr.is_archived
							ORDER BY
								rsr.property_id, rsr.order_num
						) as temp_rsr
					WHERE
						temp_rsr.row_num = 1
					ORDER BY
						temp_rsr.property_id, temp_rsr.order_num';

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchActiveRuleStopResultsByReferenceIdsByRouteTypeIdByCid( $arrintReferenceIds, $intRouteTypeId, $intCid, $objDatabase, $boolIsAdvanceApproval  = false ) {

		if( false == valIntArr( $arrintReferenceIds ) ) return;

		$strWhere = $strJoin = $strOrderBy = '';
		if( true == $boolIsAdvanceApproval ) {
			$strJoin = 'JOIN approval_preferences apr ON ( rsr.cid = apr.cid AND rsr.route_type_id = apr.route_type_id )';
			$strWhere = 'AND apr.is_enabled';
			$strOrderBy = 'rsr.reference_id, rsr.id';
		} else {
			$strOrderBy = 'rsr.order_num, rsr.id';
		}

		$strSql = 'SELECT
						rsr.*
					FROM
						rule_stop_results rsr
						' . $strJoin . '
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rsr.reference_id IN ( ' . implode( ', ', $arrintReferenceIds ) . ' )
						AND NOT rsr.is_archived
						' . $strWhere . '
					ORDER BY
						' . $strOrderBy;

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchRoutesPropertyByRouteTypeIdByReferenceIdByCid( $arrintReferenceNumbers, $intCompanyUserId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintReferenceNumbers ) ) return;

		$strSql = 'SELECT
						pr.property_id
					FROM
						rule_stop_results rsr
						JOIN rule_stops rs ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
						LEFT JOIN property_routes pr ON ( pr.cid = rsr.cid AND pr.route_id = rs.route_id )
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rsr.route_type_id = ' . CRouteType::INVOICE . '
						AND rsr.reference_id IN ( ' . implode( ',', $arrintReferenceNumbers ) . ' )
						AND rs.company_user_id <> ' . ( int ) $intCompanyUserId;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchLastDeniedNotesByReferenceIdByRouteTypeIdByOrderNumByCid( $intReferenceId, $intRouteTypeId, $intOrderNum, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						sub.notes
					FROM
						(
							SELECT
								rsr.id,
								rsr.order_num,
								rsr.notes
							FROM
								rule_stop_results rsr
							WHERE
								rsr.cid = ' . ( int ) $intCid . '
								AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
								AND rsr.reference_id = ' . ( int ) $intReferenceId . '
								AND rsr.is_archived = false
								AND rsr.rule_stop_status_type_id = ' . CRuleStopStatusType::DENIED . '
							ORDER BY
								rsr.id DESC
							LIMIT
								1
						) sub
					WHERE
						sub.order_num > ' . ( int ) $intOrderNum;

		return self::fetchColumn( $strSql, 'notes', $objDatabase );
	}

	public static function fetchPendingRuleStopResultsByRouteTypeIdByCompanyUserIdByPropertyIdsByCid( $intRouteTypeId, $intCompanyUserId, $arrintPropertyIds, $intCid, $objDatabase, $boolFetchCount = false, $boolShowDisabledData = false ) {
		if( false == valId( $intCompanyUserId ) || false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) return NULL;

		$strFields = ( true == $boolFetchCount ) ? 'COUNT( 1 ) AS count' : 'temp_rsr.*';
		$strJoin = '';
		$strWhereCondition = '';

		if( CRouteType::ADJUSTMENTS == $intRouteTypeId ) {
			$strJoin = 'JOIN budget_adjustments ba ON ( ba.cid = rsr.cid AND ba.id = rsr.reference_id )
						JOIN ap_headers ah ON ( ah.cid = ba.cid AND ah.id = ba.from_ap_header_id )';
			$strWhereCondition = ' AND ah.ap_financial_status_type_id <> ' . ( int ) CApFinancialStatusType::CANCELLED . '
						AND ah.deleted_on IS NULL';
		} elseif( CRouteType::CHANGE_ORDERS == $intRouteTypeId ) {
			$strJoin = 'JOIN budget_change_orders bca ON ( bca.cid = rsr.cid AND bca.id = rsr.reference_id )';
			$strWhereCondition = ' AND bca.cancelled_on IS NULL';
		} elseif( CRouteType::JOB_BUDGET == $intRouteTypeId ) {
			$strJoin = 'JOIN jobs j ON ( j.cid = rsr.cid AND j.id = rsr.reference_id )';
			$strWhereCondition = ' AND j.cancelled_on IS NULL';
		}

		// Rule stop results with order num
		$strSql = 'SELECT
						rs.route_id,
						rs.route_rule_id,
						rs.company_group_id,
						rs.company_user_id As rule_stop_company_user_id,
						cg.name AS company_group_name,
						ce.name_first || \' \' || ce.name_last AS company_user_name,
						p.property_name,
						rsr.*,
						ROW_NUMBER() OVER ( PARTITION BY rsr.reference_id, rsr.property_id, rs.order_num ORDER BY rsr.id DESC ) as row_num_1,
						r.allow_notes_on_approval,
						r.require_notes_on_rejected_returned
					FROM
						rule_stop_results rsr
						JOIN rule_stops rs ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
						LEFT JOIN company_groups cg ON ( cg.cid = rs.cid AND cg.id = rs.company_group_id )
						LEFT JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.company_user_id )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
						JOIN properties p ON ( p.cid = rsr.cid AND p.id = rsr.property_id ' . ( ( false == $boolShowDisabledData ) ?  ' AND p.is_disabled = 0 ' : '' ) . ' )
						JOIN routes r ON ( r.cid = rs.cid AND r.id = rs.route_id )
						' . $strJoin . '
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rsr.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND NOT rsr.is_archived
						' . $strWhereCondition . '
					ORDER BY
						rsr.property_id, rs.order_num, rsr.order_num';

		$strSubSql = '( SELECT
							cu.id AS company_user_id
						FROM
							company_employee_relievers cer
							LEFT JOIN company_employees ce ON ( ce.id = cer.company_employee_id AND ce.cid = cer.cid )
							LEFT JOIN company_users cu ON ( cu.company_employee_id = ce.id AND ( cu.cid = ce.cid AND cu.default_company_user_id IS NULL  AND cu.company_user_type_id = ' . CCompanyUserType::ENTRATA . ' ) )
							LEFT JOIN company_users cu1 ON ( cu1.cid = cer.cid AND cu1.company_employee_id = cer.reliever_company_employee_id )
						WHERE
							cu1.id = ' . ( int ) $intCompanyUserId . '
							AND cer.cid = ' . ( int ) $intCid . '
							AND cer.deleted_on IS NULL
							AND ( date( cer.begin_datetime ) <= \'' . date( 'Y-m-d' ) . '\' AND date( cer.end_datetime ) >= \'' . date( 'Y-m-d' ) . '\')
						) UNION ( 
							SELECT company_user_id FROM ( VALUES( ' . ( int ) $intCompanyUserId . ' ) ) temp_company_users( company_user_id )
						)';

		$strSql = 'SELECT
						' . $strFields . '
					FROM
						(
							SELECT 
								*,
								ROW_NUMBER() OVER ( PARTITION BY trs.reference_id, trs.property_id ORDER BY trs.order_num ) AS row_num_2
							FROM	
								( ' . $strSql . ' ) AS trs
							WHERE
								trs.row_num_1 = 1
								AND trs.rule_stop_status_type_id = ' . CRuleStopStatusType::PENDING . '
						) AS temp_rsr
					WHERE
						temp_rsr.row_num_2 = 1
						AND (
								(
									SELECT EXISTS ( SELECT * FROM company_user_preferences WHERE cid = ' . ( int ) $intCid . ' AND company_user_id = ' . ( int ) $intCompanyUserId . ' AND key = \'APPROVAL_ROUTING_MASTER\')
								)
								OR ( temp_rsr.company_group_id IN ( SELECT
																	cug.company_group_id
																FROM
																	company_user_groups cug
																WHERE
																	cug.company_user_id IN ( ' . $strSubSql . ' )
																	AND cug.cid = ' . ( int ) $intCid . ' )
								)
								OR
								temp_rsr.rule_stop_company_user_id IN ( ' . $strSubSql . ' )
						)';

		if( true == $boolFetchCount ) {
			return fetchData( $strSql, $objDatabase )[0]['count'];
		} else {
			return self::fetchRuleStopResults( $strSql, $objDatabase );
		}
	}

	public static function fetchRuleStopResultNotesByReferenceIdByRouteTypeIdByCid( $intReferenceId, $intRouteTypeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intReferenceId ) || false == valId( $intCid ) || false == valId( $intRouteTypeId ) ) return NULL;

		$strSql = '
				SELECT
					sub.*
				FROM
					(
					SELECT
						rsr.id,
						ce.name_first || \' \' || ce.name_last AS company_user_name,
						RANK ( ) over ( partition BY rsr.rule_stop_status_type_id
					ORDER BY
						rsr.id DESC ) AS rank,
						rsr.notes,
						rsr.rule_stop_status_type_id
					FROM
						rule_stop_results rsr
						LEFT JOIN company_users cu ON ( cu.cid = rsr.cid AND cu.id = rsr.company_user_id )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rsr.reference_id = ' . ( int ) $intReferenceId . '
						AND NOT rsr.is_archived
						AND rsr.rule_stop_status_type_id IN ( ' . CRuleStopStatusType::DENIED . ',' . CRuleStopStatusType::APPROVED . ' )
					) AS sub
				WHERE
					sub.rank = 1;';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchAllUnAssociatedPropertiesPendingRuleStopsByRouteTypeIdByReferenceIdByCid( $intRouteTypeId, $intReferenceId, $intAllPropertyGroupid, $intCid, $objDatabase ) {

		$strSql = 'SELECT rs.route_id,
			rs.route_rule_id,
			rs.company_group_id,
			rs.company_user_id As rule_stop_company_user_id,
			cg.name AS company_group_name,
			ce.name_first || \' \' || ce.name_last AS company_user_name,
			cu.is_administrator,
			p.property_name,
			CASE 
				WHEN rs.company_group_id IS NULL AND rs.company_user_id IS NOT NULL AND cupg.property_group_id IS NOT NULL
					THEN 1
				WHEN rs.company_user_id IS NULL AND rs.company_group_id IS NOT NULL AND cgpg.property_group_id IS NOT NULL
					THEN 1
				ELSE 0 
			END As rule_stop_associate_property,
			rsr.*,
			ROW_NUMBER() OVER ( PARTITION BY rsr.reference_id, rsr.property_id, rs.order_num ORDER BY rsr.id DESC ) as row_num_1
		FROM
			rule_stop_results rsr
			JOIN rule_stops rs ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
			LEFT JOIN company_groups cg ON ( cg.cid = rs.cid AND cg.id = rs.company_group_id )
			LEFT JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.company_user_id )
			LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
			JOIN properties p ON ( p.cid = rsr.cid AND p.id = rsr.property_id AND p.is_disabled = 0 )
			LEFT JOIN company_user_property_groups cupg ON ( cupg.cid =rsr.cid AND cupg.company_user_id = rs.company_user_id AND ( cupg.property_group_id = rsr.property_id OR cupg.property_group_id = ' . ( int ) $intAllPropertyGroupid . ' ) )
			LEFT JOIN company_group_property_groups cgpg ON ( cgpg.cid = rsr.cid AND cgpg.company_group_id = rs.company_group_id AND ( cgpg.property_group_id = rsr.property_id OR cgpg.property_group_id = ' . ( int ) $intAllPropertyGroupid . ' ) )
		WHERE
			rsr.cid = ' . ( int ) $intCid . '
			AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
			AND rsr.reference_id = ' . ( int ) $intReferenceId . '
			AND NOT rsr.is_archived
		ORDER BY
			rsr.property_id, rs.order_num, rsr.order_num';

		$strSql = 'SELECT 
				rsr1.*
			FROM
				( ' . $strSql . ' ) As rsr1
			WHERE
				rsr1.row_num_1 = 1
				AND rsr1.is_administrator = 0
				AND rsr1.rule_stop_status_type_id = ' . CRuleStopStatusType::PENDING . '
				AND rsr1.rule_stop_associate_property = 0';

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchIsFirstApprovalUserUserByRouteTypeIdByLeaseIdByCid( $intRouteTypeId, $intLeaseId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						CASE 
							WHEN 0 < MIN( order_num ) OR MIN ( order_num ) IS NULL THEN false 
							ELSE true 
						END AS is_first_approval_user
					FROM
					  (
						SELECT
							rsr.cid,
							rsr.order_num,
							rsr.reference_id,
							rsr.rule_stop_status_type_id,
							ROW_NUMBER() OVER ( PARTITION BY rsr.cid, rsr.reference_id, rsr.order_num ORDER BY rsr.id DESC ) as row_num
						FROM
							rule_stop_results rsr
							JOIN ar_approval_requests aar ON( aar.cid = rsr.cid AND aar.id = rsr.reference_id )
						WHERE
							rsr.cid = ' . ( int ) $intCid . '
							AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
							AND rsr.is_archived = FALSE
							AND aar.lease_id = ' . ( int ) $intLeaseId . '
							AND aar.is_archived = FALSE
					  ) AS temp_rsr
					WHERE
						temp_rsr.rule_stop_status_type_id = ' . CRuleStopStatusType::PENDING . '
						AND temp_rsr.row_num = 1';

		return parent::fetchColumn( $strSql, 'is_first_approval_user', $objDatabase );
	}

	public static function fetchRequiredRuleStopResultsCountByReferenceIdsByRouteTypeIdByCid( $arrintApHeaders, $intRouteTypeId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApHeaders ) ) return 0;

		$strSql = 'SELECT
						rsr.*
					FROM 
						rule_stop_results rsr
						JOIN rule_stops rs ON( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
						JOIN routes r ON( r.cid = rs.cid AND r.id = rs.route_id )
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rsr.reference_id IN( ' . sqlIntImplode( $arrintApHeaders ) . ' )
						AND r.require_notes_on_rejected_returned = TRUE
					';

		$arrobjRuleStopResults = parent::fetchRuleStopResults( $strSql, $objDatabase );

		if( true == valArr( $arrobjRuleStopResults ) ) {
			return \Psi\Libraries\UtilFunctions\count( $arrobjRuleStopResults );
		}
		return 0;
	}

	public static function fetchCustomRuleStopResultsByLeaseIdByCid( int $intLeaseId, int $intCid, CDatabase $objDatabase, array $arrintRuleStopStatusTypeIds = [], $intArTransactionId = NULL, $intArApprovalRequestId = NULL ) : array {

		$strWhereCondition = ( true == valArr( $arrintRuleStopStatusTypeIds ) ) ? ' AND rsr.rule_stop_status_type_id IN ( ' . implode( ', ', $arrintRuleStopStatusTypeIds ) . ' ) ' : '';

		$strWhereCondition .= ( true == valId( $intArTransactionId ) ) ? ' AND apr.ar_transaction_id = ' . ( int ) $intArTransactionId : '';

		$strWhereCondition .= ( true == valId( $intArApprovalRequestId ) ) ? ' AND apr.id = ' . ( int ) $intArApprovalRequestId : '';

		$strSql = 'SELECT
						rsr.id,
						rsr.rule_stop_id,
						ce.name_first || \' \' || ce.name_last AS company_user_name,
						rsr.rule_stop_status_type_id,
						rsr.notes,
						rsr.created_on,
						rsr.updated_on
					FROM
						rule_stop_results rsr
						JOIN ar_approval_requests apr ON ( apr.cid = rsr.cid AND apr.id = rsr.reference_id )
						JOIN company_users cu ON ( cu.cid = rsr.cid AND cu.id = rsr.company_user_id )
						JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
					WHERE
						apr.cid = ' . ( int ) $intCid . '
						AND apr.lease_id = ' . ( int ) $intLeaseId . '
						AND rsr.route_type_id = ' . ( int ) CRouteType::TRANSACTIONS . '
						' . $strWhereCondition . ' 
					ORDER BY
						rsr.updated_on DESC;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTotalPendingRuleStopResultsCountsByReferenceIdsByRouteTypeIdByCid( $arrintReferenceIds, $intRouteTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintReferenceIds ) || false == valId( $intRouteTypeId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'WITH total_results AS (

						SELECT
							rsr.*,
							rs.order_num,
							rs.route_id,
							rs.route_rule_id,
							ROW_NUMBER() OVER ( PARTITION BY rsr.rule_stop_id, rsr.property_id, rsr.reference_id ORDER BY rsr.id DESC ) as row_num
						FROM
							rule_stop_results rsr
							JOIN rule_stops rs ON ( rs.cid = rsr.cid AND rs.id = rsr.rule_stop_id )
						WHERE
							rsr.cid = ' . ( int ) $intCid . '
							AND NOT rsr.is_archived
							AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
							AND rsr.reference_id IN ( ' . sqlIntImplode( $arrintReferenceIds ) . ' )

					), total_rule_stops AS (

						SELECT
							tr.cid,
							tr.reference_id,
							COUNT(tr.id) AS rule_stop_count
						FROM
							total_results tr
						WHERE
							tr.cid = ' . ( int ) $intCid . '
							AND tr.row_num = 1
						GROUP BY
							tr.cid,
							tr.reference_id

					), actual_approved_stops AS (

						SELECT
							tr.cid,
							tr.reference_id,
							COUNT(tr.id) AS approved_count
						FROM
							total_results tr
						WHERE
							tr.cid = ' . ( int ) $intCid . '
							AND tr.row_num = 1
							AND tr.rule_stop_status_type_id = ' . CRuleStopStatusType::APPROVED . '
						GROUP BY
							tr.cid,
							tr.reference_id

					)

					SELECT
						trs.reference_id,
						( trs.rule_stop_count - COALESCE( aas.approved_count, 0 ) ) as pending_routes_count
					FROM
						total_rule_stops trs
						LEFT JOIN actual_approved_stops aas ON ( aas.cid = trs.cid AND aas.reference_id = trs.reference_id )
					WHERE
						trs.rule_stop_count <> COALESCE( aas.approved_count, 0 );';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllRuleStopResultsByReferenceIdsByCid( array $arrintReferenceIds, int $intCid, CDatabase $objDatabase ) {

		if( false == valIntArr( $arrintReferenceIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						rsr.*
					FROM
						rule_stop_results rsr
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND rsr.reference_id IN ( ' . implode( ', ', $arrintReferenceIds ) . ' )
						AND rsr.is_archived IS FALSE ';

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchCustomRuleStopResultsByReferenceIdByCid( int $intReferenceId, int $intCid, CDatabase $objDatabase, array $arrintRuleStopStatusTypeIds = [], $intRouteTypeId = CRouteType::TRANSACTIONS ) : array {

		$strWhereCondition	= ( true == valArr( $arrintRuleStopStatusTypeIds ) ) ? ' AND rsr.rule_stop_status_type_id IN ( ' . implode( ', ', $arrintRuleStopStatusTypeIds ) . ' ) ' : '';

		$strSql = 'WITH current_rule_stop_results AS (
							SELECT
								MAX( rsr.id ) AS id,
								rsr.cid
							FROM
								rule_stop_results rsr
								JOIN ar_approval_requests apr ON ( apr.cid = rsr.cid AND apr.id = rsr.reference_id )
							WHERE
								apr.cid = ' . ( int ) $intCid . '
								AND apr.id = ' . ( int ) $intReferenceId . '
								AND apr.route_type_id = ' . ( int ) $intRouteTypeId . '
								AND rsr.is_archived = FALSE
								' . $strWhereCondition . '
							GROUP BY
								rsr.order_num,
								rsr.cid
							)

					SELECT
						( rsr_new.order_num + 1 ) as row_num,
						ce.NAME_FIRST || \' \' || ce.name_last AS company_user_name,
						util_get_translated( \'name\', cg.name, cg.details ) AS company_group_name,
						ce_approved_by.NAME_FIRST || \' \' || ce_approved_by.name_last AS approved_by,
						rsr_new.id,
						rsr_new.notes,
						rsr_new.rule_stop_status_type_id
					FROM
						rule_stop_results rsr_new
						JOIN current_rule_stop_results crsr ON( crsr.cid = rsr_new.cid AND rsr_new.id IN ( crsr.id ) )
						JOIN rule_stops rs ON ( rs.cid = rsr_new.cid AND rs.id = rsr_new.rule_stop_id )
						LEFT JOIN company_users cu ON ( cu.cid = rs.cid AND cu.id = rs.company_user_id )
						LEFT JOIN company_employees ce ON ( ce.cid = cu.cid AND ce.id = cu.company_employee_id )
						LEFT JOIN company_groups cg ON ( cg.cid = rs.cid AND cg.id = rs.company_group_id )
						/* JOIN\'s to get ApprovedBy User details */
						LEFT JOIN company_users cu_approved_by ON ( cu_approved_by.cid = rsr_new.cid AND cu_approved_by.id = rsr_new.company_user_id AND rsr_new.rule_stop_status_type_id = ' . CRuleStopStatusType::APPROVED . '  )
						LEFT JOIN company_employees ce_approved_by ON ( ce_approved_by.cid = cu_approved_by.cid AND ce_approved_by.id = cu_approved_by.company_employee_id )
					ORDER BY 
						rsr_new.order_num';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomRuleStopResultsByOrderNumRangeByRouteTypeIdByReferenceIdByCid( int $intMinOrderNum, int $intMaxOrderNum, int $intRouteTypeId, int $intArApprovalRequestId, int $intCid, CDatabase $objDatabase ) : array {

		$strSql = ' SELECT 
						sub_query.*
					FROM
						(  SELECT
							rsr.*,
							rank() OVER ( PARTITION BY rsr.order_num ORDER BY rsr.id DESC ) as rank
							FROM
								rule_stop_results rsr
								JOIN ar_approval_requests apr ON ( apr.cid = rsr.cid AND apr.id = rsr.reference_id )
							WHERE
								apr.cid = ' . ( int ) $intCid . '
								AND apr.id = ' . ( int ) $intArApprovalRequestId . '
								AND rsr.route_type_id = ' . ( int ) $intRouteTypeId . '
								AND rsr.order_num BETWEEN ' . ( int ) $intMinOrderNum . ' AND ' . ( int ) $intMaxOrderNum . '
								AND apr.is_archived = FALSE
						) AS sub_query
					WHERE
						sub_query.cid = ' . ( int ) $intCid . '
						AND sub_query.rank = 1 ';

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchPendingRuleStopResultsByReferenceIdsByRouteTypeIdByCid( array $arrintReferenceIds, int $intRouteTypeId, int $intCid, CDatabase $objDatabase, $arrstrFieldsToBeSelected = [] ) {

		if( false == valArr( $arrintReferenceIds ) || false == valId( $intRouteTypeId ) || false == valId( $intCid ) ) {
			return false;
		}

		$strFieldsToBeSelected = '*';

		if( true == valArr( $arrstrFieldsToBeSelected ) ) {
			$strFieldsToBeSelected = implode( ',', $arrstrFieldsToBeSelected );
		}

		$strSql = 'SELECT
						' . $strFieldsToBeSelected . '
					FROM
						rule_stop_results
					WHERE
						cid = ' . ( int ) $intCid . '
						AND reference_id IN ( ' . implode( ',', $arrintReferenceIds ) . ' )
						AND route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rule_stop_status_type_id = ' . ( int ) CRuleStopStatusType::PENDING . ' ';

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchActiveRuleStopResultsOfReferencesByRouteTypeIdsByCid( $arrintRouteTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintRouteTypeIds ) ) return;

		$strSql = ' DROP TABLE IF EXISTS pg_temp.load_pending_rule_stop_results;
					CREATE TEMP TABLE load_pending_rule_stop_results AS (
							SELECT
								DISTINCT ON( trs.reference_id, trs.route_type_id )
								trs.cid,
								trs.reference_id,
								trs.route_type_id
							FROM (
									SELECT
										rsr_1.cid,
										rsr_1.reference_id,
										rsr_1.route_type_id,
										rsr_1.rule_stop_status_type_id,
										ROW_NUMBER() OVER(PARTITION BY rsr_1.reference_id, rsr_1.property_id, rsr_1.order_num ORDER BY rsr_1.id DESC) as row_num_1
									FROM
										rule_stop_results rsr_1
									WHERE
										rsr_1.cid = ' . ( int ) $intCid . '
										AND rsr_1.route_type_id IN ( ' . implode( ',', $arrintRouteTypeIds ) . ' )
										AND NOT rsr_1.is_archived
									ORDER BY
										rsr_1.property_id,
										rsr_1.order_num
								) As trs
							WHERE
								trs.row_num_1 = 1
								AND trs.rule_stop_status_type_id = ' . CRuleStopStatusType::PENDING . '
					);

					CREATE INDEX idx_load_pending_rule_stop_results_master ON pg_temp.load_pending_rule_stop_results USING btree( cid, reference_id );
					ANALYZE pg_temp.load_pending_rule_stop_results;

					SELECT
						rsr.*
					FROM
						rule_stop_results rsr
						JOIN load_pending_rule_stop_results pending_rsr ON rsr.cid = pending_rsr.cid AND rsr.reference_id = pending_rsr.reference_id AND rsr.route_type_id = pending_rsr.route_type_id
					WHERE
						rsr.cid = ' . ( int ) $intCid . '
						AND NOT rsr.is_archived';

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

	public static function fetchActiveRuleStopResultsByReferenceIdsByRouteTypeId( array $arrintReferenceIds, int $intRouteTypeId, CDatabase $objDatabase ) {

		$strSql = 'SELECT
						id
					FROM
						rule_stop_results
					WHERE
						route_type_id = ' . ( int ) $intRouteTypeId . '
						AND reference_id IN ( ' . implode( ',', $arrintReferenceIds ) . ' )
						AND is_archived IS FALSE';

		return parent::fetchRuleStopResults( $strSql, $objDatabase );
	}

}
?>