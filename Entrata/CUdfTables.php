<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CUdfTables
 * Do not add any new functions to this class.
 */

class CUdfTables extends CBaseUdfTables {

	/**
	 * @return CUdfTable
	 */
	public static function fetchUdfTableByNameByCid( $strName, $intCid, $objDatabase ) {
		return self::fetchUdfTable( sprintf( 'SELECT * FROM udf_tables WHERE name = \'%s\' AND cid = %d', ( string ) $strName, ( int ) $intCid ), $objDatabase );
	}

}
?>