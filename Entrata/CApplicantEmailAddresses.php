<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApplicantEmailAddresses
 * Do not add any new functions to this class.
 */

class CApplicantEmailAddresses extends CBaseApplicantEmailAddresses {

	public static function fetchApplicantEmailAddressByApplicantIdByEmailAddressTypeIdByCid( $intApplicantId, $intEmailAddressTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM applicant_email_addresses WHERE applicant_id =' . ( int ) $intApplicantId . ' AND cid = ' . ( int ) $intCid . ' AND email_address_type_id = ' . ( int ) $intEmailAddressTypeId . ' LIMIT 1';
		return self::fetchApplicantEmailAddress( $strSql, $objDatabase );
	}
}
?>