<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CTaxTypes
 * Do not add any new functions to this class.
 */

class CTaxTypes extends CBaseTaxTypes {

	const TAX_TYPE_PROPERTY_SALES_TAX = 1;

	public static function fetchTaxTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTaxType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTaxType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTaxType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}
}
?>