<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CSubsidyHapRequests
 * Do not add any new functions to this class.
 */

class CSubsidyHapRequests extends CBaseSubsidyHapRequests {

	public static function fetchPaginatedSubsidyHapRequestsByCidBySubsidyHapRequestFilter( $intOffset, $intPageSize, $arrmixSubsidyHapRequestsFilter, $intCid, $objDatabase ) {

		if( false === valArr( $arrmixSubsidyHapRequestsFilter ) ) return NULL;

		$intMonth					= NULL;
		$intYear					= NULL;
		$intLimit					= ( false == is_null( $intPageSize ) ) ? ( int ) $intPageSize : NULL;
		$strOrderBy					= 'voucher_month';
		$strSortDir					= 'DESC';
		$strPropertyGroupJoin		= '';

		if( true == valArr( $arrmixSubsidyHapRequestsFilter ) ) {

			$strMinMonthYear		= ( true == isset( $arrmixSubsidyHapRequestsFilter['min_month_year'] ) ) ? date( 'Y-m-d', strtotime( $arrmixSubsidyHapRequestsFilter['min_month_year'] ) ) : NULL;
			$strMaxMonthYear		= ( true == isset( $arrmixSubsidyHapRequestsFilter['max_month_year'] ) ) ? date( 'Y-m-d', strtotime( $arrmixSubsidyHapRequestsFilter['max_month_year'] ) ) : NULL;

			$strSortDir = ( true == isset( $arrmixSubsidyHapRequestsFilter['sort_dir'] ) ) ? $arrmixSubsidyHapRequestsFilter['sort_dir'] : NULL;

			$arrintPropertyGroupIds		= ( true == isset( $arrmixSubsidyHapRequestsFilter['property_group_ids'] ) ) ? $arrmixSubsidyHapRequestsFilter['property_group_ids'] : NULL;

			if( true == valArr( $arrintPropertyGroupIds ) ) {
				$strPropertyGroupJoin = 'JOIN load_properties ( ARRAY[' . ( int ) $intCid . '], ARRAY [ ' . implode( ',', ( array ) $arrintPropertyGroupIds ) . ' ] ) AS lp ON ( lp.cid = p.cid AND lp.is_disabled = 0 AND lp.property_id = p.id ) ';
			}
		}

		if( true == isset( $arrmixSubsidyHapRequestsFilter['sort_by'] ) ) {
			$strOrderBy = $arrmixSubsidyHapRequestsFilter['sort_by'];
		}

		$strWhereCondition = ( true == valId( $intMonth ) ) ? ' AND extract( month from shr.voucher_date ) = ' . $intMonth : '';
		$strWhereCondition .= ( true == valId( $intYear ) ) ? ' AND extract( year from shr.voucher_date ) = ' . $intYear : '';
		$strWhereCondition .= ( true == valArr( $arrmixSubsidyHapRequestsFilter['status'] ) ) ? ' AND shr.subsidy_hap_request_status_type_id in ( ' . implode( ',', ( array ) $arrmixSubsidyHapRequestsFilter['status'] ) . ')': '';
		$strWhereCondition .= ( true == valArr( $arrmixSubsidyHapRequestsFilter['subsidy_contract_types'] ) ) ? ' AND sc.subsidy_contract_type_id in ( ' . implode( ',', ( array ) $arrmixSubsidyHapRequestsFilter['subsidy_contract_types'] ) . ')': '';

		if( true == valStr( $strMinMonthYear ) || true == valStr( $strMaxMonthYear ) ) {

			if( true == valStr( $strMinMonthYear ) && false == valStr( $strMaxMonthYear ) ) {
				$strWhereCondition .= ' AND ( shr.voucher_date >= \'' . $strMinMonthYear . '\' )';
			}

			if( true == valStr( $strMaxMonthYear ) && false == valStr( $strMinMonthYear ) ) {
				$strWhereCondition .= ' AND ( shr.voucher_date <= \'' . $strMaxMonthYear . '\' )';
			}

			if( true == valStr( $strMinMonthYear ) && true == valStr( $strMaxMonthYear ) ) {
				$strWhereCondition .= ' AND ( DATE_TRUNC( \'day\', shr.voucher_date ) BETWEEN \'' . $strMinMonthYear . '\' AND \'' . $strMaxMonthYear . '\' )';
			}

		}

		$strSql = 'SELECT
						extract( year from shr.voucher_date ) as voucher_year,
						extract( month from shr.voucher_date ) as voucher_month,
						shrst.name as status,
						shrst.id as subsidy_hap_request_status_type_id,
						p.property_name,
						sct.name as contract_name,
						shr.id,
						shr.assistance_payment_amount,
						shr.adjustment_payment_amount,
						shr.repayment_agreement_amount,
						shr.miscellaneous_request_amount,
						shr.special_claims_amount,
						shr.total_amount,
						sc.contract_number
					FROM
						properties as p
					' . $strPropertyGroupJoin . '
						JOIN subsidy_hap_requests as shr ON ( p.cid = shr.cid AND p.id = shr.property_id )
						JOIN subsidy_hap_request_status_types as shrst ON ( shrst.id =  shr.subsidy_hap_request_status_type_id )
						JOIN subsidy_contracts sc ON ( sc.cid = shr.cid AND sc.property_id = shr.property_id AND sc.id = shr.subsidy_contract_id )
						JOIN subsidy_contract_types sct ON ( sct.id = sc.subsidy_contract_type_id )
					WHERE
						shrst.is_published = TRUE
						AND p.cid = ' . ( int ) $intCid . '
						AND shr.deleted_by IS NULL
						AND shr.deleted_on IS NULL
						' . $strWhereCondition . '
					ORDER BY
					' . $strOrderBy . '
							' . $strSortDir;

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset . ' ';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyHapRequestCountByPropertyIdBySubsidyContractIdByVoucherDateBySubsidyHapRequestStatusIdsByCid( $intPropertyId, $intSubsidyContractId, $strVoucherDate, $arrintSubsidyHapRequestStatusIds, $intCid, $objDatabase, $intSubsidyHapRequestId = NULL ) {

		if( false == valId( $intPropertyId ) || false == valId( $intSubsidyContractId ) || false == valStr( $strVoucherDate ) || false == valArr( $arrintSubsidyHapRequestStatusIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					COUNT( shr.id ) AS count,
					shr.id as subsidy_hap_request_id
				FROM
					subsidy_hap_requests shr
				WHERE
					shr.cid = ' . ( int ) $intCid . '
					AND shr.property_id = ' . ( int ) $intPropertyId . '
					AND shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
					AND shr.subsidy_hap_request_status_type_id IN ( ' . implode( ',', $arrintSubsidyHapRequestStatusIds ) . ' )
					AND shr.voucher_date = \'' . trim( date( 'm-d-Y', strtotime( $strVoucherDate ) ) ) . '\'
					AND shr.deleted_by IS NULL
					AND shr.deleted_on IS NULL';

		$strSql .= ( true == valId( $intSubsidyHapRequestId ) ) ? ' AND shr.id <> ' . ( int ) $intSubsidyHapRequestId : '';
		$strSql .= ' GROUP BY
					shr.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyHapRequestByIdByCid( $intId, $intCid, $objDatabase ) {

		if( false == valId( $intId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					shr.*,
					p.property_name,
					sct.id AS subsidy_contract_type_id,
					sct.name AS subsidy_contract_type_name,
					sc.contract_number
				FROM
					subsidy_hap_requests shr
					JOIN properties p ON ( p.cid = shr.cid AND p.id = shr.property_id )
					JOIN subsidy_contracts sc ON ( sc.cid = shr.cid AND sc.property_id = shr.property_id AND sc.id = shr.subsidy_contract_id )
					JOIN subsidy_contract_types sct ON ( sct.id = sc.subsidy_contract_type_id )
				WHERE
					shr.id = ' . ( int ) $intId . '
					AND shr.cid = ' . ( int ) $intCid . '
					AND shr.deleted_by IS NULL
					AND shr.deleted_on IS NULL';

		return parent::fetchObject( $strSql, 'CSubsidyHapRequest', $objDatabase );
	}

	public static function fetchSubsidyVoucherDateByPropertyIdBySubsidyContractIdByCid( $intPropertyId, $intSubsidyContractId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intSubsidyContractId ) ) return NULL;

		$strSql = '
				SELECT
					shr.voucher_date + interval \'1\' month AS subsidy_voucher_date
				FROM
					subsidy_hap_requests shr
					JOIN subsidy_contracts sc ON ( sc.cid = shr.cid AND sc.property_id = shr.property_id AND sc.id = shr.subsidy_contract_id AND date_trunc( \'month\', shr.voucher_date ) <= date_trunc( \'month\', sc.end_date ) )
				WHERE
					shr.cid = ' . ( int ) $intCid . '
					AND shr.property_id = ' . ( int ) $intPropertyId . '
					AND shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
					AND shr.deleted_by IS NULL
					AND shr.deleted_on IS NULL
				ORDER BY
					shr.id DESC
				LIMIT 1';

		return self::fetchColumn( $strSql, 'subsidy_voucher_date', $objDatabase );
	}

	public static function fetchSubsidyHapVoucherDataByPropertyIdByCid( $intPropertyId,  $intSubsidyContractId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						shr.id,
						shr.voucher_date,
						shr.total_amount
					FROM
						subsidy_hap_requests shr
					WHERE
						shr.property_id = ' . ( int ) $intPropertyId . '
						AND shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
						AND shr.cid = ' . ( int ) $intCid . '
						AND shr.subsidy_hap_request_status_type_id = ' . ( int ) CSubsidyHapRequestStatusType::FINALIZED;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSubsidyHapRequestsByIdsByCid( $arrintSubsidyhapRequestIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSubsidyhapRequestIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						subsidy_hap_requests shr
					WHERE
						shr.id IN (' . implode( ',', $arrintSubsidyhapRequestIds ) . ')
						AND shr.cid = ' . ( int ) $intCid . '
						AND shr.deleted_by IS NULL
						AND shr.deleted_on IS NULL';

		return self::fetchSubsidyHapRequests( $strSql, $objDatabase );
	}

	public static function fetchClientDetailsBySubsidyRequestHapIdByCid( $intSubsidyRequestHapId, $intCid, $objDatabase ) {
		if( false == valId( $intSubsidyRequestHapId ) || false == valId( $intCid ) ) return NULL;

		$strSql = '
				SELECT
					c.company_name,
					ale.tax_number_encrypted AS company_ein
				FROM
					subsidy_hap_requests AS shr
					JOIN clients AS c ON ( c.id = shr.cid )
					JOIN property_group_associations AS pga ON ( pga.cid = shr.cid AND pga.property_id = shr.property_id )
					JOIN property_groups AS pg ON ( pg.cid = shr.cid AND pg.id = pga.property_group_id  AND pg.system_code = \'ALL\')
					JOIN ap_payee_property_groups AS appg ON ( appg.cid = pga.cid AND appg.property_group_id = pga.property_group_id )
					JOIN ap_payees AS ap_client ON ( ap_client.cid = appg.cid AND ap_client.id = appg.ap_payee_id AND ap_client.ap_payee_type_id = ' . CApPayeeType::INTERCOMPANY . ' )
					JOIN ap_payee_locations AS parent_company_apl ON ( parent_company_apl.cid = ap_client.cid AND parent_company_apl.ap_payee_id = ap_client.id AND ap_client.ap_payee_type_id = ' . CApPayeeType::INTERCOMPANY . ' AND parent_company_apl.is_primary = TRUE AND parent_company_apl.disabled_on IS NULL AND parent_company_apl.deleted_on IS NULL )
					JOIN ap_legal_entities AS ale ON ( ale.cid = ap_client.cid AND ale.ap_payee_id = ap_client.id AND ap_client.ap_payee_type_id = ' . CApPayeeType::INTERCOMPANY . ' )
				WHERE
					shr.cid = ' . ( int ) $intCid . '
					AND shr.id  = ' . ( int ) $intSubsidyRequestHapId . '
					AND ale.tax_number_encrypted IS NOT NULL
					AND shr.deleted_by IS NULL
					AND shr.deleted_on IS NULL
					AND pg.deleted_by IS NULL
					AND pg.deleted_on IS NULL
					AND ale.deleted_by IS NULL
					AND ale.deleted_on IS NULL
				GROUP BY
					c.company_name,
					ale.tax_number_encrypted';

		$arrmixData = fetchData( $strSql, $objDatabase );
		return ( ( true == valArr( $arrmixData ) )? $arrmixData[0] : NULL );
	}

	public static function fetchVoucherhrDataByCertificationIdsByCid( $arrintSubsidyHapRequestIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSubsidyHapRequestIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = '
			SELECT
				c.company_name AS sender_name,
				pa.street_line1 AS sender_street_address,
				pa.city AS sender_city_name,
				pa.state_code AS sender_state,
				pa.postal_code AS sender_zip_code,
				psd.hud_project_name AS hud_project_name,
				subcontract.subsidy_contract_type_id AS subsidy_type,
				psd.hud_project_number AS hud_project_number,
				subcontract.contract_number,
				CONVERT_FROM( DECODE( psd.hud_project_imax_id_encrypted, \'BASE64\' ), \'UTF-8\' ) AS project_imax_id,
				owner_apl.duns_number AS owner_duns_number,
				parent_company_apl.duns_number AS parent_company_duns_number,
				o.tax_number_encrypted AS owner_tin,
				ale.tax_number_encrypted AS parent_company_tin,
				stv.name AS subsidy_tracs_version_name
			FROM
				subsidy_hap_requests AS shr
				JOIN clients AS c ON ( c.id = shr.cid )
				JOIN subsidy_contracts AS subcontract ON ( subcontract.cid = shr.cid AND subcontract.id = shr.subsidy_contract_id )
				JOIN property_subsidy_details AS psd ON ( psd.cid = shr.cid AND psd.property_id = shr.property_id )
				JOIN subsidy_tracs_versions AS stv ON ( stv.id = psd.subsidy_tracs_version_id )
				JOIN property_addresses AS pa ON ( pa.cid = shr.cid AND pa.property_id = shr.property_id AND pa.address_type_id = ' . CAddressType::PRIMARY . ' AND pa.is_alternate = false )
				JOIN properties AS p ON ( p.cid = shr.cid AND p.id = shr.property_id )
				JOIN owners AS o ON ( o.cid = shr.cid AND o.id = p.owner_id )
				JOIN ap_payees AS ap ON ( ap.cid = o.cid AND ap.id = o.ap_payee_id AND  ap.ap_payee_type_id = ' . CApPayeeType::OWNER . ' )
   				JOIN ap_payee_locations AS owner_apl ON ( owner_apl.cid = ap.cid AND owner_apl.ap_payee_id = ap.id AND  owner_apl.is_primary = TRUE AND owner_apl.disabled_on IS NULL AND owner_apl.deleted_on IS NULL )
				JOIN property_group_associations AS pga ON ( pga.cid = shr.cid AND pga.property_id = shr.property_id )
				JOIN property_groups AS pg ON ( pg.cid = shr.cid AND pg.id = pga.property_group_id  AND pg.system_code = \'ALL\')
				JOIN ap_payee_property_groups AS appg ON ( appg.cid = pga.cid AND appg.property_group_id = pga.property_group_id )
				LEFT JOIN ap_payees AS ap_client ON ( ap_client.cid = appg.cid AND ap_client.id = appg.ap_payee_id AND ap_client.ap_payee_type_id = ' . CApPayeeType::INTERCOMPANY . ' AND ap_client.is_system = true )
				LEFT JOIN ap_payee_locations AS parent_company_apl ON ( parent_company_apl.cid = ap_client.cid AND parent_company_apl.ap_payee_id = ap_client.id AND ap_client.ap_payee_type_id = ' . CApPayeeType::INTERCOMPANY . ' AND parent_company_apl.is_primary = TRUE AND parent_company_apl.disabled_on IS NULL AND parent_company_apl.deleted_on IS NULL )
				LEFT JOIN ap_legal_entities AS ale ON ( ale.cid = ap_client.cid AND ale.ap_payee_id = ap_client.id AND ap_client.ap_payee_type_id = ' . CApPayeeType::INTERCOMPANY . ' )
			WHERE
				shr.id IN (' . implode( ',', $arrintSubsidyHapRequestIds ) . ')
				AND shr.cid = ' . ( int ) $intCid . '
				AND pg.deleted_by IS NULL
				AND pg.deleted_on IS NULL
				AND ale.deleted_by IS NULL
				AND ale.deleted_on IS NULL
			GROUP BY
	 			c.company_name,
				pa.street_line1,
				pa.city,
				pa.state_code,
				pa.postal_code,
			 	psd.hud_project_name,
  				subcontract.subsidy_contract_type_id,
  				psd.hud_project_number,
  				subcontract.contract_number,
 				psd.hud_project_imax_id_encrypted,
   				o.tax_number_encrypted,
				ale.tax_number_encrypted,
   				parent_company_apl.duns_number,
  				owner_apl.duns_number,
  				stv.name';

		$arrmixTenantHrRecordData = fetchData( $strSql, $objDatabase );

		return $arrmixTenantHrRecordData[0];
	}

	public static function fetchSubsidyContractTypeIdBySubsidyHapRequestIdByCID( $intSubsidyHapRequestId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						sc.subsidy_contract_type_id AS subsidy_contract_type_id,
						sc.id,
						sc.hap_title AS owner_signed_title,
						sc.hap_person_name AS owner_signed_name,
						sc.hap_person_phone_number AS owner_signed_phone_number
					FROM
						subsidy_hap_requests shr
						JOIN subsidy_contracts sc ON ( sc.id = shr.subsidy_contract_id AND shr.cid = sc.cid )
					WHERE
						shr.id = ' . ( int ) $intSubsidyHapRequestId . '
						AND shr.cid = ' . ( int ) $intCid;
		$arrmixSubsidyContractsData = fetchData( $strSql, $objDatabase );

		return $arrmixSubsidyContractsData[0];
	}

	public static function fetchIncludeAdjustmentsThroughDatetimeByLeaseIdsBSubsidyContractIdByPropertyIdByCid( $arrintLeaseIds, $intSubsidyContractId, $intPropertyId, $intCid, $objDatabase, $arrintExcludedAssistanceCurrentArTransactionIds = [] ) {

		if( false == valArr( $arrintLeaseIds ) || false == valId( $intCid ) ) return NULL;

		$strJoinSubQuerySql = '';
		$strWhereSubQuerySql = '';
		if( true == valArr( $arrintExcludedAssistanceCurrentArTransactionIds ) ) {
			$strJoinSubQuerySql = ' JOIN (
			        SELECT
			               MIN( date_trunc ( \'month\', ssc.first_voucher_date ) ) AS calculated_first_voucher_date,
			               at.cid                             
			              FROM
			                  ar_transactions at
			                  JOIN scheduled_charges sc ON ( sc.cid = at.cid AND sc.id = at.scheduled_charge_id )
			                  JOIN subsidy_certification_scheduled_charges scsc ON ( scsc.cid = sc.cid AND scsc.scheduled_charge_id = sc. id )
			                  JOIN subsidy_certifications ssc ON ( ssc.cid = scsc.cid AND ssc.id = scsc.subsidy_certification_id )
			              WHERE
			                  at.id IN (' . implode( ',', $arrintExcludedAssistanceCurrentArTransactionIds ) . ')
			             AND at.cid = ' . ( int ) $intCid . ' 
			             group by at.cid
			       ) subq ON ( subq.cid = sc.cid AND ( sc.first_voucher_date IS NULL OR sc.first_voucher_date >=  subq.calculated_first_voucher_date ) )';
		} else {
			$strWhereSubQuerySql = 'AND sc.first_voucher_date IS NULL';
		}

		$strSql = 'SELECT
					    MIN( date_trunc ( \'month\', sc.effective_date ) ) AS include_adjustment_through_date
					FROM
					    subsidy_certifications sc
					    ' . $strJoinSubQuerySql . '
						WHERE
					    sc.cid = ' . ( int ) $intCid . '
					    AND sc.lease_id  IN (' . implode( ',', $arrintLeaseIds ) . ')
					    AND sc.property_id = ' . ( int ) $intPropertyId . '
					    AND sc.subsidy_contract_id =  ' . ( int ) $intSubsidyContractId . '
					    ' . $strWhereSubQuerySql . '
					    AND sc.deleted_on IS NULL
					    AND sc.deleted_by IS NULL
					    AND sc.subsidy_certification_disable_reason_id IS NULL';

		return self::fetchColumn( $strSql, 'include_adjustment_through_date', $objDatabase );
	}

	public static function fetchSubsidyHapRequestsBySubsidyContractIdByVoucherDateByPropertyIdByCid( $intSubsidyContractId, $strEffectiveDate, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intSubsidyContractId ) || false == valId( $intPropertyId ) ) return NULL;

		$strEffectiveDateConditionSql  = ( true == valStr( $strEffectiveDate ) ) ? ' AND shr.voucher_date >= \'' . $strEffectiveDate . '\'' : '';

		$strSql = 'SELECT
						*
					FROM
						subsidy_hap_requests shr
					WHERE
						shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
						AND shr.property_id = ' . ( int ) $intPropertyId . '
						AND shr.subsidy_hap_request_status_type_id = ' . ( int ) CSubsidyHapRequestStatusType::OPEN . '
						' . $strEffectiveDateConditionSql . '
						AND shr.cid = ' . ( int ) $intCid . '
						AND shr.deleted_by IS NULL
						AND shr.deleted_on IS NULL
						LIMIT 1';

		return self::fetchSubsidyHapRequest( $strSql, $objDatabase );
	}

	public static function fetchFirstSubsidyHapRequestsBySubsidyContractIdByPropertyIdByCid( $intSubsidyContractId, $intPropertyId, $intCid, $objDatabase, $boolIsShowVoucherDate = false ) {
		if( false == valId( $intSubsidyContractId ) || false == valId( $intPropertyId ) ) return NULL;
		$strWhereCondition = '';

		if( false == $boolIsShowVoucherDate ) {
			$strWhereCondition .= ' AND shr.subsidy_hap_request_status_type_id = ' . CSubsidyHapRequestStatusType::OPEN;
		}

		$strSql = 'SELECT
						*
					FROM
						subsidy_hap_requests shr
					WHERE
						shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
						AND shr.property_id = ' . ( int ) $intPropertyId . '
						AND shr.cid = ' . ( int ) $intCid . '
						AND shr.deleted_by IS NULL
						AND shr.deleted_on IS NULL
						' . $strWhereCondition . '
					ORDER BY
						voucher_date ASC
						LIMIT 1';

		return self::fetchSubsidyHapRequest( $strSql, $objDatabase );
	}

	public static function fetchLatestHapVoucherDateBySubsidyContractIdByPropertyIdByCid( $intSubsidyContractId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intSubsidyContractId ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                       MAX( shr.voucher_date ) AS latest_voucher_date
				   FROM
                        subsidy_hap_requests shr
				   WHERE
                        shr.cid = ' . ( int ) $intCid . '
                        AND shr.property_id = ' . ( int ) $intPropertyId . '
                        AND shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
                        AND shr.deleted_on IS NULL 
                        AND shr.deleted_by IS NULL';

		return self::fetchColumn( $strSql, 'latest_voucher_date', $objDatabase );

	}

	public static function fetchDateFinalizedForLatestHapVoucherBySubsidyContractIdByPropertyIdByCid( $intSubsidyContractId, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valId( $intSubsidyContractId ) || false == valId( $intPropertyId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
                       shr.date_finalized AS latest_voucher_date_finalized
				   FROM
                        subsidy_hap_requests shr
				   WHERE
                        shr.cid = ' . ( int ) $intCid . '
                        AND shr.property_id = ' . ( int ) $intPropertyId . '
                        AND shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
                        AND shr.deleted_on IS NULL 
                        AND shr.deleted_by IS NULL
                    ORDER BY
                        shr.date_finalized DESC
                        LIMIT 1';

		return self::fetchColumn( $strSql, 'latest_voucher_date_finalized', $objDatabase );

	}

	public static function fetchSubsidyHapRequestByPropertyIdBySubsidyContractIdByVoucherDateBySubsidyHapRequestStatusIdsByCid( $intPropertyId, $intSubsidyContractId, $strVoucherDate, $arrintSubsidyHapRequestStatusIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					shr.*
				FROM
					subsidy_hap_requests shr
				WHERE
					shr.cid = ' . ( int ) $intCid . '
					AND shr.property_id = ' . ( int ) $intPropertyId . '
					AND shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
					AND shr.subsidy_hap_request_status_type_id IN ( ' . implode( ',', $arrintSubsidyHapRequestStatusIds ) . ' )
					AND shr.voucher_date =  \'' . $strVoucherDate . '\'::date
					AND shr.deleted_by IS NULL
					AND shr.deleted_on IS NULL';

		return self::fetchSubsidyHapRequest( $strSql, $objDatabase );
	}

	public static function fetchSubsidyHapRequestsByPreviousSubsidyHapRequestIdByPropertyIdByCid( $intPreviousSubsidyHapRequestId, $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					shr.*
				FROM
					subsidy_hap_requests shr
				WHERE
					shr.cid = ' . ( int ) $intCid . '
					AND shr.property_id = ' . ( int ) $intPropertyId . '
					AND shr.previous_subsidy_hap_request_id = ' . ( int ) $intPreviousSubsidyHapRequestId . '
					AND shr.deleted_by IS NULL
					AND shr.deleted_on IS NULL';

		return self::fetchSubsidyHapRequest( $strSql, $objDatabase );
	}

	public static function fetchSubsidyHapRequestCountBySubsidyContractIdByPropertyIdByCid( $intSubsidyContractId, $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intSubsidyContractId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
					COUNT( shr.id ) AS subsidy_hap_request_count
				FROM
					subsidy_hap_requests shr
				WHERE
					shr.cid = ' . ( int ) $intCid . '
					AND shr.property_id = ' . ( int ) $intPropertyId . '
					AND shr.subsidy_contract_id = ' . ( int ) $intSubsidyContractId . '
					AND shr.deleted_by IS NULL
					AND shr.deleted_on IS NULL';

		return self::fetchColumn( $strSql, 'subsidy_hap_request_count', $objDatabase );
	}

}
?>