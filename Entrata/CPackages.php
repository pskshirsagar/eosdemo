<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CPackages
 * Do not add any new functions to this class.
 */

class CPackages extends CBasePackages {

	public static function fetchPackagesByIdsByCid( $arrintPackageIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPackageIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						packages
					WHERE
						id IN ( ' . implode( ',', $arrintPackageIds ) . ' )
						AND cid = ' . ( int ) $intCid;
		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchCustomPackageByIdByCid( $intPackageId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.*,
						sv.vendor_name as package_name,
						sv.is_custom as custom_vendor,
						c.name_first || \' \' || c.name_last as customer_name,
						COALESCE( l.building_name || \' - \' || l.unit_number_cache, l.building_name, l.unit_number_cache ) as unit_number,
						fl.file_path as signature_file_path,
    					fl.file_name as signature_file_name
				   FROM
						packages p
						JOIN customers c ON ( p.customer_id = c.id AND p.cid = c.cid )
						JOIN cached_leases l ON ( p.lease_id = l.id AND p.cid = l.cid )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND p.cid = sv.cid )
						LEFT JOIN files AS fl ON ( p.signature_file_id = fl.id AND p.cid = fl.cid )
				   WHERE
						p.id =' . ( int ) $intPackageId . '
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchPackage( $strSql, $objDatabase );
	}

	public static function fetchCustomPackagesByIdsByCid( $intPackageIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.*,
						sv.vendor_name as package_name,
						sv.is_custom as custom_vendor,
						c.name_first || \' \' || c.name_last as customer_name,
						l.unit_number_cache as unit_number,
						fl.file_path as signature_file_path,
						fl.file_name as signature_file_name
				   FROM
						packages p
						JOIN customers c ON ( p.customer_id = c.id AND p.cid = c.cid )
						JOIN cached_leases l ON ( p.lease_id = l.id AND p.cid = l.cid )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id )
						LEFT JOIN files AS fl ON ( p.signature_file_id = fl.id AND p.cid = fl.cid )
				   WHERE
						p.id IN ( ' . implode( ',', $intPackageIds ) . ' )
						AND p.cid = ' . ( int ) $intCid;

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchPackagesNotMailedNotificationByBatchIdByCid( $intBatchId, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSql = 'SELECT
						p.* ,
    					c.name_first,
                        c.name_last,
                        pu.id,
                        l.unit_number_cache AS unit_number,
                        pb.building_name AS building_name,
						sv.vendor_name as package_name,
						sv.is_custom as custom_vendor
					FROM
						packages p
						JOIN customers c ON ( p.customer_id = c.id AND p.cid = c.cid )
						JOIN cached_leases l ON ( p.lease_id = l.id AND p.cid = l.cid )
                        JOIN unit_spaces us ON ( us.id = l.unit_space_id AND us.cid = l.cid ' . $strCheckDeletedUnitSpacesSql . ' )
                        JOIN property_units pu ON ( pu.id = us.property_unit_id AND pu.cid = us.cid ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN property_buildings pb ON ( pu.property_building_id = pb.id AND pu.cid = pb.cid AND pb.deleted_on IS NULL )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid )
					WHERE
						p.package_batch_id = ' . ( int ) $intBatchId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.emailed_on IS NULL';

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchPackagesWithCustomerByPackageBatchId( $intPackageBatchId, $intCid, $objPackageFilter, $objDatabase ) {

		$strSql = 'SELECT
						p.*,
						c.name_first,
						c.name_last,
						c.id as customer_id,
						c.email_address AS customer_email_address,
						l.unit_number_cache AS unit_number,
						sv.vendor_name AS package_name,
						sv.is_custom as custom_vendor
					FROM
						packages AS p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid )
						LEFT JOIN cached_leases l ON ( l.id = p.lease_id AND l.cid = p.cid )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid )
					WHERE
						p.package_batch_id = ' . ( int ) $intPackageBatchId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $objPackageFilter->getFilterPropertyIds() ) . ' )';

		if( true == valObj( $objPackageFilter, 'CPackageFilter' ) && false == is_null( $objPackageFilter->getSortBy() ) && false == is_null( $objPackageFilter->getSortDirection() ) ) {
			switch( trim( $objPackageFilter->getSortBy() ) ) {
				case 'resident_unit':
					$strSql .= ' ORDER BY (c.name_first, l.unit_number_cache) ' . addslashes( $objPackageFilter->getSortDirection() );
					break;

				case 'created_on':
					$strSql .= ' ORDER BY p.created_on ' . addslashes( $objPackageFilter->getSortDirection() ) . ' NULLS LAST';
					break;

				default:
					$strSql .= ' ORDER BY p.created_on DESC';
					break;
			}
		} else {
			$strSql .= ' ORDER BY p.created_on DESC';
		}
		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchUndeliveredPackagesByCid( $intUndeliveredPackagesPageNo, $intPageSize, $intCid, $intStatusTypePendingId, $objPackageFilter, $objDatabase, $intShowPickedUp = NULL ) {
		if( false == valArr( $objPackageFilter->getFilterPropertyIds() ) ) {
			return [];
		}
				$strSql = 'SELECT
                        p.*,
                        pr.property_name,
                        c.name_first,
						c.name_last,
						c.id as customer_id,
						c.email_address AS customer_email_address,
                        COALESCE( l.building_name || \' - \' || l.unit_number_cache, l.building_name, l.unit_number_cache ) as unit_number,
						(CASE
							WHEN sv.vendor_name IS NOT NULL THEN sv.vendor_name
							ELSE dsv.name
						END) AS package_name,
						(CASE
							WHEN sv.is_custom IS NOT NULL THEN sv.is_custom
							ELSE false
						END) AS custom_vendor,
						(CASE
							WHEN sv.property_id IS NOT NULL THEN sv.property_id
							ELSE p.property_id
						END) AS shipping_vendor_property_id,
						sv.color as shipping_vendor_color,
						sv.is_vendor_icon as shipping_vendor_is_icon,
						cmf.fullsize_uri,
						lc.lease_status_type_id
					FROM
						packages AS p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid)
						LEFT JOIN properties pr ON ( pr.id = p.property_id AND pr.cid = p.cid )
						LEFT JOIN cached_leases l ON ( l.id = p.lease_id AND l.cid = p.cid )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid AND sv.property_id IS NOT NULL )
						LEFT JOIN default_shipping_vendors dsv ON ( p.package_type_id = dsv.id )
						LEFT JOIN lease_customers lc ON ( lc.customer_id = c.id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )
						LEFT JOIN company_media_files cmf ON ( cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid)
					WHERE
                         c.cid = ' . ( int ) $intCid . '
						AND l.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintApprovedLeaseStatusTypeIds ) . ' )
						AND p.property_id  IN ( ' . implode( ',', $objPackageFilter->getFilterPropertyIds() ) . ' )
						AND p.deleted_by IS NULL
						AND p.deleted_on IS NULL';

					if( 0 == $intShowPickedUp ) {
						$strSql .= ' AND p.package_status_type_id = ' . ( int ) $intStatusTypePendingId;
					}

					if( true == valObj( $objPackageFilter, 'CPackageFilter' ) && false == is_null( $objPackageFilter->getSortBy() ) && false == is_null( $objPackageFilter->getSortDirection() ) ) {
						switch( trim( $objPackageFilter->getSortBy() ) ) {
							case 'resident':
								$strSql .= ' ORDER BY (c.name_first) ' . addslashes( $objPackageFilter->getSortDirection() );
								break;

							case 'unit':
								$strSql .= ' ORDER BY (l.unit_number_cache) ' . addslashes( $objPackageFilter->getSortDirection() );
								break;

							case 'created_on':
								$strSql .= ' ORDER BY p.created_on ' . addslashes( $objPackageFilter->getSortDirection() ) . ' NULLS LAST';
								break;

							default:
								$strSql .= ' ORDER BY p.created_on DESC';
								break;
						}
					} else {
						$strSql .= ' ORDER BY p.created_on DESC';
					}

				$strSql .= ' LIMIT ' . ( int ) ( $intUndeliveredPackagesPageNo * $intPageSize );

			return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchTotalUndeliveredPackagesCountByCid( $intCid, $intStatusTypePendingId, $arrintPropertyIds, $objDatabase, $intShowPickedUp = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) return;

		$strSql = 'SELECT
						count(DISTINCT ( p.id )) as count
					FROM
						packages AS p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid )
						LEFT JOIN lease_customers lc ON ( lc.lease_id = p.lease_id AND lc.cid = p.cid )
						LEFT JOIN leases l ON ( l.id = lc.lease_id AND l.cid = p.cid )
						LEFT JOIN lease_status_types lst ON ( lc.lease_status_type_id = lst.id )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', ( array ) $arrintPropertyIds ) . ' )
						AND p.deleted_by IS NULL
						AND p.deleted_on IS NULL';

		if( 0 == $intShowPickedUp ) {
			$strSql .= ' AND p.package_status_type_id = ' . ( int ) $intStatusTypePendingId;
		}

		$arrstrData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;

	}

	public static function loadPackageSearchSql( $objPackageFilter ) {
		$strSearchSql = NULL;
		if( true == valObj( $objPackageFilter, 'CPackageFilter' ) ) {
			if( true == valArr( $objPackageFilter->getFilterPropertyIds() ) ) $strSearchSql .= ' AND p.property_id IN ( ' . implode( ',', $objPackageFilter->getFilterPropertyIds() ) . ' ) ';
			if( true == valArr( $objPackageFilter->getPackageStatusTypeIds() ) ) $strSearchSql .= ' AND p.package_status_type_id IN ( ' . implode( ',', $objPackageFilter->getPackageStatusTypeIds() ) . ' ) ';
			if( false == is_null( $objPackageFilter->getStartDate() ) && true == valStr( $objPackageFilter->getStartDate() ) )  $strSearchSql .= ' AND p.package_datetime >  \'' . date( 'Y-m-d', strtotime( $objPackageFilter->getStartDate() ) ) . ' 00:00:00\'';
			if( false == is_null( $objPackageFilter->getEndDate() ) && true == valStr( $objPackageFilter->getEndDate() ) )  $strSearchSql .= ' AND p.package_datetime <  \'' . date( 'Y-m-d', strtotime( $objPackageFilter->getEndDate() ) ) . ' 23:59:59\'';
			if( false == is_null( $objPackageFilter->getLeaseStatusTypeId() ) && true == valArr( $objPackageFilter->getLeaseStatusTypeId() ) )  $strSearchSql .= ' AND l.lease_status_type_id IN( ' . implode( ',', $objPackageFilter->getLeaseStatusTypeId() ) . ' ) ';
			if( false == is_null( $objPackageFilter->getTrackingNumber() ) && true == valStr( $objPackageFilter->getTrackingNumber() ) ) $strSearchSql .= '  AND p.barcode ILIKE E\'%' . $objPackageFilter->getTrackingNumber() . '%\' ';
			if( false == is_null( $objPackageFilter->getQuickSearch() ) ) {
				$arrstrExplodedSearch = explode( ' ', $objPackageFilter->getQuickSearch() );
				$arrstrExplodedSearch = array_filter( array_map( 'trim', $arrstrExplodedSearch ) );
				$arrstrTextSearchParameters = [];
				// For Text Parameter
				if( true == valArr( $arrstrExplodedSearch ) ) {
					foreach( $arrstrExplodedSearch as $strExplodedSearch ) {
						addslashes( $strExplodedSearch );
						if( false === strrchr( $strExplodedSearch, '\'' ) ) {
							$strFilteredExplodedSearch = $strExplodedSearch;
						} else {
							$strFilteredExplodedSearch = pg_escape_string( $strExplodedSearch );
						}
						array_push( $arrstrTextSearchParameters, $strFilteredExplodedSearch );
					}
				}
				$intTextSearchParametersCount = \Psi\Libraries\UtilFunctions\count( $arrstrTextSearchParameters );

				for( $intPackagesParameter = 0; $intPackagesParameter < $intTextSearchParametersCount; $intPackagesParameter++ ) {

					$strAndParameters = str_replace( ',', '', $arrstrTextSearchParameters[$intPackagesParameter] );
					$strTrackingNumber  = $strBillToUnitNumber = $strAndParameters;

					if( \Psi\CStringService::singleton()->strstr( $strAndParameters, '-' ) && 1 < strlen( $strAndParameters ) ) {

						$arrstrUnitNumber    = explode( '-', $strAndParameters );
						$strBuildingName     = $arrstrUnitNumber[0];
						$strBillToUnitNumber = $arrstrUnitNumber[1];
					}

					$strSearchSql .= ' AND ( c.name_first ILIKE E\'%' . $strAndParameters . '%\' OR  c.name_last ILIKE E\'%' . $strAndParameters . '%\' OR  l.unit_number_cache ILIKE E\'%' . $strAndParameters . '%\' ) ';

				}
			}
		}
		return $strSearchSql;
	}

	public static function fetchPackagesWithCustomerByPackageBatchIdByPackageFilter( $intPageNo, $intPageSize, $strPackageDate, $intShowPickedUp, $intCid, $objPackageFilter, $objDatabase, $boolIsFromSearch = false, $arrintPackageStatusTypeIds = [] ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;
		$strWhereSql = '';
		if( true == valStr( $strPackageDate ) && true == CValidation::validateDate( date( 'm/d/Y', $strPackageDate ) ) ) {
			$strWhereSql .= ' AND p.property_id IN ( ' . implode( ',', $objPackageFilter->getFilterPropertyIds() ) . ' ) ';
		} else {
			$strWhereSql = CPackages::loadPackageSearchSql( $objPackageFilter );

			if( false == is_null( $objPackageFilter->getVendorId() ) && true == valId( $objPackageFilter->getVendorId() ) ) {
				$strPropertyShippingVendorIds = CShippingVendors::fetchPropertyShippingVendorsIdsByIdByPropertyIds( $objPackageFilter->getVendorId(), $objPackageFilter->getFilterPropertyIds(), $objDatabase );
				$strWhereSql .= ' AND p.package_type_id IN ( ' . $strPropertyShippingVendorIds . ' ) ';
			}
		}

		$strSql = 'SELECT
						p.*,
						pr.property_name,
						c.name_first,
						c.name_last,
						c.id as customer_id,
						c.email_address AS customer_email_address,
						COALESCE( l.building_name || \' - \' || l.unit_number_cache, l.building_name, l.unit_number_cache ) as unit_number,
						(CASE
			              WHEN sv.vendor_name IS NOT NULL THEN sv.vendor_name
			              ELSE dsv.name
			            END) AS package_name,
						(CASE
			              WHEN sv.is_custom IS NOT NULL THEN sv.is_custom
			              ELSE false
			            END) AS custom_vendor,
						(CASE
			              WHEN sv.property_id IS NOT NULL THEN sv.property_id
			              ELSE p.property_id
			            END) AS shipping_vendor_property_id,
						sv.color as shipping_vendor_color,
						sv.is_vendor_icon as shipping_vendor_is_icon,
						lc.lease_status_type_id,
						cmf.fullsize_uri
					FROM
						packages AS p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid)
						LEFT JOIN properties pr ON ( pr.id = p.property_id AND pr.cid = p.cid)
						LEFT JOIN cached_leases l ON ( l.id =  p.lease_id  AND l.cid = p.cid)
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid AND sv.property_id IS NOT NULL )
						LEFT JOIN default_shipping_vendors dsv ON ( p.package_type_id = dsv.id )
						LEFT JOIN lease_customers lc ON ( lc.customer_id = c.id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )
						LEFT JOIN company_media_files cmf ON ( cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid)
					WHERE
						p.cid = ' . ( int ) $intCid . ' ' . $strWhereSql . ' AND lc.lease_status_type_id !=' . CLeaseStatusType::CANCELLED . ' AND p.deleted_on IS NULL';

		if( true == valStr( $strPackageDate ) && true == CValidation::validateDate( date( 'm/d/Y', $strPackageDate ) ) ) {
			$strSql .= ' AND CAST( p.package_datetime AS DATE ) = \'' . date( 'm/d/Y', $strPackageDate ) . '\'::date';
		}

		if( false == valStr( $strPackageDate ) && true == isset( $arrintPackageStatusTypeIds ) && true == valArr( $arrintPackageStatusTypeIds ) ) {
			$strSql .= ' AND p.package_status_type_id IN ( ' . implode( ',', $arrintPackageStatusTypeIds ) . ' )';
		} elseif( ( 0 == $intShowPickedUp && false == $boolIsFromSearch ) ) {
			$strSql .= ' AND p.package_status_type_id NOT IN ( ' . implode( ',', [ CPackageStatusType::PICKEDUP, CPackageStatusType::DELIVERED, CPackageStatusType::RETURNED_TO_SENDER ] ) . ' )';
		}

		if( true == valObj( $objPackageFilter, 'CPackageFilter' ) && false == is_null( $objPackageFilter->getSortBy() ) && false == is_null( $objPackageFilter->getSortDirection() ) ) {
			switch( trim( $objPackageFilter->getSortBy() ) ) {
				case NULL:
				case 'resident':
					$strSql .= ' ORDER BY (c.name_first) ' . addslashes( $objPackageFilter->getSortDirection() );
					break;

				case 'unit':
					$strSql .= ' ORDER BY (l.unit_number_cache) ' . addslashes( $objPackageFilter->getSortDirection() );
					break;

				case 'created_on':
					$strSql .= ' ORDER BY p.package_datetime ' . addslashes( $objPackageFilter->getSortDirection() ) . ' NULLS LAST';
					break;

				case 'property':
					$strSql .= ' ORDER BY pr.property_name ' . addslashes( $objPackageFilter->getSortDirection() );
					break;

				case 'vendor':
					$strSql .= ' ORDER BY sv.vendor_name ' . addslashes( $objPackageFilter->getSortDirection() );
					break;

				case 'status':
					$strSql .= ' ORDER BY p.package_status_type_id ' . addslashes( $objPackageFilter->getSortDirection() ) . ', p.package_datetime DESC';
					break;

				case 'tracking':
					$strSql .= ' ORDER BY p.barcode ' . addslashes( $objPackageFilter->getSortDirection() ) . ' NULLS LAST';
					break;

				case 'status created_on':
					$strSql .= ' ORDER BY p.package_status_type_id ASC, p.package_datetime DESC';
					break;

				default:
					$strSql .= ' ORDER BY p.package_status_type_id ASC, p.package_datetime DESC';
					break;
			}
		} else {
			$strSql .= ' ORDER BY p.package_status_type_id ASC, p.package_datetime DESC';
		}

		// optimize the query using CTE as Order by on huge record set slows down the above prepared sql
		$strSql = 'WITH result_set as ( ' . $strSql . ' ) SELECT * FROM result_set';
		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchPackagesByPackageBatchIdByPackageFilter( $intPackageBatchId, $intCid, $objPackageFilter, $objDatabase ) {

		$strSql = 'SELECT
						p.*,
						c.name_first,
						c.name_last,
						c.id as customer_id,
						c.email_address AS customer_email_address,
						l.unit_number_cache AS unit_number,
						sv.vendor_name as package_name,
						sv.is_custom as custom_vendor,
						sv.property_id as shipping_vendor_property_id,
						sv.color as shipping_vendor_color
					FROM
						packages AS p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid)
						LEFT JOIN cached_leases l ON ( l.id =  p.lease_id  AND l.cid = p.cid)
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid )
					WHERE
						p.package_batch_id = ' . ( int ) $intPackageBatchId . '
						AND p.cid = ' . ( int ) $intCid . ' ' . CPackages::loadPackageSearchSql( $objPackageFilter );

		if( false == is_null( $objPackageFilter->getVendorId() ) && true == valId( $objPackageFilter->getVendorId() ) ) {
			$strPropertyShippingVendorIds = CShippingVendors::fetchPropertyShippingVendorsIdsByIdByPropertyIds( $objPackageFilter->getVendorId(), $objPackageFilter->getFilterPropertyIds(), $objDatabase );
			$strSql .= ' AND p.package_type_id IN ( ' . $strPropertyShippingVendorIds . ' ) ';
		}

		if( true == valObj( $objPackageFilter, 'CPackageFilter' ) && false == is_null( $objPackageFilter->getSortBy() ) && false == is_null( $objPackageFilter->getSortDirection() ) ) {
			switch( trim( $objPackageFilter->getSortBy() ) ) {
				case 'resident_unit':
					$strSql .= ' ORDER BY (c.name_first, l.unit_number_cache) ' . addslashes( $objPackageFilter->getSortDirection() );
					break;

				case 'created_on':
					$strSql .= ' ORDER BY p.created_on ' . addslashes( $objPackageFilter->getSortDirection() ) . ' NULLS LAST';
					break;

				default:
					$strSql .= ' ORDER BY (c.name_first, l.unit_number_cache) ASC';
					break;
			}
		} else {
			$strSql .= ' ORDER BY (c.name_first, l.unit_number_cache) ASC';
		}

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchPackageBatchPropertiesByCidByPackageBatchDate( $intCid, $intPackageBatchDate, $objDatabase ) {
		if( false == valStr( $intPackageBatchDate ) )  return;

		$strSql = 'SELECT
						p.description,
						prt.property_name
					FROM
						packages p
				 		INNER JOIN properties prt ON ( prt.id = p.property_id AND prt.cid = p.cid )
					WHERE
						CAST( p.package_datetime AS DATE ) = \'' . date( 'm/d/Y', $intPackageBatchDate ) . '\'::date
						AND p.cid = ' . ( int ) $intCid . '
					GROUP BY
						p.description, prt.property_name';

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchUndeliveredPackagesWithCustomerByCustomerId( $intCustomerId, $intCid, $intPackageStatusType, $arrintPropertyIds, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.*,
						c.name_first,
						c.name_last,
						c.email_address AS customer_email_address,
						c.username as customer_username,
						COALESCE( l.building_name || \' - \' || l.unit_number_cache, l.building_name, l.unit_number_cache ) as unit_number,
						lc.id AS lease_customer_id,
						(CASE
							WHEN sv.vendor_name IS NOT NULL THEN sv.vendor_name
							ELSE dsv.name
						END) AS package_name,
						(CASE
							WHEN sv.is_custom IS NOT NULL THEN sv.is_custom
							ELSE false
						END) AS custom_vendor,
						(CASE
							WHEN sv.property_id IS NOT NULL THEN sv.property_id
							ELSE p.property_id
						END) AS shipping_vendor_property_id,
						sv.color as shipping_vendor_color,
						sv.is_vendor_icon as shipping_vendor_is_icon,
						cmf.fullsize_uri,
						lc.lease_status_type_id
					FROM
						packages AS p
						JOIN view_customers c ON ( c.id = p.customer_id AND c.cid = p.cid )
						LEFT JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = c.cid AND lc.lease_id = p.lease_id )
						LEFT JOIN cached_leases l ON ( l.id = p.lease_id AND l.cid = p.cid )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid AND sv.property_id IS NOT NULL )
						LEFT JOIN default_shipping_vendors dsv ON ( p.package_type_id = dsv.id )
						LEFT JOIN company_media_files cmf ON ( cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid)
					WHERE
						c.id = ' . ( int ) $intCustomerId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.package_status_type_id = ' . ( int ) $intPackageStatusType . '
						AND p.deleted_by IS NULL
						AND p.deleted_on IS NULL
					ORDER BY
						p.id';

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchCustomerPackagesByPackageIdsByCid( $arrintPackageIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( p.id )
						p.*,
						c.name_first,
						c.name_last,
						c.email_address AS customer_email_address,
						lc.id as lease_customer_id,
						l.unit_number_cache AS unit_number,
						(CASE
			              WHEN sv.vendor_name IS NOT NULL THEN sv.vendor_name
			              ELSE dsv.name
			            END) AS package_name,
			            (CASE
			              WHEN sv.is_custom IS NOT NULL THEN sv.is_custom
			              ELSE false
			            END) AS custom_vendor
					FROM
						packages AS p
						JOIN customers AS c ON ( c.id = p.customer_id AND c.cid = p.cid )
						JOIN lease_customers lc ON ( lc.customer_id = c.id AND lc.cid = c.cid )
						JOIN cached_leases l ON ( l.id = lc.lease_id AND l.cid = lc.cid )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid AND sv.property_id IS NOT NULL )
						LEFT JOIN default_shipping_vendors dsv ON ( p.package_type_id = dsv.id )
					WHERE
						p.id IN ( ' . implode( ',', $arrintPackageIds ) . ' )
						AND p.cid = ' . ( int ) $intCid . '
					ORDER BY
						p.id,
						CASE
							WHEN lc.lease_status_type_id = ' . CLeaseStatusType::CURRENT . '
							THEN 1
				            ELSE 2
				        END';

		return self::fetchPackages( $strSql, $objDatabase );

	}

	public static function fetchUndeliveredPackageCountByPropertyIdByCidByCustomerIdByLeaseIds( $intPropertyId, $intCid, $intCustomerId, $arrintLeaseIds, $objDatabase ) {

		$strSql = 'SELECT
						count(p.id) as package_count,
						cp.property_name
				   FROM
						packages p
						JOIN properties cp ON ( p.property_id = cp.id AND p.cid = cp.cid )
				   WHERE
						p.property_id = ' . ( int ) $intPropertyId . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.customer_id = ' . ( int ) $intCustomerId . '
						AND p.lease_id IN ( ' . implode( ',', $arrintLeaseIds ) . ' )
						AND p.package_status_type_id = ' . ( int ) CPackageStatusType::UNDELIVERED . '
						AND p.delivered_on IS NULL
						AND p.deleted_by IS NULL
						AND p.deleted_on IS NULL
				   GROUP BY
						p.customer_id,
						cp.property_name';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchDistinctUndeliveredPackagesByPropertyIdsByBuildingIdsByCid( $arrintPropertyIds, $strBuildingIds, $intCid, $objDatabase, $boolIncludeDeletedUnits = false, $boolIncludeDeletedUnitSpaces = false ) {

		$strCheckDeletedUnitsSql 	  = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';
		$strCheckDeletedUnitSpacesSql = ( false == $boolIncludeDeletedUnitSpaces ) ? ' AND us.deleted_on IS NULL' : '';

		$strSqlWhere = '';

		if( true == valStr( $strBuildingIds ) ) {
			$strSqlWhere = ' AND pb.id IN ( ' . $strBuildingIds . ' )';
		}

		$strSql = 'WITH result AS (
					    SELECT 
							p.*,
							l.unit_number_cache AS unit_number,
							pb.building_name,
							pb.id as building_id,
							COUNT( l.unit_number_cache ) OVER (PARTITION BY l.unit_number_cache, pb.building_name) AS package_count,
							(CASE
								WHEN sv.vendor_name IS NOT NULL THEN sv.vendor_name
								ELSE dsv.name
							END) AS package_name,
							cmf.fullsize_uri,
							sv.is_custom as custom_vendor,
							CASE
							  WHEN pb.order_num IS NULL THEN \'-1\'
							  ELSE pb.order_num
							END building_order,
					        ROW_NUMBER() OVER ( PARTITION BY l.unit_number_cache,pb.building_name ORDER BY l.unit_number_cache,pb.building_name ) rownum
					    FROM
					        packages AS p
							JOIN cached_leases l ON ( l.id = p.lease_id AND l.cid = p.cid )
							JOIN unit_spaces us ON ( us.id = l.unit_space_id AND us.cid = l.cid ' . $strCheckDeletedUnitSpacesSql . ' )
	                        JOIN property_units pu ON ( pu.id = us.property_unit_id AND pu.cid = us.cid ' . $strCheckDeletedUnitsSql . ' )
							LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid AND sv.property_id IS NOT NULL )
							LEFT JOIN default_shipping_vendors dsv ON ( p.package_type_id = dsv.id )
							LEFT JOIN company_media_files cmf ON cmf.id = sv.company_media_file_id AND cmf.cid = sv.cid
							LEFT JOIN property_buildings pb ON ( pb.id = pu.property_building_id AND pb.cid = pu.cid AND pb.deleted_on IS NULL )
						WHERE
							p.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND p.cid = ' . ( int ) $intCid . '
							AND p.package_status_type_id = ' . ( int ) CPackageStatusType::UNDELIVERED . '
							AND p.deleted_by IS NULL
							AND p.deleted_on IS NULL
							AND delivered_by IS NULL
							AND delivered_on IS NULL ' . ' ' . $strSqlWhere . '
						ORDER BY
							CASE
								WHEN l.unit_number_cache < \'A\'
								THEN lpad(l.unit_number_cache, 50, \'0\')
								ELSE l.unit_number_cache
							END
					)
					SELECT 
					  result.*
					FROM 
					    result
					WHERE
					    rownum = 1';

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchCustomersUndeliveredPackagesByPropertyIdsBySearchFilterByCid( $arrintPropertyIds, $strSearchFilter, $intCid,  $objDatabase, $boolIncludeDeletedUnits = false ) {

		$strCheckDeletedUnitsSql = ( false == $boolIncludeDeletedUnits ) ? ' AND pu.deleted_on IS NULL' : '';

		if( \Psi\CStringService::singleton()->strstr( $strSearchFilter, ' ' ) ) {
			$arrstrFastLookupFilter = explode( ' ', $strSearchFilter );
			$strSqlWhere = ' AND ( ( c.name_first ILIKE \'' . addslashes( $arrstrFastLookupFilter[0] ) . '%\' AND c.name_last ILIKE \'' . addslashes( $arrstrFastLookupFilter[1] ) . '%\' ) OR ( c.name_first ILIKE \'' . addslashes( $arrstrFastLookupFilter[1] ) . '%\' AND c.name_last ILIKE \'%' . addslashes( $arrstrFastLookupFilter[0] ) . '%\' ) )';
		} else {
			$strSqlWhere = ' AND ( c.name_first ILIKE \'' . addslashes( $strSearchFilter ) . '%\' OR c.name_last ILIKE \'' . addslashes( $strSearchFilter ) . '%\' OR l.unit_number_cache ILIKE \'%' . addslashes( $strSearchFilter ) . '%\' OR p.barcode ILIKE \'' . addslashes( $strSearchFilter ) . '%\' )';
		}

		if( true == valArr( $arrintPropertyIds ) ) {
				$strSqlWhere .= ' AND l.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}
		$strSql = 'SELECT
						DISTINCT ON( c.id )
						c.id as customer_id,
						c.name_first,
						c.name_last,
						c.name_first||\' \'||c.name_last as customer_name,
						pr.property_name,
						l.unit_number_cache AS unit_number,
						p.barcode,
						lc.id as lease_customer_id,
						pb.building_name
					 FROM
						customers   AS c
						JOIN packages AS p ON ( c.id = p.customer_id AND c.cid = p.cid )
                        JOIN lease_customers lc ON ( lc.customer_id = c.id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )
						JOIN cached_leases l ON ( l.id = lc.lease_id  AND l.cid = lc.cid )
						JOIN properties pr ON ( p.property_id = pr.id AND p.cid = pr.cid )
						LEFT JOIN property_units pu ON ( pu.id = l.property_unit_id AND pu.cid = l.cid ' . $strCheckDeletedUnitsSql . ' )
						LEFT JOIN property_buildings pb ON ( pb.property_id = pu.property_id AND pb.cid = pu.cid AND pb.deleted_on IS NULL )
					WHERE
						p.deleted_by IS NULL
						AND p.package_status_type_id = ' . ( int ) CPackageStatusType::UNDELIVERED . '
						AND p.delivered_by IS NULL
						AND p.delivered_on IS NULL
                        AND p.cid = ' . ( int ) $intCid . ' ' . $strSqlWhere . '
					ORDER BY
                        c.id';

		return self::fetchPackages( $strSql, $objDatabase );
	}

    public static function fetchPaginatedPackagesByCidByPackageFilter( $intPageNo, $intPageSize, $objPackageFiler, $intCid, $arrintPropertyIds, $objDatabase ) {

    	$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit	= ( int ) $intPageSize;

    	switch( $objPackageFiler->getPickedupPeriod() ) {
    		case 1:
    			$strLastWeek = 'now() - interval \'7 day\'';
    			$strDateCondition = ' AND p.package_datetime Between ' . $strLastWeek . ' AND \'now()\' ';
    			break;

    		case 2:
    			$strLastWeek = 'now() - interval \'14 day\'';
    			$strDateCondition = ' AND p.package_datetime Between ' . $strLastWeek . ' AND \'now()\' ';
    			break;

    		case 3:
    			$strLastWeek = 'now() - interval \'1 month\'';
    			$strDateCondition = ' AND p.package_datetime Between ' . $strLastWeek . ' AND \'now()\' ';
    			break;

    		default:
    			$strDateCondition = '';
    			break;
    	}

    	$strSql = 'SELECT
						p.*,
						sv.vendor_name AS package_name,
						sv.is_custom as custom_vendor,
						l.unit_number_cache AS unit_number,
                        ce.name_first as user_name_first,
                    	ce.name_last as user_name_last,
                    	cu.username as customer_username,
    					sv.property_id as shipping_vendor_property_id,
       					sv.color as shipping_vendor_color
				   FROM
						packages AS p
                        JOIN cached_leases AS l ON ( p.lease_id = l.id AND p.cid = l.cid )
                        JOIN company_users AS cu ON ( p.updated_by = cu.id AND p.cid = cu.cid ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
                        LEFT JOIN company_employees AS ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
						LEFT JOIN shipping_vendors AS sv ON ( sv.id = p.package_type_id AND p.cid = sv.cid )
					WHERE
						p.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.customer_id = ' . ( int ) $objPackageFiler->getCustomerId() . ' ' . $strDateCondition . '
						AND p.cid = ' . ( int ) $intCid . '
						AND p.deleted_by IS NULL
						AND p.deleted_on IS NULL
					ORDER BY
						p.id DESC';

    	$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

    	return self::fetchPackages( $strSql, $objDatabase );
    }

	public static function loadPackageSearchSqlByCid( $intCid, $objPackageFilter ) {
		$strSearchSql = NULL;

		if( true == valObj( $objPackageFilter, 'CPackageFilter' ) ) {
			if( true == valArr( $objPackageFilter->getFilterPropertyIds() ) ) {
				$strSearchSql .= '  AND property_id IN ( ' . implode( ',', $objPackageFilter->getFilterPropertyIds() ) . ' )
									AND cid = ' . ( int ) $intCid;
			}
		}

		return $strSearchSql;
	}

	public static function fetchPaginatedPackagesByCidByDateByPackageFilter( $intCid, $intCustomerId, $objDatabase, $intSelectionLimit = NULL, $arrintPropertyIds = [] ) {

		if( false == isset( $intSelectionLimit ) ) {
			$intSelectionLimit = 30;
		}

		$strSql = 'SELECT
						DATE( p.package_datetime ) as package_datetime, count( p.id ) as package_count
					FROM
						packages p
					JOIN lease_customers lc ON ( lc.customer_id = p.customer_id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )';

		$strSql .= ' WHERE p.cid = ' . ( int ) $intCid . '
						AND lc.lease_status_type_id !=' . CLeaseStatusType::CANCELLED . '
						AND p.deleted_on IS NULL';

		if( false == is_null( $intCustomerId ) && 0 != $intCustomerId ) {
			$strSql .= ' AND p.customer_id =' . ( int ) $intCustomerId;
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql .= ' GROUP BY
						DATE( p.package_datetime )
						ORDER BY
						package_datetime DESC';
		if( NULL != $intSelectionLimit && true == valId( $intSelectionLimit ) ) {
			$strSql .= ' LIMIT ' . $intSelectionLimit;
		}
		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchPackagesByCidByDateByPackageStatusByPackageBatchDates( $intCid, $arrobjPackageBatches, $intCustomerId, $objDatabase, $arrintPropertyIds = [] ) {
		if( false == valArr( $arrobjPackageBatches ) ) {
			return NULL;
		}

		$arrstrPackageDates = [];

		foreach( $arrobjPackageBatches as $objPackageBatch ) {
			$arrstrPackageDates[] = '\'' . $objPackageBatch->getPackageDatetime() . '\'';
		}

		$strSql = 'SELECT
						DATE( p.package_datetime ) as package_datetime, count( p.id )
					FROM
						packages p
					JOIN lease_customers lc ON ( lc.customer_id = p.customer_id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )';

		$strSql .= ' WHERE p.cid = ' . ( int ) $intCid . '
						AND lc.lease_status_type_id !=' . CLeaseStatusType::CANCELLED . '
						AND p.deleted_on IS NULL
						AND p.package_status_type_id NOT IN (2,3,4)
						AND CAST( p.package_datetime AS DATE ) IN ( ' . implode( ',', $arrstrPackageDates ) . ' )';

		if( false == is_null( $intCustomerId ) && 0 != $intCustomerId ) {
			$strSql .= ' AND p.customer_id =' . ( int ) $intCustomerId;
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql .= ' GROUP BY
						DATE( p.package_datetime )
					ORDER BY
						package_datetime DESC';
		$arrstrResponse = fetchData( $strSql, $objDatabase );
		return $arrstrResponse;
	}

	public static function fetchIncompletePackageCountByPackageBatchDateByLeaseIdByCustomerIdByCid( $intCid, $strPackageBatchDate, $intCustomerId, $objDatabase, $boolTotalCount = false, $arrintPropertyIds = [] ) {
		if( false == valStr( $strPackageBatchDate ) ) {
			return 0;
		}

		$strSql = 'SELECT
						count( DISTINCT ( p.id ) ) as total_count
					FROM
						packages AS p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid )
						LEFT JOIN cached_leases l ON ( l.id = p.lease_id AND l.cid = p.cid )
						LEFT JOIN lease_customers lc ON ( lc.customer_id = c.id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND lc.lease_status_type_id !=' . CLeaseStatusType::CANCELLED . '
						AND p.deleted_by IS NULL
						AND p.deleted_on IS NULL';

		if( false == is_null( $strPackageBatchDate ) && true == CValidation::validateDate( date( 'm/d/Y', $strPackageBatchDate ) ) ) {
			$strSql .= ' AND CAST( p.package_datetime AS DATE ) = \'' . date( 'm/d/Y', $strPackageBatchDate ) . '\'::date';
		}

		if( false == $boolTotalCount ) {
			$strSql .= ' AND p.package_status_type_id NOT IN (2,3,4)';
		}

		if( false == is_null( $intCustomerId ) && 0 != $intCustomerId ) {
			$strSql .= ' AND p.customer_id =' . ( int ) $intCustomerId;
		}

		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		return self::fetchColumn( $strSql, 'total_count', $objDatabase );
	}

	public static function fetchPackagesCountsByDateByPackageFilter( $intCid, $intCustomerId, $objPackageFilter, $objDatabase ) {

		$strSql = 'SELECT
				        DISTINCT( DATE( package_datetime ) ) as package_datetime
					FROM
						packages';

		$strSql .= ' WHERE cid = ' . ( int ) $intCid;

		if( false == is_null( $intCustomerId ) && 0 != $intCustomerId ) {
			$strSql .= ' AND customer_id =' . ( int ) $intCustomerId;
		}

		if( false == is_null( $objPackageFilter->getLeaseId() ) && 0 != $objPackageFilter->getLeaseId() ) {
			$strSql .= ' AND lease_id =' . ( int ) $objPackageFilter->getLeaseId();
		}

		$strSql .= CPackages::loadPackageSearchSqlByCid( $intCid, $objPackageFilter );
		$strSql .= ' GROUP BY
						package_datetime
					ORDER BY
						package_datetime DESC';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if ( true == isset ( $arrintResponse ) ) return \Psi\Libraries\UtilFunctions\count( $arrintResponse );

		return 0;
	}

    public static function fetchPackagesCountByCidByPackageFilter( $objPackageFiler, $intCid, $arrintPropertyIds, $objDatabase ) {

    	switch( $objPackageFiler->getPickedupPeriod() ) {
    		case 1:
    			$strLastWeek = 'now() - interval \'7 day\'';
    			$strDateCondition = ' AND p.package_datetime Between ' . $strLastWeek . ' AND now() ';
    			break;

    		case 2:
    			$strLastWeek = 'now() - interval \'14 day\'';
    			$strDateCondition = ' AND p.package_datetime Between ' . $strLastWeek . ' AND \'now()\' ';
    			break;

    		case 3:
    			$strLastWeek = 'now() - interval \'1 month\'';
    			$strDateCondition = ' AND p.package_datetime Between ' . $strLastWeek . ' AND \'now()\' ';
    			break;

    		default:
    			$strDateCondition = '';
    			break;
    	}

    	$strSql = 'SELECT
				    	count( p.id ) as count
			        FROM
					    packages AS p
					    JOIN leases AS l ON ( p.lease_id = l.id AND p.cid = l.cid )
					    JOIN company_users AS cu ON ( p.updated_by = cu.id AND p.cid = cu.cid ) AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
					    LEFT JOIN company_employees AS ce ON ( cu.company_employee_id = ce.id AND cu.cid = ce.cid )
					    LEFT JOIN shipping_vendors AS sv ON ( sv.id = p.package_type_id AND p.cid = sv.cid )
			    	WHERE
					    p.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
					    AND p.customer_id = ' . ( int ) $objPackageFiler->getCustomerId() . ' ' . $strDateCondition . '
					    AND p.cid = ' . ( int ) $intCid . '
					    AND p.deleted_by IS NULL
					    AND p.deleted_on IS NULL
					GROUP BY
					    p.id
					ORDER BY
					    p.id';

    	$arrintResponse = fetchData( $strSql, $objDatabase );

		if ( true == isset ( $arrintResponse ) ) return \Psi\Libraries\UtilFunctions\count( $arrintResponse );

    	return 0;
    }

    public static function fetchPackageCountWithCustomerByCustomerIdByTrackingNumberByPackageTypeIdByCid( $intCustomerId, $strTrackingNumber, $intPackageTypeId, $intCid, $objDatabase ) {

    	if( true == is_null( $intCustomerId ) || true == is_null( $intPackageTypeId ) || true == is_null( $strTrackingNumber ) ) return NULL;

    	$strFrom = ' packages AS p
    				 JOIN customers c on ( c.id = p.customer_id AND c.cid = p.cid AND p.customer_id = ' . ( int ) $intCustomerId . ' AND p.cid = ' . ( int ) $intCid . ' )';

    	$strWhere = ' WHERE
    					p.barcode = \'' . addslashes( $strTrackingNumber ) . '\'
						AND p.package_type_id = ' . ( int ) $intPackageTypeId . '
						AND p.customer_id IS NOT NULL';

    	return parent::fetchRowCount( $strWhere, $strFrom, $objDatabase );
    }

	public static function fetchTotalUndeliveredPackagesCountByBatchIdByCid( $intBatchId, $intCid, $intStatusTypePendingId, $intCustomerId, $objDatabase ) {

		if( false == valId( $intBatchId ) ) return;

		$strSqlWhere = ( true == valId( $intCustomerId ) ) ? ' AND p.customer_id = ' . $intCustomerId : '';

		$strSql = 'SELECT
						count( DISTINCT ( p.id ) ) as total_count
					FROM
						packages AS p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid )
						LEFT JOIN lease_customers lc ON ( lc.lease_id = p.lease_id AND lc.cid = p.cid )
						LEFT JOIN leases l ON ( l.id = lc.lease_id AND l.cid = p.cid )
						LEFT JOIN lease_status_types lst ON ( lc.lease_status_type_id = lst.id )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid )
					WHERE
						p.package_batch_id =  ' . ( int ) $intBatchId . '
						AND c.cid = ' . ( int ) $intCid . '
						AND p.package_status_type_id =  ' . ( int ) $intStatusTypePendingId . '  ' . $strSqlWhere . '
						AND p.deleted_by IS NULL
						AND p.deleted_on IS NULL';

		return self::fetchColumn( $strSql, 'total_count', $objDatabase );

	}

	public static function fetchActivePackagesByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						p.*
					FROM
						packages AS p
						JOIN package_batches pb on ( pb.cid = p.cid AND pb.id = p.package_batch_id )
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND p.property_id = ' . ( int ) $intPropertyId . '
						AND p.deleted_on IS NULL;';

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchPackagesByPropertyIdByCidByCreatedBy( $intPropertyId, $intCid, $intCompanyUserId, $objDatabase ) {
		if( false == is_numeric( $intCompanyUserId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						packages
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND created_by = ' . ( int ) $intCompanyUserId;

		return self::fetchPackages( $strSql, $objDatabase );
	}

	public static function fetchCustomPackagesDataWithCustomerByPackageBatchIdByPackageFilter( $intCid, $objPackageFilter, $objDatabase ) {

		$strSql = 'SELECT
						COUNT(CASE
			               WHEN p.package_status_type_id =' . CPackageStatusType::UNDELIVERED . ' THEN \'Undelivred\'
			               ELSE NULL
			             END) as undelivered_count,
			            COUNT(p.id)
					FROM
						packages AS p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid)
						LEFT JOIN properties pr ON ( pr.id = p.property_id AND pr.cid = p.cid)
						LEFT JOIN cached_leases l ON ( l.id =  p.lease_id  AND l.cid = p.cid)
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid )
						LEFT JOIN lease_customers lc ON ( lc.customer_id = c.id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )
					WHERE
						p.cid = ' . ( int ) $intCid . ' ' . CPackages::loadPackageSearchSql( $objPackageFilter ) . ' AND lc.lease_status_type_id !=' . CLeaseStatusType::CANCELLED . ' AND p.deleted_on IS NULL';

		if( false == is_null( $objPackageFilter->getVendorId() ) && true == valId( $objPackageFilter->getVendorId() ) ) {
			$strPropertyShippingVendorIds = CShippingVendors::fetchPropertyShippingVendorsIdsByIdByPropertyIds( $objPackageFilter->getVendorId(), $objPackageFilter->getFilterPropertyIds(), $objDatabase );
			if( true == valStr( $strPropertyShippingVendorIds ) ) {
				$strSql .= ' AND p.package_type_id IN ( ' . $strPropertyShippingVendorIds . ' ) ';
			}
		}

		$arrintData = fetchData( $strSql, $objDatabase );
		return $arrintData;
	}

	public static function fetchPackagesCountByPackageTypeIdByCid( $intPackageTypeId, $intCid, $objDatabase ) {

		$strWhere = 'WHERE
						package_type_id = ' . ( int ) $intPackageTypeId . '
						AND cid = ' . ( int ) $intCid;
		return parent::fetchRowCount( $strWhere, 'packages', $objDatabase );
	}

	public static function fetchUndeliveredPackagesByPropertyIdsByCustomerIdsByCid( $arrintPropertyIds, $arrintCustomerIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						p.*,
						sv.vendor_name as vendor_name
					FROM
						packages p
						JOIN customers c ON ( c.id = p.customer_id AND c.cid = p.cid )
						JOIN properties pr ON ( pr.id = p.property_id AND pr.cid = p.cid )
						JOIN cached_leases l ON ( l.id = p.lease_id AND l.cid = p.cid )
						LEFT JOIN shipping_vendors sv ON ( p.package_type_id = sv.id AND sv.cid = p.cid )
						JOIN lease_customers lc ON ( lc.customer_id = c.id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND l.lease_status_type_id IN ( ' . implode( ',', CLeaseStatusType::$c_arrintApprovedLeaseStatusTypeIds ) . ' )
						AND p.property_id  IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND p.package_status_type_id = ' . CPackageStatusType::UNDELIVERED . '
						AND p.deleted_by IS NULL
						AND p.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUndeliveredPacakgesCountForChartByPropertyIdsByCid( $intCid, $objDatabase, $arrintPropertyIds = [] ) {
		$strSql = 'SELECT
						count( CASE WHEN p.package_status_type_id = ' . CPackageStatusType::UNDELIVERED . '
									THEN 1
									ELSE NULL
								END ) as total_undelivered_packages,
						count( CASE WHEN p.created_on >= date_trunc( \'hour\', CURRENT_TIMESTAMP - \'24 hours\'::interval )
										 AND p.package_status_type_id = ' . CPackageStatusType::UNDELIVERED . '
									THEN 1
									ELSE NULL
								END ) as undelivered_since_one_day,
						count( CASE WHEN p.created_on < date_trunc( \'hour\', CURRENT_TIMESTAMP - \'24 hours\'::interval )
										 AND p.created_on::Date >= date_trunc ( \'day\', CURRENT_TIMESTAMP - \'3 days\'::interval )::date
										 AND p.package_status_type_id = ' . CPackageStatusType::UNDELIVERED . '
									THEN 1
									ELSE NULL
								END ) as undelivered_since_one_to_three_days,
						count( CASE WHEN p.created_on::Date <= date_trunc( \'day\', CURRENT_TIMESTAMP - \'4 days\'::interval )::date
										 AND p.created_on::Date >= date_trunc ( \'day\', CURRENT_TIMESTAMP - \'10 days\'::interval )::date
										 AND p.package_status_type_id = ' . CPackageStatusType::UNDELIVERED . '
									THEN 1
									ELSE NULL
								END ) as undelivered_since_four_to_ten_days,
						count( CASE WHEN p.created_on::Date < date_trunc( \'day\', CURRENT_TIMESTAMP - \'10 days\'::interval )::date
										 AND p.package_status_type_id = ' . CPackageStatusType::UNDELIVERED . '
									THEN 1
									ELSE NULL
								END ) as undelivered_since_elven_plus_days,
						count( CASE WHEN ( p.delivered_on is null OR p.delivered_on::Date > date_trunc( \'day\', CURRENT_TIMESTAMP - \'7 days\'::interval )::Date )
										   AND p.created_on::Date <= date_trunc( \'day\', CURRENT_TIMESTAMP - \'7 days\'::interval )::Date
									THEN 1
									ELSE NULL
								END) as seven_days_previous_undelivered_count,
						count( CASE WHEN ( p.delivered_on is null OR p.delivered_on::Date > date_trunc( \'day\', CURRENT_TIMESTAMP - \'30 days\'::interval )::Date )
										   AND p.created_on::Date <= date_trunc( \'day\', CURRENT_TIMESTAMP - \'30 days\'::interval )::Date
									THEN 1
									ELSE NULL
								END) as one_month_previous_undelivered_count,
						count( CASE WHEN ( p.delivered_on is null OR p.delivered_on::Date > date_trunc( \'day\', CURRENT_TIMESTAMP - \'365 days\'::interval )::Date )
										   AND p.created_on::Date <= date_trunc( \'day\', CURRENT_TIMESTAMP - \'365 days\'::interval )::Date
									THEN 1
									ELSE NULL
								END) as one_year_previous_undelivered_count
					FROM
						packages p
						JOIN lease_customers lc ON ( lc.customer_id = p.customer_id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )
					WHERE
						p.cid = ' . ( int ) $intCid;
		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}
		$strSql .= ' AND p.deleted_by IS NULL
					 AND p.deleted_on IS NULL
					 AND lc.lease_status_type_id !=' . CLeaseStatusType::CANCELLED . ' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDeliveredPacakgesCountForChartByPropertyIdsByCid( $intCid, $objDatabase, array $arrintPropertyIds = [] ) {
		$strSql = 'SELECT
						count( CASE WHEN p.delivered_on::Date = CURRENT_TIMESTAMP::Date 
										 AND p.package_status_type_id = ' . CPackageStatusType::DELIVERED . '
										 THEN 1 
										 ELSE NULL 
								END ) as delivered_today,
						count( CASE WHEN p.delivered_on >= date_trunc( \'hour\', CURRENT_TIMESTAMP - \'30 days\'::interval ) 
										AND p.package_status_type_id = ' . CPackageStatusType::DELIVERED . '
									THEN 1
									ELSE NULL
								END ) as total_delivered_packages_in_thirty_days,
						count( CASE WHEN p.delivered_on >= date_trunc( \'hour\', CURRENT_TIMESTAMP - \'24 hours\'::interval )
										 AND p.package_status_type_id = ' . CPackageStatusType::DELIVERED . '
						            THEN 1
									ELSE NULL
								END ) as delivered_since_one_day,
						count( CASE WHEN p.delivered_on < date_trunc( \'hour\', CURRENT_TIMESTAMP - \'24 hours\'::interval )
										 AND p.delivered_on::Date >= date_trunc ( \'day\', CURRENT_TIMESTAMP - \'3 days\'::interval )::date
										 AND p.package_status_type_id = ' . CPackageStatusType::DELIVERED . '
									THEN 1
									ELSE NULL
								END ) as delivered_since_one_to_three_days,
						count( CASE WHEN p.delivered_on::Date <= date_trunc( \'day\', CURRENT_TIMESTAMP - \'4 days\'::interval )::date
										 AND p.delivered_on::Date >= date_trunc ( \'day\', CURRENT_TIMESTAMP - \'10 days\'::interval )::date
										 AND p.package_status_type_id = ' . CPackageStatusType::DELIVERED . '
									THEN 1
									ELSE NULL
								END ) as delivered_since_four_to_ten_days,
						count( CASE WHEN p.delivered_on::Date < date_trunc( \'day\', CURRENT_TIMESTAMP - \'10 days\'::interval )::date
										 AND p.delivered_on::Date >= date_trunc ( \'day\', CURRENT_TIMESTAMP - \'30 days\'::interval )::date
										 AND p.package_status_type_id = ' . CPackageStatusType::DELIVERED . '
									THEN 1
									ELSE NULL
								END ) as delivered_since_elven_to_thirty_days
					FROM
						packages p
						JOIN lease_customers lc ON ( lc.customer_id = p.customer_id AND p.lease_id = lc.lease_id AND lc.cid = p.cid )
					WHERE
						p.cid = ' . ( int ) $intCid;
		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}
		$strSql .= ' AND p.deleted_by IS NULL
					 AND p.deleted_on IS NULL
					 AND lc.lease_status_type_id !=' . CLeaseStatusType::CANCELLED . ' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAverageDeliveredPackagesForDateByPropertyIdsByCid( $intCid, $objDatabase, array $arrintPropertyIds = [], $strFrom = '30 days' ) {
		$strSql = 'SELECT AVG( DATE_PART(\'day\', p.delivered_on::timestamp - p.created_on::timestamp) *
                        24 + DATE_PART(\'hour\', p.delivered_on::timestamp - p.created_on::timestamp))
					FROM packages p
					WHERE p.package_status_type_id = ' . CPackageStatusType::DELIVERED . '
						AND p.created_on > date_trunc(\'day\', CURRENT_TIMESTAMP - \'' . $strFrom . '\'::interval)
						AND p.cid = ' . ( int ) $intCid . '
						AND p.delivered_on IS NOT NULL';
		if( true == valArr( $arrintPropertyIds ) ) {
			$strSql .= ' AND p.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		}
		$strSql .= ' AND p.deleted_by IS NULL
					 AND p.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase )[0]['avg'];
	}

}
?>