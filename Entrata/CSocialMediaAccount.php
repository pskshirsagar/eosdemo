<?php

class CSocialMediaAccount extends CBaseSocialMediaAccount {

	protected $m_strLocationDetail;

	public function setLocationDetail( $strLocationDetail ) {
		$this->m_strLocationDetail = $strLocationDetail;
	}

	public function getLocationDetail() {
		return $this->m_strLocationDetail;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['location_detail'] ) ) {
			$this->setLocationDetail( $arrmixValues['location_detail'] );
		}
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSocialPostTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUserAccountKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUserTokenKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>