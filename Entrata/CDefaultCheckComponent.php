<?php

class CDefaultCheckComponent extends CBaseDefaultCheckComponent {

    public function valId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valDefaultCheckTemplateId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getDefaultCheckTemplateId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'default_check_template_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valCheckComponentTypeId() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getCheckComponentTypeId() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_component_type_id', '' ) );
        // }

        return $boolIsValid;
    }

    public function valXPosition() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getXPosition() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'x_position', '' ) );
        // }

        return $boolIsValid;
    }

    public function valYPosition() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getYPosition() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'y_position', '' ) );
        // }

        return $boolIsValid;
    }

    public function valWidth() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getWidth() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'width', '' ) );
        // }

        return $boolIsValid;
    }

    public function valHeight() {
        $boolIsValid = true;

        // Validation example

        // if( true == is_null( $this->getHeight() ) ) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'height', '' ) );
        // }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>