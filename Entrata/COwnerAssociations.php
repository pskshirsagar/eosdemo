<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\COwnerAssociations
 * Do not add any new functions to this class.
 */

class COwnerAssociations extends CBaseOwnerAssociations {

	/**
	 * Fetch single record.
	 *
	 */
	public static function fetchOwnerAssociationByOwnerIdByParentOwnerIdByCid( $intOwnerId, $intParentOwnerId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						owner_associations
					WHERE
						cid = ' . ( int ) $intCid . '
						AND owner_id = ' . ( int ) $intOwnerId . '
						AND parent_owner_id = ' . ( int ) $intParentOwnerId;

		return self::fetchOwnerAssociation( $strSql, $objClientDatabase );
	}

	/**
	 * Fetch muliple records in array format.
	 *
	 */

	public static function fetchOwnerAssociationsByParentOwnerIdByCid( $intParentOwnerId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						oa.*,
						o.owner_name
					FROM
						owner_associations oa
						JOIN owners o ON( oa.cid = o.cid AND oa.owner_id = o.id )
					WHERE
						o.cid = ' . ( int ) $intCid . '
						AND oa.parent_owner_id = ' . ( int ) $intParentOwnerId . '
					ORDER BY o.id';

		return self::fetchOwnerAssociations( $strSql, $objClientDatabase );
	}

	public static function fetchRecursiveOwnerIdsByOwnerIdByCid( $intOwnerId, $intCid, $objClientDatabase ) {

		$strSql = 'WITH RECURSIVE
						recursive_owner_associations( owner_id, cid, parent_owner_id )
					AS (
							SELECT
								owner_id,
								cid,
								parent_owner_id
							FROM
								owner_associations
							WHERE
								cid = ' . ( int ) $intCid . '
								AND owner_id = ' . ( int ) $intOwnerId . '

							UNION

							SELECT
								oa1.owner_id,
								oa1.cid,
								oa1.parent_owner_id
							FROM
								owner_associations oa1,
								recursive_owner_associations roa
							WHERE
								oa1.cid = roa.cid
								AND oa1.owner_id = roa.parent_owner_id
								AND oa1.cid = ' . ( int ) $intCid . '
						)

					SELECT
						DISTINCT oa.parent_owner_id
					FROM
						recursive_owner_associations roa1
						JOIN owner_associations oa ON ( roa1.cid = oa.cid AND roa1.owner_id = oa.owner_id )
					WHERE
						oa.cid = ' . ( int ) $intCid;

		$arrintData		= fetchData( $strSql, $objClientDatabase );
		$arrintOwnerIds	= [ ( int ) $intOwnerId ];

		foreach( $arrintData as $arrintDatum ) {
			$arrintOwnerIds[] = $arrintDatum['parent_owner_id'];
		}

		return $arrintOwnerIds;
	}

	public static function fetchRecursiveOwnerAssociationsByOwnerIdByCid( $intOwnerId, $intCid, $objClientDatabase ) {

		if( false == is_numeric( $intOwnerId ) ) {
			return NULL;
		}

		$strSql = 'WITH RECURSIVE
						recursive_owner_associations( cid, id, owner_id, parent_owner_id, percentage_ownership )
						AS (
								SELECT
									cid,
									id,
									owner_id,
									parent_owner_id,
									percentage_ownership
								FROM
									owner_associations
								WHERE
									cid = ' . ( int ) $intCid . '
									AND owner_id = ' . ( int ) $intOwnerId . '

								UNION

								SELECT
									oa1.cid,
									oa1.id,
									oa1.owner_id,
									oa1.parent_owner_id,
									oa1.percentage_ownership
								FROM
									owner_associations oa1,
									recursive_owner_associations roa
								WHERE
									oa1.cid = roa.cid
									AND oa1.owner_id = roa.parent_owner_id
									AND oa1.cid = ' . ( int ) $intCid . '
							)

							SELECT
								*
							FROM
								recursive_owner_associations
							WHERE
								cid = ' . ( int ) $intCid . '
							ORDER BY id';

		return self:: fetchOwnerAssociations( $strSql, $objClientDatabase );
	}

	public static function fetchRecursiveOwnerAssociationsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'WITH RECURSIVE
						recursive_owner_associations( cid, id, owner_id, parent_owner_id, percentage_ownership, property_id, calculated_percentage )
						AS (
								SELECT
									oa.cid,
									oa.id,
									oa.owner_id,
									oa.parent_owner_id,
									oa.percentage_ownership,
									p.id AS property_id,
									oa.percentage_ownership::FLOAT as calculated_percentage
								FROM
									owner_associations oa
									JOIN properties p ON ( oa.cid = p.cid AND oa.parent_owner_id = p.owner_id )
								WHERE
									oa.cid = ' . ( int ) $intCid . '
									AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )

								UNION

								SELECT
									oa1.cid,
									oa1.id,
									oa1.owner_id,
									oa1.parent_owner_id,
									oa1.percentage_ownership,
									roa.property_id,
									NULL as calculated_percentage
								FROM
									owner_associations oa1,
									recursive_owner_associations roa
								WHERE
									oa1.cid = roa.cid
									AND oa1.parent_owner_id = roa.owner_id
									AND oa1.cid = ' . ( int ) $intCid . '
							)

							SELECT
								*
							FROM
								recursive_owner_associations
							WHERE
								cid = ' . ( int ) $intCid . '
							ORDER BY property_id';

		return self:: fetchOwnerAssociations( $strSql, $objClientDatabase, false );
	}

	public static function fetchRecursiveOwnerAssociationsByParentOwnerIdsByPropertyIdsByCid( $arrintParentOwnerIds, $arrintPropertyIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintParentOwnerIds ) || false == valIntArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'WITH
						recursive owner_recursive( id, cid, owner_name, owner_id, parent_owner_id, percentage_ownership, should_forward_distributions, property_id )
					AS (
						SELECT
							oa.id,
							o.cid,
							o.owner_name,
							o.id AS owner_id,
							NULL::INTEGER AS parent_owner_id,
							oa.percentage_ownership,
							o.should_forward_distributions,
							p.id AS property_id
						FROM
							owners o
							LEFT JOIN owner_associations oa ON ( o.cid = oa.cid AND o.id = oa.owner_id )
							LEFT JOIN owners o2 ON ( o2.cid = oa.cid AND o2.id = oa.parent_owner_id AND o2.should_forward_distributions = 1 )
							LEFT JOIN properties p ON ( p.cid = o.cid AND p.owner_id = o.id )
						WHERE
							o.id IN ( ' . implode( ',', $arrintParentOwnerIds ) . ' )
							AND p.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND o.cid = ' . ( int ) $intCid . '

						UNION ALL
						SELECT
							oa.id,
							o.cid,
							o.owner_name,
							o.id AS owner_id,
							oa.parent_owner_id,
							oa.percentage_ownership,
							o.should_forward_distributions,
							orr.property_id
						FROM
							owners o
							JOIN owner_associations oa ON ( o.cid = oa.cid AND o.id = oa.owner_id )
							JOIN owners o2 ON ( o2.cid = oa.cid and o2.id = oa.parent_owner_id AND o2.should_forward_distributions = 1 )
							JOIN owner_recursive orr ON ( oa.cid = orr.cid AND oa.parent_owner_id = orr.owner_id )
							LEFT JOIN properties p ON ( p.cid = o.cid AND p.owner_id = o.id )
						WHERE
							oa.cid = ' . ( int ) $intCid . '
					)

					SELECT
						*
					FROM
						owner_recursive
					WHERE
						cid = ' . ( int ) $intCid;

		return self::fetchOwnerAssociations( $strSql, $objClientDatabase, false );
	}

}
?>