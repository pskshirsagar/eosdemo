<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CJobs
 * Do not add any new functions to this class.
 */

class CJobs extends CBaseJobs {

	public static function fetchPaginatedJobsByJobFilterByCid( $objJobFilter, $intCid, $objDatabase, $objPagination = NULL, $boolIsFromJobTemplates = false ) {
		if( false == valObj( $objJobFilter, 'CJobFilter' ) || false == valId( $intCid ) || false == valArr( $objJobFilter->getPropertyIds() ) ) return NULL;

		self::createTempTableJobsByJobFilterByCid( $objJobFilter, $intCid, $objDatabase, $boolIsFromJobTemplates );

		$strSql = 'SELECT * FROM temp_jobs_listing';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobsCountByJobFilterByCid( $objJobFilter, $intCid, $objDatabase ) {

		if( false == valObj( $objJobFilter, 'CJobFilter' ) || false == valId( $intCid ) || false == valArr( $objJobFilter->getPropertyIds() ) ) return NULL;

		self::createTempTableJobsByJobFilterByCid( $objJobFilter, $intCid, $objDatabase );

		$strSql = 'SELECT COUNT( 1 ) FROM temp_jobs_listing';

		return fetchData( $strSql, $objDatabase )[0]['count'];
	}

	public static function createTempTableJobsByJobFilterByCid( $objJobFilter, $intCid, $objDatabase, $boolIsFromJobTemplates = false ) {

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists( \'temp_jobs_listing\' ) AS result;', $objDatabase )[0]['result'] ) {
			return;
		}

		$strWhereCondition	= '';
		$strJoins			= ( true == $boolIsFromJobTemplates ) ? 'JOIN job_phases jp1 ON ( jp1.cid = j.cid AND jp1.job_id = j.id )' : '';

		$strSelectFields	= 'j.id,
								j.name,
								p.property_name,
								js.name as job_status,
								j.original_start_date,
								j.original_end_date,
								j.approved_on,
								ah1.transaction_amount as budget_amount,
								COALESCE( COALESCE(subquery.invoice_sum, 0) + COALESCE( subquery1.je_sum, 0 ), 0 ) as actual_amount,
								COALESCE( ah1.transaction_amount, 0 ) - COALESCE( COALESCE(subquery.invoice_sum, 0) + COALESCE( subquery1.je_sum, 0 ), 0 ) as cost_to_complete';

		if( true == $boolIsFromJobTemplates ) {
			$strSelectFields .= ',
								count( jp1.id ) AS phase_count,
								( SELECT
										array_to_string( array_agg ( distinct ac.name ), \', \' )
									FROM
										ap_details ad1
										LEFT JOIN ap_codes ac ON ( ac.cid = ad1.cid AND ac.id = ad1.ap_code_id )
									WHERE
										ad1.cid = ah1.cid AND ad1.ap_header_id = ah1.id
								) AS ap_code_name';
		}

		$strOrderByField		= 'js.order_num ASC';
		$arrstrSortByFields		= [ 'j.name', 'p.property_name', 'js.order_num', 'j.original_start_date', 'j.original_end_date', 'ah1.transaction_amount', 'actual_amount', 'cost_to_complete' ];

		$arrstrAndConditions	= self::fetchSearchCriteria( $intCid, $objJobFilter, $boolIsFromJobTemplates );

		if( true == valArr( $arrstrAndConditions ) ) {
			$strWhereCondition = implode( ' AND ', $arrstrAndConditions );
		}

		if( true == valStr( $objJobFilter->getOrderByFieldName() ) && true == in_array( $objJobFilter->getOrderByFieldName(), $arrstrSortByFields ) ) {
			if( 'actual_amount' == $objJobFilter->getOrderByFieldName() ) {
				$strOrderByField = 'COALESCE( COALESCE(subquery.invoice_sum, 0) + COALESCE( subquery1.je_sum, 0 ), 0 )' . ' ' . $objJobFilter->getOrderByType();
			} elseif( 'cost_to_complete' == $objJobFilter->getOrderByFieldName() ) {
				$strOrderByField = 'COALESCE( ah1.transaction_amount, 0 ) - COALESCE( COALESCE(subquery.invoice_sum, 0) + COALESCE( subquery1.je_sum, 0 ), 0 )' . ' ' . $objJobFilter->getOrderByType();
			} else {
				$strOrderByField = $objJobFilter->getOrderByFieldName() . ' ' . $objJobFilter->getOrderByType();
			}
		}

		$strSql = 'DROP TABLE IF EXISTS pg_temp.temp_jobs_listing;
					CREATE TEMP TABLE temp_jobs_listing AS (
						SELECT
							' . $strSelectFields . '
						FROM
							jobs j
							JOIN job_statuses js ON (j.job_status_id = js.id)
							JOIN properties p ON ( p.id = j.property_id AND p.cid = j.cid )
							JOIN ap_headers ah1 ON ( j.budget_summary_ap_header_id = ah1.id AND j.cid = ah1.cid )
							' . $strJoins . '
							LEFT OUTER JOIN
							(
								SELECT
									jp.job_id,
									jp.cid,
									sum(ad.transaction_amount) as invoice_sum
								FROM 
									job_phases jp
									JOIN ap_details ad ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
									JOIN ap_headers ah2 ON ( ah2.cid = ad.cid AND ah2.id = ad.ap_header_id )
								WHERE
									ah2.ap_header_type_id = ' . CApHeaderType::INVOICE . ' AND
									ah2.cid = ' . ( int ) $intCid . ' AND
									ah2.ap_header_sub_type_id IN ( ' . CApHeaderSubType::CATALOG_JOB_INVOICE . ',' . CApHeaderSubType::STANDARD_JOB_INVOICE . ') AND
									ah2.is_posted = true AND 
									ad.deleted_on IS NULL AND
									ah2.is_reversed = false
								GROUP BY
									jp.cid,
									jp.job_id
							) subquery on j.id = subquery.job_id AND j.cid = subquery.cid
							LEFT OUTER JOIN 
							(
								SELECT 
									gd.cid,
									jp.job_id,
									sum(gd.amount) as je_sum
								FROM 
									job_phases jp
									JOIN gl_details gd ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id AND gd.reclass_gl_detail_id IS NULL )
									JOIN gl_headers gh ON ( jp.cid = gh.cid AND gh.id = gd.gl_header_id AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' AND gh.reclass_gl_header_id IS NULL )
								WHERE
									gd.cid = ' . ( int ) $intCid . '
									AND gh.gl_header_status_type_id = ' . CGlHeaderStatusType::POSTED . '
									AND gh.is_template IS FALSE
								GROUP BY 
									gd.cid,
									jp.job_id
							) subquery1 on j.id = subquery1.job_id AND j.cid = subquery1.cid
						WHERE
							' . $strWhereCondition . '
						GROUP BY
							j.cid,
							j.id,
							p.property_name,
							js.name,
							js.order_num,
							ah1.id,
							ah1.cid,
							subquery.invoice_sum,
							subquery1.je_sum
						ORDER BY
							' . $strOrderByField . '
					)';

		fetchData( $strSql, $objDatabase );
	}

    public static function fetchSearchCriteria( $intCid, $objJobFilter, $boolIsFromJobTemplates ) {

        $arrstrAndConditions[] = ' j.cid = ' . ( int ) $intCid . ' AND j.property_id IN ( ' . sqlIntImplode( $objJobFilter->getPropertyIds() ) . ' )';

        if( true == valStr( $objJobFilter->getName() ) ) {
            $arrstrAndConditions[] = ' j.name ILIKE \'%' . addslashes( $objJobFilter->getName() ) . '%\' ';
        }

        if( true == valArr( $objJobFilter->getJobCategoryIds() ) ) {
            $arrstrAndConditions[] = ' j.job_category_id IN ( ' . sqlIntImplode( $objJobFilter->getJobCategoryIds() ) . ' )';
        }

        if( true == valArr( $objJobFilter->getJobStatusIds() ) ) {
            $arrstrAndConditions[] = ' j.job_status_id IN ( ' . sqlIntImplode( $objJobFilter->getJobStatusIds() ) . ' )';
        }

        if( true == valStr( $objJobFilter->getOriginalFromStartDate() ) ) {
            $arrstrAndConditions[] = ' DATE_TRUNC( \'day\', j.original_start_date ) >= \'' . date( 'Y-m-d', strtotime( $objJobFilter->getOriginalFromStartDate() ) ) . ' \'';
        }

        if( true == valStr( $objJobFilter->getOriginalToStartDate() ) ) {
            $arrstrAndConditions[] = ' DATE_TRUNC( \'day\', j.original_start_date ) <= \'' . date( 'Y-m-d', strtotime( $objJobFilter->getOriginalToStartDate() ) ) . ' \'';
        }

        if( true == valStr( $objJobFilter->getOriginalFromEndDate() ) ) {
            $arrstrAndConditions[] = ' DATE_TRUNC( \'day\', j.original_end_date ) >= \'' . date( 'Y-m-d', strtotime( $objJobFilter->getOriginalFromEndDate() ) ) . ' \'';
        }

        if( true == valStr( $objJobFilter->getOriginalToEndDate() ) ) {
            $arrstrAndConditions[] = ' DATE_TRUNC( \'day\', j.original_end_date ) <= \'' . date( 'Y-m-d', strtotime( $objJobFilter->getOriginalToEndDate() ) ) . ' \'';
        }

        if( true == $boolIsFromJobTemplates ) {
            $arrstrAndConditions[] = ' ah1.approved_on IS NOT NULL ';
        }

        return $arrstrAndConditions;
    }

	public static function fetchJobsWithPhaseCountByCid( $intCid, $objDatabase, $boolIncludeWorkingJobs = false ) {

		if( false == valId( $intCid ) ) return NULL;

		$strWhereCondition = ( false == $boolIncludeWorkingJobs ) ? ' AND j.approved_on IS NOT NULL' : '';

		$strSql = 'SELECT
						j.id,
						j.name,
						j.property_id,
						count(jp.id) AS phase_count,
						CASE WHEN true = j.use_retention AND 1 = cp.value::INTEGER THEN 1 ELSE 0 END as use_retention
					FROM
						jobs j
						LEFT JOIN job_phases jp ON ( j.id = jp.job_id AND j.cid = jp.cid )
						LEFT JOIN company_preferences cp ON ( cp.cid = j.cid AND cp.key = \'USE_RETENTION\' )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.job_status_id NOT IN( ' . sqlIntImplode( [ CJobStatus::COMPLETED, CJobStatus::CANCELLED, CJobStatus::ON_HOLD ] ) . ' )'
						. $strWhereCondition . '
					GROUP BY
						j.id,
						j.cid,
						cp.value
					ORDER BY j.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobIdsAndJobNamesByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT j.id AS job_id, j.name AS job_name FROM jobs j WHERE j.cid = ' . ( int ) $intCid . ' AND j.job_type_id != ' . CJobType::JOB_TYPE_TEMPLATE;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchJobContractByJobIdOrByContractIdByCid( $intCid, $objDatabase, $intJobId = NULL, $intContractId = NULL ) {

		if( false == valId( $intCid ) ) return NULL;

		if( false == valId( $intJobId ) && false == valId( $intContractId ) ) return NULL;

		$strSubSql = ( true == valId( $intContractId ) ) ? 'apc.id = ' . ( int ) $intContractId : 'j.id = ' . ( int ) $intJobId;

		$strSql = 'SELECT
						j.property_id,
						j.id AS job_id,
						apc.ap_payee_id AS vendor_id,
						ba.bank_account_type_id,
						ba.reimbursed_property_id,
						p.property_name AS property_name,
						apc.name AS contract_name,
						j.name AS job_name,
						ap.company_name AS vendor_name,
						ap.ap_payee_term_id AS payment_term,
						to_char( pgs.ap_post_month, \'MM/YYYY\') AS ap_post_month,
						pgs.is_ap_migration_mode,
						apc.retention_percent,
						CASE WHEN true = j.use_retention AND 1 = cp.value::INTEGER THEN true ELSE false END as use_retention
					FROM
						jobs j
						JOIN properties p ON ( p.cid = j.cid AND p.id = j.property_id )
						JOIN property_gl_settings pgs ON ( p.cid = pgs.cid AND p.id = pgs.property_id )
						LEFT JOIN bank_accounts ba ON ( pgs.cid = ba.cid AND pgs.ap_bank_account_id = ba.id )
						LEFT JOIN ap_contracts apc ON ( apc.cid = j.cid AND apc.job_id = j.id )
						LEFT JOIN ap_payees ap ON ( ap.cid = j.cid AND ap.id = apc.ap_payee_id )
						LEFT JOIN company_preferences cp ON ( cp.cid = j.cid AND cp.key = \'USE_RETENTION\' )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.job_status_id NOT IN( ' . CJobStatus::CANCELLED . ' )
						AND ' . $strSubSql . ' LIMIT 1';

		return fetchData( $strSql, $objDatabase )[0];
	}

	public static function fetchJobsByJobStatusIdsByCid( $arrintJobStatusIds, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintJobStatusIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						jobs
					WHERE
						cid = ' . ( int ) $intCid . '
						AND job_status_id IN ( ' . implode( ',', $arrintJobStatusIds ) . ' )';

		return self::fetchJobs( $strSql, $objDatabase );
	}

	public static function fetchJobByApContractIdByCid( $intApContractId, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intApContractId ) ) return NULL;

		$strSql = 'SELECT
						j.*
					FROM
						jobs j
						JOIN ap_contracts ac ON ( j.cid = ac.cid AND j.id = ac.job_id )
					WHERE
						ac.cid = ' . ( int ) $intCid . '
						AND ac.id = ' . ( int ) $intApContractId . '
						AND j.cancelled_by IS NULL';

		return self::fetchJob( $strSql, $objDatabase );
	}

	public static function fetchAllJobsByCid( $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						j.*
					FROM
						jobs j
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.cancelled_by IS NULL
					ORDER BY 
						j.name ASC,
						j.created_on DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobsByPropertyIdsByCid( $arrintPropertyGroupIds = [], $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;
		$strSql = '	SELECT
						j.id,
						j.name
					FROM
						jobs j
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $arrintPropertyGroupIds, ',' ) . '] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = j.property_id )						
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.cancelled_by IS NULL';
		$strSql .= ' ORDER BY
						j.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchExistingJobsByJobStatusIdsByCid( $arrintJobStatusIds, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintJobStatusIds ) ) return NULL;

		$strSql = 'SELECT
					distinct j.id,
					j.name,
					j.approved_on,
					p.property_name,
					js.name AS job_status,
					ah.transaction_amount AS budget_amount,
					array_to_string ( array_agg ( distinct ac.name ), \', \' ) AS ap_code_name,
					COUNT( ac.id ) AS ap_code_count,
					COUNT( jp.id ) OVER ( PARTITION BY j.id ) AS phase_count
				FROM
					jobs j
					JOIN job_statuses js ON ( j.job_status_id = js.id )
					JOIN job_phases jp ON ( j.cid = jp.cid AND j.id = jp.job_id )
					JOIN properties p ON ( p.cid = j.cid AND p.id = j.property_id )
					JOIN ap_headers ah ON ( j.cid = ah.cid AND j.budget_summary_ap_header_id = ah.id AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_BUDGET_SUMMARY . ' )
					JOIN ap_details ad ON ( ah.cid = ad.cid AND ad.ap_header_id = ah.id AND ad.deleted_on IS NULL )
					LEFT JOIN ap_codes ac ON ( ad.cid = ac.cid AND ad.ap_code_id = ac.id )
				WHERE
					j.cid = ' . ( int ) $intCid . '
					AND j.approved_on IS NOT NULL
					AND ah.deleted_on IS NULL
					AND ah.approved_on IS NOT NULL
					AND j.job_status_id IN ( ' . implode( ', ', $arrintJobStatusIds ) . ' )
					AND ad.transaction_amount > 0
				GROUP BY
					j.id,
					j.cid,
					p.property_name,
					js.name,
					ah.transaction_amount,
					jp.id
				ORDER BY j.name ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTemplatePhasesWithJobCostCodesByIdByCid( $intId, $intCid, $objDatabase, $boolIsTemplate = false ) {
		if( false == valId( $intId ) || false == valId( $intCid ) ) return NULL;

		$strWhereCondition	= ( true == $boolIsTemplate ) ? ' AND j.job_type_id = ' . CJobType::JOB_TYPE_TEMPLATE . ' AND j.id = ' . ( int ) $intId : ' AND j.id = ' . ( int ) $intId;

		$strSql = 'SELECT 
						DISTINCT ON (jp.id) ad.job_phase_id,
						jp.name,
						jp.original_start_date,
						jp.original_end_date,
						json_object_agg( ad.ap_code_id, ap.name ) OVER( PARTITION BY jp.id ) AS job_cost_codes
					FROM 
						jobs j
						JOIN job_phases jp on ( j.cid = jp.cid and j.id = jp.job_id )
						JOIN ap_details ad on ( jp.cid = ad.cid and jp.id = ad.job_phase_id AND ad.deleted_on IS NULL )
						JOIN ap_headers ah on ( ah.cid = ad.cid and ah.id = ad.ap_header_id )
						JOIN ap_codes ap ON ( ap.cid = ad.cid AND ap.id = ad.ap_code_id )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND ( ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_CHANGE_ORDER . ' AND ad.approved_on IS NOT NULL ) OR ah.ap_header_sub_type_id in ( ' . CApHeaderSubType::JOB_PHASE_BUDGET . ',' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) )'
						. $strWhereCondition;

		return  fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobPhasesAndJobsByCid( CFilterPropertyGroupIds $objPropertyGroupIds, array $arrintJobStatusIds = [], int $intCid, CDatabase $objDatabase ) : array {

		if( false == valId( $intCid ) ) return [];

		if( true == valArr( $arrintJobStatusIds ) ) {
			$strJobStatusWhereSql = ' AND j.job_status_id IN( ' . implode( ',', $arrintJobStatusIds ) . ' )';
		} else {
			$strJobStatusWhereSql = '';
		}

		$strSql = 'SELECT
						j.name AS group,
						jp.id,
						jp.name
					FROM
						jobs j
						JOIN job_phases jp ON ( jp.cid = j.cid AND jp.job_id = j.id )
						JOIN ' . $objPropertyGroupIds->getJoinSql( 'j.property_id' ) . '
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.cancelled_by IS NULL
						AND j.job_type_id <> ' . CJobType::JOB_TYPE_TEMPLATE . '
						' . $strJobStatusWhereSql . '
					ORDER BY
						j.name,
						jp.name';

		return ( array ) nestArrayByKeys( fetchData( $strSql, $objDatabase ) );

	}

	public static function fetchJobsByPhasesByCid( $arrintJobPhaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintJobPhaseIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT j.id
					FROM
						jobs j
						JOIN job_phases jp ON ( jp.cid = j.cid AND jp.job_id = j.id  )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND jp.id IN ( ' . sqlIntImplode( $arrintJobPhaseIds ) . ' )
					ORDER BY
						j.id';

		return  fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobsDataByCid( $arrintPropertyGroupIds = [], $arrintJobStatusIds = [], $intCid, $objDatabase ) {
		if( false == valId( $intCid ) ) return NULL;

		if( true == valArr( $arrintJobStatusIds ) ) {
			$strJobStatusWhereSql = ' AND j.job_status_id IN( ' . implode( ',', $arrintJobStatusIds ) . ' )';
		} else {
			$strJobStatusWhereSql = '';
		}

		$strSql = 'SELECT
						j.id,
						j.name
					FROM
						jobs j
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $arrintPropertyGroupIds, ',' ) . '] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = j.property_id )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.job_type_id <> ' . CJobType::JOB_TYPE_TEMPLATE . '
						AND j.cancelled_by IS NULL
						' . $strJobStatusWhereSql . '
					ORDER BY
						j.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobPhasesAndJobsByJobStatusByCid( $arrintJobStatusIds = [], $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return NULL;

		if( true == valArr( $arrintJobStatusIds ) ) {
			$strJobStatusWhereSql = ' AND j.job_status_id IN( ' . implode( ',', $arrintJobStatusIds ) . ' )';
		} else {
			$strJobStatusWhereSql = '';
		}

		$strSql = 'SELECT
						j.name AS group,
						jp.id,
						jp.name
					FROM
						jobs j
						JOIN job_phases jp ON ( jp.cid = j.cid AND jp.job_id = j.id )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.job_type_id <> ' . CJobType::JOB_TYPE_TEMPLATE . '
						AND j.cancelled_by IS NULL
						' . $strJobStatusWhereSql . '
					ORDER BY
						j.name,
						jp.name';

		return nestArrayByKeys( fetchData( $strSql, $objDatabase ) );

	}

	public static function fetchJobNamesByJobPhasesByCid( $arrintJobPhases, $intCid, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT j.id,
						j.name
					FROM
						jobs j
						JOIN job_phases jp ON ( jp.cid = j.cid AND jp.job_id = j.id  )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND jp.id IN ( ' . implode( ', ', $arrintJobPhases ) . ' )
					ORDER BY
						j.name ASC';

		return  fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobCountByJobCategoryIdByCid( $intJobCategoryId, $intCid, $objDatabase ) {

		$strWhereSql = 'WHERE job_category_id = ' . ( int ) $intJobCategoryId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchJobCount( $strWhereSql, $objDatabase );

	}

	public static function fetchJobsByPropertyIdByJobStatusIdsByCidOrderByJobName( $arrintPropertyGroupIds, $arrintJobStatusIds, $intCid, $objDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintJobStatusIds ) || false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						jobs j
						JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( $arrintPropertyGroupIds, ',' ) . '] ) lp ON ( lp.is_disabled = 0 AND lp.property_id = j.property_id )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.job_type_id <> ' . CJobType::JOB_TYPE_TEMPLATE . '
						AND j.job_status_id IN ( ' . implode( ',', $arrintJobStatusIds ) . ' )
					ORDER BY 
						j.name ASC';

		return self::fetchJobs( $strSql, $objDatabase );
	}

	public static function fetchJobBudgetDetailsByIdsByCid( $arrintJobIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintJobIds ) ) return NULL;

		$strSql = 'SELECT
					j.id,
				    j.name,
				    j.original_start_date,
				    j.original_end_date,
				    p.property_name,
				    ah.transaction_amount
				FROM 
					jobs j
					join ap_headers ah ON ( ah.cid = j.cid AND j.budget_summary_ap_header_id = ah.id AND ah.deleted_on IS NULL )
					JOIN properties p ON ( j.cid = p.cid AND j.property_id = p.id )
				WHERE 
					j.id IN ( ' . implode( ',', $arrintJobIds ) . ' )
					AND j.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActiveUseRetentionJobsByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT * FROM  jobs WHERE cid = ' . ( int ) $intCid . ' AND use_retention = TRUE';

		return parent::fetchJobs( $strSql, $objDatabase );

	}

	public static function fetchPhasesByIdsByCid( $arrintPhaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPhaseIds ) ) return NULL;

			$strSql = 'SELECT
							jp.id,
							j.name || \' : \' || jp.name  AS name
						FROM 
							job_phases jp
							JOIN jobs j on ( jp.cid = j.cid AND jp.job_id = j.id ) 
						WHERE 
							jp.cid = ' . ( int ) $intCid . '
							AND jp.id IN ( ' . implode( ', ', $arrintPhaseIds ) . ' )
						ORDER BY 
							j.name, jp.name';

			return fetchData( $strSql, $objDatabase );

	}

	public static function fetchJobBudgetAmountWithContractedAmountWithActualBudgetAmountByJobIdByCid( $intJobId, $intCid, $objDatabase, $intContractId = NULL ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strContractJoinCondition  = ( false == valId( $intContractId ) ) ? ' AND apc.ap_contract_status_id <> ' . CApContractStatus::CANCELLED : '';
		$strContractWhereCondition = ( true == valId( $intContractId ) ) ? ' AND apc.id = ' . ( int ) $intContractId : '';

		$strSql = 'SELECT
						sub.cid,
						sub.job_id,
						sum ( sub.unfinalize_budget_amount ) AS unfinalize_budget_amount,
						sum ( sub.budget_amount ) AS budget_amount,
						sum ( sub.contracted_amount ) AS contracted_amount,
						sum ( sub.actual_amount ) AS actual_amount,
						array_length ( array 
						(
							SELECT
								unnest ( string_to_array ( string_agg ( sub.actual_ap_code_ids, \',\' ), \',\' ) )
							EXCEPT
							SELECT
								unnest ( string_to_array ( string_agg ( sub.budgeted_ap_code_ids, \',\' ), \',\' ) )
						), 1 ) AS non_budgeted_ap_code_count
					FROM
						(
							SELECT
								DISTINCT jp.job_id,
								jp.cid,
								SUM ( CASE
										WHEN ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' THEN ad.transaction_amount
										ELSE 0
										END 
									) OVER ( PARTITION BY jp.cid, jp.job_id ) AS unfinalize_budget_amount,
								SUM ( CASE
										WHEN ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_PHASE_BUDGET . ', ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) AND ah.approved_on IS NOT NULL THEN ad.transaction_amount
										ELSE 0
										END 
									) OVER ( PARTITION BY jp.cid, jp.job_id ) AS budget_amount,
								SUM ( CASE
										WHEN ( ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::CONTRACT_CHANGE_ORDER . ' ) AND ah.approved_on IS NOT NULL AND ad.ap_contract_id IS NOT NULL AND apc.id IS NOT NULL ) THEN ad.transaction_amount
										WHEN ah.ap_header_sub_type_id = ' . CApHeaderSubType::CONTRACT_BUDGET . ' AND apc.id IS NOT NULL THEN ad.transaction_amount
										ELSE 0
										END 
								) OVER ( PARTITION BY jp.cid, jp.job_id ) AS contracted_amount,
								SUM ( CASE
										WHEN ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' AND ah.is_posted IS TRUE ) THEN ad.transaction_amount
										ELSE 0
										END 
									) OVER ( PARTITION BY jp.cid, jp.job_id ) AS actual_amount,
								string_agg ( ( CASE
													WHEN ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' OR ( ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) AND ah.approved_on IS NOT NULL ) THEN coalesce ( ad.ap_code_id, 0 )
												END 
											)::TEXT, \',\' ) OVER ( PARTITION BY jp.cid, jp.job_id ) AS budgeted_ap_code_ids,
								string_agg ( ( CASE
													WHEN ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' AND ah.is_posted IS TRUE ) THEN coalesce ( ad.ap_code_id, 0 )
												END 
											)::TEXT, \',\' ) OVER ( PARTITION BY jp.cid, jp.job_id ) AS actual_ap_code_ids
							FROM
								job_phases jp
								JOIN ap_details ad ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id AND ad.deleted_on IS NULL AND ad.reversal_ap_detail_id IS NULL )
								JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ah.reversal_ap_header_id IS NULL AND ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_PHASE_BUDGET . ', ' . CApHeaderSubType::CONTRACT_BUDGET . ', ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::CONTRACT_CHANGE_ORDER . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ', ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' ) AND ah.deleted_on IS NULL )
								LEFT JOIN ap_contracts apc ON ( apc.cid = ad.cid AND apc.job_id = jp.job_id AND apc.id = ad.ap_contract_id ' . $strContractJoinCondition . ' )
							WHERE
								jp.cid = ' . ( int ) $intCid . '
								AND jp.job_id = ' . ( int ) $intJobId . '
								' . $strContractWhereCondition . '

							UNION

							SELECT
								DISTINCT jp.job_id,
								jp.cid,
								0 AS unfinalize_budget_amount,
								0 AS budget_amount,
								0 AS contracted_amount,
								SUM ( gd.amount ) OVER ( PARTITION BY jp.cid, jp.job_id ) AS actual_amount,
								string_agg ( 0::TEXT, \',\' ) OVER ( PARTITION BY jp.cid, jp.job_id ) AS budgeted_ap_code_ids,
								string_agg ( coalesce ( gd.ap_code_id::TEXT, 0::TEXT ), \',\' ) OVER ( PARTITION BY jp.cid, jp.job_id ) AS actual_ap_code_ids
							FROM
								job_phases jp
								JOIN gl_details gd ON ( jp.cid = gd.cid AND jp.id = gd.job_phase_id AND gd.reclass_gl_detail_id IS NULL )
								JOIN gl_headers gh ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id AND gh.reclass_gl_header_id IS NULL AND gh.gl_header_type_id = ' . CGlHeaderType::STANDARD . ' AND gh.gl_transaction_type_id = ' . CGlTransactionType::GL_GJ . ' AND gh.gl_header_status_type_id = ' . CGlHeaderStatusType::POSTED . ' )
								LEFT JOIN ap_contracts apc ON ( apc.cid = gd.cid AND apc.job_id = jp.job_id AND apc.id = gd.ap_contract_id ' . $strContractJoinCondition . ' )
							WHERE
								jp.cid = ' . ( int ) $intCid . '
								AND jp.job_id = ' . ( int ) $intJobId . '
								' . $strContractWhereCondition . '
								AND gh.is_template IS FALSE
						) AS sub
					GROUP BY
						sub.cid,
						sub.job_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchJobNamesContractNamesByApCodeIdsByCid( $arrintApCodeIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApCodeIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						j.name As job_name,
						NULL As contract_name,
						ac.id As ap_code_id
					FROM 
						ap_codes ac
						JOIN ap_details ad ON( ad.ap_code_id = ac.id AND ad.cid = ac.cid AND ad.job_phase_id IS NOT NULL )
						JOIN ap_headers ah ON( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
						JOIN job_phases jp ON( jp.id = ad.job_phase_id AND jp.cid = ad.cid )
						JOIN jobs j ON( j.id = jp.job_id AND j.cid = jp.cid )
						LEFT JOIN ap_contracts apc ON( apc.id = ad.ap_contract_id AND j.id = apc.job_id  AND apc.cid = ad.cid )
					WHERE
						ac.id IN ( ' . implode( ',', $arrintApCodeIds ) . ' )
						AND ac.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' 
						AND j.job_status_id NOT IN( ' . CJobStatus::COMPLETED . ', ' . CJobStatus::CANCELLED . ', ' . CJobStatus::CLOSED . ' )
					GROUP BY
						j.name,
						ac.id

					UNION

					SELECT
						j.name As job_name,
						apc.name As contract_name,
						ac.id As ap_code_id
					FROM 
						ap_codes ac
						JOIN ap_details ad ON( ad.ap_code_id = ac.id AND ad.cid = ac.cid AND ad.job_phase_id IS NOT NULL )
						JOIN ap_headers ah ON( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
						JOIN job_phases jp ON( jp.id = ad.job_phase_id AND jp.cid = ad.cid )
						JOIN jobs j ON( j.id = jp.job_id AND j.cid = jp.cid )
						LEFT JOIN ap_contracts apc ON( apc.id = ad.ap_contract_id AND j.id = apc.job_id  AND apc.cid = ad.cid )
					WHERE
						ac.id IN ( ' . implode( ',', $arrintApCodeIds ) . ' )
						AND ac.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::CONTRACT_BUDGET . '
						AND apc.ap_contract_status_id = ' . CApContractStatus::ACTIVE . '
					GROUP BY 
						ac.id,
						j.name,
						apc.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobNamesContractNameAssociatedWithChangeOrderByApCodeIdsByCid( $arrintApCodeIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApCodeIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ac.id AS ap_code_id,
						bco.id AS budget_change_order_id,
						j.name AS co_job_name,
						apc.name AS co_contract_name
					FROM
						ap_codes ac
						JOIN ap_details ad ON( ad.ap_code_id = ac.id AND ad.cid = ac.cid AND ad.job_phase_id IS NOT NULL )
						JOIN ap_headers ah ON( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
						JOIN job_phases jp ON( jp.id = ad.job_phase_id AND jp.cid = ad.cid )
						JOIN jobs j ON( j.id = jp.job_id AND j.cid = jp.cid )
						LEFT JOIN ap_contracts  apc ON( apc.id = ad.ap_contract_id AND j.id = apc.job_id  AND apc.cid = ad.cid )
						JOIN budget_change_orders bco ON( bco.job_id = j.id AND bco.cid = j.cid AND bco.id = ah.budget_change_order_id )
					WHERE
						ac.id IN ( ' . implode( ',', $arrintApCodeIds ) . ' )
						AND ac.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_sub_type_id IN( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::CONTRACT_CHANGE_ORDER . ' )
						AND j.job_status_id NOT IN( ' . CJobStatus::COMPLETED . ', ' . CJobStatus::CANCELLED . ', ' . CJobStatus::CLOSED . ' )
						AND bco.approved_on IS NULL
					GROUP BY
						ac.id,
						bco.id,
						j.name,
						apc.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobAssociatedUnitTypeAndMaintenanceLocationsByJobIdByCid( $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$strSql = '( SELECT
						\'maintenance_location\' as budget_type,
						ml.id as type_id,
                        ml.name as budget_type_name
					FROM 
						job_maintenance_locations jml
                        JOIN maintenance_locations ml ON ( ml.cid = jml.cid AND ml.id = jml.maintenance_location_id )
					WHERE jml.cid = ' . ( int ) $intCid . ' AND
                        jml.job_id = ' . ( int ) $intJobId . '  )
                UNION
                ( SELECT 
					\'unit_type\' as budget_type,
					ut.id as type_id,
                    ut.name AS budget_type_name
				FROM 
					job_unit_type_phases jutp
                    JOIN unit_types ut ON ( ut.cid = jutp.cid AND ut.id = jutp.unit_type_id )
				WHERE 
					jutp.job_id = ' . ( int ) $intJobId . '  AND
                    jutp.cid = ' . ( int ) $intCid . '  )';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchJobPhasesByJobsByCid( $arrintJobIds, $intCid, $objClientDatabase ) {

		if( false == valId( $intCid ) || false == valArr( $arrintJobIds ) ) return NULL;

		$strSql = '	SELECT
						jp.id,
						jp.name,
						jp.job_id
					FROM 
						job_phases jp
					WHERE 
						jp.cid = ' . ( int ) $intCid . '
						AND jp.job_id IN ( ' . implode( ', ', $arrintJobIds ) . ' )
					ORDER BY 
						jp.job_id,
						jp.id,
						jp.name';

		return fetchdata( $strSql, $objClientDatabase );

	}

	public static function fetchJobTemplatesWithPhasesAndJobCostCodesByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( jp.id ) j.id AS job_template_id,
						j.name AS template_name,
						jp.id AS job_phase_id,
						jp.name AS phase_name,
						jp.original_start_date,
						jp.original_end_date,
						json_object_agg( ad.ap_code_id, ac.name ) OVER ( PARTITION BY jp.id ) AS job_cost_codes
					FROM
						jobs j
						JOIN job_phases jp ON ( j.cid = jp.cid AND j.id = jp.job_id )
						JOIN ap_details ad ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
						JOIN ap_codes ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_code_id )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.job_type_id = ' . CJobType::JOB_TYPE_TEMPLATE . '
						AND j.cancelled_on IS NULL
						AND ad.deleted_on IS NULL
						AND ac.name IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobTemplatesCountByCid( $intCid, $objDatabase ) {

		if( false == valId( $intCid ) ) return 0;

		self::createTempTableJobTemplatesByCid( $intCid, $objDatabase );

		$strSql	= 'SELECT COUNT( 1 ) FROM temp_job_templates_listing';

		return fetchData( $strSql, $objDatabase )[0]['count'];
	}

	public static function fetchPaginatedJobTemplatesByCid( $intCid, $objDatabase, $objPagination = NULL ) {

		if( false == valId( $intCid ) ) return NULL;

		self::createTempTableJobTemplatesByCid( $intCid, $objDatabase );

		$strSql	= 'SELECT * FROM temp_job_templates_listing';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' LIMIT ' . ( int ) $objPagination->getPageSize() . ' OFFSET ' . ( int ) $objPagination->getOffset();
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function createTempTableJobTemplatesByCid( $intCid, $objDatabase ) {

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists( \'temp_job_templates_listing\' ) AS result;', $objDatabase )[0]['result'] ) {
			return;
		}

		$strSql = 'DROP TABLE IF EXISTS pg_temp.temp_job_templates_listing;
					CREATE TEMP TABLE temp_job_templates_listing AS (
					SELECT
						j.id,
						j.name,
						j.description,
						COUNT ( DISTINCT jp.id ) AS phase_count,
						COUNT( DISTINCT ad.ap_code_id ) AS job_cost_code_count,
						JSON_AGG( DISTINCT ac.name ) AS job_cost_codes
					FROM
						jobs j
						LEFT JOIN job_phases jp ON ( j.cid = jp.cid AND j.id = jp.job_id )
						LEFT JOIN ap_headers ah ON ( jp.cid = ah.cid AND ah.job_phase_id = jp.id )
						LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ad.deleted_on IS NULL )
						LEFT JOIN ap_codes ac ON ( ad.cid = ac.cid AND ad.ap_code_id = ac.id )
					WHERE
						j.cid = ' . ( int ) $intCid . '
						AND j.job_type_id = ' . CJobType::JOB_TYPE_TEMPLATE . '
						AND j.cancelled_by IS NULL
						AND j.cancelled_on IS NULL
					GROUP BY
						j.id,
						j.name,
						j.description
					ORDER BY j.name ASC
				)';

		fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobChangeOrdersAndAdjustmentsCountByJobIdByCid( $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intJobId ) ) return NULL;

		$strSql = ' SELECT
						COALESCE( SUM ( total_count ), 0 ) AS total_sum
					FROM
						(
						SELECT
							COUNT( id ) AS total_count
						FROM
							budget_change_orders
						WHERE
							cid = ' . ( int ) $intCid . '
							AND job_id = ' . ( int ) $intJobId . '
							AND cancelled_on IS NULL

						UNION

						SELECT
							COUNT( ba.id ) AS total_count
						FROM
							budget_adjustments ba
							JOIN ap_headers ah ON ( ba.cid = ah.cid AND ah.deleted_on IS NULL AND ah.id = ba.from_ap_header_id AND ah.ap_financial_status_type_id <> ' . CApFinancialStatusType::CANCELLED . ' )
						WHERE
							ba.cid = ' . ( int ) $intCid . '
							AND ba.job_id = ' . ( int ) $intJobId . '
						) AS SUB';

		return ( 0 == fetchData( $strSql, $objDatabase )[0]['total_sum'] );
	}

}
?>