<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlAccountProperties
 * Do not add any new functions to this class.
 */

class CGlAccountProperties extends CBaseGlAccountProperties {

	public static function fetchSimpleGlAccountPropertiesByGlAccountIdByCid( $intGlAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gap.*,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS gl_account_name,
						gat.account_number AS gl_account_number
					FROM
						gl_account_properties gap
						JOIN gl_account_trees gat ON ( gap.cid = gat.cid AND gap.gl_account_id = gat.gl_account_id )
					WHERE
						gap.cid = ' . ( int ) $intCid . '
						AND gap.gl_account_id = ' . ( int ) $intGlAccountId;

		return self::fetchGlAccountProperties( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleGlAccountPropertiesByCid( $intCid, $objClientDatabase ) {
		return fetchData( 'SELECT * FROM gl_account_properties WHERE cid = ' . ( int ) $intCid, $objClientDatabase );
	}

	public static function fetchGlAccountPropertiesByGlAccountIdsByCid( $arrintGlAccountIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlAccountIds ) ) return NULL;

		$strSql = 'SELECT
						gap.*,
						pga.property_id
					FROM
						gl_account_properties gap
						LEFT JOIN property_group_associations pga ON ( gap.cid = pga.cid AND pga.property_group_id = gap.property_group_id )
						JOIN properties p ON ( p.cid = pga.cid AND p.id = pga.property_id )
					WHERE
						gap.cid = ' . ( int ) $intCid . '
						AND gap.gl_account_id IN ( ' . implode( ',', $arrintGlAccountIds ) . ' )
						AND p.property_type_id NOT IN ( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.disabled_on IS NULL';

		return self::fetchGlAccountProperties( $strSql, $objClientDatabase, false );
	}

	public static function fetchGlAccountPropertiesByOldAndNewGlAccountIdByCid( $intOldGlAccountId, $intNewGlAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gap1.*
					FROM
						gl_account_properties gap1
					WHERE
						gap1.cid = ' . ( int ) $intCid . '
						AND gap1.gl_account_id = ' . ( int ) $intNewGlAccountId . '
						AND EXISTS (
										SELECT
											1
										FROM
											gl_account_properties gap2
										WHERE
											gap2.cid = ' . ( int ) $intCid . '
											AND gap2.gl_account_id = ' . ( int ) $intOldGlAccountId . '
											AND gap1.property_group_id = gap2.property_group_id
									)';

		return self::fetchGlAccountProperties( $strSql, $objClientDatabase );
	}

	public static function fetchGlAccountPropertiesByApCodeIdByCid( $intApCodeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApCodeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						gap.*,
						pga.property_id
					FROM
						gl_account_properties gap
						LEFT JOIN property_group_associations pga ON ( gap.cid = pga.cid AND gap.property_group_id = pga.property_group_id )
						JOIN properties p ON ( gap.cid = p.cid AND gap.property_group_id = p.id )
						JOIN ap_codes ac ON ( ac.cid = gap.cid AND ( ac.consumption_gl_account_id = gap.gl_account_id OR ac.item_gl_account_id = gap.gl_account_id ) )
					WHERE
						gap.cid = ' . ( int ) $intCid . '
						AND ac.id = ' . ( int ) $intApCodeId . '
						AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' )
						AND p.disabled_on IS NULL';

		return self::fetchGlAccountProperties( $strSql, $objClientDatabase );

	}

	public static function fetchCustomGlAccountPropertiesByPropertyGroupIdByCid( $intGlTreeId, $arrintPropertyGroupIds, $intCid, $objClientDatabase, $arrintGlAccountTypeIds = [] ) {
		$strGlAccountTypeIdsCondition = ( true == valArr( $arrintGlAccountTypeIds ) ) ? ' AND gat.gl_account_type_id IN( ' . implode( ',', $arrintGlAccountTypeIds ) . ' )' : NULL;
		$strSql = 'SELECT 
					sub.grouping_gl_account_id,
					sub.account_number,
					sub.display_name
					FROM
					( SELECT
						DISTINCT
						gat.gl_account_id AS grouping_gl_account_id,
						gat.account_number,
						gat.formatted_account_number || \': \' || gat.name AS display_name
					FROM
						gl_account_trees gat
					JOIN gl_account_properties gap ON ( gap.cid = gat.cid AND gap.gl_account_id = gat.gl_account_id )
					JOIN load_properties ( ARRAY [ ' . ( int ) $intCid . ' ]::INT [ ], ARRAY [ ' . implode( ',', $arrintPropertyGroupIds ) . ' ]::INT [ ], ARRAY [ 1 ]::INT [ ] ) load_prop ON load_prop.property_id = gap.property_group_id AND load_prop.is_disabled = 0 AND load_prop.is_test = 0
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.hide_account_balance_from_reports = 0
						AND gap.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' )
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						' . $strGlAccountTypeIdsCondition . '
					UNION ALL
					SELECT
						DISTINCT
						gat.gl_account_id AS grouping_gl_account_id,
						gat.account_number,
						gat.formatted_account_number || \': \' || gat.name AS display_name
					FROM
						gl_account_trees gat
					LEFT JOIN gl_account_properties gap ON ( gap.cid = gat.cid AND gap.gl_account_id = gat.gl_account_id )
					WHERE
						gat.cid = ' . ( int ) $intCid . '
						AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND gap.gl_account_id IS NULL) as sub
					ORDER BY
					sub.account_number';
		return fetchData( $strSql, $objClientDatabase );
	}

}
?>