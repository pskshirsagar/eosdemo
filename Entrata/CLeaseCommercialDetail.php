<?php

class CLeaseCommercialDetail extends CBaseLeaseCommercialDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMasterLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNoticePriorCustomerAddressId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNoticeAfterCustomerAddressId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCobrokerName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCobrokerPhone() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCobrokerAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseTaxYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseExpensesYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTenantDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOperatingHours() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCamExpenses() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResponsibleUtilities() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResponsibleJanitorial() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResponsibleSecurity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>