<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRevenueScheduledChargeTypes
 * Do not add any new functions to this class.
 */

class CRevenueScheduledChargeTypes extends CBaseRevenueScheduledChargeTypes {

    public static function fetchRevenueScheduledChargeTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CRevenueScheduledChargeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchRevenueScheduledChargeType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CRevenueScheduledChargeType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }
}
?>