<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultImplementationChecklists
 * Do not add any new functions to this class.
 */

class CDefaultImplementationChecklists extends CBaseDefaultImplementationChecklists {

    public static function fetchDefaultImplementationChecklists( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CDefaultImplementationChecklist', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchDefaultImplementationChecklist( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CDefaultImplementationChecklist', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

 	public static function fetchRecursiveDefaultImplementationChecklistsByCid( $intCid, $objDatabase, $boolIsReturnRawData = false ) {
 		if( true == empty( $intCid ) ) return NULL;

 		$strSql = ' With RECURSIVE corporate_settings( module_id, title, parent_module_id, url, module_name ) AS (
						SELECT
    						DISTINCT m.id AS module_id,
    						util_get_translated( \'title\', m.title, m.details ) AS title,
    						m.parent_module_id,
    						m.url,
 							m.name AS module_name
						FROM
    						default_implementation_checklists dic
    						JOIN modules m ON ( m.id = dic.module_id )
						UNION ALL
						SELECT
    						m.id AS module_id,
    						util_get_translated( \'title\', m.title, m.details ) AS title,
    						m.parent_module_id,
    						m.url,
 				 			m.name AS module_name
						FROM
    						modules m,
 				    		corporate_settings cs
						WHERE
    						m.id = cs.parent_module_id
					)
					SELECT
    					DISTINCT ON ( module_id ) cs.*,
 						CASE WHEN TRUE = ic.is_implemented AND TRUE = ic.is_completion_recommended THEN 3
 							 WHEN TRUE = ic.is_implemented AND FALSE = ic.is_completion_recommended THEN 2
 							 WHEN TRUE = ic.is_completion_recommended THEN 1
        					 ELSE 0
 						END AS check_mark_state
					FROM
    					corporate_settings cs
 						LEFT JOIN implementation_checklists ic ON ( ic.module_id = cs.module_id AND ic.cid = ' . ( int ) $intCid . ' )';

 		$arrstrDefaultImplementationChecklists	= fetchData( $strSql, $objDatabase );

	    // Return raw data.
		if( true == $boolIsReturnRawData ) return $arrstrDefaultImplementationChecklists;

 		$intCorporateChecklistCount 			= 0;

 		if( true == valArr( $arrstrDefaultImplementationChecklists ) ) {
 			foreach( $arrstrDefaultImplementationChecklists as $arrstrDefaultImplementationChecklist ) {
 				if( false == isset( $arrstrCorporateChecklists[CModule::COMPANY_SETUP][$arrstrDefaultImplementationChecklist['parent_module_id']] ) ) {
 					$arrstrCorporateChecklists[$arrstrDefaultImplementationChecklist['parent_module_id']][$arrstrDefaultImplementationChecklist['module_id']] = $arrstrDefaultImplementationChecklist;
 				} else {
 					$arrstrCorporateChecklists[CModule::COMPANY_SETUP][$arrstrDefaultImplementationChecklist['parent_module_id']]['child_modules'][$arrstrDefaultImplementationChecklist['module_id']] = $arrstrDefaultImplementationChecklist;
 				}

 				if( 1 < $arrstrDefaultImplementationChecklist['check_mark_state'] ) {
 					$intCorporateChecklistCount++;
 				}
 			}
 		}

 		$intModuleCount 												= ( true == valArr( $arrstrCorporateChecklists[CModule::COMPANY_SETUP] ) ) ? round( \Psi\Libraries\UtilFunctions\count( $arrstrCorporateChecklists[CModule::COMPANY_SETUP], true ) / 7, 0, PHP_ROUND_HALF_DOWN ) : 0;
 		$arrstrTempCorporateChecklists['percentage'] 					= ( 0 < $intModuleCount ) ? round( ( $intCorporateChecklistCount / $intModuleCount ) * 100 ) : 0;
		$arrstrTempCorporateChecklists['implementation_checklists']		= ( true == valArr( $arrstrCorporateChecklists[CModule::COMPANY_SETUP] ) ) ? $arrstrCorporateChecklists[CModule::COMPANY_SETUP] : [];

 		return $arrstrTempCorporateChecklists;
    }

    public static function fetchChildModulesInDefaultImplementationChecklistsByModuleId( $intModuleId, $objDatabase ) {

    	if( true == empty( $intModuleId ) ) return NULL;

    	$strSql = 'SELECT
    					DISTINCT m.id AS module_id,
    					util_get_translated( \'title\', m.title, m.details ) AS title,
    					m.parent_module_id,
    					m.url,
 						m.name AS module_name
    				FROM
    					default_implementation_checklists dic
    					JOIN modules m ON ( m.id = dic.module_id )
    				WHERE
    					m.parent_module_id = ' . ( int ) $intModuleId;

    	return fetchData( $strSql, $objDatabase );
    }

    public static function fetchModuleInDefaultImplementationChecklistsByModuleId( $intModuleId, $objDatabase ) {

    	if( true == empty( $intModuleId ) ) return NULL;

    	$strSql = 'SELECT
    					DISTINCT m.id AS module_id,
    					util_get_translated( \'title\', m.title, m.details ) AS title,
    					m.parent_module_id,
    					m.url,
 						m.name AS module_name
    				FROM
    					default_implementation_checklists dic
    					JOIN modules m ON ( m.id = dic.module_id )
    				WHERE
    					m.id = ' . ( int ) $intModuleId;

    	return fetchData( $strSql, $objDatabase );
    }

}
?>