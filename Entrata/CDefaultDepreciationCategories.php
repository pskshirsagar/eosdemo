<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CDefaultDepreciationCategories
 * Do not add any new functions to this class.
 */

class CDefaultDepreciationCategories extends CBaseDefaultDepreciationCategories {

	public static function fetchDefaultDepreciationCategories( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CDefaultDepreciationCategory::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDefaultDepreciationCategory( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CDefaultDepreciationCategory::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>