<?php

class CFileSignature extends CBaseFileSignature {
	use Psi\Libraries\Container\TContainerized;
	protected $m_intFileId;
	protected $m_intDocumentSubTypeId;
	protected $m_intLeaseCustomerId;
	protected $m_intDocumentMergeFieldId;
	protected $m_intXPosition;
	protected $m_intYPosition;
	protected $m_intMergeFieldHeight;
	protected $m_intMergeFieldWidth;
	protected $m_intCustomerId;
	protected $m_intApplicationId;
	protected $m_intLeaseId;
	protected $m_intSignatureNumber;

	protected $m_strFilePath;
	protected $m_strDocumentMergeFieldName;
	protected $m_strSignatureFileName;

	protected $m_strHorizontalAlign;
	protected $m_strBlockDocumentMergeFieldId;

    /**
     * Get Functions
     */

	public function getFileId() {
		return $this->m_intFileId;
	}

	public function getDocumentSubTypeId() {
		return $this->m_intDocumentSubTypeId;
	}

	public function getLeaseCustomerId() {
		return $this->m_intLeaseCustomerId;
	}

	public function getDocumentMergeFieldId() {
		return $this->m_intDocumentMergeFieldId;
	}

	public function getXPosition() {
		return $this->m_intXPosition;
	}

	public function getYPosition() {
		return $this->m_intYPosition;
	}

	public function getMergeFieldHeight() {
		return $this->m_intMergeFieldHeight;
	}

	public function getMergeFieldWidth() {
		return $this->m_intMergeFieldWidth;
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getDocumentMergeFieldName() {
		return $this->m_strDocumentMergeFieldName;
	}

	public function getFullFilePath( $strFolderName = PATH_MOUNTS_DOCUMENTS ) {
		return getMountsPath( $this->getCid(), $strFolderName ) . $this->getFilePath();
	}

	public function getSignatureFileName() {
		return $this->m_strSignatureFileName;
	}

	public function getHorizontalAlign() {
		return $this->m_strHorizontalAlign;
	}

	public function getBlockDocumentMergeFieldId() {
		return $this->m_strBlockDocumentMergeFieldId;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getSignatureNumber() {
		return $this->m_intSignatureNumber;
	}

	public function getDetails() {
		return $this->m_arrobjDetails;
	}

    /**
     * Set Functions
     */

	public function setFileId( $intFileId ) {
		$this->m_intFileId = $intFileId;
	}

	public function setDocumentSubTypeId( $intDocumentSubTypeId ) {
		$this->m_intDocumentSubTypeId = $intDocumentSubTypeId;
	}

	public function setLeaseCustomerId( $intLeaseCustomerId ) {
		$this->m_intLeaseCustomerId = $intLeaseCustomerId;
	}

	public function setDocumentMergeFieldId( $intDocumentMergeFieldId ) {
		return $this->m_intDocumentMergeFieldId = $intDocumentMergeFieldId;
	}

	public function setXPosition( $intXPosition ) {
		return $this->m_intXPosition = $intXPosition;
	}

	public function setYPosition( $intYPosition ) {
		return $this->m_intYPosition = $intYPosition;
	}

	public function setMergeFieldHeight( $intMergeFieldHeight ) {
		return $this->m_intMergeFieldHeight = $intMergeFieldHeight;
	}

	public function setMergeFieldWidth( $intMergeFieldWidth ) {
		return $this->m_intMergeFieldWidth = $intMergeFieldWidth;
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setDocumentMergeFieldName( $strDocumentMergeFieldName ) {
		$this->m_strDocumentMergeFieldName = $strDocumentMergeFieldName;
	}

	public function setSignatureFileName( $strSignatureFileName ) {
		$this->m_strSignatureFileName = $strSignatureFileName;
	}

	public function setHorizontalAlign( $strHorizontalAlign ) {
		$this->m_strHorizontalAlign = $strHorizontalAlign;
	}

	public function setBlockDocumentMergeFieldId( $strBlockDocumentMergeFieldId ) {
		$this->m_strBlockDocumentMergeFieldId = $strBlockDocumentMergeFieldId;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setSignatureNumber( $intSignatureNumber ) {
		$this->m_intSignatureNumber = $intSignatureNumber;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['file_id'] ) ) $this->setFileId( $arrmixValues['file_id'] );
        if( true == isset( $arrmixValues['document_sub_type_id'] ) ) $this->setDocumentSubTypeId( $arrmixValues['document_sub_type_id'] );
        if( true == isset( $arrmixValues['lease_customer_id'] ) ) $this->setLeaseCustomerId( $arrmixValues['lease_customer_id'] );
        if( true == isset( $arrmixValues['document_merge_field_id'] ) ) $this->setDocumentMergeFieldId( $arrmixValues['document_merge_field_id'] );
        if( true == isset( $arrmixValues['x_pos'] ) ) $this->setXPosition( $arrmixValues['x_pos'] );
        if( true == isset( $arrmixValues['y_pos'] ) ) $this->setYPosition( $arrmixValues['y_pos'] );
        if( true == isset( $arrmixValues['merge_field_height'] ) ) $this->setMergeFieldHeight( $arrmixValues['merge_field_height'] );
        if( true == isset( $arrmixValues['merge_field_width'] ) ) $this->setMergeFieldWidth( $arrmixValues['merge_field_width'] );

        if( true == isset( $arrmixValues['file_path'] ) ) $this->setFilePath( $arrmixValues['file_path'] );
        if( true == isset( $arrmixValues['document_merge_field_name'] ) ) $this->setDocumentMergeFieldName( $arrmixValues['document_merge_field_name'] );
        if( true == isset( $arrmixValues['signature_file_name'] ) ) $this->setSignatureFileName( $arrmixValues['signature_file_name'] );
		if( true == isset( $arrmixValues['horizontal_align'] ) ) $this->setHorizontalAlign( $arrmixValues['horizontal_align'] );
		if( true == isset( $arrmixValues['block_document_merge_field_id'] ) ) $this->setBlockDocumentMergeFieldId( $arrmixValues['block_document_merge_field_id'] );
		if( true == isset( $arrmixValues['customer_id'] ) ) $this->setCustomerId( $arrmixValues['customer_id'] );
		if( true == isset( $arrmixValues['application_id'] ) ) $this->setApplicationId( $arrmixValues['application_id'] );
		if( true == isset( $arrmixValues['lease_id'] ) ) $this->setLeaseId( $arrmixValues['lease_id'] );
		if( true == isset( $arrmixValues['signature_number'] ) ) $this->setSignatureNumber( $arrmixValues['signature_number'] );

        return;
	}

	public function setDetails( $arrobjDetails ) {
		$this->m_arrobjDetails = $arrobjDetails;
	}

    /**
     * Validate Functions
     */

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required' ) );
        }

        return $boolIsValid;
    }

    public function valApplicantApplicationId() {
        $boolIsValid = true;

        if( true == is_null( $this->getApplicantApplicationId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'applicant_application_id', 'Applicant application is required' ) );
        }

        return $boolIsValid;
    }

    public function valFileAssociationId() {
        $boolIsValid = true;

        if( true == is_null( $this->getFileAssociationId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_association_id', 'File association is required' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valFileAssociationId();
            	break;

            case 'other_esign_document_insert':
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valFileAssociationId();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
               	$boolIsValid &= $this->valFileAssociationId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = true ) {

		if( false == $boolIsSoftDelete ) {
			return parent::delete( $intUserId, $objDatabase );
		}

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( 'NOW()' );
		$this->setAllowDifferentialUpdate( true );

		return $this->update( $intUserId, $objDatabase );
	}

	public function bindDetails( $arrmixDetails ) {
		$arrmixDetails = ( object ) $arrmixDetails;
		$this->setDetails( $arrmixDetails );
	}

}
?>