<?php

class CInspectionFormLocationProblem extends CBaseInspectionFormLocationProblem {

	protected $m_strMaintenanceLocationName;
	protected $m_strMaintenanceProblemName;
	protected $m_strNote;

	protected $m_intMaintenanceLocationId;
	protected $m_intInspectionId;
	protected $m_intMaintenanceProblemId;
	protected $m_intPropertyUnitMaintenanceLocationId;

	protected $m_strInspectionMaintenanceLocationName;

    /**
     * Set Functions
     */

    public function setMaintenanceLocationName( $strMaintenanceLocationName ) {
    	$this->m_strMaintenanceLocationName = $strMaintenanceLocationName;
    }

    public function setMaintenanceProblemName( $strMaintenanceProblemName ) {
    	$this->m_strMaintenanceProblemName = $strMaintenanceProblemName;
    }

    public function setMaintenanceLocationId( $intMaintenanceLocationId ) {
    	$this->m_intMaintenanceLocationId = $intMaintenanceLocationId;
    }

    public function setMaintenanceProblemId( $intMaintenanceProblemId ) {
    	$this->m_intMaintenanceProblemId = $intMaintenanceProblemId;
    }

    public function setInspectionMaintenanceLocationName( $strInspectionMaintenanceLocationName ) {
    	$this->m_strInspectionMaintenanceLocationName = CStrings::strTrimDef( $strInspectionMaintenanceLocationName, 240, NULL, true );
    }

	public function setNote( $strNote ) {
        $this->m_strNote = CStrings::strTrimDef( $strNote, -1, NULL, true );
    }

    public function setInspectionId( $intInspectionId ) {
    	$this->m_intInspectionId = $intInspectionId;
    }

	public function setPropertyUnitMaintenanceLocationId( $intPropertyUnitMaintenanceLocationId ) {
		$this->m_intPropertyUnitMaintenanceLocationId = $intPropertyUnitMaintenanceLocationId;
	}

    /**
     * Get Functions
     */

    public function getMaintenanceLocationName() {
    	return $this->m_strMaintenanceLocationName;
    }

    public function getMaintenanceProblemName() {
    	return $this->m_strMaintenanceProblemName;
    }

    public function getMaintenanceLocationId() {
    	return $this->m_intMaintenanceLocationId;
    }

    public function getMaintenanceProblemId() {
    	return $this->m_intMaintenanceProblemId;
    }

    public function getInspectionMaintenanceLocationName() {
    	return $this->m_strInspectionMaintenanceLocationName;
    }

	public function getNote() {
        return $this->m_strNote;
    }

    public function getInspectionId() {
    	return $this->m_intInspectionId;
    }

	public function getPropertyUnitMaintenanceLocationId() {
		return $this->m_intPropertyUnitMaintenanceLocationId;
	}

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['maintenance_location_name'] ) ) 	$this->setMaintenanceLocationName( $arrValues['maintenance_location_name'] );
    	if( true == isset( $arrValues['maintenance_problem_name'] ) ) 	$this->setMaintenanceProblemName( $arrValues['maintenance_problem_name'] );
    	if( true == isset( $arrValues['maintenance_location_id'] ) ) 	$this->setMaintenanceLocationId( $arrValues['maintenance_location_id'] );
    	if( true == isset( $arrValues['maintenance_problem_id'] ) ) 	$this->setMaintenanceProblemId( $arrValues['maintenance_problem_id'] );
    	if( true == isset( $arrValues['note'] ) ) 						$this->setNote( $arrValues['note'] );
    	if( true == isset( $arrValues['inspection_maintenance_location_name'] ) ) $this->setInspectionMaintenanceLocationName( $arrValues['inspection_maintenance_location_name'] );
    	if( true == isset( $arrValues['inspection_id'] ) ) 	$this->setInspectionId( $arrValues['inspection_id'] );
	    if( true == isset( $arrValues['property_unit_maintenance_location_id'] ) ) 	$this->setPropertyUnitMaintenanceLocationId( $arrValues['property_unit_maintenance_location_id'] );
    	return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionFormId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInspectionFormLocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceProblemId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valMaintenanceLocationId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	break;
        }

        return $boolIsValid;
    }

}
?>