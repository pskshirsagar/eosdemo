<?php

class CWebsitePageType extends CBaseWebsitePageType {

	const HOME							= 1;
	const PROPERTY_SEARCH				= 2;
	const PROPERTY_DETAILS				= 3;
	const FLOORPLANS					= 4;
	const FLOORPLAN						= 5;
	const HIGH_RISE_VIEWER				= 6;
	const CONTACT						= 7;
	const CONTACT_FORM					= 8;
	const PHOTOS_AND_TOURS				= 9;
	const AMENITIES						= 10;
	const DIRECTIONS					= 11;
	const NEIGHBORHOOD					= 12;
	const RATING_AND_REVIEWS			= 13;
	const PROPERTY_SITE_PLAN 			= 14;
	const AVAILABILITY					= 15;
	const BLOG							= 16;
	const BLOG_POST						= 17;
	const CUSTOM_PAGE					= 18;
	const TERMS_AND_CONDITIONS			= 19;
	const EQUAL_HOUSING_OPPORTUNITY		= 20;
	const SMS_TERMS_AND_CONDITIONS		= 21;
	const CORPORATE_CONTACT				= 22;
	const APPLY_NOW						= 23;
	const WEBSITE_MAP					= 24;
	const AVAILABILITY_ALERT_COMPLETE	= 25;
	const REQUEST_INFO_COMPLETE			= 26;
	const TOUR_SCHEDULE_COMPLETE		= 27;
	const GUEST_CARD_COMPLETE			= 28;
	const ERROR_PAGE					= 29;
	const DESIGN_CUSTOM_PAGE			= 30;
	const FAQS                          = 31;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUrlSlug() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSeoUrlSlug() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPrimary() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsMultiProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSingleProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>