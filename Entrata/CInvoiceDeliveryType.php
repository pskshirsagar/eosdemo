<?php

class CInvoiceDeliveryType extends CBaseInvoiceDeliveryType {

	const EMAIL 	= 1;
	const MAIL		= 2;

	public function getInvoiceDeliveryTypeNameByInvoiceDeliveryTypeId( $intInvoiceDeliveryTypeId ) {

    	$strDeliveryTypeName = NULL;

    	switch( $intInvoiceDeliveryTypeId ) {

    		case CInvoiceDeliveryType::EMAIL:
				$strDeliveryTypeName = 'Email';
				break;

    		case CInvoiceDeliveryType::MAIL:
				$strDeliveryTypeName = 'Mail';
				break;

    		default:
    			// default case
    			break;
    	}

    	return $strDeliveryTypeName;
    }
}
?>