<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CCompanyUserPermissions
 * Do not add any new functions to this class.
 */

class CCompanyUserPermissions extends CBaseCompanyUserPermissions {

	public static function fetchCustomCompanyUserPermissionsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase, $boolAllowedOnly = NULL ) {
		$strSql = sprintf( 'SELECT * FROM %s WHERE company_user_id = %d AND cid = %d', 'company_user_permissions', ( int ) $intCompanyUserId, ( int ) $intCid );
		if( $boolAllowedOnly ) {
			$strSql . ' AND allow = ' . ( ( bool ) $boolAllowedOnly ) . ' AND COALESCE( deny,0::int4 ) = ' . ( !$boolAllowedOnly );
		}

		$strSql .= ' ORDER BY id ';

		return rekeyObjects( 'ModuleId', parent::fetchCompanyUserPermissions( $strSql, $objDatabase ) );
	}

	public static function fetchCompanyUserPermissionsByCompanyUserIdByModuleIdsByCid( $intCompanyUserId, $arrintModuleIds, $intCid, $objDatabase ) {
		return self::fetchCompanyUserPermissions( 'SELECT * FROM company_user_permissions WHERE company_user_id = ' . ( int ) $intCompanyUserId . ' AND cid = ' . ( int ) $intCid . ' AND module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )', $objDatabase );
	}

	public static function fetchCompanyUserPermissionByCompanyUserIdByModuleIdByCid( $intCompanyUserId, $intModuleId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_user_permissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND module_id  = ' . ( int ) $intModuleId;

		return self::fetchCompanyUserPermission( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPermissionsIsAllowedByCompanyUserIdByModuleIdByCid( $intCompanyUserId, $intModuleId, $intCid, $objDatabase ) {

		if( false == ( int ) $intCid || false == ( int ) $intCompanyUserId || false == ( int ) $intModuleId ) return NULL;
		$strSql = 'SELECT 
						is_allowed
					FROM
						company_user_permissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND module_id  = ' . ( int ) $intModuleId;

		return parent::fetchColumn( $strSql, 'is_allowed', $objDatabase );
	}

	public static function fetchCompanyUserPermissionsByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_user_permissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND is_allowed = 1';

		return self::fetchCompanyUserPermissions( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPermissionsByManagementIdByModuleIdsByUserIds( $intCid, $arrintModuleIds, $arrintUserIds, $objDatabase ) {
		$strSql = 'SELECT
						*
				   FROM company_user_permissions
				   WHERE cid = ' . ( int ) $intCid . ' AND
						 company_user_id IN ( ' . implode( ',', $arrintUserIds ) . ' )
				   		AND module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )';
		return self::fetchCompanyUserPermissions( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPermissionsCountByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count(*)
					FROM
						company_user_permissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ' . ( int ) $intCompanyUserId . '
						AND is_allowed = 1
						AND module_id < 100000000';

		$arrintCompanyUserPermissionCount = fetchData( $strSql, $objDatabase );

		return $arrintCompanyUserPermissionCount[0]['count'];
	}

	public static function fetchCompanyUserPermissionsByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_user_permissions
					WHERE
						cid = ' . ( int ) $intCid . ' AND
						company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' ) AND
						( is_allowed = 1 OR is_inherited  = 0 ) ';

		return self::fetchCompanyUserPermissions( $strSql, $objDatabase );
	}

	public static function fetchCompanyUserPermissionsByCompanyUserIdsByCidByFieldNames( $arrintCompanyUserIds, $intCid, $strFieldNames, $objDatabase ) {

		$strSql = 'SELECT
						' . implode( ',', $strFieldNames ) . '
					FROM
						company_user_permissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id = ANY( ARRAY[ ' . implode( ',', $arrintCompanyUserIds ) . ' ] )
						AND ( is_allowed = 1 OR is_inherited  = 0 ) ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCompanyUserPermissionInfoByModuleIdsByCidByCompanyUserId( $strModuleIds, $intCid, $intCompanyUserId, $objDatabase ) {

		$strSql = 'SELECT
						cup.is_allowed AS is_allowed,
						cup.module_id
					FROM
						company_user_permissions AS cup
					WHERE
						cup.module_id IN ( ' . $strModuleIds . ' )
						AND cup.cid = ' . ( int ) $intCid . '
						AND cup.company_user_id = ' . ( int ) $intCompanyUserId;

		$arrmixCompanyUserPermissions = fetchData( $strSql, $objDatabase );

		$arrmixCompanyUserModulePermission = [];

		foreach( $arrmixCompanyUserPermissions as $arrmixCompanyUserPermission ) {
			$arrmixCompanyUserModulePermission[$arrmixCompanyUserPermission['module_id']] = $arrmixCompanyUserPermission['is_allowed'];
		}

		return ( array ) $arrmixCompanyUserModulePermission;
	}

	public static function fetchCustomNonPermissionedCompanyUserPermissionsByCompanyGroupIdByModuleIdsByCid( $intCompanyGroupId, $arrintModuleIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( cup.company_user_id ) cup.company_user_id
					FROM
        				company_user_permissions cup
        				JOIN company_user_groups cug ON ( cug.company_user_id = cup.company_user_id AND cug.cid = cup.cid )
					WHERE
						cup.cid = ' . ( int ) $intCid . '
						AND cup.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND cup.is_allowed = 0
						AND cup.is_inherited = 0
						AND cug.company_group_id = ' . ( int ) $intCompanyGroupId . '
					GROUP BY
        				cup.company_user_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomPermissionedModulesByCompanyUserIdByModuleIdsByCid( $intCompanyUserId, $arrintModuleIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    cup.module_id,
					    m.name
					FROM
					    company_user_permissions cup
					    JOIN modules m ON ( m.id = cup.module_id )
					WHERE
						cup.cid = ' . ( int ) $intCid . '
					    AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
					    AND m.id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
					    AND cup.is_inherited = 0
					    AND cup.is_allowed = 1
					UNION
					SELECT
					    cgp.module_id,
					    m.name
					FROM
					    company_user_groups cug
					    JOIN company_group_permissions cgp ON ( cgp.company_group_id = cug.company_group_id AND cgp.cid = cug.cid )
					    JOIN company_user_permissions cup ON ( cup.company_user_id = cug.company_user_id AND cup.module_id = cgp.module_id AND cup.cid = cug.cid )
					    JOIN modules m ON ( m.id = cgp.module_id )
					WHERE
					    cup.cid = ' . ( int ) $intCid . '
					    AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
					    AND cup.is_inherited = 1
					    AND cgp.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
					    AND cgp.is_allowed = 1
					GROUP BY
					    cgp.module_id,
					    m.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomCompanyUserPermissionsByModuleIdsByCompanyUserIdByCid( $arrintModuleIds, $intCompanyUserId, $intCid, $objDatabase ) {

		if( false == valArr( $arrintModuleIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cup.module_id,
						m.name,
						cup.is_allowed AS is_allowed
					FROM
						company_user_permissions cup
						JOIN modules m ON ( m.id = cup.module_id )
					WHERE
						cup.cid = ' . ( int ) $intCid . '
						AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND m.id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND cup.is_inherited = 0
						AND cup.is_allowed = 1
					UNION
					SELECT
						cgp.module_id,
						m.name,
						cgp.is_allowed AS is_allowed
					FROM
						company_user_groups cug
						JOIN company_group_permissions cgp ON ( cgp.company_group_id = cug.company_group_id AND cgp.cid = cug.cid )
						JOIN company_user_permissions cup ON ( cup.company_user_id = cug.company_user_id AND cup.module_id = cgp.module_id AND cup.cid = cug.cid )
						JOIN modules m ON ( m.id = cgp.module_id )
					WHERE
						cup.cid = ' . ( int ) $intCid . '
						AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cup.is_inherited = 1
						AND cgp.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND cgp.is_allowed = 1
					GROUP BY
						cgp.module_id,
						m.name,
						cgp.is_allowed';

		$arrmixCompanyUserPermissions = ( array ) fetchData( $strSql, $objDatabase );

		$arrmixCompanyUserModulePermission = [];
		foreach( $arrmixCompanyUserPermissions as $arrmixCompanyUserPermission ) {
			$arrmixCompanyUserModulePermission[$arrmixCompanyUserPermission['module_id']] = $arrmixCompanyUserPermission['is_allowed'];
		}

		return ( array ) $arrmixCompanyUserModulePermission;
	}

	public static function fetchCustomNonPermissionedModulesByCompanyUserIdsByModuleIdsByCid( $arrintCompanyUserIds, $arrintModuleIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
					    cup.module_id,
					    cup.company_user_id
					FROM
					    company_user_permissions cup
					    JOIN modules m ON ( m.id = cup.module_id )
					WHERE
						cup.cid = ' . ( int ) $intCid . '
					    AND cup.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
					    AND m.id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
					    AND cup.is_inherited = 0
					    AND cup.is_allowed = 0
					UNION
					SELECT
					    cgp.module_id,
					    cup.company_user_id
					FROM
					    company_user_groups cug
					    JOIN company_group_permissions cgp ON ( cgp.company_group_id = cug.company_group_id AND cgp.cid = cug.cid )
					    JOIN company_user_permissions cup ON ( cup.company_user_id = cug.company_user_id AND cup.module_id = cgp.module_id AND cup.cid = cug.cid )
					    JOIN modules m ON ( m.id = cgp.module_id )
					WHERE
					    cup.cid = ' . ( int ) $intCid . '
					    AND cup.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
					    AND cup.is_inherited = 1
					    AND cgp.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
					    AND cgp.is_allowed = 0
					GROUP BY
					    cgp.module_id,
					    cup.company_user_id';
		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCustomPermissionedModulesByCompanyUserIdsByModuleIdsByCid( $arrintCompanyUserIds, $arrintModuleIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCompanyUserIds ) || false == valArr( $arrintModuleIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cup.module_id,
						cup.company_user_id
					FROM
						company_user_permissions cup
						JOIN modules m ON ( m.id = cup.module_id )
					WHERE
						cup.cid = ' . ( int ) $intCid . '
						AND cup.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
						AND m.id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND cup.is_inherited = 0
						AND cup.is_allowed = 1
					UNION
					SELECT
						cgp.module_id,
						cug.company_user_id
					FROM
						company_user_groups cug
						JOIN company_group_permissions cgp ON ( cgp.company_group_id = cug.company_group_id AND cgp.cid = cug.cid )
						JOIN modules m ON ( m.id = cgp.module_id )
					WHERE
						cug.cid = ' . ( int ) $intCid . '
						AND cug.company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )
						AND cgp.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND cgp.is_allowed = 1
					GROUP BY
						cgp.module_id,
						cug.company_user_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomNonPermissionedModulesByCompanyUserIdByParentModuleIdsByCid( $intCompanyUserId, $arrintModuleIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						company_user_permissions cup
					WHERE
						cup.cid = ' . ( int ) $intCid . '
						AND cup.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND cup.module_id IN ( ' . implode( ',', $arrintModuleIds ) . ' )
						AND cup.is_allowed = 0';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPermissionedModulesByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT
						cup.company_user_id,
						string_agg( cup.module_id::text || \'::\' || CASE WHEN is_read_only = 1 THEN \'READ\' ELSE \'Write\' END || \'~^~\', \'\' ) AS old_logs,
						string_agg( cup.module_id::text || \'::Inherit~^~\', \'\' ) AS new_logs
					FROM
						company_user_permissions cup
					WHERE
						cup.cid = ' . ( int ) $intCid . '
						AND cup.is_allowed = 1
					GROUP BY 
						cup.company_user_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteCustomUserLevelPermissionsByCid( $intCid, $objDatabase ) {

		$strSql = 'DELETE
					FROM 
						company_user_permissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_allowed = 1';

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return false;
		} else {
			$objDataset->cleanup();
			return true;
		}
	}

	public static function deleteAllCompanyUserPermissionsByCompanyUserIdsByCid( $arrintCompanyUserIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintCompanyUserIds ) ) {
			return false;
		}

		$strSql = 'DELETE FROM company_user_permissions
					WHERE
						cid = ' . ( int ) $intCid . '
						AND company_user_id IN ( ' . implode( ',', $arrintCompanyUserIds ) . ' )';

		return executeSql( $strSql, $objDatabase );
	}

}
?>
