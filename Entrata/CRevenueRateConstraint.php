<?php

class CRevenueRateConstraint extends CBaseRevenueRateConstraint {

	protected $m_strRevenueRateConstraintTypeName;
	protected $m_intRevenueRateConstraintCalcType;

	public function setRevenueRateConstraintTypeName( $strRevenueRateConstraintTypeName ) {
        $this->m_strRevenueRateConstraintTypeName = $strRevenueRateConstraintTypeName;
    }

    public function getRevenueRateConstraintTypeName() {
        return $this->m_strRevenueRateConstraintTypeName;
    }

    public function sqlRevenueRateConstraintTypeName() {
        return ( true == isset( $this->m_strRevenueRateConstraintTypeName ) ) ? ( string ) $this->m_strRevenueRateConstraintTypeName : '';
    }

 	public function setRevenueRateConstraintCalcType( $intRevenueRateConstraintCalcType ) {
        $this->m_intRevenueRateConstraintCalcType = CStrings::strToIntDef( $intRevenueRateConstraintCalcType, NULL, false );
    }

    public function getRevenueRateConstraintCalcType() {
        return $this->m_intRevenueRateConstraintCalcType;
    }

    public function sqlRevenueRateConstraintCalcType() {
        return ( true == isset( $this->m_intRevenueRateConstraintCalcType ) ) ? ( string ) $this->m_intRevenueRateConstraintCalcType : '0';
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRevenueRateConstraintTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRuleChangeAmount() {
        $boolIsValid = true;
        if( true == is_nan( $this->getRuleChangeAmount() ) ) {
			$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rule_change_amount', 'Rule Change Amount should be a valid number' ) );
        }

        if( 1 == $this->getIsFixed() ) {
        	if( ( true == is_null( $this->getRuleChangeAmount() ) ) || ( -999 > $this->getRuleChangeAmount() ) || ( 999 < $this->getRuleChangeAmount() ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rule_change_amount', 'Rule Change Amount should be in between -999 to 999' ) );
        	}
        } else {
        	if( ( true == is_null( $this->getRuleChangeAmount() ) ) || ( -100 > $this->getRuleChangeAmount() ) || ( 100 < $this->getRuleChangeAmount() ) ) {
        		$boolIsValid = false;
        		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rule_change_amount', 'Rule Change Amount should be in between -100 to 100' ) );
        	}
        }

        return $boolIsValid;
    }

    public function valIsFixed() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valRuleChangeAmount();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valRuleChangeAmount();
            	break;

			case VALIDATE_DELETE:
				break;

           	default:
				$boolIsValid &= false;
            	break;
        }

        return $boolIsValid;
    }

}
?>