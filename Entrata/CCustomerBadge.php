<?php

class CCustomerBadge extends CBaseCustomerBadge {

	protected $m_intLeaseId;

	protected $m_strFileHandle;
	protected $m_strName;

    /**
     * Get Functions
     */

    public function getLeaseId() {
    	return $this->m_intLeaseId;
    }

    public function getFileHandle() {
    	return $this->m_strFileHandle;
    }

    public function getName() {
    	return $this->m_strName;
    }

    /**
     * Set Functions
     */

    public function setLeaseId( $intLeaseId ) {
    	$this->m_intLeaseId = $intLeaseId;
    }

    public function setFileHandle( $strFileHandle ) {
    	$this->m_strFileHandle = $strFileHandle;
    }

    public function setName( $strName ) {
    	$this->m_strName = $strName;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

    	parent::setValues( $arrValues );

    	if( isset( $arrValues['lease_id'] ) && $boolDirectSet )
    		$this->m_intLeaseId = trim( $arrValues['lease_id'] );
    	elseif ( isset( $arrValues['lease_id'] ) )
    		$this->setLeaseId( $arrValues['lease_id'] );
    	if( isset( $arrValues['file_handle'] ) && $boolDirectSet )
    		$this->m_strFileHandle = trim( $arrValues['file_handle'] );
    	elseif ( isset( $arrValues['file_handle'] ) )
    		$this->setFileHandle( $arrValues['file_handle'] );
    	if( isset( $arrValues['name'] ) && $boolDirectSet )
    		$this->m_strName = trim( $arrValues['name'] );
    	elseif ( isset( $arrValues['name'] ) )
    		$this->setName( $arrValues['name'] );
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCustomerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCompanyBadgeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEarnedDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsProAward() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valCustomerId();
            	$boolIsValid &= $this->valCompanyBadgeId();
				break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>