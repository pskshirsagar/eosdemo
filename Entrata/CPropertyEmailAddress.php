<?php

class CPropertyEmailAddress extends CBasePropertyEmailAddress {

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    }

    /**
     * Validation Functions
     */

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) || ( 1 > $this->m_intId ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Email address id does not appear valid.' ) ) );
        }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( false == isset( $this->m_intCid ) || ( 1 > $this->m_intCid ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Email address client does not appear valid.' ) ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intPropertyId ) || ( 1 > $this->m_intPropertyId ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Email address property id does not appear valid.' ) ) );
        }

        return $boolIsValid;
    }

    public function valEmailAddressTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intEmailAddressTypeId ) || ( 1 > $this->m_intEmailAddressTypeId ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address_type_id', __( 'Email address type does not appear valid.' ) ) );
        }

        return $boolIsValid;
    }

    public function valMarketingName() {
        // Validation example

        // if( false == isset( $this->m_strMarketingName )) {
        //    $boolIsValid = false;
        //    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'marketing_name', '' ));
        // }

        return true;
    }

    public function valEmailAddress() {
        $boolIsValid = true;

        if( false == isset( $this->m_strEmailAddress ) || ( false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'The email address does not appear to be valid.' ) ) );
        }

        return $boolIsValid;
    }

    public function valBulkEmailAddress() {
        $boolIsValid = true;
		if( false == is_null( $this->m_strEmailAddress ) ) {
			if( false == isset( $this->m_strEmailAddress ) || ( false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) ) {
	            $boolIsValid = false;
	            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'The email address does not appear to be valid.' ) );
	        }
		}
        return $boolIsValid;
    }

    public function validate( $strAction ) {
		require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valEmailAddressTypeId();
				$boolIsValid &= $this->valEmailAddress();
				break;

            case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valEmailAddressTypeId();
				$boolIsValid &= $this->valEmailAddress();
                break;

            case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
                break;

            case 'property_add_wizard_insert':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valEmailAddressTypeId();
                break;

            case 'property_add_wizard_update':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valEmailAddressTypeId();
                break;

            case 'validate_insert_email':
				$boolIsValid &= $this->valEmailAddress();
				break;

            case 'validate_bulk_insert_email':
            	$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valEmailAddressTypeId();
				$boolIsValid &= $this->valBulkEmailAddress();
                break;

            default:
               	$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

}
?>