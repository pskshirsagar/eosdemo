<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CAnnouncementTypes
 * Do not add any new functions to this class.
 */

class CAnnouncementTypes extends CBaseAnnouncementTypes {

	public static function fetchAnnouncementTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CAnnouncementType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAnnouncementType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CAnnouncementType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>