<?php

class CArCodeTypeAllowances extends CBaseArCodeTypeAllowances {

    public static function fetchArCodeTypeAllowances( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CArCodeTypeAllowance', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

    public static function fetchArCodeTypeAllowance( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CArCodeTypeAllowance', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
    }

}
?>