<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGlTrees
 * Do not add any new functions to this class.
 */

class CGlTrees extends CBaseGlTrees {

	public static function fetchGlTreesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						is_system DESC,
						updated_on DESC';

		return self::fetchGlTrees( $strSql, $objClientDatabase );
	}

	public static function fetchSystemGlTreeByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_system = 1';

		return self::fetchGlTree( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlTreesByIdsByCid( $arrintGlTreeIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintGlTreeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						gl_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND	id IN ( ' . implode( ',', $arrintGlTreeIds ) . ' )
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						is_system DESC,
						updated_on DESC';

		return self::fetchGlTrees( $strSql, $objClientDatabase );
	}

	public static function fetchActiveGlTreeByIdByCid( $intId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						gl_trees
					WHERE
						id = ' . ( int ) $intId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return self::fetchGlTree( $strSql, $objDatabase );
	}

	public static function fetchActiveGlTreesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gt.id,
						gt.name,
						gt.is_system,
						gt.updated_on,
						gt.details,
						COUNT ( p.id ) AS property_count,
						array_to_string( array_agg( p.property_name ), \', \' ) AS associated_properties
					FROM
						gl_trees gt
						LEFT JOIN property_gl_settings pgs ON ( gt.cid = pgs.cid AND gt.id = pgs.gl_tree_id )
						LEFT JOIN properties p ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND p.is_disabled = 0 AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) )
					WHERE
						gt.cid = ' . ( int ) $intCid . '
						AND gt.deleted_by IS NULL
						AND gt.deleted_on IS NULL
					GROUP BY
						gt.id,
						gt.name,
						gt.is_system,
						gt.updated_on,
						gt.details
					ORDER BY
						gt.is_system DESC,
						gt.updated_on DESC';

		return self::fetchGlTrees( $strSql, $objClientDatabase, false );
	}

	public static function fetchActiveChildGlTreesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						gt.id,
						gt.name,
						gt.is_system,
						gt.updated_on,
						COUNT ( p.id ) AS property_count,
						array_to_string( array_agg( p.property_name ), \', \' ) AS associated_properties
					FROM
						gl_trees gt
						LEFT JOIN property_gl_settings pgs ON ( gt.cid = pgs.cid AND gt.id = pgs.gl_tree_id )
						LEFT JOIN properties p ON ( pgs.cid = p.cid AND pgs.property_id = p.id AND p.is_disabled = 0 AND p.property_type_id NOT IN( ' . CPropertyType::SETTINGS_TEMPLATE . ', ' . CPropertyType::TEMPLATE . ' ) )
					WHERE
						gt.cid = ' . ( int ) $intCid . '
						AND gt.deleted_by IS NULL
						AND gt.deleted_on IS NULL
						AND gt.is_system = 0
					GROUP BY
						gt.id,
						gt.name,
						gt.is_system,
						gt.updated_on
					ORDER BY
						gt.is_system DESC,
						gt.updated_on DESC';

		return self::fetchGlTrees( $strSql, $objClientDatabase );
	}

	public static function fetchRecursiveGlTreeWithGlGroupsByGlTreeIdByCid( $intGlTreeId, $intCid, $objClientDatabase, $intCurrentTabGlGroupId = NULL, $boolIsAllowUnmappedAccounts = false ) {

		if( false == valId( $intGlTreeId ) ) {
			return NULL;
		}

		$strJoin		= 'LEFT JOIN gl_account_trees gat ON ( cb.cid = gat.cid AND cb.id = gat.gl_group_id AND gat.gl_tree_id = cb.gl_tree_id)';
		$strCondition	= 'AND gat.hide_account_balance_from_reports <> 1';

		if( true == valId( $intCurrentTabGlGroupId ) ) {
			$strJoin = 'LEFT JOIN LATERAL 
							(
								SELECT
									id,
									CID,
									gl_group_id,
									gl_tree_id
								FROM
									gl_account_trees gat
								WHERE
									cb.cid = gat.cid
									AND cb.id = gat.gl_group_id
									AND gat.gl_tree_id = cb.gl_tree_id

								UNION

								SELECT
									gamg.gl_account_tree_id,
									gamg.cid,
									gamg.gl_group_id,
									gat.gl_tree_id
								FROM
									gl_account_mask_groups gamg
									JOIN gl_account_trees gat ON( gamg.cid = gat.cid and gat.id = gamg.gl_account_tree_id )
								WHERE
									cb.cid = gamg.cid
									AND cb.id = gamg.gl_group_id
									AND gat.gl_tree_id = cb.gl_tree_id
							) net_cash_flow ON ( TRUE )
						LEFT JOIN gl_account_trees gat ON ( net_cash_flow.cid = gat.cid AND net_cash_flow.id = gat.id )';

			$strCondition = 'AND ( cb.main_group_id IN ( ' . ( int ) $intCurrentTabGlGroupId . ' ) OR cb.id IN( ' . ( int ) $intCurrentTabGlGroupId . ') )';

		}

		if( !$boolIsAllowUnmappedAccounts ) {
			$strCondition .= ' AND ( gat.is_default = 1 OR gat.gl_account_tree_id IS NULL )';
		}

		$strSql = 'DROP TABLE
					IF EXISTS temp_gl_account_trees;
					CREATE TEMP TABLE temp_gl_account_trees AS(
						SELECT
							DISTINCT gat.id
						FROM
							gl_account_trees gat
							JOIN gl_account_trees gat2 ON ( gat2.cid = gat.cid AND gat2.gl_account_tree_id = gat.id AND gat2.disabled_by IS NULL )
						WHERE
							gat.cid = ' . ( int ) $intCid . '
							AND gat.gl_tree_id = ' . ( int ) $intGlTreeId . '
							AND gat.disabled_by IS NOT NULL);';

		$strSql .= 'WITH RECURSIVE cte_groups ( id, system_code, level, main_group_id, top_parent_id, parent_id, name, default_name, sort, gl_account_type_id, cid ) AS (
					SELECT
						glg.id,
						glg.system_code,
						0,
						NULL::INTEGER,
						NULL::INTEGER,
						NULL::INTEGER,
						util_get_translated( \'name\', glg.name, glg.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS name,
						util_get_translated( \'name\', glg.name, glg.details, \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' ) AS default_name,
						glg.order_num::TEXT,
						glg.gl_account_type_id,
						glg.cid,
						glg.gl_tree_id,
						glg.gl_group_type_id,
						glg.disabled_by,
						glg.order_num,
						glg.is_system,
						glg.default_gl_group_id,
						case WHEN glg.gl_group_type_id = 2 THEN
							1
						ELSE 
							0
						END AS is_total_group
					FROM
						gl_groups glg
					WHERE
						glg.cid = ' . ( int ) $intCid . '
						AND glg.gl_tree_id = ' . ( int ) $intGlTreeId . '
						AND glg.system_code IN ( \'INCST\', \'BALSH\' ) 
					UNION
					
					SELECT
						glg.id,
						glg.system_code,
						LEVEL + 1,
						CASE
							WHEN LEVEL < 1 THEN glg.parent_gl_group_id::INTEGER
							ELSE ( main_group_id ) 
						END::INTEGER AS main_group_id,
						CASE
							WHEN LEVEL < 1 AND cte_groups.system_code = \'INCST\' THEN 1
							WHEN LEVEL < 1 AND cte_groups.system_code = \'BALSH\' THEN 2
							ELSE top_parent_id::INTEGER
						END AS top_parent_id,     
						glg.parent_gl_group_id,
						util_get_translated( \'name\', glg.name, glg.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS name,
						util_get_translated( \'name\', glg.name, glg.details, \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' ) AS default_name,
						CASE
							WHEN LEVEL < 1 THEN to_char ( glg.order_num, \'FM00000\' )
							ELSE ( sort::TEXT || \'|\' || to_char ( glg.order_num, \'FM00000\' ) ) 
						END::TEXT AS order_num,
						glg.gl_account_type_id,
						glg.cid,
						glg.gl_tree_id,
						glg.gl_group_type_id,
						glg.disabled_by,
						glg.order_num,
						glg.is_system,
						glg.default_gl_group_id,
						case WHEN glg.gl_group_type_id = 2 THEN
							1
						ELSE 
							0
						END AS is_total_group
					FROM
						gl_groups glg
						JOIN cte_groups ON glg.cid = ' . ( int ) $intCid . ' AND glg.parent_gl_group_id = cte_groups.id
					WHERE
						glg.cid = ' . ( int ) $intCid . '
						AND glg.gl_tree_id = ' . ( int ) $intGlTreeId . '
					)
					SELECT
						cb.id,
						cb.main_group_id,
						cb.parent_id,
						cb.is_total_group,
						cb.name,
						cb.default_name,
						cb.name AS group_location,
						cb.gl_group_type_id,
						cb.gl_account_type_id,
						cb.is_system,
						cb.disabled_by,
						gat1.account_number AS master_account_number,
						gat.id as gl_account_tree_id,
						gat.account_number,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS account_name,
						util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' ) AS default_account_name,
						gat.secondary_number,
						util_get_translated( \'description\', gat.description, gat.details, \'' . CLocaleContainer::createService()->getTargetLocaleCode() . '\' ) AS description,
						util_get_system_translated( \'name\', glat.name, glat.details ) AS gl_account_type,
						gat.order_num,
						CASE
                          WHEN gat.disabled_by IS NOT NULL AND ( SELECT true FROM temp_gl_account_trees WHERE id = gat.id ) THEN NULL
                          ELSE gat.disabled_by
                        END AS gl_account_tree_disabled_by,
						gat.hide_account_balance_from_reports,
						gat.is_system gl_account_tree_is_system,
						util_get_system_translated( \'name\', dgg.name, dgg.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS system_name,
						ga.system_code,
						ga.default_gl_account_id,
						count(*) OVER ( PARTITION BY cb.id ) AS total_nodes,
						ROW_NUMBER() OVER ( PARTITION BY cb.id order by gat.order_num, gat.account_number NULLS FIRST, CASE WHEN gat.gl_account_tree_id IS NULL THEN 0 ELSE 1 END, gat1.account_number, util_get_translated( \'name\', gat.name, gat.details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) DESC ) AS row_number_of_nodes
					FROM
						cte_groups cb
						LEFT JOIN default_gl_groups dgg ON ( dgg.id = cb.default_gl_group_id )
						' . $strJoin . '
						LEFT JOIN gl_accounts ga ON ( ga.cid = gat.cid AND ga.id = gat.gl_account_id )
						LEFT JOIN gl_account_types glat ON ( glat.id = cb.gl_account_type_id )
						LEFT JOIN gl_account_trees gat1 ON ( gat.cid = gat1.cid AND gat.gl_account_id = gat1.gl_account_id AND gat1.is_default = 1 )
					WHERE
						cb.cid = ' . ( int ) $intCid . '
						AND cb.gl_tree_id = ' . ( int ) $intGlTreeId . '
						' . $strCondition . '
					ORDER BY
						top_parent_id,
						main_group_id,
						sort,
						cb.is_total_group DESC,
						cb.gl_group_type_id,
						row_number_of_nodes';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSystemGlTreesByCid( $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						id,name
					FROM
						gl_trees
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_system = 1';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>