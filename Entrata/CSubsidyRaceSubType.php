<?php

class CSubsidyRaceSubType extends CBaseSubsidyRaceSubType {

	const ASIAN_INDIA 				= 1;
	const JAPANESE 					= 2;
	const CHINESE 					= 3;
	const KOREN 					= 4;
	const FILIPINO 					= 5;
	const VIETNAMESE 				= 6;
	const OTHER_ASIAN 				= 7;
	const NATIVE_HAWAIIAN 			= 8;
	const SAMOAN 					= 9;
	const GUAMANIAN_CHAMORRO 		= 10;
	const OTHER_PACIFIC_ISLANDER    = 11;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubsidyRaceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public static function getSubsidizeRaceSubTypeIdByName( $strSubsidyRaceSubType, $strSubsidyRaceType ) {
		switch( \Psi\CStringService::singleton()->strtolower( $strSubsidyRaceType ) ) {

			case 'asian':
				switch( \Psi\CStringService::singleton()->strtolower( $strSubsidyRaceSubType ) ) {

					case 'asian india':
						$intSubsidyRaceSubTypeId = self::ASIAN_INDIA;
						break;

					case 'japanese':
						$intSubsidyRaceSubTypeId = self::JAPANESE;
						break;

					case 'chinese':
						$intSubsidyRaceSubTypeId = self::CHINESE;
						break;

					case 'korean':
						$intSubsidyRaceSubTypeId = self::KOREN;
						break;

					case 'filipino':
						$intSubsidyRaceSubTypeId = self::FILIPINO;
						break;

					case 'vietnamese':
						$intSubsidyRaceSubTypeId = self::VIETNAMESE;
						break;

					case 'other asian':
						$intSubsidyRaceSubTypeId = self::OTHER_ASIAN;
						break;

					default:
						$intSubsidyRaceSubTypeId = NULL;
						break;
				}
				break;

			case 'native hawaiian or other pacific islander':
				switch( \Psi\CStringService::singleton()->strtolower( $strSubsidyRaceSubType ) ) {

					case 'native hawaiian':
						$intSubsidyRaceSubTypeId = self::NATIVE_HAWAIIAN;
						break;

					case 'samoan':
						$intSubsidyRaceSubTypeId = self::SAMOAN;
						break;

					case 'guamanian chamorro':
						$intSubsidyRaceSubTypeId = self::GUAMANIAN_CHAMORRO;
						break;

					case 'other pacific islander':
						$intSubsidyRaceSubTypeId = self::OTHER_PACIFIC_ISLANDER;
						break;

					default:
						$intSubsidyRaceSubTypeId = NULL;
						break;
				}
				break;

			default:
				$intSubsidyRaceSubTypeId = NULL;
				break;
		}

		return $intSubsidyRaceSubTypeId;
	}

}
?>