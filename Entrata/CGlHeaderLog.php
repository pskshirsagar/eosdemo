<?php

class CGlHeaderLog extends CBaseGlHeaderLog {

	const ACTION_CREATED				= 'Created';
	const ACTION_EDITED					= 'Edited';
	const ACTION_POSTED					= 'Posted';
	const ACTION_DELETED				= 'Deleted';
	const ACTION_REVERSED				= 'Reversed';
	const ACTION_REVERSAL				= 'Reversal';
	const ACTION_APPROVED				= 'Approved';
	const ACTION_REJECTED				= 'Rejected';
	const ACTION_RETURNED_TO_PREVIOUS	= 'Returned To Previous';
	const ACTION_RETURNED_TO_BEGINNING	= 'Returned To Beginning';
	const ACTION_ATTACHMENT_ADDED		= 'Attachment added';
	const ACTION_ATTACHMENT_REMOVED		= 'Attachment removed';
	const ACTION_FINAL_APPROVAL			= 'Final Approval';
	const ACTION_UNAPPROVED				= 'Unapproved';
	const ACTION_RE_SUBMITTED			= 'Re-Submitted';

	protected $m_strBulkPropertyName;
	protected $m_strAccountingMethod;
	protected $m_strCompanyEmployeeFullName;

	/**
	 * Get Functions
	 */

	public function getAccountingMethod() {
		return $this->m_strAccountingMethod;
	}

	public function getFormattedPostMonth() {

		if( true == is_null( $this->m_strPostMonth ) ) {
			return NULL;
		}

		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strPostMonth );
	}

	public function getCompanyEmployeeFullName() {
		return $this->m_strCompanyEmployeeFullName;
	}

	public function getBulkPropertyName() {
		return $this->m_strBulkPropertyName;
	}

	public function getFormattedReversePostMonth() {

		if( true == is_null( $this->m_strReversePostMonth ) ) {
			return NULL;
		}

		return preg_replace( '/(\d+)\/(\d+)\/(\d+)/', '$1/$3', $this->m_strReversePostMonth );
	}

	/**
	 * Set Functions
	 */

	public function setAccountingMethod( $strAccountingMethod ) {
		$this->m_strAccountingMethod = $strAccountingMethod;
	}

	public function setCompanyEmployeeFullName( $strCompanyEmployeeFullName ) {
		$this->m_strCompanyEmployeeFullName = CStrings::strTrimDef( $strCompanyEmployeeFullName, 100, NULL, true );
	}

	public function setBulkPropertyName( $strBulkPropertyName ) {
		$this->m_strBulkPropertyName = $strBulkPropertyName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_employee_full_name'] ) )	$this->setCompanyEmployeeFullName( $arrmixValues['company_employee_full_name'] );
		if( true == isset( $arrmixValues['bulk_property_name'] ) )			$this->setBulkPropertyName( $arrmixValues['bulk_property_name'] );

		return;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPeriodId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlHeaderTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlTransactionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlHeaderStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOffsettingGlHeaderId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlHeaderScheduleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGlBookId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHeaderNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMemo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function loadGlHeaderLogActions() {
		$arrstrGlHeaderLogActions = [];

		$arrstrGlHeaderLogActions[self::ACTION_CREATED]					= self::ACTION_CREATED;
		$arrstrGlHeaderLogActions[self::ACTION_EDITED]					= self::ACTION_EDITED;
		$arrstrGlHeaderLogActions[self::ACTION_POSTED]					= self::ACTION_POSTED;
		$arrstrGlHeaderLogActions[self::ACTION_DELETED]					= self::ACTION_DELETED;
		$arrstrGlHeaderLogActions[self::ACTION_REVERSED]				= self::ACTION_REVERSED;
		$arrstrGlHeaderLogActions[self::ACTION_REVERSAL]				= self::ACTION_REVERSAL;
		$arrstrGlHeaderLogActions[self::ACTION_APPROVED]				= self::ACTION_APPROVED;
		$arrstrGlHeaderLogActions[self::ACTION_RETURNED_TO_PREVIOUS]	= self::ACTION_RETURNED_TO_PREVIOUS;
		$arrstrGlHeaderLogActions[self::ACTION_RETURNED_TO_BEGINNING]	= self::ACTION_RETURNED_TO_BEGINNING;
		$arrstrGlHeaderLogActions[self::ACTION_ATTACHMENT_ADDED]		= self::ACTION_ATTACHMENT_ADDED;
		$arrstrGlHeaderLogActions[self::ACTION_ATTACHMENT_REMOVED]		= self::ACTION_ATTACHMENT_REMOVED;
		$arrstrGlHeaderLogActions[self::ACTION_REJECTED]				= self::ACTION_REJECTED;
		$arrstrGlHeaderLogActions[self::ACTION_RE_SUBMITTED]			= self::ACTION_RE_SUBMITTED;

		return $arrstrGlHeaderLogActions;
	}

}
?>