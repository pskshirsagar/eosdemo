<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CRouteRules
 * Do not add any new functions to this class.
 */

class CRouteRules extends CBaseRouteRules {

	public static function fetchRouteRuleByRouteIdByIdByCid( $intRouteId, $intId, $intCid, $objClientDatabase ) {
		return self::fetchRouteRule( 'SELECT * FROM route_rules WHERE route_id = ' . ( int ) $intRouteId . ' AND id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid, $objClientDatabase );
	}

	public static function fetchActiveRouteRulesByRouteIdByCid( $intRouteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT * FROM
						route_rules
					WHERE
						cid = ' . ( int ) $intCid . '
						AND route_id = ' . ( int ) $intRouteId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_default_rule  = 0
					ORDER BY
						order_num';

		return self::fetchRouteRules( $strSql, $objClientDatabase );
	}

	public static function fetchAllRouteRulesByRouteIdByCid( $intRouteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT * FROM
						route_rules
					WHERE
						cid = ' . ( int ) $intCid . '
						AND route_id = ' . ( int ) $intRouteId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						order_num';

		return self::fetchRouteRules( $strSql, $objClientDatabase );
	}

	public static function fetchRouteRulesWithRuleConditionsAndRuleConditionDetailsByRouteIdByCid( $intRouteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( rr.is_default_rule, rr.order_num, rr.id, rc.id )
						rr.id AS route_rule_id,
						rr.order_num,
						rr.is_auto_approve,
						rr.use_all_conditions,
						rr.route_type_id,
						rr.is_default_rule,
						rr.is_auto_approve,
						rc.route_rule_type_id,
						rc.minimum_amount,
						rc.maximum_amount,
						sub_gat.gl_account_names,
						sub_ac.job_cost_codes,
						sub_ap.company_names,
						sub_ba.account_names,
						sub_art.routing_tag_names,
						sub_arc.ar_code_names,
						sub_jc.job_category_name,
						sub_ahst.invoice_po_type_names,
						sub_cu.company_user_names,
						rc.condition_details::TEXT AS reference_numbers
					FROM
						route_rules rr
						LEFT JOIN rule_conditions rc ON ( rc.cid = rr.cid AND rc.route_id = rr.route_id AND rc.route_rule_id = rr.id )
						
						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING ( ARRAY_AGG( gat.name ORDER BY gat.name ), \', \' ) AS gl_account_names
							FROM
								gl_account_trees gat
							WHERE
								gat.cid = ' . ( int ) $intCid . '
								AND gat.is_default = 1 
								AND rc.route_rule_type_id = ' . CRouteRuleType::SPECIFIC_GL_ACCOUNTS . ' 
								AND gat.gl_account_id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_gat ON TRUE
						
						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING ( ARRAY_AGG( ac.name ORDER BY ac.name ), \', \' ) AS job_cost_codes
							FROM
								ap_codes ac
							WHERE
								ac.cid = ' . ( int ) $intCid . '
								AND ac.deleted_on IS NULL 
								AND rc.route_rule_type_id = ' . CRouteRuleType::SPECIFIC_JOB_COST_CODE . '
								AND ac.id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_ac ON TRUE
    
						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING ( ARRAY_AGG( ap.company_name ORDER BY ap.company_name ), \', \' ) AS company_names
							FROM
								ap_payees ap
							WHERE
								ap.cid = ' . ( int ) $intCid . '
								AND rc.route_rule_type_id = ' . CRouteRuleType::SPECIFIC_VENDORS . '
								AND ap.id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_ap ON TRUE
						
						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING ( ARRAY_AGG( ba.account_name ORDER BY ba.account_name ), \', \' ) AS account_names
							FROM
								bank_accounts ba
							WHERE
								ba.cid = ' . ( int ) $intCid . '
								AND rc.route_rule_type_id = ' . CRouteRuleType::SPECIFIC_BANK_ACCOUNTS . '
								AND ba.id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_ba ON TRUE

						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING ( ARRAY_AGG( art.name ORDER BY art.name ), \', \' ) AS routing_tag_names
							FROM
								ap_routing_tags art
							WHERE
								art.cid = ' . ( int ) $intCid . '
								AND art.deleted_on IS NULL
								AND rc.route_rule_type_id = ' . CRouteRuleType::SPECIFIC_ROUTING_TAGS . '
								AND art.id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_art ON TRUE
						
						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING ( ARRAY_AGG( arc.name ORDER BY arc.name ), \', \' ) AS ar_code_names
							FROM
								ar_codes arc
							WHERE
								arc.cid = ' . ( int ) $intCid . '
								AND rc.route_rule_type_id = ' . CRouteRuleType::FINANCIAL_MOVE_OUT_REFUND_IS_ALLOCATED_TO . '
								AND arc.id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_arc ON TRUE
						
						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING ( ARRAY_AGG( jc.name ORDER BY jc.name ), \', \' ) AS job_category_name
							FROM
								job_categories jc
							WHERE
								jc.cid = ' . ( int ) $intCid . '
								AND rc.route_rule_type_id = ' . CRouteRuleType::SPECIFIC_JOB_CATEGORY . '
								AND jc.id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_jc ON TRUE 
							
						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING ( ARRAY_AGG( ahst.name ORDER BY ahst.name ), \', \' ) AS invoice_po_type_names
							FROM
								ap_header_sub_types ahst
							WHERE
								rc.route_rule_type_id = ' . CRouteRuleType::INVOICE_PO_TYPE_IS . '
								AND ahst.id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_ahst ON TRUE  
							
						LEFT JOIN LATERAL (
							SELECT
								ARRAY_TO_STRING( ARRAY_AGG( concat( ce.name_first, \' \', ce.name_last ) ORDER BY concat( ce.name_first, \' \', ce.name_last ) ), \', \' ) AS company_user_names
							FROM
								company_users cu
								JOIN company_employees ce ON ( cu.cid = ce.cid AND cu.company_employee_id = ce.id )
							WHERE
								cu.cid = ' . ( int ) $intCid . ' 
								AND rc.route_rule_type_id = ' . CRouteRuleType::SPECIFIC_COMPANY_USERS . ' 
								AND cu.id IN ( SELECT json_array_elements ( rc.condition_details )::TEXT::INT )
						) sub_cu ON TRUE
					WHERE
						rr.cid = ' . ( int ) $intCid . '
						AND rr.route_id = ' . ( int ) $intRouteId . '
						AND rr.deleted_on IS NULL
						AND rc.deleted_on IS NULL
					ORDER BY
						rr.is_default_rule,
						rr.order_num DESC,
						rr.id,
						rc.id';

		return self::fetchRouteRules( $strSql, $objClientDatabase );
	}

	public static function fetchLastOrderNumberByRouteIdByCid( $intRouteId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						order_num
					FROM
						route_rules
					WHERE
						cid = ' . ( int ) $intCid . '
						AND route_id = ' . ( int ) $intRouteId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
					ORDER BY
						order_num DESC
					LIMIT 1';

		return self::fetchRouteRule( $strSql, $objClientDatabase );
	}

	public static function fetchRouteRuleByRuleStopIdByRouteTypeIdByCid( $intRuleStopId, $intRouteTypeId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						rr.*
					FROM
						route_rules rr
						JOIN rule_stops rs ON ( rs.cid = rr.cid AND rs.route_id = rr.route_id AND rs.route_rule_id = rr.id )
					WHERE
						rr.cid = ' . ( int ) $intCid . '
						AND rr.route_type_id = ' . ( int ) $intRouteTypeId . '
						AND rs.id = ' . ( int ) $intRuleStopId;

		return self::fetchRouteRule( $strSql, $objDatabase );
	}

}
?>