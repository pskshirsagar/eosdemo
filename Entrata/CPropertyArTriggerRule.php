<?php

class CPropertyArTriggerRule extends CBasePropertyArTriggerRule {

	protected $m_strArTriggerName;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArTriggerId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCacheToLease() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    /**
     * Get Functions
     */

	public function getArTriggerName() {
		return $this->m_strArTriggerName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ar_trigger_name'] ) ) $this->setArTriggerName( $arrmixValues['ar_trigger_name'] );

		return;
	}

	public function setArTriggerName( $strArTriggerName ) {
		$this->m_strArTriggerName = $strArTriggerName;
	}

}
?>