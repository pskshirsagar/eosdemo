<?php

class CLoggedAction extends CBaseLoggedAction {

    public function valSchemaName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTableName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUserName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valActionTstamp() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAction() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOriginalData() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNewData() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valQuery() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClientIp() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

}
?>