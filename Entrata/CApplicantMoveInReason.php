<?php

class CApplicantMoveInReason extends CBaseApplicantMoveInReason {

	const RENT_INCREASED 				= 1;
	const NEW_JOB_OR_TRANSFER			= 2;
	const EASIER_COMMUTE 				= 3;
	const UPGRADE_HOUSING 				= 4;
	const LESS_EXPENSIVE_HOUSING 		= 5;
	const BETTER_NEIGHBORHOOD 			= 6;
	const CLOSER_TO_WORK 				= 7;
	const ATTEND_OR_LEAVE_COLLEGE 		= 8;
	const DOWNSIZING 					= 9;
	const EVICTION 						= 10;
	const UNHAPPY_WITH_MANAGEMENT		= 11;
	const CORPORATE_LEASE_EXPIRED		= 12;
	const APRATMENT_FINISHES 			= 13;
	const LACK_OF_AMENITIES				= 14;
	const LARGER_APRARTMENT				= 15;
	const NEIGHBOR_PROBLEMS_OR_NOISE	= 16;
	const PARKING						= 17;
	const PET_POLICIES					= 18;
	const ROOMMATE_CHANGE_OR_PROBLEMS	= 19;
	const SCHOOL_OUT_OR_GRADUATED		= 20;
	const PERSONAL						= 21;

}
?>