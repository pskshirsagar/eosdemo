<?php

class CCatalogSkuPropertyGroup extends CBaseCatalogSkuPropertyGroup {

	protected $m_intApPayeeId;
	protected $m_intGlAccountId;
	protected $m_intApCodeId;

	protected $m_strSystemCode;

	/**
	 * Get Functions
	 */

	public function getApPayeeId() {
		return $this->m_intApPayeeId;
	}

	public function getApCodeId() {
		return $this->m_intApCodeId;
	}

	public function getGlAccountId() {
		return $this->m_intGlAccountId;
	}

	public function getSystemCode() {
		return $this->m_strSystemCode;
	}

	/**
	 * Set Functions
	 */

	public function setApPayeeId( $intApPayeeId ) {
		$this->m_intApPayeeId = $intApPayeeId;
	}

	public function setApCodeId( $intApCodeId ) {
		$this->m_intApCodeId = $intApCodeId;
	}

	public function setGlAccountId( $intGlAccountId ) {
		$this->m_intGlAccountId = $intGlAccountId;
	}

	public function setSystemCode( $strSystemCode ) {
		$this->m_strSystemCode = $strSystemCode;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ap_payee_id'] ) ) $this->setApPayeeId( $arrmixValues['ap_payee_id'] );
		if( true == isset( $arrmixValues['ap_code_id'] ) ) $this->setApCodeId( $arrmixValues['ap_code_id'] );
		if( true == isset( $arrmixValues['gl_account_id'] ) ) $this->setGlAccountId( $arrmixValues['gl_account_id'] );
		if( true == isset( $arrmixValues['system_code'] ) ) $this->setSystemCode( $arrmixValues['system_code'] );
		return;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPropertyGroupId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valApCatalogItemId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>