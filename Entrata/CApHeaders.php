<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CApHeaders
 * Do not add any new functions to this class.
 */
use Psi\Eos\Entrata\CApBatches;
use Psi\Eos\Entrata\CProperties;


class CApHeaders extends CBaseApHeaders {

	public static function fetchApHeadersByPoApHeaderIdByCid( $intPoApHeaderId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						ap.company_name,
						ah.header_number,
						ah.po_ap_header_ids,
						ah.id
					FROM
						ap_headers ah
						JOIN ap_payees ap ON ap.cid = ah.cid AND ap.id = ah.ap_payee_id
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.po_ap_header_ids @> ARRAY[' . ( int ) $intPoApHeaderId . ']::INT[]
						AND ah.reversal_ap_header_id IS NULL
						AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
						AND NOT ah.is_template
					ORDER BY
						ah.id DESC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApHeaderByIdByCid( $intApHeaderId, $intCid, $objClientDatabase, $boolHasJobCostingProduct = false ) {
		if( false == valId( $intApHeaderId ) ) return NULL;

		$strJobCostingJoins = '';
		$strJobCostingFields = '';
		$strJobCostingGroupBy = '';

		if( true == $boolHasJobCostingProduct ) {
			$strJobCostingJoins = 'LEFT JOIN job_phases jp ON( jp.id = ah.bulk_job_phase_id AND jp.cid = ah.cid )
									LEFT JOIN jobs j ON( j.id = jp.job_id AND j.cid = jp.cid )
									LEFT JOIN ap_contracts ac ON( ac.id = ah.bulk_ap_contract_id AND ac.cid = ah.cid )';

			$strJobCostingFields = ',
									jp.name AS job_phase_name,
									concat_ws( \' - \', j.name, jp.name ) AS bulk_job_phase_name,
									ac.name AS bulk_ap_contract_name';

			$strJobCostingGroupBy = ',
									jp.name,
									j.name,
									ac.name';
		}

		$strSql = '
			WITH ad AS (
				SELECT
					ad.id,
					ad.cid,
					ad.ap_header_id,
					ad.gl_transaction_type_id,
					ad.post_month,
					ad.property_id,
					ad.pre_approval_amount,
					ad.transaction_amount,
					ad.transaction_amount_due,
					reim_ad.ap_header_id AS reimbursement_ap_header_id
				FROM
					ap_details ad
					LEFT JOIN ap_detail_reimbursements adr ON ad.cid = adr.cid AND ad.id = adr.original_ap_detail_id AND adr.is_deleted = false
					LEFT JOIN ap_details reim_ad ON adr.cid = reim_ad.cid AND adr.reimbursement_ap_detail_id = reim_ad.id
				WHERE
					ad.cid = ' . ( int ) $intCid . '
					AND ad.ap_header_id = ' . ( int ) $intApHeaderId . '
					AND ad.deleted_by IS NULL
					AND ad.deleted_on IS NULL
			)

			SELECT DISTINCT
				ah.*,
				func_format_refund_customer_names( ah.header_memo ) AS refund_customer_names,
				po_ah.header_number AS po_number,
				ap.ap_payee_status_type_id,
				CASE
					WHEN ah.lease_customer_id IS NULL THEN ( COALESCE ( ap.company_name, \'\' ) || \' \' || COALESCE ( apl.location_name, \'\' ) )
					ELSE func_format_refund_customer_names( ah.header_memo )
				END AS payee_name,
				cua.street_line1,
				cua.street_line2,
				cua.city,
				cua.state_code,
				cua.postal_code,
				cua.province,
				c.name AS country,
				apt.name AS ap_payee_term,
				cu.username,
				ce.name_first AS company_employee_name_first,
				ce.name_last AS company_employee_name_last,
				SUM ( COALESCE ( ad.pre_approval_amount, 0 ) ) AS pre_approval_amount,
				SUM ( COALESCE ( ad.transaction_amount_due, 0 ) ) AS transaction_amount_due,
				CASE
					WHEN 0 <> MAX ( COALESCE ( ad.pre_approval_amount, 0 ) ) OR 0 <> MIN ( COALESCE ( ad.pre_approval_amount, 0 ) )
						THEN 1
					ELSE 0
				END AS is_approved,
				apa.account_number AS ap_payee_account_number,
				CASE
					WHEN p2.lookup_code IS NULL THEN p2.property_name
					ELSE p2.lookup_code || \' - \' || p2.property_name
				END AS property_name,
				art.name AS ap_routing_tag_name,
				MAX( reimbed_ah.header_number ) AS reimbursed_header_number,
				apl.vendor_code' . $strJobCostingFields . '
			FROM
				ap_headers ah
				LEFT JOIN ap_headers po_ah ON ah.cid = po_ah.cid
					AND ah.po_ap_header_ids && ARRAY[po_ah.id]::INT[]
					AND ' . CApHeaderType::PURCHASE_ORDER . ' = po_ah.ap_header_type_id
				LEFT JOIN ap_payee_accounts apa ON ( ah.cid = apa.cid AND ah.ap_payee_account_id = apa.id )
				LEFT JOIN properties p2 ON ( ah.cid = p2.cid AND ah.bulk_property_id = p2.id )
				LEFT JOIN ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
				LEFT JOIN ap_headers reimbed_ah ON ( ad.cid = reimbed_ah.cid AND reimbed_ah.id = ad.reimbursement_ap_header_id  )
				LEFT JOIN ap_payee_terms apt ON ( ah.ap_payee_term_id = apt.id AND apt.cid = ah.cid )
				LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
				LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
				JOIN company_users cu ON ( cu.cid = ah.cid AND cu.id = ah.created_by  AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' ) )
				LEFT JOIN company_employees ce ON ( cu.cid = ce.cid AND ce.id = cu.company_employee_id )
				LEFT JOIN lease_customers lc ON ( ah.cid = lc.cid AND ah.lease_customer_id = lc.id )
				LEFT JOIN lease_processes lp ON ( lp.cid = lc.cid AND lp.lease_id = lc.lease_id AND lp.customer_id IS NULL )
				LEFT JOIN lease_customers lc1 ON(
													lc.cid = lc1.cid
													AND lc.lease_id = lc1.lease_id
													AND
														(
															CASE
																WHEN ah.lease_customer_id IS NOT NULL AND lp.refund_method_id = ' . CRefundMethod::MULTIPLE_REFUNDS . ' THEN lc1.id = ah.lease_customer_id
																ELSE lc1.customer_type_id = ' . ( int ) CCustomerType::PRIMARY . '
															END
														)
												)
				LEFT JOIN customer_addresses cua ON ( ah.cid = cua.cid AND cua.customer_id = lc1.customer_id AND address_type_id = ' . ( int ) CAddressType::FORWARDING . ' )
				LEFT JOIN countries c ON ( cua.country_code = c.code )
				LEFT JOIN ap_routing_tags art ON ( ah.cid = art.cid AND ah.ap_routing_tag_id = art.id )
				' . $strJobCostingJoins . '
			WHERE
				ah.id = ' . ( int ) $intApHeaderId . '
				AND ah.cid = ' . ( int ) $intCid . '
			GROUP BY
				ah.id,
				ah.cid,
				ap.company_name,
				apl.location_name,
				apl.vendor_code,
				po_ah.header_number,
				ap.ap_payee_status_type_id,
				cua.street_line1,
				cua.street_line2,
				cua.city,
				cua.state_code,
				cua.postal_code,
				cua.province,
				country,
				ap_payee_term,
				cu.username,
				company_employee_name_first,
				company_employee_name_last,
				apa.account_number,
				apa.default_ap_remittance_id,
				p2.property_name,
				p2.lookup_code,
				art.name' . $strJobCostingGroupBy;

		return self::fetchApHeader( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApHeadersByIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'WITH ad AS (
						SELECT
							ad.id,
							ad.cid,
							ad.ap_header_id,
							ad.gl_transaction_type_id,
							ad.post_month,
							ad.property_id,
							ad.pre_approval_amount,
							ad.transaction_amount,
							SUM( ABS( aa.allocation_amount ) ) AS allocation_amount,
							ad.transaction_amount_due
						FROM
							ap_details ad
							LEFT JOIN ap_allocations aa ON aa.cid = ad.cid
								AND aa.is_deleted = false
								AND (
									ad.transaction_amount > 0 AND ad.id = aa.charge_ap_detail_id
									OR
									ad.transaction_amount <= 0 AND ad.id = aa.credit_ap_detail_id
								)
						WHERE
							ad.cid = ' . ( int ) $intCid . '
							AND ad.ap_header_id IN ( ' . implode( ',', array_filter( $arrintApHeaderIds ) ) . ' )
							AND ad.deleted_by IS NULL
							AND ad.deleted_on IS NULL
						GROUP BY
							ad.id,
							ad.cid
					)

					SELECT
						ah.*,
						ap.ap_payee_status_type_id,
						ap.ap_payee_type_id,
						apl.payee_name AS payee_name,
						apl.vendor_code,
						apt.name AS ap_payee_term,
						SUM ( COALESCE ( ad.pre_approval_amount, 0 ) ) AS pre_approval_amount,
						SUM ( COALESCE ( ad.allocation_amount, 0 ) ) AS paid_amount,
						CASE
							WHEN ah.lease_customer_id IS NULL THEN ( COALESCE ( ap.company_name, \'\' ) || \' \' || COALESCE ( apl.location_name, \'\' ) )
							ELSE func_format_refund_customer_names ( ah.header_memo )
						END AS ap_payee_name,
						apa.account_number AS ap_payee_account_number,
						apl.location_name
					FROM
						ap_headers ah
						LEFT JOIN ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
						LEFT JOIN ap_payee_terms apt ON ( ah.ap_payee_term_id = apt.id AND apt.cid = ah.cid )
						LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_locations apl ON ( ah.cid = apl.cid AND ah.ap_payee_id = apl.ap_payee_id AND ah.ap_payee_location_id = apl.id )
						LEFT JOIN ap_payee_accounts apa ON ( ah.cid = apa.cid AND ah.ap_payee_id = apa.ap_payee_id AND ah.ap_payee_account_id = apa.id )
					WHERE
						ah.id IN ( ' . implode( ',', array_filter( $arrintApHeaderIds ) ) . ' )
						AND ah.cid = ' . ( int ) $intCid . '
						AND NOT ah.is_template
					GROUP BY
						ah.id,
						ah.cid,
						ap.ap_payee_status_type_id,
						apl.vendor_code,
						ap.ap_payee_type_id,
						payee_name,
						ap_payee_term,
						ap_payee_name,
						apa.account_number,
						apl.location_name';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchIntegratedApHeadersByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND remote_primary_key IS NOT NULL';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeaderByRemotePrimaryKeyByApPayeeIdByCid( $strRemotePrimaryKey, $intApPayeeId, $intCid, $objClientDatabase ) {
		return self::fetchApHeader( 'SELECT * FROM ap_headers WHERE ap_payee_id = ' . ( int ) $intApPayeeId . ' AND cid = ' . ( int ) $intCid . ' AND LOWER( remote_primary_key ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strRemotePrimaryKey ) ) ) . '\'', $objClientDatabase );
	}

	private static function createTempTableApHeadersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter = NULL, $intCompanyUserId = NULL, $boolIsAdministrator = false ) {

		if( 't' == fetchData( 'SELECT util.util_temp_table_exists(\'temp_ap_headers_listing\') AS result;', $objClientDatabase )[0]['result'] ) {
			return;
		}

		$strOrderBy   = 'ah.id DESC';
		$arrstrSortBy = [ 'header_number', 'post_date', 'post_month', 'due_date', 'ap_payee_name', 'vendor_code', 'property_name', 'is_posted', 'ap_physical_status_type_id', 'is_attachment', 'transaction_amount', 'transaction_amount_due' ];
		if( $objApTransactionsFilter->getApHeaderTypeId() == CApHeaderType::INVOICE ) {
			$intKeyOfElement = array_search( 'ap_physical_status_type_id', $arrstrSortBy );
			unset( $arrstrSortBy[$intKeyOfElement] );
		}
		if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy ) ) {

			$strOrderBy = $objApTransactionsFilter->getSortBy() . ' ' . $objApTransactionsFilter->getSortDirection() . ', ah.header_number ' . $objApTransactionsFilter->getSortDirection();

			if( 'header_number' == $objApTransactionsFilter->getSortBy() ) {
				$strOrderBy = $objApTransactionsFilter->getSortBy() . ' ' . $objApTransactionsFilter->getSortDirection();
			}

			if( 'is_posted' == $objApTransactionsFilter->getSortBy() ) {
				$strOrderBy = ( ( 'ASC' == $objApTransactionsFilter->getSortDirection() ) ?
					'CASE 
						WHEN ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' 
						THEN \'Rejected\' END, 
					 CASE 
						WHEN ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN
						THEN \'Posted\' END, 
					CASE 
						WHEN ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN 
						THEN \'Not Posted\' END ' . $objApTransactionsFilter->getSortDirection() :
					'CASE
						WHEN ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN 
						THEN \'Not Posted\' END, 
					CASE
						WHEN ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN 
						THEN \'Posted\' END, 
					CASE 
						WHEN ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' 
						THEN \'Rejected\' END ' . $objApTransactionsFilter->getSortDirection() );
			}
		}

		$strSqlJoins					= NULL;
		$strConditions					= NULL;
		$strJoinConditions				= NULL;
		$strHavingConditions			= NULL;
		$strPropertyCondition			= NULL;
		$strCheckRangeCondition			= NULL;
		$strMultiPropertyCondition		= NULL;
		$strAdvanceSearchCondition		= NULL;
		$strStandardInvoiceCondition	= NULL;

		$strApHeaderIdsCondition = ( true == valArr( array_filter( ( array ) $objApTransactionsFilter->getApHeadersIds() ) ) ) ? ' ah.id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' ) ' : ' 1 = 1 ';

		if( false == $objApTransactionsFilter->getIsExport() ) {
			$objApTransactionsFilter->setApHeadersIds( NULL ); // Temp Fix for Task #1590325 - Pending but approved invoices
		}

		$arrstrAndConditions = ( array ) self::fetchSearchCriteria( $intCid, $objApTransactionsFilter );

		if( true == array_key_exists( 'from_clause', $arrstrAndConditions ) ) {
			$strJoinConditions .= array_shift( $arrstrAndConditions['from_clause'] );
		}

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		if( 1 == $objApTransactionsFilter->getIsShowExportedTransactions() ) {
			$strConditions .= ' AND ad.ap_export_batch_id IS NOT NULL';
		} elseif( true == is_numeric( $objApTransactionsFilter->getIsShowExportedTransactions() ) && 0 == $objApTransactionsFilter->getIsShowExportedTransactions() ) {
			$strConditions .= ' AND ad.ap_export_batch_id IS NULL';
		}

		if( true == array_key_exists( 'having_clause', $arrstrAndConditions ) ) {
			$strHavingConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['having_clause'] );
		}

		$strStandardInvoiceCondition = ' AND ah.ap_header_sub_type_id IN ( ' . sqlIntImplode( $arrintInvoiceTypePermissions ) . ' ) ';

		// Advanced Search for Payment filter Terms
		if( true == valStr( $objApTransactionsFilter->getApPayeeTerms() ) ) {
			$strAdvanceSearchCondition .= ' AND ah.ap_payee_term_id = ' . $objApTransactionsFilter->getApPayeeTerms();
			$strSqlJoins .= ' JOIN ap_payee_terms apt ON ah.cid = apt.cid AND ah.ap_payee_term_id = apt.id';
		}

//		Combination of Both Unit Number & Building Name
		if( true == valArr( $objApTransactionsFilter->getPropertyBuildings() ) || true == valArr( $objApTransactionsFilter->getPropertyUnits() ) ) {

			$intPropertyUnitCount	= 0;
			$intBuildingCount		= 0;
			$arrstrPropertyUnits	= array_filter( $objApTransactionsFilter->getPropertyUnits() );
			$arrintBuildingUnits	= array_filter( $objApTransactionsFilter->getPropertyBuildings() );

			$strAdvanceSearchCondition .= ' AND (';

			foreach( $arrintBuildingUnits as $strBuildingIds ) {
				if( $intBuildingCount != 0 ) {
					$strAdvanceSearchCondition .= 'OR';
				}

				$strBuildingIds = preg_replace( '/[{}]/', '', $strBuildingIds );
				$strAdvanceSearchCondition .= ' ad.property_building_id IN (  ' . $strBuildingIds . ' ) ';
				$intBuildingCount++;
			}

			if( 0 != $intBuildingCount ) {
				$strSqlJoins .= ' JOIN property_buildings pb ON ad.cid = pb.cid  AND ad.property_building_id = pb.id';
				if( NULL != $arrstrPropertyUnits ) $strAdvanceSearchCondition .= 'OR';
			}

			foreach( $arrstrPropertyUnits as $strPropertyUnit ) {
				if( $intPropertyUnitCount != 0 ) {
					$strAdvanceSearchCondition .= 'OR';
				}

				$strPropertyUnit = preg_replace( '/[{}]/', '', $strPropertyUnit );

				if( false != \Psi\CStringService::singleton()->strpos( $strPropertyUnit, '-' ) ) {

					$strAdvanceSearchCondition .= '( ad.property_building_id, ad.property_unit_id ) IN ( ' . str_replace( '-', ', ',  '( ' . str_replace( ',', ' ), ( ',  $strPropertyUnit ) . ' )' ) . ' )';
				} else {
					$strAdvanceSearchCondition .= '  ad.property_unit_id IN ( ' . $strPropertyUnit . ' ) ';
				}
				$intPropertyUnitCount++;
			}

			if( $intPropertyUnitCount > 0 ) {
				$strSqlJoins .= ' JOIN property_units pu ON ad.cid = pu.cid AND ad.property_unit_id = pu.id';
			}

			$strAdvanceSearchCondition .= ') ';
		}

		if( false == is_null( $objApTransactionsFilter->getHeaderMemo() ) ) {
			$strAdvanceSearchCondition .= ' AND ah.header_memo ILIKE \'%' . trim( addslashes( $objApTransactionsFilter->getHeaderMemo() ) ) . '%\' ';
		}
		if( false == is_null( $objApTransactionsFilter->getDescription() ) ) {
			$strAdvanceSearchCondition .= ' AND ad.description ILIKE \'%' . trim( addslashes( $objApTransactionsFilter->getDescription() ) ) . '%\' ';
		}

		if( true == valIntArr( $objApTransactionsFilter->getRoutingTagIds() ) ) {
			$strAdvanceSearchCondition .= ' AND ah.ap_routing_tag_id IN ( ' . implode( ',', $objApTransactionsFilter->getRoutingTagIds() ) . ' )';
			$strSqlJoins .= ' JOIN ap_routing_tags art ON ah.cid = art.cid AND ah.ap_routing_tag_id = art.id';
		}

		if( true == valStr( $objApTransactionsFilter->getApCustomTags() ) ) {
			$strAdvanceSearchCondition .= ' AND ad.gl_dimension_id = ' . $objApTransactionsFilter->getApCustomTags();
			$strSqlJoins .= ' JOIN gl_dimensions gd ON ad.cid = gd.cid AND ad.gl_dimension_id = gd.id';
		}

		if( false == is_null( $objApTransactionsFilter->getGlAccounts() ) && true == in_array( 'no_gl_accounts', $objApTransactionsFilter->getGlAccounts() ) && false == in_array( 'all_gl_accounts', $objApTransactionsFilter->getGlAccounts() ) ) {
			$strAdvancedGlAccountCondition = ( 1 < \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getGlAccounts() ) ) ? 'OR ad.gl_account_id IN ( ' . trim( ( implode( ', ', array_diff( $objApTransactionsFilter->getGlAccounts(), [ 'all_gl_accounts', 'no_gl_accounts' ] ) ) ), ' ,' ) . ' )' : '';
			$strAdvanceSearchCondition .= ' AND ad.gl_account_id IS NULL ' . $strAdvancedGlAccountCondition;
		} elseif( false == is_null( $objApTransactionsFilter->getGlAccounts() ) ) {
			$strAdvanceSearchCondition .= ' AND ad.gl_account_id IN ( ' . trim( ( implode( ', ', array_diff( $objApTransactionsFilter->getGlAccounts(), [ 'all_gl_accounts', 'no_gl_accounts' ] ) ) ), ' ,' ) . ' )';
		}

		if( true == valIntArr( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
			$strJoinConditions .= ' LEFT JOIN ap_payee_compliance_statuses apcs ON ( ad.cid = apcs.cid AND ah.ap_payee_id = apcs.ap_payee_id AND ad.property_id = apcs.property_id AND apcs.ap_legal_entity_id = apl.ap_legal_entity_id )';
			if( true == in_array( 1, $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
				$strAdvanceSearchCondition .= ' AND apcs.compliance_status_id IS NULL';
			} elseif( true == in_array( 1, $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) && 1 < \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
				$strAdvanceSearchCondition .= ' AND ( apcs.compliance_status_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) . ' ) OR apcs.compliance_status_id IS NULL )';
			} else {
				$strAdvanceSearchCondition .= ' AND apcs.compliance_status_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) . ' )';
			}
		}

		if( true == valArr( $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {
			$arrintInvoiceTypeIds = array_flip( $objApTransactionsFilter->getApInvoiceTypeIds() );
			if( true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_REFUND_INVOICE, $objApTransactionsFilter->getApInvoiceTypeIds() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

				$strAdvanceSearchCondition .= ' AND ah.lease_customer_id IS NOT NULL';
			} elseif( true == valIntArr( $objApTransactionsFilter->getJobIds() ) && true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

				unset( $arrintInvoiceTypeIds[CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT] );
				if( true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_REFUND_INVOICE, $objApTransactionsFilter->getApInvoiceTypeIds() ) && true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

					$strAdvanceSearchCondition .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintInvoiceTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT . ' AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) ) OR ah.lease_customer_id IS NOT NULL )';
				} elseif( true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

					$strAdvanceSearchCondition .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApInvoiceTypeIds() ) . ' ) AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) )';
				} elseif( false == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_REFUND_INVOICE, $objApTransactionsFilter->getApInvoiceTypeIds() ) && true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

					$strAdvanceSearchCondition .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintInvoiceTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT . ' AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) ) )';
				}

				$strJoinConditions .= ' LEFT JOIN job_phases jp ON ad.cid = jp.cid AND ad.job_phase_id = jp.id';
			} elseif( true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_REFUND_INVOICE, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {

				$strAdvanceSearchCondition .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApInvoiceTypeIds() ) . ' ) OR ah.lease_customer_id IS NOT NULL )';
			} else {

				$strAdvanceSearchCondition .= ' AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApInvoiceTypeIds() ) . ' ) AND ah.lease_customer_id IS NULL';
			}

			if( true == valArr( $objApTransactionsFilter->getJobCostCodeIds() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApInvoiceTypeIds() ) && true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {
				$strAdvanceSearchCondition .= ' AND ( ah.ap_header_sub_type_id = ' . CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT . ' AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) )';
			} elseif( true == valArr( $objApTransactionsFilter->getJobCostCodeIds() ) && true == in_array( CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT, $objApTransactionsFilter->getApInvoiceTypeIds() ) ) {
				$strAdvanceSearchCondition .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintInvoiceTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApTransactionsFilter::AP_INVOICE_SUB_TYPE_STANDARD_JOB_OR_CONTRACT . ' AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) ) )';
			}

		}

		if( true == valArr( $objApTransactionsFilter->getPropertyGroupIds() ) ) {

			$strPropertyCondition = ' AND ( ah.id, ad.property_id ) IN (
														SELECT
															DISTINCT ap_header_id,
                                                            property_id
														FROM
															ap_details
														WHERE
															cid = ' . ( int ) $intCid . '
															AND ah.id = ap_header_id
															AND property_id IN (	SELECT 
																						pga.property_id 
																					FROM 
																						property_group_associations AS pga
																						JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
																					WHERE 
																						pg.cid = ' . ( int ) $intCid . '
																						AND pg.id IN ( SELECT 
																						pga.property_id 
																					FROM 
																						property_group_associations AS pga
																						JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
																					WHERE 
																						pg.cid = ' . ( int ) $intCid . '
																						AND pg.id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyGroupIds() ) . ' )
																						AND pg.deleted_by IS NULL
																						AND pg.deleted_on IS NULL
																					 )
																					AND pg.deleted_by IS NULL
																					AND pg.deleted_on IS NULL
																				)
															AND deleted_by IS NULL
															AND deleted_on IS NULL
													)';
		} else {
			$strPropertyCondition = ' AND CASE
											WHEN ah.control_total = 0
											THEN CASE
													WHEN ah.lease_customer_id IS NOT NULL
													THEN ad.property_id IN ( SELECT property_id FROM pg_temp.load_properties_info )
													ELSE EXISTS (
																	SELECT
																		ap_header_id
																	FROM
																		ap_details
																	WHERE
																		cid = ' . ( int ) $intCid . '
																		AND ah.id = ap_header_id
																		AND ad.property_id IN( SELECT property_id FROM pg_temp.load_properties_info )
																		AND deleted_by IS NULL
																		AND deleted_on IS NULL
																	)
												END
											ELSE
												ah.id = ah.id
											AND ah.cid = ah.cid
										END ';
		}

		if( false == $boolIsAdministrator && true == valId( $intCompanyUserId ) ) {

			$strMultiPropertyCondition = ' AND ah.id NOT IN(
																SELECT
																	DISTINCT( ad.ap_header_id )
																FROM
																	ap_details ad
																	LEFT JOIN ( ' . CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS vcup ON( vcup.cid = ad.cid AND vcup.id = ad.property_id )
																WHERE
																	ad.cid = ' . ( int ) $intCid . '
																	AND ( vcup.is_disabled = 1 OR vcup.id IS NULL )
														)';
		}

		if( true == is_numeric( $objApTransactionsFilter->getCheckMinNumber() ) && true == is_numeric( $objApTransactionsFilter->getCheckMaxNumber() ) ) {

			$strCheckRangeCondition = 'AND ah.id IN (
												SELECT
													DISTINCT unnest(string_to_array(ah.header_number, \',\'))::INT
												FROM
													ap_payments ap
													JOIN ap_headers ah ON (ap.cid = ah.cid AND ap.id = ah.ap_payment_id AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . ')
												WHERE ap.cid = ' . ( int ) $intCid . '
													AND ap.ap_payment_type_id = ' . CApPaymentType::CHECK . '
													AND ap.payment_number::INT >= ' . ( int ) $objApTransactionsFilter->getCheckMinNumber() . '
													AND ap.payment_number::INT <= ' . ( int ) $objApTransactionsFilter->getCheckMaxNumber() . ' 
													AND ap.payment_status_type_id != ' . CPaymentStatusType::VOIDED . ')';
		}

		if( true == is_numeric( $objApTransactionsFilter->getIsDrawn() ) ) {
			$strJoinConditions .= ' LEFT JOIN draw_request_details drd ON drd.cid = ad.cid AND drd.ap_detail_id = ad.id';
		}

		$strSql = '
				DROP TABLE IF EXISTS pg_temp.load_properties_info;
				CREATE TEMP TABLE load_properties_info AS (
					SELECT
						lp.property_id
					FROM
						load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $arrintPropertyIds ) . '], ARRAY[' . CPsProduct::ENTRATA . '], true ) lp
				);
				ANALYZE pg_temp.load_properties_info;

				DROP TABLE IF EXISTS pg_temp.temp_ap_headers_listing;
				CREATE TEMP TABLE temp_ap_headers_listing AS (
					SELECT
						ah.id,
						ah.cid,
						ah.ap_payee_id,
						ah.ap_header_sub_type_id,
						ah.ap_payee_term_id,
						ah.lease_customer_id,
						ah.ap_payment_id,
						ah.po_ap_header_ids,
						ah.reversal_ap_header_id,
						ah.refund_ar_transaction_id,
						ah.transaction_datetime,
						ah.bulk_is_confidential,
						CASE
							WHEN ah.lease_customer_id IS NULL THEN ( COALESCE ( ap.company_name, \'\' ) || \' \' || COALESCE ( apl.location_name, \'\' ) )
							ELSE func_format_refund_customer_names ( ah.header_memo )
						END AS ap_payee_name,
						CASE
							WHEN MAX ( ad.property_id ) <> MIN ( ad.property_id ) THEN \'Multiple\'
							ELSE MAX ( p.property_name )
						END AS property_name,
						SUM ( COALESCE( ad.pre_approval_amount, 0 ) ) AS pre_approval_amount,
						SUM ( COALESCE( ad.transaction_amount, 0 ) ) AS transaction_amount,
						SUM ( COALESCE( ad.transaction_amount_due, 0 ) ) AS transaction_amount_due,
						ah.post_date,
						ah.due_date,
						ah.post_month,
						ah.header_number,
						ah.account_number,
						ah.header_memo,
						CASE
							WHEN ( ah.header_memo IS NOT NULL ) THEN func_format_refund_customer_names( ah.header_memo )
						END AS refund_customer_names,
						ah.is_temporary,
						ah.is_posted::integer,
						ah.ap_financial_status_type_id,
						ah.is_initial_import,
						ah.created_on,
						ap.ap_payee_status_type_id,
						apa.account_number AS ap_payee_account_number,
						ad.ap_export_batch_id,
						CASE
							WHEN fa.ap_header_id IS NOT NULL THEN 1
							ELSE 0
						END AS is_attachment,
						apl.vendor_code
					FROM
						ap_headers ah
						JOIN ap_payees ap ON ah.cid = ap.cid AND ah.ap_payee_id = ap.id
						JOIN ap_payee_locations apl ON apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id
						JOIN ap_details ad ON ah.cid = ad.cid AND ah.id = ad.ap_header_id
						JOIN properties p ON ad.cid = p.cid AND p.id = ad.property_id AND p.id IN ( SELECT property_id FROM pg_temp.load_properties_info )
						' . $strSqlJoins . '
						JOIN ap_payee_accounts apa ON ah.cid = apa.cid AND ah.ap_payee_account_id = apa.id
						LEFT JOIN LATERAL
							(
								SELECT
									fa.id,
									fa.ap_header_id
								FROM
									file_associations fa
								WHERE
									ah.cid = fa.cid
									AND ah.id = fa.ap_header_id
									AND fa.deleted_by IS NULL
								ORDER BY
									fa.id DESC
								LIMIT 1
							) fa ON true
						' . $strJoinConditions . '
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . ( int ) $objApTransactionsFilter->getApHeaderTypeId() . '
						AND ah.is_batching = false
						AND NOT ah.is_template
						AND ah.ap_payment_id IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
						' . $strConditions . $strAdvanceSearchCondition . $strPropertyCondition . $strMultiPropertyCondition . $strStandardInvoiceCondition . $strCheckRangeCondition . '
						AND (
								( ' . $strApHeaderIdsCondition . ' )
								OR
								( SELECT id FROM rule_stop_results rsr WHERE rsr.cid = ah.cid AND rsr.reference_id = ah.id AND NOT rsr.is_archived AND rsr.route_type_id = ' . CRouteType::INVOICE . ' LIMIT 1 ) IS NULL
							)

					GROUP BY
						ah.id,
						ah.cid,
						ap.ap_payee_status_type_id,
						apa.account_number,
						ap.company_name,
						apl.vendor_code,
						apl.location_name,
						ad.ap_export_batch_id,
						fa.ap_header_id
					HAVING
						1 = 1
						' . $strHavingConditions . '
					ORDER BY
						' . $strOrderBy . '
				);
				ANALYZE pg_temp.temp_ap_headers_listing;';

		fetchData( $strSql, $objClientDatabase );

		return true;
	}

	public static function fetchPaginatedApHeadersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objPagination, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter = NULL, $intCompanyUserId = NULL, $boolIsAdministrator = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintInvoiceTypePermissions ) ) {
			return NULL;
		}

		self::createTempTableApHeadersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter, $intCompanyUserId, $boolIsAdministrator );

		$strSql = 'SELECT * FROM pg_temp.temp_ap_headers_listing';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersCountByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter = NULL, $intCompanyUserId = NULL, $boolIsAdministrator = false ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintInvoiceTypePermissions ) ) {
			return NULL;
		}

		self::createTempTableApHeadersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $arrintInvoiceTypePermissions, $objApTransactionsFilter, $intCompanyUserId, $boolIsAdministrator );

		$strSql = 'SELECT count( id ), SUM( CASE WHEN is_attachment = 1 THEN 1 ELSE 0 END ) AS attachments_count FROM pg_temp.temp_ap_headers_listing';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedApHeadersForDashboardByPropertyIdsByCompanyUserIdByCid( $intPropertyIdsCount, $arrintPropertyIds, $intCompanyUserId, $intCid, $objPagination, $objClientDatabase, $objApTransactionsFilter = NULL, $boolIsAdministrator = false, $intPeriodId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strOrderBy              = 'ah.id DESC';
		$strConditions           = NULL;
		$strClosingCondition     = NULL;
		$strPropertyCondition    = NULL;
		$strApHeaderIdsCondition = NULL;
		$arrstrSortBy            = [ 'pending_approval', 'id', 'transaction_amount', 'ap_payee_name', 'property_name', 'created_on' ];

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valStr( $objApTransactionsFilter->getSortBy() )
		    && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy ) ) {

			$strOrderBy = $objApTransactionsFilter->getSortBy() . ' ' . $objApTransactionsFilter->getSortDirection();
		}

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objApTransactionsFilter->getPropertyIds() ) && $intPropertyIdsCount != \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getPropertyIds() ) ) {
			$strPropertyCondition = ' AND ah.id IN (
														SELECT
															ap_header_id
														FROM
															ap_details
														WHERE
															cid = ' . ( int ) $intCid . '
															AND property_id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' )
															AND deleted_by IS NULL
															AND deleted_on IS NULL
													)';
		} else {
			$strPropertyCondition = ' AND CASE
											WHEN ah.control_total = 0
											THEN CASE
													WHEN ah.lease_customer_id IS NOT NULL
													THEN ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
													ELSE ah.id IN (
																	SELECT
																		ap_header_id
																	FROM
																		ap_details
																	WHERE
																		cid = ' . ( int ) $intCid . '
																		AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
																		AND deleted_by IS NULL
																		AND deleted_on IS NULL
																	)
												END
											ELSE
												ah.id = ah.id
											AND ah.cid = ah.cid
										END ';
		}

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {
			$strApHeaderIdsCondition = ( true == valArr( $objApTransactionsFilter->getApHeadersIds() ) ) ? ' AND ah.id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' ) ' : '';
		}

		if( true == valId( $intPeriodId ) ) {
			$strClosingCondition = 'JOIN periods pd ON ( ad.cid = pd.cid AND ad.property_id = pd.property_id AND pd.id = ' . ( int ) $intPeriodId . ' AND ah.post_month <= pd.post_month ) ';
		}

		if( ( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == $objApTransactionsFilter->getHideDisabledProperties() ) || true == valId( $intPeriodId ) ) {

			$arrstrAndConditions['where_clause'][] = ' ah.id NOT IN (
																		SELECT
																			DISTINCT ad.ap_header_id
																		FROM
																			ap_details ad
																			LEFT JOIN ( ' . CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS vcup ON( vcup.cid = ad.cid AND vcup.id = ad.property_id AND vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' )
																			LEFT JOIN rule_stop_results rsr ON ( ad.cid = rsr.cid AND ad.ap_header_id = rsr.reference_id AND NOT rsr.is_archived AND rsr.route_type_id = ' . CRouteType::INVOICE . ' )
																		WHERE
																			ad.cid = ' . ( int ) $intCid . '
																			AND rsr.id IS NULL
																			AND ( 
																					( vcup.id IS NULL )
																					OR
																					( vcup.is_disabled = 1 AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
																				)
																	)';
		}

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == is_numeric( $objApTransactionsFilter->getIsPosted() ) ) {
			$arrstrAndConditions['where_clause'][] = 'ah.is_posted = ' . ( int ) $objApTransactionsFilter->getIsPosted() . ' ::BOOLEAN';
		} elseif( true == valId( $intPeriodId ) ) {
			$arrstrAndConditions['where_clause'][] = 'ah.is_posted = false';
		}

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		$strSql = 'SELECT
						ah.id,
						ah.cid,
						ah.ap_payee_id,
						ah.ap_payee_term_id,
						ah.lease_customer_id,
						ah.ap_payment_id,
						ah.po_ap_header_ids,
						ah.reversal_ap_header_id,
						ah.refund_ar_transaction_id,
						ah.transaction_datetime,
						CASE
							WHEN ah.lease_customer_id IS NULL
							THEN ( COALESCE( ap.company_name, \'\' ) || \' \' || COALESCE( apl.location_name, \'\' ) )
							ELSE func_format_refund_customer_names( ah.header_memo )
						END AS ap_payee_name,
						CASE
							WHEN MAX( ad.property_id ) <> MIN( ad.property_id )
							THEN \'Multiple\'
							ELSE MAX( p.property_name )
						END AS property_name,
						(
							SELECT
								SUM ( COALESCE( ad.transaction_amount, 0 ) )
							FROM
								ap_details ad
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND ah.cid = ad.cid
								AND ah.id = ad.ap_header_id
								AND ah.gl_transaction_type_id = ad.gl_transaction_type_id
								AND ah.post_month = ad.post_month
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL
						) AS transaction_amount,
						ah.post_month,
						ah.header_number,
						ah.is_temporary,
						ah.is_posted,
						ah.created_on,
						ah.post_date,
						ap.ap_payee_status_type_id,
						(

							SELECT
								CASE
									WHEN MIN(ad.approved_on) IS NOT NULL
									THEN MIN(ad.approved_on)
									ELSE MIN(ad.created_on)
								END
							FROM
								ap_details ad
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND ad.cid = ah.cid
								AND ad.ap_header_id = ah.id
								AND ad.gl_transaction_type_id = ah.gl_transaction_type_id
								AND ad.post_month = ah.post_month
						) AS pending_approval
					FROM
						ap_headers ah
						LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
						LEFT JOIN properties p ON ( ad.cid = p.cid AND p.id = ad.property_id )
						LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						' . $strClosingCondition . '
					WHERE
						ah.is_batching = FALSE
						AND ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payment_id IS NULL
						AND ah.is_template = FALSE
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
						AND ( ap.ap_payee_status_type_id <> ' . CApPayeeStatusType::INACTIVE . ' OR ah.lease_customer_id IS NOT NULL )
						AND ah.reversal_ap_header_id IS NULL
						AND ah.ap_financial_status_type_id IS DISTINCT FROM ' . CApFinancialStatusType::REJECTED .
						$strConditions . $strPropertyCondition . ' ' . $strApHeaderIdsCondition . '
					GROUP BY
						ah.id,
						ah.cid,
						ah.ap_payee_id,
						ah.ap_payee_term_id,
						ah.lease_customer_id,
						ah.ap_payment_id,
						ah.po_ap_header_ids,
						ah.reversal_ap_header_id,
						ah.refund_ar_transaction_id,
						ah.remote_primary_key,
						ah.transaction_datetime,
						ah.header_memo,
						ah.scheduled_payment_date,
						ah.post_month,
						ah.header_number,
						--ah.is_managerial,
						ah.pay_with_single_check,
						ah.is_on_hold,
						ah.is_temporary,
						ah.is_posted,
						ah.created_on,
						ah.post_date,
						ap.company_name,
						apl.location_name,
						ap.ap_payee_status_type_id
					HAVING
						1 = 1
					ORDER BY
						' . $strOrderBy;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersByApPaymentIdsByCid( $arrintApPaymentIds, $intCid, $objClientDatabase, $boolIsFromUnclaimedproperty = false, $boolIsRequiredInvoiceApHeaders = false ) {

		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strCondition = '';

		if( true == $boolIsFromUnclaimedproperty ) {
			$strCondition = ' AND ah.is_posted = true
								AND ap.is_unclaimed_property = true
								AND ah.ap_header_sub_type_id = ' . \CApHeaderSubType::UNCLAIMED_PROPERTY_PAYMENT . '';
		}

		if( true == $boolIsRequiredInvoiceApHeaders ) {
			$strCondition = ' AND ah.reversal_ap_header_id IS NOT NULL';
		}

		$strSql = 'SELECT
						ah.*,
						ap.payment_number
					FROM
						ap_headers ah
						JOIN ap_payments ap ON ( ap.cid = ah.cid AND ap.id = ah.ap_payment_id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payment_id IN ( ' . sqlIntImplode( $arrintApPaymentIds ) . ' )' . 'AND ah.deleted_by IS NULL
						AND ah.deleted_by IS NULL'
						. $strCondition;

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApHeadersByApBatchIdByCid( $intApBatchId, $intCid, $objClientDatabase, $strBatchMemo ) {

		$strOrderBy = 'ah.id';

		if( false == is_null( $strBatchMemo ) ) {
			$strOrderBy = 'ah.id DESC';
		}

		$strSql = 'SELECT
						DISTINCT ah.*,
						( COALESCE( ap.company_name, \'\' ) || \', \' || COALESCE( apl.location_name, \'\' ) ) AS payee_name,
						ap.ap_payee_status_type_id,
						apl.vendor_code
					FROM
						ap_headers ah
						LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_batch_id = ' . ( int ) $intApBatchId . '
						AND ah.reversal_ap_header_id IS NULL
					ORDER BY ' . $strOrderBy;

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApPaymentDetailsByApHeaderIdsByApPayeeIdByCid( $arrintApHeaderIds, $intApPayeeId, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.id AS charge_ap_header_id,
						ad.id AS charge_ap_detail_id,
						ap.payment_number,
						ap.issue_datetime,
						ap.payment_date
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month AND ah.id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' ) AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
						LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND ( aa.charge_ap_detail_id = ad.id OR aa.credit_ap_detail_id = ad.id) AND aa.is_deleted = false )
						LEFT JOIN ap_details cad ON ( aa.cid = cad.cid AND aa.credit_ap_detail_id = cad.id )
						LEFT JOIN ap_headers cah ON ( cad.cid = cah.cid AND ( cad.ap_header_id = cah.id OR aa.lump_ap_header_id = cah.id ) )
						LEFT JOIN ap_payments ap ON ( cah.cid = ap.cid AND ap.id = cah.ap_payment_id AND ap.payment_status_type_id <> ' . ( int ) CPaymentStatusType::VOIDED . ' AND cah.payment_ap_header_id IS NULL )
					WHERE
						ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.cid = ' . ( int ) $intCid . '
						AND ah.payment_ap_header_id IS NULL
					ORDER BY
						ap.payment_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersByApBatchIdsByCid( $arrintApBatchIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApBatchIds ) ) {
			return NULL;
		}

		return self::fetchApHeaders( 'SELECT * FROM ap_headers WHERE cid = ' . ( int ) $intCid . ' AND ap_batch_id IN ( ' . implode( ',', $arrintApBatchIds ) . ' )', $objClientDatabase );
	}

	public static function fetchApHeaderByApHeaderIdByCid( $intApHeaderId, $intCid, $objClientDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intApHeaderId ) ) {
			return NULL;
		}

		return self::fetchApHeader( 'SELECT * FROM ap_headers WHERE cid = ' . ( int ) $intCid . ' AND reversal_ap_header_id = ' . ( int ) $intApHeaderId, $objClientDatabase );
	}

	public static function fetchApHeadersByHeaderNumberByApPayeeIdOrNotByIdByCid( $strHeaderNumber, $intApPayeeId, $intCid, $objClientDatabase, $intId = NULL, $boolAllowDuplicateInvoiceNumber = false ) {

		if( true == is_null( $intCid ) || true == is_null( $strHeaderNumber ) || true == is_null( $intApPayeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payment_id IS NULL
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND reversal_ap_header_id IS NULL
						AND COALESCE( ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
						AND LOWER( header_number ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strHeaderNumber ) ) . '\'
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_header_type_id = ' . CApHeaderType::INVOICE;

		if( true == valId( $intId ) ) {
			$strSql .= ' AND id <> ' . ( int ) $intId;
		}

		if( false == $boolAllowDuplicateInvoiceNumber ) {
			$strSql .= ' AND ( is_temporary = true OR is_posted = true )';
		}

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchSearchCriteria( $intCid, $objApTransactionsFilter = NULL ) {

		$arrstrAndConditions = [];

		if( false == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {
			return $arrstrAndConditions;
		}

		if( true == valId( $objApTransactionsFilter->getApHeaderId() ) ) {
			$arrstrAndConditions['where_clause'][] = 'ah.id = ' . $objApTransactionsFilter->getApHeaderId();
		}

		if( true == valStr( $objApTransactionsFilter->getHeaderNumber() ) ) {
			$arrstrAndConditions['where_clause'][] = 'ah.header_number ILIKE \'%' . trim( addslashes( $objApTransactionsFilter->getHeaderNumber() ) ) . '%\' ';
		}

		if( true == valArr( $objApTransactionsFilter->getApHeadersIds() ) ) {
			$arrstrAndConditions['where_clause'][] = 'ah.id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' ) ';
		}

		if( true == valStr( $objApTransactionsFilter->getPayeeName() ) ) {

			$strLocationName = ( true == valStr( trim( $objApTransactionsFilter->getLocationName() ) ) ) ? " AND apl.location_name = '" . trim( $objApTransactionsFilter->getLocationName() ) . "'" : '';

			$arrstrAndConditions['where_clause'][] = '(
										( ( ap.company_name ILIKE \'%' . trim( addslashes( $objApTransactionsFilter->getPayeeName() ) ) . '%\' AND ap.ap_payee_type_id <> ' . CApPayeeType::PROPERTY_SOLUTIONS . ' )
											OR apl.vendor_code ILIKE E\'%' . addslashes( trim( $objApTransactionsFilter->getPayeeName() ) ) . '%\'
											OR (	ah.lease_customer_id IS NOT NULL AND ( ah.header_memo ILIKE \'%' . trim( addslashes( $objApTransactionsFilter->getPayeeName() ) ) . '%\'
													OR ah.header_memo ILIKE \'%' . trim( addslashes( str_replace( ', ', '~^~', $objApTransactionsFilter->getPayeeName() ) ) ) . '%\'
												)
											) ) ' . $strLocationName . ')';
		}

		if( true == valArr( $objApTransactionsFilter->getApPayeeStatusTypeIds() ) ) {

			$arrintApPayeeStatusTypeIds = $objApTransactionsFilter->getApPayeeStatusTypeIds();

			if( true == valArr( $arrintApPayeeStatusTypeIds ) ) {
				$arrstrAndConditions['where_clause'][] = 'ap.ap_payee_status_type_id IN ( ' . implode( ',', $arrintApPayeeStatusTypeIds ) . ' )';
			}
		}

		if( true == valId( $objApTransactionsFilter->getCreatedBy() ) ) {
			$arrstrAndConditions['where_clause'][] = 'ah.created_by = ' . $objApTransactionsFilter->getCreatedBy();
		}

		if( true == valArr( $objApTransactionsFilter->getPropertyIds() ) ) {
			$arrstrAndConditions['where_clause'][] = 'ad.property_id IN (	SELECT 
																				pga.property_id 
																			FROM 
																				properties AS p
																				JOIN property_group_associations AS pga ON ( pga.cid = p.cid AND p.id = pga.property_id )
																				JOIN property_groups AS pg ON ( pg.cid = pga.cid AND pga.property_group_id = pg.id )
																			WHERE 
																				p.cid = ' . ( int ) $intCid . '
																				AND pg.id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' )
																				AND pg.deleted_by IS NULL
																				AND pg.deleted_on IS NULL
																	)';
		}

		if( true == valIntArr( $objApTransactionsFilter->getApPayeeLocationIds() ) ) {
			$arrstrAndConditions['where_clause'][] = 'apl.id IN ( ' . sqlIntImplode( $objApTransactionsFilter->getApPayeeLocationIds() ) . ' )';
		}

		if( CApHeaderType::PURCHASE_ORDER == $objApTransactionsFilter->getApHeaderTypeId() ) {

			if( true == valStr( $objApTransactionsFilter->getPoNumber() ) ) {
				$arrstrAndConditions['where_clause'][] = 'ah.header_number ILIKE \'%' . trim( addslashes( $objApTransactionsFilter->getPoNumber() ) ) . '%\' ';
			}

			if( true == valStr( $objApTransactionsFilter->getCreatedOnFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getCreatedOnFrom() ) ) {
				$arrstrAndConditions['where_clause'][] = 'ah.post_date >= \'' . date( 'm/d/Y', strtotime( $objApTransactionsFilter->getCreatedOnFrom() ) ) . '\'';
			}

			if( true == valStr( $objApTransactionsFilter->getCreatedOnTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getCreatedOnTo() ) ) {
				$arrstrAndConditions['where_clause'][] = 'ah.post_date <= \'' . date( 'm/d/Y', strtotime( $objApTransactionsFilter->getCreatedOnTo() ) ) . '\'';
			}

			if( false == is_null( $objApTransactionsFilter->getMaxTransactionAmount() ) ) {
				$arrstrAndConditions['where_clause'][] = 'ah.transaction_amount <= ' . ( float ) $objApTransactionsFilter->getMaxTransactionAmount();
			}

			if( false == is_null( $objApTransactionsFilter->getMinTransactionAmount() ) ) {
				$arrstrAndConditions['where_clause'][] = 'ah.transaction_amount >= ' . ( float ) $objApTransactionsFilter->getMinTransactionAmount();
			}

			if( true == valArr( $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
				$arrstrAndConditions['where_clause'][] .= 'ah.ap_physical_status_type_id IN (' . implode( ',', $objApTransactionsFilter->getApPostStatusTypeIds() ) . ')';
			}

			if( true == valStr( $objApTransactionsFilter->getApPayeeId() ) ) {
				$arrstrAndConditions['where_clause'][] .= 'ah.ap_payee_id = ' . $objApTransactionsFilter->getApPayeeId();
			}

			if( true == valStr( $objApTransactionsFilter->getVendorCode() ) ) {
				$arrstrAndConditions['where_clause'][] .= '(ap.company_name ILIKE \'%' . addslashes( trim( $objApTransactionsFilter->getVendorCode() ) ) . '%\' OR apl.vendor_code ILIKE E\'%' . addslashes( trim( $objApTransactionsFilter->getVendorCode() ) ) . '%\')';
			}
		} else {
			if( CApHeaderType::INVOICE == $objApTransactionsFilter->getApHeaderTypeId() ) {

				if( true == valStr( $objApTransactionsFilter->getPostDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateFrom() ) ) {
					$arrstrAndConditions['where_clause'][] = 'DATE_TRUNC( \'day\', ah.post_date ) >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateFrom() ) ) . ' \'';
				}

				if( true == valStr( $objApTransactionsFilter->getPostDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateTo() ) ) {
					$arrstrAndConditions['where_clause'][] = 'DATE_TRUNC( \'day\', ah.post_date ) <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateTo() ) ) . ' \'';
				}

				if( true == valStr( $objApTransactionsFilter->getDueDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDateFrom() ) ) {
					$arrstrAndConditions['where_clause'][] = 'DATE_TRUNC( \'day\', ah.due_date ) >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDateFrom() ) ) . ' \'';
				}

				if( true == valStr( $objApTransactionsFilter->getDueDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDateTo() ) ) {
					$arrstrAndConditions['where_clause'][] = 'DATE_TRUNC( \'day\', ah.due_date ) <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDateTo() ) ) . ' \'';
				}

				if( true == valStr( $objApTransactionsFilter->getDueDate() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDate() ) ) {
					$arrstrAndConditions['where_clause'][] = 'DATE_TRUNC( \'day\', ah.due_date ) <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDate() ) ) . ' \'';
				}

				if( true == valStr( $objApTransactionsFilter->getInvoicePostMonthFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getInvoicePostMonthFrom() ) ) {
					$arrstrAndConditions['where_clause'][] = 'DATE_TRUNC( \'day\', ah.post_month ) >= \'' . date( 'm-d-Y', strtotime( $objApTransactionsFilter->getInvoicePostMonthFrom() ) ) . ' \'';
				}

				if( true == valStr( $objApTransactionsFilter->getInvoicePostMonthTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getInvoicePostMonthTo() ) ) {
					$arrstrAndConditions['where_clause'][] = 'DATE_TRUNC( \'day\', ah.post_month ) <= \'' . date( 'm-d-Y', strtotime( $objApTransactionsFilter->getInvoicePostMonthTo() ) ) . ' \'';
				}

				if( true == is_numeric( $objApTransactionsFilter->getIsPosted() ) ) {

					$arrstrAndConditions['where_clause'][] = 'ah.is_posted = ' . $objApTransactionsFilter->getIsPosted() . ' ::BOOLEAN';
				} elseif( true == valArr( $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {

					if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
						$arrstrAndConditions['or_clause'][] = '( ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' )';
					}

					if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
						$arrstrAndConditions['or_clause'][] = '( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN AND ah.reversal_ap_header_id IS NULL AND ( ah.ap_financial_status_type_id IS NULL OR ah.ap_financial_status_type_id <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' ) )';
					}

					if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_DELETED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
						$arrstrAndConditions['or_clause'][] = '( ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_DELETED . ' AND ( ah.ap_financial_status_type_id IS NULL OR ah.ap_financial_status_type_id <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' ) )';
					}

					if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
						$arrstrAndConditions['or_clause'][] = '( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN AND ( ah.ap_financial_status_type_id IS NULL OR ah.ap_financial_status_type_id <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' ) )';
					}

					if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_REVERSED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
						$arrstrAndConditions['or_clause'][] = '( ah.reversal_ap_header_id IS NOT NULL AND ah.id < ah.reversal_ap_header_id )';
					}

					if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndConditions['or_clause'] ) ) {
						$arrstrAndConditions['where_clause'][] = ' ( ' . implode( ' OR ', $arrstrAndConditions['or_clause'] ) . ' )';
					}

				} else {
					$arrstrAndConditions['where_clause'][] = '( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN  OR ( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN AND ( ah.ap_financial_status_type_id IS NULL OR ah.ap_financial_status_type_id NOT IN (  ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' , ' . CApFinancialStatusType::DELETED . ') ) ) )';
				}

				if( 1 == $objApTransactionsFilter->getShowReversals() ) {
					$arrstrAndConditions['where_clause'][] = 'ah.reversal_ap_header_id IS NOT NULL AND ah.id < ah.reversal_ap_header_id';
				} elseif( false == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_REVERSED, $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
					$arrstrAndConditions['where_clause'][] = 'ah.reversal_ap_header_id IS NULL';
				}

				if( 1 == $objApTransactionsFilter->getIsInvoicesAttachedToPo() ) {
					$arrstrAndConditions['where_clause'][] = 'ah.po_ap_header_ids IS NOT NULL';
				}

				if( true == is_numeric( $objApTransactionsFilter->getIsInitialImport() ) ) {
					$arrstrAndConditions['where_clause'][] = 'ah.is_initial_import = ' . $objApTransactionsFilter->getIsInitialImport() . '::BOOLEAN';
				}

				if( true == valId( $objApTransactionsFilter->getPoApHeaderIds() ) ) {
					$arrstrAndConditions['where_clause'][] = $objApTransactionsFilter->getPoApHeaderIds() . ' = ANY( ah.po_ap_header_ids )';
				}

				if( true == $objApTransactionsFilter->getHideDisabledProperties() ) {
					$arrstrAndConditions['where_clause'][] = 'ah.id NOT IN (
																		SELECT
																			DISTINCT ad.ap_header_id
																		FROM
																			ap_details ad
																			JOIN properties p ON ( p.id = ad.property_id AND p.cid = ad.cid )
																		WHERE
																			ad.cid = ' . ( int ) $intCid . '
																			AND ad.cid = ah.cid
																			AND ad.ap_header_id = ah.id
																			AND ad.gl_transaction_type_id = ah.gl_transaction_type_id
																			AND ad.post_month = ah.post_month
																			AND ap.cid = ah.cid
																			AND p.is_disabled = 1
																			AND ad.deleted_by IS NULL
																			AND ad.deleted_on IS NULL
																	)';
				}

				if( false == is_null( $objApTransactionsFilter->getApPayeeAccountNumber() ) ) {
					$arrstrAndConditions['where_clause'][] = 'ah.ap_payee_account_id IN ( SELECT UNNEST( array_agg( apa1.id )::integer[] ) FROM ap_payee_accounts apa1 WHERE apa1.account_number ILIKE \'%' . $objApTransactionsFilter->getApPayeeAccountNumber() . '%\' AND apa1.cid = ' . ( int ) $intCid . ' ) ';
				}

				if( false == is_null( $objApTransactionsFilter->getMaxTransactionAmount() ) ) {
					$arrstrAndConditions['where_clause'][] = 'ah.transaction_amount <= ' . ( float ) $objApTransactionsFilter->getMaxTransactionAmount();
				}

				if( false == is_null( $objApTransactionsFilter->getMinTransactionAmount() ) ) {
					$arrstrAndConditions['where_clause'][] = 'ah.transaction_amount >= ' . ( float ) $objApTransactionsFilter->getMinTransactionAmount();
				}

				// Search by payment_post_month
				if( true == valStr( $objApTransactionsFilter->getPaymentPostMonthFrom() ) || true == valStr( $objApTransactionsFilter->getPaymentPostMonthTo() ) ) {
					$arrstrAndConditions['from_clause'][] = 'LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.is_deleted = false AND ad.reversal_ap_detail_id IS NULL AND ( ( ad.transaction_amount > 0 AND ad.id = aa.charge_ap_detail_id ) OR ( ad.transaction_amount <= 0 AND ad.id = aa.credit_ap_detail_id ) ) )';
				}

				if( true == valStr( $objApTransactionsFilter->getPaymentPostMonthFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPaymentPostMonthFrom() ) ) {
					$arrstrAndConditions['having_clause'][] = 'DATE_TRUNC( \'day\', MAX( aa.post_month ) ) >= \'' . date( 'm-d-Y', strtotime( $objApTransactionsFilter->getPaymentPostMonthFrom() ) ) . ' \'';
				}

				if( true == valStr( $objApTransactionsFilter->getPaymentPostMonthTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPaymentPostMonthTo() ) ) {
					$arrstrAndConditions['having_clause'][] = 'DATE_TRUNC( \'day\', MIN( aa.post_month ) ) <= \'' . date( 'm-d-Y', strtotime( $objApTransactionsFilter->getPaymentPostMonthTo() ) ) . ' \'';
				}

				if( true == is_numeric( $objApTransactionsFilter->getIsAttachment() ) && 1 == $objApTransactionsFilter->getIsAttachment() ) {
					$arrstrAndConditions['where_clause'][] = ' fa.id IS NOT NULL';
				} else {
					if( true == is_numeric( $objApTransactionsFilter->getIsAttachment() ) && 0 == $objApTransactionsFilter->getIsAttachment() ) {
						$arrstrAndConditions['where_clause'][] = ' fa.id IS NULL';
					}
				}

				if( true == valArr( $objApTransactionsFilter->getApPaymentStatusTypeIds() ) ) {
					$arrstrPaymentStatusConditions = [
						CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PAID           => ' ah.transaction_amount_due = 0 ',
						CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PARTIALLY_PAID => ' ( ah.transaction_amount_due <> 0 AND ah.transaction_amount_due <> ah.transaction_amount ) ',
						CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNPAID         => ' ah.transaction_amount_due = ah.transaction_amount '
					];

					$arrstrTempPaymentStatusConditions = [];
					foreach( $objApTransactionsFilter->getApPaymentStatusTypeIds() as $intApPaymentStatusTypeId ) {
						$arrstrTempPaymentStatusConditions[] = $arrstrPaymentStatusConditions[$intApPaymentStatusTypeId];
					}

					$arrstrTempPaymentStatusConditions = array_filter( $arrstrTempPaymentStatusConditions );

					if( \Psi\Libraries\UtilFunctions\count( $arrstrPaymentStatusConditions ) != \Psi\Libraries\UtilFunctions\count( $arrstrTempPaymentStatusConditions ) && true == valArr( $arrstrTempPaymentStatusConditions ) ) {
						$arrstrAndConditions['where_clause'][] = ' ( ' . implode( ' OR ', $arrstrTempPaymentStatusConditions ) . ' )';
					}
				}

				/** @TODO MLewis- needs to be used instead I expect */
				if( true == is_numeric( $objApTransactionsFilter->getIsDrawn() ) && 1 == $objApTransactionsFilter->getIsDrawn() ) {
					$arrstrAndConditions['where_clause'][] = 'drd.id IS NOT NULL ';
				} elseif( true == is_numeric( $objApTransactionsFilter->getIsDrawn() ) && 0 == $objApTransactionsFilter->getIsDrawn() ) {
					$arrstrAndConditions['where_clause'][] = 'drd.id IS NULL ';
				}

				if( true == valStr( $objApTransactionsFilter->getPostMonth() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostMonth() ) ) {
					$arrstrAndConditions['where_clause'][] = ' ah.post_month = \'' . addslashes( $objApTransactionsFilter->getPostMonth() ) . '\' ';
				}
			}
		}

		if( true == valId( $objApTransactionsFilter->getApContractId() ) ) {
			$arrstrAndConditions['where_clause'][] = 'ad.ap_contract_id = ' . ( int ) $objApTransactionsFilter->getApContractId();
		}

		return $arrstrAndConditions;
	}

	public static function fetchApHeadersByApPayeeIdsByCid( $arrintApPayeeIds, $intCid, $objClientDatabase, $boolValidateNonDeletedRecord = false ) {
		if( false == valArr( $arrintApPayeeIds ) ) {
			return NULL;
		}

		$strCondition = '';
		if( true == $boolValidateNonDeletedRecord ) {
			// $strCondition = ' AND ap_payment_id IS NULL AND deleted_by IS NULL AND deleted_on IS NULL AND ( is_temporary = true OR is_posted = true ) AND reversal_ap_header_id IS NULL';
			$strCondition = ' AND ap_payment_id IS NULL AND ( is_temporary = true OR is_posted = true ) AND reversal_ap_header_id IS NULL';
		}

		return self::fetchApHeaders( 'SELECT * FROM ap_headers WHERE cid = ' . ( int ) $intCid . $strCondition . ' AND ap_payee_id IN ( ' . implode( ',', $arrintApPayeeIds ) . ' )', $objClientDatabase );
	}

	public static function fetchApHeadersByArTransactionIdsByCid( $arrintArTransactionIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintArTransactionIds ) ) {
			return NULL;
		}

		$strSql = '
			SELECT
				ah.*,
				func_format_refund_customer_names( ah.header_memo ) AS refund_customer_names,
				CASE
					WHEN ah.lease_customer_id IS NULL
					THEN ( COALESCE( ap.company_name, \'\' ) || \' \' || COALESCE( apl.location_name, \'\' ) )
					ELSE func_format_refund_customer_names( ah.header_memo )
				END AS payee_name,
				apt.name AS ap_payee_term,
				SUM ( COALESCE( ad.pre_approval_amount, 0 ) ) AS pre_approval_amount,
				SUM ( COALESCE( ad.transaction_amount, 0 ) ) AS transaction_amount,
				SUM ( COALESCE( ad.transaction_amount_due, 0 ) ) AS transaction_amount_due
			FROM
				ap_headers ah
				JOIN ap_details ad ON ah.cid = ad.cid AND ah.id = ad.ap_header_id
					AND ah.gl_transaction_type_id = ad.gl_transaction_type_id
					AND ah.post_month = ad.post_month
					AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL
				LEFT JOIN ap_payee_terms apt ON ah.ap_payee_term_id = apt.id AND apt.cid = ah.cid
				LEFT JOIN ap_payees ap ON ah.cid = ap.cid AND ah.ap_payee_id = ap.id
				LEFT JOIN ap_payee_locations apl ON apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id
			WHERE
				ah.cid = ' . ( int ) $intCid . '
				AND ah.refund_ar_transaction_id IN ( ' . implode( ',', $arrintArTransactionIds ) . ' )
				AND ah.reversal_ap_header_id IS NULL
			GROUP BY
				ah.id,
				ah.cid,
				ap.company_name,
				apl.location_name,
				payee_name,
				ap_payee_term';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersByArProcessIdByLeaseIdByCid( $intArProcessId, $intLeaseId, $intCid, $objClientDatabase, $boolFetchPaidInvoiceOnly = false ) {

		$strCondition = ( true == $boolFetchPaidInvoiceOnly ) ? ' AND ah.is_posted = true AND ad.transaction_amount_due >= 0 AND ad.transaction_amount <> ad.transaction_amount_due ' : '';

		$strSql = '
			SELECT
				ah.*,
				func_format_refund_customer_names( ah.header_memo ) AS refund_customer_names,
				CASE
					WHEN ah.lease_customer_id IS NULL
					THEN ( COALESCE( ap.company_name, \'\' ) || \' \' || COALESCE( apl.location_name, \'\' ) )
					ELSE func_format_refund_customer_names( ah.header_memo )
				END AS payee_name,
				apt.name AS ap_payee_term,
				SUM ( COALESCE( ad.pre_approval_amount, 0 ) ) AS pre_approval_amount,
				SUM ( COALESCE( ad.transaction_amount, 0 ) ) AS transaction_amount,
				SUM ( COALESCE( ad.transaction_amount_due, 0 ) ) AS transaction_amount_due
			FROM
				ap_headers ah
				JOIN ap_details ad ON ah.cid = ad.cid AND ah.id = ad.ap_header_id
					AND ah.gl_transaction_type_id = ad.gl_transaction_type_id
					AND ah.post_month = ad.post_month
					AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL
				JOIN ar_transactions art ON( art.cid = ah.cid AND art.id = ah.refund_ar_transaction_id )
				LEFT JOIN ap_payee_terms apt ON ( ah.ap_payee_term_id = apt.id AND apt.cid = ah.cid )
				LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
				LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
			WHERE
				ah.cid = ' . ( int ) $intCid . '
				AND art.ar_process_id = ' . ( int ) $intArProcessId . '
				AND art.lease_id = ' . ( int ) $intLeaseId . '
				AND art.is_deleted = FALSE
				AND ah.reversal_ap_header_id IS NULL
				' . $strCondition . '
			GROUP BY
				ah.id,
				ah.cid,
				ap.company_name,
				apl.location_name,
				payee_name,
				ap_payee_term';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersWithPayeeNameByIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.*,
						ap.company_name AS ap_payee_name
					FROM
						ap_headers ah
						JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
					WHERE
						ah.id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
						AND ah.cid = ' . ( int ) $intCid;

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchUnPaidCreditApHeadersByCompanyUserIdByCid( $intCompanyUserId, $intCid, $objClientDatabase, $boolIsAdministrator = false, $arrintPropertyIds = [] ) {

		if( false == valId( $intCompanyUserId ) || false == valIntArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.*,
						ad.pre_approval_amount,
						ad.transaction_amount,
						ad.transaction_amount_due
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.id = ad.ap_header_id AND ad.cid = ah.cid )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ap.cid = ah.cid )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payment_id IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND ah.is_posted = true
						AND ah.is_on_hold = false
						AND NOT ah.is_template
						AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ad.id NOT IN (
											SELECT
												DISTINCT ad.id
											FROM
												ap_details ad
												LEFT JOIN ( ' . CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS vcup ON ( vcup.cid = ad.cid AND vcup.id = ad.property_id AND vcup.company_user_id = ' . ( int ) $intCompanyUserId . ' )
											WHERE
												ad.cid = ' . ( int ) $intCid . '
												AND ( ( vcup.id IS NULL )
														OR
														( vcup.is_disabled = 1 AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL ) )
										)
						AND ad.transaction_amount < 0
						AND ad.transaction_amount_due <> 0
						AND ap.ap_payee_status_type_id = ' . ( int ) CApPayeeStatusType::ACTIVE . '
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchSimpleApHeadersByApHeaderTypeByScheduledApTransactionHeaderIdByCid( $strApHeaderType, $intScheduledApTransactionHeaderId, $intCid, $objClientDatabase ) {

		$strSql = ' SELECT
						ah.id,
						ah.header_number,
						ah.post_date,
						ah.post_month,
						ah.scheduled_ap_header_id,
						SUM( COALESCE ( ad.transaction_amount, 0 ) ) AS transaction_amount
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.scheduled_ap_header_id = ' . ( int ) $intScheduledApTransactionHeaderId . '
						AND ( ah.reversal_ap_header_id IS NULL OR ah.id < ah.reversal_ap_header_id )
						AND ap_header_type_id = ' . $strApHeaderType . '
					GROUP BY
						ah.id,
						ah.header_number,
						ah.post_date,
						ah.post_month,
						ah.scheduled_ap_header_id
					ORDER BY
						ah.id DESC';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchPaginatedApHeadersByDashboardFilterByCidByApTransactionFilter( $objDashboardFilter, $intCid, $objApTransactionsFilter, $intOffset, $intPageLimit, $strSortBy, $objClientDatabase, $arrintInvoiceTypePermissions ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valArr( $arrintInvoiceTypePermissions ) ) {
			return NULL;
		}

		if( true == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {

			$arrstrWhere[] = '
				AND EXISTS (
					SELECT
					FROM
						load_properties( ARRAY[' . ( int ) $intCid . '],ARRAY[' . sqlIntImplode( $objDashboardFilter->getPropertyGroupIds() ) . '], ARRAY[]::INT[], ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'true' : 'false' ) . ' ) AS lp
						JOIN ap_details AS ad_1 ON ad_1.property_id = lp.property_id
					WHERE
						ad_1.cid = ' . ( int ) $intCid . '
						and ad_1.ap_header_id = ah.id
					) ';
		}

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strConditions = NULL;

		$strApHeaderIdsCondition = ( true == valArr( $objApTransactionsFilter->getApHeadersIds() ) ) ? ' AND ah.id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' ) ' : '';

		$arrstrAndConditions['where_clause'][] = 'ah.ap_header_sub_type_id IN( ' . sqlIntImplode( $arrintInvoiceTypePermissions ) . ' )';

		$strApHeaderSubTypeIdsCondition = ( true == valArr( $objApTransactionsFilter->getApHeaderSubTypeIds() ) ) ? ' AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeaderSubTypeIds() ) . ' ) ' : ' ';

		if( true == is_numeric( $objApTransactionsFilter->getIsPosted() ) ) {
			$arrstrAndConditions['where_clause'][] = ( true == $objApTransactionsFilter->getIsPosted() ) ? 'ah.is_posted = true' : ( false == $objApTransactionsFilter->getIsPosted() ) ? ' ah.is_posted = false ' : ' NULL ';
		}

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							ah.id,
							ah.cid,
							ah.header_number,
							ah.created_on,
							ah.due_date As due_date,
							ah.transaction_amount,
							ah.ap_header_sub_type_id,
							CASE
								WHEN ah.lease_customer_id IS NULL THEN
									( COALESCE( ap.company_name, \'\' ) || \' \' || COALESCE( apl.location_name, \'\' ) )
								ELSE func_format_refund_customer_names( ah.header_memo )
							END AS ap_payee_name,
							CASE
								WHEN MAX( ad.property_id ) <> MIN( ad.property_id )
								THEN \'Multiple\'
								ELSE MAX( p.property_name )
							END AS property_name,
							CASE
								WHEN MIN(ad.approved_on) IS NOT NULL THEN
									MIN(ad.approved_on)
								ELSE
									MIN(ad.created_on)
							END AS pending_approval,
							CASE
								WHEN TRIM(dp.approvals_invoices->>\'urgent_invoice_submitted_since\') != \'\' AND CURRENT_DATE >= ( ah.created_on::date + ( dp.approvals_invoices->>\'urgent_invoice_submitted_since\' )::int ) THEN
									3
								WHEN TRIM(dp.approvals_invoices->>\'urgent_invoice_amount_greater_than\') != \'\' AND SUM( ad.transaction_amount ) >= ( dp.approvals_invoices->>\'urgent_invoice_amount_greater_than\' )::float THEN
									3
								WHEN TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.approvals_invoices->>\'urgent_invoice_due_date_within\' ) )
									AND ( CURRENT_DATE - ( TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) ::int ) ) >= ah.due_date::date THEN
									3
								WHEN TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.approvals_invoices->>\'urgent_invoice_due_date_within\' ) )
									AND (
											( ah.due_date::date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) ::int ) )
											OR ( ( \'past\' = TRIM( dp.approvals_invoices->>\'important_invoice_due_date_within\' ) ) AND ( ah.due_date::date BETWEEN ( CURRENT_DATE - TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
											OR ( CURRENT_DATE >= ah.due_date::date )
										) THEN
									3
								WHEN TRIM(dp.approvals_invoices->>\'important_invoice_amount_greater_than\') != \'\' AND SUM( ad.transaction_amount ) >= ( dp.approvals_invoices->>\'important_invoice_amount_greater_than\' )::float THEN
									2
								WHEN TRIM(dp.approvals_invoices->>\'important_invoice_submitted_since\') != \'\' AND CURRENT_DATE >= ( ah.created_on::date + ( dp.approvals_invoices->>\'important_invoice_submitted_since\' )::int ) THEN
									2
								WHEN TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.approvals_invoices->>\'important_invoice_due_date_within\' ) )
									AND ( CURRENT_DATE - ( TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) ::int ) ) >= ah.due_date::date THEN
									2
								WHEN TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.approvals_invoices->>\'important_invoice_due_date_within\' ) )
									AND (
											( ah.due_date::date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) ::int ) )
											OR ( ( \'past\' = TRIM( dp.approvals_invoices->>\'urgent_invoice_due_date_within\' ) ) AND ( ah.due_date::date BETWEEN ( CURRENT_DATE - TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
											OR ( CURRENT_DATE >= ah.due_date::date )
										) THEN
									2
								ELSE
									1
							END as priority
						FROM
							ap_headers ah
							JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
							JOIN properties p ON ( ad.cid = p.cid AND p.id = ad.property_id )
							LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
							LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ah.cid
						WHERE
							ah.is_batching = false
							AND ah.cid = ' . ( int ) $intCid . '
							AND ah.ap_payment_id IS NULL
							AND NOT ah.is_template
							AND COALESCE( ah.ap_financial_status_type_id, 0 ) NOT IN ( ' . CApFinancialStatusType::REJECTED . ', ' . CApFinancialStatusType::DELETED . ' )
							AND ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId() . '
							' . $strApHeaderSubTypeIdsCondition . '
							AND ( ap.ap_payee_status_type_id <> ' . CApPayeeStatusType::INACTIVE . ' OR ah.lease_customer_id IS NOT NULL )
							AND ah.reversal_ap_header_id IS NULL ' .
		          $strConditions . ' ' . $strApHeaderIdsCondition . ' ' . implode( ' ', $arrstrWhere ) . '
						GROUP BY
							ah.id,
							ah.header_number,
							ah.created_on,
							ah.due_date,
							ah.cid,
							ah.post_date,
							ah.lease_customer_id,
							ap.company_name,
							apl.location_name,
							ah.header_memo,
							dp.approvals_invoices->>\'urgent_invoice_submitted_since\',
							dp.approvals_invoices->>\'urgent_invoice_amount_greater_than\',
							dp.approvals_invoices->>\'urgent_invoice_due_date_within\',
							dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\',
							dp.approvals_invoices->>\'important_invoice_amount_greater_than\',
							dp.approvals_invoices->>\'important_invoice_submitted_since\',
							dp.approvals_invoices->>\'important_invoice_due_date_within\',
							dp.approvals_invoices->>\'important_invoice_due_date_within_days\'
					) invoices
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY ' . $strSortBy;

		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchApHeadersCountByDashboardFilterByCidByApTransactionFilter( $objDashboardFilter, $intCid, $objApTransactionsFilter, $objClientDatabase, $arrintInvoiceTypePermissions, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) || false == valArr( $arrintInvoiceTypePermissions ) ) {
			return NULL;
		}

		$strConditions      = NULL;
		$strSqlApHeaderType = '';

		$strApHeaderIdsCondition = ( true == valArr( $objApTransactionsFilter->getApHeadersIds() ) ) ? ' AND ah.id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' ) ' : '';

		$arrstrAndConditions['where_clause'][] = 'ah.ap_header_sub_type_id IN( ' . sqlIntImplode( $arrintInvoiceTypePermissions ) . ' )';

		if( true == is_numeric( $objApTransactionsFilter->getIsPosted() ) ) {
			$arrstrAndConditions['where_clause'][] = 'ah.is_posted = ' . $objApTransactionsFilter->getIsPosted() . ' ::BOOLEAN';
		}

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		if( true == is_numeric( $objApTransactionsFilter->getApHeaderTypeId() ) ) {
			$strSqlApHeaderType = ' AND ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId();
		}

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\'; ' : '';

		$strSql .= '	SELECT
						COUNT(*) AS ' .
		           ( false == $boolIsGroupByProperties ?
			           ' count,
							COALESCE( MAX( invoices.priority ), 1 ) AS priority'
			           :
			           ' approvals_invoices,
							invoices.property_id,
							p.property_name,
							priority' ) . '
					FROM (
						SELECT
							*
						FROM (
							SELECT
								DISTINCT ah.id,
								CASE WHEN TRIM(dp.approvals_invoices->>\'urgent_invoice_submitted_since\') != \'\' AND CURRENT_DATE >= ( ah.created_on::date + ( dp.approvals_invoices->>\'urgent_invoice_submitted_since\' )::int ) THEN
									3
								WHEN TRIM(dp.approvals_invoices->>\'urgent_invoice_amount_greater_than\') != \'\' AND SUM( ad.transaction_amount ) >= ( dp.approvals_invoices->>\'urgent_invoice_amount_greater_than\' )::float THEN
									3
								WHEN TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.approvals_invoices->>\'urgent_invoice_due_date_within\' ) )
									AND ( CURRENT_DATE - ( TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) ::int ) ) >= ah.due_date::date THEN
									3
								WHEN TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.approvals_invoices->>\'urgent_invoice_due_date_within\' ) )
									AND (
											( ah.due_date::date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) ::int ) )
											OR ( ( \'past\' = TRIM( dp.approvals_invoices->>\'important_invoice_due_date_within\' ) ) AND ( ah.due_date::date BETWEEN ( CURRENT_DATE - TRIM ( dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
										) THEN
									3
								WHEN TRIM(dp.approvals_invoices->>\'important_invoice_amount_greater_than\') != \'\' AND SUM( ad.transaction_amount ) >= ( dp.approvals_invoices->>\'important_invoice_amount_greater_than\' )::float THEN
									2
								WHEN TRIM(dp.approvals_invoices->>\'important_invoice_submitted_since\') != \'\' AND CURRENT_DATE >= ( ah.created_on::date + ( dp.approvals_invoices->>\'important_invoice_submitted_since\' )::int ) THEN
									2
								WHEN TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) != \'\' AND ( \'past\' = TRIM( dp.approvals_invoices->>\'important_invoice_due_date_within\' ) )
									AND ( CURRENT_DATE - ( TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) ::int ) ) >= ah.due_date::date THEN
									2
								WHEN TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) != \'\' AND ( \'within\' = TRIM( dp.approvals_invoices->>\'important_invoice_due_date_within\' ) )
									AND (
											( ah.due_date::date BETWEEN CURRENT_DATE AND ( CURRENT_DATE + TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) ::int ) )
											OR ( ( \'past\' = TRIM( dp.approvals_invoices->>\'urgent_invoice_due_date_within\' ) ) AND ( ah.due_date::date BETWEEN ( CURRENT_DATE - TRIM ( dp.approvals_invoices->>\'important_invoice_due_date_within_days\' ) ::int ) AND CURRENT_DATE ) )
										) THEN
									2
								ELSE
									1
								END as priority,
								' . ( true == $boolIsGroupByProperties ? 'ad.property_id,' : '' ) . '
								ah.cid
							FROM
								ap_headers ah
								JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
								JOIN load_properties( ARRAY[' . ( int ) $intCid . ']::INT[], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . ']::INT[], ARRAY[' . CPsProduct::ENTRATA . ']::INT[], ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? 'true' : 'false' ) . '  ) lp ON lp.property_id = ad.property_id
								LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
								LEFT JOIN dashboard_priorities dp ON dp.cid = ah.cid
							WHERE
								ah.cid = ' . ( int ) $intCid . '
								AND ah.is_batching = false
								AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
								AND NOT ah.is_template
								AND ah.ap_payment_id IS NULL ' . $strSqlApHeaderType . '
								AND ( ap.ap_payee_status_type_id <> ' . CApPayeeStatusType::INACTIVE . ' OR ah.lease_customer_id IS NOT NULL )
								AND ah.reversal_ap_header_id IS NULL ' .
		           $strConditions . ' ' . $strApHeaderIdsCondition . '
							GROUP BY
								ah.id,
								ah.created_on,
								dp.approvals_invoices->>\'urgent_invoice_submitted_since\',
								dp.approvals_invoices->>\'urgent_invoice_amount_greater_than\',
								dp.approvals_invoices->>\'urgent_invoice_due_date_within\',
								dp.approvals_invoices->>\'urgent_invoice_due_date_within_days\',
								dp.approvals_invoices->>\'important_invoice_amount_greater_than\',
								dp.approvals_invoices->>\'important_invoice_submitted_since\',
								dp.approvals_invoices->>\'important_invoice_due_date_within\',
								dp.approvals_invoices->>\'important_invoice_due_date_within_days\',
								' . ( true == $boolIsGroupByProperties ? 'ad.property_id,' : '' ) . '
								ah.cid
						) a_invoices
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere . '
						ORDER BY
							priority DESC ' .
		           ( false == $boolIsGroupByProperties ? ' LIMIT 100' : '' ) . '
					) as invoices' .
		           ( true == $boolIsGroupByProperties ? '
						JOIN properties p ON ( p.cid = invoices.cid AND p.id = invoices.property_id )
					WHERE
						invoices.cid = ' . ( int ) $intCid . '
					GROUP BY
						invoices.property_id,
						p.property_name,
						priority'
			           : '' );

		if( true == $boolIsGroupByProperties ) {
			return fetchData( $strSql, $objClientDatabase );
		}
		return fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objClientDatabase );
	}

	public static function fetchApHeaderByApDetailIdByCid( $intApDetailId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApDetailId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.*
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.id = ' . ( int ) $intApDetailId;

		return self::fetchApHeader( $strSql, $objClientDatabase );
	}

	/** @TODO MLewis- this function is only used to get ap_headers then the details get fetched right after. seems very weird */
	public static function fetchApHeadersByApDetailIdsByCid( $arrintApDetailIds, $intCid, $objClientDatabase, $boolIsFromInventory = false ) {

		if( false == valArr( $arrintApDetailIds ) ) {
			return NULL;
		}

		$arrstrAndConditions = [];

		if( true == $boolIsFromInventory ) {

			$arrstrAndConditions[] = 'ad.deleted_by IS NULL';
			$arrstrAndConditions[] = 'ad.deleted_on IS NULL';
			$arrstrAndConditions[] = 'ah.reversal_ap_header_id IS NULL';
			$arrstrAndConditions[] = 'ah.is_batching = false';
			$arrstrAndConditions[] = 'ah.lease_customer_id IS NULL';
			$arrstrAndConditions[] = 'ah.po_ap_header_ids IS NOT NULL';
			$arrstrAndConditions[] = 'ah.scheduled_ap_header_id IS NULL';
		}

		$strSql = 'SELECT
						ah.*
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ad.id IN ( ' . implode( ', ', $arrintApDetailIds ) . ' )';

		if( true == valArr( $arrstrAndConditions ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchPaidApHeaderByIdByCid( $intId, $intCid, $objClientDatabase ) {

		if( false == valId( $intId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.id
					FROM
						ap_headers ah
						JOIN ap_details ad ON( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN ap_allocations aa ON( ad.cid = aa.cid AND ad.id = aa.charge_ap_detail_id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.id = ' . ( int ) $intId . '
						AND aa.is_deleted = false
					LIMIT 1';

		return self::fetchColumn( $strSql, 'id', $objClientDatabase );
	}

	public static function fetchApHeadersAssociatedWithCreditMemosByIdByCid( $arrintOriginalApHeaderIds, $arrintPaymentApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintOriginalApHeaderIds ) || false == valArr( $arrintPaymentApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ah.id
					FROM
						ap_headers ah
						JOIN ap_details ad ON( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN ap_allocations aa ON( ad.cid = aa.cid AND ( ad.id = aa.charge_ap_detail_id OR ad.id = aa.credit_ap_detail_id ) )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.id IN ( ' . implode( ',', $arrintOriginalApHeaderIds ) . ' )
						AND aa.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . '
						AND aa.is_deleted = false
						AND aa.id NOT IN (
											SELECT
												DISTINCT aa.id
											FROM
												ap_headers ah
												JOIN ap_details ad ON( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
												JOIN ap_allocations aa ON( ad.cid = aa.cid AND ad.id = aa.credit_ap_detail_id )
											WHERE
												ah.cid = ' . ( int ) $intCid . '
												AND ah.id IN ( ' . implode( ',', $arrintPaymentApHeaderIds ) . ' )
												AND aa.gl_transaction_type_id = 4
												AND aa.is_deleted = false
										)
					ORDER BY ah.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	/** @TODO MLewis- change usage of fetchSearchCriteria */
	public static function fetchApHeadersCountByPropertyIdsByApTransactionsFilterByCid( $arrintPropertyIds, $objApTransactionsFilter, $intCid, $arrintPoTypePermissions, $objClientDatabase, $boolIsJcProductPermission = false ) {

		$strSqlConditions             = NULL;
		$strJoinConditions			  = NULL;
		$strJcJoinConditions          = NULL;
		$strUninvoicedPoJoinCondition = NULL;
		$arrstrAndConditions          = self::fetchSearchCriteria( $intCid, $objApTransactionsFilter );
		$arrintApHeaderSubTypeIds 	  = ( true == $boolIsJcProductPermission ) ? [ CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::CATALOG_PURCHASE_ORDER, CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER ] : [ CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::CATALOG_PURCHASE_ORDER ];

		if( true == $boolIsJcProductPermission ) {
			$strJcJoinConditions = 'LEFT JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id )';
		}

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strSqlConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		if( true == valStr( $objApTransactionsFilter->getApRoutingTags() ) ) {
			$strSqlConditions .= ' AND ah.ap_routing_tag_id = ' . $objApTransactionsFilter->getApRoutingTags();
		}

		if( true == valArr( $objApTransactionsFilter->getApPostStatusTypeIds() ) ) {
			$strSqlConditions .= ' AND ah.ap_physical_status_type_id IN (' . implode( ',', $objApTransactionsFilter->getApPostStatusTypeIds() ) . ')';
		}

		if( true == valStr( $objApTransactionsFilter->getApPayeeId() ) ) {
			$strSqlConditions .= ' AND ah.ap_payee_id = ' . $objApTransactionsFilter->getApPayeeId();
		}

		if( true == valStr( $objApTransactionsFilter->getVendorCode() ) ) {
			$strSqlConditions .= 'AND (ap.company_name ILIKE \'%' . addslashes( trim( $objApTransactionsFilter->getVendorCode() ) ) . '%\' OR apl.vendor_code ILIKE E\'%' . addslashes( trim( $objApTransactionsFilter->getVendorCode() ) ) . '%\')';
		}

		if( true == valArr( $arrintPoTypePermissions ) ) {
			$strSqlConditions .= ' AND ah.ap_header_sub_type_id IN ( ' . sqlIntImplode( $arrintPoTypePermissions ) . ' )';
		}

		if( true == valIntArr( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
			$strJoinConditions .= ' LEFT JOIN ap_payee_compliance_statuses apcs ON ( ad.cid = apcs.cid AND ah.ap_payee_id = apcs.ap_payee_id AND ad.property_id = apcs.property_id AND apcs.ap_legal_entity_id = apl.ap_legal_entity_id )';
			if( true == in_array( 1, $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
				$strSqlConditions .= ' AND apcs.compliance_status_id IS NULL';
			} elseif( true == in_array( 1, $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) && 1 < \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
				$strSqlConditions .= ' AND ( apcs.compliance_status_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) . ' ) OR apcs.compliance_status_id IS NULL )';
			} else {
				$strSqlConditions .= ' AND apcs.compliance_status_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) . ' )';
			}
		}

		if( true == valArr( $objApTransactionsFilter->getApPoTypeIds() ) ) {
			$arrintPoTypeIds = array_flip( $objApTransactionsFilter->getApPoTypeIds() );
			unset( $arrintPoTypeIds[CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER] );
			if( true == $boolIsJcProductPermission && true == valIntArr( $objApTransactionsFilter->getJobIds() ) && true == in_array( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, $objApTransactionsFilter->getApPoTypeIds() ) ) {
				if( 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPoTypeIds() ) ) {
					$strSqlConditions .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPoTypeIds() ) . ' ) AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) )';
				} else {
					$strSqlConditions .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintPoTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) ) )';
				}
			} else {
				$strSqlConditions .= ' AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPoTypeIds() ) . ' )';
			}
			if( true == $boolIsJcProductPermission && true == in_array( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, $objApTransactionsFilter->getApPoTypeIds() ) && true == valIntArr( $objApTransactionsFilter->getJobCostCodeIds() ) ) {
				if( 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPoTypeIds() ) ) {
					$strSqlConditions .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPoTypeIds() ) . ' ) AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) )';
				} else {
					$strSqlConditions .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintPoTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) ) )';
				}
			}
		}

		$objApTransactionsFilter->setCid( $intCid );
		$arrstrPreparedConditions = self::prepareConditionsForUninvoicedFilter( $objApTransactionsFilter );

		$boolIncludeApFinancialStatusType = $arrstrPreparedConditions['boolIncludeApFinancialStatusType'];
		$strSqlConditions .= $arrstrPreparedConditions['strSqlConditions'];
		$strUninvoicedPoJoinCondition = $arrstrPreparedConditions['strUninvoicedPoJoinCondition'];

		$strTempTableSql = '
			DROP TABLE IF EXISTS invoiced_pos;
			CREATE TEMP TABLE invoiced_pos AS (
				SELECT
					inv_ad.cid,
					inv_ad.po_ap_detail_id AS po_ap_detail_id
				FROM
					ap_headers inv_ah
					JOIN ap_details inv_ad ON ( inv_ad.cid = inv_ah.cid AND inv_ad.ap_header_id = inv_ah.id AND inv_ad.reversal_ap_detail_id IS NULL AND inv_ad.deleted_on IS NULL )
				WHERE
					inv_ah.cid = ' . $objApTransactionsFilter->getCid() . '
					AND inv_ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
					AND inv_ah.deleted_on IS NULL
					AND inv_ad.po_ap_detail_id IS NOT NULL 
				GROUP BY
					inv_ad.cid,
					inv_ad.po_ap_detail_id
			);

			ANALYZE invoiced_pos;
			CREATE INDEX idx_invoiced_pos_master ON invoiced_pos USING btree ( cid, po_ap_detail_id ); ';

		$strSql = $strTempTableSql . PHP_EOL;

		$strSql .= 'SELECT
						COUNT( DISTINCT( ah.id ) )
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						' . $strUninvoicedPoJoinCondition . '
						JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						JOIN ap_payee_locations apl ON ( ah.cid = apl.cid AND ah.ap_payee_id = apl.ap_payee_id AND ah.ap_payee_location_id = apl.id )
						' . $strJcJoinConditions . '
						' . $strJoinConditions . '
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId() . '
						AND ah.is_template = false
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL ' .
		          $strSqlConditions .
		          self::buildFilterStatements( $arrintPropertyIds, $objApTransactionsFilter, true, $boolIncludeApFinancialStatusType );

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	/** @TODO MLewis- what the duece is this?!? Why do we have three static functions that take in a CApTransactionsFilter?!? */
	public static function prepareConditionsForUninvoicedFilter( $objApTransactionsFilter ) {

		$boolIncludeApFinancialStatusType = true;
		$strSqlConditions                 = '';
		$strUninvoicedPoJoinCondition     = '';
		$arrintApFinancialStatusTypes     = [
			CApFinancialStatusType::PENDING,
			CApFinancialStatusType::APPROVED,
			CApFinancialStatusType::PARTIALLY_INVOICED,
			CApFinancialStatusType::CLOSED,
			CApFinancialStatusType::CANCELLED,
			CApFinancialStatusType::REJECTED
		];

		$strUninvoicedPoJoinCondition = ' LEFT JOIN invoiced_pos ip ON ( ip.cid = ah.cid AND ip.po_ap_detail_id = ad.id ) ';

		if( true == $objApTransactionsFilter->getIsUninvoicedPo() ) {

			if( false == in_array( CApFinancialStatusType::CLOSED, $objApTransactionsFilter->getApFinancialStatusTypeIds() )
			    && false == in_array( CApFinancialStatusType::PARTIALLY_INVOICED, $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {

				$strSqlConditions .= '	AND ip.po_ap_detail_id IS NULL ';

			} elseif( true == in_array( CApFinancialStatusType::CLOSED, $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {
				$boolIncludeApFinancialStatusType = false;

				if( false == in_array( CApFinancialStatusType::CANCELLED, $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {
					$strSqlConditions .= ' AND ah.ap_financial_status_type_id <> ' . CApFinancialStatusType::PARTIALLY_INVOICED;
				}

				if( false == in_array( CApFinancialStatusType::CANCELLED, $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {
					$strSqlConditions .= ' AND ah.ap_financial_status_type_id <> ' . CApFinancialStatusType::CANCELLED;
				}
			}

		}

		if( true == in_array( CApFinancialStatusType::CLOSED, $objApTransactionsFilter->getApFinancialStatusTypeIds() ) && false == $objApTransactionsFilter->getIsUninvoicedPo() ) {

			$boolIncludeApFinancialStatusType = false;

			$arrintSelectApFinancialStatusTypes   = array_intersect( $arrintApFinancialStatusTypes, $objApTransactionsFilter->getApFinancialStatusTypeIds() );
			$strSqlConditions .= ' AND ( ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_PURCHASE_ORDER . ' AND ip.po_ap_detail_id IS NOT NULL ) OR ah.ap_financial_status_type_id IN (' . implode( ',', $arrintSelectApFinancialStatusTypes ) . ' ) )';
		}

		if( true == $objApTransactionsFilter->getIsUninvoicedPo()
		    && true == in_array( CApFinancialStatusType::CLOSED, $objApTransactionsFilter->getApFinancialStatusTypeIds() )
		    && false == in_array( CApFinancialStatusType::PARTIALLY_INVOICED, $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {

			$strSqlConditions .= ' AND ah.ap_financial_status_type_id <> ' . CApFinancialStatusType::PARTIALLY_INVOICED;
		}

		if( true == $objApTransactionsFilter->getIsUninvoicedPo()
		    && false == valArr( $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {
			$boolIncludeApFinancialStatusType = false;
			$strSqlConditions .= ' AND ah.ap_financial_status_type_id IN (' . implode( ',', [ CApFinancialStatusType::PENDING, CApFinancialStatusType::APPROVED, CApFinancialStatusType::REJECTED ] ) . ')';
		}

		return [ 'boolIncludeApFinancialStatusType' => $boolIncludeApFinancialStatusType, 'strSqlConditions' => $strSqlConditions, 'strUninvoicedPoJoinCondition' => $strUninvoicedPoJoinCondition ];
	}

	/** @TODO MLewis- change usage of fetchSearchCriteria */
	public static function fetchPaginatedApHeadersByPropertyIdsByApTransactionsFilterByCid( $arrintPropertyIds, $objApTransactionsFilter, $intCid, $objPagination, $arrintPoTypePermissions, $objClientDatabase, $boolIsConfidential = true, $boolIsJcProductPermission = false ) {

		$strOrderBy                   = 'ah.id';
		$strGroupBy                   = NULL;
		$strSqlConditions             = NULL;
		$strJoinConditions			  = NULL;
		$strSortDirection             = 'DESC';
		$strTransactionAmountLabel    = 'ah.transaction_amount';
		$strUninvoicedPoJoinCondition = NULL;
		$strJcJoinConditions          = NULL;

		$arrintApHeaderSubTypeIds = ( true == $boolIsJcProductPermission ) ? [ CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::CATALOG_PURCHASE_ORDER, CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER ] : [ CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::CATALOG_PURCHASE_ORDER ];
		$arrstrSortBy             = [
			'ah.header_number',
			'ah.created_on',
			'ap.company_name',
			'property_name',
			'ah.ap_physical_status_type_id',
			'ah.ap_financial_status_type_id',
			'is_attachment',
			'transaction_amount',
			'apl.vendor_code'
		];

		switch( $objApTransactionsFilter->getSortBy() ) {

			case 'header_number':
				$objApTransactionsFilter->setSortBy( 'ah.header_number' );
				break;

			case 'created_on':
				$objApTransactionsFilter->setSortBy( 'ah.created_on' );
				break;

			case 'company_name':
				$objApTransactionsFilter->setSortBy( 'ap.company_name' );
				break;

			case 'ap_physical_status_type_id':
				$objApTransactionsFilter->setSortBy( 'ah.ap_physical_status_type_id' );
				break;

			case 'ap_financial_status_type_id':
				$objApTransactionsFilter->setSortBy( 'ah.ap_financial_status_type_id' );
				break;

			case 'vendor_code':
				$objApTransactionsFilter->setSortBy( 'apl.vendor_code' );
				break;

			default:
				NULL;
				break;
		}

		if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy )
		    && true == valStr( $objApTransactionsFilter->getSortDirection() ) ) {

			$strSortDirection = $objApTransactionsFilter->getSortDirection();
			$strOrderBy       = addslashes( $objApTransactionsFilter->getSortBy() );
			if( 'ah.header_number' == $strOrderBy ) {
				$strOrderBy = 'ah.header_number::integer';
			}
		}

		if( false == $boolIsConfidential ) {
			$strSqlConditions .= ' AND ad.is_confidential = false';
			$strTransactionAmountLabel = ' SUM( ad.transaction_amount )';
			$strGroupBy                = ' GROUP BY
												ah.id,
												ah.cid,
												ah.header_number,
												ah.ap_payee_id,
												ah.ap_physical_status_type_id,
												ah.ap_financial_status_type_id,
												ah.ap_header_sub_type_id,
												ah.created_on,
												ah.post_date,
												ap.company_name,
												property_name,
												is_attachment,
												apl.vendor_code,
												apl.location_name';
		}

		$arrstrAndConditions = self::fetchSearchCriteria( $intCid, $objApTransactionsFilter );

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strSqlConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		if( true == valStr( $objApTransactionsFilter->getApRoutingTags() ) ) {
			$strSqlConditions .= ' AND ah.ap_routing_tag_id = ' . $objApTransactionsFilter->getApRoutingTags();
		}

		if( true == valArr( $arrintPoTypePermissions ) ) {
			$strSqlConditions .= ' AND ah.ap_header_sub_type_id IN ( ' . sqlIntImplode( $arrintPoTypePermissions ) . ' )';
		}

		if( true == valIntArr( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
			$strJoinConditions .= ' LEFT JOIN ap_payee_compliance_statuses apcs ON ( ad.cid = apcs.cid AND ah.ap_payee_id = apcs.ap_payee_id AND ad.property_id = apcs.property_id AND apcs.ap_legal_entity_id = apl.ap_legal_entity_id )';
			if( true == in_array( 1, $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) && 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
				$strSqlConditions .= ' AND apcs.compliance_status_id IS NULL';
			} elseif( true == in_array( 1, $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) && 1 < \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) ) {
				$strSqlConditions .= ' AND ( apcs.compliance_status_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) . ' ) OR apcs.compliance_status_id IS NULL )';
			} else {
				$strSqlConditions .= ' AND apcs.compliance_status_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPayeeComplianceStatusTypeIds() ) . ' )';
			}
		}

		$arrintApHeaderSubTypeIds = ( true == $boolIsJcProductPermission ) ? [ CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::CATALOG_PURCHASE_ORDER, CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER ] : [ CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::CATALOG_PURCHASE_ORDER ];

		if( true == valArr( $objApTransactionsFilter->getApPoTypeIds() ) ) {
			$arrintPoTypeIds = array_flip( $objApTransactionsFilter->getApPoTypeIds() );
			unset( $arrintPoTypeIds[CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER] );
			if( true == valIntArr( $objApTransactionsFilter->getJobIds() ) && true == in_array( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, $objApTransactionsFilter->getApPoTypeIds() ) && true == $boolIsJcProductPermission ) {
				if( 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPoTypeIds() ) ) {
					$strSqlConditions .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPoTypeIds() ) . ' ) AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) )';
				} else {
					$strSqlConditions .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintPoTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' AND jp.job_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobIds() ) . ' ) ) )';
				}
			} else {
				$strSqlConditions .= ' AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPoTypeIds() ) . ' )';
			}

			if( true == valArr( $objApTransactionsFilter->getJobCostCodeIds() ) && true == in_array( CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, $objApTransactionsFilter->getApPoTypeIds() ) && true == $boolIsJcProductPermission ) {
				if( 1 == \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getApPoTypeIds() ) ) {
					$strSqlConditions .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionsFilter->getApPoTypeIds() ) . ' ) AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) )';
				} else {
					$strSqlConditions .= ' AND ( ah.ap_header_sub_type_id IN ( ' . implode( ',', array_flip( $arrintPoTypeIds ) ) . ' ) OR ( ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) ) )';
				}
			}
		}

		if( true == $boolIsJcProductPermission ) {
			$strJcJoinConditions = 'LEFT JOIN job_phases jp ON ( ad.cid = jp.cid AND ad.job_phase_id = jp.id )';
		}

		$objApTransactionsFilter->setCid( $intCid );
		$arrstrPreparedConditions = self::prepareConditionsForUninvoicedFilter( $objApTransactionsFilter );

		$boolIncludeApFinancialStatusType = $arrstrPreparedConditions['boolIncludeApFinancialStatusType'];
		$strSqlConditions .= $arrstrPreparedConditions['strSqlConditions'];
		$strUninvoicedPoJoinCondition = $arrstrPreparedConditions['strUninvoicedPoJoinCondition'];

		$strTempTableSql = '
			DROP TABLE IF EXISTS invoiced_pos;
			CREATE TEMP TABLE invoiced_pos AS (
				SELECT
				  inv_ad.cid,
				  inv_ad.po_ap_detail_id AS po_ap_detail_id
				FROM
				  ap_headers inv_ah
				  JOIN ap_details inv_ad ON ( inv_ad.cid = inv_ah.cid AND inv_ad.ap_header_id = inv_ah.id AND inv_ad.reversal_ap_detail_id IS NULL AND inv_ad.deleted_on IS NULL )
				WHERE
				  inv_ah.cid = ' . $objApTransactionsFilter->getCid() . '
				  AND inv_ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
				  AND inv_ah.deleted_on IS NULL
				  AND inv_ad.po_ap_detail_id IS NOT NULL 
				GROUP BY
				  inv_ad.cid,
				  inv_ad.po_ap_detail_id
			);

		ANALYZE invoiced_pos;
		CREATE INDEX idx_invoiced_pos_master ON invoiced_pos USING btree ( cid, po_ap_detail_id ); ';

		$strSql = $strTempTableSql . PHP_EOL;

		$strSql .= 'SELECT
						DISTINCT ah.id,
						ah.cid,
						ah.header_number::INTEGER,
						ah.ap_payee_id,
						ah.ap_physical_status_type_id,
						ah.ap_financial_status_type_id,
						ah.ap_header_sub_type_id,
						ah.post_date,
						ah.created_on,
						ah.header_memo,
						ah.external_url,
						' . $strTransactionAmountLabel . ' AS transaction_amount,
						ap.company_name,
						ap.company_name AS payee_name,
						(
							SELECT
								CASE
									WHEN 1 < COUNT ( DISTINCT ad_sub_select.property_id ) THEN \'Multiple\'
									ELSE p.property_name
								END AS property_name
							FROM
								ap_details ad_sub_select
							WHERE
								ad_sub_select.cid = ' . ( int ) $intCid . '
								AND ad_sub_select.cid = ah.cid
								AND ad_sub_select.ap_header_id = ah.id
								AND ad_sub_select.deleted_by IS NULL
								AND ad_sub_select.deleted_on IS NULL
						) AS property_name,
						CASE
							WHEN fa.ap_header_id IS NOT NULL THEN
								1
							ELSE
								0
						END AS is_attachment,
						ah.post_month,
						ah.ap_routing_tag_id,
						ah.bulk_unit_number_id,
						ah.bulk_property_building_id,
						apl.vendor_code,
						apl.location_name
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						' . $strJcJoinConditions . '
						' . $strUninvoicedPoJoinCondition . '
						JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN ap_payees ap ON ( p.cid = ap.cid AND ah.ap_payee_id = ap.id )
						JOIN ap_payee_locations apl ON ( ah.cid = apl.cid AND ah.ap_payee_id = apl.ap_payee_id AND ah.ap_payee_location_id = apl.id )
						LEFT JOIN file_associations fa ON ( ah.cid = fa.cid AND ah.id = fa.ap_header_id AND fa.deleted_by IS NULL
															AND fa.id = ( SELECT MAX( fa1.id ) FROM file_associations fa1 WHERE fa1.cid = fa.cid AND fa1.ap_header_id = fa.ap_header_id ) )
						' . $strJoinConditions . '
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId() . '
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ah.is_template = false
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL ' .
		          $strSqlConditions .
		          self::buildFilterStatements( $arrintPropertyIds, $objApTransactionsFilter, true, $boolIncludeApFinancialStatusType ) .
		          $strGroupBy . '
					ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApHeadersByPropertyIdsByApTransactionFilterByCid( $arrintPropertyIds, $objApTransactionsFilter, $intCid, $objClientDatabase, $boolIsConfidential = true ) {

		$strSqlJoinCondition              = NULL;
		$strSqlConfidentialWhereCondition = NULL;

		if( false == $boolIsConfidential ) {
			$strSqlConfidentialWhereCondition = ' AND ad.is_confidential = false';
		}

		if( true == valStr( $objApTransactionsFilter->getHeaderNumber() ) ) {
			$strSqlJoinCondition = ' LEFT JOIN ap_headers ah1 ON ( ah1.cid = ah.cid AND ah.id = ANY( ah1.po_ap_header_ids ) ) ';
		}

		$strOrderBy   = 'ah.header_number DESC';
		$arrstrSortBy = [ 'po_number', 'post_date', 'company_name', 'property_name', 'po_status_type_id', 'transaction_amount' ];

		if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy )
		    && true == valStr( $objApTransactionsFilter->getSortDirection() ) ) {

			//	$strSortBy = ( 'po_number' == $objApTransactionsFilter->getSortBy() ) ? 'ah.header_number' : $objApTransactionsFilter->getSortBy();

			switch( $objApTransactionsFilter->getSortBy() ) {

				case 'po_number':
					$strSortBy = 'ah.header_number';
					break;

				case 'transaction_amount':
					$strSortBy = 'ah.transaction_amount';
					break;

				default:
					$strSortBy = $objApTransactionsFilter->getSortBy();
					break;
			}

			$strOrderBy = $strSortBy . ' ' . $objApTransactionsFilter->getSortDirection();

		}

		$objApTransactionsFilter->setCid( $intCid );
		$arrstrPreparedConditions = self::prepareConditionsForUninvoicedFilter( $objApTransactionsFilter );

		$boolIncludeApFinancialStatusType = $arrstrPreparedConditions['boolIncludeApFinancialStatusType'];
		$strSqlConditions                 = $arrstrPreparedConditions['strSqlConditions'];
		$strUninvoicedPoJoinCondition     = $arrstrPreparedConditions['strUninvoicedPoJoinCondition'];

		$strTempTableSql = '
			DROP TABLE IF EXISTS invoiced_pos;
			CREATE TEMP TABLE invoiced_pos AS (
			
				SELECT
					inv_ad.cid,
					inv_ad.po_ap_detail_id AS po_ap_detail_id
				FROM
					ap_headers inv_ah
					JOIN ap_details inv_ad ON ( inv_ad.cid = inv_ah.cid AND inv_ad.ap_header_id = inv_ah.id AND inv_ad.reversal_ap_detail_id IS NULL AND inv_ad.deleted_on IS NULL )
				WHERE
					inv_ah.cid = ' . $objApTransactionsFilter->getCid() . '
					AND inv_ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
					AND inv_ah.deleted_on IS NULL
					AND inv_ad.po_ap_detail_id IS NOT NULL 
				GROUP BY
					inv_ad.cid,
					inv_ad.po_ap_detail_id
			);
			
			ANALYZE invoiced_pos;
			CREATE INDEX idx_invoiced_pos_master ON invoiced_pos USING btree ( cid, po_ap_detail_id ); ';

		$strSql = $strTempTableSql . PHP_EOL;

		$strSql .= 'SELECT
						DISTINCT ( ah.* ),
						(
							SELECT
								SUM ( COALESCE( ad.transaction_amount, 0 ) )
							FROM
								ap_details ad
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND ad.ap_header_id = ah.id
								AND ad.cid = ah.cid
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL
								' . $strSqlConfidentialWhereCondition . '
						) AS transaction_amount,
						ap.company_name,
						(
							SELECT
								CASE
									WHEN 1 < COUNT ( DISTINCT ad_sub_select.property_id ) THEN \'Multiple\'
									ELSE p.property_name
								END AS property_name
							FROM
								ap_details ad_sub_select
							WHERE
								ad_sub_select.cid = ' . ( int ) $intCid . '
								AND ad_sub_select.ap_header_id = ah.id
								AND ad_sub_select.cid = ah.cid
						) AS property_name,
						CASE
							WHEN fa.ap_header_id IS NOT NULL THEN
								1
							ELSE
								0
						END AS is_attachment,
						apl.vendor_code
					FROM
						ap_headers ah
						JOIN ap_payee_locations apl ON ( ah.cid = apl.cid AND  ah.ap_payee_location_id = apl.id )
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						' . $strUninvoicedPoJoinCondition . '
						JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						LEFT JOIN file_associations fa ON ( ah.cid = fa.cid AND ah.id = fa.ap_header_id AND fa.deleted_by IS NULL
															AND fa.id = ( SELECT MAX( fa1.id ) FROM file_associations fa1 WHERE fa1.cid = fa.cid AND fa1.ap_header_id = fa.ap_header_id ) )' .
		          $strSqlJoinCondition . '
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId() . '
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL ' .
		          $strSqlConditions .
		          self::buildFilterStatements( $arrintPropertyIds, $objApTransactionsFilter, true, $boolIncludeApFinancialStatusType ) . '
					ORDER BY ' . $strOrderBy;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function buildFilterStatements( $arrintPropertyIds, $objApTransactionsFilter, $boolIncludeProperties = true, $boolIncludeApFinancialStatusType = true ) {
		$arrstrAndConditions = [];

		if( false == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {
			return NULL;
		}

		if( true == valStr( $objApTransactionsFilter->getPoNumber() ) ) {
			$arrstrAndConditions[] = 'ah.header_number ILIKE \'%' . addslashes( trim( $objApTransactionsFilter->getPoNumber() ) ) . '%\' ';
		}

		if( true == $boolIncludeApFinancialStatusType && true == valArr( $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {
			$arrstrAndConditions[] = ' ah.ap_financial_status_type_id IN( ' . implode( ',', $objApTransactionsFilter->getApFinancialStatusTypeIds() ) . ' )';
		}

		if( true == valArr( $objApTransactionsFilter->getApPhysicalStatusTypeIds() ) ) {
			$arrstrAndConditions[] = ' ah.ap_physical_status_type_id IN( ' . implode( ',', $objApTransactionsFilter->getApPhysicalStatusTypeIds() ) . ' )';
		}

		if( true == valStr( $objApTransactionsFilter->getPayeeName() ) ) {
			$arrstrAndConditions[] = '(ap.company_name ILIKE \'%' . addslashes( trim( $objApTransactionsFilter->getPayeeName() ) ) . '%\' OR apl.vendor_code ILIKE E\'%' . addslashes( trim( $objApTransactionsFilter->getPayeeName() ) ) . '%\')';
		}

		if( true == valStr( $objApTransactionsFilter->getHeaderNumber() ) ) {
			$arrstrAndConditions[] = 'ah.header_number ILIKE \'%' . addslashes( trim( $objApTransactionsFilter->getHeaderNumber() ) ) . '%\' ';
		}

		if( true == valStr( $objApTransactionsFilter->getTransactionDatetime() ) && true == CValidation::validateDate( $objApTransactionsFilter->getTransactionDatetime() ) ) {
			$arrstrAndConditions[] = 'ah.transaction_datetime = \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getTransactionDatetime() ) ) . '\'';
		}

		if( true == is_numeric( $objApTransactionsFilter->getCreatedBy() ) ) {
			$arrstrAndConditions[] = 'ah.created_by = ' . ( int ) $objApTransactionsFilter->getCreatedBy();
		}

		if( true == valStr( $objApTransactionsFilter->getCreatedOnFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getCreatedOnFrom() ) ) {
			$arrstrAndConditions[] = 'ah.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getCreatedOnFrom() ) ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getCreatedOnTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getCreatedOnTo() ) ) {
			$arrstrAndConditions[] = 'ah.transaction_datetime <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getCreatedOnTo() ) ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPostDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateFrom() ) ) {
			$arrstrAndConditions[] = 'ah.post_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateFrom() ) ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPostDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateTo() ) ) {
			$arrstrAndConditions[] = 'ah.post_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateTo() ) ) . '\'';
		}

		if( true == valArr( $objApTransactionsFilter->getPropertyIds() ) && true == $boolIncludeProperties ) {
			$arrstrAndConditions[] = 'ad.property_id IN( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' )';
		} else {
			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) {
				$arrstrAndConditions[] = 'ad.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';
			}
		}

		$strConditions = NULL;

		if( true == valArr( $arrstrAndConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		return $strConditions;
	}

	public static function fetchApHeadersByPeriodIdByPropertyIdByCid( $intPeriodId, $intPropertyId, $intCid, $objClientDatabase ) {

		if( false == valId( $intPropertyId ) || false == valId( $intPeriodId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ah.id,
						ah.header_number,
						cl.id AS lease_id,
						cl.primary_customer_id,
						func_format_customer_name( cl.name_first, cl.name_last ) AS customer_name,
						cl.property_id,
						cl.property_name,
						ad.transaction_amount,
						ad.transaction_amount_due
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month )
						JOIN periods p ON ( ad.cid = p.cid AND ad.property_id = p.property_id AND ad.post_month <= p.post_month )
						JOIN ar_allocations aa ON ( ad.cid = aa.cid AND ad.refund_ar_transaction_id = aa.credit_ar_transaction_id )
						JOIN ar_transactions art ON ( aa.cid = art.cid AND art.id = aa.credit_ar_transaction_id AND art.ar_trigger_id =' . CArTrigger::DEPOSIT_INTEREST . ' )
						JOIN property_interest_formulas pif ON ( pif.cid = art.cid AND pif.property_id = art.property_id AND pif.calculate_interest = TRUE AND ( pif.effective_through_date IS NULL OR pif.effective_through_date >= CURRENT_DATE ) )
						JOIN cached_leases cl ON ( art.cid = cl.cid AND art.lease_id = cl.id )					
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND p.id = ' . ( int ) $intPeriodId . '
						AND art.property_id = ' . ( int ) $intPropertyId . '
						AND pif.interest_formula_type_id = ' . CInterestFormulaType::DEPOSIT_INTEREST . '
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND ah.reversal_ap_header_id IS NULL
						AND COALESCE( ah.ap_financial_status_type_id, 0 ) <> ' . CApFinancialStatusType::DELETED . '
						AND ad.transaction_amount_due <> 0
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
					ORDER BY
						customer_name';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersWithTransactionAmountByIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) return NULL;

		$strSql = 'SELECT
						ah.*,
						array_agg( poah.header_number ) AS po_ap_header_numbers
					FROM
						ap_headers ah
						LEFT JOIN ap_headers poah ON ( poah.cid = ah.cid AND poah.id = ANY( ah.po_ap_header_ids ) )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND ah.is_reversed = false
						AND ah.id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
					GROUP BY
						ah.id,
						ah.cid';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchActivePoApHeadersByIdsByCid( $arrintPoApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valIntArr( $arrintPoApHeaderIds ) ) {
			return NULL;
		}

		$strSqlPart = '';

		$strSql = 'SELECT
						ah.*,
						ap.ap_payee_status_type_id,
						p.id as property_id,
						p.property_name,
						ah.ap_routing_tag_id
					FROM
						ap_headers ah
						JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						JOIN ap_details ad ON ( ad.ap_header_id = ah.id AND ad.cid = ah.cid )
						LEFT JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id AND p.is_disabled = 0 )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ah.id IN ( ' . implode( ',', $arrintPoApHeaderIds ) . ' )
						AND ap.ap_payee_status_type_id IN( ' . CApPayeeStatusType::ACTIVE . ', ' . CApPayeeStatusType::LOCKED . ' )
						AND ah.deleted_on IS NULL
						AND ( p.is_disabled = 0 OR p.id IS NULL )';

		$strSql .= ' ORDER BY ah.id';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersCountByApPayeeLocationIdsByCid( $arrintApPayeeLocationIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApPayeeLocationIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ap_payee_location_id,
						count(ap_payee_location_id) as transaction_count
					FROM
						ap_headers
					WHERE
						ap_payee_location_id IN ( ' . implode( ',', $arrintApPayeeLocationIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
					GROUP BY
						ap_payee_location_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApHeaderByIdByApHeaderTypeIdByCid( $intId, $intApHeaderTypeId, $intCid, $objClientDatabase ) {

		if( false == valId( $intId ) || false == valId( $intApHeaderTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.*
					FROM
						ap_headers ah
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.id = ' . ( int ) $intId . '
						AND ah.ap_header_type_id = ' . ( int ) $intApHeaderTypeId;

		return self::fetchApHeader( $strSql, $objClientDatabase );
	}

	public static function fetchApprovedPoApHeadersWithUnreceivedItemsQuantityByAssetIdsByCid( $arrintAssetIds, $intCid, $objClientDatabase, $arrintPropertyIds = [], $arrintApHeaderIds = [] ) {

		if( false == valIntArr( $arrintAssetIds ) ) {
			return NULL;
		}

		$strConditions    = NULL;
		$arrstrConditions = [];

		if( true == valIntArr( $arrintPropertyIds ) ) {
			$arrstrConditions[] = 'ad.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )';
		}

		if( true == valIntArr( $arrintApHeaderIds ) ) {
			$arrstrConditions[] = 'ah.id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )';
		}

		if( true == valArr( $arrstrConditions ) ) {
			$strConditions = ' AND ' . implode( ' AND ', $arrstrConditions );
		}

		$strSql = 'WITH received_items AS (

						SELECT
							at.cid,
							at.po_ap_detail_id,
							SUM ( at.display_quantity ) AS received_quantity
						FROM
							asset_transactions at
							JOIN ap_details ad ON ( at.cid = ad.cid AND at.po_ap_detail_id = ad.id )
							JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ah.ap_header_type_id = ' . ( int ) CApHeaderType::PURCHASE_ORDER . ' )
						WHERE
							at.cid = ' . ( int ) $intCid . '
							AND at.asset_transaction_type_id = ' . ( int ) CAssetTransactionType::RECEIVE_INVENTORY . '
							AND at.asset_transaction_id IS NULL
						GROUP BY
							at.cid,
							at.po_ap_detail_id
				)

				SELECT
					ah.id,
					ah.header_number,
					afst.name AS ap_financial_status,
					apst.name AS ap_physical_status,
					ap.company_name AS ap_payee_name,
					p.property_name,
					p.id AS property_id,
					ad.ap_code_id,
					a.id AS asset_id,
					ad.quantity_ordered - COALESCE ( tri.received_quantity, 0 ) AS quantity
				FROM
					ap_headers ah
					JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					LEFT JOIN received_items tri ON ( ad.cid = tri.cid AND ad.id = tri.po_ap_detail_id )
					JOIN ap_financial_status_types afst ON ( afst.id = ah.ap_financial_status_type_id )
					JOIN ap_physical_status_types apst ON ( apst.id = ah.ap_physical_status_type_id )
					JOIN ap_payees ap ON ( ah.ap_payee_id = ap.id AND ah.cid = ap.cid )
					JOIN properties p ON ( p.cid = ad.cid AND p.id = ad.property_id )
					JOIN assets a ON ( ad.cid = a.cid AND ad.ap_code_id = a.ap_code_id )
				WHERE
					ah.cid = ' . ( int ) $intCid . '
					AND ( ad.quantity_ordered - COALESCE ( tri.received_quantity, 0 ) ) <> 0
					AND ah.approved_by IS NOT NULL
					AND ah.deleted_by IS NULL
					AND a.id IN ( ' . implode( ', ', $arrintAssetIds ) . ' ) '
		          . $strConditions;

		return self::fetchApHeaders( $strSql, $objClientDatabase, false );
	}

	public static function fetchPoApHeaderNumberByApHeaderIdByCid( $intApHeaderId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApHeaderId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT( ah.header_number )
					FROM
						ap_header_logs ahl
						LEFT JOIN ap_headers ah ON ( ah.cid = ahl.cid AND ah.id = ANY ( ahl.po_ap_header_ids ) )
					WHERE
						ahl.cid = ' . ( int ) $intCid . '
						AND ahl.ap_header_id = ' . ( int ) $intApHeaderId . '
						AND ahl.reversal_ap_header_id IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	// This function is written for entrata new dashboard

	public static function fetchPaginatedPendingApHeadersByDashboardFilterByApTransactionsFilterByCid( $objDashboardFilter, $objApTransactionFilter, $intCid, $intOffset, $intLimit, $arrintPoTypePermissions, $strSortBy, $objClientDatabase ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$strPriorityWhere = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';

		$strApHeaderIdsCondition = ( true == valArr( $objApTransactionFilter->getApHeadersIds() ) ) ? ' AND ah.id IN ( ' . implode( ',', $objApTransactionFilter->getApHeadersIds() ) . ' ) ' : ' ';

		$strApHeaderSubTypeIdsCondition = ( true == valArr( $objApTransactionFilter->getApHeaderSubTypeIds() ) ) ? ' AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $objApTransactionFilter->getApHeaderSubTypeIds() ) . ' ) ' : ' ';

		$strSqlConditions = ( true == valArr( $arrintPoTypePermissions ) ) ? ' AND ah.ap_header_sub_type_id IN ( ' . sqlIntImplode( $arrintPoTypePermissions ) . ' )' : '';

		$strMessageStatuses = '\'' . Psi\Libraries\Queue\Message\CMessageStatusMessage::STATUS_PROCESSING . '\'';

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							DISTINCT ( ah.id ),
							ah.cid,
							ah.approved_on,
							ah.header_number,
							ah.ap_header_sub_type_id,
							ah.queue_status,
							ah.created_on,
							(
								SELECT
									CASE
										WHEN MIN( ad.approved_on ) IS NOT NULL THEN  MIN( ad.approved_on )
										ELSE  MIN( ad.created_on )
									END
								FROM
									ap_details ad
								WHERE
									ad.cid = ' . ( int ) $intCid . '
									AND ad.ap_header_id = ah.id
									AND ad.cid = ah.cid
							) AS pending_approval,
							(
								SELECT
									SUM ( COALESCE( ad.transaction_amount, 0::numeric ) )
								FROM
									ap_details ad
								WHERE
									ad.cid = ' . ( int ) $intCid . '
									AND ad.ap_header_id = ah.id
									AND ad.cid = ah.cid
									AND ad.deleted_by IS NULL
							) AS transaction_amount,
							(
								SELECT
									CASE
										WHEN 1 < COUNT ( DISTINCT ad_sub_select.property_id ) THEN \'Multiple\'
										ELSE p.property_name
									END AS property_name
								FROM
									ap_details ad_sub_select
								WHERE
									ad_sub_select.cid = ' . ( int ) $intCid . '
									AND ad_sub_select.ap_header_id = ah.id
									AND ad_sub_select.cid = ah.cid
							) AS property_name,
							ap.company_name AS ap_payee_name,
							CASE
								WHEN TRIM(dp.approvals_purchase_orders->>\'urgent_po_submitted_since\') != \'\' AND CURRENT_DATE >= ( ah.created_on::date + ( dp.approvals_purchase_orders->>\'urgent_po_submitted_since\' )::int ) THEN
									3
								WHEN TRIM(dp.approvals_purchase_orders->>\'urgent_po_amount_greater_than\') != \'\' AND SUM( ad.transaction_amount ) >= ( dp.approvals_purchase_orders->>\'urgent_po_amount_greater_than\' )::float THEN
									3
								WHEN TRIM(dp.approvals_purchase_orders->>\'important_po_amount_greater_than\') != \'\' AND SUM( ad.transaction_amount ) >= ( dp.approvals_purchase_orders->>\'important_po_amount_greater_than\' )::float THEN
									2
								WHEN TRIM(dp.approvals_purchase_orders->>\'important_po_submitted_since\') != \'\' AND CURRENT_DATE >= ( ah.created_on::date + ( dp.approvals_purchase_orders->>\'important_po_submitted_since\' )::int ) THEN
									2
								ELSE
									1
							END as priority
						FROM
							ap_headers ah
							JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
							JOIN load_properties( ARRAY[' . ( int ) $intCid . '], array[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.property_id = ad.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ?  ' AND lp.is_disabled = 0 ' : '' ) . '
							JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
							JOIN ap_payees ap ON ( p.cid = ap.cid AND ah.ap_payee_id = ap.id )
							LEFT JOIN dashboard_priorities dp ON dp.cid = ah.cid
						WHERE
							ah.cid = ' . ( int ) $intCid . '
							AND NOT ah.is_template
							AND ( ah.queue_status IS NULL OR ah.queue_status <> ' . $strMessageStatuses . ' )
							AND ah.ap_header_type_id = ' . $objApTransactionFilter->getApHeaderTypeId() . '
							AND ah.deleted_by IS NULL ' . self::buildFilterStatements( NULL, $objApTransactionFilter, false ) . '
							AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
							' . $strApHeaderSubTypeIdsCondition . '
							' . $strApHeaderIdsCondition . '
							' . $strSqlConditions . '
						GROUP BY
							ah.id,
							ah.approved_on,
							ah.header_number,
							ah.created_on,
							ah.cid,
							p.property_name,
							ap.company_name,
							dp.approvals_purchase_orders->>\'urgent_po_submitted_since\',
							dp.approvals_purchase_orders->>\'urgent_po_amount_greater_than\',
							dp.approvals_purchase_orders->>\'important_po_amount_greater_than\',
							dp.approvals_purchase_orders->>\'important_po_submitted_since\'
					) po
					WHERE
						cid = ' . ( int ) $intCid . '
						' . $strPriorityWhere . '
					ORDER BY ' . $strSortBy . '
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPendingApHeadersCountByDashboardFilterByApTransactionsFilterByCid( $objDashboardFilter, $objApTransactionsFilter, $intCid, $objClientDatabase, $boolIsGroupByProperties = false, $intMaxExecutionTimeOut = 0 ) {

		if( false == valArr( $objDashboardFilter->getPropertyGroupIds() ) ) {
			return NULL;
		}

		$strPriorityWhere        = ( true == valArr( $objDashboardFilter->getTaskPriorities() ) ) ? ' AND priority IN ( ' . implode( ',', $objDashboardFilter->getTaskPriorities() ) . ' )' : '';
		$strApHeaderIdsCondition = ( true == valArr( $objApTransactionsFilter->getApHeadersIds() ) ) ? ' AND ah.id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' ) ' : ' ';

		$strSql = ( 0 != $intMaxExecutionTimeOut ) ? 'SET STATEMENT_TIMEOUT = \'' . ( int ) $intMaxExecutionTimeOut . 's\';' : '';
		$strSql .= 'SELECT
						COUNT(*) ' .
		           ( false == $boolIsGroupByProperties ?
			           ', COALESCE( MAX( priority ), 1 ) AS priority'
			           :
			           ' AS approvals_pos,
							purchase_orders.property_id,
							p.property_name,
							priority' ) . '
					FROM (
						SELECT
							*
						FROM (
							SELECT
								DISTINCT ON( ah.id ) ah.id,
								CASE
									WHEN TRIM(dp.approvals_purchase_orders->>\'urgent_po_submitted_since\') != \'\' AND CURRENT_DATE >= ( ah.created_on::date + ( dp.approvals_purchase_orders->>\'urgent_po_submitted_since\' )::int ) THEN
										3
									WHEN TRIM(dp.approvals_purchase_orders->>\'urgent_po_amount_greater_than\') != \'\' AND SUM( ad.transaction_amount ) >= ( dp.approvals_purchase_orders->>\'urgent_po_amount_greater_than\' )::float THEN
										3
									WHEN TRIM(dp.approvals_purchase_orders->>\'important_po_amount_greater_than\') != \'\' AND SUM( ad.transaction_amount ) >= ( dp.approvals_purchase_orders->>\'important_po_amount_greater_than\' )::float THEN
										2
									WHEN TRIM(dp.approvals_purchase_orders->>\'important_po_submitted_since\') != \'\' AND CURRENT_DATE >= ( ah.created_on::date + ( dp.approvals_purchase_orders->>\'important_po_submitted_since\' )::int ) THEN
										2
									ELSE
										1
								END as priority,
								ad.property_id,
								ah.cid
							FROM
								ap_headers ah
								JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ap.cid = ah.cid )
								JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
								JOIN load_properties( ARRAY[' . ( int ) $intCid . '], ARRAY[' . implode( ',', $objDashboardFilter->getPropertyGroupIds() ) . '] ) lp ON lp.property_id = ad.property_id ' . ( ( false == $objDashboardFilter->getShowDisabledData() ) ? ' AND lp.is_disabled = 0 ' : '' ) . '
								LEFT JOIN dashboard_priorities dp ON dp.cid = ah.cid
							WHERE
								ah.cid = ' . ( int ) $intCid . '
								AND ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId() . '
								AND ah.deleted_by IS NULL ' . self::buildFilterStatements( NULL, $objApTransactionsFilter, false ) . '
								AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
								' . $strApHeaderIdsCondition . '
							GROUP BY
								ah.id,
								ah.created_on,
								dp.approvals_purchase_orders->>\'urgent_po_submitted_since\',
								dp.approvals_purchase_orders->>\'urgent_po_amount_greater_than\',
								dp.approvals_purchase_orders->>\'important_po_amount_greater_than\',
								dp.approvals_purchase_orders->>\'important_po_submitted_since\',
								ad.property_id,
								ah.cid
						) a_po
						WHERE
							cid = ' . ( int ) $intCid . '
							' . $strPriorityWhere . '
						ORDER BY
							priority DESC ' .
		           ( false == $boolIsGroupByProperties ? ' LIMIT 100' : '' ) . '
					) as purchase_orders' .
		           ( true == $boolIsGroupByProperties ? '
					JOIN properties p ON ( p.cid = purchase_orders.cid AND p.id = purchase_orders.property_id )
				WHERE
					purchase_orders.cid = ' . ( int ) $intCid . '
				GROUP BY
					purchase_orders.property_id,
					p.property_name,
					priority'
			           : '' );

		if( true == $boolIsGroupByProperties ) {
			return fetchData( $strSql, $objClientDatabase );
		}
		return fetchOrCacheData( $strSql, $intCacheLifetime = 900, NULL, $objClientDatabase );
	}

	public static function fetchActiveApHeadersByApHeaderTypeByScheduledApTransactionHeaderIdByCid( $strApHeaderType, $intScheduledApTransactionHeaderId, $intCid, $objClientDatabase ) {

		$strSql = ' SELECT
						DISTINCT ( ah.* ),
						ah.header_number po_number,
						( SELECT SUM ( COALESCE( ad.transaction_amount, 0 ) ) FROM ap_details ad WHERE ad.cid = ' . ( int ) $intCid . ' AND ad.ap_header_id = ah.id AND ad.cid = ah.cid ) AS transaction_amount
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN ap_payees ap ON ( ad.cid = ap.cid AND ah.ap_payee_id = ap.id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.scheduled_ap_header_id = ' . ( int ) $intScheduledApTransactionHeaderId . '
						AND ah.ap_header_type_id = ' . $strApHeaderType . '
						AND ah.deleted_by IS NULL
					ORDER BY
						ah.header_number DESC';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersCountByApFinancialStatusTypeIdByCompanyUserIdByPurchaseOrdersFilterByCid( $intApFinancialStatusTypeId, $intCompanyUserId, $objApTransactionsFilter, $intCid, $objClientDatabase, $boolIsAdministrator = false ) {

		$strApHeaderIdsCondition = ( true == valArr( $objApTransactionsFilter->getApHeadersIds() ) ) ? ' AND ah.id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' ) ' : '';

		$arrintPropertyIds = ( 0 < \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getPropertyIds() ) ) ? $objApTransactionsFilter->getPropertyIds() : NULL;

		$strSql = ' SELECT
						COUNT( DISTINCT(ah.id) )
					FROM
						ap_headers ah
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ap.cid = ah.cid ),
						ap_details ad
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.id = ad.ap_header_id
						AND ah.cid = ad.cid
						AND ah.ap_financial_status_type_id = ' . ( int ) $intApFinancialStatusTypeId . '
						AND ah.deleted_by IS NULL ' . self::buildFilterStatements( $arrintPropertyIds, $objApTransactionsFilter, false ) . '
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND ah.id NOT IN (
											SELECT
												DISTINCT ad.ap_header_id
											FROM
												ap_details ad
												LEFT JOIN ( ' . CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS vcup ON( vcup.cid = ad.cid AND vcup.id = ad.property_id  )
											WHERE
												ad.cid = ' . ( int ) $intCid . '
												AND ( vcup.is_disabled = 1 OR vcup.id IS NULL )
										)' .
		          $strApHeaderIdsCondition;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchPaginatedApHeadersByApFinancialStatusTypeIdByCompanyUserIdByCid( $intApFinancialStatusTypeId, $intCompanyUserId, $intCid, $objClientDatabase, $objApTransactionsFilter = NULL, $objPagination = NULL, $boolIsAdministrator = false, $intPeriodId = NULL, $strEndDate = NULL ) {

		if( false == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && false == valId( $intPeriodId ) ) {
			return NULL;
		}

		$strOrderBy              = 'ah.id';
		$strSortDirection        = 'DESC';
		$strClosingCondition     = NULL;
		$strApHeaderIdsCondition = NULL;

		$arrintPropertyIds = [];
		$arrstrSortBy      = [ 'po_number', 'post_date', 'company_name', 'property_name', 'po_status_type_id', 'transaction_amount' ];

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {

			if( true == valStr( $objApTransactionsFilter->getSortBy() ) && true == in_array( $objApTransactionsFilter->getSortBy(), $arrstrSortBy ) && true == valStr( $objApTransactionsFilter->getSortDirection() ) ) {

				$strSortDirection = $objApTransactionsFilter->getSortDirection();
				$strOrderBy       = addslashes( $objApTransactionsFilter->getSortBy() );
			}

			$strApHeaderIdsCondition = ( true == valArr( $objApTransactionsFilter->getApHeadersIds() ) ) ? ' AND ah.id IN ( ' . implode( ',', $objApTransactionsFilter->getApHeadersIds() ) . ' ) ' : '';
			$arrintPropertyIds       = ( 0 < \Psi\Libraries\UtilFunctions\count( $objApTransactionsFilter->getPropertyIds() ) ) ? $objApTransactionsFilter->getPropertyIds() : NULL;
		}

		if( true == valId( $intPeriodId ) ) {

			$strEndDateCondition = 'LEAST( COALESCE( pr.ap_end_date, DATE( pr.post_month + INTERVAL \'1 month\' - INTERVAL \'1 day\' ) ), DATE( NOW() ) )';

			if( true == valStr( $strEndDate ) ) {
				$strEndDateCondition = '\'' . $strEndDate . '\'';
			}

			$strClosingCondition = ' JOIN periods pr ON ( ad.cid = pr.cid AND ad.property_id = pr.property_id AND pr.id = ' . ( int ) $intPeriodId . ' AND ah.post_date <= ' . $strEndDateCondition . ' )';
		}

		$strSql = ' SELECT
						DISTINCT ( ah.id ),
						ah.*,
						( SELECT
								CASE
									WHEN MIN( ad.approved_on ) IS NOT NULL THEN  MIN( ad.approved_on )
									ELSE MIN( ad.created_on )
								END
							FROM
								ap_details ad
							WHERE
								ad.cid = ' . ( int ) $intCid . ' AND ad.ap_header_id = ah.id AND ad.cid = ah.cid
							AND ad.cid = ' . ( int ) $intCid . '
						) AS pending_approval,
						( SELECT
								SUM ( COALESCE( ad.transaction_amount, 0::numeric ) )
							FROM
								ap_details ad
							WHERE
								ad.cid = ' . ( int ) $intCid . ' AND ad.ap_header_id = ah.id AND ad.cid = ah.cid
							AND ad.cid = ' . ( int ) $intCid . '
						) AS transaction_amount,
						( SELECT
								CASE
									WHEN 1 < COUNT ( DISTINCT ad_sub_select.property_id ) THEN \'Multiple\'
									ELSE p.property_name
								END AS property_name
							FROM
								ap_details ad_sub_select
							WHERE
								ad_sub_select.cid = ' . ( int ) $intCid . ' AND ad_sub_select.ap_header_id = ah.id AND ad_sub_select.cid = ah.cid
							AND ad_sub_select.cid = ' . ( int ) $intCid . '
						) AS property_name,
						ap.company_name AS ap_payee_name
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
						JOIN ap_payees ap ON ( p.cid = ap.cid AND ah.ap_payee_id = ap.id ) ' .
		          $strClosingCondition . '
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_financial_status_type_id = ' . ( int ) $intApFinancialStatusTypeId . '
						AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ah.is_template = FALSE
						AND ah.deleted_by IS NULL ' . self::buildFilterStatements( $arrintPropertyIds, $objApTransactionsFilter, false ) . '
						AND ap.ap_payee_status_type_id = ' . CApPayeeStatusType::ACTIVE . '
						AND ah.id NOT IN (
											SELECT
												DISTINCT ad.ap_header_id
											FROM
												ap_details ad
												LEFT JOIN ( ' . CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS vcup ON( vcup.cid = ad.cid AND vcup.id = ad.property_id  )
											WHERE
												ad.cid = ' . ( int ) $intCid . '
												AND ( vcup.is_disabled = 1 OR vcup.id IS NULL )
										) ' .
		          $strApHeaderIdsCondition . '
					ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchHeaderNumbersByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {
		if( false == valArr( $arrintApHeaderIds ) ) return NULL;
		$strSql = 'SELECT id, header_number FROM ap_headers ah WHERE ah.id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' ) AND ah.cid = ' . ( int ) $intCid;
		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchActiveApHeaderByApHeaderNumberByApPayeeIdByCid( $strHeaderNumber, $intApPayeeId, $intCid, $objClientDatabase, $boolIsApCodePresent = false ) {

		$strSqlPart = '';

		if( true == $boolIsApCodePresent ) {

			$strSqlPart = ' JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
							LEFT JOIN asset_transactions at ON ( at.cid = ad.cid AND at.po_ap_detail_id = ad.id AND at.invoice_ap_detail_id IS NULL )';
		}

		$strSql = 'SELECT
						DISTINCT( ah.id ),
						ah.cid,
						ah.ap_payee_id,
						ah.ap_payee_location_id,
						ah.ap_physical_status_type_id,
						ah.ap_financial_status_type_id,
						ah.scheduled_ap_header_id,
						ah.remote_primary_key,
						ah.header_number,
						ah.post_date,
						ah.post_month,
						ah.header_memo,
						ah.approved_by,
						ah.approved_on,
						ah.deleted_by,
						ah.deleted_on,
						ah.updated_by,
						ah.updated_on,
						ah.created_by,
						ah.created_on
					FROM
						ap_headers ah '
		          . $strSqlPart . '
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.id NOT IN (
											SELECT
												DISTINCT ad.ap_header_id
											FROM
												ap_details ad
												JOIN properties p ON ( ad.property_id = p.id AND ad.cid = p.cid )
											WHERE
												ad.cid = ' . ( int ) $intCid . '
												AND ad.ap_header_id = ah.id
												AND ad.cid = ah.cid
												AND ah.header_number =  \'' . addslashes( $strHeaderNumber ) . '\'
												AND p.is_disabled = 1
										)
						AND ah.header_number = \'' . addslashes( $strHeaderNumber ) . '\'
						AND ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.deleted_on IS NULL';

		if( true == $boolIsApCodePresent ) {

			// 1. po_details is not created through catalog item.
			// 2. po_details is created through catalog item but Item Receiving Setting is off so there will not be any records in asset_transactions.
			// 3. po_details is created through catalog item and Item Receiving Setting is on.

			$strSql .= ' AND ( ad.ap_code_id IS NULL
								OR
								( ad.ap_code_id IS NOT NULL AND at.id IS NULL )
								OR
								( ad.ap_code_id IS NOT NULL AND at.asset_transaction_type_id = ' . CAssetTransactionType::RECEIVE_INVENTORY . ' AND at.is_consumed = false )
							 )';

			$arrintPoPhysicalStatusTypeIds = [
				CApPhysicalStatusType::PARTIALLY_RECEIVED,
				CApPhysicalStatusType::RECEIVED
			];

			$strSql .= ' AND ah.ap_financial_status_type_id = ' . CApFinancialStatusType::PARTIALLY_INVOICED;
			$strSql .= ' AND ah.ap_physical_status_type_id IN ( ' . implode( ',', $arrintPoPhysicalStatusTypeIds ) . ' )';
		}

		$strSql .= ' ORDER BY ah.id';

		return self::fetchApHeader( $strSql, $objClientDatabase );
	}

	public static function fetchApHeaderWithTransactionAmountByIdsByCid( $arrintIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.*,
						p.property_name,
						ah.transaction_amount
					FROM
						ap_headers ah
						LEFT JOIN properties p ON ( p.cid = ah.cid AND p.id = ah.bulk_property_id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ah.id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchUnApprovedPoApHeaderByIdsCid( $arrintPoApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPoApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.*
					FROM
						ap_headers ah
					WHERE
						ah.id IN( ' . implode( ',', $arrintPoApHeaderIds ) . ' )
						AND ah.cid=' . ( int ) $intCid . '
						AND ah.approved_by IS NOT NULL';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	/** @TODO Mlewis- These functions do not do anything. Commented out the one place they are called */
//	public static function fetchInvoicesCountByPropertyIdsByApTransactionsFilterByCid( $arrintPropertyIds ) {
//
//		$strSqlCondition    = '';
//		$arrstrSqlCondition = self::fetchSearchCriteriaForReimbursement( $arrintPropertyIds, $objApTransactionsFilter = NULL );
//
//		if( true == valArr( $arrstrSqlCondition ) ) {
//			$strSqlCondition = ' AND ' . implode( ' AND ', $arrstrSqlCondition );
//		}
//	}
//
//	public static function fetchSearchCriteriaForReimbursement( $arrintPropertyIds, $objApTransactionsFilter ) {
//
//		$arrstrSqlCondition = [];
//
//		if( true == valArr( $objApTransactionsFilter->getPropertyIds() ) ) {
//			$arrstrSqlCondition[] = 'a.property_id IN( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' )';
//		} else {
//			if( 0 < count( $arrintPropertyIds ) ) {
//				$arrstrSqlCondition[] = 'a.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )';
//			}
//		}
//
//		return $arrstrSqlCondition;
//	}

	public static function fetchInterCompanyApHeaderByApHeaderIdByCid( $intApHeaderId, $intCid, $objClientDatabase ) {
		if( false == valId( $intApHeaderId ) )return NULL;

		$strSql = 'SELECT
						subQ.*,
						( SELECT ap.company_name FROM ap_payees ap WHERE ap.cid = subQ.cid AND ap.id = COALESCE( subQ.ap_payee_id, 0 ) ) AS company_name,
						( SELECT apl.location_name FROM ap_payee_locations apl WHERE apl.cid = subQ.cid AND apl.ap_payee_id = COALESCE( subQ.ap_payee_id, 0 ) AND apl.is_primary LIMIT 1 ) AS location_name,
						( SELECT apa.account_number FROM ap_payee_accounts apa WHERE apa.cid = subQ.cid AND apa.ap_payee_id = COALESCE( subQ.ap_payee_id, 0 ) AND NOT apa.is_disabled ORDER BY id DESC LIMIT 1 ) AS account_number
					FROM
					( 
						SELECT
							DISTINCT ah.*
						FROM ap_details inter_co_ad
							JOIN ap_detail_reimbursements adr ON ( inter_co_ad.cid = adr.cid AND inter_co_ad.id = adr.reimbursement_ap_detail_id AND adr.is_deleted = false )
							JOIN ap_details ad ON ( adr.cid = ad.cid AND adr.original_ap_detail_id = ad.id )
							JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						WHERE
							inter_co_ad.cid = ' . ( int ) $intCid . '
							AND inter_co_ad.ap_header_id = ' . ( int ) $intApHeaderId . '
							AND ad.reimbursement_method_id NOT IN ( ' . CReimbursementMethod::REIMBURSEMENT_METHOD_USE_SUBLEDGERS_NEW . ', ' . CReimbursementMethod::BILLBACK . ' ) 
						UNION
						SELECT
							DISTINCT reim_ah.*
						FROM
							ap_details ad
							JOIN ap_detail_reimbursements adr ON ( ad.cid = adr.cid AND ad.id = adr.original_ap_detail_id AND adr.is_deleted = false )
							JOIN ap_details reim_ad ON ( adr.cid = reim_ad.cid AND reim_ad.id = adr.reimbursement_ap_detail_id )
							JOIN ap_headers reim_ah ON ( reim_ad.cid = reim_ah.cid AND reim_ad.ap_header_id = reim_ah.id )
						WHERE
							ad.cid = ' . ( int ) $intCid . '
							AND ad.ap_header_id = ' . ( int ) $intApHeaderId . '
							AND reim_ad.reimbursement_method_id NOT IN ( ' . CReimbursementMethod::REIMBURSEMENT_METHOD_USE_SUBLEDGERS_NEW . ', ' . CReimbursementMethod::BILLBACK . ' )  
						LIMIT 1
					) as subQ';

		return self::fetchApHeader( $strSql, $objClientDatabase );
	}

	public static function fetchInterCompanyApHeadersByApHeaderIdsByCid( $arrintApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ah.*
					FROM ap_details inter_co_ad
						JOIN ap_detail_reimbursements adr ON ( inter_co_ad.cid = adr.cid AND inter_co_ad.id = adr.reimbursement_ap_detail_id AND adr.is_deleted = false )
						JOIN ap_details ad ON ( adr.cid = ad.cid AND adr.original_ap_detail_id = ad.id )
						JOIN ap_headers ah ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
					WHERE
						inter_co_ad.cid = ' . ( int ) $intCid . '
						AND inter_co_ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' )
					UNION
					SELECT
						DISTINCT reim_ah.*
					FROM
						ap_details ad
						JOIN ap_detail_reimbursements adr ON ( ad.cid = adr.cid AND ad.id = adr.original_ap_detail_id AND adr.is_deleted = false )
						JOIN ap_details reim_ad ON ( adr.cid = reim_ad.cid AND reim_ad.id = adr.reimbursement_ap_detail_id )
						JOIN ap_headers reim_ah ON ( reim_ad.cid = reim_ah.cid AND reim_ad.ap_header_id = reim_ah.id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.ap_header_id IN ( ' . implode( ',', $arrintApHeaderIds ) . ' ) ';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersByCidByPropertyIdByBlobTypeIdBySearchKeywords( $intCid, $arrmixFastLookup, $arrstrFilteredExplodedSearch, $objDatabase, $arrintInvoiceTypePermission ) {

		if( false == valArr( $arrintInvoiceTypePermission ) ) return NULL;

		$strSql                         = '';
		$arrstrAdvancedSearchParameters = [];

		$objApTransactionsFilter = new CApTransactionsFilter();
		$arrstrInvoiceValues     = [];

		if( false == is_null( $arrmixFastLookup['invoices_property_ids'] ) ) {
			$arrstrInvoiceValues['invoice_property_ids'] = explode( ',', $arrmixFastLookup['invoices_property_ids'] );
		}
		if( false == is_null( $arrmixFastLookup['post_date_from'] ) ) {
			$arrstrInvoiceValues['post_date_from'] = $arrmixFastLookup['post_date_from'];
		}
		if( false == is_null( $arrmixFastLookup['post_date_to'] ) ) {
			$arrstrInvoiceValues['post_date_to'] = $arrmixFastLookup['post_date_to'];
		}
		if( false == is_null( $arrmixFastLookup['due_date_from'] ) ) {
			$arrstrInvoiceValues['due_date_from'] = $arrmixFastLookup['due_date_from'];
		}
		if( false == is_null( $arrmixFastLookup['due_date_to'] ) ) {
			$arrstrInvoiceValues['due_date_to'] = $arrmixFastLookup['due_date_to'];
		}
		if( false == is_null( $arrmixFastLookup['invoice_post_month_from'] ) ) {
			$arrstrInvoiceValues['invoice_post_month_from'] = $arrmixFastLookup['invoice_post_month_from'];
		}
		if( false == is_null( $arrmixFastLookup['invoice_post_month_to'] ) ) {
			$arrstrInvoiceValues['invoice_post_month_to'] = $arrmixFastLookup['invoice_post_month_to'];
		}
		if( false == is_null( $arrmixFastLookup['payment_post_month_from'] ) ) {
			$arrstrInvoiceValues['payment_post_month_from'] = $arrmixFastLookup['payment_post_month_from'];
		}
		if( false == is_null( $arrmixFastLookup['payment_post_month_to'] ) ) {
			$arrstrInvoiceValues['payment_post_month_to'] = $arrmixFastLookup['payment_post_month_to'];
		}
		if( false == is_null( $arrmixFastLookup['transaction_location_ids'] ) ) {
			$arrstrInvoiceValues['location_name'] = explode( ',', $arrmixFastLookup['transaction_location_ids'] );
		}
		if( false == is_null( $arrmixFastLookup['vendor_ap_payee_id'] ) ) {
			$arrstrInvoiceValues['vendor_ap_payee_id'] = $arrmixFastLookup['vendor_ap_payee_id'];
		}
		if( true == isset( $arrmixFastLookup['invoices_post_status_ids'] ) ) {
			$arrstrInvoiceValues['is_posted'] = explode( ',', $arrmixFastLookup['invoices_post_status_ids'] );
		}

		if( true == isset( $arrmixFastLookup['invoices_post_status_ids'] ) ) {
			$arrstrInvoiceValues['is_posted'] = explode( ',', $arrmixFastLookup['invoices_post_status_ids'] );
		}

		if( true == isset( $arrstrInvoiceValues['post_date_from'] ) ) {
			$objApTransactionsFilter->setPostDateFrom( $arrstrInvoiceValues['post_date_from'] );
		}
		if( true == isset( $arrstrInvoiceValues['post_date_to'] ) ) {
			$objApTransactionsFilter->setPostDateTo( $arrstrInvoiceValues['post_date_to'] );
		}
		if( true == isset( $arrstrInvoiceValues['due_date_from'] ) ) {
			$objApTransactionsFilter->setDueDateFrom( $arrstrInvoiceValues['due_date_from'] );
		}
		if( true == isset( $arrstrInvoiceValues['due_date_to'] ) ) {
			$objApTransactionsFilter->setDueDateTo( $arrstrInvoiceValues['due_date_to'] );
		}
		if( true == isset( $arrstrInvoiceValues['invoice_post_month_from'] ) ) {
			$objApTransactionsFilter->setInvoicePostMonthFrom( $arrstrInvoiceValues['invoice_post_month_from'] );
		}
		if( true == isset( $arrstrInvoiceValues['invoice_post_month_to'] ) ) {
			$objApTransactionsFilter->setInvoicePostMonthTo( $arrstrInvoiceValues['invoice_post_month_to'] );
		}
		if( true == isset( $arrstrInvoiceValues['payment_post_month_from'] ) ) {
			$objApTransactionsFilter->setPaymentPostMonthFrom( $arrstrInvoiceValues['payment_post_month_from'] );
		}
		if( true == isset( $arrstrInvoiceValues['payment_post_month_to'] ) ) {
			$objApTransactionsFilter->setPaymentPostMonthTo( $arrstrInvoiceValues['payment_post_month_to'] );
		}
		if( true == isset( $arrstrInvoiceValues['location_name'] ) && 'on' != trim( $arrstrInvoiceValues['location_name'][0] ) ) {
			$objApTransactionsFilter->setApPayeeLocationIds( $arrstrInvoiceValues['location_name'] );
		}
		if( true == isset( $arrstrInvoiceValues['invoice_property_ids'] ) && 'on' != trim( $arrstrInvoiceValues['invoice_property_ids'][0] ) ) {
			$objApTransactionsFilter->setPropertyIds( $arrstrInvoiceValues['invoice_property_ids'] );
		}

		$strConditions       = '';
		$arrstrAndConditions = [];
		$strOrderBy          = 'ap_header_id DESC';

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) ) {

			$objApTransactionsFilter->setApHeaderTypeId( CApHeaderType::INVOICE );

			if( true == valArr( $objApTransactionsFilter->getPropertyIds() ) ) {
				$arrstrAndConditions[] = 'ad.property_id IN ( ' . implode( ',', $objApTransactionsFilter->getPropertyIds() ) . ' ) ';
			}
		}

		if( true == valArr( array_filter( $objApTransactionsFilter->getApPayeeLocationIds() ) ) ) {
			$arrstrAndConditions[] = 'apl.id IN ( ' . implode( ',', array_filter( $objApTransactionsFilter->getApPayeeLocationIds() ) ) . ' )';
		}

		if( true == valId( $objApTransactionsFilter->getApHeaderTypeId() ) ) {
			$arrstrAndConditions[] = 'ah.ap_header_type_id = ' . $objApTransactionsFilter->getApHeaderTypeId();
		}

		if( true == valStr( $objApTransactionsFilter->getPostDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateFrom() ) ) {
			$arrstrAndConditions[] = 'ah.post_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateFrom() ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPostDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPostDateTo() ) ) {
			$arrstrAndConditions[] = 'ah.post_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getPostDateTo() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objApTransactionsFilter->getDueDateFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDateFrom() ) ) {
			$arrstrAndConditions[] = 'ah.due_date >= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDateFrom() ) ) . ' 00:00:00\'';
		}

		if( true == valStr( $objApTransactionsFilter->getDueDateTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getDueDateTo() ) ) {
			$arrstrAndConditions[] = 'ah.due_date <= \'' . date( 'Y-m-d', strtotime( $objApTransactionsFilter->getDueDateTo() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objApTransactionsFilter->getInvoicePostMonthFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getInvoicePostMonthFrom() ) ) {
			$arrstrAndConditions[] = 'ah.post_month >= \'' . addslashes( $objApTransactionsFilter->getInvoicePostMonthFrom() ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getInvoicePostMonthTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getInvoicePostMonthTo() ) ) {
			$arrstrAndConditions[] = 'ah.post_month <= \'' . addslashes( $objApTransactionsFilter->getInvoicePostMonthTo() ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPaymentPostMonthFrom() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPaymentPostMonthFrom() ) ) {
			$arrstrAndConditions[] = 'aa.post_month >= \'' . addslashes( $objApTransactionsFilter->getPaymentPostMonthFrom() ) . '\'';
		}

		if( true == valStr( $objApTransactionsFilter->getPaymentPostMonthTo() ) && true == CValidation::validateDate( $objApTransactionsFilter->getPaymentPostMonthTo() ) ) {
			$arrstrAndConditions[] = 'aa.post_month <= \'' . addslashes( $objApTransactionsFilter->getPaymentPostMonthTo() ) . '\'';
		}

		if( true == isset( $arrstrInvoiceValues['vendor_ap_payee_id'] ) && false == empty( $arrstrInvoiceValues['vendor_ap_payee_id'] ) ) {
			$arrstrAndConditions[] = 'ah.ap_payee_id = ' . ( int ) $arrstrInvoiceValues['vendor_ap_payee_id'];
		}

		if( true == isset( $arrstrInvoiceValues['is_posted'] ) ) {

			if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED, $arrstrInvoiceValues['is_posted'] ) ) {
				$arrstrConditions['or_clause'][] = '( ah.ap_financial_status_type_id = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' )';
			}

			if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED, $arrstrInvoiceValues['is_posted'] ) ) {
				$arrstrConditions['or_clause'][] = '( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_POSTED . ' ::BOOLEAN AND ( ah.ap_financial_status_type_id IS NULL OR ah.ap_financial_status_type_id <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' ) )';
			}

			if( true == in_array( CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED, $arrstrInvoiceValues['is_posted'] ) ) {
				$arrstrConditions['or_clause'][] = '( ah.is_posted = ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_NOT_POSTED . ' ::BOOLEAN AND ( ah.ap_financial_status_type_id IS NULL OR ah.ap_financial_status_type_id <> ' . CApTransactionsFilter::AP_POST_STATUS_TYPE_REJECTED . ' ) )';
			}

			if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrConditions['or_clause'] ) ) {
				$arrstrAndConditions[] = ' ( ' . implode( ' OR ', $arrstrConditions['or_clause'] ) . ' ) ';
			}
		}

		foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
			if( '' != $strKeywordAdvancedSearch ) {
				array_push( $arrstrAdvancedSearchParameters, 'ah.header_number ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
			}
		}

		if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
			$arrstrAndConditions[] = ' ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
		}

		$arrstrAndConditions[] = 'ah.ap_header_sub_type_id IN( ' . sqlIntImplode( $arrintInvoiceTypePermission ) . ' )';

		if( true == valArr( $arrstrAndConditions ) ) {
			$strConditions .= ' AND ' . implode( ' AND ', $arrstrAndConditions );
		}

		$strSql = 'SELECT
						ah.id AS ap_header_id,
						ah.header_number,
						ah.post_date,
						ah.due_date,
						apl.location_name,
						ap.company_name,
						CASE
							WHEN MAX( ad.property_id ) <> MIN( ad.property_id )
							THEN \'Multiple\'
							ELSE MAX( p.property_name )
						END AS property_name,
						apl.id AS ap_payee_location_id,
						(
							SELECT
								COALESCE( SUM ( ad.transaction_amount ), 0 )
							FROM
								ap_details ad
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND ah.cid = ad.cid
								AND ah.id = ad.ap_header_id
								AND ah.gl_transaction_type_id = ad.gl_transaction_type_id
								AND ah.post_month = ad.post_month
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL
						) AS transaction_amount,
						( COALESCE( ad2.transaction_amount_paid, 0 ) ) AS transaction_amount_paid
					FROM
						ap_headers ah
						LEFT JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.gl_transaction_type_id = ad.gl_transaction_type_id AND ah.post_month = ad.post_month AND ad.deleted_by IS NULL AND ad.deleted_on IS NULL )
						LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND ( aa.charge_ap_detail_id = ad.id OR aa.credit_ap_detail_id = ad.id ) AND aa.is_deleted = false )
						LEFT JOIN
						(
							SELECT
								ap_header_id,
								cid,
								SUM ( paid_amount ) AS transaction_amount_paid
							FROM
							(
								SELECT
									ad.ap_header_id,
									ad.cid,
									CASE
										WHEN ad.transaction_amount >= 0::numeric THEN ABS ( COALESCE ( SUM ( aa.allocation_amount ), 0::NUMERIC ) )
										ELSE COALESCE ( SUM ( aa.allocation_amount ), 0::NUMERIC )
									END AS paid_amount
								FROM
									ap_details ad
									JOIN ap_allocations aa ON ( aa.cid = ad.cid AND ( aa.charge_ap_detail_id = ad.id OR aa.credit_ap_detail_id = ad.id ) AND aa.is_deleted = false )
								WHERE
									ad.cid = ' . ( int ) $intCid . '
									AND aa.cid = ' . ( int ) $intCid . '
								GROUP BY
									ad.ap_header_id,
									ad.cid,
									ad.transaction_amount
							) aa2
							GROUP BY
								ap_header_id,
								cid
						) ad2 ON ( ad2.cid = ah.cid AND ah.id = ad2.ap_header_id )
						LEFT JOIN properties p ON ( ad.cid = p.cid AND p.id = ad.property_id )
						LEFT JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
					WHERE
						ah.is_batching = false
						AND ah.cid = ' . ( int ) $intCid . '
						AND ah.reversal_ap_header_id IS NULL
						AND NOT ah.is_template
						AND ah.ap_payment_id IS NULL ' . $strConditions . '
					GROUP BY
						ah.id,
						ah.cid,
						ah.header_number,
						ah.post_date,
						ah.due_date,
						ah.post_month,
						apl.id,
						apl.location_name,
						transaction_amount_paid,
						ap.company_name
					ORDER BY ' . $strOrderBy;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApHeadersByCidByPropertyIdBySearchKeywordsByPoType( $intCid, $arrmixFastLookup, $arrstrFilteredExplodedSearch, $intIsStandardPo,$intIsCatalogPo, $objDatabase ) {

		$strLookUpType = $arrmixFastLookup['lookup_type'];

		if( CApHeader::LOOKUP_TYPE_PURCHASE_ORDERS_CREATED_BY == $strLookUpType ) {

			$arrstrAdvancedSearchParameters = [];

			$strSql = 'SELECT
							cu.id,
							cu.cid,
							cu.company_employee_id,
							COALESCE( ce.name_first, NULL ) AS name_first,
							COALESCE( ce.name_last, NULL ) AS name_last,
							cu.username,
							cu.is_administrator,
							cu.is_disabled
						FROM
							company_users cu
							LEFT OUTER JOIN company_employees ce ON ( ce.id = cu.company_employee_id AND ( ce.cid = cu.cid AND cu.default_company_user_id IS NULL ) )
						WHERE
							cu.cid = ' . ( int ) $intCid . ' 
							AND cu.company_user_type_id IN ( ' . implode( ', ', CCompanyUserType::$c_arrintEntrataAndPsiUserTypeIds ) . ' )
							AND cu.default_company_user_id IS NULL';

			foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
				if( '' != $strKeywordAdvancedSearch ) {
					array_push( $arrstrAdvancedSearchParameters, 'ce.name_first  ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'ce.name_last ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					array_push( $arrstrAdvancedSearchParameters, 'cu.username ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
				}
			}

			if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
				$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
			}

			$strSql .= ' ORDER BY LOWER( ce.name_first || ce.name_last ) ASC';

		} else {
			if( ( CApHeader::LOOKUP_TYPE_PURCHASE_ORDERS_HEADER_NUMBER == $strLookUpType )
			    && true == valArr( $arrstrFilteredExplodedSearch ) ) {

				$arrstrAdvancedSearchParameters = [];

				foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
					if( '' != $strKeywordAdvancedSearch ) {
						array_push( $arrstrAdvancedSearchParameters, 'ah.header_number ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
					}
				}

				$strSqlPart = '';

				if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
					$strSqlPart .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
				}

				$strSql = 'SELECT
						ah.header_number,
						ah.po_ap_header_ids[1] AS id
					FROM
						ap_headers ah
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.po_ap_header_ids[1] IS NOT NULL'
				          . $strSqlPart . '
					 ORDER BY ah.header_number DESC ';

			} else {

				if( false == $intIsStandardPo && false == $intIsCatalogPo ) {
					return;
				}

				$intHiddenApPayeeId       = NULL;
				$strPoNumber              = '';
				$strPayeeName             = '';
				$strPoCreatedBy           = '';
				$strPostDate              = '';
				$strPostDateTo            = '';
				$strPostDateFrom          = '';
				$arrintPoPropertyIds      = [];
				$arrintPoStatusTypeIds    = [];
				$arrintVendorLocationIds  = [];
				$arrintApHeaderSubTypeIds = [ CApHeaderSubType::STANDARD_PURCHASE_ORDER, CApHeaderSubType::CATALOG_PURCHASE_ORDER ];
				$intPropertyId            = $arrmixFastLookup['property_id'];

				if( false == $intIsStandardPo ) {

					$arrintApHeaderSubTypeIds = array_flip( $arrintApHeaderSubTypeIds );
					unset( $arrintApHeaderSubTypeIds[CApHeaderSubType::STANDARD_PURCHASE_ORDER] );
					$arrintApHeaderSubTypeIds = array_flip( $arrintApHeaderSubTypeIds );
				}

				if( false == $intIsCatalogPo ) {

					$arrintApHeaderSubTypeIds = array_flip( $arrintApHeaderSubTypeIds );
					unset( $arrintApHeaderSubTypeIds[CApHeaderSubType::CATALOG_PURCHASE_ORDER] );
					$arrintApHeaderSubTypeIds = array_flip( $arrintApHeaderSubTypeIds );
				}

				$arrintApPhysicalStatusTypeIds  = [ CApPhysicalStatusType::NOT_ORDERED, CApPhysicalStatusType::BACK_ORDERED, CApPhysicalStatusType::ORDERED, CApPhysicalStatusType::PARTIALLY_RECEIVED, CApPhysicalStatusType::RECEIVED, CApPhysicalStatusType::RETURNED, CApPhysicalStatusType::CANCELLED, CApPhysicalStatusType::ON_HOLD ];
				$arrintApFinancialStatusTypeIds = [ CApFinancialStatusType::PENDING, CApFinancialStatusType::APPROVED, CApFinancialStatusType::CANCELLED, CApFinancialStatusType::CLOSED, CApFinancialStatusType::PARTIALLY_INVOICED ];

				$strSql = ' SELECT
						DISTINCT( ah.* ),
						( SELECT
								SUM( COALESCE( ad.transaction_amount, 0::numeric ) )
							FROM
								ap_details ad
							WHERE
								ad.ap_header_id = ah.id
								AND ad.cid = ah.cid
								AND ad.cid = ' . ( int ) $intCid . '
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL
						) AS transaction_amount,
						ap.company_name,
						apl.phone_number,
						apl.street_line1,
						apl.street_line2,
						apl.city,
						apl.state_code,
						apl.postal_code,
						apl.location_name,
						CASE
							WHEN fa.ap_header_id IS NOT NULL THEN
								1
							ELSE
								0
						END AS is_attachment
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id AND apl.ap_payee_id = ap.id )
						LEFT JOIN file_associations fa ON ( ah.cid = fa.cid AND ah.id = fa.ap_header_id AND fa.deleted_by IS NULL
															AND fa.id = ( SELECT MAX( id ) FROM file_associations WHERE cid = fa.cid AND ap_header_id = fa.ap_header_id ) )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )
						AND ah.ap_financial_status_type_id IN ( ' . implode( ',', $arrintApFinancialStatusTypeIds ) . ' )
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL ';

				if( CApHeader::LOOKUP_TYPE_VENDOR_PURCHASE_ORDERS_PO_NUMBER == $strLookUpType ) {

					if( false == is_null( $arrmixFastLookup['vendor_po_status_type_ids'] ) ) {
						$arrintPoStatusTypeIds = explode( ',', $arrmixFastLookup['vendor_po_status_type_ids'] );
					}

					if( false == is_null( $arrmixFastLookup['vendor_po_property_id'] ) ) {
						$arrintPoPropertyIds = array_filter( explode( ',', $arrmixFastLookup['vendor_po_property_id'] ) );
					}

					if( false == is_null( $arrmixFastLookup['vendor_ap_payee_id'] ) ) {
						$intHiddenApPayeeId = $arrmixFastLookup['vendor_ap_payee_id'];
					}

					if( false == is_null( $arrmixFastLookup['po_header_date_from'] ) ) {
						$strPostDateFrom = $arrmixFastLookup['po_header_date_from'];
					}

					if( false == is_null( $arrmixFastLookup['po_header_date_to'] ) ) {
						$strPostDateTo = $arrmixFastLookup['po_header_date_to'];
					}

					if( false == is_null( $arrmixFastLookup['vendor_locations'] ) ) {
						$arrintVendorLocationIds = explode( ',', $arrmixFastLookup['vendor_locations'] );
					}

					if( false == is_null( $arrmixFastLookup['vendor_po_created_by'] ) ) {
						$strPoCreatedBy = $arrmixFastLookup['vendor_po_created_by'];
					}

					if( true == valArr( array_filter( $arrintVendorLocationIds ) ) && 'on' != $arrintVendorLocationIds[0] ) {
						$strSql .= ' AND ah.ap_payee_location_id IN ( ' . implode( ',', array_filter( $arrintVendorLocationIds ) ) . ' )';
					}

					if( true == is_numeric( $intHiddenApPayeeId ) ) {
						$strSql .= ' AND ah.ap_payee_id = ' . ( int ) $intHiddenApPayeeId;
					}

					if( !empty( $strPostDateFrom ) && 'From' != trim( $strPostDateFrom ) && true == CValidation::validateDate( $strPostDateFrom ) ) {
						$strSql .= ' AND ah.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $strPostDateFrom ) ) . '\'';
					}

					if( !empty( $strPostDateTo ) && 'To' != trim( $strPostDateTo ) && true == CValidation::validateDate( $strPostDateTo ) ) {
						$strSql .= ' AND ah.transaction_datetime <= \'' . date( 'Y-m-d', strtotime( $strPostDateTo ) ) . '\'';
					}
				} else {

					if( false == is_null( $arrmixFastLookup['ap_financial_status_type_id'] ) ) {
						$arrintApFinancialStatusTypeIds = explode( ',', $arrmixFastLookup['ap_financial_status_type_id'] );
					}

					if( false == is_null( $arrmixFastLookup['ap_physical_status_type_ids'] ) ) {
						$arrintApPhysicalStatusTypeIds = explode( ',', $arrmixFastLookup['ap_physical_status_type_ids'] );
					}

					if( false == is_null( $arrmixFastLookup['is_bulk_receive'] )
					    && true == $arrmixFastLookup['is_bulk_receive'] ) {
						$arrintPoStatusTypeIds = [
							CApFinancialStatusType::APPROVED,
							CApFinancialStatusType::PARTIALLY_INVOICED,
							CApPhysicalStatusType::BACK_ORDERED,
							CApPhysicalStatusType::ON_HOLD,
							CApPhysicalStatusType::ORDERED,
							CApPhysicalStatusType::PARTIALLY_RECEIVED,
						];
					}

					if( false == is_null( $arrmixFastLookup['po_property_id'] ) ) {
						$arrintPoPropertyIds = array_filter( explode( ',', $arrmixFastLookup['po_property_id'] ) );
					}

					if( false == is_null( $arrmixFastLookup['po_header_date'] ) ) {
						$strPostDate = $arrmixFastLookup['po_header_date'];
					}

					if( false == is_null( $arrmixFastLookup['header_number'] ) ) {
						$strPoNumber = $arrmixFastLookup['header_number'];
					}

					if( false == is_null( $arrmixFastLookup['payee_name'] ) ) {
						$strPayeeName = $arrmixFastLookup['payee_name'];
					}

					if( false == is_null( $arrmixFastLookup['po_created_by'] ) ) {
						$strPoCreatedBy = $arrmixFastLookup['po_created_by'];
					}

					if( !empty( $strPostDate ) && true == CValidation::validateDate( $strPostDate ) ) {
						$strSql .= ' AND ah.transaction_datetime = \'' . date( 'Y-m-d', strtotime( $strPostDate ) ) . '\'';
					}

					if( !empty( $strPostDateFrom ) && 'From' != trim( $strPostDateFrom ) && true == CValidation::validateDate( $strPostDateFrom ) ) {
						$strSql .= ' AND ah.transaction_datetime >= \'' . date( 'Y-m-d', strtotime( $strPostDateFrom ) ) . '\'';
					}

					if( !empty( $strPostDateTo ) && 'To' != trim( $strPostDateTo ) && true == CValidation::validateDate( $strPostDateTo ) ) {
						$strSql .= ' AND ah.transaction_datetime <= \'' . date( 'Y-m-d', strtotime( $strPostDateTo ) ) . '\'';
					}
				}

				if( true == valArr( $arrintApFinancialStatusTypeIds ) && 'on' != $arrintApFinancialStatusTypeIds[0] ) {
					$strSql .= ' AND ah.ap_financial_status_type_id IN ( ' . implode( ',', $arrintApFinancialStatusTypeIds ) . ' )';
				}

				if( true == valArr( $arrintApPhysicalStatusTypeIds ) && 'on' != $arrintApPhysicalStatusTypeIds[0] ) {
					$strSql .= ' AND ah.ap_physical_status_type_id IN ( ' . implode( ',', $arrintApPhysicalStatusTypeIds ) . ' )';
				}

				if( true == valArr( $arrintPoPropertyIds ) && 'on' != $arrintPoPropertyIds[0] ) {
					$strSql .= ' AND ad.property_id IN ( ' . implode( ',', $arrintPoPropertyIds ) . ' )';
				}

				if( !empty( $strPoCreatedBy ) ) {
					$strSql .= ' AND ah.created_by = ' . ( int ) $strPoCreatedBy;
				}

				if( CApHeader::LOOKUP_TYPE_PURCHASE_ORDERS_VENDOR == $strLookUpType ) {

					$arrstrAdvancedSearchParameters = [];
					if( '' != $strPoNumber ) {
						$strSql .= ' AND ah.header_number ILIKE \'%' . addslashes( trim( $strPoNumber ) ) . '%\' ';
					}

					foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
						if( '' != $strKeywordAdvancedSearch ) {
							array_push( $arrstrAdvancedSearchParameters, 'ap.company_name ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
							array_push( $arrstrAdvancedSearchParameters, 'apl.vendor_code ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
						}
					}
				} else {

					$arrstrAdvancedSearchParameters = [];

					foreach( $arrstrFilteredExplodedSearch as $strKeywordAdvancedSearch ) {
						if( '' != $strKeywordAdvancedSearch ) {
							array_push( $arrstrAdvancedSearchParameters, 'ah.header_number  ILIKE E\'%' . addslashes( trim( $strKeywordAdvancedSearch ) ) . '%\'' );
						}
					}
				}

				if( true == isset( $arrstrAdvancedSearchParameters ) && true == valArr( $arrstrAdvancedSearchParameters ) ) {
					$strSql .= ' AND ( ' . implode( ' OR ', $arrstrAdvancedSearchParameters ) . ' )';
				}

				$strSql .= ' ORDER BY ah.id DESC';
			}
		}

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchApHeadersByCidBySearchKeywords( $intCid, $strFilteredExplodedSearch, $objDatabase ) {

		$strSqlPart = '';

		if( true == valStr( $strFilteredExplodedSearch ) ) {
			$strSqlPart = ' AND ah.header_number ILIKE E\'%' . addslashes( trim( $strFilteredExplodedSearch ) ) . '%\'';
		}

		$strSql = 'SELECT
						DISTINCT ah.id,
						ah.header_number AS po_header_number
					FROM
						ap_headers ah
						JOIN ap_headers ah_invoice ON ( ah.cid = ah_invoice.cid AND ah.id = ANY( ah_invoice.po_ap_header_ids ) AND ah_invoice.ap_header_type_id = ' . CApHeaderType::INVOICE . ' )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.deleted_on IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER
		          . $strSqlPart . '
					ORDER BY ah.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPoApHeadersByHeaderNumberByApPayeeIdByCid( $intApPayeeId, $intCid, $objClientDatabase ) {

		$strSql = ' SELECT
						DISTINCT ah.id as id,
						ah.header_number as number
					FROM
						ap_headers ah
						INNER JOIN ap_details ad ON ( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ah.ap_financial_status_type_id = ' . CApFinancialStatusType::APPROVED . '
						AND ah.deleted_by IS NULL
						AND ah.deleted_by IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApHeaderByHeaderNumbersOrByIdsByCid( $arrstrHeaderNumbers, $arrintTransactionIds, $intCid, $objClientDatabase ) {

		$strWhereCondition = '';
		if( true == valIntArr( $arrintTransactionIds ) ) {
			$strWhereCondition = ( true == valIntArr( $arrintTransactionIds ) ) ? ' AND ah.id IN ( ' . implode( ',', $arrintTransactionIds ) . ' )' : '';
			$strWhereCondition .= ( true == valArr( $arrstrHeaderNumbers ) ) ? ' OR ( ah.header_number LIKE \'%' . implode( '%\' OR ah.header_number LIKE \'%', $arrstrHeaderNumbers ) . '%\' )' : '';
		} else {
			$strWhereCondition .= ( true == valArr( $arrstrHeaderNumbers ) ) ? ' AND ( ah.header_number LIKE \'%' . implode( '%\' OR ah.header_number LIKE \'%', $arrstrHeaderNumbers ) . '%\' )' : '';
		}

		$strSql = ' SELECT
						ah.*
					FROM
						ap_headers ah
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND reversal_ap_header_id IS NULL
						AND NOT ah.is_template
						AND ah.deleted_by IS NULL
						AND ah.deleted_by IS NULL
						' . $strWhereCondition;

		return self::fetchApHeaders( $strSql, $objClientDatabase, false );
	}

	public static function fetchLastInvoiceApHeaderByApPayeeIdByApPayeeLocationIdByApHeaderSubTypeIdByApPayeeAccountIdByCid( $intApPayeeId, $intApPayeeLocationId, $intApHeaderSubTypeId, $intApPayeeAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						MAX( ah.id ) as ap_header_id
					FROM
						ap_headers ah
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . ( int ) CApHeaderType::INVOICE . '
						AND ah.ap_header_sub_type_id = ' . ( int ) $intApHeaderSubTypeId . '
						AND ah.gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . '
						AND ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND ah.ap_payee_account_id = ' . ( int ) $intApPayeeAccountId . '
						AND NOT ah.is_reversed
						AND ah.reversal_ap_header_id IS NULL
						AND NOT ah.is_deleted
						AND ah.deleted_on IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchReversePaymentApHeadersByApPaymentIdsByCid( $arrintApPaymentIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_posted = false
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND reversal_ap_header_id IS NOT NULL
						AND ap_header_sub_type_id <> ' . CApHeaderSubType::UNCLAIMED_PROPERTY_PAYMENT . '
						AND gl_transaction_type_id = ' . CGlTransactionType::AP_PAYMENT . '
						AND ap_payment_id IN ( ' . implode( ',', $arrintApPaymentIds ) . ' )
					ORDER BY
						id DESC';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchJobBudgetApHeadersByJobPhaseIdsByApHeaderSubTypeIdsByCid( $arrintJobPhaseIds, $arrintApHeaderSubTypeIds, $intCid, $objDatabase, $strOrderByField = NULL, $boolIsJobBudgetOnly = false, $boolIsUnitLocationBudgetOnly = false, $intUnitTypeId = NULL, $intMaintenanceLocationId = NULL ) {

		$strOrderBy = '';
		$strWhereCondition = '';

		if( false == valArr( $arrintJobPhaseIds ) || false == valArr( $arrintApHeaderSubTypeIds ) ) {
			return NULL;
		}

		if( true == valStr( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ah.' . $strOrderByField;
		}

		if( true == $boolIsJobBudgetOnly ) {
			$strWhereCondition .= ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id IS NULL';
		} elseif( true == $boolIsUnitLocationBudgetOnly ) {
			$strWhereCondition .= ' AND ( ad.unit_type_id IS NOT NULL OR ad.maintenance_location_id IS NOT NULL )';
		}

		if( true == valId( $intUnitTypeId ) ) {
			$strWhereCondition .= ' AND ad.unit_type_id = ' . ( int ) $intUnitTypeId . ' AND ad.maintenance_location_id IS NULL';
		} elseif( true == valId( $intMaintenanceLocationId ) ) {
			$strWhereCondition .= ' AND ad.unit_type_id IS NULL AND ad.maintenance_location_id = ' . ( int ) $intMaintenanceLocationId;
		}

		$strSql = 'SELECT
						ah.*,
						ad.unit_type_id,
						ad.maintenance_location_id
					FROM
						ap_headers ah
						LEFT JOIN ap_details ad ON( ah.cid = ad.cid AND ad.ap_header_id = ah.id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.job_phase_id IN ( ' . implode( ',', $arrintJobPhaseIds ) . ' )
						' . $strWhereCondition . '
						AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )
						AND ( ( ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ' ) AND ah.approved_on IS NOT NULL ) OR ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET . ' )
						AND ah.deleted_by IS NULL' . $strOrderBy . '
					GROUP BY
						ah.id,
						ah.cid,
						ad.unit_type_id,
						ad.maintenance_location_id';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPoApHeadersByJobIdByCid( $intJobId, $intCid, $objDatabase, $objApTransactionsFilter = NULL, $objPagination = NULL ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$arrstrAndConditions = ( array ) self::fetchSearchCriteria( $intCid, $objApTransactionsFilter );

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objApTransactionsFilter->getApPhysicalStatusTypeIds() ) ) {
			$arrstrAndConditions['or_clause'][] = ' ah.ap_physical_status_type_id IN ( ' . sqlIntImplode( $objApTransactionsFilter->getApPhysicalStatusTypeIds() ) . ' )';
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndConditions['or_clause'] ) ) {
			$arrstrAndConditions['where_clause'][] = ' ( ' . implode( ' OR ', $arrstrAndConditions['or_clause'] ) . ' )';
		}

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strWhereConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		if( true == valArr( $objApTransactionsFilter->getJobCostCodeIds() ) ) {
			$strWhereConditions .= ' AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) ';
		}

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {
			$strWhereConditions .= ' AND ah.ap_financial_status_type_id IN( ' . sqlIntImplode( $objApTransactionsFilter->getApFinancialStatusTypeIds() ) . ' )';
		}

		$strSql = 'SELECT
						ah.id,
						ah.header_number,
						ah.created_on,
						ah.ap_financial_status_type_id,
						SUM( COALESCE( ad.transaction_amount, 0 ) ) AS transaction_amount,
						apst.name AS ap_physical_status_type_name,
						ap.company_name,
						(
							SELECT
							  array_to_string ( array_agg ( DISTINCT ac.name order by ac.name DESC ), \'","\' )
							FROM
								ap_details ad
								JOIN ap_contracts ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_contract_id )
							WHERE
								ad.ap_header_id = ah.id
								AND ad.cid = ah.cid
								AND ad.ap_contract_id IS NOT NULL
								AND ad.deleted_on IS NULL
								AND ac.job_id = ' . ( int ) $intJobId . '
							GROUP BY
								ad.ap_header_id,
								ad.cid
						) AS ap_contract_name
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ad.ap_header_id = ah.id AND ad.cid = ah.cid )
						JOIN job_phases jp ON ( jp.id = ad.job_phase_id AND jp.cid = ad.cid )
						JOIN jobs j ON ( j.id = jp.job_id AND j.cid = jp.cid )
						LEFT JOIN ap_physical_status_types apst ON ( apst.id = ah.ap_physical_status_type_id )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ap.cid = ah.cid )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id AND apl.ap_payee_id = ap.id )
					WHERE
						j.id = ' . ( int ) $intJobId . '
						AND j.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ah.ap_header_sub_type_id IN( ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ', ' . CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER . ' )
						AND ah.is_template IS FALSE
						AND ah.deleted_on IS NULL
						AND ad.deleted_on IS NULL'
						. $strWhereConditions . '
					GROUP BY 
						ah.id,
						ah.cid,
						apst.name,
						ap.company_name
					ORDER BY
						ah.header_number::INT DESC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedApHeadersByContractIdByCid( $intCid, $intContractId, $objClientDatabase, $objPagination = NULL ) {

		if( false == valId( $intContractId ) || false == valId( $intCid ) ) return NULL;

		$arrintApHeaderSubTypeIds		= [ CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER ];

		$strSql = ' SELECT
						DISTINCT ah.*,
						SUM ( COALESCE( ad.transaction_amount, 0 ) ) AS transaction_amount,
						CASE
							WHEN ah.lease_customer_id IS NULL THEN ( COALESCE ( ap.company_name, \'\' ) || \' \' || COALESCE ( apl.location_name, \'\' ) )
							ELSE func_format_refund_customer_names ( ah.header_memo )
						END AS ap_payee_name,
						j.name as job_name
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
						JOIN ap_contracts ac ON ( ac.id = ad.ap_contract_id AND ac.cid = ad.cid )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ah.cid = ap.cid )
						JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						LEFT JOIN job_phases jp ON ( jp.id = ad.job_phase_id AND jp.cid = ad.cid )
						LEFT JOIN jobs j ON ( j.id = jp.job_id AND j.cid = jp.cid )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ac.id = ' . ( int ) $intContractId . '
						AND ah.ap_header_type_id = ' . ( int ) CApHeaderType::PURCHASE_ORDER . '
						AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )
						AND ah.is_template IS FALSE
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
					GROUP BY
						ah.id,
						ah.cid,
						ah.header_number,
						ah.ap_payee_id,
						ah.ap_physical_status_type_id,
						ah.ap_financial_status_type_id,
						ah.created_on,
						ap.company_name,
						j.id,
						j.name,
						ad.ap_contract_id,
						apl.location_name
					ORDER BY
						ah.created_on DESC,
						ah.header_number DESC';

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return CApHeaders::fetchApHeaders( $strSql, $objClientDatabase );

	}

	public static function fetchPaginatedJobsApHeadersByJobIdByCid( $intJobId, $intCid, $objClientDatabase, $objApTransactionsFilter = NULL, $objPagination = NULL ) {

		$strWhereConditions = '';

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$arrstrAndConditions = ( array ) self::fetchSearchCriteria( $intCid, $objApTransactionsFilter );

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strWhereConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		if( true == valArr( $objApTransactionsFilter->getJobCostCodeIds() ) ) {
			$strWhereConditions .= ' AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) ';
		}

		$strSql = 'SELECT
						DISTINCT ah.*,
						CASE WHEN ah.lease_customer_id IS NULL THEN ( COALESCE( ap.company_name, \'\' ) || \', \' || COALESCE( apl.location_name, \'\' ) )
							ELSE func_format_refund_customer_names( ah.header_memo )
						END AS ap_payee_name,
						ah.is_posted::INTEGER,
						j.name AS job_name,
						(
							SELECT
								array_remove( array_agg(DISTINCT ac.name order by ac.name DESC ), NULL )
							FROM
								ap_details ad
								JOIN ap_contracts ac ON ( ac.cid = ad.cid AND ac.id = ad.ap_contract_id )
							WHERE
								ad.ap_header_id = ah.id
								AND ad.cid = ah.cid
								AND ad.ap_contract_id IS NOT NULL
								AND ad.deleted_on IS NULL
								AND ac.job_id = ' . ( int ) $intJobId . '
							GROUP BY 
								ad.ap_header_id,
								ad.cid        
						) AS contract_names,
						CASE
							WHEN fa.ap_header_id IS NOT NULL THEN
								1
							ELSE
								0
						END AS is_attachment,
						CASE
							WHEN drd.id IS NOT NULL THEN
								1
							ELSE
								0
						END AS is_drawn
					FROM
						ap_headers ah
						JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
						JOIN jobs j ON ( j.cid = jp.cid AND j.id = jp.job_id )
						LEFT JOIN draw_request_details drd ON ( drd.cid = ad.cid AND drd.ap_detail_id = ad.id )
						LEFT JOIN file_associations fa ON ( ah.cid = fa.cid AND ah.id = fa.ap_header_id AND fa.deleted_by IS NULL AND fa.id =
							(
								SELECT
									MAX ( fa1.id )
								FROM
									file_associations fa1
								WHERE
									fa1.cid = fa.cid
									AND fa1.ap_header_id = fa.ap_header_id
							))
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND j.id = ' . ( int ) $intJobId . '
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND ah.ap_header_sub_type_id IN  ( ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ',' . CApHeaderSubType::CATALOG_JOB_INVOICE . ' )
						AND ah.deleted_on IS NULL
						AND	ad.deleted_on IS NULL
						AND ah.is_template IS FALSE
						' . $strWhereConditions . '
					ORDER BY
						ah.id DESC';
		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return fetchData( $strSql, $objClientDatabase );

	}

	public static function fetchApHeadersCountByJobPhaseIdsByApHeaderSubTypeIdsByCid( $arrintJobPhaseIds, $arrintApHeaderSubTypeIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintJobPhaseIds ) || false == valArr( $arrintApHeaderSubTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COUNT( DISTINCT( ah.id ) )
				FROM
					ap_headers ah
					JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
	 			WHERE
	 				ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' ) AND
	 				ad.job_phase_id IN( ' . implode( ',', $arrintJobPhaseIds ) . ' ) AND
	 				ah.cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchPoInvoiceJECountByJobPhaseIdsByCid( $arrintJobPhaseIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintJobPhaseIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						jp.id AS job_phase_id,
						( COUNT( DISTINCT( ah.id ) ) + COUNT( DISTINCT( gd.id ) ) ) AS ap_header_count
				FROM
					job_phases jp
					LEFT JOIN ap_details ad ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
					LEFT JOIN ap_headers ah ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id AND ah.ap_header_sub_type_id IN( ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ',' . CApHeaderSubType::CATALOG_JOB_INVOICE . ',' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ',' . CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER . ',' . CApHeaderSubType::JOB_PHASE_BUDGET . ',' . CApHeaderSubType::CONTRACT_CHANGE_ORDER . ',' . CApHeaderSubType::JOB_CHANGE_ORDER . ',' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ') )
					LEFT JOIN gl_details gd ON ( gd.cid = jp.cid AND gd.job_phase_id = jp.id )
	 			WHERE
	 				jp.id IN( ' . implode( ',', $arrintJobPhaseIds ) . ' ) AND
	 				jp.cid = ' . ( int ) $intCid . '
	 			GROUP BY
					jp.id,
					jp.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchJobBudgetSummaryApHeaderByJobIdByCid( $intJobId, $intCid, $objDatabase ) {

		if( false == valId( $intJobId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.*
					FROM
						ap_headers ah
						JOIN jobs j ON ( ah.cid = j.cid AND j.budget_summary_ap_header_id = ah.id )
					WHERE
						j.id = ' . ( int ) $intJobId . '
						AND ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_BUDGET_SUMMARY . '
						AND ah.deleted_on IS NULL';

		return parent::fetchApHeader( $strSql, $objDatabase );
	}

	public static function fetchJobBudgetApHeaderByJobPhaseIdByApCodeIdByPostMonthByCid( $intJobPhaseId, $intApCodeId, $strPostMonth, $intCid, $objDatabase ) {
		if( false == valId( $intJobPhaseId ) || false == valId( $intApCodeId ) || false == valStr( $strPostMonth ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ah.*
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					WHERE
						ad.cid = ' . ( int ) $intCid . '
						AND ad.job_phase_id = ' . ( int ) $intJobPhaseId . '
						AND ad.ap_code_id = ' . ( int ) $intApCodeId . '
						AND ad.post_month = \'' . $strPostMonth . '\'
						AND ad.deleted_on IS NULL
						AND ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::JOB_PHASE_BUDGET . ',' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT . ',' . CApHeaderSubType::JOB_CHANGE_ORDER . ' )
					LIMIT 1';

		return self::fetchApHeader( $strSql, $objDatabase );
	}

	public static function fetchApHeaderPropertiesById( $intApHeaderId, $intCid, $objClientDatabase ) {

		if( false == valId( $intApHeaderId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
					    ah.id,
					    string_agg( DISTINCT CASE WHEN p.is_disabled = 1 THEN p.property_name || \' (Disabled) \' ELSE p.property_name END, \', \') AS invoice_properties   
					FROM
					    ap_headers ah
					    JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					    JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
					WHERE
					    ah.CID = ' . ( int ) $intCid . '
					    AND ah.id = ' . ( int ) $intApHeaderId . '
					GROUP BY
					    ah.id';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPaginatedTemplateApHeadersByPropertyIdsByPurchaseOrdersFilterByCid( $arrintPropertyIds, $intCid, $objPagination, $objClientDatabase, $objPurchaseOrdersFilter = NULL, $intCompanyUserId = NULL, $boolShowCatalogPoTemplates = false, $boolIsFromPoTemplates = false, $boolIsAdministrator = true, $boolShowJobCostingPoTemplates = false, $intApHeaderSubTypeId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$boolIsArrayFormat          = true;
		$strConditions              = NULL;
		$strPropertyCondition       = NULL;
		$strPurchaseOrderCondition  = NULL;
		$strUserPropertiesCondition = NULL;

		$strOrderBy = 'ah.id DESC';

		if( false == is_null( $intCompanyUserId ) && false == is_null( $boolIsAdministrator ) ) {
			$strUserPropertiesCondition = 'JOIN ( ' . CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS cup ON ( cup.cid = p.cid AND cup.id = p.id )';
		}

		if( false == $boolIsFromPoTemplates ) {
			$strConditions .= ' AND ah.frequency_id = ' . CFrequency::MANUAL;
		}

		$arrintApHeaderSubTypeIds[] = CApHeaderSubType::STANDARD_PURCHASE_ORDER;
		if( true == $boolShowCatalogPoTemplates ) {
			$arrintApHeaderSubTypeIds[] = CApHeaderSubType::CATALOG_PURCHASE_ORDER;
		}

		if( true == $boolShowJobCostingPoTemplates ) {
			$arrintApHeaderSubTypeIds[] = CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER;
			$arrintApHeaderSubTypeIds[] = CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER;
		}

		if( true == valId( $intApHeaderSubTypeId ) ) {
			$strPurchaseOrderCondition = ' AND ah.ap_header_sub_type_id = ' . ( int ) $intApHeaderSubTypeId;
		} else {
			$strPurchaseOrderCondition = ' AND ah.ap_header_sub_type_id IN ( ' . sqlIntImplode( $arrintApHeaderSubTypeIds ) . ' ) ';
		}

		if( true == valObj( $objPurchaseOrdersFilter, 'CPurchaseOrdersFilter' ) && true == valStr( $objPurchaseOrdersFilter->getTemplateName() ) ) {
			$strConditions .= ' AND ah.template_name ILIKE \'%' . trim( addslashes( $objPurchaseOrdersFilter->getTemplateName() ) ) . '%\' ';
		}

		if( true == valObj( $objPurchaseOrdersFilter, 'CPurchaseOrdersFilter' ) && true == valArr( $objPurchaseOrdersFilter->getPropertyIds() ) ) {
			$strPropertyCondition = ' AND ad.property_id IN ( ' . implode( ',', $objPurchaseOrdersFilter->getPropertyIds() ) . ' ) ';
		} elseif( true == valObj( $objPurchaseOrdersFilter, 'CPurchaseOrdersFilter' ) && true == valArr( $objPurchaseOrdersFilter->getPropertyGroupIds() ) ) {
			$strPropertyCondition = ' AND p.id IN ( SELECT
													property_id
												FROM
													get_property_ids( ' . ( int ) $intCid . ', ARRAY[' . implode( ', ', $objPurchaseOrdersFilter->getPropertyGroupIds() ) . ']::INTEGER[] ) )';
		} else {
			$strPropertyCondition = ' AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql = ' SELECT
						ah.*,
						(
							SELECT
								CASE
									WHEN MAX ( ad.property_id ) <> MIN ( ad.property_id ) THEN \'Multiple\'
									ELSE MAX ( p.property_name )
								END
							FROM
								ap_details ad
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND ad.ap_header_id = ah.id
								AND ad.cid = ah.cid
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL
						) AS property_name,
						(
							SELECT
								SUM ( COALESCE( ad.transaction_amount, 0 ) )
							FROM
								ap_details ad
							WHERE
								ad.cid = ' . ( int ) $intCid . '
								AND ad.ap_header_id = ah.id
								AND ad.cid = ah.cid
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL
						) AS transaction_amount
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						LEFT JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
						' . $strUserPropertiesCondition . '
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.is_template IS TRUE
						AND ad.is_confidential IS FALSE
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL ' . $strConditions . $strPropertyCondition . $strPurchaseOrderCondition . '
					GROUP BY
						ah.id,
						ah.cid
					ORDER BY
						' . $strOrderBy;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchTemplateApHeadersCountByPropertyIdsByPurchaseOrdersFilterByCid( $arrintPropertyIds, $intCid, $objClientDatabase, $objPurchaseOrdersFilter = NULL, $intCompanyUserId = NULL, $boolShowCatalogPoTemplates = false, $boolIsFromPoTemplates = false, $boolIsAdministrator = true, $boolShowJobCostingPoTemplates = false, $intApHeaderSubTypeId = NULL ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return NULL;
		}

		$boolIsArrayFormat          = true;
		$strConditions              = NULL;
		$strPropertyCondition       = NULL;
		$strPurchaseOrderCondition  = NULL;
		$strUserPropertiesCondition = NULL;

		$strOrderBy = 'ah.id DESC';

		if( false == is_null( $intCompanyUserId ) && false == is_null( $boolIsAdministrator ) ) {
			$strUserPropertiesCondition = 'JOIN ( ' . CProperties::createService()->buildCompanyUserPropertiesSql( $intCid, $intCompanyUserId, $boolIsAdministrator ) . ' ) AS cup ON ( cup.cid = p.cid AND cup.id = p.id )';
		}

		if( false == $boolIsFromPoTemplates ) {
			$strConditions .= ' AND ah.frequency_id = ' . CFrequency::MANUAL;
		}

		$arrintApHeaderSubTypeIds[] = CApHeaderSubType::STANDARD_PURCHASE_ORDER;
		if( true == $boolShowCatalogPoTemplates ) {
			$arrintApHeaderSubTypeIds[] = CApHeaderSubType::CATALOG_PURCHASE_ORDER;
		}

		if( true == $boolShowJobCostingPoTemplates ) {
			$arrintApHeaderSubTypeIds[] = CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER;
			$arrintApHeaderSubTypeIds[] = CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER;
		}

		if( true == valId( $intApHeaderSubTypeId ) ) {
			$strPurchaseOrderCondition = ' AND ah.ap_header_sub_type_id = ' . ( int ) $intApHeaderSubTypeId;
		} else {
			$strPurchaseOrderCondition = ' AND ah.ap_header_sub_type_id IN ( ' . sqlIntImplode( $arrintApHeaderSubTypeIds ) . ' ) ';
		}

		if( true == valObj( $objPurchaseOrdersFilter, 'CPurchaseOrdersFilter' ) && true == valStr( $objPurchaseOrdersFilter->getTemplateName() ) ) {
			$strConditions .= ' AND ah.template_name ILIKE \'%' . trim( addslashes( $objPurchaseOrdersFilter->getTemplateName() ) ) . '%\' ';
		}

		if( true == valObj( $objPurchaseOrdersFilter, 'CPurchaseOrdersFilter' ) && true == valArr( $objPurchaseOrdersFilter->getPropertyIds() ) ) {
			$strPropertyCondition = ' AND ad.property_id IN ( ' . implode( ',', $objPurchaseOrdersFilter->getPropertyIds() ) . ' ) ';
		} elseif( true == valObj( $objPurchaseOrdersFilter, 'CPurchaseOrdersFilter' ) && true == valArr( $objPurchaseOrdersFilter->getPropertyGroupIds() ) ) {
			$strPropertyCondition = ' AND p.id IN ( SELECT
													property_id
												FROM
													get_property_ids( ' . ( int ) $intCid . ', ARRAY[' . implode( ', ', $objPurchaseOrdersFilter->getPropertyGroupIds() ) . ']::INTEGER[] ) )';
		} else {
			$strPropertyCondition = ' AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		}

		$strSql = 'SELECT
						COUNT( sub.id )
					FROM
						(
							SELECT
								ah.id,
								ah.cid,
								SUM ( COALESCE( ad.transaction_amount, 0 ) ) AS transaction_amount
							FROM
								ap_headers ah
								JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
								LEFT JOIN properties p ON ( ad.cid = p.cid AND ad.property_id = p.id )
								' . $strUserPropertiesCondition . '
							WHERE
								ah.cid = ' . ( int ) $intCid . '
								AND ah.is_template IS TRUE
								AND ad.is_confidential IS FALSE
								AND ad.deleted_by IS NULL
								AND ad.deleted_on IS NULL ' . $strConditions . $strPropertyCondition . $strPurchaseOrderCondition . '
							GROUP BY
								ah.id,
								ah.cid
							ORDER BY
								' . $strOrderBy . '
						) sub
					WHERE
						sub.cid = ' . ( int ) $intCid;

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchApHeadersByJobPhaseIdsByCid( $arrintJobPhaseIds, $intCid, $objDatabase, $arrintApHeaderSubtypeIds = NULL, $boolIsJobBudgetOnly = false ) {

		if( false == valArr( $arrintJobPhaseIds ) ) {
			return NULL;
		}

		if( true == valArr( $arrintApHeaderSubtypeIds ) ) {
			$strSubSql = ' AND ap_header_sub_type_id NOT IN ( ' . implode( ',', $arrintApHeaderSubtypeIds ) . ' )';
		}

		if( true == $boolIsJobBudgetOnly ) {
			$strSubSql = ' AND ap_header_sub_type_id = ' . CApHeaderSubType::JOB_PHASE_BUDGET;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND job_phase_id IN ( ' . implode( ',', $arrintJobPhaseIds ) . ' )
						' . $strSubSql . '
						AND deleted_on IS NULL';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchContractBudgetApHeadersByContractIdByApHeaderSubTypeIdsByCid( $intApContractId, $arrintApHeaderSubTypeIds, $intCid, $objDatabase ) {

		if( false == valId( $intApContractId ) || false == valArr( $arrintApHeaderSubTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						ap_contract_id = ' . ( int ) $intApContractId . ' 
						AND ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )
						AND ( ( ( ap_header_sub_type_id = ' . CApHeaderSubType::JOB_CHANGE_ORDER . ' OR ap_header_sub_type_id = ' . CApHeaderSubType::CONTRACT_CHANGE_ORDER . ' ) AND approved_on IS NOT NULL ) OR ap_header_sub_type_id NOT IN ( ' . CApHeaderSubType::JOB_CHANGE_ORDER . ', ' . CApHeaderSubType::CONTRACT_CHANGE_ORDER . ' ) )
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchPoInvoiceApHeaderCountByApContractIdByApHeaderSubTypeIdsByCid( $intApContractId, $arrintApHeaderSubTypeIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintApHeaderSubTypeIds ) || false == valId( $intApContractId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						count(*)
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
					WHERE
						ad.ap_contract_id = ' . ( int ) $intApContractId . ' 
						AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )
						AND ah.cid = ' . ( int ) $intCid . '
						AND ah.deleted_by is NULL';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchApHeadersByApContractIdsByCid( $arrintApContractIds, $intCid, $objDatabase, $arrintApHeaderSubtypeIds = NULL ) {

		if( false == valArr( $arrintApContractIds ) ) {
			return NULL;
		}

		$strSubSql = '';

		if( true == valArr( $arrintApHeaderSubtypeIds ) ) {
			$strSubSql = ' AND ap_header_sub_type_id NOT IN ( ' . implode( ',', $arrintApHeaderSubtypeIds ) . ' )';
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_contract_id IN ( ' . implode( ',', $arrintApContractIds ) . ' )
						' . $strSubSql . '
						AND deleted_on IS NULL';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchApHeadersByBudgetChangeOrderIdsByCid( $arrintBudgetChangeOrderIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintBudgetChangeOrderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND budget_change_order_id IN ( ' . implode( ',', $arrintBudgetChangeOrderIds ) . ' )
						AND deleted_on IS NULL';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchUnExportedApHeadersByPropertyIdsByBankAccountIdsByApPaymentTypeIdsByCids( $arrintPropertyIds, $arrintBankAccountIds, $arrintApPaymentTypeIds, $arrintCids, $objDatabase, $arrintExcludedPaymentStatuses = [], $boolIncludeWrittenChecks = false, $strWrittenCheckStartDate = NULL ) {

		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintBankAccountIds ) || false == valArr( $arrintCids ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ah.*,
						ap.bank_account_id,
						ad.property_id
					FROM
						ap_headers ah
						JOIN ap_payments ap ON ( ah.cid = ap.cid AND ah.ap_payment_id = ap.id )
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
					WHERE
						ap.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ap.bank_account_id IN ( ' . implode( ',', $arrintBankAccountIds ) . ' )
						AND ah.accounting_export_batch_id IS NULL ';

		if( false != valArr( $arrintExcludedPaymentStatuses ) ) {
			$strSql .= ' AND ap.payment_status_type_id NOT IN ( ' . implode( ',', $arrintExcludedPaymentStatuses ) . ')';
		}
		$strSql .= ' AND ( ap.ap_payment_type_id IN ( ' . implode( ', ', $arrintApPaymentTypeIds ) . ' ) ';
		if( false != $boolIncludeWrittenChecks ) {
			$strSql .= ' OR ( ap.ap_payment_type_id = ' . CApPaymentType::WRITTEN_CHECK . '  AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $strWrittenCheckStartDate ) ) . '\' )';
		}
		$strSql .= ' )';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchApHeaderByApPayeeIdByApHeaderTypeIdsByCid( $intApPayeeId, $arrintApHeaderTypeIds, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valArr( $arrintApHeaderTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_header_type_id IN ( ' . implode( ',', $arrintApHeaderTypeIds ) . ' ) LIMIT 1';

		return self::fetchApHeader( $strSql, $objClientDatabase );

	}

	public static function fetchApHeadersByCidByApBatchIdsByCreatedBy( $intCid, $arrintApBatchIds, $intCreatedBy, $objDatabase ) {

		if( false == valArr( $arrintApBatchIds ) ) return NULL;

		$strSql = 'SELECT
						ah.ap_batch_id,
						CASE
							WHEN apa.utility_bill_account_id IS NULL THEN \'Invoice Processing\'
							WHEN apa.utility_bill_account_id IS NOT NULL THEN \'Utility Expense Management\'
						END AS product_name
					FROM
						ap_headers ah
						JOIN ap_payee_accounts apa ON ( ah.cid = apa.cid AND ah.ap_payee_account_id = apa.id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_batch_id IN ( ' . sqlIntImplode( $arrintApBatchIds ) . ' )
						AND ah.created_by = ' . ( int ) $intCreatedBy;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApHeadersCountByCustomCondition( $strWhereCondition, $objClientDatabase ) {

		$strSql = ' SELECT concat_ws( \'~\', cid, ap_payee_id, header_number ) AS header, max(id) as ap_header_id FROM ap_headers WHERE ( header_number, cid, ap_payee_id ) IN ( ' . $strWhereCondition . ' ) AND deleted_by IS NULL AND deleted_on IS NULL AND reversal_ap_header_id IS NULL GROUP BY ( header_number, cid, ap_payee_id )';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCountApHeadersByCidByApPayeeIdByUtilityBillAccountId( $intCid, $intApPayeeId, $intUtilityBillAccountId, $objDatabase ) {

		$strSql = 'SELECT 
						count( ah.id ) AS count
					FROM
						ap_headers ah
						JOIN ap_payee_accounts apa ON ( ah.cid = apa.cid AND ah.ap_payee_account_id = apa.id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND apa.utility_bill_account_id = ' . ( int ) $intUtilityBillAccountId . ' 
						AND ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.reversal_ap_header_id IS NULL
						AND ah.ap_financial_status_type_id NOT IN( ' . CApFinancialStatusType::CANCELLED . ', ' . CApFinancialStatusType::REJECTED . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function  fetchSyncedApHeadersByOrderHeaderIdsByCid( $arrintOrderHeaderIds, $intCid, $objDatabase ) {
		if( false == ( $arrintOrderHeaderIds = getIntValuesFromArr( $arrintOrderHeaderIds ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						*
					FROM 
						ap_headers
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND order_header_id IN ( ' . sqlIntImplode( $arrintOrderHeaderIds ) . ' )';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function  fetchSyncedApHeaderByOrderHeaderIdByCid( $intOrderHeaderId, $intCid, $objDatabase ) {
		$strSql = 'SELECT 
						*
					FROM 
						ap_headers
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND reversal_ap_header_id IS NULL
						AND order_header_id =' . ( int ) $intOrderHeaderId;

		return self::fetchApHeader( $strSql, $objDatabase );
	}

	public static function fetchApHeadersByApPayeeIdByApPayeeLocationIdsByApPayeeAccountIdsByCid( $intApPayeeId, $arrintLocationIds, $arrintAccountIds, $intCid, $objClientDatabase ) {

		if( false == valId( $intApPayeeId ) || false == valArr( $arrintLocationIds ) || false == valArr( $arrintAccountIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_payee_location_id IN ( ' . implode( ',', $arrintLocationIds ) . ' )
						AND ap_payee_account_id IN ( ' . implode( ',', $arrintAccountIds ) . ' )';

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersByCidByStartDateByEndDate( $arrintCids, $strStartDate, $strEndDate, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$arrintUsers = [ CUser::ID_SCRIPT_LAYER ];

		$strSql = 'SELECT 
						ad.cid,
						ad.property_id,
						ah.ap_payee_id AS ap_payee_id,
						to_char( ah.created_on,\'yyyy-mm-dd\' ) as span,
						COUNT( DISTINCT ah.id) filter( where ah.created_by IN ( ' . sqlIntImplode( $arrintUsers ) . ') ) as ip_by_entrata,
						COUNT(DISTINCT ah.id) filter( where ah.created_by NOT IN ( ' . sqlIntImplode( $arrintUsers ) . ' ) ) as ip_by_client,
						COUNT(DISTINCT ah.id) as total_ip
					FROM 
						ap_headers ah
						JOIN ap_details ad ON (ad.ap_header_id = ah.id)
					WHERE 
 						ad.cid IN ( ' . sqlIntImplode( array_filter( $arrintCids ) ) . ' )
 						AND ad.deleted_by IS NULL 
 						AND ad.deleted_on IS NULL 
 						AND ah.created_on::date >= \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . '\'
						AND ah.created_on::date <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\'
 						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
					GROUP BY 
						ad.cid,
						ad.property_id,
						ah.ap_payee_id,
						to_char( ah.created_on,\'yyyy-mm-dd\' )
					ORDER BY ad.cid,
						ad.property_id,
						to_char( ah.created_on,\'yyyy-mm-dd\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRefundedApHeadersByRefundArTransactionIdByCid( $intRefundArTransactionId, $intCid, $objClientDatabase ) {

		$arrstrApPaymentNewStatusTypes = CApTransactionsFilter::createService()->getApPaymentNewStatusTypes();

		$strSql = 'WITH at_header_refund AS (
							SELECT
								at.id,
								at.cid,
								at.ar_code_type_id,
								ah.id ap_header_id
							FROM
								ar_transactions at
								JOIN ap_headers ah ON ( ah.cid = at.cid AND ah.refund_ar_transaction_id = at.id AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . ') 
							WHERE
								at.cid = ' . ( int ) $intCid . '
								AND at.ar_code_type_id = ' . CArCodeType::REFUND . ' 
								AND at.is_reversal = false 
								AND at.id = ' . ( int ) $intRefundArTransactionId . '
					), 
					at_header_refund_reversal AS (
						SELECT
							at.id,
							at.cid,
							at.ar_code_type_id,
							ah.id ap_header_id
						FROM
							ar_transactions at
							JOIN ap_headers ah ON ( ah.cid = at.cid AND ah.refund_ar_transaction_id = at.id AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . ') 
						WHERE
							at.cid = ' . ( int ) $intCid . '
							AND at.ar_code_type_id = ' . CArCodeType::REFUND . ' 
							AND at.is_reversal = true
							AND at.id < at.ar_transaction_id
							AND at.id = ' . ( int ) $intRefundArTransactionId . '
						ORDER BY
							ah.id ASC
						LIMIT 1
					), 
					at_header_refund_reversal_offset AS (
						SELECT
							at.id,
							at.cid,
							at.ar_code_type_id,
							ah.id ap_header_id
						FROM
							ar_transactions at
							JOIN ar_transactions at_reversal ON (at_reversal.cid = at.cid AND at_reversal.id = at.ar_transaction_id)
							JOIN ap_headers ah ON ( ah.cid = at_reversal.cid AND ah.refund_ar_transaction_id = at_reversal.id AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . ') 
						WHERE
							at.cid = ' . ( int ) $intCid . '
							AND at.ar_code_type_id = ' . CArCodeType::REFUND . ' 
							AND at.is_reversal = true
							AND at.id > at.ar_transaction_id
							AND at.id = ' . ( int ) $intRefundArTransactionId . '
						ORDER BY 
							ah.id DESC
						LIMIT 1
					), at_header AS (
						(
							SELECT 
								*
							FROM
								at_header_refund
						)
						UNION
						(
							SELECT 
								*
							FROM
								at_header_refund_reversal
						)
						UNION
						(
							SELECT 
								*
							FROM
								at_header_refund_reversal_offset
						)
					), at_invoices AS (
						SELECT
							at_header.ap_header_id,
							jsonb_agg(
								json_build_object(
									\'payment_id\', ap.id,
									\'payment_number\', ap.payment_number,
									\'ap_payment_type_id\', ap.ap_payment_type_id,
									\'allocation_amount\', aa.allocation_amount,
									\'payment_status_type_id\', ap.payment_status_type_id,
									\'payment_date\', ap.payment_date,
									\'payment_status_type_name\', 
									CASE
										WHEN ap.ap_payment_type_id = ' . CApPaymentType::BILL_PAY_ACH . ' OR ap.ap_payment_type_id = ' . CApPaymentType::BILL_PAY_CHECK . ' THEN (
											CASE
												WHEN ap.payment_status_type_id = ' . CPaymentStatusType::PENDING . ' THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_PENDING] . '\'
												WHEN ap.payment_status_type_id = ' . CPaymentStatusType::RECEIVED . ' THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECEIVED] . '\'
												WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDING . ' THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDING] . '\'
												WHEN ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURING . ' THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURING] . '\'
												WHEN ap.payment_status_type_id = ' . CPaymentStatusType::CAPTURED . ' THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CAPTURED] . '\'
												WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
												WHEN ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECALL . ',' . CPaymentStatusType::RETURNING . ' ) THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
											END ) 
										WHEN ap.payment_status_type_id = ' . CPaymentStatusType::VOIDED . ' THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_VOIDED] . '\'
										WHEN ap.payment_status_type_id IN ( ' . CPaymentStatusType::RECALL . ',' . CPaymentStatusType::RETURNING . ' ) THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RETURNED] . '\'
										ELSE 
											CASE
												WHEN gd.gl_reconciliation_id IS NOT NULL AND gr.gl_reconciliation_status_type_id =' . CGlReconciliationStatusType::PAUSED . ' THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_CLEARED] . '\'
												WHEN gd.gl_reconciliation_id IS NULL THEN \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_UNRECONCILED] . '\'
											ELSE \'' . $arrstrApPaymentNewStatusTypes[CApTransactionsFilter::AP_PAYMENT_STATUS_TYPE_RECONCILED] . '\'
										END
									END
								)
								ORDER BY
									ap.payment_date
							) AS payments
						FROM
							at_header
							LEFT JOIN ap_headers ah_payment ON ( ah_payment.cid = at_header.cid AND at_header.ap_header_id::text = any( string_to_array( ah_payment.header_number, \',\' ) ) AND ah_payment.reversal_ap_header_id IS NULL )
							LEFT JOIN ap_payments ap ON ( ap.cid = ah_payment.cid AND ap.id = ah_payment.ap_payment_id )
							LEFT JOIN ap_details ad ON ( ad.cid = ap.cid AND ad.ap_header_id = ah_payment.id )
							LEFT JOIN ap_allocations aa ON ( aa.cid = ad.cid AND aa.credit_ap_detail_id = ad.id )
							LEFT JOIN bank_accounts ba ON ( ba.cid = ap.cid AND ba.id = ap.bank_account_id )
							LEFT JOIN gl_headers gh ON ( gh.cid = aa.cid AND gh.reference_id = aa.id AND gh.gl_transaction_type_id = aa.gl_transaction_type_id AND gh.gl_transaction_type_id = ' . CGlTransactionType::AP_ALLOCATION . ' )
							LEFT JOIN gl_details gd ON ( gd.cid = gh.cid AND gd.gl_header_id = gh.id
														AND gd.amount = CASE
																		WHEN aa.origin_ap_allocation_id IS NOT NULL OR aa.lump_ap_header_id IS NOT NULL THEN
																			aa.allocation_amount * - 1
																		ELSE
																			aa.allocation_amount
																	END
													AND gd.property_id = CASE
																			WHEN ba.reimbursed_property_id <> aa.property_id THEN
																				ba.reimbursed_property_id
																			ELSE
																				aa.property_id
																			END
														)
								LEFT JOIN gl_reconciliations gr ON ( gr.cid = gd.cid AND gr.id = gd.gl_reconciliation_id )
						WHERE 
							at_header.cid = ' . ( int ) $intCid . '
							AND at_header.ar_code_type_id = ' . CArCodeType::REFUND . ' 
							AND at_header.id =  ' . ( int ) $intRefundArTransactionId . '
						GROUP BY
							at_header.ap_header_id
					)
					SELECT 
						ah.id,
						ah.header_number,
						ah.is_deleted,
						ah.is_posted,
						ah.reversal_ap_header_id,
						ah.post_date,
						ah.post_month,
						ai.payments
					FROM 
						ap_headers ah
						JOIN at_invoices ai on ai.ap_header_id = ah.id
					WHERE
						ah.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersByBudgetAdjustmentIdsByCid( $arrintBudgetAdjustmentIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintBudgetAdjustmentIds ) ) return NULL;

		$strSql = 'SELECT
						ah.*
					FROM
						ap_headers ah
						JOIN budget_adjustments ba ON ( ah.cid = ba.cid AND ( ah.id = ba.to_ap_header_id OR ah.id = ba.from_ap_header_id ) )
					WHERE
						ba.cid = ' . ( int ) $intCid . '
						AND ba.id IN ( ' . implode( ',', $arrintBudgetAdjustmentIds ) . ' )
						AND ah.deleted_on IS NULL
						AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::JOB_BUDGET_ADJUSTMENT;

		return self::fetchApHeaders( $strSql, $objDatabase );

	}

	public static function fetchApHeadersByPropertyIdsByApPaymentTypeIdsByCid( $arrintPropertyIds, $arrintApPaymentTypeIds, $intCid, $objDatabase, $arrintBankAccountIds = [], $boolIsExported = false, $strStartDate = NULL, $strEndDate = NULL, $arrintExCludedPaymentStatuses = [], $boolIncludeWrittenChecks = false, $strWrittenCheckStartDate = NULL, $intCheckNumberRangeFrom = NULL, $intCheckNumberRangeTo = NULL ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT DISTINCT ON ( ah.id )
                       ah.*
					FROM
					    ap_headers ah
                        JOIN ap_payments ap ON ( ah.cid = ap.cid AND ah.ap_payment_id = ap.id )
                        JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.gl_transaction_type_id = ah.gl_transaction_type_id AND ad.post_month = ah.post_month )
					WHERE
					    ah.cid = ' . ( int ) $intCid . '
					    AND ad.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';
		if( false == $boolIsExported ) {
			$strSql .= ' AND ah.accounting_export_batch_id IS NULL ';
		}
		if( false != valArr( $arrintBankAccountIds ) ) {
			$strSql .= ' AND ap.bank_account_id IN ( ' . implode( ', ', $arrintBankAccountIds ) . ' ) ';
		}
		if( false == is_null( $strStartDate ) && false == is_null( $strEndDate ) ) {
			$strSql .= ' AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . '\' AND ap.payment_date <=\'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . '\' ';
		}

		if( false != valArr( $arrintExCludedPaymentStatuses ) ) {
			$strSql .= ' AND ap.payment_status_type_id NOT IN ( ' . implode( ',', $arrintExCludedPaymentStatuses ) . ')';
		}

		if( false != valId( $intCheckNumberRangeFrom ) && false != valId( $intCheckNumberRangeTo ) ) {
			$strSql .= ' AND  ap.payment_number::int >= ' . $intCheckNumberRangeFrom . '
			            AND ap.payment_number::int <= ' . $intCheckNumberRangeTo;
		}
		$strSql .= ' AND ( ap.ap_payment_type_id IN ( ' . implode( ', ', $arrintApPaymentTypeIds ) . ' ) ';
		if( false != $boolIncludeWrittenChecks ) {
			$strSql .= ' OR ( ap.ap_payment_type_id = ' . CApPaymentType::WRITTEN_CHECK . '  AND ap.payment_date >= \'' . date( 'Y-m-d', strtotime( $strWrittenCheckStartDate ) ) . '\' )';
		}
		$strSql .= ' )';
		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchApHeadersByApPayeeLocationIdByApPayeeAccountIdByCid( $intApPayeeLocationId, $intApPayeeAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND ap_payee_account_id = ' . ( int ) $intApPayeeAccountId;

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchUnpaidInvoicesCountByApPayeeAccountIdByCid( $intApPayeeAccountId, $intCid, $objDatabase ) {

		if( false == valId( $intApPayeeAccountId ) ) return NULL;

		$strSql = 'SELECT
						COUNT( ah.id )
					FROM
						ap_headers ah
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payee_account_id = ' . ( int ) $intApPayeeAccountId . '
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND ah.ap_payment_id IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND ah.is_batching = FALSE
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ah.is_template = FALSE
						AND ah.transaction_amount_due != 0
						AND ( ah.ap_financial_status_type_id NOT IN ( ' . CApFinancialStatusType::DELETED . ' ) OR ah.ap_financial_status_type_id IS NULL )';

		return self::fetchColumn( $strSql, 'count', $objDatabase );

	}

	/**
	 * @FIXME - Shall function return keyed array of ap headers or not keyed?
	 */

	public static function fetchInvoiceApHeadersByApPaymentIdsByCid( $arrintApPaymentIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintApPaymentIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT DISTINCT
						ah_invoices.*,
						ah_payments.ap_payment_id,
						ah_invoices.id AS ap_header_id,
						ap.payment_number,
						sub.payment_amount
					FROM
						(
							SELECT
								id,
								CID,
								ap_payment_id,
								STRING_TO_ARRAY ( STRING_AGG ( header_number, \',\' ), \',\' )::INTEGER [ ] AS invoice_ap_header_ids
							FROM
								ap_headers
							WHERE
								cid = ' . ( int ) $intCid . '
								AND ap_payment_id IN ( ' . implode( ', ', $arrintApPaymentIds ) . ' )
							GROUP BY
								id,
								cid,
								ap_payment_id
						) ah_payments
						JOIN ap_headers ah_invoices ON ( ah_invoices.cid = ah_payments.cid AND ah_invoices.id = ANY ( ah_payments.invoice_ap_header_ids ) )
						JOIN ap_payments ap ON ( ap.cid = ah_payments.cid AND ap.id = ah_payments.ap_payment_id )
						JOIN LATERAL (
							SELECT 
								CASE
								WHEN ad_invoices.transaction_amount > 0 THEN ABS ( SUM ( aa.allocation_amount ) OVER ( PARTITION BY ah_invoices.cid, ah_invoices.header_number, ah_payments.ap_payment_id, ah_invoices.id, ap.payment_number ) )
								ELSE SUM ( aa.allocation_amount ) OVER ( PARTITION BY ah_invoices.cid, ah_invoices.header_number, ah_payments.ap_payment_id, ah_invoices.id, ap.payment_number )
								END AS payment_amount
							FROM
								ap_allocations aa
								JOIN ap_details ad_invoices ON ( ad_invoices.cid = ah_invoices.cid AND ad_invoices.ap_header_id = ah_invoices.id )
								JOIN ap_details ad_payments ON ( ad_payments.cid = ah_payments.cid AND ad_payments.ap_header_id = ah_payments.id )
							WHERE
								aa.cid = ' . ( int ) $intCid . '
								AND aa.cid = ad_invoices.cid
								AND ad_invoices.id IN ( aa.charge_ap_detail_id, aa.credit_ap_detail_id )
								AND ad_payments.id IN ( aa.charge_ap_detail_id, aa.credit_ap_detail_id )
						) sub ON TRUE
					WHERE
						ah_payments.cid = ' . ( int ) $intCid . '
						AND ah_payments.ap_payment_id IN ( ' . implode( ', ', $arrintApPaymentIds ) . ' )';

		return self::fetchApHeaders( $strSql, $objClientDatabase, false );
	}

	public static function fetchStandardPoApHeadersByApPayeeIdByApPayeeAccountIdByCid( $intApPayeeId, $intApPayeeAccountId, $intCid, $objClientDatabase ) {

		$strSql = 'SELECT DISTINCT
						ah.id AS id,
						ah.header_number as number
					FROM
						ap_headers ah
						INNER JOIN ap_details ad ON ( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ah.ap_payee_account_id = ' . ( int ) $intApPayeeAccountId . '
						AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ah.ap_financial_status_type_id IN ( ' . CApFinancialStatusType::APPROVED . ', ' . CApFinancialStatusType::PARTIALLY_INVOICED . ' )
						AND ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::STANDARD_PURCHASE_ORDER . ' )
						AND ah.deleted_by IS NULL
						AND ah.deleted_by IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchCatalogPoApHeadersByIdsByCid( $arrintPoApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPoApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id
					FROM
						ap_headers ah
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . sqlIntImplode( $arrintPoApHeaderIds ) . ' )
						AND ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ap_header_sub_type_id = ' . CApHeaderSubType::CATALOG_PURCHASE_ORDER . '
						AND deleted_by IS NULL
						AND deleted_by IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchStandardJobPoApHeadersByIdsByCid( $arrintPoApHeaderIds, $intCid, $objClientDatabase ) {

		if( false == valArr( $arrintPoApHeaderIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						id
					FROM
						ap_headers ah
					WHERE
						cid = ' . ( int ) $intCid . '
						AND id IN ( ' . sqlIntImplode( $arrintPoApHeaderIds ) . ' )
						AND ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . '
						AND deleted_by IS NULL
						AND deleted_by IS NULL';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersByApContractIdByApHeaderSubTypeIdsByCid( $intApContractId, $arrintApHeaderSubTypeIds, $intCid, $objDatabase ) {
		if( false == valId( $intApContractId ) || false == valIntArr( $arrintApHeaderSubTypeIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ah.*
					FROM
						ap_headers ah
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_contract_id = ' . ( int ) $intApContractId . '
						AND ah.ap_header_sub_type_id IN ( ' . sqlIntImplode( $arrintApHeaderSubTypeIds ) . ' )
						AND ah.deleted_on IS NULL';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchScheduledApHeadersByNextPostDate( $intApHeaderId = CApHeaderType::PURCHASE_ORDER, $strNextPostDate, $objClientDatabase ) {

		if( true == is_null( $strNextPostDate ) ) return NULL;

		$strHavingCondition = '';
		$strWhereCondition = ( CApHeaderType::INVOICE == $intApHeaderId ) ? 'AND NOT ah.is_disabled' : ' AND ah.approved_on IS NOT NULL AND ah.approved_by IS NOT NULL';

		if( CApHeaderType::PURCHASE_ORDER == $intApHeaderId ) {
			$strHavingCondition = ' HAVING
										( ah.ap_header_sub_type_id <> ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' OR bool_and( j.job_status_id IN ( ' . sqlIntImplode( \CJobStatus::$c_arrintAllowedFinancialJobStatusIds ) . ' ) ) )';
		}

		$strSql = 'SELECT
						ah.*,
						CASE
							WHEN apa.account_number IS NULL THEN TRUE
							ELSE FALSE 
						END AS is_location_dummy_account
					FROM
						ap_headers ah
						LEFT JOIN ap_payee_accounts apa ON( ah.cid = apa.cid AND ah.ap_payee_id = apa.ap_payee_id AND ah.ap_payee_location_id = apa.ap_payee_location_id AND ah.ap_payee_account_id = apa.id )
						LEFT JOIN ap_details ad ON( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						LEFT JOIN job_phases jp ON( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
						LEFT JOIN jobs j ON( j.cid = jp.cid AND j.id = jp.job_id )
					WHERE
						ah.ap_header_type_id = ' . ( int ) $intApHeaderId . '
						AND ah.is_template = TRUE
						AND ah.frequency_id IN ( ' . CFrequency::DAILY . ', ' . CFrequency::WEEKLY . ', ' . CFrequency::MONTHLY . ', ' . CFrequency::YEARLY . ', ' . CFrequency::WEEKDAY_OF_MONTH . ' )
						AND ah.next_post_date <= \'' . $strNextPostDate . '\'
						AND ah.deleted_on IS NULL
						AND ah.deleted_by IS NULL
						AND ( ah.is_on_hold IS NULL OR ah.is_on_hold = FALSE )
						' . $strWhereCondition . '
					GROUP BY
						ah.cid,
						ah.id,
						apa.account_number
						' . $strHavingCondition;

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeaderWithApPayeesByIdByCid( $intApHeaderId, $intCid, $objDatabase ) {
		if( false == valId( $intApHeaderId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ah.*,
						ap.company_name AS ap_payee_name,
						apl.location_name AS ap_payee_location_name,
						apa.account_number AS ap_payee_account_number,
						apt.name AS ap_payee_term_name
					FROM
						ap_headers ah
						JOIN ap_payees ap ON ( ap.cid = ah.cid AND ap.id = ah.ap_payee_id )
						JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						JOIN ap_payee_accounts apa ON ( apa.cid = ah.cid AND apa.id = ah.ap_payee_account_id )
						JOIN ap_remittances ar ON ( ar.cid = ah.cid AND ar.id = ah.ap_remittance_id )
						JOIN ap_payee_terms apt ON ( apt.cid = ah.cid AND apt.id = ah.ap_payee_term_id )
					WHERE
						ah.id = ' . ( int ) $intApHeaderId . '
						AND ah.cid = ' . ( int ) $intCid . '
						AND ah.deleted_on IS NULL';

		return self::fetchApHeader( $strSql, $objDatabase );
	}

	public static function fetchApHeadersCountByApRemittanceIdsByCid( $strApRemittanceIds, $intCid, $objDatabase ) {

		if( false == valStr( $strApRemittanceIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ap_remittance_id,
						count(ap_remittance_id) AS remittance_count
					FROM
						ap_headers
					WHERE
						ap_remittance_id IN ( ' . $strApRemittanceIds . ' )
						AND cid = ' . ( int ) $intCid . '
					GROUP BY
						ap_remittance_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRefundCustomerDetailsByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						ah.id,
						lc.customer_id,
						c.name_first,
						c.name_middle,
						c.name_last
					FROM
						ap_headers ah
						LEFT JOIN ap_header_lease_customers ahlc ON ( ah.id = ahlc.ap_header_id AND ah.cid = ahlc.cid )
						LEFT JOIN lease_customers lc ON ( lc.cid = ah.cid AND lc.id = COALESCE( ahlc.lease_customer_id, ah.lease_customer_id ) )
						LEFT JOIN customers c ON ( c.id = lc.customer_id AND c.cid = lc.cid )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.id IN(' . implode( ',', $arrintIds ) . ')';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApHeadersByApCodeIdsByCid( $arrintApCodeIds, $intCid, $objDatabase ) {

		if( false == valIntArr( $arrintApCodeIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						ah.ap_header_type_id as header_type,
						ah.header_number as header_name,
						ap.company_name as vendor_name,
						ac.id as ap_code_id
					FROM
						ap_codes ac
						JOIN ap_details ad ON( ad.ap_code_id = ac.id AND ad.cid = ac.cid AND ad.job_phase_id IS NOT NULL)
						JOIN ap_headers ah ON( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
						JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
					WHERE
						ac.id IN ( ' . implode( ',', $arrintApCodeIds ) . ' )
						AND ac.cid = ' . ( int ) $intCid . '
						AND ( ( ah.ap_header_sub_type_id IN  ( ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ' ) AND ( ah.transaction_amount = ah.transaction_amount_due OR ( ah.transaction_amount_due <> 0 AND ah.transaction_amount_due <> ah.transaction_amount ) ) ) OR
								( ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' ) AND ah.ap_financial_status_type_id IN( ' . CApFinancialStatusType::PENDING . ', ' . CApFinancialStatusType::APPROVED . ' ) ))
						AND ah.ap_financial_status_type_id IS DISTINCT FROM ' . CApFinancialStatusType::DELETED . '							
						AND ah.deleted_on IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND	ad.deleted_on IS NULL
					GROUP BY
						ah.ap_header_type_id,
						ah.header_number,
						ap.company_name,
						ac.id
					UNION
					SELECT
					    ah.ap_header_type_id as header_type,
					    ah.header_number as header_name,
					    ap.company_name as vendor_name,
						ad.ap_code_id
					FROM ap_details ad
					    JOIN ap_details ad1 ON( ad.cid = ad1.cid AND ad.id = ad1.po_ap_detail_id )
					    JOIN ap_headers ah ON( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
					    JOIN ap_payees ap ON( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
					WHERE
					    ad.ap_code_id IN ( ' . implode( ',', $arrintApCodeIds ) . ' )
					    AND ad.cid = ' . ( int ) $intCid . '
						AND ( ah.ap_header_sub_type_id IN ( ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ' ) AND ah.ap_financial_status_type_id IN( ' . CApFinancialStatusType::PENDING . ', ' . CApFinancialStatusType::APPROVED . ', ' . CApFinancialStatusType::PARTIALLY_INVOICED . ' ) )
						AND ah.deleted_on IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND ad.deleted_on IS NULL
					GROUP BY
					    ad.cid,
					    ad.id,
					    ah.header_number,
					    ah.ap_header_type_id,
					    ap.company_name,
					    ad.ap_code_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApHeaderIdsByCidByApPayeeIdByApPayeeLocationIdByHeaderNumberByTransactionByDueDate( $intCid, $intApPayeeId, $intApPayeeLocationId, $strHeaderNumber, $objDatabase, $strDueDate = NULL, $intLimit = 1 ) {

		if( false == valId( $intCid ) || false == valId( $intApPayeeId ) || false == valId( $intApPayeeLocationId ) || false == valStr( $strHeaderNumber ) ) {
			return NULL;
		}

		$strWhereClause = '';

		if( false == is_null( $strDueDate ) ) {
			$strWhereClause = ' AND due_date = \'' . $strDueDate . '\'';
		}

		$strSql = 'SELECT
						*
					FROM
						ap_headers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND ap_payee_id = ' . ( int ) $intApPayeeId . '
						AND ap_payee_location_id = ' . ( int ) $intApPayeeLocationId . '
						AND header_number = \'' . $strHeaderNumber . '\'' . $strWhereClause . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchApHeader( $strSql, $objDatabase );
	}

	public static function fetchJobsApHeadersCountByJobIdByCid( $intJobId, $intCid, $objClientDatabase, $objApTransactionsFilter = NULL ) {

		$strWhereConditions = '';

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$arrstrAndConditions = ( array ) self::fetchSearchCriteria( $intCid, $objApTransactionsFilter );

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strWhereConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		if( true == valArr( $objApTransactionsFilter->getJobCostCodeIds() ) ) {
			$strWhereConditions .= ' AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) ';
		}

		$strSql = 'SELECT
						COUNT( DISTINCT ah.id )
					FROM
						ap_headers ah
						JOIN ap_payees ap ON ( ah.cid = ap.cid AND ah.ap_payee_id = ap.id )
						JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN job_phases jp ON ( jp.cid = ad.cid AND jp.id = ad.job_phase_id )
						LEFT JOIN draw_request_details drd ON ( drd.cid = ad.cid AND drd.ap_detail_id = ad.id )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND jp.job_id = ' . ( int ) $intJobId . '
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND ah.ap_header_sub_type_id IN  ( ' . CApHeaderSubType::STANDARD_JOB_INVOICE . ',' . CApHeaderSubType::CATALOG_JOB_INVOICE . ' )
						AND ah.deleted_on IS NULL
						AND	ad.deleted_on IS NULL
						AND ah.is_template IS FALSE
						' . $strWhereConditions . ' ';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );

	}

	public static function fetchPoApHeadersCountByJobIdByCid( $intJobId, $intCid, $objDatabase, $objApTransactionsFilter = NULL ) {

		if( false == valId( $intJobId ) || false == valId( $intCid ) ) return NULL;

		$arrstrAndConditions = ( array ) self::fetchSearchCriteria( $intCid, $objApTransactionsFilter );

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objApTransactionsFilter->getApPhysicalStatusTypeIds() ) ) {
			$arrstrAndConditions['or_clause'][] = ' ah.ap_physical_status_type_id IN ( ' . sqlIntImplode( $objApTransactionsFilter->getApPhysicalStatusTypeIds() ) . ' )';
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndConditions['or_clause'] ) ) {
			$arrstrAndConditions['where_clause'][] = ' ( ' . implode( ' OR ', $arrstrAndConditions['or_clause'] ) . ' )';
		}

		if( true == array_key_exists( 'where_clause', $arrstrAndConditions ) ) {
			$strWhereConditions = ' AND ' . implode( ' AND ', $arrstrAndConditions['where_clause'] );
		}

		if( true == valArr( $objApTransactionsFilter->getJobCostCodeIds() ) ) {
			$strWhereConditions .= ' AND ad.ap_code_id IN ( ' . implode( ',', $objApTransactionsFilter->getJobCostCodeIds() ) . ' ) ';
		}

		if( true == valObj( $objApTransactionsFilter, 'CApTransactionsFilter' ) && true == valArr( $objApTransactionsFilter->getApFinancialStatusTypeIds() ) ) {
			$strWhereConditions .= ' AND ah.ap_financial_status_type_id IN( ' . sqlIntImplode( $objApTransactionsFilter->getApFinancialStatusTypeIds() ) . ' )';
		}

		$strSql = 'SELECT
						COUNT( DISTINCT ah.id)
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ad.ap_header_id = ah.id AND ad.cid = ah.cid )
						JOIN job_phases jp ON ( jp.id = ad.job_phase_id AND jp.cid = ad.cid )
						JOIN ap_payees ap ON ( ap.id = ah.ap_payee_id AND ap.cid = ah.cid )
						LEFT JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id AND apl.ap_payee_id = ap.id )
					WHERE
						jp.job_id = ' . ( int ) $intJobId . '
						AND jp.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
						AND ah.ap_header_sub_type_id IN( ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . ', ' . CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER . ' )
						AND ah.is_template IS FALSE
						AND ah.deleted_on IS NULL
						AND ad.deleted_on IS NULL'
		          . $strWhereConditions . ' ';

		return self::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchApHeadersCountByContractIdByCid( $intCid, $intContractId, $objClientDatabase ) {

		if( false == valId( $intContractId ) || false == valId( $intCid ) ) return NULL;

		$arrintApHeaderSubTypeIds		= [ CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER, CApHeaderSubType::CATALOG_JOB_PURCHASE_ORDER ];

		$strSql = ' SELECT
						COUNT( DISTINCT ah.id )
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.id = ad.ap_header_id AND ah.cid = ad.cid )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ad.ap_contract_id = ' . ( int ) $intContractId . '
						AND ah.ap_header_type_id = ' . ( int ) CApHeaderType::PURCHASE_ORDER . '
						AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )
						AND ah.is_template IS FALSE
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );

	}

	public static function fetchContractApHeadersCountByContractIdByCid( $intCid, $intContractId, $objClientDatabase ) {

		if( false == valId( $intContractId ) || false == valId( $intCid ) ) return NULL;

		$arrintApHeaderSubTypeIds	= [ CApHeaderSubType::STANDARD_JOB_INVOICE, CApHeaderSubType::CATALOG_JOB_INVOICE ];

		$strSql = ' SELECT
						COUNT( DISTINCT ah.id )
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN ap_contracts ac ON ( ac.id = ad.ap_contract_id AND ac.cid = ad.cid )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ad.ap_contract_id = ' . ( int ) $intContractId . '
						AND ah.ap_header_type_id = ' . ( int ) CApHeaderType::INVOICE . '
						AND ah.ap_header_sub_type_id IN ( ' . implode( ',', $arrintApHeaderSubTypeIds ) . ' )
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND ad.deleted_by IS NULL
						AND ad.deleted_on IS NULL
						AND ah.is_template IS FALSE
						AND ( ah.ap_financial_status_type_id IS NULL OR ah.ap_financial_status_type_id <> ' . CApFinancialStatusType::DELETED . ' )
						AND ah.reversal_ap_header_Id IS NULL';

		return self::fetchColumn( $strSql, 'count', $objClientDatabase );
	}

	public static function fetchApHeadersCountByApLegalEntityIdsByHeaderNumbersByCids( $arrintApLegalEntityIds, $arrstrHeaderNumbers, $arrintCids, $objDatabase ) {

		if( false == ( $arrintApLegalEntityIds = getIntValuesFromArr( $arrintApLegalEntityIds ) ) || false == ( $arrintCids = getIntValuesFromArr( $arrintCids ) ) || false == valArr( $arrstrHeaderNumbers ) ) {
			return [];
		}

		$strSql = 'SELECT
						ah.cid,
						count(ah.id) AS ap_header_count
					FROM
						ap_headers ah
						JOIN ap_legal_entities ale ON ( ah.ap_payee_id = ale.ap_payee_id AND ah.cid = ale.cid )
					WHERE
						ah.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND ah.gl_transaction_type_id = ' . CGlTransactionType::AP_CHARGE . '
						AND LOWER ( ah.header_number ) IN ( \'' . implode( '\',\'', array_map( 'addslashes', ( array ) $arrstrHeaderNumbers ) ) . '\' )
						AND ah.reversal_ap_header_id IS NULL
						AND EXISTS (
										SELECT 
											NULL 
										FROM
											company_preferences cp
										WHERE
											cp.cid IN ( ' . sqlIntImplode( $arrintCids ) . ' )
											AND cp.key = \'ALLOW_DUPLICATE_INVOICE_NUMBERS_PER_VENDOR\'
											AND cp.value = \'0\'
									)
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
					GROUP BY
						ah.cid';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnPaidPostedInvoicesByCcVendorByPropertyIdsByCid( $intBankAccountId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valId( $intBankAccountId ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ah.id,
						ah.header_number,
						SUM( ad.transaction_amount_due ) AS transaction_amount_due
					FROM ap_headers ah
						JOIN bank_accounts ba ON ( ba.cid = ah.cid AND ba.ap_payee_id = ah.ap_payee_id AND ba.ap_payee_location_id = ah.ap_payee_location_id AND ba.ap_payee_account_id = ah.ap_payee_account_id )
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id AND ad.deleted_on IS NULL )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.is_posted IS TRUE
						AND ah.transaction_amount_due > 0
						AND ad.property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND ba.id = ' . $intBankAccountId . '
						AND ah.deleted_on IS NULL
						AND ah.reversal_ap_header_id IS NULL
						AND ( ah.ap_financial_status_type_id NOT IN ( ' . implode( ',', [ CApFinancialStatusType::DELETED, CApFinancialStatusType::REJECTED ] ) . ' ) OR ah.ap_financial_status_type_id IS NULL )
					GROUP BY ah.id, ah.header_number';

		return self::fetchApHeaders( $strSql, $objDatabase );
	}

	public static function fetchPendingBillbackApHeadersByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						ah.id,
						ah.header_number
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ad.cid = ah.cid AND ad.ap_header_id = ah.id )
						LEFT JOIN ap_detail_reimbursements adr ON ( ad.cid = adr.cid AND ad.id = adr.original_ap_detail_id AND adr.is_deleted = false )
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id = ' . CApHeaderType::INVOICE . '
						AND NOT ah.is_template
						AND ah.transaction_amount_due = 0
						AND ad.property_id = ' . $intPropertyId . '
						AND adr.id IS NULL
						AND ad.ap_transaction_type_id = ' . CApTransactionType::STANDARD . '
						AND ad.reimbursement_method_id = ' . CReimbursementMethod::BILLBACK . '
					GROUP BY 
						ah.id,
						ah.header_number
					ORDER BY
						ah.header_number DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApHeadersByApHeaderTypeIdByApHeaderSubTypeIdByCid( $intApHeaderTypeId, $intApHeaderSubTypeId, $intCid, $objClientDatabase, $arrmixParameters ) {
		$strSelect = 'SELECT
						DISTINCT ON( ah.id )
						ah.*';

		$strFrom = ' ap_headers ah';
		if( false != valStr( $arrmixParameters['vendor_code'] ) ) {
			$strFrom .= ' JOIN ap_payee_locations apl ON ( apl.cid = ah.cid AND apl.id = ah.ap_payee_location_id )';
		}

		$strWhere = ' WHERE
						ah.cid = ' . ( int ) $intCid;
		if( false != valId( $arrmixParameters['ap_payee_id'] ) ) {
			$strWhere .= ' AND ah.ap_payee_id = ' . $arrmixParameters['ap_payee_id'];
		}
		if( false != valStr( $arrmixParameters['vendor_code'] ) ) {
			$strWhere .= ' AND apl.vendor_code = ' . pg_escape_literal( $objClientDatabase->getHandle(), $arrmixParameters['vendor_code'] ) . '';
		}
		$strWhere .= '
			AND reversal_ap_header_id IS NULL
			AND ah.is_template IS FALSE
			AND ah.deleted_by IS NULL
			AND ah.deleted_by IS NULL
			AND ah.ap_header_type_id = ' . ( int ) $intApHeaderTypeId . '
			AND ah.ap_header_sub_type_Id = ' . ( int ) $intApHeaderSubTypeId . '
			  ';

		if( false != valArr( $arrmixParameters['header_numbers'] ) ) {
			$strWhere .= ' AND ah.header_number IN ( ' . sqlStrImplode( $arrmixParameters['header_numbers'] ) . ' )';
		}

		if( true == $arrmixParameters['is_return_count_only'] ) {
			return self::fetchRowCount( $strWhere, $strFrom, $objClientDatabase );
		}

		$strSql = $strSelect .
		          ' FROM ' .
		          $strFrom .
		          $strWhere;
		if( false != valObj( $arrmixParameters['pagination'], \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' OFFSET ' . ( int ) $arrmixParameters['pagination']->getOffset() .
			           ' LIMIT ' . ( int ) $arrmixParameters['pagination']->getPageSize();
		}

		return self::fetchApHeaders( $strSql, $objClientDatabase );
	}

	public static function fetchApHeadersByYearByPropertyIdByCid( $intYear, $intPropertyId, $intCid, $objDatabase, $intMonth = NULL ) {
		if( false === valId( $intYear ) || false === valId( $intPropertyId ) || false === valId( $intCid ) || false === valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$strApHeaderWhereConditions = '';
		$strGlHeaderWhereConditions = '';

		if( true === valId( $intMonth ) ) {
			$strApHeaderWhereConditions .= ' AND date_part(\'month\', ah.post_month) = ' . ( int ) $intMonth;
			$strGlHeaderWhereConditions .= ' AND date_part(\'month\', gh.post_month) = ' . ( int ) $intMonth;
		}

		$strSql = 'SELECT
						ad.gl_account_id,
						gat.name AS gl_account_name,
						gat.account_number,
						sum( ad.transaction_amount ) AS transaction_amount,
						ad.ap_header_id,
						date_part(\'month\', ah.post_month) AS post_month,
						ah.header_number :: text,
						ah.post_month as invoice_date,
						\'invoice\' as type
					FROM
						ap_headers ah
						JOIN ap_details ad ON ( ah.cid = ad.cid AND ah.id = ad.ap_header_id )
						JOIN gl_account_trees gat ON ( ad.cid = gat.cid AND ad.gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
					WHERE
						ah.lease_customer_id IS NULL
						AND date_part( \'year\', ah.post_month ) = ' . ( int ) $intYear . '
						' . $strApHeaderWhereConditions . '
						AND ad.property_id = ' . ( int ) $intPropertyId . '
						AND ah.is_posted = TRUE
						AND ah.cid = ' . ( int ) $intCid . '
						AND ah.deleted_by IS NULL
						AND ad.deleted_by IS NULL
						AND ah.gl_transaction_type_id = ' . \CGlTransactionType::AP_CHARGE . '
					GROUP BY
						ad.gl_account_id,
						gat.name,
						gat.account_number,
						ad.ap_header_id,
						ah.post_month,
						ah.header_number
					
					UNION 
					
					SELECT
						gd.accrual_gl_account_id as gl_account_id,
						gat.name AS gl_account_name,
						gat.account_number,
						sum ( gd.amount ) AS transaction_amount,
					    gd.gl_header_id as ap_header_id,
					    date_part ( \'month\', gh.post_month) AS post_month,
					    gh.header_number :: text,
					    gh.post_month as invoice_date,
					    \'journal_entry\' as type
					FROM
					    gl_headers gh
					    JOIN gl_details gd ON ( gh.cid = gd.cid AND gh.id = gd.gl_header_id )
					    JOIN gl_account_trees gat ON ( gd.cid = gat.cid AND gd.accrual_gl_account_id = gat.gl_account_id AND gat.is_default = 1 )
					    JOIN property_gl_settings pgs ON( pgs.property_id = gd.property_id and pgs.cid = gd.cid )
					WHERE
					    date_part( \'year\', gh.post_month ) = ' . ( int ) $intYear . '
					    ' . $strGlHeaderWhereConditions . '
					    AND gd.property_id = ' . ( int ) $intPropertyId . '
					    AND gh.cid = ' . ( int ) $intCid . '
					    AND pgs.activate_standard_posting = true
					    AND gl_header_status_type_id = 1
					    AND gh.gl_transaction_type_id = 1
					GROUP BY
					    gd.accrual_gl_account_id,
					    gat.name,
					    gat.account_number,
					    gd.gl_header_id,
					    gh.post_month,
					    gh.header_number';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomApHeaderDetailsByAccountingExportBatchIdByCid( $intAccountingExportBatchId, $intCid, $objDatabase ) {
		if( false == valId( $intAccountingExportBatchId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						count(ah.id) as line_item_count,
						sum(ap.payment_amount) as transaction_amount
					FROM 
						accounting_export_batches aeb
						JOIN ap_headers ah ON (ah.accounting_export_batch_id = aeb.id and ah.cid = aeb.cid)
						JOIN ap_payments ap ON ( ah.ap_payment_id = ap.id and ah.cid = ap.cid )
					WHERE 
						aeb.cid = ' . ( int ) $intCid . '
						AND aeb.id = ' . ( int ) $intAccountingExportBatchId;

		$arrmixResults = fetchData( $strSql, $objDatabase );
		if( false == valArr( $arrmixResults ) ) {
			return false;
		}
		$arrmixResult['line_item_count']    = $arrmixResults[0]['line_item_count'];
		$arrmixResult['transaction_amount'] = $arrmixResults[0]['transaction_amount'];

		return $arrmixResult;
	}

	public static function fetchAllPoApHeadersByFilterByCid( $objApTransactionFilter, $intCid, $arrstrCompanyPreferences, $objClientDatabase ) {

		// Implementation phase. This function is currently only fetching standard purchase orders, to be modified for catalog item purchase orders.
		if( false == valObj( $objApTransactionFilter, 'CApTransactionsFilter' ) ) {
			return NULL;
		}

		$strWhereConditions = $strSubQueryWhereConditions = $strJoins = NULL;

		if( true == valId( $objApTransactionFilter->getPoApHeaderId() ) ) {
			$strWhereConditions .= ' AND ah.id = ' . $objApTransactionFilter->getPoApHeaderId();
		}

		switch( $objApTransactionFilter->getApHeaderSubTypeId() ) {

			case CApHeaderSubType::STANDARD_INVOICE:
				// All PO's

				if( false == valArr( $arrstrCompanyPreferences ) || 0 == $arrstrCompanyPreferences['ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS'] ) {

					$strSubQueryWhereConditions .= ' AND ad.subtotal_amount != 0.00 ';

				}
				$strWhereConditions .= ' AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_PURCHASE_ORDER . '
										 AND ah.ap_financial_status_type_id IN ( ' . CApFinancialStatusType::APPROVED . ' , ' . CApFinancialStatusType::PARTIALLY_INVOICED . ' ) ';
				break;

			case CApHeaderSubType::STANDARD_JOB_INVOICE:
				// Standard Job PO's

				if( false == valArr( $arrstrCompanyPreferences ) || 0 == $arrstrCompanyPreferences['ALLOW_MULTIPLE_INVOICES_FOR_STANDARD_PO_LINE_ITEMS'] ) {
					$strWhereConditions .= ' AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . '
											 AND ah.ap_financial_status_type_id IN ( ' . CApFinancialStatusType::APPROVED . ', ' . CApFinancialStatusType::PARTIALLY_INVOICED . ' ) ';
					$strSubQueryWhereConditions .= ' AND ad.subtotal_amount != 0.00 ';
				} else {
					$strWhereConditions .= ' AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::STANDARD_JOB_PURCHASE_ORDER . '
											 AND ah.ap_financial_status_type_id IN ( ' . CApFinancialStatusType::APPROVED . ' , ' . CApFinancialStatusType::CLOSED . ', ' . CApFinancialStatusType::PARTIALLY_INVOICED . ' ) ';
				}
				break;

			case CApHeaderSubType::CATALOG_INVOICE:
				$strWhereConditions .= ' AND ah.ap_header_sub_type_id = ' . CApHeaderSubType::CATALOG_PURCHASE_ORDER;

				$strJoins .= ' JOIN asset_transactions at ON at.cid = ad.cid AND at.po_ap_detail_id = ad.id AND at.invoice_ap_detail_id IS NULL AND at.asset_transaction_type_id = ' . CAssetTransactionType::RECEIVE_INVENTORY . '
								LEFT JOIN asset_transactions at_return ON at_return.cid = at.cid AND at_return.asset_transaction_id = at.id AND at_return.asset_transaction_type_id = ' . CAssetTransactionType::RETURN_INVENTORY;

				$strSubQueryWhereConditions .= ' AND at_return.id IS NULL';
				break;

			default:
//				add code if there is defaut condition code
				break;
		}

		$strWhereConditions .= ' AND EXISTS (
									SELECT
										1
									FROM
										ap_details ad
										' . $strJoins . '
									WHERE
										ad.cid = ' . ( int ) $intCid . '
										AND ad.deleted_on IS NULL
										AND ad.ap_header_id = ah.id
										' . $strSubQueryWhereConditions . '
									GROUP BY
										ad.ap_header_id
								)';

		$strSql = 'SELECT
					ah.id AS ap_header_id,
					ah.header_number as number,
					ah.ap_payee_account_id,
					ah.ap_payee_id,
					ah.bulk_property_id,
					ah.ap_payee_location_id,
					ah.ap_header_sub_type_id
				FROM
					ap_headers ah
				WHERE
					ah.cid = ' . ( int ) $intCid . '
					AND ah.ap_header_type_id = ' . CApHeaderType::PURCHASE_ORDER . '
					AND ah.reversal_ap_header_id IS NULL
					AND ah.deleted_by IS NULL
					AND ah.deleted_on IS NULL
					AND NOT ah.is_template
					' . $strWhereConditions . '
				ORDER BY
					ah.header_number ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

	public static function fetchPoApHeadersByHeaderNumbersByCidByApPayeeIdByApPayeeLocationIdByApPayeeAccountId( $arrstrPoNumbers, $intCid, $arrmixApHeader, $objClientDatabase ) {

		if( false == valArr( $arrmixApHeader ) || false == is_numeric( $arrmixApHeader['ap_payee_id'] ) || false == is_numeric( $arrmixApHeader['ap_payee_account_id'] ) || false == is_numeric( $arrmixApHeader['ap_payee_location_id'] ) || false == is_numeric( $arrmixApHeader['bulk_property_id'] ) ) {
			return false;
		}

		$strSql = 'SELECT
						ah.id
					FROM
						ap_headers ah
					WHERE
						ah.cid = ' . ( int ) $intCid . '
						AND ah.ap_header_type_id =' . CApHeaderType::PURCHASE_ORDER . '
						AND header_number in ( \'' . implode( '\', \'', $arrstrPoNumbers ) . '\' )
						AND ap_payee_id =' . ( int ) $arrmixApHeader['ap_payee_id'] . '
						AND ap_payee_account_id =' . $arrmixApHeader['ap_payee_account_id'] . '
						AND ap_payee_location_id =' . $arrmixApHeader['ap_payee_location_id'] . '
						AND bulk_property_id =' . $arrmixApHeader['bulk_property_id'] . '
						AND ah.reversal_ap_header_id IS NULL
						AND ah.deleted_by IS NULL
						AND ah.deleted_on IS NULL
						AND NOT ah.is_template
					ORDER BY
						ah.id ASC';

		return fetchData( $strSql, $objClientDatabase );
	}

}
?>