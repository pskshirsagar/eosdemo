<?php

class CFileExport extends CBaseFileExport {

	protected $m_strBatchName;
	protected $m_strArchiveFilePath;
	protected $m_strArchiveFileName;
	protected $m_strExportPath;
	Protected $m_strCreatedByFullName;
	protected $m_strSearchFilter;

	protected $m_intFileCount;

	public static $c_arrintSkipFileCountClientIds = [ CClient::ID_ASSET_CAMPUS_HOUSING, CClient::ID_LINCOLN_PROPERTY_COMPANY ];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileBatchId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsExported() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileExportTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileExportStatusId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExpiresOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExportedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExportedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeletedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    /**
     * Get Functions
     */

    public function getBatchName() {
    	return $this->m_strBatchName;
    }

	public function getSearchFilter() {
		return $this->m_strSearchFilter;
	}

    public function getArchiveFilePath() {
    	return $this->m_strArchiveFilePath;
    }

	public function getArchiveFileName() {
		return $this->m_strArchiveFileName;
	}

    public function getExportPath() {
    	return $this->m_strExportPath;
    }

    public function getCreatedByFullName() {
    	return $this->m_strCreatedByFullName;
    }

    public function getExportDetails() {

    	$strExportDetails = parent::getExportDetails();

    	switch( $this->getFileExportTypeId() ) {
    		case CFileExportType::OTHER_ESIGN_DOCUMENT_EXPORT:
    			$arrmixExportDetails = json_decode( $strExportDetails );
    			break;

    		default:
    			$arrmixExportDetails = $strExportDetails;
    			break;
    	}

    	return $arrmixExportDetails;

    }

	public function getFileCount() {
		return $this->m_intFileCount;
	}

    /**
     * Set Functions
     */

    public function setBatchName( $strBatchName ) {
    	$this->m_strBatchName = $strBatchName;
    }

	public function setSearchFilter( $strSearchFilter ) {
		$this->m_strSearchFilter = CStrings::strTrimDef( $strSearchFilter, -1, NULL, true );
	}

    public function setArchiveFilePath( $strArchiveFilePath ) {
    	$this->m_strArchiveFilePath = $strArchiveFilePath;
    }

	public function setArchiveFileName( $strArchiveFileName ) {
		$this->m_strArchiveFileName = $strArchiveFileName;
	}

    public function setExportPath( $strExportPath ) {
    	$this->m_strExportPath = $strExportPath;
    }

    public function setCreatedByFullName( $strCreatedByFullName ) {
    	$this->m_strCreatedByFullName = $strCreatedByFullName;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

    	if( true == isset( $arrmixValues['batch_name'] ) )			$this->setBatchName( $arrmixValues['batch_name'] );
	    if( true == isset( $arrmixValues['search_filter'] ) )		$this->setSearchFilter( $arrmixValues['search_filter'] );
		if( true == isset( $arrmixValues['archive_file_path'] ) )	$this->setArchiveFilePath( $arrmixValues['archive_file_path'] );
	    if( true == isset( $arrmixValues['archive_file_name'] ) )	$this->setArchiveFileName( $arrmixValues['archive_file_name'] );
    	if( true == isset( $arrmixValues['export_path'] ) )			$this->setExportPath( $arrmixValues['export_path'] );
    	if( true == isset( $arrmixValues['created_by_fullname'] ) ) $this->m_strCreatedByFullName = ( $arrmixValues['created_by_fullname'] );
		if( true == isset( $arrmixValues['file_count'] ) )          $this->m_intFileCount = ( $arrmixValues['file_count'] );

    	return;
    }

	public function setFileCount( $intFileCount ) {
		$this->m_intFileCount = $intFileCount;
	}

    public function setExportDetails( $arrmixExportDetails ) {

    	if( false == valArr( $arrmixExportDetails ) ) {
    		parent::setExportDetails( $arrmixExportDetails );
    		return;
    	}

    	switch( $this->getFileExportTypeId() ) {
    		case CFileExportType::OTHER_ESIGN_DOCUMENT_EXPORT:
    			$strExportDetails = json_encode( $arrmixExportDetails );
    			break;

    		default:
    			$strExportDetails = $arrmixExportDetails;
    			break;
    	}

    	parent::setExportDetails( $strExportDetails );
    }

}
?>