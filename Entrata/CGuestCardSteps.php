<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CGuestCardSteps
 * Do not add any new functions to this class.
 */

class CGuestCardSteps extends CBaseGuestCardSteps {

	public static function fetchGuestCardStep( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGuestCardStep', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>