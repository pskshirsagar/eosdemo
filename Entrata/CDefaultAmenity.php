<?php

class CDefaultAmenity extends CBaseDefaultAmenity {

	const PARKING						= 116;
	const BREED_RESTRICTIONS			= 117;
	const RECENT_REMODEL			    = 120;
	const GENERAL_PET_POLICY			= 118;
	const NOT_CATEGORIZED_COMMUNITY		= 1000;
	const NOT_CATEGORIZED_APARTMENT		= 1001;

	const MOBILITY_IMPAIRED				= 136;
	const HEARING_IMPAIRED				= 137;
	const VISUALLY_IMPAIRED				= 138;

	public static $c_arrintDefaultAmenityIds = [
		self::MOBILITY_IMPAIRED,
		self::HEARING_IMPAIRED,
		self::VISUALLY_IMPAIRED
	];

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAmenityTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valInternalDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsMits() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public static function getApprovedVacancyFilterIds() {
		return [
			1,2,3,4,5,6,7,8,9,
			10,11,12,16,17,18,19,
			20,21,22,23,24,25,26,27,28,
			30,31,32,33,34,36,
			43,45,46,47,48,49,
			51,53,54,55,56,58,
			63,64,65,66,67,68,69,
			70,71,72,77,
			80,81,82,83,85,87,
			90,91,
		];
	}

	public function getFormattedName() {
		if( true == is_null( $this->getName() ) || 0 == strlen( $this->getName() ) ) {
			return NULL;
		}

		$strAmenityName = $this->getName();

		\Psi\CStringService::singleton()->preg_match_all( '/[A-Z0-9]+/', $strAmenityName, $arrstrOut );

		$arrstrOut = array_pop( $arrstrOut );

		if( true == valArr( $arrstrOut ) ) {

			if( 1 == strlen( $arrstrOut[0] ) ) {
				unset( $arrstrOut[0] );
			} elseif( 2 < strlen( $arrstrOut[0] ) ) {
				$strOut  		= $arrstrOut[0];
				$arrstrOut[0] 	= $strOut[( strlen( $strOut ) - 1 )];
			}

			foreach( $arrstrOut as $strOut ) {
				$arrstrPattern[] 		= '/' . $strOut . '/';
				$arrstrReplacements[] 	= ' ' . $strOut;
			}

			if( true == isset( $arrstrPattern ) && true == isset( $arrstrReplacements ) ) {
				$strAmenityName = \Psi\CStringService::singleton()->preg_replace( $arrstrPattern, $arrstrReplacements, $strAmenityName );
			}

			$strAmenityName = \Psi\CStringService::singleton()->preg_replace( '/\s+/i', ' ', $strAmenityName );
		}

		return $strAmenityName;
	}

	public function getUrlFormattedName() {
		return str_replace( ' ', '-', trim( \Psi\CStringService::singleton()->strtolower( $this->getFormattedName() ) ) );
	}

	public static function parseUrlFormattedName( $urlFormattedName ) {
		return str_replace( '-', '', \Psi\CStringService::singleton()->strtolower( $urlFormattedName ) );
	}

}
?>