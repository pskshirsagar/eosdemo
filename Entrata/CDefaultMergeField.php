<?php

class CDefaultMergeField extends CBaseDefaultMergeField {

	protected $m_arrobjDefaultMergeFields;
	protected $m_strFieldValue;
	protected $m_strMergeFieldGroupName;

	protected $m_boolCheckMergeFieldSubGroup;

	const APPLICANT_INITIAL 	= 'applicant_initial';
	const APPLICANT_SIGNATURE 	= 'applicant_signature';

	const PRIMARY_APPLICANT_INITIAL 	= 'primary_applicant_initial';
	const PRIMARY_APPLICANT_SIGNATURE 	= 'primary_applicant_signature';

	const FIRST_CO_APPLICANT_INITIAL 	= 'first_co_applicant_initial';
	const FIRST_CO_APPLICANT_SIGNATURE 	= 'first_co_applicant_signature';
	const SECOND_CO_APPLICANT_INITIAL 	= 'second_co_applicant_initial';
	const SECOND_CO_APPLICANT_SIGNATURE = 'second_co_applicant_signature';
	const THIRD_CO_APPLICANT_INITIAL 	= 'third_co_applicant_initial';
	const THIRD_CO_APPLICANT_SIGNATURE 	= 'third_co_applicant_signature';
	const FOURTH_CO_APPLICANT_INITIAL 	    = 'fourth_co_applicant_initial';
	const FOURTH_CO_APPLICANT_SIGNATURE     = 'fourth_co_applicant_signature';
	const FIFTH_CO_APPLICANT_INITIAL 	    = 'fifth_co_applicant_initial';
	const FIFTH_CO_APPLICANT_SIGNATURE 	    = 'fifth_co_applicant_signature';
	const SIXTH_CO_APPLICANT_INITIAL 	    = 'sixth_co_applicant_initial';
	const SIXTH_CO_APPLICANT_SIGNATURE 	    = 'sixth_co_applicant_signature';
	const SEVENTH_CO_APPLICANT_INITIAL 	    = 'seventh_co_applicant_initial';
	const SEVENTH_CO_APPLICANT_SIGNATURE    = 'seventh_co_applicant_signature';
	const EIGHTH_CO_APPLICANT_INITIAL 	    = 'eighth_co_applicant_initial';
	const EIGHTH_CO_APPLICANT_SIGNATURE     = 'eighth_co_applicant_signature';
	const NINTH_CO_APPLICANT_INITIAL 	    = 'ninth_co_applicant_initial';
	const NINTH_CO_APPLICANT_SIGNATURE 	    = 'ninth_co_applicant_signature';
	const TENTH_CO_APPLICANT_INITIAL 	    = 'tenth_co_applicant_initial';
	const TENTH_CO_APPLICANT_SIGNATURE 	    = 'tenth_co_applicant_signature';
	const ELEVENTH_CO_APPLICANT_INITIAL     = 'eleventh_co_applicant_initial';
	const ELEVENTH_CO_APPLICANT_SIGNATURE 	= 'eleventh_co_applicant_signature';
	const TWELFTH_CO_APPLICANT_INITIAL 	    = 'twelfth_co_applicant_initial';
	const TWELFTH_CO_APPLICANT_SIGNATURE 	= 'twelfth_co_applicant_signature';

	const FIRST_GUARANTOR_INITIAL 		    = 'first_guarantor_initial';
	const FIRST_GUARANTOR_SIGNATURE 	    = 'first_guarantor_signature';
	const SECOND_GUARANTOR_INITIAL 		    = 'second_guarantor_initial';
	const SECOND_GUARANTOR_SIGNATURE 	    = 'second_guarantor_signature';
	const THIRD_GUARANTOR_INITIAL 		    = 'third_guarantor_initial';
	const THIRD_GUARANTOR_SIGNATURE 	    = 'third_guarantor_signature';

	const PROPERTY_SIGNATURE_INITIAL 	    = 'property_signature_initial';
	const PROPERTY_SIGNATURE_FULL 	        = 'property_signature_full';
	const PROPERTY_MANAGER_SIGNATURE_DETAILS 	= 'property_manager_signature_details';

	const INPUT_TYPE_RADIO 	            = 'radio';
	const INPUT_TYPE_CHECKBOX 		    = 'checkbox';
	const INPUT_TYPE_DROPDOWN 	        = 'dropdown';
	const INPUT_TYPE_TEXTAREA 		    = 'textarea';
	const INPUT_TYPE_DATE   	        = 'date';
	const INPUT_TYPE_CURRENCY 		    = 'currency';
	const INPUT_TYPE_IMAGE   		    = 'image';

	public static $c_arrstrMemberLevelMergeFields = [
		self::APPLICANT_INITIAL 	=> 'applicant_initial',
		self::APPLICANT_SIGNATURE 	=> 'applicant_signature'
	];

	public static $c_arrstrResponsibleApplicantSignatureMergeFields = [
		self::PRIMARY_APPLICANT_INITIAL 	    => 'primary_applicant_initial',
		self::PRIMARY_APPLICANT_SIGNATURE 	    => 'primary_applicant_signature',
		self::FIRST_CO_APPLICANT_INITIAL 	    => 'first_co_applicant_initial',
		self::FIRST_CO_APPLICANT_SIGNATURE 	    => 'first_co_applicant_signature',
		self::SECOND_CO_APPLICANT_INITIAL 	    => 'second_co_applicant_initial',
		self::SECOND_CO_APPLICANT_SIGNATURE     => 'second_co_applicant_signature',
		self::THIRD_CO_APPLICANT_INITIAL 	    => 'third_co_applicant_initial',
		self::THIRD_CO_APPLICANT_SIGNATURE 	    => 'third_co_applicant_signature',
		self::FOURTH_CO_APPLICANT_INITIAL 	    => 'fourth_co_applicant_initial',
		self::FOURTH_CO_APPLICANT_SIGNATURE     => 'fourth_co_applicant_signature',
		self::FIFTH_CO_APPLICANT_INITIAL 	    => 'fifth_co_applicant_initial',
		self::FIFTH_CO_APPLICANT_SIGNATURE 	    => 'fifth_co_applicant_signature',
		self::SIXTH_CO_APPLICANT_INITIAL 	    => 'sixth_co_applicant_initial',
		self::SIXTH_CO_APPLICANT_SIGNATURE 	    => 'sixth_co_applicant_signature',
		self::SEVENTH_CO_APPLICANT_INITIAL 	    => 'seventh_co_applicant_initial',
		self::SEVENTH_CO_APPLICANT_SIGNATURE    => 'seventh_co_applicant_signature',
		self::EIGHTH_CO_APPLICANT_INITIAL 	    => 'eighth_co_applicant_initial',
		self::EIGHTH_CO_APPLICANT_SIGNATURE     => 'eighth_co_applicant_signature',
		self::NINTH_CO_APPLICANT_INITIAL 	    => 'ninth_co_applicant_initial',
		self::NINTH_CO_APPLICANT_SIGNATURE 	    => 'ninth_co_applicant_signature',
		self::TENTH_CO_APPLICANT_INITIAL 	    => 'tenth_co_applicant_initial',
		self::TENTH_CO_APPLICANT_SIGNATURE 	    => 'tenth_co_applicant_signature',
		self::ELEVENTH_CO_APPLICANT_INITIAL     => 'eleventh_co_applicant_initial',
		self::ELEVENTH_CO_APPLICANT_SIGNATURE 	=> 'eleventh_co_applicant_signature',
		self::TWELFTH_CO_APPLICANT_INITIAL 	    => 'twelfth_co_applicant_initial',
		self::TWELFTH_CO_APPLICANT_SIGNATURE 	=> 'twelfth_co_applicant_signature'
	];

	public static $c_arrstrGuarantorSignatureMergeFields = [
		self::FIRST_GUARANTOR_INITIAL 		=> 'first_guarantor_initial',
		self::FIRST_GUARANTOR_SIGNATURE 	=> 'first_guarantor_signature',
		self::SECOND_GUARANTOR_INITIAL 		=> 'second_guarantor_initial',
		self::SECOND_GUARANTOR_SIGNATURE 	=> 'second_guarantor_signature',
		self::THIRD_GUARANTOR_INITIAL 		=> 'third_guarantor_initial',
		self::THIRD_GUARANTOR_SIGNATURE 	=> 'third_guarantor_signature'
	];

	public static $c_arrstrHouseHoldAllApplicantSignatureMergeFields = [
		self::PRIMARY_APPLICANT_INITIAL 	    => 'primary_applicant_initial',
		self::PRIMARY_APPLICANT_SIGNATURE 	    => 'primary_applicant_signature',
		self::FIRST_CO_APPLICANT_INITIAL 	    => 'first_co_applicant_initial',
		self::FIRST_CO_APPLICANT_SIGNATURE 	    => 'first_co_applicant_signature',
		self::SECOND_CO_APPLICANT_INITIAL 	    => 'second_co_applicant_initial',
		self::SECOND_CO_APPLICANT_SIGNATURE     => 'second_co_applicant_signature',
		self::THIRD_CO_APPLICANT_INITIAL 	    => 'third_co_applicant_initial',
		self::THIRD_CO_APPLICANT_SIGNATURE 	    => 'third_co_applicant_signature',
		self::FIRST_GUARANTOR_INITIAL 		    => 'first_guarantor_initial',
		self::FIRST_GUARANTOR_SIGNATURE 	    => 'first_guarantor_signature',
		self::SECOND_GUARANTOR_INITIAL 		    => 'second_guarantor_initial',
		self::SECOND_GUARANTOR_SIGNATURE 	    => 'second_guarantor_signature',
		self::THIRD_GUARANTOR_INITIAL 		    => 'third_guarantor_initial',
		self::THIRD_GUARANTOR_SIGNATURE 	    => 'third_guarantor_signature',
		self::FOURTH_CO_APPLICANT_INITIAL 	    => 'fourth_co_applicant_initial',
		self::FOURTH_CO_APPLICANT_SIGNATURE     => 'fourth_co_applicant_signature',
		self::FIFTH_CO_APPLICANT_INITIAL 	    => 'fifth_co_applicant_initial',
		self::FIFTH_CO_APPLICANT_SIGNATURE 	    => 'fifth_co_applicant_signature',
		self::SIXTH_CO_APPLICANT_INITIAL 	    => 'sixth_co_applicant_initial',
		self::SIXTH_CO_APPLICANT_SIGNATURE 	    => 'sixth_co_applicant_signature',
		self::SEVENTH_CO_APPLICANT_INITIAL 	    => 'seventh_co_applicant_initial',
		self::SEVENTH_CO_APPLICANT_SIGNATURE    => 'seventh_co_applicant_signature',
		self::EIGHTH_CO_APPLICANT_INITIAL 	    => 'eighth_co_applicant_initial',
		self::EIGHTH_CO_APPLICANT_SIGNATURE     => 'eighth_co_applicant_signature',
		self::NINTH_CO_APPLICANT_INITIAL 	    => 'ninth_co_applicant_initial',
		self::NINTH_CO_APPLICANT_SIGNATURE 	    => 'ninth_co_applicant_signature',
		self::TENTH_CO_APPLICANT_INITIAL 	    => 'tenth_co_applicant_initial',
		self::TENTH_CO_APPLICANT_SIGNATURE 	    => 'tenth_co_applicant_signature',
		self::ELEVENTH_CO_APPLICANT_INITIAL     => 'eleventh_co_applicant_initial',
		self::ELEVENTH_CO_APPLICANT_SIGNATURE 	=> 'eleventh_co_applicant_signature',
		self::TWELFTH_CO_APPLICANT_INITIAL 	    => 'twelfth_co_applicant_initial',
		self::TWELFTH_CO_APPLICANT_SIGNATURE 	=> 'twelfth_co_applicant_signature'
	];

	public static $c_arrstrPropertySignatureMergeFields = [
	    self::PROPERTY_SIGNATURE_INITIAL 	=> 'property_signature_initial',
	    self::PROPERTY_SIGNATURE_FULL 	    => 'property_signature_full',
		self::PROPERTY_MANAGER_SIGNATURE_DETAILS => 'property_manager_signature_details'
	];

	/**
	 * Create Functions
	 */

	public function createDocumentMergeField() {

		$objDocumentMergeField = new CDocumentMergeField();

		$objDocumentMergeField->setDefaultMergeFieldId( $this->m_intId );
		$objDocumentMergeField->setField( $this->m_strField );
		$objDocumentMergeField->setDataType( $this->m_strDataType );
		$objDocumentMergeField->setMergeFieldGroupId( $this->getMergeFieldGroupId() );
		$objDocumentMergeField->setAllowUserUpdate( $this->m_intAllowUserUpdate );
		$objDocumentMergeField->setAllowAdminUpdate( $this->m_intAllowAdminUpdate );
		$objDocumentMergeField->setForceOriginUpdate( $this->m_intAllowOriginUpdate );
		$objDocumentMergeField->setShowInMergeDisplay( $this->m_intShowInMergeDisplay );
		$objDocumentMergeField->setIsBlockField( $this->m_intIsBlockField );
		$objDocumentMergeField->setIsRequired( $this->m_intIsRequired );

		return $objDocumentMergeField;
	}

	/**
	 * Get Functions
	 */

	public function getDefaultMergeFields() {
		return $this->m_arrobjDefaultMergeFields;
	}

	public function getFieldValue() {
		return $this->m_strFieldValue;
	}

	public function getMergeFieldGroupName() {
		return $this->m_strMergeFieldGroupName;
	}

	public function getCheckMergeFieldSubGroup() {
		return $this->m_boolCheckMergeFieldSubGroup;
	}

	/**
	 * Set Functions
	 */

	public function setFieldValue( $strFieldValue ) {
		$this->m_strFieldValue = $strFieldValue;
	}

	public function setMergeFieldGroupName( $strMergeFieldGroupName ) {
		$this->m_strMergeFieldGroupName = $strMergeFieldGroupName;
	}

	public function setCheckMergeFieldSubGroup( $boolCheckMergeFieldSubGroup ) {
		$this->m_boolCheckMergeFieldSubGroup = $boolCheckMergeFieldSubGroup;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['merge_field_group_name'] ) ) {
			$this->setMergeFieldGroupName( $arrmixValues['merge_field_group_name'] );
		}
	}

     /**
      * Validate Functions
      */

	public function valField( $objDatabase ) {
		$boolIsValid = true;
		if( true == is_null( $this->getField() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Field is required.' ) );
		}
		if( true == $boolIsValid ) {
			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			if( true == is_null( $this->getBlockDefaultMergeFieldId() ) ) {
				$strSql = " WHERE block_default_merge_field_id IS NULL AND field ILIKE '" . trim( addslashes( $this->getField() ) ) . "'" . $strSqlCondition;
			} else {
				$strSql = ' WHERE block_default_merge_field_id =' . $this->getBlockDefaultMergeFieldId() . " AND field ILIKE '" . trim( addslashes( $this->getField() ) ) . "'" . $strSqlCondition;
			}

			$intCount = CDefaultMergeFields::fetchDefaultMergeFieldCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'field', 'Name already Exist.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );
		}
		return $boolIsValid;
	}

	public function valMergeFieldTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getMergeFieldTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merge_field_type_id', 'Merge Field Type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDataType() {
		$boolIsValid = true;
		if( true == is_null( $this->getDataType() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'data_type', 'Data Type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valMergeFieldGroupId() {
		$boolIsValid = true;
		if( true == is_null( $this->getMergeFieldGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merge_field_group_id', 'Merge Field Group is required.' ) );
		}
		return $boolIsValid;
	}

	public function valMergeFieldSubGroupId() {
		$boolIsValid = true;
		if( true == is_null( $this->getMergeFieldSubGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merge_field_sub_group_id', 'Merge Field Subgroup is required.' ) );
		}
		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
	        case VALIDATE_INSERT:
		        $boolIsValid &= $this->valField( $objDatabase );
		        $boolIsValid &= $this->valTitle();
		        break;

	        case VALIDATE_UPDATE:
	            $boolIsValid &= $this->valField( $objDatabase );
	            $boolIsValid &= $this->valMergeFieldTypeId();
	            $boolIsValid &= $this->valDataType();
	            $boolIsValid &= $this->valMergeFieldGroupId();
		        $boolIsValid &= $this->valTitle();
		        if( true === $this->getCheckMergeFieldSubGroup() ) {
	            	$boolIsValid &= $this->valMergeFieldSubGroupId();
				}
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

     /**
      * Other Functions
      */

    public function addDefaultMergeField( $objDefaultMergeField ) {
    	$this->m_arrobjDefaultMergeFields[$objDefaultMergeField->getId()] = $objDefaultMergeField;
    }

    public function fetchDropDownListItems( $objPrimaryObject, $objDatabase = NULL ) {
        if( $objDatabase == NULL ) {
            $objDatabase = $this->m_objDatabase;
        }

        $arrstrViewExpressionOptions = json_decode( json_encode( $this->getViewExpression() ), true )['options'];
        $strSql = $arrstrViewExpressionOptions['query'];

        $arrmixFilterValues = [];
        foreach( $arrstrViewExpressionOptions['parameters'] as $strMethod ) {
            $arrmixFilterValues[] = array_reduce( explode( '.', $strMethod ), function( $objObject, $strCustomMethod ) {
                return $objObject->$strCustomMethod();
            }, $objPrimaryObject );
        }

        $strSql = vsprintf( $strSql, $arrmixFilterValues );

        return fetchData( $strSql, $objDatabase );
    }

    public function fetchMergeSubFields( $objDatabase = NULL ) {
        if( true == is_null( $objDatabase ) ) {
        	$objDatabase = $this->m_objDatabase;
		}

        if( $this->getMergeFieldTypeId() != CMergeFieldType::BLOCK_MERGE_FIELD ) {
        	return false;
		}

        return CDefaultMergeFields::fetchDefaultMergeFieldsByBlockDefaultMergeFieldId( $this->getId(), $objDatabase );
    }

    public function fetchValue( $objPrimaryObject, $objDatabase = NULL ) {
        if( true == isset( $this->m_strFieldValue ) ) {
        	return $this->m_strFieldValue;
		}

        switch( $this->getMergeFieldTypeId() ) {
            case CMergeFieldType::REFERENCE_MERGE_FIELD:
                return $this->fetchReferenceMergeFieldValue( $objPrimaryObject, $objDatabase );
                break;
            case CMergeFieldType::COMPOSITE_MERGE_FIELD:
                return $this->fetchCompositeMergeFieldValue( $objPrimaryObject, $objDatabase );
                break;
            case CMergeFieldType::DROPDOWN_MERGE_FIELD:
                return $this->fetchDropDownListItems( $objPrimaryObject, $objDatabase );
                break;
            case CMergeFieldType::MATH_MERGE_FIELD:
                return $this->fetchMathMergeFieldValue( $objPrimaryObject, $objDatabase );
                break;
	        case CMergeFieldType::METHOD_MERGE_FIELD:
		        return $this->fetchMethodMergeFieldValue( $objPrimaryObject );
		        break;
            default:
                return;
                break;
        }
    }

	public function fetchMethodMergeFieldValue( $objPrimaryObject ) {
		$this->m_strFieldValue = '';

		if( '' != $this->getViewExpression() ) {
			$strMethodName = json_decode( json_encode( $this->getViewExpression() ), true )['value'];
			if( false == valObj( $objPrimaryObject, 'stdClass' ) && '' != $strMethodName && true == method_exists( $objPrimaryObject, $strMethodName ) ) {
				$this->m_strFieldValue = $objPrimaryObject->$strMethodName();
			} elseif( true == property_exists( $objPrimaryObject, 'agent_access_password_reset_link' ) ) {
				$this->m_strFieldValue = $objPrimaryObject->agent_access_password_reset_link;
			}
		}
		return $this->m_strFieldValue;
	}

    public function fetchReferenceMergeFieldValue( $objPrimaryObject, $objDatabase = NULL ) {
		if( true == is_null( $objDatabase ) ) {
			$objDatabase = $this->m_objDatabase;
		}

		$arrstrViewExpressionValue = json_decode( json_encode( $this->getViewExpression() ), true )['value'];

		if( false == valArr( $arrstrViewExpressionValue ) ) {
			return '';
		}

		$strSql = '';

		if( true == valStr( $arrstrViewExpressionValue['select'] ) ) {
			$strSql = 'SELECT ' . $arrstrViewExpressionValue['select'];
		}

		if( true == valStr( $arrstrViewExpressionValue['from'] ) ) {
			$strSql .= ' FROM ' . $arrstrViewExpressionValue['from'];
		}

		if( true == valStr( $arrstrViewExpressionValue['where'] ) ) {
			$strSql .= ' WHERE ' . $arrstrViewExpressionValue['where'];
		}

		$arrmixFilterValues = [];
		foreach( $arrstrViewExpressionValue['parameters'] as $strMethod ) {
			$arrmixFilterValues[] = array_reduce( explode( '.', $strMethod ), function( $objObject, $strCustomMethod ) {
				return $objObject->$strCustomMethod();
			}, $objPrimaryObject );
		}

		$arrmixFieldValue = [];

		if( true == valStr( $strSql ) ) {
			$strSql = vsprintf( $strSql, $arrmixFilterValues );
			$arrmixFieldValue = fetchData( $strSql, $objDatabase );
		}

		if( true != valArr( $arrmixFieldValue ) ) {
			return '';
		}

		$this->m_strFieldValue = $arrmixFieldValue[0][$this->getField()];

		return $this->getFormatedValue();
    }

    public function getFormatedValue() {
		if( 0 >= strlen( $this->m_strFieldValue ) ) {
			return $this->m_strFieldValue;
		}

    	switch( $this->getDataType() ) {
			case 'numeric':
				$strFieldValue = __( '{%f, 0}', [ $this->m_strFieldValue ] );
				break;

			case 'currency':
				$strFieldValue = __( '{%m, 0}', [ $this->m_strFieldValue ] );
				break;

			case 'phone_number':
				$strFieldValue = __( '{%h, 0}', [ $this->m_strFieldValue ] );
				break;

			case 'datetime':
				$strFieldValue = __( '{%t, 0, DATE_NUMERIC_STANDARD}', [ $this->m_strFieldValue ] );
				break;

			case 'time':
				$strFieldValue = __( '{%t, 0, TIME_SHORT}', [ $this->m_strFieldValue ] );
				break;

			default:
				$strFieldValue = $this->m_strFieldValue;
		}

		return $strFieldValue;
	}

    public function fetchFilteredBlockMergeSubFieldValues( $objBlockPrimaryObject, $arrstrFilterParameters = [], $objDatabase = NULL ) {
        if( true == is_null( $objDatabase ) ) {
        	$objDatabase = $this->m_objDatabase;
		}

        if( $this->getMergeFieldTypeId() != CMergeFieldType::BLOCK_MERGE_FIELD || false == isset( $objBlockPrimaryObject ) || false == valArr( $arrstrFilterParameters ) ) {
        	return false;
		}

        $arrobjMergeSubFields = $this->fetchMergeSubFields();

        $arrobjUnfilteredObjects = $this->fetchUnfilteredBlockMergeFieldObjects( $objBlockPrimaryObject, $objDatabase );

        foreach( $arrstrFilterParameters as $strMergeField => $strValue ) {
            if( false == valArr( $arrobjUnfilteredObjects ) ) {
            	return [];
			}

            $arrobjUnfilteredObjects = array_filter( $arrobjUnfilteredObjects, function( $objPrimaryObject ) use ( $strValue, $strMergeField, $arrobjMergeSubFields, $objDatabase ) {
                return $arrobjMergeSubFields[$strMergeField]->fetchValue( $objPrimaryObject, $objDatabase ) == $strValue;
            } );
        }

        $arrmixFilteredMergeFieldValues = array_reduce( $arrobjUnfilteredObjects, function( $arrmixValues, $objPrimaryObject ) use ( $arrobjMergeSubFields, $objDatabase ) {
            $arrmixValues[$objPrimaryObject->getId()] = array_reduce( $arrobjMergeSubFields, function( $arrmixValues, $objMergeField ) use ( $objPrimaryObject, $objDatabase ) {
                $arrmixValues[$objMergeField->getField()] = $objMergeField->fetchValue( $objPrimaryObject, $objDatabase );
                return $arrmixValues;
            } );
            return $arrmixValues;
        } );

        return $arrmixFilteredMergeFieldValues;

    }

    public function fetchUnfilteredBlockMergeFieldObjects( $objBlockPrimaryObject, $objDatabase = NULL ) {
        if( true == is_null( $objDatabase ) ) {
        	$objDatabase = $this->m_objDatabase;
		}

        if( $this->getMergeFieldTypeId() != CMergeFieldType::BLOCK_MERGE_FIELD ) {
        	return false;
		}

        $arrstrViewExpressionObjects = json_decode( json_encode( $this->getViewExpression() ), true )['objects'];
        $strSql = $arrstrViewExpressionObjects['query'];

        $arrmixFilterValues = [];
        foreach( $arrstrViewExpressionObjects['parameters'] as $strMethod ) {
            $arrmixFilterValues[] = array_reduce( explode( '.', $strMethod ), function( $objObject, $strCustomMethod ) {
                return $objObject->$strCustomMethod();
            }, $objBlockPrimaryObject );
        }

        $strSql = vsprintf( $strSql, $arrmixFilterValues );

        return $this->fetchObjects( $strSql, $arrstrViewExpressionObjects['className'], $objDatabase );

    }

    public function fetchCompositeMergeFieldValue( $objPrimaryObject, $objDatabase = NULL ) {
        if( true == is_null( $objDatabase ) ) {
        	$objDatabase = $this->m_objDatabase;
		}
        $this->m_strFieldValue = $this->constructCompositeMergeField( $objPrimaryObject, $objDatabase );
        return $this->m_strFieldValue;
    }

    public function constructCompositeMergeField( $objPrimaryObject, $objDatabase = NULL ) {
        if( true == is_null( $objDatabase ) ) {
        	$objDatabase = $this->m_objDatabase;
		}

        $strCompositionExpression = json_decode( json_encode( $this->getViewExpression() ), true )['value'];
        $arrstrMergeFields = self::splitCompositeMergeFields( $strCompositionExpression );
        $arrobjMergeFields = CDefaultMergeFields::fetchDefaultMergeFieldsByFieldsKeyedByFields( $arrstrMergeFields, $objDatabase );

        if( true == valArr( $arrobjMergeFields ) ) {
            foreach( $arrobjMergeFields as $strMergeField => $objMergeField ) {
                $strCompositionExpression = \Psi\CStringService::singleton()->str_ireplace( '~^~' . $strMergeField . '^~^', $objMergeField->fetchValue( $objPrimaryObject, $objDatabase ), $strCompositionExpression );
            }
        }

        return $strCompositionExpression;
    }

    public static function splitCompositeMergeFields( $strCompositionExpression ) {
        if( true == is_null( $strCompositionExpression ) ) {
        	return NULL;
		}

        preg_match_all( '/~\^~[a-zA-Z]\w*\^~\^/', $strCompositionExpression, $arrstrCompositionMergeFields );
        $arrstrCompositionMergeFields = array_reduce( $arrstrCompositionMergeFields, function( $arrmixCarry, $strMergeField ) {
            $strMergeField = \Psi\CStringService::singleton()->str_ireplace( '~', '', $strMergeField );
            $strMergeField = \Psi\CStringService::singleton()->str_ireplace( '^', '', $strMergeField );
			$arrmixCarry[] = $strMergeField;
            return $arrmixCarry;
        }, [] );

        return $arrstrCompositionMergeFields;
    }

    public function fetchMathMergeFieldValue( $objPrimaryObject, $objDatabase = NULL ) {
        if( true == is_null( $objDatabase ) ) {
        	$objDatabase = $this->m_objDatabase;
		}
        $this->m_strFieldValue = $this->calculateMathMergeFieldValue( $objPrimaryObject, $objDatabase );
        return $this->m_strFieldValue;
    }

    public function calculateMathMergeFieldValue( $objPrimaryObject, $objDatabase = NULL, $boolRoundDecimalPoint = true ) {
        if( true == is_null( $objDatabase ) ) {
        	$objDatabase = $this->m_objDatabase;
		}

        $strMathExpression = json_decode( json_encode( $this->getViewExpression() ), true )['value'];
        $arrstrMergeFields = self::splitFormulaMergeFields( $strMathExpression );
        $arrobjMergeFields = CDefaultMergeFields::fetchDefaultMergeFieldsByFieldsKeyedByFields( $arrstrMergeFields, $objDatabase );

        if( true == valArr( $arrobjMergeFields ) ) {
            foreach( $arrobjMergeFields as $strMergeField => $objMergeField ) {
                $strMathExpression = \Psi\CStringService::singleton()->str_ireplace( $strMergeField, $objMergeField->fetchValue( $objPrimaryObject, $objDatabase ), $strMathExpression );
            }
        }

        if( true == CValidation::validateSimpleMathExpression( $strMathExpression ) ) {
            eval( '$strExpressionValue = ' . $strMathExpression . ';' );
            if( true == is_numeric( $strExpressionValue ) ) {
                if( false == $boolRoundDecimalPoint ) {
                    $strVal = number_format( str_replace( ',', '', $strExpressionValue ) );
                } else {
                    $strVal = number_format( str_replace( ',', '', $strExpressionValue ), 2 );
                }
            }
        }

        return $strVal;
    }

    public static function splitFormulaMergeFields( $strMathExpression ) {

        if( true == is_null( $strMathExpression ) ) {
        	return NULL;
		}

        preg_match_all( '/\w*/', $strMathExpression, $arrstrFormulaMergeFields );

        if( true == valArr( $arrstrFormulaMergeFields[0] ) ) {
            foreach( $arrstrFormulaMergeFields[0] as $strFormulaMergeField ) {
                if( false == is_numeric( $strFormulaMergeField ) && true == valStr( $strFormulaMergeField ) ) {
                    $arrstrMergeFields[] = $strFormulaMergeField;
                }
            }
        }

        return $arrstrMergeFields;
    }

}
?>