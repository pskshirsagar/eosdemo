<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Entrata\CReportInstances
 * Do not add any new functions to this class.
 */

class CReportInstances extends CBaseReportInstances {

	/**
	 * @param $arrintIds
	 * @param $intCid
	 * @param $objDatabase
	 * @return CReportInstance[]
	 */
	public static function fetchReportInstancesByIdsByCid( $arrintIds, $intCid, $objDatabase ) {
		if( true == valArr( $arrintIds ) ) {
			return self::fetchReportInstances( sprintf( 'SELECT * FROM report_instances WHERE id IN ( %s ) AND cid = %d ORDER BY name, id', implode( $arrintIds, ',' ), ( int ) $intCid ), $objDatabase );
		} else {
			return [];
		}
	}

	public static function fetchReportInstancesByReportGroupIdsByCid( $arrintReportGroupIds, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				*
			FROM
				report_instances
			WHERE
				cid = ' . ( int ) $intCid . '
				AND report_group_id IN ( ' . implode( ',', $arrintReportGroupIds ) . ' )';

		return self::fetchReportInstances( $strSql, $objDatabase );
	}

	public static function fetchReportInstanceByReportIdByCid( $intReportId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				ri.id as report_instance_id,
				ri.name
			FROM
				reports r
				JOIN report_instances ri ON ( ri.cid = r.cid AND ri.report_id = r.id )
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND r.id = ' . ( int ) $intReportId . '
				AND ri.deleted_by IS NULL
				AND ri.deleted_on IS NULL
			ORDER BY
				ri.id
			LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportInstanceDetailsByIdByCid( $intReportInstanceId, $intCid, $objDatabase ) {
		$strSql = '
			SELECT
				r.name,
				util_get_translated( \'title\', r.title, r.details ) AS title,
				r.id,
				ri.id as report_instance_id,
				ri.name as report_instance_name,
				ri.description,
				ri.report_filter_id
			FROM
				reports r
				JOIN report_instances ri ON ( r.cid = ri.cid AND r.id = ri.report_id )
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.id = ' . ( int ) $intReportInstanceId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportInstanceByCidByDefaultReportIdByName( $intCid, $intDefaultReportId, $strReportName, $objDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intDefaultReportId ) || false == valStr( $strReportName ) ) {
			return NULL;
		}
		$strSql = '
			SELECT
				ri.id
			FROM
				reports r
				JOIN report_instances ri ON ( ri.cid = r.cid AND ri.report_id = r.id )
			WHERE
				r.cid = ' . ( int ) $intCid . '
				AND r.default_report_id = ' . ( int ) $intDefaultReportId . '
				AND r.name = \'' . ( string ) $strReportName . '\'
				AND ri.deleted_on IS NULL
			ORDER BY
				ri.id
			LIMIT 1';

		$arrintData = fetchData( $strSql, $objDatabase );
		if ( true == isset ( $arrintData[0]['id'] ) ) return $arrintData[0]['id'];
		return 0;

	}

	public static function fetchReportInstancesByDefaultFilterIdByCid( $intDefaultFilterId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				ri.id
			FROM
				report_instances ri
				JOIN report_filters rf ON ( ri.cid = rf.cid AND rf.id = ri.report_filter_id )
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.deleted_by IS NULL
				AND rf.deleted_by IS NULL
				AND rf.is_public = true
				AND ri.report_filter_id = ' . ( int ) $intDefaultFilterId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchParentsByReportModuleIdByCid( $intReportModuleId, $intCid, $objDatabase ) {
		$strSql = 'SELECT
					ri.parent_module_id AS report_group_module,
					rg.parent_module_id AS report_category_module
				FROM
					report_instances ri
					JOIN report_groups rg ON ( rg.cid = ri.cid AND ri.report_group_id = rg.id )
				WHERE
					ri.cid = ' . ( int ) $intCid . '
					AND ri.module_id = ' . ( int ) $intReportModuleId . ' ';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportInstancesByReportNameByCid( $strReportName, $intCid, $objDatabase ) {

		if( false == valStr( $strReportName ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
					m.name
				FROM report_instances ri
                JOIN reports r ON( ri.cid = r.cid AND ri.report_id = r.id )
                JOIN modules m ON( ri.module_id = m.id )
                WHERE r.name = \'' . ( string ) $strReportName . '\'
                      AND ri.cid = ' . ( int ) $intCid . ' ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchReportInstancesByReportFilterIdByReportGroupTypeIdByCid( $intReportFilterId, $intReportGroupTypeId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				ri.*
			FROM
				report_instances ri
				JOIN report_groups rg ON ri.cid =  rg.cid AND rg.id = ri.report_group_id
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND ri.report_filter_id =  ' . ( int ) $intReportFilterId . '
				AND rg.report_group_type_id =  ' . ( int ) $intReportGroupTypeId . '
				AND ri.deleted_by IS NULL
				AND ri.deleted_on IS NULL';

		return self::fetchReportInstances( $strSql, $objDatabase );

	}

	public static function fetchActiveReportInstancesByReportGroupIdByCid( $intReportGroupId, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				ri.*
			FROM
				report_instances ri
				JOIN report_versions rv ON ( ri.report_version_id = rv.id AND ri.cid = rv.cid )
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND COALESCE((rv.definition ->> \'is_published\')::BOOLEAN, TRUE) = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND ri.report_group_id =  ' . ( int ) $intReportGroupId . '
				AND ri.deleted_by IS NULL';

		return self::fetchReportInstances( $strSql, $objDatabase );

	}

	public static function fetchReportInstanceModuleIdsByReportNameByCid( string $strReportName, int $intCid, $objDatabase ) : array {

		if( !valStr( $strReportName ) ) {
			return [];
		}

		$strSql = '
				SELECT 
					ri.module_id
				FROM 
					report_instances ri
					JOIN reports r ON ri.cid = r.cid AND ri.report_id = r.id
					JOIN report_groups rg ON ri.cid = rg.cid AND ri.report_group_id = rg.id
				WHERE
					ri.cid = ' . ( int ) $intCid . ' AND
					ri.deleted_by IS NULL AND
					r.is_published = TRUE AND
					r.name = \'' . ( string ) $strReportName . '\' AND 
					rg.report_group_type_id = ' . CReportGroupType::STANDARD;

		return fetchOrCacheData( $strSql, 180, $strSql, $objDatabase );
	}

	public function fetchSapReportInstancesByCid( $intCid, $objDatabase ) {

		$strSql = '
				SELECT 
					util_get_translated( \'title\', ri.name, ri.details ) AS name,
					ri.id,
					ri.created_by AS company_user_id,
					ri.report_version_id,
					ri.report_id,
					ce.name_first,
					ce.name_last,
					rg.name AS report_group,
					rep_packet.packet_count,
					COALESCE( rep_sch.schedule_count, 0 ) AS schedule_count
				FROM
					report_instances AS ri
					JOIN reports AS r ON ri.cid = r.cid AND ri.report_id = r.id
					JOIN report_groups AS rg ON ri.cid = rg.cid AND ri.report_group_id = rg.id
					JOIN company_users AS cu ON cu.cid = ri.cid AND cu.id = ri.created_by
					LEFT JOIN company_employees AS ce ON cu.company_employee_id = ce.id AND cu.cid = ce.cid
					LEFT JOIN LATERAL (
						SELECT
							COUNT ( rep_grp.id ) AS packet_count
						FROM
							report_instances AS rep_ins 
							JOIN report_groups AS rep_grp ON rep_ins.cid = rep_grp.cid AND rep_ins.report_group_id = rep_grp.id
						WHERE
							rep_ins.cid = ' . ( int ) $intCid . '
							AND rep_grp.report_group_type_id = ' . \CReportGroupType::PACKET . '
							AND rep_ins.report_id = ri.report_id
							AND rep_ins.report_version_id = ri.report_version_id
							AND rep_grp.deleted_by IS NULL
					) AS rep_packet ON TRUE
					LEFT JOIN LATERAL (
						SELECT
							COUNT ( rs.id ) AS schedule_count,
							rs.report_filter_id
						FROM
							report_schedules AS rs
							JOIN report_filters AS rf ON rs.cid = rf.cid AND rs.report_filter_id = rf.id AND ri.report_id = rf.report_id
						WHERE
							rs.cid = ' . ( int ) $intCid . '
							AND rs.deleted_by IS NULL
						GROUP BY
							rs.report_filter_id
					) AS rep_sch ON TRUE
				WHERE
					ri.cid = ' . ( int ) $intCid . '
					AND ri.deleted_by IS NULL
					AND r.is_published = TRUE
					AND ri.details->\'hide_from_migration\' IS NULL
					AND r.report_type_id = ' . \CReportType::SAP . '
					AND rg.report_group_type_id = ' . \CReportGroupType::STANDARD . '
				ORDER BY
					ri.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInsightReportInstanceByReportVersionIdByCid( $intReportVersionId, $intCId, $objDatabase ) {
		$strSql = '
				SELECT
					ri.id,
					ri.report_version_id,
					ri.created_by AS company_user_id,
					ri.report_id,
					ri.description,
					r.report_type_id,
					ri.name AS title,
					ri.report_filter_id,
					ri.name,
					rv.major,
					rv.minor
					
				FROM
					report_instances AS ri
					JOIN report_versions AS rv ON ri.report_version_id = rv.id AND ri.cid = rv.cid
					JOIN reports AS r ON ri.report_id = r.id AND ri.cid = r.cid
				WHERE
					rv.default_report_version_id IS NULL
					AND ri.deleted_by IS NULL
					AND ri.deleted_on IS NULL
					AND r.deleted_by IS NULL
					AND r.deleted_on IS NULL
					AND r.report_type_id = ' . CReportType::SAP . '
					AND rv.id = ' . ( int ) $intReportVersionId . '
					AND ri.cid = ' . ( int ) $intCId;

		return rekeyArray( 'report_version_id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchReportInstancesByReportIdsByCid( array $arrintReportIds, $intCid, $objDatabase ) {

		if( !valArr( $arrintReportIds ) ) return [];

		$strSql = '
			SELECT
				ri.*
			FROM
				report_instances ri
				JOIN report_versions rv ON ( ri.report_version_id = rv.id AND ri.cid = rv.cid )
			WHERE
				ri.cid = ' . ( int ) $intCid . '
				AND COALESCE((rv.definition ->> \'is_published\')::BOOLEAN, TRUE) = TRUE
				AND COALESCE( rv.expiration, NOW() ) >= NOW()
				AND ri.report_id IN ( ' . sqlIntImplode( $arrintReportIds ) . ' )
				AND ri.deleted_by IS NULL';

		return self::fetchReportInstances( $strSql, $objDatabase );
	}

}
